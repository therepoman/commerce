package ingestion

import (
	"context"
	"errors"
	"testing"

	dynamo_event "code.justin.tv/commerce/pantheon/backend/dynamo/event"
	dynamo_moderation "code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/timeunit"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	event_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/event"
	moderation_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	index_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/index"
	redis_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	shadow_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis/shadow"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/uber-go/tally"
)

func TestLeaderboardIngester_Ingest(t *testing.T) {
	Convey("Given a persist ingester", t, func() {
		mockFactory := new(redis_mock.LeaderboardFactory)
		mockShadowKeyManager := new(shadow_mock.KeyManager)
		mockEventDao := new(event_mock.DAO)
		mockModerationDao := new(moderation_mock.DAO)
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			EventDAO:      mockEventDao,
			ModerationDAO: mockModerationDao,
		})
		groupingKeyIndexFactory := new(redis_mock.GroupingKeyIndexFactory)
		groupingKeyIndex := new(index_mock.Index)
		groupingKeyIndexFactory.On("GetRedisGroupingKeyIndex", mock.Anything, mock.Anything).Return(groupingKeyIndex)

		ingester := LeaderboardIngester{
			LeaderboardFactory:  mockFactory,
			IndexFactory:        groupingKeyIndexFactory,
			ShadowKeyManager:    mockShadowKeyManager,
			TenantClient:        mockTenantClient,
			RootMetricsScope:    tally.NoopScope,
			DomainConfigManager: domain.NewHardCodedConfigManager(),
		}

		Convey("Errors when ingesting nil event", func() {
			So(ingester.Ingest(context.Background(), nil), ShouldNotBeNil)
		})

		Convey("Given a test event", func() {
			e := test.RandomEvent()

			Convey("Errors when id is blank", func() {
				e.ID = ""
				So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
			})

			Convey("Errors when domain is blank", func() {
				e.Domain = ""
				So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
			})

			Convey("Errors when entry key is blank", func() {
				e.EntryKey = ""
				So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
			})

			Convey("Errors when dynamo errors getting event", func() {
				mockEventDao.On("GetEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
			})

			Convey("Errors when dynamo entry key does not match", func() {
				mockEventDao.On("GetEvent", mock.Anything, mock.Anything).Return(&dynamo_event.Event{
					EntryKey: "Not_" + e.EntryKey,
					Value:    e.EventValue,
				}, nil)
				So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
			})

			Convey("Errors when dynamo event value does not match", func() {
				mockEventDao.On("GetEvent", mock.Anything, mock.Anything).Return(&dynamo_event.Event{
					EntryKey: e.EntryKey,
					Value:    e.EventValue + 1,
				}, nil)
				So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
			})

			Convey("Exits early when dynamo key and value matches", func() {
				mockEventDao.On("GetEvent", mock.Anything, mock.Anything).Return(&dynamo_event.Event{
					EntryKey: e.EntryKey,
					Value:    e.EventValue,
				}, nil)
				So(ingester.Ingest(context.Background(), &e), ShouldBeNil)
				mockFactory.AssertNotCalled(t, "GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything)
			})

			Convey("Exits early when dynamo value matches and key was never set", func() {
				mockEventDao.On("GetEvent", mock.Anything, mock.Anything).Return(&dynamo_event.Event{
					EntryKey: "",
					Value:    e.EventValue,
				}, nil)
				So(ingester.Ingest(context.Background(), &e), ShouldBeNil)
				mockFactory.AssertNotCalled(t, "GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything)
			})

			Convey("When dynamo event does not exist", func() {
				mockEventDao.On("GetEvent", mock.Anything, mock.Anything).Return(nil, nil)

				Convey("When the factory returns a leaderboard", func() {
					lb := local.NewLeaderboard()
					mockFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(lb, nil)

					Convey("Errors when adding to grouping key index fails", func() {
						groupingKeyIndex.On("Add", mock.Anything, mock.Anything).Return(errors.New("failed to add to grouping key index"))
						So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
					})

					Convey("When adding to grouping key index succeeds", func() {
						groupingKeyIndex.On("Add", mock.Anything, mock.Anything).Return(nil)

						Convey("Errors when dynamo event update fails", func() {
							mockEventDao.On("UpdateEvent", mock.Anything).Return(errors.New("test error"))
							So(ingester.Ingest(context.Background(), &e), ShouldNotBeNil)
						})

						Convey("When dynamo event update succeeds", func() {
							mockEventDao.On("UpdateEvent", mock.Anything).Return(nil)

							Convey("When the moderation dao returns nil", func() {
								mockModerationDao.On("GetModeration", mock.Anything, mock.Anything).Return(nil, nil)

								Convey("When shadow key manager succeeds", func() {
									mockShadowKeyManager.On("EnsureShadowKeyExists", mock.Anything).Return(nil)

									Convey("When we ingest an event", func() {
										err := ingester.Ingest(context.Background(), &e)

										Convey("The ingester does not return an error", func() {
											So(err, ShouldBeNil)
										})

										Convey("The change was persisted in the leaderboard", func() {
											len, err := lb.Len(leaderboard.RetrievalSettings{})
											So(err, ShouldBeNil)
											So(len, ShouldEqual, 1)
										})

										Convey("The shadow key manager was called", func() {
											mockShadowKeyManager.AssertCalled(t, "EnsureShadowKeyExists", mock.Anything)
										})

										Convey("The factory should have been called for each timeunit", func() {
											So(len(mockFactory.Calls), ShouldEqual, len(timeunit.GetTimeUnits()))
										})
									})

									Convey("When we ingest a fortuna event", func() {
										e2 := test.RandomEvent()
										e2.Domain = string(domain.FortunaProgressDomain)
										err := ingester.Ingest(context.Background(), &e2)

										Convey("The ingester does not return an error", func() {
											So(err, ShouldBeNil)
										})

										Convey("The change was persisted in the leaderboard", func() {
											len, err := lb.Len(leaderboard.RetrievalSettings{})
											So(err, ShouldBeNil)
											So(len, ShouldEqual, 1)
										})

										Convey("The shadow key manager was called", func() {
											mockShadowKeyManager.AssertCalled(t, "EnsureShadowKeyExists", mock.Anything)
										})

										Convey("The factory should have been called for only the all-time time unit", func() {
											So(len(mockFactory.Calls), ShouldEqual, 1)
										})
									})
								})

								Convey("When shadow key manager fails", func() {
									mockShadowKeyManager.On("EnsureShadowKeyExists", mock.Anything).Return(errors.New("test error"))

									Convey("When we ingest an event", func() {
										err := ingester.Ingest(context.Background(), &e)

										Convey("The ingester still does not return an error", func() {
											So(err, ShouldBeNil)
										})

										Convey("The change was still persisted in the leaderboard", func() {
											len, err := lb.Len(leaderboard.RetrievalSettings{})
											So(err, ShouldBeNil)
											So(len, ShouldEqual, 1)
										})
									})
								})
							})

							Convey("When the redis LB is not moderated but the moderation dao returns a moderated entry", func() {
								mockModerationDao.On("GetModeration", mock.Anything, mock.Anything).Return(&dynamo_moderation.Moderation{
									LeaderboardSetID: e.Domain + "/" + e.GroupingKey,
									EntryKey:         e.EntryKey,
									State:            "MODERATE",
								}, nil)

								Convey("When shadow key manager succeeds", func() {
									mockShadowKeyManager.On("EnsureShadowKeyExists", mock.Anything).Return(nil)

									Convey("When we ingest an event", func() {
										err := ingester.Ingest(context.Background(), &e)

										Convey("The ingester does not return an error", func() {
											So(err, ShouldBeNil)
										})

										Convey("The change was persisted in the leaderboard and the entry is moderated", func() {
											len, err := lb.Len(leaderboard.RetrievalSettings{})
											So(err, ShouldBeNil)
											So(len, ShouldEqual, 1)

											entryScore, err := lb.GetScore(entry.Key(e.EntryKey), leaderboard.RetrievalSettings{ConsistentRead: true})
											So(err, ShouldBeNil)
											So(entryScore.DecodeScore().ModeratedFlag, ShouldEqual, score.Moderated)
										})

										Convey("The shadow key manager was called", func() {
											mockShadowKeyManager.AssertCalled(t, "EnsureShadowKeyExists", mock.Anything)
										})

										Convey("The factory should have been called for each timeunit", func() {
											So(len(mockFactory.Calls), ShouldEqual, len(timeunit.GetTimeUnits()))
										})
									})
								})
							})

							Convey("When the redis LB is moderated but the moderation dao returns a unmoderated entry", func() {
								err := lb.ModerateEntry(entry.Key(e.EntryKey))
								So(err, ShouldBeNil)

								mockModerationDao.On("GetModeration", mock.Anything, mock.Anything).Return(&dynamo_moderation.Moderation{
									LeaderboardSetID: e.Domain + "/" + e.GroupingKey,
									EntryKey:         e.EntryKey,
									State:            "UNMODERATE",
								}, nil)

								Convey("When shadow key manager succeeds", func() {
									mockShadowKeyManager.On("EnsureShadowKeyExists", mock.Anything).Return(nil)

									Convey("When we ingest an event", func() {
										err := ingester.Ingest(context.Background(), &e)

										Convey("The ingester does not return an error", func() {
											So(err, ShouldBeNil)
										})

										Convey("The change was persisted in the leaderboard and the entry is not moderated", func() {
											len, err := lb.Len(leaderboard.RetrievalSettings{})
											So(err, ShouldBeNil)
											So(len, ShouldEqual, 1)

											entryScore, err := lb.GetScore(entry.Key(e.EntryKey), leaderboard.RetrievalSettings{ConsistentRead: true})
											So(err, ShouldBeNil)
											So(entryScore.DecodeScore().ModeratedFlag, ShouldEqual, score.NotModerated)
										})

										Convey("The shadow key manager was called", func() {
											mockShadowKeyManager.AssertCalled(t, "EnsureShadowKeyExists", mock.Anything)
										})

										Convey("The factory should have been called for each timeunit", func() {
											So(len(mockFactory.Calls), ShouldEqual, len(timeunit.GetTimeUnits()))
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
