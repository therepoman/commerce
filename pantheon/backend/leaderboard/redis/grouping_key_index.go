package redis

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/redis"
)

const (
	GroupingKeyIndexPrefix = "grouping_key_index"
)

type GroupingKeyIndex struct {
	Domain      identifier.Domain
	GroupingKey identifier.GroupingKey
	redis       redis.Client
}

func NewGroupingKeyIndex(redis redis.Client, domain identifier.Domain, groupingKey identifier.GroupingKey) *GroupingKeyIndex {
	return &GroupingKeyIndex{
		Domain:      domain,
		GroupingKey: groupingKey,
		redis:       redis,
	}
}

func (i *GroupingKeyIndex) Key() string {
	return fmt.Sprintf("%s-%s-%s", GroupingKeyIndexPrefix, i.Domain, i.GroupingKey)
}

func (i *GroupingKeyIndex) Add(ctx context.Context, id identifier.Identifier) error {
	_, err := i.redis.SAdd(ctx, i.Key(), id.Key())
	return err
}

func (i *GroupingKeyIndex) Delete(ctx context.Context) error {
	_, err := i.redis.Del(ctx, i.Key())
	return err
}

func (i *GroupingKeyIndex) DeleteMember(ctx context.Context, id identifier.Identifier) error {
	_, err := i.redis.SRem(ctx, i.Key(), id.Key())
	return err
}

func (i *GroupingKeyIndex) Get(ctx context.Context, rs leaderboard.RetrievalSettings) ([]identifier.Identifier, error) {
	keys, err := i.redis.SMembers(ctx, i.Key(), rs.ToRedisReadSettings())
	var ids []identifier.Identifier
	for _, key := range keys {
		var id identifier.Identifier
		id, err = identifier.FromKey(key)
		if err != nil {
			return nil, err
		}

		ids = append(ids, id)
	}

	return ids, err
}
