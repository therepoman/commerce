package redis

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	. "code.justin.tv/commerce/pantheon/test/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGroupingKeyIndex(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		Convey("Given a grouping key index", func() {
			id := test.RandomIdentifier()
			i := NewGroupingKeyIndex(redisClient, id.Domain, id.GroupingKey)

			Convey("Members can be added and fetched", func() {
				id1 := test.RandomIdentifier()
				err := i.Add(context.Background(), id1)
				So(err, ShouldBeNil)
				var results []identifier.Identifier
				results, _ = i.Get(context.Background(), leaderboard.RetrievalSettings{})
				So(results[0].Key(), ShouldEqual, id1.Key())
			})

			Convey("Index can be deleted", func() {
				err := i.Delete(context.Background())
				So(err, ShouldBeNil)
				var results []identifier.Identifier
				results, _ = i.Get(context.Background(), leaderboard.RetrievalSettings{})
				So(len(results), ShouldEqual, 0)
			})
		})
	}))
}
