package shadow

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "code.justin.tv/commerce/pantheon/test/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func testID(domain identifier.Domain, tau pantheonrpc.TimeUnit) identifier.Identifier {
	return identifier.Identifier{
		Domain:              domain,
		TimeAggregationUnit: tau,
		GroupingKey:         "default",
		TimeBucket:          bucket.TimestampToTimeBucket(time.Now(), tau),
	}
}

func shadowKeyExists(redisClient redis.Client, id identifier.Identifier) bool {
	_, err := redisClient.Get(context.Background(), GetShadowKey(id), redis.ReadSettings{})
	return err == nil
}

func shadowKeyTTL(redisClient redis.Client, id identifier.Identifier) time.Duration {
	dur, err := redisClient.TTL(context.Background(), GetShadowKey(id), redis.ReadSettings{})
	if err != nil {
		return 0 * time.Nanosecond
	}
	return dur
}

func TestKeyManagerImpl_EnsureShadowKeyExists(t *testing.T) {
	Convey("Given a shadow key manager", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			RedisClient: redisClient,
		})
		km := &KeyManagerImpl{
			TenantClient:        mockTenantClient,
			DomainConfigManager: domain.NewHardCodedConfigManager(),
		}

		Convey("Succeeds without writing to redis for all-time with no TTL enabled", func() {
			id := testID(domain.GlobalDomain, pantheonrpc.TimeUnit_ALLTIME)
			err := km.EnsureShadowKeyExists(id)
			So(err, ShouldBeNil)
			So(shadowKeyExists(redisClient, id), ShouldBeFalse)
		})

		Convey("Succeeds with writing to redis for all-time with TTL enabled", func() {
			id := testID(domain.HypeTrainDomain, pantheonrpc.TimeUnit_ALLTIME)
			err := km.EnsureShadowKeyExists(id)
			So(err, ShouldBeNil)
			So(shadowKeyExists(redisClient, id), ShouldBeTrue)
			exp := time.Now().Add(km.DomainConfigManager.Get(domain.HypeTrainDomain).LeaderboardTTLDuration)
			So(shadowKeyTTL(redisClient, id), ShouldBeBetween, time.Until(exp.Add(-5*time.Second)), time.Until(exp.Add(5*time.Second)))
		})

		Convey("Succeeds with writing to redis for day", func() {
			tu := pantheonrpc.TimeUnit_DAY
			id := testID(domain.GlobalDomain, tu)
			err := km.EnsureShadowKeyExists(id)
			So(err, ShouldBeNil)
			So(shadowKeyExists(redisClient, id), ShouldBeTrue)
			exp := *bucket.TimestampToTimeBucket(time.Now(), tu).ToExpirationTime(tu)
			So(shadowKeyTTL(redisClient, id), ShouldBeBetween, time.Until(exp.Add(MinDayOffset)), time.Until(exp.Add(MaxDayOffset)))
		})

		Convey("Succeeds with writing to redis for week", func() {
			tu := pantheonrpc.TimeUnit_WEEK
			id := testID(domain.GlobalDomain, tu)
			err := km.EnsureShadowKeyExists(id)
			So(err, ShouldBeNil)
			So(shadowKeyExists(redisClient, id), ShouldBeTrue)
			exp := *bucket.TimestampToTimeBucket(time.Now(), tu).ToExpirationTime(tu)
			So(shadowKeyTTL(redisClient, id), ShouldBeBetween, time.Until(exp.Add(MinWeekOffset)), time.Until(exp.Add(MaxWeekOffset)))
		})

		Convey("Succeeds with writing to redis for month", func() {
			tu := pantheonrpc.TimeUnit_MONTH
			id := testID(domain.GlobalDomain, tu)
			err := km.EnsureShadowKeyExists(id)
			So(err, ShouldBeNil)
			So(shadowKeyExists(redisClient, id), ShouldBeTrue)
			exp := *bucket.TimestampToTimeBucket(time.Now(), tu).ToExpirationTime(tu)
			So(shadowKeyTTL(redisClient, id), ShouldBeBetween, time.Until(exp.Add(MinMonthOffset)), time.Until(exp.Add(MaxMonthOffset)))
		})

		Convey("Succeeds with writing to redis for year", func() {
			tu := pantheonrpc.TimeUnit_YEAR
			id := testID(domain.GlobalDomain, tu)
			err := km.EnsureShadowKeyExists(id)
			So(err, ShouldBeNil)
			So(shadowKeyExists(redisClient, id), ShouldBeTrue)
			exp := *bucket.TimestampToTimeBucket(time.Now(), tu).ToExpirationTime(tu)
			So(shadowKeyTTL(redisClient, id), ShouldBeBetween, time.Until(exp.Add(MinYearOffset)), time.Until(exp.Add(MaxYearOffset)))
		})
	}))
}
