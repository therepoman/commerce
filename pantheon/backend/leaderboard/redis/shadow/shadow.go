package shadow

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/pantheon/utils/random"
)

const (
	ShadowSuffix   = "-shadow"
	MinDayOffset   = time.Hour
	MaxDayOffset   = 12 * time.Hour
	MinWeekOffset  = 24 * time.Hour
	MaxWeekOffset  = 2 * 24 * time.Hour
	MinMonthOffset = 2 * 24 * time.Hour
	MaxMonthOffset = 6 * 24 * time.Hour
	MinYearOffset  = 6 * 24 * time.Hour
	MaxYearOffset  = 20 * 24 * time.Hour
)

type KeyManager interface {
	EnsureShadowKeyExists(id identifier.Identifier) error
}

type KeyManagerImpl struct {
	TenantClient        tenant.Landlord      `inject:""`
	DomainConfigManager domain.ConfigManager `inject:""`
}

func GetShadowKey(id identifier.Identifier) string {
	return id.Key() + ShadowSuffix
}

func (km *KeyManagerImpl) EnsureShadowKeyExists(id identifier.Identifier) error {
	domainConfig := km.DomainConfigManager.Get(id.Domain)

	if id.TimeAggregationUnit == pantheonrpc.TimeUnit_ALLTIME && domainConfig.IsKnownDomain() && !domainConfig.LeaderboardTTL {
		return nil
	}

	key := GetShadowKey(id)
	existsResult, err := km.TenantClient.GetTenant(id.Domain).RedisClient.Exists(context.Background(), []string{key}, redis.ReadSettings{ConsistentRead: true})
	if err != nil {
		return err
	}

	shadowKeyExists := false
	if existsResult > 0 {
		shadowKeyExists = true
	}

	if shadowKeyExists {
		return nil
	}

	_, err = km.TenantClient.GetTenant(id.Domain).RedisClient.Set(context.Background(), key, true, time.Duration(0))
	if err != nil {
		return err
	}

	var expireTimeWithOffset time.Time
	if domainConfig.IsKnownDomain() && domainConfig.LeaderboardTTL {
		expireTimeWithOffset = time.Now().Add(domainConfig.LeaderboardTTLDuration)
	} else {
		expireTime := id.TimeBucket.ToExpirationTime(id.TimeAggregationUnit)
		if expireTime == nil {
			return nil
		}

		if expireTime.Before(time.Now()) {
			now := time.Now()
			expireTime = &now
		}
		expireTimeWithOffset = addExpirationOffset(*expireTime, id.TimeAggregationUnit)
	}

	expSet, err := km.TenantClient.GetTenant(id.Domain).RedisClient.ExpireAt(context.Background(), key, expireTimeWithOffset)

	if err != nil {
		return err
	}

	if !expSet {
		return errors.New("could not set shadow key expiration")
	}

	return nil
}

func addExpirationOffset(expireTime time.Time, unit pantheonrpc.TimeUnit) time.Time {
	var dur time.Duration
	switch unit {
	case pantheonrpc.TimeUnit_DAY:
		dur = random.Duration(MinDayOffset, MaxDayOffset)
	case pantheonrpc.TimeUnit_WEEK:
		dur = random.Duration(MinWeekOffset, MaxWeekOffset)
	case pantheonrpc.TimeUnit_MONTH:
		dur = random.Duration(MinMonthOffset, MaxMonthOffset)
	case pantheonrpc.TimeUnit_YEAR:
		dur = random.Duration(MinYearOffset, MaxYearOffset)
	}
	return expireTime.Add(dur)
}
