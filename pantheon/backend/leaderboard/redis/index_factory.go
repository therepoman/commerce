package redis

import (
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/index"
	"code.justin.tv/commerce/pantheon/clients/tenant"
)

type GroupingKeyIndexFactory interface {
	GetRedisGroupingKeyIndex(domain identifier.Domain, groupingKey identifier.GroupingKey) index.Index
}

type IndexFactory struct {
	TenantClient tenant.Landlord `inject:""`
}

func (f IndexFactory) GetRedisGroupingKeyIndex(domain identifier.Domain, groupingKey identifier.GroupingKey) index.Index {
	t := f.TenantClient.GetTenant(domain)
	return NewGroupingKeyIndex(t.RedisClient, domain, groupingKey)
}
