package redis

import (
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"

	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "code.justin.tv/commerce/pantheon/test/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestIndexFactory_GetRedisGroupingKeyIndex(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			RedisClient:           redisClient,
			StandardLockingClient: lockingClient,
		})

		if !usingLocal {
			t.Skip("Skipping factory test. Pass the test arg redis.local to run this")
		}

		f := &IndexFactory{
			TenantClient: mockTenantClient,
		}

		Convey("Can create a grouping key index", func() {
			id := test.RandomIdentifier()
			i := f.GetRedisGroupingKeyIndex(id.Domain, id.GroupingKey)
			So(i, ShouldNotBeNil)
		})
	}))
}
