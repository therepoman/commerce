package redis

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/utils/math"
	goRedis "github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
)

const (
	InfiniteLowerBoundSymbol = "-inf"
	InfiniteUpperBoundSymbol = "+inf"
)

type Leaderboard struct {
	identifier.Identifier
	redis         redis.Client
	lockingClient lock.LockingClient
	observers     []observers.Observer
}

func NewLeaderboard(redis redis.Client, lockingClient lock.LockingClient, id identifier.Identifier) *Leaderboard {
	return &Leaderboard{
		Identifier:    id,
		lockingClient: lockingClient,
		redis:         redis,
	}
}

func Exists(ctx context.Context, rc redis.Client, id identifier.Identifier, rs leaderboard.RetrievalSettings) (bool, error) {
	var exists int64
	exists, err := rc.Exists(ctx, []string{id.Key()}, rs.ToRedisReadSettings())
	if err == goRedis.Nil {
		exists = 0
	} else if err != nil {
		return false, err
	}

	if exists > 0 {
		return true, nil
	}
	return false, nil
}

func (l *Leaderboard) AddObservers(observers ...observers.Observer) {
	l.observers = append(l.observers, observers...)
}

func (l *Leaderboard) notify(lbUpdate observers.LeaderboardUpdate) {
	for _, observer := range l.observers {
		// we create a copy of the update to avoid concurrent write to metadata map
		lbUpdateCopy := observers.LeaderboardUpdate{
			ID:             lbUpdate.ID,
			Leaderboard:    lbUpdate.Leaderboard,
			UpdateEntryKey: lbUpdate.UpdateEntryKey,
		}
		if lbUpdate.Event != nil {
			updateEventCopy := event.Event{
				Domain:      lbUpdate.Event.Domain,
				ID:          lbUpdate.Event.ID,
				TimeOfEvent: lbUpdate.Event.TimeOfEvent,
				GroupingKey: lbUpdate.Event.GroupingKey,
				EntryKey:    lbUpdate.Event.EntryKey,
				EventValue:  lbUpdate.Event.EventValue,
			}
			lbUpdateCopy.Event = &updateEventCopy
		}
		go observer.Observe(lbUpdateCopy)
	}
}

func (lb *Leaderboard) Type() string {
	return "redis"
}

func (lb *Leaderboard) executeWithEntryLock(key entry.Key, lockedFunction func() error) error {
	lockKey := fmt.Sprintf("%s:%s", lb.Identifier.Key(), key)
	lbEntryLock, err := lb.lockingClient.ObtainLock(lockKey)
	if err != nil {
		return err
	}
	defer func() {
		unlockErr := lbEntryLock.Unlock()
		if unlockErr != nil {
			log.WithField("lockKey", lockKey).WithError(err).Error("could not unlock redis leaderboard entry lock")
		}
	}()

	return lockedFunction()
}

func (lb *Leaderboard) setScore(key entry.Key, score score.Score, event *event.Event) error {
	z := goRedis.Z{
		Score:  float64(score),
		Member: string(key),
	}

	_, err := lb.redis.ZAdd(context.Background(), lb.Key(), &z)
	if err == nil {
		lb.notify(observers.LeaderboardUpdate{
			ID:             lb.Identifier,
			Leaderboard:    lb,
			UpdateEntryKey: &key,
			Event:          event,
		})
	}
	return err
}

func (lb *Leaderboard) UpdateScore(key entry.Key, scoreAddition score.EntryScore, event *event.Event) error {
	return lb.executeWithEntryLock(key, func() error {
		prevScore, err := lb.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
		if err != nil {
			return err
		}

		return lb.setScore(key, prevScore.Add(scoreAddition), event)
	})
}

func (lb *Leaderboard) ClearScores() error {
	_, err := lb.redis.Del(context.Background(), lb.Key())
	if err == nil {
		lb.notify(observers.LeaderboardUpdate{
			ID:          lb.Identifier,
			Leaderboard: lb,
		})
	}
	return err
}

func (lb *Leaderboard) GetScore(key entry.Key, rs leaderboard.RetrievalSettings) (score.Score, error) {
	redisScore, err := lb.redis.ZScore(context.Background(), lb.Key(), string(key), rs.ToRedisReadSettings())
	if err == goRedis.Nil {
		return score.Score(0), nil
	} else if err != nil {
		return score.Score(0), err
	}

	return score.Score(redisScore), nil
}

func (lb *Leaderboard) Len(rs leaderboard.RetrievalSettings) (int, error) {
	count, err := lb.redis.ZCard(context.Background(), lb.Key(), rs.ToRedisReadSettings())

	if err != nil {
		return 0, err
	}

	return int(count), nil
}

func (lb *Leaderboard) GetTop(ctx context.Context, n int, rs leaderboard.RetrievalSettings) ([]entry.RankedEntry, error) {
	n = math.MaxInt(0, n-1)
	members, err := lb.redis.ZRevRangeWithScores(ctx, lb.Key(), 0, int64(n), rs.ToRedisReadSettings())

	if err == goRedis.Nil {
		return []entry.RankedEntry{}, nil
	} else if err != nil {
		return nil, err
	}

	return sortedSetMembersToRankedEntries(members, 1)
}

func (lb *Leaderboard) GetTopWithTimeoutAndRetry(ctx context.Context, n int, rs leaderboard.RetrievalSettings, timeout time.Duration, maxAttempts int) ([]entry.RankedEntry, error) {
	var entries []entry.RankedEntry
	var err error
	for attempt := 0; attempt < maxAttempts; attempt++ {
		contextForGetTop, cancel := context.WithTimeout(ctx, timeout)
		entries, err = lb.GetTop(contextForGetTop, n, rs)
		cancel()
		if err != nil {
			if err == context.DeadlineExceeded {
				log.WithFields(log.Fields{
					"timeout":     timeout,
					"attempt":     attempt,
					"maxAttempts": maxAttempts,
					"lb.Key":      lb.Key(),
					"n":           n,
				}).Info("GetTop exceeded context deadline, retrying")
				continue
			}
			return entries, err
		}
	}

	return entries, err
}

func (lb *Leaderboard) GetRank(k entry.Key, rs leaderboard.RetrievalSettings) (entry.Rank, error) {
	rank, err := lb.redis.ZRevRank(context.Background(), lb.Key(), string(k), rs.ToRedisReadSettings())
	if err != nil {
		return entry.Rank(0), err
	}

	return entry.Rank(rank + 1), nil
}

func (lb *Leaderboard) GetCountForRange(ctx context.Context, min *int64, max *int64, rs leaderboard.RetrievalSettings) (int64, error) {
	var minRedisValue string
	var maxRedisValue string

	if min == nil {
		minRedisValue = InfiniteLowerBoundSymbol
	} else {
		minRedisValue = strconv.FormatInt(*min, 10)
	}

	if max == nil {
		maxRedisValue = InfiniteUpperBoundSymbol
	} else {
		// Scores are stored as floats (with the decimal representing the time of the entry). Therefore,
		// in order to make our query properly inclusive, we must add 1.
		//
		// e.g. If an entry has a score of 100, actual stored value might be something like 100.65656453.
		// To capture all entries with a score of 100, we would query for 100 <= x <= 101.
		maxRedisValue = strconv.FormatInt(*max+1, 10)
	}

	count, err := lb.redis.ZCount(ctx, lb.Key(), minRedisValue, maxRedisValue, rs.ToRedisReadSettings())

	if err == goRedis.Nil {
		return 0, nil
	} else if err != nil {
		return 0, err
	}

	return int64(count), nil
}

func (lb *Leaderboard) GetEntriesAboveThreshold(threshold, limit, offset int64, rs leaderboard.RetrievalSettings) ([]entry.RankedEntry, error) {
	min := strconv.FormatInt(threshold, 10)

	members, err := lb.redis.ZRevRangeByScoreWithScore(context.Background(), lb.Key(), min, InfiniteUpperBoundSymbol, limit, offset, rs.ToRedisReadSettings())

	if err == goRedis.Nil {
		return []entry.RankedEntry{}, nil
	} else if err != nil {
		return nil, err
	}

	return sortedSetMembersToRankedEntries(members, int(offset+1))
}

func (lb *Leaderboard) GetEntryContext(ctx context.Context, key entry.Key, spread int, rs leaderboard.RetrievalSettings) (entryOut entry.RankedEntry, entries []entry.RankedEntry, err error) {
	rank, err := lb.redis.ZRevRank(ctx, lb.Key(), string(key), rs.ToRedisReadSettings())
	if err != nil {
		return
	}

	beginning := math.MaxInt64(rank-int64(spread), 0)
	end := rank + int64(spread)

	zs, err := lb.redis.ZRevRangeWithScores(ctx, lb.Key(), beginning, end, rs.ToRedisReadSettings())
	if err != nil {
		return
	}

	entries, err = sortedSetMembersToRankedEntries(zs, int(beginning)+1)
	if err != nil {
		return
	}

	index := rank - beginning
	sliceLength := int64(len(entries))
	if index < 0 || index >= sliceLength {
		err = errors.New("index out of bounds")
		log.WithFields(log.Fields{
			"rank":        rank,
			"lb.Key":      lb.Key(),
			"entry.Key":   key,
			"spread":      spread,
			"sliceLength": sliceLength,
		}).Error("index out of bounds")
		return
	}
	entryOut = entries[index]
	return
}

func (lb *Leaderboard) GetEntryContextWithTimeoutAndRetry(ctx context.Context, key entry.Key, spread int, rs leaderboard.RetrievalSettings, timeout time.Duration, maxAttempts int) (entry.RankedEntry, []entry.RankedEntry, error) {
	var entryOut entry.RankedEntry
	var entries []entry.RankedEntry
	var err error
	for attempt := 0; attempt < maxAttempts; attempt++ {
		contextForEntryLookup, cancel := context.WithTimeout(ctx, timeout)
		entryOut, entries, err = lb.GetEntryContext(contextForEntryLookup, key, spread, rs)
		cancel()
		if err != nil {
			if err == context.DeadlineExceeded {
				log.WithFields(log.Fields{
					"timeout":     timeout,
					"attempt":     attempt,
					"maxAttempts": maxAttempts,
					"lb.Key":      lb.Key(),
					"entry.Key":   key,
					"spread":      spread,
				}).Info("GetEntryContext exceeded context deadline, retrying")
				continue
			}
			return entryOut, entries, err
		}
	}

	return entryOut, entries, err
}

func (lb *Leaderboard) Serialize(rs leaderboard.RetrievalSettings) ([]byte, error) {
	result, err := lb.redis.Dump(context.Background(), lb.Key(), rs.ToRedisReadSettings())
	if err != nil {
		return nil, err
	}
	return []byte(result), nil
}

func (lb *Leaderboard) Restore(bs []byte) error {
	_, err := lb.redis.Restore(context.Background(), lb.Key(), time.Duration(0), string(bs))
	return err
}

func (lb *Leaderboard) Destroy() error {
	_, err := lb.redis.Del(context.Background(), lb.Identifier.Key())
	return err
}

func sortedSetMembersToRankedEntries(zs []goRedis.Z, startRank int) ([]entry.RankedEntry, error) {
	entries := make([]entry.RankedEntry, len(zs))
	for i, z := range zs {
		e, err := sortedSetMemberToEntry(z)
		if err != nil {
			return entries, err
		}

		entries[i] = entry.RankedEntry{
			Entry: e,
			Rank:  entry.Rank(i + startRank),
		}
	}
	return entries, nil
}

func sortedSetMemberToEntry(z goRedis.Z) (entry.Entry, error) {
	if s, ok := z.Member.(string); ok {
		es := score.Score(z.Score).DecodeScore()
		return entry.Entry{
			Key:   entry.Key(s),
			Score: es,
		}, nil
	}
	return entry.Entry{}, fmt.Errorf("encountered an unparsable redis sorted set member %v", z)
}

func (lb *Leaderboard) ModerateEntry(key entry.Key) error {
	return lb.executeWithEntryLock(key, func() error {
		prevScore, err := lb.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
		if err != nil {
			return err
		}

		// Entry is already moderated
		if prevScore.DecodeScore().ModeratedFlag == score.Moderated {
			return nil
		}

		// Don't moderate entries with a zero scores (meaning the entry does not exist)
		// We don't want to fill up the sorted set with garbage data
		if prevScore.DecodeScore().BaseScore == 0 {
			return nil
		}

		return lb.setScore(key, prevScore.Moderate(), nil)
	})
}

func (lb *Leaderboard) UnmoderateEntry(key entry.Key) error {
	return lb.executeWithEntryLock(key, func() error {
		prevScore, err := lb.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
		if err != nil {
			return err
		}

		// Entry is already unmoderated
		if prevScore.DecodeScore().ModeratedFlag == score.NotModerated {
			return nil
		}

		// Don't unmoderate entries with a zero scores (meaning the entry does not exist)
		// We don't want to fill up the sorted set with garbage data
		if prevScore.DecodeScore().BaseScore == 0 {
			return nil
		}

		return lb.setScore(key, prevScore.Unmoderate(), nil)
	})
}

func (lb *Leaderboard) DeleteEntry(key entry.Key) error {
	_, err := lb.redis.ZRem(context.Background(), lb.Key(), string(key))
	return err
}
