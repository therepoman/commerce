package redis

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "code.justin.tv/commerce/pantheon/test/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLeaderboardFactory_GetLeaderboard(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			RedisClient:           redisClient,
			StandardLockingClient: lockingClient,
		})

		if !usingLocal {
			t.Skip("Skipping factory test. Pass the test arg redis.local to run this")
		}

		Convey("Given a leaderboard that has been serialized", func() {
			id := test.RandomIdentifier()
			lb := NewLeaderboard(redisClient, lockingClient, id)
			defer func() {
				_ = lb.ClearScores()
			}()

			entries := test.RandomEntries(5)
			test.AddEntries(lb, entries...)
			top, _ := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})

			p := new(persistence.BytePersister)
			err := p.Export(context.Background(), lb.Identifier, lb, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)

			err = lb.ClearScores()
			So(err, ShouldBeNil)

			f := &Factory{
				TenantClient: mockTenantClient,
				Persister:    p,
			}

			Convey("The Factory Can create the leaderboard", func() {
				restored, err := f.GetRedisLeaderboard(context.Background(), id, leaderboard.RetrievalSettings{})
				So(restored, ShouldNotBeNil)
				So(err, ShouldBeNil)
				Convey("Leaderboard has the same top score", func() {
					newTop, err := restored.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					So(newTop[0].Key, ShouldEqual, top[0].Key)
					So(newTop[0].Score.BaseScore, ShouldEqual, top[0].Score.BaseScore)
				})
			})
		})
	}))

}
