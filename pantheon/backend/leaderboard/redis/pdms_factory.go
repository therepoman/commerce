package redis

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers/pubsub"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers/sns"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	"code.justin.tv/commerce/pantheon/clients/tenant"
)

type PDMSLeaderboardFactory interface {
	GetRedisLeaderboard(ctx context.Context, id identifier.Identifier, rs leaderboard.RetrievalSettings) (leaderboard.Leaderboard, error)
}

type PDMSFactory struct {
	TenantClient   tenant.Landlord       `inject:""`
	Persister      persistence.Persister `inject:""`
	PubsubObserver *pubsub.Observer      `inject:""`
	SNSObserver    *sns.Observer         `inject:""`
}

func (f PDMSFactory) GetRedisLeaderboard(ctx context.Context, id identifier.Identifier, rs leaderboard.RetrievalSettings) (leaderboard.Leaderboard, error) {
	exists, err := Exists(ctx, f.TenantClient.GetTenant(id.Domain).PDMSRedisClient, id, rs)
	if err != nil {
		return nil, err
	}

	t := f.TenantClient.GetTenant(id.Domain)
	lb := NewLeaderboard(t.PDMSRedisClient, t.StandardLockingClient, id)
	if !exists {
		if !rs.SkipImport {
			err := f.Persister.Import(ctx, id, lb)
			if err != nil {
				return nil, err
			}
		}
	}

	if f.PubsubObserver != nil {
		lb.AddObservers(f.PubsubObserver)
	}

	if f.SNSObserver != nil {
		lb.AddObservers(f.SNSObserver)
	}

	return lb, err
}
