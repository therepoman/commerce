package redis

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	observer_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	. "code.justin.tv/commerce/pantheon/test/redis"
	"code.justin.tv/commerce/pantheon/utils/math"
	"code.justin.tv/commerce/pantheon/utils/random"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"golang.org/x/sync/errgroup"
)

func TestRedisLeaderboard(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {

		Convey("Given a redis leaderboard", func() {

			id := test.RandomIdentifier()
			lb := NewLeaderboard(redisClient, lockingClient, id)
			observer := new(observer_mock.Observer)
			lb.AddObservers(observer)
			observer.On("Observe", mock.Anything)

			defer func() {
				_ = lb.ClearScores()
			}()

			Convey("Items can be added", func() {
				entry1 := entry.Entry{
					Key:   "entry1",
					Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.NotModerated),
				}

				entry2 := entry.Entry{
					Key:   "entry2",
					Score: score.ToEntryScore(score.BaseScore(2), score.EventTime(time.Now().UnixNano()), score.NotModerated),
				}

				entry3 := entry.Entry{
					Key:   "entry3",
					Score: score.ToEntryScore(score.BaseScore(3), score.EventTime(time.Now().UnixNano()), score.NotModerated),
				}

				err := lb.UpdateScore(entry1.Key, entry1.Score, nil)
				So(err, ShouldBeNil)

				err = lb.UpdateScore(entry2.Key, entry2.Score, nil)
				So(err, ShouldBeNil)

				err = lb.UpdateScore(entry3.Key, entry3.Score, nil)
				So(err, ShouldBeNil)

				Convey("Observer was called", func() {
					time.Sleep(time.Second)
					So(observer.AssertExpectations(t), ShouldBeTrue)
					So(len(observer.Calls), ShouldEqual, 3)
				})

				Convey("Count is 3", func() {
					count, err := lb.Len(leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					So(count, ShouldEqual, 3)
				})

				Convey("Highest Score is 3", func() {
					entries, err := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
					So(len(entries), ShouldEqual, 1)
					So(err, ShouldBeNil)
					So(entries[0].Score.BaseScore, ShouldEqual, 3)
				})

				Convey("Entry 3 has rank 1", func() {
					rank, err := lb.GetRank(entry3.Key, leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					So(rank, ShouldEqual, entry.Rank(1))
				})

				Convey("can get counts of entries that fall within a range", func() {
					Convey("when a min is not provided", func() {
						Convey("we should return the count of elements that fall in that range, defaulting min to 0", func() {
							var max int64 = 3
							count, err := lb.GetCountForRange(context.Background(), nil, &max, leaderboard.RetrievalSettings{})
							So(err, ShouldBeNil)
							So(count, ShouldEqual, 3)
						})
					})

					Convey("when a max is not provided", func() {
						Convey("we should return the count of elements that fall in that range, defaulting max to infinity", func() {
							var min int64 = 0
							count, err := lb.GetCountForRange(context.Background(), &min, nil, leaderboard.RetrievalSettings{})
							So(err, ShouldBeNil)
							So(count, ShouldEqual, 3)
						})
					})

					Convey("when both a min and max are provided", func() {
						Convey("we should return the count of elements that fall in that range", func() {
							var min int64 = 1
							var max int64 = 2
							count, err := lb.GetCountForRange(context.Background(), &min, &max, leaderboard.RetrievalSettings{})
							So(err, ShouldBeNil)
							So(count, ShouldEqual, 2)
						})
					})
				})

				Convey("can get entries above a particular threshold", func() {
					Convey("when the offset nets an index that is out of range, we should return 0 items", func() {
						entries, err := lb.GetEntriesAboveThreshold(0, 1, 3, leaderboard.RetrievalSettings{})
						So(entries, ShouldHaveLength, 0)
						So(err, ShouldBeNil)
					})

					Convey("when the threshold filters out all results, we should return 0 items", func() {
						entries, err := lb.GetEntriesAboveThreshold(4, 1, 0, leaderboard.RetrievalSettings{})
						So(entries, ShouldHaveLength, 0)
						So(err, ShouldBeNil)
					})

					Convey("we should return all results above a threshold", func() {
						entries, err := lb.GetEntriesAboveThreshold(0, 3, 0, leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(entries, ShouldHaveLength, 3)
					})

					Convey("we should apply a pagination offset", func() {
						entries, err := lb.GetEntriesAboveThreshold(0, 2, 1, leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(entries, ShouldHaveLength, 2)
					})

					Convey("we should apply a pagination limit", func() {
						entries, err := lb.GetEntriesAboveThreshold(0, 2, 0, leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(entries, ShouldHaveLength, 2)
					})

					Convey("we should apply an upper bound to pagination limit", func() {
						entries, err := lb.GetEntriesAboveThreshold(0, 100, 0, leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(entries, ShouldHaveLength, 3)
					})
				})

				Convey("Can get Entry's context", func() {
					e, _, err := lb.GetEntryContext(context.Background(), entry2.Key, 5, leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					Convey("Score is 2", func() {
						So(e.Score.BaseScore, ShouldEqual, entry2.Score.BaseScore)
					})
					Convey("Key is equivalent", func() {
						So(e.Key, ShouldEqual, entry2.Key)
					})
					Convey("Rank is 2", func() {
						So(e.Rank, ShouldEqual, entry.Rank(2))
					})
				})

				Convey("Errors when getting Entry's context with too short a timeout", func() {
					_, _, err := lb.GetEntryContextWithTimeoutAndRetry(context.Background(), entry2.Key, 5, leaderboard.RetrievalSettings{}, 1*time.Nanosecond, 2)
					So(err, ShouldNotBeNil)
				})

				Convey("Can get Entry's context with timeout and retry", func() {
					e, _, err := lb.GetEntryContextWithTimeoutAndRetry(context.Background(), entry2.Key, 5, leaderboard.RetrievalSettings{}, 100*time.Millisecond, 2)
					So(err, ShouldBeNil)
					Convey("Score is 2", func() {
						So(e.Score.BaseScore, ShouldEqual, entry2.Score.BaseScore)
					})
					Convey("Key is equivalent", func() {
						So(e.Key, ShouldEqual, entry2.Key)
					})
					Convey("Rank is 2", func() {
						So(e.Rank, ShouldEqual, entry.Rank(2))
					})
				})

				Convey("Scores can be updated", func() {
					err := lb.UpdateScore(entry2.Key, score.ToEntryScore(score.BaseScore(4), entry2.Score.EventTime, score.NotModerated), nil)
					So(err, ShouldBeNil)

					Convey("Count is 3", func() {
						count, err := lb.Len(leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(count, ShouldEqual, 3)
					})

					Convey("Highest Score is now 6", func() {
						entries, err := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
						So(len(entries), ShouldEqual, 1)
						So(err, ShouldBeNil)
						So(entries[0].Score.BaseScore, ShouldEqual, 6)
					})

					Convey("Errors when GetTop is called with too short a timeout", func() {
						_, err := lb.GetTopWithTimeoutAndRetry(context.Background(), 1, leaderboard.RetrievalSettings{}, 1*time.Nanosecond, 2)
						So(err, ShouldNotBeNil)
					})

					Convey("Can GetTop with timeout and retry", func() {
						entries, err := lb.GetTopWithTimeoutAndRetry(context.Background(), 1, leaderboard.RetrievalSettings{}, 100*time.Millisecond, 2)
						So(err, ShouldBeNil)
						So(len(entries), ShouldEqual, 1)
						So(err, ShouldBeNil)
						So(entries[0].Score.BaseScore, ShouldEqual, 6)
					})
				})

				Convey("Can Get Score from key", func() {
					actualScore, err := lb.GetScore(entry1.Key, leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					So(actualScore.DecodeScore().BaseScore, ShouldEqual, entry1.Score.BaseScore)
				})

				Convey("Score can be Incremented", func() {
					err = lb.UpdateScore(entry1.Key, score.ToEntryScore(10, entry1.Score.EventTime, score.NotModerated), nil)
					So(err, ShouldBeNil)
					Convey("Score is 11", func() {
						actualScore, _ := lb.GetScore(entry1.Key, leaderboard.RetrievalSettings{})
						So(actualScore.DecodeScore().BaseScore, ShouldEqual, score.BaseScore(11))
					})

				})

			})

			Convey("Can increment on empty leaderboard", func() {
				entry := test.RandomEntryWithScore(12)
				err := lb.UpdateScore(entry.Key, entry.Score, nil)
				So(err, ShouldBeNil)
				Convey("Has count of 1", func() {
					len, err := lb.Len(leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					So(len, ShouldEqual, 1)
				})
			})

			Convey("Passes GetEntryContextTest", func() {
				test.GetEntryContextTest(lb)
			})
		})
	}))
}

func TestRedisSerialization(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {

		if !usingLocal {
			t.Skip("Skipping serialization test. Pass the test arg redis.local to run this")
		}

		id := test.RandomIdentifier()
		Convey("Given a redis leaderboard", func() {
			lb := NewLeaderboard(redisClient, lockingClient, id)
			defer func() {
				_ = lb.ClearScores()
			}()

			test.SerializationRoundTripTest(lb)
		})
	}))
}

func TestRedisTimeBreaksTies(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {

		Convey("Given a redis leaderboard", func() {
			id := test.RandomIdentifier()
			lb := NewLeaderboard(redisClient, lockingClient, id)

			defer func() {
				_ = lb.ClearScores()
			}()

			test.TimeBreaksTiesTest(lb)
		})
	}))
}

func validateEventTimesInAcceptableTimeDelta(et1 score.EventTime, et2 score.EventTime, maxAllowedTimeDelta time.Duration) {
	timeDelta := time.Duration(math.MaxInt64(int64(et1), int64(et2)) - math.MinInt64(int64(et1), int64(et2)))
	So(timeDelta, ShouldBeLessThanOrEqualTo, maxAllowedTimeDelta)
}

func TestRedisManyUpdatesInParallel(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		if !usingLocal {
			t.Skip("Skipping factory test. Pass the test arg redis.local to run this")
		}
		Convey("Given a redis leaderboard", func() {
			id := test.RandomIdentifier()
			lb := NewLeaderboard(redisClient, lockingClient, id)

			defer func() {
				_ = lb.ClearScores()
			}()

			numEvents := 200
			total := int64(0)
			key := random.String(16)
			latestEventTime := time.Date(2014, time.January, 1, 0, 0, 0, 0, time.UTC)

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			errg, _ := errgroup.WithContext(ctx)
			Convey("Update the leaderboard quickly in parallel", func() {
				for i := 0; i < numEvents; i++ {
					nextBaseScore := random.Int64(1, 1000)
					nextEventTime := random.Time(365 * 24 * time.Hour)

					k := entry.Key(key)
					bs := score.BaseScore(nextBaseScore)
					et := score.EventTime(nextEventTime.UnixNano())
					errg.Go(func() error {
						return lb.UpdateScore(k, score.ToEntryScore(bs, et, score.NotModerated), nil)
					})

					total += nextBaseScore
					if nextEventTime.After(latestEventTime) {
						latestEventTime = nextEventTime
					}
					time.Sleep(random.Duration(100*time.Microsecond, time.Millisecond))
				}

				Convey("Wait for all updates to finish without errors", func() {
					err := errg.Wait()
					So(err, ShouldBeNil)

					Convey("Validate the final leaderboard state", func() {
						entries, err := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(entries[0].Score.BaseScore, ShouldEqual, total)
						validateEventTimesInAcceptableTimeDelta(entries[0].Score.EventTime, score.EventTime(latestEventTime.UnixNano()), time.Second)
					})
				})
			})

		})
	}))
}

// 1. Moderate an entry that is not in the leaderboard
// 2. Undmoderate the same entry
// 3. Publish an event to ingest data for that entry
// 4. Validate that the entry appears correctly in the leaderboard
func TestModeraterUnmoderatePublish(t *testing.T) {
	Convey("Given a redis client", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		if !usingLocal {
			t.Skip("Skipping factory test. Pass the test arg redis.local to run this")
		}

		Convey("Given a redis leaderboard", func() {
			id := test.RandomIdentifier()
			lb := NewLeaderboard(redisClient, lockingClient, id)
			entryKey := entry.Key(random.String(16))

			Convey("Moderate the entry", func() {
				err := lb.ModerateEntry(entryKey)
				So(err, ShouldBeNil)

				Convey("Unmoderate the entry", func() {
					err = lb.UnmoderateEntry(entryKey)
					So(err, ShouldBeNil)

					Convey("Publish an event", func() {
						baseScore := score.BaseScore(random.Int64(1, 1000))
						eventTime := score.EventTime(random.Time(24 * time.Hour).UnixNano())
						err := lb.UpdateScore(entryKey, score.ToEntryScore(baseScore, eventTime, score.NotModerated), nil)

						So(err, ShouldBeNil)

						Convey("Validate the top elements in the LB", func() {
							top, err := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
							So(err, ShouldBeNil)
							So(len(top), ShouldEqual, 1)
							topEntry := top[0]
							So(topEntry.Score.BaseScore, ShouldEqual, baseScore)
							So(topEntry.Score.ModeratedFlag, ShouldEqual, score.NotModerated)
						})
					})
				})
			})
		})
	}))
}
