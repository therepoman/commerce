package redis

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers/pubsub"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers/sns"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/middleware"
)

type LeaderboardFactory interface {
	IsInRedis(ctx context.Context, id identifier.Identifier, rs leaderboard.RetrievalSettings) (bool, error)
	GetRedisLeaderboard(ctx context.Context, id identifier.Identifier, rs leaderboard.RetrievalSettings) (leaderboard.Leaderboard, error)
}

type Factory struct {
	TenantClient   tenant.Landlord       `inject:""`
	Persister      persistence.Persister `inject:""`
	PubsubObserver *pubsub.Observer      `inject:""`
	SNSObserver    *sns.Observer         `inject:""`
}

func (f Factory) IsInRedis(ctx context.Context, id identifier.Identifier, rs leaderboard.RetrievalSettings) (bool, error) {
	exists, err := Exists(ctx, f.TenantClient.GetTenant(id.Domain).RedisClient, id, rs)
	if err != nil {
		return false, err
	}
	return exists, nil
}

func (f Factory) GetRedisLeaderboard(ctx context.Context, id identifier.Identifier, rs leaderboard.RetrievalSettings) (leaderboard.Leaderboard, error) {
	exists, err := Exists(ctx, f.TenantClient.GetTenant(id.Domain).RedisClient, id, rs)
	if err != nil {
		return nil, err
	}

	t := f.TenantClient.GetTenant(id.Domain)
	lb := NewLeaderboard(t.RedisClient, t.StandardLockingClient, id)
	if !exists {
		logRequestForNotFoundLB(ctx, id)

		if !rs.SkipImport {
			err := f.Persister.Import(ctx, id, lb)
			if err != nil {
				return nil, err
			}
		}
	}

	if f.PubsubObserver != nil {
		lb.AddObservers(f.PubsubObserver)
	}

	if f.SNSObserver != nil {
		lb.AddObservers(f.SNSObserver)
	}

	return lb, err
}

func logRequestForNotFoundLB(ctx context.Context, id identifier.Identifier) {
	var clientID, ipAddress, requestingUserID string

	if id, ok := middleware.ClientID(ctx); ok {
		clientID = id
	}

	if address, ok := middleware.IPAddress(ctx); ok {
		ipAddress = address
	}

	if userID, ok := middleware.UserID(ctx); ok {
		requestingUserID = userID
	}

	logrus.WithFields(logrus.Fields{
		"domain":                id.Domain,
		"grouping_key":          id.GroupingKey,
		"time_bucket":           id.TimeBucket,
		"time_aggregation_unit": id.TimeAggregationUnit,
		"client_id":             clientID,
		"ip_address":            ipAddress,
		"requesting_user_id":    requestingUserID,
	}).Info("requested leaderboard that does not exist in redis")
}
