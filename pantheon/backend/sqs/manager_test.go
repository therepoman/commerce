package sqs_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/sqs"
	sqs_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/sqs"
	sqsiface_mock "code.justin.tv/commerce/pantheon/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/aws/aws-sdk-go/aws"
	awsSQS "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSQSManager(t *testing.T) {
	Convey("Given all the SQS worker manager dependencies", t, func() {
		stats, _ := statsd.NewNoopClient()
		mockWorker := new(sqs_mock.Worker)
		mockSqs := new(sqsiface_mock.SQSAPI)

		getQueueUrlOutput := awsSQS.GetQueueUrlOutput{
			QueueUrl: aws.String("testUrl"),
		}
		mockSqs.On("GetQueueUrl", mock.Anything).Return(&getQueueUrlOutput, nil)

		getQueueAttributesOutput := awsSQS.GetQueueAttributesOutput{
			Attributes: map[string]*string{
				"MessageRetentionPeriod": aws.String("3"),
				"VisibilityTimeout":      aws.String("3"),
			},
		}
		mockSqs.On("GetQueueAttributes", mock.Anything).Return(&getQueueAttributesOutput, nil)

		mockSqs.On("ReceiveMessageWithContext", mock.Anything, mock.Anything).Return(&awsSQS.ReceiveMessageOutput{}, nil)

		Convey("Can create a new SQS worker manager", func() {
			manager := sqs.NewSQSWorkerManager("queuename", 1, mockSqs, mockWorker, stats)
			So(manager, ShouldNotBeNil)

			Convey("Worker manager can be shutdown", func() {
				err := manager.Shutdown(context.Background())
				So(err, ShouldBeNil)
			})
		})
	})
}
