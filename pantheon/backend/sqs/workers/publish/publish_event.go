package publish

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/ingestion"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/aws/aws-sdk-go/service/sqs"
	log "github.com/sirupsen/logrus"
	"github.com/uber-go/tally"
)

const timeoutTime = 5 * time.Second

type EventWorker struct {
	Ingester            ingestion.Ingester   `inject:""`
	DomainConfigManager domain.ConfigManager `inject:""`
	RootMetricsScope    tally.Scope          `inject:""`
	TenantClient        tenant.Landlord      `inject:""`
}

const (
	ingestUnknownDomainMetric = "ingest-unknown-domain"
	ingestTimeMetric          = "ingest-time"
)

func getMetricTags(d identifier.Domain) map[string]string {
	metricTags := map[string]string{
		"queue":  "publish-events",
		"domain": string(d),
	}
	return metricTags
}

func (w *EventWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutTime)
	defer cancel()

	if msg == nil || msg.Body == nil {
		return errors.New("received nil SQS message")
	}

	e, err := event.FromJSON(*msg.Body)
	if err != nil {
		return err
	}

	if e == nil {
		return errors.New("received nil event")
	}

	eventDomain := identifier.Domain(e.Domain)
	domainConfig := w.DomainConfigManager.Get(eventDomain)

	if !domainConfig.IsKnownDomain() {
		w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Counter(ingestUnknownDomainMetric).Inc(1)
		log.WithField("domain", string(e.Domain)).Warn("received ingest event for unknown domain")
	}

	metricStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer(ingestTimeMetric).Start()
	defer metricStopwatch.Stop()

	ingestLock, err := w.TenantClient.GetTenant(eventDomain).StandardLockingClient.ObtainLock(getLockKey(e))
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"domain":       e.Domain,
			"grouping_key": e.GroupingKey,
			"event_id":     e.ID,
		}).Error("error obtaining an ingestion lock for the event")
		return errors.New("error obtaining an ingestion lock for the event")
	}

	defer func() {
		unlockErr := ingestLock.Unlock()
		if unlockErr != nil {
			log.WithError(unlockErr).Error("error unlocking ingestion lock")
		}
	}()

	return w.Ingester.Ingest(ctx, e)
}

func getLockKey(e *event.Event) string {
	return fmt.Sprintf("%s.%s.%s", e.Domain, e.GroupingKey, e.ID)
}
