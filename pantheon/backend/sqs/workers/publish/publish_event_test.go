package publish

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	ingestion_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/ingestion"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/uber-go/tally"
)

func TestPublishEventWorker_Handle(t *testing.T) {
	Convey("Given a publish event worker", t, func() {
		mockedIngester := new(ingestion_mock.Ingester)
		mockedDomainConfigManager := new(domain_mock.ConfigManager)
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			StandardLockingClient: lock.NewNoOpLockingClient(),
		})
		worker := EventWorker{
			Ingester:            mockedIngester,
			RootMetricsScope:    tally.NoopScope,
			DomainConfigManager: mockedDomainConfigManager,
			TenantClient:        mockTenantClient,
		}

		Convey("Nil message errors", func() {
			resp := worker.Handle(nil)
			So(resp, ShouldNotBeNil)
		})

		Convey("Nil message body errors", func() {
			resp := worker.Handle(&sqs.Message{
				Body: nil,
			})
			So(resp, ShouldNotBeNil)
		})

		Convey("Bad event JSON errors", func() {
			badJSON := "}badJson{"
			resp := worker.Handle(&sqs.Message{
				Body: &badJSON,
			})
			So(resp, ShouldNotBeNil)
		})

		Convey("Given valid event JSON SQS message", func() {
			validEventJSON := "{}"
			msg := &sqs.Message{Body: &validEventJSON}

			Convey("When domain config manager returns an unknown domain config", func() {
				mockedDomainConfigManager.On("Get", mock.Anything).Return(domain.UnknownDomainConfig())

				Convey("Errors when ingester errors", func() {
					mockedIngester.On("Ingest", mock.Anything, mock.Anything).Return(errors.New("test error"))
					resp := worker.Handle(msg)
					So(resp, ShouldNotBeNil)
				})

				Convey("Succeeds when ingester succeeds", func() {
					mockedIngester.On("Ingest", mock.Anything, mock.Anything).Return(nil)
					resp := worker.Handle(msg)
					So(resp, ShouldBeNil)
				})
			})
		})
	})
}
