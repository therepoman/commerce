package moderate

import (
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/timeunit"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	moderation_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	redis_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/uber-go/tally"
)

func testModerateMessageJSON(moderationAction string) string {
	m := moderation.Moderation{
		Domain:           "test-domain",
		GroupingKey:      "test-grouping-key",
		EntryKey:         "test-entry",
		ModerationAction: moderationAction,
		TimeOfModeration: time.Now(),
	}
	json, err := moderation.ToJSON(m)
	So(err, ShouldBeNil)
	return json
}

func TestModerateEntryWorker_Handle(t *testing.T) {
	Convey("Given a moderate entry worker", t, func() {
		mockFactory := new(redis_mock.LeaderboardFactory)
		mockModerationDAO := new(moderation_mock.DAO)
		mockedDomainConfigManager := new(domain_mock.ConfigManager)
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			ModerationDAO: mockModerationDAO,
		})
		fakeDynamicConfig := config.NewDynamicConfigFake()
		worker := Worker{
			LeaderboardFactory:  mockFactory,
			RootMetricsScope:    tally.NoopScope,
			DomainConfigManager: mockedDomainConfigManager,
			TenantClient:        mockTenantClient,
			DynamicConfig:       fakeDynamicConfig,
		}

		Convey("Nil message errors", func() {
			resp := worker.Handle(nil)
			So(resp, ShouldNotBeNil)
		})

		Convey("Nil message body errors", func() {
			resp := worker.Handle(&sqs.Message{
				Body: nil,
			})
			So(resp, ShouldNotBeNil)
		})

		Convey("Bad moderation JSON errors", func() {
			badJSON := "}badJson{"
			resp := worker.Handle(&sqs.Message{
				Body: &badJSON,
			})
			So(resp, ShouldNotBeNil)
		})

		Convey("Given valid moderation JSON SQS message", func() {
			validModerationJSON := testModerateMessageJSON("MODERATE")
			msg := &sqs.Message{Body: &validModerationJSON}

			Convey("When domain config manager returns an unknown domain config", func() {
				mockedDomainConfigManager.On("Get", mock.Anything).Return(domain.UnknownDomainConfig())

				Convey("Errors when dynamo update errors", func() {
					mockModerationDAO.On("UpdateModeration", mock.Anything).Return(errors.New("test error"))
					resp := worker.Handle(msg)
					So(resp, ShouldNotBeNil)
				})

				Convey("When dynamo update succeeds", func() {
					mockModerationDAO.On("UpdateModeration", mock.Anything).Return(nil)

					Convey("Errors when leaderboard factory get errors", func() {
						mockFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
						resp := worker.Handle(msg)
						So(resp, ShouldNotBeNil)
					})

					Convey("When leaderboard factory get succeeds", func() {
						mockLB := new(leaderboard_mock.Leaderboard)
						mockFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLB, nil)

						Convey("Errors when message contained an invalid moderation action", func() {
							validModerationJSON := testModerateMessageJSON("INVALID_ACTION")
							msg = &sqs.Message{Body: &validModerationJSON}
							resp := worker.Handle(msg)
							So(resp, ShouldNotBeNil)
						})

						Convey("When message contained a moderate action", func() {
							validModerationJSON := testModerateMessageJSON("MODERATE")
							msg = &sqs.Message{Body: &validModerationJSON}

							Convey("When leaderboard moderation errors", func() {
								mockLB.On("ModerateEntry", mock.Anything).Return(errors.New("test error"))
								resp := worker.Handle(msg)
								So(resp, ShouldNotBeNil)
							})

							Convey("Succeeds when leaderboard moderation succeeds", func() {
								mockLB.On("ModerateEntry", mock.Anything).Return(nil)
								resp := worker.Handle(msg)
								So(resp, ShouldBeNil)
								mockFactory.AssertNumberOfCalls(t, "GetRedisLeaderboard", len(timeunit.GetTimeUnits()))
							})
						})

						Convey("When message contained an unmoderate action", func() {
							validModerationJSON := testModerateMessageJSON("UNMODERATE")
							msg = &sqs.Message{Body: &validModerationJSON}

							Convey("When leaderboard unmoderation errors", func() {
								mockLB.On("UnmoderateEntry", mock.Anything).Return(errors.New("test error"))
								resp := worker.Handle(msg)
								So(resp, ShouldNotBeNil)
							})

							Convey("Succeeds when leaderboard unmoderation succeeds", func() {
								mockLB.On("UnmoderateEntry", mock.Anything).Return(nil)
								resp := worker.Handle(msg)
								So(resp, ShouldBeNil)
								mockFactory.AssertNumberOfCalls(t, "GetRedisLeaderboard", len(timeunit.GetTimeUnits()))
							})
						})
					})
				})
			})
		})
	})
}
