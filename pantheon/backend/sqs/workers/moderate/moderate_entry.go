package moderate

import (
	"context"
	"errors"
	"time"

	dynamo_moderation "code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/timeunit"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sqs"
	log "github.com/sirupsen/logrus"
	"github.com/uber-go/tally"
	"golang.org/x/time/rate"
)

const (
	timeoutTime                   = 30 * time.Second
	moderationUnknownDomainMetric = "moderation-unknown-domain"
	moderationTimeMetric          = "moderation-time"
)

var rateLimiter *rate.Limiter

type Worker struct {
	LeaderboardFactory  redis.LeaderboardFactory   `inject:""`
	DomainConfigManager domain.ConfigManager       `inject:""`
	RootMetricsScope    tally.Scope                `inject:""`
	TenantClient        tenant.Landlord            `inject:""`
	DynamicConfig       config.DynamicConfigClient `inject:""`
}

func getMetricTags(d identifier.Domain) map[string]string {
	metricTags := map[string]string{
		"queue":  "moderation-processing",
		"domain": string(d),
	}
	return metricTags
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutTime)
	defer cancel()

	rateLimitFromConfig := w.DynamicConfig.GetModerationQueueRateLimit()
	// Either instantiate the rate limiter, or make a new one if the dynamic config value has changed
	if rateLimiter == nil || rateLimitFromConfig != int(rateLimiter.Limit()) {
		rateLimiter = rate.NewLimiter(rate.Limit(rateLimitFromConfig), 1)
	}

	err := rateLimiter.Wait(ctx)
	if err != nil {
		return errors.New("could not rate limit ModerateEntry worker")
	}

	if msg == nil || msg.Body == nil {
		return errors.New("received nil SQS message")
	}

	m, err := moderation.FromJSON(*msg.Body)
	if err != nil {
		return err
	}

	moderationDomain := identifier.Domain(m.Domain)
	domainConfig := w.DomainConfigManager.Get(moderationDomain)

	if !domainConfig.IsKnownDomain() {
		w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Counter(moderationUnknownDomainMetric).Inc(1)
		log.WithField("domain", string(moderationDomain)).Warn("received ingest event for unknown domain")
	}

	metricStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer(moderationTimeMetric).Start()
	defer metricStopwatch.Stop()

	err = w.TenantClient.GetTenant(moderationDomain).ModerationDAO.UpdateModeration(&dynamo_moderation.Moderation{
		LeaderboardSetID: m.ToLeaderboardSetID(),
		EntryKey:         m.EntryKey,
		State:            string(m.ModerationAction),
		TimeOfModeration: m.TimeOfModeration,
		LastUpdated:      time.Now(),
	})
	if err != nil {
		if _, ok := err.(*dynamodb.ConditionalCheckFailedException); ok {
			// conditional check failing indicates out of order SQS messages. Not an error, but we don't want to write to redis either.
			log.Warnf("Received out of order moderation event %+v", m)
			return nil
		}
		return err
	}

	for _, tu := range timeunit.GetTimeUnits() {
		id := identifier.Identifier{
			Domain:              identifier.Domain(m.Domain),
			GroupingKey:         identifier.GroupingKey(m.GroupingKey),
			TimeAggregationUnit: tu,
			TimeBucket:          bucket.TimestampToTimeBucket(m.TimeOfModeration, tu),
		}

		lb, err := w.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{ConsistentRead: true})
		if err != nil {
			return err
		}

		if m.ModerationAction == pantheonrpc.ModerationAction_name[int32(pantheonrpc.ModerationAction_MODERATE)] {
			err = lb.ModerateEntry(entry.Key(m.EntryKey))
		} else if m.ModerationAction == pantheonrpc.ModerationAction_name[int32(pantheonrpc.ModerationAction_UNMODERATE)] {
			err = lb.UnmoderateEntry(entry.Key(m.EntryKey))
		} else {
			err = errors.New("received unsupported moderation action")
		}
		if err != nil {
			return err
		}
	}

	return nil
}
