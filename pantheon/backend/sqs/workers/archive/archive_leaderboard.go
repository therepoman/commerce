package archive

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/archival"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/ingestion"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local/adapter"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/aws/aws-sdk-go/service/sqs"
	log "github.com/sirupsen/logrus"
	"github.com/uber-go/tally"
)

const timeoutTime = 30 * time.Second

type Worker struct {
	LeaderboardFactory      redis.LeaderboardFactory      `inject:""`
	IndexFactory            redis.GroupingKeyIndexFactory `inject:""`
	Ingester                ingestion.Ingester            `inject:""`
	Persister               persistence.Persister         `inject:""`
	LocalLeaderboardAdapter adapter.LeaderboardAdapter    `inject:""`
	TenantClient            tenant.Landlord               `inject:""`
	DomainConfigManager     domain.ConfigManager          `inject:""`
	RootMetricsScope        tally.Scope                   `inject:""`
}

const (
	archiveUnknownDomainMetric = "archive-unknown-domain"
	archiveTimeMetric          = "archive-time"
)

func getMetricTags(d identifier.Domain) map[string]string {
	metricTags := map[string]string{
		"queue":  "archival-processing",
		"domain": string(d),
	}
	return metricTags
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutTime)
	defer cancel()

	if msg == nil || msg.Body == nil {
		return errors.New("received nil SQS message")
	}

	arc, err := archival.FromJSON(*msg.Body)
	if err != nil {
		return err
	}

	id, err := identifier.FromKey(arc.Key)
	if err != nil {
		return err
	}

	archiveDomain := id.Domain
	domainConfig := w.DomainConfigManager.Get(archiveDomain)

	if !domainConfig.IsKnownDomain() {
		w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Counter(archiveUnknownDomainMetric).Inc(1)
		log.WithField("domain", string(archiveDomain)).Warn("received archive event for unknown domain")
	}

	metricStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer(archiveTimeMetric).Start()
	defer metricStopwatch.Stop()

	obtainLockStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer("archive-step_obtain-lock").Start()
	archivalLock, err := w.TenantClient.GetTenant(id.Domain).StandardLockingClient.ObtainLock(fmt.Sprintf("%s-archival-lock", id.Key()))
	obtainLockStopwatch.Stop()
	if err != nil {
		return err
	}

	defer func() {
		err = archivalLock.Unlock()
		if err != nil {
			log.WithError(err).Error("Archival unlock failed")
		}
	}()

	isInRedisMetricStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer("archive-step_is-in-redis").Start()
	inRedis, err := w.LeaderboardFactory.IsInRedis(ctx, id, leaderboard.RetrievalSettings{ConsistentRead: true})
	isInRedisMetricStopwatch.Stop()
	if err != nil {
		return err
	}

	if !inRedis {
		return nil
	}

	getLeaderboardStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer("archive-step_get-leaderboard").Start()
	lb, err := w.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{ConsistentRead: true})
	getLeaderboardStopwatch.Stop()
	if err != nil {
		return err
	}

	if domainConfig.ArchiveRedisLeaderboards {
		archiveRedisLeaderboardStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer("archive-step_archive-redis-leaderboard").Start()
		err = w.Persister.Export(ctx, id, lb, leaderboard.RetrievalSettings{ConsistentRead: true})
		archiveRedisLeaderboardStopwatch.Stop()
		if err != nil {
			return err
		}
	}

	deleteGroupingKeyIndexMemberStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer("archive-step_delete-grouping-key-index-member").Start()
	groupingKeyIndex := w.IndexFactory.GetRedisGroupingKeyIndex(id.Domain, id.GroupingKey)
	err = groupingKeyIndex.DeleteMember(ctx, id)
	if err != nil {
		return err
	}
	deleteGroupingKeyIndexMemberStopwatch.Stop()

	destroyLeaderboardStopwatch := w.RootMetricsScope.Tagged(getMetricTags(domainConfig.Domain)).Timer("archive-step_destroy-leaderboard").Start()
	err = lb.Destroy()
	destroyLeaderboardStopwatch.Stop()

	return err
}
