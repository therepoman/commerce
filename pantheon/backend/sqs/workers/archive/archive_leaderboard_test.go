package archive

import (
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/archival"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	index_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/index"
	ingestion_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/ingestion"
	adapter_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/local/adapter"
	persistence_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	redis_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/uber-go/tally"
)

func archivalMessageJSON(key string) string {
	am := archival.Message{
		Key: key,
	}
	json, err := archival.ToJSON(am)
	So(err, ShouldBeNil)
	return json
}

func TestArchiveLeaderboardWorker_Handle(t *testing.T) {
	Convey("Given an archive leaderboard worker", t, func() {
		mockedIngester := new(ingestion_mock.Ingester)
		mockedPersister := new(persistence_mock.Persister)
		mockedFactory := new(redis_mock.LeaderboardFactory)
		mockedLeaderboardAdapter := new(adapter_mock.LeaderboardAdapter)
		mockedDomainConfigManager := new(domain_mock.ConfigManager)
		lockingClient := lock.NewNoOpLockingClient()
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			StandardLockingClient: lockingClient,
		})
		groupingKeyIndexFactory := new(redis_mock.GroupingKeyIndexFactory)
		groupingKeyIndex := new(index_mock.Index)
		groupingKeyIndexFactory.On("GetRedisGroupingKeyIndex", mock.Anything, mock.Anything).Return(groupingKeyIndex)

		alw := &Worker{
			LeaderboardFactory:      mockedFactory,
			Ingester:                mockedIngester,
			Persister:               mockedPersister,
			LocalLeaderboardAdapter: mockedLeaderboardAdapter,
			DomainConfigManager:     mockedDomainConfigManager,
			TenantClient:            mockTenantClient,
			RootMetricsScope:        tally.NoopScope,
			IndexFactory:            groupingKeyIndexFactory,
		}

		Convey("Errors when the SQS message is nil ", func() {
			So(alw.Handle(nil), ShouldNotBeNil)
		})

		Convey("Errors when the SQS message body is nil", func() {
			So(alw.Handle(&sqs.Message{Body: nil}), ShouldNotBeNil)
		})

		Convey("Errors when the SQS message body is invalid json", func() {
			So(alw.Handle(&sqs.Message{Body: aws.String("not valid json")}), ShouldNotBeNil)
		})

		Convey("Errors when archival message contains an improper key", func() {
			So(alw.Handle(&sqs.Message{Body: aws.String(archivalMessageJSON("not a valid key"))}), ShouldNotBeNil)
		})

		Convey("Given a valid SQS message", func() {
			id := identifier.Identifier{
				Domain:              identifier.Domain("test-domain"),
				GroupingKey:         identifier.GroupingKey("test-grouping-key"),
				TimeAggregationUnit: pantheonrpc.TimeUnit_ALLTIME,
				TimeBucket:          bucket.TimestampToTimeBucket(time.Now(), pantheonrpc.TimeUnit_ALLTIME),
			}
			sqsMsg := &sqs.Message{Body: aws.String(archivalMessageJSON(id.Key()))}

			Convey("When domain config manager returns an unknown domain config", func() {
				mockedDomainConfigManager.On("Get", mock.Anything).Return(domain.UnknownDomainConfig())

				Convey("Errors when leaderboard factory errors checking if leaderboard exists", func() {
					mockedFactory.On("IsInRedis", mock.Anything, mock.Anything, mock.Anything).Return(false, errors.New("error")).Once()
					So(alw.Handle(sqsMsg), ShouldNotBeNil)
				})

				Convey("Succeeds early when leaderboard factory says leaderboard does not exist", func() {
					mockedFactory.On("IsInRedis", mock.Anything, mock.Anything, mock.Anything).Return(false, nil).Once()
					So(alw.Handle(sqsMsg), ShouldBeNil)
					So(len(mockedFactory.Calls), ShouldEqual, 1)
				})

				Convey("When leaderboard factory says leaderboard exists", func() {
					mockedFactory.On("IsInRedis", mock.Anything, mock.Anything, mock.Anything).Return(true, nil).Once()

					Convey("Errors when leaderboard factory errors getting leaderboard", func() {
						mockedFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error")).Once()
						So(alw.Handle(sqsMsg), ShouldNotBeNil)
					})

					Convey("When leaderboard factory returns a leaderboard", func() {
						mockLeaderboard1 := new(leaderboard_mock.Leaderboard)
						mockedFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard1, nil).Once()

						Convey("Errors when lb1 persister errors", func() {
							mockedPersister.On("Export", mock.Anything, mock.Anything, mockLeaderboard1, mock.Anything).Return(errors.New("error")).Once()
							So(alw.Handle(sqsMsg), ShouldNotBeNil)
						})

						Convey("When lb1 persister succeeds", func() {
							mockedPersister.On("Export", mock.Anything, mock.Anything, mockLeaderboard1, mock.Anything).Return(nil).Once()

							Convey("Errors when grouping key index member deletion fails", func() {
								groupingKeyIndex.On("DeleteMember", mock.Anything, id).Return(errors.New("Failed to delete member from grouping key index"))
								So(alw.Handle(sqsMsg), ShouldNotBeNil)
							})

							Convey("When grouping key index member deletion succeeds", func() {
								groupingKeyIndex.On("DeleteMember", mock.Anything, id).Return(nil)

								Convey("When lb adapter convert succeeds", func() {
									mockLeaderboard1.On("Destroy", mock.Anything).Return(nil).Once()
									So(alw.Handle(sqsMsg), ShouldBeNil)

									mockedFactory.AssertCalled(t, "GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything)
									mockedPersister.AssertCalled(t, "Export", mock.Anything, mock.Anything, mockLeaderboard1, mock.Anything)
									mockLeaderboard1.AssertCalled(t, "Destroy", mock.Anything)
								})
							})
						})
					})
				})
			})
		})
	})
}
