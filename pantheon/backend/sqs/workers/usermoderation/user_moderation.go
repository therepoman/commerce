package usermoderation

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/sirupsen/logrus"
	"github.com/uber-go/tally"
)

const (
	timeoutTime                       = 30 * time.Second
	minTimeoutModerationEventToHandle = 1 * time.Minute
)

type Worker struct {
	DomainConfigManager domain.ConfigManager `inject:""`
	RootMetricsScope    tally.Scope          `inject:""`
	ModerateEntryAPI    api.ModerateEntryAPI `inject:"moderate_entry_api"`
}

type moderationEvent struct {
	groupingKey      string
	entryKey         string
	moderationAction pantheonrpc.ModerationAction
	eventTime        time.Time
	isTempBan        bool
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutTime)
	defer cancel()

	if msg == nil || msg.Body == nil {
		return errors.New("received nil SQS message")
	}

	snsMsg, err := sns.Extract(msg)
	if err != nil {
		message := "unable to extract snsMessage in userModeration handler"
		logrus.WithError(err).Error(message)
		return err
	}

	log := logrus.WithField("message", snsMsg)

	// The user moderation SNS topic can send two different structs (Ban and Unban)
	normalizedEvent, err := normalizeModerationEvent(snsMsg)
	if err != nil {
		message := "unable to normalize moderation event"
		log.WithError(err).Error(message)
		return err
	}
	if normalizedEvent == nil {
		return nil
	}

	// Fan out user moderation events to all domains that have opted in.
	for domainName, config := range w.DomainConfigManager.GetAll() {
		if !config.EnableUserModerationHandling {
			continue
		}

		if normalizedEvent.isTempBan && !config.EnableTemporarySuspensionHandling {
			continue
		}

		_, err := w.ModerateEntryAPI.ModerateEntry(ctx, &pantheonrpc.ModerateEntryReq{
			Domain:           string(domainName),
			GroupingKey:      normalizedEvent.groupingKey,
			EntryKey:         normalizedEvent.entryKey,
			ModerationAction: normalizedEvent.moderationAction,
			TimeOfEvent:      uint64(normalizedEvent.eventTime.UnixNano()),
		})
		if err != nil {
			message := "error calling ModerateEntry in user moderation handler"
			log.WithError(err).Error(message)
			return errors.New(message)
		}
	}

	return nil
}

func normalizeModerationEvent(snsMsg sns.Message) (*moderationEvent, error) {
	eventAttribute, ok := snsMsg.MessageAttributes["event"]
	if !ok {
		return nil, errors.New("sns message is missing event attribute")
	}

	var normalizedEvent *moderationEvent
	switch eventAttribute.Value {
	case tmi.ChatBanEventName:
		event, err := parseBanEvent(snsMsg)
		if err != nil {
			return normalizedEvent, err
		}

		// Regardless of whether the domain is configured to listen for temporary suspensions, we discard anything shorter
		// than 1 minute.
		if event.ExpiresAt != nil && event.ExpiresAt.After(event.BannedAt) && event.ExpiresAt.Sub(event.BannedAt) <= minTimeoutModerationEventToHandle {
			logrus.WithField("duration_seconds", event.ExpiresAt.Sub(event.BannedAt).Seconds()).Info("encountered a very short duration timeout, ignoring")
			return nil, nil
		}

		normalizedEvent = &moderationEvent{
			groupingKey:      event.ChannelID,
			entryKey:         event.TargetID,
			eventTime:        event.BannedAt,
			moderationAction: pantheonrpc.ModerationAction_MODERATE,
			isTempBan:        event.ExpiresAt != nil,
		}
	case tmi.ChatUnbanEventName:
		event, err := parseUnbanEvent(snsMsg)
		if err != nil {
			return nil, err
		}

		normalizedEvent = &moderationEvent{
			groupingKey:      event.ChannelID,
			entryKey:         event.TargetID,
			eventTime:        event.UnbannedAt,
			moderationAction: pantheonrpc.ModerationAction_UNMODERATE,
		}
	default:
		logrus.WithField("message", snsMsg).Warn("encountered unknown userModeration event type")
		return nil, nil
	}

	return normalizedEvent, nil
}

func parseBanEvent(snsMsg sns.Message) (tmi.ChatBanEventPayload, error) {
	var chatBan tmi.ChatBanEventPayload
	err := json.Unmarshal([]byte(snsMsg.Message), &chatBan)
	if err != nil {
		message := "unable to parse ChatBanEventPayload msg struct in UserModeration handler"
		logrus.WithError(err).Error(message)
		return chatBan, errors.New(message)
	}

	return chatBan, nil
}

func parseUnbanEvent(snsMsg sns.Message) (tmi.ChatUnbanEventPayload, error) {
	var chatUnban tmi.ChatUnbanEventPayload
	err := json.Unmarshal([]byte(snsMsg.Message), &chatUnban)
	if err != nil {
		message := "unable to parse ChatUnbanEventPayload msg struct in UserModeration handler"
		logrus.WithError(err).Error(message)
		return chatUnban, errors.New(message)
	}

	return chatUnban, nil
}
