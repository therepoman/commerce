package usermoderation

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	api_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserModerationWorker_Handle(t *testing.T) {
	Convey("Given a user moderation worker", t, func() {
		mockedDomainConfigManager := new(domain_mock.ConfigManager)
		mockModerateEntryAPI := new(api_mock.ModerateEntryAPI)

		worker := Worker{
			DomainConfigManager: mockedDomainConfigManager,
			ModerateEntryAPI:    mockModerateEntryAPI,
		}

		Convey("Given a nil message, we return an error", func() {
			err := worker.Handle(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Given a nil message body, we return an error", func() {
			err := worker.Handle(&sqs.Message{
				Body: nil,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a message with an unhandled event type, we should return nil", func() {
			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "foobar"
					}
				}
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
		})

		Convey("Given a message that is only for a temp timeout of one minute we should return nil", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_ban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\",\"banned_at\":\"2020-11-12T23:18:00+00:00\",\"expires_at\":\"2020-11-12T23:19:00+00:00\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertNotCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a message that is for a temp timeout of one minute and one second and a domain that handles temp suspensions, we should process", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling:      true,
					EnableTemporarySuspensionHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_ban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\",\"banned_at\":\"2020-11-12T23:18:00+00:00\",\"expires_at\":\"2020-11-12T23:19:01+00:00\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a message that is for a temp timeout of one minute and one second and a domain that doesn't handle temp suspensions, we should not process", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_ban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\",\"banned_at\":\"2020-11-12T23:18:00+00:00\",\"expires_at\":\"2020-11-12T23:19:01+00:00\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertNotCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a message that is for a permanent ban we should process", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_ban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\",\"banned_at\":\"2020-11-12T23:18:00+00:00\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a domain that is not opted in to user moderation handling, we should not call ModerateEntry and we should return nil", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: false,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_ban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertNotCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a domain that is opted in to user moderation handling, we should call ModerateEntry for chat_ban events and we should return nil", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_ban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a domain that is opted in to user moderation handling, we should call ModerateEntry for chat_unban events and we should return nil", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, nil)
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_unban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldBeNil)
			mockModerateEntryAPI.AssertCalled(t, "ModerateEntry", mock.Anything, mock.Anything)
		})

		Convey("Given a domain that is opted in to user moderation handling, we should error when ModerateEntry errors", func() {
			mockModerateEntryAPI.On("ModerateEntry", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))
			mockedDomainConfigManager.On("GetAll").Return(map[identifier.Domain]domain.Config{
				domain.BitsUsageByChannelDomain: {
					EnableUserModerationHandling: true,
				},
			})

			validEventJSON := `{
				"MessageAttributes": {
 					"event": {
						"Value": "chat_unban"
					}
				},
				"Message": "{\"channel_id\":\"kappa123\",\"target_id\":\"some_bad_apple\"}"
			}`

			err := worker.Handle(&sqs.Message{
				Body: &validEventJSON,
			})
			So(err, ShouldNotBeNil)
		})
	})
}
