package sqs

import (
	"context"
	"sync"

	"code.justin.tv/chat/workerqueue"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-errors/errors"
	log "github.com/sirupsen/logrus"
)

// TODO: Migrate to shared commerce library (https://twitchtv.atlassian.net/browse/FUEL-3034)

const (
	workerVersion = workerqueue.TaskVersion(1)
)

type Worker interface {
	Handle(msg *sqs.Message) error
}

type WorkerManager interface {
	Name() string
	Shutdown(ctx context.Context) error
}

type WorkerManagerImpl struct {
	name        string
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
	worker      Worker
}

func NewSQSWorkerManager(queueName string, numWorkers int, client sqsiface.SQSAPI, worker Worker, stats statsd.Statter) WorkerManager {
	manager := &WorkerManagerImpl{
		name:        queueName,
		stopChannel: make(chan struct{}),
		worker:      worker,
	}

	params := workerqueue.CreateWorkersParams{
		NumWorkers: numWorkers,
		QueueName:  queueName,
		Client:     client,
		Tasks: map[workerqueue.TaskVersion]workerqueue.TaskFn{
			workerqueue.FallbackTaskVersion: manager.worker.Handle,
			workerVersion:                   manager.worker.Handle,
		},
	}

	var errCh <-chan error
	var err error
	manager.waitGroup, errCh, err = workerqueue.CreateWorkers(params, manager.stopChannel, stats)
	if err != nil {
		log.Fatalf("Failed to create workers: %v", err)
	}

	go func() {
		for err := range errCh {
			log.WithField("queueName", queueName).WithError(err).Error("Error occurred in SQS workers")
		}
	}()
	return manager
}

func (mgr *WorkerManagerImpl) Name() string {
	return mgr.name
}

func (mgr *WorkerManagerImpl) Shutdown(ctx context.Context) error {
	close(mgr.stopChannel)
	if wait(ctx, mgr.waitGroup) {
		return errors.New("Timed out while waiting for SQS workers to complete")
	}
	return nil
}

func wait(ctx context.Context, wg *sync.WaitGroup) bool {
	workersDone := make(chan struct{})
	go func() {
		defer close(workersDone)
		wg.Wait()
	}()

	select {
	case <-workersDone:
		return false // done, no timeout
	case <-ctx.Done():
		return true // done, timed out
	}
}
