package moderation

import (
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

type daoImpl struct {
	*dynamo.Table
}

type DAO interface {
	GetModeration(leaderboardSetID string, entryKey string) (*Moderation, error)
	GetModerations(leaderboardSetID string, limit int, pagingKey dynamo.PagingKey) ([]*Moderation, error)
	UpdateModeration(m *Moderation) error
	DeleteModeration(leaderboardSetID string, entryKey string) error
	BatchDeleteModeration(deletes ...BatchDeleteModeration) (int, error)
	GetModerationsByEntryKey(entryKey string, limit int, cursor *GetModerationsByEntryKeyCursor) ([]Moderation, *GetModerationsByEntryKeyCursor, error)
}

type Moderation struct {
	LeaderboardSetID string    `dynamo:"leaderboard_set_id"`
	EntryKey         string    `dynamo:"entry_key"`
	State            string    `dynamo:"state"`
	TimeOfModeration time.Time `dynamo:"time_of_moderation"`
	LastUpdated      time.Time `dynamo:"last_updated"`
}

type BatchDeleteModeration struct {
	LeaderboardSetID string
	EntryKey         string
}

type GetModerationsByEntryKeyCursor struct {
	LeaderboardSetID string
	LastUpdated      string
}

func (bd BatchDeleteModeration) HashKey() interface{} {
	return bd.LeaderboardSetID
}

func (bd BatchDeleteModeration) RangeKey() interface{} {
	return bd.EntryKey
}

func NewDAO(sess *session.Session, name string) DAO {
	db := dynamo.New(sess)
	table := db.Table(name)
	return &daoImpl{Table: &table}
}

func (dao *daoImpl) GetModeration(leaderboardSetID string, entryKey string) (*Moderation, error) {
	var moderation Moderation
	err := dao.Get("leaderboard_set_id", leaderboardSetID).
		Range("entry_key", dynamo.Equal, entryKey).
		Consistent(true).
		One(&moderation)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &moderation, err
}

func (dao *daoImpl) GetModerations(leaderboardSetID string, limit int, pagingKey dynamo.PagingKey) ([]*Moderation, error) {
	var moderations []*Moderation
	err := dao.Get("leaderboard_set_id", leaderboardSetID).
		Limit(int64(limit)).
		StartFrom(pagingKey).
		Consistent(true).
		All(moderations)

	return moderations, err
}

func (dao *daoImpl) UpdateModeration(m *Moderation) error {
	return dao.Put(m).If("attribute_not_exists('time_of_moderation') OR 'time_of_moderation' < ?", m.TimeOfModeration).Run()
}

func (dao *daoImpl) DeleteModeration(leaderboardSetID string, entryKey string) error {
	return dao.Delete("leaderboard_set_id", leaderboardSetID).
		Range("entry_key", entryKey).
		Run()
}

func (dao *daoImpl) BatchDeleteModeration(deletes ...BatchDeleteModeration) (int, error) {
	var keyed []dynamo.Keyed
	for _, d := range deletes {
		keyed = append(keyed, d)
	}

	return dao.
		Batch("leaderboard_set_id", "entry_key").
		Write().
		Delete(keyed...).
		Run()
}

func (dao *daoImpl) GetModerationsByEntryKey(entryKey string, limit int, cursor *GetModerationsByEntryKeyCursor) ([]Moderation, *GetModerationsByEntryKeyCursor, error) {
	var pagingKey dynamo.PagingKey
	if cursor != nil {
		pagingKey = dynamo.PagingKey{
			"leaderboard_set_id": &dynamodb.AttributeValue{
				S: aws.String(cursor.LeaderboardSetID),
			},
			"entry_key": &dynamodb.AttributeValue{
				S: aws.String(entryKey),
			},
			"last_updated": &dynamodb.AttributeValue{
				S: aws.String(cursor.LastUpdated),
			},
		}
	}

	var moderations []Moderation
	newPagingKey, err := dao.Get("entry_key", entryKey).
		Index(dao.Name() + "_entry_key-index").
		StartFrom(pagingKey).
		Limit(int64(limit)).
		AllWithLastEvaluatedKey(&moderations)
	if err == dynamo.ErrNotFound {
		return []Moderation{}, nil, nil
	} else if err != nil {
		return []Moderation{}, nil, err
	}

	var newLastUpdated string
	lastUpdatedAttribute, ok := newPagingKey["last_updated"]
	if ok && lastUpdatedAttribute != nil {
		newLastUpdated = pointers.StringOrDefault(lastUpdatedAttribute.S, "")
	}

	var newLeaderboardSetId string
	leaderboardSetIdAttribute, ok := newPagingKey["leaderboard_set_id"]
	if ok && leaderboardSetIdAttribute != nil {
		newLeaderboardSetId = pointers.StringOrDefault(leaderboardSetIdAttribute.S, "")
	}

	var newCursor *GetModerationsByEntryKeyCursor
	if lastUpdatedAttribute != nil && leaderboardSetIdAttribute != nil {
		newCursor = &GetModerationsByEntryKeyCursor{
			LeaderboardSetID: newLeaderboardSetId,
			LastUpdated:      newLastUpdated,
		}
	}

	return moderations, newCursor, nil
}
