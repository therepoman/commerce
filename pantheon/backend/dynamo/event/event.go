package event

import (
	"code.justin.tv/commerce/gogogadget/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

type daoImpl struct {
	*dynamo.Table
}

type DAO interface {
	GetEvent(leaderboardID string, eventID string) (*Event, error)
	GetEvents(leaderboardID string, limit int, pagingKey dynamo.PagingKey) ([]*Event, error)
	UpdateEvent(e *Event) error
	DeleteEvent(leaderboardID string, eventID string) error
	BatchDeleteEvent(deletes ...BatchDeleteEvent) (int, error)
	GetEventsByEntryKey(entryKey string, limit int, cursor *GetEventsByEntryKeyCursor) ([]Event, *GetEventsByEntryKeyCursor, error)
}

type Event struct {
	LeaderboardID string `dynamo:"leaderboard_id"`
	EventID       string `dynamo:"event_id"`
	EntryKey      string `dynamo:"entry_key"`
	Value         int64  `dynamo:"value"`
	Timestamp     int64  `dynamo:"timestamp"`
}

type BatchDeleteEvent struct {
	LeaderboardID string
	EventID       string
}

type GetEventsByEntryKeyCursor struct {
	LeaderboardID string
	EventID       string
	Timestamp     string
}

func (bd BatchDeleteEvent) HashKey() interface{} {
	return bd.LeaderboardID
}

func (bd BatchDeleteEvent) RangeKey() interface{} {
	return bd.EventID
}

func NewDAO(sess *session.Session, name string) DAO {
	db := dynamo.New(sess)
	table := db.Table(name)
	return &daoImpl{Table: &table}
}

func (dao *daoImpl) GetEvent(leaderboardID string, eventID string) (*Event, error) {
	var event Event
	err := dao.Get("leaderboard_id", leaderboardID).
		Range("event_id", dynamo.Equal, eventID).
		Consistent(true).
		One(&event)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &event, err
}

func (dao *daoImpl) GetEvents(leaderboardID string, limit int, pagingKey dynamo.PagingKey) ([]*Event, error) {
	var events []*Event
	err := dao.Get("leaderboard_id", leaderboardID).
		Limit(int64(limit)).
		StartFrom(pagingKey).
		Consistent(true).
		All(events)

	return events, err
}

func (dao *daoImpl) UpdateEvent(e *Event) error {
	return dao.Put(e).If("attribute_not_exists(leaderboard_id) AND attribute_not_exists(event_id)").Run()
}

func (dao *daoImpl) DeleteEvent(leaderboardID string, eventID string) error {
	return dao.Delete("leaderboard_id", leaderboardID).
		Range("event_id", eventID).
		Run()
}

func (dao *daoImpl) BatchDeleteEvent(deletes ...BatchDeleteEvent) (int, error) {
	var keyed []dynamo.Keyed
	for _, d := range deletes {
		keyed = append(keyed, d)
	}

	return dao.
		Batch("leaderboard_id", "event_id").
		Write().
		Delete(keyed...).
		Run()
}

func (dao *daoImpl) GetEventsByEntryKey(entryKey string, limit int, cursor *GetEventsByEntryKeyCursor) ([]Event, *GetEventsByEntryKeyCursor, error) {
	var pagingKey dynamo.PagingKey
	if cursor != nil {
		pagingKey = dynamo.PagingKey{
			"leaderboard_id": &dynamodb.AttributeValue{
				S: aws.String(cursor.LeaderboardID),
			},
			"event_id": &dynamodb.AttributeValue{
				S: aws.String(cursor.EventID),
			},
			"entry_key": &dynamodb.AttributeValue{
				S: aws.String(entryKey),
			},
			"timestamp": &dynamodb.AttributeValue{
				N: aws.String(cursor.Timestamp),
			},
		}
	}

	var events []Event
	newPagingKey, err := dao.Get("entry_key", entryKey).
		Index(dao.Name() + "_entry_key-index").
		StartFrom(pagingKey).
		Limit(int64(limit)).
		AllWithLastEvaluatedKey(&events)
	if err == dynamo.ErrNotFound {
		return []Event{}, nil, nil
	} else if err != nil {
		return []Event{}, nil, err
	}

	var newTimestamp string
	timestampAttribute, ok := newPagingKey["timestamp"]
	if ok && timestampAttribute != nil {
		newTimestamp = pointers.StringOrDefault(timestampAttribute.N, "")
	}

	var newLeaderboardID string
	leaderboardIDAttribute, ok := newPagingKey["leaderboard_id"]
	if ok && leaderboardIDAttribute != nil {
		newLeaderboardID = pointers.StringOrDefault(leaderboardIDAttribute.S, "")
	}

	var newEventID string
	eventIDAttribute, ok := newPagingKey["event_id"]
	if ok && eventIDAttribute != nil {
		newEventID = pointers.StringOrDefault(eventIDAttribute.S, "")
	}

	var newCursor *GetEventsByEntryKeyCursor
	if timestampAttribute != nil && leaderboardIDAttribute != nil && eventIDAttribute != nil {
		newCursor = &GetEventsByEntryKeyCursor{
			LeaderboardID: newLeaderboardID,
			EventID:       newEventID,
			Timestamp:     newTimestamp,
		}
	}

	return events, newCursor, nil
}
