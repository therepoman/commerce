package delete_leaderboard_entries

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	lb "code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	redisLB "code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/clients/s3"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	goRedis "github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type Input struct {
	Domain        identifier.Domain `json:"domain"`
	EntryKey      entry.Key         `json:"entry_key"`
	Events        []event.Event     `json:"events"`
	LeaderboardID string            `json:"leaderboard_id"`
	EventID       string            `json:"event_id"`
	Timestamp     string            `json:"timestamp"`
}

type Output struct {
	Domain        identifier.Domain `json:"domain"`
	EntryKey      entry.Key         `json:"entry_key"`
	LeaderboardID string            `json:"leaderboard_id"`
	EventID       string            `json:"event_id"`
	Timestamp     string            `json:"timestamp"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (Output, error)
}

type handler struct {
	Config      *config.Config                 `inject:""`
	Landlord    tenant.Landlord                `inject:""`
	Persister   persistence.Persister          `inject:""`
	Factory     redisLB.LeaderboardFactory     `inject:""`
	PDMSFactory redisLB.PDMSLeaderboardFactory `inject:"pdms-lb-factory"`
	S3Client    s3.Client                      `inject:""`
}

func NewHandler() Handler {
	return &handler{}
}

func (h *handler) Handle(ctx context.Context, input Input) (Output, error) {
	if string(input.Domain) == "" {
		return Output{}, errors.New("domain cannot be empty")
	}

	for _, publishEvent := range input.Events {
		log := logrus.WithField("event", publishEvent)
		key := publishEvent.LeaderboardID
		entryKey := entry.Key(publishEvent.EntryKey)
		retrievalSetting := lb.RetrievalSettings{ConsistentRead: true}
		lbIdentifier, err := identifier.FromKey(key)
		if err != nil {
			log.WithField("key", key).WithError(err).Error("Failed to get identifier from the given key")
			return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to get identifier from key %s", key))
		}

		lbInRedis, err := h.Factory.IsInRedis(ctx, lbIdentifier, retrievalSetting)
		if err != nil {
			log.WithError(err).Error("Failed to check if this leaderboard exists in Redis")
			return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to check if this leaderboard exists in Redis for identifier %s", lbIdentifier))
		}

		var targetLB lb.Leaderboard
		if !lbInRedis {
			// Load leaderboard from Redis if exists. If not exists, import it from S3
			targetLB, err = h.PDMSFactory.GetRedisLeaderboard(ctx, lbIdentifier, retrievalSetting)
			if err != nil {
				log.WithError(err).Error("Failed to get and load this Redis leaderboard into PDMS redis")
				return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to get and load this Redis leaderboard into PDMS redis with identifier %s", lbIdentifier))
			}
		} else {
			targetLB, err = h.Factory.GetRedisLeaderboard(ctx, lbIdentifier, retrievalSetting)
			if err != nil {
				log.WithError(err).Error("Failed to get this Redis leaderboard")
				return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to get Redis leaderboard with identifier %s", lbIdentifier))
			}
		}

		err = targetLB.DeleteEntry(entryKey)
		if err != nil {
			log.WithError(err).Error("Failed to delete this entry from this Redis leaderboard")
			return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to delete this entry from this Redis leaderboard with identifier %s and key %s", lbIdentifier, entryKey))
		}

		if !lbInRedis {
			err = h.Persister.Export(ctx, lbIdentifier, targetLB, retrievalSetting)
			// This occurs when the entry key just removed happens to be the only member in that leaderboard
			if err == goRedis.Nil {
				s3Key := persistence.GetS3Key(lbIdentifier, targetLB)
				log.WithField("s3Key", s3Key).Info("Leaderboard no longer has entry. Deleting S3 file")

				err = h.S3Client.DeleteFiles(h.Config.LeaderboardS3Bucket, s3Key)
				if err != nil {
					log.WithField("s3Key", s3Key).WithError(err).Error("Failed to delete leaderboard from S3")
					return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to delete leaderboard from S3 for %s", s3Key))
				}
			} else if err != nil {
				log.WithError(err).Error("Failed to export leaderboard from Redis to S3")
				return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to export leaderboard from Redis to S3 for %s", lbIdentifier))
			} else {
				err = targetLB.Destroy()
				if err != nil {
					log.WithError(err).Error("Failed to destroy this leaderboard from Redis")
					return Output{}, errors.Wrap(err, fmt.Sprintf("Failed to destroy this leaderboard from Redis for %s", lbIdentifier))
				}
			}
		}
	}

	return Output{
		Domain:        input.Domain,
		EntryKey:      input.EntryKey,
		LeaderboardID: input.LeaderboardID,
		EventID:       input.EventID,
		Timestamp:     input.Timestamp,
	}, nil
}
