package delete_leaderboard_entries

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	tenant_client "code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	persistence_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	redis_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	redisMock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/redis"
	s3_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/s3"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	goRedis "github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandle(t *testing.T) {
	Convey("Given delete_leaderboard_entries handler", t, func() {
		mockConfig := &config.Config{
			LeaderboardS3Bucket: "test-s3",
		}
		mockLandlord := new(tenant_mock.Landlord)
		mockRedisClient := new(redisMock.Client)
		mockFactory := new(redis_mock.LeaderboardFactory)
		mockPDMSLeaderboardFactory := new(redis_mock.PDMSLeaderboardFactory)
		mockPersister := new(persistence_mock.Persister)
		mockS3Client := new(s3_mock.Client)
		mockTenant := tenant_client.Tenant{
			RedisClient: mockRedisClient,
		}
		mockLeadeboard := new(leaderboard_mock.Leaderboard)
		mockLandlord.On("GetTenant", identifier.Domain("bits")).Return(mockTenant)
		testHandler := handler{
			Config:      mockConfig,
			Landlord:    mockLandlord,
			Persister:   mockPersister,
			Factory:     mockFactory,
			PDMSFactory: mockPDMSLeaderboardFactory,
			S3Client:    mockS3Client,
		}
		ctx := context.Background()
		sampleInput := Input{
			Domain: "bits",
			Events: []event.Event{
				{
					LeaderboardID: "bits-usage-by-channel-v1/1111/ALLTIME/000",
					EventID:       "lesad-eventid",
					EntryKey:      "lesad-ohki",
				},
			},
			EntryKey:      "12345",
			LeaderboardID: "xyz",
			EventID:       "abc",
			Timestamp:     "456",
		}
		mockLeadeboard.On("Type").Return("redis")

		Convey("when domain is empty", func() {
			_, err := testHandler.Handle(ctx, Input{})

			So(err, ShouldNotBeNil)
			mockRedisClient.AssertNumberOfCalls(t, "Exists", 0)
		})

		Convey("when Factory.IsInRedis errors", func() {
			mockFactory.On("IsInRedis", mock.Anything, mock.Anything, mock.Anything).
				Return(false, errors.New("lesad-isinredis")).Once()
			_, err := testHandler.Handle(ctx, sampleInput)

			So(err, ShouldNotBeNil)
			mockFactory.AssertNumberOfCalls(t, "IsInRedis", 1)
			mockFactory.AssertNumberOfCalls(t, "GetRedisLeaderboard", 0)
			mockPersister.AssertNumberOfCalls(t, "Import", 0)
		})

		Convey("when the leaderboard exists in Redis", func() {
			mockFactory.On("IsInRedis", mock.Anything, mock.Anything, mock.Anything).
				Return(true, nil).Once()

			Convey("when Factory.GetRedisLeaderboard errors", func() {
				mockFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("lesad-getleaderboard")).Once()
				_, err := testHandler.Handle(ctx, sampleInput)

				So(err, ShouldNotBeNil)
				mockFactory.AssertNumberOfCalls(t, "GetRedisLeaderboard", 1)
				mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 0)
				mockPersister.AssertNumberOfCalls(t, "Import", 0)
			})

			Convey("when deleting entry fails", func() {
				mockLeadeboard.On("DeleteEntry", mock.Anything).
					Return(errors.New("lesad-deleteentry")).Once()
				mockFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).
					Return(mockLeadeboard, nil).Once()
				_, err := testHandler.Handle(ctx, sampleInput)

				So(err, ShouldNotBeNil)
				mockFactory.AssertNumberOfCalls(t, "GetRedisLeaderboard", 1)
				mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
				mockPersister.AssertNumberOfCalls(t, "Import", 0)
			})

			Convey("when everything goes fine", func() {
				mockLeadeboard.On("DeleteEntry", mock.Anything).Return(nil).Once()
				mockFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).
					Return(mockLeadeboard, nil).Once()
				_, err := testHandler.Handle(ctx, sampleInput)

				So(err, ShouldBeNil)
				mockFactory.AssertNumberOfCalls(t, "GetRedisLeaderboard", 1)
				mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
				mockPersister.AssertNumberOfCalls(t, "Import", 0)
			})
		})

		Convey("when the leaderboard does not exist in Redis", func() {
			mockPDMSLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).
				Return(mockLeadeboard, nil).Once()
			mockFactory.On("IsInRedis", mock.Anything, mock.Anything, mock.Anything).
				Return(false, nil).Once()
			mockLeadeboard.On("DeleteEntry", mock.Anything).Return(nil).Once()

			Convey("when Persister.Export fails because the leaderboard is now empty", func() {
				mockPersister.On("Export", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
					Return(goRedis.Nil).Once()

				Convey("when DeleteFiles fails", func() {
					mockS3Client.On("DeleteFiles", mock.Anything, mock.Anything).
						Return(errors.New("lesad deletefiles")).Once()
					_, err := testHandler.Handle(ctx, sampleInput)

					So(err, ShouldNotBeNil)
					mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
					mockPersister.AssertNumberOfCalls(t, "Export", 1)
					mockS3Client.AssertNumberOfCalls(t, "DeleteFiles", 1)
					mockLeadeboard.AssertNumberOfCalls(t, "Destroy", 0)
				})

				Convey("when DeleteFiles succeeds", func() {
					mockS3Client.On("DeleteFiles", mock.Anything, mock.Anything).
						Return(nil).Once()
					_, err := testHandler.Handle(ctx, sampleInput)

					So(err, ShouldBeNil)
					mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
					mockPersister.AssertNumberOfCalls(t, "Export", 1)
					mockS3Client.AssertNumberOfCalls(t, "DeleteFiles", 1)
					mockLeadeboard.AssertNumberOfCalls(t, "Destroy", 0)
				})
			})

			Convey("when Persister.Export really fails for some reason", func() {
				mockPersister.On("Export", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
					Return(errors.New("not Redis.Nil")).Once()
				_, err := testHandler.Handle(ctx, sampleInput)

				So(err, ShouldNotBeNil)
				mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
				mockPersister.AssertNumberOfCalls(t, "Export", 1)
				mockS3Client.AssertNumberOfCalls(t, "DeleteFiles", 0)
				mockLeadeboard.AssertNumberOfCalls(t, "Destroy", 0)
			})

			Convey("when Persister.Export succeeds", func() {
				mockPersister.On("Export", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
					Return(nil).Once()

				Convey("when destroying the leaderboard fails", func() {
					mockLeadeboard.On("Destroy").Return(errors.New("lesad-destroy")).Once()
					_, err := testHandler.Handle(ctx, sampleInput)

					So(err, ShouldNotBeNil)
					mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
					mockPersister.AssertNumberOfCalls(t, "Export", 1)
					mockS3Client.AssertNumberOfCalls(t, "DeleteFiles", 0)
					mockLeadeboard.AssertNumberOfCalls(t, "Destroy", 1)
				})

				Convey("when everything works", func() {
					mockLeadeboard.On("Destroy").Return(nil).Once()
					_, err := testHandler.Handle(ctx, sampleInput)

					So(err, ShouldBeNil)
					mockLeadeboard.AssertNumberOfCalls(t, "DeleteEntry", 1)
					mockPersister.AssertNumberOfCalls(t, "Export", 1)
					mockS3Client.AssertNumberOfCalls(t, "DeleteFiles", 0)
					mockLeadeboard.AssertNumberOfCalls(t, "Destroy", 1)
				})
			})
		})
	})
}
