package batch_delete_moderations

import (
	"context"
	"errors"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
)

type Input struct {
	Domain           identifier.Domain       `json:"domain"`
	EntryKey         entry.Key               `json:"entry_key"`
	Moderations      []moderation.Moderation `json:"moderations"`
	LeaderboardSetId string                  `json:"leaderboard_set_id"`
	LastUpdated      string                  `json:"last_updated"`
}

type Output struct {
	Domain           identifier.Domain `json:"domain"`
	EntryKey         entry.Key         `json:"entry_key"`
	LeaderboardSetId string            `json:"leaderboard_set_id"`
	LastUpdated      string            `json:"last_updated"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (Output, error)
}

type handler struct {
	Landlord tenant.Landlord `inject:""`
}

func NewHandler() Handler {
	return &handler{}
}

func (h *handler) Handle(ctx context.Context, input Input) (Output, error) {
	if string(input.Domain) == "" {
		return Output{}, errors.New("domain cannot be empty")
	}

	batchDeleteModerations := make([]moderation.BatchDeleteModeration, 0)
	for _, m := range input.Moderations {
		batchDeleteModerations = append(batchDeleteModerations, moderation.BatchDeleteModeration{
			LeaderboardSetID: m.LeaderboardSetID,
			EntryKey:         m.EntryKey,
		})
	}

	_, err := h.Landlord.GetTenant(input.Domain).ModerationDAO.BatchDeleteModeration(batchDeleteModerations...)
	if err != nil {
		logrus.WithError(err).Error("Failed to batch delete moderations")
	}

	return Output{
		Domain:           input.Domain,
		EntryKey:         input.EntryKey,
		LeaderboardSetId: input.LeaderboardSetId,
		LastUpdated:      input.LastUpdated,
	}, err
}
