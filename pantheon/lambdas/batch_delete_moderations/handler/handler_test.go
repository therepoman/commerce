package batch_delete_moderations

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	tenant_client "code.justin.tv/commerce/pantheon/clients/tenant"
	moderation_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	Convey("Given batch delete moderation handler", t, func() {
		mockModerationDAO := new(moderation_mock.DAO)
		mockLandlord := new(tenant_mock.Landlord)
		mockTenant := tenant_client.Tenant{
			ModerationDAO: mockModerationDAO,
		}
		mockLandlord.On("GetTenant", identifier.Domain("bits")).Return(mockTenant)
		testHandler := handler{
			Landlord: mockLandlord,
		}
		ctx := context.Background()
		testInput := Input{
			Domain: "bits",
			Moderations: []moderation.Moderation{
				{
					LeaderboardSetID: "test-lb-set-id",
					EntryKey:         "test-entry-key",
				},
			},
			LeaderboardSetId: "003",
			LastUpdated:      "abc",
		}

		Convey("when input is bad", func() {
			_, err := testHandler.Handle(ctx, Input{})

			So(err, ShouldNotBeNil)
			mockLandlord.AssertNumberOfCalls(t, "GetTenant", 0)
		})

		Convey("when dao errors", func() {
			mockModerationDAO.On("BatchDeleteModeration", mock.Anything).
				Return(1, errors.New("lesad-delete")).Once()
			_, err := testHandler.Handle(ctx, testInput)

			So(err, ShouldNotBeNil)
			mockModerationDAO.AssertNumberOfCalls(t, "BatchDeleteModeration", 1)
		})

		Convey("when everything succeeds", func() {
			mockModerationDAO.On("BatchDeleteModeration", mock.Anything).
				Return(0, nil).Once()
			output, err := testHandler.Handle(ctx, testInput)

			So(err, ShouldBeNil)
			mockModerationDAO.AssertNumberOfCalls(t, "BatchDeleteModeration", 1)
			So(output.LeaderboardSetId, ShouldEqual, "003")
		})
	})
}
