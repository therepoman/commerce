package query_events_by_entry_key

import (
	"context"
	"math/rand"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/pkg/errors"
)

const (
	query_limit = 100
)

type Input struct {
	Domain        identifier.Domain `json:"domain"`
	EntryKey      entry.Key         `json:"entry_key"`
	LeaderboardID string            `json:"leaderboard_id"`
	EventID       string            `json:"event_id"`
	Timestamp     string            `json:"timestamp"`
}

type Output struct {
	Domain        identifier.Domain `json:"domain"`
	EntryKey      entry.Key         `json:"entry_key"`
	Events        []event.Event     `json:"events"`
	LeaderboardID string            `json:"leaderboard_id"`
	EventID       string            `json:"event_id"`
	Timestamp     string            `json:"timestamp"`
	WaitSeconds   int               `json:"wait_seconds"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (Output, error)
}

type handler struct {
	Landlord tenant.Landlord `inject:""`
}

func NewHandler() Handler {
	return &handler{}
}

func (l handler) Handle(ctx context.Context, input Input) (Output, error) {
	if string(input.Domain) == "" || string(input.EntryKey) == "" {
		return Output{}, errors.Errorf("invalid input: %v", input)
	}

	var cursor *event.GetEventsByEntryKeyCursor
	if input.LeaderboardID != "" && input.EventID != "" && input.Timestamp != "" {
		cursor = &event.GetEventsByEntryKeyCursor{
			LeaderboardID: input.LeaderboardID,
			EventID:       input.EventID,
			Timestamp:     input.Timestamp,
		}
	}

	events, nextCursor, err := l.Landlord.GetTenant(input.Domain).
		EventDAO.GetEventsByEntryKey(string(input.EntryKey), query_limit, cursor)
	if err != nil {
		logrus.WithError(err).Error("Failed calling GetEventsByEntryKey")
		return Output{}, err
	}

	var newLeaderboardID, newEventID, newTimestamp string
	if nextCursor != nil {
		newLeaderboardID = nextCursor.LeaderboardID
		newEventID = nextCursor.EventID
		newTimestamp = nextCursor.Timestamp
	}

	// Random number between 0 and 100
	rand.Seed(time.Now().UnixNano())
	waitSeconds := rand.Intn(100)

	return Output{
		Domain:        input.Domain,
		EntryKey:      input.EntryKey,
		Events:        events,
		LeaderboardID: newLeaderboardID,
		EventID:       newEventID,
		Timestamp:     newTimestamp,
		WaitSeconds:   waitSeconds,
	}, nil
}
