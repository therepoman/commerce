package query_events_by_entry_key

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	tenant_client "code.justin.tv/commerce/pantheon/clients/tenant"
	event_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/event"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	Convey("Given query_events handler", t, func() {
		mockEventDAO := new(event_mock.DAO)
		mockLandlord := new(tenant_mock.Landlord)
		mockTenant := tenant_client.Tenant{
			EventDAO: mockEventDAO,
		}
		mockLandlord.On("GetTenant", identifier.Domain("bits")).Return(mockTenant)
		testHandler := handler{
			Landlord: mockLandlord,
		}
		ctx := context.Background()

		Convey("when input is invalid", func() {
			_, err := testHandler.Handle(ctx, Input{})
			So(err, ShouldNotBeNil)
		})

		Convey("when DAO errors", func() {
			mockEventDAO.On("GetEventsByEntryKey", mock.Anything, mock.Anything, mock.Anything).
				Return([]event.Event{}, nil, errors.New("lesadohki")).Once()

			_, err := testHandler.Handle(ctx, Input{Domain: "bits", EntryKey: entry.Key("111")})
			So(err, ShouldNotBeNil)
			mockEventDAO.AssertNumberOfCalls(t, "GetEventsByEntryKey", 1)
		})

		Convey("when DAO succeeds", func() {
			mockEventDAO.On("GetEventsByEntryKey", mock.Anything, mock.Anything, mock.Anything).
				Return([]event.Event{
					{
						LeaderboardID: "test-lb-id",
						EventID:       "test-event-id",
						EntryKey:      "111",
					},
				}, &event.GetEventsByEntryKeyCursor{
					LeaderboardID: "abc",
					EventID:       "def",
					Timestamp:     "123",
				}, nil).Once()

			output, err := testHandler.Handle(ctx, Input{Domain: "bits", EntryKey: entry.Key("111")})
			So(err, ShouldBeNil)
			So(output.LeaderboardID, ShouldEqual, "abc")
			if len(output.Events) < 1 {
				t.Fail()
			}
			So(output.Events[0].LeaderboardID, ShouldEqual, "test-lb-id")
		})
	})
}
