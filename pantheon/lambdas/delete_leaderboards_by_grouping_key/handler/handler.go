package delete_leaderboards_by_grouping_key

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis/shadow"
	"code.justin.tv/commerce/pantheon/clients/s3"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
)

type Input struct {
	GroupingKey identifier.GroupingKey `json:"grouping_key"`
	Domain      identifier.Domain      `json:"domain"`
}

type Output struct {
	GroupingKey identifier.GroupingKey `json:"grouping_key"`
	Domain      identifier.Domain      `json:"domain"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (*Output, error)
}

type handler struct {
	TenantClient tenant.Landlord               `inject:""`
	IndexFactory redis.GroupingKeyIndexFactory `inject:""`
	S3Client     s3.Client                     `inject:""`
	Config       *config.Config                `inject:""`
}

func NewHandler() Handler {
	return &handler{}
}

func (l handler) Handle(ctx context.Context, input Input) (*Output, error) {
	groupingKeyIndex := l.IndexFactory.GetRedisGroupingKeyIndex(input.Domain, input.GroupingKey)
	client := l.TenantClient.GetTenant(input.Domain).RedisClient

	log := logrus.WithFields(logrus.Fields{
		"groupingKey": input.GroupingKey,
		"domain":      input.Domain,
	})

	// get redis leaderboards from grouping key index
	ids, err := groupingKeyIndex.Get(ctx, leaderboard.RetrievalSettings{})
	if err != nil {
		log.WithError(err).Error("failed to get redis grouping key index")
		return nil, err
	}

	if len(ids) > 0 {
		var keysToDelete []string
		for _, id := range ids {
			// need to delete the leaderboard and shadow entries
			keysToDelete = append(keysToDelete, id.Key())
			keysToDelete = append(keysToDelete, shadow.GetShadowKey(id))
		}

		// delete redis leaderboards
		logrus.Info(fmt.Sprintf("Deleting %d keys: %v", len(keysToDelete), keysToDelete))
		_, err = client.Del(ctx, keysToDelete...)
		if err != nil {
			log.WithError(err).Error("failed to delete redis leaderboards")
			return nil, err
		}
	}

	// get S3 leaderboards by domain/grouping_key
	bucket := l.Config.LeaderboardS3Bucket
	path := fmt.Sprintf("%s/%s/", input.Domain, input.GroupingKey)
	logrus.Info(fmt.Sprintf("Getting s3 files with prefix: %s, under bucket: %s", path, bucket))

	files, err := l.S3Client.ListFileNames(bucket, path)
	if err != nil {
		log.WithError(err).Error("failed to list s3 leaderboards")
		return nil, err
	}

	if len(files) > 0 {
		// delete S3 leaderboards
		logrus.Info(fmt.Sprintf("Deleting %d s3 files: %v", len(files), files))
		err = l.S3Client.DeleteFiles(bucket, files...)
		if err != nil {
			log.WithError(err).Error("failed to delete s3 leaderboards")
			return nil, err
		}
	}

	// delete redis grouping key index
	logrus.Info(fmt.Sprintf("Deleting grouping key index for: %s", groupingKeyIndex.Key()))
	err = groupingKeyIndex.Delete(ctx)
	if err != nil {
		log.WithError(err).Error("failed to delete redis grouping key index")
		return nil, err
	}

	return &Output{
		GroupingKey: input.GroupingKey,
		Domain:      input.Domain,
	}, nil
}
