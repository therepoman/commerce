package delete_leaderboards_by_grouping_key

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis/shadow"
	tenant_client "code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	index_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/index"
	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	redis "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/redis"
	s3_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/s3"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/pantheon/utils/random"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a delete leaderboards by grouping key lambda handler", t, func() {
		tenantClient := new(tenant_mock.Landlord)
		redisClient := new(redis.Client)
		groupingKeyIndexFactory := new(leaderboard_mock.GroupingKeyIndexFactory)
		s3Client := new(s3_mock.Client)
		s3LeaderboardBucket := "pantheon-leaderboard-s3-bucket"
		config := &config.Config{
			LeaderboardS3Bucket: s3LeaderboardBucket,
		}

		tenant := tenant_client.Tenant{
			RedisClient: redisClient,
		}

		domain := "bits"
		groupingKey := random.String(16)
		s3Path := fmt.Sprintf("%s/%s/", domain, groupingKey)
		tenantClient.On("GetTenant", identifier.Domain(domain)).Return(tenant)

		groupingKeyIndex := new(index_mock.Index)
		groupingKeyIndex.On("Key").Return(fmt.Sprintf("%s-%s-%s", "grouping-key-index", domain, groupingKey))
		groupingKeyIndexFactory.On("GetRedisGroupingKeyIndex", mock.Anything, mock.Anything).Return(groupingKeyIndex)

		handler := handler{
			TenantClient: tenantClient,
			IndexFactory: groupingKeyIndexFactory,
			S3Client:     s3Client,
			Config:       config,
		}
		input := Input{
			GroupingKey: identifier.GroupingKey(groupingKey),
			Domain:      identifier.Domain(domain),
		}
		ctx := context.Background()

		t := time.Now()
		timeBucket := bucket.TimestampToTimeBucket(t, pantheonrpc.TimeUnit_DAY)
		ids := []identifier.Identifier{{
			Domain:              identifier.Domain(domain),
			GroupingKey:         identifier.GroupingKey(groupingKey),
			TimeAggregationUnit: pantheonrpc.TimeUnit_DAY,
			TimeBucket:          timeBucket,
		}}
		idsToDelete := []string{ids[0].Key(), shadow.GetShadowKey(ids[0])}
		files := []string{fmt.Sprintf("%s/%s/%s", domain, groupingKey, "filename")}

		Convey("Returns an error getting redis leaderboards from grouping key index fails", func() {
			groupingKeyIndex.On("Get", ctx, mock.Anything).Return(nil, errors.New("Failed to get grouping key index"))
			output, err := handler.Handle(ctx, input)
			So(output, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("When getting redis leaderboards from grouping key index succeeds", func() {
			groupingKeyIndex.On("Get", ctx, mock.Anything).Return(ids, nil)

			Convey("Returns an error when deleting leaderboards fails", func() {
				redisClient.On("Del", ctx, idsToDelete[0], idsToDelete[1]).Return(int64(0), errors.New("Failed to delete leaderboards"))
				output, err := handler.Handle(ctx, input)
				So(output, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("When deleting leaderboards succeeds", func() {
				redisClient.On("Del", ctx, idsToDelete[0], idsToDelete[1]).Return(int64(2), nil)

				Convey("Returns an error when listing the s3 leaderboards fails", func() {
					s3Client.On("ListFileNames", s3LeaderboardBucket, s3Path).Return(nil, errors.New("Failed to list s3 leaderboards"))
					output, err := handler.Handle(ctx, input)
					So(output, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("When listing the s3 leaderboards succeeds", func() {
					s3Client.On("ListFileNames", s3LeaderboardBucket, s3Path).Return(files, nil)

					Convey("Returns an error when deleting the s3 leaderboards fails", func() {
						s3Client.On("DeleteFiles", s3LeaderboardBucket, files[0]).Return(errors.New("Failed to delete s3 leaderboards"))
						output, err := handler.Handle(ctx, input)
						So(output, ShouldBeNil)
						So(err, ShouldNotBeNil)
					})

					Convey("When deleting the s3 leaderboards succeeds", func() {
						s3Client.On("DeleteFiles", s3LeaderboardBucket, files[0]).Return(nil)

						Convey("Returns an error when deleting the grouping key index fails", func() {
							groupingKeyIndex.On("Delete", ctx).Return(errors.New("Failed to delete grouping key index"))
							output, err := handler.Handle(ctx, input)
							So(output, ShouldBeNil)
							So(err, ShouldNotBeNil)
						})

						Convey("When deleting grouping key index succeeds", func() {
							groupingKeyIndex.On("Delete", ctx).Return(nil)
							output, err := handler.Handle(ctx, input)
							So(output, ShouldResemble, &Output{
								GroupingKey: identifier.GroupingKey(groupingKey),
								Domain:      identifier.Domain(domain),
							})
							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
