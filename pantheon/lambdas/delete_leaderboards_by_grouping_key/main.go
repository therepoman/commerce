package main

import (
	"os"

	"code.justin.tv/commerce/pantheon/backend"
	"code.justin.tv/commerce/pantheon/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/codegangsta/cli"
	"github.com/sirupsen/logrus"
)

var environment string

func main() {
	app := cli.NewApp()
	app.Name = "scan_redis_by_grouping_key"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Value:       "staging",
			Destination: &environment,
		},
	}

	app.Action = actionFunc

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Error running lambda")
	}
}

func actionFunc(ctx *cli.Context) error {
	env := config.Environment(environment)

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		return err
	}

	logrus.Infof("Loaded config: %s", cfg.EnvironmentName)

	be, err := backend.NewLambdaBackend(cfg)
	if err != nil {
		return err
	}

	if env != config.SmokeTest {
		lambda.Start(be.GetLambdaHandlers().DeleteLeaderboardsByGroupingKey.Handle)
	}

	return nil
}
