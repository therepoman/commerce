package batch_delete_events

import (
	"context"
	"errors"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
)

type Input struct {
	Domain        identifier.Domain `json:"domain"`
	EntryKey      entry.Key         `json:"entry_key"`
	Events        []event.Event     `json:"events"`
	LeaderboardID string            `json:"leaderboard_id"`
	EventID       string            `json:"event_id"`
	Timestamp     string            `json:"timestamp"`
}

type Output struct {
	Domain        identifier.Domain `json:"domain"`
	EntryKey      entry.Key         `json:"entry_key"`
	LeaderboardID string            `json:"leaderboard_id"`
	EventID       string            `json:"event_id"`
	Timestamp     string            `json:"timestamp"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (Output, error)
}

type handler struct {
	Landlord tenant.Landlord `inject:""`
}

func NewHandler() Handler {
	return &handler{}
}

func (l *handler) Handle(ctx context.Context, input Input) (Output, error) {
	if string(input.Domain) == "" {
		return Output{}, errors.New("domain cannot be empty")
	}

	batchDeleteEvent := make([]event.BatchDeleteEvent, 0)
	for _, e := range input.Events {
		batchDeleteEvent = append(batchDeleteEvent, event.BatchDeleteEvent{
			LeaderboardID: e.LeaderboardID,
			EventID:       e.EventID,
		})
	}

	_, err := l.Landlord.GetTenant(input.Domain).
		EventDAO.BatchDeleteEvent(batchDeleteEvent...)
	if err != nil {
		logrus.WithField("events", batchDeleteEvent).WithError(err).
			Error("Failed to batch delete events")
	}

	return Output{
		Domain:        input.Domain,
		EntryKey:      input.EntryKey,
		LeaderboardID: input.LeaderboardID,
		EventID:       input.EventID,
		Timestamp:     input.Timestamp,
	}, err
}
