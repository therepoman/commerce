package batch_delete_events

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	tenant_client "code.justin.tv/commerce/pantheon/clients/tenant"
	event_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/event"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	Convey("Given batch delete event handler", t, func() {
		mockEventDAO := new(event_mock.DAO)
		mockLandlord := new(tenant_mock.Landlord)
		mockTenant := tenant_client.Tenant{
			EventDAO: mockEventDAO,
		}
		mockLandlord.On("GetTenant", identifier.Domain("bits")).Return(mockTenant)
		testHandler := handler{
			Landlord: mockLandlord,
		}
		ctx := context.Background()
		testInput := Input{
			Domain: "bits",
			Events: []event.Event{
				{
					LeaderboardID: "test-lb-id",
					EventID:       "test-event-id",
				},
			},
			LeaderboardID: "001",
			EventID:       "111",
			EntryKey:      "345",
			Timestamp:     "1111",
		}

		Convey("when input is bad", func() {
			_, err := testHandler.Handle(ctx, Input{})

			So(err, ShouldNotBeNil)
			mockLandlord.AssertNumberOfCalls(t, "GetTenant", 0)
		})

		Convey("when batch delete fails", func() {
			mockEventDAO.On("BatchDeleteEvent", mock.Anything).
				Return(1, errors.New("lesad-batch-delete")).Once()
			_, err := testHandler.Handle(ctx, testInput)

			So(err, ShouldNotBeNil)
			mockEventDAO.AssertNumberOfCalls(t, "BatchDeleteEvent", 1)
		})

		Convey("when everything succeeds", func() {
			mockEventDAO.On("BatchDeleteEvent", mock.Anything).
				Return(0, nil).Once()
			output, err := testHandler.Handle(ctx, testInput)

			So(err, ShouldBeNil)
			mockEventDAO.AssertNumberOfCalls(t, "BatchDeleteEvent", 1)
			So(output.LeaderboardID, ShouldEqual, "001")
		})
	})
}
