package query_moderations_by_entry_key

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	tenant_client "code.justin.tv/commerce/pantheon/clients/tenant"
	moderation_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	Convey("Given query moderations handler", t, func() {
		mockModerationDAO := new(moderation_mock.DAO)
		mockLandlord := new(tenant_mock.Landlord)
		mockTenant := tenant_client.Tenant{
			ModerationDAO: mockModerationDAO,
		}
		mockLandlord.On("GetTenant", identifier.Domain("bits")).Return(mockTenant)
		ctx := context.Background()
		testHandler := handler{
			Landlord: mockLandlord,
		}
		testInput := Input{
			Domain:           "bits",
			EntryKey:         "12345",
			LeaderboardSetId: "001",
			LastUpdated:      "1-1-2020",
		}

		Convey("when the input is bad", func() {
			_, err := testHandler.Handle(ctx, Input{})

			So(err, ShouldNotBeNil)
			mockLandlord.AssertNumberOfCalls(t, "GetTenant", 0)
		})

		Convey("when dao errors", func() {
			mockModerationDAO.On("GetModerationsByEntryKey", mock.Anything, mock.Anything, mock.Anything).
				Return(nil, nil, errors.New("lesad-moderations")).Once()
			_, err := testHandler.Handle(ctx, testInput)

			So(err, ShouldNotBeNil)
			mockModerationDAO.AssertNumberOfCalls(t, "GetModerationsByEntryKey", 1)
		})

		Convey("when everything succeeds", func() {
			mockModerationDAO.On("GetModerationsByEntryKey", mock.Anything, mock.Anything, mock.Anything).
				Return([]moderation.Moderation{
					{
						LeaderboardSetID: "test-lb-set-id",
						EntryKey:         "12345",
					},
				}, &moderation.GetModerationsByEntryKeyCursor{
					LeaderboardSetID: "abc",
					LastUpdated:      "1-2-2020",
				}, nil).Once()
			output, err := testHandler.Handle(ctx, testInput)

			So(err, ShouldBeNil)
			mockModerationDAO.AssertNumberOfCalls(t, "GetModerationsByEntryKey", 1)
			So(len(output.Moderations), ShouldEqual, 1)
			So(output.Domain, ShouldEqual, "bits")
			So(output.LeaderboardSetId, ShouldEqual, "abc")
		})
	})
}
