package query_moderations_by_entry_key

import (
	"context"
	"math/rand"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/pkg/errors"
)

const (
	query_limit = 100
)

type Input struct {
	Domain           identifier.Domain `json:"domain"`
	EntryKey         entry.Key         `json:"entry_key"`
	LeaderboardSetId string            `json:"leaderboard_set_id"`
	LastUpdated      string            `json:"last_updated"`
}

type Output struct {
	Domain           identifier.Domain       `json:"domain"`
	EntryKey         entry.Key               `json:"entry_key"`
	Moderations      []moderation.Moderation `json:"moderations"`
	LeaderboardSetId string                  `json:"leaderboard_set_id"`
	LastUpdated      string                  `json:"last_updated"`
	WaitSeconds      int                     `json:"wait_seconds"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (Output, error)
}

type handler struct {
	Landlord tenant.Landlord `inject:""`
}

func NewHandler() Handler {
	return &handler{}
}

func (h handler) Handle(ctx context.Context, input Input) (Output, error) {
	if string(input.Domain) == "" || string(input.EntryKey) == "" {
		return Output{}, errors.Errorf("invalid input: %v", input)
	}

	var cursor *moderation.GetModerationsByEntryKeyCursor
	if input.LeaderboardSetId != "" && input.LastUpdated != "" {
		cursor = &moderation.GetModerationsByEntryKeyCursor{
			LeaderboardSetID: input.LeaderboardSetId,
			LastUpdated:      input.LastUpdated,
		}
	}

	moderations, nextCursor, err := h.Landlord.GetTenant(input.Domain).
		ModerationDAO.GetModerationsByEntryKey(string(input.EntryKey), query_limit, cursor)
	if err != nil {
		logrus.WithError(err).Error("Failed calling GetModerationByEntryKey")
		return Output{}, err
	}

	var newLeaderboardSetId, newLastUpdated string
	if nextCursor != nil {
		newLeaderboardSetId = nextCursor.LeaderboardSetID
		newLastUpdated = nextCursor.LastUpdated
	}

	// Random number between 0 and 100
	rand.Seed(time.Now().UnixNano())
	waitSeconds := rand.Intn(100)

	return Output{
		Domain:           input.Domain,
		EntryKey:         input.EntryKey,
		Moderations:      moderations,
		LeaderboardSetId: newLeaderboardSetId,
		LastUpdated:      newLastUpdated,
		WaitSeconds:      waitSeconds,
	}, nil
}
