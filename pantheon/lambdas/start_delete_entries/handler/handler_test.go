package start_delete_entries

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	sfn_workers "code.justin.tv/commerce/pantheon/backend/sfn"
	sqs_client "code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	sfn_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sfn"
	sqs_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sqs"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a handler", t, func() {
		sfnClient := new(sfn_mock.Client)
		sqsClient := new(sqs_mock.Client)
		tenantClient := new(tenant_mock.Landlord)
		sfnARN := "sfnARN"
		cfg := &config.Config{
			Privacy: config.Privacy{
				DeleteEntriesStepFunctionConfig: &config.StepFunctionConfig{
					ARN: sfnARN,
				},
			},
		}

		h := &handler{
			SQSClient: sqsClient,
			SFNClient: sfnClient,
			Tenants:   tenantClient,
			Config:    cfg,
		}

		domain := "test-domain"
		entryKey := "test-entry-key"
		tenantSQSQueueURL := "queue-url"
		ctx := context.Background()
		tenantClient.On("GetTenant", identifier.Domain(domain)).Return(tenant.Tenant{
			Config: config.TenantConfig{
				PDMSDeleteEntriesQueueConfig: &config.QueueConfig{
					URL: tenantSQSQueueURL,
				},
			},
		})

		queueMessage := sfn_workers.DeleteEntriesStateMachineInput{
			Domain:      domain,
			EntryKey:    entryKey,
			WaitSeconds: 123,
		}
		queueMessageJSON, _ := json.Marshal(queueMessage)
		sqsMessage := &sqs.Message{
			Body: aws.String(string(queueMessageJSON)),
		}
		sqsMessages := []*sqs.Message{
			sqsMessage,
		}

		Convey("returns error", func() {
			Convey("when we fail to read messages from the queue", func() {
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to delete messages off the queue", func() {
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return(sqsMessages, nil).Once()
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()
				sfnClient.On("Execute", ctx, sfnARN, mock.Anything, queueMessage).Return(nil)
				sqsClient.On("DeleteMessages", ctx, tenantSQSQueueURL, []*sqs.Message{sqsMessage}).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := h.Handle(ctx, Input{
				Tenant: domain,
			})

			So(err, ShouldNotBeNil)
			So(sqsClient.AssertExpectations(t), ShouldBeTrue)
			So(sfnClient.AssertExpectations(t), ShouldBeTrue)
		})

		Convey("returns success", func() {
			var entriesDeletedCount int
			var entriesDeletedMapLength int
			var entriesToRetryCount int

			Convey("when no messages are in the queue", func() {
				entriesDeletedCount = 0
				entriesDeletedMapLength = 0
				entriesToRetryCount = 0

				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil)
			})

			Convey("when messages in the queue start a sfn execution", func() {
				entriesDeletedCount = 1
				entriesDeletedMapLength = 1
				entriesToRetryCount = 0

				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return(sqsMessages, nil).Once()
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()
				sfnClient.On("Execute", ctx, sfnARN, mock.Anything, queueMessage).Return(nil)
				sqsClient.On("DeleteMessages", ctx, tenantSQSQueueURL, []*sqs.Message{sqsMessage}).Return(nil)
			})

			Convey("when the body of the message in the queue is empty", func() {
				entriesDeletedCount = 1
				entriesDeletedMapLength = 0
				entriesToRetryCount = 0

				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{
					{},
				}, nil).Once()
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()
				sqsClient.On("DeleteMessages", ctx, tenantSQSQueueURL, []*sqs.Message{{}}).Return(nil)
			})

			Convey("when messages in the queue have a sfn execution already started", func() {
				entriesDeletedCount = 1
				entriesDeletedMapLength = 1
				entriesToRetryCount = 0

				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return(sqsMessages, nil).Once()
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()
				sfnClient.On("Execute", ctx, sfnARN, mock.Anything, queueMessage).Return(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				sqsClient.On("DeleteMessages", ctx, tenantSQSQueueURL, []*sqs.Message{sqsMessage}).Return(nil)
			})

			Convey("when we fail to start the stepfn execution and need to not delete the message from the queue", func() {
				entriesDeletedCount = 0
				entriesDeletedMapLength = 0
				entriesToRetryCount = 1

				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return(sqsMessages, nil).Once()
				sqsClient.On("ReceiveMessages", ctx, tenantSQSQueueURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()
				sfnClient.On("Execute", ctx, sfnARN, mock.Anything, queueMessage).Return(errors.New("WALRUS STRIKE"))
			})

			output, err := h.Handle(ctx, Input{
				Tenant: domain,
			})

			So(err, ShouldBeNil)
			So(output.EntriesDeleted, ShouldHaveLength, entriesDeletedMapLength)
			So(output.EntriesDeletedCount, ShouldEqual, entriesDeletedCount)
			So(output.EntriesToRetry, ShouldHaveLength, entriesToRetryCount)
			So(output.EntriesToRetryCount, ShouldEqual, entriesToRetryCount)
			So(sqsClient.AssertExpectations(t), ShouldBeTrue)
			So(sfnClient.AssertExpectations(t), ShouldBeTrue)
		})
	})
}
