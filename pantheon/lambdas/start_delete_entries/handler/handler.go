package start_delete_entries

import (
	"context"
	"crypto/md5" //nolint //crypto just used for execution name hashing
	"encoding/base64"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	sfn_workers "code.justin.tv/commerce/pantheon/backend/sfn"
	sfn_client "code.justin.tv/commerce/pantheon/clients/sfn"
	sqs_client "code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Input struct {
	Tenant      string `json:"tenant"`
	MaxMessages int    `json:"max_messages"`
	IsSmokeTest bool   `json:"is_smoke_test"`
}

type Output struct {
	EntriesDeletedCount int               `json:"entries_deleted_count"`
	EntriesDeleted      map[string]string `json:"entries_delete"`
	EntriesToRetryCount int               `json:"entries_to_retry_count"`
	EntriesToRetry      []*sqs.Message    `json:"entries_to_retry"`
}

type Handler interface {
	Handle(ctx context.Context, input Input) (Output, error)
}

type handler struct {
	SQSClient sqs_client.Client `inject:""`
	SFNClient sfn_client.Client `inject:""`
	Tenants   tenant.Landlord   `inject:""`
	Config    *config.Config    `inject:""`
}

func NewHandler() *handler {
	return &handler{}
}

func (h *handler) Handle(ctx context.Context, input Input) (Output, error) {
	sqsURL := h.Tenants.GetTenant(identifier.Domain(input.Tenant)).Config.PDMSDeleteEntriesQueueConfig.URL

	var messages []*sqs.Message

	retry := true
	for retry {
		batch, err := h.SQSClient.ReceiveMessages(ctx, sqsURL, sqs_client.MaxReceivedMessage)
		if err != nil {
			return Output{}, err
		}

		messages = append(messages, batch...)
		if input.MaxMessages > 0 && len(messages) >= input.MaxMessages {
			break
		}
		retry = len(batch) != 0 // exit on first empty receive
	}

	messagesToDelete := make([]*sqs.Message, 0)
	executionToEntryKeyMapping := map[string]string{}
	messagesToRetry := make([]*sqs.Message, 0)

	for _, msg := range messages {
		if msg.Body == nil {
			// we should just get this message off the queue since it's invalid
			messagesToDelete = append(messagesToDelete, msg)
			continue
		}

		var req sfn_workers.DeleteEntriesStateMachineInput
		err := json.Unmarshal([]byte(*msg.Body), &req)
		if err != nil {
			// we should just get this message off the queue since it's not a valid format
			messagesToDelete = append(messagesToDelete, msg)
			continue
		}

		log := logrus.WithFields(logrus.Fields{
			"domain":   req.Domain,
			"entryKey": req.EntryKey,
		})

		stateMachineARN := h.Config.Privacy.DeleteEntriesStepFunctionConfig.ARN
		executionName, err := getDeleteEntriesExecutionName(req.Domain, req.EntryKey)
		if err != nil {
			log.WithError(err).Error("Failed to create step function execution name")
			messagesToRetry = append(messagesToRetry, msg)
			continue
		}

		err = h.SFNClient.Execute(ctx, stateMachineARN, executionName, req)
		if err != nil {
			if awsErr, ok := err.(awserr.Error); ok {
				// Our execution name ensures idempotent executions, so if we see this error we do not want to error
				if awsErr.Code() != sfn.ErrCodeExecutionAlreadyExists {
					log.WithError(err).Error("failed to execute step function, aws error")
					messagesToRetry = append(messagesToRetry, msg)
					continue
				}
			} else {
				logrus.WithError(err).Error("failed to execute step function, non aws error")
				messagesToRetry = append(messagesToRetry, msg)
				continue
			}
		}

		messagesToDelete = append(messagesToDelete, msg)
		executionToEntryKeyMapping[fmt.Sprintf("%s.%s", req.Domain, req.EntryKey)] = stateMachineARN
	}

	output := Output{
		EntriesDeletedCount: len(messagesToDelete),
		EntriesToRetryCount: len(messagesToRetry),
		EntriesDeleted:      executionToEntryKeyMapping,
		EntriesToRetry:      messagesToRetry,
	}

	var err error
	if len(messagesToDelete) > 0 {
		err = h.SQSClient.DeleteMessages(ctx, sqsURL, messagesToDelete)
	}

	return output, err
}

func getDeleteEntriesExecutionName(domain string, entryKey string) (string, error) {
	hasher := md5.New() //nolint //crypto just used for execution name hashing
	_, err := hasher.Write([]byte(entryKey))
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s.%s.%d", domain, base64.URLEncoding.EncodeToString(hasher.Sum(nil)), time.Now().UnixNano()), nil
}
