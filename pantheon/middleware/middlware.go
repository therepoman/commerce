package middleware

import (
	"context"
	"net/http"
)

const (
	ipAddressTempKey = "X-Forwarded-For-Temp"
	ipAddressKey     = "X-Forwarded-For"
	clientIDKey      = "Client-ID"
	userIDKey        = "User-ID"
)

func WithRequestInfo(base http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		// TODO: The X-Forwarded-For header is currently broken in GQL. Stop reading the temp
		// 	header once it is fixed.
		// 	Context: https://twitch.slack.com/archives/C2WGRCVPY/p1586475699277200
		ipAddress := r.Header.Get(ipAddressTempKey)
		if ipAddress == "" {
			ipAddress = r.Header.Get(ipAddressKey)
		}

		// record orginating IP address
		ctx = context.WithValue(ctx, ipAddressKey, ipAddress)

		// record client ID
		ctx = context.WithValue(ctx, clientIDKey, r.Header.Get(clientIDKey))

		// record requesting user ID
		ctx = context.WithValue(ctx, userIDKey, r.Header.Get(userIDKey))

		base.ServeHTTP(w, r.WithContext(ctx))
	})
}

func IPAddress(ctx context.Context) (string, bool) {
	address, ok := ctx.Value(ipAddressKey).(string)
	return address, ok
}

func ClientID(ctx context.Context) (string, bool) {
	id, ok := ctx.Value(clientIDKey).(string)
	return id, ok
}

func UserID(ctx context.Context) (string, bool) {
	id, ok := ctx.Value(userIDKey).(string)
	return id, ok
}
