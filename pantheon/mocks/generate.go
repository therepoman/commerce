package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// Tenant Client
//go:generate retool do mockery -name=Landlord -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/clients/tenant -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant -outpkg=tenant_mock

// SFN Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/clients/sfn -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sfn -outpkg=sfn_mock

// Redis Leaderboard Factory
//go:generate retool do mockery -name=LeaderboardFactory -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/redis -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis  -outpkg=redis_mock

// Redis Leaderboard Factory
//go:generate retool do mockery -name=PDMSLeaderboardFactory -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/redis -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis  -outpkg=redis_mock

// Redis Index Factory
//go:generate retool do mockery -name=GroupingKeyIndexFactory -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/redis -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis  -outpkg=redis_mock

// Redis Index Factory
//go:generate retool do mockery -name=Index -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/index -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/index -outpkg=index_mock

// Domain Config Manager
//go:generate retool do mockery -name=ConfigManager -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/domain -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain -outpkg=domain_mock

// Leaderboard
//go:generate retool do mockery -name=Leaderboard -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard -outpkg=leaderboard_mock

// Redis Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/clients/redis -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/redis -outpkg=redis_mock

// S3 Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/clients/s3 -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/s3 -outpkg=s3_mock

// Persister
//go:generate retool do mockery -name=Persister -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/persistence -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/persistence -outpkg=persistence_mock

// Topper
//go:generate retool do mockery -name=Topper -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/observers -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/observers -outpkg=observer_mock

// ConfigManager
//go:generate retool do mockery -name=ConfigManager -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard/domain -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain -outpkg=domain_mock

// CloudWatchAPI
//go:generate retool do mockery -name=CloudWatchAPI -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/vendor/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface -outpkg=cloudwatchiface_mock

// GetBucketedLeaderboardCountsAPI
//go:generate retool do mockery -name=GetBucketedLeaderboardCountsAPI -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/api -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api -outpkg=api_mock

// GetLeaderboardEntriesAboveThresholdAPI
//go:generate retool do mockery -name=GetLeaderboardEntriesAboveThresholdAPI -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/api -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api -outpkg=api_mock

// DeleteEntriesAPI
//go:generate retool do mockery -name=DeleteEntriesAPI -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/api -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api -outpkg=api_mock

// Leaderboard
//go:generate retool do mockery -name=Factory -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/leaderboard -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard -outpkg=leaderboard_mock

// SQS Client
//go:generate retool do mockery -name=SQSAPI -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/vendor/github.com/aws/aws-sdk-go/service/sqs/sqsiface -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface -outpkg=sqsiface_mock

// SQS Client Wrapper
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/clients/sqs -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sqs -outpkg=sqs_mock

// CloudWatch Client
//go:generate retool do mockery -name=CloudWatchAPI -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/vendor/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface -outpkg=cloudwatchiface_mock

// Statter Client
//go:generate retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/vendor/github.com/cactus/go-statsd-client/statsd -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/github.com/cactus/go-statsd-client/statsd -outpkg=statter_mock

// Event DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/dynamo/event -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/event -outpkg=event_mock

// Moderation DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/backend/dynamo/moderation -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/dynamo/moderation -outpkg=moderation_mock

// User Service Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pantheon/clients/users -output=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/users -outpkg=users_mock
