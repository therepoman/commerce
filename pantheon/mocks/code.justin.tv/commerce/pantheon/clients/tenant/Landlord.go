// Code generated by mockery v1.0.0. DO NOT EDIT.

package tenant_mock

import (
	identifier "code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	mock "github.com/stretchr/testify/mock"

	tenant "code.justin.tv/commerce/pantheon/clients/tenant"
)

// Landlord is an autogenerated mock type for the Landlord type
type Landlord struct {
	mock.Mock
}

// GetDefaultTenant provides a mock function with given fields:
func (_m *Landlord) GetDefaultTenant() tenant.Tenant {
	ret := _m.Called()

	var r0 tenant.Tenant
	if rf, ok := ret.Get(0).(func() tenant.Tenant); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(tenant.Tenant)
	}

	return r0
}

// GetTenant provides a mock function with given fields: domain
func (_m *Landlord) GetTenant(domain identifier.Domain) tenant.Tenant {
	ret := _m.Called(domain)

	var r0 tenant.Tenant
	if rf, ok := ret.Get(0).(func(identifier.Domain) tenant.Tenant); ok {
		r0 = rf(domain)
	} else {
		r0 = ret.Get(0).(tenant.Tenant)
	}

	return r0
}

// GetTenants provides a mock function with given fields:
func (_m *Landlord) GetTenants() map[string]tenant.Tenant {
	ret := _m.Called()

	var r0 map[string]tenant.Tenant
	if rf, ok := ret.Get(0).(func() map[string]tenant.Tenant); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]tenant.Tenant)
		}
	}

	return r0
}
