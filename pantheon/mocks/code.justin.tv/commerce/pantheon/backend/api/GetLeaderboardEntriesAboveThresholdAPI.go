// Code generated by mockery v1.0.0. DO NOT EDIT.

package api_mock

import (
	context "context"

	entry "code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	mock "github.com/stretchr/testify/mock"

	pantheonrpc "code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

// GetLeaderboardEntriesAboveThresholdAPI is an autogenerated mock type for the GetLeaderboardEntriesAboveThresholdAPI type
type GetLeaderboardEntriesAboveThresholdAPI struct {
	mock.Mock
}

// GetLeaderboardEntriesAboveThreshold provides a mock function with given fields: ctx, req
func (_m *GetLeaderboardEntriesAboveThresholdAPI) GetLeaderboardEntriesAboveThreshold(ctx context.Context, req *pantheonrpc.GetLeaderboardEntriesAboveThresholdReq) ([]entry.RankedEntry, error) {
	ret := _m.Called(ctx, req)

	var r0 []entry.RankedEntry
	if rf, ok := ret.Get(0).(func(context.Context, *pantheonrpc.GetLeaderboardEntriesAboveThresholdReq) []entry.RankedEntry); ok {
		r0 = rf(ctx, req)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]entry.RankedEntry)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *pantheonrpc.GetLeaderboardEntriesAboveThresholdReq) error); ok {
		r1 = rf(ctx, req)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
