// Code generated by mockery v1.0.0. DO NOT EDIT.

package leaderboard_mock

import (
	context "context"

	entry "code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	event "code.justin.tv/commerce/pantheon/backend/leaderboard/event"

	leaderboard "code.justin.tv/commerce/pantheon/backend/leaderboard"

	mock "github.com/stretchr/testify/mock"

	score "code.justin.tv/commerce/pantheon/backend/leaderboard/score"

	time "time"
)

// Leaderboard is an autogenerated mock type for the Leaderboard type
type Leaderboard struct {
	mock.Mock
}

// ClearScores provides a mock function with given fields:
func (_m *Leaderboard) ClearScores() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteEntry provides a mock function with given fields: key
func (_m *Leaderboard) DeleteEntry(key entry.Key) error {
	ret := _m.Called(key)

	var r0 error
	if rf, ok := ret.Get(0).(func(entry.Key) error); ok {
		r0 = rf(key)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Destroy provides a mock function with given fields:
func (_m *Leaderboard) Destroy() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCountForRange provides a mock function with given fields: ctx, min, max, rs
func (_m *Leaderboard) GetCountForRange(ctx context.Context, min *int64, max *int64, rs leaderboard.RetrievalSettings) (int64, error) {
	ret := _m.Called(ctx, min, max, rs)

	var r0 int64
	if rf, ok := ret.Get(0).(func(context.Context, *int64, *int64, leaderboard.RetrievalSettings) int64); ok {
		r0 = rf(ctx, min, max, rs)
	} else {
		r0 = ret.Get(0).(int64)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *int64, *int64, leaderboard.RetrievalSettings) error); ok {
		r1 = rf(ctx, min, max, rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetEntriesAboveThreshold provides a mock function with given fields: threshold, limit, offset, rs
func (_m *Leaderboard) GetEntriesAboveThreshold(threshold int64, limit int64, offset int64, rs leaderboard.RetrievalSettings) ([]entry.RankedEntry, error) {
	ret := _m.Called(threshold, limit, offset, rs)

	var r0 []entry.RankedEntry
	if rf, ok := ret.Get(0).(func(int64, int64, int64, leaderboard.RetrievalSettings) []entry.RankedEntry); ok {
		r0 = rf(threshold, limit, offset, rs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]entry.RankedEntry)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int64, int64, int64, leaderboard.RetrievalSettings) error); ok {
		r1 = rf(threshold, limit, offset, rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetEntryContext provides a mock function with given fields: ctx, key, spread, rs
func (_m *Leaderboard) GetEntryContext(ctx context.Context, key entry.Key, spread int, rs leaderboard.RetrievalSettings) (entry.RankedEntry, []entry.RankedEntry, error) {
	ret := _m.Called(ctx, key, spread, rs)

	var r0 entry.RankedEntry
	if rf, ok := ret.Get(0).(func(context.Context, entry.Key, int, leaderboard.RetrievalSettings) entry.RankedEntry); ok {
		r0 = rf(ctx, key, spread, rs)
	} else {
		r0 = ret.Get(0).(entry.RankedEntry)
	}

	var r1 []entry.RankedEntry
	if rf, ok := ret.Get(1).(func(context.Context, entry.Key, int, leaderboard.RetrievalSettings) []entry.RankedEntry); ok {
		r1 = rf(ctx, key, spread, rs)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).([]entry.RankedEntry)
		}
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, entry.Key, int, leaderboard.RetrievalSettings) error); ok {
		r2 = rf(ctx, key, spread, rs)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetEntryContextWithTimeoutAndRetry provides a mock function with given fields: ctx, key, spread, rs, timeout, maxAttempts
func (_m *Leaderboard) GetEntryContextWithTimeoutAndRetry(ctx context.Context, key entry.Key, spread int, rs leaderboard.RetrievalSettings, timeout time.Duration, maxAttempts int) (entry.RankedEntry, []entry.RankedEntry, error) {
	ret := _m.Called(ctx, key, spread, rs, timeout, maxAttempts)

	var r0 entry.RankedEntry
	if rf, ok := ret.Get(0).(func(context.Context, entry.Key, int, leaderboard.RetrievalSettings, time.Duration, int) entry.RankedEntry); ok {
		r0 = rf(ctx, key, spread, rs, timeout, maxAttempts)
	} else {
		r0 = ret.Get(0).(entry.RankedEntry)
	}

	var r1 []entry.RankedEntry
	if rf, ok := ret.Get(1).(func(context.Context, entry.Key, int, leaderboard.RetrievalSettings, time.Duration, int) []entry.RankedEntry); ok {
		r1 = rf(ctx, key, spread, rs, timeout, maxAttempts)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).([]entry.RankedEntry)
		}
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, entry.Key, int, leaderboard.RetrievalSettings, time.Duration, int) error); ok {
		r2 = rf(ctx, key, spread, rs, timeout, maxAttempts)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetRank provides a mock function with given fields: key, rs
func (_m *Leaderboard) GetRank(key entry.Key, rs leaderboard.RetrievalSettings) (entry.Rank, error) {
	ret := _m.Called(key, rs)

	var r0 entry.Rank
	if rf, ok := ret.Get(0).(func(entry.Key, leaderboard.RetrievalSettings) entry.Rank); ok {
		r0 = rf(key, rs)
	} else {
		r0 = ret.Get(0).(entry.Rank)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(entry.Key, leaderboard.RetrievalSettings) error); ok {
		r1 = rf(key, rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetScore provides a mock function with given fields: key, rs
func (_m *Leaderboard) GetScore(key entry.Key, rs leaderboard.RetrievalSettings) (score.Score, error) {
	ret := _m.Called(key, rs)

	var r0 score.Score
	if rf, ok := ret.Get(0).(func(entry.Key, leaderboard.RetrievalSettings) score.Score); ok {
		r0 = rf(key, rs)
	} else {
		r0 = ret.Get(0).(score.Score)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(entry.Key, leaderboard.RetrievalSettings) error); ok {
		r1 = rf(key, rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTop provides a mock function with given fields: ctx, n, rs
func (_m *Leaderboard) GetTop(ctx context.Context, n int, rs leaderboard.RetrievalSettings) ([]entry.RankedEntry, error) {
	ret := _m.Called(ctx, n, rs)

	var r0 []entry.RankedEntry
	if rf, ok := ret.Get(0).(func(context.Context, int, leaderboard.RetrievalSettings) []entry.RankedEntry); ok {
		r0 = rf(ctx, n, rs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]entry.RankedEntry)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, int, leaderboard.RetrievalSettings) error); ok {
		r1 = rf(ctx, n, rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTopWithTimeoutAndRetry provides a mock function with given fields: ctx, n, rs, timeout, maxAttempts
func (_m *Leaderboard) GetTopWithTimeoutAndRetry(ctx context.Context, n int, rs leaderboard.RetrievalSettings, timeout time.Duration, maxAttempts int) ([]entry.RankedEntry, error) {
	ret := _m.Called(ctx, n, rs, timeout, maxAttempts)

	var r0 []entry.RankedEntry
	if rf, ok := ret.Get(0).(func(context.Context, int, leaderboard.RetrievalSettings, time.Duration, int) []entry.RankedEntry); ok {
		r0 = rf(ctx, n, rs, timeout, maxAttempts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]entry.RankedEntry)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, int, leaderboard.RetrievalSettings, time.Duration, int) error); ok {
		r1 = rf(ctx, n, rs, timeout, maxAttempts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Len provides a mock function with given fields: rs
func (_m *Leaderboard) Len(rs leaderboard.RetrievalSettings) (int, error) {
	ret := _m.Called(rs)

	var r0 int
	if rf, ok := ret.Get(0).(func(leaderboard.RetrievalSettings) int); ok {
		r0 = rf(rs)
	} else {
		r0 = ret.Get(0).(int)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(leaderboard.RetrievalSettings) error); ok {
		r1 = rf(rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ModerateEntry provides a mock function with given fields: key
func (_m *Leaderboard) ModerateEntry(key entry.Key) error {
	ret := _m.Called(key)

	var r0 error
	if rf, ok := ret.Get(0).(func(entry.Key) error); ok {
		r0 = rf(key)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Restore provides a mock function with given fields: bs
func (_m *Leaderboard) Restore(bs []byte) error {
	ret := _m.Called(bs)

	var r0 error
	if rf, ok := ret.Get(0).(func([]byte) error); ok {
		r0 = rf(bs)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Serialize provides a mock function with given fields: rs
func (_m *Leaderboard) Serialize(rs leaderboard.RetrievalSettings) ([]byte, error) {
	ret := _m.Called(rs)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(leaderboard.RetrievalSettings) []byte); ok {
		r0 = rf(rs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(leaderboard.RetrievalSettings) error); ok {
		r1 = rf(rs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Type provides a mock function with given fields:
func (_m *Leaderboard) Type() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// UnmoderateEntry provides a mock function with given fields: key
func (_m *Leaderboard) UnmoderateEntry(key entry.Key) error {
	ret := _m.Called(key)

	var r0 error
	if rf, ok := ret.Get(0).(func(entry.Key) error); ok {
		r0 = rf(key)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UpdateScore provides a mock function with given fields: key, _a1, _a2
func (_m *Leaderboard) UpdateScore(key entry.Key, _a1 score.EntryScore, _a2 *event.Event) error {
	ret := _m.Called(key, _a1, _a2)

	var r0 error
	if rf, ok := ret.Get(0).(func(entry.Key, score.EntryScore, *event.Event) error); ok {
		r0 = rf(key, _a1, _a2)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
