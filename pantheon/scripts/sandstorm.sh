#!/bin/bash

set -e

ACCOUNT=$1

case "$ACCOUNT" in
        production)
            ENVIRONMENTS=(prod)
            ;;
        development)
            ENVIRONMENTS=(dev staging)
            ;;
        *)
            echo "Unknown account \"$ACCOUNT\", exiting"
            exit 1
esac

for ENVIRONMENT in "${ENVIRONMENTS[@]}"
do
    ROLE=`terraform output -state=./terraform/${ACCOUNT}/.terraform/terraform.tfstate ${ENVIRONMENT}-iam_role_arn`

    tcs sandstorm role-flags \
        --owner team-commerce \
        --name commerce-${ENVIRONMENT}-pantheon \
        --secret_key "commerce/pantheon/${ENVIRONMENT}/*" \
        --allowed_arn "${ROLE}" \
        --output ./sandstorm.d/${ACCOUNT}_${ENVIRONMENT}.json

done
