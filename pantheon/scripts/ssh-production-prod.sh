#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile pantheon-prod prod-commerce-pantheon-env
popd deploy
