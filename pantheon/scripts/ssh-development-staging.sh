#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile pantheon-devo staging-commerce-pantheon-env
popd deploy
