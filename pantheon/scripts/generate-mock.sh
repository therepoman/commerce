#!/bin/bash

dir=$GOPATH/src/$1
iname=$2
outdir=$GOPATH/src/code.justin.tv/commerce/pantheon/mocks/$1
outpackage=$3_mock

retool do mockery -name=$iname -dir=$dir -output=$outdir -outpkg=$outpackage
