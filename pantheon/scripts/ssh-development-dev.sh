#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile pantheon-devo dev-commerce-pantheon-env
popd deploy
