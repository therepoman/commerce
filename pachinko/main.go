package main

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"syscall"
	"time"

	twitchlogging "code.justin.tv/amzn/TwitchLogging"
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchProcessIdentifier/expvars"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/amzn/TwitchS2SLegacyMigrationMiddleware/twitchs2smigration"
	"code.justin.tv/chat/ecsgomaxprocs"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend"
	"code.justin.tv/commerce/pachinko/backend/audit"
	"code.justin.tv/commerce/pachinko/backend/clients/cloudwatchlogger"
	s2s2client "code.justin.tv/commerce/pachinko/backend/clients/s2s2"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/common/golibs/bininfo"
	s2sevents "code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	"code.justin.tv/video/autoprof/profs3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/codegangsta/cli"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	goji "goji.io"
	"goji.io/pat"
)

var environment string
var tenantDomain string
var isCanary bool

const shutdownTimeout = 1 * time.Minute

func main() {
	app := cli.NewApp()
	app.Name = "Pachinko"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
			EnvVar:      config.EnvironmentEnvironmentVariable,
		},
		cli.StringFlag{
			Name:        "tenant, t",
			Usage:       "Tenant identifier to use for running commands.",
			Destination: &tenantDomain,
			EnvVar:      config.DomainTenantEnvironmentVariable,
		},
		cli.BoolFlag{
			Name:        "canary, c",
			Usage:       "Boolean flag to determine if it's the canary",
			EnvVar:      config.CanaryEnvironmentVariable,
			Destination: &isCanary,
		},
	}

	app.Action = runPachinko

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

// TODO: if you remove the maxprocs stuff, remove this struct. If kept, move it to it's own file.
type maxProcsLogger struct{}

func (m *maxProcsLogger) Log(keyvals ...interface{}) {
	logrus.Println(keyvals...)
}

func runPachinko(c *cli.Context) {
	env := config.GetOrDefaultEnvironment(environment)

	rand.Seed(time.Now().UnixNano())

	f := ecsgomaxprocs.Fixer{
		Log: &maxProcsLogger{},
	}
	if err := f.Exec(); err != nil {
		logrus.WithError(err).Error("Failed to get go max procs, but continuing...")
	}

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	domain := cfg.Tenants.GetDomainByIdentifier(tenantDomain)
	if _, ok := pachinko.Domain_name[int32(domain)]; !ok {
		logrus.Panic("Invalid Domain passed to Lambda Environment")
	}

	cfg.TenantDomain = domain
	cfg.IsCanary = isCanary

	logrus.Infof("Loaded config {env: %s} {tenant: %s} {canary: %t}", cfg.Environment.Name, cfg.TenantDomain, cfg.IsCanary)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error initializing backend")
	}

	telemetryMiddleware, telemetryObserver, err := metrics.NewTwirpTelemetryHook("pachinko", cfg.Environment.AWSRegion, cfg.GetEnvironmentMetricName(), tenantDomain)
	if err != nil {
		logrus.Fatal(err)
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.Statter()), telemetryMiddleware)
	twirpHandler := pachinko.NewPachinkoServer(be.TwirpServer(), twirpStatsHook)
	defer func() {
		telemetryObserver.Stop()
		err := be.Statter().Close()
		if err != nil {
			logrus.WithError(err).Error("could not close statter")
		}
	}()

	pid := identifier.ProcessIdentifier{
		Service:  "pachinko",
		Stage:    cfg.GetEnvironmentMetricName(),
		Substage: cfg.TenantDomain.String(),
		// Region env var provided by ECS
		Region: os.Getenv("AWS_DEFAULT_REGION"),
		// Task id (obtained from task ARN)
		Machine:  os.Getenv("SERVICE_TASK_ID"),
		LaunchID: os.Getenv("HOSTNAME"),
		Version:  bininfo.Revision(),
	}
	expvars.Publish(&pid)

	// S2Sv0 client
	s2sv0Enabled := cfg.Auth != nil && cfg.Auth.S2S != nil && cfg.GetTenant().S2SEnabled
	var s2sCalleeClient *callee.Client
	if s2sv0Enabled {
		eventWriterEnvironment := "testing"
		if environment == string(config.Prod) {
			eventWriterEnvironment = "production"
		}

		eventsWriterClient, err := s2sevents.NewEventLogger(s2sevents.Config{
			Environment: eventWriterEnvironment,
		}, logrus.New())
		if err != nil {
			logrus.WithError(err).Panic("Error initializing Event logger client")
		}

		s2sCalleeClient = &callee.Client{
			ServiceName: cfg.GetS2SName(),
			Config: &callee.Config{
				DisableStatsClient: true,
			},
			EventsWriterClient: eventsWriterClient,
			Logger:             logrus.New(),
		}

		err = s2sCalleeClient.Start()
		if err != nil {
			logrus.WithError(err).Panic("Error initializing S2S client")
		}
	}

	// S2S2 client
	s2s2Enabled := cfg.Auth != nil && cfg.Auth.S2S != nil && cfg.GetTenant().S2S2Enabled
	s2s2logger := cloudwatchlogger.NewCloudWatchLogNoopClient()
	var adaptedLogger twitchlogging.Logger
	var s2s2Client *s2s2.S2S2
	if s2s2Enabled {
		logGroupName := cfg.GetTenant().S2SAuthLogGroupName
		s2s2logger, err = cloudwatchlogger.NewCloudWatchLogClient(cfg.Environment.AWSRegion, logGroupName)
		if err != nil {
			logrus.WithError(err).Panic("Error initializing cloudwatch logger for S2S2")
		}
		adaptedLogger = cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, logGroupName, cfg.GetTenant().ResourceIdentifier)
		s2s2Client, err = s2s2client.NewS2S2Client(cfg, adaptedLogger)
		if err != nil {
			logrus.WithError(err).Panic("Error initializing S2S2 client")
		}
	}

	var handler http.Handler
	// S2S2 Migration (reorganize when S2Sv0 is off-boarded)
	if s2sv0Enabled && s2s2Enabled {
		migration, err := twitchs2smigration.New(
			s2sCalleeClient,
			s2s2Client,
			adaptedLogger,
		)
		if err != nil {
			logrus.WithError(err).Panic("Error initializing S2S Migration Middleware")
		}

		var s2sCallers []twitchs2smigration.Service
		for _, serviceName := range cfg.GetTenant().S2SCallers {
			service, err := migration.Service(serviceName)
			if err != nil {
				logrus.WithField("serviceName", serviceName).WithError(err).Panic("Error initializing service for S2S2 migration")
			}
			logrus.WithField("service", service).Info("initialized service for S2S2 migration")
			s2sCallers = append(s2sCallers, service)
		}
		handler = migration.RequireAuthentication(twirpHandler, &twitchs2smigration.RequireAuthenticationOptions{
			AuthorizedCallers: s2sCallers,
		})
	} else if s2sv0Enabled {
		handler = s2sCalleeClient.RequestValidatorMiddleware(twirpHandler)
	} else if s2s2Enabled {
		if cfg.GetTenant().S2S2DisablePassthrough {
			handler = s2s2Client.RequireAuthentication(twirpHandler)
		} else {
			handler = s2s2Client.RequireAuthentication(twirpHandler).RecordMetricsOnly()
		}
	} else {
		handler = twirpHandler
	}

	handler = audit.NewMuxWrapper(handler, tenantDomain)

	go func() {
		sess, err := session.NewSession(&aws.Config{
			Region: aws.String(cfg.Environment.AWSRegion),
		})
		if err != nil {
			logrus.WithError(err).Error("autoprof: Error starting aws session")
			return
		}

		autoprofBucket := getAutoprofBucketName(cfg)
		autoprofCollector := profs3.Collector{
			S3:       s3.New(sess),
			S3Bucket: autoprofBucket,
			OnError: func(err error) error {
				logrus.WithError(err).Error("autoprof profs3.Collector.Run error")
				return err
			},
		}
		err = autoprofCollector.Run(context.Background())
		if err != nil {
			logrus.WithError(err).Error("autoprof: Error starting autoprof collector")
			return
		}
		logrus.Infof("autoprof: Started autoprof in bucket %s", autoprofBucket)
	}()

	mux := goji.NewMux()

	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(pachinko.PachinkoPathPrefix+"*"), handler)

	goji_graceful.HandleSignals()
	goji_graceful.AddSignal(syscall.SIGTERM)
	goji_graceful.AddSignal(syscall.SIGKILL)
	goji_graceful.PreHook(func() {
		logrus.Info("Initiated shutdown process")
	})

	logrus.Info("service starting on 8000")
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		logrus.WithError(err).Error("Mux listen and serve error")
	}

	goji_graceful.Wait()

	shutdownStart := time.Now()
	shutdownContext, shutdownContextCancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer shutdownContextCancel()

	be.Shutdown(shutdownContext)
	s2s2logger.Shutdown()

	logrus.Infof("Finished shutting down in: %v", time.Since(shutdownStart))
}

func getAutoprofBucketName(cfg *config.Config) string {
	if cfg.IsCanary {
		return fmt.Sprintf("autoprof-%s-canary-%s-us-west-2", cfg.GetTenant().ResourceIdentifier, cfg.Environment.Name)
	}
	return fmt.Sprintf("autoprof-%s-%s-us-west-2", cfg.GetTenant().ResourceIdentifier, cfg.Environment.Name)
}
