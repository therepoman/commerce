package lambda_common

import (
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cenkalti/backoff"
)

func GetUniqueItemsFromMessages(sqsMessages []*sqs.Message) []string {
	var uniqueItems []string
	var set = make(map[string]struct{})
	duplicateCount := 0
	for _, sqsMessage := range sqsMessages {
		uniqueItem := *sqsMessage.Body
		if _, exists := set[uniqueItem]; !exists {
			uniqueItems = append(uniqueItems, uniqueItem)
			set[uniqueItem] = struct{}{}
		} else {
			duplicateCount++
		}
	}
	if duplicateCount > 0 {
		logrus.WithField("duplicateCount", duplicateCount).Info("Deduplicated Items Found")
	}

	return uniqueItems
}

func GetNewBackoff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = 500 * time.Millisecond
	exponetial.MaxElapsedTime = 5 * time.Second
	exponetial.Multiplier = 2
	return exponetial
}
