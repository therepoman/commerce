package backend

import (
	"time"

	"code.justin.tv/commerce/logrus"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/cache/balancelock"
	cache_tokens "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	cache_tx "code.justin.tv/commerce/pachinko/backend/cache/transaction"
	"code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp"
	"code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	redis_client "code.justin.tv/commerce/pachinko/backend/circuit/redis-client"
	"code.justin.tv/commerce/pachinko/backend/clients/lock"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	"code.justin.tv/commerce/pachinko/backend/clients/sfn"
	"code.justin.tv/commerce/pachinko/backend/clients/sqs"
	dynamo_tokens "code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	dynamo_tokens_scan "code.justin.tv/commerce/pachinko/backend/dynamo/tokens_scan"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	dynamo_tx_scan "code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan"
	"code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/backend/transactions"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/backend/clients/redshift"
	redshift_tx "code.justin.tv/commerce/pachinko/lambdas/backend/redshift/transactions"
	delete_tokens_by_group_id "code.justin.tv/commerce/pachinko/lambdas/functions/delete_tokens_by_group_id/handler"
	delete_tokens_by_owner_id "code.justin.tv/commerce/pachinko/lambdas/functions/delete_tokens_by_owner_id/handler"
	delete_transactions_by_group_id "code.justin.tv/commerce/pachinko/lambdas/functions/delete_transactions_by_group_id/handler"
	delete_transactions_by_owner_id "code.justin.tv/commerce/pachinko/lambdas/functions/delete_transactions_by_owner_id/handler"
	start_delete_by_group_id "code.justin.tv/commerce/pachinko/lambdas/functions/start_delete_by_group_id/handler"
	start_delete_by_owner_id "code.justin.tv/commerce/pachinko/lambdas/functions/start_delete_by_owner_id/handler"
	"code.justin.tv/commerce/splatter"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/pkg/errors"
)

type LambdaHandlers struct {
	DeleteTokensByOwnerId       *delete_tokens_by_owner_id.LambdaHandler       `inject:"delete_tokens_by_owner_id_lambda_handler"`
	DeleteTransactionsByOwnerId *delete_transactions_by_owner_id.LambdaHandler `inject:"delete_transactions_by_owner_id_lambda_handler"`
	DeleteTokensByGroupId       *delete_tokens_by_group_id.LambdaHandler       `inject:"delete_tokens_by_group_id_lambda_handler"`
	DeleteTransactionsByGroupId *delete_transactions_by_group_id.LambdaHandler `inject:"delete_transactions_by_group_id_lambda_handler"`
	StartDeleteByGroupId        *start_delete_by_group_id.LambdaHandler        `inject:"start_delete_by_group_id_lambda_handler"`
	StartDeleteByOwnerId        *start_delete_by_owner_id.LambdaHandler        `inject:"start_delete_by_owner_id_lambda_handler"`
}

type Backend interface {
	GetLambdaHandlers() *LambdaHandlers
}

type backend struct {
	// Config
	Config *config.Config `inject:""`

	// Lambdas
	LambdaHandlers *LambdaHandlers `inject:"lambda_handlers"`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetrics != nil {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.Environment.AWSRegion,
			ServiceName:       "pachinko",
			Stage:             cfg.Environment.Name,
			Substage:          cfg.TenantDomain.String(),
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	// create another session for us-east-2 to do dynamo scans
	ddbScanSess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegionDynamoScan),
	})
	if err != nil {
		return nil, err
	}

	hystrixMetrics := metrics.NewHystrixMetricsCollector()

	tenantTokensRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-tokens")
	tenantTxRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-txs")
	tenantTxTsRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-tx-ts")

	sfnClient, err := sfn.NewDefaultClient(cfg)
	if err != nil {
		return nil, err
	}

	sqsClient, err := sqs.NewDefaultClient(cfg)
	if err != nil {
		return nil, err
	}

	redshiftClient, err := redshift.NewClient(cfg, sess)
	if err != nil {
		return nil, err
	}

	statter := metrics.NewStatter()

	deps := []*inject.Object{
		// Backend (graph root)
		{Value: b},

		// Config
		{Value: cfg},

		// Clients
		{Value: redshiftClient},

		// Stats
		{Value: stats},
		{Value: statter},
		{Value: hystrixMetrics},

		// Clients
		{Value: sfnClient},
		{Value: sqsClient},
		{Value: lock.NewDynamoDBLockingClient()},

		// Redis
		{Value: redis.NewClient(tenantTokensRedisClient), Name: "redis-tokens"},
		{Value: redis.NewClient(tenantTxRedisClient), Name: "redis-txs"},
		{Value: redis.NewClient(tenantTxTsRedisClient), Name: "redis-tx-ts"},

		// Caches
		{Value: cache_tokens.NewCache()},
		{Value: cache_tx.NewCache()},
		{Value: balancelock.NewCache()},
		{Value: transaction_timestamp.NewCache()},

		// Dynamo
		{Value: worker_pool.NewScanWorkerPool()},

		// DAOs
		{Value: dynamo_tokens.NewDAOWithClient(dynamo.DynamoBatchWriteClient(sess))},
		{Value: dynamo_tx.NewDAOWithClient(dynamo.DynamoBatchWriteClient(sess))},
		{Value: dynamo_tx_scan.NewDAOWithClient(dynamo.DynamoScanClient(ddbScanSess))},
		{Value: dynamo_tokens_scan.NewDAOWithClient(dynamo.DynamoScanClient(ddbScanSess))},
		{Value: redshift_tx.NewDAOWithClient(redshiftClient, cfg, statter)},
		{Value: lock.NewDAOWithClient(dynamo.DynamoClient(sess))},

		// Lambdas
		{Value: &LambdaHandlers{}, Name: "lambda_handlers"},
		{Value: &delete_tokens_by_owner_id.LambdaHandler{}, Name: "delete_tokens_by_owner_id_lambda_handler"},
		{Value: &delete_transactions_by_owner_id.LambdaHandler{}, Name: "delete_transactions_by_owner_id_lambda_handler"},
		{Value: &delete_tokens_by_group_id.LambdaHandler{}, Name: "delete_tokens_by_group_id_lambda_handler"},
		{Value: &delete_transactions_by_group_id.LambdaHandler{}, Name: "delete_transactions_by_group_id_lambda_handler"},
		{Value: &start_delete_by_group_id.LambdaHandler{}, Name: "start_delete_by_group_id_lambda_handler"},
		{Value: &start_delete_by_owner_id.LambdaHandler{}, Name: "start_delete_by_owner_id_lambda_handler"},

		// Tokens config
		{Value: tokens.NewEntitler()},
		{Value: tokens.NewConsumer()},
		{Value: tokens.NewGetter()},
		{Value: tokens.NewFetcher()},
		{Value: tokens.NewStatusUpdater()},

		// Transactions config
		{Value: transactions.NewValidator()},
		{Value: transactions.NewConductor()},
		{Value: transactions.NewLocker()},
		{Value: transactions.NewExecutor()},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixMetrics.NewHystrixCommandMetricsCollector(name)
	})

	logrus.Info("Backend initialization complete")

	return b, nil
}

func (b *backend) GetLambdaHandlers() *LambdaHandlers {
	return b.LambdaHandlers
}
