package transactions

import (
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/backend/clients/redshift"
)

const (
	DeleteAllByOwnerIdCommandPrefix = "DELETE FROM transactions WHERE domain_name = '%s' AND owner_id IN"
	DeleteAllByGroupIdCommandPrefix = "DELETE FROM transactions WHERE domain_name = '%s' AND group_id IN"

	deleteByOwnerIdRedshiftLatency  = "delete-all-by-owner-id-transactions-redshift-latency"
	deleteByGroupIdRedshiftLatency  = "delete-all-by-group-id-transactions-redshift-latency"
	deleteByOwnerIdsRedshiftLatency = "delete-all-by-owner-ids-transactions-redshift-latency"
	deleteByGroupIdsRedshiftLatency = "delete-all-by-group-ids-transactions-redshift-latency"
)

type DAO interface {
	DeleteAllByOwnerId(ownerId string) error
	DeleteAllByGroupId(groupId string) error
	DeleteAllByOwnerIds(ownerIds []string) error
	DeleteAllByGroupIds(groupIds []string) error
}

type dao struct {
	RedshiftClient redshift.Client
	Config         *config.Config  `inject:""`
	Statter        metrics.Statter `inject:""`
}

func NewDAOWithClient(redshiftClient redshift.Client, config *config.Config, statter metrics.Statter) DAO {
	return &dao{
		RedshiftClient: redshiftClient,
		Config:         config,
		Statter:        statter,
	}
}

func (d *dao) DeleteAllByOwnerId(ownerId string) error {
	start := time.Now()
	suffix := formatDeleteSuffix([]string{ownerId})
	err := d.RedshiftClient.Exec(fmt.Sprintf(DeleteAllByOwnerIdCommandPrefix, d.Config.TenantDomain.String()) + suffix)
	go d.Statter.Duration(deleteByOwnerIdRedshiftLatency, time.Since(start))
	return err
}

func (d *dao) DeleteAllByGroupId(groupId string) error {
	start := time.Now()
	suffix := formatDeleteSuffix([]string{groupId})
	err := d.RedshiftClient.Exec(fmt.Sprintf(DeleteAllByGroupIdCommandPrefix, d.Config.TenantDomain.String()) + suffix)
	go d.Statter.Duration(deleteByGroupIdRedshiftLatency, time.Since(start))
	return err
}

func (d *dao) DeleteAllByOwnerIds(ownerIds []string) error {
	start := time.Now()
	suffix := formatDeleteSuffix(ownerIds)
	execCommand := fmt.Sprintf(DeleteAllByOwnerIdCommandPrefix, d.Config.TenantDomain.String()) + suffix
	err := d.RedshiftClient.Exec(execCommand)
	go d.Statter.Duration(deleteByOwnerIdsRedshiftLatency, time.Since(start))
	return err
}

func (d *dao) DeleteAllByGroupIds(groupIds []string) error {
	start := time.Now()
	suffix := formatDeleteSuffix(groupIds)
	execCommand := fmt.Sprintf(DeleteAllByGroupIdCommandPrefix, d.Config.TenantDomain.String()) + suffix
	err := d.RedshiftClient.Exec(execCommand)
	go d.Statter.Duration(deleteByGroupIdsRedshiftLatency, time.Since(start))
	return err
}

func formatDeleteSuffix(ids []string) string {
	var quotedIds []string
	for _, id := range ids {
		quotedIds = append(quotedIds, fmt.Sprintf(`'%s'`, id))
	}
	return " (" + strings.Join(quotedIds, ", ") + ")"
}
