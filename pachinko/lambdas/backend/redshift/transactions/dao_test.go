package transactions

import (
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	redshift_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/backend/clients/redshift"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestTransactionsDAO(t *testing.T) {
	Convey("given a transactionsDAO", t, func() {
		statter := new(metrics_mock.Statter)
		domain := "STICKER"
		cfg := &config.Config{
			Tenants: &tenant.Wrapper{
				"STICKER": &tenant.Config{
					ResourceIdentifier: domain,
				},
			},
		}
		redshift := new(redshift_mock.Client)
		statter.On("Duration", mock.Anything, mock.Anything).Return()

		dao := &dao{
			RedshiftClient: redshift,
			Config:         cfg,
			Statter:        statter,
		}

		Convey("DeleteAllByOwnerId", func() {
			ownerId := random.String(16)
			suffix := fmt.Sprintf(` ('%s')`, ownerId)
			expectedExecCommand := fmt.Sprintf(DeleteAllByOwnerIdCommandPrefix, domain) + suffix

			Convey("Successfully deletes items", func() {
				redshift.On("Exec", expectedExecCommand).Return(nil)
				err := dao.DeleteAllByOwnerId(ownerId)
				So(err, ShouldBeNil)
			})

			Convey("Returns an error when Exec fails", func() {
				redshift.On("Exec", expectedExecCommand).Return(errors.New("Failed to delete items"))
				err := dao.DeleteAllByOwnerId(ownerId)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("DeleteAllByOwnerIds", func() {
			ownerIds := []string{"a", "b", "c"}
			suffix := " ('a', 'b', 'c')"
			expectedExecCommand := fmt.Sprintf(DeleteAllByOwnerIdCommandPrefix, domain) + suffix

			Convey("Successfully deletes items", func() {
				redshift.On("Exec", expectedExecCommand).Return(nil)
				err := dao.DeleteAllByOwnerIds(ownerIds)
				So(err, ShouldBeNil)
			})

			Convey("Returns an error when Exec fails", func() {
				redshift.On("Exec", expectedExecCommand).Return(errors.New("Failed to delete items"))
				err := dao.DeleteAllByOwnerIds(ownerIds)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("DeleteAllByGroupId", func() {
			groupId := random.String(16)
			suffix := fmt.Sprintf(` ('%s')`, groupId)
			expectedExecCommand := fmt.Sprintf(DeleteAllByGroupIdCommandPrefix, domain) + suffix

			Convey("Successfully deletes items", func() {
				redshift.On("Exec", expectedExecCommand).Return(nil)
				err := dao.DeleteAllByGroupId(groupId)
				So(err, ShouldBeNil)
			})

			Convey("Returns an error when Exec fails", func() {
				redshift.On("Exec", expectedExecCommand).Return(errors.New("Failed to delete items"))
				err := dao.DeleteAllByGroupId(groupId)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("DeleteAllByGroupIds", func() {
			groupIds := []string{"a", "b", "c"}
			suffix := " ('a', 'b', 'c')"
			expectedExecCommand := fmt.Sprintf(DeleteAllByGroupIdCommandPrefix, domain) + suffix

			Convey("Successfully deletes items", func() {
				redshift.On("Exec", expectedExecCommand).Return(nil)
				err := dao.DeleteAllByGroupIds(groupIds)
				So(err, ShouldBeNil)
			})

			Convey("Returns an error when Exec fails", func() {
				redshift.On("Exec", expectedExecCommand).Return(errors.New("Failed to delete items"))
				err := dao.DeleteAllByGroupIds(groupIds)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
