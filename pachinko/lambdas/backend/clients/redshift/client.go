package redshift

import (
	"encoding/json"
	"errors"
	"fmt"

	"code.justin.tv/commerce/pachinko/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type secretsManagerData struct {
	UserName string `json:"username"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	DBName   string `json:"dbname"`
}

type Client interface {
	Exec(sql string, values ...interface{}) error
}

type client struct {
	db *gorm.DB
}

func NewClient(cfg *config.Config, sess *session.Session) (Client, error) {
	dbCredentials, err := getCredentials(cfg, sess)
	if err != nil {
		return nil, err
	}

	host := dbCredentials.Host
	port := dbCredentials.Port

	dbArgs := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s",
		host,
		port,
		dbCredentials.UserName,
		dbCredentials.DBName,
		dbCredentials.Password)
	db, err := gorm.Open("postgres", dbArgs)

	if err != nil {
		return nil, err
	}

	return &client{
		db: db,
	}, nil
}

func (c client) Exec(sql string, values ...interface{}) error {
	return c.db.Exec(sql, values...).Error
}

func getCredentials(cfg *config.Config, sess *session.Session) (*secretsManagerData, error) {
	secretsManager := secretsmanager.New(sess)
	secrets, err := secretsManager.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: &cfg.Redshift.AWSSecretStoreARN,
	})
	if err != nil {
		return nil, err
	}

	if secrets == nil {
		return nil, errors.New("secret manager returned nil")
	}

	dbCredentials := secretsManagerData{}
	err = json.Unmarshal([]byte(*secrets.SecretString), &dbCredentials)
	if err != nil {
		return nil, err
	}
	return &dbCredentials, nil
}
