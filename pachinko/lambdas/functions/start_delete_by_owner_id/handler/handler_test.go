package start_delete_by_owner_id

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"testing"

	lambda_common "code.justin.tv/commerce/pachinko/lambdas/common"

	sqs_client "code.justin.tv/commerce/pachinko/backend/clients/sqs"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	sfn_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sfn"
	sqs_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sqs"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a start_delete_by_owner_id lambda handler", t, func() {
		sqsClient := new(sqs_mock.Client)
		sfnClient := new(sfn_mock.Client)

		sticker := "STICKER"
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
					ResourceIdentifier: sticker,
					DeleteBalanceByOwnerIdSQSConfig: &tenant.SQSConfig{
						URL: "test-sqs-url",
					},
					DeleteBalanceByOwnerIdStepFunctionConfig: &tenant.StepFunctionConfig{
						ARN: "test-sfn-arn",
					},
				},
			},
		}

		handler := &LambdaHandler{
			SQSClient: sqsClient,
			SFNClient: sfnClient,
			Config:    c,
		}

		ctx := context.Background()

		Convey("Test getSQSMessages", func() {
			numOfOwnerIDs := MaxBatchSize + 100
			numOfPages := numOfOwnerIDs / sqs_client.MaxReceivedMessage

			var testOwnerIDs []*sqs.Message
			for i := 1; i <= numOfOwnerIDs; i++ {
				ownerId := fmt.Sprintf("test-owner-id-%d", i)
				testOwnerIDs = append(testOwnerIDs, &sqs.Message{
					Body: &ownerId,
				})
			}

			SQSURL, err := handler.getSQSURL()
			So(err, ShouldBeNil)
			So(SQSURL, ShouldEqual, "test-sqs-url")

			Convey("When queue has more messages than MaxBatchSize", func() {
				for i := 0; i < numOfPages; i++ {
					lowIndex := i * sqs_client.MaxReceivedMessage
					highIndex := lowIndex + sqs_client.MaxReceivedMessage
					sqsClient.On("ReceiveMessages", ctx, SQSURL, sqs_client.MaxReceivedMessage).Return(testOwnerIDs[lowIndex:highIndex], nil).Once()
				}

				messages, err := handler.getSQSMessages(ctx, SQSURL)
				So(err, ShouldBeNil)
				So(len(messages), ShouldEqual, MaxBatchSize)
			})

			Convey("When queue has fewer messages than MaxBatchSize", func() {
				expectedMessageCount := 50

				for i := 0; i < expectedMessageCount/sqs_client.MaxReceivedMessage; i++ {
					lowIndex := i * sqs_client.MaxReceivedMessage
					highIndex := lowIndex + sqs_client.MaxReceivedMessage
					sqsClient.On("ReceiveMessages", ctx, SQSURL, sqs_client.MaxReceivedMessage).Return(testOwnerIDs[lowIndex:highIndex], nil).Once()
				}
				sqsClient.On("ReceiveMessages", ctx, SQSURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()

				messages, err := handler.getSQSMessages(ctx, SQSURL)
				So(err, ShouldBeNil)
				So(len(messages), ShouldEqual, expectedMessageCount)
			})
		})

		Convey("Test getUniqueOwnerIdsFromMessages", func() {
			ownerId := "test-owner-id"
			var testOwnerIDsRepeated []*sqs.Message
			for i := 1; i < 10; i++ {
				testOwnerIDsRepeated = append(testOwnerIDsRepeated, &sqs.Message{
					Body: &ownerId,
				})
			}
			uniqueOwnerIDs := lambda_common.GetUniqueItemsFromMessages(testOwnerIDsRepeated)
			So(len(uniqueOwnerIDs), ShouldEqual, 1)
			So(uniqueOwnerIDs[0], ShouldEqual, ownerId)
		})

		Convey("Test startStepFn", func() {
			SFNARN, err := handler.getSFNARN()
			So(err, ShouldBeNil)
			So(SFNARN, ShouldEqual, "test-sfn-arn")

			testOwnerIDs := []string{
				"test-owner-id-1",
				"test-owner-id-2",
				"test-owner-id-3",
			}

			Convey("Empty Owner IDs", func() {
				executionName, err := handler.startStepFn(ctx, SFNARN, []string{})
				So(err, ShouldBeNil)
				So(executionName, ShouldBeNil)
			})

			Convey("Execution fails", func() {
				sfnClient.On("Execute", ctx, SFNARN, mock.Anything, mock.Anything).Return(errors.New("SOMETHING BAD HAPPENED"))
				executionName, err := handler.startStepFn(ctx, SFNARN, testOwnerIDs)
				So(err, ShouldNotBeNil)
				So(executionName, ShouldBeNil)
			})

			Convey("Execution succeeds", func() {
				sfnClient.On("Execute", ctx, SFNARN, mock.Anything, mock.Anything).Return(nil)
				executionName, err := handler.startStepFn(ctx, SFNARN, testOwnerIDs)
				So(err, ShouldBeNil)
				So(executionName, ShouldNotBeNil)
			})
		})
	})
}
