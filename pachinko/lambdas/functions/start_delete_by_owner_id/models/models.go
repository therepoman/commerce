package models

type StartDeleteByOwnerIdResp struct {
	ExecutionName string
	OwnerIDCount  int
	OwnerIDs      []string
}

type StartDeleteByOwnerIdReq struct {
	IsSmokeTest bool `json:"is_smoke_test"`
}
