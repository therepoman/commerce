package delete_transactions_by_owner_id

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/slice"
	"code.justin.tv/commerce/logrus"
	db "code.justin.tv/commerce/pachinko/backend/dynamo"
	transactionsDb "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	transactionsScanDb "code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan"
	workerPool "code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	"code.justin.tv/commerce/pachinko/config"
	transactionsRedshift "code.justin.tv/commerce/pachinko/lambdas/backend/redshift/transactions"
	lambda_common "code.justin.tv/commerce/pachinko/lambdas/common"
	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_transactions_by_owner_id/models"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/cenkalti/backoff"
	"github.com/guregu/dynamo"
)

const (
	ScanLimit     = int64(2000)
	MaxIterations = 300
	TotalSegments = 10
	DeadlineLimit = 30
)

type LambdaHandler struct {
	DAO         transactionsDb.DAO       `inject:""`
	ScanDAO     transactionsScanDb.DAO   `inject:""`
	RedshiftDAO transactionsRedshift.DAO `inject:""`
	Config      *config.Config           `inject:""`
	WorkerPool  workerPool.WorkerPool    `inject:""`
}

func (l *LambdaHandler) Handle(ctx context.Context, input models.DeleteTransactionsByOwnerIdReq) (*models.DeleteTransactionsByOwnerIdResp, error) {
	if input.IsSmokeTest {
		return nil, nil
	}

	ownerIds := input.OwnerIds
	if len(ownerIds) == 0 {
		return nil, errors.New("failed to delete transactions, ownerIds was empty")
	}

	// don't scan transactions for CoPo
	if l.Config.TenantDomain == pachinko.Domain_COMMUNITY_POINTS {
		return &models.DeleteTransactionsByOwnerIdResp{
			OwnerIds:       ownerIds,
			IsScanComplete: true,
			ScanJobs:       nil,
		}, nil
	}

	jobs := input.ScanJobs
	if len(input.ScanJobs) == 0 {
		// Delete transactions in Redshift, only need to do it the first time this function is run (when there are no initial jobs)
		err := l.RedshiftDAO.DeleteAllByOwnerIds(ownerIds)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"ownerIds": ownerIds,
			}).Error("failed to delete transactions in Redshift")
			return nil, err
		}

		// if no initial jobs, create 10 parallel scan jobs
		jobs = make([]*workerPool.Job, 0)
		for i := 0; i < TotalSegments; i++ {
			jobs = append(jobs, &workerPool.Job{
				Segment:       int64(i),
				TotalSegments: TotalSegments,
			})
		}
	}

	workerFn := func(job workerPool.Job) workerPool.Result {
		return l.scanAndDeleteByOwnerId(ctx, job, ownerIds)
	}
	newJobs, err := l.WorkerPool.Execute(jobs, workerFn, len(jobs))
	if err != nil {
		return nil, err
	}

	return &models.DeleteTransactionsByOwnerIdResp{
		OwnerIds:       ownerIds,
		IsScanComplete: len(newJobs) == 0,
		ScanJobs:       newJobs,
	}, nil
}

func (l *LambdaHandler) scanAndDeleteByOwnerId(ctx context.Context, job workerPool.Job, ownerIds []string) workerPool.Result {
	bo := backoff.WithMaxRetries(lambda_common.GetNewBackoff(), 5)

	ownerIdsMap := slice.ToStringMap(ownerIds)
	pagingKey := job.PagingKey
	for i := 0; i < MaxIterations; i++ {
		i := i // fix scoping issue for backoffs

		var results []db.Transaction
		var newPagingKey db.PagingKey
		var err error

		scanTransactionsBackoffFunc := func() error {
			results, newPagingKey, err = l.ScanDAO.ScanTransactions(ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments)
			if err != nil {
				logrus.WithError(err).WithFields(logrus.Fields{
					"ownerIds":    ownerIds,
					"iteration":   i,
					"job.Segment": job.Segment,
					"pagingKey":   pagingKey,
				}).Error("failed to scan transactions")
				return err
			}
			return nil
		}
		err = backoff.Retry(scanTransactionsBackoffFunc, bo)
		if err != nil {
			return workerPool.Result{
				Err: err,
			}
		}

		transactionsToDelete := make([]dynamo.Keyed, 0)

		for _, transaction := range results {
			if _, ok := ownerIdsMap[transaction.OwnerID]; ok {
				// delete transactions for matching ownerIds
				transactionsToDelete = append(transactionsToDelete, dynamo.Keys{transaction.ID})
			}
		}

		// Delete transactions in DDB
		batchDeleteTransactionsBackoffFunc := func() error {
			err = l.DAO.BatchDeleteTransactions(ctx, transactionsToDelete)
			if err != nil {
				logrus.WithError(err).WithFields(logrus.Fields{
					"ownerId":     ownerIds,
					"iteration":   i,
					"job.Segment": job.Segment,
					"pagingKey":   pagingKey,
				}).Error("could not delete transactions")
				return err
			}
			return nil
		}
		err = backoff.Retry(batchDeleteTransactionsBackoffFunc, bo)
		if err != nil {
			return workerPool.Result{
				Err: err,
			}
		}

		pagingKey = newPagingKey
		if len(pagingKey) == 0 {
			return workerPool.Result{}
		}

		// if we are almost out of time, stop execution to continue later
		deadline, _ := ctx.Deadline()
		now := time.Now()
		if deadline.Unix()-now.Unix() < DeadlineLimit {
			logrus.WithFields(logrus.Fields{
				"job.Id":    job.Id,
				"iteration": i,
				"deadline":  deadline.Unix(),
			}).Info("timeout limit almost exceeded, will continue in another execution")
			break
		}
	}

	// if we hit MaxIterations and still have a pagingKey then return a job to be run in a subsequent execution
	return workerPool.Result{
		NextJob: &workerPool.Job{
			Segment:       job.Segment,
			TotalSegments: job.TotalSegments,
			PagingKey:     pagingKey,
		},
	}
}
