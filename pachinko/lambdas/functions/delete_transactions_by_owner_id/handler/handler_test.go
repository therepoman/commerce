package delete_transactions_by_owner_id

import (
	"context"
	"errors"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/random"
	pachinko_dynamo "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_transactions_by_owner_id/models"
	ddb_transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	ddb_transactions_scan_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan"
	workerPoolMock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/backend/redshift/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a transactions lambda handler", t, func() {
		transactionsDAO := new(ddb_transactions_mock.DAO)
		scanDAO := new(ddb_transactions_scan_mock.DAO)
		redshiftDAO := new(transactions_mock.DAO)
		workerPoolMock := new(workerPoolMock.WorkerPool)

		sticker := "STICKER"
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
					ResourceIdentifier: sticker,
				},
			},
		}

		handler := &LambdaHandler{
			Config:      c,
			DAO:         transactionsDAO,
			ScanDAO:     scanDAO,
			RedshiftDAO: redshiftDAO,
			WorkerPool:  workerPoolMock,
		}

		ctx := context.Background()
		ownerIds := []string{"owner-id-0", "owner-id-1", "owner-id-2"}
		input := models.DeleteTransactionsByOwnerIdReq{
			OwnerIds: ownerIds,
			ScanJobs: nil,
		}

		Convey("Test Handle", func() {
			Convey("returns successfully for smoke test", func() {
				input := models.DeleteTransactionsByOwnerIdReq{
					IsSmokeTest: true,
				}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("throws an error when the input doesnt contain ownerIds", func() {
				input := models.DeleteTransactionsByOwnerIdReq{}

				resp, err := handler.Handle(ctx, input)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("Returns without processing when domain is CoPo", func() {
				c := &config.Config{
					TenantDomain: pachinko.Domain_COMMUNITY_POINTS,
				}
				handler := &LambdaHandler{
					Config: c,
				}
				input := models.DeleteTransactionsByOwnerIdReq{
					OwnerIds: ownerIds,
					ScanJobs: nil,
				}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &models.DeleteTransactionsByOwnerIdResp{
					OwnerIds:       ownerIds,
					IsScanComplete: true,
					ScanJobs:       nil,
				})
			})

			Convey("when there are no initial scan jobs", func() {
				Convey("when there is an error deleting transactions in Redshift", func() {
					redshiftDAO.On("DeleteAllByOwnerIds", ownerIds).Return(errors.New("Failed to delete transactions in Redshift"))
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})

				Convey("when deleting the transactions is successful", func() {
					redshiftDAO.On("DeleteAllByOwnerIds", ownerIds).Return(nil)
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(nil, nil)
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &models.DeleteTransactionsByOwnerIdResp{
						OwnerIds:       ownerIds,
						IsScanComplete: true,
						ScanJobs:       nil,
					})
				})

				Convey("A worker pool is executed for scanning and deleting ownerIds", func() {
					redshiftDAO.On("DeleteAllByOwnerIds", ownerIds).Return(nil)
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(nil, nil)
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &models.DeleteTransactionsByOwnerIdResp{
						OwnerIds:       ownerIds,
						IsScanComplete: true,
						ScanJobs:       nil,
					})
				})

				Convey("An error is returned when the worker pool fails to execute the jobs", func() {
					redshiftDAO.On("DeleteAllByOwnerIds", ownerIds).Return(nil)
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(nil, errors.New("Failed to execute jobs"))
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when there are some initial scan jobs", func() {
				jobs := []*worker_pool.Job{{
					PagingKey:     nil,
					Segment:       1,
					TotalSegments: 5,
				}}
				input.ScanJobs = jobs
				redshiftDAO.On("DeleteAllByOwnerIds", ownerIds).Return(nil)
				workerPoolMock.On("Execute", jobs, mock.AnythingOfType("worker_pool.WorkerFn"), 1).Return(nil, nil)
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &models.DeleteTransactionsByOwnerIdResp{
					OwnerIds:       ownerIds,
					IsScanComplete: true,
					ScanJobs:       nil,
				})
			})

			Convey("when there are leftover jobs from the worker pool", func() {
				jobs := []*worker_pool.Job{{
					PagingKey:     nil,
					Segment:       1,
					TotalSegments: 5,
				}}
				redshiftDAO.On("DeleteAllByOwnerIds", ownerIds).Return(nil)
				workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
					mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(jobs, nil)
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &models.DeleteTransactionsByOwnerIdResp{
					OwnerIds:       ownerIds,
					IsScanComplete: false,
					ScanJobs:       jobs,
				})
			})
		})

		Convey("Test scanAndDeleteByOwnerId", func() {
			job := worker_pool.Job{
				PagingKey:     nil,
				Segment:       0,
				TotalSegments: 1,
			}

			transactionID := random.String(16)
			transactionID2 := random.String(16)
			transactions := []pachinko_dynamo.Transaction{{
				ID:        transactionID,
				OwnerID:   ownerIds[0],
				Operation: pachinko_dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*pachinko_dynamo.THToken{
					{
						GroupId:  "2434234235",
						Domain:   pachinko_dynamo.TwirpDomain(pachinko.Domain_STICKER),
						Quantity: 15,
						OwnerId:  ownerIds[0],
					},
				},
				IsCommitted: true,
			}, {
				ID:        transactionID2,
				OwnerID:   ownerIds[1],
				Operation: pachinko_dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*pachinko_dynamo.THToken{
					{
						GroupId:  "2434234235",
						Domain:   pachinko_dynamo.TwirpDomain(pachinko.Domain_STICKER),
						Quantity: 15,
						OwnerId:  ownerIds[1],
					},
				},
				IsCommitted: true,
			}}

			keys := []dynamo.Keyed{dynamo.Keys{transactionID}, dynamo.Keys{transactionID2}}

			Convey("when there is an error scanning the Transactions table", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(nil, nil, errors.New("Failed to scan transactions"))
				result := handler.scanAndDeleteByOwnerId(ctx, job, ownerIds)
				So(result.Err, ShouldNotBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are no Transactions matching the given OwnerId", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(nil, nil, nil)
				transactionsDAO.On("BatchDeleteTransactions", ctx, []dynamo.Keyed{}).Return(nil)

				result := handler.scanAndDeleteByOwnerId(ctx, job, ownerIds)
				So(result.Err, ShouldBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are Transactions matching the given OwnerId", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(transactions, nil, nil)

				Convey("when there is an error deleting transactions", func() {
					transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(errors.New("Failed to delete transactions"))
					result := handler.scanAndDeleteByOwnerId(ctx, job, ownerIds)
					So(result.Err, ShouldNotBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})

			Convey("when there are more Transactions to scan in the table", func() {
				newPagingKey := pachinko_dynamo.PagingKey{
					"item1": &dynamodb.AttributeValue{
						S: pointers.StringP("foobar"),
					},
				}

				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(transactions, newPagingKey, nil).Once()
				scanDAO.On("ScanTransactions", ctx, ScanLimit, newPagingKey, job.Segment, job.TotalSegments).Return(transactions, newPagingKey, nil)
				transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(nil)

				Convey("when deleting the transactions is successful", func() {
					result := handler.scanAndDeleteByOwnerId(ctx, job, ownerIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						TotalSegments: job.TotalSegments,
						Segment:       job.Segment,
						PagingKey:     newPagingKey,
					})
				})
			})

			Convey("when the lambda is close to the execution timeout", func() {
				now := time.Now()
				shortCtx, cancel := context.WithDeadline(ctx, now.Add((DeadlineLimit-5)*time.Second))
				defer cancel()
				newPagingKey := pachinko_dynamo.PagingKey{
					"item1": &dynamodb.AttributeValue{
						S: pointers.StringP("foobar"),
					},
				}

				// expect only one ScanTransactions call
				scanDAO.On("ScanTransactions", shortCtx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(transactions, newPagingKey, nil).Once()
				transactionsDAO.On("BatchDeleteTransactions", shortCtx, keys).Return(nil)

				Convey("stops scanning and returns a followup job", func() {
					result := handler.scanAndDeleteByOwnerId(shortCtx, job, ownerIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						TotalSegments: job.TotalSegments,
						Segment:       job.Segment,
						PagingKey:     newPagingKey,
					})
				})
			})

			Convey("when given a paging key it passes it to ScanTransactions", func() {
				pagingKey := pachinko_dynamo.PagingKey{
					"item1": &dynamodb.AttributeValue{
						S: pointers.StringP("foobar"),
					},
				}
				job.PagingKey = pagingKey

				scanDAO.On("ScanTransactions", ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments).Return(transactions, nil, nil)
				transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(nil)

				Convey("when deleting the transactions is successful", func() {
					result := handler.scanAndDeleteByOwnerId(ctx, job, ownerIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})
		})
	})
}
