package models

import (
	workerPool "code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
)

type DeleteTransactionsByOwnerIdReq struct {
	IsSmokeTest bool              `json:"is_smoke_test"`
	OwnerIds    []string          `json:"owner_ids"`
	ScanJobs    []*workerPool.Job `json:"scan_jobs"`
}

type DeleteTransactionsByOwnerIdResp struct {
	OwnerIds       []string          `json:"owner_ids"`
	IsScanComplete bool              `json:"is_scan_complete"`
	ScanJobs       []*workerPool.Job `json:"scan_jobs"`
}
