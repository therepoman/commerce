package models

import (
	workerPool "code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
)

type DeleteTransactionsByGroupIdReq struct {
	IsSmokeTest bool              `json:"is_smoke_test"`
	GroupIds    []string          `json:"group_ids"`
	ScanJobs    []*workerPool.Job `json:"scan_jobs"`
}

type DeleteTransactionsByGroupIdResp struct {
	GroupIds       []string          `json:"group_ids"`
	IsScanComplete bool              `json:"is_scan_complete"`
	ScanJobs       []*workerPool.Job `json:"scan_jobs"`
}
