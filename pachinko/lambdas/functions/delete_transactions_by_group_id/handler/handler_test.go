package delete_transactions_by_group_id

import (
	"context"
	"errors"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/random"
	pachinko_dynamo "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_transactions_by_group_id/models"
	ddb_transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	ddb_transactions_scan_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan"
	workerPoolMock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/backend/redshift/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a transactions lambda handler", t, func() {
		transactionsDAO := new(ddb_transactions_mock.DAO)
		scanDAO := new(ddb_transactions_scan_mock.DAO)
		redshiftDAO := new(transactions_mock.DAO)
		workerPoolMock := new(workerPoolMock.WorkerPool)

		sticker := "STICKER"
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
					ResourceIdentifier: sticker,
				},
			},
		}

		handler := &LambdaHandler{
			Config:      c,
			DAO:         transactionsDAO,
			ScanDAO:     scanDAO,
			RedshiftDAO: redshiftDAO,
			WorkerPool:  workerPoolMock,
		}

		ctx := context.Background()

		groupIds := []string{random.String(16), random.String(16), random.String(16)}

		Convey("Test Handle", func() {
			Convey("returns successfully for smoke test", func() {
				input := models.DeleteTransactionsByGroupIdReq{
					IsSmokeTest: true,
				}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("throws an error when the input doesnt contain GroupIds", func() {
				input := models.DeleteTransactionsByGroupIdReq{}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("Returns without processing when domain is CoPo", func() {
				c := &config.Config{
					TenantDomain: pachinko.Domain_COMMUNITY_POINTS,
				}
				handler := &LambdaHandler{
					Config: c,
				}
				input := models.DeleteTransactionsByGroupIdReq{
					GroupIds: groupIds,
					ScanJobs: nil,
				}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &models.DeleteTransactionsByGroupIdResp{
					GroupIds:       groupIds,
					IsScanComplete: true,
					ScanJobs:       nil,
				})
			})

			Convey("when there are no initial scan jobs", func() {
				input := models.DeleteTransactionsByGroupIdReq{
					GroupIds: groupIds,
					ScanJobs: nil,
				}
				Convey("when there is an error deleting transactions in Redshift", func() {
					redshiftDAO.On("DeleteAllByGroupIds", groupIds).Return(errors.New("Failed to delete transactions in Redshift"))
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})

				Convey("when deleting and updating the transactions is successful", func() {
					redshiftDAO.On("DeleteAllByGroupIds", groupIds).Return(nil)
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(nil, nil)

					resp, err := handler.Handle(ctx, input)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &models.DeleteTransactionsByGroupIdResp{
						GroupIds:       groupIds,
						IsScanComplete: true,
						ScanJobs:       nil,
					})
				})

				Convey("A worker pool is executed for scanning and deleting by groupId", func() {
					redshiftDAO.On("DeleteAllByGroupIds", groupIds).Return(nil)
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(nil, nil)
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &models.DeleteTransactionsByGroupIdResp{
						GroupIds:       groupIds,
						IsScanComplete: true,
						ScanJobs:       nil,
					})
				})

				Convey("An error is returned when the worker pool fails to execute the jobs", func() {
					redshiftDAO.On("DeleteAllByGroupIds", groupIds).Return(nil)
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), TotalSegments).Return(nil, errors.New("Failed to execute jobs"))
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when there are some initial scan jobs", func() {
				jobs := []*worker_pool.Job{{
					PagingKey:     nil,
					Segment:       1,
					TotalSegments: 5,
				}}

				input := models.DeleteTransactionsByGroupIdReq{
					GroupIds: groupIds,
					ScanJobs: jobs,
				}

				redshiftDAO.On("DeleteAllByGroupIds", groupIds).Return(nil)
				workerPoolMock.On("Execute", jobs, mock.AnythingOfType("worker_pool.WorkerFn"), 1).Return(nil, nil)
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &models.DeleteTransactionsByGroupIdResp{
					GroupIds:       groupIds,
					IsScanComplete: true,
					ScanJobs:       nil,
				})
			})

			Convey("when there are leftover jobs from the worker pool", func() {
				jobs := []*worker_pool.Job{{
					PagingKey:     nil,
					Segment:       1,
					TotalSegments: 5,
				}}

				input := models.DeleteTransactionsByGroupIdReq{
					GroupIds: groupIds,
					ScanJobs: jobs,
				}

				redshiftDAO.On("DeleteAllByGroupIds", groupIds).Return(nil)
				workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"), mock.AnythingOfType("worker_pool.WorkerFn"), 1).Return(jobs, nil)
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &models.DeleteTransactionsByGroupIdResp{
					GroupIds:       groupIds,
					IsScanComplete: false,
					ScanJobs:       jobs,
				})
			})
		})

		Convey("Test scanAndDeleteByGroupId", func() {
			job := worker_pool.Job{
				PagingKey:     nil,
				Segment:       0,
				TotalSegments: 1,
			}

			tokensWithMatchingGroupId := make([]*pachinko_dynamo.THToken, 0)
			for i, groupId := range groupIds {
				tokensWithMatchingGroupId = append(tokensWithMatchingGroupId, &pachinko_dynamo.THToken{
					OwnerId:  "2434234235",
					Domain:   pachinko_dynamo.TwirpDomain(pachinko.Domain_STICKER),
					Quantity: int64(i + 1),
					GroupId:  groupId,
				})
			}

			tokenWithoutMatchingGroupId := &pachinko_dynamo.THToken{
				OwnerId:  "2434234235",
				Domain:   pachinko_dynamo.TwirpDomain(pachinko.Domain_STICKER),
				Quantity: 15,
				GroupId:  random.String(16),
			}

			txWithNoMatchingTokens := pachinko_dynamo.Transaction{
				ID:        random.String(16),
				OwnerID:   "2434234235",
				Operation: pachinko_dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*pachinko_dynamo.THToken{
					tokenWithoutMatchingGroupId,
				},
				IsCommitted: true,
			}

			txWithSomeMatchingTokens := pachinko_dynamo.Transaction{
				ID:        random.String(16),
				OwnerID:   "2434234235",
				Operation: pachinko_dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*pachinko_dynamo.THToken{
					tokensWithMatchingGroupId[0],
					tokensWithMatchingGroupId[1],
					tokenWithoutMatchingGroupId,
				},
				IsCommitted: true,
			}

			txWithSomeMatchingTokensRemoved := txWithSomeMatchingTokens
			txWithSomeMatchingTokensRemoved.Tokens = []*pachinko_dynamo.THToken{
				tokenWithoutMatchingGroupId,
			}

			txWithAllMatchingTokens := pachinko_dynamo.Transaction{
				ID:          random.String(16),
				OwnerID:     "2434234235",
				Operation:   pachinko_dynamo.AddTokensOperation,
				Timestamp:   time.Now(),
				Tokens:      tokensWithMatchingGroupId,
				IsCommitted: true,
			}

			transactions := []pachinko_dynamo.Transaction{txWithNoMatchingTokens, txWithSomeMatchingTokens, txWithAllMatchingTokens}
			keys := []dynamo.Keyed{dynamo.Keys{txWithAllMatchingTokens.ID}}
			pagingKey := pachinko_dynamo.PagingKey{
				"item1": &dynamodb.AttributeValue{
					S: pointers.StringP("foobar"),
				},
			}

			Convey("when there is an error scanning the Transactions table", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(nil, nil, errors.New("Failed to scan transactions"))
				result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
				So(result.Err, ShouldNotBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are no Transactions with Tokens that match the given GroupId", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(nil, nil, nil)
				transactionsDAO.On("BatchPutTransactions", ctx, []pachinko_dynamo.Transaction{}).Return(nil)
				transactionsDAO.On("BatchDeleteTransactions", ctx, []dynamo.Keyed{}).Return(nil)
				result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
				So(result.Err, ShouldBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are Transactions with Tokens that match the given GroupId", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(transactions, nil, nil)

				Convey("when there is an error updating transactions", func() {
					transactionsDAO.On("BatchPutTransactions", ctx, []pachinko_dynamo.Transaction{txWithSomeMatchingTokensRemoved}).Return(errors.New("Failed to update transactions"))
					transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(nil)
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldNotBeNil)
					So(result.NextJob, ShouldBeNil)
				})

				Convey("when there is an error deleting transactions", func() {
					transactionsDAO.On("BatchPutTransactions", ctx, []pachinko_dynamo.Transaction{txWithSomeMatchingTokensRemoved}).Return(nil)
					transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(errors.New("Failed to delete transactions"))
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldNotBeNil)
					So(result.NextJob, ShouldBeNil)
				})

				Convey("when deleting and updating the transactions is successful", func() {
					transactionsDAO.On("BatchPutTransactions", ctx, []pachinko_dynamo.Transaction{txWithSomeMatchingTokensRemoved}).Return(nil)
					transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(nil)

					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})

			Convey("when there are more Transactions to scan in the table", func() {
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(transactions, pagingKey, nil).Once()
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments).Return(transactions, pagingKey, nil)
				transactionsDAO.On("BatchPutTransactions", ctx, []pachinko_dynamo.Transaction{txWithSomeMatchingTokensRemoved}).Return(nil)
				transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(nil)

				Convey("when deleting the transactions is successful", func() {
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						Segment:       job.Segment,
						TotalSegments: job.TotalSegments,
						PagingKey:     pagingKey,
					})
				})
			})

			Convey("when the lambda is close to the execution timeout", func() {
				now := time.Now()
				shortCtx, cancel := context.WithDeadline(ctx, now.Add((DeadlineLimit-5)*time.Second))
				defer cancel()
				// expect only one ScanTransactions call
				scanDAO.On("ScanTransactions", shortCtx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(transactions, pagingKey, nil).Once()
				transactionsDAO.On("BatchPutTransactions", shortCtx, []pachinko_dynamo.Transaction{txWithSomeMatchingTokensRemoved}).Return(nil)
				transactionsDAO.On("BatchDeleteTransactions", shortCtx, keys).Return(nil)

				Convey("stops scanning and returns a followup job", func() {
					result := handler.scanAndDeleteByGroupId(shortCtx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						Segment:       job.Segment,
						TotalSegments: job.TotalSegments,
						PagingKey:     pagingKey,
					})
				})
			})

			Convey("when given a paging key it passes it to ScanTransactions", func() {
				job.PagingKey = pagingKey
				scanDAO.On("ScanTransactions", ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments).Return(transactions, nil, nil)
				transactionsDAO.On("BatchPutTransactions", ctx, []pachinko_dynamo.Transaction{txWithSomeMatchingTokensRemoved}).Return(nil)
				transactionsDAO.On("BatchDeleteTransactions", ctx, keys).Return(nil)

				Convey("when deleting the transactions is successful", func() {
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})
		})
	})
}
