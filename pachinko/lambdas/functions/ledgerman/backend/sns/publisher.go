package sns

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/models"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp/hooks/statsd"
)

type Publisher interface {
	Publish(ctx context.Context, transactionIDs []string) error
}

func NewPublisher() Publisher {
	return &publisher{}
}

type publisher struct {
	SNS     sns.Client     `inject:""`
	Config  *config.Config `inject:""`
	Statter statsd.Statter `inject:""`
}

func (p *publisher) Publish(ctx context.Context, transactionIDs []string) error {
	if len(transactionIDs) < 1 {
		return errors.New("cannot perform operation without transaction IDs")
	}

	conf := p.Config.Tenants.Get(p.Config.TenantDomain)
	if conf == nil {
		return errors.New("cannot perform operation on domain that is not configured")
	}

	mJSON, err := json.Marshal(&models.BalanceUpdate{
		TransactionIDs: transactionIDs,
	})
	if err != nil {
		return err
	}

	start := time.Now()
	defer func() {
		err := p.Statter.TimingDuration("sns-publish-latency", time.Since(start), 1.0)
		if err != nil {
			logrus.WithError(err).Error("could not stat SNS publish latency")
		}
	}()
	return p.SNS.Publish(ctx, conf.BalanceUpdatesTopicARN, string(mJSON))
}
