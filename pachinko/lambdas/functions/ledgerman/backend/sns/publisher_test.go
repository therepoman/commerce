package sns

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	sns_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	statsd_mock "code.justin.tv/commerce/pachinko/mocks/github.com/twitchtv/twirp/hooks/statsd"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPublisher_Publish(t *testing.T) {
	Convey("given a token transaction publisher", t, func() {
		snsClient := new(sns_mock.Client)
		statter := new(statsd_mock.Statter)

		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}

		publisher := &publisher{
			Config:  c,
			SNS:     snsClient,
			Statter: statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()

		Convey("when the request has no transaction IDs", func() {
			err := publisher.Publish(ctx, []string{})

			So(err, ShouldNotBeNil)
		})

		Convey("when the request does have tokens", func() {
			transactionIDs := []string{random.String(16)}

			Convey("when the snsClient fails", func() {
				snsClient.On("Publish", ctx, tenantConf.BalanceUpdatesTopicARN, mock.Anything).Return(errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					err := publisher.Publish(ctx, transactionIDs)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the snsClient succeeds", func() {
				snsClient.On("Publish", ctx, tenantConf.BalanceUpdatesTopicARN, mock.Anything).Return(nil)

				Convey("then we should return nil", func() {
					err := publisher.Publish(ctx, transactionIDs)

					So(err, ShouldBeNil)
				})
			})
		})
	})
}
