package backend

import (
	"time"

	awssns "code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/logrus"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/cache/balancelock"
	cache_tx "code.justin.tv/commerce/pachinko/backend/cache/transaction"
	"code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	redis_client "code.justin.tv/commerce/pachinko/backend/circuit/redis-client"
	"code.justin.tv/commerce/pachinko/backend/clients/lock"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/backend/transactions"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/clients/firehose"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/sns"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/worker"
	"code.justin.tv/commerce/splatter"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awsFirehose "github.com/aws/aws-sdk-go/service/firehose"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/pkg/errors"
)

type Backend interface {
	LedgermanWorker() worker.Ledgerman
}

type backend struct {
	// Config
	Config *config.Config `inject:""`

	// The ledgerman worker that is used by the Lambda
	Ledgerman worker.Ledgerman `inject:""`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetrics != nil {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.Environment.AWSRegion,
			ServiceName:       "pachinko",
			Stage:             cfg.Environment.Name,
			Substage:          cfg.TenantDomain.String(),
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	hystrixMetrics := metrics.NewHystrixMetricsCollector()

	tenantTxRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-txs")

	deps := []*inject.Object{
		// Backend (graph root)
		{Value: b},

		// Config
		{Value: cfg},

		// Stats
		{Value: stats},
		{Value: metrics.NewStatter()},
		{Value: hystrixMetrics},

		{Value: awsFirehose.New(sess, &aws.Config{Region: aws.String(cfg.Environment.AWSRegion)})},
		{Value: firehose.NewClient()},
		{Value: awssns.NewDefaultClient()},

		// Redis
		{Value: lock.NewDynamoDBLockingClient()},
		{Value: redis.NewClient(tenantTxRedisClient), Name: "redis-txs"},
		{Value: lock.NewDAOWithClient(dynamo.DynamoClient(sess))},

		// Caches
		{Value: cache_tx.NewCache()},
		{Value: balancelock.NewCache()},

		// DAOs
		{Value: dynamo_tx.NewDAOWithClient(dynamo.DynamoClient(sess))},

		// Transactions config
		{Value: transactions.NewLocker()},

		{Value: sns.NewPublisher()},
		{Value: worker.NewLedgerman()},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixMetrics.NewHystrixCommandMetricsCollector(name)
	})

	logrus.Info("Backend initialization complete")

	return b, nil
}

func (b *backend) LedgermanWorker() worker.Ledgerman {
	return b.Ledgerman
}
