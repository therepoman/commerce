package worker

import (
	"context"
	"errors"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	ddb_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/clients/firehose"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/models"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/sns"
	"github.com/aws/aws-lambda-go/events"
)

type Ledgerman interface {
	HandleRequest(ctx context.Context, input models.LedgermanInput) (string, error)
}

type ledgerman struct {
	TransactionsDAO ddb_tx.DAO      `inject:""`
	Firehose        firehose.Client `inject:""`
	Config          *config.Config  `inject:""`
	Publisher       sns.Publisher   `inject:""`
}

func NewLedgerman() Ledgerman {
	return &ledgerman{}
}

// This implements the handler func signature
func (w *ledgerman) HandleRequest(ctx context.Context, input models.LedgermanInput) (string, error) {
	if input.IsSmokeTest {
		return fmt.Sprintf("smoke test complete"), nil
	}

	transactionsCommitted := map[string]*dynamo.Transaction{}

	for _, record := range input.Records {
		if record.EventName == string(events.DynamoDBOperationTypeRemove) {
			// Skip any deletions
			continue
		}

		newRecordAttributeMap := record.Change.NewImage
		if len(newRecordAttributeMap) == 0 {
			logrus.WithField("attribute_map", newRecordAttributeMap).Error("new record attribute map was empty")
			return "", errors.New("new record attribute map was empty")
		}

		transactionID, ok := newRecordAttributeMap["latest_transaction_id"]
		if !ok {
			logrus.Info("new record is missing latest_transaction_id field, so it is coming from old code. No updates needed.")
			continue
		}

		if transactionID.DataType() != events.DataTypeString {
			logrus.Info("found a transaction ID that's not a string, but we should continue")
			continue
		}

		fields := logrus.Fields{
			"transaction_id":        transactionID.String(),
			"transaction_table_fmt": ddb_tx.DBFormat,
		}

		t, err := w.TransactionsDAO.GetTransactionRecord(ctx, transactionID.String())
		if err != nil {
			logrus.WithError(err).WithFields(fields).Error("failed to fetch transaction from transactions db")
			return "", errors.New("failed to fetch transaction from transactions db")
		}

		if t == nil {
			logrus.WithFields(fields).Error("could not find transaction in transactions db")
			return "", errors.New("could not find transaction in transactions db")
		}

		if _, ok := transactionsCommitted[transactionID.String()]; !ok {
			if !t.IsCommitted {
				logrus.WithFields(fields).Error("found uncommitted transaction")
				continue
			}

			transactionsCommitted[transactionID.String()] = t
		}
	}

	if len(transactionsCommitted) > 0 {
		transactionsList := make([]*models.LedgermanMessage, 0)
		transactionIDs := make([]string, 0)
		for key, t := range transactionsCommitted {
			transactionsList = append(transactionsList, &models.LedgermanMessage{
				TransactionID: t.ID,
				Domain:        w.Config.TenantDomain,
				Tokens:        dynamo.ToTwirpTokens(t.Tokens),
				Timestamp:     t.Timestamp,
				Operation:     t.Operation,
			})
			transactionIDs = append(transactionIDs, key)
		}

		if !w.Config.Tenants.Get(w.Config.TenantDomain).SkipRedshift {
			err := w.Firehose.PutRecords(w.Config.Reporting.TransactionsDeliveryStream, transactionsList...)
			if err != nil {
				logrus.WithError(err).Error("could not put records onto firehose stream")
				return "", errors.New("could not put records onto firehose stream")
			}
		}

		if !strings.Blank(w.Config.Tenants.Get(w.Config.TenantDomain).BalanceUpdatesTopicARN) {
			err := w.Publisher.Publish(ctx, transactionIDs)
			if err != nil {
				logrus.WithError(err).Error("could not publish SNS balance update")
				return "", errors.New("could not publish SNS balance update")
			}
		}
	}

	return fmt.Sprintf("complete for %d records", len(transactionsCommitted)), nil
}
