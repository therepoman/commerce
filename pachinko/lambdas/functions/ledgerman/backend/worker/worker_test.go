package worker

import (
	"context"
	"errors"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/reporting"
	"code.justin.tv/commerce/pachinko/config/tenant"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/models"
	ddb_tx_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	firehose_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/ledgerman/backend/clients/firehose"
	sns_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/ledgerman/backend/sns"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-lambda-go/events"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestWorker_HandleRequest(t *testing.T) {
	Convey("given a ledgerman worker", t, func() {
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
					ResourceIdentifier: "sticker",
				},
			},
			Reporting: &reporting.Config{
				TransactionsDeliveryStream: "fooooooo",
			},
		}

		transactionsDAO := new(ddb_tx_mock.DAO)
		firehose := new(firehose_mock.Client)
		publisher := new(sns_mock.Publisher)

		worker := &ledgerman{
			Config:          c,
			Publisher:       publisher,
			Firehose:        firehose,
			TransactionsDAO: transactionsDAO,
		}

		ctx := context.Background()

		input := models.LedgermanInput{
			IsSmokeTest: false,
		}

		Convey("returns successfully for smoke test", func() {
			input.IsSmokeTest = true
			res, err := worker.HandleRequest(ctx, input)
			So(err, ShouldBeNil)
			So(res, ShouldEqual, "smoke test complete")
		})

		Convey("when the event has no records", func() {
			res, err := worker.HandleRequest(ctx, input)
			So(err, ShouldBeNil)
			So(res, ShouldEqual, "complete for 0 records")
		})

		Convey("when the event has records", func() {
			Convey("when the event doesn't have an attribute map from the new record", func() {
				input.Records = []events.DynamoDBEventRecord{
					{
						Change: events.DynamoDBStreamRecord{},
					},
				}

				Convey("then we should return an error", func() {
					_, err := worker.HandleRequest(ctx, input)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "new record attribute map was empty")
				})
			})

			Convey("when the event doesn't have a transaction ID", func() {
				input.Records = []events.DynamoDBEventRecord{
					{
						Change: events.DynamoDBStreamRecord{
							NewImage: map[string]events.DynamoDBAttributeValue{
								"foo": events.NewStringAttribute("bar"),
							},
						},
					},
				}

				Convey("then we should not error, since transaction processing would have been done on older SQS processor code", func() {
					res, err := worker.HandleRequest(ctx, input)
					So(err, ShouldBeNil)
					So(res, ShouldEqual, "complete for 0 records")
				})
			})

			Convey("when the event has a transaction ID", func() {
				transactionID := random.String(32)

				input.Records = []events.DynamoDBEventRecord{
					{
						Change: events.DynamoDBStreamRecord{
							NewImage: map[string]events.DynamoDBAttributeValue{
								"latest_transaction_id": events.NewStringAttribute(transactionID),
							},
						},
					},
				}

				Convey("when we fail to lookup the transaction", func() {
					transactionsDAO.On("GetTransactionRecord", ctx, transactionID).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("then we should return an error", func() {
						_, err := worker.HandleRequest(ctx, input)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "failed to fetch transaction from transactions db")
					})
				})

				Convey("when the transaction doesn't exist", func() {
					transactionsDAO.On("GetTransactionRecord", ctx, transactionID).Return(nil, nil)

					Convey("then we should return an error", func() {
						_, err := worker.HandleRequest(ctx, input)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "could not find transaction in transactions db")
					})
				})

				Convey("when we do find a transaction", func() {
					ownerID, groupID := random.String(16), random.String(16)
					token := &dynamo.THToken{
						Domain:   dynamo.TwirpDomain(c.TenantDomain),
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: 322,
					}

					transaction := &dynamo.Transaction{
						ID:          transactionID,
						Operation:   dynamo.ConsumeTokensOperation,
						Timestamp:   time.Now(),
						Tokens:      []*dynamo.THToken{token},
						OwnerID:     ownerID,
						IsCommitted: true,
					}

					transactionsDAO.On("GetTransactionRecord", ctx, transactionID).Return(transaction, nil)

					Convey("when we fail to put records into firehose", func() {
						firehose.On("PutRecords", c.Reporting.TransactionsDeliveryStream, mock.Anything).Return(errors.New("WALRUS STRIKE"))

						Convey("then we should return an error", func() {
							_, err := worker.HandleRequest(ctx, input)
							So(err, ShouldNotBeNil)
							So(err.Error(), ShouldEqual, "could not put records onto firehose stream")
						})
					})

					Convey("when we succeed to put records into firehose", func() {
						firehose.On("PutRecords", c.Reporting.TransactionsDeliveryStream, mock.Anything).Return(nil)

						Convey("when we do not have a balance update topic ARN present", func() {
							res, err := worker.HandleRequest(ctx, input)
							So(err, ShouldBeNil)
							So(res, ShouldEqual, "complete for 1 records")

							publisher.AssertNotCalled(t, "Publish", ctx, transactionID)
						})

						Convey("when we have a balance update topic ARN present", func() {
							c.Tenants = &tenant.Wrapper{
								strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
									ResourceIdentifier:     "sticker",
									BalanceUpdatesTopicARN: "foooooo",
								},
							}

							Convey("when the publisher fails", func() {
								publisher.On("Publish", ctx, []string{transactionID}).Return(errors.New("WALRUS STRIKE"))

								Convey("then we should return an error", func() {
									_, err := worker.HandleRequest(ctx, input)
									So(err, ShouldNotBeNil)
									So(err.Error(), ShouldEqual, "could not publish SNS balance update")
								})
							})

							Convey("when the publisher succeeds", func() {
								publisher.On("Publish", ctx, []string{transactionID}).Return(nil)

								res, err := worker.HandleRequest(ctx, input)
								So(err, ShouldBeNil)
								So(res, ShouldEqual, "complete for 1 records")

								publisher.AssertCalled(t, "Publish", ctx, []string{transactionID})
							})
						})
					})
				})
			})
		})
	})
}
