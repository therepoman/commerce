package firehose

import (
	"encoding/json"
	"time"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/models"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/firehose"
	"github.com/aws/aws-sdk-go/service/firehose/firehoseiface"
)

type TransactionRecord struct {
	ID        string           `json:"transaction_id"`
	Timestamp int64            `json:"timestamp"`
	Domain    string           `json:"domain_name"`
	OwnerID   string           `json:"owner_id"`
	GroupID   string           `json:"group_id"`
	Operation dynamo.Operation `json:"operation"`
	Amount    int              `json:"amount"`
}

func ToTransactionRecord(id string, timestamp time.Time, operation dynamo.Operation, token *pachinko.Token) TransactionRecord {
	amount := int(token.Quantity)
	switch operation {
	case dynamo.ConsumeTokensOperation:
		amount *= -1
	default:
		amount *= 1
	}

	return TransactionRecord{
		ID:        id,
		Timestamp: timestamp.Unix(),
		Operation: operation,
		Domain:    token.Domain.String(),
		OwnerID:   token.OwnerId,
		GroupID:   token.GroupId,
		Amount:    amount,
	}
}

type Client interface {
	PutRecords(deliveryStreamName string, messages ...*models.LedgermanMessage) error
}

func NewClient() Client {
	return &client{}
}

type client struct {
	Firehose firehoseiface.FirehoseAPI `inject:""`
}

func (c *client) PutRecords(deliveryStreamName string, messages ...*models.LedgermanMessage) error {
	records := make([]*firehose.Record, 0)

	for _, transaction := range messages {
		for _, token := range transaction.Tokens {
			transactionRecord := ToTransactionRecord(transaction.TransactionID, transaction.Timestamp, transaction.Operation, token)

			transactionBytes, err := json.Marshal(&transactionRecord)
			if err != nil {
				return err
			}

			records = append(records, &firehose.Record{Data: transactionBytes})
		}
	}

	_, err := c.Firehose.PutRecordBatch(&firehose.PutRecordBatchInput{
		DeliveryStreamName: aws.String(deliveryStreamName),
		Records:            records,
	})

	return err
}
