package firehose

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/models"
	firehoseiface_mock "code.justin.tv/commerce/pachinko/mocks/github.com/aws/aws-sdk-go/service/firehose/firehoseiface"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestClient_PutRecords(t *testing.T) {
	Convey("given a firehose client wrapper", t, func() {
		firehoseClient := new(firehoseiface_mock.FirehoseAPI)

		client := &client{
			Firehose: firehoseClient,
		}

		deliverStream := "foobar"
		msg := &models.LedgermanMessage{
			TransactionID: uuid.NewV4().String(),
			Operation:     dynamo.ConsumeTokensOperation,
			Timestamp:     time.Now(),
			Tokens: []*pachinko.Token{
				{
					OwnerId:  random.String(16),
					GroupId:  random.String(16),
					Quantity: int64(322),
					Domain:   pachinko.Domain_STICKER,
				},
			},
		}

		Convey("when a records do not get put correctly", func() {
			firehoseClient.On("PutRecordBatch", mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

			err := client.PutRecords(deliverStream, msg)
			So(err, ShouldNotBeNil)
		})

		Convey("when we succed to put records", func() {
			firehoseClient.On("PutRecordBatch", mock.Anything).Return(nil, nil)

			err := client.PutRecords(deliverStream, msg)
			So(err, ShouldBeNil)
		})
	})
}
