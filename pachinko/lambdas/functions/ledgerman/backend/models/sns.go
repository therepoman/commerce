package models

type BalanceUpdate struct {
	TransactionIDs []string `json:"transaction_ids"`
}
