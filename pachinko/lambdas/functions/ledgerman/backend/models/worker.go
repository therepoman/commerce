package models

import (
	"time"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-lambda-go/events"
)

type LedgermanMessage struct {
	TransactionID string            `json:"transaction_id"`
	Domain        pachinko.Domain   `json:"domain"`
	Operation     dynamo.Operation  `json:"operation"`
	Timestamp     time.Time         `json:"timestamp"`
	Tokens        []*pachinko.Token `json:"tokens"`
}

type LedgermanInput struct {
	IsSmokeTest bool `json:"is_smoke_test"`
	events.DynamoDBEvent
}
