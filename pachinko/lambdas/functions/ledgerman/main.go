package main

import (
	"math/rand"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/codegangsta/cli"
)

var environment string
var tenantDomain string

func main() {
	app := cli.NewApp()
	app.Name = "Pachinko"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
			EnvVar:      config.EnvironmentEnvironmentVariable,
		},
		cli.StringFlag{
			Name:        "tenant, t",
			Usage:       "Tenant identifier to use for running commands.",
			Destination: &tenantDomain,
			EnvVar:      config.DomainTenantEnvironmentVariable,
		},
	}

	app.Action = runLedgerman

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runLedgerman(c *cli.Context) {
	env := config.GetOrDefaultEnvironment(environment)

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	domain := cfg.Tenants.GetDomainByIdentifier(tenantDomain)
	if _, ok := pachinko.Domain_name[int32(domain)]; !ok {
		logrus.Panic("Invalid Domain passed to Lambda Environment")
	}

	cfg.TenantDomain = domain

	logrus.Infof("Loaded config: %s tenant: %s", cfg.Environment.Name, cfg.TenantDomain)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error initializing backend")
	}

	if env != config.SmokeTest {
		lambda.Start(be.LedgermanWorker().HandleRequest)
	}
}
