package models

type StartDeleteByGroupIdResp struct {
	ExecutionName string
	GroupIDCount  int
	GroupIDs      []string
}

type StartDeleteByGroupIdReq struct {
	IsSmokeTest bool `json:"is_smoke_test"`
}
