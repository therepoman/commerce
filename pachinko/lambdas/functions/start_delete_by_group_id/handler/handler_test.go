package start_delete_by_group_id

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"testing"

	lambda_common "code.justin.tv/commerce/pachinko/lambdas/common"

	sqs_client "code.justin.tv/commerce/pachinko/backend/clients/sqs"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	sfn_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sfn"
	sqs_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sqs"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a start_delete_by_group_id lambda handler", t, func() {
		sqsClient := new(sqs_mock.Client)
		sfnClient := new(sfn_mock.Client)

		sticker := "STICKER"
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
					ResourceIdentifier: sticker,
					DeleteBalanceByGroupIdSQSConfig: &tenant.SQSConfig{
						URL: "test-sqs-url",
					},
					DeleteBalanceByGroupIdStepFunctionConfig: &tenant.StepFunctionConfig{
						ARN: "test-sfn-arn",
					},
				},
			},
		}

		handler := &LambdaHandler{
			SQSClient: sqsClient,
			SFNClient: sfnClient,
			Config:    c,
		}

		ctx := context.Background()

		Convey("Test getSQSMessages", func() {
			numOfGroupIDs := MaxBatchSize + 100
			numOfPages := numOfGroupIDs / sqs_client.MaxReceivedMessage

			var testGroupIDs []*sqs.Message
			for i := 1; i <= numOfGroupIDs; i++ {
				groupId := fmt.Sprintf("test-group-id-%d", i)
				testGroupIDs = append(testGroupIDs, &sqs.Message{
					Body: &groupId,
				})
			}

			SQSURL, err := handler.getSQSURL()
			So(err, ShouldBeNil)
			So(SQSURL, ShouldEqual, "test-sqs-url")

			Convey("When queue has more messages than MaxBatchSize", func() {
				for i := 0; i < numOfPages; i++ {
					lowIndex := i * sqs_client.MaxReceivedMessage
					highIndex := lowIndex + sqs_client.MaxReceivedMessage
					sqsClient.On("ReceiveMessages", ctx, SQSURL, sqs_client.MaxReceivedMessage).Return(testGroupIDs[lowIndex:highIndex], nil).Once()
				}

				messages, err := handler.getSQSMessages(ctx, SQSURL)
				So(err, ShouldBeNil)
				So(len(messages), ShouldEqual, MaxBatchSize)
			})

			Convey("When queue has fewer messages than MaxBatchSize", func() {
				expectedMessageCount := 50

				for i := 0; i < expectedMessageCount/sqs_client.MaxReceivedMessage; i++ {
					lowIndex := i * sqs_client.MaxReceivedMessage
					highIndex := lowIndex + sqs_client.MaxReceivedMessage
					sqsClient.On("ReceiveMessages", ctx, SQSURL, sqs_client.MaxReceivedMessage).Return(testGroupIDs[lowIndex:highIndex], nil).Once()
				}
				sqsClient.On("ReceiveMessages", ctx, SQSURL, sqs_client.MaxReceivedMessage).Return([]*sqs.Message{}, nil).Once()

				messages, err := handler.getSQSMessages(ctx, SQSURL)
				So(err, ShouldBeNil)
				So(len(messages), ShouldEqual, expectedMessageCount)
			})
		})

		Convey("Test getUniqueGroupIdsFromMessages", func() {
			groupId := "test-group-id"
			var testGroupIDsRepeated []*sqs.Message
			for i := 1; i < 10; i++ {
				testGroupIDsRepeated = append(testGroupIDsRepeated, &sqs.Message{
					Body: &groupId,
				})
			}
			uniqueGroupIDs := lambda_common.GetUniqueItemsFromMessages(testGroupIDsRepeated)
			So(len(uniqueGroupIDs), ShouldEqual, 1)
			So(uniqueGroupIDs[0], ShouldEqual, groupId)
		})

		Convey("Test startStepFn", func() {
			SFNARN, err := handler.getSFNARN()
			So(err, ShouldBeNil)
			So(SFNARN, ShouldEqual, "test-sfn-arn")

			testGroupIDs := []string{
				"test-group-id-1",
				"test-group-id-2",
				"test-group-id-3",
			}

			Convey("Empty Group IDs", func() {
				executionName, err := handler.startStepFn(ctx, SFNARN, []string{})
				So(err, ShouldBeNil)
				So(executionName, ShouldBeNil)
			})

			Convey("Execution fails", func() {
				sfnClient.On("Execute", ctx, SFNARN, mock.Anything, mock.Anything).Return(errors.New("SOMETHING BAD HAPPENED"))
				executionName, err := handler.startStepFn(ctx, SFNARN, testGroupIDs)
				So(err, ShouldNotBeNil)
				So(executionName, ShouldBeNil)
			})

			Convey("Execution succeeds", func() {
				sfnClient.On("Execute", ctx, SFNARN, mock.Anything, mock.Anything).Return(nil)
				executionName, err := handler.startStepFn(ctx, SFNARN, testGroupIDs)
				So(err, ShouldBeNil)
				So(executionName, ShouldNotBeNil)
			})
		})
	})
}
