package start_delete_by_group_id

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	sfn_client "code.justin.tv/commerce/pachinko/backend/clients/sfn"
	sqs_client "code.justin.tv/commerce/pachinko/backend/clients/sqs"
	sfnWorker "code.justin.tv/commerce/pachinko/backend/sfn"
	"code.justin.tv/commerce/pachinko/config"
	lambda_common "code.justin.tv/commerce/pachinko/lambdas/common"
	"code.justin.tv/commerce/pachinko/lambdas/functions/start_delete_by_group_id/models"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	MaxBatchSize        = 10000
	ExecutionNamePrefix = "delete_by_group_ids"
)

type LambdaHandler struct {
	SQSClient sqs_client.Client `inject:""`
	SFNClient sfn_client.Client `inject:""`
	Config    *config.Config    `inject:""`
}

func (l *LambdaHandler) Handle(ctx context.Context, input models.StartDeleteByGroupIdReq) (*models.StartDeleteByGroupIdResp, error) {
	if input.IsSmokeTest {
		return nil, nil
	}

	SFNARN, err := l.getSFNARN()
	if err != nil {
		logrus.WithError(err).Error("Failed to get SFN ARN")
		return nil, err
	}

	SQSURL, err := l.getSQSURL()
	if err != nil {
		logrus.WithError(err).Error("Failed to get SQS URL")
		return nil, err
	}

	msgs, err := l.getSQSMessages(ctx, SQSURL)
	if err != nil {
		logrus.WithError(err).Error("Failed to get Group IDs from SQS")
		return nil, err
	}

	uniqueGroupIDs := lambda_common.GetUniqueItemsFromMessages(msgs)
	executionName, err := l.startStepFn(ctx, SFNARN, uniqueGroupIDs)
	if err != nil {
		logrus.WithError(err).Error("Failed to start Step Fn execution")
		return nil, err
	}

	err = l.deleteSQSMessages(ctx, SQSURL, msgs)
	if err != nil {
		logrus.WithError(err).Error("Failed to delete Group IDs from SQS")
		return nil, err
	}

	resp := &models.StartDeleteByGroupIdResp{
		GroupIDCount: len(uniqueGroupIDs),
		GroupIDs:     uniqueGroupIDs,
	}
	if executionName != nil {
		resp.ExecutionName = *executionName
	}

	return resp, nil
}

func (l *LambdaHandler) getSQSMessages(ctx context.Context, SQSURL string) ([]*sqs.Message, error) {
	var messages []*sqs.Message

	retry := true
	for retry {
		batch, err := l.SQSClient.ReceiveMessages(ctx, SQSURL, sqs_client.MaxReceivedMessage)
		if err != nil {
			return messages, err
		}

		for _, item := range batch {
			messages = append(messages, item)
			if len(messages) >= MaxBatchSize {
				// the unappended messages will be added back to the queue since we don't delete them later
				return messages, nil
			}
		}
		retry = len(batch) != 0 // exit on first empty receive
	}
	return messages, nil
}

func (l *LambdaHandler) startStepFn(ctx context.Context, SFNARN string, groupIDs []string) (*string, error) {
	if len(groupIDs) > 0 {
		executionName := getExecutionName(groupIDs)
		executionInput := sfnWorker.DeleteBalanceByGroupIdStateMachineInput{
			GroupIds: groupIDs,
		}

		err := l.SFNClient.Execute(ctx, SFNARN, executionName, executionInput)
		if err != nil {
			return nil, err
		}
		return &executionName, nil
	} else {
		logrus.Info("Empty Group IDs, skipping execution")
		return nil, nil
	}
}

func (l *LambdaHandler) deleteSQSMessages(ctx context.Context, SQSURL string, messagesToDelete []*sqs.Message) error {
	return l.SQSClient.DeleteMessages(ctx, SQSURL, messagesToDelete)
}

func (l *LambdaHandler) getSFNARN() (string, error) {
	var SFNARN string

	tenantConf := l.Config.Tenants.Get(l.Config.TenantDomain)
	if tenantConf == nil {
		return SFNARN, errors.New("Failed to get tenant config")
	}

	if tenantConf.DeleteBalanceByGroupIdStepFunctionConfig != nil {
		SFNARN = tenantConf.DeleteBalanceByGroupIdStepFunctionConfig.ARN
	}

	if SFNARN == "" {
		return SFNARN, errors.New("Failed to get ARN of step function")
	}

	return SFNARN, nil
}

func (l *LambdaHandler) getSQSURL() (string, error) {
	var SQSURL string

	tenantConf := l.Config.Tenants.Get(l.Config.TenantDomain)
	if tenantConf == nil {
		return SQSURL, errors.New("Failed to get tenant config")
	}

	if tenantConf.DeleteBalanceByGroupIdStepFunctionConfig != nil {
		SQSURL = tenantConf.DeleteBalanceByGroupIdSQSConfig.URL
	}

	if SQSURL == "" {
		return SQSURL, errors.New("Failed to get URL of DeleteBalanceByGroupId SQS Queue")
	}

	return SQSURL, nil
}

// The naming strategy is to just use the first id in the list with the batchsize.
func getExecutionName(groupIds []string) string {
	return fmt.Sprintf("%s.%s(%d).%d", ExecutionNamePrefix, groupIds[0], len(groupIds), time.Now().UnixNano())
}
