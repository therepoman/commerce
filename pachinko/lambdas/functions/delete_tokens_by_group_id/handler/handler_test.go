package delete_tokens_by_group_id

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/random"
	pachinko_dynamo "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_tokens_by_group_id/models"
	ddb_tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	ddb_tokens_scan_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens_scan"
	workerPoolMock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a tokens lambda handler", t, func() {
		tokensDAO := new(ddb_tokens_mock.DAO)
		tokensScanDAO := new(ddb_tokens_scan_mock.DAO)
		workerPoolMock := new(workerPoolMock.WorkerPool)
		cfg := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
		}

		handler := &LambdaHandler{
			Config:     cfg,
			DAO:        tokensDAO,
			ScanDAO:    tokensScanDAO,
			WorkerPool: workerPoolMock,
		}

		ctx := context.Background()
		groupIds := []string{random.String(16), random.String(16), random.String(16)}
		ownerId := random.String(16)
		input := models.DeleteTokensByGroupIdReq{
			GroupIds: groupIds,
			Jobs:     nil,
		}

		tokens := make([]pachinko_dynamo.Token, 0)
		for i, groupId := range groupIds {
			tokens = append(tokens, pachinko_dynamo.Token{
				OwnerID: ownerId,
				GroupID: groupId,
				Balance: i + 1,
			})
		}

		pagingKey := pachinko_dynamo.PagingKey{
			"item1": &dynamodb.AttributeValue{
				S: pointers.StringP("foobar"),
			},
		}

		Convey("Test Handle", func() {
			Convey("returns successfully for smoke test", func() {
				input := models.DeleteTokensByGroupIdReq{
					IsSmokeTest: true,
				}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("throws an error when the input doesnt contain GroupIds", func() {
				input := models.DeleteTokensByGroupIdReq{}
				resp, err := handler.Handle(ctx, input)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when there are no initial scan jobs", func() {
				Convey("A worker pool is executed for scanning and deleting by ownerIds", func() {
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), MaxWorkerCount).Return(nil, nil)
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &models.DeleteTokensByGroupIdResp{
						GroupIds:       groupIds,
						IsScanComplete: true,
						Jobs:           nil,
					})
				})

				Convey("An error is returned when the worker pool fails to execute the jobs", func() {
					workerPoolMock.On("Execute", mock.AnythingOfType("[]*worker_pool.Job"),
						mock.AnythingOfType("worker_pool.WorkerFn"), MaxWorkerCount).Return(nil, errors.New("failed to execute jobs"))
					resp, err := handler.Handle(ctx, input)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})
		})

		Convey("Test scanAndDeleteByGroupId", func() {
			job := worker_pool.Job{
				PagingKey:     nil,
				Segment:       0,
				TotalSegments: 1,
			}

			Convey("when there is an error getting all Tokens matching the groupIds", func() {
				tokensScanDAO.On("ScanTokens", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(nil, nil, errors.New("failed to get tokens"))
				result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
				So(result.Err, ShouldNotBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are no Tokens matching the given GroupId", func() {
				tokensScanDAO.On("ScanTokens", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(nil, nil, nil)
				tokensDAO.On("BatchDeleteTokens", ctx, []dynamo.Keyed{}).Return(nil)
				result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
				So(result.Err, ShouldBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are Tokens matching the given GroupId", func() {
				tokensScanDAO.On("ScanTokens", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(tokens, nil, nil)
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupIds[0]},
					dynamo.Keys{ownerId, groupIds[1]},
					dynamo.Keys{ownerId, groupIds[2]},
				}

				Convey("when there is an error deleting tokens", func() {
					tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(errors.New("failed to delete tokens"))
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldNotBeNil)
					So(result.NextJob, ShouldBeNil)
				})

				Convey("when deleting the tokens is successful", func() {
					tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(nil)
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})

			Convey("when there are more Tokens to scan in the table", func() {
				tokensScanDAO.On("ScanTokens", ctx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(tokens, pagingKey, nil).Once()
				tokensScanDAO.On("ScanTokens", ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments).Return(tokens, pagingKey, nil)
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupIds[0]},
					dynamo.Keys{ownerId, groupIds[1]},
					dynamo.Keys{ownerId, groupIds[2]},
				}
				tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(nil)

				Convey("when deleting the tokens is successful", func() {
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						TotalSegments: job.TotalSegments,
						Segment:       job.Segment,
						PagingKey:     pagingKey,
					})
				})
			})

			Convey("when the lambda is close to the execution timeout", func() {
				now := time.Now()
				shortCtx, cancel := context.WithDeadline(ctx, now.Add((DeadlineLimit-5)*time.Second))
				defer cancel()
				// expect only one ScanTokens call
				tokensScanDAO.On("ScanTokens", shortCtx, ScanLimit, pachinko_dynamo.PagingKey(nil), job.Segment, job.TotalSegments).Return(tokens, pagingKey, nil).Once()
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupIds[0]},
					dynamo.Keys{ownerId, groupIds[1]},
					dynamo.Keys{ownerId, groupIds[2]},
				}
				tokensDAO.On("BatchDeleteTokens", shortCtx, keys).Return(nil)
				result := handler.scanAndDeleteByGroupId(shortCtx, job, groupIds)

				Convey("stops scanning and returns a followup job", func() {
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						TotalSegments: job.TotalSegments,
						Segment:       job.Segment,
						PagingKey:     pagingKey,
					})
				})
			})

			Convey("when given a paging key it passes it to ScanTokens", func() {
				job.PagingKey = pagingKey
				tokensScanDAO.On("ScanTokens", ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments).Return(tokens, nil, nil)
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupIds[0]},
					dynamo.Keys{ownerId, groupIds[1]},
					dynamo.Keys{ownerId, groupIds[2]},
				}
				tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(nil)

				Convey("when deleting the tokens is successful", func() {
					result := handler.scanAndDeleteByGroupId(ctx, job, groupIds)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})
		})

		Convey("Test queryAndDeleteByGroupId", func() {
			groupId := random.String(16)
			job := worker_pool.Job{
				Id: groupId,
			}
			matchingTokens := []pachinko_dynamo.Token{{
				OwnerID: ownerId,
				GroupID: groupId,
			}}

			Convey("when there is an error getting all Tokens matching the groupIds", func() {
				tokensScanDAO.On("QueryTokensByGroupId", ctx, QueryLimit, pachinko_dynamo.PagingKey(nil), job.Id).Return(nil, nil, errors.New("failed to query tokens"))
				result := handler.queryAndDeleteByGroupId(ctx, job)
				So(result.Err, ShouldNotBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are no Tokens matching the given GroupId", func() {
				tokensScanDAO.On("QueryTokensByGroupId", ctx, QueryLimit, pachinko_dynamo.PagingKey(nil), job.Id).Return(nil, nil, nil)
				tokensDAO.On("BatchDeleteTokens", ctx, []dynamo.Keyed{}).Return(nil)
				result := handler.queryAndDeleteByGroupId(ctx, job)
				So(result.Err, ShouldBeNil)
				So(result.NextJob, ShouldBeNil)
			})

			Convey("when there are Tokens matching the given GroupId", func() {
				tokensScanDAO.On("QueryTokensByGroupId", ctx, QueryLimit, pachinko_dynamo.PagingKey(nil), job.Id).Return(matchingTokens, nil, nil)
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupId},
				}

				Convey("when there is an error deleting tokens", func() {
					tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(errors.New("failed to delete tokens"))
					result := handler.queryAndDeleteByGroupId(ctx, job)
					So(result.Err, ShouldNotBeNil)
					So(result.NextJob, ShouldBeNil)
				})

				Convey("when deleting the tokens is successful", func() {
					tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(nil)
					result := handler.queryAndDeleteByGroupId(ctx, job)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})

			Convey("when there are more Tokens to query in the index", func() {
				tokensScanDAO.On("QueryTokensByGroupId", ctx, QueryLimit, pachinko_dynamo.PagingKey(nil), job.Id).Return(matchingTokens, pagingKey, nil).Once()
				tokensScanDAO.On("QueryTokensByGroupId", ctx, QueryLimit, pagingKey, job.Id).Return(matchingTokens, pagingKey, nil)
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupId},
				}
				tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(nil)

				Convey("when deleting the tokens is successful", func() {
					result := handler.queryAndDeleteByGroupId(ctx, job)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						Id:        groupId,
						PagingKey: pagingKey,
					})
				})
			})

			Convey("when the lambda is close to the execution timeout", func() {
				now := time.Now()
				shortCtx, cancel := context.WithDeadline(ctx, now.Add((DeadlineLimit-5)*time.Second))
				defer cancel()
				// expect only one QueryTokensByGroupId call
				tokensScanDAO.On("QueryTokensByGroupId", shortCtx, QueryLimit, pachinko_dynamo.PagingKey(nil), job.Id).Return(matchingTokens, pagingKey, nil).Once()
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupId},
				}
				tokensDAO.On("BatchDeleteTokens", shortCtx, keys).Return(nil)

				Convey("stops querying and returns a followup job", func() {
					result := handler.queryAndDeleteByGroupId(shortCtx, job)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldResemble, &worker_pool.Job{
						Id:        groupId,
						PagingKey: pagingKey,
					})
				})
			})

			Convey("when given a paging key it passes it to QueryTokensByGroupId", func() {
				job.PagingKey = pagingKey
				tokensScanDAO.On("QueryTokensByGroupId", ctx, QueryLimit, pagingKey, job.Id).Return(matchingTokens, nil, nil)
				keys := []dynamo.Keyed{
					dynamo.Keys{ownerId, groupId},
				}
				tokensDAO.On("BatchDeleteTokens", ctx, keys).Return(nil)

				Convey("when deleting the tokens is successful", func() {
					result := handler.queryAndDeleteByGroupId(ctx, job)
					So(result.Err, ShouldBeNil)
					So(result.NextJob, ShouldBeNil)
				})
			})
		})
	})
}
