package delete_tokens_by_group_id

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/slice"
	"code.justin.tv/commerce/logrus"
	tokensDb "code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	tokensScanDb "code.justin.tv/commerce/pachinko/backend/dynamo/tokens_scan"
	workerPool "code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_tokens_by_group_id/models"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
)

const (
	ScanLimit      = int64(2000)
	QueryLimit     = int64(2000)
	MaxIterations  = 300
	MaxWorkerCount = 10
	DeadlineLimit  = 30
)

type LambdaHandler struct {
	Config     *config.Config        `inject:""`
	DAO        tokensDb.DAO          `inject:""`
	ScanDAO    tokensScanDb.DAO      `inject:""`
	WorkerPool workerPool.WorkerPool `inject:""`
}

func (l *LambdaHandler) Handle(ctx context.Context, input models.DeleteTokensByGroupIdReq) (*models.DeleteTokensByGroupIdResp, error) {
	if input.IsSmokeTest {
		return nil, nil
	}

	groupIds := input.GroupIds
	if len(groupIds) == 0 {
		return nil, errors.New("failed to delete tokens, groupIds was empty")
	}

	jobs := input.Jobs
	workerCount := len(jobs)
	if len(input.Jobs) == 0 {
		workerCount = MaxWorkerCount
		jobs = make([]*workerPool.Job, 0)

		if l.Config.TenantDomain == pachinko.Domain_COMMUNITY_POINTS {
			// if CoPo tenant and no initial jobs, create query jobs for GSI for each groupId
			for _, groupId := range groupIds {
				jobs = append(jobs, &workerPool.Job{
					Id: groupId,
				})
			}
		} else {
			// if no initial jobs, create 10 parallel scan jobs
			for i := 0; i < MaxWorkerCount; i++ {
				jobs = append(jobs, &workerPool.Job{
					Segment:       int64(i),
					TotalSegments: MaxWorkerCount,
				})
			}
		}
	}

	workerFn := func(job workerPool.Job) workerPool.Result {
		return l.scanAndDeleteByGroupId(ctx, job, groupIds)
	}
	// Only CoPo tenant has a GSI on groupId for Tokens
	if l.Config.TenantDomain == pachinko.Domain_COMMUNITY_POINTS {
		workerFn = func(job workerPool.Job) workerPool.Result {
			return l.queryAndDeleteByGroupId(ctx, job)
		}
	}

	newJobs, err := l.WorkerPool.Execute(jobs, workerFn, workerCount)
	if err != nil {
		return nil, err
	}

	return &models.DeleteTokensByGroupIdResp{
		GroupIds:       groupIds,
		IsScanComplete: len(newJobs) == 0,
		Jobs:           newJobs,
	}, nil
}

func (l *LambdaHandler) scanAndDeleteByGroupId(ctx context.Context, job workerPool.Job, groupIds []string) workerPool.Result {
	groupIdMap := slice.ToStringMap(groupIds)
	pagingKey := job.PagingKey

	for i := 0; i < MaxIterations; i++ {
		results, newPagingKey, err := l.ScanDAO.ScanTokens(ctx, ScanLimit, pagingKey, job.Segment, job.TotalSegments)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"groupIds":    groupIds,
				"iteration":   i,
				"job.Segment": job.Segment,
				"pagingKey":   pagingKey,
			}).Error("failed to scan tokens")
			return workerPool.Result{
				Err: err,
			}
		}

		keys := make([]dynamo.Keyed, 0)

		for _, token := range results {
			if _, ok := groupIdMap[token.GroupID]; ok {
				keys = append(keys, dynamo.Keys{token.OwnerID, token.GroupID})
			}
		}

		// Delete tokens in DDB
		err = l.DAO.BatchDeleteTokens(ctx, keys)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"groupIds":    groupIds,
				"iteration":   i,
				"job.Segment": job.Segment,
				"pagingKey":   pagingKey,
			}).Error("could not delete tokens from given keys")
			return workerPool.Result{
				Err: err,
			}
		}

		pagingKey = newPagingKey
		if len(pagingKey) == 0 {
			return workerPool.Result{}
		}

		// if we are almost out of time, stop execution to continue later
		deadline, _ := ctx.Deadline()
		now := time.Now()
		if deadline.Unix()-now.Unix() < DeadlineLimit {
			logrus.WithFields(logrus.Fields{
				"job.Id":    job.Id,
				"iteration": i,
				"deadline":  deadline.Unix(),
			}).Info("timeout limit almost exceeded, will continue in another execution")
			break
		}
	}

	// if we hit MaxIterations and still have a pagingKey then return a job to be run in a subsequent execution
	return workerPool.Result{
		NextJob: &workerPool.Job{
			Segment:       job.Segment,
			TotalSegments: job.TotalSegments,
			PagingKey:     pagingKey,
		},
	}
}

func (l *LambdaHandler) queryAndDeleteByGroupId(ctx context.Context, job workerPool.Job) workerPool.Result {
	pagingKey := job.PagingKey

	for i := 0; i < MaxIterations; i++ {
		results, newPagingKey, err := l.ScanDAO.QueryTokensByGroupId(ctx, QueryLimit, pagingKey, job.Id)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"job.Id":    job.Id,
				"iteration": i,
				"pagingKey": pagingKey,
			}).Error("failed to query tokens")
			return workerPool.Result{
				Err: err,
			}
		}

		keys := make([]dynamo.Keyed, 0)
		for _, token := range results {
			keys = append(keys, dynamo.Keys{token.OwnerID, token.GroupID})
		}

		// Delete tokens in DDB
		err = l.DAO.BatchDeleteTokens(ctx, keys)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"job.Id":    job.Id,
				"iteration": i,
				"pagingKey": pagingKey,
			}).Error("could not delete tokens from given keys")
			return workerPool.Result{
				Err: err,
			}
		}

		pagingKey = newPagingKey
		if len(pagingKey) == 0 {
			return workerPool.Result{}
		}

		// if we are almost out of time, stop execution to continue later
		deadline, _ := ctx.Deadline()
		now := time.Now()
		if deadline.Unix()-now.Unix() < DeadlineLimit {
			logrus.WithFields(logrus.Fields{
				"job.Id":    job.Id,
				"iteration": i,
				"deadline":  deadline.Unix(),
			}).Info("timeout limit almost exceeded, will continue in another execution")
			break
		}
	}

	// if we still have a pagingKey then return a job to be run in a subsequent execution
	return workerPool.Result{
		NextJob: &workerPool.Job{
			Id:        job.Id,
			PagingKey: pagingKey,
		},
	}
}
