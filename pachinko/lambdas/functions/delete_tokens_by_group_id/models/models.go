package models

import (
	workerPool "code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool"
)

type DeleteTokensByGroupIdReq struct {
	IsSmokeTest bool              `json:"is_smoke_test"`
	GroupIds    []string          `json:"group_ids"`
	Jobs        []*workerPool.Job `json:"jobs"`
}

type DeleteTokensByGroupIdResp struct {
	GroupIds       []string          `json:"group_ids"`
	IsScanComplete bool              `json:"is_scan_complete"`
	Jobs           []*workerPool.Job `json:"jobs"`
}
