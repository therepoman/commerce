package models

type DeleteTokensByOwnerIdReq struct {
	IsSmokeTest bool     `json:"is_smoke_test"`
	OwnerIds    []string `json:"owner_ids"`
}

type DeleteTokensByOwnerIdResp struct {
	OwnerIds []string `json:"owner_ids"`
}
