package delete_tokens_by_owner_id

import (
	"context"
	"sync"

	"code.justin.tv/commerce/logrus"
	tokensDb "code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_tokens_by_owner_id/models"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

type LambdaHandler struct {
	DAO    tokensDb.DAO  `inject:""`
	Getter tokens.Getter `inject:""`
}

const (
	MaxBatchSize = 50
)

func (l *LambdaHandler) Handle(ctx context.Context, input models.DeleteTokensByOwnerIdReq) (*models.DeleteTokensByOwnerIdResp, error) {
	if input.IsSmokeTest {
		return nil, nil
	}

	ownerIds := input.OwnerIds
	if len(ownerIds) == 0 {
		return nil, errors.New("failed to delete tokens, ownerIds was empty")
	}

	// process these ids in batches to not overload the system
	for i := 0; i < len(ownerIds); i += MaxBatchSize {
		endIndex := i + MaxBatchSize
		if endIndex >= len(ownerIds) {
			endIndex = len(ownerIds)
		}
		ownerIdsBatch := ownerIds[i:endIndex]

		var wg sync.WaitGroup
		errChan := make(chan error)
		doneChan := make(chan bool)

		for _, ownerId := range ownerIdsBatch {
			wg.Add(1)

			go func(oId string) {
				defer wg.Done()

				err := l.deleteTokensByOwnerId(ctx, oId)
				if err != nil {
					logrus.WithError(err).WithFields(logrus.Fields{
						"ownerId":  oId,
						"ownerIds": ownerIdsBatch,
					}).Error("could not delete tokens")
					errChan <- err
				}
			}(ownerId)
		}

		go func() {
			wg.Wait()
			close(doneChan)
		}()

		select {
		case err := <-errChan:
			return nil, err
		case <-doneChan:
			break
		}
	}

	return &models.DeleteTokensByOwnerIdResp{
		OwnerIds: ownerIds,
	}, nil
}

func (l *LambdaHandler) deleteTokensByOwnerId(ctx context.Context, ownerId string) error {
	tokens, err := l.Getter.GetAll(ctx, ownerId)
	if err != nil {
		return errors.Errorf("failed to get tokens with: %v", err)
	}

	if len(tokens) == 0 {
		return nil
	}

	keys := make([]dynamo.Keyed, len(tokens))
	for i, token := range tokens {
		keys[i] = dynamo.Keys{ownerId, token.GroupId}
	}

	// Delete tokens in DDB
	err = l.DAO.BatchDeleteTokens(ctx, keys)
	if err != nil {
		return errors.Errorf("failed to delete tokens with: %v", err)
	}

	return nil
}
