package delete_tokens_by_owner_id

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachinko/lambdas/functions/delete_tokens_by_owner_id/models"
	ddb_tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	tokens "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a tokens lambda handler", t, func() {
		tokensDAO := new(ddb_tokens_mock.DAO)
		tokensGetter := new(tokens.Getter)

		handler := &LambdaHandler{
			DAO:    tokensDAO,
			Getter: tokensGetter,
		}

		ctx := context.Background()
		ownerIds := []string{"owner-id-0", "owner-id-1", "owner-id-2"}

		Convey("returns successfully for smoke test", func() {
			input := models.DeleteTokensByOwnerIdReq{
				IsSmokeTest: true,
			}
			resp, err := handler.Handle(ctx, input)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("when the input doesnt contain ownerIds", func() {
			input := models.DeleteTokensByOwnerIdReq{}

			Convey("then an error is returned", func() {
				_, err := handler.Handle(ctx, input)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the input has ownerIds", func() {
			input := models.DeleteTokensByOwnerIdReq{
				OwnerIds: ownerIds,
			}

			Convey("when there are Tokens matching the ownerIds", func() {
				groupId := "group-id"
				for _, ownerId := range ownerIds {
					tokens := []*pachinko.Token{
						{
							OwnerId:  ownerId,
							GroupId:  groupId,
							Quantity: 3,
							Domain:   pachinko.Domain_STICKER,
						},
						{
							OwnerId:  ownerId,
							GroupId:  groupId,
							Quantity: 3,
							Domain:   pachinko.Domain_STICKER,
						},
					}

					tokensGetter.On("GetAll", ctx, ownerId).Return(tokens, nil)
				}

				Convey("when there is a single error deleting tokens", func() {
					for i, ownerId := range ownerIds {
						var err error
						if i == 0 {
							err = errors.New("Failed to delete tokens")
						}

						tokensDAO.On("BatchDeleteTokens", ctx, []dynamo.Keyed{
							dynamo.Keys{ownerId, groupId},
							dynamo.Keys{ownerId, groupId},
						}).Return(err)
					}

					Convey("then an error is returned", func() {
						_, err := handler.Handle(ctx, input)
						So(len(tokensDAO.Calls), ShouldBeLessThanOrEqualTo, len(ownerIds)) // this is because we will return early on the first error
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when deleting the tokens is successful", func() {
					for _, ownerId := range ownerIds {
						tokensDAO.On("BatchDeleteTokens", ctx, []dynamo.Keyed{
							dynamo.Keys{ownerId, groupId},
							dynamo.Keys{ownerId, groupId},
						}).Return(nil)
					}

					resp, err := handler.Handle(ctx, input)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &models.DeleteTokensByOwnerIdResp{
						OwnerIds: ownerIds,
					})
				})
			})
		})
	})
}
