package backend

import (
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	cache_tokens "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	"code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp"
	"code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	redis_client "code.justin.tv/commerce/pachinko/backend/circuit/redis-client"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	dynamo_tokens "code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/lambdas/functions/add_tokens_processor/backend/worker"
	"code.justin.tv/commerce/splatter"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
)

type Backend interface {
	GetWorker() worker.Worker
}

type backend struct {
	// Config
	Config *config.Config `inject:""`

	// The worker that is used by the Lambda
	Worker worker.Worker `inject:""`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetrics != nil {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.Environment.AWSRegion,
			ServiceName:       "pachinko",
			Stage:             cfg.Environment.Name,
			Substage:          cfg.TenantDomain.String(),
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	} else {
		logrus.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	hystrixMetrics := metrics.NewHystrixMetricsCollector()

	tenantTokensRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-tokens")
	tenantTxTsRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-tx-ts")

	deps := []*inject.Object{
		// Backend (graph root)
		{Value: b},

		// Config
		{Value: cfg},

		// Metrics
		{Value: stats},
		{Value: metrics.NewStatter()},
		{Value: hystrixMetrics},

		// Cache Clients
		{Value: redis.NewClient(tenantTokensRedisClient), Name: "redis-tokens"},
		{Value: redis.NewClient(tenantTxTsRedisClient), Name: "redis-tx-ts"},

		// Caches
		{Value: cache_tokens.NewCache()},
		{Value: transaction_timestamp.NewCache()},

		// DAOs
		{Value: dynamo_tx.NewDAOWithClient(dynamo.DynamoClient(sess))},
		{Value: dynamo_tokens.NewDAOWithClient(dynamo.DynamoClient(sess))},

		// Entitler
		{Value: tokens.NewEntitler()},

		// Worker
		{Value: worker.NewWorker()},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixMetrics.NewHystrixCommandMetricsCollector(name)
	})

	logrus.Info("Backend initialization complete")

	return b, nil
}

func (b *backend) GetWorker() worker.Worker {
	return b.Worker
}
