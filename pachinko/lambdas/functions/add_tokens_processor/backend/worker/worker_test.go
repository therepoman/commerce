package worker

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/sns"
	"code.justin.tv/commerce/pachinko/config"
	tx_dao_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-lambda-go/events"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestWorker_HandleRequest(t *testing.T) {
	Convey("given a add tokens processor", t, func() {
		entitler := new(tokens_mock.Entitler)
		transactionsDAO := new(tx_dao_mock.DAO)
		cfg := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
		}

		w := &worker{
			Entitler:        entitler,
			TransactionsDAO: transactionsDAO,
			Config:          cfg,
		}

		ctx := context.Background()

		Convey("returns error when", func() {
			var errorString string
			Convey("we fail to fetch transactions from DB", func() {
				transactionsDAO.On("BatchGetTransactionRecords", ctx, []string{"test", "failedTx"}).Return(nil, errors.New("WALRUS STRIKE"))
				errorString = "failed to check transactions in DB"
			})

			Convey("we fail to entitle tokens in the request for any amount of failures", func() {
				transactionsDAO.On("BatchGetTransactionRecords", ctx, []string{"test", "failedTx"}).Return(nil, nil)
				entitler.On("Entitle", ctx, "test", mock.Anything).Return(nil, nil)
				entitler.On("Entitle", ctx, "failedTx", mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))
				errorString = "failed to entitle some tokens"
			})

			result, err := w.HandleRequest(ctx, getTestSQSEvent([]events.SQSMessage{
				createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
					TransactionID: "test",
					Domain:        pachinko.Domain_STICKER,
					OwnerID:       "walrus",
					GroupID:       "BALANCE",
					Amount:        45,
				})),
				createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
					TransactionID: "failedTx",
					Domain:        pachinko.Domain_STICKER,
					OwnerID:       "walrus",
					GroupID:       "BALANCE",
					Amount:        45,
				})),
			}))
			So(result, ShouldEqual, errorString)
			So(err, ShouldNotBeNil)
			transactionsDAO.AssertExpectations(t)
			entitler.AssertExpectations(t)
		})

		Convey("returns nil when", func() {
			var event events.SQSEvent
			Convey("fails to parse SNS message", func() {
				event = getTestSQSEvent([]events.SQSMessage{{Body: "THIS IS INVALID"}})
			})

			Convey("fails to parse Add Tokens message", func() {
				snsMsg := sns.SNSMessage{Message: "THIS IS NOT VALID"}
				body, _ := json.Marshal(snsMsg)
				event = getTestSQSEvent([]events.SQSMessage{{Body: string(body)}})
			})

			Convey("domain is not correct for processor", func() {
				event = getTestSQSEvent([]events.SQSMessage{
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_BITS,
						TransactionID: "test",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
				})
			})

			Convey("duplicate transactions found in batch", func() {
				event = getTestSQSEvent([]events.SQSMessage{
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_STICKER,
						TransactionID: "test",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_STICKER,
						TransactionID: "test",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
				})
				transactionsDAO.On("BatchGetTransactionRecords", ctx, []string{"test"}).Return(nil, nil)
				entitler.On("Entitle", ctx, "test", mock.Anything).Return(nil, nil)
			})

			Convey("transaction already committed", func() {
				event = getTestSQSEvent([]events.SQSMessage{
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_STICKER,
						TransactionID: "test",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_STICKER,
						TransactionID: "alreadyCommitted",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
				})
				transactionsDAO.On("BatchGetTransactionRecords", ctx, []string{"test", "alreadyCommitted"}).Return([]dynamo.Transaction{
					{
						ID:          "alreadyCommitted",
						IsCommitted: true,
					},
				}, nil)
				entitler.On("Entitle", ctx, "test", mock.Anything).Return(nil, nil)
			})

			Convey("successfully entitle tokens", func() {
				event = getTestSQSEvent([]events.SQSMessage{
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_STICKER,
						TransactionID: "test",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
					createTestSQSEvent(createTestSNSEvent(sns.AddTokensMessage{
						Domain:        pachinko.Domain_STICKER,
						TransactionID: "otherTx",
						OwnerID:       "walrus",
						GroupID:       "BALANCE",
						Amount:        45,
					})),
				})
				transactionsDAO.On("BatchGetTransactionRecords", ctx, []string{"test", "otherTx"}).Return(nil, nil)
				entitler.On("Entitle", ctx, "test", mock.Anything).Return(nil, nil)
				entitler.On("Entitle", ctx, "otherTx", mock.Anything).Return(nil, nil)
			})

			result, err := w.HandleRequest(ctx, event)
			So(result, ShouldEqual, "successfully processed transactions")
			So(err, ShouldBeNil)
			transactionsDAO.AssertExpectations(t)
			entitler.AssertExpectations(t)
		})
	})
}

func createTestSNSEvent(req sns.AddTokensMessage) sns.SNSMessage {
	addTokensReqJSON, _ := json.Marshal(req)

	return sns.SNSMessage{
		Message: string(addTokensReqJSON),
	}
}

func createTestSQSEvent(req sns.SNSMessage) events.SQSMessage {
	body, _ := json.Marshal(req)

	return events.SQSMessage{
		Body: string(body),
	}
}

func getTestSQSEvent(records []events.SQSMessage) events.SQSEvent {
	return events.SQSEvent{
		Records: records,
	}
}
