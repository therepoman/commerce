package worker

import (
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/sns"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-lambda-go/events"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

type Worker interface {
	HandleRequest(ctx context.Context, event events.SQSEvent) (string, error)
}

type worker struct {
	Config          *config.Config   `inject:""`
	TransactionsDAO transactions.DAO `inject:""`
	Entitler        tokens.Entitler  `inject:""`
}

func NewWorker() *worker {
	return &worker{}
}

func (w *worker) HandleRequest(ctx context.Context, event events.SQSEvent) (string, error) {
	transactionsInBatch := make(map[string]sns.AddTokensMessage)
	transactionIDs := make([]string, 0)

	for _, record := range event.Records {
		var snsMsg sns.SNSMessage
		err := json.Unmarshal([]byte(record.Body), &snsMsg)
		if err != nil {
			logrus.WithField("message", record.Body).Warn("message was not unmarshalable into SNS message type")
			continue
		}

		var addTokensReq sns.AddTokensMessage
		err = json.Unmarshal([]byte(snsMsg.Message), &addTokensReq)
		if err != nil {
			logrus.WithField("message", snsMsg.Message).Warn("SNS message was not unmarshalable into Add Tokens message type")
			continue
		}

		if addTokensReq.Domain != w.Config.TenantDomain {
			logrus.WithFields(logrus.Fields{
				"transactionID": addTokensReq.TransactionID,
				"tenantDomain":  w.Config.TenantDomain,
				"requestDomain": addTokensReq.Domain,
			}).Warn("event contained incorrect domain")
			continue
		}

		if _, ok := transactionsInBatch[addTokensReq.TransactionID]; ok {
			logrus.WithFields(logrus.Fields{
				"transactionID": addTokensReq.TransactionID,
				"ownerID":       addTokensReq.OwnerID,
				"groupID":       addTokensReq.GroupID,
				"amount":        addTokensReq.Amount,
			}).Warn("event received contained a duplicate transaction ID")
			continue
		}

		transactionsInBatch[addTokensReq.TransactionID] = addTokensReq
		transactionIDs = append(transactionIDs, addTokensReq.TransactionID)
	}

	if len(transactionsInBatch) < 1 {
		return "successfully processed transactions", nil
	}

	txRecords, err := w.TransactionsDAO.BatchGetTransactionRecords(ctx, transactionIDs)
	if err != nil {
		return "failed to check transactions in DB", err
	}

	for _, record := range txRecords {
		// this allows us to filter out already processed transactions in the event of a redrive.
		if record.IsCommitted {
			delete(transactionsInBatch, record.ID)
		}
	}

	errs := &multierror.Error{}

	for _, request := range transactionsInBatch {
		_, err := w.Entitler.Entitle(ctx, request.TransactionID, pachinko.Token{
			Domain:   request.Domain,
			OwnerId:  request.OwnerID,
			GroupId:  request.GroupID,
			Quantity: request.Amount,
		})
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, fmt.Sprintf("%s failed to process", request.TransactionID)))
		}
	}

	if len(errs.Errors) > 0 {
		return "failed to entitle some tokens", errs.ErrorOrNil()
	}

	return "successfully processed transactions", nil
}
