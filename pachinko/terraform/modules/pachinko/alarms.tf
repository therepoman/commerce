
resource "aws_cloudwatch_metric_alarm" "account_provisioned_write_capacity_warning" {
  alarm_name          = "account_provisioned_write_capacity-alarm_warning"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "AccountProvisionedWriteCapacityUtilization"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
  }

  period                    = "300"
  statistic                 = "Average"
  threshold                 = "70"
  alarm_description         = "Alarms when the percentage of provisioned write capacity units utilized by an account reaches a certain percentage"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "account_provisioned_write_capacity_severe" {
  alarm_name          = "account_provisioned_write_capacity-alarm_severe"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "AccountProvisionedWriteCapacityUtilization"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
  }

  period                    = "300"
  statistic                 = "Average"
  threshold                 = "90"
  alarm_description         = "Alarms when the percentage of provisioned write capacity units utilized by an account reaches a certain percentage"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "account_provisioned_read_capacity_warning" {
  alarm_name          = "account_provisioned_read_capacity-alarm_warning"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "AccountProvisionedReadCapacityUtilization"
  namespace           = "pachinko"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pachinko"
  }

  period                    = "300"
  statistic                 = "Average"
  threshold                 = "70"
  alarm_description         = "Alarms when the percentage of provisioned read capacity units utilized by an account reaches a certain percentage"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "account_provisioned_read_capacity_severe" {
  alarm_name          = "account_provisioned_read_capacity-alarm_severe"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "AccountProvisionedReadCapacityUtilization"
  namespace           = "pachinko"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pachinko"
  }

  period                    = "300"
  statistic                 = "Average"
  threshold                 = "90"
  alarm_description         = "Alarms when the percentage of provisioned read capacity units utilized by an account reaches a certain percentage"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}