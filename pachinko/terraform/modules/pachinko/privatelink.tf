// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = [var.sg_id]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.private_subnets)
  dns             = "ldap.twitch.a2z.com"
}
