locals {
  bb_dynamo_min_read_capacity  = var.env == "staging" ? 100 : var.dynamo_min_read_capacity
  bb_dynamo_min_write_capacity = var.env == "staging" ? 100 : var.dynamo_min_write_capacity

  bits_min_task_count                     = var.env == "staging" ? var.min_host_count : 20
  bits_ecs_ec2_min_instances              = var.env == "staging" ? var.ecs_ec2_backed_min_instances : 12
  bits_dynamo_tokens_write_capacity       = var.env == "staging" ? var.dynamo_min_write_capacity : 500
  bits_dynamo_transactions_write_capacity = var.env == "staging" ? var.dynamo_min_write_capacity : 500
  bits_transactions_min_read_capacity     = 2000
  bits_tokens_min_read_capacity           = var.env == "staging" ? 200 : 4000
  bits_dynamo_tokens_max_read_capacity    = var.env == "staging" ? 200 : 40000
  bits_dynamo_locks_min_read_capactiy     = var.env == "staging" ? var.dynamo_min_read_capacity : 1600
  bits_dynamo_locks_min_write_capacity    = var.env == "staging" ? var.dynamo_min_write_capacity : 1600

  copo_min_task_count                  = var.env == "staging" ? var.min_host_count : 47
  copo_ecs_ec2_min_instances           = var.env == "staging" ? var.ecs_ec2_backed_min_instances : 24
  copo_dynamo_tokens_read_capacity     = var.env == "staging" ? 200 : 7000
  copo_dynamo_txs_read_capacity        = var.env == "staging" ? 200 : 10000
  copo_dynamo_write_capacity           = var.env == "staging" ? var.dynamo_min_write_capacity : 12000
  copo_dynamo_tokens_max_read_capacity = var.env == "staging" ? 10000 : 80000
  copo_dynamo_max_read_capacity        = var.env == "staging" ? 10000 : 80000
  copo_dynamo_max_write_capacity       = var.env == "staging" ? 10000 : 80000
  copo_dynamo_read_utilization         = 50
  copo_dynamo_write_utilization        = 50
  copo_dynamo_locks_min_read_capacity  = var.env == "staging" ? var.dynamo_min_read_capacity : 40000
  copo_dynamo_locks_min_write_capacity = var.env == "staging" ? var.dynamo_min_write_capacity : 40000
  copo_dynamo_locks_max_read_capacity  = var.env == "staging" ? 10000 : 40000
  copo_dynamo_locks_max_write_capacity = var.env == "staging" ? 10000 : 40000
  copo_account_id = var.env == "staging" ? "962912953676" : "471167244615"

  sub_tokens_min_task_count                        = var.env == "staging" ? var.min_host_count : 5
  sub_tokens_ecs_ec2_min_instances                 = var.env == "staging" ? var.ecs_ec2_backed_min_instances : 3
  sub_tokens_dynamo_read_capacity                  = var.env == "staging" ? var.dynamo_min_read_capacity : 200
  sub_tokens_dynamo_tokens_min_read_capacity       = 200
  sub_tokens_dynamo_transactions_min_read_capacity = 800
  sub_tokens_dynamo_write_capacity                 = var.env == "staging" ? var.dynamo_min_write_capacity : 50

  sticker_tokens_min_read_capacity       = var.env == "staging" ? var.dynamo_min_read_capacity : 800
  sticker_transactions_min_read_capacity = var.env == "staging" ? var.dynamo_min_read_capacity : 800
  stickers_ecs_ec2_min_instances         = var.env == "staging" ? var.ecs_ec2_backed_min_instances : 3

  free_subs_get_token_latency_threshold = "0.3"

  copo_num_shards         = var.env == "staging" ? 1 : 6
  sub_tokens_num_shards   = var.env == "staging" ? 1 : 4
  bits_num_shards         = var.env == "staging" ? 1 : 4
  stickers_num_shards     = var.env == "staging" ? 1 : 1
  wallet_num_shards       = var.env == "staging" ? 1 : 1
  free_subs_num_shards    = var.env == "staging" ? 1 : 1
  bounty_board_num_shards = var.env == "staging" ? 1 : 1

  // pachter, payday
  bits_whitelisted_arns = {
    staging = list("arn:aws:iam::721938063588:root", "arn:aws:iam::021561903526:root")
    prod    = list("arn:aws:iam::458492755823:root", "arn:aws:iam::021561903526:root")
  }

  wallet_whitelisted_arns = {
    staging = list("arn:aws:iam::295778065675:root")
    prod    = list("arn:aws:iam::405647336342:root")
  }

  free_subs_whitelisted_arns = {
    staging = list("arn:aws:iam::958836777662:root")
    prod    = list("arn:aws:iam::522398581884:root")
  }

  free_subs_sns_subscribers = {
    staging = list("958836777662")
    prod    = list("522398581884")
  }

  bounty_board_whitelisted_arns = {
    staging = list("arn:aws:iam::726281605084:root")
    prod    = list("arn:aws:iam::737882373154:root")
  }

  bounty_board_sns_subscribers = {
    staging = list("726281605084")
    prod    = list("737882373154")
  }

  twitch_security_log_funnel_role_arn = var.env == "prod" ? aws_cloudformation_stack.twitch_security_log_funnel[0].outputs["CWLRoleARN"] : ""
  twitch_security_log_funnel_arn      = var.env == "prod" ? aws_cloudformation_stack.twitch_security_log_funnel[0].outputs["LogFunnelARN"] : ""
}

module "stickers_tenant" {
  source                          = "./tenant"
  tenant_name                     = "sticker"
  tenant_metric_name              = "STICKER"
  suffix                          = var.env
  autoscaling_role                = var.autoscaling_role
  min_tokens_read_capacity        = local.sticker_tokens_min_read_capacity
  min_tokens_write_capacity       = var.dynamo_min_write_capacity
  min_transactions_read_capacity  = local.sticker_transactions_min_read_capacity
  min_transactions_write_capacity = var.dynamo_min_write_capacity
  min_locks_read_capacity         = var.dynamo_min_read_capacity
  min_locks_write_capacity        = var.dynamo_min_write_capacity
  min_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  min_tokens_gsi_write_capacity   = var.dynamo_min_write_capacity
  max_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  max_tokens_gsi_write_capacity   = var.dynamo_min_write_capacity
  backup_region                   = var.backup_region
  profile                         = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = var.min_host_count
  ecs_ec2_backed_min_instances = local.stickers_ecs_ec2_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.stickers_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  create_sns_subscriber_sqs       = var.env == "staging" ? true : false
  ecs_upgrade_sns_topic           = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days = var.alb_access_logs_expiration_days
  enable_audit_logging            = true
  enable_s2s_auth_logging         = true
  audit_logs_retention_in_days    = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days = var.s2s_auth_logs_retention_in_days
}

module "community_points_tenant" {
  source             = "./tenant"
  tenant_name        = "cp"
  tenant_metric_name = "COMMUNITY_POINTS"
  suffix             = var.env
  autoscaling_role   = var.autoscaling_role

  min_tokens_read_capacity      = local.copo_dynamo_tokens_read_capacity
  min_tokens_write_capacity     = local.copo_dynamo_write_capacity
  max_tokens_read_capacity      = local.copo_dynamo_tokens_max_read_capacity
  max_tokens_write_capacity     = local.copo_dynamo_max_write_capacity
  min_tokens_gsi_read_capacity  = local.copo_dynamo_tokens_read_capacity
  min_tokens_gsi_write_capacity = local.copo_dynamo_write_capacity
  max_tokens_gsi_read_capacity  = local.copo_dynamo_tokens_max_read_capacity
  max_tokens_gsi_write_capacity = local.copo_dynamo_max_write_capacity

  tokens_allow_full_control_for_account_id = local.copo_account_id

  enable_tokens_replica_table_gsi = true

  min_transactions_read_capacity  = local.copo_dynamo_txs_read_capacity
  min_transactions_write_capacity = local.copo_dynamo_write_capacity
  max_transactions_read_capacity  = local.copo_dynamo_max_read_capacity
  max_transactions_write_capacity = local.copo_dynamo_max_write_capacity
  enable_transactions_ttl         = true

  min_locks_read_capacity  = local.copo_dynamo_locks_min_read_capacity
  min_locks_write_capacity = local.copo_dynamo_locks_min_write_capacity
  max_locks_read_capacity  = local.copo_dynamo_locks_max_read_capacity
  max_locks_write_capacity = local.copo_dynamo_locks_max_write_capacity

  read_utilization  = local.copo_dynamo_read_utilization
  write_utilization = local.copo_dynamo_write_utilization

  backup_region = var.backup_region
  profile       = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = local.copo_min_task_count
  ecs_ec2_backed_min_instances = local.copo_ecs_ec2_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.copo_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  error_alarm_threshold              = "4000"
  add_tokens_latency_alarm_threshold = "0.5"

  start_transaction_latency_alarm_threshold  = "0.5"
  commit_transaction_latency_alarm_threshold = "0.5"
  ecs_upgrade_sns_topic                      = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days            = var.alb_access_logs_expiration_days
  audit_logs_retention_in_days               = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days            = var.s2s_auth_logs_retention_in_days
}

module "bits_tenant" {
  source             = "./tenant"
  tenant_name        = "bits"
  tenant_metric_name = "BITS"
  suffix             = var.env
  autoscaling_role   = var.autoscaling_role

  min_tokens_read_capacity        = local.bits_tokens_min_read_capacity
  min_tokens_write_capacity       = local.bits_dynamo_tokens_write_capacity
  max_tokens_read_capacity        = local.bits_dynamo_tokens_max_read_capacity
  min_transactions_read_capacity  = local.bits_transactions_min_read_capacity
  min_transactions_write_capacity = local.bits_dynamo_transactions_write_capacity
  min_locks_read_capacity         = local.bits_dynamo_locks_min_read_capactiy
  min_locks_write_capacity        = local.bits_dynamo_locks_min_write_capacity
  min_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  min_tokens_gsi_write_capacity   = local.bits_dynamo_tokens_write_capacity
  max_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  max_tokens_gsi_write_capacity   = local.bits_dynamo_tokens_write_capacity

  backup_region = var.backup_region
  profile       = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = local.bits_min_task_count
  ecs_ec2_backed_min_instances = local.bits_ecs_ec2_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  updates_sns_subscribers = var.bits_updates_sns_subscribers

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.bits_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  error_alarm_threshold                      = "100"
  start_transaction_latency_alarm_threshold  = "0.4"
  commit_transaction_latency_alarm_threshold = "0.4"

  whitelisted_arns                    = local.bits_whitelisted_arns[var.env]
  ecs_upgrade_sns_topic               = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days     = var.alb_access_logs_expiration_days
  enable_audit_logging                = true
  enable_s2s_auth_logging             = true
  audit_logs_retention_in_days        = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days     = var.s2s_auth_logs_retention_in_days
  twitch_security_log_funnel_role_arn = local.twitch_security_log_funnel_role_arn
  twitch_security_log_funnel_arn      = local.twitch_security_log_funnel_arn
}

module "sub_tokens_tenant" {
  source             = "./tenant"
  tenant_name        = "st"
  tenant_metric_name = "SUB_TOKENS"
  suffix             = var.env
  autoscaling_role   = var.autoscaling_role

  min_tokens_read_capacity        = local.sub_tokens_dynamo_tokens_min_read_capacity
  min_tokens_write_capacity       = local.sub_tokens_dynamo_write_capacity
  min_transactions_read_capacity  = local.sub_tokens_dynamo_transactions_min_read_capacity
  min_transactions_write_capacity = local.sub_tokens_dynamo_write_capacity
  min_locks_read_capacity         = var.dynamo_min_read_capacity
  min_locks_write_capacity        = var.dynamo_min_write_capacity
  min_tokens_gsi_read_capacity    = local.sub_tokens_dynamo_read_capacity
  min_tokens_gsi_write_capacity   = local.sub_tokens_dynamo_write_capacity
  max_tokens_gsi_read_capacity    = local.sub_tokens_dynamo_read_capacity
  max_tokens_gsi_write_capacity   = local.sub_tokens_dynamo_write_capacity

  backup_region = var.backup_region
  profile       = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = local.sub_tokens_min_task_count
  ecs_ec2_backed_min_instances = local.sub_tokens_ecs_ec2_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.sub_tokens_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  updates_sns_subscribers = var.sub_tokens_updates_sns_subscribers

  commit_transaction_latency_alarm_threshold = "0.3"
  ecs_upgrade_sns_topic                      = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days            = var.alb_access_logs_expiration_days
  enable_audit_logging                       = true
  enable_s2s_auth_logging                    = true
  audit_logs_retention_in_days               = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days            = var.s2s_auth_logs_retention_in_days
  twitch_security_log_funnel_role_arn        = local.twitch_security_log_funnel_role_arn
  twitch_security_log_funnel_arn             = local.twitch_security_log_funnel_arn
}

module "wallet_tenant" {
  source                          = "./tenant"
  tenant_name                     = "wallet"
  tenant_metric_name              = "WALLET"
  suffix                          = var.env
  autoscaling_role                = var.autoscaling_role
  min_tokens_read_capacity        = var.dynamo_min_read_capacity
  min_tokens_write_capacity       = var.dynamo_min_write_capacity
  min_transactions_read_capacity  = var.dynamo_min_read_capacity
  min_transactions_write_capacity = var.dynamo_min_write_capacity
  min_locks_read_capacity         = var.dynamo_min_read_capacity
  min_locks_write_capacity        = var.dynamo_min_write_capacity
  min_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  min_tokens_gsi_write_capacity   = var.dynamo_min_write_capacity
  max_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  max_tokens_gsi_write_capacity   = var.dynamo_min_write_capacity
  backup_region                   = var.backup_region
  profile                         = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = var.min_host_count
  ecs_ec2_backed_min_instances = var.ecs_ec2_backed_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.wallet_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  whitelisted_arns                    = local.wallet_whitelisted_arns[var.env]
  ecs_upgrade_sns_topic               = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days     = var.alb_access_logs_expiration_days
  enable_audit_logging                = true
  enable_s2s_auth_logging             = true
  audit_logs_retention_in_days        = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days     = var.s2s_auth_logs_retention_in_days
  twitch_security_log_funnel_role_arn = local.twitch_security_log_funnel_role_arn
  twitch_security_log_funnel_arn      = local.twitch_security_log_funnel_arn
}

module "free_subs_tenant" {
  source                          = "./tenant"
  tenant_name                     = "freesubs"
  tenant_metric_name              = "FREE_SUBS"
  suffix                          = var.env
  autoscaling_role                = var.autoscaling_role
  min_tokens_read_capacity        = var.dynamo_min_read_capacity
  min_tokens_write_capacity       = var.dynamo_min_write_capacity
  min_transactions_read_capacity  = var.dynamo_min_read_capacity
  min_transactions_write_capacity = var.dynamo_min_write_capacity
  min_locks_read_capacity         = var.dynamo_min_read_capacity
  min_locks_write_capacity        = var.dynamo_min_write_capacity
  min_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  min_tokens_gsi_write_capacity   = var.dynamo_min_write_capacity
  max_tokens_gsi_read_capacity    = var.dynamo_min_read_capacity
  max_tokens_gsi_write_capacity   = var.dynamo_min_write_capacity
  backup_region                   = var.backup_region
  profile                         = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = var.min_host_count
  ecs_ec2_backed_min_instances = var.ecs_ec2_backed_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.free_subs_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  whitelisted_arns                  = local.free_subs_whitelisted_arns[var.env]
  updates_sns_subscribers           = local.free_subs_sns_subscribers[var.env]
  ecs_upgrade_sns_topic             = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days   = var.alb_access_logs_expiration_days
  audit_logs_retention_in_days      = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days   = var.s2s_auth_logs_retention_in_days
  get_token_latency_alarm_threshold = local.free_subs_get_token_latency_threshold
}

module "bounty_board_tenant" {
  source                          = "./tenant"
  tenant_name                     = "bb"
  tenant_metric_name              = "BOUNTY_BOARD"
  suffix                          = var.env
  autoscaling_role                = var.autoscaling_role
  min_tokens_read_capacity        = local.bb_dynamo_min_read_capacity
  min_tokens_write_capacity       = local.bb_dynamo_min_write_capacity
  min_transactions_read_capacity  = var.dynamo_min_read_capacity
  min_transactions_write_capacity = var.dynamo_min_write_capacity
  min_locks_read_capacity         = var.dynamo_min_read_capacity
  min_locks_write_capacity        = var.dynamo_min_write_capacity
  min_tokens_gsi_read_capacity    = local.bb_dynamo_min_read_capacity
  min_tokens_gsi_write_capacity   = local.bb_dynamo_min_write_capacity
  max_tokens_gsi_read_capacity    = local.bb_dynamo_min_read_capacity
  max_tokens_gsi_write_capacity   = local.bb_dynamo_min_write_capacity
  backup_region                   = var.backup_region
  profile                         = var.profile

  account_id                   = var.account_id
  security_group_id            = var.sg_id
  redis_security_group_id      = aws_security_group.pachinko_redis_security_group.id
  service                      = var.service_short_name
  subnets                      = var.private_subnets
  vpc_id                       = var.vpc_id
  cpu                          = var.cpu
  memory                       = var.memory
  env                          = var.env
  min_task_count               = var.min_host_count
  ecs_ec2_backed_min_instances = var.ecs_ec2_backed_min_instances
  sandstorm_arn                = var.sandstorm_arn
  ecr_repo                     = data.aws_ecr_repository.main.repository_url

  elasticache_replicas_per_node_group = var.elasticache_replicas_per_node_group
  elasticache_num_node_groups         = local.bounty_board_num_shards
  elasticache_instance_type           = var.elasticache_instance_type
  elasticache_auto_failover           = var.elasticache_auto_failover
  cidr_blocks                         = var.private_cidr_blocks

  whitelisted_arns                    = local.bounty_board_whitelisted_arns[var.env]
  updates_sns_subscribers             = local.bounty_board_sns_subscribers[var.env]
  ecs_upgrade_sns_topic               = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  alb_access_logs_expiration_days     = var.alb_access_logs_expiration_days
  enable_audit_logging                = true
  enable_s2s_auth_logging             = true
  audit_logs_retention_in_days        = var.audit_logs_retention_in_days
  s2s_auth_logs_retention_in_days     = var.s2s_auth_logs_retention_in_days
  twitch_security_log_funnel_role_arn = local.twitch_security_log_funnel_role_arn
  twitch_security_log_funnel_arn      = local.twitch_security_log_funnel_arn
}
