resource "aws_lb" nlb_proxy {
  name               = "${var.service_short_name}-proxy"
  internal           = true
  load_balancer_type = "network"
  subnets            = split(",", var.private_subnets)

  enable_deletion_protection = true
}

resource "aws_s3_bucket" "nlb_proxy" {
  bucket = "${var.service_short_name}-${var.env_full}-proxy-ips"
}
