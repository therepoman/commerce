resource "aws_cloudwatch_event_rule" "start_delete_by_group_id" {
  name        = "cron_${module.start_delete_by_group_id_lambda.lambda_name}"
  description = "Starts the ${module.start_delete_by_group_id_lambda.lambda_name} lambda to kick off batch delete step fn"

  // This is in GMT time, so we need to set it 7 hours ahead
  schedule_expression = "cron(0 19 * * ? *)"
}

resource "aws_cloudwatch_event_target" "start_delete_by_group_id_lambda" {
  rule = aws_cloudwatch_event_rule.start_delete_by_group_id.name
  arn  = module.start_delete_by_group_id_lambda.lambda_arn
}

resource "aws_cloudwatch_event_rule" "start_delete_by_owner_id" {
  name        = "cron_${module.start_delete_by_owner_id_lambda.lambda_name}"
  description = "Starts the ${module.start_delete_by_owner_id_lambda.lambda_name} lambda to kick off batch delete step fn"

  // This is in GMT time, so we need to set it 7 hours ahead
  schedule_expression = "cron(0 19 * * ? *)"
}

resource "aws_cloudwatch_event_target" "start_delete_by_owner_id_lambda" {
  rule = aws_cloudwatch_event_rule.start_delete_by_owner_id.name
  arn  = module.start_delete_by_owner_id_lambda.lambda_arn
}

locals {
  audit_logs_name = "pachinko-${var.tenant_name}-${var.env}-audit-logs"
}

resource "aws_cloudwatch_log_group" "audit_logs" {
  count             = var.enable_audit_logging ? 1 : 0
  name              = local.audit_logs_name
  retention_in_days = var.audit_logs_retention_in_days
  kms_key_id        = aws_kms_key.audit_logs[0].arn
}

locals {
  should_subscribe_to_audit_logs = (var.env == "prod") && (var.tenant_name != "sticker") && var.enable_audit_logging
}

resource "aws_cloudwatch_log_subscription_filter" "audit_log_funnel_subscription" {
  count           = local.should_subscribe_to_audit_logs ? 1 : 0
  name            = "${local.audit_logs_name}_subscription-filter"
  log_group_name  = local.audit_logs_name
  filter_pattern  = ""
  destination_arn = var.twitch_security_log_funnel_arn
  role_arn        = var.twitch_security_log_funnel_role_arn
}

locals {
  s2s_auth_logs_name = "pachinko-${var.tenant_name}-${var.env}-s2s-auth-logs"
}

resource "aws_cloudwatch_log_group" "s2s_auth_logs" {
  count             = var.enable_s2s_auth_logging ? 1 : 0
  name              = local.s2s_auth_logs_name
  retention_in_days = var.s2s_auth_logs_retention_in_days
  kms_key_id        = aws_kms_key.s2s_auth_logs[0].arn
}

locals {
  should_subscribe_to_s2s_auth_logs = (var.env == "prod") && (var.tenant_name != "sticker") && var.enable_s2s_auth_logging
}

resource "aws_cloudwatch_log_subscription_filter" "s2s_auth_log_funnel_subscription" {
  count           = local.should_subscribe_to_s2s_auth_logs ? 1 : 0
  name            = "${local.s2s_auth_logs_name}_subscription-filter"
  log_group_name  = local.s2s_auth_logs_name
  filter_pattern  = ""
  destination_arn = var.twitch_security_log_funnel_arn
  role_arn        = var.twitch_security_log_funnel_role_arn
}