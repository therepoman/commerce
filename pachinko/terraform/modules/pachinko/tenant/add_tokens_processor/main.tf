resource "aws_lambda_function" "lambda" {
  function_name = "${var.tenant_name}-${var.env}-add_tokens_processor"
  role          = module.lambda-role.arn
  description   = "Async add tokens processor for ${var.tenant_name} in env ${var.env}"
  handler       = "add_tokens_processor"
  runtime       = "go1.x"
  s3_bucket     = var.s3_bucket
  s3_key        = "add_tokens_processor/latest/add_tokens_processor.zip"
  timeout       = "60"

  environment {
    variables = {
      ENVIRONMENT = var.env
      TENANT      = var.tenant_name
    }
  }

  vpc_config {
    security_group_ids = split(",", var.security_groups)
    subnet_ids         = split(",", var.subnets)
  }
}

module "lambda-role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"
  name   = "${var.tenant_name}-${var.env}-add-tokens-role"
}

resource aws_iam_role_policy_attachment "lambda_dynamodb_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = module.lambda-role.name
}

resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role       = module.lambda-role.name
}

resource "aws_lambda_event_source_mapping" "queue_stream" {
  event_source_arn  = var.queue_arn
  function_name     = aws_lambda_function.lambda.arn
}
