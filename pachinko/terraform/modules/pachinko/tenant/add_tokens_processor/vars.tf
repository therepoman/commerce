variable "tenant_name" {
  description = "The name of the tenant."
}

variable "env" {
  description = "Whether it's prod, dev, etc."
}

variable "security_groups" {
  description = "Twitch subnets Sec group ID."
}

variable "subnets" {
  description = "Private subnets in VPC."
}

variable "s3_bucket" {
  description = "The s3 bucket for the lambda source."
}

variable "queue_arn" {
  description = "the queue ARN that will be used to listen for events"
}
