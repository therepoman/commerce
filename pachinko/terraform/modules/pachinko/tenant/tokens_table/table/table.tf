resource "aws_dynamodb_table" "tokens_table" {
  count = var.enable_gsi ? 0 : 1

  name           = "${var.token_name}-tokens-gbl-${var.suffix}"
  hash_key       = "owner_id"
  range_key      = "group_id"
  read_capacity  = var.min_read_capacity
  write_capacity = var.min_write_capacity

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "owner_id"
    type = "S"
  }

  attribute {
    name = "group_id"
    type = "S"
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

resource "aws_dynamodb_table" "tokens_table_with_gsi" {
  count = var.enable_gsi ? 1 : 0

  name           = "${var.token_name}-tokens-gbl-${var.suffix}"
  hash_key       = "owner_id"
  range_key      = "group_id"
  read_capacity  = var.min_read_capacity
  write_capacity = var.min_write_capacity

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "owner_id"
    type = "S"
  }

  attribute {
    name = "group_id"
    type = "S"
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }

  global_secondary_index {
    name            = "${var.token_name}-token-by-group-id-gsi-${var.suffix}"
    hash_key        = "group_id"
    range_key       = "owner_id"
    read_capacity   = var.min_gsi_read_capacity
    write_capacity  = var.min_gsi_write_capacity
    projection_type = "KEYS_ONLY"
  }
}

module "transactions_table_alarms" {
  source                 = "../../alarms/dynamo_alarms"
  table_name             = "${var.token_name}-tokens-gbl-${var.suffix}"
  threshold              = "1"
  enable_capacity_alarms = var.enable_capacity_alarms
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
}

module "tokens_table_autoscaling" {
  source             = "../../../../dynamo_table_autoscaling"
  table_name         = "${var.token_name}-tokens-gbl-${var.suffix}"
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
  read_utilization   = var.read_utilization
  write_utilization  = var.write_utilization
  autoscaling_role   = var.autoscaling_role
  region             = var.region
}

module "token_by_group_id_gsi_autoscaling" {
  source             = "../../../../dynamo_table_autoscaling"
  enable_resources   = var.enable_gsi
  table_name         = "${var.token_name}-tokens-gbl-${var.suffix}/index/${var.token_name}-token-by-group-id-gsi-${var.suffix}"
  table_kind         = "index"
  min_read_capacity  = var.min_gsi_read_capacity
  min_write_capacity = var.min_gsi_write_capacity
  max_read_capacity  = var.max_gsi_read_capacity
  max_write_capacity = var.max_gsi_write_capacity
  read_utilization   = var.read_utilization
  write_utilization  = var.write_utilization
  autoscaling_role   = var.autoscaling_role
  region             = var.region
}

resource "aws_lambda_event_source_mapping" "tokens_stream" {
  count = var.ledgerman_lambda_arn == "" ? 0 : 1
  // the following join is an indirect conditional, it's joining the arns from the "count"ed table resources, only one of the tables will exist.
  // see https://github.com/hashicorp/terraform/issues/15605 and https://github.com/hashicorp/hil/issues/50
  event_source_arn  = join("", aws_dynamodb_table.tokens_table.*.stream_arn, aws_dynamodb_table.tokens_table_with_gsi.*.stream_arn)
  function_name     = var.ledgerman_lambda_arn
  starting_position = "LATEST"

  lifecycle {
    ignore_changes = [starting_position]
  }
}
