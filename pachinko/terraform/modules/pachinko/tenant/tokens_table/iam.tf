resource "aws_iam_role" "tokens_table_role" {
  count = var.allow_full_control_for_account_id == "" ? 0 : 1

  name = "${var.token_name}-tokens-gbl-${var.suffix}-full-table-access"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "AWS": "arn:aws:iam::${var.allow_full_control_for_account_id}:root"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "tokens_table_policy" {
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:*"
    ]

    resources = ["arn:aws:dynamodb:*:*:table/${var.token_name}-tokens-gbl-${var.suffix}"]
  }
}

resource "aws_iam_role_policy" "tokens_table_role_policy" {
  count = var.allow_full_control_for_account_id == "" ? 0 : 1

  role   = aws_iam_role.tokens_table_role[count.index].name
  policy = data.aws_iam_policy_document.tokens_table_policy.json
}