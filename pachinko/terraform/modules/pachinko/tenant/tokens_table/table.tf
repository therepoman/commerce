module "primary_table" {
  source                 = "./table"
  token_name             = var.token_name
  suffix                 = var.suffix
  min_read_capacity      = var.min_read_capacity
  min_write_capacity     = var.min_write_capacity
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
  min_gsi_read_capacity  = var.min_gsi_read_capacity
  min_gsi_write_capacity = var.min_gsi_write_capacity
  max_gsi_read_capacity  = var.max_gsi_read_capacity
  max_gsi_write_capacity = var.max_gsi_write_capacity
  read_utilization       = var.read_utilization
  write_utilization      = var.write_utilization
  autoscaling_role       = var.autoscaling_role
  ledgerman_lambda_arn   = var.ledgerman_lambda_arn
  region                 = ""
  enable_capacity_alarms = var.enable_capacity_alarms
}

provider "aws" {
  alias   = "backup"
  profile = var.profile
  region  = var.backup_region
}

module "replica_table" {
  source                 = "./table"
  token_name             = var.token_name
  suffix                 = var.suffix
  min_read_capacity      = var.min_read_capacity
  min_write_capacity     = var.min_write_capacity
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
  min_gsi_read_capacity  = var.min_gsi_read_capacity
  min_gsi_write_capacity = var.min_gsi_write_capacity
  max_gsi_read_capacity  = var.max_gsi_read_capacity
  max_gsi_write_capacity = var.max_gsi_write_capacity
  enable_gsi             = var.enable_replica_table_gsi
  read_utilization       = var.read_utilization
  write_utilization      = var.write_utilization
  autoscaling_role       = var.autoscaling_role
  region                 = var.backup_region

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "global_table" {
  depends_on = [module.primary_table, module.replica_table]

  name = "${var.token_name}-tokens-gbl-${var.suffix}"

  replica {
    region_name = "us-west-2"
  }

  replica {
    region_name = var.backup_region
  }
}
