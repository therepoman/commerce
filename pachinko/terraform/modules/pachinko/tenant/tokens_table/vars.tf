variable "token_name" {
  description = "Type of tokens this table will track"
}

variable "suffix" {
  description = "Suffix at the end of dynamo table names"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}

variable "max_read_capacity" {
  description = "Maximum dynamodb read capacity"
}

variable "max_write_capacity" {
  description = "Maximum dynamodb write capacity"
}

variable "min_gsi_read_capacity" {
  description = "Minimum dynamodb read capacity on GSI"
}

variable "min_gsi_write_capacity" {
  description = "Minimum dynamodb write capacity on GSI"
}

variable "max_gsi_read_capacity" {
  description = "Maximum dynamodb read capacity for GSI autoscaling"
}

variable "max_gsi_write_capacity" {
  description = "Maximum dynamodb write capacity for GSI autoscaling"
}

variable "enable_replica_table_gsi" {
  description = "This flag indicates if a GSI should be added to the replica table."
  default = false
}

variable "read_utilization" {
  description = "DynamoDB read capacity utilization"
}

variable "write_utilization" {
  description = "DynamoDB write capacity utilization"
}

variable "profile" {}

variable "backup_region" {}

variable "ledgerman_lambda_arn" {
  description = "ARN for the ledgerman lambda function"
}

variable "enable_capacity_alarms" {
  default = false
}

variable "allow_full_control_for_account_id" {
  default = ""
}
