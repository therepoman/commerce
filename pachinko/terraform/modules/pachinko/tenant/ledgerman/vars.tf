variable "suffix" {}

variable "tenant_name" {}

variable "security_groups" {}

variable "subnets" {}

variable "s3_bucket" {
  description = "The s3 bucket for the lambda source."
}
