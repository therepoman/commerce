resource "aws_sqs_queue" "sqs_queue" {
  message_retention_seconds = 1209600
  name                      = "${var.tenant_name}-${var.suffix}-deadletter"
  receive_wait_time_seconds = 20
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = aws_sqs_queue.sqs_queue.id
  policy    = data.aws_iam_policy_document.queue_policy_doc.json
}

data "aws_iam_policy_document" "queue_policy_doc" {
  statement {
    sid = "${var.tenant_name}-${var.suffix}-Sid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [aws_sqs_queue.sqs_queue.arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [aws_lambda_function.ledgerman.arn]
    }
  }
}

resource "aws_lambda_function" "ledgerman" {
  function_name = "${var.tenant_name}-${var.suffix}-ledgerman"
  role          = aws_iam_role.ledgerman.arn
  description   = "Ledgerman for ${var.tenant_name} tenant in ${var.suffix}"
  handler       = "ledgerman"
  runtime       = "go1.x"
  timeout       = 300

  s3_bucket = var.s3_bucket
  s3_key    = "ledgerman/latest/ledgerman.zip"

  vpc_config {
    security_group_ids = split(",", var.security_groups)
    subnet_ids         = split(",", var.subnets)
  }

  dead_letter_config {
    target_arn = aws_sqs_queue.sqs_queue.arn
  }

  environment {
    variables = {
      ENVIRONMENT = var.suffix
      TENANT      = var.tenant_name
    }
  }

  lifecycle {
    ignore_changes = [s3_key, s3_bucket, publish, last_modified]
  }
}

output "lambda_arn" {
  value = aws_lambda_function.ledgerman.arn
}
