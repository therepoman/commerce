resource aws_cloudwatch_log_group "logs" {
  name = "/aws/lambda/${aws_lambda_function.ledgerman.function_name}"
}

resource aws_cloudwatch_log_metric_filter "logscan_errors" {
  log_group_name = aws_cloudwatch_log_group.logs.name

  metric_transformation {
    name          = "ledgerman-${var.tenant_name}-logscan-errors-${var.suffix}"
    namespace     = "pachinko-logs"
    value         = "1"
    default_value = "0"
  }

  name    = "ledgerman-${var.tenant_name}-logscan-errors-${var.suffix}"
  pattern = "level=error"
}

resource aws_cloudwatch_log_metric_filter "logscan_panics" {
  log_group_name = aws_cloudwatch_log_group.logs.name

  metric_transformation {
    name          = "ledgerman-${var.tenant_name}-logscan-panics-${var.suffix}"
    namespace     = "pachinko-logs"
    value         = "1"
    default_value = "0"
  }

  name    = "ledgerman-${var.tenant_name}-logscan-panics-${var.suffix}"
  pattern = "panic"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_alarm" {
  alarm_name                = "ledgerman-${var.tenant_name}-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "ledgerman-${var.tenant_name}-panics"
  namespace                 = "pachinko-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric is used for pachinko ledgerman panics on the ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_error" {
  alarm_name                = "ledgerman-${var.tenant_name}-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "ledgerman-${var.tenant_name}-errors"
  namespace                 = "pachinko-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  alarm_description         = "This metric is used for pachinko ledgerman logscan errors on the ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "iterator_age_growing" {
  alarm_name          = "ledgerman-${var.tenant_name}-iterator-age-growing"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "IteratorAge"
  namespace           = "AWS/Lambda"

  dimensions = {
    FunctionName = aws_lambda_function.ledgerman.function_name
    Resource     = aws_lambda_function.ledgerman.function_name
  }
  period             = "60"
  statistic          = "Average"
  threshold          = "2000"
  alarm_description  = "This metric monitors the ledgerman stream iterator for ${var.tenant_name}. It will alarm if the iterator is backing up due to messages not processing."
  treat_missing_data = "notBreaching"
}
