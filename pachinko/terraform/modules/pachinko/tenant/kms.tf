// for audit log group
resource "aws_kms_key" "audit_logs" {
  count               = var.enable_audit_logging ? 1 : 0
  description         = "pachinko-${var.tenant_name}-${var.env} cloudwatch audit logs encryption"
  enable_key_rotation = true
  policy              = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.account_id}:root",
          "arn:aws:iam::${var.account_id}:role/Admin"
        ]
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Enable CloudWatch Audit Logs Encryption",
      "Effect": "Allow",
      "Principal": {
        "Service": "logs.${var.region}.amazonaws.com"
      },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*",
      "Condition": {
          "ArnEquals": {
              "kms:EncryptionContext:aws:logs:arn": "arn:aws:logs:${var.region}:${var.account_id}:log-group:${local.audit_logs_name}"
          }
      }
    }
  ]
}
EOF
}

resource "aws_kms_alias" "audit_logs" {
  count         = var.enable_audit_logging ? 1 : 0
  name          = "alias/pachinko-${var.tenant_name}-${var.env}/audit-logs"
  target_key_id = aws_kms_key.audit_logs[0].key_id
}

// for s2s auth log group
resource "aws_kms_key" "s2s_auth_logs" {
  count               = var.enable_s2s_auth_logging ? 1 : 0
  description         = "pachinko-${var.tenant_name}-${var.env} cloudwatch s2s auth logs encryption"
  enable_key_rotation = true
  policy              = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.account_id}:root",
          "arn:aws:iam::${var.account_id}:role/Admin"
        ]
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Enable CloudWatch S2S Auth Logs Encryption",
      "Effect": "Allow",
      "Principal": {
        "Service": "logs.${var.region}.amazonaws.com"
      },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*",
      "Condition": {
          "ArnEquals": {
              "kms:EncryptionContext:aws:logs:arn": "arn:aws:logs:${var.region}:${var.account_id}:log-group:${local.s2s_auth_logs_name}"
          }
      }
    }
  ]
}
EOF
}

resource "aws_kms_alias" "s2s_auth_logs" {
  count         = var.enable_s2s_auth_logging ? 1 : 0
  name          = "alias/pachinko-${var.tenant_name}-${var.env}/s2s-auth-logs"
  target_key_id = aws_kms_key.s2s_auth_logs[0].key_id
}