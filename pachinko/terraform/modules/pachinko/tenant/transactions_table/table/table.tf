resource "aws_dynamodb_table" "transactions_table" {
  name           = "${var.token_name}-tx-gbl-${var.suffix}"
  hash_key       = "transaction_id"
  read_capacity  = var.min_read_capacity
  write_capacity = var.min_write_capacity

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "transaction_id"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  ttl {
    attribute_name = var.enable_ttl ? "ttl" : ""
    enabled        = var.enable_ttl
  }
}

module "transactions_table_alarms" {
  source                 = "../../alarms/dynamo_alarms"
  table_name             = "${var.token_name}-tx-gbl-${var.suffix}"
  threshold              = "1"
  enable_capacity_alarms = var.enable_capacity_alarms
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
}

module "transactions_table_autoscaling" {
  source             = "../../../../dynamo_table_autoscaling"
  table_name         = "${var.token_name}-tx-gbl-${var.suffix}"
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
  read_utilization   = var.read_utilization
  write_utilization  = var.write_utilization
  autoscaling_role   = var.autoscaling_role
  region             = var.region
}
