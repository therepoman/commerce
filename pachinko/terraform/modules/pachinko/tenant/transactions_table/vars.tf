variable "token_name" {
  description = "Type of tokens this table will track"
}

variable "suffix" {
  description = "Suffix at the end of dynamo table names"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}

variable "max_read_capacity" {
  description = "Maximum dynamodb read capacity"
}

variable "max_write_capacity" {
  description = "Maximum dynamodb write capacity"
}

variable "read_utilization" {
  description = "DynamoDB read capacity utilization"
}

variable "write_utilization" {
  description = "DynamoDB write capacity utilization"
}

variable "enable_ttl" {
  description = "DynamoDB ttl utilization"
}

variable "backup_region" {}

variable "profile" {}

variable "enable_capacity_alarms" {
  default = false
}
