variable "tenant_name" {
  description = "The name of the tenant"
}

variable "tenant_metric_name" {
  description = "The name of the tenant used as the substage for metric emission. This is an uppercase version of the tenant tag in the prod.yaml file."
}

variable "suffix" {
  description = "Suffix at the end of dynamo table names"
}

# Dynamo Params

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "min_tokens_read_capacity" {
  description = "Minimum dynamodb read capacity for tokens"
}

variable "min_tokens_write_capacity" {
  description = "Minimum dynamodb write capacity for tokens"
}

variable "max_tokens_read_capacity" {
  description = "Maximum dynamodb read capacity for tokens"
  default     = "10000"
}

variable "max_tokens_write_capacity" {
  description = "Maximum dynamodb write capacity for tokens"
  default     = "10000"
}

variable "enable_tokens_capacity_alarms" {
  default = true
}

variable "tokens_allow_full_control_for_account_id" {
  default = ""
}

variable "min_transactions_read_capacity" {
  description = "Minimum dynamodb read capacity for transactions"
}

variable "min_transactions_write_capacity" {
  description = "Minimum dynamodb write capacity for transactions"
}

variable "max_transactions_read_capacity" {
  description = "Maximum dynamodb read capacity for transactions"
  default     = "10000"
}

variable "max_transactions_write_capacity" {
  description = "Maximum dynamodb write capacity for transactions"
  default     = "10000"
}

variable "enable_transactions_ttl" {
  description = "Whether or not transactions table uses ttl"
  default     = false
}

variable "enable_transactions_capacity_alarms" {
  default = true
}

variable "min_tokens_gsi_read_capacity" {
  description = "Minimum dynamodb read capacity for GSIs on tokens"
}

variable "min_tokens_gsi_write_capacity" {
  description = "Minimum dynamodb write capacity for GSIs on tokens"
}

variable "max_tokens_gsi_read_capacity" {
  description = "Maximum dynamodb read capacity for GSIs on tokens"
}

variable "max_tokens_gsi_write_capacity" {
  description = "Maximum dynamodb write capacity for GSIs on tokens"
}

variable "enable_tokens_replica_table_gsi" {
  description = "This flag indicates if a GSI should be added to the tokens replica table."
  default     = false
}

variable "min_locks_read_capacity" {
  description = "Minimum dynamodb read capacity for locks"
}

variable "min_locks_write_capacity" {
  description = "Minimum dynamodb write capacity for locks"
}

variable "max_locks_read_capacity" {
  description = "Maximum dynamodb read capacity for locks"
  default     = "10000"
}

variable "max_locks_write_capacity" {
  description = "Maximum dynamodb write capacity for locks"
  default     = "10000"
}

variable "read_utilization" {
  description = "DynamoDB read capacity utilization"
  default     = "70"
}

variable "write_utilization" {
  description = "DynamoDB write capacity utilization"
  default     = "70"
}

variable "backup_region" {}

variable "profile" {}

# ECS params

variable "account_id" {
  description = "Account ID for ECR repo URL"
}

variable "service" {}

variable "vpc_id" {
  description = "VPC ID"
}

variable "subnets" {
  description = "Private subnets in VPC"
}

variable "cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = list(string)
}

variable "security_group_id" {
  description = "Twitch subnets Sec group ID"
}

variable "redis_security_group_id" {}

variable "env" {}

variable "cpu" {
  default     = 256
  description = "CPU credits for ECS task"
}

variable "memory" {
  default     = 512
  description = "Memory credits for ECS task"
}

variable "min_task_count" {
  default = "1"
}

variable "max_task_count" {
  default = "300"
}

variable "updates_sns_subscribers" {
  description = "list of aws account numbers that can subscribe to updates SNS topic"
  type        = list(string)
  default     = ["021561903526"]
}

# AWS ElastiCache
variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "r5.large"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "error_alarm_threshold" {
  default     = "15"
  description = "Number of errors in a 5 minute period that should alarm us for a tenant."
}

variable "get_token_latency_alarm_threshold" {
  default     = "0.1"
  description = "p99 latency threshold for AddTokens API in a 5 minute period that should alarm us for a tenant."
}

variable "add_tokens_latency_alarm_threshold" {
  default     = "0.2"
  description = "p99 latency threshold for AddTokens API in a 5 minute period that should alarm us for a tenant."
}

variable "async_add_tokens_latency_alarm_threshold" {
  default     = "0.2"
  description = "p99 latency threshold for AsyncAddTokens API in a 5 minute period that should alarm us for a tenant."
}

variable "commit_transaction_latency_alarm_threshold" {
  default     = "0.15"
  description = "p99 latency threshold of CommitTransaction API in a 5 minute period that should alarm us for a tenant."
}

variable "start_transaction_latency_alarm_threshold" {
  default     = "0.3"
  description = "p99 latency threshold of StartTransaction API in a 5 minute period that should alarm us for a tenant."
}

variable "ecs_ec2_backed_min_instances" {
  default = 5
}

variable "sandstorm_arn" {
  description = "sandstorm arn yo"
}

variable "ecr_repo" {}

# PrivateLink variables

variable "region" {
  default     = "us-west-2"
  description = "AWS region in which the PrivateLink infrastructure will be deployed"
}

variable "whitelisted_arns" {
  type        = list(string)
  default     = []
  description = "Set of AWS ARNs to allow access to tenant Endpoint Service"
}

# For Integration Test
variable "create_sns_subscriber_sqs" {
  default     = false
  description = "Set to true to create an SQS that subscribes to balance_updates_topic"
}

variable "ecs_upgrade_sns_topic" {}

variable "alb_access_logs_expiration_days" {}

variable "enable_audit_logging" {
  default     = false
  description = "Set to true to create an encrypted CloudWatch Log group for audit logs"
}

variable "enable_s2s_auth_logging" {
  default     = false
  description = "Set to true to create an encrypted CloudWatch Log group for s2s auth logs"
}

variable "audit_logs_retention_in_days" {}

variable "s2s_auth_logs_retention_in_days" {}

variable "twitch_security_log_funnel_role_arn" {
  default     = ""
  description = "The arn for the role that sends logs to Twitch Security"
}

variable "twitch_security_log_funnel_arn" {
  default     = ""
  description = "The arn for the Log Funnel that sends logs to Twitch Security"
}
