locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF
}

module "delete_by_owner_id_step_function" {
  source             = "./privacy_sfn"
  state_machine_name = "${var.tenant_name}-${var.env}-DeleteBalanceByOwnerId"
  tenant_name        = var.tenant_name
  env                = var.env

  step_function_definition = <<EOF
{
  "Comment": "A state machine that deletes Tokens and Transactions by OwnerId",
  "StartAt": "DeleteBalanceByOwnerId",
  "States": {
    "DeleteBalanceByOwnerId": {
        "Type": "Parallel",
        "End": true,
        "ResultPath": null,
        "Branches": [{
            "StartAt": "DeleteTokensByOwnerId",
            "States": {
                "DeleteTokensByOwnerId": {
                    "Type": "Task",
                    "Resource": "${module.delete_tokens_by_owner_id_lambda.lambda_arn}",
                    "End": true,
                    "Retry": ${local.default_retry},
                    "Catch": [{
                        "ErrorEquals": [
                            "States.ALL"
                        ],
                        "Next": "DeleteTokensFailure"
                    }]
                },
                "DeleteTokensFailure": {
                  "Comment": "Failed to delete token",
                  "Type": "Fail"
                }
            }
        }, {
            "StartAt": "ScanAndDeleteTransactionsByOwnerId",
            "States": {
                "ScanAndDeleteTransactionsByOwnerId": {
                    "Type": "Task",
                    "Resource": "${module.delete_transactions_by_owner_id_lambda.lambda_arn}",
                    "Next": "ContinueScanTransactionsChoice",
                    "Retry": ${local.default_retry},
                    "Catch": [{
                        "ErrorEquals": [
                            "States.ALL"
                        ],
                        "Next": "DeleteTransactionsFailure"
                    }]
                },
                "ContinueScanTransactionsChoice": {
                  "Type": "Choice",
                  "Choices": [{
                    "Variable": "$.is_scan_complete",
                    "BooleanEquals": true,
                    "Next": "DeleteTransactionsSuccess"
                  }, {
                    "Variable": "$.is_scan_complete",
                    "BooleanEquals": false,
                    "Next": "ScanAndDeleteTransactionsByOwnerId"
                  }]
              },
              "DeleteTransactionsFailure": {
                "Comment": "Failed to delete transactions",
                "Type": "Fail"
              },
              "DeleteTransactionsSuccess": {
                "Type": "Succeed"
              }
            }
        }]
      }
  }
}
EOF
}

module "delete_by_group_id_step_function" {
  source             = "./privacy_sfn"
  state_machine_name = "${var.tenant_name}-${var.env}-DeleteBalanceByGroupId"
  tenant_name        = var.tenant_name
  env                = var.env

  step_function_definition = <<EOF
{
  "Comment": "A state machine that deletes Tokens and Transactions by GroupId",
  "StartAt": "DeleteBalanceByGroupId",
  "States": {
    "DeleteBalanceByGroupId": {
        "Type": "Parallel",
        "End": true,
        "ResultPath": null,
        "Branches": [{
            "StartAt": "ScanAndDeleteTokensByGroupId",
            "States": {
                "ScanAndDeleteTokensByGroupId": {
                    "Type": "Task",
                    "Resource": "${module.delete_tokens_by_group_id_lambda.lambda_arn}",
                    "Next": "ContinueScanTokensChoice",
                    "Retry": ${local.default_retry},
                    "Catch": [{
                        "ErrorEquals": [
                            "States.ALL"
                        ],
                        "Next": "DeleteTokensFailure"
                    }]
                },
                "ContinueScanTokensChoice": {
                  "Type": "Choice",
                  "Choices": [{
                    "Variable": "$.is_scan_complete",
                    "BooleanEquals": true,
                    "Next": "DeleteTokensSuccess"
                  }, {
                    "Variable": "$.is_scan_complete",
                    "BooleanEquals": false,
                    "Next": "ScanAndDeleteTokensByGroupId"
                  }]
              },
              "DeleteTokensFailure": {
                "Comment": "Failed to delete tokens",
                "Type": "Fail"
              },
              "DeleteTokensSuccess": {
                "Type": "Succeed"
              }
            }
        }, {
            "StartAt": "ScanAndDeleteTransactionsByGroupId",
            "States": {
                "ScanAndDeleteTransactionsByGroupId": {
                    "Type": "Task",
                    "Resource": "${module.delete_transactions_by_group_id_lambda.lambda_arn}",
                    "Next": "ContinueScanTransactionsChoice",
                    "Retry": ${local.default_retry},
                    "Catch": [{
                        "ErrorEquals": [
                            "States.ALL"
                        ],
                        "Next": "DeleteTransactionsFailure"
                    }]
                },
                "ContinueScanTransactionsChoice": {
                  "Type": "Choice",
                  "Choices": [{
                    "Variable": "$.is_scan_complete",
                    "BooleanEquals": true,
                    "Next": "DeleteTransactionsSuccess"
                  }, {
                    "Variable": "$.is_scan_complete",
                    "BooleanEquals": false,
                    "Next": "ScanAndDeleteTransactionsByGroupId"
                  }]
              },
              "DeleteTransactionsFailure": {
                "Comment": "Failed to delete transactions",
                "Type": "Fail"
              },
              "DeleteTransactionsSuccess": {
                "Type": "Succeed"
              }
            }
        }]
      }
  }
}
EOF
}
