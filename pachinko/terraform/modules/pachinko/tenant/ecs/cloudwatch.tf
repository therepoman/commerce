resource aws_cloudwatch_log_group "cw_logs" {
  name              = "${var.service}-${var.tenant_name}-ECSLogGroup"
  retention_in_days = 14
}

resource aws_cloudwatch_log_metric_filter "logscan_errors" {
  log_group_name = aws_cloudwatch_log_group.cw_logs.name

  metric_transformation {
    name          = "${var.service}-${var.tenant_name}-logscan-errors-${var.env}"
    namespace     = "pachinko-logs"
    value         = "1"
    default_value = "0"
  }

  name    = "${var.service}-${var.tenant_name}-logscan-errors-${var.env}"
  pattern = "level=error"
}

resource aws_cloudwatch_log_metric_filter "logscan_panics" {
  log_group_name = aws_cloudwatch_log_group.cw_logs.name

  metric_transformation {
    name          = "${var.service}-${var.tenant_name}-logscan-panics-${var.env}"
    namespace     = "pachinko-logs"
    value         = "1"
    default_value = "0"
  }

  name    = "${var.service}-${var.tenant_name}-logscan-panics-${var.env}"
  pattern = "panic"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_alarm" {
  alarm_name                = "${var.service}-${var.tenant_name}-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "${var.service}-${var.tenant_name}-logscan-panics-${var.env}"
  namespace                 = "pachinko-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric is used for pachinko panics on the ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "logscan_errors_alarm" {
  alarm_name                = "${var.service}-${var.tenant_name}-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "${var.service}-${var.tenant_name}-logscan-errors-${var.env}"
  namespace                 = "pachinko-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = var.error_alarm_threshold
  datapoints_to_alarm       = "3"
  alarm_description         = "This metric is used for pachinko logscan errors on the ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}
