# The bucket where Go profiles captured by autoprof are saved.
resource "aws_s3_bucket" "autoprof" {
  # The bucket names follows the standard "autoprof-<app_name>-<region>".
  bucket = "autoprof-${var.tenant_name}-${var.env}-us-west-2"
  region = "us-west-2"
  acl    = "private"
}

# This policy enables the application to write files to the S3 bucket.
resource "aws_s3_bucket_policy" "autoprof" {
  bucket = aws_s3_bucket.autoprof.bucket
  policy = data.aws_iam_policy_document.autoprof.json
}

# This role allows the Autoprof Service account to read
# from the application's autoprof s3 bucket.
resource "aws_iam_role" "autoprof_reader" {
  name               = "${var.tenant_name}-autoprof-reader"
  assume_role_policy = data.aws_iam_policy_document.autoprof_reader_role.json
}

# This policy gives the autoprof_reader role the ability
# to read the autoprof s3 bucket.
resource "aws_iam_role_policy" "autoprof_reader" {
  name   = "${aws_iam_role.autoprof_reader.name}-policy"
  role   = aws_iam_role.autoprof_reader.id
  policy = data.aws_iam_policy_document.autoprof_reader_bucket.json
}

# Used to build the JSON IAM policy for the autoprof S3 bucket.
# This policy enables the Autoprof service to perform actions
# on the S3 autoprof bucket and its objects.
data "aws_iam_policy_document" "autoprof_reader_bucket" {
  # Enables actions on the S3 autoprof bucket.
  statement {
    resources = ["arn:aws:s3:::${aws_s3_bucket.autoprof.bucket}"]

    actions = [
      "s3:GetBucketLocation",
      "s3:GetBucketNotification",
      "s3:ListBucket",
      "s3:PutBucketNotification",
    ]
  }

  # Enables actions on the S3 autoprof bucket's objects.
  statement {
    resources = ["arn:aws:s3:::${aws_s3_bucket.autoprof.bucket}/*"]
    actions   = ["s3:GetObject"]
  }
}

# Used to build the JSON IAM policy for the autoprof reader role.
# This policy enables the Autoprof Service account to assume
# our account's autoprof_reader role.
data "aws_iam_policy_document" "autoprof_reader_role" {
  statement {
    actions = ["sts:AssumeRole"]

    # Identifies the Autoprof Service role.
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::636562298954:role/autoprof-reader"]
    }
  }
}

# This policy gives all access to the autoprof S3 bucket
# to anything in the account.
data "aws_iam_policy_document" "autoprof" {
  policy_id = "${aws_s3_bucket.autoprof.bucket}-policy"

  statement {
    sid = "${aws_s3_bucket.autoprof.bucket}-access"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.account_id}:root"]
    }

    actions   = ["s3:*"]
    resources = ["arn:aws:s3:::${aws_s3_bucket.autoprof.bucket}/*"]
  }
}
