output "service_alb_dns" {
  value = module.service.dns
}

output "cert_arn" {
  value = module.service.cert
}

output "service_role_arn" {
  value = module.service.service_role_arn
}