# Beanstalk general variables
variable "account_id" {
  description = "AWS Account ID needed for Docker URL"
}

variable "tenant_name" {}

variable "service" {
  description = "All lower­case, in  team/service/[role] format. This tag should indicate the server’s function and what is running on it;  role is optional but highly recommended. #REQUIRED"
}

# Note: All of the variables below are used to define EB environment configuration.
# If the description is not sufficient, you may find additional information and context
# at: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
variable "vpc_id" {
  description = "VPC ID used for the environment. #REQUIRED"
}

variable "subnet_ids" {
  description = "Comma-separated string of vpc security groups to be used by instances. #REQUIRED"
}

variable "security_group" {
  description = "Assign one or more security groups that you created to the load balancer. #REQUIRED"
}

variable "redis_sec_group_id" {}

variable "min_host_count" {
  description = "Minimum number of hosts in the beanstalk ASG"
  default     = "1"
}

variable "max_host_count" {
  description = "Maximum number of hosts in the beanstalk ASG"
  default     = "4"
}

variable "cpu" {}

variable "memory" {}

variable "env" {}

variable "error_alarm_threshold" {
  default = "15"
}

variable "ecs_ec2_backed_min_instances" {
  default = 5
}

variable "sandstorm_arn" {
  description = "sandstorm arn yo"
}

variable "ecr_repo" {}

variable "ecs_upgrade_sns_topic" {}

variable "alb_access_logs_expiration_days" {}