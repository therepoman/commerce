locals {
  cpu = {
    prod    = 4096
    staging = 512
  }

  memory = {
    prod    = 4096
    staging = 512
  }

  dns_prefix = {
    prod    = "prod.pachinko.twitch.a2z.com"
    staging = "staging.pachinko.twitch.a2z.com"
  }
}

module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name   = "${var.service}-${var.tenant_name}-${lower(var.env)}"
  repo   = "${var.service}/${var.tenant_name}"
  image  = var.ecr_repo
  port   = 8000
  cpu    = local.cpu[var.env]
  memory = local.memory[var.env]

  canary_cpu    = local.cpu[var.env]
  canary_memory = local.memory[var.env]

  counts = {
    min = var.min_host_count
    max = var.max_host_count
  }

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = var.env
    },
    {
      name  = "TENANT"
      value = var.tenant_name
    },
  ]

  cluster                  = module.cluster.ecs_name
  fargate_platform_version = ""
  security_group           = var.security_group
  region                   = "us-west-2"
  vpc_id                   = var.vpc_id
  environment              = var.env
  subnets                  = var.subnet_ids
  dns                      = "${var.tenant_name}.${local.dns_prefix[var.env]}"
  healthcheck              = "/ping"
  http2_enabled            = false
  debug_enabled            = false

  access_logs_expiration_days = var.alb_access_logs_expiration_days
}

// For S2S2
resource "aws_iam_policy" "s2s2_iam_policy" {
  name        = "${var.service}_${var.tenant_name}_${lower(var.env)}_s2s2_iam_policy"
  description = "required IAM permission to use S2S2"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "execute-api:Invoke"
            ],
            "Resource": [
                "arn:aws:execute-api:*:985585625942:*"
            ],
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_service_attachment" {
  role       = module.service.service_role
  policy_arn = aws_iam_policy.s2s2_iam_policy.arn
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_canary_attachment" {
  role       = module.service.canary_role
  policy_arn = aws_iam_policy.s2s2_iam_policy.arn
}