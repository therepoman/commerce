module "service_iam_attachments" {
  source        = "./iam"
  role_name     = module.service.service_role
  sandstorm_arn = var.sandstorm_arn
  tenant_name   = var.tenant_name
}

module "canary_iam_attachments" {
  source        = "./iam"
  role_name     = module.service.canary_role
  sandstorm_arn = var.sandstorm_arn
  tenant_name   = var.tenant_name
}
