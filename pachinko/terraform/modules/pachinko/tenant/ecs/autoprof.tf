module "main_autoprof" {
  source      = "./autoprof"
  tenant_name = var.tenant_name
  env         = var.env
  account_id  = var.account_id
}

module "canary_autoprof" {
  source      = "./autoprof"
  tenant_name = "${var.tenant_name}-canary"
  env         = var.env
  account_id  = var.account_id
}
