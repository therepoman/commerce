variable "role_name" {
  description = "the name of the role to attach policies to"
}

variable "sandstorm_arn" {
  description = "sandstorm arn yo"
}

variable "service" {
  description = "Short name of the service being built. Should probably leave this as the default"
  default     = "pachinko"
}

variable "tenant_name" {}
