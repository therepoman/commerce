resource aws_iam_role_policy "ecs_task_role_sandstorm_policy" {
  name = "${var.service}-${var.tenant_name}-sandstorm-policy"
  role = var.role_name

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "${var.sandstorm_arn}",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource aws_iam_role_policy "ecs_task_role_s2s_policy" {
  name = "${var.service}-${var.tenant_name}-s2s-policy"
  role = var.role_name

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "arn:aws:iam::180116294062:role/malachai/*",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource aws_iam_role_policy_attachment "ecs_task_role_dynamo_access" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// Grant the Instance Profile full Firehose access
resource "aws_iam_role_policy_attachment" "firehose_policy_attachment" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFirehoseFullAccess"
}

// Grant the Instance Profile Read only Elasticache Permissions
resource "aws_iam_role_policy_attachment" "elasticache_policy_attachment" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonElastiCacheReadOnlyAccess"
}

resource aws_iam_role_policy_attachment "cloudwatch_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  role       = var.role_name
}

resource aws_iam_role_policy_attachment "secrets_manager_access" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "sfn_lambda_execution"
  role   = var.role_name
  policy = data.aws_iam_policy_document.lambda_execution_policy_document.json
}
