resource "aws_s3_bucket" "lambda_bucket" {
  bucket = "${var.tenant_name}-${var.env}-lambdas-source"

  lifecycle {
    ignore_changes = [acl, force_destroy]
  }
}
