module "delete_tokens_by_owner_id_lambda" {
  source             = "./privacy_lambda"
  env                = var.env
  security_groups    = var.security_group_id
  subnets            = var.subnets
  tenant_name        = var.tenant_name
  lambda_name        = "delete_tokens_by_owner_id"
  lambda_description = "Lambda function for deleting tokens by matching ownerId for tenant ${var.tenant_name} in ${var.env}"
  s3_bucket          = aws_s3_bucket.lambda_bucket.bucket
}

module "delete_transactions_by_owner_id_lambda" {
  source             = "./privacy_lambda"
  env                = var.env
  security_groups    = var.security_group_id
  subnets            = var.subnets
  tenant_name        = var.tenant_name
  lambda_name        = "delete_transactions_by_owner_id"
  lambda_description = "Lambda function for deleting transactions by matching ownerId for tenant ${var.tenant_name} in ${var.env}"
  s3_bucket          = aws_s3_bucket.lambda_bucket.bucket

  // timeout needs to be this large so long running parallel scans do not exceed default 3s limit.
  timeout = 900

  // memory size needs to be this large so long running parallel scans do not exceed default 128MB limit.
  memory_size = 1792
}

module "delete_tokens_by_group_id_lambda" {
  source             = "./privacy_lambda"
  env                = var.env
  security_groups    = var.security_group_id
  subnets            = var.subnets
  tenant_name        = var.tenant_name
  lambda_name        = "delete_tokens_by_group_id"
  lambda_description = "Lambda function for deleting tokens by matching groupId for tenant ${var.tenant_name} in ${var.env}"
  s3_bucket          = aws_s3_bucket.lambda_bucket.bucket

  // timeout needs to be this large so long running parallel scans do not exceed default 3s limit.
  timeout = 900

  // memory size needs to be this large so long running parallel scans do not exceed default 128MB limit.
  memory_size = 1792
}

module "delete_transactions_by_group_id_lambda" {
  source             = "./privacy_lambda"
  env                = var.env
  security_groups    = var.security_group_id
  subnets            = var.subnets
  tenant_name        = var.tenant_name
  lambda_name        = "delete_transactions_by_group_id"
  lambda_description = "Lambda function for deleting transactions by matching groupId for tenant ${var.tenant_name} in ${var.env}"
  s3_bucket          = aws_s3_bucket.lambda_bucket.bucket

  // timeout needs to be this large so long running parallel scans do not exceed default 3s limit.
  timeout = 900

  // memory size needs to be this large so long running parallel scans do not exceed default 128MB limit.
  memory_size = 1792
}

module "start_delete_by_group_id_lambda" {
  source             = "./privacy_lambda"
  env                = var.env
  security_groups    = var.security_group_id
  subnets            = var.subnets
  tenant_name        = var.tenant_name
  lambda_name        = "start_delete_by_group_id"
  lambda_description = "Lambda function for starting step function to delete matching groupId for tenant ${var.tenant_name} in ${var.env}"
  s3_bucket          = aws_s3_bucket.lambda_bucket.bucket
}

resource "aws_lambda_permission" "start_delete_by_group_id_lambda_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch-${module.start_delete_by_group_id_lambda.lambda_name}"
  action        = "lambda:InvokeFunction"
  function_name = module.start_delete_by_group_id_lambda.lambda_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_delete_by_group_id.arn
}

module "start_delete_by_owner_id_lambda" {
  source             = "./privacy_lambda"
  env                = var.env
  security_groups    = var.security_group_id
  subnets            = var.subnets
  tenant_name        = var.tenant_name
  lambda_name        = "start_delete_by_owner_id"
  lambda_description = "Lambda function for starting step function to delete matching ownerId for tenant ${var.tenant_name} in ${var.env}"
  s3_bucket          = aws_s3_bucket.lambda_bucket.bucket
}

resource "aws_lambda_permission" "start_delete_by_owner_id_lambda_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch-${module.start_delete_by_owner_id_lambda.lambda_name}"
  action        = "lambda:InvokeFunction"
  function_name = module.start_delete_by_owner_id_lambda.lambda_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_delete_by_owner_id.arn
}

module "add_tokens_processor" {
  source = "./add_tokens_processor"
  env = var.env
  tenant_name = var.tenant_name
  s3_bucket = aws_s3_bucket.lambda_bucket.bucket
  security_groups = var.security_group_id
  subnets = var.subnets
  queue_arn = aws_sqs_queue.add_tokens_sqs_queue.arn
}
