resource "aws_cloudwatch_metric_alarm" "step_function_executions_failed_alarm" {
  alarm_name                = "${var.tenant_name}-${var.env}-${var.state_machine_name}-executions-failed-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/States"
  metric_name               = "ExecutionsFailed"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.state_machine_name} executions failed in ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = aws_sfn_state_machine.state_machine.id
  }
}
