variable "state_machine_name" {
  description = "The name of the state machine."
}

variable "step_function_definition" {
  description = "The definition of the step function."
}

variable "region" {
  default = "us-west-2"
}

variable "tenant_name" {
  description = "The name of the tenant."
}

variable "env" {
  description = "Whether it's prod, dev, etc."
}
