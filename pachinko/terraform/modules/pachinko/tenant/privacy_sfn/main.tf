resource "aws_sfn_state_machine" "state_machine" {
  name       = var.state_machine_name
  role_arn   = aws_iam_role.sfn.arn
  definition = var.step_function_definition
}
