resource "aws_sns_topic" "balance_updates_topic" {
  name = "${var.tenant_name}-${var.suffix}-updates"
}

resource "aws_sns_topic_policy" "balance_updates_topic_policy" {
  arn = aws_sns_topic.balance_updates_topic.arn

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "${aws_sns_topic.balance_updates_topic.name}-policy",
  "Statement": [
    {
      "Sid":    "default",
      "Effect": "Allow",
      "Principal": {
        "AWS": ["${join("\",\"",formatlist("arn:aws:iam::%s:root", var.updates_sns_subscribers))}"]
      },
      "Action": [
        "SNS:Subscribe",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.balance_updates_topic.arn}"
    },
    {
      "Sid": "allowPublish",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${var.account_id}:root"
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.balance_updates_topic.arn}"
    }
  ]
}
POLICY
}

# Async Add Tokens API SNS Topic
resource "aws_sns_topic" "add_tokens_topic" {
  name = "${var.tenant_name}-${var.suffix}-add-tokens"
}

resource "aws_sns_topic_policy" "add_tokens_topic_policy" {
  arn = aws_sns_topic.add_tokens_topic.arn

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "${aws_sns_topic.add_tokens_topic.name}-policy",
  "Statement": [
    {
      "Sid": "allowSNS",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${var.account_id}:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:Subscribe",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.add_tokens_topic.arn}"
    }
  ]
}
POLICY
}