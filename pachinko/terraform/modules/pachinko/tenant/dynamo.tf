module "ledgerman" {
  source          = "./ledgerman"
  tenant_name     = var.tenant_name
  suffix          = var.suffix
  security_groups = "${var.redis_security_group_id},${var.security_group_id}"
  subnets         = var.subnets
  s3_bucket       = aws_s3_bucket.lambda_bucket.bucket
}

module "tokens_table" {
  source                   = "./tokens_table"
  token_name               = var.tenant_name
  suffix                   = var.suffix
  min_read_capacity        = var.min_tokens_read_capacity
  min_write_capacity       = var.min_tokens_write_capacity
  max_read_capacity        = var.max_tokens_read_capacity
  min_gsi_read_capacity    = var.min_tokens_gsi_read_capacity
  min_gsi_write_capacity   = var.min_tokens_gsi_write_capacity
  max_gsi_read_capacity    = var.max_tokens_gsi_read_capacity
  max_gsi_write_capacity   = var.max_tokens_gsi_write_capacity
  enable_replica_table_gsi = var.enable_tokens_replica_table_gsi
  max_write_capacity       = var.max_tokens_write_capacity
  read_utilization         = var.read_utilization
  write_utilization        = var.write_utilization
  autoscaling_role         = var.autoscaling_role
  ledgerman_lambda_arn     = var.tenant_name == "cp" ? "" : module.ledgerman.lambda_arn // no ledgerman stream processing for CoPo
  backup_region            = var.backup_region
  profile                  = var.profile
  enable_capacity_alarms   = var.enable_tokens_capacity_alarms
  allow_full_control_for_account_id = var.tokens_allow_full_control_for_account_id
}

module "stickers_transaction_table" {
  source                 = "./transactions_table"
  token_name             = var.tenant_name
  suffix                 = var.suffix
  min_read_capacity      = var.min_transactions_read_capacity
  min_write_capacity     = var.min_transactions_write_capacity
  max_read_capacity      = var.max_transactions_read_capacity
  max_write_capacity     = var.max_transactions_write_capacity
  read_utilization       = var.read_utilization
  write_utilization      = var.write_utilization
  autoscaling_role       = var.autoscaling_role
  backup_region          = var.backup_region
  profile                = var.profile
  enable_ttl             = var.enable_transactions_ttl
  enable_capacity_alarms = var.enable_transactions_capacity_alarms
}

module "locks_table" {
  source             = "./locks_table"
  token_name         = var.tenant_name
  suffix             = var.suffix
  min_read_capacity  = var.min_locks_read_capacity
  min_write_capacity = var.min_locks_write_capacity
  max_read_capacity  = var.max_locks_read_capacity
  max_write_capacity = var.max_locks_write_capacity
  autoscaling_role   = var.autoscaling_role
  backup_region      = var.backup_region
  profile            = var.profile
}
