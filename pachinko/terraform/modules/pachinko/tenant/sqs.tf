module "delete_balance_by_group_id_sqs" {
  source                     = "./privacy_sqs"
  queue_name                 = "${var.tenant_name}-${var.env}-DeleteBalanceByGroupId"
  alarm_dlq                  = true
  message_sender_arn         = module.ecs.service_role_arn
  message_receiver_role_name = module.start_delete_by_group_id_lambda.lambda_role_name
}

module "delete_balance_by_owner_id_sqs" {
  source                     = "./privacy_sqs"
  queue_name                 = "${var.tenant_name}-${var.env}-DeleteBalanceByOwnerId"
  alarm_dlq                  = true
  message_sender_arn         = module.ecs.service_role_arn
  message_receiver_role_name = module.start_delete_by_owner_id_lambda.lambda_role_name
}

# Async Add Tokens API SQS Queue
resource "aws_sqs_queue" "add_tokens_sqs_queue" {
  message_retention_seconds  = "1209600"
  name                       = "${var.tenant_name}-${var.env}-add-tokens"
  delay_seconds              = "0"
  visibility_timeout_seconds = "60"

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.add_tokens_sqs_queue_deadletter.arn}",
  "maxReceiveCount":     4
}
EOF
}

resource "aws_sqs_queue" "add_tokens_sqs_queue_deadletter" {
  name = "${var.tenant_name}-${var.env}-add-tokens-deadletter"
}

resource "aws_sqs_queue_policy" "add_tokens_queue_policy" {
  queue_url = aws_sqs_queue.add_tokens_sqs_queue.id
  policy    = data.aws_iam_policy_document.add_tokens_queue_policy_doc.json
}

data "aws_iam_policy_document" "add_tokens_queue_policy_doc" {
  statement {
    sid = "${aws_sqs_queue.add_tokens_sqs_queue.name}-Sid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [aws_sqs_queue.add_tokens_sqs_queue.arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [aws_sns_topic.add_tokens_topic.arn]
    }
  }
}

resource "aws_sns_topic_subscription" "add_tokens_sqs" {
  topic_arn = aws_sns_topic.add_tokens_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.add_tokens_sqs_queue.arn
}