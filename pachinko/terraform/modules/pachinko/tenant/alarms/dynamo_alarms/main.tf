variable "table_name" {
  type = string
}

variable "threshold" {
  type = string
}

variable "enable_capacity_alarms" {
  default = false
}

variable "max_read_capacity" {
  type    = number
  default = 10000
}

variable "max_write_capacity" {
  type    = number
  default = 10000
}

resource "aws_cloudwatch_metric_alarm" "throttled_write_events_alarm" {
  alarm_name                = "${var.table_name}-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = var.threshold
  unit                      = "Count"
  alarm_description         = "${var.table_name} DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = var.table_name
  }
}

resource "aws_cloudwatch_metric_alarm" "throttled_read_events_alarm" {
  alarm_name                = "${var.table_name}-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = var.threshold
  unit                      = "Count"
  alarm_description         = "${var.table_name} DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = var.table_name
  }
}

resource "aws_cloudwatch_metric_alarm" "approaching_maximum_read_capacity_alarm" {
  count                     = var.enable_capacity_alarms ? 1 : 0
  alarm_name                = "${var.table_name}-approach-max-read-capacity"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  threshold                 = var.max_read_capacity * 0.8
  alarm_description         = "${var.table_name} DynamoDB table has consumed 80% of its Maximum Read Capacity"
  insufficient_data_actions = []

  metric_query {
    id          = "e1"
    expression  = "m1/60"
    label       = "ConsumedReadCapacityUnits"
    return_data = "true"
  }

  metric_query {
    id = "m1"
    metric {
      metric_name = "ConsumedReadCapacityUnits"
      period      = "60"
      stat        = "Sum"
      unit        = "Count"
      namespace   = "AWS/DynamoDB"
      dimensions = {
        TableName = var.table_name
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "approaching_maximum_write_capacity_alarm" {
  count                     = var.enable_capacity_alarms ? 1 : 0
  alarm_name                = "${var.table_name}-approach-max-write-capacity"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  threshold                 = var.max_write_capacity * 0.8
  alarm_description         = "${var.table_name} DynamoDB table has consumed 80% of its Maximum Write Capacity"
  insufficient_data_actions = []

  metric_query {
    id          = "e1"
    expression  = "m1/60"
    label       = "ConsumedWriteCapacityUnits"
    return_data = "true"
  }

  metric_query {
    id = "m1"
    metric {
      metric_name = "ConsumedWriteCapacityUnits"
      period      = "60"
      stat        = "Sum"
      unit        = "Count"
      namespace   = "AWS/DynamoDB"
      dimensions = {
        TableName = var.table_name
      }
    }
  }
}