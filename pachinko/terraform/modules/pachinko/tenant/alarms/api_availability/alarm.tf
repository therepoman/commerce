resource "aws_cloudwatch_metric_alarm" "availability_alarm" {
  alarm_name                = "${var.service}-${var.tenant_name}-${var.alarm_name}"
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = "3"
  threshold                 = var.low_watermark
  alarm_description         = var.alarm_description
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  metric_query {
    id          = "e1"
    expression  = "(FILL(m1,1)-m2)/FILL(m1,1)"
    label       = "Availability"
    return_data = "true"
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = var.requests_metric_name_total
      namespace   = "pachinko"
      period      = "300"
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        Stage    = var.env
        Region   = "us-west-2"
        Service  = "pachinko"
        Substage = var.tenant_metric_name
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = var.requests_metric_name_5xx
      namespace   = "pachinko"
      period      = "300"
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        Stage    = var.env
        Region   = "us-west-2"
        Service  = "pachinko"
        Substage = var.tenant_metric_name
      }
    }
  }
}
