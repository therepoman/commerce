variable "env" {}

variable "service" {}

variable "tenant_metric_name" {
  description = "The name of the tenant used as the substage for metric emission. This is an uppercase version of the tenant tag in the prod.yaml file."
}

variable "tenant_name" {
  description = "The name of the tenant"
}

variable "alarm_name" {
  description = "Suffix for the alarm name"
}

variable "alarm_description" {
  description = "Description of alarm"
}

variable "low_watermark" {
  description = "0-100. Dropping below this rate will trigger alarm."
}

variable "requests_metric_name_total" {
  description = "Name of metric representing total requests to a single API."
}

variable "requests_metric_name_5xx" {
  description = "Name of metric representing total 5xx requests to a single API."
}
