# This sets up a cloudformation stack to automatically send the alb logs in S3 to Twitch Security.
# If this is your first time setting this up, make sure you reach out to #security-expectations
# to ensure logs are flowing.
#
# this counts as HTTP Logs under https://wiki.xarth.tv/display/CTI/%5BRM-03%5D+What+to+log
# requirement: https://wiki.xarth.tv/display/CTI/%5BRM-04%5D+Log+Rotation+and+Retention
# instruction: https://wiki.xarth.tv/pages/viewpage.action?spaceKey=SEC&title=Sending+Logs+to+Twitch+Security#SendingLogstoTwitchSecurity-AdvancedScenarios

# Source bucket is hardcoded because terracode does not expose this
# https://git.xarth.tv/subs/terracode/blob/master/service/alb.tf#L91

resource "aws_cloudformation_stack" "alb_log_funnel" {
  count = var.env == "prod" ? 1 : 0
  name  = "${var.service}-${var.tenant_name}-alb-logs-to-twitch-security"

  parameters = {
    DataStorageLocation = "indexed"
    Environment         = "production"
    LogType             = "AWSLogs"
    SourceBucket        = "twitch-${var.service}-${var.tenant_name}-${var.env}-${var.env}-alb-access-logs"
  }

  template_url = "https://s3-us-west-2.amazonaws.com/twitch-tails-cloudformation-production-us-west-2/s3-replicate.yaml"
  capabilities = ["CAPABILITY_IAM"]
}
