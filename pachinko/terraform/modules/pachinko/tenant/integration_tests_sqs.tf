# For integration tests
resource "aws_iam_role" "integration_test_role" {
  count = var.create_sns_subscriber_sqs ? 1 : 0
  name  = "integration_test_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "AWS": "arn:aws:iam::${var.account_id}:root"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Add Tokens
resource "aws_sqs_queue" "add_tokens_integration_test_sqs_queue" {
  count                      = var.create_sns_subscriber_sqs ? 1 : 0
  message_retention_seconds  = 120
  name                       = "${var.tenant_name}-${var.env}-integ-tests-add_tokens-sqs"
  delay_seconds              = "0"
  visibility_timeout_seconds = 120
}

resource "aws_sns_topic_subscription" "add_tokens_integration_test_subscription" {
  count     = var.create_sns_subscriber_sqs ? 1 : 0
  topic_arn = aws_sns_topic.balance_updates_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.add_tokens_integration_test_sqs_queue[count.index].arn
}

resource "aws_sqs_queue_policy" "add_tokens_sqs_queue_policy" {
  count     = var.create_sns_subscriber_sqs ? 1 : 0
  queue_url = aws_sqs_queue.add_tokens_integration_test_sqs_queue[count.index].id
  policy    = data.aws_iam_policy_document.add_tokens_sqs_queue_policy_doc[count.index].json
}

data "aws_iam_policy_document" "add_tokens_sqs_queue_policy_doc" {
  count = var.create_sns_subscriber_sqs ? 1 : 0

  statement {
    sid = "SenderPermissionAddTokens"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]

      type = "AWS"
    }

    resources = [
      aws_sqs_queue.add_tokens_integration_test_sqs_queue[count.index].arn,
    ]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values   = [aws_sns_topic.balance_updates_topic.arn]
    }
  }
}

resource "aws_iam_policy" "add_tokens_sqs_queue_reader_policy" {
  count       = var.create_sns_subscriber_sqs ? 1 : 0
  name        = "IntegrationTest_AddTokens"
  description = "This allows read access to SQS queue for AddTokens Integration Test"
  policy      = data.aws_iam_policy_document.add_tokens_sqs_queue_reader_policy_doc[count.index].json
}

data "aws_iam_policy_document" "add_tokens_sqs_queue_reader_policy_doc" {
  count = var.create_sns_subscriber_sqs ? 1 : 0

  statement {
    sid    = "ReaderPermissionAddTokens"
    effect = "Allow"

    actions = [
      "*",
    ]

    resources = [
      aws_sqs_queue.add_tokens_integration_test_sqs_queue[count.index].arn,
    ]
  }
}

resource aws_iam_role_policy_attachment "add_tokens_integration_test_sqs_read_access_attachment" {
  count      = var.create_sns_subscriber_sqs ? 1 : 0
  role       = aws_iam_role.integration_test_role[count.index].name
  policy_arn = aws_iam_policy.add_tokens_sqs_queue_reader_policy[count.index].arn
}

# Async Add Tokens
resource "aws_sqs_queue" "async_add_tokens_integration_test_sqs_queue" {
  count                      = var.create_sns_subscriber_sqs ? 1 : 0
  message_retention_seconds  = 120
  name                       = "${var.tenant_name}-${var.env}-integ-tests-async_add_tokens-sqs"
  delay_seconds              = "0"
  visibility_timeout_seconds = 120
}

resource "aws_sns_topic_subscription" "async_add_tokens_integration_test_subscription" {
  count     = var.create_sns_subscriber_sqs ? 1 : 0
  topic_arn = aws_sns_topic.balance_updates_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.async_add_tokens_integration_test_sqs_queue[count.index].arn
}

resource "aws_sqs_queue_policy" "async_add_tokens_sqs_queue_policy" {
  count     = var.create_sns_subscriber_sqs ? 1 : 0
  queue_url = aws_sqs_queue.async_add_tokens_integration_test_sqs_queue[count.index].id
  policy    = data.aws_iam_policy_document.async_add_tokens_sqs_queue_policy_doc[count.index].json
}

data "aws_iam_policy_document" "async_add_tokens_sqs_queue_policy_doc" {
  count = var.create_sns_subscriber_sqs ? 1 : 0

  statement {
    sid = "SenderPermissionAsyncAddTokens"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]

      type = "AWS"
    }

    resources = [
      aws_sqs_queue.async_add_tokens_integration_test_sqs_queue[count.index].arn,
    ]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values   = [aws_sns_topic.balance_updates_topic.arn]
    }
  }
}

resource "aws_iam_policy" "async_add_tokens_sqs_queue_reader_policy" {
  count       = var.create_sns_subscriber_sqs ? 1 : 0
  name        = "IntegrationTest_AsyncAddTokens"
  description = "This allows read access to SQS queue for AsyncAddTokens Integration Test"
  policy      = data.aws_iam_policy_document.async_add_tokens_sqs_queue_reader_policy_doc[count.index].json
}

data "aws_iam_policy_document" "async_add_tokens_sqs_queue_reader_policy_doc" {
  count = var.create_sns_subscriber_sqs ? 1 : 0

  statement {
    sid    = "ReaderPermissionAsyncAddTokens"
    effect = "Allow"

    actions = [
      "*",
    ]

    resources = [
      aws_sqs_queue.async_add_tokens_integration_test_sqs_queue[count.index].arn,
    ]
  }
}

resource aws_iam_role_policy_attachment "async_add_tokens_integration_test_sqs_read_access_attachment" {
  count      = var.create_sns_subscriber_sqs ? 1 : 0
  role       = aws_iam_role.integration_test_role[count.index].name
  policy_arn = aws_iam_policy.async_add_tokens_sqs_queue_reader_policy[count.index].arn
}

# Delete Balance By Owner ID
resource "aws_sqs_queue" "delete_balance_by_owner_id_integration_test_sqs_queue" {
  count                      = var.create_sns_subscriber_sqs ? 1 : 0
  message_retention_seconds  = 120
  name                       = "${var.tenant_name}-${var.env}-integ-tests-delete_balance_by_owner_id-sqs"
  delay_seconds              = "0"
  visibility_timeout_seconds = 120
}

resource "aws_sns_topic_subscription" "delete_balance_by_owner_id_integration_test_subscription" {
  count     = var.create_sns_subscriber_sqs ? 1 : 0
  topic_arn = aws_sns_topic.balance_updates_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.delete_balance_by_owner_id_integration_test_sqs_queue[count.index].arn
}

resource "aws_sqs_queue_policy" "delete_balance_by_owner_id_sqs_queue_policy" {
  count     = var.create_sns_subscriber_sqs ? 1 : 0
  queue_url = aws_sqs_queue.delete_balance_by_owner_id_integration_test_sqs_queue[count.index].id
  policy    = data.aws_iam_policy_document.delete_balance_by_owner_id_sqs_queue_policy_doc[count.index].json
}

data "aws_iam_policy_document" "delete_balance_by_owner_id_sqs_queue_policy_doc" {
  count = var.create_sns_subscriber_sqs ? 1 : 0

  statement {
    sid = "SenderPermissionDeleteBalanceByOwnerID"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]

      type = "AWS"
    }

    resources = [
      aws_sqs_queue.delete_balance_by_owner_id_integration_test_sqs_queue[count.index].arn,
    ]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values   = [aws_sns_topic.balance_updates_topic.arn]
    }
  }
}

resource "aws_iam_policy" "delete_balance_by_owner_id_sqs_queue_reader_policy" {
  count       = var.create_sns_subscriber_sqs ? 1 : 0
  name        = "IntegrationTest_DeleteBalanceByOwnerID"
  description = "This allows read access to SQS queue for DeleteBalanceByOwnerID Integration Test"
  policy      = data.aws_iam_policy_document.delete_balance_by_owner_id_sqs_queue_reader_policy_doc[count.index].json
}

data "aws_iam_policy_document" "delete_balance_by_owner_id_sqs_queue_reader_policy_doc" {
  count = var.create_sns_subscriber_sqs ? 1 : 0

  statement {
    sid    = "ReaderPermissionDeleteBalanceByOwnerID"
    effect = "Allow"

    actions = [
      "*",
    ]

    resources = [
      aws_sqs_queue.delete_balance_by_owner_id_integration_test_sqs_queue[count.index].arn,
    ]
  }
}

resource aws_iam_role_policy_attachment "delete_balance_by_owner_id_integration_test_sqs_read_access_attachment" {
  count      = var.create_sns_subscriber_sqs ? 1 : 0
  role       = aws_iam_role.integration_test_role[count.index].name
  policy_arn = aws_iam_policy.delete_balance_by_owner_id_sqs_queue_reader_policy[count.index].arn
}
