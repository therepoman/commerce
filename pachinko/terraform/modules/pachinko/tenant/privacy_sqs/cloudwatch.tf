resource "aws_cloudwatch_metric_alarm" "dlq_not_empty" {
  count               = var.alarm_dlq ? 1 : 0
  alarm_name          = "${var.queue_name}_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  dimensions = {
    QueueName = "${var.queue_name}_deadletter${var.queue_name_suffix}"
  }
  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of Pachinko tenant's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}
