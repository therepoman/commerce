resource "aws_dynamodb_table" "locks_table" {
  name           = "${var.token_name}-locks-gbl-${var.suffix}"
  hash_key       = "key"
  read_capacity  = var.min_read_capacity
  write_capacity = var.min_write_capacity

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    enabled        = true
    attribute_name = "ttl"
  }

  attribute {
    name = "key"
    type = "S"
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "transactions_table_alarms" {
  source             = "../../alarms/dynamo_alarms"
  table_name         = "${var.token_name}-locks-gbl-${var.suffix}"
  threshold          = "1"
}

module "locks_table_autoscaling" {
  source             = "../../../../dynamo_table_autoscaling"
  table_name         = "${var.token_name}-locks-gbl-${var.suffix}"
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
  autoscaling_role   = var.autoscaling_role
  region             = var.region
}
