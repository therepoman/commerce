variable "token_name" {
  description = "Type of tokens this table will lock on"
}

variable "suffix" {
  description = "Suffix at the end of dynamo table names"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}

variable "max_read_capacity" {
  description = "Maximum dynamodb read capacity"
}

variable "max_write_capacity" {
  description = "Maximum dynamodb write capacity"
}

variable "profile" {}

variable "backup_region" {}
