module "privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  region           = var.region
  name             = "${var.service}-${var.tenant_name}"
  environment      = var.env
  alb_dns          = module.ecs.service_alb_dns
  cert_arn         = module.ecs.cert_arn
  vpc_id           = var.vpc_id
  subnets          = split(",", var.subnets)
  whitelisted_arns = var.whitelisted_arns
}
