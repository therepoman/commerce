# Latency Alarms
resource "aws_cloudwatch_metric_alarm" "add_tokens_latency" {
  alarm_name          = "${var.service}-${var.tenant_name}-add-tokens-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "timing.twirp.AddTokens.response"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = var.add_tokens_latency_alarm_threshold
  alarm_description         = "Alarms when AddTokens API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "async_add_tokens_latency" {
  alarm_name          = "${var.service}-${var.tenant_name}-async-add-tokens-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "timing.twirp.AsyncAddTokens.response"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = var.async_add_tokens_latency_alarm_threshold
  alarm_description         = "Alarms when AsyncAddTokens API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "get_tokens_latency" {
  alarm_name          = "${var.service}-${var.tenant_name}-get-tokens-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "timing.twirp.GetToken.response"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = var.get_token_latency_alarm_threshold
  alarm_description         = "Alarms when GetToken API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "start_transaction_latency" {
  alarm_name          = "${var.service}-${var.tenant_name}-start-transaction-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "timing.twirp.StartTransaction.response"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = var.start_transaction_latency_alarm_threshold
  alarm_description         = "Alarms when StartTransaction API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "commit_transaction_latency" {
  alarm_name          = "${var.service}-${var.tenant_name}-commit-transaction-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "timing.twirp.CommitTransaction.response"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = var.commit_transaction_latency_alarm_threshold
  alarm_description         = "Alarms when CommitTransaction API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "get_transaction_records_latency" {
  alarm_name          = "${var.service}-${var.tenant_name}-get-transaction-records-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "timing.twirp.GetTransactionRecords.response"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.1"
  alarm_description         = "Alarms when GetTransactionRecords API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

# 500 Error Alarms
module "add_tokens_500_errors" {
  source                     = "./alarms/api_availability"
  alarm_description          = "Alarms when AddTokens API 500 rate breaches threshold"
  alarm_name                 = "add-tokens-500-errors"
  env                        = var.env
  low_watermark              = "0.99"
  requests_metric_name_5xx   = "counter.twirp.status_codes.AddTokens.500"
  requests_metric_name_total = "counter.twirp.AddTokens.requests"
  service                    = var.service
  tenant_metric_name         = var.tenant_metric_name
  tenant_name                = var.tenant_name
}

module "async_add_tokens_500_errors" {
  source                     = "./alarms/api_availability"
  alarm_description          = "Alarms when AsyncAddTokens API 500 rate breaches threshold"
  alarm_name                 = "async-add-tokens-500-errors"
  env                        = var.env
  low_watermark              = "0.99"
  requests_metric_name_5xx   = "counter.twirp.status_codes.AsyncAddTokens.500"
  requests_metric_name_total = "counter.twirp.AsyncAddTokens.requests"
  service                    = var.service
  tenant_metric_name         = var.tenant_metric_name
  tenant_name                = var.tenant_name
}

module "get_token_500_errors" {
  source                     = "./alarms/api_availability"
  alarm_description          = "Alarms when GetToken API 500 rate breaches threshold"
  alarm_name                 = "get-tokens-500-errors"
  env                        = var.env
  low_watermark              = "0.99"
  requests_metric_name_5xx   = "counter.twirp.status_codes.GetToken.500"
  requests_metric_name_total = "counter.twirp.GetToken.requests"
  service                    = var.service
  tenant_metric_name         = var.tenant_metric_name
  tenant_name                = var.tenant_name
}

module "start_transaction_500_errors" {
  source                     = "./alarms/api_availability"
  alarm_description          = "Alarms when StartTransaction API 500 rate breaches threshold"
  alarm_name                 = "start-transaction-500-errors"
  env                        = var.env
  low_watermark              = "0.99"
  requests_metric_name_5xx   = "counter.twirp.status_codes.StartTransaction.500"
  requests_metric_name_total = "counter.twirp.StartTransaction.requests"
  service                    = var.service
  tenant_metric_name         = var.tenant_metric_name
  tenant_name                = var.tenant_name
}

module "commit_transaction_500_errors" {
  source                     = "./alarms/api_availability"
  alarm_description          = "Alarms when CommitTransaction API 500 rate breaches threshold"
  alarm_name                 = "commit-transaction-500-errors"
  env                        = var.env
  low_watermark              = "0.99"
  requests_metric_name_5xx   = "counter.twirp.status_codes.CommitTransaction.500"
  requests_metric_name_total = "counter.twirp.CommitTransaction.requests"
  service                    = var.service
  tenant_metric_name         = var.tenant_metric_name
  tenant_name                = var.tenant_name
}

resource "aws_cloudwatch_metric_alarm" "get_transaction_records_500_errors" {
  alarm_name          = "${var.service}-${var.tenant_name}-get-transaction-records-500-errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "counter.twirp.status_codes.GetTransactionRecords.500"
  namespace           = "pachinko"

  dimensions = {
    Stage    = var.env
    Region   = "us-west-2"
    Service  = "pachinko"
    Substage = var.tenant_metric_name
  }

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  alarm_description         = "Alarms when GetTransactionRecords API 500 status code error count breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "dlq_not_empty" {
  alarm_name          = "${aws_sqs_queue.add_tokens_sqs_queue_deadletter.name}_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = aws_sqs_queue.add_tokens_sqs_queue_deadletter.name
  }
  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors the add tokens processor DLQ for ${var.tenant_name}. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}
