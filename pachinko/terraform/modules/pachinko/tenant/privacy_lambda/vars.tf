variable "tenant_name" {
  description = "The name of the tenant."
}

variable "security_groups" {
  description = "Twitch subnets Sec group ID."
}

variable "subnets" {
  description = "Private subnets in VPC."
}

variable "env" {
  description = "Whether it's prod, dev, etc."
}

variable "lambda_name" {
  description = "The name of the lambda."
}

variable "lambda_description" {
  description = "The description of the lambda."
}

variable "s3_bucket" {
  description = "The s3 bucket for the lambda source."
}

variable "timeout" {
  description = "The timeout in Seconds of the lambda."
  default = 300
}

variable "memory_size" {
  description = "The memory size in MB to allocate for the lambda."
  default = 128
}
