resource aws_cloudwatch_log_group "logs" {
  name = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
}

resource aws_cloudwatch_log_metric_filter "logscan_errors" {
  log_group_name = aws_cloudwatch_log_group.logs.name

  metric_transformation {
    name          = "${var.tenant_name}-${var.env}-${var.lambda_name}-logscan-errors"
    namespace     = "pachinko-logs"
    value         = "1"
    default_value = "0"
  }

  name    = "${var.tenant_name}-${var.env}-${var.lambda_name}-logscan-errors"
  pattern = "level=error"
}

resource aws_cloudwatch_log_metric_filter "logscan_panics" {
  log_group_name = aws_cloudwatch_log_group.logs.name

  metric_transformation {
    name          = "${var.tenant_name}-${var.env}-${var.lambda_name}-logscan-panics"
    namespace     = "pachinko-logs"
    value         = "1"
    default_value = "0"
  }

  name    = "${var.tenant_name}-${var.env}-${var.lambda_name}-logscan-panics"
  pattern = "panic"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_error" {
  alarm_name                = "${var.tenant_name}-${var.env}-${var.lambda_name}-logscan-error-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "${var.tenant_name}-${var.env}-${var.lambda_name}-errors"
  namespace                 = "pachinko-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  alarm_description         = "This metric is used for pachinko ${var.lambda_name} logscan errors on the ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_alarm" {
  alarm_name                = "${var.tenant_name}-${var.env}-${var.lambda_name}-logscan-panic-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "${var.tenant_name}-${var.env}-${var.lambda_name}-panics"
  namespace                 = "pachinko-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric is used for pachinko ${var.lambda_name} panics on the ${var.tenant_name} tenant"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}
