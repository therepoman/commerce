resource "aws_lambda_function" "lambda" {
  function_name = "${var.tenant_name}-${var.env}-${var.lambda_name}"
  role          = aws_iam_role.lambda_iam_role.arn
  description   = var.lambda_description
  handler       = var.lambda_name
  runtime       = "go1.x"
  s3_bucket     = var.s3_bucket
  s3_key        = "${var.lambda_name}/latest/${var.lambda_name}.zip"
  timeout       = var.timeout
  memory_size   = var.memory_size

  environment {
    variables = {
      ENVIRONMENT = var.env
      TENANT      = var.tenant_name
    }
  }

  vpc_config {
    security_group_ids = split(",", var.security_groups)
    subnet_ids         = split(",", var.subnets)
  }
}

output "lambda_name" {
  value = aws_lambda_function.lambda.function_name
}

output "lambda_arn" {
  value = aws_lambda_function.lambda.arn
}

output "lambda_role_name" {
  value = aws_iam_role.lambda_iam_role.name
}
