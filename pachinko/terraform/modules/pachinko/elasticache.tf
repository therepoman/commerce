locals {
  redis_instance_type = var.env == "staging" ? "cache.m3.medium" : "cache.m4.large"
}

resource "aws_elasticache_replication_group" "pachinko_redis_lock" {
  replication_group_id          = "pach-locks-${var.env}"
  replication_group_description = "pachinko redis non cluster"
  engine                        = "redis"
  engine_version                = "4.0.10"
  number_cache_clusters         = 3
  node_type                     = local.redis_instance_type
  parameter_group_name          = aws_elasticache_parameter_group.pachinko_redis_lock_params.name
  port                          = var.redis_port
  subnet_group_name             = aws_elasticache_subnet_group.pachinko_redis_subnet_group.name
  security_group_ids            = [aws_security_group.pachinko_redis_security_group.id]
}

resource "aws_elasticache_parameter_group" "pachinko_redis_lock_params" {
  family = "redis4.0"
  name   = "pachinko-lock-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "cluster-enabled"
    value = "no"
  }

  parameter {
    name  = "notify-keyspace-events"
    value = "Ex"
  }
}

resource "aws_elasticache_subnet_group" "pachinko_redis_subnet_group" {
  name       = "pachinko-${var.env}-cache"
  subnet_ids = split(",", var.private_subnets)
}

resource "aws_security_group" "pachinko_redis_security_group" {
  name        = "pachinko-${var.env}-redis"
  vpc_id      = var.vpc_id
  description = "Allows communication with redis server"

  ingress {
    from_port   = var.redis_port
    protocol    = "tcp"
    to_port     = var.redis_port
    cidr_blocks = var.private_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pachinko-${var.env}-redis"
  }
}
