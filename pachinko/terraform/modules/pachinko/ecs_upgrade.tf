//creates the lambda, and the SNS topic
module "upgrade_complete" {
  source = "git::git+ssh://git@git.xarth.tv/edge/upgrade-complete//terraform?ref=1171046271eaed04e01a77afd72896c0b7d4c94e"
  aws_account_id = "12345"
  function_zip_path = "${path.module}/files/upgrade-complete.zip"
}
