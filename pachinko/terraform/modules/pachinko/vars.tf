variable "account_id" {
  description = "AWS Account ID for the service"
}

variable "aws_profile" {
  type        = string
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = list(string)
}

variable "sg_id" {
  description = "Id of the security group"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/pachinko"
}

variable "service_short_name" {
  description = "Short name of the service being built. Should probably leave this as the default"
  default     = "pachinko"
}

variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "env_full" {
  description = "Whether it's production, development, etc"
}

variable "cname_prefix" {
  description = "Prefix of the cname"
}

# AWS ElastiCache
variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m4.large"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "min_host_count" {
  description = "Minimum number of hosts in the beanstalk ASG"
  default     = 1
}

variable "max_host_count" {
  description = "Maximum number of hosts in the beanstalk ASG"
  default     = 4
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "dynamo_min_read_capacity" {
  description = "Min read capacity for dynamodb tables"
}

variable "dynamo_min_write_capacity" {
  description = "Min write capacity for dynamodb tables"
}

variable "dynamo_min_gsi_read_capacity" {
  description = "Min read capacity for dynamodb GSIs"
}

variable "dynamo_min_gsi_write_capacity" {
  description = "Min write capacity for dynamodb GSIs"
}

variable "dynamo_transactions_min_read_capacity" {
  description = "Min read capacity for transaction table"
  default     = 50
}

variable "redshift_transactions_cluster" {
  description = "The name of the redshift cluster for the transactions audit"
}

variable "backup_region" {}

variable "profile" {}

variable "sandstorm_arn" {
  description = "sandstorm arn yo"
}

variable "cpu" {}

variable "memory" {}

variable "bits_updates_sns_subscribers" {
  description = "list of aws account numbers that have access to bits balance update SNS topic"
  type        = list(string)
}

variable "sub_tokens_updates_sns_subscribers" {
  description = "list of aws account numbers that have access to sub token balance update SNS topic"
  type        = list(string)
}

variable "ecs_ec2_backed_min_instances" {
  default = 5
}

variable "alb_access_logs_expiration_days" {
  default     = 7
  description = "Application Load Balancer Access Log Retention Period, defaults to 7 days"
}

variable "audit_logs_retention_in_days" {
  default     = 7
  description = "Access Audit Log Retention Period, defaults to 7 days"
}

variable "s2s_auth_logs_retention_in_days" {
  default     = 7
  description = "S2S Auth Log Retention Period, defaults to 7 days"
}