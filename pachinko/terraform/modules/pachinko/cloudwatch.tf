resource "aws_cloudwatch_metric_alarm" "transactions_to_redshift_delivery" {
  alarm_name = "transactions-to-redshift-ingestion-alarm"
  namespace  = "AWS/Firehose"

  dimensions = {
    DeliveryStreamName = "transactions-to-redshift-${var.env_full}"
  }

  metric_name         = "DeliveryToRedshift.Success"
  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}

resource "aws_cloudwatch_metric_alarm" "redshift_maintenance_mode" {
  alarm_name = "redshift-transactions-maintenance-mode-alarm"
  namespace  = "AWS/Redshift"

  dimensions = {
    ClusterIdentifier = "r${var.redshift_transactions_cluster}"
  }

  comparison_operator = "GreaterThanOrEqualToThreshold"
  statistic           = "Average"
  metric_name         = "MaintenanceMode"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "redshift_health" {
  alarm_name = "redshift-transactions-health-status-alarm"
  namespace  = "AWS/Redshift"

  dimensions = {
    ClusterIdentifier = "r${var.redshift_transactions_cluster}"
  }

  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  metric_name         = "HealthStatus"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}
