// Create resources according to the Wiki
// https://wiki.twitch.com/display/PS/How+to+Configure+a+VPC+Resolver+for+Internal+Twitch+DNS
locals {
  jtv_dns_ips = {
    prod = [
      "10.203.126.62",
      "10.203.124.218",
      "10.203.125.211",
      "10.203.126.205",
    ]

    staging = [
      "10.202.125.133",
      "10.202.124.213",
      "10.202.126.110",
      "10.202.124.10",
    ]
  }
}

resource "aws_route53_resolver_rule_association" "fwd_justin_tv_dns_cache_rule_association" {
  resolver_rule_id = aws_route53_resolver_rule.fwd_justin_tv_dns_cache.id
  vpc_id           = var.vpc_id
}

resource "aws_route53_resolver_rule" "fwd_justin_tv_dns_cache" {
  domain_name          = "justin.tv"
  name                 = "fwd_justin_tv_dns_cache"
  rule_type            = "FORWARD"
  resolver_endpoint_id = aws_route53_resolver_endpoint.outbound_endpoint.id

  target_ip {
    ip = element(local.jtv_dns_ips[var.env], 0)
  }

  target_ip {
    ip = element(local.jtv_dns_ips[var.env], 1)
  }

  target_ip {
    ip = element(local.jtv_dns_ips[var.env], 2)
  }

  target_ip {
    ip = element(local.jtv_dns_ips[var.env], 3)
  }
}

resource "aws_route53_resolver_endpoint" "outbound_endpoint" {
  name      = "${var.env}-pachinko-vpc-outbound-dns"
  direction = "OUTBOUND"

  security_group_ids = [
    var.sg_id,
  ]

  ip_address {
    subnet_id = element(split(",", var.private_subnets), 0)
  }

  ip_address {
    subnet_id = element(split(",", var.private_subnets), 1)
  }

  ip_address {
    subnet_id = element(split(",", var.private_subnets), 2)
  }
}

// Point DNS look up to AmazonProvidedDNS (route 53)
resource "aws_vpc_dhcp_options" "dns_resolver" {
  domain_name         = "${var.aws_region}.compute.internal"
  domain_name_servers = ["AmazonProvidedDNS"]

  // https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/set-time.html
  ntp_servers = ["169.254.169.123"]

  tags = {
    Name = "AmazonProvidedDNS"
  }
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = var.vpc_id
  dhcp_options_id = aws_vpc_dhcp_options.dns_resolver.id
}
