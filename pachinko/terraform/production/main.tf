module "pachinko" {
  source                                = "../modules/pachinko"
  aws_profile                           = "pachinko-prod"
  env                                   = "prod"
  env_full                              = "production"
  vpc_id                                = "vpc-03f9d62c97421b8e4"
  private_subnets                       = "subnet-004ad255c07c50231,subnet-0c13ee46ece68593e,subnet-0d53e17f2c3973a6a"
  private_cidr_blocks                   = ["10.205.0.0/24", "10.205.2.0/24", "10.205.1.0/24"]
  sg_id                                 = "sg-017d2040cc44d995d"
  cname_prefix                          = "pachinko-prod"
  elasticache_instance_type             = "cache.r5.xlarge"
  elasticache_replicas_per_node_group   = "1"
  elasticache_num_node_groups           = "8"
  autoscaling_role                      = "arn:aws:iam::702056039310:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_min_read_capacity              = 50
  dynamo_min_write_capacity             = 50
  dynamo_min_gsi_read_capacity          = 50
  dynamo_min_gsi_write_capacity         = 50
  dynamo_transactions_min_read_capacity = 80
  min_host_count                        = "3"
  sandstorm_arn                         = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/pachinko-production"
  account_id                            = "702056039310"
  cpu                                   = "2048"
  memory                                = "4096"
  ecs_ec2_backed_min_instances          = 5
  bits_updates_sns_subscribers          = ["458492755823", "021561903526"]
  sub_tokens_updates_sns_subscribers    = ["075758494382"]
  backup_region                         = "us-east-2"
  profile                               = "pachinko-prod"
  redshift_transactions_cluster         = "transactions-production-v2"

  // Legal recommends 30 days TTL for sensitive log groups
  audit_logs_retention_in_days    = 30
  s2s_auth_logs_retention_in_days = 30
}

terraform {
  backend "s3" {
    bucket  = "pachinko-terraform-prod"
    key     = "tfstate/commerce/pachinko/terraform/prod"
    region  = "us-west-2"
    profile = "pachinko-prod"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "pachinko-prod"
}
