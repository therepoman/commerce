module "pachinko" {
  source                                = "../modules/pachinko"
  account_id                            = "946879801923"
  aws_profile                           = "pachinko-devo"
  env                                   = "staging"
  env_full                              = "staging"
  vpc_id                                = "vpc-089fc3b0b26bcac4a"
  private_subnets                       = "subnet-076ded465f04275f8,subnet-096dbdb229bbffa70,subnet-0a27ebfa56bbecb4e"
  private_cidr_blocks                   = ["10.204.249.0/24", "10.204.250.0/24", "10.204.248.0/24"]
  sg_id                                 = "sg-04dd01e54c7ebc87d"
  cname_prefix                          = "pachinko-staging"
  elasticache_replicas_per_node_group   = "2"
  elasticache_num_node_groups           = "5"
  autoscaling_role                      = "arn:aws:iam::946879801923:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_min_read_capacity              = 50
  dynamo_min_write_capacity             = 50
  dynamo_min_gsi_read_capacity          = 5
  dynamo_min_gsi_write_capacity         = 5
  dynamo_transactions_min_read_capacity = 10
  min_host_count                        = "2"
  sandstorm_arn                         = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/pachinko-staging"
  cpu                                   = "1024"
  memory                                = "2048"
  ecs_ec2_backed_min_instances          = 3
  bits_updates_sns_subscribers          = ["721938063588", "021561903526"]
  sub_tokens_updates_sns_subscribers    = ["038123293457"]
  backup_region                         = "us-east-2"
  profile                               = "pachinko-devo"
  redshift_transactions_cluster         = "transactions-staging-v3"
}

terraform {
  backend "s3" {
    bucket  = "pachinko-terraform-devo"
    key     = "tfstate/commerce/pachinko/terraform/staging"
    region  = "us-west-2"
    profile = "pachinko-devo"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "pachinko-devo"
}
