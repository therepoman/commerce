package slice

func ToStringMap(sSlice []string) map[string]struct{} {
	m := map[string]struct{}{}

	for _, s := range sSlice {
		m[s] = struct{}{}
	}

	return m
}

func ToBoolMap(bSlice []bool) map[bool]struct{} {
	m := map[bool]struct{}{}

	for _, b := range bSlice {
		m[b] = struct{}{}
	}

	return m
}

func ToIntMap(iSlice []int) map[int]struct{} {
	m := map[int]struct{}{}

	for _, i := range iSlice {
		m[i] = struct{}{}
	}

	return m
}

func ToInt32Map(int32Slice []int32) map[int32]struct{} {
	m := map[int32]struct{}{}

	for _, i := range int32Slice {
		m[i] = struct{}{}
	}

	return m
}

func ToInt64Map(int64Slice []int64) map[int64]struct{} {
	m := map[int64]struct{}{}

	for _, i := range int64Slice {
		m[i] = struct{}{}
	}

	return m
}

func ToFloat32Map(float32Slice []float32) map[float32]struct{} {
	m := map[float32]struct{}{}

	for _, f := range float32Slice {
		m[f] = struct{}{}
	}

	return m
}

func ToFloat64Map(float64Slice []float64) map[float64]struct{} {
	m := map[float64]struct{}{}

	for _, f := range float64Slice {
		m[f] = struct{}{}
	}

	return m
}
