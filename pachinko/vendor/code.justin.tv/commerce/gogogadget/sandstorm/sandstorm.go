package sandstorm

import (
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

const (
	assumeRoleDuration     = 900 * time.Second
	assumeRoleExpiryWindow = 10 * time.Second
	tableName              = "sandstorm-production"
	keyID                  = "alias/sandstorm-production"
)

type Config struct {
	Role      string
	AWSRegion string
}

func NewSandstorm(cfg *Config) (SecretGetter, error) {
	if cfg != nil && !strings.Blank(cfg.Role) && !strings.Blank(cfg.AWSRegion) {
		sand, err := New(cfg.Role, cfg.AWSRegion)
		if err != nil {
			return nil, err
		}
		return sand, nil
	}
	return &NoOpSecretGetter{}, nil
}

var once = new(sync.Once)
var sandstormManager *manager.Manager
var secretCache map[string]*manager.Secret

func init() {
	secretCache = make(map[string]*manager.Secret)
}

type SecretGetter interface {
	Get(secretName string) (*manager.Secret, error)
}

type NoOpSecretGetter struct {
}

func (dsr *NoOpSecretGetter) Get(secretName string) (*manager.Secret, error) {
	return &manager.Secret{
		Plaintext: []byte{},
	}, nil
}

type CachedSecretGetter struct {
}

func (csg *CachedSecretGetter) Get(secretName string) (*manager.Secret, error) {
	cachedSecret, secretCached := secretCache[secretName]
	if secretCached {
		return cachedSecret, nil
	}

	secret, err := sandstormManager.Get(secretName)
	if err != nil {
		return nil, err
	}
	secretCache[secretName] = secret
	return secret, nil
}

func ensureSandstormManagerExists(role string, region string) error {
	awsConfig := &aws.Config{
		Region:              aws.String(region),
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	}

	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return err
	}

	once.Do(func() {
		if sandstormManager == nil {
			stsClient := sts.New(sess)

			arp := &stscreds.AssumeRoleProvider{
				Duration:     assumeRoleDuration,
				ExpiryWindow: assumeRoleExpiryWindow,
				RoleARN:      role,
				Client:       stsClient,
			}

			creds := credentials.NewCredentials(arp)
			awsConfig.WithCredentials(creds)

			config := manager.Config{
				AWSConfig: awsConfig,
				TableName: tableName,
				KeyID:     keyID,
			}

			sandstormManager = manager.New(config)
		}
	})
	return nil
}

func New(role string, region string) (SecretGetter, error) {
	err := ensureSandstormManagerExists(role, region)
	if err != nil {
		return nil, err
	}
	return &CachedSecretGetter{}, nil
}
