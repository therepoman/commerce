package math

import (
	"math/big"
	"unicode"

	"code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

func IntAbs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func Int64Abs(i int64) int64 {
	if i < 0 {
		return -i
	}
	return i
}

func NumericStringToBigInt(numString string) (*big.Int, error) {
	if len(numString) == 0 {
		msg := "Cannot convert empty string to bigInt"
		logrus.Error(msg)
		return nil, errors.New(msg)
	}

	for _, char := range numString {
		if !unicode.IsNumber(char) {
			msg := "String contained non-numeric rune"
			logrus.WithField("char", char).Error(msg)
			return nil, errors.New(msg)
		}
	}

	bigInt := big.Int{}
	_, ok := bigInt.SetString(numString, 10)
	if !ok {
		msg := "Failed converting string to bigInt"
		logrus.WithField("numString", numString).Error(msg)
		return nil, errors.New(msg)
	}
	return &bigInt, nil
}

func MaxInt(x int, y int) int {
	if x > y {
		return x
	}

	return y
}

func MaxInt64(x int64, y int64) int64 {
	if x > y {
		return x
	}

	return y
}

func MinInt(x int, y int) int {
	if x < y {
		return x
	}

	return y
}

func MinInt64(x int64, y int64) int64 {
	if x < y {
		return x
	}

	return y
}

func MaxBigFloat(x *big.Float, y *big.Float) *big.Float {
	if x.Cmp(y) > 0 {
		return x
	}

	return y
}

func MinBigFloat(x *big.Float, y *big.Float) *big.Float {
	if x.Cmp(y) < 0 {
		return x
	}

	return y
}

// Restricts the input value between a given minimum and maximum
func ClampBigFloat(in *big.Float, min *big.Float, max *big.Float) *big.Float {
	return MaxBigFloat(MinBigFloat(in, max), min)
}

func Uint64ToBigFloat(val uint64) *big.Float {
	bf := new(big.Float)
	return bf.SetUint64(val)
}

func Int64ToBigFloat(val int64) *big.Float {
	bf := new(big.Float)
	return bf.SetInt64(val)
}
