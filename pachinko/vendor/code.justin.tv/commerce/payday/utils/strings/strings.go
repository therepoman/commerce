package strings

import (
	"strconv"
	"strings"

	"code.justin.tv/commerce/payday/utils/math"
	"code.justin.tv/commerce/payday/utils/pointers"
)

func CommatizeInt(value int) string {
	tempValue := strconv.Itoa(value)
	humanizeTotal := []string{}
	count := 0
	for i := len(tempValue) - 1; i >= 0; i-- {
		if count == 3 {
			humanizeTotal = append(humanizeTotal, ",")
			count = 0
		}
		humanizeTotal = append(humanizeTotal, string(tempValue[i]))
		count++
	}

	return Reverse(strings.Join(humanizeTotal, ""))
}

type Set interface {
	Add(s string)
	GetList() []string
}

type set struct {
	l []string
	m map[string]interface{}
}

func (u *set) Add(s string) {
	if _, ok := u.m[s]; !ok {
		u.l = append(u.l, s)
		u.m[s] = nil
	}
}

func (u *set) GetList() []string {
	return u.l
}

func NewSet() *set {
	return &set{
		l: make([]string, 0),
		m: make(map[string]interface{}),
	}
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func BlankP(s *string) bool {
	return s == nil || strings.TrimSpace(*s) == ""
}

func Blank(s string) bool {
	return BlankP(pointers.StringP(s))
}

func NotBlankP(s *string) bool {
	return !BlankP(s)
}

func NotBlank(s string) bool {
	return !Blank(s)
}

func OneOf(s string, validOptions ...string) bool {
	for _, validOption := range validOptions {
		if validOption == s {
			return true
		}
	}
	return false
}

func Trucate(s string, maxLen int) string {
	return s[:math.MinInt(maxLen, len(s))]
}

func Contains(slice []string, itemToFind string) bool {
	for _, item := range slice {
		if item == itemToFind {
			return true
		}
	}
	return false
}
