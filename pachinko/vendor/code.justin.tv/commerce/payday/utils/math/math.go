package math

import (
	"math/big"
	"unicode"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
)

func IntAbs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func Int64Abs(i int64) int64 {
	if i < 0 {
		return -i
	}
	return i
}

func NumericStringToBigInt(numString string) (*big.Int, error) {
	if len(numString) == 0 {
		msg := "Cannot convert empty string to bigInt"
		log.Error(msg)
		return nil, errors.New(msg)
	}

	for _, char := range numString {
		if !unicode.IsNumber(char) {
			msg := "String contained non-numeric rune"
			log.WithField("char", char).Error(msg)
			return nil, errors.New(msg)
		}
	}

	bigInt := big.Int{}
	_, ok := bigInt.SetString(numString, 10)
	if !ok {
		msg := "Failed converting string to bigInt"
		log.WithField("numString", numString).Error(msg)
		return nil, errors.New(msg)
	}
	return &bigInt, nil
}

func MaxInt(x int, y int) int {
	if x > y {
		return x
	}
	return y
}

func MaxInt64(x int64, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

func MinInt(x int, y int) int {
	if x < y {
		return x
	}
	return y
}

func MinInt64(x int64, y int64) int64 {
	if x < y {
		return x
	}
	return y
}
