package ecsgomaxprocs

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"runtime"
	"sync"
	"time"
)

type logger interface {
	Log(keyvals ...interface{})
}

type httpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// Fixer can attempt to set GOMAXPROCS to match the ECS task's vCPU hard limit
type Fixer struct {
	// Log is optional. If nil, no logging happens
	Log logger
	// HTTPClient is optional. Defaults to http.DefaultClient
	HTTPClient httpClient

	numCPU     func() int          // default set to runtime.NumCPU
	getEnv     func(string) string // default set to os.Getenv
	gomaxprocs func(int) int       // default set to runtime.GOMAXPROCS

	setupOnce sync.Once
}

// Exec attempts to set GOMAXPROCS to match the ECS task's vCPU hard limit
func (f *Fixer) Exec() error {
	f.setup()

	envValue := f.getEnv("GOMAXPROCS")
	if envValue != "" {
		f.log("GOMAXPROCS environment variable is already set. Nothing to do")
		return nil
	}

	// Docs: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-metadata-endpoint-v3.html
	metadataURI := f.getEnv("ECS_CONTAINER_METADATA_URI")
	if metadataURI == "" {
		f.log("No ECS_CONTAINER_METADATA_URI environment variable is set. Nothing to do")
		return nil
	}

	taskURI := metadataURI + "/task"
	taskCPU, err := f.getTaskCPU(taskURI)
	if err != nil {
		return err
	}

	if taskCPU == noTaskCPU {
		f.log("No task CPU limit is set. Nothing to do")
		return nil
	}

	if math.Trunc(taskCPU) != taskCPU {
		f.log("taskCPU", taskCPU, "task cpu value is not a whole number. Rounding down")
		taskCPU = math.Max(1, math.Floor(taskCPU))
	}

	numCPU := f.numCPU()
	tCPU := int(taskCPU)
	// tCPU should never be greater than numCPU because that would mean the ECS service scheduler placed
	// a task with a higher vCPU requirement than possible on the host
	if numCPU != tCPU {
		f.log("runtime.NumCPU", numCPU, "task CPU", tCPU, "setting GOMAXPROCS to task CPU because there is a mismatch")
		f.gomaxprocs(tCPU)
	} else {
		f.log("runtime.NumCPU() is equal to task CPU. Nothing to do")
	}

	return nil
}

func (f *Fixer) setup() {
	f.setupOnce.Do(func() {
		if f.HTTPClient == nil {
			f.HTTPClient = http.DefaultClient
		}

		if f.getEnv == nil {
			f.getEnv = os.Getenv
		}

		if f.gomaxprocs == nil {
			f.gomaxprocs = runtime.GOMAXPROCS
		}

		if f.numCPU == nil {
			f.numCPU = runtime.NumCPU
		}
	})
}

func (f *Fixer) log(keyvals ...interface{}) {
	if f.Log != nil {
		f.Log.Log(keyvals...)
	}
}

const noTaskCPU = -1

// getTaskCPU returns the ECS task CPU limit as a floating point. A value of 1.0 is equivalent to 1024 CPU units.
// If there is no task CPU limit, then noTaskCPU is returned
func (f *Fixer) getTaskCPU(taskURI string) (float64, error) {
	b, err := f.httpGet(taskURI)
	if err != nil {
		return 0, err
	}

	var tr taskResponse
	if err := json.Unmarshal(b, &tr); err != nil {
		return 0, err
	}

	if tr.Limits == nil || tr.Limits.CPU == nil {
		return noTaskCPU, nil
	}

	return *tr.Limits.CPU, nil
}

func (f *Fixer) httpGet(uri string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("creating new HTTP request: %w", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("doing HTTP request: %w", err)
	}

	defer func() {
		if cerr := resp.Body.Close(); cerr != nil {
			f.log("err", cerr, "closing body")
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("got non-200 status code %v", resp.StatusCode)
	}

	// prevent unbounded read
	lr := &io.LimitedReader{
		R: resp.Body,
		N: 200 * 1024, // 200 KB
	}

	b, err := ioutil.ReadAll(lr)
	if err != nil {
		return nil, fmt.Errorf("reading response body: %w", err)
	}
	return b, nil
}

// taskResponse defines the schema for the task response JSON object
// Taken from https://github.com/aws/amazon-ecs-agent/blob/0e4174f6ca3154af6c88532345d584a50e09287f/agent/handlers/v2/response.go#L32
type taskResponse struct {
	Limits *limitsResponse `json:"Limits,omitempty"`
}

// limitsResponse defines the schema for task/cpu limits response JSON object
type limitsResponse struct {
	CPU *float64 `json:"CPU,omitempty"`
}
