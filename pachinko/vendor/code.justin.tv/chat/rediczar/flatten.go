package rediczar

// flattenArgs is used for migration compatibility from when a function changed from accepting
// a []interface{} to a ...interface{}.
func flattenArgs(args []interface{}) []interface{} {
	if len(args) == 0 {
		return args
	}

	if iargs, ok := args[0].([]interface{}); ok {
		return iargs
	}

	return args
}
