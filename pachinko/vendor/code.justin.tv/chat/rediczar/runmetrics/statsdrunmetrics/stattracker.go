package statsdrunmetrics

import (
	"time"
)

// StatSender sends stats
type StatSender interface {
	TimingDuration(string, time.Duration, float32) error
}

// Logger logs values
type Logger interface {
	Log(vals ...interface{})
}

// StatTracker tracks run stats
type StatTracker struct {
	// Stats sends metrics. If nil, no stats are sent
	Stats StatSender
	// SampleRate of stat sending. Defaults to 0.1
	SampleRate float32
	// Logger logs values. If nil, logging is off
	Logger Logger
}

// Success is called each time a command does not return an error, or if the error is redis.Nil
func (s *StatTracker) Success(name string, dur time.Duration) {
	s.timingDuration(name+".success", dur)
}

// Error is called each time a command returns an error, and if the error is not redis.Nil
func (s *StatTracker) Error(name string, dur time.Duration) {
	s.timingDuration(name+".error", dur)
}

func (s *StatTracker) timingDuration(stat string, val time.Duration) {
	if s.Stats == nil {
		return
	}

	if err := s.Stats.TimingDuration(stat, val, s.sampleRate()); err != nil {
		s.log("err", err, "timing run stat")
	}
}

func (s *StatTracker) sampleRate() float32 {
	if s.SampleRate <= 0 {
		return 0.1
	}

	return s.SampleRate
}

func (s *StatTracker) log(vals ...interface{}) {
	if s.Logger != nil {
		s.Logger.Log(vals...)
	}
}
