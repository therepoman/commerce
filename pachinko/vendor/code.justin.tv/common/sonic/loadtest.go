package sonic

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	tm "github.com/buger/goterm"
	"github.com/gosuri/uiprogress"
	"github.com/pkg/errors"
)

const (
	defaultWorkers = 10
)

type LoadTestConfig struct {
	// RPS is the request per second to serve. Default 10.
	RPS int

	// TotalRequests is the total number of requests to send. Default 100.
	TotalRequests uint64

	// LoadTestFn is the function to be load tested.
	LoadTestFn func() error

	// Whether to show the dashboard
	ShowDashboard bool
}

type loadTest struct {
	timings       []time.Duration
	timingChannel chan time.Duration
	timingMutex   *sync.Mutex

	errors       []error
	errorChannel chan error
	errorMutex   *sync.Mutex

	wg                       *sync.WaitGroup
	wgResultsProcessor       *sync.WaitGroup
	shutdown                 chan struct{}
	shutdownResultsProcessor chan struct{}
	running                  bool
	requestsSent             uint64
	ticker                   chan struct{}

	uiprogress *uiprogress.Progress
	bar        *uiprogress.Bar

	progressBox *tm.Box
	errorBox    *tm.Box
}

func (l *loadTest) recordTiming(t time.Duration) {
	l.timingChannel <- t
}

func (l *loadTest) recordError(err error) {
	l.errorChannel <- err
}

func (l *loadTest) processResults(cfg LoadTestConfig) {
	defer l.wgResultsProcessor.Done()
	for {
		select {
		case t := <-l.timingChannel:
			l.timingMutex.Lock()
			l.timings = append(l.timings, t)
			l.timingMutex.Unlock()
		case err := <-l.errorChannel:
			l.errorMutex.Lock()
			l.errors = append(l.errors, err)
			l.errorMutex.Unlock()
			if cfg.ShowDashboard {
				fmt.Fprintln(l.errorBox, fmt.Sprintf("%d: %s", len(l.errors), err.Error()))
			}
		case <-l.shutdownResultsProcessor:
			if len(l.timingChannel) > 0 || len(l.errorChannel) > 0 {
				continue
			}
			return
		}
	}
}

// RunLoadTest kicks off a load test at the request rps (requests per second) for a total
func RunLoadTest(ctx context.Context, cfg LoadTestConfig) (LoadTestResult, error) {
	if cfg.RPS == 0 {
		cfg.RPS = 10
	}
	if cfg.TotalRequests == 0 {
		cfg.TotalRequests = 100
	}
	if cfg.LoadTestFn == nil {
		return LoadTestResult{}, errors.New("must provide a LoadTestFn in config")
	}

	l := loadTest{
		timings:       make([]time.Duration, 0, cfg.TotalRequests),
		timingChannel: make(chan time.Duration, cfg.RPS),
		timingMutex:   &sync.Mutex{},

		errors:       make([]error, 0),
		errorChannel: make(chan error, cfg.RPS),
		errorMutex:   &sync.Mutex{},

		wg:                       &sync.WaitGroup{},
		wgResultsProcessor:       &sync.WaitGroup{},
		shutdown:                 make(chan struct{}),
		shutdownResultsProcessor: make(chan struct{}),
		running:                  false,
		ticker:                   make(chan struct{}),
	}

	l.running = true
	if cfg.ShowDashboard {
		l.setupCanvas(cfg)
	}

	go l.tick(ctx, cfg)
	if cfg.ShowDashboard {
		go l.drawLoop(ctx, cfg)
	}

	var i uint64
	for i = 0; i < defaultWorkers; i++ {
		l.spawnWorker(ctx, cfg)
	}

	l.wgResultsProcessor.Add(1)
	go l.processResults(cfg)

	l.wg.Wait()
	close(l.shutdownResultsProcessor)
	l.wgResultsProcessor.Wait()

	fmt.Print("\033[?25h")
	if cfg.ShowDashboard {
		tm.Clear()
	}

	return LoadTestResult{
		Timings: l.timings,
		Errors:  l.errors,
	}, ctx.Err()
}

func (l *loadTest) spawnWorker(ctx context.Context, cfg LoadTestConfig) {
	l.wg.Add(1)
	go func() {
		defer l.wg.Done()

		for {
			select {
			case <-l.ticker:
				if l.requestsSent >= cfg.TotalRequests {
					if l.running {
						l.running = false
						close(l.shutdown)
					}
					return
				}
				atomic.AddUint64(&l.requestsSent, 1)
				startTime := time.Now()
				if err := cfg.LoadTestFn(); err != nil {
					l.recordError(err)
				}
				l.recordTiming(time.Now().Sub(startTime))
				if cfg.ShowDashboard {
					l.bar.Incr()
				}
			case <-ctx.Done():
				if l.running {
					l.running = false
					close(l.shutdown)
				}
				return
			case <-l.shutdown:
				return
			}
		}
	}()
}

func (l *loadTest) tick(ctx context.Context, cfg LoadTestConfig) {
	tickRate := 1000 * time.Millisecond / time.Duration(cfg.RPS)
	ticker := time.NewTicker(tickRate)

	for {
		select {
		case <-ticker.C:
			select {
			case l.ticker <- struct{}{}:
				break
			default:
				l.spawnWorker(ctx, cfg)
			}
		case <-ctx.Done():
			if l.running {
				l.running = false
				close(l.shutdown)
			}
			return
		case <-l.shutdown:
			return
		}
	}
}

func (l *loadTest) setupCanvas(cfg LoadTestConfig) {
	fmt.Print("\033[?25l")
	l.progressBox = tm.NewBox(100|tm.PCT, 3, 0)
	l.errorBox = tm.NewBox(100|tm.PCT, 100|tm.PCT-25, 0)

	l.uiprogress = uiprogress.New()
	l.bar = l.uiprogress.AddBar(int(cfg.TotalRequests))
	l.bar.Width = l.progressBox.Width - 14 - l.progressBox.PaddingX*4
	l.bar.AppendCompleted()
	l.bar.PrependElapsed()
}

func (l *loadTest) drawLoop(ctx context.Context, cfg LoadTestConfig) {
	titleBox := tm.NewBox(100|tm.PCT, 3, 0)
	tm.Clear()

	ticker := time.NewTicker(100 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			lines := strings.Split(l.errorBox.Buf.String(), "\n")
			if len(lines) > l.errorBox.Height-1 {
				diff := len(lines) - (l.errorBox.Height - 1)
				lines = lines[diff:len(lines)]
				l.errorBox.Buf = bytes.NewBuffer([]byte(strings.Join(lines, "\n")))
			}

			tm.MoveCursor(1, 1)
			l.progressBox.Buf = bytes.NewBuffer(make([]byte, 0, 100))
			l.progressBox.Buf = bytes.NewBuffer([]byte(l.bar.String()))
			tm.Print(l.progressBox)

			tm.MoveCursor(1, 6)
			titleBox.Buf = bytes.NewBuffer([]byte(tm.Bold(fmt.Sprintf("%d Error(s), %d Request(s) Sent", len(l.errors), l.requestsSent))))
			tm.Println(titleBox)
			tm.Print(l.errorBox)
			tm.Flush()
		case <-ctx.Done():
			if l.running {
				l.running = false
				close(l.shutdown)
			}
			return
		case <-l.shutdown:
			return
		}
	}
}
