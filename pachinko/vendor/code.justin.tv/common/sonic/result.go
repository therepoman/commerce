package sonic

import (
	"fmt"
	"time"

	"github.com/montanaflynn/stats"
	"github.com/pkg/errors"
)

type LoadTestResult struct {
	Timings []time.Duration
	Errors  []error
}

func (r LoadTestResult) Percentile(percentile float64) (time.Duration, error) {
	floats := timingsToFloats(r.Timings)
	float, err := stats.Percentile(floats, percentile)
	if err != nil {
		return time.Duration(0), err
	}
	return time.Duration(float), nil
}

type ReportParams struct {
	HideErrors bool
}

func (r LoadTestResult) PrintReport(params ReportParams) {
	fmt.Println("========== Sonic Report ==========\n")

	if len(r.Timings) == 0 {
		fmt.Println("No timings reported")
	} else {
		p100, err := r.Percentile(99.9)
		if err != nil {
			fmt.Println(errors.Wrap(err, "getting p99.9"))
			return
		}
		p99, err := r.Percentile(99)
		if err != nil {
			fmt.Println(errors.Wrap(err, "getting p99"))
			return
		}
		p90, err := r.Percentile(90)
		if err != nil {
			fmt.Println(errors.Wrap(err, "getting p90"))
			return
		}
		p50, err := r.Percentile(50)
		if err != nil {
			fmt.Println(errors.Wrap(err, "getting p50"))
			return
		}
		fmt.Printf("p100: %v\n", p100)
		fmt.Printf("p99 : %v\n", p99)
		fmt.Printf("p90 : %v\n", p90)
		fmt.Printf("p50 : %v\n", p50)

		fmt.Printf("\nSucesses: %v\n", len(r.Timings)-len(r.Errors))
		fmt.Printf("Errors  : %v\n", len(r.Errors))
		if len(r.Errors) > 0 && !params.HideErrors {
			fmt.Println("Error report:")
			for _, err := range r.Errors {
				fmt.Printf("  - %v\n", err)
			}
		}
	}
	fmt.Println("\n==================================")
}

func timingsToFloats(ts []time.Duration) []float64 {
	floats := make([]float64, 0, len(ts))
	for _, t := range ts {
		floats = append(floats, float64(t))
	}
	return floats
}
