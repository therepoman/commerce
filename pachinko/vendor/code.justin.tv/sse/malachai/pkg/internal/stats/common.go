package stats

// Func is the method call expected in this package
type Func func() error
