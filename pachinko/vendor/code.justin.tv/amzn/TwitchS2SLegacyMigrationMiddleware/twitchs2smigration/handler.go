package twitchs2smigration

import "net/http"

// Handler is the handler this package returns
type Handler interface {
	http.Handler
}
