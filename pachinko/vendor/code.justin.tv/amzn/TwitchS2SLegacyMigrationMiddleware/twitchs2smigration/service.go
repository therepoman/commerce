package twitchs2smigration

// Service is the S2S version agnostic interface for a service identifier
type Service interface {
	// unique identifier for a service
	id() string
	// friendly name - only for informative purposes
	name() string
}

// MustService is a helper for Middleware.Service that panics if that returns
// error.
func MustService(s Service, err error) Service {
	if err != nil {
		panic(err)
	}
	return s
}

type service struct {
	ID   string
	Name string
}

func (s service) id() string   { return s.ID }
func (s service) name() string { return s.Name }

type distributedIdentityService struct {
	Service, Stage, ID string
}

func (s distributedIdentityService) id() string   { return s.ID }
func (s distributedIdentityService) name() string { return s.Service + "/" + s.Stage }
