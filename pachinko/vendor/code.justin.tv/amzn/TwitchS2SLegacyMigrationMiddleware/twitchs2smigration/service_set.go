package twitchs2smigration

func newServiceSet(services ...Service) serviceSet {
	set := make(serviceSet, 0)
	for _, s := range services {
		set[s.id()] = s
	}
	return set
}

type serviceSet map[string]Service

func (s serviceSet) Get(id string) (Service, bool) {
	svc, ok := s[id]
	return svc, ok
}
