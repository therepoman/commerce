package twitchs2smigration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	logging "code.justin.tv/amzn/TwitchLogging"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/amzn/TwitchS2S2/s2s2/s2s2iface"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
)

// Options for New
type Options struct {
	// rate limit at which S2Sv0RequestReceived logs will be rate limited at
	// default: 1s
	LogS2Sv0RequestReceivedRateLimit time.Duration
}

func (o *Options) merge(other *Options) *Options {
	if other.LogS2Sv0RequestReceivedRateLimit != 0 {
		o.LogS2Sv0RequestReceivedRateLimit = other.LogS2Sv0RequestReceivedRateLimit
	}
	return o
}

func (o Options) getLogS2Sv0RequestReceivedRateLimit() time.Duration {
	if o.LogS2Sv0RequestReceivedRateLimit == 0 {
		return time.Second
	}
	return o.LogS2Sv0RequestReceivedRateLimit
}

// New returns a new Middleware
func New(
	s2sv0 *callee.Client,
	s2s2 *s2s2.S2S2,
	l logging.Logger,
	options ...*Options,
) (*Middleware, error) {
	mergedOptions := new(Options)
	for _, other := range options {
		mergedOptions = mergedOptions.merge(other)
	}
	return &Middleware{
		s2sv0: s2sv0,
		s2s2:  s2s2,
		logger: &logger{
			Logger:                             l,
			LogS2Sv0RequestReceivedRateLimiter: time.NewTicker(mergedOptions.getLogS2Sv0RequestReceivedRateLimit()).C,
		},
	}, nil
}

// Middleware assists in migrating a service from S2Sv0 to S2S2
type Middleware struct {
	s2sv0  callee.ClientAPI
	s2s2   s2s2iface.S2S2API
	logger *logger
}

// Service returns a service to compare to RequestSubject
func (m *Middleware) Service(name string) (Service, error) {
	serviceID, err := m.s2sv0.ServiceID(name)
	if err != nil {
		return nil, err
	}
	return service{
		ID:   m.s2s2.ServiceURIFromID(serviceID),
		Name: name,
	}, nil
}

// DistributedIdentityService returns a service that represents a service that
// implements the TwitchS2S2 Distributed Identity specification.
func (m *Middleware) DistributedIdentityService(name, stage string) Service {
	return distributedIdentityService{
		Service: name,
		Stage:   stage,
		ID:      m.s2s2.DistributedIdentitiesServiceURI(name, stage),
	}
}

// RequireAuthenticationOptions for RequireAuthentication
type RequireAuthenticationOptions struct {
	AuthorizedCallers []Service
}

// RequireAuthentication returns a handler
func (m *Middleware) RequireAuthentication(h http.Handler, options *RequireAuthenticationOptions) Handler {
	return m.s2s2.RequireAuthentication(
		m.s2sv0.RequestValidatorPassthroughMiddleware(
			m.injectS2SAuthentication(h, newServiceSet(options.AuthorizedCallers...)),
		),
	).PassthroughIfAuthorizationNotPresented()
}

type jsonError struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func (m *Middleware) injectS2SAuthentication(h http.Handler, authorizedCallers serviceSet) Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var requestSubject Subject
		if s, ok := s2s2.OptionalRequestSubject(r.Context()); ok {
			requestSubject = subject(s.ID())
		} else if s := callee.GetCallerID(r.Context()); s != nil {
			if callee.IsUnidentified(s) {
				m.serveError(w, http.StatusUnauthorized, "unauthenticated", "no s2s authentication presented")
				return
			}
			requestSubject = s2sv0Subject{subject: subject(m.s2s2.ServiceURIFromID(s.Caller))}
		} else {
			m.serveError(w, http.StatusUnauthorized, "unauthenticated", "no s2s authentication presented")
			return
		}

		svc, ok := authorizedCallers.Get(requestSubject.id())
		if !ok {
			m.serveError(w, http.StatusForbidden, "permission_denied", fmt.Sprintf("caller not authorized: %v", requestSubject))
			return
		}

		if requestSubject.isS2Sv0() {
			m.logger.S2Sv0RequestReceived(svc)
		}

		h.ServeHTTP(w, r.WithContext(setRequestSubject(r.Context(), requestSubject)))
	})
}

func (m *Middleware) serveError(w http.ResponseWriter, httpCode int, code, msg string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	if err := json.NewEncoder(w).Encode(jsonError{Code: code, Msg: msg}); err != nil {
		http.Error(w, fmt.Sprintf("could not encode json error: %s", err.Error()), http.StatusInternalServerError)
	}
}
