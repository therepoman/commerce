package twitchs2smigration

// Subject is the S2S version agnostic interface for a request subject
type Subject interface {
	// unique identifier for a service
	id() string
	// returns if this subject is a specific service
	isService(Service) bool
	isS2Sv0() bool
}

type subject string

func (s subject) id() string                 { return string(s) }
func (s subject) isService(svc Service) bool { return s.id() == svc.id() }
func (s subject) isS2Sv0() bool              { return false }

type s2sv0Subject struct {
	subject
}

func (s2sv0Subject) isS2Sv0() bool { return true }
