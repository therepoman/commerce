package twitchs2smigration

import (
	"time"

	logging "code.justin.tv/amzn/TwitchLogging"
)

// logger logs all events emitted by this middleware
type logger struct {
	Logger                             logging.Logger
	LogS2Sv0RequestReceivedRateLimiter <-chan time.Time
}

func (l logger) S2Sv0RequestReceived(svc Service) {
	select {
	case <-l.LogS2Sv0RequestReceivedRateLimiter:
		l.Logger.Log(
			"LegacyS2Sv0CallerDetected",
			"ServiceName", svc.name(),
		)
	default:
	}
}
