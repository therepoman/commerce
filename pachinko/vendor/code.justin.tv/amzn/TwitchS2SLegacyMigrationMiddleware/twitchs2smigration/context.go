package twitchs2smigration

import "context"

type contextKey string

const requestSubjectContextKey = contextKey("requestSubject")

// RequestSubject returns the subject regardless of S2S or S2S2
func RequestSubject(ctx context.Context) Subject {
	if val, ok := ctx.Value(requestSubjectContextKey).(Subject); ok {
		return val
	}
	return nil
}

func setRequestSubject(ctx context.Context, requestSubject Subject) context.Context {
	return context.WithValue(ctx, requestSubjectContextKey, requestSubject)
}
