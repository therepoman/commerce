package httpwrap

import (
	"net/http"
)

// RoundTripperFromHTTPClient wraps HTTPClient to make it act like a
// RoundTripper
func RoundTripperFromHTTPClient(h HTTPClient) http.RoundTripper {
	return &roundTripper{HTTPClient: h}
}

// roundTripper wraps HTTPClient to make it look like a RoundTripper
type roundTripper struct {
	HTTPClient HTTPClient
}

func (rt *roundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	return rt.HTTPClient.Do(req)
}
