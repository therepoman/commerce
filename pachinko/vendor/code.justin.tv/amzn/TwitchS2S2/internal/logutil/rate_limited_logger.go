package logutil

import "time"

// RateLimitedLogger is a rate limited implementation of Logger
type RateLimitedLogger struct {
	Logger
	LogAnonymousRequestRateLimit <-chan time.Time
}

// LogAnonymousRequest is the rate limited version of LogAnonymousRequest
func (l *RateLimitedLogger) LogAnonymousRequest(clientIP, forwardedForIP, forwardedForProto, forwardedForPort string) {
	select {
	case <-l.LogAnonymousRequestRateLimit:
	default:
		return
	}
	l.Logger.LogAnonymousRequest(clientIP, forwardedForIP, forwardedForProto, forwardedForPort)
}
