package interfaces

import (
	"net/http"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/s2s2dicallee"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

// Handler is just for mocks.
type Handler interface {
	http.Handler
}

// RoundTripper is just for mocks.
type RoundTripper interface {
	http.RoundTripper
}

// HTTPClient the commonly used HTTPClient interface we are using.
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

// LambdaTransport is implemented by the aws lambda transport
type LambdaTransport interface {
	HTTPClient
	LambdaName() string
}

// CalleeAPI is the interface for *s2s2dicallee.Callee
type CalleeAPI interface {
	ValidateAuthentication(req *http.Request) (s2s2dicallee.AuthenticatedSubject, error)
}

// OpMonitor just for mocks
type OpMonitor interface {
	operation.OpMonitor
}
