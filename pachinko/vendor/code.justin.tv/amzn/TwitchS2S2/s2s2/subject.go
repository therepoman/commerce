// Package s2s2 contains s2s2 handlers
package s2s2

import (
	"fmt"

	"code.justin.tv/amzn/TwitchS2S2/c7s"
)

type Subject interface {
	// return id of configured subject to compare with AuthorizedSubject
	id() string
}

type service struct {
	// Service of the s2s2 service,
	// work with service owner or secdev team to collect id
	// of the  service you want to authorize
	// named ID becuase it has a field id()
	ID string

	Cfg *c7s.Config
}

// NewAuthorizedService return a new authorized service to be used during
// custom authorization
func (s *S2S2) NewAuthorizedService(serviceID string) Subject {
	return service{
		ID:  serviceID,
		Cfg: s.config,
	}
}
func (s service) id() string {
	return fmt.Sprintf("%s/%s", s.Cfg.ServiceByIDAuthorityURI, s.ID)
}

// diService represents a distributed Identities services
// to be used for custom authorization
type diService struct {
	Name  string
	Stage string
	Cfg   *c7s.Config
}

// NewAuthorizedDistributedIdentity return a new authorized subject
func (s *S2S2) NewAuthorizedDistributedIdentity(name, stage string) Subject {
	return diService{
		Name:  name,
		Stage: stage,
		Cfg:   s.config,
	}
}

// id returns "service-name/stage" as id
func (di diService) id() string {
	return distributedIdentityURI(di.Cfg.IdentityOrigin, di.Cfg.ServiceDomain, di.Name, di.Stage)
}
