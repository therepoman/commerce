package s2s2iface

import (
	"net/http"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
)

// S2S2API is the interface s2s2.S2S2 implements.
type S2S2API interface {
	CapabilityScope(string) string
	ServiceURIFromID(string) string
	DistributedIdentitiesServiceURI(_, _ string) string
	HTTPClient(inner s2s2.HTTPClient) s2s2.HTTPClient
	RoundTripper(inner s2s2.HTTPClient) s2s2.RoundTripper
	RequireAuthentication(http.Handler) s2s2.Handler
	RequireScopes(http.Handler, ...string) s2s2.Handler
}
