package s2s2

import (
	"errors"
	"net/http"
	"strings"
)

// Errors
var (
	errMissingAuthorizationHeader = errors.New("authorization header not provided")
	errInvalidAuthorizationHeader = errors.New("authorization header must have the format '<authorizationType> <token>'")
)

func parseAuthorizationHeader(r *http.Request) (*rawAuthorization, error) {
	if len(r.Header.Get("Authorization")) == 0 {
		return nil, errMissingAuthorizationHeader
	}

	parts := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(parts) != 2 {
		return nil, errInvalidAuthorizationHeader
	}

	return &rawAuthorization{TokenType: parts[0], Token: parts[1]}, nil
}

type rawAuthorization struct {
	TokenType string
	Token     string
}
