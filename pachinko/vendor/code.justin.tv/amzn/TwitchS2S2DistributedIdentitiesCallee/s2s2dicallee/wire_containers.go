package s2s2dicallee

import (
	"context"
	"net/http"
	"net/url"
	"time"

	logging "code.justin.tv/amzn/TwitchLogging"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/cachedvalidation"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/cert"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/logutil"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/logutil/logutiliface"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

// rate at which we will refresh certificates in the background
const refreshRateLimit = 10 * time.Minute

// rate at which we will refresh certificates in the foreground
const foregroundRefreshRateLimit = time.Minute

// Options are options for New
type Options struct {
	// REQUIRED CONFIGURATION
	// set of origins this service is authoritative for
	WebOrigins []string

	// REQUIRED CONFIGURATION
	// list of services authorized to call us
	AuthorizedServices []Service

	// RECOMMENDED CONFIGURATION
	// operation starter to use for metrics.
	OperationStarter *operation.Starter

	// RECOMMENDED CONFIGURATION
	// logger to log failures
	Logger logging.Logger

	// OPTIONAL CONFIGURATION
	// logging of who unknown callers are.
	// defaults to a log rate limit of 15 minutes.
	// set to a duration value greater than zero to have logs appear
	// every integer value times minute, ie. if value is set to
	// 2 * time.Minute, it will log every 2 minutes.
	// set to less than zero (ie. -1) to disable this logging.
	UnknownCallerLoggerRateLimit time.Duration

	// SHOULD NOT BE SET - default should be used for production.
	// The origin where we will be pull certificates
	CertificateStoreOrigin *url.URL

	// SHOULD NOT BE SET - defaults should be used for production.
	// The origin where we will be pull certificates
	ServicesDomain string
}

func newOperationStarter(
	options *Options,
) *operation.Starter {
	if options.OperationStarter == nil {
		return &operation.Starter{}
	}

	return options.OperationStarter
}

func newCallee(
	validations *cachedvalidation.CachedValidations,
	certificates *cert.Certificates,
	refreshRateLimiter *time.Ticker,
	logger logutiliface.LoggerAPI,
) *Callee {
	return &Callee{
		certificates:       certificates,
		refreshRateLimiter: refreshRateLimiter,
		validations:        validations,
		logger:             logger,
	}
}

func newValidations(
	options *Options,
	certificates *cert.Certificates,
	os *operation.Starter,
) (*validation.Validations, error) {
	serverOrigins, err := validation.NewOrigins(options.WebOrigins...)
	if err != nil {
		return nil, err
	}

	return &validation.Validations{
		Certificates:     certificates,
		ServerOrigins:    serverOrigins,
		OperationStarter: os,
	}, nil
}

func newCachedValidations(
	validations *validation.Validations,
) *cachedvalidation.CachedValidations {
	return cachedvalidation.New(validations, 1024)
}

func newCertificates(
	options *Options,
	httpClient *http.Client,
	servicesDomain servicesDomain,
	certificateStoreOrigin certificateStoreOrigin,
	os *operation.Starter,
	logger logging.Logger,
	UnknownCallerLoggerRateLimiter time.Duration,
) (*cert.Certificates, error) {

	if len(options.AuthorizedServices) < 1 {
		logger.Log("WARNING: no authorized services are configured  - no callers will be authenticated")
	}

	certs := cert.New(
		httpClient,
		url.URL(certificateStoreOrigin),
		string(servicesDomain),
		os,
		logger,
		foregroundRefreshRateLimit,
		UnknownCallerLoggerRateLimiter,
	)
	if err := certs.LoadRootCertificatePool(context.Background()); err != nil {
		return nil, err
	}

	for _, svc := range options.AuthorizedServices {
		if err := certs.WhitelistService(context.Background(), svc.Name, svc.Stage); err != nil {
			return nil, err
		}
		logger.Log("PulledServiceCertificates", "service", svc.Name, "stage", svc.Stage)
	}
	return certs, nil
}

func newHTTPClient() *http.Client {
	return &http.Client{}
}

type servicesDomain string

func newServicesDomain(
	options *Options,
) servicesDomain {
	if options.ServicesDomain == "" {
		return servicesDomain("twitch")
	}

	return servicesDomain(options.ServicesDomain)
}

type certificateStoreOrigin url.URL

func newCertificateStoreOrigin(
	options *Options,
) certificateStoreOrigin {
	if options.CertificateStoreOrigin == nil {
		return certificateStoreOrigin{
			Scheme: "https",
			Host:   "prod.s2s2identities.twitch.a2z.com",
		}
	}

	return certificateStoreOrigin(*options.CertificateStoreOrigin)
}

func newLogger(options *Options) logging.Logger {
	if options.Logger == nil {
		return logutil.NoopLogger
	}
	return options.Logger
}

func newLoggerAPI(logger logging.Logger) logutiliface.LoggerAPI {
	return &logutil.Logger{Logger: logger}
}

func newRefreshRateLimiter() *time.Ticker {
	return time.NewTicker(refreshRateLimit)
}

func newUnknownCallerLoggerRateLimit(options *Options) time.Duration {
	if options.UnknownCallerLoggerRateLimit != 0 {
		return options.UnknownCallerLoggerRateLimit
	}
	return 15 * time.Minute
}
