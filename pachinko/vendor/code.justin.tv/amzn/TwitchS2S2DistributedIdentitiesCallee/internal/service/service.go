package service

import (
	"fmt"
	"strings"
)

// Parse a service common name in a certificate
func Parse(in string) (*Service, error) {
	parts := strings.SplitN(in, ".", 3)
	if len(parts) != 3 {
		return nil, fmt.Errorf("invalid service common name from certificate: %s", in)
	}

	svc := new(Service)
	svc.Stage, svc.Name, svc.Domain = parts[0], parts[1], parts[2]
	for _, val := range []string{svc.Stage, svc.Name, svc.Domain} {
		if len(val) == 0 {
			return nil, fmt.Errorf("invalid service common name from certificate: %s", in)
		}
	}

	return svc, nil
}

// Service represents a service in the Twitch service tuple specification.
type Service struct {
	Domain string
	Name   string
	Stage  string
}
