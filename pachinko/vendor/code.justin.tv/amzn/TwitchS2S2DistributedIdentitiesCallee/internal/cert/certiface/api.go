package certiface

import (
	"context"
	"crypto/ecdsa"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/service"
)

// CertificatesAPI is what CertificatePool implements
type CertificatesAPI interface {
	WhitelistService(ctx context.Context, service, stage string) error
	Get(ctx context.Context, x5u string) (*service.Service, *ecdsa.PublicKey, error)
	Refresh(ctx context.Context) error
}
