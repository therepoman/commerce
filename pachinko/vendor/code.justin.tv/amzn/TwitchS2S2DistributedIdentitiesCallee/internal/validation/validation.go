package validation

import (
	"fmt"
	"time"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/service"
)

// Validation contains information after an authentication validation
type Validation struct {
	// Subject of the authentication extracted from the certificate.
	Subject    service.Service
	Expiration time.Time
	NotBefore  time.Time
}

// ValidateTimeClaims re-validates time based claims
func (v *Validation) ValidateTimeClaims() error {
	now := time.Now()

	if time.Time(v.NotBefore).After(now) {
		return &ClaimValidationError{
			Field:   "nbf",
			Message: fmt.Sprintf("%v before %v", now, v.NotBefore),
		}
	}

	if time.Time(v.Expiration).Before(now) {
		return &ClaimValidationError{
			Field:   "exp",
			Message: fmt.Sprintf("%v after %v", now, v.Expiration),
		}
	}

	return nil
}
