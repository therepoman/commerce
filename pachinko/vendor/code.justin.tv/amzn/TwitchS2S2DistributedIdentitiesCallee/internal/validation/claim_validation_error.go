package validation

import "fmt"

// ClaimValidationError is returned when there is an issue with a claim
type ClaimValidationError struct {
	Field   string
	Message string
}

func (err ClaimValidationError) Error() string {
	return fmt.Sprintf("error validating claim %s: %s", err.Field, err.Message)
}
