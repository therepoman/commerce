package validationiface

import (
	"context"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation"
)

// ValidationsAPI validates authentication
type ValidationsAPI interface {
	Validate(ctx context.Context, token []byte) (*validation.Validation, error)
}
