package logutiliface

import "net/http"

// LoggerAPI is the what logutil.Logger implements
type LoggerAPI interface {
	LogAnonymousRequest(*http.Request)
	LogUnknownError(error)
	LogUncachedX5U(string)
}
