/*

Package operation provides general middleware for marking and observing
client/server calls and other operations.

To act as a passive observer, implement the OpMonitor interface. The MonitorOp
method will be called at the start of each operation providing an opportunity
to decorate the call's Context. The returned callback struct is consulted
during the operation's lifecycle: the End function will be called with a
status report.

To instrument a client or server SDK, expect a Starter value. Call its StartOp
method at the start of each operation, and use the returned Op value to report
success or failure and to mark the operation's end.

Service owners will generally use pre-instrumented SDKs and pre-written
observation packages. To connect those together, create a Starter value that
lists the OpMonitors and pass it to the client/server SDKs.

*/
package operation
