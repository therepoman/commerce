// This file is based on github.com/showmax/go-fqdn/fqdn.go

// Copyright since 2015 Showmax s.r.o.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package autoprof

import (
	"context"
	"errors"
	"net"
	"os"
	"strings"
)

func fqdn() (string, error) {
	ctx := context.Background()

	res := &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			return nil, errors.New("fqdn lookup attempted to dial")
		},
	}

	hostname, err := os.Hostname()
	if err != nil {
		return "unknown", err
	}

	addrs, err := res.LookupIPAddr(ctx, hostname)
	if err != nil {
		return hostname, err
	}

	for _, addr := range addrs {
		ipv4 := addr.IP.To4()
		if ipv4 == nil {
			continue
		}
		ip, err := ipv4.MarshalText()
		if err != nil {
			return hostname, err
		}
		hosts, err := res.LookupAddr(ctx, string(ip))
		if err != nil {
			return hostname, err
		}
		if len(hosts) > 1 {
			return strings.TrimSuffix(hosts[0], "."), nil
		}
	}

	return hostname, errors.New("fqdn not available")
}
