# Pachinko
The Consumable Token Service

![Pachinko Machine](/assets/machine-big-boss.gif)

Check out the [Docs site](https://git.xarth.tv/pages/commerce/pachinko-docs/) for quality documentation content, provided by a Walrus.
