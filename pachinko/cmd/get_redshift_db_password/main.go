package main

import (
	"fmt"
	"os"

	"code.justin.tv/commerce/gogogadget/sandstorm"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/codegangsta/cli"
)

var environment string

func main() {
	app := cli.NewApp()
	app.Name = "Redshift DB Password Fetcher"
	app.Usage = "Gets the password for the given stage requested, so that you don't have to paste it somewhere."

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
	}

	app.Action = getReshiftPassword

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func getReshiftPassword(c *cli.Context) {
	logrus.Infof("Loading config: %s", c.String("e"))

	cfg, err := config.LoadConfig(config.GetOrDefaultEnvironment(c.String("e")))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
		panic(err)
	}

	logrus.Infof("Fetching DB password for %s", cfg.Environment.LongName)

	client, err := sandstorm.New(cfg.Auth.Sandstorm.RoleARN, cfg.Environment.AWSRegion)
	if err != nil {
		logrus.WithError(err).Panic("Could not load sandstorm client")
		panic(err)
	}

	secret, err := client.Get(fmt.Sprintf("commerce/pachinko/%s/redshift-password", cfg.Environment.LongName))
	if err != nil {
		logrus.WithError(err).Panic("Could not fetch sandstorm secret")
		panic(err)
	}

	if secret == nil {
		logrus.WithError(err).Panic("Could not find sandstorm secret")
		panic(err)
	}

	logrus.Infof("redshift password is: %s", secret.Plaintext)
}
