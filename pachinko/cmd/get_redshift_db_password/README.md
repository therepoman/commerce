### Redshift DB Password Fetcher

#### CLI Help Prompt
```java
NAME:
   Redshift DB Password Fetcher - Gets the password for the given stage requested, so that you don't have to paste it somewhere.

USAGE:
   main [global options] command [command options] [arguments...]

VERSION:
   0.0.0

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --environment value, -e value  Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset. (default: "staging")
   --help, -h                     show help
   --version, -v                  print the version
```
