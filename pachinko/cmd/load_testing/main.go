package main

import (
	"context"
	"net"
	"net/http"
	"os"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/common/sonic"
	"github.com/codegangsta/cli"
	uuid "github.com/satori/go.uuid"
)

const (
	defaultMaxTPS                    = 1
	defaultLoadTestDurationInSeconds = 600
	defaultNumberOfUsers             = 100
)

var environment string
var maxTPS int
var duration int
var numUsers int

func main() {
	app := cli.NewApp()
	app.Name = "Pachinko Load Tester"
	app.Usage = "Runs a load test on Pachinko."

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
		cli.IntFlag{
			Name:        "maxTPS, m",
			Value:       defaultMaxTPS,
			Usage:       "max TPS to run",
			Destination: &maxTPS,
		},
		cli.IntFlag{
			Name:        "duration, d",
			Value:       defaultLoadTestDurationInSeconds,
			Usage:       "load test duration in seconds",
			Destination: &duration,
		},
		cli.IntFlag{
			Name:        "users, u",
			Value:       defaultNumberOfUsers,
			Usage:       "number of users to randomly distribute over",
			Destination: &numUsers,
		},
	}

	app.Action = runLoadTest

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runLoadTest(c *cli.Context) {
	logrus.Infof("Loading config: %s", environment)

	cfg, err := config.LoadConfig(config.GetOrDefaultEnvironment(environment))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
		panic(err)
	}

	testTuids := make([]string, numUsers)
	for i := 0; i < numUsers; i++ {
		testTuids[i] = random.NumberString(16)
	}

	pachinkoClient := pachinko.NewPachinkoProtobufClient(cfg.Environment.Endpoint, &http.Client{
		Timeout: 1 * time.Second,
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			MaxConnsPerHost:       100,
			MaxIdleConns:          100,
			IdleConnTimeout:       10 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	})
	totalRequests := duration * maxTPS

	ctx, cancel := context.WithTimeout(context.Background(), time.Hour*10)
	defer cancel()

	result, err := sonic.RunLoadTest(ctx, sonic.LoadTestConfig{
		RPS:           maxTPS,
		TotalRequests: uint64(totalRequests),
		ShowDashboard: true,
		LoadTestFn: func() error {
			_, err := pachinkoClient.AddTokens(ctx, &pachinko.AddTokensReq{
				TransactionId: uuid.NewV4().String(),
				Token: &pachinko.Token{
					Domain:   pachinko.Domain_STICKER,
					OwnerId:  testTuids[random.Int(0, numUsers-1)],
					GroupId:  testTuids[random.Int(0, numUsers-1)],
					Quantity: 5,
				},
			})
			return err
		},
	})

	if err != nil {
		panic(err)
	}

	result.PrintReport(sonic.ReportParams{
		HideErrors: false,
	})
}
