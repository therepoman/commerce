#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile pachinko-devo | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "Setting file descriptors on ${i}..."
    TC=twitch-pachinko-devo ssh -t "jakgregg@${i}" "echo '*  soft   nofile 80000' | sudo tee /etc/security/limits.conf"
    TC=twitch-pachinko-devo ssh -t "jakgregg@${i}" "echo '*  hard   nofile 80000' | sudo tee -a /etc/security/limits.conf"
done