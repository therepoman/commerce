#!/bin/bash

outputDir=$1
loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile pachinko-devo | jq -cr .Reservations[].Instances[].PrivateIpAddress))

mkdir $outputDir

for i in "${loadTestIPs[@]}"
do
    echo "Getting results from to ${i}..."
    TC=twitch-pachinko-devo scp "jakgregg@${i}:test.out" "${outputDir}/${i}.txt"
done