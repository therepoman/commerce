#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile pachinko-devo | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "SCPing to ${i}..."
    TC=twitch-pachinko-devo scp add-tokens-loadtest.toml "jakgregg@${i}:."
    TC=twitch-pachinko-devo scp /go-workspace/src/code.justin.tv/commerce/nebbiolo/n "jakgregg@${i}:attack"
    TC=twitch-pachinko-devo scp run-load-test.sh "jakgregg@${i}:."
    TC=twitch-pachinko-devo ssh -t "jakgregg@${i}" nohup ./run-load-test.sh
done