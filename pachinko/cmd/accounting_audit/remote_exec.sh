input=$1.csv
output=$2
tenant=$3

outputfile="$output.json"
exe=accounting_audit
dir=$exe

function main() {
  load_config

  # build the linux compatible binary
  env GOOS=linux go build
  mkdir -p temp/etc/pachinko/config
  mv $exe temp/$exe
  cp $input temp/$input
  cp -R ../../config/data/* temp/etc/pachinko/config

  cmd="./$exe"
  if [ $environment != 'prod' ]; then
    jb='twitch-pachinko-devo'
  else
    jb='twitch-pachinko-prod'
    cmd="$cmd -e=prod"
  fi

  cmd="$cmd -tenant=$tenant -input=$input -output=$output"
  echo -------------------------------
  echo "To be executed: $cmd"
  echo -------------------------------
  read -p "Does the above look right? (y/n) " ans

  if [ $ans == 'y' ]; then
    TC=$jb ssh jumpbox "mkdir -p $dir"
    TC=$jb scp -rp temp/* "jumpbox:$dir"
    TC=$jb ssh jumpbox "cd $dir\
      && export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID\
      && export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY\
      && $cmd"
    TC=$jb scp "jumpbox:$dir/$outputfile" $outputfile
    TC=$jb ssh jumpbox "rm -rf $dir"
  fi

  rm -rf temp
}

function load_config() {
  . ./config.sh

  load_success=true
  if [ -z $environment ] ; then
    echo "Empty environment in config (staging/prod)"
    load_success=false
  elif [ -z $AWS_ACCESS_KEY_ID ]; then
    echo "Empty $AWS_ACCESS_KEY_ID in config"
    load_success=false
  elif [ -z $AWS_SECRET_ACCESS_KEY ]; then
    echo "Empty $AWS_SECRET_ACCESS_KEY in config"
    load_success=false
  fi

  if [ $load_success == false ]; then
    echo "Missing fields! Check your config!"
    exit
  fi
}

main