package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	pachinko_dynamo "code.justin.tv/commerce/pachinko/backend/dynamo"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/codegangsta/cli"
	"github.com/facebookgo/inject"
	"github.com/sirupsen/logrus"
)

var (
	environment  string
	tenantDomain string
	input        string
	output       string
)

func main() {
	app := cli.NewApp()
	app.Name = "Accounting Audit"
	app.Usage = "Get Bits Transactions"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod' and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "environment",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "tenant, t",
			Usage:       "Tenant identifier to use for running commands.",
			Destination: &tenantDomain,
			EnvVar:      config.DomainTenantEnvironmentVariable,
		},
		cli.StringFlag{
			Name:        "input, in, i",
			Usage:       "the csv file you are using, we'll assume there is a header row and use just 1st column",
			Destination: &input,
		},
		cli.StringFlag{
			Name:        "output, out, o",
			Usage:       "the name of file you'll be outputting to, this will be a json file",
			Destination: &output,
		},
	}

	app.Action = runProgram
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runProgram(c *cli.Context) {
	log.Infof("Loading file: %s", input)
	txIds := getTXFromCSV()

	log.Infof("[OPTIONS] Environment: %s", environment)
	ctx := context.Background()
	txDAO := GetDynamoTransactionsDAO()

	txs, err := txDAO.BatchGetTransactionRecords(ctx, txIds)
	if err != nil {
		log.WithError(err).Panic("Failed to get transactions!")
	}

	sort.Slice(txs, func(i, j int) bool {
		return txs[i].ID < txs[j].ID
	})

	saveToJSON(txs)
}

func GetDynamoTransactionsDAO() dynamo_tx.DAO {
	log.Info("Loading config file")
	env := config.GetOrDefaultEnvironment(environment)
	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}
	domain := cfg.Tenants.GetDomainByIdentifier(tenantDomain)
	if _, ok := pachinko.Domain_name[int32(domain)]; !ok {
		logrus.WithError(err).Panic("Invalid Domain passed to Lambda Environment")
	}
	cfg.TenantDomain = domain

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to start AWS Session")
	}

	stats, err := statsd.NewNoopClient()
	if err != nil {
		log.Panic("could not initialize statsd NoopClient")
	}

	txDAO := dynamo_tx.NewDAOWithClient(dynamo.DynamoBatchWriteClient(sess))

	deps := []*inject.Object{
		{Value: cfg},
		{Value: stats},
		{Value: metrics.NewStatter()},
		{Value: txDAO},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)
	if err != nil {
		log.Panic("could not initialize dependencies for Dynamo Transactions DAO")
	}

	err = graph.Populate()
	if err != nil {
		log.Panic("could not populate dependencies for Dynamo Transactions DAO")
	}

	return txDAO
}

func getTXFromCSV() []string {
	csvfile, err := os.Open(input)
	if err != nil {
		log.WithError(err).Panicf("Rekt while opening csv %s", input)
	}
	r := csv.NewReader(csvfile)
	var txIds []string

	headerSkipped := false
	for {
		record, err := r.Read()
		if !headerSkipped {
			headerSkipped = true
			continue
		}
		if err == io.EOF {
			break
		}
		if err != nil {
			log.WithError(err).Panicf("Rekt while parsing csv %s", input)
		}
		txId := strings.Trim(record[0], " ")
		txIds = append(txIds, txId)
		log.Info(txId)
	}
	return txIds
}

func saveToJSON(txs []pachinko_dynamo.Transaction) {
	file, err := json.MarshalIndent(txs, "", " ")
	if err != nil {
		log.WithError(err).Panic("Rekt while marshalling transactions to json")
	}
	outputName := fmt.Sprintf("%s.json", output)
	err = ioutil.WriteFile(outputName, file, 0644)
	if err != nil {
		log.WithError(err).Panic("Rekt while saving json file")
	}
	log.Infof("Finished, file saved to %s", outputName)
}
