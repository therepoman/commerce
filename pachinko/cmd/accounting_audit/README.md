# Accounting Audit Script

For SOX Compliance, Accounting needs to audit how we store data in Pachinko. Because we need to do this multiple times and we may have to do this in the future, I'm putting it in a script and documenting how it can be used.

There are 2 parts to this script:
- go script
- bash script

The go script is the executable, however due to Pachinko's VPC bubble, you have to run this in the jumpbox, which is what the bash script does.

## Running

You have to first set up a `config.sh` file with the following:
```
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

# expects prod/staging
export environment=
```
(The AWS credentials must be first setup in AWS console in the account that you're querying from.)

Then you just execute the script `bash remote_exec.sh <csv-name-w/o-filetype> <json-name-w/o-file-type> <tenant>`.

(e.g, given `testcsv.csv`, run `bash remote_exec.sh testcsv testjson bits`)

before it kicks off the execution it'll verify with you first that the command it'll execute in the jumpbox looks right

```
-------------------------------
To be executed: ./accounting_audit -tenant=bits -input=testcsv.csv -output=testjson
-------------------------------
Does the above look right? (y/n) 
```

If everything ran without errors, you'll find a `.json` file in the same directory (e.g, `testjson.json`) which you can give to accounting.
