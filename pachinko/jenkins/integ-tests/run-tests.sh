#!/bin/bash

if [ -z "$ENVIRONMENT" ]
then
    echo "$$ENVIRONMENT is required!"
    exit 1
fi

if [ -z "$AWS_PROFILE" ]
then
    echo "$$AWS_PROFILE is required!"
    exit 1
fi

docker run \
 -e AWS_ACCESS_KEY=$(aws configure get aws_access_key_id --profile $AWS_PROFILE) \
 -e AWS_SECRET_KEY=$(aws configure get aws_secret_access_key --profile $AWS_PROFILE) \
 -e ENVIRONMENT=$ENVIRONMENT \
 -t builder make go_integration_tests