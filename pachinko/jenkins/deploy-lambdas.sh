#!/bin/bash

set -e

if [ -z "$ENVIRONMENT" ]
then
    echo "$$ENVIRONMENT is required!"
    exit 1
fi

if [ -z "$AWS_PROFILE" ]
then
    echo "$$AWS_PROFILE is required!"
    exit 1
fi

if [ -z "$LAMBDA_PATH" ]
then
    echo "$$LAMBDA_PATH is required!"
    exit 1
fi

if [ -z "$LAMBDA_S3_BUCKET" ]
then
    echo "$$LAMBDA_S3_BUCKET is required!"
    exit 1
fi

if [ -z "$VERSION" ]
then
    echo "$$VERSION is required!"
    exit 1
fi

docker run -t \
    -e AWS_ACCESS_KEY=$(aws configure get aws_access_key_id --profile $AWS_PROFILE) \
    -e AWS_SECRET_KEY=$(aws configure get aws_secret_access_key --profile $AWS_PROFILE) \
    lambdas-cli deploy -e=$ENVIRONMENT -dir=$LAMBDA_PATH -bucketSuffix=$LAMBDA_S3_BUCKET -version=$VERSION