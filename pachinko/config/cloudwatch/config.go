package cloudwatch

type Config struct {
	BufferSize               int     `yaml:"buffer-size"`
	BatchSize                int     `yaml:"batch-size"`
	FlushIntervalMS          int     `yaml:"flush-interval-ms"`
	FlushCheckDelayMS        int     `yaml:"flush-check-delay-ms"`
	EmergencyFlushPercentage float64 `yaml:"emergency-flush-percentage"`
}
