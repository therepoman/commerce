package tenant

import (
	"strings"

	"code.justin.tv/commerce/logrus"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type Wrapper map[string]*Config

func (w Wrapper) All() []*Config {
	ret := make([]*Config, 0)
	for _, t := range w {
		ret = append(ret, t)
	}
	return ret
}

func (w Wrapper) Get(domain pachinko.Domain) *Config {
	conf, ok := w[strings.ToLower(domain.String())]
	if !ok {
		logrus.WithField("domain", domain.String()).Error("tried looking up domain that does not exist")
		return nil
	}

	return conf
}

func (w Wrapper) GetDomainByIdentifier(id string) pachinko.Domain {
	for tenant, conf := range w {
		if conf.ResourceIdentifier == id {
			return pachinko.Domain(pachinko.Domain_value[strings.ToUpper(tenant)])
		}
	}

	return pachinko.Domain(-1)
}

type StepFunctionConfig struct {
	ARN string `yaml:"arn"`
}

type SQSConfig struct {
	URL string `yaml:"url"`
}

type TTLConfig struct {
	TransactionTableDays int `yaml:"transaction-table-days"`
}

type Config struct {
	ResourceIdentifier                       string              `yaml:"resource-identifier"`
	URL                                      string              `yaml:"url"`
	BalanceUpdatesTopicARN                   string              `yaml:"balance-updates-topic-arn"`
	AsyncAddTokensTopicARN                   string              `yaml:"async-add-tokens-topic-arn"`
	S2SEnabled                               bool                `yaml:"s2s-enabled"`
	S2S2Enabled                              bool                `yaml:"s2s2-enabled"`
	S2S2DisablePassthrough                   bool                `yaml:"s2s2-disable-passthrough"`
	S2SCallers                               []string            `yaml:"s2s-callers"`
	S2SAuthLogGroupName                      string              `yaml:"s2s-auth-log-group-name"`
	OverrideAPIAllowed                       bool                `yaml:"override-api-allowed"`
	RedisCluster                             string              `yaml:"redis-cluster"`
	SkipRedshift                             bool                `yaml:"skip-redshift"`
	AuditLogGroupName                        string              `yaml:"audit-log-group-name"`
	DeleteBalanceByOwnerIdStepFunctionConfig *StepFunctionConfig `yaml:"delete-balance-by-owner-id-step-function"`
	DeleteBalanceByGroupIdStepFunctionConfig *StepFunctionConfig `yaml:"delete-balance-by-group-id-step-function"`
	DeleteBalanceByOwnerIdSQSConfig          *SQSConfig          `yaml:"delete-balance-by-owner-id-sqs"`
	DeleteBalanceByGroupIdSQSConfig          *SQSConfig          `yaml:"delete-balance-by-group-id-sqs"`
	TTL                                      *TTLConfig          `yaml:"ttl"`
}
