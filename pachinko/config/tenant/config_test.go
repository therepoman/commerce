package tenant

import (
	"testing"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestWrapper_GetDomainByIdentifier(t *testing.T) {
	Convey("given a tenant wrapper", t, func() {
		w := Wrapper{
			"sticker": &Config{
				ResourceIdentifier: "sticker",
			},
			"community_points": &Config{
				ResourceIdentifier: "cp",
			},
		}

		Convey("when there's a match", func() {
			domain := w.GetDomainByIdentifier("cp")

			So(domain, ShouldEqual, pachinko.Domain_COMMUNITY_POINTS)
		})

		Convey("when there's not a match", func() {
			domain := w.GetDomainByIdentifier("foobar")

			So(domain, ShouldEqual, pachinko.Domain(-1))
		})
	})
}
