package reporting

type Config struct {
	Enabled                    bool   `yaml:"enabled"`
	TransactionsDeliveryStream string `yaml:"transactions-delivery-stream"`
}
