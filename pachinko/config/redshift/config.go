package redshift

type Config struct {
	AWSSecretStoreARN string `yaml:"aws-secret-store-arn"`
}
