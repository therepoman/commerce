package config

import (
	"strings"
	"testing"

	"code.justin.tv/commerce/pachinko/config/auth"
	"code.justin.tv/commerce/pachinko/config/tenant"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLoadConfig(t *testing.T) {
	Convey("The unit test config is loadable", t, func() {
		cfg, err := LoadConfig(UnitTest)
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		So(cfg.Environment.Name, ShouldEqual, "unit-test")
		So(cfg.Tenants, ShouldNotBeNil)
		tenantConfig := cfg.Tenants.Get(pachinko.Domain_STICKER)
		So(tenantConfig, ShouldNotBeNil)
		So(tenantConfig.ResourceIdentifier, ShouldEqual, "sticker")
	})
}

func TestConfig_GetTenant(t *testing.T) {
	Convey("Given a config", t, func() {
		config := Config{}

		Convey("with nothing configured should return nil", func() {
			So(config.GetTenant(), ShouldBeNil)
		})

		Convey("with an empty tenant wrapper", func() {
			config.Tenants = &tenant.Wrapper{}
			So(config.GetTenant(), ShouldBeNil)
		})

		Convey("with a populated tenant wrapper", func() {
			config.Tenants = &tenant.Wrapper{
				strings.ToLower(pachinko.Domain_BITS.String()): &tenant.Config{
					ResourceIdentifier: "BITS IS BEST",
				},
			}
			So(config.GetTenant(), ShouldBeNil)

			Convey("with current tenant set", func() {
				config.TenantDomain = pachinko.Domain_BITS
				tenantConfig := config.GetTenant()
				So(tenantConfig, ShouldNotBeNil)
				So(tenantConfig.ResourceIdentifier, ShouldEqual, "BITS IS BEST")
			})
		})
	})
}

func TestConfig_GetS2SName(t *testing.T) {
	Convey("Given a config", t, func() {
		config := Config{
			TenantDomain: pachinko.Domain(66),
		}
		So(config.GetS2SName(), ShouldEqual, "")

		Convey("with configured service name", func() {
			config.Auth = &auth.Config{
				S2S: &auth.S2S{
					ServiceName: "pachinko-staging",
				},
			}
			So(config.GetS2SName(), ShouldEqual, "pachinko-staging-66")

			Convey("with domain too", func() {
				config.TenantDomain = pachinko.Domain_STICKER
				So(config.GetS2SName(), ShouldEqual, "pachinko-staging-sticker")
			})

			Convey("with a domain that has underscore", func() {
				config.TenantDomain = pachinko.Domain_SUB_TOKENS
				So(config.GetS2SName(), ShouldEqual, "pachinko-staging-sub-tokens")
			})
		})
	})
}
