package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config/auth"
	"code.justin.tv/commerce/pachinko/config/cloudwatch"
	"code.justin.tv/commerce/pachinko/config/environment"
	"code.justin.tv/commerce/pachinko/config/redis"
	"code.justin.tv/commerce/pachinko/config/redshift"
	"code.justin.tv/commerce/pachinko/config/reporting"
	"code.justin.tv/commerce/pachinko/config/tenant"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/go-yaml/yaml"
)

const (
	LocalConfigFilePath             = "/src/code.justin.tv/commerce/pachinko/config/data/%s.yaml"
	GlobalConfigFilePath            = "/etc/pachinko/config/%s.yaml"
	LambdaConfigFilePath            = "etc/pachinko/config/%s.yaml"
	EnvironmentEnvironmentVariable  = "ENVIRONMENT"
	DomainTenantEnvironmentVariable = "TENANT"
	CanaryEnvironmentVariable       = "CANARY"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")
	Local     = Environment("local")
	Staging   = Environment("staging")
	Prod      = Environment("prod")
	Default   = Local
)

type Config struct {
	// Local variable to store env type we have
	environment Environment

	// Local variable to store the tenant domain
	TenantDomain pachinko.Domain

	// If it is a canary instance, this is true
	IsCanary bool

	// The configuration of the environment, like name / region
	Environment *environment.Config `yaml:"environment"`

	// The configuration for things like sandstorm, S2S, etc.
	Auth *auth.Config `yaml:"auth"`

	// Configuration for cloudwatch metrics.
	CloudwatchMetrics *cloudwatch.Config `yaml:"cloudwatch"`

	// Configuration for setting up redis.
	Redis *redis.Config `yaml:"redis"`

	// The tenants that exist in the system.
	Tenants *tenant.Wrapper `yaml:"tenants"`

	// The configuration for managing reporting to redshift, etc.
	Reporting *reporting.Config `yaml:"reporting"`

	// The config for connecting to redshift
	Redshift *redshift.Config `yaml:"redshift"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func GetOrDefaultEnvironment(env string) Environment {
	if !isValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}

func (c *Config) GetEnvironment() Environment {
	return c.environment
}

func (c *Config) GetEnvironmentMetricName() string {
	if c.IsCanary {
		return c.Environment.Name + "-canary"
	}
	return c.Environment.Name
}

func isValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func (c *Config) GetTenant() *tenant.Config {
	if c.Tenants == nil {
		return nil
	}
	return c.Tenants.Get(c.TenantDomain)
}

// I wish this was in Auth, but it needs stuff from tenant and auth, so it goes at their shared root here.
func (c *Config) GetS2SName() string {
	if c.Auth == nil || c.Auth.S2S == nil {
		return ""
	}
	// so we convert to lower to match our existing naming schemes and we swap the underscores to dashes, because
	// S2S doesn't allow underscores in service names.
	return c.Auth.S2S.ServiceName + "-" + strings.ReplaceAll(strings.ToLower(c.TenantDomain.String()), "_", "-")
}

func LoadConfig(env Environment) (*Config, error) {
	if !isValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}

	cfg, err := loadConfig(filePath)
	if err != nil {
		return nil, err
	}

	cfg.environment = env

	return cfg, err
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	lambdaFname := fmt.Sprintf(LambdaConfigFilePath, baseFilename)
	if _, err := os.Stat(lambdaFname); !os.IsNotExist(err) {
		return lambdaFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}
