package redis

type Config struct {
	Endpoint     string `yaml:"endpoint"`
	LockEndpoint string `yaml:"lock-endpoint"`
}

type LocalClusterSlots struct {
	Start int      `yaml:"start"`
	End   int      `yaml:"end"`
	Nodes []string `yaml:"nodes"`
}
