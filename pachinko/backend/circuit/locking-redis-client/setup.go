package locking_redis_client

import (
	"code.justin.tv/commerce/pachinko/config"
	"github.com/go-redis/redis"
)

func New(cfg *config.Config) redis.Cmdable {
	lockingRedisClient := &redis.Client{}
	if cfg.Redis != nil {
		lockingRedisClient = redis.NewClient(&redis.Options{
			Addr: cfg.Redis.LockEndpoint,
		})
	}

	return lockingRedisClient
}
