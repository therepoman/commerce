package dynamo

import (
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

func DynamoClient(sess *session.Session) dynamodbiface.DynamoDBAPI {
	return dynamoClientWithTimeout(sess, 500*time.Millisecond)
}

func DynamoBatchWriteClient(sess *session.Session) dynamodbiface.DynamoDBAPI {
	return dynamoClientWithTimeout(sess, 3*time.Second)
}

func DynamoScanClient(sess *session.Session) dynamodbiface.DynamoDBAPI {
	return dynamoClientWithTimeout(sess, 10*time.Second)
}

func dynamoClientWithTimeout(sess *session.Session, timeout time.Duration) dynamodbiface.DynamoDBAPI {
	region := aws.String(endpoints.UsWest2RegionID)
	if sess.Config != nil && sess.Config.Region != nil {
		region = sess.Config.Region
	}

	dynamoClient := dynamodb.New(sess, &aws.Config{
		Region: region,
		Retryer: client.DefaultRetryer{
			NumMaxRetries: 3,
		},
		HTTPClient: &http.Client{
			Timeout: timeout,
			Transport: &http.Transport{
				MaxConnsPerHost:     1000,
				MaxIdleConnsPerHost: 1000,
				IdleConnTimeout:     90 * time.Second,
			},
		},
	})

	return dynamoClient
}
