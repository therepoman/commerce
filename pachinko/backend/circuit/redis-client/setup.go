package redis_client

import (
	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/chat/rediczar/runmetrics/statsdrunmetrics"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-redis/redis"
)

func New(cfg *config.Config, prefix string) rediczar.ThickClient {
	return newClient(cfg, prefix, true)
}

func NewConsistentTenantClient(cfg *config.Config, prefix string) rediczar.ThickClient {
	return newClient(cfg, prefix, false)
}

func newClient(cfg *config.Config, prefix string, readOnly bool) rediczar.ThickClient {
	stats := &statsd.NoopClient{}

	var client redis.Cmdable
	if cfg.GetTenant() != nil && strings.NotBlank(cfg.GetTenant().RedisCluster) {
		client = redefault.NewClusterClient(cfg.GetTenant().RedisCluster, &redefault.ClusterOpts{
			ReadOnly:     readOnly,
			MaxRedirects: 3,
		})
	} else {
		// Default to localhost
		addr := "localhost:6397"
		if cfg.Redis != nil {
			addr = cfg.Redis.Endpoint
		}

		client = redis.NewClient(&redis.Options{
			Addr: addr,
		})
	}

	return &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			Redis:     client,
			KeyPrefix: prefix,
			RunMetrics: &statsdrunmetrics.StatTracker{
				Stats:      stats.NewSubStatter(prefix),
				SampleRate: 1.0,
			},
		},
	}
}
