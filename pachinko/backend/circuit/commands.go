package circuit

import afex "github.com/afex/hystrix-go/hystrix"

const (
	// tokens cache
	GetTokenCacheCommand       = "get_token_cache"
	GetTokensBatchCacheCommand = "get_token_batch_cache"
	MultiGetTokensCacheCommand = "multi_get_tokens_cache"
	GetTokensAllCacheCommand   = "get_token_all_cache"
	SetTokenCacheCommand       = "set_token_cache"
	SetTokensBatchCacheCommand = "set_token_batch_cache"
	MultiSetTokensCacheCommand = "multi_set_token_cache"
	DelTokenCacheCommand       = "del_token_cache"
	ExpireTokenCacheCommand    = "expire_token_cache"

	// transactions cache
	GetTransactionCacheCommand     = "get_transaction_cache"
	GetTransactionsAllCacheCommand = "get_transaction_all_cache"
	SetTransactionCacheCommand     = "set_transaction_cache"
	DelTransactionCacheCommand     = "del_transaction_cache"
	ExpireTransactionCacheCommand  = "expire_transaction_cache"

	// transaction timestamp cache
	GetTransactionTimestampCacheCommand = "get_transaction_timestamp_cache"
	SetTransactionTimestampCacheCommand = "set_transaction_timestamp_cache"
	DelTransactionTimestampCacheCommand = "del_transaction_timestamp_cache"

	// tokens dynamo
	GetTokenDynamoCommand       = "get_token_dynamo"
	GetTokensDynamoCommand      = "get_tokens_dynamo"
	GetTokensBatchCommand       = "get_tokens_batch_dynamo"
	IncTokensCommand            = "inc_tokens_dynamo"
	DecTokensCommand            = "dec_tokens_dynamo"
	SetTokenBalanceCommand      = "set_tokens_dynamo"
	SetTokenStatusCommand       = "set_token_status_dynamo"
	BatchDeleteTokensCommand    = "batch_delete_tokens_dynamo"
	ScanTokensCommand           = "scan_tokens_dynamo"
	QueryTokensByGroupIdCommand = "query_tokens_by_groupId_dynamo"

	// transactions dynamo
	GetTransactionRecordCommand           = "get_transaction_record"
	BatchGetTransactionRecordsCommand     = "batch_get_transaction_records"
	CommitTransactionCommand              = "commit_transaction"
	WriteTransactionCommand               = "write_transaction"
	WriteNewTransactionCommand            = "write_new_transaction"
	WriteIfNotCommittedTransactionCommand = "write_if_not_committed_transaction"
	BatchDeleteTransactionsCommand        = "batch_delete_transactions_dynamo"
	BatchPutTransactionsCommand           = "batch_put_transactions_dynamo"
	ScanTransactionsCommand               = "scan_transactions_dynamo"

	// locks
	ObtainLockCommand  = "obtain_lock"
	ReleaseLockCommand = "release_lock"

	//Timeouts
	TestTimeout = 30000

	// Concurrency
	DefaultMaxConcurrent = 600
)

func init() {
	configureTimeout(GetTokenCacheCommand, 300)
	configureTimeout(GetTokensBatchCacheCommand, 300)
	configureTimeout(MultiGetTokensCacheCommand, 300)
	configureTimeout(GetTokensAllCacheCommand, 300)
	configureTimeout(SetTokenCacheCommand, 300)
	configureTimeout(MultiSetTokensCacheCommand, 300)
	configureTimeout(SetTokensBatchCacheCommand, 300)
	configureTimeout(DelTokenCacheCommand, 300)
	configureTimeout(ExpireTokenCacheCommand, 300)

	configureTimeout(GetTransactionCacheCommand, 300)
	configureTimeout(GetTransactionsAllCacheCommand, 300)
	configureTimeout(SetTransactionCacheCommand, 300)
	configureTimeout(DelTransactionCacheCommand, 300)
	configureTimeout(ExpireTransactionCacheCommand, 300)

	configureTimeout(GetTransactionTimestampCacheCommand, 300)
	configureTimeout(SetTransactionTimestampCacheCommand, 300)
	configureTimeout(DelTransactionTimestampCacheCommand, 300)

	configureTimeout(GetTokenDynamoCommand, 500)
	configureTimeout(GetTokensDynamoCommand, 500)
	configureTimeout(GetTokensBatchCommand, 500)
	configureTimeout(IncTokensCommand, 500)
	configureTimeout(DecTokensCommand, 500)
	configureTimeout(SetTokenBalanceCommand, 500)
	configureTimeout(ScanTokensCommand, 10000)
	configureTimeout(BatchDeleteTokensCommand, 3000)

	configureTimeout(GetTransactionRecordCommand, 500)
	configureTimeout(BatchGetTransactionRecordsCommand, 500)
	configureTimeout(CommitTransactionCommand, 500)
	configureTimeout(WriteNewTransactionCommand, 500)
	configureTimeout(WriteIfNotCommittedTransactionCommand, 500)
	configureTimeout(WriteTransactionCommand, 500)
	configureTimeout(ScanTransactionsCommand, 10000)
	configureTimeout(BatchDeleteTransactionsCommand, 3000)
	configureTimeout(BatchPutTransactionsCommand, 3000)

	configureTimeout(ObtainLockCommand, 300)
	configureTimeout(ReleaseLockCommand, 300)
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func ConfigureTimeoutOverride(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func configure(cmd string, timeout int, maxConcurrent int) {
	afex.ConfigureCommand(cmd, afex.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: maxConcurrent,
		ErrorPercentThreshold: afex.DefaultErrorPercentThreshold,
	})
}
