package audit

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

const testAPI = "some-api"

type testBodyStruct struct {
	FieldA   string
	FieldB   int
	FieldC   []string
	FieldD   *string
	NilField *string
}

func TestCreateAccessLogMsg(t *testing.T) {
	Convey("Given a context", t, func() {
		Convey("with relevant fields set up in an auditEvent", func() {
			testTenant := "Bits"
			testURL := &url.URL{
				Path: "/some-path",
			}
			testMethod := "POST"
			testHeader := http.Header{}
			testHeader.Add("header1", "value1")
			testHeader.Add("header2", "value2")

			testRemoteAddr := "some-remote-addr"

			ctx := context.Background()
			ctx = context.WithValue(ctx, &auditKey, auditEvent{
				Method:     testMethod,
				Tenant:     testTenant,
				Url:        testURL.String(),
				RemoteAddr: testRemoteAddr,
				Header:     testHeader,
			})

			testBody := testBodyStruct{
				FieldA: "a",
				FieldB: 1,
				FieldC: []string{
					"a", "b", "c",
				},
				FieldD: pointers.StringP("some-string"),
			}

			Convey("Created message should not be blank and can be unmarshalled into auditEvent struct", func() {
				msg, err := CreateAccessLogMsg(ctx, testAPI, testBody)
				So(err, ShouldBeNil)
				log.WithField("msg", msg).Info("Generated msg")
				So(msg, ShouldNotBeBlank)

				var testAuditEvent auditEvent
				bytes := []byte(msg)
				err = json.Unmarshal(bytes, &testAuditEvent)
				So(err, ShouldBeNil)
				So(testAuditEvent.Api, ShouldEqual, testAPI)
				So(testAuditEvent.Method, ShouldEqual, testMethod)
				So(testAuditEvent.Tenant, ShouldEqual, testTenant)
				So(testAuditEvent.Url, ShouldEqual, testURL.String())
				So(testAuditEvent.RemoteAddr, ShouldEqual, testRemoteAddr)
				So(testAuditEvent.Header, ShouldResemble, testHeader)
				So(testAuditEvent.Body, ShouldNotBeEmpty)
				So(testAuditEvent.Timestamp, ShouldNotBeEmpty)
			})
		})
	})
}
