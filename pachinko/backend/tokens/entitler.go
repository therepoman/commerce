package tokens

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	cache "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	"code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
)

type Entitler interface {
	Entitle(ctx context.Context, transactionID string, token pachinko.Token) (*pachinko.Token, error)
}

func NewEntitler() Entitler {
	return &entitler{}
}

type entitler struct {
	TransactionsDAO           transactions.DAO            `inject:""`
	TokensDAO                 tokens.DAO                  `inject:""`
	Cache                     cache.Cache                 `inject:""`
	TransactionTimestampCache transaction_timestamp.Cache `inject:""`
}

func (a *entitler) Entitle(ctx context.Context, transactionID string, token pachinko.Token) (*pachinko.Token, error) {
	ddbToken := dynamo.ToDynamoToken(token)
	ddbToken.LatestTransactionID = transactionID

	transactionTimestamp := a.TransactionTimestampCache.Get(ctx, transactionID)
	if transactionTimestamp == nil {
		now := time.Now()
		transactionTimestamp = &now
		err := a.TransactionTimestampCache.Set(ctx, transactionID, now)
		if err != nil {
			return nil, errors.Wrap(err, "could not set transaction timestamp in cache")
		}
	}

	transaction := dynamo.Transaction{
		ID:          transactionID,
		OwnerID:     token.OwnerId,
		Operation:   dynamo.AddTokensOperation,
		Timestamp:   *transactionTimestamp,
		Tokens:      dynamo.ToTHTokens([]*pachinko.Token{&token}),
		IsCommitted: true,
	}

	put, err := a.TransactionsDAO.GetWriteTransactionTx(transaction)
	if err != nil {
		return nil, err
	}

	resp, err := a.TokensDAO.IncrementTokenBalance(ctx, ddbToken, []dynamo.Put{put})
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"ownerID": token.OwnerId,
			"groupID": token.GroupId,
			"balance": token.Quantity,
		}).Error("could not entitle user tokens")
		return nil, err
	}

	a.Cache.Del(ctx, token.OwnerId, token.GroupId)

	// if the response is nil, we should return a default response.
	if resp == nil {
		return &pachinko.Token{
			Domain:   token.Domain,
			OwnerId:  token.OwnerId,
			GroupId:  token.GroupId,
			Quantity: token.Quantity,
		}, nil
	}
	newBalance := resp.ToTwirpToken(token.Domain)

	return &newBalance, nil
}
