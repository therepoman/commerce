package tokens

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	cache "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type StatusUpdater interface {
	UpdateStatus(ctx context.Context, transactionID string, token pachinko.Token) (*pachinko.Token, error)
}

func NewStatusUpdater() StatusUpdater {
	return &statusUpdater{}
}

type statusUpdater struct {
	TransactionsDAO transactions.DAO `inject:""`
	TokensDAO       tokens.DAO       `inject:""`
	Cache           cache.Cache      `inject:""`
}

func (s *statusUpdater) UpdateStatus(ctx context.Context, transactionID string, token pachinko.Token) (*pachinko.Token, error) {
	token.Quantity = 0
	ddbToken := dynamo.ToDynamoToken(token)
	ddbToken.LatestTransactionID = transactionID

	transaction := dynamo.Transaction{
		ID:          transactionID,
		OwnerID:     token.OwnerId,
		Operation:   dynamo.UpdateStatusOperation,
		Timestamp:   time.Now(),
		Tokens:      []*dynamo.THToken{dynamo.ToTHToken(&token)},
		IsCommitted: true,
	}

	put, err := s.TransactionsDAO.GetWriteTransactionTx(transaction)
	if err != nil {
		return nil, err
	}

	updatedDynamoToken, err := s.TokensDAO.SetTokenStatus(ctx, ddbToken, []dynamo.Put{put})
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"ownerID": token.OwnerId,
			"groupID": token.GroupId,
			"balance": token.Quantity,
		}).Error("could not set token status")
		return nil, err
	}

	s.Cache.Del(ctx, token.OwnerId, token.GroupId)

	// if the response is nil, we should return a default response.
	if updatedDynamoToken == nil {
		return &pachinko.Token{
			Domain:   token.Domain,
			OwnerId:  token.OwnerId,
			GroupId:  token.GroupId,
			Quantity: token.Quantity,
		}, nil
	}
	newToken := updatedDynamoToken.ToTwirpToken(token.Domain)

	return &newToken, nil
}
