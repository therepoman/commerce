package tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	db "code.justin.tv/commerce/pachinko/backend/dynamo"
	cache_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/tokens"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConsumer_Consume(t *testing.T) {
	Convey("given a token consumer", t, func() {
		dao := new(tokens_mock.DAO)
		cache := new(cache_mock.Cache)

		consumer := &consumer{
			DAO:   dao,
			Cache: cache,
		}

		ctx := context.Background()
		transactionID := random.String(32)
		userID := "123123123"
		channelID := "321321321"
		quantity := int64(2)
		token := pachinko.Token{
			OwnerId:  userID,
			GroupId:  channelID,
			Domain:   pachinko.Domain_STICKER,
			Quantity: quantity,
		}

		dynamoToken := db.ToDynamoToken(token)
		dynamoToken.LatestTransactionID = transactionID

		cache.On("Del", ctx, userID, channelID).Return()

		Convey("when the DAO call fails", func() {
			dao.On("DecrementTokenBalances", ctx, []db.Token{dynamoToken}, []db.Update{}).Return(errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				err := consumer.Consume(ctx, transactionID, []*pachinko.Token{&token}, []db.Update{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the DAO call succeeds and consumes the balance", func() {
			dao.On("DecrementTokenBalances", ctx, []db.Token{dynamoToken}, []db.Update{}).Return(nil)

			Convey("then we return nil", func() {
				err := consumer.Consume(ctx, transactionID, []*pachinko.Token{&token}, []db.Update{})
				So(err, ShouldBeNil)
			})
		})

	})
}
