package tokens

import (
	"context"

	"code.justin.tv/commerce/logrus"
	cache "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	db "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
)

type Consumer interface {
	Consume(ctx context.Context, transactionID string, tokens []*pachinko.Token, additionalTransactions []db.Update) error
}

func NewConsumer() Consumer {
	return &consumer{}
}

type consumer struct {
	DAO   tokens.DAO  `inject:""`
	Cache cache.Cache `inject:""`
}

func (c *consumer) Consume(ctx context.Context, transactionID string, requestTokens []*pachinko.Token, additionalTransactions []db.Update) error {
	tokensToConsume := make([]db.Token, 0)
	var ownerID string
	groupIDs := make([]string, 0)
	for _, token := range requestTokens {
		if token != nil {
			tokenToConsume := db.ToDynamoToken(*token)

			if ownerID == "" {
				ownerID = tokenToConsume.OwnerID
			} else if ownerID != tokenToConsume.OwnerID {
				return errors.New("cannot consume multiple ownerIDs of tokens")
			}

			groupIDs = append(groupIDs, tokenToConsume.GroupID)
			tokenToConsume.LatestTransactionID = transactionID
			tokensToConsume = append(tokensToConsume, tokenToConsume)
		}
	}
	err := c.DAO.DecrementTokenBalances(ctx, tokensToConsume, additionalTransactions)

	if err != nil {
		if awsError, ok := err.(awserr.Error); ok {
			// If the item was updated with lower quantity, don't fail
			if awsError.Code() != dynamodb.ErrCodeTransactionCanceledException {
				logrus.WithError(err).Error("could not consume tokens")
			}
		}
	}

	for _, groupID := range groupIDs {
		c.Cache.Del(ctx, ownerID, groupID)
	}

	return err
}
