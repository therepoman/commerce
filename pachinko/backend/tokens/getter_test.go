package tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_GetAll(t *testing.T) {
	Convey("given a token getter", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		fetcher := new(tokens_mock.Fetcher)

		getter := &getter{
			Config:  c,
			Fetcher: fetcher,
		}

		ctx := context.Background()
		ownerID := random.String(16)

		Convey("when the fetcher fails", func() {
			fetcher.On("FetchAll", ctx, ownerID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				_, err := getter.GetAll(ctx, ownerID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the fetcher succeeds", func() {
			groupID1 := random.String(16)
			groupID2 := random.String(16)
			fetcherResult := map[string]*pachinko.Token{
				groupID1: {
					OwnerId:  ownerID,
					GroupId:  groupID1,
					Quantity: 3,
					Domain:   pachinko.Domain_STICKER,
				},
				groupID2: {
					OwnerId:  ownerID,
					GroupId:  groupID2,
					Quantity: 4,
					Domain:   pachinko.Domain_STICKER,
				},
			}

			fetcher.On("FetchAll", ctx, ownerID).Return(fetcherResult, nil)

			Convey("then we should return all the tokens for the domain", func() {
				resp, err := getter.GetAll(ctx, ownerID)
				So(err, ShouldBeNil)
				So(resp, ShouldHaveLength, 2)
			})
		})
	})
}

func TestGetter_Get(t *testing.T) {
	Convey("given a token getter", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		fetcher := new(tokens_mock.Fetcher)

		getter := &getter{
			Config:  c,
			Fetcher: fetcher,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER

		Convey("when the fetcher fails", func() {
			fetcher.On("Fetch", ctx, ownerID, groupID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				_, err := getter.Get(ctx, ownerID, groupID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the fetcher succeeds", func() {
			fetcherResult := &pachinko.Token{
				OwnerId:  ownerID,
				GroupId:  groupID,
				Quantity: 3,
				Domain:   domain,
			}

			Convey("when the groupID requested is not in the list of returned balances", func() {
				fetcher.On("Fetch", ctx, ownerID, groupID).Return(nil, nil)

				resp, err := getter.Get(ctx, ownerID, groupID)
				So(err, ShouldBeNil)
				So(resp.OwnerId, ShouldEqual, ownerID)
				So(resp.GroupId, ShouldEqual, groupID)
				So(resp.Quantity, ShouldEqual, int64(0))
			})

			Convey("then we should return all the tokens for the domain", func() {
				fetcher.On("Fetch", ctx, ownerID, groupID).Return(fetcherResult, nil)

				resp, err := getter.Get(ctx, ownerID, groupID)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.GroupId, ShouldEqual, groupID)
			})
		})
	})
}

func TestGetter_GetFiltered(t *testing.T) {
	Convey("given a token getter", t, func() {
		fetcher := new(tokens_mock.Fetcher)
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}

		getter := &getter{
			Config:  c,
			Fetcher: fetcher,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupIDs := []string{
			random.String(16),
			random.String(16),
		}

		Convey("when the fetcher fails", func() {
			fetcher.On("FetchBatchForOwner", ctx, ownerID, groupIDs).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				_, err := getter.GetFiltered(ctx, ownerID, groupIDs)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the fetcher succeeds", func() {
			fetcherResult := map[string]*pachinko.Token{
				groupIDs[0]: {
					OwnerId:  ownerID,
					GroupId:  groupIDs[0],
					Quantity: 3,
					Domain:   pachinko.Domain_STICKER,
				},
				groupIDs[1]: {
					OwnerId:  ownerID,
					GroupId:  groupIDs[1],
					Quantity: 4,
					Domain:   pachinko.Domain_STICKER,
				},
			}

			fetcher.On("FetchBatchForOwner", ctx, ownerID, groupIDs).Return(fetcherResult, nil)

			Convey("then we should return all the tokens for the domain", func() {
				resp, err := getter.GetFiltered(ctx, ownerID, groupIDs)
				So(err, ShouldBeNil)
				So(resp, ShouldHaveLength, 2)
			})
		})
	})
}

func TestGetter_GetFilteredByBatch(t *testing.T) {
	Convey("given a token getter", t, func() {
		fetcher := new(tokens_mock.Fetcher)
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}

		getter := &getter{
			Config:  c,
			Fetcher: fetcher,
		}

		ctx := context.Background()
		ownerID1 := random.String(16)
		ownerID2 := random.String(16)
		groupID1 := random.String(16)
		groupID2 := random.String(16)

		keys := []dynamo.Keyed{
			dynamo.Keys{ownerID1, groupID1},
			dynamo.Keys{ownerID2, groupID2},
		}

		Convey("when the fetcher fails", func() {
			fetcher.On("FetchBatch", ctx, keys).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				_, err := getter.GetFilteredByBatch(ctx, keys)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the fetcher succeeds", func() {
			Convey("when fetcher finds all tokens requested", func() {
				fetcherResult := []*pachinko.Token{
					{
						OwnerId:  ownerID1,
						GroupId:  groupID1,
						Quantity: 3,
						Domain:   pachinko.Domain_STICKER,
					},
					{
						OwnerId:  ownerID2,
						GroupId:  groupID2,
						Quantity: 4,
						Domain:   pachinko.Domain_STICKER,
					},
				}

				fetcher.On("FetchBatch", ctx, keys).Return(fetcherResult, nil)

				Convey("then we should return all the tokens for the domain", func() {
					resp, err := getter.GetFilteredByBatch(ctx, keys)
					So(err, ShouldBeNil)
					So(resp, ShouldHaveLength, 2)
				})
			})

			Convey("when fetcher doesn't find all tokens requested", func() {
				fetcherResult := []*pachinko.Token{
					{
						OwnerId:  ownerID1,
						GroupId:  groupID1,
						Quantity: 3,
						Domain:   pachinko.Domain_STICKER,
					},
				}

				fetcher.On("FetchBatch", ctx, keys).Return(fetcherResult, nil)

				Convey("then we should return all the tokens for the domain", func() {
					resp, err := getter.GetFilteredByBatch(ctx, keys)
					So(err, ShouldBeNil)
					So(resp, ShouldHaveLength, 2)
					So(resp, ShouldResemble, append(fetcherResult, &pachinko.Token{
						Domain:   pachinko.Domain_STICKER,
						OwnerId:  ownerID2,
						GroupId:  groupID2,
						Quantity: 0,
					}))
				})
			})
		})
	})
}
