package tokens

import (
	"code.justin.tv/commerce/gogogadget/random"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

func MakeTokenList(ownerID string, domain pachinko.Domain, size int) []*pachinko.Token {
	tokenList := make([]*pachinko.Token, size)
	for i := range tokenList {
		tokenList[i] = &pachinko.Token{
			OwnerId:  ownerID,
			GroupId:  random.String(16),
			Quantity: random.Int64(1, 17),
			Domain:   domain,
		}
	}

	return tokenList
}
