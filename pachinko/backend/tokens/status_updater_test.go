package tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	cache_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/tokens"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	gd "github.com/guregu/dynamo"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestStatusUpdater_UpdateStatus(t *testing.T) {
	Convey("given a token status updater", t, func() {
		tokensDao := new(tokens_mock.DAO)
		transactionDao := new(transactions_mock.DAO)
		cache := new(cache_mock.Cache)

		statusUpdater := &statusUpdater{
			TokensDAO:       tokensDao,
			TransactionsDAO: transactionDao,
			Cache:           cache,
		}

		transactionID := random.String(32)

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"
		quantity := int64(0)
		token := pachinko.Token{
			OwnerId:  userID,
			GroupId:  channelID,
			Domain:   pachinko.Domain_STICKER,
			Quantity: quantity,
			Status:   pachinko.TokenStatus_FROZEN,
		}

		dynamoToken := dynamo.ToDynamoToken(token)
		dynamoToken.LatestTransactionID = transactionID

		cache.On("Del", ctx, userID, channelID).Return()

		Convey("when the transactions DAO works", func() {
			transactionDao.On("GetWriteTransactionTx", mock.Anything).Return(&gd.Put{}, nil)

			Convey("when the tokens DAO returns an error", func() {
				tokensDao.On("SetTokenStatus", ctx, dynamoToken, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := statusUpdater.UpdateStatus(ctx, transactionID, token)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the tokens DAO succeeds", func() {
				tokensDao.On("SetTokenStatus", ctx, dynamoToken, mock.Anything).Return(&dynamo.Token{
					OwnerID: userID,
					GroupID: channelID,
					Balance: 322,
					Status:  dynamo.FrozenTokenStatus,
				}, nil)

				Convey("then we return nil", func() {
					newBalance, err := statusUpdater.UpdateStatus(ctx, transactionID, token)
					So(err, ShouldBeNil)
					So(newBalance, ShouldNotBeNil)
					So(newBalance.Quantity, ShouldEqual, int64(322))
					So(newBalance.Status, ShouldEqual, pachinko.TokenStatus_FROZEN)
				})
			})
		})
		Convey("when the transactions DAO returns an error", func() {
			transactionDao.On("GetWriteTransactionTx", mock.Anything).Return(nil, errors.New("ANIME BEAR CONVENTION IN PROGRESS"))
			_, err := statusUpdater.UpdateStatus(ctx, transactionID, token)
			So(err, ShouldNotBeNil)
		})
	})
}
