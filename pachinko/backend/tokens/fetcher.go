package tokens

import (
	"context"

	"code.justin.tv/commerce/logrus"
	cache "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	"code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
)

type Fetcher interface {
	Fetch(ctx context.Context, ownerID, groupID string) (*pachinko.Token, error)
	FetchBatchForOwner(ctx context.Context, ownerID string, groupIDs []string) (map[string]*pachinko.Token, error)
	FetchBatch(ctx context.Context, batches []dynamo.Keyed) ([]*pachinko.Token, error)
	FetchAll(ctx context.Context, ownerID string) (map[string]*pachinko.Token, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Config *config.Config `inject:""`
	Cache  cache.Cache    `inject:""`
	DAO    tokens.DAO     `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, ownerID, groupID string) (*pachinko.Token, error) {
	cachedToken := f.Cache.Get(ctx, ownerID, groupID)
	if cachedToken != nil {
		return cachedToken, nil
	}

	dbToken, err := f.DAO.GetTokenBalance(ctx, ownerID, groupID)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"ownerID": ownerID,
			"groupID": groupID,
		}).Error("could not get token balance")
		return nil, err
	}

	if dbToken == nil {
		return nil, nil
	}

	twirpToken := dbToken.ToTwirpToken(f.Config.TenantDomain)

	f.Cache.Set(ctx, &twirpToken)

	return &twirpToken, nil
}

func (f *fetcher) FetchBatchForOwner(ctx context.Context, ownerID string, groupIDs []string) (map[string]*pachinko.Token, error) {
	cachedTokens := f.Cache.GetBatch(ctx, ownerID, groupIDs)
	if len(cachedTokens) > 0 && len(cachedTokens) == len(groupIDs) {
		return cachedTokens, nil
	}

	dbTokens, err := f.DAO.GetTokenBalances(ctx, ownerID, groupIDs...)

	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"domain":  f.Config.TenantDomain.String(),
			"ownerID": ownerID,
		}).Error("could not get token balances")
		return nil, err
	}

	if cachedTokens == nil {
		cachedTokens = map[string]*pachinko.Token{}
	}

	for _, dbValue := range dbTokens {
		v := dbValue
		twirpToken := v.ToTwirpToken(f.Config.TenantDomain)
		if _, ok := cachedTokens[twirpToken.GroupId]; !ok {
			cachedTokens[dbValue.GroupID] = &twirpToken
		}
	}

	f.Cache.SetBatch(ctx, ownerID, cachedTokens)

	return cachedTokens, nil
}

func (f *fetcher) FetchBatch(ctx context.Context, batches []dynamo.Keyed) ([]*pachinko.Token, error) {
	ownerGroupPairs := make([][2]string, len(batches))
	for i, batch := range batches {
		ownerGroupPairs[i] = [2]string{batch.HashKey().(string), batch.RangeKey().(string)}
	}
	cachedTokens := f.Cache.MultiGet(ctx, ownerGroupPairs...)

	if cachedTokens != nil && len(cachedTokens) == len(batches) {
		return cachedTokens, nil
	}

	dbTokens, err := f.DAO.GetTokenBalancesBatch(ctx, batches)

	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"batches": batches,
		}).Error("could not get token balances")
		return nil, err
	}

	cachedTokenMap := make(map[[2]string]*pachinko.Token)
	for _, cachedToken := range cachedTokens {
		cachedTokenMap[[2]string{cachedToken.OwnerId, cachedToken.GroupId}] = cachedToken
	}

	for _, dbValue := range dbTokens {
		v := dbValue
		twirpToken := v.ToTwirpToken(f.Config.TenantDomain)
		ownerGroupPair := [2]string{twirpToken.OwnerId, twirpToken.GroupId}
		if _, ok := cachedTokenMap[ownerGroupPair]; !ok {
			cachedTokenMap[ownerGroupPair] = &twirpToken
			cachedTokens = append(cachedTokens, &twirpToken)
		}
	}

	f.Cache.MultiSet(ctx, cachedTokenMap)

	return cachedTokens, nil
}

func (f *fetcher) FetchAll(ctx context.Context, ownerID string) (map[string]*pachinko.Token, error) {
	cachedTokens, totalRecords := f.Cache.GetAll(ctx, ownerID)
	if len(cachedTokens) > 0 && len(cachedTokens) == totalRecords {
		return cachedTokens, nil
	}

	dbTokens, err := f.DAO.GetTokenBalances(ctx, ownerID)

	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"domain":  f.Config.TenantDomain.String(),
			"ownerID": ownerID,
		}).Error("could not get token balances")
		return nil, err
	}

	if cachedTokens == nil {
		cachedTokens = map[string]*pachinko.Token{}
	}

	for _, dbValue := range dbTokens {
		v := dbValue
		twirpToken := v.ToTwirpToken(f.Config.TenantDomain)
		if _, ok := cachedTokens[twirpToken.GroupId]; !ok {
			cachedTokens[dbValue.GroupID] = &twirpToken
		}
	}

	f.Cache.SetBatch(ctx, ownerID, cachedTokens)

	return cachedTokens, nil
}
