package tokens

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	cache_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/tokens"
	transaction_timestamp_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	gd "github.com/guregu/dynamo"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAdder_Add(t *testing.T) {
	Convey("given a token entitler", t, func() {
		tokensDao := new(tokens_mock.DAO)
		transactionDao := new(transactions_mock.DAO)
		cache := new(cache_mock.Cache)
		transactionTimestampCache := new(transaction_timestamp_mock.Cache)

		entitler := &entitler{
			TokensDAO:                 tokensDao,
			TransactionsDAO:           transactionDao,
			Cache:                     cache,
			TransactionTimestampCache: transactionTimestampCache,
		}

		transactionID := random.String(32)

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"
		quantity := int64(2)
		token := pachinko.Token{
			OwnerId:  userID,
			GroupId:  channelID,
			Domain:   pachinko.Domain_STICKER,
			Quantity: quantity,
		}

		transactionTimestamp := time.Now()

		dynamoToken := dynamo.ToDynamoToken(token)
		dynamoToken.LatestTransactionID = transactionID

		cache.On("Del", ctx, userID, channelID).Return()

		Convey("we return success", func() {
			transactionDao.On("GetWriteTransactionTx", mock.Anything).Return(&gd.Put{}, nil)
			tokensDao.On("IncrementTokenBalance", ctx, dynamoToken, mock.Anything).Return(&dynamo.Token{
				OwnerID: userID,
				GroupID: channelID,
				Balance: 322,
			}, nil)

			Convey("when we find the transaction timestamp", func() {
				transactionTimestampCache.On("Get", ctx, transactionID).Return(&transactionTimestamp)
			})

			Convey("when we don't find the transaction timestamp and successfully put the transaction timestamp in the cache", func() {
				transactionTimestampCache.On("Get", ctx, transactionID).Return(nil)
				transactionTimestampCache.On("Set", ctx, transactionID, mock.Anything).Return(nil)
			})

			newBalance, err := entitler.Entitle(ctx, transactionID, token)
			So(err, ShouldBeNil)
			So(newBalance, ShouldNotBeNil)
			So(newBalance.Quantity, ShouldEqual, int64(322))
		})

		Convey("we return error", func() {
			Convey("when we fail to set the transaction timestamp if we didn't find one", func() {
				transactionTimestampCache.On("Get", ctx, transactionID).Return(nil)
				transactionTimestampCache.On("Set", ctx, transactionID, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			Convey("when the transaction dao fails", func() {
				transactionTimestampCache.On("Get", ctx, transactionID).Return(&transactionTimestamp)
				transactionDao.On("GetWriteTransactionTx", mock.Anything).Return(nil, errors.New("ANIME BEAR CONVENTION IN PROGRESS"))
			})

			Convey("when the tokens dao fails", func() {
				transactionTimestampCache.On("Get", ctx, transactionID).Return(&transactionTimestamp)
				transactionDao.On("GetWriteTransactionTx", mock.Anything).Return(&gd.Put{}, nil)
				tokensDao.On("IncrementTokenBalance", ctx, dynamoToken, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := entitler.Entitle(ctx, transactionID, token)
			So(err, ShouldNotBeNil)
		})
	})
}
