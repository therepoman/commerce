package tokens

import (
	"context"

	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
)

type Getter interface {
	Get(ctx context.Context, ownerID, groupID string) (*pachinko.Token, error)
	GetFiltered(ctx context.Context, ownerID string, groupIDs []string) ([]*pachinko.Token, error)
	GetFilteredByBatch(ctx context.Context, keys []dynamo.Keyed) ([]*pachinko.Token, error)
	GetAll(ctx context.Context, ownerID string) ([]*pachinko.Token, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	Config  *config.Config `inject:""`
	Fetcher Fetcher        `inject:""`
}

func (g *getter) Get(ctx context.Context, ownerID, groupID string) (*pachinko.Token, error) {
	balance, err := g.Fetcher.Fetch(ctx, ownerID, groupID)
	if err != nil {
		return nil, err
	}

	if balance == nil {
		return &pachinko.Token{
			Domain:  g.Config.TenantDomain,
			OwnerId: ownerID,
			GroupId: groupID,
		}, nil
	}

	return balance, nil
}

func (g *getter) GetFiltered(ctx context.Context, ownerID string, groupIDs []string) ([]*pachinko.Token, error) {
	tokenBalances, err := g.Fetcher.FetchBatchForOwner(ctx, ownerID, groupIDs)
	if err != nil {
		return nil, err
	}

	balances := make([]*pachinko.Token, 0)
	for _, gID := range groupIDs {
		balance, ok := tokenBalances[gID]
		if ok {
			balances = append(balances, balance)
		} else {
			balances = append(balances, &pachinko.Token{
				Domain:   g.Config.TenantDomain,
				OwnerId:  ownerID,
				GroupId:  gID,
				Quantity: int64(0),
			})
		}
	}

	return balances, nil
}

func (g *getter) GetFilteredByBatch(ctx context.Context, keys []dynamo.Keyed) ([]*pachinko.Token, error) {
	tokenBalances, err := g.Fetcher.FetchBatch(ctx, keys)
	if err != nil {
		return nil, err
	}

	for _, key := range keys {
		balanceWasFound := false
		for _, balance := range tokenBalances {
			if balance != nil && key.HashKey() == balance.OwnerId && key.RangeKey() == balance.GroupId {
				balanceWasFound = true
			}
		}
		if !balanceWasFound {
			tokenBalances = append(tokenBalances, &pachinko.Token{
				Domain:   g.Config.TenantDomain,
				OwnerId:  key.HashKey().(string),
				GroupId:  key.RangeKey().(string),
				Quantity: int64(0),
			})
		}
	}

	return tokenBalances, nil
}

func (g *getter) GetAll(ctx context.Context, ownerID string) ([]*pachinko.Token, error) {
	tokens, err := g.Fetcher.FetchAll(ctx, ownerID)
	if err != nil {
		return nil, err
	}

	tokensList := make([]*pachinko.Token, 0)
	for _, token := range tokens {
		tokensList = append(tokensList, token)
	}

	return tokensList, nil
}
