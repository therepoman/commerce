package tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	cache_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/tokens"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	guregu_dynamo "github.com/guregu/dynamo"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a token fetcher", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		dao := new(tokens_mock.DAO)
		cache := new(cache_mock.Cache)

		fetcher := &fetcher{
			Config: c,
			Cache:  cache,
			DAO:    dao,
		}

		ctx := context.Background()
		domain := pachinko.Domain_STICKER
		ownerID := "123123123"
		groupID := "321321321"

		Convey("when the cache returns a value", func() {
			cache.On("Get", ctx, ownerID, groupID).Return(&pachinko.Token{
				Domain:  domain,
				OwnerId: ownerID,
				GroupId: groupID,
			})

			res, err := fetcher.Fetch(ctx, ownerID, groupID)
			So(err, ShouldBeNil)
			So(res, ShouldNotBeNil)
		})

		Convey("when the cache does not return a value", func() {
			cache.On("Get", ctx, ownerID, groupID).Return(nil)

			Convey("when we fail to call the DAO", func() {
				dao.On("GetTokenBalance", ctx, ownerID, groupID).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := fetcher.Fetch(ctx, ownerID, groupID)
				So(err, ShouldNotBeNil)
			})

			Convey("if the dao cannot find the balance", func() {
				dao.On("GetTokenBalance", ctx, ownerID, groupID).Return(nil, nil)

				res, err := fetcher.Fetch(ctx, ownerID, groupID)
				So(err, ShouldBeNil)
				So(res, ShouldBeNil)
			})

			Convey("if the dao finds the balance", func() {
				dynamoToken := &dynamo.Token{
					OwnerID: ownerID,
					GroupID: groupID,
					Balance: 322,
				}

				twirpToken := dynamoToken.ToTwirpToken(domain)

				dao.On("GetTokenBalance", ctx, ownerID, groupID).Return(dynamoToken, nil)
				cache.On("Set", ctx, &twirpToken).Return()

				res, err := fetcher.Fetch(ctx, ownerID, groupID)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &twirpToken)
			})
		})
	})
}

func TestFetcher_FetchBatchForOwner(t *testing.T) {
	Convey("given a token fetcher", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		dao := new(tokens_mock.DAO)
		cache := new(cache_mock.Cache)

		fetcher := &fetcher{
			Config: c,
			Cache:  cache,
			DAO:    dao,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		groupIDOther := "765765765"
		groupIDs := []string{groupID, groupIDOther}

		Convey("when the cache returns values", func() {
			cache.On("GetBatch", ctx, ownerID, groupIDs).Return(map[string]*pachinko.Token{
				groupID: {
					OwnerId:  ownerID,
					GroupId:  groupID,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 1,
				},
				groupIDOther: {
					OwnerId:  ownerID,
					GroupId:  groupIDOther,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 2,
				},
			})

			Convey("then we return the cached values", func() {
				resp, err := fetcher.FetchBatchForOwner(ctx, ownerID, groupIDs)
				So(err, ShouldBeNil)
				So(resp, ShouldHaveLength, 2)
				So(resp[groupID].Quantity, ShouldEqual, int64(1))
				So(resp[groupIDOther].Quantity, ShouldEqual, int64(2))
			})
		})

		Convey("when the cache returns no values", func() {
			cache.On("GetBatch", ctx, ownerID, groupIDs).Return(map[string]*pachinko.Token{
				groupID: {
					OwnerId:  ownerID,
					GroupId:  groupID,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 1,
				},
			})

			Convey("when DAO fails to return balances", func() {
				dao.On("GetTokenBalances", ctx, ownerID, groupID, groupIDOther).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := fetcher.FetchBatchForOwner(ctx, ownerID, groupIDs)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the DAO call succeeds", func() {
				dao.On("GetTokenBalances", ctx, ownerID, groupID, groupIDOther).Return([]*dynamo.Token{
					{
						OwnerID: ownerID,
						GroupID: groupID,
						Balance: 1,
					},
					{
						OwnerID: ownerID,
						GroupID: groupIDOther,
						Balance: 2,
					},
				}, nil)

				cache.On("SetBatch", ctx, ownerID, map[string]*pachinko.Token{
					groupID: {
						OwnerId:  ownerID,
						GroupId:  groupID,
						Domain:   pachinko.Domain_STICKER,
						Quantity: 1,
					},
					groupIDOther: {
						OwnerId:  ownerID,
						GroupId:  groupIDOther,
						Domain:   pachinko.Domain_STICKER,
						Quantity: 2,
					},
				}).Return(nil)

				Convey("then we return the list of token balances", func() {
					resp, err := fetcher.FetchBatchForOwner(ctx, ownerID, groupIDs)
					So(err, ShouldBeNil)
					So(resp, ShouldHaveLength, 2)
					So(resp[groupID].Quantity, ShouldEqual, int64(1))
					So(resp[groupIDOther].Quantity, ShouldEqual, int64(2))
				})
			})
		})
	})
}

func TestFetcher_FetchBatch(t *testing.T) {
	Convey("given a token fetcher", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		dao := new(tokens_mock.DAO)
		cache := new(cache_mock.Cache)

		fetcher := &fetcher{
			Config: c,
			Cache:  cache,
			DAO:    dao,
		}

		ctx := context.Background()
		ownerID1 := "123123123"
		groupID1 := "321321321"

		batches := []guregu_dynamo.Keyed{
			guregu_dynamo.Keys{ownerID1, groupID1},
		}

		ownerGroupPairs := [][2]string{
			{ownerID1, groupID1},
		}

		Convey("when the cache returns values", func() {
			cache.On("MultiGet", ctx, ownerGroupPairs[0]).Return([]*pachinko.Token{
				{
					OwnerId:  ownerID1,
					GroupId:  groupID1,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 1,
				},
			})

			Convey("then we return the cached values", func() {
				resp, err := fetcher.FetchBatch(ctx, batches)
				So(err, ShouldBeNil)
				So(resp, ShouldHaveLength, 1)
				So(resp[0].Quantity, ShouldEqual, int64(1))
			})
		})

		Convey("when the cache returns no values", func() {
			cache.On("MultiGet", ctx, ownerGroupPairs[0]).Return([]*pachinko.Token{})

			Convey("when DAO fails to return balances", func() {
				dao.On("GetTokenBalancesBatch", ctx, batches).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := fetcher.FetchBatch(ctx, batches)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the DAO call succeeds", func() {
				dao.On("GetTokenBalancesBatch", ctx, batches).Return([]*dynamo.Token{
					{
						OwnerID: ownerID1,
						GroupID: groupID1,
						Balance: 1,
					},
				}, nil)

				cache.On("MultiSet", ctx, map[[2]string]*pachinko.Token{
					{ownerID1, groupID1}: {
						OwnerId:  ownerID1,
						GroupId:  groupID1,
						Domain:   pachinko.Domain_STICKER,
						Quantity: 1,
					},
				}).Return(nil)

				Convey("then we return the list of token balances", func() {
					resp, err := fetcher.FetchBatch(ctx, batches)
					So(err, ShouldBeNil)
					So(resp, ShouldHaveLength, 1)
					So(resp[0].Quantity, ShouldEqual, int64(1))
				})
			})
		})
	})
}

func TestFetcher_FetchAll(t *testing.T) {
	Convey("given a token fetcher", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		dao := new(tokens_mock.DAO)
		cache := new(cache_mock.Cache)

		fetcher := &fetcher{
			Config: c,
			Cache:  cache,
			DAO:    dao,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		groupIDOther := "765765765"

		Convey("when the cache returns values", func() {
			cache.On("GetAll", ctx, ownerID).Return(map[string]*pachinko.Token{
				groupID: {
					OwnerId:  ownerID,
					GroupId:  groupID,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 1,
				},
				groupIDOther: {
					OwnerId:  ownerID,
					GroupId:  groupIDOther,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 2,
				},
			}, 2)

			Convey("then we return the cached values", func() {
				resp, err := fetcher.FetchAll(ctx, ownerID)
				So(err, ShouldBeNil)
				So(resp, ShouldHaveLength, 2)
				So(resp[groupID].Quantity, ShouldEqual, int64(1))
				So(resp[groupIDOther].Quantity, ShouldEqual, int64(2))
			})
		})

		Convey("when the cache returns no values", func() {
			cache.On("GetAll", ctx, ownerID).Return(map[string]*pachinko.Token{
				groupID: {
					OwnerId:  ownerID,
					GroupId:  groupID,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 1,
				},
			}, 2)

			Convey("when DAO fails to return balances", func() {
				dao.On("GetTokenBalances", ctx, ownerID).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := fetcher.FetchAll(ctx, ownerID)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the DAO call succeeds", func() {
				dao.On("GetTokenBalances", ctx, ownerID).Return([]*dynamo.Token{
					{
						OwnerID: ownerID,
						GroupID: groupID,
						Balance: 1,
					},
					{
						OwnerID: ownerID,
						GroupID: groupIDOther,
						Balance: 2,
					},
				}, nil)

				cache.On("SetBatch", ctx, ownerID, map[string]*pachinko.Token{
					groupID: {
						OwnerId:  ownerID,
						GroupId:  groupID,
						Domain:   pachinko.Domain_STICKER,
						Quantity: 1,
					},
					groupIDOther: {
						OwnerId:  ownerID,
						GroupId:  groupIDOther,
						Domain:   pachinko.Domain_STICKER,
						Quantity: 2,
					},
				}).Return(nil)

				Convey("then we return the list of token balances", func() {
					resp, err := fetcher.FetchAll(ctx, ownerID)
					So(err, ShouldBeNil)
					So(resp, ShouldHaveLength, 2)
					So(resp[groupID].Quantity, ShouldEqual, int64(1))
					So(resp[groupIDOther].Quantity, ShouldEqual, int64(2))
				})
			})
		})
	})
}
