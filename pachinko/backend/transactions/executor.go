package transactions

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"github.com/pkg/errors"
)

type Executor interface {
	Execute(ctx context.Context, transaction *dynamo.Transaction) error
}

func NewExecutor() Executor {
	return &executor{}
}

type executor struct {
	Consumer tokens.Consumer `inject:""`
}

func (e *executor) Execute(ctx context.Context, transaction *dynamo.Transaction) error {
	if transaction.Operation == dynamo.ConsumeTokensOperation {
		err := e.Consumer.Consume(ctx, transaction.ID, dynamo.ToTwirpTokens(transaction.Tokens), []dynamo.Update{})
		if err != nil {
			return err
		}
	} else {
		return errors.Errorf("invalid operation passed to execute transaction: %s", string(transaction.Operation))
	}

	return nil
}
