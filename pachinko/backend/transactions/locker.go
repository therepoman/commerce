package transactions

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/cache/balancelock"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type Locker interface {
	Lock(ctx context.Context, token pachinko.Token) (func(), error)
}

func NewLocker() Locker {
	return &locker{}
}

type locker struct {
	BalanceLocker balancelock.Cache `inject:""`
}

func (l *locker) Lock(ctx context.Context, token pachinko.Token) (unlockFn func(), err error) {
	fields := logrus.Fields{
		"ownerID": token.OwnerId,
		"groupID": token.GroupId,
		"domain":  token.Domain.String(),
	}

	lockingKey, err := l.BalanceLocker.LockTokenBalance(ctx, token)
	if err != nil {
		return nil, err
	}

	return func() {
		err := lockingKey.Unlock()
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not release lock on balance transactions")
		}
	}, nil
}
