package transactions

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	tx_cache "code.justin.tv/commerce/pachinko/backend/cache/transaction"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
)

type Conductor interface {
	Start(ctx context.Context, transaction *models.TransactionRequest) (*pachinko.ValidationError, error)
	Finish(ctx context.Context, transactionID string) ([]*pachinko.Token, error)
}

func NewConductor() Conductor {
	return &conductor{}
}

type conductor struct {
	Config            *config.Config   `inject:""`
	Validator         Validator        `inject:""`
	TransactionsCache tx_cache.Cache   `inject:""`
	TransactionsDAO   transactions.DAO `inject:""`
	Executor          Executor         `inject:""`
	Locker            Locker           `inject:""`
	TokensGetter      tokens.Getter    `inject:""`
}

func (e *conductor) Start(ctx context.Context, transaction *models.TransactionRequest) (*pachinko.ValidationError, error) {
	transactionID := transaction.ID
	operation := transaction.Operation
	operandID := transaction.OperandID
	tokens := transaction.Tokens

	existingTransaction, err := e.TransactionsDAO.GetTransactionRecord(ctx, transactionID)
	if err != nil {
		logrus.WithError(err).Error("could not fetch transaction record in start transaction")
		return nil, err
	}
	if existingTransaction != nil {
		if existingTransaction.IsCommitted {
			return &pachinko.ValidationError{
				Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
			}, nil
		}
		return nil, nil
	}

	var ownerID string
	if !strings.Blank(transaction.OperandID) {
		operandTransaction, err := e.TransactionsDAO.GetTransactionRecord(ctx, transaction.OperandID)
		if err != nil {
			return nil, err
		}
		if operandTransaction == nil {
			return &pachinko.ValidationError{
				Code: pachinko.ValidationErrorCode_OPERAND_NOT_FOUND,
			}, nil
		}
		if !operandTransaction.IsCommitted {
			return &pachinko.ValidationError{
				Code: pachinko.ValidationErrorCode_OPERAND_NOT_COMMITTED,
			}, nil
		}
		// use the previous transaction's ownerID and tokens
		ownerID = operandTransaction.OwnerID
		tokens = dynamo.ToTwirpTokens(operandTransaction.Tokens)
	} else {
		ownerID = tokens[0].OwnerId
	}

	for _, token := range tokens {
		fields := logrus.Fields{
			"transactionID": transactionID,
			"amount":        GetOperator(operation) * int(token.Quantity),
			"ownerID":       token.OwnerId,
			"groupID":       token.GroupId,
			"domain":        e.Config.TenantDomain.String(),
		}

		unlockFn, err := e.Locker.Lock(ctx, *token)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not lock balance on start transaction validation")
			return nil, err
		}

		transaction := models.Transaction{
			ID:        transactionID,
			Operation: operation,
			OperandID: operandID,
			Amount:    GetOperator(operation) * int(token.Quantity),
		}

		validationErr, err := e.Validator.IsValid(ctx, *token, transaction)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not validate transaction")
			unlockFn()
			return nil, err
		}

		if validationErr != nil {
			unlockFn()
			return validationErr, nil
		}

		unlockFn()
	}

	err = e.TransactionsDAO.WriteNew(ctx, dynamo.Transaction{
		ID:        transactionID,
		Operation: dynamo.ToDynamoOperation(operation),
		OperandID: operandID,
		OwnerID:   ownerID,
		Timestamp: time.Now(),
		Tokens:    dynamo.ToTHTokens(tokens),
	})
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			// If we fail to create a new transaction because the ID already exists, we do not fail on this.
			// But we will return successful as if this transaction was created, but we will not add this to the
			// in-flight transaction cache.
			if awsErr.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
				return nil, nil
			}
		}
		logrus.WithError(err).Error("could not add transaction record to dynamo")
		return nil, err
	}

	for _, token := range tokens {
		fields := logrus.Fields{
			"transactionID": transactionID,
			"amount":        GetOperator(operation) * int(token.Quantity),
			"ownerID":       token.OwnerId,
			"groupID":       token.GroupId,
			"domain":        e.Config.TenantDomain.String(),
		}

		unlockFn, err := e.Locker.Lock(ctx, *token)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not lock balance for adding to transaction cache")
			return nil, err
		}

		transaction := models.Transaction{
			ID:        transactionID,
			Operation: operation,
			OperandID: operandID,
			Amount:    GetOperator(operation) * int(token.Quantity),
		}

		err = e.TransactionsCache.Add(ctx, *token, transaction)
		if err != nil {
			logrus.WithFields(fields).Error("could not add transaction to cache")
			unlockFn()
			return nil, err
		}

		unlockFn()
	}

	return nil, nil
}

func (e *conductor) Finish(ctx context.Context, transactionID string) ([]*pachinko.Token, error) {
	fields := logrus.Fields{
		"transactionID": transactionID,
		"domain":        e.Config.TenantDomain.String(),
	}

	transaction, err := e.TransactionsDAO.GetTransactionRecord(ctx, transactionID)
	if err != nil {
		logrus.WithFields(fields).WithError(err).Error("could not retrieve transaction from dynamo")
		return nil, err
	}

	if transaction == nil {
		logrus.WithFields(fields).Error("no transaction was found while finishing transaction")
		return nil, errors.New("no transaction was found while finishing transaction")
	}

	fields = logrus.Fields{
		"transactionID": transactionID,
		"domain":        e.Config.TenantDomain.String(),
		"operation":     string(transaction.Operation),
	}

	err = e.Executor.Execute(ctx, transaction)
	if err != nil {
		logrus.WithError(err).WithFields(fields).Error("could not execute transaction")
		return nil, err
	}

	groupIDs := make([]string, 0)
	for _, token := range transaction.Tokens {
		groupIDs = append(groupIDs, token.GroupId)

		fields := logrus.Fields{
			"ownerID": token.OwnerId,
			"groupID": token.GroupId,
		}

		// try to remove this transaction from the in-flight transactions cache
		unlockFn, err := e.Locker.Lock(ctx, *token.ToTwirpToken())
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not lock balance to complete transaction")
			continue
		}

		err = e.TransactionsCache.Remove(ctx, *token.ToTwirpToken(), transactionID)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not remove transaction from transactions cache")
			unlockFn()
			continue
		}

		unlockFn()
	}

	updatedTokens, err := e.TokensGetter.GetFiltered(ctx, transaction.OwnerID, groupIDs)
	if err != nil {
		logrus.WithFields(fields).WithError(err).Error("could not look up transacted token balances")
		return nil, err
	}

	return updatedTokens, nil
}
