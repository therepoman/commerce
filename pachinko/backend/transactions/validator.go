package transactions

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/cache/transaction"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type Validator interface {
	IsValid(ctx context.Context, token pachinko.Token, transaction models.Transaction) (*pachinko.ValidationError, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	TokenGetter       tokens.Getter     `inject:""`
	TransactionsCache transaction.Cache `inject:""`
}

func (v *validator) IsValid(ctx context.Context, token pachinko.Token, transaction models.Transaction) (*pachinko.ValidationError, error) {
	fields := logrus.Fields{
		"ownerID":        token.OwnerId,
		"groupID":        token.GroupId,
		"domain":         token.Domain.String(),
		"transaction_id": transaction.ID,
		"operation":      transaction.Operation,
		"operand_id":     transaction.OperandID,
	}

	transactions, err := v.TransactionsCache.GetAll(ctx, token)
	if err != nil {
		logrus.WithFields(fields).WithError(err).Error("could not get balance transactions")
		return nil, err
	}

	balance, err := v.TokenGetter.Get(ctx, token.OwnerId, token.GroupId)
	if err != nil {
		logrus.WithFields(fields).WithError(err).Error("could not get balance")
		return nil, err
	}

	if transaction.Operation == pachinko.Operation_CONSUME {
		if balance == nil {
			return &pachinko.ValidationError{
				Code:         pachinko.ValidationErrorCode_NO_BALANCE,
				InvalidToken: &token,
			}, nil
		}

		if balance.Status == pachinko.TokenStatus_FROZEN {
			return &pachinko.ValidationError{
				Code:         pachinko.ValidationErrorCode_BALANCE_FROZEN,
				InvalidToken: &token,
			}, nil
		}

		sumOfOperations := 0
		for _, t := range transactions {
			sumOfOperations += t.Amount
		}

		if int(balance.Quantity)+sumOfOperations+transaction.Amount < 0 {
			return &pachinko.ValidationError{
				Code:         pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
				InvalidToken: &token,
			}, nil
		}
	}

	return nil, nil
}
