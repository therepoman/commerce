package transactions

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestExecutor_Execute(t *testing.T) {
	Convey("given a transaction executor", t, func() {
		consumer := new(tokens_mock.Consumer)

		e := &executor{
			Consumer: consumer,
		}

		tokens := []*pachinko.Token{
			{
				Domain:   pachinko.Domain_STICKER,
				OwnerId:  random.String(16),
				GroupId:  random.String(16),
				Quantity: random.Int64(0, 16),
			},
		}

		transaction := &dynamo.Transaction{
			ID:        random.String(32),
			Timestamp: time.Now(),
			Tokens:    dynamo.ToTHTokens(tokens),
		}

		ctx := context.Background()
		updates := []dynamo.Update{}

		Convey("when the operation is a consume operation", func() {
			transaction.Operation = dynamo.ConsumeTokensOperation

			Convey("when the consumer fails", func() {
				consumer.On("Consume", ctx, transaction.ID, tokens, updates).Return(errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					err := e.Execute(ctx, transaction)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the consumer succeeds", func() {
				consumer.On("Consume", ctx, transaction.ID, tokens, updates).Return(nil)

				Convey("then we should return nil", func() {
					err := e.Execute(ctx, transaction)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("when the operation is not valid", func() {
			transaction.Operation = dynamo.AddTokensOperation

			Convey("then we should return an error", func() {
				err := e.Execute(ctx, transaction)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
