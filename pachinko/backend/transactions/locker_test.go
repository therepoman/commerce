package transactions

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	balancelock_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/balancelock"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLocker_Lock(t *testing.T) {
	Convey("given a transaction locker", t, func() {
		balanceLocker := new(balancelock_mock.Cache)

		locker := &locker{
			BalanceLocker: balanceLocker,
		}

		ctx := context.Background()

		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}

		Convey("when we fail to lock the balance", func() {
			balanceLocker.On("LockTokenBalance", ctx, reqToken).Return(nil, errors.New("WALRUS STRIKE"))

			_, err := locker.Lock(ctx, reqToken)
			So(err, ShouldNotBeNil)
		})

		Convey("when we succeed to lock the balance", func() {
			balanceLocker.On("LockTokenBalance", ctx, reqToken).Return(nil, nil)

			unlockFn, err := locker.Lock(ctx, reqToken)
			So(err, ShouldBeNil)
			So(unlockFn, ShouldNotBeNil)
		})
	})
}
