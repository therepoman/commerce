package models

import (
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type Transaction struct {
	ID        string             `json:"transaction_id"`
	Operation pachinko.Operation `json:"operation"`
	OperandID string             `json:"operand_id"`
	Amount    int                `json:"amount"`
}

type TransactionRequest struct {
	Domain    pachinko.Domain
	ID        string
	Operation pachinko.Operation
	OperandID string
	Tokens    []*pachinko.Token
}
