package transactions

import pachinko "code.justin.tv/commerce/pachinko/rpc"

func GetOperator(operation pachinko.Operation) int {
	switch operation {
	case pachinko.Operation_CONSUME:
		return -1
	case pachinko.Operation_CREATE_HOLD:
		return -1
	default:
		return 1
	}
}
