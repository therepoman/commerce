package transactions

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	transaction_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/transaction"
	ddb_tx_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConductor_Start(t *testing.T) {
	Convey("given a transactions conductor", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		transactionCache := new(transaction_mock.Cache)
		transactionsDao := new(ddb_tx_mock.DAO)
		validator := new(transactions_mock.Validator)
		locker := new(transactions_mock.Locker)
		executor := new(transactions_mock.Executor)

		conductor := &conductor{
			Config:            c,
			Locker:            locker,
			TransactionsCache: transactionCache,
			TransactionsDAO:   transactionsDao,
			Validator:         validator,
			Executor:          executor,
		}

		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		transactionID := uuid.NewV4().String()
		reqToken := pachinko.Token{
			Quantity: 1,
			OwnerId:  ownerID,
			GroupId:  groupID,
			Domain:   domain,
		}
		tokens := []*pachinko.Token{&reqToken}
		transaction := models.Transaction{
			ID:        transactionID,
			Operation: pachinko.Operation_CONSUME,
			Amount:    -1,
		}

		transactionReq := &models.TransactionRequest{
			ID:        transactionID,
			Operation: pachinko.Operation_CONSUME,
			Tokens:    tokens,
		}

		ctx := context.Background()

		Convey("when the transaction lookup errors", func() {
			transactionsDao.On("GetTransactionRecord", ctx, transactionID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("should return an error", func() {
				_, err := conductor.Start(ctx, transactionReq)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the transaction is found in the database and is committed", func() {
			transactionsDao.On("GetTransactionRecord", ctx, transactionID).Return(&dynamo.Transaction{IsCommitted: true}, nil)

			Convey("should a validation error and no error", func() {
				validationErr, err := conductor.Start(ctx, transactionReq)
				So(err, ShouldBeNil)
				So(validationErr, ShouldNotBeNil)
				So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
			})
		})

		Convey("when the transaction is found in the database and isn't committed", func() {
			transactionsDao.On("GetTransactionRecord", ctx, transactionID).Return(&dynamo.Transaction{}, nil)

			Convey("should return nil and no error", func() {
				validationErr, err := conductor.Start(ctx, transactionReq)
				So(err, ShouldBeNil)
				So(validationErr, ShouldBeNil)
			})
		})

		Convey("when the transaction is not found in the database", func() {
			transactionsDao.On("GetTransactionRecord", ctx, transactionID).Return(nil, nil)

			Convey("when the locker returns an error", func() {
				locker.On("Lock", ctx, reqToken).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("should return an error", func() {
					_, err := conductor.Start(ctx, transactionReq)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the locker succeeds", func() {
				locker.On("Lock", ctx, reqToken).Return(func() {}, nil)

				Convey("when the validator fails", func() {
					validator.On("IsValid", ctx, reqToken, transaction).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("should return an error", func() {
						_, err := conductor.Start(ctx, transactionReq)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the validator catches a validation error", func() {
					validator.On("IsValid", ctx, reqToken, transaction).Return(&pachinko.ValidationError{
						Code:         pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
						InvalidToken: &reqToken,
					}, nil)

					Convey("should return an error", func() {
						validationErr, err := conductor.Start(ctx, transactionReq)
						So(validationErr, ShouldNotBeNil)
						So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE)
						So(validationErr.InvalidToken, ShouldResemble, &reqToken)
						So(err, ShouldBeNil)
					})
				})

				Convey("when the validator succeeds", func() {
					validator.On("IsValid", ctx, reqToken, transaction).Return(nil, nil)

					Convey("when the transaction DAO fails to create a new transaction", func() {
						transactionsDao.On("WriteNew", ctx, mock.Anything).Return(errors.New("WALRUS STRIKE"))

						Convey("should return an error", func() {
							_, err := conductor.Start(ctx, transactionReq)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("when the transaction DAO returns a conditional check failure", func() {
						transactionsDao.On("WriteNew", ctx, mock.Anything).Return(awserr.New(dynamodb.ErrCodeConditionalCheckFailedException, "WALRUS STRIKE", errors.New("WALRUS STRIKE")))

						Convey("should return nil as if we succeeded, since we don't want to add to the transaction cache on this operation.", func() {
							validationErr, err := conductor.Start(ctx, transactionReq)
							So(validationErr, ShouldBeNil)
							So(err, ShouldBeNil)
						})
					})

					Convey("when the transaction DAO succeeds to create a new transaction", func() {
						transactionsDao.On("WriteNew", ctx, mock.Anything).Return(nil)

						Convey("when the locker returns an error", func() {
							locker.ExpectedCalls = nil
							locker.On("Lock", ctx, reqToken).Return(nil, errors.New("WALRUS STRIKE"))

							Convey("should return an error", func() {
								_, err := conductor.Start(ctx, transactionReq)
								So(err, ShouldNotBeNil)
							})
						})

						Convey("when the locker succeeds", func() {
							locker.On("Lock", ctx, reqToken).Return(func() {}, nil)

							Convey("when adding to the transaction cache fails", func() {
								transactionCache.On("Add", ctx, reqToken, transaction).Return(errors.New("WALRUS STRIKE"))

								Convey("should return an error", func() {
									_, err := conductor.Start(ctx, transactionReq)
									So(err, ShouldNotBeNil)
								})
							})

							Convey("when adding to the transaction cache succeeds", func() {
								transactionCache.On("Add", ctx, reqToken, transaction).Return(nil)

								Convey("should return nil", func() {
									_, err := conductor.Start(ctx, transactionReq)
									So(err, ShouldBeNil)
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestConductor_Finish(t *testing.T) {
	Convey("given a transactions conductor", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		transactionCache := new(transaction_mock.Cache)
		transactionDao := new(ddb_tx_mock.DAO)
		validator := new(transactions_mock.Validator)
		locker := new(transactions_mock.Locker)
		executor := new(transactions_mock.Executor)
		getter := new(tokens_mock.Getter)

		conductor := &conductor{
			Config:            c,
			Locker:            locker,
			TransactionsDAO:   transactionDao,
			TransactionsCache: transactionCache,
			Validator:         validator,
			Executor:          executor,
			TokensGetter:      getter,
		}

		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}
		transactionID := uuid.NewV4().String()
		ctx := context.Background()

		Convey("when we fail to get the transaction from dynamo", func() {
			transactionDao.On("GetTransactionRecord", ctx, transactionID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("should return an error", func() {
				_, err := conductor.Finish(ctx, transactionID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we cannot find the transaction in dynamo", func() {
			transactionDao.On("GetTransactionRecord", ctx, transactionID).Return(nil, nil)

			Convey("should return an error", func() {
				_, err := conductor.Finish(ctx, transactionID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we find the transaction in dynamo", func() {
			transaction := &dynamo.Transaction{
				ID:          transactionID,
				OwnerID:     reqToken.OwnerId,
				Operation:   dynamo.ConsumeTokensOperation,
				Timestamp:   time.Now(),
				Tokens:      []*dynamo.THToken{dynamo.ToTHToken(&reqToken)},
				IsCommitted: false,
			}
			transactionDao.On("GetTransactionRecord", ctx, transactionID).Return(transaction, nil)

			Convey("when the executor fails", func() {
				executor.On("Execute", ctx, transaction).Return(errors.New("WALRUS STRIKE"))

				Convey("should return an error", func() {
					_, err := conductor.Finish(ctx, transactionID)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the executor succeeds", func() {
				executor.On("Execute", ctx, transaction).Return(nil)

				Convey("when the transaction is successfully removed from cache", func() {
					locker.On("Lock", ctx, reqToken).Return(func() {}, nil)
					transactionCache.On("Remove", ctx, mock.Anything, transactionID).Return(nil)

					Convey("when the token getter fails", func() {
						getter.On("GetFiltered", ctx, transaction.OwnerID, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

						Convey("should return an error", func() {
							_, err := conductor.Finish(ctx, transactionID)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("when the token getter succeeds", func() {
						token := &pachinko.Token{
							Domain:   domain,
							OwnerId:  ownerID,
							GroupId:  groupID,
							Quantity: int64(322),
						}

						getter.On("GetFiltered", ctx, transaction.OwnerID, mock.Anything).Return([]*pachinko.Token{token}, nil)

						Convey("should return the token balance", func() {
							updatedBalances, err := conductor.Finish(ctx, transactionID)
							So(err, ShouldBeNil)
							So(updatedBalances, ShouldNotBeNil)
							So(updatedBalances[0].Quantity, ShouldEqual, token.Quantity)
						})
					})
				})
			})
		})
	})
}
