package transactions

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	transaction_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/transaction"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a transactions validator", t, func() {
		tokenGetter := new(tokens_mock.Getter)
		transactionsCache := new(transaction_mock.Cache)

		validator := &validator{
			TransactionsCache: transactionsCache,
			TokenGetter:       tokenGetter,
		}

		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}
		transaction := models.Transaction{
			ID:        uuid.NewV4().String(),
			Amount:    -1,
			Operation: pachinko.Operation_CONSUME,
		}

		ctx := context.Background()

		Convey("when we error fetching the transactions from cache", func() {
			transactionsCache.On("GetAll", ctx, reqToken).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				validationErr, err := validator.IsValid(ctx, reqToken, transaction)
				So(validationErr, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we succeed fetching the transactions from cache", func() {
			transactionID := uuid.NewV4().String()
			transactionsList := map[string]*models.Transaction{
				transactionID: {
					ID:        transactionID,
					Amount:    -3,
					Operation: pachinko.Operation_CONSUME,
				},
			}

			transactionsCache.On("GetAll", ctx, reqToken).Return(transactionsList, nil)

			Convey("when we fail to fetch token balance", func() {
				tokenGetter.On("Get", ctx, reqToken.OwnerId, reqToken.GroupId).Return(nil, errors.New("WARLUS STRIKE"))

				Convey("then we return an error", func() {
					validationErr, err := validator.IsValid(ctx, reqToken, transaction)
					So(validationErr, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the balance does not exist on a consume operation", func() {
				tokenGetter.On("Get", ctx, reqToken.OwnerId, reqToken.GroupId).Return(nil, nil)

				Convey("then we return an error", func() {
					validationErr, err := validator.IsValid(ctx, reqToken, transaction)
					So(validationErr, ShouldNotBeNil)
					So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_NO_BALANCE)
					So(validationErr.InvalidToken, ShouldResemble, &reqToken)
					So(err, ShouldBeNil)
				})
			})

			Convey("when we succeed to fetch token balance", func() {
				tokenBalance := &pachinko.Token{
					OwnerId:  ownerID,
					GroupId:  groupID,
					Domain:   domain,
					Quantity: 4,
				}

				tokenGetter.On("Get", ctx, reqToken.OwnerId, reqToken.GroupId).Return(tokenBalance, nil)

				Convey("when the sum of the existing operations and the current balance is not greater than 0", func() {
					transaction.Amount = -3

					validationErr, err := validator.IsValid(ctx, reqToken, transaction)
					So(validationErr, ShouldNotBeNil)
					So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE)
					So(validationErr.InvalidToken, ShouldResemble, &reqToken)
					So(err, ShouldBeNil)
				})

				Convey("when the balance is frozen", func() {
					tokenBalance.Status = pachinko.TokenStatus_FROZEN
					transaction.Amount = -1

					validationErr, err := validator.IsValid(ctx, reqToken, transaction)
					So(validationErr, ShouldNotBeNil)
					So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_BALANCE_FROZEN)
					So(validationErr.InvalidToken, ShouldResemble, &reqToken)
					So(err, ShouldBeNil)
				})

				Convey("when the sum of the existing operations and the current balance is greater than or equal to 0", func() {
					validationErr, err := validator.IsValid(ctx, reqToken, transaction)
					So(validationErr, ShouldBeNil)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
