package worker_pool

import (
	"errors"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestScanWorkerPool(t *testing.T) {
	Convey("given a scan worker pool", t, func() {
		pool := NewScanWorkerPool()
		jobs := []*Job{{
			PagingKey:     nil,
			Segment:       0,
			TotalSegments: 2,
		}, {
			PagingKey:     nil,
			Segment:       1,
			TotalSegments: 2,
		}}

		Convey("Execute", func() {
			Convey("When a worker completes successfully without providing leftover jobs", func() {
				worker := func(job Job) Result {
					// simulate long running task
					time.Sleep(50 * time.Millisecond)
					return Result{}
				}
				leftoverJobs, err := pool.Execute(jobs, worker, 2)
				So(err, ShouldBeNil)
				So(leftoverJobs, ShouldBeNil)
			})

			Convey("When a worker completes successfully and provides leftover jobs", func() {
				worker := func(job Job) Result {
					// simulate long running task
					time.Sleep(50 * time.Millisecond)
					return Result{NextJob: &job}
				}
				leftoverJobs, err := pool.Execute(jobs, worker, 2)
				So(err, ShouldBeNil)
				So(len(leftoverJobs), ShouldEqual, 2)
			})

			Convey("An error is returned when a worker errors", func() {
				worker := func(job Job) Result {
					// simulate long running task
					time.Sleep(50 * time.Millisecond)
					return Result{Err: errors.New("worker failed")}
				}
				leftoverJobs, err := pool.Execute(jobs, worker, 2)
				So(err, ShouldNotBeNil)
				So(leftoverJobs, ShouldBeNil)
			})

			Convey("When there are more jobs than workers", func() {
				jobs := []*Job{{
					PagingKey:     nil,
					Segment:       0,
					TotalSegments: 4,
				}, {
					PagingKey:     nil,
					Segment:       1,
					TotalSegments: 4,
				}, {
					PagingKey:     nil,
					Segment:       2,
					TotalSegments: 4,
				}, {
					PagingKey:     nil,
					Segment:       3,
					TotalSegments: 4,
				}}

				worker := func(job Job) Result {
					// simulate long running task
					time.Sleep(50 * time.Millisecond)
					return Result{}
				}
				leftoverJobs, err := pool.Execute(jobs, worker, 2)
				So(err, ShouldBeNil)
				So(leftoverJobs, ShouldBeNil)
			})
		})
	})
}
