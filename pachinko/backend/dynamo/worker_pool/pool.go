package worker_pool

type WorkerPool interface {
	Execute([]*Job, WorkerFn, int) ([]*Job, error)
}

type workerPool struct{}

func NewScanWorkerPool() WorkerPool {
	return &workerPool{}
}

func (s *workerPool) Execute(jobs []*Job, workerFn WorkerFn, workerCount int) ([]*Job, error) {
	jobCount := len(jobs)
	if jobCount == 0 {
		return nil, nil
	}

	if workerCount < 1 {
		workerCount = 1
	} else if workerCount > jobCount {
		workerCount = jobCount
	}

	var jobsChan = make(chan *Job, jobCount)
	var resultsChan = make(chan Result, jobCount)

	// create go routines that read from the jobs channel and call the provided worker
	// results go to the results channel
	for w := 0; w < workerCount; w++ {
		go func() {
			for j := range jobsChan {
				resultsChan <- workerFn(*j)
			}
		}()
	}
	// feed jobs into jobs channel
	for _, job := range jobs {
		jobsChan <- job
	}
	close(jobsChan)

	// Dynamo scans can take a long time. Result can contain a Job to indicate that the workerFn did not fully complete the job.
	// These "leftover" jobs are returned to the caller to allow it to continue where the current jobs left off.
	var leftoverJobs []*Job

	// listen for results, errors immediately return
	// Results with Jobs are returned as a list of leftover work
	for i := 0; i < jobCount; i++ {
		result := <-resultsChan

		if result.Err != nil {
			return nil, result.Err
		}

		if result.NextJob != nil {
			leftoverJobs = append(leftoverJobs, result.NextJob)
		}
	}
	close(resultsChan)

	return leftoverJobs, nil
}
