package worker_pool

import (
	"code.justin.tv/commerce/pachinko/backend/dynamo"
)

type WorkerFn func(Job) Result

type Job struct {
	PagingKey     dynamo.PagingKey
	Segment       int64
	TotalSegments int64
	Id            string
}

type Result struct {
	NextJob *Job
	Err     error
}
