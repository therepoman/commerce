package tokens

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachinko/backend/circuit"
	db "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	DBFormat = "%s-tokens-gbl-%s"

	HashKey  = "owner_id"
	RangeKey = "group_id"

	getDynamoLatency         = "get-token-dynamo-latency"
	getAllDynamoLatency      = "get-token-all-dynamo-latency"
	getAllBatchDynamoLatency = "get-token-batch-dynamo-latency"
	incrementDynamoLatency   = "inc-token-dynamo-latency"
	decrementDynamoLatency   = "dec-tokens-dynamo-latency"
	overrideDynamoLatency    = "override-token-dynamo-latency"
	setStatusDynamoLatency   = "set-token-status-dynamo-latency"
	batchDeleteDynamoLatency = "batch-delete-token-dynamo-latency"
)

type DAO interface {
	GetTokenBalance(ctx context.Context, ownerID, groupID string) (*db.Token, error)
	GetTokenBalances(ctx context.Context, ownerID string, groupIDs ...string) ([]*db.Token, error)
	GetTokenBalancesBatch(ctx context.Context, batches []dynamo.Keyed) ([]*db.Token, error)
	IncrementTokenBalance(ctx context.Context, token db.Token, additionalTransactions []db.Put) (*db.Token, error)
	DecrementTokenBalances(ctx context.Context, tokens []db.Token, additionalTransactions []db.Update) error
	SetTokenBalance(ctx context.Context, ownerID, groupID string, balance int64) (*db.Token, error)
	SetTokenStatus(ctx context.Context, token db.Token, additionalTransactions []db.Put) (*db.Token, error)
	BatchDeleteTokens(ctx context.Context, batches []dynamo.Keyed) error
}

type dao struct {
	Session *dynamo.DB
	Config  *config.Config   `inject:""`
	Statter metrics.Statter  `inject:""`
	TxDAO   transactions.DAO `inject:""`
}

func NewDAO(sess *session.Session, cfg *config.Config) DAO {
	return &dao{
		Session: dynamo.New(sess),
		Config:  cfg,
	}
}

func NewDAOWithClient(dynamoClient dynamodbiface.DynamoDBAPI) DAO {
	return &dao{
		Session: dynamo.NewFromIface(dynamoClient),
	}
}

func (d *dao) GetTokenBalance(ctx context.Context, ownerID, groupID string) (*db.Token, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	var token db.Token
	notFound := false
	err := hystrix.Do(circuit.GetTokenDynamoCommand, func() error {
		start := time.Now()
		err := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
			Get(HashKey, ownerID).
			Range(RangeKey, dynamo.Equal, groupID).
			Consistent(true).
			OneWithContext(ctx, &token)
		go d.Statter.Duration(getDynamoLatency, time.Since(start))
		if err == dynamo.ErrNotFound {
			notFound = true
			return nil
		}
		return err
	}, nil)

	if notFound {
		return nil, nil
	}
	return &token, err
}

// TODO: Make this paginated.
func (d *dao) GetTokenBalances(ctx context.Context, ownerID string, groupIDs ...string) ([]*db.Token, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}
	var tokens []*db.Token
	query := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Get(HashKey, ownerID).
		Consistent(true).
		Filter("'balance' > ?", 0)
	if len(groupIDs) > 0 {
		queryIDs := make([]interface{}, len(groupIDs))
		for i, groupID := range groupIDs {
			queryIDs[i] = groupID
		}
		query = query.Range(RangeKey, dynamo.Equal, queryIDs...)
	}

	err := hystrix.Do(circuit.GetTokensDynamoCommand, func() error {
		start := time.Now()
		err := query.AllWithContext(ctx, &tokens)
		go d.Statter.Duration(getAllDynamoLatency, time.Since(start))
		if err != nil {
			if awsError, ok := err.(awserr.Error); ok {
				// If the item was updated with lower quantity or even deleted, don't fail
				if awsError.Code() == dynamodb.ErrCodeResourceNotFoundException {
					return nil
				}
			}
		}
		return err
	}, nil)

	return tokens, err
}

// TODO: Make this paginated.
func (d *dao) GetTokenBalancesBatch(ctx context.Context, batches []dynamo.Keyed) ([]*db.Token, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}
	var tokens []*db.Token
	query := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Batch(HashKey, RangeKey).
		Get(batches...).
		Consistent(true)

	err := hystrix.Do(circuit.GetTokensBatchCommand, func() error {
		start := time.Now()
		err := query.AllWithContext(ctx, &tokens)
		go d.Statter.Duration(getAllBatchDynamoLatency, time.Since(start))
		if err != nil {
			if awsError, ok := err.(awserr.Error); ok {
				// If the item was updated with lower quantity or even deleted, don't fail
				if awsError.Code() == dynamodb.ErrCodeResourceNotFoundException {
					return nil
				}
			} else if err == dynamo.ErrNotFound {
				return nil
			}
		}
		return err
	}, nil)

	return tokens, err
}

func (d *dao) IncrementTokenBalance(ctx context.Context, token db.Token, additionalTransactions []db.Put) (*db.Token, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	tokenUpdate := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Update(HashKey, token.OwnerID).
		Range(RangeKey, token.GroupID).
		Set("latest_transaction_id", token.LatestTransactionID).
		SetExpr("balance = if_not_exists($, ?) + ?", "balance", 0, token.Balance).
		If("not 'status' = ?", db.FrozenTokenStatus)

	idempotencyToken, err := db.CreateIdempotencyToken(token.LatestTransactionID)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("failed to create idempotency token from: %s", token.LatestTransactionID))
	}
	tx := d.Session.WriteTx().IdempotentWithToken(idempotencyToken)
	for _, additionalTransaction := range additionalTransactions {
		tx.Put(additionalTransaction)
	}
	tx.Update(tokenUpdate)

	err = hystrix.Do(circuit.IncTokensCommand, func() error {
		start := time.Now()
		err := tx.RunWithContext(ctx)
		go d.Statter.Duration(incrementDynamoLatency, time.Since(start))

		return err
	}, nil)

	if err != nil {
		return nil, err
	}

	return d.GetTokenBalance(ctx, token.OwnerID, token.GroupID)
}

func (d *dao) DecrementTokenBalances(ctx context.Context, tokens []db.Token, additionalTransactions []db.Update) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform dynamo op on non configured domain")
	}

	idempotencyToken, err := db.CreateIdempotencyToken(tokens[0].LatestTransactionID)
	if err != nil {
		return errors.New(fmt.Sprintf("failed to create idempotency token from: %s", tokens[0].LatestTransactionID))
	}
	tx := d.Session.WriteTx().IdempotentWithToken(idempotencyToken)
	for _, token := range tokens {
		tx.Update(d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
			Update(HashKey, token.OwnerID).
			Range(RangeKey, token.GroupID).
			Set("latest_transaction_id", token.LatestTransactionID).
			SetExpr("balance = if_not_exists($, ?) - ?", "balance", 0, token.Balance).
			If("'balance' >= ? and not 'status' = ?", token.Balance, db.FrozenTokenStatus))

		transactionCommit, err := d.TxDAO.GetCommitTx(token.LatestTransactionID)
		if err != nil {
			return err
		}

		tx.Update(transactionCommit)
	}

	for _, additionalTransaction := range additionalTransactions {
		if additionalTransaction != nil {
			tx.Update(additionalTransaction)
		}
	}
	err = hystrix.Do(circuit.DecTokensCommand, func() error {
		start := time.Now()
		err := tx.RunWithContext(ctx)
		go d.Statter.Duration(decrementDynamoLatency, time.Since(start))

		return err
	}, nil)

	return err
}

func (d *dao) SetTokenBalance(ctx context.Context, ownerID, groupID string, balance int64) (*db.Token, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	groupUpdate := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Update(HashKey, ownerID).
		Range(RangeKey, groupID).
		Set("balance", balance)

	token := &db.Token{}
	err := hystrix.Do(circuit.SetTokenBalanceCommand, func() error {
		start := time.Now()
		err := groupUpdate.ValueWithContext(ctx, token)
		go d.Statter.Duration(overrideDynamoLatency, time.Since(start))
		return err
	}, nil)

	if err != nil {
		return nil, err
	}

	return token, err
}

func (d *dao) SetTokenStatus(ctx context.Context, token db.Token, additionalTransactions []db.Put) (*db.Token, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	tokenUpdate := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Update(HashKey, token.OwnerID).
		Range(RangeKey, token.GroupID).
		Set("latest_transaction_id", token.LatestTransactionID).
		Set("status", token.Status)

	tx := d.Session.WriteTx()
	for _, additionalTransaction := range additionalTransactions {
		tx.Put(additionalTransaction)
	}
	tx.Update(tokenUpdate)

	err := hystrix.Do(circuit.SetTokenStatusCommand, func() error {
		start := time.Now()
		err := tx.RunWithContext(ctx)
		go d.Statter.Duration(setStatusDynamoLatency, time.Since(start))

		return err
	}, nil)

	if err != nil {
		return nil, err
	}

	return d.GetTokenBalance(ctx, token.OwnerID, token.GroupID)
}

func (d *dao) BatchDeleteTokens(ctx context.Context, batches []dynamo.Keyed) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform dynamo op on non configured domain")
	}

	if len(batches) == 0 {
		return nil
	}

	query := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Batch(HashKey, RangeKey).
		Write().
		Delete(batches...)

	err := hystrix.Do(circuit.BatchDeleteTokensCommand, func() error {
		start := time.Now()
		_, err := query.RunWithContext(ctx)
		go d.Statter.Duration(batchDeleteDynamoLatency, time.Since(start))

		return err
	}, nil)

	return err
}
