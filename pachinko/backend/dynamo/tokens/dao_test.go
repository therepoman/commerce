package tokens

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/environment"
	"code.justin.tv/commerce/pachinko/config/tenant"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/gofrs/uuid"
	db "github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDao_Operations(t *testing.T) {
	Convey("given a tokens dao", t, func() {
		session := dynamo.GetAWSSession(t, "")
		statter := new(metrics_mock.Statter)
		cfg := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Environment: &environment.Config{
				DynamoSuffix: dynamo.TestTablesSuffix,
			},
			Tenants: &tenant.Wrapper{
				"sticker": &tenant.Config{
					ResourceIdentifier: "sticker",
				},
			},
		}

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		dao := &dao{
			TxDAO:   transactions.NewDAO(session, cfg, statter),
			Session: db.New(session),
			Config:  cfg,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupID1 := random.String(16)
		groupID2 := random.String(16)
		groupID3 := random.String(16)

		Convey("when a user's token balance does not exist", func() {
			balances, err := dao.GetTokenBalances(ctx, ownerID)
			So(err, ShouldBeNil)
			So(balances, ShouldHaveLength, 0)
		})

		Convey("when we add a token balance for a channel", func() {
			transactionId := uuid.Must(uuid.NewV4()).String()
			writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID1, dynamo.AddTokensOperation, 3)
			newTokenBalance, err := dao.IncrementTokenBalance(ctx, dynamo.Token{
				OwnerID:             ownerID,
				GroupID:             groupID1,
				Balance:             3,
				LatestTransactionID: transactionId,
			}, []dynamo.Put{})
			So(err, ShouldBeNil)
			So(newTokenBalance, ShouldNotBeNil)
			Convey("we should add a new item to the DB", func() {
				So(newTokenBalance.GroupID, ShouldEqual, groupID1)
				So(newTokenBalance.Balance, ShouldEqual, 3)
			})

			Convey("when we increment the channel's token balance", func() {
				transactionId := uuid.Must(uuid.NewV4()).String()
				writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID1, dynamo.AddTokensOperation, 3)
				newTokenBalance, err := dao.IncrementTokenBalance(ctx, dynamo.Token{
					OwnerID:             ownerID,
					GroupID:             groupID1,
					Balance:             3,
					LatestTransactionID: transactionId,
				}, []dynamo.Put{})
				So(err, ShouldBeNil)
				So(newTokenBalance, ShouldNotBeNil)

				Convey("we should increment the item in the DB", func() {
					So(newTokenBalance.GroupID, ShouldEqual, groupID1)
					So(newTokenBalance.Balance, ShouldEqual, 6)
				})

				Convey("when we increment another channel token balance", func() {
					transactionId := uuid.Must(uuid.NewV4()).String()
					writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID2, dynamo.AddTokensOperation, 3)
					newTokenBalance, err := dao.IncrementTokenBalance(ctx, dynamo.Token{
						OwnerID:             ownerID,
						GroupID:             groupID2,
						Balance:             3,
						LatestTransactionID: transactionId,
					}, []dynamo.Put{})
					So(err, ShouldBeNil)
					So(newTokenBalance, ShouldNotBeNil)
					Convey("we should add a new item to the DB", func() {
						So(newTokenBalance.GroupID, ShouldEqual, groupID2)
						So(newTokenBalance.Balance, ShouldEqual, 3)
					})

					Convey("when we batch get the token balances from the DB we should have 2 balances", func() {
						batches := []db.Keyed{
							db.Keys{ownerID, groupID1},
							db.Keys{ownerID, groupID2},
						}

						balances, err := dao.GetTokenBalancesBatch(ctx, batches)
						So(err, ShouldBeNil)
						So(balances, ShouldHaveLength, 2)
						So(verifyTokenBalance(balances, groupID1, 6), ShouldBeTrue)
						So(verifyTokenBalance(balances, groupID2, 3), ShouldBeTrue)
					})

					Convey("when we get the token balances from the DB we should have 2 balances", func() {
						balances, err := dao.GetTokenBalances(ctx, ownerID)
						So(err, ShouldBeNil)
						So(balances, ShouldHaveLength, 2)
						So(verifyTokenBalance(balances, groupID1, 6), ShouldBeTrue)
						So(verifyTokenBalance(balances, groupID2, 3), ShouldBeTrue)

						Convey("when we try to consume the balances", func() {
							transactionId1 := uuid.Must(uuid.NewV4()).String()
							transactionId2 := uuid.Must(uuid.NewV4()).String()
							writeSingleTokenTransaction(dao.TxDAO, transactionId1, ownerID, groupID1, dynamo.ConsumeTokensOperation, 1)
							writeSingleTokenTransaction(dao.TxDAO, transactionId2, ownerID, groupID2, dynamo.ConsumeTokensOperation, 1)

							err := dao.DecrementTokenBalances(ctx, []dynamo.Token{
								{
									OwnerID:             ownerID,
									GroupID:             groupID1,
									Balance:             1,
									LatestTransactionID: transactionId1,
								},
								{
									OwnerID:             ownerID,
									GroupID:             groupID2,
									Balance:             1,
									LatestTransactionID: transactionId2,
								},
							}, []dynamo.Update{})

							So(err, ShouldBeNil)

							Convey("the token balances should be decremented", func() {
								balances, err := dao.GetTokenBalances(ctx, ownerID)
								So(err, ShouldBeNil)
								So(balances, ShouldHaveLength, 2)
								So(verifyTokenBalance(balances, groupID1, 5), ShouldBeTrue)
								So(verifyTokenBalance(balances, groupID2, 2), ShouldBeTrue)
							})

							Convey("when we consume all the balance for a token", func() {
								transactionId := uuid.Must(uuid.NewV4()).String()
								writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID2, dynamo.ConsumeTokensOperation, 2)
								err := dao.DecrementTokenBalances(ctx, []dynamo.Token{
									{
										OwnerID:             ownerID,
										GroupID:             groupID2,
										Balance:             2,
										LatestTransactionID: transactionId,
									},
								}, []dynamo.Update{})

								So(err, ShouldBeNil)

								Convey("we should only have the other balance left", func() {
									balances, err := dao.GetTokenBalances(ctx, ownerID)
									So(err, ShouldBeNil)
									So(balances, ShouldHaveLength, 1)
									So(verifyTokenBalance(balances, groupID1, 5), ShouldBeTrue)
								})

								Convey("when we try to consume a non existent token we should error", func() {
									transactionId := uuid.Must(uuid.NewV4()).String()
									writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID2, dynamo.ConsumeTokensOperation, 2)

									err := dao.DecrementTokenBalances(ctx, []dynamo.Token{
										{
											OwnerID:             ownerID,
											GroupID:             groupID2,
											Balance:             2,
											LatestTransactionID: transactionId,
										},
									}, []dynamo.Update{})

									So(err, ShouldNotBeNil)
								})

								Convey("when we try to consume a more tokens than we have we error", func() {
									transactionId := uuid.Must(uuid.NewV4()).String()
									writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID1, dynamo.ConsumeTokensOperation, 9000)
									err := dao.DecrementTokenBalances(ctx, []dynamo.Token{
										{
											OwnerID:             ownerID,
											GroupID:             groupID1,
											Balance:             9000,
											LatestTransactionID: transactionId,
										},
									}, []dynamo.Update{})

									So(err, ShouldNotBeNil)
								})

								Convey("when we create another token", func() {
									transactionId := uuid.Must(uuid.NewV4()).String()
									writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID1, dynamo.AddTokensOperation, 3)
									newTokenBalance, err := dao.IncrementTokenBalance(ctx, dynamo.Token{
										OwnerID:             ownerID,
										GroupID:             groupID3,
										Balance:             3,
										LatestTransactionID: transactionId,
									}, []dynamo.Put{})
									So(err, ShouldBeNil)
									So(newTokenBalance, ShouldNotBeNil)
									Convey("we should add a new item to the DB", func() {
										So(newTokenBalance.GroupID, ShouldEqual, groupID3)
										So(newTokenBalance.Balance, ShouldEqual, 3)
									})

									Convey("when we freeze the balance", func() {
										transactionId := uuid.Must(uuid.NewV4()).String()
										writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID3, dynamo.UpdateStatusOperation, 0)
										token, err := dao.SetTokenStatus(ctx, dynamo.Token{
											OwnerID:             ownerID,
											GroupID:             groupID3,
											LatestTransactionID: transactionId,
											Status:              dynamo.FrozenTokenStatus,
										}, []dynamo.Put{})
										So(err, ShouldBeNil)
										So(token, ShouldNotBeNil)
										So(token.Balance, ShouldEqual, 3)
										So(token.Status, ShouldEqual, dynamo.FrozenTokenStatus)
										So(token.OwnerID, ShouldEqual, ownerID)
										So(token.GroupID, ShouldEqual, groupID3)

										Convey("increment should fail", func() {
											transactionId := uuid.Must(uuid.NewV4()).String()
											_, err := dao.IncrementTokenBalance(ctx, dynamo.Token{
												OwnerID:             ownerID,
												GroupID:             groupID3,
												Balance:             5,
												LatestTransactionID: transactionId,
											}, []dynamo.Put{})

											So(err, ShouldNotBeNil)
										})

										Convey("decrement should fail", func() {
											transactionId := uuid.Must(uuid.NewV4()).String()
											err := dao.DecrementTokenBalances(ctx, []dynamo.Token{
												{
													OwnerID:             ownerID,
													GroupID:             groupID3,
													Balance:             5,
													LatestTransactionID: transactionId,
												},
											}, []dynamo.Update{})

											So(err, ShouldNotBeNil)
										})
									})
								})

								Convey("when we consume the rest of our tokens", func() {
									transactionId := uuid.Must(uuid.NewV4()).String()
									writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID1, dynamo.ConsumeTokensOperation, 5)
									err := dao.DecrementTokenBalances(ctx, []dynamo.Token{
										{
											OwnerID:             ownerID,
											GroupID:             groupID1,
											Balance:             5,
											LatestTransactionID: transactionId,
										},
									}, []dynamo.Update{})

									So(err, ShouldBeNil)

									Convey("we should have an empty balance list", func() {
										balances, err := dao.GetTokenBalances(ctx, ownerID)
										So(err, ShouldBeNil)
										So(balances, ShouldHaveLength, 0)
									})
								})
							})
						})
					})
				})
			})
		})

		Convey("#BatchDeleteTokens", func() {
			transactionId := uuid.Must(uuid.NewV4()).String()
			ownerID := random.String(16)
			groupID1 := random.String(16)
			batches := []db.Keyed{db.Keys{ownerID, groupID1}}

			Convey("returns an nil when batches is length 0", func() {
				batches = []db.Keyed{}
				err := dao.BatchDeleteTokens(context.Background(), batches)
				So(err, ShouldBeNil)
			})

			Convey("returns nil when we delete the transactions", func() {
				writeSingleTokenTransaction(dao.TxDAO, transactionId, ownerID, groupID1, dynamo.AddTokensOperation, 3)
				token, err := dao.IncrementTokenBalance(ctx, dynamo.Token{
					OwnerID:             ownerID,
					GroupID:             groupID1,
					Balance:             3,
					LatestTransactionID: transactionId,
				}, []dynamo.Put{})
				So(err, ShouldBeNil)
				So(token, ShouldNotBeNil)

				balances, err := dao.GetTokenBalances(ctx, ownerID)
				So(err, ShouldBeNil)
				So(balances, ShouldHaveLength, 1)

				err = dao.BatchDeleteTokens(context.Background(), batches)
				So(err, ShouldBeNil)

				balances, err = dao.GetTokenBalances(ctx, ownerID)
				So(err, ShouldBeNil)
				So(balances, ShouldHaveLength, 0)
			})
		})
	})
}

func verifyTokenBalance(tokens []*dynamo.Token, groupID string, expectedBalance int) bool {
	for _, token := range tokens {
		if token.GroupID == groupID {
			if expectedBalance == token.Balance {
				return true
			} else {
				logrus.WithField("groupID", groupID).WithField("expected", expectedBalance).WithField("actual", token.Balance).Error("quantities not the same")
			}
		}
	}
	return false
}

func writeSingleTokenTransaction(txDAO transactions.DAO, transactionId, ownerId, groupId string, operation dynamo.Operation, quantity int64) {
	err := txDAO.Write(context.Background(), dynamo.Transaction{
		ID:        transactionId,
		OwnerID:   ownerId,
		Operation: operation,
		Timestamp: time.Now(),
		Tokens: []*dynamo.THToken{
			{
				OwnerId:  ownerId,
				GroupId:  groupId,
				Domain:   dynamo.TwirpDomain(pachinko.Domain_STICKER),
				Quantity: quantity,
			},
		},
	})
	So(err, ShouldBeNil)
}
