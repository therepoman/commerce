package transactions_scan

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	db "code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/environment"
	"code.justin.tv/commerce/pachinko/config/tenant"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDao_ScanTransactions(t *testing.T) {
	Convey("given a transactions dao", t, func() {
		session := dynamo.GetAWSSession(t, "us-east-2")
		statter := new(metrics_mock.Statter)
		resourceId := "sticker"
		statter.On("Duration", mock.Anything, mock.Anything).Return()

		dynamoDB := db.DynamoScanClient(session)

		dao := &dao{
			Dynamo: dynamoDB,
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
				Environment: &environment.Config{
					DynamoSuffix: dynamo.TestTablesSuffix,
				},
				Tenants: &tenant.Wrapper{
					"sticker": &tenant.Config{
						ResourceIdentifier: "sticker",
					},
				},
			},
			Statter: statter,
		}

		Convey("Creates a new transaction", func() {
			table := fmt.Sprintf(DBFormat, resourceId, dynamo.TestTablesSuffix)
			transactionID := uuid.Must(uuid.NewV4()).String()
			userID := random.String(16)
			channelID := random.String(16)
			quantity := int64(3)
			domain := dynamo.TwirpDomain(pachinko.Domain_STICKER)

			transaction := dynamo.Transaction{
				ID:        transactionID,
				OwnerID:   userID,
				Operation: dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*dynamo.THToken{
					{
						GroupId:  channelID,
						Domain:   domain,
						Quantity: quantity,
						OwnerId:  userID,
					},
				},
				IsCommitted: true,
			}
			err := writeSingleTransaction(dynamoDB, transaction, table)
			So(err, ShouldBeNil)

			// check it
			input := &dynamodb.GetItemInput{
				TableName: aws.String(table),
				Key: map[string]*dynamodb.AttributeValue{
					"transaction_id": {
						S: aws.String(transactionID),
					},
				},
			}
			output, err := dynamoDB.GetItem(input)
			So(err, ShouldBeNil)
			var checkedTransaction dynamo.Transaction
			err = dynamodbattribute.UnmarshalMap(output.Item, &checkedTransaction)
			So(checkedTransaction, ShouldNotBeNil)
			So(err, ShouldBeNil)

			Convey("scans and returns Transactions with a paging key", func() {
				scanSearchLimit := int64(5)
				transactions, _, err := dao.ScanTransactions(context.Background(), scanSearchLimit, nil, 0, 1)
				So(err, ShouldBeNil)
				So(transactions, ShouldNotBeNil)
				So(len(transactions), ShouldEqual, scanSearchLimit)
			})
		})
	})
}

func writeSingleTransaction(dynamoDB dynamodbiface.DynamoDBAPI, transaction dynamo.Transaction, table string) error {
	av, err := dynamodbattribute.MarshalMap(transaction)
	if err != nil {
		return err
	}

	item := &dynamodb.PutItemInput{
		Item:      av,
		TableName: &table,
	}

	_, err = dynamoDB.PutItem(item)
	return err
}
