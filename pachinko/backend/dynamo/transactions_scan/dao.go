package transactions_scan

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachinko/backend/circuit"
	db "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

const (
	DBFormat          = "%s-tx-gbl-%s"
	scanDynamoLatency = "scan-transactions-dynamo-latency"
)

type DAO interface {
	ScanTransactions(ctx context.Context, limit int64, pagingKey db.PagingKey, segment int64, totalSegments int64) ([]db.Transaction, db.PagingKey, error)
}

type dao struct {
	Dynamo  dynamodbiface.DynamoDBAPI
	Config  *config.Config  `inject:""`
	Statter metrics.Statter `inject:""`
}

func NewDAOWithClient(dynamoClient dynamodbiface.DynamoDBAPI) DAO {
	return &dao{
		Dynamo: dynamoClient,
	}
}

func (d *dao) ScanTransactions(ctx context.Context, limit int64, pagingKey db.PagingKey, segment int64, totalSegments int64) ([]db.Transaction, db.PagingKey, error) {
	tenantConfig := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConfig == nil {
		return nil, nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	input := &dynamodb.ScanInput{
		TableName:         aws.String(fmt.Sprintf(DBFormat, tenantConfig.ResourceIdentifier, d.Config.Environment.DynamoSuffix)),
		Limit:             &limit,
		ExclusiveStartKey: pagingKey,
		Segment:           &segment,
		TotalSegments:     &totalSegments,
	}

	var output *dynamodb.ScanOutput
	var results []db.Transaction
	var err error
	err = hystrix.Do(circuit.ScanTransactionsCommand, func() error {
		start := time.Now()
		output, err = d.Dynamo.Scan(input)
		go d.Statter.Duration(scanDynamoLatency, time.Since(start))
		return err
	}, nil)

	if err != nil {
		return nil, nil, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &results)
	if err != nil {
		return nil, nil, err
	}

	return results, output.LastEvaluatedKey, err
}
