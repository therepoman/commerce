package dynamo

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateIdempotencyToken(t *testing.T) {
	Convey("testing create idempotency token function", t, func() {
		Convey("should create size less than max size allowed by DDB", func() {
			for i := 0; i < 1000; i++ {
				key := random.String(i)
				token, err := CreateIdempotencyToken(key)
				So(err, ShouldBeNil)
				So(len(token), ShouldBeLessThanOrEqualTo, 35)
			}
		})

		Convey("should create the same key every time", func() {
			key := "foobarbaz"
			refToken, err := CreateIdempotencyToken(key)
			So(err, ShouldBeNil)
			for i := 0; i < 1000; i++ {
				token, err := CreateIdempotencyToken(key)
				So(err, ShouldBeNil)
				So(token, ShouldEqual, refToken)
			}
		})
	})
}

func TestToTwirpTransactionRecord(t *testing.T) {
	Convey("testing to twirp transaction record function", t, func() {
		Convey("should create size less than max size allowed by DDB", func() {
			now := time.Now()

			transaction := Transaction{
				Timestamp: now,
			}

			twirpTransaction := transaction.ToTwirpTransactionRecord()
			So(twirpTransaction, ShouldNotBeNil)

			So(twirpTimestampToGo(twirpTransaction.Timestamp), ShouldEqual, now)
		})
	})
}

func twirpTimestampToGo(timestamp *timestamp.Timestamp) time.Time {
	convertedTime, _ := ptypes.Timestamp(timestamp)
	return convertedTime
}
