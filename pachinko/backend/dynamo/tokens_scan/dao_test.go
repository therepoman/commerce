package tokens_scan

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	dbClient "code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/environment"
	"code.justin.tv/commerce/pachinko/config/tenant"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDao_Operations(t *testing.T) {
	Convey("given a tokens dao", t, func() {
		session := dynamo.GetAWSSession(t, "us-east-2")
		statter := new(metrics_mock.Statter)
		resourceId := "sticker"
		cfg := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Environment: &environment.Config{
				DynamoSuffix: dynamo.TestTablesSuffix,
			},
			Tenants: &tenant.Wrapper{
				"sticker": &tenant.Config{
					ResourceIdentifier: resourceId,
				},
			},
		}
		dynamoDB := dbClient.DynamoScanClient(session)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		dao := &dao{
			Dynamo:  dynamoDB,
			Config:  cfg,
			Statter: statter,
		}

		Convey("#ScanTokens", func() {
			transactionId := random.String(16)
			ownerID := random.String(16)
			groupID1 := random.String(16)
			err := createToken(dynamoDB, resourceId, dynamo.Token{
				OwnerID:             ownerID,
				GroupID:             groupID1,
				Balance:             3,
				LatestTransactionID: transactionId,
			})
			So(err, ShouldBeNil)

			Convey("scans and returns Tokens with a paging key", func() {
				scanSearchLimit := int64(5)
				tokens, _, err := dao.ScanTokens(context.Background(), scanSearchLimit, nil, 0, 1)
				So(err, ShouldBeNil)
				So(tokens, ShouldNotBeNil)
				So(len(tokens), ShouldBeLessThanOrEqualTo, scanSearchLimit)
			})
		})
	})
}

func createToken(dynamoDB dynamodbiface.DynamoDBAPI, resourceId string, token dynamo.Token) error {
	av, err := dynamodbattribute.MarshalMap(token)
	if err != nil {
		return err
	}
	table := fmt.Sprintf(DBFormat, resourceId, dynamo.TestTablesSuffix)

	update := &dynamodb.PutItemInput{
		Item:      av,
		TableName: &table,
	}
	_, err = dynamoDB.PutItem(update)
	return err
}
