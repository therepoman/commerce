package tokens_scan

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachinko/backend/circuit"
	db "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

const (
	DBFormat                         = "%s-tokens-gbl-%s"
	GroupIdGSIFormat                 = "%s-token-by-group-id-gsi-%s"
	scanDynamoLatency                = "scan-token-dynamo-latency"
	queryTokenByGroupIdDynamoLatency = "query-token-by-groupId-dynamo-latency"
)

type DAO interface {
	ScanTokens(ctx context.Context, limit int64, pagingKey db.PagingKey, segment int64, totalSegments int64) ([]db.Token, db.PagingKey, error)
	QueryTokensByGroupId(ctx context.Context, limit int64, pagingKey db.PagingKey, groupId string) ([]db.Token, db.PagingKey, error)
}

type dao struct {
	Dynamo  dynamodbiface.DynamoDBAPI
	Config  *config.Config  `inject:""`
	Statter metrics.Statter `inject:""`
}

func NewDAOWithClient(dynamoClient dynamodbiface.DynamoDBAPI) DAO {
	return &dao{
		Dynamo: dynamoClient,
	}
}

func (d *dao) ScanTokens(ctx context.Context, limit int64, pagingKey db.PagingKey, segment int64, totalSegments int64) ([]db.Token, db.PagingKey, error) {
	tenantConfig := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConfig == nil {
		return nil, nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	input := &dynamodb.ScanInput{
		TableName:         aws.String(fmt.Sprintf(DBFormat, tenantConfig.ResourceIdentifier, d.Config.Environment.DynamoSuffix)),
		Limit:             &limit,
		ExclusiveStartKey: pagingKey,
		Segment:           &segment,
		TotalSegments:     &totalSegments,
	}

	var output *dynamodb.ScanOutput
	var results []db.Token
	var err error
	err = hystrix.Do(circuit.ScanTokensCommand, func() error {
		start := time.Now()
		output, err = d.Dynamo.Scan(input)
		go d.Statter.Duration(scanDynamoLatency, time.Since(start))
		return err
	}, nil)

	if err != nil {
		return nil, nil, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &results)
	if err != nil {
		return nil, nil, err
	}

	return results, output.LastEvaluatedKey, err
}

func (d *dao) QueryTokensByGroupId(ctx context.Context, limit int64, pagingKey db.PagingKey, groupId string) ([]db.Token, db.PagingKey, error) {
	tenantConfig := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConfig == nil {
		return nil, nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	condition := "group_id = :groupId"
	input := &dynamodb.QueryInput{
		TableName:                 aws.String(fmt.Sprintf(DBFormat, tenantConfig.ResourceIdentifier, d.Config.Environment.DynamoSuffix)),
		IndexName:                 aws.String(fmt.Sprintf(GroupIdGSIFormat, tenantConfig.ResourceIdentifier, d.Config.Environment.DynamoSuffix)),
		Limit:                     &limit,
		ExclusiveStartKey:         pagingKey,
		KeyConditionExpression:    &condition,
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{":groupId": {S: &groupId}},
	}

	var output *dynamodb.QueryOutput
	var results []db.Token
	var err error
	err = hystrix.Do(circuit.QueryTokensByGroupIdCommand, func() error {
		start := time.Now()
		output, err = d.Dynamo.Query(input)
		go d.Statter.Duration(queryTokenByGroupIdDynamoLatency, time.Since(start))
		return err
	}, nil)

	if err != nil {
		return nil, nil, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &results)
	if err != nil {
		return nil, nil, err
	}

	return results, output.LastEvaluatedKey, err
}
