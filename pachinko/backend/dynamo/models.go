package dynamo

import (
	"crypto/sha1"
	"encoding/base64"
	"time"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/golang/protobuf/ptypes"
	"github.com/guregu/dynamo"
)

type Operation string
type TokenStatus string
type PagingKey map[string]*dynamodb.AttributeValue

const (
	AddTokensOperation     = Operation("AddTokens")
	ConsumeTokensOperation = Operation("ConsumeTokens")
	// Deprecated: holds no longer supported
	HoldTokensOperation = Operation("HoldTokens")
	// Deprecated: holds no longer supported
	FinalizeHoldOperation = Operation("FinalizeHold")
	// Deprecated: holds no longer supported
	ReleaseHoldOperation  = Operation("ReleaseHold")
	UpdateStatusOperation = Operation("UpdateStatus")
	UnknownOperation      = Operation("Unknown")

	NormalTokenStatus = TokenStatus("NORMAL")
	FrozenTokenStatus = TokenStatus("FROZEN")
)

type Transaction struct {
	ID          string     `dynamo:"transaction_id" dynamodbav:"transaction_id"`
	OwnerID     string     `dynamo:"owner_id" dynamodbav:"owner_id"`
	Operation   Operation  `dynamo:"operation" dynamodbav:"operation"`
	OperandID   string     `dynamo:"operand_id" dynamodbav:"operand_id"`
	Timestamp   time.Time  `dynamo:"timestamp" dynamodbav:"timestamp"`
	Tokens      []*THToken `dynamo:"tokens" dynamodbav:"tokens"`
	IsCommitted bool       `dynamo:"is_committed" dynamodbav:"is_committed"`
	TTL         int64      `dynamo:"ttl" dynamodbav:"ttl"`
}

type TwirpDomain int32
type TwirpTokenStatus int32

// We use this THToken in the Transactions types that replaces the previously used Twirp-generated Token.
// This replacement was done to be able to add dynamo attributes to the fields, which is impossible via Twirp.
// Ideally, we would've used the Token type defined below for the replacement,
// however this is not possible (without a backfill) since it has different fields (Balance instead of Quantity).
type THToken struct {
	OwnerId  string           `dynamo:"OwnerId" dynamodbav:"OwnerId"`
	GroupId  string           `dynamo:"GroupId" dynamodbav:"GroupId"`
	Domain   TwirpDomain      `dynamo:"Domain" dynamodbav:"Domain"`
	Quantity int64            `dynamo:"Quantity" dynamodbav:"Quantity"`
	Status   TwirpTokenStatus `dynamo:"Status" dynamodbav:"Status"`
}

func (t *THToken) ToTwirpToken() *pachinko.Token {
	return &pachinko.Token{
		OwnerId:  t.OwnerId,
		GroupId:  t.GroupId,
		Domain:   pachinko.Domain(t.Domain),
		Quantity: t.Quantity,
		Status:   pachinko.TokenStatus(t.Status),
	}
}

func ToTwirpTokens(tokens []*THToken) []*pachinko.Token {
	twirpTokens := make([]*pachinko.Token, 0)
	for _, token := range tokens {
		twirpTokens = append(twirpTokens, token.ToTwirpToken())
	}
	return twirpTokens
}

func ToTHToken(token *pachinko.Token) *THToken {
	return &THToken{
		OwnerId:  token.OwnerId,
		Quantity: token.Quantity,
		GroupId:  token.GroupId,
		Status:   TwirpTokenStatus(token.Status),
		Domain:   TwirpDomain(token.Domain),
	}
}

func ToTHTokens(tokens []*pachinko.Token) []*THToken {
	twirpTokens := make([]*THToken, 0)
	for _, token := range tokens {
		twirpTokens = append(twirpTokens, ToTHToken(token))
	}
	return twirpTokens
}

func ToDynamoOperation(op pachinko.Operation) Operation {
	switch op {
	case pachinko.Operation_CONSUME:
		return ConsumeTokensOperation
	case pachinko.Operation_CREATE_HOLD:
		return HoldTokensOperation
	case pachinko.Operation_FINALIZE_HOLD:
		return FinalizeHoldOperation
	case pachinko.Operation_RELEASE_HOLD:
		return ReleaseHoldOperation
	}
	return UnknownOperation
}

type Put *dynamo.Put
type Update *dynamo.Update

type Token struct {
	OwnerID             string      `dynamo:"owner_id" dynamodbav:"owner_id"`
	GroupID             string      `dynamo:"group_id" dynamodbav:"group_id"`
	Balance             int         `dynamo:"balance" dynamodbav:"balance"`
	LatestTransactionID string      `dynamo:"latest_transaction_id" dynamodbav:"latest_transaction_id"`
	Status              TokenStatus `dynamo:"status" dynamodbav:"status"`
}

func (t *Token) ToTwirpToken(domain pachinko.Domain) pachinko.Token {
	var status pachinko.TokenStatus
	switch t.Status {
	case TokenStatus(pachinko.TokenStatus_FROZEN.String()):
		status = pachinko.TokenStatus_FROZEN
	default:
		status = pachinko.TokenStatus_NORMAL
	}

	return pachinko.Token{
		OwnerId:  t.OwnerID,
		GroupId:  t.GroupID,
		Domain:   domain,
		Quantity: int64(t.Balance),
		Status:   status,
	}
}

func (t *Transaction) ToTwirpTransactionRecord() *pachinko.TransactionRecord {
	timestamp, _ := ptypes.TimestampProto(t.Timestamp)

	return &pachinko.TransactionRecord{
		Id:          t.ID,
		OwnerId:     t.OwnerID,
		Timestamp:   timestamp,
		Tokens:      ToTwirpTokens(t.Tokens),
		Operation:   string(t.Operation),
		IsCommitted: t.IsCommitted,
	}
}

func ToDynamoToken(token pachinko.Token) Token {
	return Token{
		OwnerID: token.OwnerId,
		Balance: int(token.Quantity),
		GroupID: token.GroupId,
		Status:  ToDynamoTokenStatus(token.Status),
	}
}

func ToDynamoTokenStatus(tokenStatus pachinko.TokenStatus) TokenStatus {
	switch tokenStatus {
	case pachinko.TokenStatus_FROZEN:
		return FrozenTokenStatus
	}
	return NormalTokenStatus
}

func CreateIdempotencyToken(key string) (string, error) {
	hasher := sha1.New()
	_, err := hasher.Write([]byte(key))
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil)), nil
}
