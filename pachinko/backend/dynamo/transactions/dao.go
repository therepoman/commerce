package transactions

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachinko/backend/circuit"
	pachinko_dynamo "code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	ScanSearchLimit = 500

	DBFormat = "%s-tx-gbl-%s"
	PageSize = 10

	HashKey = "transaction_id"

	DefaultTTLDays = 30

	getDynamoLatency         = "get-transaction-dynamo-latency"
	batchGetDynamoLatency    = "batch-get-transaction-dynamo-latency"
	createNewDynamoLatency   = "create-new-transaction-dynamo-latency"
	createDynamoLatency      = "create-transaction-dynamo-latency"
	batchDeleteDynamoLatency = "batch-delete-transactions-dynamo-latency"
	batchPutDynamoLatency    = "batch-put-transactions-dynamo-latency"
)

type DAO interface {
	GetCommitTx(transactionID string) (*dynamo.Update, error)
	GetTransactionRecord(ctx context.Context, transactionID string) (*pachinko_dynamo.Transaction, error)
	BatchGetTransactionRecords(ctx context.Context, transactionIDs []string) ([]pachinko_dynamo.Transaction, error)
	GetWriteTransactionTx(transaction pachinko_dynamo.Transaction) (*dynamo.Put, error)
	WriteNew(ctx context.Context, transaction pachinko_dynamo.Transaction) error
	Write(ctx context.Context, transaction pachinko_dynamo.Transaction) error
	BatchPutTransactions(ctx context.Context, transactions []pachinko_dynamo.Transaction) error
	BatchDeleteTransactions(ctx context.Context, batches []dynamo.Keyed) error
}

type dao struct {
	Session *dynamo.DB
	Config  *config.Config  `inject:""`
	Statter metrics.Statter `inject:""`
}

func NewDAO(sess *session.Session, cfg *config.Config, statter metrics.Statter) DAO {
	return &dao{
		Session: dynamo.New(sess),
		Config:  cfg,
		Statter: statter,
	}
}

func NewDAOWithClient(dynamoClient dynamodbiface.DynamoDBAPI) DAO {
	return &dao{
		Session: dynamo.NewFromIface(dynamoClient),
	}
}

func (d *dao) GetTransactionRecord(ctx context.Context, transactionID string) (*pachinko_dynamo.Transaction, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	var transaction pachinko_dynamo.Transaction
	notFound := false
	err := hystrix.Do(circuit.GetTransactionRecordCommand, func() error {
		start := time.Now()
		err := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
			Get(HashKey, transactionID).Consistent(true).OneWithContext(ctx, &transaction)
		go d.Statter.Duration(getDynamoLatency, time.Since(start))
		if err == dynamo.ErrNotFound {
			notFound = true
			return nil
		}
		return err
	}, nil)

	if notFound {
		return nil, nil
	}
	return &transaction, err
}

func (d *dao) BatchGetTransactionRecords(ctx context.Context, transactionIDs []string) ([]pachinko_dynamo.Transaction, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	var transactions []pachinko_dynamo.Transaction
	var keys []dynamo.Keyed
	for _, transactionID := range transactionIDs {
		keys = append(keys, dynamo.Keys{transactionID})
	}

	err := hystrix.Do(circuit.BatchGetTransactionRecordsCommand, func() error {
		start := time.Now()
		err := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
			Batch(HashKey).
			Get(keys...).
			AllWithContext(ctx, &transactions)
		go d.Statter.Duration(batchGetDynamoLatency, time.Since(start))
		if err != nil {
			if awsError, ok := err.(awserr.Error); ok {
				if awsError.Code() == dynamodb.ErrCodeResourceNotFoundException {
					return nil
				}
			} else if err == dynamo.ErrNotFound {
				return nil
			}
		}
		return err
	}, nil)

	return transactions, err
}

func (d *dao) BatchDeleteTransactions(ctx context.Context, batches []dynamo.Keyed) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform dynamo op on non configured domain")
	}

	if len(batches) == 0 {
		return nil
	}

	query := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Batch(HashKey).
		Write().
		Delete(batches...)

	return hystrix.Do(circuit.BatchDeleteTransactionsCommand, func() error {
		start := time.Now()
		_, err := query.RunWithContext(ctx)
		go d.Statter.Duration(batchDeleteDynamoLatency, time.Since(start))
		return err
	}, nil)
}

func (d *dao) BatchPutTransactions(ctx context.Context, transactions []pachinko_dynamo.Transaction) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform dynamo op on non configured domain")
	}

	var ttlValue = DefaultTTLDays
	if tenantConf.TTL != nil && tenantConf.TTL.TransactionTableDays > 0 {
		ttlValue = tenantConf.TTL.TransactionTableDays
	}

	for i := range transactions {
		// this assumes the transaction is having it's time set to time.Now() or close
		transactions[i].TTL = transactions[i].Timestamp.AddDate(0, 0, ttlValue).Unix()
	}

	if len(transactions) == 0 {
		return nil
	}

	items := make([]interface{}, len(transactions))
	for i, transaction := range transactions {
		items[i] = transaction
	}

	query := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Batch(HashKey).
		Write().
		Put(items...)

	return hystrix.Do(circuit.BatchPutTransactionsCommand, func() error {
		start := time.Now()
		_, err := query.RunWithContext(ctx)
		go d.Statter.Duration(batchPutDynamoLatency, time.Since(start))
		return err
	}, nil)
}

func (d *dao) GetWriteTransactionTx(transaction pachinko_dynamo.Transaction) (*dynamo.Put, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	var ttlValue = DefaultTTLDays
	if tenantConf.TTL != nil && tenantConf.TTL.TransactionTableDays > 0 {
		ttlValue = tenantConf.TTL.TransactionTableDays
	}

	// this assumes the transaction is having it's time set to time.Now() or close
	transaction.TTL = transaction.Timestamp.AddDate(0, 0, ttlValue).Unix()

	return d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Put(&transaction).If("attribute_not_exists(transaction_id) OR is_committed = ?", false), nil
}

func (d *dao) WriteNew(ctx context.Context, transaction pachinko_dynamo.Transaction) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform dynamo op on non configured domain")
	}

	var ttlValue = DefaultTTLDays
	if tenantConf.TTL != nil && tenantConf.TTL.TransactionTableDays > 0 {
		ttlValue = tenantConf.TTL.TransactionTableDays
	}

	// this assumes the transaction is having it's time set to time.Now() or close
	transaction.TTL = transaction.Timestamp.AddDate(0, 0, ttlValue).Unix()

	return hystrix.Do(circuit.WriteNewTransactionCommand, func() error {
		start := time.Now()
		err := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
			Put(&transaction).If("attribute_not_exists(transaction_id)").RunWithContext(ctx)
		go d.Statter.Duration(createNewDynamoLatency, time.Since(start))
		return err
	}, nil)
}

func (d *dao) Write(ctx context.Context, transaction pachinko_dynamo.Transaction) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform dynamo op on non configured domain")
	}

	var ttlValue = DefaultTTLDays
	if tenantConf.TTL != nil && tenantConf.TTL.TransactionTableDays > 0 {
		ttlValue = tenantConf.TTL.TransactionTableDays
	}

	// this assumes the transaction is having it's time set to time.Now() or close
	transaction.TTL = transaction.Timestamp.AddDate(0, 0, ttlValue).Unix()

	return hystrix.Do(circuit.WriteTransactionCommand, func() error {
		start := time.Now()
		err := d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).Put(&transaction).RunWithContext(ctx)
		go d.Statter.Duration(createDynamoLatency, time.Since(start))
		return err
	}, nil)
}

func (d *dao) GetCommitTx(transactionID string) (*dynamo.Update, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform dynamo op on non configured domain")
	}

	return d.Session.Table(fmt.Sprintf(DBFormat, tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Update(HashKey, transactionID).
		If("attribute_exists(transaction_id) AND is_committed = ?", false).
		Set("is_committed", true), nil
}
