package transactions

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/environment"
	"code.justin.tv/commerce/pachinko/config/tenant"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	db "github.com/guregu/dynamo"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func setupDao(t *testing.T) DAO {
	session := dynamo.GetAWSSession(t, "")
	statter := new(metrics_mock.Statter)

	statter.On("Duration", mock.Anything, mock.Anything).Return()

	return &dao{
		Session: db.New(session),
		Config: &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Environment: &environment.Config{
				DynamoSuffix: dynamo.TestTablesSuffix,
			},
			Tenants: &tenant.Wrapper{
				"sticker": &tenant.Config{
					ResourceIdentifier: "sticker",
				},
			},
		},
		Statter: statter,
	}
}

func TestDao_Write(t *testing.T) {
	Convey("given a transactions dao", t, func() {
		ctx := context.Background()
		dao := setupDao(t)

		transactionID := fmt.Sprintf("test-%s", uuid.NewV4().String())
		userID := random.String(16)
		channelID := random.String(16)
		quantity := int64(3)
		domain := dynamo.TwirpDomain(pachinko.Domain_STICKER)

		transaction := &dynamo.Transaction{
			ID:        transactionID,
			OwnerID:   userID,
			Timestamp: time.Now(),
			Operation: "TEST",
			Tokens: []*dynamo.THToken{
				{
					GroupId:  channelID,
					Domain:   domain,
					Quantity: quantity,
					OwnerId:  userID,
				},
			},
		}

		// Should write transaction
		err := dao.Write(ctx, *transaction)
		So(err, ShouldBeNil)

		// Should read transaction correctly
		transaction, err = dao.GetTransactionRecord(ctx, transactionID)
		So(err, ShouldBeNil)
		So(transaction, ShouldNotBeNil)
		So(transaction.OwnerID, ShouldEqual, userID)
		So(transaction.Tokens, ShouldHaveLength, 1)
		So(transaction.Tokens[0].GroupId, ShouldEqual, channelID)
		So(transaction.Tokens[0].Quantity, ShouldEqual, quantity)
		So(transaction.Tokens[0].Domain, ShouldEqual, domain)

		// Should error when there's an existing transaction and we try to write it as new
		err = dao.WriteNew(ctx, *transaction)
		So(err, ShouldNotBeNil)
		So(err.(awserr.Error).Code(), ShouldEqual, dynamodb.ErrCodeConditionalCheckFailedException)
	})
}

func TestDao_GetWriteTransactionTx(t *testing.T) {
	ctx := context.Background()
	Convey("given a transactions dao", t, func() {
		dao := setupDao(t)

		transactionID := fmt.Sprintf("test-%s", uuid.NewV4().String())
		userID := random.String(16)
		channelID := random.String(16)
		quantity := int64(3)
		domain := dynamo.TwirpDomain(pachinko.Domain_STICKER)

		// create it
		put, err := dao.GetWriteTransactionTx(dynamo.Transaction{
			ID:        transactionID,
			OwnerID:   userID,
			Operation: dynamo.AddTokensOperation,
			Timestamp: time.Now(),
			Tokens: []*dynamo.THToken{
				{
					GroupId:  channelID,
					Domain:   domain,
					Quantity: quantity,
					OwnerId:  userID,
				},
			},
			IsCommitted: true,
		})
		So(err, ShouldBeNil)
		So(put, ShouldNotBeNil)

		// run it
		err = put.RunWithContext(ctx)
		So(err, ShouldBeNil)

		// check it
		transaction, err := dao.GetTransactionRecord(ctx, transactionID)
		So(err, ShouldBeNil)

		So(transaction.ID, ShouldEqual, transactionID)
		So(transaction.OwnerID, ShouldEqual, userID)
		So(transaction.Operation, ShouldEqual, dynamo.AddTokensOperation)
		So(transaction.Timestamp, ShouldNotBeNil)
		So(transaction.Tokens, ShouldHaveLength, 1)
		So(transaction.Tokens[0].GroupId, ShouldEqual, channelID)
		So(transaction.Tokens[0].Domain, ShouldEqual, domain)
		So(transaction.Tokens[0].Quantity, ShouldEqual, quantity)
		So(transaction.Tokens[0].OwnerId, ShouldEqual, userID)
	})
}

func TestDao_BatchDeleteTransactions(t *testing.T) {
	ctx := context.Background()
	Convey("given a transactions dao", t, func() {
		dao := setupDao(t)

		transactionID := fmt.Sprintf("test-%s", uuid.NewV4().String())
		userID := random.String(16)
		channelID := random.String(16)
		quantity := int64(3)
		domain := dynamo.TwirpDomain(pachinko.Domain_STICKER)
		batches := []db.Keyed{db.Keys{transactionID}}

		Convey("returns an nil when batches is length 0", func() {
			batches = []db.Keyed{}
			err := dao.BatchDeleteTransactions(context.Background(), batches)
			So(err, ShouldBeNil)
		})

		Convey("returns nil when we delete the transactions", func() {
			// create it
			put, err := dao.GetWriteTransactionTx(dynamo.Transaction{
				ID:        transactionID,
				OwnerID:   userID,
				Operation: dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*dynamo.THToken{
					{
						GroupId:  channelID,
						Domain:   domain,
						Quantity: quantity,
						OwnerId:  userID,
					},
				},
				IsCommitted: true,
			})
			So(err, ShouldBeNil)
			So(put, ShouldNotBeNil)

			// run it
			err = put.RunWithContext(ctx)
			So(err, ShouldBeNil)

			// check it
			transaction, err := dao.GetTransactionRecord(ctx, transactionID)
			So(err, ShouldBeNil)
			So(transaction, ShouldNotBeNil)

			err = dao.BatchDeleteTransactions(context.Background(), batches)
			So(err, ShouldBeNil)

			transaction, err = dao.GetTransactionRecord(ctx, transactionID)
			So(err, ShouldBeNil)
			So(transaction, ShouldBeNil)
		})
	})
}

func TestDao_BatchPutTransactions(t *testing.T) {
	ctx := context.Background()
	Convey("given a transactions dao", t, func() {
		dao := setupDao(t)

		transactionID := fmt.Sprintf("test-%s", uuid.NewV4().String())
		userID := random.String(16)
		channelID := random.String(16)
		quantity := int64(3)
		domain := dynamo.TwirpDomain(pachinko.Domain_STICKER)

		put, err := dao.GetWriteTransactionTx(dynamo.Transaction{
			ID:        transactionID,
			OwnerID:   userID,
			Operation: dynamo.AddTokensOperation,
			Timestamp: time.Now(),
			Tokens: []*dynamo.THToken{
				{
					GroupId:  channelID,
					Domain:   domain,
					Quantity: quantity,
					OwnerId:  userID,
				},
			},
			IsCommitted: true,
		})
		So(err, ShouldBeNil)
		So(put, ShouldNotBeNil)

		// run it
		err = put.RunWithContext(ctx)
		So(err, ShouldBeNil)

		// check it
		transaction, err := dao.GetTransactionRecord(ctx, transactionID)
		So(err, ShouldBeNil)
		So(transaction, ShouldNotBeNil)

		transactions := []dynamo.Transaction{*transaction}

		Convey("returns an nil when batches is length 0", func() {
			transactions = []dynamo.Transaction{}
			err := dao.BatchPutTransactions(context.Background(), transactions)
			So(err, ShouldBeNil)
		})

		Convey("returns nil when we delete the transactions", func() {
			newUserId := random.String(16)
			newTransactions := []dynamo.Transaction{{
				ID:        transactionID,
				OwnerID:   newUserId,
				Operation: dynamo.AddTokensOperation,
				Timestamp: time.Now(),
				Tokens: []*dynamo.THToken{
					{
						GroupId:  channelID,
						Domain:   domain,
						Quantity: quantity,
						OwnerId:  newUserId,
					},
				},
				IsCommitted: true,
			}}

			err = dao.BatchPutTransactions(context.Background(), newTransactions)
			So(err, ShouldBeNil)

			transaction, err = dao.GetTransactionRecord(ctx, transactionID)
			So(err, ShouldBeNil)
			So(transaction, ShouldNotBeNil)
			So(transaction.OwnerID, ShouldEqual, newUserId)
		})
	})
}
