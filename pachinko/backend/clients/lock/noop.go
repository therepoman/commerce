package lock

import "context"

type noOpLockingClient struct {
}

func NewNoOpLockingClient() LockingClient {
	return &noOpLockingClient{}
}

func (c *noOpLockingClient) ObtainLock(ctx context.Context, key string) (Lock, error) {
	return &noOpLock{}, nil
}

type noOpLock struct {
}

func (l *noOpLock) Unlock() error {
	return nil
}
