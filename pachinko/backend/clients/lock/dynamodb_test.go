package lock

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/environment"
	"code.justin.tv/commerce/pachinko/config/tenant"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/gofrs/uuid"
	db "github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO(t *testing.T) {
	session := dynamo.GetAWSSession(t, "")

	ctx := context.Background()
	ttl := int64(5)

	cfg := &config.Config{
		TenantDomain: pachinko.Domain_STICKER,
		Environment: &environment.Config{
			DynamoSuffix: dynamo.TestTablesSuffix,
		},
		Tenants: &tenant.Wrapper{
			"sticker": &tenant.Config{
				ResourceIdentifier: "sticker",
			},
		},
	}

	d := &dao{
		Config:  cfg,
		Session: db.New(session),
	}

	Convey("Given Lock table DAO", t, func() {
		Convey("#Put", func() {
			randomKey := uuid.Must(uuid.NewV4()).String()
			err := d.Put(ctx, randomKey, ttl)
			So(err, ShouldBeNil)

			Convey("will fail when we try and put same key with new TTL if it hasn't expired", func() {
				err := d.Put(ctx, randomKey, ttl)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("#Get", func() {
			Convey("when the lock entry already expired", func() {
				randomKey := uuid.Must(uuid.NewV4()).String()
				err := d.Put(ctx, randomKey, int64(1))
				So(err, ShouldBeNil)

				time.Sleep(2 * time.Second) //wait for this lock entry to expire

				entry, err := d.Get(ctx, randomKey)
				So(err, ShouldBeNil)
				So(entry, ShouldBeNil)
			})

			Convey("when the lock entry is there", func() {
				randomKey := uuid.Must(uuid.NewV4()).String()
				err := d.Put(ctx, randomKey, ttl)
				So(err, ShouldBeNil)
				entry, err := d.Get(ctx, randomKey)
				So(err, ShouldBeNil)
				if entry == nil {
					t.Fail()
				} else {
					So(entry.Key, ShouldEqual, randomKey)
				}
			})
		})

		Convey("#Del", func() {
			randomKey := uuid.Must(uuid.NewV4()).String()
			err := d.Put(ctx, randomKey, ttl)
			So(err, ShouldBeNil)

			err = d.Del(ctx, randomKey)
			So(err, ShouldBeNil)

			entry, err := d.Get(ctx, randomKey)
			So(err, ShouldBeNil)
			So(entry, ShouldBeNil)
		})
	})
}
