package lock

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/pachinko/backend/circuit"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"github.com/afex/hystrix-go/hystrix"
	lock "github.com/bsm/redislock"
)

const (
	defaultLockTimeout = 50 * time.Millisecond
	defaultWaitTimeout = 10 * time.Millisecond
)

const (
	obtainLockLatencyMetric  = "obtain-lock-latency"
	releaseLockLatencyMetric = "release-lock-latency"
)

type redisLockingClient struct {
	RedisClient lock.RedisClient `inject:"locking-redis"`
	Statter     metrics.Statter  `inject:""`
}

func NewRedisLockingClient() LockingClient {
	return &redisLockingClient{}
}

func (c *redisLockingClient) ObtainLock(ctx context.Context, key string) (Lock, error) {
	var rl *lock.Lock
	err := hystrix.Do(circuit.ObtainLockCommand, func() error {
		var err error
		start := time.Now()
		locker := lock.New(c.RedisClient)
		rl, err = locker.Obtain(key, defaultLockTimeout, &lock.Options{
			Context:       ctx,
			RetryStrategy: lock.LinearBackoff(defaultWaitTimeout),
		})
		go c.Statter.Duration(obtainLockLatencyMetric, time.Since(start))
		return err
	}, nil)

	if err != nil {
		return nil, err
	}

	if rl == nil {
		return nil, errors.New("failed to obtain redis lock")
	}

	return &redisLock{
		RedisLock: rl,
		Statter:   c.Statter,
	}, nil
}

type redisLock struct {
	RedisLock *lock.Lock
	Statter   metrics.Statter
}

func (l *redisLock) Unlock() error {
	return hystrix.Do(circuit.ReleaseLockCommand, func() error {
		start := time.Now()
		err := l.RedisLock.Release()
		go l.Statter.Duration(releaseLockLatencyMetric, time.Since(start))
		return err
	}, nil)
}
