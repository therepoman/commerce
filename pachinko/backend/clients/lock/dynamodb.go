package lock

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachinko/backend/circuit"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/guregu/dynamo"
)

type dynamodbLockingClient struct {
	DAO DAO `inject:""`
}

func NewDynamoDBLockingClient() *dynamodbLockingClient {
	return &dynamodbLockingClient{}
}

func (l *dynamodbLockingClient) ObtainLock(ctx context.Context, key string) (Lock, error) {
	err := hystrix.Do(circuit.ObtainLockCommand, func() error {
		return l.DAO.Put(ctx, key, int64(defaultLockTimeout.Seconds()))
	}, nil)
	if err != nil {
		return nil, err
	}

	return &dynamoLock{
		key: key,
		dao: l.DAO,
	}, nil
}

type dynamoLock struct {
	key string
	dao DAO
}

func (l *dynamoLock) Unlock() error {
	err := hystrix.Do(circuit.ReleaseLockCommand, func() error {
		return l.dao.Del(context.Background(), l.key)
	}, nil)
	if err != nil {
		return err
	}
	return nil
}

const (
	TableNameFormat = "%s-locks-gbl-%s"
	HashKey         = "key"
	TTLField        = "ttl"
)

type LockEntry struct {
	Key       string `dynamo:"key"`
	ExpiresAt int64  `dynamo:"ttl"`
}

type DAO interface {
	Put(ctx context.Context, key string, ttl int64) error
	Get(ctx context.Context, key string) (*LockEntry, error)
	Del(ctx context.Context, key string) error
}

type dao struct {
	Session *dynamo.DB
	Config  *config.Config `inject:""`
}

func NewDAOWithClient(dynamoClient dynamodbiface.DynamoDBAPI) DAO {
	return &dao{
		Session: dynamo.NewFromIface(dynamoClient),
	}
}

func tableFormat(tenant, env string) string {
	return fmt.Sprintf(TableNameFormat, tenant, env)
}

func (d *dao) Put(ctx context.Context, key string, ttlSeconds int64) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform lock dynamo op on non configured domain")
	}

	now := time.Now().Unix()
	newEntry := LockEntry{
		Key:       key,
		ExpiresAt: now + ttlSeconds,
	}

	return d.Session.
		Table(tableFormat(tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Put(&newEntry).
		If("attribute_not_exists($) OR $ < ?", HashKey, TTLField, now).
		RunWithContext(ctx)
}

func (d *dao) Get(ctx context.Context, key string) (*LockEntry, error) {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return nil, errors.New("cannot perform lock dynamo op on non configured domain")
	}

	var entry LockEntry

	err := d.Session.
		Table(tableFormat(tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Get(HashKey, key).
		Consistent(true).
		OneWithContext(ctx, &entry)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	if entry.ExpiresAt < time.Now().Unix() {
		return nil, nil
	}

	return &entry, err
}

func (d *dao) Del(ctx context.Context, key string) error {
	tenantConf := d.Config.Tenants.Get(d.Config.TenantDomain)
	if tenantConf == nil {
		return errors.New("cannot perform lock dynamo op on non configured domain")
	}

	return d.Session.
		Table(tableFormat(tenantConf.ResourceIdentifier, d.Config.Environment.DynamoSuffix)).
		Delete(HashKey, key).
		RunWithContext(ctx)
}
