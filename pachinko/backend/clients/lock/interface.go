package lock

import "context"

type LockingClient interface {
	ObtainLock(ctx context.Context, key string) (Lock, error)
}

type Lock interface {
	Unlock() error
}
