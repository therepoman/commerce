package cloudwatchlogger

import (
	"context"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	stringsUtil "code.justin.tv/commerce/payday/utils/strings"
	cwlog "code.justin.tv/video/cloudwatchlogger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"github.com/gofrs/uuid"
)

const (
	maxWorkerCount   = 1
	maxBatchSize     = 500
	inputBufferSize  = 10000
	batchBufferSize  = 10000
	maxBatchDuration = 5 * time.Second

	shutdownDuration = 5 * time.Second
)

type CloudWatchLogger interface {
	Send(ctx context.Context, message string) error
	Shutdown()
}

type cloudWatchLoggerImpl struct {
	client   *cwlog.Sender
	ctx      context.Context
	shutdown context.CancelFunc
}

type Fields map[string]interface{}

func NewCloudWatchLogClient(region, logGroupName string) (CloudWatchLogger, error) {
	if stringsUtil.Blank(region) || stringsUtil.Blank(logGroupName) {
		return NewCloudWatchLogNoopClient(), nil
	}

	awsSession := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(region),
	}))

	streamName, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	cwlogSender, err := cwlog.NewSender(&cwlog.Config{
		CloudwatchLogs:            cloudwatchlogs.New(awsSession),
		LogGroupName:              logGroupName,
		InstanceIdentifier:        streamName.String(),
		CloudwatchLogsWorkerCount: maxWorkerCount,
		InputBufferLength:         inputBufferSize,
		BatchBufferLength:         batchBufferSize,
		MaxBatchSize:              maxBatchSize,
		MaxBatchDuration:          maxBatchDuration,
		EventHooks: cwlog.EventHooks{
			InputDroppedBufferExceeded: func(messages []string) {
				msgCount := len(messages)
				if msgCount > 0 {
					log.WithField("count", msgCount).Warn("cwlogSender dropped input events")
				}
			},
			BatchDroppedBufferExceeded: func(batch []*cloudwatchlogs.InputLogEvent) {
				batchCount := len(batch)
				if batchCount > 0 {
					log.WithField("count", len(batch)).Warn("cwlogSender dropped batched events")
				}
			},
			EventsSentFailure: func(input *cloudwatchlogs.PutLogEventsInput, err error) {
				eventCount := len(input.LogEvents)
				if eventCount > 0 {
					log.WithError(err).WithField("count", len(input.LogEvents)).Warn("cwlogSender failed to send log events")
				}
			},
		},
	})
	if err != nil {
		return nil, err
	}

	ctx, shutdown := context.WithCancel(context.Background())
	go func() {
		// The Run Method runs indefinitely, if it errors for any reason we try and restart it
		for {
			select {
			case <-ctx.Done():
				return
			default:
				if err := cwlogSender.Run(ctx); err != nil {
					if !errorIsSuccessfulShutdown(err.Error()) {
						log.WithError(err).Error("cwlogSender errored, restarting")
					}
				}
			}
		}
	}()

	return &cloudWatchLoggerImpl{
		client:   cwlogSender,
		ctx:      ctx,
		shutdown: shutdown,
	}, nil
}

func (c *cloudWatchLoggerImpl) Send(ctx context.Context, message string) error {
	return c.client.Send(ctx, []string{message})
}

func (c *cloudWatchLoggerImpl) Shutdown() {
	// Cancels running context sent to cwlogSender.Run()
	c.shutdown()
	// the shutdown process takes 5 seconds
	// https://git.xarth.tv/video/cloudwatchlogger/blob/master/sender.go#L25
	time.Sleep(shutdownDuration)
}

func errorIsSuccessfulShutdown(errString string) bool {
	// video/cloudwatchlogger does not currently export explicit error types
	successExit := strings.Contains(errString, "exit")
	emptyFinalFlush := strings.Contains(errString, "failed final flush") &&
		strings.Contains(errString, "minimum field size of 1")
	return successExit || emptyFinalFlush
}
