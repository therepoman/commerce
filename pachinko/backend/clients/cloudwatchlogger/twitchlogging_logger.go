package cloudwatchlogger

import (
	"context"
	"encoding/json"
	"fmt"

	twitchlogging "code.justin.tv/amzn/TwitchLogging"
	log "code.justin.tv/commerce/logrus"
)

type loggerImpl struct {
	logger   CloudWatchLogger
	loggerId string
	tenant   string
}

const (
	unmappedKey = "UNKEYED_VALUE"
	msgKey      = "msg"
)

func AdaptToTwitchLoggingLogger(cwlogger CloudWatchLogger, loggerId string, tenant string) twitchlogging.Logger {
	return &loggerImpl{
		logger:   cwlogger,
		loggerId: loggerId,
		tenant:   tenant,
	}
}

func (l *loggerImpl) Log(msg string, keyvals ...interface{}) {
	log := log.WithField("loggerId", l.loggerId)
	keyvals = append([]interface{}{"tenant", l.tenant}, keyvals...)
	msgMap := mapFromKeyVals(msg, keyvals...)
	jsonString, err := json.Marshal(msgMap)
	if err != nil {
		log.WithError(err).Warn("error while marshaling log to json in twitch logging logger")
		return
	}
	err = l.logger.Send(context.Background(), string(jsonString))
	if err != nil {
		log.WithError(err).Warn("error while sending log in twitch logging logger")
	}
}

// ripped from https://git.xarth.tv/amzn/TwitchLoggingCommonLoggers/blob/master/json_logger.go
func mapFromKeyVals(msg string, keyvals ...interface{}) map[string]string {
	// Determine the length of the map
	// The size of the map we need is half the size of keyvals or keyvals + 1 (if keyvals is odd to account for
	// the odd, unmapped value). We then add +1 to the keyval-based result to account for actual message being logged
	mSize := ((len(keyvals) + (len(keyvals) % 2)) / 2) + 1
	m := make(map[string]string, mSize)
	// Add all the key, value pairs
	for i := 0; i < len(keyvals); i += 2 {
		var key, value interface{}
		if i == len(keyvals)-1 {
			key, value = unmappedKey, keyvals[i]
		} else {
			key, value = keyvals[i], keyvals[i+1]
		}
		m[fmt.Sprint(key)] = fmt.Sprint(value)
	}
	// Add the final message
	m[msgKey] = msg
	return m
}
