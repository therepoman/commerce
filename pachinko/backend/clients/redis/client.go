package redis

import (
	"context"
	"time"

	"code.justin.tv/chat/rediczar"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/config"
	go_redis "github.com/go-redis/redis"
	"github.com/go-redis/redis/v7"
)

type Client interface {
	Del(ctx context.Context, keys ...string) (int64, error)
	Expire(ctx context.Context, key string, expiration time.Duration) (bool, error)
	Get(ctx context.Context, key string) (string, error)
	HDel(ctx context.Context, key string, fields ...string) (int64, error)
	HGet(ctx context.Context, key string, field string) (string, error)
	HGetAll(ctx context.Context, key string) (map[string]string, error)
	HMGet(ctx context.Context, key string, fields ...string) ([]interface{}, error)
	HSet(ctx context.Context, key string, field string, value interface{}) (bool, error)
	HMSet(ctx context.Context, key string, fields map[string]interface{}) (string, error)
	PipelinedHGet(ctx context.Context, keyFieldPairs [][2]string) (map[[2]string]string, error)
	PipelinedHGetAll(ctx context.Context, keys []string) (map[string]map[string]string, error)
	PipelinedHSet(ctx context.Context, keyFieldValMap map[[2]string]interface{}) (map[[2]string]bool, error)
	PipelinedHMSet(ctx context.Context, keyFieldValMap map[string]map[string]interface{}) (map[string]string, error)
	Set(ctx context.Context, key string, value interface{}, ttl time.Duration) (string, error)
	TTL(ctx context.Context, key string) (time.Duration, error)
}

type client struct {
	Config       *config.Config `inject:""`
	TenantClient rediczar.ThickClient
}

func NewClient(tenantClient rediczar.ThickClient) Client {
	return &client{
		TenantClient: tenantClient,
	}
}

func (c client) Del(ctx context.Context, keys ...string) (int64, error) {
	return c.TenantClient.Del(ctx, keys...)
}

func (c client) Expire(ctx context.Context, key string, expiration time.Duration) (bool, error) {
	return c.TenantClient.Expire(ctx, key, expiration)
}

func (c client) HDel(ctx context.Context, key string, fields ...string) (int64, error) {
	return c.TenantClient.HDel(ctx, key, fields...)
}

func (c client) HGet(ctx context.Context, key string, field string) (string, error) {
	return c.TenantClient.HGet(ctx, key, field)
}

func (c client) HGetAll(ctx context.Context, key string) (map[string]string, error) {
	return c.TenantClient.HGetAll(ctx, key)
}

func (c client) HMGet(ctx context.Context, key string, fields ...string) ([]interface{}, error) {
	return c.TenantClient.HMGet(ctx, key, fields...)
}

func (c client) HSet(ctx context.Context, key string, field string, value interface{}) (bool, error) {
	return c.TenantClient.HSet(ctx, key, field, value)
}

func (c client) HMSet(ctx context.Context, key string, fields map[string]interface{}) (string, error) {
	return c.TenantClient.HMSet(ctx, key, fields)
}

func (c client) PipelinedHGet(ctx context.Context, keyFieldPairs [][2]string) (map[[2]string]string, error) {
	keyFieldPairToCmd := make(map[[2]string]*redis.StringCmd)
	keyFieldPairToVal := make(map[[2]string]string)
	_, err := c.TenantClient.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, pair := range keyFieldPairs {
			stringCmd := pipe.HGet(pair[0], pair[1])
			if stringCmd != nil {
				keyFieldPairToCmd[pair] = stringCmd
			}
		}

		return nil
	})

	// We encountered an error that is NOT just a missing key.
	if err != nil && err != go_redis.Nil {
		return nil, err
	}

	for pair, stringCmd := range keyFieldPairToCmd {
		if !strings.Blank(stringCmd.Val()) {
			keyFieldPairToVal[pair] = stringCmd.Val()
		}
	}

	return keyFieldPairToVal, nil
}

func (c client) PipelinedHGetAll(ctx context.Context, keys []string) (map[string]map[string]string, error) {
	keyToCmd := make(map[string]*redis.StringStringMapCmd)
	keyToVal := make(map[string]map[string]string)
	_, err := c.TenantClient.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, key := range keys {
			stringStringMapCmd := pipe.HGetAll(key)
			if stringStringMapCmd != nil {
				keyToCmd[key] = stringStringMapCmd
			}
		}

		return nil
	})

	// We encountered an error that is NOT just a missing key.
	if err != nil && err != go_redis.Nil {
		return nil, err
	}

	for key, stringStringMapCmd := range keyToCmd {
		if stringStringMapCmd.Val() != nil {
			keyToVal[key] = stringStringMapCmd.Val()
		}
	}

	return keyToVal, nil
}

func (c client) PipelinedHSet(ctx context.Context, keyFieldValMap map[[2]string]interface{}) (map[[2]string]bool, error) {
	keyFieldBoolCmdMap := make(map[[2]string]*redis.BoolCmd)
	keyFieldBoolMap := make(map[[2]string]bool)
	_, err := c.TenantClient.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
		for keyField, val := range keyFieldValMap {
			boolCmd := pipe.HSet(keyField[0], keyField[1], val)
			if boolCmd != nil {
				keyFieldBoolCmdMap[keyField] = boolCmd
			}
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	for keyField, boolCmd := range keyFieldBoolCmdMap {
		keyFieldBoolMap[keyField] = boolCmd.Val()
	}

	return keyFieldBoolMap, nil
}

func (c client) PipelinedHMSet(ctx context.Context, keyFieldValMap map[string]map[string]interface{}) (map[string]string, error) {
	keyStatusCmdMap := make(map[string]*redis.StatusCmd)
	keyStringMap := make(map[string]string)
	_, err := c.TenantClient.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
		for key, fieldValMap := range keyFieldValMap {
			statusCmd := pipe.HMSet(key, fieldValMap)
			if statusCmd != nil {
				keyStatusCmdMap[key] = statusCmd
			}
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	for key, statusCmd := range keyStatusCmdMap {
		keyStringMap[key] = statusCmd.Val()
	}

	return keyStringMap, nil
}

func (c client) TTL(ctx context.Context, key string) (time.Duration, error) {
	return c.TenantClient.TTL(ctx, key)
}

func (c client) Get(ctx context.Context, key string) (string, error) {
	return c.TenantClient.Get(ctx, key)
}

func (c client) Set(ctx context.Context, key string, value interface{}, ttl time.Duration) (string, error) {
	return c.TenantClient.Set(ctx, key, value, ttl)
}
