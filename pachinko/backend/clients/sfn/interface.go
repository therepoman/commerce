package sfn

import "context"

type Client interface {
	Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error
}
