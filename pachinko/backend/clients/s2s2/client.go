package s2s2

import (
	"os"
	"time"

	twitchlogging "code.justin.tv/amzn/TwitchLogging"
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	cw "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"
	twitchtelemetrymiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"github.com/gofrs/uuid"
)

const (
	// Default settings
	metricFlushInterval     = 30 * time.Second
	metricBufferSize        = 100000
	metricAggregationPeriod = time.Minute

	unknownHostName = "unknown_hostname"
	unknownLaunchId = "unknown_launch_id"
)

func NewS2S2Client(cfg *config.Config, logger twitchlogging.Logger) (*s2s2.S2S2, error) {
	return s2s2.New(&s2s2.Options{
		Config: &c7s.Config{
			ClientServiceName:   cfg.GetS2SName(),
			ServiceOrigins:      cfg.GetTenant().URL,
			EnableAccessLogging: true,
		},
		OperationStarter: &operation.Starter{
			OpMonitors: []operation.OpMonitor{
				&twitchtelemetrymiddleware.OperationMonitor{
					SampleReporter: getSampleReporter(cfg),
				},
			},
		},
		Logger: logger,
	})
}

func getSampleReporter(cfg *config.Config) telemetry.SampleReporter {
	pid := &identifier.ProcessIdentifier{
		Service:  "pachinko",
		Region:   cfg.Environment.AWSRegion,
		Stage:    cfg.GetEnvironmentMetricName(),
		LaunchID: getLaunchId(),
		Machine:  getHostName(),
		Substage: cfg.GetTenant().ResourceIdentifier,
	}
	sender := cw.NewUnbuffered(pid, nil)
	sampleObserver := telemetry.NewBufferedAggregator(metricFlushInterval, metricBufferSize, metricAggregationPeriod, sender, nil)
	sampleReporter := telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: *pid},
		SampleObserver: sampleObserver,
	}
	return sampleReporter
}

func getHostName() string {
	if envHostname, err := os.Hostname(); err == nil {
		return envHostname
	}
	return unknownHostName
}

func getLaunchId() string {
	if newUUID, err := uuid.NewV4(); err != nil {
		log.WithError(err).Warn("error creating launchID for telemetryReporter")
		return unknownLaunchId
	} else {
		return newUUID.String()
	}
}
