package sqs

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachinko/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cenkalti/backoff"
)

const (
	DefaultVisibilityTimeoutSeconds = 40
	WaitTimeSeconds                 = 20
	MaxReceivedMessage              = 10
	MaxDeleteBatchSize              = 10
	DefaultDeleteRetries            = 7
	BackoffInitialInterval          = 500 * time.Millisecond
	BackoffMaxElapsedTime           = 5 * time.Second
	BackoffMultiplier               = 2
)

type SDKClientWrapper struct {
	SDKClient sqsiface.SQSAPI
}

func NewDefaultConfig(cfg *config.Config) *aws.Config {
	return &aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	}
}

func NewDefaultClient(cfg *config.Config) (Client, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return &SDKClientWrapper{
		SDKClient: sqs.New(sess, NewDefaultConfig(cfg)),
	}, nil
}

func (c *SDKClientWrapper) SendMessage(ctx context.Context, queueURL string, msgBody string) error {
	input := &sqs.SendMessageInput{
		QueueUrl:    aws.String(queueURL),
		MessageBody: aws.String(msgBody),
	}

	_, err := c.SDKClient.SendMessageWithContext(ctx, input)
	return err
}

func (c *SDKClientWrapper) ReceiveMessages(ctx context.Context, queueURL string, maxReceivedMessages int) ([]*sqs.Message, error) {
	if maxReceivedMessages < 1 || maxReceivedMessages > MaxReceivedMessage {
		return nil, fmt.Errorf("maxReceivedMessages must be from 1 to %d", MaxReceivedMessage)
	}
	input := &sqs.ReceiveMessageInput{
		QueueUrl:            aws.String(queueURL),
		MaxNumberOfMessages: aws.Int64(int64(maxReceivedMessages)),
		VisibilityTimeout:   aws.Int64(DefaultVisibilityTimeoutSeconds),
		WaitTimeSeconds:     aws.Int64(WaitTimeSeconds),
	}

	output, err := c.SDKClient.ReceiveMessageWithContext(ctx, input)
	if err != nil {
		return nil, err
	}

	return output.Messages, nil
}

func (c *SDKClientWrapper) DeleteMessages(ctx context.Context, queueURL string, messages []*sqs.Message) error {
	for i := 0; i < len(messages); i += MaxDeleteBatchSize {
		var entriesToDelete []*sqs.DeleteMessageBatchRequestEntry
		isLastBatch := len(messages)-1-i < MaxDeleteBatchSize
		var endIndex int
		if !isLastBatch {
			endIndex = i + MaxDeleteBatchSize
		} else {
			endIndex = len(messages)
		}

		for _, message := range messages[i:endIndex] {
			entriesToDelete = append(entriesToDelete, &sqs.DeleteMessageBatchRequestEntry{
				Id:            message.MessageId,
				ReceiptHandle: message.ReceiptHandle,
			})
		}
		input := &sqs.DeleteMessageBatchInput{
			QueueUrl: aws.String(queueURL),
			Entries:  entriesToDelete,
		}

		bo := backoff.WithMaxRetries(newBackOff(), DefaultDeleteRetries)
		backoffFunc := func() error {
			// Although DeleteMessageBatch returns number of failed deletes, it does not return the Id and ReceiptHandle,
			// So we'll just send in the original payload
			_, err := c.SDKClient.DeleteMessageBatchWithContext(ctx, input)
			if err != nil {
				return err
			}
			return nil
		}
		err := backoff.Retry(backoffFunc, bo)
		if err != nil {
			return err
		}
	}
	return nil
}

func newBackOff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = BackoffInitialInterval
	exponetial.MaxElapsedTime = BackoffMaxElapsedTime
	exponetial.Multiplier = BackoffMultiplier
	return exponetial
}

func (c *SDKClientWrapper) PurgeQueue(ctx context.Context, queueURL string) error {
	_, err := c.SDKClient.PurgeQueueWithContext(ctx, &sqs.PurgeQueueInput{
		QueueUrl: aws.String(queueURL),
	})
	return err
}
