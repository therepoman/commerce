package sqs

import (
	"context"

	"github.com/aws/aws-sdk-go/service/sqs"
)

type Client interface {
	SendMessage(ctx context.Context, queueURL, msgBody string) error
	ReceiveMessages(ctx context.Context, queueURL string, maxReceivedMessages int) ([]*sqs.Message, error)
	DeleteMessages(ctx context.Context, queueURL string, messages []*sqs.Message) error
	// This is for integration test only
	PurgeQueue(ctx context.Context, queueURL string) error
}
