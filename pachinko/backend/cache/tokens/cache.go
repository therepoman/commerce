package tokens

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/circuit"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/afex/hystrix-go/hystrix"
	go_redis "github.com/go-redis/redis"
)

const (
	cacheKeyFormat = "balance.%s.%s"
	cacheTTL       = time.Minute * 5

	getCacheLatency      = "get-token-cache-latency"
	getBatchCacheLatency = "get-token-batch-cache-latency"
	multiGetCacheLatency = "multi-get-token-cache-latency"
	getAllCacheLatency   = "get-token-all-cache-latency"
	setCacheLatency      = "set-token-cache-latency"
	setBatchCacheLatency = "set-token-batch-cache-latency"
	multiSetCacheLatency = "multi-set-token-cache-latency"
	delCacheLatency      = "del-token-cache-latency"
	delAllCacheLatency   = "del-token-all-cache-latency"
)

type cacheRecord struct {
	Token     *pachinko.Token `json:"token"`
	ExpiresAt time.Time       `json:"expires_at"`
}

type Cache interface {
	Get(ctx context.Context, ownerID, groupID string) *pachinko.Token
	GetAll(ctx context.Context, ownerID string) (map[string]*pachinko.Token, int)
	GetBatch(ctx context.Context, ownerID string, groupIDs []string) map[string]*pachinko.Token
	Set(ctx context.Context, token *pachinko.Token)
	SetBatch(ctx context.Context, ownerID string, tokens map[string]*pachinko.Token)
	Del(ctx context.Context, ownerID, groupID string)
	MultiGet(ctx context.Context, ownerGroupPairs ...[2]string) []*pachinko.Token
	MultiSet(ctx context.Context, ownerGroupTokenMap map[[2]string]*pachinko.Token)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	Config  *config.Config  `inject:""`
	Redis   redis.Client    `inject:"redis-tokens"`
	Statter metrics.Statter `inject:""`
}

func cacheKey(domain pachinko.Domain, ownerID string) string {
	return fmt.Sprintf(cacheKeyFormat, domain.String(), ownerID)
}

func (c *cache) Get(ctx context.Context, ownerID, groupID string) *pachinko.Token {
	var cachedValue string
	err := hystrix.Do(circuit.GetTokenCacheCommand, func() error {
		var err error
		start := time.Now()
		cachedValue, err = c.Redis.HGet(ctx, cacheKey(c.Config.TenantDomain, ownerID), groupID)
		go c.Statter.Duration(getCacheLatency, time.Since(start))
		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("could not fetch token balance from cache")
			return err
		}
		return nil
	}, nil)

	if err != nil || strings.Blank(cachedValue) {
		return nil
	}

	var cachedRecord cacheRecord
	err = json.Unmarshal([]byte(cachedValue), &cachedRecord)
	if err != nil {
		c.Del(context.Background(), ownerID, groupID)
		logrus.WithError(err).Error("could not unmarshal cached token balance entry")
		return nil
	}

	if time.Now().After(cachedRecord.ExpiresAt) {
		c.Del(context.Background(), ownerID, groupID)
		return nil
	}

	return cachedRecord.Token
}

func (c *cache) MultiGet(ctx context.Context, ownerGroupPairs ...[2]string) []*pachinko.Token {
	for i, ownerGroupPair := range ownerGroupPairs {
		ownerGroupPairs[i][0] = cacheKey(c.Config.TenantDomain, ownerGroupPair[0])
	}

	var cachedValues map[[2]string]string
	err := hystrix.Do(circuit.MultiGetTokensCacheCommand, func() error {
		var err error
		start := time.Now()
		cachedValues, err = c.Redis.PipelinedHGet(ctx, ownerGroupPairs)
		go c.Statter.Duration(multiGetCacheLatency, time.Since(start))
		if err != nil {
			logrus.WithError(err).Error("could not fetch token balances from cache")
			return err
		}
		return nil
	}, nil)

	if err != nil || len(cachedValues) < 1 {
		return nil
	}

	tokens, expiredRecords := []*pachinko.Token{}, make([]*pachinko.Token, 0)
	for cachedOwnerGroupPair, value := range cachedValues {
		var record cacheRecord
		err := json.Unmarshal([]byte(value), &record)
		if err != nil {
			c.delWithCacheKey(context.Background(), cachedOwnerGroupPair[0], cachedOwnerGroupPair[1])
			logrus.WithError(err).Error("could not unmarshal cached token balances entry")
			return nil
		}

		if time.Now().After(record.ExpiresAt) {
			expiredRecords = append(expiredRecords, record.Token)
		} else {
			tokens = append(tokens, record.Token)
		}
	}

	go c.cleanExpired(context.Background(), expiredRecords)

	return tokens
}

func (c *cache) GetBatch(ctx context.Context, ownerID string, groupIDs []string) map[string]*pachinko.Token {
	var cachedValues []interface{}
	err := hystrix.Do(circuit.GetTokensBatchCacheCommand, func() error {
		var err error
		start := time.Now()
		cachedValues, err = c.Redis.HMGet(ctx, cacheKey(c.Config.TenantDomain, ownerID), groupIDs...)
		go c.Statter.Duration(getBatchCacheLatency, time.Since(start))
		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("could not fetch batch token balances from cache")
			return err
		}
		return nil
	}, nil)

	if err != nil || len(cachedValues) < 1 {
		return nil
	}

	tokens, expiredRecords := map[string]*pachinko.Token{}, make([]*pachinko.Token, 0)
	for _, value := range cachedValues {
		if value != nil {
			var record cacheRecord
			err := json.Unmarshal([]byte(value.(string)), &record)
			if err != nil {
				c.delAll(context.Background(), ownerID)
				logrus.WithError(err).Error("could not unmarshal cached token balances entry")
				return nil
			}

			if time.Now().After(record.ExpiresAt) {
				expiredRecords = append(expiredRecords, record.Token)
			} else {
				tokens[record.Token.GroupId] = record.Token
			}
		}
	}

	go c.cleanExpired(context.Background(), expiredRecords)

	return tokens
}

func (c *cache) GetAll(ctx context.Context, ownerID string) (map[string]*pachinko.Token, int) {
	var cachedValues map[string]string
	err := hystrix.Do(circuit.GetTokensAllCacheCommand, func() error {
		var err error
		start := time.Now()
		cachedValues, err = c.Redis.HGetAll(ctx, cacheKey(c.Config.TenantDomain, ownerID))
		go c.Statter.Duration(getAllCacheLatency, time.Since(start))
		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("could not fetch token balances from cache")
			return err
		}
		return nil
	}, nil)

	if err != nil || len(cachedValues) < 1 {
		return nil, 0
	}

	tokens, expiredRecords := map[string]*pachinko.Token{}, make([]*pachinko.Token, 0)
	for groupID, value := range cachedValues {
		var record cacheRecord
		err := json.Unmarshal([]byte(value), &record)
		if err != nil {
			c.Del(context.Background(), ownerID, groupID)
			logrus.WithError(err).Error("could not unmarshal cached token balances entry")
			return nil, 0
		}

		if time.Now().After(record.ExpiresAt) {
			expiredRecords = append(expiredRecords, record.Token)
		} else {
			tokens[record.Token.GroupId] = record.Token
		}
	}

	go c.cleanExpired(context.Background(), expiredRecords)

	return tokens, len(cachedValues)
}

func (c *cache) Set(ctx context.Context, token *pachinko.Token) {
	ttl := c.ttl(ctx, token.OwnerId)

	cacheRecord := cacheRecord{
		Token:     token,
		ExpiresAt: ttl,
	}

	cacheValue, err := json.Marshal(&cacheRecord)
	if err != nil {
		logrus.WithError(err).Error("could not marshal token balance for caching")
		return
	}
	_ = hystrix.Do(circuit.SetTokenCacheCommand, func() error {
		start := time.Now()
		_, err = c.Redis.HSet(ctx, cacheKey(c.Config.TenantDomain, token.OwnerId), token.GroupId, string(cacheValue))
		go c.Statter.Duration(setCacheLatency, time.Since(start))

		if err != nil {
			logrus.WithError(err).Error("could not set token balances in cache")
			return err
		}
		return nil
	}, nil)

	go func() {
		_ = c.ttl(context.Background(), token.OwnerId)
	}()
}

func (c *cache) SetBatch(ctx context.Context, ownerID string, tokens map[string]*pachinko.Token) {
	ttl := c.ttl(ctx, ownerID)

	cacheRecords := map[string]interface{}{}
	for _, token := range tokens {
		cacheRecord := cacheRecord{
			Token:     token,
			ExpiresAt: ttl,
		}
		cacheValue, err := json.Marshal(&cacheRecord)
		if err != nil {
			logrus.WithError(err).Error("could not marshal token balances for caching")
			return
		}
		cacheRecords[token.GroupId] = string(cacheValue)
	}

	if len(cacheRecords) < 1 {
		return
	}

	_ = hystrix.Do(circuit.SetTokensBatchCacheCommand, func() error {
		start := time.Now()
		_, err := c.Redis.HMSet(ctx, cacheKey(c.Config.TenantDomain, ownerID), cacheRecords)
		go c.Statter.Duration(setBatchCacheLatency, time.Since(start))

		if err != nil {
			logrus.WithError(err).Error("could not batch set token balances in cache")
			return err
		}
		return nil
	}, nil)

	go func() {
		_ = c.ttl(context.Background(), ownerID)
	}()
}

func (c *cache) MultiSet(ctx context.Context, ownerGroupTokenMap map[[2]string]*pachinko.Token) {
	cacheKeyGroupTokenMap := make(map[[2]string]*pachinko.Token)
	for ownerGroupPair, token := range ownerGroupTokenMap {
		cacheKeyGroup := [2]string{cacheKey(c.Config.TenantDomain, ownerGroupPair[0]), ownerGroupPair[1]}
		cacheKeyGroupTokenMap[cacheKeyGroup] = token
	}

	cacheKeyGroupTtlMap := make(map[[2]string]time.Time)
	for cacheKeyGroup := range cacheKeyGroupTokenMap {
		cacheKeyGroupTtlMap[cacheKeyGroup] = c.ttl(ctx, cacheKeyGroup[0])
	}

	cacheRecords := map[[2]string]interface{}{}
	for cacheKeyGroup, token := range cacheKeyGroupTokenMap {
		cacheRecord := cacheRecord{
			Token:     token,
			ExpiresAt: cacheKeyGroupTtlMap[cacheKeyGroup],
		}
		cacheValue, err := json.Marshal(&cacheRecord)
		if err != nil {
			logrus.WithError(err).Error("could not marshal token balances for caching")
			return
		}
		cacheRecords[cacheKeyGroup] = string(cacheValue)
	}

	if len(cacheRecords) < 1 {
		return
	}

	_ = hystrix.Do(circuit.MultiSetTokensCacheCommand, func() error {
		start := time.Now()
		_, err := c.Redis.PipelinedHSet(ctx, cacheRecords)
		go c.Statter.Duration(multiSetCacheLatency, time.Since(start))

		if err != nil {
			logrus.WithError(err).Error("could not batch set token balances in cache")
			return err
		}
		return nil
	}, nil)

	for cacheKeyGroup := range cacheKeyGroupTokenMap {
		go func(cacheKey string) {
			_ = c.ttl(context.Background(), cacheKey)
		}(cacheKeyGroup[0])
	}
}

func (c *cache) Del(ctx context.Context, ownerID, groupID string) {
	c.delWithCacheKey(ctx, cacheKey(c.Config.TenantDomain, ownerID), groupID)
}

func (c *cache) delWithCacheKey(ctx context.Context, cacheKey, groupID string) {
	_ = hystrix.Do(circuit.DelTokenCacheCommand, func() error {
		start := time.Now()
		if _, err := c.Redis.HDel(ctx, cacheKey, groupID); err != nil {
			logrus.WithError(err).Error("could not delete cached token balance")
			return err
		}
		go c.Statter.Duration(delCacheLatency, time.Since(start))
		return nil
	}, nil)
}

func (c *cache) ttl(ctx context.Context, ownerID string) time.Time {
	now := time.Now()
	newExpiresAt := now.Add(cacheTTL)

	err := hystrix.Do(circuit.ExpireTokenCacheCommand, func() error {
		_, err := c.Redis.Expire(ctx, cacheKey(c.Config.TenantDomain, ownerID), cacheTTL)
		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("failed to reset TTL for new key")
		}
		return nil
	}, nil)

	if err != nil {
		logrus.WithField("cacheKey", cacheKey(c.Config.TenantDomain, ownerID)).WithError(err).Warn("hystrix error when expiring token cache entry")
	}

	return newExpiresAt
}

func (c *cache) delAll(ctx context.Context, ownerID string) {
	start := time.Now()
	if _, err := c.Redis.Del(ctx, cacheKey(c.Config.TenantDomain, ownerID)); err != nil {
		logrus.WithError(err).Error("could not delete cached token balance hash")
	}
	go c.Statter.Duration(delAllCacheLatency, time.Since(start))
}

func (c *cache) cleanExpired(ctx context.Context, tokens []*pachinko.Token) {
	for _, token := range tokens {
		c.Del(ctx, token.OwnerId, token.GroupId)
	}
}
