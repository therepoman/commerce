package tokens

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/config"
	redis_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/redis"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("given a cache getter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		domain := cache.Config.TenantDomain

		cacheKey := cacheKey(domain, ownerID)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HGet", ctx, cacheKey, groupID).Return("", errors.New("ERROR"))
			result := cache.Get(ctx, ownerID, groupID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			redisClient.On("HGet", ctx, cacheKey, groupID).Return("", nil)
			result := cache.Get(ctx, ownerID, groupID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			redisClient.On("HGet", ctx, cacheKey, groupID).Return("this is invalid json &*!#(", nil)
			redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(0), nil)

			result := cache.Get(ctx, ownerID, groupID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
			})
		})

		Convey("when we do return a cached balance", func() {
			cachedValue := cacheRecord{
				Token: &pachinko.Token{
					Domain:   pachinko.Domain_STICKER,
					OwnerId:  ownerID,
					GroupId:  groupID,
					Quantity: 3,
				},
			}

			Convey("when the cached balance has been expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(-5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("HGet", ctx, cacheKey, groupID).Return(string(cachedBytes), nil)
				redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(0), nil)

				result := cache.Get(ctx, ownerID, groupID)

				Convey("we should return nil", func() {
					So(result, ShouldBeNil)
				})

				Convey("we should delete the record from the cache", func() {
					redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
				})
			})

			Convey("when the cached balance has not expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("HGet", ctx, cacheKey, groupID).Return(string(cachedBytes), nil)

				returnValue := cache.Get(ctx, ownerID, groupID)

				So(returnValue, ShouldResemble, cachedValue.Token)
			})

		})
	})
}

func TestCache_GetBatch(t *testing.T) {
	Convey("given a cache setter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		domain := cache.Config.TenantDomain

		cacheKey := cacheKey(domain, ownerID)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HMGet", ctx, cacheKey, groupID).Return(nil, errors.New("ERROR"))
			result := cache.GetBatch(ctx, ownerID, []string{groupID})

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			redisClient.On("HMGet", ctx, cacheKey, groupID).Return(nil, nil)
			result := cache.GetBatch(ctx, ownerID, []string{groupID})

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			redisClient.On("HMGet", ctx, cacheKey, groupID).Return([]interface{}{
				"this is invalid json &*!#(",
			}, nil)
			redisClient.On("Del", ctx, cacheKey).Return(int64(0), nil)

			result := cache.GetBatch(ctx, ownerID, []string{groupID})

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "Del", ctx, cacheKey)
			})
		})

		Convey("when we do return a cached balance", func() {
			cachedValue := cacheRecord{
				Token: &pachinko.Token{
					Domain:   pachinko.Domain_STICKER,
					OwnerId:  ownerID,
					GroupId:  groupID,
					Quantity: 3,
				},
			}

			Convey("when the cached balance has been expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(-5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("HMGet", ctx, cacheKey, groupID).Return([]interface{}{
					string(cachedBytes),
				}, nil)
				redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(0), nil)

				result := cache.GetBatch(ctx, ownerID, []string{groupID})

				Convey("we should return nil", func() {
					So(result, ShouldHaveLength, 0)
				})

				Convey("we should delete the record from the cache", func() {
					// sleep a little bit because this happens async
					time.Sleep(time.Second * 3)
					redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
				})
			})

			Convey("when the cached balance has not expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("HMGet", ctx, cacheKey, groupID).Return([]interface{}{
					string(cachedBytes),
				}, nil)

				returnValue := cache.GetBatch(ctx, ownerID, []string{groupID})

				So(returnValue, ShouldHaveLength, 1)
				So(returnValue[groupID], ShouldResemble, cachedValue.Token)
			})
		})
	})
}

func TestCache_MultiGet(t *testing.T) {
	Convey("given a cache setter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		domain := cache.Config.TenantDomain

		ownerID1 := random.String(16)
		ownerID1CacheKey := cacheKey(domain, ownerID1)
		groupID1 := random.String(16)
		ownerGroupPairs := [][2]string{{ownerID1, groupID1}}
		cacheKeyedOwnerGroupPairs := [][2]string{{ownerID1CacheKey, groupID1}}

		Convey("when the redis client returns an error", func() {
			redisClient.On("PipelinedHGet", ctx, cacheKeyedOwnerGroupPairs).Return(nil, errors.New("ERROR"))
			result := cache.MultiGet(ctx, ownerGroupPairs...)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			redisClient.On("PipelinedHGet", ctx, cacheKeyedOwnerGroupPairs).Return(nil, nil)
			result := cache.MultiGet(ctx, ownerGroupPairs...)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			redisClient.On("PipelinedHGet", ctx, cacheKeyedOwnerGroupPairs).Return(map[[2]string]string{
				cacheKeyedOwnerGroupPairs[0]: "this is invalid json &*!#(",
			}, nil)
			redisClient.On("HDel", ctx, ownerID1CacheKey, groupID1).Return(int64(0), nil)

			result := cache.MultiGet(ctx, ownerGroupPairs...)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", ctx, ownerID1CacheKey, groupID1)
			})
		})

		Convey("when we do return a cached balance", func() {
			cachedValue := cacheRecord{
				Token: &pachinko.Token{
					Domain:   pachinko.Domain_STICKER,
					OwnerId:  ownerID1,
					GroupId:  groupID1,
					Quantity: 3,
				},
			}

			Convey("when the cached balance has been expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(-5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("PipelinedHGet", ctx, cacheKeyedOwnerGroupPairs).Return(map[[2]string]string{
					cacheKeyedOwnerGroupPairs[0]: string(cachedBytes),
				}, nil)
				redisClient.On("HDel", ctx, ownerID1CacheKey, groupID1).Return(int64(0), nil)

				result := cache.MultiGet(ctx, ownerGroupPairs...)

				Convey("we should return nil", func() {
					So(result, ShouldHaveLength, 0)
				})

				Convey("we should delete the record from the cache", func() {
					// sleep a little bit because this happens async
					time.Sleep(time.Second * 3)
					redisClient.AssertCalled(t, "HDel", ctx, ownerID1CacheKey, groupID1)
				})
			})

			Convey("when the cached balance has not expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("PipelinedHGet", ctx, cacheKeyedOwnerGroupPairs).Return(map[[2]string]string{
					cacheKeyedOwnerGroupPairs[0]: string(cachedBytes),
				}, nil)

				returnValue := cache.MultiGet(ctx, ownerGroupPairs...)

				So(returnValue, ShouldHaveLength, 1)
				So(returnValue[0], ShouldResemble, cachedValue.Token)
			})
		})
	})
}

func TestCache_GetAll(t *testing.T) {
	Convey("given a cache setter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		domain := cache.Config.TenantDomain

		cacheKey := cacheKey(domain, ownerID)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HGetAll", ctx, cacheKey).Return(nil, errors.New("ERROR"))
			result, totalResults := cache.GetAll(ctx, ownerID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
				So(totalResults, ShouldEqual, 0)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			redisClient.On("HGetAll", ctx, cacheKey).Return(nil, nil)
			result, totalResults := cache.GetAll(ctx, ownerID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
				So(totalResults, ShouldEqual, 0)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			redisClient.On("HGetAll", ctx, cacheKey).Return(map[string]string{
				groupID: "this is invalid json &*!#(",
			}, nil)
			redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(0), nil)

			result, totalResults := cache.GetAll(ctx, ownerID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
				So(totalResults, ShouldEqual, 0)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
			})
		})

		Convey("when we do return a cached balance", func() {
			cachedValue := cacheRecord{
				Token: &pachinko.Token{
					Domain:   pachinko.Domain_STICKER,
					OwnerId:  ownerID,
					GroupId:  groupID,
					Quantity: 3,
				},
			}

			Convey("when the cached balance has been expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(-5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("HGetAll", ctx, cacheKey).Return(map[string]string{
					groupID: string(cachedBytes),
				}, nil)
				redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(0), nil)

				result, totalResults := cache.GetAll(ctx, ownerID)

				Convey("we should return nil", func() {
					So(result, ShouldHaveLength, 0)
					So(totalResults, ShouldEqual, 1)
				})

				Convey("we should delete the record from the cache", func() {
					// sleep a little bit because this happens async
					time.Sleep(time.Second * 3)
					redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
				})
			})

			Convey("when the cached balance has not expired", func() {
				cachedValue.ExpiresAt = time.Now().Add(5 * time.Minute)

				cachedBytes, _ := json.Marshal(cachedValue)

				redisClient.On("HGetAll", ctx, cacheKey).Return(map[string]string{
					groupID: string(cachedBytes),
				}, nil)

				returnValue, totalResults := cache.GetAll(ctx, ownerID)

				So(totalResults, ShouldEqual, 1)
				So(returnValue, ShouldHaveLength, 1)
				So(returnValue[groupID], ShouldResemble, cachedValue.Token)
			})
		})
	})
}

func TestCache_Set(t *testing.T) {
	Convey("given a cache setter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		domain := cache.Config.TenantDomain

		cacheKey := cacheKey(domain, ownerID)
		token := &pachinko.Token{
			Domain:   domain,
			OwnerId:  ownerID,
			GroupId:  groupID,
			Quantity: 3,
		}

		redisClient.On("TTL", ctx, cacheKey).Return(time.Minute*5, nil)
		redisClient.On("Expire", ctx, cacheKey, mock.Anything).Return(true, nil)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HSet", ctx, cacheKey, groupID, mock.Anything).Return(false, errors.New("ERROR"))

			Convey("we should return nothing", func() {
				cache.Set(ctx, token)

				redisClient.AssertCalled(t, "HSet", ctx, cacheKey, groupID, mock.Anything)
			})
		})

		Convey("when the redis client does not return an error", func() {
			redisClient.On("HSet", ctx, cacheKey, groupID, mock.Anything).Return(true, nil)

			Convey("we should return nothing", func() {
				cache.Set(ctx, token)

				redisClient.AssertCalled(t, "HSet", ctx, cacheKey, groupID, mock.Anything)
			})
		})
	})
}

func TestCache_SetBatch(t *testing.T) {
	Convey("given a cache setter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		domain := cache.Config.TenantDomain

		cacheKey := cacheKey(domain, ownerID)
		tokens := map[string]*pachinko.Token{
			ownerID: {
				Domain:   pachinko.Domain_STICKER,
				OwnerId:  ownerID,
				GroupId:  groupID,
				Quantity: 3,
			},
		}

		redisClient.On("TTL", ctx, cacheKey).Return(time.Minute*5, nil)
		redisClient.On("Expire", ctx, cacheKey, mock.Anything).Return(true, nil)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HMSet", ctx, cacheKey, mock.Anything).Return("", errors.New("ERROR"))

			Convey("we should return nothing", func() {
				cache.SetBatch(ctx, ownerID, tokens)

				redisClient.AssertCalled(t, "HMSet", ctx, cacheKey, mock.Anything)
			})
		})

		Convey("when the redis client does not return an error", func() {
			redisClient.On("HMSet", ctx, cacheKey, mock.Anything).Return(cacheKey, nil)

			Convey("we should return nothing", func() {
				cache.SetBatch(ctx, ownerID, tokens)

				redisClient.AssertCalled(t, "HMSet", ctx, cacheKey, mock.Anything)
			})
		})
	})
}

func TestCache_MultiSet(t *testing.T) {
	Convey("given a cache setter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		domain := cache.Config.TenantDomain

		ownerID1 := random.String(16)
		groupID1 := random.String(16)

		ownerGroup1 := [2]string{cacheKey(domain, ownerID1), groupID1}
		ownerGroupTokenMap := map[[2]string]*pachinko.Token{
			ownerGroup1: {
				Domain:   pachinko.Domain_STICKER,
				OwnerId:  ownerID1,
				GroupId:  groupID1,
				Quantity: 3,
			},
		}

		redisClient.On("TTL", ctx, mock.Anything).Return(time.Minute*5, nil)
		redisClient.On("Expire", ctx, mock.Anything, mock.Anything).Return(true, nil)

		Convey("when the redis client returns an error", func() {
			redisClient.On("PipelinedHSet", ctx, mock.Anything).Return(nil, errors.New("ERROR"))

			Convey("we should return nothing", func() {
				cache.MultiSet(ctx, ownerGroupTokenMap)

				redisClient.AssertCalled(t, "PipelinedHSet", ctx, mock.Anything)
			})
		})

		Convey("when the redis client does not return an error", func() {
			redisClient.On("PipelinedHSet", ctx, mock.Anything).Return(make(map[[2]string]bool), nil)

			Convey("we should return nothing", func() {
				cache.MultiSet(ctx, ownerGroupTokenMap)

				redisClient.AssertCalled(t, "PipelinedHSet", ctx, mock.Anything)
			})
		})
	})
}

func TestCache_Del(t *testing.T) {
	Convey("given a cache deleter", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := "123123123"
		groupID := "321321321"
		domain := cache.Config.TenantDomain

		cacheKey := cacheKey(domain, ownerID)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(0), errors.New("ERROR"))

			Convey("we should return nothing", func() {
				cache.Del(ctx, ownerID, groupID)
				redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
			})
		})

		Convey("when the redis client does not return an error", func() {
			redisClient.On("HDel", ctx, cacheKey, groupID).Return(int64(1), nil)

			Convey("we should return nothing", func() {
				cache.Del(ctx, ownerID, groupID)
				redisClient.AssertCalled(t, "HDel", ctx, cacheKey, groupID)
			})
		})
	})
}
