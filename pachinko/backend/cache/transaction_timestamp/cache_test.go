package transaction_timestamp

import (
	"context"
	"strconv"
	"testing"
	"time"

	"code.justin.tv/commerce/pachinko/config"
	redis_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/redis"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("given a transaction cache", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		transactionID := uuid.Must(uuid.NewV4()).String()

		cacheKey := cacheKey(pachinko.Domain_STICKER, transactionID)

		Convey("when the redis client returns an error", func() {
			redisClient.On("Get", ctx, cacheKey).Return("", errors.New("ERROR"))
			result := cache.Get(ctx, transactionID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			redisClient.On("Get", ctx, cacheKey).Return("", nil)
			result := cache.Get(ctx, transactionID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid value", func() {
			redisClient.On("Get", ctx, cacheKey).Return("this is invalid json &*!#(", nil)
			redisClient.On("Del", ctx, cacheKey).Return(int64(0), nil)

			result := cache.Get(ctx, transactionID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				// sleep a little bit because this happens async
				time.Sleep(time.Second * 3)
				redisClient.AssertCalled(t, "Del", ctx, cacheKey)
			})
		})

		Convey("when we return the pending transaction", func() {
			cacheRecord := time.Now().Add(5 * time.Minute)
			cacheRecordUnix := cacheRecord.UnixNano()
			redisClient.On("Get", ctx, cacheKey).Return(strconv.FormatInt(cacheRecordUnix, 10), nil)

			returnValue := cache.Get(ctx, transactionID)

			Convey("we should return the timestamp we cached", func() {
				So(returnValue, ShouldNotBeNil)
				val := *returnValue
				So(val.UnixNano(), ShouldEqual, cacheRecordUnix)
			})
		})
	})
}

func TestCache_Set(t *testing.T) {
	Convey("given a transaction cache", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		transactionID := uuid.Must(uuid.NewV4()).String()
		ts := time.Now()

		cacheKey := cacheKey(pachinko.Domain_STICKER, transactionID)

		Convey("when redis returns an error", func() {
			redisClient.On("Set", ctx, cacheKey, strconv.FormatInt(ts.UnixNano(), 10), cacheTTL).Return("", errors.New("WALRUS STRIKE"))

			err := cache.Set(ctx, transactionID, ts)
			So(err, ShouldNotBeNil)
		})

		Convey("when redis succeeds", func() {
			redisClient.On("Set", ctx, cacheKey, strconv.FormatInt(ts.UnixNano(), 10), cacheTTL).Return("", nil)

			err := cache.Set(ctx, transactionID, ts)
			So(err, ShouldBeNil)
		})
	})
}
