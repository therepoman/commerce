package transaction_timestamp

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/circuit"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/afex/hystrix-go/hystrix"
	go_redis "github.com/go-redis/redis"
)

const (
	cacheKeyFormat = "transaction-timestamp.%s.%s"
	cacheTTL       = 10 * time.Minute
)

type Cache interface {
	Get(ctx context.Context, transactionID string) *time.Time
	Set(ctx context.Context, transactionID string, timestamp time.Time) error
	Del(ctx context.Context, transactionID string)
}

type cache struct {
	Config  *config.Config  `inject:""`
	Redis   redis.Client    `inject:"redis-tx-ts"`
	Statter metrics.Statter `inject:""`
}

func NewCache() Cache {
	return &cache{}
}

func cacheKey(domain pachinko.Domain, transactionID string) string {
	return fmt.Sprintf(cacheKeyFormat, domain.String(), transactionID)
}

func (c *cache) Get(ctx context.Context, transactionID string) *time.Time {
	var cachedValue string
	err := hystrix.Do(circuit.GetTransactionTimestampCacheCommand, func() error {
		var err error
		cachedValue, err = c.Redis.Get(ctx, cacheKey(c.Config.TenantDomain, transactionID))

		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("could not fetching token balance transaction timestamp from cache")
			return err
		}
		return nil
	}, nil)
	if err != nil {
		return nil
	}

	if strings.Blank(cachedValue) {
		return nil
	}

	unixTimeNano, err := strconv.ParseInt(cachedValue, 10, 64)
	if err != nil {
		go c.Del(context.Background(), transactionID)
		logrus.WithError(err).WithField("value", cachedValue).Error("could not parse cached value into int")
		return nil
	}

	ts := time.Unix(0, unixTimeNano)
	return &ts
}

func (c *cache) Set(ctx context.Context, transactionID string, timestamp time.Time) error {
	unixTimeNano := timestamp.UnixNano()

	err := hystrix.Do(circuit.SetTransactionTimestampCacheCommand, func() error {
		_, err := c.Redis.Set(ctx, cacheKey(c.Config.TenantDomain, transactionID), strconv.FormatInt(unixTimeNano, 10), cacheTTL)
		if err != nil {
			logrus.WithError(err).WithField("transactionID", transactionID).Error("failed to set transaction timestamp in cache")
			return err
		}
		return nil
	}, nil)
	if err != nil {
		return err
	}

	return nil
}

func (c *cache) Del(ctx context.Context, transactionID string) {
	_ = hystrix.Do(circuit.DelTransactionTimestampCacheCommand, func() error {
		_, err := c.Redis.Del(ctx, cacheKey(c.Config.TenantDomain, transactionID))
		if err != nil {
			logrus.WithError(err).WithField("transactionID", transactionID).Error("failed to delete entry from cache")
		}
		return nil
	}, nil)
}
