package transaction

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/circuit"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/afex/hystrix-go/hystrix"
	go_redis "github.com/go-redis/redis"
)

const (
	cacheKeyFormat = "transaction.%s.%s.%s"
	cacheTTL       = 30 * time.Second

	getCacheLatency    = "get-transaction-cache-latency"
	getAllCacheLatency = "get-transaction-all-cache-latency"
	setCacheLatency    = "set-transaction-cache-latency"
	delCacheLatency    = "del-transaction-cache-latency"
)

type cacheRecord struct {
	Transaction *models.Transaction `json:"transaction"`
	ExpiresAt   time.Time           `json:"expires_at"`
}

type Cache interface {
	Get(ctx context.Context, token pachinko.Token, transactionID string) (*models.Transaction, error)
	GetAll(ctx context.Context, token pachinko.Token) (map[string]*models.Transaction, error)
	Add(ctx context.Context, token pachinko.Token, transaction models.Transaction) error
	Remove(ctx context.Context, token pachinko.Token, transactionID string) error
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	Config  *config.Config  `inject:""`
	Redis   redis.Client    `inject:"redis-txs"`
	Statter metrics.Statter `inject:""`
}

func cacheKey(domain pachinko.Domain, token pachinko.Token) string {
	return fmt.Sprintf(cacheKeyFormat, domain.String(), token.OwnerId, token.GroupId)
}

func (c *cache) Get(ctx context.Context, token pachinko.Token, transactionID string) (*models.Transaction, error) {
	var cachedValue string
	err := hystrix.Do(circuit.GetTransactionCacheCommand, func() error {
		var err error
		start := time.Now()
		cachedValue, err = c.Redis.HGet(ctx, cacheKey(c.Config.TenantDomain, token), transactionID)
		go c.Statter.Duration(getCacheLatency, time.Since(start))

		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("could not fetching token balance transaction from cache")
			return err
		}
		return nil
	}, nil)
	if err != nil {
		return nil, err
	}

	if strings.Blank(cachedValue) {
		return nil, nil
	}

	var record cacheRecord
	err = json.Unmarshal([]byte(cachedValue), &record)
	if err != nil {
		c.del(context.Background(), token, transactionID)
		logrus.WithError(err).Error("could not unmarshal cached token balance transactions entry")
		return nil, err
	}

	if time.Now().After(record.ExpiresAt) {
		c.del(context.Background(), token, transactionID)
		return nil, nil
	}

	return record.Transaction, nil
}

func (c *cache) GetAll(ctx context.Context, token pachinko.Token) (map[string]*models.Transaction, error) {
	var cachedValues map[string]string
	err := hystrix.Do(circuit.GetTransactionsAllCacheCommand, func() error {
		var err error
		start := time.Now()
		cachedValues, err = c.Redis.HGetAll(ctx, cacheKey(c.Config.TenantDomain, token))
		go c.Statter.Duration(getAllCacheLatency, time.Since(start))
		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("could not fetch token balances from cache")
			return err
		}
		return nil
	}, nil)
	if err != nil {
		return nil, err
	}

	if len(cachedValues) < 1 {
		return nil, nil
	}

	transactions, expiredRecords := map[string]*models.Transaction{}, make([]*models.Transaction, 0)
	for transactionID, value := range cachedValues {
		var record cacheRecord
		err := json.Unmarshal([]byte(value), &record)
		if err != nil {
			c.del(context.Background(), token, transactionID)
			logrus.WithError(err).Error("could not unmarshal cached token balances entry")
			return nil, err
		}

		if time.Now().After(record.ExpiresAt) {
			expiredRecords = append(expiredRecords, record.Transaction)
		} else {
			transactions[record.Transaction.ID] = record.Transaction
		}
	}

	go c.cleanExpired(context.Background(), token, expiredRecords)

	return transactions, nil
}

func (c *cache) Add(ctx context.Context, token pachinko.Token, transaction models.Transaction) error {
	expiresAt, err := c.ttl(ctx, token)
	if err != nil {
		return err
	}

	cacheRecord := cacheRecord{
		Transaction: &transaction,
		ExpiresAt:   expiresAt,
	}

	cacheValue, err := json.Marshal(&cacheRecord)
	if err != nil {
		logrus.WithError(err).Error("could not marshal token balance transactions entry for caching")
		return err
	}

	err = hystrix.Do(circuit.SetTransactionCacheCommand, func() error {
		start := time.Now()
		_, err = c.Redis.HSet(ctx, cacheKey(c.Config.TenantDomain, token), transaction.ID, string(cacheValue))
		go c.Statter.Duration(setCacheLatency, time.Since(start))

		if err != nil {
			logrus.WithFields(logrus.Fields{
				"ownerID":       token.OwnerId,
				"groupID":       token.GroupId,
				"domain":        token.Domain.String(),
				"transactionID": transaction.ID,
			}).WithError(err).Error("could not push transaction into cache key")
			return err
		}
		return nil
	}, nil)

	go func() {
		_, err := c.ttl(context.Background(), token)
		if err != nil {
			logrus.WithError(err).Error("failed to set TTL after adding transaction to cache")
		}
	}()

	return err
}

func (c *cache) Remove(ctx context.Context, token pachinko.Token, transactionID string) error {
	fields := logrus.Fields{
		"ownerID":       token.OwnerId,
		"groupID":       token.GroupId,
		"domain":        token.Domain.String(),
		"transactionID": transactionID,
	}

	return hystrix.Do(circuit.DelTransactionCacheCommand, func() error {
		start := time.Now()
		_, err := c.Redis.HDel(ctx, cacheKey(c.Config.TenantDomain, token), transactionID)
		go c.Statter.Duration(delCacheLatency, time.Since(start))
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not remove transaction from cache on balance")
		}
		return err
	}, nil)
}

func (c *cache) ttl(ctx context.Context, token pachinko.Token) (time.Time, error) {
	now := time.Now()
	newExpiresAt := now.Add(cacheTTL)

	err := hystrix.Do(circuit.ExpireTransactionCacheCommand, func() error {
		// Set the TTL of the transaction cache entry to the default TTL
		_, err := c.Redis.Expire(ctx, cacheKey(c.Config.TenantDomain, token), cacheTTL)
		// if we fail to set the TTL based on either a redis error, we just return the current TTL on the existing transactions hash.
		if err != nil && err != go_redis.Nil {
			logrus.WithError(err).Error("failed to reset TTL for new key")
			return err
		}
		return nil
	}, nil)

	return newExpiresAt, err
}

func (c *cache) del(ctx context.Context, token pachinko.Token, transactionID string) {
	_ = hystrix.Do(circuit.DelTransactionCacheCommand, func() error {
		start := time.Now()
		if _, err := c.Redis.HDel(ctx, cacheKey(c.Config.TenantDomain, token), transactionID); err != nil {
			logrus.WithError(err).Error("could not deleted cached token balance transaction")
		}
		go c.Statter.Duration(delCacheLatency, time.Since(start))
		return nil
	}, nil)
}

func (c *cache) cleanExpired(ctx context.Context, token pachinko.Token, transactions []*models.Transaction) {
	for _, transaction := range transactions {
		c.del(ctx, token, transaction.ID)
	}
}
