package transaction

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	"code.justin.tv/commerce/pachinko/config"
	redis_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/redis"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("given a transaction cache", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}
		transactionID := uuid.NewV4().String()

		cacheKey := cacheKey(pachinko.Domain_STICKER, reqToken)

		Convey("when the redis client returns an error", func() {
			redisClient.On("HGet", ctx, cacheKey, transactionID).Return("", errors.New("ERROR"))
			result, err := cache.Get(ctx, reqToken, transactionID)

			Convey("we should return nil", func() {
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			redisClient.On("HGet", ctx, cacheKey, transactionID).Return("", nil)
			result, err := cache.Get(ctx, reqToken, transactionID)

			Convey("we should return nil", func() {
				So(err, ShouldBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			redisClient.On("HGet", ctx, cacheKey, transactionID).Return("this is invalid json &*!#(", nil)
			redisClient.On("HDel", ctx, cacheKey, transactionID).Return(int64(0), nil)

			result, err := cache.Get(ctx, reqToken, transactionID)

			Convey("we should return nil", func() {
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", ctx, cacheKey, transactionID)
			})
		})

		Convey("when we return the pending transaction", func() {
			tokenTransaction := models.Transaction{
				ID:        transactionID,
				Operation: pachinko.Operation_CONSUME,
				Amount:    -3,
			}

			cacheRecord := cacheRecord{
				Transaction: &tokenTransaction,
			}

			Convey("when the transaction is expired", func() {
				cacheRecord.ExpiresAt = time.Now().Add(-5 * time.Minute)

				cachedBytes, _ := json.Marshal(&cacheRecord)

				redisClient.On("HGet", ctx, cacheKey, transactionID).Return(string(cachedBytes), nil)
				redisClient.On("HDel", ctx, cacheKey, transactionID).Return(int64(0), nil)

				returnValue, err := cache.Get(ctx, reqToken, transactionID)

				Convey("we should return nil", func() {
					So(err, ShouldBeNil)
					So(returnValue, ShouldBeNil)
				})

				Convey("we should delete the record from the cache", func() {
					time.Sleep(3 * time.Second)
					redisClient.AssertCalled(t, "HDel", ctx, cacheKey, transactionID)
				})
			})

			Convey("when the transaction is not expired", func() {
				cacheRecord.ExpiresAt = time.Now().Add(5 * time.Minute)

				cachedBytes, _ := json.Marshal(&cacheRecord)

				redisClient.On("HGet", ctx, cacheKey, transactionID).Return(string(cachedBytes), nil)

				returnValue, err := cache.Get(ctx, reqToken, transactionID)

				Convey("we should return nil and an error", func() {
					So(err, ShouldBeNil)
					So(returnValue, ShouldResemble, &tokenTransaction)
				})
			})
		})
	})
}

func TestCache_GetAll(t *testing.T) {
	Convey("given a transaction cache", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}
		transactionID := uuid.NewV4().String()

		cacheKey := cacheKey(pachinko.Domain_STICKER, reqToken)

		Convey("when HGetAll returns an error", func() {
			redisClient.On("HGetAll", ctx, cacheKey).Return(nil, errors.New("WALRUS STRIKE"))

			result, err := cache.GetAll(ctx, reqToken)

			Convey("we should return nil and an error", func() {
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("when the HGetAll returns invalid interface for a cache record", func() {
			redisClient.On("HGetAll", ctx, cacheKey).Return(map[string]string{
				transactionID: "this is invalid json &*!#(",
			}, nil)
			redisClient.On("HDel", ctx, cacheKey, transactionID).Return(int64(0), nil)

			result, err := cache.GetAll(ctx, reqToken)

			Convey("we should return nil and an error", func() {
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				time.Sleep(time.Second * 3)
				redisClient.AssertCalled(t, "HDel", ctx, cacheKey, transactionID)
			})
		})

		Convey("when the HGetAll returns the pending transactions", func() {
			tokenTransaction := models.Transaction{
				ID:        transactionID,
				Operation: pachinko.Operation_CONSUME,
				Amount:    -3,
			}

			cacheRecord := cacheRecord{
				Transaction: &tokenTransaction,
			}

			Convey("when the transaction is expired", func() {
				cacheRecord.ExpiresAt = time.Now().Add(-5 * time.Minute)

				cachedBytes, _ := json.Marshal(&cacheRecord)

				redisClient.On("HGetAll", ctx, cacheKey).Return(map[string]string{
					transactionID: string(cachedBytes),
				}, nil)
				redisClient.On("HDel", ctx, cacheKey, transactionID).Return(int64(0), nil)

				returnValue, err := cache.GetAll(ctx, reqToken)

				Convey("we should return nil and an error", func() {
					So(err, ShouldBeNil)
					So(returnValue, ShouldHaveLength, 0)
				})

				Convey("we should delete the record from the cache", func() {
					time.Sleep(time.Second * 3)
					redisClient.AssertCalled(t, "HDel", ctx, cacheKey, transactionID)
				})
			})

			Convey("when the transaction is not expired", func() {
				cacheRecord.ExpiresAt = time.Now().Add(5 * time.Minute)

				cachedBytes, _ := json.Marshal(&cacheRecord)

				redisClient.On("HGetAll", ctx, cacheKey).Return(map[string]string{
					transactionID: string(cachedBytes),
				}, nil)

				returnValue, err := cache.GetAll(ctx, reqToken)

				Convey("we should return nil and an error", func() {
					So(err, ShouldBeNil)
					So(returnValue[transactionID], ShouldResemble, &tokenTransaction)
				})
			})
		})
	})
}

func TestCache_Add(t *testing.T) {
	Convey("given a transaction cache", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}

		transactionID := uuid.NewV4().String()

		cacheKey := cacheKey(pachinko.Domain_STICKER, reqToken)

		tokenTransaction := models.Transaction{
			ID:        transactionID,
			Operation: pachinko.Operation_CONSUME,
			Amount:    -3,
		}

		redisClient.On("TTL", ctx, cacheKey).Return(time.Minute*5, nil)
		redisClient.On("Expire", ctx, cacheKey, mock.Anything).Return(true, nil)

		Convey("when the redis client fails to RPush", func() {
			redisClient.On("HSet", ctx, cacheKey, transactionID, mock.Anything).Return(false, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				err := cache.Add(ctx, reqToken, tokenTransaction)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the redis client succeeds to RPush", func() {
			redisClient.On("HSet", ctx, cacheKey, transactionID, mock.Anything).Return(true, nil)

			Convey("then we should return nil", func() {
				err := cache.Add(ctx, reqToken, tokenTransaction)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestCache_Remove(t *testing.T) {
	Convey("given a transaction cache", t, func() {
		redisClient := new(redis_mock.Client)
		statter := new(metrics_mock.Statter)

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		cache := &cache{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
			Redis:   redisClient,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}
		transactionID := uuid.NewV4().String()

		cacheKey := cacheKey(pachinko.Domain_STICKER, reqToken)

		Convey("when the redis client fails to RPush", func() {
			redisClient.On("HDel", ctx, cacheKey, transactionID).Return(int64(0), errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				err := cache.Remove(ctx, reqToken, transactionID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the redis client succeeds to RPush", func() {
			redisClient.On("HDel", ctx, cacheKey, transactionID).Return(int64(1), nil)

			Convey("then we should return nil", func() {
				err := cache.Remove(ctx, reqToken, transactionID)
				So(err, ShouldBeNil)
			})
		})
	})
}
