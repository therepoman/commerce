package balancelock

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	lock_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/lock"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_LockTokenBalance(t *testing.T) {
	Convey("given a balance lock cache", t, func() {
		lockingClient := new(lock_mock.LockingClient)

		cache := &cache{
			Lock: lockingClient,
		}

		ctx := context.Background()

		ownerID := random.String(16)
		groupID := random.String(16)
		domain := pachinko.Domain_STICKER
		reqToken := pachinko.Token{
			OwnerId: ownerID,
			GroupId: groupID,
			Domain:  domain,
		}

		Convey("when we fail to obtain a lock", func() {
			lockingClient.On("ObtainLock", ctx, lockCacheKey(reqToken)).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("we should return the error", func() {
				_, err := cache.LockTokenBalance(ctx, reqToken)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we succeed at obtaining a lock", func() {
			lockingClient.On("ObtainLock", ctx, lockCacheKey(reqToken)).Return(nil, nil)

			Convey("we should return the success", func() {
				_, err := cache.LockTokenBalance(ctx, reqToken)
				So(err, ShouldBeNil)
			})
		})
	})
}
