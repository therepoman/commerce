package balancelock

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/pachinko/backend/clients/lock"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

const (
	lockCacheKeyFormat = "balance.%s.%s.%s"
)

type Cache interface {
	LockTokenBalance(ctx context.Context, token pachinko.Token) (lock.Lock, error)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	Lock lock.LockingClient `inject:""`
}

func lockCacheKey(token pachinko.Token) string {
	return fmt.Sprintf(lockCacheKeyFormat, token.Domain.String(), token.OwnerId, token.GroupId)
}

func (c *cache) LockTokenBalance(ctx context.Context, token pachinko.Token) (lock.Lock, error) {
	return c.Lock.ObtainLock(ctx, lockCacheKey(token))
}
