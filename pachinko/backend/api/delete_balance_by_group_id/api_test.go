package delete_balance_by_group_id

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	deleteBalanceByGroupIdAPIMock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id"
	sqsMock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sqs"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestApi_Delete(t *testing.T) {
	Convey("given a delete balance by group id API", t, func() {
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants:      &tenant.Wrapper{},
		}

		validator := new(deleteBalanceByGroupIdAPIMock.Validator)
		sqsClient := new(sqsMock.Client)

		api := &api{
			Config:    c,
			Validator: validator,
			SQSClient: sqsClient,
		}
		fakeSQSUrl := "test-url"

		ctx := context.Background()

		Convey("when the request is invalid, we should error", func() {
			req := &pachinko.DeleteBalanceByGroupIdReq{}
			validator.On("IsValid", ctx, req).Return(twirp.InvalidArgumentError("request", "GroupId is missing"))
			_, err := api.Delete(ctx, req)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &pachinko.DeleteBalanceByGroupIdReq{
				GroupId: "1234567890",
			}
			validator.On("IsValid", ctx, req).Return(nil)

			Convey("when we fail to fetch the tenant config", func() {
				c := &config.Config{
					TenantDomain: pachinko.Domain_STICKER,
					Tenants:      &tenant.Wrapper{},
				}
				api.Config = c
				_, err := api.Delete(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("when we fail to fetch the SQS URL", func() {
				tenantConf := &tenant.Config{}
				c := &config.Config{
					TenantDomain: pachinko.Domain_STICKER,
					Tenants: &tenant.Wrapper{
						"sticker": tenantConf,
					},
				}
				api.Config = c
				_, err := api.Delete(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("when we get the tenant config successfully", func() {
				tenantConf := &tenant.Config{
					DeleteBalanceByGroupIdSQSConfig: &tenant.SQSConfig{
						URL: fakeSQSUrl,
					},
				}
				c := &config.Config{
					TenantDomain: pachinko.Domain_STICKER,
					Tenants: &tenant.Wrapper{
						"sticker": tenantConf,
					},
				}
				api.Config = c

				Convey("when adding message to sqs fails", func() {
					sqsClient.On("SendMessage", ctx, fakeSQSUrl, req.GroupId).Return(errors.New("Add to SQS ERROR"))

					_, err := api.Delete(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("when adding message to sqs succeeds", func() {
					sqsClient.On("SendMessage", ctx, fakeSQSUrl, req.GroupId).Return(nil)

					resp, err := api.Delete(ctx, req)
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &pachinko.DeleteBalanceByGroupIdResp{})
				})
			})
		})
	})
}
