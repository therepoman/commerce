package delete_balance_by_group_id

import (
	"context"

	sqsClient "code.justin.tv/commerce/pachinko/backend/clients/sqs"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Delete(ctx context.Context, req *pachinko.DeleteBalanceByGroupIdReq) (*pachinko.DeleteBalanceByGroupIdResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validator Validator        `inject:""`
	SQSClient sqsClient.Client `inject:""`
	Config    *config.Config   `inject:""`
}

func (a *api) Delete(ctx context.Context, req *pachinko.DeleteBalanceByGroupIdReq) (*pachinko.DeleteBalanceByGroupIdResp, error) {
	err := a.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	tenantConf := a.Config.Tenants.Get(a.Config.TenantDomain)
	if tenantConf == nil {
		return nil, twirp.InternalError("Failed to get tenant config")
	}

	var queueURL string
	if tenantConf.DeleteBalanceByGroupIdSQSConfig != nil {
		queueURL = tenantConf.DeleteBalanceByGroupIdSQSConfig.URL
	}

	if queueURL == "" {
		return nil, twirp.InternalError("Failed to get tenant config")
	}

	err = a.SQSClient.SendMessage(ctx, queueURL, req.GroupId)
	if err != nil {
		return nil, twirp.InternalError("Failed to save request to delete balance by group ID")
	}

	return &pachinko.DeleteBalanceByGroupIdResp{}, nil
}
