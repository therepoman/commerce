package delete_balance_by_group_id

import (
	"context"
	"testing"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a delete balance by group id request validator", t, func() {
		v := &validator{}
		ctx := context.Background()

		Convey("when the request is nil we return an error", func() {
			err := v.IsValid(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has no group ID we return an error", func() {
			err := v.IsValid(ctx, &pachinko.DeleteBalanceByGroupIdReq{})
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid request", func() {
			err := v.IsValid(ctx, &pachinko.DeleteBalanceByGroupIdReq{
				GroupId: "1231231123",
			})
			So(err, ShouldBeNil)
		})
	})
}
