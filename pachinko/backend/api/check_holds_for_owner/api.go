package check_holds_for_owner

import (
	"context"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Check(ctx context.Context, req *pachinko.CheckHoldsForOwnerReq) (*pachinko.CheckHoldsForOwnerResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
}

func (a *api) Check(ctx context.Context, req *pachinko.CheckHoldsForOwnerReq) (*pachinko.CheckHoldsForOwnerResp, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Holds are no longer supported in Pachinko.")
}
