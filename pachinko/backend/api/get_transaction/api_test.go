package get_transaction

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI(t *testing.T) {
	Convey("Given GetTransaction API", t, func() {
		transactionDAO := new(transactions_mock.DAO)
		api := api{
			TransactionDAO: transactionDAO,
		}
		ctx := context.Background()
		requestedTransactionID := "pagliacci-bits-1122"
		req := &pachinko.GetTransactionRecordReq{
			TransactionId: requestedTransactionID,
		}
		desiredTransaction := dynamo.Transaction{
			ID: requestedTransactionID,
		}

		Convey("when DAO errors", func() {
			transactionDAO.On("GetTransactionRecord", ctx, requestedTransactionID).Return(nil, errors.New("boom")).Once()

			_, err := api.Get(ctx, req)
			So(err, ShouldNotBeNil)
		})

		Convey("when transaction is not found", func() {
			transactionDAO.On("GetTransactionRecord", ctx, requestedTransactionID).Return(nil, nil).Once()

			resp, err := api.Get(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &pachinko.GetTransactionRecordResp{})
		})

		Convey("when transaction is found", func() {
			transactionDAO.On("GetTransactionRecord", ctx, requestedTransactionID).Return(&desiredTransaction, nil).Once()

			resp, err := api.Get(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &pachinko.GetTransactionRecordResp{
				Transaction: desiredTransaction.ToTwirpTransactionRecord(),
			})
		})
	})
}
