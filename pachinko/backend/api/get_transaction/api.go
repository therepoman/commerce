package get_transaction

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachinko.GetTransactionRecordReq) (*pachinko.GetTransactionRecordResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	TransactionDAO transactions.DAO `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachinko.GetTransactionRecordReq) (*pachinko.GetTransactionRecordResp, error) {
	record, err := a.TransactionDAO.GetTransactionRecord(ctx, req.TransactionId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if record == nil {
		return &pachinko.GetTransactionRecordResp{}, nil
	}

	return &pachinko.GetTransactionRecordResp{
		Transaction: record.ToTwirpTransactionRecord(),
	}, nil
}
