package add_tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	add_tokens_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/add_tokens"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAddTokensApi_Add(t *testing.T) {
	Convey("given a server ", t, func() {
		entitler := new(tokens_mock.Entitler)
		validator := new(add_tokens_api_mock.Validator)
		tokenGetter := new(tokens_mock.Getter)

		api := &api{
			Entitler:    entitler,
			Validator:   validator,
			TokenGetter: tokenGetter,
		}

		ctx := context.Background()
		token := &pachinko.Token{
			OwnerId:  random.String(16),
			GroupId:  random.String(16),
			Domain:   pachinko.Domain_STICKER,
			Quantity: random.Int64(1, 16),
		}

		req := &pachinko.AddTokensReq{
			TransactionId: random.String(32),
			Token:         token,
		}

		Convey("when the request isn't valid", func() {
			Convey("when there is an actual bad request", func() {
				validator.On("IsValid", ctx, req).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := api.Add(ctx, req)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "WALRUS STRIKE")
			})

			Convey("when the balance is frozen", func() {
				validator.On("IsValid", ctx, req).Return(&pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_BALANCE_FROZEN,
				}, nil)
				tokenGetter.On("Get", ctx, req.Token.OwnerId, req.Token.GroupId).Return(&pachinko.Token{
					OwnerId:  req.Token.OwnerId,
					GroupId:  req.Token.GroupId,
					Domain:   pachinko.Domain_STICKER,
					Quantity: 100,
				}, nil)

				resp, err := api.Add(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ValidationError.Code, ShouldEqual, pachinko.ValidationErrorCode_BALANCE_FROZEN)
			})

			Convey("when the transaction is already committed", func() {
				validator.On("IsValid", ctx, req).Return(&pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
				}, nil)

				Convey("when retrieving the latest token balance fails", func() {
					tokenGetter.On("Get", ctx, req.Token.OwnerId, req.Token.GroupId).Return(nil, errors.New("ohki strike"))

					resp, err := api.Add(ctx, req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})

				Convey("when retrieving the latest token balance succeeds", func() {
					tokenGetter.On("Get", ctx, req.Token.OwnerId, req.Token.GroupId).Return(&pachinko.Token{
						OwnerId:  req.Token.OwnerId,
						GroupId:  req.Token.GroupId,
						Domain:   pachinko.Domain_STICKER,
						Quantity: 100,
					}, nil)

					resp, err := api.Add(ctx, req)
					So(err, ShouldBeNil)
					So(resp.ValidationError, ShouldNotBeNil)
					So(resp.ValidationError.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
					So(resp.Token.Quantity, ShouldEqual, 100)
				})
			})
		})

		Convey("when the request is valid", func() {
			validator.On("IsValid", ctx, req).Return(nil, nil)
			tokenGetter.On("Get", ctx, req.Token.OwnerId, req.Token.GroupId).Return(&pachinko.Token{
				OwnerId:  req.Token.OwnerId,
				GroupId:  req.Token.GroupId,
				Domain:   pachinko.Domain_STICKER,
				Quantity: 100,
			}, nil)

			Convey("add tokens returned an error", func() {
				entitler.On("Entitle", ctx, req.TransactionId, mock.Anything).Return(nil, errors.New("anime not allowed"))
				_, err := api.Add(ctx, req)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error internal: anime not allowed")
			})

			Convey("add tokens succeeded", func() {
				updatedToken := &pachinko.Token{
					OwnerId:  token.OwnerId,
					Domain:   pachinko.Domain_STICKER,
					GroupId:  token.GroupId,
					Quantity: int64(322),
				}
				entitler.On("Entitle", ctx, req.TransactionId, mock.Anything).Return(updatedToken, nil)

				resp, err := api.Add(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Token, ShouldNotBeNil)
				So(resp.Token.Quantity, ShouldEqual, int64(322))
			})
		})
	})
}
