package add_tokens

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Add(ctx context.Context, req *pachinko.AddTokensReq) (*pachinko.AddTokensResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Entitler    tokens.Entitler `inject:""`
	Validator   Validator       `inject:""`
	TokenGetter tokens.Getter   `inject:""`
}

func (s *api) Add(ctx context.Context, req *pachinko.AddTokensReq) (*pachinko.AddTokensResp, error) {
	validationErr, err := s.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	currentToken, err := s.TokenGetter.Get(ctx, req.Token.OwnerId, req.Token.GroupId)
	if err != nil {
		return nil, twirp.InternalError("something went wrong")
	}

	if currentToken.Status == pachinko.TokenStatus_FROZEN {
		validationErr = &pachinko.ValidationError{
			Code: pachinko.ValidationErrorCode_BALANCE_FROZEN,
		}
	}

	if validationErr != nil {
		return &pachinko.AddTokensResp{
			Token:           currentToken,
			ValidationError: validationErr,
		}, nil
	}

	newToken, err := s.Entitler.Entitle(ctx, req.TransactionId, *req.Token)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &pachinko.AddTokensResp{
		Token: newToken,
	}, nil
}
