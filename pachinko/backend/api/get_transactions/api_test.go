package get_transactions

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachinko/backend/dynamo"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI(t *testing.T) {
	Convey("Given GetTransactions API", t, func() {
		transactionDAO := new(transactions_mock.DAO)
		api := api{
			TransactionDAO: transactionDAO,
		}
		ctx := context.Background()
		requestedTransactionIDs := []string{"pagliacci-bits-1122"}
		req := &pachinko.GetTransactionRecordsReq{
			TransactionIds: requestedTransactionIDs,
		}
		desiredTransactions := []dynamo.Transaction{
			{
				ID: requestedTransactionIDs[0],
			},
		}

		Convey("when DAO errors", func() {
			transactionDAO.On("BatchGetTransactionRecords", ctx, requestedTransactionIDs).Return(nil, errors.New("boom")).Once()

			_, err := api.Get(ctx, req)
			So(err, ShouldNotBeNil)
		})

		Convey("when are are some transactions", func() {
			transactionDAO.On("BatchGetTransactionRecords", ctx, requestedTransactionIDs).Return(desiredTransactions, nil).Once()

			resp, err := api.Get(ctx, req)
			So(err, ShouldBeNil)

			expectedTransactionRecords := []*pachinko.TransactionRecord{}
			for _, desiredTransaction := range desiredTransactions {
				expectedTransactionRecords = append(expectedTransactionRecords, desiredTransaction.ToTwirpTransactionRecord())
			}

			So(resp, ShouldResemble, &pachinko.GetTransactionRecordsResp{
				Transactions: expectedTransactionRecords,
			})
		})
	})
}
