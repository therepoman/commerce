package get_transactions

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachinko.GetTransactionRecordsReq) (*pachinko.GetTransactionRecordsResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	TransactionDAO transactions.DAO `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachinko.GetTransactionRecordsReq) (*pachinko.GetTransactionRecordsResp, error) {
	transactionRecords := make([]*pachinko.TransactionRecord, 0)
	transactions, err := a.TransactionDAO.BatchGetTransactionRecords(ctx, req.TransactionIds)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	for _, transaction := range transactions {
		transactionRecords = append(transactionRecords, transaction.ToTwirpTransactionRecord())
	}

	return &pachinko.GetTransactionRecordsResp{
		Transactions: transactionRecords,
	}, nil
}
