package start_transaction

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/backend/api/validation"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *pachinko.StartTransactionReq) (*pachinko.ValidationError, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	CommittedTransactions dynamo_tx.DAO     `inject:""`
	Tokens                validation.Tokens `inject:""`
}

func (v *validator) IsValid(ctx context.Context, req *pachinko.StartTransactionReq) (*pachinko.ValidationError, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "body is missing")
	}

	_, ok := pachinko.Operation_name[int32(req.Operation)]
	if !ok {
		return nil, twirp.InvalidArgumentError("operation", "is invalid")
	} else if req.Operation == pachinko.Operation_CREATE_HOLD ||
		req.Operation == pachinko.Operation_RELEASE_HOLD ||
		req.Operation == pachinko.Operation_FINALIZE_HOLD {

		return nil, twirp.NewError(twirp.Unimplemented, "Holds are no longer supported in Pachinko.")
	}

	if v.ShouldValidateTokens(req.Operation) {
		err := v.Tokens.IsListValid(req.Tokens...)
		if err != nil {
			return nil, err
		}
	}

	if !strings.Blank(req.TransactionId) {
		existingTransaction, err := v.CommittedTransactions.GetTransactionRecord(ctx, req.TransactionId)
		if err != nil {
			return nil, twirp.InternalErrorWith(err)
		}

		if existingTransaction != nil && existingTransaction.IsCommitted {
			return &pachinko.ValidationError{
				Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
			}, nil
		}
	}

	return nil, nil
}

func (v *validator) ShouldValidateTokens(op pachinko.Operation) bool {
	switch op {
	case pachinko.Operation_FINALIZE_HOLD:
		return false
	case pachinko.Operation_RELEASE_HOLD:
		return false
	default:
		return true
	}
}
