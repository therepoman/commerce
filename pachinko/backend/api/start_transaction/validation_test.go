package start_transaction

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	validation_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/validation"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a start transaction validator", t, func() {
		tokensValidator := new(validation_mock.Tokens)
		transactionDao := new(transactions_mock.DAO)

		ctx := context.Background()
		validator := &validator{
			Tokens:                tokensValidator,
			CommittedTransactions: transactionDao,
		}

		Convey("if the request is nil, we error", func() {
			_, err := validator.IsValid(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("if the request is not nil", func() {
			req := &pachinko.StartTransactionReq{}

			Convey("if the operation isn't valid", func() {
				req.Operation = pachinko.Operation(-1)

				_, err := validator.IsValid(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("if the operation is valid", func() {
				req.Operation = pachinko.Operation_CONSUME

				Convey("if the token list is not valid", func() {
					tokensValidator.On("IsListValid", mock.Anything).Return(errors.New("WALRUS STRIKE"))

					_, err := validator.IsValid(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("if the token list is valid", func() {
					tokensValidator.On("IsListValid", mock.Anything).Return(nil)

					req.Tokens = []*pachinko.Token{
						{
							OwnerId:  random.String(16),
							GroupId:  random.String(16),
							Domain:   pachinko.Domain_STICKER,
							Quantity: random.Int64(1, 16),
						},
					}

					Convey("if the request provided a transaction ID", func() {
						req.TransactionId = random.String(32)

						Convey("if we fail to look up the transaction record", func() {
							transactionDao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, errors.New("WALRUS STRIKE"))

							_, err := validator.IsValid(ctx, req)
							So(err, ShouldNotBeNil)
						})

						Convey("if we succeed our dao call and find a transaction record that is committed", func() {
							transactionDao.On("GetTransactionRecord", ctx, req.TransactionId).Return(&dynamo.Transaction{ID: req.TransactionId, IsCommitted: true}, nil)

							validationErr, err := validator.IsValid(ctx, req)
							So(validationErr, ShouldNotBeNil)
							So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
							So(err, ShouldBeNil)
						})

						Convey("if we succeed our dao call and find a transaction record", func() {
							transactionDao.On("GetTransactionRecord", ctx, req.TransactionId).Return(&dynamo.Transaction{ID: req.TransactionId, IsCommitted: false}, nil)

							validationErr, err := validator.IsValid(ctx, req)
							So(validationErr, ShouldBeNil)
							So(err, ShouldBeNil)
						})

						Convey("if we succeed our dao call and do not find a transaction record", func() {
							transactionDao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, nil)

							validationErr, err := validator.IsValid(ctx, req)
							So(validationErr, ShouldBeNil)
							So(err, ShouldBeNil)
						})
					})

					Convey("if the request did not provide a transaction ID", func() {
						validationErr, err := validator.IsValid(ctx, req)
						So(validationErr, ShouldBeNil)
						So(err, ShouldBeNil)
					})
				})
			})

			Convey("if the operation is a create hold operation", func() {
				_, err := validator.IsValid(ctx, &pachinko.StartTransactionReq{
					Operation: pachinko.Operation_CREATE_HOLD,
				})
				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
			})

			Convey("if the operation is a release hold operation", func() {
				_, err := validator.IsValid(ctx, &pachinko.StartTransactionReq{
					Operation: pachinko.Operation_RELEASE_HOLD,
				})
				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
			})

			Convey("if the operation is a finalize hold operation", func() {
				_, err := validator.IsValid(ctx, &pachinko.StartTransactionReq{
					Operation: pachinko.Operation_FINALIZE_HOLD,
				})
				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
			})
		})
	})
}
