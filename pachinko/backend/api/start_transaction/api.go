package start_transaction

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/backend/transactions"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	uuid "github.com/satori/go.uuid"
	"github.com/twitchtv/twirp"
)

type API interface {
	Start(ctx context.Context, req *pachinko.StartTransactionReq) (*pachinko.StartTransactionResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validator Validator              `inject:""`
	Conductor transactions.Conductor `inject:""`
}

func (a *api) Start(ctx context.Context, req *pachinko.StartTransactionReq) (*pachinko.StartTransactionResp, error) {
	validationErr, err := a.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	if validationErr != nil {
		return &pachinko.StartTransactionResp{
			ValidationError: validationErr,
		}, nil
	}

	transactionID := req.TransactionId
	if strings.Blank(transactionID) {
		transactionID = uuid.NewV4().String()
	}

	validationErr, err = a.Conductor.Start(ctx, &models.TransactionRequest{
		ID:        transactionID,
		Operation: req.Operation,
		OperandID: req.OperandTransactionId,
		Tokens:    req.Tokens,
	})

	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if validationErr != nil {
		return &pachinko.StartTransactionResp{
			ValidationError: validationErr,
		}, nil
	}
	return &pachinko.StartTransactionResp{
		TransactionId: transactionID,
	}, nil
}
