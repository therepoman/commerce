package start_transaction

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	start_transaction_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/start_transaction"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestApi_Start(t *testing.T) {
	Convey("given a start transaction API", t, func() {
		validator := new(start_transaction_api_mock.Validator)
		conductor := new(transactions_mock.Conductor)

		api := &api{
			Validator: validator,
			Conductor: conductor,
		}

		reqToken := &pachinko.Token{
			OwnerId:  random.String(16),
			GroupId:  random.String(16),
			Domain:   pachinko.Domain_STICKER,
			Quantity: random.Int64(1, 16),
		}

		ctx := context.Background()

		Convey("when we have an invalid request error", func() {
			validator.On("IsValid", ctx, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				_, err := api.Start(ctx, nil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we have an invalid request validation error", func() {
			validator.On("IsValid", ctx, mock.Anything).Return(&pachinko.ValidationError{
				Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
			}, nil)

			Convey("then we should return an error", func() {
				resp, err := api.Start(ctx, nil)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ValidationError, ShouldNotBeNil)
				So(resp.ValidationError.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
			})
		})

		Convey("when we have a valid request", func() {
			req := &pachinko.StartTransactionReq{
				Operation: pachinko.Operation_CONSUME,
				Tokens:    []*pachinko.Token{reqToken},
			}

			validator.On("IsValid", ctx, req).Return(nil, nil)

			Convey("when the conductor fails to start a transaction", func() {
				conductor.On("Start", ctx, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					_, err := api.Start(ctx, req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the conductor fails to validate on start of a transaction", func() {
				conductor.On("Start", ctx, mock.Anything, mock.Anything).Return(&pachinko.ValidationError{
					Code:         pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
					InvalidToken: reqToken,
				}, nil)

				Convey("then we should return an error", func() {
					resp, err := api.Start(ctx, req)
					So(resp, ShouldNotBeNil)
					So(resp.ValidationError, ShouldNotBeNil)
					So(resp.ValidationError.Code, ShouldEqual, pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE)
					So(resp.ValidationError.InvalidToken, ShouldResemble, reqToken)
					So(err, ShouldBeNil)
				})
			})

			Convey("when the conductor succeeds in starting a transaction", func() {
				conductor.On("Start", ctx, mock.Anything, mock.Anything).Return(nil, nil)

				Convey("if we pass in a transaction ID, then we get the transaction ID back", func() {
					req.TransactionId = uuid.NewV4().String()
					resp, err := api.Start(ctx, req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.TransactionId, ShouldNotBeBlank)
					So(resp.TransactionId, ShouldEqual, req.TransactionId)
				})

				Convey("then we should return success with a non-blank transaction ID", func() {
					resp, err := api.Start(ctx, req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.TransactionId, ShouldNotBeBlank)
				})
			})
		})
	})
}
