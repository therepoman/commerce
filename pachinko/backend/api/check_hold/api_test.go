package check_hold

import (
	"context"
	"testing"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI(t *testing.T) {
	Convey("given an api", t, func() {
		api := &api{}

		ctx := context.Background()

		Convey("When we provide no owner ID", func() {
			_, err := api.Check(ctx, &pachinko.CheckHoldReq{})

			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
		})
	})
}
