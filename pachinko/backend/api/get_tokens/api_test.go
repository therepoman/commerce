package get_tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_GetTokens(t *testing.T) {
	Convey("given a get tokens API", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		tokenGetter := new(tokens_mock.Getter)
		validator := &validator{
			Config: c,
		}

		api := &api{
			Getter:    tokenGetter,
			Validator: validator,
		}

		ctx := context.Background()

		Convey("when the request is invalid", func() {
			_, err := api.Get(ctx, &pachinko.GetTokensReq{OwnerId: "1231231123", Domain: pachinko.Domain(-1)})
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid request", func() {
			req := &pachinko.GetTokensReq{OwnerId: "123123123", Domain: pachinko.Domain_STICKER}

			Convey("when the token getter fails", func() {
				tokenGetter.On("GetAll", ctx, req.OwnerId).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := api.Get(ctx, req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the token getter succeeds", func() {
				res := []*pachinko.Token{
					{
						OwnerId:  req.OwnerId,
						Quantity: 3,
						Domain:   pachinko.Domain_STICKER,
						GroupId:  "321321321",
					},
					{
						OwnerId:  req.OwnerId,
						Quantity: 3,
						Domain:   pachinko.Domain_STICKER,
						GroupId:  "321321321123",
					},
				}

				tokenGetter.On("GetAll", ctx, req.OwnerId).Return(res, nil)

				Convey("then we return the list of all types of tokens", func() {
					resp, err := api.Get(ctx, req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.Tokens, ShouldHaveLength, 2)
				})
			})
		})
	})
}
