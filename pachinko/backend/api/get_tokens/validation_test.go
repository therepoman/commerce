package get_tokens

import (
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a get tokens request validator", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		v := &validator{
			Config: c,
		}

		Convey("when the request is nil we return an error", func() {
			err := v.IsValid(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has no owner ID we return an error", func() {
			err := v.IsValid(&pachinko.GetTokensReq{})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has an invalid domain then we return an error", func() {
			err := v.IsValid(&pachinko.GetTokensReq{OwnerId: "1231231123", Domain: pachinko.Domain(-1)})
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid request", func() {
			req := &pachinko.GetTokensReq{OwnerId: "123123123", Domain: pachinko.Domain_STICKER}
			err := v.IsValid(req)
			So(err, ShouldBeNil)
		})
	})
}
