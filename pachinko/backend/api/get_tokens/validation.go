package get_tokens

import (
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(req *pachinko.GetTokensReq) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	Config *config.Config `inject:""`
}

func (v *validator) IsValid(req *pachinko.GetTokensReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("OwnerId", "cannot send blank request body")
	}
	if strings.Blank(req.OwnerId) {
		return twirp.InvalidArgumentError("OwnerId", "owner ID cannot be blank")
	}
	if req.Domain != v.Config.TenantDomain {
		return twirp.InvalidArgumentError("domain", fmt.Sprintf("is invalid for tenant %s", v.Config.TenantDomain))
	}

	return nil
}
