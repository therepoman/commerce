package get_tokens

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachinko.GetTokensReq) (*pachinko.GetTokensResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Getter    tokens.Getter `inject:""`
	Validator Validator     `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachinko.GetTokensReq) (*pachinko.GetTokensResp, error) {
	err := a.Validator.IsValid(req)
	if err != nil {
		return nil, err
	}

	tokenBalances, err := a.Getter.GetAll(ctx, req.OwnerId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &pachinko.GetTokensResp{
		Tokens: tokenBalances,
	}, nil
}
