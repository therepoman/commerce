package get_token

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_Get(t *testing.T) {
	Convey("given a get token API", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		validator := &validator{
			Config: c,
		}
		getter := new(tokens_mock.Getter)

		api := &api{
			Validator: validator,
			Getter:    getter,
		}

		ctx := context.Background()

		Convey("when the request is invalid, we should error", func() {
			_, err := api.Get(ctx, &pachinko.GetTokenReq{})

			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &pachinko.GetTokenReq{
				Domain:  pachinko.Domain_STICKER,
				OwnerId: "123123123123",
				GroupId: "321321321321",
			}

			Convey("when we fail to fetch the token balance", func() {
				getter.On("Get", ctx, req.OwnerId, req.GroupId).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := api.Get(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("when we succeed to fetch the token balance", func() {
				token := &pachinko.Token{
					Domain:   req.Domain,
					OwnerId:  req.OwnerId,
					GroupId:  req.GroupId,
					Quantity: int64(322),
				}
				getter.On("Get", ctx, req.OwnerId, req.GroupId).Return(token, nil)

				resp, err := api.Get(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Token, ShouldResemble, token)
			})
		})
	})
}
