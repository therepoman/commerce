package get_token

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachinko.GetTokenReq) (*pachinko.GetTokenResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validator Validator     `inject:""`
	Getter    tokens.Getter `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachinko.GetTokenReq) (*pachinko.GetTokenResp, error) {
	err := a.Validator.IsValid(req)
	if err != nil {
		return nil, err
	}

	balance, err := a.Getter.Get(ctx, req.OwnerId, req.GroupId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &pachinko.GetTokenResp{
		Token: balance,
	}, nil
}
