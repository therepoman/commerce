package validation

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	validation_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/validation"
	tx_dao_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given an add tokens validator", t, func() {
		dao := new(tx_dao_mock.DAO)
		tokensValidator := new(validation_mock.Tokens)

		ctx := context.Background()
		v := &validator{
			TransactionsDAO: dao,
			Tokens:          tokensValidator,
		}

		transactionID := random.String(32)
		token := &pachinko.Token{
			OwnerId:  random.String(16),
			GroupId:  random.String(16),
			Domain:   pachinko.Domain_STICKER,
			Quantity: random.Int64(1, 16),
		}

		Convey("returns nil when", func() {
			Convey("when the transaction fetch returns a record that is not committed", func() {
				tokensValidator.On("IsValid", token).Return(nil)
				dao.On("GetTransactionRecord", ctx, transactionID).Return(&dynamo.Transaction{ID: transactionID, IsCommitted: false}, nil)
			})

			Convey("when the transaction fetch returns nothing", func() {
				tokensValidator.On("IsValid", token).Return(nil)
				dao.On("GetTransactionRecord", ctx, transactionID).Return(nil, nil)
			})

			validationErr, err := v.IsValid(ctx, transactionID, token)
			So(err, ShouldBeNil)
			So(validationErr, ShouldBeNil)
		})

		Convey("returns validation error when", func() {
			var errorCode pachinko.ValidationErrorCode

			Convey("when the transaction fetch returns a record that is committed", func() {
				errorCode = pachinko.ValidationErrorCode_TRANSACTION_COMMITTED
				tokensValidator.On("IsValid", token).Return(nil)
				dao.On("GetTransactionRecord", ctx, transactionID).Return(&dynamo.Transaction{ID: transactionID, IsCommitted: true}, nil)
			})

			validationErr, err := v.IsValid(ctx, transactionID, token)
			So(err, ShouldBeNil)
			So(validationErr, ShouldNotBeNil)
			So(validationErr.Code, ShouldEqual, errorCode)
		})

		Convey("returns error when", func() {
			errString := ""
			Convey("request has no transaction ID", func() {
				transactionID = ""
				errString = "twirp error invalid_argument: transaction_id is missing"
			})

			Convey("when the token is invalid", func() {
				errString = "WALRUS STRIKE"
				tokensValidator.On("IsValid", token).Return(errors.New(errString))
			})

			Convey("when the transaction fetch errors", func() {
				errString = "twirp error internal: WALRUS STRIKE"
				tokensValidator.On("IsValid", token).Return(nil)
				dao.On("GetTransactionRecord", ctx, transactionID).Return(nil, errors.New("WALRUS STRIKE"))
			})

			validationErr, err := v.IsValid(ctx, transactionID, token)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, errString)
			So(validationErr, ShouldBeNil)
		})
	})
}
