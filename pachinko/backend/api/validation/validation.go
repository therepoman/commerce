package validation

import (
	"context"

	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type AddTokensValidator interface {
	IsValid(ctx context.Context, transactionID string, token *pachinko.Token) (*pachinko.ValidationError, error)
}

func NewValidator() AddTokensValidator {
	return &validator{}
}

type validator struct {
	TransactionsDAO dynamo_tx.DAO `inject:""`
	Tokens          Tokens        `inject:""`
}

func (v *validator) IsValid(ctx context.Context, transactionID string, token *pachinko.Token) (*pachinko.ValidationError, error) {
	if transactionID == "" {
		return nil, twirp.InvalidArgumentError("transaction_id", "is missing")
	}

	err := v.Tokens.IsValid(token)
	if err != nil {
		return nil, err
	}

	existingTransaction, err := v.TransactionsDAO.GetTransactionRecord(ctx, transactionID)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if existingTransaction != nil && existingTransaction.IsCommitted {
		return &pachinko.ValidationError{
			Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
		}, nil
	}

	return nil, nil
}
