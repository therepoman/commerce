package validation

import (
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Tokens interface {
	IsListValid(tokens ...*pachinko.Token) error
	IsValid(token *pachinko.Token) error
}

func NewTokens() Tokens {
	return &tokens{}
}

type tokens struct {
	Config *config.Config `inject:""`
}

func (t *tokens) IsListValid(tokens ...*pachinko.Token) error {
	if len(tokens) < 1 {
		return twirp.InvalidArgumentError("tokens", "were not provided")
	}

	if len(tokens) > 10 {
		return twirp.InvalidArgumentError("tokens", "of quantity greater than 10 are not supported yet")
	}

	var ownerID string
	groupIDs := map[string]interface{}{}

	for _, token := range tokens {
		err := t.IsValid(token)
		if err != nil {
			return err
		}

		if ownerID == "" {
			ownerID = token.OwnerId
		} else if ownerID != token.OwnerId {
			return twirp.InvalidArgumentError("tokens", "cannot provide more than one owner_id")
		}

		_, ok := groupIDs[token.GroupId]
		if !ok {
			groupIDs[token.GroupId] = nil
		} else {
			return twirp.InvalidArgumentError("tokens", "cannot provide duplicate group IDs in tokens list")
		}
	}

	return nil
}

func (t *tokens) IsValid(token *pachinko.Token) error {
	if token == nil {
		return twirp.InvalidArgumentError("token", "is missing")
	}

	if strings.Blank(token.GroupId) {
		return twirp.InvalidArgumentError("group_id", "is required")
	}

	if strings.Blank(token.OwnerId) {
		return twirp.InvalidArgumentError("owner_id", "is required")
	}

	if token.Quantity < 1 {
		return twirp.InvalidArgumentError("quantity", "must be 1 or greater")
	}

	if token.Domain != t.Config.TenantDomain {
		return twirp.InvalidArgumentError("domain", fmt.Sprintf("is invalid for tenant %s", t.Config.TenantDomain))
	}

	return nil
}
