package validation

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	test_utils "code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTokens_IsListValid(t *testing.T) {
	Convey("given a token validator", t, func() {
		v := &tokens{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
		}

		ownerID := random.String(16)

		Convey("when the token list has no items", func() {
			err := v.IsListValid(test_utils.MakeTokenList(ownerID, pachinko.Domain_STICKER, 0)...)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: tokens were not provided")
		})

		Convey("when the token list has more than 10 items", func() {
			err := v.IsListValid(test_utils.MakeTokenList(ownerID, pachinko.Domain_STICKER, 15)...)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: tokens of quantity greater than 10 are not supported yet")
		})

		Convey("when the token list has a correct amount of tokens", func() {

			tokens := test_utils.MakeTokenList(ownerID, pachinko.Domain_STICKER, 10)

			Convey("when a token in the list is not a valid token", func() {
				tokens[2].OwnerId = ""

				err := v.IsListValid(tokens...)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: owner_id is required")
			})

			Convey("when the tokens in the list do not all have the same owner ID", func() {
				tokens[2].OwnerId = "WALRUS STRIKE"

				err := v.IsListValid(tokens...)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: tokens cannot provide more than one owner_id")
			})

			Convey("when the tokens in the list do not have unique group IDs", func() {
				tokens[2].GroupId = tokens[1].GroupId

				err := v.IsListValid(tokens...)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: tokens cannot provide duplicate group IDs in tokens list")
			})

			Convey("when the tokens are properly formatted we should return nil", func() {
				err := v.IsListValid(tokens...)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestTokens_IsValid(t *testing.T) {
	Convey("given a token validator", t, func() {
		v := &tokens{
			Config: &config.Config{
				TenantDomain: pachinko.Domain_STICKER,
			},
		}

		Convey("when the token is nil, we return an error", func() {
			err := v.IsValid(nil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: token is missing")
		})

		Convey("when the token is not nil", func() {
			token := &pachinko.Token{}

			Convey("token doesn't have a group id", func() {
				err := v.IsValid(token)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: group_id is required")
			})

			Convey("token does have group id", func() {
				token.GroupId = "46024993"

				Convey("token doesn't have owner id", func() {
					err := v.IsValid(token)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "twirp error invalid_argument: owner_id is required")
				})

				Convey("token does have owner id", func() {
					token.OwnerId = "116076154"

					Convey("token doesn't have quantity", func() {
						err := v.IsValid(token)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "twirp error invalid_argument: quantity must be 1 or greater")
					})

					Convey("token does have quantity", func() {
						token.Quantity = 1

						Convey("token doesn't have valid domain", func() {
							token.Domain = pachinko.Domain(-1)

							err := v.IsValid(token)
							So(err, ShouldNotBeNil)
							So(err.Error(), ShouldEqual, "twirp error invalid_argument: domain is invalid for tenant STICKER")
						})

						Convey("token does have valid domain", func() {
							token.Domain = pachinko.Domain_STICKER

							Convey("then we return the token is valid", func() {
								err := v.IsValid(token)
								So(err, ShouldBeNil)
							})
						})
					})
				})
			})
		})
	})
}
