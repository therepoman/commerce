package get_tokens_batch

import (
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a get tokens request validator", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		v := &validator{
			Config: c,
		}

		Convey("when the request is nil we return an error", func() {
			err := v.IsValid(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has no batch we return an error", func() {
			err := v.IsValid(&pachinko.GetTokensBatchReq{})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has an invalid domain then we return an error", func() {
			err := v.IsValid(&pachinko.GetTokensBatchReq{GetTokensBatches: []*pachinko.GetTokensBatch{{OwnerId: "foo"}}, Domain: pachinko.Domain(-1)})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request contains an invalid batch", func() {
			err := v.IsValid(&pachinko.GetTokensBatchReq{GetTokensBatches: []*pachinko.GetTokensBatch{{OwnerId: "", GroupId: "bar"}}, Domain: pachinko.Domain_STICKER})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request contains a duplicate batch", func() {
			err := v.IsValid(&pachinko.GetTokensBatchReq{GetTokensBatches: []*pachinko.GetTokensBatch{{OwnerId: "foo", GroupId: "bar"}, {OwnerId: "foo", GroupId: "bar"}}, Domain: pachinko.Domain_STICKER})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request contains more than 5 batches", func() {
			err := v.IsValid(&pachinko.GetTokensBatchReq{
				GetTokensBatches: []*pachinko.GetTokensBatch{
					{OwnerId: "foo1", GroupId: "bar"},
					{OwnerId: "foo2", GroupId: "bar"},
					{OwnerId: "foo3", GroupId: "bar"},
					{OwnerId: "foo4", GroupId: "bar"},
					{OwnerId: "foo5", GroupId: "bar"},
					{OwnerId: "foo6", GroupId: "bar"},
				},
				Domain: pachinko.Domain_STICKER,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid request", func() {
			req := &pachinko.GetTokensBatchReq{GetTokensBatches: []*pachinko.GetTokensBatch{{OwnerId: "foo", GroupId: "bar"}}, Domain: pachinko.Domain_STICKER}
			err := v.IsValid(req)
			So(err, ShouldBeNil)
		})
	})
}
