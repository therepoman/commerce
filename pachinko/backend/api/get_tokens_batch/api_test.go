package get_tokens_batch

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_GetTokensBatch(t *testing.T) {
	Convey("given a get tokens API", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		tokenGetter := new(tokens_mock.Getter)
		validator := &validator{
			Config: c,
		}

		api := &api{
			Getter:    tokenGetter,
			Validator: validator,
		}

		ctx := context.Background()

		Convey("when the request is invalid", func() {
			_, err := api.Get(ctx, &pachinko.GetTokensBatchReq{
				GetTokensBatches: []*pachinko.GetTokensBatch{{OwnerId: "foo", GroupId: "bar"}},
				Domain:           pachinko.Domain(-1),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid request", func() {
			req := &pachinko.GetTokensBatchReq{
				GetTokensBatches: []*pachinko.GetTokensBatch{
					{OwnerId: "foo", GroupId: "bar"},
					{OwnerId: "baz", GroupId: "bar"},
				},
				Domain: pachinko.Domain_STICKER,
			}

			keys := []dynamo.Keyed{
				dynamo.Keys{req.GetTokensBatches[0].OwnerId, req.GetTokensBatches[0].GroupId},
				dynamo.Keys{req.GetTokensBatches[1].OwnerId, req.GetTokensBatches[1].GroupId},
			}

			Convey("when the token getter fails", func() {
				tokenGetter.On("GetFilteredByBatch", ctx, keys).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := api.Get(ctx, req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the token getter succeeds", func() {
				res := []*pachinko.Token{
					{
						OwnerId:  req.GetTokensBatches[0].OwnerId,
						Quantity: 3,
						Domain:   pachinko.Domain_STICKER,
						GroupId:  req.GetTokensBatches[0].GroupId,
					},
					{
						OwnerId:  req.GetTokensBatches[1].OwnerId,
						Quantity: 3,
						Domain:   pachinko.Domain_STICKER,
						GroupId:  req.GetTokensBatches[1].GroupId,
					},
				}

				tokenGetter.On("GetFilteredByBatch", ctx, keys).Return(res, nil)

				Convey("then we return the list of all types of tokens", func() {
					resp, err := api.Get(ctx, req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.Tokens, ShouldHaveLength, 2)
				})
			})
		})
	})
}
