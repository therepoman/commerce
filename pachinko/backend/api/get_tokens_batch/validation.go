package get_tokens_batch

import (
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(req *pachinko.GetTokensBatchReq) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	Config *config.Config `inject:""`
}

func (v *validator) IsValid(req *pachinko.GetTokensBatchReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("GetTokensBatches", "cannot send blank request body")
	}
	if len(req.GetTokensBatches) < 1 {
		return twirp.InvalidArgumentError("GetTokensBatches", "at least 1 batch must be provided")
	}
	if len(req.GetTokensBatches) > 5 {
		return twirp.InvalidArgumentError("GetTokensBatches", "a maximum of 5 batches may be provided")
	}

	seenBatches := make(map[string]bool)
	for _, getTokensBatch := range req.GetTokensBatches {
		if strings.Blank(getTokensBatch.OwnerId) {
			return twirp.InvalidArgumentError("GetTokensBatches", "in each batch, OwnerID must be provided")
		} else if strings.Blank(getTokensBatch.GroupId) {
			return twirp.InvalidArgumentError("GetTokensBatches", "in each batch, GroupID must be provided")
		}

		batchKey := fmt.Sprintf("%s-%s", getTokensBatch.OwnerId, getTokensBatch.GroupId)
		if seenBatches[batchKey] {
			return twirp.InvalidArgumentError("GetTokensBatches", "request batches must be unique")
		} else {
			seenBatches[batchKey] = true
		}
	}

	if req.Domain != v.Config.TenantDomain {
		return twirp.InvalidArgumentError("domain", fmt.Sprintf("is invalid for tenant %s", v.Config.TenantDomain))
	}

	return nil
}
