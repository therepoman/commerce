package get_tokens_batch

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/guregu/dynamo"
)

type API interface {
	Get(ctx context.Context, req *pachinko.GetTokensBatchReq) (*pachinko.GetTokensBatchResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Getter    tokens.Getter `inject:""`
	Validator Validator     `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachinko.GetTokensBatchReq) (*pachinko.GetTokensBatchResp, error) {
	err := a.Validator.IsValid(req)
	if err != nil {
		return nil, err
	}

	batches := make([]dynamo.Keyed, len(req.GetTokensBatches))
	for i, batch := range req.GetTokensBatches {
		batches[i] = dynamo.Keys{batch.OwnerId, batch.GroupId}
	}

	tokenBalances, err := a.Getter.GetFilteredByBatch(ctx, batches)
	if err != nil {
		return nil, err
	}

	return &pachinko.GetTokensBatchResp{
		Tokens: tokenBalances,
	}, nil
}
