package delete_balance_by_owner_id

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *pachinko.DeleteBalanceByOwnerIdReq) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
}

func (v *validator) IsValid(ctx context.Context, req *pachinko.DeleteBalanceByOwnerIdReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "body is missing")
	}

	if strings.Blank(req.OwnerId) {
		return twirp.InvalidArgumentError("owner_id", "is missing")
	}

	return nil
}
