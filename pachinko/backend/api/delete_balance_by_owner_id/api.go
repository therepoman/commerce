package delete_balance_by_owner_id

import (
	"context"

	sqsClient "code.justin.tv/commerce/pachinko/backend/clients/sqs"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Delete(ctx context.Context, req *pachinko.DeleteBalanceByOwnerIdReq) (*pachinko.DeleteBalanceByOwnerIdResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validator Validator        `inject:""`
	SQSClient sqsClient.Client `inject:""`
	Config    *config.Config   `inject:""`
}

func (a *api) Delete(ctx context.Context, req *pachinko.DeleteBalanceByOwnerIdReq) (*pachinko.DeleteBalanceByOwnerIdResp, error) {
	err := a.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	tenantConf := a.Config.Tenants.Get(a.Config.TenantDomain)
	if tenantConf == nil {
		return nil, twirp.InternalError("Failed to get tenant config")
	}

	var queueURL string
	if tenantConf.DeleteBalanceByOwnerIdSQSConfig != nil {
		queueURL = tenantConf.DeleteBalanceByOwnerIdSQSConfig.URL
	}

	if queueURL == "" {
		return nil, twirp.InternalError("Failed to get tenant config")
	}

	err = a.SQSClient.SendMessage(ctx, queueURL, req.OwnerId)
	if err != nil {
		return nil, twirp.InternalError("Failed to save request to delete balance by owner ID")
	}

	return &pachinko.DeleteBalanceByOwnerIdResp{}, nil
}
