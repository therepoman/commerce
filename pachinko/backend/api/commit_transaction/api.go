package commit_transaction

import (
	"context"

	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/backend/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Commit(ctx context.Context, req *pachinko.CommitTransactionReq) (*pachinko.CommitTransactionResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validator             Validator              `inject:""`
	Conductor             transactions.Conductor `inject:""`
	CommittedTransactions dynamo_tx.DAO          `inject:""`
	TokenGetter           tokens.Getter          `inject:""`
}

func (s *api) Commit(ctx context.Context, req *pachinko.CommitTransactionReq) (*pachinko.CommitTransactionResp, error) {
	validationErr, err := s.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	if validationErr != nil {
		latestTokens, err := s.getLatestTokens(ctx, req)
		if err != nil {
			return nil, twirp.InternalError("something went wrong")
		}

		return &pachinko.CommitTransactionResp{
			Tokens:          latestTokens,
			ValidationError: validationErr,
		}, nil
	}

	updatedTokens, err := s.Conductor.Finish(ctx, req.TransactionId)
	if err != nil {
		return nil, err
	}

	return &pachinko.CommitTransactionResp{
		Tokens: updatedTokens,
	}, nil
}

func (s *api) getLatestTokens(ctx context.Context, req *pachinko.CommitTransactionReq) ([]*pachinko.Token, error) {
	transactionRecord, err := s.CommittedTransactions.GetTransactionRecord(ctx, req.TransactionId)
	if err != nil {
		return nil, err
	}

	latestTokens := make([]*pachinko.Token, 0)
	for _, oldToken := range transactionRecord.Tokens {
		newToken, err := s.TokenGetter.Get(ctx, oldToken.OwnerId, oldToken.GroupId)
		if err != nil {
			return nil, err
		}

		latestTokens = append(latestTokens, newToken)
	}

	return latestTokens, nil
}
