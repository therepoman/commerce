package commit_transaction

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/api/validation"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	"code.justin.tv/commerce/pachinko/backend/transactions/models"
	transaction_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/transaction"
	tx_dao_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given an add tokens validator", t, func() {
		dao := new(tx_dao_mock.DAO)
		tokensValidator := validation.NewTokens()
		cache := new(transaction_mock.Cache)

		ctx := context.Background()
		v := &validator{
			CommittedTransactions: dao,
			Tokens:                tokensValidator,
			TransactionsCache:     cache,
		}

		Convey("request has no body", func() {
			validationErr, err := v.IsValid(ctx, nil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: request body is missing")
			So(validationErr, ShouldBeNil)
		})

		Convey("request has body", func() {
			req := &pachinko.CommitTransactionReq{}

			Convey("request has no transaction ID", func() {
				validationErr, err := v.IsValid(ctx, req)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: transaction_id is missing")
				So(validationErr, ShouldBeNil)
			})

			Convey("request has transaction ID", func() {
				req.TransactionId = random.String(32)

				Convey("when the transaction fetch errors", func() {
					dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("then we should return an error", func() {
						validationErr, err := v.IsValid(ctx, req)
						So(err, ShouldNotBeNil)
						So(validationErr, ShouldBeNil)
					})
				})

				Convey("when the transaction fetch returns nothing", func() {
					dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, nil)

					Convey("then we should return an error", func() {
						validationErr, err := v.IsValid(ctx, req)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "twirp error invalid_argument: transaction_id has not been created")
						So(validationErr, ShouldBeNil)
					})
				})

				Convey("when the transaction fetch returns a record", func() {
					transaction := &dynamo.Transaction{
						ID: req.TransactionId,
					}

					Convey("when the transaction is marked as committed", func() {
						transaction.IsCommitted = true
						dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(transaction, nil)

						Convey("then we should return an error", func() {
							validationErr, err := v.IsValid(ctx, req)
							So(err, ShouldBeNil)
							if validationErr == nil {
								t.Fail()
							} else {
								So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
							}
						})
					})

					Convey("when the transaction is not marked as committed", func() {
						transaction.IsCommitted = false
						token := &pachinko.Token{
							Domain:   pachinko.Domain_STICKER,
							OwnerId:  random.String(16),
							GroupId:  random.String(16),
							Quantity: int64(322),
						}
						transaction.Tokens = dynamo.ToTHTokens([]*pachinko.Token{
							token,
						})

						dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(transaction, nil)

						Convey("if the transaction cache lookup returns an error", func() {
							cache.On("Get", ctx, *token, req.TransactionId).Return(nil, errors.New("WALRUS STRIKE"))

							Convey("then we should return an error", func() {
								validationErr, err := v.IsValid(ctx, req)
								So(err, ShouldNotBeNil)
								So(validationErr, ShouldBeNil)
							})
						})

						Convey("if the transaction cache lookup does not return anything", func() {
							cache.On("Get", ctx, *token, req.TransactionId).Return(nil, nil)

							Convey("then we should return an error", func() {
								validationErr, err := v.IsValid(ctx, req)
								So(err, ShouldBeNil)
								So(validationErr, ShouldBeNil)
							})
						})

						Convey("if the transaction cache lookup returns a record", func() {
							cache.On("Get", ctx, *token, req.TransactionId).Return(&models.Transaction{
								ID:        req.TransactionId,
								Amount:    -10,
								Operation: pachinko.Operation_CONSUME,
							}, nil)

							Convey("then we should return nil", func() {
								validationErr, err := v.IsValid(ctx, req)
								So(err, ShouldBeNil)
								So(validationErr, ShouldBeNil)
							})
						})
					})
				})
			})
		})
	})
}
