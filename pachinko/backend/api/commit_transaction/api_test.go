package commit_transaction

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	commit_transaction_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/commit_transaction"
	tx_dao_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	transactions_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_Commit(t *testing.T) {
	Convey("given a commit transaction api", t, func() {
		conductor := new(transactions_mock.Conductor)
		validator := new(commit_transaction_api_mock.Validator)
		transactionDao := new(tx_dao_mock.DAO)
		tokenGetter := new(tokens_mock.Getter)

		a := &api{
			Validator:             validator,
			Conductor:             conductor,
			CommittedTransactions: transactionDao,
			TokenGetter:           tokenGetter,
		}

		ctx := context.Background()
		transactionID := random.String(32)
		req := &pachinko.CommitTransactionReq{
			TransactionId: transactionID,
		}

		Convey("when the validation fails", func() {
			Convey("because there is real bad problem", func() {
				validator.On("IsValid", ctx, req).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := a.Commit(ctx, req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("because the transaction is already committed", func() {
				validator.On("IsValid", ctx, req).Return(&pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
				}, nil)

				Convey("when transaction DAO fails for unknown reasons", func() {
					transactionDao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, errors.New("ohki strike"))

					Convey("then we return an error", func() {
						_, err := a.Commit(ctx, req)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when transaction DAO succeeds", func() {
					transactionDao.On("GetTransactionRecord", ctx, req.TransactionId).Return(&dynamo.Transaction{
						ID:        req.TransactionId,
						Operation: dynamo.Operation(pachinko.Operation_CONSUME.String()),
						Tokens: []*dynamo.THToken{
							{
								Domain:   dynamo.TwirpDomain(pachinko.Domain_BITS),
								OwnerId:  "1111",
								GroupId:  "BALANCE",
								Quantity: 5, // the transaction was to spend 5 bits
							},
						},
						IsCommitted: true,
					}, nil)

					Convey("when token getter fails for unknown reasons", func() {
						tokenGetter.On("Get", ctx, "1111", "BALANCE").Return(nil, errors.New("ohki strike"))

						Convey("then we return an error", func() {
							_, err := a.Commit(ctx, req)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("when token getter succeeds for all tokens", func() {
						tokenGetter.On("Get", ctx, "1111", "BALANCE").Return(&pachinko.Token{
							OwnerId:  "1111",
							GroupId:  "BALANCE",
							Domain:   pachinko.Domain_BITS,
							Quantity: 95,
						}, nil)

						Convey("then we should get validation error with the latest balance", func() {
							resp, err := a.Commit(ctx, req)
							So(err, ShouldBeNil)
							if resp.ValidationError == nil {
								t.Fail()
							} else {
								So(resp.ValidationError.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
								So(resp.Tokens[0].Quantity, ShouldEqual, 95)
							}
						})
					})
				})
			})
		})

		Convey("when the validation succeeds", func() {
			validator.On("IsValid", ctx, req).Return(nil, nil)

			Convey("when the conductor fails", func() {
				conductor.On("Finish", ctx, transactionID).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					_, err := a.Commit(ctx, req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the conductor succeeds", func() {
				tokens := []*pachinko.Token{
					{
						Domain:   pachinko.Domain_STICKER,
						OwnerId:  "123123123",
						GroupId:  "321321321",
						Quantity: int64(322),
					},
				}

				conductor.On("Finish", ctx, transactionID).Return(tokens, nil)

				Convey("then we the updated balances", func() {
					resp, err := a.Commit(ctx, req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.Tokens, ShouldResemble, tokens)
				})
			})
		})
	})
}
