package commit_transaction

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/api/validation"
	"code.justin.tv/commerce/pachinko/backend/cache/transaction"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *pachinko.CommitTransactionReq) (*pachinko.ValidationError, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	CommittedTransactions dynamo_tx.DAO     `inject:""`
	TransactionsCache     transaction.Cache `inject:""`
	Tokens                validation.Tokens `inject:""`
}

func (v *validator) IsValid(ctx context.Context, req *pachinko.CommitTransactionReq) (*pachinko.ValidationError, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "body is missing")
	}

	if req.TransactionId == "" {
		return nil, twirp.InvalidArgumentError("transaction_id", "is missing")
	}

	existingTransaction, err := v.CommittedTransactions.GetTransactionRecord(ctx, req.TransactionId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if existingTransaction == nil {
		return nil, twirp.InvalidArgumentError("transaction_id", "has not been created")
	}

	if existingTransaction.IsCommitted {
		return &pachinko.ValidationError{
			Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
		}, nil
	}

	for _, token := range existingTransaction.Tokens {
		inflightRecord, err := v.TransactionsCache.Get(ctx, *token.ToTwirpToken(), req.TransactionId)
		if err != nil {
			return nil, twirp.InternalErrorWith(err)
		}

		if inflightRecord == nil {
			logrus.WithFields(logrus.Fields{
				"transactionId": req.TransactionId,
				"token":         token,
				"tokenCount":    len(existingTransaction.Tokens),
			}).Debug("Reusing an existing transaction that's no longer inflight")
			break
		}
	}

	return nil, nil
}
