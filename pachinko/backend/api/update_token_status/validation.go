package update_token_status

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *pachinko.UpdateTokenStatusReq) (*pachinko.ValidationError, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	Config          *config.Config `inject:""`
	TransactionsDAO dynamo_tx.DAO  `inject:""`
}

func (v *validator) IsValid(ctx context.Context, req *pachinko.UpdateTokenStatusReq) (*pachinko.ValidationError, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "body is missing")
	}

	if req.TransactionId == "" {
		return nil, twirp.InvalidArgumentError("transaction_id", "is missing")
	}

	if req.Token == nil {
		return nil, twirp.InvalidArgumentError("token", "is missing")
	}

	if strings.Blank(req.Token.OwnerId) {
		return nil, twirp.InvalidArgumentError("token.owner_id", "is missing")
	}

	if strings.Blank(req.Token.GroupId) {
		return nil, twirp.InvalidArgumentError("token.group_id", "is missing")
	}

	existingTransaction, err := v.TransactionsDAO.GetTransactionRecord(ctx, req.TransactionId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if existingTransaction != nil && existingTransaction.IsCommitted {
		return &pachinko.ValidationError{
			Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
		}, nil
	}

	return nil, nil
}
