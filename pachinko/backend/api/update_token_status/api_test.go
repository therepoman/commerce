package update_token_status

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	update_token_status_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/update_token_status"
	tokens_cache_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/tokens"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestApi_Update(t *testing.T) {
	Convey("given update token status API", t, func() {
		tenantConf := &tenant.Config{
			BalanceUpdatesTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}
		validator := new(update_token_status_api_mock.Validator)
		getter := new(tokens_mock.Getter)
		cache := new(tokens_cache_mock.Cache)
		statusUpdater := new(tokens_mock.StatusUpdater)

		api := &api{
			Config:        c,
			Validator:     validator,
			Cache:         cache,
			Getter:        getter,
			StatusUpdater: statusUpdater,
		}

		ctx := context.Background()

		transactionId := uuid.NewV4().String()
		token := &pachinko.Token{
			OwnerId:  random.String(16),
			GroupId:  random.String(16),
			Domain:   pachinko.Domain_STICKER,
			Quantity: 0,
			Status:   pachinko.TokenStatus_FROZEN,
		}

		req := &pachinko.UpdateTokenStatusReq{
			Token:         token,
			TransactionId: transactionId,
		}

		newToken := &pachinko.Token{
			Domain:   token.Domain,
			OwnerId:  token.OwnerId,
			GroupId:  token.GroupId,
			Quantity: int64(322),
			Status:   pachinko.TokenStatus_FROZEN,
		}

		getter.On("Get", ctx, token.OwnerId, token.GroupId).Return(newToken, nil)

		Convey("when the request is invalid, we should error", func() {
			validator.On("IsValid", ctx, req).Return(&pachinko.ValidationError{}, nil)
			resp, err := api.Update(ctx, req)

			So(resp, ShouldNotBeNil)
			So(resp.ValidationError, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when the request is valid", func() {
			validator.On("IsValid", ctx, req).Return(nil, nil)

			Convey("when we fail to set the token status", func() {
				statusUpdater.On("UpdateStatus", ctx, transactionId, *token).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := api.Update(ctx, req)
				cache.AssertNotCalled(t, "Del")
				So(err, ShouldNotBeNil)
			})

			Convey("when we succeed to set the token status", func() {
				statusUpdater.On("UpdateStatus", ctx, transactionId, *token).Return(newToken, nil)
				cache.On("Del", mock.Anything, req.Token.OwnerId, req.Token.GroupId).Return()

				resp, err := api.Update(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Token.Status, ShouldEqual, pachinko.TokenStatus_FROZEN)
			})
		})
	})
}
