package update_token_status

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	cache "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	uuid "github.com/satori/go.uuid"
	"github.com/twitchtv/twirp"
)

type API interface {
	Update(ctx context.Context, req *pachinko.UpdateTokenStatusReq) (*pachinko.UpdateTokenStatusResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Config        *config.Config       `inject:""`
	Validator     Validator            `inject:""`
	Cache         cache.Cache          `inject:""`
	Getter        tokens.Getter        `inject:""`
	StatusUpdater tokens.StatusUpdater `inject:""`
}

func (a *api) Update(ctx context.Context, req *pachinko.UpdateTokenStatusReq) (*pachinko.UpdateTokenStatusResp, error) {
	validationErr, err := a.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	currentToken, err := a.Getter.Get(ctx, req.Token.OwnerId, req.Token.GroupId)
	if err != nil {
		return nil, twirp.InternalError("something went wrong")
	}

	if validationErr != nil {
		return &pachinko.UpdateTokenStatusResp{
			Token:           currentToken,
			ValidationError: validationErr,
		}, nil
	}

	transactionID := req.TransactionId
	if strings.Blank(transactionID) {
		transactionID = uuid.NewV4().String()
	}

	updatedToken, err := a.StatusUpdater.UpdateStatus(ctx, transactionID, *req.Token)
	if err != nil {
		return nil, err
	}

	go a.Cache.Del(context.Background(), req.Token.OwnerId, req.Token.GroupId)

	return &pachinko.UpdateTokenStatusResp{
		Token: updatedToken,
	}, nil
}
