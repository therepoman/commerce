package update_token_status

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/dynamo"
	tx_dao_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given an update token status validator", t, func() {
		dao := new(tx_dao_mock.DAO)

		ctx := context.Background()
		v := &validator{
			TransactionsDAO: dao,
		}

		Convey("request has no body", func() {
			validationErr, err := v.IsValid(ctx, nil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: request body is missing")
			So(validationErr, ShouldBeNil)
		})

		Convey("request has body", func() {
			req := &pachinko.UpdateTokenStatusReq{}

			Convey("request has no transaction ID", func() {
				validationErr, err := v.IsValid(ctx, req)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: transaction_id is missing")
				So(validationErr, ShouldBeNil)
			})

			Convey("request has transaction ID", func() {
				req.TransactionId = random.String(32)

				Convey("when the token is invalid", func() {
					validationErr, err := v.IsValid(ctx, req)
					So(err, ShouldNotBeNil)
					So(validationErr, ShouldBeNil)
				})

				Convey("when the token is valid", func() {
					req.Token = &pachinko.Token{
						OwnerId:  random.String(16),
						GroupId:  random.String(16),
						Domain:   pachinko.Domain_STICKER,
						Quantity: random.Int64(1, 16),
					}

					Convey("when the transaction fetch errors", func() {
						dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, errors.New("WALRUS STRIKE"))

						Convey("then we should return an error", func() {
							validationErr, err := v.IsValid(ctx, req)
							So(err, ShouldNotBeNil)
							So(validationErr, ShouldBeNil)
						})
					})

					Convey("when the transaction fetch returns a record that is committed", func() {
						dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(&dynamo.Transaction{ID: req.TransactionId, IsCommitted: true}, nil)

						Convey("then we should return an error", func() {
							validationErr, err := v.IsValid(ctx, req)
							So(err, ShouldBeNil)
							So(validationErr, ShouldNotBeNil)
							So(validationErr.Code, ShouldEqual, pachinko.ValidationErrorCode_TRANSACTION_COMMITTED)
						})
					})

					Convey("when the transaction fetch returns a record that is not committed", func() {
						dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(&dynamo.Transaction{ID: req.TransactionId, IsCommitted: false}, nil)

						Convey("then we should return nil", func() {
							validationErr, err := v.IsValid(ctx, req)
							So(err, ShouldBeNil)
							So(validationErr, ShouldBeNil)
						})
					})

					Convey("when the transaction fetch returns nothing", func() {
						dao.On("GetTransactionRecord", ctx, req.TransactionId).Return(nil, nil)

						Convey("then we should return nil", func() {
							validationErr, err := v.IsValid(ctx, req)
							So(err, ShouldBeNil)
							So(validationErr, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
