package override_balance

import (
	"context"

	cache "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	"code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type API interface {
	Override(ctx context.Context, req *pachinko.OverrideBalanceReq) (*pachinko.OverrideBalanceResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validator Validator      `inject:""`
	DAO       tokens.DAO     `inject:""`
	Cache     cache.Cache    `inject:""`
	Config    *config.Config `inject:""`
}

func (a *api) Override(ctx context.Context, req *pachinko.OverrideBalanceReq) (*pachinko.OverrideBalanceResp, error) {
	err := a.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	token, err := a.DAO.SetTokenBalance(ctx, req.Token.OwnerId, req.Token.GroupId, req.Token.Quantity)
	if err != nil {
		return nil, err
	}

	go a.Cache.Del(context.Background(), req.Token.OwnerId, req.Token.GroupId)

	return &pachinko.OverrideBalanceResp{Token: &pachinko.Token{
		OwnerId:  token.OwnerID,
		GroupId:  token.GroupID,
		Domain:   a.Config.TenantDomain,
		Quantity: int64(token.Balance),
	}}, nil
}
