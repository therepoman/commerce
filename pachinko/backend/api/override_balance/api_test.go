package override_balance

import (
	"context"
	"errors"
	"testing"

	override_balance_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/override_balance"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestOverrideBalanceApi_Override(t *testing.T) {
	ctx := context.Background()
	req := pachinko.OverrideBalanceReq{}
	validator := new(override_balance_api_mock.Validator)

	Convey("given a server ", t, func() {
		api := &api{
			Validator: validator,
		}

		Convey("validator that returns an error", func() {
			validator.On("IsValid", ctx, &req).Return(errors.New("NO ANIME BEARS ALLOWED"))

			_, err := api.Override(ctx, &req)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "NO ANIME BEARS ALLOWED")
		})
	})
}
