package override_balance

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *pachinko.OverrideBalanceReq) error
}

func NewValidator() Validator {
	return &validator{}
}

func newValidator(config *config.Config) Validator {
	return &validator{config}
}

type validator struct {
	Config *config.Config `inject:""`
}

func (v *validator) IsValid(ctx context.Context, req *pachinko.OverrideBalanceReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "body is missing")
	}

	if req.Token == nil {
		return twirp.InvalidArgumentError("token", "is required")
	}

	if strings.Blank(req.GetToken().GroupId) {
		return twirp.InvalidArgumentError("group_id", "is required")
	}

	if strings.Blank(req.GetToken().OwnerId) {
		return twirp.InvalidArgumentError("owner_id", "is required")
	}

	// this check here is why we can't use the regular token validator, it doesn't like zero, I like zero.
	if req.GetToken().Quantity < 0 {
		return twirp.InvalidArgumentError("balance", "must be 0 or greater")
	}

	if req.GetToken().Domain != v.Config.TenantDomain {
		return twirp.InvalidArgumentError("domain", fmt.Sprintf("is invalid for tenant %s", v.Config.TenantDomain))
	}

	tenantConf := v.Config.GetTenant()
	if tenantConf == nil {
		return twirp.InternalError("cannot check tenant configuration")
	}

	if !tenantConf.OverrideAPIAllowed {
		return twirp.NewError(twirp.PermissionDenied, "operation is not allowed")
	}

	return nil
}
