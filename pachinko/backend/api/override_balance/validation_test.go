package override_balance

import (
	"context"
	"strings"
	"testing"

	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	ctx := context.Background()
	cfg := config.Config{
		TenantDomain: pachinko.Domain_STICKER,
		Tenants:      &tenant.Wrapper{},
	}

	Convey("given an override token balance validator", t, func() {
		validator := newValidator(&cfg)

		Convey("with a nil request", func() {
			err := validator.IsValid(ctx, nil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: request body is missing")
		})

		Convey("with a request struct", func() {
			req := pachinko.OverrideBalanceReq{}

			Convey("with a missing token", func() {
				err := validator.IsValid(ctx, nil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: request body is missing")
			})

			Convey("with a token", func() {
				req.Token = &pachinko.Token{}

				Convey("with a missing group id", func() {
					err := validator.IsValid(ctx, &req)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "twirp error invalid_argument: group_id is required")
				})

				Convey("with a group id", func() {
					req.Token.GroupId = "foo"

					Convey("with a missing owner id", func() {
						err := validator.IsValid(ctx, &req)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "twirp error invalid_argument: owner_id is required")

						Convey("with a owner id", func() {
							req.Token.OwnerId = "foo"

							Convey("with a negative balance", func() {
								req.Token.Quantity = -1

								err := validator.IsValid(ctx, &req)
								So(err, ShouldNotBeNil)
								So(err.Error(), ShouldEqual, "twirp error invalid_argument: balance must be 0 or greater")

								Convey("with a zero balance", func() {
									req.Token.Quantity = 0
									req.Token.Domain = pachinko.Domain(99)

									Convey("with default invalid tenant", func() {
										err := validator.IsValid(ctx, &req)
										So(err, ShouldNotBeNil)
										So(err.Error(), ShouldEqual, "twirp error invalid_argument: domain is invalid for tenant STICKER")

										Convey("with valid tenant", func() {
											req.Token.Domain = pachinko.Domain_STICKER

											Convey("with missing config for tenant", func() {
												err := validator.IsValid(ctx, &req)
												So(err, ShouldNotBeNil)
												So(err.Error(), ShouldEqual, "twirp error internal: cannot check tenant configuration")
											})

											Convey("with config for tenant, but operation disallowed", func() {
												cfg.Tenants = &tenant.Wrapper{
													strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
														OverrideAPIAllowed: false,
													},
												}

												err := validator.IsValid(ctx, &req)
												So(err, ShouldNotBeNil)
												So(err.Error(), ShouldEqual, "twirp error permission_denied: operation is not allowed")
											})

											Convey("with config for tenant, but operation allowed", func() {
												cfg.Tenants = &tenant.Wrapper{
													strings.ToLower(pachinko.Domain_STICKER.String()): &tenant.Config{
														OverrideAPIAllowed: true,
													},
												}

												err := validator.IsValid(ctx, &req)
												So(err, ShouldBeNil)
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
