package get_pending_transactions

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/cache/transaction"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachinko.GetPendingTransactionsReq) (*pachinko.GetPendingTransactionsResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	TransactionsCache transaction.Cache `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachinko.GetPendingTransactionsReq) (*pachinko.GetPendingTransactionsResp, error) {
	token := pachinko.Token{
		Domain:  req.Domain,
		OwnerId: req.OwnerId,
		GroupId: req.GroupId,
	}

	transactions, err := a.TransactionsCache.GetAll(ctx, token)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	ret := make([]*pachinko.Transaction, 0)
	for _, tx := range transactions {
		ret = append(ret, &pachinko.Transaction{
			Id:        tx.ID,
			Operation: tx.Operation,
			Amount:    int64(tx.Amount),
		})
	}

	return &pachinko.GetPendingTransactionsResp{
		Transactions: ret,
	}, nil
}
