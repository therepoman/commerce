package async_add_tokens

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/backend/sns"
	async_add_tokens_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/async_add_tokens"
	sns_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/sns"
	tokens_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_Add(t *testing.T) {
	Convey("given an async add tokens API", t, func() {
		publisher := new(sns_mock.Publisher)
		tokenGetter := new(tokens_mock.Getter)
		validator := new(async_add_tokens_api_mock.Validator)

		a := &api{
			Publisher:   publisher,
			Validator:   validator,
			TokenGetter: tokenGetter,
		}

		ctx := context.Background()
		transactionID := random.String(32)
		token := &pachinko.Token{
			Domain:   pachinko.Domain_STICKER,
			OwnerId:  random.String(16),
			GroupId:  random.String(16),
			Quantity: 42,
		}
		req := &pachinko.AsyncAddTokensReq{
			TransactionId: transactionID,
			Token:         token,
		}

		Convey("returns empty response when", func() {
			Convey("request is valid and publishes successfully", func() {
				validator.On("IsValid", ctx, req).Return(nil, nil)
				tokenGetter.On("Get", ctx, token.OwnerId, token.GroupId).Return(token, nil)
				publisher.On("PublishAsyncAddTokensRequest", ctx, sns.AddTokensMessage{
					TransactionID: transactionID,
					Domain:        pachinko.Domain_STICKER,
					OwnerID:       token.OwnerId,
					GroupID:       token.GroupId,
					Amount:        token.Quantity,
				}).Return(nil)
			})

			resp, err := a.Add(ctx, req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.ValidationError, ShouldBeNil)
		})

		Convey("returns validation error when", func() {
			var errCode pachinko.ValidationErrorCode

			Convey("when validator returns error", func() {
				errCode = pachinko.ValidationErrorCode_TRANSACTION_COMMITTED
				validator.On("IsValid", ctx, req).Return(&pachinko.ValidationError{Code: errCode}, nil)
				tokenGetter.On("Get", ctx, token.OwnerId, token.GroupId).Return(token, nil)
			})

			Convey("when balance is frozen", func() {
				errCode = pachinko.ValidationErrorCode_BALANCE_FROZEN
				validator.On("IsValid", ctx, req).Return(nil, nil)
				tokenGetter.On("Get", ctx, token.OwnerId, token.GroupId).Return(&pachinko.Token{
					Domain:   pachinko.Domain_STICKER,
					OwnerId:  random.String(16),
					GroupId:  random.String(16),
					Quantity: 42,
					Status:   pachinko.TokenStatus_FROZEN,
				}, nil)
			})

			resp, err := a.Add(ctx, req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.ValidationError, ShouldNotBeNil)
			So(resp.ValidationError.Code, ShouldEqual, errCode)
		})

		Convey("returns error when", func() {
			Convey("when validator returns error", func() {
				validator.On("IsValid", ctx, req).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when token getter returns error", func() {
				validator.On("IsValid", ctx, req).Return(nil, nil)
				tokenGetter.On("Get", ctx, token.OwnerId, token.GroupId).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when publisher returns error", func() {
				validator.On("IsValid", ctx, req).Return(nil, nil)
				tokenGetter.On("Get", ctx, token.OwnerId, token.GroupId).Return(token, nil)
				publisher.On("PublishAsyncAddTokensRequest", ctx, sns.AddTokensMessage{
					TransactionID: transactionID,
					Domain:        pachinko.Domain_STICKER,
					OwnerID:       token.OwnerId,
					GroupID:       token.GroupId,
					Amount:        token.Quantity,
				}).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := a.Add(ctx, req)

			So(err, ShouldNotBeNil)
		})
	})
}
