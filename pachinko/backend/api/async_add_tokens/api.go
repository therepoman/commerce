package async_add_tokens

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/sns"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Add(ctx context.Context, req *pachinko.AsyncAddTokensReq) (*pachinko.AsyncAddTokensResp, error)
}

type api struct {
	TokenGetter tokens.Getter `inject:""`
	Validator   Validator     `inject:""`
	Publisher   sns.Publisher `inject:""`
}

func NewAPI() *api {
	return &api{}
}

func (s *api) Add(ctx context.Context, req *pachinko.AsyncAddTokensReq) (*pachinko.AsyncAddTokensResp, error) {
	validationErr, err := s.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	currentToken, err := s.TokenGetter.Get(ctx, req.Token.OwnerId, req.Token.GroupId)
	if err != nil {
		return nil, twirp.InternalError("something went wrong")
	}

	if currentToken.Status == pachinko.TokenStatus_FROZEN {
		validationErr = &pachinko.ValidationError{
			Code: pachinko.ValidationErrorCode_BALANCE_FROZEN,
		}
	}

	if validationErr != nil {
		return &pachinko.AsyncAddTokensResp{
			ValidationError: validationErr,
		}, nil
	}

	err = s.Publisher.PublishAsyncAddTokensRequest(ctx, sns.AddTokensMessage{
		TransactionID: req.TransactionId,
		Domain:        req.Token.Domain,
		OwnerID:       req.Token.OwnerId,
		GroupID:       req.Token.GroupId,
		Amount:        req.Token.Quantity,
	})
	if err != nil {
		return nil, twirp.InternalError("failed to publish add tokens request")
	}

	return &pachinko.AsyncAddTokensResp{}, nil
}
