package async_add_tokens

import (
	"context"

	"code.justin.tv/commerce/pachinko/backend/api/validation"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *pachinko.AsyncAddTokensReq) (*pachinko.ValidationError, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	Validation validation.AddTokensValidator `inject:""`
}

func (v *validator) IsValid(ctx context.Context, req *pachinko.AsyncAddTokensReq) (*pachinko.ValidationError, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "body is missing")
	}

	return v.Validation.IsValid(ctx, req.TransactionId, req.Token)
}
