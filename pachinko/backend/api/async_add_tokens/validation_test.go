package async_add_tokens

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	validation_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/validation"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given an add tokens validator", t, func() {
		addTokensValidator := new(validation_mock.AddTokensValidator)

		ctx := context.Background()
		v := &validator{
			Validation: addTokensValidator,
		}

		transactionID := random.String(32)
		token := &pachinko.Token{
			OwnerId:  random.String(16),
			GroupId:  random.String(16),
			Domain:   pachinko.Domain_STICKER,
			Quantity: random.Int64(1, 16),
		}

		req := &pachinko.AsyncAddTokensReq{
			TransactionId: transactionID,
			Token:         token,
		}

		Convey("returns nil when", func() {
			Convey("when the request is valid", func() {
				addTokensValidator.On("IsValid", ctx, transactionID, token).Return(nil, nil)
			})

			validationErr, err := v.IsValid(ctx, req)
			So(err, ShouldBeNil)
			So(validationErr, ShouldBeNil)
		})

		Convey("return validation error when", func() {
			var errorCode pachinko.ValidationErrorCode

			Convey("when the add tokens validator returns validation error", func() {
				errorCode = pachinko.ValidationErrorCode_TRANSACTION_COMMITTED
				addTokensValidator.On("IsValid", ctx, transactionID, token).Return(&pachinko.ValidationError{Code: errorCode}, nil)
			})

			validationErr, err := v.IsValid(ctx, req)
			So(err, ShouldBeNil)
			So(validationErr, ShouldNotBeNil)
			So(validationErr.Code, ShouldEqual, errorCode)
		})

		Convey("returns error when", func() {
			var errorString string

			Convey("when the body is nil", func() {
				errorString = "twirp error invalid_argument: request body is missing"
				req = nil
			})

			Convey("when the add tokens validator returns error", func() {
				errorString = "WALRUS STRIKE"
				addTokensValidator.On("IsValid", ctx, transactionID, token).Return(nil, errors.New(errorString))
			})

			validationErr, err := v.IsValid(ctx, req)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, errorString)
			So(validationErr, ShouldBeNil)
		})
	})
}
