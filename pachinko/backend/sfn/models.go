package sfn

type DeleteBalanceByOwnerIdStateMachineInput struct {
	OwnerIds []string `json:"owner_ids"`
}

type DeleteBalanceByGroupIdStateMachineInput struct {
	GroupIds []string `json:"group_ids"`
}
