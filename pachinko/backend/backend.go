package backend

import (
	"context"
	"io"
	"net/http"
	"sync"
	"time"

	awssns "code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/api/add_tokens"
	"code.justin.tv/commerce/pachinko/backend/api/async_add_tokens"
	"code.justin.tv/commerce/pachinko/backend/api/check_hold"
	"code.justin.tv/commerce/pachinko/backend/api/check_holds_for_owner"
	"code.justin.tv/commerce/pachinko/backend/api/commit_transaction"
	"code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id"
	"code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id"
	"code.justin.tv/commerce/pachinko/backend/api/get_pending_transactions"
	"code.justin.tv/commerce/pachinko/backend/api/get_token"
	"code.justin.tv/commerce/pachinko/backend/api/get_tokens"
	"code.justin.tv/commerce/pachinko/backend/api/get_tokens_batch"
	"code.justin.tv/commerce/pachinko/backend/api/get_transaction"
	"code.justin.tv/commerce/pachinko/backend/api/get_transactions"
	"code.justin.tv/commerce/pachinko/backend/api/override_balance"
	"code.justin.tv/commerce/pachinko/backend/api/start_transaction"
	"code.justin.tv/commerce/pachinko/backend/api/update_token_status"
	"code.justin.tv/commerce/pachinko/backend/api/validation"
	"code.justin.tv/commerce/pachinko/backend/cache/balancelock"
	cache_tokens "code.justin.tv/commerce/pachinko/backend/cache/tokens"
	cache_tx "code.justin.tv/commerce/pachinko/backend/cache/transaction"
	"code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp"
	"code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	redis_client "code.justin.tv/commerce/pachinko/backend/circuit/redis-client"
	"code.justin.tv/commerce/pachinko/backend/clients/cloudwatchlogger"
	"code.justin.tv/commerce/pachinko/backend/clients/lock"
	"code.justin.tv/commerce/pachinko/backend/clients/redis"
	"code.justin.tv/commerce/pachinko/backend/clients/sfn"
	"code.justin.tv/commerce/pachinko/backend/clients/sqs"
	dynamo_tokens "code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	dynamo_tokens_scan "code.justin.tv/commerce/pachinko/backend/dynamo/tokens_scan"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	dynamo_tx_scan "code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/backend/server"
	"code.justin.tv/commerce/pachinko/backend/sns"
	"code.justin.tv/commerce/pachinko/backend/tokens"
	"code.justin.tv/commerce/pachinko/backend/transactions"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/commerce/splatter"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/pkg/errors"
)

type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() pachinko.Pachinko
	Statter() statsd.Statter
	Shutdown(ctx context.Context)
}

type backend struct {
	// Twirp Server
	Server pachinko.Pachinko `inject:""`

	// Config
	Config *config.Config `inject:""`

	// Stats
	Stats statsd.Statter `inject:""`

	// Audit Logger
	AuditLogger cloudwatchlogger.CloudWatchLogger `inject:""`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetrics != nil {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.Environment.AWSRegion,
			ServiceName:       "pachinko",
			Stage:             cfg.GetEnvironmentMetricName(),
			Substage:          cfg.TenantDomain.String(),
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	// create another session for us-east-2 to do dynamo scans
	ddbScanSess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegionDynamoScan),
	})
	if err != nil {
		return nil, err
	}

	hystrixMetrics := metrics.NewHystrixMetricsCollector()

	tenantTokensRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-tokens")
	tenantTxRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-txs")
	tenantTxTsRedisClient := redis_client.NewConsistentTenantClient(cfg, "redis-tenant-tx-ts")

	sfnClient, err := sfn.NewDefaultClient(cfg)
	if err != nil {
		return nil, err
	}

	sqsClient, err := sqs.NewDefaultClient(cfg)
	if err != nil {
		return nil, err
	}

	auditLogGroup := cfg.Tenants.Get(cfg.TenantDomain).AuditLogGroupName
	cwloggerClient := cloudwatchlogger.NewCloudWatchLogNoopClient()

	if strings.NotBlank(auditLogGroup) {
		cwloggerClient, err = cloudwatchlogger.NewCloudWatchLogClient(cfg.Environment.AWSRegion, auditLogGroup)
		if err != nil {
			return nil, err
		}
	}

	deps := []*inject.Object{
		// Backend (graph root)
		{Value: b},

		// Config
		{Value: cfg},

		// Stats
		{Value: stats},
		{Value: metrics.NewStatter()},
		{Value: hystrixMetrics},

		// Clients
		{Value: sfnClient},
		{Value: sqsClient},
		{Value: awssns.NewDefaultClient()},
		{Value: lock.NewDynamoDBLockingClient()},
		{Value: cwloggerClient},

		// Redis
		{Value: redis.NewClient(tenantTokensRedisClient), Name: "redis-tokens"},
		{Value: redis.NewClient(tenantTxRedisClient), Name: "redis-txs"},
		{Value: redis.NewClient(tenantTxTsRedisClient), Name: "redis-tx-ts"},

		// Caches
		{Value: cache_tokens.NewCache()},
		{Value: cache_tx.NewCache()},
		{Value: balancelock.NewCache()},
		{Value: transaction_timestamp.NewCache()},

		// DAOs
		{Value: dynamo_tokens.NewDAOWithClient(dynamo.DynamoClient(sess))},
		{Value: dynamo_tx.NewDAOWithClient(dynamo.DynamoClient(sess))},
		{Value: dynamo_tx_scan.NewDAOWithClient(dynamo.DynamoClient(ddbScanSess))},
		{Value: dynamo_tokens_scan.NewDAOWithClient(dynamo.DynamoClient(ddbScanSess))},
		{Value: lock.NewDAOWithClient(dynamo.DynamoClient(sess))},

		// APIs
		{Value: get_tokens.NewAPI()},
		{Value: get_tokens_batch.NewAPI()},
		{Value: get_token.NewAPI()},
		{Value: add_tokens.NewAPI()},
		{Value: async_add_tokens.NewAPI()},
		{Value: start_transaction.NewAPI()},
		{Value: commit_transaction.NewAPI()},
		{Value: check_hold.NewAPI()},
		{Value: get_pending_transactions.NewAPI()},
		{Value: check_holds_for_owner.NewAPI()},
		{Value: get_transaction.NewAPI()},
		{Value: get_transactions.NewAPI()},
		{Value: override_balance.NewAPI()},
		{Value: update_token_status.NewAPI()},
		{Value: delete_balance_by_owner_id.NewAPI()},
		{Value: delete_balance_by_group_id.NewAPI()},

		// API Validation
		{Value: validation.NewTokens()},
		{Value: validation.NewValidator()},
		{Value: add_tokens.NewValidator()},
		{Value: async_add_tokens.NewValidator()},
		{Value: get_tokens.NewValidator()},
		{Value: get_tokens_batch.NewValidator()},
		{Value: get_token.NewValidator()},
		{Value: start_transaction.NewValidator()},
		{Value: commit_transaction.NewValidator()},
		{Value: override_balance.NewValidator()},
		{Value: update_token_status.NewValidator()},
		{Value: delete_balance_by_owner_id.NewValidator()},
		{Value: delete_balance_by_group_id.NewValidator()},

		// Tokens config
		{Value: tokens.NewEntitler()},
		{Value: tokens.NewConsumer()},
		{Value: tokens.NewGetter()},
		{Value: tokens.NewFetcher()},
		{Value: tokens.NewStatusUpdater()},

		// Transactions config
		{Value: transactions.NewValidator()},
		{Value: transactions.NewConductor()},
		{Value: transactions.NewLocker()},
		{Value: transactions.NewExecutor()},

		// Publisher
		{Value: sns.NewPublisher()},

		// RPC Server
		{Value: server.NewServer()},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixMetrics.NewHystrixCommandMetricsCollector(name)
	})

	log.Info("Backend initialization complete")

	return b, nil
}

func (b *backend) Ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func (b *backend) TwirpServer() pachinko.Pachinko {
	return b.Server
}

func (b *backend) Statter() statsd.Statter {
	return b.Stats
}

func (b *backend) Shutdown(ctx context.Context) {
	var wg sync.WaitGroup

	if b.AuditLogger != nil {
		wg.Add(1)
		go func(auditLogger cloudwatchlogger.CloudWatchLogger) {
			auditLogger.Shutdown()
			wg.Done()
		}(b.AuditLogger)
	}

	allDone := make(chan bool)
	go func() {
		wg.Wait()
		allDone <- true
	}()

	select {
	case <-allDone:
		return
	case <-ctx.Done():
		log.Error("context timed out while shutting down")
	}
}
