package sns

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/config/tenant"
	sns_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	metrics_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPublisher_Publish(t *testing.T) {
	Convey("given a token transaction publisher", t, func() {
		snsClient := new(sns_mock.Client)
		statter := new(metrics_mock.Statter)

		tenantConf := &tenant.Config{
			AsyncAddTokensTopicARN: "foobarbaz",
		}
		c := &config.Config{
			TenantDomain: pachinko.Domain_STICKER,
			Tenants: &tenant.Wrapper{
				"sticker": tenantConf,
			},
		}

		publisher := &publisher{
			Config:  c,
			SNS:     snsClient,
			Statter: statter,
		}

		statter.On("Duration", mock.Anything, mock.Anything).Return()

		ctx := context.Background()
		req := AddTokensMessage{
			TransactionID: random.String(32),
			Domain:        pachinko.Domain_STICKER,
			OwnerID:       random.String(16),
			GroupID:       random.String(16),
			Amount:        42,
		}

		Convey("returns nil when", func() {
			Convey("tenant is configured and sns publish is successful", func() {
				snsClient.On("Publish", ctx, tenantConf.AsyncAddTokensTopicARN, mock.Anything).Return(nil)
			})

			err := publisher.PublishAsyncAddTokensRequest(ctx, req)

			So(err, ShouldBeNil)
		})

		Convey("returns error when", func() {
			Convey("the tenant is not configured", func() {
				c.Tenants = &tenant.Wrapper{}
			})

			Convey("the topic arn is not configured", func() {
				c.Tenants.Get(pachinko.Domain_STICKER).AsyncAddTokensTopicARN = ""
			})

			Convey("sns fails to publish", func() {
				snsClient.On("Publish", ctx, tenantConf.AsyncAddTokensTopicARN, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			err := publisher.PublishAsyncAddTokensRequest(ctx, req)

			So(err, ShouldNotBeNil)
		})
	})
}
