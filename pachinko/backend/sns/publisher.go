package sns

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
)

type Publisher interface {
	PublishAsyncAddTokensRequest(ctx context.Context, req AddTokensMessage) error
}

func NewPublisher() Publisher {
	return &publisher{}
}

type publisher struct {
	SNS     sns.Client      `inject:""`
	Config  *config.Config  `inject:""`
	Statter metrics.Statter `inject:""`
}

func (p *publisher) PublishAsyncAddTokensRequest(ctx context.Context, req AddTokensMessage) error {
	conf := p.Config.Tenants.Get(p.Config.TenantDomain)
	if conf == nil {
		return errors.New("cannot perform operation on domain that is not configured")
	}

	if conf.AsyncAddTokensTopicARN == "" {
		return errors.New("no topic configured for async add tokens")
	}

	mJSON, err := json.Marshal(&req)
	if err != nil {
		return err
	}

	start := time.Now()
	defer func() {
		p.Statter.Duration("sns-publish-latency", time.Since(start))
	}()
	return p.SNS.Publish(ctx, conf.AsyncAddTokensTopicARN, string(mJSON))
}
