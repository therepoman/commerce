package sns

import pachinko "code.justin.tv/commerce/pachinko/rpc"

type SNSMessage struct {
	MessageID         string                         `json:"MessageID"`
	Message           string                         `json:"Message"`
	MessageAttributes map[string]SNSMessageAttribute `json:"MessageAttributes,optional"`
}

type SNSMessageAttribute struct {
	Type  string `json:"Type"`
	Value string `json:"Value"`
}

type AddTokensMessage struct {
	TransactionID string          `json:"transaction_id"`
	Domain        pachinko.Domain `json:"domain"`
	OwnerID       string          `json:"owner_id"`
	GroupID       string          `json:"group_id"`
	Amount        int64           `json:"amount"`
}
