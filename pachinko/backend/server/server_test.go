package server

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/backend/clients/cloudwatchlogger"
	add_tokens_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/add_tokens"
	async_add_tokens_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/async_add_tokens"
	commit_transaction_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/commit_transaction"
	delete_balance_by_group_id_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id"
	delete_balance_by_owner_id_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id"
	get_token_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/get_token"
	get_tokens_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/get_tokens"
	start_transaction_api_mock "code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/start_transaction"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_HealthCheck(t *testing.T) {
	Convey("given a server", t, func() {
		server := &server{}

		resp, err := server.HealthCheck(context.Background(), &pachinko.HealthCheckReq{})
		So(resp, ShouldNotBeNil)
		So(err, ShouldBeNil)
	})
}

func TestServer_AddTokens(t *testing.T) {
	Convey("given a server", t, func() {
		api := new(add_tokens_api_mock.API)
		server := &server{
			AddTokensAPI: api,
			AuditLogger:  cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}
		ctx := context.Background()

		Convey("given an add tokens api that does error", func() {
			api.On("Add", ctx, mock.Anything).Return(nil, errors.New("anime not allowed"))

			resp, err := server.AddTokens(ctx, &pachinko.AddTokensReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given an add tokens api that doesn't error", func() {
			api.On("Add", ctx, mock.Anything).Return(&pachinko.AddTokensResp{}, nil)

			resp, err := server.AddTokens(ctx, &pachinko.AddTokensReq{})
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_AsyncAddTokens(t *testing.T) {
	Convey("given a server", t, func() {
		api := new(async_add_tokens_api_mock.API)
		server := &server{
			AsyncAddTokensAPI: api,
			AuditLogger:       cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}
		ctx := context.Background()

		Convey("given an add tokens api that does error", func() {
			api.On("Add", ctx, mock.Anything).Return(nil, errors.New("anime not allowed"))

			resp, err := server.AsyncAddTokens(ctx, &pachinko.AsyncAddTokensReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given an add tokens api that doesn't error", func() {
			api.On("Add", ctx, mock.Anything).Return(&pachinko.AsyncAddTokensResp{}, nil)

			resp, err := server.AsyncAddTokens(ctx, &pachinko.AsyncAddTokensReq{})
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_GetTokens(t *testing.T) {
	Convey("given a server", t, func() {
		getTokenApi := new(get_tokens_api_mock.API)
		server := &server{
			GetTokensAPI: getTokenApi,
			AuditLogger:  cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}
		ctx := context.Background()

		Convey("given a get tokens api that errors", func() {
			getTokenApi.On("Get", ctx, mock.Anything).Return(nil, errors.New("get is failing"))

			resp, err := server.GetTokens(ctx, &pachinko.GetTokensReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given a get tokens api that doesn't error", func() {
			getTokenApi.On("Get", ctx, mock.Anything).Return(&pachinko.GetTokensResp{}, nil)

			resp, err := server.GetTokens(ctx, &pachinko.GetTokensReq{})
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_GetToken(t *testing.T) {
	Convey("given a server", t, func() {
		getTokenApi := new(get_token_api_mock.API)
		server := &server{
			GetTokenAPI: getTokenApi,
			AuditLogger: cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}
		ctx := context.Background()

		Convey("given a get token api that errors", func() {
			getTokenApi.On("Get", ctx, mock.Anything).Return(nil, errors.New("get is failing"))

			resp, err := server.GetToken(ctx, &pachinko.GetTokenReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given a get token api that doesn't error", func() {
			getTokenApi.On("Get", ctx, mock.Anything).Return(&pachinko.GetTokenResp{}, nil)

			resp, err := server.GetToken(ctx, &pachinko.GetTokenReq{})
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_StartTransaction(t *testing.T) {
	Convey("given a server", t, func() {

		api := new(start_transaction_api_mock.API)
		server := &server{
			StartTransactionAPI: api,
			AuditLogger:         cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}

		Convey("given an start transaction api that does error", func() {
			api.On("Start", context.Background(), mock.Anything).Return(nil, errors.New("anime not allowed"))

			resp, err := server.StartTransaction(context.Background(), &pachinko.StartTransactionReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given an start transaction api that doesn't error", func() {
			api.On("Start", context.Background(), mock.Anything).Return(&pachinko.StartTransactionResp{}, nil)

			resp, err := server.StartTransaction(context.Background(), &pachinko.StartTransactionReq{})
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_CommitTransaction(t *testing.T) {
	Convey("given a server", t, func() {

		api := new(commit_transaction_api_mock.API)
		server := &server{
			CommitTransactionAPI: api,
			AuditLogger:          cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}

		Convey("given an commit transaction api that does error", func() {
			api.On("Commit", context.Background(), mock.Anything).Return(nil, errors.New("anime not allowed"))

			resp, err := server.CommitTransaction(context.Background(), &pachinko.CommitTransactionReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given an commit transaction api that doesn't error", func() {
			api.On("Commit", context.Background(), mock.Anything).Return(&pachinko.CommitTransactionResp{}, nil)

			resp, err := server.CommitTransaction(context.Background(), &pachinko.CommitTransactionReq{})
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_DeleteBalanceByOwnerId(t *testing.T) {
	Convey("given a server", t, func() {

		api := new(delete_balance_by_owner_id_api_mock.API)
		server := &server{
			DeleteBalanceByOwnerIdAPI: api,
			AuditLogger:               cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}

		Convey("given an delete balance by owner id api that does error", func() {
			api.On("Delete", context.Background(), mock.Anything).Return(nil, errors.New("failed to delete balance"))

			resp, err := server.DeleteBalanceByOwnerId(context.Background(), &pachinko.DeleteBalanceByOwnerIdReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given an delete balance by owner id api that succeeds", func() {
			api.On("Delete", context.Background(), mock.Anything).Return(&pachinko.DeleteBalanceByOwnerIdResp{}, nil)

			resp, err := server.DeleteBalanceByOwnerId(context.Background(), &pachinko.DeleteBalanceByOwnerIdReq{})
			So(resp, ShouldResemble, &pachinko.DeleteBalanceByOwnerIdResp{})
			So(err, ShouldBeNil)
		})
	})
}

func TestServer_DeleteBalanceByGroupId(t *testing.T) {
	Convey("given a server", t, func() {

		api := new(delete_balance_by_group_id_api_mock.API)
		server := &server{
			DeleteBalanceByGroupIdAPI: api,
			AuditLogger:               cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}

		Convey("given an delete balance by group id api that does error", func() {
			api.On("Delete", context.Background(), mock.Anything).Return(nil, errors.New("failed to delete balance"))

			resp, err := server.DeleteBalanceByGroupId(context.Background(), &pachinko.DeleteBalanceByGroupIdReq{})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("given an delete balance by group id api that succeeds", func() {
			api.On("Delete", context.Background(), mock.Anything).Return(&pachinko.DeleteBalanceByGroupIdResp{}, nil)

			resp, err := server.DeleteBalanceByGroupId(context.Background(), &pachinko.DeleteBalanceByGroupIdReq{})
			So(resp, ShouldResemble, &pachinko.DeleteBalanceByGroupIdResp{})
			So(err, ShouldBeNil)
		})
	})
}
