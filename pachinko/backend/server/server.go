package server

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/api/add_tokens"
	"code.justin.tv/commerce/pachinko/backend/api/async_add_tokens"
	"code.justin.tv/commerce/pachinko/backend/api/check_hold"
	"code.justin.tv/commerce/pachinko/backend/api/check_holds_for_owner"
	"code.justin.tv/commerce/pachinko/backend/api/commit_transaction"
	"code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id"
	"code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id"
	"code.justin.tv/commerce/pachinko/backend/api/get_pending_transactions"
	"code.justin.tv/commerce/pachinko/backend/api/get_token"
	"code.justin.tv/commerce/pachinko/backend/api/get_tokens"
	"code.justin.tv/commerce/pachinko/backend/api/get_tokens_batch"
	"code.justin.tv/commerce/pachinko/backend/api/get_transaction"
	"code.justin.tv/commerce/pachinko/backend/api/get_transactions"
	"code.justin.tv/commerce/pachinko/backend/api/override_balance"
	"code.justin.tv/commerce/pachinko/backend/api/start_transaction"
	"code.justin.tv/commerce/pachinko/backend/api/update_token_status"
	"code.justin.tv/commerce/pachinko/backend/audit"
	"code.justin.tv/commerce/pachinko/backend/clients/cloudwatchlogger"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
)

type server struct {
	AddTokensAPI              add_tokens.API                 `inject:""`
	AsyncAddTokensAPI         async_add_tokens.API           `inject:""`
	GetTokensAPI              get_tokens.API                 `inject:""`
	GetTokensForOwnersAPI     get_tokens_batch.API           `inject:""`
	GetTokenAPI               get_token.API                  `inject:""`
	StartTransactionAPI       start_transaction.API          `inject:""`
	CommitTransactionAPI      commit_transaction.API         `inject:""`
	GetPendingTransactionsAPI get_pending_transactions.API   `inject:""`
	CheckHoldAPI              check_hold.API                 `inject:""`
	CheckHoldsForOwnerAPI     check_holds_for_owner.API      `inject:""`
	GetTransactionRecordAPI   get_transaction.API            `inject:""`
	GetTransactionRecordsAPI  get_transactions.API           `inject:""`
	OverrideBalanceAPI        override_balance.API           `inject:""`
	UpdateTokenStatusAPI      update_token_status.API        `inject:""`
	DeleteBalanceByOwnerIdAPI delete_balance_by_owner_id.API `inject:""`
	DeleteBalanceByGroupIdAPI delete_balance_by_group_id.API `inject:""`

	// Audit Logging for SOX Compliance
	AuditLogger cloudwatchlogger.CloudWatchLogger `inject:""`
}

func NewServer() pachinko.Pachinko {
	return &server{}
}

func (s *server) HealthCheck(ctx context.Context, req *pachinko.HealthCheckReq) (*pachinko.HealthCheckResp, error) {
	return &pachinko.HealthCheckResp{}, nil
}

func (s *server) AddTokens(ctx context.Context, req *pachinko.AddTokensReq) (*pachinko.AddTokensResp, error) {
	s.AsyncLogRequest(ctx, *req, "AddTokens")
	return s.AddTokensAPI.Add(ctx, req)
}

func (s *server) AsyncAddTokens(ctx context.Context, req *pachinko.AsyncAddTokensReq) (*pachinko.AsyncAddTokensResp, error) {
	s.AsyncLogRequest(ctx, *req, "AsyncAddTokens")
	return s.AsyncAddTokensAPI.Add(ctx, req)
}

func (s *server) GetTokens(ctx context.Context, req *pachinko.GetTokensReq) (*pachinko.GetTokensResp, error) {
	return s.GetTokensAPI.Get(ctx, req)
}

func (s *server) GetTokensBatch(ctx context.Context, req *pachinko.GetTokensBatchReq) (*pachinko.GetTokensBatchResp, error) {
	return s.GetTokensForOwnersAPI.Get(ctx, req)
}

func (s *server) GetToken(ctx context.Context, req *pachinko.GetTokenReq) (*pachinko.GetTokenResp, error) {
	return s.GetTokenAPI.Get(ctx, req)
}

func (s *server) StartTransaction(ctx context.Context, req *pachinko.StartTransactionReq) (*pachinko.StartTransactionResp, error) {
	s.AsyncLogRequest(ctx, *req, "StartTransaction")
	return s.StartTransactionAPI.Start(ctx, req)
}

func (s *server) CommitTransaction(ctx context.Context, req *pachinko.CommitTransactionReq) (*pachinko.CommitTransactionResp, error) {
	s.AsyncLogRequest(ctx, *req, "CommitTransaction")
	return s.CommitTransactionAPI.Commit(ctx, req)
}

func (s *server) CheckHold(ctx context.Context, req *pachinko.CheckHoldReq) (*pachinko.CheckHoldResp, error) {
	return s.CheckHoldAPI.Check(ctx, req)
}

func (s *server) GetPendingTransactions(ctx context.Context, req *pachinko.GetPendingTransactionsReq) (*pachinko.GetPendingTransactionsResp, error) {
	return s.GetPendingTransactionsAPI.Get(ctx, req)
}

func (s *server) CheckHoldsForOwner(ctx context.Context, req *pachinko.CheckHoldsForOwnerReq) (*pachinko.CheckHoldsForOwnerResp, error) {
	return s.CheckHoldsForOwnerAPI.Check(ctx, req)
}

func (s *server) GetTransactionRecord(ctx context.Context, req *pachinko.GetTransactionRecordReq) (*pachinko.GetTransactionRecordResp, error) {
	return s.GetTransactionRecordAPI.Get(ctx, req)
}

func (s *server) GetTransactionRecords(ctx context.Context, req *pachinko.GetTransactionRecordsReq) (*pachinko.GetTransactionRecordsResp, error) {
	return s.GetTransactionRecordsAPI.Get(ctx, req)
}

func (s *server) OverrideBalance(ctx context.Context, req *pachinko.OverrideBalanceReq) (*pachinko.OverrideBalanceResp, error) {
	s.AsyncLogRequest(ctx, *req, "OverrideBalance")
	return s.OverrideBalanceAPI.Override(ctx, req)
}

func (s *server) UpdateTokenStatus(ctx context.Context, req *pachinko.UpdateTokenStatusReq) (*pachinko.UpdateTokenStatusResp, error) {
	s.AsyncLogRequest(ctx, *req, "UpdateTokenStatus")
	return s.UpdateTokenStatusAPI.Update(ctx, req)
}

func (s *server) DeleteBalanceByOwnerId(ctx context.Context, req *pachinko.DeleteBalanceByOwnerIdReq) (*pachinko.DeleteBalanceByOwnerIdResp, error) {
	s.AsyncLogRequest(ctx, *req, "DeleteBalanceByOwnerId")
	return s.DeleteBalanceByOwnerIdAPI.Delete(ctx, req)
}

func (s *server) DeleteBalanceByGroupId(ctx context.Context, req *pachinko.DeleteBalanceByGroupIdReq) (*pachinko.DeleteBalanceByGroupIdResp, error) {
	s.AsyncLogRequest(ctx, *req, "DeleteBalanceByGroupId")
	return s.DeleteBalanceByGroupIdAPI.Delete(ctx, req)
}

func (s *server) AsyncLogRequest(ctx context.Context, req interface{}, api string) {
	go func() {
		msg, err := audit.CreateAccessLogMsg(ctx, api, req)
		if err != nil {
			log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", api))
		} else {
			err = s.AuditLogger.Send(ctx, msg)
			if err != nil {
				log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", api))
			}
		}
	}()
}
