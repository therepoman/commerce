// +build integration

package health_check_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("Given a pachinko client", t, func() {
		phantoClient := common.GetPachinkoClient()

		Convey("Test the health check endpoint", func() {
			_, err := phantoClient.HealthCheck(context.Background(), &pachinko.HealthCheckReq{})
			So(err, ShouldBeNil)
		})
	})
}
