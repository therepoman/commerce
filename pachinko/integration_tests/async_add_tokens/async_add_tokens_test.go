// +build integration

package async_add_tokens

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	sqsName               = "sticker-staging-integ-tests-async_add_tokens-sqs"
	quantityOfTokensToAdd = 5
	groupID               = common.QaBitsTestsId
	ownerID               = common.BitsTestUser7Id
	checkInterval         = 1 * time.Second
)

func TestAsyncAddTokens(t *testing.T) {
	ctx := context.Background()
	worker, manager := common.GetSQSWorker(sqsName)

	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()
		expectedTransactionID := uuid.NewV4().String()

		Convey("with an existing balance endpoint", func() {
			balanceResp, err := client.GetToken(ctx, &pachinko.GetTokenReq{
				OwnerId: ownerID,
				GroupId: groupID,
				Domain:  pachinko.Domain_STICKER,
			})
			So(err, ShouldBeNil)
			So(balanceResp, ShouldNotBeNil)

			Convey("async add tokens", func() {
				addTokenReq := pachinko.AsyncAddTokensReq{
					TransactionId: expectedTransactionID,
					Token: &pachinko.Token{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: quantityOfTokensToAdd,
						Domain:   pachinko.Domain_STICKER,
					},
				}
				addTokensResp, err := client.AsyncAddTokens(ctx, &addTokenReq)
				So(err, ShouldBeNil)
				So(addTokensResp, ShouldNotBeNil)

				Convey("transaction should be published and stored in the sqs", func() {
					// messages in this queue are short lived and it doesn't receive much traffic,
					// will retry try to find it
					var found bool
					for i := 0; i < 90; i++ {
						logrus.WithField("iteration", i).WithField("transactionID", expectedTransactionID).Info("Async Add Tokens validating add in SQS queue...")
						time.Sleep(checkInterval)
						found = worker.HasMessageBeenReceived(expectedTransactionID)
						if found {
							break
						}
					}
					So(found, ShouldBeTrue)

					Convey("should have additional tokens in get still", func() {
						updatedResp, err := client.GetToken(ctx, &pachinko.GetTokenReq{
							OwnerId: ownerID,
							GroupId: groupID,
							Domain:  pachinko.Domain_STICKER,
						})
						So(err, ShouldBeNil)
						So(updatedResp.Token.Quantity, ShouldEqual, balanceResp.Token.Quantity+quantityOfTokensToAdd)
					})
				})
			})
		})
	})
	t.Cleanup(func() {
		err := manager.Shutdown(context.Background())
		logrus.WithField("sqsName", sqsName).Info("successfully shutdown worker")
		if err != nil {
			logrus.WithError(err).Errorf("failed to shutdown SQS worker in test %s", sqsName)
		}
	})
}
