// +build integration

package mass_consume_tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	tokensToConsume = 10

	groupID = common.QaBitsTestsId
	ownerID = common.BitsTestUser3Id
)

func TestMassConsumeTokens(t *testing.T) {
	client := common.GetPachinkoClient()

	Convey("given enough tokens to consume", t, func() {
		initialBalance, err := client.AddTokens(context.Background(), &pachinko.AddTokensReq{
			TransactionId: uuid.Must(uuid.NewV4()).String(),
			Token: &pachinko.Token{
				OwnerId:  ownerID,
				GroupId:  groupID,
				Quantity: tokensToConsume,
				Domain:   pachinko.Domain_STICKER,
			},
		})
		So(err, ShouldBeNil)
		So(initialBalance, ShouldNotBeNil)

		Convey("consume all tokens, 1 at a time", func() {
			for i := 0; i < tokensToConsume; i++ {
				resp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
					Operation: pachinko.Operation_CONSUME,
					Tokens: []*pachinko.Token{
						{
							OwnerId:  ownerID,
							GroupId:  groupID,
							Quantity: 1,
							Domain:   pachinko.Domain_STICKER,
						},
					},
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ValidationError, ShouldBeNil)
				So(resp.TransactionId, ShouldNotBeBlank)

				updatedBalance, err := client.CommitTransaction(context.Background(), &pachinko.CommitTransactionReq{
					Domain:        pachinko.Domain_STICKER,
					TransactionId: resp.TransactionId,
				})
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldNotBeNil)
				So(updatedBalance.Tokens, ShouldHaveLength, 1)

				So(updatedBalance.Tokens[0].Quantity, ShouldEqual, initialBalance.Token.Quantity-int64(i+1))
			}
		})
	})
}
