// +build integration

package override_balance

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	firstTestBalance  = 5
	secondTestBalance = 25
	groupID           = common.QaBitsTestsId
	ownerID           = common.BitsTestUser6Id
)

func TestOverrideBalance(t *testing.T) {
	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()

		Convey("with an existing balance endpoint", func() {
			_, err := client.GetToken(context.Background(), &pachinko.GetTokenReq{
				OwnerId: ownerID,
				GroupId: groupID,
				Domain:  pachinko.Domain_STICKER,
			})
			So(err, ShouldBeNil)

			Convey("override token balance once", func() {
				addTokensResp, err := client.OverrideBalance(context.Background(), &pachinko.OverrideBalanceReq{
					Token: &pachinko.Token{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: firstTestBalance,
						Domain:   pachinko.Domain_STICKER,
					},
				})
				So(err, ShouldBeNil)
				So(addTokensResp, ShouldNotBeNil)
				So(addTokensResp.Token, ShouldNotBeNil)
				So(addTokensResp.Token.Quantity, ShouldEqual, firstTestBalance)
			})

			// so we're not setting the balance to the same number over and over again, which doesn't seem like a good test.
			Convey("override token balance again", func() {
				addTokensResp, err := client.OverrideBalance(context.Background(), &pachinko.OverrideBalanceReq{
					Token: &pachinko.Token{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: secondTestBalance,
						Domain:   pachinko.Domain_STICKER,
					},
				})
				So(err, ShouldBeNil)
				So(addTokensResp, ShouldNotBeNil)
				So(addTokensResp.Token, ShouldNotBeNil)
				So(addTokensResp.Token.Quantity, ShouldEqual, secondTestBalance)
			})
		})
	})
}
