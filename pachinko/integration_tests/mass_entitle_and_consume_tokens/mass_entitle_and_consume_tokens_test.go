// +build integration

package mass_entitle_and_consume_tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	tokensToConsume = 10

	groupID = common.QaBitsTestsId
	ownerID = common.BitsTestUser5Id
)

func TestMassConsumeTokens(t *testing.T) {
	client := common.GetPachinkoClient()

	Convey("consume all tokens, 1 at a time", t, func() {
		initialBalance, err := client.GetToken(context.Background(), &pachinko.GetTokenReq{
			GroupId: groupID,
			OwnerId: ownerID,
			Domain:  pachinko.Domain_STICKER,
		})
		So(err, ShouldBeNil)
		So(initialBalance, ShouldNotBeNil)
		So(initialBalance.Token, ShouldNotBeNil)

		for i := 0; i < tokensToConsume; i++ {
			// add 1 token
			balanceAfterAdd, err := client.AddTokens(context.Background(), &pachinko.AddTokensReq{
				TransactionId: uuid.Must(uuid.NewV4()).String(),
				Token: &pachinko.Token{
					OwnerId:  ownerID,
					GroupId:  groupID,
					Quantity: 1,
					Domain:   pachinko.Domain_STICKER,
				},
			})
			So(err, ShouldBeNil)
			So(balanceAfterAdd, ShouldNotBeNil)

			currentBalance := balanceAfterAdd.Token.Quantity

			// consume 1 token
			resp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
				Operation: pachinko.Operation_CONSUME,
				Tokens: []*pachinko.Token{
					{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: 1,
						Domain:   pachinko.Domain_STICKER,
					},
				},
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.ValidationError, ShouldBeNil)
			So(resp.TransactionId, ShouldNotBeBlank)

			balanceAfterConsume, err := client.CommitTransaction(context.Background(), &pachinko.CommitTransactionReq{
				Domain:        pachinko.Domain_STICKER,
				TransactionId: resp.TransactionId,
			})
			So(err, ShouldBeNil)
			So(balanceAfterConsume, ShouldNotBeNil)
			So(balanceAfterConsume.Tokens, ShouldHaveLength, 1)
			So(balanceAfterConsume.Tokens[0].Quantity, ShouldEqual, currentBalance-1)
		}
	})
}
