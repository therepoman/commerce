// +build integration

package get_token_batch

import (
	"context"
	"math/rand"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	getTokensBatchGroupID              = "BALANCE"
	getTokensBatchknownBalanceQuantity = int64(20)
)

func TestGetTokenBatch(t *testing.T) {
	Convey("given a pachinko client", t, func() {
		client := common.GetPachinkoClient()

		Convey("fetches no balance when there's no balance in the batch call", func() {
			// reseeding the random here because it seems to not work from the main test call.
			rand.Seed(time.Now().UTC().UnixNano())
			firstOwnerID := random.NumberString(16)
			logrus.WithField("firstOwnerID", firstOwnerID).Info("testing get tokens batch with first user...")
			resp, err := client.GetTokensBatch(context.Background(), &pachinko.GetTokensBatchReq{
				Domain: pachinko.Domain_STICKER,
				GetTokensBatches: []*pachinko.GetTokensBatch{
					{
						OwnerId: firstOwnerID,
						GroupId: getTokensBatchGroupID,
					},
				},
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.GetTokens(), ShouldHaveLength, 1)
			So(resp.GetTokens()[0], ShouldNotBeNil)
			So(resp.GetTokens()[0].Quantity, ShouldEqual, int64(0))

			Convey("when at least one balance exists", func() {
				resp, err := client.AddTokens(context.Background(), &pachinko.AddTokensReq{
					TransactionId: uuid.Must(uuid.NewV4()).String(),
					Token: &pachinko.Token{
						Domain:   pachinko.Domain_STICKER,
						OwnerId:  firstOwnerID,
						GroupId:  getTokensBatchGroupID,
						Quantity: getTokensBatchknownBalanceQuantity,
					},
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.GetToken(), ShouldNotBeNil)
				So(resp.GetToken().Quantity, ShouldEqual, getTokensBatchknownBalanceQuantity)

				Convey("fetches multiple balances and there's at least one balance with no balance", func() {
					resp, err := client.GetTokensBatch(context.Background(), &pachinko.GetTokensBatchReq{
						Domain: pachinko.Domain_STICKER,
						GetTokensBatches: []*pachinko.GetTokensBatch{
							{
								OwnerId: firstOwnerID,
								GroupId: getTokensBatchGroupID,
							},
							{
								OwnerId: random.NumberString(16),
								GroupId: getTokensBatchGroupID,
							},
						},
					})

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.GetTokens(), ShouldHaveLength, 2)
					So(resp.GetTokens()[0], ShouldNotBeNil)
					So(resp.GetTokens()[0].Quantity, ShouldEqual, getTokensBatchknownBalanceQuantity)
					So(resp.GetTokens()[1], ShouldNotBeNil)
					So(resp.GetTokens()[1].Quantity, ShouldEqual, int64(0))
				})
			})
		})
	})
}
