// +build integration

package delete_balance_by_owner_id

import (
	"context"
	"fmt"
	"testing"
	"time"

	sfnWorker "code.justin.tv/commerce/pachinko/backend/sfn"
	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

/*
- Since we do not publish deletion data, we can’t test it the same way with other transaction updates, instead:
	- Create record for owner that’s safe to delete
	- Invoke delete sfn (we can skip the batching workflow for this)
	- Wait some time
	- Add token for a different user
	- Check that token can be found in SQS
The idea is if deletion was blocking the shard iterator, the new add token request would not have been publishable.
*/

const (
	sqsName               = "sticker-staging-integ-tests-delete_balance_by_owner_id-sqs"
	quantityOfTokensToAdd = 5
	groupID               = common.QaBitsTestsId
	ownerIDSafeToDelete   = common.BitsTestUser8Id
	ownerIDAddTokens      = common.BitsTestUser9Id
	waitTimeForSFN        = 15 * time.Second
	checkInterval         = 1 * time.Second
)

func TestDeleteBalanceByOwnerId(t *testing.T) {
	ctx := context.Background()
	worker, manager := common.GetSQSWorker(sqsName)
	sfnClient := common.GetSFNClient()
	sfnARN := getSFNARN()

	// since GetToken endpoint is cached and there is no easy way to clear cache, we query DynamoDB directly
	tokensDAO := common.GetDynamoTokensDAO()

	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()

		Convey("with a user that does not have any tokens", func() {
			tokenBalanceResp, err := tokensDAO.GetTokenBalance(ctx, ownerIDSafeToDelete, groupID)
			So(err, ShouldBeNil)
			So(tokenBalanceResp, ShouldBeNil) // if not found, the resp will be nil

			Convey("add tokens for owner that's safe to delete", func() {
				addTokenReq := pachinko.AddTokensReq{
					TransactionId: uuid.NewV4().String(),
					Token: &pachinko.Token{
						OwnerId:  ownerIDSafeToDelete,
						GroupId:  groupID,
						Quantity: quantityOfTokensToAdd,
						Domain:   pachinko.Domain_STICKER,
					},
				}
				addTokensResp, err := client.AddTokens(ctx, &addTokenReq)
				So(err, ShouldBeNil)
				So(addTokensResp, ShouldNotBeNil)
				So(addTokensResp.Token.Quantity, ShouldEqual, quantityOfTokensToAdd)

				Convey("invoke owner delete sfn", func() {
					executionName := fmt.Sprintf("integ-test.%s.%d", ownerIDSafeToDelete, time.Now().UnixNano())
					executionInput := sfnWorker.DeleteBalanceByOwnerIdStateMachineInput{
						OwnerIds: []string{ownerIDSafeToDelete},
					}
					err := sfnClient.Execute(ctx, sfnARN, executionName, executionInput)
					So(err, ShouldBeNil)
					time.Sleep(waitTimeForSFN)

					Convey("querying deleted owner should return 0 quantity", func() {
						tokenBalanceResp, err := tokensDAO.GetTokenBalance(ctx, ownerIDSafeToDelete, groupID)
						So(err, ShouldBeNil)
						So(tokenBalanceResp, ShouldBeNil)

						Convey("Add token for a different owner", func() {
							addTokenTransactionID := uuid.NewV4().String()

							addTokenReq := pachinko.AddTokensReq{
								TransactionId: addTokenTransactionID,
								Token: &pachinko.Token{
									OwnerId:  ownerIDAddTokens,
									GroupId:  groupID,
									Quantity: quantityOfTokensToAdd,
									Domain:   pachinko.Domain_STICKER,
								},
							}
							addTokensResp, err := client.AddTokens(ctx, &addTokenReq)
							So(err, ShouldBeNil)
							So(addTokensResp, ShouldNotBeNil)

							Convey("transaction should be published and stored in the sqs", func() {
								var found bool
								for i := 0; i < 90; i++ {
									logrus.WithField("iteration", i).WithField("transactionID", addTokenTransactionID).Info("Add Tokens validating add in SQS queue...")
									time.Sleep(checkInterval)
									found = worker.HasMessageBeenReceived(addTokenTransactionID)
									if found {
										break
									}
								}
								So(found, ShouldBeTrue)
							})
						})
					})
				})
			})
		})
	})
	t.Cleanup(func() {
		err := manager.Shutdown(context.Background())
		logrus.WithField("sqsName", sqsName).Info("successfully shutdown worker")
		if err != nil {
			logrus.WithError(err).Errorf("failed to shutdown SQS worker in test %s", sqsName)
		}
	})
}

func getSFNARN() string {
	var SFNARN string

	cfg := common.LoadConfig()
	tenantConf := cfg.Tenants.Get(cfg.TenantDomain)
	if tenantConf == nil {
		logrus.Panic("Failed to get tenant config")
	}

	if tenantConf.DeleteBalanceByOwnerIdStepFunctionConfig != nil {
		SFNARN = tenantConf.DeleteBalanceByOwnerIdStepFunctionConfig.ARN
	}

	if SFNARN == "" {
		logrus.Panic("Failed to get ARN of SFN")
	}

	return SFNARN
}
