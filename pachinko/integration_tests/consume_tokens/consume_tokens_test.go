// +build integration

package consume_tokens

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

const (
	quantityOfTokensToAdd = 5
	groupID               = common.QaBitsTestsId
	ownerID               = common.BitsTestUser2Id
)

func TestConsumeTokens(t *testing.T) {
	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()

		Convey("without a transaction started, should fail", func() {
			_, err := client.CommitTransaction(context.Background(), &pachinko.CommitTransactionReq{
				TransactionId: uuid.NewV4().String(),
				Domain:        pachinko.Domain_STICKER,
			})

			So(err, ShouldNotBeNil)

			twirpErr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
		})

		Convey("with a transaction started without enough tokens should fail", func() {
			token := &pachinko.Token{
				OwnerId:  ownerID,
				GroupId:  groupID,
				Quantity: 10000,
				Domain:   pachinko.Domain_STICKER,
			}

			resp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
				Operation: pachinko.Operation_CONSUME,
				Tokens:    []*pachinko.Token{token},
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.ValidationError, ShouldNotBeNil)
			So(resp.ValidationError.Code, ShouldEqual, pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE)
			So(resp.ValidationError.InvalidToken, ShouldNotBeNil)
			So(resp.ValidationError.InvalidToken.OwnerId, ShouldEqual, ownerID)
			So(resp.ValidationError.InvalidToken.GroupId, ShouldEqual, groupID)
			So(resp.ValidationError.InvalidToken.Quantity, ShouldEqual, 10000)

			Convey("with added tokens", func() {
				initialBalance, err := client.AddTokens(context.Background(), &pachinko.AddTokensReq{
					TransactionId: uuid.NewV4().String(),
					Token: &pachinko.Token{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: quantityOfTokensToAdd,
						Domain:   pachinko.Domain_STICKER,
					},
				})
				So(err, ShouldBeNil)
				So(initialBalance, ShouldNotBeNil)

				Convey("should get a transaction on the balance", func() {
					resp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
						Operation: pachinko.Operation_CONSUME,
						Tokens: []*pachinko.Token{
							{
								OwnerId:  ownerID,
								GroupId:  groupID,
								Quantity: initialBalance.Token.Quantity,
								Domain:   pachinko.Domain_STICKER,
							},
						},
					})

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.ValidationError, ShouldBeNil)
					So(resp.TransactionId, ShouldNotBeBlank)

					Convey("should be able to call start again with the same transaction ID and it will not error", func() {
						resp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
							TransactionId: resp.TransactionId,
							Operation:     pachinko.Operation_CONSUME,
							Tokens: []*pachinko.Token{
								{
									OwnerId:  ownerID,
									GroupId:  groupID,
									Quantity: initialBalance.Token.Quantity,
									Domain:   pachinko.Domain_STICKER,
								},
							},
						})

						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(resp.ValidationError, ShouldBeNil)
						So(resp.TransactionId, ShouldNotBeBlank)

						Convey("should consume all remaining tokens without error", func() {
							updatedBalance, err := client.CommitTransaction(context.Background(), &pachinko.CommitTransactionReq{
								Domain:        pachinko.Domain_STICKER,
								TransactionId: resp.TransactionId,
							})
							So(err, ShouldBeNil)
							So(updatedBalance, ShouldNotBeNil)
							So(updatedBalance.Tokens, ShouldHaveLength, 1)
							So(updatedBalance.Tokens[0].Quantity, ShouldEqual, 0)
						})
					})
				})
			})
		})
	})
}
