package common

import (
	"encoding/json"

	"code.justin.tv/commerce/logrus"
	ledgerman_models "code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/models"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Worker struct {
	sqsName                 string
	transactionsReceivedSet map[string]struct{}
}

func NewWorker(sqsName string) *Worker {
	worker := Worker{
		sqsName:                 sqsName,
		transactionsReceivedSet: make(map[string]struct{}),
	}
	return &worker
}

func (w *Worker) Handle(msg *sqs.Message) error {
	var parseSNS sns.PublishInput
	err := json.Unmarshal([]byte(*msg.Body), &parseSNS)
	if err != nil {
		return err
	}

	var parseMessage ledgerman_models.BalanceUpdate
	err = json.Unmarshal([]byte(*parseSNS.Message), &parseMessage)
	if err != nil {
		return err
	}

	for _, transactionID := range parseMessage.TransactionIDs {
		logrus.WithFields(logrus.Fields{
			"sqsName":       w.sqsName,
			"transactionID": transactionID,
		}).Info("message received on queue")
		w.transactionsReceivedSet[transactionID] = struct{}{}
	}

	return nil
}

func (w *Worker) HasMessageBeenReceived(transactionID string) bool {
	_, exists := w.transactionsReceivedSet[transactionID]
	return exists
}
