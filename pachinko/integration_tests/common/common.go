package common

import (
	"net/http"
	"os"

	sqsWorker "code.justin.tv/commerce/gogogadget/aws/sqs/worker"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/backend/circuit/dynamo"
	"code.justin.tv/commerce/pachinko/backend/clients/sfn"
	dynamo_tokens "code.justin.tv/commerce/pachinko/backend/dynamo/tokens"
	dynamo_tx "code.justin.tv/commerce/pachinko/backend/dynamo/transactions"
	"code.justin.tv/commerce/pachinko/backend/metrics"
	"code.justin.tv/commerce/pachinko/config"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
)

const (
	QaBitsTestsId   = "145903336"
	BitsTestUser1Id = "138165611" // bits_test_1
	BitsTestUser2Id = "138165652" // bits_test_2
	BitsTestUser3Id = "138165685" // bits_test_3
	BitsTestUser4Id = "138165669" // random user ID that maybe doesn't exist
	BitsTestUser5Id = "244376671" // leosun_test2
	BitsTestUser6Id = "177167226" // bontatest
	BitsTestUser7Id = "138165677" // random user ID that maybe doesn't exist
	BitsTestUser8Id = "987654321" // random user ID that maybe doesn't exist
	BitsTestUser9Id = "987654322" // random user ID that maybe doesn't exist
)

func LoadConfig() *config.Config {
	envVar := os.Getenv(config.EnvironmentEnvironmentVariable)
	if envVar != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", envVar)
	}
	env := config.GetOrDefaultEnvironment(envVar)
	if env == config.Prod {
		log.Panic("Please don't run these integration tests against prod!")
	}
	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}
	log.Infof("Loaded config: %s", cfg.Environment.Name)
	return cfg
}

func GetPachinkoClient() pachinko.Pachinko {
	cfg := LoadConfig()

	s2sConfig := caller.Config{
		DisableStatsClient: true,
	}

	rt, err := caller.NewRoundTripper(cfg.GetS2SName(), &s2sConfig, log.New())
	if err != nil {
		log.Panicf("Failed to create s2s round tripper: %v", err)
	}

	httpClient := http.Client{
		Transport: rt,
	}

	return pachinko.NewPachinkoProtobufClient(cfg.Environment.Endpoint, &httpClient)
}

func GetSQSWorker(sqsName string) (*Worker, sqsWorker.Manager) {
	cfg := LoadConfig()

	sess, err := session.NewSession()
	if err != nil {
		log.Panic("could not start AWS Session")
	}
	sqsClient := sqs.New(sess, &aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})

	worker := NewWorker(sqsName)
	manager := sqsWorker.NewSQSWorkerManager(
		sqsName,
		1,
		1,
		sqsClient,
		worker,
		&statsd.NoopClient{})
	log.Info("started worker")

	return worker, manager
}

func GetSFNClient() sfn.Client {
	cfg := LoadConfig()
	sfnClient, err := sfn.NewDefaultClient(cfg)
	if err != nil {
		log.Panic("could not start a new SFN client")
	}
	return sfnClient
}

func GetDynamoTokensDAO() dynamo_tokens.DAO {
	cfg := LoadConfig()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})
	if err != nil {
		log.Panic("could not start AWS Session")
	}

	stats, err := statsd.NewNoopClient()
	if err != nil {
		log.Panic("could not initialize statsd NoopClient")
	}

	tokensDAO := dynamo_tokens.NewDAOWithClient(dynamo.DynamoClient(sess))

	deps := []*inject.Object{
		{Value: cfg},
		{Value: stats},
		{Value: metrics.NewStatter()},
		{Value: tokensDAO},
		{Value: dynamo_tx.NewDAOWithClient(dynamo.DynamoClient(sess))},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)
	if err != nil {
		log.Panic("could not initialize dependencies for Dynamo Tokens DAO")
	}

	err = graph.Populate()
	if err != nil {
		log.Panic("could not populate dependencies for Dynamo Tokens DAO")
	}

	return tokensDAO
}
