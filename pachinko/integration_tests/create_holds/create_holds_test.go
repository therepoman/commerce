// +build integration

package create_holds

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

const (
	quantityOfTokensToAdd  = 10
	quantityOfTokensToHold = 10
	groupID                = common.QaBitsTestsId
	ownerID                = common.BitsTestUser4Id
)

func TestCreateHolds(t *testing.T) {
	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()

		Convey("start a hold transaction", func() {
			startTxResp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
				Operation: pachinko.Operation_CREATE_HOLD,
				Tokens: []*pachinko.Token{
					{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: quantityOfTokensToHold,
						Domain:   pachinko.Domain_STICKER,
					},
				},
			})

			So(startTxResp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
		})

		Convey("start a release transaction", func() {
			startTxResp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
				Operation: pachinko.Operation_RELEASE_HOLD,
				Tokens: []*pachinko.Token{
					{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: quantityOfTokensToHold,
						Domain:   pachinko.Domain_STICKER,
					},
				},
			})

			So(startTxResp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
		})

		Convey("start a finalize transaction", func() {
			startTxResp, err := client.StartTransaction(context.Background(), &pachinko.StartTransactionReq{
				Operation: pachinko.Operation_FINALIZE_HOLD,
				Tokens: []*pachinko.Token{
					{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: quantityOfTokensToHold,
						Domain:   pachinko.Domain_STICKER,
					},
				},
			})

			So(startTxResp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.Unimplemented)
		})
	})
}
