// +build integration

package add_tokens

import (
	"context"
	"sync"
	"testing"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/integration_tests/common"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	uuid "github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	sqsName               = "sticker-staging-integ-tests-add_tokens-sqs"
	quantityOfTokensToAdd = 5
	groupID               = common.QaBitsTestsId
	ownerID               = common.BitsTestUser1Id
	checkInterval         = 1 * time.Second
)

func TestAddTokens(t *testing.T) {
	ctx := context.Background()
	worker, manager := common.GetSQSWorker(sqsName)

	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()
		expectedTransactionID := uuid.NewV4().String()

		Convey("with an existing balance endpoint", func() {
			balanceResp, err := client.GetToken(ctx, &pachinko.GetTokenReq{
				OwnerId: ownerID,
				GroupId: groupID,
				Domain:  pachinko.Domain_STICKER,
			})
			So(err, ShouldBeNil)
			So(balanceResp, ShouldNotBeNil)

			Convey("add tokens", func() {
				addTokenReq := pachinko.AddTokensReq{
					TransactionId: expectedTransactionID,
					Token: &pachinko.Token{
						OwnerId:  ownerID,
						GroupId:  groupID,
						Quantity: quantityOfTokensToAdd,
						Domain:   pachinko.Domain_STICKER,
					},
				}
				addTokensResp, err := client.AddTokens(ctx, &addTokenReq)
				So(err, ShouldBeNil)
				So(addTokensResp, ShouldNotBeNil)
				So(addTokensResp.Token.Quantity, ShouldEqual, balanceResp.Token.Quantity+quantityOfTokensToAdd)

				Convey("should have additional tokens in get still", func() {
					updatedResp, err := client.GetToken(ctx, &pachinko.GetTokenReq{
						OwnerId: ownerID,
						GroupId: groupID,
						Domain:  pachinko.Domain_STICKER,
					})
					So(err, ShouldBeNil)
					So(updatedResp.Token.Quantity, ShouldEqual, addTokensResp.Token.Quantity)

					Convey("transaction should be published and stored in the sqs", func() {
						// messages in this queue are short lived and it doesn't receive much traffic,
						// will retry to try to find it
						var found bool
						for i := 0; i < 90; i++ {
							logrus.WithField("iteration", i).WithField("transactionID", expectedTransactionID).Info("Add Tokens validating add in SQS queue...")
							time.Sleep(checkInterval)
							found = worker.HasMessageBeenReceived(expectedTransactionID)
							if found {
								break
							}
						}
						So(found, ShouldBeTrue)
					})
				})
			})
		})
	})
	t.Cleanup(func() {
		err := manager.Shutdown(context.Background())
		logrus.WithField("sqsName", sqsName).Info("successfully shutdown worker")
		if err != nil {
			logrus.WithError(err).Errorf("failed to shutdown SQS worker in test %s", sqsName)
		}
	})
}

func TestAddTokensIdempotency(t *testing.T) {
	ctx := context.Background()

	Convey("Given a pachinko client", t, func() {
		client := common.GetPachinkoClient()

		Convey("test with patient requests", func() {
			ownerID := uuid.NewV4().String()
			balance := getBalance(ctx, client, ownerID)

			req := pachinko.AddTokensReq{
				TransactionId: uuid.NewV4().String(),
				Token: &pachinko.Token{
					OwnerId:  ownerID,
					GroupId:  groupID,
					Quantity: quantityOfTokensToAdd,
					Domain:   pachinko.Domain_STICKER,
				},
			}

			// first
			So(runTokenIncrement(ctx, client, req, balance), ShouldEqual, 0)
			// second
			So(runTokenIncrement(ctx, client, req, balance), ShouldEqual, 1)
			// third
			So(runTokenIncrement(ctx, client, req, balance), ShouldEqual, 1)

			Convey("should only have one set of additional tokens in the get", func() {
				So(getBalance(ctx, client, ownerID), ShouldEqual, balance+quantityOfTokensToAdd)
			})
		})

		Convey("test with async impatient requests", func() {
			ownerID := uuid.NewV4().String()
			balance := getBalance(ctx, client, ownerID)

			req := pachinko.AddTokensReq{
				TransactionId: uuid.NewV4().String(),
				Token: &pachinko.Token{
					OwnerId:  ownerID,
					GroupId:  groupID,
					Quantity: quantityOfTokensToAdd,
					Domain:   pachinko.Domain_STICKER,
				},
			}

			var wg sync.WaitGroup
			wg.Add(3)
			var validationErrorCount int

			// first
			go func() {
				Convey("first idempotent async add tokens run", t, func() {
					validationErrorCount += runTokenIncrement(ctx, client, req, balance)
				})
				wg.Done()
			}()

			time.Sleep(10)

			// second
			go func() {
				Convey("second idempotent async add tokens run", t, func() {
					validationErrorCount += runTokenIncrement(ctx, client, req, balance)
				})
				wg.Done()
			}()

			time.Sleep(10)

			// third
			go func() {
				Convey("third idempotent async add tokens run", t, func() {
					validationErrorCount += runTokenIncrement(ctx, client, req, balance)
				})
				wg.Done()
			}()
			wg.Wait()

			So(validationErrorCount, ShouldEqual, 2)
			Convey("should only have one set of additional tokens in the get", func() {
				So(getBalance(ctx, client, ownerID), ShouldEqual, balance+quantityOfTokensToAdd)
			})
		})
	})
}

func runTokenIncrement(ctx context.Context, client pachinko.Pachinko, req pachinko.AddTokensReq, previousBalance int) int {
	resp, err := client.AddTokens(ctx, &req)
	So(err, ShouldBeNil)
	So(resp, ShouldNotBeNil)
	So(resp.Token.Quantity, ShouldEqual, previousBalance+quantityOfTokensToAdd)

	if resp.ValidationError != nil && resp.ValidationError.Code == pachinko.ValidationErrorCode_TRANSACTION_COMMITTED {
		return 1
	} else {
		return 0
	}
}

func getBalance(ctx context.Context, client pachinko.Pachinko, ownerID string) int {
	updatedResp, err := client.GetToken(ctx, &pachinko.GetTokenReq{
		OwnerId: ownerID,
		GroupId: groupID,
		Domain:  pachinko.Domain_STICKER,
	})
	So(err, ShouldBeNil)
	return int(updatedResp.Token.Quantity)
}
