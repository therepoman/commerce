// Code generated by mockery v1.0.0. DO NOT EDIT.

package transactions_mock

import mock "github.com/stretchr/testify/mock"

// DAO is an autogenerated mock type for the DAO type
type DAO struct {
	mock.Mock
}

// DeleteAllByGroupId provides a mock function with given fields: groupId
func (_m *DAO) DeleteAllByGroupId(groupId string) error {
	ret := _m.Called(groupId)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(groupId)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteAllByGroupIds provides a mock function with given fields: groupIds
func (_m *DAO) DeleteAllByGroupIds(groupIds []string) error {
	ret := _m.Called(groupIds)

	var r0 error
	if rf, ok := ret.Get(0).(func([]string) error); ok {
		r0 = rf(groupIds)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteAllByOwnerId provides a mock function with given fields: ownerId
func (_m *DAO) DeleteAllByOwnerId(ownerId string) error {
	ret := _m.Called(ownerId)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(ownerId)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteAllByOwnerIds provides a mock function with given fields: ownerIds
func (_m *DAO) DeleteAllByOwnerIds(ownerIds []string) error {
	ret := _m.Called(ownerIds)

	var r0 error
	if rf, ok := ret.Get(0).(func([]string) error); ok {
		r0 = rf(ownerIds)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
