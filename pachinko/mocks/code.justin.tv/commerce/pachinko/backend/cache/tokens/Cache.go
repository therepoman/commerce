// Code generated by mockery v1.0.0. DO NOT EDIT.

package tokens_mock

import (
	context "context"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	mock "github.com/stretchr/testify/mock"
)

// Cache is an autogenerated mock type for the Cache type
type Cache struct {
	mock.Mock
}

// Del provides a mock function with given fields: ctx, ownerID, groupID
func (_m *Cache) Del(ctx context.Context, ownerID string, groupID string) {
	_m.Called(ctx, ownerID, groupID)
}

// Get provides a mock function with given fields: ctx, ownerID, groupID
func (_m *Cache) Get(ctx context.Context, ownerID string, groupID string) *pachinko.Token {
	ret := _m.Called(ctx, ownerID, groupID)

	var r0 *pachinko.Token
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *pachinko.Token); ok {
		r0 = rf(ctx, ownerID, groupID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*pachinko.Token)
		}
	}

	return r0
}

// GetAll provides a mock function with given fields: ctx, ownerID
func (_m *Cache) GetAll(ctx context.Context, ownerID string) (map[string]*pachinko.Token, int) {
	ret := _m.Called(ctx, ownerID)

	var r0 map[string]*pachinko.Token
	if rf, ok := ret.Get(0).(func(context.Context, string) map[string]*pachinko.Token); ok {
		r0 = rf(ctx, ownerID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]*pachinko.Token)
		}
	}

	var r1 int
	if rf, ok := ret.Get(1).(func(context.Context, string) int); ok {
		r1 = rf(ctx, ownerID)
	} else {
		r1 = ret.Get(1).(int)
	}

	return r0, r1
}

// GetBatch provides a mock function with given fields: ctx, ownerID, groupIDs
func (_m *Cache) GetBatch(ctx context.Context, ownerID string, groupIDs []string) map[string]*pachinko.Token {
	ret := _m.Called(ctx, ownerID, groupIDs)

	var r0 map[string]*pachinko.Token
	if rf, ok := ret.Get(0).(func(context.Context, string, []string) map[string]*pachinko.Token); ok {
		r0 = rf(ctx, ownerID, groupIDs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]*pachinko.Token)
		}
	}

	return r0
}

// MultiGet provides a mock function with given fields: ctx, ownerGroupPairs
func (_m *Cache) MultiGet(ctx context.Context, ownerGroupPairs ...[2]string) []*pachinko.Token {
	_va := make([]interface{}, len(ownerGroupPairs))
	for _i := range ownerGroupPairs {
		_va[_i] = ownerGroupPairs[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	var r0 []*pachinko.Token
	if rf, ok := ret.Get(0).(func(context.Context, ...[2]string) []*pachinko.Token); ok {
		r0 = rf(ctx, ownerGroupPairs...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*pachinko.Token)
		}
	}

	return r0
}

// MultiSet provides a mock function with given fields: ctx, ownerGroupTokenMap
func (_m *Cache) MultiSet(ctx context.Context, ownerGroupTokenMap map[[2]string]*pachinko.Token) {
	_m.Called(ctx, ownerGroupTokenMap)
}

// Set provides a mock function with given fields: ctx, token
func (_m *Cache) Set(ctx context.Context, token *pachinko.Token) {
	_m.Called(ctx, token)
}

// SetBatch provides a mock function with given fields: ctx, ownerID, _a2
func (_m *Cache) SetBatch(ctx context.Context, ownerID string, _a2 map[string]*pachinko.Token) {
	_m.Called(ctx, ownerID, _a2)
}
