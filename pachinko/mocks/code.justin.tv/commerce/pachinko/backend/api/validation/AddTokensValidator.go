// Code generated by mockery v1.0.0. DO NOT EDIT.

package validation_mock

import (
	context "context"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	mock "github.com/stretchr/testify/mock"
)

// AddTokensValidator is an autogenerated mock type for the AddTokensValidator type
type AddTokensValidator struct {
	mock.Mock
}

// IsValid provides a mock function with given fields: ctx, transactionID, token
func (_m *AddTokensValidator) IsValid(ctx context.Context, transactionID string, token *pachinko.Token) (*pachinko.ValidationError, error) {
	ret := _m.Called(ctx, transactionID, token)

	var r0 *pachinko.ValidationError
	if rf, ok := ret.Get(0).(func(context.Context, string, *pachinko.Token) *pachinko.ValidationError); ok {
		r0 = rf(ctx, transactionID, token)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*pachinko.ValidationError)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *pachinko.Token) error); ok {
		r1 = rf(ctx, transactionID, token)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
