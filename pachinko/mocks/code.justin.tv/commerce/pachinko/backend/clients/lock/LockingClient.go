// Code generated by mockery v1.0.0. DO NOT EDIT.

package lock_mock

import (
	context "context"

	lock "code.justin.tv/commerce/pachinko/backend/clients/lock"
	mock "github.com/stretchr/testify/mock"
)

// LockingClient is an autogenerated mock type for the LockingClient type
type LockingClient struct {
	mock.Mock
}

// ObtainLock provides a mock function with given fields: ctx, key
func (_m *LockingClient) ObtainLock(ctx context.Context, key string) (lock.Lock, error) {
	ret := _m.Called(ctx, key)

	var r0 lock.Lock
	if rf, ok := ret.Get(0).(func(context.Context, string) lock.Lock); ok {
		r0 = rf(ctx, key)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(lock.Lock)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, key)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
