package mocks

// Tokens Fetcher
//go:generate retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens -outpkg=tokens_mock

// Tokens Getter
//go:generate retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens -outpkg=tokens_mock

// Tokens Entitler
//go:generate retool do mockery -name=Entitler -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens -outpkg=tokens_mock

// Tokens Consumer
//go:generate retool do mockery -name=Consumer -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens -outpkg=tokens_mock

// Tokens Consumer
//go:generate retool do mockery -name=StatusUpdater -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/tokens -outpkg=tokens_mock

// Firehose AWS Client
//go:generate retool do mockery -name=FirehoseAPI -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/vendor/github.com/aws/aws-sdk-go/service/firehose/firehoseiface -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/github.com/aws/aws-sdk-go/service/firehose/firehoseiface -outpkg=firehoseiface_mock

// GoGoGadget SNS Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/vendor/code.justin.tv/commerce/gogogadget/aws/sns -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/gogogadget/aws/sns -outpkg=sns_mock

// Add Tokens API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/add_tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/add_tokens -outpkg=add_tokens_api_mock

// Add Tokens API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/add_tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/add_tokens -outpkg=add_tokens_api_mock

// Async Add Tokens API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/async_add_tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/async_add_tokens -outpkg=async_add_tokens_api_mock

// Async Add Tokens API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/async_add_tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/async_add_tokens -outpkg=async_add_tokens_api_mock

// DeleteBalanceByOwnerId API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id -outpkg=delete_balance_by_owner_id_api_mock

// DeleteBalanceByOwnerId API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_owner_id -outpkg=delete_balance_by_owner_id_api_mock

// DeleteBalanceByGroupId API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id -outpkg=delete_balance_by_group_id_api_mock

// DeleteBalanceByGroupId API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/delete_balance_by_group_id -outpkg=delete_balance_by_group_id_api_mock

// Get Tokens API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/get_tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/get_tokens -outpkg=get_tokens_api_mock

// Get Token API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/get_token -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/get_token -outpkg=get_token_api_mock

// Start Transaction API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/start_transaction -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/start_transaction -outpkg=start_transaction_api_mock

// Start Transaction API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/start_transaction -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/start_transaction -outpkg=start_transaction_api_mock

// Commit Transaction API
//go:generate retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/commit_transaction -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/commit_transaction -outpkg=commit_transaction_api_mock

// Commit Transaction API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/commit_transaction -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/commit_transaction -outpkg=commit_transaction_api_mock

// Update Token Status API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/update_token_status -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/update_token_status -outpkg=update_token_status_api_mock

// Token API Validator
//go:generate retool do mockery -name=Tokens -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/validation -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/validation -outpkg=validation_mock

// Add Tokens Shared API Validator
//go:generate retool do mockery -name=AddTokensValidator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/validation -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/validation -outpkg=validation_mock

// Commit Transaction API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/commit_transaction -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/commit_transaction -outpkg=commit_transaction_api_mock

// Token API Validator
//go:generate retool do mockery -name=Tokens -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/validation -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/validation -outpkg=validation_mock

// Override Balance API Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/api/override_balance -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/api/override_balance -outpkg=override_balance_api_mock

// Balance Locker
//go:generate retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/cache/balancelock -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/balancelock -outpkg=balancelock_mock

// Transactions Cache
//go:generate retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/cache/transaction -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/transaction -outpkg=transaction_mock

// Tokens Cache
//go:generate retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/cache/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/tokens -outpkg=tokens_mock

// Transaction Timestamp Cache
//go:generate retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/cache/transaction_timestamp -outpkg=transaction_timestamp_mock

// Tokens DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/dynamo/tokens -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens -outpkg=tokens_mock

// Transactions DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/dynamo/transactions -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions -outpkg=transactions_mock

// TransactionsScan DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/transactions_scan -outpkg=transactions_scan_mock

// TokensScan DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/dynamo/tokens_scan -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/tokens_scan -outpkg=tokens_scan_mock

// WorkerPool
//go:generate retool do mockery -name=WorkerPool -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/dynamo/worker_pool -outpkg=worker_pool_mock

// SFN Client Wrapper
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/clients/sfn -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sfn -outpkg=sfn_mock

// SQS Client Wrapper
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/clients/sqs -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/sqs -outpkg=sqs_mock

// Redis Thick Client
//go:generate retool do mockery -name=ThickClient -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/vendor/code.justin.tv/chat/rediczar -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/chat/rediczar -outpkg=redis_mock

// Redis Client Wrapper
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/clients/redis -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/redis -outpkg=redis_mock

// Redshift Client Wrapper
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/lambdas/backend/clients/redshift -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/backend/clients/redshift -outpkg=redshift_mock

// Redshift Transactions DAO
//go:generate retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/lambdas/backend/redshift/transactions -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/backend/redshift/transactions -outpkg=transactions_mock

// Redis Lock Client
//go:generate retool do mockery -name=LockingClient -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/clients/lock -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/lock -outpkg=lock_mock

// Redis Lock
//go:generate retool do mockery -name=Lock -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/clients/lock -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/clients/lock -outpkg=lock_mock

// Transaction Conductor
//go:generate retool do mockery -name=Conductor -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/transactions -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions -outpkg=transactions_mock

// Transaction Locker
//go:generate retool do mockery -name=Locker -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/transactions -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions -outpkg=transactions_mock

// Transaction Validator
//go:generate retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/transactions -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions -outpkg=transactions_mock

// Transaction Executor
//go:generate retool do mockery -name=Executor -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/transactions -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/transactions -outpkg=transactions_mock

// Ledgerman SNS Publisher
//go:generate retool do mockery -name=Publisher -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/sns -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/sns -outpkg=sns_mock

// Firehose Client Wrapper
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/clients/firehose -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/lambdas/functions/ledgerman/backend/clients/firehose -outpkg=firehose_mock

// Statter
//go:generate retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/vendor/github.com/twitchtv/twirp/hooks/statsd -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/github.com/twitchtv/twirp/hooks/statsd -outpkg=statsd_mock

// Stats Wrapper
//go:generate retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/metrics -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/metrics -outpkg=metrics_mock

// SNS Publisher
//go:generate retool do mockery -name=Publisher -dir=$GOPATH/src/code.justin.tv/commerce/pachinko/backend/sns -output=$GOPATH/src/code.justin.tv/commerce/pachinko/mocks/code.justin.tv/commerce/pachinko/backend/sns -outpkg=sns_mock
