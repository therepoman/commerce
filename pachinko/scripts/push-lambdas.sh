#!/bin/bash

set -e

# Flags
s3bucket=$2
version=$3

# User Config
profile=$4
builddir=./builds
lambdadir=$1

tenants=(cp sticker bits st wallet freesubs bb)

push() {
    name=$1
    version=$2
    s3path=$name/$version

    echo "-> [$name] pushing to s3 for version '$version'...."

    for tenant in ${tenants[@]}; do
        aws s3 --profile=$profile cp $builddir/$name.zip s3://$tenant$s3bucket/$s3path/$name.zip
        aws s3 --profile=$profile cp --content-type text/plain $builddir/$name.hash s3://$tenant$s3bucket/$s3path/hash
    done
}

buildall() {
    for path in $lambdadir/*; do
        name=$(basename $path)
        echo "-> [$name] building lambda..."

        push $name $version
    done
}

buildall