#!/bin/bash

set -e

# Config
builddir=./builds
lambdadir=$1
arch=$2

clean() {
  echo "-> Cleaning build directory"
  rm -rf $builddir
}

build() {
    path=$1
    name=$2
    bin=$builddir/$name/$name
    currentdir=`pwd`

    GOOS=$arch go build -o $bin $path/*.go
    mkdir -p $builddir/$name/etc/pachinko/config
    cp -R $currentdir/config/data/* $builddir/$name/etc/pachinko/config
    cd $(dirname $bin) && zip -r ../$name.zip * > /dev/null && cd $currentdir

    openssl dgst -sha256 -binary $builddir/$name.zip | openssl enc -base64 > $builddir/$name.hash

    hash=$(cat $builddir/$name.hash)

    echo "-> [$name] computed hash: $hash"
}

buildall() {
    for path in $lambdadir/*; do
        name=$(basename $path)
        echo "-> [$name] building lambda..."

        build $path $name
    done
}

clean
buildall