#!/bin/bash

set -e

# Flags
name=$1
s3BucketSuffix=$2
version=$3
profile=$4

# User Config
s3bucket="-$ENVIRONMENT-$s3BucketSuffix"
tenants=(cp sticker bits st wallet freesubs bb)

name="${name//\/}"
version="${version//\/}"

for tenant in ${tenants[@]}; do
    aws lambda --region us-west-2 --profile $profile update-function-code \
        --function-name $tenant-$ENVIRONMENT-$name \
        --s3-bucket $tenant$s3bucket \
        --s3-key $name/$version/$name.zip \
        --publish
done
