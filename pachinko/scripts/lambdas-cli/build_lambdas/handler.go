package build_lambdas

import (
	"archive/zip"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"time"

	str "code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/constants"
	"github.com/codegangsta/cli"
)

type BuildLambdaHandler struct {
	LambdaDir    string
	Architecture string
	IsSync       bool
	Dryrun       bool
	CleanDir     bool
}

func (b *BuildLambdaHandler) Handle(c *cli.Context) error {
	start := time.Now()

	err := b.validateInput()
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"directory":      b.LambdaDir,
		"architecture":   b.Architecture,
		"isSynchronous":  b.IsSync,
		"dryrun":         b.Dryrun,
		"cleanDirectory": b.CleanDir,
	}).Info("Configurations")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10)*time.Minute)
	defer cancel()

	if b.CleanDir {
		err = b.cleanPreviousBuilds()
		if err != nil {
			return err
		}
	}

	if !b.IsSync {
		err := b.handleBuildParallel(ctx)
		if err != nil {
			return err
		}
	} else {
		err := b.handleBuildSync(ctx)
		if err != nil {
			return err
		}
	}

	log.Infof("Execution Complete! It took %v", time.Since(start))

	return nil
}

func (b *BuildLambdaHandler) validateInput() error {
	if str.Blank(b.Architecture) {
		return errors.New("You didn't enter architecture for Golang to compile")
	}
	return nil
}

func (b *BuildLambdaHandler) handleBuildParallel(ctx context.Context) error {
	log.Info("Building Lambdas (Parallel)")

	lambdas, err := filepath.Glob(fmt.Sprintf("%s/*", b.LambdaDir))
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)
	for i := 0; i < len(lambdas); i++ {
		lambdaPath := lambdas[i]
		_, lambdaName := path.Split(lambdaPath)
		wg.Add(1)
		go func() {
			err := b.execBuildSteps(lambdaPath, lambdaName)
			if err != nil {
				// fatalError channel will close on first error, this is for ease of auditing when we have multiple errors
				log.WithError(err).Info("Error executing build step, sending error to fatalError channel")
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

// For demo
func (b *BuildLambdaHandler) handleBuildSync(ctx context.Context) error {
	log.Info("Building Lambdas (Synchronous)")

	lambdas, err := filepath.Glob(fmt.Sprintf("%s/*", b.LambdaDir))
	if err != nil {
		return err
	}

	for i := 0; i < len(lambdas); i++ {
		lambdaPath := lambdas[i]
		_, lambdaName := path.Split(lambdaPath)
		err := b.execBuildSteps(lambdaPath, lambdaName)
		if err != nil {
			return err
		}
	}

	return nil
}

func (b *BuildLambdaHandler) cleanPreviousBuilds() error {
	log.Info("Cleaning Build Directory")
	err := os.RemoveAll(constants.BuildDir)
	if err != nil {
		return err
	}
	return nil
}

func (b *BuildLambdaHandler) execBuildSteps(lambdaPath, lambdaName string) error {
	log := log.WithField("lambdaName", lambdaName)

	if b.Dryrun {
		log.Info("Dryrun: skipping build lambda step")
	} else {
		log.Info("Real Run: building lambda")
		err := b.buildLambda(lambdaPath, lambdaName)
		if err != nil {
			return err
		}
	}

	if b.Dryrun {
		log.Info("Dryrun: skipping copy configs")
	} else {
		log.Info("Real Run: copying configs")
		err := b.copyConfigs(lambdaName)
		if err != nil {
			return err
		}
	}

	if b.Dryrun {
		log.Info("Dryrun: skipping zip file building for built lambda")
	} else {
		log.Info("Real Run: building zip file for built lambda")
		err := b.buildZipFile(lambdaName)
		if err != nil {
			return err
		}
	}

	if b.Dryrun {
		log.Info("Dryrun: skipping hash generation for the lambda")
	} else {
		log.Info("Real Run: generating hash for the lambda")
		err := b.generateHash(lambdaName)
		if err != nil {
			return err
		}
	}
	return nil
}

func (b *BuildLambdaHandler) buildLambda(lambdaPath, lambdaName string) error {
	var stderr bytes.Buffer
	src := fmt.Sprintf("%s/main.go", lambdaPath)
	bin := fmt.Sprintf("%s/%s/%s", constants.BuildDir, lambdaName, lambdaName)
	cmd := exec.Command("go", "build", "-o", bin, src)
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("GOOS=%s", b.Architecture),
	)
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		log.Info(stderr)
		return err
	}
	return nil
}

func (b *BuildLambdaHandler) copyConfigs(lambdaName string) error {
	buildConfigPath := fmt.Sprintf("%s/%s/etc/pachinko/config", constants.BuildDir, lambdaName)
	err := os.MkdirAll(buildConfigPath, 0755)
	if err != nil {
		return err
	}

	configs, err := filepath.Glob("./config/data/*")
	if err != nil {
		return err
	}
	for _, config := range configs {
		_, configName := path.Split(config)
		input, err := ioutil.ReadFile(config)
		if err != nil {
			return err
		}
		dest := fmt.Sprintf("%s/%s", buildConfigPath, configName)
		err = ioutil.WriteFile(dest, input, 0644)
		if err != nil {
			return err
		}
	}

	return nil
}

func (b *BuildLambdaHandler) buildZipFile(lambdaName string) error {
	var stderr bytes.Buffer
	src := lambdaName
	dest := fmt.Sprintf("%s.zip", lambdaName)
	cmd := exec.Command("zip", "-r", dest, src)
	cmd.Dir = constants.BuildDir
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		log.Info(stderr)
		return err
	}

	err = validateZipFile(lambdaName, dest)
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"lambdaName": lambdaName,
		"zipName":    dest,
	}).Info("Zipped built lambda and validation passed")

	return nil
}

// zip validation strategy: compare all files (excluding directories) from the source directory and zipped file
func validateZipFile(lambdaName, zipName string) error {
	srcDir := fmt.Sprintf("%s/%s", constants.BuildDir, lambdaName)
	zipDir := fmt.Sprintf("%s/%s", constants.BuildDir, zipName)

	srcContent, err := listSrcContent(srcDir)
	if err != nil {
		return err
	}

	zipContent, err := listZipContent(zipDir)
	if err != nil {
		return err
	}

	zipContentSet := make(map[string]struct{})
	for _, zipFile := range zipContent {
		zipContentSet[zipFile] = struct{}{}
	}

	if len(srcContent) != len(zipContentSet) {
		return errors.New("file counts from zipfile and src directory are different")
	}

	for _, srcFile := range srcContent {
		if _, ok := zipContentSet[srcFile]; !ok {
			msg := fmt.Sprintf("source file %s was not found in zipped package", srcFile)
			return errors.New(msg)
		}
	}

	return nil
}

func listSrcContent(srcDir string) ([]string, error) {
	var fileList []string

	err := filepath.Walk(srcDir, func(path string, info os.FileInfo, err error) error {
		if path != srcDir && !info.IsDir() {
			buildDirCorrected := strings.Replace(constants.BuildDir, "./", "", 1) + "/"
			pathTruncated := strings.Replace(path, buildDirCorrected, "", 1)
			fileList = append(fileList, pathTruncated)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return fileList, nil
}

func listZipContent(zipfile string) ([]string, error) {
	zipread, err := zip.OpenReader(zipfile)
	if err != nil {
		return nil, err
	}
	defer zipread.Close()

	var fileList []string
	for _, f := range zipread.File {
		if !(f.FileInfo().IsDir()) {
			fileList = append(fileList, f.Name)
		}
	}

	return fileList, nil
}

func (b *BuildLambdaHandler) generateHash(lambdaName string) error {
	var stdout bytes.Buffer
	var err error
	zipFile := fmt.Sprintf("%s/%s.zip", constants.BuildDir, lambdaName)
	hashFile := fmt.Sprintf("%s/%s.hash", constants.BuildDir, lambdaName)
	digestBinCmd := exec.Command("openssl", "dgst", "-sha256", "-binary", zipFile)
	base64EncCmd := exec.Command("openssl", "enc", "-base64")

	r, w := io.Pipe()
	digestBinCmd.Stdout = w
	base64EncCmd.Stdin = r
	base64EncCmd.Stdout = &stdout

	err = digestBinCmd.Start()
	if err != nil {
		return err
	}
	err = base64EncCmd.Start()
	if err != nil {
		return err
	}
	err = digestBinCmd.Wait()
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	err = base64EncCmd.Wait()
	if err != nil {
		return err
	}

	computedHash := stdout.String()
	err = validateHash(computedHash)
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"lambdaName": lambdaName,
		"hash":       computedHash,
	}).Info("Computed hash for the built lambda and validated size check")

	err = ioutil.WriteFile(hashFile, stdout.Bytes(), 0644)
	if err != nil {
		return err
	}

	return nil
}

// hash validation strategy: we can only check for length since the hash changes on every run
func validateHash(hash string) error {
	if len(hash) != constants.ExpectedHashSize {
		msg := fmt.Sprintf("built hash has a different hash size (%d) than expected (%d)", len(hash), constants.ExpectedHashSize)
		return errors.New(msg)
	}
	return nil
}
