package deploy_lambdas

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"path"
	"path/filepath"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/constants"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/codegangsta/cli"
)

type LambdaPayload struct {
	IsSmokeTest bool `json:"is_smoke_test"`
}

type DeployLambdaHandler struct {
	Environment  string
	LambdaDir    string
	BucketSuffix string
	Version      string
	IsSync       bool
	Dryrun       bool
	LambdaClient *lambda.Lambda
}

func (d *DeployLambdaHandler) Handle(c *cli.Context) error {
	start := time.Now()

	err := d.validateInput()
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"environment":   d.Environment,
		"directory":     d.LambdaDir,
		"bucketSuffix":  d.BucketSuffix,
		"version":       d.Version,
		"isSynchronous": d.IsSync,
		"dryrun":        d.Dryrun,
	}).Info("Configurations")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10)*time.Minute)
	defer cancel()

	log.Info("Initializing Lambda Client...")
	cfg, err := config.LoadConfig(config.Environment(d.Environment))
	if err != nil || cfg == nil {
		msg := "Error loading config"
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}
	sess, err := session.NewSession()
	if err != nil {
		return err
	}
	d.LambdaClient = lambda.New(sess, &aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})

	if !d.IsSync {
		err := d.handleDeployParallel(ctx)
		if err != nil {
			return err
		}
	} else {
		err := d.handleDeploySync(ctx)
		if err != nil {
			return err
		}
	}

	log.Infof("Execution Complete! It took %v", time.Since(start))

	return nil
}

func (d *DeployLambdaHandler) validateInput() error {
	if strings.Blank(d.BucketSuffix) {
		return errors.New("You didn't enter S3 bucket suffix")
	}
	if strings.Blank(d.Version) {
		return errors.New("You didn't enter the version for the built lambda")
	}
	return nil
}

func (d *DeployLambdaHandler) handleDeployParallel(ctx context.Context) error {
	log.Info("Deploying Lambdas (Parallel)")

	lambdas, err := filepath.Glob(fmt.Sprintf("%s/*", d.LambdaDir))
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)
	for i := 0; i < len(lambdas); i++ {
		lambda := lambdas[i]
		_, lambdaName := path.Split(lambda)
		wg.Add(1)
		go func() {
			err := d.execDeployStepsParallel(lambdaName)
			if err != nil {
				// fatalError channel will close on first error, this is for ease of auditing when we have multiple errors
				log.WithError(err).Info("Error deploying, sending error to fatalError channel")
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

// For demo
func (d *DeployLambdaHandler) handleDeploySync(ctx context.Context) error {
	log.Info("Deploying Lambdas (Synchronous)")

	lambdas, err := filepath.Glob(fmt.Sprintf("%s/*", d.LambdaDir))
	if err != nil {
		return err
	}

	for i := 0; i < len(lambdas); i++ {
		lambda := lambdas[i]
		_, lambdaName := path.Split(lambda)
		err := d.execDeployStepsSync(lambdaName)
		if err != nil {
			return err
		}
	}

	return nil
}

func (d *DeployLambdaHandler) execDeployStepsParallel(lambdaName string) error {
	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)

	for i := 0; i < len(constants.Tenants); i++ {
		tenant := constants.Tenants[i]
		wg.Add(1)
		go func() {
			err := d.deployLambda(lambdaName, tenant)
			if err != nil {
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

func (d *DeployLambdaHandler) execDeployStepsSync(lambdaName string) error {
	for i := 0; i < len(constants.Tenants); i++ {
		tenant := constants.Tenants[i]
		err := d.deployLambda(lambdaName, tenant)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *DeployLambdaHandler) deployLambda(lambdaName, tenant string) error {
	s3Bucket := fmt.Sprintf("%s%s", tenant, d.BucketSuffix)
	s3Key := fmt.Sprintf("%s/%s/%s.zip", lambdaName, d.Version, lambdaName)
	functionName := fmt.Sprintf("%s-%s-%s", tenant, d.Environment, lambdaName)

	log := log.WithFields(log.Fields{
		"s3Bucket":     s3Bucket,
		"s3Key":        s3Key,
		"functionName": functionName,
		"tenant":       tenant,
	})
	if d.Dryrun {
		log.Info("Dryrun: skipping lambda deploy")
	} else {
		// UpdateFunctionCodeInput has a Dryrun param, but I think it's safer to just not call it on Dryrun
		log.Info("Real Run: deploying lambda")
		_, err := d.LambdaClient.UpdateFunctionCode(&lambda.UpdateFunctionCodeInput{
			FunctionName: aws.String(functionName),
			S3Bucket:     aws.String(s3Bucket),
			S3Key:        aws.String(s3Key),
			Publish:      aws.Bool(true),
		})
		if err != nil {
			return err
		}

		payload := &LambdaPayload{
			IsSmokeTest: true,
		}
		payloadBytes, err := json.Marshal(payload)
		if err != nil {
			return err
		}

		log.Info("Real Run: invoking lambda smoke test")
		result, err := d.LambdaClient.Invoke(&lambda.InvokeInput{
			FunctionName: aws.String(functionName),
			Payload:      payloadBytes,
		})

		if err != nil {
			return err
		}

		if result.FunctionError != nil {
			return errors.New(*result.FunctionError)
		}
	}
	return nil
}
