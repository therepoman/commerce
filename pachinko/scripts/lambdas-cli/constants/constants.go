package constants

const BuildDir = "./builds"
const ExpectedHashSize = 45

var Tenants = [...]string{
	"cp",
	"sticker",
	"bits",
	"st",
	"wallet",
	"freesubs",
	"bb",
}
