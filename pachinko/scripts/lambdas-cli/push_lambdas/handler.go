package push_lambdas

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/constants"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/codegangsta/cli"
)

type PushLambdaHandler struct {
	Environment  string
	LambdaDir    string
	BucketSuffix string
	Version      string
	IsSync       bool
	Dryrun       bool
	S3Client     *s3.S3
}

func (p *PushLambdaHandler) Handle(c *cli.Context) error {
	start := time.Now()

	err := p.validateInput()
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"environment":   p.Environment,
		"directory":     p.LambdaDir,
		"bucketSuffix":  p.BucketSuffix,
		"version":       p.Version,
		"isSynchronous": p.IsSync,
		"dryrun":        p.Dryrun,
	}).Info("Configurations")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10)*time.Minute)
	defer cancel()

	log.Info("Initializing S3 Upload Client...")
	cfg, err := config.LoadConfig(config.Environment(p.Environment))
	if err != nil || cfg == nil {
		msg := "Error loading config"
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}
	sess, err := session.NewSession()
	if err != nil {
		return err
	}
	p.S3Client = s3.New(sess, &aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})

	if !p.IsSync {
		err := p.handlePushParallel(ctx)
		if err != nil {
			return err
		}
	} else {
		err := p.handlePushSync(ctx)
		if err != nil {
			return err
		}
	}

	log.Infof("Execution Complete! It took %v", time.Since(start))

	return nil
}

func (p *PushLambdaHandler) validateInput() error {
	if strings.Blank(p.BucketSuffix) {
		return errors.New("You didn't enter S3 bucket suffix")
	}
	if strings.Blank(p.Version) {
		return errors.New("You didn't enter the version for the built lambda")
	}
	return nil
}

func (p *PushLambdaHandler) handlePushParallel(ctx context.Context) error {
	log.Info("Pushing Lambdas (Parallel)")

	lambdas, err := filepath.Glob(fmt.Sprintf("%s/*", p.LambdaDir))
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)
	for i := 0; i < len(lambdas); i++ {
		lambda := lambdas[i]
		_, lambdaName := path.Split(lambda)
		wg.Add(1)
		go func() {
			err := p.execPushStepsParallel(lambdaName)
			if err != nil {
				// fatalError channel will close on first error, this is for ease of auditing when we have multiple errors
				log.WithError(err).Info("Error executing push step, sending error to fatalError channel")
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

// For demo
func (p *PushLambdaHandler) handlePushSync(ctx context.Context) error {
	log.Info("Pushing Lambdas (Synchronous)")

	lambdas, err := filepath.Glob(fmt.Sprintf("%s/*", p.LambdaDir))
	if err != nil {
		return err
	}

	for i := 0; i < len(lambdas); i++ {
		lambda := lambdas[i]
		_, lambdaName := path.Split(lambda)
		err := p.execPushStepsSync(lambdaName)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *PushLambdaHandler) execPushStepsParallel(lambdaName string) error {
	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)

	for i := 0; i < len(constants.Tenants); i++ {
		tenant := constants.Tenants[i]
		wg.Add(1)
		go func() {
			err := p.pushLambda(lambdaName, tenant)
			if err != nil {
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

func (p *PushLambdaHandler) execPushStepsSync(lambdaName string) error {
	for i := 0; i < len(constants.Tenants); i++ {
		tenant := constants.Tenants[i]
		err := p.pushLambda(lambdaName, tenant)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *PushLambdaHandler) pushLambda(lambdaName, tenant string) error {
	err := p.pushLambdaZip(lambdaName, tenant)
	if err != nil {
		return err
	}

	err = p.pushLambdaHash(lambdaName, tenant)
	if err != nil {
		return err
	}

	return nil
}

func (p *PushLambdaHandler) pushLambdaZip(lambdaName, tenant string) error {
	s3Bucket := fmt.Sprintf("%s%s", tenant, p.BucketSuffix)
	s3Key := fmt.Sprintf("%s/%s/%s.zip", lambdaName, p.Version, lambdaName)
	srcPath := fmt.Sprintf("%s/%s.zip", constants.BuildDir, lambdaName)

	file, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer file.Close()

	fileInfo, _ := file.Stat()
	var fileSize = fileInfo.Size()
	buffer := make([]byte, fileSize)
	_, err = file.Read(buffer)
	if err != nil {
		return err
	}

	log := log.WithFields(log.Fields{
		"s3Bucket": s3Bucket,
		"s3Key":    s3Key,
		"zipPath":  srcPath,
		"tenant":   tenant,
	})
	if p.Dryrun {
		log.Info("Dryrun: skipping pushing lambda zip")
	} else {
		log.Info("Real Run: pushing lambda zip")
		_, err := p.S3Client.PutObject(&s3.PutObjectInput{
			Bucket:        aws.String(s3Bucket),
			Key:           aws.String(s3Key),
			Body:          bytes.NewReader(buffer),
			ContentLength: aws.Int64(fileSize),
			ContentType:   aws.String(http.DetectContentType(buffer)),
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *PushLambdaHandler) pushLambdaHash(lambdaName, tenant string) error {
	s3Bucket := fmt.Sprintf("%s%s", tenant, p.BucketSuffix)
	s3Key := fmt.Sprintf("%s/%s/hash", lambdaName, p.Version)
	srcPath := fmt.Sprintf("%s/%s.hash", constants.BuildDir, lambdaName)

	file, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer file.Close()

	fileInfo, _ := file.Stat()
	var fileSize = fileInfo.Size()
	buffer := make([]byte, fileSize)
	_, err = file.Read(buffer)
	if err != nil {
		return err
	}

	log := log.WithFields(log.Fields{
		"s3Bucket": s3Bucket,
		"s3Key":    s3Key,
		"hashPath": srcPath,
		"tenant":   tenant,
	})
	if p.Dryrun {
		log.Info("Dryrun: skipping pushing lambda hash")
	} else {
		log.Info("Real Run: pushing lambda hash")
		_, err := p.S3Client.PutObject(&s3.PutObjectInput{
			Bucket:        aws.String(s3Bucket),
			Key:           aws.String(s3Key),
			Body:          bytes.NewReader(buffer),
			ContentLength: aws.Int64(fileSize),
			ContentType:   aws.String("text/plain"),
		})
		if err != nil {
			return err
		}
	}
	return nil
}
