package clean_old_versions

import (
	"context"
	"errors"
	"strconv"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/codegangsta/cli"
)

const (
	versionsToKeep = 5
)

type CleanOldLambdaVersionsHandler struct {
	Environment  string
	IsSync       bool
	Dryrun       bool
	LambdaClient *lambda.Lambda
}

type LambdaFunctionInfo struct {
	Name string
	Arn  string
}

type LambdaFunctionVersionInfo struct {
	Arn           string
	Version       string
	VersionNumber int64
}

func (h *CleanOldLambdaVersionsHandler) Handle(c *cli.Context) error {
	start := time.Now()

	err := h.validateInput()
	if err != nil {
		return err
	}

	logrus.WithFields(logrus.Fields{
		"environment":   h.Environment,
		"isSynchronous": h.IsSync,
		"dryrun":        h.Dryrun,
	}).Info("Configurations")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10)*time.Minute)
	defer cancel()

	logrus.Info("Initializing Lambda Client...")
	cfg, err := config.LoadConfig(config.Environment(h.Environment))
	if err != nil || cfg == nil {
		msg := "Error loading config"
		logrus.WithError(err).Error(msg)
		return errors.New(msg)
	}
	sess, err := session.NewSession()
	if err != nil {
		return err
	}
	h.LambdaClient = lambda.New(sess, &aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})

	if !h.IsSync {
		err := h.handleCleaningParallel(ctx)
		if err != nil {
			return err
		}
	} else {
		err := h.handleCleaningSync(ctx)
		if err != nil {
			return err
		}
	}

	logrus.Infof("Execution Complete! It took %v", time.Since(start))

	return nil
}

func (h *CleanOldLambdaVersionsHandler) validateInput() error {
	return nil
}

func (h *CleanOldLambdaVersionsHandler) handleCleaningParallel(ctx context.Context) error {
	logrus.Info("Cleaning Lambdas (Parallel)")

	lambdas, err := h.fetchLambdas()
	if err != nil {
		return err
	}

	if lambdas == nil {
		logrus.Info("no lambdas to clean, quitting")
		return nil
	}

	logrus.WithField("numLambdas", len(lambdas)).Info("number of lambdas in list call")

	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)
	for i := 0; i < len(lambdas); i++ {
		l := lambdas[i]
		if l == nil {
			logrus.Info("found nil lambda function entry in list, continuing")
			continue
		}

		wg.Add(1)
		go func() {
			err := h.execCleaningStepsParallel(LambdaFunctionInfo{
				Name: pointers.StringOrDefault(l.FunctionName, ""),
				Arn:  pointers.StringOrDefault(l.FunctionArn, ""),
			})
			if err != nil {
				// fatalError channel will close on first error, this is for ease of auditing when we have multiple errors
				logrus.WithError(err).
					WithField("functionName", pointers.StringOrDefault(l.FunctionName, "")).
					Info("Error cleaning, sending error to fatalError channel")
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

// For demo
func (h *CleanOldLambdaVersionsHandler) handleCleaningSync(ctx context.Context) error {
	logrus.Info("Cleaning Lambdas (Synchronous)")

	lambdas, err := h.fetchLambdas()
	if err != nil {
		return err
	}

	if lambdas == nil {
		logrus.Info("no lambdas to clean, quitting")
		return nil
	}

	logrus.WithField("numLambdas", len(lambdas)).Info("number of lambdas in list call")

	for i := 0; i < len(lambdas); i++ {
		l := lambdas[i]
		if l == nil {
			logrus.Info("found nil lambda function entry in list, continuing")
			continue
		}

		err := h.execCleaningStepsSync(LambdaFunctionInfo{
			Name: pointers.StringOrDefault(l.FunctionName, ""),
			Arn:  pointers.StringOrDefault(l.FunctionArn, ""),
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (h *CleanOldLambdaVersionsHandler) execCleaningStepsParallel(lambdaInfo LambdaFunctionInfo) error {
	var wg sync.WaitGroup
	fatalError := make(chan error)
	wgDone := make(chan bool)

	functionVersions, err := h.fetchFunctionVersions(lambdaInfo.Arn)
	if err != nil {
		return err
	}
	if functionVersions == nil {
		logrus.WithField("function", lambdaInfo.Name).Info("no function versions found, returning")
		return nil
	}

	// limit to keep includes $LATEST
	if len(functionVersions) <= versionsToKeep+1 {
		logrus.WithField("function", lambdaInfo.Name).Info("function versions within limit, returning")
		return nil
	}

	logrus.WithField("numLambdas", len(functionVersions)).Info("number of lambda versions in list call")

	versionInfo, topVersionNumber := cleanVersions(functionVersions)

	for i := 0; i < len(versionInfo); i++ {
		v := versionInfo[i]

		// the $LATEST tag we keep
		if v.Version == "$LATEST" {
			continue
		}
		// keep any version above the limit
		if v.VersionNumber > topVersionNumber-versionsToKeep {
			continue
		}

		wg.Add(1)
		go func() {
			err := h.cleanVersion(v.Arn)
			if err != nil {
				fatalError <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()
	select {
	case err := <-fatalError:
		return err
	case <-wgDone:
		break
	}
	return nil
}

func (h *CleanOldLambdaVersionsHandler) execCleaningStepsSync(lambdaInfo LambdaFunctionInfo) error {
	functionVersions, err := h.fetchFunctionVersions(lambdaInfo.Arn)
	if err != nil {
		return err
	}
	if functionVersions == nil {
		logrus.WithField("function", lambdaInfo.Name).Info("no function versions found, returning")
		return nil
	}

	// limit to keep includes $LATEST
	if len(functionVersions) <= versionsToKeep+1 {
		logrus.WithField("function", lambdaInfo.Name).Info("function versions within limit, returning")
		return nil
	}

	logrus.WithField("numLambdas", len(functionVersions)).Info("number of lambda versions in list call")

	versionInfo, topVersionNumber := cleanVersions(functionVersions)

	for i := 0; i < len(versionInfo); i++ {
		v := versionInfo[i]

		// the $LATEST tag we keep
		if v.Version == "$LATEST" {
			continue
		}
		// keep any version above the limit
		if v.VersionNumber > topVersionNumber-versionsToKeep {
			continue
		}

		err := h.cleanVersion(v.Arn)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *CleanOldLambdaVersionsHandler) cleanVersion(versionArn string) error {
	log := logrus.WithFields(logrus.Fields{
		"versionArn": versionArn,
	})
	if h.Dryrun {
		log.Info("Dryrun: skipping cleaning of lambda version")
	} else {
		// UpdateFunctionCodeInput has a Dryrun param, but I think it's safer to just not call it on Dryrun
		log.Info("Real Run: cleaning up version of lambda")
		_, err := h.LambdaClient.DeleteFunction(&lambda.DeleteFunctionInput{
			FunctionName: aws.String(versionArn),
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *CleanOldLambdaVersionsHandler) fetchLambdas() ([]*lambda.FunctionConfiguration, error) {
	functions := make([]*lambda.FunctionConfiguration, 0)

	var marker *string
	for {
		input := &lambda.ListFunctionsInput{}
		if marker != nil {
			input.Marker = marker
		}
		lambdas, err := h.LambdaClient.ListFunctions(input)
		if err != nil {
			return nil, err
		}

		if lambdas == nil {
			logrus.Info("no lambdas in list call, returning")
			return functions, nil
		}

		functions = append(functions, lambdas.Functions...)
		if lambdas.NextMarker == nil {
			break
		}
		marker = lambdas.NextMarker
	}

	return functions, nil
}

func (h *CleanOldLambdaVersionsHandler) fetchFunctionVersions(functionVersion string) ([]*lambda.FunctionConfiguration, error) {
	versions := make([]*lambda.FunctionConfiguration, 0)

	var marker *string
	for {
		input := &lambda.ListVersionsByFunctionInput{
			FunctionName: aws.String(functionVersion),
		}
		if marker != nil {
			input.Marker = marker
		}
		lambdas, err := h.LambdaClient.ListVersionsByFunction(input)
		if err != nil {
			return nil, err
		}

		if lambdas == nil {
			logrus.Info("no lambda versions in list call, returning")
			return versions, nil
		}

		versions = append(versions, lambdas.Versions...)
		if lambdas.NextMarker == nil {
			break
		}
		marker = lambdas.NextMarker
	}

	return versions, nil
}

func cleanVersions(functionVersions []*lambda.FunctionConfiguration) ([]LambdaFunctionVersionInfo, int64) {
	topVersionNumber := int64(0)
	versionInfo := make([]LambdaFunctionVersionInfo, 0)
	for _, function := range functionVersions {
		if function == nil {
			logrus.Info("found nil lambda function version entry in list, continuing")
			continue
		}
		if strings.BlankP(function.Version) {
			logrus.Info("found nil lambda function version with no version, continuing")
			continue
		}

		version := *function.Version
		versionInfoEntry := LambdaFunctionVersionInfo{
			Arn:     pointers.StringOrDefault(function.FunctionArn, ""),
			Version: version,
		}

		if version != "$LATEST" {
			if !strings.IsInt64Parsable(version) {
				logrus.WithField("version", version).Info("found lambda function version that wasn't a number or latest, continuing")
				continue
			}
			versionNumber, err := strconv.ParseInt(version, 10, 64)
			if err != nil {
				logrus.WithField("version", version).Info("found lambda function version that wasn't parsable, continuing")
				continue
			}
			versionInfoEntry.VersionNumber = versionNumber
			if topVersionNumber < versionNumber {
				topVersionNumber = versionNumber
			}
		}

		versionInfo = append(versionInfo, versionInfoEntry)
	}

	return versionInfo, topVersionNumber
}
