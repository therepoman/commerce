package main

import (
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachinko/config"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/build_lambdas"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/clean_old_versions"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/deploy_lambdas"
	"code.justin.tv/commerce/pachinko/scripts/lambdas-cli/push_lambdas"
	"github.com/codegangsta/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "LambdasCLI"

	buildLambdaHandler := build_lambdas.BuildLambdaHandler{}
	pushLambdaHandler := push_lambdas.PushLambdaHandler{}
	deployLambdaHandler := deploy_lambdas.DeployLambdaHandler{}
	cleanOldVersionsLambdaHandler := clean_old_versions.CleanOldLambdaVersionsHandler{}

	app.Commands = []cli.Command{
		{
			Name:   "build",
			Usage:  "build the lambdas",
			Action: buildLambdaHandler.Handle,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "dir, directory",
					Value:       "lambdas/functions",
					Usage:       "directory where built lambdas are saved, defaults to 'lambdas/functions'",
					Destination: &buildLambdaHandler.LambdaDir,
				},
				cli.StringFlag{
					Name:        "arch, architecture",
					Usage:       "the GOOS for the build",
					Destination: &buildLambdaHandler.Architecture,
				},
				cli.BoolFlag{
					Name:        "isSync",
					Usage:       "pass in this flag to run the build commands synchronously",
					Destination: &buildLambdaHandler.IsSync,
				},
				cli.BoolFlag{
					Name:        "dryrun",
					Usage:       "Dryrun, outlines the steps without actually running them",
					Destination: &buildLambdaHandler.Dryrun,
				},
				cli.BoolFlag{
					Name:        "cleanDir",
					Usage:       "If supplied, will clean the build directory",
					Destination: &buildLambdaHandler.CleanDir,
				},
			},
		},
		{
			Name:   "push",
			Usage:  "push the lambdas to S3 buckets",
			Action: pushLambdaHandler.Handle,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "environment, e",
					Value:       "staging",
					Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
					Destination: &pushLambdaHandler.Environment,
					EnvVar:      config.EnvironmentEnvironmentVariable,
				},
				cli.StringFlag{
					Name:        "dir, directory",
					Value:       "lambdas/functions",
					Usage:       "directory where built lambdas are saved, defaults to 'lambdas/functions'",
					Destination: &pushLambdaHandler.LambdaDir,
				},
				cli.StringFlag{
					Name:        "bucketSuffix",
					Usage:       "the suffix of the S3 bucket",
					Destination: &pushLambdaHandler.BucketSuffix,
				},
				cli.StringFlag{
					Name:        "version",
					Usage:       "the version of the built lambda (could be anything, usually it's git commit or branch)",
					Destination: &pushLambdaHandler.Version,
				},
				cli.BoolFlag{
					Name:        "isSync",
					Usage:       "pass in this flag to run the build commands synchronously",
					Destination: &pushLambdaHandler.IsSync,
				},
				cli.BoolFlag{
					Name:        "dryrun",
					Usage:       "Dryrun, outlines the steps without actually running them",
					Destination: &pushLambdaHandler.Dryrun,
				},
			},
		},
		{
			Name:   "deploy",
			Usage:  "deploy the lambdas",
			Action: deployLambdaHandler.Handle,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "environment, e",
					Value:       "staging",
					Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
					Destination: &deployLambdaHandler.Environment,
					EnvVar:      config.EnvironmentEnvironmentVariable,
				},
				cli.StringFlag{
					Name:        "dir, directory",
					Value:       "lambdas/functions",
					Usage:       "directory where built lambdas are saved, defaults to 'lambdas/functions'",
					Destination: &deployLambdaHandler.LambdaDir,
				},
				cli.StringFlag{
					Name:        "bucketSuffix",
					Usage:       "the suffix of the S3 bucket",
					Destination: &deployLambdaHandler.BucketSuffix,
				},
				cli.StringFlag{
					Name:        "version",
					Usage:       "the version of the built lambda (could be anything, usually it's git commit or branch)",
					Destination: &deployLambdaHandler.Version,
				},
				cli.BoolFlag{
					Name:        "isSync",
					Usage:       "pass in this flag to run the build commands synchronously",
					Destination: &deployLambdaHandler.IsSync,
				},
				cli.BoolFlag{
					Name:        "dryrun",
					Usage:       "Dryrun, outlines the steps without actually running them",
					Destination: &deployLambdaHandler.Dryrun,
				},
			},
		},
		{
			Name:   "clean",
			Usage:  "clean old versions of the lambdas",
			Action: cleanOldVersionsLambdaHandler.Handle,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "environment, e",
					Value:       "staging",
					Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
					Destination: &cleanOldVersionsLambdaHandler.Environment,
					EnvVar:      config.EnvironmentEnvironmentVariable,
				},
				cli.BoolFlag{
					Name:        "isSync",
					Usage:       "pass in this flag to run the build commands synchronously",
					Destination: &cleanOldVersionsLambdaHandler.IsSync,
				},
				cli.BoolFlag{
					Name:        "dryrun",
					Usage:       "Dryrun, outlines the steps without actually running them",
					Destination: &cleanOldVersionsLambdaHandler.Dryrun,
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.WithError(err).Fatal(err)
	}
}
