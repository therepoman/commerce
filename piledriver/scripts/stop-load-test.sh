#!/bin/bash

. variables.sh

# Check that process is running before killing
if [[ $(ps wx | grep -E "attack -s2s2=.* ${LOADTEST_PLAN}" | grep -v grep ) ]]; then
    kill $(ps wx | grep -E "attack -s2s2=.* ${LOADTEST_PLAN}" | grep -v grep | cut -f 1 -d " ")
else
    exit 126
fi
