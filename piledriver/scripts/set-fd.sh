#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "Setting file descriptors on ${i}..."
    ssh -t "${USERNAME}@${i}" "echo '*  soft   nofile 80000' | sudo tee /etc/security/limits.conf"
    ssh -t "${USERNAME}@${i}" "echo '*  hard   nofile 80000' | sudo tee -a /etc/security/limits.conf"
done
