#!/bin/bash

# This file wraps scripts with the variables defined in variables.sh,
# along with some additional settings used by the local scripts.

. variables.sh

PILEDRIVER_ROOT=$(git rev-parse --show-toplevel)

shopt -s expand_aliases
alias ssh="ssh -J $BASTION"
alias scp="scp -o \"ProxyCommand ssh $BASTION -W %h:%p\""

. $PILEDRIVER_ROOT/scripts/$1.sh
