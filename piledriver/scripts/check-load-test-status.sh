#!/bin/bash

. variables.sh

# Check that process is running before killing
if [[ $(ps wx | grep -E "attack -s2s2=.* $LOADTEST_PLAN" | grep -v grep | cut -f 2 -d " ") ]]; then
    exit 0
else
    exit 1
fi
