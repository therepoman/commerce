#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    (echo "SCPing to ${i}..." \
    && scp $LOADTEST_PLAN "${USERNAME}@${i}:." \
    && scp variables.sh "${USERNAME}@${i}:." \
    && echo -e "\033[92mSuccessfully copied updated load test plan to instance: ${i} \033[0m") || echo -e "\033[31mFailed to copy updated load test plan to instance: ${i} \033[0m"
done
