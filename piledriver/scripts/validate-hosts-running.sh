#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

if (( ${#loadTestIPs[@]} )); then
  exit 0
else
  echo -e "\033[31mThere are no load tests hosts running\033[0m" && exit 1
fi
