#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "${i}"
done
