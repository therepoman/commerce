#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    (echo "SCPing to ${i}..." \
    && scp $LOADTEST_PLAN "${USERNAME}@${i}:." \
    && scp $PILEDRIVER_ROOT/n "${USERNAME}@${i}:attack" \
    && scp $PILEDRIVER_ROOT/scripts/run-load-test.sh "${USERNAME}@${i}:." \
    && scp $PILEDRIVER_ROOT/scripts/stop-load-test.sh "${USERNAME}@${i}:." \
    && scp $PILEDRIVER_ROOT/scripts/check-load-test-status.sh "${USERNAME}@${i}:." \
    && scp variables.sh "${USERNAME}@${i}:." \
    && echo -e "\033[92mSuccessfully copied all load test files to instance: ${i} \033[0m") || echo -e "\033[31mFailed to copy all load test files to instance: ${i} \033[0m"
done
