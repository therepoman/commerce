#!/bin/bash

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    (ssh -t "${USERNAME}@${i}" nohup ./run-load-test.sh && echo -e "\033[92mSuccessfully started load test on instance: ${i} \033[0m") || echo -e "\033[31mFailed to start load test on instance: ${i} \033[0m"
done
