#!/bin/bash

outputDir=${OUTPUT_DIR:-loadtest-results}
loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=${USERNAME}-load-testing" "Name=instance-state-name,Values=running" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

mkdir -p $outputDir

for i in "${loadTestIPs[@]}"
do
    (echo "Getting results from to ${i}..." \
    && scp "${USERNAME}@${i}:test.out" "${outputDir}/${i}.txt" \
    && echo -e "\033[92mSuccessfully copied load test results from instance: ${i} \033[0m") || echo -e "\033[31mFailed to load test results from instance: ${i} \033[0m"
done
