# PILEDRIVER
Ad Hoc Distributed Load Testing

![Zangeif Piledrives!](/images/zangief.gif?raw=true "Zangief Piledrives!")

## Why you would want to use this
* You want to make sure your service is operationally ready.
* The service you want to test is network isolated and you need a way to call it.
* You want to generate so much load that your laptop shouldn't be considered as an option to run it.

## Fetching Piledrive is Easy
I install this via `git clone` like so:
```bash
git clone git@git.xarth.tv:commerce/piledriver.git
```

## Things you'll need to know before we start PILEDRIVING
This repository uses [Nebbiolo](https://git.xarth.tv/commerce/nebbiolo/), which is a plan based load testing framework made by and for the commerce organization. You'll want to read up on how to use their TOML file plans to run load tests with Piledriver.

You will also need to have setup your SSH key on your box. This will allow your local machine to SSH into the instances created by Piledriver to start, stop, set file descriptors, and get results.

## Time to PILEDRIVE your Service!

You will need to run some terraform in the account you wish to load test. This will setup a launch template and an ASG attached to your VPC. You'll want to copy the example TFVars file and replace it with the correct values associated with your VPC.
 
### Terraform Setup
Piledriver requires Terraform 11. If primarily use Terraform 12, you can use [tfenv](https://github.com/tfutils/tfenv) to easily work with Terraform 11 on your machine. 
```bash
brew install tfenv
tfenv install 0.11.15
tfenv list 
tfenv use 0.11.15
```

### Authentication
Prior to running any commands, you'll need to make sure you have an active AWS credentials session. If you use `ada`, this can be accomplished using: 
```bash
mwinit
ada credentials update --account=<your account name here> --provider=isengard --role=<your account role>
```

### SSM Setup

If using an [SSM Bastion](https://docs.fulton.twitch.a2z.com/docs/ssm_bastion.html) instead of teleport bastion, you'll
need to take the following steps:
1. Set up a security group for your piledriver instance similar to [this commit](https://code.amazon.com/packages/TwitchDartAPICDK/commits/1be449c0e5bd926a962ce5114bdbfb9b5a1e5bad).
1. SSH into your bastion with `ssh -A ssm-jumpbox.$YOUR_ACCOUNT_ID`
1. Either set up Piledriver and run all the 'make' commands directly from your bastion instance, or copy the necessary files
manually to each Piledriver instance from your bastion and run the start script manually.

### Terraform terminal commands
```bash
cd terraform
cp piledriver.tfvars.example piledriver.tfvars
# Edit the TF Vars with the appropriate values
terraform init
terraform plan -var-file piledriver.tfvars -out loadtest.plan
terraform apply loadtest.plan
```

You will need to wait for terraform to finish creating your resources. This should not take but only a few minutes.

You will need to edit the file `variables.sh` that exists in the root directory of this repository. This will contain variables that specify the AWS profile, the Teleport Bastion Cluster, your username that is used to SSH into instances, as well as the loadtest plan file you wish to run.

If you think you need more file descriptors on your hosts, run the following command. It will increase your hard / soft limits to 80,000.
```bash
make set_file_descriptors
```
 Now you will want to make your load test plan. Create the load test plan file in this directoy. See the [Nebbiolo](https://git.xarth.tv/commerce/nebbiolo/) repository for example usages. Your traffic plan will be copied to every host, so if you desire a total of `X` TPS, the value configured in your `<plan-name>.toml` should be `X/Num Hosts`

First, prepare you load test hosts for testing. This step will only have to be run once. You should see that it is SCPing some files to all the hosts in your ASG.
```bash
make prep
```

You are ready to start load testing now! To start, simply run this command:
```bash
make start
```
You should see that it is starting the command to run the load test on each instance.

If you need to stop the load test from running, simply run:
```bash
make stop
```

When your loadtest is complete, simply run this command to fetch all your results from your ASG:
```bash
make get_results
```
This will place the output into `/loadtest-results/`. If you desire a specific output location, you can run: 
```bash
make get_results OUTPUT_DIR=<your directory here>
```

where you specify what directory to write the results.

If you need to run a different load test plan using the same hosts, run:
```bash
make change_plan
```

## You're done PILEDRIVING, now CLEAN UP

To remove all your resources you made while running Piledrive, simply run the following commands:

```bash
cd terraform
terraform destroy -var-file piledriver.tfvars
```

It should ask you to confirm that you want to destroy the resources you created, which you will just type `yes` and watch the resources disappear.
