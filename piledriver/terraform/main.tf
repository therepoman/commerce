module "piledriver" {
  source = "modules/piledriver"

  public_key_path = "${var.public_key_path}"
  security_group = "${var.security_group}"
  subnets = "${var.subnets}"
  username = "${var.username}"
  vpc_id = "${var.vpc_id}"

  min_hosts = "${var.min_hosts}"
  max_hosts = "${var.max_hosts}"
  instance_type = "${var.instance_type}"
  iam_instance_profile = "${var.iam_instance_profile}"
}

provider "aws" {
  region = "${var.region}"
  profile = "${var.aws_profile}"
}
