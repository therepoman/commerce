variable "min_hosts" {
  default = 1
  description = "the minimum number of load testing hosts"
}

variable "max_hosts" {
  default = 10
  description = "the maximum number of load testing hosts"
}

variable "vpc_id" {
  description = "the VPC you wish to run the load test in"
}

variable "subnets" {
  description = "the subnets of your VPC"
}

variable "security_group" {
  description = "the security group to access stuff in the VPC"
}

variable "public_key_path" {
  description = "the path to your public key file to load on the boxes"
}

variable "username" {
  description = "your LDAP username"
}

variable "instance_type" {
  default = "c5.xlarge"
  description = "the load test instances to run on"
}

variable "region" {
  default = "us-west-2"
  description = "AWS region to operate"
}

variable "aws_profile" {
  description = "AWS profile to operate on"
}

variable "iam_instance_profile" {
  default = ""
  description = "the IAM instance profile to attach to instances, if none is provided a new iam_instance supporting SSM management will be created"
}
