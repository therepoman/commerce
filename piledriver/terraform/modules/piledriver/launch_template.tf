data template_file "userdata" {
  template = "${file("${path.module}/userdata.tpl")}"
  vars {
    username = "${var.username}"
    public_key = "${file("${var.public_key_path}")}"
  }
}

resource "aws_launch_template" "main" {
  name = "${var.username}-load-testing"

  image_id = "ami-061392db613a6357b"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${var.security_group}"]
  iam_instance_profile {
    name = "${var.iam_instance_profile != "" ? var.iam_instance_profile : aws_iam_instance_profile.instance_profile.name}"
  }

  user_data = "${base64encode(data.template_file.userdata.rendered)}"
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "${var.username}-load-testing-profile"
  role = "${aws_iam_role.instance_profile_role.id}"
}

resource "aws_iam_role" "instance_profile_role" {
  name = "${var.username}-load-testing-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "role_policy" {
  name = "allow-lambda-access"
  role = "${aws_iam_role.instance_profile_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "execute-api:Invoke",
      "Effect": "Allow",
      "Resource": "arn:aws:execute-api:*:985585625942:*"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "ssm_policy" {
  role = "${aws_iam_role.instance_profile_role.id}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
