const path = require('path')
const fs = require('fs')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: {
      viewer: path.resolve(__dirname, './src/viewer/index.js'),
      config: path.resolve(__dirname, './src/config/index.js'),
      live_config: path.resolve(__dirname, './src/live_config/index.js')
    },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './dist')
  },
  module: {
      rules: [
          {
              test: /\.jsx?$/,
              exclude: /(node_modules|bower_components)/,
              use: {
                loader: 'babel-loader',
                options: {
                    presets: ['babel-preset-react', 'babel-preset-env'],
                    plugins: ['transform-object-rest-spread']
                }
              }
          }, {
              test: /\.css/,
              use: [
                  {
                      loader: 'style-loader'
                  }, {
                      loader: 'css-loader'
                  }
              ]
          }
      ]
  },
  plugins: [
      new CopyWebpackPlugin([
          {
              from: path.resolve(__dirname, './src/viewer/index.html'),
              to: path.resolve(__dirname, './dist/viewer.html')
          }, {
            from: path.resolve(__dirname, './src/config/index.html'),
            to: path.resolve(__dirname, './dist/config.html')
        }, {
            from: path.resolve(__dirname, './src/live_config/index.html'),
            to: path.resolve(__dirname, './dist/live_config.html')
        },
      ])
  ],
  devServer: {
      contentBase: path.resolve(__dirname, './dist'),
      https: {
          key: fs.readFileSync(path.resolve(__dirname, './certs/testing.key')),
          cert: fs.readFileSync(path.resolve(__dirname, './certs/testing.crt'))
      },
      port: 8081
  },
  devtool: 'inline-source-map'
};