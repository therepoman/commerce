import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { ACTION_TYPES } from './actions'

export const initialState = {
    authData: null,
    context: null,
    error: null,
    messages: {},
    followed: {}
}

function TwitchExt(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ON_AUTHORIZED:
            return {
                ...state,
                authData: action.data
            }
            break
        case ACTION_TYPES.ON_CONTEXT:
            return {
                ...state,
                context: action.data
            }
            break
        case ACTION_TYPES.ON_ERROR:
            return {
                ...state,
                error: action.data
            }
            break
        case ACTION_TYPES.ON_MESSAGE:
            const messageData = {
                ...store.messages
            }
            messageData[action.target] = action.messageData

            return {
                ...store,
                messages: {
                    ...messageData
                }
            }
            break
        case ACTION_TYPES.ON_FOLLOW:
            const followed = {}
            followed[data.channelName] = data.didFollow
            return {
                ...store,
                followed
            }
            break
    }
}

export default createStore(TwitchExt, applyMiddleware(thunkMiddleware))
