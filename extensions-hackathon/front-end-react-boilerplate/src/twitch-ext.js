import * as Actions from './actions'

let TwitchExt = null

if (window.Twitch && window.Twitch.ext) {
    TwitchExt = window.Twitch.ext;
}

export function onAuthorized(callback) {
    if (TwitchExt) {
        TwitchExt.onAuthorized(callback)
    }
}

export function onContext(callback) {
    if (TwitchExt) {
        TwitchExt.onContext(callback)
    }
}

export function onError(callback) {
    if (TwitchExt) {
        TwitchExt.onError(callback)
    }
}

export function send(target, contentType, message) {
    if (TwitchExt) {
        TwitchExt.send(target, contentType, message)
    }
}

export function listen(target, callback) {
    if (TwitchExt) {
        TwitchExt.listen(target, callback)
    }
}

export function unlisten(target, callback) {
    if (TwitchExt) {
        TwitchExt.unlisten(target, callback)
    }
}

function followChannel(channelName) {
    if (TwitchExt) {
        TwitchExt.actions.followChannel(channelName)
    }
}

function onFollow(callback) {
    if (TwitchExt) {
        TwitchExt.actions.onFollow(callback)
    }
}

function requestIdShare() {
    if (TwitchExt) {
        TwitchExt.actions.requestIdShare()
    }
}

export const actions = {
    followChannel,
    onFollow,
    requestIdShare
}
