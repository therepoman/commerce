# Pixels

## Install
First, install dependencies via
```
npm install
```

Then create the self signed certs

```
cd certs
./generate_local_ssl.sh
```

To start the dev server @https://localhost:8081 run
```
npm run dev
```

You can then see the `viewer` page at `https://localhost:8081/viewer.html`

The `Twitch.ext` global library is hooked up to the application via Redux.
In the example, the Redux state is being connected to the App component and then displayed on the page.

Additional actions are wired up for listening to pubsub messages, stop listening to them, sending pubsub
messages, and for following a channel.