import * as ReactDom from 'react-dom'
import * as React from 'react'
import { connect, Provider } from 'react-redux'
import * as Actions from '../actions'
import Store, { initialState } from '../store'
import { App } from './app.jsx'
import * as TwitchExt from '../twitch-ext'
import './index.css'

TwitchExt.onAuthorized((authData) => Store.dispatch(Actions.setAuthorization(authData)))
TwitchExt.onContext((contextData) => Store.dispatch(Actions.setContext(contextData)))
TwitchExt.onError((error) => Store.dispatch(Actions.setError(error)))

const mapStateToProps = (state) => {
    return {
        ...initialState,
        ...state
    }
}

const AppWithRedux = connect(mapStateToProps)(App)

ReactDom.render(
    <Provider store={Store}>
        <AppWithRedux />
    </Provider>,
    document.getElementById('app')
)
