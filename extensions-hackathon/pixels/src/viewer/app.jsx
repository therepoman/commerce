import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as Actions from '../actions'
import classnames from 'classnames';
import PixelGrid from './pixel-grid.jsx';
import Pixel from './pixel.jsx';
import ColorPicker from './color-picker.jsx';

const mapStateToProps = (state) => {
  return {
    pixelSize: state.pixelSize,
    selectedColor: state.selectedColor,
    configHidden: state.configHidden
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setPixelSize: pixelSize => dispatch(Actions.setPixelSize(pixelSize)),
    toggleConfig: () => dispatch(Actions.toggleConfig())
  }
}

class App extends Component {
  handlePixelSizeChange(event) {
    const { setPixelSize } = this.props;
    setPixelSize(event.target.value);
  }

  render() {
    const configClassNames = classnames(
      'config',
      { 'hidden': this.props.configHidden }
    );

    return (
      <div className="app">
        <span className="button button--config" onClick={this.props.toggleConfig}>
          ⚙️
        </span>
        <div className="pixel-grid--container">
          <PixelGrid />
        </div>
        <div className={configClassNames}>
          <ColorPicker />
          <div className="zoom">
            Zoom:
            <input
              type="number"
              defaultValue={this.props.pixelSize}
              onChange={this.handlePixelSizeChange.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
