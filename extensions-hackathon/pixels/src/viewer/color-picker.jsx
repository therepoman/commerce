import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as Actions from '../actions'

const mapStateToProps = (state) => {
  return {
    selectedColor: state.selectedColor
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setSelectedColor: color => dispatch(Actions.setSelectedColor(color))
  }
}

class ColorPicker extends Component {
  handleSelectedColorChange(event) {
    const { setSelectedColor } = this.props;
    setSelectedColor(event.target.value);
  }

  render() {
    return (
      <div className="color-picker">
        Color:
        <input
          type="text"
          className="color-picker__input"
          defaultValue={this.props.selectedColor}
          onChange={this.handleSelectedColorChange.bind(this)}
        />
        <svg width="20" height="20">
          <rect width="20" height="20" fill={this.props.selectedColor} />
        </svg>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ColorPicker);