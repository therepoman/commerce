import * as ReactDom from 'react-dom'
import * as React from 'react'
import { Provider } from 'react-redux'
import Store from '../store'
import App from './app.jsx'
import './index.css'

function generateColorGrid(rows=5, cols=5) {
  let colorGrid = {};
  for (let y = 0; y < rows; y++) {
      for (let x = 0; x < cols; x++) {
          const coordinate = `${x},${y}`;
          colorGrid[coordinate] = getRandomColor();
      }
  }
  return colorGrid;
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += _.sample(letters);
  }
  return color;
}

ReactDom.render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById('app')
)
