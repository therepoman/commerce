import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as Actions from '../actions'
import pixel from './pixel.jsx'
import _ from 'lodash'

const mapStateToProps = (state) => {
  return {
      colorGrid: state.colorGrid,
      pixelSize: state.pixelSize,
      selectedColor: state.selectedColor,
      rows: state.rows,
      cols: state.cols
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
      onPixelClick: pixelData => dispatch(Actions.onPixelClick(pixelData))
  }
}

class PixelGrid extends Component {
  getClickHandler(coordinate) {
    const { onPixelClick } = this.props;
    return () => onPixelClick({ coordinate: coordinate });
  }

  render() {
    const width = this.props.pixelSize * this.props.cols;
    const height = this.props.pixelSize * this.props.rows;

    const pixels = _.map(this.props.colorGrid, (color, coordinate) => {
      const [x, y] = coordinate.split(',');
      const onClick = this.getClickHandler(coordinate);
      return (
        <rect key={coordinate}
          x={x} y={y}
          width="1" height="1"
          fill={color}
          onClick={onClick}
        />
      )
    });

    return (
      <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg"
          className="pixel-grid" width={width} height={height}>
        <g transform={`scale(${this.props.pixelSize})`}>
          {pixels}
        </g>
      </svg>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PixelGrid);