import React, { Component } from 'react';
import _ from 'lodash';

class Pixel extends Component {
  render() {
    return (
      <rect className="pixel"
        x={this.props.x} y={this.props.y}
        width="1" height="1"
        fill={this.props.color}
        onClick={this.props.onClick}
      />
    );
  }
}

export default Pixel;