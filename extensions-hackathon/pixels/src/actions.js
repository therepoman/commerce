export const ACTION_TYPES = {
    ON_PIXEL_CLICK: 'onPixelClick',
    SET_PIXEL_SIZE: 'setPixelSize',
    SET_SELECTED_COLOR: 'setSelectedColor',
    TOGGLE_CONFIG: 'toggleConfig'
}

export const onPixelClick = pixelData => ({
    type: ACTION_TYPES.ON_PIXEL_CLICK,
    data: pixelData
})

export const setPixelSize = pixelSize => ({
    type: ACTION_TYPES.SET_PIXEL_SIZE,
    data: pixelSize
})

export const setSelectedColor = color => ({
    type: ACTION_TYPES.SET_SELECTED_COLOR,
    data: color
})

export const toggleConfig = () => ({
    type: ACTION_TYPES.TOGGLE_CONFIG
})