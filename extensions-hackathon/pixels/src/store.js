import { createStore, combineReducers } from 'redux'
import { ACTION_TYPES } from './actions'
import _ from 'lodash'

const ROWS = 300;
const COLS = 318;

export const initialState = {
    colorGrid: generateColorGrid(ROWS, COLS),
    rows: ROWS,
    cols: COLS,
    pixelSize: 1,
    selectedColor: 'black',
    configHidden: true
}

function generateColorGrid(rows=5, cols=5) {
    let colorGrid = {};
    for (let y = 0; y < rows; y++) {
        for (let x = 0; x < cols; x++) {
            const coordinate = `${x},${y}`;
            colorGrid[coordinate] = getRandomColor();
        }
    }
    return colorGrid;
}
  
  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += _.sample(letters);
    }
    return color;
}

function ColorGrid(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ON_PIXEL_CLICK:
            const key = action.data.coordinate;
            let newState = {
                ...state,
                colorGrid: {
                    ...state.colorGrid
                } 
            };
            newState.colorGrid[key] = state.selectedColor;
            return newState;
        case ACTION_TYPES.SET_PIXEL_SIZE:
            return {
                ...state,
                pixelSize: action.data
            }
        case ACTION_TYPES.SET_SELECTED_COLOR:
            return {
                ...state,
                selectedColor: action.data
            }
        case ACTION_TYPES.TOGGLE_CONFIG:
            return {
                ...state,
                configHidden: !state.configHidden
            }
        default:
            return state;
    }
}

export default createStore(ColorGrid)
