#!/usr/bin/env bash
echo "test\ntest\n" | openssl req -x509 -newkey rsa:4096 -keyout server-key.pem -out server-cert.pem -days 1000 -config config -subj "/C=US/ST=California/L=San Francisco/O=Twitch/OU=web/CN="`hostname`
