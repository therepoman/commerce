import boto3
import json
import logging
import os
from time import time

import messages
import broadcast

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def send():
    now = time();
    active_messages = [ message for message in messages.get_messages() if message['last_refresh'] + message['refresh_interval'] < now ]

    serialized_message, message_ids = select_messages(active_messages)

    if not message_ids:
        return message_ids

    broadcast.send_broadcast_message('application/json', serialized_message)

    messages.mark_messages_sent(message_ids)
    return message_ids

def select_messages(messages):
    now = time()

    sorted_messages = sorted(messages, key=lambda message: message.get('last_refresh', 0) + message.get('refresh_interval', 60) - now)

    message_payloads = []
    serialized_message = ''
    for message in sorted_messages:
        payload = {'message_id': message['id'], 'payload': message['payload']}
        payloads_to_serialize = list(message_payloads)
        payloads_to_serialize.append(payload)
        serialized = json.dumps({'messages': payloads_to_serialize})
        if len(serialized) <= 5 * 1024:
            message_payloads.append(payload)
            serialized_message = serialized
        else:
            break

    message_ids = [ message['message_id'] for message in message_payloads ]
    return serialized_message, message_ids
