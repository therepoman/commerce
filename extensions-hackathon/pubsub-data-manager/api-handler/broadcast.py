import base64
import json
import jwt
import logging
import httplib
from time import time

import handler_config

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def send_broadcast_message(content_type, payload):
    if len(payload) > 5 * 1024:
        raise RuntimeError('The payload is too large: {0}'.format(len(payload)))

    config = handler_config.get_config()

    now = time()
    last_broadcast_time = float(config.get('last_broadcast_time', '0'))
    if now - last_broadcast_time < 0.8:
        raise RuntimeError('Request throttled')
    handler_config.put_config('last_broadcast_time', str(now))

    jwt_payload = {
        "exp": int(now + 300),
        "user_id": config['broadcaster_id'],
        "role": "external",
        "channel_id": config['broadcaster_id'],
        "pubsub_perms": {
            "send":["*"]
        }
    }

    jwt_secret = base64.b64decode(config['extension_secret'])
    encoded = jwt.encode(jwt_payload, jwt_secret, algorithm='HS256')

    broadcast_payload = {
        "content_type": payload,
        "message": payload,
        "targets": ["broadcast"]
    }

    headers = {
        'Authorization': 'Bearer {0}'.format(encoded),
        'Client-Id': config['extension_client_id'],
        'Content-Type': 'application/json',
    }

    conn = httplib.HTTPSConnection('api.twitch.tv')
    try:
        conn.request('POST', '/extensions/message/{0}'.format(config['broadcaster_id']), json.dumps(broadcast_payload), headers)
        response = conn.getresponse()
        if response.status < 200 or response.status >= 300:
            logger.info('Broadcast response: %s %s', response.status, response.reason)
            logger.info('Broadcast response body: %s', response.read())
            raise RuntimeError('Broadcast response status: {0}'.format(response.status))
    finally:
        conn.close()
