from __future__ import print_function

import boto3
import os
import messages
import message_batch_sender

def handler(event, context):
    print(event)
    operation = event.get('operation')
    if operation == 'ping':
        return {}
    elif operation == 'create_message':
        body = event.get('body', {})
        messages.put_message(body['message_id'], body['payload'], body['retention_seconds'], body['refresh_interval_seconds'])
        return {}
    elif operation == 'get_messages':
        active_messages = messages.get_messages()
        return {'messages': active_messages}
    elif operation == 'send_batch':
        message_ids = message_batch_sender.send()
        return {'message_ids_sent': message_ids}
    return {'errorMessage': 'Unknown request type'}
