import boto3
import logging
import os
from time import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def get_messages():
    select_statement = 'SELECT payload, expiration, refresh_interval, last_refresh FROM {0}'.format(quote_identifier(get_messages_db()))
    logger.debug('Running: %s', select_statement)
    response = get_client().select(SelectExpression=select_statement, ConsistentRead=True)
    logger.info('Messages: %s', response)

    now = time();
    active_messages = []
    expired_messages = []
    for item in response.get('Items', []):
        attributes = { attribute.get('Name', ''): attribute.get('Value', '') for attribute in item.get('Attributes', []) }
        message = {
            'id': item.get('Name'),
            'payload': attributes.get('payload'),
            'expiration': int(attributes.get('expiration', '0')),
            'refresh_interval': int(attributes.get('refresh_interval', '60')),
            'last_refresh': int(attributes.get('last_refresh', '0')),
        }
        if message['expiration'] < now:
            expired_messages.append(message)
        else:
            active_messages.append(message)

    if expired_messages:
        if len(expired_messages) > 25:
            expired_messages = expired_messages[0:25]
        delete_messages = [ {'Name': message['id']} for message in expired_messages ]
        get_client().batch_delete_attributes(DomainName=get_messages_db(), Items=delete_messages)
        logger.info('Deleted %s messages: %s', len(delete_messages), delete_messages)

    return active_messages

def put_message(message_id, payload, ttl_seconds, refresh_interval_seconds):
    get_client().put_attributes(
        DomainName=get_messages_db(),
        ItemName=message_id,
        Attributes=[
            {'Name': 'payload', 'Value': payload,'Replace': True},
            {'Name': 'expiration', 'Value': str(int(time() + int(ttl_seconds))), 'Replace': True},
            {'Name': 'refresh_interval', 'Value': str(int(refresh_interval_seconds)), 'Replace': True},
            {'Name': 'last_refresh', 'Value': '0', 'Replace': True},
        ]
    )
    logger.info('Stored message "%s" with ttl %s', message_id, ttl_seconds)

def mark_messages_sent(messages):
    messages_by_id = { message['id']: message for message in get_messages() }
    message_ids_to_update = [ id for id in messages if id in messages_by_id ]

    now_str = str(int(time()))
    for batch in [ message_ids_to_update[index:index+25] for index in xrange(0, len(message_ids_to_update), 25) ]:
        items = []
        for id in batch:
            items.append({
                'Name': id,
                'Attributes': [
                    {'Name': 'last_refresh', 'Value': now_str, 'Replace': True}
                ]
            })
        get_client().batch_put_attributes(DomainName=get_messages_db(), Items=items)
        logger.info('Updated send time on messages: %s', batch)

def get_client():
    return boto3.client('sdb')

def get_messages_db():
    return os.environ['MessagesDB']

def quote_identifier(id):
    return '`' + id.replace('`', '``') + '`'
