import boto3
import logging
import os
from time import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def get_config():
    select_statement = 'SELECT value FROM {0}'.format(quote_identifier(get_config_db()))
    logger.debug('Running: %s', select_statement)
    response = get_sdb_client().select(SelectExpression=select_statement, ConsistentRead=False)

    config = {}
    for item in response.get('Items'):
        attributes = { attribute.get('Name', ''): attribute.get('Value', '') for attribute in item.get('Attributes', []) }
        config[item.get('Name')] = attributes.get('value')

    return config

def put_config(name, value):
    get_sdb_client().put_attributes(
        DomainName=get_config_db(),
        ItemName=name,
        Attributes=[
            {'Name': 'value', 'Value': value, 'Replace': True},
        ]
    )

def get_sdb_client():
    return boto3.client('sdb')

def get_config_db():
    return os.environ['ConfigDB']

def quote_identifier(id):
    return '`' + id.replace('`', '``') + '`'
