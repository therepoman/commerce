# Extension networking background #

There's two main ways to get data into a viewer extension so that it has something to display.

### Backend service ###
Pro
* Fast load time.  The extension can fetch data as soon as it finishes loading.
* Simpler.  Services are common and the backend service gives the viewer extension data when it wants it.

Con
* Costs money.  API Gateway costs $3.50 per million API calls received plus transfer/Lambda/database/etc.  It's easy enough to reach that, for example one call per viewer per minute would equal minutes watched.  Raids would have a dollar cost associated with them after the free tier.
* Would small broadcasters fit in the free tier?  The goal is to be engaging and get a lot of viewership, which could be more than one call per minute and easily will be more than the AWS free tier.
* Service scaling, maintenance, security patching, etc.

### PubSub ###
Pro
* Free.
* The source doesn't handle the distribution, so it can be sent from a home network with no cloud infrastructure.

Con
* Slow load time.  After loading, the extension waits for the information to be re-broadcast to all viewers.
* More complicated due to periodically re-sending information and staying within the limits of 1 message per second and 5kb per message.  I don't have data on PubSub usage for real extensions.
* Delivery is not currently guaranteed, though this doesn't matter much since messages generally need to be re-broadcast anyways so that joining viewers get it.

### Data lifetime ###
Different types of data have different lifespans:
* Short lived data that is not worth re-broadcasting, such as the current rankings of players in a match or events such as which team has the ball/flag.
* Medium term data that is likely to be valid for 5 or 10 minutes, such as the list of current players in a match.
* Long term data that likely won't change for the duration of the stream such as a message of the day.

# PubSub data manager demo #

### Purpose ###
A low cost and low effort way to keep data in sync between the broadcaster and viewer extensions.  The broadcaster dashboard and tools maintain state in a small database that gets automatically replicated to all viewers.

* The caller (or whatever tools the broadcaster is using) can collect data when it makes sense to do so and not worry about having to ensure that it reliably gets sent to current viewers or new viewers that join later in the stream.
  * Short term data can be stored as it happens without worrying about the one message per second limit.
  * Medium term data can be stored once at the start of a match and viewers that join halfway through the match will get the data.
  * Similarly, long term data can be stored once at stream start with a long ttl and no further upkeep is needed.
* The caller can prioritize data
  * Data that is displayed at all times on an overlay can be configured to be included in every message to ensure that it shows up quickly after a viewer joins.
  * A large block of data that is interesting but rarely changes and not all viewers will click the button to reveal it can be configured re-broadcast infrequently.
* A framework layer that ensures compliance with the terms of service, 1 message per second and 5kb payloads.

### Architecture ###
![Diagram](https://diagrams.amazon.com/render?grid=%2B-----------%2B+++++%2B-----------%2B+++++%2B---------%2B%0D%0A%7CBroadcaster%2B----%3E%7CAPI+Gateway%2B----%3E%7CSimple+DB%7C%0D%0A%7C+Front+End+%7C+++++%2B--------%2B--%2B+++++%2B---------%2B%0D%0A%2B-----------%2B++++++%5E+++++++%7C%0D%0A+++++++++++++++++++%7C+++++++%7C%0D%0A+++++++++++++++++++%7C+++++++%7C%0D%0A%2B-----------%2B++++++%7C+++++++v%0D%0A%7CBroadcaster%2B------%2B++%2B-------------%2B%0D%0A%7C++Tools++++%7C+++++++++%7CTwitch+PubSub%7C%0D%0A%2B-----------%2B+++++++++%2B-------------%2B%0D%0A+++++++++++++++++++++++++++%5E%0D%0A+++++++++++++++++++++++++++%7C%0D%0A+%2B---------%2B+++++++++++++++%7C%0D%0A+%7C++Viewer+%2B---------------%2B%0D%0A+%7CFront+End%7C%0D%0A+%2B---------%2B&scale=1&background=FFFFFF&E=on&timeout=10&type=.png)

### API Gateway ###
* Send message API that accepts messages (message id, payload, time to live, re-broadcast interval).
  * The message is stored in SimpleDB.
* Message pump API that should be called frequently, every 1 or 2 seconds.  This is currently called by the live broadcaster dashboard frontend as an easy way to make this happen.
  * 1 call per second would consume the free tier in about 1/3rd of a month continuously running.
* Get active messages.  Not used, but could be used by whatever is managing the messages to reload state and determine what needs to be refreshed from other sources.

### Message pump ###
* Throttles the request if a PubSub broadcast message has been sent too recently to avoid breaking terms of service.
* Loads messages from SimpleDB
* Filters out messages that do not need to be refreshed yet.  Exits if nothing to do.
* Sorts the remaining messages by priority.
* Builds a payload up to 5kb containing as many of those messages as it can fit.
* Sends the payload to PubSub.
* Updates the last send time on the sent messages.

# What if it was built in? #
The API Gateway part would no longer be part of the extension itself and gets merged into the API somehow.  Maybe PubSub or something specific to extensions built on top of PubSub.

What might be different if it was a built in feature?
* When an extension loads and hooks into PubSub, one or more initial payloads could be sent to get the extension up and showing content with no delay.
* The extension doesn't have to redrive messages.

Implementation details
* Automatic message compression/decompression to fit more into the 5kb message limit.
* If PubSub can't trigger something when a new listener connects, the client could send a separate message after connecting to PubSub to trigger the initial connection payload.
* The viewer initial connection payload could be directed to just that viewer, or viewer connection(s) could just reset the refresh delay and the next batch message to all viewers could include the priority data to get the extension up and running followed by another message with less important data and maybe a repeat of the priority data.
* Add ability to delete a stored message.  Currently possible by overwriting a message with ttl of zero.
* Limits on the overall stored payloads.

# Demo setup #
* Create an extension and copy the client id and secret into `config.json`.
* Fill in your developer/broadcaster id in `config.json`.
* Configure the aws command line with admin credentials: `aws configure`
* Create the Cloud Formation stack by running create-stack.  The scripts create and re-use the S3 bucket and stack starting with the prefix in `config.json`.
  * This should automatically upload the contents of `config.json` into the simple db table and update the top of `content/live_config.js`.
* Create a cert by running `generate_local_ssl.sh`.
* If needed, strip the pass phrase using `openssl rsa -in server-key.pem -out server-key2.pem`.
* Run `zsh -c 'while :; do; ./run-content-server; done'` to serve the content directory over self-signed https.  It may lock up and need to be restarted occasionally.
* Configure the extension to test locally with the url provided by `run-content-server`.

# Running the demo #
* Clicking a radio button on the live broadcaster frontend will send that value to API gateway once.  The message pump will send the update in the next batch and then periodically afterward.
* The time is sent to API gateway once per second.
* The message pump is called after sending an even time.
* The viewer extension shows data that it receives.
* Reload the viewer extension to see the difference in receiving the time vs last radio state.
