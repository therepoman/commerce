console.log("Test extension loading.");

$(document).ready(function() {

    class DataManager {
        constructor() {
            this.messages = new Map();
            this.updateCallbacks = [];
        }

        receive(target, contentType, message_batch) {
            let thisDataManager = this;

            console.log("Received: ", message_batch);

            $.each(message_batch["messages"], function(index, message) {
                thisDataManager.messages.set(message["message_id"], message["payload"]);
            });

            $.each(thisDataManager.updateCallbacks, function(index, callback) {
                callback();
            });
        }

        get(id) {
            return this.messages.get(id);
        }

        onUpdate(callback) {
            console.log(callback);
            this.updateCallbacks.push(callback);
            console.log(this.updateCallbacks);
        }
    }

    let dataManager = new DataManager()

    window.Twitch.ext.listen("broadcast", function(target, contentType, message) {
        dataManager.receive(target, contentType, $.parseJSON(message));
    });

    dataManager.onUpdate(function(){
        console.log("Update...");
        let value = dataManager.get("Message1");
        if (value != undefined) {
            $("#message1").text("Radio: " + value);
        } else {
            $("#message1").text("Radio: " + "None");
        }

        value = dataManager.get("Time");
        if (value != undefined) {
            $("#time").text("Time: " + value);
        } else {
            $("#time").text("Time: " + "None");
        }
    });

    console.log("Test extension loaded.");
})
