console.log("Test extension loading.");

$(document).ready(function() {
    let region = "YOUR_REGION_HERE";
    let api_id = "YOUR_API_ID_HERE";
    let endpoint = "https://" + api_id + ".execute-api." + region + ".amazonaws.com/api";

    class DataGenerator {
        constructor() {
        }

        put(id, payload, retention_seconds, refresh_interval_seconds) {
            let message = {
                    "message_id": id,
                    "payload": payload,
                    "retention_seconds": retention_seconds,
                    "refresh_interval_seconds": refresh_interval_seconds
                };
            $.ajax({
                method: "POST",
                url: endpoint + "/messages",
                data: JSON.stringify(message),
                contentType: 'application/json'
            }).done(function(response) {
                //console.log("Response: ", response);
            });
        }

        sendBatch() {
            $.ajax({
                method: "POST",
                url: endpoint + "/messages/send_batch",
                data: JSON.stringify({}),
                contentType: 'application/json'
            })
        }
    }

    let dataGenerator = new DataGenerator();

    $("input[name=message1value]").click(function() {
        let value = $("input[name=message1value]:checked").val();
        if (value != undefined) {
            dataGenerator.put("Message1", value, 300, 10);
        }
    });

    let currentTime = 0;
    window.setInterval(function(){
        currentTime = currentTime + 1;
        dataGenerator.put("Time", "" + currentTime, 10, 5);
        $("#currentTime").text("Time: " + currentTime);

        if (currentTime % 2 == 0) {
            if ($("input[name=batchEnabled]").is(":checked")) {
                dataGenerator.sendBatch();
            }
        }
    }, 1000);

    console.log("Test extension loaded.");
})
