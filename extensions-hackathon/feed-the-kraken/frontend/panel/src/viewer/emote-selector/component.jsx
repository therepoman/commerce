import * as React from 'react'
import './styles.css'

const availableEmotes = [
    {
        string: 'Kapa',
        icon: 'https://static-cdn.jtvnw.net/emoticons/v1/25/2.0'
    }, {
        string: 'KapaRoss',
        icon: 'https://static-cdn.jtvnw.net/emoticons/v1/70433/2.0'
    }, {
        string: 'SeemsMinton',
        icon: 'https://static-cdn.jtvnw.net/emoticons/v1/160148/2.0'
    }, {
        string: 'Squid3',
        icon: 'https://static-cdn.jtvnw.net/emoticons/v1/191764/2.0'
    }
]
export class EmoteSelectorPresentation extends React.Component {
    render () {
        return (
            <div>
                <header>Set Avatar</header>
                <div className="emote-selector">
                    { this.renderEmotes() }
                </div>
            </div>
        )
    }

    renderEmotes () {
        return availableEmotes.map(emote => {
            return (
                <span key={emote.string} onClick={this.setEmote(emote.icon)} className={'emote-icon pointer' + this.isSelected(emote.icon)}>
                    <img src={emote.icon}  title={emote.string} />
                </span>
            )
        })
    }

    isSelected (emote) {
        return this.props.selectedEmote === emote ? ' selected' : ''
    }

    setEmote (emote) {
        return () => {
            this.props.setEmote(emote, this.props.jwt)
        }
    }
}