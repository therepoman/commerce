import * as React from 'react'
import { connect, Provider } from 'react-redux'
import * as Actions from '../stores/emotes/actions'
import store, { initialState } from '../stores/root'
import { EmoteSelectorPresentation } from './component.jsx'

const mapStateToProps = (state) => {
    return {
        jwt: state && state.twitch.authData.token || null,
        ...initialState.emotes,
        ...state.emotes
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setEmote: (avatarIcon, jwt) => dispatch(Actions.setEmote(avatarIcon, jwt)),
    }
}

export const EmoteSelectorWithRedux = connect(mapStateToProps, mapDispatchToProps)(EmoteSelectorPresentation)

export class EmoteSelector extends React.Component {
    render () {
        return (
            <Provider store={store}>
                <EmoteSelectorWithRedux />
            </Provider>
        )
    }
}