import TwitchExtStore, { TwitchInitialState } from '../../store'
import BitsStore, { BitsInitialState } from './bits/store'
import EmotesStore, { EmotesInitialState } from './emotes/store'
import FeedStore, { FeedInitialState } from './feed/store'
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'

export default createStore(combineReducers({
    twitch: TwitchExtStore,
    bits: BitsStore,
    emotes: EmotesStore,
    feed: FeedStore,
}), applyMiddleware(thunkMiddleware))

export const initialState = {
    twitch: TwitchInitialState,
    bits: BitsInitialState,
    emotes: EmotesInitialState,
    feed: FeedInitialState,
}
