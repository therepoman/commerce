import { post } from '../../../shared/requests'

export const ACTION_TYPES = {
    FEED: 'feed',
    FEED_COMPLETE: 'feed-complete',
}

export const feed = (bitsCount, channelID, JWT) => {
    return function (dispatch) {
        dispatch({
            type: ACTION_TYPES.FEED,
        })
        post('https://labzhs79qg.execute-api.us-west-2.amazonaws.com/test/feedHackathon', {
            bits_amount: bitsCount,
        }, JWT).then(() => {
            dispatch({
                type: ACTION_TYPES.FEED_COMPLETE
            })
        }).catch((err) => {
            console.log('feed error', err)
            dispatch({
                type: ACTION_TYPES.FEED_COMPLETE
            })
        })
    }
}
