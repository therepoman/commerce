import { createStore } from 'redux'
import { ACTION_TYPES } from './actions'

export const initialState = {
    feedInProgress: false
}

function FeedStore(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.FEED:
            return {
                feedInProgress: true
            }
            break
        case ACTION_TYPES.FEED_COMPLETE:
            return {
                feedInProgress: false
            }
            break
    }
    return state;
}

export default FeedStore
