import { createStore } from 'redux'
import { ACTION_TYPES } from './actions'

export const initialState = {
    bitsCount: 5
}

function BitsStore(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.SET_BITS:
            return {
                ...state,
                bitsCount: action.bitsCount
            }
            break
    }
    return state;
}

export default BitsStore
