export const ACTION_TYPES = {
    SET_BITS: 'setBitsCount'
}

export const setBitsCount = (bitsCount) => ({
    type: ACTION_TYPES.SET_BITS,
    bitsCount
})
