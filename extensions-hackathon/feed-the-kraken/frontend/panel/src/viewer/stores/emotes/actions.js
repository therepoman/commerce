import { post } from '../../../shared/requests'

export const ACTION_TYPES = {
    SET_EMOTE: 'setEmote'
}

export const setEmote = (avatarIcon, jwt) => {
    return (dispatch) => {
        let body = null;
        if (avatarIcon) {
            body = {
                avatarIcon
            }
        }
        post('https://labzhs79qg.execute-api.us-west-2.amazonaws.com/test/updateKrakenHackathonAvatar', body, jwt)
        .then((response) => {
            const body = JSON.parse(response.body)
            dispatch({
                type: ACTION_TYPES.SET_EMOTE,
                avatarIcon: body.avatarIcon
            })
        })
    }
}
