import { createStore } from 'redux'
import { ACTION_TYPES } from './actions'

export const initialState = {
    selectedEmote: null
}

function EmoteStore(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.SET_EMOTE:
            return {
                ...state,
                selectedEmote: action.avatarIcon
            }
            break
    }
    return state;
}

export default EmoteStore
