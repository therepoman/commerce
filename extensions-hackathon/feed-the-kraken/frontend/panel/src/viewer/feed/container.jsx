import * as React from 'react'
import { connect, Provider } from 'react-redux'
import store, { initialState } from '../stores/root'
import * as Actions from '../stores/feed/actions'
import { FeedPresentation } from './component.jsx'

const mapStateToProps = (state) => {
    return {
        bitsCount: state && state.bits.bitsCount || initialState.bits.bitsCount,
        authData: state && state.twitch.authData || initialState.twitch.authData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        feed: (bitsCount, channelID, JWT) => dispatch(Actions.feed(bitsCount, channelID, JWT)),
    }
}

export const FeedWithRedux = connect(mapStateToProps, mapDispatchToProps)(FeedPresentation)

export class Feed extends React.Component {
    render () {
        return (
            <Provider store={store}>
                <FeedWithRedux />
            </Provider>
        )
    }
}