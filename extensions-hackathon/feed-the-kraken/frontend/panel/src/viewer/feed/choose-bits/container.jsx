import * as React from 'react'
import { connect, Provider } from 'react-redux'
import * as Actions from '../../stores/bits/actions'
import store, { initialState } from '../../stores/root'
import { ChooseBitsPresentation } from './component.jsx'

const mapStateToProps = (state) => {
    return {
        ...initialState.bits,
        ...state.bits
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setBitsCount: emoteString => dispatch(Actions.setBitsCount(emoteString)),
    }
}

export const ChooseBitsWithRedux = connect(mapStateToProps, mapDispatchToProps)(ChooseBitsPresentation)

export class ChooseBits extends React.Component {
    render () {
        return (
            <Provider store={store}>
                <ChooseBitsWithRedux />
            </Provider>
        )
    }
}