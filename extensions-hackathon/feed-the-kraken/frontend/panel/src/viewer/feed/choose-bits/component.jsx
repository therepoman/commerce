import * as React from 'react'
import './styles.css'

const bitsCounts = [5, 10, 100]

export class ChooseBitsPresentation extends React.Component {
    render () {
        return (
            <div className="choose-bits">
                { this.renderBits() }
            </div>
        )
    }

    renderBits () {
        return bitsCounts.map(bitCount => {
            return (
                <span key={bitCount + 'bits'} onClick={this.setBitsCount(bitCount)} className={'bits pointer' + this.isSelected(bitCount)} title={bitCount + ' bits'}>
                    {bitCount} <img className="bit-logo" src="https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/100/2.png" />
                </span>
            )
        })
    }

    isSelected (count) {
        return this.props.bitsCount === count ? ' selected' : ''
    }

    setBitsCount (count) {
        return () => {
            this.props.setBitsCount(count)
        }
    }
}