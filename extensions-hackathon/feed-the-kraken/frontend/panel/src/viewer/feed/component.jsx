import * as React from 'react'
import { ChooseBits } from './choose-bits/container.jsx'
import './styles.css'

export class FeedPresentation extends React.Component {
    render () {
        return (
            <div className="feed">
                <ChooseBits />
                <button onClick={this.feed.bind(this)} className="feed-btn">Feed the Kraken</button>
            </div>
        )
    }

    feed () {
        this.props.feed(this.props.bitsCount, this.props.authData.channelID, this.props.authData.token)
    }
}