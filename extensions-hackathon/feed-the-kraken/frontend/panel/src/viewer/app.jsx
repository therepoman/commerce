import * as React from 'react'
import { EmoteSelector } from './emote-selector/container.jsx'
import { Feed } from './feed/container.jsx'

export class App extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (!(this.props.authData && this.props.authData.token) && (nextProps.authData && nextProps.authData.token)) {
            this.props.getAvatar(nextProps.authData.token)
        }
    }

    render() {        
        if (!this.props.authData) return null
        return (
            <div>
                <header>Feed the Kraken</header>
                <Feed />
                <EmoteSelector />
            </div>
        );
    }
}
