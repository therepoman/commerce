import { ACTION_TYPES } from './actions'

export const initialState = {
    authData: null,
    context: null,
    error: null,
    messages: {},
    followed: {}
}

function TwitchExt(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ON_AUTHORIZED:
            return {
                ...state,
                authData: action.data
            }
            break
        case ACTION_TYPES.ON_CONTEXT:
            return {
                ...state,
                context: action.data
            }
            break
        case ACTION_TYPES.ON_ERROR:
            return {
                ...state,
                error: action.data
            }
            break
        case ACTION_TYPES.ON_MESSAGE:
            const messageData = {
                ...state.messages
            }
            messageData[action.target] = JSON.parse(action.message)
            return {
                ...state,
                messages: {
                    ...messageData
                }
            }
            break
        case ACTION_TYPES.ON_FOLLOW:
            const followed = {}
            followed[data.channelName] = data.didFollow
            return {
                ...state,
                followed
            }
            break
    }
    return state
}

export default TwitchExt
