import { get, post } from '../../../shared/requests'

export const ACTION_TYPES = {
    SET_KRAKENS: 'setKrakens',
    SELECT_KRAKEN: 'selectKraken',
}

export const getKrakens = () => {
    return (dispatch) => {
        get('https://labzhs79qg.execute-api.us-west-2.amazonaws.com/test/getKrakensHackathon')
            .then((response) => {
                let krakens
                if(response.body) {
                    krakens = JSON.parse(response.body).krakens
                }
                if (krakens) {
                    dispatch({
                        type: ACTION_TYPES.SET_KRAKENS,
                        krakens
                    })
                }

            });
    }
}

export const selectKraken = (kraken_id, jwt) => {
    return (dispatch) => {
        let body = null;
        if (kraken_id) {
            body = {
                kraken_id,
            }
        }
        post('https://labzhs79qg.execute-api.us-west-2.amazonaws.com/test/setBroadcasterKrakenHackathon', body, jwt)
        .then((response) => {
            const body = JSON.parse(response.body)
            dispatch({
                type: ACTION_TYPES.SELECT_KRAKEN,
                kraken_id: body.kraken_id,
            })
        })
    }
}