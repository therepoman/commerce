import { ACTION_TYPES } from './actions'

export const initialState = {
    krakens: [],
    selectedKraken: null,
}

function FeedStore(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.SET_KRAKENS:
            return {
                ...state,
                krakens: action.krakens,
            }
            break
        case ACTION_TYPES.SELECT_KRAKEN:
            return {
                ...state,
                selectedKraken: action.kraken_id
            }
            break
    }
    return state;
}

export default FeedStore