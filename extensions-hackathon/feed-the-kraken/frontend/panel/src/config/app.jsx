import * as React from 'react'

export class App extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (!(this.props.authData && this.props.authData.token) && (nextProps.authData && nextProps.authData.token)) {
            this.props.selectKraken(null, nextProps.authData.token)
        }
    }

    render() {
        if (this.props.krakens.length === 0) {
            this.props.getKrakens()
            return null;
        }
        return (
            <div className="kraken-list">
                <h1>Select your Kraken</h1>
                <ul>
                    {this.renderKrakens()}
                </ul>
            </div>
        );
    }

    renderKrakens () {
        return this.props.krakens.map((kraken) => {
            return (
                <li className={`pointer ${this.props.selectedKraken === kraken.id ? 'selected' : ''}`} key={kraken.id} onClick={this.onSelectKraken(kraken.id)}>
                    {kraken.name}
                </li>
            )
        })
    }

    onSelectKraken (krakenId) {
        return () => {
            this.props.selectKraken(krakenId, this.props.authData.token)
        }
    }
}
