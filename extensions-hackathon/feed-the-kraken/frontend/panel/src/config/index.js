import * as ReactDom from 'react-dom'
import * as React from 'react'
import { connect, Provider } from 'react-redux'
import * as Actions from '../actions'
import TwitchExtStore, { initialState as TwitchExtInitialState } from '../store'
import KrakensStore, { initialState as KrakenStoreInitialState } from './stores/krakens/store'
import * as KrakenActions from './stores/krakens/actions'
import { App } from './app.jsx'
import * as TwitchExt from '../twitch-ext'
import './index.css'
import { applyMiddleware, createStore, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk'

TwitchExt.onAuthorized((authData) => Store.dispatch(Actions.setAuthorization(authData)))
TwitchExt.onContext((contextData) => Store.dispatch(Actions.setContext(contextData)))
TwitchExt.onError((error) => Store.dispatch(Actions.setError(error)))

const Store = createStore(combineReducers({
    twitch: TwitchExtStore,
    krakens: KrakensStore
}), applyMiddleware(thunkMiddleware))

const mapStateToProps = (state) => {
    return {
        authData: state && state.twitch.authData || TwitchExtInitialState.authData,
        krakens: state && state.krakens.krakens || KrakenStoreInitialState.krakens,
        selectedKraken: state && state.krakens.selectedKraken || KrakenStoreInitialState.selectedKraken,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getKrakens: () => dispatch(KrakenActions.getKrakens()),
        selectKraken: (krakenId, jwt) => dispatch(KrakenActions.selectKraken(krakenId, jwt))
    }
}

const AppWithRedux = connect(mapStateToProps, mapDispatchToProps)(App)

ReactDom.render(
    <Provider store={Store}>
        <AppWithRedux />
    </Provider>,
    document.getElementById('app')
)
