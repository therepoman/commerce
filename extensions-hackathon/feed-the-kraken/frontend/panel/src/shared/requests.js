
function makeRequest(method, url, body, jwt, cb) {
    const httpRequest = new XMLHttpRequest()

    httpRequest.onreadystatechange = readyStateChange.bind(httpRequest, cb)
    httpRequest.open(method, url)
    if (jwt) {
        httpRequest.setRequestHeader('Authorization', jwt)
    }
    httpRequest.setRequestHeader('x-api-key', '6Ks7TP8OVX1CIA4r5FT9f1K0kKsJZw1ualhITmEw')
    if (body && (method === 'POST' || method === 'PUT')) {
        try {
            body = JSON.stringify(body)
            httpRequest.setRequestHeader('Content-Type', 'application/json')
        } catch (e) {
            body = body
        }
    }
    httpRequest.send(body || null)
}

function readyStateChange(cb) {
    const httpRequest = this
    let response = null
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        try {
            response = JSON.parse(httpRequest.responseText)
        } catch (e) {
            response = httpRequest.responseText
        }
        if (httpRequest.status === 200) {
            cb(null, response)
        } else {
            cb(response, null)
        }
    }
}

export function get(url, jwt) {
    return new Promise((resolve, reject) => {
        makeRequest('GET', url, null, jwt, (err, response) => {
            if (err) {
                return reject(err)
            }
            resolve(response)
        })
    })
}

export function post(url, body, jwt) {
    return new Promise((resolve, reject) => {
        makeRequest('POST', url, body, jwt, (err, response) => {
            if (err) {
                return reject(err)
            }
            resolve(response)
        })
    })
}

export function put(url, body, jwt) {
    return new Promise((resolve, reject) => {
        makeRequest('PUT', url, body, jwt, (err, response) => {
            if (err) {
                return reject(err)
            }
            resolve(response)
        })
    })
}

export function send(type, url, body, jwt) {
    return new Promise((resolve, reject) => {
        makeRequest(type, url, body, jwt, (err, response) => {
            if (err) {
                return reject(err)
            }
            resolve(response)
        })
    })
}
