import * as TwitchExt from './twitch-ext'

const callbacksByTarget = {}

export const ACTION_TYPES = {
    LOADED: 'loaded',
    ON_AUTHORIZED: 'onAuthorized',
    ON_ERROR: 'onError',
    ON_CONTEXT: 'onContext',
    ON_MESSAGE: 'onMessage',
    ON_FOLLOW: 'onFollow'
}

export const setAuthorization = (authData) => ({
    type: ACTION_TYPES.ON_AUTHORIZED,
    data: authData
})

export const setContext = (contextData) => ({
    type: ACTION_TYPES.ON_CONTEXT,
    data: contextData
})

export const setError = (error) => ({
    type: ACTION_TYPES.ON_ERROR,
    data: error
})

export const setLoaded = () => ({
    type: ACTION_TYPES.LOADED
})

export const setMessage = (target, message) => ({
    type: ACTION_TYPES.ON_MESSAGE,
    target,
    message
})

export const listen = (target) => {
    return function(dispatch) {
        if (!callbacksByTarget[target]) {
            callbacksByTarget[target] = (target, contentType, message) => {
                dispatch(setMessage(target, message))
            }
            TwitchExt.listen(target, callbacksByTarget[target])
        }
    }
}

export const unlisten = (target) => {
    if (!callbacksByTarget[target]) {
        TwitchExt.listen(target, callbacksByTarget[target])
        callbacksByTarget[target] = null
    }
}

export const send = (target, contentType, message) => {
    TwitchExt.send(target, conentType, message)
}

export const follow = (channelName) => {
    TwitchExt.actions.follow(channelName)
}

export const setFollow = (data) => ({
    type: ACTION_TYPES.ON_FOLLOW,
    data
})

export const requestIdShare = () => {
    TwitchExt.actions.requestIdShare()
    return {
        type: ACTION_TYPES.REQUEST_ID_SHARE
    }
}
