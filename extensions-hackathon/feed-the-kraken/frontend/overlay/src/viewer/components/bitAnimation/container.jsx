import * as React from 'react'
import { BitAnimationComponent } from './component.jsx'
import { connect, Provider } from 'react-redux'
import { Store } from '../../stores/root'
import { initialState as KrakenStoreInitialState } from '../../stores/kraken/store'

const mapStateToProps = (state) => {
    return {
        feed: state && state.kraken.feed || KrakenStoreInitialState.feed,
        resume: state && state.kraken.resume || KrakenStoreInitialState.resume,
        didExplode: state && state.kraken.didExplode || KrakenStoreInitialState.didExplode,
    }
}

const BitAnimationWithRedux = connect(mapStateToProps)(BitAnimationComponent)

export class BitAnimation extends React.Component {
    render () {
        return (
            <Provider store={Store}>
                <BitAnimationWithRedux bitProps={this.props.bitProps}/>
            </Provider>
        )
    }
}