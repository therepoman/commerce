import * as React from 'react'

export class BitAnimationComponent extends React.Component {
    constructor (props) {
        super(props)
        this.setRef = this.setRef.bind(this)
        this.ref = null
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.feed || (nextProps.resume && !this.props.didExplode)) {
            const computedStyles = window.getComputedStyle(this.ref.parentElement);
            this.containerHeight = this.ref.parentElement.parentElement.offsetHeight;
            this.containerWidth = this.ref.parentElement.parentElement.offsetWidth;
            this.bottom = computedStyles.bottom;
            this.top = computedStyles.top;
            this.left = computedStyles.left;
        }
    }

    render () {
        const {name, originalLeft, originalTop, endLeft, endTop, behind, orbitDuration, rotationStart} = this.props.bitProps;
        const bitImage = <img src="./static/image2.png"  className="bitElement"/>
        const animation = `${name} ${orbitDuration}s infinite`
        let animationStyle = `
            @keyframes ${name} {
                0% { top: ${originalTop}%; left: ${originalLeft}%; z-index: ${behind*2-1};}
                50% { top: ${endTop}%; left: ${endLeft}%; z-index: ${(behind*2-1)}; }
                51% { top: ${endTop}%; left: ${endLeft}%; z-index: ${0-(behind*2-1)}; }
                100% { top: ${originalTop}%; left: ${originalLeft}%; z-index: ${0-(behind*2-1)}; }
            }


            #${name} {
                animation: ${animation};
            }`

        if (this.props.feed && !this.props.didExplode) {
            animationStyle = `
                @keyframes ${name}-drop {
                    0% {
                        bottom: ${this.bottom};
                        transform: rotate(0deg);
                    }
                    40% {
                        bottom: 0px;
                        transform: rotate(90deg);
                    }
                    75% {
                        bottom: 10%;
                        transform: rotate(180deg);
                    }
                    100% {
                        bottom: 0px;
                        transform: rotate(270deg);
                    }
                }
                #${name} {
                    bottom: 0px;
                    left: ${this.left};
                    transform: rotate(270deg);
                    animation: ${name}-drop .5s ease-out;
                }
            `
        }

        if (this.props.feed && this.props.didExplode) {
            const botNum = Math.round(+this.bottom.replace('px', '')) - Math.round(this.containerHeight/2)
            const leftNum = Math.round(+this.left.replace('px', '')) - Math.round(this.containerWidth/2)
            let endBottom = (botNum/(this.containerHeight/2) * 1000) + '%';
            let endLeft = (leftNum/(this.containerWidth/2) * 1000) + '%';
            let duration = Math.abs(2*botNum/(this.containerHeight/2))
            animationStyle = `
                @keyframes ${name}-explode {
                    0% {
                        bottom: ${this.bottom};
                        left: ${this.left};
                    }
                    100% {
                        bottom: ${endBottom};
                        left: ${endLeft};
                    }
                }
                #${name} {
                    bottom: ${endBottom};
                    left: ${endLeft};
                    animation: ${name}-explode ${duration}s linear;
                }
            `
        }

        if (this.props.resume && !this.props.didExplode) {
            animationStyle = `
                @keyframes ${name}-resume {
                    0% {
                        top: ${this.top};
                        left: ${this.left};
                        transform: rotate(270deg);
                        z-index: -1;
                    }
                    50% {
                        top: 45%;
                        left: 45%;
                        transform: rotate(360deg);
                        z-index: -1;
                    }
                    100% { 
                        top: ${originalTop}%; 
                        left: ${originalLeft}%; 
                        transform: rotate(360deg);
                        z-index: -1;
                    }
                }
                #${name} {
                    top: ${originalTop}%; 
                    left: ${originalLeft}%; 
                    transform: rotate(0deg);
                    animation: ${name}-resume 3s ease-out;
                }
            `
        }

        return (
            <style ref={this.setRef}>{animationStyle}</style>
        );
    }

    setRef (elem) {
        this.ref = elem
    }
}