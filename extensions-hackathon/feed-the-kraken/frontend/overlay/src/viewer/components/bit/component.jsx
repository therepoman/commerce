import * as React from 'react'
import { BitAnimation } from '../bitAnimation/container.jsx'

export class BitComponent extends React.Component {
    render () {
        const {name, originalLeft, originalTop, endLeft, endTop, behind, orbitDuration, rotationStart} = this.props.bitProps;
        const bitImage = <img src="./static/image2.png"  className="bitElement"/>
        return (
            <div key={name} id={name} className="bitContainer">
                <BitAnimation bitProps={this.props.bitProps} />
                {bitImage}
            </div>
        );
    }
}