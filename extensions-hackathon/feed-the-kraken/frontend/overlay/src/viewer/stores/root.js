import TwitchExtStore, { initialState as TwitchExtInitialState } from '../../store'
import KrakenStore, { initialState as KrakenStoreInitialState } from './kraken/store'
import { combineReducers, applyMiddleware, createStore } from 'redux';
import thunkMiddelware from 'redux-thunk';

export const Store = createStore(combineReducers({
    twitch: TwitchExtStore,
    kraken: KrakenStore,
}), applyMiddleware(thunkMiddelware))