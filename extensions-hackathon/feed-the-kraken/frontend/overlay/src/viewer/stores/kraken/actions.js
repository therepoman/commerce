
export const ACTION_TYPES = {
    SET_BITS: 'setBits',
    MOVE_KRAKEN: 'moveKraken',
    FEED_KRAKEN: 'feedKraken',
    RESUME: 'resume',
    RESET: 'reset',
}

export const setBits = (bitsQuantity) => ({
    type: ACTION_TYPES.SET_BITS,
    bitsQuantity
})

export const feedKraken = (bitsQuantity, avatar, didExplode) => {
    return (dispatch) => {
        dispatch(setBits(bitsQuantity))
        dispatch({
            type: ACTION_TYPES.MOVE_KRAKEN,
            avatar,
        })
        setTimeout(() => {
            dispatch({
                type: ACTION_TYPES.FEED_KRAKEN,
                didExplode
            })
            setTimeout(() => {
                if (didExplode) {
                    dispatch(setBits(0))
                }
                dispatch({
                    type: ACTION_TYPES.RESUME,
                })

                setTimeout(() => {
                    dispatch({
                        type: ACTION_TYPES.RESET,
                    })
                }, 3000)
            }, 2000)
        }, 3000)
    }
}