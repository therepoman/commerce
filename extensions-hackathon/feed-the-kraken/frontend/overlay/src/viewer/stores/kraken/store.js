import { ACTION_TYPES } from './actions'

export const initialState = {
    bits: setupBits(0),
    feed: false,
    resume: false,
    angry: false,
    avatar: null,
    didExplode: false,
}

function KrakenStore(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.SET_BITS:
            const bitsQuantity = Math.min(action.bitsQuantity, 125)
            if (bitsQuantity === state.bits.length) {
                return {
                    ...state
                }
            }
            return {
                ...state,
                bits: setupBits(bitsQuantity)
            }
            break
        case ACTION_TYPES.MOVE_KRAKEN:
            return {
                ...state,
                move: true,
                avatar: action.avatar,
            }
            break
        case ACTION_TYPES.FEED_KRAKEN:
            return {
                ...state,
                feed: true,
                didExplode: action.didExplode,
                angry: true,
            }
            break
        case ACTION_TYPES.RESUME:
            return {
                ...state,
                resume: true,
                angry: false,
            }
            break
        case ACTION_TYPES.RESET:
            return {
                ...state,
                resume: false,
                feed: false,
                angry: false,
                move: false,
                didExplode: false,
            }
            break
    }
    return state;
}

function setupBits(bitsQuantity){
    const bits = []
        for (var i = 0; i < bitsQuantity; i++) {
            // The bits orbit in a 50% radius. Find a random top and left value.
            var originalLeft, originalTop, endLeft, endTop;
            var sample = Math.random() * Math.PI;
            var point1X = Math.cos(sample) * 30; // radius is 25
            var point1Y = Math.sin(sample) * 30;
            var point2X = -point1X;
            var point2Y = -point1Y;
        
            if (Math.random() < 0.5) {
                // Start at point1
                originalLeft = "" + (45 + (point1X));
                originalTop = "" + (45 + (point1Y));
                endLeft = "" + (45 + (point2X));
                endTop = "" + (45 + (point2Y));
            } else {
                // Start at point2
                originalLeft = "" + (45 + (point2X));
                originalTop = "" + (45 + (point2Y));
                endLeft = "" + (45 + (point1X));
                endTop = "" + (45 + (point1Y));
            }
        
            bits.push({
                name: "krakenBit"+i, 
                originalLeft: Math.round(originalLeft), 
                originalTop: Math.round(originalTop), 
                endLeft: Math.round(endLeft), 
                endTop: Math.round(endTop), 
                behind: Math.random() < 0.5, 
                orbitDuration: (2 + Math.round(Math.random()*4)),
                rotationStart: Math.round(Math.random()*360),
            })
        }

        return bits
}
export default KrakenStore
