import * as React from 'react'
import { BitComponent } from './components/bit/component.jsx'

export class App extends React.Component {
  componentDidMount () {
    this.props.listen()
  }

  componentWillUnmount () {
    this.props.unlisten()
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.message !== this.props.message && !this.props.move) {
      this.props.feedKraken(+nextProps.message.totalPotAmount, nextProps.message.userAvatar, nextProps.message.hasExploded)
    }
  }

  render() {
      return (
        <div className={this.props.move ? 'active' : ''}>
          <img src={this.props.avatar} className="user-avatar" />
          <div className="avatar-bit">
            <div className="avatar-bit-container">
              <img src="./static/image2.png" />
            </div>
          </div>
          <div id="kraken">
              <div id="kraken-container">
                  <div id="kraken-bg" className={(this.props.message && this.props.message.krakenID) || ''}/>
                  <img id="kraken-face" src={this.getKrakenFace()}/>
                  {this.createKrakenBits(this.props.bits)}
              </div>
          </div>
        </div>
      );
  }

  getKrakenFace () {
      return this.props.isAngry ? './static/angry.png' : './static/neutral.png'
  }

  createKrakenBits (bits) {
      return bits.map((bit) => {
          return <BitComponent key={bit.name} bitProps={bit} />
      })
  }
}
