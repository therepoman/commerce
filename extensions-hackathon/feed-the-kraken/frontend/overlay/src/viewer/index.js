import * as ReactDom from 'react-dom'
import * as React from 'react'
import { connect, Provider } from 'react-redux'
import { Store } from './stores/root'
import * as Actions from '../actions'
import * as KrakenActions from './stores/kraken/actions'
import { initialState as TwitchExtInitialState } from '../store'
import { initialState as KrakenStoreInitialState } from './stores/kraken/store'
import { App } from './app.jsx'
import * as TwitchExt from '../twitch-ext'
import './index.css'


TwitchExt.onAuthorized((authData) => Store.dispatch(Actions.setAuthorization(authData)))
TwitchExt.onContext((contextData) => Store.dispatch(Actions.setContext(contextData)))
TwitchExt.onError((error) => Store.dispatch(Actions.setError(error)))

const mapStateToProps = (state) => {
    return {
        authData: state &&  state.twitch.authData || TwitchExtInitialState.authData,
        bits: state && state.kraken.bits || KrakenStoreInitialState.bits,
        isAngry: state && state.kraken.angry || KrakenStoreInitialState.angry,
        message: state && state.twitch.messages.broadcast || null,
        move: state && state.kraken.move || false,
        avatar: state && state.kraken.avatar || null,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        feedKraken: (bits, avatar, didExplode) => dispatch(KrakenActions.feedKraken(bits, avatar, didExplode)),
        listen: () => dispatch(Actions.listen('broadcast')),
        unlisten: () => dispatch(Actions.unlisten('broadcast')),
    }
}

const AppWithRedux = connect(mapStateToProps, mapDispatchToProps)(App)

ReactDom.render(
    <Provider store={Store}>
        <AppWithRedux />
    </Provider>,
    document.getElementById('app')
)
