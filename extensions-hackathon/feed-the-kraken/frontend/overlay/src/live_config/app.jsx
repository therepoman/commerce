import * as React from 'react'

export class App extends React.Component {
    render() {
        return (
            <div>
                <p>Auth</p>
                <code>{JSON.stringify(this.props.authData, null, 4)}</code>
                <p>Context</p>
                <code>{JSON.stringify(this.props.context, null, 4)}</code>
                <p>Error</p>
                <code>{JSON.stringify(this.props.error, null, 4)}</code>
            </div>
        );
    }
}
