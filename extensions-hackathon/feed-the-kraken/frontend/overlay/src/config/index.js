import * as ReactDom from 'react-dom'
import * as React from 'react'
import { App } from './app.jsx'
import './index.css'

ReactDom.render(
    <App />,
    document.getElementById('app')
)
