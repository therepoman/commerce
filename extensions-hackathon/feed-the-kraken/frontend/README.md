# Feed the Front End
Includes front end for feeding panel and overlay with kraken animations.

First, install dependencies via
```
npm install
```

Then create the self signed certs

```
cd certs
./generate_local_ssl.sh
```

To start the dev server @https://localhost:8081 run
```
npm run dev
```