import boto3, logging, json

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TUID_PARAM = 'tuid'
DONATOR_PARAM = 'donator'
BITS_PARAM = 'bits'
COUNT_PARAM = 'count'
KRAKEN_PARAM = 'kraken'
BROADCASTER_PARAM = 'broadcaster'
TOTAL_POT_PARAM = 'totalPotAmount'
SINGLE_DONATION_PARAM = 'donationAmount'

SNS_ARN = 'arn:aws:sns:us-west-2:703507011456:explodeKraken'
SNS = boto3.client('sns')
DYNAMODB = boto3.resource('dynamodb')
TABLE = DYNAMODB.Table('krakenHackathon')

# Helper method to publish to SNS and return a response
def returnResponseAndPushToSNS(message):
    SNS.publish(
        TargetArn=SNS_ARN,
        Message=json.dumps({'default': json.dumps(message)}),
        Subject='Explode Kraken',
        MessageStructure='json'
    )
    return message

# Helper method to return a valid response with the updated allowance
def handleHappyResponse(tuid, payout, count, bitsAmount, broadcaster, kraken):
    message = {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({
            DONATOR_PARAM : str(tuid),
            TOTAL_POT_PARAM : str(payout),
            COUNT_PARAM : str(count),
            SINGLE_DONATION_PARAM : str(bitsAmount),
            BROADCASTER_PARAM : str(broadcaster),
            KRAKEN_PARAM : str(kraken)
        })
    }
    return returnResponseAndPushToSNS(message)

# Helper method for a 400 response
def handleInvalidRequest():
    return {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': 'Not all required parameters provided/ some parameters invalid' })
    }

# Helper method to feed the Kraken
def feedKrakenPostConsume(tuid, broadcaster, kraken, bitsAmount):
    # Updates kraken with new bits
    krakenTableResponse = TABLE.update_item(
        Key={'id': kraken},
        UpdateExpression='SET bitsAmount = bitsAmount + :bits, feedCount = feedCount + :one',
        ExpressionAttributeValues={
            ':bits': int(bitsAmount),
            ':one': 1
        },
        ReturnValues='ALL_NEW'
    )
    payout = krakenTableResponse['Attributes']['bitsAmount']
    count = krakenTableResponse['Attributes']['feedCount']
    
    # Explode the Kraken if necessary
    return handleHappyResponse(tuid, payout, count, bitsAmount, broadcaster, kraken)

# Helper method to handle the query (check inputs)
def handleQuery(event):
    tuid =  event[TUID_PARAM]
    bits = event[BITS_PARAM]
    kraken = event[KRAKEN_PARAM]
    broadcaster = event[BROADCASTER_PARAM]
    if tuid == None or tuid == '':
        logger.error("No valid tuid provided")
        return handleInvalidRequest()
    if bits == None or not bits.isdigit():
        logger.error("No valid bits amount provided")
        return handleInvalidRequest()
    if kraken == None or kraken == '':
        logger.error("No valid kraken provided")
        return handleInvalidRequest()
    if broadcaster == None or broadcaster == '':
        logger.error("No valid broadcaster provided")
        return handleInvalidRequest()
    return feedKrakenPostConsume(tuid, broadcaster, kraken, bits)

# Default behavior between SNS and API Gateway Calls
def commonBehavior(event):
    # Ensure required data is passed in
    if (TUID_PARAM not in event or 
        BITS_PARAM not in event or 
        KRAKEN_PARAM not in event or 
        BROADCASTER_PARAM not in event):
        return handleInvalidRequest()
    return handleQuery(event)

# Helper method to convert a string into a useable dictionary
def convertToDictionary(someString):
    json_acceptable_string = someString.replace("'", "\"")
    return json.loads(json_acceptable_string)

# Main handler
def lambda_handler(event, context):
    logger.info("Received event {}".format(event))
    # Check if SNS
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                sns = record['Sns']
                # Message is wrongfully read as a string
                json_dict = convertToDictionary(sns['Message'])
                # Get the message body
                if 'body' in json_dict:
                    # Body is wrongfully read as a string
                    return commonBehavior(convertToDictionary(json_dict['body']))   
    # Otherwise, assume API gateway
    return commonBehavior(event)

