import logging, json, boto3, time, random, requests, jwt, base64, datetime
from boto3.dynamodb.conditions import Key

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TOTAL_POT_PARAM = 'totalPotAmount'
DONATOR_PARAM = 'donator'
COUNT_PARAM = 'count'
KRAKEN_PARAM = 'kraken'
DONATION_PARAM = 'donationAmount'
BROADCASTER_PARAM = 'broadcaster'
TUID_PARAM = 'tuid'
BITS_PARAM = 'bits'

DYNAMODB = boto3.resource('dynamodb')
BITS_TABLE = DYNAMODB.Table('HackathonBitsAllowance')
AVATAR_TABLE = DYNAMODB.Table('KrakenAvatarHackathon')
KRAKEN_TABLE = DYNAMODB.Table('krakenHackathon')
BROADCASTER_KRAKEN_TABLE = DYNAMODB.Table('broadcasterKrakenMappingHackathon')

USER_KEY = "user"
AVATAR_ICON_KEY = "avatarIcon"
DEFAULT_AVATAR = "https://static-cdn.jtvnw.net/emoticons/v1/25/2.0"

# TODO: Client ID for panel extension, need to swap with overlay extension once that's set up
CLIENT_ID = "xn3p8lzablvtuccfjzzy1d9r74ypoy"

# TODO: Move out of code and then rotate
OVERLAY_SECRET = "oI0aSrVDr92jNt87PN1K2s/CqxxB8zrT4e1W1k00NPU="

# Helper method to determine if the Kraken explodes
def determineKrakenExplosion(numBits, feedCount):
    return numBits > 100 and feedCount > 5 and random.random() > 0.5

# Size is an int in [0, 10], 10 representing the largest size
# Current calculation to determine size is something I randomly made up
# and is probably really dumb
def determineKrakenSize(numBits, feedCount):
    return min(10, (min(100, numBits) - 10) + (min(10, feedCount)))
    
def getUserAvatar(userID):
    item = AVATAR_TABLE.get_item(Key={USER_KEY: userID})

    # Return default if user doesn't exist in our table
    if item == None or item == {} or 'Item' not in item:
        return DEFAULT_AVATAR
    else:
        return item['Item'][AVATAR_ICON_KEY]

# Helper method to explode the Kraken (and update DynamoDB)
def explodeKraken(krakenID, totalPotAmount, count):
    updatedItem = KRAKEN_TABLE.update_item(
        Key={'id': krakenID},
        UpdateExpression='SET bitsAmount = bitsAmount - :totalPotAmount, feedCount = feedCount - :count, lastExplodeTime = :now',
        ConditionExpression="bitsAmount >= :totalPotAmount",
        ExpressionAttributeValues={
            ':totalPotAmount': totalPotAmount,
            ':count': count,
            ':now': int(time.time())
        },
        ReturnValues='ALL_NEW'
    )
    return updatedItem

def handleQuery(event):
    donatorID =  event[DONATOR_PARAM]
    totalPotAmount = event[TOTAL_POT_PARAM]
    donationAmount = event[DONATION_PARAM]
    broadcasterID = event[BROADCASTER_PARAM]
    count = event[COUNT_PARAM]
    krakenID = event[KRAKEN_PARAM]
    if donatorID == None or donatorID == '':
        logger.error("No valid donatorID provided")
        return handleInvalidRequest()
    if broadcasterID == None or broadcasterID == '':
        logger.error("No valid broadcasterID provided")
        return handleInvalidRequest()
    if count == None or not count.isdigit():
        logger.error("No valid count provided")
        return handleInvalidRequest()
    if totalPotAmount == None or not totalPotAmount.isdigit():
        logger.error("No valid totalPotAmount provided: {}".format(totalPotAmount))
        return handleInvalidRequest()
    if krakenID == None or krakenID == '':
        logger.error("No valid kraken provided")
        return handleInvalidRequest()
    if donationAmount == None or not donationAmount.isdigit():
        logger.error("No valid donationAmount provided")
        return handleInvalidRequest()

    countVal = int(count)
    totalPotAmountVal = int(totalPotAmount)
    donationVal = int(donationAmount)
    krakenSize = determineKrakenSize(totalPotAmountVal, countVal)

    if determineKrakenExplosion(totalPotAmountVal, countVal):
        try:
            explodedKraken = explodeKraken(krakenID, totalPotAmountVal, countVal)
            newBitsVal = explodedKraken['Attributes']['bitsAmount']
            countVal = explodedKraken['Attributes']['feedCount']
            krakenSize = determineKrakenSize(newBitsVal, countVal)
            addBits(broadcasterID, totalPotAmountVal)
            return sendExplodeUpdate(krakenID, krakenSize, donatorID, broadcasterID, donationAmount, totalPotAmount)
        except:
            logger.error("Failure during the kraken explosion")
            # Note that in this case the krakenSize and totalPotAmount are incorrect, so
            # we'll just pretend they are 0. It's possible another donation got in in between the previous explosion
            # and this donation, but treating this as the donation immediately after the explsion shouldn't
            # result in any weird behvaior.
            return sendFeedUpdate(krakenID, 0, donatorID, broadcasterID, donationAmount, 0)
    else:
        return sendFeedUpdate(krakenID, krakenSize, donatorID, broadcasterID, donationAmount, totalPotAmount)

# Helper method to add bits to an account
def addBits(tuid, bitVal):
    item = BITS_TABLE.get_item(Key={TUID_PARAM: tuid})
    logger.info("Attempting to add " + str(bitVal) + " bits to tuid " +  tuid)
    # An existing item will have 'Item' as part of the returned dictionary
    # No 'Item' means no entry in DynamoDB
    if item == None or item == {} or 'Item' not in item:
        logger.info("New item being added to table for user " + tuid)
        BITS_TABLE.put_item(
           Item={
                TUID_PARAM: tuid,
                BITS_PARAM: bitVal
            }
        )
    # Otherwise, the item already exists, just update it
    else:
        logger.info("Updating existing table for user " + tuid)
        BITS_TABLE.update_item(
            Key={TUID_PARAM: tuid},
            UpdateExpression='SET bits = bits + :val1',
            ExpressionAttributeValues={
                ':val1': bitVal
            }
        )

# Helper method to explode the Kraken on the front-end
def sendExplodeUpdate(krakenID, krakenSize, winningUser, winningBroadcaster, donationAmount, totalPotAmount):
    broadcastersToNotify = []
    krakenBroadcasterQueryResponse = BROADCASTER_KRAKEN_TABLE.query(
        IndexName='kraken_id-index',
        KeyConditionExpression=Key('kraken_id').eq(krakenID)
    )
    for item in krakenBroadcasterQueryResponse['Items']:
        broadcastersToNotify.append(item['br_id'])

    for broadcasterID in broadcastersToNotify:
        sendPubsub(broadcasterID, krakenID, winningUser, donationAmount, krakenSize, True, winningBroadcaster, totalPotAmount)
    return handleHappyResponse()

# Helper methpd to update the front-end when the Kraken does not explode
def sendFeedUpdate(krakenID, krakenSize, userID, broadcasterID, donationAmount, totalPotAmount):
    sendPubsub(broadcasterID, krakenID, userID, donationAmount, krakenSize, False, broadcasterID, totalPotAmount)
    return handleHappyResponse()

# Helper method to notify the front-end
def sendPubsub(recipientChannel, krakenID, donatingUser, donationAmount, krakenSize, hasExploded, donationChannel, totalPotAmount):
    message = {
        "krakenID": str(krakenID),
        "krakenSize": str(krakenSize),
        "donatingUser": str(donatingUser),
        "donationAmount": str(donationAmount),
        "hasExploded": hasExploded,
        "donationChannel": str(donationChannel),
        "totalPotAmount": str(totalPotAmount),
        "userAvatar": str(getUserAvatar(donatingUser))
    }
    
    print(message)

    # Post the pubsub
    headers = {"Client-Id": CLIENT_ID, "Content-Type": "application/json", "Authorization": "Bearer {}".format(createJWT(donatingUser, recipientChannel))}
    payload = {"content_type":"application/json", "message": json.dumps(message), "targets":["broadcast"]}
    url = "https://api.twitch.tv/extensions/message/{}".format(recipientChannel)
    r =  requests.post(url, headers=headers, json=payload)
    
    # TODO not sure how we want to handle an error here.
    # Logging it for now
    if r.status_code != 204:
        logger.info("Failed to send Pubsub message: {}".format(r.text))

# Helper method to construct a JWT
def createJWT(userID, channelID):
    payload = {
        "exp": datetime.datetime.now() + datetime.timedelta(minutes=5),
        "user_id": userID,
        "role": "external",
        "channel_id": channelID,
        "pubsub_perms": {
            "send":[
                "*"
            ]
        }
    }
    return jwt.encode(payload, base64.b64decode(OVERLAY_SECRET), algorithm='HS256')

# Helper method to return a 200
def handleHappyResponse():
    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': 'Successfully notified extension!'
    }

# Helper method for a 400 response
def handleFailureResponse(message):
    return {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': message })
    }

# Helper method for a 400 response because of the Kraken explosion
def handleInvalidExplosion():
    return handleFailureResponse('Failure during Kraken explosion')

# Helper method for a 400 response because of the input
def handleInvalidRequest():
    return handleFailureResponse('Not all required parameters provided')

# Default behavior between SNS and API Gateway Calls
def commonBehavior(event):
    # Ensure required data is passed in
    if (TOTAL_POT_PARAM not in event or 
        DONATOR_PARAM not in event or 
        KRAKEN_PARAM not in event or 
        DONATION_PARAM not in event or 
        BROADCASTER_PARAM not in event):
        return handleInvalidRequest()
    return handleQuery(event)

# Helper method to convert a string into a useable dictionary
def convertToDictionary(someString):
    json_acceptable_string = someString.replace("'", "\"")
    return json.loads(json_acceptable_string)

# Main handler
def lambda_handler(event, context):
    logger.info("Received event {}".format(event))
    # Check if SNS
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                sns = record['Sns']
                # Message is wrongfully read as a string
                json_dict = convertToDictionary(sns['Message'])
                # Get the message body
                if 'body' in json_dict:
                    # Body is wrongfully read as a string
                    return commonBehavior(convertToDictionary(json_dict['body']))   
    # Otherwise, assume API gateway
    return commonBehavior(event)
