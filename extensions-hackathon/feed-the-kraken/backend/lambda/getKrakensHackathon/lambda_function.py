import logging, json, boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

BROADCASTER_KEY = 'br_id'
KRAKEN_KEY = "kraken_id"

DYNAMODB = boto3.resource('dynamodb')
TABLE = DYNAMODB.Table('krakenHackathon')

class InvalidRequestException(Exception): pass

def handleQuery(event):

    response = TABLE.scan()
    
    krakenJSON = {"krakens": []}
    
    for item in response['Items']:
        kraken = {
            "id": str(item['id']),
            "bitsAmount": str(item['bitsAmount']),
            "lastExplodeTime": str(item['lastExplodeTime']),
            "feedCount": str(item['feedCount']),
            "name": str(item['name'])
        }
        krakenJSON["krakens"].append(kraken)
    
    return handleHappyResponse(krakenJSON)

# Helper method to return a valid response with the updated allowance
def handleHappyResponse(message):
    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps(message)
    }

# Helper method for a 400 response
def handleInvalidRequest(message):
    raise InvalidRequestException('Invalid Request: {}'.format(message))

# Default behavior between SNS and API Gateway Calls
def commonBehavior(event):
    # Ensure required data is passed in
    # Currently no required params
    return handleQuery(event)

# Main handler
def lambda_handler(event, context):
    # Check if SNS
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                sns = record['Sns']
                message = sns['Message']
                # Message is wrongfully read as a string
                json_acceptable_string = message.replace("'", "\"")
                json_dict = json.loads(json_acceptable_string)
                return commonBehavior(json_dict)                
    # Otherwise, assume API gateway
    return commonBehavior(event)

