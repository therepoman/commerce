import logging, json, boto3, random
from boto3.dynamodb.conditions import Key, Attr

logger = logging.getLogger()
logger.setLevel(logging.INFO)

DEVELOPER_PARAM = 'developer'
EXTENSION_PARAM = 'extension'
ENTITLEMENT_PARAM = 'entitlement'
DELIMITER = ":"
DEV_PRIMARY_KEY = 'developer:extension'

DYNAMODB = boto3.resource('dynamodb')
DEV_ENTITLEMENT_TABLE = DYNAMODB.Table('DeveloperEntitlementHackathon')

def handleQuery(event):
    entitlement =  event[ENTITLEMENT_PARAM]
    developer = event[DEVELOPER_PARAM]
    extension = event[EXTENSION_PARAM]

    if entitlement == None or entitlement == '':
        logger.error("No valid entitlement provided")
        return handleInvalidRequest()
    if developer == None or developer == '':
        logger.error("No valid developer provided")
        return handleInvalidRequest()
    if extension == None or extension == '':
        logger.error("No valid extension provided")
        return handleInvalidRequest()

    # Primary Key is developer:extension
    primaryKey = developer + DELIMITER + extension
    logger.info("New item being added to table")
    DEV_ENTITLEMENT_TABLE.put_item(
       Item={
            DEV_PRIMARY_KEY: primaryKey,
            ENTITLEMENT_PARAM: entitlement
        }
    )

    return handleHappyResponse()

# Helper method to return a valid response with the new entitlement
def handleHappyResponse():
    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': ""
    }

# Helper method for a 400 response
def handleInvalidRequest():
    return {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': 'Not all required parameters provided' })
    }

# Main handler
def lambda_handler(event, context):
    # Ensure query params are provided
    if ENTITLEMENT_PARAM not in event or DEVELOPER_PARAM not in event or EXTENSION_PARAM not in event:
        return handleInvalidRequest()
    return handleQuery(event)
