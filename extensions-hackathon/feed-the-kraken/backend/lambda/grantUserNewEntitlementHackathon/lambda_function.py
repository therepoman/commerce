import logging, json, boto3, random, json
from boto3.dynamodb.conditions import Key, Attr

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TUID_PARAM = 'tuid'
DEVELOPER_PARAM = 'developer'
EXTENSION_PARAM = 'extension'
ENTITLEMENT_PARAM = 'entitlement'
DELIMITER = ":"
DEV_PRIMARY_KEY = 'developer:extension'
TUID_SORT_KEY = 'developer:extension:entitlement'

DYNAMODB = boto3.resource('dynamodb')
DEV_ENTITLEMENT_TABLE = DYNAMODB.Table('DeveloperEntitlementHackathon')
CUSTOMER_ENTITLEMENT_TABLE = DYNAMODB.Table('CustomerEntitlementHackathon')

def handleQuery(event):
    tuid =  event[TUID_PARAM]
    developer = event[DEVELOPER_PARAM]
    extension = event[EXTENSION_PARAM]

    if tuid == None or tuid == '':
        logger.error("No valid tuid provided")
        return handleInvalidRequest()
    if developer == None or developer == '':
        logger.error("No valid developer provided")
        return handleInvalidRequest()
    if extension == None or extension == '':
        logger.error("No valid extension provided")
        return handleInvalidRequest()

    # Obtain a list of all possible entitlements
    allPossibleEntitlements = getRelevantEntitlements(developer, extension)
    if len(allPossibleEntitlements) == 0:
        logger.error("No valid entitlements exist for developer " + developer + " and extension " + extension)
        return handleInvalidParameter()

    # Obtain the list of all entitlemnets for the customer
    existingCustomerEntitlements = getExistingRelevantEntitlementsForCustomer(tuid, developer, extension)

    # Get the subset of possible entitlements the customer does not already have
    possibleChoices = getNewEntitlementsForCustomer(allPossibleEntitlements, existingCustomerEntitlements)
    if len(possibleChoices) == 0:
        logger.error("Customer " + tuid + " already owns all entitlements for extension " + extension)
        return handleInvalidParameter()

    # Choose an entitlement and give it to the customer
    chosenEntitlement = random.choice(possibleChoices)
    sortKey = developer + DELIMITER + extension + DELIMITER + chosenEntitlement
    grantUserEntitlement(tuid, sortKey)
    return handleHappyResponse(tuid, chosenEntitlement)

# Helper method to add an entitlement to the customer
def grantUserEntitlement(tuid, sortKey):
    logger.info("New item being added to table for user " + tuid)
    CUSTOMER_ENTITLEMENT_TABLE.put_item(
       Item={
            TUID_PARAM: tuid,
            TUID_SORT_KEY: sortKey
        }
    )

# Helper method to get all new entitlement the customer does not already have:
def getNewEntitlementsForCustomer(allPossibleEntitlements, existingCustomerEntitlements):
    newEntitlements = []
    for newEntitlement in allPossibleEntitlements:
        if newEntitlement[ENTITLEMENT_PARAM] not in existingCustomerEntitlements:
            newEntitlements.append(newEntitlement[ENTITLEMENT_PARAM])
    return newEntitlements

# Helper method to obtain all entitlements the customer already has, filtered down to the given developer and extension
def getExistingRelevantEntitlementsForCustomer(tuid, developer, extension):
    # The primary key for CustomerEntitlementHackathon is tuid
    tuidEntitlementResponse = CUSTOMER_ENTITLEMENT_TABLE.query(KeyConditionExpression=Key(TUID_PARAM).eq(tuid))
    tuidEntitlements = tuidEntitlementResponse['Items']
    existingEntitlements = {}
    # Filter for only relevant items
    if len(tuidEntitlements) > 0:
        for singleEntitlement in tuidEntitlements:
            # Relevant items have a sort key with the developer:extension: combination
            match = developer + DELIMITER + extension + DELIMITER
            if match in singleEntitlement[TUID_SORT_KEY]:
                split = singleEntitlement[TUID_SORT_KEY].split(match)
                # Sanity check to ensure we don't crash
                if len(split) != 2:
                    continue
                existingEntitlements[split[1]] = True
    return existingEntitlements

# Helper method to obtain all entitlements for the developer:extension combination
def getRelevantEntitlements(developer, extension):
    # The primary key for DeveloperEntitlementHackathon is developer:extension 
    devKey = developer + DELIMITER + extension
    devEntitlementResponse = DEV_ENTITLEMENT_TABLE.query(KeyConditionExpression=Key(DEV_PRIMARY_KEY).eq(devKey))
    return devEntitlementResponse['Items']

# Helper method to return a valid response with the new entitlement
def handleHappyResponse(tuid, entitlement):
    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({TUID_PARAM : tuid, ENTITLEMENT_PARAM : entitlement})
    }

# Helper method for a 400 response because of an invalid parameter
def handleInvalidParameter():
    return {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': 'Invalid parameter provided' })
    }

# Helper method for a 400 response
def handleInvalidRequest():
    return {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': 'Not all required parameters provided' })
    }

# Default behavior between SNS and API Gateway Calls
def commonBehavior(event):
    # Ensure query params are provided
    if TUID_PARAM not in event or DEVELOPER_PARAM not in event or EXTENSION_PARAM not in event:
        return handleInvalidRequest()
    return handleQuery(event)

# Main handler
def lambda_handler(event, context):
    # Check if SNS
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                sns = record['Sns']
                message = sns['Message']
                # Message is wrongfully read as a string
                json_acceptable_string = message.replace("'", "\"")
                json_dict = json.loads(json_acceptable_string)
                return commonBehavior(json_dict)                
    # Otherwise, assume API gateway
    return commonBehavior(event)

