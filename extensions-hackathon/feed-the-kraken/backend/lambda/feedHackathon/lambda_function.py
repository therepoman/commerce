import boto3, logging, json, time, random, datetime, jwt, requests, base64
from boto3.dynamodb.conditions import Key

logger = logging.getLogger()
logger.setLevel(logging.INFO)

SNS_ARN = 'arn:aws:sns:us-west-2:703507011456:consumeBitsHackathon'
SNS = boto3.client('sns')
DYNAMODB = boto3.resource('dynamodb')
KRAKEN_TABLE = DYNAMODB.Table('broadcasterKrakenMappingHackathon')

TUID_PARAM = 'tuid'
BITS_PARAM = 'bits'
KRAKEN_PARAM = 'kraken'
BROADCASTER_PARAM = 'broadcaster'
BITS_AMOUNT = 'bits_amount'
BODY_PARAM = 'body'
HEADERS_PARAM = 'headers'

# TODO: Move out of code and then rotate
SECRET = "8q1lekDBOlV1tzYeG+leOrLLJCubaSG7AzS+7e1rolU="

def returnFailure(message):
    return {
        'statusCode': 400,
        HEADERS_PARAM: { 'Content-Type': 'application/json' },
        BODY_PARAM: json.dumps({'Error': message}),
    }

def lambda_handler(event, context):
    logger.info('Received event {}'.format(event))

    # Verify correct request form
    if (BODY_PARAM not in event or HEADERS_PARAM not in event):
        return returnFailure('Invalid request')
    request_body = event[BODY_PARAM]
    request_headers = event[HEADERS_PARAM]

    # Verify JWT provided
    if ('Authorization' not in request_headers):
        return returnFailure('No auth provided.')
    jwt_payload = getJWTPayload(request_headers['Authorization'])
    if ('user_id' not in jwt_payload or jwt_payload['user_id'] == None or jwt_payload['user_id'] == ""):
        return returnFailure('No auth provided.')
    user_id = jwt_payload['user_id']
    broadcaster_id = jwt_payload['channel_id']

    # Verify request has all required parameters
    if (BITS_AMOUNT not in request_body or 
        request_body[BITS_AMOUNT] == None or
        user_id == None or
        broadcaster_id == None):
        return returnFailure('Invalid request, missing required parameters.')
    logger.info('Handling event for user ' + user_id)
    return feedKraken(user_id, broadcaster_id, int(request_body[BITS_AMOUNT]))

# Helper method to decode the JWT token
def getJWTPayload(encoded):
    return jwt.decode(encoded, base64.b64decode(SECRET), algorithms=['HS256'])
    
def feedKraken(user_id, broadcaster_id, bits_amount):
    # Determine kraken to feed
    kraken_id = getKrakenForBroadcaster(broadcaster_id)
    if kraken_id == None:
        return returnFailure('No kraken linked to channel.')
    logger.info('Feeding Kraken for ' + kraken_id)
    # Take bits from user (SNS message to start next step)
    return callConsumeBits(user_id, broadcaster_id, kraken_id, bits_amount)

# Helper method to publish an SNS message for consuming bits
def callConsumeBits(user_id, broadcaster_id, kraken_id, bits_amount):
    logger.info('Calling SNS to consume bits')
    message = {
        'statusCode': 200,
        HEADERS_PARAM: { 'Content-Type': 'application/json' },
        BODY_PARAM: json.dumps({TUID_PARAM : str(user_id), BITS_PARAM : str(bits_amount), BROADCASTER_PARAM : str(broadcaster_id), KRAKEN_PARAM : str(kraken_id)})
    }
    SNS.publish(
        TargetArn=SNS_ARN,
        Message=json.dumps({'default': json.dumps(message)}),
        Subject='Consume Bits',
        MessageStructure='json'
    )
    # Sanitize the message so the front end isn't leaked info
    message[BODY_PARAM] = json.dumps("Successful Call!")
    return message

# Helper method to obtain the Kraken for a given broadcast id
def getKrakenForBroadcaster(broadcaster_id):
    krakenBroadcasterTableResponse = KRAKEN_TABLE.get_item(
        Key={'br_id': broadcaster_id}
    )
    if (krakenBroadcasterTableResponse == None or 
        krakenBroadcasterTableResponse == {} or 
        'Item' not in krakenBroadcasterTableResponse or
        'kraken_id' not in krakenBroadcasterTableResponse['Item']):
        return None
    else:
        return krakenBroadcasterTableResponse['Item']['kraken_id']
