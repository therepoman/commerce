import logging, json, boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TUID_PARAM = 'tuid'
BITS_PARAM = 'bits'
KRAKEN_PARAM = 'kraken'
BROADCASTER_PARAM = 'broadcaster'
SNS_ARN = 'arn:aws:sns:us-west-2:703507011456:completedConsumeBits'
SNS = boto3.client('sns')
DYNAMODB = boto3.resource('dynamodb')
TABLE = DYNAMODB.Table('HackathonBitsAllowance')

def handleQuery(event):
    tuid =  event[TUID_PARAM]
    bits = event[BITS_PARAM]
    kraken = event[KRAKEN_PARAM]
    broadcaster = event[BROADCASTER_PARAM]
    if tuid == None or tuid == '':
        logger.error("No valid tuid provided")
        return handleInvalidRequest()
    if bits == None or not bits.isdigit():
        logger.error("No valid bits amount provided")
        return handleInvalidRequest()
    if kraken == None or kraken == '':
        logger.error("No valid kraken provided")
        return handleInvalidRequest()
    if broadcaster == None or broadcaster == '':
        logger.error("No valid broadcaster provided")
        return handleInvalidRequest()

    bitVal = int(bits)
    item = TABLE.get_item(Key={TUID_PARAM: tuid})

    # An existing item will have 'Item' as part of the returned dictionary
    # No 'Item' means no entry in DynamoDB
    if item == None or item == {} or 'Item' not in item:
        logger.error("Tuid does not exist in table")
        return handleInvalidBits()
    # Otherwise, the item already exists, so we can update
    else:
        logger.info("Attempting to update existing table for user " + tuid)
        try:
            TABLE.update_item(
                Key={TUID_PARAM: tuid},
                UpdateExpression="SET bits = bits - :bitVal",
                ConditionExpression="bits >= :bitVal",
                ExpressionAttributeValues={
                    ':bitVal': bitVal
                },
                ReturnValues="UPDATED_NEW"
            )
            return handleHappyResponse(tuid, bitVal, kraken, broadcaster)
        except ClientError as e:
            if e.response['Error']['Code'] == "ConditionalCheckFailedException":
                return handleInvalidBits()
            else:
                return handleInvalidRequest()
        except:
            return handleInvalidRequest()

# Helper method to publish to SNS and return a response
def returnResponseAndPushToSNS(message):
    logger.info("Posting SNS message")
    SNS.publish(
        TargetArn=SNS_ARN,
        Message=json.dumps({'default': json.dumps(message)}),
        Subject='Successful Consume Bits Response',
        MessageStructure='json'
    )
    return message

# Helper method to return a valid response with the updated allowance
def handleHappyResponse(tuid, newAllowance, kraken, broadcaster):
    message = {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({
            TUID_PARAM : str(tuid),
            BITS_PARAM : str(newAllowance),
            KRAKEN_PARAM : str(kraken),
            BROADCASTER_PARAM : str(broadcaster)
        })
    }
    return returnResponseAndPushToSNS(message)

# Helper method to 400's
def handleCommonInvalidRequest(message):
    return {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': message })
    }

# Helper method for a 400 response
def handleInvalidRequest():
    return handleCommonInvalidRequest('Not all required parameters provided')

# Helper method for a 400 response because of bits
def handleInvalidBits():
    return handleCommonInvalidRequest('Not enough bits for given customer')

# Default behavior between SNS and API Gateway Calls
def commonBehavior(event):
    # Ensure required data is passed in
    if (TUID_PARAM not in event or 
        BITS_PARAM not in event or 
        KRAKEN_PARAM not in event or 
        BROADCASTER_PARAM not in event):
        return handleInvalidRequest()
    return handleQuery(event)

# Helper method to convert a string into a useable dictionary
def convertToDictionary(someString):
    json_acceptable_string = someString.replace("'", "\"")
    return json.loads(json_acceptable_string)

# Main handler
def lambda_handler(event, context):
    logger.info("Received event {}".format(event))
    # Check if SNS
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                sns = record['Sns']
                # Message is wrongfully read as a string
                json_dict = convertToDictionary(sns['Message'])
                # Get the message body
                if 'body' in json_dict:
                    # Body is wrongfully read as a string
                    return commonBehavior(convertToDictionary(json_dict['body']))   
    # Otherwise, assume API gateway
    return commonBehavior(event)


