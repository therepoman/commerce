import logging, json, boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TUID_PARAM = 'tuid'
BITS_PARAM = 'bits'
SNS_ARN = 'arn:aws:sns:us-west-2:703507011456:completedAddBits'
SNS = boto3.client('sns')
DYNAMODB = boto3.resource('dynamodb')
TABLE = DYNAMODB.Table('HackathonBitsAllowance')

def handleQuery(event):
    tuid =  event[TUID_PARAM]
    bits = event[BITS_PARAM]
    if tuid == None or tuid == '':
        logger.error("No valid tuid provided")
        return handleInvalidRequest()
    if bits == None or not bits.isdigit():
        logger.error("No valid bit amount provided")
        return handleInvalidRequest()
        
    bitVal = int(bits)
    item = TABLE.get_item(Key={TUID_PARAM: tuid})
    logger.info("Attempting to add " + bits + " bits to tuid " +  tuid)
    # An existing item will have 'Item' as part of the returned dictionary
    # No 'Item' means no entry in DynamoDB
    if item == None or item == {} or 'Item' not in item:
        logger.info("New item being added to table for user " + tuid)
        TABLE.put_item(
           Item={
                TUID_PARAM: tuid,
                BITS_PARAM: bitVal
            }
        )
        return handleHappyResponse(tuid, bitVal)
    # Otherwise, the item already exists, just update it
    else:
        logger.info("Updating existing table for user " + tuid)
        newVal = int(item['Item'][BITS_PARAM]) + bitVal
        TABLE.update_item(
            Key={TUID_PARAM: tuid},
            UpdateExpression='SET bits = :val1',
            ExpressionAttributeValues={
                ':val1': newVal
            }
        )
        return handleHappyResponse(tuid, newVal)

# Helper method to publish to SNS and return a response
def returnResponseAndPushToSNS(message):
    SNS.publish(
        TargetArn=SNS_ARN,
        Message=json.dumps({'default': json.dumps(message)}),
        Subject='Add Bits Response',
        MessageStructure='json'
    )
    return message

# Helper method to return a valid response with the updated allowance
def handleHappyResponse(tuid, newAllowance):
    message = {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({TUID_PARAM : tuid, BITS_PARAM : newAllowance})
    }
    return returnResponseAndPushToSNS(message)

# Helper method for a 400 response
def handleInvalidRequest():
    message = {
        'statusCode': 400,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({ 'Error': 'Not all required parameters provided' })
    }
    return returnResponseAndPushToSNS(message)
    
# Default behavior between SNS and API Gateway Calls
def commonBehavior(event):
    # Ensure required data is passed in
    if TUID_PARAM not in event or BITS_PARAM not in event:
        return handleInvalidRequest()
    return handleQuery(event)

# Main handler
def lambda_handler(event, context):
    # Check if SNS
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                sns = record['Sns']
                message = sns['Message']
                # Message is wrongfully read as a string
                json_acceptable_string = message.replace("'", "\"")
                json_dict = json.loads(json_acceptable_string)
                return commonBehavior(json_dict)                
    # Otherwise, assume API gateway
    return commonBehavior(event)

