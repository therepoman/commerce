import logging, json, boto3, jwt, base64

logger = logging.getLogger()
logger.setLevel(logging.INFO)

USER_KEY = 'user'
DYNAMODB = boto3.resource('dynamodb')
TABLE = DYNAMODB.Table('KrakenAvatarHackathon')

BODY_PARAM = 'body'
HEADERS_PARAM = 'headers'

# TODO: Move out of code and then rotate
SECRET = "8q1lekDBOlV1tzYeG+leOrLLJCubaSG7AzS+7e1rolU="

class InvalidRequestException(Exception): pass

def handleQuery(event):
    # Verify correct request form
    if (BODY_PARAM not in event or HEADERS_PARAM not in event):
        return handleInvalidRequest('Request received inproperly formatted.')
    request_body = event[BODY_PARAM]
    request_headers = event[HEADERS_PARAM]

    # Verify JWT provided
    if ('Authorization' not in request_headers):
        return handleInvalidRequest('Missing Authorization header.')
    jwtPayload = getJWTPayload(request_headers['Authorization'])
    
    if 'user_id' not in jwtPayload or jwtPayload['user_id'] == "":
        return handleInvalidRequest('Missing user_id from jwt')

    user = jwtPayload['user_id']
    if user == None or user == '':
        logger.error("No valid user provided")
        return handleInvalidRequest("No valid user provided")

    item = TABLE.get_item(Key={USER_KEY: user})
    request_body['user'] = user

    # An existing item will have 'Item' as part of the returned dictionary
    # No 'Item' means no entry in DynamoDB
    if item == None or item == {} or 'Item' not in item:
        logger.info("New item being added to table for user " + user)
        TABLE.put_item(Item=request_body)
        return handleHappyResponse(request_body)
    # Otherwise, the item already exists, just update it
    else:
        logger.info("Updating existing table for user " + user)
        newDict = request_body
        currentItem = item['Item']
        for param in currentItem:
            if param not in newDict:
                newDict[param] = currentItem[param]
        TABLE.put_item(Item=newDict)
        return handleHappyResponse(newDict)

# Helper method to return a valid response with the updated allowance
def handleHappyResponse(fullAvatar):
    
    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps(fullAvatar)
    }

# Helper method for a 400 response
def handleInvalidRequest(message):
    raise InvalidRequestException('Invalid Request: {}'.format(message))

# Helper method to decode the JWT token
def getJWTPayload(encoded):
    return jwt.decode(encoded, base64.b64decode(SECRET), algorithms=['HS256'])

# Main handler
def lambda_handler(event, context):
    return handleQuery(event)

