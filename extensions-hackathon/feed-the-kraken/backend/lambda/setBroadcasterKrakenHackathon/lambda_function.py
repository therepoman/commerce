import logging, json, boto3, jwt, base64

logger = logging.getLogger()
logger.setLevel(logging.INFO)

BROADCASTER_KEY = 'br_id'
KRAKEN_KEY = "kraken_id"

BODY_PARAM = 'body'
HEADERS_PARAM = 'headers'

DYNAMODB = boto3.resource('dynamodb')
TABLE = DYNAMODB.Table('broadcasterKrakenMappingHackathon')

# TODO: Move out of code and then rotate
SECRET = "8q1lekDBOlV1tzYeG+leOrLLJCubaSG7AzS+7e1rolU="

class InvalidRequestException(Exception): pass

def handleQuery(event):
    # Verify correct request form
    if (BODY_PARAM not in event or HEADERS_PARAM not in event):
        return handleInvalidRequest('Request recieved improperly formatted.')
    request_body = event[BODY_PARAM]
    request_headers = event[HEADERS_PARAM]
    
    # Verify JWT provided
    if ('Authorization' not in request_headers):
        return handleInvalidRequest('Missing Authorization header.')
    jwtPayload = getJWTPayload(request_headers['Authorization'])
    
    if 'user_id' not in jwtPayload or jwtPayload['user_id'] == "":
        return handleInvalidRequest('Missing user_id from jwt')
    if (jwtPayload['user_id'] != jwtPayload['channel_id']):
        return handleInvalidRequest('Incorrect acting user from jwt.')

    broadcasterID = jwtPayload['user_id']
    
    # If they didn't send a kraken_id, just return the entry of that user
    # Ideally we would make a get api but this is faster and will unblock forntend
    if KRAKEN_KEY not in request_body or request_body[KRAKEN_KEY] == None or request_body[KRAKEN_KEY] == '':
        item = TABLE.get_item(Key={BROADCASTER_KEY: broadcasterID})
        return handleHappyResponse(item['Item'])

    item = TABLE.get_item(Key={BROADCASTER_KEY: broadcasterID})
    
    krakenID = request_body[KRAKEN_KEY]
    updatedEntry = {BROADCASTER_KEY: broadcasterID, KRAKEN_KEY: krakenID}

    TABLE.put_item(Item=updatedEntry)
    return handleHappyResponse(updatedEntry)

# Helper method to decode the JWT token
def getJWTPayload(encoded):
    return jwt.decode(encoded, base64.b64decode(SECRET), algorithms=['HS256'])

# Helper method to return a valid response with the updated allowance
def handleHappyResponse(message):
    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps(message)
    }

# Helper method for a 400 response
def handleInvalidRequest(message):
    raise InvalidRequestException('Invalid Request: {}'.format(message))
    
# Main handler
def lambda_handler(event, context):
    return handleQuery(event)

