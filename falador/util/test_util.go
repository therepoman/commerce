package util

import (
	"sync"
	"sync/atomic"
	"time"
)

// LoadRunner runs tasks concurrently with optional setup and cleanup for each thread.
type LoadRunner struct {
	Concurrency          int
	TestDuration         time.Duration
	Task                 func([]interface{})
	ThreadContextSetup   func(index int) []interface{}
	ThreadContextCleanup func(index int, parameters []interface{})
	Invocations          int64
}

// NewLoadRunner constructs a new LoadRunner.
func NewLoadRunner(concurrency int, testDuration time.Duration, task func([]interface{})) *LoadRunner {
	return &LoadRunner{
		Concurrency:  concurrency,
		TestDuration: testDuration,
		Task:         task,
	}
}

// Run runs the tasks.
func (loadRunner *LoadRunner) Run() {
	testEndTime := time.Now().Add(loadRunner.TestDuration)
	var waitgroup sync.WaitGroup
	for i := 0; i < loadRunner.Concurrency; i++ {
		waitgroup.Add(1)
		go func(i int) {
			defer waitgroup.Done()
			var threadParameters []interface{}
			if loadRunner.ThreadContextSetup != nil {
				threadParameters = loadRunner.ThreadContextSetup(i)
			}
			for time.Now().Before(testEndTime) {
				loadRunner.Task(threadParameters)
				atomic.AddInt64(&loadRunner.Invocations, 1)
			}
			if loadRunner.ThreadContextCleanup != nil {
				loadRunner.ThreadContextCleanup(i, threadParameters)
			}
		}(i)
	}
	waitgroup.Wait()
}

// GetInvocations returns the number of times the task was invoked.
func (loadRunner *LoadRunner) GetInvocations() int64 {
	return atomic.LoadInt64(&loadRunner.Invocations)
}
