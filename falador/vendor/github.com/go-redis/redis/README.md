This repository is a mirror of [github.com/go-redis/redis/v7](https://github.com/go-redis/redis) with the following changes:

## Forwarding Shim

The top-level module is a forwarding shim to the `v7` directory. This offers compatibility for using v7 in a `dep` project because it is not [module-aware](https://github.com/golang/dep/pull/1963).

Looking at https://github.com/go-redis/redis, the import paths are `github.com/go-redis/redis/v7`, and the directory path is `github.com/go-redis/redis`, which breaks `dep`.

The forwarding shim is generated with [goforward](https://go-review.googlesource.com/c/tools/+/137076/). 

## Connection Pool Patch

https://github.com/go-redis/redis/pull/1120 is applied to improve availabilty and latency when the redis cluster is under memory pressure (evicting).
