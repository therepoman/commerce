# Go Authorization

Go library for generating, verifying, and validating authorization tokens


# Token Generation

First initialize an encoder with an algorithm, issuer, and raw byte secret.

```
raw, err := ioutil.ReadFile("path/to/private/key")

encoder, err := goauthorization.NewEncoder("RS256", "code.justin.tv/web/cartman", raw)
```

Valid algorithms and expected secret/key types:

* `HS512`: Signs tokens with HMAC using the provided secret key string
* `RS256`: Signs tokens with RSA using the provided path to a RSA private key
* `ES256`: Signs tokens with ECC using the provided path to an ECC private key

To generate token:

```
capabilityClaim := map[string]goauthorization.CapValue{"channel": "dathass"}
capabilities := map[string]goauthorization.CapabilityClaim{"spectre::edit_channel": capabilityClaim}
token := encoder.Encode(goauthorization.TokenParams{
  Exp: time.Now(),
  Nbf: time.Now(),
  Aud: []string{"code.justin.tv/web/web"},
  Sub: "dianers",
  Claims: capabilities,
})
```

Serialized string of token:

```
tokenString, err := token.String()
```

# Token Verification and Validation

Initialize a decoder with an algorithm, strings for the expected audience and issuer, and raw byte secret.

```
raw, err := ioutil.ReadFile("path/to/public/key")

decoder, err := goauthorization.NewDecoder("RS256", "code.justin.tv/web/audience", "code.justin.tv/web/issuer", raw)
```

To extract and unmarshal a Cartman token set in the "Twitch-Authorization" header of an http request:

```
token, err := decoder.ParseToken(r)
```

You can also deserialize a token:

```
token, err := decoder.Decode(serialized)
```

To validate JWT tokens:

```
capabilities := goauthorization.CapabilityClaims{
  "channel" : goauthorization.CapabilityClaim{"channel_id": 123, "login": "dianers"}
}
err := decoder.Validate(token, capabilities)
```

# Related Repositories

Cartman: https://git-aws.internal.justin.tv/web/cartman

Cartman-shim: https://git-aws.internal.justin.tv/web/cartman-shim

Cartman-configs: https://git-aws.internal.justin.tv/web/cartman-configs

Cartman-mock: https://git-aws.internal.justin.tv/web/cartman-mock
