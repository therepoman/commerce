//Package jwt implements encoding and decoding of JSON objects as defined in RFC 7519.
//
//A JWT is represented by 3 fields: A header JSON object, specifying information about the JWT itself, a claims JSON object
//specifying a series of statements and a signature that validates the token. These are dot separated and encoded in base64url (see URLEncoding of encoding/base64).
//
//This package is essentially the JSON package with some seasoning on top, it uses all the struct label logic from the encoding/json
//stdlib package.
package jwt

//Header is an example JWT header. Check RFC 7519 for other fields
//that are useful in a header
type Header struct {
	Algorithm string `json:"alg"`
	Type      string `json:"typ"`
}

//NewHeader returns a new Header where the Algorithm is set to the Name of the Algorithm parameter, and the Type is "jwt".
func NewHeader(a Algorithm) (h Header) { h.Type = "JWT"; h.Algorithm = a.Name(); return }

//ValidateEqual returns ErrInvalidHeader if h1 is not equal to h2.
func (h1 Header) ValidateEqual(h2 Header) (err error) {
	if h1 != h2 {
		err = ErrInvalidHeader
	}
	return
}

//Algorithm represents an algorithm that can be used to
//validate or sign a JWT.
type Algorithm interface {
	// Sign signs the given byte value.
	Sign(value []byte) (signature []byte, err error)
	//Validate validates the signature for the given byte value.
	Validate(value, signature []byte) error
	// Size returns the size of this Algorithm's signature in bytes.
	Size() int
	// Name returns the RFC 7518 ("JSON Web Algorithms") compliant name of this Algorithm.
	Name() string
}

//None is a no-op Algorithm. The signature is empty.
var None Algorithm = none{}

type none struct{}

func (none) Validate(_, _ []byte) error    { return nil }
func (none) Sign(_ []byte) ([]byte, error) { return []byte(""), nil }
func (none) Name() string                  { return "none" }
func (none) Size() int                     { return 0 }
