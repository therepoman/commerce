import argparse
import base64
import boto3
from botocore.exceptions import ClientError
import json
import logging
import re
import shutil
import os
from time import sleep

DEV_TEMPLATE_PATH = 'cloudformation/dev-pipeline-template.json'
PROD_TEMPLATE_PATH = 'cloudformation/prod-pipeline-template.json'
LAMBDA_FILE_PATH = 'cloudformation/lambda'
LIFECYCLE_LAMBDA_FILE_PATH = 'cloudformation/asg-lifecycle-lambda'
LIFECYCLE_LAMBDA_S3_KEY = 'asg-lifecycle-lambda.zip'
DEV_IMAGE_TAG = 'latest-dev'
ONEBOX_IMAGE_TAG = 'latest-onebox'
PROD_IMAGE_TAG = 'latest-prod'


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main():
    logging.basicConfig()

    args = parse_args()
    dev_session = get_session('dev', args.dev_profile_name, args.dev_odin_material_set)
    prod_session = get_session('prod', args.prod_profile_name, args.prod_odin_material_set)
    validator = ArgsValidator(args, dev_session, prod_session)
    if not validator.is_valid():
        return

    bootstrapper = ServiceBootstrapper(
        service_name=args.service_name,
        code_commit_branch=args.code_commit_branch,
        code_commit_repository=args.code_commit_repository,
        dev_vpc_id=args.dev_vpc_id,
        dev_vpc_security_group_id=args.dev_vpc_security_group_id,
        dev_vpc_subnet=args.dev_vpc_subnet,
        prod_vpc_id=args.prod_vpc_id,
        prod_vpc_security_group_id=args.prod_vpc_security_group_id,
        prod_vpc_subnet=args.prod_vpc_subnet,
        dev_service_role=args.dev_service_role_arn,
        prod_service_role=args.prod_service_role_arn,
        dev_session=dev_session,
        prod_session=prod_session
    )
    bootstrapper.bootstrap(args.skip_dev_bootstrap, args.skip_prod_bootstrap)


def parse_args():
    parser = argparse.ArgumentParser(description='Bootstrap a service using cloud formation.')
    parser.add_argument('--service-name', help='The name of the service.  This will be used to create stack and resource names.')
    parser.add_argument('--code-commit-branch', help='Branch name for code commit')
    parser.add_argument('--code-commit-repository', help='Repository name for code commit')
    parser.add_argument('--prod-vpc-id', help='Prod VPC id')
    parser.add_argument('--prod-vpc-security-group-id', help='Prod VPC security group id')
    parser.add_argument('--prod-vpc-subnet', nargs='+', help='Prod VPC subnet.  Can specify multiple separated by spaces.')
    parser.add_argument('--dev-vpc-id', help='Dev VPC id')
    parser.add_argument('--dev-vpc-security-group-id', help='Dev VPC security group id')
    parser.add_argument('--dev-vpc-subnet', nargs='+', help='Dev VPC subnet.  Can specify multiple separated by spaces.')

    parser.add_argument('--dev-service-role-arn', help='ARN of the IAM Role the DEV service will use to call AWS.')
    parser.add_argument('--prod-service-role-arn', help='ARN of the IAM Role the PROD service will use to call AWS.')

    parser.add_argument('--dev-profile-name', help='Profile name for dev account credentials, usually stored in ~/.aws/credentials')
    parser.add_argument('--dev-odin-material-set', help='Odin material set name for the dev account credentials.')
    parser.add_argument('--prod-profile-name', help='Profile name for prod account credentials, usually stored in ~/.aws/credentials')
    parser.add_argument('--prod-odin-material-set', help='Odin material set name for the prod account credentials.')

    parser.add_argument('--skip-dev-bootstrap', action='store_true', help='Skip creating the stack in the dev account using a bootstrap template.')
    parser.add_argument('--skip-prod-bootstrap', action='store_true', help='Skip creating the stack in the prod account using a bootstrap template.')

    return parser.parse_args()


class ArgsValidator:
    def __init__(self, args, dev_session, prod_session):
        self.args = args
        self.dev_session = dev_session
        self.prod_session = prod_session

    def is_valid(self):
        is_valid = True

        if not self.args.service_name:
            is_valid = False
            print('Missing --service-name parameter.')

        if not self.args.code_commit_branch:
            is_valid = False
            print('Missing --code-commit-branch parameter.')

        if not self.args.code_commit_repository:
            is_valid = False
            print('Missing --code-commit-repository parameter.')

        if not self.args.dev_service_role_arn:
            is_valid = False
            print('Missing --dev-service-role-arn parameter.')

        if not self.args.prod_service_role_arn:
            is_valid = False
            print('Missing --prod-service-role-arn parameter.')

        is_valid = self.is_vpc_valid(
            self.prod_session, self.args.prod_vpc_id, self.args.prod_vpc_security_group_id, self.args.prod_vpc_subnet,
            '--prod-vpc-id', '--prod-vpc-security-group-id', '--prod-vpc-subnet') and is_valid

        is_valid = self.is_vpc_valid(
            self.dev_session, self.args.dev_vpc_id, self.args.dev_vpc_security_group_id, self.args.dev_vpc_subnet,
            '--dev-vpc-id', '--dev-vpc-security-group-id', '--dev-vpc-subnet') and is_valid

        return is_valid

    def is_vpc_valid(self, session, vpc_id, security_group_id, subnets, vpc_id_arg, security_group_id_arg, subnet_arg):
        is_valid = True

        vpcs = set()
        if not vpc_id:
            is_valid = False
            print('Missing {} parameter.'.format(vpc_id_arg))
            for vpc in session.client('ec2').describe_vpcs()['Vpcs']:
                print('    {}'.format(vpc['VpcId']))
                vpcs.add(vpc['VpcId'])

        security_groups_by_vpc = {}
        if not security_group_id:
            is_valid = False
            print('Missing {} parameter.'.format(security_group_id_arg))
            for security_group in session.client('ec2').describe_security_groups()['SecurityGroups']:
                print('    {}: {} {}: {}'.format(security_group['GroupId'], security_group['VpcId'],
                    security_group['GroupName'], security_group['Description']))
                if security_group['VpcId'] not in security_groups_by_vpc:
                    security_groups_by_vpc[security_group['VpcId']] = []
                security_groups_by_vpc[security_group['VpcId']].append(security_group['GroupId'])

        subnets_by_vpc = {}
        if not subnets:
            is_valid = False
            print('Missing {} parameter.  More than one subnet can be used.'.format(subnet_arg))
            for subnet in session.client('ec2').describe_subnets()['Subnets']:
                print('    {}: {} {} {}'.format(subnet['SubnetId'], subnet['VpcId'], subnet['AvailabilityZone'],
                    ('MapPublicIpOnLaunch' if subnet['MapPublicIpOnLaunch'] else '')))
                if not subnet['MapPublicIpOnLaunch']:
                    if subnet['VpcId'] not in subnets_by_vpc:
                        subnets_by_vpc[subnet['VpcId']] = []
                    subnets_by_vpc[subnet['VpcId']].append(subnet['SubnetId'])

        examples = []
        for vpc in vpcs:
            subnets = subnets_by_vpc.get(vpc, [])
            if subnets:
                for security_group in security_groups_by_vpc.get(vpc, []):
                    examples.append('{} {} {} {} {} {}'.format(
                        vpc_id_arg, vpc, security_group_id_arg, security_group, subnet_arg, ' '.join(subnets)))

        if examples:
            print('Example:')
            for example in examples:
                print('    {}'.format(example))

        return is_valid


class ServiceBootstrapper:
    def __init__(self, service_name, code_commit_branch, code_commit_repository,
        dev_vpc_id, dev_vpc_security_group_id, dev_vpc_subnet,
        prod_vpc_id, prod_vpc_security_group_id, prod_vpc_subnet,
        dev_service_role, prod_service_role,
        dev_session, prod_session):
        self.service_name = service_name
        self.code_commit_branch = code_commit_branch
        self.code_commit_repository = code_commit_repository
        self.dev_vpc_id = dev_vpc_id
        self.dev_vpc_security_group_id = dev_vpc_security_group_id
        self.dev_vpc_subnet = dev_vpc_subnet
        self.prod_vpc_id = prod_vpc_id
        self.prod_vpc_security_group_id = prod_vpc_security_group_id
        self.prod_vpc_subnet = prod_vpc_subnet
        self.dev_session = dev_session
        self.prod_session = prod_session
        self.cloudformationdev = CloudFormationFacade(self.dev_session)
        self.cloudformationprod = CloudFormationFacade(self.prod_session)
        self.dev_account = self.dev_session.client('sts').get_caller_identity()['Account']
        self.prod_account = self.prod_session.client('sts').get_caller_identity()['Account']
        self.dev_service_role = dev_service_role
        self.prod_service_role = prod_service_role
        self.lambda_s3_bucket = '{0}-lambda-{1}'.format(self.service_name, self.prod_account)
        self.dev_stack_name = '{}-dev'.format(self.service_name)
        self.prod_stack_name = self.service_name

    def bootstrap(self, skip_dev_bootstrap, skip_prod_bootstrap):
        self.upload_lambda_functions()

        if skip_dev_bootstrap:
            dev_stack = self.cloudformationdev.describe_stack(self.dev_stack_name)
        else:
            dev_stack = self.bootstrap_dev()
        self.dev_stack_outputs = { entry['OutputKey']: entry['OutputValue'] for entry in dev_stack['Outputs'] }
        self.dev_stack_parameters = { entry['ParameterKey']: entry.get('ParameterValue') for entry in dev_stack['Parameters'] }

        if skip_prod_bootstrap:
            prod_stack = self.cloudformationprod.describe_stack(self.prod_stack_name)
        else:
            prod_stack = self.bootstrap_prod()
        self.prod_stack_outputs = { entry['OutputKey']: entry['OutputValue'] for entry in prod_stack['Outputs'] }
        self.prod_stack_parameters = { entry['ParameterKey']: entry.get('ParameterValue') for entry in prod_stack['Parameters'] }

        self.update_dev()
        if not skip_prod_bootstrap:
            self.create_prod_image_tags()
        self.update_prod()

    def bootstrap_dev(self):
        with open(DEV_TEMPLATE_PATH, 'r') as f:
            template = f.read()
        parameters = [
            {'ParameterKey': 'ServiceName', 'ParameterValue': self.service_name},
            {'ParameterKey': 'ServiceRoleArn', 'ParameterValue': self.dev_service_role},
            {'ParameterKey': 'ProdAWSAccountID', 'ParameterValue': self.prod_account},
            {'ParameterKey': 'ProdAWSAccountCodePipelineRoleArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'ProdS3ArtifactBucketArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'ProdCodePipelineArtifactKeyArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'ProdRepositoryName', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'VpcId', 'ParameterValue': self.dev_vpc_id},
            {'ParameterKey': 'TwitchSecurityGroupId', 'ParameterValue': self.dev_vpc_security_group_id},
            {'ParameterKey': 'VpcSubnetIds', 'ParameterValue': ','.join(self.dev_vpc_subnet)},
            {'ParameterKey': 'ProdS3LambdaBucketName', 'ParameterValue': self.lambda_s3_bucket},
            {'ParameterKey': 'LifecycleLambdaS3Key', 'ParameterValue': LIFECYCLE_LAMBDA_S3_KEY},
            {'ParameterKey': 'StackType', 'ParameterValue': "Bootstrap"},
            {'ParameterKey': 'ECSImageType', 'ParameterValue': "Bootstrap"},
        ]
        s3_object = self.get_s3_bucket().put_object(Key="dev-pipeline-template.json", Body=template)
        logger.info('Creating stack {} in account {} with parameters {}'.format(self.dev_stack_name, self.dev_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters if 'ParameterValue' in param]))
        return self.cloudformationdev.create_stack(self.dev_stack_name, s3_object=s3_object, parameters=parameters)

    def update_dev(self):
        with open(DEV_TEMPLATE_PATH, 'r') as f:
            template = f.read()
        parameters = [
            {'ParameterKey': 'ServiceName', 'UsePreviousValue': True},
            {'ParameterKey': 'ServiceRoleArn', 'UsePreviousValue': True},
            {'ParameterKey': 'ProdAWSAccountID', 'UsePreviousValue': True},
            {'ParameterKey': 'ProdAWSAccountCodePipelineRoleArn', 'ParameterValue': self.prod_stack_outputs['ProdAWSAccountCodePipelineRoleArn']},
            {'ParameterKey': 'ProdS3ArtifactBucketArn', 'ParameterValue': self.prod_stack_outputs['ProdS3ArtifactBucketArn']},
            {'ParameterKey': 'ProdCodePipelineArtifactKeyArn', 'ParameterValue': self.prod_stack_outputs['ProdCodePipelineArtifactKeyArn']},
            {'ParameterKey': 'ProdRepositoryName', 'ParameterValue': self.prod_stack_outputs['ProdRepositoryName']},
            {'ParameterKey': 'VpcId', 'UsePreviousValue': True},
            {'ParameterKey': 'TwitchSecurityGroupId', 'UsePreviousValue': True},
            {'ParameterKey': 'VpcSubnetIds', 'UsePreviousValue': True},
            {'ParameterKey': 'MinSize', 'UsePreviousValue': True},
            {'ParameterKey': 'DesiredCapacity', 'UsePreviousValue': True},
            {'ParameterKey': 'MaxSize', 'UsePreviousValue': True},
            {'ParameterKey': 'MinimumHealthyPercent', 'UsePreviousValue': True},
            {'ParameterKey': 'ProdS3LambdaBucketName', 'UsePreviousValue': True},
            {'ParameterKey': 'LifecycleLambdaS3Key', 'UsePreviousValue': True},
            {'ParameterKey': 'ECSImageType', 'ParameterValue': "Latest"},
            {'ParameterKey': 'InstanceType', 'UsePreviousValue': True},
            {'ParameterKey': 'RedisNodeType', 'UsePreviousValue': True},
        ]
        if self.dev_stack_parameters.get("StackType") == "Bootstrap":
            parameters.append({'ParameterKey': 'StackType', 'ParameterValue': "Debug"})
        else:
            parameters.append({'ParameterKey': 'StackType', 'UsePreviousValue': True})
        s3_object = self.get_s3_bucket().put_object(Key="dev-pipeline-template.json", Body=template)
        logger.info('Updating stack {} in account {} with parameters {}'.format(self.dev_stack_name, self.dev_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters if 'ParameterValue' in param]))
        return self.cloudformationdev.update_stack(self.dev_stack_name, s3_object=s3_object, parameters=parameters)

    def bootstrap_prod(self):
        with open(PROD_TEMPLATE_PATH, 'r') as f:
            template = f.read()
        parameters = [
            {'ParameterKey': 'ServiceName', 'ParameterValue': self.service_name},
            {'ParameterKey': 'ServiceRoleArn', 'ParameterValue': self.prod_service_role},
            {'ParameterKey': 'DevAWSAccountID', 'ParameterValue': self.dev_account},
            {'ParameterKey': 'DevAWSAccountECSRoleArn', 'ParameterValue': self.dev_stack_outputs['DevAWSAccountECSRoleArn']},
            {'ParameterKey': 'DevECSClusterArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'DevECSServiceArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'DevECSALB', 'ParameterValue': self.dev_stack_outputs['ECSALB']},
            {'ParameterKey': 'CodeCommitBranchName', 'ParameterValue': self.code_commit_branch},
            {'ParameterKey': 'CodeCommitRepositoryName', 'ParameterValue': self.code_commit_repository},
            {'ParameterKey': 'VpcId', 'ParameterValue': self.prod_vpc_id},
            {'ParameterKey': 'TwitchSecurityGroupId', 'ParameterValue': self.prod_vpc_security_group_id},
            {'ParameterKey': 'VpcSubnetIds', 'ParameterValue': ','.join(self.prod_vpc_subnet)},
            {'ParameterKey': 'LambdaS3Bucket', 'ParameterValue': self.lambda_s3_bucket},
            {'ParameterKey': 'LifecycleLambdaS3Key', 'ParameterValue': LIFECYCLE_LAMBDA_S3_KEY},
            {'ParameterKey': 'ProdECSTaskImage', 'ParameterValue': BOOTSTRAP_DOCKER_IMAGE},
            {'ParameterKey': 'OneboxECSTaskImage', 'ParameterValue': BOOTSTRAP_DOCKER_IMAGE},
            {'ParameterKey': 'StackType', 'ParameterValue': "Bootstrap"},
            {'ParameterKey': 'ECSImageType', 'ParameterValue': "Bootstrap"},
        ]
        s3_object = self.get_s3_bucket().put_object(Key="prod-pipeline-template.json", Body=template)
        logger.info('Creating stack {} in account {} with parameters {}'.format(self.prod_stack_name, self.prod_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters if 'ParameterValue' in param]))
        return self.cloudformationprod.create_stack(self.prod_stack_name, s3_object=s3_object, parameters=parameters)

    def update_prod(self):
        with open(PROD_TEMPLATE_PATH, 'r') as f:
            template = f.read()
        parameters = [
            {'ParameterKey': 'ServiceName', 'UsePreviousValue': True},
            {'ParameterKey': 'ServiceRoleArn', 'UsePreviousValue': True},
            {'ParameterKey': 'DevAWSAccountID', 'UsePreviousValue': True},
            {'ParameterKey': 'DevAWSAccountECSRoleArn', 'UsePreviousValue': True},
            {'ParameterKey': 'DevECSClusterArn', 'ParameterValue': self.dev_stack_outputs['DevECSClusterArn']},
            {'ParameterKey': 'DevECSServiceArn', 'ParameterValue': self.dev_stack_outputs['DevECSServiceArn']},
            {'ParameterKey': 'DevECSALB', 'UsePreviousValue': True},
            {'ParameterKey': 'CodeSource', 'UsePreviousValue': True},
            {'ParameterKey': 'CodeCommitBranchName', 'UsePreviousValue': True},
            {'ParameterKey': 'CodeCommitRepositoryName', 'UsePreviousValue': True},
            {'ParameterKey': 'CodeBuildImage', 'UsePreviousValue': True},
            {'ParameterKey': 'IntegrationTestImage', 'UsePreviousValue': True},
            {'ParameterKey': 'VpcId', 'UsePreviousValue': True},
            {'ParameterKey': 'TwitchSecurityGroupId', 'UsePreviousValue': True},
            {'ParameterKey': 'VpcSubnetIds', 'UsePreviousValue': True},
            {'ParameterKey': 'MinSize', 'UsePreviousValue': True},
            {'ParameterKey': 'DesiredCapacity', 'UsePreviousValue': True},
            {'ParameterKey': 'MaxSize', 'UsePreviousValue': True},
            {'ParameterKey': 'MinimumHealthyPercent', 'UsePreviousValue': True},
            {'ParameterKey': 'LambdaS3Bucket', 'UsePreviousValue': True},
            {'ParameterKey': 'LifecycleLambdaS3Key', 'UsePreviousValue': True},
            {'ParameterKey': 'ECSImageType', 'ParameterValue': "Latest"},
            {'ParameterKey': 'InstanceType', 'UsePreviousValue': True},
            {'ParameterKey': 'RedisNodeType', 'UsePreviousValue': True},
        ]
        if self.prod_stack_parameters.get("StackType") == "Bootstrap":
            parameters.append({'ParameterKey': 'StackType', 'ParameterValue': "Debug"})
        else:
            parameters.append({'ParameterKey': 'StackType', 'UsePreviousValue': True})
        s3_object = self.get_s3_bucket().put_object(Key="prod-pipeline-template.json", Body=template)
        logger.info('Updating stack {} in account {} with parameters {}'.format(self.prod_stack_name, self.prod_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters if 'ParameterValue' in param]))
        self.cloudformationprod.update_stack(self.prod_stack_name, s3_object=s3_object, parameters=parameters)

    def create_prod_image_tags(self):
        repository_name = self.prod_stack_outputs['ProdRepositoryName']
        dev_manifest = self.get_image_manifest(DEV_IMAGE_TAG)
        if not dev_manifest:
            raise Exception('Image not found: {} Make sure the CodePipeline gets past the first image tagger.'.format(DEV_IMAGE_TAG))

        ecr = self.prod_session.client('ecr')
        if self.get_image_manifest(ONEBOX_IMAGE_TAG):
            logger.info("Repository already has an image for {}".format(ONEBOX_IMAGE_TAG))
        else:
            ecr.put_image(
                repositoryName=repository_name,
                imageManifest=dev_manifest,
                imageTag=ONEBOX_IMAGE_TAG
            )
            logger.info("Created image tag {}".format(ONEBOX_IMAGE_TAG))

        if self.get_image_manifest(PROD_IMAGE_TAG):
            logger.info("Repository already has an image for {}".format(PROD_IMAGE_TAG))
        else:
            ecr.put_image(
                repositoryName=repository_name,
                imageManifest=dev_manifest,
                imageTag=PROD_IMAGE_TAG
            )
            logger.info("Created image tag {}".format(PROD_IMAGE_TAG))

    def get_image_manifest(self, tag):
        try:
            repository_name = self.prod_stack_outputs['ProdRepositoryName']
            ecr = self.prod_session.client('ecr')
            response = ecr.batch_get_image(
                repositoryName=repository_name,
                imageIds=[
                    {
                        'imageTag': tag
                    }
                ]
            )

            if len(response['failures']) > 0:
                failure_code = response['failures'][0]['failureCode']
                if failure_code == 'ImageNotFound':
                    return None
                failure_reason = response['failures'][0]['failureReason']
                raise Exception('{0}: {1}'.format(failure_code, failure_reason))

            if not response.get('images'):
                return None

            if len(response['images']) > 1:
                raise Exception('Too many images found for "{0}"'.format(tag))

            return response['images'][0]['imageManifest']

        except ClientError as e:
            raise Exception('Error getting image "{0}"'.format(tag), e)

    def upload_lambda_functions(self):
        filename = 'lambda'
        shutil.make_archive(filename, 'zip', LAMBDA_FILE_PATH)
        self.get_s3_bucket().upload_file(filename + '.zip', 'functions')
        os.remove(filename + '.zip')

        shutil.make_archive(filename, 'zip', LIFECYCLE_LAMBDA_FILE_PATH)
        self.get_s3_bucket().upload_file(filename + '.zip', LIFECYCLE_LAMBDA_S3_KEY)
        os.remove(filename + '.zip')

    def get_s3_bucket(self):
        s3 = self.prod_session.resource('s3')
        bucket = s3.Bucket(self.lambda_s3_bucket)
        # Check if bucket exists and create it if not
        try:
            s3.meta.client.head_bucket(Bucket=self.lambda_s3_bucket)
        except ClientError as e:
            error_code = int(e.response['Error']['Code'])
            if error_code == 404:
                bucket.create(CreateBucketConfiguration={'LocationConstraint': self.prod_session.region_name})
                bucket.wait_until_exists()
        policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "arn:aws:iam::{}:root".format(self.dev_account)
                    },
                    "Action": [
                        "s3:Get*"
                    ],
                    "Resource": [
                        "arn:aws:s3:::{}".format(self.lambda_s3_bucket),
                        "arn:aws:s3:::{}/*".format(self.lambda_s3_bucket)
                    ]
                }
            ]
        }
        self.prod_session.client('s3').put_bucket_policy(Bucket=self.lambda_s3_bucket, Policy=json.dumps(policy, indent=1))
        return bucket


class CloudFormationFacade:
    def __init__(self, session):
        self.client = session.client('cloudformation')

    def describe_stack(self, stack_name):
        return self.client.describe_stacks(StackName=stack_name)['Stacks'][0]

    def create_stack(self, stack_name, template=None, parameters=[], s3_object=None):
        request = {
            "StackName": stack_name,
            "Parameters": parameters,
            "Capabilities": ['CAPABILITY_IAM']
        }
        if template:
            request["TemplateBody"] = template
        if s3_object:
            request["TemplateURL"] = 'http://{}.s3.amazonaws.com/{}'.format(s3_object.bucket_name, s3_object.key)
        self.client.create_stack(**request)
        return self.wait_for_stack_complete('create', stack_name)

    def update_stack(self, stack_name, template=None, parameters=[], s3_object=None):
        try:
            request = {
                "StackName": stack_name,
                "Parameters": parameters,
                "Capabilities": ['CAPABILITY_IAM']
            }
            if template:
                request["TemplateBody"] = template
            if s3_object:
                request["TemplateURL"] = 'http://{}.s3.amazonaws.com/{}'.format(s3_object.bucket_name, s3_object.key)
            self.client.update_stack(**request)
        except ClientError as e:
            if e.response['Error']['Code'] == 'ValidationError' and 'No updates' in e.response['Error']['Message']:
                logger.info('No updates needed.')
                return
            raise e

        self.wait_for_stack_complete('update', stack_name)

    def wait_for_stack_complete(self, action, stack_name):
        last_status = None
        while True:
            stack = self.describe_stack(stack_name)
            status = stack['StackStatus']
            if status != last_status:
                logger.info('Stack {} status: {}'.format(stack_name, status))
                last_status = status
            if 'IN_PROGRESS' not in status:
                if 'FAILED' in status or 'ROLLBACK' in status:
                    raise RuntimeError('Failed to {} stack {}'.format(action, stack_name))
                break
            sleep(2)
        return stack


def get_session(name, profile, material):
    if profile:
        return boto3.Session(profile_name=profile)

    if material:
        # http.client is only required for odin and doesn't exist on all hosts.
        import http.client

        response = http_get('localhost:2009', '/query?ContentType=JSON&Operation=retrieve&material.materialName={}&material.materialType=Principal'.format(material))
        principal = base64.b64decode(json.loads(response.decode('utf-8'))['material']['materialData']).decode('utf-8')

        response = http_get('localhost:2009', '/query?ContentType=JSON&Operation=retrieve&material.materialName={}&material.materialType=Credential'.format(material))
        credential = base64.b64decode(json.loads(response.decode('utf-8'))['material']['materialData']).decode('utf-8')

        return boto3.Session(aws_access_key_id=principal, aws_secret_access_key=credential)

    raise RuntimeError('Missing {} profile or material set'.format(name))


def http_get(host, request):
    conn = http.client.HTTPConnection(host)
    try:
        conn.request('GET', request)
        response = conn.getresponse()
        if response.status < 200 or response.status >= 300:
            logger.error('Odin response: %s %s', response.status, response.reason)
            logger.error('Odin response body: %s', response.read())
            raise RuntimeError('Odin response status: {}'.format(response.status))
        return response.read()
    finally:
        conn.close()


if __name__ == "__main__":
    main()
