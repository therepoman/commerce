## How to create a new service stack
* The script needs python with boto3.  If using Odin, it also needs the http.client module.  For amazon managed linux, this can be installed from brazil with:
```
brazil-bootstrap --environmentRoot ~/python --environmentType runtime --flavor DEV.STD.PTHREAD --packages Python-default,Boto3-1.x
PATH=~/python/bin:$PATH
```
* Obtain credentials or odin material sets with sufficient permissions on both accounts for creating all of the resources.
* Collect the required parameters.  This can be done by running the script with only credentials.  It will list the available vpc items and print example parameters to copy/paste.
```
python cloudformation/create-service.py --dev-odin-material-set com.amazon.credentials.isengard.1234.user/my-user --prod-odin-material-set com.amazon.credentials.isengard.12345.user/my-user
python cloudformation/create-service.py --dev-profile-name myservicedev --prod-profile-name myservice
```
* Use the AWS Console to create the service stacks using dev-service-template.json and prod-service-template.json.  Copy the service role arn from each of the stacks, expand the Outputs section to see it.  These are used as parameters for the script.
* To create the service from scratch, run it without --skip* parameters.  It will first create both stacks using modified bootstrap templates to work around circular dependencies, then update them with the full template to connect everything properly.
```
python cloudformation/create-service.py \
--service-name test1234 \
--code-commit-branch master \
--code-commit-repository falador \
--dev-vpc-id vpc-1234 --dev-vpc-security-group-id sg-1234 --dev-vpc-subnet subnet-1234 subnet-1234 subnet-1234 \
--prod-vpc-id vpc-1234 --prod-vpc-security-group-id sg-1234 --prod-vpc-subnet subnet-1234 subnet-1234 subnet-1234 \
--dev-service-role-arn <arn copied from the dev service stack outputs section> \
--prod-service-role-arn <arn copied from the prod service stack outputs section> \
```
If using Odin for credentials:
```
--dev-odin-material-set com.amazon.credentials.isengard.1234.user/my-user \
--prod-odin-material-set com.amazon.credentials.isengard.12345.user/my-user
```
If using AWS CLI/SDK profiles for credentials:
```
--dev-profile-name myservicedev \
--prod-profile-name myservice
```

## How to update a service stack
* Once the service is live it would be best to manually upload the template using the AWS console since it will give a summary of what is changing.
* If no sanity checks or confirmation prompts are needed, add the following to the above create command.  It will skip the create calls with bootstrap versions of the templates and do an update to each stack.
```
--skip-dev-bootstrap \
--skip-prod-bootstrap \
```

## How the bootstrap works around circular dependencies
The templates with circular dependencies have a StackType parameter.  Setting this parameter to the Bootstrap option will
replace parts of the template with stubs that will not actually work, but are valid enough to allow resources to be created.
Once the stack has been created, it needs to be updated to another value for things to work.
