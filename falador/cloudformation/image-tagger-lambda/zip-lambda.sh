#!/bin/bash

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

zipfile="$PWD/image-tagger-lambda.zip"

if [[ -e $zipfile ]]; then
    echo "Removing old zip"
    rm $zipfile
fi

echo "Creating zip and adding dependencies"
pushd $VIRTUAL_ENV/lib/python3.6/site-packages/
zip -rq9 --exclude=boto* $zipfile *
popd

echo "Adding lambda function to zip"
zip -gq $zipfile image-tagger.py