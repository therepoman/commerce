package facade_test

import (
	"code.justin.tv/commerce/falador/facade"
	"code.justin.tv/commerce/falador/mocks"
	"code.justin.tv/commerce/falador/models"
	"github.com/twitchtv/twirp"

	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"
)

const (
	domain              = "Domain"
	bits                = "bits"
	badDomain           = "BadDomain"
	sku                 = "sku1"
	badSku              = "badSku"
	expiredSku          = "expiredSku"
	badExpirationSku    = "badlyFormattedExpirationSku"
	futureExpirationSku = "futureExpirationSku"
	displayName         = "DisplayName"
	anotherDisplayName  = "DisplayName2"
	dontBroadcast       = false
	broadcast           = true
)

// TestHappyFacade tests the happy cases for all of the Facade APIs
func TestFacadeHappyCases(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	// Test the happy PutProduct case
	defaultProduct := defaultProduct()
	defaultProductList := defaultProductList()
	productStorage.On("Put", mock.Anything, defaultProduct).Return(nil)
	productStorage.On("Get", mock.Anything, domain).Return(defaultProductList, nil)
	productCache.On("Set", mock.Anything, domain, defaultProductList).Return(nil)
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	err := facade.PutProduct(nil, defaultProduct)
	assert.Nil(t, err)

	// Test the happy GetProduct case
	product, err := facade.GetProduct(nil, domain, sku, false)
	assert.Nil(t, err)
	assert.Equal(t, defaultProduct, product, "GetProduct returned incorrect value")

	// Test the happy ListProducts case
	productList, err := facade.ListProducts(nil, domain, false)
	assert.Nil(t, err)
	assert.Equal(t, defaultProductList, productList, "ListProducts returned incorrect list")
	return
}

// TestPutProductStoragePutFailure tests PutProduct when storage put fails
func TestPutProductStoragePutFailure(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	newErrorProductStoragePut := errors.New("Something bad in product storage put")
	defaultProduct := defaultProduct()
	defaultProductList := defaultProductList()
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)
	productStorage.On("Put", mock.Anything, defaultProduct).Return(newErrorProductStoragePut)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	err := facade.PutProduct(nil, defaultProduct)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
}

// TestPutProductStorageGetFailure tests PutProduct when storage get fails
func TestPutProductStorageGetFailure(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	defaultProduct := defaultProduct()
	defaultProductList := defaultProductList()
	newErrorProductStorageGet := errors.New("Something bad in product storage get")
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)
	productStorage.On("Put", mock.Anything, defaultProduct).Return(nil)
	productStorage.On("Get", mock.Anything, domain).Return(nil, newErrorProductStorageGet)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	err := facade.PutProduct(nil, defaultProduct)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
}

// TestPutProductCacheSetFailure tests PutProduct when cache set fails
func TestPutProductCacheSetFailure(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	defaultProduct := defaultProduct()
	defaultProductList := defaultProductList()
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)
	productStorage.On("Put", mock.Anything, defaultProduct).Return(nil)
	productStorage.On("Get", mock.Anything, domain).Return(defaultProductList, nil)
	newErrorProductCacheSet := errors.New("Something bad in product cache set")
	productCache.On("Set", mock.Anything, domain, defaultProductList).Return(newErrorProductCacheSet)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	err := facade.PutProduct(nil, defaultProduct)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
}

// TestPutProductStoragePutFailure tests PutProduct when the given domain has reached the sku limit
func TestPutProductDomainSkuLimitFailure(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	defaultProduct := defaultProduct()
	longProductList := longProductList()
	productCache.On("Get", mock.Anything, domain).Return(longProductList, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	err := facade.PutProduct(nil, defaultProduct)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.NewError(twirp.InvalidArgument, "Some error"), err)
}

// TestPutProductDomainSkuLimitWithExistingSkuSuccess tests PutProduct when the given domain has reached the sku limit, but the sku is replacing an existing sku
func TestPutProductDomainSkuLimitWithExistingSkuSuccess(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	longProductList := longProductList()
	productCache.On("Get", mock.Anything, domain).Return(longProductList, nil)
	productCache.On("Set", mock.Anything, domain, longProductList).Return(nil)
	productStorage.On("Get", mock.Anything, domain).Return(longProductList, nil)
	productStorage.On("Put", mock.Anything, longProductList[0]).Return(nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	err := facade.PutProduct(nil, longProductList[0])
	assert.Nil(t, err)
}

// TestGetProductCacheGetFailure tests GetProduct when cache get fails
func TestGetProductCacheGetFailure(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	newErrorProductCacheGet := errors.New("Something bad in product cache get")
	productCache.On("Get", mock.Anything, domain).Return(nil, newErrorProductCacheGet)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	product, err := facade.GetProduct(nil, domain, sku, false)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	assert.Nil(t, product)
}

// TestGetProductMissingProductList tests GetProduct when the cache returns a response, but it is nil
func TestGetProductMissingProductList(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	productCache.On("Get", mock.Anything, badDomain).Return(nil, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	product, err := facade.GetProduct(nil, badDomain, sku, false)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	assert.Nil(t, product)
}

// TestGetProductEmptyProductList tests GetProduct when the cache returns a response, but it is an empty list
func TestGetProductEmptyProductList(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	productCache.On("Get", mock.Anything, badDomain).Return([]*models.Product{}, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	product, err := facade.GetProduct(nil, badDomain, sku, false)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	assert.Nil(t, product)
}

// TestGetProductMissingProduct tests GetProduct when the cache returns a response, but it doesn't have the expected sku
func TestGetProductMissingProduct(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	defaultProductList := defaultProductList()
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	product, err := facade.GetProduct(nil, domain, badSku, false)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	assert.Nil(t, product)
}

// TestGetProductExpiredProductIsNotAllowed tests GetProduct when the cache returns a response, but the
// product is expired and this is not allowed
func TestGetProductExpiredProductIsNotAllowed(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	// Test the happy PutProduct case
	defaultProductList := productListWithExpiredProductsAndBadlyFormattedProducts()
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	product, err := facade.GetProduct(nil, domain, expiredSku, false)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	assert.Nil(t, product)
}

// TestGetProductExpiredProductIsAllowed tests GetProduct when the cache returns a response, the product is expired, and this is okay
func TestGetProductExpiredProductIsAllowed(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	// Test the happy PutProduct case
	defaultProductList := productListWithExpiredProductsAndBadlyFormattedProducts()
	productCache.On("Get", mock.Anything, domain).Return(defaultProductList, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	product, err := facade.GetProduct(nil, domain, expiredSku, true)
	assert.Nil(t, err)
	assert.NotNil(t, product)
	assert.Equal(t, constructExpiredProduct(), product, "Expired product does not match")
}

// TestListProductCacheGetFailure tests ListProduct when cache get fails
func TestListProductCacheGetFailure(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	newErrorProductCacheGet := errors.New("Something bad in product cache get")
	productCache.On("Get", mock.Anything, domain).Return(nil, newErrorProductCacheGet)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	productList, err := facade.ListProducts(nil, domain, false)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	assert.Nil(t, productList)
}

// TestListProductMissingProductList tests ListProduct when cache returns a response, but it is nil
func TestListProductMissingProductList(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	productCache.On("Get", mock.Anything, badDomain).Return(nil, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	productList, err := facade.ListProducts(nil, badDomain, false)
	assert.Nil(t, err)
	assert.Equal(t, []*models.Product{}, productList, "Product list should be empty")
}

// TestListProductDifferentExpirationDates tests ListProduct when different expiration dates (both valid and invalid) are used
func TestListProductDifferentExpirationDates(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	expirationProductList := productListWithExpiredProductsAndBadlyFormattedProducts()
	productCache.On("Get", mock.Anything, domain).Return(expirationProductList, nil)
	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)

	// Test for allowExpired is false
	productList, err := facade.ListProducts(nil, domain, false)
	assert.Nil(t, err)
	assert.NotNil(t, productList)
	assert.Equal(t, 2, len(productList), "Product list has incorrect length")
	assert.Equal(t, defaultProduct(), productList[0], "Product list has incorrect first product")
	assert.Equal(t, constructFutureExpirationDateProduct(), productList[1], "Product list has incorrect second product")

	// Test for allowExpired is true
	productList, err = facade.ListProducts(nil, domain, true)
	assert.Nil(t, err)
	assert.NotNil(t, productList)
	assert.Equal(t, 4, len(productList), "Product list has incorrect length")
	assert.Equal(t, defaultProduct(), productList[0], "Product list has incorrect first product")
	assert.Equal(t, constructExpiredProduct(), productList[1], "Product list has incorrect second product")
	assert.Equal(t, constructBadExpirationProduct(), productList[2], "Product list has incorrect third product")
	assert.Equal(t, constructFutureExpirationDateProduct(), productList[3], "Product list has incorrect fourth product")
}

// TestListProductEmptyProductList tests ListProduct when cache returns a response, but it is an empty list
func TestListProductEmptyProductList(t *testing.T) {
	productStorage := new(mocks.ProductStorage)
	productCache := new(mocks.ProductCache)

	productCache.On("Get", mock.Anything, badDomain).Return([]*models.Product{}, nil)

	facade := facade.NewCachedDynamoProductManager(productStorage, productCache)
	productList, err := facade.ListProducts(nil, badDomain, false)
	assert.Nil(t, err)
	assert.NotNil(t, productList)
}

func defaultProduct() *models.Product {
	currentTimeString := time.Now().UTC().Format(time.RFC3339)
	cost := &models.Cost{bits, 1}
	return &models.Product{
		Domain:        domain,
		Sku:           sku,
		Cost:          cost,
		InDevelopment: false,
		DisplayName:   displayName,
		Expiration:    "",
		LastUpdated:   currentTimeString,
		LastEditor:    "",
		Broadcast:     broadcast}
}

func defaultProductList() []*models.Product {
	currentTimeString := time.Now().UTC().Format(time.RFC3339)
	firstCost := &models.Cost{bits, 1}
	firstProduct := models.Product{
		Domain:        domain,
		Sku:           sku,
		Cost:          firstCost,
		InDevelopment: false,
		DisplayName:   displayName,
		Expiration:    "",
		LastUpdated:   currentTimeString,
		LastEditor:    "",
		Broadcast:     broadcast}

	secondCost := &models.Cost{bits, 2}
	secondProduct := models.Product{
		Domain:        domain,
		Sku:           "sku2",
		Cost:          secondCost,
		InDevelopment: true,
		DisplayName:   anotherDisplayName,
		Expiration:    "",
		LastUpdated:   currentTimeString,
		LastEditor:    "",
		Broadcast:     dontBroadcast}
	return []*models.Product{&firstProduct, &secondProduct}
}

func productListWithExpiredProductsAndBadlyFormattedProducts() []*models.Product {
	return []*models.Product{defaultProduct(), constructExpiredProduct(), constructBadExpirationProduct(), constructFutureExpirationDateProduct()}
}

func longProductList() []*models.Product {
	productList := []*models.Product{}
	for i := 0; i < facade.DomainSkuLimit; i++ {
		productList = append(productList, &models.Product{
			Domain: domain,
			Sku:    fmt.Sprintf("long-product-sku-%d", i),
		})
	}
	return productList
}

func constructBadExpirationProduct() *models.Product {
	currentTimeString := time.Now().UTC().Format(time.RFC3339)
	badlyFormattedString := "201aaa-0"
	thirdCost := &models.Cost{bits, 3}
	return &models.Product{
		Domain:        domain,
		Sku:           badExpirationSku,
		Cost:          thirdCost,
		InDevelopment: true,
		DisplayName:   badExpirationSku,
		Expiration:    badlyFormattedString,
		LastUpdated:   currentTimeString,
		LastEditor:    "",
		Broadcast:     dontBroadcast}
}

func constructExpiredProduct() *models.Product {
	currentTimeString := time.Now().UTC().Format(time.RFC3339)
	expiredTimeString := "2012-11-01T22:08:41+00:00"
	cost := &models.Cost{bits, 2}
	return &models.Product{
		Domain:        domain,
		Sku:           expiredSku,
		Cost:          cost,
		InDevelopment: true,
		DisplayName:   expiredSku,
		Expiration:    expiredTimeString,
		LastUpdated:   currentTimeString,
		LastEditor:    "",
		Broadcast:     dontBroadcast}
}

func constructFutureExpirationDateProduct() *models.Product {
	currentTimeString := time.Now().UTC().Format(time.RFC3339)
	futureExpirationTime := time.Now().Add(time.Hour).UTC().Format(time.RFC3339)
	cost := &models.Cost{bits, 4}
	return &models.Product{
		Domain:        domain,
		Sku:           futureExpirationSku,
		Cost:          cost,
		InDevelopment: true,
		DisplayName:   futureExpirationSku,
		Expiration:    futureExpirationTime,
		LastUpdated:   currentTimeString,
		LastEditor:    "",
		Broadcast:     dontBroadcast}
}
