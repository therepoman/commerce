package facade

import (
	"code.justin.tv/commerce/falador/dynamo"
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/middleware"
	"code.justin.tv/commerce/falador/models"
	"context"
	"github.com/twitchtv/twirp"
)

// AuthorizationManager represents the interface for determine if a tuid can change a given domain
type AuthorizationManager interface {
	HasValidListProductAuth(ctx context.Context, req *ft.ListProductsRequest) twirp.Error
	HasValidPutProductAuth(ctx context.Context, req *ft.PutProductRequest) twirp.Error
}

// DynamoAuthorizationManager is the Dynamo implementation of DynamoAuthorizationManager
type DynamoAuthorizationManager struct {
	authStorage dynamo.AuthorizationStorage
}

// NewDynamoAuthorizationManager constructs a new Dynamo implementation of AuthorizationManager
func NewDynamoAuthorizationManager(storage dynamo.AuthorizationStorage) AuthorizationManager {
	return &DynamoAuthorizationManager{
		authStorage: storage,
	}
}

func (authManager DynamoAuthorizationManager) HasValidListProductAuth(ctx context.Context, req *ft.ListProductsRequest) twirp.Error {
	extensionId, ok := ctx.Value(models.ExtensionIdCtxKey).(string)
	if ok {
		if extensionId == req.GetDomain() {
			// You can go about your business.
			return nil
		}
	}

	userId, ok := ctx.Value(models.UserIdCtxKey).(string)
	if !ok {
		return twirp.NewError(twirp.Unauthenticated, "User not authenticated.")
	}
	authorized, err := authManager.tuidCanEditDomain(userId, req.GetDomain())
	if err != nil {
		middleware.GetLogger(ctx).Errorf("Error occurred Checking Authorization: %v", err)
		return twirp.InternalError("Error occurred Checking Authorization")
	}
	if !authorized {
		return twirp.NewError(twirp.PermissionDenied, "User not authorized to see expired products under specified Domain.")
	}
	return nil
}

func (authManager DynamoAuthorizationManager) HasValidPutProductAuth(ctx context.Context, req *ft.PutProductRequest) twirp.Error {
	extensionId, ok := ctx.Value(models.ExtensionIdCtxKey).(string)
	if ok {
		if extensionId == req.GetProduct().GetDomain() {
			// You can go about your business.
			return nil
		}
	}

	userID, ok := ctx.Value(models.UserIdCtxKey).(string)
	if !ok {
		return twirp.NewError(twirp.Unauthenticated, "User not authenticated.")
	}
	authorized, err := authManager.tuidCanEditDomain(userID, req.GetProduct().GetDomain())
	if err != nil {
		middleware.GetLogger(ctx).Errorf("Error occurred Checking Authorization: %v", err)
		return twirp.InternalError("Error occurred Checking Authorization")
	}
	if !authorized {
		return twirp.NewError(twirp.PermissionDenied, "User not authorized to see expired products under specified Domain.")
	}
	return nil
}

// tuidCanEditDomain determines if a tuid can edit a given domain
func (authManager DynamoAuthorizationManager) tuidCanEditDomain(tuid, domainID string) (bool, error) {
	isAdmin, err := authManager.isAdmin(tuid)
	if err != nil {
		return false, err
	}
	if isAdmin {
		return true, nil
	}

	domains, err := authManager.getDomains(tuid)
	if err != nil {
		return false, err
	}
	for _, domain := range domains {
		if domainID == domain {
			return true, nil
		}
	}
	return false, nil
}

func (authManager DynamoAuthorizationManager) isAdmin(tuid string) (bool, error) {
	admins, err := authManager.authStorage.GetAdmins()
	if err != nil {
		return false, err
	}
	for _, admin := range admins {
		if tuid == admin {
			return true, nil
		}
	}
	return false, nil
}

func (authManager DynamoAuthorizationManager) getDomains(tuid string) ([]string, error) {
	user, err := authManager.authStorage.GetUser(tuid)
	if err != nil {
		return nil, err
	}
	return user.DomainIds, nil
}
