package facade

import (
	"fmt"

	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/falador/cache"
	"code.justin.tv/commerce/falador/dynamo"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/models"
)

const (
	DomainSkuLimit = 250
)

// CachedDynamoProductManager that is used for handling Products
type CachedDynamoProductManager struct {
	productStorage dynamo.ProductStorage
	productCache   cache.ProductCache
}

// ProductManager represents the Facade interface for handling Products
type ProductManager interface {
	PutProduct(mCollection *metrics.MetricCollection, product *models.Product) twirp.Error
	GetProduct(mCollection *metrics.MetricCollection, domain string, sku string, allowExpired bool) (*models.Product, twirp.Error)
	ListProducts(mCollection *metrics.MetricCollection, domain string, allowExpired bool) ([]*models.Product, twirp.Error)
	GetMetrics() *metrics.MetricCollection
}

// NewCachedDynamoProductManager constructs a new Product Facade
func NewCachedDynamoProductManager(productStorage dynamo.ProductStorage, productCache cache.ProductCache) ProductManager {
	productManager := &CachedDynamoProductManager{}
	productManager.productStorage = productStorage
	productManager.productCache = productCache
	return productManager
}

// PutProduct adds a new Product to DynamoDB
func (productManager *CachedDynamoProductManager) PutProduct(mCollection *metrics.MetricCollection, product *models.Product) twirp.Error {
	// Ensure we do not exceed the max number of Skus under one domain
	domain := product.Domain
	currentProducts, err := productManager.productCache.Get(mCollection, domain)
	if err != nil {
		return twirp.InternalError("Failed to get all products")
	}

	// Check if Put will result in exceeding SKU limit
	foundProduct, _ := filterForSku(currentProducts, product.Sku, true)
	if foundProduct == nil && len(currentProducts) >= DomainSkuLimit {
		return twirp.NewError(twirp.ResourceExhausted, fmt.Sprintf("Domain already at %d Sku limit. No more Skus can be created under this domain", DomainSkuLimit))
	}

	// Put the product in dynamo
	err = productManager.productStorage.Put(mCollection, product)
	if err != nil {
		return twirp.InternalError("Failed to add product")
	}
	// Query for all products in Dynamo directly
	productList, err := productManager.productStorage.Get(mCollection, domain)
	if err != nil {
		return twirp.InternalError("Failed to get all products")
	}

	// Update the cache with all products
	err = productManager.productCache.Set(mCollection, domain, productList)
	if err != nil {
		return twirp.InternalError("Failed to set new products")
	}
	return nil
}

// GetProduct obtains a specific Product given a unique domain, sku combination
func (productManager *CachedDynamoProductManager) GetProduct(mCollection *metrics.MetricCollection, domain string, sku string, allowExpired bool) (*models.Product, twirp.Error) {
	productList, err := productManager.productCache.Get(mCollection, domain)
	if err != nil {
		return nil, twirp.InternalError("Failed to get all products")
	}
	if productList == nil || len(productList) == 0 {
		return nil, twirp.InvalidArgumentError("domain", "Invalid domain")
	}
	return filterForSku(productList, sku, allowExpired)
}

// ListProducts obtains all non-expired products under a domain
func (productManager *CachedDynamoProductManager) ListProducts(mCollection *metrics.MetricCollection, domain string, allowExpired bool) ([]*models.Product, twirp.Error) {
	productList, err := productManager.productCache.Get(mCollection, domain)
	if err != nil {
		return nil, twirp.InternalError("Failed to get all products")
	}
	if !allowExpired {
		productList = filterForUnexpired(productList)
	}
	return productList, nil
}

// GetMetrics returns cache metrics.
func (productManager *CachedDynamoProductManager) GetMetrics() *metrics.MetricCollection {
	return productManager.productCache.GetMetrics()
}

// filterForSku filters a slice of Product to the Product matching the sku
func filterForSku(productList []*models.Product, sku string, allowExpired bool) (*models.Product, twirp.Error) {
	for _, element := range productList {
		if element.Sku == sku {
			// If the product is expired and expired products are not allowed, error
			if element.IsExpired() && !allowExpired {
				return nil, twirp.InvalidArgumentError("sku", "Expired product")
			}
			// Otherwise, the product is either not expired or we allow expired products
			return element, nil
		}
	}
	return nil, twirp.InvalidArgumentError("sku", "Invalid product")
}

// filterForUnexpired filters a slice of Product to the Products that are not yet expired
func filterForUnexpired(productList []*models.Product) []*models.Product {
	unexpiredProducts := []*models.Product{}
	for _, element := range productList {
		if !element.IsExpired() {
			unexpiredProducts = append(unexpiredProducts, element)
		}
	}
	return unexpiredProducts
}
