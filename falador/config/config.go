package config

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/aws/session"
	"os"
)

const (
	// Environment represents the environment variable for determining prod, staging, local, etc.
	Environment = "ENVIRONMENT"
	prod        = "production"
	staging     = "staging"
	local       = "local"
	onebox      = "onebox"

	// ServiceNameEnvironmentVariableName is the name of the environment variable for the service name.
	ServiceNameEnvironmentVariableName = "SERVICE_NAME"

	stagingSandstormArn    = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/falador-staging-role"
	stagingSandstormKey    = "identity/cartman/staging/hmac_secret_key"
	productionSandstormArn = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/falador-prod-role"
	productionSandstormKey = "identity/cartman/production/hmac_secret_key"
)

// FaladorConfig is the struct to contain common config variables used throughout falador
type FaladorConfig struct {
	environment            string
	serviceName            string
	defaultDynamoSession   *session.Session
	creds                  *credentials.Credentials
	sandstormArn           string
	sandstormKey           string
	productsTableName      string
	authorizationTableName string
}

// NewFaladorConfig constructs a new Falador config
func NewFaladorConfig() FaladorConfig {
	tConfig := FaladorConfig{}
	tConfig.environment = GetCurrentEnvironment()
	tConfig.serviceName = GetServiceName()
	if tConfig.IsProd() {
		tConfig.sandstormArn = productionSandstormArn
		tConfig.sandstormKey = productionSandstormKey
	} else {
		tConfig.sandstormArn = stagingSandstormArn
		tConfig.sandstormKey = stagingSandstormKey
	}
	tConfig.defaultDynamoSession = newDefaultDynamoAWSSession()
	tConfig.creds = defaults.CredChain(defaults.Config(), defaults.Handlers())
	tConfig.productsTableName = tConfig.serviceName + "-products"
	tConfig.authorizationTableName = tConfig.serviceName + "-authorization"
	return tConfig
}

// newDefaultDynamoAWSSession creates a Dynamo session with a default retry strategy
func newDefaultDynamoAWSSession() *session.Session {
	sess, err := session.NewSession(&aws.Config{
		Retryer: client.DefaultRetryer{
			NumMaxRetries: 3,
		},
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		panic(err)
	}
	return sess
}

// GetCurrentEnvironment returns the current environment
func GetCurrentEnvironment() string {
	env := os.Getenv(Environment)
	if env == prod {
		return prod
	} else if env == onebox {
		return onebox
	} else if env == staging {
		return staging
	}
	return local
}

// GetServiceName returns the service name
func GetServiceName() string {
	return os.Getenv(ServiceNameEnvironmentVariableName)
}

// GetSandstormRoleArn returns the role arn for accessing sandstorm.
func (c *FaladorConfig) GetSandstormRoleArn() string {
	return c.sandstormArn
}

// GetSandstormKeyName returns the sandstorm key name.
func (c *FaladorConfig) GetSandstormKeyName() string {
	return c.sandstormKey
}

// GetEnvironmentString returns the environment variable for determining prod, staging, local, etc.
func (c *FaladorConfig) GetEnvironmentString() string {
	return c.environment
}

// GetProductsTableName returns the products table name.
func (c *FaladorConfig) GetProductsTableName() string {
	return c.productsTableName
}

// GetAuthorizationTableName returns the authorization table name.
func (c *FaladorConfig) GetAuthorizationTableName() string {
	return c.authorizationTableName
}

// GetIsProd determines if the environment is prod
func (c *FaladorConfig) IsProd() bool {
	return prod == c.environment || c.IsOnebox()
}

// GetIsStaging determines if the environment is staging
func (c *FaladorConfig) IsStaging() bool {
	return staging == c.environment
}

func (c *FaladorConfig) IsOnebox() bool {
	return onebox == c.environment
}

// GetIsLocal determines if the environment is local
func (c *FaladorConfig) IsLocal() bool {
	return local == c.environment
}

// GetDefaultDynamoSession returns the default Dynamo AWS session
func (c *FaladorConfig) GetDefaultDynamoSession() *session.Session {
	return c.defaultDynamoSession
}

// GetCreds returns the AWS credentials
func (c *FaladorConfig) GetCreds() *credentials.Credentials {
	return c.creds
}
