package cache

import (
	"code.justin.tv/commerce/falador/dynamo"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/models"

	rediscache "github.com/go-redis/cache"
	redis "github.com/go-redis/redis/v7"

	"encoding/json"
	"math/rand"
	"time"
)

const (
	localProductCacheSize int = 10000

	//RedisSet represents the metric name of the Redis set
	RedisSet = "Redis:Set"
	//RedisGet represents the metric name of the Redis get
	RedisGet = "Redis:Get"
	//RedisPing represents the metric name of the Redis ping
	RedisPing = "Redis:Ping"
)

// ProductCacheStats represents hits and misses for the cache
type ProductCacheStats struct {
	Hits        uint64
	Misses      uint64
	LocalHits   uint64
	LocalMisses uint64
}

// RedisProductCache that is used for getting/ putting into Redis's 'product' cache
type RedisProductCache struct {
	client         *redis.Client
	codec          *rediscache.Codec
	productStorage dynamo.ProductStorage
	stats          ProductCacheStats
}

// ProductCache represents the DAO interface for the 'product' cache
type ProductCache interface {
	Set(mCollection *metrics.MetricCollection, domain string, products []*models.Product) error
	Get(mCollection *metrics.MetricCollection, domain string) ([]*models.Product, error)
	Ping(mCollection *metrics.MetricCollection) bool
	GetCacheStats() *ProductCacheStats
	GetMetrics() *metrics.MetricCollection
}

// NewProductCache constructs a new ProductCache.
func NewProductCache(host string, port string, productStorage dynamo.ProductStorage) ProductCache {
	rand.Seed(time.Now().Unix())
	newProductCache := &RedisProductCache{}

	address := host + ":" + port
	client := redis.NewClient(&redis.Options{
		Addr:         address,
		Password:     "", // no password set, uses VPC security group
		DB:           0,  // use default DB
		DialTimeout:  time.Duration(500 * time.Millisecond),
		ReadTimeout:  time.Duration(500 * time.Millisecond),
		WriteTimeout: time.Duration(500 * time.Millisecond),
	})

	codecDef := rediscache.Codec{
		Redis: client,
		Marshal: func(v interface{}) ([]byte, error) {
			return json.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return json.Unmarshal(b, v)
		},
	}
	codecDef.UseLocalCache(localProductCacheSize, 1*time.Second)
	newProductCache.codec = &codecDef
	newProductCache.client = client
	newProductCache.productStorage = productStorage
	return newProductCache
}

// Set updates all products for a domain in the Redis cache
func (pCache *RedisProductCache) Set(mCollection *metrics.MetricCollection, domain string, products []*models.Product) error {
	start := time.Now()
	err := pCache.codec.Set(&rediscache.Item{
		Key:    domain,
		Object: products,
		// Use seconds instead of minutes so a random second value is returned instead of a random minute value
		Expiration: getCacheExpiration(5*60, 10*60, time.Second),
	})
	// Note the relevant dependency metrics
	mCollection.AddDependencyCallMetrics(err, time.Since(start), RedisSet)
	return err
}

// Get queries the Redis cache for a domain and returns all associated products
func (pCache *RedisProductCache) Get(mCollection *metrics.MetricCollection, domain string) ([]*models.Product, error) {
	productList := new([]*models.Product)
	item := rediscache.Item{
		Key:    domain,
		Object: productList,
		// Use seconds instead of minutes so a random second value is returned instead of a random minute value
		Expiration: getCacheExpiration(5*60, 10*60, time.Second),
		Func: func() (interface{}, error) {
			// Attempt to get from db if not in cache/ expired
			dynamoProductList, err := pCache.productStorage.Get(mCollection, domain)
			if err != nil {
				return nil, err
			}
			return dynamoProductList, nil
		},
	}
	start := time.Now()
	err := pCache.codec.Once(&item)
	// Note the relevant dependency metrics
	mCollection.AddDependencyCallMetrics(err, time.Since(start), RedisGet)
	if err != nil {
		return nil, err
	}
	return *productList, nil
}

// Ping verifies the Redis cache can be reached
func (pCache *RedisProductCache) Ping(mCollection *metrics.MetricCollection) bool {
	start := time.Now()
	_, err := pCache.client.Ping().Result()
	// Note the relevant dependency metrics
	mCollection.AddDependencyCallMetrics(err, time.Since(start), RedisPing)
	if err != nil {
		return false
	}
	return true
}

// GetCacheStats obtains stats about the cache
func (pCache *RedisProductCache) GetCacheStats() *ProductCacheStats {
	stats := pCache.codec.Stats()
	cacheStats := ProductCacheStats{
		Hits:        stats.Hits,
		Misses:      stats.Misses,
		LocalHits:   stats.LocalHits,
		LocalMisses: stats.LocalMisses,
	}
	return &cacheStats
}

// GetMetrics returns cache hit and miss metrics.
func (pCache *RedisProductCache) GetMetrics() *metrics.MetricCollection {
	updatedStats := pCache.GetCacheStats()
	if pCache.stats == *updatedStats {
		return nil
	}

	diff := updatedStats.Sub(&pCache.stats)
	pCache.stats = *updatedStats

	metricsCollection := metrics.NewMetricCollection("ProductCache")
	metricsCollection.AddDependencyNumberMetric("CacheHits", float64(diff.Hits), "ProductCache")
	metricsCollection.AddDependencyNumberMetric("CacheMisses", float64(diff.Misses), "ProductCache")
	metricsCollection.AddDependencyNumberMetric("CacheLocalHits", float64(diff.LocalHits), "ProductCache")
	metricsCollection.AddDependencyNumberMetric("CacheLocalMisses", float64(diff.LocalMisses), "ProductCache")
	return metricsCollection
}

// Sub creates a new ProductCacheStats with the difference of this instance and another instance.
func (stats *ProductCacheStats) Sub(other *ProductCacheStats) *ProductCacheStats {
	return &ProductCacheStats{
		Hits:        stats.Hits - other.Hits,
		Misses:      stats.Misses - other.Misses,
		LocalHits:   stats.LocalHits - other.LocalHits,
		LocalMisses: stats.LocalMisses - other.LocalMisses,
	}
}

func getCacheExpiration(minTime int, maxTime int, timeUnit time.Duration) time.Duration {
	randNumber := rand.Intn(maxTime-minTime) + minTime
	return time.Duration(randNumber) * timeUnit
}
