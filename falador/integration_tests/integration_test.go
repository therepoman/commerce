package integrationtests

import (
	"code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/util"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/systems/sandstorm/manager"
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"
)

const (
	LoadTestDuration           = "1m"
	LoadTestConcurrency        = 2
	IntegrationTestTuid        = "99526743" // twitch.tv/ampt
	IntegrationTestDomain      = "IntegrationTestDomain"
	IntegrationTestErrorDomain = "IntegrationTestErrorDomain"
	TestSku1                   = "TestSku1"
	DisplayName1               = "DisplayName1"
	TestSku2                   = "TestSku2"
	DisplayName2               = "DisplayName2"
	TestSku3                   = "TestSku3"
	DisplayName3               = "DisplayName3"
)

func TestHealth(t *testing.T) {
	testDuration, err := time.ParseDuration(LoadTestDuration)
	if err != nil {
		panic(err)
	}

	authedCtx := getSignedTokenFromCartman(testDuration)

	endpoint := os.Getenv("STAGING_ENDPOINT")
	fmt.Printf("STAGING_ENDPOINT environment variable: %v", endpoint)
	rand.Seed(time.Now().Unix())
	cost1 := falador.Cost{Amount: rand.Int63n(100) + 1, Type: "bits"}
	cost2 := falador.Cost{Amount: rand.Int63n(250) + 1, Type: "bits"}
	cost3 := falador.Cost{Amount: rand.Int63n(10000) + 1, Type: "bits"}
	product1 := falador.Product{Domain: IntegrationTestDomain, Sku: TestSku1, Cost: &cost1, DisplayName: DisplayName1}
	product2 := falador.Product{Domain: IntegrationTestDomain, Sku: TestSku2, Cost: &cost2, DisplayName: DisplayName2}
	product3 := falador.Product{Domain: IntegrationTestDomain, Sku: TestSku3, Cost: &cost3, DisplayName: DisplayName3}

	Convey("Given a falador server", t, func() {
		Convey("And given an http client", func() {
			response, err := http.Get("http://" + endpoint + "/health")
			So(err, ShouldBeNil)

			Convey("Service health should return ok", func() {
				defer response.Body.Close()
				body, err := ioutil.ReadAll(response.Body)
				So(err, ShouldBeNil)
				So(string(body), ShouldEqual, "OK")
			})
		})

		Convey("And given a twirp client", func() {
			client := falador.NewFaladorProtobufClient("http://"+endpoint, &http.Client{})

			Convey("When no product exists", func() {
				Convey("GetProduct returns an error", func() {
					_, err := client.GetProduct(authedCtx, &falador.GetProductRequest{Domain: IntegrationTestErrorDomain, Sku: "TestSku"})
					So(err, ShouldNotBeNil)
				})

				Convey("ListProducts returns an empty list", func() {
					response, err := client.ListProducts(authedCtx, &falador.ListProductsRequest{Domain: IntegrationTestErrorDomain})
					So(err, ShouldBeNil)
					So(response.Products, ShouldBeEmpty)
				})
			})

			Convey("And Given 2 new products", func() {
				_, err := client.PutProduct(authedCtx, &falador.PutProductRequest{Product: &product1})
				So(err, ShouldBeNil)

				_, err = client.PutProduct(authedCtx, &falador.PutProductRequest{Product: &product2})
				So(err, ShouldBeNil)

				Convey("When we request them", func() {
					fetched1, err1 := client.GetProduct(authedCtx, &falador.GetProductRequest{
						Domain: product1.Domain,
						Sku:    product1.Sku,
					})
					fetched2, err2 := client.GetProduct(authedCtx, &falador.GetProductRequest{
						Domain: product2.Domain,
						Sku:    product2.Sku,
					})

					Convey("We should get them back", func() {
						So(*fetched1, ShouldEqualProduct, product1)
						So(err1, ShouldBeNil)
						So(*fetched2, ShouldEqualProduct, product2)
						So(err2, ShouldBeNil)
					})
				})

				Convey("When we update those products", func() {
					product1.Cost.Amount = rand.Int63n(10000) + 1
					updated1, err1 := client.PutProduct(authedCtx, &falador.PutProductRequest{
						Product: &product1,
					})
					product2.Cost.Amount = rand.Int63n(10000) + 1
					updated2, err2 := client.PutProduct(authedCtx, &falador.PutProductRequest{
						Product: &product2,
					})

					Convey("They should be updated successfully", func() {
						So(*updated1.Product, ShouldEqualProduct, product1)
						So(err1, ShouldBeNil)
						So(*updated2.Product, ShouldEqualProduct, product2)
						So(err2, ShouldBeNil)
					})
				})

				Convey("When we list the products", func() {
					response, err := client.ListProducts(authedCtx, &falador.ListProductsRequest{Domain: IntegrationTestDomain})

					Convey("We should get them both back", func() {
						So(err, ShouldBeNil)
						So(len(response.Products), ShouldEqual, 2)
						So(&product1, ShouldBeIn, response.Products)
						So(&product2, ShouldBeIn, response.Products)
						So(&product3, ShouldNotBeIn, response.Products)
					})
				})

				Convey("Succeeds when under load for "+LoadTestDuration+" with "+strconv.Itoa(LoadTestConcurrency)+" concurrent calls", func() {
					var callStats CallStats

					loadRunner := util.NewLoadRunner(LoadTestConcurrency, testDuration, func(parameters []interface{}) {
						localStats := parameters[0].(*CallStats)
						threadClient := parameters[1].(falador.Falador)

						request := falador.ListProductsRequest{Domain: IntegrationTestDomain}
						response, err := threadClient.ListProducts(context.Background(), &request)
						localStats.Calls++
						if err != nil {
							localStats.Errors++
						} else {
							if response == nil || response.Products == nil || len(response.Products) != 2 {
								localStats.BadResponse++
							} else if !IsProductInList(&product1, response.Products) {
								localStats.Product1Missing++
							} else if !IsProductInList(&product2, response.Products) {
								localStats.Product2Missing++
							}
						}

						requestNonExistentDomain := falador.ListProductsRequest{Domain: IntegrationTestErrorDomain}
						response, err = threadClient.ListProducts(context.Background(), &requestNonExistentDomain)
						localStats.Calls++
						if err != nil {
							localStats.Errors++
						} else {
							if response == nil {
								localStats.NonExistentDomainBadResponse++
							}
						}
					})
					loadRunner.ThreadContextSetup = func(index int) []interface{} {
						return []interface{}{
							new(CallStats),
							falador.NewFaladorProtobufClient("http://"+endpoint, &http.Client{}),
						}
					}
					loadRunner.ThreadContextCleanup = func(index int, parameters []interface{}) {
						localStats := parameters[0].(*CallStats)
						localStats.LatencySumSeconds = testDuration.Seconds()
						callStats.Accumulate(localStats)
					}
					loadRunner.Run()

					tps := float64(callStats.Calls) / testDuration.Seconds()
					latency := callStats.LatencySumSeconds / float64(callStats.Calls)
					fmt.Printf(" %d calls made, %.2f tps, %.3f latency(s)", callStats.Calls, tps, latency)
					So(callStats.Errors, ShouldEqual, 0)
					So(callStats.BadResponse, ShouldEqual, 0)
					So(callStats.Product1Missing, ShouldEqual, 0)
					So(callStats.Product2Missing, ShouldEqual, 0)
					So(callStats.Calls, ShouldBeGreaterThan, testDuration.Seconds()*LoadTestConcurrency)
					So(tps, ShouldBeGreaterThan, 50)
				})
			})

		})
	})
}

type CallStats struct {
	mutex                        sync.Mutex
	Calls                        int
	Errors                       int
	BadResponse                  int
	NonExistentDomainBadResponse int
	Product1Missing              int
	Product2Missing              int
	LatencySumSeconds            float64
}

func (callStats *CallStats) Accumulate(other *CallStats) {
	callStats.mutex.Lock()
	callStats.Calls += other.Calls
	callStats.Errors += other.Errors
	callStats.BadResponse += other.BadResponse
	callStats.Product1Missing += other.Product1Missing
	callStats.Product2Missing += other.Product2Missing
	callStats.NonExistentDomainBadResponse += other.NonExistentDomainBadResponse
	callStats.LatencySumSeconds += other.LatencySumSeconds
	callStats.mutex.Unlock()
}

func IsProductInList(product *falador.Product, products []*falador.Product) bool {
	for _, other := range products {
		if reflect.DeepEqual(other, product) {
			return true
		}
	}
	return false
}

func getSignedTokenFromCartman(testDuration time.Duration) context.Context {
	encoder, err := makeNewCartmanEncoder()
	if err != nil {
		log.Fatal("Error making cartman Encoder")
	}
	token := encoder.Encode(goauthorization.TokenParams{
		Exp:    time.Now().Add(testDuration + time.Minute*2),
		Nbf:    time.Now(),
		Aud:    []string{"code.justin.tv/web/cartman"},
		Sub:    IntegrationTestTuid,
		Claims: nil,
	})
	serializedToken, err := token.String()
	if err != nil {
		log.Fatal("Error serializing token")
	}
	header := make(http.Header)
	header.Set("Twitch-Authorization", serializedToken)
	authedCtx := context.Background()
	authedCtx, err = twirp.WithHTTPRequestHeaders(authedCtx, header)
	if err != nil {
		log.Fatal("Error making CTX with headers")
	}
	return authedCtx
}

func makeNewCartmanEncoder() (goauthorization.Encoder, error) {
	// Set up Sandstorm SDK
	awsConfig := &aws.Config{Region: aws.String("us-west-2")}
	stsclient := sts.New(session.New(awsConfig))
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/falador-staging-role",
		Client:       stsclient,
	}
	credentials := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(credentials)

	sandstormManager := manager.New(manager.Config{
		AWSConfig:   awsConfig,
		ServiceName: "falador",
	})

	cartmanPublicKey, err := sandstormManager.Get("identity/cartman/staging/hmac_secret_key")
	if err != nil {
		fmt.Println("Error fetching cartman public key. Bailing")
		panic(err)
	}

	return goauthorization.NewEncoder("HS512", "code.justin.tv/web/cartman", cartmanPublicKey.Plaintext)
}

func ShouldEqualProduct(actual interface{}, expected ...interface{}) string {
	actualProduct, ok := actual.(falador.Product)
	if !ok {
		return "Actual should be a product"
	}

	expectedProduct, ok := expected[0].(falador.Product)
	if !ok {
		return "Expected should be a product"
	}

	if actualProduct.Domain != expectedProduct.Domain {
		return "Actual domain should match expected domain"
	}

	if actualProduct.Sku != expectedProduct.Sku {
		return "Actual SKU should equal expected SKU"
	}

	if actualProduct.DisplayName != expectedProduct.DisplayName {
		return "Actual DisplayName should equal expected DisplayName"
	}

	if actualProduct.Cost.Amount != expectedProduct.Cost.Amount {
		return "Actual cost amount should equal expected cost amount"
	}

	if actualProduct.Cost.Type != expectedProduct.Cost.Type {
		return "Actual cost type should equal expected cost type"
	}

	return ""
}
