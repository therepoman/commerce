import argparse
import boto3
import json
import StringIO
import subprocess
import sys
import tarfile
import threading
import time

endpoint_map = {
    'staging' : 'http://falador-staging.internal.justin.tv/twirp/code.justin.tv.commerce.falador.Falador/',
    'prod' : 'http://falador-prod.internal.justin.tv/twirp/code.justin.tv.commerce.falador.Falador/',
}

targets_tar_filename = '/tmp/vegeta-load-test-targets.tar'
remote_path = '/tmp'
remote_vegeta_filename = remote_path + '/vegeta'
remote_tar_filename = remote_path + '/vegeta-load-test-targets.tar'
tar_base_path = "vegeta-targets"
tar_targets_filename = tar_base_path + "/targets"
domain_filename_pattern = 'loadtests/{}-domains.txt'
get_product_method = 'GetProduct'
list_products_method = 'ListProducts'

def generate_targets(url, stage):
    print "Generating targets for: " + url

    with tarfile.open(targets_tar_filename, "w") as tar:
        targets = []
        with open(domain_filename_pattern.format(stage), 'r') as f:
            for entry_index, entry in enumerate(f):
                filename = tar_base_path + '/' + "payload-{}.json".format(entry_index)
                if ',' in entry:
                    domain, sku = entry.split(',')
                    method = get_product_method
                    payload = {"domain": domain.strip(), "sku": sku.strip()}
                else:
                    method = list_products_method
                    payload = {"domain": entry.strip()}
                add_file_to_tar(tar, filename, json.dumps(payload))
                targets.append('POST {}{}\nContent-Type: application/json\n@{}/{}\n'.format(url, method, remote_path, filename))
        add_file_to_tar(tar, tar_targets_filename, "\n".join(targets))

def add_file_to_tar(tar, name, content):
    string_file = StringIO.StringIO()
    string_file.write(content)
    string_file.seek(0)
    tarinfo=tarfile.TarInfo(name=name)
    tarinfo.mtime = time.time()
    tarinfo.size=len(string_file.buf)
    tar.addfile(tarinfo=tarinfo, fileobj=string_file)

def get_ec2_instances(profile, asg):
    session = boto3.Session(profile_name=profile)
    groups = session.client('autoscaling').describe_auto_scaling_groups(AutoScalingGroupNames=[asg])
    instances = []
    for group in groups.get('AutoScalingGroups', []):
        if group.get('AutoScalingGroupName') == asg:
            for instance in group.get('Instances', []):
                if instance.get('LifecycleState') == 'InService':
                    instances.append(instance.get('InstanceId'))
    if not instances:
        raise Exception('No instances found in {}'.format(asg))

    instance_ips = []
    instance_descriptions = session.client('ec2').describe_instances(InstanceIds=instances)
    for reservation in instance_descriptions.get('Reservations', []):
        for instance in reservation.get('Instances', []):
            for interface in instance.get('NetworkInterfaces', []):
                ip = interface.get('PrivateIpAddress')
                instance_ips.append(ip)
                print('Found EC2 instance {} at {}'.format(instance.get('InstanceId'), ip))

    return instance_ips

def bootstrap_ec2(instance_ips, key, vegeta_binary):
    for ip in instance_ips:
        host = 'ec2-user@' + ip
        print('\nChecking {} for vegeta.'.format(ip))
        if subprocess.call(['ssh', '-o', 'StrictHostKeyChecking=no', '-i', key, host, 'ls ' + remote_vegeta_filename]) != 0:
            print('Copying vegeta')
            subprocess.check_call(['scp', '-o', 'StrictHostKeyChecking=no', '-i', key, vegeta_binary, host + ':' + remote_vegeta_filename])
        print('Copying targets')
        subprocess.check_call(['scp', '-o', 'StrictHostKeyChecking=no', '-i', key, targets_tar_filename, host + ':' + remote_tar_filename])
        print('Extracting targets')
        subprocess.check_call(['ssh', '-o', 'StrictHostKeyChecking=no', '-i', key, host, 'sh -c "cd {} && tar -xf {}"'.format(remote_path, remote_tar_filename)])

def run_vegeta(vegeta_binary, instance_ips, key, run_time, tps, align_to_minute):
    if align_to_minute:
        seconds_remaining = 60 - time.gmtime()[5]
        if seconds_remaining > 0:
            print
            print('Sleeping {} seconds.'.format(seconds_remaining))
            time.sleep(seconds_remaining)
    aligned_start = time.strftime('%Y-%m-%d_%H:%M:%S')
    for step, step_tps in enumerate(tps.split(',')):
        print('\nStarting {} tps load test at {} for {}'.format(step_tps, time.strftime('%Y-%m-%d %H:%M:%S'), run_time))
        threads = []
        report_files = []
        split_tps = int(max(1, int(step_tps) / len(instance_ips)))
        for ip in instance_ips:
            report = '/tmp/vegeta-report-{}-step{}-{}tps-{}'.format(aligned_start, step, step_tps, ip)
            report_files.append(report)
            thread = threading.Thread(target=run_vegeta_on_instance, args=(ip, key, report, run_time, split_tps))
            thread.start()
            threads.append(thread)

        for thread in threads:
            thread.join()

        print_report(vegeta_binary, report_files)

def run_vegeta_on_instance(ip, key, report, run_time, tps):
    host = 'ec2-user@' + ip
    attack_command = 'ulimit -Sn 4096; {} attack -targets {} -duration {} -rate {}'.format(
        remote_vegeta_filename, remote_path + '/' + tar_targets_filename, run_time, tps)
    output_file = open(report, 'w')
    subprocess.check_call(['ssh', '-o', 'StrictHostKeyChecking=no', '-i', key, host, attack_command], stdout=output_file)
    output_file.close()

def print_report(vegeta_binary, report_files):
    command = [vegeta_binary, 'report', '-inputs', ','.join(report_files)]
    print("\n" + ' '.join(command))
    subprocess.check_call(command)

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--tps', required=True, help='''
        the desired tps. Must be an int or a comma separated list of int.
        Example: 100''')
    parser.add_argument('--run-time', required=True, help='''
        the desired time to run the load test in minutes or seconds for each of the tps listed.
        Example: 10m or 600s''')
    parser.add_argument('--stage', required=True, help='''
        the desired stage to run the test against ('staging', or 'prod')
        Example: prod''')
    parser.add_argument('--profile', required=True, help='''
        the aws profile to use for getting autoscaling group information
        Example: twitch-falador-dev''')
    parser.add_argument('--asg', required=True, help='''
        the name of the auto scaling group to use
        Example: twitch-falador-dev''')
    parser.add_argument('--vegeta-binary', required=True, help='''
        path to the vegeta binary''')
    parser.add_argument('--key', required=True, help='''
        path to .pem private key file for connecting to the ec2 instances''')
    parser.add_argument('--align-to-minute', action='store_true', help='''
        delay starting until the beginning of a minute so that the load test is roughly aligned to one-minute metrics''')
    parser.add_argument('--domain-count', help='''
        The number of domains to use in the load test''')
    args = parser.parse_args()

    if args.stage.lower() not in endpoint_map:
        print "The desired stage must be 'staging', or 'prod'"
        return

    url = endpoint_map[args.stage.lower()]
    generate_targets(url, args.stage.lower())
    ips = get_ec2_instances(args.profile, args.asg)
    bootstrap_ec2(ips, args.key, args.vegeta_binary)
    run_vegeta(args.vegeta_binary, ips, args.key, args.run_time, args.tps, args.align_to_minute)

if __name__ == "__main__":
    main(sys.argv)
