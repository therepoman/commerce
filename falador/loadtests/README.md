## Setting up for load tests
* Find an aws account to use for the EC2 instances, such as the dev account.
    * It needs access to the VPN.
    * EC2 instance limits can be found at AWS Console -> EC2 -> Limits.
* If you don't already have a key pair, create one using the EC2 section of the AWS Console.
* Create a Cloud Formation stack using `load-test-auto-scaling-group-template.json`.
    * t2 instances work well since most accounts have a high limit for them, and they can CPU burst using CPU credits.  New instances start out with about half an hour of CPU credits and slowly accumulate them.
    * It will likely need more instances than the service fleet being tested.
* Set up an aws cli/sdk profile with access to read the list of instances in the ASG (`describe_auto_scaling_groups` api).
* Install https://github.com/tsenart/vegeta and add it to the path if that's not already done.
* The endpoint urls are at the top of the script are correct, change them if needed.
* If needed, update the `*-domains.txt` files to match what's in dynamo.

## Running load tests

The script will connect to each instance, copy the vegeta binary over to it if it's missing, copy the targets, run the load test, and put together the report.

It prints the command used to generate the report.  If you want a graph, add `-reporter plot > /tmp/report.html` to the end and load it in a browser.  Note that the size of the file increases with total request count, and can easily exceed 100mb.

Quick test to make sure things are working:
```
python loadtests/generateVegetaLoadTestFiles.py --tps 10 --run-time 1s --stage staging --profile twitch-falador-dev --asg LoadTest-AutoScalingGroup-DEMIMPL41EAT --vegeta-binary `which vegeta` --key ~/.ssh/my-key.pem
```

A series of short bursts to find the point where it breaks.  This runs four 10-second load tests in a row and generates a report for each part:
```
python loadtests/generateVegetaLoadTestFiles.py --tps 1000,2000,3000,4000, --run-time 10s --stage staging --profile twitch-falador-dev --asg LoadTest-AutoScalingGroup-DEMIMPL41EAT --vegeta-binary `which vegeta` --key ~/.ssh/my-key.pem
```

Similar to above, but somewhat aligned to cloudwatch data points so that you can compare tps with other graphs like cpu.  Runs 58 seconds for each step to allow overhead for making the report and restarting.
```
python loadtests/generateVegetaLoadTestFiles.py --tps 1000,2000,3000,4000, --run-time 58s --stage staging --profile twitch-falador-dev --asg LoadTest-AutoScalingGroup-DEMIMPL41EAT --vegeta-binary `which vegeta` --key ~/.ssh/my-key.pem --align-to-minute
```

Once you've found the rough limit, use this to hold it for a period of time to ensure it can sustain it.  This also starts at roughly the beginning of a minute so the cloudwatch graphs look nice.
```
python loadtests/generateVegetaLoadTestFiles.py --tps 1000 --run-time 15m --stage staging --profile twitch-falador-dev --asg LoadTest-AutoScalingGroup-DEMIMPL41EAT --vegeta-binary `which vegeta` --key ~/.ssh/my-key.pem --align-to-minute
```

## Results

Table of load test results: https://w.amazon.com/bin/view/Fuel/CommerceThirdPartyOfferings/Services/Falador/Runbook/#HLoadTesting
