package models_test

import (
	"reflect"

	twirp "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/models"
	"github.com/stretchr/testify/assert"

	"testing"
)

const (
	testEmoteGroupId1 = "TestEmoteGroupId1"
	testEmoteGroupId2 = "TestEmoteGroupId2"
)

func TestTwirpModelConversions(t *testing.T) {
	inputTwirpModel := newTwirpEmoteGroupBenefit(testEmoteGroupId1)
	expectedInternalModel := newInternalEmoteGroup(testEmoteGroupId1)

	internalEmoteGroup := models.NewEmoteGroupBenefit(inputTwirpModel)
	assert.NotNil(t, internalEmoteGroup, "Internal emote group is empty")
	assert.NotEmpty(t, internalEmoteGroup.EmoteGroupID, "EmoteGroupId is empty")
	assert.Equal(t, expectedInternalModel, internalEmoteGroup, "twirp model to internal model conversion failed")

	expectedTwirpModel := internalEmoteGroup.ToTwirpModel()
	assert.Equal(t, inputTwirpModel, expectedTwirpModel, "internal model to twirp model conversion doesn't work")
}

func TestBenefitAttributesTwirpConversions(t *testing.T) {
	inputAttributeMap := newBenefitAttributes(testEmoteGroupId1)
	expectedInternalModel := newInternalEmoteGroup(testEmoteGroupId1)
	resultantEmoteGroupBenefit, err := models.NewEmoteGroupBenefitFromAttributes(inputAttributeMap)
	assert.NoError(t, err, "Error converting from benefit attributes to model")

	areEqual := reflect.DeepEqual(*expectedInternalModel, *resultantEmoteGroupBenefit)
	assert.True(t, areEqual, "Actual EmoteGroupBenefit does not match expected")

	inputAttributeMap = newBenefitAttributes(testEmoteGroupId2)
	resultantEmoteGroupBenefit, err = models.NewEmoteGroupBenefitFromAttributes(inputAttributeMap)
	assert.NoError(t, err, "Error converting from benefit attributes to model")
	areEqual = reflect.DeepEqual(*expectedInternalModel, resultantEmoteGroupBenefit)
	assert.False(t, areEqual, "Negative test for equality failed")

	inputAttributeMap = newBenefitAttributes("")
	resultantEmoteGroupBenefit, err = models.NewEmoteGroupBenefitFromAttributes(inputAttributeMap)
	assert.Error(t, err, "Empty emoteSetId did not return error on conversion")

	inputAttributeMap = newBenefitAttributes(9)
	resultantEmoteGroupBenefit, err = models.NewEmoteGroupBenefitFromAttributes(inputAttributeMap)
	assert.Error(t, err, "Incorrect emoteSetId type did not return error on conversion")

	inputAttributeMap = make(map[string]interface{})
	resultantEmoteGroupBenefit, err = models.NewEmoteGroupBenefitFromAttributes(inputAttributeMap)
	assert.Error(t, err, "Empty inputAttributeMap did not return error on conversion")

	inputAttributeMap = nil
	resultantEmoteGroupBenefit, err = models.NewEmoteGroupBenefitFromAttributes(inputAttributeMap)
	assert.Error(t, err, "Nil inputAttributeMap did not return error on conversion")
}

func newTwirpEmoteGroupBenefit(emoteGroupId string) *twirp.Benefit_EmoteGroup {
	testEmoteGroup := twirp.EmoteGroupBenefit{
		EmoteGroupId: emoteGroupId,
	}
	return &twirp.Benefit_EmoteGroup{
		EmoteGroup: &testEmoteGroup,
	}
}

func newInternalEmoteGroup(emoteGroupId string) *models.EmoteGroupBenefit {
	return &models.EmoteGroupBenefit{
		EmoteGroupID: emoteGroupId,
	}
}

func newBenefitAttributes(emoteGroupId interface{}) map[string]interface{} {
	benefitAttributes := make(map[string]interface{})
	benefitAttributes[models.EmoteGroupIdAttributeName] = emoteGroupId
	return benefitAttributes
}
