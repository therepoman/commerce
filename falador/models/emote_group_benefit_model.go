package models

import (
	"errors"

	twirp "code.justin.tv/commerce/falador/falador-twirp"
)

const (
	EmoteGroupIdAttributeName = "EmoteGroupId" // @TODO: Should probably live somewhere else, perhaps the DAO and be exported from there
)

type EmoteGroupBenefit struct {
	EmoteGroupID string `dynamo:"EmoteGroupId"`
}

func NewEmoteGroupBenefit(twirpEmoteGroupBenefit *twirp.Benefit_EmoteGroup) *EmoteGroupBenefit {
	return &EmoteGroupBenefit{
		EmoteGroupID: twirpEmoteGroupBenefit.EmoteGroup.GetEmoteGroupId(),
	}
}

func (emoteGroupBenefit *EmoteGroupBenefit) ToTwirpModel() *twirp.Benefit_EmoteGroup {
	emoteGroup := twirp.EmoteGroupBenefit{
		EmoteGroupId: emoteGroupBenefit.EmoteGroupID,
	}
	return &twirp.Benefit_EmoteGroup{
		EmoteGroup: &emoteGroup,
	}
}

func NewEmoteGroupBenefitFromAttributes(benefitAttributes map[string]interface{}) (*EmoteGroupBenefit, error) {
	if benefitAttributes == nil {
		return nil, errors.New("BenefitAttributes is empty")
	}

	emoteGroupId, ok := benefitAttributes[EmoteGroupIdAttributeName].(string)
	if !ok || emoteGroupId == "" {
		return nil, errors.New("BenefitAttributes does not contain required fields")
	}

	emoteGroupBenefit := EmoteGroupBenefit{
		EmoteGroupID: emoteGroupId,
	}
	return &emoteGroupBenefit, nil
}
