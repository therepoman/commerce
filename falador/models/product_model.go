package models

import (
	twirp "code.justin.tv/commerce/falador/falador-twirp"
	"time"
)

// Cost is our internal cost model used by the Product
type Cost struct {
	Type   string
	Amount int64
}

// Product is our internal product model used with DynamoDB
type Product struct {
	Domain        string `dynamo:"DomainId,hash"`
	Sku           string `dynamo:"SKU,range"`
	Cost          *Cost  `dynamo:"Cost"`
	InDevelopment bool   `dynamo:"InDevelopment"`
	DisplayName   string `dynamo:"DisplayName"`
	Expiration    string `dynamo:"Expiration"`
	LastUpdated   string `dynamo:"LastUpdated"`
	LastEditor    string `dynamo:"LastEditor"`
	Broadcast     bool   `dynamo:"Broadcast"`
}

// NewProduct constructs a new internal product from a Twirp product model
func NewProduct(twirpProduct *twirp.Product, tuid string) *Product {
	twirpCost := twirpProduct.GetCost()

	var internalCost *Cost = nil
	if twirpCost != nil {
		internalCost = &Cost{
			Amount: twirpCost.GetAmount(),
			Type:   twirpCost.GetType(),
		}
	}

	return &Product{
		Domain:        twirpProduct.GetDomain(),
		Sku:           twirpProduct.GetSku(),
		Cost:          internalCost,
		InDevelopment: twirpProduct.GetInDevelopment(),
		DisplayName:   twirpProduct.GetDisplayName(),
		Expiration:    twirpProduct.GetExpiration(),
		LastUpdated:   time.Now().UTC().Format(time.RFC3339),
		LastEditor:    tuid,
		Broadcast:     twirpProduct.GetBroadcast(),
	}
}

// IsExpired determines whether the current product is expired
func (product *Product) IsExpired() bool {
	// If the expiration is empty, then the item is not expired
	if product.Expiration == "" {
		return false
	}
	// Attempt to parse the time as RFC3339
	expiration, err := time.Parse(time.RFC3339, product.Expiration)
	// If an error arises in parsing the expiration, then the product could be expired
	if err != nil {
		return true
	}
	// If no errors arose, check whether the expiration is before the current time. If so, it's expired
	if expiration.Before(time.Now().UTC()) {
		return true
	}
	// Finally, if we got this far, the item has a future expiration date so it's not expired yet
	return false
}

// GetTwirpProductModel constructs a new Twirp product from an Product
func (internalProduct Product) GetTwirpProductModel() *twirp.Product {
	var cost *twirp.Cost = nil
	if internalProduct.Cost != nil {
		cost = &twirp.Cost{
			Amount: internalProduct.Cost.Amount,
			Type:   internalProduct.Cost.Type,
		}
	}
	return &twirp.Product{
		Domain:        internalProduct.Domain,
		Sku:           internalProduct.Sku,
		Cost:          cost,
		InDevelopment: internalProduct.InDevelopment,
		DisplayName:   internalProduct.DisplayName,
		Expiration:    internalProduct.Expiration,
		Broadcast:     internalProduct.Broadcast,
	}
}
