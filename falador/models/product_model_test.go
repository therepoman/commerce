package models_test

import (
	twirp "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/models"

	"github.com/stretchr/testify/assert"

	"testing"
	"time"
)

const (
	domain     = "domain"
	sku        = "sku1"
	bits       = "bits"
	bitValue   = 100
	lastEditor = ""
)

func TestProductModel(t *testing.T) {
	// Create constants
	someTime := time.Now().Add(time.Hour).UTC().Format(time.RFC3339)
	currentTime := time.Now().UTC().Format(time.RFC3339)
	expectedTwirpModel := newTwirpProduct(someTime)
	expectedInternalModel := newInternalProduct(someTime, currentTime)

	// Verify twirp product -> internal product
	internalProduct := models.NewProduct(expectedTwirpModel, lastEditor)
	// Ensure the current time doesn't break if it's off by a few seconds, but ensure it exists before replacing
	assert.NotNil(t, internalProduct, "Internal product is empty")
	assert.NotEmpty(t, internalProduct.LastUpdated, "Last updated time is empty")
	internalProduct.LastUpdated = currentTime
	assert.Equal(t, expectedInternalModel, internalProduct, "NewProduct returned incorrect value")

	// Verify internal product -> twirp product
	expectedOriginalTwirpProduct := internalProduct.GetTwirpProductModel()
	assert.Equal(t, expectedTwirpModel, expectedOriginalTwirpProduct, "GetTwirpProductModel returned incorrect value")
}

func TestProductExpiration(t *testing.T) {
	// Create constants
	currentTime := time.Now().UTC().Format(time.RFC3339)
	internalModel := newInternalProduct("", currentTime)

	// Verify the item is not expired since there is no expiration date
	assert.False(t, internalModel.IsExpired(), "Product should not be expired for empty expirations")

	// Verify the use case where the product expiration is incorrectly formatted
	internalModel.Expiration = "someRandomThing"
	assert.True(t, internalModel.IsExpired(), "Product should be expired for badly formatted expirations")

	// Verify the use case where the product expiration is correctly formatted and in the past
	internalModel.Expiration = "2012-11-01T22:08:41+00:00"
	assert.True(t, internalModel.IsExpired(), "Product should be expired for expirations in the past")

	// Verify the use case where the product expiration is correctly formatted and in the future
	internalModel.Expiration = time.Now().Add(time.Hour).UTC().Format(time.RFC3339)
	assert.False(t, internalModel.IsExpired(), "Product should not be expired for expirations in the future")
}

func newInternalProduct(expiration string, currentTime string) *models.Product {
	internalCost := &models.Cost{
		Amount: bitValue,
		Type:   bits,
	}

	return &models.Product{
		Domain:        domain,
		Sku:           sku,
		Cost:          internalCost,
		InDevelopment: false,
		Expiration:    expiration,
		LastUpdated:   currentTime,
		Broadcast:     false,
	}
}

func newTwirpProduct(expiration string) *twirp.Product {
	return &twirp.Product{
		Domain: domain,
		Sku:    sku,
		Cost: &twirp.Cost{
			Amount: bitValue,
			Type:   bits,
		},
		InDevelopment: false,
		Expiration:    expiration,
		Broadcast:     false,
	}
}
