package models

type ctxKey string

const (
	// AdminKey is the Admins Dynamo key
	AdminKey = "Admins"
	// UserIdCtxKey is the VerifiedUserId context key
	UserIdCtxKey = ctxKey("VerifiedUserId")
	// ExtensionIdCtxKey is the extensionId that the user is authorized to edit
	ExtensionIdCtxKey = ctxKey("VerifiedExtensionId")
)

// User is the struct Dynamo uses for manintaing users and their related domains
type User struct {
	Tuid      string   `dynamo:"Tuid,hash"`
	DomainIds []string `dynamo:"DomainIds,set"`
}

// AdminList is the struct Dynamo uses for maintaining admin tuids
type AdminList struct {
	AdminKey   string   `dynamo:"Tuid"`
	AdminTuids []string `dynamo:"AdminIds,set"`
}
