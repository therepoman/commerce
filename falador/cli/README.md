# Falador CLI

## Installation

### Dependencies

* Python
* pip
* Setuptools

If you use a Mac, you can install these all with a single command in [homebrew](https://docs.brew.sh/Homebrew-and-Python):

```bash
brew install python
```

### Install tool

1. Download the latest version in the [`dist`](./dist) directory.
2. Navigate to where you downloaded the archive and run:

```bash
unzip faladorcli-latest.zip
cd faladorcli-latest
pip install .
```

Now you should be able to run the tool with:

```bash
faladorcli
```

## Usage

### get-oauth-token

```bash
Usage: faladorcli get-oauth-token [OPTIONS]

Options:
  --help  Show this message and exit.
```

This function will get an OAuth token from Twitch.
You will need it to call the other functions by providing it to the `--token` option or by setting the `TWITCH_API_TOKEN` environment variable.
Alternatively, you can save this by adding this line to your `.bashrc` (or `.zshrc` if you use zsh):

```bash
export TWITCH_API_TOKEN="<token>"
```

### list-products

```bash
Usage: faladorcli list-products [OPTIONS]

Options:
  --token TEXT               Twitch OAuth token
  --clientid TEXT            Extension Client ID  [required]
  --format [table|csv|json]  Ouput format
  --help                     Show this message and exit.
```

This function calls falador and prints out all the products and their metadata for a given client ID.
By default it will pretty print the products in a table, but you can also print in csv or JSON format.

### put-products

 ```bash
Usage: faladorcli put-products [OPTIONS]

Options:
  --token TEXT         Twitch OAuth token
  --clientid TEXT      Extension Client ID  [required]
  --filename TEXT      Product file  [required]
  --format [csv|json]  Product file format  [required]
  --help               Show this message and exit.
```

This function reads a csv or JSON file and puts each product into falador for a given client ID.

Here is an example csv file:

```csv
domain,sku,cost.amount,cost.type,inDevelopment,displayName,broadcast
twitch.ext.dppeppelTest,test1,1,bits,True,Test 1,
twitch.ext.dppeppelTest,test2,1,bits,True,Test 2,True
twitch.ext.dppeppelTest,test3,1,bits,True,Test 3,
```

Here is an example JSON file:

```json
{
  "products": [
    {
      "domain": "twitch.ext.dppeppelTest",
      "sku": "test1",
      "cost": {
        "amount": 1,
        "type": "bits"
      },
      "inDevelopment": true,
      "displayName": "Test 1"
    },
    {
      "domain": "twitch.ext.dppeppelTest",
      "sku": "test2",
      "cost": {
        "amount": 1,
        "type": "bits"
      },
      "inDevelopment": true,
      "displayName": "Test 2",
      "broadcast": true
    },
    {
      "domain": "twitch.ext.dppeppelTest",
      "sku": "test3",
      "cost": {
        "amount": 1,
        "type": "bits"
      },
      "inDevelopment": true,
      "displayName": "Test 3"
    }
  ]
}
```