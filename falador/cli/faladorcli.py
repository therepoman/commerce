from tabulate import tabulate
from webbrowser import open_new

import click
import csv
import json
import requests
import sys


API_URL = 'https://api.twitch.tv/v5/bits/extensions'
OAUTH_AUTH_URI = 'https://id.twitch.tv/oauth2/authorize?client_id={}&redirect_uri={}&response_type=token'
OAUTH_CLIENT_ID = 'kjez8e2msun1hjaarv2h89pheb6060'
OAUTH_REDIRECT_URI = 'https://localhost'
CSV_FIELD_NAMES = ['domain', 'sku', 'cost.amount', 'cost.type',
                   'inDevelopment', 'displayName', 'broadcast']


@click.group()
def cli():
    pass


@cli.command('get-token')
def getOAuthToken():
    open_new(OAUTH_AUTH_URI.format(OAUTH_CLIENT_ID, OAUTH_REDIRECT_URI))
    response_url = click.prompt("Paste the full callback URL from your browser")
    _, _, token = response_url.partition('access_token=')
    if token == '':
        click.echo('Token not found')
        exit(-1)
    token = token.split('&')[0]
    click.echo('OAuth token: ' + token)
    click.echo(
        'Add "export TWITCH_API_TOKEN="{}"" to your .bashrc file to save this.'.format(token))


@cli.command('put-products')
@click.option('--token', envvar='TWITCH_API_TOKEN', help='Twitch OAuth token')
@click.option('--clientid', required=True, help='Extension Client ID')
@click.option('--filename', required=True, help='Product file')
@click.option('--format', required=True, type=click.Choice(['csv', 'json']), help='Product file format')
def putProducts(token, clientid, filename, format):
    if not token:
        click.echo('Missing Twitch OAuth token.')
        click.echo(
            'Please provide one with the --token option or by setting the TWITCH_API_TOKEN environment variable.')
        exit(-1)

    products = __loadFile(filename, format)
    headers = {
        'Client-ID': clientid,
        'Authorization': 'OAuth ' + token
    }
    url = '{}/twitch.ext.{}/products/put'.format(API_URL, clientid)

    for product in products:
        resp = requests.post(url, headers=headers, json={'product': product})
        if resp.status_code != requests.codes.ok:
            click.echo('Failed to put product with sku[{}]: {} {}:'.format(
                product['sku'], resp.status_code, resp.reason), err=True)
            click.echo('\t' + resp.text, err=True)
        else:
            click.echo('Successfully put product with sku[{}]'.format(
                product['sku']))


@cli.command('list-products')
@click.option('--token', envvar='TWITCH_API_TOKEN', help='Twitch OAuth token')
@click.option('--clientid', required=True, help='Extension Client ID')
@click.option('--format', default='table', type=click.Choice(['table', 'csv', 'json']), help='Ouput format')
def listProducts(token, clientid, format):
    if not token:
        click.echo('Missing Twitch OAuth token.')
        click.echo(
            'Please provide one with the --token option or by setting the TWITCH_API_TOKEN environment variable.')
        exit(-1)

    headers = {
        'Client-ID': clientid,
        'Authorization': 'OAuth ' + token
    }
    url = '{}/twitch.ext.{}/products'.format(API_URL, clientid)
    resp = requests.get(url, headers=headers)

    if resp.status_code != requests.codes.ok:
        click.echo('Failed to get products for clientid[{}]. {} {}:'.format(
            clientid, resp.status_code, resp.reason), err=True)
        click.echo('\t' + resp.text, err=True)
        sys.exit(-1)

    if format == 'table':
        table = [__marshalProduct(p) for p in resp.json()['products']]
        click.echo(tabulate(table, headers='keys', tablefmt='presto'))
    if format == 'csv':
        table = [__marshalProduct(p) for p in resp.json()['products']]
        w = csv.DictWriter(sys.stdout, fieldnames=CSV_FIELD_NAMES)
        w.writeheader()
        w.writerows(table)
    if format == 'json':
        click.echo(json.dumps(resp.json()))


def __marshalProduct(product):
    data = product.copy()

    # flatten 'cost'
    data['cost.amount'] = product['cost']['amount']
    data['cost.type'] = product['cost']['type']
    del data['cost']

    return data


def __unmarshalProduct(data):
    product = data.copy()

    # "unflatten" 'cost'
    product['cost'] = {
        'type': data['cost.type']
    }
    try:
        product['cost']['amount'] = int(data['cost.amount'])
    except ValueError:
        product['cost']['amount'] = -1

    del product['cost.amount']
    del product['cost.type']

    return {k: v for k, v in product.items() if v}


def __validateProduct(product):
    errors = []

    if ('domain' not in product
        or product['domain'] is None
            or product['domain'] == ''):
        errors.append('"domain" is missing')

    if ('sku' not in product
        or product['sku'] is None
            or product['sku'] == ''):
        errors.append('"sku" is missing')
    elif len(product['sku']) > 256:
        errors.append('"sku" must be shorter than 256 characters')

    if ('cost' not in product
        or 'amount' not in product['cost']
        or product['cost']['amount'] is None
            or product['cost']['amount'] == ''):
        errors.append('"cost.amount" is missing')
    elif product['cost']['amount'] < 1 or product['cost']['amount'] > 10000:
        errors.append('"cost.amount" must be a number between 1 and 10,000')

    if ('cost' not in product
        or 'type' not in product['cost']
        or product['cost']['type'] is None
            or product['cost']['type'] == ''):
        errors.append('"cost.type" is missing')
    elif product['cost']['type'] != 'bits':
        errors.append('"cost.type" must be bits')

    if ('inDevelopment' not in product
        or product['inDevelopment'] is None
            or product['inDevelopment'] == ''):
        errors.append('"inDevelopment" is missing')
    elif product['inDevelopment'] != 'True' and product['inDevelopment'] != 'False':
        errors.append('"inDevelopment" must be True or False')

    if ('displayName' not in product
        or product['displayName'] is None
            or product['displayName'] == ''):
        errors.append('"displayName" is missing')
    elif len(product['displayName']) > 256:
        errors.append('"displayName" must be shorter than 256 characters')

    if ('broadcast' not in product
        or product['broadcast'] is None
            or product['broadcast'] == ''):
        errors.append('"broadcast" is missing')
    elif product['broadcast'] != 'True' and product['broadcast'] != 'False':
        errors.append('"broadcast" must be True or False')

    return errors


def __loadFile(filename, format):
    try:
        with open(filename) as f:
            if format == 'csv':
                data = csv.DictReader(f)
                products = [__unmarshalProduct(row) for row in data]
            if format == 'json':
                products = json.load(f)['products']

            errorsFound = False
            prevDomain = ''
            for i, product in enumerate(products):
                if prevDomain and prevDomain != product['domain']:
                    click.echo('\nERROR: Multiple domains found!')
                    click.echo('A file should only contain products from a single domain.')
                    click.echo('{} does not match {}'.format(prevDomain, product['domain']))
                    sys.exit(-1)
                elif product['domain']:
                    prevDomain = product['domain']

                errors = __validateProduct(product)
                if errors:
                    if not errorsFound:
                        errorsFound = True
                        click.echo('Validation errors found in file!')
                    click.echo('\nErrors found for product {} with SKU "{}":'.format(
                        i + 1, product.get('sku', '')))
                    for error in errors:
                        click.echo('\t' + error)
            if errorsFound:
                sys.exit(-1)
            return products
    except OSError:
        click.echo('Error reading file')
        sys.exit(-1)
