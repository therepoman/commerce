from setuptools import setup

setup(
    name='faladorcli',
    url='https://git.xarth.tv/commerce/falador/tree/master/cli',
    author='David Peppel',
    author_email='dppeppel@justin.tv',
    version='0.2',
    py_modules=['faladorcli'],
    install_requires=[
        'Click',
        'requests',
        'tabulate'
    ],
    entry_points='''
        [console_scripts]
        faladorcli=faladorcli:cli
    ''',
)
