package metrics_test

import (
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/mocks"
	"code.justin.tv/commerce/falador/util"

	"bufio"
	"bytes"
	"errors"
	"os"
	"sync/atomic"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	LoadTestBatchSize      = 100
	LoadTestCollectionSize = 20
	LoadTestConcurrency    = 10
	LoadTestDuration       = time.Second
)

func TestBatchingMetricClient(t *testing.T) {
	Convey("When testing the batching metric client", t, func() {
		Convey("Without auto flush", func() {
			mockMetricsClient := new(mocks.MetricsClient)
			batchingMetricClient := &metrics.BatchingMetricClient{
				DelegateClient:  mockMetricsClient,
				MaxDataPerBatch: 2,
				Batch:           []*metrics.MetricDatum{},
			}

			Convey("The client should batch metrics", func() {
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(1))
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(1))
				mockMetricsClient.On("PublishAllMetrics", mock.Anything).Return(nil).Twice()
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(1))
			})

			Convey("The client should send metrics on shutdown", func() {
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(1))
				mockMetricsClient.On("PublishAllMetrics", mock.Anything).Return(nil).Once()
			})

			Convey("The client should not split large collections", func() {
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(10))
				mockMetricsClient.On("PublishAllMetrics", mock.Anything).Return(nil).Twice()
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(10))
			})

			Convey("The client should log error messages", func() {
				var buffer bytes.Buffer
				writer := bufio.NewWriter(&buffer)
				log.SetFormatter(&log.JSONFormatter{})
				log.SetOutput(writer)

				var mockCalled NonBlockingEvent
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(2))
				mockMetricsClient.On("PublishAllMetrics", mock.Anything).
					Run(func(mock.Arguments) { mockCalled.Complete() }).
					Return(errors.New("test error")).
					Once()
				batchingMetricClient.Shutdown()

				log.SetOutput(os.Stderr)
				writer.Flush()

				logLine, err := buffer.ReadString('\n')
				So(err, ShouldBeNil)
				So(logLine, ShouldContainSubstring, "test error")
			})

			Reset(func() {
				batchingMetricClient.Shutdown()
				So(mockMetricsClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With auto flush", func() {
			mockMetricsClient := new(mocks.MetricsClient)
			batchingMetricClient := metrics.NewBatchingMetricClient(mockMetricsClient, 2, time.Millisecond*100)

			Convey("The client should auto flush metrics", func() {
				var mockCalled NonBlockingEvent
				mockMetricsClient.On("PublishAllMetrics", mock.Anything).
					Run(func(mock.Arguments) { mockCalled.Complete() }).
					Return(nil).
					Once()
				batchingMetricClient.PublishAllMetrics(CreateMetricCollection(1))
				So(mockCalled.Wait(time.Second*5), ShouldBeTrue)
				So(mockMetricsClient.AssertExpectations(t), ShouldBeTrue)
			})

			Reset(func() {
				batchingMetricClient.Shutdown()
				So(mockMetricsClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With load test configuration", func() {
			var mockMetricsClient SimpleMockMetricsClient
			batchingMetricClient := metrics.NewBatchingMetricClient(&mockMetricsClient, LoadTestBatchSize, time.Second)

			Convey("The client should be thread safe", func() {
				loadRunner := util.NewLoadRunner(LoadTestConcurrency, LoadTestDuration, func(context []interface{}) {
					batchingMetricClient.PublishAllMetrics(CreateMetricCollection(LoadTestCollectionSize))
				})
				loadRunner.Run()
				batchingMetricClient.Shutdown()

				invocations := loadRunner.GetInvocations()
				batchCount := invocations / (LoadTestBatchSize / LoadTestCollectionSize)
				So(batchCount, ShouldBeGreaterThan, 100)
				So(batchCount, ShouldAlmostEqual, mockMetricsClient.GetCallCount(), LoadTestConcurrency*2)
				So(invocations*LoadTestCollectionSize, ShouldEqual, mockMetricsClient.GetDatumCount())
			})
		})
	})
}

func CreateMetricCollection(size int) *metrics.MetricCollection {
	metricCollection := metrics.NewMetricCollection("test")
	for i := 0; i < size; i++ {
		metricCollection.AddDependencyNumberMetric("metric", 1, "dependency")
	}
	return metricCollection
}

type SimpleMockMetricsClient struct {
	CallCount  int64
	DatumCount int64
}

func (client *SimpleMockMetricsClient) PublishAllMetrics(mCollection *metrics.MetricCollection) error {
	atomic.AddInt64(&client.CallCount, 1)
	atomic.AddInt64(&client.DatumCount, int64(len(mCollection.MetricData)))
	return nil
}

func (client *SimpleMockMetricsClient) GetCallCount() int64 {
	return atomic.LoadInt64(&client.CallCount)
}

func (client *SimpleMockMetricsClient) GetDatumCount() int64 {
	return atomic.LoadInt64(&client.DatumCount)
}

type NonBlockingEvent struct {
	IsComplete int32
}

func (event *NonBlockingEvent) Complete() {
	atomic.StoreInt32(&event.IsComplete, 1)
}

func (event *NonBlockingEvent) Wait(timeoutDuration time.Duration) bool {
	timeout := time.Now().Add(timeoutDuration)
	for time.Now().Before(timeout) {
		time.Sleep(time.Millisecond * 10)
		if atomic.LoadInt32(&event.IsComplete) == 1 {
			return true
		}
	}
	return false
}
