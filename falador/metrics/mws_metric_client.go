package metrics

import (
	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	"code.justin.tv/commerce/falador/config"

	"fmt"
	"time"
)

const (
	// MWSRegion represents the region for MWS (PMET) metric reporting
	MWSRegion = "PDX"
	// MWSMarketplace represents the region where our hosts live
	MWSMarketplace = "us-west-2"
	// Reporting periods
	minPeriod = mws.PeriodOneMinute
	maxPeriod = mws.PeriodOneHour
	// MWSEnvironment represents the reporting environment
	MWSEnvironment = "falador/PDX"
	// MWSOptional represents optional fields
	MWSOptional = ""
	// MWSCountMetricType is the type for count (non-latency) metrics
	MWSCountMetricType = "Count"
	// MWSHostName is the host name for MWS (PMET); Not really used
	MWSHostName = "AWS-ECS"
)

// MWSMetricClient that is used for creating custom metrics in MWS (PMET)
type MWSMetricClient struct {
	client        mws.IAmazonMWSGoClient
	faladorConfig config.FaladorConfig
}

// NewMWSClient constructs a new metrics client for MWS (PMET)
func NewMWSClient(client mws.IAmazonMWSGoClient, faladorConfig config.FaladorConfig) MetricsClient {
	pmet := &MWSMetricClient{}
	pmet.client = client
	pmet.faladorConfig = faladorConfig
	return pmet
}

// PublishAllMetrics publishes all metrics tracked by the given MetricCollection
func (pmet *MWSMetricClient) PublishAllMetrics(mCollection *MetricCollection) error {
	// Don't publish metrics in Local
	if pmet.faladorConfig.IsLocal() {
		return nil
	}

	// No metrics means nothing to publish
	if (mCollection.MetricData == nil) || (len(mCollection.MetricData) == 0) {
		return nil
	}

	// Establish the metric report
	metricReport := mws.NewMetricReport(pmet.faladorConfig.IsProd(), MWSHostName, minPeriod, maxPeriod, MWSOptional, MWSEnvironment, MWSOptional)

	// Convert all datums into MWS metrics and add to the report
	for _, element := range mCollection.MetricData {
		newMetric := pmet.ConstructNewMWSMetric(element)
		if newMetric == nil {
			// TODO: Log element that failed here?
			continue
		}
		metricReport.AddMetric(*newMetric)
	}

	pmet.AddMWSCallMetrics(&metricReport)

	// Publish the metrics report
	req := mws.NewPutMetricsForAggregationRequest()
	req.AddMetricReport(metricReport)
	if pmet.faladorConfig.IsOnebox() {
		oneboxMetric := metricReport
		var newMetrics []mws.Metric
		for _, metric := range oneboxMetric.Metrics {
			metric.Dimensions.DataSet = "Onebox"
			newMetrics = append(newMetrics, metric)
		}
		oneboxMetric.Metrics = newMetrics
		req.AddMetricReport(oneboxMetric)
	}

	_, err := pmet.client.PutMetricsForAggregation(req, mws.Regions[MWSRegion], pmet.faladorConfig.GetCreds())
	return err
}

// AddMWSCallMetrics updates a metric report to contain metrics about itself.
func (pmet *MWSMetricClient) AddMWSCallMetrics(metricReport *mws.MetricReport) {
	hostSpecific := false
	clientName := ""
	metric := mws.NewMetric(pmet.faladorConfig.IsProd(), MWSHostName, hostSpecific, MWSMarketplace, NameSpace, "SendMetrics", clientName, "MWS:PutMetricsForAggregation:BatchSize")
	metric.AddValue(float64(len(metricReport.Metrics) + 1))
	metric.Unit = pmet.GetMWSMetricType(NoUnit)
	metric.Timestamp = time.Now()
	metricReport.AddMetric(metric)
}

// ConstructNewMWSMetric constructs a new MWS metric from an internal metric datum
func (pmet *MWSMetricClient) ConstructNewMWSMetric(datum *MetricDatum) *mws.Metric {

	// Ensure there's a metric name and dimensions
	if (datum.Name == "") || (datum.Dimensions == nil) || (len(datum.Dimensions) == 0) {
		return nil
	}

	// Metric metadata used to initialize the metric.
	hostSpecific := false

	// Get the relevant information from the dimensions
	apiName, clientName, dependencyName := pmet.ExtractMWSInfoFromDimensions(datum.Dimensions)

	// Ensure we have an API name
	if apiName == "" {
		return nil
	}

	// If we have a dependencyName, then we're looking at a dependency metric
	metricName := datum.Name
	if dependencyName != "" {
		// No current way to easily pivot this without concatenating the dependency and metric
		metricName = fmt.Sprintf("%s:%s", dependencyName, metricName)
	}

	metric := mws.NewMetric(pmet.faladorConfig.IsProd(), MWSHostName, hostSpecific, MWSMarketplace, NameSpace, apiName, clientName, metricName)

	// Add the actual value to the metric.
	metric.AddValue(datum.Value)
	metric.Unit = pmet.GetMWSMetricType(datum.Type)
	metric.Timestamp = datum.Timestamp
	return &metric
}

// GetMWSMetricType returns the correct MWS (PMET) metric type for a given metricType
func (pmet *MWSMetricClient) GetMWSMetricType(metricType string) string {
	if metricType == TimeMS {
		return mws.UnitMilliseconds
	}
	return MWSCountMetricType
}

// ExtractMWSInfoFromDimensions returns the correct MWS (PMET) metric information from a dimension list
func (pmet *MWSMetricClient) ExtractMWSInfoFromDimensions(dimensions []*MetricDimension) (api, domain, dependency string) {
	for _, singleDimension := range dimensions {
		// Ensure the dimensions are valid
		if (singleDimension.Name == "") || (singleDimension.Value == "") {
			continue
		}
		// TODO: Make this better/ less prone to future errors when we add more dimensions
		if singleDimension.Name == APINameDimension {
			api = singleDimension.Value
		}

		if singleDimension.Name == ClientDomainDimension {
			domain = singleDimension.Value
		}

		if singleDimension.Name == DependencyDimension {
			dependency = singleDimension.Value
		}
	}
	return
}
