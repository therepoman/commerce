package metrics_test

import (
	"code.justin.tv/commerce/falador/metrics"

	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

const (
	dependency   = "someDependency"
	metric       = "someMetric"
	clientDomain = "someClient"
	someValue    = 3.0
)

func TestNewMetricCollection(t *testing.T) {
	collection := metrics.NewMetricCollection("MethodName")
	datum := collection.MetricData
	apiDimensions := collection.APIDimension

	// Validate api dimensions
	assert.NotNil(t, apiDimensions)
	assert.Equal(t, 1, len(apiDimensions), "Dimension name incorrect")
	assert.Equal(t, metrics.APINameDimension, apiDimensions[0].Name, "Dimension name incorrect")
	assert.Equal(t, "MethodName", apiDimensions[0].Value, "Dimension value incorrect")

	assert.NotNil(t, datum)
	assert.Equal(t, 0, len(datum), "Incorrect number of datum")
}

func TestAddDependencyCallMetrics(t *testing.T) {
	collection := metrics.NewMetricCollection("MethodName")

	duration := time.Since(time.Now())
	collection.AddDependencyCallMetrics(nil, duration, dependency)
	datum := collection.MetricData
	apiDimensions := collection.APIDimension

	assert.NotNil(t, datum)
	assert.Equal(t, 2, len(datum), "Incorrect number of datum")

	// Validate first datum
	firstDatum := datum[0]
	assert.NotNil(t, firstDatum)
	assert.Equal(t, metrics.LatencyMetricName, firstDatum.Name, "Incorrect metric name")
	assert.Equal(t, metrics.TimeMS, firstDatum.Type, "Incorrect metric type")
	assert.Equal(t, duration.Seconds()*1000.0, firstDatum.Value, "Incorrect metric value")
	assert.NotNil(t, firstDatum.Timestamp, "Timestamp is nil")
	firstDimension := firstDatum.Dimensions
	assert.NotNil(t, firstDimension, "Dimensions are nil")
	assert.Equal(t, 2, len(firstDimension), "Incorrect number of dimensions")
	assert.Equal(t, apiDimensions[0], firstDimension[0], "Incorrect api dimension")
	assert.Equal(t, metrics.DependencyDimension, firstDimension[1].Name, "Dimension name incorrect")
	assert.Equal(t, dependency, firstDimension[1].Value, "Dimension value incorrect")

	// Validate second datum
	secondatum := datum[1]
	assert.NotNil(t, secondatum)
	assert.Equal(t, metrics.SuccessMetricName, secondatum.Name, "Incorrect metric name")
	assert.Equal(t, metrics.NoUnit, secondatum.Type, "Incorrect metric type")
	assert.Equal(t, 1.0, secondatum.Value, "Incorrect metric value")
	assert.NotNil(t, secondatum.Timestamp, "Timestamp is nil")
	secondDimension := secondatum.Dimensions
	assert.NotNil(t, secondDimension, "Dimensions are nil")
	assert.Equal(t, 2, len(secondDimension), "Incorrect number of dimensions")
	assert.Equal(t, apiDimensions[0], secondDimension[0], "Incorrect api dimension")
	assert.Equal(t, metrics.DependencyDimension, secondDimension[1].Name, "Dimension name incorrect")
	assert.Equal(t, dependency, secondDimension[1].Value, "Dimension value incorrect")
}

func TestAddDependencyNumberMetric(t *testing.T) {
	collection := metrics.NewMetricCollection("MethodName")

	collection.AddDependencyNumberMetric(metric, someValue, dependency)
	datum := collection.MetricData
	apiDimensions := collection.APIDimension

	assert.NotNil(t, datum)
	assert.Equal(t, 1, len(datum), "Incorrect number of datum")

	// Validate datum
	firstDatum := datum[0]
	assert.NotNil(t, firstDatum)
	assert.Equal(t, metric, firstDatum.Name, "Incorrect metric name")
	assert.Equal(t, metrics.NoUnit, firstDatum.Type, "Incorrect metric type")
	assert.Equal(t, someValue, firstDatum.Value, "Incorrect metric value")
	assert.NotNil(t, firstDatum.Timestamp, "Timestamp is nil")
	firstDimension := firstDatum.Dimensions
	assert.NotNil(t, firstDimension, "Dimensions are nil")
	assert.Equal(t, 2, len(firstDimension), "Incorrect number of dimensions")
	assert.Equal(t, apiDimensions[0], firstDimension[0], "Incorrect api dimension")
	assert.Equal(t, metrics.DependencyDimension, firstDimension[1].Name, "Dimension name incorrect")
	assert.Equal(t, dependency, firstDimension[1].Value, "Dimension value incorrect")
}

func TestAddAvailabilityMetrics(t *testing.T) {
	collection := metrics.NewMetricCollection("MethodName")

	responseCode := 500
	collection.AddRequestDimension(metrics.ClientDomainDimension, clientDomain)
	collection.AddAvailabilityMetrics(responseCode)
	datum := collection.MetricData
	apiDimensions := collection.APIDimension

	assert.NotNil(t, datum)
	assert.Equal(t, 1, len(datum), "Incorrect number of datum")

	// Validate first datum
	firstDatum := datum[0]
	assert.NotNil(t, firstDatum)
	assert.Equal(t, metrics.ServerError, firstDatum.Name, "Incorrect metric name")
	assert.Equal(t, metrics.NoUnit, firstDatum.Type, "Incorrect metric type")
	assert.Equal(t, 1.0, firstDatum.Value, "Incorrect metric value")
	assert.NotNil(t, firstDatum.Timestamp, "Timestamp is nil")
	firstDimension := firstDatum.Dimensions
	assert.NotNil(t, firstDimension, "Dimensions are nil")
	assert.Equal(t, 2, len(firstDimension), "Incorrect number of dimensions")
	assert.Equal(t, apiDimensions[0], firstDimension[0], "Incorrect api dimension")
	assert.Equal(t, metrics.ClientDomainDimension, firstDimension[1].Name, "Dimension name incorrect")
	assert.Equal(t, clientDomain, firstDimension[1].Value, "Dimension value incorrect")
}

func TestAddClientCallMetrics(t *testing.T) {
	collection := metrics.NewMetricCollection("MethodName")

	duration := time.Since(time.Now())
	collection.AddRequestDimension(metrics.ClientDomainDimension, clientDomain)
	collection.AddRequestTime(duration)
	datum := collection.MetricData
	apiDimensions := collection.APIDimension

	assert.NotNil(t, datum)
	assert.Equal(t, 1, len(datum), "Incorrect number of datum")

	// Validate first datum
	firstDatum := datum[0]
	assert.NotNil(t, firstDatum)
	assert.Equal(t, metrics.LatencyMetricName, firstDatum.Name, "Incorrect metric name")
	assert.Equal(t, metrics.TimeMS, firstDatum.Type, "Incorrect metric type")
	assert.Equal(t, duration.Seconds()*1000.0, firstDatum.Value, "Incorrect metric value")
	assert.NotNil(t, firstDatum.Timestamp, "Timestamp is nil")
	firstDimension := firstDatum.Dimensions
	assert.NotNil(t, firstDimension, "Dimensions are nil")
	assert.Equal(t, 2, len(firstDimension), "Incorrect number of dimensions")
	assert.Equal(t, apiDimensions[0], firstDimension[0], "Incorrect api dimension")
	assert.Equal(t, metrics.ClientDomainDimension, firstDimension[1].Name, "Dimension name incorrect")
	assert.Equal(t, clientDomain, firstDimension[1].Value, "Dimension value incorrect")
}
