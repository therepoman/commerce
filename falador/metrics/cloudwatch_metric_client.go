package metrics

import (
	"code.justin.tv/commerce/falador/config"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

// CloudWatchMetricClient that is used for creating custom metrics in CloudWatch
type CloudWatchMetricClient struct {
	// Client for calling CloudWatch metrics
	client        *cloudwatch.CloudWatch
	faladorConfig config.FaladorConfig
}

// NewCloudWatchMetricsClient constructs a new metrics client for CloudWatch
func NewCloudWatchMetricsClient(client *cloudwatch.CloudWatch, faladorConfig config.FaladorConfig) MetricsClient {
	cw := &CloudWatchMetricClient{}
	cw.client = client
	cw.faladorConfig = faladorConfig
	return cw
}

// PublishAllMetrics publishes all metrics tracked by the given MetricCollection
func (cw *CloudWatchMetricClient) PublishAllMetrics(mCollection *MetricCollection) error {
	// Don't publish metrics in Local
	if cw.faladorConfig.IsLocal() {
		return nil
	}

	cwDatum := cw.ConvertMetricCollectionToCloudWatchMetrics(mCollection)
	// TODO: Paginate/ split metrics when over 40 KB
	// See: https://docs.aws.amazon.com/AmazonCloudWatch/latest/APIReference/API_PutMetricData.html
	if cwDatum != nil {
		_, err := cw.client.PutMetricData(&cloudwatch.PutMetricDataInput{
			MetricData: cwDatum,
			Namespace:  aws.String(NameSpace),
		})
		if err != nil {
			return err
		}
	}

	return nil
}

// ConvertMetricCollectionToCloudWatchMetrics converts a metricCollection into a metric datum usable by CloudWatch
func (cw *CloudWatchMetricClient) ConvertMetricCollectionToCloudWatchMetrics(mCollection *MetricCollection) []*cloudwatch.MetricDatum {
	// No metrics means nothing to publish
	if (mCollection.MetricData == nil) || (len(mCollection.MetricData) == 0) {
		return nil
	}

	allDatum := []*cloudwatch.MetricDatum{}
	for _, element := range mCollection.MetricData {
		newDatum := cw.ConstructNewCWMetric(element)
		if newDatum == nil {
			// TODO: Log element that failed here?
			continue
		}
		allDatum = append(allDatum, newDatum)
	}
	return allDatum
}

// ConstructNewCWMetric constructs a new CW metric from an internal metric datum
func (cw *CloudWatchMetricClient) ConstructNewCWMetric(datum *MetricDatum) *cloudwatch.MetricDatum {
	// Ensure there's a metric name and dimensions
	if (datum.Name == "") || (datum.Dimensions == nil) || (len(datum.Dimensions) == 0) {
		return nil
	}

	dimensions := []*cloudwatch.Dimension{}
	for _, singleDimension := range datum.Dimensions {
		// Ensure the dimensions are valid
		if (singleDimension.Name == "") || (singleDimension.Value == "") {
			continue
		}
		newDimension := cloudwatch.Dimension{
			Name:  aws.String(singleDimension.Name),
			Value: aws.String(singleDimension.Value),
		}
		dimensions = append(dimensions, &newDimension)

	}

	// Append the final dimension for the stage (to determine prod v staging)
	stageDimension := cloudwatch.Dimension{
		Name:  aws.String("environmentStage"),
		Value: aws.String(config.GetCurrentEnvironment()),
	}
	dimensions = append(dimensions, &stageDimension)

	// Create the single datum point
	singleDatum := cloudwatch.MetricDatum{
		MetricName: aws.String(datum.Name),
		Unit:       aws.String(cw.GetCloudWatchMetricType(datum.Type)),
		Value:      aws.Float64(datum.Value),
		Dimensions: dimensions,
	}
	singleDatum.SetTimestamp(datum.Timestamp)
	return &singleDatum
}

// GetCloudWatchMetricType returns the correct CloudWatch metric type for a given collectionType
func (cw *CloudWatchMetricClient) GetCloudWatchMetricType(collectionType string) string {
	if collectionType == TimeMS {
		return cloudwatch.StandardUnitMilliseconds
	}
	return cloudwatch.StandardUnitNone
}
