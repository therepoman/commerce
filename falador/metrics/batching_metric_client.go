package metrics

import (
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

// BatchingMetricClient holds incoming metrics for a short period of time, combines them into a batch,
// and then forwards them to another client.
type BatchingMetricClient struct {
	DelegateClient            MetricsClient
	MaxDataPerBatch           int
	MetricsFlushSleepDuration time.Duration
	BatchMutex                sync.Mutex
	Batch                     []*MetricDatum
	BatchesBeingSent          sync.WaitGroup
	IsShutdown                bool
	ShutdownMutex             sync.Mutex
}

// NewBatchingMetricClient constructs a new BatchingMetricClient.
func NewBatchingMetricClient(delegateClient MetricsClient, maxDataPerBatch int, metricsFlushSleepDuration time.Duration) *BatchingMetricClient {
	client := &BatchingMetricClient{
		DelegateClient:            delegateClient,
		MaxDataPerBatch:           maxDataPerBatch,
		MetricsFlushSleepDuration: metricsFlushSleepDuration,
		Batch:                     []*MetricDatum{},
	}
	go client.PeriodicFlush()
	return client
}

// PublishAllMetrics accepts incoming metrics to be batched.
func (client *BatchingMetricClient) PublishAllMetrics(mCollection *MetricCollection) error {
	client.BatchMutex.Lock()
	batchSize := len(client.Batch)
	if batchSize > 0 && batchSize+len(mCollection.MetricData) > client.MaxDataPerBatch {
		client.SendBatch(client.Batch)
		client.Batch = []*MetricDatum{}
	}
	client.Batch = append(client.Batch, mCollection.MetricData...)
	client.BatchMutex.Unlock()
	return nil
}

// PeriodicFlush is an infinite loop that ensures that metrics are not delayed too long.
func (client *BatchingMetricClient) PeriodicFlush() {
	var isShutdown bool
	for !isShutdown {
		client.Flush()
		time.Sleep(client.MetricsFlushSleepDuration)

		client.ShutdownMutex.Lock()
		isShutdown = client.IsShutdown
		client.ShutdownMutex.Unlock()
	}
}

// Flush synchronously sends any metrics that have been accumulated.
func (client *BatchingMetricClient) Flush() {
	var batch []*MetricDatum
	client.BatchMutex.Lock()
	if len(client.Batch) > 0 {
		batch = client.Batch
		client.Batch = []*MetricDatum{}
	}
	client.BatchMutex.Unlock()
	if batch != nil {
		client.SendBatch(batch)
	}
}

// SendBatch sends a batch of metrics.
func (client *BatchingMetricClient) SendBatch(batch []*MetricDatum) {
	client.ShutdownMutex.Lock()
	defer client.ShutdownMutex.Unlock()

	if client.IsShutdown {
		log.Warn("BatchingMetricClient.SendBatch called after Shutdown")
		return
	}

	client.BatchesBeingSent.Add(1)
	go func() {
		defer client.BatchesBeingSent.Done()
		err := client.DelegateClient.PublishAllMetrics(&MetricCollection{MetricData: batch})
		if err != nil {
			log.Error("Failed to send metrics batch: ", err)
		}
	}()
}

// Shutdown flushes metrics and waits for all batches being sent to complete.
func (client *BatchingMetricClient) Shutdown() {
	client.Flush()

	client.ShutdownMutex.Lock()
	defer client.ShutdownMutex.Unlock()

	client.IsShutdown = true
	client.BatchesBeingSent.Wait()
}
