package metrics

// AggregatedDatum represents an aggreated value and type.
type AggregatedDatum struct {
	Type  string
	Value float64
}

// AggregatedMetrics represents a set of aggregated metrics.
type AggregatedMetrics struct {
	Dependency map[string]*AggregatedDatum
}

// NewAggregatedMetrics aggregates a metrics collection that may have multiple values for the same metric name.
func NewAggregatedMetrics(metricCollection *MetricCollection) *AggregatedMetrics {
	metrics := AggregatedMetrics{Dependency: make(map[string]*AggregatedDatum)}

	for _, datum := range metricCollection.MetricData {
		for _, dimen := range datum.Dimensions {
			if dimen.Name == "Dependency" {
				metricName := dimen.Value + ":" + datum.Name
				aggregateDatum := metrics.Dependency[metricName]
				if aggregateDatum == nil {
					aggregateDatum = &AggregatedDatum{Type: datum.Type, Value: datum.Value}
					metrics.Dependency[metricName] = aggregateDatum
				} else {
					aggregateDatum.Value += datum.Value
				}
			}
		}
	}

	return &metrics
}
