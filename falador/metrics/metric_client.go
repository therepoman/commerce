package metrics

const (
	// NameSpace for Falador
	NameSpace = "falador"
)

// MetricsClient represents the interface for metrics providers
type MetricsClient interface {
	PublishAllMetrics(mCollection *MetricCollection) error
}
