package metrics

import (
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

// MetricsProvider represents the interface for getting a set of metrics covering what has happened since the previous call.
type MetricsProvider interface {
	GetMetrics() *MetricCollection
}

// MetricsPoller periodically collects metrics, publishes them and logs them.
type MetricsPoller struct {
	Client    MetricsClient
	Providers []MetricsProvider
	Ticker    *time.Ticker
}

// NewMetricsPoller constructs a new MetricsPoller.
func NewMetricsPoller(client MetricsClient, providers []MetricsProvider, interval time.Duration) *MetricsPoller {
	poller := &MetricsPoller{
		Client:    client,
		Providers: providers,
	}
	go poller.workerLoop(interval)
	return poller
}

func (poller *MetricsPoller) workerLoop(interval time.Duration) {
	// Given the current time, determine when the next collection time is that will be aligned to the given interval.
	now := time.Now()
	startTime := now.Truncate(interval).Add(interval)

	// Sleep until the collection time.
	if now.Before(startTime) {
		time.Sleep(startTime.Sub(now))
	}

	// Start a ticker to keep track of the collection times.
	poller.Ticker = time.NewTicker(interval)
	for {
		<-poller.Ticker.C
		metricData := []*MetricDatum{}
		for _, provider := range poller.Providers {
			metricsCollection := provider.GetMetrics()
			if metricsCollection != nil {
				poller.Client.PublishAllMetrics(metricsCollection)
				metricData = append(metricData, metricsCollection.MetricData...)
			}
		}
		if len(metricData) > 0 {
			metricsCollection := &MetricCollection{MetricData: metricData}
			log.WithFields(poller.getMetricsFields(metricsCollection)).Info("Metrics Poller")
		}
	}
}

func (poller *MetricsPoller) getMetricsFields(metricsCollection *MetricCollection) log.Fields {
	logEntryFields := log.Fields{}
	aggregateMetrics := NewAggregatedMetrics(metricsCollection)
	for metricName, datum := range aggregateMetrics.Dependency {
		unit := ""
		if datum.Type != NoUnit {
			unit = " " + datum.Type
		}
		fractionalDigits := -1
		if datum.Type == TimeMS {
			fractionalDigits = 0
		}
		logEntryFields[metricName] = strconv.FormatFloat(datum.Value, 'f', fractionalDigits, 64) + unit
	}
	for _, dimen := range metricsCollection.RequestDimension {
		logEntryFields[dimen.Name] = dimen.Value
	}
	return logEntryFields
}
