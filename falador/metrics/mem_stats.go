package metrics

import (
	"runtime"
	"time"
)

// MemStatsProvider is a provider for heap and garbage collector metrics.
type MemStatsProvider struct {
	Stats            runtime.MemStats
	LastPauseTotalNs uint64
}

// NewMemStatsProvider constructs a new MemStatsProvider.
func NewMemStatsProvider() *MemStatsProvider {
	provider := &MemStatsProvider{}
	runtime.ReadMemStats(&provider.Stats)
	provider.LastPauseTotalNs = provider.Stats.PauseTotalNs
	return provider
}

// GetMetrics returns heap and garbage collector metrics.
func (provider *MemStatsProvider) GetMetrics() *MetricCollection {
	runtime.ReadMemStats(&provider.Stats)

	metricsCollection := NewMetricCollection("GoLang")
	metricsCollection.AddDependencyNumberMetric("HeapAlloc", float64(provider.Stats.HeapAlloc), "MemStats")
	metricsCollection.AddDependencyNumberMetric("HeapObjects", float64(provider.Stats.HeapObjects), "MemStats")
	pauseDuration := time.Duration(provider.Stats.PauseTotalNs-provider.LastPauseTotalNs) * time.Nanosecond
	provider.LastPauseTotalNs = provider.Stats.PauseTotalNs
	metricsCollection.AddDependencyTimeMetric("GCPause", pauseDuration, "MemStats")
	return metricsCollection
}
