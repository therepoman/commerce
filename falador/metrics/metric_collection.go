package metrics

import (
	"time"
)

const (
	// UnknownAPIName represents the API name for unknown Twirp methods
	UnknownAPIName = "UnknownAPI"
	// APINameDimension for pivoting on the API name
	APINameDimension = "API"
	// ClientDomainDimension for pivoting on the client domain
	ClientDomainDimension = "ClientDomain"
	// DependencyDimension for pivoting on the dependency
	DependencyDimension = "Dependency"
	// SuccessMetricName is the name for Success, used for Availability
	SuccessMetricName = "Success"
	// ClientError is the name for a 4xx error, used for Availability
	ClientError = "Client_Error"
	// ServerError is the name for a 5xx error, used for Availability
	ServerError = "Server_Error"
	// LatencyMetricName is the name for Latency
	LatencyMetricName = "Latency"
	// Success value
	Success = 1.0
	// Failure value
	Failure = 0.0
	// TimeMS represents time in ms
	TimeMS = "MS"
	// NoUnit represents no unit on the metric
	NoUnit = "NoUnit"
)

// MetricDimension that is used for storing dimensions
type MetricDimension struct {
	// Name of the dimension
	Name string
	// Value of the dimension
	Value string
}

// MetricDatum represents a single metric with dimensions
type MetricDatum struct {
	// Dimensions associated with the metric
	Dimensions []*MetricDimension
	// Name of the metric
	Name string
	// Type of the metric (ms or )
	Type string
	// Value of the metric
	Value float64
	// Timestamp of the metric
	Timestamp time.Time
}

// MetricCollection that is used for storing metrics
type MetricCollection struct {
	// MetricData associated with the collection
	MetricData []*MetricDatum
	// APIDimension associated with the API pivot
	APIDimension []*MetricDimension
	// RequestDimension contains dimensions specific to the request
	RequestDimension []*MetricDimension
}

// NewMetricCollection constructs a new metric collection for an api name
func NewMetricCollection(apiName string) *MetricCollection {
	// Construct the MetricCollection for the given API
	metricCollection := &MetricCollection{}
	metricCollection.MetricData = []*MetricDatum{}
	metricCollection.APIDimension = []*MetricDimension{
		&MetricDimension{
			Name:  APINameDimension,
			Value: apiName,
		},
	}
	metricCollection.RequestDimension = []*MetricDimension{}

	return metricCollection
}

// AddDependencyCallMetrics adds availability and latency metrics (in ms) for a dependency but does NOT publish anything
func (m *MetricCollection) AddDependencyCallMetrics(potentialError error, duration time.Duration, dependencyName string) {
	newDimensions := m.addAdditionalDimension(DependencyDimension, dependencyName)
	dependencyLatency := m.constructNewTimeMetric(LatencyMetricName, duration.Seconds()*1000.0, newDimensions)
	m.MetricData = append(m.MetricData, &dependencyLatency)
	dependencyAvailability := m.constructNewNumberMetric(SuccessMetricName, m.getSuccessVal(potentialError), newDimensions)
	m.MetricData = append(m.MetricData, &dependencyAvailability)
	return
}

// AddDependencyNumberMetric adds a new number metric for dependencies to MetricData but does NOT publish anything
func (m *MetricCollection) AddDependencyNumberMetric(metricName string, metricValue float64, dependencyName string) {
	newDimensions := m.addAdditionalDimension(DependencyDimension, dependencyName)
	newMetric := m.constructNewNumberMetric(metricName, metricValue, newDimensions)
	m.MetricData = append(m.MetricData, &newMetric)
	return
}

// AddDependencyTimeMetric adds a new time metric for dependencies to MetricData but does NOT publish anything
func (m *MetricCollection) AddDependencyTimeMetric(metricName string, duration time.Duration, dependencyName string) {
	newDimensions := m.addAdditionalDimension(DependencyDimension, dependencyName)
	dependencyLatency := m.constructNewTimeMetric(metricName, duration.Seconds()*1000.0, newDimensions)
	m.MetricData = append(m.MetricData, &dependencyLatency)
	return
}

// AddAvailabilityMetrics adds availability metrics but does NOT publish anything
func (m *MetricCollection) AddAvailabilityMetrics(status int) {
	// Determine the correct metric name (to represent 200, 4xx, or 5xx)
	availabilityMetricName := m.getAvailabilityMetric(status)
	requestDimensions := append(m.APIDimension, m.RequestDimension...)
	availability := m.constructNewNumberMetric(availabilityMetricName, 1.0, requestDimensions)
	m.MetricData = append(m.MetricData, &availability)
	return
}

// AddRequestTime adds a latency metric (in ms) for the overall request but does NOT publish anything
func (m *MetricCollection) AddRequestTime(duration time.Duration) {
	requestDimensions := append(m.APIDimension, m.RequestDimension...)
	requestLatency := m.constructNewTimeMetric(LatencyMetricName, duration.Seconds()*1000.0, requestDimensions)
	m.MetricData = append(m.MetricData, &requestLatency)
	return
}

// addAdditionalDimension returns a list with the additional dimension
func (m *MetricCollection) addAdditionalDimension(dimensionName string, dimensionValue string) []*MetricDimension {
	newDimension := MetricDimension{
		Name:  dimensionName,
		Value: dimensionValue,
	}
	return append(m.APIDimension, &newDimension)
}

// AddRequestDimension adds another request specific dimension to the collection.
func (m *MetricCollection) AddRequestDimension(dimensionName string, dimensionValue string) {
	newDimension := MetricDimension{
		Name:  dimensionName,
		Value: dimensionValue,
	}
	m.RequestDimension = append(m.RequestDimension, &newDimension)
}

// constructNewNumberMetric construct a new metric without units
func (m *MetricCollection) constructNewNumberMetric(metricName string, metricValue float64, dimensions []*MetricDimension) MetricDatum {
	return m.constructNewMetric(metricName, metricValue, NoUnit, dimensions)
}

// constructNewTimeMetric construct a new time metric with ms as units
func (m *MetricCollection) constructNewTimeMetric(metricName string, metricValue float64, dimensions []*MetricDimension) MetricDatum {
	return m.constructNewMetric(metricName, metricValue, TimeMS, dimensions)
}

// constructNewMetric construct a new metric with the provided units
func (m *MetricCollection) constructNewMetric(metricName string, metricValue float64, unitType string, dimensions []*MetricDimension) MetricDatum {
	return MetricDatum{
		Name:       metricName,
		Type:       unitType,
		Value:      metricValue,
		Dimensions: dimensions,
		Timestamp:  time.Now().UTC(),
	}
}

// getSuccessVal returns 1.0 or 0.0 depending on whether or not error is nil
func (m *MetricCollection) getSuccessVal(potentialError error) float64 {
	if potentialError == nil {
		return Success
	}
	return Failure
}

// getAvailabilityMetric returns the correct metric name given a status code
func (m *MetricCollection) getAvailabilityMetric(status int) string {
	// status codes under 400 are successful.
	if status < 400 {
		return SuccessMetricName
	}
	// Otherwise, it's a 4xx or 5xx, handle accordingly
	if status < 500 {
		return ClientError
	}
	return ServerError
}
