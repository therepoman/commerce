# Falador

![Build Status](https://codebuild.us-west-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiWU5hU2x5dGJtd3htenFUMHA3ZG81NEtWUzFmbWNiKzBoZFZGdXhaYlVyUXBkd0YzbmVTVXl2TERwSFpJVE80NHNkTVA5NzBLM1VZS3pzSkd5LzAzZTRBPSIsIml2UGFyYW1ldGVyU3BlYyI6Ii9sWjFCZkM0Z2VpMGU0bm4iLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

![RuneFullHelm](/documentation/falador.png)

Twitch Commerce Catalog Service

## TODO
- More responsibility and resiliency with downstream dependencies: [[research doc]](https://docs.google.com/document/d/1BC7JCR0Vi60_aHTTkjoWe6yfbUKvZU9ndOTY2CHclQU/)

## Local Falador Guide
### Before Running Falador... 
 - Install the necessary dependencies:

		brew install go
		brew install docker
		brew cask install docker
		brew install redis
		brew install protobuf

 - Gain access to the following AWS accounts, create an IAM user in either/both, and add acccess_key/secret to AWS config:
        twitch-falador-dev@amazon.com (883125488860)
		twitch-falador@amazon.com (147025004534)

 - Add (at least) the dev profile to the AWS cli

		aws configure --profile twitch-falador-dev

 - Run docker (this is a standard application you run in the background). This can take a minute to start up.

		open -a Docker

 - Monitor redis if necessary (in another terminal tab)

		redis-cli ping # To verify redis is running
		redis-cli monitor

### Now to Run...
 - To built/ run without docker:

		make local

 - To built/ run with docker:

		make docker

### Run unit tests
 - To run our unit tests

        make test

 - To run DAO unit tests (requires JRE to run dynamodb local)

        make dynamodb_local_test

#### How to update the test's OAuth tokens
If the OAuth token expires, credentials of this account are stored in Sandstorm under:

        commerce/falador/staging/integ_test_falador_test_password
The secret will need to be refreshed manually as we don't have an automated way of doing this with reCaptcha in today's
update path.

To get the username and password for this test account:

1. Get the Sandstorm CLI: https://wiki.twitch.com/display/SSE/Using+Sandstorm+for+Development
2. Follow the steps earlier in the README to get credentials for twitch-falador-dev@amazon.com (883125488860)
3. Run the command: "sandstorm get --role-arn arn:aws:iam::734326455073:role/sandstorm/production/templated/role/falador-staging-role commerce/falador/staging/integ_test_falador_test_password"
