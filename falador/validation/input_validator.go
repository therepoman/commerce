package validation

import (
	ft "code.justin.tv/commerce/falador/falador-twirp"

	"github.com/twitchtv/twirp"
	"regexp"
	"time"
	"unicode/utf8"
)

// RequestInputValidator that is used for validating inputs
type RequestInputValidator struct {
	// TODO: At the moment, sku and domain validation is the same. However, we might want to split this up in the future
	productRegex *regexp.Regexp
}

const (
	// The maximum number of bits an item can cost
	MaxBits = 10000
	// The minimum number of bits an item can cost
	MinBits = 1
	// Bits type
	BitsType = "bits"
	// The maximum allowed Sku length
	MaxSkuLength = 255
	// The maximum allowed DisplayName length
	MaxDisplayNameLength = 255
)

// InputValidator represents the interface for validating different types of inputs
type InputValidator interface {
	IsValidSku(sku string) twirp.Error
	IsValidDomain(domain string) twirp.Error
	IsValidTwirpGetProductRequest(req *ft.GetProductRequest) twirp.Error
	IsValidTwirpListProductsRequest(req *ft.ListProductsRequest) twirp.Error
	IsValidTwirpPutProductRequest(req *ft.PutProductRequest) twirp.Error
}

// NewRequestInputValidator constructs a new RequestInputValidator
func NewRequestInputValidator(productInputRegex *regexp.Regexp) InputValidator {
	requestInputValidator := &RequestInputValidator{}
	requestInputValidator.productRegex = productInputRegex
	return requestInputValidator
}

// IsValidSku determines if the sku matches the provided regex
func (requestInputVal *RequestInputValidator) IsValidSku(sku string) twirp.Error {
	if !requestInputVal.productRegex.MatchString(sku) || len(sku) > MaxSkuLength {
		return twirp.InvalidArgumentError("sku", "is invalid")
	}
	return nil
}

// IsValidDomain determines if the sku matches the provided regex
func (requestInputVal *RequestInputValidator) IsValidDomain(domain string) twirp.Error {
	if !requestInputVal.productRegex.MatchString(domain) {
		return twirp.InvalidArgumentError("domain", "is invalid")
	}
	return nil
}

// IsValidTwirpGetProductRequest determines if the GetProduct request is valid
func (requestInputVal *RequestInputValidator) IsValidTwirpGetProductRequest(req *ft.GetProductRequest) twirp.Error {
	// Ensure the request is not null and contains all required parameters
	if req == nil {
		return twirp.NewError(twirp.InvalidArgument, "Request cannot be empty")
	}
	if req.GetDomain() == "" {
		return twirp.RequiredArgumentError("Domain")
	}
	if req.GetSku() == "" {
		return twirp.RequiredArgumentError("Sku")
	}

	// Ensure the domain is valid
	invalidDomain := requestInputVal.IsValidDomain(req.GetDomain())
	if invalidDomain != nil {
		return invalidDomain
	}

	// Ensure the sku is valid
	invalidSku := requestInputVal.IsValidSku(req.GetSku())
	if invalidSku != nil {
		return invalidSku
	}

	return nil
}

// IsValidTwirpListProductsRequest determines if the ListProducts request is valid
func (requestInputVal *RequestInputValidator) IsValidTwirpListProductsRequest(req *ft.ListProductsRequest) twirp.Error {
	// Ensure the request is not null and contains all required parameters
	if req == nil {
		return twirp.NewError(twirp.InvalidArgument, "Request cannot be empty")
	}
	if req.GetDomain() == "" {
		return twirp.RequiredArgumentError("Domain")
	}

	// Ensure the domain is valid
	invalidDomain := requestInputVal.IsValidDomain(req.GetDomain())
	if invalidDomain != nil {
		return invalidDomain
	}

	return nil
}

// IsValidTwirpPutProductRequest determines if the PutProducts request is valid
func (requestInputVal *RequestInputValidator) IsValidTwirpPutProductRequest(req *ft.PutProductRequest) twirp.Error {
	// Ensure the request is not null and the product is not null
	if req == nil {
		return twirp.NewError(twirp.InvalidArgument, "Request cannot be empty")
	}
	if req.GetProduct() == nil {
		return twirp.RequiredArgumentError("Product")
	}

	// Ensure the product has the required parameters
	product := req.GetProduct()
	if product.GetDomain() == "" {
		return twirp.RequiredArgumentError("Domain")
	}
	if product.GetSku() == "" {
		return twirp.RequiredArgumentError("Sku")
	}
	if product.GetDisplayName() == "" {
		return twirp.RequiredArgumentError("DisplayName")
	}

	// Ensure the domain is valid
	invalidDomain := requestInputVal.IsValidDomain(product.GetDomain())
	if invalidDomain != nil {
		return invalidDomain
	}

	// Ensure the sku is valid
	invalidSku := requestInputVal.IsValidSku(product.GetSku())
	if invalidSku != nil {
		return invalidSku
	}

	// Ensure the display name is UTF-8 compliant
	if !utf8.ValidString(product.GetDisplayName()) || len(product.GetDisplayName()) > MaxDisplayNameLength {
		return twirp.InvalidArgumentError("DisplayName", "is invalid")
	}

	// If Cost exists, ensure the cost has required parameters and is not negative
	if product.GetCost() != nil {
		cost := product.GetCost()
		if cost.GetType() == "" {
			return twirp.RequiredArgumentError("Cost Type")
		}

		if cost.GetType() != BitsType {
			return twirp.InvalidArgumentError("Cost Type", "is invalid")
		}

		if cost.GetAmount() > MaxBits || cost.GetAmount() < MinBits {
			return twirp.InvalidArgumentError("Cost Amount", "is invalid")
		}
	}

	// Ensure expiration dates are valid
	if product.GetExpiration() != "" {
		_, err := time.Parse(time.RFC3339, product.GetExpiration())
		if err != nil {
			return twirp.InvalidArgumentError("Expiration", "is invalidly formatted")
		}
	}

	return nil
}
