package validation_test

import (
	"code.justin.tv/commerce/falador/api"
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/validation"

	"github.com/stretchr/testify/assert"
	"github.com/twitchtv/twirp"
	"regexp"
	"strings"
	"testing"
	"time"
)

const (
	goodDomain      = "someDomain123.ABC_abc-"
	badDomain       = "someDomain!@#$%^&*()[]"
	goodSku         = "someSku123.ABC_abc-"
	badSku          = "someSku!@#$%^&*()[]"
	goodDisplayName = "someDisplayName私34879/()^&*^*"
	goodCost        = 100
	negativeCost    = -1
	zeroCost        = 0
	tooLargeCost    = 10001
	goodCostType    = "bits"
)

func TestIsValidTwirpListProductsRequest(t *testing.T) {
	inputValidation := validation.NewRequestInputValidator(constructRegex())

	// Ensure a valid input works
	err := inputValidation.IsValidTwirpListProductsRequest(&ft.ListProductsRequest{Domain: goodDomain})
	assert.Nil(t, err)

	// Check null input
	err = inputValidation.IsValidTwirpListProductsRequest(nil)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty domain
	err = inputValidation.IsValidTwirpListProductsRequest(&ft.ListProductsRequest{})
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for invalid domain
	err = inputValidation.IsValidTwirpListProductsRequest(&ft.ListProductsRequest{Domain: badDomain})
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)
	return
}

func TestIsValidTwirpGetProductRequest(t *testing.T) {
	inputValidation := validation.NewRequestInputValidator(constructRegex())

	// Ensure a valid input works
	err := inputValidation.IsValidTwirpListProductsRequest(&ft.ListProductsRequest{Domain: goodDomain})
	assert.Nil(t, err)

	// Check null input
	err = inputValidation.IsValidTwirpGetProductRequest(nil)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty domain
	err = inputValidation.IsValidTwirpGetProductRequest(constructGetProductRequest("", goodSku))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for invalid domain
	err = inputValidation.IsValidTwirpGetProductRequest(constructGetProductRequest(badDomain, goodSku))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty sku
	err = inputValidation.IsValidTwirpGetProductRequest(constructGetProductRequest(goodDomain, ""))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for invalid sku
	err = inputValidation.IsValidTwirpGetProductRequest(constructGetProductRequest(goodDomain, badSku))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)
	return
}

func TestIsValidPutProductRequest(t *testing.T) {
	inputValidation := validation.NewRequestInputValidator(constructRegex())

	// Ensure a valid input works
	validInput := newTwirpPutProductRequest(goodDomain, goodSku, goodCost, goodCostType, goodDisplayName)
	err := inputValidation.IsValidTwirpPutProductRequest(validInput)
	assert.Nil(t, err)

	// Check null input
	err = inputValidation.IsValidTwirpPutProductRequest(nil)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check nil product
	err = inputValidation.IsValidTwirpPutProductRequest(&ft.PutProductRequest{})
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty domain
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest("", goodSku, goodCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty sku
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, "", goodCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check negative cost
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, negativeCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check 0 cost
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, zeroCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check cost that is too large
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, tooLargeCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check min cost
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, validation.MinBits, goodCostType, goodDisplayName))
	assert.Nil(t, err)

	// Check max cost
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, validation.MaxBits, goodCostType, goodDisplayName))
	assert.Nil(t, err)

	// Check empty cost type
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, goodCost, "", goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check bad cost type
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, goodCost, "notBits", goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty display name
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, goodCost, goodCostType, ""))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check valid UTF-8 display name check
	// Taken from https://golang.org/pkg/unicode/utf8/#pkg-examples
	invalidDisplayName := string([]byte{0xff, 0xfe, 0xfd})
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, goodCost, goodCostType, invalidDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check invalid length display name
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, goodSku, goodCost, goodCostType, strings.Repeat("a", validation.MaxDisplayNameLength+1)))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for invalid domain
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(badDomain, goodSku, goodCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for invalid character sku
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, badSku, goodCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check invalid length sku
	err = inputValidation.IsValidTwirpPutProductRequest(newTwirpPutProductRequest(goodDomain, strings.Repeat("a", validation.MaxSkuLength+1), goodCost, goodCostType, goodDisplayName))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for badly formatted expiration dates
	expirationInput := validInput
	expirationInput.Product.Expiration = "someBadString"
	err = inputValidation.IsValidTwirpPutProductRequest(expirationInput)
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check for correctly formatted expiration dates
	expirationInput.Product.Expiration = time.Now().Add(time.Hour).UTC().Format(time.RFC3339)
	err = inputValidation.IsValidTwirpPutProductRequest(expirationInput)
	assert.Nil(t, err)

	return
}

func TestIsValidSku(t *testing.T) {
	inputValidation := validation.NewRequestInputValidator(constructRegex())

	// Ensure a valid input works
	err := inputValidation.IsValidSku(goodSku)
	assert.Nil(t, err)

	// Check bad sku
	err = inputValidation.IsValidSku(badSku)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty sku
	err = inputValidation.IsValidSku("")
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty sku (with spaces only)
	err = inputValidation.IsValidSku(" ")
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check long sku
	err = inputValidation.IsValidSku(strings.Repeat("a", validation.MaxSkuLength+1))
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)
	return
}

func TestIsValidDomain(t *testing.T) {
	inputValidation := validation.NewRequestInputValidator(constructRegex())

	// Ensure a valid input works
	err := inputValidation.IsValidDomain(goodDomain)
	assert.Nil(t, err)

	// Check bad domain
	err = inputValidation.IsValidDomain(badDomain)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty domain
	err = inputValidation.IsValidDomain("")
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)

	// Check empty domain (with spaces only)
	err = inputValidation.IsValidDomain(" ")
	assert.NotNil(t, err)
	twerr, _ = err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.InvalidArgument, err)
	return
}

func constructGetProductRequest(domain string, sku string) *ft.GetProductRequest {
	return &ft.GetProductRequest{
		Domain: domain,
		Sku:    sku,
	}
}

func newTwirpPutProductRequest(domain string, sku string, costAmount int64, costType string, displayName string) *ft.PutProductRequest {
	cost := &ft.Cost{
		Amount: costAmount,
		Type:   costType,
	}
	product := &ft.Product{
		Domain:        domain,
		Sku:           sku,
		Cost:          cost,
		InDevelopment: false,
		DisplayName:   displayName,
		Expiration:    "",
	}
	return &ft.PutProductRequest{product}
}

func constructRegex() *regexp.Regexp {
	return regexp.MustCompile(api.InputStringRegex)
}
