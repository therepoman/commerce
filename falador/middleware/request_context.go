package middleware

import (
	"code.justin.tv/commerce/falador/metrics"

	"context"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

var requestIDKey = new(int)
var requestMetricsKey = new(int)
var requestLoggerFieldsKey = new(int)
var requestCompleteLogEntryFieldsKey = new(int)

// StatusTrackingResponseWriter is a ResponseWriter that keeps track of the http response code.
type StatusTrackingResponseWriter struct {
	http.ResponseWriter
	// HTTP status code
	Status int
}

// WriteHeader is overridden to capture the status code.
func (w *StatusTrackingResponseWriter) WriteHeader(status int) {
	w.Status = status
	w.ResponseWriter.WriteHeader(status)
}

// RequestLoggingConfig is a configuration for what gets logged while the request is being handled.
type RequestLoggingConfig struct {
	LogSuccessfulRequests bool
}

// NewRequestLoggingConfig creates a new RequestLoggingConfig.
func NewRequestLoggingConfig(LogSuccessfulRequests bool) *RequestLoggingConfig {
	return &RequestLoggingConfig{
		LogSuccessfulRequests: LogSuccessfulRequests,
	}
}

// RequestContextMiddleware is middleware that can create a handler that adds requestID, metrics, and logging.
type RequestContextMiddleware struct {
	metricsClient         metrics.MetricsClient
	concurrentRequests    int32
	maxConcurrentRequests int32
	loggingConfig         map[string]*RequestLoggingConfig
}

// NewRequestContextMiddleware creates a middleware handler that adds requestID, metrics, and logging.
func NewRequestContextMiddleware(metricsClient metrics.MetricsClient, loggingConfig map[string]*RequestLoggingConfig) *RequestContextMiddleware {
	return &RequestContextMiddleware{
		metricsClient: metricsClient,
		loggingConfig: loggingConfig,
	}
}

// GetHandler returns the middleware's http.Handler.
func (m *RequestContextMiddleware) GetHandler() func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				requestStart := time.Now()

				concurrency := atomic.AddInt32(&m.concurrentRequests, 1)
				defer atomic.AddInt32(&m.concurrentRequests, -1)

				maxConcurrency := atomic.LoadInt32(&m.maxConcurrentRequests)
				if concurrency > maxConcurrency {
					// Best effort, ignore failure.
					atomic.CompareAndSwapInt32(&m.maxConcurrentRequests, maxConcurrency, concurrency)
				}

				// Use the URL path to determine the method name (the last part of the URL)
				splitPath := strings.Split(r.URL.Path, "/")
				methodName := splitPath[len(splitPath)-1]

				updatedContext, metricsCollection, logEntryFields :=
					InitializeRequestContext(r.Context(), methodName)

				updatedRequest := r.WithContext(updatedContext)
				statusTrackingWriter := &StatusTrackingResponseWriter{w, http.StatusOK}

				h.ServeHTTP(statusTrackingWriter, updatedRequest)

				requestTime := time.Now().Sub(requestStart)
				metricsCollection.AddRequestTime(requestTime)
				metricsCollection.AddAvailabilityMetrics(statusTrackingWriter.Status)

				err := m.metricsClient.PublishAllMetrics(metricsCollection)
				if err != nil {
					GetLogger(updatedContext).Error("Failed to publish metrics: ", err)
				}
				metricsFields := getMetricsFields(metricsCollection)

				loggingConfig := m.loggingConfig[methodName]
				if statusTrackingWriter.Status >= 400 || loggingConfig == nil || loggingConfig.LogSuccessfulRequests {
					logEntryFields["status"] = strconv.Itoa(statusTrackingWriter.Status)
					logEntryFields["request.time"] = strconv.FormatFloat(requestTime.Seconds()*1000.0, 'f', 0, 64) + " ms"
					GetLogger(updatedContext).WithFields(logEntryFields).WithFields(metricsFields).Info("Complete")
				}
			},
		)
	}
}

// GetMetrics returns MaxConcurrentRequests metrics.
func (m *RequestContextMiddleware) GetMetrics() *metrics.MetricCollection {
	maxConcurrentRequests := atomic.SwapInt32(&m.maxConcurrentRequests, 0)

	metricsCollection := metrics.NewMetricCollection("Middleware")
	metricsCollection.AddDependencyNumberMetric("MaxConcurrentRequests", float64(maxConcurrentRequests), "HttpServer")
	return metricsCollection
}

// InitializeRequestContext provides an updated context containing requestID, metrics, and logging fields.
func InitializeRequestContext(ctx context.Context, methodName string) (context.Context, *metrics.MetricCollection, log.Fields) {
	requestID := uuid.NewV4().String()

	metricsCollection := metrics.NewMetricCollection(methodName)
	loggerFields := log.Fields{"request.id": requestID}
	logEntryFields := log.Fields{"method": methodName}

	updatedContext := context.WithValue(ctx, requestMetricsKey, metricsCollection)
	updatedContext = context.WithValue(updatedContext, requestIDKey, requestID)
	updatedContext = context.WithValue(updatedContext, requestLoggerFieldsKey, loggerFields)
	updatedContext = context.WithValue(updatedContext, requestCompleteLogEntryFieldsKey, logEntryFields)

	return updatedContext, metricsCollection, logEntryFields
}

// GetRequestID returns the request ID embedded in the context.
func GetRequestID(ctx context.Context) (string, bool) {
	requestID, ok := ctx.Value(requestIDKey).(string)
	return requestID, ok
}

// GetRequestMetrics returns the MetricCollection embedded in the context.
func GetRequestMetrics(ctx context.Context) *metrics.MetricCollection {
	metricsCollection, ok := ctx.Value(requestMetricsKey).(*metrics.MetricCollection)
	if ok {
		return metricsCollection
	}
	GetLogger(ctx).Error("GetRequestMetrics called without RequestContext middleware, some metrics may be lost")
	return metrics.NewMetricCollection(metrics.UnknownAPIName)
}

// GetRequestCompleteLogEntryFields returns a pointer to the set of log entry fields that will be included
// in the log line that summarizes the completed request.
func GetRequestCompleteLogEntryFields(ctx context.Context) (log.Fields, bool) {
	fields, haveFields := ctx.Value(requestCompleteLogEntryFieldsKey).(log.Fields)
	return fields, haveFields
}

// GetLogger returns a logger that will automatically include fields relevant to the request, such as request ID.
func GetLogger(ctx context.Context) *log.Entry {
	fields, haveFields := ctx.Value(requestLoggerFieldsKey).(log.Fields)
	if haveFields {
		return log.WithFields(fields)
	}

	return log.WithFields(log.Fields{})
}

func getMetricsFields(metricsCollection *metrics.MetricCollection) log.Fields {
	logEntryFields := log.Fields{}
	aggregateMetrics := metrics.NewAggregatedMetrics(metricsCollection)
	for metricName, datum := range aggregateMetrics.Dependency {
		unit := ""
		if datum.Type != metrics.NoUnit {
			unit = " " + datum.Type
		}
		fractionalDigits := -1
		if datum.Type == metrics.TimeMS {
			fractionalDigits = 0
		}
		logEntryFields[metricName] = strconv.FormatFloat(datum.Value, 'f', fractionalDigits, 64) + unit
	}
	for _, dimen := range metricsCollection.RequestDimension {
		logEntryFields[dimen.Name] = dimen.Value
	}
	return logEntryFields
}

// NewErrorHandler returns twirp server hooks for logging errors.
func NewErrorHandler() *twirp.ServerHooks {
	return &twirp.ServerHooks{
		RequestReceived: func(ctx context.Context) (context.Context, error) {
			return ctx, nil
		},
		RequestRouted: func(ctx context.Context) (context.Context, error) {
			return ctx, nil
		},
		ResponsePrepared: func(ctx context.Context) context.Context {
			return ctx
		},
		ResponseSent: func(ctx context.Context) {
		},
		Error: func(ctx context.Context, error twirp.Error) context.Context {
			// Log invalid error codes as Error.
			if !twirp.IsValidErrorCode(error.Code()) {
				GetLogger(ctx).Error(error)
				return ctx
			}
			// Log 4xx codes as Info.
			if twirp.ServerHTTPStatusFromErrorCode(error.Code()) < 500 {
				GetLogger(ctx).Info(error)
				return ctx
			}
			// Log 5xx codes as Error.
			GetLogger(ctx).Error(error)
			return ctx
		},
	}
}
