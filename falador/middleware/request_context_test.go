package middleware

import (
	"code.justin.tv/commerce/falador/mocks"

	"bufio"
	"bytes"
	"net/http"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

type MockHandler struct {
	Called     bool
	StatusCode int
}

func (h *MockHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.Called = true

	requestID, ok := GetRequestID(r.Context())
	So(ok, ShouldBeTrue)
	So(requestID, ShouldNotEqual, "")

	requestMetrics := GetRequestMetrics(r.Context())
	So(requestMetrics, ShouldNotBeNil)

	fields, ok := GetRequestCompleteLogEntryFields(r.Context())
	So(ok, ShouldBeTrue)
	fields["TestKey"] = "TestValue"

	w.WriteHeader(h.StatusCode)
}

type MockResponseWriter struct {
}

func (w *MockResponseWriter) Header() http.Header {
	return nil
}

func (w *MockResponseWriter) Write([]byte) (int, error) {
	return 0, nil
}

func (w *MockResponseWriter) WriteHeader(statusCode int) {
}

func TestRequestContext(t *testing.T) {
	Convey("Given a request context middleware invocation", t, func() {
		mockMetricsClient := new(mocks.MetricsClient)
		mockMetricsClient.On("PublishAllMetrics", mock.Anything).Return(nil)
		loggingConfig := make(map[string]*RequestLoggingConfig)
		requestContextMiddleware := NewRequestContextMiddleware(mockMetricsClient, loggingConfig)

		request, err := http.NewRequest("GET", "http://example.com/foo/MethodName", nil)
		So(err, ShouldBeNil)
		mockHandler := &MockHandler{StatusCode: 200}

		Convey("Middleware should print metrics and completion log lines on successful request", func() {
			buffer := serveRequest(requestContextMiddleware, mockHandler, request)

			logLine, err := buffer.ReadString('\n')
			So(err, ShouldBeNil)
			So(logLine, ShouldContainSubstring, "\"msg\":\"Complete\"")
			So(logLine, ShouldContainSubstring, "\"TestKey\":\"TestValue\"")
			So(logLine, ShouldContainSubstring, "\"request.id\":")
			So(logLine, ShouldContainSubstring, "\"request.time\":")
			So(logLine, ShouldContainSubstring, "\"status\":\"200\"")

			// No other lines should be logged.
			logLine, err = buffer.ReadString('\n')
			So(err, ShouldNotBeNil)
		})

		Convey("Middleware should not log successful requests when that is turned off", func() {
			loggingConfig["MethodName"] = NewRequestLoggingConfig(false)
			buffer := serveRequest(requestContextMiddleware, mockHandler, request)

			// No lines should be logged.
			_, err := buffer.ReadString('\n')
			So(err, ShouldNotBeNil)
		})

		Convey("Middleware should always log unsuccessful requests", func() {
			loggingConfig["MethodName"] = NewRequestLoggingConfig(false)
			mockHandler.StatusCode = 500
			buffer := serveRequest(requestContextMiddleware, mockHandler, request)

			logLine, err := buffer.ReadString('\n')
			So(err, ShouldBeNil)
			So(logLine, ShouldContainSubstring, "\"status\":\"500\"")
		})

		Reset(func() {
			So(mockHandler.Called, ShouldBeTrue)
			mockMetricsClient.AssertExpectations(t)
		})
	})
}

func serveRequest(middleware *RequestContextMiddleware, mockHandler *MockHandler, request *http.Request) *bytes.Buffer {
	handler := middleware.GetHandler()
	handlerInstance := handler(mockHandler)
	var buffer bytes.Buffer
	writer := bufio.NewWriter(&buffer)
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(writer)
	handlerInstance.ServeHTTP(&MockResponseWriter{}, request)
	log.SetOutput(os.Stderr)
	writer.Flush()
	return &buffer
}
