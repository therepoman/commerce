package middleware

import (
	"code.justin.tv/commerce/falador/config"
	"code.justin.tv/commerce/falador/models"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/systems/sandstorm/manager"
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"time"
)

/* MultiDecoder is used to overcome the problem that decoders are audience specific, and we have to accept two tokens. This struct implements
   a decoder that will validate using both provided decoders, allowing us to seamlessly check both in the authorization middleware
*/
type MultiDecoder struct {
	CartmanDecoder goauthorization.Decoder
	FaladorDecoder goauthorization.Decoder
}

func (m MultiDecoder) Decode(serialized string) (*goauthorization.AuthorizationToken, error) {
	// Both implementations are the same here
	return m.CartmanDecoder.Decode(serialized)
}

func (m MultiDecoder) ParseToken(req *http.Request) (*goauthorization.AuthorizationToken, error) {
	// Both implementations are the same here
	return m.CartmanDecoder.ParseToken(req)
}

func (m MultiDecoder) Validate(token *goauthorization.AuthorizationToken, claims goauthorization.CapabilityClaims) error {
	// Validate using cartman audience
	err := m.CartmanDecoder.Validate(token, claims)
	// If no error, validation was successful using this decoder, proceed
	if err == nil {
		return nil
	}
	// If error, try with falador audience
	return m.FaladorDecoder.Validate(token, claims)
}

// NewAuthMiddleware that fetches the auth token, decodes it, verifies it using the cartman secret key, and then injects the valid TUID into the ctx
func NewAuthMiddleware(faladorConfig config.FaladorConfig) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		// Set up Sandstorm SDK
		awsConfig := new(aws.Config).WithRegion("us-west-2").WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint)
		sess := session.Must(session.NewSession(awsConfig))
		stsclient := sts.New(sess)
		arp := &stscreds.AssumeRoleProvider{
			Duration:     900 * time.Second,
			ExpiryWindow: 10 * time.Second,
			RoleARN:      faladorConfig.GetSandstormRoleArn(),
			Client:       stsclient,
		}
		credentials := credentials.NewCredentials(arp)
		awsConfig.WithCredentials(credentials)

		sandstormManager := manager.New(manager.Config{
			AWSConfig:   awsConfig,
			ServiceName: "falador",
		})

		cartmanPublicKey, err := sandstormManager.Get(faladorConfig.GetSandstormKeyName())
		if err != nil {
			logrus.Error("Error fetching cartman public key. Bailing")
			panic(err)
		}

		cartmanDecoder, err := goauthorization.NewDecoder("HS512", "code.justin.tv/web/cartman", "code.justin.tv/web/cartman", cartmanPublicKey.Plaintext)
		if err != nil {
			log.Fatal("error initializing cartmanDecoder: ", err)
		}

		faladorDecoder, err := goauthorization.NewDecoder("HS512", "code.justin.tv/commerce/falador", "code.justin.tv/web/cartman", cartmanPublicKey.Plaintext)
		if err != nil {
			log.Fatal("error initializing faladorDecoder: ", err)
		}

		multiDecoder := MultiDecoder{
			CartmanDecoder: cartmanDecoder,
			FaladorDecoder: faladorDecoder,
		}

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token, err := multiDecoder.ParseToken(r)
			if err != nil {
				h.ServeHTTP(w, r) // no auth if token failed to parse
				return
			}

			err = multiDecoder.Validate(token, nil)
			if err != nil {
				h.ServeHTTP(w, r) // no auth if token failed to validate
				return
			}

			userID := token.GetSubject()
			ctx := context.WithValue(r.Context(), models.UserIdCtxKey, userID)
			r = r.WithContext(ctx)

			for _, s := range token.Claims.Audience {
				// Only accept capabilities directed at us.
				if s == "code.justin.tv/commerce/falador" {
					extensionClaim, ok := token.Claims.Authorizations["manage_catalog"]
					if !ok {
						h.ServeHTTP(w, r) // no extension id if no claims
						return
					}

					extensionId, ok := extensionClaim["extension_id"]
					if !ok {
						h.ServeHTTP(w, r) // no extension id if claim not included
						return
					}

					extensionIdString, ok := extensionId.(string)
					if !ok {
						h.ServeHTTP(w, r) // no extension id if extensionId isn't a string
						return
					}

					ctx = context.WithValue(ctx, models.ExtensionIdCtxKey, "twitch.ext."+extensionIdString)
				}
			}
			h.ServeHTTP(w, r.WithContext(ctx))
			return
		})
	}
}
