package main

import (
	"code.justin.tv/commerce/falador/api"
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/middleware"
	"context"
	"goji.io"
	"goji.io/pat"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	"code.justin.tv/commerce/falador/config"
	log "github.com/sirupsen/logrus"
)

const (
	// RunningPort represents the port to run falador on
	RunningPort = ":8080"
	// MWSBatchSize is the number of metrics.Datum to include in a batch.
	// Each metrics.Datum converts to a little more than 2 mws.Metric.
	// 1000 metrics.Datum converts to about a 30kb request body after gzipping.
	// Each API call generates around 20 metrics.Datum, so 1000 metrics.Datum is around 50 API calls.
	MWSBatchSize = 1000
	// MetricsFlushSleepDuration is the sleep duration between flushing metrics.
	MetricsFlushSleepDuration = 5 * time.Second
)

func main() {
	// Construct the falador config
	falConfig := config.NewFaladorConfig()
	if !falConfig.IsLocal() {
		log.SetFormatter(&log.JSONFormatter{})
	}

	// Construct Falador Service
	faladorService := api.NewServer(falConfig)
	twirpHandler := ft.NewFaladorServer(&faladorService, middleware.NewErrorHandler())

	batchingMetricClient := constructMetricClient(falConfig)

	// Logging every request limits the service throughput.
	// Successful health checks and ListProducts calls do not need to be logged.
	loggingConfig := make(map[string]*middleware.RequestLoggingConfig)
	loggingConfig["health"] = middleware.NewRequestLoggingConfig(false)
	loggingConfig["ListProducts"] = middleware.NewRequestLoggingConfig(false)
	requestContextMiddleware := middleware.NewRequestContextMiddleware(batchingMetricClient, loggingConfig)

	mux := goji.NewMux()
	mux.Use(requestContextMiddleware.GetHandler())
	mux.Use(middleware.NewAuthMiddleware(falConfig))
	mux.Handle(pat.Post(ft.FaladorPathPrefix+"*"), twirpHandler)
	mux.HandleFunc(pat.Get("/health"), handler)
	log.Info("Server running in " + falConfig.GetEnvironmentString() + " configuration.")
	log.Info("Server listening on " + RunningPort)
	server := &http.Server{
		Addr:    RunningPort,
		Handler: mux,
	}

	metricsProviders := []metrics.MetricsProvider{
		faladorService.ProductManager,
		metrics.NewMemStatsProvider(),
		requestContextMiddleware,
	}
	metrics.NewMetricsPoller(batchingMetricClient, metricsProviders, time.Duration(10)*time.Second)

	shutdownCompleteChannel := make(chan bool)
	go func() {
		shutdownSignalChannel := make(chan os.Signal, 1)
		signal.Notify(shutdownSignalChannel, syscall.SIGINT, syscall.SIGTERM)
		sig := <-shutdownSignalChannel
		log.Info("Received signal: ", sig)

		err := server.Shutdown(context.Background())
		batchingMetricClient.Shutdown()

		if err != nil {
			log.Fatal("Failed to shut down server: ", err)
		}
		close(shutdownCompleteChannel)
	}()

	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}

	<-shutdownCompleteChannel
	log.Info("Shutdown complete.")
}

func handler(w http.ResponseWriter, r *http.Request) {
	safeWrite(w, "OK")
	return
}

func safeWrite(w http.ResponseWriter, str string) {
	_, err := w.Write([]byte(str))
	if err != nil {
		panic(err)
	}
}

func constructMetricClient(faladorConfig config.FaladorConfig) *metrics.BatchingMetricClient {
	// Create new MWS (PMET) client.
	client := mws.AmazonMWSGoClient{}
	mwsClient := metrics.NewMWSClient(client, faladorConfig)
	// Add batching
	return metrics.NewBatchingMetricClient(mwsClient, MWSBatchSize, MetricsFlushSleepDuration)
}
