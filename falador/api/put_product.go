package api

import (
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/middleware"
	"code.justin.tv/commerce/falador/models"

	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

// PutProduct creates and stores a new product
func (s *FaladorServer) PutProduct(ctx context.Context, req *ft.PutProductRequest) (*ft.PutProductResponse, error) {
	// Before we do anything, validate the input
	validationError := s.InputValidation.IsValidTwirpPutProductRequest(req)
	if validationError != nil {
		return nil, validationError
	}

	authorizationError := s.AuthorizationManager.HasValidPutProductAuth(ctx, req)
	if authorizationError != nil {
		return nil, authorizationError
	}

	userID, ok := ctx.Value(models.UserIdCtxKey).(string)
	if !ok {
		return nil, twirp.NewError(twirp.Unauthenticated, "User not authenticated.")
	}

	middleware.GetLogger(ctx).Infof("Product being created or changed by tuid %s with domain %s and sku %s", userID, req.GetProduct().GetDomain(), req.GetProduct().GetSku())

	// Convert the twirp model to an internal model and call the product manager
	product := models.NewProduct(req.GetProduct(), userID)
	// Set up metrics
	apiMetrics := middleware.GetRequestMetrics(ctx)
	apiMetrics.AddRequestDimension(metrics.ClientDomainDimension, product.Domain)
	// Make the AddProduct call
	twirpErr := s.ProductManager.PutProduct(apiMetrics, product)

	// If an error was received, return that error
	if twirpErr != nil {
		return nil, twirpErr
	}
	return &ft.PutProductResponse{
		Product: product.GetTwirpProductModel(),
	}, nil
}
