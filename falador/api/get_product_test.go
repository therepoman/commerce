package api_test

import (
	"code.justin.tv/commerce/falador/api"
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/middleware"
	"code.justin.tv/commerce/falador/mocks"
	"code.justin.tv/commerce/falador/models"
	"github.com/twitchtv/twirp"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"golang.org/x/net/context"
	"testing"
)

const (
	Domain        = "Domain"
	Sku           = "sku1"
	InDevelopment = false
	DisplayName   = "Display Name"
)

func TestGetProductHappy(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	internalProduct := NewInternalProduct(100, Sku)
	expectedProduct := NewTwirpProduct(100, Sku)
	mockProductManager.On("GetProduct", apiMetrics, Domain, Sku, false).Return(internalProduct, nil)
	request := newTwirpGetProductRequest()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpGetProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.GetProduct(ctx, request)
	assert.Nil(t, err)
	assert.Equal(t, expectedProduct, response, "GetProduct returned incorrect response")
	return
}

func TestGetProductProductManagerErrors(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	newErr := twirp.InternalError("Something bad")
	mockProductManager.On("GetProduct", apiMetrics, Domain, Sku, false).Return(nil, newErr)
	request := newTwirpGetProductRequest()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpGetProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.GetProduct(ctx, request)
	assert.Nil(t, response)
	assert.Equal(t, newErr, err, "Incorrect error returned")
	return
}

func TestGetProductInvalidInputs(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx := context.Background()
	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpGetProductRequest", mock.Anything).Return(twirp.InternalError("Some error"))

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.GetProduct(ctx, nil)
	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	return
}

func NewTwirpProduct(costVal int64, skuVal string) *ft.Product {
	return &ft.Product{
		Domain: Domain,
		Sku:    skuVal,
		Cost: &ft.Cost{
			Amount: costVal,
			Type:   "bits",
		},
	}
}

func NewInternalProduct(costVal int64, skuVal string) *models.Product {
	return &models.Product{
		Domain: Domain,
		Sku:    skuVal,
		Cost: &models.Cost{
			Amount: costVal,
			Type:   "bits",
		},
	}
}

func newTwirpGetProductRequest() *ft.GetProductRequest {
	return &ft.GetProductRequest{
		Domain: Domain,
		Sku:    Sku,
	}
}
