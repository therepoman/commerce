package api_test

import (
	"code.justin.tv/commerce/falador/api"
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/middleware"
	"code.justin.tv/commerce/falador/mocks"
	"code.justin.tv/commerce/falador/models"
	"github.com/twitchtv/twirp"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"golang.org/x/net/context"
	"testing"
)

func TestListProductsHappy(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	internalProductList := newInternalProductList()
	expectedProductList := newTwirpProductList()
	mockProductManager.On("ListProducts", apiMetrics, Domain, false).Return(internalProductList, nil)
	request := newTwirpListProductsRequest()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, err)
	assert.Equal(t, expectedProductList, response, "ListProducts returned incorrect response")
	return
}

func TestListProductsProductManagerErrors(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	newErr := twirp.InternalError("Something bad")
	mockProductManager.On("ListProducts", apiMetrics, Domain, false).Return(nil, newErr)
	request := newTwirpListProductsRequest()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, response)
	assert.Equal(t, newErr, err, "Incorrect error returned")
	return
}

func TestListProductsProductManagerReturnsEmptyList(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	internalProductList := []*models.Product{}
	expectedProductList := newEmptyTwirpProductList()
	mockProductManager.On("ListProducts", apiMetrics, Domain, false).Return(internalProductList, nil)
	request := newTwirpListProductsRequest()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, err)
	assert.Equal(t, expectedProductList, response, "ListProducts returned incorrect response")
	return
}

func TestListProductsInvalidInputs(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx := context.Background()
	mockAuthManager := new(mocks.AuthorizationManager)
	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(twirp.InternalError("Some error"))
	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)

	response, err := server.ListProducts(ctx, nil)
	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	return
}

func TestListProductsAllowExpiredHappyCase(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	internalProductList := newInternalProductList()
	expectedProductList := newTwirpProductList()
	mockProductManager.On("ListProducts", apiMetrics, Domain, true).Return(internalProductList, nil)
	request := newTwirpListProductsRequestAllowExpired()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidListProductAuth", mock.Anything, mock.Anything).Return(nil)

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, err)
	assert.Equal(t, expectedProductList, response, "ListProducts returned incorrect response")
	return
}

func TestListProductsAllowExpiredUserNotAuthenticated(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	internalProductList := newInternalProductList()
	mockProductManager.On("ListProducts", apiMetrics, Domain, true).Return(internalProductList, nil)
	request := newTwirpListProductsRequestAllowExpired()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidListProductAuth", mock.Anything, mock.Anything).Return(twirp.NewError(twirp.Unauthenticated, "User not authenticated"))

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, response)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.Unauthenticated, "ListProducts returned incorrect response")
	return
}

func TestListProductsAllowExpiredUserNotAuthorized(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	internalProductList := newInternalProductList()
	mockProductManager.On("ListProducts", apiMetrics, Domain, true).Return(internalProductList, nil)
	request := newTwirpListProductsRequestAllowExpired()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidListProductAuth", mock.Anything, mock.Anything).Return(twirp.NewError(twirp.PermissionDenied, "User not authorized"))

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, response)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.PermissionDenied, "ListProducts returned incorrect response")
	return
}

func TestListProductsAllowExpiredAuthorizationError(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	internalProductList := newInternalProductList()
	mockProductManager.On("ListProducts", apiMetrics, Domain, true).Return(internalProductList, nil)
	request := newTwirpListProductsRequestAllowExpired()

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidListProductAuth", mock.Anything, mock.Anything).Return(twirp.InternalError("Authorization Error"))

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpListProductsRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.ListProducts(ctx, request)
	assert.Nil(t, response)
	assert.NotNil(t, err)
	twerr, _ := err.(twirp.Error)
	assert.Equal(t, twerr.Code(), twirp.Internal, "ListProducts returned incorrect response")
	return
}

func newEmptyTwirpProductList() *ft.ProductList {
	twirpProducts := []*ft.Product{}
	return &ft.ProductList{twirpProducts}
}

func newTwirpProductList() *ft.ProductList {
	twirpProducts := []*ft.Product{NewTwirpProduct(100, "sku1"), NewTwirpProduct(500, "sku2")}
	return &ft.ProductList{twirpProducts}
}

func newInternalProductList() []*models.Product {
	return []*models.Product{NewInternalProduct(100, "sku1"), NewInternalProduct(500, "sku2")}

}

func newTwirpListProductsRequest() *ft.ListProductsRequest {
	return &ft.ListProductsRequest{
		Domain: Domain,
	}
}

func newTwirpListProductsRequestAllowExpired() *ft.ListProductsRequest {
	return &ft.ListProductsRequest{
		Domain:     Domain,
		IncludeAll: true,
	}
}
