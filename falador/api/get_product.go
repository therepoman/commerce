package api

import (
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/middleware"

	"golang.org/x/net/context"
)

// GetProduct obtains an existing Product
func (s *FaladorServer) GetProduct(ctx context.Context, req *ft.GetProductRequest) (*ft.Product, error) {
	// Before we do anything, validate the input
	validationError := s.InputValidation.IsValidTwirpGetProductRequest(req)
	if validationError != nil {
		return nil, validationError
	}

	// Set up metrics
	apiMetrics := middleware.GetRequestMetrics(ctx)
	apiMetrics.AddRequestDimension(metrics.ClientDomainDimension, req.GetDomain())
	// Make the GetProduct call
	product, twirpErr := s.ProductManager.GetProduct(apiMetrics, req.GetDomain(), req.GetSku(), req.GetAllowExpired())

	// If an error was received, return that error
	if twirpErr != nil {
		return nil, twirpErr
	}
	return product.GetTwirpProductModel(), nil
}
