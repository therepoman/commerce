package api_test

import (
	"code.justin.tv/commerce/falador/api"
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/middleware"
	"code.justin.tv/commerce/falador/mocks"
	"code.justin.tv/commerce/falador/models"
	"github.com/twitchtv/twirp"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"golang.org/x/net/context"
	"testing"
)

const (
	LastEditor = "TestTuid"
)

func TestPutProductHappy(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	request := newTwirpPutProductRequest()
	expectedProductConversion := models.NewProduct(request.GetProduct(), LastEditor)
	mockProductManager.On("PutProduct", apiMetrics, expectedProductConversion).Return(nil)

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(nil)

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.PutProduct(ctx, request)
	assert.Nil(t, err)
	assert.EqualValues(t, &ft.PutProductResponse{request.Product}, response, "PutProduct returned incorrect response")
	return
}

func TestPutDevelopmentProduct(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	request := newTwirpPutProductRequest()
	request.Product.InDevelopment = true
	expectedProductConversion := models.NewProduct(request.GetProduct(), LastEditor)
	expectedProductConversion.InDevelopment = true
	mockProductManager.On("PutProduct", apiMetrics, expectedProductConversion).Return(nil)

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(nil)

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.PutProduct(ctx, request)
	assert.Nil(t, err)
	assert.EqualValues(t, &ft.PutProductResponse{request.Product}, response, "PutProduct returned incorrect response")
	return
}

func TestPutProductNoCost(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	request := newTwirpPutProductRequest()
	request.Product.Cost = nil
	expectedProductConversion := models.NewProduct(request.GetProduct(), LastEditor)
	mockProductManager.On("PutProduct", apiMetrics, expectedProductConversion).Return(nil)

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(nil)

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.PutProduct(ctx, request)
	assert.Nil(t, err)
	assert.EqualValues(t, &ft.PutProductResponse{request.Product}, response, "PutProduct returned incorrect response")
	return
}

func TestPutProductNoAuth(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	request := newTwirpPutProductRequest()
	expectedProductConversion := models.NewProduct(request.GetProduct(), LastEditor)
	mockProductManager.On("PutProduct", apiMetrics, expectedProductConversion).Return(nil)

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(twirp.NewError(twirp.Unauthenticated, "User not authenticated."))

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.PutProduct(ctx, request)
	assert.Nil(t, response)
	assert.EqualValues(t, twirp.NewError(twirp.Unauthenticated, "User not authenticated."), err, "PutProduct returned incorrect response")
	return
}

func TestPutProductNoAccess(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	request := newTwirpPutProductRequest()
	expectedProductConversion := models.NewProduct(request.GetProduct(), LastEditor)
	mockProductManager.On("PutProduct", apiMetrics, expectedProductConversion).Return(nil)

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(twirp.NewError(twirp.Unauthenticated, "User not authorized to edit specified Domain."))

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.PutProduct(ctx, request)
	assert.Nil(t, response)
	assert.EqualValues(t, twirp.NewError(twirp.Unauthenticated, "User not authorized to edit specified Domain."), err, "PutProduct returned incorrect response")
	return
}

func TestPutProductProductManagerErrors(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, apiMetrics, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	request := newTwirpPutProductRequest()
	expectedProductConversion := models.NewProduct(request.GetProduct(), LastEditor)
	newErr := twirp.InternalError("Something bad")
	mockProductManager.On("PutProduct", apiMetrics, expectedProductConversion).Return(newErr)

	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(nil)

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(nil)

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	productList, err := server.PutProduct(ctx, request)
	assert.Nil(t, productList)
	assert.Equal(t, newErr, err, "Incorrect error returned")
	return
}

func TestPutProductInvalidInputs(t *testing.T) {
	mockProductManager := new(mocks.ProductManager)
	ctx, _, _ := middleware.InitializeRequestContext(context.Background(), "MethodName")
	ctx = context.WithValue(ctx, models.UserIdCtxKey, "TestTuid")
	mockAuthManager := new(mocks.AuthorizationManager)
	mockAuthManager.On("HasValidPutProductAuth", mock.Anything, mock.Anything).Return(nil)

	mockInputValidator := new(mocks.InputValidator)
	mockInputValidator.On("IsValidTwirpPutProductRequest", mock.Anything).Return(twirp.InternalError("Some error"))

	server := api.NewFaladorServer(mockProductManager, mockAuthManager, mockInputValidator)
	response, err := server.PutProduct(ctx, nil)
	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.IsType(t, twirp.InternalError("Some error"), err)
	return
}

func newTwirpCost() *ft.Cost {
	return &ft.Cost{
		Amount: 100,
		Type:   "bits",
	}
}

func newTwirpPutProductRequest() *ft.PutProductRequest {
	cost := newTwirpCost()
	product := &ft.Product{
		Domain:        Domain,
		Sku:           Sku,
		Cost:          cost,
		InDevelopment: InDevelopment,
		DisplayName:   DisplayName,
	}
	return &ft.PutProductRequest{product}
}
