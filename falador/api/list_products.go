package api

import (
	ft "code.justin.tv/commerce/falador/falador-twirp"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/middleware"
	"golang.org/x/net/context"
)

// ListProducts obtains all products under a given domain in the ListProductsRequest
func (s *FaladorServer) ListProducts(ctx context.Context, req *ft.ListProductsRequest) (*ft.ProductList, error) {
	// Before we do anything, validate the input
	validationError := s.InputValidation.IsValidTwirpListProductsRequest(req)
	if validationError != nil {
		return nil, validationError
	}

	// Check authentication and authorization for viewing expired products
	if req.GetIncludeAll() {
		authError := s.AuthorizationManager.HasValidListProductAuth(ctx, req)
		if authError != nil {
			return nil, authError
		}
	}

	// Set up metrics
	apiMetrics := middleware.GetRequestMetrics(ctx)
	apiMetrics.AddRequestDimension(metrics.ClientDomainDimension, req.GetDomain())
	// Make the ListProducts call
	productList, twirpErr := s.ProductManager.ListProducts(apiMetrics, req.GetDomain(), req.GetIncludeAll())
	allProducts := []*ft.Product{}

	// Convert the response to a Twirp model from an internal model
	if (twirpErr == nil) && (productList != nil) {
		for _, element := range productList {
			singleModel := element.GetTwirpProductModel()
			allProducts = append(allProducts, singleModel)
		}
	}

	// If an error was received, return that error
	if twirpErr != nil {
		return nil, twirpErr
	}
	return &ft.ProductList{allProducts}, nil
}
