package api

import (
	"os"
	"time"

	"code.justin.tv/commerce/falador/cache"
	"code.justin.tv/commerce/falador/config"
	"code.justin.tv/commerce/falador/dynamo"
	"code.justin.tv/commerce/falador/facade"
	"code.justin.tv/commerce/falador/validation"
	guregu "github.com/guregu/dynamo"
	"regexp"
)

const (
	// InputStringRegex represents the regex to use for validating skus and domains
	// Allow alphanumeric, _, -, and . while ensuring at least one character is provided
	InputStringRegex = `^[A-Za-z0-9_.\-]+$`
)

// FaladorServer that is implemented by APIs
type FaladorServer struct {
	ProductManager       facade.ProductManager
	AuthorizationManager facade.AuthorizationManager
	InputValidation      validation.InputValidator
}

// NewServer constructs a new server for Falador
func NewServer(falConfig config.FaladorConfig) FaladorServer {
	// Construct necessary service dependencies
	guregu.RetryTimeout = time.Second
	productStorage := constructProductStorage(falConfig)
	productCache := constructProductCache(productStorage)
	productManager := facade.NewCachedDynamoProductManager(productStorage, productCache)
	authStorage := constructAuthStorage(falConfig)
	authManager := facade.NewDynamoAuthorizationManager(authStorage)
	inputValidator := constructRequestInputValidator()
	return NewFaladorServer(productManager, authManager, inputValidator)
}

// NewFaladorServer constructs the falador server using the provided dependencies
func NewFaladorServer(productManager facade.ProductManager, authManager facade.AuthorizationManager, inputValidation validation.InputValidator) FaladorServer {
	server := FaladorServer{}
	server.ProductManager = productManager
	server.AuthorizationManager = authManager
	server.InputValidation = inputValidation
	return server
}

func constructAuthStorage(faladorConfig config.FaladorConfig) dynamo.AuthorizationStorage {
	// Construct Authorization Manager
	db := guregu.New(faladorConfig.GetDefaultDynamoSession())
	return dynamo.NewDynamoAuthorizationStorage(db, faladorConfig.GetAuthorizationTableName(), faladorConfig)
}

func constructProductCache(productStorage dynamo.ProductStorage) cache.ProductCache {
	endpoint := os.Getenv("REDIS_ENDPOINT_ADDRESS")
	port := os.Getenv("REDIS_ENDPOINT_PORT")
	return cache.NewProductCache(endpoint, port, productStorage)
}

func constructProductStorage(faladorConfig config.FaladorConfig) dynamo.ProductStorage {
	// Construct Product Manager
	db := guregu.New(faladorConfig.GetDefaultDynamoSession())
	return dynamo.NewDynamoProductStorage(db, faladorConfig.GetProductsTableName(), faladorConfig)
}

func constructRequestInputValidator() validation.InputValidator {
	productStringRegex := regexp.MustCompile(InputStringRegex)
	return validation.NewRequestInputValidator(productStringRegex)
}
