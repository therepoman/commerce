package dynamo

import (
	"testing"

	"code.justin.tv/commerce/falador/config"
	dynamoTest "code.justin.tv/commerce/falador/dynamo/test"
	"code.justin.tv/commerce/falador/models"

	guregu "github.com/guregu/dynamo"
	"github.com/stretchr/testify/assert"
)

const (
	TEST_AUTHORIZATION_TABLE_NAME = "test-falador-authorization"
	TEST_TUID                     = "testTuid"
	TEST_DOMAIN_ID                = "testDomainId"
)

func TestEmptyGetAdmins(t *testing.T) {
	testConfig := config.NewFaladorConfig()
	dao := NewDynamoAuthorizationStorage(dynamoTest.CreateTestClient(), TEST_AUTHORIZATION_TABLE_NAME, testConfig)
	assert.Nil(t, DynamoContext.RegisterDao(dao))

	result, err := dao.GetAdmins()
	assert.EqualValues(t, err, guregu.ErrNotFound)
	assert.Equal(t, len(result), 0)
}

func TestEmptyGetUser(t *testing.T) {
	testConfig := config.NewFaladorConfig()
	dao := NewDynamoAuthorizationStorage(dynamoTest.CreateTestClient(), TEST_AUTHORIZATION_TABLE_NAME, testConfig)
	assert.Nil(t, DynamoContext.RegisterDao(dao))

	result, err := dao.GetUser(TEST_TUID)
	assert.Nil(t, err)
	assert.EqualValues(t, result, &models.User{})
}

func TestGetUser(t *testing.T) {
	testConfig := config.NewFaladorConfig()
	dao := NewDynamoAuthorizationStorage(dynamoTest.CreateTestClient(), TEST_AUTHORIZATION_TABLE_NAME, testConfig)
	assert.Nil(t, DynamoContext.RegisterDao(dao))

	user := models.User{
		Tuid:      TEST_TUID,
		DomainIds: []string{TEST_DOMAIN_ID},
	}

	err := dao.PutUser(&user)
	assert.Nil(t, err)

	result, err := dao.GetUser(TEST_TUID)
	assert.Nil(t, err)
	assert.EqualValues(t, *result, user)

	return
}

func TestGetAdmins(t *testing.T) {
	testConfig := config.NewFaladorConfig()
	dao := NewDynamoAuthorizationStorage(dynamoTest.CreateTestClient(), TEST_AUTHORIZATION_TABLE_NAME, testConfig)
	assert.Nil(t, DynamoContext.RegisterDao(dao))

	adminList := []string{TEST_TUID}

	admins := models.AdminList{
		AdminKey:   models.AdminKey,
		AdminTuids: adminList,
	}

	err := dao.PutAdmins(&admins)
	assert.Nil(t, err)

	result, err := dao.GetAdmins()
	assert.Nil(t, err)
	assert.EqualValues(t, result, adminList)

	return
}
