package dynamo

import (
	"testing"
	"time"

	"code.justin.tv/commerce/falador/config"
	dynamoTest "code.justin.tv/commerce/falador/dynamo/test"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/models"

	"github.com/stretchr/testify/assert"
)

const (
	TEST_PRODUCT_TABLE_NAME = "test-falador-products"
	TEST_SKU                = "testSKU"
	TEST_DOMAIN             = "testDomain"
	TEST_COST_TYPE          = "testType"
	TEST_COST_AMOUNT        = 100
	TEST_DISPLAY_NAME       = "testDisplayName"
	TEST_LAST_EDITOR        = "testLastEditor"
	TEST_METRIC             = "testMetric"
)

func TestEmptyGetProduct(t *testing.T) {
	testConfig := config.NewFaladorConfig()
	dao := NewDynamoProductStorage(dynamoTest.CreateTestClient(), TEST_PRODUCT_TABLE_NAME, testConfig)
	assert.Nil(t, DynamoContext.RegisterDao(dao))

	metricCollection := metrics.NewMetricCollection(TEST_METRIC)

	result, err := dao.Get(metricCollection, TEST_DOMAIN)
	assert.Nil(t, err)
	assert.Equal(t, len(result), 0)
}

func TestPutAndGetProduct(t *testing.T) {
	testConfig := config.NewFaladorConfig()
	dao := NewDynamoProductStorage(dynamoTest.CreateTestClient(), TEST_PRODUCT_TABLE_NAME, testConfig)
	assert.Nil(t, DynamoContext.RegisterDao(dao))

	metricCollection := metrics.NewMetricCollection(TEST_METRIC)
	currentTimeString := time.Now().UTC().Format(time.RFC3339)
	expirationTimeString := time.Now().UTC().Add(time.Hour * 1).Format(time.RFC3339)
	testProduct := models.Product{
		Domain:        TEST_DOMAIN,
		Sku:           TEST_SKU,
		Cost:          &models.Cost{TEST_COST_TYPE, TEST_COST_AMOUNT},
		InDevelopment: true,
		DisplayName:   TEST_DISPLAY_NAME,
		Expiration:    currentTimeString,
		LastUpdated:   expirationTimeString,
		LastEditor:    TEST_LAST_EDITOR,
		Broadcast:     true,
	}

	err := dao.Put(metricCollection, &testProduct)
	assert.Nil(t, err)

	result, err := dao.Get(metricCollection, TEST_DOMAIN)
	assert.Nil(t, err)
	assert.Equal(t, len(result), 1)
	assert.EqualValues(t, *result[0], testProduct)

	return
}
