package dynamo

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/falador/config"
	dynamoTest "code.justin.tv/commerce/falador/dynamo/test"
	"code.justin.tv/commerce/falador/models"

	guregu "github.com/guregu/dynamo"
	"github.com/sirupsen/logrus"
)

var (
	NonLocalDeleteAuthorizationTableErr = errors.New("Attempted to delete auth table in non-local environment")
	NonLocalCreateAuthorizationTableErr = errors.New("Attempted to create new auth table in non-local environment")
)

// DynamoAuthorizationStorage Dynamo implementation of AuthorizationStorage
type DynamoAuthorizationStorage struct {
	client  *guregu.DB
	table   guregu.Table
	isLocal bool
}

// AuthorizationStorage interface for getting valid falador admins
type AuthorizationStorage interface {
	GetAdmins() ([]string, error)
	GetUser(tuid string) (*models.User, error)
	PutUser(user *models.User) error
	PutAdmins(user *models.AdminList) error
	CreateTable() error
	DeleteTable() error
}

// NewDynamoAuthorizationStorage constructs the admin auth storage
func NewDynamoAuthorizationStorage(client *guregu.DB, tableName string, falConfig config.FaladorConfig) AuthorizationStorage {
	dao := &DynamoAuthorizationStorage{}
	dao.client = client
	dao.table = client.Table(tableName)
	dao.isLocal = falConfig.IsLocal()
	return dao
}

// GetAdmins gets all admins
func (dao *DynamoAuthorizationStorage) GetAdmins() ([]string, error) {
	var result *models.AdminList

	// Set timeout to 150 milliseconds
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()
	err := dao.table.Get("Tuid", models.AdminKey).OneWithContext(ctx, &result)
	var adminTuids []string
	if result != nil {
		adminTuids = result.AdminTuids
	}
	return adminTuids, err
}

// GetUser gets a user given a tuid
func (dao *DynamoAuthorizationStorage) GetUser(tuid string) (*models.User, error) {
	var result *models.User

	// Set timeout to 150 milliseconds
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()
	err := dao.table.Get("Tuid", tuid).OneWithContext(ctx, &result)
	// Do no propogate NotFound errors from this Get, instead assume empty record
	if err != nil && err == guregu.ErrNotFound {
		return &models.User{}, nil
	}
	return result, err
}

// Put adds a new user to the 'authorization' dynamoDB table
func (dao *DynamoAuthorizationStorage) PutUser(user *models.User) error {
	// Set timeout to 150 milliseconds
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()
	return dao.table.Put(user).RunWithContext(ctx)
}

// Put updates the Admin list in the 'authorization' dynamoDB table
func (dao *DynamoAuthorizationStorage) PutAdmins(admins *models.AdminList) error {
	// Set timeout to 150 milliseconds
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()
	return dao.table.Put(admins).RunWithContext(ctx)
}

// Deletes the table the DAO interacts with
// Only used for local testing purposes
func (dao *DynamoAuthorizationStorage) DeleteTable() error {
	// Return an error if this is called non-locally
	if !dao.isLocal {
		return NonLocalDeleteAuthorizationTableErr
	}

	return dao.table.DeleteTable().Run()
}

// Creates a table for the DAO to interact with
// Only used for local testing purposes
func (dao *DynamoAuthorizationStorage) CreateTable() error {
	// Return an error if this is called non-locally
	if !dao.isLocal {
		return NonLocalCreateAuthorizationTableErr
	}

	if dynamoTest.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}

	err := dao.client.CreateTable(dao.table.Name(), models.User{}).
		Provision(100, 100).
		Run()
	if err != nil {
		logrus.Error("Table creation error: ", err)
	}

	return err
}
