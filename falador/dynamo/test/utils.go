package dynamo_test

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	db "github.com/guregu/dynamo"
)

const (
	TestDynamoEndpoint string = "http://localhost:9001"
	TestDynamoRegion   string = "us-east-1"
)

func CreateTestClient() *db.DB {
	sess, err := session.NewSession()

	if err != nil {
		panic(err)
	}

	return db.New(sess, aws.NewConfig().
		WithRegion(TestDynamoRegion).
		WithEndpoint(TestDynamoEndpoint))
}

func DoesTableExist(tableName string, client *db.DB) bool {
	tables, err := client.ListTables().All()
	if err != nil {
		panic(err)
	}

	for _, preexistingTableName := range tables {
		if preexistingTableName == tableName {
			return true
		}
	}

	return false
}
