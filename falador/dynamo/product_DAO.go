package dynamo

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/falador/config"
	dynamoTest "code.justin.tv/commerce/falador/dynamo/test"
	"code.justin.tv/commerce/falador/metrics"
	"code.justin.tv/commerce/falador/models"

	guregu "github.com/guregu/dynamo"
	"github.com/sirupsen/logrus"
)

const (
	//DynamoPut represents the metric name of the Dynamo put
	DynamoPut = "Dynamo:Put"
	//DynamoGet represents the metric name of the Dynamo put
	DynamoGet = "Dynamo:Get"
)

var (
	NonLocalDeleteProductTableErr = errors.New("Attempted to delete product table in non-local environment")
	NonLocalCreateProductTableErr = errors.New("Attempted to create new product table in non-local environment")
)

// DynamoProductStorage that is used for getting/ putting into DynamoDB's 'product' Table
type DynamoProductStorage struct {
	client  *guregu.DB
	table   guregu.Table
	isLocal bool
}

// ProductStorage represents the DAO interface for the 'product' DynamoDB table
type ProductStorage interface {
	Put(mCollection *metrics.MetricCollection, product *models.Product) error
	Get(mCollection *metrics.MetricCollection, domain string) ([]*models.Product, error)
	Delete(domain string, sku string) error
	CreateTable() error
	DeleteTable() error
}

// NewDynamoProductStorage constructs a new DynamoProductStorage
func NewDynamoProductStorage(client *guregu.DB, tableName string, falConfig config.FaladorConfig) ProductStorage {
	dao := &DynamoProductStorage{}
	dao.client = client
	dao.table = client.Table(tableName)
	dao.isLocal = falConfig.IsLocal()
	return dao
}

// Put add a new product to the 'products' dynamoDB table
func (dao *DynamoProductStorage) Put(mCollection *metrics.MetricCollection, product *models.Product) error {
	start := time.Now()

	// Set timeout to 150 milliseconds
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()
	err := dao.table.Put(product).RunWithContext(ctx)
	// Note the relevant dependency metrics
	mCollection.AddDependencyCallMetrics(err, time.Since(start), DynamoPut)
	return err
}

// Get queries dynamoDB for all products matching a domain
func (dao *DynamoProductStorage) Get(mCollection *metrics.MetricCollection, domain string) ([]*models.Product, error) {
	var results []*models.Product
	start := time.Now()

	// Set timeout to 150 milliseconds
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()
	err := dao.table.Get("DomainId", domain).Consistent(true).AllWithContext(ctx, &results)
	// Note the relevant dependency metrics
	mCollection.AddDependencyCallMetrics(err, time.Since(start), DynamoGet)

	if err != nil {
		logrus.Errorf("Error querying Dynamo with domain %s: %v", domain, err)
	}

	return results, err
}

func (dao *DynamoProductStorage) Delete(domain string, sku string) error {
	return dao.table.Delete("DomainId", domain).Range("SKU", sku).Run()
}

// Deletes the table the DAO interacts with
// Only used for local testing purposes
func (dao *DynamoProductStorage) DeleteTable() error {
	// Return an error if this is called non-locally
	if !dao.isLocal {
		return NonLocalDeleteProductTableErr
	}

	return dao.table.DeleteTable().Run()
}

// Creates a table for the DAO to interact with.
// Only used for local testing purposes
func (dao *DynamoProductStorage) CreateTable() error {
	// Return an error if this is called non-locally
	if !dao.isLocal {
		return NonLocalCreateProductTableErr
	}

	if dynamoTest.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}

	err := dao.client.CreateTable(dao.table.Name(), models.Product{}).
		Provision(100, 100).
		Run()

	if err != nil {
		logrus.Error("Table creation error: ", err)
	}

	return err
}
