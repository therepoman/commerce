package dynamo

import (
	"os"
	"testing"

	dynamoTest "code.justin.tv/commerce/falador/dynamo/test"
)

var DynamoContext dynamoTest.DynamoTestContext

func TestMain(m *testing.M) {
	var err error

	DynamoContext := dynamoTest.DynamoTestContext{}

	exitCode := m.Run()

	err = DynamoContext.Cleanup()
	if err != nil {
		os.Exit(1)
	}

	os.Exit(exitCode)
}
