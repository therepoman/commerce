# include the base module
module "base" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/release/terraform.git//base"
}

# set the default AWS region to us-west-2 (this is where our direct connect is)
provider "aws" {
  region = "us-west-2"
}

variable "cluster" {
  default = "internal_search"
} 