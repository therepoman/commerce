package iss

import (
	"fmt"
)

type IndexDocument struct {
	Title   string
	Created string
	Indexed string
	Author  string
	Url     string
	Body    string
	Types   []string
}

type Indexer struct {
}

func (self *Indexer) Index(doc IndexDocument) error {
	fmt.Println(doc.Title)
	return nil
}
