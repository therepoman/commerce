package main

import (
	"code.justin.tv/commerce/internal_search/confluence"
	"code.justin.tv/commerce/internal_search/github"
	"code.justin.tv/commerce/internal_search/jira"
	// "encoding/json"
	"fmt"
	"github.com/bluele/slack"
	"github.com/jaytaylor/html2text"
	"github.com/olivere/elastic"
	"log"
	"os"
	"strings"
	"time"
)

type Document struct {
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	Url       string    `json:"url"`
	Type      string    `json:"type"`
	CreatedAt time.Time `json:"crated_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func ScrapeJira(e *elastic.Client) {
	key := os.Getenv("JIRA_KEY")
	if key == "" {
		fmt.Println("To index Jira, set JIRA_KEY environment variable to an access key, to find it get your crowd cookie from your browser.")
		return
	}

	c := jira.NewJira("https://twitchtv.atlassian.net", key)

	for ii := c.ListIssues(); ii != nil; ii = ii.Next(c) {
		for _, issue := range ii.Issues {
			createdAt, _ := time.Parse("2006-01-02T15:04:05.999999-0700", issue.Fields.Created)
			updatedAt, _ := time.Parse("2006-01-02T15:04:05.999999-0700", issue.Fields.Updated)
			id := "jira-" + issue.Key
			document := Document{
				Title:     issue.Fields.Summary,
				Body:      issue.Fields.Description,
				Url:       fmt.Sprintf("https://twitchtv.atlassian.net/browse/%s", issue.Key),
				Type:      "jiraissue",
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			}
			_, err := e.Index().Index("document").Type("document").Id(id).BodyJson(document).Do()
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func ScrapeConfluence(e *elastic.Client) {
	key := os.Getenv("CONFLUENCE_KEY")
	if key == "" {
		fmt.Println("To index Confluence, set CONFLUENCE_KEY environment variable to an access key, to find it get your crowd cookie from your browser.")
		return
	}
	c := confluence.NewConfluence("https://twitchtv.atlassian.net", key)

	for pi := c.ListPages(); pi != nil; pi = pi.Next(c) {
		for _, page := range pi.Results {
			createdAt, _ := time.Parse("2006-01-02T15:04:05.999999-07:00", page.History.CreatedDate)
			updatedAt, _ := time.Parse("2006-01-02T15:04:05.999999-07:00", page.Version.When)

			body, _ := html2text.FromString(page.Body.Storage.Value)

			document := Document{
				Title:     page.Title,
				Body:      body,
				Url:       fmt.Sprintf("https://twitchtv.atlassian.net/wiki%s", page.Links.Webui),
				Type:      "confluencepage",
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			}
			id := "confluence-" + strings.Replace(page.Links.Webui, "/", "_", -1)
			_, err := e.Index().Index("document").Type("document").Id(id).BodyJson(document).Do()
			if err != nil {
				fmt.Println("indexing")
				fmt.Println(err)
			}
		}
	}
}

func ScrapeGithub(e *elastic.Client) {
	key := os.Getenv("GITHUB_KEY")
	if key == "" {
		fmt.Println("To index Github, set GITHUB_KEY environment variable to an access key, go here https://git-aws.internal.justin.tv/settings/tokens")
		return
	}
	g := github.NewGithub("https://git-aws.internal.justin.tv", key)

	for ri := g.PublicRepositories(); ri != nil; ri = ri.Next(g) {
		for _, repo := range ri.Repos {
			fmt.Println(repo.Url, repo.Name)
			for _, file := range g.Contents(repo.Name) {
				document := Document{
					Title: fmt.Sprintf("%s/%s", repo.Name, file.Path),
					Body:  file.Contents,
					Url:   file.Url,
					Type:  "gitdocument",
				}
				id := "github-" + strings.Replace(document.Title, "/", "_", -1)
				_, err := e.Index().Index("document").Type("document").Id(id).BodyJson(document).Do()
				if err != nil {
					fmt.Println("indexing")
					fmt.Println(err)
				}
				fmt.Println(">gd")
			}
		}
	}
}

func ScrapeSlack(e *elastic.Client) {
	key := os.Getenv("SLACK_KEY")
	if key == "" {
		fmt.Println("To index Slack, set SLACK_KEY environment variable to an access key, go here: https://api.slack.com/web")
		return
	}
	api := slack.New(key)
	channels, _ := api.ChannelsList()
	for _, channel := range channels {
		fmt.Println(channel.Name, channel.Id)
	}

	// messages, _, _ := api.ChannelsHistory("C03T04TNL") //channel.Id)
	// for _, message := range messages {
	// 	b, _ := json.Marshal(message)
	// 	fmt.Println("\t", string(b))
	// }
}

func InitIndex(e elastic.Client) {
	mapping := `
{
    "settings":{
        "number_of_shards":4,
        "number_of_replicas":0
    },
    "mappings":{
        "document":{
            "properties":{
                "title":{
                    "type":"string",
                    "store":"true",
                    "index":"analyzed"
                },
                "body":{
                    "type":"string",
                    "store":"true",
                    "index":"analyzed"
                },
                "url":{
                    "type":"string",
                    "store":"false",
                    "index":"not_analyzed"
                },
                "type":{
                    "type":"string",
                    "store":"true",
                    "index":"not_analyzed"
                },
                "created_at":{
                	"type":"date",
                    "store":"false"
                },
                "updated_at":{
                	"type":"date",
                    "store":"false"
                }
            }
        }
    }
}`

	_, err := e.CreateIndex("document").Body(mapping).Do()
	if err != nil {
		fmt.Println(err)
	}
}

func Elastic() *elastic.Client {
	e, err := elastic.NewClient()
	if err != nil {
		log.Fatal(err)
	}
	return e
}

func main() {
	fmt.Println("Starting Searcher")
	ScrapeJira(Elastic())
	ScrapeConfluence(Elastic())
	ScrapeGithub(Elastic())
	ScrapeSlack(Elastic())
}
