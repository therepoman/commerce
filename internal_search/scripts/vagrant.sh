# scripts/vagrant.sh
apt-get update
apt-get install -y manta

apt-get install zsh
git clone --depth=1 git://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
cp /home/vagrant/.oh-my-zsh/templates/zshrc.zsh-template /home/vagrant/.zshrc
chown -R vagrant:vagrant /home/vagrant
chsh vagrant -s /usr/bin/zsh

wget -q https://storage.googleapis.com/golang/go1.4.2.linux-amd64.tar.gz
echo "5020af94b52b65cc9b6f11d50a67e4bae07b0aff  go1.4.2.linux-amd64.tar.gz" | sha1sum --check
tar -C /usr/local -xzf go1.4.2.linux-amd64.tar.gz

echo '
export PATH="/usr/local/go/bin:/go/bin/:$PATH"
export GOPATH="/go"
' >> /home/vagrant/.zshrc

source /home/vagrant/.zshrc

apt-get -y -qq install git mercurial
go get github.com/golang/lint/golint
go get golang.org/x/tools/cmd/goimports
go get github.com/tools/godep

chown -R vagrant:vagrant /go

apt-get install -y unzip
wget -q https://dl.bintray.com/mitchellh/terraform/terraform_0.6.3_linux_amd64.zip
sudo unzip -d /usr/local/bin terraform_0.6.3_linux_amd64.zip