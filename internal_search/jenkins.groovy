job {
	name 'internal_search'
	using 'TEMPLATE-autobuild'

	scm {
		git {
			remote {
				github 'commerce/internal_search', 'ssh', 'git.internal.justin.tv'
				credentials 'git-aws-read-key'
			}
			branches "origin/mainline"
		}
	}

	steps {
		shell 'manta -v'
		saveDeployArtifact 'commerce/internal_search', '.manta'
	}
}

job {
	name 'commerce-internal_search-deploy'
	using 'TEMPLATE-deploy'
	steps {
		shell 'courier deploy --repo "commerce/internal_search" --dir "/opt/twitch/internal_search"'
	}
}