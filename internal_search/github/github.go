package github

import (
	"fmt"
	json "github.com/bitly/go-simplejson"
	"io/ioutil"
	"net/http"

	// "strings"
)

type Github struct {
	Base  string
	token string
}

func NewGithub(base, token string) *Github {
	return &Github{
		Base:  base,
		token: token,
	}
}

func (self *Github) RawFetch(url string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET",
		url,
		nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Accept", "application/vnd.github.VERSION.raw")
	req.Header.Add("Authorization", "token "+self.token)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	fmt.Println(url, "->", len(body), "bytes")
	return string(body), nil
}

func (self *Github) Fetch(url string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("%s/api/v3%s", self.Base, url),
		nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "token "+self.token)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	fmt.Println(url, "->", len(body), "bytes")
	return string(body), nil
}

func tos(v interface{}) string {
	return fmt.Sprintf("%s", v)
}

func (self *Github) FetchDirectory(repo, path string, files []File) []File {
	body, _ := self.Fetch(fmt.Sprintf("/repos/%s/contents/%s", repo, path))

	j, err := json.NewJson([]byte(body))
	if err != nil {
		fmt.Println(err)
		return files
	}

	a, err := j.Array()
	for _, ai := range a {
		f := ai.(map[string]interface{})
		t := tos(f["type"])
		if t == "file" {
			file := File{
				Url:    tos(f["html_url"]),
				ApiUrl: tos(f["url"]),
				Path:   tos(f["path"]),
			}
			files = append(files, self.FetchFile(file))
		} else {
			files = self.FetchDirectory(repo, tos(f["path"]), files)
		}
	}
	return files
}

func (self *Github) FetchFile(file File) File {
	file.Contents, _ = self.RawFetch(file.ApiUrl)
	return file
}

func (self *Github) Contents(repo string) []File {
	return self.FetchDirectory(repo, "", make([]File, 0, 1024))
}

func (self *Github) PublicRepositories() *RepositoryIterator {
	body, err := self.Fetch("/repositories")
	if err != nil {
		return nil
	}
	j, err := json.NewJson([]byte(body))
	if err != nil {
		fmt.Println(err)
		return nil
	}

	ri := RepositoryIterator{
		Repos: make([]Repository, 0, 1024),
	}

	a, err := j.Array()
	for _, ai := range a {
		r := ai.(map[string]interface{})
		repo := Repository{
			Url:  tos(r["url"]),
			Name: tos(r["full_name"]),
			Id:   tos(r["id"]),
		}
		ri.Repos = append(ri.Repos, repo)
		ri.NextUrl = fmt.Sprintf("/repositories?since=%s", repo.Id)
	}
	return &ri
}

type File struct {
	Path     string
	Url      string
	Contents string
	ApiUrl   string
}

type FileIterator struct {
}

type Repository struct {
	Url  string
	Name string
	Id   string
}

type RepositoryIterator struct {
	Repos   []Repository
	NextUrl string
}

func (self *RepositoryIterator) Next(g *Github) *RepositoryIterator {
	body, err := g.Fetch(self.NextUrl)
	if err != nil {
		return nil
	}
	j, err := json.NewJson([]byte(body))
	if err != nil {
		fmt.Println(err)
		return nil
	}

	ri := RepositoryIterator{
		Repos: make([]Repository, 0, 1024),
	}

	a, err := j.Array()
	for _, ai := range a {
		r := ai.(map[string]interface{})
		repo := Repository{
			Url:  tos(r["url"]),
			Name: tos(r["full_name"]),
			Id:   tos(r["id"]),
		}
		ri.Repos = append(ri.Repos, repo)
		ri.NextUrl = fmt.Sprintf("/repositories?since=%s", repo.Id)
	}
	return &ri
}
