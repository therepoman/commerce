package jira

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Jira struct {
	Base  string
	token string
}

func NewJira(base, token string) *Jira {
	return &Jira{
		Base: base,
	}
}

func (self *Jira) Fetch(url string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("%s/rest/api/latest/%s", self.Base, url),
		nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Cookie", "studio.crowd.tokenkey="+self.token)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func (self *Jira) ListIssues() *IssueIterator {
	body, _ := self.Fetch("search?cql=&maxResults=500")
	dec := json.NewDecoder(strings.NewReader(body))
	var ii IssueIterator
	dec.Decode(&ii)
	return &ii
}

type Fields struct {
	Summary     string
	Created     string
	Updated     string
	Description string
}

type Issue struct {
	Self   string
	Fields Fields
	Key    string
}

type IssueIterator struct {
	Issues     []Issue
	StartAt    int64
	MaxResults int64
	Total      int64
}

func (self *IssueIterator) Next(jira *Jira) *IssueIterator {
	if self.StartAt >= self.Total {
		return nil
	}
	url := fmt.Sprintf("search?cql=&startAt=%d&maxResults=500", self.StartAt+self.MaxResults)
	fmt.Println(url)
	fmt.Println("-------------")
	body, _ := jira.Fetch(url)
	dec := json.NewDecoder(strings.NewReader(body))
	var ii IssueIterator
	dec.Decode(&ii)
	// for _, issue := range ii.Issues {
	// }
	fmt.Println(ii.StartAt)
	fmt.Println(ii.MaxResults)
	fmt.Println(ii.Total)
	return &ii
}
