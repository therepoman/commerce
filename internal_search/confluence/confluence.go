package confluence

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Confluence struct {
	Base  string
	token string
}

func NewConfluence(base, token string) *Confluence {
	return &Confluence{
		Base:  base,
		token: token,
	}
}

func (self *Confluence) Fetch(url string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf("%s/wiki/rest/api/%s", self.Base, url),
		nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Cookie", "studio.crowd.tokenkey="+self.token)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	fmt.Println(url, "->", len(body), "bytes")
	return string(body), nil
}

func (self *Confluence) ListPages() *PageIterator {
	body, _ := self.Fetch("content?expand=body.storage,version,history")
	dec := json.NewDecoder(strings.NewReader(body))
	var pi PageIterator
	dec.Decode(&pi)
	return &pi
}

type History struct {
	CreatedDate string `json:"createdDate"`
}

type Version struct {
	When string
}

type Storage struct {
	Value string
}

type Body struct {
	Storage Storage
}

type Links struct {
	Webui   string `json:"webui"`
	Next    string
	Base    string
	Self    string
	Context string
}

type Result struct {
	Title   string
	Body    Body
	Links   Links   `json:"_links"`
	Version Version `json:"version"`
	History History `json:"history"`
}

type PageIterator struct {
	Results []Result `json:"results"`
	Start   int64
	Limit   int64
	Total   int64
	Links   Links `json:"_links"`
}

func (self *PageIterator) Next(c *Confluence) *PageIterator {
	if self.Links.Next == "" {
		return nil
	}
	body, _ := c.Fetch(strings.Replace(self.Links.Next, "/rest/api/", "", 1))

	dec := json.NewDecoder(strings.NewReader(body))
	var pi PageIterator
	dec.Decode(&pi)
	return &pi
}
