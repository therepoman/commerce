package main

import (
	// "github.com/olivere/elastic"

	"github.com/zenazn/goji/web"

	"bytes"
	"encoding/json"
	"fmt"
	"github.com/hypebeast/gojistatic"
	"github.com/zenazn/goji"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type QueryRequest struct {
	Query string `json:"query"`
	Page  int
	Size  int
}

func query(c web.C, w http.ResponseWriter, r *http.Request) {
	// request should look like {"query":"aws","page":0,"size":10}

	var query QueryRequest
	requestBytes, _ := ioutil.ReadAll(r.Body)
	_ = json.Unmarshal(requestBytes, &query)
	start := query.Page * query.Size

	encodedQueryBuf := new(bytes.Buffer)
	json.HTMLEscape(encodedQueryBuf, []byte(query.Query))
	encodedQuery := string(encodedQueryBuf.Bytes())

	strFmt :=
		`{
	"query": {
		"query_string" : {
			"fields" : ["body"],
			"query" : "%s"
		}
	},
	"highlight" : {
        "fields" : {
            "body" : {}
        }
    },
	"from": %d,
	"size": %d
}`
	strQuery := fmt.Sprintf(
		strFmt,
		encodedQuery,
		start,
		query.Size)

	// fmt.Printf("curl -XGET http://localhost:9200/document/_search -d '%s'\n", strQuery)

	req, err := http.NewRequest("GET", "http://localhost:9200/document/_search", strings.NewReader(strQuery))
	if err != nil {
		return
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	io.Copy(w, resp.Body)
}

func main() {
	fmt.Println(os.Getwd())
	goji.Use(gojistatic.Static("public", gojistatic.StaticOptions{
		SkipLogging: true,
		IndexFile:   "index.html",
		Prefix:      "",
	}))
	goji.Post("/search", query)
	goji.Serve()
}
