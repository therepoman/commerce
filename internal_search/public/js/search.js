var searchApp = angular.module("searchApp", ['ui.bootstrap']);
searchApp.controller("SearchCtrl", function SearchCtrl($scope, $http, $location) {
  $scope.url = 'search.php'; // The url of our search
  $scope.results = [];
  $scope.count = 0;
  $scope.page = 0;
  $scope.itemsPerPage = 10;

  var prevQuery = "";

  $scope.init = function() {
    if ($location.search().query == null) {
      console.log("no search");
    }
    else {
      $scope.keywords = $location.search().query;
      $scope.search();
    }
  };
    
  // The function that will be executed on button click (ng-click="search()")
  $scope.search = function() {
    if (prevQuery != $scope.keywords) {
      $scope.page = 0;
      prevQuery = $scope.keywords;
    }
    var q = {query: $scope.keywords, page: $scope.page};
    $location.search(q);
    $location.replace();
    q.size = 10;
    $http.post("/search", q)
    .success(function(data){
      $scope.results = data.hits.hits;
      $scope.count = data.hits.total;
    });
  };

  $scope.init();
});
