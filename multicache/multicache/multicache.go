package multicache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type MultiCacher interface {
	Fetch(ctx context.Context, cachePrefix string, keys []CacheKeyer, defaultTTL time.Duration, fn FetchCallback) ([][]byte, error)
	Invalidate(ctx context.Context, cachePrefix string, keys []CacheKeyer) error
}

type FetchCallback func([]interface{}) (map[string]CacheItem, error)

// CacheKeyer allows the conusmer to pass in arbitrary interfaces that will be used as arguments in
// the callback function when fetching cache misses. CacheKeyer also allows a mapping from the keys
// that are structs to a string to be used as the key in the cache. For example, GetGifts(keys []GiftKey)
// requires you pass in a slice of structs, but we also need a way to go from these structs to strings
// used as the key in the store.
type CacheKeyer interface {
	CacheKey() string
}
type MultiCache struct {
	redisClient redis.Cmdable
	statter     statsd.Statter
}

// CacheItem is used by the consumer to return the byte data and ttl of a record to be cached
type CacheItem struct {
	Data []byte
	TTL  time.Duration
}

// cacheData is used when storing data in the cache. It has an additional field, "key", that the
// exported CacheValue does not have.
type cacheData struct {
	Key  string `json:"key"`
	Data []byte `json:"data"`
}

func New(redisClient redis.Cmdable, statter statsd.Statter) (*MultiCache, error) {
	return &MultiCache{
		redisClient: redisClient,
		statter:     statter,
	}, nil
}

func (rmc *MultiCache) Invalidate(ctx context.Context, cachePrefix string, keys []CacheKeyer) error {
	pipelinerDel := rmc.redisClient.Pipeline()
	defer func() {
		err := pipelinerDel.Close()
		if err != nil {
			err = errors.Wrap(err, "failed to close pipelinerDel")
			log.WithError(err).Error(err)
		}
	}()
	for _, key := range keys {
		pipelinerDel.Del(keyWithPrefix(cachePrefix, key.CacheKey()))
	}
	_, err := pipelinerDel.Exec()
	if err != nil && err != redis.Nil {
		return err
	}
	return nil
}

func (rmc *MultiCache) Fetch(ctx context.Context, cachePrefix string, keys []CacheKeyer, defaultTTL time.Duration, fn FetchCallback) ([][]byte, error) {
	// Remove duplicate keys (duplicate => keys[i].CacheKey() == keys[j].CacheKey()).
	// In the event of duplicates, the behaviour of removeDuplicateKeys is to only use the first occurrence.
	uniqueKeys := removeDuplicateKeys(keys)
	// Get values from already cached data
	getCachedValuesStart := time.Now()
	cachedValues, err := rmc.getCachedValues(ctx, cachePrefix, uniqueKeys)
	if err != nil {
		err = errors.Wrap(err, "getCachedValues")
		log.WithError(err).Error(err)
		cachedValues = make(map[string][]byte)
	}
	getCachedValuesTiming := time.Now().Sub(getCachedValuesStart)

	// Get keys that aren't cached yet
	nonCachedKeys := rmc.getNonCachedKeys(ctx, cachePrefix, uniqueKeys, cachedValues)

	// Get values that need to be cached from passed in func
	getValuesToCacheStart := time.Now()
	valuesToCache, err := getValuesToCache(ctx, nonCachedKeys, fn)
	if err != nil {
		return nil, errors.Wrap(err, "getValuesToCache")
	}
	getValuesToCacheTiming := time.Now().Sub(getValuesToCacheStart)

	cacheValuesStart := time.Now()
	// Convert valuesToCache to cacheData struct
	resultValuesToCache, err := rmc.cacheValues(ctx, cachePrefix, nonCachedKeys, valuesToCache, defaultTTL)
	if err != nil {
		return nil, errors.Wrap(err, "cacheValues")
	}
	cacheValuesTiming := time.Now().Sub(cacheValuesStart)

	// Gather results from cachedValues and resultValuesToCache
	result := gatherResults(cachedValues, resultValuesToCache)

	// Report cache hits/misses to statter
	cacheHits := len(cachedValues)
	cacheMisses := len(nonCachedKeys)
	err = rmc.statter.Inc(fmt.Sprintf("multicache.%s.hits", cachePrefix), int64(cacheHits), 1)
	if err != nil {
		err = errors.Wrap(err, "failed to report cache hits")
		log.WithError(err).Error(err)
	}
	err = rmc.statter.Inc(fmt.Sprintf("multicache.%s.misses", cachePrefix), int64(cacheMisses), 1)
	if err != nil {
		err = errors.Wrap(err, "failed to report cache misses")
		log.WithError(err).Error(err)
	}

	// Report timings to statter
	err = rmc.statter.TimingDuration(fmt.Sprintf("multicache.%s.get_cached_values", cachePrefix), getCachedValuesTiming, 1)
	if err != nil {
		err = errors.Wrap(err, "failed to report getCachedValues timing")
		log.WithError(err).Error(err)
	}
	err = rmc.statter.TimingDuration(fmt.Sprintf("multicache.%s.get_values_to_cache", cachePrefix), getValuesToCacheTiming, 1)
	if err != nil {
		err = errors.Wrap(err, "failed to report getValuesToCache timing")
		log.WithError(err).Error(err)
	}
	if len(valuesToCache) > 0 {
		err = rmc.statter.TimingDuration(fmt.Sprintf("multicache.%s.cache_values", cachePrefix), cacheValuesTiming, 1)
		if err != nil {
			err = errors.Wrap(err, "failed to report cacheValues timing")
			log.WithError(err).Error(err)
		}
	}

	return result, nil
}

func (rmc *MultiCache) getCachedValues(ctx context.Context, cachePrefix string, keys []CacheKeyer) (map[string][]byte, error) {
	pipelinerGet := rmc.redisClient.Pipeline()
	defer func() {
		err := pipelinerGet.Close()
		if err != nil {
			err = errors.Wrap(err, "failed to close pipelinerGet")
			log.WithError(err).Error(err)
		}
	}()

	cachedData := make([]*redis.StringCmd, len(keys))
	for i, key := range keys {
		cachedData[i] = pipelinerGet.Get(keyWithPrefix(cachePrefix, key.CacheKey()))
	}

	_, err := pipelinerGet.Exec()
	if err != nil && err != redis.Nil {
		err = errors.Wrap(err, "failed to get cached keys")
		log.WithError(err).Error(err)
	}

	cachedValues := map[string][]byte{}
	for _, value := range cachedData {
		if value.Val() == "" {
			continue
		}
		var cacheValue cacheData
		err := json.Unmarshal([]byte(value.Val()), &cacheValue)
		// if there is an invalid cached value we should delete all inputed cache values
		if err != nil {
			keyStrings := make([]string, len(keys))

			for i, key := range keys {
				keyStrings[i] = keyWithPrefix(cachePrefix, key.CacheKey())
			}

			rmc.redisClient.Del(keyStrings...)
			return nil, nil
		}
		cachedValues[cacheValue.Key] = cacheValue.Data
	}
	return cachedValues, nil
}

func (rmc *MultiCache) getNonCachedKeys(ctx context.Context, cachePrefix string, keys []CacheKeyer, cachedValues map[string][]byte) []CacheKeyer {
	var nonCachedKeys []CacheKeyer
	for _, key := range keys {
		if _, ok := cachedValues[keyWithPrefix(cachePrefix, key.CacheKey())]; !ok {
			nonCachedKeys = append(nonCachedKeys, key)
		}
	}
	return nonCachedKeys
}

func getValuesToCache(ctx context.Context, nonCachedKeys []CacheKeyer, fn FetchCallback) (map[string]CacheItem, error) {
	// Don't call fetch callback if len keys is 0
	if len(nonCachedKeys) == 0 {
		return nil, nil
	}
	keys := make([]interface{}, len(nonCachedKeys))
	for i, key := range nonCachedKeys {
		keys[i] = key.(interface{})
	}
	valuesToCache, err := fn(keys)
	if err != nil {
		return nil, err
	}
	return valuesToCache, nil
}

func (rmc *MultiCache) cacheValues(ctx context.Context, cachePrefix string, nonCachedKeys []CacheKeyer, valuesToCache map[string]CacheItem, defaultTTL time.Duration) ([][]byte, error) {
	pipelinerSet := rmc.redisClient.Pipeline()
	defer func() {
		err := pipelinerSet.Close()
		if err != nil {
			err = errors.Wrap(err, "failed to close pipelinerSet")
			log.WithError(err).Error(err)
		}
	}()
	for _, key := range nonCachedKeys {
		if _, ok := valuesToCache[key.CacheKey()]; !ok {
			value, err := json.Marshal(&cacheData{Key: keyWithPrefix(cachePrefix, key.CacheKey()), Data: []byte("")})
			if err != nil {
				return nil, err
			}

			pipelinerSet.Set(keyWithPrefix(cachePrefix, key.CacheKey()), value, defaultTTL)
		}
	}

	var resultValuesToCache [][]byte
	for key, val := range valuesToCache {
		resultValuesToCache = append(resultValuesToCache, val.Data)
		value, err := json.Marshal(&cacheData{Key: keyWithPrefix(cachePrefix, key), Data: val.Data})
		if err != nil {
			return nil, err
		}

		pipelinerSet.Set(keyWithPrefix(cachePrefix, key), value, val.TTL)
	}

	_, err := pipelinerSet.Exec()
	if err != nil {
		err = errors.Wrap(err, "failed to set cached values")
		log.WithError(err).Error(err)
	}

	return resultValuesToCache, nil
}

func gatherResults(cachedValues map[string][]byte, resultValuesToCache [][]byte) [][]byte {
	var result [][]byte
	for _, val := range cachedValues {
		if len(val) != 0 || string(val) != "" {
			result = append(result, val)
		}
	}

	for _, val := range resultValuesToCache {
		result = append(result, val)
	}

	return result
}

func keyWithPrefix(prefix, key string) string {
	return fmt.Sprintf("%s:%s", prefix, key)
}

// Takes a slice of CacheKeyer and returns a new slice that contains only the values of the
// input whose CacheKey() result is unique. If there are multiple values with the same CacheKey() value, then only the first is used.
func removeDuplicateKeys(keys []CacheKeyer) []CacheKeyer {
	// keyMap used to determine if a given key has already been seen
	keyMap := make(map[string]bool)
	// results used to collect the unique CacheKeyer values
	results := make([]CacheKeyer, 0, len(keys))

	for _, key := range keys {
		if _, ok := keyMap[key.CacheKey()]; !ok {
			keyMap[key.CacheKey()] = true
			results = append(results, key)
		}
	}

	return results
}

type CacheKey string

func (k CacheKey) CacheKey() string {
	return string(k)
}

func CacheKeyersFromStrings(ids []string) []CacheKeyer {
	keys := make([]CacheKeyer, len(ids))
	for i, id := range ids {
		keys[i] = CacheKey(id)
	}
	return keys
}
