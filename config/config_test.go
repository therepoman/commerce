package config_test

import (
	"testing"

	"os"

	"code.justin.tv/commerce/config"
	"github.com/stretchr/testify/assert"
)

type configuration struct {
	TestValue string `valid:"required"`
}

type badConfiguration struct {
	BadTestValue string `validate:"nonzero"`
}

func TestDefaultEnvironment(t *testing.T) {
	environment := config.GetEnvironment()

	assert.Equal(t, "dev", environment)
}

func TestDevConfig(t *testing.T) {
	environment := "dev"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization: "commerce",
		RepoService:      "config",
		Environment:      environment,
	})

	assert.Nil(t, err)
	assert.Equal(t, "development_value", cfg.TestValue)
}

func TestStagingConfig(t *testing.T) {
	environment := "staging"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization: "commerce",
		RepoService:      "config",
		Environment:      environment,
	})

	assert.Nil(t, err)
	assert.Equal(t, "staging_value", cfg.TestValue)
}

func TestProdConfig(t *testing.T) {
	environment := "prod"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization: "commerce",
		RepoService:      "config",
		Environment:      environment,
	})

	assert.Nil(t, err)
	assert.Equal(t, "production_value", cfg.TestValue)
}

func TestLocalPathOverride(t *testing.T) {
	environment := "dev"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization:  "commerce",
		RepoService:       "config",
		Environment:       environment,
		LocalPathOverride: os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/config/config/dev.json",
	})

	assert.Nil(t, err)
	assert.Equal(t, "development_value", cfg.TestValue)
}

func TestGlobalPathOverride(t *testing.T) {
	environment := "dev"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization:   "commerce",
		RepoService:        "config",
		Environment:        environment,
		LocalPathOverride:  "",
		GlobalPathOverride: os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/config/config/dev.json",
	})

	assert.Nil(t, err)
	assert.Equal(t, "development_value", cfg.TestValue)
}

func TestEmptyOptions(t *testing.T) {
	environment := "dev"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		Environment: environment,
	})

	assert.Error(t, err, "missing organization name or service name")
}

func TestBadOptions(t *testing.T) {
	environment := "dev"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization: "commerce",
		RepoService:      "foobar",
		Environment:      environment,
	})

	assert.Error(t, err, "failed to open config file")
}

func TestBadValidation(t *testing.T) {
	environment := "dev"
	cfg := &badConfiguration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization: "commerce",
		RepoService:      "foobar",
		Environment:      environment,
	})

	assert.Error(t, err)
}

func TestBadFileDecode(t *testing.T) {
	environment := "dev"
	cfg := &configuration{}
	err := config.LoadConfigForEnvironment(cfg, &config.Options{
		RepoOrganization:  "commerce",
		RepoService:       "config",
		Environment:       environment,
		LocalPathOverride: os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/config/test_bad.json",
	})

	assert.Error(t, err)
}
