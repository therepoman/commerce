package httptracing

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
)

type roundTripper struct {
	base http.RoundTripper
	name string
}

func NewRoundTripper(name string) func(http.RoundTripper) http.RoundTripper {
	return func(base http.RoundTripper) http.RoundTripper {
		return &roundTripper{
			base: base,
			name: name,
		}
	}
}

func (r *roundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	urlParts := strings.Split(req.URL.Path, "/")
	method := urlParts[len(urlParts)-1]

	span, spanCtx := opentracing.StartSpanFromContext(req.Context(), r.name+"."+method)

	span.SetTag("component", "twirp")
	span.SetTag("peer.service", r.name)
	span.SetTag("span.kind", "client")

	_ = opentracing.GlobalTracer().Inject(
		span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(req.Header),
	)

	defer span.Finish()

	res, err := r.base.RoundTrip(req.WithContext(spanCtx))
	if err != nil {
		span.SetTag("error", true)
		span.LogFields(
			log.Error(fmt.Errorf("twirp responded with error code '%s'", err.Error())),
		)

		return nil, err
	}

	span.SetTag("component", "twirp")
	span.SetTag("http.method", req.Method)
	span.SetTag("http.status_code", res.StatusCode)

	if res.StatusCode >= 400 {
		span.SetTag("error", true)
		span.LogFields(
			log.Error(fmt.Errorf("twirp responded with error code '%v'", res.StatusCode)),
		)
	}

	return res, nil
}
