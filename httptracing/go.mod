module code.justin.tv/commerce/httptracing

go 1.14

require github.com/opentracing/opentracing-go v1.2.0
