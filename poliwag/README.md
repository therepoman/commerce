<p align="center">
    <img alt="pollingwag - shoutout to @hansylvi" src="/assets/pollingwag-banner.png" height="350"></a>
    <br />
    <br />
    <a href="https://jenkins.internal.justin.tv/job/commerce/job/poliwag/">
        <img alt="build status" src="https://jenkins.internal.justin.tv/job/commerce/job/poliwag/job/master/badge/icon">
    </a>
</p>

## Intro

Poliwag is a Twirp-powered Go service for creating and maintaining channel polls at Twitch.

## Endpoints

Contact the [#moments](https://twitch.slack.com/archives/CNKB0U0B0) team if you need to private-link your AWS account with poliwag.

| Environment | Endpoint | AWS Account | AWS Account ID |
| --- | --- | --- | --- |
| Staging | https://main.us-west-2.beta.poliwag.twitch.a2z.com | [twitch-poliwag-aws-devo@amazon.com](https://isengard.amazon.com/federate?account=720603942490&role=admin) | `720603942490` | 
| Prod | https://main.us-west-2.prod.poliwag.twitch.a2z.com | [twitch-poliwag-aws-prod@amazon.com](https://isengard.amazon.com/federate?account=246232734983&role=admin) | `246232734983` |

## Credentials

Poliwag utilizes [Amazon Aurora](https://aws.amazon.com/rds/aurora/) to store votes in a relational MySQL database. You'll need credentials stored in [sandstorm](https://dashboard.internal.justin.tv/sandstorm) to access it. This service cannot be accessed directly on the public network and must be tunneled through our jumpbox.

### Getting Jumpbox credentials

For now, contact the [#moments](https://twitch.slack.com/archives/CNKB0U0B0) and we'll get you access for your LDAP account.

### Getting Aurora Credentials
Aurora passwords are stored in [sandstorm](https://dashboard.internal.justin.tv/sandstorm). To access these passwords locally so you can connect to the Aurora instance yourself, simply run the script in `cmd/sandstorm-dumper/main.go`
```
go run ./cmd/sandstorm-dumper/main.go <sandstorm_secret_key>
```
Your AWS IAM user must be granted access to the poliwag sandstorm role via the [sandstorm dashboard](https://dashboard.internal.justin.tv/sandstorm/manage-secrets). Contact [#moments](https://twitch.slack.com/archives/CNKB0U0B0) team if you need this.

### Aurora Credentials

#### Master Credentials
| Environment | Sandstorm Secret |
| --- | --- |
| Staging | [commerce/poliwag/staging/aurora-master-password](https://dashboard.internal.justin.tv/sandstorm/manage-secrets?nameFilter=staging%2Faurora-master-password) |
| Prod | [commerce/poliwag/production/aurora-master-password](https://dashboard.internal.justin.tv/sandstorm/manage-secrets?nameFilter=production%2Faurora-master-password) |

#### Service Credentials 
| Environment | Sandstorm Secret |
| --- | --- |
| Staging | [commerce/poliwag/staging/aurora-service-password](https://dashboard.internal.justin.tv/sandstorm/manage-secrets?nameFilter=staging%2Faurora-service-password) |
| Prod | [commerce/poliwag/production/aurora-service-password](https://dashboard.internal.justin.tv/sandstorm/manage-secrets?nameFilter=production%2Faurora-service-password) |

## Development 
    
### Running Locally
1. Ensure you have proper credentials (see above).

2. Create an SSH tunnel from your machine's port `3306` to the staging Amazon Aurora instance. You can tunnel through the VPC jumpbox:
   ```
   TC=twitch-poliwag-staging ssh -L 3306:<aurora_cluster_endpoint_from_RDS_dashboard>:3306 <jumpbox_endpoint>
   ```  
   This creates a tunnel on `localhost:3306` that goes through the jumpbox and terminates at the Aurora cluster. Any requests to `localhost:3306` will forward to the staging Aurora cluster.
   
3. Make sure you have [redis](https://redis.io/topics/quickstart) running locally:
   ```
   redis-server
   ```
   
4. Now you can start up poliwag locally:
   ```
   make run
   ```

### Connecting to Aurora via Mysql Client

When debugging votes, it may help to see the actual vote records in Amazon Aurora. You can access the RDS instance by:

1. SSH into the teleport remote jumpbox
   ```shell script
    TC=twitch-poliwag-staging ssh <jumpbox_host>
   ```
2. Connect to [MySQL](https://dev.mysql.com/doc/):
   ```shell script
   mysql --host=<aurora_cluster_endpoint_from_RDS_dashboard> --port=3306 --user=admin --password=<password_from_sandstorm> 
   ```

3. Use the `poliwag` database:
   ```
   > USE poliwag;
   ```
   
4. Perform your MySQL queries!

### Infrastructure / Terraform 

Poliwag makes use of [Terraform](https://www.terraform.io/) to manage it's infrastructure.

The service is currently on version `v0.11.x` of [Terraform](https://www.terraform.io/). Ensure you are running this version! (tfenv can help)

| Terraform | File |
| --- | --- |
| Staging | [`/terraform/staging`](/terraform/staging) |
| Prod | [`/terraform/prod`](/terraform/prod) |

Run `terraform init` in one of these folders to get started.

### Updating GraphQL Documentation

```bash
make gql_docs
```
