#### Last updated: Tue, 17 Mar 2020 11:59:46 PDT
# Table of Contents
- [Fields](#Fields)
  - [User](#User)
  - [Query](#Query)
- [Objects](#Objects)
  - [ArchivePollInput](#ArchivePollInput)
  - [ArchivePollPayload](#ArchivePollPayload)
  - [CreatePollChoiceInput](#CreatePollChoiceInput)
  - [CreatePollError](#CreatePollError)
  - [CreatePollInput](#CreatePollInput)
  - [CreatePollPayload](#CreatePollPayload)
  - [PageInfo](#PageInfo)
  - [Poll](#Poll)
  - [PollChoice](#PollChoice)
  - [PollChoiceSelfEdge](#PollChoiceSelfEdge)
  - [PollChoiceVoterConnection](#PollChoiceVoterConnection)
  - [PollConnection](#PollConnection)
  - [PollEdge](#PollEdge)
  - [PollSelfEdge](#PollSelfEdge)
  - [PollSettings](#PollSettings)
  - [PollSettingsBitsVotes](#PollSettingsBitsVotes)
  - [PollSettingsCommunityPointsVotes](#PollSettingsCommunityPointsVotes)
  - [PollSettingsMultichoice](#PollSettingsMultichoice)
  - [PollSettingsSubscriberMultiplier](#PollSettingsSubscriberMultiplier)
  - [PollSettingsSubscriberOnly](#PollSettingsSubscriberOnly)
  - [PollTokenBreakdown](#PollTokenBreakdown)
  - [PollTopBitsContributor](#PollTopBitsContributor)
  - [PollTopCommunityPointsContributor](#PollTopCommunityPointsContributor)
  - [PollTopContributor](#PollTopContributor)
  - [PollVoteBreakdown](#PollVoteBreakdown)
  - [PollVoteTokensInput](#PollVoteTokensInput)
  - [PollVoter](#PollVoter)
  - [PollVoterChoice](#PollVoterChoice)
  - [PollVoterConnectionEdge](#PollVoterConnectionEdge)
  - [TerminatePollInput](#TerminatePollInput)
  - [TerminatePollPayload](#TerminatePollPayload)
  - [VoteInPollError](#VoteInPollError)
  - [VoteInPollInput](#VoteInPollInput)
  - [VoteInPollPayload](#VoteInPollPayload)
- [Enums](#Enums)
  - [CreatePollErrorCode](#CreatePollErrorCode)
  - [PollSort](#PollSort)
  - [PollStatus](#PollStatus)
  - [PollVoterConnectionSort](#PollVoterConnectionSort)
  - [PollVoterConnectionSortDirection](#PollVoterConnectionSortDirection)
  - [SortOrder](#SortOrder)
  - [VoteInPollErrorCode](#VoteInPollErrorCode)
- [Mutations](#Mutations)
  - [archivePoll](#archivePoll)
  - [createPoll](#createPoll)
  - [terminatePoll](#terminatePoll)
  - [voteInPoll](#voteInPoll)
# Fields
## User
*Twitch user.*
### Fields:
Name | Type | Description
--- | --- | ---
latestPoll | [Poll](#Poll) | Fetches the latest created Poll for the user.
polls (first: [Int](#Int) = 15, after: [Cursor](#Cursor), sort: [PollSort](#PollSort) = START_TIME, direction: [SortOrder](#SortOrder) = DESC, status: [[PollStatus!]](#PollStatus))| [PollConnection](#PollConnection) | Fetch polls for the user based on a variety of sorting options. By default returns 15 polls, sorted by start time.
viewablePoll | [Poll](#Poll) | The viewable poll for a channel. Null if no viewable poll is present.
## Query
*Root fields to access the Twitch API.*
### Fields:
Name | Type | Description
--- | --- | ---
poll (id: [ID!](#ID))| [Poll](#Poll) | Returns a poll by its ID.
# Objects
## ArchivePollInput
*Inputs for archiving a poll.*
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
pollID | [ID!](#ID) | The id of the poll to archive. | *n/a*
## ArchivePollPayload
*Outputs from the create poll mutation.*
### Fields:
Name | Type | Description
--- | --- | ---
poll | [Poll](#Poll) | The created poll.
## CreatePollChoiceInput
*Inputs for creating a choice.*
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
title | [String!](#String) | Title of the choice. | *n/a*
## CreatePollError
*Vote in poll error.*
### Fields:
Name | Type | Description
--- | --- | ---
code | [CreatePollErrorCode!](#CreatePollErrorCode) | Code describing the error.
## CreatePollInput
*Inputs for creating a new poll.*
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
bitsCost | [Int](#Int) | The cost in bits for casting a vote. | 0
bitsVoting | [Boolean](#Boolean) | Denotes if votes can be cast with bits. | false
choices | [[CreatePollChoiceInput!]!](#CreatePollChoiceInput) | Choices that can be voted for in the poll. | *n/a*
communityPointsCost | [Int](#Int) | The cost in Community Points for casting a vote. | 0
durationSeconds | [Int!](#Int) | Duration of the poll in seconds. | *n/a*
isCommunityPointsVotingEnabled | [Boolean](#Boolean) | Denotes if votes can be cast with Community Points. | false
multichoiceEnabled | [Boolean](#Boolean) | Denotes if the poll allows voting for multiple options. | true
ownedBy | [ID!](#ID) | Id of the channel this poll is owned by. | *n/a*
subscriberMultiplier | [Boolean](#Boolean) | Denotes if subscribers receives bonus votes. Deprecated: Subscriber multipliers are no longer supported. | false
subscriberOnly | [Boolean](#Boolean) | Denotes if the poll is only open to subscribers. Deprecated: Subscriber-only polls are no longer supported. | false
title | [String!](#String) | Title of the poll. | *n/a*
## CreatePollPayload
*Outputs from the create poll mutation.*
### Fields:
Name | Type | Description
--- | --- | ---
error | [CreatePollError](#CreatePollError) | If present, there was an error with the request.
poll | [Poll](#Poll) | The created poll.
## PageInfo
*PageInfo is a special field which contains information about the page,
specifically the cursors which the page starts and ends, and whether or
not the client can forward-paginate or backward-paginate.

This is part of the Relay Cursor Connections Specification:
https://facebook.github.io/relay/graphql/connections.htm.*
### Fields:
Name | Type | Description
--- | --- | ---
hasNextPage | [Boolean!](#Boolean) | 
hasPreviousPage | [Boolean!](#Boolean) | 
## Poll
*A poll users can vote in.*
### Fields:
Name | Type | Description
--- | --- | ---
choice (id: [ID!](#ID))| [PollChoice](#PollChoice) | A choice specified by a choice id.
choices | [[PollChoice!]!](#PollChoice) | A list of choices users can vote for.
createdBy | [User](#User) | User that created the poll. Mods and editors can make polls on behalf of a broadcaster.
durationSeconds | [Int!](#Int) | Amount of seconds from when the poll starts to when it ends. Since a broadcaster can end a poll early ("terminate a poll"), it may be possible for endedAt - startedAt != duration.
endedAt | [Time](#Time) | Time when the poll ended. Null if the poll is still active.
endedBy | [User](#User) | User that ended the poll. Mods and editors can end polls on behalf of a broadcaster. Null if no user manually ended the poll.
id | [ID!](#ID) | ID of poll.
isViewable | [Boolean!](#Boolean) | Whether the poll is viewable by other users.
ownedBy | [User](#User) | User who owns this poll. The poll will appear on their channel.
remainingDurationMilliseconds | [Int!](#Int) | Amount of milliseconds before the poll ends. 0 when the polls is ended.
self | [PollSelfEdge](#PollSelfEdge) | The authenticated user's relationship with this poll. Main use case is to check if the user has voted in the poll already. Null if un-authenticated user is making this query.
settings | [PollSettings!](#PollSettings) | A map of poll settings.
startedAt | [Time!](#Time) | Time when poll started.
status | [PollStatus!](#PollStatus) | The status of the poll.
title | [String!](#String) | Title of poll.
tokens | [PollTokenBreakdown!](#PollTokenBreakdown) | A breakdown of the different tokens used in this poll.
topBitsContributor | [PollTopBitsContributor](#PollTopBitsContributor) | The top Bits contributor for the poll.
topCommunityPointsContributor | [PollTopCommunityPointsContributor](#PollTopCommunityPointsContributor) | The top Community Points contributor for the poll.
topContributor | [PollTopContributor](#PollTopContributor) | The top Bits contributor for the poll. **Deprecated**: Use topBitsContributor instead.
totalVoters | [Int!](#Int) | Total number of unique voters that have voted in this poll.
votes | [PollVoteBreakdown!](#PollVoteBreakdown) | A breakdown of the different votes cast in this poll.
## PollChoice
*A choice in a poll that users can vote for.*
### Fields:
Name | Type | Description
--- | --- | ---
id | [ID!](#ID) | ID of choice.
self | [PollChoiceSelfEdge!](#PollChoiceSelfEdge) | The authenticated user's relationship with this choice.
title | [String!](#String) | The title of the choice.
tokens | [PollTokenBreakdown!](#PollTokenBreakdown) | A breakdown of the different tokens used for this choice.
totalVoters | [Int!](#Int) | Total number of unique voters that have voted for this choice.
voters (first: [Int](#Int) = 15, after: [Cursor](#Cursor), sort: [PollVoterConnectionSort](#PollVoterConnectionSort) = VOTES, direction: [PollVoterConnectionSortDirection](#PollVoterConnectionSortDirection) = DESC)| [PollChoiceVoterConnection](#PollChoiceVoterConnection) | A list of voters for this choice. Only the poll's ownerID and their mods/editors can search for this.
votes | [PollVoteBreakdown!](#PollVoteBreakdown) | A breakdown of the different votes cast for this choice.
## PollChoiceSelfEdge
*A connection between poll choice and the authenticated user.*
### Fields:
Name | Type | Description
--- | --- | ---
voter | [PollVoter](#PollVoter) | The voter object pertaining to the authenticated user.
## PollChoiceVoterConnection
*A connection between poll and voters and metadata.*
### Fields:
Name | Type | Description
--- | --- | ---
nodes | [[PollVoterConnectionEdge!]!](#PollVoterConnectionEdge) | The list of voters in this poll / choice.
## PollConnection
*A connection between poll and metadata.*
### Fields:
Name | Type | Description
--- | --- | ---
edges | [[PollEdge!]](#PollEdge) | The list of polls for the user.
pageInfo | [PageInfo!](#PageInfo) | Information about pagination in this connection.
## PollEdge
*A page entry, that contains the Poll item and a cursor to return from the query to allow pagination.*
### Fields:
Name | Type | Description
--- | --- | ---
cursor | [Cursor!](#Cursor) | Cursor used for next query.
node | [Poll!](#Poll) | The underlying poll voter.
## PollSelfEdge
*A connection between a poll and the authenticated user.*
### Fields:
Name | Type | Description
--- | --- | ---
voter | [PollVoter](#PollVoter) | The voter object pertaining to the authenticated user.
## PollSettings
*Poll Settings.*
### Fields:
Name | Type | Description
--- | --- | ---
bitsVotes | [PollSettingsBitsVotes!](#PollSettingsBitsVotes) | Bits votes poll settings.
communityPointsVotes | [PollSettingsCommunityPointsVotes!](#PollSettingsCommunityPointsVotes) | Channel Points votes poll settings.
id | [ID!](#ID) | ID of poll settings.
multichoice | [PollSettingsMultichoice!](#PollSettingsMultichoice) | Multichoice voting poll settings.
subscriberMultiplier | [PollSettingsSubscriberMultiplier!](#PollSettingsSubscriberMultiplier) | Subscriber multiplier poll settings. **Deprecated**: Subscriber multipliers are no longer supported.
subscriberOnly | [PollSettingsSubscriberOnly!](#PollSettingsSubscriberOnly) | Subscriber only poll settings. **Deprecated**: Subscriber-only polls are no longer supported.
## PollSettingsBitsVotes
*Bits votes poll settings.*
### Fields:
Name | Type | Description
--- | --- | ---
cost | [Int!](#Int) | Cost of bits for a vote.
isEnabled | [Boolean!](#Boolean) | Whether the poll allows for users to use bits for votes.
## PollSettingsCommunityPointsVotes
*Channel Points votes poll settings.*
### Fields:
Name | Type | Description
--- | --- | ---
cost | [Int!](#Int) | Cost of channel points for a vote.
isEnabled | [Boolean!](#Boolean) | Whether the poll allows for users to use channel points for votes.
## PollSettingsMultichoice
*Multichoice voting enabled poll setting.*
### Fields:
Name | Type | Description
--- | --- | ---
isEnabled | [Boolean!](#Boolean) | Whether the poll has multichoice voting enabled.
## PollSettingsSubscriberMultiplier
*Subscriber multiplier poll settings.
Deprecated: subscriber multipliers are no longer supported.*
### Fields:
Name | Type | Description
--- | --- | ---
isEnabled | [Boolean!](#Boolean) | Whether the poll has a subscriber multipler.
## PollSettingsSubscriberOnly
*Subscriber only poll settings.
Deprecated: subscriber-only polls are no longer supported.*
### Fields:
Name | Type | Description
--- | --- | ---
isEnabled | [Boolean!](#Boolean) | Whether the poll is subscriber only.
## PollTokenBreakdown
*A breakdown of tokens used/by for this poll/choice/user.*
### Fields:
Name | Type | Description
--- | --- | ---
bits | [Int!](#Int) | Total number of bits used.
communityPoints | [Int!](#Int) | Total number of community points used.
id | [ID!](#ID) | ID of token breakdown.
## PollTopBitsContributor
*Top contributor to the poll and the bits they contributed.*
### Fields:
Name | Type | Description
--- | --- | ---
bitsAmount | [Int!](#Int) | Bits amount contributed.
user | [User](#User) | Top contributor to the poll.
## PollTopCommunityPointsContributor
*Top contributor to the poll and the community points they contributed.*
### Fields:
Name | Type | Description
--- | --- | ---
communityPointsAmount | [Int!](#Int) | Community Points amount contributed.
user | [User](#User) | Top contributor to the poll.
## PollTopContributor
*Top contributor to the poll and the bits they contributed.
Deprecated: use PollTopBitsContributor instead.*
### Fields:
Name | Type | Description
--- | --- | ---
bitsAmount | [Int!](#Int) | Bits amount contributed.
user | [User](#User) | Top contributor to the poll.
## PollVoteBreakdown
*A breakdown of votes cast for/by this poll/choice/user.*
### Fields:
Name | Type | Description
--- | --- | ---
base | [Int!](#Int) | Total number of base votes.
bits | [Int!](#Int) | Total number of votes due to bits contributions.
communityPoints | [Int!](#Int) | Total number of votes due to Community Points contributions.
id | [ID!](#ID) | ID of vote breakdown.
total | [Int!](#Int) | Total number of votes across all different vote types.
## PollVoteTokensInput
*The tokens that are used on a vote.*
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
bits | [Int!](#Int) | The amount of bits used for this vote. | *n/a*
channelPoints | [Int!](#Int) | The amount of channel points used for this vote. | *n/a*
## PollVoter
*A voter taking part in a poll and associated information.*
### Fields:
Name | Type | Description
--- | --- | ---
choices | [[PollVoterChoice!]!](#PollVoterChoice) | The Choices this voter voted for.
id | [ID!](#ID) | id of the voter in the poll.
poll | [Poll](#Poll) | The Poll this voter voted in.
tokens | [PollTokenBreakdown!](#PollTokenBreakdown) | A breakdown of the different tokens used by the voter in the poll.
user | [User](#User) | The User object relating to this voter.
votes | [PollVoteBreakdown!](#PollVoteBreakdown) | A breakdown of the different votes this voter used in the poll.
## PollVoterChoice
*A choice a voter made taking part in a poll.*
### Fields:
Name | Type | Description
--- | --- | ---
id | [ID!](#ID) | The id of the PollVoterChoice.
pollChoice | [PollChoice](#PollChoice) | The poll choice.
tokens | [PollTokenBreakdown!](#PollTokenBreakdown) | A breakdown of the different tokens used by the voter.
votes | [PollVoteBreakdown!](#PollVoteBreakdown) | A breakdown of the different votes this voter used.
## PollVoterConnectionEdge
*A page entry, that contains the PollVoter item and a cursor to return from the query to allow pagination.*
### Fields:
Name | Type | Description
--- | --- | ---
cursor | [Cursor!](#Cursor) | Cursor used for next query.
node | [PollVoter!](#PollVoter) | The underlying poll voter.
## TerminatePollInput
*Inputs for terminating a poll.*
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
pollID | [ID!](#ID) | The id of the poll to terminate. | *n/a*
## TerminatePollPayload
*Outputs from the create poll mutation.*
### Fields:
Name | Type | Description
--- | --- | ---
poll | [Poll](#Poll) | The created poll.
## VoteInPollError
*Vote in poll error.*
### Fields:
Name | Type | Description
--- | --- | ---
code | [VoteInPollErrorCode!](#VoteInPollErrorCode) | Code describing the error.
## VoteInPollInput
*Inputs for voting on a poll.*
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
choiceID | [ID!](#ID) | The id of the choice the vote is casted on in the poll. | *n/a*
pollID | [ID!](#ID) | The id of the poll that is being voted in. | *n/a*
tokens | [PollVoteTokensInput](#PollVoteTokensInput) | The tokens used for this vote. | *n/a*
userID | [ID!](#ID) | The user id of the voter. | *n/a*
voteID | [ID!](#ID) | The unique id for this vote action. | *n/a*
## VoteInPollPayload
*Output from the vote on poll mutation.*
### Fields:
Name | Type | Description
--- | --- | ---
error | [VoteInPollError](#VoteInPollError) | If present, there was an error with the request.
voter | [PollVoter](#PollVoter) | The voter objet returned by the vote operation.
# Enums
## CreatePollErrorCode
*Vote in poll error code.*
### Values:
Name |  Description
--- | ---
AUTOMOD_FAILED | User attempted to create poll with restricted content.
POLL_ALREADY_ACTIVE | User attempted to create poll when a poll was already active.
CHANNEL_NOT_BITS_ENABLED | User attempted to create poll with bits on a channel where bits are not enabled.
UNKNOWN | An unknown error occurred.
## PollSort
*Possible sort orders for lists of polls.*
### Values:
Name |  Description
--- | ---
START_TIME | Sort the polls by time.
## PollStatus
*The status of the poll.*
### Values:
Name |  Description
--- | ---
UNKNOWN | Encountered some poll status that we do not know how to handle BibleThump.
ACTIVE | Poll is running. Users can vote. Results are publicly visible.
COMPLETED | Poll ran its entire duration and "naturally" completed. Users cannot vote. Results are publicly visible.
TERMINATED | Poll was manually ended ("terminated") by a user. Users cannot vote. Results are publicly visible.
ARCHIVED | Poll has ended and is no longer publicly visible. Users cannot vote. Results are not publicly visible.
MODERATED | Poll has been moderated by Twitch and is no longer viewable, even to the poll owner. Users cannot vote. Results are not visible to any user.
## PollVoterConnectionSort
*Possible sort orders for lists of voters.*
### Values:
Name |  Description
--- | ---
VOTES | Sort the voters by # of votes.
CREATED_DATE | Sort the voters by time of creation.
BITS | Sort the voters by amount of Bits on vote.
CHANNEL_POINTS | Sort the voters by amount of channel points.
## PollVoterConnectionSortDirection
*Possible sort directions for lists of voters.*
### Values:
Name |  Description
--- | ---
DESC | Sort in descending order.
ASC | Sort in ascending order.
## SortOrder
*Sort direction.*
### Values:
Name |  Description
--- | ---
ASC | Ascending (A-Z, 1-9).
DESC | Descending (Z-A, 9-1).
## VoteInPollErrorCode
*Vote in poll error code.*
### Values:
Name |  Description
--- | ---
POLL_NOT_FOUND | User tried to vote in a poll that doesn't exist i.e. there is no poll running at all).
POLL_NOT_ACTIVE | User tried to vote in a poll that is not active.
VOTE_ID_CONFLICT | Request has a vote ID that is in progress or has already been submitted.
MULTI_CHOICE_VOTE_FORBIDDEN | User is trying to vote for another choice in a poll that only allows one choice.
INVALID_CHANNEL_ID | Request is for a channel ID that is invalid (e.g. banned channel, channel doesn't exist).
INVALID_CHOICE_INDEX | Request is for a choice index that is invalid (e.g. there are 3 choices and the request is for index 10).
INVALID_CHOICE_ID | Request is for a choice ID that invalid.
INVALID_BITS_AMOUNT | Request is for an invalid bits amount (e.g. -100 bits, or request is not for a multiple of the Bits cost).
INVALID_COMMUNITY_POINTS_AMOUNT | Request is for an invalid Community Points amount (e.g. -100 Points, or request is not for a multiple of the Points cost).
INSUFFICIENT_BITS_BALANCE | User's bits balance is too low to perform request (e.g. they only have 10 bits and they request to spend 100).
INSUFFICIENT_COMMUNITY_POINTS_BALANCE | Users's Community Points balance is too low to perform request (e.g. they only have 10 points and they request to spend 100).
TOKENS_REQUIRED | User has already used their base votes, so in order to continue voting, they must use tokens (bits or channel points).
USER_FORBIDDEN | User is not allowed to vote in poll (e.g. they're banned in the channel).
SELF_BITS_VOTE_NOT_ALLOWED | User is not allowed to vote in their own poll with bits.
RATE_LIMITED | User cannot vote because they have hit a per-user or per-poll rate limit. The user can try again later.
UNKNOWN | An unknown error occurred.
# Mutations
## archivePoll
*Archive a poll with the given poll id.*
##### archivePoll (input: [ArchivePollInput!](#ArchivePollInput)): [ArchivePollPayload](#ArchivePollPayload)
## createPoll
*createPoll creates a poll.*
##### createPoll (input: [CreatePollInput!](#CreatePollInput)): [CreatePollPayload](#CreatePollPayload)
## terminatePoll
*Terminates the poll with the given poll id.*
##### terminatePoll (input: [TerminatePollInput!](#TerminatePollInput)): [TerminatePollPayload](#TerminatePollPayload)
## voteInPoll
*Casts a vote for a specific choice in a poll.*
##### voteInPoll (input: [VoteInPollInput!](#VoteInPollInput)): [VoteInPollPayload](#VoteInPollPayload)
