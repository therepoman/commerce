# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [proto/poliwag/poliwag_api.proto](#proto/poliwag/poliwag_api.proto)
    - [ArchivePollRequest](#code.justin.tv.commerce.poliwag.ArchivePollRequest)
    - [ArchivePollResponse](#code.justin.tv.commerce.poliwag.ArchivePollResponse)
    - [Choice](#code.justin.tv.commerce.poliwag.Choice)
    - [CreatePollRequest](#code.justin.tv.commerce.poliwag.CreatePollRequest)
    - [CreatePollRequest.CreatePollChoice](#code.justin.tv.commerce.poliwag.CreatePollRequest.CreatePollChoice)
    - [CreatePollResponse](#code.justin.tv.commerce.poliwag.CreatePollResponse)
    - [GetPollRequest](#code.justin.tv.commerce.poliwag.GetPollRequest)
    - [GetPollResponse](#code.justin.tv.commerce.poliwag.GetPollResponse)
    - [GetPollsRequest](#code.justin.tv.commerce.poliwag.GetPollsRequest)
    - [GetPollsResponse](#code.justin.tv.commerce.poliwag.GetPollsResponse)
    - [GetViewablePollRequest](#code.justin.tv.commerce.poliwag.GetViewablePollRequest)
    - [GetViewablePollResponse](#code.justin.tv.commerce.poliwag.GetViewablePollResponse)
    - [GetVoterRequest](#code.justin.tv.commerce.poliwag.GetVoterRequest)
    - [GetVoterResponse](#code.justin.tv.commerce.poliwag.GetVoterResponse)
    - [GetVotersByChoiceRequest](#code.justin.tv.commerce.poliwag.GetVotersByChoiceRequest)
    - [GetVotersByChoiceResponse](#code.justin.tv.commerce.poliwag.GetVotersByChoiceResponse)
    - [GetVotersRequest](#code.justin.tv.commerce.poliwag.GetVotersRequest)
    - [GetVotersResponse](#code.justin.tv.commerce.poliwag.GetVotersResponse)
    - [Poll](#code.justin.tv.commerce.poliwag.Poll)
    - [PollSettings](#code.justin.tv.commerce.poliwag.PollSettings)
    - [PollSettings.BitsVotesPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.BitsVotesPollSetting)
    - [PollSettings.ChannelPointsVotesPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.ChannelPointsVotesPollSetting)
    - [PollSettings.MultiChoicePollSetting](#code.justin.tv.commerce.poliwag.PollSettings.MultiChoicePollSetting)
    - [PollSettings.SettingsSubscriberOnlyPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.SettingsSubscriberOnlyPollSetting)
    - [PollSettings.SubscriberMultiplierPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.SubscriberMultiplierPollSetting)
    - [TerminatePollRequest](#code.justin.tv.commerce.poliwag.TerminatePollRequest)
    - [TerminatePollResponse](#code.justin.tv.commerce.poliwag.TerminatePollResponse)
    - [Tokens](#code.justin.tv.commerce.poliwag.Tokens)
    - [TopBitsContributor](#code.justin.tv.commerce.poliwag.TopBitsContributor)
    - [TopChannelPointsContributor](#code.justin.tv.commerce.poliwag.TopChannelPointsContributor)
    - [TopContributor](#code.justin.tv.commerce.poliwag.TopContributor)
    - [VoteRequest](#code.justin.tv.commerce.poliwag.VoteRequest)
    - [VoteResponse](#code.justin.tv.commerce.poliwag.VoteResponse)
    - [Voter](#code.justin.tv.commerce.poliwag.Voter)
    - [VoterChoice](#code.justin.tv.commerce.poliwag.VoterChoice)
    - [Votes](#code.justin.tv.commerce.poliwag.Votes)
  
    - [Direction](#code.justin.tv.commerce.poliwag.Direction)
    - [GetPollsSort](#code.justin.tv.commerce.poliwag.GetPollsSort)
    - [PollCreationErrorCode](#code.justin.tv.commerce.poliwag.PollCreationErrorCode)
    - [PollStatus](#code.justin.tv.commerce.poliwag.PollStatus)
    - [VoteErrorCode](#code.justin.tv.commerce.poliwag.VoteErrorCode)
    - [VoterSort](#code.justin.tv.commerce.poliwag.VoterSort)
  
    - [PoliwagAPI](#code.justin.tv.commerce.poliwag.PoliwagAPI)
  
- [Scalar Value Types](#scalar-value-types)



<a name="proto/poliwag/poliwag_api.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## proto/poliwag/poliwag_api.proto



<a name="code.justin.tv.commerce.poliwag.ArchivePollRequest"></a>

### ArchivePollRequest
Request to ArchivePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of user performing the archive |
| poll_id | [string](#string) |  | ID of the poll to archive |






<a name="code.justin.tv.commerce.poliwag.ArchivePollResponse"></a>

### ArchivePollResponse
Response from ArchivePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll | [Poll](#code.justin.tv.commerce.poliwag.Poll) |  |  |






<a name="code.justin.tv.commerce.poliwag.Choice"></a>

### Choice
Information about a choice in a poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| choice_id | [string](#string) |  |  |
| title | [string](#string) |  |  |
| votes | [Votes](#code.justin.tv.commerce.poliwag.Votes) |  | A breakdown of how many votes were cast for the choice. |
| tokens | [Tokens](#code.justin.tv.commerce.poliwag.Tokens) |  | A breakdown of how many tokens were spent for the choice. |
| total_voters | [int64](#int64) |  | Total number of unique voters for the choice. |






<a name="code.justin.tv.commerce.poliwag.CreatePollRequest"></a>

### CreatePollRequest
Request to CreatePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of user making the request. |
| owned_by | [string](#string) |  | User who owns the poll. |
| title | [string](#string) |  |  |
| duration | [google.protobuf.Duration](#google.protobuf.Duration) |  |  |
| choices | [CreatePollRequest.CreatePollChoice](#code.justin.tv.commerce.poliwag.CreatePollRequest.CreatePollChoice) | repeated |  |
| settings | [PollSettings](#code.justin.tv.commerce.poliwag.PollSettings) |  |  |






<a name="code.justin.tv.commerce.poliwag.CreatePollRequest.CreatePollChoice"></a>

### CreatePollRequest.CreatePollChoice
Information about choices in the poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| title | [string](#string) |  |  |






<a name="code.justin.tv.commerce.poliwag.CreatePollResponse"></a>

### CreatePollResponse
Response from CreatePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll | [Poll](#code.justin.tv.commerce.poliwag.Poll) |  |  |






<a name="code.justin.tv.commerce.poliwag.GetPollRequest"></a>

### GetPollRequest
Request to GetPoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of user making the request |
| poll_id | [string](#string) |  | ID of the poll to request |






<a name="code.justin.tv.commerce.poliwag.GetPollResponse"></a>

### GetPollResponse
Response from GetPoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll | [Poll](#code.justin.tv.commerce.poliwag.Poll) |  |  |






<a name="code.justin.tv.commerce.poliwag.GetPollsRequest"></a>

### GetPollsRequest
Request to GetPolls API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of user making the request |
| owned_by | [string](#string) |  | ID of the owner of the polls to request |
| status | [PollStatus](#code.justin.tv.commerce.poliwag.PollStatus) | repeated | Status of the polls to search for |
| sort | [GetPollsSort](#code.justin.tv.commerce.poliwag.GetPollsSort) |  | Sorting of the polls |
| direction | [Direction](#code.justin.tv.commerce.poliwag.Direction) |  | Direction to query for polls |
| limit | [int64](#int64) |  | Limit on the number of polls in one request |
| cursor | [string](#string) |  | cursor for getting next batch of polls |






<a name="code.justin.tv.commerce.poliwag.GetPollsResponse"></a>

### GetPollsResponse
Response from GetPolls API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| polls | [Poll](#code.justin.tv.commerce.poliwag.Poll) | repeated |  |
| cursor | [string](#string) |  |  |






<a name="code.justin.tv.commerce.poliwag.GetViewablePollRequest"></a>

### GetViewablePollRequest
Request to GetViewablePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | Optional: userID of user making the request |
| owned_by | [string](#string) |  | User who owns the poll. |






<a name="code.justin.tv.commerce.poliwag.GetViewablePollResponse"></a>

### GetViewablePollResponse
Response from GetViewablePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll | [Poll](#code.justin.tv.commerce.poliwag.Poll) |  |  |






<a name="code.justin.tv.commerce.poliwag.GetVoterRequest"></a>

### GetVoterRequest
Request to GetVoter API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of the user performing the get request |
| poll_id | [string](#string) |  | ID of the poll |
| voter_user_id | [string](#string) |  | UserID of the voter to get information for. |






<a name="code.justin.tv.commerce.poliwag.GetVoterResponse"></a>

### GetVoterResponse
Response to GetVoter API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| voter | [Voter](#code.justin.tv.commerce.poliwag.Voter) |  |  |






<a name="code.justin.tv.commerce.poliwag.GetVotersByChoiceRequest"></a>

### GetVotersByChoiceRequest
Request to GetVotersByChoice API


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of the user performing the get request |
| poll_id | [string](#string) |  | ID of the poll |
| choice_id | [string](#string) |  | ID of the choice |
| sort | [VoterSort](#code.justin.tv.commerce.poliwag.VoterSort) |  | Sort of voters. Default: VOTES |
| direction | [Direction](#code.justin.tv.commerce.poliwag.Direction) |  | Direction of sort. Default: DESC (currently only DESC is supported) |
| limit | [int64](#int64) |  | Number of voters to request for. Default: 10 Min: 1, Max: 10 |
| cursor | [string](#string) |  | Cursor for getting next batch of results. (currently not supported) |






<a name="code.justin.tv.commerce.poliwag.GetVotersByChoiceResponse"></a>

### GetVotersByChoiceResponse
Response from GetVotersByChoice API


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| voters | [Voter](#code.justin.tv.commerce.poliwag.Voter) | repeated |  |
| cursor | [string](#string) |  |  |






<a name="code.justin.tv.commerce.poliwag.GetVotersRequest"></a>

### GetVotersRequest
Request to GetVoters API


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of the user performing the get request |
| poll_id | [string](#string) |  | ID of the poll |
| sort | [VoterSort](#code.justin.tv.commerce.poliwag.VoterSort) |  | Sort of voters. Default: VOTES |
| direction | [Direction](#code.justin.tv.commerce.poliwag.Direction) |  | Direction of sort. Default: DESC (currently only DESC is supported) |
| limit | [int64](#int64) |  | Number of voters to request for. Default: 10 Min: 1, Max: 10 |
| cursor | [string](#string) |  | Cursor for getting next batch of results. (currently not supported) |






<a name="code.justin.tv.commerce.poliwag.GetVotersResponse"></a>

### GetVotersResponse
Response from GetVoters API


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| voters | [Voter](#code.justin.tv.commerce.poliwag.Voter) | repeated |  |
| cursor | [string](#string) |  |  |






<a name="code.justin.tv.commerce.poliwag.Poll"></a>

### Poll
Information about a poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll_id | [string](#string) |  |  |
| owned_by | [string](#string) |  | User who owns the poll. |
| created_by | [string](#string) |  | User who created the poll. Consider that polls can be created by moderators on behalf of a broadcaster. In such a scenario: owned_by: broadcaster_id created_by: moderator_id |
| title | [string](#string) |  |  |
| start_time | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |
| end_time | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |
| ended_by | [string](#string) |  | User who ended the poll. Blank if poll completed by virtue of running its entire duration. |
| duration | [google.protobuf.Duration](#google.protobuf.Duration) |  |  |
| settings | [PollSettings](#code.justin.tv.commerce.poliwag.PollSettings) |  |  |
| status | [PollStatus](#code.justin.tv.commerce.poliwag.PollStatus) |  |  |
| choices | [Choice](#code.justin.tv.commerce.poliwag.Choice) | repeated | Information about the choices. |
| votes | [Votes](#code.justin.tv.commerce.poliwag.Votes) |  | A breakdown of how many votes were cast in the poll. |
| tokens | [Tokens](#code.justin.tv.commerce.poliwag.Tokens) |  | A breakdown of how many tokens were spent in the poll. |
| total_voters | [int64](#int64) |  | Total number of unique voters in the poll. |
| is_viewable | [bool](#bool) |  | Whether the poll is publicly viewable by users. |
| remaining_duration | [google.protobuf.Duration](#google.protobuf.Duration) |  | Duration remaining before poll completes. Due to how protobuf serializes durations, this value will be nil when the duration is 0 Please account for this in your client code with a nil check, as any poll that is ended will have remaining_duration == 0 |
| top_contributor | [TopContributor](#code.justin.tv.commerce.poliwag.TopContributor) |  | Top Bits Contributor for the poll. Blank if poll is not ended or no bits were spent. Deprecated: use TopBitsContributor instead. |
| top_bits_contributor | [TopBitsContributor](#code.justin.tv.commerce.poliwag.TopBitsContributor) |  | Top Bits Contributor for the poll. Blank if poll is not ended or no bits were spent. |
| top_channel_points_contributor | [TopChannelPointsContributor](#code.justin.tv.commerce.poliwag.TopChannelPointsContributor) |  | Top channel points contributor for the poll. Blank if the poll is not ended or no channel points were spent. |






<a name="code.justin.tv.commerce.poliwag.PollSettings"></a>

### PollSettings
Controls settings on the poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| multi_choice | [PollSettings.MultiChoicePollSetting](#code.justin.tv.commerce.poliwag.PollSettings.MultiChoicePollSetting) |  |  |
| subscriber_only | [PollSettings.SettingsSubscriberOnlyPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.SettingsSubscriberOnlyPollSetting) |  |  |
| subscriber_multiplier | [PollSettings.SubscriberMultiplierPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.SubscriberMultiplierPollSetting) |  |  |
| bits_votes | [PollSettings.BitsVotesPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.BitsVotesPollSetting) |  |  |
| channel_points_votes | [PollSettings.ChannelPointsVotesPollSetting](#code.justin.tv.commerce.poliwag.PollSettings.ChannelPointsVotesPollSetting) |  |  |






<a name="code.justin.tv.commerce.poliwag.PollSettings.BitsVotesPollSetting"></a>

### PollSettings.BitsVotesPollSetting
Controls whether users can purchase bits for votes.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enabled | [bool](#bool) |  |  |
| cost | [int64](#int64) |  | Cost of bits per vote. |






<a name="code.justin.tv.commerce.poliwag.PollSettings.ChannelPointsVotesPollSetting"></a>

### PollSettings.ChannelPointsVotesPollSetting
Controls whether users can use Channel Points for votes.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enabled | [bool](#bool) |  |  |
| cost | [int64](#int64) |  | Cost of channel points per vote. |






<a name="code.justin.tv.commerce.poliwag.PollSettings.MultiChoicePollSetting"></a>

### PollSettings.MultiChoicePollSetting
Controls whether users can vote for multiple choices.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enabled | [bool](#bool) |  |  |






<a name="code.justin.tv.commerce.poliwag.PollSettings.SettingsSubscriberOnlyPollSetting"></a>

### PollSettings.SettingsSubscriberOnlyPollSetting
Controls whether only subscribers can vote in the poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enabled | [bool](#bool) |  |  |






<a name="code.justin.tv.commerce.poliwag.PollSettings.SubscriberMultiplierPollSetting"></a>

### PollSettings.SubscriberMultiplierPollSetting
Controls whether subscribers received a 2x vote multiplier.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enabled | [bool](#bool) |  |  |






<a name="code.justin.tv.commerce.poliwag.TerminatePollRequest"></a>

### TerminatePollRequest
Request to TerminatePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of user performing the termination |
| poll_id | [string](#string) |  | ID of the poll to terminate |






<a name="code.justin.tv.commerce.poliwag.TerminatePollResponse"></a>

### TerminatePollResponse
Response from TerminatePoll API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll | [Poll](#code.justin.tv.commerce.poliwag.Poll) |  |  |






<a name="code.justin.tv.commerce.poliwag.Tokens"></a>

### Tokens
Breakdown of tokens.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| bits | [int64](#int64) |  | Total number of bits spent. |
| channel_points | [int64](#int64) |  | Total number of channel points spent. |






<a name="code.justin.tv.commerce.poliwag.TopBitsContributor"></a>

### TopBitsContributor
Top Bits Contributor for a poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of the top contributor. |
| bits_contributed | [int64](#int64) |  | Amount of bits contributed. |
| display_name | [string](#string) |  | Users&#39;s displayName |






<a name="code.justin.tv.commerce.poliwag.TopChannelPointsContributor"></a>

### TopChannelPointsContributor
Top Channel Points Contributor for a poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of the top contributor. |
| channel_points_contributed | [int64](#int64) |  | Amount of channel points contributed. |
| display_name | [string](#string) |  | Users&#39;s displayName |






<a name="code.justin.tv.commerce.poliwag.TopContributor"></a>

### TopContributor
Top Bits Contributor for a poll.
Deprecated: use TopBitsContributor instead


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | UserID of the top contributor. |
| bits_contributed | [int64](#int64) |  | Amount of bits contributed. |
| display_name | [string](#string) |  | Users&#39;s displayName |






<a name="code.justin.tv.commerce.poliwag.VoteRequest"></a>

### VoteRequest
Request to Vote API


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| vote_id | [string](#string) |  | Unique ID representing this vote |
| poll_id | [string](#string) |  | ID of the poll |
| user_id | [string](#string) |  | UserID of the user performing the vote request |
| choice_id | [string](#string) |  | ID of the choice |
| tokens | [Tokens](#code.justin.tv.commerce.poliwag.Tokens) |  | Tokens used as part of the vote |






<a name="code.justin.tv.commerce.poliwag.VoteResponse"></a>

### VoteResponse
Response from Vote API


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| voter | [Voter](#code.justin.tv.commerce.poliwag.Voter) |  |  |






<a name="code.justin.tv.commerce.poliwag.Voter"></a>

### Voter
A user who has voted in a poll.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| poll_id | [string](#string) |  | ID of poll voter voted in. |
| user_id | [string](#string) |  | UserID of voter |
| choices | [VoterChoice](#code.justin.tv.commerce.poliwag.VoterChoice) | repeated | Choices the voter voted for |
| votes | [Votes](#code.justin.tv.commerce.poliwag.Votes) |  | A breakdown of votes this voter voted in this poll. |
| tokens | [Tokens](#code.justin.tv.commerce.poliwag.Tokens) |  | A breakdown of tokens this voter used in this poll. |
| is_subscriber | [bool](#bool) |  | Whether the voter is a subscriber at the time of voting. |






<a name="code.justin.tv.commerce.poliwag.VoterChoice"></a>

### VoterChoice
Vote information about a voter for a choice.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| choice_id | [string](#string) |  | ID of choice voter voted for. |
| tokens | [Tokens](#code.justin.tv.commerce.poliwag.Tokens) |  | A breakdown of tokens this voter voted for this choice. |
| votes | [Votes](#code.justin.tv.commerce.poliwag.Votes) |  | A breakdown of votes this voter used for this choice. |






<a name="code.justin.tv.commerce.poliwag.Votes"></a>

### Votes
Breakdown of votes.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total | [int64](#int64) |  | Total number of votes. |
| bits | [int64](#int64) |  | Total number of bit votes. |
| channel_points | [int64](#int64) |  | Total number of channel point votes. |
| base | [int64](#int64) |  | Total number of base (or &#34;free&#34;) votes. |





 


<a name="code.justin.tv.commerce.poliwag.Direction"></a>

### Direction
Direction which specifies the traversal of the index.

| Name | Number | Description |
| ---- | ------ | ----------- |
| DESC | 0 | Descending |
| ASC | 1 | Ascending |



<a name="code.justin.tv.commerce.poliwag.GetPollsSort"></a>

### GetPollsSort
Sorting used when returning polls.

| Name | Number | Description |
| ---- | ------ | ----------- |
| START_TIME | 0 | Start times |



<a name="code.justin.tv.commerce.poliwag.PollCreationErrorCode"></a>

### PollCreationErrorCode
Errors that can occur when creating a poll.

| Name | Number | Description |
| ---- | ------ | ----------- |
| AUTOMOD_FAILED | 0 | Automod has failed for a choice or title. |
| POLL_ALREADY_ACTIVE | 1 | User attempted to create a poll when on is already active. |
| CHANNEL_NOT_BITS_ENABLED | 2 | User attempted to make a bits poll on a channel that does not support bits. |
| CHANNEL_NOT_CHANNEL_POINTS_ENABLED | 3 | User attempted to make a channel points poll on a channel that does not support channel points. |



<a name="code.justin.tv.commerce.poliwag.PollStatus"></a>

### PollStatus
Status of a poll, describing its lifecycle state.

| Name | Number | Description |
| ---- | ------ | ----------- |
| INVALID | 0 | Something went wrong determining the state :monkaS:. |
| ACTIVE | 1 | Poll is ACTIVE and users can vote. |
| COMPLETED | 2 | Poll ran its entire duration and &#34;naturally&#34; completed. Users cannot vote, but results are publicly visible. |
| TERMINATED | 3 | Poll was manually terminated by owner before it ran its entire duration. Users cannot vote, but results are publicly visible. |
| ARCHIVED | 4 | Poll is no longer publicly visible to users. |
| MODERATED | 5 | Poll is no longer visible to any user on Twitch. |



<a name="code.justin.tv.commerce.poliwag.VoteErrorCode"></a>

### VoteErrorCode
Errors that can occur when voting.

| Name | Number | Description |
| ---- | ------ | ----------- |
| POLL_NOT_FOUND | 0 | User tried to vote in a poll that doesn&#39;t exist i.e. there is no poll running at all). |
| POLL_NOT_ACTIVE | 1 | User tried to vote in a poll that is not active. |
| VOTE_ID_CONFLICT | 2 | Request has a vote ID that is in progress or has already been submitted. |
| MULTI_CHOICE_VOTE_FORBIDDEN | 3 | User is trying to vote for another choice in a poll that only allows one choice. |
| INVALID_CHOICE_ID | 4 | Request is for a choice ID that invalid. |
| INVALID_BITS_AMOUNT | 5 | Request is for an invalid bits amount (e.g. -100 bits, or request is not for a multiple of the Bits cost). |
| INSUFFICIENT_BITS_BALANCE | 6 | User&#39;s bits balance is too low to perform request (e.g. they only have 10 bits and they request to spend 100). |
| TOKENS_REQUIRED | 7 | User has already used their base votes, so in order to continue voting, they must use tokens (bits or channel points). |
| SUBSCRIBER_REQUIRED | 8 | User must be a subscriber to vote in this poll. |
| SELF_BITS_VOTE_NOT_ALLOWED | 9 | User cannot vote in their own poll using bits. |
| INSUFFICIENT_CHANNEL_POINTS_BALANCE | 10 | User&#39;s channel points balance is too low to perform request (e.g. they only have 10 channel points and they request to spend 100). |
| INVALID_CHANNEL_POINTS_AMOUNT | 11 | Request is for an invalid channel points amount (e.g. -100 channel points, or request is not for a multiple of the channel points cost). |
| SELF_CHANNEL_POINTS_VOTE_NOT_ALLOWED | 12 | User cannot vote in their own poll using channel points. |
| RATE_LIMITED | 13 | The user cannot vote because they have exceeded a rate limit. |



<a name="code.justin.tv.commerce.poliwag.VoterSort"></a>

### VoterSort
Sorting used when returning voters.

| Name | Number | Description |
| ---- | ------ | ----------- |
| VOTES | 0 | Sort by number of votes cast |
| BITS | 1 | Sort by number of bits used |
| CHANNEL_POINTS | 2 | Sort by number of channel points used |


 

 


<a name="code.justin.tv.commerce.poliwag.PoliwagAPI"></a>

### PoliwagAPI
The Twitch Polls Service.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| CreatePoll | [CreatePollRequest](#code.justin.tv.commerce.poliwag.CreatePollRequest) | [CreatePollResponse](#code.justin.tv.commerce.poliwag.CreatePollResponse) | Creates a Poll with status ACTIVE. |
| GetPoll | [GetPollRequest](#code.justin.tv.commerce.poliwag.GetPollRequest) | [GetPollResponse](#code.justin.tv.commerce.poliwag.GetPollResponse) | Gets a Poll with the given ID. |
| GetViewablePoll | [GetViewablePollRequest](#code.justin.tv.commerce.poliwag.GetViewablePollRequest) | [GetViewablePollResponse](#code.justin.tv.commerce.poliwag.GetViewablePollResponse) | Gets a viewable Poll for the given ownedBy. |
| TerminatePoll | [TerminatePollRequest](#code.justin.tv.commerce.poliwag.TerminatePollRequest) | [TerminatePollResponse](#code.justin.tv.commerce.poliwag.TerminatePollResponse) | Terminates a Poll placing it into status TERMINATED. |
| GetPolls | [GetPollsRequest](#code.justin.tv.commerce.poliwag.GetPollsRequest) | [GetPollsResponse](#code.justin.tv.commerce.poliwag.GetPollsResponse) | Gets all Polls with the given owner IDs and status. |
| ArchivePoll | [ArchivePollRequest](#code.justin.tv.commerce.poliwag.ArchivePollRequest) | [ArchivePollResponse](#code.justin.tv.commerce.poliwag.ArchivePollResponse) | Archives a Poll placing it into status ARCHIVED. |
| GetVoter | [GetVoterRequest](#code.justin.tv.commerce.poliwag.GetVoterRequest) | [GetVoterResponse](#code.justin.tv.commerce.poliwag.GetVoterResponse) | Gets a Voter in a poll with the given userID. |
| GetVoters | [GetVotersRequest](#code.justin.tv.commerce.poliwag.GetVotersRequest) | [GetVotersResponse](#code.justin.tv.commerce.poliwag.GetVotersResponse) | Gets all Voters for the given poll ID. |
| GetVotersByChoice | [GetVotersByChoiceRequest](#code.justin.tv.commerce.poliwag.GetVotersByChoiceRequest) | [GetVotersByChoiceResponse](#code.justin.tv.commerce.poliwag.GetVotersByChoiceResponse) | Gets all Voters for a given choice in a Poll. |
| Vote | [VoteRequest](#code.justin.tv.commerce.poliwag.VoteRequest) | [VoteResponse](#code.justin.tv.commerce.poliwag.VoteResponse) | Vote on a particular choice in a poll. |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

