package mocks

// Pantheon Client
//go:generate mockery -name=Pantheon -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/commerce/pantheon/rpc/pantheonrpc -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/pantheon/rpc/pantheonrpc -outpkg=pantheonrpc_mock

// SNS Client
//go:generate mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/commerce/gogogadget/aws/sns -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/gogogadget/aws/sns -outpkg=sns_mock

// Pubsub Client
//go:generate mockery -name=PubClient -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/chat/pubsub-go-pubclient/client -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/pubsub-go-pubclient/client -outpkg=pubsub_mock

// Eventbus Client
//go:generate mockery -name=Publisher -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/eventbus/client -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/eventbus/client -outpkg=eventbus_mock

// Get Poll Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll -outpkg=get_poll_mock

// Query Choice Aggregations Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations -outpkg=query_choice_aggregations_mock

// Query Poll Aggregation Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_poll_aggregation -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_poll_aggregation -outpkg=query_poll_aggregation_mock

// Update Is Aggregations Equal Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal -outpkg=update_is_aggregations_equal_mock

// Save Choice Aggregations Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_choice_aggregations -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_choice_aggregations -outpkg=save_choice_aggregations_mock

// Save Poll Aggregation Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_poll_aggregation -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_poll_aggregation -outpkg=save_poll_aggregation_mock

// Send Poll Update Pubsub Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/send_poll_update_pubsub -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/send_poll_update_pubsub -outpkg=send_poll_update_pubsub_mock

// Publish Poll Update Eventbus Step
//go:generate mockery -name=Step -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/publish_poll_update_eventbus -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/publish_poll_update_eventbus -outpkg=publish_poll_update_eventbus_mock

// Datascience Tracker
//go:generate mockery -name=Tracker -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/clients/datascience -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/clients/datasceince -outpkg=datascience_mock

// Viewable Poll Cache
//go:generate mockery -name=ViewablePollCache -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/cache/viewable_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/viewable_poll -outpkg=viewable_poll_cache_mock

// Vote Rate Limiter Cache
//go:generate mockery -name=VoteRateLimiter -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/cache/vote_rate_limiter -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/vote_rate_limiter -outpkg=vote_rate_limiter_mock

// Polls DAO
//go:generate mockery -name=PollsDAO -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/dao/polls -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls -outpkg=polls_dao_mock

// Choices DAO
//go:generate mockery -name=ChoicesDAO -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/dao/choices -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices -outpkg=choices_dao_mock

// Voters DAO
//go:generate mockery -name=VotersDAO -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/dao/voters -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/voters -outpkg=voters_dao_mock

// Votes DAO
//go:generate mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/dao/votes -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/votes -outpkg=votes_dao_mock

// Rediczar Client
//go:generate mockery -name=ThickClient -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/chat/rediczar -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/rediczar -outpkg=rediczar_mock

// Payday Client
//go:generate mockery -name=Payday -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/commerce/payday/rpc/payday -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/payday/rpc/payday -outpkg=paydayrpc_mock

// Zuma Client
//go:generate mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/chat/zuma/client -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/zuma/client -outpkg=zuma_client_mock

// TMI Client
//go:generate mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/chat/tmi/client -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/tmi/client -outpkg=tmi_client_mock

// Hallpass Client
//go:generate mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/cb/hallpass/client/hallpass -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/cb/hallpass/client/hallpass -outpkg=hallpass_client_mock

// Ripley Client
//go:generate mockery -name=Ripley -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/revenue/ripley/rpc -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/revenue/ripley/rpc -outpkg=ripley_client_mock

// StepFn Client
//go:generate mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/clients/stepfn -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/clients/stepfn -outpkg=stepfn_mock

// Subscriptions Service Client
//go:generate mockery -name=Subscriptions -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/revenue/subscriptions/twirp -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/revenue/subscriptions/twirp -outpkg=substwirp_mock

// Copo Client
//go:generate mockery -name=Copo -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/chat/copo/proto/copo -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/copo/proto/copo -outpkg=copo_mock

// Users Service Client
//go:generate mockery -name=InternalClient -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/web/users-service/client/usersclient_internal -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/web/users-service/client/usersclient_internal -outpkg=users_client_mock

// Create Poll Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll -outpkg=create_poll_mock

// Get Poll Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll -outpkg=get_poll_mock

// Get Viewable Poll Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/get_viewable_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_viewable_poll -outpkg=get_viewable_poll_mock

// Terminate Poll Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/terminate_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/terminate_poll -outpkg=terminate_poll_mock

// Moderate Poll Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/moderate_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/moderate_poll -outpkg=moderate_poll_mock

// Get Polls Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls -outpkg=get_polls_mock

// Archive Poll Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/archive_poll -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/archive_poll -outpkg=archive_poll_mock

// Get Voter Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter -outpkg=get_voter_mock

// Get Voters Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters -outpkg=get_voters_mock

// Get Voters By Choice Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters_by_choice -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters_by_choice -outpkg=get_voters_by_choice_mock

// Vote Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/poll/vote -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/vote -outpkg=vote_mock

// Pubsub Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/pubsub -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/pubsub -outpkg=pubsub_mock

// Eventbus Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/eventbus -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/eventbus -outpkg=eventbus_mock

// Slack Controller
//go:generate mockery -name=Controller -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/controller/slack -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/slack -outpkg=slack_mock

// Poll Editor Authorizer
//go:generate mockery -name=Authorizer -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor -outpkg=poll_editor_mock

// Channel Editor Authorizer
//go:generate mockery -name=Authorizer -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/authorization/channel_editor -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/channel_editor -outpkg=channel_editor_mock

// Poll Viewer Authorizer
//go:generate mockery -name=Authorizer -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/authorization/poll_viewer -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_viewer -outpkg=poll_viewer_mock

// Poll Voter Authorizer
//go:generate mockery -name=Authorizer -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/authorization/poll_voter -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_voter -outpkg=poll_voter_mock

// Voter Viewer Authorizer
//go:generate mockery -name=Authorizer -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/authorization/voter_viewer -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/voter_viewer -outpkg=voter_viewer_mock

// Poll Input Moderator
//go:generate mockery -name=Moderator -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/moderation/input_automod -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/moderation/input_automod -outpkg=input_automod_mock

// Poll Update V1 Executor
//go:generate mockery -name=Executor -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_update_v1 -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_update_v1 -outpkg=poll_update_v1_mock

// Poll Complete V1 Executor
//go:generate mockery -name=Executor -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_complete_v1 -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_complete_v1 -outpkg=poll_complete_v1_mock

// Poll Terminate V1 Executor
//go:generate mockery -name=Executor -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_terminate_v1 -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_terminate_v1 -outpkg=poll_terminate_v1_mock

// Poll Archive V1 Executor
//go:generate mockery -name=Executor -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_archive_v1 -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_archive_v1 -outpkg=poll_archive_v1_mock

// Poll Moderate V1 Executor
//go:generate mockery -name=Executor -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_moderate_v1 -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_moderate_v1 -outpkg=poll_moderate_v1_mock

// PDMS Client
//go:generate mockery -name=PDMSService -dir=$GOPATH/src/code.justin.tv/commerce/poliwag/vendor/code.justin.tv/amzn/PDMSLambdaTwirp -output=$GOPATH/src/code.justin.tv/commerce/poliwag/mocks/code.justin.tv/amzn/PDMSLambdaTwirp -outpkg=pdms_mock
