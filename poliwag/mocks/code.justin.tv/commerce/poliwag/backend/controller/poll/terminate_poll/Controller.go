// Code generated by mockery v1.0.0. DO NOT EDIT.

package terminate_poll_mock

import (
	context "context"

	models "code.justin.tv/commerce/poliwag/backend/models"
	mock "github.com/stretchr/testify/mock"
)

// Controller is an autogenerated mock type for the Controller type
type Controller struct {
	mock.Mock
}

// TerminatePoll provides a mock function with given fields: ctx, userID, pollID
func (_m *Controller) TerminatePoll(ctx context.Context, userID string, pollID string) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error) {
	ret := _m.Called(ctx, userID, pollID)

	var r0 models.Poll
	if rf, ok := ret.Get(0).(func(context.Context, string, string) models.Poll); ok {
		r0 = rf(ctx, userID, pollID)
	} else {
		r0 = ret.Get(0).(models.Poll)
	}

	var r1 []models.Choice
	if rf, ok := ret.Get(1).(func(context.Context, string, string) []models.Choice); ok {
		r1 = rf(ctx, userID, pollID)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).([]models.Choice)
		}
	}

	var r2 *models.TopBitsContributor
	if rf, ok := ret.Get(2).(func(context.Context, string, string) *models.TopBitsContributor); ok {
		r2 = rf(ctx, userID, pollID)
	} else {
		if ret.Get(2) != nil {
			r2 = ret.Get(2).(*models.TopBitsContributor)
		}
	}

	var r3 *models.TopChannelPointsContributor
	if rf, ok := ret.Get(3).(func(context.Context, string, string) *models.TopChannelPointsContributor); ok {
		r3 = rf(ctx, userID, pollID)
	} else {
		if ret.Get(3) != nil {
			r3 = ret.Get(3).(*models.TopChannelPointsContributor)
		}
	}

	var r4 error
	if rf, ok := ret.Get(4).(func(context.Context, string, string) error); ok {
		r4 = rf(ctx, userID, pollID)
	} else {
		r4 = ret.Error(4)
	}

	return r0, r1, r2, r3, r4
}
