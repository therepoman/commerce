// Code generated by mockery v1.0.0. DO NOT EDIT.

package poll_voter_mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// Authorizer is an autogenerated mock type for the Authorizer type
type Authorizer struct {
	mock.Mock
}

// Authorize provides a mock function with given fields: ctx, userID, pollID
func (_m *Authorizer) Authorize(ctx context.Context, userID string, pollID string) error {
	ret := _m.Called(ctx, userID, pollID)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string) error); ok {
		r0 = rf(ctx, userID, pollID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
