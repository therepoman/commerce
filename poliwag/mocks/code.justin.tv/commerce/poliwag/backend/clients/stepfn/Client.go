// Code generated by mockery v1.0.0. DO NOT EDIT.

package stepfn_mock

import (
	context "context"

	sfn "github.com/aws/aws-sdk-go/service/sfn"
	mock "github.com/stretchr/testify/mock"
)

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// CountActiveExecutions provides a mock function with given fields: ctx, stateMachineARN
func (_m *Client) CountActiveExecutions(ctx context.Context, stateMachineARN string) (int, error) {
	ret := _m.Called(ctx, stateMachineARN)

	var r0 int
	if rf, ok := ret.Get(0).(func(context.Context, string) int); ok {
		r0 = rf(ctx, stateMachineARN)
	} else {
		r0 = ret.Get(0).(int)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, stateMachineARN)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Execute provides a mock function with given fields: ctx, stateMachineARN, executionName, executionInput
func (_m *Client) Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error {
	ret := _m.Called(ctx, stateMachineARN, executionName, executionInput)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, interface{}) error); ok {
		r0 = rf(ctx, stateMachineARN, executionName, executionInput)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetActivityTask provides a mock function with given fields: activityARN, workerName
func (_m *Client) GetActivityTask(activityARN string, workerName string) (*sfn.GetActivityTaskOutput, error) {
	ret := _m.Called(activityARN, workerName)

	var r0 *sfn.GetActivityTaskOutput
	if rf, ok := ret.Get(0).(func(string, string) *sfn.GetActivityTaskOutput); ok {
		r0 = rf(activityARN, workerName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*sfn.GetActivityTaskOutput)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(activityARN, workerName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SendTaskFailure provides a mock function with given fields: taskToken, taskErr
func (_m *Client) SendTaskFailure(taskToken string, taskErr error) {
	_m.Called(taskToken, taskErr)
}

// SendTaskHeartbeat provides a mock function with given fields: taskToken
func (_m *Client) SendTaskHeartbeat(taskToken string) {
	_m.Called(taskToken)
}

// SendTaskSuccess provides a mock function with given fields: taskToken, output
func (_m *Client) SendTaskSuccess(taskToken string, output string) {
	_m.Called(taskToken, output)
}
