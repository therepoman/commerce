// Code generated by mockery v1.0.0. DO NOT EDIT.

package vote_rate_limiter_mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// VoteRateLimiter is an autogenerated mock type for the VoteRateLimiter type
type VoteRateLimiter struct {
	mock.Mock
}

// CanVote provides a mock function with given fields: ctx, userID, pollID
func (_m *VoteRateLimiter) CanVote(ctx context.Context, userID string, pollID string) (bool, error) {
	ret := _m.Called(ctx, userID, pollID)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, string, string) bool); ok {
		r0 = rf(ctx, userID, pollID)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, userID, pollID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
