// Code generated by mockery v1.0.0. DO NOT EDIT.

package votes_dao_mock

import (
	context "context"

	votes "code.justin.tv/commerce/poliwag/backend/dao/votes"
	mock "github.com/stretchr/testify/mock"
)

// DAO is an autogenerated mock type for the DAO type
type DAO struct {
	mock.Mock
}

// CreateVote provides a mock function with given fields: ctx, vote
func (_m *DAO) CreateVote(ctx context.Context, vote votes.Vote) error {
	ret := _m.Called(ctx, vote)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, votes.Vote) error); ok {
		r0 = rf(ctx, vote)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteVote provides a mock function with given fields: ctx, pollID, voteID
func (_m *DAO) DeleteVote(ctx context.Context, pollID string, voteID string) error {
	ret := _m.Called(ctx, pollID, voteID)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string) error); ok {
		r0 = rf(ctx, pollID, voteID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteVotesForUsers provides a mock function with given fields: ctx, userIDs
func (_m *DAO) DeleteVotesForUsers(ctx context.Context, userIDs []string) error {
	ret := _m.Called(ctx, userIDs)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []string) error); ok {
		r0 = rf(ctx, userIDs)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetChoiceAggregations provides a mock function with given fields: ctx, pollID
func (_m *DAO) GetChoiceAggregations(ctx context.Context, pollID string) ([]votes.ChoiceAggregation, error) {
	ret := _m.Called(ctx, pollID)

	var r0 []votes.ChoiceAggregation
	if rf, ok := ret.Get(0).(func(context.Context, string) []votes.ChoiceAggregation); ok {
		r0 = rf(ctx, pollID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]votes.ChoiceAggregation)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, pollID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPollAggregation provides a mock function with given fields: ctx, pollID
func (_m *DAO) GetPollAggregation(ctx context.Context, pollID string) (votes.PollAggregation, error) {
	ret := _m.Called(ctx, pollID)

	var r0 votes.PollAggregation
	if rf, ok := ret.Get(0).(func(context.Context, string) votes.PollAggregation); ok {
		r0 = rf(ctx, pollID)
	} else {
		r0 = ret.Get(0).(votes.PollAggregation)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, pollID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetVote provides a mock function with given fields: ctx, pollID, voteID, includeDeleted
func (_m *DAO) GetVote(ctx context.Context, pollID string, voteID string, includeDeleted bool) (*votes.Vote, error) {
	ret := _m.Called(ctx, pollID, voteID, includeDeleted)

	var r0 *votes.Vote
	if rf, ok := ret.Get(0).(func(context.Context, string, string, bool) *votes.Vote); ok {
		r0 = rf(ctx, pollID, voteID, includeDeleted)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*votes.Vote)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, bool) error); ok {
		r1 = rf(ctx, pollID, voteID, includeDeleted)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
