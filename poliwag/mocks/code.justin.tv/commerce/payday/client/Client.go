// Code generated by mockery v1.0.0. DO NOT EDIT.

package payday_mock

import api "code.justin.tv/commerce/payday/models/api"
import context "context"
import mock "github.com/stretchr/testify/mock"

import twitchclient "code.justin.tv/foundation/twitchclient"

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// AddBits provides a mock function with given fields: ctx, request, reqOpts
func (_m *Client) AddBits(ctx context.Context, request *api.AddBitsRequest, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, request, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *api.AddBitsRequest, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, request, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ClaimTournamentUserRewards provides a mock function with given fields: ctx, tournament, request, reqOpts
func (_m *Client) ClaimTournamentUserRewards(ctx context.Context, tournament string, request api.ClaimRewardRequest, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, tournament, request, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, api.ClaimRewardRequest, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, tournament, request, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// EntitleBits provides a mock function with given fields: ctx, request, reqOpts
func (_m *Client) EntitleBits(ctx context.Context, request *api.EntitleBitsRequest, reqOpts *twitchclient.ReqOpts) (*api.EntitleBitsResponse, error) {
	ret := _m.Called(ctx, request, reqOpts)

	var r0 *api.EntitleBitsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *api.EntitleBitsRequest, *twitchclient.ReqOpts) *api.EntitleBitsResponse); ok {
		r0 = rf(ctx, request, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.EntitleBitsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *api.EntitleBitsRequest, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, request, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetActionAsset provides a mock function with given fields: ctx, options, reqOpts
func (_m *Client) GetActionAsset(ctx context.Context, options api.GetActionAssetOptions, reqOpts *twitchclient.ReqOpts) (*api.GetActionAssetResponse, error) {
	ret := _m.Called(ctx, options, reqOpts)

	var r0 *api.GetActionAssetResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetActionAssetOptions, *twitchclient.ReqOpts) *api.GetActionAssetResponse); ok {
		r0 = rf(ctx, options, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetActionAssetResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetActionAssetOptions, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, options, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetActions provides a mock function with given fields: ctx, request, reqOpts
func (_m *Client) GetActions(ctx context.Context, request api.GetActionsRequest, reqOpts *twitchclient.ReqOpts) (*api.GetActionsResponse, error) {
	ret := _m.Called(ctx, request, reqOpts)

	var r0 *api.GetActionsResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetActionsRequest, *twitchclient.ReqOpts) *api.GetActionsResponse); ok {
		r0 = rf(ctx, request, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetActionsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetActionsRequest, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, request, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetActionsInfo provides a mock function with given fields: ctx, channelID, reqOpts
func (_m *Client) GetActionsInfo(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetActionsInfoResponse, error) {
	ret := _m.Called(ctx, channelID, reqOpts)

	var r0 *api.GetActionsInfoResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetActionsInfoResponse); ok {
		r0 = rf(ctx, channelID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetActionsInfoResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBadgeTiers provides a mock function with given fields: ctx, channelID, reqOpts
func (_m *Client) GetBadgeTiers(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	ret := _m.Called(ctx, channelID, reqOpts)

	var r0 *api.GetBadgesResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetBadgesResponse); ok {
		r0 = rf(ctx, channelID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetBadgesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBadgeTiersAdmin provides a mock function with given fields: ctx, channelID, reqOpts
func (_m *Client) GetBadgeTiersAdmin(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	ret := _m.Called(ctx, channelID, reqOpts)

	var r0 *api.GetBadgesResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetBadgesResponse); ok {
		r0 = rf(ctx, channelID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetBadgesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBalance provides a mock function with given fields: ctx, userId, channelId, reqOpts
func (_m *Client) GetBalance(ctx context.Context, userId string, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetBalanceResponse, error) {
	ret := _m.Called(ctx, userId, channelId, reqOpts)

	var r0 *api.GetBalanceResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, *twitchclient.ReqOpts) *api.GetBalanceResponse); ok {
		r0 = rf(ctx, userId, channelId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetBalanceResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userId, channelId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBanned provides a mock function with given fields: ctx, userId, reqOpts
func (_m *Client) GetBanned(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetBannedResponse, error) {
	ret := _m.Called(ctx, userId, reqOpts)

	var r0 *api.GetBannedResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetBannedResponse); ok {
		r0 = rf(ctx, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetBannedResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBitsProducts provides a mock function with given fields: ctx, userId, country, locale, platform, reqOpts
func (_m *Client) GetBitsProducts(ctx context.Context, userId string, country string, locale string, platform string, reqOpts *twitchclient.ReqOpts) ([]api.Product, error) {
	ret := _m.Called(ctx, userId, country, locale, platform, reqOpts)

	var r0 []api.Product
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, string, *twitchclient.ReqOpts) []api.Product); ok {
		r0 = rf(ctx, userId, country, locale, platform, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]api.Product)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userId, country, locale, platform, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBitsUsageLeaderboard provides a mock function with given fields: ctx, channelId, options, reqOpts
func (_m *Client) GetBitsUsageLeaderboard(ctx context.Context, channelId string, options api.GetLeaderboardOptions, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	ret := _m.Called(ctx, channelId, options, reqOpts)

	var r0 *api.GetLeaderboardResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, api.GetLeaderboardOptions, *twitchclient.ReqOpts) *api.GetLeaderboardResponse); ok {
		r0 = rf(ctx, channelId, options, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetLeaderboardResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, api.GetLeaderboardOptions, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelId, options, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetChannelImages provides a mock function with given fields: ctx, channelId, reqOpts
func (_m *Client) GetChannelImages(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetChannelImagesResponse, error) {
	ret := _m.Called(ctx, channelId, reqOpts)

	var r0 *api.GetChannelImagesResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetChannelImagesResponse); ok {
		r0 = rf(ctx, channelId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetChannelImagesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetChannelInfo provides a mock function with given fields: ctx, channelId, reqOpts
func (_m *Client) GetChannelInfo(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.ChannelEligibleResponse, error) {
	ret := _m.Called(ctx, channelId, reqOpts)

	var r0 *api.ChannelEligibleResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.ChannelEligibleResponse); ok {
		r0 = rf(ctx, channelId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.ChannelEligibleResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetChannelSettings provides a mock function with given fields: ctx, channelID, reqOpts
func (_m *Client) GetChannelSettings(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetSettingsResponse, error) {
	ret := _m.Called(ctx, channelID, reqOpts)

	var r0 *api.GetSettingsResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetSettingsResponse); ok {
		r0 = rf(ctx, channelID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetSettingsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCustomCheermotes provides a mock function with given fields: ctx, channelId, reqOpts
func (_m *Client) GetCustomCheermotes(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetCustomCheermotesResponse, error) {
	ret := _m.Called(ctx, channelId, reqOpts)

	var r0 *api.GetCustomCheermotesResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetCustomCheermotesResponse); ok {
		r0 = rf(ctx, channelId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetCustomCheermotesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetOptOut provides a mock function with given fields: ctx, userId, reqOpts
func (_m *Client) GetOptOut(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetOptOutResponse, error) {
	ret := _m.Called(ctx, userId, reqOpts)

	var r0 *api.GetOptOutResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetOptOutResponse); ok {
		r0 = rf(ctx, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetOptOutResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPrices provides a mock function with given fields: ctx, request, reqOpts
func (_m *Client) GetPrices(ctx context.Context, request api.GetPricesRequest, reqOpts *twitchclient.ReqOpts) (*api.GetPricesResponse, error) {
	ret := _m.Called(ctx, request, reqOpts)

	var r0 *api.GetPricesResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetPricesRequest, *twitchclient.ReqOpts) *api.GetPricesResponse); ok {
		r0 = rf(ctx, request, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetPricesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetPricesRequest, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, request, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournament provides a mock function with given fields: ctx, userId, reqOpts
func (_m *Client) GetTournament(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.Tournament, error) {
	ret := _m.Called(ctx, userId, reqOpts)

	var r0 *api.Tournament
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.Tournament); ok {
		r0 = rf(ctx, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.Tournament)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournamentGlobalProgress provides a mock function with given fields: ctx, tournament, userId, reqOpts
func (_m *Client) GetTournamentGlobalProgress(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetProgressResponse, error) {
	ret := _m.Called(ctx, tournament, userId, reqOpts)

	var r0 *api.GetProgressResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, *twitchclient.ReqOpts) *api.GetProgressResponse); ok {
		r0 = rf(ctx, tournament, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetProgressResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, tournament, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournamentTeamFanLeaderboard provides a mock function with given fields: ctx, tournament, teamId, userId, reqOpts
func (_m *Client) GetTournamentTeamFanLeaderboard(ctx context.Context, tournament string, teamId string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	ret := _m.Called(ctx, tournament, teamId, userId, reqOpts)

	var r0 *api.GetLeaderboardResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, *twitchclient.ReqOpts) *api.GetLeaderboardResponse); ok {
		r0 = rf(ctx, tournament, teamId, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetLeaderboardResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, tournament, teamId, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournamentTeamLeaderboard provides a mock function with given fields: ctx, tournament, teamId, reqOpts
func (_m *Client) GetTournamentTeamLeaderboard(ctx context.Context, tournament string, teamId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	ret := _m.Called(ctx, tournament, teamId, reqOpts)

	var r0 *api.GetLeaderboardResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, *twitchclient.ReqOpts) *api.GetLeaderboardResponse); ok {
		r0 = rf(ctx, tournament, teamId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetLeaderboardResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, tournament, teamId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournamentTeamProgress provides a mock function with given fields: ctx, tournament, teamId, userId, reqOpts
func (_m *Client) GetTournamentTeamProgress(ctx context.Context, tournament string, teamId string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetProgressResponse, error) {
	ret := _m.Called(ctx, tournament, teamId, userId, reqOpts)

	var r0 *api.GetProgressResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, *twitchclient.ReqOpts) *api.GetProgressResponse); ok {
		r0 = rf(ctx, tournament, teamId, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetProgressResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, tournament, teamId, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournamentUserLeaderboard provides a mock function with given fields: ctx, tournament, userId, reqOpts
func (_m *Client) GetTournamentUserLeaderboard(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	ret := _m.Called(ctx, tournament, userId, reqOpts)

	var r0 *api.GetLeaderboardResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, *twitchclient.ReqOpts) *api.GetLeaderboardResponse); ok {
		r0 = rf(ctx, tournament, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetLeaderboardResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, tournament, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTournamentUserRewards provides a mock function with given fields: ctx, tournament, userId, reqOpts
func (_m *Client) GetTournamentUserRewards(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetTournamentUserRewardsResponse, error) {
	ret := _m.Called(ctx, tournament, userId, reqOpts)

	var r0 *api.GetTournamentUserRewardsResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, *twitchclient.ReqOpts) *api.GetTournamentUserRewardsResponse); ok {
		r0 = rf(ctx, tournament, userId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetTournamentUserRewardsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, tournament, userId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserEvents provides a mock function with given fields: ctx, userId, cursor, limit, reqOpts
func (_m *Client) GetUserEvents(ctx context.Context, userId string, cursor string, limit int64, reqOpts *twitchclient.ReqOpts) (*api.UserEventsResponse, error) {
	ret := _m.Called(ctx, userId, cursor, limit, reqOpts)

	var r0 *api.UserEventsResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, int64, *twitchclient.ReqOpts) *api.UserEventsResponse); ok {
		r0 = rf(ctx, userId, cursor, limit, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.UserEventsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, int64, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userId, cursor, limit, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserInfo provides a mock function with given fields: ctx, userID, reqOpts
func (_m *Client) GetUserInfo(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*api.GetUserInfoResponse, error) {
	ret := _m.Called(ctx, userID, reqOpts)

	var r0 *api.GetUserInfoResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetUserInfoResponse); ok {
		r0 = rf(ctx, userID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetUserInfoResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, userID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWithholdingByAccountId provides a mock function with given fields: ctx, accountId, reqOpts
func (_m *Client) GetWithholdingByAccountId(ctx context.Context, accountId string, reqOpts *twitchclient.ReqOpts) (*api.GetWithholdingByAccountIdResponse, error) {
	ret := _m.Called(ctx, accountId, reqOpts)

	var r0 *api.GetWithholdingByAccountIdResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetWithholdingByAccountIdResponse); ok {
		r0 = rf(ctx, accountId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetWithholdingByAccountIdResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, accountId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWithholdingByPayoutEntityId provides a mock function with given fields: ctx, payoutEntityId, reqOpts
func (_m *Client) GetWithholdingByPayoutEntityId(ctx context.Context, payoutEntityId string, reqOpts *twitchclient.ReqOpts) (*api.GetWithholdingByPayoutEntityIdResponse, error) {
	ret := _m.Called(ctx, payoutEntityId, reqOpts)

	var r0 *api.GetWithholdingByPayoutEntityIdResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *api.GetWithholdingByPayoutEntityIdResponse); ok {
		r0 = rf(ctx, payoutEntityId, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetWithholdingByPayoutEntityIdResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, payoutEntityId, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ModerateCheermote provides a mock function with given fields: ctx, userID, opts
func (_m *Client) ModerateCheermote(ctx context.Context, userID string, opts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, userID, opts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, userID, opts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RemoveBits provides a mock function with given fields: ctx, request, reqOpts
func (_m *Client) RemoveBits(ctx context.Context, request *api.RemoveBitsRequest, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, request, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *api.RemoveBitsRequest, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, request, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// SetBadgeTiers provides a mock function with given fields: ctx, channelID, params, reqOpts
func (_m *Client) SetBadgeTiers(ctx context.Context, channelID string, params api.SetBadgesRequest, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	ret := _m.Called(ctx, channelID, params, reqOpts)

	var r0 *api.GetBadgesResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, api.SetBadgesRequest, *twitchclient.ReqOpts) *api.GetBadgesResponse); ok {
		r0 = rf(ctx, channelID, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetBadgesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, api.SetBadgesRequest, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetBadgeTiersAdmin provides a mock function with given fields: ctx, channelID, params, reqOpts
func (_m *Client) SetBadgeTiersAdmin(ctx context.Context, channelID string, params api.SetBadgeTiersAdminRequest, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	ret := _m.Called(ctx, channelID, params, reqOpts)

	var r0 *api.GetBadgesResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, api.SetBadgeTiersAdminRequest, *twitchclient.ReqOpts) *api.GetBadgesResponse); ok {
		r0 = rf(ctx, channelID, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.GetBadgesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, api.SetBadgeTiersAdminRequest, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetBanned provides a mock function with given fields: ctx, userId, request, reqOpts
func (_m *Client) SetBanned(ctx context.Context, userId string, request *api.SetBannedRequest, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, userId, request, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *api.SetBannedRequest, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, userId, request, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// SetOptOut provides a mock function with given fields: ctx, userId, request, reqOpts
func (_m *Client) SetOptOut(ctx context.Context, userId string, request *api.SetOptOutRequest, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, userId, request, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *api.SetOptOutRequest, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, userId, request, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UpdateChannelSettings provides a mock function with given fields: ctx, channelID, params, reqOpts
func (_m *Client) UpdateChannelSettings(ctx context.Context, channelID string, params api.UpdateSettingsRequest, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, channelID, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, api.UpdateSettingsRequest, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, channelID, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadCustomCheermotes provides a mock function with given fields: ctx, params, reqOpts
func (_m *Client) UploadCustomCheermotes(ctx context.Context, params *api.UploadCustomCheermoteRequest, reqOpts *twitchclient.ReqOpts) (*api.UploadCustomCheermoteResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 *api.UploadCustomCheermoteResponse
	if rf, ok := ret.Get(0).(func(context.Context, *api.UploadCustomCheermoteRequest, *twitchclient.ReqOpts) *api.UploadCustomCheermoteResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.UploadCustomCheermoteResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *api.UploadCustomCheermoteRequest, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
