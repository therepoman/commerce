package errors

import (
	"encoding/json"

	"github.com/twitchtv/twirp"
)

const (
	poliwagErrorMetaKey = "poliwag_error"
)

// NewPoliwagError returns a new ClientError that wraps a twirp.Error and stores
// an error code enum.
func NewPoliwagError(twErr twirp.Error, poliwagError PoliwagError) twirp.Error {
	b, err := json.Marshal(&poliwagError)
	if err != nil {
		return twErr
	}

	return twErr.WithMeta(poliwagErrorMetaKey, string(b))
}

// ParseClientError takes an error, and if it is a PoliwagError returns the PoliwagError.
func ParsePoliwagError(err error) (*PoliwagError, error) {
	twErr, ok := err.(twirp.Error)
	if !ok {
		return nil, err
	}

	clientErr := twErr.Meta(poliwagErrorMetaKey)
	if clientErr == "" {
		return nil, err
	}

	var poliwagError PoliwagError
	jsonErr := json.Unmarshal([]byte(clientErr), &poliwagError)

	return &poliwagError, jsonErr
}
