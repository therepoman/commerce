package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws/endpoints"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/config"
)

const (
	POLLS_TABLE_NAME_FORMAT = "polls-%s"
)

var affirmativePhrases = map[string]interface{}{
	"y":           nil,
	"yes":         nil,
	"yep":         nil,
	"ok":          nil,
	"k":           nil,
	"sure thing":  nil,
	"sounds good": nil,
	"why not":     nil,
}

func main() {
	inputReader := bufio.NewReader(os.Stdin)

	fmt.Println("Hello and welcome to the active poll deleter tool!")
	fmt.Println("This tool takes a list of poll_ids and deletes them if they are marked active")
	fmt.Println("Just sit back, relax, and I'll guide you through this")

	waitForOk(inputReader)

	fmt.Println("Great!")
	envInput := getResponse(inputReader, "Now what environment are we dealing with here? Staging? Prod?")

	cfg, err := config.LoadConfig(config.Environment(envInput))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("error loading config")
	}

	awsConfig := aws.NewConfig().WithRegion(cfg.AWSRegion).WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint)
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		logrus.WithError(err).Panic("error creating AWS session")
	}

	dynamoDBClient := dynamodb.New(sess)

	csvFilename := getResponse(inputReader, "What is the CSV filename with the list of poll_ids?")
	csvFile, err := os.Open(csvFilename)
	if err != nil {
		logrus.WithField("csvFilename", csvFilename).WithError(err).Panic("error opening csv file")
	}

	csvReader := csv.NewReader(bufio.NewReader(csvFile))

	fmt.Printf("Ready to delete all ACTIVE polls from %s!\n", csvFilename)
	waitForOk(inputReader)

	pollsTableName := fmt.Sprintf(POLLS_TABLE_NAME_FORMAT, envInput)
	numDeletedActivePolls := 0
	for {
		line, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			logrus.WithError(err).Panic("error reading line from csv")
		}

		pollID := line[0]

		getItemOutput, err := dynamoDBClient.GetItem(&dynamodb.GetItemInput{
			ConsistentRead: aws.Bool(true),
			Key:            getDynamoHashKey(pollID),
			TableName:      aws.String(pollsTableName),
		})
		if err != nil {
			logrus.WithField("pollID", pollID).WithError(err).Error("error getting poll from dynamo")
			continue
		}

		if getItemOutput.Item == nil {
			logrus.WithField("pollID", pollID).WithError(err).Error("no poll found")
			continue
		}

		var poll models.Poll
		err = dynamodbattribute.UnmarshalMap(getItemOutput.Item, &poll)
		if err != nil {
			logrus.WithField("pollID", pollID).WithError(err).Error("error marshalling poll")
			continue
		}

		if poll.Status == models.PollStatusActive {
			_, err = dynamoDBClient.DeleteItem(&dynamodb.DeleteItemInput{
				Key:       getDynamoHashKey(pollID),
				TableName: aws.String(pollsTableName),
			})
			if err != nil {
				logrus.WithField("pollID", pollID).WithError(err).Error("error deleting poll in dynamo")
				continue
			}

			logrus.WithField("pollID", pollID).Info("successfully deleted ACTIVE poll")
			numDeletedActivePolls++
		} else {
			logrus.WithField("pollID", pollID).Warn("poll is not currently ACTIVE")
		}
	}

	fmt.Printf("Complete! %d active polls deleted!\n", numDeletedActivePolls)
}

func getDynamoHashKey(pollID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		"poll_id": {S: aws.String(pollID)},
	}
}

func waitForOk(reader *bufio.Reader) {
	fmt.Println("\nOk?")

	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	text = strings.TrimSpace(strings.ToLower(text))
	_, ok := affirmativePhrases[text]
	if !ok {
		panic("No problem, let's stop")
	}

	fmt.Println()
}

func getResponse(reader *bufio.Reader, prompt string) string {
	fmt.Println(prompt)

	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	fmt.Println()

	text = strings.TrimSpace(strings.ToLower(text))
	return text
}
