package main

import (
	"fmt"
	"os"

	"code.justin.tv/commerce/poliwag/clients/sandstorm"
	"code.justin.tv/commerce/poliwag/config"
)

func main() {
	if len(os.Args) < 2 {
		panic("missing secret key as arg")
	}

	secretKey := os.Args[1]

	cfg, err := config.LoadConfig(config.Staging)
	if err != nil {
		panic(err)
	}

	if cfg == nil {
		panic("could not load config")
	}

	sandstormClient, err := sandstorm.New(cfg)
	if err != nil {
		panic(err)
	}

	secret, err := sandstormClient.Get(secretKey)
	if err != nil {
		panic(err)
	}

	if secret == nil {
		panic("nil secret returned by sandstorm")
	}

	fmt.Println(string(secret.Plaintext))
}
