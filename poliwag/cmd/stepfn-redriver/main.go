package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws/endpoints"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"

	"code.justin.tv/commerce/poliwag/config"
)

var affirmativePhrases = map[string]interface{}{
	"y":           nil,
	"yes":         nil,
	"yep":         nil,
	"ok":          nil,
	"k":           nil,
	"sure thing":  nil,
	"sounds good": nil,
	"why not":     nil,
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Hello and welcome to the step function redriver tool!")
	fmt.Println("Just sit back, relax, and I'll guide you through this")

	waitForOk(reader)

	fmt.Println("Great!")
	envInput := getResponse(reader, "Now what environment are we dealing with here? Staging? Prod?")

	cfg, err := config.LoadConfig(config.Environment(envInput))
	if err != nil {
		panic(err)
	}

	sess, err := session.NewSession()
	if err != nil {
		panic(err)
	}

	sfnClient := sfn.New(sess, aws.NewConfig().WithRegion(cfg.AWSRegion).WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint))

	fmt.Println("Sounds good!")
	minInput := getResponse(reader, "How far back should I go looking for failures? Give me the number of minutes please!")

	min, err := strconv.Atoi(minInput)
	if err != nil {
		panic(err)
	}

	lookForFailuresAfter := time.Now().Add(time.Minute * time.Duration(-min))

	fmt.Printf("So I'm going to look for failures after %s \n", lookForFailuresAfter.Format(time.RFC850))

	fmt.Println("Next I'm going to start checking for failed step function executions by calling AWS")
	waitForOk(reader)

	pollCompleteExecs := getExecutions(sfnClient, cfg.StepFn.PollCompleteStepFn.StateMachineARN, lookForFailuresAfter)
	pollTerminateExecs := getExecutions(sfnClient, cfg.StepFn.PollTerminateStepFn.StateMachineARN, lookForFailuresAfter)
	pollModerateExecs := getExecutions(sfnClient, cfg.StepFn.PollModerateStepFn.StateMachineARN, lookForFailuresAfter)
	pollArchiveExecs := getExecutions(sfnClient, cfg.StepFn.PollArchiveStepFn.StateMachineARN, lookForFailuresAfter)

	printAndRedriveExecutions(reader, sfnClient, pollCompleteExecs, "poll_complete", cfg.StepFn.PollCompleteStepFn.StateMachineARN)
	printAndRedriveExecutions(reader, sfnClient, pollTerminateExecs, "poll_terminate", cfg.StepFn.PollTerminateStepFn.StateMachineARN)
	printAndRedriveExecutions(reader, sfnClient, pollModerateExecs, "poll_moderate", cfg.StepFn.PollModerateStepFn.StateMachineARN)
	printAndRedriveExecutions(reader, sfnClient, pollArchiveExecs, "poll_archive", cfg.StepFn.PollArchiveStepFn.StateMachineARN)
}

func getExecutions(sfnClient *sfn.SFN, stateMachineARN string, lookForFailuresAfter time.Time) []*sfn.ExecutionListItem {
	execs := make([]*sfn.ExecutionListItem, 0)
	resp, err := sfnClient.ListExecutions(&sfn.ListExecutionsInput{
		MaxResults:      aws.Int64(1000),
		StateMachineArn: aws.String(stateMachineARN),
		StatusFilter:    aws.String("FAILED"),
	})
	if err != nil {
		panic(err)
	}

	for _, exec := range resp.Executions {
		if exec.StartDate.After(lookForFailuresAfter) {
			execs = append(execs, exec)
		}
	}
	return execs
}

func printAndRedriveExecutions(reader *bufio.Reader, sfnClient *sfn.SFN, execs []*sfn.ExecutionListItem, sfnType string, stateMachineARN string) {
	if len(execs) > 0 {
		fmt.Printf("Found %d failures for the %s step functions \n", len(execs), sfnType)
		fmt.Println("I'm going to list out each execution")
		waitForOk(reader)

		fmt.Println("Failed Executions:")
		for i, exec := range execs {
			fmt.Printf("#%d : %s : %s : %s \n", i+1, *exec.Name, exec.StartDate.Format(time.RFC850), *exec.Status)
		}
	} else {
		fmt.Printf("No failures for the %s step function \n", sfnType)
		return
	}

	fmt.Println()
	fmt.Println("Now I'm going to redrive them")
	waitForOk(reader)

	for i, exec := range execs {
		gehResp, err := sfnClient.GetExecutionHistory(&sfn.GetExecutionHistoryInput{
			ExecutionArn: exec.ExecutionArn,
			MaxResults:   aws.Int64(1),
		})
		if err != nil {
			panic(err)
		}

		input := gehResp.Events[0].ExecutionStartedEventDetails.Input

		name := fmt.Sprintf("%s.%s", *exec.Name, "redrive")
		_, err = sfnClient.StartExecution(&sfn.StartExecutionInput{
			Input:           input,
			Name:            aws.String(name),
			StateMachineArn: aws.String(stateMachineARN),
		})
		if err != nil {
			awsErr, ok := err.(awserr.Error)
			if !ok {
				panic(err)
			}
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				fmt.Printf("#%d: Has alredy been redriven %s \n", i+1, name)
			} else {
				panic(err)
			}
		} else {
			fmt.Printf("#%d : Redrove %s \n", i+1, name)
		}
	}

	fmt.Printf("Done redriving %s \n \n", sfnType)
}

func waitForOk(reader *bufio.Reader) {
	fmt.Println("\nOk?")

	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	text = strings.TrimSpace(strings.ToLower(text))
	_, ok := affirmativePhrases[text]
	if !ok {
		panic("No problem, let's stop")
	}

	fmt.Println()
}

func getResponse(reader *bufio.Reader, prompt string) string {
	fmt.Println(prompt)

	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	fmt.Println()

	text = strings.TrimSpace(strings.ToLower(text))
	return text
}
