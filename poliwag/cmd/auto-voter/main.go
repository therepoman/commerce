package main

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gofrs/uuid"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/poliwag/config"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const (
	channelID = "108707191" // seph
)

func main() {
	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(config.Staging)
	if err != nil {
		panic(err)
	}

	pc := poliwag.NewPoliwagAPIProtobufClient(cfg.Endpoints.Integration, http.DefaultClient)

	resp, err := pc.GetViewablePoll(context.Background(), &poliwag.GetViewablePollRequest{
		UserId:  channelID,
		OwnedBy: channelID,
	})
	if err != nil {
		panic(err)
	}

	choices := make([]string, 0)
	pollID := resp.Poll.PollId
	for _, choice := range resp.Poll.Choices {
		choices = append(choices, choice.ChoiceId)
	}

	voteCount := 0
	for {
		choiceIndex := random.Int(0, len(choices)-1)
		choiceID := choices[choiceIndex]
		vID, err := uuid.NewV4()
		if err != nil {
			panic(err)
		}

		_, err = pc.Vote(context.Background(), &poliwag.VoteRequest{
			VoteId:   vID.String(),
			PollId:   pollID,
			UserId:   random.NumberString(10),
			ChoiceId: choiceID,
		})
		if err != nil {
			panic(err)
		}
		time.Sleep(100 * time.Millisecond)

		voteCount++
		fmt.Printf("Vote: %d \n", voteCount)
	}

}
