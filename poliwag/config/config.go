package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/go-yaml/yaml"

	log "code.justin.tv/commerce/logrus"
)

const (
	LocalConfigFilePath  = "/src/code.justin.tv/commerce/poliwag/config/data/%s.yaml"
	GlobalConfigFilePath = "/etc/poliwag/config/%s.yaml"
	LambdaConfigFilePath = "/config/data/%s.yaml"

	LambdaRootPathEnvironmentVariable = "LAMBDA_TASK_ROOT"
	EnvironmentEnvironmentVariable    = "ENVIRONMENT"
	CanaryEnvironmentVariable         = "CANARY"

	Local   = Environment("local")
	Staging = Environment("staging")
	Prod    = Environment("prod")
	Default = Local
)

type QueueConfig struct {
	Name       string `yaml:"name"`
	URL        string `yaml:"url"`
	NumWorkers int    `yaml:"num-workers"`
}

type Queues struct {
	VotePantheonIngestionQueue QueueConfig `yaml:"vote-pantheon-ingestion-queue"`
	VotePollUpdateQueue        QueueConfig `yaml:"vote-poll-update-queue"`
}

type Endpoints struct {
	Integration   string `yaml:"integration"`
	Ripley        string `yaml:"ripley"`
	Zuma          string `yaml:"zuma"`
	Copo          string `yaml:"copo"`
	Hallpass      string `yaml:"hallpass"`
	Payday        string `yaml:"payday"`
	Pantheon      string `yaml:"pantheon"`
	Redis         string `yaml:"redis"`
	Pubsub        string `yaml:"pubsub"`
	Spade         string `yaml:"spade"`
	Subscriptions string `yaml:"subscriptions"`
	Users         string `yaml:"users"`
	TMI           string `yaml:"tmi"`
	DAX           string `yaml:"dax"`
	PDMS          string `yaml:"pdms"`
}

type PDMS struct {
	LambdaARN      string `yaml:"lambda-arn"`
	CallerRole     string `yaml:"caller-role"`
	DryRun         bool   `yaml:"dryrun"`
	ReportDeletion bool   `yaml:"report-deletion"`
}

type StepFnStateMachineConfig struct {
	StateMachineARN string `yaml:"state-machine-arn"`
}

type AuroraConfig struct {
	Username                string `yaml:"username"`
	PasswordSandstormSecret string `yaml:"password-sandstorm-secret"`
	Endpoint                string `yaml:"endpoint"`
	DBName                  string `yaml:"db-name"`
}

type SlackConfig struct {
	OauthTokenSandstormSecret string `yaml:"oauth-token-sandstorm-secret"`
}

type StepFnConfig struct {
	PollUpdateStepFn    StepFnStateMachineConfig `yaml:"poll-update-step-fn"`
	PollCompleteStepFn  StepFnStateMachineConfig `yaml:"poll-complete-step-fn"`
	PollTerminateStepFn StepFnStateMachineConfig `yaml:"poll-terminate-step-fn"`
	PollArchiveStepFn   StepFnStateMachineConfig `yaml:"poll-archive-step-fn"`
	PollModerateStepFn  StepFnStateMachineConfig `yaml:"poll-moderate-step-fn"`
	UserDeletionStepFn  StepFnStateMachineConfig `yaml:"user-deletion-step-fn"`
}

type Config struct {
	EnvironmentName          string       `yaml:"environment-name"`
	AWSRegion                string       `yaml:"aws-region"`
	DynamoSuffix             string       `yaml:"dynamo-suffix"`
	SandstormRole            string       `yaml:"sandstorm-role"`
	CloudWatchMetricsEnabled bool         `yaml:"cloudwatch-metrics-enabled"`
	Endpoints                Endpoints    `yaml:"endpoints"`
	Queues                   Queues       `yaml:"queues"`
	StepFn                   StepFnConfig `yaml:"step-fn"`
	Aurora                   AuroraConfig `yaml:"aurora"`
	Slack                    SlackConfig  `yaml:"slack"`
	PDMS                     PDMS         `yaml:"pdms"`
	EnableProfiler           bool         `yaml:"enable-profiler"`
	VoteSNSTopic             string       `yaml:"vote-sns-topic"`
	AuditLogGroupName        string       `yaml:"audit-log-group"`
	S2SServiceName           string       `yaml:"s2s-service-name"`
	S2SServiceOrigin         string       `yaml:"s2s-service-origin"`
	S2SEnabled               bool         `yaml:"s2s-enabled"`
	S2SDisablePassthrough    bool         `yaml:"s2s-disable-passthrough"`
	S2SAuthLogGroup          string       `yaml:"s2s-auth-log-group"`
}

type Environment string

var Environments = map[Environment]interface{}{Local: nil, Staging: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath)
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	if lambdaRootPath := os.Getenv(LambdaRootPathEnvironmentVariable); lambdaRootPath != "" {
		lambdaFName := path.Join(lambdaRootPath, fmt.Sprintf(LambdaConfigFilePath, baseFilename))
		_, err := os.Stat(lambdaFName)
		return lambdaFName, err
	}
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func IsCanary() bool {
	return strings.EqualFold(os.Getenv(CanaryEnvironmentVariable), "true")
}

func GetEnv() Environment {
	env := os.Getenv(EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !IsValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}
