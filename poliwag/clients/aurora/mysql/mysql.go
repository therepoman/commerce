package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"code.justin.tv/commerce/poliwag/clients/aurora"
	"code.justin.tv/commerce/poliwag/clients/sandstorm"
)

const (
	mysqlDriver = "mysql"
)

func connectionString(cfg aurora.Config, sandstormClient sandstorm.Client) (string, error) {
	passwordSecret, err := sandstormClient.Get(cfg.PasswordSandstormSecret)
	if err != nil {
		return "", err
	}

	if passwordSecret == nil {
		return "", errors.New("password not found in sandstorm")
	}

	return fmt.Sprintf("%s:%s@tcp(%s)/%s",
		cfg.Username,
		string(passwordSecret.Plaintext),
		cfg.Endpoint,
		cfg.DBName), nil
}

type client struct {
	db *sql.DB
}

type Client interface {
	Ping(ctx context.Context) error
}

func NewClient(cfg aurora.Config, sandstormClient sandstorm.Client) (Client, error) {
	connectionString, err := connectionString(cfg, sandstormClient)
	if err != nil {
		return nil, err
	}

	db, err := sql.Open(mysqlDriver, connectionString)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(50)

	// We don't want the DB to have to close any connections until the five minute window has gone by,
	// as this just increases the amount of work done under heavy load.
	db.SetMaxIdleConns(50)
	db.SetConnMaxIdleTime(5 * time.Minute)

	// Having connections open forever also seems to cause problems. See a long max time so there isn't frequent churn
	db.SetConnMaxLifetime(30 * time.Minute)

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &client{
		db: db,
	}, nil
}

func (c *client) Ping(ctx context.Context) error {
	return c.db.PingContext(ctx)
}
