package gorm

import (
	"context"
	"errors"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"

	"code.justin.tv/commerce/poliwag/clients/aurora"
	"code.justin.tv/commerce/poliwag/clients/sandstorm"
)

type client struct {
	db *gorm.DB
}

type Client interface {
	Shutdown(ctx context.Context) error
	AutoMigrate(ctx context.Context, values ...interface{}) error

	Create(ctx context.Context, value interface{}) error
	First(ctx context.Context, out interface{}, where ...interface{}) error
	FirstIncludingDeleted(ctx context.Context, out interface{}, where ...interface{}) error
	Delete(ctx context.Context, out interface{}, where ...interface{}) error

	DB() *gorm.DB
}

func connectionString(cfg aurora.Config, sandstormClient sandstorm.Client) (string, error) {
	passwordSecret, err := sandstormClient.Get(cfg.PasswordSandstormSecret)
	if err != nil {
		return "", err
	}

	if passwordSecret == nil {
		return "", errors.New("password not found in sandstorm")
	}

	return fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		cfg.Username,
		string(passwordSecret.Plaintext),
		cfg.Endpoint,
		cfg.DBName), nil
}

func NewClient(cfg aurora.Config, sandstormClient sandstorm.Client) (Client, error) {
	connectionString, err := connectionString(cfg, sandstormClient)
	if err != nil {
		return nil, err
	}

	db, err := gorm.Open("mysql", connectionString)
	if err != nil {
		return nil, err
	}

	db.DB().SetMaxOpenConns(50)

	// We don't want the DB to have to close any connections until the five minute window has gone by,
	// as this just increases the amount of work done under heavy load.
	db.DB().SetMaxIdleConns(50)
	db.DB().SetConnMaxIdleTime(5 * time.Minute)

	// Having connections open forever also seems to cause problems. See a long max time so there isn't frequent churn
	db.DB().SetConnMaxLifetime(30 * time.Minute)

	return &client{
		db: db,
	}, nil
}

func (c *client) Shutdown(ctx context.Context) error {
	return c.db.Close()
}

func (c *client) AutoMigrate(ctx context.Context, values ...interface{}) error {
	return c.db.AutoMigrate(values...).Error
}

func (c *client) Create(ctx context.Context, value interface{}) error {
	return c.db.Create(value).Error
}

func (c *client) First(ctx context.Context, out interface{}, where ...interface{}) error {
	return c.db.First(out, where...).Error
}

func (c *client) FirstIncludingDeleted(ctx context.Context, out interface{}, where ...interface{}) error {
	return c.db.Unscoped().First(out, where...).Error
}

func (c *client) Delete(ctx context.Context, out interface{}, where ...interface{}) error {
	return c.db.Delete(out, where...).Error
}

func (c *client) DB() *gorm.DB {
	return c.db
}
