package aurora

type Config struct {
	Username                string
	PasswordSandstormSecret string
	Endpoint                string
	DBName                  string
}
