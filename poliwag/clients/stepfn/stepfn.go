package stepfn

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/awshttptelemetry"
	"code.justin.tv/chat/telemetryext"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/config"
)

const (
	serviceName      = "stepfn"
	executionAPIName = "StartExecution"
)

type Client interface {
	Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error
	GetActivityTask(activityARN string, workerName string) (*sfn.GetActivityTaskOutput, error)
	SendTaskSuccess(taskToken string, output string)
	SendTaskHeartbeat(taskToken string)
	SendTaskFailure(taskToken string, taskErr error)
	CountActiveExecutions(ctx context.Context, stateMachineARN string) (int, error)
}

type client struct {
	InternalClient sfniface.SFNAPI
	Stats          statsd.Statter
	SampleReporter *telemetry.SampleReporter
}

func NewClient(config *config.Config, stats statsd.Statter, sampleReporter *telemetry.SampleReporter, httpClient *http.Client) Client {
	s, _ := session.NewSession()
	s = awshttptelemetry.AttachServiceAndOperation(s)

	sfnClient := sfn.New(s, aws.NewConfig().
		WithRegion(config.AWSRegion).
		WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint).
		WithHTTPClient(httpClient))

	return &client{
		InternalClient: sfnClient,
		Stats:          stats,
		SampleReporter: sampleReporter,
	}
}

func (c *client) Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error {
	defer func(startTime time.Time) {
		d := time.Since(startTime)

		metricErr := c.Stats.TimingDuration(fmt.Sprintf("service.%s.%s", serviceName, executionAPIName), d, 1.0)
		if metricErr != nil {
			logrus.WithError(metricErr).Error("error recording stepfunction execution metric")
		}

		reporter := telemetryext.ReporterWithDimensions(*c.SampleReporter, map[string]string{
			"Service":          serviceName,
			"ExecutionAPIName": executionAPIName,
		})
		reporter.ReportDurationSample("StepFunctionStartExecutionDuration", d)
	}(time.Now())

	inputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return errors.Wrapf(err, "failed to marshall state machine execution input. stateMachineARN: %s executionInput: %+v", stateMachineARN, executionInput)
	}

	startExecInput := &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputBytes)),
		Name:            &executionName,
		StateMachineArn: &stateMachineARN,
	}
	if _, err := c.InternalClient.StartExecutionWithContext(ctx, startExecInput); err != nil {
		return errors.Wrapf(err, "failed to start state machine execution. stateMachineARN: %s startExecInput: %+v", stateMachineARN, startExecInput)
	}
	return nil
}

func (c *client) CountActiveExecutions(ctx context.Context, stateMachineARN string) (int, error) {
	count := 0
	var nextToken *string = nil
	for {
		resp, err := c.InternalClient.ListExecutionsWithContext(ctx, &sfn.ListExecutionsInput{
			StateMachineArn: pointers.StringP(stateMachineARN),
			MaxResults:      pointers.Int64P(1000),
			StatusFilter:    aws.String("RUNNING"),
			NextToken:       nextToken,
		})
		if err != nil {
			return 0, err
		}
		count += len(resp.Executions)
		nextToken = resp.NextToken

		if nextToken == nil || count > 10000 {
			break
		}
	}
	return count, nil
}

func (c *client) GetActivityTask(activityARN string, workerName string) (*sfn.GetActivityTaskOutput, error) {
	return c.InternalClient.GetActivityTask(&sfn.GetActivityTaskInput{
		ActivityArn: &activityARN,
		WorkerName:  pointers.StringP(workerName),
	})
}

func (c *client) SendTaskSuccess(taskToken string, output string) {
	_, sendErr := c.InternalClient.SendTaskSuccess(&sfn.SendTaskSuccessInput{
		TaskToken: &taskToken,
		Output:    &output,
	})
	if sendErr != nil {
		logrus.WithFields(logrus.Fields{
			"taskToken": taskToken,
			"output":    output,
		}).WithError(sendErr).Error("failed to send task success to SFN")
	}
}

func (c *client) SendTaskHeartbeat(taskToken string) {
	_, sendErr := c.InternalClient.SendTaskHeartbeat(&sfn.SendTaskHeartbeatInput{
		TaskToken: &taskToken,
	})
	if sendErr != nil {
		logrus.WithField("taskToken", taskToken).WithError(sendErr).Error("failed to send task heartbeat to SFN")
	}
}

func (c *client) SendTaskFailure(taskToken string, taskErr error) {
	cause := fmt.Sprintf("task error: %+v", taskErr)
	_, sendErr := c.InternalClient.SendTaskFailure(&sfn.SendTaskFailureInput{
		Cause:     &cause,
		TaskToken: &taskToken,
	})
	if sendErr != nil {
		logrus.WithFields(logrus.Fields{
			"taskToken": taskToken,
			"cause":     cause,
		}).WithError(sendErr).Error("failed to send task failure to SFN")
	}
}
