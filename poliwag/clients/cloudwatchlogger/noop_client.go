package cloudwatchlogger

import "context"

type noop struct {
}

func NewCloudWatchLogNoopClient() CloudWatchLogger {
	return &noop{}
}

func (c *noop) Send(ctx context.Context, message string) error {
	return nil
}

func (c *noop) Shutdown() {}
