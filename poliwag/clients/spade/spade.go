package spade

import (
	"io"
	"time"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/config"
	"code.justin.tv/hygienic/distconf"
	hygienicspade "code.justin.tv/hygienic/spade"
	"code.justin.tv/hygienic/spade/spadedconf"
	spadetelemetrymetrics "code.justin.tv/hygienic/spade/spademetrics/telemetrymetrics"
)

const (
	timeout = 1 * time.Second
)

// Client is an interface of the exported methods of hygienicspade.Client
type Client interface {
	io.Closer
	Start() error

	// QueueEvents adds events to the internal queue.  Returns instantly if the queue is full.
	QueueEvents(events ...hygienicspade.Event)
}

func NewClient(cfg *config.Config, sampleReporter *telemetry.SampleReporter) (Client, error) {
	if !shouldEnableSpade(cfg) {
		log.Info("using noop spade client")
		return noop{}, nil
	}

	var dconf distconf.Distconf
	dconf.Str("spade.endpoint", cfg.Endpoints.Spade)
	dconf.Duration("spade.timeout", timeout)

	var spadeConfig spadedconf.Config
	if err := spadeConfig.Load(&dconf); err != nil {
		return nil, err
	}

	if spadeConfig.GetSpadeHost() == "" {
		log.Info("using noop spade client")
		return noop{}, nil
	}

	c := &hygienicspade.Client{
		Config:     &spadeConfig,
		HTTPClient: nil, // http.DefaultClient
		Logger:     loggerFunc(log.Info),
		Reporter: &spadetelemetrymetrics.Reporter{
			SampleReporter: sampleReporter,
		},
	}
	if err := c.Setup(); err != nil {
		return nil, err
	}

	return c, nil
}

func shouldEnableSpade(cfg *config.Config) bool {
	return cfg.EnvironmentName == string(config.Prod)
}

type noop struct{}

func (noop) Start() error                       { return nil }
func (noop) Close() error                       { return nil }
func (noop) QueueEvents(...hygienicspade.Event) {}

type loggerFunc func(...interface{})

func (f loggerFunc) Log(vals ...interface{}) {
	f(vals...)
}
