package lock

import (
	"context"
	"errors"
	"time"

	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v7"
)

const (
	defaultLockTimeout = 5 * time.Second
	defaultWaitTimeout = 50 * time.Millisecond
	defaultRetryLimit  = 3
)

type redisLockingClient struct {
	redisClient redis.UniversalClient
}

func NewRedisLockingClient(redisClient redis.UniversalClient) LockingClient {
	return &redisLockingClient{
		redisClient: redisClient,
	}
}

func (c redisLockingClient) ObtainLock(ctx context.Context, key string) (Lock, error) {
	rl, err := redislock.Obtain(c.redisClient, key, defaultLockTimeout, &redislock.Options{
		RetryStrategy: redislock.LimitRetry(redislock.LinearBackoff(defaultWaitTimeout), defaultRetryLimit),
		Context:       ctx,
	})
	if err != nil {
		return nil, err
	}

	if rl == nil {
		return nil, errors.New("failed to obtain redis lock")
	}

	return rl, nil
}
