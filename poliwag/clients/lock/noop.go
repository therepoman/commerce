package lock

import (
	"context"
)

type noopLockingClient struct {
}

func NewNoopLockingClient() LockingClient {
	return &noopLockingClient{}
}

func (c *noopLockingClient) ObtainLock(ctx context.Context, key string) (Lock, error) {
	return &noopLock{}, nil
}

type noopLock struct {
}

func (l *noopLock) Release() error {
	return nil
}
