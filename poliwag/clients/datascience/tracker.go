package datascience

import (
	"code.justin.tv/commerce/poliwag/clients/spade"
	hygienicspade "code.justin.tv/hygienic/spade"
)

type EventName string

const (
	CreatePoll = EventName("polls_server_create_poll")
	EndPoll    = EventName("polls_server_end_poll")
	DeletePoll = EventName("polls_server_delete_poll")
	Vote       = EventName("polls_server_vote")
)

type Tracker interface {
	TrackEventAsync(eventName EventName, eventProperties interface{})
}

type tracker struct {
	spadeClient spade.Client
}

func NewTracker(spadeClient spade.Client) Tracker {
	return &tracker{
		spadeClient: spadeClient,
	}
}

func (ds *tracker) TrackEventAsync(eventName EventName, eventProperties interface{}) {
	ds.spadeClient.QueueEvents(hygienicspade.Event{
		Name:       string(eventName),
		Properties: eventProperties,
	})
}
