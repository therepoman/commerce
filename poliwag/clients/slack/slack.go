package slack

import (
	"context"
	"time"

	"github.com/avast/retry-go"
	"github.com/slack-go/slack"

	"code.justin.tv/commerce/poliwag/clients/sandstorm"
)

type Client interface {
	PostMessageContext(ctx context.Context, channelID string, options ...slack.MsgOption) (string, string, error)
}

func NewClient(oauthTokenSecret string, sandstormClient sandstorm.Client) (Client, error) {
	if oauthTokenSecret == "" {
		return noopClient{}, nil
	}

	oathToken, err := sandstormClient.Get(oauthTokenSecret)
	if err != nil {
		return nil, err
	}

	return client{
		client: slack.New(string(oathToken.Plaintext)),
	}, nil
}

type client struct {
	client *slack.Client
}

func (c client) PostMessageContext(ctx context.Context, channelID string, options ...slack.MsgOption) (string, string, error) {
	var channel string
	var timestamp string

	err := retry.Do(func() error {
		respChannel, respTimestamp, respErr := c.client.PostMessageContext(ctx, channelID, options...)

		channel = respChannel
		timestamp = respTimestamp
		return respErr
	},
		retry.Context(ctx),
		retry.Attempts(3),
		retry.Delay(time.Second),
		retry.DelayType(retry.FixedDelay), // we want a fixed delay of 1 second, as that is what the Slack API recommends
	)

	return channel, timestamp, err
}

type noopClient struct {
}

func (n noopClient) PostMessageContext(ctx context.Context, channelID string, options ...slack.MsgOption) (string, string, error) {
	return "<NOOP_CHANNEL_ID", "<NOOP_TIMESTAMP>", nil
}
