package s2s2

import (
	logging "code.justin.tv/amzn/TwitchLogging"
	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	twitchtelemetry "code.justin.tv/amzn/TwitchTelemetry"
	twitchtelemetrymiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	"code.justin.tv/commerce/poliwag/config"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

func NewS2S2Client(cfg *config.Config, sampleReporter twitchtelemetry.SampleReporter, logger logging.Logger) (*s2s2.S2S2, error) {
	s2s2Client, err := s2s2.New(&s2s2.Options{
		Config: &c7s.Config{
			ClientServiceName:   cfg.S2SServiceName,
			ServiceOrigins:      cfg.S2SServiceOrigin,
			EnableAccessLogging: true,
		},
		OperationStarter: &operation.Starter{
			OpMonitors: []operation.OpMonitor{
				&twitchtelemetrymiddleware.OperationMonitor{
					SampleReporter: sampleReporter,
				},
			},
		},
		Logger: logger,
	})
	if err != nil {
		return nil, err
	}

	return s2s2Client, nil
}
