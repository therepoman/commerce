package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"runtime/pprof"
	"time"

	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	metricsmiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend"
	"code.justin.tv/commerce/poliwag/backend/audit"
	"code.justin.tv/commerce/poliwag/clients/cloudwatchlogger"
	s2s2client "code.justin.tv/commerce/poliwag/clients/s2s2"
	"code.justin.tv/commerce/poliwag/clients/spade"
	"code.justin.tv/commerce/poliwag/config"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
	"code.justin.tv/commerce/poliwag/proto/poliwag_internal"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/twirpmetric"
)

const (
	shutdownTimeout = 1 * time.Minute
	port            = 8000
)

var pprofFileName string

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Error("error loading config")
		os.Exit(1)
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	if cfg.EnvironmentName == string(config.Local) {
		// Force the timezone to UTC when running the server locally
		err := os.Setenv("TZ", "Etc/UTC")
		if err != nil {
			log.WithError(err).Error("error setting timezone to UTC")
		}
	}

	if cfg.EnableProfiler {
		pprofFileName = fmt.Sprintf("pprof-%d.pprof", time.Now().Unix())
		pprofFile, err := os.Create(pprofFileName)
		if err != nil {
			log.WithError(err).Error("error creating pprof file")
			os.Exit(1)
		}
		defer func() {
			err := pprofFile.Close()
			if err != nil {
				log.WithError(err).Error("error closing pprof file")
			}
		}()
		err = pprof.StartCPUProfile(pprofFile)
		if err != nil {
			log.WithError(err).Error("error starting cpu profile")
			os.Exit(1)
		}
	}

	log.Infof("Initializing backend...")
	sampleReporter, stop := backend.SetupTwitchTelemetry(cfg, false)
	defer stop()

	spadeClient, err := spade.NewClient(cfg, sampleReporter)
	if err != nil {
		log.WithError(err).Error("error creating spade client")
		os.Exit(1)
	}
	go func() {
		if err := spadeClient.Start(); err != nil {
			log.WithError(err).Error("starting spade client")
		}
	}()
	defer func() {
		if err := spadeClient.Close(); err != nil {
			log.WithError(err).Error("closing spade client")
		}
	}()

	var s2s2Client *s2s2.S2S2
	if cfg.S2SEnabled {
		s2s2logger, err := cloudwatchlogger.NewCloudWatchLogClient(cfg.AWSRegion, cfg.S2SAuthLogGroup)
		if err != nil {
			log.WithError(err).Error("error initializing s2s2 logger")
			os.Exit(1)
		}
		defer s2s2logger.Shutdown()
		s2s2Client, err = s2s2client.NewS2S2Client(cfg, *sampleReporter, cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, cfg.S2SAuthLogGroup))
		if err != nil {
			log.WithError(err).Error("error initializing s2s2 client")
			os.Exit(1)
		}
	}

	be, err := backend.NewBackend(cfg, false, sampleReporter, spadeClient, s2s2Client)
	if err != nil {
		log.WithError(err).Error("error initializing backend")
		os.Exit(1)
	}
	log.Infof("Backend successfully initialized")

	metricsMiddleware := &twirpmetric.Server{
		Starter: &operation.Starter{
			OpMonitors: []operation.OpMonitor{
				&metricsmiddleware.OperationMonitor{
					SampleReporter: *sampleReporter,
					AutoFlush:      false,
				},
			},
		},
	}
	twirpStatsHook := twirp.ChainHooks(
		splatter.NewStatsdServerHooks(be.Statter()),
		metricsMiddleware.ServerHooks(),
	)
	twirpHandler := poliwag.NewPoliwagAPIServer(be.TwirpServer(), twirpStatsHook)
	twirpInternalHandler := poliwag_internal.NewPoliwagInternalAPIServer(be.TwirpInternalServer(), twirpStatsHook)
	twirpAdminHandler := poliwag_admin.NewPoliwagAdminAPIServer(be.TwirpAdminServer(), twirpStatsHook)
	defer func() {
		err := be.Statter().Close()
		if err != nil {
			log.WithError(err).Error("could not close statter")
		}
	}()

	var voteHandler http.Handler
	if cfg.S2SEnabled {
		handler := s2s2Client.RequireAuthentication(twirpHandler)
		if cfg.S2SDisablePassthrough {
			voteHandler = handler
		} else {
			voteHandler = handler.RecordMetricsOnly()
		}
	} else {
		voteHandler = twirpHandler
	}

	log.Infof("Running aurora sql migrator...")
	err = be.Migrate(context.Background())
	if err != nil {
		log.WithError(err).Error("error running aurora sql migrator")
		os.Exit(1)
	}

	mux := goji.NewMux()

	mux.HandleFunc(pat.Get("/stopprofiler"), StopProfiler)
	mux.HandleFunc(pat.Get("/"), be.Ping)
	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(poliwag.PoliwagAPIPathPrefix+"Vote"), audit.NewMuxWrapper(voteHandler))
	mux.Handle(pat.Post(poliwag.PoliwagAPIPathPrefix+"*"), twirpHandler)
	mux.Handle(pat.Post(poliwag_internal.PoliwagInternalAPIPathPrefix+"*"), twirpInternalHandler)
	mux.Handle(pat.Post(poliwag_admin.PoliwagAdminAPIPathPrefix+"*"), twirpAdminHandler)

	log.Infof("Listening on port %d", port)

	goji_graceful.HandleSignals()
	err = goji_graceful.ListenAndServe(fmt.Sprintf(":%d", port), mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")

	shutdownStart := time.Now()
	shutdownContext, shutdownContextCancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer shutdownContextCancel()

	be.Shutdown(shutdownContext)

	goji_graceful.Wait()
	log.Infof("Finished shutting down in: %v", time.Since(shutdownStart))
}

func StopProfiler(w http.ResponseWriter, r *http.Request) {
	pprof.StopCPUProfile()
	pprofFile, err := os.Open(pprofFileName)
	if err != nil {
		w.WriteHeader(500)
		_, _ = w.Write([]byte("error"))
		return
	}
	pprofBytes, err := ioutil.ReadAll(pprofFile)
	if err != nil {
		w.WriteHeader(500)
		_, _ = w.Write([]byte("error"))
		return
	}
	_, _ = w.Write(pprofBytes)
	return
}
