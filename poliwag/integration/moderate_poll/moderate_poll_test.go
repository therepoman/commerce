// +build integration

package moderate_poll

import (
	"testing"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
	poliwag "code.justin.tv/commerce/poliwag/proto/poliwag"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestModeratePoll(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		admin := client.NewAdminPoliwagClient()
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the moderate poll API happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Moderate poll should succeed", func() {
					utils.ModeratePoll(admin, poll.PollId, nil)
					expectedStatus := poliwag.PollStatus_MODERATED
					utils.AdminWaitForPollStatus(admin, poll.PollId, &expectedStatus)
					actualPoll, err := utils.AdminGetPoll(admin, poll.PollId, nil)

					So(err, ShouldBeNil)
					So(actualPoll, ShouldNotBeNil)
					So(actualPoll.Status, ShouldEqual, poliwag.PollStatus_MODERATED)
				})
			})
		})

		Convey("Test moderate poll API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Moderating with a blank poll ID returns an invalid argument error", func() {
					utils.ModeratePoll(admin, "", TwirpErrorP(twirp.InvalidArgument))

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
					})
				})
			})
		})

		Convey("Test the moderate poll API's error handling of non-existent resources", func() {
			Convey("Moderating a non-existent poll ID returns a not found error", func() {
				utils.ModeratePoll(admin, "ceci n'est pas une poll", TwirpErrorP(twirp.NotFound))
			})
		})
	})
}
