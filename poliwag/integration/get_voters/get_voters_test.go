// +build integration

package get_voters_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestGetVoters(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get voters API happy path", func() {
			voter1 := utils.RandomUser()
			voter2 := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Vote in the test channel", func() {
					utils.Vote(p, nil, poll.PollId, voter1, poll.Choices[0].ChoiceId, nil)
					utils.Vote(p, nil, poll.PollId, voter2, poll.Choices[0].ChoiceId, nil)

					Convey("Get voters should succeed", func() {
						utils.GetVoters(p, poll.PollId, utils.QaBitsTestUser, nil, nil, nil, pointers.IntP(2))
					})
				})
			})
		})

		Convey("Test the get voters API with no voters", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get voters with should succeed", func() {
					utils.GetVoters(p, poll.PollId, utils.QaBitsTestUser, nil, nil, nil, pointers.IntP(0))
				})
			})
		})

		Convey("Test the get voters API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get voters with a blank poll ID returns an invalid argument error", func() {
					utils.GetVoters(p, "", utils.QaBitsTestUser, nil, nil, TwirpErrorP(twirp.InvalidArgument), nil)

					Convey("Get voters with a blank user ID returns an invalid argument error", func() {
						utils.TerminatePoll(p, poll.PollId, "", TwirpErrorP(twirp.InvalidArgument))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})
	})
}
