// +build integration

package create_poll

import (
	"context"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	proto_duration "github.com/golang/protobuf/ptypes/duration"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const regularUserID = "434516654"
const defaultCreatePollTimeout = 2 * time.Second

func TestCreatePoll(t *testing.T) {
	Convey("given a Poliwag client", t, func() {
		poliwagClient := client.NewPoliwagClient()

		userID := regularUserID
		ownedBy := regularUserID
		title := "[Integration Test] What should I play?"
		duration := ptypes.DurationProto(5 * time.Minute)

		choice0 := &poliwag.CreatePollRequest_CreatePollChoice{
			Title: "[Integration Test] Chrono Trigger",
		}
		choice1 := &poliwag.CreatePollRequest_CreatePollChoice{
			Title: "[Integration Test] Final Fantasy VII",
		}

		choices := []*poliwag.CreatePollRequest_CreatePollChoice{choice0, choice1}

		Convey("when the user field is blank", func() {
			blankUserID := ""

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   blankUserID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the ownedBy field is blank", func() {
			blankOwnedBy := ""

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  blankOwnedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the title field is blank", func() {
			blankTitle := ""

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    blankTitle,
					Duration: duration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the duration field is nil", func() {
			var nilDuration *proto_duration.Duration

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: nilDuration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the duration field is too short", func() {
			shortDuration := ptypes.DurationProto(1 * time.Second)

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: shortDuration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the duration field is too long", func() {
			longDuration := ptypes.DurationProto(100 * 24 * time.Hour)

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: longDuration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the choices field is blank", func() {
			var nilChoices []*poliwag.CreatePollRequest_CreatePollChoice

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  nilChoices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the bits votes poll settings field is enabled but has no bits cost", func() {
			noBitsCostSettings := &poliwag.PollSettings{
				BitsVotes: &poliwag.PollSettings_BitsVotesPollSetting{
					Enabled: true,
					Cost:    0,
				},
			}

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
					Settings: noBitsCostSettings,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the channel points votes poll settings field is enabled but has no channel points cost", func() {
			noChannelPointsCostSettings := &poliwag.PollSettings{
				ChannelPointsVotes: &poliwag.PollSettings_ChannelPointsVotesPollSetting{
					Enabled: true,
					Cost:    0,
				},
			}

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
					Settings: noChannelPointsCostSettings,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when title has a blocked word", func() {
			title := "shit!!!"

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when choice has a blocked word", func() {
			blockedWordChoice := &poliwag.CreatePollRequest_CreatePollChoice{
				Title: "shit",
			}

			blockedWordChoices := []*poliwag.CreatePollRequest_CreatePollChoice{blockedWordChoice}

			Convey("we should return an invalid argument error", func() {
				ctx, cancel := context.WithTimeout(context.Background(), defaultCreatePollTimeout)
				defer cancel()

				_, err := poliwagClient.CreatePoll(ctx, &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  blockedWordChoices,
				})

				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})
	})
}
