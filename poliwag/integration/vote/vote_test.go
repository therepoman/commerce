// +build integration

package vote

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestVote(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the vote API happy path", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("A user's base vote should succeed", func() {
					utils.Vote(p, nil, poll.PollId, voter, poll.Choices[0].ChoiceId, nil)

					Convey("Validate that democracy occurred", func() {
						utils.GetVoterAndAssertVote(p, poll.PollId, voter, poll.Choices[0].ChoiceId, 1)

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							expectedStatus := poliwag.PollStatus_ARCHIVED
							utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
						})
					})
				})
			})
		})

		Convey("Test the vote API's input validation", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Voting with a blank vote ID returns an invalid argument error", func() {
					utils.Vote(p, pointers.StringP(""), poll.PollId, voter, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.InvalidArgument))

					Convey("Voting on a blank poll ID returns an invalid argument error", func() {
						utils.Vote(p, nil, "", voter, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.InvalidArgument))

						Convey("Voting with a blank user ID returns an invalid argument error", func() {
							utils.Vote(p, nil, poll.PollId, "", poll.Choices[0].ChoiceId, TwirpErrorP(twirp.InvalidArgument))

							Convey("Voting on a blank choice ID returns an invalid argument error", func() {
								utils.Vote(p, nil, poll.PollId, voter, "", TwirpErrorP(twirp.InvalidArgument))

								Convey("Cleanup the poll", func() {
									utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
									expectedStatus := poliwag.PollStatus_ARCHIVED
									utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
								})

							})
						})
					})
				})
			})
		})

		Convey("Test the vote API's error handling of non-existent resources", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Voting on a non-existent poll ID returns a not found error", func() {
					utils.Vote(p, nil, "ceci n'est pas une poll", voter, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.NotFound))

					Convey("Voting on a non-existent choice ID returns a not found error", func() {
						utils.Vote(p, nil, poll.PollId, voter, "ceci n'est pas une choice", TwirpErrorP(twirp.NotFound))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							expectedStatus := poliwag.PollStatus_ARCHIVED
							utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
						})
					})
				})
			})

		})

		Convey("Test the vote API's error handling around polls that have ended", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("End the poll", func() {
					utils.TerminatePoll(p, poll.PollId, poll.OwnedBy, nil)
					expectedStatus := poliwag.PollStatus_TERMINATED
					utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)

					Convey("Votes on a poll that has ended are not allowed", func() {
						// pause momentarily to avoid staled read
						time.Sleep(50 * time.Microsecond)
						utils.Vote(p, nil, poll.PollId, voter, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.FailedPrecondition))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							expectedStatus := poliwag.PollStatus_ARCHIVED
							utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
						})
					})
				})
			})
		})

		Convey("Test that the vote API prevents a voter fraud where a voter votes multiple times", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("The first vote succeeds", func() {
					utils.Vote(p, nil, poll.PollId, voter, poll.Choices[0].ChoiceId, nil)

					Convey("Subsequent votes are illegal (except for the wealthy)", func() {
						for i := 0; i < 10; i++ {
							utils.Vote(p, nil, poll.PollId, voter, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.InvalidArgument))
						}

						Convey("Validate that only a single vote was counted", func() {
							utils.GetVoterAndAssertVote(p, poll.PollId, voter, poll.Choices[0].ChoiceId, 1)

							Convey("Cleanup the poll", func() {
								utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
								expectedStatus := poliwag.PollStatus_ARCHIVED
								utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
							})
						})
					})
				})
			})
		})

		Convey("Test that the vote API rejects the reuse of a vote id", func() {
			voter1 := utils.RandomUser()
			voter2 := utils.RandomUser()
			voteIDUUID, err := uuid.NewV4()
			So(err, ShouldBeNil)
			voteID := voteIDUUID.String()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("The first vote succeeds", func() {
					utils.Vote(p, pointers.StringP(voteID), poll.PollId, voter1, poll.Choices[0].ChoiceId, nil)

					Convey("A second user cannot reuse this voteID", func() {
						utils.Vote(p, pointers.StringP(voteID), poll.PollId, voter2, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.InvalidArgument))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							expectedStatus := poliwag.PollStatus_ARCHIVED
							utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
						})
					})
				})
			})
		})

		Convey("Test that the vote API only accepts a single vote when multiple votes are fired off in parallel", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Fire off several vote requests in parallel", func() {
					numCalls := 20
					errChan := make(chan error, 0)
					for i := 0; i < numCalls; i++ {
						go func() {
							errChan <- utils.VoteAndReturnError(p, nil, poll.PollId, voter, poll.Choices[0].ChoiceId)
						}()
					}

					Convey("Wait for all the responses to return", func() {
						ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
						defer cancel()

						successCount := 0
						errCount := 0

					responseWaitLoop:
						for i := 0; i < numCalls; i++ {
							select {
							case <-ctx.Done():
								So(errors.New("timed out waiting on poliwag service response"), ShouldBeNil)
								break responseWaitLoop

							case err := <-errChan:
								if err != nil {
									errCount++
								} else {
									successCount++
								}
							}
						}

						Convey("Validate that only a single request succeeded", func() {
							So(successCount, ShouldEqual, 1)
							So(errCount, ShouldEqual, numCalls-1)

							Convey("Validate that only a single vote was counted", func() {
								utils.GetVoterAndAssertVote(p, poll.PollId, voter, poll.Choices[0].ChoiceId, 1)

								Convey("Cleanup the poll", func() {
									utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
									expectedStatus := poliwag.PollStatus_ARCHIVED
									utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
								})
							})
						})
					})
				})
			})
		})

		Convey("Test that the vote API does not allow chat banned users to vote", func() {
			chatBannedVoter := utils.QaBitsTest_BannedUser
			voteIDUUID, err := uuid.NewV4()
			So(err, ShouldBeNil)
			voteID := voteIDUUID.String()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Validate that vote API rejects the user's vote", func() {
					utils.Vote(p, pointers.StringP(voteID), poll.PollId, chatBannedVoter, poll.Choices[0].ChoiceId, TwirpErrorP(twirp.PermissionDenied))

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						expectedStatus := poliwag.PollStatus_ARCHIVED
						utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
					})
				})
			})
		})
	})
}
