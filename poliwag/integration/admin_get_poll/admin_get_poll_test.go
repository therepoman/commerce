// +build integration

package get_poll

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TestAdminGetPoll(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		admin := client.NewAdminPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the admin get poll API happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get poll should succeed on active poll", func() {
					actualPoll, err := utils.AdminGetPoll(admin, poll.PollId, nil)

					So(err, ShouldBeNil)
					So(actualPoll, ShouldNotBeNil)
					So(actualPoll.PollId, ShouldEqual, poll.PollId)

					Convey("Moderate poll should succeed", func() {
						utils.ModeratePoll(admin, poll.PollId, nil)

						Convey("Get poll should succeed on moderated poll", func() {
							actualPoll, err := utils.AdminGetPoll(admin, poll.PollId, nil)

							So(err, ShouldBeNil)
							So(actualPoll, ShouldNotBeNil)
							So(actualPoll.PollId, ShouldEqual, poll.PollId)
						})
					})
				})
			})
		})
	})
}
