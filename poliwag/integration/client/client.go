// +build integration

package client

import (
	"log"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/config"
	poliwag "code.justin.tv/commerce/poliwag/proto/poliwag"
	poliwag_admin "code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

func loadConfig() *config.Config {
	environment := os.Getenv("ENVIRONMENT")
	if environment == "" {
		log.Panic("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panic("Please don't run these integration tests against prod!")
	}

	logrus.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		logrus.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		logrus.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	return cfg
}

func NewPoliwagClient() poliwag.PoliwagAPI {
	cfg := loadConfig()

	return poliwag.NewPoliwagAPIProtobufClient(cfg.Endpoints.Integration, http.DefaultClient)
}

func NewAdminPoliwagClient() poliwag_admin.PoliwagAdminAPI {
	cfg := loadConfig()

	return poliwag_admin.NewPoliwagAdminAPIProtobufClient(cfg.Endpoints.Integration, http.DefaultClient)
}
