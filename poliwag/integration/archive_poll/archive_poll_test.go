// +build integration

package archive_poll

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestArchive(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the archive poll API happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Archive poll should succeed", func() {
					// Archive the poll and wait for the status to update
					utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
					expectedStatus := poliwag.PollStatus_ARCHIVED
					utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)

					poll, err := utils.GetPoll(p, poll.PollId, poll.OwnedBy, nil)
					So(err, ShouldBeNil)
					So(poll, ShouldNotBeNil)
					So(poll.Status, ShouldEqual, expectedStatus)
					So(poll.GetRemainingDuration().GetSeconds(), ShouldEqual, 0)
					So(poll.GetRemainingDuration().GetNanos(), ShouldEqual, 0)
				})
			})
		})

		Convey("Test archive poll API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Archiving with a blank poll ID returns an invalid argument error", func() {
					utils.ArchivePoll(p, "", utils.QaBitsTestUser, TwirpErrorP(twirp.InvalidArgument))

					Convey("Archiving with a blank user ID returns an invalid argument error", func() {
						utils.ArchivePoll(p, poll.PollId, "", TwirpErrorP(twirp.InvalidArgument))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})

		Convey("Test the archive poll API's error handling of non-existent resources", func() {
			Convey("Archiving a non-existent poll ID returns a not found error", func() {
				utils.ArchivePoll(p, "ceci n'est pas une poll", utils.QaBitsTestUser, TwirpErrorP(twirp.NotFound))
			})
		})

		Convey("Test the archive poll API's error handling around polls ended and archived", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("End the poll", func() {
					utils.TerminatePoll(p, poll.PollId, poll.OwnedBy, nil)

					Convey("Archive the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)

						Convey("Archive on a poll that has archived are not allowed", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, TwirpErrorP(twirp.FailedPrecondition))
						})
					})
				})
			})
		})
	})
}
