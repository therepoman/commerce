// +build integration

package get_poll

import (
	"testing"

	"code.justin.tv/commerce/poliwag/proto/poliwag"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestGetViewablePoll(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get viewable poll API happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get viewable poll should succeed", func() {
					actualPoll := utils.GetViewablePoll(p, poll.OwnedBy, nil)

					So(actualPoll, ShouldNotBeNil)
					So(actualPoll.PollId, ShouldEqual, poll.PollId)

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
					})
				})
			})
		})

		Convey("Test the get viewable poll API with termianted poll", func() {
			Convey("Create a new poll in the test channel and terminate it", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				utils.TerminatePoll(p, poll.PollId, poll.OwnedBy, nil)
				expectedStatus := poliwag.PollStatus_TERMINATED
				utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)

				Convey("Get viewable poll should succeed", func() {
					actualPoll := utils.GetViewablePoll(p, poll.OwnedBy, nil)
					So(actualPoll, ShouldNotBeNil)
					So(actualPoll.PollId, ShouldEqual, poll.PollId)

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
					})
				})
			})
		})

		Convey("Test the get viewable poll API with no viewable poll", func() {
			viewablePoll := utils.GetViewablePoll(p, utils.QaBitsTestUser, nil)
			for viewablePoll != nil {
				utils.ArchivePoll(p, viewablePoll.PollId, viewablePoll.OwnedBy, nil)
				expectedStatus := poliwag.PollStatus_ARCHIVED
				utils.WaitForPollStatus(p, viewablePoll.PollId, viewablePoll.OwnedBy, &expectedStatus)
				viewablePoll = utils.GetViewablePoll(p, utils.QaBitsTestUser, nil)
			}

			Convey("Create a new poll in the test channel and archive it", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
				expectedStatus := poliwag.PollStatus_ARCHIVED
				utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)

				Convey("Get viewable poll should succeed", func() {
					actualPoll := utils.GetViewablePoll(p, utils.QaBitsTestUser, nil)
					So(actualPoll, ShouldBeNil)
				})
			})
		})

		Convey("Test get poll API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get Poll with a blank user ID returns an invalid argument error", func() {
					utils.GetViewablePoll(p, "", TwirpErrorP(twirp.InvalidArgument))

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
					})
				})
			})
		})
	})
}
