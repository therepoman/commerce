// +build integration

package get_voter

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestGetVoter(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get voter API happy path", func() {
			voter := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("A user's base vote should succeed", func() {
					utils.Vote(p, nil, poll.PollId, voter, poll.Choices[0].ChoiceId, nil)

					Convey("Get voter succeeds", func() {
						utils.GetVoter(p, poll.PollId, voter, nil)

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})

		Convey("Test the get voter API's error handling of non-existent resources", func() {
			voter := utils.RandomUser()

			Convey("Get voter call with non-existent poll returns a not found error", func() {
				utils.GetVoter(p, "poll", voter, TwirpErrorP(twirp.NotFound))

				Convey("Create a new poll in the test channel", func() {
					poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

					Convey("Get voter call with non-existent voter returns a not found error", func() {
						utils.GetVoter(p, poll.PollId, voter, TwirpErrorP(twirp.NotFound))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})

		Convey("Test the get voter API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				Convey("Get voter with a blank poll ID returns an invalid argument error", func() {
					utils.GetVoter(p, "", utils.QaBitsTestUser, TwirpErrorP(twirp.InvalidArgument))

					Convey("Get voter with a blank user ID returns an invalid argument error", func() {
						utils.GetVoter(p, poll.PollId, "", TwirpErrorP(twirp.InvalidArgument))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})
	})
}
