// +build integration

package get_polls

import (
	"testing"

	"code.justin.tv/commerce/poliwag/proto/poliwag"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestGetPolls(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get polls API happy path", func() {
			Convey("Create a new poll in the test channel and terminate it", func() {
				poll1 := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				utils.TerminatePoll(p, poll1.PollId, poll1.OwnedBy, nil)
				expectedStatus := poliwag.PollStatus_TERMINATED
				utils.WaitForPollStatus(p, poll1.PollId, poll1.OwnedBy, &expectedStatus)

				Convey("Create a second poll in the test channel", func() {
					poll2 := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

					Convey("Get polls should succeed", func() {
						polls, cursor := utils.GetPolls(p, utils.QaBitsTestUser, utils.QaBitsTestUser, nil, nil, nil, nil, nil, nil)

						So(polls, ShouldNotBeNil)
						So(len(polls), ShouldEqual, 1)
						So(polls[0].PollId, ShouldEqual, poll2.PollId)
						So(cursor, ShouldNotBeNil)

						Convey("Get polls with cursor should succeed", func() {
							polls, _ := utils.GetPolls(p, utils.QaBitsTestUser, utils.QaBitsTestUser, nil, nil, nil, nil, &cursor, nil)

							So(polls, ShouldNotBeNil)
							So(len(polls), ShouldEqual, 1)
							So(polls[0].PollId, ShouldEqual, poll1.PollId)
						})
					})
				})
			})
		})

		Convey("Get Polls respects status", func() {
			Convey("Create a new poll in the test channel and terminate it", func() {
				terminatedPoll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				utils.TerminatePoll(p, terminatedPoll.PollId, terminatedPoll.OwnedBy, nil)
				expectedStatus := poliwag.PollStatus_TERMINATED
				utils.WaitForPollStatus(p, terminatedPoll.PollId, terminatedPoll.OwnedBy, &expectedStatus)

				Convey("Create a second poll in the test channel and archive it", func() {
					archivedPoll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
					utils.ArchivePoll(p, archivedPoll.PollId, archivedPoll.OwnedBy, nil)
					expectedStatus := poliwag.PollStatus_ARCHIVED
					utils.WaitForPollStatus(p, archivedPoll.PollId, archivedPoll.OwnedBy, &expectedStatus)

					Convey("Create a third poll in the test channel and leave it active", func() {
						activePoll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

						limit := 3
						Convey("Get active polls should succeed", func() {
							activeStatus := []poliwag.PollStatus{poliwag.PollStatus_ACTIVE}
							activePolls, _ := utils.GetPolls(p, utils.QaBitsTestUser, utils.QaBitsTestUser, &activeStatus, nil, nil, &limit, nil, nil)

							So(activePolls, ShouldNotBeNil)
							So(len(activePolls), ShouldEqual, 1)
							So(activePolls[0].PollId, ShouldEqual, activePoll.PollId)
						})

						Convey("Get archived polls should succeed", func() {
							archivedStatus := []poliwag.PollStatus{poliwag.PollStatus_ARCHIVED}
							archivedPolls, _ := utils.GetPolls(p, utils.QaBitsTestUser, utils.QaBitsTestUser, &archivedStatus, nil, nil, &limit, nil, nil)

							So(archivedPolls, ShouldNotBeNil)
							So(len(archivedPolls), ShouldEqual, 1)
							So(archivedPolls[0].PollId, ShouldEqual, archivedPoll.PollId)
						})

						Convey("Get termianted polls should succeed", func() {
							termiantedStatus := []poliwag.PollStatus{poliwag.PollStatus_TERMINATED}
							terminatedPolls, _ := utils.GetPolls(p, utils.QaBitsTestUser, utils.QaBitsTestUser, &termiantedStatus, nil, nil, &limit, nil, nil)

							So(terminatedPolls, ShouldNotBeNil)
							So(len(terminatedPolls), ShouldEqual, 1)
							So(terminatedPolls[0].PollId, ShouldEqual, terminatedPoll.PollId)
						})
					})
				})
			})
		})

		Convey("Test get polls API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				Convey("Get Polls with a blank owned ID returns an invalid argument error", func() {
					utils.GetPolls(p, "", utils.QaBitsTestUser, nil, nil, nil, nil, nil, TwirpErrorP(twirp.InvalidArgument))

					Convey("Get Poll with a blank user ID returns an invalid argument error", func() {
						utils.GetPolls(p, utils.QaBitsTestUser, "", nil, nil, nil, nil, nil, TwirpErrorP(twirp.InvalidArgument))
					})
				})
			})
		})
	})
}
