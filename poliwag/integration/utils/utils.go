package utils

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/random"
	poliwag "code.justin.tv/commerce/poliwag/proto/poliwag"
	poliwag_admin "code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

const (
	defaultTimeout        = 5 * time.Second
	QaBitsTestUser        = "145903336"
	QaBitsTest_BannedUser = "434516781"

	numAttempts              = 10
	baseDelayBetweenAttempts = 2 * time.Second
)

func PreTestCleanup(p poliwag.PoliwagAPI, ownerID string) {
	poll := GetViewablePoll(p, ownerID, nil)
	if poll != nil && poll.Status == poliwag.PollStatus_ACTIVE {
		ArchivePoll(p, poll.PollId, ownerID, nil)
		expectedStatus := poliwag.PollStatus_ARCHIVED
		WaitForPollStatus(p, poll.PollId, ownerID, &expectedStatus)
	}
}

func RandomUser() string {
	return fmt.Sprintf("%d", random.Int64(100000000, 999999999))
}

func GetViewablePoll(p poliwag.PoliwagAPI, ownerID string, expectedErrorCode *twirp.ErrorCode) *poliwag.Poll {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	resp, err := p.GetViewablePoll(ctx, &poliwag.GetViewablePollRequest{
		OwnedBy: ownerID,
	})

	if expectedErrorCode == nil {
		So(err, ShouldBeNil)
		return resp.Poll
	} else {
		So(err, ShouldNotBeNil)
		twirpErr, ok := err.(twirp.Error)
		So(ok, ShouldBeTrue)
		So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)

		return nil
	}
}

func CreatePoll(p poliwag.PoliwagAPI, user string, owner string) *poliwag.Poll {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	resp, err := p.CreatePoll(ctx, &poliwag.CreatePollRequest{
		UserId:   user,
		OwnedBy:  owner,
		Title:    "[Integration Test] Best game?",
		Duration: ptypes.DurationProto(5 * time.Minute),
		Choices: []*poliwag.CreatePollRequest_CreatePollChoice{
			{Title: "Chester Cheetah: Too Cool to Fool (1992)"},
			{Title: "McDonald's Treasure Land Adventure (1993)"},
			{Title: "Little Caesars Fractions Pizza (1998)"},
			{Title: "Spot Goes to Hollywood (1995)"},
			{Title: "The California Raisins: The Grape Escape (1990)"},
		},
	})

	So(err, ShouldBeNil)
	return resp.Poll
}

func CreatePollWithDuration(p poliwag.PoliwagAPI, user string, owner string, duration time.Duration) *poliwag.Poll {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	resp, err := p.CreatePoll(ctx, &poliwag.CreatePollRequest{
		UserId:   user,
		OwnedBy:  owner,
		Title:    "[Integration Test] Best game?",
		Duration: ptypes.DurationProto(duration),
		Choices: []*poliwag.CreatePollRequest_CreatePollChoice{
			{Title: "Chester Cheetah: Too Cool to Fool (1992)"},
			{Title: "McDonald's Treasure Land Adventure (1993)"},
			{Title: "Little Caesars Fractions Pizza (1998)"},
			{Title: "Spot Goes to Hollywood (1995)"},
			{Title: "The California Raisins: The Grape Escape (1990)"},
		},
	})

	So(err, ShouldBeNil)
	return resp.Poll
}

func TerminatePoll(p poliwag.PoliwagAPI, pollID string, ownerID string, expectedErrorCode *twirp.ErrorCode) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	_, err := p.TerminatePoll(ctx, &poliwag.TerminatePollRequest{
		UserId: ownerID,
		PollId: pollID,
	})

	if expectedErrorCode == nil {
		So(err, ShouldBeNil)
	} else {
		So(err, ShouldNotBeNil)
		twirpErr, ok := err.(twirp.Error)
		So(ok, ShouldBeTrue)
		So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
	}
}

func VoteAndReturnError(p poliwag.PoliwagAPI, voteID *string, pollID string, userID string, choiceID string) error {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	if voteID == nil {
		voteIDUUID, err := uuid.NewV4()
		if err != nil {
			panic(err)
		}
		voteID = pointers.StringP(voteIDUUID.String())
	}

	_, err := p.Vote(ctx, &poliwag.VoteRequest{
		VoteId:   *voteID,
		PollId:   pollID,
		UserId:   userID,
		ChoiceId: choiceID,
	})
	return err
}

func Vote(p poliwag.PoliwagAPI, voteID *string, pollID string, userID string, choiceID string, expectedErrorCode *twirp.ErrorCode) {
	err := VoteAndReturnError(p, voteID, pollID, userID, choiceID)
	if expectedErrorCode == nil {
		So(err, ShouldBeNil)
	} else {
		So(err, ShouldNotBeNil)
		twirpErr, ok := err.(twirp.Error)
		So(ok, ShouldBeTrue)
		So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
	}
}

func GetVoter(p poliwag.PoliwagAPI, pollID string, userID string, expectedErrorCode *twirp.ErrorCode) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		defer cancel()

		resp, err := p.GetVoter(ctx, &poliwag.GetVoterRequest{
			PollId:      pollID,
			UserId:      userID,
			VoterUserId: userID,
		})
		if expectedErrorCode == nil {
			if err != nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetVoter returned an error (%v), expected nil", err)))
			}
			if resp == nil {
				failures = append(failures, errors.New("GetVoter returned a nil response, expected nil"))
			}
		} else {
			if err == nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetVoter did not return an error, expected (%v)", *expectedErrorCode)))
			} else {
				twirpErr, ok := err.(twirp.Error)
				if !ok {
					failures = append(failures, errors.New(fmt.Sprintf("GetVoter returned a non-twirp error (%v), expected (%v)", err, *expectedErrorCode)))
				} else {
					if twirpErr.Code() != *expectedErrorCode {
						failures = append(failures, errors.New(fmt.Sprintf("GetVoter returned the wrong twirp error code (%v), expected (%v)", twirpErr.Code(), *expectedErrorCode)))
					}
				}
			}
		}

		cancel()

		if len(failures) == 0 {
			return
		} else if attempt < numAttempts {
			logrus.Infof("GetVoter did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
		}
	}
}

func GetVoterAndAssertVote(p poliwag.PoliwagAPI, pollID string, userID string, choiceID string, baseVotes int64) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	resp, err := p.GetVoter(ctx, &poliwag.GetVoterRequest{
		PollId:      pollID,
		UserId:      userID,
		VoterUserId: userID,
	})
	So(err, ShouldBeNil)

	So(resp, ShouldNotBeNil)
	So(resp.Voter, ShouldNotBeNil)
	So(resp.Voter.Votes, ShouldNotBeNil)
	So(resp.Voter.Votes.Total, ShouldEqual, baseVotes)
	So(resp.Voter.Votes.Base, ShouldEqual, baseVotes)
	So(resp.Voter.Votes.Bits, ShouldEqual, 0)
	So(resp.Voter.Votes.ChannelPoints, ShouldEqual, 0)

	var votedChoice *poliwag.VoterChoice
	for _, choice := range resp.Voter.Choices {
		if choice.ChoiceId == choiceID {
			votedChoice = choice
		}
	}

	So(votedChoice, ShouldNotBeNil)
	So(votedChoice.Votes, ShouldNotBeNil)
	So(votedChoice.Votes.Total, ShouldEqual, baseVotes)
	So(votedChoice.Votes.Base, ShouldEqual, baseVotes)
	So(votedChoice.Votes.Bits, ShouldEqual, 0)
	So(votedChoice.Votes.ChannelPoints, ShouldEqual, 0)
}

func GetVoters(p poliwag.PoliwagAPI, pollID string, userID string, sort *poliwag.VoterSort, limit *int, expectedErrorCode *twirp.ErrorCode, expectedNumberOfVoters *int) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		req := poliwag.GetVotersRequest{
			PollId: pollID,
			UserId: userID,
		}

		if sort != nil {
			req.Sort = *sort
		}

		if limit != nil {
			req.Limit = int64(*limit)
		}

		resp, err := p.GetVoters(ctx, &req)
		if expectedErrorCode == nil {
			if err != nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetVoters returned an error (%v), expected nil", err)))
			}
			if resp == nil {
				failures = append(failures, errors.New("GetVoters returned a nil response, expected nil"))
			}
		} else {
			if err == nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetVoters did not return an error, expected (%v)", *expectedErrorCode)))
			} else {
				twirpErr, ok := err.(twirp.Error)
				if !ok {
					failures = append(failures, errors.New(fmt.Sprintf("GetVoters returned a non-twirp error (%v), expected (%v)", err, *expectedErrorCode)))
				} else {
					if twirpErr.Code() != *expectedErrorCode {
						failures = append(failures, errors.New(fmt.Sprintf("GetVoters returned the wrong twirp error code (%v), expected (%v)", twirpErr.Code(), *expectedErrorCode)))
					}
				}
			}
		}

		if expectedNumberOfVoters != nil {
			if len(resp.Voters) != *expectedNumberOfVoters {
				failures = append(failures, errors.New(fmt.Sprintf("GetVoters returned the wrong number of voters (%d), expected (%d)", len(resp.Voters), *expectedNumberOfVoters)))
			}
		}

		cancel()

		if len(failures) == 0 {
			return
		} else if attempt < numAttempts {
			logrus.Infof("GetVoters did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
		}
	}
}

func GetVotersByChoice(p poliwag.PoliwagAPI, pollID string, choiceID string, userID string, sort *poliwag.VoterSort, limit *int, expectedErrorCode *twirp.ErrorCode, expectedNumberOfVoters *int) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)

		req := poliwag.GetVotersByChoiceRequest{
			PollId:   pollID,
			ChoiceId: choiceID,
			UserId:   userID,
		}

		if sort != nil {
			req.Sort = *sort
		}

		if limit != nil {
			req.Limit = int64(*limit)
		}

		resp, err := p.GetVotersByChoice(ctx, &req)
		if expectedErrorCode == nil {
			if err != nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetVotersByChoice returned an error (%v), expected nil", err)))
			}
			if resp == nil {
				failures = append(failures, errors.New("GetVotersByChoice returned a nil response, expected not nil"))
			}
		} else {
			if err == nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetVotersByChoice did not return an error, expected (%v)", *expectedErrorCode)))
			} else {
				twirpErr, ok := err.(twirp.Error)
				if !ok {
					failures = append(failures, errors.New(fmt.Sprintf("GetVotersByChoice returned a non-twirp error (%v), expected (%v)", err, *expectedErrorCode)))
				} else {
					if twirpErr.Code() != *expectedErrorCode {
						failures = append(failures, errors.New(fmt.Sprintf("GetVotersByChoice returned the wrong twirp error code (%v), expected (%v)", twirpErr.Code(), *expectedErrorCode)))
					}
				}
			}
		}

		if expectedNumberOfVoters != nil {
			if len(resp.Voters) != *expectedNumberOfVoters {
				failures = append(failures, errors.New(fmt.Sprintf("GetVotersByChoice returned the wrong number of voters (%d), expected (%d)", len(resp.Voters), *expectedNumberOfVoters)))
			}
		}

		cancel()

		if len(failures) == 0 {
			return
		} else if attempt < numAttempts {
			logrus.Infof("GetVotersByChoice did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
		}
	}
}

func GetPolls(p poliwag.PoliwagAPI, ownerID string, userID string, status *[]poliwag.PollStatus, sort *poliwag.GetPollsSort, direction *poliwag.Direction, limit *int, cursor *string, expectedErrorCode *twirp.ErrorCode) ([]*poliwag.Poll, string) {
	req := &poliwag.GetPollsRequest{
		OwnedBy: ownerID,
		UserId:  userID,
		Limit:   1,
	}

	if status != nil {
		req.Status = *status
	}

	if sort != nil {
		req.Sort = *sort
	}

	if direction != nil {
		req.Direction = *direction
	}

	if limit != nil {
		req.Limit = int64(*limit)
	}

	if cursor != nil {
		req.Cursor = *cursor
	}

	return getPolls(p, req, expectedErrorCode)
}

func getPolls(p poliwag.PoliwagAPI, req *poliwag.GetPollsRequest, expectedErrorCode *twirp.ErrorCode) ([]*poliwag.Poll, string) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	resp, err := p.GetPolls(ctx, req)

	if expectedErrorCode == nil {
		So(err, ShouldBeNil)
		So(resp, ShouldNotBeNil)

		return resp.Polls, resp.Cursor
	} else {
		So(err, ShouldNotBeNil)
		twirpErr, ok := err.(twirp.Error)
		So(ok, ShouldBeTrue)
		So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)

		return nil, ""
	}
}

func AdminGetPolls(admin poliwag_admin.PoliwagAdminAPI, ownerID string, status *[]poliwag.PollStatus, sort *poliwag.GetPollsSort, direction *poliwag.Direction, limit *int, cursor *string, expectedErrorCode *twirp.ErrorCode) ([]*poliwag.Poll, *string, error) {
	req := &poliwag_admin.AdminGetPollsRequest{
		OwnedBy: ownerID,
		Limit:   1,
	}

	if status != nil {
		req.Status = *status
	}

	if sort != nil {
		req.Sort = *sort
	}

	if direction != nil {
		req.Direction = *direction
	}

	if limit != nil {
		req.Limit = int64(*limit)
	}

	if cursor != nil {
		req.Cursor = *cursor
	}

	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)

		resp, err := admin.AdminGetPolls(ctx, req)
		if expectedErrorCode == nil {
			if err != nil {
				failures = append(failures, errors.New(fmt.Sprintf("AdminGetPolls returned an error (%v), expected nil", err)))
			} else if resp == nil {
				failures = append(failures, errors.New("AdminGetPolls returned a nil response, expected not nil"))
			}
		} else {
			if err == nil {
				failures = append(failures, errors.New(fmt.Sprintf("AdminGetPolls did not return an error, expected (%v)", *expectedErrorCode)))
			} else {
				twirpErr, ok := err.(twirp.Error)
				if !ok {
					failures = append(failures, errors.New(fmt.Sprintf("AdminGetPolls returned a non-twirp error (%v), expected (%v)", err, *expectedErrorCode)))
				} else if twirpErr.Code() != *expectedErrorCode {
					failures = append(failures, errors.New(fmt.Sprintf("AdminGetPolls returned the wrong twirp error code (%v), expected (%v)", twirpErr.Code(), *expectedErrorCode)))
				} else {
					So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
				}
			}
		}

		cancel()

		if len(failures) == 0 && resp != nil {
			return resp.Polls, &resp.Cursor, nil
		} else if len(failures) == 0 {
			return nil, nil, nil
		} else if attempt < numAttempts {
			logrus.Infof("AdminGetPolls did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
			return nil, nil, errors.New(fmt.Sprintf("AdminGetPolls did not return the expected results after %d attempts", numAttempts))
		}
	}

	return nil, nil, errors.New(fmt.Sprintf("GetPoll did not return the expected results after %d attempts", numAttempts))
}

func AdminGetPoll(admin poliwag_admin.PoliwagAdminAPI, pollID string, expectedErrorCode *twirp.ErrorCode) (*poliwag.Poll, error) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		resp, err := admin.AdminGetPoll(ctx, &poliwag_admin.AdminGetPollRequest{
			PollId: pollID,
		})
		if expectedErrorCode == nil {
			if err != nil {
				failures = append(failures, errors.New(fmt.Sprintf("AdminGetPoll returned an error (%v), expected nil", err)))
			} else if resp == nil {
				failures = append(failures, errors.New("AdminGetPoll returned a nil response, expected not nil"))
			}
		} else {
			if err == nil {
				failures = append(failures, errors.New(fmt.Sprintf("AdminGetPoll did not return an error, expected (%v)", *expectedErrorCode)))
			} else {
				twirpErr, ok := err.(twirp.Error)
				if !ok {
					failures = append(failures, errors.New(fmt.Sprintf("AdminGetPoll returned a non-twirp error (%v), expected (%v)", err, *expectedErrorCode)))
				} else if twirpErr.Code() != *expectedErrorCode {
					failures = append(failures, errors.New(fmt.Sprintf("AdminGetPoll returned the wrong twirp error code (%v), expected (%v)", twirpErr.Code(), *expectedErrorCode)))
				} else {
					So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
				}
			}
		}

		cancel()

		if len(failures) == 0 && resp != nil {
			return resp.Poll, nil
		} else if len(failures) == 0 {
			return nil, nil
		} else if attempt < numAttempts {
			logrus.Infof("GetPoll did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
			return nil, errors.New(fmt.Sprintf("GetPoll did not return the expected results after %d attempts", numAttempts))
		}
	}

	return nil, errors.New(fmt.Sprintf("GetPoll did not return the expected results after %d attempts", numAttempts))
}

func GetPoll(p poliwag.PoliwagAPI, pollID string, userID string, expectedErrorCode *twirp.ErrorCode) (*poliwag.Poll, error) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		resp, err := p.GetPoll(ctx, &poliwag.GetPollRequest{
			PollId: pollID,
			UserId: userID,
		})
		if expectedErrorCode == nil {
			if err != nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned an error (%v), expected nil", err)))
			} else if resp == nil {
				failures = append(failures, errors.New("GetPoll returned a nil response, expected not nil"))
			}
		} else {
			if err == nil {
				failures = append(failures, errors.New(fmt.Sprintf("GetPoll did not return an error, expected (%v)", *expectedErrorCode)))
			} else {
				twirpErr, ok := err.(twirp.Error)
				if !ok {
					failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned a non-twirp error (%v), expected (%v)", err, *expectedErrorCode)))
				} else if twirpErr.Code() != *expectedErrorCode {
					failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned the wrong twirp error code (%v), expected (%v)", twirpErr.Code(), *expectedErrorCode)))
				} else {
					So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
				}
			}
		}

		cancel()

		if len(failures) == 0 && resp != nil {
			return resp.Poll, nil
		} else if len(failures) == 0 {
			return nil, nil
		} else if attempt < numAttempts {
			logrus.Infof("GetPoll did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
			return nil, errors.New(fmt.Sprintf("GetPoll did not return the expected results after %d attempts", numAttempts))
		}
	}

	return nil, errors.New(fmt.Sprintf("GetPoll did not return the expected results after %d attempts", numAttempts))
}

func ArchivePoll(p poliwag.PoliwagAPI, pollID string, userID string, expectedErrorCode *twirp.ErrorCode) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	_, err := p.ArchivePoll(ctx, &poliwag.ArchivePollRequest{
		PollId: pollID,
		UserId: userID,
	})

	if expectedErrorCode == nil {
		So(err, ShouldBeNil)
	} else {
		So(err, ShouldNotBeNil)
		twirpErr, ok := err.(twirp.Error)
		So(ok, ShouldBeTrue)
		So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
	}
}

func ModeratePoll(admin poliwag_admin.PoliwagAdminAPI, pollID string, expectedErrorCode *twirp.ErrorCode) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	_, err := admin.ModeratePoll(ctx, &poliwag_admin.ModeratePollRequest{
		PollId: pollID,
	})

	if expectedErrorCode == nil {
		So(err, ShouldBeNil)
	} else {
		So(err, ShouldNotBeNil)
		twirpErr, ok := err.(twirp.Error)
		So(ok, ShouldBeTrue)
		So(twirpErr.Code(), ShouldEqual, *expectedErrorCode)
	}
}

func WaitForPollStatus(p poliwag.PoliwagAPI, pollId string, ownedBy string, status *poliwag.PollStatus) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		resp, err := p.GetPoll(ctx, &poliwag.GetPollRequest{
			PollId: pollId,
			UserId: ownedBy,
		})
		if err != nil {
			failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned an error (%v), expected nil", err)))
		} else if resp == nil {
			failures = append(failures, errors.New("GetPoll returned a nil response, expected not nil"))
		}

		if resp.Poll == nil {
			failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned nil Poll, expected poll with id (%s)", pollId)))
		} else if resp.Poll.Status != *status {
			failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned the status (%s), expected (%s)", resp.Poll.Status, *status)))
		}

		cancel()

		if len(failures) == 0 {
			// pause momentarily to avoid staled read
			time.Sleep(50 * time.Microsecond)
			return
		} else if attempt < numAttempts {
			logrus.Infof("GetPoll did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
		}
	}
}

func AdminWaitForPollStatus(admin poliwag_admin.PoliwagAdminAPI, pollId string, status *poliwag.PollStatus) {
	for attempt := 1; attempt <= numAttempts; attempt++ {
		failures := make([]error, 0)

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		resp, err := admin.AdminGetPoll(ctx, &poliwag_admin.AdminGetPollRequest{
			PollId: pollId,
		})
		if err != nil {
			failures = append(failures, errors.New(fmt.Sprintf("AdminGetPoll returned an error (%v), expected nil", err)))
		} else if resp == nil {
			failures = append(failures, errors.New("AdminGetPoll returned a nil response, expected not nil"))
		}

		if resp.Poll.Status != *status {
			failures = append(failures, errors.New(fmt.Sprintf("GetPoll returned the status (%s), expected (%s)", resp.Poll.Status, *status)))
		}

		cancel()

		if len(failures) == 0 {
			return
		} else if attempt < numAttempts {
			logrus.Infof("AdminGetPoll did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		} else {
			So(failures, ShouldBeEmpty)
		}
	}
}
