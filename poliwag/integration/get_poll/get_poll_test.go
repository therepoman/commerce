// +build integration

package get_poll

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestGetPoll(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get poll API happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get poll should succeed", func() {
					actualPoll, err := utils.GetPoll(p, poll.PollId, poll.OwnedBy, nil)

					So(err, ShouldBeNil)
					So(actualPoll, ShouldNotBeNil)
					So(actualPoll.OwnedBy, ShouldEqual, poll.OwnedBy)

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
					})
				})
			})
		})

		Convey("Test get poll API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get Poll with a blank poll ID returns an invalid argument error", func() {
					actualPoll, err := utils.GetPoll(p, "", utils.QaBitsTestUser, TwirpErrorP(twirp.InvalidArgument))
					So(actualPoll, ShouldBeNil)
					So(err, ShouldBeNil)

					Convey("Get Poll with a blank user ID returns an invalid argument error", func() {
						actualPoll, err = utils.GetPoll(p, poll.PollId, "", TwirpErrorP(twirp.InvalidArgument))
						So(actualPoll, ShouldBeNil)
						So(err, ShouldBeNil)

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})

		Convey("Test the get poll API's error handling of non-existent resources", func() {
			Convey("Get Poll a non-existent poll ID returns a not found error", func() {
				actualPoll, err := utils.GetPoll(p, "ceci n'est pas une poll", utils.QaBitsTestUser, TwirpErrorP(twirp.NotFound))
				So(actualPoll, ShouldBeNil)
				So(err, ShouldBeNil)
			})
		})
	})
}
