// +build integration

package admin_get_polls

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestAdminGetPolls(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		admin := client.NewAdminPoliwagClient()

		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get polls API happy path", func() {
			Convey("Create a new poll in the test channel and moderate it", func() {
				poll1 := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				utils.ModeratePoll(admin, poll1.PollId, nil)

				Convey("Create a second poll in the test channel", func() {
					poll2 := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

					Convey("Get polls should succeed", func() {
						polls, cursor, err := utils.AdminGetPolls(admin, utils.QaBitsTestUser, nil, nil, nil, nil, nil, nil)

						So(err, ShouldBeNil)
						So(polls, ShouldNotBeNil)
						So(len(polls), ShouldEqual, 1)
						So(polls[0].PollId, ShouldEqual, poll2.PollId)
						So(cursor, ShouldNotBeNil)

						Convey("Get polls with cursor should succeed", func() {
							polls, _, err := utils.AdminGetPolls(admin, utils.QaBitsTestUser, nil, nil, nil, nil, cursor, nil)

							So(err, ShouldBeNil)
							So(polls, ShouldNotBeNil)
							So(len(polls), ShouldEqual, 1)
							So(polls[0].PollId, ShouldEqual, poll1.PollId)
						})
					})
				})
			})
		})

		Convey("Test get admin polls API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				Convey("Admin Get Polls with a blank owned ID returns an invalid argument error", func() {
					polls, cursor, err := utils.AdminGetPolls(admin, "", nil, nil, nil, nil, nil, TwirpErrorP(twirp.InvalidArgument))
					So(err, ShouldBeNil)
					So(polls, ShouldBeNil)
					So(cursor, ShouldBeNil)
				})
			})
		})
	})
}
