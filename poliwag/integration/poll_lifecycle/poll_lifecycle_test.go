// +build integration

package poll_lifecycle

import (
	"testing"
	"time"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
	poliwag "code.justin.tv/commerce/poliwag/proto/poliwag"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPollLifecycle(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the poll lifecycle happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				duration := 15 * time.Second
				poll := utils.CreatePollWithDuration(p, utils.QaBitsTestUser, utils.QaBitsTestUser, duration)

				Convey("Wait 15+ seconds for poll to complete naturally", func() {
					expectedStatus := poliwag.PollStatus_COMPLETED
					utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
				})
			})
		})
	})
}
