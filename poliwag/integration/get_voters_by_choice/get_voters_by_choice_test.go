// +build integration

package get_voters_by_choice_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestGetVotersByChoice(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the get voters API happy path", func() {
			voter1 := utils.RandomUser()
			voter2 := utils.RandomUser()
			voter3 := utils.RandomUser()

			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Vote in the test channel", func() {
					utils.Vote(p, nil, poll.PollId, voter1, poll.Choices[0].ChoiceId, nil)
					utils.Vote(p, nil, poll.PollId, voter2, poll.Choices[0].ChoiceId, nil)
					utils.Vote(p, nil, poll.PollId, voter3, poll.Choices[1].ChoiceId, nil)

					Convey("Get choice voters should succeed for first choice", func() {
						utils.GetVotersByChoice(p, poll.PollId, poll.Choices[0].ChoiceId, utils.QaBitsTestUser, nil, nil, nil, pointers.IntP(2))
					})

					Convey("Get choice voters should succeed for second choice", func() {
						utils.GetVotersByChoice(p, poll.PollId, poll.Choices[1].ChoiceId, utils.QaBitsTestUser, nil, nil, nil, pointers.IntP(1))
					})

					Convey("Get choice voters should succeed for third choice", func() {
						utils.GetVotersByChoice(p, poll.PollId, poll.Choices[2].ChoiceId, utils.QaBitsTestUser, nil, nil, nil, pointers.IntP(0))
					})
				})
			})
		})

		Convey("Test the get voter by choice API's error handling of non-existent resources", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Get voter by choice call with non-existent poll returns a not found error", func() {
					utils.GetVotersByChoice(p, "not the droid you are looking for", poll.Choices[0].ChoiceId, utils.QaBitsTestUser, nil, nil, TwirpErrorP(twirp.NotFound), nil)

					Convey("Get voter by choice call with non-existent choice id returns a not found error", func() {
						utils.GetVotersByChoice(p, poll.PollId, "lies", utils.QaBitsTestUser, nil, nil, TwirpErrorP(twirp.NotFound), nil)

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						})
					})
				})
			})
		})

		Convey("Test the get voter by choice API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)
				Convey("Get choice voters with a blank poll ID returns an invalid argument error", func() {
					utils.GetVotersByChoice(p, "", poll.Choices[0].ChoiceId, utils.QaBitsTestUser, nil, nil, TwirpErrorP(twirp.InvalidArgument), nil)

					Convey("Get choice voters with a blank choice ID returns an invalid argument error", func() {
						utils.GetVotersByChoice(p, poll.PollId, "", utils.QaBitsTestUser, nil, nil, TwirpErrorP(twirp.InvalidArgument), nil)

						Convey("Get voter with a blank user ID returns an invalid argument error", func() {
							utils.GetVotersByChoice(p, poll.PollId, poll.Choices[0].ChoiceId, "", nil, nil, TwirpErrorP(twirp.InvalidArgument), nil)

							Convey("Cleanup the poll", func() {
								utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							})
						})
					})
				})
			})
		})
	})
}
