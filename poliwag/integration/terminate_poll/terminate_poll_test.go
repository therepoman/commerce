// +build integration

package terminate_poll

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/integration/client"
	"code.justin.tv/commerce/poliwag/integration/utils"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TwirpErrorP(twirpErrorCode twirp.ErrorCode) *twirp.ErrorCode {
	return &twirpErrorCode
}

func TestTerminatePoll(t *testing.T) {
	Convey("Cleanup any lingering polls for the test user", t, func() {
		p := client.NewPoliwagClient()
		utils.PreTestCleanup(p, utils.QaBitsTestUser)

		Convey("Test the terminate poll API happy path", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Terminate poll should succeed", func() {
					// Terminate the poll and wait for the status to update
					utils.TerminatePoll(p, poll.PollId, poll.OwnedBy, nil)
					expectedStatus := poliwag.PollStatus_TERMINATED
					utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)

					poll, err := utils.GetPoll(p, poll.PollId, poll.OwnedBy, nil)
					So(err, ShouldBeNil)
					So(poll, ShouldNotBeNil)
					So(poll.Status, ShouldEqual, expectedStatus)
					So(poll.GetRemainingDuration().GetSeconds(), ShouldEqual, 0)
					So(poll.GetRemainingDuration().GetNanos(), ShouldEqual, 0)

					Convey("Cleanup the poll", func() {
						utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
						expectedStatus := poliwag.PollStatus_ARCHIVED
						utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
					})
				})
			})
		})

		Convey("Test terminate poll API's input validation", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("Terminating with a blank poll ID returns an invalid argument error", func() {
					utils.TerminatePoll(p, "", utils.QaBitsTestUser, TwirpErrorP(twirp.InvalidArgument))

					Convey("Terminating with a blank user ID returns an invalid argument error", func() {
						utils.TerminatePoll(p, poll.PollId, "", TwirpErrorP(twirp.InvalidArgument))

						Convey("Cleanup the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							expectedStatus := poliwag.PollStatus_ARCHIVED
							utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
						})
					})
				})
			})
		})

		Convey("Test the terminate poll API's error handling of non-existent resources", func() {
			Convey("Terminating a non-existent poll ID returns a not found error", func() {
				utils.ArchivePoll(p, "ceci n'est pas une poll", utils.QaBitsTestUser, TwirpErrorP(twirp.NotFound))
			})
		})

		Convey("Test the terminate poll API's error handling around polls terminated and archived", func() {
			Convey("Create a new poll in the test channel", func() {
				poll := utils.CreatePoll(p, utils.QaBitsTestUser, utils.QaBitsTestUser)

				Convey("End the poll", func() {
					utils.TerminatePoll(p, poll.PollId, poll.OwnedBy, nil)
					expectedStatus := poliwag.PollStatus_TERMINATED
					utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)

					Convey("Terminateing the poll again not allow", func() {
						utils.TerminatePoll(p, poll.PollId, poll.OwnedBy, TwirpErrorP(twirp.FailedPrecondition))

						Convey("Archive the poll", func() {
							utils.ArchivePoll(p, poll.PollId, poll.OwnedBy, nil)
							expectedStatus := poliwag.PollStatus_ARCHIVED
							utils.WaitForPollStatus(p, poll.PollId, poll.OwnedBy, &expectedStatus)
						})
					})
				})
			})
		})
	})
}
