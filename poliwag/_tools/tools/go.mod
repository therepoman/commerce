module code.justin.tv/commerce/poliwag/_tools/tools

go 1.16

require (
	code.justin.tv/dkwasnick/gql-to-md v1.1.0
	github.com/envoyproxy/protoc-gen-validate v0.3.0-java // indirect
	github.com/golang/protobuf v1.4.3
	github.com/golangci/golangci-lint v1.38.0
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/maxbrunsfeld/counterfeiter/v6 v6.4.1
	github.com/pseudomuto/protoc-gen-doc v1.3.2
	github.com/twitchtv/twirp v5.7.1-0.20190429180631-337e90237d72+incompatible
	github.com/uber/prototool v1.10.0
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5
	golang.org/x/mod v0.4.2 // indirect
)

replace github.com/go-critic/go-critic@v0.0.0-20181204210945-c3db6069acc5 => github.com/go-critic/go-critic v0.3.5-0.20190422201921-c3db6069acc5

replace github.com/golang/protobuf => github.com/golang/protobuf v1.3.4
