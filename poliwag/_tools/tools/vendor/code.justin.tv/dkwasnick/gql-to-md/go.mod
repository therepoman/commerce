module code.justin.tv/dkwasnick/gql-to-md

go 1.16

require (
	github.com/gookit/color v1.3.8
	gopkg.in/yaml.v2 v2.2.7
)
