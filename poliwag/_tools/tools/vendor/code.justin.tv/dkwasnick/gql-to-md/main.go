package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"code.justin.tv/dkwasnick/gql-to-md/ast"

	"github.com/gookit/color"
	"gopkg.in/yaml.v2"
)

var (
	configFile string
	outputFile string
	gqlHost    string
	clientID   string
	maxDepth   int
)

func init() {
	flag.StringVar(&configFile, "config", "config.yaml", "Path to the config yaml file")
	flag.StringVar(&outputFile, "output", "docs.md", "Path to output file")
	flag.StringVar(&gqlHost, "gql-url", defaultGQLHost, "URL to query GraphQL (should end in /gql)")
	flag.StringVar(&clientID, "client-id", defaultClientID, "Client ID to pass to GraphQL")
	flag.IntVar(&maxDepth, "max-depth", defaultMaxDepth, "Maximum depth to search for undefined objects")
	flag.Parse()
}

const (
	defaultGQLHost  = "https://gql.twitch.tv/gql"
	defaultClientID = "453c81m7vdprsgq1sfaih2f8avsmaf" // Twilight Local Development
	defaultMaxDepth = 5
)

type Config struct {
	Title     string                `yaml:"Title"`
	Fields    []map[string][]string `yaml:"Fields"`
	Types     []string              `yaml:"Types"`
	Mutations []string              `yaml:"Mutations"`
	Enums     []string              `yaml:"Enums"`
	Blacklist []string              `yaml:"Blacklist"`
}

func main() {
	fmt.Printf("Generating GQL documentation...\n")

	configFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		color.Red.Printf("Error reading config file: '%v'\n", err)
		os.Exit(1)
	}
	config := &Config{}
	err = yaml.Unmarshal(configFile, config)
	if err != nil {
		color.Red.Printf("Error unmarshalling config file: '%v'\n", err)
		os.Exit(1)
	}

	fmt.Printf("Read configuration successfully...\n")

	schema, err := fetchSchema()
	if err != nil {
		color.Red.Printf("Error fetching schema: '%v'\n", err)
		os.Exit(1)
	}

	fmt.Printf("Schema fetched successfully...\n")

	printer := NewPrinter(config, schema)
	body := printer.printAll()

	err = ioutil.WriteFile(outputFile, []byte(body), 0644)
	if err != nil {
		color.Red.Printf("Error writing output: '%v'\n", err)
		os.Exit(1)
	}

	color.LightGreen.Printf("Successfully generated documentation.\n")
	os.Exit(0)
}

func fetchSchema() (*ast.Schema, error) {
	gqlRequestBody := struct {
		Query string `json:"query"`
	}{
		ast.IntrospectionQuery,
	}
	bodyBytes, _ := json.Marshal(gqlRequestBody)
	req, err := http.NewRequestWithContext(context.Background(), "POST", gqlHost, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, fmt.Errorf("error creating request: '%v'", err)
	}
	req.Header.Set("Content-Type", "application/graphql")
	req.Header.Set("Client-ID", clientID)

	client := http.DefaultClient
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	} else if resp.StatusCode != http.StatusOK {
		body, readErr := ioutil.ReadAll(resp.Body)
		return nil, fmt.Errorf("got status '%d', body '%s', readErr '%v'", resp.StatusCode, string(body), readErr)
	}
	defer resp.Body.Close()

	respBodyBytes, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		return nil, fmt.Errorf("reading body: '%v'", readErr)
	}

	var gqlResp ast.IntrospectionResponse
	unmarshalErr := json.Unmarshal(respBodyBytes, &gqlResp)
	if unmarshalErr != nil {
		return nil, fmt.Errorf("unmarshalling response: '%v'", unmarshalErr)
	}

	schema := ast.ParseSchema(&gqlResp)
	return schema, nil
}

func (p *Printer) printAll() string {
	sort.Strings(p.Config.Mutations)
	sort.Strings(p.Config.Types)
	sort.Strings(p.Config.Enums)

	if p.Config.Title != "" {
		p.body += fmt.Sprintf("# %s\n", p.Config.Title)
	}
	p.body += fmt.Sprintf("#### Last updated: %s\n", time.Now().Format(time.RFC1123))
	p.printTOC()
	p.body += fmt.Sprintf("# Fields\n")
	for _, nameMap := range p.Config.Fields {
		for name, fields := range nameMap {
			if object, ok := p.Schema.Types[name]; ok {
				p.printObject(object, fields)
			}
		}
	}

	p.body += fmt.Sprintf("# Objects\n")
	for _, name := range p.Config.Types {
		if object, ok := p.Schema.Types[name]; ok {
			p.printObject(object, nil)
		}
	}

	p.body += fmt.Sprintf("# Enums\n")
	for _, name := range p.Config.Enums {
		if object, ok := p.Schema.Types[name]; ok {
			p.printEnum(object)
		}
	}

	p.body += fmt.Sprintf("# Mutations\n")
	for _, name := range p.Config.Mutations {
		if mutation, ok := p.Schema.Mutations[name]; ok {
			p.printMutation(mutation)
		}
	}

	return p.body
}

func NewPrinter(config *Config, schema *ast.Schema) *Printer {
	printer := &Printer{
		Config:    config,
		Schema:    schema,
		Undefined: []*ast.FullType{},
		Defined:   map[string]bool{},
	}
	for _, name := range config.Types {
		printer.Defined[name] = true
	}
	for _, name := range config.Enums {
		printer.Defined[name] = true
	}
	printer.determineUndefined()

	for _, undef := range printer.Undefined {
		switch undef.Kind {
		case ast.Enum:
			printer.Config.Enums = append(printer.Config.Enums, undef.Name)
		case ast.Object, ast.InputObject:
			printer.Config.Types = append(printer.Config.Types, undef.Name)
		}
	}

	return printer
}

func (p *Printer) determineUndefined() {
	for _, name := range p.Config.Types {
		if obj, ok := p.Schema.Types[name]; ok {
			p.walkDeterineUndefined(0, obj)
		}
	}
	for _, name := range p.Config.Mutations {
		if mutation, ok := p.Schema.Mutations[name]; ok {
			p.walkDeterminedUndefinedField(0, mutation)
		}
	}
	for _, nameMap := range p.Config.Fields {
		for name, fields := range nameMap {
			if object, ok := p.Schema.Types[name]; ok {
				fieldsList := []*ast.Field(object.Fields)
				for _, field := range fieldsList {
					if containsStr(fields, field.Name) {
						p.walkDeterminedUndefinedField(0, field)
					}
				}
			}
		}
	}
}

func (p *Printer) printTOC() {
	p.body += fmt.Sprintf("# Table of Contents\n")
	p.body += fmt.Sprintf("- [Fields](#Fields)\n")
	for _, nameMap := range p.Config.Fields {
		for name, _ := range nameMap {
			p.body += fmt.Sprintf("  - [%s](#%s)\n", name, name)
		}
	}

	p.body += fmt.Sprintf("- [Objects](#Objects)\n")
	for _, name := range p.Config.Types {
		p.body += fmt.Sprintf("  - [%s](#%s)\n", name, name)
	}

	p.body += fmt.Sprintf("- [Enums](#Enums)\n")
	for _, name := range p.Config.Enums {
		p.body += fmt.Sprintf("  - [%s](#%s)\n", name, name)
	}

	p.body += fmt.Sprintf("- [Mutations](#Mutations)\n")
	for _, name := range p.Config.Mutations {
		p.body += fmt.Sprintf("  - [%s](#%s)\n", name, name)
	}
}

func (p *Printer) walkDeterineUndefined(depth int, object *ast.FullType) {
	if depth > maxDepth {
		return
	}
	for _, field := range object.Fields {
		p.walkDeterminedUndefinedField(depth, field)
	}
	for _, field := range object.InputFields {
		p.walkDeterminedUndefinedArgument(depth, field)
	}
}

func (p *Printer) walkDeterminedUndefinedField(depth int, field *ast.Field) {
	fname := field.Type.GetUnderlyingName()
	if _, ok := p.Defined[fname]; !ok {
		if obj, ok := p.Schema.Types[fname]; ok && !containsStr(p.Config.Blacklist, fname) {
			p.Undefined = append(p.Undefined, obj)
			p.Defined[fname] = false
			p.walkDeterineUndefined(depth+1, obj)
		}
	}
	for _, arg := range field.Args {
		p.walkDeterminedUndefinedArgument(depth+1, arg)
	}
}

func (p *Printer) walkDeterminedUndefinedArgument(depth int, arg *ast.InputValue) {
	aname := arg.Type.GetUnderlyingName()
	if _, ok := p.Defined[aname]; !ok {
		if obj, ok := p.Schema.Types[aname]; ok && !containsStr(p.Config.Blacklist, aname) {
			p.Undefined = append(p.Undefined, obj)
			p.Defined[aname] = false
			p.walkDeterineUndefined(depth+1, obj)
		}
	}
}

type Printer struct {
	Config *Config
	Schema *ast.Schema
	// Types that have been (or will be) defined
	Defined map[string]bool
	// Types that we need to add somewhere
	Undefined []*ast.FullType

	body string
}

// printObjects prints out an object and all of its subfields in table form.
// if a list of subfields is provided, will only print that subset of the object's fields.
func (p *Printer) printObject(object *ast.FullType, subfields []string) {
	subfieldsMap := sliceToMap(subfields)
	p.body += fmt.Sprintf("## %s\n", object.Name)
	if object.Description != "" {
		p.body += fmt.Sprintf("*%s*\n", object.Description)
	}
	if len(object.Fields) > 0 {
		p.body += fmt.Sprintf("### Fields:\n")
		p.body += fmt.Sprintf("Name | Type | Description\n")
		p.body += fmt.Sprintf("--- | --- | ---\n")
		for _, f := range object.Fields {
			if len(subfieldsMap) > 0 && !contains(subfieldsMap, f.Name) {
				// skip this field
				continue
			}
			description := strings.ReplaceAll(f.Description, "\n", " ")
			if deprecated, reason := isDeprecated(f); deprecated {
				description = fmt.Sprintf("%s **Deprecated**: %s", description, reason)
			}
			p.body += fmt.Sprintf("%s %s| %s | %s\n", f.Name, argString(f.Args), typeLink(f.Type), description)
		}
	}
	if len(object.InputFields) > 0 {
		p.body += fmt.Sprintf("### Input Fields:\n")
		p.body += fmt.Sprintf("Name | Type | Description | Default\n")
		p.body += fmt.Sprintf("--- | --- | --- | ---\n")
		for _, f := range object.InputFields {
			if len(subfieldsMap) > 0 && !contains(subfieldsMap, f.Name) {
				// skip this field
				continue
			}
			description := strings.ReplaceAll(f.Description, "\n", " ")
			defaultValue := "*n/a*"
			if f.DefaultValue != nil {
				defaultValue = *f.DefaultValue
			}
			p.body += fmt.Sprintf("%s | %s | %s | %s\n", f.Name, typeLink(f.Type), description, defaultValue)
		}
	}
}

// Returns a boolean whether the field is deprecated, and the deprecation reason
func isDeprecated(field *ast.Field) (bool, string) {
	if !field.IsDeprecated {
		return false, ""
	} else if field.DeprecationReason != nil {
		return true, *field.DeprecationReason
	} else {
		return true, "Deprecated"
	}
}

func (p *Printer) printEnum(object *ast.FullType) {
	p.body += fmt.Sprintf("## %s\n", object.Name)
	if object.Description != "" {
		p.body += fmt.Sprintf("*%s*\n", object.Description)
	}
	if len(object.EnumValues) > 0 {
		p.body += fmt.Sprintf("### Values:\n")
		p.body += fmt.Sprintf("Name |  Description\n")
		p.body += fmt.Sprintf("--- | ---\n")
		for _, v := range object.EnumValues {
			p.body += fmt.Sprintf("%s | %s\n", v.Name, v.Description)
		}
	}
}

// printMutation prints out a mutation in table form.
func (p *Printer) printMutation(mutation *ast.Field) {
	p.body += fmt.Sprintf("## %s\n", mutation.Name)
	if mutation.Description != "" {
		p.body += fmt.Sprintf("*%s*\n", mutation.Description)
	}
	p.body += fmt.Sprintf("##### %s %s: %s\n", mutation.Name, argString(mutation.Args), typeLink(mutation.Type))
}

func argString(args []*ast.InputValue) string {
	if len(args) > 0 {
		argStrs := make([]string, len(args))
		for i, arg := range args {
			argStrs[i] = fmt.Sprintf("%s: %s", arg.Name, typeLink(arg.Type))
			if arg.DefaultValue != nil {
				argStrs[i] += fmt.Sprintf(" = %s", *arg.DefaultValue)
			}
		}
		return fmt.Sprintf("(%s)", strings.Join(argStrs, ", "))
	}
	return ""
}

func typeLink(t *ast.TypeRef) string {
	return fmt.Sprintf("[%s](#%s)", t.GetNameWithDetail(), t.GetUnderlyingName())
}

func containsStr(strs []string, s string) bool {
	return contains(sliceToMap(strs), s)
}

func contains(m map[string]struct{}, s string) bool {
	_, ok := m[s]
	return ok
}

func sliceToMap(strs []string) map[string]struct{} {
	resp := make(map[string]struct{}, len(strs))
	for _, s := range strs {
		resp[s] = struct{}{}
	}
	return resp
}
