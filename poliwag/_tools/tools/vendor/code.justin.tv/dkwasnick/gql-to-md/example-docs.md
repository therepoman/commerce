# My Example GraphQL Documentation
#### Last updated: Sat, 07 Dec 2019 12:46:56 PST
# Table of Contents
- [Fields](#Fields)
  - [User](#User)
- [Objects](#Objects)
  - [UpdateChatColorInput](#UpdateChatColorInput)
  - [UpdateChatColorPayload](#UpdateChatColorPayload)
  - [WhisperSettings](#WhisperSettings)
- [Enums](#Enums)
  - [PollStatus](#PollStatus)
- [Mutations](#Mutations)
  - [updateChatColor](#updateChatColor)
# Fields
## User
*Twitch user.*
### Fields:
Name | Type | Description
--- | --- | ---
id | [ID!](#ID) | The user's unique identifier.
login | [String](#String) | The user's standard alphanumeric Twitch name.
# Objects
## UpdateChatColorInput
### Input Fields:
Name | Type | Description | Default
--- | --- | --- | ---
color | [String!](#String) | Named color for normal users (e.g. "Blue"  or "Coral", see https://help.twitch.tv/customer/portal/articles/659095-twitch-chat-and-moderation-commands) or a hex color for Turbo users (e.g. "#001122"). | *n/a*
## UpdateChatColorPayload
### Fields:
Name | Type | Description
--- | --- | ---
user | [User](#User) | The user whose chat color was set.
## WhisperSettings
*Settings related to the Whispers (private user-to-user messaging) feature.*
### Fields:
Name | Type | Description
--- | --- | ---
isBlockingWhispersFromStrangers | [Boolean!](#Boolean) | If true, disallows strangers from initiating a whisper thread to this user.
isWhisperBanned | [Boolean!](#Boolean) | If true, this user is disallowed from sending or receiving whisper messages.
# Enums
## PollStatus
*The status of the poll.*
### Values:
Name |  Description
--- | ---
UNKNOWN | Encountered some poll status that we do not know how to handle BibleThump.
ACTIVE | Poll is running. Users can vote. Results are publicly visible.
COMPLETED | Poll ran its entire duration and "naturally" completed. Users cannot vote. Results are publicly visible.
TERMINATED | Poll was manually ended ("terminated") by a user. Users cannot vote. Results are publicly visible.
ARCHIVED | Poll has ended and is no longer publicly visible. Users cannot vote. Results are not publicly visible.
MODERATED | Poll has been moderated by Twitch and is no longer viewable, even to the poll owner. Users cannot vote. Results are not visible to any user.
# Mutations
## updateChatColor
*Updates a user's chat color.*
##### updateChatColor (input: [UpdateChatColorInput!](#UpdateChatColorInput)): [UpdateChatColorPayload](#UpdateChatColorPayload)
