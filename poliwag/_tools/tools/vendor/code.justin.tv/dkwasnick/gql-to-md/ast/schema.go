package ast

type Schema struct {
	Queries   map[string]*Field
	Mutations map[string]*Field
	Types     map[string]*FullType
}

func ParseSchema(resp *IntrospectionResponse) *Schema {
	typesMap := make(map[string]*FullType, len(resp.Data.Schema.Types))
	for _, t := range resp.Data.Schema.Types {
		typesMap[t.Name] = t
	}

	query := typesMap["Query"]
	queryMap := make(map[string]*Field, len(query.Fields))
	for _, f := range query.Fields {
		queryMap[f.Name] = f
	}

	mutation := typesMap["Mutation"]
	mutationMap := make(map[string]*Field, len(mutation.Fields))
	for _, f := range mutation.Fields {
		mutationMap[f.Name] = f
	}

	return &Schema{
		Queries:   queryMap,
		Mutations: mutationMap,
		Types:     typesMap,
	}
}
