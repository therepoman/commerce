package ast

import "fmt"

type DefinitionKind string

const (
	// Types
	Scalar      DefinitionKind = "SCALAR"
	Object      DefinitionKind = "OBJECT"
	Interface   DefinitionKind = "INTERFACE"
	Union       DefinitionKind = "UNION"
	Enum        DefinitionKind = "ENUM"
	InputObject DefinitionKind = "INPUT_OBJECT"
	// Wrappers
	NonNullObject DefinitionKind = "NON_NULL"
	ListObject    DefinitionKind = "LIST"
)

type FullType struct {
	Kind          DefinitionKind `json:"kind"`
	Name          string         `json:"name"`
	Description   string         `json:"description"`
	Fields        []*Field       `json:"fields"`
	InputFields   []*InputValue  `json:"inputFields"`
	Interfaces    []*TypeRef     `json:"interfaces"`
	EnumValues    []*EnumValue   `json:"enumValues"`
	PossibleTypes []*TypeRef     `json:"possibleTypes"`
}

type Field struct {
	Name              string        `json:"name"`
	Description       string        `json:"description"`
	Args              []*InputValue `json:"args"`
	Type              *TypeRef      `json"type"`
	IsDeprecated      bool          `json:"isDeprecated"`
	DeprecationReason *string       `json:"deprecationReason"`
}

type InputValue struct {
	Name         string   `json:"name"`
	Description  string   `json:"description"`
	Type         *TypeRef `json:"type"`
	DefaultValue *string  `json:"defaultValue"`
}

type TypeRef struct {
	Kind   DefinitionKind `json:"kind"`
	Name   *string        `json:"name"`
	OfType *TypeRef       `json:"ofType"`
}

func (t *TypeRef) GetUnderlyingName() string {
	if t.Name != nil {
		return *t.Name
	}
	return t.OfType.GetUnderlyingName()
}

func (t *TypeRef) GetNameWithDetail() string {
	switch t.Kind {
	case Scalar, Object, Interface, Union, Enum, InputObject:
		return *t.Name
	case ListObject:
		return fmt.Sprintf("[%s]", t.OfType.GetNameWithDetail())
	case NonNullObject:
		return fmt.Sprintf("%s!", t.OfType.GetNameWithDetail())
	}
	return ""
}

type EnumValue struct {
	Name              string  `json:"name"`
	Description       string  `json:"description"`
	IsDeprecated      bool    `json:"isDeprecated"`
	DeprecationReason *string `json:"deprecationReason"`
}
