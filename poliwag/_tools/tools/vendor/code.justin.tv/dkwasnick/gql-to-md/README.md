# gql-to-md

Convert a subset of a GraphQL schema to a Markdown document.

![Example](example.png)

## Motivation
GraphQL is wonderfully self-documenting, and tools such as GraphiQL make it easy to browse the entire schema. However,
the schema for the entire Twitch website is extremely large, which makes it difficult to find a specific field or type
even if you know exactly what you're looking for. When creating a new feature we often need to create many new types,
mutations, enums, and fields – but there is no way to aggregate all of these new definitions besides searching for the
name of the new feature in GraphiQL and hoping that they all come up.

This tool makes it easy to generate a unified documentation of the GraphQL schema related to a project/feature. It also
automatically detects all dependent sub-fields and types that must be defined in order to fully define your project.
For example, when creating a new mutation, we often also create new associated Input, Payload, Error, and ErrorCode
types. Rather than requiring you to include all of these fields in your config.yaml, this tool will automatically find
and document all sub-fields unless you specifically instruct it to skip them.

## Installation

```bash
go get -u code.justin.tv/dkwasnick/gql-to-md
```

## Usage
```bash
gql-to-md -config config.yaml -output docs.md
```
In order to use *gql-to-md*, you must create a configuration yaml file that specifies which GQL types should be documented.
You can find an example configuration file in [example-config.yaml](example-config.yaml), and the documentation it
generated in [example-docs.md](example-docs.md).

