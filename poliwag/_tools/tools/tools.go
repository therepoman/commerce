// +build tools

package tools

// Track tool dependencies https://github.com/golang/go/wiki/Modules#how-can-i-track-tool-dependencies-for-a-module

import (
	_ "github.com/golang/protobuf/protoc-gen-go"
	_ "github.com/golangci/golangci-lint/cmd/golangci-lint"
	_ "github.com/maxbrunsfeld/counterfeiter/v6"
	_ "github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc"
	_ "github.com/twitchtv/twirp/protoc-gen-twirp"
	_ "github.com/uber/prototool/cmd/prototool"
	_ "github.com/vektra/mockery/cmd/mockery"

	_ "code.justin.tv/dkwasnick/gql-to-md"
)
