-- Delete the user if it already exists
DROP USER 'poliwag-service'@'%';

-- Create the user
CREATE USER 'poliwag-service'@'%' IDENTIFIED BY 'some_password_that_should_be_changed';

-- Grant the user all permissions except dropping tables
GRANT CREATE, DELETE, INSERT, SELECT, UPDATE, ALTER, INDEX on poliwag.* TO 'poliwag-service'@'%';