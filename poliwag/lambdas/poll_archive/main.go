package main

import (
	"os"

	"github.com/aws/aws-lambda-go/lambda"

	"code.justin.tv/commerce/poliwag/clients/spade"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend"
	"code.justin.tv/commerce/poliwag/config"
)

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Error("error loading config")
		os.Exit(1)
	}

	sampleReporter, stop := backend.SetupTwitchTelemetry(cfg, true)
	defer stop()

	spadeClient, err := spade.NewClient(cfg, sampleReporter)
	if err != nil {
		log.WithError(err).Error("error creating spade client")
		os.Exit(1)
	}
	go func() {
		if err := spadeClient.Start(); err != nil {
			log.WithError(err).Error("starting spade client")
		}
	}()
	defer func() {
		if err := spadeClient.Close(); err != nil {
			log.WithError(err).Error("closing spade client")
		}
	}()

	be, err := backend.NewBackend(cfg, true, sampleReporter, spadeClient, nil)
	if err != nil {
		log.WithError(err).Error("error initializing backend")
		os.Exit(1)
	}

	lambda.Start(be.GetLambdaHandlers().PollArchive.Handle)
}
