package main

import (
	"context"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/codegangsta/cli"
	"github.com/pkg/errors"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	"code.justin.tv/commerce/poliwag/config"
)

var environment string
var dryrun bool
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "ReportDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringSliceFlag{
			Name:  "userID",
			Usage: "Runtime user IDs to report deletion for. Only works if local is set to true.",
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Error("error loading config")
		os.Exit(1)
	}

	sampleReporter, stop := backend.SetupTwitchTelemetry(cfg, true)
	defer stop()

	app.Action = func(c *cli.Context) {
		userIDs := c.StringSlice("userIDs")
		err := createBackend(cfg, sampleReporter, userIDs)
		if err != nil {
			log.WithError(err).Error("error creating backend")
			os.Exit(1)
		}
	}

	if err := app.Run(os.Args); err != nil {
		log.WithError(err).Error("error running app")
		os.Exit(1)
	}
}

func createBackend(cfg *config.Config, sampleReporter *telemetry.SampleReporter, userIDs []string) error {
	be, err := pdms.NewBackend(cfg, sampleReporter)
	if err != nil {
		return errors.Wrap(err, "error initializing backend")
	}

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Minute)
		defer cancel()

		output, err := be.GetLambdaHandlers().ReportDeletion.Handle(ctx, &sfn_models.ReportDeletionRequest{
			UserDeletionRequestData: sfn_models.UserDeletionRequestData{
				UserIDs:  userIDs,
				IsDryRun: dryrun,
			},
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}

		log.WithField("output", output).Info("Execution Output")
		return nil
	}

	lambda.Start(be.GetLambdaHandlers().ReportDeletion.Handle)
	return nil
}
