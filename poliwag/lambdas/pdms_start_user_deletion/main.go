package main

import (
	"context"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/codegangsta/cli"
	"github.com/pkg/errors"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms"
	"code.justin.tv/commerce/poliwag/config"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/lambdafunc"
	"code.justin.tv/eventbus/schema/pkg/user"
)

var environment string
var dryrun bool
var userID string
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "ReportDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "userID, u",
			Usage:       "The user ID to start deletion on; only used for local execution",
			Destination: &userID,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Error("error loading config")
		os.Exit(1)
	}

	sampleReporter, stop := backend.SetupTwitchTelemetry(cfg, true)
	defer stop()

	app.Action = func(c *cli.Context) {
		err := createBackend(cfg, sampleReporter)
		if err != nil {
			log.WithError(err).Error("error creating backend")
			os.Exit(1)
		}
	}

	if err := app.Run(os.Args); err != nil {
		log.WithError(err).Error("error running app")
		os.Exit(1)
	}
}

func createBackend(cfg *config.Config, sampleReporter *telemetry.SampleReporter) error {
	be, err := pdms.NewBackend(cfg, sampleReporter)
	if err != nil {
		return errors.Wrap(err, "error initializing backend")
	}

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()

		err := be.GetLambdaHandlers().StartUserDeletion.Handle(ctx, &eventbus.Header{}, &user.Destroy{
			UserId: userID,
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}

		log.Info("Execution Complete")
		return nil
	}

	mux := eventbus.NewMux()
	user.RegisterDestroyHandler(mux, be.GetLambdaHandlers().StartUserDeletion.Handle)
	lambda.Start(lambdafunc.NewSQS(mux.Dispatcher()))
	return nil
}
