module "poliwag_service" {
  source = "../poliwag"

  name          = "poliwag"
  region        = "${var.region}"
  backup_region = "${var.backup_region}"
  environment   = "${var.environment}"
  env           = "${var.env}"

  cluster = "poliwag-cluster"

  vpc_id         = "${var.vpc_id}"
  security_group = "${var.security_group}"
  subnets        = "${var.subnets}"

  autoscaling_role = "arn:aws:iam::720603942490:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dax_role         = "arn:aws:iam::720603942490:role/aws-service-role/dax.amazonaws.com/AWSServiceRoleForDAX"

  min_tasks = "2"
  max_tasks = "10"

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = "${var.env}"
    },
  ]

  whitelisted_arns_for_privatelink = "${var.whitelisted_arns_for_privatelink}"

  aws_profile    = "${var.aws_profile}"
  aws_account_id = "${var.aws_account_id}"

  min_aurora_capacity = 2
  max_aurora_capacity = 8

  sandstorm_role = "poliwag-staging"

  subnet_ips = "${var.subnet_ips}"

  elasticache_instance_type = "cache.m3.medium"
  dax_instance_type         = "dax.t2.small"

  providers = {
    aws        = "aws"
    aws.backup = "aws.backup"
  }
}

module "poliwag_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "poliwag.staging.poliwag.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-0b26374a50ed55e20"
  vpc_id            = "${var.vpc_id}"
}

module "hallpass_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "hallpass.staging.cb.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-05c9a9682cec9fa80"
  vpc_id            = "${var.vpc_id}"
}

module "ripley_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "ripley.dev.us-west2.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-0302ab0bc9133e1a4"
  vpc_id            = "${var.vpc_id}"
}

module "ripley_vpc_endpoint__NEW" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_ripley_twitch_a2z_com_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "beta.ripley.twitch.a2z.com"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-0302ab0bc9133e1a4"
  vpc_id            = "${var.vpc_id}"
}

// LDAP privatelink does not use the same pattern as above because it's an edge case where
// we cannot use CNAME as the look-up record since it would have the same value as the
// name of the hosted zone (both ldap.twitch.a2z.com), the terracode module uses ALIAS
// instead which would not have this issue.
module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.security_group}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.subnets)}"
  dns             = "ldap.twitch.a2z.com"
}

module "payday_peering" {
  source = "../vpc-peering"

  vpc_id  = "${var.vpc_id}"
  subnets = ["${split(",", var.subnets)}"]

  peer_account_alias = "twitch-bits-aws"
  peer_account_id    = "021561903526"
  peer_cidr_block    = "10.193.64.0/18"
  peer_vpc_id        = "vpc-4cfeb629"
}

module "chat_peering" {
  source = "../vpc-peering"

  vpc_id  = "${var.vpc_id}"
  subnets = ["${split(",", var.subnets)}"]

  peer_account_alias = "twitch-chat-aws"
  peer_account_id    = "603200399373"
  peer_cidr_block    = "10.193.192.0/18"
  peer_vpc_id        = "vpc-6183fe04"
}

module "pubsub_dns_entry" {
  source = "../dns-entry"

  route53_zone_id = "${module.poliwag_service.route_53_internal_twitch_tv_zone}"
  dns_cname       = "pubsub-broker-darklaunch.internal.twitch.tv"
  dns_resolves_to = "internal-pubsub-broker-darklaunch-1635774229.us-west-2.elb.amazonaws.com"
}

module "subscriptions_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "subscriptions-stag.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-01a6cf5f16aa49e81"
  vpc_id            = "${var.vpc_id}"
}

module "users_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_uswest2_twitch_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "users-service.dev.us-west2.twitch.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-05dd975d62ccc04fe"
  vpc_id            = "${var.vpc_id}"
}

module "pantheon_peering" {
  source = "../vpc-peering"

  vpc_id  = "${var.vpc_id}"
  subnets = ["${split(",", var.subnets)}"]

  peer_account_alias = "twitch-pantheon-aws-devo"
  peer_account_id    = "192960249206"
  peer_cidr_block    = "10.201.180.0/22"
  peer_vpc_id        = "vpc-11635a76"
}

module "pantheon_dns_entry" {
  source = "../dns-entry"

  route53_zone_id = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  dns_cname       = "pantheon-staging.internal.justin.tv"
  dns_resolves_to = "staging-commerce-pantheon-alb-env.hgizn9e8id.us-west-2.elasticbeanstalk.com"
}

module "copo_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_copo_twitch_a2z_com_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "staging.copo.twitch.a2z.com"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-013eac8455ce7d6be"
  vpc_id            = "${var.vpc_id}"
}

module "privatelink-pantheon" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=980609112b9b584795efc720221af72b81e928ef"

  name            = "pantheon"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0e024e12c03495b3e"
  environment     = "${var.environment}"
  security_groups = ["${var.security_group}"]
  subnets         = "${split(",", var.subnets)}"
  vpc_id          = "${var.vpc_id}"
}

// Add the twitch-inventory role
module "twitch-inventory" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory.git?ref=a7207ce935fce975ee739198bcd7e2df80419680"
  account = "poliwag-devo"
}

terraform {
  backend "s3" {
    bucket  = "poliwag-terraform-staging"
    key     = "tfstate/commerce/poliwag/terraform/staging"
    region  = "us-west-2"
    profile = "poliwag-devo"
  }
}
