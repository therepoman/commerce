environment = "staging"

env = "staging"

aws_profile = "poliwag-devo"

aws_account_id = "720603942490"

region = "us-west-2"

backup_region = "us-east-2"

vpc_id = "vpc-053527e190df55087"

security_group = "sg-033f6947191f702c8"

subnets = "subnet-0ac808526d06dd3cb,subnet-0703c2b3309af8f50,subnet-0ab88735718b2c568"

whitelisted_arns_for_privatelink = [
  "arn:aws:iam::021561903526:root", // payday
  "arn:aws:iam::720603942490:root", // poliwag-devo
  "arn:aws:iam::645130450452:root", // graphql-dev
  "arn:aws:iam::665448329738:root", // aegis-devo
  "arn:aws:iam::327140220177:root", // helix-dev
]

subnet_ips = [
  "10.206.152.0/24",
  "10.206.153.0/24",
  "10.206.154.0/24",
]
