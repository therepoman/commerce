variable "env" {}

variable "poliwag_logscan_namespace" {}

variable "security_groups" {
  type = "list"
}

variable "subnets" {}

variable "lambda_name" {}

variable "lambda_bucket" {}

variable "source_path" {}

variable "timeout" {
  default = 60
}

variable "memory_size" {
  default = 1024
}

variable "sandstorm_assume_role_policy_arn" {}

variable "concurrent_executions" {
  default = -1
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
