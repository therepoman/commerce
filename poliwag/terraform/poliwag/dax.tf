module "dax" {
  source = "dax"

  env            = "${var.env}"
  instance_type  = "${var.dax_instance_type}"
  port           = "${var.dax_port}"
  security_group = "${var.security_group}"
  vpc_id         = "${var.vpc_id}"
  subnets        = "${var.subnets}"
}
