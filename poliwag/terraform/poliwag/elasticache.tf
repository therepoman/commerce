resource "aws_elasticache_parameter_group" "poliwag_redis_params" {
  family = "redis4.0"
  name   = "poliwag-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "cluster-enabled"
    value = "no"
  }

  parameter {
    name  = "notify-keyspace-events"
    value = "Ex"
  }
}

resource "aws_elasticache_replication_group" "poliwag_redis" {
  replication_group_id          = "poliwag-${var.env}"
  replication_group_description = "poliwag redis non cluster"
  engine                        = "redis"
  engine_version                = "4.0.10"
  number_cache_clusters         = 3
  node_type                     = "${var.elasticache_instance_type}"
  parameter_group_name          = "${aws_elasticache_parameter_group.poliwag_redis_params.name}"
  port                          = "${var.redis_port}"
  subnet_group_name             = "${aws_elasticache_subnet_group.poliwag_redis_subnet_group.name}"
  security_group_ids            = ["${aws_security_group.poliwag_redis_security_group.id}"]
}

resource "aws_elasticache_subnet_group" "poliwag_redis_subnet_group" {
  name       = "poliwag-${var.env}-redis"
  subnet_ids = ["${split(",", var.subnets)}"]
}

resource "aws_security_group" "poliwag_redis_security_group" {
  name        = "poliwag-${var.env}-redis"
  vpc_id      = "${var.vpc_id}"
  description = "Allows communication with redis server"

  ingress {
    from_port   = "${var.redis_port}"
    protocol    = "tcp"
    to_port     = "${var.redis_port}"
    cidr_blocks = "${var.subnet_ips}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "poliwag-${var.env}-redis"
  }
}
