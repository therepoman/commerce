variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "namespace" {
  default = "poliwag"
  type    = "string"
}

variable "service" {
  default = "Poliwag"
  type    = "string"
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
