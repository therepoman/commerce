resource "aws_cloudwatch_metric_alarm" "panic_log_alarm" {
  alarm_name          = "poliwag-${var.env}-panic-logs-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "poliwag-${var.env}-panics"
  namespace           = "${var.namespace}"
  period              = "60"
  statistic           = "Sum"
  datapoints_to_alarm = "1"
  threshold           = "1"
  alarm_description   = "Panic has occured"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "error_log_alarm" {
  alarm_name          = "poliwag-${var.env}-error-logs-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "poliwag-${var.env}-errors"
  namespace           = "${var.namespace}"
  period              = "60"
  statistic           = "Sum"
  datapoints_to_alarm = "5"
  threshold           = "200"
  alarm_description   = "High volume of error logs"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.low_urgency_action_arn}"]
  ok_actions          = ["${var.low_urgency_action_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "warn_log_alarm" {
  alarm_name          = "poliwag-${var.env}-warn-logs-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "poliwag-${var.env}-warns"
  namespace           = "${var.namespace}"
  period              = "60"
  statistic           = "Sum"
  datapoints_to_alarm = "5"
  threshold           = "25"
  alarm_description   = "High level of warns"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.low_urgency_action_arn}"]
  ok_actions          = ["${var.low_urgency_action_arn}"]
}
