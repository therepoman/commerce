module "create_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation              = "CreatePoll"
  stage                  = "${var.env}"
  substage               = "primary"
  service                = "${var.service}"
  latency_threshold      = "1"
  client_error_threshold = "20"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "create_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation              = "CreatePoll"
  stage                  = "${var.env}"
  substage               = "canary"
  service                = "${var.service}"
  latency_threshold      = "1"
  client_error_threshold = "20"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetPoll"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetPoll"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_viewable_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetViewablePoll"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_viewable_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetViewablePoll"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_polls_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetPolls"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.3"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_polls_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetPolls"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.3"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "terminate_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "TerminatePoll"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "1"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "terminate_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "TerminatePoll"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "1"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "archive_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "ArchivePoll"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "1"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "archive_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "ArchivePoll"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "1"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_voter_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation                 = "GetVoter"
  stage                     = "${var.env}"
  substage                  = "primary"
  service                   = "${var.service}"
  latency_threshold         = "0.5"
  client_error_threshold    = "100"            // setting the GetVoter client error threshold and eval periods high due to high baseline 4xx
  client_error_eval_periods = "30"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_voter_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation                 = "GetVoter"
  stage                     = "${var.env}"
  substage                  = "canary"
  service                   = "${var.service}"
  latency_threshold         = "0.5"
  client_error_threshold    = "100"            // setting the GetVoter client error threshold and eval periods high due to high baseline 4xx
  client_error_eval_periods = "30"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_voters_by_choice_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetVotersByChoice"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.5"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_voters_by_choice_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetVotersByChoice"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.5"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_voters_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetVoters"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.5"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "get_voters_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "GetVoters"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.5"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "vote_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation              = "Vote"
  stage                  = "${var.env}"
  substage               = "primary"
  service                = "${var.service}"
  latency_threshold      = "1.5"
  client_error_threshold = "20"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "vote_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation              = "Vote"
  stage                  = "${var.env}"
  substage               = "canary"
  service                = "${var.service}"
  latency_threshold      = "1.5"
  client_error_threshold = "20"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "moderate_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "ModeratePoll"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "1"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "moderate_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "ModeratePoll"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "1"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "admin_get_poll_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "AdminGetPoll"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "admin_get_poll_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "AdminGetPoll"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "admin_get_polls_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "AdminGetPolls"
  stage             = "${var.env}"
  substage          = "primary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}

module "admin_get_polls_canary_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_api_alarms"

  operation         = "AdminGetPolls"
  stage             = "${var.env}"
  substage          = "canary"
  service           = "${var.service}"
  latency_threshold = "0.2"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}
