resource "aws_cloudwatch_metric_alarm" "high_cpu_alarm" {
  alarm_name                = "poliwag-${var.env}-high-cpu-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/ECS"
  period                    = "60"
  statistic                 = "Maximum"
  threshold                 = "80"
  unit                      = "Percent"
  alarm_description         = "High CPUUtilization Alarm"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  alarm_actions             = ["${var.low_urgency_action_arn}"]
  ok_actions                = ["${var.low_urgency_action_arn}"]

  dimensions = {
    ClusterName = "poliwag-cluster"
    ServiceName = "poliwag"
  }
}
