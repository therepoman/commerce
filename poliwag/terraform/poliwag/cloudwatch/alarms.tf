module "cloudwatch_alarms" {
  source = "alarms"

  env       = "${var.env}"
  service   = "${var.service}"
  namespace = "${var.namespace}"

  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
  high_urgency_action_arn = "${var.high_urgency_action_arn}"
}
