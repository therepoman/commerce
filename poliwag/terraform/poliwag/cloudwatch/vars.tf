variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "namespace" {
  type = "string"
}

variable "service" {
  type = "string"
}

variable "log_group" {
  description = "Cloudwatch log group in which application logs are written"
  type        = "string"
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
