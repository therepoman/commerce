module "cloudwatch_logs" {
  source = "logs"

  env       = "${var.env}"
  namespace = "${var.namespace}"
  log_group = "${var.log_group}"
}
