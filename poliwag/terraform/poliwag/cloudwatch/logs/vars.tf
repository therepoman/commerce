variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "log_group" {
  description = "Cloudwatch log group in which application logs are written"
  type        = "string"
}

variable "namespace" {
  default = "poliwag"
  type    = "string"
}
