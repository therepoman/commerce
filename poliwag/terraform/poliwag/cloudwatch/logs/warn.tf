resource "aws_cloudwatch_log_metric_filter" "warn_log_scan" {
  name           = "poliwag-${var.env}-warn-scan"
  pattern        = "level=warn"
  log_group_name = "${var.namespace}"

  metric_transformation {
    name          = "poliwag-${var.env}-warns"
    namespace     = "${var.namespace}"
    value         = "1"
    default_value = "0"
  }
}
