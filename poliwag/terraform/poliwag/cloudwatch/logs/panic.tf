resource "aws_cloudwatch_log_metric_filter" "panic_logs" {
  name           = "poliwag-${var.env}-panic-scan"
  pattern        = "?\"panic:\" ?panic.go"
  log_group_name = "${var.log_group}"

  metric_transformation {
    name          = "poliwag-${var.env}-panics"
    namespace     = "${var.namespace}"
    value         = "1"
    default_value = "0"
  }
}
