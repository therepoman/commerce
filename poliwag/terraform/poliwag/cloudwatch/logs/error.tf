resource "aws_cloudwatch_log_metric_filter" "error_log_scan" {
  name           = "poliwag-${var.env}-error-scan"
  pattern        = "level=error"
  log_group_name = "${var.log_group}"

  metric_transformation {
    name          = "poliwag-${var.env}-errors"
    namespace     = "${var.namespace}"
    value         = "1"
    default_value = "0"
  }
}
