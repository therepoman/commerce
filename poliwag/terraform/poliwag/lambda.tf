module "dynamo_backup_lambda" {
  source = "modules/dynamo/dynamo_backup_lambda"
  region = "${var.region}"

  providers = {
    aws = "aws"
  }
}

module "replica_dynamo_backup_lambda" {
  source = "modules/dynamo/dynamo_backup_lambda"
  region = "${var.backup_region}"

  providers = {
    aws = "aws.backup"
  }
}

module "poll_update_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "poll_update"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/poll_update/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_complete_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "poll_complete"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/poll_complete/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_terminate_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "poll_terminate"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/poll_terminate/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_archive_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "poll_archive"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/poll_archive/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_moderate_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "poll_moderate"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/poll_moderate/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "get_poll_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "get_poll"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/get_poll/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "pdms_delete_user_leaderboard_entries_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "pdms_delete_user_leaderboard_entries"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/pdms_delete_user_leaderboard_entries/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "pdms_delete_user_votes_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "pdms_delete_user_votes"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/pdms_delete_user_votes/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "pdms_report_deletion_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "pdms_report_deletion"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/pdms_report_deletion/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "pdms_start_user_deletion_lambda" {
  source                           = "../lambda"
  env                              = "${var.env}"
  poliwag_logscan_namespace        = "poliwag"
  security_groups                  = ["${var.security_group}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "pdms_start_user_deletion"
  sandstorm_assume_role_policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
  source_path                      = "${path.module}/lambda/pdms_start_user_deletion/deployment.zip"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  concurrent_executions            = 5
  high_urgency_action_arn          = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn           = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

resource "aws_lambda_event_source_mapping" "pdms_start_user_deletion_queue_event_source" {
  event_source_arn = "${aws_sqs_queue.eventbus_user_deletion_queue.arn}"
  function_name    = "${module.pdms_start_user_deletion_lambda.lambda_name}"
  batch_size       = 1
}
