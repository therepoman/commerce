data "http" "eventbus_cloudformation" {
  url = "https://eventbus-setup.s3-us-west-2.amazonaws.com/cloudformation.yaml"
}

resource "aws_cloudformation_stack" "eventbus" {
  name          = "EventBus"
  capabilities  = ["CAPABILITY_NAMED_IAM"]
  template_body = "${data.http.eventbus_cloudformation.body}"
}

resource "aws_iam_role_policy_attachment" "eventbus_start_user_deletion_lambda_access_attach" {
  role       = "${module.pdms_start_user_deletion_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_service_access_attach" {
  role       = "${module.service.service_role}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_service_canary_access_attach" {
  role       = "${module.service.canary_role}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_poll_update_lambda_access_attach" {
  role       = "${module.poll_update_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_poll_complete_lambda_access_attach" {
  role       = "${module.poll_complete_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_poll_terminate_lambda_access_attach" {
  role       = "${module.poll_terminate_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_poll_archive_lambda_access_attach" {
  role       = "${module.poll_archive_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_poll_moderate_lambda_access_attach" {
  role       = "${module.poll_moderate_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_iam_role_policy_attachment" "eventbus_get_poll_lambda_access_attach" {
  role       = "${module.get_poll_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}
