module "dynamo" {
  source                       = "dynamo"
  autoscaling_role             = "${var.autoscaling_role}"
  region                       = "${var.region}"
  backup_region                = "${var.backup_region}"
  environment                  = "${var.environment}"
  env                          = "${var.env}"
  dynamo_backup_lambda         = "${module.dynamo_backup_lambda.lambda_arn}"
  replica_dynamo_backup_lambda = "${module.replica_dynamo_backup_lambda.lambda_arn}"

  providers = {
    aws        = "aws"
    aws.backup = "aws.backup"
  }
}
