resource "aws_s3_bucket" "lambdas" {
  bucket = "poliwag-lambdas-${var.env}"
  acl    = "private"
}
