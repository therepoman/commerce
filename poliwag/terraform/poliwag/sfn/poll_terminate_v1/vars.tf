variable "region" {
  description = "The region where AWS resources live"
}

variable "poll_update_lambda_arn" {
  description = "The ARN of the poll update lambda"
}

variable "poll_terminate_lambda_arn" {
  description = "The ARN of the poll terminate lambda"
}

variable "get_poll_lambda_arn" {
  description = "The ARN of the get poll lambda"
}

variable "poll_archive_lambda_arn" {
  description = "The ARN of the poll archive lambda"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
