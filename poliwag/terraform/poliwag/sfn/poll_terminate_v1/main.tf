resource "aws_iam_role" "poll_terminate_v1_sfn" {
  name = "poll_terminate_v1_sfn"

  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "poll_terminate_v1_sfn_lambda_execution"
  role   = "${aws_iam_role.poll_terminate_v1_sfn.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}

resource "aws_sfn_state_machine" "poll_terminate_v1_state_machine" {
  name     = "poll_terminate_v1_state_machine"
  role_arn = "${aws_iam_role.poll_terminate_v1_sfn.arn}"

  definition = <<EOF
{
  "Comment": "A state machine that terminates a poll and then archives it after the appropriate time",
  "StartAt": "OneLastUpdate",
  "States": {
    "OneLastUpdate" : {
      "Type" : "Task",
      "Resource": "${var.poll_update_lambda_arn}",
      "Next": "PollTerminate",
      "Retry": ${local.default_retry},
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "PollTerminate"
          }
      ]
    },
    "PollTerminate": {
      "Type" : "Task",
      "Resource": "${var.poll_terminate_lambda_arn}",
      "Next": "WaitArchive",
      "Retry": ${local.default_retry},
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ]
    },
    "WaitArchive": {
      "Type" : "Wait",
      "Seconds": 60,
      "Next": "GetPoll"
    },
    "GetPoll": {
      "Type" : "Task",
      "Resource": "${var.get_poll_lambda_arn}",
      "Next": "ShouldArchive",
      "Retry": ${local.default_retry},
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ]
    },
    "ShouldArchive": {
      "Type": "Choice",
      "Choices": [
        {
          "Variable": "$.poll.Status",
          "StringEquals": "TERMINATED",
          "Next": "PollArchive"
        }
      ],
      "Default": "SuccessState"
    },
    "PollArchive": {
      "Type" : "Task",
      "Resource": "${var.poll_archive_lambda_arn}",
      "Next": "SuccessState",
      "Retry": ${local.default_retry},
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ]
    },
    "FailState": {
      "Type": "Fail"
    },
    "SuccessState": {
      "Type": "Succeed"
    }
  }
}
EOF
}

module "poll_terminate_v1_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_sfn_alarms"

  env                = "${var.env}"
  state_machine_name = "${aws_sfn_state_machine.poll_terminate_v1_state_machine.name}"
  state_machine_arn  = "${aws_sfn_state_machine.poll_terminate_v1_state_machine.id}"
  log_name           = "/aws/lambda/poll_terminate"

  high_urgency_action_arn = "${var.high_urgency_action_arn}"
  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
}

locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF
}
