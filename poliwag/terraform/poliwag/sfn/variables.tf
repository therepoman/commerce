variable "aws_profile" {
  description = "AWS profile used to manage resources"
}

variable "aws_region" {
  description = "AWS region to manage resources within"
  default     = "us-west-2"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
  default     = "dev"
}
