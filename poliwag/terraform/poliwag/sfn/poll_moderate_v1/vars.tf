variable "region" {
  description = "The region where AWS resources live"
}

variable "poll_update_lambda_arn" {
  description = "The ARN of the poll update lambda"
}

variable "poll_moderate_lambda_arn" {
  description = "The ARN of the poll moderate lambda"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
