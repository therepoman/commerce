resource "aws_iam_role" "poll_archive_v1_sfn" {
  name = "poll_archive_v1_sfn"

  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "poll_archive_v1_sfn_lambda_execution"
  role   = "${aws_iam_role.poll_archive_v1_sfn.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}

resource "aws_sfn_state_machine" "poll_archive_v1_state_machine" {
  name     = "poll_archive_v1_state_machine"
  role_arn = "${aws_iam_role.poll_archive_v1_sfn.arn}"

  definition = <<EOF
{
  "Comment": "A state machine that archives a poll",
  "StartAt": "OneLastUpdate",
  "States": {
    "OneLastUpdate" : {
      "Type" : "Task",
      "Resource": "${var.poll_update_lambda_arn}",
      "Next": "PollArchive",
      "Retry": ${local.default_retry},
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "PollArchive"
          }
      ]
    },
    "PollArchive": {
      "Type" : "Task",
      "Resource": "${var.poll_archive_lambda_arn}",
      "Next": "SuccessState",
      "Retry": ${local.default_retry},
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ]
    },
    "FailState": {
      "Type": "Fail"
    },
    "SuccessState": {
      "Type": "Succeed"
    }
  }
}
EOF
}

module "poll_archive_v1_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_sfn_alarms"

  env                = "${var.env}"
  state_machine_name = "${aws_sfn_state_machine.poll_archive_v1_state_machine.name}"
  state_machine_arn  = "${aws_sfn_state_machine.poll_archive_v1_state_machine.id}"
  log_name           = "/aws/lambda/poll_archive"

  high_urgency_action_arn = "${var.high_urgency_action_arn}"
  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
}

locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF
}
