resource "aws_iam_role" "pdms_user_deletion_v1_sfn" {
  name = "pdms_user_deletion_v1"

  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "pdms_user_deletion_v1_sfn_lambda_execution"
  role   = "${aws_iam_role.pdms_user_deletion_v1_sfn.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}

resource "aws_sfn_state_machine" "pdms_user_deletion_v1_sfn_state_machine" {
  name     = "pdms_user_deletion_v1_state_machine"
  role_arn = "${aws_iam_role.pdms_user_deletion_v1_sfn.arn}"

  definition = <<EOF
  {
    "Comment": "Handles Eventbus UserDestroy event",
    "StartAt": "ParallelDeletes",
    "States": {
      "ParallelDeletes": {
        "Type": "Parallel",
        "Next": "ReportDeletion",
        "ResultPath": "$.parallelOutput",
        "Branches": [
          ${local.pdms_delete_user_leaderboard_entries_step},
          ${local.pdms_delete_user_votes_step}
        ],
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }]
      },
      "ReportDeletion": {
        "Type": "Task",
        "End": true,
        "Resource": "${var.pdms_report_deletion_lambda_arn}",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }],
        "Retry": ${local.default_retry}
      },
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      }
    }
  }
EOF
}

locals {
  pdms_delete_user_leaderboard_entries_step = <<EOF
  {
    "StartAt": "DeleteUserLeaderboardEntries",
    "States": {
      "DeleteUserLeaderboardEntries": {
        "Comment": "Deleting user entries from Poll leaderboards",
        "Type": "Task",
        "Resource": "${var.pdms_delete_user_leaderboard_entries_lambda_arn}",
        "ResultPath": "$.deletedUserLeaderboardEntries",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserLeaderboardEntriesFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserLeaderboardEntriesFailState": {
        "Comment": "Failed to delete user entries from Poll leaderboards",
        "Type": "Fail"
      }
    }
  }
EOF

  pdms_delete_user_votes_step = <<EOF
  {
    "StartAt": "DeleteUserVotes",
    "States": {
      "DeleteUserVotes": {
        "Comment": "Deleting user votes from RDS",
        "Type": "Task",
        "Resource": "${var.pdms_delete_user_votes_lambda_arn}",
        "ResultPath": "$.deletedUserVotes",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserVotesFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserVotesFailState": {
        "Comment": "Failed to delete user votes from RDS",
        "Type": "Fail"
      }
    }
  }
EOF
}

module "pdms_user_deletion_v1_alarms" {
  source = "../../modules/cloudwatch/cloudwatch_sfn_alarms"

  env                = "${var.env}"
  state_machine_name = "${aws_sfn_state_machine.pdms_user_deletion_v1_sfn_state_machine.name}"
  state_machine_arn  = "${aws_sfn_state_machine.pdms_user_deletion_v1_sfn_state_machine.id}"
  log_name           = "/aws/lambda/pdms_user_deletion"

  high_urgency_action_arn = "${var.high_urgency_action_arn}"
  low_urgency_action_arn  = "${var.low_urgency_action_arn}"
}

locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF
}
