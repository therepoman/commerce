variable "region" {
  description = "The region where AWS resources live"
}

variable "pdms_delete_user_leaderboard_entries_lambda_arn" {
  description = "The ARN of the PDMS Delete User Leaderboard Entries lambda"
}

variable "pdms_delete_user_votes_lambda_arn" {
  description = "The ARN of the PDMS Delete User Votes lambda"
}

variable "pdms_report_deletion_lambda_arn" {
  description = "The ARN of the PDMS Report Deletion lambda"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
