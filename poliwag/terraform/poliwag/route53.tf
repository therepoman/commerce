resource "aws_route53_zone" "internal_justin_tv" {
  name    = "internal.justin.tv"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

output "route_53_internal_justin_tv_zone" {
  value = "${aws_route53_zone.internal_justin_tv.id}"
}

resource "aws_route53_zone" "uswest2_justin_tv" {
  name    = "us-west2.justin.tv"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

output "route_53_uswest2_justin_tv_zone" {
  value = "${aws_route53_zone.uswest2_justin_tv.id}"
}

resource "aws_route53_zone" "uswest2_twitch_tv" {
  name    = "us-west2.twitch.tv"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

output "route_53_uswest2_twitch_tv_zone" {
  value = "${aws_route53_zone.uswest2_twitch_tv.id}"
}

resource "aws_route53_zone" "internal_twitch_tv" {
  name    = "internal.twitch.tv"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

output "route_53_internal_twitch_tv_zone" {
  value = "${aws_route53_zone.internal_twitch_tv.id}"
}

resource "aws_route53_zone" "copo_twitch_a2z_com" {
  name    = "copo.twitch.a2z.com"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

output "route_53_copo_twitch_a2z_com_zone" {
  value = "${aws_route53_zone.copo_twitch_a2z_com.id}"
}

resource "aws_route53_zone" "ripley_twitch_a2z_com" {
  name    = "ripley.twitch.a2z.com"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

output "route_53_ripley_twitch_a2z_com_zone" {
  value = "${aws_route53_zone.ripley_twitch_a2z_com.id}"
}
