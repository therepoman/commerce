resource "aws_iam_policy" "sqs_policy" {
  name        = "sqs_policy"
  description = "Read from SQS queues"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sqs:GetQueueAttributes",
        "sqs:ListQueues",
        "sqs:ReceiveMessage",
        "sqs:DeleteMessage"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:sqs:*:*:*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "sqs_policy_attachment" {
  name       = "sqs_policy_attachment"
  policy_arn = "${aws_iam_policy.sqs_policy.arn}"
  roles      = ["${module.service.service_role}"]
}

module "vote_pantheon_ingestion_queue" {
  source                     = "modules/queue"
  env                        = "${var.env}"
  queue_name                 = "vote-pantheon-ingestion"
  sns_topic                  = "${aws_sns_topic.vote_topic.arn}"
  visibility_timeout_seconds = 2
  high_urgency_action_arn    = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn     = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

resource "aws_sns_topic_subscription" "vote_pantheon_ingestion_sqs_target" {
  topic_arn = "${aws_sns_topic.vote_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.vote_pantheon_ingestion_queue.queue_arn}"
}

module "vote_poll_update_queue" {
  source                     = "modules/queue"
  env                        = "${var.env}"
  queue_name                 = "vote-poll-update"
  sns_topic                  = "${aws_sns_topic.vote_topic.arn}"
  visibility_timeout_seconds = 20
  max_receive_count          = 10
  high_urgency_action_arn    = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn     = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

resource "aws_sns_topic_subscription" "vote_poll_update_sqs_target" {
  topic_arn = "${aws_sns_topic.vote_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.vote_poll_update_queue.queue_arn}"
}

resource "aws_sqs_queue" "eventbus_user_deletion_queue" {
  name                       = "eventbus-poliwag-user-deletion-queue"
  policy                     = "${aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]}"
  kms_master_key_id          = "${aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]}"
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.eventbus_user_deletion_queue_dlq.arn}\",\"maxReceiveCount\":5}"
  message_retention_seconds  = 1209600
  visibility_timeout_seconds = 60
}

resource "aws_sqs_queue" "eventbus_user_deletion_queue_dlq" {
  name                      = "eventbus-poliwag-user-deletion-queue-dlq"
  message_retention_seconds = 1209600
  kms_master_key_id         = "${aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]}"
}

resource "aws_cloudwatch_metric_alarm" "pdms_dlq_low_watermark" {
  alarm_name          = "${aws_sqs_queue.eventbus_user_deletion_queue_dlq.name}-${var.env}-low-watermark"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${aws_sqs_queue.eventbus_user_deletion_queue_dlq.name}"
  }

  period             = "300"
  statistic          = "Average"
  threshold          = "0"
  alarm_description  = "PDMS DLQ has at least 1 message"
  treat_missing_data = "notBreaching"
}
