module "poliwag_cloudwatch" {
  source = "cloudwatch"

  env       = "${var.env}"
  service   = "Poliwag"
  namespace = "poliwag"
  log_group = "poliwag"

  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
}

module "alb_alarms" {
  source = "./modules/cloudwatch/cloudwatch_alb_alarms"

  alb_name                = "${element(split("/",module.service.alb_arn_suffix), 1)}"
  alb_arn_suffix          = "${module.service.alb_arn_suffix}"
  target_group_arn_suffix = "${element(split(":", module.service.alb_arn), 5)}"       # module.service.alb_arn is actually the target group arn

  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
}

locals {
  audit_logs_name    = "${var.name}-${var.env}-audit-logs"
  s2s_auth_logs_name = "${var.name}-${var.env}-s2s-auth-logs"
}

resource "aws_cloudwatch_log_group" "audit_logs" {
  name              = "${local.audit_logs_name}"
  retention_in_days = "${var.audit_logs_retention_in_days}"
  kms_key_id        = "${aws_kms_key.audit_logs.arn}"
}

resource "aws_cloudwatch_log_subscription_filter" "audit_log_funnel_subscription" {
  count           = "${var.env == "prod" ? 1 : 0}"
  name            = "${local.audit_logs_name}_subscription-filter"
  log_group_name  = "${local.audit_logs_name}"
  filter_pattern  = ""
  destination_arn = "${aws_cloudformation_stack.twitch_security_log_funnel.outputs["LogFunnelARN"]}"
  role_arn        = "${aws_cloudformation_stack.twitch_security_log_funnel.outputs["CWLRoleARN"]}"
}

resource "aws_cloudwatch_log_group" "s2s_auth_logs" {
  name              = "${local.s2s_auth_logs_name}"
  retention_in_days = "${var.s2s_auth_logs_rentention_in_days}"
  kms_key_id        = "${aws_kms_key.s2s_auth_logs_kms_key.arn}"
}

resource "aws_cloudwatch_log_subscription_filter" "s2s_auth_log_funnel_subscription" {
  count           = "${var.env == "prod" ? 1 : 0}"
  name            = "${local.s2s_auth_logs_name}_subscription-filter"
  log_group_name  = "${local.s2s_auth_logs_name}"
  filter_pattern  = ""
  destination_arn = "${aws_cloudformation_stack.twitch_security_log_funnel.outputs["LogFunnelARN"]}"
  role_arn        = "${aws_cloudformation_stack.twitch_security_log_funnel.outputs["CWLRoleARN"]}"
}
