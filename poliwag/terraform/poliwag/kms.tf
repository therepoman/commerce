// For SOX Audit Logs
resource "aws_kms_key" "audit_logs" {
  description         = "${var.name}-${var.env} cloudwatch audit logs encryption"
  enable_key_rotation = true

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.aws_account_id}:root",
          "arn:aws:iam::${var.aws_account_id}:role/admin"
        ]
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Enable CloudWatch Audit Logs Encryption",
      "Effect": "Allow",
      "Principal": {
        "Service": "logs.${var.region}.amazonaws.com"
      },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*",
      "Condition": {
          "ArnEquals": {
              "kms:EncryptionContext:aws:logs:arn": "arn:aws:logs:${var.region}:${var.aws_account_id}:log-group:${local.audit_logs_name}"
          }
      }
    }
  ]
}
EOF
}

resource "aws_kms_alias" "audit_logs" {
  name          = "alias/${var.name}-${var.env}/audit-logs"
  target_key_id = "${aws_kms_key.audit_logs.key_id}"
}

// For S2S Auth Logs
resource "aws_kms_key" "s2s_auth_logs_kms_key" {
  description         = "${var.name}-${var.env} cloudwatch s2s auth logs encryption"
  enable_key_rotation = true

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.aws_account_id}:root",
          "arn:aws:iam::${var.aws_account_id}:role/admin"
        ]
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Enable CloudWatch S2S Auth Logs Encryption",
      "Effect": "Allow",
      "Principal": {
        "Service": "logs.${var.region}.amazonaws.com"
      },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*",
      "Condition": {
          "ArnEquals": {
              "kms:EncryptionContext:aws:logs:arn": "arn:aws:logs:${var.region}:${var.aws_account_id}:log-group:${local.s2s_auth_logs_name}"
          }
      }
    }
  ]
}
EOF
}

resource "aws_kms_alias" "s2s_auth_logs_kms_alias" {
  name          = "alias/${var.name}-${var.env}/s2s-auth-logs"
  target_key_id = "${aws_kms_key.s2s_auth_logs_kms_key.key_id}"
}
