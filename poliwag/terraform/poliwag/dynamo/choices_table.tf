locals {
  choices_name     = "choices-${var.env}"
  choices_hash_key = "choice_id"

  choices_attributes = [
    {
      name = "choice_id"
      type = "S"
    },
    {
      name = "poll_id"
      type = "S"
    },
  ]

  poll_id_choice_id_index = "PollIDChoiceIDIndex"
}

resource "aws_dynamodb_table" "choices_primary_table" {
  name         = "${local.choices_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "${local.choices_hash_key}"
  attribute    = "${local.choices_attributes}"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  global_secondary_index {
    name            = "${local.poll_id_choice_id_index}"
    hash_key        = "poll_id"
    range_key       = "choice_id"
    projection_type = "ALL"
  }
}

module "choices_primary_table_scheduled_backups" {
  source                   = "../modules/dynamo/dynamo_backup"
  table_name               = "${local.choices_name}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda}"
  region                   = "${var.region}"
}

resource "aws_dynamodb_table" "choices_replica_table" {
  name         = "${local.choices_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "${local.choices_hash_key}"
  attribute    = "${local.choices_attributes}"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  global_secondary_index {
    name            = "${local.poll_id_choice_id_index}"
    hash_key        = "poll_id"
    range_key       = "choice_id"
    projection_type = "ALL"
  }

  provider = "aws.backup"
}

module "choices_replica_table_scheduled_backups" {
  source                   = "../modules/dynamo/dynamo_backup"
  table_name               = "${local.choices_name}"
  dynamo_backup_lambda_arn = "${var.replica_dynamo_backup_lambda}"
  region                   = "${var.backup_region}"

  providers = {
    aws = "aws.backup"
  }
}

resource "aws_dynamodb_global_table" "choices_gloabl_table" {
  depends_on = ["aws_dynamodb_table.choices_primary_table", "aws_dynamodb_table.choices_replica_table"]

  name = "${local.choices_name}"

  replica {
    region_name = "${var.region}"
  }

  replica {
    region_name = "${var.backup_region}"
  }
}
