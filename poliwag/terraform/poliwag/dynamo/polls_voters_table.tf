locals {
  polls_voters_name     = "polls_voters-${var.env}"
  polls_voters_hash_key = "poll_id-user_id"

  polls_voters_attributes = [
    {
      name = "poll_id-user_id"
      type = "S"
    },
  ]
}

resource "aws_dynamodb_table" "polls_voters_primary_table" {
  name         = "${local.polls_voters_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "${local.polls_voters_hash_key}"
  attribute    = "${local.polls_voters_attributes}"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

module "polls_voters_primary_table_scheduled_backups" {
  source                   = "../modules/dynamo/dynamo_backup"
  table_name               = "${local.polls_voters_name}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda}"
  region                   = "${var.region}"
}

resource "aws_dynamodb_table" "polls_voters_replica_table" {
  name         = "${local.polls_voters_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "${local.polls_voters_hash_key}"
  attribute    = "${local.polls_voters_attributes}"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  provider = "aws.backup"
}

module "polls_voters_replica_table_scheduled_backups" {
  source                   = "../modules/dynamo/dynamo_backup"
  table_name               = "${local.polls_voters_name}"
  dynamo_backup_lambda_arn = "${var.replica_dynamo_backup_lambda}"
  region                   = "${var.backup_region}"

  providers = {
    aws = "aws.backup"
  }
}

resource "aws_dynamodb_global_table" "polls_voters_gloabl_table" {
  depends_on = ["aws_dynamodb_table.polls_voters_primary_table", "aws_dynamodb_table.polls_voters_replica_table"]

  name = "${local.polls_voters_name}"

  replica {
    region_name = "${var.region}"
  }

  replica {
    region_name = "${var.backup_region}"
  }
}
