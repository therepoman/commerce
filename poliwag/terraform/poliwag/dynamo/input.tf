provider "aws" {
  alias = "backup"
}

variable "environment" {
  type        = "string"
  description = "The environment to run the service in (e.g, staging/production)"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  devo (development),  stage (staging), or  prod (production)."
}

variable "region" {
  default     = "us-west-2"
  description = "The AWS Region for the resources to live in"
}

variable "backup_region" {
  default     = "us-east-2"
  description = "The AWS Region for the resources to live in"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "dynamo_backup_lambda" {
  type = "string"
}

variable "replica_dynamo_backup_lambda" {
  type = "string"
}
