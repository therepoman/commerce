locals {
  polls_name     = "polls-${var.env}"
  polls_hash_key = "poll_id"

  polls_attributes = [
    {
      name = "poll_id"
      type = "S"
    },
    {
      name = "owned_by"
      type = "S"
    },
    {
      name = "start_time"
      type = "S"
    },
  ]

  owned_by_start_time_index = "OwnedByStartTimeIndex"
}

resource "aws_dynamodb_table" "polls_primary_table" {
  name         = "${local.polls_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "${local.polls_hash_key}"
  attribute    = "${local.polls_attributes}"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  global_secondary_index {
    name            = "${local.owned_by_start_time_index}"
    hash_key        = "owned_by"
    range_key       = "start_time"
    projection_type = "ALL"
  }
}

module "polls_primary_table_scheduled_backups" {
  source                   = "../modules/dynamo/dynamo_backup"
  table_name               = "${local.polls_name}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda}"
  region                   = "${var.region}"
}

resource "aws_dynamodb_table" "polls_replica_table" {
  name         = "${local.polls_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "${local.polls_hash_key}"
  attribute    = "${local.polls_attributes}"

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  global_secondary_index {
    name            = "${local.owned_by_start_time_index}"
    hash_key        = "owned_by"
    range_key       = "start_time"
    projection_type = "ALL"
  }

  provider = "aws.backup"
}

module "polls_replica_table_scheduled_backups" {
  source                   = "../modules/dynamo/dynamo_backup"
  table_name               = "${local.polls_name}"
  dynamo_backup_lambda_arn = "${var.replica_dynamo_backup_lambda}"
  region                   = "${var.backup_region}"

  providers = {
    aws = "aws.backup"
  }
}

resource "aws_dynamodb_global_table" "polls_global_table" {
  depends_on = ["aws_dynamodb_table.polls_primary_table", "aws_dynamodb_table.polls_replica_table"]

  name = "${local.polls_name}"

  replica {
    region_name = "${var.region}"
  }

  replica {
    region_name = "${var.backup_region}"
  }
}
