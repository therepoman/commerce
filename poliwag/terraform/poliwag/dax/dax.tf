resource "aws_dax_cluster" "poliwag_dax_cluster_v2" {
  cluster_name       = "dax-v2-${var.env}"
  iam_role_arn       = "${aws_iam_role.dax_role.arn}"
  node_type          = "${var.instance_type}"
  replication_factor = 5

  security_group_ids = ["${aws_security_group.poliwag_dax_security_group.id}"]
  subnet_group_name  = "${aws_dax_subnet_group.poliwag_dax_subnet_group.name}"
}

resource "aws_dax_subnet_group" "poliwag_dax_subnet_group" {
  name       = "poliwag-${var.env}-dax"
  subnet_ids = ["${split(",", var.subnets)}"]
}

resource "aws_security_group" "poliwag_dax_security_group" {
  name        = "poliwag-${var.env}-dax"
  vpc_id      = "${var.vpc_id}"
  description = "Allows communication with DAX server"

  ingress {
    from_port       = "${var.port}"
    to_port         = "${var.port}"
    protocol        = "TCP"
    security_groups = ["${var.security_group}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "poliwag-${var.env}-dax"
  }
}

data "aws_iam_policy_document" "policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["dax.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "dax_role" {
  name               = "poliwag-${var.env}-dax-role"
  assume_role_policy = "${data.aws_iam_policy_document.policy.json}"
}

resource "aws_iam_role_policy_attachment" "dynamo_full_access_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = "${aws_iam_role.dax_role.name}"
}
