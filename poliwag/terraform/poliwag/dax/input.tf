variable "env" {
  description = "The AWS environment. Must begin with one of the following:  devo (development),  stage (staging), or  prod (production)."
}

variable "security_group" {
  type = "string"
}

variable "instance_type" {
  description = "Describes the type of instance to be used in our DAX cluster"
}

variable "port" {
  description = "The port used by the dax server"
}

variable "vpc_id" {
  type = "string"
}

variable "subnets" {
  type = "string"
}
