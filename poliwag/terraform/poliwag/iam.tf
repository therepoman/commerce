// Grant the Instance Profile full dynamo access
resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// Grant the Instance Profile full Step Function access
resource "aws_iam_role_policy_attachment" "step_function_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

resource "aws_iam_role_policy_attachment" "step_function_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

// Sandstorm policy enables sandstorm by granting the ability to assume a specifc role
resource "aws_iam_policy" "sandstorm_policy" {
  name = "sandstorm-assume-role-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/${var.sandstorm_role}"
      ],
      "Effect": "Allow"
    }
  ]
}
EOF
}

// Grant the Instance Profile Sandstorm access
resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
}

// Jenkins user
resource "aws_iam_user" "tcs_user" {
  name = "poliwag-${var.env}-tcs-user"
}

resource "aws_iam_access_key" "tcs_user_key" {
  user = "${aws_iam_user.tcs_user.name}"
}

resource "aws_iam_user_policy" "tcs_user_beanstalk_user_policy" {
  name = "beanstalk"
  user = "${aws_iam_user.tcs_user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Effect": "Allow",
          "Action": [
              "elasticbeanstalk:*",
              "ec2:*",
              "ecs:*",
              "elasticloadbalancing:*",
              "autoscaling:*",
              "cloudwatch:*",
              "dynamodb:*",
              "s3:*",
              "sns:*",
              "cloudformation:*",
              "rds:*",
              "sqs:*",
              "iam:GetPolicyVersion",
              "iam:GetRole",
              "iam:PassRole",
              "iam:ListRolePolicies",
              "iam:ListAttachedRolePolicies",
              "iam:ListInstanceProfiles",
              "iam:ListRoles",
              "iam:ListServerCertificates",
              "acm:DescribeCertificate",
              "acm:ListCertificates",
              "logs:CreateLogGroup",
              "logs:PutRetentionPolicy"
          ],
          "Resource": "*"
      },
      {
          "Effect": "Allow",
          "Action": [
              "iam:AddRoleToInstanceProfile",
              "iam:CreateInstanceProfile",
              "iam:CreateRole"
          ],
          "Resource": [
              "arn:aws:iam::*:role/aws-elasticbeanstalk*",
              "arn:aws:iam::*:instance-profile/aws-elasticbeanstalk*"
          ]
      },
      {
          "Effect": "Allow",
          "Action": [
              "iam:AttachRolePolicy"
          ],
          "Resource": "*",
          "Condition": {
              "StringLike": {
                  "iam:PolicyArn": [
                      "arn:aws:iam::aws:policy/AWSElasticBeanstalk*",
                      "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalk*"
                  ]
              }
          }
      }
  ]
}
EOF
}

resource "aws_iam_user_policy" "tcs_user_ecr_user_policy" {
  name = "ecr"
  user = "${aws_iam_user.tcs_user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": "ecr:*",
          "Resource": "*"
      }
  ]
}
EOF
}

resource "aws_iam_user_policy" "tcs_user_ecs_user_policy" {
  name = "ecs"
  user = "${aws_iam_user.tcs_user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": "ecs:*",
          "Resource": "*"
      }
  ]
}
EOF
}
