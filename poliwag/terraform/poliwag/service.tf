module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=37fa4f68f54fc1f638947cec9c029b33d2b63999"

  name          = "${var.name}"
  image         = "${aws_ecr_repository.repository.repository_url}"
  memory        = "${var.memory}"
  canary_memory = "${var.canary_memory}"
  logging       = "${var.logging}"
  cpu           = "${var.cpu}"
  canary_cpu    = "${var.canary_cpu}"
  alb_port      = "${var.alb_port}"
  env_vars      = "${var.env_vars}"
  command       = "${var.command}"

  counts = {
    min = "${var.min_tasks}"
    max = "${var.max_tasks}"
  }

  port = 8000
  dns  = "*.${var.env}.${var.name}.internal.justin.tv"

  secondary_dns = "${module.privatelink-cert.dns}"

  healthcheck              = "/ping"
  cluster                  = "poliwag-cluster"
  security_group           = "${var.security_group}"
  region                   = "${var.region}"
  vpc_id                   = "${var.vpc_id}"
  environment              = "${var.environment}"
  subnets                  = "${var.subnets}"
  launch_type              = "FARGATE"
  fargate_platform_version = "1.3.0"
}

// For S2S2
resource "aws_iam_policy" "s2s2_iam_policy" {
  name        = "${var.name}_${var.env}_s2s2_iam_policy"
  description = "required IAM permission to use S2S2"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "execute-api:Invoke"
            ],
            "Resource": [
                "arn:aws:execute-api:*:985585625942:*"
            ],
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_service_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "${aws_iam_policy.s2s2_iam_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_canary_attachment" {
  role       = "${module.service.canary_role}"
  policy_arn = "${aws_iam_policy.s2s2_iam_policy.arn}"
}
