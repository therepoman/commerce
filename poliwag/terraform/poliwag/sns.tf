resource "aws_sns_topic" "alarms_topic" {
  name = "${var.name}-${var.env}-alarms"
}

resource "aws_sns_topic" "low_urgency_alarms_topic" {
  name = "${var.name}-${var.env}-low-urgency-alarms"
}

resource "aws_sns_topic" "vote_topic" {
  name = "vote-${var.env}"
}

data "aws_iam_policy_document" "vote_topic_policy_document" {
  statement {
    actions = ["sns:Subscribe", "sns:Publish"]
    effect  = "Allow"

    principals {
      identifiers = ["arn:aws:iam::${var.aws_profile}:root"]
      type        = "AWS"
    }

    resources = ["${aws_sns_topic.vote_topic.arn}"]
  }
}
