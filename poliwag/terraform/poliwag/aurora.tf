module "aurora" {
  source = "aurora"

  name            = "poliwag-db-${var.environment}"
  database_name   = "poliwag"
  environment     = "${var.environment}"
  port            = "3306"
  master_username = "admin"
  vpc_id          = "${var.vpc_id}"
  security_group  = "${var.security_group}"
  subnets         = "${var.subnets}"
  min_capacity    = "${var.min_aurora_capacity}"
  max_capacity    = "${var.max_aurora_capacity}"
}
