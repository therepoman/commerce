module "sfn" {
  source      = "sfn"
  env         = "${var.env}"
  aws_profile = "${var.aws_profile}"
}

module "poll_update_sfn" {
  source                 = "sfn/poll_update_v1"
  env                    = "${var.env}"
  region                 = "${var.region}"
  poll_update_lambda_arn = "${module.poll_update_lambda.lambda_arn}"

  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_complete_sfn" {
  source                   = "sfn/poll_complete_v1"
  env                      = "${var.env}"
  region                   = "${var.region}"
  poll_update_lambda_arn   = "${module.poll_update_lambda.lambda_arn}"
  poll_complete_lambda_arn = "${module.poll_complete_lambda.lambda_arn}"
  get_poll_lambda_arn      = "${module.get_poll_lambda.lambda_arn}"
  poll_archive_lambda_arn  = "${module.poll_archive_lambda.lambda_arn}"

  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_terminate_sfn" {
  source                    = "sfn/poll_terminate_v1"
  env                       = "${var.env}"
  region                    = "${var.region}"
  poll_update_lambda_arn    = "${module.poll_update_lambda.lambda_arn}"
  poll_terminate_lambda_arn = "${module.poll_terminate_lambda.lambda_arn}"
  get_poll_lambda_arn       = "${module.get_poll_lambda.lambda_arn}"
  poll_archive_lambda_arn   = "${module.poll_archive_lambda.lambda_arn}"

  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_archive_sfn" {
  source                  = "sfn/poll_archive_v1"
  env                     = "${var.env}"
  region                  = "${var.region}"
  poll_update_lambda_arn  = "${module.poll_update_lambda.lambda_arn}"
  poll_archive_lambda_arn = "${module.poll_archive_lambda.lambda_arn}"

  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "poll_moderate_sfn" {
  source                   = "sfn/poll_moderate_v1"
  env                      = "${var.env}"
  region                   = "${var.region}"
  poll_update_lambda_arn   = "${module.poll_update_lambda.lambda_arn}"
  poll_moderate_lambda_arn = "${module.poll_moderate_lambda.lambda_arn}"

  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}

module "pdms_user_deletion_sfn" {
  source                                          = "sfn/pdms_user_deletion_v1"
  env                                             = "${var.env}"
  region                                          = "${var.region}"
  pdms_delete_user_leaderboard_entries_lambda_arn = "${module.pdms_delete_user_leaderboard_entries_lambda.lambda_arn}"
  pdms_report_deletion_lambda_arn                 = "${module.pdms_report_deletion_lambda.lambda_arn}"
  pdms_delete_user_votes_lambda_arn               = "${module.pdms_delete_user_votes_lambda.lambda_arn}"

  high_urgency_action_arn = "${aws_sns_topic.alarms_topic.arn}"
  low_urgency_action_arn  = "${aws_sns_topic.low_urgency_alarms_topic.arn}"
}
