// TODO: Remove once all callers are on a2z
module "privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=099a2d57ba837699caeee47ec079b4f0e4028e6f"

  region           = "${var.region}"
  name             = "${var.name}"
  environment      = "${var.env}"
  alb_dns          = "${module.service.dns}"
  cert_arn         = "${module.service.cert}"
  vpc_id           = "${var.vpc_id}"
  subnets          = ["${split(",", var.subnets)}"]
  whitelisted_arns = "${var.whitelisted_arns_for_privatelink}"
}

module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name        = "poliwag"
  environment = "${var.environment}"
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=5ff49ed9d38812064501f06947e0cbf51044016f"

  name        = "poliwag"
  environment = "${var.environment}"
  zone_id     = "${module.privatelink-zone.zone_id}"
  alb_dns     = "${module.service.dns}"
}

module "privatelink_a2z" {
  source           = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name             = "poliwag-a2z"
  region           = "${var.region}"
  environment      = "${var.environment}"
  alb_dns          = "${module.service.dns}"
  cert_arn         = "${module.privatelink-cert.arn}"
  vpc_id           = "${var.vpc_id}"
  subnets          = "${split(",", var.subnets)}"
  whitelisted_arns = "${var.whitelisted_arns_for_privatelink}"
}
