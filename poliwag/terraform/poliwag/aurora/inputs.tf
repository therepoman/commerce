variable "name" {
  type        = "string"
  description = "The name of the RDS cluster"
}

variable "port" {
  type        = "string"
  description = "The port on which the DB accepts connections."
}

variable "database_name" {
  type        = "string"
  description = "Name for an automatically created database on cluster creation."
}

variable "master_username" {
  type        = "string"
  description = "Username for the master DB user."
}

variable "skip_final_snapshot" {
  type        = "string"
  description = "Determines whether a final DB snapshot is created before the DB cluster is deleted."
  default     = false
}

variable "backup_retention_period" {
  default = 14
}

variable "preferred_backup_window" {
  type        = "string"
  description = "The daily time range during which automated backups are created if automated backups are enabled."
  default     = "07:00-09:00"
}

variable "environment" {
  type        = "string"
  description = "The environment to run the service in (e.g, staging/production)"
}

variable "subnets" {}

variable "apply_immediately" {
  type        = "string"
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window."
  default     = false
}

variable "vpc_id" {
  type = "string"
}

variable "security_group" {
  type = "string"
}

variable "min_capacity" {
  default = 4
}

variable "max_capacity" {
  default = 16
}
