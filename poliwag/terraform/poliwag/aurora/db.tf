resource "aws_rds_cluster" "db" {
  cluster_identifier        = "${var.name}"
  engine_mode               = "serverless"
  port                      = "${var.port}"
  db_subnet_group_name      = "${aws_db_subnet_group.db.name}"
  database_name             = "${var.database_name}"
  skip_final_snapshot       = "${var.skip_final_snapshot}"
  backup_retention_period   = "${var.backup_retention_period}"
  preferred_backup_window   = "${var.preferred_backup_window}"
  vpc_security_group_ids    = ["${var.security_group}"]
  final_snapshot_identifier = "${var.name}-before-deletion"

  // IMPORTANT: change this password manually via the AWS console and store the new password in sandstorm
  // Staging: commerce/poliwag/staging/aurora-password
  // Prod: commerce/poliwag/production/aurora-password
  master_password = "change_me"

  master_username = "${var.master_username}"

  lifecycle {
    ignore_changes = [
      "engine_version",
      "master_password",
    ]
  }

  scaling_configuration {
    auto_pause   = false
    min_capacity = "${var.min_capacity}"
    max_capacity = "${var.max_capacity}"
  }

  tags {
    Environment = "${var.environment}"
  }
}

resource "aws_db_subnet_group" "db" {
  subnet_ids = ["${split(",",var.subnets)}"]
}

resource "aws_security_group" "main" {
  name        = "${var.name}-rds-cluster"
  description = "Allows traffic to rds from other security groups"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port       = "${var.port}"
    to_port         = "${var.port}"
    protocol        = "TCP"
    security_groups = ["${var.security_group}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "RDS cluster (${var.name})"
    Environment = "${var.environment}"
  }
}
