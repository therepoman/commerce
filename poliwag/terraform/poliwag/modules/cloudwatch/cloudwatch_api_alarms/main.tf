variable "service" {
  type        = "string"
  description = "The service the operation metrics are under"
}

variable "stage" {
  type        = "string"
  description = "The stage to look at for metrics"
}

variable "substage" {
  type        = "string"
  description = "The substage to look at for metrics"
}

variable "operation" {
  type        = "string"
  description = "The operation to look at for metrics"
}

variable "latency_threshold" {
  type        = "string"
  default     = "0"
  description = "The latency threshold in seconds, latency alarms won't be created if this isn't set"
}

variable "latency_p999_eval_periods" {
  type        = "string"
  default     = "30"
  description = "The number of evaluation periods for the p99.9 latency alarm"
}

variable "latency_p99_eval_periods" {
  type        = "string"
  default     = "10"
  description = "The number of evaluation periods for the p99 latency alarm"
}

variable "latency_p90_eval_periods" {
  type        = "string"
  default     = "5"
  description = "The number of evaluation periods for the p90 latency alarm"
}

variable "latency_low_urgency_p999_eval_periods" {
  type        = "string"
  default     = "10"
  description = "The number of evaluation periods for the low urgency p99.9 latency alarm"
}

variable "server_error_01_percent_eval_periods" {
  type        = "string"
  default     = "30"
  description = "The number of evaluation periods for the 0.1% server error alarm"
}

variable "server_error_1_percent_eval_periods" {
  type        = "string"
  default     = "10"
  description = "The number of evaluation periods for the 1% server error alarm"
}

variable "server_error_10_percent_eval_periods" {
  type        = "string"
  default     = "5"
  description = "The number of evaluation periods for the 10% server error alarm"
}

variable "server_error_low_urgency_01_percent_eval_periods" {
  type        = "string"
  default     = "10"
  description = "The number of evaluation periods for the low urgency 0.1% server error alarm"
}

variable "client_error_threshold" {
  type        = "string"
  default     = "0.1"                                #0.1%
  description = "The client error threshold percent"
}

variable "client_error_eval_periods" {
  type        = "string"
  default     = "10"
  description = "The number of evaluation periods for the client error alarm"
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}

data "aws_region" "current_region" {}

resource "aws_cloudwatch_metric_alarm" "latency_p999_alarm" {
  count               = "${var.latency_threshold == 0 ? 0 : 1}"
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-p999-latency-alarm"
  alarm_description   = "Alerts on ${var.operation} p99.9 latency >= ${var.latency_threshold}s for ${var.latency_p999_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.latency_p999_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "Duration"
  period              = "60"
  extended_statistic  = "p99.9"
  threshold           = "${var.latency_threshold}"
  unit                = "Seconds"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "latency_p99_alarm" {
  count               = "${var.latency_threshold == 0 ? 0 : 1}"
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-p99-latency-alarm"
  alarm_description   = "Alerts on ${var.operation} p99 latency >= ${var.latency_threshold}s for ${var.latency_p99_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.latency_p99_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "Duration"
  period              = "60"
  extended_statistic  = "p99"
  threshold           = "${var.latency_threshold}"
  unit                = "Seconds"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "latency_p90_alarm" {
  count               = "${var.latency_threshold == 0 ? 0 : 1}"
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-p90-latency-alarm"
  alarm_description   = "Alerts on ${var.operation} p90 latency >= ${var.latency_threshold}s for ${var.latency_p90_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.latency_p90_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "Duration"
  period              = "60"
  extended_statistic  = "p90"
  threshold           = "${var.latency_threshold}"
  unit                = "Seconds"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "latency_low_urgency_p999_alarm" {
  count               = "${var.latency_threshold == 0 ? 0 : 1}"
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-low-urgency-p999-latency-alarm"
  alarm_description   = "Alerts on ${var.operation} p99.9 latency >= ${var.latency_threshold}s for ${var.latency_low_urgency_p999_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.latency_low_urgency_p999_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "Duration"
  period              = "60"
  extended_statistic  = "p99.9"
  threshold           = "${var.latency_threshold}"
  unit                = "Seconds"
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.low_urgency_action_arn}"]
  ok_actions          = ["${var.low_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "server_error_01_percent_alarm" {
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-01-percent-server-error-alarm"
  alarm_description   = "Alerts on ${var.operation} server errors >= 0.1% for ${var.server_error_01_percent_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.server_error_01_percent_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "ServerError"
  period              = "60"
  statistic           = "Average"
  threshold           = "0.001"                                                                                                                                                #0.1%
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "server_error_1_percent_alarm" {
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-1-percent-server-error-alarm"
  alarm_description   = "Alerts on ${var.operation} server errors >= 1% for ${var.server_error_1_percent_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.server_error_1_percent_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "ServerError"
  period              = "60"
  statistic           = "Average"
  threshold           = "0.01"                                                                                                                                              #1%
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "server_error_10_percent_alarm" {
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-10-percent-server-error-alarm"
  alarm_description   = "Alerts on ${var.operation} server errors >= 10% for ${var.server_error_10_percent_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.server_error_10_percent_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "ServerError"
  period              = "60"
  statistic           = "Average"
  threshold           = "0.1"                                                                                                                                                 #10%
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.high_urgency_action_arn}"]
  ok_actions          = ["${var.high_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "server_error_low_urgency_01_percent_alarm" {
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-low-urgency-01-percent-server-error-alarm"
  alarm_description   = "Alerts on ${var.operation} server errors >= 0.1% for ${var.server_error_low_urgency_01_percent_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.server_error_low_urgency_01_percent_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "ServerError"
  period              = "60"
  statistic           = "Average"
  threshold           = "0.001"                                                                                                                                                            #0.1%
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.low_urgency_action_arn}"]
  ok_actions          = ["${var.low_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}

resource "aws_cloudwatch_metric_alarm" "client_error_alarm" {
  alarm_name          = "${var.service}-${var.stage}-${var.substage}-${var.operation}-client-error-alarm"
  alarm_description   = "Alerts on ${var.operation} client errors >= ${var.client_error_threshold}% for ${var.client_error_eval_periods}m in stage ${var.stage} and substage ${var.substage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.client_error_eval_periods}"
  namespace           = "${var.service}"
  metric_name         = "ClientError"
  period              = "60"
  statistic           = "Average"
  threshold           = "${__builtin_StringToFloat(var.client_error_threshold) / 100}"                                                                                                        // hack to interpolate string as float in terraform 0.11
  treat_missing_data  = "notBreaching"
  alarm_actions       = ["${var.low_urgency_action_arn}"]
  ok_actions          = ["${var.low_urgency_action_arn}"]

  dimensions = {
    Region    = "${data.aws_region.current_region.name}"
    Service   = "${var.service}"
    Stage     = "${var.stage}"
    Substage  = "${var.substage}"
    Operation = "${var.operation}"
  }
}
