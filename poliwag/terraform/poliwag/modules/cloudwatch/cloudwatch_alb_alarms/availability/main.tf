locals {
  threshold_label = "${var.threshold == "99.9" ? "999" : var.threshold}"
}

resource "aws_cloudwatch_metric_alarm" "alb_availability" {
  alarm_name          = "${var.alb_name}-alb-${var.name_prefix}${local.threshold_label}percent-availability-alarm"
  alarm_description   = "Alerts on availability <= ${var.threshold}% for ${var.evaluation_periods}m for ALB ${var.alb_name}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "${var.evaluation_periods}"

  metric_query {
    id          = "availability"
    expression  = "(1 - ((backend_5xx + elb_5xx) / (request_count + elb_5xx))) * 100"
    return_data = "true"
  }

  metric_query {
    id = "backend_5xx"

    metric {
      namespace   = "AWS/ApplicationELB"
      metric_name = "HTTPCode_Target_5XX_Count"

      dimensions = {
        LoadBalancer = "${var.alb_arn_suffix}"
      }

      period = "60"
      stat   = "Sum"
    }
  }

  metric_query {
    id = "elb_5xx"

    metric {
      namespace   = "AWS/ApplicationELB"
      metric_name = "HTTPCode_ELB_5XX_Count"

      dimensions = {
        LoadBalancer = "${var.alb_arn_suffix}"
      }

      period = "60"
      stat   = "Sum"
    }
  }

  metric_query {
    id = "request_count"

    metric {
      namespace   = "AWS/ApplicationELB"
      metric_name = "RequestCount"

      dimensions = {
        LoadBalancer = "${var.alb_arn_suffix}"
      }

      period = "60"
      stat   = "Sum"
    }
  }

  threshold          = "${var.threshold}"
  treat_missing_data = "notBreaching"

  alarm_actions = ["${var.action_arn}"]
  ok_actions    = ["${var.action_arn}"]
}
