variable "alb_name" {
  type        = "string"
  description = "The name of the ALB"
}

variable "alb_arn_suffix" {
  type        = "string"
  description = "The arn suffix of the ALB used for cloudwatch metrics"
}

variable "name_prefix" {
  type        = "string"
  description = "A prefix to add to the alarm name for uniqueness"
  default     = ""
}

variable "evaluation_periods" {
  type        = "string"
  description = "The number of evaluation periods for the latency alarm"
}

variable "threshold" {
  type        = "string"
  description = "The threshold in seconds of the latency alarm (eg. 0.5 for 500ms)"
}

variable "action_arn" {
  type = "string"
}
