module "alb_999_percent_availability_alarm" {
  source             = "./availability"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.p999_availability_eval_periods}"
  threshold          = "99.9"
  action_arn         = "${var.high_urgency_action_arn}"
}

module "alb_99_percent_availability_alarm" {
  source             = "./availability"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.p99_availability_eval_periods}"
  threshold          = "99"
  action_arn         = "${var.high_urgency_action_arn}"
}

module "alb_90_percent_availability_alarm" {
  source             = "./availability"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.p90_availability_eval_periods}"
  threshold          = "90"
  action_arn         = "${var.high_urgency_action_arn}"
}

module "alb_low_urgency_999_percent_availability_alarm" {
  source             = "./availability"
  name_prefix        = "low-urgency-"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.low_urgency_p999_availability_eval_periods}"
  threshold          = "99.9"
  action_arn         = "${var.low_urgency_action_arn}"
}

module "alb_p999_latency_alarm" {
  source             = "./latency"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.p999_latency_eval_periods}"
  threshold          = "${var.latency_threshold}"
  extended_statistic = "p99.9"
  action_arn         = "${var.high_urgency_action_arn}"
}

module "alb_p99_latency_alarm" {
  source             = "./latency"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.p99_latency_eval_periods}"
  threshold          = "${var.latency_threshold}"
  extended_statistic = "p99"
  action_arn         = "${var.high_urgency_action_arn}"
}

module "alb_p90_latency_alarm" {
  source             = "./latency"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.p90_latency_eval_periods}"
  threshold          = "${var.latency_threshold}"
  extended_statistic = "p90"
  action_arn         = "${var.high_urgency_action_arn}"
}

module "alb_low_urgency_p999_latency_alarm" {
  source             = "./latency"
  name_prefix        = "low-urgency-"
  alb_name           = "${var.alb_name}"
  alb_arn_suffix     = "${var.alb_arn_suffix}"
  evaluation_periods = "${var.low_urgency_p999_latency_eval_periods}"
  threshold          = "${var.latency_threshold}"
  extended_statistic = "p99.9"
  action_arn         = "${var.low_urgency_action_arn}"
}

resource "aws_cloudwatch_metric_alarm" "alb_elb_5xx_alarm" {
  alarm_name          = "${var.alb_name}-alb-elb-5xx-alarm"
  alarm_description   = "Alerts on ELB 5xxs >= ${var.elb_5xx_threshold} for ${var.elb_5xx_eval_periods}m for ALB ${var.alb_name}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.elb_5xx_eval_periods}"
  metric_name         = "HTTPCode_ELB_5XX_Count"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Sum"
  threshold           = "${var.elb_5xx_threshold}"
  treat_missing_data  = "notBreaching"

  dimensions = {
    LoadBalancer = "${var.alb_arn_suffix}"
  }

  alarm_actions = ["${var.low_urgency_action_arn}"]
  ok_actions    = ["${var.low_urgency_action_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "alb_rejected_connections_alarm" {
  alarm_name          = "${var.alb_name}-alb-rejected-connection-alarm"
  alarm_description   = "Alerts on non zero ELB rejected connections for ${var.rejected_connections_eval_periods}m ALB ${var.alb_name}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.rejected_connections_eval_periods}"
  metric_name         = "RejectedConnectionCount"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"

  dimensions = {
    LoadBalancer = "${var.alb_arn_suffix}"
  }

  alarm_actions = ["${var.low_urgency_action_arn}"]
  ok_actions    = ["${var.low_urgency_action_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "alb_unhealthy_host_alarm" {
  alarm_name          = "${var.alb_name}-alb-unhealthy-host-alarm"
  alarm_description   = "Alerts on unhealthy hosts for ${var.unhealthy_host_eval_periods}m ALB ${var.alb_name}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.unhealthy_host_eval_periods}"
  metric_name         = "UnHealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"

  dimensions = {
    LoadBalancer = "${var.alb_arn_suffix}"
    TargetGroup  = "${var.target_group_arn_suffix}"
  }

  alarm_actions = ["${var.low_urgency_action_arn}"]
  ok_actions    = ["${var.low_urgency_action_arn}"]
}
