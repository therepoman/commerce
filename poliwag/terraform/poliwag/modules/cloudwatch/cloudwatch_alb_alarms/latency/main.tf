locals {
  extended_statistic_label = "${var.extended_statistic == "p99.9" ? "p999" : (var.extended_statistic == "p99" ? "p99" : "p90")}"
}

resource "aws_cloudwatch_metric_alarm" "alb_latency" {
  alarm_name          = "${var.alb_name}-alb-${var.name_prefix}${local.extended_statistic_label}-latency-alarm"
  alarm_description   = "Alerts on ${var.extended_statistic} latency >= ${var.threshold}s for ${var.evaluation_periods}m for ALB ${var.alb_name}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.evaluation_periods}"
  metric_name         = "TargetResponseTime"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  extended_statistic  = "${var.extended_statistic}"
  threshold           = "${var.threshold}"
  treat_missing_data  = "notBreaching"

  dimensions = {
    LoadBalancer = "${var.alb_arn_suffix}"
  }

  alarm_actions = ["${var.action_arn}"]
  ok_actions    = ["${var.action_arn}"]
}
