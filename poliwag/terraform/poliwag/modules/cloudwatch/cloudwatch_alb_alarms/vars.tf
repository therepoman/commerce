variable "alb_name" {
  type        = "string"
  description = "The name of the ALB"
}

variable "alb_arn_suffix" {
  type        = "string"
  description = "The arn suffix of the ALB used for cloudwatch metrics"
}

variable "target_group_arn_suffix" {
  type        = "string"
  description = "The arn suffix of the target group used for cloudwatch metrics"
}

variable "latency_threshold" {
  type        = "string"
  description = "The latency threshold in seconds"
  default     = "0.5"                              #500ms
}

variable "p999_availability_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the 99.9% availability alarm"
  default     = "30"
}

variable "p99_availability_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the 99% availability alarm"
  default     = "10"
}

variable "p90_availability_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the 90% availability alarm"
  default     = "5"
}

variable "low_urgency_p999_availability_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the low urgency 99.9% availability alarm"
  default     = "5"
}

variable "p999_latency_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the 99.9% latency alarm"
  default     = "30"
}

variable "p99_latency_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the 99% latency alarm"
  default     = "10"
}

variable "p90_latency_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the 90% latency alarm"
  default     = "5"
}

variable "low_urgency_p999_latency_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the low urgency 99.9% latency alarm"
  default     = "5"
}

variable "elb_5xx_threshold" {
  type        = "string"
  description = "The threshold for the ELB 5XX alarm"
  default     = "100"
}

variable "elb_5xx_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the ELB 5XX alarm"
  default     = "3"
}

variable "rejected_connections_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the rejected connections alarm"
  default     = "3"
}

variable "unhealthy_host_eval_periods" {
  type        = "string"
  description = "The number of evaluation periods for the unhealthy host alarm"
  default     = "15"
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}
