variable "state_machine_name" {
  type = "string"
}

variable "state_machine_arn" {
  type = "string"
}

variable "env" {
  type = "string"
}

variable "log_name" {
  type = "string"
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}

resource "aws_cloudwatch_metric_alarm" "state_function_executions_throttled_alarm" {
  alarm_name                = "poliwag-${var.env}-${var.state_machine_name}-executions-throttled-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/States"
  metric_name               = "ExecutionsThrottled"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.state_machine_name} executions throttled"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  alarm_actions             = ["${var.high_urgency_action_arn}"]
  ok_actions                = ["${var.high_urgency_action_arn}"]

  dimensions = {
    StateMachineArn = "${var.state_machine_arn}"
  }
}

resource "aws_cloudwatch_metric_alarm" "state_function_executions_failed_alarm" {
  alarm_name                = "poliwag-${var.env}-${var.state_machine_name}-executions-failed-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/States"
  metric_name               = "ExecutionsFailed"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.state_machine_name} executions failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  alarm_actions             = ["${var.high_urgency_action_arn}"]
  ok_actions                = ["${var.high_urgency_action_arn}"]

  dimensions = {
    StateMachineArn = "${var.state_machine_arn}"
  }
}
