variable "queue_name" {
  type = "string"
}

variable "env" {
  type = "string"
}

variable "sns_topic" {
  type = "string"
}

variable "visibility_timeout_seconds" {
  type    = "string"
  default = 30
}

variable "max_receive_count" {
  default = 4
}

variable "high_urgency_action_arn" {
  type = "string"
}

variable "low_urgency_action_arn" {
  type = "string"
}

resource "aws_sqs_queue" "queue" {
  name                       = "${var.queue_name}-${var.env}"
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.deadletter_queue.arn}\",\"maxReceiveCount\":${var.max_receive_count}}"
  visibility_timeout_seconds = "${var.visibility_timeout_seconds}"
}

resource "aws_sqs_queue" "deadletter_queue" {
  name = "${var.queue_name}-dlq-${var.env}"
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = "${aws_sqs_queue.queue.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "${var.queue_name}_sqs_policy",
  "Statement": [
    {
      "Sid": "AllowSNSTopicToPublishTo_${var.queue_name}",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "${aws_sqs_queue.queue.arn}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${var.sns_topic}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_cloudwatch_metric_alarm" "queue_empty_receives" {
  alarm_name          = "${var.queue_name}-empty-recieves"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "3"
  metric_name         = "NumberOfEmptyReceives"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${aws_sqs_queue.queue.name}"
  }

  period            = "300"
  statistic         = "Average"
  threshold         = "0.5"
  alarm_description = "This metric monitors health of the queue. If it drops to 0 it would indicate that the service has stopped polling or is overwhelmed with messages"
  alarm_actions     = ["${var.high_urgency_action_arn}"]
  ok_actions        = ["${var.high_urgency_action_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "queue_dlq_low_watermark" {
  alarm_name          = "${var.queue_name}-dlq-low-watermark"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${aws_sqs_queue.deadletter_queue.name}"
  }

  period             = "300"
  statistic          = "Average"
  threshold          = "1"
  alarm_description  = "This metric monitors the queue's dlq. It will alarm if there is a message in that queue"
  treat_missing_data = "notBreaching"
  alarm_actions      = ["${var.low_urgency_action_arn}"]
  ok_actions         = ["${var.low_urgency_action_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "queue_dlq_high_watermark" {
  alarm_name          = "${var.queue_name}-dlq-high-watermark"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${aws_sqs_queue.deadletter_queue.name}"
  }

  period             = "300"
  statistic          = "Average"
  threshold          = "50"
  alarm_description  = "This metric monitors the queue's dlq. It will alarm if there are many messages in that queue"
  treat_missing_data = "notBreaching"
  alarm_actions      = ["${var.high_urgency_action_arn}"]
  ok_actions         = ["${var.high_urgency_action_arn}"]
}

output "queue_arn" {
  value = "${aws_sqs_queue.queue.arn}"
}
