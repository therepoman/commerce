variable "vpc_id" {
  description = "The id of the VPC for this service."
}

variable "subnets" {
  description = "Private subnets associated with this service's VPC."
  type        = "list"
}

variable "peer_account_alias" {
  description = "The alias of the account containing the VPC with which to peer."
}

variable "peer_account_id" {
  description = "The id of the account containing the VPC with which to peer."
}

variable "peer_cidr_block" {
  description = "The CIDR block specifying which IP traffic will be routed to the peered VPC."
}

variable "peer_vpc_id" {
  description = "The id of the VPC which which to peer."
}
