resource "aws_vpc_peering_connection" "peer" {
  vpc_id = "${var.vpc_id}"

  peer_vpc_id   = "${var.peer_vpc_id}"
  peer_owner_id = "${var.peer_account_id}"

  tags {
    Name = "${var.peer_account_alias}"
  }
}

data "aws_route_tables" "private_subnet_rts" {
  vpc_id = "${var.vpc_id}"

  filter {
    name   = "association.subnet-id"
    values = ["${var.subnets}"]
  }
}

resource "aws_route" "private_subnet_to_dest_account" {
  count                     = "${length(data.aws_route_tables.private_subnet_rts.ids)}"
  route_table_id            = "${data.aws_route_tables.private_subnet_rts.ids[count.index]}"
  destination_cidr_block    = "${var.peer_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peer.id}"
}
