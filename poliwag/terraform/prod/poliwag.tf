module "poliwag_service" {
  source = "../poliwag"

  name          = "poliwag"
  region        = "${var.region}"
  backup_region = "${var.backup_region}"
  environment   = "${var.environment}"
  env           = "${var.env}"

  cluster = "poliwag-cluster"

  vpc_id         = "${var.vpc_id}"
  security_group = "${var.security_group}"
  subnets        = "${var.subnets}"

  autoscaling_role = "arn:aws:iam::246232734983:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dax_role         = "arn:aws:iam::246232734983:role/aws-service-role/dax.amazonaws.com/AWSServiceRoleForDAX"

  memory        = "4096"
  canary_memory = "4096"
  cpu           = "4096"
  canary_cpu    = "4096"

  min_tasks = "35"
  max_tasks = "70"

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = "${var.env}"
    },
  ]

  whitelisted_arns_for_privatelink = "${var.whitelisted_arns_for_privatelink}"

  aws_profile    = "${var.aws_profile}"
  aws_account_id = "${var.aws_account_id}"

  min_aurora_capacity = 64
  max_aurora_capacity = 128

  sandstorm_role = "poliwag-prod"

  subnet_ips = "${var.subnet_ips}"

  elasticache_instance_type = "cache.m4.large"
  dax_instance_type         = "dax.r4.2xlarge"

  providers = {
    aws        = "aws"
    aws.backup = "aws.backup"
  }

  // Legal recommends 30 days TTL for sensitive log groups
  audit_logs_retention_in_days     = 30
  s2s_auth_logs_rentention_in_days = 30
}

module "poliwag_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "poliwag.prod.poliwag.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-0786f3fa0e4bff7c4"
  vpc_id            = "${var.vpc_id}"
}

module "hallpass_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "hallpass.production.cb.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-051cf257eb25dd6d5"
  vpc_id            = "${var.vpc_id}"
}

module "ripley_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "ripley.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-0e9e8a2896b1d5e89"
  vpc_id            = "${var.vpc_id}"
}

module "ripley_vpc_endpoint__NEW" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_ripley_twitch_a2z_com_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "prod.ripley.twitch.a2z.com"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-0e9e8a2896b1d5e89"
  vpc_id            = "${var.vpc_id}"
}

// LDAP privatelink does not use the same pattern as above because it's an edge case where
// we cannot use CNAME as the look-up record since it would have the same value as the
// name of the hosted zone (both ldap.twitch.a2z.com), the terracode module uses ALIAS
// instead which would not have this issue.
module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.security_group}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.subnets)}"
  dns             = "ldap.twitch.a2z.com"
}

module "payday_peering" {
  source = "../vpc-peering"

  vpc_id  = "${var.vpc_id}"
  subnets = ["${split(",", var.subnets)}"]

  peer_account_alias = "twitch-bits-aws"
  peer_account_id    = "021561903526"
  peer_cidr_block    = "10.193.64.0/18"
  peer_vpc_id        = "vpc-4cfeb629"
}

module "chat_peering" {
  source = "../vpc-peering"

  vpc_id  = "${var.vpc_id}"
  subnets = ["${split(",", var.subnets)}"]

  peer_account_alias = "twitch-chat-aws"
  peer_account_id    = "603200399373"
  peer_cidr_block    = "10.193.192.0/18"
  peer_vpc_id        = "vpc-6183fe04"
}

module "pubsub_dns_entry" {
  source = "../dns-entry"

  route53_zone_id = "${module.poliwag_service.route_53_internal_twitch_tv_zone}"
  dns_cname       = "pubsub-broker.internal.twitch.tv"
  dns_resolves_to = "internal-pubsub-production-internal-1513809412.us-west-2.elb.amazonaws.com"
}

module "subscriptions_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "subscriptions.internal.justin.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-02758ede819c44292"
  vpc_id            = "${var.vpc_id}"
}

module "users_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_uswest2_twitch_tv_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "users-service.prod.us-west2.twitch.tv"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-044e493c84b7dc984"
  vpc_id            = "${var.vpc_id}"
}

module "pantheon_peering" {
  source = "../vpc-peering"

  vpc_id  = "${var.vpc_id}"
  subnets = ["${split(",", var.subnets)}"]

  peer_account_alias = "twitch-pantheon-aws-prod"
  peer_account_id    = "375121467453"
  peer_cidr_block    = "10.201.184.0/22"
  peer_vpc_id        = "vpc-50635a37"
}

module "pantheon_dns_entry" {
  source = "../dns-entry"

  route53_zone_id = "${module.poliwag_service.route_53_internal_justin_tv_zone}"
  dns_cname       = "pantheon-prod.internal.justin.tv"
  dns_resolves_to = "prod-commerce-pantheon-alb-env.sj5q4jvtba.us-west-2.elasticbeanstalk.com"
}

module "copo_vpc_endpoint" {
  source = "../vpc-endpoint"

  route53_zone_id   = "${module.poliwag_service.route_53_copo_twitch_a2z_com_zone}"
  security_groups   = ["${var.security_group}"]
  subnet_ids        = ["${split(",", var.subnets)}"]
  service_dns_entry = "prod.copo.twitch.a2z.com"
  service_name      = "com.amazonaws.vpce.us-west-2.vpce-svc-081e8e8b6475c499e"
  vpc_id            = "${var.vpc_id}"
}

module "privatelink-pantheon" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=980609112b9b584795efc720221af72b81e928ef"

  name            = "pantheon"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-03caeabab7289eff3"
  environment     = "${var.environment}"
  security_groups = ["${var.security_group}"]
  subnets         = "${split(",", var.subnets)}"
  vpc_id          = "${var.vpc_id}"
}

// Add the twitch-inventory role
module "twitch-inventory" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory.git?ref=a7207ce935fce975ee739198bcd7e2df80419680"
  account = "poliwag-prod"
}

terraform {
  backend "s3" {
    bucket  = "poliwag-terraform-prod"
    key     = "tfstate/commerce/poliwag/terraform/production"
    region  = "us-west-2"
    profile = "poliwag-prod"
  }
}
