environment = "production"

env = "prod"

aws_profile = "poliwag-prod"

aws_account_id = "246232734983"

region = "us-west-2"

backup_region = "us-east-2"

vpc_id = "vpc-01105b4df584d9924"

security_group = "sg-0d6bbf791e17318a6"

subnets = "subnet-093d5373d3cb6ff2b,subnet-002c7aa781e0ca7c5,subnet-08d636b9f9cf7f3ac"

whitelisted_arns_for_privatelink = [
  "arn:aws:iam::021561903526:root", // payday
  "arn:aws:iam::246232734983:root", // poliwag-prod
  "arn:aws:iam::787149559823:root", // graphql-prod
  "arn:aws:iam::654663525958:root", // aegis-prod
  "arn:aws:iam::028439334451:root", // helix-prod
]

subnet_ips = [
  "10.206.156.0/24",
  "10.206.157.0/24",
  "10.206.158.0/24",
]
