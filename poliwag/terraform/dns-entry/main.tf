resource "aws_route53_record" "main" {
  zone_id = "${var.route53_zone_id}"
  name    = "${var.dns_cname}"
  type    = "CNAME"
  ttl     = "300"

  records = [
    "${var.dns_resolves_to}",
  ]
}
