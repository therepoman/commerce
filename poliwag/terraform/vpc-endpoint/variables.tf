variable "security_groups" {
  type        = "list"
  description = "Security groups to add to the vpc endpoint."
}

variable "subnet_ids" {
  type        = "list"
  description = "Subnets to add to the vpc endpoint."
}

variable "service_dns_entry" {
  description = "DNS entry to map to VPC endpoint. Cert served should match this entry."
}

variable "service_name" {
  description = "Service name of the VPC endpoint."
}

variable "vpc_id" {}
variable "route53_zone_id" {}
