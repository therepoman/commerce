resource "aws_vpc_endpoint" "main" {
  vpc_id              = "${var.vpc_id}"
  service_name        = "${var.service_name}"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false
  security_group_ids  = ["${var.security_groups}"]
  subnet_ids          = ["${var.subnet_ids}"]
}

resource "aws_route53_record" "main" {
  zone_id = "${var.route53_zone_id}"
  name    = "${var.service_dns_entry}"
  type    = "CNAME"
  ttl     = "300"

  records = [
    "${lookup(aws_vpc_endpoint.main.dns_entry[0], "dns_name")}",
  ]
}
