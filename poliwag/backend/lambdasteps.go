package backend

import (
	"github.com/go-playground/validator/v10"

	get_poll_step "code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/publish_poll_update_eventbus"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_poll_aggregation"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_choice_aggregations"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_poll_aggregation"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/send_poll_update_pubsub"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal"
)

type lambdaSteps struct {
	getPollStep                   get_poll_step.Step                `validate:"required"`
	queryChoiceAggregationsStep   query_choice_aggregations.Step    `validate:"required"`
	saveChoiceAggregationsStep    save_choice_aggregations.Step     `validate:"required"`
	queryPollAaggregationStep     query_poll_aggregation.Step       `validate:"required"`
	savePollAggregationStep       save_poll_aggregation.Step        `validate:"required"`
	sendPollUpdatePubsubStep      send_poll_update_pubsub.Step      `validate:"required"`
	publishPollUpdateEventbusStep publish_poll_update_eventbus.Step `validate:"required"`
	updateIsAggregationsEqualStep update_is_aggregations_equal.Step `validate:"required"`
}

func createLambdaSteps(daos *daos, controllers *controllers) (*lambdaSteps, error) {
	out := &lambdaSteps{
		getPollStep:                   get_poll_step.NewStep(daos.polls),
		queryChoiceAggregationsStep:   query_choice_aggregations.NewStep(daos.votes),
		saveChoiceAggregationsStep:    save_choice_aggregations.NewStep(daos.choices),
		queryPollAaggregationStep:     query_poll_aggregation.NewStep(daos.votes),
		savePollAggregationStep:       save_poll_aggregation.NewStep(daos.polls),
		sendPollUpdatePubsubStep:      send_poll_update_pubsub.NewStep(controllers.pubsubController),
		publishPollUpdateEventbusStep: publish_poll_update_eventbus.NewStep(controllers.eventbusController),
		updateIsAggregationsEqualStep: update_is_aggregations_equal.NewStep(controllers.pubsubController),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
