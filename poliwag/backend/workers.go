package backend

import (
	"github.com/go-playground/validator/v10"

	"code.justin.tv/commerce/poliwag/backend/worker"
	"code.justin.tv/commerce/poliwag/backend/worker/vote_pantheon_ingestion"
	"code.justin.tv/commerce/poliwag/backend/worker/vote_poll_update"
)

type workers struct {
	votePantheonIngestionWorker worker.Worker `validate:"required"`
	votePollUpdateWorker        worker.Worker `validate:"required"`
}

func createWorkers(c *clients, d *daos, ctrls *controllers) (*workers, error) {
	out := &workers{
		votePantheonIngestionWorker: vote_pantheon_ingestion.NewWorker(c.pantheon, d.votes),
		votePollUpdateWorker:        vote_poll_update.NewWorker(d.votes, d.polls, d.choices, ctrls.pubsubController, ctrls.eventbusController, c.locking),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
