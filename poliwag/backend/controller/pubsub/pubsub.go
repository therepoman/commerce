package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/avast/retry-go"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"

	pubsub_client "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	"code.justin.tv/commerce/poliwag/backend/utils/pubsub"
	"code.justin.tv/web/users-service/client/usersclient_internal"
)

type Controller interface {
	SendPollCreatePubsub(ctx context.Context, pollID string) error
	SendPollUpdatePubsub(ctx context.Context, pollID string) error
	SendPollCompletePubsub(ctx context.Context, pollID string) error
	SendPollTerminatePubsub(ctx context.Context, pollID string) error
	SendPollArchivePubsub(ctx context.Context, pollID string) error
	SendPollModeratePubsub(ctx context.Context, pollID string) error
}

const pollsTopic = "polls"

type controller struct {
	pubsubClient      pubsub_client.PubClient
	usersClient       usersclient_internal.InternalClient
	pollsDAO          polls_dao.PollsDAO
	getPollController get_poll.Controller
	stats             statsd.Statter
	isLambda          bool
}

func NewController(
	isLambda bool,
	pubsubClient pubsub_client.PubClient,
	usersClient usersclient_internal.InternalClient,
	pollsDAO polls_dao.PollsDAO,
	getPollController get_poll.Controller,
	stats statsd.Statter,
) Controller {
	return &controller{
		pubsubClient:      pubsubClient,
		usersClient:       usersClient,
		pollsDAO:          pollsDAO,
		getPollController: getPollController,
		stats:             stats,
		isLambda:          isLambda,
	}
}

func getPubsubTopic(channelID string) string {
	return fmt.Sprintf("%s.%s", pollsTopic, channelID)
}

func (c controller) sendPubsub(ctx context.Context, topics []string, msg string) error {
	beforePubsub := time.Now()

	// Lambdas shouldn't leverage hystrix
	var err error
	if c.isLambda {
		err = retry.Do(func() error {
			return c.pubsubClient.Publish(ctx, topics, msg, nil)
		}, retry.Context(ctx), retry.Attempts(3))
	} else {
		err = hystrix.CallTwirpWithHystrix(hystrix.PubsubPublishCommand, hystrix.HystrixDo, func() error {
			return c.pubsubClient.Publish(ctx, topics, msg, nil)
		})
	}

	go func(d time.Duration) {
		statsErr := c.stats.TimingDuration("Pubsub_Latency", d, 1.0)
		if statsErr != nil {
			logrus.WithError(err).Error("error sending pubsub latency metric")
		}
	}(time.Since(beforePubsub))

	go func(err error) {
		var errCount int64
		if err != nil {
			errCount = 1
		}
		statsErr := c.stats.Inc("Pubsub_Errors", errCount, 1.0)
		if statsErr != nil {
			logrus.WithError(err).Error("error sending pubsub errors metric")
		}
	}(err)
	return err
}

func (c controller) getPoll(ctx context.Context, pollID string) (models.PubsubPoll, error) {
	poll, choices, topBits, topChannelPoints, err := c.getPollController.GetPoll(ctx, pollID, true)
	if err != nil {
		logrus.WithField("pollID", pollID).WithError(err).Error("error calling GetPoll")
		return models.PubsubPoll{}, err
	}

	if topBits != nil {
		topContributor, err := c.usersClient.GetUserByID(ctx, topBits.UserID, nil)
		if err != nil {
			msg := fmt.Sprintf("error getting top bits contributor display name for poll id %s", poll.PollID)
			logrus.WithField("pollID", poll.PollID).WithError(err).Error(msg)
		} else if topContributor != nil {
			topBits.DisplayName = topContributor.Displayname
		}
	}

	if topChannelPoints != nil {
		topContributor, err := c.usersClient.GetUserByID(ctx, topChannelPoints.UserID, nil)
		if err != nil {
			msg := fmt.Sprintf("error getting top channel points contributor display name for poll id %s", poll.PollID)
			logrus.WithField("pollID", poll.PollID).WithError(err).Error(msg)
		} else if topContributor != nil {
			topChannelPoints.DisplayName = topContributor.Displayname
		}
	}

	pubsubPoll := pubsub.PollToPubsubPoll(poll, choices, topBits, topChannelPoints)

	return pubsubPoll, nil
}

func (c controller) SendPollCreatePubsub(ctx context.Context, pollID string) error {
	pubsubPoll, err := c.getPoll(ctx, pollID)
	if err != nil {
		return err
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: models.PubsubEventTypePollCreate,
		Data: models.PubsubEventDataPollCreate{
			Poll: pubsubPoll,
		},
	})
	if err != nil {
		return err
	}

	return c.sendPubsub(ctx, []string{getPubsubTopic(pubsubPoll.OwnedBy)}, string(pubsubEventBytes))
}

func (c controller) SendPollUpdatePubsub(ctx context.Context, pollID string) error {
	pubsubPoll, err := c.getPoll(ctx, pollID)
	if err != nil {
		return err
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: models.PubsubEventTypePollUpdate,
		Data: models.PubsubEventDataPollUpdate{
			Poll: pubsubPoll,
		},
	})
	if err != nil {
		return err
	}

	return c.sendPubsub(ctx, []string{getPubsubTopic(pubsubPoll.OwnedBy)}, string(pubsubEventBytes))
}

func (c controller) SendPollCompletePubsub(ctx context.Context, pollID string) error {
	pubsubPoll, err := c.getPoll(ctx, pollID)
	if err != nil {
		return err
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: models.PubsubEventTypePollComplete,
		Data: models.PubsubEventDataPollComplete{
			Poll: pubsubPoll,
		},
	})
	if err != nil {
		return err
	}

	return c.sendPubsub(ctx, []string{getPubsubTopic(pubsubPoll.OwnedBy)}, string(pubsubEventBytes))
}

func (c controller) SendPollTerminatePubsub(ctx context.Context, pollID string) error {
	pubsubPoll, err := c.getPoll(ctx, pollID)
	if err != nil {
		return err
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: models.PubsubEventTypePollTerminate,
		Data: models.PubsubEventDataPollTerminate{
			Poll: pubsubPoll,
		},
	})
	if err != nil {
		return err
	}

	return c.sendPubsub(ctx, []string{getPubsubTopic(pubsubPoll.OwnedBy)}, string(pubsubEventBytes))
}

func (c controller) SendPollArchivePubsub(ctx context.Context, pollID string) error {
	pubsubPoll, err := c.getPoll(ctx, pollID)
	if err != nil {
		return err
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: models.PubsubEventTypePollArchive,
		Data: models.PubsubEventDataPollArchive{
			Poll: pubsubPoll,
		},
	})
	if err != nil {
		return err
	}

	return c.sendPubsub(ctx, []string{getPubsubTopic(pubsubPoll.OwnedBy)}, string(pubsubEventBytes))
}

func (c controller) SendPollModeratePubsub(ctx context.Context, pollID string) error {
	pubsubPoll, err := c.getPoll(ctx, pollID)
	if err != nil {
		return err
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: models.PubsubEventTypePollModerate,
		Data: models.PubsubEventDataPollModerate{
			Poll: pubsubPoll,
		},
	})
	if err != nil {
		return err
	}

	return c.sendPubsub(ctx, []string{getPubsubTopic(pubsubPoll.OwnedBy)}, string(pubsubEventBytes))
}
