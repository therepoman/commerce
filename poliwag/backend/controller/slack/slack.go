package slack

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/joomcode/errorx"
	"github.com/olekukonko/tablewriter"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"

	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	slack_client "code.justin.tv/commerce/poliwag/clients/slack"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	user_service_models "code.justin.tv/web/users-service/models"
)

const (
	pollsLiveChannelID = "CP9UYP1MM"
	bigPollAmount      = 15000
	bigBitsPollAmount  = 10000
)

var intToSlackNumberEmoji = map[int]string{
	1: ":chompyletter1:",
	2: ":chompyletter2:",
	3: ":chompyletter3:",
	4: ":chompyletter4:",
	5: ":chompyletter5:",
}

type Controller interface {
	PostPollStart(ctx context.Context, pollID string) error
	PostPollEnd(ctx context.Context, pollID string) error
}

type controller struct {
	pollsDAO          polls_dao.PollsDAO
	getPollController get_poll.Controller
	usersClient       usersclient_internal.InternalClient
	slackClient       slack_client.Client
}

func NewController(
	pollsDAO polls_dao.PollsDAO,
	getPollController get_poll.Controller,
	usersClient usersclient_internal.InternalClient,
	slackClient slack_client.Client,
) Controller {
	return &controller{
		pollsDAO:          pollsDAO,
		getPollController: getPollController,
		usersClient:       usersClient,
		slackClient:       slackClient,
	}
}

func (c controller) PostPollStart(ctx context.Context, pollID string) error {
	poll, choices, _, _, err := c.getPollController.GetPoll(ctx, pollID, true)
	if err != nil {
		logrus.WithError(err).Warn("error getting poll when posting poll start to slack")
		return err
	}

	// Short circuit if slack message timestamp present
	if poll.SlackMessageTimestamp != "" {
		return nil
	}

	ownedByUser, err := c.usersClient.GetUserByID(ctx, poll.OwnedBy, nil)
	if err != nil {
		logrus.WithError(err).Warn("error getting owned by user from user service when posting poll start to slack")
		return err
	}

	if ownedByUser == nil || ownedByUser.Login == nil {
		logrus.Warn("user service returned nil owned by user when posting poll start to slack")
		return nil
	}

	createdByUser, err := c.usersClient.GetUserByID(ctx, poll.CreatedBy, nil)
	if err != nil {
		logrus.WithError(err).Warn("error getting created by user from user service when posting poll start to slack")
		return err
	}

	if createdByUser == nil || createdByUser.Login == nil {
		logrus.Warn("user service returned nil created by when posting poll start to slack")
		return nil
	}

	messageText := createPollStartMessageText(poll, choices, ownedByUser, createdByUser)

	_, respTimestamp, err := c.slackClient.PostMessageContext(
		ctx,
		pollsLiveChannelID,
		slack.MsgOptionText(messageText, false),
		slack.MsgOptionParse(true),
		slack.MsgOptionDisableLinkUnfurl(),
	)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).WithError(err).Info("failed to post poll start to slack")
		return err
	}

	_, err = c.pollsDAO.UpdatePoll(ctx, pollID, polls_dao.PollUpdateArgs{
		SlackMessageTimestamp: &respTimestamp,
	})
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID":           pollID,
			"messageTimestamp": respTimestamp,
		}).WithError(err).Warn("failed to update poll message timestamp")
		return err
	}

	return nil
}

func (c controller) PostPollEnd(ctx context.Context, pollID string) error {
	poll, choices, topBitsContributor, topChannelPointsContributor, err := c.getPollController.GetPoll(ctx, pollID, true)
	if err != nil {
		return err
	}

	if poll.SlackMessageTimestamp == "" {
		return errorx.IllegalState.New("no message timestamp detected for pollID %s", pollID)
	}

	messageText := createPollEndMessageText(poll, choices, topBitsContributor, topChannelPointsContributor)

	_, _, err = c.slackClient.PostMessageContext(
		ctx,
		pollsLiveChannelID,
		slack.MsgOptionTS(poll.SlackMessageTimestamp),
		slack.MsgOptionText(messageText, false),
		slack.MsgOptionParse(true),
		slack.MsgOptionDisableLinkUnfurl(),
	)

	return err
}

func createPollStartMessageText(poll models.Poll, choices []models.Choice, ownedByUser *user_service_models.Properties, createdByUser *user_service_models.Properties) string {
	var sb strings.Builder

	sb.WriteString(fmt.Sprintf(":poliwag: *New Poll - https://twitch.tv/%s *:poliwag:\n", *ownedByUser.Login))
	sb.WriteString(fmt.Sprintf(">%s\n", poll.Title))
	for i, choice := range choices {
		slackNumberEmoji := intToSlackNumberEmoji[i+1]
		sb.WriteString(fmt.Sprintf("%s %s\n", slackNumberEmoji, choice.Title))
	}

	sb.WriteString("```\n")

	sb.WriteString(fmt.Sprintf("Poll ID: %s\n", poll.PollID))
	sb.WriteString(fmt.Sprintf("Created By: %s (%s) \n", createdByUser.ID, *createdByUser.Displayname))
	sb.WriteString(fmt.Sprintf("Duration: %s\n", poll.Duration.String()))
	sb.WriteString(fmt.Sprintf("Subscriber Multiplier?: %t\n", poll.IsSubscriberMultiplierEnabled))
	sb.WriteString(fmt.Sprintf("Subscriber Only?: %t\n", poll.IsSubscriberOnlyEnabled))

	bitsVotesCostText := ""
	if poll.IsBitsVotesEnabled {
		bitsVotesCostText = fmt.Sprintf(", %d", poll.BitsVotesCost)
	}
	sb.WriteString(fmt.Sprintf("Bits Votes?: %t%s\n", poll.IsBitsVotesEnabled, bitsVotesCostText))

	isChannelPointsVotesEnabled := poll.IsChannelPointsVotesEnabled
	channelPointsVotesCostText := ""
	if isChannelPointsVotesEnabled {
		channelPointsVotesCostText = fmt.Sprintf(", %d", poll.ChannelPointsVotesCost)
	}
	sb.WriteString(fmt.Sprintf("Channel Points Votes?: %t%s\n", isChannelPointsVotesEnabled, channelPointsVotesCostText))

	sb.WriteString("```")

	return sb.String()
}

func createPollEndMessageText(poll models.Poll, choices []models.Choice, topBitsContributor *models.TopBitsContributor, topChannelPointsContributor *models.TopChannelPointsContributor) string {
	var sb strings.Builder

	sb.WriteString(":poliwag: *Poll Ended* :poliwag:\n")

	if poll.TotalVotes >= bigPollAmount {
		sb.WriteString(fmt.Sprintf(":bigchungusw: @here BIG POLL ALERT - %d votes :bigchungusw:\n", poll.TotalVotes))
	}

	if poll.TotalBits >= bigBitsPollAmount {
		sb.WriteString(fmt.Sprintf(":cheer10000: @here BIG BITS POLL ALERT - %d bits :cheer10000:\n", poll.TotalBits))
	}

	sb.WriteString(fmt.Sprintf("Reason: `%s`\n", poll.Status))

	// Sort choices by total votes
	sort.Slice(choices, func(i, j int) bool {
		return choices[i].TotalVotes > choices[j].TotalVotes
	})

	sb.WriteString("Results:\n")

	sb.WriteString("```\n")

	table := tablewriter.NewWriter(&sb)
	table.SetHeader([]string{"Rank", "Title", "Votes", "Bits", "Channel Points"})

	for _, choice := range choices {
		rankString := strconv.Itoa(getRank(choices, choice))

		percent := 0.0
		if poll.TotalVotes > 0 {
			percent = float64(choice.TotalVotes) / float64(poll.TotalVotes) * 100
		}

		totalBitsString := strconv.Itoa(choice.TotalBits)
		totalChannelPointsString := strconv.Itoa(choice.TotalChannelPoints)

		row := []string{rankString, choice.Title, fmt.Sprintf("%d (%.1f%%)", choice.TotalVotes, percent), totalBitsString, totalChannelPointsString}
		table.Append(row)
	}

	totalVotesString := strconv.Itoa(poll.TotalVotes)
	totalBitsString := strconv.Itoa(poll.TotalBits)
	totalChannelPointsString := strconv.Itoa(poll.TotalChannelPoints)
	table.SetFooter([]string{"", "Totals", totalVotesString, totalBitsString, totalChannelPointsString})
	table.Render()

	if topBitsContributor != nil && topBitsContributor.DisplayName != nil {
		sb.WriteString(fmt.Sprintf("Top Bits Contributor: %s (%d)\n", *topBitsContributor.DisplayName, topBitsContributor.BitsContributed))
	}

	if topChannelPointsContributor != nil && topChannelPointsContributor.DisplayName != nil {
		sb.WriteString(fmt.Sprintf("Top Channel Points Contributor: %s (%d)\n", *topChannelPointsContributor.DisplayName, topChannelPointsContributor.ChannelPointsContributed))
	}

	sb.WriteString("```")

	return sb.String()
}

func getRank(choices []models.Choice, choice models.Choice) int {
	numOfChoicesGreaterOrEqual := 0

	for _, c := range choices {
		if c.TotalVotes > choice.TotalVotes {
			numOfChoicesGreaterOrEqual++
		}
	}

	return numOfChoicesGreaterOrEqual + 1
}
