package get_voter

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/dao/voters"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Controller interface {
	GetVoter(ctx context.Context, pollID string, voterUserID string) (models.Poll, models.Voter, error)
}

type controller struct {
	getPollController get_poll.Controller
	pollsDAO          polls.PollsDAO
	votersDAO         voters.VotersDAO
}

func NewController(
	getPollController get_poll.Controller,
	pollsDAO polls.PollsDAO,
	votersDAO voters.VotersDAO,
) Controller {
	return &controller{
		getPollController: getPollController,
		pollsDAO:          pollsDAO,
		votersDAO:         votersDAO,
	}
}

func (c controller) GetVoter(ctx context.Context, pollID string, voterUserID string) (models.Poll, models.Voter, error) {
	poll, _, _, _, err := c.getPollController.GetPoll(ctx, pollID, false)
	if err != nil {
		return models.Poll{}, models.Voter{}, err
	}

	voter, err := c.votersDAO.GetVoter(ctx, pollID, voterUserID)
	if err != nil {
		msg := fmt.Sprintf("error getting voter for poll_id %s and user_id %s", pollID, voterUserID)
		logrus.WithError(err).WithField("pollID", pollID).Error(msg)
		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, msg)
	}

	if voter == nil {
		return models.Poll{}, models.Voter{}, errors.NotFound.New("voter %s does not exist for poll %s", voterUserID, pollID)
	}

	return poll, *voter, nil
}
