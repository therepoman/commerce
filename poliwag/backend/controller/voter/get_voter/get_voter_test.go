package get_voter_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter"
	"code.justin.tv/commerce/poliwag/backend/models"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	voters_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/voters"
)

func TestGetVoterController(t *testing.T) {
	Convey("given a get voter controller", t, func() {
		getPollsController := new(get_poll_mock.Controller)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		votersDAO := new(voters_dao_mock.VotersDAO)

		controller := get_voter.NewController(
			getPollsController,
			pollsDAO,
			votersDAO,
		)

		pollID := "test-poll-id"
		voterUserID := "test-voter-user-id"

		Convey("when GetPoll returns an error", func() {
			getPollsController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{}, nil, nil, nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, _, err := controller.GetVoter(context.Background(), pollID, voterUserID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetPoll returns a poll", func() {
			poll := models.Poll{
				PollID: pollID,
			}
			getPollsController.On("GetPoll", mock.Anything, pollID, false).Return(poll, nil, nil, nil, nil)

			Convey("when GetVoter returns an error", func() {
				votersDAO.On("GetVoter", mock.Anything, pollID, voterUserID).Return(nil, errors.New("ERRORS"))

				Convey("we should return an error", func() {
					_, _, err := controller.GetVoter(context.Background(), pollID, voterUserID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetVoter returns no voter", func() {
				votersDAO.On("GetVoter", mock.Anything, pollID, voterUserID).Return(nil, nil)

				Convey("we should return an error", func() {
					_, _, err := controller.GetVoter(context.Background(), pollID, voterUserID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetVoter returns a voter", func() {
				voter := models.Voter{
					PollID: pollID,
					UserID: voterUserID,
				}
				votersDAO.On("GetVoter", mock.Anything, pollID, voterUserID).Return(&voter, nil)

				Convey("we should return a poll and voter", func() {
					actualPoll, actualVoter, err := controller.GetVoter(context.Background(), pollID, voterUserID)

					So(err, ShouldBeNil)
					So(actualPoll, ShouldResemble, poll)
					So(actualVoter, ShouldResemble, voter)
				})
			})
		})
	})
}
