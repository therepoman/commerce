package get_voters_by_choice_test

import (
	"context"
	"errors"
	"testing"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/joomcode/errorx"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters_by_choice"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	voters_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/voters"
)

func TestController_GetVotersByChoice(t *testing.T) {
	Convey("given a controller", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		choicesDAO := new(choices_dao_mock.ChoicesDAO)
		votersDAO := new(voters_dao_mock.VotersDAO)
		statter, err := statsd.NewNoopClient()

		So(err, ShouldBeNil)

		controller := get_voters_by_choice.NewController(
			pollsDAO,
			choicesDAO,
			votersDAO,
			statter,
			telemetry.NewNoOpSampleReporter(),
		)

		pollID := "test-poll-id"
		choiceID := "test-choice-id"
		sort := models.VoterSortVotes
		limit := 20

		Convey("when GetPoll returns an error", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, errors.New("ERROR"))

			Convey("we should return an internal error", func() {
				_, _, err := controller.GetVotersByChoice(context.Background(), pollID, choiceID, sort, limit)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
			})
		})

		Convey("when GetPoll returns no poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, nil)

			Convey("we should return a not found error", func() {
				_, _, err := controller.GetVotersByChoice(context.Background(), pollID, choiceID, sort, limit)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.NotFound), ShouldBeTrue)
			})
		})

		Convey("when GetPoll returns a poll", func() {
			poll := models.Poll{
				PollID: pollID,
			}
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&poll, nil)

			Convey("when GetChoices returns an error", func() {
				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return(nil, errors.New("test error"))

				Convey("we should return an internal error", func() {
					_, _, err := controller.GetVotersByChoice(context.Background(), pollID, choiceID, sort, limit)

					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
				})
			})

			Convey("when GetChoices returns an empty list", func() {
				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return([]models.Choice{}, nil)

				Convey("we should return a not found error", func() {
					_, _, err := controller.GetVotersByChoice(context.Background(), pollID, choiceID, sort, limit)

					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.NotFound), ShouldBeTrue)
				})
			})

			Convey("when GetChoices returns the choice", func() {
				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return([]models.Choice{
					{
						ChoiceID: choiceID,
					},
				}, nil)

				Convey("when GetVotersByChoice returns an error", func() {
					votersDAO.On("GetVotersByChoice", mock.Anything, pollID, choiceID, sort, limit).Return(nil, errors.New("ERROR"))

					Convey("we should return an internal error", func() {
						_, _, err := controller.GetVotersByChoice(context.Background(), pollID, choiceID, sort, limit)

						So(err, ShouldNotBeNil)
						So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
					})
				})

				Convey("when GetVotersByChoice returns succeeds", func() {
					votersForChoice := []models.Voter{
						{
							PollID: pollID,
							UserID: "voter-user-id-0",
						},
						{
							PollID: pollID,
							UserID: "voter-user-id-1",
						},
					}

					votersDAO.On("GetVotersByChoice", mock.Anything, pollID, choiceID, sort, limit).Return(votersForChoice, nil)

					Convey("we should return the polls, voters, and new cursor", func() {
						actualPoll, actualVoters, err := controller.GetVotersByChoice(context.Background(), pollID, choiceID, sort, limit)

						So(err, ShouldBeNil)
						So(actualPoll, ShouldResemble, poll)
						So(actualVoters, ShouldResemble, votersForChoice)
					})
				})
			})
		})
	})
}
