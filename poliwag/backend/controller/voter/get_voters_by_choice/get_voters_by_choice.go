package get_voters_by_choice

import (
	"context"

	"github.com/cactus/go-statsd-client/statsd"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/logrus"
	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/dao/voters"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Controller interface {
	GetVotersByChoice(ctx context.Context, pollID string, choiceID string, sort models.VoterSort, limit int) (models.Poll, []models.Voter, error)
}

type controller struct {
	pollsDAO       polls_dao.PollsDAO
	choicesDAO     choices_dao.ChoicesDAO
	votersDAO      voters.VotersDAO
	stats          statsd.Statter
	sampleReporter *telemetry.SampleReporter
}

func NewController(
	pollsDAO polls_dao.PollsDAO,
	choicesDAO choices_dao.ChoicesDAO,
	votersDAO voters.VotersDAO,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
) Controller {
	return &controller{
		pollsDAO:       pollsDAO,
		choicesDAO:     choicesDAO,
		votersDAO:      votersDAO,
		stats:          stats,
		sampleReporter: sampleReporter,
	}
}

func (c controller) GetVotersByChoice(ctx context.Context, pollID string, choiceID string, sort models.VoterSort, limit int) (models.Poll, []models.Voter, error) {
	poll, err := c.pollsDAO.GetPoll(ctx, pollID, false)
	if err != nil {
		return models.Poll{}, nil, errors.InternalError.Wrap(err, "error fetching poll from dao")
	}

	if poll == nil {
		return models.Poll{}, nil, errors.NotFound.Wrap(err, "poll not found")
	}

	choices, err := c.choicesDAO.GetChoicesByPollID(ctx, pollID)
	if err != nil {
		return models.Poll{}, nil, errors.InternalError.Wrap(err, "error fetching choices from dao")
	}

	if len(choices) == 0 {
		logrus.WithField("pollID", poll.PollID).Warn("retrieved 0 choices for poll in GetVotersByChoice")
		statErr := c.stats.Inc(models.ZERO_CHOICE_METRIC_NAME, 1, 1.0)
		if statErr != nil {
			logrus.WithField("pollID", poll.PollID).Error("error tracking zero choice metric in GetVotersByChoice")
		}

		c.sampleReporter.Report(models.ZERO_CHOICE_METRIC_NAME, 1.0, telemetry.UnitCount)
	}

	choiceIDs := make(map[string]interface{})
	for _, choice := range choices {
		choiceIDs[choice.ChoiceID] = nil
	}

	if _, ok := choiceIDs[choiceID]; !ok {
		return models.Poll{}, nil, errors.NotFound.Wrap(err, "choice not found")
	}

	votersForChoice, err := c.votersDAO.GetVotersByChoice(ctx, pollID, choiceID, sort, limit)
	if err != nil {
		msg := "error getting voters by choice from dynamo"
		logrus.WithFields(logrus.Fields{
			"pollID":   pollID,
			"choiceID": choiceID,
			"sort":     sort,
			"limit":    limit,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, "error getting voters by choice from dynamo")
	}

	return *poll, votersForChoice, nil
}
