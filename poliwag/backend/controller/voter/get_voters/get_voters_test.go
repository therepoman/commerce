package get_voters_test

import (
	"context"
	"errors"
	"testing"

	"github.com/joomcode/errorx"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	voters_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/voters"
)

func TestController_GetVoters(t *testing.T) {
	Convey("given a controller", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		votersDAO := new(voters_dao_mock.VotersDAO)

		controller := get_voters.NewController(pollsDAO, votersDAO)

		pollID := "test-poll-id"
		sort := models.VoterSortVotes
		limit := 5

		Convey("when GetPoll returns an error", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, _, err := controller.GetVoters(context.Background(), pollID, sort, limit)
				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
			})
		})

		Convey("when GetPoll return no poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, nil)

			Convey("we should return an error", func() {
				_, _, err := controller.GetVoters(context.Background(), pollID, sort, limit)
				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.NotFound), ShouldBeTrue)
			})
		})

		Convey("when GetPoll returns a poll", func() {
			poll := models.Poll{
				PollID: pollID,
			}

			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&poll, nil)

			Convey("when GetVoters returns an error", func() {
				votersDAO.On("GetVoters", mock.Anything, pollID, sort, limit).Return(nil, errors.New("ERROR"))

				Convey("when GetVoters returns an error", func() {
					_, _, err := controller.GetVoters(context.Background(), pollID, sort, limit)
					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
				})
			})

			Convey("when GetVoters returns voters", func() {
				voters := []models.Voter{
					{
						PollID: pollID,
						UserID: "voter-user-id-0",
					},
					{
						PollID: pollID,
						UserID: "voter-user-id-1",
					},
				}
				votersDAO.On("GetVoters", mock.Anything, pollID, sort, limit).Return(voters, nil)

				Convey("when GetVoters returns an error", func() {
					actualPoll, actualVoters, err := controller.GetVoters(context.Background(), pollID, sort, limit)

					So(err, ShouldBeNil)
					So(actualPoll, ShouldResemble, poll)
					So(actualVoters, ShouldResemble, voters)
				})
			})
		})
	})
}
