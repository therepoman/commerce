package get_voters

import (
	"context"

	"github.com/sirupsen/logrus"

	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/dao/voters"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Controller interface {
	GetVoters(ctx context.Context, pollID string, sort models.VoterSort, limit int) (models.Poll, []models.Voter, error)
}

type controller struct {
	pollsDAO  polls_dao.PollsDAO
	votersDAO voters.VotersDAO
}

func NewController(
	pollsDAO polls_dao.PollsDAO,
	votersDAO voters.VotersDAO,
) Controller {
	return &controller{
		pollsDAO:  pollsDAO,
		votersDAO: votersDAO,
	}
}

func (c controller) GetVoters(ctx context.Context, pollID string, sort models.VoterSort, limit int) (models.Poll, []models.Voter, error) {
	poll, err := c.pollsDAO.GetPoll(ctx, pollID, false)
	if err != nil {
		return models.Poll{}, nil, errors.InternalError.Wrap(err, "error fetching poll from dao")
	}

	if poll == nil {
		return models.Poll{}, nil, errors.NotFound.Wrap(err, "poll not found")
	}

	votersForPoll, err := c.votersDAO.GetVoters(ctx, poll.PollID, sort, limit)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
			"sort":   sort,
			"limit":  limit,
		})
		return models.Poll{}, nil, errors.InternalError.Wrap(err, "error getting voters from dynamo")
	}

	return *poll, votersForPoll, nil
}
