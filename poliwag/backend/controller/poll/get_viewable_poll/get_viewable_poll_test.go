package get_viewable_poll_test

import (
	"context"
	"errors"
	"testing"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	viewable_poll_cache_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestController_GetViewablePoll(t *testing.T) {
	Convey("given a poll controller", t, func() {
		choicesDAO := new(choices_dao_mock.ChoicesDAO)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		viewablePollCache := new(viewable_poll_cache_mock.ViewablePollCache)
		statter, err := statsd.NewNoopClient()

		So(err, ShouldBeNil)

		controller := get_viewable_poll.NewController(choicesDAO, pollsDAO, viewablePollCache, statter, telemetry.NewNoOpSampleReporter())

		userID := "test-user-id"
		ownedBy := "test-owned-by"

		Convey("when ViewablePollCache returns an error", func() {
			viewablePollCache.On("Get", mock.Anything, ownedBy).Return(nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, _, _, _, err := controller.GetViewablePoll(context.Background(), userID, ownedBy)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when ViewablePollCache returns no viewablePoll", func() {
			viewablePollCache.On("Get", mock.Anything, ownedBy).Return(nil, nil)

			Convey("we should not return an error", func() {
				actualPoll, actualChoices, actualTopBitsContributor, actualTopChannelPointsContributor, err := controller.GetViewablePoll(context.Background(), userID, ownedBy)

				So(err, ShouldBeNil)
				So(actualPoll, ShouldBeNil)
				So(actualChoices, ShouldBeNil)
				So(actualTopBitsContributor, ShouldBeNil)
				So(actualTopChannelPointsContributor, ShouldBeNil)
			})
		})

		Convey("when ViewablePollCache returns a viewablePoll", func() {
			pollID := "test-poll-id"

			viewablePoll := models.Poll{
				PollID:  pollID,
				OwnedBy: ownedBy,
			}

			viewablePollCache.On("Get", mock.Anything, ownedBy).Return(&viewablePoll, nil)

			Convey("when GetChoicesByPollID returns an error", func() {
				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return(nil, errors.New("ERROR"))

				Convey("we should return an error", func() {
					_, _, _, _, err := controller.GetViewablePoll(context.Background(), userID, ownedBy)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetChoicesByPollID returns choices", func() {
				choices := []models.Choice{
					{
						Title:    "Final Fantasy VII",
						PollID:   pollID,
						ChoiceID: "choice-id-0",
					},
					{
						Title:    "Chrono Trigger",
						PollID:   pollID,
						ChoiceID: "choice-id-1",
					},
				}

				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return(choices, nil)
				pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, pollID).Return(nil, nil, nil)

				Convey("we should return a viewable poll", func() {
					actualPoll, actualChoices, _, _, err := controller.GetViewablePoll(context.Background(), userID, ownedBy)

					So(err, ShouldBeNil)
					So(*actualPoll, ShouldResemble, viewablePoll)
					So(actualChoices, ShouldResemble, choices)
				})
			})
		})
	})
}
