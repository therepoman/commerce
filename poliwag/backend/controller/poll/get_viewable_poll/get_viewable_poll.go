package get_viewable_poll

import (
	"context"
	"fmt"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	viewable_poll_cache "code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Controller interface {
	GetViewablePoll(ctx context.Context, userID string, ownedBy string) (*models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error)
}

type controller struct {
	choicesDAO        choices_dao.ChoicesDAO
	pollsDAO          polls_dao.PollsDAO
	viewablePollCache viewable_poll_cache.ViewablePollCache
	stats             statsd.Statter
	sampleReporter    *telemetry.SampleReporter
}

func NewController(
	choicesDAO choices_dao.ChoicesDAO,
	pollsDAO polls_dao.PollsDAO,
	viewablePollCache viewable_poll_cache.ViewablePollCache,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
) Controller {
	return &controller{
		choicesDAO:        choicesDAO,
		pollsDAO:          pollsDAO,
		viewablePollCache: viewablePollCache,
		stats:             stats,
		sampleReporter:    sampleReporter,
	}
}

func (c controller) GetViewablePoll(ctx context.Context, userID string, ownedBy string) (*models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error) {
	viewablePoll, err := c.viewablePollCache.Get(ctx, ownedBy)
	if err != nil {
		msg := fmt.Sprintf("error getting viewable poll from cache")
		logrus.WithField("ownedBy", ownedBy).WithError(err).Error(msg)
		return nil, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	if viewablePoll == nil {
		return nil, nil, nil, nil, nil
	}

	viewableChoices, err := c.choicesDAO.GetChoicesByPollID(ctx, viewablePoll.PollID)
	if err != nil {
		msg := fmt.Sprintf("error getting choices for poll %s", viewablePoll.PollID)
		logrus.WithField("pollID", viewablePoll.PollID).WithError(err).Error(msg)
		return nil, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	if len(viewableChoices) == 0 {
		logrus.WithField("pollID", viewablePoll.PollID).Warn("retrieved 0 choices for poll in GetViewablePoll")
		statErr := c.stats.Inc(models.ZERO_CHOICE_METRIC_NAME, 1, 1.0)
		if statErr != nil {
			logrus.WithField("pollID", viewablePoll.PollID).Error("error tracking zero choice metric in GetViewablePoll")
		}

		c.sampleReporter.Report(models.ZERO_CHOICE_METRIC_NAME, 1.0, telemetry.UnitCount)
	}

	var topBitsContributor *models.TopBitsContributor
	var topChannelPointsContributor *models.TopChannelPointsContributor
	if viewablePoll.Status != models.PollStatusActive {
		topBitsContributor, topChannelPointsContributor, err = c.pollsDAO.GetTopContributorsFromPantheon(ctx, viewablePoll.PollID)
		if err != nil {
			msg := fmt.Sprintf("error getting top contributors for poll %s", viewablePoll.PollID)
			logrus.WithField("pollID", viewablePoll.PollID).WithError(err).Error(msg)
			return nil, nil, nil, nil, errors.InternalError.Wrap(err, msg)
		}
	}

	return viewablePoll, viewableChoices, topBitsContributor, topChannelPointsContributor, nil
}
