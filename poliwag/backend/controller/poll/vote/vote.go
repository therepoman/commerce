package vote

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/bsm/redislock"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/joomcode/errorx"
	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/copo/proto/copo"
	"code.justin.tv/commerce/gogogadget/aws/sns"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter"
	"code.justin.tv/commerce/poliwag/backend/dao/voters"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	"code.justin.tv/commerce/poliwag/clients/datascience"
	"code.justin.tv/commerce/poliwag/clients/lock"
	"code.justin.tv/commerce/poliwag/config"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
)

const (
	SubscriptionMultiplier = 2
)

type Controller interface {
	Vote(ctx context.Context, voteID string, pollID string, userID string, choiceID string, tokens map[models.TokenType]int64) (models.Poll, models.Voter, error)
}

type controller struct {
	getPollController  get_poll.Controller
	getVoterController get_voter.Controller
	votesDAO           votes.DAO
	votersDAO          voters.VotersDAO
	lockingClient      lock.LockingClient
	paydayClient       paydayrpc.Payday
	copoClient         copo.Copo
	datascienceTracker datascience.Tracker
	subsClient         substwirp.Subscriptions
	stats              statsd.Statter
	sampleReporter     *telemetry.SampleReporter
	snsClient          sns.Client
	config             *config.Config
}

func NewController(
	getPollController get_poll.Controller,
	getVoterController get_voter.Controller,
	votesDAO votes.DAO,
	votersDAO voters.VotersDAO,
	lockingClient lock.LockingClient,
	paydayClient paydayrpc.Payday,
	copoClient copo.Copo,
	datascienceTracker datascience.Tracker,
	subsClient substwirp.Subscriptions,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
	snsClient sns.Client,
	config *config.Config,
) Controller {
	return &controller{
		getPollController:  getPollController,
		getVoterController: getVoterController,
		votesDAO:           votesDAO,
		votersDAO:          votersDAO,
		lockingClient:      lockingClient,
		paydayClient:       paydayClient,
		copoClient:         copoClient,
		datascienceTracker: datascienceTracker,
		subsClient:         subsClient,
		stats:              stats,
		sampleReporter:     sampleReporter,
		snsClient:          snsClient,
		config:             config,
	}
}

func (c controller) trackMetrics(metric string, startTime time.Time, err error, allowedErrType *errorx.Type) {
	d := time.Since(startTime)
	go func(m string, dur time.Duration) {
		statErr := c.stats.TimingDuration(m, dur, 1.0)
		if statErr != nil {
			logrus.WithField("metric", m).WithError(err).Error("error tracking latency metric")
		}
	}(metric+"_Latency", d)

	go func(m string, err error) {
		errCount := int64(0)
		if err != nil {
			if allowedErrType == nil || (allowedErrType != nil && !errorx.IsOfType(err, allowedErrType)) {
				errCount = 1
			}
		}
		statErr := c.stats.Inc(m, errCount, 1.0)
		if statErr != nil {
			logrus.WithField("metric", m).WithError(err).Error("error tracking error count metric")
		}
	}(metric+"_Errors", err)
}

func (c controller) Vote(ctx context.Context, voteID string, pollID string, userID string, choiceID string, tokens map[models.TokenType]int64) (models.Poll, models.Voter, error) {
	beforeRDSGetVote := time.Now()
	vote, err := c.votesDAO.GetVote(ctx, pollID, voteID, true)
	c.trackMetrics("Vote_RDSGetVote", beforeRDSGetVote, err, nil)
	if err != nil {
		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error getting vote")
	}

	if vote != nil {
		return models.Poll{}, models.Voter{}, errors.VoteIDConflict.New("a vote already exists with this id")
	}

	beforeObtainLock := time.Now()
	voteLock, err := c.lockingClient.ObtainLock(ctx, fmt.Sprintf("%s-%s", pollID, userID))
	c.trackMetrics("Vote_ObtainLock", beforeObtainLock, err, nil)
	if err == redislock.ErrNotObtained {
		return models.Poll{}, models.Voter{}, errors.VoteIDConflict.New("a conflicting vote action is currently in progress")
	} else if err != nil {
		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error obtaining vote lock")
	}

	defer func() {
		err := voteLock.Release()
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"poll_id":   pollID,
				"choice_id": choiceID,
				"user_id":   userID,
				"vote_id":   voteID,
			}).Warn("error unlocking vote lock")
		}
	}()

	beforeDynamoGetPoll := time.Now()

	// Using eventually consistent reads here introduces two possible race conditions:
	//   * A poll's status is no longer active, but the cache believes it is. In the event that a poll ends early, a vote could be counted erroneously.
	//   * A vote is cast before the Poll write operation has completed (very unlikely). In this case, the vote would be rejected - PollNotFound.
	//
	// We are opting to tolerate this risk to save us from hot spotting that we were seeing during large
	// bursts of a votes for a single poll.
	poll, choices, _, _, err := c.getPollController.GetPoll(ctx, pollID, false)
	c.trackMetrics("Vote_DynamoGetPoll", beforeDynamoGetPoll, err, nil)
	if err != nil {
		if errorx.IsOfType(err, errors.NotFound) {
			return models.Poll{}, models.Voter{}, errors.PollNotFound.New("poll not found")
		}

		return models.Poll{}, models.Voter{}, err
	}

	if poll.Status != models.PollStatusActive {
		return models.Poll{}, models.Voter{}, errors.PollNotActive.New("poll is not active")
	}

	if poll.EndTime != nil || poll.StartTime.Add(poll.Duration).Before(time.Now()) {
		return models.Poll{}, models.Voter{}, errors.PollNotActive.New("poll has expired")
	}

	var voteChoice *models.Choice
	for _, choice := range choices {
		choice := choice
		if choice.ChoiceID == choiceID {
			voteChoice = &choice
			break
		}
	}

	if voteChoice == nil {
		return models.Poll{}, models.Voter{}, errors.InvalidChoiceID.New("choice id %s does not exists for poll id %s", choiceID, pollID)
	}

	beforeDynamoGetVoter := time.Now()
	var existingVoter *models.Voter
	_, voter, err := c.getVoterController.GetVoter(ctx, pollID, userID)
	c.trackMetrics("Vote_DynamoGetVoter", beforeDynamoGetVoter, err, errors.NotFound)
	if err != nil && !errorx.IsOfType(err, errors.NotFound) {
		return models.Poll{}, models.Voter{}, err
	}

	if !errorx.IsOfType(err, errors.NotFound) {
		existingVoter = &voter
	}

	userHasSubscription := false
	if poll.IsSubscriberOnlyEnabled || poll.IsSubscriberMultiplierEnabled {
		if existingVoter == nil {
			beforeSubsCall := time.Now()

			var subsResp *substwirp.GetUserChannelSubscriptionResponse
			err = hystrix.CallTwirpWithHystrix(hystrix.SubscriptionsGetUserChannelSubscriptionCommand, hystrix.HystrixDo, func() error {
				subsResp, err = c.subsClient.GetUserChannelSubscription(ctx, &substwirp.GetUserChannelSubscriptionRequest{
					OwnerId:   userID,
					ChannelId: poll.OwnedBy,
				})
				return err
			})

			c.trackMetrics("Vote_SubsServiceCall", beforeSubsCall, err, nil)
			if err != nil {
				return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error checking user subscription in channel")
			}

			userHasSubscription = subsResp != nil && subsResp.Subscription != nil
		} else {
			userHasSubscription = existingVoter.HasSubscription
		}
	}

	if poll.IsSubscriberOnlyEnabled && !userHasSubscription {
		return models.Poll{}, models.Voter{}, errors.SubscriberRequired.New("this poll is only for subscribers")
	}

	var newBaseVotes int64
	var totalBaseVotes int64

	otherChoices := make(map[string]models.VoterChoice)
	if existingVoter != nil {
		for _, choice := range existingVoter.Choices {
			if choice.ChoiceID != choiceID {
				otherChoices[choice.ChoiceID] = choice
			}
			totalBaseVotes += int64(choice.TotalBaseVotes)
		}
	}

	if !poll.IsMultiChoiceEnabled && len(otherChoices) > 0 {
		return models.Poll{}, models.Voter{}, errors.MultiChoiceVoteForbidden.New("cannot vote on multiple choices for this poll")
	}

	if totalBaseVotes == 0 {
		newBaseVotes = 1
	}

	totalTokens := countAllTokens(tokens)
	if newBaseVotes == 0 && totalTokens == 0 {
		return models.Poll{}, models.Voter{}, errors.TokensRequired.New("base vote already used - new votes must be performed with tokens")
	}

	rawBitsTokens := tokens[models.TokenTypeBits]
	rawBitsVotes := int64(0)
	if rawBitsTokens > 0 {
		if !poll.IsBitsVotesEnabled || poll.BitsVotesCost <= 0 {
			return models.Poll{}, models.Voter{}, errors.IllegalArgument.New("bits tokens used in request, but poll is not configured for bits")
		}

		if poll.OwnedBy == userID {
			return models.Poll{}, models.Voter{}, errors.SelfBitsVoteNotAllowed.New("owner cannot vote for their own poll with bits")
		}

		if rawBitsTokens%int64(poll.BitsVotesCost) != 0 {
			return models.Poll{}, models.Voter{}, errors.InvalidBitsAmount.New("bits tokens must be a multiple of the vote cost")
		}

		rawBitsVotes = rawBitsTokens / int64(poll.BitsVotesCost)
	}

	if rawBitsTokens > 0 {
		beforePaydayCall := time.Now()
		err := hystrix.CallTwirpWithHystrix(hystrix.PaydayUseBitsOnPollCommand, hystrix.HystrixDo, func() error {
			_, err := c.paydayClient.UseBitsOnPoll(ctx, &paydayrpc.UseBitsOnPollReq{
				VoteId:     voteID,
				UserId:     userID,
				ChannelId:  poll.OwnedBy,
				BitsAmount: int32(rawBitsTokens),
				PollId:     pollID,
				ChoiceId:   choiceID,
			})
			return err
		})

		c.trackMetrics("Vote_PaydayCall", beforePaydayCall, err, nil)
		if err != nil {
			var rpcError paydayrpc.ClientError
			parsingErr := paydayrpc.ParseClientError(err, &rpcError)
			if parsingErr == nil && rpcError.ErrorCode == paydayrpc.ErrCodeInsufficientBitsBalance {
				return models.Poll{}, models.Voter{}, errors.InsufficientBitsBalance.New("insufficient bits balance to perform vote")
			}

			return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error using bits to vote")
		}
	}

	rawChannelPointsTokens := tokens[models.TokenTypeChannelPoints]
	rawChannelPointsVotes := int64(0)
	if rawChannelPointsTokens > 0 {
		if !poll.IsChannelPointsVotesEnabled || poll.ChannelPointsVotesCost <= 0 {
			return models.Poll{}, models.Voter{}, errors.IllegalArgument.New("channel points tokens used in request, but poll is not configured for channel points")
		}

		if poll.OwnedBy == userID {
			return models.Poll{}, models.Voter{}, errors.SelfChannelPointsVoteNotAllowed.New("owner cannot vote for their own poll with channel points")
		}

		if rawChannelPointsTokens%int64(poll.ChannelPointsVotesCost) != 0 {
			return models.Poll{}, models.Voter{}, errors.InvalidChannelPointsAmount.New("channel points tokens must be a multiple of the vote cost")
		}

		rawChannelPointsVotes = rawChannelPointsTokens / int64(poll.ChannelPointsVotesCost)
	}

	if rawChannelPointsTokens > 0 {
		beforePaydayCall := time.Now()
		var copoResp *copo.SubtractPointsResponse
		var copoErr error
		err := hystrix.CallTwirpWithHystrix(hystrix.CopoSpendPoints, hystrix.HystrixDo, func() error {
			copoResp, copoErr = c.copoClient.SubtractPoints(ctx, &copo.SubtractPointsRequest{
				UserId:     userID,
				ChannelId:  poll.OwnedBy,
				Points:     rawChannelPointsTokens,
				Reason:     copo.SubtractPointsReason_SUBTRACT_POINTS_VOTE_IN_POLL,
				ReasonArgs: &copo.SubtractPointsReasonArgs{PollId: pollID},
			})
			return copoErr
		})

		c.trackMetrics("Vote_CopoCall", beforePaydayCall, err, nil)
		if err != nil {
			return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error using channel points to vote")
		} else if copoResp.GetError() != nil {
			switch copoResp.GetError().GetCode() {
			case copo.SubtractPointsErrorCode_SUBTRACT_POINTS_INSUFFICIENT_POINTS:
				return models.Poll{}, models.Voter{}, errors.InsufficientChannelPointsBalance.New("insufficient channel points balance to perform vote")
			default:
				return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error using channel points to vote")
			}
		}
	}

	newBitsVotes := rawBitsVotes
	newChannelPointsVotes := rawChannelPointsVotes
	if poll.IsSubscriberMultiplierEnabled && userHasSubscription {
		newBitsVotes *= SubscriptionMultiplier
		newChannelPointsVotes *= SubscriptionMultiplier
		newBaseVotes *= SubscriptionMultiplier
	}

	newVote := votes.Vote{
		PollID:             pollID,
		ChoiceID:           choiceID,
		VoteID:             voteID,
		UserID:             userID,
		TotalVotes:         newBaseVotes + newBitsVotes + newChannelPointsVotes,
		BaseVotes:          newBaseVotes,
		BitsVotes:          newBitsVotes,
		Bits:               rawBitsTokens,
		ChannelPointsVotes: newChannelPointsVotes,
		ChannelPoints:      rawChannelPointsTokens,
	}

	beforeRDSCreateVote := time.Now()
	err = c.votesDAO.CreateVote(ctx, newVote)
	c.trackMetrics("Vote_RDSCreateVote", beforeRDSCreateVote, err, nil)
	if err != nil {
		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error creating vote")
	}

	snsVoteModel := models.Vote{
		PollID:   poll.PollID,
		VoteID:   voteID,
		UserID:   userID,
		ChoiceID: choiceID,
	}

	snsVoteJSON, err := json.Marshal(&snsVoteModel)
	if err != nil {
		logrus.WithError(err).Error("error marshaling poll update into json")
		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error marshaling poll update into json")
	}

	beforeSNSPublish := time.Now()
	err = c.snsClient.Publish(ctx, c.config.VoteSNSTopic, string(snsVoteJSON))
	c.trackMetrics("Vote_SNSPublish", beforeSNSPublish, err, nil)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"poll_id":   pollID,
			"choice_id": choiceID,
			"vote_id":   voteID,
			"user_id":   userID,
		}).WithError(err).Error("Error sending vote to sns")

		go c.rollbackVote(pollID, voteID)

		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error sending vote to sns")
	}

	beforeDynamoIncrementTotals := time.Now()
	updatedVoter, err := c.votersDAO.SaveVoterAndIncrementTotals(ctx, pollID, choiceID, userID, newBaseVotes, newBitsVotes, rawBitsTokens, newChannelPointsVotes, rawChannelPointsTokens, userHasSubscription)
	c.trackMetrics("Vote_DynamoIncrementTotals", beforeDynamoIncrementTotals, err, nil)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"poll_id":   pollID,
			"choice_id": choiceID,
			"vote_id":   voteID,
			"user_id":   userID,
		}).WithError(err).Error("Error incrementing voter choice tables")

		go c.rollbackVote(pollID, voteID)

		return models.Poll{}, models.Voter{}, errors.InternalError.Wrap(err, "error incrementing voter choice tables")
	}

	c.datascienceTracker.TrackEventAsync(datascience.Vote, models.VoteProperties{
		UserID:             userID,
		ChannelID:          poll.OwnedBy,
		PollID:             pollID,
		PollResponse:       voteChoice.Index + 1,
		BitsSpent:          int(rawBitsTokens),
		ChannelPointsSpent: int(rawChannelPointsTokens),
	})

	return poll, updatedVoter, nil
}

func (c controller) rollbackVote(pollID string, voteID string) {
	rollbackContext, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := c.votesDAO.DeleteVote(rollbackContext, pollID, voteID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"poll_id": pollID,
			"vote_id": voteID,
		}).WithError(err).Error("error deleting vote from aurora rds during rollback")
	}
}

func countAllTokens(tokens map[models.TokenType]int64) int64 {
	total := int64(0)
	for _, tokenAmt := range tokens {
		total += tokenAmt
	}
	return total
}
