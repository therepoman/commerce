package vote_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/copo/proto/copo"
	"code.justin.tv/commerce/gogogadget/pointers"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/vote"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/clients/lock"
	"code.justin.tv/commerce/poliwag/config"
	copo_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/copo/proto/copo"
	sns_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	paydayrpc_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/payday/rpc/payday"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	get_voter_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter"
	voters_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/voters"
	votes_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/votes"
	datascience_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/clients/datasceince"
	substwirp_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/revenue/subscriptions/twirp"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
)

func TestController_Vote(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		getVoterController := new(get_voter_mock.Controller)
		votersDAO := new(voters_dao_mock.VotersDAO)
		votesDAO := new(votes_dao_mock.DAO)
		nooplockingClient := lock.NewNoopLockingClient()
		paydayClient := new(paydayrpc_mock.Payday)
		copoClient := new(copo_mock.Copo)
		datascienceTracker := new(datascience_mock.Tracker)
		subscriptionClient := new(substwirp_mock.Subscriptions)
		snsClient := new(sns_mock.Client)

		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)

		controller := vote.NewController(
			getPollController,
			getVoterController,
			votesDAO,
			votersDAO,
			nooplockingClient,
			paydayClient,
			copoClient,
			datascienceTracker,
			subscriptionClient,
			statter,
			telemetry.NewNoOpSampleReporter(),
			snsClient,
			&config.Config{},
		)

		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		voteID := "test-vote-id"
		pollID := "test-poll-id"
		userID := "test-user-id"
		choiceID := "test-choice-id"
		tokensEmpty := map[models.TokenType]int64{
			models.TokenTypeBits:          0,
			models.TokenTypeChannelPoints: 0,
		}

		Convey("errors when votes dao get vote call errors", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(nil, errors.New("test-error"))
			_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
			So(err, ShouldNotBeNil)
		})

		Convey("errors when votes dao get vote call returns a record", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(&votes.Vote{
				VoteID: voteID,
			}, nil)
			_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
			So(err, ShouldNotBeNil)
		})

		Convey("when votes dao returns nil", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(nil, nil)

			Convey("errors when get poll controller errors", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{}, []models.Choice{}, nil, nil, errors.New("test-error"))
				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
				So(err, ShouldNotBeNil)
			})

			Convey("errors when get poll controller returns a poll with the wrong status", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID: pollID,
					Status: models.PollStatusTerminated,
				}, []models.Choice{}, nil, nil, nil)
				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
				So(err, ShouldNotBeNil)
			})

			Convey("errors when get poll controller returns a poll with an end time", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:  pollID,
					Status:  models.PollStatusActive,
					EndTime: pointers.TimeP(time.Now().Add(-time.Minute)),
				}, []models.Choice{}, nil, nil, nil)
				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
				So(err, ShouldNotBeNil)
			})

			Convey("errors when get poll controller returns a poll that should have expired", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:    pollID,
					Status:    models.PollStatusActive,
					StartTime: time.Now().Add(-time.Hour),
					Duration:  time.Minute,
				}, []models.Choice{}, nil, nil, nil)
				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
				So(err, ShouldNotBeNil)
			})

			Convey("errors when get poll controller doesn't return the requested choice", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:    pollID,
					Status:    models.PollStatusActive,
					StartTime: time.Now(),
					Duration:  5 * time.Minute,
				}, []models.Choice{}, nil, nil, nil)
				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
				So(err, ShouldNotBeNil)
			})

			Convey("when get poll controller returns the requested choice and bits are not enabled", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:    pollID,
					Status:    models.PollStatusActive,
					StartTime: time.Now(),
					Duration:  5 * time.Minute,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)

				Convey("error when get voter returns an error", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, errors.New("test-error"))
					_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
					So(err, ShouldNotBeNil)
				})

				Convey("errors when get voter returns another choice", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{
						Choices: []models.VoterChoice{
							{
								ChoiceID: "some-other-choice",
							},
						},
					}, nil)
					_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
					So(err, ShouldNotBeNil)
				})

				Convey("errors when get voter returns a choice with a base vote already used (and tokens ARE NOT used in the request)", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{
						Choices: []models.VoterChoice{
							{
								ChoiceID:       choiceID,
								TotalBaseVotes: 1,
							},
						},
					}, nil)
					_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
					So(err, ShouldNotBeNil)
				})

				Convey("when get voter returns a not found error", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

					Convey("errors when bits tokens are used (since the poll doesn't support bits)", func() {
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, map[models.TokenType]int64{
							models.TokenTypeBits: 1,
						})
						So(err, ShouldNotBeNil)
					})

					Convey("errors when channel points tokens are used (since the poll doesn't support channel points)", func() {
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, map[models.TokenType]int64{
							models.TokenTypeChannelPoints: 1,
						})
						So(err, ShouldNotBeNil)
					})

					Convey("errors when vote dao create vote errors", func() {
						votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(errors.New("test-error"))
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
						So(err, ShouldNotBeNil)
					})

					Convey("when vote dao create vote succeeds", func() {
						votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)
						votesDAO.On("DeleteVote", mock.Anything, pollID, voteID).Return(nil)

						Convey("errors when sns publish errors", func() {
							snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test-error"))
							_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
							So(err, ShouldNotBeNil)
						})

						Convey("when sns publish succeeds", func() {
							snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

							Convey("errors when voters dao vote func errors", func() {
								votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(0), int64(0), int64(0), int64(0), false).Return(models.Voter{}, errors.New("test-error"))
								_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
								So(err, ShouldNotBeNil)
							})

							Convey("succeeds when voters dao vote func succeeds", func() {
								votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(0), int64(0), int64(0), int64(0), false).Return(models.Voter{}, nil)
								_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokensEmpty)
								So(err, ShouldBeNil)
								So(len(paydayClient.Calls), ShouldEqual, 0)
								So(len(copoClient.Calls), ShouldEqual, 0)
								So(len(subscriptionClient.Calls), ShouldEqual, 0)
							})
						})
					})
				})
			})
		})
	})
}

func TestController_VoteWithBits(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		getVoterController := new(get_voter_mock.Controller)
		votersDAO := new(voters_dao_mock.VotersDAO)
		votesDAO := new(votes_dao_mock.DAO)
		noopLockingClient := lock.NewNoopLockingClient()
		paydayClient := new(paydayrpc_mock.Payday)
		copoClient := new(copo_mock.Copo)
		datascienceTracker := new(datascience_mock.Tracker)
		subscriptionClient := new(substwirp_mock.Subscriptions)
		snsClient := new(sns_mock.Client)
		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)

		controller := vote.NewController(
			getPollController,
			getVoterController,
			votesDAO,
			votersDAO,
			noopLockingClient,
			paydayClient,
			copoClient,
			datascienceTracker,
			subscriptionClient,
			statter,
			telemetry.NewNoOpSampleReporter(),
			snsClient,
			&config.Config{},
		)

		voteID := "test-vote-id"
		pollID := "test-poll-id"
		userID := "test-user-id"
		channelID := "test-channel-id"
		choiceID := "test-choice-id"

		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when votes dao returns nil", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(nil, nil)

			Convey("errors when get poll controller returns the requested choice and bits are enabled but there's no cost and bits tokens are present in the request", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:             pollID,
					OwnedBy:            channelID,
					Status:             models.PollStatusActive,
					StartTime:          time.Now(),
					Duration:           5 * time.Minute,
					IsBitsVotesEnabled: true,
					BitsVotesCost:      0,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)
				getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, map[models.TokenType]int64{
					models.TokenTypeBits: 1,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("errors when voter is owner of poll", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:             pollID,
					OwnedBy:            channelID,
					Status:             models.PollStatusActive,
					StartTime:          time.Now(),
					Duration:           5 * time.Minute,
					IsBitsVotesEnabled: true,
					BitsVotesCost:      2,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)
				getVoterController.On("GetVoter", mock.Anything, pollID, channelID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

				_, _, err := controller.Vote(context.Background(), voteID, pollID, channelID, choiceID, map[models.TokenType]int64{
					models.TokenTypeBits: 8,
				})

				So(err, ShouldNotBeNil)
			})

			Convey("when get poll controller returns the requested choice and bits are enabled with a proper cost", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:             pollID,
					OwnedBy:            channelID,
					Status:             models.PollStatusActive,
					StartTime:          time.Now(),
					Duration:           5 * time.Minute,
					IsBitsVotesEnabled: true,
					BitsVotesCost:      2,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)
				getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

				Convey("errors when bits in the request are not an even multiple of the cost", func() {
					_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, map[models.TokenType]int64{
						models.TokenTypeBits: 3,
					})
					So(err, ShouldNotBeNil)
				})

				Convey("when bits in the request are an even multiple of the cost", func() {
					tokens := map[models.TokenType]int64{
						models.TokenTypeBits: 8,
					}

					Convey("errors when payday errors", func() {
						paydayClient.On("UseBitsOnPoll", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokens)
						So(err, ShouldNotBeNil)
					})

					Convey("when payday succeeds", func() {
						paydayClient.On("UseBitsOnPoll", mock.Anything, mock.Anything).Return(nil, nil)

						Convey("when vote dao create vote succeeds", func() {
							votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)

							Convey("when sns publish succeeds", func() {
								snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

								Convey("succeeds when voters dao vote func succeeds", func() {
									votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(4), int64(8), int64(0), int64(0), false).Return(models.Voter{}, nil)
									_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokens)
									So(err, ShouldBeNil)
									So(len(paydayClient.Calls), ShouldEqual, 1)
									paydayReq, ok := paydayClient.Calls[0].Arguments[1].(*paydayrpc.UseBitsOnPollReq)
									So(ok, ShouldBeTrue)
									So(paydayReq.UserId, ShouldEqual, userID)
									So(paydayReq.ChannelId, ShouldEqual, channelID)
									So(paydayReq.VoteId, ShouldEqual, voteID)
									So(paydayReq.BitsAmount, ShouldEqual, tokens[models.TokenTypeBits])
									So(paydayReq.PollId, ShouldEqual, pollID)
									So(paydayReq.ChoiceId, ShouldEqual, choiceID)
									So(len(subscriptionClient.Calls), ShouldEqual, 0)
									So(len(copoClient.Calls), ShouldEqual, 0)
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestController_SubOnlyMode(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		getVoterController := new(get_voter_mock.Controller)
		votersDAO := new(voters_dao_mock.VotersDAO)
		votesDAO := new(votes_dao_mock.DAO)
		noopLockingClient := lock.NewNoopLockingClient()
		paydayClient := new(paydayrpc_mock.Payday)
		copoClient := new(copo_mock.Copo)
		datascienceTracker := new(datascience_mock.Tracker)
		subscriptionClient := new(substwirp_mock.Subscriptions)
		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)
		snsClient := new(sns_mock.Client)

		controller := vote.NewController(
			getPollController,
			getVoterController,
			votesDAO,
			votersDAO,
			noopLockingClient,
			paydayClient,
			copoClient,
			datascienceTracker,
			subscriptionClient,
			statter,
			telemetry.NewNoOpSampleReporter(),
			snsClient,
			&config.Config{},
		)

		voteID := "test-vote-id"
		pollID := "test-poll-id"
		userID := "test-user-id"
		channelID := "test-channel-id"
		choiceID := "test-choice-id"

		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when votes dao returns nil", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(nil, nil)

			Convey("when get poll controller returns a sub only poll", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:                  pollID,
					OwnedBy:                 channelID,
					Status:                  models.PollStatusActive,
					StartTime:               time.Now(),
					Duration:                5 * time.Minute,
					IsSubscriberOnlyEnabled: true,
					IsBitsVotesEnabled:      true,
					BitsVotesCost:           2,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)

				Convey("errors when get voter returns a non subbed voter", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{
						PollID:          pollID,
						UserID:          userID,
						HasSubscription: false,
					}, nil)

					_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, nil)
					So(err, ShouldNotBeNil)
					So(len(subscriptionClient.Calls), ShouldEqual, 0)
				})

				Convey("when get voter returns a subbed voter", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{
						PollID:          pollID,
						UserID:          userID,
						HasSubscription: true,
					}, nil)

					Convey("when vote dao create vote succeeds", func() {
						votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)

						Convey("when sns publish succeeds", func() {
							snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

							Convey("succeeds when voters dao vote func succeeds", func() {
								votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(0), int64(0), int64(0), int64(0), true).Return(models.Voter{}, nil)
								_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, nil)
								So(err, ShouldBeNil)
								So(len(subscriptionClient.Calls), ShouldEqual, 0)
							})
						})
					})
				})

				Convey("when get voter returns not found", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

					Convey("errors when subs service call fails", func() {
						subscriptionClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, nil)
						So(err, ShouldNotBeNil)
					})

					Convey("errors when subs service indicates the user is not subbed", func() {
						subscriptionClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(&substwirp.GetUserChannelSubscriptionResponse{
							Subscription: nil,
						}, nil)
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, nil)
						So(err, ShouldNotBeNil)
					})

					Convey("when subs service indicates the user is subbed", func() {
						subscriptionClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(&substwirp.GetUserChannelSubscriptionResponse{
							Subscription: &substwirp.Subscription{},
						}, nil)

						Convey("when vote dao create vote succeeds", func() {
							votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)

							Convey("when sns publish succeeds", func() {
								snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

								Convey("succeeds when voters dao vote func succeeds", func() {
									votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(0), int64(0), int64(0), int64(0), true).Return(models.Voter{}, nil)
									_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, nil)
									So(err, ShouldBeNil)
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestController_SubMultiplier(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		getVoterController := new(get_voter_mock.Controller)
		votersDAO := new(voters_dao_mock.VotersDAO)
		votesDAO := new(votes_dao_mock.DAO)
		noopLockingClient := lock.NewNoopLockingClient()
		paydayClient := new(paydayrpc_mock.Payday)
		datascienceTracker := new(datascience_mock.Tracker)
		subscriptionClient := new(substwirp_mock.Subscriptions)
		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)
		snsClient := new(sns_mock.Client)

		controller := vote.NewController(
			getPollController,
			getVoterController,
			votesDAO,
			votersDAO,
			noopLockingClient,
			paydayClient,
			nil,
			datascienceTracker,
			subscriptionClient,
			statter,
			telemetry.NewNoOpSampleReporter(),
			snsClient,
			&config.Config{},
		)

		voteID := "test-vote-id"
		pollID := "test-poll-id"
		userID := "test-user-id"
		channelID := "test-channel-id"
		choiceID := "test-choice-id"

		tokens := map[models.TokenType]int64{
			models.TokenTypeBits: 8,
		}

		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when votes dao returns nil", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(nil, nil)

			Convey("when get poll controller returns a sub only poll", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:                        pollID,
					OwnedBy:                       channelID,
					Status:                        models.PollStatusActive,
					StartTime:                     time.Now(),
					Duration:                      5 * time.Minute,
					IsSubscriberMultiplierEnabled: true,
					IsBitsVotesEnabled:            true,
					BitsVotesCost:                 2,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)

				Convey("when get voter returns not found", func() {
					getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

					Convey("when subs service indicates the user is not subbed", func() {
						subscriptionClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(&substwirp.GetUserChannelSubscriptionResponse{
							Subscription: nil,
						}, nil)

						Convey("when the payday call succeeds", func() {
							paydayClient.On("UseBitsOnPoll", mock.Anything, mock.Anything).Return(nil, nil)

							Convey("when vote dao create vote succeeds", func() {
								votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)

								Convey("when then sns publish succeeds", func() {
									snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

									Convey("succeeds when voters dao vote func succeeds", func() {
										votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(4), int64(8), int64(0), int64(0), false).Return(models.Voter{}, nil)
										_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokens)
										So(err, ShouldBeNil)
										So(len(paydayClient.Calls), ShouldEqual, 1)
										paydayReq, ok := paydayClient.Calls[0].Arguments[1].(*paydayrpc.UseBitsOnPollReq)
										So(ok, ShouldBeTrue)
										So(paydayReq.BitsAmount, ShouldEqual, tokens[models.TokenTypeBits])
									})
								})
							})
						})
					})

					Convey("when subs service indicates the user is subbed", func() {
						subscriptionClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(&substwirp.GetUserChannelSubscriptionResponse{
							Subscription: &substwirp.Subscription{},
						}, nil)

						Convey("when the payday call succeeds", func() {
							paydayClient.On("UseBitsOnPoll", mock.Anything, mock.Anything).Return(nil, nil)

							Convey("when vote dao create vote succeeds", func() {
								votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)

								Convey("when then sns publish succeeds", func() {
									snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

									Convey("succeeds when voters dao vote func succeeds", func() {
										votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(2), int64(8), int64(8), int64(0), int64(0), true).Return(models.Voter{}, nil)
										_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokens)
										So(err, ShouldBeNil)
										So(len(paydayClient.Calls), ShouldEqual, 1)
										paydayReq, ok := paydayClient.Calls[0].Arguments[1].(*paydayrpc.UseBitsOnPollReq)
										So(ok, ShouldBeTrue)
										So(paydayReq.BitsAmount, ShouldEqual, tokens[models.TokenTypeBits])
									})
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestController_VoteWithChannelPoints(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		getVoterController := new(get_voter_mock.Controller)
		votersDAO := new(voters_dao_mock.VotersDAO)
		votesDAO := new(votes_dao_mock.DAO)
		noopLockingClient := lock.NewNoopLockingClient()
		paydayClient := new(paydayrpc_mock.Payday)
		copoClient := new(copo_mock.Copo)
		datascienceTracker := new(datascience_mock.Tracker)
		subscriptionClient := new(substwirp_mock.Subscriptions)
		snsClient := new(sns_mock.Client)
		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)

		controller := vote.NewController(
			getPollController,
			getVoterController,
			votesDAO,
			votersDAO,
			noopLockingClient,
			paydayClient,
			copoClient,
			datascienceTracker,
			subscriptionClient,
			statter,
			telemetry.NewNoOpSampleReporter(),
			snsClient,
			&config.Config{},
		)

		voteID := "test-vote-id"
		pollID := "test-poll-id"
		userID := "test-user-id"
		channelID := "test-channel-id"
		choiceID := "test-choice-id"

		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when votes dao returns nil", func() {
			votesDAO.On("GetVote", mock.Anything, pollID, voteID, true).Return(nil, nil)

			Convey("errors when get poll controller returns the requested choice and channel points are enabled but there's no cost and channel points tokens are present in the request", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:                      pollID,
					OwnedBy:                     channelID,
					Status:                      models.PollStatusActive,
					StartTime:                   time.Now(),
					Duration:                    5 * time.Minute,
					IsChannelPointsVotesEnabled: true,
					ChannelPointsVotesCost:      0,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)
				getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

				_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, map[models.TokenType]int64{
					models.TokenTypeChannelPoints: 1,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("when get poll controller returns the requested choice and channel points are enabled with a proper cost", func() {
				getPollController.On("GetPoll", mock.Anything, pollID, false).Return(models.Poll{
					PollID:                      pollID,
					OwnedBy:                     channelID,
					Status:                      models.PollStatusActive,
					StartTime:                   time.Now(),
					Duration:                    5 * time.Minute,
					IsChannelPointsVotesEnabled: true,
					ChannelPointsVotesCost:      20,
				}, []models.Choice{{ChoiceID: choiceID}}, nil, nil, nil)
				getVoterController.On("GetVoter", mock.Anything, pollID, userID).Return(models.Poll{}, models.Voter{}, poliwag_errors.NotFound.New("not found"))

				Convey("errors when channel points in the request are not an even multiple of the cost", func() {
					_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, map[models.TokenType]int64{
						models.TokenTypeChannelPoints: 30,
					})
					So(err, ShouldNotBeNil)
				})

				Convey("when channel points in the request are an even multiple of the cost", func() {
					tokens := map[models.TokenType]int64{
						models.TokenTypeChannelPoints: 80,
					}

					Convey("errors when copo errors", func() {
						copoClient.On("SubtractPoints", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
						_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokens)
						So(err, ShouldNotBeNil)
					})

					Convey("when copo succeeds", func() {
						copoClient.On("SubtractPoints", mock.Anything, mock.Anything).Return(&copo.SubtractPointsResponse{Balance: &copo.Balance{UserId: userID, ChannelId: channelID, Balance: 100}}, nil)

						Convey("when vote dao create vote succeeds", func() {
							votesDAO.On("CreateVote", mock.Anything, mock.Anything).Return(nil)

							Convey("when sns publish succeeds", func() {
								snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)

								Convey("succeeds when voters dao vote func succeeds", func() {
									votersDAO.On("SaveVoterAndIncrementTotals", mock.Anything, pollID, choiceID, userID, int64(1), int64(0), int64(0), int64(4), int64(80), false).Return(models.Voter{}, nil)
									_, _, err := controller.Vote(context.Background(), voteID, pollID, userID, choiceID, tokens)
									So(err, ShouldBeNil)
									So(len(copoClient.Calls), ShouldEqual, 1)
									copoReq, ok := copoClient.Calls[0].Arguments[1].(*copo.SubtractPointsRequest)
									So(ok, ShouldBeTrue)
									So(copoReq.UserId, ShouldEqual, userID)
									So(copoReq.ChannelId, ShouldEqual, channelID)
									So(copoReq.Reason, ShouldEqual, copo.SubtractPointsReason_SUBTRACT_POINTS_VOTE_IN_POLL)
									So(copoReq.Points, ShouldEqual, tokens[models.TokenTypeChannelPoints])
									So(copoReq.GetReasonArgs().GetPollId(), ShouldEqual, pollID)
									So(len(subscriptionClient.Calls), ShouldEqual, 0)
									So(len(paydayClient.Calls), ShouldEqual, 0)
								})
							})
						})
					})
				})
			})
		})
	})
}
