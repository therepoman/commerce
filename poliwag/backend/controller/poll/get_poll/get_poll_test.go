package get_poll_test

import (
	"context"
	"errors"
	"testing"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestController_GetPoll(t *testing.T) {
	Convey("given a poll controller", t, func() {
		choicesDAO := new(choices_dao_mock.ChoicesDAO)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)

		controller := get_poll.NewController(choicesDAO, pollsDAO, statter, telemetry.NewNoOpSampleReporter())

		userID := "test-user-id"
		pollID := "test-poll-id"

		choices := []models.Choice{
			{
				ChoiceID: "choice-id-0",
				Title:    "Final Fantasy VII",
			},
			{
				ChoiceID: "choice-id-1",
				Title:    "Chrono Trigger",
			},
		}

		topBits := &models.TopBitsContributor{
			UserID:          "1234",
			BitsContributed: 10,
		}

		topChannelPoints := &models.TopChannelPointsContributor{
			UserID:                   "5678",
			ChannelPointsContributed: 100,
		}

		Convey("with error getting poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, mock.Anything, false).Return(nil, errors.New("ERROR"))

			_, _, _, _, err := controller.GetPoll(context.Background(), "1", false)
			So(err, ShouldNotBeNil)
		})

		Convey("with nil dynamo record", func() {
			pollsDAO.On("GetPoll", mock.Anything, mock.Anything, false).Return(nil, nil)

			_, _, _, _, err := controller.GetPoll(context.Background(), "1", false)
			So(err, ShouldNotBeNil)
		})

		Convey("when dynamo getPoll succeeds", func() {
			ownedPoll := models.Poll{
				PollID:  pollID,
				OwnedBy: userID,
			}

			pollsDAO.On("GetPoll", mock.Anything, mock.Anything, false).Return(&ownedPoll, nil)

			Convey("when GetChoicesByPollID returns an error", func() {
				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return(nil, errors.New("ERROR"))

				_, _, _, _, err := controller.GetPoll(context.Background(), pollID, false)
				So(err, ShouldNotBeNil)
			})

			Convey("when GetChoicesByPollID returns choices", func() {
				choicesDAO.On("GetChoicesByPollID", mock.Anything, pollID).Return(choices, nil)

				Convey("when GetTopContributorsFromPantheon returns an error", func() {
					pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, pollID).Return(nil, nil, errors.New("ERROR"))

					_, _, _, _, err := controller.GetPoll(context.Background(), pollID, false)
					So(err, ShouldNotBeNil)
				})

				Convey("when GetTopContributorsFromPantheon returns successfully", func() {
					pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, pollID).Return(topBits, topChannelPoints, nil)

					actualPoll, actualChoices, actualTopBits, actualTopChannelPoints, err := controller.GetPoll(context.Background(), pollID, false)

					So(actualPoll, ShouldResemble, ownedPoll)
					So(actualChoices, ShouldResemble, choices)
					So(actualTopBits, ShouldResemble, topBits)
					So(actualTopChannelPoints, ShouldResemble, topChannelPoints)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
