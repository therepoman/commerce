package get_poll

import (
	"context"
	"fmt"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Controller interface {
	GetPoll(ctx context.Context, pollID string, consistentRead bool) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error)
}

type controller struct {
	choicesDAO     choices_dao.ChoicesDAO
	pollsDAO       polls_dao.PollsDAO
	stats          statsd.Statter
	sampleReporter *telemetry.SampleReporter
}

func NewController(
	choicesDAO choices_dao.ChoicesDAO,
	pollsDAO polls_dao.PollsDAO,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
) Controller {
	return &controller{
		choicesDAO:     choicesDAO,
		pollsDAO:       pollsDAO,
		stats:          stats,
		sampleReporter: sampleReporter,
	}
}

func (c controller) GetPoll(ctx context.Context, pollID string, consistentRead bool) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error) {
	// Get the poll from dynamo
	poll, err := c.pollsDAO.GetPoll(ctx, pollID, consistentRead)
	if err != nil {
		msg := fmt.Sprintf("error getting poll with id %s", pollID)
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	// Poll does not exist
	if poll == nil {
		return models.Poll{}, nil, nil, nil, errors.NotFound.New("poll %s does not exist", pollID)
	}

	// Get choices associated with the poll from dynamo
	choices, err := c.choicesDAO.GetChoicesByPollID(ctx, poll.PollID)
	if err != nil {
		msg := fmt.Sprintf("error getting choices for poll id %s", poll.PollID)
		logrus.WithField("pollID", poll.PollID).WithError(err).Error(msg)
		return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	if len(choices) == 0 {
		logrus.WithField("pollID", poll.PollID).Warn("retrieved 0 choices for poll in GetPoll")
		statErr := c.stats.Inc(models.ZERO_CHOICE_METRIC_NAME, 1, 1.0)
		if statErr != nil {
			logrus.WithField("pollID", poll.PollID).WithError(statErr).Error("error tracking zero choice metric in GetPoll")
		}

		c.sampleReporter.Report(models.ZERO_CHOICE_METRIC_NAME, 1.0, telemetry.UnitCount)
	}

	var topBits *models.TopBitsContributor
	var topChannelPoints *models.TopChannelPointsContributor
	if poll.Status != models.PollStatusActive {
		topBits, topChannelPoints, err = c.pollsDAO.GetTopContributorsFromPantheon(ctx, pollID)
		if err != nil {
			msg := fmt.Sprintf("error getting top contributors for pollID %s", pollID)
			logrus.WithField("pollID", pollID).WithError(err).Error(msg)
			return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
		}
	}

	return *poll, choices, topBits, topChannelPoints, nil
}
