package create_poll

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/copo/proto/copo"
	payday "code.justin.tv/commerce/payday/client"
	payday_api "code.justin.tv/commerce/payday/models/api"
	viewable_poll_cache "code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/controller/slack"
	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_complete_v1"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_update_v1"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	"code.justin.tv/commerce/poliwag/clients/datascience"
	ripley "code.justin.tv/revenue/ripley/rpc"
)

// Redefining here - don't want API layer to reference DAO directly
// In the future we may add more or less args to the Controller structs
// Which will remove this type assertion in favor of Controller defined structs
type CreateChoiceArgs = choices_dao.CreateChoiceArgs

type Controller interface {
	CreatePoll(ctx context.Context, userID string, ownedBy string, title string, duration time.Duration, createChoiceArgs []CreateChoiceArgs, isMultiChoiceEnabled bool, isSubscriberOnlyEnabled bool, isSubscriberMultiplierEnabled bool, isBitsVotesEnabled bool, bitsVotesCost int, isChannelPointsVotesEnabled bool, channelPointsVotesCost int) (models.Poll, []models.Choice, error)
}

type controller struct {
	copoClient             copo.Copo
	datascienceTracker     datascience.Tracker
	paydayClient           payday.Client
	ripleyClient           ripley.Ripley
	pollsDAO               polls_dao.PollsDAO
	choicesDAO             choices_dao.ChoicesDAO
	viewablePollCache      viewable_poll_cache.ViewablePollCache
	eventbusController     eventbus.Controller
	pubsubController       pubsub.Controller
	slackController        slack.Controller
	pollUpdateV1Executor   poll_update_v1.Executor
	pollCompleteV1Executor poll_complete_v1.Executor
	stats                  statsd.Statter
	sampleReporter         *telemetry.SampleReporter
}

func NewController(
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
	copoClient copo.Copo,
	datascienceTracker datascience.Tracker,
	paydayClient payday.Client,
	ripleyClient ripley.Ripley,
	pollsDAO polls_dao.PollsDAO,
	choicesDAO choices_dao.ChoicesDAO,
	viewablePollCache viewable_poll_cache.ViewablePollCache,
	eventbusController eventbus.Controller,
	pubsubController pubsub.Controller,
	slackController slack.Controller,
	pollUpdateV1Executor poll_update_v1.Executor,
	pollCompleteV1Executor poll_complete_v1.Executor,
) Controller {
	return &controller{
		copoClient:             copoClient,
		datascienceTracker:     datascienceTracker,
		paydayClient:           paydayClient,
		ripleyClient:           ripleyClient,
		pollsDAO:               pollsDAO,
		choicesDAO:             choicesDAO,
		viewablePollCache:      viewablePollCache,
		eventbusController:     eventbusController,
		pubsubController:       pubsubController,
		slackController:        slackController,
		pollUpdateV1Executor:   pollUpdateV1Executor,
		pollCompleteV1Executor: pollCompleteV1Executor,
		stats:                  stats,
		sampleReporter:         sampleReporter,
	}
}

func (c controller) trackMetrics(metric string, startTime time.Time, err error) {
	d := time.Since(startTime)
	go func(m string, dur time.Duration) {
		statErr := c.stats.TimingDuration(m, dur, 1.0)
		if statErr != nil {
			logrus.WithField("metric", m).WithError(err).Error("error tracking latency metric")
		}
	}(metric+"_Latency", d)

	go func(m string, err error) {
		errCount := int64(0)
		if err != nil {
			errCount = 1
		}
		statErr := c.stats.Inc(m, errCount, 1.0)
		if statErr != nil {
			logrus.WithField("metric", m).WithError(err).Error("error tracking error count metric")
		}
	}(metric+"_Errors", err)
}

func (c controller) CreatePoll(ctx context.Context, userID string, ownedBy string, title string, duration time.Duration,
	createChoiceArgs []CreateChoiceArgs, isMultiChoiceEnabled bool, isSubscriberOnlyEnabled bool, isSubscriberMultiplierEnabled bool,
	isBitsVotesEnabled bool, bitsVotesCost int,
	isChannelPointsVotesEnabled bool, channelPointsVotesCost int) (models.Poll, []models.Choice, error) {
	if isBitsVotesEnabled {
		beforePaydayCall := time.Now()
		var paydayChannel *payday_api.ChannelEligibleResponse
		var paydayError error

		err := hystrix.CallTwirpWithHystrix(hystrix.PaydayGetChannelInfoCommand, hystrix.HystrixDo, func() error {
			paydayChannel, paydayError = c.paydayClient.GetChannelInfo(ctx, ownedBy, nil)
			return paydayError
		})

		c.trackMetrics("CreatePoll_PaydayCall", beforePaydayCall, err)
		if paydayChannel == nil || err != nil {
			msg := fmt.Sprintf("error fetching channel info from Payday for ownedBy %s", ownedBy)
			logrus.WithField("ownedBy", ownedBy).WithError(err).Error(msg)
			return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
		}

		if !paydayChannel.Eligible {
			return models.Poll{}, nil, errors.BitsNotEnabled.New("ownedBy %s does not have bits enabled", ownedBy)
		}
	} else {
		bitsVotesCost = 0
	}

	if isChannelPointsVotesEnabled {
		beforeCopoCall := time.Now()
		var copoChannelSettings *copo.GetChannelSettingsResponse
		var copoError error

		err := hystrix.CallTwirpWithHystrix(hystrix.CopoGetChannelSettings, hystrix.HystrixDo, func() error {
			copoChannelSettings, copoError = c.copoClient.GetChannelSettings(ctx, &copo.GetChannelSettingsRequest{ChannelId: ownedBy})
			return copoError
		})

		c.trackMetrics("CreatePoll_CopoCall", beforeCopoCall, err)
		if copoChannelSettings == nil || err != nil {
			msg := fmt.Sprintf("error fetching channel info from Copo for ownedBy %s", ownedBy)
			logrus.WithField("ownedBy", ownedBy).WithError(err).Error(msg)
			return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
		}

		if !copoChannelSettings.GetSettings().GetEnabled() {
			return models.Poll{}, nil, errors.ChannelPointsNotEnabled.New("ownedBy %s does not have channel points enabled", ownedBy)
		}
	} else {
		channelPointsVotesCost = 0
	}

	// Make sure the ownedBy is a partner or affiliate
	beforeRipleyCall := time.Now()
	var ripleyResp *ripley.GetPayoutTypeResponse
	var ripleyError error

	err := hystrix.CallTwirpWithHystrix(hystrix.RipleyGetPayoutTypeCommand, hystrix.HystrixDo, func() error {
		ripleyResp, ripleyError = c.ripleyClient.GetPayoutType(ctx, &ripley.GetPayoutTypeRequest{
			ChannelId: ownedBy,
		})
		return ripleyError
	})

	c.trackMetrics("CreatePoll_RipleyCall", beforeRipleyCall, err)
	if err != nil {
		msg := fmt.Sprintf("error fetching partner/affiliate status from Ripley for ownedBy %s", ownedBy)
		logrus.WithField("ownedBy", ownedBy).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	if !ripleyResp.PayoutType.IsAffiliate && !ripleyResp.PayoutType.IsPartner {
		return models.Poll{}, nil, errors.PermissionDenied.New("ownedBy %s is not a partner or affiliate", ownedBy)
	}

	// Check that there is no active poll for ownedBy
	beforeDynamoGetCall := time.Now()
	activePolls, _, err := c.pollsDAO.GetPollsByOwnedBy(ctx, ownedBy, []models.PollStatus{models.PollStatusActive}, models.PollSortStartTime, models.DirectionDescending, 20, nil)
	c.trackMetrics("CreatePoll_DynamoGetActivePolls", beforeDynamoGetCall, err)
	if err != nil {
		msg := fmt.Sprintf("error getting active polls for ownedBy %s", ownedBy)
		logrus.WithField("ownedBy", ownedBy).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	if len(activePolls) > 0 {
		if len(activePolls) > 1 {
			var activePollIDs []string
			for _, activePoll := range activePolls {
				activePollIDs = append(activePollIDs, activePoll.PollID)
			}

			logrus.WithFields(logrus.Fields{
				"ownedBy":       ownedBy,
				"activePollIDs": strings.Join(activePollIDs, ","),
			}).Errorf("found multiple active polls for ownedBy %s", ownedBy)
		}

		return models.Poll{}, nil, errors.PollAlreadyActive.New("active poll found for ownedBy %s. only one poll may be active for an owner at a time.", ownedBy)
	}

	// Validation complete - create the poll and choices
	pollID, err := uuid.NewV4()
	if err != nil {
		msg := fmt.Sprintf("error generating a new poll ID for userID %s in ownedBy %s", userID, ownedBy)
		logrus.WithFields(logrus.Fields{
			"userID":  userID,
			"ownedBy": ownedBy,
		}).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	beforeDynamoCreateChoicesCall := time.Now()
	choices, err := c.choicesDAO.CreateChoices(ctx, pollID.String(), createChoiceArgs)
	c.trackMetrics("CreatePoll_DynamoCreateChoices", beforeDynamoCreateChoicesCall, err)
	if err != nil {
		msg := fmt.Sprintf("error creating choices for pollID %s for userID %s in ownedBy %s", pollID.String(), userID, ownedBy)
		logrus.WithFields(logrus.Fields{
			"ownedBy": ownedBy,
			"userID":  userID,
			"pollID":  pollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	beforeDynamoCreatePollCall := time.Now()
	newPoll, err := c.pollsDAO.CreatePoll(
		ctx,
		pollID.String(), userID, ownedBy,
		title, duration, isMultiChoiceEnabled,
		isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled,
		isBitsVotesEnabled, bitsVotesCost,
		isChannelPointsVotesEnabled, channelPointsVotesCost, "")
	c.trackMetrics("CreatePoll_DynamoCreatePoll", beforeDynamoCreatePollCall, err)
	if err != nil {
		msg := fmt.Sprintf("error creating newPoll for userID %s in ownedBy %s", userID, ownedBy)
		logrus.WithFields(logrus.Fields{
			"ownedBy": ownedBy,
			"userID":  userID,
			"pollID":  pollID.String(),
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	// Update the cache
	if err := c.viewablePollCache.Set(ctx, ownedBy, viewable_poll_cache.ViewablePollJustCreated); err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID":  newPoll.PollID,
			"ownedBy": ownedBy,
		}).WithError(err).Warnf("error setting viewable poll cache for owned by %s", ownedBy)
	}

	// Send the create poll pubsub
	beforeEmitPubsub := time.Now()
	err = c.pubsubController.SendPollCreatePubsub(ctx, newPoll.PollID)
	c.trackMetrics("CreatePoll_EmitPubsub", beforeEmitPubsub, err)
	if err != nil {
		msg := fmt.Sprintf("error sending poll create pubsub for pollID %s", newPoll.PollID)
		logrus.WithFields(logrus.Fields{
			"pollID": newPoll.PollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	// Send the poll create eventbus event
	c.eventbusController.PublishPollCreate(ctx, newPoll, choices)

	// Now start the poll complete step function execution
	beforeStepfnStart := time.Now()
	err = c.pollCompleteV1Executor.Execute(ctx, poll_complete_v1.PollCompleteRequest{
		PollID:              newPoll.PollID,
		PollDurationSeconds: int(newPoll.Duration.Seconds()),
		PollStartTime:       newPoll.StartTime,
	})
	c.trackMetrics("CreatePoll_StartStepFN", beforeStepfnStart, err)
	if err != nil {
		msg := fmt.Sprintf("error starting poll end sfn for pollID %s", newPoll.PollID)
		logrus.WithFields(logrus.Fields{
			"userID": userID,
			"pollID": newPoll.PollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	go func(newPollID string) {
		postToSlackCtx, cancel := context.WithTimeout(context.Background(), time.Duration(4)*time.Second)
		defer cancel()

		err := c.slackController.PostPollStart(postToSlackCtx, newPollID)
		if err != nil {
			logrus.WithField("pollID", newPollID).WithError(err).Info("failed to post poll start to slack")
		}
	}(newPoll.PollID)

	c.datascienceTracker.TrackEventAsync(datascience.CreatePoll, datascienceModel(newPoll, choices))
	return newPoll, choices, nil
}

func datascienceModel(poll models.Poll, choices []models.Choice) models.CreatePollProperties {
	cpp := models.CreatePollProperties{
		UserID:               poll.CreatedBy,
		ChannelID:            poll.OwnedBy,
		PollID:               poll.PollID,
		PollQuestion:         poll.Title,
		TotalResponses:       len(choices),
		BitsPerVote:          poll.BitsVotesCost,
		ChannelPointsPerVote: poll.ChannelPointsVotesCost,
		HasSubscriberBonus:   poll.IsSubscriberMultiplierEnabled,
		IsSubscriberOnly:     poll.IsSubscriberOnlyEnabled,
		PollDurationSeconds:  int(poll.Duration.Seconds()),
	}

	for _, choice := range choices {
		switch choice.Index {
		case 0:
			cpp.PollResponse1 = choice.Title
		case 1:
			cpp.PollResponse2 = choice.Title
		case 2:
			cpp.PollResponse3 = choice.Title
		case 3:
			cpp.PollResponse4 = choice.Title
		case 4:
			cpp.PollResponse5 = choice.Title
		}
	}
	return cpp
}
