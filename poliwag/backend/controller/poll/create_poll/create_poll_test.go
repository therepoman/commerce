package create_poll_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/copo/proto/copo"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_complete_v1"
	copo_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/copo/proto/copo"
	payday_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/payday/client"
	viewable_poll_cache_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	eventbus_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	pubsub_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	slack_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/slack"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	poll_complete_v1_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_complete_v1"
	poll_update_v1_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_update_v1"
	datascience_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/clients/datasceince"
	ripley_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/revenue/ripley/rpc"
	ripley "code.justin.tv/revenue/ripley/rpc"
)

func TestController_CreatePoll(t *testing.T) {
	Convey("given a poll controller", t, func() {
		ripleyClient := new(ripley_client_mock.Ripley)
		choicesDAO := new(choices_dao_mock.ChoicesDAO)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		pubsubController := new(pubsub_mock.Controller)
		eventbusController := new(eventbus_mock.Controller)
		slackController := new(slack_mock.Controller)
		pollCompleteExecutor := new(poll_complete_v1_mock.Executor)
		pollUpdateExecutor := new(poll_update_v1_mock.Executor)
		datascienceTracker := new(datascience_mock.Tracker)
		paydayClient := new(payday_mock.Client)
		copoClient := new(copo_mock.Copo)
		viewablePollCache := new(viewable_poll_cache_mock.ViewablePollCache)
		statter, err := statsd.NewNoopClient()
		sampleReporter := telemetry.NewNoOpSampleReporter()
		So(err, ShouldBeNil)

		controller := create_poll.NewController(
			statter,
			sampleReporter,
			copoClient,
			datascienceTracker,
			paydayClient,
			ripleyClient,
			pollsDAO,
			choicesDAO,
			viewablePollCache,
			eventbusController,
			pubsubController,
			slackController,
			pollUpdateExecutor,
			pollCompleteExecutor,
		)

		userID := "test-user-id"
		ownedBy := "another-test-user-id"

		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when bits votes are enabled", func() {
			isBitsVotesEnabled := true
			isChannelPointsVotesEnabled := false

			Convey("When payday GetChannelInfo errors", func() {
				paydayClient.On("GetChannelInfo", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))

				Convey("we should return an error", func() {
					_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 0)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When payday GetChannelInfo returns a channel that does not support bits", func() {
				paydayClient.On("GetChannelInfo", mock.Anything, mock.Anything, mock.Anything).Return(&api.ChannelEligibleResponse{
					Eligible: false,
				}, nil)

				Convey("we should return an error when trying to create a bits enabled poll", func() {
					_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 0)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("when channel points votes are enabled", func() {
			isBitsVotesEnabled := false
			isChannelPointsVotesEnabled := true

			Convey("When copo GetChannelSettings errors", func() {
				copoClient.On("GetChannelSettings", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))

				Convey("we should return an error", func() {
					_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 1)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When copo GetChannelSettings returns a channel that does not have channel points enabled", func() {
				copoClient.On("GetChannelSettings", mock.Anything, mock.Anything).Return(&copo.GetChannelSettingsResponse{
					Settings: &copo.ChannelSettings{
						ChannelId: ownedBy,
						Enabled:   false,
					},
				}, nil)

				Convey("we should return an error when trying to create a channel points enabled enabled poll", func() {
					_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 1)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("when bits and channel points votes are not enabled", func() {
			isBitsVotesEnabled := false
			isChannelPointsVotesEnabled := false

			Convey("when GetPayoutType errors", func() {
				ripleyClient.On("GetPayoutType", mock.Anything, &ripley.GetPayoutTypeRequest{
					ChannelId: ownedBy,
				}).Return(nil, errors.New("ERROR"))

				Convey("we should return an error", func() {
					_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 0)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetPayoutType returns false for partner and affiliate", func() {
				ripleyClient.On("GetPayoutType", mock.Anything, &ripley.GetPayoutTypeRequest{
					ChannelId: ownedBy,
				}).Return(&ripley.GetPayoutTypeResponse{
					PayoutType: &ripley.PayoutType{
						IsAffiliate: false,
						IsPartner:   false,
					},
				}, nil)

				Convey("we should return an error", func() {
					_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 0)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetPayoutType returns true for partner or affiliate", func() {
				ripleyClient.On("GetPayoutType", mock.Anything, &ripley.GetPayoutTypeRequest{
					ChannelId: ownedBy,
				}).Return(&ripley.GetPayoutTypeResponse{
					PayoutType: &ripley.PayoutType{
						IsAffiliate: true,
						IsPartner:   false,
					},
				}, nil)

				Convey("when PollsDAO.GetPollsByOwnedBy errors", func() {
					pollsDAO.On("GetPollsByOwnedBy", mock.Anything, ownedBy, []models.PollStatus{models.PollStatusActive}, models.PollSortStartTime, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil, errors.New("ERROR"))

					Convey("we should return an error", func() {
						_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 0)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when PollsDAO.GetPollsByOwnedBy returns an active poll", func() {
					activePolls := []models.Poll{
						{
							PollID: "test-poll-id",
							Status: models.PollStatusActive,
						},
					}

					pollsDAO.On("GetPollsByOwnedBy", mock.Anything, ownedBy, []models.PollStatus{models.PollStatusActive}, models.PollSortStartTime, mock.Anything, mock.Anything, mock.Anything).Return(activePolls, nil, nil)

					Convey("we should return an error", func() {
						_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, "", time.Duration(100), nil, false, false, false, isBitsVotesEnabled, 0, isChannelPointsVotesEnabled, 0)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when PollsDAO.GetPollsByOwnedBy does not return any active polls", func() {
					pollID := "test-poll-id"
					title := "test-title"
					createChoiceArgs := []create_poll.CreateChoiceArgs{
						{
							Title: "Final Fantasy VII",
						},
						{
							Title: "Chrono Trigger",
						},
					}
					duration := time.Duration(100)
					isMultiChoiceEnabled := false
					isSubscriberOnlyEnabled := false
					isSubscriberMultiplierEnabled := false
					isBitsVotesEnabled := false
					bitsVotesCost := 0
					isChannelPointsVotesEnabled := false
					channelPointsVotesCost := 0

					pollsDAO.On("GetPollsByOwnedBy", mock.Anything, ownedBy, []models.PollStatus{models.PollStatusActive}, models.PollSortStartTime, mock.Anything, mock.Anything, mock.Anything).Return([]models.Poll{}, nil, nil)

					Convey("when ChoicesDAO.CreateChoices errors", func() {
						choicesDAO.On("CreateChoices", mock.Anything, mock.Anything, createChoiceArgs).Return(nil, errors.New("ERROR"))

						Convey("we should return an error", func() {
							_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, title, duration, createChoiceArgs, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("when ChoicesDAO.CreateChoices returns new choices", func() {
						newChoices := []models.Choice{
							{
								Title:    "Final Fantasy VII",
								PollID:   pollID,
								ChoiceID: "choice-id-0",
							},
							{
								Title:    "Chrono Trigger",
								PollID:   pollID,
								ChoiceID: "choice-id-1",
							},
						}

						choicesDAO.On("CreateChoices", mock.Anything, mock.Anything, createChoiceArgs).Return(newChoices, nil)
						viewablePollCache.On("Set", mock.Anything, ownedBy, viewable_poll.ViewablePollJustCreated).Return(nil)

						Convey("when PollsDAO.CreatePoll errors", func() {
							pollsDAO.On("CreatePoll", mock.Anything, mock.Anything, userID, ownedBy, title, duration, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost, "").Return(models.Poll{}, errors.New("ERROR"))

							Convey("we should return an error", func() {
								_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, title, duration, createChoiceArgs, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost)

								So(err, ShouldNotBeNil)
							})
						})

						Convey("when PollsDAO.CreatePoll creates a new poll", func() {
							newPoll := models.Poll{
								PollID:                        pollID,
								Title:                         title,
								Duration:                      duration,
								IsMultiChoiceEnabled:          isMultiChoiceEnabled,
								IsSubscriberOnlyEnabled:       isSubscriberOnlyEnabled,
								IsSubscriberMultiplierEnabled: isSubscriberMultiplierEnabled,
								IsBitsVotesEnabled:            isBitsVotesEnabled,
								BitsVotesCost:                 bitsVotesCost,
								IsChannelPointsVotesEnabled:   isChannelPointsVotesEnabled,
								ChannelPointsVotesCost:        channelPointsVotesCost,
							}

							pollsDAO.On("CreatePoll", mock.Anything, mock.Anything, userID, ownedBy, title, duration, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost, "").Return(newPoll, nil)

							Convey("when PubsubController.SendPollCreatePubsub returns an error", func() {
								pubsubController.On("SendPollCreatePubsub", mock.Anything, newPoll.PollID).Return(errors.New("ERROR"))

								Convey("we should return an error", func() {
									_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, title, duration, createChoiceArgs, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost)

									So(err, ShouldNotBeNil)
								})
							})

							Convey("when PollCompleteV1Executor.Execute returns an error", func() {
								pubsubController.On("SendPollCreatePubsub", mock.Anything, newPoll.PollID).Return(nil)
								eventbusController.On("PublishPollCreate", mock.Anything, newPoll, newChoices).Return()
								pollCompleteExecutor.On("Execute", mock.Anything, poll_complete_v1.PollCompleteRequest{
									PollID:              newPoll.PollID,
									PollDurationSeconds: int(newPoll.Duration.Seconds()),
									PollStartTime:       newPoll.StartTime,
								}).Return(errors.New("test-error"))

								Convey("we should return an error", func() {
									_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, title, duration, createChoiceArgs, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost)

									So(err, ShouldNotBeNil)
								})
							})

							Convey("Succeeds When pubsub controller succeeds", func() {
								pubsubController.On("SendPollCreatePubsub", mock.Anything, newPoll.PollID).Return(nil)
								eventbusController.On("PublishPollCreate", mock.Anything, newPoll, newChoices).Return()
								slackController.On("PostPollStart", mock.Anything, newPoll.PollID).Return(nil)
								pollCompleteExecutor.On("Execute", mock.Anything, poll_complete_v1.PollCompleteRequest{
									PollID:              newPoll.PollID,
									PollDurationSeconds: int(newPoll.Duration.Seconds()),
									PollStartTime:       newPoll.StartTime,
								}).Return(nil)

								_, _, err := controller.CreatePoll(context.Background(), userID, ownedBy, title, duration, createChoiceArgs, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled, bitsVotesCost, isChannelPointsVotesEnabled, channelPointsVotesCost)
								So(err, ShouldBeNil)
								So(pubsubController.Calls, ShouldHaveLength, 1)
								So(eventbusController.Calls, ShouldHaveLength, 1)
							})
						})
					})
				})
			})
		})
	})
}
