package archive_poll

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/gogogadget/pointers"
	viewable_poll_cache "code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/slack"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_archive_v1"
	"code.justin.tv/commerce/poliwag/clients/datascience"
)

type Controller interface {
	ArchivePoll(ctx context.Context, userID string, pollID string) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error)
}

type controller struct {
	pollsDAO            polls_dao.PollsDAO
	getPollController   get_poll.Controller
	datascienceTracker  datascience.Tracker
	viewablePollCache   viewable_poll_cache.ViewablePollCache
	pollArchiveExecutor poll_archive_v1.Executor
	slackController     slack.Controller
}

func NewController(
	pollsDAO polls_dao.PollsDAO,
	getPollController get_poll.Controller,
	datascienceTracker datascience.Tracker,
	viewablePollCache viewable_poll_cache.ViewablePollCache,
	pollArchiveExecutor poll_archive_v1.Executor,
	slackController slack.Controller,
) Controller {
	return &controller{
		pollsDAO:            pollsDAO,
		getPollController:   getPollController,
		datascienceTracker:  datascienceTracker,
		viewablePollCache:   viewablePollCache,
		pollArchiveExecutor: pollArchiveExecutor,
		slackController:     slackController,
	}
}

func (c controller) ArchivePoll(ctx context.Context, userID string, pollID string) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error) {
	archiveTime := pointers.TimeP(time.Now())
	newStatus := models.PollStatusArchived

	poll, choices, topBits, topChannelPoints, err := c.getPollController.GetPoll(ctx, pollID, true)
	if err != nil {
		return models.Poll{}, nil, nil, nil, err
	}

	// Poll is not viewable
	isPollViewable := false
	for _, activeStatus := range models.PubliclyViewablePollStatuses {
		if poll.Status == activeStatus {
			isPollViewable = true
			break
		}
	}
	if !isPollViewable {
		return models.Poll{}, nil, nil, nil, errors.IllegalState.New("poll %s must be viewable to be archived", pollID)
	}

	// Update the dynamo record
	updatedPoll, err := c.pollsDAO.UpdatePoll(ctx, pollID, polls_dao.PollUpdateArgs{
		Status:  &newStatus,
		EndedBy: pointers.StringP(userID),
		EndTime: archiveTime,
	})
	if err != nil {
		msg := fmt.Sprintf("error updating status of pollID %s for userID %s to ARCHIVED", pollID, userID)
		logrus.WithFields(logrus.Fields{
			"userID": userID,
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	// Bust the cache
	err = c.viewablePollCache.Del(ctx, poll.OwnedBy)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID":  poll.PollID,
			"ownedBy": poll.OwnedBy,
		}).WithError(err).Warnf("error deleting viewable poll from cache for owned by %s", poll.OwnedBy)
	}

	err = c.pollArchiveExecutor.Execute(ctx, poll_archive_v1.PollArchiveRequest{
		PollID: pollID,
	})
	if err != nil {
		msg := fmt.Sprintf("error starting archive step function for pollID %s", pollID)
		logrus.WithField("pollID", pollID).WithError(err).Error(msg)
		return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	go func(pID string) {
		postToSlackCtx, cancel := context.WithTimeout(context.Background(), time.Duration(4)*time.Second)
		defer cancel()

		err = c.slackController.PostPollEnd(postToSlackCtx, pID)
		if err != nil {
			logrus.WithField("pollID", pID).WithError(err).Info("failed to post poll end to slack when archiving poll")
		}
	}(pollID)

	// Archive pubsub message is sent in the archive step function
	c.datascienceTracker.TrackEventAsync(datascience.DeletePoll, models.EndPollProperties{
		UserID:    userID,
		ChannelID: updatedPoll.OwnedBy,
		PollID:    pollID,
	})

	return updatedPoll, choices, topBits, topChannelPoints, err
}
