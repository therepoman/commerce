package archive_poll_test

import (
	"context"
	"errors"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/archive_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	viewable_poll_cache_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	slack_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/slack"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	poll_archive_v1_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_archive_v1"
	datascience_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/clients/datasceince"
)

func TestController_ArchivePoll(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		datascienceTracker := new(datascience_mock.Tracker)
		viewablePollCache := new(viewable_poll_cache_mock.ViewablePollCache)
		pollArchiveExecutor := new(poll_archive_v1_mock.Executor)
		slackController := new(slack_mock.Controller)

		controller := archive_poll.NewController(
			pollsDAO,
			getPollController,
			datascienceTracker,
			viewablePollCache,
			pollArchiveExecutor,
			slackController,
		)

		userID := "test-user-id"
		pollID := "test-poll-id"

		slackController.On("PostPollEnd", mock.Anything, pollID).Return(nil)
		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when the GetPollController returns an error", func() {
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(models.Poll{}, []models.Choice{}, nil, nil, errors.New("test-error"))

			Convey("we should return an error", func() {
				_, _, _, _, err := controller.ArchivePoll(context.Background(), userID, pollID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the GetPollController returns a not viewable poll", func() {
			poll := models.Poll{
				PollID:  pollID,
				OwnedBy: userID,
				Status:  models.PollStatusArchived,
			}
			choices := []models.Choice{
				{
					ChoiceID: "choice-id-0",
					Title:    "Final Fantasy VII",
				},
				{
					ChoiceID: "choice-id-1",
					Title:    "Chrono Trigger",
				},
			}
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(poll, choices, nil, nil, nil)

			Convey("we should return an error", func() {
				_, _, _, _, err := controller.ArchivePoll(context.Background(), userID, pollID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the GetPollController returns an active poll", func() {
			poll := models.Poll{
				PollID:  pollID,
				OwnedBy: userID,
				Status:  models.PollStatusActive,
			}
			choices := []models.Choice{
				{
					ChoiceID: "choice-id-0",
					Title:    "Final Fantasy VII",
				},
				{
					ChoiceID: "choice-id-1",
					Title:    "Chrono Trigger",
				},
			}
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(poll, choices, nil, nil, nil)

			Convey("when the DAO errors during update", func() {
				pollsDAO.On("UpdatePoll", mock.Anything, pollID, mock.Anything).Return(models.Poll{}, errors.New("test-error"))

				Convey("we should return an error", func() {
					_, _, _, _, err := controller.ArchivePoll(context.Background(), userID, pollID)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the DAO succeeds during update", func() {
				endTime := time.Now()
				pollsDAO.On("UpdatePoll", mock.Anything, pollID, mock.Anything).Return(models.Poll{
					Status:  models.PollStatusArchived,
					EndedBy: userID,
					EndTime: pointers.TimeP(endTime),
				}, nil)

				Convey("when ViewablePollCache.Del is called", func() {
					viewablePollCache.On("Del", mock.Anything, poll.OwnedBy).Return(nil)

					Convey("when the sfn executor fails", func() {
						pollArchiveExecutor.On("Execute", mock.Anything, mock.Anything).Return(errors.New("test-error"))

						Convey("we should return an error", func() {
							_, _, _, _, err := controller.ArchivePoll(context.Background(), userID, pollID)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("when sfn executor succeeds", func() {
						pollArchiveExecutor.On("Execute", mock.Anything, mock.Anything).Return(nil)
						pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, pollID).Return(nil, nil, nil)

						Convey("we should succeed and return the updated poll", func() {
							poll, choices, _, _, err := controller.ArchivePoll(context.Background(), userID, pollID)
							So(err, ShouldBeNil)
							So(choices, ShouldHaveLength, len(choices))
							So(poll.Status, ShouldEqual, models.PollStatusArchived)
							So(poll.EndedBy, ShouldEqual, userID)
							So(poll.EndTime.UnixNano(), ShouldEqual, endTime.UnixNano())
						})
					})
				})
			})
		})
	})
}
