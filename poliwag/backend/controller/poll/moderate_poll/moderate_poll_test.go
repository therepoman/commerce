package moderate_poll_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/controller/poll/moderate_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	viewable_poll_cache_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	poll_moderate_v1_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_moderate_v1"
)

func TestController_ModeratePoll(t *testing.T) {
	Convey("given a ModeratePoll controller", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		getPollController := new(get_poll_mock.Controller)
		viewablePollCache := new(viewable_poll_cache_mock.ViewablePollCache)
		pollModerateExecutor := new(poll_moderate_v1_mock.Executor)

		controller := moderate_poll.NewController(pollsDAO, getPollController, viewablePollCache, pollModerateExecutor)

		pollID := "test-poll-id"

		Convey("when GetPoll errors", func() {
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(models.Poll{}, nil, nil, nil, errors.New("GetPoll ERROR"))

			Convey("we should return an error", func() {
				_, _, err := controller.ModeratePoll(context.Background(), pollID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetPoll succeeds", func() {
			poll := models.Poll{
				PollID: pollID,
				Status: models.PollStatusCompleted,
			}

			choices := []models.Choice{
				{
					PollID:   pollID,
					ChoiceID: "test-choice-id-0",
				},
				{
					PollID:   pollID,
					ChoiceID: "test-choice-id-1",
				},
			}

			getPollController.On("GetPoll", context.Background(), pollID, true).Return(poll, choices, nil, nil, nil)

			Convey("when UpdatePoll returns an error", func() {
				pollsDAO.On("UpdatePoll", mock.Anything, pollID, mock.Anything).Return(models.Poll{}, errors.New("UpdatePoll ERROR"))

				Convey("we should return an error", func() {
					_, _, err := controller.ModeratePoll(context.Background(), pollID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when UpdatePoll succeeds", func() {
				updatedPoll := models.Poll{
					PollID: pollID,
					Status: models.PollStatusModerated,
				}
				pollsDAO.On("UpdatePoll", mock.Anything, pollID, mock.Anything).Return(updatedPoll, nil)

				Convey("when ViewablePollCache.Del is called", func() {
					viewablePollCache.On("Del", mock.Anything, poll.OwnedBy).Return(nil)

					Convey("when sfn executor fails", func() {
						pollModerateExecutor.On("Execute", mock.Anything, mock.Anything).Return(errors.New("test-error"))

						Convey("we should return an error", func() {
							_, _, err := controller.ModeratePoll(context.Background(), pollID)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("when sfn executor succeeds", func() {
						pollModerateExecutor.On("Execute", mock.Anything, mock.Anything).Return(nil)

						Convey("we should return an the moderated poll", func() {
							actualPoll, actualChoices, err := controller.ModeratePoll(context.Background(), pollID)

							So(err, ShouldBeNil)
							So(actualPoll, ShouldResemble, updatedPoll)
							So(actualChoices, ShouldResemble, choices)
						})
					})
				})
			})
		})
	})
}
