package moderate_poll

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	viewable_poll_cache "code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_moderate_v1"
)

type Controller interface {
	ModeratePoll(ctx context.Context, pollID string) (models.Poll, []models.Choice, error)
}

type controller struct {
	pollsDAO             polls_dao.PollsDAO
	getPollController    get_poll.Controller
	viewablePollCache    viewable_poll_cache.ViewablePollCache
	pollModerateExecutor poll_moderate_v1.Executor
}

func NewController(
	pollsDAO polls_dao.PollsDAO,
	getPollController get_poll.Controller,
	viewablePollCache viewable_poll_cache.ViewablePollCache,
	pollModerateExecutor poll_moderate_v1.Executor,
) Controller {
	return &controller{
		pollsDAO:             pollsDAO,
		getPollController:    getPollController,
		viewablePollCache:    viewablePollCache,
		pollModerateExecutor: pollModerateExecutor,
	}
}

func (c controller) ModeratePoll(ctx context.Context, pollID string) (models.Poll, []models.Choice, error) {
	moderatedTime := pointers.TimeP(time.Now())
	newStatus := models.PollStatusModerated

	_, choices, _, _, err := c.getPollController.GetPoll(ctx, pollID, true)
	if err != nil {
		return models.Poll{}, nil, err
	}

	updatedPoll, err := c.pollsDAO.UpdatePoll(ctx, pollID, polls_dao.PollUpdateArgs{
		ModeratedTime: moderatedTime,
		Status:        &newStatus,
	})
	if err != nil {
		msg := fmt.Sprintf("error updating status of pollID %s to MODERATED", pollID)
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	err = c.viewablePollCache.Del(ctx, updatedPoll.OwnedBy)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID":  updatedPoll.PollID,
			"ownedBy": updatedPoll.OwnedBy,
		}).WithError(err).Warnf("error deleting viewable poll from cache for owned by %s", updatedPoll.OwnedBy)
	}

	err = c.pollModerateExecutor.Execute(ctx, poll_moderate_v1.PollModerateRequest{
		PollID: pollID,
	})
	if err != nil {
		msg := fmt.Sprintf("error starting poll-moderate stepfunction for pollID %s", pollID)
		logrus.WithField("pollID", pollID).WithError(err).Error(msg)
		return models.Poll{}, nil, errors.InternalError.Wrap(err, msg)
	}

	// Moderate pubsub message is sent in the moderate step function

	return updatedPoll, choices, err
}
