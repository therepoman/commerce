package get_polls

import (
	"context"
	"fmt"
	"sync"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Controller interface {
	GetPolls(ctx context.Context, ownedBy string, status []models.PollStatus, sort models.PollSort, direction models.Direction, limit int, key polls_dao.PollsCursor) ([]models.Poll, map[string][]models.Choice, map[string]*models.TopBitsContributor, map[string]*models.TopChannelPointsContributor, polls_dao.PollsCursor, error)
}

type controller struct {
	choicesDAO     choices_dao.ChoicesDAO
	pollsDAO       polls_dao.PollsDAO
	stats          statsd.Statter
	sampleReporter *telemetry.SampleReporter
}

type PollsCursor = polls_dao.PollsCursor

func NewController(
	choicesDAO choices_dao.ChoicesDAO,
	pollsDAO polls_dao.PollsDAO,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
) Controller {
	return &controller{
		choicesDAO:     choicesDAO,
		pollsDAO:       pollsDAO,
		stats:          stats,
		sampleReporter: sampleReporter,
	}
}

func (c controller) GetPolls(ctx context.Context, ownedBy string, status []models.PollStatus, sort models.PollSort, direction models.Direction, limit int, cursor PollsCursor) ([]models.Poll, map[string][]models.Choice, map[string]*models.TopBitsContributor, map[string]*models.TopChannelPointsContributor, PollsCursor, error) {
	polls, newCursor, err := c.pollsDAO.GetPollsByOwnedBy(ctx, ownedBy, status, sort, direction, limit, cursor)
	if err != nil {
		msg := fmt.Sprintf("error getting polls with owner id %s", ownedBy)
		logrus.WithFields(logrus.Fields{
			"ownedBy": ownedBy,
			"status":  status,
		}).WithError(err).Error(msg)
		return nil, nil, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	choicesMap := map[string][]models.Choice{}
	topBitsMap := map[string]*models.TopBitsContributor{}
	topChannelPointsMap := map[string]*models.TopChannelPointsContributor{}

	mu := sync.Mutex{}
	g := errgroup.Group{}
	for _, poll := range polls {
		pollID := poll.PollID
		g.Go(func() error {
			choices, err := c.choicesDAO.GetChoicesByPollID(ctx, pollID)
			if err != nil {
				msg := fmt.Sprintf("error getting choices for poll %s", pollID)
				logrus.WithField("pollID", pollID).WithError(err).Error(msg)
				return errors.InternalError.Wrap(err, msg)
			}

			if len(choices) == 0 {
				logrus.WithField("pollID", pollID).Warn("retrieved 0 choices for poll in GetPolls")
				statErr := c.stats.Inc(models.ZERO_CHOICE_METRIC_NAME, 1, 1.0)
				if statErr != nil {
					logrus.WithField("pollID", pollID).Error("error tracking zero choice metric in GetPolls")
				}

				c.sampleReporter.Report(models.ZERO_CHOICE_METRIC_NAME, 1.0, telemetry.UnitCount)
			}

			topBits, topChannelPoints, err := c.pollsDAO.GetTopContributorsFromPantheon(ctx, pollID)
			if err != nil {
				msg := fmt.Sprintf("error getting top contributors for pollID %s", pollID)
				logrus.WithField("pollID", pollID).WithError(err).Error(msg)
				return errors.InternalError.Wrap(err, msg)
			}

			mu.Lock()
			defer mu.Unlock()
			choicesMap[pollID] = choices
			topBitsMap[pollID] = topBits
			topChannelPointsMap[pollID] = topChannelPoints
			return nil
		})
	}
	err = g.Wait()
	if err != nil {
		return nil, nil, nil, nil, nil, err
	}

	return polls, choicesMap, topBitsMap, topChannelPointsMap, newCursor, nil
}
