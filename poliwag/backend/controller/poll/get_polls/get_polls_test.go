package get_polls_test

import (
	"context"
	"errors"
	"testing"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestPollControllerImpl_GetPolls(t *testing.T) {
	Convey("given a poll controller", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		choicesDAO := new(choices_dao_mock.ChoicesDAO)
		statter, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)

		controller := get_polls.NewController(choicesDAO, pollsDAO, statter, telemetry.NewNoOpSampleReporter())

		userID := "test-user-id"
		ownedBy := "test-owned-by"
		var statuses []models.PollStatus
		sort := models.PollSortStartTime
		direction := models.DirectionDescending
		limit := 20
		var cursor get_polls.PollsCursor

		Convey("when GetPollsByOwnedBy returns an error", func() {
			pollsDAO.On("GetPollsByOwnedBy", mock.Anything, userID, statuses, sort, direction, limit, cursor).Return(nil, nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, _, _, _, _, err := controller.GetPolls(context.Background(), userID, statuses, sort, direction, 20, cursor)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetPollsByOwnedBy return no polls", func() {
			pollsDAO.On("GetPollsByOwnedBy", mock.Anything, userID, statuses, sort, direction, limit, cursor).Return([]models.Poll{}, nil, nil)

			Convey("we not return an error", func() {
				_, _, _, _, _, err := controller.GetPolls(context.Background(), userID, statuses, sort, direction, 20, cursor)

				So(err, ShouldBeNil)
			})
		})

		Convey("when GetPollsByOwnedBy return polls", func() {
			poll1 := models.Poll{
				PollID:  "1",
				OwnedBy: ownedBy,
			}

			poll2 := models.Poll{
				PollID:  "2",
				OwnedBy: ownedBy,
			}

			choices := []models.Choice{
				{
					Title:    "Final Fantasy VII",
					PollID:   "pollID",
					ChoiceID: "choice-id-0",
				},
				{
					Title:    "Chrono Trigger",
					PollID:   "pollID",
					ChoiceID: "choice-id-1",
				},
			}

			choicesMap := map[string][]models.Choice{
				"1": choices,
				"2": choices,
			}

			topBits1 := &models.TopBitsContributor{
				UserID:          "1234",
				BitsContributed: 10,
			}

			topChannelPoints1 := &models.TopChannelPointsContributor{
				UserID:                   "5678",
				ChannelPointsContributed: 100,
			}

			topBits2 := &models.TopBitsContributor{
				UserID:          "9876",
				BitsContributed: 20,
			}

			topChannelPoints2 := &models.TopChannelPointsContributor{
				UserID:                   "5432",
				ChannelPointsContributed: 200,
			}

			topBitsMap := map[string]*models.TopBitsContributor{
				"1": topBits1,
				"2": topBits2,
			}

			topChannelPointsMap := map[string]*models.TopChannelPointsContributor{
				"1": topChannelPoints1,
				"2": topChannelPoints2,
			}

			polls := []models.Poll{
				poll1,
				poll2,
			}

			pollsDAO.On("GetPollsByOwnedBy", mock.Anything, userID, statuses, sort, direction, limit, cursor).Return(polls, nil, nil)
			choicesDAO.On("GetChoicesByPollID", mock.Anything, mock.Anything).Return(choices, nil)
			pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, poll1.PollID).Return(topBits1, topChannelPoints1, nil)
			pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, poll2.PollID).Return(topBits2, topChannelPoints2, nil)

			Convey("we should return those polls", func() {
				actualPolls, actualChoices, actualBitsMap, actualChannelPointsMap, key, err := controller.GetPolls(context.Background(), userID, statuses, sort, direction, 20, cursor)

				So(err, ShouldBeNil)
				So(key, ShouldBeNil)
				So(actualPolls, ShouldResemble, polls)
				So(actualChoices, ShouldResemble, choicesMap)
				So(actualBitsMap, ShouldResemble, topBitsMap)
				So(actualChannelPointsMap, ShouldResemble, topChannelPointsMap)
			})
		})
	})
}
