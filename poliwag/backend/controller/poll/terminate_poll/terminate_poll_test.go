package terminate_poll_test

import (
	"context"
	"errors"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/terminate_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	slack_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/slack"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	poll_terminate_v1_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_terminate_v1"
	datascience_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/clients/datasceince"
)

func TestController_TerminatePoll(t *testing.T) {
	Convey("given a poll controller", t, func() {
		getPollController := new(get_poll_mock.Controller)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		datascienceTracker := new(datascience_mock.Tracker)
		pollTerminateExecutor := new(poll_terminate_v1_mock.Executor)
		slackController := new(slack_mock.Controller)

		controller := terminate_poll.NewController(pollsDAO, getPollController, datascienceTracker, pollTerminateExecutor, slackController)

		userID := "test-user-id"
		pollID := "test-poll-id"

		slackController.On("PostPollEnd", mock.Anything, pollID).Return(nil)
		datascienceTracker.On("TrackEventAsync", mock.Anything, mock.Anything).Return()

		Convey("when the GetPollController returns an error", func() {
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(models.Poll{}, []models.Choice{}, nil, nil, errors.New("test-error"))

			Convey("we should return an error", func() {
				_, _, _, _, err := controller.TerminatePoll(context.Background(), userID, pollID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the GetPollController returns an inactive poll", func() {
			poll := models.Poll{
				PollID:  pollID,
				OwnedBy: userID,
				Status:  models.PollStatusCompleted,
			}
			choices := []models.Choice{
				{
					ChoiceID: "choice-id-0",
					Title:    "Final Fantasy VII",
				},
				{
					ChoiceID: "choice-id-1",
					Title:    "Chrono Trigger",
				},
			}
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(poll, choices, nil, nil, nil)

			Convey("we should return an error", func() {
				pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, pollID).Return(nil, nil, nil)

				_, _, _, _, err := controller.TerminatePoll(context.Background(), userID, pollID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the GetPollController returns an active poll", func() {
			poll := models.Poll{
				PollID:  pollID,
				OwnedBy: userID,
				Status:  models.PollStatusActive,
			}
			choices := []models.Choice{
				{
					ChoiceID: "choice-id-0",
					Title:    "Final Fantasy VII",
				},
				{
					ChoiceID: "choice-id-1",
					Title:    "Chrono Trigger",
				},
			}
			getPollController.On("GetPoll", mock.Anything, pollID, true).Return(poll, choices, nil, nil, nil)

			Convey("when the DAO errors during update", func() {
				pollsDAO.On("UpdatePoll", mock.Anything, pollID, mock.Anything).Return(models.Poll{}, errors.New("test-error"))

				Convey("we should return an error", func() {
					_, _, _, _, err := controller.TerminatePoll(context.Background(), userID, pollID)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the DAO succeeds during update", func() {
				endTime := time.Now()
				pollsDAO.On("UpdatePoll", mock.Anything, pollID, mock.Anything).Return(models.Poll{
					Status:  models.PollStatusTerminated,
					EndedBy: userID,
					EndTime: pointers.TimeP(endTime),
				}, nil)

				Convey("when the poll terminate step function executor errors", func() {
					pollTerminateExecutor.On("Execute", mock.Anything, mock.Anything).Return(errors.New("test-error"))

					Convey("we should return an error", func() {
						_, _, _, _, err := controller.TerminatePoll(context.Background(), userID, pollID)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the poll terminate step function executor succeeds", func() {
					pollTerminateExecutor.On("Execute", mock.Anything, mock.Anything).Return(nil)
					pollsDAO.On("GetTopContributorsFromPantheon", mock.Anything, pollID).Return(nil, nil, nil)

					Convey("we should succeed and return the updated poll", func() {
						poll, choices, _, _, err := controller.TerminatePoll(context.Background(), userID, pollID)
						So(err, ShouldBeNil)
						So(choices, ShouldHaveLength, len(choices))
						So(poll.Status, ShouldEqual, models.PollStatusTerminated)
						So(poll.EndedBy, ShouldEqual, userID)
						So(poll.EndTime.UnixNano(), ShouldEqual, endTime.UnixNano())
					})
				})
			})
		})
	})
}
