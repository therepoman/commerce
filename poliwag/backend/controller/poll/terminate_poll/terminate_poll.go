package terminate_poll

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/slack"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_terminate_v1"
	"code.justin.tv/commerce/poliwag/clients/datascience"
)

type Controller interface {
	TerminatePoll(ctx context.Context, userID string, pollID string) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error)
}

type controller struct {
	pollsDAO              polls_dao.PollsDAO
	getPollController     get_poll.Controller
	datascienceTracker    datascience.Tracker
	pollTerminateExecutor poll_terminate_v1.Executor
	slackController       slack.Controller
}

func NewController(
	pollsDAO polls_dao.PollsDAO,
	getPollController get_poll.Controller,
	datascienceTracker datascience.Tracker,
	pollTerminateExecutor poll_terminate_v1.Executor,
	slackController slack.Controller,
) Controller {
	return &controller{
		pollsDAO:              pollsDAO,
		getPollController:     getPollController,
		datascienceTracker:    datascienceTracker,
		pollTerminateExecutor: pollTerminateExecutor,
		slackController:       slackController,
	}
}

func (c controller) TerminatePoll(ctx context.Context, userID string, pollID string) (models.Poll, []models.Choice, *models.TopBitsContributor, *models.TopChannelPointsContributor, error) {
	terminateTime := pointers.TimeP(time.Now())
	newStatus := models.PollStatusTerminated

	poll, choices, topBits, topChannelPoints, err := c.getPollController.GetPoll(ctx, pollID, true)
	if err != nil {
		return models.Poll{}, nil, nil, nil, err
	}

	// Poll is not active
	if poll.Status != models.PollStatusActive {
		return models.Poll{}, nil, nil, nil, errors.IllegalState.New("poll %s must be active to be terminated", pollID)
	}

	// Update the dynamo record
	updatedPoll, err := c.pollsDAO.UpdatePoll(ctx, pollID, polls_dao.PollUpdateArgs{
		Status:  &newStatus,
		EndedBy: pointers.StringP(userID),
		EndTime: terminateTime,
	})
	if err != nil {
		msg := fmt.Sprintf("error updating status of pollID %s for userID %s to TERMINATED", pollID, userID)
		logrus.WithFields(logrus.Fields{
			"userID": userID,
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	err = c.pollTerminateExecutor.Execute(ctx, poll_terminate_v1.PollTerminateRequest{
		PollID: pollID,
	})
	if err != nil {
		msg := fmt.Sprintf("error starting poll-terminate stepfunction for pollID %s", pollID)
		logrus.WithField("pollID", pollID).WithError(err).Error(msg)
		return models.Poll{}, nil, nil, nil, errors.InternalError.Wrap(err, msg)
	}

	go func(pID string) {
		postToSlackCtx, cancel := context.WithTimeout(context.Background(), time.Duration(4)*time.Second)
		defer cancel()

		err = c.slackController.PostPollEnd(postToSlackCtx, pID)
		if err != nil {
			logrus.WithField("pollID", pID).WithError(err).Info("failed to post poll end to slack when terminating poll")
		}
	}(pollID)

	// Terminate pubsub message is sent in the terminate step function
	c.datascienceTracker.TrackEventAsync(datascience.EndPoll, models.EndPollProperties{
		UserID:    userID,
		ChannelID: updatedPoll.OwnedBy,
		PollID:    pollID,
	})

	return updatedPoll, choices, topBits, topChannelPoints, err
}
