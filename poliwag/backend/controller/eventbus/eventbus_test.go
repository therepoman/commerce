package eventbus_test

import (
	"context"
	"errors"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/models"
	util "code.justin.tv/commerce/poliwag/backend/utils/eventbus"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
	eventbus_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/eventbus/client"
	eventbus_client "code.justin.tv/eventbus/client"
)

type fakeEventbusPublisherWithEventTypeCheck struct {
	client eventbus_client.Publisher
	t      *testing.T
}

func (f *fakeEventbusPublisherWithEventTypeCheck) Publish(ctx context.Context, message eventbus_client.Message) error {
	Convey("event type should be in EventTypesToPublish", f.t, func() {
		So(eventbus.EventTypesToPublish, ShouldContain, message.EventBusName())
	})
	return f.client.Publish(ctx, message)
}

func TestController_PublishPollCreate(t *testing.T) {
	Convey("given an eventbus controller", t, func() {
		eventbusClient := new(eventbus_mock.Publisher)
		eventbusPublisher := &fakeEventbusPublisherWithEventTypeCheck{client: eventbusClient, t: t}
		choicesDAO := new(choices_dao_mock.ChoicesDAO)

		controller := eventbus.NewController(false, eventbusPublisher, choicesDAO)

		poll := models.Poll{
			PollID:                      "test-poll-id",
			OwnedBy:                     "test-user-id",
			Title:                       "test title",
			StartTime:                   time.Now(),
			Duration:                    30 * time.Second,
			Status:                      models.PollStatusActive,
			IsBitsVotesEnabled:          true,
			BitsVotesCost:               10,
			IsChannelPointsVotesEnabled: true,
			ChannelPointsVotesCost:      500,
		}

		choices := []models.Choice{
			{
				ChoiceID: "test-choice-id-1",
				PollID:   "test-poll-id",
				Title:    "test choice title 1",
				Index:    0,
			},
			{
				ChoiceID: "test-choice-id-2",
				PollID:   "test-poll-id",
				Title:    "test choice title 2",
				Index:    1,
			},
		}

		Convey("publishes create event", func() {
			eventbusClient.On("Publish", mock.Anything, util.PollToEventbusPollCreate(poll, choices)).Return(nil)
			controller.PublishPollCreate(context.Background(), poll, choices)
			So(eventbusClient.Calls, ShouldHaveLength, 1)
		})
	})
}

func TestController_PublishPollUpdate(t *testing.T) {
	Convey("given an eventbus controller", t, func() {
		eventbusClient := new(eventbus_mock.Publisher)
		eventbusPublisher := &fakeEventbusPublisherWithEventTypeCheck{client: eventbusClient, t: t}
		choicesDAO := new(choices_dao_mock.ChoicesDAO)

		controller := eventbus.NewController(false, eventbusPublisher, choicesDAO)

		poll := models.Poll{
			PollID:                      "test-poll-id",
			OwnedBy:                     "test-user-id",
			Title:                       "test title",
			StartTime:                   time.Now().Add(-30 * time.Second),
			Duration:                    30 * time.Second,
			Status:                      models.PollStatusActive,
			IsBitsVotesEnabled:          true,
			BitsVotesCost:               10,
			IsChannelPointsVotesEnabled: true,
			ChannelPointsVotesCost:      500,
			TotalVoters:                 100,
			TotalVotes:                  150,
			TotalBaseVotes:              100,
			TotalBits:                   20 * 10,  // 20 bits votes
			TotalChannelPoints:          30 * 500, // 30 channel points votes
		}

		choices := []models.Choice{
			{
				ChoiceID:           "test-choice-id-1",
				PollID:             "test-poll-id",
				Title:              "test choice title 1",
				Index:              0,
				TotalVoters:        40,
				TotalVotes:         55,
				TotalBaseVotes:     40,
				TotalBits:          5 * 10,   // 5 bits votes
				TotalChannelPoints: 10 * 500, // 10 channel points votes
			},
			{
				ChoiceID:           "test-choice-id-2",
				PollID:             "test-poll-id",
				Title:              "test choice title 2",
				Index:              1,
				TotalVoters:        60,
				TotalVotes:         95,
				TotalBaseVotes:     60,
				TotalBits:          15 * 10,  // 15 bits votes
				TotalChannelPoints: 20 * 500, // 20 channel points votes
			},
		}

		Convey("when GetChoicesByPollID errors", func() {
			choicesDAO.On("GetChoicesByPollID", mock.Anything, poll.PollID).Return([]models.Choice{}, errors.New("error"))

			Convey("doesn't publish an event", func() {
				controller.PublishPollUpdate(context.Background(), poll, false)
				So(eventbusClient.Calls, ShouldHaveLength, 0)
			})
		})

		Convey("when GetChoicesByPollID succeeds", func() {
			Convey("when status hasn't changed", func() {
				Convey("publishes update event", func() {
					choicesDAO.On("GetChoicesByPollID", mock.Anything, poll.PollID).Return(choices, nil)

					eventbusClient.On("Publish", mock.Anything, util.PollToEventbusPollUpdate(poll, choices, false)).Return(nil)
					controller.PublishPollUpdate(context.Background(), poll, false)
					So(eventbusClient.Calls, ShouldHaveLength, 1)
				})
			})

			Convey("when status changed", func() {
				Convey("publishes update event", func() {
					endTime := time.Now()
					poll.EndTime = &endTime
					poll.Status = models.PollStatusCompleted
					choicesDAO.On("GetChoicesByPollID", mock.Anything, poll.PollID).Return(choices, nil)

					eventbusClient.On("Publish", mock.Anything, util.PollToEventbusPollUpdate(poll, choices, true)).Return(nil)
					controller.PublishPollUpdate(context.Background(), poll, true)
					So(eventbusClient.Calls, ShouldHaveLength, 1)
				})
			})
		})
	})
}
