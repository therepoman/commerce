package eventbus

import (
	"context"
	"fmt"

	"github.com/avast/retry-go"
	"github.com/sirupsen/logrus"

	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	"code.justin.tv/commerce/poliwag/backend/models"
	util "code.justin.tv/commerce/poliwag/backend/utils/eventbus"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	eventbus_client "code.justin.tv/eventbus/client"
	eventbus_poll "code.justin.tv/eventbus/schema/pkg/poll"
)

var EventTypesToPublish = []string{
	eventbus_poll.CreateEventType,
	eventbus_poll.UpdateEventType,
}

type Controller interface {
	PublishPollCreate(ctx context.Context, poll models.Poll, choices []models.Choice)
	PublishPollUpdate(ctx context.Context, poll models.Poll, statusUpdated bool)
}

type controller struct {
	eventbusPublisher eventbus_client.Publisher
	choicesDAO        choices_dao.ChoicesDAO
	isLambda          bool
}

func NewController(isLambda bool, eventbusPublisher eventbus_client.Publisher, choicesDAO choices_dao.ChoicesDAO) Controller {
	return &controller{
		eventbusPublisher: eventbusPublisher,
		choicesDAO:        choicesDAO,
		isLambda:          isLambda,
	}
}

func (c controller) publish(ctx context.Context, msg eventbus_client.Message) error {
	// Lambdas shouldn't leverage hystrix
	var err error
	if c.isLambda {
		err = retry.Do(func() error {
			return c.eventbusPublisher.Publish(ctx, msg)
		}, retry.Context(ctx), retry.Attempts(3))
	} else {
		err = hystrix.HystrixDo(hystrix.EventbusPublish, func() error {
			return c.eventbusPublisher.Publish(ctx, msg)
		}, nil)
	}
	return err
}

func (c controller) PublishPollCreate(ctx context.Context, poll models.Poll, choices []models.Choice) {
	err := c.publish(ctx, util.PollToEventbusPollCreate(poll, choices))
	if err != nil {
		logrus.WithField("pollID", poll.PollID).WithError(err).Error("failed publishing PollCreate to Eventbus")
	}
}

// Publish an Event update event
func (c controller) PublishPollUpdate(ctx context.Context, poll models.Poll, statusUpdated bool) {
	// Get choices associated with the poll from dynamo
	choices, err := c.choicesDAO.GetChoicesByPollID(ctx, poll.PollID)
	if err != nil {
		logrus.WithField("pollID", poll.PollID).WithError(err).Error(fmt.Sprintf("error getting choices for poll id %s for publishing PollUpdate to Eventbus", poll.PollID))
		return
	}

	err = c.publish(ctx, util.PollToEventbusPollUpdate(poll, choices, statusUpdated))
	if err != nil {
		logrus.WithField("pollID", poll.PollID).WithError(err).Error("failed publishing PollUpdate to Eventbus")
	}
}
