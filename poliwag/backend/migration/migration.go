package migration

import (
	"context"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/clients/aurora/gorm"
)

type Migrator interface {
	Migrate(ctx context.Context) error
}

type migrator struct {
	gormClient gorm.Client
}

func NewMigrator(gormClient gorm.Client) Migrator {
	return &migrator{
		gormClient: gormClient,
	}
}

func (m *migrator) Migrate(ctx context.Context) error {
	return m.gormClient.AutoMigrate(ctx,
		&votes.Vote{},
		// TODO: Add additional tables
	)
}
