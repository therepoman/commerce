package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/cb/hallpass/client/hallpass"
	"code.justin.tv/chat/zuma/app/api"
	zuma "code.justin.tv/chat/zuma/client"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/poliwag/clients/aurora/mysql"
	"code.justin.tv/commerce/poliwag/proto/poliwag_internal"
	ripley "code.justin.tv/revenue/ripley/rpc"
)

type internalServer struct {
	payday   paydayrpc.Payday
	hallpass hallpass.Client
	ripley   ripley.Ripley
	zuma     zuma.Client
	aurora   mysql.Client
}

const (
	qaTestBitsUser1     = "434515886"
	qaTestBitsUser2     = "434515890"
	healthCheckResponse = "https://youtu.be/F0yB93XlsX8"
)

func NewInternalServer(
	payday paydayrpc.Payday,
	hallpass hallpass.Client,
	ripley ripley.Ripley,
	zuma zuma.Client,
	aurora mysql.Client,
) poliwag_internal.PoliwagInternalAPI {
	return &internalServer{
		payday:   payday,
		hallpass: hallpass,
		ripley:   ripley,
		zuma:     zuma,
		aurora:   aurora,
	}
}

func (s *internalServer) NetworkHealthCheck(ctx context.Context, req *poliwag_internal.NetworkHealthCheckRequest) (*poliwag_internal.NetworkHealthCheckResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, Timeout)
	defer cancel()

	_, err := s.payday.HealthCheck(ctx, &paydayrpc.HealthCheckReq{})
	if err != nil {
		logrus.WithError(err).Error("error calling payday for network health check")
		return nil, twirp.NewError(twirp.Internal, "error calling payday health check")
	}

	_, err = s.hallpass.GetV1IsEditor(ctx, qaTestBitsUser1, qaTestBitsUser2, nil)
	if err != nil {
		logrus.WithError(err).Error("error calling hallpass for network health check")
		return nil, twirp.NewError(twirp.Internal, "error calling hallpass GetV1IsEditor")
	}

	_, err = s.zuma.GetUserChatProperties(ctx, api.UserChatPropertiesRequest{
		UserID: qaTestBitsUser1,
	}, nil)
	if err != nil {
		logrus.WithError(err).Error("error calling zuma for network health check")
		return nil, twirp.NewError(twirp.Internal, "error calling zuma GetUserChatProperties")
	}

	_, err = s.ripley.GetPayoutType(ctx, &ripley.GetPayoutTypeRequest{
		ChannelId: qaTestBitsUser1,
	})
	if err != nil {
		logrus.WithError(err).Error("error calling ripley for network health check")
		return nil, twirp.NewError(twirp.Internal, "error calling ripley GetPayoutType")
	}

	err = s.aurora.Ping(ctx)
	if err != nil {
		logrus.WithError(err).Error("error calling aurora for network health check")
		return nil, twirp.NewError(twirp.Internal, "error pinging aurora")
	}

	return &poliwag_internal.NetworkHealthCheckResponse{
		Response: healthCheckResponse,
	}, nil
}

func (s *internalServer) HealthCheck(ctx context.Context, req *poliwag_internal.HealthCheckRequest) (*poliwag_internal.HealthCheckResponse, error) {
	return &poliwag_internal.HealthCheckResponse{
		Response: healthCheckResponse,
	}, nil
}
