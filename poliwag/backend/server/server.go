package server

import (
	"context"
	"time"

	"code.justin.tv/commerce/poliwag/backend/api/archive_poll"
	"code.justin.tv/commerce/poliwag/backend/api/create_poll"
	"code.justin.tv/commerce/poliwag/backend/api/get_poll"
	"code.justin.tv/commerce/poliwag/backend/api/get_polls"
	"code.justin.tv/commerce/poliwag/backend/api/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/api/get_voter"
	"code.justin.tv/commerce/poliwag/backend/api/get_voters"
	"code.justin.tv/commerce/poliwag/backend/api/get_voters_by_choice"
	"code.justin.tv/commerce/poliwag/backend/api/terminate_poll"
	"code.justin.tv/commerce/poliwag/backend/api/vote"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const (
	// Context timeouts are set to match GQL timeouts
	archivePollAPITimeout       = 1500 * time.Millisecond
	createPollAPITimeout        = 1500 * time.Millisecond
	getPollAPITimeout           = 200 * time.Millisecond
	getPollsAPITimeout          = 300 * time.Millisecond
	getViewablePollAPITimeout   = 200 * time.Millisecond
	terminatePollAPITimeout     = 1500 * time.Millisecond
	getVoterAPITimeout          = 500 * time.Millisecond
	getVotersAPITimeout         = 500 * time.Millisecond
	getVotersByChoiceAPITimeout = 500 * time.Millisecond
	voteAPITimeout              = 2500 * time.Millisecond
	// For admin polls call
	Timeout = 2 * time.Second
)

type server struct {
	ArchivePollAPI       archive_poll.API
	CreatePollAPI        create_poll.API
	GetPollAPI           get_poll.API
	GetPollsAPI          get_polls.API
	GetViewablePollAPI   get_viewable_poll.API
	TerminatePollAPI     terminate_poll.API
	GetVoterAPI          get_voter.API
	GetVotersAPI         get_voters.API
	GetVotersByChoiceAPI get_voters_by_choice.API
	VoteAPI              vote.API
}

func NewServer(
	ap archive_poll.API,
	cp create_poll.API,
	gp get_poll.API,
	gps get_polls.API,
	gvp get_viewable_poll.API,
	tp terminate_poll.API,
	gv get_voter.API,
	gvs get_voters.API,
	gvbc get_voters_by_choice.API,
	v vote.API,
) poliwag.PoliwagAPI {
	return &server{
		ArchivePollAPI:       ap,
		CreatePollAPI:        cp,
		GetPollAPI:           gp,
		GetPollsAPI:          gps,
		GetViewablePollAPI:   gvp,
		TerminatePollAPI:     tp,
		GetVoterAPI:          gv,
		GetVotersAPI:         gvs,
		GetVotersByChoiceAPI: gvbc,
		VoteAPI:              v,
	}
}

func (s *server) ArchivePoll(ctx context.Context, req *poliwag.ArchivePollRequest) (*poliwag.ArchivePollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, archivePollAPITimeout)
	defer cancel()

	return s.ArchivePollAPI.ArchivePoll(ctx, req)
}

func (s *server) CreatePoll(ctx context.Context, req *poliwag.CreatePollRequest) (*poliwag.CreatePollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, createPollAPITimeout)
	defer cancel()

	return s.CreatePollAPI.CreatePoll(ctx, req)
}

func (s *server) GetPoll(ctx context.Context, req *poliwag.GetPollRequest) (*poliwag.GetPollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, getPollAPITimeout)
	defer cancel()

	return s.GetPollAPI.GetPoll(ctx, req)
}

func (s *server) GetViewablePoll(ctx context.Context, req *poliwag.GetViewablePollRequest) (*poliwag.GetViewablePollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, getViewablePollAPITimeout)
	defer cancel()

	return s.GetViewablePollAPI.GetViewablePoll(ctx, req)
}

func (s *server) TerminatePoll(ctx context.Context, req *poliwag.TerminatePollRequest) (*poliwag.TerminatePollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, terminatePollAPITimeout)
	defer cancel()

	return s.TerminatePollAPI.TerminatePoll(ctx, req)
}

func (s *server) GetPolls(ctx context.Context, req *poliwag.GetPollsRequest) (*poliwag.GetPollsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, getPollsAPITimeout)
	defer cancel()

	return s.GetPollsAPI.GetPolls(ctx, req)
}

func (s *server) GetVoter(ctx context.Context, req *poliwag.GetVoterRequest) (*poliwag.GetVoterResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, getVoterAPITimeout)
	defer cancel()

	return s.GetVoterAPI.GetVoter(ctx, req)
}

func (s *server) GetVoters(ctx context.Context, req *poliwag.GetVotersRequest) (*poliwag.GetVotersResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, getVotersAPITimeout)
	defer cancel()

	return s.GetVotersAPI.GetVoters(ctx, req)
}

func (s *server) GetVotersByChoice(ctx context.Context, req *poliwag.GetVotersByChoiceRequest) (*poliwag.GetVotersByChoiceResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, getVotersByChoiceAPITimeout)
	defer cancel()

	return s.GetVotersByChoiceAPI.GetVotersByChoice(ctx, req)
}

func (s *server) Vote(ctx context.Context, req *poliwag.VoteRequest) (*poliwag.VoteResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, voteAPITimeout)
	defer cancel()

	return s.VoteAPI.Vote(ctx, req)
}
