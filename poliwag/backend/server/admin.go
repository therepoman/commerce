package server

import (
	"context"

	"code.justin.tv/commerce/poliwag/backend/api/admin_get_poll"
	"code.justin.tv/commerce/poliwag/backend/api/admin_get_polls"
	"code.justin.tv/commerce/poliwag/backend/api/moderate_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

type adminServer struct {
	adminGetPollAPI  admin_get_poll.API
	adminGetPollsAPI admin_get_polls.API
	moderatePollAPI  moderate_poll.API
}

func NewAdminServer(adminGetPollAPI admin_get_poll.API, adminGetPollsAPI admin_get_polls.API, moderatePollAPI moderate_poll.API) poliwag_admin.PoliwagAdminAPI {
	return &adminServer{
		adminGetPollAPI:  adminGetPollAPI,
		adminGetPollsAPI: adminGetPollsAPI,
		moderatePollAPI:  moderatePollAPI,
	}
}

func (s adminServer) AdminGetPoll(ctx context.Context, req *poliwag_admin.AdminGetPollRequest) (*poliwag_admin.AdminGetPollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, Timeout)
	defer cancel()

	return s.adminGetPollAPI.AdminGetPoll(ctx, req)
}

func (s adminServer) AdminGetPolls(ctx context.Context, req *poliwag_admin.AdminGetPollsRequest) (*poliwag_admin.AdminGetPollsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, Timeout)
	defer cancel()

	return s.adminGetPollsAPI.AdminGetPolls(ctx, req)
}

func (s adminServer) ModeratePoll(ctx context.Context, req *poliwag_admin.ModeratePollRequest) (*poliwag_admin.ModeratePollResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, Timeout)
	defer cancel()

	return s.moderatePollAPI.ModeratePoll(ctx, req)
}
