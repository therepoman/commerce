package input_automod

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/chat/zuma/app/api"
	zuma_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/zuma/client"
)

func TestModerator_ValidateInput(t *testing.T) {
	Convey("Given an moderator", t, func() {
		zumaClient := new(zuma_client_mock.Client)

		m := moderator{
			zuma: zumaClient,
		}

		senderID := "sender-id"
		input := "input"

		Convey("Errors when the zuma call errors", func() {
			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{}, errors.New("test-error"))
			So(m.ValidateInput(context.Background(), senderID, input), ShouldNotBeNil)
		})

		Convey("Succeed when the zuma call is all passed", func() {
			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
				EnforceProperties: api.EnforceProperties{
					AllPassed: true,
				},
			}, nil)
			So(m.ValidateInput(context.Background(), senderID, input), ShouldBeNil)
		})

		Convey("Errors when the automod fails", func() {
			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
				EnforceProperties: api.EnforceProperties{
					AllPassed: false,
					AutoMod: &api.AutoModResponse{
						GenericEnforcementResponse: api.GenericEnforcementResponse{
							Passed: false,
						},
					},
				},
			}, nil)
			So(m.ValidateInput(context.Background(), senderID, input), ShouldNotBeNil)
		})
	})
}
