package input_automod

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"

	zumaApi "code.justin.tv/chat/zuma/app/api"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/errors"
)

// Checks if the given input passes automod.
type Moderator interface {
	ValidateInput(ctx context.Context, senderID string, input string) error
}

type moderator struct {
	zuma zuma.Client
}

func NewModerator(zuma zuma.Client) Moderator {
	return &moderator{
		zuma: zuma,
	}
}

func (m *moderator) ValidateInput(ctx context.Context, senderID string, input string) error {
	enforcementRules := zumaApi.EnforcementRuleset{
		AutoMod:             true,
		Banned:              false,
		Blocked:             false,
		ChannelBlockedTerms: false,
		DisposableEmail:     false,
		EmotesOnly:          false,
		Entropy:             false,
		FollowersOnly:       false,
		Spam:                false,
		SubscribersOnly:     false,
		Suspended:           false,
		VerifiedOnly:        false,
		Zalgo:               true,
	}

	enforcementConfig := zumaApi.EnforcementConfiguration{
		DefaultAutoModLevel: pointers.IntP(4),
	}

	extractMessageReq := zumaApi.EnforceMessageRequest{
		SkipExtraction:    true,
		SenderID:          senderID,
		MessageText:       input,
		EnforcementRules:  enforcementRules,
		EnforcementConfig: enforcementConfig,
	}

	// Check if the user is a channel mod by calling zuma
	var zumaResponse zuma_api.EnforceMessageResponse
	var reqError error

	err := hystrix.CallTwirpWithHystrix(hystrix.ZumaEnforceMessageCommand, hystrix.HystrixDo, func() error {
		zumaResponse, reqError = m.zuma.EnforceMessage(ctx, extractMessageReq, nil)
		return reqError
	})
	if err != nil {
		msg := fmt.Sprintf("error getting zuma eligibility from zuma service")
		logrus.WithFields(logrus.Fields{
			"senderID": senderID,
			"message":  input,
		}).WithError(err).Error(msg)
		return errors.InternalError.Wrap(err, msg)
	}

	if zumaResponse.AllPassed {
		return nil
	} else if zumaResponse.AutoMod != nil && !zumaResponse.AutoMod.Passed {
		return errors.IllegalArgument.New("failed automod check with input \"%s\"", input)
	} else {
		return errors.IllegalArgument.New("create poll failed zuma eligibility.")
	}
}
