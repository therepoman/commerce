package backend

import (
	"net/http"
	"time"

	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-playground/validator/v10"
	"github.com/go-redis/redis/v7"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/cb/hallpass/client/hallpass"
	"code.justin.tv/chat/awshttptelemetry"
	"code.justin.tv/chat/copo/proto/copo"
	"code.justin.tv/chat/httptelemetry"
	"code.justin.tv/chat/pubsub-go-pubclient/client"
	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/chat/rediczar/runmetrics/telemetryrunmetrics"
	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/chat/twitchclienthttptelemetry"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/gogogadget/aws/sns"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	paydaylegacy "code.justin.tv/commerce/payday/client"
	payday "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/clients/aurora"
	"code.justin.tv/commerce/poliwag/clients/aurora/gorm"
	"code.justin.tv/commerce/poliwag/clients/aurora/mysql"
	"code.justin.tv/commerce/poliwag/clients/datascience"
	"code.justin.tv/commerce/poliwag/clients/lock"
	"code.justin.tv/commerce/poliwag/clients/sandstorm"
	"code.justin.tv/commerce/poliwag/clients/slack"
	"code.justin.tv/commerce/poliwag/clients/spade"
	"code.justin.tv/commerce/poliwag/clients/stepfn"
	"code.justin.tv/commerce/poliwag/config"
	eventbus_publisher "code.justin.tv/eventbus/client/publisher"
	"code.justin.tv/foundation/twitchclient"
	ripley "code.justin.tv/revenue/ripley/rpc"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/web/users-service/client/usersclient_internal"
)

type clients struct {
	aurora       mysql.Client                        `validate:"required"`
	gorm         gorm.Client                         `validate:"required"`
	zuma         zuma.Client                         `validate:"required"`
	copo         copo.Copo                           `validate:"required"`
	ripley       ripley.Ripley                       `validate:"required"`
	hallpass     hallpass.Client                     `validate:"required"`
	payday       payday.Payday                       `validate:"required"`
	paydayLegacy paydaylegacy.Client                 `validate:"required"`
	stepfn       stepfn.Client                       `validate:"required"`
	sandstorm    sandstorm.Client                    `validate:"required"`
	pubsub       client.PubClient                    `validate:"required"`
	eventbus     *eventbus_publisher.Publisher       `validate:"required"`
	redis        *redis.Client                       `validate:"required"`
	rediczar     *rediczar.Client                    `validate:"required"`
	locking      lock.LockingClient                  `validate:"required"`
	spade        spade.Client                        `validate:"required"`
	datascience  datascience.Tracker                 `validate:"required"`
	subs         substwirp.Subscriptions             `validate:"required"`
	users        usersclient_internal.InternalClient `validate:"required"`
	pantheon     pantheonrpc.Pantheon                `validate:"required"`
	tmi          tmi.Client                          `validate:"required"`
	sns          sns.Client                          `validate:"required"`
	sqs          *sqs.SQS                            `validate:"required"`
	dynamo       *dynamodb.DynamoDB                  `validate:"required"`
	dax          dynamodbiface.DynamoDBAPI           `validate:"required"`
	slack        slack.Client                        `validate:"required"`
}

func createClients(cfg *config.Config, stats statsd.Statter, sampleReporter *telemetry.SampleReporter, spadeClient spade.Client, s2s2Client *s2s2.S2S2) (*clients, error) {
	sess, err := session.NewSession(aws.NewConfig().
		WithRegion(cfg.AWSRegion).
		WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint).
		WithHTTPClient(httpClient(sampleReporter)))
	if err != nil {
		return nil, err
	}
	sess = awshttptelemetry.AttachServiceAndOperation(sess)

	sandstormClient, err := sandstorm.New(cfg)
	if err != nil {
		return nil, err
	}

	auroraCfg := aurora.Config{
		Username:                cfg.Aurora.Username,
		PasswordSandstormSecret: cfg.Aurora.PasswordSandstormSecret,
		Endpoint:                cfg.Aurora.Endpoint,
		DBName:                  cfg.Aurora.DBName,
	}

	log.Infof("Setting up mysql client...")
	mysqlClient, err := mysql.NewClient(auroraCfg, sandstormClient)
	if err != nil {
		return nil, err
	}

	log.Infof("Setting up gorm client...")
	gormClient, err := gorm.NewClient(auroraCfg, sandstormClient)
	if err != nil {
		return nil, err
	}

	clientHttpTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: DefaultNumberOfConnections,
	}

	roundTripperWrappers := []func(http.RoundTripper) http.RoundTripper{
		func(rt http.RoundTripper) http.RoundTripper {
			return &httptelemetry.RoundTripper{
				SampleReporter: sampleReporter,
				RoundTripper:   rt,
				PreRoundTrip:   twitchclienthttptelemetry.AttachServiceAndOperation,
			}
		},
	}

	zumaClient, err := zuma.NewClient(twitchclient.ClientConf{
		Host:                 cfg.Endpoints.Zuma,
		Transport:            clientHttpTransport,
		Stats:                stats,
		RoundTripperWrappers: roundTripperWrappers,
	})
	if err != nil {
		return nil, err
	}

	hallpassClient, err := hallpass.NewClient(twitchclient.ClientConf{
		Host:                 cfg.Endpoints.Hallpass,
		Transport:            clientHttpTransport,
		Stats:                stats,
		RoundTripperWrappers: roundTripperWrappers,
	})
	if err != nil {
		return nil, err
	}

	var paydayClient payday.Payday
	if s2s2Client != nil {
		paydayClient = payday.NewPaydayProtobufClient(cfg.Endpoints.Payday, httpClientTwirpS2S2(sampleReporter, s2s2Client))
	} else {
		paydayClient = payday.NewPaydayProtobufClient(cfg.Endpoints.Payday, httpClientTwirp(sampleReporter))
	}

	paydayLegacyClient, err := paydaylegacy.NewClient(twitchclient.ClientConf{
		Host:                 cfg.Endpoints.Payday,
		Transport:            clientHttpTransport,
		RoundTripperWrappers: roundTripperWrappers,
	})
	if err != nil {
		return nil, err
	}

	pubsubClient, err := pubclient.NewPubClient(twitchclient.ClientConf{
		Host:                 cfg.Endpoints.Pubsub,
		Stats:                stats,
		Transport:            clientHttpTransport,
		RoundTripperWrappers: roundTripperWrappers,
	})
	if err != nil {
		return nil, err
	}

	eventbusEnv := eventbus_publisher.EnvStaging
	if cfg.EnvironmentName == string(config.Prod) {
		eventbusEnv = eventbus_publisher.EnvProduction
	}

	eventbusClient, err := eventbus_publisher.New(eventbus_publisher.Config{
		Session:     sess,
		Environment: eventbusEnv,
		EventTypes:  eventbus.EventTypesToPublish,
	})
	if err != nil {
		return nil, err
	}

	log.Infof("Setting up redis client...")
	redisClient := redefault.NewSingleNodeClient(cfg.Endpoints.Redis, nil)

	rediczarClient := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			Redis: redisClient,
			RunMetrics: &telemetryrunmetrics.StatTracker{
				SampleReporter: sampleReporter,
			},
		},
		RunMetrics: &telemetryrunmetrics.StatTracker{
			SampleReporter: sampleReporter,
		},
	}

	usersClient, err := usersclient_internal.NewClient(twitchclient.ClientConf{
		Host:                 cfg.Endpoints.Users,
		Transport:            clientHttpTransport,
		RoundTripperWrappers: roundTripperWrappers,
	})
	if err != nil {
		return nil, err
	}

	tmiClient, err := tmi.NewClient(twitchclient.ClientConf{
		Host:                 cfg.Endpoints.TMI,
		Transport:            clientHttpTransport,
		RoundTripperWrappers: roundTripperWrappers,
	})
	if err != nil {
		return nil, err
	}

	log.Info("Setting up Dynamo client...")
	dynamoDBClient := dynamodb.New(sess)

	log.Info("Setting up DAX client...")
	var daxClient dynamodbiface.DynamoDBAPI
	if cfg.EnvironmentName == string(config.Local) {
		log.Info("Running in local, using Dynamo client in place of DAX")
		daxClient = dynamoDBClient
	} else {
		daxConfig := dax.NewConfigWithSession(*sess)
		daxConfig.HostPorts = []string{cfg.Endpoints.DAX}
		daxConfig.Region = cfg.AWSRegion
		daxConfig.RequestTimeout = time.Second * 2 // default is 1 minute aintnobodygottimeforthat.jpeg
		daxClient, err = dax.New(daxConfig)
		if err != nil {
			return nil, err
		}
	}

	log.Info("Setting up Slack client...")
	slackClient, err := slack.NewClient(cfg.Slack.OauthTokenSandstormSecret, sandstormClient)
	if err != nil {
		return nil, err
	}

	out := &clients{
		aurora:       mysqlClient,
		gorm:         gormClient,
		zuma:         zumaClient,
		copo:         copo.NewCopoProtobufClient(cfg.Endpoints.Copo, httpClientTwirp(sampleReporter)),
		ripley:       ripley.NewRipleyProtobufClient(cfg.Endpoints.Ripley, httpClientTwirp(sampleReporter)),
		hallpass:     hallpassClient,
		payday:       paydayClient,
		paydayLegacy: paydayLegacyClient,
		stepfn:       stepfn.NewClient(cfg, stats, sampleReporter, httpClient(sampleReporter)),
		sandstorm:    sandstormClient,
		pubsub:       pubsubClient,
		eventbus:     eventbusClient,
		redis:        redisClient,
		rediczar:     rediczarClient,
		locking:      lock.NewRedisLockingClient(redisClient),
		spade:        spadeClient,
		datascience:  datascience.NewTracker(spadeClient),
		subs:         substwirp.NewSubscriptionsProtobufClient(cfg.Endpoints.Subscriptions, httpClientTwirp(sampleReporter)),
		users:        usersClient,
		pantheon:     pantheonrpc.NewPantheonProtobufClient(cfg.Endpoints.Pantheon, httpClientTwirp(sampleReporter)),
		tmi:          tmiClient,
		sns:          snsClient(sess),
		sqs:          sqs.New(sess, &aws.Config{Region: aws.String(cfg.AWSRegion), STSRegionalEndpoint: endpoints.RegionalSTSEndpoint}),
		dynamo:       dynamoDBClient,
		dax:          daxClient,
		slack:        slackClient,
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
