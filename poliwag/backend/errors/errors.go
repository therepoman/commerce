package errors

import (
	"github.com/joomcode/errorx"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/proto/errors"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

var (
	PoliwagError = errorx.NewNamespace("Error")

	NotFound         = PoliwagError.NewType("NotFound")
	PermissionDenied = PoliwagError.NewType("PermissionDenied")

	IllegalArgument = errorx.IllegalArgument
	InternalError   = errorx.InternalError
	IllegalState    = errorx.IllegalState
	NotImplemented  = errorx.NotImplemented

	// Vote errors
	PollNotFound                     = PoliwagError.NewType("PollNotFound")
	PollNotActive                    = PoliwagError.NewType("PollNotActive")
	VoteIDConflict                   = PoliwagError.NewType("VoteIDConflict")
	MultiChoiceVoteForbidden         = PoliwagError.NewType("MultiChoiceVoteForbidden")
	InvalidChoiceID                  = PoliwagError.NewType("InvalidChoiceID")
	InvalidBitsAmount                = PoliwagError.NewType("InvalidBitsAmount")
	InsufficientBitsBalance          = PoliwagError.NewType("InsufficientBitsBalance")
	TokensRequired                   = PoliwagError.NewType("TokensRequired")
	SubscriberRequired               = PoliwagError.NewType("SubscriberRequired")
	SelfBitsVoteNotAllowed           = PoliwagError.NewType("SelfBitsVoteNotAllowed")
	SelfChannelPointsVoteNotAllowed  = PoliwagError.NewType("SelfChannelPointsVoteNotAllowed")
	InvalidChannelPointsAmount       = PoliwagError.NewType("InvalidChannelPointsAmount")
	InsufficientChannelPointsBalance = PoliwagError.NewType("InsufficientChannelPointsBalance")
	RateLimited                      = PoliwagError.NewType("RateLimited")

	// Poll creation errors
	AutoModFailed           = PoliwagError.NewType("AutomodFailed")
	PollAlreadyActive       = PoliwagError.NewType("PollAlreadyActive")
	BitsNotEnabled          = PoliwagError.NewType("BitsNotEnabled")
	ChannelPointsNotEnabled = PoliwagError.NewType("ChannelPointsNotEnabled")

	// Poll Update error
	PollUpdateThrottled   = PoliwagError.NewType("PollUpdateThrottled")
	PollConflictingUpdate = PoliwagError.NewType("PollConflictingUpdate")
)

func PoliwagVoteErrorToTwirpError(err error) twirp.Error {
	switch errorx.TypeSwitch(err, PollNotFound, PollNotActive, VoteIDConflict, MultiChoiceVoteForbidden, InvalidChoiceID, InvalidBitsAmount, InsufficientBitsBalance, InsufficientChannelPointsBalance, TokensRequired, SubscriberRequired, SelfBitsVoteNotAllowed, SelfChannelPointsVoteNotAllowed, InvalidChannelPointsAmount, RateLimited) {
	case PollNotFound:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_POLL_NOT_FOUND.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.NotFound, err.Error()), errorResponse)
	case PollNotActive:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_POLL_NOT_ACTIVE.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
	case VoteIDConflict:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_VOTE_ID_CONFLICT.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case MultiChoiceVoteForbidden:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_MULTI_CHOICE_VOTE_FORBIDDEN.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case InvalidChoiceID:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_INVALID_CHOICE_ID.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.NotFound, err.Error()), errorResponse)
	case InvalidBitsAmount:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_INVALID_BITS_AMOUNT.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case InsufficientBitsBalance:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_INSUFFICIENT_BITS_BALANCE.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case TokensRequired:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_TOKENS_REQUIRED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case SubscriberRequired:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_SUBSCRIBER_REQUIRED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.PermissionDenied, err.Error()), errorResponse)
	case SelfBitsVoteNotAllowed:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_SELF_BITS_VOTE_NOT_ALLOWED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.PermissionDenied, err.Error()), errorResponse)
	case SelfChannelPointsVoteNotAllowed:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_SELF_CHANNEL_POINTS_VOTE_NOT_ALLOWED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.PermissionDenied, err.Error()), errorResponse)
	case InvalidChannelPointsAmount:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_INVALID_CHANNEL_POINTS_AMOUNT.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case InsufficientChannelPointsBalance:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_INSUFFICIENT_CHANNEL_POINTS_BALANCE.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case RateLimited:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.VoteErrorCode_RATE_LIMITED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.ResourceExhausted, err.Error()), errorResponse)
	case errorx.NotRecognisedType():
		fallthrough
	default:
		return PoliwagErrorToTwirpError(err)
	}
}

func PoliwagCreateErrorToTwirpError(err error) twirp.Error {
	switch errorx.TypeSwitch(err, AutoModFailed, PollAlreadyActive, BitsNotEnabled, ChannelPointsNotEnabled) {
	case AutoModFailed:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.PollCreationErrorCode_AUTOMOD_FAILED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case PollAlreadyActive:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.PollCreationErrorCode_POLL_ALREADY_ACTIVE.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case BitsNotEnabled:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.PollCreationErrorCode_CHANNEL_NOT_BITS_ENABLED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case ChannelPointsNotEnabled:
		errorResponse := errors.PoliwagError{
			ErrorCode: poliwag.PollCreationErrorCode_CHANNEL_NOT_CHANNEL_POINTS_ENABLED.String(),
		}
		return errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case errorx.NotRecognisedType():
		fallthrough
	default:
		return PoliwagErrorToTwirpError(err)
	}
}

func PoliwagErrorToTwirpError(err error) twirp.Error {
	switch errorx.TypeSwitch(err, NotFound, PermissionDenied, IllegalArgument, InternalError, IllegalState, NotImplemented) {
	case IllegalArgument:
		return twirp.NewError(twirp.InvalidArgument, err.Error())
	case NotFound:
		return twirp.NewError(twirp.NotFound, err.Error())
	case PermissionDenied:
		return twirp.NewError(twirp.PermissionDenied, err.Error())
	case IllegalState:
		return twirp.NewError(twirp.FailedPrecondition, err.Error())
	case NotImplemented:
		return twirp.NewError(twirp.Unimplemented, err.Error())
	case InternalError:
		fallthrough
	case errorx.NotRecognisedType():
		fallthrough
	default:
		logrus.WithError(err).Error("returning twirp internal error from general error handler")
		return twirp.InternalErrorWith(err)
	}
}
