package backend

import (
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-playground/validator/v10"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	choices_dao "code.justin.tv/commerce/poliwag/backend/dao/choices"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	voters_dao "code.justin.tv/commerce/poliwag/backend/dao/voters"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/config"
)

type daos struct {
	// Dynamo DAOs
	polls   polls_dao.PollsDAO     `validate:"required"`
	choices choices_dao.ChoicesDAO `validate:"required"`
	voters  voters_dao.VotersDAO   `validate:"required"`

	// Aurora DAOs
	votes votes.DAO `validate:"required"`
}

func createDAOs(cfg *config.Config, stats statsd.Statter, sampleReporter *telemetry.SampleReporter, c *clients) (*daos, error) {
	out := &daos{
		polls:   polls_dao.NewPollsDAO(cfg.DynamoSuffix, stats, sampleReporter, c.dynamo, c.dax, c.pantheon),
		choices: choices_dao.NewChoicesDAO(cfg.DynamoSuffix, stats, sampleReporter, c.dynamo, c.dax),
		voters:  voters_dao.NewVotersDAO(cfg.DynamoSuffix, c.dynamo, c.dax, c.pantheon),
		votes:   votes.NewDAO(c.gorm),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
