package backend

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"sync"
	"time"

	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws/client"
	awssns "github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-playground/validator/v10"
	"github.com/go-redis/redis/v7"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	metricsmiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	poller "code.justin.tv/amzn/TwitchTelemetryPollingCollector"
	"code.justin.tv/chat/httptelemetry"
	"code.justin.tv/chat/machineid"
	"code.justin.tv/chat/telemetryext"
	"code.justin.tv/commerce/gogogadget/aws/sns"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/migration"
	"code.justin.tv/commerce/poliwag/backend/server"
	"code.justin.tv/commerce/poliwag/backend/stepfn/counter"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	"code.justin.tv/commerce/poliwag/backend/worker"
	"code.justin.tv/commerce/poliwag/clients/aurora/gorm"
	"code.justin.tv/commerce/poliwag/clients/cloudwatchlogger"
	"code.justin.tv/commerce/poliwag/clients/spade"
	stepfn_client "code.justin.tv/commerce/poliwag/clients/stepfn"
	"code.justin.tv/commerce/poliwag/config"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
	"code.justin.tv/commerce/poliwag/proto/poliwag_internal"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/common/hystrixtelemetry"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/twirpmetric"
)

const (
	localhostServerEndpoint    = "http://localhost:8000"
	cloudwatchSubstage         = "primary"
	serviceName                = "poliwag"
	DefaultNumberOfConnections = 200
)

type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() poliwag.PoliwagAPI
	TwirpInternalServer() poliwag_internal.PoliwagInternalAPI
	TwirpAdminServer() poliwag_admin.PoliwagAdminAPI
	Shutdown(ctx context.Context)
	Statter() statsd.Statter
	Migrate(ctx context.Context) error
	GetLambdaHandlers() *LambdaHandlers
}

type backend struct {
	// Servers
	server         poliwag.PoliwagAPI                  `validate:"required"`
	internalServer poliwag_internal.PoliwagInternalAPI `validate:"required"`
	adminServer    poliwag_admin.PoliwagAdminAPI       `validate:"required"`

	// Clients
	statsd         statsd.Statter                    `validate:"required"`
	sampleReporter *telemetry.SampleReporter         `validate:"required"`
	stepFnClient   stepfn_client.Client              `validate:"required"`
	redisClient    redis.UniversalClient             `validate:"required"`
	gormClient     gorm.Client                       `validate:"required"`
	sqsClient      sqsiface.SQSAPI                   `validate:"required"`
	auditLogger    cloudwatchlogger.CloudWatchLogger `validate:"required"`

	// Aurora Migrator
	migrator migration.Migrator `validate:"required"`

	// Lambdas
	lambdaHandlers *LambdaHandlers `validate:"required"`

	// Stepfn Counters
	pollCompleteCounter counter.Counter `validate:"required"`

	// Config
	config *config.Config `validate:"required"`

	// Worker Terminators
	terminators []worker.NamedTerminator // only required is isLambda==true
}

func NewBackend(cfg *config.Config, isLambda bool, sampleReporter *telemetry.SampleReporter, spadeClient spade.Client, s2s2Client *s2s2.S2S2) (Backend, error) {
	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	var statters []statsd.Statter
	if cfg.CloudWatchMetricsEnabled {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.AWSRegion,
			ServiceName:       serviceName,
			Stage:             cfg.EnvironmentName,
			Substage:          cloudwatchSubstage,
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})
		statters = []statsd.Statter{cloudwatchStatter}
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	hystrixCollector := hystrix.NewHystrixMetricsCollector(stats)

	auditLogger, err := cloudwatchlogger.NewCloudWatchLogClient(cfg.AWSRegion, cfg.AuditLogGroupName)
	if err != nil {
		return nil, err
	}

	c, err := createClients(cfg, stats, sampleReporter, spadeClient, s2s2Client)
	if err != nil {
		return nil, err
	}

	d, err := createDAOs(cfg, stats, sampleReporter, c)
	if err != nil {
		return nil, err
	}

	executors, err := createStepFnExecutors(cfg, c)
	if err != nil {
		return nil, err
	}

	auths, err := createAuthorizers(c, d)
	if err != nil {
		return nil, err
	}

	mods, err := createModerators(c)
	if err != nil {
		return nil, err
	}

	cache, err := createCaches(c, d)
	if err != nil {
		return nil, err
	}

	ctrls, err := createControllers(cfg, isLambda, stats, sampleReporter, c, d, cache, executors)
	if err != nil {
		return nil, err
	}

	a, err := createAPIs(sampleReporter, auditLogger, c, auths, mods, ctrls)
	if err != nil {
		return nil, err
	}

	steps, err := createLambdaSteps(d, ctrls)
	if err != nil {
		return nil, err
	}

	lambdaHandlers, err := createLambdaHandlers(stats, sampleReporter, d, ctrls, steps)
	if err != nil {
		return nil, err
	}

	pollCompleteCounter := counter.NewCounter(cfg.StepFn.PollCompleteStepFn.StateMachineARN, "NumberOfConcurrentViewablePolls", "NumConcurrentViewablePolls", c.stepfn, stats, sampleReporter)

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixCollector.NewHystrixCommandMetricsCollector(name)
	})

	twitchTelemetryStatter := hystrixtelemetry.NewTwitchTelemetryStatter(sampleReporter, 10*time.Second, nil)
	metricCollector.Registry.Register(twitchTelemetryStatter.Create)

	b := &backend{
		server: server.NewServer(
			a.archivePollAPI,
			a.createPollAPI,
			a.getPollAPI,
			a.getPollsAPI,
			a.getViewablePollAPI,
			a.terminatePollAPI,
			a.getVoterAPI,
			a.getVotersAPI,
			a.getVotersByChoiceAPI,
			a.voteAPI,
		),
		internalServer: server.NewInternalServer(
			c.payday,
			c.hallpass,
			c.ripley,
			c.zuma,
			c.aurora,
		),
		adminServer:         server.NewAdminServer(a.adminGetPollAPI, a.adminGetPollsAPI, a.moderatePollAPI),
		statsd:              stats,
		sampleReporter:      sampleReporter,
		stepFnClient:        c.stepfn,
		redisClient:         c.redis,
		gormClient:          c.gorm,
		sqsClient:           c.sqs,
		auditLogger:         auditLogger,
		migrator:            migration.NewMigrator(c.gorm),
		lambdaHandlers:      lambdaHandlers,
		pollCompleteCounter: pollCompleteCounter,
		config:              cfg,
		terminators:         nil, // initialized within b.setupWorkers()
	}

	if !isLambda {
		w, err := createWorkers(c, d, ctrls)
		if err != nil {
			return nil, err
		}

		b.setupWorkers(w)
		go pollCompleteCounter.CountAtInterval()
	}

	if err := validator.New().Struct(b); err != nil {
		return nil, err
	}

	return b, nil
}

func (b *backend) Ping(w http.ResponseWriter, r *http.Request) {
	poliwagInternalClient := poliwag_internal.NewPoliwagInternalAPIProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err := poliwagInternalClient.HealthCheck(r.Context(), &poliwag_internal.HealthCheckRequest{})
	if err != nil {
		log.WithError(err).Error("Could not ping poliwag server")
		w.WriteHeader(http.StatusInternalServerError)
		_, innerErr := io.WriteString(w, err.Error())
		if innerErr != nil {
			log.WithError(innerErr).Error("Error writing http response")
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func (b *backend) TwirpServer() poliwag.PoliwagAPI {
	return b.server
}

func (b *backend) TwirpInternalServer() poliwag_internal.PoliwagInternalAPI {
	return b.internalServer
}

func (b *backend) TwirpAdminServer() poliwag_admin.PoliwagAdminAPI {
	return b.adminServer
}

func (b *backend) Statter() statsd.Statter {
	return b.statsd
}

func (b *backend) Shutdown(ctx context.Context) {
	var wg sync.WaitGroup

	if b.redisClient != nil {
		wg.Add(1)
		go func(redis redis.UniversalClient) {
			err := redis.Close()
			if err != nil {
				log.WithError(err).Error("error closing redis client")
			}
			wg.Done()
		}(b.redisClient)
	}

	if b.gormClient != nil {
		wg.Add(1)
		go func(gc gorm.Client) {
			err := gc.Shutdown(ctx)
			if err != nil {
				log.WithError(err).Error("error shutting down gorm client")
			}
			wg.Done()
		}(b.gormClient)
	}

	if b.auditLogger != nil {
		wg.Add(1)
		go func(al cloudwatchlogger.CloudWatchLogger) {
			al.Shutdown()
			wg.Done()
		}(b.auditLogger)
	}

	wg.Add(1)
	go func(c counter.Counter) {
		c.Stop()
		wg.Done()
	}(b.pollCompleteCounter)

	for _, terminator := range b.terminators {
		wg.Add(1)
		go func(t worker.NamedTerminator) {
			terminatorErr := t.Shutdown(context.Background())
			if terminatorErr != nil {
				log.WithError(terminatorErr).Error(fmt.Sprintf("error shutting down %s worker", t.Name()))
			}
			wg.Done()
		}(terminator)
	}

	allDone := make(chan bool)
	go func() {
		wg.Wait()
		allDone <- true
	}()

	select {
	case <-allDone:
		return
	case <-ctx.Done():
		log.Error("context timed out while shutting down")
	}
}

func (b *backend) Migrate(ctx context.Context) error {
	return b.migrator.Migrate(ctx)
}

func (b *backend) GetLambdaHandlers() *LambdaHandlers {
	return b.lambdaHandlers
}

func (b *backend) setupWorkers(w *workers) {
	b.terminators = b.setupSQSWorker(b.terminators, b.config.Queues.VotePantheonIngestionQueue, w.votePantheonIngestionWorker)
	b.terminators = b.setupSQSWorker(b.terminators, b.config.Queues.VotePollUpdateQueue, w.votePollUpdateWorker)
}

func (b *backend) setupSQSWorker(terminators []worker.NamedTerminator, queueConfig config.QueueConfig, w worker.Worker) []worker.NamedTerminator {
	if queueConfig.Name != "" {
		terminators = append(terminators, worker.NewSQSWorkerManager(queueConfig.Name, queueConfig.NumWorkers, 1, b.sqsClient, w, b.statsd, b.sampleReporter))
	}
	return terminators
}

// SetupTwitchTelemetry returns a telemetry.SampleReporter and a stop function to be called when the process exits.
// If isLambda is false then a machineSample reporter with a gometrics poller is started as well.
func SetupTwitchTelemetry(cfg *config.Config, isLambda bool) (*telemetry.SampleReporter, func()) {
	if !cfg.CloudWatchMetricsEnabled {
		return telemetry.NewNoOpSampleReporter(), func() {}
	}

	var stops []func()

	// Setup the process identifier. The values will be used as dimensions in CloudWatch metrics
	tpid := identifier.ProcessIdentifier{
		Service:  "Poliwag",
		Region:   cfg.AWSRegion,
		Stage:    cfg.EnvironmentName,
		Substage: cloudwatchSubstage,
		Version:  "",
		Machine:  "",
		LaunchID: "",
	}
	if config.IsCanary() {
		tpid.Substage = "canary"
	}
	if !isLambda {
		// This is only sent when EnableProcessAddressDimension is true
		tpid.Machine = machineid.Resolve()
	}

	// Create the reporter. This is what the application will use to send metrics
	var srf telemetryext.SampleReporterFactory
	sampleReporter := srf.Create(tpid)
	stops = append(stops, sampleReporter.Stop)

	if !isLambda {
		// Create a reporter that records the machine ID as a separate dimension
		machineSampleReporter := sampleReporter // copy struct
		machineSampleReporter.EnableProcessAddressDimension = true

		// Create gometrics polling with the machine reporter
		gometricsPoller := poller.NewGoStatsPollingCollector(10*time.Second, &machineSampleReporter.SampleBuilder, sampleReporter.SampleObserver, nil)
		// Start gometrics
		gometricsPoller.Start()
		stops = append(stops, gometricsPoller.Stop)
	}

	return &sampleReporter, func() {
		for _, stop := range stops {
			stop()
		}
	}
}

func httpClient(sampleReporter *telemetry.SampleReporter) *http.Client {
	return &http.Client{
		Transport: &httptelemetry.RoundTripper{
			SampleReporter: sampleReporter,
			RoundTripper: &http.Transport{
				MaxIdleConnsPerHost: DefaultNumberOfConnections,
			},
		},
	}
}

func httpClientTwirp(sampleReporter *telemetry.SampleReporter) *http.Client {
	return twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: DefaultNumberOfConnections,
		},
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			func(rt http.RoundTripper) http.RoundTripper {
				metricsOpMonitor := &metricsmiddleware.OperationMonitor{
					SampleReporter: *sampleReporter,
					AutoFlush:      false,
				}
				opStarter := &operation.Starter{
					OpMonitors: []operation.OpMonitor{
						metricsOpMonitor,
					},
				}
				return &twirpmetric.Client{
					Starter:   opStarter,
					Transport: rt,
				}
			},
		},
	})
}

func httpClientTwirpS2S2(sampleReporter *telemetry.SampleReporter, s2s2Client *s2s2.S2S2) *http.Client {
	return twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: DefaultNumberOfConnections,
		},
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			func(rt http.RoundTripper) http.RoundTripper {
				metricsOpMonitor := &metricsmiddleware.OperationMonitor{
					SampleReporter: *sampleReporter,
					AutoFlush:      false,
				}
				opStarter := &operation.Starter{
					OpMonitors: []operation.OpMonitor{
						metricsOpMonitor,
					},
				}
				return &twirpmetric.Client{
					Starter:   opStarter,
					Transport: rt,
				}
			},
			s2s2Client.RoundTripperWrapper,
		},
	})
}

func snsClient(p client.ConfigProvider) sns.Client {
	return &sns.SDKClientWrapper{
		SDKClient: awssns.New(p, sns.NewDefaultConfig()),
	}
}
