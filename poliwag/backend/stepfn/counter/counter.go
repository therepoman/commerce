package counter

import (
	"context"
	"time"

	"github.com/cactus/go-statsd-client/statsd"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/clients/stepfn"
)

const (
	countDelay = time.Minute
	timeout    = 5 * time.Second
)

type Counter interface {
	CountAtInterval()
	Stop()
}

type counter struct {
	stateMachineARN string
	oldMetricName   string
	metricName      string
	stop            chan bool
	stepFNClient    stepfn.Client
	stats           statsd.Statter
	sampleReporter  *telemetry.SampleReporter
}

func NewCounter(stateMachineARN string, oldMetricName, metricName string, stepFNClient stepfn.Client, stats statsd.Statter, sampleReporter *telemetry.SampleReporter) Counter {
	return &counter{
		stateMachineARN: stateMachineARN,
		oldMetricName:   oldMetricName,
		metricName:      metricName,
		stop:            make(chan bool),
		stepFNClient:    stepFNClient,
		stats:           stats,
		sampleReporter:  sampleReporter,
	}
}

func (c *counter) Stop() {
	c.stop <- true
}

func (c *counter) CountAtInterval() {
	// Wait a random duration before we start counting to evenly distribute counters across hosts
	initialStartDelay := random.Duration(0, 2*countDelay)
	initialStartDelayChan := time.After(initialStartDelay)
	select {
	case <-c.stop:
		return
	case <-initialStartDelayChan:
	}

	timer := time.NewTicker(countDelay)
	for {
		select {
		case <-c.stop:
			return
		case <-timer.C:
			c.count()
		}
	}
}

func (c *counter) count() {
	ctx, cancelFunc := context.WithTimeout(context.Background(), timeout)
	defer cancelFunc()

	numExecutions, err := c.stepFNClient.CountActiveExecutions(ctx, c.stateMachineARN)
	if err != nil {
		log.WithError(err).Error("error counting running stepfn executions")
		return
	}

	statErr := c.stats.Gauge(c.oldMetricName, int64(numExecutions), 1.0)
	if statErr != nil {
		log.WithError(err).Error("error emitting running stepfn executions metric")
	}
	c.sampleReporter.Report(c.metricName, float64(numExecutions), telemetry.UnitCount)
}
