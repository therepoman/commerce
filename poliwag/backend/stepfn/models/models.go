package models

import (
	"time"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type PollUpdateRequest struct {
	PollID         string         `json:"poll_id"`
	LifecycleState LifecycleState `json:"lifecycle_state"`
}

type PollUpdateResponse struct {
	PollID         string         `json:"poll_id"`
	LifecycleState LifecycleState `json:"lifecycle_state"`
	Done           bool           `json:"done"`
	WaitUntil      string         `json:"wait_until"`
}

type LifecycleState struct {
	Poll                   models.Poll               `json:"poll"`
	PrevChoiceAggregations []votes.ChoiceAggregation `json:"prev_choice_aggregations"`
	PrevPollAggregation    votes.PollAggregation     `json:"prev_poll_aggregation"`
}

type PollCompleteRequest struct {
	PollStartTime       time.Time `json:"poll_start_time"`
	PollID              string    `json:"poll_id"`
	PollDurationSeconds int       `json:"poll_duration_seconds"`
}

type PollCompleteResponse struct {
	PollID string `json:"poll_id"`
}

type PollTerminateRequest struct {
	PollID string `json:"poll_id"`
}

type PollTerminateResponse struct {
	PollID string `json:"poll_id"`
}

type PollArchiveRequest struct {
	PollID string `json:"poll_id"`
}

type PollModerateRequest struct {
	PollID string `json:"poll_id"`
}

type GetPollRequest struct {
	PollID string `json:"poll_id"`
}

type GetPollResponse struct {
	PollID string      `json:"poll_id"`
	Poll   models.Poll `json:"poll"`
}

type UserDeletionRequest interface {
	GetUserDeletionRequestData() *UserDeletionRequestData
}

type UserDeletionRequestData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type UserDeletionResponseData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type DeleteUserLeaderboardEntriesRequest struct {
	UserDeletionRequestData
}

func (r *DeleteUserLeaderboardEntriesRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type DeleteUserLeaderboardEntriesResponse struct {
	UserDeletionResponseData
}

type DeleteUserVotesRequest struct {
	UserDeletionRequestData
}

func (r *DeleteUserVotesRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type DeleteUserVotesResponse struct {
	UserDeletionResponseData
}

type ReportDeletionRequest struct {
	UserDeletionRequestData
}

func (r *ReportDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type ReportDeletionResponse struct {
	UserDeletionResponseData
	Timestamp time.Time `json:"timestamp"`
}
