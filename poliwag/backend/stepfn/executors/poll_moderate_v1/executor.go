package poll_moderate_v1

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
	"code.justin.tv/commerce/poliwag/clients/stepfn"
)

const (
	StepFnName = "poll_moderate"
)

type Executor interface {
	Execute(ctx context.Context, input PollModerateRequest) error
}

type PollModerateRequest = models.PollModerateRequest

type executor struct {
	arn    string
	client stepfn.Client
}

func NewExecutor(arn string, client stepfn.Client) Executor {
	return &executor{
		arn:    arn,
		client: client,
	}
}

func (e executor) Execute(ctx context.Context, input PollModerateRequest) error {
	executionName := fmt.Sprintf("%s.%s", StepFnName, input.PollID)
	return e.client.Execute(ctx, e.arn, executionName, input)
}
