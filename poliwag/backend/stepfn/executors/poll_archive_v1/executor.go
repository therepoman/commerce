package poll_archive_v1

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
	"code.justin.tv/commerce/poliwag/clients/stepfn"
)

const (
	StepFnName = "poll_archive"
)

type Executor interface {
	Execute(ctx context.Context, input PollArchiveRequest) error
}

type PollArchiveRequest = models.PollArchiveRequest

type executor struct {
	arn    string
	client stepfn.Client
}

func NewExecutor(arn string, client stepfn.Client) Executor {
	return &executor{
		arn:    arn,
		client: client,
	}
}

func (e executor) Execute(ctx context.Context, input PollArchiveRequest) error {
	executionName := fmt.Sprintf("%s.%s", StepFnName, input.PollID)
	return e.client.Execute(ctx, e.arn, executionName, input)
}
