package poll_update_v1

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
	"code.justin.tv/commerce/poliwag/clients/stepfn"
)

const (
	StepFnName = "poll_update"
)

type Executor interface {
	Execute(ctx context.Context, input PollUpdateRequest) error
}

type PollUpdateRequest = models.PollUpdateRequest

type executor struct {
	arn    string
	client stepfn.Client
}

func NewExecutor(arn string, client stepfn.Client) Executor {
	return &executor{
		arn:    arn,
		client: client,
	}
}

func (e executor) Execute(ctx context.Context, input PollUpdateRequest) error {
	executionName := fmt.Sprintf("%s.%s", StepFnName, input.PollID)
	return e.client.Execute(ctx, e.arn, executionName, input)
}
