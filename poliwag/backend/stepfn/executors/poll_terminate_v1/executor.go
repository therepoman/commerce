package poll_terminate_v1

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
	"code.justin.tv/commerce/poliwag/clients/stepfn"
)

const (
	StepFnName = "poll_terminate"
)

type Executor interface {
	Execute(ctx context.Context, input PollTerminateRequest) error
}

type PollTerminateRequest = models.PollTerminateRequest

type executor struct {
	arn    string
	client stepfn.Client
}

func NewExecutor(arn string, client stepfn.Client) Executor {
	return &executor{
		arn:    arn,
		client: client,
	}
}

func (e executor) Execute(ctx context.Context, input PollTerminateRequest) error {
	executionName := fmt.Sprintf("%s.%s", StepFnName, input.PollID)
	return e.client.Execute(ctx, e.arn, executionName, input)
}
