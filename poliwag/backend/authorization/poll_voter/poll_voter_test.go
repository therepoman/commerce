package poll_voter_test

import (
	"context"
	"errors"
	"testing"

	"github.com/joomcode/errorx"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/commerce/poliwag/backend/authorization/poll_voter"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	tmi_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/tmi/client"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/foundation/twitchclient"
)

func TestAuthorizer_Authorize(t *testing.T) {
	Convey("given a PollVoter authorizer", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		tmiClient := new(tmi_client_mock.Client)

		authorizer := poll_voter.NewAuthorizer(pollsDAO, tmiClient)

		pollID := "poll-id"
		ownedBy := "owned-by"
		userID := "user-id"

		Convey("when GetPoll errors", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, errors.New("error"))

			Convey("we should return an InternalError error", func() {
				err := authorizer.Authorize(context.Background(), userID, pollID)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
			})
		})

		Convey("when GetPoll returns no poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, nil)

			Convey("we should return a NotFound error", func() {
				err := authorizer.Authorize(context.Background(), userID, pollID)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.NotFound), ShouldBeTrue)
			})
		})

		Convey("when GetPoll returns a poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&models.Poll{
				PollID:  pollID,
				OwnedBy: ownedBy,
			}, nil)

			Convey("when GetBanStatus errors", func() {
				var statusRequesterUserID *string
				var reqOpts *twitchclient.ReqOpts
				tmiClient.On("GetBanStatus", mock.Anything, ownedBy, userID, statusRequesterUserID, reqOpts).Return(tmi.ChannelBannedUser{}, true, errors.New("error"))

				Convey("we should return a InternalError error", func() {
					err := authorizer.Authorize(context.Background(), userID, pollID)

					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
				})
			})

			Convey("when GetBanStatus returns true", func() {
				var statusRequesterUserID *string
				var reqOpts *twitchclient.ReqOpts
				tmiClient.On("GetBanStatus", mock.Anything, ownedBy, userID, statusRequesterUserID, reqOpts).Return(tmi.ChannelBannedUser{}, true, nil)

				Convey("we should return a PermissionDenied error", func() {
					err := authorizer.Authorize(context.Background(), userID, pollID)

					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.PermissionDenied), ShouldBeTrue)
				})
			})

			Convey("when GetBanStatus returns false", func() {
				var statusRequesterUserID *string
				var reqOpts *twitchclient.ReqOpts
				tmiClient.On("GetBanStatus", mock.Anything, ownedBy, userID, statusRequesterUserID, reqOpts).Return(tmi.ChannelBannedUser{}, false, nil)

				Convey("we should return no error", func() {
					err := authorizer.Authorize(context.Background(), userID, pollID)

					So(err, ShouldBeNil)
				})
			})
		})
	})
}
