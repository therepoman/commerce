package poll_voter

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"

	tmi "code.justin.tv/chat/tmi/client"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
)

// Checks if the user is authorized to vote in the given poll
// The user is authorized if:
// - They are not banned in chat
type Authorizer interface {
	Authorize(ctx context.Context, userID string, pollID string) error
}

type authorizer struct {
	pollsDAO  polls_dao.PollsDAO
	tmiClient tmi.Client
}

func NewAuthorizer(
	pollsDAO polls_dao.PollsDAO,
	tmiClient tmi.Client,
) Authorizer {
	return &authorizer{
		pollsDAO:  pollsDAO,
		tmiClient: tmiClient,
	}
}

func (a *authorizer) Authorize(ctx context.Context, userID string, pollID string) error {
	poll, err := a.pollsDAO.GetPoll(ctx, pollID, false)
	if err != nil {
		msg := fmt.Sprintf("error getting poll with id %s", pollID)
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return errors.InternalError.Wrap(err, msg)
	}

	// Poll does not exist
	if poll == nil {
		return errors.NotFound.New("poll %s does not exist", pollID)
	}

	// Check if the user is banned in channel
	var isChatBanned bool
	err = hystrix.CallTwirpWithHystrix(hystrix.TMIGetBanStatus, hystrix.HystrixDo, func() error {
		var reqError error
		_, isChatBanned, reqError = a.tmiClient.GetBanStatus(ctx, poll.OwnedBy, userID, nil, nil)

		return reqError
	})

	if err != nil {
		msg := fmt.Sprintf("error getting banned status from TMI")
		logrus.WithFields(logrus.Fields{
			"pollID":  pollID,
			"ownedBy": poll.OwnedBy,
			"userID":  userID,
		}).WithError(err).Error(msg)

		return errors.InternalError.Wrap(err, msg)
	}

	if isChatBanned {
		return errors.PermissionDenied.New("user %s is chat banned in channel %s and cannot vote in poll %s", userID, poll.OwnedBy, pollID)
	}

	return nil
}
