package voter_viewer_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/authorization/voter_viewer"
	poll_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestAuthorizer_Authorize(t *testing.T) {
	Convey("Given an authorizer", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		pollEditorAuthorizer := new(poll_editor_mock.Authorizer)

		a := voter_viewer.NewAuthorizer(pollsDAO, pollEditorAuthorizer)

		userID := "test-user-id"
		pollID := "test-poll-id"
		voterID := "another-user-id"

		Convey("Succeeds early when userID matches voterID", func() {
			So(a.Authorize(context.Background(), userID, pollID, userID), ShouldBeNil)
			So(pollEditorAuthorizer.Calls, ShouldHaveLength, 0)
		})

		Convey("Errors when pollEditorAuthorizer errors", func() {
			pollEditorAuthorizer.On("Authorize", mock.Anything, userID, pollID).Return(errors.New("test-error"))
			So(a.Authorize(context.Background(), userID, pollID, voterID), ShouldNotBeNil)
		})

		Convey("Succeeds when pollEditorAuthorizer succeeds", func() {
			pollEditorAuthorizer.On("Authorize", mock.Anything, userID, pollID).Return(nil)
			So(a.Authorize(context.Background(), userID, pollID, voterID), ShouldBeNil)
		})
	})
}
