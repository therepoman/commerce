package voter_viewer

import (
	"context"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
)

// Checks if the user is authorized to view details about a particular voter
// The user is authorized if:
// - They are the voter for whom they are requesting details
// - They are requesting details about a voter in a poll that they are authorized to edit (see poll_editor_authorizer)
type Authorizer interface {
	Authorize(ctx context.Context, userID string, pollID string, voterID string) error
}

type authorizer struct {
	pollsDAO             polls_dao.PollsDAO
	pollEditorAuthorizer poll_editor.Authorizer
}

func NewAuthorizer(
	pollsDAO polls_dao.PollsDAO,
	pollEditorAuthorizer poll_editor.Authorizer,
) Authorizer {
	return &authorizer{
		pollsDAO:             pollsDAO,
		pollEditorAuthorizer: pollEditorAuthorizer,
	}
}

func (a *authorizer) Authorize(ctx context.Context, userID string, pollID string, voterID string) error {
	if userID == voterID {
		return nil
	}

	return a.pollEditorAuthorizer.Authorize(ctx, userID, pollID)
}
