package poll_editor_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	"code.justin.tv/commerce/poliwag/backend/models"
	hallpass_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/cb/hallpass/client/hallpass"
	channel_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestAuthorizer_Authorize(t *testing.T) {
	Convey("Given an authorizer", t, func() {
		hallpassClient := new(hallpass_client_mock.Client)
		pollsDAO := new(polls_dao_mock.PollsDAO)
		channelEditorAuthorizer := new(channel_editor_mock.Authorizer)

		a := poll_editor.NewAuthorizer(
			hallpassClient,
			pollsDAO,
			channelEditorAuthorizer,
		)

		userID := "test-user-id"
		pollID := "test-poll-id"
		ownerID := "another-user-id"

		Convey("Errors when pollsDao errors", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, errors.New("test-error"))
			So(a.Authorize(context.Background(), userID, pollID), ShouldNotBeNil)
		})

		Convey("Errors when pollsDao returns nil", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, nil)
			So(a.Authorize(context.Background(), userID, pollID), ShouldNotBeNil)
		})

		Convey("Errors when pollsDao returns a moderated poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&models.Poll{
				PollID:  pollID,
				OwnedBy: ownerID,
				Status:  models.PollStatusModerated,
			}, nil)

			So(a.Authorize(context.Background(), userID, pollID), ShouldNotBeNil)
		})

		Convey("When pollsDao returns a poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&models.Poll{
				PollID:  pollID,
				OwnedBy: ownerID,
				Status:  models.PollStatusCompleted,
			}, nil)

			Convey("Errors when the channel editor authorizer errors", func() {
				channelEditorAuthorizer.On("Authorize", mock.Anything, userID, ownerID).Return(errors.New("test-error"))
				So(a.Authorize(context.Background(), userID, pollID), ShouldNotBeNil)
			})

			Convey("Succeeds when the channel editor authorizer succeeds", func() {
				channelEditorAuthorizer.On("Authorize", mock.Anything, userID, ownerID).Return(nil)
				So(a.Authorize(context.Background(), userID, pollID), ShouldBeNil)
			})
		})
	})
}
