package poll_editor

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/poliwag/backend/models"

	"code.justin.tv/cb/hallpass/client/hallpass"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
)

// Checks if the user is authorized to make edits for a given poll
// The user is authorized if:
// - They are editing a poll that is owned by a channel they are authorized to edit (see channel_editor_authorizer)
type Authorizer interface {
	Authorize(ctx context.Context, userID string, pollID string) error
}

type authorizer struct {
	hallpass                hallpass.Client
	pollsDAO                polls_dao.PollsDAO
	channelEditorAuthorizer channel_editor.Authorizer
}

func NewAuthorizer(
	hallpass hallpass.Client,
	pollsDAO polls_dao.PollsDAO,
	channelEditorAuthorizer channel_editor.Authorizer,
) Authorizer {
	return &authorizer{
		hallpass:                hallpass,
		pollsDAO:                pollsDAO,
		channelEditorAuthorizer: channelEditorAuthorizer,
	}
}

func (a *authorizer) Authorize(ctx context.Context, userID string, pollID string) error {
	// Get the poll record from dynamo
	poll, err := a.pollsDAO.GetPoll(ctx, pollID, false)
	if err != nil {
		msg := fmt.Sprintf("error getting poll with id %s", pollID)
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return errors.InternalError.Wrap(err, msg)
	}

	// Poll does not exist
	if poll == nil {
		return errors.NotFound.New("poll %s does not exist", pollID)
	}

	if poll.Status == models.PollStatusModerated {
		return errors.PermissionDenied.New("poll %s has status MODERATED", pollID)
	}

	return a.channelEditorAuthorizer.Authorize(ctx, userID, poll.OwnedBy)
}
