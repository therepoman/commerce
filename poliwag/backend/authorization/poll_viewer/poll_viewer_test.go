package poll_viewer_test

import (
	"context"
	"testing"

	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_viewer"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	poll_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestAuthorizer_Authorize(t *testing.T) {
	Convey("given a PollViewer authorizer", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		pollEditorAuthorizer := new(poll_editor_mock.Authorizer)

		authorizer := poll_viewer.NewAuthorizer(
			pollsDAO,
			pollEditorAuthorizer,
		)

		userID := "test-user-id"
		pollID := "test-poll-id"

		Convey("when get poll errors", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, errors.New("ERROR"))

			Convey("we should return an internal error", func() {
				err := authorizer.Authorize(context.Background(), userID, pollID)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
			})
		})

		Convey("when get poll returns no poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, nil)

			Convey("we should return an internal error", func() {
				err := authorizer.Authorize(context.Background(), userID, pollID)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.NotFound), ShouldBeTrue)
			})
		})

		Convey("when get poll returns a publicly viewable poll", func() {
			viewablePoll := models.Poll{
				PollID: pollID,
				Status: models.PollStatusActive,
			}
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&viewablePoll, nil)

			Convey("we should return no error", func() {
				err := authorizer.Authorize(context.Background(), userID, pollID)

				So(err, ShouldBeNil)
			})
		})

		Convey("when get poll returns a non-publicly viewable poll", func() {
			notViewablePoll := models.Poll{
				PollID: pollID,
				Status: models.PollStatusArchived,
			}
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&notViewablePoll, nil)

			Convey("when poll editor authorizer errors", func() {
				pollEditorAuthorizer.On("Authorize", mock.Anything, userID, pollID).Return(errors.New("ERROR"))

				Convey("we should return an error", func() {
					err := authorizer.Authorize(context.Background(), userID, pollID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when poll editor authorizer succeeds", func() {
				pollEditorAuthorizer.On("Authorize", mock.Anything, userID, pollID).Return(nil)

				Convey("we should return no error", func() {
					err := authorizer.Authorize(context.Background(), userID, pollID)

					So(err, ShouldBeNil)
				})
			})
		})
	})
}
