package poll_viewer

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

// Checks if the user is authorized to view the given poll
// The user is authorized if:
// - The poll is publicly viewable
// - They are viewing a poll they are allowed to edit (see poll_editor_authorizer)
type Authorizer interface {
	Authorize(ctx context.Context, userID string, pollID string) error
}

type authorizer struct {
	pollsDAO             polls_dao.PollsDAO
	pollEditorAuthorizer poll_editor.Authorizer
}

func NewAuthorizer(
	pollsDAO polls_dao.PollsDAO,
	pollEditorAuthorizer poll_editor.Authorizer,
) Authorizer {
	return &authorizer{
		pollsDAO:             pollsDAO,
		pollEditorAuthorizer: pollEditorAuthorizer,
	}
}

func (a *authorizer) Authorize(ctx context.Context, userID string, pollID string) error {
	// Get the poll record from dynamo
	poll, err := a.pollsDAO.GetPoll(ctx, pollID, false)
	if err != nil {
		msg := fmt.Sprintf("error getting poll with id %s", pollID)
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).WithError(err).Error(msg)
		return errors.InternalError.Wrap(err, msg)
	}

	// Poll does not exist
	if poll == nil {
		return errors.NotFound.New("poll %s does not exist", pollID)
	}

	isPollViewable := models.IsPollViewable(*poll, time.Now(), models.ViewableDuration)
	if isPollViewable {
		return nil
	}

	return a.pollEditorAuthorizer.Authorize(ctx, userID, pollID)
}
