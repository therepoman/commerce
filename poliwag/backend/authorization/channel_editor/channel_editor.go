package channel_editor

import (
	"context"
	"fmt"

	"code.justin.tv/cb/hallpass/client/hallpass"
	hallpass_view "code.justin.tv/cb/hallpass/view"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	users_models "code.justin.tv/web/users-service/models"
)

// Checks if the user is authorized to make edits for a given channel
// The user is authorized if:
// - They are editing their own channel
// - They are a mod in the channel according to zuma
// - They are an editor in the channel according to hallpass
// - They are staff according to users service
type Authorizer interface {
	Authorize(ctx context.Context, userID string, channelID string) error
}

type authorizer struct {
	zuma     zuma.Client
	hallpass hallpass.Client
	users    usersclient_internal.InternalClient
}

func NewAuthorizer(
	zuma zuma.Client,
	hallpass hallpass.Client,
	users usersclient_internal.InternalClient,
) Authorizer {
	return &authorizer{
		zuma:     zuma,
		hallpass: hallpass,
		users:    users,
	}
}

func (a *authorizer) Authorize(ctx context.Context, userID string, channelID string) error {
	// User is authorized to edit in their own channel
	if userID == channelID {
		return nil
	}

	// Check if the user is a channel mod by calling zuma
	var getModResp zuma_api.GetModResponse
	var reqError error

	err := hystrix.CallTwirpWithHystrix(hystrix.ZumaGetModCommand, hystrix.HystrixDo, func() error {
		getModResp, reqError = a.zuma.GetMod(ctx, channelID, userID, nil)
		return reqError
	})
	if err != nil {
		msg := fmt.Sprintf("error fetching mod status for user_id %s in channel_id %s", userID, channelID)
		logrus.WithFields(logrus.Fields{
			"channelID": channelID,
			"userID":    userID,
		}).WithError(err).Error(msg)
		return errors.InternalError.Wrap(err, msg)
	}

	// Channel mods are authorized to edit
	if getModResp.IsMod {
		return nil
	}

	// Check if the user is a channel editor by calling hallpass
	var isEditorResp *hallpass_view.GetIsEditorResponse
	err = hystrix.CallTwirpWithHystrix(hystrix.HallPassGetV1sEditorCommand, hystrix.HystrixDo, func() error {
		isEditorResp, reqError = a.hallpass.GetV1IsEditor(ctx, channelID, userID, nil)
		return reqError
	})

	if err != nil {
		msg := fmt.Sprintf("error fetching editor status for user_id %s in channel_id %s", userID, channelID)
		logrus.WithFields(logrus.Fields{
			"ownedBy": channelID,
			"userID":  userID,
		}).WithError(err).Error(msg)
		return errors.InternalError.Wrap(err, msg)
	}
	if isEditorResp == nil {
		msg := fmt.Sprintf("hallpass response was nil for user_id %s in channel_id %s", userID, channelID)
		logrus.WithFields(logrus.Fields{
			"ownedBy": channelID,
			"userID":  userID,
		}).WithError(err).Error(msg)
		return errors.InternalError.New(msg)
	}

	// Channel editors are authorized
	if isEditorResp.IsEditor {
		return nil
	}

	// Check if the user is staff by calling users service
	var getUserResp *users_models.Properties
	err = hystrix.CallTwirpWithHystrix(hystrix.UsersGetUserByIDCommand, hystrix.HystrixDo, func() error {
		getUserResp, reqError = a.users.GetUserByID(ctx, userID, nil)
		return reqError
	})

	// Check if the user is staff by calling users service
	if err != nil {
		msg := fmt.Sprintf("error fetching user status for user_id %s", userID)
		logrus.WithFields(logrus.Fields{
			"channelID": channelID,
			"userID":    userID,
		}).WithError(err).Error(msg)

		// If user service is down, failure scenario is to assume the user is not allowed to access polls
		return errors.PermissionDenied.New("user_id %s is not allowed to access polls on channel_id %s", userID, channelID)
	}
	if getUserResp == nil {
		msg := fmt.Sprintf("users service response was nil for user_id %s", userID)
		logrus.WithFields(logrus.Fields{
			"ownedBy": channelID,
			"userID":  userID,
		}).Error(msg)
		return errors.InternalError.New(msg)
	}

	// Staff users are authorized
	if getUserResp.Admin != nil && *getUserResp.Admin {
		return nil
	}

	return errors.PermissionDenied.New("user_id %s is not allowed to access polls on channel_id %s", userID, channelID)
}
