package channel_editor_test

import (
	"context"
	"errors"
	"testing"

	"github.com/joomcode/errorx"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	hallpass_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/cb/hallpass/client/hallpass"
	zuma_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/zuma/client"
	users_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	users_models "code.justin.tv/web/users-service/models"
)

func TestAuthorizer_Authorize(t *testing.T) {
	Convey("Given an authorizer", t, func() {
		zumaClient := new(zuma_client_mock.Client)
		hallpassClient := new(hallpass_client_mock.Client)
		usersClient := new(users_client_mock.InternalClient)

		a := channel_editor.NewAuthorizer(zumaClient, hallpassClient, usersClient)

		userID := "test-user-id"
		channelID := "another-user-id"

		Convey("Succeeds early when the channelID and userID are the same", func() {
			So(a.Authorize(context.Background(), userID, userID), ShouldBeNil)
			So(zumaClient.Calls, ShouldHaveLength, 0)
			So(hallpassClient.Calls, ShouldHaveLength, 0)
			So(usersClient.Calls, ShouldHaveLength, 0)
		})

		Convey("Errors when the zuma call errors", func() {
			zumaClient.On("GetMod", mock.Anything, channelID, userID, mock.Anything).Return(api.GetModResponse{}, errors.New("test-error"))

			err := a.Authorize(context.Background(), userID, channelID)

			So(err, ShouldNotBeNil)
			So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
			So(hallpassClient.Calls, ShouldHaveLength, 0)
			So(usersClient.Calls, ShouldHaveLength, 0)
		})

		Convey("Succeeds early when zuma says the user is a mod", func() {
			zumaClient.On("GetMod", mock.Anything, channelID, userID, mock.Anything).Return(api.GetModResponse{
				IsMod: true,
			}, nil)
			So(a.Authorize(context.Background(), userID, channelID), ShouldBeNil)
			So(hallpassClient.Calls, ShouldHaveLength, 0)
			So(usersClient.Calls, ShouldHaveLength, 0)
		})

		Convey("When zuma says the user is not a mod", func() {
			zumaClient.On("GetMod", mock.Anything, channelID, userID, mock.Anything).Return(api.GetModResponse{
				IsMod: false,
			}, nil)

			Convey("Errors when hallpass errors", func() {
				hallpassClient.On("GetV1IsEditor", mock.Anything, channelID, userID, mock.Anything).Return(nil, errors.New("test-error"))

				err := a.Authorize(context.Background(), userID, channelID)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
				So(usersClient.Calls, ShouldHaveLength, 0)
			})

			Convey("Errors when hallpass returns nil", func() {
				hallpassClient.On("GetV1IsEditor", mock.Anything, channelID, userID, mock.Anything).Return(nil, nil)
				err := a.Authorize(context.Background(), userID, channelID)

				So(err, ShouldNotBeNil)
				So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
				So(usersClient.Calls, ShouldHaveLength, 0)
			})

			Convey("Succeeds when hallpass says the user is an editor", func() {
				hallpassClient.On("GetV1IsEditor", mock.Anything, channelID, userID, mock.Anything).Return(&view.GetIsEditorResponse{
					IsEditor: true,
				}, nil)
				So(a.Authorize(context.Background(), userID, channelID), ShouldBeNil)
				So(usersClient.Calls, ShouldHaveLength, 0)
			})

			Convey("When hallpass says the user is not an editor", func() {
				hallpassClient.On("GetV1IsEditor", mock.Anything, channelID, userID, mock.Anything).Return(&view.GetIsEditorResponse{
					IsEditor: false,
				}, nil)

				Convey("Errors when users service call errors", func() {
					usersClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(nil, errors.New("test-error"))
					err := a.Authorize(context.Background(), userID, channelID)

					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.PermissionDenied), ShouldBeTrue)
				})

				Convey("Errors when users service returns nil", func() {
					usersClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(nil, nil)
					err := a.Authorize(context.Background(), userID, channelID)

					So(err, ShouldNotBeNil)
					So(errorx.IsOfType(err, poliwag_errors.InternalError), ShouldBeTrue)
				})

				Convey("Succeeds when users service says user is staff", func() {
					usersClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(&users_models.Properties{
						Admin: pointers.BoolP(true),
					}, nil)
					So(a.Authorize(context.Background(), userID, channelID), ShouldBeNil)
				})
			})
		})
	})
}
