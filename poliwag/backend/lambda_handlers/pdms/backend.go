package pdms

import (
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-playground/validator/v10"

	pdms "code.justin.tv/amzn/PDMSLambdaTwirp"
	twirplambdatransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	metricsmiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	"code.justin.tv/chat/httptelemetry"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/delete_user_leaderboard_entries"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/delete_user_votes"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/report_deletion"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/start_user_deletion"
	"code.justin.tv/commerce/poliwag/clients/aurora"
	"code.justin.tv/commerce/poliwag/clients/aurora/gorm"
	"code.justin.tv/commerce/poliwag/clients/sandstorm"
	stepfn_client "code.justin.tv/commerce/poliwag/clients/stepfn"
	"code.justin.tv/commerce/poliwag/config"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/twirpmetric"
)

const (
	cloudwatchSubstage = "primary"
	serviceName        = "poliwag"
)

type LambdaHandlers struct {
	StartUserDeletion        *start_user_deletion.Handler                   `validate:"required"`
	DeleteLeaderboardEntries *delete_user_leaderboard_entries.LambdaHandler `validate:"required"`
	DeleteUserVotes          *delete_user_votes.LambdaHandler               `validate:"required"`
	ReportDeletion           *report_deletion.LambdaHandler                 `validate:"required"`
}

type Backend interface {
	GetLambdaHandlers() *LambdaHandlers
}

type backend struct {
	lambdaHandlers *LambdaHandlers `validate:"required"`
}

func NewBackend(cfg *config.Config, sampleReporter *telemetry.SampleReporter) (Backend, error) {
	statters := make([]statsd.Statter, 0)

	if cfg.CloudWatchMetricsEnabled {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.AWSRegion,
			ServiceName:       serviceName,
			Stage:             cfg.EnvironmentName,
			Substage:          cloudwatchSubstage,
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	// dedicating a session for just PDMS client
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: 10 * time.Second},
		},
	}))
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	creds := stscreds.NewCredentials(sess, cfg.PDMS.CallerRole)
	pdmsLambdaCli := lambda.New(sess, &aws.Config{Credentials: creds})
	pdmsTransport := twirplambdatransport.NewClient(pdmsLambdaCli, cfg.PDMS.LambdaARN)
	pdmsClient := pdms.NewPDMSServiceProtobufClient("", pdmsTransport)

	sandstormClient, err := sandstorm.New(cfg)
	if err != nil {
		return nil, err
	}

	auroraCfg := aurora.Config{
		Username:                cfg.Aurora.Username,
		PasswordSandstormSecret: cfg.Aurora.PasswordSandstormSecret,
		Endpoint:                cfg.Aurora.Endpoint,
		DBName:                  cfg.Aurora.DBName,
	}

	gormClient, err := gorm.NewClient(auroraCfg, sandstormClient)
	if err != nil {
		return nil, err
	}

	b := &backend{
		lambdaHandlers: &LambdaHandlers{
			StartUserDeletion: &start_user_deletion.Handler{
				PDMS:      pdmsClient,
				SFNClient: stepfn_client.NewClient(cfg, stats, sampleReporter, httpClient(sampleReporter)),
				Config:    cfg,
			},
			DeleteLeaderboardEntries: &delete_user_leaderboard_entries.LambdaHandler{
				Pantheon: pantheonrpc.NewPantheonProtobufClient(cfg.Endpoints.Pantheon, httpClientTwirp(sampleReporter)),
			},
			DeleteUserVotes: &delete_user_votes.LambdaHandler{
				VotesDAO: votes.NewDAO(gormClient),
			},
			ReportDeletion: &report_deletion.LambdaHandler{
				PDMS: pdmsClient,
			},
		},
	}
	if err := validator.New().Struct(b); err != nil {
		return nil, err
	}

	return b, nil
}

func (b *backend) GetLambdaHandlers() *LambdaHandlers {
	return b.lambdaHandlers
}

func httpClient(sampleReporter *telemetry.SampleReporter) *http.Client {
	return &http.Client{
		Transport: &httptelemetry.RoundTripper{
			SampleReporter: sampleReporter,
		},
	}
}

func httpClientTwirp(sampleReporter *telemetry.SampleReporter) *http.Client {
	return twitchclient.NewHTTPClient(twitchclient.ClientConf{
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			func(rt http.RoundTripper) http.RoundTripper {
				metricsOpMonitor := &metricsmiddleware.OperationMonitor{
					SampleReporter: *sampleReporter,
					AutoFlush:      false,
				}
				opStarter := &operation.Starter{
					OpMonitors: []operation.OpMonitor{
						metricsOpMonitor,
					},
				}
				return &twirpmetric.Client{
					Starter:   opStarter,
					Transport: rt,
				}
			},
		},
	})
}
