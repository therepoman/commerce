package delete_user_votes

import (
	"context"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/validation"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type LambdaHandler struct {
	VotesDAO votes.DAO `validate:"required"`
}

func (h *LambdaHandler) Handle(ctx context.Context, input *sfn_models.DeleteUserVotesRequest) (*sfn_models.DeleteUserVotesResponse, error) {
	err := validation.IsValid(input)
	if err != nil {
		return nil, err
	}
	if input.IsDryRun {
		logrus.Info("Dry Run, not deleting entries in Votes DB")
	} else {
		err := h.VotesDAO.DeleteVotesForUsers(ctx, input.UserIDs)
		if err != nil {
			return nil, errors.Wrap(err, "failed to delete user vote entries from Votes DB")
		}
	}

	return &sfn_models.DeleteUserVotesResponse{
		UserDeletionResponseData: sfn_models.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
