package delete_user_votes

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	votes_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/votes"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a PDMS delete user votes handler", t, func() {
		votesDAO := new(votes_dao_mock.DAO)
		handler := &LambdaHandler{
			VotesDAO: votesDAO,
		}

		ctx := context.Background()
		userIDs := []string{"123123123"}

		Convey("returns error", func() {
			var input *sfn_models.DeleteUserVotesRequest

			Convey("when the input is nil", func() {
				input = nil
			})

			Convey("when the input contains no user IDs", func() {
				input = &sfn_models.DeleteUserVotesRequest{}
			})

			Convey("when we get an error from DAO", func() {
				input = &sfn_models.DeleteUserVotesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				votesDAO.On("DeleteVotesForUsers", ctx, userIDs).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := handler.Handle(ctx, input)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *sfn_models.DeleteUserVotesRequest
			var numberOfCalls int

			Convey("when we successfully call DAO with valid input", func() {
				input = &sfn_models.DeleteUserVotesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				votesDAO.On("DeleteVotesForUsers", ctx, userIDs).Return(nil)
				numberOfCalls = 1
			})

			Convey("when it is a dry run", func() {
				input = &sfn_models.DeleteUserVotesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs:  userIDs,
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			resp, err := handler.Handle(ctx, input)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, userIDs)
			votesDAO.AssertNumberOfCalls(t, "DeleteVotesForUsers", numberOfCalls)
		})
	})
}
