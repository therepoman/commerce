package delete_user_leaderboard_entries

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/validation"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type LambdaHandler struct {
	Pantheon pantheonrpc.Pantheon `validate:"required"`
}

func (h *LambdaHandler) Handle(ctx context.Context, input *sfn_models.DeleteUserLeaderboardEntriesRequest) (*sfn_models.DeleteUserLeaderboardEntriesResponse, error) {
	err := validation.IsValid(input)
	if err != nil {
		return nil, err
	}

	if input.IsDryRun {
		logrus.Info("Dry Run, not deleting entries in Pantheon")
	} else {
		for _, userID := range input.UserIDs {
			_, err := h.Pantheon.DeleteEntries(ctx, &pantheonrpc.DeleteEntriesReq{
				Domain:   models.PollsDomain,
				EntryKey: userID,
			})
			if err != nil {
				return nil, errors.Wrap(err, fmt.Sprintf("failed to call Pantheon to delete entries for %s", userID))
			}
		}
	}

	return &sfn_models.DeleteUserLeaderboardEntriesResponse{
		UserDeletionResponseData: sfn_models.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
