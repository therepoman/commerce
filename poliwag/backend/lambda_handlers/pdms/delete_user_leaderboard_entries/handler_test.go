package delete_user_leaderboard_entries

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	pantheonrpc_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a PDMS delete user leaderboard entries handler", t, func() {
		pantheon := new(pantheonrpc_mock.Pantheon)
		handler := &LambdaHandler{
			Pantheon: pantheon,
		}

		ctx := context.Background()
		userID := "123123123"

		Convey("returns error", func() {
			var input *sfn_models.DeleteUserLeaderboardEntriesRequest

			Convey("when the input is nil", func() {
				input = nil
			})

			Convey("when the input contains no user ID", func() {
				input = &sfn_models.DeleteUserLeaderboardEntriesRequest{}
			})

			Convey("when we get an error from Pantheon", func() {
				input = &sfn_models.DeleteUserLeaderboardEntriesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: []string{userID},
					},
				}
				pantheon.On("DeleteEntries", ctx, &pantheonrpc.DeleteEntriesReq{
					Domain:   models.PollsDomain,
					EntryKey: userID,
				}).Return(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := handler.Handle(ctx, input)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *sfn_models.DeleteUserLeaderboardEntriesRequest
			var numberOfCalls int

			Convey("when we successfully call Pantheon with valid input", func() {
				input = &sfn_models.DeleteUserLeaderboardEntriesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: []string{userID},
					},
				}
				pantheon.On("DeleteEntries", ctx, &pantheonrpc.DeleteEntriesReq{
					Domain:   models.PollsDomain,
					EntryKey: userID,
				}).Return(&pantheonrpc.DeleteEntriesResp{}, nil)
				numberOfCalls = 1
			})

			Convey("when it is a dry run", func() {
				input = &sfn_models.DeleteUserLeaderboardEntriesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs:  []string{userID},
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			resp, err := handler.Handle(ctx, input)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, []string{userID})
			pantheon.AssertNumberOfCalls(t, "DeleteEntries", numberOfCalls)
		})
	})
}
