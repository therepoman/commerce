package validation

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

func TestIsValid(t *testing.T) {
	Convey("given a validation function for PDMS step function input", t, func() {
		Convey("returns error when", func() {
			// a struct that implements the UserDeletionRequest interface
			var input *sfn_models.DeleteUserVotesRequest

			Convey("input is nil", func() {
				input = nil
			})
			Convey("list of user IDs is empty", func() {
				input = &sfn_models.DeleteUserVotesRequest{}
			})
			Convey("list of user IDs contains empty IDs", func() {
				input = &sfn_models.DeleteUserVotesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: []string{"123123123", ""},
					},
				}
			})

			err := IsValid(input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when", func() {
			var input *sfn_models.DeleteUserVotesRequest

			Convey("input is valid", func() {
				input = &sfn_models.DeleteUserVotesRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: []string{"123123123", "456456456"},
					},
				}
			})

			err := IsValid(input)
			So(err, ShouldBeNil)
		})
	})
}
