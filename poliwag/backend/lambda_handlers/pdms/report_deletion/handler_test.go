package report_deletion

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/config"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	pdms_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/amzn/PDMSLambdaTwirp"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a PDMS delete user leaderboard entries handler", t, func() {
		pdmsClient := new(pdms_mock.PDMSService)
		handler := &LambdaHandler{
			PDMS: pdmsClient,
		}

		ctx := context.Background()
		userID := "123123123"

		Convey("returns error", func() {
			var input *sfn_models.ReportDeletionRequest

			Convey("when the input is nil", func() {
				input = nil
			})

			Convey("when the input contains no user ID", func() {
				input = &sfn_models.ReportDeletionRequest{}
			})

			Convey("when we get an error from PDMS when reporting deletion is enabled", func() {
				input = &sfn_models.ReportDeletionRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.On("ReportDeletion", ctx, mock.MatchedBy(func(req *pdms_service.ReportDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == config.PoliwagServiceCatalogID
				})).Return(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := handler.Handle(ctx, input)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *sfn_models.ReportDeletionRequest
			var numberOfCalls int

			Convey("when we successfully call Pantheon with valid input", func() {
				input = &sfn_models.ReportDeletionRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.On("ReportDeletion", ctx, mock.MatchedBy(func(req *pdms_service.ReportDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == config.PoliwagServiceCatalogID
				})).Return(&pdms_service.ReportDeletionPayload{}, nil)
				numberOfCalls = 1
			})

			Convey("when it is a dry run", func() {
				input = &sfn_models.ReportDeletionRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs:  []string{userID},
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when reporting deletion is disabled and it's not a dry run", func() {
				input = &sfn_models.ReportDeletionRequest{
					UserDeletionRequestData: sfn_models.UserDeletionRequestData{
						UserIDs: []string{userID},
					},
				}
				numberOfCalls = 0
			})

			resp, err := handler.Handle(ctx, input)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, []string{userID})
			pdmsClient.AssertNumberOfCalls(t, "ReportDeletion", numberOfCalls)
		})
	})
}
