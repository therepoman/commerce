package report_deletion

import (
	"context"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/config"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/pdms/validation"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type LambdaHandler struct {
	PDMS pdms_service.PDMSService `validate:"required"`
}

func (h *LambdaHandler) Handle(ctx context.Context, input *sfn_models.ReportDeletionRequest) (*sfn_models.ReportDeletionResponse, error) {
	err := validation.IsValid(input)
	if err != nil {
		return nil, err
	}

	timeOfDeletion := time.Now()

	if input.IsDryRun {
		logrus.Info("Dry Run, not reporting deletion to PDMS")
	} else if !input.ReportDeletion {
		logrus.Info("not reporting deletion to PDMS due to input has it disabled")
	} else {
		timeOfDeletionTwirp, err := ptypes.TimestampProto(timeOfDeletion)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert timestamp of deletion to ptypes timestamp")
		}

		for _, userID := range input.UserIDs {
			_, err = h.PDMS.ReportDeletion(ctx, &pdms_service.ReportDeletionRequest{
				UserId:    userID,
				ServiceId: config.PoliwagServiceCatalogID,
				Timestamp: timeOfDeletionTwirp,
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to report deletion to PDMS")
			}
		}
	}

	return &sfn_models.ReportDeletionResponse{
		UserDeletionResponseData: sfn_models.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
		Timestamp: timeOfDeletion,
	}, nil
}
