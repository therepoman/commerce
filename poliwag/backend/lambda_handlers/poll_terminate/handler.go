package poll_terminate

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

var forbiddenStatuses = map[models.PollStatus]interface{}{
	models.PollStatusModerated: nil,
	models.PollStatusArchived:  nil,
}

type LambdaHandler struct {
	PollsDAO           polls.PollsDAO      `validate:"required"`
	PubsubController   pubsub.Controller   `validate:"required"`
	EventbusController eventbus.Controller `validate:"required"`
}

func (l *LambdaHandler) Handle(ctx context.Context, input sfn_models.PollTerminateRequest) (*sfn_models.PollTerminateResponse, error) {
	poll, err := l.PollsDAO.GetPoll(ctx, input.PollID, true)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting poll from dynamo (poll terminate lambda)")
		return nil, err
	}

	if poll == nil {
		logrus.WithField("poll_id", input.PollID).Error("poll not found (poll terminate lambda)")
		return nil, errors.New("poll not found")
	}

	_, forbidden := forbiddenStatuses[poll.Status]
	if forbidden {
		logrus.WithField("poll_id", input.PollID).Infof("%s poll was archived or moderated in another flow (poll terminate lambda)", poll.Status)
		return &sfn_models.PollTerminateResponse{
			PollID: input.PollID,
		}, nil
	}

	updatedPoll := *poll
	if poll.Status == models.PollStatusTerminated {
		logrus.WithField("poll_id", input.PollID).Info("poll already marked terminated in dynamo (poll terminate lambda)")
		// we don't need to update dynamo (this was done in the API), but we still need to send the terminate pubsub
	} else {
		newStatus := models.PollStatusTerminated
		updatedPoll, err = l.PollsDAO.UpdatePoll(ctx, input.PollID, polls.PollUpdateArgs{
			Status: &newStatus,
		})
		if err != nil {
			logrus.WithField("poll_id", input.PollID).WithError(err).Errorf("error setting poll status to %s (poll terminate lambda)", newStatus)
			return nil, err
		}
	}

	err = l.PubsubController.SendPollTerminatePubsub(ctx, input.PollID)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error publishing poll terminate pubsub (poll terminate lambda)")
		return nil, err
	}

	l.EventbusController.PublishPollUpdate(ctx, updatedPoll, true)

	return &sfn_models.PollTerminateResponse{
		PollID: input.PollID,
	}, nil
}
