package poll_moderate

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

var forbiddenStatuses = map[models.PollStatus]interface{}{
	models.PollStatusArchived: nil,
}

type LambdaHandler struct {
	PollsDAO           polls.PollsDAO      `validate:"required"`
	PubsubController   pubsub.Controller   `validate:"required"`
	EventbusController eventbus.Controller `validate:"required"`
}

func (l *LambdaHandler) Handle(ctx context.Context, input sfn_models.PollModerateRequest) error {
	poll, err := l.PollsDAO.GetPoll(ctx, input.PollID, true)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting poll from dynamo (poll moderate lambda)")
		return err
	}

	if poll == nil {
		logrus.WithField("poll_id", input.PollID).Error("poll not found (poll moderate lambda)")
		return errors.New("poll not found")
	}

	_, forbidden := forbiddenStatuses[poll.Status]
	if forbidden {
		logrus.WithField("poll_id", input.PollID).WithError(err).Errorf("%s poll cannot be moderated (poll moderate lambda)", poll.Status)
		return errors.New("poll cannot be moderated")
	}

	updatedPoll := *poll
	if poll.Status == models.PollStatusModerated {
		logrus.WithField("poll_id", input.PollID).Info("poll already marked moderated in dynamo (poll moderate lambda)")
		// we don't need to update dynamo (this was done in the API), but we still need to send the moderate pubsub
	} else {
		newStatus := models.PollStatusModerated
		updatedPoll, err = l.PollsDAO.UpdatePoll(ctx, input.PollID, polls.PollUpdateArgs{
			Status: &newStatus,
		})
		if err != nil {
			logrus.WithField("poll_id", input.PollID).WithError(err).Errorf("error setting poll status to %s (poll moderate lambda)", newStatus)
			return err
		}
	}

	err = l.PubsubController.SendPollModeratePubsub(ctx, input.PollID)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error publishing poll moderate pubsub (poll moderate lambda)")
		return err
	}

	l.EventbusController.PublishPollUpdate(ctx, updatedPoll, true)

	return nil
}
