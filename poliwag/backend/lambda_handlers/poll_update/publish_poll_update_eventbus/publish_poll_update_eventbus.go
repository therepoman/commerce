package publish_poll_update_eventbus

import (
	"context"

	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type Step interface {
	PublishPollUpdate(ctx context.Context, input Input)
}

type step struct {
	controller eventbus.Controller
}

func NewStep(controller eventbus.Controller) Step {
	return &step{
		controller: controller,
	}
}

type Input struct {
	Poll models.Poll
}

func (s step) PublishPollUpdate(ctx context.Context, input Input) {
	s.controller.PublishPollUpdate(ctx, input.Poll, false)
}
