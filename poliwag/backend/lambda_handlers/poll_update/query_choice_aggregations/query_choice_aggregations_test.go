package query_choice_aggregations_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations"
	votes_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/votes"
)

func TestActivity_QueryChoiceAggregations(t *testing.T) {
	Convey("given a QueryChoiceAggregations activity", t, func() {
		votesDAO := new(votes_dao_mock.DAO)

		step := query_choice_aggregations.NewStep(votesDAO)

		Convey("when GetChoiceAggregations errors", func() {
			pollID := "test-poll-id"

			votesDAO.On("GetChoiceAggregations", mock.Anything, pollID).Return(nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, err := step.QueryChoiceAggregations(context.Background(), query_choice_aggregations.Input{
					PollID: pollID,
				})

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetChoiceAggregations returns aggregations", func() {
			pollID := "test-poll-id"
			aggregations := []votes.ChoiceAggregation{
				{
					ChoiceID: "choice-id-0",
				},
				{
					ChoiceID: "choice-id-1",
				},
			}

			votesDAO.On("GetChoiceAggregations", mock.Anything, pollID).Return(aggregations, nil)

			Convey("we should return output", func() {
				actualOutput, err := step.QueryChoiceAggregations(context.Background(), query_choice_aggregations.Input{
					PollID: pollID,
				})

				So(err, ShouldBeNil)
				So(actualOutput.ChoiceAggregations, ShouldResemble, aggregations)
			})
		})
	})
}
