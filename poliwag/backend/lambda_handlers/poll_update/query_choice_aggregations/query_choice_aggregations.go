package query_choice_aggregations

import (
	"context"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
)

type Step interface {
	QueryChoiceAggregations(ctx context.Context, input Input) (*Output, error)
}

type step struct {
	votesDAO votes.DAO
}

func NewStep(votesDAO votes.DAO) Step {
	return &step{
		votesDAO: votesDAO,
	}
}

type Input struct {
	PollID string
}

type Output struct {
	ChoiceAggregations []votes.ChoiceAggregation
}

func (s *step) QueryChoiceAggregations(ctx context.Context, input Input) (*Output, error) {
	choiceAggregations, err := s.votesDAO.GetChoiceAggregations(ctx, input.PollID)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting choice aggregations")
		return nil, err
	}

	return &Output{
		ChoiceAggregations: choiceAggregations,
	}, nil
}
