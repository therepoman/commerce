package poll_update

import (
	"context"
	"sync"
	"time"

	"github.com/joomcode/errorx"
	"github.com/sirupsen/logrus"

	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/publish_poll_update_eventbus"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_poll_aggregation"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_choice_aggregations"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_poll_aggregation"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/send_poll_update_pubsub"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

const (
	timeout = 10 * time.Second
)

type LambdaHandler struct {
	GetPoll                   get_poll.Step                     `validate:"required"`
	QueryChoiceAggregations   query_choice_aggregations.Step    `validate:"required"`
	QueryPollAggregation      query_poll_aggregation.Step       `validate:"required"`
	UpdateIsAggregationsEqual update_is_aggregations_equal.Step `validate:"required"`
	SaveChoiceAggregations    save_choice_aggregations.Step     `validate:"required"`
	SavePollAggregation       save_poll_aggregation.Step        `validate:"required"`
	SendPollUpdatePubsub      send_poll_update_pubsub.Step      `validate:"required"`
	PublishPollUpdateEventbus publish_poll_update_eventbus.Step `validate:"required"`
}

func (l *LambdaHandler) Handle(ctx context.Context, input sfn_models.PollUpdateRequest) (*sfn_models.PollUpdateResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	nextRunTime := time.Now().Add(2 * time.Second)

	lifecycleState := input.LifecycleState

	getPollOut, err := l.GetPoll.GetPoll(ctx, get_poll.Input{
		PollID:         input.PollID,
		LifecycleState: input.LifecycleState,
	})
	if err != nil {
		if errorx.IsOfType(err, poliwag_errors.NotFound) {
			// Treat poll not found as a critical error and terminate the step function execution
			logrus.WithField("poll_id", input.PollID).WithError(err).Error("poll not found - terminating stepfunction (poll update lambda)")
			return nil, err
		} else {
			// Stop the current poll_update lambda, but don't terminate the step function execution
			logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting poll - stopping lambda, NOT terminating stepfunction (poll update lambda)")
			return makeResponse(input.PollID, lifecycleState, nextRunTime), nil
		}
	}

	lifecycleState = getPollOut.LifecycleState

	if lifecycleState.Poll.PollID != "" && lifecycleState.Poll.Status != models.PollStatusActive {
		// Poll has ended, stop the lambda with the done flag
		return makeResponse(input.PollID, lifecycleState, nextRunTime), nil
	}

	wg := new(sync.WaitGroup)
	wg.Add(2)

	var queryPollAggOut *query_poll_aggregation.Output
	go func() {
		queryPollAggOut, err = l.QueryPollAggregation.QueryPollAggregation(ctx, query_poll_aggregation.Input{
			PollID: input.PollID,
		})
		if err != nil {
			logrus.WithField("poll_id", input.PollID).WithError(err).Error("error querying poll aggregations (poll update lambda)")
		} else {
			savePollAggOut, err := l.SavePollAggregation.SavePollAggregation(ctx, save_poll_aggregation.Input{
				PollID:          input.PollID,
				PollAggregation: queryPollAggOut.PollAggregation,
				LifecycleState:  lifecycleState,
			})
			if err != nil {
				logrus.WithField("poll_id", input.PollID).WithError(err).Error("error saving poll aggregations (poll update lambda)")
			} else {
				lifecycleState = savePollAggOut.LifecycleState
			}
		}
		wg.Done()
	}()

	var queryChoiceAggOut *query_choice_aggregations.Output
	go func() {
		queryChoiceAggOut, err = l.QueryChoiceAggregations.QueryChoiceAggregations(ctx, query_choice_aggregations.Input{
			PollID: input.PollID,
		})
		if err != nil {
			logrus.WithField("poll_id", input.PollID).WithError(err).Error("error querying choice aggregations (poll update lambda)")
		} else {
			err := l.SaveChoiceAggregations.SaveChoiceAggregations(ctx, save_choice_aggregations.Input{
				ChoiceAggregations: queryChoiceAggOut.ChoiceAggregations,
				LifecycleState:     lifecycleState,
			})
			if err != nil {
				logrus.WithField("poll_id", input.PollID).WithError(err).Error("error saving poll aggregations (poll update lambda)")
			}
		}
		wg.Done()
	}()

	doneChan := make(chan bool)
	go func() {
		wg.Wait()
		doneChan <- true
	}()

	select {
	case _ = <-ctx.Done():
		// Stop the current poll_update lambda, but don't terminate the step function execution
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("timed out waiting for query/save aggregation go routines to complete (poll update lambda)")
	case _ = <-doneChan:
	}

	if queryPollAggOut != nil && queryChoiceAggOut != nil {
		// If both queries responded, check for new changes and if so send pubsub msg

		hasPreviousPollAgg := lifecycleState.PrevPollAggregation.PollID != ""
		updateIsAggEqualOut, err := l.UpdateIsAggregationsEqual.UpdateIsAggregationsEqual(ctx, update_is_aggregations_equal.Input{
			PollAggregation:    queryPollAggOut.PollAggregation,
			ChoiceAggregations: queryChoiceAggOut.ChoiceAggregations,
			LifecycleState:     lifecycleState,
		})
		if err != nil {
			// Stop the current poll_update lambda, but don't terminate the step function execution
			logrus.WithField("poll_id", input.PollID).WithError(err).Error("error saving poll aggregations (poll update lambda)")
			return makeResponse(input.PollID, lifecycleState, nextRunTime), nil
		} else {
			lifecycleState = updateIsAggEqualOut.LifecycleState

			if hasPreviousPollAgg && !updateIsAggEqualOut.IsAggregationsEqual {
				err := l.SendPollUpdatePubsub.SendPollUpdatePubsub(ctx, send_poll_update_pubsub.Input{
					PollID: input.PollID,
				})
				if err != nil {
					logrus.WithField("poll_id", input.PollID).WithError(err).Error("error sending poll update pubsub (poll update lambda)")
				}

				l.PublishPollUpdateEventbus.PublishPollUpdate(ctx, publish_poll_update_eventbus.Input{
					Poll: lifecycleState.Poll,
				})
			}
		}
	}

	return makeResponse(input.PollID, lifecycleState, nextRunTime), nil
}

func makeResponse(pollID string, lifecycleState sfn_models.LifecycleState, nextRunTime time.Time) *sfn_models.PollUpdateResponse {
	scheduledEndTime := lifecycleState.Poll.StartTime.Add(lifecycleState.Poll.Duration)
	return &sfn_models.PollUpdateResponse{
		PollID:         pollID,
		LifecycleState: lifecycleState,
		Done:           lifecycleState.Poll.PollID != "" && (lifecycleState.Poll.Status != models.PollStatusActive || time.Now().After(scheduledEndTime)), // Stops the step function when true
		WaitUntil:      nextRunTime.Format(time.RFC3339),
	}
}
