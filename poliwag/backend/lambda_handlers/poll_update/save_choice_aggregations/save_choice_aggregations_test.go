package save_choice_aggregations_test

import (
	"context"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_choice_aggregations"
	"code.justin.tv/commerce/poliwag/backend/models"
	step_fn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	choices_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/choices"
)

func TestActivity_SaveChoiceAggregations(t *testing.T) {
	Convey("given a SaveChoiceAggregations activity", t, func() {
		choicesDAO := new(choices_dao_mock.ChoicesDAO)

		step := save_choice_aggregations.NewStep(choicesDAO)

		Convey("when there are no choice aggregations", func() {
			poll := models.Poll{
				PollID: "test-poll-id",
			}
			choiceAggregations := make([]votes.ChoiceAggregation, 0)

			Convey("we should return an output", func() {
				err := step.SaveChoiceAggregations(context.Background(), save_choice_aggregations.Input{
					ChoiceAggregations: choiceAggregations,
					LifecycleState: step_fn_models.LifecycleState{
						Poll: poll,
					},
				})
				So(err, ShouldBeNil)
			})
		})
	})
}
