package save_choice_aggregations

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/dao/choices"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type Step interface {
	SaveChoiceAggregations(ctx context.Context, input Input) error
}

type step struct {
	choicesDAO choices.ChoicesDAO
}

func NewStep(choicesDAO choices.ChoicesDAO) Step {
	return &step{
		choicesDAO: choicesDAO,
	}
}

type Input struct {
	ChoiceAggregations []votes.ChoiceAggregation
	LifecycleState     models.LifecycleState
}

func (s step) SaveChoiceAggregations(ctx context.Context, input Input) error {
	// No votes have come in yet, no reason to perform updates
	if len(input.ChoiceAggregations) == 0 {
		return nil
	}

	var updateArgs []choices.UpdateChoiceArgs
	for _, a := range input.ChoiceAggregations {
		args := choices.UpdateChoiceArgs{
			ChoiceID:           a.ChoiceID,
			TotalVoters:        pointers.IntP(a.TotalVoters),
			TotalVotes:         pointers.IntP(a.TotalVotes),
			TotalBaseVotes:     pointers.IntP(a.TotalBaseVotes),
			TotalBits:          pointers.IntP(a.TotalBits),
			TotalChannelPoints: pointers.IntP(a.TotalChannelPoints),
		}

		updateArgs = append(updateArgs, args)
	}

	err := s.choicesDAO.UpdateChoices(ctx, updateArgs)
	if err != nil {
		return err
	}

	return nil
}
