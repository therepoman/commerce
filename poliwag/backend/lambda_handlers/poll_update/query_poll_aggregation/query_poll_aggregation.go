package query_poll_aggregation

import (
	"context"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
)

type Step interface {
	QueryPollAggregation(ctx context.Context, input Input) (*Output, error)
}

type step struct {
	votesDAO votes.DAO
}

func NewStep(votesDAO votes.DAO) Step {
	return &step{
		votesDAO: votesDAO,
	}
}

type Input struct {
	PollID string
}

type Output struct {
	PollAggregation votes.PollAggregation
}

func (s step) QueryPollAggregation(ctx context.Context, input Input) (*Output, error) {
	aggregation, err := s.votesDAO.GetPollAggregation(ctx, input.PollID)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error calling GetPollAggregation")
		return nil, err
	}

	return &Output{
		PollAggregation: aggregation,
	}, nil
}
