package poll_update

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/joomcode/errorx"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	poliwag_errors "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/publish_poll_update_eventbus"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_poll_aggregation"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_poll_aggregation"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll"
	publish_poll_update_eventbus_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/publish_poll_update_eventbus"
	query_choice_aggregations_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_choice_aggregations"
	query_poll_aggregation_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/query_poll_aggregation"
	save_choice_aggregations_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_choice_aggregations"
	save_poll_aggregation_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/save_poll_aggregation"
	send_poll_update_pubsub_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/send_poll_update_pubsub"
	update_is_aggregations_equal_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a handler", t, func() {
		getPoll := new(get_poll_mock.Step)
		queryChoiceAggregations := new(query_choice_aggregations_mock.Step)
		queryPollAggregation := new(query_poll_aggregation_mock.Step)
		updateIsAggregationsEqual := new(update_is_aggregations_equal_mock.Step)
		saveChoiceAggregations := new(save_choice_aggregations_mock.Step)
		savePollAggregation := new(save_poll_aggregation_mock.Step)
		sendPollUpdatePubsub := new(send_poll_update_pubsub_mock.Step)
		publishPollUpdateEventbus := new(publish_poll_update_eventbus_mock.Step)

		lh := &LambdaHandler{
			GetPoll:                   getPoll,
			QueryChoiceAggregations:   queryChoiceAggregations,
			QueryPollAggregation:      queryPollAggregation,
			UpdateIsAggregationsEqual: updateIsAggregationsEqual,
			SaveChoiceAggregations:    saveChoiceAggregations,
			SavePollAggregation:       savePollAggregation,
			SendPollUpdatePubsub:      sendPollUpdatePubsub,
			PublishPollUpdateEventbus: publishPollUpdateEventbus,
		}

		input := sfn_models.PollUpdateRequest{
			PollID: "test-poll-id",
		}
		Convey("Errors when get poll returns not found error", func() {
			getPoll.On("GetPoll", mock.Anything, mock.Anything).Return(nil, errorx.NewErrorBuilder(poliwag_errors.NotFound).Create())
			resp, err := lh.Handle(context.Background(), input)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("Returns early when get poll returns a general error", func() {
			getPoll.On("GetPoll", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
			resp, err := lh.Handle(context.Background(), input)
			So(resp, ShouldNotBeNil)
			So(resp.PollID, ShouldEqual, input.PollID)
			So(err, ShouldBeNil)
			So(queryChoiceAggregations.Calls, ShouldHaveLength, 0)
			So(queryPollAggregation.Calls, ShouldHaveLength, 0)
			So(saveChoiceAggregations.Calls, ShouldHaveLength, 0)
			So(savePollAggregation.Calls, ShouldHaveLength, 0)
			So(updateIsAggregationsEqual.Calls, ShouldHaveLength, 0)
			So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 0)
			So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 0)
		})

		Convey("Returns early when get poll returns a non-active poll", func() {
			getPoll.On("GetPoll", mock.Anything, mock.Anything).Return(&get_poll.Output{
				LifecycleState: sfn_models.LifecycleState{
					Poll: models.Poll{
						PollID: input.PollID,
						Status: models.PollStatusTerminated,
					},
				},
			}, nil)
			resp, err := lh.Handle(context.Background(), input)
			So(resp, ShouldNotBeNil)
			So(resp.PollID, ShouldEqual, input.PollID)
			So(resp.Done, ShouldBeTrue)
			So(err, ShouldBeNil)
			So(queryChoiceAggregations.Calls, ShouldHaveLength, 0)
			So(queryPollAggregation.Calls, ShouldHaveLength, 0)
			So(saveChoiceAggregations.Calls, ShouldHaveLength, 0)
			So(savePollAggregation.Calls, ShouldHaveLength, 0)
			So(updateIsAggregationsEqual.Calls, ShouldHaveLength, 0)
			So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 0)
			So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 0)
		})

		Convey("When get poll returns an active poll", func() {
			getPoll.On("GetPoll", mock.Anything, mock.Anything).Return(&get_poll.Output{
				LifecycleState: sfn_models.LifecycleState{
					Poll: models.Poll{
						PollID:    input.PollID,
						Status:    models.PollStatusActive,
						StartTime: time.Now(),
						Duration:  time.Minute,
					},
				},
			}, nil)

			Convey("Returns early when context times out before queries finish", func() {
				queryChoiceAggregations.On("QueryChoiceAggregations", mock.Anything, mock.Anything).Return(&query_choice_aggregations.Output{}, nil).After(100 * time.Millisecond)
				queryPollAggregation.On("QueryPollAggregation", mock.Anything, mock.Anything).Return(&query_poll_aggregation.Output{}, nil).After(100 * time.Millisecond)
				ctx, cancel := context.WithCancel(context.Background())
				cancel()

				resp, err := lh.Handle(ctx, input)
				So(resp, ShouldNotBeNil)
				So(resp.PollID, ShouldEqual, input.PollID)
				So(resp.Done, ShouldBeFalse)
				So(err, ShouldBeNil)
				So(saveChoiceAggregations.Calls, ShouldHaveLength, 0)
				So(savePollAggregation.Calls, ShouldHaveLength, 0)
				So(updateIsAggregationsEqual.Calls, ShouldHaveLength, 0)
				So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 0)
				So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 0)
			})

			Convey("Returns early when one query errors and the other succeeds", func() {
				queryChoiceAggregations.On("QueryChoiceAggregations", mock.Anything, mock.Anything).Return(&query_choice_aggregations.Output{}, nil)
				saveChoiceAggregations.On("SaveChoiceAggregations", mock.Anything, mock.Anything).Return(nil)
				queryPollAggregation.On("QueryPollAggregation", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))

				resp, err := lh.Handle(context.Background(), input)
				So(resp, ShouldNotBeNil)
				So(resp.PollID, ShouldEqual, input.PollID)
				So(resp.Done, ShouldBeFalse)
				So(err, ShouldBeNil)
				So(savePollAggregation.Calls, ShouldHaveLength, 0)
				So(updateIsAggregationsEqual.Calls, ShouldHaveLength, 0)
				So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 0)
				So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 0)
			})

			Convey("When both query/updates succeed", func() {
				updatedPoll := models.Poll{
					PollID:    input.PollID,
					Status:    models.PollStatusActive,
					StartTime: time.Now().Add(-30 * time.Second),
					Duration:  1 * time.Minute,
				}
				queryChoiceAggregations.On("QueryChoiceAggregations", mock.Anything, mock.Anything).Return(&query_choice_aggregations.Output{}, nil)
				saveChoiceAggregations.On("SaveChoiceAggregations", mock.Anything, mock.Anything).Return(nil)
				queryPollAggregation.On("QueryPollAggregation", mock.Anything, mock.Anything).Return(&query_poll_aggregation.Output{}, nil)
				savePollAggregation.On("SavePollAggregation", mock.Anything, mock.Anything).Return(&save_poll_aggregation.Output{
					LifecycleState: sfn_models.LifecycleState{
						Poll: updatedPoll,
						PrevPollAggregation: votes.PollAggregation{
							PollID: input.PollID,
						},
					},
				}, nil)

				Convey("Returns early when update_is_aggregations_equal errors", func() {
					updateIsAggregationsEqual.On("UpdateIsAggregationsEqual", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
					resp, err := lh.Handle(context.Background(), input)
					So(resp, ShouldNotBeNil)
					So(resp.PollID, ShouldEqual, input.PollID)
					So(resp.Done, ShouldBeFalse)
					So(err, ShouldBeNil)
					So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 0)
					So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 0)
				})

				Convey("Returns early when update_is_aggregations_equal returns true", func() {
					updateIsAggregationsEqual.On("UpdateIsAggregationsEqual", mock.Anything, mock.Anything).Return(&update_is_aggregations_equal.Output{
						IsAggregationsEqual: true,
					}, nil)
					resp, err := lh.Handle(context.Background(), input)
					So(resp, ShouldNotBeNil)
					So(resp.PollID, ShouldEqual, input.PollID)
					So(resp.Done, ShouldBeFalse)
					So(err, ShouldBeNil)
					So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 0)
					So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 0)
				})

				Convey("When update_is_aggregations_equal returns false", func() {
					updateIsAggregationsEqual.On("UpdateIsAggregationsEqual", mock.Anything, mock.Anything).Return(&update_is_aggregations_equal.Output{
						IsAggregationsEqual: false,
						LifecycleState: sfn_models.LifecycleState{
							Poll: updatedPoll,
						},
					}, nil)
					publishPollUpdateEventbus.On("PublishPollUpdate", mock.Anything, publish_poll_update_eventbus.Input{
						Poll: updatedPoll,
					}).Return()

					Convey("Succeeds when pubsub errors", func() {
						sendPollUpdatePubsub.On("SendPollUpdatePubsub", mock.Anything, mock.Anything).Return(nil)
						resp, err := lh.Handle(context.Background(), input)
						So(resp, ShouldNotBeNil)
						So(resp.PollID, ShouldEqual, input.PollID)
						So(resp.Done, ShouldBeFalse)
						So(err, ShouldBeNil)
						So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 1)
						So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 1)
					})

					Convey("Succeeds when pubsub succeeds", func() {
						sendPollUpdatePubsub.On("SendPollUpdatePubsub", mock.Anything, mock.Anything).Return(errors.New("test-error"))
						resp, err := lh.Handle(context.Background(), input)
						So(resp, ShouldNotBeNil)
						So(resp.PollID, ShouldEqual, input.PollID)
						So(resp.Done, ShouldBeFalse)
						So(err, ShouldBeNil)
						So(sendPollUpdatePubsub.Calls, ShouldHaveLength, 1)
						So(publishPollUpdateEventbus.Calls, ShouldHaveLength, 1)
					})
				})
			})
		})
	})
}
