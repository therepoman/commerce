package save_poll_aggregation

import (
	"context"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type Step interface {
	SavePollAggregation(ctx context.Context, input Input) (*Output, error)
}

type step struct {
	pollsDAO polls.PollsDAO
}

func NewStep(pollsDAO polls.PollsDAO) Step {
	return &step{
		pollsDAO: pollsDAO,
	}
}

type Input struct {
	PollID          string
	PollAggregation votes.PollAggregation
	LifecycleState  models.LifecycleState
}

type Output struct {
	LifecycleState models.LifecycleState
}

func (s step) SavePollAggregation(ctx context.Context, input Input) (*Output, error) {
	updateArgs := polls.PollUpdateArgs{
		TotalVoters:        pointers.IntP(input.PollAggregation.TotalVoters),
		TotalVotes:         pointers.IntP(input.PollAggregation.TotalVotes),
		TotalBaseVotes:     pointers.IntP(input.PollAggregation.TotalBaseVotes),
		TotalBits:          pointers.IntP(input.PollAggregation.TotalBits),
		TotalChannelPoints: pointers.IntP(input.PollAggregation.TotalChannelPoints),
	}

	updatedPoll, err := s.pollsDAO.UpdatePoll(ctx, input.PollID, updateArgs)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error calling UpdatePoll")
		return nil, err
	}

	lifecycleState := input.LifecycleState
	lifecycleState.Poll = updatedPoll

	return &Output{
		LifecycleState: lifecycleState,
	}, nil
}
