package save_poll_aggregation

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/models"
	step_fn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestActivity_SavePollAggregation(t *testing.T) {
	Convey("given an SavePollAggregation activity", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		step := step{
			pollsDAO: pollsDAO,
		}

		pollID := "test-poll-id"
		totalVoters := 2000
		totalVotes := 1337
		totalBaseVotes := 99
		totalBits := 88
		totalChannelPoints := 151
		updateArgs := polls.PollUpdateArgs{
			TotalVoters:        &totalVoters,
			TotalVotes:         &totalVotes,
			TotalBaseVotes:     &totalBaseVotes,
			TotalBits:          &totalBits,
			TotalChannelPoints: &totalChannelPoints,
		}

		Convey("when UpdatePoll errors", func() {
			pollsDAO.On("UpdatePoll", mock.Anything, pollID, updateArgs).Return(models.Poll{}, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, err := step.SavePollAggregation(context.Background(), Input{
					PollID: pollID,
					PollAggregation: votes.PollAggregation{
						TotalVoters:        totalVoters,
						TotalVotes:         totalVotes,
						TotalBaseVotes:     totalBaseVotes,
						TotalBits:          totalBits,
						TotalChannelPoints: totalChannelPoints,
					},
					LifecycleState: step_fn_models.LifecycleState{
						Poll: models.Poll{
							PollID: pollID,
						},
					},
				})

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when UpdatePoll returns an updated poll", func() {
			updatedPoll := models.Poll{
				PollID: "new-poll-id",
			}

			pollsDAO.On("UpdatePoll", mock.Anything, pollID, updateArgs).Return(updatedPoll, nil)

			Convey("we should return with the new poll", func() {
				actualOutput, err := step.SavePollAggregation(context.Background(), Input{
					PollID: pollID,
					PollAggregation: votes.PollAggregation{
						TotalVoters:        totalVoters,
						TotalVotes:         totalVotes,
						TotalBaseVotes:     totalBaseVotes,
						TotalBits:          totalBits,
						TotalChannelPoints: totalChannelPoints,
					},
					LifecycleState: step_fn_models.LifecycleState{
						Poll: models.Poll{
							PollID: pollID,
						},
					},
				})

				expectedOutput := &Output{
					LifecycleState: step_fn_models.LifecycleState{
						Poll: updatedPoll,
					},
				}

				So(err, ShouldBeNil)
				So(actualOutput, ShouldResemble, expectedOutput)
			})
		})
	})
}
