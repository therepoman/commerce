package send_poll_update_pubsub

import (
	"context"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
)

type Step interface {
	SendPollUpdatePubsub(ctx context.Context, input Input) error
}

type step struct {
	controller pubsub.Controller
}

func NewStep(controller pubsub.Controller) Step {
	return &step{
		controller: controller,
	}
}

type Input struct {
	PollID string
}

func (s step) SendPollUpdatePubsub(ctx context.Context, input Input) error {
	err := s.controller.SendPollUpdatePubsub(ctx, input.PollID)
	if err != nil {
		logrus.WithField("pollID", input.PollID).WithError(err).Error("error send PollUpdate pubsub")
		return err
	}

	return nil
}
