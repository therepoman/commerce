package update_is_aggregations_equal_test

import (
	"context"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/update_is_aggregations_equal"
	stepfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	pubsub_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/pubsub"
)

func TestActivity_UpdateIsAggregationsEqual(t *testing.T) {
	Convey("given a UpdateIsAggregationsEqual activity", t, func() {
		pubsubController := new(pubsub_mock.Controller)

		step := update_is_aggregations_equal.NewStep(pubsubController)

		pollID := "test-poll-id"
		Convey("when choice aggregations are nil", func() {
			input := update_is_aggregations_equal.Input{
				ChoiceAggregations: nil,
				LifecycleState: stepfn_models.LifecycleState{
					PrevChoiceAggregations: nil,
				},
			}

			Convey("we should return an IsAggregationsEqual true", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeTrue)
			})
		})

		Convey("when choice aggregations are different lengths", func() {
			input := update_is_aggregations_equal.Input{
				ChoiceAggregations: []votes.ChoiceAggregation{
					{
						ChoiceID:       "test-choice-id",
						TotalVoters:    10,
						TotalBaseVotes: 10,
					},
				},
				LifecycleState: stepfn_models.LifecycleState{
					PrevChoiceAggregations: nil,
				},
			}

			Convey("we should return an IsAggregationsEqual false", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeFalse)
			})
		})

		Convey("when choice aggregations are different unequal", func() {
			input := update_is_aggregations_equal.Input{
				ChoiceAggregations: []votes.ChoiceAggregation{
					{
						ChoiceID:       "test-choice-id",
						TotalVoters:    10,
						TotalBaseVotes: 10,
					},
				},
				LifecycleState: stepfn_models.LifecycleState{
					PrevChoiceAggregations: []votes.ChoiceAggregation{
						{
							ChoiceID:       "test-choice-id",
							TotalVoters:    5,
							TotalBaseVotes: 5,
						},
					},
				},
			}

			Convey("we should return an IsAggregationsEqual false", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeFalse)
			})
		})

		Convey("when choice aggregations are different equal", func() {
			choiceAggregations := []votes.ChoiceAggregation{
				{
					ChoiceID:       "test-choice-id",
					TotalVoters:    10,
					TotalBaseVotes: 10,
				},
			}

			input := update_is_aggregations_equal.Input{
				ChoiceAggregations: choiceAggregations,
				LifecycleState: stepfn_models.LifecycleState{
					PrevChoiceAggregations: choiceAggregations,
				},
			}

			Convey("we should return an IsAggregationsEqual true", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeTrue)
			})
		})

		Convey("when poll aggregation are nil", func() {
			input := update_is_aggregations_equal.Input{}

			Convey("we should return an IsAggregationsEqual true", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeTrue)
			})
		})

		Convey("when poll aggregation are unequal", func() {
			input := update_is_aggregations_equal.Input{
				PollAggregation: votes.PollAggregation{
					PollID:         pollID,
					TotalVoters:    10,
					TotalBaseVotes: 10,
				},
				LifecycleState: stepfn_models.LifecycleState{
					PrevPollAggregation: votes.PollAggregation{
						PollID:         pollID,
						TotalVoters:    7,
						TotalBaseVotes: 7,
					},
				},
			}

			Convey("we should return an IsAggregationsEqual false", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeFalse)
			})
		})

		Convey("when poll aggregation are equal", func() {
			input := update_is_aggregations_equal.Input{
				PollAggregation: votes.PollAggregation{
					PollID:         pollID,
					TotalVoters:    7,
					TotalBaseVotes: 7,
				},
				LifecycleState: stepfn_models.LifecycleState{
					PrevPollAggregation: votes.PollAggregation{
						PollID:         pollID,
						TotalVoters:    7,
						TotalBaseVotes: 7,
					},
				},
			}

			Convey("we should return an IsAggregationsEqual true", func() {
				output, err := step.UpdateIsAggregationsEqual(context.Background(), input)

				So(err, ShouldBeNil)
				So(output.IsAggregationsEqual, ShouldBeTrue)
			})
		})
	})
}
