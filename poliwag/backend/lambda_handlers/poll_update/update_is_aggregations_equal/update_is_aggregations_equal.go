package update_is_aggregations_equal

import (
	"context"

	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	stepfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type Step interface {
	UpdateIsAggregationsEqual(ctx context.Context, input Input) (*Output, error)
}

type step struct {
	controller pubsub.Controller
}

func NewStep(controller pubsub.Controller) Step {
	return &step{
		controller: controller,
	}
}

type Input struct {
	PollAggregation    votes.PollAggregation
	ChoiceAggregations []votes.ChoiceAggregation
	LifecycleState     stepfn_models.LifecycleState
}

type Output struct {
	IsAggregationsEqual bool
	LifecycleState      stepfn_models.LifecycleState
}

func (s step) UpdateIsAggregationsEqual(ctx context.Context, input Input) (*Output, error) {
	// Determine choice aggregation is equal
	prevChoiceAggregations := input.LifecycleState.PrevChoiceAggregations
	newChoiceAggregations := input.ChoiceAggregations

	isChoiceAggregationsEqual := true
	if len(prevChoiceAggregations) != len(newChoiceAggregations) {
		isChoiceAggregationsEqual = false
	} else {
		for i := range newChoiceAggregations {
			if newChoiceAggregations[i] != prevChoiceAggregations[i] {
				isChoiceAggregationsEqual = false
				break
			}
		}
	}

	// Determine poll aggregation is equal
	prevPollAggregation := input.LifecycleState.PrevPollAggregation
	newPollAggregations := input.PollAggregation

	isPollAggregationsEqual := prevPollAggregation == newPollAggregations

	lifecycleState := input.LifecycleState
	lifecycleState.PrevChoiceAggregations = newChoiceAggregations
	lifecycleState.PrevPollAggregation = newPollAggregations

	return &Output{
		IsAggregationsEqual: isChoiceAggregationsEqual && isPollAggregationsEqual,
		LifecycleState:      lifecycleState,
	}, nil
}
