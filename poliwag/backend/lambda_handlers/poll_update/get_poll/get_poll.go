package get_poll

import (
	"context"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type Step interface {
	GetPoll(ctx context.Context, input Input) (*Output, error)
}

type step struct {
	pollsDAO polls.PollsDAO
}

func NewStep(pollsDAO polls.PollsDAO) Step {
	return &step{
		pollsDAO: pollsDAO,
	}
}

type Input struct {
	PollID         string
	LifecycleState models.LifecycleState
}

type Output struct {
	LifecycleState models.LifecycleState
}

func (s *step) GetPoll(ctx context.Context, input Input) (*Output, error) {
	poll, err := s.pollsDAO.GetPoll(ctx, input.PollID, false)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err)
		return nil, err
	}

	if poll == nil {
		logrus.WithField("poll_id", input.PollID).Error("no poll found for given PollID")
		return nil, errors.NotFound.New("no poll found for PollID %s", input.PollID)
	}

	lifecycleState := input.LifecycleState
	lifecycleState.Poll = *poll

	return &Output{
		LifecycleState: lifecycleState,
	}, nil
}
