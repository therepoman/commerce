package get_poll_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update/get_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	stepfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestActivity_GetPoll(t *testing.T) {
	Convey("given a GetPoll activity", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)

		step := get_poll.NewStep(pollsDAO)

		pollID := "test-poll-id"
		Convey("when GetPoll errors", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, err := step.GetPoll(context.Background(), get_poll.Input{
					PollID: pollID,
				})

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetPoll returns no poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(nil, nil)

			Convey("we should return an error", func() {
				_, err := step.GetPoll(context.Background(), get_poll.Input{
					PollID: pollID,
				})

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetPoll returns a poll", func() {
			poll := models.Poll{
				PollID: pollID,
			}
			pollsDAO.On("GetPoll", mock.Anything, pollID, false).Return(&poll, nil)

			Convey("we should return an error", func() {
				actualOutput, err := step.GetPoll(context.Background(), get_poll.Input{
					PollID: pollID,
				})

				expectedOutput := &get_poll.Output{
					LifecycleState: stepfn_models.LifecycleState{
						Poll: poll,
					},
				}

				So(err, ShouldBeNil)
				So(actualOutput, ShouldResemble, expectedOutput)
			})
		})
	})
}
