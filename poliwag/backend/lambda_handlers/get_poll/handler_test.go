package get_poll

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a lambda handler", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		lh := &LambdaHandler{
			PollsDAO: pollsDAO,
		}

		req := sfn_models.GetPollRequest{
			PollID: "test-poll-id",
		}
		Convey("Errors when pollsDAO get error", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, false).Return(nil, errors.New("test-error"))
			_, err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when pollsDAO returns nil", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, false).Return(nil, nil)
			_, err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when pollDAO returns a poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, false).Return(&models.Poll{
				PollID: req.PollID,
			}, nil)
			resp, err := lh.Handle(context.Background(), req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.PollID, ShouldEqual, req.PollID)
			So(resp.Poll.PollID, ShouldEqual, req.PollID)
		})
	})
}
