package get_poll

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type LambdaHandler struct {
	PollsDAO polls.PollsDAO `validate:"required"`
}

func (l *LambdaHandler) Handle(ctx context.Context, input sfn_models.GetPollRequest) (*sfn_models.GetPollResponse, error) {
	poll, err := l.PollsDAO.GetPoll(ctx, input.PollID, false)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting poll from dynamo (get poll lambda)")
		return nil, err
	}

	if poll == nil {
		logrus.WithField("poll_id", input.PollID).Error("poll not found (get poll lambda)")
		return nil, errors.New("poll not found")
	}

	return &sfn_models.GetPollResponse{
		PollID: input.PollID,
		Poll:   *poll,
	}, nil
}
