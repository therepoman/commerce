package poll_archive

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	eventbus_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	pubsub_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a lambda handler", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		pubsubController := new(pubsub_mock.Controller)
		eventbusController := new(eventbus_mock.Controller)
		lh := &LambdaHandler{
			PollsDAO:           pollsDAO,
			PubsubController:   pubsubController,
			EventbusController: eventbusController,
		}

		req := sfn_models.PollArchiveRequest{
			PollID: "test-poll-id",
		}
		Convey("Errors when pollsDAO get error", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(nil, errors.New("test-error"))
			err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when pollsDAO returns nil", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(nil, nil)
			err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when pollsDAO returns moderated poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(&models.Poll{
				PollID: req.PollID,
				Status: models.PollStatusModerated,
			}, nil)
			err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("When pollsDAO returns terminated poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(&models.Poll{
				PollID: req.PollID,
				Status: models.PollStatusTerminated,
			}, nil)

			Convey("Errors when pollDAO update errors", func() {
				pollsDAO.On("UpdatePoll", mock.Anything, req.PollID, mock.Anything).Return(models.Poll{}, errors.New("test-error"))
				err := lh.Handle(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("When pollDAO update succeeds", func() {
				updatedPoll := models.Poll{
					PollID: req.PollID,
					Status: models.PollStatusArchived,
				}
				pollsDAO.On("UpdatePoll", mock.Anything, req.PollID, mock.Anything).Return(updatedPoll, nil)

				Convey("Errors when pubsub controller errors", func() {
					pubsubController.On("SendPollArchivePubsub", mock.Anything, req.PollID).Return(errors.New("test-error"))
					err := lh.Handle(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("Succeeds when pubsub controller succeeds", func() {
					pubsubController.On("SendPollArchivePubsub", mock.Anything, req.PollID).Return(nil)
					eventbusController.On("PublishPollUpdate", mock.Anything, updatedPoll, true).Return()
					err := lh.Handle(context.Background(), req)
					So(err, ShouldBeNil)
					So(pubsubController.Calls, ShouldHaveLength, 1)
					So(eventbusController.Calls, ShouldHaveLength, 1)
				})
			})
		})

		Convey("Does not perform dynamo update when pollsDAO returns already archived poll", func() {
			poll := models.Poll{
				PollID: req.PollID,
				Status: models.PollStatusArchived,
			}
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(&poll, nil)
			pubsubController.On("SendPollArchivePubsub", mock.Anything, req.PollID).Return(nil)
			eventbusController.On("PublishPollUpdate", mock.Anything, poll, true).Return()
			err := lh.Handle(context.Background(), req)
			So(err, ShouldBeNil)
			So(pubsubController.Calls, ShouldHaveLength, 1)
			So(eventbusController.Calls, ShouldHaveLength, 1)
			So(pollsDAO.Calls, ShouldHaveLength, 1)
		})
	})
}
