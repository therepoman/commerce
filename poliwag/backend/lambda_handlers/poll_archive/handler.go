package poll_archive

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

var forbiddenStatuses = map[models.PollStatus]interface{}{
	models.PollStatusModerated: nil,
}

type LambdaHandler struct {
	PollsDAO           polls.PollsDAO      `validate:"required"`
	PubsubController   pubsub.Controller   `validate:"required"`
	EventbusController eventbus.Controller `validate:"required"`
}

func (l *LambdaHandler) Handle(ctx context.Context, input sfn_models.PollArchiveRequest) error {
	poll, err := l.PollsDAO.GetPoll(ctx, input.PollID, true)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting poll from dynamo (poll archive lambda)")
		return err
	}

	if poll == nil {
		logrus.WithField("poll_id", input.PollID).Error("poll not found (poll archive lambda)")
		return errors.New("poll not found")
	}

	_, forbidden := forbiddenStatuses[poll.Status]
	if forbidden {
		msg := "poll cannot be archived"
		logrus.WithFields(logrus.Fields{"poll_id": input.PollID, "status": poll.Status}).WithError(err).Errorf("%s (poll archive lambda)", msg)
		return errors.New(msg)
	}

	updatedPoll := *poll
	if poll.Status == models.PollStatusArchived {
		logrus.WithField("poll_id", input.PollID).Info("poll already marked archived in dynamo (poll archive lambda)")
		// we don't need to update dynamo (this was done in the API), but we still need to send the archive pubsub
	} else {
		newStatus := models.PollStatusArchived
		updatedPoll, err = l.PollsDAO.UpdatePoll(ctx, input.PollID, polls.PollUpdateArgs{
			Status: &newStatus,
		})
		if err != nil {
			logrus.WithField("poll_id", input.PollID).WithError(err).Errorf("error setting poll status to %s (poll archive lambda)", newStatus)
			return err
		}
	}

	err = l.PubsubController.SendPollArchivePubsub(ctx, input.PollID)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error publishing poll archive pubsub (poll archive lambda)")
		return err
	}

	l.EventbusController.PublishPollUpdate(ctx, updatedPoll, true)

	return nil
}
