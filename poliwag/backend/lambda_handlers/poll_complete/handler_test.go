package poll_complete

import (
	"context"
	"errors"
	"testing"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
	eventbus_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	pubsub_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	slack_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/slack"
	polls_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/polls"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a lambda handler", t, func() {
		pollsDAO := new(polls_dao_mock.PollsDAO)
		pubsubController := new(pubsub_mock.Controller)
		eventbusController := new(eventbus_mock.Controller)
		statsClient, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)
		slackController := new(slack_mock.Controller)

		lh := &LambdaHandler{
			PollsDAO:           pollsDAO,
			PubsubController:   pubsubController,
			EventbusController: eventbusController,
			Statsd:             statsClient,
			SampleReporter:     telemetry.NewNoOpSampleReporter(),
			SlackController:    slackController,
		}

		req := sfn_models.PollCompleteRequest{
			PollID: "test-poll-id",
		}

		Convey("Errors when pollsDAO get error", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(nil, errors.New("test-error"))
			_, err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when pollsDAO returns nil", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(nil, nil)
			_, err := lh.Handle(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Exits early when pollsDAO returns non-active poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(&models.Poll{
				PollID: req.PollID,
				Status: models.PollStatusTerminated,
			}, nil)
			_, err := lh.Handle(context.Background(), req)
			So(err, ShouldBeNil)
			So(pubsubController.Calls, ShouldBeEmpty)
			So(eventbusController.Calls, ShouldBeEmpty)
		})

		Convey("When pollsDAO returns active poll", func() {
			pollsDAO.On("GetPoll", mock.Anything, req.PollID, true).Return(&models.Poll{
				PollID: req.PollID,
				Status: models.PollStatusActive,
			}, nil)

			Convey("Errors when pollDAO update errors", func() {
				pollsDAO.On("UpdatePoll", mock.Anything, req.PollID, mock.Anything).Return(models.Poll{}, errors.New("test-error"))
				_, err := lh.Handle(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("When pollDAO update succeeds", func() {
				updatedPoll := models.Poll{
					PollID: req.PollID,
					Status: models.PollStatusCompleted,
				}
				pollsDAO.On("UpdatePoll", mock.Anything, req.PollID, mock.Anything).Return(updatedPoll, nil)

				Convey("Errors when pubsub controller errors", func() {
					pubsubController.On("SendPollCompletePubsub", mock.Anything, req.PollID).Return(errors.New("test-error"))
					_, err := lh.Handle(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("When pubsub controller succeeds", func() {
					pubsubController.On("SendPollCompletePubsub", mock.Anything, req.PollID).Return(nil)
					eventbusController.On("PublishPollUpdate", mock.Anything, updatedPoll, true).Return()

					Convey("Succeeds when slack controller errors", func() {
						slackController.On("PostPollEnd", mock.Anything, req.PollID).Return(errors.New("test-error"))
						_, err := lh.Handle(context.Background(), req)
						So(err, ShouldBeNil)
						So(pubsubController.Calls, ShouldHaveLength, 1)
						So(eventbusController.Calls, ShouldHaveLength, 1)
						So(slackController.Calls, ShouldHaveLength, 1)
					})

					Convey("Succeeds when slack controller succeeds", func() {
						slackController.On("PostPollEnd", mock.Anything, req.PollID).Return(nil)

						_, err := lh.Handle(context.Background(), req)
						So(err, ShouldBeNil)
						So(pubsubController.Calls, ShouldHaveLength, 1)
						So(eventbusController.Calls, ShouldHaveLength, 1)
						So(slackController.Calls, ShouldHaveLength, 1)
					})
				})
			})
		})
	})
}
