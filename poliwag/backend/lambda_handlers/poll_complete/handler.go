package poll_complete

import (
	"context"
	"errors"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/controller/slack"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	sfn_models "code.justin.tv/commerce/poliwag/backend/stepfn/models"
)

type LambdaHandler struct {
	PollsDAO           polls.PollsDAO            `validate:"required"`
	PubsubController   pubsub.Controller         `validate:"required"`
	EventbusController eventbus.Controller       `validate:"required"`
	Statsd             statsd.Statter            `validate:"required"`
	SampleReporter     *telemetry.SampleReporter `validate:"required"`
	SlackController    slack.Controller          `validate:"required"`
}

func (l *LambdaHandler) Handle(ctx context.Context, input sfn_models.PollCompleteRequest) (*sfn_models.PollCompleteResponse, error) {
	poll, err := l.PollsDAO.GetPoll(ctx, input.PollID, true)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error getting poll from dynamo (poll complete lambda)")
		return nil, err
	}

	if poll == nil {
		logrus.WithField("poll_id", input.PollID).Error("poll not found (poll complete lambda)")
		return nil, errors.New("poll not found")
	}

	if poll.Status != models.PollStatusActive {
		logrus.WithField("poll_id", input.PollID).Info("poll has already ended (poll complete lambda)")
		return &sfn_models.PollCompleteResponse{
			PollID: input.PollID,
		}, nil
	}

	newStatus := models.PollStatusCompleted
	endTime := poll.StartTime.Add(poll.Duration)
	updatedPoll, err := l.PollsDAO.UpdatePoll(ctx, input.PollID, polls.PollUpdateArgs{
		Status:  &newStatus,
		EndTime: &endTime,
	})
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Errorf("error setting poll status to %s (poll complete lambda)", newStatus)
		return nil, err
	}

	err = l.PubsubController.SendPollCompletePubsub(ctx, input.PollID)
	if err != nil {
		logrus.WithField("poll_id", input.PollID).WithError(err).Error("error publishing poll complete pubsub (poll complete lambda)")
		return nil, err
	}

	l.EventbusController.PublishPollUpdate(ctx, updatedPoll, true)

	err = l.SlackController.PostPollEnd(ctx, input.PollID)
	if err != nil {
		logrus.WithField("pollID", input.PollID).WithError(err).Info("failed to post poll end to slack when completing poll")
	}

	drift := time.Duration(math.Int64Abs(int64(time.Now().Sub(endTime))))
	if statErr := l.Statsd.TimingDuration("PollComplete_Drift", drift, 1.0); statErr != nil {
		logrus.WithFields(logrus.Fields{"poll_id": input.PollID, "drift": drift}).WithError(err).Error("error logging PollComplete_Drift metric")
	}
	l.SampleReporter.ReportDurationSample("PollCompleteDriftDuration", drift)

	return &sfn_models.PollCompleteResponse{
		PollID: input.PollID,
	}, nil
}
