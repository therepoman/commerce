package get_poll_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/get_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	poll_viewer_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_viewer"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestPollAPI_GetPoll(t *testing.T) {
	Convey("given a poll api", t, func() {
		authorizer := new(poll_viewer_mock.Authorizer)
		controller := new(get_poll_mock.Controller)

		api := get_poll.NewAPI(authorizer, controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPoll(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPoll(context.Background(), &poliwag.GetPollRequest{
					UserId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		userID := "test-user-id"

		Convey("when pollID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPoll(context.Background(), &poliwag.GetPollRequest{
					UserId: userID,
					PollId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		pollID := "poll-id"

		Convey("when the request passes validation", func() {
			req := &poliwag.GetPollRequest{
				UserId: userID,
				PollId: pollID,
			}

			Convey("when the authorizer errors", func() {
				authorizer.On("Authorize", mock.Anything, userID, pollID).Return(errors.New("test-error"))

				Convey("we should return an error", func() {
					resp, err := api.GetPoll(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when the authorizer succeeds", func() {
				authorizer.On("Authorize", mock.Anything, userID, pollID).Return(nil)

				Convey("when GetPoll returns an error", func() {
					controller.On("GetPoll", mock.Anything, req.PollId, false).Return(models.Poll{}, []models.Choice{}, nil, nil, errors.New("boom!~"))

					Convey("we should return an error", func() {
						resp, err := api.GetPoll(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})
				})

				Convey("when GetPoll does not return an error", func() {
					pollID := "test-poll-id"
					newPoll := models.Poll{
						PollID: pollID,
					}
					topBits := &models.TopBitsContributor{
						UserID:          "1234",
						BitsContributed: 10,
					}
					topChannelPoints := &models.TopChannelPointsContributor{
						UserID:                   "5678",
						ChannelPointsContributed: 100,
					}

					controller.On("GetPoll", mock.Anything, req.PollId, false).Return(newPoll, []models.Choice{
						{ChoiceID: "choice-1"},
						{ChoiceID: "choice-2"},
					}, topBits, topChannelPoints, nil)

					Convey("we should return a response", func() {
						resp, err := api.GetPoll(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.Poll.PollId, ShouldEqual, pollID)
						So(resp.Poll.Choices, ShouldHaveLength, 2)
						So(resp.Poll.TopBitsContributor, ShouldNotBeNil)
						So(resp.Poll.TopChannelPointsContributor, ShouldNotBeNil)
					})
				})
			})
		})
	})
}
