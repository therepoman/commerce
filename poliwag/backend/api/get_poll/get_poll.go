package get_poll

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_viewer"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

type API interface {
	GetPoll(ctx context.Context, req *poliwag.GetPollRequest) (*poliwag.GetPollResponse, error)
}

type api struct {
	authorizer poll_viewer.Authorizer
	controller get_poll.Controller
}

func NewAPI(authorizer poll_viewer.Authorizer, controller get_poll.Controller) API {
	return &api{
		authorizer: authorizer,
		controller: controller,
	}
}

func (api api) GetPoll(ctx context.Context, req *poliwag.GetPollRequest) (*poliwag.GetPollResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userId")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollId")
	}

	if err := api.authorizer.Authorize(ctx, req.UserId, req.PollId); err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	p, choices, topBits, topChannelPoints, err := api.controller.GetPoll(ctx, req.PollId, false)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	protoPoll, err := proto.PollToProtoPoll(p, choices, topBits, topChannelPoints, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling poll to proto poll"
		logrus.WithField("pollID", p.PollID).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &poliwag.GetPollResponse{
		Poll: &protoPoll,
	}, nil
}
