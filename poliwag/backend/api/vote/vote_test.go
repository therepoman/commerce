package vote_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/api/vote"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/clients/cloudwatchlogger"
	poll_voter_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_voter"
	vote_rate_limiter_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/cache/vote_rate_limiter"
	vote_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/vote"
	poliwag_error "code.justin.tv/commerce/poliwag/proto/errors"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestApi_Vote(t *testing.T) {
	Convey("given a poll api", t, func() {
		authorizer := new(poll_voter_mock.Authorizer)
		controller := new(vote_mock.Controller)
		rateLimiter := new(vote_rate_limiter_mock.VoteRateLimiter)
		sampleReporter := telemetry.NewNoOpSampleReporter()
		auditLogger := cloudwatchlogger.NewCloudWatchLogNoopClient()

		api := vote.NewAPI(authorizer, controller, rateLimiter, sampleReporter, auditLogger)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when vote_id is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), &poliwag.VoteRequest{
					VoteId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when user_id is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), &poliwag.VoteRequest{
					VoteId: "VoteID",
					UserId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when poll_id is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), &poliwag.VoteRequest{
					VoteId: "VoteID",
					UserId: "UserID",
					PollId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when choice_id is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), &poliwag.VoteRequest{
					VoteId:   "VoteID",
					UserId:   "UserID",
					PollId:   "PollID",
					ChoiceId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the tokens map contains negative bits", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), &poliwag.VoteRequest{
					VoteId:   "VoteID",
					UserId:   "UserID",
					PollId:   "PollID",
					ChoiceId: "ChoiceID",
					Tokens: &poliwag.Tokens{
						Bits: -1,
					},
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the tokens map contains negative channel points", func() {
			Convey("we should return an error", func() {
				resp, err := api.Vote(context.Background(), &poliwag.VoteRequest{
					VoteId:   "VoteID",
					UserId:   "UserID",
					PollId:   "PollID",
					ChoiceId: "ChoiceID",
					Tokens: &poliwag.Tokens{
						ChannelPoints: -1,
					},
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the request is valid", func() {
			req := &poliwag.VoteRequest{
				VoteId:   "VoteID",
				UserId:   "UserID",
				PollId:   "PollID",
				ChoiceId: "ChoiceID",
				Tokens:   nil,
			}

			Convey("when the rate limit check errors", func() {
				rateLimiter.On("CanVote", mock.Anything, req.UserId, req.PollId).Return(false, errors.New("error"))

				Convey("we should return an error", func() {
					resp, err := api.Vote(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when the user/poll is rate limited", func() {
				rateLimiter.On("CanVote", mock.Anything, req.UserId, req.PollId).Return(false, nil)

				Convey("we should return a specific error", func() {
					resp, err := api.Vote(context.Background(), req)
					So(err, ShouldNotBeNil)
					poliwagError, parseErr := poliwag_error.ParsePoliwagError(err)
					So(parseErr, ShouldBeNil)
					So(poliwagError.ErrorCode, ShouldResemble, poliwag.VoteErrorCode_RATE_LIMITED.String())
					So(resp, ShouldBeNil)
				})
			})

			Convey("when the user/poll is not rate limited", func() {
				rateLimiter.On("CanVote", mock.Anything, req.UserId, req.PollId).Return(true, nil)

				Convey("when authorization errors", func() {
					authorizer.On("Authorize", mock.Anything, req.UserId, req.PollId).Return(errors.New("error"))

					Convey("we should return an error", func() {
						resp, err := api.Vote(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})
				})

				Convey("when authorization succeeds", func() {
					authorizer.On("Authorize", mock.Anything, req.UserId, req.PollId).Return(nil)

					Convey("when the controller errors", func() {
						controller.On("Vote", mock.Anything, req.VoteId, req.PollId, req.UserId, req.ChoiceId, mock.Anything).Return(models.Poll{}, models.Voter{}, errors.New("test-error"))
						Convey("we should return an error", func() {
							resp, err := api.Vote(context.Background(), req)
							So(err, ShouldNotBeNil)
							So(resp, ShouldBeNil)
						})
					})

					Convey("when the controller succeeds", func() {
						controller.On("Vote", mock.Anything, req.VoteId, req.PollId, req.UserId, req.ChoiceId, mock.Anything).Return(models.Poll{}, models.Voter{}, nil)
						Convey("we should succeed", func() {
							resp, err := api.Vote(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
						})
					})
				})
			})
		})
	})
}
