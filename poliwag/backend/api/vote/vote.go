package vote

import (
	"context"
	"fmt"
	"time"

	"github.com/twitchtv/twirp"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/audit"
	"code.justin.tv/commerce/poliwag/backend/authorization/poll_voter"
	"code.justin.tv/commerce/poliwag/backend/cache/vote_rate_limiter"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/vote"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/clients/cloudwatchlogger"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

type API interface {
	Vote(ctx context.Context, req *poliwag.VoteRequest) (*poliwag.VoteResponse, error)
}

type api struct {
	authorizer     poll_voter.Authorizer
	controller     vote.Controller
	rateLimiter    vote_rate_limiter.VoteRateLimiter
	sampleReporter *telemetry.SampleReporter
	auditLogger    cloudwatchlogger.CloudWatchLogger
}

func NewAPI(authorizer poll_voter.Authorizer, controller vote.Controller, rateLimiter vote_rate_limiter.VoteRateLimiter, sampleReporter *telemetry.SampleReporter, auditLogger cloudwatchlogger.CloudWatchLogger) API {
	return &api{
		authorizer:     authorizer,
		controller:     controller,
		rateLimiter:    rateLimiter,
		sampleReporter: sampleReporter,
		auditLogger:    auditLogger,
	}
}

func (api *api) Vote(ctx context.Context, req *poliwag.VoteRequest) (*poliwag.VoteResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.VoteId == "" {
		return nil, twirp.RequiredArgumentError("voteId")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userId")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollId")
	}

	if req.ChoiceId == "" {
		return nil, twirp.RequiredArgumentError("choiceId")
	}

	tokenMap, err := proto.TokensProtoToTokenMap(req.Tokens)
	if err != nil {
		return nil, errors.PoliwagVoteErrorToTwirpError(err)
	}

	if canVote, err := api.rateLimiter.CanVote(ctx, req.UserId, req.PollId); err != nil {
		return nil, errors.PoliwagVoteErrorToTwirpError(err)
	} else if !canVote {
		err = errors.RateLimited.New("too many vote attempts per second")
		return nil, errors.PoliwagVoteErrorToTwirpError(err)
	}

	if err := api.authorizer.Authorize(ctx, req.UserId, req.PollId); err != nil {
		return nil, errors.PoliwagVoteErrorToTwirpError(err)
	}

	// for SOX audit logging
	rawBitsTokens := tokenMap[models.TokenTypeBits]
	if rawBitsTokens > 0 {
		apiName := "Vote"
		auditLoggerTimer := time.Now()
		msg, err := audit.CreateAccessLogMsg(ctx, apiName, req)
		if err != nil {
			log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
		} else {
			err = api.auditLogger.Send(ctx, msg)
			if err != nil {
				log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
			}
		}
		api.sampleReporter.ReportDurationSample(models.VOTE_AUDITLOGGER_METRIC_NAME, time.Since(auditLoggerTimer))
	}

	poll, voter, err := api.controller.Vote(ctx, req.VoteId, req.PollId, req.UserId, req.ChoiceId, tokenMap)
	if err != nil {
		return nil, errors.PoliwagVoteErrorToTwirpError(err)
	}

	protoVoter := proto.VoterToProtoVoter(poll, voter)

	return &poliwag.VoteResponse{
		Voter: &protoVoter,
	}, nil
}
