package archive_poll_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/archive_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	poll_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	archive_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/archive_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestApi_ArchivePoll(t *testing.T) {
	Convey("given a poll api", t, func() {
		authorizer := new(poll_editor_mock.Authorizer)
		controller := new(archive_poll_mock.Controller)

		api := archive_poll.NewAPI(authorizer, controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.ArchivePoll(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.ArchivePoll(context.Background(), &poliwag.ArchivePollRequest{
					UserId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		userID := "test-user-id"

		Convey("when pollID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.ArchivePoll(context.Background(), &poliwag.ArchivePollRequest{
					UserId: userID,
					PollId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the request passes validation", func() {
			pollID := "test-poll-id"
			req := &poliwag.ArchivePollRequest{
				UserId: userID,
				PollId: pollID,
			}

			Convey("when the authorizer errors", func() {
				authorizer.On("Authorize", mock.Anything, userID, pollID).Return(errors.New("test-error"))

				Convey("we should return an error", func() {
					resp, err := api.ArchivePoll(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when the authorizer succeeds", func() {
				authorizer.On("Authorize", mock.Anything, userID, pollID).Return(nil)

				Convey("when poll controller ArchivePoll returns an error", func() {
					controller.On("ArchivePoll", mock.Anything, req.UserId, req.PollId).Return(models.Poll{}, nil, nil, nil, errors.New("test-error"))

					Convey("we should return an error", func() {
						resp, err := api.ArchivePoll(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})
				})

				Convey("when poll controller ArchivePoll succeeds", func() {
					pollID := "test-poll-id"
					newPoll := models.Poll{
						PollID: pollID,
						Status: models.PollStatusArchived,
					}
					newChoices := []models.Choice{
						{
							Title:    "Final Fantasy VII",
							PollID:   pollID,
							ChoiceID: "choice-id-0",
						},
						{
							Title:    "Chrono Trigger",
							PollID:   pollID,
							ChoiceID: "choice-id-1",
						},
					}

					controller.On("ArchivePoll", mock.Anything, req.UserId, req.PollId).Return(newPoll, newChoices, nil, nil, nil)

					Convey("we should return a response", func() {
						resp, err := api.ArchivePoll(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.Poll.PollId, ShouldEqual, pollID)
						So(resp.Poll.Status, ShouldEqual, poliwag.PollStatus_ARCHIVED)
						So(resp.Poll.Choices, ShouldHaveLength, len(newChoices))
					})
				})
			})
		})
	})
}
