package admin_get_polls_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/admin_get_polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	get_polls_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

func TestApi_AdminGetPolls(t *testing.T) {
	Convey("given an admin poll api", t, func() {
		controller := new(get_polls_mock.Controller)

		api := admin_get_polls.NewAPI(controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.AdminGetPolls(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when ownedBy is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.AdminGetPolls(context.Background(), &poliwag_admin.AdminGetPollsRequest{
					OwnedBy: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		ownedBy := "test-owned-by"

		Convey("when limit is too high", func() {
			limit := int64(admin_get_polls.MaxLimit + 100)

			Convey("we should return an error", func() {
				resp, err := api.AdminGetPolls(context.Background(), &poliwag_admin.AdminGetPollsRequest{
					OwnedBy: ownedBy,
					Limit:   limit,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when limit is too low", func() {
			limit := int64(admin_get_polls.MinLimit - 100)

			Convey("we should return an error", func() {
				resp, err := api.AdminGetPolls(context.Background(), &poliwag_admin.AdminGetPollsRequest{
					OwnedBy: ownedBy,
					Limit:   limit,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the request passes validation", func() {
			req := &poliwag_admin.AdminGetPollsRequest{
				OwnedBy: ownedBy,
			}

			Convey("when GetPolls returns an error", func() {
				controller.On("GetPolls", mock.Anything, req.OwnedBy, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil, nil, nil, nil, errors.New("boom!~"))

				Convey("we should return an error", func() {
					resp, err := api.AdminGetPolls(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when GetPolls does not return an error", func() {
				poll1 := models.Poll{
					PollID: "poll1",
				}

				pollChoices := []models.Choice{
					{
						Title:    "Final Fantasy VII",
						PollID:   "poll1",
						ChoiceID: "choice-id-0",
					},
					{
						Title:    "Chrono Trigger",
						PollID:   "poll1",
						ChoiceID: "choice-id-1",
					},
				}

				poll2 := models.Poll{
					PollID: "poll2",
				}

				polls := []models.Poll{
					poll1,
					poll2,
				}

				choices := map[string][]models.Choice{
					"1": pollChoices,
					"2": pollChoices,
					"3": pollChoices,
				}

				controller.On("GetPolls", mock.Anything, req.OwnedBy, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(polls, choices, nil, nil, nil, nil)

				Convey("we should return a response", func() {
					resp, err := api.AdminGetPolls(context.Background(), req)
					So(err, ShouldBeNil)
					So(len(resp.Polls), ShouldEqual, 2)
					So(resp.Polls[0].PollId, ShouldEqual, "poll1")
					So(resp.Polls[1].PollId, ShouldEqual, "poll2")
				})
			})
		})
	})
}
