package moderate_poll_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/moderate_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	moderate_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/moderate_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

func TestApi_ModeratePoll(t *testing.T) {
	Convey("given an api", t, func() {
		controller := new(moderate_poll_mock.Controller)

		api := moderate_poll.NewAPI(controller)

		Convey("when request is nil", func() {
			var req *poliwag_admin.ModeratePollRequest

			Convey("we should return an error", func() {
				_, err := api.ModeratePoll(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when poll id is blank", func() {
			req := &poliwag_admin.ModeratePollRequest{
				PollId: "",
			}

			Convey("we should return an error", func() {
				_, err := api.ModeratePoll(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		pollID := "poll-id"

		req := &poliwag_admin.ModeratePollRequest{
			PollId: pollID,
		}

		Convey("when ModeratePoll errors", func() {
			controller.On("ModeratePoll", mock.Anything, pollID).Return(models.Poll{}, nil, errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, err := api.ModeratePoll(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when ModeratePoll succeeds", func() {
			moderatedPoll := models.Poll{
				PollID: pollID,
				Status: models.PollStatusModerated,
			}

			choices := []models.Choice{
				{
					PollID:   pollID,
					ChoiceID: "choice-id-0",
				},
				{
					PollID:   pollID,
					ChoiceID: "choice-id-1",
				},
			}

			controller.On("ModeratePoll", mock.Anything, pollID).Return(moderatedPoll, choices, nil)

			Convey("we should return a moderated poll", func() {
				moderatedProtoPoll, err := api.ModeratePoll(context.Background(), req)

				So(err, ShouldBeNil)
				So(moderatedProtoPoll.Poll.PollId, ShouldEqual, moderatedPoll.PollID)
				So(moderatedProtoPoll.Poll.Status, ShouldEqual, poliwag.PollStatus_MODERATED)
			})
		})
	})
}
