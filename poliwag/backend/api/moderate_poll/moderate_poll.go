package moderate_poll

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/controller/poll/moderate_poll"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

type API interface {
	ModeratePoll(ctx context.Context, req *poliwag_admin.ModeratePollRequest) (*poliwag_admin.ModeratePollResponse, error)
}

type api struct {
	controller moderate_poll.Controller
}

func NewAPI(controller moderate_poll.Controller) API {
	return &api{
		controller: controller,
	}
}

func (api api) ModeratePoll(ctx context.Context, req *poliwag_admin.ModeratePollRequest) (*poliwag_admin.ModeratePollResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollId")
	}

	moderatedPoll, choices, err := api.controller.ModeratePoll(ctx, req.PollId)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	moderatedProtoPoll, err := proto.PollToProtoPoll(moderatedPoll, choices, nil, nil, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling poll to proto poll"
		logrus.WithField("pollID", req.PollId).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &poliwag_admin.ModeratePollResponse{
		Poll: &moderatedProtoPoll,
	}, nil
}
