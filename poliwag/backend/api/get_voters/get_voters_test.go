package get_voters_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/api/get_voters"
	"code.justin.tv/commerce/poliwag/backend/models"
	poll_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	get_voters_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestApi_GetVoters(t *testing.T) {
	Convey("given a getvoters api", t, func() {
		authorizer := new(poll_editor_mock.Authorizer)
		controller := new(get_voters_mock.Controller)

		api := get_voters.NewAPI(authorizer, controller)

		Convey("when req is nil", func() {
			var req *poliwag.GetVotersRequest

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), req)
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when userID is blank", func() {
			userID := ""

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId: userID,
				})
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		userID := "test-user-id"

		Convey("when pollID is blank", func() {
			pollID := ""

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId: userID,
					PollId: pollID,
				})
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		pollID := "test-poll-id"

		Convey("when sort is invalid", func() {
			sort := poliwag.VoterSort(-777)

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId: userID,
					PollId: pollID,
					Sort:   sort,
				})
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		sort := poliwag.VoterSort_VOTES

		Convey("when direction is invalid", func() {
			direction := poliwag.Direction(-777)

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId:    userID,
					PollId:    pollID,
					Sort:      sort,
					Direction: direction,
				})
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		direction := poliwag.Direction_DESC

		Convey("when limit is too large", func() {
			limit := int64(123456)

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId:    userID,
					PollId:    pollID,
					Sort:      sort,
					Direction: direction,
					Limit:     limit,
				})
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when limit is too small", func() {
			limit := int64(-123456)

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId:    userID,
					PollId:    pollID,
					Sort:      sort,
					Direction: direction,
					Limit:     limit,
				})
				So(err, ShouldNotBeNil)

				twerr, _ := err.(twirp.Error)
				So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		limit := int64(10)

		Convey("when Authorize returns an error", func() {
			authorizer.On("Authorize", mock.Anything, userID, pollID).Return(errors.New("ERROR"))

			Convey("we should return an error", func() {
				_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
					UserId:    userID,
					PollId:    pollID,
					Sort:      sort,
					Direction: direction,
					Limit:     limit,
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when Authorize succeeds", func() {
			authorizer.On("Authorize", mock.Anything, userID, pollID).Return(nil)

			Convey("when GetVoters returns an error", func() {
				controller.On("GetVoters", mock.Anything, pollID, models.VoterSortVotes, int(limit)).Return(models.Poll{}, nil, errors.New("ERROR"))

				Convey("we should return an error", func() {
					_, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
						UserId:    userID,
						PollId:    pollID,
						Sort:      sort,
						Direction: direction,
						Limit:     limit,
					})

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetVoters returns polls and voters", func() {
				poll := models.Poll{
					PollID: pollID,
				}

				voters := []models.Voter{
					{
						PollID: pollID,
						UserID: "voter-user-id-0",
					},
					{
						PollID: pollID,
						UserID: "voter-user-id-1",
					},
				}

				controller.On("GetVoters", mock.Anything, pollID, models.VoterSortVotes, int(limit)).Return(poll, voters, nil)

				Convey("we should return voters", func() {
					resp, err := api.GetVoters(context.Background(), &poliwag.GetVotersRequest{
						UserId:    userID,
						PollId:    pollID,
						Sort:      sort,
						Direction: direction,
						Limit:     limit,
					})

					So(err, ShouldBeNil)
					for i, actualVoter := range resp.Voters {
						So(actualVoter.UserId, ShouldEqual, voters[i].UserID)
					}
				})
			})
		})
	})
}
