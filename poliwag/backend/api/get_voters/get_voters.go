package get_voters

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const (
	DefaultLimit = 10
	MinLimit     = 1
	MaxLimit     = 10
)

type API interface {
	GetVoters(ctx context.Context, req *poliwag.GetVotersRequest) (*poliwag.GetVotersResponse, error)
}

type api struct {
	authorizer poll_editor.Authorizer
	controller get_voters.Controller
}

func NewAPI(authorizer poll_editor.Authorizer, controller get_voters.Controller) API {
	return &api{
		authorizer: authorizer,
		controller: controller,
	}
}

func (api api) GetVoters(ctx context.Context, req *poliwag.GetVotersRequest) (*poliwag.GetVotersResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userID")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollID")
	}

	sort, err := proto.ProtoVoterSortToVoterSort(req.Sort)
	if err != nil {
		return nil, twirp.InvalidArgumentError("sort", "is not valid")
	}

	direction, err := proto.ProtoDirectionToDirection(req.Direction)
	if err != nil {
		return nil, twirp.InvalidArgumentError("direction", "is not valid")
	}

	if direction != models.DirectionDescending {
		return nil, twirp.InvalidArgumentError("direction", "descending is the only currently supported direction")
	}

	limit := DefaultLimit
	if req.Limit != 0 {
		if req.Limit > MaxLimit {
			return nil, twirp.InvalidArgumentError("limit", fmt.Sprintf("must be less than or equal to %d", MaxLimit))
		}

		if req.Limit < MinLimit {
			return nil, twirp.InvalidArgumentError("limit", fmt.Sprintf("must be greater than or equal to %d", MinLimit))
		}

		limit = int(req.Limit)
	}

	if req.Cursor != "" {
		return nil, twirp.InvalidArgumentError("cursor", "cursor is currently unsupported")
	}

	if err := api.authorizer.Authorize(ctx, req.UserId, req.PollId); err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	poll, voters, err := api.controller.GetVoters(ctx, req.PollId, sort, limit)
	if err != nil {
		logrus.WithError(err).Error("encountered error calling controller in GetVoters")
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	protoVoters := proto.VotersToProtoVoters(poll, voters)
	return &poliwag.GetVotersResponse{
		Voters: protoVoters,
	}, nil
}
