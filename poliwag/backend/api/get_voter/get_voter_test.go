package get_voter_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/get_voter"
	"code.justin.tv/commerce/poliwag/backend/models"
	voter_viewer_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/voter_viewer"
	get_voter_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestGetVoter(t *testing.T) {
	Convey("given a get voter api", t, func() {
		authorizer := new(voter_viewer_mock.Authorizer)
		controller := new(get_voter_mock.Controller)

		api := get_voter.NewAPI(authorizer, controller)

		Convey("when userID is empty", func() {
			userID := ""

			Convey("we should return an error", func() {
				_, err := api.GetVoter(context.Background(), &poliwag.GetVoterRequest{
					UserId: userID,
				})

				So(err, ShouldNotBeNil)
			})
		})

		userID := "test-user-id"

		Convey("when pollID is empty", func() {
			pollID := ""

			Convey("we should return an error", func() {
				_, err := api.GetVoter(context.Background(), &poliwag.GetVoterRequest{
					UserId: userID,
					PollId: pollID,
				})

				So(err, ShouldNotBeNil)
			})
		})

		pollID := "test-poll-id"

		Convey("when voterUserID is empty", func() {
			voterUserID := ""

			Convey("we should return an error", func() {
				_, err := api.GetVoter(context.Background(), &poliwag.GetVoterRequest{
					UserId:      userID,
					PollId:      pollID,
					VoterUserId: voterUserID,
				})

				So(err, ShouldNotBeNil)
			})
		})

		voterUserID := "test-voter-user-id"

		Convey("When the authorizer errors", func() {
			authorizer.On("Authorize", mock.Anything, userID, pollID, voterUserID).Return(errors.New("test-error"))

			Convey("we should return an error", func() {
				_, err := api.GetVoter(context.Background(), &poliwag.GetVoterRequest{
					UserId:      userID,
					PollId:      pollID,
					VoterUserId: voterUserID,
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the authorizer succeeds", func() {
			authorizer.On("Authorize", mock.Anything, userID, pollID, voterUserID).Return(nil)

			Convey("when GetVoter returns an error", func() {
				controller.On("GetVoter", mock.Anything, pollID, voterUserID).Return(models.Poll{}, models.Voter{}, errors.New("ERRORS"))

				Convey("we should return an error", func() {
					_, err := api.GetVoter(context.Background(), &poliwag.GetVoterRequest{
						UserId:      userID,
						PollId:      pollID,
						VoterUserId: voterUserID,
					})

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetVoter returns poll and voter", func() {
				poll := models.Poll{
					PollID: pollID,
				}

				voter := models.Voter{
					PollID: pollID,
					UserID: voterUserID,
				}

				controller.On("GetVoter", mock.Anything, pollID, voterUserID).Return(poll, voter, nil)

				Convey("we should return an error", func() {
					actualVoter, err := api.GetVoter(context.Background(), &poliwag.GetVoterRequest{
						UserId:      userID,
						PollId:      pollID,
						VoterUserId: voterUserID,
					})

					So(err, ShouldBeNil)
					So(actualVoter.Voter.UserId, ShouldEqual, voterUserID)
				})
			})
		})
	})
}
