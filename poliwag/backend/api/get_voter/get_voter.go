package get_voter

import (
	"context"

	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/authorization/voter_viewer"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

type API interface {
	GetVoter(ctx context.Context, req *poliwag.GetVoterRequest) (*poliwag.GetVoterResponse, error)
}

type api struct {
	authorizer voter_viewer.Authorizer
	controller get_voter.Controller
}

func NewAPI(authorizer voter_viewer.Authorizer, controller get_voter.Controller) API {
	return &api{
		authorizer: authorizer,
		controller: controller,
	}
}

func (api api) GetVoter(ctx context.Context, req *poliwag.GetVoterRequest) (*poliwag.GetVoterResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userID")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollID")
	}

	if req.VoterUserId == "" {
		return nil, twirp.RequiredArgumentError("voterUserID")
	}

	if err := api.authorizer.Authorize(ctx, req.UserId, req.PollId, req.VoterUserId); err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	poll, voter, err := api.controller.GetVoter(ctx, req.PollId, req.VoterUserId)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	protoVoter := proto.VoterToProtoVoter(poll, voter)

	return &poliwag.GetVoterResponse{
		Voter: &protoVoter,
	}, nil
}
