package terminate_poll

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/terminate_poll"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

type API interface {
	TerminatePoll(ctx context.Context, req *poliwag.TerminatePollRequest) (*poliwag.TerminatePollResponse, error)
}

type api struct {
	authorizer poll_editor.Authorizer
	controller terminate_poll.Controller
}

func NewAPI(authorizer poll_editor.Authorizer, controller terminate_poll.Controller) API {
	return &api{
		authorizer: authorizer,
		controller: controller,
	}
}

func (api api) TerminatePoll(ctx context.Context, req *poliwag.TerminatePollRequest) (*poliwag.TerminatePollResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userId")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollId")
	}

	if err := api.authorizer.Authorize(ctx, req.UserId, req.PollId); err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	updatedPoll, choices, topBits, topChannelPoints, err := api.controller.TerminatePoll(ctx, req.UserId, req.PollId)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	protoPoll, err := proto.PollToProtoPoll(updatedPoll, choices, topBits, topChannelPoints, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling poll to proto poll"
		logrus.WithField("pollID", req.PollId).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &poliwag.TerminatePollResponse{
		Poll: &protoPoll,
	}, nil
}
