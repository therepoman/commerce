package terminate_poll_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/terminate_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	poll_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	terminate_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/terminate_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestApi_TerminatePoll(t *testing.T) {
	Convey("given a poll api", t, func() {
		controller := new(terminate_poll_mock.Controller)
		authorizer := new(poll_editor_mock.Authorizer)

		api := terminate_poll.NewAPI(authorizer, controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.TerminatePoll(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.TerminatePoll(context.Background(), &poliwag.TerminatePollRequest{
					UserId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		userID := "test-user-id"

		Convey("when pollID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.TerminatePoll(context.Background(), &poliwag.TerminatePollRequest{
					UserId: userID,
					PollId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the request passes validation", func() {
			pollID := "test-poll-id"
			req := &poliwag.TerminatePollRequest{
				UserId: userID,
				PollId: pollID,
			}

			Convey("when the authorizer errors", func() {
				authorizer.On("Authorize", mock.Anything, userID, pollID).Return(errors.New("test-error"))

				Convey("we should return an error", func() {
					resp, err := api.TerminatePoll(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when the authorizer succeeds", func() {
				authorizer.On("Authorize", mock.Anything, userID, pollID).Return(nil)

				Convey("when poll controller TerminatePoll returns an error", func() {
					controller.On("TerminatePoll", mock.Anything, req.UserId, req.PollId).Return(models.Poll{}, nil, nil, nil, errors.New("test-error"))

					Convey("we should return an error", func() {
						resp, err := api.TerminatePoll(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})
				})

				Convey("when poll controller TerminatePoll succeeds", func() {
					pollID := "test-poll-id"
					newPoll := models.Poll{
						PollID: pollID,
						Status: models.PollStatusTerminated,
					}
					newChoices := []models.Choice{
						{
							Title:    "Final Fantasy VII",
							PollID:   pollID,
							ChoiceID: "choice-id-0",
						},
						{
							Title:    "Chrono Trigger",
							PollID:   pollID,
							ChoiceID: "choice-id-1",
						},
					}

					controller.On("TerminatePoll", mock.Anything, req.UserId, req.PollId).Return(newPoll, newChoices, nil, nil, nil)

					Convey("we should return a response", func() {
						resp, err := api.TerminatePoll(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.Poll.PollId, ShouldEqual, pollID)
						So(resp.Poll.Status, ShouldEqual, poliwag.PollStatus_TERMINATED)
						So(resp.Poll.Choices, ShouldHaveLength, len(newChoices))
					})
				})
			})
		})
	})
}
