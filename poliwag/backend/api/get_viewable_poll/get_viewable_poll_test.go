package get_viewable_poll_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	get_viewable_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestGetViewablePoll(t *testing.T) {
	Convey("given a poll api", t, func() {
		controller := new(get_viewable_poll_mock.Controller)

		api := get_viewable_poll.NewAPI(controller)

		userID := "test-user-id"

		Convey("when ownedBy is empty", func() {
			ownedBy := ""

			Convey("we should return an error", func() {
				_, err := api.GetViewablePoll(context.Background(), &poliwag.GetViewablePollRequest{
					UserId:  userID,
					OwnedBy: ownedBy,
				})

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when ownedBy is not empty", func() {
			ownedBy := "test-owned-by"

			Convey("when GetViewablePoll returns an error", func() {
				controller.On("GetViewablePoll", mock.Anything, userID, ownedBy).Return(nil, nil, nil, nil, errors.New("ERROR"))

				Convey("we should return an error", func() {
					Convey("we should return an error", func() {
						_, err := api.GetViewablePoll(context.Background(), &poliwag.GetViewablePollRequest{
							UserId:  userID,
							OwnedBy: ownedBy,
						})

						So(err, ShouldNotBeNil)
					})
				})
			})

			Convey("when GetViewablePoll returns a poll", func() {
				pollID := "test-poll-id"

				poll := models.Poll{
					PollID:  pollID,
					OwnedBy: ownedBy,
				}

				choices := []models.Choice{
					{
						ChoiceID: "test-choice-0",
						PollID:   pollID,
						Title:    "Final Fantasy VII",
					},
					{
						ChoiceID: "test-choice-1",
						PollID:   pollID,
						Title:    "Chrono Trigger",
					},
				}

				controller.On("GetViewablePoll", mock.Anything, userID, ownedBy).Return(&poll, choices, nil, nil, nil)

				Convey("we should return a poll", func() {
					actualPoll, err := api.GetViewablePoll(context.Background(), &poliwag.GetViewablePollRequest{
						UserId:  userID,
						OwnedBy: ownedBy,
					})

					So(err, ShouldBeNil)
					So(actualPoll.Poll.PollId, ShouldEqual, pollID)
				})
			})
		})
	})
}
