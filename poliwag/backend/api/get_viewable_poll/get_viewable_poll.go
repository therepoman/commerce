package get_viewable_poll

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

type API interface {
	GetViewablePoll(ctx context.Context, req *poliwag.GetViewablePollRequest) (*poliwag.GetViewablePollResponse, error)
}

type api struct {
	controller get_viewable_poll.Controller
}

func NewAPI(controller get_viewable_poll.Controller) API {
	return &api{
		controller: controller,
	}
}

func (api api) GetViewablePoll(ctx context.Context, req *poliwag.GetViewablePollRequest) (*poliwag.GetViewablePollResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.OwnedBy == "" {
		return nil, twirp.RequiredArgumentError("ownedBy")
	}

	p, choices, topBits, topChannelPoints, err := api.controller.GetViewablePoll(ctx, req.UserId, req.OwnedBy)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	if p == nil {
		return &poliwag.GetViewablePollResponse{
			Poll: nil,
		}, nil
	}

	protoPoll, err := proto.PollToProtoPoll(*p, choices, topBits, topChannelPoints, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling poll to proto poll"
		logrus.WithField("pollID", p.PollID).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &poliwag.GetViewablePollResponse{
		Poll: &protoPoll,
	}, nil
}
