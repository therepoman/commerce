package admin_get_poll

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

type API interface {
	AdminGetPoll(ctx context.Context, req *poliwag_admin.AdminGetPollRequest) (*poliwag_admin.AdminGetPollResponse, error)
}

type api struct {
	controller get_poll.Controller
}

func NewAPI(controller get_poll.Controller) API {
	return &api{
		controller: controller,
	}
}

func (api api) AdminGetPoll(ctx context.Context, req *poliwag_admin.AdminGetPollRequest) (*poliwag_admin.AdminGetPollResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.PollId == "" {
		return nil, twirp.RequiredArgumentError("pollId")
	}

	p, choices, topBits, topChannelPoints, err := api.controller.GetPoll(ctx, req.PollId, true)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	protoPoll, err := proto.PollToProtoPoll(p, choices, topBits, topChannelPoints, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling poll to proto poll"
		logrus.WithField("pollID", p.PollID).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &poliwag_admin.AdminGetPollResponse{
		Poll: &protoPoll,
	}, nil
}
