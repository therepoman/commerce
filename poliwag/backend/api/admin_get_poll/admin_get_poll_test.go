package admin_get_poll_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/admin_get_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	get_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/proto/poliwag_admin"
)

func TestApi_AdminGetPoll(t *testing.T) {
	Convey("given a poll api", t, func() {
		controller := new(get_poll_mock.Controller)

		api := admin_get_poll.NewAPI(controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.AdminGetPoll(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when pollID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.AdminGetPoll(context.Background(), &poliwag_admin.AdminGetPollRequest{
					PollId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		pollID := "poll-id"

		Convey("when the request passes validation", func() {
			req := &poliwag_admin.AdminGetPollRequest{
				PollId: pollID,
			}

			Convey("when GetPoll returns an error", func() {
				controller.On("GetPoll", mock.Anything, req.PollId, true).Return(models.Poll{}, []models.Choice{}, nil, nil, errors.New("boom!~"))

				Convey("we should return an error", func() {
					resp, err := api.AdminGetPoll(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when GetPoll does not return an error", func() {
				pollID := "test-poll-id"
				newPoll := models.Poll{
					PollID: pollID,
				}

				controller.On("GetPoll", mock.Anything, req.PollId, true).Return(newPoll, []models.Choice{
					{ChoiceID: "choice-1"},
					{ChoiceID: "choice-2"},
				}, nil, nil, nil)

				Convey("we should return a response", func() {
					resp, err := api.AdminGetPoll(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp.Poll.PollId, ShouldEqual, pollID)
					So(resp.Poll.Choices, ShouldHaveLength, 2)
				})
			})
		})
	})
}
