package pagination

import (
	b64 "encoding/base64"
	"encoding/json"
	"errors"
	"time"

	"github.com/aws/aws-sdk-go/service/dynamodb"

	"code.justin.tv/commerce/poliwag/backend/models"
)

const (
	CursorTTL = 1 * time.Hour
)

type RequestCursor struct {
	CreatedTime      time.Time
	Sort             string
	Direction        models.Direction
	LastEvaluatedKey map[string]*dynamodb.AttributeValue
}

func Decode(requestCursorString string) (RequestCursor, error) {
	decoded, err := b64.StdEncoding.DecodeString(requestCursorString)
	if err != nil {
		return RequestCursor{}, err
	}

	var requestCursor RequestCursor
	err = json.Unmarshal(decoded, &requestCursor)
	if err != nil {
		return RequestCursor{}, err
	}

	if requestCursor.CreatedTime.After(time.Now().Add(CursorTTL)) {
		return RequestCursor{}, errors.New("cursor has expired")
	}

	return requestCursor, nil
}

func Encode(sort string, direction models.Direction, lastEvaluatedKey map[string]*dynamodb.AttributeValue) (string, error) {
	if lastEvaluatedKey == nil {
		return "", nil
	}

	cursor := RequestCursor{
		CreatedTime:      time.Now(),
		LastEvaluatedKey: lastEvaluatedKey,
		Sort:             sort,
		Direction:        direction,
	}

	bytes, err := json.Marshal(cursor)
	if err != nil {
		return "", err
	}

	return b64.StdEncoding.EncodeToString(bytes), nil
}
