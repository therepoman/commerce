package pagination

import (
	"fmt"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/poliwag/backend/models"
)

func TestEncode(t *testing.T) {
	sort := "test-sort"
	direction := models.DirectionDescending

	Convey("given key that is nil", t, func() {
		var lastEvaluatedKey map[string]*dynamodb.AttributeValue

		Convey("we should return a blank encoded cursor", func() {
			actualEncodedCursor, err := Encode(sort, direction, lastEvaluatedKey)
			So(actualEncodedCursor, ShouldEqual, "")
			So(err, ShouldBeNil)
		})
	})

	Convey("given pagination token that not nil", t, func() {
		lastEvaluatedKey := map[string]*dynamodb.AttributeValue{
			"test": {
				S: aws.String("sup"),
			},
		}

		Convey("we should return an encoded cursor", func() {
			actualEncodedCursor, err := Encode(sort, direction, lastEvaluatedKey)
			fmt.Println(actualEncodedCursor)
			So(actualEncodedCursor, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestDecode(t *testing.T) {
	sort := "test-sort"
	direction := models.DirectionDescending
	lastEvaluatedKey := map[string]*dynamodb.AttributeValue{
		"test": {
			S: aws.String("sup"),
		},
	}
	createdTime, err := time.Parse(time.RFC3339, "2019-07-08T18:14:32.522604-07:00")
	if err != nil {
		panic(err)
	}

	// This is the encoded cursor with the data above
	encodedCursor := "eyJDcmVhdGVkVGltZSI6IjIwMTktMDctMDhUMTg6MTQ6MzIuNTIyNjA0LTA3OjAwIiwiU29ydCI6InRlc3Qtc29ydCIsIkRpcmVjdGlvbiI6IkRFU0MiLCJMYXN0RXZhbHVhdGVkS2V5Ijp7InRlc3QiOnsiQiI6bnVsbCwiQk9PTCI6bnVsbCwiQlMiOm51bGwsIkwiOm51bGwsIk0iOm51bGwsIk4iOm51bGwsIk5TIjpudWxsLCJOVUxMIjpudWxsLCJTIjoic3VwIiwiU1MiOm51bGx9fX0="

	Convey("pagination token should successfully decode", t, func() {
		expectedRequestCursor := RequestCursor{
			Sort:             sort,
			Direction:        direction,
			LastEvaluatedKey: lastEvaluatedKey,
			CreatedTime:      createdTime,
		}

		actualRequestCursor, err := Decode(encodedCursor)
		So(err, ShouldBeNil)
		So(actualRequestCursor, ShouldResemble, expectedRequestCursor)
	})
}
