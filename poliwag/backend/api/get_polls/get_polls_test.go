package get_polls_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/get_polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	channel_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	get_polls_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestPollAPI_GetPolls(t *testing.T) {
	Convey("given a poll api", t, func() {
		authorizer := new(channel_editor_mock.Authorizer)
		controller := new(get_polls_mock.Controller)

		api := get_polls.NewAPI(authorizer, controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPolls(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPolls(context.Background(), &poliwag.GetPollsRequest{
					UserId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		userID := "test-user-id"

		Convey("when ownedBy is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPolls(context.Background(), &poliwag.GetPollsRequest{
					UserId:  userID,
					OwnedBy: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when status has MODERATED", func() {
			Convey("we should return an error", func() {
				resp, err := api.GetPolls(context.Background(), &poliwag.GetPollsRequest{
					UserId:  userID,
					OwnedBy: userID,
					Status:  []poliwag.PollStatus{poliwag.PollStatus_ACTIVE, poliwag.PollStatus_MODERATED},
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the request passes validation", func() {
			req := &poliwag.GetPollsRequest{
				UserId:  userID,
				OwnedBy: userID,
			}

			Convey("when the authorizer errors", func() {
				authorizer.On("Authorize", mock.Anything, userID, userID).Return(errors.New("test-error"))
				resp, err := api.GetPolls(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when the authorizer succeeds", func() {
				authorizer.On("Authorize", mock.Anything, userID, userID).Return(nil)

				Convey("when GetPolls returns an error", func() {
					controller.On("GetPolls", mock.Anything, req.OwnedBy, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil, nil, nil, nil, errors.New("boom!~"))

					Convey("we should return an error", func() {
						resp, err := api.GetPolls(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})
				})

				Convey("when GetPolls does not return an error", func() {
					poll1 := models.Poll{
						PollID: "poll1",
					}

					pollChoices := []models.Choice{
						{
							Title:    "Final Fantasy VII",
							PollID:   "poll1",
							ChoiceID: "choice-id-0",
						},
						{
							Title:    "Chrono Trigger",
							PollID:   "poll1",
							ChoiceID: "choice-id-1",
						},
					}

					poll2 := models.Poll{
						PollID: "poll2",
					}

					polls := []models.Poll{
						poll1,
						poll2,
					}

					choices := map[string][]models.Choice{
						"1": pollChoices,
						"2": pollChoices,
						"3": pollChoices,
					}

					topBitsMap := map[string]*models.TopBitsContributor{
						poll1.PollID: {
							UserID:          "1234",
							BitsContributed: 10,
						},
						poll2.PollID: {
							UserID:          "5678",
							BitsContributed: 20,
						},
					}

					topChannelPointsMap := map[string]*models.TopChannelPointsContributor{
						poll1.PollID: {
							UserID:                   "5432",
							ChannelPointsContributed: 200,
						},
					}

					controller.On("GetPolls", mock.Anything, req.OwnedBy, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(polls, choices, topBitsMap, topChannelPointsMap, nil, nil)

					Convey("we should return a response", func() {
						resp, err := api.GetPolls(context.Background(), req)
						So(err, ShouldBeNil)
						So(len(resp.Polls), ShouldEqual, 2)
						So(resp.Polls[0].PollId, ShouldEqual, "poll1")
						So(resp.Polls[1].PollId, ShouldEqual, "poll2")
						So(resp.Polls[0].TopBitsContributor, ShouldNotBeNil)
						So(resp.Polls[0].TopChannelPointsContributor, ShouldNotBeNil)
						So(resp.Polls[1].TopBitsContributor, ShouldNotBeNil)
						So(resp.Polls[1].TopChannelPointsContributor, ShouldBeNil)
					})
				})
			})
		})
	})
}
