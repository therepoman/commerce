package get_polls

import (
	"context"
	"fmt"

	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/logrus"
	pagination "code.justin.tv/commerce/poliwag/backend/api/utils"
	"code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const (
	DefaultLimit = 5
	MinLimit     = 1
	MaxLimit     = 20
)

type API interface {
	GetPolls(ctx context.Context, req *poliwag.GetPollsRequest) (*poliwag.GetPollsResponse, error)
}

type api struct {
	authorizer channel_editor.Authorizer
	controller get_polls.Controller
}

func NewAPI(authorizer channel_editor.Authorizer, controller get_polls.Controller) API {
	return &api{
		authorizer: authorizer,
		controller: controller,
	}
}

func (api api) GetPolls(ctx context.Context, req *poliwag.GetPollsRequest) (*poliwag.GetPollsResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userId")
	}

	if req.OwnedBy == "" {
		return nil, twirp.RequiredArgumentError("ownedBy")
	}

	limit := DefaultLimit
	if req.Limit != 0 {
		if req.Limit > MaxLimit {
			return nil, twirp.InvalidArgumentError("limit", fmt.Sprintf("must be less than or equal to %d", MaxLimit))
		}

		if req.Limit < MinLimit {
			return nil, twirp.InvalidArgumentError("limit", fmt.Sprintf("must be greater than or equal to %d", MinLimit))
		}

		limit = int(req.Limit)
	}

	sort, err := proto.ProtoGetPollsSortToPollSort(req.Sort)
	if err != nil {
		return nil, err
	}

	direction, err := proto.ProtoDirectionToDirection(req.Direction)
	if err != nil {
		return nil, err
	}

	var pollsCursor get_polls.PollsCursor
	if req.Cursor != "" {
		requestCursor, err := pagination.Decode(req.Cursor)
		if err != nil {
			return nil, twirp.InvalidArgumentError("cursor", "is not valid")
		}

		sort = models.PollSort(requestCursor.Sort)
		direction = requestCursor.Direction
		pollsCursor = requestCursor.LastEvaluatedKey
	}

	var statuses []models.PollStatus
	if len(req.Status) == 0 {
		statuses = models.BroadcasterViewablePollStatuses
	} else {
		for _, protoStatus := range req.Status {
			if protoStatus == poliwag.PollStatus_MODERATED {
				return nil, twirp.InvalidArgumentError("statuses", fmt.Sprintf("userID %s is not allowed to request for status MODERATED", req.UserId))
			}

			status := models.PollStatus(protoStatus.String())
			statuses = append(statuses, status)
		}
	}

	err = api.authorizer.Authorize(ctx, req.UserId, req.OwnedBy)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	polls, choicesMap, topBitsMap, topChannelPointsMap, newCursor, err := api.controller.GetPolls(ctx, req.OwnedBy, statuses, sort, direction, limit, pollsCursor)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	protoPolls, err := proto.PollsToProtoPolls(polls, choicesMap, topBitsMap, topChannelPointsMap, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling polls to proto polls"
		logrus.WithField("ownedBy", req.OwnedBy).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	encodedRequestCursor, err := pagination.Encode(string(sort), direction, newCursor)
	if err != nil {
		return nil, errors.PoliwagErrorToTwirpError(err)
	}

	return &poliwag.GetPollsResponse{
		Polls:  protoPolls,
		Cursor: encodedRequestCursor,
	}, nil
}
