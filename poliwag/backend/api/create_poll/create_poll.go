package create_poll

import (
	"context"
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/moderation/input_automod"
	"code.justin.tv/commerce/poliwag/backend/utils/proto"
	poliwag_errors "code.justin.tv/commerce/poliwag/proto/errors"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const (
	MinChoiceLength   = 2
	MaxChoiceLength   = 5
	MinDurationLength = 15 * time.Second
	MaxDurationLength = 30 * time.Minute
)

type API interface {
	CreatePoll(ctx context.Context, req *poliwag.CreatePollRequest) (*poliwag.CreatePollResponse, error)
}

type api struct {
	authorizer channel_editor.Authorizer
	moderator  input_automod.Moderator
	controller create_poll.Controller
}

func NewAPI(authorizer channel_editor.Authorizer, moderator input_automod.Moderator, controller create_poll.Controller) API {
	return &api{
		authorizer: authorizer,
		moderator:  moderator,
		controller: controller,
	}
}

func (api api) CreatePoll(ctx context.Context, req *poliwag.CreatePollRequest) (*poliwag.CreatePollResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("userId")
	}

	if req.OwnedBy == "" {
		return nil, twirp.RequiredArgumentError("ownedBy")
	}

	if req.Title == "" {
		return nil, twirp.RequiredArgumentError("title")
	}

	if req.Duration == nil {
		return nil, twirp.RequiredArgumentError("duration")
	}

	duration, err := ptypes.Duration(req.Duration)
	if err != nil {
		return nil, twirp.InvalidArgumentError("duration", "is not valid")
	}

	if duration.Seconds() < MinDurationLength.Seconds() {
		return nil, twirp.InvalidArgumentError("duration", "must have a length greater than 15 seconds")
	}

	if duration.Seconds() > MaxDurationLength.Seconds() {
		return nil, twirp.InvalidArgumentError("duration", "must have a length less than 30 minutes")
	}

	if req.Choices == nil {
		return nil, twirp.RequiredArgumentError("choices")
	}

	if len(req.Choices) < MinChoiceLength {
		return nil, twirp.InvalidArgumentError("choices", fmt.Sprintf("must have a length greater than or equal to %d", MinChoiceLength))
	}

	if len(req.Choices) > MaxChoiceLength {
		return nil, twirp.InvalidArgumentError("choices", fmt.Sprintf("must have a length less than or equal to %d", MaxChoiceLength))
	}

	var createPollChoices []create_poll.CreateChoiceArgs
	for _, choice := range req.Choices {
		if choice == nil {
			return nil, twirp.InvalidArgumentError("choices", "must not have null values")
		}

		if choice.Title == "" {
			return nil, twirp.RequiredArgumentError("choices.title")
		}

		createPollChoices = append(createPollChoices, create_poll.CreateChoiceArgs{
			Title: choice.Title,
		})
	}

	if req.GetSettings().GetBitsVotes().GetEnabled() && (req.GetSettings().GetBitsVotes().GetCost() <= 0 || req.GetSettings().GetBitsVotes().GetCost() > proto.MaxBitsPerVote) {
		return nil, twirp.InvalidArgumentError("settings.bits_votes.cost", fmt.Sprintf("bits cost must be between 0 and %d", proto.MaxBitsPerVote))
	}

	if req.GetSettings().GetChannelPointsVotes().GetEnabled() && (req.GetSettings().GetChannelPointsVotes().GetCost() <= 0 || req.GetSettings().GetChannelPointsVotes().GetCost() > proto.MaxChannelPointsPerVote) {
		return nil, twirp.InvalidArgumentError("settings.channel_points_votes.cost", fmt.Sprintf("channel points cost must be between 0 and %d", proto.MaxChannelPointsPerVote))
	}

	err = api.authorizer.Authorize(ctx, req.UserId, req.OwnedBy)
	if err != nil {
		return nil, errors.PoliwagCreateErrorToTwirpError(err)
	}

	if err := api.moderator.ValidateInput(ctx, req.UserId, req.Title); err != nil {
		errorResponse := poliwag_errors.PoliwagError{
			ErrorCode: poliwag.PollCreationErrorCode_AUTOMOD_FAILED.String(),
		}
		return nil, poliwag_errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	}

	for _, choice := range req.Choices {
		if err = api.moderator.ValidateInput(ctx, req.UserId, choice.Title); err != nil {
			errorResponse := poliwag_errors.PoliwagError{
				ErrorCode: poliwag.PollCreationErrorCode_AUTOMOD_FAILED.String(),
			}
			return nil, poliwag_errors.NewPoliwagError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
		}
	}

	newPoll, newChoices, err := api.controller.CreatePoll(ctx, req.UserId, req.OwnedBy,
		req.Title, duration, createPollChoices,
		req.GetSettings().GetMultiChoice().GetEnabled(),
		req.Settings.GetSubscriberOnly().GetEnabled(), req.Settings.GetSubscriberMultiplier().GetEnabled(),
		req.Settings.GetBitsVotes().GetEnabled(), int(req.Settings.GetBitsVotes().GetCost()),
		req.Settings.GetChannelPointsVotes().GetEnabled(), int(req.Settings.GetChannelPointsVotes().GetCost()))
	if err != nil {
		return nil, errors.PoliwagCreateErrorToTwirpError(err)
	}

	newProtoPoll, err := proto.PollToProtoPoll(newPoll, newChoices, nil, nil, models.ViewableDuration)
	if err != nil {
		msg := "error marshalling poll to proto poll"
		logrus.WithField("pollID", newPoll.PollID).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &poliwag.CreatePollResponse{
		Poll: &newProtoPoll,
	}, nil
}
