package create_poll_test

import (
	"context"
	"errors"
	"testing"

	protoduration "github.com/golang/protobuf/ptypes/duration"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/poliwag/backend/api/create_poll"
	controller_create_poll "code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll"
	"code.justin.tv/commerce/poliwag/backend/models"
	channel_editor_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	create_poll_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll"
	poll_input_moderator_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/moderation/input_automod"
	users_client_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

func TestApi_CreatePoll(t *testing.T) {
	Convey("given a poll api", t, func() {
		authorizer := new(channel_editor_mock.Authorizer)
		moderator := new(poll_input_moderator_mock.Moderator)
		controller := new(create_poll_mock.Controller)
		usersClient := new(users_client_mock.InternalClient)
		usersClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

		api := create_poll.NewAPI(authorizer, moderator, controller)

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		userID := "test-user-id"

		Convey("when ownedBy is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId:  userID,
					OwnedBy: "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		ownedBy := "test-owned-by"

		Convey("when title is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId:  userID,
					OwnedBy: ownedBy,
					Title:   "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		title := "test-title"

		Convey("when duration is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: nil,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		duration := &protoduration.Duration{Seconds: 300}

		Convey("when choices is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  nil,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		var choices []*poliwag.CreatePollRequest_CreatePollChoice

		Convey("when there are less than 2 choices", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		choices = []*poliwag.CreatePollRequest_CreatePollChoice{
			{
				Title: "Final Fantasy I",
			},
			{
				Title: "Final Fantasy II",
			},
			{
				Title: "Final Fantasy III",
			},
			{
				Title: "Final Fantasy IV",
			},
			{
				Title: "Final Fantasy V",
			},
			{
				Title: "Final Fantasy VI",
			},
		}

		Convey("when there are more than 5 choices", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
					UserId:   userID,
					OwnedBy:  ownedBy,
					Title:    title,
					Duration: duration,
					Choices:  choices,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		choices = []*poliwag.CreatePollRequest_CreatePollChoice{
			{
				Title: "Final Fantasy VII",
			},
			{
				Title: "Chrono Trigger",
			},
		}

		Convey("when bits votes is enabled", func() {
			isGetBitsVotesEnabled := true

			Convey("when bits votes cost is 0", func() {
				bitsVotesCost := int64(0)

				Convey("we should return an error", func() {
					resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
						UserId:   userID,
						OwnedBy:  ownedBy,
						Title:    title,
						Duration: duration,
						Choices:  choices,
						Settings: &poliwag.PollSettings{
							BitsVotes: &poliwag.PollSettings_BitsVotesPollSetting{
								Enabled: isGetBitsVotesEnabled,
								Cost:    bitsVotesCost,
							},
						},
					})
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when bits votes cost is 10001", func() {
				bitsVotesCost := int64(10001)

				Convey("we should return an error", func() {
					resp, err := api.CreatePoll(context.Background(), &poliwag.CreatePollRequest{
						UserId:   userID,
						OwnedBy:  ownedBy,
						Title:    title,
						Duration: duration,
						Choices:  choices,
						Settings: &poliwag.PollSettings{
							BitsVotes: &poliwag.PollSettings_BitsVotesPollSetting{
								Enabled: isGetBitsVotesEnabled,
								Cost:    bitsVotesCost,
							},
						},
					})
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})
		})

		Convey("when the request passes validation", func() {
			req := &poliwag.CreatePollRequest{
				UserId:   userID,
				OwnedBy:  ownedBy,
				Title:    title,
				Duration: duration,
				Choices:  choices,
			}

			createPollChoices := []controller_create_poll.CreateChoiceArgs{
				{
					Title: "Final Fantasy VII",
				},
				{
					Title: "Chrono Trigger",
				},
			}

			Convey("When the authorizer returns an error", func() {
				authorizer.On("Authorize", mock.Anything, req.UserId, req.OwnedBy).Return(errors.New("test-error"))

				Convey("we should return an error", func() {
					resp, err := api.CreatePoll(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("When the moderator returns an error", func() {
				authorizer.On("Authorize", mock.Anything, req.UserId, req.OwnedBy).Return(nil)
				moderator.On("ValidateInput", mock.Anything, req.UserId, mock.Anything).Return(errors.New("test-error"))

				Convey("we should return an error", func() {
					resp, err := api.CreatePoll(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("When the authorizer and moderator succeeds", func() {
				authorizer.On("Authorize", mock.Anything, req.UserId, req.OwnedBy).Return(nil)
				moderator.On("ValidateInput", mock.Anything, req.UserId, mock.Anything).Return(nil)

				Convey("when CreatePoll returns an error", func() {
					controller.On("CreatePoll", mock.Anything, req.UserId, req.OwnedBy, req.Title, mock.Anything, createPollChoices, false, false, false, false, 0, false, 0).Return(models.Poll{}, nil, errors.New("ERROR"))

					Convey("we should return an error", func() {
						resp, err := api.CreatePoll(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})
				})

				Convey("when CreatePoll does not return an error", func() {
					pollID := "test-poll-id"
					newPoll := models.Poll{
						PollID: pollID,
					}
					newChoices := []models.Choice{
						{
							Title:    "Final Fantasy VII",
							PollID:   pollID,
							ChoiceID: "choice-id-0",
						},
						{
							Title:    "Chrono Trigger",
							PollID:   pollID,
							ChoiceID: "choice-id-1",
						},
					}

					controller.On("CreatePoll", mock.Anything, req.UserId, req.OwnedBy, req.Title, mock.Anything, createPollChoices, false, false, false, false, 0, false, 0).Return(newPoll, newChoices, nil)

					Convey("we should return a response", func() {
						resp, err := api.CreatePoll(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.Poll.PollId, ShouldEqual, pollID)
					})
				})
			})
		})
	})
}
