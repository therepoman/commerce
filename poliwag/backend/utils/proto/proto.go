package proto

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/duration"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/joomcode/errorx"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/proto/poliwag"
)

const (
	MaxBitsPerVote          = 10000
	MaxChannelPointsPerVote = 1000000
)

func PollToProtoPoll(poll models.Poll, choices []models.Choice, topBitsContributor *models.TopBitsContributor, topChannelPointsContributor *models.TopChannelPointsContributor, viewableDuration time.Duration) (poliwag.Poll, error) {
	protoStartTime, err := ptypes.TimestampProto(poll.StartTime)
	if err != nil {
		return poliwag.Poll{}, err
	}

	protoDuration := ptypes.DurationProto(poll.Duration)

	var protoEndTime *timestamp.Timestamp
	var protoRemainingDuration *duration.Duration
	if poll.EndTime != nil {
		protoEndTime, err = ptypes.TimestampProto(*poll.EndTime)
		if err != nil {
			return poliwag.Poll{}, err
		}

		protoRemainingDuration = ptypes.DurationProto(time.Duration(0))
	} else {
		expectedEndTime := poll.StartTime.Add(poll.Duration)
		remainingDuration := expectedEndTime.Sub(time.Now())

		// Polls are completed asynchronously, so it's possible to get a negative remainingDuration here
		// In this scenario, it should be zero, so let's account for that
		remainingDuration = time.Duration(math.MaxInt64(int64(remainingDuration), 0))

		protoRemainingDuration = ptypes.DurationProto(remainingDuration)
	}

	var top *poliwag.TopContributor
	var topBits *poliwag.TopBitsContributor
	if topBitsContributor != nil {
		top = &poliwag.TopContributor{
			UserId:          topBitsContributor.UserID,
			BitsContributed: int64(topBitsContributor.BitsContributed),
		}
		topBits = &poliwag.TopBitsContributor{
			UserId:          topBitsContributor.UserID,
			BitsContributed: int64(topBitsContributor.BitsContributed),
		}

		if topBitsContributor.DisplayName != nil {
			top.DisplayName = *topBitsContributor.DisplayName
			topBits.DisplayName = *topBitsContributor.DisplayName
		}
	}

	var topChannelPoints *poliwag.TopChannelPointsContributor
	if topChannelPointsContributor != nil {
		topChannelPoints = &poliwag.TopChannelPointsContributor{
			UserId:                   topChannelPointsContributor.UserID,
			ChannelPointsContributed: int64(topChannelPointsContributor.ChannelPointsContributed),
		}

		if topChannelPointsContributor.DisplayName != nil {
			topChannelPoints.DisplayName = *topChannelPointsContributor.DisplayName
		}
	}

	var totalVotes int
	var totalBitsVotes int
	var totalChannelPointsVotes int
	var totalBaseVotes int
	var totalBits int
	var totalChannelPoints int

	for _, choice := range choices {
		totalVotes += choice.TotalVotes
		totalBitsVotes += models.GetTotalBitsVotesForChoice(poll, choice)
		totalChannelPointsVotes += models.GetTotalChannelPointsVotesForChoice(poll, choice)
		totalBaseVotes += choice.TotalBaseVotes
		totalBits += choice.TotalBits
		totalChannelPoints += choice.TotalChannelPoints
	}

	return poliwag.Poll{
		PollId:            poll.PollID,
		OwnedBy:           poll.OwnedBy,
		CreatedBy:         poll.CreatedBy,
		Title:             poll.Title,
		StartTime:         protoStartTime,
		EndTime:           protoEndTime,
		EndedBy:           poll.EndedBy,
		Duration:          protoDuration,
		RemainingDuration: protoRemainingDuration,
		Choices:           ChoicesToProtoChoices(poll, choices),
		Settings: &poliwag.PollSettings{
			MultiChoice: &poliwag.PollSettings_MultiChoicePollSetting{
				Enabled: poll.IsMultiChoiceEnabled,
			},
			SubscriberOnly: &poliwag.PollSettings_SettingsSubscriberOnlyPollSetting{
				Enabled: poll.IsSubscriberOnlyEnabled,
			},
			SubscriberMultiplier: &poliwag.PollSettings_SubscriberMultiplierPollSetting{
				Enabled: poll.IsSubscriberMultiplierEnabled,
			},
			BitsVotes: &poliwag.PollSettings_BitsVotesPollSetting{
				Enabled: poll.IsBitsVotesEnabled,
				Cost:    int64(poll.BitsVotesCost),
			},
			ChannelPointsVotes: &poliwag.PollSettings_ChannelPointsVotesPollSetting{
				Enabled: poll.IsChannelPointsVotesEnabled,
				Cost:    int64(poll.ChannelPointsVotesCost),
			},
		},
		Status:                      PollStatusToProtoPollStatus(poll.Status),
		TotalVoters:                 int64(poll.TotalVoters),
		Votes:                       VotesToProtoVotes(totalVotes, totalBitsVotes, totalChannelPointsVotes, totalBaseVotes),
		Tokens:                      TokensToProtoTokens(totalBits, totalChannelPoints),
		IsViewable:                  models.IsPollViewable(poll, time.Now(), viewableDuration),
		TopContributor:              top,
		TopBitsContributor:          topBits,
		TopChannelPointsContributor: topChannelPoints,
	}, nil
}

func PollsToProtoPolls(polls []models.Poll, choices map[string][]models.Choice,
	topBitsMap map[string]*models.TopBitsContributor, topChannelPointsMap map[string]*models.TopChannelPointsContributor,
	viewableDuration time.Duration) ([]*poliwag.Poll, error) {
	var protoPolls []*poliwag.Poll
	for _, poll := range polls {
		protoPoll, err := PollToProtoPoll(poll, choices[poll.PollID], topBitsMap[poll.PollID], topChannelPointsMap[poll.PollID], viewableDuration)
		if err != nil {
			return nil, errorx.Decorate(err, "error marshalling poll to proto poll %s", poll.PollID)
		}

		protoPolls = append(protoPolls, &protoPoll)
	}

	return protoPolls, nil
}

func ChoicesToProtoChoices(poll models.Poll, choices []models.Choice) []*poliwag.Choice {
	var protoChoices []*poliwag.Choice

	for _, choice := range choices {
		protoChoice := &poliwag.Choice{
			ChoiceId:    choice.ChoiceID,
			Title:       choice.Title,
			TotalVoters: int64(choice.TotalVoters),
			Votes: VotesToProtoVotes(choice.TotalVotes,
				models.GetTotalBitsVotesForChoice(poll, choice),
				models.GetTotalChannelPointsVotesForChoice(poll, choice),
				choice.TotalBaseVotes),
			Tokens: TokensToProtoTokens(choice.TotalBits, choice.TotalChannelPoints),
		}

		protoChoices = append(protoChoices, protoChoice)
	}

	return protoChoices
}

func PollStatusToProtoPollStatus(pollStatus models.PollStatus) poliwag.PollStatus {
	switch pollStatus {
	case models.PollStatusActive:
		return poliwag.PollStatus_ACTIVE
	case models.PollStatusCompleted:
		return poliwag.PollStatus_COMPLETED
	case models.PollStatusTerminated:
		return poliwag.PollStatus_TERMINATED
	case models.PollStatusArchived:
		return poliwag.PollStatus_ARCHIVED
	case models.PollStatusModerated:
		return poliwag.PollStatus_MODERATED
	default:
		return poliwag.PollStatus(-1)
	}
}

func VotesToProtoVotes(totalVotes int, bitsVotes int, channelPointsVotes int, baseVotes int) *poliwag.Votes {
	return &poliwag.Votes{
		Total:         int64(totalVotes),
		Bits:          int64(bitsVotes),
		ChannelPoints: int64(channelPointsVotes),
		Base:          int64(baseVotes),
	}
}

func TokensToProtoTokens(bits, channelPoints int) *poliwag.Tokens {
	return &poliwag.Tokens{
		Bits:          int64(bits),
		ChannelPoints: int64(channelPoints),
	}
}

func ProtoGetPollsSortToPollSort(sort poliwag.GetPollsSort) (models.PollSort, error) {
	switch sort {
	case poliwag.GetPollsSort_START_TIME:
		return models.PollSortStartTime, nil
	default:
		return models.PollSort(fmt.Sprint(-1)), errors.New("invalid sort")
	}
}

func ProtoDirectionToDirection(direction poliwag.Direction) (models.Direction, error) {
	switch direction {
	case poliwag.Direction_DESC:
		return models.DirectionDescending, nil
	case poliwag.Direction_ASC:
		return models.DirectionAscending, nil
	default:
		return models.Direction(fmt.Sprint(-1)), errors.New("invalid direction")
	}
}

func ProtoVoterSortToVoterSort(sort poliwag.VoterSort) (models.VoterSort, error) {
	switch sort {
	case poliwag.VoterSort_VOTES:
		return models.VoterSortVotes, nil
	case poliwag.VoterSort_BITS:
		return models.VoterSortBits, nil
	case poliwag.VoterSort_CHANNEL_POINTS:
		return models.VoterSortChannelPoints, nil
	default:
		return models.VoterSort(fmt.Sprint(-1)), errors.New("invalid sort")
	}
}

func VotersToProtoVoters(poll models.Poll, voters []models.Voter) []*poliwag.Voter {
	var protoVoters []*poliwag.Voter

	for _, voter := range voters {
		protoVoter := VoterToProtoVoter(poll, voter)
		protoVoters = append(protoVoters, &protoVoter)
	}

	return protoVoters
}

func VoterToProtoVoter(poll models.Poll, voter models.Voter) poliwag.Voter {
	return poliwag.Voter{
		PollId:  voter.PollID,
		UserId:  voter.UserID,
		Choices: VoterChoicesToProtoVoterChoices(poll, voter.Choices),
		Votes: VotesToProtoVotes(voter.TotalVotes,
			models.GetTotalBitsVotesForVoter(poll, voter),
			models.GetTotalChannelPointsVotesForVoter(poll, voter),
			voter.TotalBaseVotes),
		Tokens: TokensToProtoTokens(voter.TotalBits, voter.TotalChannelPoints),
		// TODO: Add IsSubscriber
	}
}

func VoterChoicesToProtoVoterChoices(poll models.Poll, voterChoices []models.VoterChoice) []*poliwag.VoterChoice {
	var protoVoterChoices []*poliwag.VoterChoice
	for _, voterChoice := range voterChoices {
		protoVoterChoices = append(protoVoterChoices, &poliwag.VoterChoice{
			ChoiceId: voterChoice.ChoiceID,
			Votes: VotesToProtoVotes(voterChoice.TotalVotes,
				models.GetTotalBitsVotesForVoterChoice(poll, voterChoice),
				models.GetTotalChannelPointsVotesForVoterChoice(poll, voterChoice),
				voterChoice.TotalBaseVotes),
			Tokens: TokensToProtoTokens(voterChoice.TotalBits, voterChoice.TotalChannelPoints),
		})
	}

	return protoVoterChoices
}

func TokensProtoToTokenMap(tokens *poliwag.Tokens) (map[models.TokenType]int64, error) {
	tokenMap := make(map[models.TokenType]int64)
	if tokens != nil {
		if tokens.Bits < 0 {
			return nil, twirp.NewError(twirp.InvalidArgument, "bits tokens cannot be negative")
		} else if tokens.Bits > MaxBitsPerVote {
			return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("cannot use more than %d bits tokens per vote", MaxBitsPerVote))
		} else {
			tokenMap[models.TokenTypeBits] = tokens.Bits
		}

		if tokens.ChannelPoints < 0 {
			return nil, twirp.NewError(twirp.InvalidArgument, "channel point tokens cannot be negative")
		} else if tokens.ChannelPoints > MaxChannelPointsPerVote {
			return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("cannot use more than %d channel points tokens per vote", MaxChannelPointsPerVote))
		} else {
			tokenMap[models.TokenTypeChannelPoints] = tokens.ChannelPoints
		}
	}
	return tokenMap, nil
}
