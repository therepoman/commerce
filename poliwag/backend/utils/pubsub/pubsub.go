package pubsub

import (
	"time"

	"code.justin.tv/commerce/gogogadget/math"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/models"
)

func PollToPubsubPoll(poll models.Poll, choices []models.Choice,
	topBitsContributor *models.TopBitsContributor,
	topChannelPointsContributor *models.TopChannelPointsContributor) models.PubsubPoll {
	var endedAt *string
	var remainingDurationMilliseconds int
	if poll.EndTime != nil {
		endedAt = pointers.StringP(poll.EndTime.Format(time.RFC3339Nano))
		remainingDurationMilliseconds = 0
	} else {
		expectedEndTime := poll.StartTime.Add(poll.Duration)
		remainingDuration := expectedEndTime.Sub(time.Now())

		// Polls are completed asynchronously, so it's possible to get a negative remainingDuration here
		// In this scenario, it should be zero, so let's account for that
		remainingDuration = time.Duration(math.MaxInt64(int64(remainingDuration), 0))

		remainingDurationMilliseconds = int(remainingDuration.Milliseconds())
	}

	var endedBy *string
	if poll.EndedBy != "" {
		endedBy = &poll.EndedBy
	}

	var totalVotes int
	var totalBitsVotes int
	var totalChannelPointsVotes int
	var totalBaseVotes int
	var totalBits int
	var totalChannelPoints int

	for _, choice := range choices {
		totalVotes += choice.TotalVotes
		totalBitsVotes += models.GetTotalBitsVotesForChoice(poll, choice)
		totalChannelPointsVotes += models.GetTotalChannelPointsVotesForChoice(poll, choice)
		totalBaseVotes += choice.TotalBaseVotes
		totalBits += choice.TotalBits
		totalChannelPoints += choice.TotalChannelPoints
	}

	var topBits *models.PubsubTopBitsContributor
	if topBitsContributor != nil {
		topBits = &models.PubsubTopBitsContributor{
			UserID:          topBitsContributor.UserID,
			BitsContributed: topBitsContributor.BitsContributed,
		}

		if topBitsContributor.DisplayName != nil {
			topBits.DisplayName = *topBitsContributor.DisplayName
		}
	}

	var topChannelPoints *models.PubsubTopChannelPointsContributor
	if topChannelPointsContributor != nil {
		topChannelPoints = &models.PubsubTopChannelPointsContributor{
			UserID:                   topChannelPointsContributor.UserID,
			ChannelPointsContributed: topChannelPointsContributor.ChannelPointsContributed,
		}

		if topChannelPointsContributor.DisplayName != nil {
			topChannelPoints.DisplayName = *topChannelPointsContributor.DisplayName
		}
	}

	return models.PubsubPoll{
		PollID:                        poll.PollID,
		OwnedBy:                       poll.OwnedBy,
		CreatedBy:                     poll.CreatedBy,
		Title:                         poll.Title,
		StartedAt:                     poll.StartTime.Format(time.RFC3339Nano),
		EndedAt:                       endedAt,
		EndedBy:                       endedBy,
		DurationSeconds:               int(poll.Duration.Seconds()),
		Settings:                      SettingsToPubsubPollSettings(poll),
		Status:                        poll.Status,
		Choices:                       ChoicesToPubsubChoices(poll, choices),
		Votes:                         VotesToPubsubVotes(totalVotes, totalBitsVotes, totalChannelPointsVotes, totalBaseVotes),
		Tokens:                        TokensToPubsubTokens(totalBits, totalChannelPoints),
		TotalVoters:                   poll.TotalVoters,
		RemainingDurationMilliseconds: remainingDurationMilliseconds,
		TopContributor:                topBits,
		TopBitsContributor:            topBits,
		TopChannelPointsContributor:   topChannelPoints,
	}
}

func SettingsToPubsubPollSettings(poll models.Poll) models.PubsubPollSettings {
	return models.PubsubPollSettings{
		MultiChoice: models.PubsubPollSettingsMultiChoice{
			IsEnabled: poll.IsMultiChoiceEnabled,
		},
		SubscriberOnly: models.PubsubPollSettingsSubscriberOnly{
			IsEnabled: poll.IsSubscriberOnlyEnabled,
		},
		SubscriberMultiplier: models.PubsubPollSettingsSubscriberMultiplier{
			IsEnabled: poll.IsSubscriberMultiplierEnabled,
		},
		BitsVotes: models.PubsubPollSettingsBitsVotes{
			IsEnabled: poll.IsBitsVotesEnabled,
			Cost:      poll.BitsVotesCost,
		},
		ChannelPointsVotes: models.PubsubPollSettingsChannelPointsVotes{
			IsEnabled: poll.IsChannelPointsVotesEnabled,
			Cost:      poll.ChannelPointsVotesCost,
		},
	}
}

func ChoicesToPubsubChoices(poll models.Poll, choices []models.Choice) []models.PubsubChoice {
	var pubsubChoices []models.PubsubChoice
	for _, choice := range choices {
		pubsubChoices = append(pubsubChoices, models.PubsubChoice{
			ChoiceID: choice.ChoiceID,
			Title:    choice.Title,
			Votes: VotesToPubsubVotes(choice.TotalVotes,
				models.GetTotalBitsVotesForChoice(poll, choice),
				models.GetTotalChannelPointsVotesForChoice(poll, choice),
				choice.TotalBaseVotes),
			Tokens:      TokensToPubsubTokens(choice.TotalBits, choice.TotalChannelPoints),
			TotalVoters: choice.TotalVoters,
		})
	}

	return pubsubChoices
}

func VotesToPubsubVotes(totalVotes, bitsVotes, channelPointsVotes, baseVotes int) models.PubsubVotes {
	return models.PubsubVotes{
		Total:         totalVotes,
		Bits:          bitsVotes,
		ChannelPoints: channelPointsVotes,
		Base:          baseVotes,
	}
}

func TokensToPubsubTokens(bits, channelPoints int) models.PubsubTokens {
	return models.PubsubTokens{
		Bits:          bits,
		ChannelPoints: channelPoints,
	}
}
