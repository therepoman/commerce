package pubsub_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/utils/pubsub"
)

func TestChoicesToPubsubChoices(t *testing.T) {
	const (
		pollID = "TestPollPlzIgnore"

		bitsVotersOver  = 60
		bitsVotersUnder = 40
		copoVotersOver  = 250
		copoVotersUnder = 150
		baseVotersOver  = 20
		baseVotersUnder = 30

		bitsVotesOver  = 90
		bitsVotesUnder = 60
		copoVotesOver  = 375
		copoVotesUnder = 225
		baseVotesOver  = 30
		baseVotesUnder = 45

		bitsCost = 10
		copoCost = 100
	)

	poll := models.Poll{
		PollID:                 pollID,
		BitsVotesCost:          bitsCost,
		ChannelPointsVotesCost: copoCost,
		TotalVoters:            bitsVotersOver + copoVotersOver + baseVotesOver + bitsVotersUnder + copoVotersUnder + baseVotesUnder,
		TotalVotes:             bitsVotesOver + copoVotesOver + baseVotesOver + bitsVotesUnder + copoVotesUnder + baseVotesUnder,
		TotalBaseVotes:         baseVotesOver + baseVotesUnder,
		TotalBits:              (bitsVotesOver + bitsVotesUnder) * bitsCost,
		TotalChannelPoints:     (copoVotesOver + copoVotesUnder) * copoCost,
	}
	choiceOver := models.Choice{
		ChoiceID:           "ChoiceOver",
		PollID:             pollID,
		Title:              "Over",
		TotalVoters:        bitsVotersOver + copoVotersOver + baseVotersOver,
		TotalVotes:         bitsVotesOver + copoVotesOver + baseVotesOver,
		TotalBaseVotes:     baseVotesOver,
		TotalBits:          bitsVotesOver * bitsCost,
		TotalChannelPoints: copoVotesOver * copoCost,
	}
	choiceUnder := models.Choice{
		ChoiceID:           "ChoiceUnder",
		PollID:             pollID,
		Title:              "Under",
		TotalVoters:        bitsVotersUnder + copoVotersUnder + baseVotersUnder,
		TotalVotes:         bitsVotesUnder + copoVotesUnder + baseVotesUnder,
		TotalBaseVotes:     baseVotesUnder,
		TotalBits:          bitsVotesUnder * bitsCost,
		TotalChannelPoints: copoVotesUnder * copoCost,
	}

	result := pubsub.ChoicesToPubsubChoices(poll, []models.Choice{choiceOver, choiceUnder})

	assert.Len(t, result, 2)
	assert.Equal(t, models.PubsubChoice{
		ChoiceID: choiceOver.ChoiceID,
		Title:    choiceOver.Title,
		Votes: models.PubsubVotes{
			Total:         bitsVotesOver + copoVotesOver + baseVotesOver,
			Bits:          bitsVotesOver,
			ChannelPoints: copoVotesOver,
			Base:          baseVotesOver,
		},
		Tokens: models.PubsubTokens{
			Bits:          bitsVotesOver * bitsCost,
			ChannelPoints: copoVotesOver * copoCost,
		},
		TotalVoters: bitsVotersOver + copoVotersOver + baseVotersOver,
	}, result[0])
	assert.Equal(t, models.PubsubChoice{
		ChoiceID: choiceUnder.ChoiceID,
		Title:    choiceUnder.Title,
		Votes: models.PubsubVotes{
			Total:         bitsVotesUnder + copoVotesUnder + baseVotesUnder,
			Bits:          bitsVotesUnder,
			ChannelPoints: copoVotesUnder,
			Base:          baseVotesUnder,
		},
		Tokens: models.PubsubTokens{
			Bits:          bitsVotesUnder * bitsCost,
			ChannelPoints: copoVotesUnder * copoCost,
		},
		TotalVoters: bitsVotersUnder + copoVotersUnder + baseVotersUnder,
	}, result[1])
}
