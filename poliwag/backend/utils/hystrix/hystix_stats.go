package hystrix

import (
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"
)

// HystrixMetricsCollector holds information about the circuit state.
// This implementation of MetricCollector is the canonical source of information about the circuit.
// It is used for for all internal hystrix operations
// including circuit health checks and metrics sent to the hystrix dashboard.
//
// Metric Collectors do not need Mutexes as they are updated by circuits within a locked context.
type HystrixMetricsCollector struct {
	stats statsd.Statter
}

func NewHystrixMetricsCollector(stats statsd.Statter) *HystrixMetricsCollector {
	return &HystrixMetricsCollector{
		stats: stats,
	}
}

type HystrixCommandMetricsCollector struct {
	name string

	attemptsMetric          string
	errorsMetric            string
	successesMetric         string
	failuresMetric          string
	rejectsMetric           string
	shortCircuitsMetric     string
	timeoutsMetric          string
	fallbackSuccessesMetric string
	fallbackFailuresMetric  string
	contextCancelledMetric  string
	contextDeadlineMetric   string
	totalDurationMetric     string
	runDurationMetric       string

	Statter statsd.Statter
}

func (h *HystrixMetricsCollector) NewHystrixCommandMetricsCollector(name string) metricCollector.MetricCollector {
	statsPrefix := "hystrix." + name

	return &HystrixCommandMetricsCollector{
		name:                    name,
		attemptsMetric:          statsPrefix + ".attempts",
		errorsMetric:            statsPrefix + ".errors",
		failuresMetric:          statsPrefix + ".failures",
		rejectsMetric:           statsPrefix + ".rejects",
		shortCircuitsMetric:     statsPrefix + ".shortCircuits",
		timeoutsMetric:          statsPrefix + ".timeouts",
		fallbackSuccessesMetric: statsPrefix + ".fallbackSuccesses",
		fallbackFailuresMetric:  statsPrefix + ".fallbackFailures",
		contextCancelledMetric:  statsPrefix + ".contextCancelled",
		contextDeadlineMetric:   statsPrefix + ".contextDeadlineExceeded",
		totalDurationMetric:     statsPrefix + ".totalDuration",
		runDurationMetric:       statsPrefix + ".runDuration",
		Statter:                 h.stats,
	}
}

func (h *HystrixCommandMetricsCollector) Update(r metricCollector.MetricResult) {
	go func() {
		h.doInc(h.attemptsMetric, int64(r.Attempts))
		h.doInc(h.errorsMetric, int64(r.Errors))

		h.doInc(h.successesMetric, int64(r.Successes))
		h.doInc(h.failuresMetric, int64(r.Failures))
		h.doInc(h.rejectsMetric, int64(r.Rejects))
		h.doInc(h.shortCircuitsMetric, int64(r.ShortCircuits))
		h.doInc(h.timeoutsMetric, int64(r.Timeouts))
		h.doInc(h.fallbackSuccessesMetric, int64(r.FallbackSuccesses))
		h.doInc(h.fallbackFailuresMetric, int64(r.FallbackFailures))
		h.doInc(h.contextCancelledMetric, int64(r.ContextCanceled))
		h.doInc(h.contextDeadlineMetric, int64(r.ContextDeadlineExceeded))

		h.doInc(h.totalDurationMetric, int64(r.TotalDuration))
		h.doInc(h.runDurationMetric, int64(r.RunDuration))
	}()
}

func (d *HystrixCommandMetricsCollector) doInc(metric string, value int64) {
	statErr := d.Statter.Inc(metric, value, 1)
	if statErr != nil {
		logrus.WithError(statErr).Error("error logging metric")
	}
}

// It's a noop because it's all set remotely
func (d *HystrixCommandMetricsCollector) Reset() {}
