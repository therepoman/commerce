package hystrix

import "github.com/afex/hystrix-go/hystrix"

const (
	// zuma
	ZumaGetModCommand         = "zuma_get_mod"
	ZumaEnforceMessageCommand = "zuma_enforce_message"
	// hallpass
	HallPassGetV1sEditorCommand = "hallpass_get_editor"
	// users
	UsersGetUserByIDCommand = "users_get_user_by_id"
	// pubsub
	PubsubPublishCommand = "pubsub_publish_command"
	// eventbus
	EventbusPublish = "eventbus_publish"
	// payday
	PaydayUseBitsOnPollCommand  = "payday_use_bits_on_poll"
	PaydayGetChannelInfoCommand = "payday_get_channel_info"
	// copo
	CopoGetChannelSettings = "copo_get_channel_settings"
	CopoSpendPoints        = "copo_spend_points"
	// ripley
	RipleyGetPayoutTypeCommand = "ripley_get_payout_type"
	// subscriptions
	SubscriptionsGetUserChannelSubscriptionCommand = "subs_get_user_channel_subscriptions"
	// redis
	ViewablePollCacheRedisGet = "viewable_poll_cache_get"
	ViewablePollCacheRedisSet = "viewable_poll_cache_set"
	// tmi
	TMIGetBanStatus = "tmi_get_ban_status"
	// votes dao
	VotesGetVote               = "votes_get_vote"
	VotesCreateVote            = "votes_create_vote"
	VotesDeleteVote            = "votes_delete_vote"
	VotesGetChoiceAggregations = "votes_get_choice_aggregations"
	VotesGetPollAggregation    = "votes_get_poll_aggregation"
	// Max concurrency per command
	DefaultMaxConcurrent = 1000
)

func init() {
	// zuma
	configureTimeout(ZumaGetModCommand, 500)
	configureTimeout(ZumaEnforceMessageCommand, 500)
	// hallpass
	configureTimeout(HallPassGetV1sEditorCommand, 5000) // 5 seconds???
	// users
	configureTimeout(UsersGetUserByIDCommand, 500)
	// pubsub
	configureTimeout(PubsubPublishCommand, 1000)
	// eventbus
	configureTimeout(EventbusPublish, 1000)
	// payday
	configureTimeout(PaydayUseBitsOnPollCommand, 6000)
	configureTimeout(PaydayGetChannelInfoCommand, 500) // p99-10ms, 4 week max-480ms (as of 8/24/19)
	// ripley
	configureTimeout(RipleyGetPayoutTypeCommand, 500)
	// subscriptions
	configureTimeout(SubscriptionsGetUserChannelSubscriptionCommand, 500)
	// redis
	configure(ViewablePollCacheRedisGet, 100, 500) // Part of GetViewablePoll API, which can get very high concurrent TPS
	configure(ViewablePollCacheRedisSet, 100, 500) // Part of GetViewablePoll API, which can get very high concurrent TPS
	// tmi
	configure(TMIGetBanStatus, 500, 2500) // Part of Vote API validation, which can get high concurrent TPS
	// copo
	configureTimeout(CopoGetChannelSettings, 400) // p99-65ms, 4 week max-500ms (as of 4/8/20)
	configureTimeout(CopoSpendPoints, 600)        // p99-440ms, 4 week max-1s (as of 4/8/20)
	// votes dao
	configureTimeout(VotesGetVote, 500) // 4 week max - 409ms (as of 11/15/19)
	configureTimeout(VotesCreateVote, 800)
	configureTimeout(VotesDeleteVote, 800)
	configureTimeout(VotesGetChoiceAggregations, 10000) // 10 seconds
	configureTimeout(VotesGetPollAggregation, 10000)    // 10 seconds
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func configure(cmd string, timeout int, maxConcurrent int) {
	hystrix.ConfigureCommand(cmd, hystrix.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: maxConcurrent,
		ErrorPercentThreshold: hystrix.DefaultErrorPercentThreshold,
	})
}
