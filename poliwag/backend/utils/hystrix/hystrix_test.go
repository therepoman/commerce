package hystrix

import (
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"

	"code.justin.tv/foundation/twitchclient"
)

// Asserts run function passed to hystrix returns the correct error.
// Returning an error means that the hystrix circuit will be opened.
func makeHystrixDoSpy(expectedErr error) HystrixDoer {
	return func(name string, run func() error, fallback func(error) error) error {
		err := run()
		So(err, ShouldEqual, expectedErr)

		return err
	}
}

func TestHystrixUtils_CallTwirpWithHystrix(t *testing.T) {
	Convey("given a CallTwirpWithHystrix util", t, func() {
		Convey("when the twirp call returns nil, we should not trip the circuit and return nil", func() {
			err := CallTwirpWithHystrix("1", makeHystrixDoSpy(nil), func() error {
				return nil
			})

			So(err, ShouldBeNil)
		})

		Convey("when the twirp call returns a non-twirp error, we should trip the circuit and return the error", func() {
			nonTwirpErr := errors.New("not a twirp error")

			err := CallTwirpWithHystrix("2", makeHystrixDoSpy(nonTwirpErr), func() error {
				return nonTwirpErr
			})

			So(err, ShouldEqual, nonTwirpErr)
		})

		Convey("when the twirp call returns a non-500 error, we should not trip the circuit and return the error", func() {
			notFoundErr := twirp.NewError(twirp.NotFound, "not found")

			err := CallTwirpWithHystrix("3", makeHystrixDoSpy(nil), func() error {
				return notFoundErr
			})

			So(err, ShouldEqual, notFoundErr)
		})

		Convey("when the twirp call returns a 500 error, we should trip the circuit and return the error", func() {
			internalErr := twirp.NewError(twirp.Internal, "internal error")

			err := CallTwirpWithHystrix("4", makeHystrixDoSpy(internalErr), func() error {
				return internalErr
			})

			So(err, ShouldEqual, internalErr)
		})
	})
}

func TestHystrixUtils_CallTwitchClientWithHystrix(t *testing.T) {
	Convey("given a CallTwitchClientWithHystrix util", t, func() {
		Convey("when the twitch client call returns nil, we should not trip the circuit and return nil", func() {
			err := CallTwitchClientWithHystrix("5", makeHystrixDoSpy(nil), func() error {
				return nil
			})

			So(err, ShouldBeNil)
		})

		Convey("when the twitch client call returns a non-twitch client error, we should trip the circuit and return the error", func() {
			nonTwitchClientErr := errors.New("not a twitch client error")

			err := CallTwitchClientWithHystrix("6", makeHystrixDoSpy(nonTwitchClientErr), func() error {
				return nonTwitchClientErr
			})

			So(err, ShouldEqual, nonTwitchClientErr)
		})

		Convey("when the twitch client call returns a non-500 error, we should not trip the circuit and return the error", func() {
			notFoundErr := &twitchclient.Error{
				Message:    "not found",
				StatusCode: 404,
			}

			err := CallTwitchClientWithHystrix("7", makeHystrixDoSpy(nil), func() error {
				return notFoundErr
			})

			So(err, ShouldEqual, notFoundErr)
		})

		Convey("when the twitch client call returns a 500 error, we should trip the circuit and return the error", func() {
			internalErr := &twitchclient.Error{
				Message:    "internal error",
				StatusCode: 500,
			}

			err := CallTwitchClientWithHystrix("8", makeHystrixDoSpy(internalErr), func() error {
				return internalErr
			})

			So(err, ShouldEqual, internalErr)
		})
	})
}
