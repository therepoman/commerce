package eventbus

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/poliwag/backend/models"
	eventbus_change "code.justin.tv/eventbus/schema/pkg/eventbus/change"
	eventbus_poll "code.justin.tv/eventbus/schema/pkg/poll"
)

func PollToEventbusPollCreate(poll models.Poll, choices []models.Choice) *eventbus_poll.Create {
	startTime, err := ptypes.TimestampProto(poll.StartTime)
	if err != nil {
		logrus.WithField("pollID", poll.PollID).WithError(err).Error("error converting StartTime for publishing PollCreate to Eventbus")
		startTime = ptypes.TimestampNow()
	}

	eventChoices := make([]*eventbus_poll.Choice, 0, len(choices))
	for _, choice := range choices {
		eventChoices = append(eventChoices, &eventbus_poll.Choice{
			Id:    choice.ChoiceID,
			Title: choice.Title,
		})
	}

	return &eventbus_poll.Create{
		Id:            poll.PollID,
		OwnedByUserId: poll.OwnedBy,
		Title:         poll.Title,
		Settings: &eventbus_poll.PollSettings{
			BitsVotes: &eventbus_poll.PollSettings_BitsVotes{
				Enabled: poll.IsBitsVotesEnabled,
				Cost:    int64(poll.BitsVotesCost),
			},
			ChannelPointsVotes: &eventbus_poll.PollSettings_ChannelPointsVotes{
				Enabled: poll.IsChannelPointsVotesEnabled,
				Cost:    int64(poll.ChannelPointsVotesCost),
			},
		},
		Choices:   eventChoices,
		Duration:  ptypes.DurationProto(poll.Duration),
		Status:    ConvertPollStatus(poll.Status),
		StartedAt: startTime,
	}
}

func PollToEventbusPollUpdate(poll models.Poll, choices []models.Choice, statusUpdated bool) *eventbus_poll.Update {
	startTime, err := ptypes.TimestampProto(poll.StartTime)
	if err != nil {
		logrus.WithField("pollID", poll.PollID).WithError(err).Error("error converting StartTime for publishing PollUpdate to Eventbus")
		startTime = ptypes.TimestampNow()
	}

	var endTime *timestamp.Timestamp
	if poll.EndTime != nil {
		endTime, err = ptypes.TimestampProto(*poll.EndTime)
		if err != nil {
			logrus.WithField("pollID", poll.PollID).WithError(err).Error("error converting EndTime for publishing PollUpdate to Eventbus")
			endTime = ptypes.TimestampNow()
		}
	}

	eventChoices := make([]*eventbus_poll.ChoiceChange, 0, len(choices))
	for _, choice := range choices {
		eventChoices = append(eventChoices, &eventbus_poll.ChoiceChange{
			Id:    choice.ChoiceID,
			Title: choice.Title,
			Votes: &eventbus_poll.VotesChange{
				Updated: !statusUpdated,
				Value: &eventbus_poll.Votes{
					Total:         int64(choice.TotalVotes),
					Bits:          int64(models.GetTotalBitsVotesForChoice(poll, choice)),
					ChannelPoints: int64(models.GetTotalChannelPointsVotesForChoice(poll, choice)),
					Base:          int64(choice.TotalBaseVotes),
				},
			},
			TotalVoters: &eventbus_change.Int64Change{
				Updated: !statusUpdated,
				Value:   int64(choice.TotalVoters),
			},
		})
	}

	return &eventbus_poll.Update{
		Id:            poll.PollID,
		OwnedByUserId: poll.OwnedBy,
		Title:         poll.Title,
		Settings: &eventbus_poll.PollSettings{
			BitsVotes: &eventbus_poll.PollSettings_BitsVotes{
				Enabled: poll.IsBitsVotesEnabled,
				Cost:    int64(poll.BitsVotesCost),
			},
			ChannelPointsVotes: &eventbus_poll.PollSettings_ChannelPointsVotes{
				Enabled: poll.IsChannelPointsVotesEnabled,
				Cost:    int64(poll.ChannelPointsVotesCost),
			},
		},
		Choices:  eventChoices,
		Duration: ptypes.DurationProto(poll.Duration),
		Status: &eventbus_poll.PollStatusChange{
			Updated: statusUpdated,
			Value:   ConvertPollStatus(poll.Status),
		},
		StartedAt: startTime,
		EndedAt: &eventbus_change.TimestampChange{
			Updated: statusUpdated,
			Value:   endTime,
		},
	}
}

func ConvertPollStatus(status models.PollStatus) eventbus_poll.PollStatus {
	switch status {
	case models.PollStatusActive:
		return eventbus_poll.PollStatus_POLL_STATUS_ACTIVE
	case models.PollStatusTerminated:
		return eventbus_poll.PollStatus_POLL_STATUS_TERMINATED
	case models.PollStatusCompleted:
		return eventbus_poll.PollStatus_POLL_STATUS_COMPLETED
	case models.PollStatusArchived:
		return eventbus_poll.PollStatus_POLL_STATUS_ARCHIVED
	case models.PollStatusModerated:
		return eventbus_poll.PollStatus_POLL_STATUS_MODERATED
	}
	return eventbus_poll.PollStatus_POLL_STATUS_INVALID
}
