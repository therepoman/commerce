package eventbus_test

import (
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"code.justin.tv/commerce/poliwag/backend/models"
	util "code.justin.tv/commerce/poliwag/backend/utils/eventbus"
	eventbus_change "code.justin.tv/eventbus/schema/pkg/eventbus/change"
	eventbus_poll "code.justin.tv/eventbus/schema/pkg/poll"
)

func TestPollToEventbusPollCreate(t *testing.T) {
	startTime := time.Now()
	startTimeProto, err := ptypes.TimestampProto(startTime)
	require.Nil(t, err)

	poll := models.Poll{
		PollID:                      "test-poll-id",
		OwnedBy:                     "test-user-id",
		Title:                       "test title",
		StartTime:                   startTime,
		Duration:                    30 * time.Second,
		Status:                      models.PollStatusActive,
		IsBitsVotesEnabled:          true,
		BitsVotesCost:               10,
		IsChannelPointsVotesEnabled: true,
		ChannelPointsVotesCost:      500,
	}

	choices := []models.Choice{
		{
			ChoiceID: "test-choice-id-1",
			PollID:   "test-poll-id",
			Title:    "test choice title 1",
			Index:    0,
		},
		{
			ChoiceID: "test-choice-id-2",
			PollID:   "test-poll-id",
			Title:    "test choice title 2",
			Index:    1,
		},
	}

	expectedEvent := &eventbus_poll.Create{
		Id:            "test-poll-id",
		OwnedByUserId: "test-user-id",
		Title:         "test title",
		Settings: &eventbus_poll.PollSettings{
			BitsVotes: &eventbus_poll.PollSettings_BitsVotes{
				Enabled: true,
				Cost:    10,
			},
			ChannelPointsVotes: &eventbus_poll.PollSettings_ChannelPointsVotes{
				Enabled: true,
				Cost:    500,
			},
		},
		Choices: []*eventbus_poll.Choice{
			{
				Id:    "test-choice-id-1",
				Title: "test choice title 1",
			},
			{
				Id:    "test-choice-id-2",
				Title: "test choice title 2",
			},
		},
		Duration:  ptypes.DurationProto(30 * time.Second),
		Status:    eventbus_poll.PollStatus_POLL_STATUS_ACTIVE,
		StartedAt: startTimeProto,
	}

	assert.Equal(t, util.PollToEventbusPollCreate(poll, choices), expectedEvent)
}

func TestPollToEventbusPollUpdate(t *testing.T) {
	endTime := time.Now()
	startTime := endTime.Add(-30 * time.Second)
	startTimeProto, err := ptypes.TimestampProto(startTime)
	require.Nil(t, err)
	endTimeProto, err := ptypes.TimestampProto(endTime)
	require.Nil(t, err)

	poll := models.Poll{
		PollID:                      "test-poll-id",
		OwnedBy:                     "test-user-id",
		Title:                       "test title",
		StartTime:                   startTime,
		Duration:                    30 * time.Second,
		Status:                      models.PollStatusActive,
		IsBitsVotesEnabled:          true,
		BitsVotesCost:               10,
		IsChannelPointsVotesEnabled: true,
		ChannelPointsVotesCost:      500,
		TotalVoters:                 100,
		TotalVotes:                  150,
		TotalBaseVotes:              100,
		TotalBits:                   20 * 10,  // 20 bits votes
		TotalChannelPoints:          30 * 500, // 30 channel points votes
	}

	choices := []models.Choice{
		{
			ChoiceID:           "test-choice-id-1",
			PollID:             "test-poll-id",
			Title:              "test choice title 1",
			Index:              0,
			TotalVoters:        40,
			TotalVotes:         55,
			TotalBaseVotes:     40,
			TotalBits:          5 * 10,   // 5 bits votes
			TotalChannelPoints: 10 * 500, // 10 channel points votes
		},
		{
			ChoiceID:           "test-choice-id-2",
			PollID:             "test-poll-id",
			Title:              "test choice title 2",
			Index:              1,
			TotalVoters:        60,
			TotalVotes:         95,
			TotalBaseVotes:     60,
			TotalBits:          15 * 10,  // 15 bits votes
			TotalChannelPoints: 20 * 500, // 20 channel points votes
		},
	}

	t.Run("without status update", func(t *testing.T) {
		expectedEvent := &eventbus_poll.Update{
			Id:            "test-poll-id",
			OwnedByUserId: "test-user-id",
			Title:         "test title",
			Settings: &eventbus_poll.PollSettings{
				BitsVotes: &eventbus_poll.PollSettings_BitsVotes{
					Enabled: true,
					Cost:    10,
				},
				ChannelPointsVotes: &eventbus_poll.PollSettings_ChannelPointsVotes{
					Enabled: true,
					Cost:    500,
				},
			},
			Choices: []*eventbus_poll.ChoiceChange{
				{
					Id:    "test-choice-id-1",
					Title: "test choice title 1",
					Votes: &eventbus_poll.VotesChange{
						Updated: true,
						Value: &eventbus_poll.Votes{
							Total:         55,
							Bits:          5,
							ChannelPoints: 10,
							Base:          40,
						},
					},
					TotalVoters: &eventbus_change.Int64Change{
						Updated: true,
						Value:   40,
					},
				},
				{
					Id:    "test-choice-id-2",
					Title: "test choice title 2",
					Votes: &eventbus_poll.VotesChange{
						Updated: true,
						Value: &eventbus_poll.Votes{
							Total:         95,
							Bits:          15,
							ChannelPoints: 20,
							Base:          60,
						},
					},
					TotalVoters: &eventbus_change.Int64Change{
						Updated: true,
						Value:   60,
					},
				},
			},
			Duration: ptypes.DurationProto(30 * time.Second),
			Status: &eventbus_poll.PollStatusChange{
				Updated: false,
				Value:   eventbus_poll.PollStatus_POLL_STATUS_ACTIVE,
			},
			StartedAt: startTimeProto,
			EndedAt: &eventbus_change.TimestampChange{
				Updated: false,
				Value:   nil,
			},
		}
		assert.Equal(t, util.PollToEventbusPollUpdate(poll, choices, false), expectedEvent)
	})

	t.Run("with status update", func(t *testing.T) {
		poll.EndTime = &endTime
		poll.Status = models.PollStatusCompleted

		expectedEvent := &eventbus_poll.Update{
			Id:            "test-poll-id",
			OwnedByUserId: "test-user-id",
			Title:         "test title",
			Settings: &eventbus_poll.PollSettings{
				BitsVotes: &eventbus_poll.PollSettings_BitsVotes{
					Enabled: true,
					Cost:    10,
				},
				ChannelPointsVotes: &eventbus_poll.PollSettings_ChannelPointsVotes{
					Enabled: true,
					Cost:    500,
				},
			},
			Choices: []*eventbus_poll.ChoiceChange{
				{
					Id:    "test-choice-id-1",
					Title: "test choice title 1",
					Votes: &eventbus_poll.VotesChange{
						Updated: false,
						Value: &eventbus_poll.Votes{
							Total:         55,
							Bits:          5,
							ChannelPoints: 10,
							Base:          40,
						},
					},
					TotalVoters: &eventbus_change.Int64Change{
						Updated: false,
						Value:   40,
					},
				},
				{
					Id:    "test-choice-id-2",
					Title: "test choice title 2",
					Votes: &eventbus_poll.VotesChange{
						Updated: false,
						Value: &eventbus_poll.Votes{
							Total:         95,
							Bits:          15,
							ChannelPoints: 20,
							Base:          60,
						},
					},
					TotalVoters: &eventbus_change.Int64Change{
						Updated: false,
						Value:   60,
					},
				},
			},
			Duration: ptypes.DurationProto(30 * time.Second),
			Status: &eventbus_poll.PollStatusChange{
				Updated: true,
				Value:   eventbus_poll.PollStatus_POLL_STATUS_COMPLETED,
			},
			StartedAt: startTimeProto,
			EndedAt: &eventbus_change.TimestampChange{
				Updated: true,
				Value:   endTimeProto,
			},
		}

		assert.Equal(t, util.PollToEventbusPollUpdate(poll, choices, true), expectedEvent)
	})
}

func TestConvertStatus(t *testing.T) {
	tests := []struct {
		name   string
		status models.PollStatus
		want   eventbus_poll.PollStatus
	}{
		{
			"active",
			models.PollStatusActive,
			eventbus_poll.PollStatus_POLL_STATUS_ACTIVE,
		},
		{
			"terminated",
			models.PollStatusTerminated,
			eventbus_poll.PollStatus_POLL_STATUS_TERMINATED,
		},
		{
			"completed",
			models.PollStatusCompleted,
			eventbus_poll.PollStatus_POLL_STATUS_COMPLETED,
		},
		{
			"archived",
			models.PollStatusArchived,
			eventbus_poll.PollStatus_POLL_STATUS_ARCHIVED,
		},
		{
			"moderated",
			models.PollStatusModerated,
			eventbus_poll.PollStatus_POLL_STATUS_MODERATED,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := util.ConvertPollStatus(tt.status)
			assert.Equal(t, tt.want, got)
		})
	}
}
