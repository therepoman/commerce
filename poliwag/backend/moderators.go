package backend

import (
	"github.com/go-playground/validator/v10"

	"code.justin.tv/commerce/poliwag/backend/moderation/input_automod"
)

type moderators struct {
	pollInputModerator input_automod.Moderator `validate:"required"`
}

func createModerators(c *clients) (*moderators, error) {
	out := &moderators{
		pollInputModerator: input_automod.NewModerator(c.zuma),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
