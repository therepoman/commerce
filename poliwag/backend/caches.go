package backend

import (
	"github.com/go-playground/validator/v10"

	viewable_poll_cache "code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
)

type caches struct {
	viewablePollCache viewable_poll_cache.ViewablePollCache `validate:"required"`
}

func createCaches(c *clients, d *daos) (*caches, error) {
	out := &caches{
		viewablePollCache: viewable_poll_cache.NewViewablePollCache(d.polls, c.rediczar),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
