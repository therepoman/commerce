package backend

import (
	"github.com/go-playground/validator/v10"

	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_archive_v1"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_complete_v1"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_moderate_v1"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_terminate_v1"
	"code.justin.tv/commerce/poliwag/backend/stepfn/executors/poll_update_v1"
	"code.justin.tv/commerce/poliwag/config"
)

type stepFnExecutors struct {
	PollUpdateExecutor    poll_update_v1.Executor    `validate:"required"`
	PollCompleteExecutor  poll_complete_v1.Executor  `validate:"required"`
	PollTerminateExecutor poll_terminate_v1.Executor `validate:"required"`
	PollArchiveExecutor   poll_archive_v1.Executor   `validate:"required"`
	PollModerateExecutor  poll_moderate_v1.Executor  `validate:"required"`
}

func createStepFnExecutors(cfg *config.Config, c *clients) (*stepFnExecutors, error) {
	out := &stepFnExecutors{
		PollUpdateExecutor:    poll_update_v1.NewExecutor(cfg.StepFn.PollUpdateStepFn.StateMachineARN, c.stepfn),
		PollCompleteExecutor:  poll_complete_v1.NewExecutor(cfg.StepFn.PollCompleteStepFn.StateMachineARN, c.stepfn),
		PollTerminateExecutor: poll_terminate_v1.NewExecutor(cfg.StepFn.PollTerminateStepFn.StateMachineARN, c.stepfn),
		PollArchiveExecutor:   poll_archive_v1.NewExecutor(cfg.StepFn.PollArchiveStepFn.StateMachineARN, c.stepfn),
		PollModerateExecutor:  poll_moderate_v1.NewExecutor(cfg.StepFn.PollModerateStepFn.StateMachineARN, c.stepfn),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
