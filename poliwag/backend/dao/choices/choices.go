package choices

import (
	"context"
	"fmt"
	"sort"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/gofrs/uuid"
	"golang.org/x/sync/singleflight"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/telemetryext"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

const (
	choicesTableNamePrefix = "choices"
	choiceHashKey          = "choice_id"
	pollIDChoiceIDIndex    = "PollIDChoiceIDIndex"
)

type CreateChoiceArgs struct {
	Title string
}

type UpdateChoiceArgs struct {
	ChoiceID           string
	Title              *string
	TotalVoters        *int
	TotalVotes         *int
	TotalBaseVotes     *int
	TotalBits          *int
	TotalChannelPoints *int
}

type ChoicesDAO interface {
	// Creates multiple Choices in Dynamo
	CreateChoices(ctx context.Context, pollID string, args []CreateChoiceArgs) ([]models.Choice, error)
	// Gets choices for a specific poll ID
	GetChoicesByPollID(ctx context.Context, pollID string) ([]models.Choice, error)
	// Updates multiple Choices
	UpdateChoices(ctx context.Context, args []UpdateChoiceArgs) error
}

type choicesDaoImpl struct {
	choicesTableName string
	stats            statsd.Statter
	sampleReporter   *telemetry.SampleReporter
	dynamoDBClient   dynamodbiface.DynamoDBAPI
	daxClient        dynamodbiface.DynamoDBAPI

	sfGetChoicesByPollID singleflight.Group
}

func NewChoicesDAO(
	env string,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
	dynamoDBClient dynamodbiface.DynamoDBAPI,
	daxClient dynamodbiface.DynamoDBAPI,
) ChoicesDAO {
	return &choicesDaoImpl{
		choicesTableName: fmt.Sprintf("%s-%s", choicesTableNamePrefix, env),
		dynamoDBClient:   dynamoDBClient,
		daxClient:        daxClient,
		stats:            stats,
		sampleReporter:   sampleReporter,
	}
}

// Creates the choice from the specified parameters
// As a convenience this function also marshals the choice into dynamodb.AttributeValue map
func newChoice(pollID string, title string, index int) (models.Choice, map[string]*dynamodb.AttributeValue, error) {
	now := time.Now()
	choiceID, err := uuid.NewV4()
	if err != nil {
		return models.Choice{}, nil, err
	}

	choice := models.Choice{
		ChoiceID:           choiceID.String(),
		PollID:             pollID,
		Title:              title,
		Index:              index,
		TotalVoters:        0,
		TotalVotes:         0,
		TotalBits:          0,
		TotalChannelPoints: 0,
		CreatedTime:        now,
		UpdatedTime:        now,
		TTL:                now.AddDate(0, 0, 90),
	}

	dynamoChoice, err := dynamodbattribute.MarshalMap(choice)
	if err != nil {
		return models.Choice{}, nil, errors.InternalError.Wrap(err, "error marshaling choice")
	}

	return choice, dynamoChoice, nil
}

func getChoiceKey(choiceID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		choiceHashKey: {
			S: aws.String(choiceID),
		},
	}
}

func (dao *choicesDaoImpl) CreateChoices(ctx context.Context, pollID string, argsArray []CreateChoiceArgs) ([]models.Choice, error) {
	var choices []models.Choice
	var writeRequests []*dynamodb.WriteRequest
	for i, args := range argsArray {
		choice, dynamoChoice, err := newChoice(pollID, args.Title, i)
		if err != nil {
			return nil, err
		}

		choices = append(choices, choice)

		writeReq := &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: dynamoChoice,
			},
		}
		writeRequests = append(writeRequests, writeReq)
	}

	_, err := dao.daxClient.BatchWriteItemWithContext(ctx, &dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{
			dao.choicesTableName: writeRequests,
		},
	})
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error creating choices in dynamo")
	}

	return choices, nil
}

func (dao *choicesDaoImpl) GetChoicesByPollID(ctx context.Context, pollID string) ([]models.Choice, error) {
	var choices []models.Choice

	keyCondition := expression.Key("poll_id").Equal(expression.Value(pollID))
	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()
	if err != nil {
		return nil, err
	}

	v, err, isShared := dao.sfGetChoicesByPollID.Do(pollID, func() (interface{}, error) {
		return dao.dynamoDBClient.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.choicesTableName),
			IndexName:                 aws.String(pollIDChoiceIDIndex),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			FilterExpression:          expr.Filter(),
			KeyConditionExpression:    expr.KeyCondition(),
		})
	})

	if isShared {
		go func() {
			if err := dao.stats.Inc("GetChoicesByPollID_isShared", 1, 1.0); err != nil {
				logrus.WithError(err).Warn("error sending GetChoicesByPollID_isShared metric")
			}
		}()
		r := telemetryext.ReporterWithDimensions(*dao.sampleReporter, map[string]string{
			"Method": "Choices_GetChoicesByPollID",
		})
		r.Report("SharedResult", 1.0, telemetry.UnitCount)
	}

	if err != nil {
		return nil, err
	}

	results, ok := v.(*dynamodb.QueryOutput)

	if !ok || results == nil {
		// This shouldn't happen
		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
		}).Error("encountered nil results")
		return []models.Choice{}, nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(results.Items, &choices)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error unmarshaling queried choices from dynamo")
	}

	// Sort the choices to provide a consistent sort
	sort.Slice(choices, func(i int, j int) bool {
		return choices[i].Index < choices[j].Index
	})

	return choices, nil
}

func (dao *choicesDaoImpl) UpdateChoices(ctx context.Context, args []UpdateChoiceArgs) error {
	var transactWriteItems []*dynamodb.TransactWriteItem
	for _, a := range args {
		updateRequest, err := dao.newChoiceUpdateRequest(a)
		if err != nil {
			return err
		}

		transactWriteItems = append(transactWriteItems, &dynamodb.TransactWriteItem{
			Update: &updateRequest,
		})
	}

	_, err := dao.daxClient.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{
		TransactItems: transactWriteItems,
	})
	if err != nil {
		return errors.InternalError.Wrap(err, "error updating choices in dynamo")
	}

	// TODO: Return updated models.Choice here as well
	return nil
}

func (dao *choicesDaoImpl) newChoiceUpdateRequest(updateArgs UpdateChoiceArgs) (dynamodb.Update, error) {
	updateBuilder := expression.Set(expression.Name("updated_time"), expression.Value(time.Now()))

	if updateArgs.Title != nil {
		updateBuilder = updateBuilder.Set(expression.Name("title"), expression.Value(*updateArgs.Title))
	}

	if updateArgs.TotalVoters != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_voters"), expression.Value(*updateArgs.TotalVoters))
	}

	if updateArgs.TotalVotes != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_votes"), expression.Value(*updateArgs.TotalVotes))
	}

	if updateArgs.TotalBaseVotes != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_base_votes"), expression.Value(*updateArgs.TotalBaseVotes))
	}

	if updateArgs.TotalBits != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_bits"), expression.Value(*updateArgs.TotalBits))
	}

	if updateArgs.TotalChannelPoints != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_channel_points"), expression.Value(*updateArgs.TotalChannelPoints))
	}

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return dynamodb.Update{}, err
	}

	return dynamodb.Update{
		TableName:                 aws.String(dao.choicesTableName),
		Key:                       getChoiceKey(updateArgs.ChoiceID),
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}, nil
}
