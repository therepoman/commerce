package polls_test

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"os"
	"sort"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/stretchr/testify/require"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/models"
	pantheon_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/common/go_test_dynamo"
)

type testCase struct {
	pollsDAO        polls.PollsDAO
	testDB          dynamodbiface.DynamoDBAPI
	testDBTableName string
	cleanup         func()
}

func setupTest(t *testing.T) *testCase {
	// Create test DynamoDB instance
	db := go_test_dynamo.Instance()
	rand.Seed(time.Now().UnixNano())
	fakeEnv := fmt.Sprintf("fake-db-%d", rand.Intn(1000000000))
	tableName := fmt.Sprintf("%s-%s", polls.PollsTableNamePrefix, fakeEnv)
	_, err := db.Dynamo.CreateTable(
		// Keep this schema up to date if the underlying schema changes.
		&dynamodb.CreateTableInput{
			AttributeDefinitions: []*dynamodb.AttributeDefinition{
				{
					AttributeName: aws.String("poll_id"),
					AttributeType: aws.String("S"),
				},
				{
					AttributeName: aws.String("owned_by"),
					AttributeType: aws.String("S"),
				},
				{
					AttributeName: aws.String("start_time"),
					AttributeType: aws.String("S"),
				},
			},
			GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
				{
					IndexName: aws.String(polls.OwnedByStartTimeIndex),
					KeySchema: []*dynamodb.KeySchemaElement{
						{
							AttributeName: aws.String("owned_by"),
							KeyType:       aws.String("HASH"),
						},
						{
							AttributeName: aws.String("start_time"),
							KeyType:       aws.String("RANGE"),
						},
					},
					Projection: &dynamodb.Projection{
						ProjectionType: aws.String(dynamodb.ProjectionTypeAll),
					},
					ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
						ReadCapacityUnits:  aws.Int64(1000),
						WriteCapacityUnits: aws.Int64(1000),
					},
				},
			},
			KeySchema: []*dynamodb.KeySchemaElement{
				{
					AttributeName: aws.String("poll_id"),
					KeyType:       aws.String("HASH"),
				},
			},
			BillingMode: aws.String(dynamodb.BillingModeProvisioned),
			ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
				ReadCapacityUnits:  aws.Int64(1000),
				WriteCapacityUnits: aws.Int64(1000),
			},
			TableName: aws.String(tableName),
		},
	)
	require.NoError(t, err, "creating table")

	statsdClient, err := statsd.NewNoopClient()
	require.NoError(t, err)

	pollsDAO := polls.NewPollsDAO(fakeEnv, statsdClient, telemetry.NewNoOpSampleReporter(), db.Dynamo, db.Dynamo, &pantheon_mock.Pantheon{})

	return &testCase{
		cleanup: func() {
			db.Cleanup()
		},
		pollsDAO:        pollsDAO,
		testDB:          db.Dynamo,
		testDBTableName: tableName,
	}
}

func TestMain(m *testing.M) {
	ddb := go_test_dynamo.Instance()
	code := m.Run()
	err := ddb.Shutdown()
	if err != nil {
		log.Println("error shutting down ddb:", err)
		os.Exit(1)
	}
	os.Exit(code)
}

func genPoll(id int, ownedBy string, status models.PollStatus, now, ttl time.Time) models.Poll {
	return models.Poll{
		PollID:      fmt.Sprintf("some-uuid-here-%d", id),
		OwnedBy:     ownedBy,
		CreatedBy:   ownedBy,
		Title:       "This is a poll",
		StartTime:   now,
		Duration:    10 * time.Second,
		Status:      status,
		CreatedTime: now,
		UpdatedTime: now,
		TTL:         ttl,

		IsMultiChoiceEnabled:          true,
		IsSubscriberOnlyEnabled:       false,
		IsSubscriberMultiplierEnabled: false,
		IsBitsVotesEnabled:            false,
		BitsVotesCost:                 0,
		IsChannelPointsVotesEnabled:   false,
		ChannelPointsVotesCost:        0,

		SlackMessageTimestamp: "",
	}
}

func TestPollsDAO_CreatePoll(t *testing.T) {
	s := setupTest(t)
	defer s.cleanup()

	now := time.Now()

	expectedPoll := genPoll(0, "123", models.PollStatusActive, now, now.AddDate(0, 0, 90))

	actualPoll, err := s.pollsDAO.CreatePoll(
		context.Background(),
		expectedPoll.PollID,
		expectedPoll.CreatedBy,
		expectedPoll.OwnedBy,
		expectedPoll.Title,
		expectedPoll.Duration,
		expectedPoll.IsMultiChoiceEnabled,
		expectedPoll.IsSubscriberOnlyEnabled,
		expectedPoll.IsSubscriberMultiplierEnabled,
		expectedPoll.IsBitsVotesEnabled,
		expectedPoll.BitsVotesCost,
		expectedPoll.IsChannelPointsVotesEnabled,
		expectedPoll.ChannelPointsVotesCost,
		expectedPoll.SlackMessageTimestamp,
	)
	require.NoError(t, err)
	// set the expected time to the actual ones since they are generated in CreatePoll
	expectedPoll.StartTime = actualPoll.StartTime
	expectedPoll.CreatedTime = actualPoll.CreatedTime
	expectedPoll.UpdatedTime = actualPoll.UpdatedTime
	expectedPoll.TTL = actualPoll.TTL

	require.Equal(t, expectedPoll, actualPoll)

	getPoll, err := s.pollsDAO.GetPoll(context.Background(), expectedPoll.PollID, true)
	require.NoError(t, err)
	require.NotNil(t, getPoll)

	// when retrieving the poll from dynamo it has gone through a marshal->unmarshal which changes
	// how the time values are stored so do the same to expectedPoll to match
	dynamoPoll, err := dynamodbattribute.MarshalMap(expectedPoll)
	require.NoError(t, err)
	err = dynamodbattribute.UnmarshalMap(dynamoPoll, &expectedPoll)
	require.NoError(t, err)

	require.Equal(t, expectedPoll, *getPoll)
}

func TestPollsDAO_GetPoll(t *testing.T) {
	s := setupTest(t)
	defer s.cleanup()

	now := time.Now()

	expectedPoll := genPoll(0, "123", models.PollStatusActive, now, now.AddDate(0, 0, 90))

	dynamoPoll, err := dynamodbattribute.MarshalMap(expectedPoll)
	require.NoError(t, err)
	_, err = s.testDB.PutItemWithContext(context.Background(), &dynamodb.PutItemInput{Item: dynamoPoll, TableName: aws.String(s.testDBTableName)})
	require.NoError(t, err)

	err = dynamodbattribute.UnmarshalMap(dynamoPoll, &expectedPoll)
	require.NoError(t, err)

	t.Run("poll exists", func(t *testing.T) {
		actualPoll, err := s.pollsDAO.GetPoll(context.Background(), expectedPoll.PollID, true)
		require.NoError(t, err)
		require.NotNil(t, actualPoll)
		require.Equal(t, expectedPoll, *actualPoll)
	})

	t.Run("poll doesn't exist", func(t *testing.T) {
		actualPoll, err := s.pollsDAO.GetPoll(context.Background(), "not-the-poll-id", true)
		require.NoError(t, err)
		require.Nil(t, actualPoll)
	})

	t.Run("poll is expired", func(t *testing.T) {
		expectedPoll = genPoll(100, "123", models.PollStatusActive, now, now.Add(-1*time.Minute))
		dynamoPoll, err := dynamodbattribute.MarshalMap(expectedPoll)
		require.NoError(t, err)

		_, err = s.testDB.PutItemWithContext(context.Background(), &dynamodb.PutItemInput{Item: dynamoPoll, TableName: aws.String(s.testDBTableName)})
		require.NoError(t, err)

		actualPoll, err := s.pollsDAO.GetPoll(context.Background(), expectedPoll.PollID, true)
		require.NoError(t, err)
		require.Nil(t, actualPoll)
	})
}

func TestPollsDAO_GetPolls(t *testing.T) {
	s := setupTest(t)
	defer s.cleanup()

	numPolls := 5
	now := time.Now()

	expectedPolls := []models.Poll{}
	allPolls := []models.Poll{}
	for i := 0; i < numPolls; i++ {
		expected := true
		startTime := now.Add(-time.Duration(i) * time.Hour)
		ttl := startTime.AddDate(0, 0, 90)
		if i == numPolls-1 { // expired poll
			ttl = now.Add(-1 * time.Minute)
			expected = false
		}
		poll := genPoll(i, "123", models.PollStatusCompleted, startTime, ttl)

		dynamoPoll, err := dynamodbattribute.MarshalMap(poll)
		require.NoError(t, err)
		_, err = s.testDB.PutItemWithContext(context.Background(), &dynamodb.PutItemInput{Item: dynamoPoll, TableName: aws.String(s.testDBTableName)})
		require.NoError(t, err)

		err = dynamodbattribute.UnmarshalMap(dynamoPoll, &poll)
		require.NoError(t, err)

		allPolls = append(allPolls, poll)
		if expected {
			expectedPolls = append(expectedPolls, poll)
		}
	}

	t.Run("returns existing non-expired polls", func(t *testing.T) {
		pollIDs := []string{}
		for _, poll := range allPolls {
			pollIDs = append(pollIDs, poll.PollID)
		}
		pollIDs = append(pollIDs, "not-a-poll-id")
		actualPolls, err := s.pollsDAO.GetPolls(context.Background(), pollIDs, true)
		require.NoError(t, err)
		require.Equal(t, len(expectedPolls), len(actualPolls))

		sort.Slice(actualPolls, func(i, j int) bool { return actualPolls[i].PollID < actualPolls[j].PollID })
		for i, expectedPoll := range expectedPolls {
			require.Equal(t, expectedPoll, actualPolls[i])
		}
	})

	t.Run("no polls exist", func(t *testing.T) {
		actualPolls, err := s.pollsDAO.GetPolls(context.Background(), []string{"not-a-poll-id-1", "not-a-poll-id-2"}, true)
		require.NoError(t, err)
		require.Zero(t, len(actualPolls))
	})
}

func TestPollsDAO_GetPollsByOwnedBy(t *testing.T) {
	s := setupTest(t)
	defer s.cleanup()

	numPolls := 20
	now := time.Now()
	expectedOwnedBy := "123"

	expectedActivePolls := []models.Poll{}
	expectedCompletedPolls := []models.Poll{}
	expectedTerminatedPolls := []models.Poll{}
	for i := 0; i < numPolls; i++ {
		expectedActive := false
		expectedCompleted := false
		expectedTerminated := false

		ownedBy := expectedOwnedBy
		status := models.PollStatusCompleted
		startTime := now.Add(-time.Duration(i) * time.Hour)
		ttl := startTime.AddDate(0, 0, 90)

		if i < 2 {
			status = models.PollStatusActive
			expectedActive = true
		} else if i < 4 {
			status = models.PollStatusTerminated
			expectedTerminated = true
		} else if i < 6 {
			ttl = now.Add(-1 * time.Minute) // expired polls
		} else {
			expectedCompleted = true
		}

		if i%2 == 0 {
			ownedBy = "321"
			expectedActive = false
			expectedCompleted = false
			expectedTerminated = false
		}

		poll := genPoll(i, ownedBy, status, startTime, ttl)

		dynamoPoll, err := dynamodbattribute.MarshalMap(poll)
		require.NoError(t, err)
		_, err = s.testDB.PutItemWithContext(context.Background(), &dynamodb.PutItemInput{Item: dynamoPoll, TableName: aws.String(s.testDBTableName)})
		require.NoError(t, err)

		err = dynamodbattribute.UnmarshalMap(dynamoPoll, &poll)
		require.NoError(t, err)

		if expectedActive {
			expectedActivePolls = append(expectedActivePolls, poll)
		}
		if expectedCompleted {
			expectedCompletedPolls = append(expectedCompletedPolls, poll)
		}
		if expectedTerminated {
			expectedTerminatedPolls = append(expectedTerminatedPolls, poll)
		}
	}

	t.Run("all polls sorted desc, no expired", func(t *testing.T) {
		expectedPolls := []models.Poll{}
		for _, poll := range expectedActivePolls {
			expectedPolls = append(expectedPolls, poll)
		}
		for _, poll := range expectedCompletedPolls {
			expectedPolls = append(expectedPolls, poll)
		}
		for _, poll := range expectedTerminatedPolls {
			expectedPolls = append(expectedPolls, poll)
		}

		actualPolls, cursor, err := s.pollsDAO.GetPollsByOwnedBy(context.Background(), expectedOwnedBy, []models.PollStatus{}, models.PollSortStartTime, models.DirectionDescending, numPolls, nil)
		require.NoError(t, err)
		require.Zero(t, len(cursor))
		require.Equal(t, len(expectedPolls), len(actualPolls))

		sort.Slice(expectedPolls, func(i, j int) bool { return expectedPolls[i].StartTime.After(expectedPolls[j].StartTime) })
		for i, expectedPoll := range expectedPolls {
			require.Equal(t, expectedPoll, actualPolls[i])
		}
	})

	t.Run("completed polls sorted asc, no expired", func(t *testing.T) {
		expectedPolls := []models.Poll{}
		for _, poll := range expectedCompletedPolls {
			expectedPolls = append(expectedPolls, poll)
		}

		actualPolls, cursor, err := s.pollsDAO.GetPollsByOwnedBy(context.Background(), expectedOwnedBy, []models.PollStatus{models.PollStatusCompleted}, models.PollSortStartTime, models.DirectionAscending, numPolls, nil)
		require.NoError(t, err)
		require.Zero(t, len(cursor))
		require.Equal(t, len(expectedPolls), len(actualPolls))

		sort.Slice(expectedPolls, func(i, j int) bool { return expectedPolls[i].StartTime.Before(expectedPolls[j].StartTime) })
		for i, expectedPoll := range expectedPolls {
			require.Equal(t, expectedPoll, actualPolls[i])
		}
	})

	t.Run("no polls exist", func(t *testing.T) {
		actualPolls, cursor, err := s.pollsDAO.GetPollsByOwnedBy(context.Background(), "notOwnedBy", []models.PollStatus{}, models.PollSortStartTime, models.DirectionDescending, numPolls, nil)
		require.NoError(t, err)
		require.Zero(t, len(cursor))
		require.Zero(t, len(actualPolls))
	})
}
