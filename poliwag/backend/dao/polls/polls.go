package polls

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/sync/singleflight"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/telemetryext"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

const (
	PollsTableNamePrefix = "polls"

	OwnedByStartTimeIndex = "OwnedByStartTimeIndex"
)

type PollsCursor = map[string]*dynamodb.AttributeValue

type PollUpdateArgs struct {
	Title                         *string
	StartTime                     *time.Time
	EndTime                       *time.Time
	ModeratedTime                 *time.Time
	EndedBy                       *string
	Duration                      *time.Duration
	Status                        *models.PollStatus
	IsMultiChoiceEnabled          *bool
	IsSubscriberOnlyEnabled       *bool
	IsSubscriberMultiplierEnabled *bool
	IsChannelPointsEnabled        *bool
	ChannelPointsVotesCost        *int
	IsBitsVotesEnabled            *bool
	BitsVotesCost                 *int
	TotalVoters                   *int
	TotalBaseVotes                *int
	TotalVotes                    *int
	TotalBits                     *int
	TotalChannelPoints            *int
	LastUpdatedAggregateTime      *time.Time
	SlackMessageTimestamp         *string
}

type pollsDaoImpl struct {
	pollsTableName string
	stats          statsd.Statter
	sampleReporter *telemetry.SampleReporter
	dynamoDBClient dynamodbiface.DynamoDBAPI
	daxClient      dynamodbiface.DynamoDBAPI
	pantheonClient pantheonrpc.Pantheon

	sfGetPoll           singleflight.Group
	sfGetPolls          singleflight.Group
	sfGetPollsByOwnedBy singleflight.Group
}

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 . PollsDAO

type PollsDAO interface {
	CreatePoll(ctx context.Context, pollID, userID, ownedBy, title string, duration time.Duration, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled bool, bitsVotesCost int, isChannelPointsVotesEnabled bool, channelPointsVotesCost int, messageTimestamp string) (models.Poll, error)
	GetPoll(ctx context.Context, pollID string, consistentRead bool) (*models.Poll, error)
	GetPolls(ctx context.Context, pollIDs []string, consistentRead bool) ([]models.Poll, error)
	GetPollsByOwnedBy(ctx context.Context, ownedBy string, statuses []models.PollStatus, sort models.PollSort, direction models.Direction, limit int, lastEvaluatedKey PollsCursor) ([]models.Poll, PollsCursor, error)
	UpdatePoll(ctx context.Context, pollID string, updateArgs PollUpdateArgs) (models.Poll, error)
	GetTopContributorsFromPantheon(ctx context.Context, pollID string) (*models.TopBitsContributor, *models.TopChannelPointsContributor, error)
}

func NewPollsDAO(
	env string,
	stats statsd.Statter,
	sampleReporter *telemetry.SampleReporter,
	dynamoDBClient dynamodbiface.DynamoDBAPI,
	daxClient dynamodbiface.DynamoDBAPI,
	pantheonClient pantheonrpc.Pantheon,
) PollsDAO {
	return &pollsDaoImpl{
		pollsTableName: fmt.Sprintf("%s-%s", PollsTableNamePrefix, env),
		pantheonClient: pantheonClient,
		dynamoDBClient: dynamoDBClient,
		daxClient:      daxClient,
		stats:          stats,
		sampleReporter: sampleReporter,
	}
}

func (d *pollsDaoImpl) CreatePoll(ctx context.Context, pollID, userID, ownedBy, title string, duration time.Duration, isMultiChoiceEnabled, isSubscriberOnlyEnabled, isSubscriberMultiplierEnabled, isBitsVotesEnabled bool, bitsVotesCost int, isChannelPointsVotesEnabled bool, channelPointsVotesCost int, slackMessageTimestamp string) (models.Poll, error) {
	now := time.Now()

	poll := models.Poll{
		PollID:                        pollID,
		OwnedBy:                       ownedBy,
		CreatedBy:                     userID,
		Title:                         title,
		StartTime:                     now,
		Duration:                      duration,
		Status:                        models.PollStatusActive,
		IsMultiChoiceEnabled:          isMultiChoiceEnabled,
		IsSubscriberOnlyEnabled:       isSubscriberOnlyEnabled,
		IsSubscriberMultiplierEnabled: isSubscriberMultiplierEnabled,
		IsBitsVotesEnabled:            isBitsVotesEnabled,
		BitsVotesCost:                 bitsVotesCost,
		IsChannelPointsVotesEnabled:   isChannelPointsVotesEnabled,
		ChannelPointsVotesCost:        channelPointsVotesCost,
		CreatedTime:                   now,
		UpdatedTime:                   now,
		SlackMessageTimestamp:         slackMessageTimestamp,
		TTL:                           now.AddDate(0, 0, 90),
	}

	dynamoPoll, err := dynamodbattribute.MarshalMap(poll)
	if err != nil {
		return models.Poll{}, err
	}

	_, err = d.daxClient.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		Item:      dynamoPoll,
		TableName: aws.String(d.pollsTableName),
	})

	if err != nil {
		return models.Poll{}, errors.InternalError.Wrap(err, "error creating poll in dynamo")
	}

	return poll, nil
}

func (d *pollsDaoImpl) GetPoll(ctx context.Context, pollID string, consistentRead bool) (*models.Poll, error) {
	var poll models.Poll

	requestGroupKey := fmt.Sprintf("%v:%v", pollID, consistentRead)
	v, err, isShared := d.sfGetPoll.Do(requestGroupKey, func() (interface{}, error) {
		return d.daxClient.GetItemWithContext(ctx, &dynamodb.GetItemInput{
			TableName:      aws.String(d.pollsTableName),
			Key:            getDynamoHashKey(pollID),
			ConsistentRead: aws.Bool(consistentRead),
		})
	})

	if isShared {
		go func() {
			if err := d.stats.Inc("GetPoll_isShared", 1, 1.0); err != nil {
				logrus.WithError(err).Warn("error sending GetPoll_isShared metric")
			}
			r := telemetryext.ReporterWithDimensions(*d.sampleReporter, map[string]string{
				"Method": "Polls_GetPoll",
			})
			r.Report("SharedResult", 1.0, telemetry.UnitCount)
		}()
	}

	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting poll from dynamo")
	}

	output, ok := v.(*dynamodb.GetItemOutput)

	if !ok || output.Item == nil {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalMap(output.Item, &poll)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error unmarshaling poll output from dynamo")
	}

	// dynamo has delayed cleanup of TTL's so check if poll is expired
	if poll.TTL.Before(time.Now()) {
		return nil, nil
	}

	return &poll, err
}

func (d *pollsDaoImpl) GetPolls(ctx context.Context, pollIDs []string, consistentRead bool) ([]models.Poll, error) {
	var polls []models.Poll

	var keys []map[string]*dynamodb.AttributeValue
	for _, pollID := range pollIDs {
		keys = append(keys, getDynamoHashKey(pollID))
	}

	requestGroupKey := fmt.Sprintf("%v:%v", pollIDs, consistentRead)
	v, err, isShared := d.sfGetPolls.Do(requestGroupKey, func() (interface{}, error) {
		return d.daxClient.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
			RequestItems: map[string]*dynamodb.KeysAndAttributes{
				d.pollsTableName: {
					Keys:           keys,
					ConsistentRead: aws.Bool(consistentRead),
				},
			},
		})
	})

	if isShared {
		go func() {
			if err := d.stats.Inc("GetPolls_isShared", 1, 1.0); err != nil {
				logrus.WithError(err).Warn("error sending GetPolls_isShared metric")
			}
			r := telemetryext.ReporterWithDimensions(*d.sampleReporter, map[string]string{
				"Method": "Polls_GetPolls",
			})
			r.Report("SharedResult", 1.0, telemetry.UnitCount)
		}()
	}

	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting polls from dynamo")
	}

	output, ok := v.(*dynamodb.BatchGetItemOutput)

	if !ok || output.Responses == nil || output.Responses[d.pollsTableName] == nil {
		// This shouldn't happen
		logrus.WithFields(logrus.Fields{
			"pollIDs": pollIDs,
		}).Error("encountered nil response when performing batch get on polls table")
		return []models.Poll{}, nil
	}

	tableResponse := output.Responses[d.pollsTableName]

	err = dynamodbattribute.UnmarshalListOfMaps(tableResponse, &polls)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error unmarshaling poll output from dynamo")
	}

	now := time.Now()
	filteredPolls := make([]models.Poll, 0, len(polls))
	for _, poll := range polls {
		// dynamo has delayed cleanup of TTL's so check if poll isn't expired
		if poll.TTL.After(now) {
			filteredPolls = append(filteredPolls, poll)
		}
	}

	return filteredPolls, nil
}

func (d *pollsDaoImpl) GetPollsByOwnedBy(ctx context.Context, ownedBy string, statuses []models.PollStatus, sort models.PollSort, direction models.Direction, limit int, lastEvaluatedKey PollsCursor) ([]models.Poll, PollsCursor, error) {
	var polls []models.Poll

	keyCondition := expression.Key("owned_by").Equal(expression.Value(ownedBy))
	exprBuilder := expression.NewBuilder().WithKeyCondition(keyCondition)

	ttlFilter := expression.Name("ttl").GreaterThan(expression.Value(time.Now().Unix()))
	if len(statuses) == 0 {
		exprBuilder = exprBuilder.WithFilter(ttlFilter)
	} else {
		var statusFilters []expression.ConditionBuilder
		for _, status := range statuses {
			statusFilters = append(statusFilters, expression.Name("status").Equal(expression.Value(string(status))))
		}

		// Damn you AWS SDK and not making your OR expression accept varargs straight up
		var filterBuilder expression.ConditionBuilder
		if len(statuses) == 1 {
			filterBuilder = statusFilters[0]
		} else if len(statuses) == 2 {
			filterBuilder = expression.Or(statusFilters[0], statusFilters[1])
		} else {
			filterBuilder = expression.Or(statusFilters[0], statusFilters[1], statusFilters[2:]...)
		}
		exprBuilder = exprBuilder.WithFilter(expression.And(ttlFilter, filterBuilder))
	}

	expr, err := exprBuilder.Build()
	if err != nil {
		return nil, nil, err
	}

	// TODO: Make more indexes than startTime when necessary
	index := ""
	switch sort {
	case models.PollSortStartTime:
		index = OwnedByStartTimeIndex
	default:
		index = OwnedByStartTimeIndex
	}

	queryInput := dynamodb.QueryInput{
		TableName:                 aws.String(d.pollsTableName),
		IndexName:                 aws.String(index),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		KeyConditionExpression:    expr.KeyCondition(),
		Limit:                     aws.Int64(int64(limit)),
		ScanIndexForward:          aws.Bool(direction == models.DirectionAscending),
		ExclusiveStartKey:         lastEvaluatedKey,
	}

	requestGroupKey := fmt.Sprintf("%v:%v:%v:%v:%v:%v", ownedBy, statuses, sort, direction, limit, lastEvaluatedKey)
	v, err, isShared := d.sfGetPollsByOwnedBy.Do(requestGroupKey, func() (interface{}, error) {
		return d.dynamoDBClient.QueryWithContext(ctx, &queryInput)
	})

	if isShared {
		go func() {
			if err := d.stats.Inc("GetPollsByOwnedBy_isShared", 1, 1.0); err != nil {
				logrus.WithError(err).Warn("error sending GetPollsByOwnedBy_isShared metric")
			}
		}()
		r := telemetryext.ReporterWithDimensions(*d.sampleReporter, map[string]string{
			"Method": "Polls_GetPollsByOwnedBy",
		})
		r.Report("SharedResult", 1.0, telemetry.UnitCount)
	}

	if err != nil {
		return nil, nil, errors.InternalError.Wrap(err, "error querying for polls from dynamo")
	}

	results, ok := v.(*dynamodb.QueryOutput)

	if !ok || results == nil {
		// This shouldn't happen
		logrus.WithFields(logrus.Fields{
			"ownedBy":  ownedBy,
			"statuses": statuses,
		}).Error("encountered nil results")
		return nil, nil, nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(results.Items, &polls)
	if err != nil {
		return nil, nil, errors.InternalError.Wrap(err, "error unmarshalling queried polls output from dynamo")
	}

	return polls, results.LastEvaluatedKey, nil
}

func (d *pollsDaoImpl) UpdatePoll(ctx context.Context, pollID string, updateArgs PollUpdateArgs) (models.Poll, error) {
	var poll models.Poll

	expr, err := expression.NewBuilder().WithUpdate(createUpdateExpression(updateArgs)).Build()
	if err != nil {
		return poll, err
	}

	updateItemInput := dynamodb.UpdateItemInput{
		TableName:                 aws.String(d.pollsTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		Key:                       getDynamoHashKey(pollID),
		UpdateExpression:          expr.Update(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := d.daxClient.UpdateItemWithContext(ctx, &updateItemInput)
	if err != nil {
		return poll, err
	}

	err = dynamodbattribute.UnmarshalMap(output.Attributes, &poll)
	if err != nil {
		return poll, err
	}

	return poll, nil
}

func createUpdateExpression(updateArgs PollUpdateArgs) expression.UpdateBuilder {
	updateBuilder := expression.Set(expression.Name("updated_time"), expression.Value(time.Now()))

	if updateArgs.Title != nil {
		updateBuilder = updateBuilder.Set(expression.Name("title"), expression.Value(*updateArgs.Title))
	}

	if updateArgs.StartTime != nil {
		updateBuilder = updateBuilder.Set(expression.Name("start_time"), expression.Value(*updateArgs.StartTime))
	}

	if updateArgs.EndTime != nil {
		updateBuilder = updateBuilder.Set(expression.Name("end_time"), expression.Value(*updateArgs.EndTime))
	}

	if updateArgs.ModeratedTime != nil {
		updateBuilder = updateBuilder.Set(expression.Name("moderated_time"), expression.Value(*updateArgs.ModeratedTime))
	}

	if updateArgs.EndedBy != nil {
		updateBuilder = updateBuilder.Set(expression.Name("ended_by"), expression.Value(*updateArgs.EndedBy))
	}

	if updateArgs.Duration != nil {
		updateBuilder = updateBuilder.Set(expression.Name("duration"), expression.Value(*updateArgs.Duration))
	}

	if updateArgs.Status != nil {
		updateBuilder = updateBuilder.Set(expression.Name("status"), expression.Value(*updateArgs.Status))
	}

	if updateArgs.IsMultiChoiceEnabled != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_multi_choice_enabled"), expression.Value(*updateArgs.IsMultiChoiceEnabled))
	}

	if updateArgs.IsSubscriberOnlyEnabled != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_subscriber_only_enabled"), expression.Value(*updateArgs.IsSubscriberOnlyEnabled))
	}

	if updateArgs.IsSubscriberMultiplierEnabled != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_subscriber_multiplier_enabled"), expression.Value(*updateArgs.IsSubscriberMultiplierEnabled))
	}

	if updateArgs.IsBitsVotesEnabled != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_bits_votes_enabled"), expression.Value(*updateArgs.IsBitsVotesEnabled))
	}

	if updateArgs.BitsVotesCost != nil {
		updateBuilder = updateBuilder.Set(expression.Name("bits_votes_cost"), expression.Value(*updateArgs.BitsVotesCost))
	}

	if updateArgs.IsChannelPointsEnabled != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_channel_points_votes_enabled"), expression.Value(*updateArgs.IsChannelPointsEnabled))
	}

	if updateArgs.ChannelPointsVotesCost != nil {
		updateBuilder = updateBuilder.Set(expression.Name("channel_points_votes_cost"), expression.Value(*updateArgs.ChannelPointsVotesCost))
	}

	if updateArgs.TotalVoters != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_voters"), expression.Value(*updateArgs.TotalVoters))
	}

	if updateArgs.TotalVotes != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_votes"), expression.Value(*updateArgs.TotalVotes))
	}

	if updateArgs.TotalBaseVotes != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_base_votes"), expression.Value(*updateArgs.TotalBaseVotes))
	}

	if updateArgs.TotalBits != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_bits"), expression.Value(*updateArgs.TotalBits))
	}

	if updateArgs.TotalChannelPoints != nil {
		updateBuilder = updateBuilder.Set(expression.Name("total_channel_points"), expression.Value(*updateArgs.TotalChannelPoints))
	}

	if updateArgs.LastUpdatedAggregateTime != nil {
		updateBuilder = updateBuilder.Set(expression.Name("last_updated_aggregate_time"), expression.Value(*updateArgs.LastUpdatedAggregateTime))
	}

	if updateArgs.SlackMessageTimestamp != nil {
		updateBuilder = updateBuilder.Set(expression.Name("slack_message_timestamp"), expression.Value(*updateArgs.SlackMessageTimestamp))
	}

	return updateBuilder
}

func getDynamoHashKey(pollID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		"poll_id": {S: aws.String(pollID)},
	}
}

func (d *pollsDaoImpl) GetTopContributorsFromPantheon(ctx context.Context, pollID string) (*models.TopBitsContributor, *models.TopChannelPointsContributor, error) {
	resp, err := d.pantheonClient.GetLeaderboards(ctx, &pantheonrpc.GetLeaderboardsReq{
		LeaderboardRequests: []*pantheonrpc.GetLeaderboardReq{
			{
				Domain:      models.PollsDomain,
				GroupingKey: models.PollGroupingKey(pollID, models.LeaderboardTypeBits),
				TimeUnit:    pantheonrpc.TimeUnit_ALLTIME,
				TopN:        1,
			},
			{
				Domain:      models.PollsDomain,
				GroupingKey: models.PollGroupingKey(pollID, models.LeaderboardTypeChannelPoints),
				TimeUnit:    pantheonrpc.TimeUnit_ALLTIME,
				TopN:        1,
			},
		},
	})
	if err != nil {
		return nil, nil, errors.InternalError.Wrap(err, "error getting top contributors for pollID %s from pantheon", pollID)
	}

	var bitsContributor *models.TopBitsContributor
	var channelPointsContributor *models.TopChannelPointsContributor
	if resp != nil {
		var bitsTopEntries, channelPointsTopEntries []*pantheonrpc.LeaderboardEntry
		if len(resp.GetLeaderboards()) >= 1 {
			bitsTopEntries = resp.GetLeaderboards()[0].GetTop()
		}
		if len(resp.GetLeaderboards()) >= 2 {
			channelPointsTopEntries = resp.GetLeaderboards()[1].GetTop()
		}

		for _, entry := range bitsTopEntries {
			if entry != nil {
				bitsContributor = &models.TopBitsContributor{
					UserID:          entry.EntryKey,
					BitsContributed: int(entry.Score),
				}
			}
		}

		for _, entry := range channelPointsTopEntries {
			if entry != nil {
				channelPointsContributor = &models.TopChannelPointsContributor{
					UserID:                   entry.EntryKey,
					ChannelPointsContributed: int(entry.Score),
				}
			}
		}
	}

	return bitsContributor, channelPointsContributor, nil
}
