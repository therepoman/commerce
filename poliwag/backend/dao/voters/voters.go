package voters

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
)

const (
	choiceVotersNamePrefix     = "choices_voters"
	pollsVotersTableNamePrefix = "polls_voters"

	pollVotersHashKey         = "poll_id-user_id"
	pollVotersHashKeyFormat   = "%s-%s"
	choiceVotersHashKey       = "poll_id-choice_id-user_id"
	choiceVotersHashKeyFormat = "%s-%s-%s"
)

type VotersCursor = map[string]*dynamodb.AttributeValue

type votersDaoImpl struct {
	choiceVotersTableName string
	pollsVotersTableName  string
	dynamoDBClient        dynamodbiface.DynamoDBAPI
	daxClient             dynamodbiface.DynamoDBAPI
	pantheonClient        pantheonrpc.Pantheon
}

type VotersDAO interface {
	GetVoter(ctx context.Context, pollID string, userID string) (*models.Voter, error)
	SaveVoterAndIncrementTotals(ctx context.Context, pollID string, choiceID string, userID string, baseVotes, bitsVotes, bits, channelPointsVotes, channelPoints int64, hasSubscription bool) (models.Voter, error)
	GetVoters(ctx context.Context, pollID string, sort models.VoterSort, limit int) ([]models.Voter, error)
	GetVotersByChoice(ctx context.Context, pollID string, choiceID string, sort models.VoterSort, limit int) ([]models.Voter, error)
}

func NewVotersDAO(
	env string,
	dynamoDBClient dynamodbiface.DynamoDBAPI,
	daxClient dynamodbiface.DynamoDBAPI,
	pantheonClient pantheonrpc.Pantheon,
) VotersDAO {
	return &votersDaoImpl{
		choiceVotersTableName: fmt.Sprintf("%s-%s", choiceVotersNamePrefix, env),
		pollsVotersTableName:  fmt.Sprintf("%s-%s", pollsVotersTableNamePrefix, env),
		dynamoDBClient:        dynamoDBClient,
		daxClient:             daxClient,
		pantheonClient:        pantheonClient,
	}
}

func getPollVoterKey(pollID string, userID string) map[string]*dynamodb.AttributeValue {
	key := fmt.Sprintf(pollVotersHashKeyFormat, pollID, userID)

	return map[string]*dynamodb.AttributeValue{
		pollVotersHashKey: {
			S: aws.String(key),
		},
	}
}

func getChoiceVoterKey(pollID string, choiceID string, userID string) map[string]*dynamodb.AttributeValue {
	key := fmt.Sprintf(choiceVotersHashKeyFormat, pollID, choiceID, userID)

	return map[string]*dynamodb.AttributeValue{
		choiceVotersHashKey: {
			S: aws.String(key),
		},
	}
}

func newVoter(pollVoter models.PollVoter, choiceVoters []models.ChoiceVoter) models.Voter {
	var voterChoices []models.VoterChoice
	for _, choiceVoter := range choiceVoters {
		voterChoices = append(voterChoices, models.VoterChoice{
			ChoiceID:           choiceVoter.ChoiceID,
			TotalVotes:         choiceVoter.TotalVotes,
			TotalBits:          choiceVoter.TotalBits,
			TotalBaseVotes:     choiceVoter.TotalBaseVotes,
			TotalChannelPoints: choiceVoter.TotalChannelPoints,
		})
	}

	return models.Voter{
		PollID:                  pollVoter.PollID,
		UserID:                  pollVoter.UserID,
		Choices:                 voterChoices,
		TotalVotes:              pollVoter.TotalVotes,
		TotalBaseVotes:          pollVoter.TotalBaseVotes,
		TotalBits:               pollVoter.TotalBits,
		TotalChannelPoints:      pollVoter.TotalChannelPoints,
		HasSubscription:         pollVoter.HasSubscription,
		TotalBitsVotes:          pollVoter.TotalBitsVotes,
		TotalChannelPointsVotes: pollVoter.TotalChannelPointsVotes,
	}
}

func (dao *votersDaoImpl) GetVoter(ctx context.Context, pollID string, userID string) (*models.Voter, error) {
	var pollVoter models.PollVoter

	// Get PollVoter
	key := getPollVoterKey(pollID, userID)
	pollVoterOutput, err := dao.daxClient.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName:      aws.String(dao.pollsVotersTableName),
		Key:            key,
		ConsistentRead: aws.Bool(true),
	})
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting poll voter with key %s from dynamo", key)
	}

	if pollVoterOutput.Item == nil {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalMap(pollVoterOutput.Item, &pollVoter)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error unmarshaling poll voter item to poll voter struct")
	}

	choiceVoters, err := dao.getChoiceVoters(ctx, pollID, pollVoter.ChoiceIDs, userID)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting choice voters")
	}

	// Create the Voter
	voter := newVoter(pollVoter, choiceVoters)

	return &voter, err
}

func (dao *votersDaoImpl) SaveVoterAndIncrementTotals(ctx context.Context, pollID string, choiceID string, userID string, baseVotes, bitsVotes, bits, channelPointsVotes, channelPoints int64, hasSubscription bool) (models.Voter, error) {
	choiceVotersUpdate, err := dao.getChoiceVotersUpdateInput(pollID, choiceID, userID, baseVotes, bitsVotes, bits, channelPointsVotes, channelPoints)
	if err != nil {
		return models.Voter{}, errors.InternalError.Wrap(err, "error building choice voters dynamo update input")
	}

	pollVotersUpdate, err := dao.getPollVotersUpdateInput(pollID, choiceID, userID, baseVotes, bitsVotes, bits, channelPointsVotes, channelPoints, hasSubscription)
	if err != nil {
		return models.Voter{}, errors.InternalError.Wrap(err, "error building poll voters dynamo update input")
	}

	_, err = dao.daxClient.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{
		TransactItems: []*dynamodb.TransactWriteItem{
			{Update: choiceVotersUpdate},
			{Update: pollVotersUpdate},
		},
	})
	if err != nil {
		if transactionCanceledErr, ok := err.(*dynamodb.TransactionCanceledException); ok {
			var reasons []string
			for _, r := range transactionCanceledErr.CancellationReasons {
				if r.Message != nil {
					reasons = append(reasons, *r.Message)
				}
			}

			return models.Voter{}, errors.InternalError.Wrap(transactionCanceledErr, "error performing choice voter and poll voter table update transaction due to transaction cancellation. Reasons: %v", reasons)
		} else if transactionConflictErr, ok := err.(*dynamodb.TransactionConflictException); ok {
			return models.Voter{}, errors.InternalError.Wrap(transactionConflictErr, "error performing choice voter and poll voter table update transaction due to transaction conflict")
		} else {
			return models.Voter{}, errors.InternalError.Wrap(err, "error performing choice voter and poll voter table update transaction")
		}
	}

	voter, err := dao.GetVoter(ctx, pollID, userID)
	if err != nil {
		return models.Voter{}, errors.InternalError.Wrap(err, "error getting voter from dynamo")
	}

	if voter == nil {
		// This shouldn't happen
		msg := "Encountered nil voter after incrementing totals"
		logrus.WithFields(logrus.Fields{
			"poll_id":   pollID,
			"choice_id": choiceID,
			"user_id":   userID,
		}).Error("Encountered nil voter after incrementing totals")
		return models.Voter{}, errors.InternalError.New(msg)
	}

	return *voter, nil
}

func (dao *votersDaoImpl) getChoiceVotersUpdateInput(pollID string, choiceID string, userID string, baseVotes, bitsVotes, bits, channelPointsVotes, channelPoints int64) (*dynamodb.Update, error) {
	exp, err := expression.NewBuilder().WithUpdate(expression.
		Add(expression.Name("total_votes"), expression.Value(baseVotes+bitsVotes+channelPointsVotes)).
		Add(expression.Name("total_base_votes"), expression.Value(baseVotes)).
		Add(expression.Name("total_bits_votes"), expression.Value(bitsVotes)).
		Add(expression.Name("total_bits"), expression.Value(bits)).
		Add(expression.Name("total_channel_points_votes"), expression.Value(channelPointsVotes)).
		Add(expression.Name("total_channel_points"), expression.Value(channelPoints)).
		Set(expression.Name("ttl"), expression.IfNotExists(expression.Name("ttl"), expression.Value(time.Now().AddDate(0, 0, 90).Unix()))).
		Set(expression.Name("poll_id"), expression.IfNotExists(expression.Name("poll_id"), expression.Value(pollID))).
		Set(expression.Name("choice_id"), expression.IfNotExists(expression.Name("choice_id"), expression.Value(choiceID))).
		Set(expression.Name("user_id"), expression.IfNotExists(expression.Name("user_id"), expression.Value(userID)))).Build()
	if err != nil {
		return nil, err
	}

	return &dynamodb.Update{
		TableName:                 aws.String(dao.choiceVotersTableName),
		Key:                       getChoiceVoterKey(pollID, choiceID, userID),
		UpdateExpression:          exp.Update(),
		ExpressionAttributeNames:  exp.Names(),
		ExpressionAttributeValues: exp.Values(),
	}, nil
}

type StringSet struct {
	Value []string
}

func (ss *StringSet) MarshalDynamoDBAttributeValue(av *dynamodb.AttributeValue) error {
	av.SS = aws.StringSlice(ss.Value)
	return nil
}

func (dao *votersDaoImpl) getPollVotersUpdateInput(pollID string, choiceID string, userID string, baseVotes, bitsVotes, bits, channelPointsVotes, channelPoints int64, hasSubscription bool) (*dynamodb.Update, error) {
	exp, err := expression.NewBuilder().WithUpdate(expression.
		Add(expression.Name("total_votes"), expression.Value(baseVotes+bitsVotes+channelPointsVotes)).
		Add(expression.Name("total_base_votes"), expression.Value(baseVotes)).
		Add(expression.Name("total_bits_votes"), expression.Value(bitsVotes)).
		Add(expression.Name("total_bits"), expression.Value(bits)).
		Add(expression.Name("total_channel_points_votes"), expression.Value(channelPointsVotes)).
		Add(expression.Name("total_channel_points"), expression.Value(channelPoints)).
		Add(expression.Name("choice_ids"), expression.Value(&StringSet{Value: []string{choiceID}})).
		Set(expression.Name("ttl"), expression.IfNotExists(expression.Name("ttl"), expression.Value(time.Now().AddDate(0, 0, 90).Unix()))).
		Set(expression.Name("poll_id"), expression.IfNotExists(expression.Name("poll_id"), expression.Value(pollID))).
		Set(expression.Name("user_id"), expression.IfNotExists(expression.Name("user_id"), expression.Value(userID))).
		Set(expression.Name("has_subscription"), expression.IfNotExists(expression.Name("has_subscription"), expression.Value(hasSubscription)))).Build()
	if err != nil {
		return nil, err
	}

	return &dynamodb.Update{
		TableName:                 aws.String(dao.pollsVotersTableName),
		Key:                       getPollVoterKey(pollID, userID),
		UpdateExpression:          exp.Update(),
		ExpressionAttributeNames:  exp.Names(),
		ExpressionAttributeValues: exp.Values(),
	}, nil
}

func (dao *votersDaoImpl) GetVoters(ctx context.Context, pollID string, sort models.VoterSort, limit int) ([]models.Voter, error) {
	lbType, err := models.ToLeaderboardType(sort)
	if err != nil {
		return nil, err
	}

	return dao.getVotersFromPantheon(ctx, pollID, models.PollGroupingKey(pollID, lbType), int64(limit))
}

func (dao *votersDaoImpl) GetVotersByChoice(ctx context.Context, pollID string, choiceID string, sort models.VoterSort, limit int) ([]models.Voter, error) {
	lbType, err := models.ToLeaderboardType(sort)
	if err != nil {
		return nil, err
	}

	return dao.getVotersFromPantheon(ctx, pollID, models.ChoiceGroupingKey(pollID, choiceID, lbType), int64(limit))
}

func (dao *votersDaoImpl) getVotersFromPantheon(ctx context.Context, pollID string, groupingKey string, limit int64) ([]models.Voter, error) {
	resp, err := dao.pantheonClient.GetLeaderboard(ctx, &pantheonrpc.GetLeaderboardReq{
		Domain:      models.PollsDomain,
		GroupingKey: groupingKey,
		TimeUnit:    pantheonrpc.TimeUnit_ALLTIME,
		TopN:        limit,
	})
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting voter leaderboard from pantheon")
	}

	voterIDs := make([]string, 0)
	if resp != nil {
		for _, entry := range resp.Top {
			if entry != nil {
				voterIDs = append(voterIDs, entry.EntryKey)
			}
		}
	}

	pollVoters, err := dao.getPollVoters(ctx, pollID, voterIDs)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting poll voters")
	}

	voters, err := dao.getVotersFromPollVoters(ctx, pollVoters)
	if err != nil {
		return nil, errors.InternalError.Wrap(err, "error getting voters from poll voters")
	}

	// BatchGetItems does not return items in the same order, so to maintain order we need to sort manually
	mappedVoters := make(map[string]models.Voter)
	for _, voter := range voters {
		mappedVoters[voter.UserID] = voter
	}

	sortedVoters := make([]models.Voter, 0)
	for _, voterID := range voterIDs {
		sortedVoters = append(sortedVoters, mappedVoters[voterID])
	}

	return sortedVoters, nil
}

func (dao *votersDaoImpl) getPollVoters(ctx context.Context, pollID string, voterIDs []string) ([]models.PollVoter, error) {
	var pollVoters []models.PollVoter

	if len(voterIDs) == 0 {
		return pollVoters, nil
	}

	var keys []map[string]*dynamodb.AttributeValue
	for _, voterID := range voterIDs {
		keys = append(keys, getPollVoterKey(pollID, voterID))
	}

	output, err := dao.daxClient.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			dao.pollsVotersTableName: {
				Keys:           keys,
				ConsistentRead: aws.Bool(true),
			},
		},
	})
	if err != nil {
		return pollVoters, err
	}

	if output.Responses == nil || output.Responses[dao.pollsVotersTableName] == nil {
		logrus.WithFields(logrus.Fields{
			"voterIDs": voterIDs,
		}).Error("encountered nil response when performing batch get on polls voters table")
		return pollVoters, nil
	}
	tableResponse := output.Responses[dao.pollsVotersTableName]
	err = dynamodbattribute.UnmarshalListOfMaps(tableResponse, &pollVoters)
	if err != nil {
		return pollVoters, err
	}

	return pollVoters, nil
}

func (dao *votersDaoImpl) getVotersFromPollVoters(ctx context.Context, pollVoters []models.PollVoter) ([]models.Voter, error) {
	var voters []models.Voter
	for _, pollVoter := range pollVoters {
		choiceVoters, err := dao.getChoiceVoters(ctx, pollVoter.PollID, pollVoter.ChoiceIDs, pollVoter.UserID)
		if err != nil {
			return nil, err
		}

		voters = append(voters, newVoter(pollVoter, choiceVoters))
	}

	return voters, nil
}

func (dao *votersDaoImpl) getChoiceVoters(ctx context.Context, pollID string, choiceIDs []string, userID string) ([]models.ChoiceVoter, error) {
	var choiceVoters []models.ChoiceVoter

	// Short circuit if empty choiceIDs
	if len(choiceIDs) == 0 {
		return choiceVoters, nil
	}

	var choiceVoterKeys []map[string]*dynamodb.AttributeValue
	for _, choiceID := range choiceIDs {
		choiceVoterKeys = append(choiceVoterKeys, getChoiceVoterKey(pollID, choiceID, userID))
	}

	choiceVoterOutput, err := dao.daxClient.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			dao.choiceVotersTableName: {
				Keys:           choiceVoterKeys,
				ConsistentRead: aws.Bool(true),
			},
		},
	})
	if err != nil {
		return nil, err
	}

	if choiceVoterOutput.UnprocessedKeys != nil && choiceVoterOutput.UnprocessedKeys[dao.choiceVotersTableName] != nil {
		// TODO: We need to handle this scenario, for now let's log an error for auditing

		logrus.WithFields(logrus.Fields{
			"pollID": pollID,
			"userID": userID,
			"keys":   choiceVoterOutput.UnprocessedKeys[dao.choiceVotersTableName].Keys,
		}).Error("encountered unprocessed keys while getting choice voters")
	}

	if choiceVoterOutput.Responses == nil || choiceVoterOutput.Responses[dao.choiceVotersTableName] == nil {
		return nil, nil
	}

	choiceVoterResponse := choiceVoterOutput.Responses[dao.choiceVotersTableName]

	err = dynamodbattribute.UnmarshalListOfMaps(choiceVoterResponse, &choiceVoters)
	if err != nil {
		return nil, err
	}

	return choiceVoters, nil
}
