package votes

import (
	"context"
	"time"

	"github.com/avast/retry-go"
	"github.com/jinzhu/gorm"

	"code.justin.tv/commerce/poliwag/backend/utils/hystrix"
	gorm_client "code.justin.tv/commerce/poliwag/clients/aurora/gorm"
)

type Vote struct {
	PollID             string `gorm:"primary_key;index:pollchoice"`
	VoteID             string `gorm:"primary_key"`
	ChoiceID           string `gorm:"index:pollchoice"`
	UserID             string `gorm:"index:userid_index"`
	SubscriberStatus   string
	TotalVotes         int64 `gorm:"default:0"`
	BaseVotes          int64 `gorm:"default:0"`
	BitsVotes          int64 `gorm:"default:0"`
	Bits               int64 `gorm:"default:0"`
	ChannelPointsVotes int64 `gorm:"default:0"`
	ChannelPoints      int64 `gorm:"default:0"`
	CreatedAt          time.Time
	UpdatedAt          time.Time
	DeletedAt          *time.Time
}

type ChoiceAggregation struct {
	ChoiceID           string
	TotalVoters        int
	TotalVotes         int
	TotalBits          int
	TotalChannelPoints int
	TotalBaseVotes     int
}

type PollAggregation struct {
	PollID             string
	TotalVoters        int
	TotalVotes         int
	TotalBits          int
	TotalChannelPoints int
	TotalBaseVotes     int
}

type DAO interface {
	GetVote(ctx context.Context, pollID string, voteID string, includeDeleted bool) (*Vote, error)
	CreateVote(ctx context.Context, vote Vote) error
	DeleteVote(ctx context.Context, pollID string, voteID string) error
	DeleteVotesForUsers(ctx context.Context, userIDs []string) error
	GetChoiceAggregations(ctx context.Context, pollID string) ([]ChoiceAggregation, error)
	GetPollAggregation(ctx context.Context, pollID string) (PollAggregation, error)
}

type dao struct {
	gormClient gorm_client.Client
}

func NewDAO(gormClient gorm_client.Client) DAO {
	return &dao{
		gormClient: gormClient,
	}
}

func (dao *dao) CreateVote(ctx context.Context, vote Vote) error {
	return dao.gormClient.Create(ctx, &vote)
}

func (dao *dao) GetVote(ctx context.Context, pollID string, voteID string, includeDeleted bool) (*Vote, error) {
	var vote *Vote

	err := retry.Do(func() error {
		return hystrix.HystrixDo(hystrix.VotesGetVote, func() error {
			var innerVote Vote
			var innerError error
			if includeDeleted {
				innerError = dao.gormClient.FirstIncludingDeleted(ctx, &innerVote, "poll_id = ? AND vote_id = ?", pollID, voteID)
			} else {
				innerError = dao.gormClient.First(ctx, &innerVote, "poll_id = ? AND vote_id = ?", pollID, voteID)
			}
			if innerError != nil {
				if gorm.IsRecordNotFoundError(innerError) {
					return nil
				}
				return innerError
			}

			vote = &innerVote

			return nil
		}, nil)
	}, retry.Context(ctx), retry.Attempts(3))

	return vote, err
}

func (dao *dao) DeleteVote(ctx context.Context, pollID string, voteID string) error {
	return hystrix.HystrixDo(hystrix.VotesDeleteVote, func() error {
		return dao.gormClient.Delete(ctx, &Vote{
			PollID: pollID,
			VoteID: voteID,
		})
	}, nil)
}

func (dao *dao) GetChoiceAggregations(ctx context.Context, pollID string) ([]ChoiceAggregation, error) {
	var aggregations *[]ChoiceAggregation

	err := hystrix.HystrixDo(hystrix.VotesGetChoiceAggregations, func() error {
		var innerAggregations []ChoiceAggregation
		query := dao.gormClient.DB().
			Table("votes").
			Select("choice_id, COUNT(DISTINCT user_id) as total_voters, SUM(total_votes) as total_votes, SUM(bits) as total_bits, SUM(channel_points) as total_channel_points, SUM(base_votes) as total_base_votes").
			Where("poll_id = ? AND deleted_at IS NULL", pollID).
			Group("choice_id").
			Scan(&innerAggregations)
		if query.Error != nil {
			return query.Error
		}
		aggregations = &innerAggregations
		return nil
	}, nil)

	if aggregations != nil {
		return *aggregations, err
	}
	return []ChoiceAggregation{}, err
}

func (dao *dao) GetPollAggregation(ctx context.Context, pollID string) (PollAggregation, error) {
	aggregation := &PollAggregation{
		PollID: pollID,
	}

	err := hystrix.HystrixDo(hystrix.VotesGetPollAggregation, func() error {
		innerAggregation := PollAggregation{
			PollID: pollID,
		}
		query := dao.gormClient.DB().
			Table("votes").
			Select("poll_id, COUNT(DISTINCT user_id) as total_voters, SUM(total_votes) as total_votes, SUM(bits) as total_bits, SUM(channel_points) as total_channel_points, SUM(base_votes) as total_base_votes").
			Where("poll_id = ? AND deleted_at IS NULL", pollID).
			First(&innerAggregation)
		if query.Error != nil {
			return query.Error
		}
		aggregation = &innerAggregation
		return nil
	}, nil)

	if aggregation != nil {
		return *aggregation, err
	}
	return PollAggregation{
		PollID: pollID,
	}, err
}

func (dao *dao) DeleteVotesForUsers(ctx context.Context, userIDs []string) error {
	// 'Unscoped' actually performs a hard delete, making the query "DELETE FROM votes WHERE user_id IN (userIDs)"
	return dao.gormClient.DB().Unscoped().Delete(Vote{}, "user_id IN (?)", userIDs).Error
}
