package models

import "time"

const ViewableDuration = 2 * time.Minute

type Poll struct {
	// Information
	PollID        string        `dynamodbav:"poll_id"` // Hash Key
	OwnedBy       string        `dynamodbav:"owned_by"`
	CreatedBy     string        `dynamodbav:"created_by"`
	Title         string        `dynamodbav:"title"`
	StartTime     time.Time     `dynamodbav:"start_time"`
	EndTime       *time.Time    `dynamodbav:"end_time,omitempty"`
	ModeratedTime *time.Time    `dynamodbav:"moderated_time,omitempty"`
	EndedBy       string        `dynamodbav:"ended_by,omitempty"`
	Duration      time.Duration `dynamodbav:"duration"`
	Status        PollStatus    `dynamodbav:"status"`
	CreatedTime   time.Time     `dynamodbav:"created_time"`
	UpdatedTime   time.Time     `dynamodbav:"updated_time"`
	TTL           time.Time     `dynamodbav:"ttl,unixtime"`

	// Settings
	IsMultiChoiceEnabled          bool `dynamodbav:"is_multi_choice_enabled"`
	IsSubscriberOnlyEnabled       bool `dynamodbav:"is_subscriber_only_enabled"`
	IsSubscriberMultiplierEnabled bool `dynamodbav:"is_subscriber_multiplier_enabled"`
	IsBitsVotesEnabled            bool `dynamodbav:"is_bits_votes_enabled"`
	BitsVotesCost                 int  `dynamodbav:"bits_votes_cost"`
	IsChannelPointsVotesEnabled   bool `dynamodbav:"is_channel_points_votes_enabled"`
	ChannelPointsVotesCost        int  `dynamodbav:"channel_points_votes_cost"`

	// Aggregations
	TotalVoters              int        `dynamodbav:"total_voters"`
	TotalVotes               int        `dynamodbav:"total_votes"`
	TotalBaseVotes           int        `dynamodbav:"total_base_votes"`
	TotalBits                int        `dynamodbav:"total_bits"`
	TotalChannelPoints       int        `dynamodbav:"total_channel_points"`
	LastUpdatedAggregateTime *time.Time `dynamodbav:"last_updated_aggregate_time"`

	// Slack
	SlackMessageTimestamp string `dynamodbav:"slack_message_timestamp"`
}

type Choice struct {
	ChoiceID           string    `dynamodbav:"choice_id"` // Hash Key
	PollID             string    `dynamodbav:"poll_id"`
	Title              string    `dynamodbav:"title"`
	Index              int       `dynamodbav:"index"`
	TotalVoters        int       `dynamodbav:"total_voters"`
	TotalVotes         int       `dynamodbav:"total_votes"`
	TotalBaseVotes     int       `dynamodbav:"total_base_votes"`
	TotalBits          int       `dynamodbav:"total_bits"`
	TotalChannelPoints int       `dynamodbav:"total_channel_points"`
	CreatedTime        time.Time `dynamodbav:"created_time"`
	UpdatedTime        time.Time `dynamodbav:"updated_time"`
	TTL                time.Time `dynamodbav:"ttl,unixtime"`
}

type PollStatus string

const (
	PollStatusActive     = PollStatus("ACTIVE")
	PollStatusTerminated = PollStatus("TERMINATED")
	PollStatusCompleted  = PollStatus("COMPLETED")
	PollStatusArchived   = PollStatus("ARCHIVED")
	PollStatusModerated  = PollStatus("MODERATED")
)

// Statuses that are publicly viewable to all users
var PubliclyViewablePollStatuses = []PollStatus{PollStatusActive, PollStatusTerminated, PollStatusCompleted}

// Statuses that a broadcaster can view
var BroadcasterViewablePollStatuses = []PollStatus{PollStatusActive, PollStatusTerminated, PollStatusCompleted, PollStatusArchived}

// All statuses
var AllPollStatuses = []PollStatus{PollStatusActive, PollStatusTerminated, PollStatusCompleted, PollStatusArchived, PollStatusModerated}

type PollSort string

const (
	PollSortStartTime PollSort = "START_TIME"
)

type Direction string

const (
	DirectionDescending Direction = "DESC"
	DirectionAscending  Direction = "ASC"
)

type PollVoter struct {
	PollIDUserID            string    `dynamodbav:"poll_id-user_id"` // Hash Key
	PollID                  string    `dynamodbav:"poll_id"`
	ChoiceIDs               []string  `dynamodbav:"choice_ids"`
	UserID                  string    `dynamodbav:"user_id"`
	HasSubscription         bool      `dynamodbav:"has_subscription"`
	TotalVotes              int       `dynamodbav:"total_votes"`
	TotalBaseVotes          int       `dynamodbav:"total_base_votes"`
	TotalBitsVotes          int       `dynamodbav:"total_bits_votes"`
	TotalBits               int       `dynamodbav:"total_bits"`
	TotalChannelPointsVotes int       `dynamodbav:"total_channel_points_votes"`
	TotalChannelPoints      int       `dynamodbav:"total_channel_points"`
	CreatedTime             time.Time `dynamodbav:"created_time"`
	UpdatedTime             time.Time `dynamodbav:"updated_time"`
	TTL                     time.Time `dynamodbav:"ttl,unixtime"`
}

type ChoiceVoter struct {
	PollIDChoiceIDUserID    string    `dynamodbav:"poll_id-choice_id-user_id"` // Hash Key
	PollID                  string    `dynamodbav:"poll_id"`
	ChoiceID                string    `dynamodbav:"choice_id"`
	UserID                  string    `dynamodbav:"user_id"`
	TotalVotes              int       `dynamodbav:"total_votes"`
	TotalBaseVotes          int       `dynamodbav:"total_base_votes"`
	TotalBitsVotes          int       `dynamodbav:"total_bits_votes"`
	TotalBits               int       `dynamodbav:"total_bits"`
	TotalChannelPointsVotes int       `dynamodbav:"total_channel_points_votes"`
	TotalChannelPoints      int       `dynamodbav:"total_channel_points"`
	CreatedTime             time.Time `dynamodbav:"created_time"`
	UpdatedTime             time.Time `dynamodbav:"updated_time"`
	TTL                     time.Time `dynamodbav:"ttl,unixtime"`
}

type Voter struct {
	PollID                  string
	Choices                 []VoterChoice
	UserID                  string
	HasSubscription         bool
	TotalVotes              int
	TotalBaseVotes          int
	TotalBitsVotes          int
	TotalBits               int
	TotalChannelPointsVotes int
	TotalChannelPoints      int
}

type VoterChoice struct {
	ChoiceID           string
	TotalVotes         int
	TotalBaseVotes     int
	TotalBits          int
	TotalChannelPoints int
}

type VoterSort string

const (
	VoterSortVotes         VoterSort = "VOTES"
	VoterSortBits          VoterSort = "BITS"
	VoterSortChannelPoints VoterSort = "CHANNEL_POINTS"
)

type TokenType string

const (
	TokenTypeBits          TokenType = "BITS"
	TokenTypeChannelPoints TokenType = "CHANNEL_POINTS"
)

var AllTokenTypes = map[TokenType]interface{}{
	TokenTypeBits:          nil,
	TokenTypeChannelPoints: nil,
}

func IsPollViewable(p Poll, referenceTime time.Time, viewableDuration time.Duration) bool {
	switch p.Status {
	case PollStatusActive:
		return true
	case PollStatusCompleted:
		fallthrough
	case PollStatusTerminated:
		if p.EndTime != nil {
			unviewableTime := p.EndTime.Add(viewableDuration)
			return referenceTime.Before(unviewableTime)
		}
		fallthrough
	case PollStatusModerated:
		fallthrough
	default:
		return false
	}
}

func GetTotalBitsVotesForChoice(poll Poll, choice Choice) int {
	if poll.BitsVotesCost == 0 {
		return 0
	}

	return choice.TotalBits / poll.BitsVotesCost
}

func GetTotalBitsVotesForVoter(poll Poll, voter Voter) int {
	if poll.BitsVotesCost == 0 {
		return 0
	}

	return voter.TotalBits / poll.BitsVotesCost
}

func GetTotalBitsVotesForVoterChoice(poll Poll, voterChoice VoterChoice) int {
	if poll.BitsVotesCost == 0 {
		return 0
	}

	return voterChoice.TotalBits / poll.BitsVotesCost
}

type TopBitsContributor struct {
	UserID          string
	BitsContributed int
	DisplayName     *string
}

func GetTotalChannelPointsVotesForChoice(poll Poll, choice Choice) int {
	if poll.ChannelPointsVotesCost == 0 {
		return 0
	}

	return choice.TotalChannelPoints / poll.ChannelPointsVotesCost
}

func GetTotalChannelPointsVotesForVoter(poll Poll, voter Voter) int {
	if poll.ChannelPointsVotesCost == 0 {
		return 0
	}

	return voter.TotalChannelPoints / poll.ChannelPointsVotesCost
}

func GetTotalChannelPointsVotesForVoterChoice(poll Poll, voterChoice VoterChoice) int {
	if poll.ChannelPointsVotesCost == 0 {
		return 0
	}

	return voterChoice.TotalChannelPoints / poll.ChannelPointsVotesCost
}

type TopChannelPointsContributor struct {
	UserID                   string
	ChannelPointsContributed int
	DisplayName              *string
}
