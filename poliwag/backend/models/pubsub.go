package models

// We originally serialized the proto Poll struct, but the default JSON serialization results in cumbersome and error-prone structs for clients
// Since there is no simple way to override protobuf's JSON serialization, we decided to create our own struct explicitly
type PubsubPoll struct {
	PollID    string `json:"poll_id"`
	OwnedBy   string `json:"owned_by"`
	CreatedBy string `json:"created_by"`
	Title     string `json:"title"`
	// ISO String
	StartedAt string `json:"started_at"`
	// ISO String
	EndedAt         *string `json:"ended_at"`
	EndedBy         *string `json:"ended_by"`
	DurationSeconds int     `json:"duration_seconds"`

	Settings PubsubPollSettings `json:"settings"`
	Status   PollStatus         `json:"status"`

	Choices []PubsubChoice `json:"choices"`
	Votes   PubsubVotes    `json:"votes"`
	Tokens  PubsubTokens   `json:"tokens"`

	TotalVoters                   int `json:"total_voters"`
	RemainingDurationMilliseconds int `json:"remaining_duration_milliseconds"`

	// Deprecated: use TopBitsContributor instead
	TopContributor *PubsubTopBitsContributor `json:"top_contributor"`

	TopBitsContributor          *PubsubTopBitsContributor          `json:"top_bits_contributor"`
	TopChannelPointsContributor *PubsubTopChannelPointsContributor `json:"top_channel_points_contributor"`
}

type PubsubPollSettingsMultiChoice struct {
	IsEnabled bool `json:"is_enabled"`
}

type PubsubPollSettingsSubscriberOnly struct {
	IsEnabled bool `json:"is_enabled"`
}

type PubsubPollSettingsSubscriberMultiplier struct {
	IsEnabled bool `json:"is_enabled"`
}

type PubsubPollSettingsBitsVotes struct {
	IsEnabled bool `json:"is_enabled"`
	Cost      int  `json:"cost"`
}

type PubsubPollSettingsChannelPointsVotes struct {
	IsEnabled bool `json:"is_enabled"`
	Cost      int  `json:"cost"`
}

type PubsubPollSettings struct {
	MultiChoice          PubsubPollSettingsMultiChoice          `json:"multi_choice"`
	SubscriberOnly       PubsubPollSettingsSubscriberOnly       `json:"subscriber_only"`
	SubscriberMultiplier PubsubPollSettingsSubscriberMultiplier `json:"subscriber_multiplier"`
	BitsVotes            PubsubPollSettingsBitsVotes            `json:"bits_votes"`
	ChannelPointsVotes   PubsubPollSettingsChannelPointsVotes   `json:"channel_points_votes"`
}

type PubsubChoice struct {
	ChoiceID    string       `json:"choice_id"`
	Title       string       `json:"title"`
	Votes       PubsubVotes  `json:"votes"`
	Tokens      PubsubTokens `json:"tokens"`
	TotalVoters int          `json:"total_voters"`
}

type PubsubVotes struct {
	Total         int `json:"total"`
	Bits          int `json:"bits"`
	ChannelPoints int `json:"channel_points"`
	Base          int `json:"base"`
}

type PubsubTokens struct {
	Bits          int `json:"bits"`
	ChannelPoints int `json:"channel_points"`
}

type PubsubTopBitsContributor struct {
	UserID          string `json:"user_id"`
	DisplayName     string `json:"display_name"`
	BitsContributed int    `json:"bits_contributed"`
}

type PubsubTopChannelPointsContributor struct {
	UserID                   string `json:"user_id"`
	DisplayName              string `json:"display_name"`
	ChannelPointsContributed int    `json:"channel_points_contributed"`
}

type PubsubEvent struct {
	Type PubsubEventType `json:"type"`
	Data interface{}     `json:"data"`
}

type PubsubEventType string

const (
	PubsubEventTypePollCreate    = PubsubEventType("POLL_CREATE")
	PubsubEventTypePollUpdate    = PubsubEventType("POLL_UPDATE")
	PubsubEventTypePollComplete  = PubsubEventType("POLL_COMPLETE")
	PubsubEventTypePollTerminate = PubsubEventType("POLL_TERMINATE")
	PubsubEventTypePollArchive   = PubsubEventType("POLL_ARCHIVE")
	PubsubEventTypePollModerate  = PubsubEventType("POLL_MODERATE")
)

type PubsubEventDataPollCreate struct {
	Poll PubsubPoll `json:"poll"`
}

type PubsubEventDataPollUpdate struct {
	Poll PubsubPoll `json:"poll"`
}

type PubsubEventDataPollComplete struct {
	Poll PubsubPoll `json:"poll"`
}

type PubsubEventDataPollTerminate struct {
	Poll PubsubPoll `json:"poll"`
}

type PubsubEventDataPollArchive struct {
	Poll PubsubPoll `json:"poll"`
}

type PubsubEventDataPollModerate struct {
	Poll PubsubPoll `json:"poll"`
}
