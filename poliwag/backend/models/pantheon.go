package models

import (
	"errors"
	"fmt"
)

type LeaderboardType string

const (
	LeaderboardTypeVotes         = LeaderboardType("votes")
	LeaderboardTypeBits          = LeaderboardType("bits")
	LeaderboardTypeChannelPoints = LeaderboardType("channel_points")
	PollsDomain                  = "polls"
)

func ToLeaderboardType(voterSort VoterSort) (LeaderboardType, error) {
	switch voterSort {
	case VoterSortBits:
		return LeaderboardTypeBits, nil
	case VoterSortChannelPoints:
		return LeaderboardTypeChannelPoints, nil
	case VoterSortVotes:
		return LeaderboardTypeVotes, nil
	default:
		return "", errors.New("cannot convert VoterSort to leaderboardType")
	}
}

func PollGroupingKey(pollID string, lbType LeaderboardType) string {
	return fmt.Sprintf("%s:%s", pollID, lbType)
}

func ChoiceGroupingKey(pollID string, choiceID string, lbType LeaderboardType) string {
	return fmt.Sprintf("%s-%s:%s", pollID, choiceID, lbType)
}
