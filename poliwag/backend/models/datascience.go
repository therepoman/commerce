package models

const (
	ZERO_CHOICE_METRIC_NAME      = "ZeroChoices"
	VOTE_AUDITLOGGER_METRIC_NAME = "Vote_AuditLogger"
)

type CreatePollProperties struct {
	UserID               string `json:"user_id"`
	ChannelID            string `json:"channel_id"`
	PollID               string `json:"poll_id"`
	PollQuestion         string `json:"poll_question"`
	PollResponse1        string `json:"poll_response_1"`
	PollResponse2        string `json:"poll_response_2"`
	PollResponse3        string `json:"poll_response_3"`
	PollResponse4        string `json:"poll_response_4"`
	PollResponse5        string `json:"poll_response_5"`
	TotalResponses       int    `json:"total_responses"`
	BitsPerVote          int    `json:"bits_per_vote"`
	ChannelPointsPerVote int    `json:"channel_points_per_vote"`
	HasSubscriberBonus   bool   `json:"has_subscriber_bonus"`
	IsSubscriberOnly     bool   `json:"is_subscriber_only"`
	PollDurationSeconds  int    `json:"poll_duration_seconds"`
}

type EndPollProperties struct {
	UserID    string `json:"user_id"`
	ChannelID string `json:"channel_id"`
	PollID    string `json:"poll_id"`
}

type DeletePollProperties struct {
	UserID    string `json:"user_id"`
	ChannelID string `json:"channel_id"`
	PollID    string `json:"poll_id"`
}

type VoteProperties struct {
	// UserID of user performing vote
	UserID string `json:"user_id"`

	// UserID of channel vote is performed in
	ChannelID string `json:"channel_id"`

	// ID of Poll voted in
	PollID string `json:"poll_id"`

	// Index of choice user voted in (NOTE: 1 INDEXED!)
	PollResponse int `json:"poll_response"`

	// Amount of bits spent in vote. 0 if no bits spent.
	BitsSpent int `json:"bits_spent"`

	// Amount of channel point sspent in vote. 0 if no channel point spent.
	ChannelPointsSpent int `json:"channel_points_spent"`
}
