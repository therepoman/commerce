package models

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsPollViewable(t *testing.T) {
	Convey("given a poll id", t, func() {
		pollID := "test-poll-id"

		Convey("when the status is active", func() {
			status := PollStatusActive

			Convey("we should return true", func() {
				poll := Poll{
					PollID: pollID,
					Status: status,
				}

				actualIsPollViewable := IsPollViewable(poll, time.Unix(0, 0), time.Duration(10)*time.Minute)
				So(actualIsPollViewable, ShouldBeTrue)
			})
		})

		Convey("when the status is completed", func() {
			status := PollStatusCompleted
			endTime := time.Unix(1561486677, 0)

			Convey("when the reference time is within viewable duration from the end time", func() {
				viewableDuration := 5 * time.Minute
				referenceTime := endTime.Add(1 * time.Minute)

				Convey("we should return true", func() {
					poll := Poll{
						PollID:  pollID,
						Status:  status,
						EndTime: &endTime,
					}

					actualIsPollViewable := IsPollViewable(poll, referenceTime, viewableDuration)
					So(actualIsPollViewable, ShouldBeTrue)
				})
			})

			Convey("when the reference time is not within viewable duration from the end time", func() {
				viewableDuration := 5 * time.Minute
				referenceTime := endTime.Add(100 * time.Minute)

				Convey("we should return false", func() {
					poll := Poll{
						PollID:  pollID,
						Status:  status,
						EndTime: &endTime,
					}

					actualIsPollViewable := IsPollViewable(poll, referenceTime, viewableDuration)
					So(actualIsPollViewable, ShouldBeFalse)
				})
			})
		})

		Convey("when the status is terminated", func() {
			status := PollStatusTerminated
			endTime := time.Unix(1561486677, 0)

			Convey("when the reference time is within viewable duration from the end time", func() {
				viewableDuration := 5 * time.Minute
				referenceTime := endTime.Add(1 * time.Minute)

				Convey("we should return true", func() {
					poll := Poll{
						PollID:  pollID,
						Status:  status,
						EndTime: &endTime,
					}

					actualIsPollViewable := IsPollViewable(poll, referenceTime, viewableDuration)
					So(actualIsPollViewable, ShouldBeTrue)
				})
			})

			Convey("when the reference time is not within viewable duration from the end time", func() {
				viewableDuration := 5 * time.Minute
				referenceTime := endTime.Add(100 * time.Minute)

				Convey("we should return false", func() {
					poll := Poll{
						PollID:  pollID,
						Status:  status,
						EndTime: &endTime,
					}

					actualIsPollViewable := IsPollViewable(poll, referenceTime, viewableDuration)
					So(actualIsPollViewable, ShouldBeFalse)
				})
			})
		})

		Convey("when the status is archived", func() {
			status := PollStatusArchived

			Convey("we should return false", func() {
				poll := Poll{
					PollID: pollID,
					Status: status,
				}

				actualIsPollViewable := IsPollViewable(poll, time.Unix(0, 0), time.Duration(10)*time.Minute)
				So(actualIsPollViewable, ShouldBeFalse)
			})
		})
	})
}

func TestGetTotalBitsVotesForChoice(t *testing.T) {
	Convey("given a poll and choice", t, func() {
		poll := Poll{
			PollID: "test-poll-id",
		}

		choice := Choice{
			ChoiceID:  "test-choice-id",
			TotalBits: 100,
		}

		Convey("when bits votes cost is 0", func() {
			poll.BitsVotesCost = 0

			Convey("we should return 0", func() {
				actualTotalBitsVotesForChoice := GetTotalBitsVotesForChoice(poll, choice)

				So(actualTotalBitsVotesForChoice, ShouldEqual, 0)
			})
		})

		Convey("when bits votes cost is non-zero", func() {
			poll.BitsVotesCost = 10

			Convey("we should return the value", func() {
				actualTotalBitsVotesForChoice := GetTotalBitsVotesForChoice(poll, choice)

				So(actualTotalBitsVotesForChoice, ShouldEqual, 100/10)
			})
		})
	})
}
