package models

type Vote struct {
	PollID   string `json:"poll_id"`
	VoteID   string `json:"vote_id"`
	UserID   string `json:"user_id"`
	ChoiceID string `json:"choice_id"`
}
