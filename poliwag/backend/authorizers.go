package backend

import (
	"github.com/go-playground/validator/v10"

	"code.justin.tv/commerce/poliwag/backend/authorization/channel_editor"
	"code.justin.tv/commerce/poliwag/backend/authorization/poll_editor"
	"code.justin.tv/commerce/poliwag/backend/authorization/poll_viewer"
	"code.justin.tv/commerce/poliwag/backend/authorization/poll_voter"
	"code.justin.tv/commerce/poliwag/backend/authorization/voter_viewer"
)

type authorizers struct {
	channelEditorAuthorizer channel_editor.Authorizer `validate:"required"`
	pollEditorAuthorizer    poll_editor.Authorizer    `validate:"required"`
	voterViewerAuthorizer   voter_viewer.Authorizer   `validate:"required"`
	pollViewerAuthorizer    poll_viewer.Authorizer    `validate:"required"`
	pollVoterAuthorizer     poll_voter.Authorizer     `validate:"required"`
}

func createAuthorizers(c *clients, d *daos) (*authorizers, error) {
	channelEditorAuthorizer := channel_editor.NewAuthorizer(c.zuma, c.hallpass, c.users)
	pollEditorAuthorizer := poll_editor.NewAuthorizer(c.hallpass, d.polls, channelEditorAuthorizer)
	out := &authorizers{
		channelEditorAuthorizer: channelEditorAuthorizer,
		pollEditorAuthorizer:    pollEditorAuthorizer,
		voterViewerAuthorizer:   voter_viewer.NewAuthorizer(d.polls, pollEditorAuthorizer),
		pollViewerAuthorizer:    poll_viewer.NewAuthorizer(d.polls, pollEditorAuthorizer),
		pollVoterAuthorizer:     poll_voter.NewAuthorizer(d.polls, c.tmi),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
