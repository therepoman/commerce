package vote_poll_update

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/bsm/redislock"
	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/dao/choices"
	"code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	poliwag_error "code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	workeri "code.justin.tv/commerce/poliwag/backend/worker"
	"code.justin.tv/commerce/poliwag/clients/lock"
)

const (
	timeout = 5 * time.Second
)

type worker struct {
	votesDAO           votes.DAO
	pollsDAO           polls.PollsDAO
	choicesDAO         choices.ChoicesDAO
	pubsubController   pubsub.Controller
	eventbusController eventbus.Controller
	lockingClient      lock.LockingClient
}

func NewWorker(
	votesDAO votes.DAO,
	pollsDAO polls.PollsDAO,
	choicesDAO choices.ChoicesDAO,
	pubsubController pubsub.Controller,
	eventbusController eventbus.Controller,
	lockingClient lock.LockingClient,
) workeri.Worker {
	return &worker{
		votesDAO:           votesDAO,
		pollsDAO:           pollsDAO,
		choicesDAO:         choicesDAO,
		pubsubController:   pubsubController,
		eventbusController: eventbusController,
		lockingClient:      lockingClient,
	}
}

func (w *worker) Handle(msg *sqs.Message) error {
	runTime := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	snsMsg, err := sns.Extract(msg)
	if err != nil {
		logrus.WithError(err).Error("error extracting sns message from sqs message")
		return err
	}

	var snsVote models.Vote
	err = json.Unmarshal([]byte(snsMsg.Message), &snsVote)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error unmarshaling sns message into vote")
		return err
	}

	vote, err := w.votesDAO.GetVote(ctx, snsVote.PollID, snsVote.VoteID, true)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error getting vote from rds")
		return err
	}

	if vote == nil {
		logrus.WithField("snsVote", snsVote).Error("vote not found in rds")
		return errors.New("vote not found")
	}

	if vote.DeletedAt != nil {
		logrus.WithFields(logrus.Fields{"pollID": snsVote.PollID, "voteID": snsVote.VoteID}).Info("vote was deleted")
		return nil
	}

	// Using eventually consistent reads here introduces two possible race conditions:
	//  * A poll's status is no longer active, but the cache believes it is. In this case, we would potentially tabulate poll
	//    totals an extra time. We use a locking mechanism to prevent concurrent updates of the same poll, so this shouldn't be a concern.
	//  * The cache has a stale value for LastUpdatedAggregateTime. In this case, we would potentially tabulate poll totals an extra time.
	//    Similar to the above, our locking mechanism should prevent this from being a concern.
	// We are opting to tolerate these risks to save us from hot spotting that we were seeing during large
	// bursts of a votes for a single poll.
	poll, err := w.pollsDAO.GetPoll(ctx, snsVote.PollID, false)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error getting poll")
		return err
	}

	if poll == nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).Error("no poll found for given PollID")
		return errors.New("no poll found for PollID " + snsVote.PollID)
	}

	if poll.Status != models.PollStatusActive || time.Now().After(poll.StartTime.Add(poll.Duration)) {
		// Poll has ended, stop
		return nil
	}

	if poll.LastUpdatedAggregateTime != nil {
		// If it is vote that happened before our last aggregation, it has already been counted and can be ignored.
		if vote.UpdatedAt.Before(*poll.LastUpdatedAggregateTime) {
			logrus.WithFields(logrus.Fields{
				"pollID": snsVote.PollID,
				"voteID": snsVote.VoteID,
			}).Info("ignoring old vote")
			return nil
		}

		// We should only send updates every 2 seconds
		nextRunTime := poll.LastUpdatedAggregateTime.Add(time.Second * 2)
		if time.Now().Before(nextRunTime) {
			logrus.WithFields(logrus.Fields{
				"pollID": snsVote.PollID,
				"voteID": snsVote.VoteID,
			}).Info("we cannot process this vote yet")
			return poliwag_error.PollUpdateThrottled.New("we cannot process this vote yet")
		}
	}

	pollUpdateLock, err := w.lockingClient.ObtainLock(ctx, fmt.Sprintf("poll_update:%s", snsVote.PollID))
	if err == redislock.ErrNotObtained {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).Info("a conflicting poll update is currently in progress")
		return poliwag_error.PollConflictingUpdate.New("a conflicting poll update is currently in progress")
	} else if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error calling locking client")
		return err
	}

	defer func() {
		err := pollUpdateLock.Release()
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"pollID": snsVote.PollID,
				"voteID": snsVote.VoteID,
			}).WithError(err).Error("error unlocking poll update lock")
		}
	}()

	var pollTotalVoters int
	aggregation, err := w.votesDAO.GetPollAggregation(ctx, snsVote.PollID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error querying poll aggregations (poll update worker)")
		return err
	} else {
		pollTotalVoters = aggregation.TotalVoters
	}

	choiceAggregations, err := w.votesDAO.GetChoiceAggregations(ctx, snsVote.PollID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error getting choice aggregations")
		return err
	}

	// No votes have come in (which should be impossible?)
	if len(choiceAggregations) == 0 {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("got vote event without any choice aggregations")
		return errors.New("got vote event without any choice aggregations")
	}

	var pollTotalVotes int
	var pollTotalBaseVotes int
	var pollTotalBits int
	var pollTotalChannelPoints int
	var updateArgs []choices.UpdateChoiceArgs
	for _, a := range choiceAggregations {
		args := choices.UpdateChoiceArgs{
			ChoiceID:           a.ChoiceID,
			TotalVoters:        pointers.IntP(a.TotalVoters),
			TotalVotes:         pointers.IntP(a.TotalVotes),
			TotalBaseVotes:     pointers.IntP(a.TotalBaseVotes),
			TotalBits:          pointers.IntP(a.TotalBits),
			TotalChannelPoints: pointers.IntP(a.TotalChannelPoints),
		}

		updateArgs = append(updateArgs, args)
		pollTotalVotes += a.TotalVotes
		pollTotalBaseVotes += a.TotalBaseVotes
		pollTotalBits += a.TotalBits
		pollTotalChannelPoints += a.TotalChannelPoints
	}

	updatedPoll, err := w.pollsDAO.UpdatePoll(ctx, snsVote.PollID, polls.PollUpdateArgs{
		TotalVoters:        pointers.IntP(pollTotalVoters),
		TotalVotes:         pointers.IntP(pollTotalVotes),
		TotalBaseVotes:     pointers.IntP(pollTotalBaseVotes),
		TotalBits:          pointers.IntP(pollTotalBits),
		TotalChannelPoints: pointers.IntP(pollTotalChannelPoints),
	})
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error saving poll aggregations (poll update worker)")
		return err
	}

	err = w.choicesDAO.UpdateChoices(ctx, updateArgs)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error saving poll aggregations (poll update lambda)")
		return err
	}

	err = w.pubsubController.SendPollUpdatePubsub(ctx, snsVote.PollID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error send PollUpdate pubsub")
		return err
	}

	w.eventbusController.PublishPollUpdate(ctx, updatedPoll, false)

	_, err = w.pollsDAO.UpdatePoll(ctx, snsVote.PollID, polls.PollUpdateArgs{
		LastUpdatedAggregateTime: &runTime,
	})

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"pollID": snsVote.PollID,
			"voteID": snsVote.VoteID,
		}).WithError(err).Error("error updating poll LastUpdatedAggregateTime")
		return err
	}

	return nil
}
