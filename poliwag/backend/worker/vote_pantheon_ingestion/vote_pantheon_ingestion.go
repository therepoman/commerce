package vote_pantheon_ingestion

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/models"
	workeri "code.justin.tv/commerce/poliwag/backend/worker"
)

const (
	timeout = 5 * time.Second
)

type worker struct {
	pantheonClient pantheonrpc.Pantheon
	votesDAO       votes.DAO
}

func NewWorker(pantheonClient pantheonrpc.Pantheon, votesDAO votes.DAO) workeri.Worker {
	return &worker{
		pantheonClient: pantheonClient,
		votesDAO:       votesDAO,
	}
}

func (w *worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	if msg == nil || msg.Body == nil {
		logrus.Error("received nil sqs msg")
		return errors.New("received nil sqs msg")
	}

	snsMsg, err := sns.Extract(msg)
	if err != nil {
		logrus.WithError(err).Error("error extracting sns message from sqs message")
		return err
	}

	var snsVote models.Vote
	err = json.Unmarshal([]byte(snsMsg.Message), &snsVote)
	if err != nil {
		logrus.WithError(err).Error("error unmarshaling sns message into vote")
		return err
	}

	vote, err := w.votesDAO.GetVote(ctx, snsVote.PollID, snsVote.VoteID, true)
	if err != nil {
		logrus.WithField("snsVote", snsVote).WithError(err).Error("error getting vote from rds")
		return err
	}

	if vote == nil {
		logrus.WithField("snsVote", snsVote).Error("vote not found in rds")
		return errors.New("vote not found")
	}

	if vote.DeletedAt != nil {
		logrus.WithField("voteID", vote.VoteID).Info("vote was deleted")
		return nil
	}

	err = w.ingestForPollAndChoice(ctx, *vote, vote.TotalVotes, models.LeaderboardTypeVotes)
	if err != nil {
		logrus.WithField("voteID", vote.VoteID).WithError(err).Error("error ingesting votes into pantheon")
		return err
	}

	if vote.Bits > 0 {
		err = w.ingestForPollAndChoice(ctx, *vote, vote.Bits, models.LeaderboardTypeBits)
		if err != nil {
			logrus.WithField("voteID", vote.VoteID).WithError(err).Error("error ingesting bits into pantheon")
			return err
		}
	}

	if vote.ChannelPoints > 0 {
		err = w.ingestForPollAndChoice(ctx, *vote, vote.ChannelPoints, models.LeaderboardTypeChannelPoints)
		if err != nil {
			logrus.WithField("voteID", vote.VoteID).WithError(err).Error("error ingesting channel points into pantheon")
			return err
		}
	}

	return nil
}

func (w *worker) ingestForPollAndChoice(ctx context.Context, vote votes.Vote, amount int64, lbType models.LeaderboardType) error {
	_, err := w.pantheonClient.PublishEvent(ctx, &pantheonrpc.PublishEventReq{
		Domain:      models.PollsDomain,
		EventId:     vote.VoteID,
		TimeOfEvent: uint64(vote.CreatedAt.UnixNano()),
		GroupingKey: models.PollGroupingKey(vote.PollID, lbType),
		EntryKey:    vote.UserID,
		EventValue:  amount,
	})
	if err != nil {
		return err
	}

	_, err = w.pantheonClient.PublishEvent(ctx, &pantheonrpc.PublishEventReq{
		Domain:      models.PollsDomain,
		EventId:     vote.VoteID,
		TimeOfEvent: uint64(vote.CreatedAt.UnixNano()),
		GroupingKey: models.ChoiceGroupingKey(vote.PollID, vote.ChoiceID, lbType),
		EntryKey:    vote.UserID,
		EventValue:  amount,
	})
	if err != nil {
		return err
	}

	return nil
}
