package vote_pantheon_ingestion_test

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/poliwag/backend/dao/votes"
	"code.justin.tv/commerce/poliwag/backend/models"
	"code.justin.tv/commerce/poliwag/backend/worker/vote_pantheon_ingestion"
	pantheonrpc_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	votes_dao_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/commerce/poliwag/backend/dao/votes"
)

func TestWorker_Handle(t *testing.T) {
	Convey("Given a worker", t, func() {
		pantheonClient := new(pantheonrpc_mock.Pantheon)
		votesDAO := new(votes_dao_mock.DAO)
		w := vote_pantheon_ingestion.NewWorker(pantheonClient, votesDAO)

		Convey("Errors when sqs msg is nil", func() {
			So(w.Handle(nil), ShouldNotBeNil)
		})

		Convey("Errors when the sns msg body is nil", func() {
			So(w.Handle(&sqs.Message{Body: nil}), ShouldNotBeNil)
		})

		Convey("Errors when the sns msg body cannot be parsed", func() {
			So(w.Handle(&sqs.Message{Body: pointers.StringP("this ain't valid json")}), ShouldNotBeNil)
		})

		Convey("Errors when the sns msg body's message field cannot be parse", func() {
			So(w.Handle(&sqs.Message{Body: pointers.StringP("{\"Message\": \"this ain't valid json\" }")}), ShouldNotBeNil)
		})

		Convey("When the input is valid", func() {
			vote := models.Vote{
				VoteID:   "test-vote-id",
				PollID:   "test-poll-id",
				ChoiceID: "test-choice-id",
			}
			voteJSON, err := json.Marshal(&vote)
			So(err, ShouldBeNil)

			snsMsg := sns.Message{
				Message: string(voteJSON),
			}
			snsJSON, err := json.Marshal(&snsMsg)
			So(err, ShouldBeNil)

			msg := &sqs.Message{Body: pointers.StringP(string(snsJSON))}

			Convey("Errors when vote dao errors", func() {
				votesDAO.On("GetVote", mock.Anything, vote.PollID, vote.VoteID, true).Return(nil, errors.New("test-error"))
				So(w.Handle(msg), ShouldNotBeNil)
			})

			Convey("Errors when vote dao returns nil", func() {
				votesDAO.On("GetVote", mock.Anything, vote.PollID, vote.VoteID, true).Return(nil, nil)
				So(w.Handle(msg), ShouldNotBeNil)
			})

			Convey("Returns early when vote dao returns a deleted vote", func() {
				votesDAO.On("GetVote", mock.Anything, vote.PollID, vote.VoteID, true).Return(&votes.Vote{
					VoteID:     vote.VoteID,
					PollID:     vote.PollID,
					ChoiceID:   vote.ChoiceID,
					TotalVotes: 1,
					Bits:       0,
					DeletedAt:  pointers.TimeP(time.Now()),
				}, nil)
				So(w.Handle(msg), ShouldBeNil)
				So(pantheonClient.Calls, ShouldHaveLength, 0)
			})

			Convey("When vote dao returns a vote without bits", func() {
				votesDAO.On("GetVote", mock.Anything, vote.PollID, vote.VoteID, true).Return(&votes.Vote{
					VoteID:     vote.VoteID,
					PollID:     vote.PollID,
					ChoiceID:   vote.ChoiceID,
					TotalVotes: 1,
					Bits:       0,
				}, nil)

				Convey("Errors when pantheon publish errors", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
					So(w.Handle(msg), ShouldNotBeNil)
				})

				Convey("Errors when pantheon publish succeeds once, but errors the second time", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, nil).Once()
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test-error")).Once()
					So(w.Handle(msg), ShouldNotBeNil)
				})

				Convey("Succeeds when pantheon publish succeeds both times", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, nil).Times(2)
					So(w.Handle(msg), ShouldBeNil)
					So(pantheonClient.Calls, ShouldHaveLength, 2)
				})
			})

			Convey("When vote dao returns a vote with bits", func() {
				votesDAO.On("GetVote", mock.Anything, vote.PollID, vote.VoteID, true).Return(&votes.Vote{
					VoteID:     vote.VoteID,
					PollID:     vote.PollID,
					ChoiceID:   vote.ChoiceID,
					TotalVotes: 1,
					Bits:       1,
				}, nil)

				Convey("Errors when pantheon publish errors", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
					So(w.Handle(msg), ShouldNotBeNil)
				})

				Convey("Errors when pantheon succeeds once before erroring", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, nil).Once()
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
					So(w.Handle(msg), ShouldNotBeNil)
				})

				Convey("Errors when pantheon succeeds twice before erroring", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, nil).Times(2)
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
					So(w.Handle(msg), ShouldNotBeNil)
				})

				Convey("Errors when pantheon succeeds thrice before erroring", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, nil).Times(3)
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
					So(w.Handle(msg), ShouldNotBeNil)
				})

				Convey("Succeeds when pantheon publish succeeds all four times", func() {
					pantheonClient.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, nil).Times(4)
					So(w.Handle(msg), ShouldBeNil)
					So(pantheonClient.Calls, ShouldHaveLength, 4)
				})
			})
		})
	})
}
