package worker

import (
	"context"
	"sync"

	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/workerqueue"
	log "code.justin.tv/commerce/logrus"
)

type sqsWorker struct {
	name        string
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
	worker      Worker
}

func NewSQSWorkerManager(queueName string, numWorkers int, workerVersion int, client sqsiface.SQSAPI, worker Worker, stats statsd.Statter, sampleReporter *telemetry.SampleReporter) NamedTerminator {
	manager := &sqsWorker{
		name:        queueName,
		stopChannel: make(chan struct{}),
		worker:      worker,
	}

	params := workerqueue.CreateWorkersParams{
		NumWorkers: numWorkers,
		QueueName:  queueName,
		Client:     client,
		Tasks: map[workerqueue.TaskVersion]workerqueue.TaskFn{
			workerqueue.FallbackTaskVersion:        manager.worker.Handle,
			workerqueue.TaskVersion(workerVersion): manager.worker.Handle,
		},
		Reporter: sampleReporter,
	}

	var err error
	// TODO: swap to this constructor when removing statsd.Statter
	// manager.waitGroup, _, err = workerqueue.CreateWorkersSansStats(params, manager.stopChannel)
	manager.waitGroup, _, err = workerqueue.CreateWorkers(params, manager.stopChannel, stats)
	if err != nil {
		log.Fatalf("Failed to create workers: %v", err)
	}

	return manager
}

func (mgr *sqsWorker) Name() string {
	return mgr.name
}

func (mgr *sqsWorker) Shutdown(ctx context.Context) error {
	close(mgr.stopChannel)
	if wait(ctx, mgr.waitGroup) {
		return errors.New("Timed out while waiting for SQS workers to complete")
	}
	return nil
}

func wait(ctx context.Context, wg *sync.WaitGroup) bool {
	workersDone := make(chan struct{})
	go func() {
		defer close(workersDone)
		wg.Wait()
	}()

	select {
	case <-workersDone:
		return false // done, no timeout
	case <-ctx.Done():
		return true // done, timed out
	}
}
