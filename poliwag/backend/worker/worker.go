package worker

import (
	"context"

	"github.com/aws/aws-sdk-go/service/sqs"
)

type NamedTerminator interface {
	Terminator
	Name() string
}

type Terminator interface {
	Shutdown(ctx context.Context) error
}

type Worker interface {
	Handle(msg *sqs.Message) error
}
