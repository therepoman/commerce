package viewable_poll

import (
	"context"
	"fmt"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v7"
	"github.com/sirupsen/logrus"

	"code.justin.tv/chat/rediczar"
	polls_dao "code.justin.tv/commerce/poliwag/backend/dao/polls"
	"code.justin.tv/commerce/poliwag/backend/errors"
	"code.justin.tv/commerce/poliwag/backend/models"
	hystrix_utils "code.justin.tv/commerce/poliwag/backend/utils/hystrix"
)

const (
	cacheKeyFormat          = "viewable_poll_cache:%s"
	CacheTTL                = 10 * time.Second
	ViewablePollNotPresent  = "VIEWABLE_POLL_NOT_PRESENT"
	ViewablePollJustCreated = "VIEWABLE_POLL_JUST_CREATED"
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 . ViewablePollCache

// The ViewablePollCache is a negative cache - it caches when a viewable poll _is not present_
// This is because 99.9% of requests return no viewable poll
type ViewablePollCache interface {
	Get(ctx context.Context, ownedBy string) (*models.Poll, error)
	Del(ctx context.Context, ownedBy string) error
	Set(ctx context.Context, ownedBy, value string) error
}

type cache struct {
	pollsDAO polls_dao.PollsDAO
	rediczar rediczar.ThickClient
}

func NewViewablePollCache(pollsDAO polls_dao.PollsDAO, rediczar rediczar.ThickClient) ViewablePollCache {
	return &cache{
		pollsDAO: pollsDAO,
		rediczar: rediczar,
	}
}

func NewCache(p polls_dao.PollsDAO, r rediczar.ThickClient) ViewablePollCache {
	return &cache{
		pollsDAO: p,
		rediczar: r,
	}
}

func CacheKey(ownedBy string) string {
	return fmt.Sprintf(cacheKeyFormat, ownedBy)
}

func (c cache) Get(ctx context.Context, ownedBy string) (*models.Poll, error) {
	var redisValue string
	err := hystrix.Do(hystrix_utils.ViewablePollCacheRedisGet, func() error {
		v, redisErr := c.rediczar.Get(ctx, CacheKey(ownedBy))
		if redisErr != nil {
			// redis.Nil is an expected error - don't consider it an error for the hystrix circuit.
			// This means there may be a viewable poll so we should check dynamo.
			if redisErr == redis.Nil {
				return nil
			}
			return redisErr
		}
		redisValue = v
		return nil
	}, nil)

	// if viewable poll is not present we don't need to check dynamo.
	if redisValue == ViewablePollNotPresent {
		return nil, nil
	}

	// If Redis returned an error other than redis.Nil we still want to fallback to dynamo and log a warn.
	if err != nil {
		logrus.WithField("key", CacheKey(ownedBy)).WithError(err).Warn("error getting viewable poll from redis, continuing to DynamoDB")
	}

	viewablePolls, _, err := c.pollsDAO.GetPollsByOwnedBy(ctx, ownedBy, models.PubliclyViewablePollStatuses, models.PollSortStartTime, models.DirectionDescending, 1, nil)
	if err != nil {
		msg := fmt.Sprintf("error getting polls for ownedBy %s", ownedBy)
		logrus.WithError(err).WithField("ownedBy", ownedBy).Error(msg)
		return nil, errors.InternalError.Wrap(err, msg)
	}

	// if viewable poll has just been created we want to always check dynamo (bypass updating the cache key).
	if redisValue == ViewablePollJustCreated {
		if len(viewablePolls) == 0 {
			return nil, nil
		}
		return &viewablePolls[0], nil
	}

	if len(viewablePolls) == 0 {
		// Save that there is no viewable poll here
		// If there's any error, we simply don't cache.
		// No big deal, but still log a warn
		if err := c.Set(ctx, ownedBy, ViewablePollNotPresent); err != nil {
			logrus.WithFields(logrus.Fields{
				"ownedBy": ownedBy,
				"key":     CacheKey(ownedBy),
			}).WithError(err).Warn("error setting value in viewable poll redis")
		}
		return nil, nil
	}

	if err := c.Del(ctx, ownedBy); err != nil {
		// Logging an error here as this is more serious
		// Not deleting from the cache means the poll will end up unviewable even though one exists
		logrus.WithFields(logrus.Fields{
			"ownedBy": ownedBy,
			"key":     CacheKey(ownedBy),
		}).WithError(err).Error("error deleting key from redis")
	}

	return &viewablePolls[0], nil
}

func (c cache) Del(ctx context.Context, ownedBy string) error {
	_, err := c.rediczar.Del(ctx, CacheKey(ownedBy))
	return err
}

func (c cache) Set(ctx context.Context, ownedBy, value string) error {
	return hystrix.Do(hystrix_utils.ViewablePollCacheRedisSet, func() error {
		_, err := c.rediczar.Set(ctx, CacheKey(ownedBy), value, CacheTTL)
		return err
	}, nil)
}
