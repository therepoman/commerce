package viewable_poll_test

import (
	"context"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v7"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"code.justin.tv/chat/rediczar"
	"code.justin.tv/commerce/poliwag/backend/cache/viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/dao/polls/pollsfakes"
	"code.justin.tv/commerce/poliwag/backend/models"
)

type testFakes struct {
	pollsDAO *pollsfakes.FakePollsDAO
}

type testContext struct {
	fakes     *testFakes
	miniRedis *miniredis.Miniredis

	// client is the system under test
	client viewable_poll.ViewablePollCache
}

type testConfig struct{}

type teardown func(*testing.T)

func setup(t *testing.T, _ testConfig) (*testContext, teardown) {
	miniRedis, rediczarThickClient, miniRedisTeardown := createMiniRedisThickClient(t)

	fakes := &testFakes{
		pollsDAO: new(pollsfakes.FakePollsDAO),
	}

	tc := &testContext{
		fakes:     fakes,
		miniRedis: miniRedis,
		client:    viewable_poll.NewCache(fakes.pollsDAO, rediczarThickClient),
	}
	return tc, func(t *testing.T) {
		miniRedisTeardown(t)
	}
}

func createMiniRedisThickClient(t *testing.T) (*miniredis.Miniredis, rediczar.ThickClient, teardown) {
	mr, err := miniredis.Run()
	require.NoError(t, err)

	client := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			Redis: redis.NewClient(&redis.Options{
				Addr: mr.Addr(),
			}),
		},
	}
	return mr, client, func(*testing.T) {
		mr.Close()
	}
}

func Test_ViewablePoll_Get(t *testing.T) {
	var (
		ownedBy  = "198273"
		cacheKey = viewable_poll.CacheKey(ownedBy)
		ctx      = context.Background()
	)

	t.Run("when poll is not present", func(t *testing.T) {
		tc, teardown := setup(t, testConfig{})
		defer teardown(t)

		require.NoError(t, tc.miniRedis.Set(cacheKey, viewable_poll.ViewablePollNotPresent))

		poll, err := tc.client.Get(ctx, ownedBy)
		require.NoError(t, err)
		assert.Nil(t, poll)

		// ensure dynamo was not queried
		assert.Zero(t, tc.fakes.pollsDAO.GetPollsByOwnedByCallCount())
	})

	t.Run("when poll was just created", func(t *testing.T) {
		t.Run("and there is NO viewable poll", func(t *testing.T) {
			tc, teardown := setup(t, testConfig{})
			defer teardown(t)

			require.NoError(t, tc.miniRedis.Set(cacheKey, viewable_poll.ViewablePollJustCreated))

			poll, err := tc.client.Get(ctx, ownedBy)
			require.NoError(t, err)
			assert.Nil(t, poll)

			// assert cache value is the same (was not updated)
			tc.miniRedis.CheckGet(t, cacheKey, viewable_poll.ViewablePollJustCreated)
		})

		t.Run("and there is a viewable poll", func(t *testing.T) {
			stubPoll := models.Poll{
				PollID:  "fake-poll-id-123",
				OwnedBy: ownedBy,
			}

			tc, teardown := setup(t, testConfig{})
			defer teardown(t)

			require.NoError(t, tc.miniRedis.Set(cacheKey, viewable_poll.ViewablePollJustCreated))
			tc.fakes.pollsDAO.GetPollsByOwnedByReturns([]models.Poll{
				stubPoll,
			}, nil, nil)

			poll, err := tc.client.Get(ctx, ownedBy)
			require.NoError(t, err)
			if assert.NotNil(t, poll) {
				assert.Equal(t, stubPoll, *poll)
			}

			// assert cache value is the same (was not updated)
			tc.miniRedis.CheckGet(t, cacheKey, viewable_poll.ViewablePollJustCreated)
		})

		t.Run("when poll cache has no value", func(t *testing.T) {
			t.Run("and there is NO viewable poll", func(t *testing.T) {
				tc, teardown := setup(t, testConfig{})
				defer teardown(t)

				// nothing in cache
				// nothing in dynamo

				poll, err := tc.client.Get(ctx, ownedBy)
				require.NoError(t, err)
				assert.Nil(t, poll)

				// ensure the negative cache was set indicated no polls present on channel
				tc.miniRedis.CheckGet(t, cacheKey, viewable_poll.ViewablePollNotPresent)
			})

			t.Run("and there is a viewable poll", func(t *testing.T) {
				stubPoll := models.Poll{
					PollID:  "fake-poll-id-234",
					OwnedBy: ownedBy,
				}

				tc, teardown := setup(t, testConfig{})
				defer teardown(t)

				tc.fakes.pollsDAO.GetPollsByOwnedByReturns([]models.Poll{
					stubPoll,
				}, nil, nil)

				poll, err := tc.client.Get(ctx, ownedBy)
				require.NoError(t, err)
				if assert.NotNil(t, poll) {
					assert.Equal(t, stubPoll, *poll)
				}

				// assert cache value was deleted
				assert.False(t, tc.miniRedis.Exists(cacheKey))
			})
		})
	})
}
