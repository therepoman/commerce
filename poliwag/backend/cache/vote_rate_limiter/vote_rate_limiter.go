package vote_rate_limiter

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/chat/rediczar"
	"code.justin.tv/commerce/poliwag/backend/errors"
)

const (
	cacheFormat                      = "vote_rate_limit:%s:%s"
	VotePerUserRateLimitWindow       = 5 * time.Second
	VotePerUserRateLimit       int64 = 20
	VotePerPollRateLimitWindow       = 1 * time.Second
	VotePerPollLowRateLimit    int64 = 1000
	VotePerPollHighRateLimit   int64 = 5000
)

// The RateLimiter uses Rediczar to rate limit votes per second for users and for polls
type VoteRateLimiter interface {
	CanVote(ctx context.Context, userID, pollID string) (bool, error)
}

type cache struct {
	rediczar       rediczar.ThickClient
	sampleReporter *telemetry.SampleReporter
}

func NewVoteRateLimiter(rediczar rediczar.ThickClient, sampleReporter *telemetry.SampleReporter) VoteRateLimiter {
	return &cache{
		rediczar:       rediczar,
		sampleReporter: sampleReporter,
	}
}

func GetUserKey(userID string) string {
	return fmt.Sprintf(cacheFormat, "user", userID)
}

func GetPollKey(pollID string) string {
	return fmt.Sprintf(cacheFormat, "poll", pollID)
}

func (c cache) CanVote(ctx context.Context, userID, pollID string) (bool, error) {
	userCount, err := c.rediczar.RateIncr(ctx, GetUserKey(userID), VotePerUserRateLimitWindow)
	if err != nil {
		msg := fmt.Sprintf("error getting per-user rate limit for user %s", userID)
		logrus.WithField("userID", userID).WithError(err).Error(msg)
		return false, errors.InternalError.Wrap(err, msg)
	}
	if userCount > VotePerUserRateLimit {
		c.sampleReporter.Report("VoteRateLimited_User", 1.0, telemetry.UnitCount)
		// Rate limited
		return false, nil
	}

	pollCount, err := c.rediczar.RateIncr(ctx, GetPollKey(pollID), VotePerPollRateLimitWindow)
	if err != nil {
		msg := fmt.Sprintf("error getting per-poll rate limit for poll %s", pollID)
		logrus.WithField("pollID", pollID).WithError(err).Error(msg)
		return false, errors.InternalError.Wrap(err, msg)
	}

	// track against the low rate limit to see how much rate limiting we would have if we lowered the limit to this
	if pollCount > VotePerPollLowRateLimit {
		c.sampleReporter.Report("VoteRateLimited_PollLow", 1.0, telemetry.UnitCount)
	}

	// Allow users voting for the first time in this window a higher threshold for their vote going through
	if (userCount <= 1 && pollCount > VotePerPollHighRateLimit) ||
		(userCount > 1 && pollCount > VotePerPollLowRateLimit) {
		c.sampleReporter.Report("VoteRateLimited_Poll", 1.0, telemetry.UnitCount)
		// Rate limited
		return false, nil
	}

	return true, nil
}
