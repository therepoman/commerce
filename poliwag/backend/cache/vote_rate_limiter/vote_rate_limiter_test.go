package vote_rate_limiter_test

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/cache/vote_rate_limiter"
	rediczar_mock "code.justin.tv/commerce/poliwag/mocks/code.justin.tv/chat/rediczar"
)

func TestVoteRateLimiter_CanVote(t *testing.T) {
	Convey("given a vote rate limiter", t, func() {
		rediczarClient := new(rediczar_mock.ThickClient)

		cache := vote_rate_limiter.NewVoteRateLimiter(rediczarClient, telemetry.NewNoOpSampleReporter())

		userID := "123456"
		pollID := "987654"
		userKey := vote_rate_limiter.GetUserKey(userID)
		pollKey := vote_rate_limiter.GetPollKey(pollID)

		Convey("when redis returns a value under the rate limit for both user and poll", func() {
			rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(1), nil)
			rediczarClient.On("RateIncr", mock.Anything, pollKey, mock.Anything).Return(int64(2), nil)

			Convey("we should return true and no error", func() {
				canVote, err := cache.CanVote(context.Background(), userID, pollID)

				So(canVote, ShouldBeTrue)
				So(err, ShouldBeNil)
			})
		})

		Convey("when redis returns a value over the rate limit for user rate", func() {
			rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(vote_rate_limiter.VotePerUserRateLimit+1, nil)
			rediczarClient.On("RateIncr", mock.Anything, pollKey, mock.Anything).Return(int64(2), nil)

			Convey("we should return false and no error", func() {
				canVote, err := cache.CanVote(context.Background(), userID, pollID)

				So(canVote, ShouldBeFalse)
				So(err, ShouldBeNil)
			})
		})

		Convey("when redis returns a value over the low rate limit for poll rate", func() {
			rediczarClient.On("RateIncr", mock.Anything, pollKey, mock.Anything).Return(vote_rate_limiter.VotePerPollLowRateLimit+1, nil)

			Convey("when the user is on their first vote in this bucket", func() {
				rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(1), nil)

				Convey("we should return true and no error", func() {
					canVote, err := cache.CanVote(context.Background(), userID, pollID)

					So(canVote, ShouldBeTrue)
					So(err, ShouldBeNil)
				})
			})

			Convey("when the user is not on their first vote in this bucket", func() {
				rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(2), nil)

				Convey("we should return false and no error", func() {
					canVote, err := cache.CanVote(context.Background(), userID, pollID)

					So(canVote, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("when redis returns a value over the HIGH rate limit for poll rate", func() {
			rediczarClient.On("RateIncr", mock.Anything, pollKey, mock.Anything).Return(vote_rate_limiter.VotePerPollHighRateLimit+1, nil)

			Convey("when the user is on their first vote in this bucket", func() {
				rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(1), nil)

				Convey("we should return false and no error", func() {
					canVote, err := cache.CanVote(context.Background(), userID, pollID)

					So(canVote, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("when the user is not on their first vote in this bucket", func() {
				rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(2), nil)

				Convey("we should return false and no error", func() {
					canVote, err := cache.CanVote(context.Background(), userID, pollID)

					So(canVote, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("when redis returns an error for user rate", func() {
			rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(0), errors.New("ERROR"))
			rediczarClient.On("RateIncr", mock.Anything, pollKey, mock.Anything).Return(int64(2), nil)

			Convey("we should return false and an error", func() {
				canVote, err := cache.CanVote(context.Background(), userID, pollID)

				So(canVote, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when redis returns an error for poll rate", func() {
			rediczarClient.On("RateIncr", mock.Anything, userKey, mock.Anything).Return(int64(1), nil)
			rediczarClient.On("RateIncr", mock.Anything, pollKey, mock.Anything).Return(int64(0), errors.New("ERROR"))

			Convey("we should return false and an error", func() {
				canVote, err := cache.CanVote(context.Background(), userID, pollID)

				So(canVote, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
