package backend

import (
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-playground/validator/v10"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/controller/eventbus"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/archive_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/create_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_polls"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/moderate_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/terminate_poll"
	"code.justin.tv/commerce/poliwag/backend/controller/poll/vote"
	"code.justin.tv/commerce/poliwag/backend/controller/pubsub"
	"code.justin.tv/commerce/poliwag/backend/controller/slack"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voter"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters"
	"code.justin.tv/commerce/poliwag/backend/controller/voter/get_voters_by_choice"
	"code.justin.tv/commerce/poliwag/config"
)

type controllers struct {
	archivePollController       archive_poll.Controller         `validate:"required"`
	createPollController        create_poll.Controller          `validate:"required"`
	moderatePollController      moderate_poll.Controller        `validate:"required"`
	getPollController           get_poll.Controller             `validate:"required"`
	getPollsController          get_polls.Controller            `validate:"required"`
	getViewablePollController   get_viewable_poll.Controller    `validate:"required"`
	terminatePollController     terminate_poll.Controller       `validate:"required"`
	getVoterController          get_voter.Controller            `validate:"required"`
	getVotersController         get_voters.Controller           `validate:"required"`
	getVotersByChoiceController get_voters_by_choice.Controller `validate:"required"`
	voteController              vote.Controller                 `validate:"required"`
	pubsubController            pubsub.Controller               `validate:"required"`
	eventbusController          eventbus.Controller             `validate:"required"`
	slackController             slack.Controller                `validate:"required"`
}

func createControllers(cfg *config.Config, isLambda bool, stats statsd.Statter, sampleReporter *telemetry.SampleReporter, c *clients, d *daos, cache *caches, ex *stepFnExecutors) (*controllers, error) {
	getPollController := get_poll.NewController(d.choices, d.polls, stats, sampleReporter)
	eventbusController := eventbus.NewController(isLambda, c.eventbus, d.choices)
	pubsubController := pubsub.NewController(isLambda, c.pubsub, c.users, d.polls, getPollController, stats)
	slackController := slack.NewController(d.polls, getPollController, c.users, c.slack)
	archivePollController := archive_poll.NewController(d.polls, getPollController, c.datascience, cache.viewablePollCache, ex.PollArchiveExecutor, slackController)
	createPollController := create_poll.NewController(stats, sampleReporter, c.copo, c.datascience, c.paydayLegacy, c.ripley, d.polls, d.choices, cache.viewablePollCache, eventbusController, pubsubController, slackController, ex.PollUpdateExecutor, ex.PollCompleteExecutor)
	moderatePollController := moderate_poll.NewController(d.polls, getPollController, cache.viewablePollCache, ex.PollModerateExecutor)
	getPollsController := get_polls.NewController(d.choices, d.polls, stats, sampleReporter)
	getViewablePollController := get_viewable_poll.NewController(d.choices, d.polls, cache.viewablePollCache, stats, sampleReporter)
	terminatePollController := terminate_poll.NewController(d.polls, getPollController, c.datascience, ex.PollTerminateExecutor, slackController)
	getVoterController := get_voter.NewController(getPollController, d.polls, d.voters)
	getVotersController := get_voters.NewController(d.polls, d.voters)
	getVotersByChoiceController := get_voters_by_choice.NewController(d.polls, d.choices, d.voters, stats, sampleReporter)
	voteController := vote.NewController(getPollController, getVoterController, d.votes, d.voters, c.locking, c.payday, c.copo, c.datascience, c.subs, stats, sampleReporter, c.sns, cfg)
	out := &controllers{
		archivePollController:       archivePollController,
		createPollController:        createPollController,
		moderatePollController:      moderatePollController,
		getPollController:           getPollController,
		getPollsController:          getPollsController,
		getViewablePollController:   getViewablePollController,
		terminatePollController:     terminatePollController,
		getVoterController:          getVoterController,
		getVotersController:         getVotersController,
		getVotersByChoiceController: getVotersByChoiceController,
		voteController:              voteController,
		pubsubController:            pubsubController,
		eventbusController:          eventbusController,
		slackController:             slackController,
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
