package backend

import (
	"github.com/go-playground/validator/v10"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/poliwag/backend/api/admin_get_poll"
	"code.justin.tv/commerce/poliwag/backend/api/admin_get_polls"
	"code.justin.tv/commerce/poliwag/backend/api/archive_poll"
	"code.justin.tv/commerce/poliwag/backend/api/create_poll"
	"code.justin.tv/commerce/poliwag/backend/api/get_poll"
	"code.justin.tv/commerce/poliwag/backend/api/get_polls"
	"code.justin.tv/commerce/poliwag/backend/api/get_viewable_poll"
	"code.justin.tv/commerce/poliwag/backend/api/get_voter"
	"code.justin.tv/commerce/poliwag/backend/api/get_voters"
	"code.justin.tv/commerce/poliwag/backend/api/get_voters_by_choice"
	"code.justin.tv/commerce/poliwag/backend/api/moderate_poll"
	"code.justin.tv/commerce/poliwag/backend/api/terminate_poll"
	"code.justin.tv/commerce/poliwag/backend/api/vote"
	"code.justin.tv/commerce/poliwag/backend/cache/vote_rate_limiter"
	"code.justin.tv/commerce/poliwag/clients/cloudwatchlogger"
)

type api struct {
	archivePollAPI       archive_poll.API         `validate:"required"`
	createPollAPI        create_poll.API          `validate:"required"`
	getPollAPI           get_poll.API             `validate:"required"`
	getPollsAPI          get_polls.API            `validate:"required"`
	getViewablePollAPI   get_viewable_poll.API    `validate:"required"`
	terminatePollAPI     terminate_poll.API       `validate:"required"`
	getVoterAPI          get_voter.API            `validate:"required"`
	getVotersAPI         get_voters.API           `validate:"required"`
	getVotersByChoiceAPI get_voters_by_choice.API `validate:"required"`
	voteAPI              vote.API                 `validate:"required"`

	adminGetPollAPI  admin_get_poll.API  `validate:"required"`
	adminGetPollsAPI admin_get_polls.API `validate:"required"`
	moderatePollAPI  moderate_poll.API   `validate:"required"`
}

func createAPIs(sampleReporter *telemetry.SampleReporter, auditLogger cloudwatchlogger.CloudWatchLogger, c *clients, auths *authorizers, mods *moderators, ctrls *controllers) (*api, error) {
	out := &api{
		archivePollAPI:       archive_poll.NewAPI(auths.pollEditorAuthorizer, ctrls.archivePollController),
		createPollAPI:        create_poll.NewAPI(auths.channelEditorAuthorizer, mods.pollInputModerator, ctrls.createPollController),
		getPollAPI:           get_poll.NewAPI(auths.pollViewerAuthorizer, ctrls.getPollController),
		getPollsAPI:          get_polls.NewAPI(auths.channelEditorAuthorizer, ctrls.getPollsController),
		getViewablePollAPI:   get_viewable_poll.NewAPI(ctrls.getViewablePollController),
		terminatePollAPI:     terminate_poll.NewAPI(auths.pollEditorAuthorizer, ctrls.terminatePollController),
		getVoterAPI:          get_voter.NewAPI(auths.voterViewerAuthorizer, ctrls.getVoterController),
		getVotersAPI:         get_voters.NewAPI(auths.pollEditorAuthorizer, ctrls.getVotersController),
		getVotersByChoiceAPI: get_voters_by_choice.NewAPI(auths.pollEditorAuthorizer, ctrls.getVotersByChoiceController),
		voteAPI:              vote.NewAPI(auths.pollVoterAuthorizer, ctrls.voteController, vote_rate_limiter.NewVoteRateLimiter(c.rediczar, sampleReporter), sampleReporter, auditLogger),
		adminGetPollAPI:      admin_get_poll.NewAPI(ctrls.getPollController),
		adminGetPollsAPI:     admin_get_polls.NewAPI(ctrls.getPollsController),
		moderatePollAPI:      moderate_poll.NewAPI(ctrls.moderatePollController),
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
