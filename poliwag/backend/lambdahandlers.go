package backend

import (
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-playground/validator/v10"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	get_poll_lambda_handler "code.justin.tv/commerce/poliwag/backend/lambda_handlers/get_poll"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_archive"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_complete"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_moderate"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_terminate"
	"code.justin.tv/commerce/poliwag/backend/lambda_handlers/poll_update"
)

type LambdaHandlers struct {
	PollUpdate    *poll_update.LambdaHandler             `validate:"required"`
	PollComplete  *poll_complete.LambdaHandler           `validate:"required"`
	PollTerminate *poll_terminate.LambdaHandler          `validate:"required"`
	PollArchive   *poll_archive.LambdaHandler            `validate:"required"`
	PollModerate  *poll_moderate.LambdaHandler           `validate:"required"`
	GetPoll       *get_poll_lambda_handler.LambdaHandler `validate:"required"`
}

func createLambdaHandlers(stats statsd.Statter, sampleReporter *telemetry.SampleReporter, d *daos, ctrls *controllers, steps *lambdaSteps) (*LambdaHandlers, error) {
	out := &LambdaHandlers{
		PollUpdate: &poll_update.LambdaHandler{
			GetPoll:                   steps.getPollStep,
			QueryChoiceAggregations:   steps.queryChoiceAggregationsStep,
			QueryPollAggregation:      steps.queryPollAaggregationStep,
			UpdateIsAggregationsEqual: steps.updateIsAggregationsEqualStep,
			SaveChoiceAggregations:    steps.saveChoiceAggregationsStep,
			SavePollAggregation:       steps.savePollAggregationStep,
			SendPollUpdatePubsub:      steps.sendPollUpdatePubsubStep,
			PublishPollUpdateEventbus: steps.publishPollUpdateEventbusStep,
		},
		PollComplete: &poll_complete.LambdaHandler{
			PollsDAO:           d.polls,
			PubsubController:   ctrls.pubsubController,
			EventbusController: ctrls.eventbusController,
			Statsd:             stats,
			SampleReporter:     sampleReporter,
			SlackController:    ctrls.slackController,
		},
		PollTerminate: &poll_terminate.LambdaHandler{
			PollsDAO:           d.polls,
			PubsubController:   ctrls.pubsubController,
			EventbusController: ctrls.eventbusController,
		},
		PollArchive: &poll_archive.LambdaHandler{
			PollsDAO:           d.polls,
			PubsubController:   ctrls.pubsubController,
			EventbusController: ctrls.eventbusController,
		},
		PollModerate: &poll_moderate.LambdaHandler{
			PollsDAO:           d.polls,
			PubsubController:   ctrls.pubsubController,
			EventbusController: ctrls.eventbusController,
		},
		GetPoll: &get_poll_lambda_handler.LambdaHandler{
			PollsDAO: d.polls,
		},
	}
	if err := validator.New().Struct(out); err != nil {
		return nil, err
	}

	return out, nil
}
