module code.justin.tv/commerce/poliwag

go 1.16

require (
	code.justin.tv/amzn/PDMSLambdaTwirp v0.0.0-20200727190725-5cb32af1bce8
	code.justin.tv/amzn/TwirpGoLangAWSTransports v0.0.0-20200311200501-30801aea0958
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchS2S2 v1.0.5-0.20210409163623-c8713d5b96d4
	code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee v1.1.0 // indirect
	code.justin.tv/amzn/TwitchS2SJWTAlgorithms v0.0.0-20191216181634-802bd37e3a44 // indirect
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20201123200649-4788eaf4d80c
	code.justin.tv/amzn/TwitchTelemetryMWSMetricsSender v0.0.0-20190731182749-aa2dfbb7fe29 // indirect
	code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware v0.0.0-20200617234727-0c15f821c15d
	code.justin.tv/amzn/TwitchTelemetryPollingCollector v0.0.0-20210106192350-cb189de3c811
	code.justin.tv/cb/hallpass v0.0.0-20181001152558-55428d35afd8
	code.justin.tv/chat/awshttptelemetry v1.0.0
	code.justin.tv/chat/badges v1.7.0 // indirect
	code.justin.tv/chat/copo v0.12.4
	code.justin.tv/chat/httptelemetry v1.0.1
	code.justin.tv/chat/machineid v1.0.0
	code.justin.tv/chat/pubsub-go-pubclient v1.0.0
	code.justin.tv/chat/rediczar v1.15.0
	code.justin.tv/chat/telemetryext v1.1.0
	code.justin.tv/chat/tmi v1.10.1
	code.justin.tv/chat/twitchclienthttptelemetry v1.0.0
	code.justin.tv/chat/workerqueue v1.2.0
	code.justin.tv/chat/zuma v1.3.9
	code.justin.tv/commerce/gogogadget v0.0.0-20210315202618-a162a9c9b939
	code.justin.tv/commerce/logrus v1.1.1
	code.justin.tv/commerce/pantheon v0.3.1-0.20201204192453-9414bf2003ee
	code.justin.tv/commerce/payday v0.0.0-20201216194630-db6b1c29f460
	code.justin.tv/commerce/splatter v1.5.6
	code.justin.tv/common/go_test_dynamo v0.0.6
	code.justin.tv/common/hystrixtelemetry v0.0.0-20200530000126-982e984949cc
	code.justin.tv/eventbus/client v1.7.0
	code.justin.tv/eventbus/schema v0.0.0-20210413174025-ae47eb9e5429
	code.justin.tv/foundation/twitchclient v4.11.0+incompatible
	code.justin.tv/hygienic/distconf v1.2.1
	code.justin.tv/hygienic/spade v1.6.0
	code.justin.tv/revenue/ripley v1.0.2
	code.justin.tv/revenue/subscriptions v0.0.0-20190823203358-d8896b48cfca
	code.justin.tv/sse/jwt v1.0.0 // indirect
	code.justin.tv/systems/sandstorm v1.6.6
	code.justin.tv/video/cloudwatchlogger v0.0.0-20201105223311-c83ba4f7eee4
	code.justin.tv/video/invoker v0.8.3 // indirect
	code.justin.tv/video/metrics-middleware/v2 v2.0.0
	code.justin.tv/web/users-service v2.12.2+incompatible
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/alicebob/miniredis/v2 v2.14.4
	github.com/avast/retry-go v2.7.0+incompatible
	github.com/aws/aws-dax-go v1.2.7
	github.com/aws/aws-lambda-go v1.17.0
	github.com/aws/aws-sdk-go v1.37.28
	github.com/bsm/redislock v0.4.2
	github.com/cactus/go-statsd-client/statsd v0.0.0-20200423205355-cb0885a1018c
	github.com/codegangsta/cli v1.11.1
	github.com/go-errors/errors v1.1.1 // indirect
	github.com/go-playground/validator/v10 v10.6.1
	github.com/go-redis/redis/v7 v7.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/go-yaml/yaml v0.0.0-00010101000000-000000000000
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/protobuf v1.4.3
	github.com/gopherjs/gopherjs v0.0.0-20190812055157-5d271430af9f // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/joomcode/errorx v0.8.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.7.0
	github.com/slack-go/slack v0.6.3
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.6.1
	github.com/twitchtv/twirp v7.1.0+incompatible
	github.com/zenazn/goji v0.9.1-0.20160507202103-64eb34159fe5
	go.uber.org/atomic v1.7.0 // indirect
	goji.io v2.0.2+incompatible
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	honnef.co/go/tools v0.1.3 // indirect
)

replace code.justin.tv/commerce/logrus => github.com/sirupsen/logrus v1.8.1

replace github.com/go-yaml/yaml => gopkg.in/yaml.v2 v2.4.0

replace github.com/cactus/go-statsd-client v3.1.1+incompatible => github.com/cactus/go-statsd-client v0.0.0-20190805010426-5089fcbbe532
