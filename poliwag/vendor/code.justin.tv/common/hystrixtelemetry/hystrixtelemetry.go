package hystrixtelemetry

import (
	"log"
	"time"

	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"

	logging "code.justin.tv/amzn/TwitchLogging"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	poller "code.justin.tv/amzn/TwitchTelemetryPollingCollector"
)

const (
	pollDuration          = 10 * time.Second
	circuitOpenMetricName = "CircuitOpen"
)

// TwitchTelemetryStatter is a wrapper struct that contains a TwitchTelemetry SampleReporter
// client to report metrics to Cloudwatch
type TwitchTelemetryStatter struct {
	client        *telemetry.SampleReporter
	gaugeDuration time.Duration
	logger        logging.Logger
}

type twitchTelemetryStatter struct {
	client       *telemetry.SampleReporter
	logger       logging.Logger
	circuitGauge *GaugePollingJob
}

// NewTwitchTelemetryStatter creates a new statter which sends metrics via TwitchTelemetry.
// It is meant to be used to work with the go hystrix package. A logger can optionally be supplied
func NewTwitchTelemetryStatter(client *telemetry.SampleReporter, gaugeDuration time.Duration, logger logging.Logger) *TwitchTelemetryStatter {
	return &TwitchTelemetryStatter{
		client:        client,
		logger:        logger,
		gaugeDuration: gaugeDuration,
	}
}

// Create makes a TwitchTelemetry client wrapper that can be integrated with hystrix metrics collector
func (t *TwitchTelemetryStatter) Create(circuitName string) metricCollector.MetricCollector {
	if t.client == nil {
		log.Fatalf("TwitchTelemetry client must be initialized before circuits are created.")
	}

	// Add the circuit name as a dimension so we don't have to prefix all of the metrics produced.
	// This copies a new client for each new circuit we build
	newClient := *t.client
	newClient.SampleBuilder.Dimensions = make(telemetry.DimensionSet)
	for k, v := range t.client.Dimensions {
		newClient.Dimensions[k] = v
	}
	newClient.SampleBuilder.Dimensions["Circuit"] = circuitName

	statter := &twitchTelemetryStatter{
		client: &newClient,
		logger: t.logger,
	}
	gauge := NewGaugePollingJob(circuitOpenMetricName)
	pollingCollector := poller.NewPollingCollector(gauge, t.gaugeDuration, &newClient.SampleBuilder, &newClient, t.logger)
	pollingCollector.Start()
	statter.circuitGauge = gauge
	return statter
}

// setGauge specifically sets the gauge value on the circuit open gauge
func (t *twitchTelemetryStatter) setCircuitOpenGauge(value float64) {
	t.circuitGauge.SetGauge(value)
}

func (t *twitchTelemetryStatter) incrementCounterMetric(suffix string, i float64) {
	if i == 0 {
		return
	}
	t.client.Report(suffix, i, telemetry.UnitCount)
}

func (t *twitchTelemetryStatter) updateTimerMetric(suffix string, dur time.Duration) {
	t.client.ReportDurationSample(suffix, dur)
}

// Update takes a metricCollector result for a hystrix circuit and submits
// them to the stat repository for the TwitchTelemetry client (in this case, CloudWatch)
func (t *twitchTelemetryStatter) Update(r metricCollector.MetricResult) {
	if r.Successes > 0 {
		t.setCircuitOpenGauge(0)
	} else if r.ShortCircuits > 0 {
		t.setCircuitOpenGauge(1)
	}
	t.incrementCounterMetric("Attempts", r.Attempts)
	t.incrementCounterMetric("Errors", r.Errors)
	t.incrementCounterMetric("Successes", r.Successes)
	t.incrementCounterMetric("Failures", r.Failures)
	t.incrementCounterMetric("Rejects", r.Rejects)
	t.incrementCounterMetric("ShortCircuits", r.ShortCircuits)
	t.incrementCounterMetric("Timeouts", r.Timeouts)
	t.incrementCounterMetric("FallbackSuccesses", r.FallbackSuccesses)
	t.incrementCounterMetric("FallbackFailures", r.FallbackFailures)
	t.updateTimerMetric("TotalDuration", r.TotalDuration)
	t.updateTimerMetric("RunDuration", r.RunDuration)
}

// Reset doesn't do anything since this statter doesn't have any state to maintain
func (t *twitchTelemetryStatter) Reset() {}
