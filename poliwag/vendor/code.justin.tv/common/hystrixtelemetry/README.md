# hystrixtelemetry

`hystrixtelemetry` is a stat plugin for the go Hystrix library that is designed to send metrics to Cloudwatch via TwitchTelemetry

It is modeled after the common/hystrixstatsd project.

## Usage

This library should be plugged in as middleware for the [afex/hystrix-go](https://github.com/afex/hystrix-go) project.
Simply drop your `TwitchTelemetry` SampleReporter into a `TwitchTelemetryStatter` and set the polling duration for the circuit gauges
(10s in the below example). Then, just pass the circuit creation closure onto the metricCollector registry.

```go
	hostname, err := os.Hostname()
	if err != nil {
		log.Warn(err.Error())
	}
	tPid := &identifier.ProcessIdentifier{
		Service:  "testService",
		Stage:    "staging",
		Substage: "canary",
		Region:   "us-west-2",
		Machine:  hostname,
	}
	sender := cw.NewUnbuffered(tPid, nil)
	sampleObserver := telemetry.NewBufferedAggregator(flushPeriod, bufferSize, aggregationPeriod, sender, nil)
	sampleReporter := &telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: *tPid},
		SampleObserver: sampleObserver,
	}
	twitchTelemetryStatter := hystrixtelemetry.NewTwitchTelemetryStatter(sampleReporter, 10*time.Second, nil)
	metricCollector.Registry.Register(twitchTelemetryStatter.Create)
```
