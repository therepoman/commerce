module code.justin.tv/common/hystrixtelemetry

go 1.13

require (
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55 // indirect
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191018223415-c7305aaf8766
	code.justin.tv/amzn/TwitchTelemetryPollingCollector v0.0.0-20190717173130-38476c0d862a
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	go.uber.org/atomic v1.5.0
)
