package hystrixtelemetry

import (
	"go.uber.org/atomic"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
)

// GaugePollingJob implements the poller.Job interface in the TwitchTelemetryPollignCollector
// package. It periodically sends a gauge metric to CloudWatch.
type GaugePollingJob struct {
	value *atomic.Float64
	name  string
}

// NewGaugePollingJob creates a new gauge polling job for a polling metrics collector
func NewGaugePollingJob(metricName string) *GaugePollingJob {
	return &GaugePollingJob{
		value: atomic.NewFloat64(0),
		name:  metricName,
	}
}

// Fetch retrieves the latest gauge value to be reported back to CloudWatch
func (g *GaugePollingJob) Fetch() ([]*telemetry.Sample, error) {
	samples := []*telemetry.Sample{
		{
			MetricID: *telemetry.NewMetricID(g.name),
			Value:    g.value.Load(),
			Unit:     telemetry.UnitCount,
		},
	}
	return samples, nil
}

// SetGauge allows us to set the value of the gauge monitored by this job
func (g *GaugePollingJob) SetGauge(value float64) {
	g.value.Store(value)
}
