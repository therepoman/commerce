package pkgpath

import (
	vpkgpath "code.justin.tv/video/pkgpath"
)

// Caller returns the import path of functions on the calling goroutine's
// stack.  The skip parameter indicates how many stack frames should be
// ascended, with 0 referring to the caller of Caller.  The return values are
// the import path of the caller and whether the import path could be
// determined.
//
// Deprecated: Use code.justin.tv/video/pkgpath instead.
func Caller(skip int) (string, bool) {
	return vpkgpath.Caller(skip + 1)
}
