package substwirp

// These error codes are meant to be returned within a substwirp.ClientError type.

const (
	// Generic error codes
	ErrCodeInvalidUser         = "INVALID_USER"
	ErrCodeInvalidProductOwner = "INVALID_PRODUCT_OWNER"

	// CreateEmoticon
	ErrCodeInvalidEmoticonPrefix = "INVALID_PREFIX"
	ErrCodeEmoticonLimit         = "EMOTICON_LIMIT_REACHED"
	ErrCodeCodeNotUnique         = "CODE_NOT_UNIQUE"
	ErrCodeInvalidSuffixFormat   = "INVALID_SUFFIX_FORMAT"
	ErrCodeImage28IDNotFound     = "IMAGE28ID_NOT_FOUND"
	ErrCodeImage56IDNotFound     = "IMAGE56ID_NOT_FOUND"
	ErrCodeImage112IDNotFound    = "IMAGE112ID_NOT_FOUND"

	// ErrCodeProductDisplayNameLimitReached represents error when product name specific is more than allowed limit.
	ErrCodeProductDisplayNameLimitReached = "PRODUCT_DISPLAY_NAME_LIMIT_REACHED"

	// SubmitEmoticonPrefix
	ErrCodePrefixNotUnique     = "PREFIX_NOT_UNIQUE"
	ErrCodeInvalidPrefixLength = "INVALID_PREFIX_LENGTH"
	ErrCodeInvalidPrefixRegex  = "INVALID_PREFIX_REGEX"
	ErrCodeInvalidPrefixState  = "INVALID_PREFIX_STATE"
	ErrCodePrefixUpdateTooSoon = "PREFIX_UPDATE_TOO_SOON"
	ErrCodeNotInGoodStanding   = "NOT_IN_GOOD_STANDING"
	ErrAffiliatePrefixUpdate   = "PREFIX_UPDATE_BEFORE_NAME_CHANGE"

	// CreateChannelBadgeUploadConfig
	ErrCodeInvalidBadgeSize = "INVALID_BADGE_SIZE"

	// CreateChannelBadge
	ErrCodeInvalidBadgeImage1XID            = "INVALID_BADGE_IMAGE_1X_ID"
	ErrCodeInvalidBadgeImage2XID            = "INVALID_BADGE_IMAGE_2X_ID"
	ErrCodeInvalidBadgeImage4XID            = "INVALID_BADGE_IMAGE_4X_ID"
	ErrCodeInvalidBadgeRequiredTenureMonths = "INVALID_BADGE_REQUIRED_TENURE_MONTHS"
	ErrCodeBadgeExists                      = "BADGE_EXISTS"
	ErrCodeBadgeTimeout                     = "BADGE_TIMEOUT"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
