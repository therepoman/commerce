package substwirp

import (
	"encoding/json"

	"code.justin.tv/common/twirp"
)

const (
	clientErrorMetaKey = "client_error"
)

// NewClientError returns a new ClientError that wraps a twirp.Error and stores
// an error code enum. If v is a struct with json tabs (defined in error_codes.go),
// ParseClientError can be used to deserialize to the same type. If v is a string,
// it is used as an error code and serialized as a ClientError.
func NewClientError(twErr twirp.Error, v interface{}) twirp.Error {
	if code, ok := v.(string); ok {
		v = ClientError{
			ErrorCode: code,
		}
	}

	b, err := json.Marshal(v)
	if err != nil {
		return twErr
	}

	return twErr.WithMeta(clientErrorMetaKey, string(b))
}

// ParseClientError takes an error, and if it is a ClientError unmarshals the error set
// by NewClientError to the value pointed to by v. Otherwise returns err.
func ParseClientError(err error, v interface{}) error {
	twErr, ok := err.(twirp.Error)
	if !ok {
		return err
	}

	clientErr := twErr.Meta(clientErrorMetaKey)
	if clientErr == "" {
		return err
	}

	jsonErr := json.Unmarshal([]byte(clientErr), v)
	if jsonErr != nil {
		return jsonErr
	}

	return nil
}
