package poller

import "code.justin.tv/amzn/TwitchTelemetry"

// PollingJob defines an interface which can be used to scrape data
// from an arbitrary source, returning a mapping from data names
// to their values
type PollingJob interface {
	Fetch() ([]*telemetry.Sample, error)
}
