# `TwitchTelemetryPollingCollector`

This package periodically polls and reports metrics that aren't within the context of a request, such as memory usage.
Normally, [Metrics Middleware](https://docs.fulton.twitch.a2z.com/docs/metrics.html#how-metricsmiddleware-works) can be
used to obtain metrics that are related to a specific request (such as duration and successes/ failures). This
package can be utilized to obtain metrics that are service/host-specific and not request/API-specific.

## Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchTelemetryPollingCollector`
