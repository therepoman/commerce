package poller

// Based on github.com/lthibault/jitterbug, but redesigned to
// properly distribute values

import (
	"math/rand"
	"time"
)

// JitterTicker behaves like time.Ticker
type JitterTicker struct {
	C        <-chan time.Time
	close    chan struct{}
	min      time.Duration
	interval time.Duration
}

// NewJitterTicker is a Ticker with the base interval d, and minimum interval min.
// When min >= d, min will be ignored and this will act like a typical time.Ticker with interval d
func NewJitterTicker(d, min time.Duration) (t *JitterTicker) {
	c := make(chan time.Time)
	t = &JitterTicker{
		C:        c,
		close:    make(chan struct{}),
		interval: d,
		min:      min,
	}
	go t.loop(c)
	return
}

// Stop the Ticker
func (t *JitterTicker) Stop() { close(t.close) }

func (t *JitterTicker) loop(c chan<- time.Time) {
	defer close(c)

	for {
		time.Sleep(t.calcDelay())

		select {
		case <-t.close:
			return
		case c <- time.Now():
		default: // there may be nobody ready to recv
		}
	}
}

func (t *JitterTicker) calcDelay() time.Duration {
	if t.min >= t.interval {
		return t.interval
	}
	interval := time.Duration(rand.Int63n(int64(t.interval - t.min)))
	return t.min + interval
}
