package oidciface

import (
	"context"
	"time"

	"code.justin.tv/amzn/TwitchS2S2/internal/oidc"
)

// OIDCAPI is the interface of oidc.OIDC
type OIDCAPI interface {
	Configuration() *oidc.Configuration
	ValidationKeys(context.Context) (map[string]oidc.ValidationKey, time.Duration, error)
	ValidationKey(context.Context, string) (oidc.ValidationKey, error)
}

// ValidationKey is just used for mocks
type ValidationKey interface {
	oidc.ValidationKey
}
