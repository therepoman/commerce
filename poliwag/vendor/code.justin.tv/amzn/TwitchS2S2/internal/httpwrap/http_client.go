package httpwrap

import (
	"net/http"
)

// HTTPClient is the interface twirp expects
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

// HTTPClientFromRoundTripper makes a http.RoundTripper act like an HTTPClient
func HTTPClientFromRoundTripper(rt http.RoundTripper) HTTPClient {
	return &httpClient{RoundTripper: rt}
}

// httpClient wraps http.RoundTripper to make it act like an HTTPClient
type httpClient struct {
	RoundTripper http.RoundTripper
}

func (h *httpClient) Do(req *http.Request) (*http.Response, error) {
	return h.RoundTripper.RoundTrip(req)
}
