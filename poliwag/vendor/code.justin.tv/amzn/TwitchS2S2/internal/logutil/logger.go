package logutil

import logging "code.justin.tv/amzn/TwitchLogging"

// Logger used for log messages in this package
//
// Generally, RateLimitedLogger should be used instead.
type Logger struct {
	logging.Logger
}

// LogAnonymousRequest is when an anonymous request is received in metrics only
// mode.
func (l *Logger) LogAnonymousRequest(clientIP, forwardedForIP, forwardedForProto, forwardedForPort string) {
	l.Log(
		"AnonymousRequestReceived",
		"client_ip", clientIP,
		"x_forwarded_for", forwardedForIP,
		"x_forwarded_proto", forwardedForProto,
		"x_forwarded_port", forwardedForPort,
	)
}
