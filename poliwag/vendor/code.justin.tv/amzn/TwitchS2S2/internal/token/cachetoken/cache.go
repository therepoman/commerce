package cachetoken

import (
	"context"
	"sync"

	"code.justin.tv/amzn/TwitchS2S2/internal/token"
)

// CachedTokens is the interface Cache implements
type CachedTokens interface {
	token.Tokens
	HardRefreshCache(context.Context) error
}

// New returns a new token cache
func New(
	inner token.Tokens,
	cacheKey func(*token.Options) string,
	cacheValueStale func(*token.Options, *token.Token) bool,
	cacheValueExpired func(*token.Options, *token.Token) bool,
) *Cache {
	return &Cache{
		inner:             inner,
		cacheKey:          cacheKey,
		cacheValueStale:   cacheValueStale,
		cacheValueExpired: cacheValueExpired,
		values:            make(map[string]*cacheValue),
	}
}

type cacheValue struct {
	Options *token.Options
	Token   *token.Token
}

// Cache is a wrapper around token.Tokens that caches tokens based on a cache
// key, cache staleness policy, and a cache expiration policy.
//
// If a cache is stale but the underlying client returns an error, the stale
// cache will be returned.
//
// If a cache is expired but the underlying client returns an error, the error
// will be surfaced.
type Cache struct {
	inner             token.Tokens
	cacheKey          func(*token.Options) string
	cacheValueStale   func(*token.Options, *token.Token) bool
	cacheValueExpired func(*token.Options, *token.Token) bool

	values map[string]*cacheValue
	lock   sync.RWMutex
}

// HardRefreshCache will hard refresh all tokens in our cache. Running this in a
// goroutine every 10 minutes will optimize P100 latencies for your service
// since tokens won't need to be refetched in hot request paths.
func (c *Cache) HardRefreshCache(ctx context.Context) error {
	var cacheValues []*cacheValue
	func() {
		c.lock.RLock()
		defer c.lock.RUnlock()

		cacheValues = make([]*cacheValue, 0, len(c.values))
		for _, cacheValue := range c.values {
			cacheValues = append(cacheValues, cacheValue)
		}
	}()

	for _, cv := range cacheValues {
		var options token.Options
		options = *cv.Options
		options.NoCache = true
		if _, err := c.Token(ctx, &options); err != nil {
			return err
		}
	}

	return nil
}

// Token implements token.Tokens
func (c *Cache) Token(ctx context.Context, options *token.Options) (*token.Token, error) {
	var cacheVal *cacheValue
	var val *token.Token
	var cacheValOK, expired, stale bool
	var err error
	cacheKey := c.cacheKey(options)

	func() {
		c.lock.RLock()
		defer c.lock.RUnlock()

		cacheVal, cacheValOK = c.values[cacheKey]

		stale = true
		expired = true

		if cacheVal != nil {
			stale = c.cacheValueStale(options, cacheVal.Token)
			expired = c.cacheValueExpired(options, cacheVal.Token)
		}
	}()

	if !options.NoCache && cacheValOK && !stale && !expired {
		return cacheVal.Token, nil
	}

	val, err = func() (*token.Token, error) {
		c.lock.Lock()
		defer c.lock.Unlock()

		options.MustSucceed = !cacheValOK || expired

		val, err = c.inner.Token(ctx, options)
		if err != nil {
			if !options.NoCache && !expired {
				// return the cached value on error since it's likely still valid unless
				// expired.
				return cacheVal.Token, nil
			}
			return nil, err
		}
		c.values[cacheKey] = &cacheValue{Token: val, Options: options}
		return val, nil
	}()

	return val, err
}
