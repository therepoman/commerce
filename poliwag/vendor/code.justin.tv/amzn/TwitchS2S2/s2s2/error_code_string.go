package s2s2

// serverErrorCodeString converts an error code int to an error code string
// for Twirp error logs
func errorCodeString(code int) string {
	switch code {
	case 400:
		return "out_of_range"
	case 401:
		return "unauthenticated"
	case 403:
		return "permission_denied"
	case 404:
		return "not_found"
	case 408:
		return "canceled"
	case 409:
		return "already_exists"
	case 412:
		return "failed_precondition"
	case 500:
		return "internal"
	case 501:
		return "unimplemented"
	case 503:
		return "unavailable"
	default:
		return ""
	}
}
