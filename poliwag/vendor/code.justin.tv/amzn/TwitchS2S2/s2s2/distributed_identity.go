package s2s2

import "strings"

// DistributedIdentities is a helper function for building a list of distributed
// identities.
//
// This function panics if Service or Stage is empty when attempting to build
// this.
func DistributedIdentities(vals ...DistributedIdentity) string {
	var b strings.Builder
	for nVal, val := range vals {
		if nVal > 0 {
			b.WriteString(",")
		}
		b.WriteString(val.Service)
		b.WriteString("/")
		b.WriteString(val.Stage)
	}
	return b.String()
}

// DistributedIdentity is a helper function for building a list of distributed
// identities.
type DistributedIdentity struct {
	Service, Stage string
}
