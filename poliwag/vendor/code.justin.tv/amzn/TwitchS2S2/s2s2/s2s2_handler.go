package s2s2

import (
	"net/http"

	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/internal/logutil/logutiliface"
	"code.justin.tv/amzn/TwitchS2S2/internal/opwrap"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/s2s2dicallee"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

// s2s2Handler implements Handler.
type s2s2Handler struct {
	http.Handler
	NoAuthHandler    http.Handler
	OperationStarter *operation.Starter

	S2S2   *S2S2
	Config *c7s.Config
	Logger logutiliface.LoggerAPI
}

// Returns a handler to use for onboarding services with existing production
// traffic. This will check authentication and authorization only if the
// Authorization header is provided.
func (h *s2s2Handler) PassthroughIfAuthorizationNotPresented() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if len(r.Header.Get("Authorization")) == 0 {
			ctx, op := h.OperationStarter.StartOp(r.Context(), opwrap.ServePassthroughRequest)
			defer opwrap.EndWithError(op, nil)

			h.NoAuthHandler.ServeHTTP(w, r.WithContext(ctx))
			return
		}
		h.Handler.ServeHTTP(w, r)
	})
}

func (h *s2s2Handler) RecordMetricsOnly() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var opName operation.Name

		// Check for whether an auth header is from Distributed Identities caller
		diAuthSub, err := h.S2S2.diCallee.ValidateAuthentication(r)
		if err != nil {
			switch err {
			case s2s2dicallee.ErrNoAuthenticationPresented:
				opName = opwrap.S2S2AuthHeaderMissing
				h.Logger.LogAnonymousRequest(
					r.RemoteAddr,
					r.Header.Get("X-Forwarded-For"),
					r.Header.Get("X-Forwarded-Proto"),
					r.Header.Get("X-Forwarded-Port"),
				)
			case s2s2dicallee.ErrMissingX5UHeader:
				opName = opwrap.S2S2AuthHeaderMissingX5U
			default:
				if authz, err := h.S2S2.parseAuthorizationHeaderAndValidate(w, r, nil); err != nil {
					opName = opwrap.S2S2AuthHeaderInvalid
				} else {
					h.NoAuthHandler.ServeHTTP(w, r.WithContext(SetRequestSubject(r.Context(), &authorizedSubject{
						Subject: authz.Subject,
						scope:   authz.Scope,
						tokenID: authz.JWTID,
					})))
					return
				}
			}

			ctx, op := h.OperationStarter.StartOp(r.Context(), opName)
			defer opwrap.EndWithError(op, nil)

			h.NoAuthHandler.ServeHTTP(w, r.WithContext(ctx))
			return
		}

		h.NoAuthHandler.ServeHTTP(w, r.WithContext(SetRequestSubject(
			setCurrentRequest(r.Context(), r),
			&distributedIdentitiesAuthorizedSubject{
				AuthenticatedSubject: diAuthSub,
				cfg:                  h.Config,
			},
		)))
		return

	})
}
