// Code generated by protoc-gen-go. DO NOT EDIT.
// source: code.justin.tv/amzn/PDMSLambdaTwirp/models.proto

package PDMSLambdaTwirp

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type DeletionStatus int32

const (
	// Indicates the user has been wiped clean
	DeletionStatus_NODATAINSYSTEM DeletionStatus = 0
	// Indicates the deletion has been promised for the user
	DeletionStatus_PROMISED DeletionStatus = 1
	// Indicates no response from a service for this request
	DeletionStatus_NORESPONSE DeletionStatus = 2
)

var DeletionStatus_name = map[int32]string{
	0: "NODATAINSYSTEM",
	1: "PROMISED",
	2: "NORESPONSE",
}

var DeletionStatus_value = map[string]int32{
	"NODATAINSYSTEM": 0,
	"PROMISED":       1,
	"NORESPONSE":     2,
}

func (x DeletionStatus) String() string {
	return proto.EnumName(DeletionStatus_name, int32(x))
}

func (DeletionStatus) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_282d407ef7a50121, []int{0}
}

// This represents an access "request" by PDMS to a particular service
type AccessState struct {
	ServiceId            string               `protobuf:"bytes,1,opt,name=service_id,json=serviceId,proto3" json:"service_id,omitempty"`
	UserId               string               `protobuf:"bytes,2,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Completed            bool                 `protobuf:"varint,3,opt,name=completed,proto3" json:"completed,omitempty"`
	HasData              bool                 `protobuf:"varint,4,opt,name=has_data,json=hasData,proto3" json:"has_data,omitempty"`
	LastUpdate           *timestamp.Timestamp `protobuf:"bytes,5,opt,name=last_update,json=lastUpdate,proto3" json:"last_update,omitempty"`
	Created              *timestamp.Timestamp `protobuf:"bytes,6,opt,name=created,proto3" json:"created,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *AccessState) Reset()         { *m = AccessState{} }
func (m *AccessState) String() string { return proto.CompactTextString(m) }
func (*AccessState) ProtoMessage()    {}
func (*AccessState) Descriptor() ([]byte, []int) {
	return fileDescriptor_282d407ef7a50121, []int{0}
}

func (m *AccessState) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AccessState.Unmarshal(m, b)
}
func (m *AccessState) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AccessState.Marshal(b, m, deterministic)
}
func (m *AccessState) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AccessState.Merge(m, src)
}
func (m *AccessState) XXX_Size() int {
	return xxx_messageInfo_AccessState.Size(m)
}
func (m *AccessState) XXX_DiscardUnknown() {
	xxx_messageInfo_AccessState.DiscardUnknown(m)
}

var xxx_messageInfo_AccessState proto.InternalMessageInfo

func (m *AccessState) GetServiceId() string {
	if m != nil {
		return m.ServiceId
	}
	return ""
}

func (m *AccessState) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *AccessState) GetCompleted() bool {
	if m != nil {
		return m.Completed
	}
	return false
}

func (m *AccessState) GetHasData() bool {
	if m != nil {
		return m.HasData
	}
	return false
}

func (m *AccessState) GetLastUpdate() *timestamp.Timestamp {
	if m != nil {
		return m.LastUpdate
	}
	return nil
}

func (m *AccessState) GetCreated() *timestamp.Timestamp {
	if m != nil {
		return m.Created
	}
	return nil
}

// This represents a deletion "request" by PDMS to a particular service
type DeletionState struct {
	ServiceId            string               `protobuf:"bytes,1,opt,name=service_id,json=serviceId,proto3" json:"service_id,omitempty"`
	UserId               string               `protobuf:"bytes,2,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	DeletionStatus       DeletionStatus       `protobuf:"varint,3,opt,name=deletion_status,json=deletionStatus,proto3,enum=twitch.fulton.privacy.pdmsservice.DeletionStatus" json:"deletion_status,omitempty"`
	Created              *timestamp.Timestamp `protobuf:"bytes,4,opt,name=created,proto3" json:"created,omitempty"`
	PromiseDue           *timestamp.Timestamp `protobuf:"bytes,5,opt,name=promise_due,json=promiseDue,proto3" json:"promise_due,omitempty"`
	DeletedOn            *timestamp.Timestamp `protobuf:"bytes,6,opt,name=deleted_on,json=deletedOn,proto3" json:"deleted_on,omitempty"`
	Completed            bool                 `protobuf:"varint,7,opt,name=completed,proto3" json:"completed,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *DeletionState) Reset()         { *m = DeletionState{} }
func (m *DeletionState) String() string { return proto.CompactTextString(m) }
func (*DeletionState) ProtoMessage()    {}
func (*DeletionState) Descriptor() ([]byte, []int) {
	return fileDescriptor_282d407ef7a50121, []int{1}
}

func (m *DeletionState) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeletionState.Unmarshal(m, b)
}
func (m *DeletionState) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeletionState.Marshal(b, m, deterministic)
}
func (m *DeletionState) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeletionState.Merge(m, src)
}
func (m *DeletionState) XXX_Size() int {
	return xxx_messageInfo_DeletionState.Size(m)
}
func (m *DeletionState) XXX_DiscardUnknown() {
	xxx_messageInfo_DeletionState.DiscardUnknown(m)
}

var xxx_messageInfo_DeletionState proto.InternalMessageInfo

func (m *DeletionState) GetServiceId() string {
	if m != nil {
		return m.ServiceId
	}
	return ""
}

func (m *DeletionState) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *DeletionState) GetDeletionStatus() DeletionStatus {
	if m != nil {
		return m.DeletionStatus
	}
	return DeletionStatus_NODATAINSYSTEM
}

func (m *DeletionState) GetCreated() *timestamp.Timestamp {
	if m != nil {
		return m.Created
	}
	return nil
}

func (m *DeletionState) GetPromiseDue() *timestamp.Timestamp {
	if m != nil {
		return m.PromiseDue
	}
	return nil
}

func (m *DeletionState) GetDeletedOn() *timestamp.Timestamp {
	if m != nil {
		return m.DeletedOn
	}
	return nil
}

func (m *DeletionState) GetCompleted() bool {
	if m != nil {
		return m.Completed
	}
	return false
}

func init() {
	proto.RegisterEnum("twitch.fulton.privacy.pdmsservice.DeletionStatus", DeletionStatus_name, DeletionStatus_value)
	proto.RegisterType((*AccessState)(nil), "twitch.fulton.privacy.pdmsservice.AccessState")
	proto.RegisterType((*DeletionState)(nil), "twitch.fulton.privacy.pdmsservice.DeletionState")
}

func init() {
	proto.RegisterFile("code.justin.tv/amzn/PDMSLambdaTwirp/models.proto", fileDescriptor_282d407ef7a50121)
}

var fileDescriptor_282d407ef7a50121 = []byte{
	// 427 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x93, 0xcf, 0x6e, 0xd3, 0x40,
	0x10, 0xc6, 0x71, 0x28, 0xf9, 0x33, 0x01, 0x13, 0xed, 0x85, 0x50, 0x81, 0x08, 0x45, 0x48, 0x11,
	0x87, 0x35, 0x14, 0x2e, 0x88, 0x53, 0x2a, 0xe7, 0x10, 0x89, 0xc4, 0x91, 0x1d, 0x0e, 0xf4, 0x62,
	0x6d, 0xbc, 0xd3, 0x66, 0x91, 0xed, 0xb5, 0xbc, 0xb3, 0xa9, 0xe0, 0xa5, 0x11, 0x6f, 0x80, 0xec,
	0x3a, 0xa2, 0xe1, 0x92, 0x48, 0x3d, 0xce, 0xcc, 0x37, 0xb3, 0xbf, 0x6f, 0x46, 0x0b, 0xef, 0x13,
	0x2d, 0x91, 0xff, 0xb0, 0x86, 0x54, 0xce, 0x69, 0xeb, 0x89, 0xec, 0x57, 0xee, 0x2d, 0xfd, 0x79,
	0xf4, 0x55, 0x64, 0x6b, 0x29, 0x56, 0x37, 0xaa, 0x2c, 0xbc, 0x4c, 0x4b, 0x4c, 0x0d, 0x2f, 0x4a,
	0x4d, 0x9a, 0xbd, 0xa6, 0x1b, 0x45, 0xc9, 0x86, 0x5f, 0xd9, 0x94, 0x74, 0xce, 0x8b, 0x52, 0x6d,
	0x45, 0xf2, 0x93, 0x17, 0x32, 0x33, 0x06, 0xcb, 0xad, 0x4a, 0xf0, 0xf4, 0xd5, 0xb5, 0xd6, 0xd7,
	0x29, 0x7a, 0x75, 0xc3, 0xda, 0x5e, 0x79, 0xa4, 0x32, 0x34, 0x24, 0xb2, 0xe2, 0x76, 0xc6, 0xd9,
	0x1f, 0x07, 0xfa, 0x93, 0x24, 0x41, 0x63, 0x22, 0x12, 0x84, 0xec, 0x25, 0x40, 0xd3, 0x1b, 0x2b,
	0x39, 0x74, 0x46, 0xce, 0xb8, 0x17, 0xf6, 0x9a, 0xcc, 0x4c, 0xb2, 0x67, 0xd0, 0xb1, 0x06, 0xcb,
	0xaa, 0xd6, 0xaa, 0x6b, 0xed, 0x2a, 0x9c, 0x49, 0xf6, 0x02, 0x7a, 0x89, 0xce, 0x8a, 0x14, 0x09,
	0xe5, 0xf0, 0xe1, 0xc8, 0x19, 0x77, 0xc3, 0x7f, 0x09, 0xf6, 0x1c, 0xba, 0x1b, 0x61, 0x62, 0x29,
	0x48, 0x0c, 0x4f, 0xea, 0x62, 0x67, 0x23, 0x8c, 0x2f, 0x48, 0xb0, 0x2f, 0xd0, 0x4f, 0x85, 0xa1,
	0xd8, 0x16, 0x52, 0x10, 0x0e, 0x1f, 0x8d, 0x9c, 0x71, 0xff, 0xfc, 0x94, 0xdf, 0x72, 0xf3, 0x1d,
	0x37, 0x5f, 0xed, 0xb8, 0x43, 0xa8, 0xe4, 0xdf, 0x6a, 0x35, 0xfb, 0x04, 0x9d, 0xa4, 0x44, 0x51,
	0xbd, 0xd9, 0x3e, 0xd8, 0xb8, 0x93, 0x9e, 0xfd, 0x6e, 0xc1, 0x13, 0x1f, 0x53, 0x24, 0xa5, 0xf3,
	0xfb, 0xb9, 0xbe, 0x84, 0xa7, 0xb2, 0x19, 0x14, 0x1b, 0x12, 0x64, 0x4d, 0xed, 0xdd, 0x3d, 0xff,
	0xc0, 0x0f, 0xde, 0x86, 0xdf, 0x45, 0xb0, 0x26, 0x74, 0xe5, 0x5e, 0x7c, 0xd7, 0xdb, 0xc9, 0xd1,
	0xde, 0xaa, 0x75, 0x16, 0xa5, 0xce, 0x94, 0xc1, 0x58, 0xda, 0xa3, 0xd6, 0xd9, 0xc8, 0x7d, 0x8b,
	0xec, 0x33, 0x40, 0x0d, 0x81, 0x32, 0xd6, 0xf9, 0x11, 0x1b, 0xed, 0x35, 0xea, 0x20, 0xdf, 0xbf,
	0x7f, 0xe7, 0xbf, 0xfb, 0xbf, 0xbb, 0x00, 0x77, 0xdf, 0x2d, 0x63, 0xe0, 0x2e, 0x02, 0x7f, 0xb2,
	0x9a, 0xcc, 0x16, 0xd1, 0xf7, 0x68, 0x35, 0x9d, 0x0f, 0x1e, 0xb0, 0xc7, 0xd0, 0x5d, 0x86, 0xc1,
	0x7c, 0x16, 0x4d, 0xfd, 0x81, 0xc3, 0x5c, 0x80, 0x45, 0x10, 0x4e, 0xa3, 0x65, 0xb0, 0x88, 0xa6,
	0x83, 0xd6, 0xc5, 0xdb, 0xcb, 0x37, 0x47, 0xfc, 0x90, 0x75, 0xbb, 0xe6, 0xfc, 0xf8, 0x37, 0x00,
	0x00, 0xff, 0xff, 0x44, 0x52, 0xb9, 0xd5, 0x4f, 0x03, 0x00, 0x00,
}
