package encryptioncontext

import (
	"bytes"
	"fmt"
	"sort"
)

// Serialize serializes the encryption context into []bytes, can be used for
// AAD or composite key material
func Serialize(ec map[string]string) []byte {
	if len(ec) == 0 {
		return []byte{}
	}

	ecList := make([]string, 0, len(ec))
	for key := range ec {
		ecList = append(ecList, key)
	}
	sort.Strings(ecList)
	var buf bytes.Buffer

	for _, key := range ecList {
		fmt.Fprintf(&buf, "%s %s ", key, ec[key])
	}
	return buf.Bytes()
}
