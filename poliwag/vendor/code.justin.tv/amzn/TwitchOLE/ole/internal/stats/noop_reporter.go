package stats

//Implements oleiface.SampleReporterAPI
type NoopReporter struct {
}

func (r *NoopReporter) Report(metricName string, value float64, units string) {
}
