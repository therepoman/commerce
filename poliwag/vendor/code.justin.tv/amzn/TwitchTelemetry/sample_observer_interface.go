package telemetry

// SampleObserver represents an observer that accepts Samples.
// For example, a statsd implementation that needs access to samples as they happen.
//
// SampleObservers are responsible for aggregating samples as they stream in. SampleObservers
// can use the basic aggregator implementation provided in this package for a sane default
// aggregator that should work in most cases.
type SampleObserver interface {
	ObserveSample(sample *Sample)
	Flush()
	Stop()
}
