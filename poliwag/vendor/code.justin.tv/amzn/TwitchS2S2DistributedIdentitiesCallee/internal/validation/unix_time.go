package validation

import (
	"encoding/json"
	"time"
)

type unixTime time.Time

func (t *unixTime) UnmarshalJSON(in []byte) error {
	var raw int64
	if err := json.Unmarshal(in, &raw); err != nil {
		return err
	}

	*(*time.Time)(t) = time.Unix(raw, 0).UTC()
	return nil
}
