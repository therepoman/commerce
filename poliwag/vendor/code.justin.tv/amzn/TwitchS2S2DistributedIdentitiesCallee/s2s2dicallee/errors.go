package s2s2dicallee

import (
	"errors"
	"fmt"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation"
)

// errors returned by this package
var (
	ErrNoAuthenticationPresented = errors.New("no Authorization header presented")
	ErrNotDistributedIdentity    = errors.New("authorization header presented is not a s2s2 distributed identity")
	ErrMissingX5UHeader          = validation.ErrMissingX5UHeader
)

// ErrInvalidAuthorizationHeader is returned when the authorization header is in
// the wrong format
type ErrInvalidAuthorizationHeader struct {
	Value string
}

func (err ErrInvalidAuthorizationHeader) Error() string {
	return fmt.Sprintf("Invalid %s header recived: %s", authorizationHeader, err.Value)
}
