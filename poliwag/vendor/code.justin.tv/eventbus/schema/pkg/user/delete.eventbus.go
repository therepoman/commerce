// NOTE this is a generated file! do not edit!

package user

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	DeleteEventType = "UserDelete"
)

type Delete = UserDelete

type UserDeleteHandler func(context.Context, *eventbus.Header, *UserDelete) error

func (h UserDeleteHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserDelete{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserDeleteHandler(mux *eventbus.Mux, f UserDeleteHandler) {
	mux.RegisterHandler(DeleteEventType, f.Handler())
}

func RegisterDeleteHandler(mux *eventbus.Mux, f UserDeleteHandler) {
	RegisterUserDeleteHandler(mux, f)
}

func (*UserDelete) EventBusName() string {
	return DeleteEventType
}
