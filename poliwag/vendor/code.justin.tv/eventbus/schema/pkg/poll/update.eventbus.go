// NOTE this is a generated file! do not edit!

package poll

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "PollUpdate"
)

type Update = PollUpdate

type PollUpdateHandler func(context.Context, *eventbus.Header, *PollUpdate) error

func (h PollUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &PollUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterPollUpdateHandler(mux *eventbus.Mux, f PollUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f PollUpdateHandler) {
	RegisterPollUpdateHandler(mux, f)
}

func (*PollUpdate) EventBusName() string {
	return UpdateEventType
}
