// NOTE this is a generated file! do not edit!

package poll

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "PollCreate"
)

type Create = PollCreate

type PollCreateHandler func(context.Context, *eventbus.Header, *PollCreate) error

func (h PollCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &PollCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterPollCreateHandler(mux *eventbus.Mux, f PollCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f PollCreateHandler) {
	RegisterPollCreateHandler(mux, f)
}

func (*PollCreate) EventBusName() string {
	return CreateEventType
}
