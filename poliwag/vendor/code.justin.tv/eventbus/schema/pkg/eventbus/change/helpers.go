package change

type Changeable interface {
	GetUpdated() bool
}

// AnyChanged returns true if any of the passed-in changeables has been updated.
func AnyChanged(items ...Changeable) bool {
	for _, item := range items {
		if item.GetUpdated() {
			return true
		}
	}
	return false
}

// AllChanged returns true only if all the passed-in changeables has been updated.
func AllChanged(items ...Changeable) bool {
	for _, item := range items {
		if !item.GetUpdated() {
			return false
		}
	}
	return true
}

// codegen hints and compile-time assertion

var _ Changeable = (*Change)(nil)
var _ Changeable = (*StringChange)(nil)
var _ Changeable = (*Int64Change)(nil)
