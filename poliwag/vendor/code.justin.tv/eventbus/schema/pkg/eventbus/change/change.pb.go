// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto-lib/eventbus/change.proto

package change // import "code.justin.tv/eventbus/schema/pkg/eventbus/change"

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import timestamp "github.com/golang/protobuf/ptypes/timestamp"
import wrappers "github.com/golang/protobuf/ptypes/wrappers"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// Denotes a field that can change
type Change struct {
	Updated              bool     `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Change) Reset()         { *m = Change{} }
func (m *Change) String() string { return proto.CompactTextString(m) }
func (*Change) ProtoMessage()    {}
func (*Change) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{0}
}
func (m *Change) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Change.Unmarshal(m, b)
}
func (m *Change) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Change.Marshal(b, m, deterministic)
}
func (dst *Change) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Change.Merge(dst, src)
}
func (m *Change) XXX_Size() int {
	return xxx_messageInfo_Change.Size(m)
}
func (m *Change) XXX_DiscardUnknown() {
	xxx_messageInfo_Change.DiscardUnknown(m)
}

var xxx_messageInfo_Change proto.InternalMessageInfo

func (m *Change) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

// Denotes a string that can change and allows for new values to be set.
type StringChange struct {
	Updated              bool     `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                string   `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool     `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             string   `protobuf:"bytes,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StringChange) Reset()         { *m = StringChange{} }
func (m *StringChange) String() string { return proto.CompactTextString(m) }
func (*StringChange) ProtoMessage()    {}
func (*StringChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{1}
}
func (m *StringChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StringChange.Unmarshal(m, b)
}
func (m *StringChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StringChange.Marshal(b, m, deterministic)
}
func (dst *StringChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StringChange.Merge(dst, src)
}
func (m *StringChange) XXX_Size() int {
	return xxx_messageInfo_StringChange.Size(m)
}
func (m *StringChange) XXX_DiscardUnknown() {
	xxx_messageInfo_StringChange.DiscardUnknown(m)
}

var xxx_messageInfo_StringChange proto.InternalMessageInfo

func (m *StringChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *StringChange) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

func (m *StringChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *StringChange) GetOldValue() string {
	if m != nil {
		return m.OldValue
	}
	return ""
}

// Denotes an integer value that can change and allows for new values to be set.
type Int64Change struct {
	Updated              bool     `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                int64    `protobuf:"varint,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool     `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             int64    `protobuf:"varint,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Int64Change) Reset()         { *m = Int64Change{} }
func (m *Int64Change) String() string { return proto.CompactTextString(m) }
func (*Int64Change) ProtoMessage()    {}
func (*Int64Change) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{2}
}
func (m *Int64Change) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Int64Change.Unmarshal(m, b)
}
func (m *Int64Change) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Int64Change.Marshal(b, m, deterministic)
}
func (dst *Int64Change) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Int64Change.Merge(dst, src)
}
func (m *Int64Change) XXX_Size() int {
	return xxx_messageInfo_Int64Change.Size(m)
}
func (m *Int64Change) XXX_DiscardUnknown() {
	xxx_messageInfo_Int64Change.DiscardUnknown(m)
}

var xxx_messageInfo_Int64Change proto.InternalMessageInfo

func (m *Int64Change) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *Int64Change) GetValue() int64 {
	if m != nil {
		return m.Value
	}
	return 0
}

func (m *Int64Change) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *Int64Change) GetOldValue() int64 {
	if m != nil {
		return m.OldValue
	}
	return 0
}

// Denotes a boolean value that can change and allows for new values to be set.
type BoolChange struct {
	Updated              bool     `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                bool     `protobuf:"varint,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool     `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             bool     `protobuf:"varint,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BoolChange) Reset()         { *m = BoolChange{} }
func (m *BoolChange) String() string { return proto.CompactTextString(m) }
func (*BoolChange) ProtoMessage()    {}
func (*BoolChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{3}
}
func (m *BoolChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BoolChange.Unmarshal(m, b)
}
func (m *BoolChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BoolChange.Marshal(b, m, deterministic)
}
func (dst *BoolChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BoolChange.Merge(dst, src)
}
func (m *BoolChange) XXX_Size() int {
	return xxx_messageInfo_BoolChange.Size(m)
}
func (m *BoolChange) XXX_DiscardUnknown() {
	xxx_messageInfo_BoolChange.DiscardUnknown(m)
}

var xxx_messageInfo_BoolChange proto.InternalMessageInfo

func (m *BoolChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *BoolChange) GetValue() bool {
	if m != nil {
		return m.Value
	}
	return false
}

func (m *BoolChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *BoolChange) GetOldValue() bool {
	if m != nil {
		return m.OldValue
	}
	return false
}

// Denotes a string array that can change and allows for new values to be set.
type StringArrayChange struct {
	Updated              bool     `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                []string `protobuf:"bytes,2,rep,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool     `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             []string `protobuf:"bytes,4,rep,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StringArrayChange) Reset()         { *m = StringArrayChange{} }
func (m *StringArrayChange) String() string { return proto.CompactTextString(m) }
func (*StringArrayChange) ProtoMessage()    {}
func (*StringArrayChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{4}
}
func (m *StringArrayChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StringArrayChange.Unmarshal(m, b)
}
func (m *StringArrayChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StringArrayChange.Marshal(b, m, deterministic)
}
func (dst *StringArrayChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StringArrayChange.Merge(dst, src)
}
func (m *StringArrayChange) XXX_Size() int {
	return xxx_messageInfo_StringArrayChange.Size(m)
}
func (m *StringArrayChange) XXX_DiscardUnknown() {
	xxx_messageInfo_StringArrayChange.DiscardUnknown(m)
}

var xxx_messageInfo_StringArrayChange proto.InternalMessageInfo

func (m *StringArrayChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *StringArrayChange) GetValue() []string {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *StringArrayChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *StringArrayChange) GetOldValue() []string {
	if m != nil {
		return m.OldValue
	}
	return nil
}

// Denotes a timestamp that can change and allows for new values to be set.
type TimestampChange struct {
	Updated              bool                 `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                *timestamp.Timestamp `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool                 `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             *timestamp.Timestamp `protobuf:"bytes,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *TimestampChange) Reset()         { *m = TimestampChange{} }
func (m *TimestampChange) String() string { return proto.CompactTextString(m) }
func (*TimestampChange) ProtoMessage()    {}
func (*TimestampChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{5}
}
func (m *TimestampChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TimestampChange.Unmarshal(m, b)
}
func (m *TimestampChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TimestampChange.Marshal(b, m, deterministic)
}
func (dst *TimestampChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TimestampChange.Merge(dst, src)
}
func (m *TimestampChange) XXX_Size() int {
	return xxx_messageInfo_TimestampChange.Size(m)
}
func (m *TimestampChange) XXX_DiscardUnknown() {
	xxx_messageInfo_TimestampChange.DiscardUnknown(m)
}

var xxx_messageInfo_TimestampChange proto.InternalMessageInfo

func (m *TimestampChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *TimestampChange) GetValue() *timestamp.Timestamp {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *TimestampChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *TimestampChange) GetOldValue() *timestamp.Timestamp {
	if m != nil {
		return m.OldValue
	}
	return nil
}

// Denotes a wrapped integer value that can change and allows for new values to be set.
type Int64ValueChange struct {
	Updated              bool                 `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                *wrappers.Int64Value `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool                 `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             *wrappers.Int64Value `protobuf:"bytes,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Int64ValueChange) Reset()         { *m = Int64ValueChange{} }
func (m *Int64ValueChange) String() string { return proto.CompactTextString(m) }
func (*Int64ValueChange) ProtoMessage()    {}
func (*Int64ValueChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{6}
}
func (m *Int64ValueChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Int64ValueChange.Unmarshal(m, b)
}
func (m *Int64ValueChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Int64ValueChange.Marshal(b, m, deterministic)
}
func (dst *Int64ValueChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Int64ValueChange.Merge(dst, src)
}
func (m *Int64ValueChange) XXX_Size() int {
	return xxx_messageInfo_Int64ValueChange.Size(m)
}
func (m *Int64ValueChange) XXX_DiscardUnknown() {
	xxx_messageInfo_Int64ValueChange.DiscardUnknown(m)
}

var xxx_messageInfo_Int64ValueChange proto.InternalMessageInfo

func (m *Int64ValueChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *Int64ValueChange) GetValue() *wrappers.Int64Value {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *Int64ValueChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *Int64ValueChange) GetOldValue() *wrappers.Int64Value {
	if m != nil {
		return m.OldValue
	}
	return nil
}

// Denotes a wrapped string value that can change and allows for new values to be set.
type StringValueChange struct {
	Updated              bool                  `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                *wrappers.StringValue `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool                  `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             *wrappers.StringValue `protobuf:"bytes,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *StringValueChange) Reset()         { *m = StringValueChange{} }
func (m *StringValueChange) String() string { return proto.CompactTextString(m) }
func (*StringValueChange) ProtoMessage()    {}
func (*StringValueChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_change_bc3668f70f19c9dc, []int{7}
}
func (m *StringValueChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StringValueChange.Unmarshal(m, b)
}
func (m *StringValueChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StringValueChange.Marshal(b, m, deterministic)
}
func (dst *StringValueChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StringValueChange.Merge(dst, src)
}
func (m *StringValueChange) XXX_Size() int {
	return xxx_messageInfo_StringValueChange.Size(m)
}
func (m *StringValueChange) XXX_DiscardUnknown() {
	xxx_messageInfo_StringValueChange.DiscardUnknown(m)
}

var xxx_messageInfo_StringValueChange proto.InternalMessageInfo

func (m *StringValueChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *StringValueChange) GetValue() *wrappers.StringValue {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *StringValueChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *StringValueChange) GetOldValue() *wrappers.StringValue {
	if m != nil {
		return m.OldValue
	}
	return nil
}

func init() {
	proto.RegisterType((*Change)(nil), "eventbus.change.Change")
	proto.RegisterType((*StringChange)(nil), "eventbus.change.StringChange")
	proto.RegisterType((*Int64Change)(nil), "eventbus.change.Int64Change")
	proto.RegisterType((*BoolChange)(nil), "eventbus.change.BoolChange")
	proto.RegisterType((*StringArrayChange)(nil), "eventbus.change.StringArrayChange")
	proto.RegisterType((*TimestampChange)(nil), "eventbus.change.TimestampChange")
	proto.RegisterType((*Int64ValueChange)(nil), "eventbus.change.Int64ValueChange")
	proto.RegisterType((*StringValueChange)(nil), "eventbus.change.StringValueChange")
}

func init() {
	proto.RegisterFile("proto-lib/eventbus/change.proto", fileDescriptor_change_bc3668f70f19c9dc)
}

var fileDescriptor_change_bc3668f70f19c9dc = []byte{
	// 384 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x54, 0xc1, 0x4e, 0xc2, 0x40,
	0x10, 0x4d, 0xad, 0x62, 0x19, 0x34, 0x68, 0xe3, 0x81, 0x80, 0x11, 0xd2, 0x13, 0x17, 0xb7, 0x8a,
	0x46, 0x25, 0x9c, 0xc4, 0x93, 0x27, 0x13, 0x34, 0x1e, 0xbc, 0x90, 0x2d, 0x5d, 0xdb, 0x6a, 0xe9,
	0x36, 0xdd, 0x2d, 0xc6, 0x83, 0x7a, 0xf1, 0xab, 0x4c, 0x4c, 0xfc, 0x34, 0x93, 0x5d, 0x4b, 0x0b,
	0x4d, 0xc0, 0x86, 0xd3, 0x66, 0xf6, 0xcd, 0xbc, 0xf7, 0x66, 0x67, 0xb2, 0xd0, 0x0c, 0x23, 0xca,
	0xe9, 0xa1, 0xef, 0x59, 0x26, 0x99, 0x90, 0x80, 0x5b, 0x31, 0x33, 0x47, 0x2e, 0x0e, 0x1c, 0x82,
	0x04, 0xa2, 0x57, 0x93, 0x6b, 0x24, 0xaf, 0xeb, 0x4d, 0x87, 0x52, 0xc7, 0x27, 0xa6, 0x80, 0xad,
	0xf8, 0xd1, 0xe4, 0xde, 0x98, 0x30, 0x8e, 0xc7, 0xa1, 0xac, 0xa8, 0x1f, 0xcc, 0x27, 0xbc, 0x44,
	0x38, 0x0c, 0x49, 0xc4, 0x24, 0x6e, 0x18, 0x50, 0xba, 0x12, 0x54, 0x7a, 0x0d, 0x36, 0xe3, 0xd0,
	0xc6, 0x9c, 0xd8, 0x35, 0xa5, 0xa5, 0xb4, 0xb5, 0x41, 0x12, 0x1a, 0x1f, 0xb0, 0x75, 0xcb, 0x23,
	0x2f, 0x70, 0x96, 0x65, 0xea, 0x7b, 0xb0, 0x31, 0xc1, 0x7e, 0x4c, 0x6a, 0x6b, 0x2d, 0xa5, 0x5d,
	0x1e, 0xc8, 0x40, 0x37, 0x60, 0xdb, 0xc5, 0x6c, 0x48, 0x7d, 0x7b, 0x28, 0x51, 0x55, 0x54, 0x55,
	0x5c, 0xcc, 0x6e, 0x7c, 0xfb, 0x5e, 0xe4, 0x34, 0xa0, 0x9c, 0xe2, 0xeb, 0xa2, 0x5a, 0xa3, 0x7f,
	0xa0, 0xf1, 0x0e, 0x95, 0xeb, 0x80, 0x9f, 0x9d, 0x16, 0xd3, 0x57, 0x57, 0xd2, 0x57, 0x33, 0xfa,
	0x6f, 0x00, 0x7d, 0x4a, 0xfd, 0x62, 0xf2, 0xda, 0x4a, 0xf2, 0x5a, 0x46, 0xfe, 0x53, 0x81, 0x5d,
	0x39, 0x80, 0xcb, 0x28, 0xc2, 0xaf, 0x45, 0x6c, 0xa8, 0x2b, 0x4d, 0x41, 0x9d, 0x99, 0xc2, 0x97,
	0x02, 0xd5, 0xbb, 0x64, 0xbd, 0x96, 0x9a, 0x38, 0xca, 0xbe, 0x45, 0xa5, 0x53, 0x47, 0x72, 0x11,
	0x51, 0xb2, 0x88, 0x68, 0x4a, 0x55, 0xc4, 0xe0, 0xf9, 0xfc, 0x3b, 0x2d, 0x66, 0x4e, 0xcd, 0x7f,
	0x2b, 0xb0, 0x23, 0x76, 0x48, 0x84, 0x4b, 0xdd, 0x1f, 0xcf, 0xba, 0x6f, 0xe4, 0x34, 0x52, 0xae,
	0x22, 0xf6, 0x2f, 0xf2, 0xf6, 0x17, 0x52, 0xa7, 0xfe, 0x7f, 0xa6, 0x3b, 0xf0, 0xbf, 0x06, 0x3a,
	0xb3, 0x0d, 0xec, 0xe7, 0x54, 0x32, 0x64, 0x45, 0x3a, 0xe8, 0xe6, 0x3b, 0x58, 0xcc, 0x3d, 0x6d,
	0xa1, 0xdf, 0x7b, 0xe8, 0x8e, 0xa8, 0x4d, 0xd0, 0x53, 0xcc, 0xb8, 0x17, 0x20, 0x3e, 0x49, 0x3f,
	0x39, 0x36, 0x72, 0xc9, 0x18, 0x9b, 0xe1, 0xb3, 0x33, 0xff, 0xf1, 0xf5, 0xe4, 0x61, 0x95, 0x04,
	0xf9, 0xc9, 0x6f, 0x00, 0x00, 0x00, 0xff, 0xff, 0x1b, 0x5f, 0xcc, 0x67, 0x23, 0x05, 0x00, 0x00,
}
