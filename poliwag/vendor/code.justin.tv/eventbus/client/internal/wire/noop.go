package wire

import (
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

func DefaultEncodeNoop(environment string) ([]byte, error) {
	msgid, err := uuid.NewV4()
	if err != nil {
		return nil, errors.Wrap(err, "could not generate UUID")
	}
	return EncodeNoop(msgid, environment, time.Now())
}

// EncodeNoop gives a no-op payload.
func EncodeNoop(msgid uuid.UUID, environment string, when time.Time) ([]byte, error) {
	header, err := MakeHeader(msgid, "", environment, when)
	if err != nil {
		return nil, err
	}

	header.Type = MessageType_NOOP

	return buildMessage(header, nil)
}

func IsNoop(h *HeaderV1) bool {
	return h.EventType == "" && h.Type == MessageType_NOOP
}
