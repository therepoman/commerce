package encryption

import (
	"time"
)

const (
	DefaultCMKArn                = "arn:aws:kms:us-west-2:859517684765:key/48c60480-9d3b-4b66-84c8-82f88e75a5ca"
	DefaultDataKeyTTL            = 10 * time.Minute
	DefaultDataKeyTTLSplay       = 1 * time.Minute
	DefaultEncryptionKeyMaxUsage = 0
	DefaultMaxCacheSize          = 10000
)
