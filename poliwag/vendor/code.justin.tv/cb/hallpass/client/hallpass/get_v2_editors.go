package hallpass

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetV2Editors(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*view.V2GetEditorsResponse, error) {
	path := fmt.Sprintf("/v2/channels/%s/editors", channelID)
	response := &view.V2GetEditorsResponse{}

	mergedOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.cb-hallpass.v2.get_permissions",
		StatSampleRate: 0.1,
	})

	req, err := c.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	_, err = c.DoJSON(ctx, response, req, mergedOpts)
	if err != nil {
		return nil, err
	}

	return response, nil
}
