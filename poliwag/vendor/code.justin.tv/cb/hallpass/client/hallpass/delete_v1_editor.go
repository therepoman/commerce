package hallpass

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) DeleteV1Editor(ctx context.Context, channelID string, editorID string, reqOpts *twitchclient.ReqOpts) (*view.DeleteEditorResponse, error) {
	path := fmt.Sprintf("/v1/permissions/channels/%s/editors/%s", channelID, editorID)
	response := &view.DeleteEditorResponse{}

	mergedOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.cb-hallpass.v1.delete_permissions",
		StatSampleRate: 0.1,
	})

	req, err := c.NewRequest(http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	_, err = c.DoJSON(ctx, response, req, mergedOpts)
	if err != nil {
		return nil, err
	}

	return response, nil
}
