package resource

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

// BaseAWSCredentials returns base credentials if a profile is provided
func BaseAWSCredentials(profile string) *credentials.Credentials {
	return defaultAWSConfigurer.BaseAWSCredentials(profile)
}

// AWSCredentials returns credentials with an assume role chain
func AWSCredentials(creds *credentials.Credentials, roleArns ...string) *credentials.Credentials {
	return defaultAWSConfigurer.AWSCredentials(creds, roleArns...)
}

// AWSConfig returns a config with an assume role chain
func AWSConfig(creds *credentials.Credentials, roleArns ...string) *aws.Config {
	return defaultAWSConfigurer.AWSConfig(creds, roleArns...)
}

// STSSession returns a session client with an assume role chain
func STSSession(creds *credentials.Credentials, roleArns ...string) *session.Session {
	return defaultAWSConfigurer.STSSession(creds, roleArns...)
}

// GetAWSIdentity returns the identity after a role chain
func GetAWSIdentity(creds *credentials.Credentials, roleArns ...string) string {
	return defaultAWSConfigurer.GetAWSIdentity(creds, roleArns...)
}
