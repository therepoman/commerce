package jwt

import (
	"fmt"
)

//Err represents an annotated error from another package.
type Err struct {
	Err  error
	Desc string
}

func (j Err) Error() string { return fmt.Sprintf("jwt: %s %s", j.Err, j.Desc) }
