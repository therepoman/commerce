package paydayrpc

const (
	// UseBitsOnPoll
	ErrCodeInsufficientBitsBalance = "INSUFFICIENT_BITS_BALANCE"

	// UpdateCheermoteTier
	ErrCodeUserNotPermitted     = "USER_NOT_PERMITTED"
	ErrCodeInvalidImageUpload   = "INVALID_IMAGE_UPLOADED"
	ErrCodeImageAssetsNotFound  = "IMAGE_ASSETS_NOT_FOUND"
	ErrCodeTooManyImageAssets   = "TOO_MANY_IMAGE_ASSETS"
	ErrCodeMissingStaticAsset   = "MISSING_STATIC_ASSET"
	ErrCodeMissingAnimatedAsset = "MISSING_ANIMATED_ASSET"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
