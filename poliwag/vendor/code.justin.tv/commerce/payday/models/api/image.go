package api

import "time"

type Extension string
type AnimationType string
type Background string
type Tier string
type Scale string
type UploadType string

type UploadImageRequest struct {
	ChannelId     string        `json:"channel_id"`
	Tier          Tier          `json:"tier"`
	Background    Background    `json:"background"`
	AnimationType AnimationType `json:"animation_type"`
	Scale         Scale         `json:"scale"`
	ImageContent  []byte        `json:"image_content"`
	UploadType    UploadType    `json:"upload_type"`
}

type UploadCustomCheermoteRequest struct {
	ChannelId     string        `json:"channel_id"`
	Tier          Tier          `json:"tier"`
	Background    Background    `json:"background"`
	AnimationType AnimationType `json:"animation_type"`
	Scale         Scale         `json:"scale"`
	UploadType    UploadType    `json:"upload_type"`
}

type UploadImageResponse struct {
	URL string `json:"url"`
}

type UploadCustomCheermoteResponse struct {
	UploadURL string   `json:"upload_url"`
	UploadID  string   `json:"upload_id"`
	ImageURLs []string `json:"image_urls"`
}

type GetChannelImagesResponse struct {
	Images Images `json:"images"`
	Jobs   []Job  `json:"jobs"`
	Prefix string `json:"prefix"`
}

type GetCustomCheermotesResponse struct {
	Images Images `json:"images"`
	Prefix string `json:"prefix"`
}

type Job struct {
	ID                  string        `json:"id"`
	StartTime           time.Time     `json:"start_time"`
	Status              string        `json:"status"`
	SourceAssetType     string        `json:"source_asset_type"`
	SourceS3Key         string        `json:"source_s3_key"`
	SourceBackground    Background    `json:"source_background"`
	SourceAnimationType AnimationType `json:"source_animation_type"`
	SourceTier          Tier          `json:"source_tier"`
	SourceScale         Scale         `json:"source_scale"`
}

type Images struct {
	Tier1     ImageTier `json:"1"`
	Tier100   ImageTier `json:"100"`
	Tier1000  ImageTier `json:"1000"`
	Tier5000  ImageTier `json:"5000"`
	Tier10000 ImageTier `json:"10000"`
}

type ImageTier struct {
	Dark  ImageTierForBackground `json:"dark"`
	Light ImageTierForBackground `json:"light"`
}

type ImageTierForBackground struct {
	Static   ImageSet `json:"static"`
	Animated ImageSet `json:"animated"`
}

type ImageSet struct {
	Scale1  string `json:"1"`
	Scale15 string `json:"1.5"`
	Scale2  string `json:"2"`
	Scale3  string `json:"3"`
	Scale4  string `json:"4"`
}
