package api

type GetSupportedTagsResponse struct {
	Tags []TagElement `json:"tags"`
}

type TagElement struct {
	Cursor  string `json:"cursor"`
	Element Tag    `json:"element"`
}

type Tag struct {
	Name        string `json:"name" yaml:"name"`
	Description string `json:"description,omitempty" yaml:"description,omitempty"`
	ImageURL    string `json:"image_url,omitempty" yaml:"image_url,omitempty"`
}
