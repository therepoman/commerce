package api

import (
	"errors"
	"time"
)

const (
	ChannelCustomAction    = "channel_custom"
	GlobalFirstPartyAction = "global_first_party"
	GlobalThirdPartyAction = "global_third_party"
	SponsoredAction        = "sponsored"
	DisplayOnlyAction      = "display_only"
	DefaultAction          = "default"
	CharityAction          = "charity"
	AnonymousAction        = "anonymous"
)

type GetActionsResponse struct {
	Actions []Action `json:"actions"`
}

type GetActionsRequest struct {
	Prefixes          []string
	ChannelId         string
	UserId            string
	IncludeUpperTiers bool
	IncludeSponsored  bool
}

type GetActionAssetOptions struct {
	ChannelID  string        `json:"channel_id"`
	Prefix     string        `json:"prefix"`
	Background Background    `json:"background"`
	State      AnimationType `json:"state"`
	Scale      Scale         `json:"scale"`
	Bits       int64         `json:"bits"`
}

type GetActionAssetResponse struct {
	ImageURL string `json:"image_url"`
	Color    string `json:"color"`
	BitsTier int64  `json:"bits_tier"`
	CanCheer bool   `json:"can_cheer"`
}

type Action struct {
	Prefix       string          `json:"prefix"`
	Scales       []string        `json:"scales"`
	Tiers        []ActionTier    `json:"tiers"`
	Backgrounds  []string        `json:"backgrounds"`
	States       []string        `json:"states"`
	Type         string          `json:"type"`
	Order        int             `json:"order"`
	LastUpdated  time.Time       `json:"last_updated"`
	Campaign     *ActionCampaign `json:"campaign,omitempty"`
	IsCharitable bool            `json:"is_charitable"`
}

type ActionCampaign struct {
	ID                        string            `json:"id"`
	TotalBits                 int64             `json:"total_bits"`
	UsedBits                  int64             `json:"used_bits"`
	StartTime                 time.Time         `json:"start_time"`
	EndTime                   time.Time         `json:"end_time"`
	IsEnabled                 bool              `json:"is_enabled"`
	MinBitsToBeSponsored      int64             `json:"min_bits_to_be_sponsored"`
	SponsoredAmountThresholds map[int64]float64 `json:"sponsored_amount_thresholds"`
	UserLimit                 int64             `json:"user_limit"`
	UserUsedBits              int64             `json:"user_used_bits"`
	BrandName                 string            `json:"brand_name"`
	BrandImageURL             string            `json:"brand_image_url"`
}

type TierImages map[string]map[string]map[string]string

func (m TierImages) Put(background string, state string, scale string, imageUrl string) {
	_, stateMapPresent := m[background]
	if !stateMapPresent {
		m[background] = make(map[string]map[string]string)
	}

	_, scaleMapPresent := m[background][state]
	if !scaleMapPresent {
		m[background][state] = make(map[string]string)
	}

	m[background][state][scale] = imageUrl
}

func (m TierImages) Get(background Background, state AnimationType, scale Scale) (string, error) {
	backgroundMap, backgroundPresent := m[string(background)]
	if !backgroundPresent {
		return "", errors.New("background does not exist in image map")
	}

	stateMap, statePresent := backgroundMap[string(state)]
	if !statePresent {
		return "", errors.New("state does not exist in image map")
	}

	imageUrl, scalePresent := stateMap[string(scale)]
	if !scalePresent {
		return "", errors.New("scale does not exist in image map")
	}

	return imageUrl, nil
}

type ActionTier struct {
	BitsMinimum    int64       `json:"min_bits"`
	Id             string      `json:"id"`
	Color          string      `json:"color"`
	Images         *TierImages `json:"images,omitempty"`
	CanCheer       bool        `json:"can_cheer"`
	ShowInBitsCard bool        `json:"show_in_bits_card"`
}

type GetActionsInfoResponse struct {
	Prefixes    []string `json:"prefixes"`
	Backgrounds []string `json:"backgrounds"`
	States      []string `json:"states"`
	Scales      []string `json:"scales"`
	Tiers       []string `json:"tiers"`
}
