package splatter

import (
	"fmt"
	"math"
	"time"

	"code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchTelemetry"
	cw "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"
	mws "code.justin.tv/amzn/TwitchTelemetryMWSMetricsSender"
	"github.com/cactus/go-statsd-client/statsd"
)

// BufferedTelemetryConfig configuration for TwitchTelemetry statter implementations
type BufferedTelemetryConfig struct {
	FlushPeriod       time.Duration // How often to send metrics to the unbuffered client
	BufferSize        int           // Buffer size for the channel
	AggregationPeriod time.Duration // Bucket for metrics, generally 1 Minute intervals
	ServiceName       string        // Name of the service (e.g. "fortuna")
	AWSRegion         string        // AWS region (e.g. "us-west-2")
	Stage             string        // Main service stage (e.g. "production", "development", etc.)
	Substage          string        // A further breakdown of the service stage (e.g. "primary", "canary", etc.)
	Prefix            string        // A prefix to add to all statsd strings
}

type TwitchTelemetryStatter struct {
	SampleReporter          telemetry.SampleReporter
	BufferedTelemetryConfig *BufferedTelemetryConfig
	FilteredStats           map[string]bool
}

// NewBufferedTelemetryCloudWatchStatter returns a new TwitchTelemetry CloudWatch Statter implementation
func NewBufferedTelemetryCloudWatchStatter(config *BufferedTelemetryConfig, filteredStats map[string]bool) statsd.Statter {
	tPid := constructTPid(config)
	unbufferedCW := cw.NewUnbuffered(&tPid, nil)
	return constructTwitchTelemetryStatter(config, tPid, filteredStats, unbufferedCW)
}

// NewBufferedTelemetryMWSStatter returns a new TwitchTelemetry MWS (Internal Amazon metric) Statter implementation
func NewBufferedTelemetryMWSStatter(config *BufferedTelemetryConfig, filteredStats map[string]bool) statsd.Statter {
	tPid := constructTPid(config)
	unbufferedMWS := mws.NewUnbuffered(&tPid, nil)
	return constructTwitchTelemetryStatter(config, tPid, filteredStats, unbufferedMWS)
}

// Inc reports a positive count metric
func (s *TwitchTelemetryStatter) Inc(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	s.SampleReporter.Report(joinPathComponents(s.BufferedTelemetryConfig.Prefix, stat), float64(value), telemetry.UnitCount)
	return nil
}

// Dec reports a negative count metric by sending the absolute value of the metric with a suffix of '.negative'
func (s *TwitchTelemetryStatter) Dec(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	prefixedMetric := joinPathComponents(s.BufferedTelemetryConfig.Prefix, stat)
	negativeNamed := joinPathComponents(prefixedMetric, "negative")
	s.SampleReporter.Report(negativeNamed, math.Abs(float64(value)), telemetry.UnitCount)
	return nil
}

// Gauge reports a positive metric with no units ('None')
func (s *TwitchTelemetryStatter) Gauge(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	s.SampleReporter.Report(joinPathComponents(s.BufferedTelemetryConfig.Prefix, stat), float64(value), "None")
	return nil
}

// GaugeDelta submits a positive delta for an existing metric, not supported by TwitchTelemetry
func (s *TwitchTelemetryStatter) GaugeDelta(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return fmt.Errorf("TwitchTelemetryStatter::GaugeDelta is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// Timing reports a delta int64 that is converted to a time.Millisecond duration and reported
func (s *TwitchTelemetryStatter) Timing(stat string, delta int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	deltaTime := time.Duration(delta * int64(time.Millisecond))
	s.SampleReporter.ReportDurationSample(joinPathComponents(s.BufferedTelemetryConfig.Prefix, stat), deltaTime)
	return nil
}

// TimingDuration reports a time.Duration that is reported
func (s *TwitchTelemetryStatter) TimingDuration(stat string, delta time.Duration, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	s.SampleReporter.ReportDurationSample(joinPathComponents(s.BufferedTelemetryConfig.Prefix, stat), delta)
	return nil
}

// Set submits a set type with value as a string, not supported by TwitchTelemetry
func (s *TwitchTelemetryStatter) Set(stat string, value string, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return fmt.Errorf("TwitchTelemetryStatter::Set is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// SetInt submits a set type with value as an int64, not supported by TwitchTelemetry
func (s *TwitchTelemetryStatter) SetInt(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return fmt.Errorf("TwitchTelemetryStatter::SetInt is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// Raw submits a pre-formatted string value, not supported by TwitchTelemetry
func (s *TwitchTelemetryStatter) Raw(stat string, value string, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return fmt.Errorf("TwitchTelemetryStatter::Raw is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// SetSamplerFunc changes the sampling rate. Not required for TwitchTelemetry
func (s *TwitchTelemetryStatter) SetSamplerFunc(sampler statsd.SamplerFunc) {}

// SetPrefix allows the config's prefix to be changed
func (s *TwitchTelemetryStatter) SetPrefix(prefix string) {
	s.BufferedTelemetryConfig.Prefix = prefix
}

// NewSubStatter constructs a new statter that has an additional prefix appended to the end of the existing config prefix
func (s *TwitchTelemetryStatter) NewSubStatter(prefix string) statsd.SubStatter {
	var c *TwitchTelemetryStatter
	if s != nil {
		c = &TwitchTelemetryStatter{
			SampleReporter: s.SampleReporter,
			BufferedTelemetryConfig: &BufferedTelemetryConfig{
				FlushPeriod:       s.BufferedTelemetryConfig.FlushPeriod,
				BufferSize:        s.BufferedTelemetryConfig.BufferSize,
				AggregationPeriod: s.BufferedTelemetryConfig.AggregationPeriod,
				ServiceName:       s.BufferedTelemetryConfig.ServiceName,
				AWSRegion:         s.BufferedTelemetryConfig.AWSRegion,
				Stage:             s.BufferedTelemetryConfig.Stage,
				Substage:          s.BufferedTelemetryConfig.Substage,
				Prefix:            s.BufferedTelemetryConfig.Prefix,
			},
			FilteredStats: s.FilteredStats,
		}
		c.BufferedTelemetryConfig.Prefix = joinPathComponents(c.BufferedTelemetryConfig.Prefix, prefix)
	}
	return c
}

// Close ends sample reporting and flushes all metrics one final time
func (s *TwitchTelemetryStatter) Close() error {
	s.SampleReporter.Stop()
	return nil
}

// filter represents a helper function that determine whether a given stat should be filtered or not (dropped without
// being reported)
func (s *TwitchTelemetryStatter) filter(stat string) bool {
	if val, ok := s.FilteredStats[stat]; ok {
		return val
	} else {
		return false
	}
}

// constructTPid represents a helper function to construct a Twitch ProcessIdentifier from a given config
func constructTPid(config *BufferedTelemetryConfig) identifier.ProcessIdentifier {
	return identifier.ProcessIdentifier{
		Service:  config.ServiceName,
		Stage:    config.Stage,
		Substage: config.Substage,
		Region:   config.AWSRegion,
	}
}

// constructTwitchTelemetryStatter represents a helper function to construct a buffered TwitchTelemetryStatter shim for
// a given unbuffered sample observer
func constructTwitchTelemetryStatter(config *BufferedTelemetryConfig, tPid identifier.ProcessIdentifier, filteredStats map[string]bool, sampleObserver telemetry.SampleUnbufferedObserver) statsd.Statter {
	bufferedReporter := telemetry.NewBufferedAggregator(config.FlushPeriod, config.BufferSize, config.AggregationPeriod, sampleObserver, nil)
	sampleReporter := telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: tPid},
		SampleObserver: bufferedReporter,
	}

	return &TwitchTelemetryStatter{
		SampleReporter:          sampleReporter,
		BufferedTelemetryConfig: config,
		FilteredStats:           filteredStats,
	}
}
