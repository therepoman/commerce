# splatter
Assorted implementations for the statsd.Statter interface

## Why splatter?
1. "All of these twitch libraries are expecting a StatsD integration for metrics, but I want to send my metrics somewhere else."
  * `Solution`: Use the TwitchTelemetryStatter to send them to Cloudwatch or MWS! Add your own Statter if you want to send them somewhere else.
2. "I want to send my metrics to both StatsD and CloudWatch so I backfill CloudWatch and have time to migrate my alarms and dashboards."
  * `Solution`: Use the CompositeStatter to send your metrics to multiple destinations!
3. "I am trying to setup metrics in my service but something isn't working."
  * `Solution`: Use the LogStatter to emit stats to logs to aid with debugging!

## Development
Dependencies - uses `dep`

    $ dep ensure

Testing

    $ go test

Run example

    $ go run cmd/example/main.go

## Contents
* New implementations for the `statsd.Statter` interface:
  * `TwitchTelemetryStatter` - Sends metrics to CloudWatch or MWS (internal Amazon metrics backed by CloudWatch) via the TwitchTelemetry libraries
  	* `NewBufferedTelemetryCloudWatchStatter` sends to CloudWatch
  	* `NewBufferedTelemetryMWSStatter` sends to MWS
  * `LogStatter` - Sends metrics to log output.
  * `CompositeStatter` - Sends metrics to all injected Statters. Can be used to send metrics to multiple destinations.
* `NewStatsdServerHooks` - Wrap a Statter to use with Twirp server hooks

## Usage
See `cmd/example/main.go` for full example usage of this library.

```go
	// Create an optional prefix for all metric names (stat strings)
	statsPrefix = "somePrefix"

	// Create a config for TwitchTelemetry Buffered Senders
	buffConfig := &BufferedTelemetryConfig {
		FlushPeriod: 30 * time.Second,  // How often to send metrics
		BufferSize: 100000,             // Size of the buffer
		AggregationPeriod: time.Minute, // Aggregation period of metrics, e.g. 1 minute metrics
		ServiceName:"TwitchMyService",  // Name of your service
		AWSRegion: "us-west-2",         // AWS Region of your service
		Stage: "production",            // Main stage of your service (e.g. production, development, etc.)
		Substage: "canary",             // A substage for the stage (e.g. primary, canary, etc.)
		Prefix: statsPrefix,            // Optional prefix for all metrics
	}

	// Create a Statter shim for CloudWatch using the given config and an optional map of metrics to ignore
	// If metrics are in the map as "someMetric" -> true, then "someMetric" will not be reported
	telemetryStatter :=  NewBufferedTelemetryCloudWatchStatter(buffConfig, map[string]bool{})

	// Create a standard statsd Statter to send metrics to the statsd endpoint
	statsdStatter, err := statsd.NewClient("statsd.internal.justin.tv:8125", statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to connect to statsd endpoint. Make sure you are on JTV-SFO network if running locally.")
	}

	// Create a splatter.LogStatter to send metrics to logs for debugging
	logStatter, err := splatter.NewLogStatter(statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Log Statter")
	}

	// Build a slice of statters that you want to inject into a splatter.CompositeStatter.
	statters := []statsd.Statter{telemetryStatter, statsdStatter, logStatter}

	// Create a splatter.CompositeStatter and inject all of the Statters into it! All of the individual Statters will be called.
	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Composite Statter")
	}
	
	// Optionally use a TwitchTelemetryStatter with Twirp server hooks
	hooks := twirp.ChainHooks(splatter.NewStatsdServerHooks(telemetryStatter))
	// Wire up hooks to your Twirp service as you normally do
```

And to close a Statter...
```go
	// Ideally in a server shutdown function
	defer stats.Close() // This will close all of the child Statters
```

### TwitchTelemetry Notes
Please note that metrics will appear in CloudWatch/ MWS as-is (the metric name will be the statsd string with the optional
provided prefix). The one exception is `Dec`, which will append `.negative` to the metric name. This can then be used
with metric math alarms/ dashboards to determine the appropriate end value.

Metrics will additionally have dimensions created from the provided BufferedTelemetryConfig information (Region, Service, Stage,
and Substage). For more information, please see [the following wiki](https://wiki.twitch.com/display/PF/TwitchTelemetry+and+CloudWatch+Metrics+Cost).
