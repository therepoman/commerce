#!/bin/bash

set -euo pipefail

echo "Testing go1.10"
docker build -f Dockerfile_gopath --build-arg GOVERSION=1.10 .

echo "Testing go1.11"
docker build -f Dockerfile_gopath --build-arg GOVERSION=1.11 .

echo "Testing go1.12 on gopath"
docker build -f Dockerfile_gopath --build-arg GOVERSION=1.12 .

echo "Testing go1.12 with modules"
docker build -f Dockerfile_modules --build-arg GOVERSION=1.12 .
