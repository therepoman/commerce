module code.justin.tv/video/cloudwatchlogger

go 1.15

require (
	code.justin.tv/video/invoker v0.7.1
	github.com/aws/aws-sdk-go v1.35.18
	github.com/sirupsen/logrus v1.7.0
	go.uber.org/zap v1.16.0
)
