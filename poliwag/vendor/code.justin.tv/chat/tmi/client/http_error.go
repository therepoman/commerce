package tmi

import "fmt"

// HTTPError embeds an error, and holds a status code from a service we got an unsuccessful HTTP status from.
type HTTPError interface {
	error
	// StatusCode returns the status code that was stored inside this error.
	StatusCode() int
}

type httpErrorImpl struct {
	err        error
	statusCode int
}

// NewHTTPError returns a new HTTPError with the given status code.
func NewHTTPError(err error, statusCode int) HTTPError {
	return httpErrorImpl{
		err:        err,
		statusCode: statusCode,
	}
}

// StatusCode returns the status code that was stored inside this error on creation.
func (httpErr httpErrorImpl) StatusCode() int {
	return httpErr.statusCode
}

func (httpErr httpErrorImpl) Error() string {
	if httpErr.err != nil {
		return httpErr.err.Error()
	}
	return fmt.Sprintf("unexpected status code %v", httpErr.statusCode)
}
