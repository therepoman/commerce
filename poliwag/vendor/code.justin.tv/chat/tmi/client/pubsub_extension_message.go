package tmi

import (
	"time"
)

type ExtensionMessagePayload struct {
	ID      string                     `json:"id"`
	SentAt  time.Time                  `json:"sent_at"`
	Content JSONMessageContent         `json:"content"`
	Sender  JSONExtensionMessageSender `json:"sender"`
}

type JSONExtensionMessageSender struct {
	ExtensionClientID string          `json:"extension_client_id"`
	ExtensionVersion  string          `json:"extension_version"`
	DisplayName       string          `json:"display_name"`
	ChatColor         string          `json:"chat_color"`
	Badges            []JSONUserBadge `json:"badges"`
}
