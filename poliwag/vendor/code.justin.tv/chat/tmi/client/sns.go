package tmi

import "time"

const (
	// ChatBanEventName is published to SNS when a user receives a permanent or
	// temporary ban from a channel.
	ChatBanEventName = "chat_ban"
	// ChatUnbanEventName is published to SNS when a user's permanent or
	// temporary ban from a channel is removed.
	ChatUnbanEventName = "chat_unban"
	// HostingInitiatedEventName is published to SNS when a channel begins hosting another channel.
	HostingInitiatedEventName = "hosting_initiated"
	//AutomodApproveEventName is published to SNS when a moderator approves an automod withheld message.
	AutomodApproveEventName = "automod_approve"
	//AutomodDenyEventName is published to SNS when a moderator denies an automod withheld message.
	AutomodDenyEventName = "automod_deny"
	//DeleteUserMessageEventName is published to SNS when a moderator deletes a users room message.
	DeleteUserMessageEventName = "delete_user_message"
)

// ChatBanEventPayload stores data about a chat ban.
type ChatBanEventPayload struct {
	ChannelID   string     `json:"channel_id"`
	TargetID    string     `json:"target_id"`
	RequesterID string     `json:"requester_id"`
	BannedAt    time.Time  `json:"banned_at"`
	ExpiresAt   *time.Time `json:"expires_at,omitempty"`
	Reason      string     `json:"reason"`
}

// ChatUnbanEventPayload stores data about a chat unban.
type ChatUnbanEventPayload struct {
	ChannelID   string    `json:"channel_id"`
	TargetID    string    `json:"target_id"`
	RequesterID string    `json:"requester_id"`
	UnbannedAt  time.Time `json:"unbanned_at"`
	Untimeout   bool      `json:"untimeout"`
}

// HostingInitiatedEventPayload stores data about the initiation of a hosting session.
type HostingInitiatedEventPayload struct {
	AutoHosted          bool      `json:"auto_hosted"`
	HostedAfterRaiding  bool      `json:"hosted_after_raiding"`
	HostChannelID       string    `json:"host_channel_id"`
	TargetChannelID     string    `json:"target_channel_id"`
	Timestamp           time.Time `json:"timestamp"`
	ViewerCount         int       `json:"viewer_count"`
	ViewerCountRevealed bool      `json:"viewer_count_revealed"`
}

// AutomodApproveEventPayload stores data about an automod approval.
type AutomodApproveEventPayload struct {
	MsgID         string    `json:"msg_id"`
	ApprovedMsgID string    `json:"approved_msg_id"`
	RequesterID   string    `json:"requester_id"`
	ChannelID     string    `json:"channel_id"`
	ApprovedAt    time.Time `json:"approved_at"`
}

// AutomodDenyEventPayload stores data about an automod denial.
type AutomodDenyEventPayload struct {
	MsgID       string    `json:"msg_id"`
	RequesterID string    `json:"requester_id"`
	ChannelID   string    `json:"channel_id"`
	DeniedAt    time.Time `json:"denied_at"`
}

// DeleteUserMessageEventPayload stores data about a delete room message.
type DeleteUserMessageEventPayload struct {
	MsgID       string    `json:"msg_id"`
	RequesterID string    `json:"requester_id"`
	ChannelID   string    `json:"channel_id"`
	DeletedAt   time.Time `json:"deleted_at"`
}
