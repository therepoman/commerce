module code.justin.tv/chat/telemetryext

go 1.13

require (
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f // indirect
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191113205906-40e3e1aa55c5
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1
	github.com/aws/aws-sdk-go v1.31.2 // indirect
	github.com/golangci/golangci-lint v1.25.1
	github.com/mgechev/revive v1.0.2
	github.com/stretchr/testify v1.5.1
)
