# telemetryext

TwitchTelemetry helpers. This library contains small helpers for common TwitchTelemetry uses cases

## ReporterWithDimensions

Creates a new SampleReporter with attached dimensions. All credit for this function goes to [TwitchTelemetryCircuitMetrics](https://code.amazon.com/packages/TwitchTelemetryCircuitMetrics/blobs/251995a73be1f04e0fe2acf9a0810214b365963d/--/circuitmetrics/metrics.go#L55)

Example
```go
reporter := telemetryext.ReporterWithDimensions(sampleReporter, map[string]string{
  "MyDimensionKey": "MyDimensionValue",
})

reporter.Report("MyCounter", 1, "Count")
```

## SampleReporterFactory

A factory to create a new SampleReporter that reports to CloudWatch with reasonable defaults

Example

```go
var factory telemetryext.SampleReporterFactory
reporter := factory.Create(identifier.ProcessIdentifier{
  Service:  "MyServiceName",
  Stage:    "staging",
  Substage: "primary",
  Region:   "us-west-2",
})
```


# Development Guidelines

Add new functionality with minimal dependencies to keep this library lightweight
