package telemetryext

import (
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	telemetrycloudwatch "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"
)

// ReporterWithDimensions creates a reporter with additional dimensions. Dimensions in dims take precedent
func ReporterWithDimensions(base telemetry.SampleReporter, dims map[string]string) telemetry.SampleReporter {
	allDims := make(map[string]string, len(base.Dimensions)+len(dims))

	for k, v := range base.Dimensions {
		allDims[k] = v
	}

	for k, v := range dims {
		allDims[k] = v
	}

	base.Dimensions = allDims

	return base
}

const (
	defaultFlushPeriod       = 30 * time.Second
	defaultBufferSize        = 100000
	defaultAggregationPeriod = 60 * time.Second
)

type logger interface {
	// Log is the general logging method.
	Log(msg string, keyvals ...interface{})
}

// SampleReporterFactory creates sample reporters with reasonable defaults
type SampleReporterFactory struct {
	// FlushPeriod sets how frequent to flush metrics. Defaults to 30s
	FlushPeriod time.Duration
	// BufferSize sets how many metrics can be buffered before being dropped. Defaults to 100000
	BufferSize int
	// AggregationPeriod sets the time resolution for metrics. Defaults to 1 minute
	AggregationPeriod time.Duration
	// Logger is an optional logger. Defaults to nil
	Logger logger
}

// Create a cloudwatch sample reporter
func (s *SampleReporterFactory) Create(tpid identifier.ProcessIdentifier) telemetry.SampleReporter {
	// Create the low-level cloudwatch sender
	cloudwatchSender := telemetrycloudwatch.NewUnbuffered(&tpid, s.Logger)

	// Create the metrics aggregator that flushes and sends CloudWatch metrics
	sampleObserver := telemetry.NewBufferedAggregator(s.flushPeriod(), s.bufferSize(), s.aggregationPeriod(), cloudwatchSender, s.Logger)

	// Create the reporter. This is what the application will use to send metrics
	sampleReporter := telemetry.SampleReporter{
		SampleBuilder: telemetry.SampleBuilder{
			ProcessIdentifier: tpid,
		},
		SampleObserver: sampleObserver,
		Logger:         s.Logger,
	}

	return sampleReporter
}

func (s *SampleReporterFactory) flushPeriod() time.Duration {
	if s.FlushPeriod <= 0 {
		return defaultFlushPeriod
	}

	return s.FlushPeriod
}

func (s *SampleReporterFactory) bufferSize() int {
	if s.BufferSize <= 0 {
		return defaultBufferSize
	}

	return s.BufferSize
}

func (s *SampleReporterFactory) aggregationPeriod() time.Duration {
	if s.AggregationPeriod <= 0 {
		return defaultAggregationPeriod
	}

	return s.AggregationPeriod
}
