package models

const (
	BadgeSetStaff            = "staff"
	BadgeSetAdmin            = "admin"
	BadgeSetGlobalMod        = "global_mod"
	BadgeSetBroadcaster      = "broadcaster"
	BadgeSetModerator        = "moderator"
	BadgeSetVIP              = "vip"
	BadgeSetTwitchBot        = "twitchbot"
	BadgeSetSubscriber       = "subscriber"
	BadgeSetSubGifter        = "sub-gifter"
	BadgeSetBits             = "bits"
	BadgeSetBitsLeader       = "bits-leader"
	BadgeSetExtension        = "extension"
	BadgeSetPartner          = "partner"
	BadgeSetPrime            = "premium"
	BadgeSetTurbo            = "turbo"
	BadgeSetAnonymousCheerer = "anonymous-cheerer"

	DefaultSubBadgeVersion = "0"
)
