package models

import "time"

// BadgeSet maps badge set versions to display info
type BadgeSet struct {
	Versions map[string]BadgeVersion `json:"versions"`
}

// BadgeVersion contains display info for a single badge set version
type BadgeVersion struct {
	ImageURL1x  *string    `json:"image_url_1x"`
	ImageURL2x  *string    `json:"image_url_2x"`
	ImageURL4x  *string    `json:"image_url_4x"`
	Description string     `json:"description"`
	Title       string     `json:"title"`
	ClickAction string     `json:"click_action"`
	ClickURL    string     `json:"click_url"`
	LastUpdated *time.Time `json:"last_updated"`
}

// Badge is defined by just a badge id and version and may include additional optional data
type Badge struct {
	BadgeSetID      string `json:"badge_set_id"`
	BadgeSetVersion string `json:"badge_set_version"`
	DynamicData     string `json:"dynamic_data"`
}

// BadgeID is a (setID, version, userID) triplet which uniquely identifies a badge.
type BadgeID struct {
	// SetID is the indentifier of the set this badge belongs to.
	// Many badges share the same SetID.
	SetID string `json:"set_id"`
	// Version further identifies a given badge within a badge set.
	Version string `json:"version"`
	// UserID identifies the broadcaster
	// The badge belongs to the global scope if UserID is not present or empty-string.
	UserID string `json:"user_id,omitempty"`
}

// DisplayInfo ...
type DisplayInfo struct {
	ID   BadgeID
	Data BadgeVersion
}
