package twitchclienthttptelemetry

import (
	"bytes"
	"net/http"
	"strings"
	"unicode"
	"unicode/utf8"

	"code.justin.tv/chat/httptelemetry"
	"code.justin.tv/foundation/twitchclient"
)

const servicePrefix = "service."

// AttachServiceAndOperation retrieves twitchclient ReqOpts from the request, parses the StatName
// for the service name and operation name, and then attaches the names to the request context.
func AttachServiceAndOperation(req *http.Request) *http.Request {
	ctx := req.Context()
	reqOpts := twitchclient.GetReqOpts(ctx)

	serviceName, operationName := serviceAndOperationFromStat(reqOpts.StatName)
	if serviceName != "" {
		ctx = httptelemetry.WithServiceName(ctx, serviceName)
	}
	if operationName != "" {
		ctx = httptelemetry.WithOperationName(ctx, operationName)
	}

	return req.WithContext(ctx)
}

// serviceAndOperationFromStat parses the statName for the service name and operation name if the
// pattern matches "service.{service_name}.{operation_name}"
func serviceAndOperationFromStat(statName string) (string, string) {
	if !strings.HasPrefix(statName, servicePrefix) {
		return "", ""
	}

	rest := statName[len(servicePrefix):]
	if len(rest) == 0 {
		return "", ""
	}

	runeOne, _ := utf8.DecodeRuneInString(rest)
	if !unicode.In(runeOne, unicode.Letter) {
		return "", ""
	}

	periodIndex := strings.Index(rest, ".")
	if periodIndex == -1 {
		return sanitizeAndPascalCase(rest), ""
	}

	serviceName := sanitizeAndPascalCase(rest[:periodIndex])

	var operationName string
	if periodIndex+1 < len(rest) {
		operationName = sanitizeAndPascalCase(rest[periodIndex+1:])
	}

	return serviceName, operationName
}

func sanitizeAndPascalCase(s string) string {
	if len(s) == 0 {
		return ""
	}

	toUpperNextASCII := true
	b := make([]byte, 0, len(s))
	buf := bytes.NewBuffer(b)
	for _, r := range s {
		switch {
		case unicode.In(r, unicode.Letter, unicode.Number):
			if toUpperNextASCII {
				buf.WriteRune(unicode.ToUpper(r))
				toUpperNextASCII = false
			} else {
				buf.WriteRune(r)
			}
		case unicode.In(r, unicode.Hyphen):
			toUpperNextASCII = true
			buf.WriteRune(r)
		default:
			toUpperNextASCII = true
			buf.WriteByte('_')
		}
	}

	return buf.String()
}
