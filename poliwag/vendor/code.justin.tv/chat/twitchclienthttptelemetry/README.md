# twitchclienthttptelemetry

foundation/twitchclient integration for for httptelemetry

## Usage

Add httptelemetry.RoundTripper to the list of RoundTripperWrappers. Use twitchclienthttptelemetry.AttachServiceAndOperation as the PreRoundTrip

The service name and operation name are parsed from the twitchclient.ReqOpts attached to the request context. twitchclients that follow the StatName pattern of
`"service.{service_name}.{operation_name}"` are parseable. If parsed, the service name and operation name are pascal cased

Audited twitchclients that follow the above StatName pattern:

* web/users-service
* chat/zuma
* chat/badges
* commerce/payday
* chat/tmi
* chat/pubsub-go-pubclient
* ads/saul
* cb/hallpass
* chat/whispers
* live/autohost
* safety/userrep
* video/clips-upload
* web/owl

## Example

[Full example](./example/main.go)

Specifying the round trip wrapper on a service's twitchclient

   ```go
	client, err := servicePackage.NewClient(twitchclient.ClientConf{
		Host: "{hostname}",
		RoundTripperWrappers: []func(rt http.RoundTripper) http.RoundTripper{
			func(rt http.RoundTripper) http.RoundTripper {
				return &httptelemetry.RoundTripper{
					SampleReporter: &sampleReporter,
					RoundTripper:   rt,
					PreRoundTrip:   twitchclienthttptelemetry.AttachServiceAndOperation,
				}
			},
		},
	})
   ```
