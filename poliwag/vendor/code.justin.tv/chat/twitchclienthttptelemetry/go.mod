module code.justin.tv/chat/twitchclienthttptelemetry

go 1.14

require (
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191113205906-40e3e1aa55c5
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1
	code.justin.tv/chat/httptelemetry v1.0.0
	code.justin.tv/chat/timing v1.0.1 // indirect
	code.justin.tv/common/golibs v1.0.7 // indirect
	code.justin.tv/foundation/twitchclient v4.11.0+incompatible
	github.com/golangci/golangci-lint v1.27.0
	github.com/mgechev/revive v1.0.2
	github.com/stretchr/testify v1.6.0
	golang.org/x/net v0.0.0-20200528225125-3c3fba18258b // indirect
)
