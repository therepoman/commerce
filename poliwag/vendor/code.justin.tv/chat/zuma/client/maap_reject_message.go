package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) RejectMessage(ctx context.Context, params api.RejectMessageRequest, reqOpts *twitchclient.ReqOpts) (api.RejectMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.RejectMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/reject", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.RejectMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.reject_message",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.RejectMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.RejectMessageResponse{}, err
	}
	return decoded, nil
}
