package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ApproveMessage(ctx context.Context, params api.ApproveMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ApproveMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ApproveMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/approve", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ApproveMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.approve_message",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ApproveMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ApproveMessageResponse{}, err
	}
	return decoded, nil
}
