package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetContainerView(ctx context.Context, params api.GetContainerViewRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerViewResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetContainerViewResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/container_views/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetContainerViewResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.get_container_view",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GetContainerViewResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GetContainerViewResponse{}, err
	}
	return decoded, nil
}
