package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetContainer(ctx context.Context, params api.GetContainerRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetContainerResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/containers/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetContainerResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.get_container",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GetContainerResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GetContainerResponse{}, err
	}
	return decoded, nil
}
