package api

const (
	SNSCommunityModerationActionEventName = "community_moderation_action"
	SNSCommunityUserReportEventName       = "user_report"
	SNSBlockUserEventName                 = "block_user"
	SNSUnblockUserEventName               = "unblock_user"
	SNSAddChannelModeratorEventName       = "add_channel_moderator"
	SNSRemoveChannelModeratorEventName    = "remove_channel_moderator"
	SNSSetChannelCommunitiesEventName     = "set_channel_communities"
	SNSAddChannelVIPEventName             = "add_channel_vip"
	SNSRemoveChannelVIPEventName          = "remove_channel_vip"
)

// CommunityModActionSNSEvent is published when a mod action is taken in a community
type CommunityModActionSNSEvent struct {
	CommunityID   string `json:"community_id"`
	ModUserID     string `json:"mod_user_id"`
	TargetUserID  string `json:"target_user_id"`
	Action        string `json:"action"`
	Reason        string `json:"reason"`
	UnixTimestamp int64  `json:"unix_timestamp"`
}

const (
	ModActionTypeBan       = "ban"
	ModActionTypeUnban     = "unban"
	ModActionTypeTimeout   = "timeout"
	ModActionTypeUntimeout = "untimeout"
)

// CommunityUserReportSNSEvent is published when a user reports a channel in a community
type CommunityUserReportSNSEvent struct {
	CommunityID     string `json:"community_id"`
	ReportingUserID string `json:"reporting_user_id"`
	ChannelID       string `json:"channel_id"`
	Description     string `json:"description"`
	UnixTimestamp   int64  `json:"unix_timestamp"`
}

// BlockUserSNSEvent is published when a user is blocked
type BlockUserSNSEvent struct {
	UserID        string `json:"user_id"`
	TargetUserID  string `json:"target_user_id"`
	Reason        string `json:"reason"`
	SourceContext string `json:"source_context"`
}

// UnblockUserSNSEvent is published when a user is unblocked
type UnblockUserSNSEvent struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
}

// AddChannelModeratorSNSEvent is published when a user is added as a channel moderator
type AddChannelModeratorSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveChannelModeratorSNSEvent is published when a user is removed as a channel moderator
type RemoveChannelModeratorSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// SetChannelCommunitiesSNSEvent is published when a channel sets its communities
type SetChannelCommunitiesSNSEvent struct {
	ChannelID    string   `json:"channel_id"`
	CommunityIDs []string `json:"community_ids"`
}

// AddChannelVIPSNSEvent is published when a user is added as a channel VIP
type AddChannelVIPSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveChannelVIPSNSEvent is published when a user is removed as a channel VIP
type RemoveChannelVIPSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}
