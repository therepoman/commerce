# awshttptelemetry

aws-sdk-go integration for for httptelemetry

## Usage

1. Create a AWS session with a custom http client using the httptelemetry.RoundTripper
1. Call awshttptelemetry.AttachServiceAndOperation on the session object
1. Construct AWS client with the session

Unknown operations may be reported if IAM role credentials are used. An example request URL is `http://169.254.170.2/v2/credentials/324d61b8-31a8-416d-95e5-b5efabdaed65`

# Example

[Full example](./example/main.go)

Creating AWS session, attaching handler, and constructing a DynamoDB client

   ```go
	sess := session.Must(session.NewSession(&aws.Config{
		HTTPClient: &http.Client{
			Transport: &httptelemetry.RoundTripper{
				SampleReporter: &sampleReporter,
				RoundTripper:   existingTransport,
			},
		},
	}))
	sess = awshttptelemetry.AttachServiceAndOperation(sess)
	dynamoDBClient := dynamodb.New(sess)
	// Calls to dynamoDBClient will automatically report metrics
   ```
