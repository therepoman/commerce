package awshttptelemetry

import (
	"code.justin.tv/chat/httptelemetry"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
)

// AttachServiceAndOperation adds a handler to the AWS session to add the AWS service and operation names to the context
func AttachServiceAndOperation(sess *session.Session) *session.Session {
	sess = sess.Copy()

	// The Sign handler is executed right before sending the HTTP request https://github.com/aws/aws-sdk-go/blob/bcb2cf3fc2263c8c28b3119b07d2dbb44d7c93a0/aws/request/request.go#L526-L531
	sess.Handlers.Sign.PushBack(func(req *request.Request) {
		if req == nil || req.HTTPRequest == nil || req.ClientInfo.ServiceName == "" || req.Operation == nil || req.Operation.Name == "" {
			return // do nothing if data is bad
		}

		ctx := req.HTTPRequest.Context()
		// Add AWS service name
		ctx = httptelemetry.WithServiceName(ctx, req.ClientInfo.ServiceName)
		// Add AWS operation name
		ctx = httptelemetry.WithOperationName(ctx, req.Operation.Name)

		req.HTTPRequest = req.HTTPRequest.WithContext(ctx)
	})
	return sess
}
