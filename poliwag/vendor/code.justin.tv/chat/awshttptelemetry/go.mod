module code.justin.tv/chat/awshttptelemetry

go 1.14

require (
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191113205906-40e3e1aa55c5
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1
	code.justin.tv/chat/httptelemetry v1.0.0
	github.com/aws/aws-sdk-go v1.31.7
	github.com/golangci/golangci-lint v1.27.0
	github.com/mgechev/revive v1.0.2
)
