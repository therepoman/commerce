package ecsmetadata

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

// Fetcher retrieves ECS task metadata
type Fetcher struct {
	getenv     func(string) string
	initOnce   sync.Once
	httpClient httpClient
}

type httpClient interface {
	Do(r *http.Request) (*http.Response, error)
}

func (f *Fetcher) init() {
	f.initOnce.Do(func() {
		if f.getenv == nil {
			f.getenv = os.Getenv
		}

		if f.httpClient == nil {
			f.httpClient = http.DefaultClient
		}
	})
}

// Get returns ECS task metadata https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-metadata-endpoint.html
// If there is no metadata then nil, nil is returned
func (f *Fetcher) Get() (*TaskMetadata, error) {
	f.init()

	// Docs: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-metadata-endpoint-v3.html
	metadataURI := f.getenv("ECS_CONTAINER_METADATA_URI")
	if metadataURI == "" {
		return nil, nil
	}

	taskResp, err := f.httpGet(metadataURI + "/task")
	if err != nil {
		return nil, err
	}

	var metadata TaskMetadata
	if err := json.Unmarshal(taskResp, &metadata); err != nil {
		return nil, err
	}

	return &metadata, nil
}

func (f *Fetcher) httpGet(uri string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("creating new HTTP request: %w", err)
	}

	resp, err := f.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("doing HTTP request: %w", err)
	}

	defer func() {
		if cerr := resp.Body.Close(); cerr != nil {
			_ = cerr // Ignore error
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("got non-200 status code %v", resp.StatusCode)
	}

	// prevent unbounded read
	lr := &io.LimitedReader{
		R: resp.Body,
		N: 200 * 1024, // 200 KB
	}

	b, err := ioutil.ReadAll(lr)
	if err != nil {
		return nil, fmt.Errorf("reading response body: %w", err)
	}
	return b, nil
}

// The structs below were all copied from github.com/aws/aws-ecs-agent. Copied to remove having to add amazon-ecs-agent as a dependency.
// AWS has published these APIs publically, so we can expect the APIs to not change in a backwards incompatible way

// TaskMetadata defines the schema for the task response JSON object
// Taken from https://github.com/aws/amazon-ecs-agent/blob/0e4174f6ca3154af6c88532345d584a50e09287f/agent/handlers/v2/response.go#L34
type TaskMetadata struct {
	Cluster               string              `json:"Cluster"`
	TaskARN               string              `json:"TaskARN"`
	Family                string              `json:"Family"`
	Revision              string              `json:"Revision"`
	DesiredStatus         string              `json:"DesiredStatus,omitempty"`
	KnownStatus           string              `json:"KnownStatus"`
	Containers            []ContainerResponse `json:"Containers,omitempty"`
	Limits                *LimitsResponse     `json:"Limits,omitempty"`
	PullStartedAt         *time.Time          `json:"PullStartedAt,omitempty"`
	PullStoppedAt         *time.Time          `json:"PullStoppedAt,omitempty"`
	ExecutionStoppedAt    *time.Time          `json:"ExecutionStoppedAt,omitempty"`
	AvailabilityZone      string              `json:"AvailabilityZone,omitempty"`
	TaskTags              map[string]string   `json:"TaskTags,omitempty"`
	ContainerInstanceTags map[string]string   `json:"ContainerInstanceTags,omitempty"`
}

// TaskID is a helper for extracting the task ID from the TaskARN. Returns a blank string if there is no task ID
// Custom added
func (t *TaskMetadata) TaskID() string {
	idx := strings.LastIndex(t.TaskARN, "/")
	if idx == -1 || idx == len(t.TaskARN)-1 {
		return ""
	}
	return t.TaskARN[idx+1:]
}

// ContainerResponse defines the schema for the container response
// JSON object
type ContainerResponse struct {
	ID            string            `json:"DockerId"`
	Name          string            `json:"Name"`
	DockerName    string            `json:"DockerName"`
	Image         string            `json:"Image"`
	ImageID       string            `json:"ImageID"`
	Ports         []PortResponse    `json:"Ports,omitempty"`
	Labels        map[string]string `json:"Labels,omitempty"`
	DesiredStatus string            `json:"DesiredStatus"`
	KnownStatus   string            `json:"KnownStatus"`
	ExitCode      *int              `json:"ExitCode,omitempty"`
	Limits        LimitsResponse    `json:"Limits"`
	CreatedAt     *time.Time        `json:"CreatedAt,omitempty"`
	StartedAt     *time.Time        `json:"StartedAt,omitempty"`
	FinishedAt    *time.Time        `json:"FinishedAt,omitempty"`
	Type          string            `json:"Type"`
	Networks      []Network         `json:"Networks,omitempty"`
	Health        *HealthStatus     `json:"Health,omitempty"`
	Volumes       []VolumeResponse  `json:"Volumes,omitempty"`
}

// LimitsResponse defines the schema for task/cpu limits response
// JSON object
type LimitsResponse struct {
	CPU    *float64 `json:"CPU,omitempty"`
	Memory *int64   `json:"Memory,omitempty"`
}

// PortResponse defines the schema for portmapping response JSON
// object.
type PortResponse struct {
	ContainerPort uint16 `json:"ContainerPort,omitempty"`
	Protocol      string `json:"Protocol,omitempty"`
	HostPort      uint16 `json:"HostPort,omitempty"`
}

// VolumeResponse is the schema for the volume response JSON object
type VolumeResponse struct {
	DockerName  string `json:"DockerName,omitempty"`
	Source      string `json:"Source,omitempty"`
	Destination string `json:"Destination,omitempty"`
}

// Network is a struct that keeps track of metadata of a network interface
type Network struct {
	NetworkMode   string   `json:"NetworkMode,omitempty"`
	IPv4Addresses []string `json:"IPv4Addresses,omitempty"`
	IPv6Addresses []string `json:"IPv6Addresses,omitempty"`
}

// HealthStatus contains the health check result returned by docker
type HealthStatus struct {
	// Status is the container health status
	Status ContainerHealthStatus `json:"status,omitempty"`
	// Since is the timestamp when container health status changed
	Since *time.Time `json:"statusSince,omitempty"`
	// ExitCode is the exitcode of health check if failed
	ExitCode int `json:"exitCode,omitempty"`
	// Output is the output of health check
	Output string `json:"output,omitempty"`
}

const (
	// ContainerHealthUnknown is the initial status of container health
	ContainerHealthUnknown ContainerHealthStatus = iota
	// ContainerHealthy represents the status of container health check when returned healthy
	ContainerHealthy
	// ContainerUnhealthy represents the status of container health check when returned unhealthy
	ContainerUnhealthy
)

// ContainerStatus is an enumeration of valid states in the container lifecycle
type ContainerStatus int32

const (
	// ContainerStatusNone is the zero state of a container; this container has not completed pull
	ContainerStatusNone ContainerStatus = iota
	// ContainerPulled represents a container which has had the image pulled
	ContainerPulled
	// ContainerCreated represents a container that has been created
	ContainerCreated
	// ContainerRunning represents a container that has started
	ContainerRunning
	// ContainerResourcesProvisioned represents a container that has completed provisioning all of its
	// resources. Non-internal containers (containers present in the task definition) transition to
	// this state without doing any additional work. However, containers that are added to a task
	// by the ECS Agent would possibly need to perform additional actions before they can be
	// considered "ready" and contribute to the progress of a task. For example, the "pause" container
	// would be provisioned by invoking CNI plugins
	ContainerResourcesProvisioned
	// ContainerStopped represents a container that has stopped
	ContainerStopped
	// ContainerZombie is an "impossible" state that is used as the maximum
	ContainerZombie
)

var containerStatusMap = map[string]ContainerStatus{
	"NONE":                  ContainerStatusNone,
	"PULLED":                ContainerPulled,
	"CREATED":               ContainerCreated,
	"RUNNING":               ContainerRunning,
	"RESOURCES_PROVISIONED": ContainerResourcesProvisioned,
	"STOPPED":               ContainerStopped,
}

// String returns a human readable string representation of this object
func (cs ContainerStatus) String() string {
	for k, v := range containerStatusMap {
		if v == cs {
			return k
		}
	}
	return "NONE"
}

// ContainerHealthStatus is an enumeration of container health check status
type ContainerHealthStatus int32

// BackendStatus returns the container health status recognized by backend
func (healthStatus ContainerHealthStatus) BackendStatus() string {
	switch healthStatus {
	case ContainerHealthy:
		return "HEALTHY"
	case ContainerUnhealthy:
		return "UNHEALTHY"
	default:
		return "UNKNOWN"
	}
}

// String returns the readable description of the container health status
func (healthStatus ContainerHealthStatus) String() string {
	return healthStatus.BackendStatus()
}
