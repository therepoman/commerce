# ecsmetadata

Retrieves ECS task metadata from Task Metadata Endpoint version >= 3. AWS ECS Agent version >= 1.21.0 or Fargate is required

Docs: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-metadata-endpoint.html

Usage
```go
var f ecsmetadata.Fetcher
metadata, err := f.Get()
if err != nil {
  // Handle error
}

if metadata == nil {
  // There is no metadata because this program could not be running in ECS
}
```
