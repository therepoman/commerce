module code.justin.tv/chat/ecsmetadata

go 1.13

require (
	github.com/golangci/golangci-lint v1.25.1 // indirect
	github.com/mgechev/revive v1.0.2 // indirect
	github.com/stretchr/testify v1.5.1
)
