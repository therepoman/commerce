# The builder and linter for this repository
# Notice how we depend upon nothing twitch specific: just pulling the core golang docker image
FROM golang:1.13.4

# Commented out because there are no internal dependencies
# This is twitch's go module proxy
# Note: A regular go build will not work since you need to use the tunnel to set
#       this build arg.  You can use tgoproxy for that.  See the Makefile for
#       an example
# ARG GOPROXY=http://jtv.goproxy.twitch.a2z.com
# ENV GOPROXY=$GOPROXY

# Do this first so this step is cached
RUN GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.27.0

RUN ls $GOPATH/bin

# Go modules don't need to be in GOPATH.  Just build/test inside app
WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

# Add our code, build it, test it, etc
COPY . .
# Make sure our code builds
RUN go build -mod=readonly ./...
# Make sure nobody hacked the module repository
RUN go mod verify
# Lint our code
RUN go vet ./...
RUN golangci-lint run
# Run the unit tests with the race detector
RUN go test -race ./...
# Upload code coverage
# RUN curl -s https://codecov.internal.justin.tv/bash | CODECOV_TOKEN=ghe bash -s - || true
