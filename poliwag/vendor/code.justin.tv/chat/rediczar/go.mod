module code.justin.tv/chat/rediczar

go 1.13

require (
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f // indirect
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191113205906-40e3e1aa55c5
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1
	code.justin.tv/chat/ratecache v1.0.1
	github.com/alicebob/miniredis/v2 v2.13.2
	github.com/aws/aws-sdk-go v1.31.7 // indirect
	github.com/cactus/go-statsd-client/statsd v0.0.0-20190805010426-5089fcbbe532
	github.com/go-redis/redis/v7 v7.4.0
	github.com/golangci/golangci-lint v1.27.0
	github.com/maxbrunsfeld/counterfeiter/v6 v6.2.3
	github.com/mgechev/revive v1.0.2
	github.com/stretchr/testify v1.5.1
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
	golang.org/x/tools v0.0.0-20200502202811-ed308ab3e770
)

replace github.com/go-redis/redis/v7 => code.justin.tv/chat/goredis/v7 v7.4.0
