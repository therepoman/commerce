package telemetryrunmetrics

import (
	"context"
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
)

const serviceName = "Redis"

// StatTracker sends run metrics to a telemetry SampleReporter. The metrics are reported as dependency metrics following
// the amzn/TwitchTelemetryMetricsMiddleware pattern.
//
// The Dependency dimension is set to "Redis:{CommandName}"
//
// Metrics reported:
//
// * DependencyServerError - Sum or average statitic is most useful
// * DependencySuccess - Sum or average statitic is most useful
// * DependencyDuration - PXX, or Maximum statistic is most useful
// * DependencyClientError - Ignore - Sum is always 0
type StatTracker struct {
	// SampleReporter sends metrics. If nil, no metrics are sent
	SampleReporter *telemetry.SampleReporter
}

// Success is called each time a command does not return an error, or if the error is redis.Nil
func (s *StatTracker) Success(ctx context.Context, name string, dur time.Duration) {
	if s.SampleReporter == nil {
		return
	}

	s.reportCommand(ctx, name, dur, telemetry.AvailabilityCodeSucccess)
}

// Error is called each time a command returns an error, and if the error is not redis.Nil
func (s *StatTracker) Error(ctx context.Context, name string, dur time.Duration) {
	if s.SampleReporter == nil {
		return
	}

	s.reportCommand(ctx, name, dur, telemetry.AvailabilityCodeServerError)
}

func (s *StatTracker) reportCommand(ctx context.Context, name string, dur time.Duration, code telemetry.AvailabilityCode) {
	reporter := s.createReporter(ctx, name)
	reporter.ReportDurationSample(telemetry.MetricDependencyDuration, dur)
	reporter.ReportAvailabilitySamples(code)
}

// createReporter must be called only when s.SampleReporter is not nil
func (s *StatTracker) createReporter(ctx context.Context, name string) *telemetry.SampleReporter {
	// Create a copy of struct (convention to change dimensions)
	var reporter telemetry.SampleReporter
	// Attach service operation and timestamp
	reporter = telemetry.SampleReporterWithContext(*s.SampleReporter, ctx)

	reporter.DependencyProcessIdentifier = identifier.ProcessIdentifier{
		Service: serviceName,
	}
	reporter.DependencyOperationName = name

	return &reporter
}
