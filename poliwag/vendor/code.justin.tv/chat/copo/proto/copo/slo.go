// Code generated by slogenerator. DO NOT EDIT.
package copo

import (
	"time"

	"code.justin.tv/hygienic/twirpserviceslohook"
)

var _ twirpserviceslohook.ServiceName = ""
var _ time.Duration = time.Second

// SLOCopo is auto generated and helps track method call SLO
var SLOCopo = map[twirpserviceslohook.ServiceName]map[twirpserviceslohook.MethodName]time.Duration{
	twirpserviceslohook.ServiceName("Copo"): {
		twirpserviceslohook.MethodName("AddPoints"):                               500000000,
		twirpserviceslohook.MethodName("BulkEditRedemptionStatus"):                500000000,
		twirpserviceslohook.MethodName("ClaimPoints"):                             1000000000,
		twirpserviceslohook.MethodName("CommitHold"):                              500000000,
		twirpserviceslohook.MethodName("ContributeToCommunityGoal"):               500000000,
		twirpserviceslohook.MethodName("CreateCommunityGoal"):                     500000000,
		twirpserviceslohook.MethodName("CreateCustomReward"):                      600000000,
		twirpserviceslohook.MethodName("CreateImageUploadInfo"):                   500000000,
		twirpserviceslohook.MethodName("DeleteCommunityGoal"):                     500000000,
		twirpserviceslohook.MethodName("EditRedemptionStatus"):                    600000000,
		twirpserviceslohook.MethodName("GetActiveMultipliers"):                    500000000,
		twirpserviceslohook.MethodName("GetAutomaticRewards"):                     500000000,
		twirpserviceslohook.MethodName("GetAvailableClaim"):                       500000000,
		twirpserviceslohook.MethodName("GetBalance"):                              750000000,
		twirpserviceslohook.MethodName("GetBulkEditRedemptionStatusProgress"):     200000000,
		twirpserviceslohook.MethodName("GetChannelLastViewedContent"):             500000000,
		twirpserviceslohook.MethodName("GetChannelSettings"):                      500000000,
		twirpserviceslohook.MethodName("GetCommunityGoal"):                        500000000,
		twirpserviceslohook.MethodName("GetCustomReward"):                         400000000,
		twirpserviceslohook.MethodName("GetCustomRewardTemplateCollections"):      200000000,
		twirpserviceslohook.MethodName("GetEarlyAccessSettings"):                  200000000,
		twirpserviceslohook.MethodName("GetEmoteModifiers"):                       200000000,
		twirpserviceslohook.MethodName("GetEmoteVariants"):                        750000000,
		twirpserviceslohook.MethodName("GetGlobalLastViewedContent"):              500000000,
		twirpserviceslohook.MethodName("GetLimitedEarnings"):                      400000000,
		twirpserviceslohook.MethodName("GetRedemption"):                           500000000,
		twirpserviceslohook.MethodName("GetRewardSummary"):                        500000000,
		twirpserviceslohook.MethodName("GetSmartCostsAcknowledgements"):           500000000,
		twirpserviceslohook.MethodName("GetUserStreamCommunityGoalContributions"): 500000000,
		twirpserviceslohook.MethodName("GetUserStreamRedemptions"):                500000000,
		twirpserviceslohook.MethodName("HoldPoints"):                              500000000,
		twirpserviceslohook.MethodName("ListArchivedCommunityGoals"):              500000000,
		twirpserviceslohook.MethodName("ListCommunityGoals"):                      500000000,
		twirpserviceslohook.MethodName("ListCustomRewards"):                       500000000,
		twirpserviceslohook.MethodName("ListRedemptions"):                         500000000,
		twirpserviceslohook.MethodName("RedeemAutomaticReward"):                   1000000000,
		twirpserviceslohook.MethodName("RedeemCustomReward"):                      1000000000,
		twirpserviceslohook.MethodName("RemoveCustomReward"):                      500000000,
		twirpserviceslohook.MethodName("SubtractPoints"):                          500000000,
		twirpserviceslohook.MethodName("UpdateAutomaticReward"):                   500000000,
		twirpserviceslohook.MethodName("UpdateChannelSettings"):                   500000000,
		twirpserviceslohook.MethodName("UpdateCommunityGoal"):                     500000000,
		twirpserviceslohook.MethodName("UpdateCustomReward"):                      600000000,
		twirpserviceslohook.MethodName("UpdateEarlyAccessSettings"):               200000000,
		twirpserviceslohook.MethodName("UpdateLastViewedContent"):                 500000000,
		twirpserviceslohook.MethodName("UpdateSmartCostsAcknowledgements"):        500000000,
	},
}
