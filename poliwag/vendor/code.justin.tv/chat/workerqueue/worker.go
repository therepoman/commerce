package workerqueue

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	// defaultVisibilityExtension is the duration to extend the visibility
	// timeout of a message that is currently being processed.
	defaultVisibilityExtension = 1 * time.Minute

	// visibilityExtensionBuffer is the time before a visibility timeout expires
	// to extend the visibility timeout.
	visibilityExtensionBuffer = 10 * time.Second

	// visibilityDelaySeconds is the number of seconds to delay a new task from being visible.
	visibilityDelaySeconds = 1
)

// CreateWorkersParams configures the workers to be created.
type CreateWorkersParams struct {
	// NumWorkers is the number of concurrent workers to spawn.
	NumWorkers int

	// AccountID is the AWS account that created the queue. If unset, defaults
	// to the AWS account of the caller.
	AccountID string

	// QueueName is the name (not ARN) of an SQS queue.
	QueueName string

	// Tasks define versioned functions that are run to handle an SQS message.
	Tasks map[TaskVersion]TaskFn

	// KeepTaskHidden will continuously extend a task periodically, up to the
	// MaxVisibilityTimeout.
	KeepTaskHidden bool

	// MaxVisibilityTimeout is the max duration a task can be hidden in the queue
	// before it is made visible to other SQS consumers. This duration should be
	// set to the longest time this job should take to process.
	MaxVisibilityTimeout time.Duration

	// Region is the AWS region where the SQS queue is hosted. This is required if no Client
	// is passed in.
	Region string

	// Client is an optional parameter to specify a custom SQS client.
	Client sqsiface.SQSAPI

	// DefaultMessageRetentionPeriod is a fallback duration if MessageRetentionPeriod attribute is not set on the queue.
	// The known case where this is not set is when using https://github.com/p4tin/goaws for local SQS.
	DefaultMessageRetentionPeriod int

	// DefaultVisibilityTimeout is a fallback duration if VisibilityTimeout attribute is not set on the queue.
	// The known case where this is not set is when using https://github.com/p4tin/goaws for local SQS.
	DefaultVisibilityTimeout int

	// Reporter is an optional SampleReporter to report metrics to
	Reporter *telemetry.SampleReporter
}

var recoverFunc func(error) error
var recoverFuncOnce sync.Once

// SetRecoverFunc sets the function to be called if a panic is recovered
// when calling a task function. This can be useful to return an error with a
// stack trace attached. Subsequent calls to this function are ignored.
func SetRecoverFunc(f func(err error) error) {
	recoverFuncOnce.Do(func() {
		recoverFunc = f
	})
}

// CreateWorkersSansStats calls CreateWorkers with the provided params and stop channel but a noop statsd client
func CreateWorkersSansStats(params CreateWorkersParams, stop <-chan struct{}) (*sync.WaitGroup, <-chan error, error) {
	return CreateWorkers(params, stop, &statsd.NoopClient{})
}

// CreateWorkers spawns some number of workers to read messages from an SQS queue
// and handle them via "task" functions. A "done" channel allows the caller to
// gracefully kill workers.
func CreateWorkers(params CreateWorkersParams, stop <-chan struct{}, stats statsd.StatSender) (*sync.WaitGroup, <-chan error, error) {
	if err := validateParams(params); err != nil {
		return nil, nil, err
	}

	var client sqsiface.SQSAPI
	if params.Client != nil {
		client = params.Client
	} else {
		session, err := session.NewSession(&aws.Config{
			Region: aws.String(params.Region),
		})
		if err != nil {
			return nil, nil, errors.New("creating aws session")
		}
		client = sqs.New(session)
	}

	var accountID *string
	if params.AccountID != "" {
		accountID = aws.String(params.AccountID)
	}

	resp, err := client.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName:              aws.String(params.QueueName),
		QueueOwnerAWSAccountId: accountID,
	})
	if err != nil {
		return nil, nil, fmt.Errorf("getting queue URL: %v", err)
	}

	queueAttrs, err := client.GetQueueAttributes(&sqs.GetQueueAttributesInput{
		QueueUrl:       resp.QueueUrl,
		AttributeNames: []*string{aws.String("All")},
	})
	if err != nil {
		return nil, nil, fmt.Errorf("getting queue attributes: %v", err)
	}

	var messageRetentionPeriodSeconds int
	if queueAttrs.Attributes["MessageRetentionPeriod"] != nil {
		messageRetentionPeriodSeconds, err = strconv.Atoi(*queueAttrs.Attributes["MessageRetentionPeriod"])
		if err != nil {
			return nil, nil, fmt.Errorf("getting queue attribute 'MessageRetentionPeriod': %v", err)
		}
	} else if params.DefaultMessageRetentionPeriod != 0 {
		messageRetentionPeriodSeconds = params.DefaultMessageRetentionPeriod
	} else {
		return nil, nil, errors.New("must provide MessageRetentionPeriod as attribute or param")
	}
	messageRetentionPeriod := time.Duration(messageRetentionPeriodSeconds) * time.Second
	if params.KeepTaskHidden && params.MaxVisibilityTimeout >= messageRetentionPeriod {
		// A message may be hidden longer than it's expiration and expire before being picked up by another worker.
		return nil, nil, ErrMaxVisibilityTimeoutTooLow
	}

	var visibilityTimeoutSeconds int
	if queueAttrs.Attributes["VisibilityTimeout"] != nil {
		visibilityTimeoutSeconds, err = strconv.Atoi(*queueAttrs.Attributes["VisibilityTimeout"])
		if err != nil {
			return nil, nil, fmt.Errorf("getting queue attribute 'VisibilityTimeout': %v", err)
		}
	} else if params.DefaultVisibilityTimeout != 0 {
		visibilityTimeoutSeconds = params.DefaultVisibilityTimeout
	} else {
		return nil, nil, errors.New("must provide VisibilityTimeout as attribute or param")
	}
	visibilityTimeout := time.Duration(visibilityTimeoutSeconds) * time.Second
	if params.KeepTaskHidden && params.MaxVisibilityTimeout < visibilityTimeout {
		// A message should be able to be hidden at least as long as the queue's default visibility timeout.
		return nil, nil, ErrMaxVisibilityTimeoutTooLow
	}

	var mr metricsReporter
	if params.Reporter != nil {
		r := reporterWithDimensions(*params.Reporter, map[string]string{"QueueName": params.QueueName})
		mr = &r
	}

	decoratedTasks := map[TaskVersion]TaskFn{}
	for version, fn := range params.Tasks {
		decoratedTasks[version] = decorateTaskFn(fn)
	}

	errCh := make(chan error)
	var wg sync.WaitGroup
	for i := 0; i < params.NumWorkers; i++ {
		wg.Add(1)
		go func(j int) {
			defer wg.Done()
			w := &workerImpl{
				client:    client,
				stats:     stats,
				reporter:  mr,
				queueName: params.QueueName,
				queueURL:  resp.QueueUrl,
				tasks:     decoratedTasks,
				errCh:     errCh,
				stop:      stop,

				defaultVisibilityTimeout: visibilityTimeout,
				maxVisibilityTimeout:     params.MaxVisibilityTimeout,
				keepTaskHidden:           params.KeepTaskHidden,
			}
			w.loop()
		}(i)
	}
	return &wg, errCh, nil
}

func validateParams(params CreateWorkersParams) error {
	switch {
	case params.NumWorkers < 1:
		return errors.New("invalid NumWorkers")
	case params.QueueName == "":
		return errors.New("invalid QueueName")
	case params.Client == nil && params.Region == "":
		return errors.New("invalid Region - required because no Client is set")
	case len(params.Tasks) == 0:
		return errors.New("invalid Tasks")
	}
	return nil
}

// decorateTaskFn adds panic protection to the TaskFn
func decorateTaskFn(fn TaskFn) TaskFn {
	return func(msg *sqs.Message) (err error) {
		defer func() {
			if r := recover(); r != nil {
				err = r.(error)

				if recoverFunc != nil {
					err = recoverFunc(err)
				}
			}
		}()

		return fn(msg)
	}
}

type workerImpl struct {
	client    sqsiface.SQSAPI
	stats     statsd.StatSender
	reporter  metricsReporter
	queueName string
	queueURL  *string
	tasks     map[TaskVersion]TaskFn
	errCh     chan<- error
	stop      <-chan struct{}

	defaultVisibilityTimeout time.Duration
	maxVisibilityTimeout     time.Duration
	keepTaskHidden           bool
}

// loop repeatedly fetches and handles jobs from SQS
func (w *workerImpl) loop() {
	for {
		select {
		case <-w.stop:
			return
		default:
			if err := w.do(); err != nil && err != ErrNoMessageInQueue && !isTransientError(err) {
				w.handleError(err)
			}
		}
	}
}

func (w *workerImpl) do() error {
	message, err := w.getTask()
	if err != nil {
		return err
	} else if message == nil {
		return ErrNoMessageInQueue
	}

	// Get handler function for this task version.
	// If no handler exists, track it and replace the task.
	fn, ok, err := w.getTaskFn(message.MessageAttributes["Version"])
	if err != nil {
		return err
	} else if !ok {
		inc(w.stats, w.statName("unhandled_version"), 1, 1.0)
		report(w.reporter, w.reporterName("UnhandledVersion"), 1)
		w.replaceTask(message.ReceiptHandle)
		return nil
	}

	// Process the task and extend visibility timeout, if configured.
	start := time.Now()
	taskCompleted := make(chan struct{})
	if w.keepTaskHidden {
		go w.extendVisibilityTimeout(message.ReceiptHandle, taskCompleted)
	}
	err = fn(message)
	dur := time.Since(start)
	close(taskCompleted)

	if err != nil {
		w.replaceTask(message.ReceiptHandle)
		timing(w.stats, w.statName("taskFn.fail"), dur, 1.0)
		reportTiming(w.reporter, w.reporterName("TaskFn_Fail"), dur)
		return err
	}
	w.deleteTask(message.ReceiptHandle)
	timing(w.stats, w.statName("taskFn.success"), dur, 1.0)
	reportTiming(w.reporter, w.reporterName("TaskFn_Success"), dur)

	if val := message.MessageAttributes["PublishTime"]; val != nil {
		publishTime, err := strconv.ParseInt(*message.MessageAttributes["PublishTime"].StringValue, 10, 64)
		if err != nil {
			w.handleError(fmt.Errorf("getting publish time: %v", err))
		}
		totalTime := time.Since(time.Unix(publishTime, 0))
		timing(w.stats, w.statName("latency"), totalTime, 1.0)
		reportTiming(w.reporter, w.reporterName("Latency"), totalTime)
	}
	return nil
}

type receiveMessageResult struct {
	err  error
	resp *sqs.ReceiveMessageOutput
}

// stop channel aware SQS receiveMessage
func (w *workerImpl) sendSQSReceiveMessage(params *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	resultCh := make(chan receiveMessageResult, 1)
	go func() {
		defer close(resultCh)
		resp, err := w.client.ReceiveMessageWithContext(aws.Context(ctx), params)
		resultCh <- receiveMessageResult{
			err:  err,
			resp: resp,
		}
	}()

	select {
	case <-w.stop:
		return &sqs.ReceiveMessageOutput{}, nil
	case result := <-resultCh:
		return result.resp, result.err
	}
}

// getTask fetches a task from SQS.
func (w *workerImpl) getTask() (*sqs.Message, error) {
	params := &sqs.ReceiveMessageInput{
		QueueUrl:              w.queueURL,
		MaxNumberOfMessages:   aws.Int64(1),
		MessageAttributeNames: []*string{aws.String("All")},
		AttributeNames:        []*string{aws.String("All")},
		// Use long polling as recommended by AWS
		// See: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-long-polling.html
		WaitTimeSeconds: aws.Int64(20),
	}
	start := time.Now()
	resp, err := w.sendSQSReceiveMessage(params)
	dur := time.Since(start)
	if err != nil {
		timing(w.stats, w.statName("receive.fail"), dur, 1.0)
		reportTiming(w.reporter, w.reporterName("Receive_Fail"), dur)
		return nil, err
	}
	timing(w.stats, w.statName("receive.success"), dur, 1.0)
	reportTiming(w.reporter, w.reporterName("Receive_Success"), dur)

	if len(resp.Messages) != 1 {
		return nil, nil
	}

	message := resp.Messages[0]
	// st, rt are in ms
	var sentTimestamp, receivedTimestamp int
	if message.Attributes["SentTimestamp"] != nil {
		sentTimestamp, _ = strconv.Atoi(*message.Attributes["SentTimestamp"])
	}
	if message.Attributes["ApproximateFirstReceiveTimestamp"] != nil {
		receivedTimestamp, _ = strconv.Atoi(*message.Attributes["ApproximateFirstReceiveTimestamp"])
	}
	dur = time.Duration(receivedTimestamp-sentTimestamp) * time.Millisecond
	timing(w.stats, w.statName("in_queue"), dur, 1.0)
	reportTiming(w.reporter, w.reporterName("InQueue"), dur)
	return message, nil
}

// getTaskFn fetches the handler for this job version. Fallback to the
// FallbackTaskVersion task if it exists.
func (w *workerImpl) getTaskFn(versionAttr *sqs.MessageAttributeValue) (TaskFn, bool, error) {
	if versionAttr != nil {
		version, err := strconv.ParseInt(*versionAttr.StringValue, 10, 64)
		if err != nil {
			return nil, false, err
		}
		if t, ok := w.tasks[TaskVersion(version)]; ok {
			return t, true, nil
		}
	}
	fn, ok := w.tasks[FallbackTaskVersion]
	if !ok {
		return nil, false, errors.New("no TaskFn found to handle this task version")
	}
	return fn, true, nil
}

// extendVisibilityTimeout extends the timeout every <defaultVisibilityExtension>
// up to <maxVisibilityTimeout>. Stop looping when <taskCompleted> is closed.
func (w *workerImpl) extendVisibilityTimeout(receiptHandle *string, taskCompleted <-chan struct{}) {
	maxVisibilityExceeded := time.NewTimer(w.maxVisibilityTimeout)
	defer maxVisibilityExceeded.Stop()

	params := &sqs.ChangeMessageVisibilityInput{
		ReceiptHandle:     receiptHandle,
		QueueUrl:          w.queueURL,
		VisibilityTimeout: aws.Int64(int64(defaultVisibilityExtension.Seconds())),
	}

	// Block until the task completes or the queue's visibility timeout is nearly expired.
	select {
	case <-taskCompleted:
		return
	case <-time.After(w.defaultVisibilityTimeout - visibilityExtensionBuffer):
	}

	if _, err := w.client.ChangeMessageVisibility(params); err != nil {
		w.handleError(fmt.Errorf("extending visibility timeout: %v", err))
	}

	ticker := time.NewTicker(defaultVisibilityExtension - visibilityExtensionBuffer)
	defer ticker.Stop()

	for {
		select {
		case <-taskCompleted:
			// Handler completed: no need to keep task hidden.
			return

		case <-maxVisibilityExceeded.C:
			// Max visibility timeout duration reached: let task become visible to other workers.
			return

		case <-ticker.C:
			// Update visibility timeout periodically.
			if _, err := w.client.ChangeMessageVisibility(params); err != nil {
				// This is possible if 2+ workers are processing the same message and
				// one worker deletes the message.
				if isInvalidParameterValueError(err) {
					return
				}
				w.handleError(fmt.Errorf("extending visibility timeout: %v", err))
			}
		}
	}
}

// deleteTask removes a job from the queue, typically after successfully processing it.
func (w *workerImpl) deleteTask(receiptHandle *string) error {
	params := &sqs.DeleteMessageInput{
		ReceiptHandle: receiptHandle,
		QueueUrl:      w.queueURL,
	}
	start := time.Now()
	_, err := w.client.DeleteMessage(params)
	dur := time.Since(start)
	if err != nil {
		timing(w.stats, w.statName("delete.fail"), dur, 1.0)
		reportTiming(w.reporter, w.reporterName("Delete_Fail"), dur)
		return err
	}
	timing(w.stats, w.statName("delete.success"), dur, 1.0)
	reportTiming(w.reporter, w.reporterName("Delete_Success"), dur)
	return nil
}

// replaceTask sets as visibility timeout to a message. This ensures that other
// messages on the queue (if any) will be read before this one again.
func (w *workerImpl) replaceTask(receiptHandle *string) error {
	params := &sqs.ChangeMessageVisibilityInput{
		ReceiptHandle:     receiptHandle,
		QueueUrl:          w.queueURL,
		VisibilityTimeout: aws.Int64(visibilityDelaySeconds),
	}
	start := time.Now()
	_, err := w.client.ChangeMessageVisibility(params)
	dur := time.Since(start)
	if err != nil {
		timing(w.stats, w.statName("replace.fail"), dur, 1.0)
		reportTiming(w.reporter, w.reporterName("Replace_Fail"), dur)
		return err
	}
	timing(w.stats, w.statName("replace.success"), dur, 1.0)
	reportTiming(w.reporter, w.reporterName("Replace_Success"), dur)
	return nil
}

// handleError dumps errors to the error channel. Drop errors if the consumer
// of the channel is slow or no consumer exists.
func (w *workerImpl) handleError(err error) {
	select {
	case w.errCh <- err:
	default:
	}
}

func (w *workerImpl) statName(stat string) string {
	return fmt.Sprintf("service.%s.%s", w.queueName, stat)
}

// reporterName is left as a function to help ensure any changes to it are done everywhere
// even though it's not performing a transformation right now.
func (w *workerImpl) reporterName(stat string) string {
	return stat
}
