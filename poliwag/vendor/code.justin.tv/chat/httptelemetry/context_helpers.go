package httptelemetry

import (
	"context"
)

type contextKey string

var (
	operationNameKey = contextKey("operation-name")
	serviceNameKey   = contextKey("service-name")
)

// WithServiceName attaches the service name to the context. This should be called on the context before the
// HTTP operation is executed. This is also known as the dependency process identifier service name
func WithServiceName(ctx context.Context, serviceName string) context.Context {
	return context.WithValue(ctx, serviceNameKey, serviceName)
}

func getServiceName(ctx context.Context) string {
	s, ok := ctx.Value(serviceNameKey).(string)
	if ok {
		return s
	}
	return ""
}

// WithOperationName attaches the operation name to the context. This should be called on the context before the
// HTTP operation is executed. This is also known as the dependency operation name
func WithOperationName(ctx context.Context, methodName string) context.Context {
	return context.WithValue(ctx, operationNameKey, methodName)
}

func getOperationName(ctx context.Context) string {
	s, ok := ctx.Value(operationNameKey).(string)
	if ok {
		return s
	}
	return ""
}
