package httptelemetry

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/http/httptrace"
	"sync"
	"sync/atomic"
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
)

// RoundTripper reports metrics for each RoundTrip call. Users should attach the service name and method name
// to the request context using WithServiceName and WithOperationName respectively. If no name is attached, then
// "Unknown" is used.
type RoundTripper struct {
	// SampleReporter is a TwitchTelemetry reporter. If nil, then no metrics are reported
	SampleReporter *telemetry.SampleReporter
	// RoundTripper is an existing http.RoundTripper. If nil, then http.DefaultTransport is used
	RoundTripper http.RoundTripper
	// PreRoundTrip is called before passing request to the RoundTripper. This is useful to attach the service name and operation
	// name to the request. Implementations should always return a non-nil *http.Request. Defaults to returning the same HTTP request passed in
	PreRoundTrip PreRoundTrip
	// LogUnknowns will turn on logging of unknown service or operations. WARNING: This can potentially log a lot of lines
	LogUnknowns bool
}

var noopPreRoundTrip = func(req *http.Request) *http.Request {
	return req
}

// RoundTrip executes the HTTP request and reports metrics
func (r *RoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	if r.SampleReporter == nil {
		return r.roundTripper().RoundTrip(req)
	}

	start := time.Now()

	// Run PreRoundTrip to attach any contextual data to the request
	req = r.preRoundTrip()(req)

	reporter := r.createReporter(req)

	// Add HTTP client tracing
	t := tracer{
		SampleReporter: reporter,
	}
	req = req.WithContext(httptrace.WithClientTrace(req.Context(), t.ClientTrace()))

	// Do HTTP request
	resp, err := r.roundTripper().RoundTrip(req)

	// Send metrics
	r.reportOperation(reporter, resp, time.Since(start))

	return resp, err
}

func (r *RoundTripper) preRoundTrip() PreRoundTrip {
	if r.PreRoundTrip == nil {
		return noopPreRoundTrip
	}

	return r.PreRoundTrip
}

func (r *RoundTripper) roundTripper() http.RoundTripper {
	if r.RoundTripper == nil {
		return http.DefaultTransport
	}

	return r.RoundTripper
}

const noStatusCode = 0

func (r *RoundTripper) reportOperation(reporter *telemetry.SampleReporter, resp *http.Response, elapsed time.Duration) {
	// statusCode could be 0 if no response is received
	statusCode := noStatusCode
	if resp != nil {
		statusCode = resp.StatusCode
	}

	reporter.ReportDurationSample(telemetry.MetricDependencyDuration, elapsed)
	reporter.ReportAvailabilitySamples(resolveAvailabilityCode(statusCode))
	reporter.Report(statusCodeMetric(statusCode), 1, telemetry.UnitCount)
}

func (r *RoundTripper) createReporter(req *http.Request) *telemetry.SampleReporter {
	ctx := req.Context()

	// Create a copy
	var reporter telemetry.SampleReporter

	// Attach the service operation and timestamp
	reporter = telemetry.SampleReporterWithContext(*r.SampleReporter, ctx)

	hasUnknown := false

	serviceName := getServiceName(ctx)
	if serviceName == "" {
		hasUnknown = true
		serviceName = "Unknown"
	}

	// Mirror how TwitchTelemetryMetricsMiddleWare reports dependency metrics
	// Source: https://git.xarth.tv/amzn/TwitchTelemetryMetricsMiddleware/blob/cb62e9370b7f9197a7516dfe0304ed2d3a5541f8/operation_monitor.go#L99
	reporter.DependencyProcessIdentifier = identifier.ProcessIdentifier{
		Service: serviceName,
	}

	operationName := getOperationName(ctx)
	if operationName == "" {
		hasUnknown = true
		operationName = "Unknown"
	}

	if r.LogUnknowns && req.URL != nil && hasUnknown {
		log.Printf("Unknown service and/or operation ServiceName=%q OperationName=%q RequestURL=%q",
			serviceName,
			operationName,
			req.URL.String(),
		)
	}

	reporter.DependencyOperationName = operationName

	return &reporter
}

func statusCodeMetric(statusCode int) string {
	// Return unknown for invalid status codes
	// Reference: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
	if statusCode < 100 || statusCode >= 600 {
		return "HTTPCode_Unknown"
	}

	h := statusCode / 100
	// Matches ELB metric name pattern
	return fmt.Sprintf("HTTPCode_%dxx", h)
}

// resolveAvailabilityCode determines the availability code
func resolveAvailabilityCode(statusCode int) telemetry.AvailabilityCode {
	switch {
	case statusCode <= noStatusCode:
		// This is an ambiguous code because there's no HTTP response. This could occur due to
		// network errors or timeouts
		return telemetry.AvailabilityCodeClientError
	case statusCode < 400:
		return telemetry.AvailabilityCodeSucccess
	case statusCode < 500:
		return telemetry.AvailabilityCodeClientError
	default:
		return telemetry.AvailabilityCodeServerError
	}
}

// tracer tracks HTTP client metrics
// These metrics are mirrored from twitchclient https://git.xarth.tv/foundation/twitchclient/blob/c2bafe6fd2a30c7fd779650a7b20f2b18a01cd02/client_trace_stats.go
type tracer struct {
	SampleReporter *telemetry.SampleReporter

	// http.ClientTrace hooks may be called concurrently on different goroutines. Make the values stored thread-safe
	mu                     sync.Mutex
	getConnectionStartTime atomicTime
	dnsStartTime           atomicTime
	dialStartTime          atomicTime
	tlsHandshakeStartTime  atomicTime
}

func (t *tracer) ClientTrace() *httptrace.ClientTrace {
	return &httptrace.ClientTrace{
		GetConn: t.getConn,
		GotConn: t.gotConn,

		DNSStart: t.dnsStart,
		DNSDone:  t.dnsDone,

		ConnectStart: t.connectStart,
		ConnectDone:  t.connectDone,

		WroteRequest: t.wroteRequest,

		TLSHandshakeStart: t.tlsHandshakeStart,
		TLSHandshakeDone:  t.tlsHandshakeDone,
	}
}

func (t *tracer) getConn(hostPort string) {
	t.getConnectionStartTime.Set(time.Now())
}

func (t *tracer) gotConn(info httptrace.GotConnInfo) {
	getConnectionStartTime := t.getConnectionStartTime.Get()
	if getConnectionStartTime.IsZero() {
		return // do not attempt to send inaccurate metric if getConn was not run
	}

	elapsed := time.Since(getConnectionStartTime)
	t.SampleReporter.ReportDurationSample("GotConn_Duration", elapsed)

	// Always report counts 0 or 1 to be able to graph percentage using Average

	if info.Reused {
		t.SampleReporter.Report("GotConn_Reused", 1, telemetry.UnitCount)
	} else {
		t.SampleReporter.Report("GotConn_Reused", 0, telemetry.UnitCount)
	}

	if info.WasIdle {
		t.SampleReporter.Report("GotConn_WasIdle", 1, telemetry.UnitCount)
		t.SampleReporter.ReportDurationSample("GotConn_IdleTime", info.IdleTime)
	} else {
		t.SampleReporter.Report("GotConn_WasIdle", 0, telemetry.UnitCount)
	}
}

func (t *tracer) dnsStart(info httptrace.DNSStartInfo) {
	t.dnsStartTime.Set(time.Now())
}

func (t *tracer) dnsDone(info httptrace.DNSDoneInfo) {
	dnsStartTime := t.dnsStartTime.Get()
	if dnsStartTime.IsZero() {
		return // do not attempt to send inaccurate stats if dnsStart was not run.
	}

	elapsed := time.Since(dnsStartTime)
	t.SampleReporter.ReportDurationSample("DNS_Duration", elapsed)

	// Always report counts 0 or 1 to be able to graph percentage using Average

	if info.Err != nil {
		t.SampleReporter.Report("DNS_Error", 1, telemetry.UnitCount)
		t.SampleReporter.Report("DNS_Success", 0, telemetry.UnitCount)
	} else {
		t.SampleReporter.Report("DNS_Success", 1, telemetry.UnitCount)
		t.SampleReporter.Report("DNS_Error", 0, telemetry.UnitCount)
	}

	if info.Coalesced {
		t.SampleReporter.Report("DNS_Coalesced", 1, telemetry.UnitCount)
	} else {
		t.SampleReporter.Report("DNS_Coalesced", 0, telemetry.UnitCount)
	}
}

func (t *tracer) connectStart(network string, addr string) {
	t.dialStartTime.Set(time.Now())
}

func (t *tracer) connectDone(network, addr string, err error) {
	dialStartTime := t.dialStartTime.Get()
	if dialStartTime.IsZero() {
		return // do not attempt to send inaccurate stats connectStart was not run.
	}

	elapsed := time.Since(dialStartTime)

	t.SampleReporter.ReportDurationSample("Dial_Duration", elapsed)

	// Always report counts 0 or 1 to be able to graph percentage using Average

	if err != nil {
		t.SampleReporter.Report("Dial_Error", 1, telemetry.UnitCount)
		t.SampleReporter.Report("Dial_Success", 0, telemetry.UnitCount)
	} else {
		t.SampleReporter.Report("Dial_Error", 0, telemetry.UnitCount)
		t.SampleReporter.Report("Dial_Success", 1, telemetry.UnitCount)
	}

}

func (t *tracer) wroteRequest(info httptrace.WroteRequestInfo) {
	if info.Err != nil {
		t.SampleReporter.Report("ReqWrite_Error", 1, telemetry.UnitCount)
		t.SampleReporter.Report("ReqWrite_Success", 0, telemetry.UnitCount)
	} else {
		t.SampleReporter.Report("ReqWrite_Error", 0, telemetry.UnitCount)
		t.SampleReporter.Report("ReqWrite_Success", 1, telemetry.UnitCount)
	}
}

func (t *tracer) tlsHandshakeStart() {
	t.tlsHandshakeStartTime.Set(time.Now())
}

func (t *tracer) tlsHandshakeDone(state tls.ConnectionState, err error) {
	tlsHandshakeStartTime := t.tlsHandshakeStartTime.Get()
	if tlsHandshakeStartTime.IsZero() {
		return // do not attempt to send inaccurate stats if tlsHandshakeStart was not run.
	}

	elapsed := time.Since(tlsHandshakeStartTime)
	t.SampleReporter.ReportDurationSample("TLSHandshake_Duration", elapsed)

	// Always report counts 0 or 1 to be able to graph percentage using Average

	if err != nil {
		t.SampleReporter.Report("TLSHandshake_Error", 1, telemetry.UnitCount)
		t.SampleReporter.Report("TLSHandshake_Success", 0, telemetry.UnitCount)
	} else {
		t.SampleReporter.Report("TLSHandshake_Error", 0, telemetry.UnitCount)
		t.SampleReporter.Report("TLSHandshake_Success", 1, telemetry.UnitCount)
	}
}

type atomicTime struct {
	value atomic.Value
}

func (a *atomicTime) Get() time.Time {
	v, ok := a.value.Load().(time.Time)
	if ok {
		return v
	}
	return time.Time{}
}

func (a *atomicTime) Set(v time.Time) {
	a.value.Store(v)
}
