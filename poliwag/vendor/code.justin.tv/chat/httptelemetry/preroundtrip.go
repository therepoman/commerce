package httptelemetry

import (
	"net/http"
)

// PreRoundTrip is an function that accepts and returns an *http.Request. This is useful to extract data
// from the HTTP request and/or attaching contextual data to the HTTP request context
type PreRoundTrip func(*http.Request) *http.Request

// ChainPreRoundTrips chains together many PreRoundTrip functions by applying them in order
func ChainPreRoundTrips(prts ...PreRoundTrip) PreRoundTrip {
	return func(req *http.Request) *http.Request {
		for _, prt := range prts {
			req = prt(req)
		}

		return req
	}
}

// StaticServiceName returns a PreRoundTrip that always attaches the same service name
func StaticServiceName(serviceName string) PreRoundTrip {
	return func(req *http.Request) *http.Request {
		ctx := WithServiceName(req.Context(), serviceName)
		return req.WithContext(ctx)
	}
}

// StaticOperationName returns a PreRoundTrip that always attaches the same operation name
func StaticOperationName(operationName string) PreRoundTrip {
	return func(req *http.Request) *http.Request {
		ctx := WithOperationName(req.Context(), operationName)
		return req.WithContext(ctx)
	}
}
