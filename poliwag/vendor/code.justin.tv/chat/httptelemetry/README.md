# httptelemetry

httptelemetry is a Go library that provides an HTTP round tripper to report dependency client metrics to CloudWatch using TwitchTelemetry

Use cases

* Dependency metrics that match Fulton are desired. The metrics reported are a superset of [amzn/TwitchTelemetryMetricsMiddleWare](https://git.xarth.tv/amzn/TwitchTelemetryMetricsMiddleware)
* In the process of migrating a service from StatsD to CloudWatch
  * Preserving the twitchclient [client trace stats](https://git.xarth.tv/foundation/twitchclient/blob/c2bafe6fd2a30c7fd779650a7b20f2b18a01cd02/client_trace_stats.go), and HTTP status code metrics is desired
  * Custom HTTP clients emitting StatsD metrics need to be migrated to CloudWatch
* [HTTP client trace](https://golang.org/pkg/net/http/httptrace/) metrics
* Supported client frameworks:
  * Any library that accepts a Go HTTP client or HTTP RoundTripper
  * aws-sdk-go via [awshttptelemetry](https://git.xarth.tv/chat/awshttptelemetry)
  * foundation/twitchclient via [twitchclienthttptelemetry](https://git.xarth.tv/chat/twitchclienthttptelemetry)
* Additional libraries for dependency metrics per service operations (Twirp method or REST):
  * Twirp integration via [twirpmetric](https://git.xarth.tv/video/metrics-middleware/tree/master/v2/twirpmetric)
  * REST integration via [httpresttelemetry](https://git.xarth.tv/chat/httpresttelemetry)

## How it works

`httptelemetry.RoundTripper` implements the [`http.RoundTripper` interface](https://golang.org/pkg/net/http/#RoundTripper). It accepts another http.RoundTripper implementation, and the TwitchTelemetry SampleReporter.

```go
type RoundTripper interface {
    RoundTrip(*Request) (*Response, error)
}
```

The `httptelemetry.RoundTripper` is set on the [`(*http.Client).Transport`](https://golang.org/pkg/net/http/#Client)

Every HTTP request executed will do the following:

1. Extract the service name and operation name from the context
1. Execute the HTTP request
1. Report metrics


## Usage

1. Create a httptelemetry.RoundTripper from a TwitchTelemetry SampleReporter and existing http.RoundTripper (most likely a http.Transport)
    1. Optional: Attach contextual data to the request context using PreRoundTrip. PreRoundTrip is a hook that is passed the http.Request and is called before executing the HTTP request. This is useful to set the service name or operation name.
        * httptelemetry.StaticServiceName specifies a static service name
        * httptelemetry.StaticOperationName specifies a static operation name
1. Create a http.Client using the httptelemetry.RoundTripper
1. Use the http.Client to execute HTTP requests
1. Integrate [httpresttelemetry](https://git.xarth.tv/chat/httpresttelemetry) or [twirpmetric](https://git.xarth.tv/video/metrics-middleware/tree/master/v2/twirpmetric) to get dependency metrics per service operation (Twirp method or REST handler)

### Attach a service name to the request context

When not using PreRoundTrip, the service name can be manually specified with httptelemetry.WithServiceName

   ```go
   ctx := req.Context()
   ctx = httptelemetry.WithServiceName(ctx, "ServiceName")
   req = req.WithContext(ctx)
   resp, err := httpClient.Do(req)
   // Metrics will be reported
   ```

### Attach a operation name to the request context

When not using PreRoundTrip, the service name can be manually  specified with httptelemetry.WithOperationName

   ```go
   ctx := req.Context()
   ctx = httptelemetry.WithOperationName(ctx, "OperationName")
   req = req.WithContext(ctx)
   resp, err := httpClient.Do(req)
   // Metrics will be reported
   ```
### Example

**For twitchclient see the [README example](https://git.xarth.tv/chat/twitchclienthttptelemetry#example). For aws-sdk-go see the [README example](https://git.xarth.tv/chat/awshttptelemetry#example)**

[Full example of hand-rolled HTTP client](./example/main.go).

HTTP request reporting dependency metrics

   ```go
	httpClient := &http.Client{
		Transport: &httptelemetry.RoundTripper{
			SampleReporter: &sampleReporter,
			RoundTripper:   existingTransport,
			PreRoundTrip:   httptelemetry.StaticServiceName("ServiceName"),
		},
	}

	req, err := http.NewRequest("GET", "https://service.a2z.com", nil)
	if err != nil {
		return err
	}
	ctx := req.Context()
	ctx = httptelemetry.WithOperationName(ctx, "OperationName")
	req = req.WithContext(ctx)

	resp, err := httpClient.Do(req)
	// Metrics will be reported
	if err != nil {
		return err
	}
   ```

## Metrics Recorded

Metrics will show up with a `Dependency` dimension that is a combination of ServiceName and the OperationName delimited by a ":", such as `ServiceA:OperationA"`. Each RoundTrip call will produce metrics. If no ServiceName or OperationName is attached, then "Unknown" is used respectively

An additional `Operation` (service operation) dimension is added to give dependency metrics per service operation. If there is no
Twirp or REST integration, then the value is set to "Unknown". See the above section for `Additional libraries for dependency metrics per service operations` for the appropriate library to integrate

Metrics below that always record counts as 0 or 1 allow graphing percentages using the Average statistic

| Name                    | Details        | Useful Statistic |
|-------------------------|----------------|------------------|
| `DependencyDuration` | Duration of RoundTrip call | p50, p90, p99, Maximum |
| `DependencySuccess` | Success count. Always recorded as 0 or 1 | Average, Sum |
| `DependencyClientError` | Client error count. Always recorded as 0 or 1. A ClientError is a 4xx status code or a Go error | Average, Sum |
| `DependencyServerError` | Remote server error count. Always recorded as a 0 or 1. A ServerError is a 5xx | Average, Sum |
| `HTTPCode_[1,2,3,4,5]xx` | HTTP status code count grouped by the first digit | Sum |
| `HTTPCode_Unknown` | No HTTP status code due to a Go error count | Sum |
| The next section describes [HTTP Client Trace](https://golang.org/pkg/net/http/httptrace/) metrics |
| `GotConn_Duration` | Time to get the HTTP connection | p50, p90, p99, Maximum |
| `GotConn_Reused` | HTTP connection was reused count. Always recorded as 0 or 1 | Average, Sum |
| `GotConn_WasIdle` | HTTP connection was was in the idle connection pool count. Always recorded as 0 or 1 | Average, Sum |
| `GotConn_IdleTime` | How long the HTTP connection was in the idle connection pool | p50, p90, p99, Maximum |
| `DNS_Duration` | How long the DNS request took | p50, p90, p99, Maximum |
| `DNS_Success` | DNS query success count. Always recorded as 0 or 1 | Average, Sum |
| `DNS_Error` | DNS query error count. Always recorded as 0 or 1 | Average, Sum |
| `DNS_Coalesced` | DNS request was deduplicated count. Always recorded as 0 or 1 | Average, Sum |
| `Dial_Duration` | How long the dial connection took | p50, p90, p99, Maximum |
| `Dial_Success` | Dial connection success count. Always recorded as 0 or 1 | Average, Sum |
| `Dial_Error` | Dial connection error count. Always recorded as 0 or 1 | Average, Sum |
| `ReqWrite_Success` | RequestWrite success count. Always recorded as 0 or 1 | Average, Sum |
| `ReqWrite_Error` | RequestWrite error count. Always recorded as 0 or 1 | Average, Sum |
| `TLSHandshake_Duration` | How long the TLS handshake took | p50, p90, p99, Maximum |
| `TLSHandshake_Success` | TLS handshake success count. Always recorded as 0 or 1 | Average, Sum |
| `TLSHandshake_Error` | TLS handshake error count. Always recorded as 0 or 1 | Average, Sum |
