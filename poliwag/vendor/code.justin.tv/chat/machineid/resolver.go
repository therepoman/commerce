package machineid

import (
	"fmt"
	"log"
	"os"
	"sync"

	"code.justin.tv/chat/ecsmetadata"
)

type logger interface {
	Log(args ...interface{})
}

type defaultLogger struct{}

func (l *defaultLogger) Log(args ...interface{}) {
	log.Println(args...)
}

// Resolver gets the machine ID
type Resolver struct {
	// Log is an optional logger
	Log logger

	taskID      func() (string, error)
	hostname    func() (string, error)
	id          string
	resolveOnce sync.Once
}

// Resolve the machine ID once ever, and then caches the value. The same value is always
// returned in subsequent calls
func (r *Resolver) Resolve() string {
	r.resolveOnce.Do(r.resolve)
	return r.id
}

func (r *Resolver) init() {
	if r.taskID == nil {
		var f ecsmetadata.Fetcher

		r.taskID = func() (string, error) {
			metadata, err := f.Get()
			if err != nil {
				return "", err
			}

			if metadata == nil {
				return "", nil
			}

			return metadata.TaskID(), nil
		}
	}

	if r.hostname == nil {
		r.hostname = os.Hostname
	}

	if r.Log == nil {
		r.Log = &defaultLogger{}
	}
}

func (r *Resolver) resolve() {
	r.init()

	var id string

	// Check ECS metadata first
	taskID, err := r.taskID()
	if err != nil {
		r.Log.Log("err", err, "failed getting ECS metadata")
	} else if taskID != "" {
		id = fmt.Sprintf("ecs-%s", taskID)
	}

	if id == "" {
		// Fallback to hostname
		hostname, err := r.hostname()
		if err != nil {
			r.Log.Log("err", err, "failed getting hostname")
		} else {
			id = hostname
		}
	}

	if id == "" {
		id = "UNKNOWN"
	}

	r.id = id
}

// DefaultResolver is the default machine ID resolver
var DefaultResolver = Resolver{}

// Resolve returns a machine ID for the host of this Go process. This should be used as the Machine
// value for ProcessIdentifier.
//
// Order of precedence:
//
//   1. ECS task ID as "ecs-{ECS_TASK_ID}". ECS_TASK_ID is retrieved from ECS task metadata
//   2. Hostname retrieved from os.Hostname()
//   3. Fallback to returning "UNKNOWN"
func Resolve() string {
	return DefaultResolver.Resolve()
}
