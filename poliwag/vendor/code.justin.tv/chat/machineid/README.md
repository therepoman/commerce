# machineid

Retrieves a machine ID

Order of precedence:
1. ECS task ID as `ecs-{ECS_TASK_ID}`. ECS_TASK_ID is retrieved from ECS task metadata
2. Hostname retrieved from os.Hostname()
3. Fallback to returning "UNKNOWN"

Usage with `code.justin.tv/amzn/TwitchProcessIdentifier`

```go
tpid := &identifier.ProcessIdentifier{
  // ...
  // Other fields here
  // ...
  Machine:  machineid.Resolve(),
}
```
