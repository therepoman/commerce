module code.justin.tv/chat/machineid

go 1.13

require (
	code.justin.tv/chat/ecsmetadata v1.0.0
	github.com/golangci/golangci-lint v1.25.1
	github.com/mgechev/revive v1.0.2
	github.com/stretchr/testify v1.5.1
)
