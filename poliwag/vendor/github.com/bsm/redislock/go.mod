module github.com/bsm/redislock

require (
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
)

go 1.13
