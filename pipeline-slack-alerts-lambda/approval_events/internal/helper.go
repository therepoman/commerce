package internal

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/pkg/errors"
)

const (
	envFederateAccountID   = "TWITCH_FEDERATE_ACCOUNT_ID"
	envFederateAccountRole = "TWITCH_FEDERATE_ACCOUNT_ROLE"
)

func ExtractApprovalEvent(event events.SNSEvent) (Event, error) {
	approvalEvent := Event{}

	if len(event.Records) < 1 {
		return approvalEvent, errors.New("Cannot extract approval event: SNS event in lambda contained no records")
	}

	err := json.Unmarshal([]byte(event.Records[0].SNS.Message), &approvalEvent)
	if err != nil {
		return approvalEvent, errors.Wrapf(err, "Failed to unmarshal SNS.Message into approval event.\nSNS Message: %s", event.Records[0].SNS.Message)
	}

	return approvalEvent, nil
}

func GetFederatedApprovalLink(parsedApprovalLink *url.URL) string {
	federateAccountID, federateAccountRole := os.Getenv(envFederateAccountID), os.Getenv(envFederateAccountRole)
	if federateAccountID == "" {
		panic(fmt.Sprintf("Expected environment variable \"%s\" to be set, but it was not", envFederateAccountID))
	}
	if federateAccountRole == "" {
		panic(fmt.Sprintf("Expected environment variable \"%s\" to be set, but it was not", envFederateAccountRole))
	}

	return fmt.Sprintf(
		"https://isengard.amazon.com/federate?account=%s&role=%s&destination=%s", federateAccountID, federateAccountRole,
		url.PathEscape(parsedApprovalLink.Path+"?"+parsedApprovalLink.RawQuery+"#"+parsedApprovalLink.Fragment))
}
