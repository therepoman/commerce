package internal

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws/session"

	"github.com/aws/aws-sdk-go/service/codepipeline"

	"code.justin.tv/commerce/pipeline-slack-alerts-lambda/common"

	"github.com/aws/aws-lambda-go/events"
	"github.com/pkg/errors"
)

const (
	envPipelineConsoleLink    = "TWITCH_PIPELINE_CONSOLE_LINK"
	envDeployStartedMessage   = "MESSAGE_DEPLOY_STARTED"
	envDeployFailedMessage    = "MESSAGE_DEPLOY_FAILED"
	envDeploySucceededMessage = "MESSAGE_DEPLOY_SUCCEEDED"
	envBuildFailedMessage     = "MESSAGE_Build_FAILED"
)

func ExtractPipelineEvent(event events.SNSEvent) (PipelineEvent, error) {
	pipelineEvent := PipelineEvent{}

	if len(event.Records) < 1 {
		return pipelineEvent, errors.New("Cannot extract pipeline event: SNS event in lambda contained no records")
	}

	err := json.Unmarshal([]byte(event.Records[0].SNS.Message), &pipelineEvent)
	if err != nil {
		return pipelineEvent, errors.Wrapf(err, "Failed to unmarshal SNS.Message into pipeline event.\nSNS Message: %s", event.Records[0].SNS.Message)
	}

	return pipelineEvent, nil
}

func GetPipelineExecution(pipelineEvent PipelineEvent) (*codepipeline.PipelineExecution, error) {
	s := session.Must(session.NewSession())
	pipelineClient := codepipeline.New(s)

	out, err := pipelineClient.GetPipelineExecution(&codepipeline.GetPipelineExecutionInput{
		PipelineExecutionId: &pipelineEvent.Detail.ExecutionID,
		PipelineName:        &pipelineEvent.Detail.Pipeline,
	})
	if err != nil {
		return nil, err
	}

	return out.PipelineExecution, nil
}

func GetSlackMessageFromPipelineChange(pipelineEvent PipelineEvent) *common.SlackMessage {
	execution, err := GetPipelineExecution(pipelineEvent)
	if err != nil {
		// Log, but don't die. Send the slack message without revision info.
		logrus.WithError(err).WithField("Pipeline Event", pipelineEvent).Error("Failed to call GetPipelineExecution")
	}

	consoleLink := os.Getenv(envPipelineConsoleLink)
	if consoleLink == "" {
		panic(fmt.Sprintf("Expected environment variable \"%s\" to be set, but it was not", envPipelineConsoleLink))
	}

	msgAttachment := common.Attachment{
		PreText:    "*Pipeline state changed:*",
		AuthorName: fmt.Sprintf("CodePipeline - %s", pipelineEvent.Detail.Pipeline),
		Text:       fmt.Sprintf("<%s|Click to view pipeline>", consoleLink),
		MarkdownIn: []string{"pretext", "text"},
	}

	if execution != nil && len(execution.ArtifactRevisions) > 0 {
		// Scan out first line of revision summary.
		scanner := bufio.NewScanner(strings.NewReader(*execution.ArtifactRevisions[0].RevisionSummary))
		scanner.Scan()
		summary := scanner.Text()

		msgAttachment.Fields = append(msgAttachment.Fields,
			&common.Field{
				Title: "Commit",
				Value: *execution.ArtifactRevisions[0].RevisionId,
			},
			&common.Field{
				Title: "Summary",
				Value: summary,
			},
		)
	}

	if pipelineEvent.Detail.State == ActionSucceeded {
		msgAttachment.Color = "good"
	}
	if pipelineEvent.Detail.State == ActionFailed {
		msgAttachment.Color = "danger"
	}
	if pipelineEvent.Detail.State == ActionStarted {
		msgAttachment.Color = "#05E9FF" // "indiglo" blue
	}

	if pipelineEvent.Detail.Type.Category == CategoryDeploy {
		var message string
		switch pipelineEvent.Detail.State {
		case ActionStarted:
			message = os.Getenv(envDeployStartedMessage)
			if message == "" {
				message = "Service pipeline deployment started. :underconstruction:"
			}
		case ActionFailed:
			message = os.Getenv(envDeployFailedMessage)
			if message == "" {
				message = "Service pipeline deployment failed! :thisisfine:"
			}
		case ActionSucceeded:
			message = os.Getenv(envDeploySucceededMessage)
			if message == "" {
				message = "Service pipeline deployment succeeded! :pika_sheepy:"
			}
		}
		msgAttachment.Title = fmt.Sprintf("[Stage: %s] %s", pipelineEvent.Detail.Stage, message)
	} else if pipelineEvent.Detail.Type.Category == CategoryBuild && pipelineEvent.Detail.State == ActionFailed {
		msgAttachment.Title = os.Getenv(envBuildFailedMessage)
		if msgAttachment.Title == "" {
			msgAttachment.Title = "Service pipeline build failed! :llama_sweat:"
		}
		msgAttachment.Fields = append([]*common.Field{
			{
				Short: true,
				Title: "Stage",
				Value: pipelineEvent.Detail.Stage,
			},
			{
				Short: true,
				Title: "Action",
				Value: pipelineEvent.Detail.Action,
			},
		}, msgAttachment.Fields...)
	}

	return &common.SlackMessage{
		Attachments: []common.Attachment{msgAttachment},
	}
}
