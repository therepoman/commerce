# pipeline-slack-alerts-lambda

This repo has two go projects to be used in AWS Lambda. It takes SNS messages posted by Code Pipeline and pushes messages to Slack.
Most of the settings are configured by the Lambda's environment variables.

| Environment Variable Name    | Description                | Required?             |
| ---------------------------- | -------------------------- |---------------------- |
| TWITCH_FEDERATE_ACCOUNT_ID   | AWS ID                     | YES                   |
| TWITCH_FEDERATE_ACCOUNT_ROLE | AWS role                   | YES                   |
| TWITCH_PIPELINE_CONSOLE_LINK | AWS code pipeline link     | YES                   |
| TWITCH_SLACK_CHANNEL         | slack channel              | YES                   |
| TWITCH_WEBHOOK_SECRET_ID     | slack webhook secret id    | YES                   |
| MESSAGE_DEPLOY_STARTED       | deploy started message     | NO                    |
| MESSAGE_DEPLOY_FAILED        | deploy failed message      | NO                    |
| MESSAGE_DEPLOY_SUCCEEDED     | deploy succeeded message   | NO                    |
| MESSAGE_BUILD_FAILED         | build failed message       | NO                    |

*note: Default messages are used if they are not provided via environment variables

## Build

Produce binary builds by
```
make build
```
Package them and use them in your AWS account.