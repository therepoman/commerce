package counter_test

import (
	"testing"
	"time"

	"code.justin.tv/commmerce/wexit_scanner/lib/counter"
)

func TestEmptyCount(t *testing.T) {
	c := counter.NewSqlCounter(time.Second * 3)
	if c.Len() != 0 {
		t.Fatalf("Expected Len to be zero but was %d", c.Len())
	}
}
