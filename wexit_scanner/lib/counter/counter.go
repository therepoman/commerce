package counter

import "time"

type SqlCounter struct {
}

func NewSqlCounter(roll time.Duration) *SqlCounter {
	return &SqlCounter{}
}

func (s *SqlCounter) Add(call SqlCall) {

}

func (s *SqlCounter) Len() int {
	return 0
}

type SqlCall struct {
	Table string
	Query string
	Api   string
}
