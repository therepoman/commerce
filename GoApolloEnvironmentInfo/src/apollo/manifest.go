package apollo

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

const (
	ocf string = "OCF."
)

type Manifest map[string]ManifestItem
type ManifestItem map[string]string

func (m Manifest) OpConfig(k string) ManifestItem {
	item := m[ocf+k]
	ocf := make(ManifestItem)
	for k, v := range item {
		ocf[k] = strings.Trim(v, `"`)
	}
	return ocf
}

func (m Manifest) OpConfigs() map[string]ManifestItem {
	opConfigs := make(map[string]ManifestItem)
	for k := range m {
		if strings.HasPrefix(k, ocf) {
			k = strings.TrimPrefix(k, ocf)
			opConfigs[k] = m.OpConfig(k)
		}
	}
	return opConfigs
}

// OpConfig file format
// ManifestItem:
//     key = value
//     key1 = value2
//
// ManifestItem2:
//     key = value
//     key1 = value2

func NewManifest(r io.Reader) (Manifest, error) {
	return parseManifest(r)
}

func parseManifest(r io.Reader) (Manifest, error) {
	br := bufio.NewReader(r)

	line := 0
	m := make(Manifest)
	var item ManifestItem

	for {

		line++
		s, err := br.ReadString('\n')

		if err == io.EOF {
			break
		} else if err != nil {
			return nil, fmt.Errorf("parseManifest: received an unexpected error at line %d with error message %v", line, err)
		}

		s = strings.TrimSpace(s)
		if s == "" {
			continue
		}
		if strings.HasSuffix(s, ":") {
			// Found a new ManifestItem
			name := strings.TrimSuffix(s, ":")
			item = make(ManifestItem)
			m[name] = item
			continue
		}
		// We either have a new key-value pair, or there's some error
		if item == nil {
			return nil, fmt.Errorf("parseManifest: found new key-value pair without ManifestItem at line %d", line)
		}
		p := strings.Index(s, "=")
		if p < 0 {
			return nil, fmt.Errorf("parseManifest: expected key-value pair, cannot find '=' at line %d", line)
		}
		k := strings.TrimSpace(s[:p])
		v := strings.TrimSpace(s[p+len("="):])
		item[k] = v
	}

	return m, nil
}
