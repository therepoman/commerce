---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Commerce ML Engineering Onboarding
- Welcome to the Team!
- This onboarding doc is meant to get you signed up to all accounts and installing the software we use daily.

## Accounts
- For historical reasons Twitch and Amazon has separate systems for authentication, mail, calendar etc. Twitch uses Github, Google Suite, etc. while Amazon uses Exchange, Gitfarm, etc.
- [Follow the Generic Twitch Onboarding Guide](https://git.xarth.tv/twitch/docs/blob/master/spinup/new.md)
  - You may find some sections repeated below with specific instructions to our team.
- Download Slack and join some key channels. Also Slack supports categories, I listed some categories i use. Some channels are also private so ask to gain access.
  - ML Projects: `#commerce-ml-ml`, `#commerce-ml-alerts`, `#commerce-ml-fraud`, `#commerce-ml-leads`
  - ML Infra: `#machine-learning`, `#vx-ml-infra`, `#vx-feature-store`, `#ml-feature-store-wg`
  - Teams: `#data-infrastructure`
  - Tech: `#golang`, `#python`, `#tech`, `#aws`, `#aws-announce`, `#cdk`, `#data`, `#flink`
  - Commerce: `#commerce-all`, `#commerce-data-questions`, `#commerce-post-launch-analyses`, `#commerce-prod-launch-announce`,
- Join mailing lists
  - https://groups.google.com/a/justin.tv/g/all
  - https://groups.google.com/a/justin.tv/g/commerce-all
  - https://groups.google.com/a/justin.tv/g/commerce-ml-dev
  - https://groups.google.com/a/justin.tv/g/commerce-ml
  - If you want to know what other teams at Twitch are doing, you can join all of these groups. setup rules in gmail with labels so you don't overwhelm your Inbox:
    - Data Platform: https://groups.google.com/a/justin.tv/g/data-platform-announce
    - Fulton: https://groups.google.com/a/justin.tv/g/fulton-interest
    - VX: https://groups.google.com/a/justin.tv/forum/#!topic/vx-program-update/MBnMwgZPTgY
    - Search: https://groups.google.com/a/justin.tv/forum/#!forum/search-announce
    - Creator: https://groups.google.com/a/justin.tv/forum/#!forum/creator-interest
    - Community: https://groups.google.com/a/justin.tv/forum/#!forum/community-program-update
    - Ads: https://groups.google.com/u/1/a/justin.tv/g/ads-n-promos-weekly?pli=1 / https://groups.google.com/u/1/a/justin.tv/g/ad-prod-all-team?pli=1 (requires permissions)
    - Identity: https://groups.google.com/u/1/a/justin.tv/g/identity-monthly-updates
- GitHub
  - Follow the instructions to set up your ssh keys for Github: https://docs.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
  - Gain Permissions to repositories. Ask to be added to:
    - [Commerce Org](https://git.xarth.tv/orgs/commerce/people)
    - [Science Engineering](https://git.xarth.tv/orgs/stats/teams/team-scieng)
  - Clone these repositories to your local drive. I use top level folders under ~/workspace/twitch for each package, but use whatever location you are comfortable with. Use SSH clone, example:
    - `~/workspace/twitch> git clone git@git.xarth.tv:commerce/commerce-ml-flink.git`
    - https://git.xarth.tv/commerce/commerce-ml-flink/
    - https://git.xarth.tv/stats/nominis/
    - https://git.xarth.tv/stats/mantra/
    - https://git.xarth.tv/subs/panem
- Setup LDAP
  - Add your ssh public key to the internal dashboard https://dashboard.xarth.tv/ldaptools/edit
    - click ADD KEY and paste in your public key
    - click UPDATE at the bottom
  - Ensure you have access to the following groups on: https://dashboard.xarth.tv/ldaptools/edit, request access using [The Service Desk Portal](https://twitchcorpit.service-now.com/sp) for any group you are missing:
  - `tahoe-tap-users`, `ghe-users`, `git`, `tableau-project-commerce`, `tableau-role-explorer`, `tableau-role-viewer`, `team-commerce`
- IT should have sent you a guide on how to connect to Twitch and Amazon VPN. Ensure you can connect to both.
- AWS Credentials - Twitch application services are hosted in the AWS public cloud. Services are usually spread across many accounts, usually organized by team. Temporary credentials are managed/audited using the internal Isengard tool, and you will want to make sure you have access to the following accounts in https://isengard.amazon.com. If you do not have these roles on Isengard - Check with a team member to set you up in the right groups.  These allow you to launch the AWS console using one of these roles, as well as create temporary credentials for programmatic access.
  - Join the LDAP group to automatically get access to most accounts
    - https://permissions.amazon.com/a/team/twitch-commerce-science
  - twitch-commerce-science-aws - our primary account
  - Personalization Service: `twitch-personalization-dev`, `twitch-personalization-prod`
  - Fraud: `twitch-commerce-science-fraud-beta`, `twitch-commerce-science-fraud-prod`
  - CGRE (under CGv2 folder): `twitch-subs-rec-dev`, `twitch-subs-rec-prod`
  - Use **Admin** permission when possible. For our primary account twitch-commerce-science-aws@amazon.com logging in will ask for permissions. As a tempoorary workaround you can enter a TT ticket URL. Enter this one:
    - https://tt.amazon.com/0504361868
- [JIRA Board for our team](https://jira.xarth.tv/secure/RapidBoard.jspa?rapidView=1590&view=planning&selectedIssue=CML-854&issueLimit=100)
- [Amazon Mail](https://ballard.amazon.com/owa/#path=/mail) - sometimes important mail is received there such as mandatory courses, so login to this email too.
- [Amazon Wiki](https://w.amazon.com/bin/view/Main/)
- [Amazon Internal Search](https://is.amazon.com/)

## Software
- Setup your shell. Newest Mac OS uses zsh, consider installing https://ohmyz.sh/ , (Guide)[https://stackabuse.com/pimp-my-terminal-an-introduction-to-oh-my-zsh/]
- Install CoScreen
- Add the following to your ~/.zshrc. aliases add useful shortcuts. while open- links directly open your browser at the relevant account without having to go through the isengard-console.

```zsh
export MANTRA_USE_INTERNAL_PKGS=true

####
## Aliases
alias gcml='git checkout mainline'
alias reload='source ~/.zshrc'
alias gmantra='cd ~/workspace/twitch/mantra'
alias gnominis='cd ~/workspace/twitch/nominis'
alias gpanem='cd ~/workspace/twitch/panem'
alias gdevdesktop='cd ~/workspace/twitch/dev-desktop'

alias open-commerce-fraud-beta='AWS_ACCOUNT_ID=911861225579 AWS_ROLE=Admin open_url.sh'
alias open-commerce-science='AWS_ACCOUNT_ID=348802193101 AWS_ROLE=Admin open_url.sh'
alias open-personalization-dev='AWS_ACCOUNT_ID=808393034690 AWS_ROLE=Admin open_url.sh'
alias open-personalization-prod='AWS_ACCOUNT_ID=426339712469 AWS_ROLE=Admin open_url.sh'

alias gc-='git checkout -'

### More PATHS

# uncomment after installing python@3.7 below
# export PATH="/usr/local/opt/python@3.7/bin:$PATH"

```

- Create a file under `/usr/local/bin/open_url.sh` with the following content:

```bash
#/bin/bash
open `curl -s -b ~/.midway/cookie -c ~/.midway/cookie -L -X POST --header "X-Amz-Target: IsengardService.GetConsoleAccessUrl" --header "Content-Encoding: amz-1.0" --header "Content-Type: application/json; charset=UTF-8" -d '{"AWSAccountID": "'"$AWS_ACCOUNT_ID"'", "IAMRoleName" : "'"$AWS_ROLE"'"}' https://isengard-service.amazon.com | jq -r '.url'`
```

- Make it executable using `chmod u+x /usr/local/bin/open_url.sh`. We will be using it with the aliases defined above to speed up accessing different AWS accounts.

- Follow the Builder Tools doc: [Setting Up Your Dev Environment](https://docs.fulton.twitch.a2z.com/docs/getting_started/setting_up_dev_env.html#install-builder-toolbox)
- Setup Teleport Bastion
  - Follow the steps: https://wiki.xarth.tv/display/SEC/Teleport+Bastion#TeleportBastion-macOS
  - Support Slack Channel: `#teleport_bastion`
- Create the following [AWS credential and configuration files](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) if they don't exist and with the following content:
- `~/.aws/config`

```
  [default]
  region=us-west-2
  output=json

  [profile twitch-commerce-science-aws]
  region = us-west-2
  output = json
```
- `~/.aws/credentials`

```
  [twitch-commerce-science-aws]
  region=us-west-2
  credential_process=ada credentials print --account 348802193101 --role Admin --sim 'https://tt.amazon.com/0504361868'
```
- Install [Homebrew](https://brew.sh/) and with it install the following packages:
  - [Go](https://golang.org/): `brew install go`
  - [Node](https://nodejs.org/en/): `brew install node`
  - [Python - Python Dependency Management](https://www.python.org/): We use version 3.7 for now, so: `brew install python@3.7`
  - [Poetry](https://python-poetry.org/): `brew install poetry`
  - [Java](): `brew install openjdk@11`
  - [Scala sbt](https://www.scala-sbt.org/): `brew install sbt`
  - [Docker](https://www.docker.com/): `brew cask install docker`
  - [AWS CLI](https://aws.amazon.com/cli/): `brew install awscli`
  - [AWS Session Manager Plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html): `brew install session-manager-plugin`
  - [jq](https://stedolan.github.io/jq/): `brew install jq`
  - [iterm2](https://iterm2.com/): `brew cask install iterm2`. Feel free to use this instead of the regular `Terminal`
- Install Command Line Tools:
  - `sudo xcode-select --install`

## Your daily workflow
- You will find yourself doing these every morning:
- Login to Amazon VPN
- Authenticate on the command line using `mwinit`
- Go to [JIRA](https://jira.twitch.com/) to go through Twitch LDAP Login.
- Go to [Isengard](https://isengard.amazon.com/console-access) to go through AWS Midway Login.

## Python
- Here is how I recommend to setup Python packages. we will use `nominis` as an example, but these steps can be applied to any python project:

```bash
   > cd ~/workspace/twitch/nominis
```

- Create a virtual environment. [Learn More about Virtual Environments](https://towardsdatascience.com/virtual-environments-104c62d48c54):
```
    > python3.7 -m venv env
    > source env/bin/activate
```
The 2nd command activates the virtual environment after its created. if you use ohmyzsh you should see indicator that this environment is active. Be sure to activate an environment before installing packages.

- Ensure you are using internal.justin.tv to get access to internal packages. Also need to be on Amazon VPN:

```bash
    > pip3 config set global.index-url https://pypi.internal.justin.tv/simple
```

- Install Packages

```
pip3 install -r requirements.txt
pip3 install -r requirements-dev.txt
```

- TODO: How to automatically change virtual env when switching folders.

## Scala
- [Short introduction to Scala](https://itnext.io/a-10-minute-introduction-to-scala-d1fed19eb74c)
- For the Scala example we will use `~/workspace/twitch/commerce-ml-flink`. This is a repository that has multiple packages, one in each folder. `Makefile` commands allow us to build each package.
- If you are not familiar with Makefiles, read a [short introduction](https://opensource.com/article/18/8/what-how-makefile)
- Build the `mockingbird-scala` package using `make build-flink-job`

## Node
- [Node.JS - Introduction](https://codeburst.io/the-only-nodejs-introduction-youll-ever-need-d969a47ef219)
- We will use the same `commerce-ml-flink` repository since it also contains a CDK package.
- Read a bit about [AWS CDK](https://aws.amazon.com/cdk/) and at some point take the [CDK workshop](https://cdkworkshop.com/) for [Typescript](https://cdkworkshop.com/20-typescript.html). Oh, and this is what [Typescript](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html) is.
- Ensure you are connected to the VPN, `mwinit` if you didn't already, and deploy the cloudwatch staging CDK repository by typing:

```zsh
> make deploy_cloudwatch_staging
```

## Go
- [Ready, set, Go (lang)](https://golang.org/)! - Many of Twitch's services are written in Go, so if you're new to the language, spend some time your first week diving into the tour and getting up to speed:
  - [Getting Started With Go](https://golang.org/doc/install)
  - [Go by Example](https://gobyexample.com/)
  - TODO: Ensure GOPATH is set properly
- We will use the same `commerce-ml-flink` repository since it also contains two `AWS Lambda` packges that are built on top of Go
- Build the lambda packages package using `make build-lambda`

## Brazil / Amazon Systems
- Most of the information above was around Twitch systems - which is Github based. Amazon has an internal code repository and tooling that is separate, but that twitch is increasingly migrating to.
- The main names are: Github = Gitfarm (https://code.amazon.com/), instead of compiling using native tooling there's a build system called [Brazil](https://builderhub.corp.amazon.com/tools/brazil/), and a CI/CD called Amazon Pipelines (https://pipelines.amazon.com/pipelines/TwitchPersonalization)
- Also instead of having a single repository per project, there are multiple repositories, each one deploying a specific piece of the service.
- On top amazon uses its own permission system (Teams), classification (CTI), oncall (escalation policies), instead of jira there's sim tickets, and so on.
- There is a lot more ramp up to get familiar with this toolset since it's different than what you may be familiar with, and at the moment there is not an easy onboarding guide - although one should be available soon.
- Twitch added standard support for Go to the Brazil build system and created a project bootstrap system - named Fulton (https://docs.fulton.twitch.a2z.com/index.html).
- Personalization Service
  - https://code.amazon.com/packages/TwitchPersonalization/
- CGRE:
  - https://code.amazon.com/packages/TwitchCGRE/trees/mainline
  - https://code.amazon.com/packages/TwitchCGRECDK/trees/mainline
  - https://code.amazon.com/packages/TwitchCGRETwirp/trees/mainline
  - https://code.amazon.com/packages/TwitchCGRESchema/trees/mainline
- A bootcamp should be available in early Q4 (you can see the planned curiculum: https://docs.google.com/document/d/1IeHz-oTneXF7kOVpa-i4bfOEqhyoP7y4n3VRmur8kn8/edit) so for now this is a FYI to be aware that this exists and if you hear any of those terms floating around - you know what they are talking about.

## Resources
- [Twitch Wiki](https://wiki.xarth.tv/)
- [Twitch US New Hires](https://twitchpeople.xarth.tv/hc/en-us/categories/115000707347-Welcome-to-Twitch-)
- [Twitch Engineer Spin-Up Guide]()
- [Twitch Data](https://data.xarth.tv/)
- [Project Fulton](https://docs.fulton.twitch.a2z.com/)
- [BuilderHub](https://builderhub.corp.amazon.com/)
- [Amazon Broadcast - Learn about anything in AWS](https://broadcast.amazon.com/)
- [Commerce ML Google Drive](https://drive.google.com/drive/folders/1TUtx5jPvv7YhW1KTZRk66xkhxrIa_KNQ)
- [Git](https://rogerdudler.github.io/git-guide/)
- [Glossary of terms](https://wiki.xarth.tv/display/ENG/Glossary)
- [Twic](https://client.twic.ai/explore) - Twitch wellness program
- Request software licenses through https://itmarketplace.corp.amazon.com/
