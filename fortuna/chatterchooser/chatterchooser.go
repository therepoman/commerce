package chatterchooser

import (
	. "math"
	"math/rand"

	"time"

	log "github.com/sirupsen/logrus"
)

type CheerbombParameters struct {
	Rng           *rand.Rand
	CheerAmount   int
	TotalChatters int
	NumberOfGifts int
	// More stuff in future if needed
}

type ChatterChooser interface {
	Choose(chatters []string, totalChatters int, numChattersToChoose int, benefactor string) []string
}
type void interface{}

func GetBlockListLookup() map[string]void {
	var member void
	return map[string]void{
		"12826":     member, // Twitch
		"149747285": member, // TwitchPresents
		"238790689": member, // twitchpresents1
		"131784680": member, // TwitchPrime
		"182953678": member, // TwitchPresents2
		"89340006":  member, // TwitchTW
		"197886470": member, // TwitchRivals
		"103368190": member, // TwitchKR
		"133151915": member, // TwitchJP
		"129232627": member, // TwitchTH
		"144300085": member, // TwitchTW2
		"49931601":  member, // TwitchOffice
		"141981764": member, // TwitchDev
		"136930911": member, // TwitchEsports
		"144403200": member, // TwitchDE
		"46325627":  member, // TwitchNotify
		"122417962": member, // twitchchatanalytics
		"157264230": member, // Slocool
		"247101509": member, // letsseehowmanygiftsiget
		"126732100": member, // VirgoProZ
		"227202086": member, // v_and_k
		"219353269": member, // LANFusion
		"198236322": member, // electricallongboard
		"114583109": member, // electricalskateboard
		"236791797": member, // decafsmurf
		"101302091": member, // Cogwhistle
		"173596651": member, // Bananennanen
		"192370723": member, // Bobmob_Bot
		"102212771": member, // BinxTVBot
		"61337518":  member, // DominaBot
		"1078":      member, // Chatbot
		"175044498": member, // navakibot
		"242355655": member, // woppes
		"194921579": member, // thronezilla
		"59172433":  member, // skinnyseahorse
		"231306843": member, // Follow4Follow
		"248741683": member, // 0_AppleBadApple_0
		"239797601": member, // sldkfjoeuw89e8r7
		"87528259":  member, // dqfan1
		"44216605":  member, // ResidentSpeedy
		"44114423":  member, // quoooo
		"84830064":  member, // RobertJan
		"24900234":  member, // BloodLustr
		"205052173": member, // FreddyyBOT
		"25391134":  member, // PreFiXAUT
		"90012695":  member, // vaceDE
		"129201303": member, // iamalovelypenguin
		"233697760": member, // jongfer
		"243547061": member, // merssene557
		"247045963": member, // shrivelling
		"42983298":  member, // Trollbear666
		"11819690":  member, // Jugachi
		"37918013":  member, // Axaruz
		"15445814":  member, // Drast
		"37972249":  member, // IrregularJinny
		"59024815":  member, // DoctorWigglez
		"87024697":  member, // deevante
		"26964566":  member, // Lordmau5
		"50963865":  member, // angelachan
		"103201752": member, // MismanagedMolotov
		"40903253":  member, // Ambl3r
		"32161641":  member, // lllllllOYDE_
		"96445156":  member, // meesh_
		"26764929":  member, // Prom3theu5
		"274598607": member, // AnAnonymousGifter
	}
}

func GetBlockList() []string {
	var blocklist []string
	for k := range GetBlockListLookup() {
		blocklist = append(blocklist, k)
	}
	return blocklist
}

func GetSecretSantaRecipientCount(totalChatters int) int {
	chosenMin := 3
	chosenMax := 60
	scalingFactor := 7.5297
	offset := 22.403

	// Specially synthesized formula to divine the chosen ones (min: 3, max: 60)
	chosenChatters := int(Min(Max(float64(chosenMin), Floor(scalingFactor*Log(float64(totalChatters))-offset)), float64(chosenMax)))

	return chosenChatters
}

func GetOwlCheerbombRecipientCount(input CheerbombParameters) int {
	chosenMin := 35.0
	chosenMax := 1000.0

	// Equation based on product spec here: https://docs.google.com/document/d/1JButvg1qiSyNS2ZY121QYv7Yb5W_-Ts0TKWWKjbL_j8/
	chosen := (0.101538462*float64(input.CheerAmount) - 15.38461538) * (input.Rng.Float64()*0.1 + 0.95)

	return int(Max(chosenMin, Min(chosenMax, chosen)))
}

func GetHgcCheerbombRecipientCount(input CheerbombParameters) int {
	chosenMin := 8.0

	// Scales from 8 min recipients, where 1000 is the min cheer amount to trigger the cheerbomb
	chosenNum := Ceil((float64(input.CheerAmount) / 1000.0) * chosenMin)

	return int(Max(chosenMin, chosenNum))
}

// GetOwl2019MegaRecipientCount returns the number of rewards gifted based upon the amount of bits cheered for OWL 2019 Mega Cheers.
// The user can rewards from multiple tiers if they have cheered enough. Ex: Cheer amount 5300 means gifting the amount of rewards for
// cheering 5000 (300 gifts) and gifting the amount for cheering 300 (10 gifts), so the user will gift 310 gifts in total.
func GetOwl2019MegaRecipientCount(input CheerbombParameters) int {
	type Tier struct {
		cheerAmount int
		giftAmount  int
	}

	tiers := []Tier{
		{
			cheerAmount: 5000,
			giftAmount:  300,
		},
		{
			cheerAmount: 1000,
			giftAmount:  50,
		},
		{
			cheerAmount: 500,
			giftAmount:  20,
		},
		{
			cheerAmount: 300,
			giftAmount:  10,
		},
	}

	chosenChattersAmount := 0
	cheerAmount := input.CheerAmount

	for _, tier := range tiers {
		chosenChattersAmount += (cheerAmount / tier.cheerAmount) * tier.giftAmount
		cheerAmount = cheerAmount % tier.cheerAmount
	}

	return chosenChattersAmount
}

func GetSeasonalRecipientCount(input CheerbombParameters) int {
	type Tier struct {
		cheerAmount int
		giftAmount  int
	}

	tiers := []Tier{
		{
			cheerAmount: 5000,
			giftAmount:  150,
		},
		{
			cheerAmount: 1000,
			giftAmount:  25,
		},
		{
			cheerAmount: 500,
			giftAmount:  10,
		},
		{
			cheerAmount: 200,
			giftAmount:  3,
		},
	}

	chosenChattersAmount := 0
	cheerAmount := input.CheerAmount

	for _, tier := range tiers {
		chosenChattersAmount += (cheerAmount / tier.cheerAmount) * tier.giftAmount
		cheerAmount = cheerAmount % tier.cheerAmount
	}

	return chosenChattersAmount
}

func GetHoliday2019RecipientCount(input CheerbombParameters) int {
	// Return precomputed number of gifts
	return input.NumberOfGifts
}

type RandomChatterChooser struct {
	Rng *rand.Rand
}

var _ ChatterChooser = &RandomChatterChooser{}

func NewRandomChatterChooser() *RandomChatterChooser {
	return &RandomChatterChooser{
		Rng: rand.New(rand.NewSource(time.Now().UTC().UnixNano())),
	}
}

func (r *RandomChatterChooser) Choose(
	chatters []string, totalChatters int, chattersToChoose int, benefactor string) []string {

	// filter bots from chat list
	// TODO: If we end up having a service that takes care of storing and updating bots, migrate this part to call that.
	chatters = r.Filter(chatters, GetBlockListLookup())

	if len(chatters) != totalChatters {
		log.WithFields(log.Fields{
			"totalChatters": totalChatters,
			"chattersLen":   len(chatters),
			"benefactor":    benefactor,
		}).Warn("Received a Chatters totalChatters count that did not match combined length of chatter lists")
	}

	if len(chatters) == 0 {
		return []string{}
	}

	randomIndices := r.Rng.Perm(len(chatters))
	var chosenChatterLogins []string

	for _, randIx := range randomIndices {
		if len(chosenChatterLogins) >= chattersToChoose {
			break
		}

		chosenChatterLogin := chatters[randIx]
		if chosenChatterLogin != benefactor {
			chosenChatterLogins = append(chosenChatterLogins, chosenChatterLogin)
		}
	}

	return chosenChatterLogins
}

func (r *RandomChatterChooser) Filter(allChatters []string, blockListLookup map[string]void) []string {
	var filteredChatters []string
	for _, chatter := range allChatters {
		if _, isBlocked := blockListLookup[chatter]; !isBlocked {
			filteredChatters = append(filteredChatters, chatter)
		}
	}
	return filteredChatters
}
