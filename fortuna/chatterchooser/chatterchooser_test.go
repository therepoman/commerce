package chatterchooser

import (
	"fmt"
	"strconv"
	"testing"

	"math/rand"

	. "github.com/smartystreets/goconvey/convey"
)

/*

	The number of chosen based on the number of viewers

	Viewers | Chosen
	----------------
	10      | 3
	50      | 7
	100     | 12
	500     | 24
	1000    | 29
	2000    | 34
	5000    | 41
	10000   | 46
	20000   | 52

*/

func TestChatters(t *testing.T) {
	Convey("Test Filtering Chatters", t, func() {
		blockListLookup := GetBlockListLookup()
		blockList := GetBlockList()
		legitChatter := "some-user"
		chattersRaw := []string{
			legitChatter,
			blockList[0],
			blockList[1],
			blockList[2],
		}
		chatterChooser := &RandomChatterChooser{}
		chattersFiltered := chatterChooser.Filter(chattersRaw, blockListLookup)
		So(len(chattersFiltered), ShouldEqual, 1)
		So(chattersFiltered[0], ShouldEqual, legitChatter)
	})
	Convey("Test Secret Santa Chatters", t, func() {
		var viewers []string
		benefactor := "richard"
		luckyUser := "ryan"

		chatterChooser := &RandomChatterChooser{
			Rng: rand.New(rand.NewSource(1337)),
		}

		Convey("with one viewer that is the benefactor", func() {
			chatters := []string{
				benefactor,
			}

			numToChoose := GetSecretSantaRecipientCount(1)
			chosenOnes := chatterChooser.Choose(chatters, 1, numToChoose, benefactor)
			So(len(chosenOnes), ShouldEqual, 0)
		})

		Convey("with less than minimum viewers, should not include benefactor", func() {
			numViewers := 2
			chatters := []string{
				benefactor,
				luckyUser,
			}

			numToChoose := GetSecretSantaRecipientCount(numViewers)
			chosenOnes := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
			So(len(chosenOnes), ShouldEqual, 1)
		})

		Convey("with same as minimum viewers, should not include benefactor", func() {
			numViewers := 5
			viewers = manyViewers(luckyUser, numViewers-1)
			chatters := append(viewers, benefactor)

			numToChoose := GetSecretSantaRecipientCount(numViewers)

			chosenOnes := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
			fmt.Println(chosenOnes)
			fmt.Println(len(chosenOnes))
			So(len(chosenOnes), ShouldEqual, 3)
		})

		Convey("with more than minimum viewers, should not include benefactor", func() {
			numViewers := 4
			viewers = manyViewers(luckyUser, numViewers-1)
			chatters := append(viewers, benefactor)

			numToChoose := GetSecretSantaRecipientCount(numViewers)

			chosenOnes := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
			So(len(chosenOnes), ShouldEqual, 3)
		})

		Convey("with much more than minimum viewers, should not include benefactor", func() {
			numViewers := 100
			viewers = manyViewers(luckyUser, numViewers-1)
			chatters := append(viewers, benefactor)

			numToChoose := GetSecretSantaRecipientCount(numViewers)

			chosenOnes := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
			So(len(chosenOnes), ShouldEqual, 12)
		})

		Convey("with much much more than minimum viewers, should not include benefactor", func() {
			numViewers := 1000
			viewers = manyViewers(luckyUser, numViewers-1)
			chatters := append(viewers, benefactor)

			numToChoose := GetSecretSantaRecipientCount(numViewers)

			chosenOnes := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
			So(len(chosenOnes), ShouldEqual, 29)
		})

		Convey("should return same list when choosing multiple times", func() {
			numViewers := 1000
			viewers = manyViewers(luckyUser, numViewers-1)
			chatters := append(viewers, benefactor)

			numToChoose := GetSecretSantaRecipientCount(numViewers)

			chosenOnes := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
			So(len(chosenOnes), ShouldEqual, 29)

			timesToHandle := 10
			for i := 0; i < timesToHandle; i++ {
				chatterChooser := &RandomChatterChooser{
					Rng: rand.New(rand.NewSource(1337)),
				}
				chosen := chatterChooser.Choose(chatters, numViewers, numToChoose, benefactor)
				for index, user := range chosenOnes {
					So(user, ShouldEqual, chosen[index])
				}
			}
		})
	})

	Convey("Test Owl 2019 Recipient Count", t, func() {
		Convey("with 1 bit cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 1,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 0)
		})

		Convey("with 300 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 300,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 10)
		})

		Convey("with 400 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 300,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 10)
		})

		Convey("with 500 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 500,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 20)
		})

		Convey("with 700 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 700,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 20)
		})

		Convey("with 800 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 800,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 30)
		})

		Convey("with 1000 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 1000,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 50)
		})

		Convey("with 5000 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 5000,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 300)
		})

		Convey("with 6900 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 6900,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 380)
		})

		Convey("with 10000 bits cheered", func() {
			input := CheerbombParameters{
				CheerAmount: 10000,
			}

			numToChoose := GetOwl2019MegaRecipientCount(input)
			So(numToChoose, ShouldEqual, 600)
		})
	})
}

func manyViewers(user string, number int) []string {
	viewers := make([]string, number)
	for i := 0; i < number; i++ {
		viewers[i] = user + strconv.Itoa(i)
	}
	return viewers
}
