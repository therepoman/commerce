// +build integration

package integration

import (
	"context"
	"testing"
	"time"

	mako_client "code.justin.tv/commerce/mako/twirp"

	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numAttempts              = 5
	baseDelayBetweenAttempts = 2 * time.Second
	defaultTimeout           = 5 * time.Second
)

func TestFulfiller(t *testing.T) {
	teardownEmoteEntitlements()

	Convey("Test EntitleTrigger", t, func() {
		randomTrigger, err := fulfiller.Utils.GetTriggerByID(testRandomTriggerID)
		So(err, ShouldBeNil)

		Convey("when entitled as a benefactor", func() {
			claimableRewards, claimedRewards, err := fulfiller.EntitleTrigger(randomTrigger, testUserID, true, 0)
			So(err, ShouldBeNil)
			So(len(claimableRewards), ShouldEqual, 3)
			So(len(claimedRewards), ShouldEqual, 3)

			for _, reward := range claimedRewards {
				So(isCreatedRecordForTriggerReward(testUserID, randomTrigger.ID, reward.ID, reward.Type, randomTrigger.Domain, true, 4, 1), ShouldBeTrue)
			}
			CheckEntitlementCount(testUserID, randomTrigger.Domain, 3)

			for _, reward := range claimedRewards {
				CleanupClaimTriggerReward(testUserID, randomTrigger.ID, reward.ID, reward.Type, randomTrigger.Domain)
			}
		})

		Convey("when entitled as a recipient", func() {
			claimableRewards, claimedRewards, err := fulfiller.EntitleTrigger(randomTrigger, testUserID, false, 0)
			So(err, ShouldBeNil)
			So(len(claimableRewards), ShouldEqual, 1)
			So(len(claimedRewards), ShouldEqual, 1)

			for _, reward := range claimedRewards {
				So(isCreatedRecordForTriggerReward(testUserID, randomTrigger.ID, reward.ID, reward.Type, randomTrigger.Domain, false, 1, 1), ShouldBeTrue)
			}
			CheckEntitlementCount(testUserID, randomTrigger.Domain, 1)

			for _, reward := range claimedRewards {
				CleanupClaimTriggerReward(testUserID, randomTrigger.ID, reward.ID, reward.Type, randomTrigger.Domain)
			}
		})
	})
}

func isCreatedRecordForTriggerReward(userID, triggerID, rewardID, rewardType, domain string, isBenefactor bool, claimableCount int, rewardsCount int) bool {
	if !checkEmoteEntitlementRecord(userID, domain, rewardID) {
		return false
	}

	// Check Claimable Reward
	claimableRewards, err := fulfiller.DAOs.ClaimableRewardDAO.GetUserRewardsForDomain(userID, domain)
	if err != nil || len(claimableRewards) != claimableCount || claimableRewards[0].IsGift != !isBenefactor {
		return false
	}

	// Check Claimed Reward Audit
	rewards, err := fulfiller.DAOs.RewardDAO.GetUserRewards(userID, rewardType, rewardID)
	So(err, ShouldBeNil)
	if isBenefactor && len(rewards) != rewardsCount {
		return false
	} else if len(rewards) != 1 {
		return false
	}

	return true
}

func CheckEntitlementCount(userID, domain string, num int) {
	ctx, _ := context.WithTimeout(context.Background(), defaultTimeout)

	// fetch user emote set entitlements
	request := &mako_client.GetEmoteEntitlementsRequest{
		OwnerId:  userID,
		OriginId: domain,
		Group:    mako_client.EmoteGroup_Rewards,
	}

	resp, err := fulfiller.Clients.MakoClient.GetEntitlements(request, ctx)
	if err != nil {
		log.WithError(err)
	}

	So(err, ShouldBeNil)
	So(len(resp.Entitlements), ShouldEqual, num)
}

func CheckCreatedRecordsForTriggerReward(userID, triggerID, rewardID, rewardType, domain string, isBenefactor bool) {
	So(checkEmoteEntitlementRecord(userID, domain, rewardID), ShouldBeTrue)

	//Check Claimable Reward
	claimableRewards, err := fulfiller.DAOs.ClaimableRewardDAO.GetUserRewardsForDomain(userID, domain)
	So(err, ShouldBeNil)
	So(len(claimableRewards), ShouldEqual, 1)
	So(claimableRewards[0].IsGift, ShouldEqual, !isBenefactor)

	// Check Claimed Reward Audit
	rewards, err := fulfiller.DAOs.RewardDAO.GetUserRewards(userID, rewardType, rewardID)
	So(err, ShouldBeNil)
	if isBenefactor {
		So(len(rewards), ShouldEqual, 3)
	} else {
		So(len(rewards), ShouldEqual, 1)
	}
}

func CleanupClaimTriggerReward(userID, triggerID, rewardID, rewardType, domain string) {
	// Clean up Mako
	now := time.Now().UTC()
	start, _ := ptypes.TimestampProto(now)
	_, err := fulfiller.Clients.MakoClient.DeleteEmoteEntitlements(&mako_client.DeleteEmoteGroupEntitlementsRequest{
		Entitlements: []*mako_client.EmoteGroupEntitlement{
			{
				OwnerId:  userID,
				ItemId:   rewardID, //not even used, but needed
				OriginId: domain,
				Group:    mako_client.EmoteGroup_Rewards,
				GroupId:  rewardID, // this is actually being used as item id
				Start:    start,    //not even used, but needed
				End: &mako_client.InfiniteTime{ //not even used, but needed
					IsInfinite: true,
				},
			},
		},
	})
	So(err, ShouldBeNil)
	emotes, err := fulfiller.Clients.MakoClient.GetEntitlementMapByDomain(domain, userID)
	So(err, ShouldBeNil)
	So(emotes[rewardID], ShouldEqual, 0)

	// Clean up ClaimableRewards
	err = fulfiller.DAOs.ClaimableRewardDAO.DeleteClaimableRewardsForUserAndDomain(userID, domain)
	So(err, ShouldBeNil)
	claimableRewards, err := fulfiller.DAOs.ClaimableRewardDAO.GetUserRewardsForDomain(userID, domain)
	So(err, ShouldBeNil)
	So(len(claimableRewards), ShouldEqual, 0)

	// Clean up Rewards
	err = fulfiller.DAOs.RewardDAO.DeleteUserRewards(userID, rewardType, rewardID)
	rewards, err := fulfiller.DAOs.RewardDAO.GetUserRewards(userID, rewardType, rewardID)
	So(err, ShouldBeNil)
	So(len(rewards), ShouldEqual, 0)
}

// Useful when a test went wrong and you just want to clean up without checking (simply run this instead of CleanupClaimTriggerReward)
func cleanUpWithoutChecking(userID, triggerID, rewardID, rewardType, domain string) {
	now := time.Now().UTC()
	start, _ := ptypes.TimestampProto(now)
	fulfiller.Clients.MakoClient.DeleteEmoteEntitlements(&mako_client.DeleteEmoteGroupEntitlementsRequest{
		Entitlements: []*mako_client.EmoteGroupEntitlement{
			{
				OwnerId:  userID,
				ItemId:   rewardID, //not even used, but needed
				OriginId: domain,
				Group:    mako_client.EmoteGroup_Rewards,
				GroupId:  rewardID, // this is actually being used as item id
				Start:    start,    //not even used, but needed
				End: &mako_client.InfiniteTime{ //not even used, but needed
					IsInfinite: true,
				},
			},
		},
	})
	fulfiller.DAOs.ClaimableRewardDAO.DeleteClaimableRewardsForUserAndDomain(userID, domain)
	fulfiller.DAOs.RewardDAO.DeleteUserRewards(userID, rewardType, rewardID)
}

func checkEmoteEntitlementRecord(userID string, domain string, rewardID string) bool {
	if userID == "" {
		return true
	}

	for attempt := 1; attempt <= numAttempts; attempt++ {
		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)

		// fetch user emote set entitlements
		request := &mako_client.GetEmoteEntitlementsRequest{
			OwnerId:  userID,
			OriginId: domain,
			Group:    mako_client.EmoteGroup_Rewards,
		}

		resp, err := fulfiller.Clients.MakoClient.GetEntitlements(request, ctx)
		if err != nil {
			log.WithError(err)
		}

		for _, entitlement := range resp.Entitlements {
			if entitlement.EmoteId == rewardID {
				return true
			}
		}

		cancel()
		if attempt < numAttempts {
			log.Infof("GetEntitlementMapByDomain did not return the expected results after %d attempts, trying again...", attempt)
			time.Sleep(time.Duration(attempt) * baseDelayBetweenAttempts)
		}
	}

	log.Errorf("Could not find %s", rewardID)
	return false
}
