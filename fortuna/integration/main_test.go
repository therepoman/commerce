// +build integration

package integration

import (
	"os"
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/s3"
	"code.justin.tv/commerce/fortuna/sqs"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/sqs/workers"
	"code.justin.tv/commerce/fortuna/utils/setup"
	"code.justin.tv/commerce/mako/twirp"

	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

const (
	shutdownTimeout         = 5 * time.Second
	testUserID              = "234092104" // login: testfortuna
	testChannelID           = "235068075" // login: testfortuna2
	testRandomTriggerID     = "integration.test.trigger.random"
	testRandomTriggerDomain = "integration.test"
	testRewardID1           = "integration.test.reward1"
)

var (
	delegator         sqs.Delegator
	fulfillmentWorker *workers.MegaCheerGiftFulfillmentEventsWorker

	fortunaConfig     *config.Configuration
	campaignPoller    *campaign.Poller
	daos              *dynamo.DAOs
	downstreamClients *clients.Clients

	fulfiller *rewards.Fulfiller
)

func TestMain(m *testing.M) {
	var err error
	fortunaConfig, err = setup.SetupFortunaConfiguration()
	fortunaConfig.UseDynamoDBForCampaigns = false
	if err != nil {
		log.Fatalf("Failed to initialize fortuna config: %s", err)
	}

	s3Client := s3.NewFromDefaultConfig()

	campaignPoller, err = setup.SetupCampaignPoller(fortunaConfig, s3Client)
	if err != nil {
		log.Fatalf("Failed to initialize campaign config: %s", err)
	}

	stats, err := setup.SetupStats(fortunaConfig)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	downstreamClients, err = clients.SetupClients(fortunaConfig, campaignPoller.CampaignConfig, stats, s3Client)
	if err != nil {
		log.Fatalf("Failed to initialize clients: %s", err)
	}

	daos, err = dynamo.SetupDAOs(fortunaConfig)
	if err != nil {
		log.Fatalf("Failed to initialize DAOs: %s", err)
	}

	selector := rewards.NewSelector(campaignPoller.CampaignConfig)
	utils := &rewards.Utils{
		CampaignConfig: campaignPoller.CampaignConfig,
		CampaignHelper: daos,
		Config:         fortunaConfig,
		Clients:        downstreamClients,
		DAOs:           daos,
	}

	fulfiller = &rewards.Fulfiller{
		Config:          fortunaConfig,
		CampaignConfig:  campaignPoller.CampaignConfig,
		Clients:         downstreamClients,
		RewardsSelector: selector,
		DAOs:            daos,
		Utils:           utils,
		Stats:           stats,
	}

	notifier := &rewards.Notifier{
		Clients: downstreamClients,
	}

	handlerInput := handler.NewEventHandlersInput{
		Clients:        downstreamClients,
		FortunaConfig:  fortunaConfig,
		CampaignConfig: campaignPoller.CampaignConfig,
		Fulfiller:      fulfiller,
		Utils:          utils,
	}

	triggerHandlers := handler.NewTriggerHandlers(handlerInput)

	options := &setup.FortunaSetupOptions{
		FortunaConfig:   fortunaConfig,
		CampaignConfig:  campaignPoller.CampaignConfig,
		Stats:           stats,
		Clients:         downstreamClients,
		Daos:            daos,
		TriggerHandlers: triggerHandlers,
		Fulfiller:       fulfiller,
		RewardsSelector: selector,
		Utils:           utils,
		Notifier:        notifier,
	}

	delegator = sqs.NewDelegator(options.Clients, options.FortunaConfig, options.Stats, options.TriggerHandlers, options.Utils)

	setupSQSWorkers(options)

	teardownEmoteEntitlements()
	testSuiteResult := m.Run()
	teardownEmoteEntitlements()

	log.Info("Shutting down campaign poller...")
	campaignPoller.Shutdown()

	log.Info("Shutdown complete.")

	os.Exit(testSuiteResult)
}

func setupSQSWorkers(options *setup.FortunaSetupOptions) {
	fulfillmentWorker = &workers.MegaCheerGiftFulfillmentEventsWorker{
		Clients:   options.Clients,
		Fulfiller: options.Fulfiller,
		Notifier:  options.Notifier,
		Utils:     options.Utils,
		Stats:     options.Stats,
	}
}

// teardownEmoteEntitlements deletes all Mako entitlements created for the test user
func teardownEmoteEntitlements() {
	res := getUserEmoteEntitlements(testUserID)

	deletes := []*mako_client.EmoteGroupEntitlement{}

	if len(res.Entitlements) == 0 {
		return
	}
	now := time.Now().UTC()
	start, _ := ptypes.TimestampProto(now)

	for _, entitlement := range res.Entitlements {
		deletes = append(deletes, &mako_client.EmoteGroupEntitlement{
			OwnerId:  entitlement.OwnerId,
			ItemId:   entitlement.EmoteId,
			OriginId: testRandomTriggerDomain,
			Group:    mako_client.EmoteGroup_Rewards,
			GroupId:  entitlement.EmoteId, // this is actually being used as item id
			Start:    start,               //not even used, but needed
			End: &mako_client.InfiniteTime{ //not even used, but needed
				IsInfinite: true,
			},
		})
	}
	_, err := downstreamClients.MakoClient.DeleteEmoteEntitlements(&mako_client.DeleteEmoteGroupEntitlementsRequest{Entitlements: deletes})
	if err != nil {
		log.Fatalf("Failed to delete test user's Mako entitlements: %s", err)
	}
}

func getUserEmoteEntitlements(userID string) *mako_client.GetEmoteEntitlementsResponse {
	res, err := downstreamClients.MakoClient.GetEntitledEmotes(userID, "")
	if err != nil {
		log.Fatalf("Failed to retrieve test user's Mako entitlements: %s", err)
	}
	return res
}
