#!/bin/bash
echo "Setting up custom logging"

eval `/opt/elasticbeanstalk/containerfiles/support/generate_env`
sed -i -e "s/__ENVIRONMENT_NAME__/$ENVIRONMENT/" /tmp/app/.ebextensions/awslogs-custom-logs.conf
cp /tmp/app/.ebextensions/awslogs-custom-logs.conf /etc/awslogs/config/