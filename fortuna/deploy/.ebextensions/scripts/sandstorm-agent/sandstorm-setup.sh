 #!/bin/sh

echo "Downloading Sandstorm-Agent"
yum-config-manager -y --enable artifactory
yum clean all
yum install -y sandstorm-agent || echo "failed to install sandstorm-agent"

eval  $(/opt/elasticbeanstalk/containerfiles/support/generate_env | sed 's/$/;/')

# Replaces TWITCH_ENV with $TWITCH_ENV environment variable
sed -i "s/{TWITCH_ENV}/${TWITCH_ENV}/g" /etc/sandstorm-agent/templates.d/*
sed -i "s/{TWITCH_ENV}/$TWITCH_ENV/g" /etc/sandstorm-agent/conf.d/*.conf
sed -i "s/{ENVIRONMENT}/${ENVIRONMENT}/g" /etc/sandstorm-agent/templates.d/*
sed -i "s/{ENVIRONMENT}/$ENVIRONMENT/g" /etc/sandstorm-agent/conf.d/*.conf

# Replaces TWITCH_ROLE with $TWITCH_ROLE environment variable
# need to use | here since roles contain "/"
sed -i "s|{TWITCH_ROLE}|${TWITCH_ROLE}|g" /etc/sandstorm-agent/templates.d/*
sed -i "s|{TWITCH_ROLE}|$TWITCH_ROLE|g" /etc/sandstorm-agent/conf.d/*.conf

echo "Setting Up Monit"

mkdir -p /var/lib/monit

monit
sleep 5
monit reload
sleep 5

echo "Restarting Sandstorm-Agent"
monit restart sandstorm-agent



# Clean Up
