package campaign_test

import (
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/mocks"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	bucket = "bucket"
	env    = "env"
	key    = "config/env.json"

	goodConfig = `
	{
		"Campaigns": {
			"owl2018": [
				{
					"ActiveEndDate": "2019-05-27T18:00:00Z",
					"ActiveStartDate": "2018-05-07T18:00:00Z",
					"ChannelList": {
						"27697171": true
					},
					"Description": "Test",
					"Id": "owl2018",
					"Objectives": [
						{
							"Description": "Test 1",
							"EventHandler": "IndividualCheerHandler",
							"Id": "owl2018.launch.cheer.global",
							"Milestones": [
								{
									"ActiveEndDate": "2020-02-27T18:00:00Z",
									"ActiveStartDate": "2018-02-07T18:00:00Z",
									"Handler": "MileStone31Handler",
									"HandlerMetadata": {
										"Rewards": [
											{
												"Description": "OWL Skin",
												"FulfillmentStrategy": "blizzard",
												"Id": "rewards-cheering-owl2018-junkrat-skin",
												"ImageUrl": "https://d3aqoihi2n8ty8.cloudfront.net/owl-2017/global-rewards/finalskin-junkrat.png",
												"Name": "Junkrat",
												"Type": "igc"
											}
										],
										"SelectionStrategy": "ALL"
									},
									"Id": "owl2018.launch.cheer.global.milestone1",
									"ParticipationThreshold": 5,
									"Threshold": 50,
									"VisibleEndDate": "2020-02-27T18:00:00Z",
									"VisibleStartDate": "2018-02-07T18:00:00Z"
								}
							],
							"ShouldAutogrant": false,
							"Tag": "INDIVIDUAL",
							"Title": "test1"
						}
					],
					"Title": "Test"
				}
			]
		},
		"Version": 1
	}
	`

	badObjectiveConfig = `
	{
		"Campaigns": {
			"owl2018": [
				{
					"Id": "owl2018",
					"ActiveStartDate": "2018-02-07T18:00:00Z",
					"ActiveEndDate": "2019-02-27T18:00:00Z",
					"Title": "Overwatch League - Staff Beta",
					"Description": "The Overwatch League - Staff Beta",
					"ChannelList": {
						"27697171": true,
						"180833069": true,
						"137512364": true,
						"188863650": true,
						"188864445": true
					},
					"Objectives": [
						{
							"Id": "owl2018.launch.cheer.global",
							"Description": "OWL Cheer Global",
							"EventHandler": "GlobalCheerHandler",
							"Tag": "GLOBAL",
							"Milestones": []
						}
					]
				}
			]
		}
	}
	`
)

func TestCampaignPoller(t *testing.T) {
	Convey("CampaignPoller", t, func() {
		mockS3Client := new(mocks.IS3Client)
		mockS3Client.On("BucketExists", bucket).Return(true, nil)
		mockS3Client.On("FileExists", bucket, key).Return(true, nil)
		testFileBytes := []byte(goodConfig)

		Convey("When Bucket does not exist", func() {
			cfg := &config.Configuration{CampaignBucket: "bad", EnvironmentPrefix: env}
			mockS3Client.On("BucketExists", Anything, Anything).Return(false, nil)
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		Convey("When BucketExists Errors", func() {
			cfg := &config.Configuration{CampaignBucket: "bad", EnvironmentPrefix: env}
			mockS3Client.On("BucketExists", Anything, Anything).Return(false, errors.New("error"))
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		Convey("When Key does not exist", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: "bad"}
			mockS3Client.On("FileExists", Anything, Anything).Return(false, nil)
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		Convey("When FileExists errors", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: "bad"}
			mockS3Client.On("FileExists", Anything, Anything).Return(false, errors.New("error"))
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		Convey("When File is empty", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env}
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(""), nil)
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		Convey("Can LoadFile errors", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env}
			mockS3Client.On("LoadFile", bucket, key).Return(nil, errors.New("error"))
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		Convey("DenormalizeData fails", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env}
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(badObjectiveConfig), nil)
			_, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldNotBeNil)
		})

		// Reads info from campaign/dev.json
		Convey("Loads test file correctly", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env}
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(testFileBytes), nil)
			poller, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldBeNil)
			So(poller.CampaignConfig, ShouldNotBeEmpty)
			campaign := poller.CampaignConfig.Campaigns["owl2018"][0]
			So(campaign.ID, ShouldEqual, "owl2018")
			So(campaign.ChannelList["27697171"], ShouldEqual, true)
			So(campaign.Objectives[0].ID, ShouldEqual, "owl2018.launch.cheer.global")
		})

		Convey("RefreshCampaignsAtInterval with errors does not effect campaigns", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env, CampaignRefreshDelaySeconds: 1}
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(testFileBytes), nil).Once()
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(badObjectiveConfig), nil)
			poller, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldBeNil)
			So(poller.CampaignConfig, ShouldNotBeEmpty)

			defer poller.Shutdown()
			go poller.RefreshCampaignsAtInterval()
			time.Sleep(2 * time.Second)
			campaign := poller.CampaignConfig.Campaigns["owl2018"][0]
			So(campaign.Objectives[0].ID, ShouldEqual, "owl2018.launch.cheer.global")
		})

		Convey("RefreshCampaignsAtInterval with updates campaigns successfully", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env, CampaignRefreshDelaySeconds: 1}
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(testFileBytes), nil)
			poller, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldBeNil)
			So(poller.CampaignConfig, ShouldNotBeEmpty)
			cmpgn := poller.CampaignConfig.Campaigns["owl2018"][0]
			cmpgn.Objectives[0].ID = "old_id" // Update objective locally

			defer poller.Shutdown()
			go poller.RefreshCampaignsAtInterval()
			time.Sleep(2 * time.Second)
			campaign := poller.CampaignConfig.Campaigns["owl2018"][0]
			So(campaign.Objectives[0].ID, ShouldEqual, "owl2018.launch.cheer.global")
		})

		Convey("RefreshCampaignsAtInterval config update does not corrupt concurrent reads", func() {
			cfg := &config.Configuration{CampaignBucket: bucket, EnvironmentPrefix: env, CampaignRefreshDelaySeconds: 1}
			mockS3Client.On("LoadFile", bucket, key).Return([]byte(testFileBytes), nil)
			poller, err := campaign.NewPoller(cfg, mockS3Client)
			So(err, ShouldBeNil)
			So(poller.CampaignConfig, ShouldNotBeEmpty)

			defer poller.Shutdown()
			go poller.RefreshCampaignsAtInterval()
			ch := make(chan struct{})
			errs := make(chan bool, 100)
			for i := 0; i < 5; i++ {
				go func() {
					for {
						select {
						case <-ch:
							return
						default:
							if _, ok := poller.CampaignConfig.Campaigns["owl2018"]; !ok {
								errs <- true
							}
						}
					}
				}()
			}
			time.Sleep(3 * time.Second)
			close(ch)
			So(errs, ShouldBeEmpty)
		})
	})
}
