package campaign

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/s3"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// Poller holds the s3 information for loading campaign configs
type Poller struct {
	FortunaConfig  *config.Configuration
	CampaignConfig *Configuration
	s3Client       s3.IS3Client
	stopChannel    chan struct{}
}

// NewPoller returns new poller with a loaded campaign config
func NewPoller(fortunaConfig *config.Configuration, s3client s3.IS3Client) (*Poller, error) {
	bucket := fortunaConfig.CampaignBucket
	key := campaignKey(fortunaConfig.EnvironmentPrefix)
	campaignConfig := Configuration{}

	if exists, err := s3client.BucketExists(bucket); !exists || err != nil {
		log.WithField("bucket", bucket).WithError(err).Error("Bucket does not exist for Campaign object creation")
		if err != nil {
			return nil, errors.Wrapf(err, "Could not check if bucket exists: %s", bucket)
		}
		return nil, errors.WithStack(fmt.Errorf("Bucket %s does not exist for campaignConfig creation", bucket))
	}

	if exists, err := s3client.FileExists(bucket, key); !exists || err != nil {
		log.WithFields(log.Fields{
			"key":    key,
			"bucket": bucket,
		}).WithError(err).Error("Key does not exist in Bucket for campaignConfig object creation")
		if err != nil {
			return nil, errors.Wrapf(err, "Could not check if file exists: %s / %s", bucket, key)
		}
		return nil, errors.WithStack(fmt.Errorf("Key %s does not exist in Bucket %s for campaignConfig object creation", key, bucket))
	}

	poller := &Poller{
		FortunaConfig:  fortunaConfig,
		CampaignConfig: &campaignConfig,
		s3Client:       s3client,
		stopChannel:    make(chan struct{}),
	}
	if err := poller.setupCampaignConfiguration(); err != nil {
		log.WithFields(log.Fields{
			"key":    key,
			"bucket": bucket,
		}).WithError(err).Error("Campaign config did not setup properly")
		return nil, errors.Wrap(err, "Campaign config did not setup properly")
	}
	return poller, nil
}

// RefreshCampaignsAtInterval polls for a new campaign config based on {CampaignRefreshDelaySeconds}
func (p *Poller) RefreshCampaignsAtInterval() {
	timer := time.Tick(time.Second * time.Duration(p.FortunaConfig.CampaignRefreshDelaySeconds))

	for {
		select {
		case <-p.stopChannel:
			return
		case <-timer:
			if err := p.setupCampaignConfiguration(); err != nil {
				log.WithError(err).Error("Error refreshing campaignConfig")
			}
		}
	}
}

// Shutdown stops any polling goroutine
func (p *Poller) Shutdown() {
	if p.stopChannel != nil {
		close(p.stopChannel)
	}
}

// setupCampaignConfiguration retrieves config from given bucket and key
func (p *Poller) setupCampaignConfiguration() error {
	campaignConfig := &Configuration{}
	campaignKey := campaignKey(p.FortunaConfig.EnvironmentPrefix)
	campaignConfigBytes, err := p.s3Client.LoadFile(p.FortunaConfig.CampaignBucket, campaignKey)
	if err != nil {
		log.WithFields(log.Fields{
			"key":    campaignKey,
			"bucket": p.FortunaConfig.CampaignBucket,
		}).WithError(err).Error("could not load file from S3 for Campaign")
		return err
	}

	if err = json.Unmarshal(campaignConfigBytes, campaignConfig); err != nil {
		return err
	}
	if err := campaignConfig.DenormalizeData(); err != nil {
		return errors.Wrap(err, "Error denormalizing campaign config")
	}

	// Copies the local struct into the current campaign config structure
	*p.CampaignConfig = *campaignConfig
	return nil
}

func campaignKey(prefix string) string {
	return fmt.Sprintf("config/%s.json", prefix)
}
