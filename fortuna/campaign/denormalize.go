package campaign

import "fmt"

// DenormalizeData flattens rewards and milestones for easy access
func (c *Configuration) DenormalizeData() error {
	configCtx := newConfigDenormalizeContext()

	for domain, campaigns := range c.Campaigns {
		for _, campaign := range campaigns {
			if _, exists := configCtx.campaignsByID[campaign.ID]; exists {
				return fmt.Errorf("Duplicate campaign in config: %v", campaign.ID)
			}

			campaign.Domain = domain
			configCtx.campaignsByID[campaign.ID] = campaign
			configCtx.denormalizeTrackedMilestones(campaign)
			for _, objective := range campaign.Objectives {
				objectiveContext := newObjectiveDenormalizeContext(objective, campaign, domain, configCtx)

				objectiveContext.denormalizeOntoCampaign(campaign)

				if err := objectiveContext.denormalizeObjective(); err != nil {
					return err
				}

				if err := objectiveContext.denormalizeMilestones(); err != nil {
					return err
				}
			}
			for _, trigger := range campaign.Triggers {
				triggerContext := newTriggerDenormalizeContext(trigger, campaign, domain, configCtx)

				if err := triggerContext.denormalizeTrigger(); err != nil {
					return err
				}

				triggerContext.denormalizeRewards()
			}
		}
	}

	configCtx.applyToConfiguration(c)
	return nil
}

type configDenormalizeContext struct {
	campaignsByID                   map[string]*Campaign
	milestones                      map[string]*Milestone
	rewardCodes                     map[string]bool
	objectives                      map[string]*Objective
	trackedMilestoneIDToTrackingIDs map[string][]string
	triggers                        map[string]*Trigger
}

func newConfigDenormalizeContext() *configDenormalizeContext {
	return &configDenormalizeContext{
		campaignsByID:                   make(map[string]*Campaign),
		milestones:                      make(map[string]*Milestone),
		rewardCodes:                     make(map[string]bool),
		objectives:                      make(map[string]*Objective),
		trackedMilestoneIDToTrackingIDs: make(map[string][]string),
		triggers:                        make(map[string]*Trigger),
	}
}

func (c *configDenormalizeContext) applyToConfiguration(config *Configuration) {
	config.CampaignsByID = c.campaignsByID
	config.Milestones = c.milestones
	config.RewardCodes = c.rewardCodes
	config.Objectives = c.objectives
	config.TrackedMilestoneIDToTrackingIDs = c.trackedMilestoneIDToTrackingIDs
	config.Triggers = c.triggers
}

type objectiveDenormalizeContext struct {
	objective *Objective
	campaign  *Campaign
	domain    string
	configCtx *configDenormalizeContext
}

func newObjectiveDenormalizeContext(objective *Objective, campaign *Campaign, domain string, configCtx *configDenormalizeContext) *objectiveDenormalizeContext {
	return &objectiveDenormalizeContext{
		objective,
		campaign,
		domain,
		configCtx,
	}
}

func (o *objectiveDenormalizeContext) denormalizeObjective() error {
	objective := o.objective

	if _, exists := o.configCtx.objectives[objective.ID]; exists {
		return fmt.Errorf("Duplicate objective in config: %v", objective.ID)
	}

	objective.Domain = o.domain
	o.configCtx.objectives[objective.ID] = objective

	objective.Campaign = o.campaign
	objective.CampaignId = o.campaign.ID

	return nil
}

func (o *objectiveDenormalizeContext) denormalizeOntoCampaign(campaign *Campaign) {
	tag := o.objective.Tag

	objectivesByTag := map[string][]*Objective{}
	if campaign.ObjectivesByTag != nil {
		for k, v := range campaign.ObjectivesByTag {
			objectivesByTag[k] = v
		}
	}

	if _, exists := objectivesByTag[tag]; !exists {
		objectivesByTag[tag] = []*Objective{}
	}

	objectivesByTag[tag] = append(objectivesByTag[tag], o.objective)

	campaign.ObjectivesByTag = objectivesByTag
}

func (o *objectiveDenormalizeContext) denormalizeMilestones() error {
	if err := validateMilestones(o.objective); err != nil {
		return err
	}

	randomIndex := 0
	for _, milestone := range o.objective.Milestones {
		if err := o.denormalizeMilestone(randomIndex, milestone); err != nil {
			return err
		}
		if milestone.HandlerMetadata.SelectionStrategy == RandomTrackSelectionStrategy {
			milestone.HandlerMetadata.SelectionIndex = randomIndex
			randomIndex++
		}
	}

	return nil
}

func (o *objectiveDenormalizeContext) denormalizeMilestone(index int, milestone *Milestone) error {
	if _, exists := o.configCtx.milestones[milestone.ID]; exists {
		return fmt.Errorf("Duplicate milestone in config: %v", milestone.ID)
	}

	// Copy contextual data onto milestone itself for convenience
	milestone.Domain = o.domain
	milestone.ObjectiveID = o.objective.ID
	milestone.ObjectiveTag = o.objective.Tag
	milestone.CampaignId = o.campaign.ID

	// Copy milestone to shallower config context
	o.configCtx.milestones[milestone.ID] = milestone

	// If using a random reward track, copy objective's set of random rewards into HandlerMetadata
	if milestone.HandlerMetadata.SelectionStrategy == RandomTrackSelectionStrategy {
		milestone.HandlerMetadata.Rewards = make([]*Reward, len(o.objective.RandomRewards))
		copy(milestone.HandlerMetadata.Rewards, o.objective.RandomRewards)
	}

	o.denormalizeRewards(milestone)

	return nil
}

// denormalizeTrackedMilestones returns a map where the keys are milestone ids
// that are being used as triggers and the value is a slice of the milestone ids
// that are using those triggers
func (c *configDenormalizeContext) denormalizeTrackedMilestones(campaign *Campaign) {
	for _, objective := range campaign.Objectives {
		if objective.Tag != ObjectiveTagCollection {
			continue
		}
		for _, milestone := range objective.Milestones {
			if len(milestone.HandlerMetadata.MilestoneTriggers) == 0 {
				continue
			}
			for _, triggerKey := range milestone.HandlerMetadata.MilestoneTriggers {
				c.trackedMilestoneIDToTrackingIDs[triggerKey] = append(c.trackedMilestoneIDToTrackingIDs[triggerKey], milestone.ID)
			}
		}
	}
}

func (o *objectiveDenormalizeContext) denormalizeRewards(milestone *Milestone) {
	for _, reward := range milestone.HandlerMetadata.Rewards {
		// Copy contextual data onto the reward
		reward.Domain = o.domain
		reward.CampaignID = o.campaign.ID
		reward.MilestoneID = milestone.ID
		reward.ObjectiveTag = o.objective.Tag
		reward.ObjectiveID = o.objective.ID

		// If using reward code strategy, copy each reward code to shallower config context
		if milestone.HandlerMetadata.SelectionStrategy == RewardCodeSelectionStrategy {
			o.configCtx.rewardCodes[reward.Code] = true
		}
	}
}

type triggerDenormalizeContext struct {
	trigger   *Trigger
	campaign  *Campaign
	domain    string
	configCtx *configDenormalizeContext
}

func newTriggerDenormalizeContext(trigger *Trigger, campaign *Campaign, domain string, configCtx *configDenormalizeContext) *triggerDenormalizeContext {
	return &triggerDenormalizeContext{
		trigger,
		campaign,
		domain,
		configCtx,
	}
}

func (t *triggerDenormalizeContext) denormalizeTrigger() error {
	trigger := t.trigger

	if _, exists := t.configCtx.triggers[trigger.ID]; exists {
		return fmt.Errorf("Duplicate trigger in config: %v", trigger.ID)
	}

	trigger.Domain = t.domain
	t.configCtx.triggers[trigger.ID] = trigger

	trigger.Campaign = t.campaign
	trigger.CampaignId = t.campaign.ID

	return nil
}

func (t *triggerDenormalizeContext) denormalizeRewards() {
	triggerRewards := append(t.trigger.RecipientRewardAttributes.Rewards, t.trigger.BenefactorRewardAttributes.Rewards...)
	for _, reward := range triggerRewards {
		// Copy contextual data onto the reward
		reward.Domain = t.domain
		reward.CampaignID = t.campaign.ID
		reward.TriggerID = t.trigger.ID
	}
}

// DenormalizeMilestoneWithParentData denormalizes a single milestone without any context other than the provided parent
// It is assumed that the milestone here is loaded from DynamoDB (which means it is already partially denormalized)
func DenormalizeMilestoneWithParentData(milestone *Milestone, parentObjective *Objective) {
	milestone.ObjectiveTag = parentObjective.Tag

	for _, reward := range milestone.HandlerMetadata.Rewards {
		reward.Domain = milestone.Domain
		reward.CampaignID = milestone.CampaignId
		reward.MilestoneID = milestone.ID
		reward.ObjectiveTag = parentObjective.Tag
		reward.ObjectiveID = parentObjective.ID
	}
}

// DenormalizeTriggerRewards denormalizes a single trigger without any context
func DenormalizeTriggerRewards(trigger *Trigger) {
	for _, reward := range trigger.BenefactorRewardAttributes.Rewards {
		reward.Domain = trigger.Domain
		reward.CampaignID = trigger.CampaignId
		reward.TriggerID = trigger.ID
	}

	for _, reward := range trigger.RecipientRewardAttributes.Rewards {
		reward.Domain = trigger.Domain
		reward.CampaignID = trigger.CampaignId
		reward.TriggerID = trigger.ID
	}
}

// DenormalizeCampaign denormalizes a single campaign
func DenormalizeCampaign(campaign *Campaign) error {
	configCtx := newConfigDenormalizeContext()
	for _, objective := range campaign.Objectives {
		objectiveContext := newObjectiveDenormalizeContext(objective, campaign, campaign.Domain, configCtx)

		objectiveContext.denormalizeOntoCampaign(campaign)

		if err := objectiveContext.denormalizeObjective(); err != nil {
			return err
		}

		if err := objectiveContext.denormalizeMilestones(); err != nil {
			return err
		}
	}
	for _, trigger := range campaign.Triggers {
		triggerContext := newTriggerDenormalizeContext(trigger, campaign, campaign.Domain, configCtx)

		if err := triggerContext.denormalizeTrigger(); err != nil {
			return err
		}

		triggerContext.denormalizeRewards()
	}
	return nil
}
