package campaign

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDenormalize(t *testing.T) {
	Convey("Test config denormalization", t, func() {
		Convey("Test denormalizeObjective", func() {
			Convey("with no duplicate objective", func() {
				domain := "owl2018"
				campaign := &Campaign{}
				objective := &Objective{
					ID: "obj1",
				}

				configCtx := newConfigDenormalizeContext()
				objCtx := newObjectiveDenormalizeContext(objective, campaign, domain, configCtx)

				err := objCtx.denormalizeObjective()
				So(err, ShouldBeNil)

				// Was the objective copied into the shallow context?
				So(configCtx.objectives, ShouldContainKey, objective.ID)
				So(configCtx.objectives[objective.ID], ShouldEqual, objective)

				// Did contextual data copy onto the objective?
				So(objective.Domain, ShouldEqual, domain)
				So(objective.Campaign, ShouldEqual, campaign)
			})

			Convey("with duplicate objective", func() {
				domain := "owl2018"
				campaign := &Campaign{}
				objective1 := &Objective{
					ID: "obj1",
				}
				objective2 := &Objective{
					ID: "obj1",
				}

				configCtx := newConfigDenormalizeContext()
				objCtx1 := newObjectiveDenormalizeContext(objective1, campaign, domain, configCtx)

				var err error
				err = objCtx1.denormalizeObjective()
				// Normalized once: fine
				So(err, ShouldBeNil)

				// Normalized a second time by different objective context: bad
				objCtx2 := newObjectiveDenormalizeContext(objective2, campaign, domain, configCtx)
				err = objCtx2.denormalizeObjective()
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test denormalizeMilestone", func() {
			domain := "owl2018"
			campaign := &Campaign{}
			objective := &Objective{
				ID:  "obj1",
				Tag: "cool tag",
			}

			Convey("with no duplicate milestone", func() {
				objCtx := newObjectiveDenormalizeContext(objective, campaign, domain, newConfigDenormalizeContext())

				milestone := generateMilestone(AllSelectionStrategy)
				milestoneIx := 1

				milestone.HandlerMetadata.Rewards = []*Reward{
					{ID: "reward1"},
					{ID: "reward2"},
				}

				err := objCtx.denormalizeMilestone(milestoneIx, milestone)
				So(err, ShouldBeNil)

				// Was contextual data copied onto milestone?
				So(milestone.Domain, ShouldEqual, objCtx.domain)
				So(milestone.ObjectiveID, ShouldEqual, objective.ID)
				So(milestone.ObjectiveTag, ShouldEqual, objective.Tag)

				// Was contextual data copied onto rewards?
				for _, reward := range milestone.HandlerMetadata.Rewards {
					So(reward.Domain, ShouldEqual, objCtx.domain)
					So(reward.MilestoneID, ShouldEqual, milestone.ID)
					So(reward.ObjectiveTag, ShouldEqual, objCtx.objective.Tag)
				}
			})

			Convey("with duplicate milestone", func() {
				objCtx := newObjectiveDenormalizeContext(objective, campaign, domain, newConfigDenormalizeContext())

				milestone := generateMilestone(AllSelectionStrategy)
				milestoneIx := 1

				var err error
				err = objCtx.denormalizeMilestone(milestoneIx, milestone)
				So(err, ShouldBeNil)

				milestoneIx++
				err = objCtx.denormalizeMilestone(milestoneIx, milestone)
				So(err, ShouldNotBeNil)
			})

			Convey("with RandomTrackSelectionStrategy", func() {
				objective := &Objective{
					ID:  "randomobj1",
					Tag: "cool random tag",
					RandomRewards: []*Reward{
						{ID: "reward1"},
						{ID: "reward2"},
						{ID: "reward3"},
					},
				}

				milestone := generateMilestone(RandomTrackSelectionStrategy)
				milestoneIx := 1

				objCtx := newObjectiveDenormalizeContext(objective, campaign, domain, newConfigDenormalizeContext())

				err := objCtx.denormalizeMilestone(milestoneIx, milestone)
				So(err, ShouldBeNil)

				So(milestone.HandlerMetadata.Rewards, ShouldResemble, objective.RandomRewards)
			})

			Convey("with RewardCodeSelectionStrategy", func() {
				configCtx := newConfigDenormalizeContext()
				objCtx := newObjectiveDenormalizeContext(objective, campaign, domain, configCtx)

				milestone := generateMilestone(RewardCodeSelectionStrategy)
				milestoneIx := 1

				// Set up some rewards with codes
				milestone.HandlerMetadata.Rewards = []*Reward{
					{ID: "reward1", Code: "code1"},
					{ID: "reward2", Code: "code2"},
					{ID: "reward3", Code: "code3"},
				}

				err := objCtx.denormalizeMilestone(milestoneIx, milestone)
				So(err, ShouldBeNil)

				for _, reward := range milestone.HandlerMetadata.Rewards {
					So(configCtx.rewardCodes, ShouldContainKey, reward.Code)
					So(configCtx.rewardCodes[reward.Code], ShouldEqual, true)
				}
			})
		})

		Convey("test denormalizeTrackedMilestones", func() {
			testDomain := "testDomain"
			config := &Configuration{
				Campaigns: map[string][]*Campaign{
					testDomain: {
						{
							Objectives: []*Objective{
								{
									ID:  "obj1",
									Tag: ObjectiveTagCollection,
									Milestones: []*Milestone{
										{
											ID: "m1",
											HandlerMetadata: HandlerMetadata{
												MilestoneTriggers: []string{
													"m3",
													"m4",
												},
											},
										},
										{
											ID: "m2",
											HandlerMetadata: HandlerMetadata{
												MilestoneTriggers: []string{
													"m1",
													"m3",
													"m5",
												},
											},
										},
									},
								},
								{
									ID:  "obj2",
									Tag: "NotACollection", // Misconfigured Objective
									Milestones: []*Milestone{
										{
											ID: "m100",
											HandlerMetadata: HandlerMetadata{
												MilestoneTriggers: []string{
													"m300",
													"m400",
												},
											},
										},
									},
								},
							},
						},
					},
				},
			}

			configCtx := newConfigDenormalizeContext()
			configCtx.denormalizeTrackedMilestones(config.Campaigns[testDomain][0])
			configCtx.applyToConfiguration(config)
			So(config.TrackedMilestoneIDToTrackingIDs, ShouldResemble, map[string][]string{
				"m1": {"m2"},
				"m3": {"m1", "m2"},
				"m4": {"m1"},
				"m5": {"m2"},
			})
		})

		Convey("test denormalizeOntoCampaign", func() {
			tag1 := "cool objective"
			tag2 := "nice objective"

			objective1 := &Objective{
				ID:  "obj1",
				Tag: tag1,
			}

			objective2 := &Objective{
				ID:  "obj2",
				Tag: tag2,
			}

			objective3 := &Objective{
				ID:  "obj3",
				Tag: tag1,
			}

			campaign := &Campaign{}
			configCtx := newConfigDenormalizeContext()

			objCtx1 := newObjectiveDenormalizeContext(objective1, campaign, "owl2018", configCtx)

			objCtx1.denormalizeOntoCampaign(campaign)

			// `denormalizeOntoCampaign` should have initialized `campaign.ObjectivesByTag`
			So(campaign.ObjectivesByTag, ShouldNotBeNil)

			objCtx2 := newObjectiveDenormalizeContext(objective2, campaign, "owl2018", configCtx)
			objCtx2.denormalizeOntoCampaign(campaign)

			objCtx3 := newObjectiveDenormalizeContext(objective3, campaign, "owl2018", configCtx)
			objCtx3.denormalizeOntoCampaign(campaign)

			So(campaign.ObjectivesByTag, ShouldResemble, map[string][]*Objective{
				tag1: {
					objective1,
					objective3,
				},
				tag2: {
					objective2,
				},
			})
		})

		Convey("Test with trigger", func() {
			campaign := &Campaign{
				ID: "cam1",
			}
			configCtx := newConfigDenormalizeContext()

			rewards1 := []*Reward{
				{ID: "reward1"},
				{ID: "reward2"},
			}

			rewards2 := []*Reward{
				{ID: "reward1"},
				{ID: "reward2"},
			}

			trigger := &Trigger{
				ID: "trigger1",
				RecipientRewardAttributes: RewardAttributes{
					Rewards: rewards1,
				},
				BenefactorRewardAttributes: RewardAttributes{
					Rewards: rewards2,
				},
			}

			triggerCxt := newTriggerDenormalizeContext(trigger, campaign, "dom", configCtx)

			Convey("Test denormalizeTrigger", func() {
				triggerCxt.denormalizeTrigger()

				So(trigger.CampaignId, ShouldEqual, "cam1")
				So(trigger.Campaign, ShouldResemble, campaign)

				Convey("Test denormalizeRewards", func() {
					triggerCxt.denormalizeRewards()
					denormedReward := trigger.RecipientRewardAttributes.Rewards[0]
					So(denormedReward.TriggerID, ShouldEqual, trigger.ID)
					So(denormedReward.Domain, ShouldEqual, trigger.Domain)
					So(denormedReward.CampaignID, ShouldEqual, campaign.ID)
				})
			})

		})

		Convey("test DenormalizeMilestoneWithParentData", func() {
			tag := "cool objective"

			objective := &Objective{
				ID:  "obj1",
				Tag: tag,
			}

			milestone := generateMilestone(AllSelectionStrategy)
			milestone.HandlerMetadata.Rewards = []*Reward{
				{ID: "reward1", Code: "code1"},
			}
			milestone.Domain = "awesome domain"
			milestone.CampaignId = "cam1"

			DenormalizeMilestoneWithParentData(milestone, objective)

			So(milestone.ObjectiveTag, ShouldEqual, tag)
			denormalizedReward := milestone.HandlerMetadata.Rewards[0]
			So(denormalizedReward.MilestoneID, ShouldEqual, milestone.ID)
			So(denormalizedReward.Domain, ShouldEqual, milestone.Domain)
			So(denormalizedReward.CampaignID, ShouldEqual, milestone.CampaignId)
			So(denormalizedReward.ObjectiveID, ShouldEqual, objective.ID)
			So(denormalizedReward.ObjectiveTag, ShouldEqual, objective.Tag)
		})

		Convey("test concurrent denormalization", func() {
			config := &Configuration{
				Campaigns: map[string][]*Campaign{
					"testDomain": {
						{
							Objectives: []*Objective{
								{
									ID:         "obj1",
									Tag:        ObjectiveTagCollection,
									Milestones: []*Milestone{},
								},
								{
									ID:         "obj2",
									Tag:        ObjectiveTagGlobal,
									Milestones: []*Milestone{},
								},
							},
						},
					},
				},
			}
			stopCh := make(chan struct{})

			for i := 0; i < 5; i++ {
				go func() {
					for {
						select {
						case <-stopCh:
							return
						default:
							config.DenormalizeData()
						}
					}
				}()
			}
			time.Sleep(1 * time.Second)
			close(stopCh)

		})

		Convey("Test denormalizeCampaign", func() {
			tag := "testTag"
			domain := "testDomain"
			objectiveID := "testObjectiveID"
			triggerID := "triggerID"

			milestone := generateMilestone(AllSelectionStrategy)
			milestone.HandlerMetadata.Rewards = []*Reward{
				{ID: "reward1", Code: "code1"},
			}

			trigger := &Trigger{
				ID: triggerID,
				RecipientRewardAttributes: RewardAttributes{
					Rewards: []*Reward{
						{
							ID:   "reward2",
							Code: "code2",
						},
					},
				},
			}

			objective := &Objective{
				ID:  objectiveID,
				Tag: tag,
				Milestones: []*Milestone{
					milestone,
				},
			}

			campaign := &Campaign{
				Domain: domain,
				ID:     "campaignID",
				Objectives: []*Objective{
					objective,
				},
				Triggers: []*Trigger{
					trigger,
				},
			}

			DenormalizeCampaign(campaign)

			// Verify Campaign denormalized values
			So(campaign.ObjectivesByTag[tag][0], ShouldEqual, objective)

			// Verify Objective denormalized values
			So(campaign.Objectives[0].Domain, ShouldEqual, domain)
			So(campaign.Objectives[0].Campaign, ShouldEqual, campaign)

			// Verify Milestone denormalized values
			So(campaign.Objectives[0].Milestones[0].ObjectiveTag, ShouldEqual, tag)
			So(campaign.Objectives[0].Milestones[0].ObjectiveID, ShouldEqual, objectiveID)
			So(campaign.Objectives[0].Milestones[0].Domain, ShouldEqual, domain)
			So(campaign.Objectives[0].Milestones[0].HandlerMetadata.SelectionIndex, ShouldEqual, 0)

			// Verify Reward denormalized values
			denormalizedObjectiveReward := campaign.Objectives[0].Milestones[0].HandlerMetadata.Rewards[0]
			So(denormalizedObjectiveReward.MilestoneID, ShouldEqual, milestone.ID)
			So(denormalizedObjectiveReward.Domain, ShouldEqual, milestone.Domain)
			So(denormalizedObjectiveReward.CampaignID, ShouldEqual, milestone.CampaignId)
			So(denormalizedObjectiveReward.ObjectiveID, ShouldEqual, objectiveID)
			So(denormalizedObjectiveReward.ObjectiveTag, ShouldEqual, tag)
			So(denormalizedObjectiveReward.TriggerID, ShouldBeEmpty)

			// Verify Trigger reward denormalized values
			denormalizedTriggerReward := campaign.Triggers[0].RecipientRewardAttributes.Rewards[0]
			So(denormalizedTriggerReward.TriggerID, ShouldEqual, triggerID)
			So(denormalizedTriggerReward.MilestoneID, ShouldBeEmpty)
			So(denormalizedTriggerReward.Domain, ShouldEqual, trigger.Domain)
			So(denormalizedTriggerReward.CampaignID, ShouldEqual, trigger.CampaignId)
			So(denormalizedTriggerReward.ObjectiveID, ShouldBeEmpty)
			So(denormalizedTriggerReward.ObjectiveTag, ShouldBeEmpty)

			// Verify Trigger denormalized values
			So(campaign.Triggers[0].Domain, ShouldEqual, domain)
			So(campaign.Triggers[0].Campaign, ShouldEqual, campaign)
			So(campaign.Triggers[0].CampaignId, ShouldEqual, campaign.ID)
		})
	})
}
