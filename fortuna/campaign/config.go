package campaign

import (
	"fmt"
	"time"
)

const (
	ObjectiveTagGlobal     = "GLOBAL"
	ObjectiveTagIndividual = "INDIVIDUAL"
	ObjectiveTagTeam       = "TEAM"
	ObjectiveTagInsider    = "INSIDER"
	ObjectiveTagCollection = "COLLECTION"
	ObjectiveTagCheerbomb  = "CHEERBOMB"

	// Reward selection strategies
	AllSelectionStrategy               = "ALL"
	RandomTrackSelectionStrategy       = "RANDOM"
	RandomIndependentSelectionStrategy = "RANDOMINDEPENDENT"
	RewardCodeSelectionStrategy        = "REWARDCODE"
)

type TriggerType string

const (
	TriggerTypeCheer            TriggerType = "CHEER"
	TriggerTypeSubscription     TriggerType = "SUBSCRIPTION"
	TriggerTypeSubscriptionGift TriggerType = "SUBGIFT"
)

type Configuration struct {
	Version   uint
	Campaigns map[string][]*Campaign `validate:"nonzero"`

	// (ccmartin): Below fields populated by `Configuration.DenormalizeData`
	CampaignsByID                   map[string]*Campaign  `validate:"max=0" json:",omitempty"`
	Objectives                      map[string]*Objective `validate:"max=0" json:",omitempty"`
	Milestones                      map[string]*Milestone `validate:"max=0" json:",omitempty"`
	RewardCodes                     map[string]bool       `validate:"max=0" json:",omitempty"`
	TrackedMilestoneIDToTrackingIDs map[string][]string   `validate:"max=0" dynamo:"-" json:",omitempty"`
	Triggers                        map[string]*Trigger   `validate:"max=0" json:",omitempty"`
}

type Campaign struct {
	ID                  string                 `validate:"nonzero" dynamo:"campaignId,range" index:"campaignId-index,hash"`
	ActiveStartDate     time.Time              `validate:"nonzero" dynamo:"activeStartDate"`
	ActiveEndDate       time.Time              `validate:"nonzero" dynamo:"activeEndDate"`
	Description         string                 `validate:"nonzero" dynamo:"description"`
	Objectives          []*Objective           `dynamo:"-" json:",omitempty"`
	Triggers            []*Trigger             `dynamo:"-" json:",omitempty"`
	Title               string                 `validate:"nonzero" dynamo:"title"`
	ChannelList         map[string]bool        `dynamo:"channelList,set" json:",omitempty"`
	ExcludedChannelList map[string]bool        `dynamo:"excludedChannelList,set" json:",omitempty"`
	CheerGroups         map[string]*CheerGroup `dynamo:"cheerGroups" json:",omitempty"`

	StaffOnly            bool            `dynamo:"staffOnly" json:",omitempty"`
	UserWhitelistEnabled bool            `dynamo:"userWhitelistEnabled" json:",omitempty"`
	UserWhitelist        map[string]bool `dynamo:"userWhitelist,set" json:",omitempty"`
	CompleteRewardIDs    map[string]bool `dynamo:"completeRewardIDs,set" json:",omitempty"`

	// (ccmartin): Below fields populated by `Configuration.DenormalizeData`
	ObjectivesByTag map[string][]*Objective `validate:"max=0" dynamo:"-" json:",omitempty"`
	Domain          string                  `dynamo:"domain,hash" json:",omitempty"`

	AllChannels bool `dynamo:"allChannels" json:",omitempty"`

	// This lets us configure if we want to use dynamoDB or pantheon as a backend tracker. Default is pantheon.
	ParticipationTrackerBackend string `dynamo:"participationTrackerBackend" json:",omitempty"`
}

// CheerGroup defines a list of cheermotes that can be used to progress the objective
type CheerGroup struct {
	CheerCodes           map[string]bool `dynamo:"cheerCodes,set"`             // Upper cased Cheermote strings for comparison
	Name                 string          `dynamo:"name"`                       // e.g. team name if the cheer group represents a esports team
	ImageURL             string          `dynamo:"imageUrl"`                   // e.g. team image if the cheer group represents a esports team
	Division             string          `dynamo:"division" json:",omitempty"` // Optional, broader grouping key. e.g. "North America" vs "Europe" or "Twitch" vs "Amazon"
	UserWhitelistEnabled bool            `dynamo:"userWhitelistEnabled" json:",omitempty"`
	UserWhitelist        map[string]bool `dynamo:"userWhitelist,set" json:",omitempty"`
	Deprecated           bool            `dynamo:"deprecated" json:",omitempty"` // Optional, if the team is no longer active and should not be used
}

type Objective struct {
	ID                        string          `validate:"nonzero" dynamo:"objectiveId" index:"objectiveId-index,hash"`
	Description               string          `validate:"nonzero" dynamo:"description"`
	EventHandler              string          `validate:"nonzero" dynamo:"eventHandler"`
	Milestones                []*Milestone    `dynamo:"-" json:",omitempty"`
	Title                     string          `validate:"nonzero" dynamo:"title"`
	Tag                       string          `validate:"nonzero" dynamo:"tag"`
	RandomRewards             []*Reward       `dynamo:"randomRewards" json:",omitempty"`
	ShouldAutogrant           bool            `dynamo:"shouldAutogrant" json:",omitempty"`
	CheerGroupKey             string          `dynamo:"cheerGroupKey" json:",omitempty"` // Used as a key for finding the CheerGroup defined in Campaign
	CampaignId                string          `dynamo:"campaignId" index:"campaignId-index,hash" json:",omitempty"`
	UserWhitelistEnabled      bool            `dynamo:"userWhitelistEnabled" json:",omitempty"`
	UserWhitelist             map[string]bool `dynamo:"userWhitelist,set" json:",omitempty"`
	Deprecated                bool            `dynamo:"deprecated" json:",omitempty"`                // Optional, if the objective is not longer active and should not be used
	ContributingMilestoneIds  []string        `dynamo:"contributingMilestoneIds" json:",omitempty"`  // Optional set of Ids used to determine if the event valid and applies.
	BadgeSetIds               map[string]bool `dynamo:"badgeSetId" json:",omitempty"`                // Optional, a map of badge set ids to listen to
	DiscoveryEventType        string          `dynamo:"discoveryEventType" json:",omitempty"`        // Optional string that will map to a specific type of discovery event
	Preference                string          `dynamo:"preference" json:",omitempty"`                // Optional string that will specify the preference value for this objective. At the moment this is the team selected but we can move to more preferene types by adding another PreferenceType field
	DiscoveryEventResetPeriod string          `dynamo:"discoveryEventResetPeriod" json:",omitempty"` // Optional string to specify the period of time used to construct the event ID for the discovery event
	DisablePubSub             bool            `dynamo:"disablePubSub" json:",omitempty"`             // Optional boolean to disable PubSub update when this objective's progress updates

	// (ccmartin): Below fields populated by `Configuration.DenormalizeData`
	Campaign *Campaign `validate:"max=0" dynamo:"-" json:",omitempty"`
	Domain   string    `validate:"max=0" dynamo:"domain,hash" json:",omitempty"`
}

type Milestone struct {
	ID                     string          `validate:"nonzero" dynamo:"milestoneId" index:"milestoneId-index,hash"`
	Handler                string          `validate:"nonzero" dynamo:"handler"`
	HandlerMetadata        HandlerMetadata `dynamo:"handlerMetadata" json:",omitempty"`
	ActiveStartDate        time.Time       `validate:"nonzero" dynamo:"activeStartDate"` // Reward is granted/visible
	ActiveEndDate          time.Time       `validate:"nonzero" dynamo:"activeEndDate"`
	VisibleStartDate       time.Time       `validate:"nonzero" dynamo:"visibleStartDate"` // Milestone is visible
	VisibleEndDate         time.Time       `validate:"nonzero" dynamo:"visibleEndDate"`
	Threshold              int64           `validate:"min=0" dynamo:"threshold"`
	ParticipationThreshold int64           `validate:"min=0" dynamo:"participationThreshold"` // Only used for global progression. The threshold is the global threshold to unlock, and participation threshold is what the individual needs to participate to claim.
	UserWhiteListEnabled   bool            `dynamo:"userWhiteListEnabled" json:",omitempty"`
	UserWhitelist          map[string]bool `dynamo:"userWhitelist,set" json:",omitempty"`
	AutograntDisabled      bool            `dynamo:"autograntDisabled" json:",omitempty"`
	CheckEligibility       bool            `dynamo:"checkEligibility" json:",omitempty"`
	CampaignId             string          `dynamo:"campaignId" index:"campaignId-index,hash" json:",omitempty"`

	// Optional setting to make a milestone only claimable if a user has Prime
	IsPrimeOnly bool `dynamo:"isPrimeOnly" json:",omitempty"`
	// Setting to specify if a milestone can only be completed if the user has the specific custom subs
	RequiredSubs []string `dynamo:"requiredSubs" json:",omitempty"`

	// (ccmartin): Below fields populated by `Configuration.DenormalizeData`
	Domain       string `validate:"max=0" dynamo:"domain,hash" json:",omitempty"`
	ObjectiveTag string `validate:"max=0" dynamo:"-" json:",omitempty"`
	ObjectiveID  string `validate:"max=0" dynamo:"objectiveId" index:"objectiveId-index,hash" json:",omitempty"`
}

type HandlerMetadata struct {
	// (ccmartin): Note that when `SelectionStrategy` == `RandomTrackSelectionStrategy`,
	// field `Rewards` will be overwritten during denormalization.
	Rewards           []*Reward `dynamo:"rewards" json:",omitempty"`
	SelectionStrategy string    `validate:"nonzero" dynamo:"selectionStrategy"` // e.g. ALL, RANDOM

	// Optional lower bound (inclusive) for cheer event amount to trigger this milestone's fulfillment flow
	MinCheerAmount int `dynamo:"minCheerAmount" json:",omitempty"`
	// Optional upper bound (inclusive) for cheer event amount to trigger this milestone's fulfillment flow
	MaxCheerAmount int `dynamo:"maxCheerAmount" json:",omitempty"`

	// (ccmartin): Below fields populated by `Configuration.DenormalizeData`
	SelectionIndex int `validate:"max=0" dynamo:"selectionIndex" json:",omitempty"`

	// (colin): Used for Collection type objectives
	MilestoneTriggers []string `dynamo:"milestoneTriggers" json:",omitempty"`
}

type Reward struct {
	ID                  string         `validate:"nonzero" dynamo:"rewardId"`
	Code                string         `dynamo:"code" json:",omitempty"`
	Type                string         `validate:"nonzero" dynamo:"type"`
	Name                string         `validate:"nonzero" dynamo:"name"`
	Description         string         `validate:"nonzero" dynamo:"description"`
	ImageURL            string         `validate:"nonzero" dynamo:"imageUrl"`
	FulfillmentStrategy string         `dynamo:"fulfillmentStrategy" json:",omitempty"`
	Rarity              int32          `dynamo:"rarity" json:",omitempty"`
	Metadata            RewardMetadata `dynamo:"metadata" json:",omitempty"`
	SKU                 string         `dynamo:"sku" json:",omitempty"`
	ChannelID           string         `dynamo:"channelId" json:",omitempty"`
	BadgeVersion        string         `dynamo:"badgeVersion" json:",omitempty"`

	// Fulfilling some quantity of items for a given type. e.g. 200 BitsForOWL Bits
	VendorType      string `dynamo:"vendorType" json:",omitempty"`
	Quantity        int32  `dynamo:"quantity" json:",omitempty"`
	Repeatable      bool   `dynamo:"repeatable" json:",omitempty"`
	ShouldAutogrant bool   `dynamo:"shouldAutogrant" json:",omitempty"`

	// Used for fulfilling emotes
	OwnerID  string    `dynamo:"ownerID" json:",omitempty"`
	StartsAt time.Time `dynamo:"startsAt" json:",omitempty"`
	EndsAt   time.Time `dynamo:"endsAt" json:",omitempty"`

	// (ccmartin): Below fields populated by `Configuration.DenormalizeData`. These are only populated for Objective rewards.
	ObjectiveTag string `validate:"max=0" dynamo:"-" json:",omitempty"`
	Domain       string `validate:"max=0" dynamo:"-" json:",omitempty"`
	CampaignID   string `validate:"max=0" dynamo:"-" json:",omitempty"`
	MilestoneID  string `validate:"max=0" dynamo:"-" json:",omitempty"`
	ObjectiveID  string `validate:"max=0" dynamo:"-" json:",omitempty"`
	TriggerID    string `validate:"max=0" dynamo:"-" json:",omitempty"`
}

type RewardMetadata struct {
	SubType   string `dynamo:"subType" json:",omitempty"`
	IsPending bool   `dynamo:"isPending" json:",omitempty"`
}

type Token struct {
	Domain    string `validate:"nonzero" dynamo:"domain"`
	Threshold int64  `validate:"min=0" dynamo:"threshold"`
	Quantity  int64  `validate:"min=0" dynamo:"quantity"`
}

type Trigger struct {
	ID                         string           `validate:"nonzero" dynamo:"triggerId" index:"triggerId-index,hash"`
	Title                      string           `validate:"nonzero" dynamo:"title"`
	Description                string           `validate:"nonzero" dynamo:"description"`
	TriggerAmountMin           int              `validate:"nonzero" dynamo:"triggerAmountMin"`
	TriggerAmountMax           int              `validate:"nonzero" dynamo:"triggerAmountMax"`
	TriggerType                TriggerType      `validate:"nonzero" dynamo:"triggerType"`
	ActiveStartDate            time.Time        `validate:"nonzero" dynamo:"activeStartDate"` // Reward is granted/visible
	ActiveEndDate              time.Time        `validate:"nonzero" dynamo:"activeEndDate"`
	VisibleStartDate           time.Time        `validate:"nonzero" dynamo:"visibleStartDate"` // Reward is visible
	VisibleEndDate             time.Time        `validate:"nonzero" dynamo:"visibleEndDate"`
	RecipientSelectionFormula  string           `validate:"nonzero" dynamo:"recipientSelectionFormula"`
	BenefactorRewardAttributes RewardAttributes `validate:"nonzero" dynamo:"benefactorRewardAttributes"`
	RecipientRewardAttributes  RewardAttributes `validate:"nonzero" dynamo:"recipientRewardAttributes"`
	Tokens                     []*Token         `dynamo:"tokens" json:",omitempty"`

	UserWhitelistEnabled bool            `dynamo:"userWhitelistEnabled" json:",omitempty"`
	UserWhitelist        map[string]bool `dynamo:"userWhitelist,set" json:",omitempty"`

	LoadTestEnabled bool `dynamo:"loadTestEnabled,set" json:",omitempty"`

	StaffOnly bool `dynamo:"staffOnly" json:",omitempty"`

	// Denormalized properties
	Campaign   *Campaign `validate:"max=0" dynamo:"-" json:",omitempty"`
	Domain     string    `validate:"max=0" dynamo:"domain,hash" json:",omitempty"`
	CampaignId string    `dynamo:"campaignId" index:"campaignId-index,hash" json:",omitempty"`
}

type RewardAttributes struct {
	Rewards               []*Reward `dynamo:"rewards"`
	SelectionStrategy     string    `validate:"nonzero" dynamo:"selectionStrategy"`    // e.g. ALL, RANDOM
	SelectionAmount       int       `dynamo:"selectionAmount" json:",omitempty"`       // Only applies if selection strategy is random
	SelectionAmountScaled bool      `dynamo:"selectionAmountScaled" json:",omitempty"` // Only applies if selection strategy is random. If true, amount is determined by trigger amount
}

func validateMilestones(objective *Objective) error {
	// an objective must have at least 1 milestone
	if len(objective.Milestones) == 0 {
		return fmt.Errorf("Objective doesn't have any milestones. Objective: %v", objective.ID)
	}

	hasRandomStrategyCount := 0
	for _, milestone := range objective.Milestones {
		if !milestone.hasValidDates() {
			return fmt.Errorf("Invalid Start and End timestamps for milestone: %v", milestone.ID)
		}

		if milestone.HandlerMetadata.SelectionStrategy == RandomTrackSelectionStrategy {
			hasRandomStrategyCount++
		}
		if milestone.HandlerMetadata.SelectionStrategy == RewardCodeSelectionStrategy {
			for _, reward := range milestone.HandlerMetadata.Rewards {
				if reward.Code == "" {
					return fmt.Errorf("invalid use of selection strategy in this Objective: %v. Missing field `Code` for this milestone: %v", objective.ID, milestone.ID)
				}
			}
		}
	}

	if hasRandomStrategyCount > 0 {
		if hasRandomStrategyCount != len(objective.RandomRewards) {
			return fmt.Errorf("Invalid count of milestone rewards versus milestones. Milestones: %v, Rewards: %v", hasRandomStrategyCount, len(objective.RandomRewards))
		}
	}

	return nil
}

// GetCampaignObjectivesByTag returns the objectives for a campaign's by tag.
func (c *Configuration) GetCampaignObjectivesByTag(campaigns []*Campaign, tag string) ([]*Objective, error) {
	objectives := []*Objective{}
	for _, campaign := range campaigns {
		if tag == "" {
			objectives = append(objectives, campaign.Objectives...)
		} else {
			objectives = append(objectives, campaign.ObjectivesByTag[tag]...)
		}
	}
	return objectives, nil
}

// GetObjectivesForDomainAndTag returns the objectives for a domain's campaign by tag.
func (c *Configuration) GetObjectivesForDomainAndTag(domain string, tag string) ([]*Objective, error) {
	campaigns, exists := c.Campaigns[domain]
	if !exists {
		return nil, fmt.Errorf("Domain does not exist: %v", domain)
	}

	objectives, err := c.GetCampaignObjectivesByTag(campaigns, tag)
	if err != nil {
		return nil, err
	}

	return objectives, nil
}

// GetObjectivesForDomain returns all objectives for a domain's active campaign
func (c *Configuration) GetObjectivesForDomain(domain string) ([]*Objective, error) {
	return c.GetObjectivesForDomainAndTag(domain, "")
}

// GetCurrentObjectivesForDomainAndTag returns the objectives for a domain's active campaign by tag.
func (c *Configuration) GetCurrentObjectivesForDomainAndTag(domain string, tag string) ([]*Objective, error) {
	campaigns, err := c.GetCurrentCampaignsForDomain(domain)
	if err != nil {
		return nil, err
	}

	objectives, err := c.GetCampaignObjectivesByTag(campaigns, tag)
	if err != nil {
		return nil, err
	}

	return objectives, nil
}

// GetCurrentObjectivesForDomain returns all objectives for a domain's active campaign
func (c *Configuration) GetCurrentObjectivesForDomain(domain string) ([]*Objective, error) {
	return c.GetCurrentObjectivesForDomainAndTag(domain, "")
}

// GetCurrentCampaignsForDomain returns the active campaigns for a domain.
func (c *Configuration) GetCurrentCampaignsForDomain(domain string) ([]*Campaign, error) {
	campaigns, exists := c.Campaigns[domain]
	if !exists {
		return nil, fmt.Errorf("Domain does not exist: %v", domain)
	}
	now := time.Now()
	activeCampaigns := []*Campaign{}
	for _, campaign := range campaigns {
		if campaign.ActiveStartDate.Before(now) && campaign.ActiveEndDate.After(now) {
			activeCampaigns = append(activeCampaigns, campaign)
		}
	}
	return activeCampaigns, nil
}

// GetCurrentTriggersForDomain returns the triggers for a domain's active campaigns.
func (c *Configuration) GetCurrentTriggersForDomain(domain string) ([]*Trigger, error) {
	campaigns, err := c.GetCurrentCampaignsForDomain(domain)
	if err != nil {
		return nil, err
	}

	triggers := []*Trigger{}
	for _, campaign := range campaigns {
		triggers = append(triggers, campaign.Triggers...)
	}
	return triggers, nil
}

// IsActive returns true if the campaign is active
func (c *Campaign) IsActive() bool {
	now := time.Now()
	return c.ActiveStartDate.Before(now) && c.ActiveEndDate.After(now)
}

func (c *Campaign) IsUserWhitelisted(userID string) bool {
	if !c.UserWhitelistEnabled {
		return true
	}
	return c.UserWhitelist[userID]
}

func (c *Campaign) IsChannelEnabled(channelID string) bool {
	if c.AllChannels {
		return !c.ExcludedChannelList[channelID]
	}
	return c.ChannelList[channelID]
}

func (m *Milestone) IsUserWhitelisted(userID string) bool {
	if !m.UserWhiteListEnabled {
		return true
	}
	return m.UserWhitelist[userID]
}

// ThresholdMet returns true if progress and participation are higher than or equal to the milestone thresholds
func (m *Milestone) ThresholdMet(progress int64, participation int64, claimedMilestones map[string]bool) bool {
	if m.ObjectiveTag == ObjectiveTagCollection { // Ignore progression for collection based milestones
		return m.TriggersMet(claimedMilestones)
	}
	if m.ObjectiveTag == ObjectiveTagCheerbomb { // No concept of thresholds for cheerbombs
		return false
	}
	return progress >= m.Threshold && participation >= m.ParticipationThreshold
}

// TriggersMet returns true if all milestone triggers are in given map of entitlements
func (m *Milestone) TriggersMet(claimedMilestones map[string]bool) bool {
	for _, trigger := range m.HandlerMetadata.MilestoneTriggers {
		if !claimedMilestones[trigger] {
			return false
		}
	}
	return true
}

// IsActive returns true if the milestone is active
func (m *Milestone) IsActive() bool {
	now := time.Now()
	return m.ActiveStartDate.Before(now) && m.ActiveEndDate.After(now)
}

// IsVisible returns true if the milestone is visible
func (m *Milestone) IsVisible(userID string) bool {
	now := time.Now()
	return m.VisibleStartDate.Before(now) && m.VisibleEndDate.After(now) && m.IsUserWhitelisted(userID)
}

func (m *Milestone) hasValidDates() bool {
	return !(m.ActiveEndDate.Before(m.ActiveStartDate) || m.VisibleEndDate.Before(m.VisibleStartDate))
}

// HasActiveMilestones returns true if any milestones are active
func (o *Objective) HasActiveMilestones() bool {
	for _, milestone := range o.Milestones {
		if milestone.IsActive() {
			return true
		}
	}
	return false
}

// GetQuantity returns Quantity field defaulting to 1 if not set
func (r *Reward) GetQuantity() int32 {
	if r.Quantity == 0 {
		return 1
	}
	return r.Quantity
}

// IsActive returns true if the trigger is active
func (t *Trigger) IsActive() bool {
	now := time.Now()
	return t.ActiveStartDate.Before(now) && t.ActiveEndDate.After(now)
}

// IsVisible returns true if the trigger is visible
func (t *Trigger) IsVisible(userID string) bool {
	now := time.Now()
	return t.VisibleStartDate.Before(now) && t.VisibleEndDate.After(now) && t.IsUserWhitelisted(userID)
}

func (t *Trigger) IsUserWhitelisted(userID string) bool {
	if !t.UserWhitelistEnabled {
		return true
	}
	return t.UserWhitelist[userID]
}

// IsUserWhitelisted returns true if the user is whitelisted or no whitelist is active
func (o *Objective) IsUserWhitelisted(userID string) bool {
	if !o.UserWhitelistEnabled {
		return true
	}

	return o.UserWhitelist[userID]
}

func (t *Trigger) GetBackendType() string {
	if t.Campaign != nil {
		return t.Campaign.ParticipationTrackerBackend
	} else {
		return ""
	}
}

func (o *Objective) GetBackendType() string {
	if o.Campaign != nil {
		return o.Campaign.ParticipationTrackerBackend
	} else {
		return ""
	}
}

// IsUserWhitelisted returns true if the user is whitelisted or no whitelist is active
func (c *CheerGroup) IsUserWhitelisted(userID string) bool {
	if !c.UserWhitelistEnabled {
		return true
	}
	return c.UserWhitelist[userID]
}
