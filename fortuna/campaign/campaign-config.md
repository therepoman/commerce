# Campaign Configuration

Campaigns in Fortuna's cheering progression and fulfillment logic are largely based on the configuration in the `campaign` directory. This doc covers how to create a new configuration and how to deploy it.

## Overview

For an overview of the architecture of campaigns, read this [document](https://docs.google.com/document/d/11xcrfUzZr5h-wyV4DOKLtVynOsuXcBqtGNd_5oNhJLc/edit#heading=h.kav8aivm970l
).

**TLDR:** A `campaign` is a timed event that holds a list of `objectives`, which are models of progression with `milestones` along the way. When a user crosses a milestone, they are granted its associated `rewards`. Campaigns can be grouped together by a `domain`, for example: `hgc2018` represents the domain for Hearthstone 2018, and the campaigns would be the competition stages.

For the specific fields and parameters for the aforementioned objects, take a look at the structs in [campaign/config.go](./config.go)

Configurations are based in `.json` files, and placed in environment specific directories.

### Structure

```
# directory file structure
\campaign
  \dev
    example-campaign.json
  \staging
    example-campaign.json
  \prod
    example-campaign.json
```

If there are repeated campaigns/domains in multiple files in the same environment, these configurations will be **MERGED** together when published. This means you can separate your configuration into multiple files if the list of objectives or campaigns becomes too large.

**TBD:** Configuration files will be synced directly with DynamoDB and skip S3 altogether.

## Architecture

`Campaigns` are comprised of `Objectives`. Those objectives have `Milestones` which can grant `Rewards` once the milestone has been achieved. When setting up a new campaign or objective, make sure that your `Id` is self descriptive and unique. Specifically, `Objective.Id` is used by Spade for event tracking purposes. A best practice for creating an objective id is to make reference to the campaign and/or domain to provide quick context when looking through logs or Spade events.

```
  // EXAMPLE
  "Campaigns": {  // TODO: rename this to "Domains"
    "cheerbomb.test": [
      {
        "ActiveEndDate": "2020-02-27T18:00:00Z",
        "ActiveStartDate": "2018-05-14T14:00:00Z",
        "ChannelList": {
          "27697171": true
        },
        "Description": "Kaboom!",
        "Id": "cheerbomb.test.campaign",
        "Objectives": [
          {
            "Description": "A cheerbomb for testing",
            "EventHandler": "CheerbombHandler",
            "Id": "cheerbomb.test.cheerbomb",
            "Milestones": [
                ...
```

If a team needs to be removed from the config post launch, that team should ’soft’ removed from the config by adding the deprecated flag to both `CheerGroup` for that team and objective corresponding to that `CheerGroup`. This should be done instead of actually deleting from the config because the dynamo upload script will not delete rows from the table. Ideally the end/visible dates should also be changed to reflect when the team was removed for readability. 

```
  // EXAMPLE
{
  "Campaigns": {
    “campaigns.test”: [
      {
        "Id": “test”,
        "CheerGroups": {
          “RemovedTeam”: {
            "CheerCodes": {
              “Code”: true
            },
            "Name": “Removed Team“,
            "ImageURL": “some url”,
            "Division": “test”,
            "Deprecated": true
          },
	...
          	{
            	"Id": "hgc2018.launch.cheer.cheergroup.removedteam”,
            	"Description": "Cheer for Removed Team“,
            	"EventHandler": "IndividualCheerHandler",
            	"Tag": "INDIVIDUAL",
            	"CheerGroupKey": "Code",
            	"ShouldAutogrant": true,
            	"Deprecated": true,
            	"Milestones": [
			...
```

If you need to have a reward that can be fulfilled repeatedly and in other milestones (e.g. HGC Loot Boxes appears in multiple milestones), make sure the IGC `SKU` Reward field is the same, and that the reward ids are **unique** for each milestone.

Configuration for campaigns are currently either loaded from an S3 bucket or the DynamoDB depending on the server config (in `config/*.json`) value `UseDynamoDBForCampaigns`. This is a short-term interim state, and we are planning to migrate completely to DynamoDB (this readme will be updated when that happens). For more details, see the Campaign Configuration In DynamoDB section below.

Dev environment is currently pulling from DynamoDB upon requests (with caching), but if you prefer to use your local copy of the JSON based config file for your local testing, you can manually set `UseDynamoDBForCampaigns` to `false`, then it will load the configs from `campaign/test.json`. If this file does not exist, it will load the dev configuration from S3.

## Create a new campaign

A configuration can be created by from scratch or by copying an existing configuration file in one of the environment directories (e.g. `campaign/dev/`).

We also have the `config_controller` command to help create a configuration file and keep it in sync in all of the environment directories. See [cmd/_scripts/config_controller/README.md](cmd/_scripts/config_controller/README.md) on how to run the `config_controller` command to create and sync your configuration file.

```bash
# From Fortuna's root directory:

# install the `config_controller` go binary
make config-controller

# create a new config file named new-cool-thing based off the owl2018 config
config_controller createConfig campaign/dev/owl2018.json campaign/dev/new-cool-thing.json
```

### Channel Properties
The Fortuna `$ENV_channel_properties` DynamoDB tables associate channel IDs to sets of domains. This powers campaigns that run on certain channels. If you are making a campaign for a set of channels instead of all, you will need to do this in addition to the steps outlined below.
This data can be managed from the command line using the `chanprops` command: `go install code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops`. Run `chanprops help` after installing for usage information.


## Deployment

Configurations files are fragmented across domains in our source code for easier management. They are compiled to a single json file before being stored in s3. To compile and upload, run:

```bash
$ go run ./cmd/_scripts/upload_campaign.go -env={environment}
```

Make sure to pay attention to the diff output and upload only when the diffs contain the intended changes.
If you want to only compile and not upload, use `-dry` flag.

__Note__: Your AWS Profile keys must be set accordingly for the environments. `twitch-fortuna-dev` for `dev` and `staging`, and `twitch-fortuna-aws` for `prod`.


### Deployment to DynamoDB

Our campaign configurations are in two places right now, which isn't the ideal state. For now, we should treat the json files checked into the repo as the source of truth. So, when you are updating the config file, please do so directly in the json files, then commit the changes. Use the upload campaign script to upload to S3. To sync this to our DynamoDB, we have the migration script. This script reads the compiled json, validates, and uploads it to our DDB storage. Running scripts like below will compile and upload the json data to DDB.

```bash
$ go run ./cmd/_scripts/upload_campaign.go -env={environment} -dry
$ go run ./cmd/_scripts/dynamo_migration/migrate_campaigns.go -env={environment}
```

Please note that this script currently does not delete previous data. It will use the PUT operation on the rows, which means if you changed an ID field, it will create the row without deleting the previous row with the old ID. Also, any deletion won't be synced. For now, please handle deletes manually in DynamoDB. Deletion should be very rare for campaigns data.
