package campaign

import (
	"strconv"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	TestUserID    = "TestUserID"
	TestChannelID = "TestChannelID"
)

func TestConfig(t *testing.T) {
	Convey("Test Config", t, func() {
		Convey("Test validateMilestones", func() {

			Convey("with no milestones", func() {
				objective, _ := generateObjective([]*Milestone{}, 0)
				err := validateMilestones(objective)
				So(err, ShouldNotBeNil)
			})

			Convey("with mixed strategies", func() {
				milestones := []*Milestone{}
				milestones = append(milestones, generateMilestone("ALL"))
				milestones = append(milestones, generateMilestone("RANDOM"))
				objective, _ := generateObjective(milestones, 1)
				err := validateMilestones(objective)
				So(err, ShouldBeNil)
			})

			Convey("with ALL strategy", func() {
				Convey("with more rewards than milestones", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("ALL"))
					milestones = append(milestones, generateMilestone("ALL"))
					objective, _ := generateObjective(milestones, 7)
					err := validateMilestones(objective)
					So(err, ShouldBeNil)
				})

				Convey("with equal number of rewards and milestones", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("ALL"))
					milestones = append(milestones, generateMilestone("ALL"))
					objective, _ := generateObjective(milestones, 2)
					err := validateMilestones(objective)
					So(err, ShouldBeNil)
				})

				Convey("with more milestones than rewards", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("ALL"))
					milestones = append(milestones, generateMilestone("ALL"))
					objective, _ := generateObjective(milestones, 1)
					err := validateMilestones(objective)
					So(err, ShouldBeNil)
				})
			})

			Convey("with RANDOM strategy", func() {
				Convey("with more rewards than milestones", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("RANDOM"))
					milestones = append(milestones, generateMilestone("RANDOM"))
					objective, _ := generateObjective(milestones, 5)
					err := validateMilestones(objective)
					So(err, ShouldNotBeNil)
				})

				Convey("with equal number of rewards and milestones", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("RANDOM"))
					milestones = append(milestones, generateMilestone("RANDOM"))
					objective, _ := generateObjective(milestones, 2)
					err := validateMilestones(objective)
					So(err, ShouldBeNil)
				})

				Convey("with more milestones than rewards", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("RANDOM"))
					milestones = append(milestones, generateMilestone("RANDOM"))
					objective, _ := generateObjective(milestones, 1)
					err := validateMilestones(objective)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("with REWARDCODE strategy", func() {
				Convey("with missing code", func() {
					milestones := []*Milestone{}
					milestones = append(milestones, generateMilestone("REWARDCODE"))
					objective, rewards := generateObjective(milestones, 1)
					milestones[0].HandlerMetadata.Rewards = rewards
					err := validateMilestones(objective)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("with invalid active dates", func() {
				milestones := []*Milestone{
					generateMilestone(AllSelectionStrategy),
				}

				milestones[0].ActiveStartDate = time.Date(2018, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestones[0].ActiveEndDate = time.Date(2018, time.February, 28, 9, 0, 0, 0, time.UTC)

				// Use valid pair of reciprocal dates to isolate test condition
				milestones[0].VisibleStartDate = time.Date(2018, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestones[0].VisibleEndDate = time.Date(2018, time.March, 11, 9, 0, 0, 0, time.UTC)

				objective, _ := generateObjective(milestones, 0)

				err := validateMilestones(objective)
				So(err, ShouldNotBeNil)
			})

			Convey("with invalid visible dates", func() {
				milestones := []*Milestone{
					generateMilestone(AllSelectionStrategy),
				}

				milestones[0].VisibleStartDate = time.Date(2018, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestones[0].VisibleEndDate = time.Date(2018, time.February, 28, 9, 0, 0, 0, time.UTC)

				// Use valid pair of reciprocal dates to isolate test condition
				milestones[0].ActiveStartDate = time.Date(2018, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestones[0].ActiveEndDate = time.Date(2018, time.March, 11, 9, 0, 0, 0, time.UTC)

				objective, _ := generateObjective(milestones, 0)

				err := validateMilestones(objective)
				So(err, ShouldNotBeNil)
			})
		})
		Convey("Test objectives", func() {
			testDomain := "testDomain"
			inactiveDomain := "inactiveDomain"
			yesterday := time.Now().AddDate(0, 0, -1)
			tomorrow := time.Now().AddDate(0, 0, 1)
			dayAfterTomorrow := time.Now().AddDate(0, 0, 2)

			globalObjective := &Objective{ID: "obj-global"}
			teamObjective := &Objective{ID: "obj-team"}
			individualObjective := &Objective{ID: "obj-individual"}

			inactiveGlobalObjective := &Objective{ID: "inactive-obj-global"}
			inactiveTeamObjective := &Objective{ID: "inactive-obj-team"}
			inactiveIndividualObjective := &Objective{ID: "inactive-obj-individual"}

			objectivesByTag := map[string][]*Objective{
				ObjectiveTagGlobal:     {globalObjective},
				ObjectiveTagTeam:       {teamObjective},
				ObjectiveTagIndividual: {individualObjective},
			}
			inactiveObjectivesByTag := map[string][]*Objective{
				ObjectiveTagGlobal:     {inactiveGlobalObjective},
				ObjectiveTagTeam:       {inactiveTeamObjective},
				ObjectiveTagIndividual: {inactiveIndividualObjective},
			}

			campaigns := map[string][]*Campaign{
				testDomain: {
					&Campaign{
						ActiveStartDate: yesterday,
						ActiveEndDate:   tomorrow,
						Objectives:      []*Objective{globalObjective, teamObjective, individualObjective},
						ObjectivesByTag: objectivesByTag,
					},
				},
				inactiveDomain: {
					&Campaign{
						ActiveStartDate: tomorrow,
						ActiveEndDate:   dayAfterTomorrow,
						Objectives:      []*Objective{inactiveGlobalObjective, inactiveTeamObjective, inactiveIndividualObjective},
						ObjectivesByTag: inactiveObjectivesByTag,
					},
				},
			}
			config := Configuration{Campaigns: campaigns}

			Convey("Test GetObjectivesForDomainAndTag", func() {
				Convey("with invalid domain", func() {
					_, err := config.GetObjectivesForDomainAndTag("invalid", ObjectiveTagGlobal)

					So(err, ShouldNotBeNil)
				})
				Convey("with inactive campaign", func() {
					objectives, err := config.GetObjectivesForDomainAndTag(inactiveDomain, ObjectiveTagGlobal)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{inactiveGlobalObjective})
				})
				Convey("with active campaign and global tag", func() {
					objectives, err := config.GetObjectivesForDomainAndTag(testDomain, ObjectiveTagGlobal)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{globalObjective})
				})
				Convey("with active campaign and team tag", func() {
					objectives, err := config.GetObjectivesForDomainAndTag(testDomain, ObjectiveTagTeam)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{teamObjective})
				})
				Convey("with active campaign and individual tag", func() {
					objectives, err := config.GetObjectivesForDomainAndTag(testDomain, ObjectiveTagIndividual)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{individualObjective})
				})
				Convey("with expected non-visibility due to whitelisting", func() {
					objectives, err := config.GetObjectivesForDomainAndTag(testDomain, ObjectiveTagIndividual)
					So(err, ShouldBeNil)

					objective := objectives[0]
					objective.UserWhitelistEnabled = true
					objective.UserWhitelist = map[string]bool{
						"user1": true,
					}

					So(objective.IsUserWhitelisted("user2"), ShouldBeFalse)
					So(objective.IsUserWhitelisted("user1"), ShouldBeTrue)
				})
				Convey("with expected non-visibility due to deprecation", func() {
					objectives, err := config.GetObjectivesForDomainAndTag(testDomain, ObjectiveTagIndividual)
					So(err, ShouldBeNil)

					objective := objectives[0]
					So(objective.Deprecated, ShouldBeFalse)
				})
			})
			Convey("Test GetObjectivesForDomain", func() {
				Convey("with invalid domain", func() {
					_, err := config.GetObjectivesForDomain("invalid")

					So(err, ShouldNotBeNil)
				})
				Convey("with inactive campaign", func() {
					objectives, err := config.GetObjectivesForDomain(inactiveDomain)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{inactiveGlobalObjective, inactiveTeamObjective, inactiveIndividualObjective})
				})
				Convey("with active campaign", func() {
					objectives, err := config.GetObjectivesForDomain(testDomain)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{globalObjective, teamObjective, individualObjective})
				})
			})
			Convey("Test GetCurrentObjectivesForDomainAndTag", func() {
				Convey("with invalid domain", func() {
					_, err := config.GetCurrentObjectivesForDomainAndTag("invalid", ObjectiveTagGlobal)

					So(err, ShouldNotBeNil)
				})
				Convey("with inactive campaign", func() {
					objectives, err := config.GetCurrentObjectivesForDomainAndTag(inactiveDomain, ObjectiveTagGlobal)

					So(err, ShouldBeNil)
					So(objectives, ShouldBeEmpty)
				})
				Convey("with active campaign and global tag", func() {
					objectives, err := config.GetCurrentObjectivesForDomainAndTag(testDomain, ObjectiveTagGlobal)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{globalObjective})
				})
				Convey("with active campaign and team tag", func() {
					objectives, err := config.GetCurrentObjectivesForDomainAndTag(testDomain, ObjectiveTagTeam)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{teamObjective})
				})
				Convey("with active campaign and individual tag", func() {
					objectives, err := config.GetCurrentObjectivesForDomainAndTag(testDomain, ObjectiveTagIndividual)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{individualObjective})
				})
			})
			Convey("Test GetCurrentObjectivesForDomain", func() {
				Convey("with invalid domain", func() {
					_, err := config.GetCurrentObjectivesForDomain("invalid")

					So(err, ShouldNotBeNil)
				})
				Convey("with inactive campaign", func() {
					objectives, err := config.GetCurrentObjectivesForDomain(inactiveDomain)

					So(err, ShouldBeNil)
					So(objectives, ShouldBeEmpty)
				})
				Convey("with active campaign", func() {
					objectives, err := config.GetCurrentObjectivesForDomain(testDomain)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, []*Objective{globalObjective, teamObjective, individualObjective})
				})
			})
			Convey("Test GetCurrentCampaignsForDomain", func() {
				Convey("with invalid domain", func() {
					_, err := config.GetCurrentCampaignsForDomain("invalid")

					So(err, ShouldNotBeNil)
				})
				Convey("with inactive campaign", func() {
					objectives, err := config.GetCurrentCampaignsForDomain(inactiveDomain)

					So(err, ShouldBeNil)
					So(objectives, ShouldBeEmpty)
				})
				Convey("with active campaign", func() {
					objectives, err := config.GetCurrentCampaignsForDomain(testDomain)

					So(err, ShouldBeNil)
					So(objectives, ShouldResemble, campaigns[testDomain])
				})
			})
		})
		Convey("Test Reward", func() {
			Convey("with zero quantity", func() {
				reward := &Reward{}
				So(reward.GetQuantity(), ShouldEqual, 1)
			})
			Convey("with non-zero quantity", func() {
				reward := &Reward{
					Quantity: 200,
				}
				So(reward.GetQuantity(), ShouldEqual, 200)
			})
		})
		Convey("Test Milestone visibility", func() {
			Convey("with non-visibility due to visible date restraints", func() {
				milestone := generateMilestone("RANDOM")
				milestone.VisibleStartDate = time.Date(2098, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestone.VisibleEndDate = time.Date(2099, time.March, 10, 9, 0, 0, 0, time.UTC)

				So(milestone.IsVisible("user"), ShouldBeFalse)
			})
			Convey("with expected non-visibility due to whitelisting", func() {
				milestone := generateMilestone("RANDOM")
				milestone.VisibleStartDate = time.Date(2000, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestone.VisibleEndDate = time.Date(2099, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestone.UserWhiteListEnabled = true
				milestone.UserWhitelist = map[string]bool{
					"user1": true,
				}

				So(milestone.IsVisible("user2"), ShouldBeFalse)
			})
			Convey("with expected visibility", func() {
				milestone := generateMilestone("RANDOM")
				milestone.VisibleStartDate = time.Date(2000, time.March, 10, 9, 0, 0, 0, time.UTC)
				milestone.VisibleEndDate = time.Date(2099, time.March, 10, 9, 0, 0, 0, time.UTC)

				So(milestone.IsVisible("user1"), ShouldBeTrue)

				milestone.UserWhiteListEnabled = true
				milestone.UserWhitelist = map[string]bool{
					"user1": true,
				}

				So(milestone.IsVisible("user1"), ShouldBeTrue)
			})
		})
		Convey("Test Milestone ThresholdMet", func() {
			Convey("returns false if milestone thresholds are not met", func() {
				milestone := &Milestone{
					Threshold:              1,
					ParticipationThreshold: 1,
				}
				claimMap := map[string]bool{}

				So(milestone.ThresholdMet(0, 0, claimMap), ShouldBeFalse)
			})
			Convey("returns false if milestone participation threshold is not met", func() {
				milestone := &Milestone{
					Threshold:              1,
					ParticipationThreshold: 1,
				}
				claimMap := map[string]bool{}

				So(milestone.ThresholdMet(1, 0, claimMap), ShouldBeFalse)
			})
			Convey("returns false if milestone progress threshold is not met", func() {
				milestone := &Milestone{
					Threshold:              2,
					ParticipationThreshold: 1,
				}
				claimMap := map[string]bool{}

				So(milestone.ThresholdMet(1, 1, claimMap), ShouldBeFalse)
			})
			Convey("returns true if milestone thresholds are met", func() {
				milestone := &Milestone{
					Threshold:              1,
					ParticipationThreshold: 1,
				}
				claimMap := map[string]bool{}

				So(milestone.ThresholdMet(1, 1, claimMap), ShouldBeTrue)
			})
			Convey("returns true if collection milestone triggers are met", func() {
				milestone := &Milestone{
					ObjectiveTag: ObjectiveTagCollection,
					HandlerMetadata: HandlerMetadata{
						MilestoneTriggers: []string{"t1", "t2"},
					},
				}
				claimMap := map[string]bool{"t1": true, "t2": true}

				So(milestone.ThresholdMet(0, 0, claimMap), ShouldBeTrue)
			})
		})
		Convey("Test Milestone TriggersMet", func() {
			Convey("returns false if all triggers are not met", func() {
				milestone := &Milestone{
					HandlerMetadata: HandlerMetadata{
						MilestoneTriggers: []string{"t1", "t2"},
					},
				}
				claimMap := map[string]bool{"t1": true}

				So(milestone.TriggersMet(claimMap), ShouldBeFalse)
			})
			Convey("returns true if all triggers are not met", func() {
				milestone := &Milestone{
					HandlerMetadata: HandlerMetadata{
						MilestoneTriggers: []string{"t1", "t2"},
					},
				}
				claimMap := map[string]bool{"t1": true, "t2": true}

				So(milestone.TriggersMet(claimMap), ShouldBeTrue)
			})
		})

		Convey("Test Triggers", func() {
			trigger := &Trigger{
				ID:     "trigger1",
				Domain: "do",
			}

			Convey("Test Visibility", func() {
				Convey("with non-visibility due to visible date restraints", func() {
					trigger.VisibleStartDate = time.Date(2098, time.March, 10, 9, 0, 0, 0, time.UTC)
					trigger.VisibleEndDate = time.Date(2099, time.March, 10, 9, 0, 0, 0, time.UTC)

					So(trigger.IsVisible("user"), ShouldBeFalse)
				})
				Convey("with expected non-visibility due to whitelisting", func() {
					trigger.VisibleStartDate = time.Date(2000, time.March, 10, 9, 0, 0, 0, time.UTC)
					trigger.VisibleEndDate = time.Date(2099, time.March, 10, 9, 0, 0, 0, time.UTC)
					trigger.UserWhitelistEnabled = true
					trigger.UserWhitelist = map[string]bool{
						"user1": true,
					}

					So(trigger.IsVisible("user2"), ShouldBeFalse)
				})
				Convey("with expected visibility", func() {
					trigger.VisibleStartDate = time.Date(2000, time.March, 10, 9, 0, 0, 0, time.UTC)
					trigger.VisibleEndDate = time.Date(2099, time.March, 10, 9, 0, 0, 0, time.UTC)

					So(trigger.IsVisible("user1"), ShouldBeTrue)

					trigger.UserWhitelistEnabled = true
					trigger.UserWhitelist = map[string]bool{
						"user1": true,
					}

					So(trigger.IsVisible("user1"), ShouldBeTrue)
				})
			})
		})

		Convey("Test Campaigns", func() {
			Convey("Test IsUserWhitelisted", func() {
				Convey("with whitelisted campaign", func() {
					campaign := Campaign{
						UserWhitelistEnabled: true,
						UserWhitelist:        map[string]bool{TestUserID: true},
					}
					Convey("with whitelisted user", func() {
						result := campaign.IsUserWhitelisted(TestUserID)
						So(result, ShouldBeTrue)
					})

					Convey("with non-whitelisted user", func() {
						result := campaign.IsUserWhitelisted("otherUserID")
						So(result, ShouldBeFalse)
					})

					Convey("with no user", func() {
						result := campaign.IsUserWhitelisted("")
						So(result, ShouldBeFalse)
					})
				})
				Convey("with non-whitelisted campaign", func() {
					campaign := Campaign{
						UserWhitelistEnabled: false,
					}
					Convey("with user", func() {
						result := campaign.IsUserWhitelisted(TestUserID)
						So(result, ShouldBeTrue)
					})

					Convey("with no user", func() {
						result := campaign.IsUserWhitelisted("")
						So(result, ShouldBeTrue)
					})
				})
			})

			Convey("Test IsChannelEnabled", func() {
				Convey("with AllChannels-enabled campaign", func() {
					campaign := Campaign{
						AllChannels:         true,
						ExcludedChannelList: map[string]bool{TestChannelID: true},
					}
					Convey("with non-excluded channel", func() {
						result := campaign.IsChannelEnabled("someotherChannelID")
						So(result, ShouldBeTrue)
					})
					Convey("with excluded channel", func() {
						result := campaign.IsChannelEnabled(TestChannelID)
						So(result, ShouldBeFalse)
					})
				})
				Convey("with non-AllChannels-enabled campaign", func() {
					campaign := Campaign{
						ChannelList: map[string]bool{TestChannelID: true},
					}
					Convey("with enabled channel", func() {
						result := campaign.IsChannelEnabled(TestChannelID)
						So(result, ShouldBeTrue)
					})
					Convey("with non-enabled channel", func() {
						result := campaign.IsChannelEnabled("someotherChannelID")
						So(result, ShouldBeFalse)
					})
				})
			})
		})
	})
}

func generateObjective(milestones []*Milestone, numRewards int) (*Objective, []*Reward) {
	rewards := []*Reward{}
	for i := 0; i < numRewards; i++ {
		reward := &Reward{
			ID: strconv.Itoa(i),
		}
		rewards = append(rewards, reward)
	}

	return &Objective{
		ID:            "objective1",
		Milestones:    milestones,
		RandomRewards: rewards,
	}, rewards
}

func generateMilestone(strategy string) *Milestone {
	return &Milestone{
		ID: "milestone1",
		HandlerMetadata: HandlerMetadata{
			Rewards:           []*Reward{},
			SelectionStrategy: strategy,
		},
	}
}
