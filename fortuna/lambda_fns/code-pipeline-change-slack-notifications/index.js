'use strict';

/**
 * Follow these steps to configure the webhook in Slack:
 *
 *   1. Navigate to https://<your-team-domain>.slack.com/services/new
 *
 *   2. Search for and select "Incoming WebHooks".
 *
 *   3. Choose the default channel where messages will be sent and click "Add Incoming WebHooks Integration".
 *
 *   4. Copy the webhook URL from the setup instructions and use it in the next section.
 *
 *
 * To encrypt your secrets use the following steps:
 *
 *  1. Create or use an existing KMS Key - http://docs.aws.amazon.com/kms/latest/developerguide/create-keys.html
 *
 *  2. Click the "Enable Encryption Helpers" checkbox
 *
 *  3. Paste <SLACK_HOOK_URL> into the kmsEncryptedHookUrl environment variable and click encrypt
 *
 *  Note: You must exclude the protocol from the URL (e.g. "hooks.slack.com/services/abc123").
 *
 *  4. Give your function's role permission for the kms:Decrypt action.
 *      Example:

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1443036478000",
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt"
            ],
            "Resource": [
                "<your KMS key ARN>"
            ]
        }
    ]
}

 */

const AWS = require('aws-sdk');
const url = require('url');
const https = require('https');

// The base-64 encoded, encrypted key (CiphertextBlob) stored in the kmsEncryptedHookUrl environment variable
const kmsEncryptedHookUrl = process.env.kmsEncryptedHookUrl;
// The Slack channel to send a message to stored in the slackChannel environment variable
const slackChannel = process.env.slackChannel;
let hookUrl;


function postMessage(message, callback) {
    const body = JSON.stringify(message);
    const options = url.parse(hookUrl);
    options.method = 'POST';
    options.headers = {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(body),
    };

    const postReq = https.request(options, (res) => {
        const chunks = [];
        res.setEncoding('utf8');
        res.on('data', (chunk) => chunks.push(chunk));
        res.on('end', () => {
            if (callback) {
                callback({
                    body: chunks.join(''),
                    statusCode: res.statusCode,
                    statusMessage: res.statusMessage,
                });
            }
        });
        return res;
    });

    postReq.write(body);
    postReq.end();
}

function getPipelineExecution(pipelineName, pipelineExecutionId, cb) {
    const pipeline = new AWS.CodePipeline();
    pipeline.getPipelineExecution({
        pipelineName, pipelineExecutionId
    }, cb);
}

function processEvent(event, callback) {
    const message = JSON.parse(event.Records[0].Sns.Message);
    console.log('SNS messageContents', event.Records[0].Sns.Message)
    
    if (message.approval) {
        processApprovalEvent(message, callback);
    } else if (message.detail['execution-id']) {
        processPipelineChange(message, callback);
    } else {
        console.error(`Not a valid message: ${message}`);
        callback(null);
    }
}

function processApprovalEvent(message, callback) {
    const approval = message.approval;
    const link = url.parse(approval.approvalReviewLink);
    // const link = url.parse('https://console.aws.amazon.com/codepipeline/home?region=us-west-2#/view/fortuna-ci/PromoteToCanary/Promote/approve/140c49e3-5f3d-4695-91bb-10bedaba1148');
    
    const slackMessage = {
        // channel: slackChannel,
        text: `${approval.pipelineName} pipeline - ${approval.customData}`,
        attachments: [{
            title: `<https://isengard.amazon.com/federate?account=180265471281&role=pipeline-approver&destination=${escape(link.path + link.hash)}|Click to approve/reject>`,
            color: 'warning',
            mrkdwn_in: ['title', 'text']
        }]
    };

    
    postMessage(slackMessage, (response) => {
        if (response.statusCode < 400) {
            console.info('Message posted successfully');
            callback(null);
        } else if (response.statusCode < 500) {
            console.error(`Error posting message to Slack API: ${response.statusCode} - ${response.statusMessage}`);
            callback(null);  // Don't retry because the error is due to a problem with the request
        } else {
            // Let Lambda retry
            callback(`Server error when processing message: ${response.statusCode} - ${response.statusMessage}`);
        }
    });
}

function processPipelineChange(message, callback) {
    const detail = message.detail;
    const pipeline = detail.pipeline;
    const pipelineExecutionId = detail['execution-id'];
    const status = detail.state;
    if (pipeline !== 'fortuna-ci' || status == 'STARTED' || (status != 'SUCCEEDED' && detail.type.category == 'Approval') || (status == 'SUCCEEDED' && (detail.stage == 'BETA' || ['Deploy', 'Approval'].indexOf(detail.type.category) == -1))) {
        console.log('Ignoring pipeline execution:', detail);
        callback(null);
        return;
    }
    const messageString = `${pipeline} pipeline status has changed: *${detail.stage} - ${status}*`;
    const slackMessage = {
        // channel: slackChannel,
        text: messageString,
        attachments: [{
            text: '<https://isengard.amazon.com/federate?account=180265471281&role=pipeline-approver&destination=/codepipeline/home%3Fregion%3Dus-west-2%23/view/fortuna-ci|View CodePipeline>',
            color: status == 'FAILED' ? 'danger' : status == 'SUCCEEDED' ? 'good' : '',
            mrkdwn_in: ['title', 'text']
        }]
    };

    getPipelineExecution(pipeline, pipelineExecutionId, (err, res) => {
        if (err) {
            console.error(`Error retrieving pipeline execution ${pipelineExecutionId}: ${err}`);
            callback(null);
            return;
        }
        const execution = res.pipelineExecution;
        const revision = execution.artifactRevisions[0];
        slackMessage.attachments[0].title = `<https://git.xarth.tv/commerce/fortuna/commit/${revision.revisionId}|${status} - [pipeline] - ${revision.revisionSummary.split('\n')[0]}>`;
        postMessage(slackMessage, (response) => {
            if (response.statusCode < 400) {
                console.info('Message posted successfully');
                callback(null);
            } else if (response.statusCode < 500) {
                console.error(`Error posting message to Slack API: ${response.statusCode} - ${response.statusMessage}`);
                callback(null);  // Don't retry because the error is due to a problem with the request
            } else {
                // Let Lambda retry
                callback(`Server error when processing message: ${response.statusCode} - ${response.statusMessage}`);
            }
        }); 
    });
}


exports.handler = (event, context, callback) => {
    if (hookUrl) {
        // Container reuse, simply process the event with the key in memory
        processEvent(event, callback);
    } else if (kmsEncryptedHookUrl && kmsEncryptedHookUrl !== '<kmsEncryptedHookUrl>') {
        const encryptedBuf = new Buffer(kmsEncryptedHookUrl, 'base64');
        const cipherText = { CiphertextBlob: encryptedBuf };

        const kms = new AWS.KMS();
        kms.decrypt(cipherText, (err, data) => {
            if (err) {
                console.log('Decrypt error:', err);
                return callback(err);
            }
            hookUrl = `https://${data.Plaintext.toString('ascii')}`;
            processEvent(event, callback);
        });
    } else {
        callback('Hook URL has not been set.');
    }
};
