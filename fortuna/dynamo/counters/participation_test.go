package counters

import (
	"testing"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestParticipation(t *testing.T) {
	Convey("With table participation", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewParticipationDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("GetParticipation", func() {
			userId := "user"
			objectiveTrackId := "objectiveTrackId"

			resp, err := dao.GetParticipation(userId, objectiveTrackId)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("UpdateParticipation", func() {
			userID := "user"
			objectiveTrackID := "objectiveTrackId"
			var amountToAdd int64
			amountToAdd = 42069
			eventID := "eventID"

			err := dao.UpdateParticipation(userID, objectiveTrackID, amountToAdd, eventID)
			So(err, ShouldBeNil)

			resp, err := dao.GetParticipation(userID, objectiveTrackID)

			So(err, ShouldBeNil)
			So(resp.Value, ShouldEqual, amountToAdd)
			So(resp.LastEventId, ShouldEqual, eventID)
			So(resp.RangeID, ShouldEqual, objectiveTrackID)
			So(resp.HashID, ShouldEqual, userID)

			Convey("BatchGetParticipation", func() {
				//items not in the set should not nuke the whole call.
				mapResp, err := dao.BatchGetParticipation(userID, []string{objectiveTrackID, "notInSet"})
				So(err, ShouldBeNil)
				So(mapResp[objectiveTrackID], ShouldEqual, resp.Value)
			})

			Convey("Update and Get Participation with last event", func() {
				//update should not take effect, since it shares eventID
				err = dao.UpdateParticipation(userID, objectiveTrackID, amountToAdd, eventID)
				So(err, ShouldBeNil)

				resp, err = dao.GetParticipation(userID, objectiveTrackID)

				So(err, ShouldBeNil)
				So(resp.Value, ShouldEqual, amountToAdd)
				So(resp.LastEventId, ShouldEqual, eventID)
				So(resp.RangeID, ShouldEqual, objectiveTrackID)
				So(resp.HashID, ShouldEqual, userID)
			})

			Convey("Update and Get Participation with new event", func() {
				//update should take effect, since it does not share eventID
				err = dao.UpdateParticipation(userID, objectiveTrackID, 100, eventID+"2")
				So(err, ShouldBeNil)

				resp, err = dao.GetParticipation(userID, objectiveTrackID)

				So(err, ShouldBeNil)
				So(resp.Value, ShouldEqual, amountToAdd+100)
				So(resp.RangeID, ShouldEqual, objectiveTrackID)
				So(resp.HashID, ShouldEqual, userID)
			})
		})
	})
}
