package counters

import (
	"code.justin.tv/commerce/fortuna/config"
	dynamo2 "code.justin.tv/commerce/fortuna/dynamo"
	"github.com/guregu/dynamo"
)

type ParticipationDAO struct {
	table    dynamo.Table
	client   *dynamo.DB
	tokenDAO IAtomicCounterDAO
}

type IParticipationDAO interface {
	GetParticipation(userID string, objectiveTrackID string) (*CounterValue, error)
	UpdateParticipation(userID string, objectiveTrackID string, amountToAdd int64, eventId string) error
	BatchGetParticipation(userID string, objectiveTrackIDs []string) (map[string]int64, error)
	CreateTable() error
	DeleteTable() error
}

const (
	ParticipationTableName = "participation"
)

// NewRewardDAO creates a new DAO
func NewParticipationDAO(db *dynamo.DB, cfg *config.Configuration) IParticipationDAO {
	table := db.Table(dynamo2.FormatTableName(cfg.EnvironmentPrefix, ParticipationTableName))
	tokenDAO := NewTokenTrackingDAO(ParticipationTableName, db, cfg)
	return &ParticipationDAO{
		table,
		db,
		tokenDAO,
	}
}

func (r *ParticipationDAO) GetParticipation(userID string, objectiveTrackID string) (*CounterValue, error) {
	return r.tokenDAO.GetCounterValue(userID, objectiveTrackID)
}

func (r *ParticipationDAO) UpdateParticipation(userID string, objectiveTrackID string, amountToAdd int64, eventId string) error {
	return r.tokenDAO.UpdateCounterValue(userID, objectiveTrackID, amountToAdd, eventId)
}

func (r *ParticipationDAO) BatchGetParticipation(userID string, objectiveTrackIDs []string) (map[string]int64, error) {
	return r.tokenDAO.BatchGetCounterValue(userID, objectiveTrackIDs)
}

// CreateTable for Participation. FOR TESTING ONLY
func (r *ParticipationDAO) CreateTable() error {
	return r.tokenDAO.CreateTable()
}

// DeleteTable for Reward. FOR TESTING ONLY
func (r *ParticipationDAO) DeleteTable() error {
	return r.tokenDAO.DeleteTable()

}
