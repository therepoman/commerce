package counters

import (
	"time"

	"code.justin.tv/commerce/fortuna/config"
	dynamo2 "code.justin.tv/commerce/fortuna/dynamo"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

type CounterValue struct {
	HashID        string    `dynamo:"hashId,hash"`
	RangeID       string    `dynamo:"rangeId,range"`
	LastUpdatedAt time.Time `dynamo:"lastUpdatedAt"`
	Value         int64     `dynamo:"value"`
	LastEventId   string    `dynamo:"lastEventId"`
}

const (
	CounterHashKeyName  = "hashId"
	CounterRangeKeyName = "rangeId"
)

type AtomicCounterDAO struct {
	table    dynamo.Table
	client   *dynamo.DB
	hashKey  string
	rangeKey string
}

type IAtomicCounterDAO interface {
	GetCounterValue(userID string, groupingID string) (*CounterValue, error)
	UpdateCounterValue(hashKey string, rangeKey string, amountToAdd int64, eventId string) error
	BatchGetCounterValue(userID string, groupingID []string) (map[string]int64, error)
	BatchGetCounterValuesByKeys(keys []dynamo.Keyed) (map[string]map[string]int64, error)
	SetupBatchGet(hashKey string, rangeKeys []string) []dynamo.Keyed
	CreateTable() error
	DeleteTable() error
}

// NewTokenTrackingDAO creates a new DAO
func NewTokenTrackingDAO(tableName string, db *dynamo.DB, cfg *config.Configuration) IAtomicCounterDAO {
	table := db.Table(dynamo2.FormatTableName(cfg.EnvironmentPrefix, tableName))

	return &AtomicCounterDAO{
		table,
		db,
		CounterHashKeyName,
		CounterRangeKeyName,
	}
}

func (r *AtomicCounterDAO) GetCounterValue(hashKey string, rangeKey string) (*CounterValue, error) {
	var result *CounterValue
	err := r.table.Get(r.hashKey, hashKey).Range(r.rangeKey, dynamo.Equal, rangeKey).One(&result)

	if err != nil {
		// Not an error, just no participation entry
		if err.Error() == dynamo.ErrNotFound.Error() {
			return nil, nil
		}
		return nil, err
	}
	return result, nil
}

func (r *AtomicCounterDAO) UpdateCounterValue(hashID string, rangeID string, amountToAdd int64, eventId string) error {
	err := r.table.Update(r.hashKey, hashID).Range(r.rangeKey, rangeID).
		If("'lastEventId' <> ?", eventId).
		Add("value", amountToAdd).
		Set("lastEventId", eventId).
		Run()

	// check the exception; if the conditional failed then we're
	// okay because the attempted update has already taken place.
	if err != nil {
		if ae, ok := err.(awserr.RequestFailure); ok {
			if ae.Code() == "ConditionalCheckFailedException" {
				return nil
			} else {
				return err
			}
		}
	}
	return nil
}

func (r *AtomicCounterDAO) BatchGetCounterValue(hashKey string, rangeKeys []string) (map[string]int64, error) {
	var results []CounterValue

	err := r.table.Batch(r.hashKey, r.rangeKey).Get(r.SetupBatchGet(hashKey, rangeKeys)...).All(&results)

	if err != nil {
		return nil, err
	} else {
		resp := make(map[string]int64)
		for _, result := range results {
			resp[result.RangeID] = result.Value
		}
		return resp, nil
	}
}

func (r *AtomicCounterDAO) BatchGetCounterValuesByKeys(keys []dynamo.Keyed) (map[string]map[string]int64, error) {
	var results []CounterValue

	err := r.table.Batch(CounterHashKeyName, CounterRangeKeyName).Get(keys...).All(&results)

	if err != nil {
		return nil, err
	} else {
		resp := make(map[string]map[string]int64)
		for _, result := range results {
			if _, exists := resp[result.HashID]; !exists {
				resp[result.HashID] = make(map[string]int64)
			}
			resp[result.HashID][result.RangeID] = result.Value
		}
		return resp, nil
	}
}

func (r *AtomicCounterDAO) SetupBatchGet(hashKey string, rangeKeys []string) []dynamo.Keyed {
	var keys []dynamo.Keyed
	keys = make([]dynamo.Keyed, len(rangeKeys))

	for i := range rangeKeys {
		keys[i] = dynamo.Keys{hashKey, rangeKeys[i]}
	}

	return keys
}

// CreateTable for Token. FOR TESTING ONLY
func (r *AtomicCounterDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(r.table.Name(), r.client) {
		return nil
	}
	err := r.client.CreateTable(r.table.Name(), CounterValue{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Token. FOR TESTING ONLY
func (r *AtomicCounterDAO) DeleteTable() error {
	return r.table.DeleteTable().Run()

}
