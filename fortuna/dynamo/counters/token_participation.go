package counters

import (
	"code.justin.tv/commerce/fortuna/config"
	dynamo2 "code.justin.tv/commerce/fortuna/dynamo"
	"github.com/guregu/dynamo"
)

type TokenParticipationDAO struct {
	table    dynamo.Table
	client   *dynamo.DB
	tokenDAO IAtomicCounterDAO
}

type ITokenParticipationDAO interface {
	GetTokenParticipation(userID string, groupingID string) (*CounterValue, error)
	UpdateTokenParticipation(userID string, groupingID string, amountToAdd int64, eventId string) error
	BatchGetTokenParticipation(userID string, groupingID []string) (map[string]int64, error)
	CreateTable() error
	DeleteTable() error
}

const (
	TokenParticipationTableName = "TokenParticipation"
)

// NewRewardDAO creates a new DAO
func NewTokenParticipationDAO(db *dynamo.DB, cfg *config.Configuration) ITokenParticipationDAO {
	table := db.Table(dynamo2.FormatTableName(cfg.EnvironmentPrefix, TokenParticipationTableName))
	tokenDAO := NewTokenTrackingDAO(TokenParticipationTableName, db, cfg)
	return &TokenParticipationDAO{
		table,
		db,
		tokenDAO,
	}
}

func (r *TokenParticipationDAO) GetTokenParticipation(userID string, groupingID string) (*CounterValue, error) {
	return r.tokenDAO.GetCounterValue(userID, groupingID)
}

func (r *TokenParticipationDAO) UpdateTokenParticipation(userID string, groupingID string, amountToAdd int64, eventId string) error {
	return r.tokenDAO.UpdateCounterValue(userID, groupingID, amountToAdd, eventId)
}

func (r *TokenParticipationDAO) BatchGetTokenParticipation(userID string, groupingIDs []string) (map[string]int64, error) {
	return r.tokenDAO.BatchGetCounterValue(userID, groupingIDs)
}

// CreateTable for TokenParticipation. FOR TESTING ONLY
func (r *TokenParticipationDAO) CreateTable() error {
	return r.tokenDAO.CreateTable()
}

// DeleteTable for TokenParticipation. FOR TESTING ONLY
func (r *TokenParticipationDAO) DeleteTable() error {
	return r.tokenDAO.DeleteTable()

}
