package counters

import (
	"testing"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTokenProgress(t *testing.T) {
	Convey("With table progress", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewTokenProgressDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("GetProgress", func() {
			userId := "user"
			objectiveTrackId := "objectiveTrackId"

			resp, err := dao.GetTokenProgress(userId, objectiveTrackId)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("UpdateProgress", func() {
			userID := "user"
			groupingID := "groupingID"
			var amountToAdd int64
			amountToAdd = 42069
			eventID := "eventID"

			err := dao.UpdateTokenProgress(userID, groupingID, amountToAdd, eventID)
			So(err, ShouldBeNil)

			resp, err := dao.GetTokenProgress(userID, groupingID)

			So(err, ShouldBeNil)
			So(resp.Value, ShouldEqual, amountToAdd)
			So(resp.LastEventId, ShouldEqual, eventID)
			So(resp.RangeID, ShouldEqual, groupingID)
			So(resp.HashID, ShouldEqual, userID)

			Convey("BatchGetProgress", func() {
				//items not in the set should not nuke the whole call.
				mapResp, err := dao.BatchGetTokenProgress(userID, []string{groupingID, "notInSet"})
				So(err, ShouldBeNil)
				So(mapResp[groupingID], ShouldEqual, resp.Value)
			})

			Convey("Update and Get Progress with last event", func() {
				//update should not take effect, since it shares eventID
				err = dao.UpdateTokenProgress(userID, groupingID, amountToAdd, eventID)
				So(err, ShouldBeNil)

				resp, err = dao.GetTokenProgress(userID, groupingID)

				So(err, ShouldBeNil)
				So(resp.Value, ShouldEqual, amountToAdd)
				So(resp.LastEventId, ShouldEqual, eventID)
				So(resp.RangeID, ShouldEqual, groupingID)
				So(resp.HashID, ShouldEqual, userID)
			})

			Convey("Update and Get Progress with new event", func() {
				//update should take effect, since it does not share eventID
				err = dao.UpdateTokenProgress(userID, groupingID, 100, eventID+"2")
				So(err, ShouldBeNil)

				resp, err = dao.GetTokenProgress(userID, groupingID)

				So(err, ShouldBeNil)
				So(resp.Value, ShouldEqual, amountToAdd+100)
				So(resp.RangeID, ShouldEqual, groupingID)
				So(resp.HashID, ShouldEqual, userID)
			})
		})
	})
}
