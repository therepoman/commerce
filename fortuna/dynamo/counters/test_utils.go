package counters

const (
	tableNamePrefixSeperator = "_"
	testTablePrefix          = "test"
)

func FormatTableName(prefix string, name string) string {
	return prefix + tableNamePrefixSeperator + name
}
