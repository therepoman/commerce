package counters

import (
	"testing"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
)

func TestProgress(t *testing.T) {
	Convey("With table progress", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewProgressDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("GetProgress", func() {
			objectiveId := "user"
			objectiveTrackId := "objectiveTrackId"

			resp, err := dao.GetProgress(objectiveId, objectiveTrackId)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("UpdateProgress", func() {
			objectiveId := "user"
			objectiveTrackID := "objectiveTrackId"
			var amountToAdd int64
			amountToAdd = 42069
			eventID := "eventID"

			err := dao.UpdateProgress(objectiveId, objectiveTrackID, amountToAdd, eventID)
			So(err, ShouldBeNil)

			resp, err := dao.GetProgress(objectiveId, objectiveTrackID)

			So(err, ShouldBeNil)
			So(resp.Value, ShouldEqual, amountToAdd)
			So(resp.LastEventId, ShouldEqual, eventID)
			So(resp.RangeID, ShouldEqual, objectiveTrackID)
			So(resp.HashID, ShouldEqual, objectiveId)

			Convey("BatchGetProgress", func() {
				//items not in the set should not nuke the whole call.
				var batchRequestKeys []dynamo.Keyed
				batchRequestKeys = append(batchRequestKeys, dynamo.Keys{objectiveId, objectiveTrackID})
				batchRequestKeys = append(batchRequestKeys, dynamo.Keys{objectiveId, "notInSet"})
				mapResp, err := dao.BatchGetProgress(batchRequestKeys)
				So(err, ShouldBeNil)
				So(mapResp[objectiveId][objectiveTrackID], ShouldEqual, resp.Value)
			})

			Convey("Update and Get Progress with last event", func() {
				//update should not take effect, since it shares eventID
				err = dao.UpdateProgress(objectiveId, objectiveTrackID, amountToAdd, eventID)
				So(err, ShouldBeNil)

				resp, err = dao.GetProgress(objectiveId, objectiveTrackID)

				So(err, ShouldBeNil)
				So(resp.Value, ShouldEqual, amountToAdd)
				So(resp.LastEventId, ShouldEqual, eventID)
				So(resp.RangeID, ShouldEqual, objectiveTrackID)
				So(resp.HashID, ShouldEqual, objectiveId)
			})

			Convey("Update and Get Progress with new event", func() {
				//update should take effect, since it does not share eventID
				err = dao.UpdateProgress(objectiveId, objectiveTrackID, 100, eventID+"2")
				So(err, ShouldBeNil)

				resp, err = dao.GetProgress(objectiveId, objectiveTrackID)

				So(err, ShouldBeNil)
				So(resp.Value, ShouldEqual, amountToAdd+100)
				So(resp.RangeID, ShouldEqual, objectiveTrackID)
				So(resp.HashID, ShouldEqual, objectiveId)
			})
		})
	})
}
