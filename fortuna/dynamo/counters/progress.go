package counters

import (
	"code.justin.tv/commerce/fortuna/config"
	dynamo2 "code.justin.tv/commerce/fortuna/dynamo"
	"github.com/guregu/dynamo"
)

type ProgressDAO struct {
	table    dynamo.Table
	client   *dynamo.DB
	tokenDAO IAtomicCounterDAO
}

type IProgressDAO interface {
	GetProgress(objectiveID string, objectiveTrackID string) (*CounterValue, error)
	UpdateProgress(objectiveID string, objectiveTrackID string, amountToAdd int64, eventId string) error
	BatchGetProgress(keys []dynamo.Keyed) (map[string]map[string]int64, error)
	CreateTable() error
	DeleteTable() error
}

const (
	progressTableName = "progress"
)

// NewRewardDAO creates a new DAO
func NewProgressDAO(db *dynamo.DB, cfg *config.Configuration) IProgressDAO {
	table := db.Table(dynamo2.FormatTableName(cfg.EnvironmentPrefix, progressTableName))
	tokenDAO := NewTokenTrackingDAO(progressTableName, db, cfg)
	return &ProgressDAO{
		table,
		db,
		tokenDAO,
	}
}

func (r *ProgressDAO) GetProgress(objectiveID string, objectiveTrackID string) (*CounterValue, error) {
	return r.tokenDAO.GetCounterValue(objectiveID, objectiveTrackID)
}

func (r *ProgressDAO) UpdateProgress(objectiveID string, objectiveTrackID string, amountToAdd int64, eventId string) error {
	return r.tokenDAO.UpdateCounterValue(objectiveID, objectiveTrackID, amountToAdd, eventId)
}

func (r *ProgressDAO) BatchGetProgress(keys []dynamo.Keyed) (map[string]map[string]int64, error) {
	return r.tokenDAO.BatchGetCounterValuesByKeys(keys)
}

// CreateTable for Progress. FOR TESTING ONLY
func (r *ProgressDAO) CreateTable() error {
	return r.tokenDAO.CreateTable()
}

// DeleteTable for Reward. FOR TESTING ONLY
func (r *ProgressDAO) DeleteTable() error {
	return r.tokenDAO.DeleteTable()

}
