package counters

import (
	"code.justin.tv/commerce/fortuna/config"
	dynamo2 "code.justin.tv/commerce/fortuna/dynamo"
	"github.com/guregu/dynamo"
)

type TokenProgressDAO struct {
	table    dynamo.Table
	client   *dynamo.DB
	tokenDAO IAtomicCounterDAO
}

type ITokenProgressDAO interface {
	GetTokenProgress(userID string, groupingID string) (*CounterValue, error)
	UpdateTokenProgress(userID string, groupingID string, amountToAdd int64, eventId string) error
	BatchGetTokenProgress(userID string, groupingID []string) (map[string]int64, error)
	CreateTable() error
	DeleteTable() error
}

const (
	TokenProgressTableName = "TokenProgress"
)

// NewRewardDAO creates a new DAO
func NewTokenProgressDAO(db *dynamo.DB, cfg *config.Configuration) ITokenProgressDAO {
	table := db.Table(dynamo2.FormatTableName(cfg.EnvironmentPrefix, TokenProgressTableName))
	tokenDAO := NewTokenTrackingDAO(TokenProgressTableName, db, cfg)

	return &TokenProgressDAO{
		table,
		db,
		tokenDAO,
	}
}

func (r *TokenProgressDAO) GetTokenProgress(userID string, groupingID string) (*CounterValue, error) {
	return r.tokenDAO.GetCounterValue(userID, groupingID)
}

func (r *TokenProgressDAO) UpdateTokenProgress(userID string, groupingID string, amountToAdd int64, eventId string) error {
	return r.tokenDAO.UpdateCounterValue(userID, groupingID, amountToAdd, eventId)
}

func (r *TokenProgressDAO) BatchGetTokenProgress(userID string, groupingIDs []string) (map[string]int64, error) {
	return r.tokenDAO.BatchGetCounterValue(userID, groupingIDs)
}

// CreateTable for TokenProgress. FOR TESTING ONLY
func (r *TokenProgressDAO) CreateTable() error {
	return r.tokenDAO.CreateTable()
}

// DeleteTable for TokenProgress. FOR TESTING ONLY
func (r *TokenProgressDAO) DeleteTable() error {
	return r.tokenDAO.DeleteTable()

}
