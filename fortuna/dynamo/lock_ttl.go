package dynamo

import (
	"time"

	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	lockTtlTableName = "lock_ttl"
	lockTtlHashKey   = "key"
)

type LockTtlDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

type LockTtl struct {
	Key            string `dynamo:"key,hash"` // Hash Key
	ExpirationTime int64  `dynamo:"TTL"`
}

type ILockTtlDAO interface {
	GetExistingLock(key string) (string, error)
	ObtainLock(key string, ttlSeconds int64) error
	ReleaseLock(key string) error
}

func NewLockTtlDAO(client *dynamo.DB, cfg *config.Configuration) *LockTtlDAO {
	return &LockTtlDAO{
		client: client,
		table:  client.Table(FormatTableName(cfg.EnvironmentPrefix, lockTtlTableName)),
	}
}

func (lt *LockTtlDAO) GetExistingLock(key string) (string, error) {
	var result LockTtl
	err := lt.table.Get(lockTtlHashKey, key).One(&result)
	if err == dynamo.ErrNotFound {
		return "", nil
	}
	return result.Key, err
}

func (lt *LockTtlDAO) ObtainLock(key string, ttlSeconds int64) error {
	var input LockTtl
	input.Key = key
	input.ExpirationTime = time.Now().Unix() + ttlSeconds
	return lt.table.Put(&input).If("attribute_not_exists($)", lockTtlHashKey).Run()
}

func (lt *LockTtlDAO) ReleaseLock(key string) error {
	return lt.table.Delete(lockTtlHashKey, key).Run()
}

// CreateTable for LockTTL. FOR TESTING ONLY
func (dao *LockTtlDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}
	err := dao.client.CreateTable(dao.table.Name(), LockTtl{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

// DeleteTable for LockTTL. FOR TESTING ONLY
func (dao *LockTtlDAO) DeleteTable() error {
	return dao.table.DeleteTable().Run()
}
