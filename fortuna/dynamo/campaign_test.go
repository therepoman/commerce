package dynamo

import (
	"testing"
	"time"

	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCampaign(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewCampaignDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("AddCampaign", func() {
			cheerCodes := map[string]bool{
				"ABC": true,
			}
			cheerGroup := campaignCfg.CheerGroup{
				CheerCodes: cheerCodes,
				Division:   "div",
				Name:       "cool group",
				ImageURL:   "abc.com/a.png",
			}
			cheerGroups := map[string]*campaignCfg.CheerGroup{
				"GroupeKey1": &cheerGroup,
			}
			channelList := map[string]bool{
				"123": true,
				"456": true,
			}
			excluededChannelList := map[string]bool{
				"789": true,
			}
			input := &campaignCfg.Campaign{
				ID:                  "hgc2",
				ActiveStartDate:     time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC),
				ActiveEndDate:       time.Date(2088, time.February, 7, 9, 0, 0, 0, time.UTC),
				Title:               "Heroes of the Storm",
				Domain:              "hgc",
				CheerGroups:         cheerGroups,
				ChannelList:         channelList,
				ExcludedChannelList: excluededChannelList,
				AllChannels:         true,
			}

			err := dao.AddCampaign(input)
			So(err, ShouldBeNil)

			Convey("GetCampaignsByDomain returns campaigns given domain", func() {
				result, err := dao.GetCampaignsByDomain("hgc")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetAllChannelsCampaign returns campaigns with allChannels set to true", func() {
				result, err := dao.GetAllChannelsCampaigns()
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0].AllChannels, ShouldBeTrue)
			})

			Convey("GetCampaignByID returns campaign with given id", func() {
				result, err := dao.GetCampaignByID("hgc2")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result, ShouldResemble, input)
			})
		})
	})
}
