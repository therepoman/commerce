package dynamo

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

const (
	claimableRewardsTableName = "claimable_rewards"

	claimableRewardsHashKey  = "userId"
	claimableRewardsRangeKey = "scopedRewardId"
)

type ClaimableReward struct {
	UserID         string    `dynamo:"userId,hash"`
	ScopedRewardID string    `dynamo:"scopedRewardId,range"`
	Domain         string    `dynamo:"domain"`
	RewardType     string    `dynamo:"rewardType"`
	RewardID       string    `dynamo:"rewardId"`
	MilestoneID    string    `dynamo:"milestoneId"`
	TriggerID      string    `dynamo:"triggerId"`
	Quantity       int32     `dynamo:"quantity"`
	Repeatable     bool      `dynamo:"repeatable"`
	CreatedTime    time.Time `dynamo:"createdTime"`
	IsGift         bool      `dynamo:"isGift"`
	TTL            time.Time `dynamo:"ttl"`
}

func (r *ClaimableReward) GetQuantity() int32 {
	if r.Quantity == 0 {
		return 1
	}
	return r.Quantity
}

type ClaimableRewardDAO struct {
	table   dynamo.Table
	client  *dynamo.DB
	limiter *rate.Limiter
}

type IClaimableRewardDAO interface {
	GetUserRewardsForDomain(userID string, domain string) ([]*ClaimableReward, error)
	GetUserRewardsByID(userID string, domain string, rewardType string, rewardID string) ([]*ClaimableReward, error)
	AddClaimableReward(input *ClaimableReward) error
	DeleteClaimableRewardsForUserAndDomain(userID string, domain string) error
	ThrottledAddClaimableReward(input *ClaimableReward) error
	GetClaimableMapForUser(userID string, domain string) (map[string]int32, error)
	CreateTable() error
	DeleteTable() error
}

// NewClaimableRewardDAO creates a new DAO
func NewClaimableRewardDAO(db *dynamo.DB, cfg *config.Configuration) IClaimableRewardDAO {
	table := db.Table(FormatTableName(cfg.EnvironmentPrefix, claimableRewardsTableName))
	limiter := rate.NewLimiter(rate.Limit(cfg.RewardDynamoWriteRateLimit), cfg.RewardDynamoWriteBurstLimit)

	return &ClaimableRewardDAO{
		table,
		db,
		limiter,
	}
}

// GetUserRewardsForDomain returns all rewards for the user belonging to a single domain
func (r *ClaimableRewardDAO) GetUserRewardsForDomain(userID string, domain string) ([]*ClaimableReward, error) {
	var result []*ClaimableReward

	err := r.table.Get(claimableRewardsHashKey, userID).
		Consistent(true).
		Range(claimableRewardsRangeKey, dynamo.BeginsWith, domain).
		All(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// GetUserRewardsByID returns all rewards for the user matching domain, rewardtype, and rewardID.
func (r *ClaimableRewardDAO) GetUserRewardsByID(userID string, domain string, rewardType string, rewardID string) ([]*ClaimableReward, error) {
	var result []*ClaimableReward

	err := r.table.Get(claimableRewardsHashKey, userID).
		Consistent(true).
		Range(claimableRewardsRangeKey, dynamo.BeginsWith, BuildScopedClaimableRewardID(domain, rewardType, rewardID, 0)).
		All(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// AddClaimableReward adds a new claimable reward row.
func (r *ClaimableRewardDAO) AddClaimableReward(input *ClaimableReward) error {
	input.TTL = time.Now().AddDate(0, 0, 90)
	return r.table.Put(input).Run()
}

// DeleteClaimableRewardsForUserAndDomain cleans up all the rewards marked as claimable for a single user.
// Warning: This is intented to be only run by tests. Don't run this on a real customer!
func (r *ClaimableRewardDAO) DeleteClaimableRewardsForUserAndDomain(userID string, domain string) error {
	result, err := r.GetUserRewardsForDomain(userID, domain)
	if err != nil {
		return err
	}
	for _, reward := range result {
		if err := r.table.Delete(claimableRewardsHashKey, reward.UserID).Range(claimableRewardsRangeKey, reward.ScopedRewardID).Run(); err != nil {
			return err
		}
	}

	return nil
}

// ThrottledAddClaimableReward runs AddClaimableReward with throttling.
func (r *ClaimableRewardDAO) ThrottledAddClaimableReward(input *ClaimableReward) error {
	r.limiter.Wait(context.Background())
	return r.AddClaimableReward(input)
}

// GetClaimableMapForUser returns a map of reward ID => number of claimable reward rows in DDB
func (r *ClaimableRewardDAO) GetClaimableMapForUser(userID string, domain string) (map[string]int32, error) {
	rewards, err := r.GetUserRewardsForDomain(userID, domain)

	if err != nil {
		return nil, errors.Wrap(err, "Could not get claimable rewards from the DynamoDB table")
	}

	claimableMap := map[string]int32{}

	for _, reward := range rewards {
		curr, ok := claimableMap[reward.RewardID]
		if !ok {
			claimableMap[reward.RewardID] = reward.GetQuantity()
		} else {
			claimableMap[reward.RewardID] = curr + reward.GetQuantity()
		}
	}

	return claimableMap, nil
}

// CreateTable for Reward. FOR TESTING ONLY
func (r *ClaimableRewardDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(r.table.Name(), r.client) {
		return nil
	}
	err := r.client.CreateTable(r.table.Name(), Reward{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Reward. FOR TESTING ONLY
func (r *ClaimableRewardDAO) DeleteTable() error {
	return r.table.DeleteTable().Run()
}

// BuildScopedClaimableRewardID builds a scopedRewardID string from the input domain, rewardType, rewardID, and optional count.
func BuildScopedClaimableRewardID(domain string, rewardType string, rewardID string, count int) string {
	if count == 0 {
		return fmt.Sprintf("%s:%s:%s", domain, rewardType, rewardID)
	}
	return fmt.Sprintf("%s:%s:%s:%d", domain, rewardType, rewardID, count)
}
