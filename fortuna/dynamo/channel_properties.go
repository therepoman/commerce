package dynamo

import (
	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	channelPropertiesTableName = "channel_properties"
	channelPropertiesChannelID = "channelId"
	channelPropertiesDomains   = "domains"
	channelPropertiesHasPass   = "hasPass"
)

// ChannelPropertiesDAO holds table participants table
type ChannelPropertiesDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

// IChannelPropertiesDAO interface
type IChannelPropertiesDAO interface {
	GetChannelProperties(channelID string) (*ChannelProperties, error)
	UpdateChannelProperties(channelID string, domains []string, hasPass bool) (*ChannelProperties, error)
	CreateChannelProperties(channelID string, domains []string, hasPass bool) error
	DeleteChannelProperties(channelID string) error
}

var _ IChannelPropertiesDAO = ChannelPropertiesDAO{}

// ChannelProperties dynamo record struct
type ChannelProperties struct {
	ChannelID string   `dynamo:"channelId,hash"` // Hash Key
	Domains   []string `dynamo:"domains"`
	HasPass   bool     `dynamo:"hasPass"`
}

// NewChannelPropertiesDAO creates a new DAO
func NewChannelPropertiesDAO(db *dynamo.DB, cfg *config.Configuration) *ChannelPropertiesDAO {
	table := db.Table(FormatTableName(cfg.EnvironmentPrefix, channelPropertiesTableName))
	return &ChannelPropertiesDAO{
		table,
		db,
	}
}

// UpdateChannelProperties adds or updates an existing channel record
func (dao ChannelPropertiesDAO) UpdateChannelProperties(channelID string, domains []string, hasPass bool) (*ChannelProperties, error) {
	var result *ChannelProperties
	err := dao.table.Update(channelPropertiesChannelID, channelID).
		Set(channelPropertiesDomains, domains).
		Set(channelPropertiesHasPass, hasPass).
		Value(&result)

	if err != nil {
		return nil, errors.Wrap(err, "Failed to add channel properties to dynamo table")
	}

	return result, nil
}

var ErrChannelPropsExist = errors.New("Attempted to insert channel properties for existing channelID")

// CreateChannelProperties creates a channel record only if it does not exist. It returns ErrChannelPropsExist if
// a record for the passed channelID already exists.
func (dao ChannelPropertiesDAO) CreateChannelProperties(channelID string, domains []string, hasPass bool) error {
	if channelID == "" {
		return errors.New("Cannot called CreateChannelProperties with empty channelID")
	}

	err := dao.table.Put(&ChannelProperties{
		ChannelID: channelID,
		Domains:   domains,
		HasPass:   hasPass,
	}).
		If("attribute_not_exists($)", channelPropertiesChannelID).
		Run()

	// Explicitly handle an error indicating condition failed
	if err != nil {
		awsError, ok := err.(awserr.Error)
		if ok && awsError.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
			return ErrChannelPropsExist
		}
	}

	return err
}

// GetChannelProperties finds the record and returns it
func (dao ChannelPropertiesDAO) GetChannelProperties(channelID string) (*ChannelProperties, error) {
	var result ChannelProperties
	err := dao.table.
		Get(channelPropertiesChannelID, channelID).
		One(&result)

	if err == dynamo.ErrNotFound {
		return &result, nil
	}

	if err != nil {
		return &result, errors.Wrapf(err, "Failed to query channel properties in dynamo table with channelID %s", channelID)
	}
	return &result, nil
}

// DeleteChannelProperties deletes an existing channel record
func (dao ChannelPropertiesDAO) DeleteChannelProperties(channelID string) error {
	err := dao.table.Delete(channelPropertiesChannelID, channelID).Run()

	if err != nil {
		return errors.Wrap(err, "Failed to delete channel properties from dynamo table")
	}

	return nil
}

// CreateTable for ChannelProperties. FOR TESTING ONLY
func (dao *ChannelPropertiesDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}
	err := dao.client.CreateTable(dao.table.Name(), ChannelProperties{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

// DeleteTable for ChannelProperties. FOR TESTING ONLY
func (dao *ChannelPropertiesDAO) DeleteTable() error {
	return dao.table.DeleteTable().Run()
}
