package dynamo

import (
	"testing"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLockTtl(t *testing.T) {
	Convey("With LockTtl table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewLockTtlDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("SetKey", func() {
			testKey := "test-key"
			err := dao.ObtainLock(testKey, 50)
			So(err, ShouldBeNil)

			Convey("try to set the same key", func() {
				err := dao.ObtainLock(testKey, 1)
				So(err, ShouldNotBeNil)
				awsError, ok := err.(awserr.Error)
				So(ok, ShouldBeTrue)
				So(awsError.Code(), ShouldEqual, dynamodb.ErrCodeConditionalCheckFailedException)
			})
		})

		Convey("GetKey", func() {
			testKey := "test-key-2"
			err := dao.ObtainLock(testKey, 50)
			So(err, ShouldBeNil)
			key, err := dao.GetExistingLock(testKey)
			So(err, ShouldBeNil)
			So(key, ShouldEqual, testKey)
		})

		Convey("DeleteKey", func() {
			testKey := "test-key-3"
			err := dao.ObtainLock(testKey, 50)
			So(err, ShouldBeNil)
			err = dao.ReleaseLock(testKey)
			So(err, ShouldBeNil)
			key, err := dao.GetExistingLock(testKey)
			So(err, ShouldBeNil)
			So(key, ShouldBeEmpty)
			err = dao.ObtainLock(testKey, 50)
			So(err, ShouldBeNil)
		})
	})
}
