package dynamo

import (
	"testing"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChannelProperties(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewChannelPropertiesDAO(db, cfg)
		dynamoContext.RegisterDao(dao)

		Reset(func() {
			dynamoContext.Reset()
		})

		defer dynamoContext.Cleanup()

		Convey("GetChannelProperties", func() {
			expected := &ChannelProperties{}

			result, err := dao.GetChannelProperties("1234")
			So(err, ShouldBeNil)
			So(result.ChannelID, ShouldEqual, "")
			So(result, ShouldResemble, expected)
		})

		Convey("UpdateChannelProperties", func() {
			expected := &ChannelProperties{
				ChannelID: "1234",
				Domains:   []string{"asdf"},
				HasPass:   true,
			}

			result, err := dao.UpdateChannelProperties("1234", []string{"asdf"}, true)
			So(err, ShouldBeNil)
			So(result.ChannelID, ShouldEqual, "1234")
			So(result, ShouldResemble, expected)

			result, err = dao.GetChannelProperties("1234")
			So(err, ShouldBeNil)
			So(result.ChannelID, ShouldEqual, "1234")
			So(result, ShouldResemble, expected)
		})

		Convey("CreateChannelProperties", func() {
			Convey("when channel props do not already exist", func() {
				want := &ChannelProperties{
					ChannelID: "169164648",
					Domains:   []string{"walrusfest2018"},
					HasPass:   true,
				}

				err := dao.CreateChannelProperties(want.ChannelID, want.Domains, want.HasPass)
				So(err, ShouldBeNil)

				got, err := dao.GetChannelProperties(want.ChannelID)
				So(err, ShouldBeNil)
				So(got, ShouldResemble, want)
			})
			Convey("when channel props already exist", func() {
				existing := &ChannelProperties{
					ChannelID: "169164648",
					Domains:   []string{"walrusfest2018"},
					HasPass:   true,
				}
				// Insert once
				err := dao.CreateChannelProperties(existing.ChannelID, existing.Domains, existing.HasPass)
				So(err, ShouldBeNil)

				// Attempt to insert another differing record for same channel ID
				err = dao.CreateChannelProperties(existing.ChannelID, []string{"bookwormdeluxe2019"}, false)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, ErrChannelPropsExist)
			})
		})

		Convey("DeleteChannelProperties", func() {
			expected := &ChannelProperties{
				ChannelID: "1234",
				Domains:   []string{"asdf"},
				HasPass:   true,
			}

			result, err := dao.UpdateChannelProperties("1234", []string{"asdf"}, true)
			So(err, ShouldBeNil)
			So(result.ChannelID, ShouldEqual, "1234")
			So(result, ShouldResemble, expected)

			result, err = dao.GetChannelProperties("1234")
			So(err, ShouldBeNil)
			So(result.ChannelID, ShouldEqual, "1234")
			So(result, ShouldResemble, expected)

			err = dao.DeleteChannelProperties("1234")
			So(err, ShouldBeNil)

			result, err = dao.GetChannelProperties("1234")
			So(err, ShouldBeNil)
			So(result.ChannelID, ShouldEqual, "")
			So(result, ShouldResemble, &ChannelProperties{})
		})
	})
}
