package dynamo

import (
	"testing"

	test "code.justin.tv/commerce/fortuna/dynamo/test"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	firstTrackID  = "firstTrackID"
	secondTrackID = "secondTrackID"
	userID        = "userID"
	otherID       = "otherID"
	amount        = 10
)

// CreateTable for Participants. FOR TESTING ONLY
func (dao ParticipantDao) CreateTable() error {
	if test.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}
	err := dao.client.CreateTable(dao.table.Name(), Participant{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Participants. FOR TESTING ONLY
func (dao ParticipantDao) DeleteTable() error {
	return dao.table.DeleteTable().Run()
}

func TestParticipant(t *testing.T) {
	dynamoContext := dynamoTest.DynamoTestContext{}

	db := dynamoTest.CreateTestClient()
	cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
	dao := NewParticipantDao(db, cfg)
	dynamoContext.RegisterDao(dao)
	defer dynamoContext.Cleanup()

	Convey("AddParticipant sets amount", t, func() {
		result, err := dao.AddParticipant(firstTrackID, userID, amount)
		So(err, ShouldBeNil)
		So(result.Amount, ShouldEqual, amount)
	})

	Convey("AddParticipant updates amount", t, func() {
		result, err := dao.AddParticipant(firstTrackID, userID, amount)
		So(err, ShouldBeNil)
		So(result.Amount, ShouldEqual, 2*amount)
	})

	Convey("GetAllParticipants correctly retrieves records by trackId", t, func() {
		dao.AddParticipant(firstTrackID, otherID, amount)
		dao.AddParticipant(secondTrackID, userID, amount)
		results, err := dao.GetAllParticipants(firstTrackID)
		So(err, ShouldBeNil)
		So(len(results), ShouldEqual, 2)
		for _, r := range results {
			So(r.TrackID, ShouldEqual, firstTrackID)
		}
	})
}
