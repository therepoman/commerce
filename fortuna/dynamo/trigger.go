package dynamo

import (
	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	triggersTableName         = "triggers"
	triggersHashKey           = "domain"
	triggersCampaignIDIndex   = "campaignId-index"
	triggersCampaignIDHashKey = "campaignId"
	triggersTriggerIDIndex    = "triggerId-index"
	triggersTriggerIDHashKey  = "triggerId"
)

type TriggerDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

type ITriggerDAO interface {
	GetTriggersByDomain(domain string) ([]*campaignCfg.Trigger, error)
	GetTriggersByCampaignID(campaignID string) ([]*campaignCfg.Trigger, error)
	GetTriggerByID(triggerID string) (*campaignCfg.Trigger, error)
	AddTrigger(input *campaignCfg.Trigger) error
	CreateTable() error
	DeleteTable() error
}

// NewTriggerDAO return a new instance of TriggerFetcher
func NewTriggerDAO(client *dynamo.DB, cfg *config.Configuration) ITriggerDAO {
	return &TriggerDAO{
		client: client,
		table:  client.Table(FormatTableName(cfg.EnvironmentPrefix, triggersTableName)),
	}
}

// AddTrigger Adds an trigger using the trigger config as an input
func (o *TriggerDAO) AddTrigger(input *campaignCfg.Trigger) error {
	return o.table.Put(input).Run()
}

// GetTriggersByDomain queries the dynamo table using domain hash key and returns a slice of the resulted triggers
func (o *TriggerDAO) GetTriggersByDomain(domain string) ([]*campaignCfg.Trigger, error) {
	var result []*campaignCfg.Trigger
	err := o.table.Get(triggersHashKey, domain).All(&result)
	return result, err
}

// GetTriggersByCampaignID queries the dynamo table using GSI on campaign id and returns a slice of the resulted triggers
func (o *TriggerDAO) GetTriggersByCampaignID(campaignID string) ([]*campaignCfg.Trigger, error) {
	var result []*campaignCfg.Trigger
	err := o.table.Get(triggersCampaignIDHashKey, campaignID).Index(triggersCampaignIDIndex).All(&result)
	return result, err
}

// GetTriggerByID queries the Dynamo table using GSI on trigger id and returns the result.
func (o *TriggerDAO) GetTriggerByID(triggerID string) (*campaignCfg.Trigger, error) {
	var result *campaignCfg.Trigger
	err := o.table.Get(triggersTriggerIDHashKey, triggerID).Index(triggersTriggerIDIndex).One(&result)
	return result, err
}

// CreateTable for Trigger. FOR TESTING ONLY
func (o *TriggerDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(o.table.Name(), o.client) {
		return nil
	}
	err := o.client.CreateTable(o.table.Name(), campaignCfg.Trigger{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Trigger. FOR TESTING ONLY
func (o *TriggerDAO) DeleteTable() error {
	return o.table.DeleteTable().Run()
}
