package dynamo

import (
	"sync"

	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	gdb "github.com/guregu/dynamo"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

var (
	ItemNotFoundErr = errors.New("Item doesn't exist in dynamo")
)

// DAOs exposes methods to use specific DAO structs
type DAOs struct {
	RewardDAO            IRewardDAO
	EligibilityDAO       IEligibilityDAO
	ChannelPropertiesDAO IChannelPropertiesDAO
	CampaignDAO          ICampaignDAO
	ObjectiveDAO         IObjectiveDAO
	MilestoneDAO         IMilestoneDAO
	TriggerDAO           ITriggerDAO
	ClaimableRewardDAO   IClaimableRewardDAO
	LockTtlDAO           ILockTtlDAO
}

// CampaignHelper provides common operations involving fetching campaign data from different tables.
type CampaignHelper interface {
	GetCampaignConfigForDomain(domain string) (*campaignCfg.Configuration, error)
	GetAllChannelsCampaigns() ([]*campaignCfg.Campaign, error)
	GetCampaignWithChildDataByID(campaignID string) (*campaignCfg.Campaign, error)
}

func SetupDAOs(cfg *config.Configuration) (*DAOs, error) {
	db := gdb.New(session.New(), &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})

	rewardDao := NewRewardDAO(db, cfg)
	eligibilityDao := NewEligibilityDAO(db, cfg)
	channelPropertiesDAO := NewChannelPropertiesDAO(db, cfg)
	campaignDAO := NewCampaignDAO(db, cfg)
	objectiveDAO := NewObjectiveDAO(db, cfg)
	milestoneeDAO := NewMilestoneDAO(db, cfg)
	triggerDAO := NewTriggerDAO(db, cfg)
	claimableRewardDAO := NewClaimableRewardDAO(db, cfg)
	lockTtlDAO := NewLockTtlDAO(db, cfg)

	return &DAOs{
		RewardDAO:            rewardDao,
		EligibilityDAO:       eligibilityDao,
		ChannelPropertiesDAO: channelPropertiesDAO,
		CampaignDAO:          campaignDAO,
		ObjectiveDAO:         objectiveDAO,
		MilestoneDAO:         milestoneeDAO,
		TriggerDAO:           triggerDAO,
		ClaimableRewardDAO:   claimableRewardDAO,
		LockTtlDAO:           lockTtlDAO,
	}, nil
}

// GetCampaignConfigForDomain fetches campaigns, objectives, and milestones for a single domain and return the config without denormalization
func (d *DAOs) GetCampaignConfigForDomain(domain string) (*campaignCfg.Configuration, error) {
	campaigns, objectives, milestones, triggers, err := d.getRawCampaignDataForDomain(domain)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to load data from dynamo")
	}

	// Create config with a single domain (multiple domain support in the future)
	config := campaignCfg.Configuration{}
	config.Campaigns = map[string][]*campaignCfg.Campaign{}

	// Do not populate domain with empty campaigns
	if len(campaigns) == 0 {
		return &config, nil
	}

	config.Campaigns[domain] = []*campaignCfg.Campaign{}

	// Populate things
	campaignMap := map[string]*campaignCfg.Campaign{}
	for _, c := range campaigns {
		campaignMap[c.ID] = c
		config.Campaigns[domain] = append(config.Campaigns[domain], c)
	}
	objectiveMap := map[string]*campaignCfg.Objective{}
	for _, o := range objectives {
		objectiveMap[o.ID] = o
		c, ok := campaignMap[o.CampaignId]
		if !ok {
			log.Warnf("Campaign in dynamo has an integrity issue. Objective %v does not belong to any campaign in domain %v", o.ID, domain)
			continue
		}
		c.Objectives = append(c.Objectives, o)
	}

	for _, m := range milestones {
		o, ok := objectiveMap[m.ObjectiveID]
		if !ok {
			log.Warnf("Campaign in dynamo has an integrity issue. Milestone %v does not belong to any objective in domain %v", m.ID, domain)
			continue
		}
		o.Milestones = append(o.Milestones, m)
	}

	triggerMap := map[string]*campaignCfg.Trigger{}
	for _, t := range triggers {
		triggerMap[t.ID] = t
		c, ok := campaignMap[t.CampaignId]
		if !ok {
			log.Warnf("Campaign in dynamo has an integrity issue. Trigger %v does not belong to any campaign in domain %v", t.ID, domain)
			continue
		}
		c.Triggers = append(c.Triggers, t)
	}

	// Do not denormalize. It will break caching (since caching is based on json and denormalization will create circular references)

	return &config, nil
}

func (d *DAOs) GetAllChannelsCampaigns() ([]*campaignCfg.Campaign, error) {
	campaigns, err := d.CampaignDAO.GetAllChannelsCampaigns()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get all channels campaigns from dynamo")
	}

	for _, unhydratedCampaign := range campaigns {
		objectives, milestones, triggers, err := d.getChildDataForCampaign(unhydratedCampaign.ID)
		if err != nil {
			return nil, errors.Wrapf(err, "Could not hydrate all channels campaign %v", unhydratedCampaign.ID)
		}

		objectiveMap := map[string]*campaignCfg.Objective{}
		for _, o := range objectives {
			objectiveMap[o.ID] = o
		}

		for _, m := range milestones {
			o, ok := objectiveMap[m.ObjectiveID]
			if !ok {
				log.Warnf("Campaign in dynamo has an integrity issue. Milestone %v does not belong to any objective in campaign %v", m.ID, unhydratedCampaign.ID)
				continue
			}
			o.Milestones = append(o.Milestones, m)
		}

		unhydratedCampaign.Objectives = objectives
		unhydratedCampaign.Triggers = triggers
	}

	return campaigns, nil
}

func (d *DAOs) GetCampaignWithChildDataByID(campaignID string) (*campaignCfg.Campaign, error) {
	campaign, err := d.CampaignDAO.GetCampaignByID(campaignID)
	if err == gdb.ErrNotFound {
		return nil, ItemNotFoundErr
	}
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to get campaign %v from dynamo", campaignID)
	}

	objectives, milestones, triggers, err := d.getChildDataForCampaign(campaign.ID)
	if err != nil {
		return nil, errors.Wrapf(err, "Could not get campaign child data %v", campaign.ID)
	}

	objectiveMap := map[string]*campaignCfg.Objective{}
	for _, o := range objectives {
		objectiveMap[o.ID] = o
	}

	for _, m := range milestones {
		o, ok := objectiveMap[m.ObjectiveID]
		if !ok {
			log.Errorf("Campaign in dynamo has an integrity issue. Milestone %v does not belong to any objective in campaign %v", m.ID, campaign.ID)
			continue
		}
		o.Milestones = append(o.Milestones, m)
	}

	campaign.Objectives = objectives
	campaign.Triggers = triggers

	return campaign, nil
}

func (d *DAOs) getRawCampaignDataForDomain(domain string) ([]*campaignCfg.Campaign, []*campaignCfg.Objective, []*campaignCfg.Milestone, []*campaignCfg.Trigger, error) {
	var campaigns []*campaignCfg.Campaign
	var objectives []*campaignCfg.Objective
	var milestones []*campaignCfg.Milestone
	var triggers []*campaignCfg.Trigger
	var err error
	campaignsChan := make(chan []*campaignCfg.Campaign, 1)
	objectivesChan := make(chan []*campaignCfg.Objective, 1)
	milestonesChan := make(chan []*campaignCfg.Milestone, 1)
	triggersChan := make(chan []*campaignCfg.Trigger, 1)
	errChan := make(chan error, 4)

	var wg sync.WaitGroup
	wg.Add(4)

	go func() {
		defer wg.Done()
		c, cErr := d.CampaignDAO.GetCampaignsByDomain(domain)
		if cErr != nil {
			errChan <- errors.Wrap(cErr, "Failed to get campaign rows")
		} else {
			campaignsChan <- c
		}
	}()

	go func() {
		defer wg.Done()
		o, oErr := d.ObjectiveDAO.GetObjectivesByDomain(domain)
		if oErr != nil {
			errChan <- errors.Wrap(oErr, "Failed to get objective rows")
		} else {
			objectivesChan <- o
		}
	}()

	go func() {
		defer wg.Done()
		m, mErr := d.MilestoneDAO.GetMilestonesByDomain(domain)
		if mErr != nil {
			errChan <- errors.Wrap(mErr, "Failed to get milestone rows")
		} else {
			milestonesChan <- m
		}
	}()

	go func() {
		defer wg.Done()
		t, tErr := d.TriggerDAO.GetTriggersByDomain(domain)
		if tErr != nil {
			errChan <- errors.Wrap(tErr, "Failed to get trigger rows")
		} else {
			triggersChan <- t
		}
	}()

	doneChan := make(chan struct{})
	go func() {
		wg.Wait()
		close(doneChan)
	}()

	select {
	case err = <-errChan:
		return nil, nil, nil, nil, err
	case <-doneChan:
		campaigns = <-campaignsChan
		objectives = <-objectivesChan
		milestones = <-milestonesChan
		triggers = <-triggersChan
		return campaigns, objectives, milestones, triggers, nil
	}
}

func (d *DAOs) getChildDataForCampaign(campaignID string) ([]*campaignCfg.Objective, []*campaignCfg.Milestone, []*campaignCfg.Trigger, error) {
	var objectives []*campaignCfg.Objective
	var milestones []*campaignCfg.Milestone
	var triggers []*campaignCfg.Trigger
	var err error
	objectivesChan := make(chan []*campaignCfg.Objective, 1)
	milestonesChan := make(chan []*campaignCfg.Milestone, 1)
	triggersChan := make(chan []*campaignCfg.Trigger, 1)
	errChan := make(chan error, 3)

	var wg sync.WaitGroup
	wg.Add(3)

	go func() {
		defer wg.Done()
		o, oErr := d.ObjectiveDAO.GetObjectivesByCampaignID(campaignID)
		if oErr != nil {
			errChan <- errors.Wrap(oErr, "Failed to get objective rows")
		} else {
			objectivesChan <- o
		}
	}()

	go func() {
		defer wg.Done()
		m, mErr := d.MilestoneDAO.GetMilestonesByCampaignID(campaignID)
		if mErr != nil {
			errChan <- errors.Wrap(mErr, "Failed to get milestone rows")
		} else {
			milestonesChan <- m
		}
	}()

	go func() {
		defer wg.Done()
		t, tErr := d.TriggerDAO.GetTriggersByCampaignID(campaignID)
		if tErr != nil {
			errChan <- errors.Wrap(tErr, "Failed to get milestone rows")
		} else {
			triggersChan <- t
		}
	}()

	doneChan := make(chan struct{})
	go func() {
		wg.Wait()
		close(doneChan)
	}()

	select {
	case err = <-errChan:
		return nil, nil, nil, err
	case <-doneChan:
		objectives = <-objectivesChan
		milestones = <-milestonesChan
		triggers = <-triggersChan
		return objectives, milestones, triggers, nil
	}
}
