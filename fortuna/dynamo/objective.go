package dynamo

import (
	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	objectivesTableName          = "objectives"
	objectivesHashKey            = "domain"
	objectivesCampaignIDIndex    = "campaignId-index"
	objectivesCampaignIDHashKey  = "campaignId"
	objectivesObjectiveIDIndex   = "objectiveId-index"
	objectivesObjectiveIDHashKey = "objectiveId"
)

type ObjectiveDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

type IObjectiveDAO interface {
	GetObjectivesByDomain(domain string) ([]*campaignCfg.Objective, error)
	GetObjectivesByCampaignID(campaignID string) ([]*campaignCfg.Objective, error)
	GetObjectiveByID(objectiveID string) (*campaignCfg.Objective, error)
	AddObjective(input *campaignCfg.Objective) error
	CreateTable() error
	DeleteTable() error
}

// NewObjectiveDAO return a new instance of ObjectiveFetcher
func NewObjectiveDAO(client *dynamo.DB, cfg *config.Configuration) IObjectiveDAO {
	return &ObjectiveDAO{
		client: client,
		table:  client.Table(FormatTableName(cfg.EnvironmentPrefix, objectivesTableName)),
	}
}

// AddObjective Adds an objective using the objective config as an input
func (o *ObjectiveDAO) AddObjective(input *campaignCfg.Objective) error {
	return o.table.Put(input).Run()
}

// GetObjectivesByDomain queries the dynamo table using domain hash key and returns a slice of the resulted objectives
func (o *ObjectiveDAO) GetObjectivesByDomain(domain string) ([]*campaignCfg.Objective, error) {
	var result []*campaignCfg.Objective
	err := o.table.Get(objectivesHashKey, domain).All(&result)
	return result, err
}

// GetObjectivesByCampaignID queries the dynamo table using GSI on campaign id and returns a slice of the resulted objectives
func (o *ObjectiveDAO) GetObjectivesByCampaignID(campaignID string) ([]*campaignCfg.Objective, error) {
	var result []*campaignCfg.Objective
	err := o.table.Get(objectivesCampaignIDHashKey, campaignID).Index(objectivesCampaignIDIndex).All(&result)
	return result, err
}

// GetObjectiveByID queries the Dynamo table using GSI on objective id and returns the result.
func (o *ObjectiveDAO) GetObjectiveByID(objectiveID string) (*campaignCfg.Objective, error) {
	var result *campaignCfg.Objective
	err := o.table.Get(objectivesObjectiveIDHashKey, objectiveID).Index(objectivesObjectiveIDIndex).One(&result)
	return result, err
}

// CreateTable for Objective. FOR TESTING ONLY
func (o *ObjectiveDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(o.table.Name(), o.client) {
		return nil
	}
	err := o.client.CreateTable(o.table.Name(), campaignCfg.Objective{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Objective. FOR TESTING ONLY
func (o *ObjectiveDAO) DeleteTable() error {
	return o.table.DeleteTable().Run()
}
