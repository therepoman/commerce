package dynamo

import (
	"testing"
	"time"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	rewardType          = "igc"
	rewardID            = "junkrat"
	repeatableRewardID  = "box"
	domain              = "domain"
	milestoneID         = "milestone1"
	milestoneType       = "global"
	externalUserID      = "blizzard12345"
	fulfillmentStrategy = "blizzard"
	quantity            = 3
	triggerID           = "hgcCheerbomb"
	count               = 1
)

func TestReward(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewRewardDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("GetUserReward empty response", func() {
			result, err := dao.GetUserReward(userID, rewardType, rewardID)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})

		Convey("GetUserRewardWithCount empty response", func() {
			result, err := dao.GetUserRewardWithCount(userID, rewardType, rewardID, count)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})

		Convey("GetAllUserRewards empty response", func() {
			result, err := dao.GetAllUserRewards(userID)
			So(err, ShouldBeNil)
			So(result, ShouldBeNil)
		})

		Convey("UpdateReward to add first reward in pending", func() {
			nowTime := time.Now().UTC()
			input := &Reward{
				UserID:              userID,
				ScopedRewardID:      BuildScopedRewardID(rewardType, rewardID),
				RewardType:          rewardType,
				RewardID:            rewardID,
				Domain:              domain,
				MilestoneID:         milestoneID,
				MilestoneType:       milestoneType,
				ExternalUserID:      externalUserID,
				FulfillmentStrategy: fulfillmentStrategy,
				FulfillmentStatus:   FulfillmentStatusPending,
				Quantity:            1,
				CreatedAt:           nowTime,
			}

			expected := &Reward{
				UserID:              userID,
				ScopedRewardID:      BuildScopedRewardID(rewardType, rewardID),
				RewardType:          rewardType,
				RewardID:            rewardID,
				Domain:              domain,
				MilestoneID:         milestoneID,
				MilestoneType:       milestoneType,
				ExternalUserID:      externalUserID,
				FulfillmentStrategy: fulfillmentStrategy,
				FulfillmentStatus:   FulfillmentStatusPending,
				Quantity:            1,
				TriggerID:           placeholder,
				BatchID:             placeholder,
				CreatedAt:           nowTime,
			}

			expectedAllRewards := []*Reward{
				expected,
			}

			result, err := dao.UpdateReward(input)
			So(err, ShouldBeNil)
			So(result.FulfillmentStatus, ShouldEqual, FulfillmentStatusPending)
			// don't check TTL
			expected.TTL = result.TTL
			So(result, ShouldResemble, expected)
			//soft check to make sure ttl is set
			So(result.TTL.After(time.Now().AddDate(0, 0, 89)), ShouldBeTrue)

			Convey("GetUserReward returns reward", func() {
				result, err := dao.GetUserReward(userID, rewardType, rewardID)
				So(err, ShouldBeNil)
				So(result, ShouldResemble, expected)
				So(result.FulfillmentStatus, ShouldEqual, FulfillmentStatusPending)
			})

			Convey("GetAllUserRewards returns reward", func() {
				result, err := dao.GetAllUserRewards(userID)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result, ShouldResemble, expectedAllRewards)
			})

			Convey("UpdateReward to update the reward", func() {
				yesterdayTime := time.Now().Add(-24 * time.Hour).UTC()
				successInput := &Reward{
					UserID:              userID,
					ScopedRewardID:      BuildScopedRewardID(rewardType, rewardID),
					RewardType:          rewardType,
					RewardID:            rewardID,
					Domain:              domain,
					MilestoneID:         milestoneID,
					MilestoneType:       milestoneType,
					ExternalUserID:      externalUserID,
					FulfillmentStrategy: fulfillmentStrategy,
					FulfillmentStatus:   FulfillmentStatusSuccess,
					Quantity:            1,
					CreatedAt:           yesterdayTime,
				}

				expected := &Reward{
					UserID:              userID,
					ScopedRewardID:      BuildScopedRewardID(rewardType, rewardID),
					RewardType:          rewardType,
					RewardID:            rewardID,
					Domain:              domain,
					MilestoneID:         milestoneID,
					MilestoneType:       milestoneType,
					ExternalUserID:      externalUserID,
					FulfillmentStrategy: fulfillmentStrategy,
					FulfillmentStatus:   FulfillmentStatusSuccess,
					Quantity:            1,
					TriggerID:           placeholder,
					BatchID:             placeholder,
					CreatedAt:           yesterdayTime,
				}

				expectedAllRewards := []*Reward{
					expected,
				}

				result, err := dao.UpdateReward(successInput)
				So(err, ShouldBeNil)
				So(result.FulfillmentStatus, ShouldEqual, FulfillmentStatusSuccess)
				// don't check TTL
				expected.TTL = result.TTL
				So(result, ShouldResemble, expected)
				//soft check to make sure ttl is set
				So(result.TTL.After(time.Now().AddDate(0, 0, 89)), ShouldBeTrue)

				Convey("GetUserReward returns reward", func() {
					result, err := dao.GetUserReward(userID, rewardType, rewardID)
					So(err, ShouldBeNil)
					So(result, ShouldResemble, expected)
					So(result.FulfillmentStatus, ShouldEqual, FulfillmentStatusSuccess)
				})

				Convey("GetAllUserRewards returns reward", func() {
					result, err := dao.GetAllUserRewards(userID)
					So(err, ShouldBeNil)
					So(len(result), ShouldEqual, 1)
					So(result, ShouldResemble, expectedAllRewards)
				})
			})

			Convey("UpdateReward to update the reward for Trigger", func() {
				successInput := &Reward{
					UserID:              userID,
					ScopedRewardID:      BuildScopedRewardIDWithCount(rewardType, repeatableRewardID, count),
					RewardType:          rewardType,
					RewardID:            repeatableRewardID,
					Domain:              domain,
					ExternalUserID:      externalUserID,
					FulfillmentStrategy: fulfillmentStrategy,
					FulfillmentStatus:   FulfillmentStatusPending,
					Quantity:            quantity,
					TriggerID:           triggerID,
				}

				expected := &Reward{
					UserID:              userID,
					ScopedRewardID:      BuildScopedRewardIDWithCount(rewardType, repeatableRewardID, count),
					RewardType:          rewardType,
					RewardID:            repeatableRewardID,
					Domain:              domain,
					MilestoneID:         placeholder,
					MilestoneType:       placeholder,
					ExternalUserID:      externalUserID,
					FulfillmentStrategy: fulfillmentStrategy,
					FulfillmentStatus:   FulfillmentStatusPending,
					Quantity:            quantity,
					TriggerID:           triggerID,
					BatchID:             placeholder,
				}

				result, err := dao.UpdateReward(successInput)
				So(err, ShouldBeNil)
				So(result.FulfillmentStatus, ShouldEqual, FulfillmentStatusPending)
				// don't check TTL
				expected.TTL = result.TTL
				So(result, ShouldResemble, expected)
				//soft check to make sure ttl is set
				So(result.TTL.After(time.Now().AddDate(0, 0, 89)), ShouldBeTrue)

				Convey("GetUserRewardWithCount returns reward", func() {
					result, err := dao.GetUserRewardWithCount(userID, rewardType, repeatableRewardID, count)
					So(err, ShouldBeNil)
					// don't check TTL
					expected.TTL = result.TTL
					So(result, ShouldResemble, expected)
					//soft check to make sure ttl is set
					So(result.TTL.After(time.Now().AddDate(0, 0, 89)), ShouldBeTrue)
					So(result.FulfillmentStatus, ShouldEqual, FulfillmentStatusPending)
				})

				Convey("GetAllUserRewards returns rewards", func() {
					result, err := dao.GetAllUserRewards(userID)
					So(err, ShouldBeNil)
					So(len(result), ShouldEqual, 2)
				})

				Convey("GetUserRewards returns a slice of rewards with count", func() {
					additionalInput := &Reward{
						UserID:              userID,
						ScopedRewardID:      BuildScopedRewardIDWithCount(rewardType, repeatableRewardID, count+1),
						RewardType:          rewardType,
						RewardID:            rewardID,
						Domain:              domain,
						ExternalUserID:      externalUserID,
						FulfillmentStrategy: fulfillmentStrategy,
						FulfillmentStatus:   FulfillmentStatusPending,
						Quantity:            quantity,
						TriggerID:           triggerID,
					}
					_, err := dao.UpdateReward(additionalInput)
					So(err, ShouldBeNil)

					result, err := dao.GetUserRewards(userID, rewardType, repeatableRewardID)
					So(err, ShouldBeNil)
					So(len(result), ShouldEqual, 2)
				})
			})

			Convey("Test DeleteUserRewards", func() {
				err := dao.DeleteUserRewards(userID, rewardType, rewardID)
				So(err, ShouldBeNil)
				result, err := dao.GetUserRewards(userID, rewardType, rewardID)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 0)

				Convey("GetAllUserRewards returns no rewards", func() {
					result, err := dao.GetAllUserRewards(userID)
					So(err, ShouldBeNil)
					So(len(result), ShouldEqual, 0)
				})
			})
		})
	})
}
