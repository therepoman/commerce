package dynamo

import (
	"testing"

	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestObjective(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewObjectiveDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("AddObjective", func() {
			reward := campaignCfg.Reward{
				ID:   "reward",
				Type: "emote",
				Name: "emote reward",
				Metadata: campaignCfg.RewardMetadata{
					SubType: "emote",
				},
			}
			randomRewards := []*campaignCfg.Reward{&reward}
			objectiveID := "owl2018.cheer.global"
			input := &campaignCfg.Objective{
				ID:                 objectiveID,
				EventHandler:       "GlobalCheerHandler",
				Title:              "OWL Cheer Global Progress",
				Description:        "Cheer for OWL",
				Domain:             "owl",
				ShouldAutogrant:    true,
				DiscoveryEventType: "COMMAND_CENTER",
				Preference:         "dal",
				CheerGroupKey:      "HELLO",
				Tag:                "GLOBAL",
				RandomRewards:      randomRewards,
				CampaignId:         "owl2018",
				BadgeSetIds: map[string]bool{
					"testBadgeId": true,
				},
				ContributingMilestoneIds: []string{
					"test.milestone.id",
				},
			}

			err := dao.AddObjective(input)
			So(err, ShouldBeNil)

			Convey("GetObjectivessByDomain returns objectives given domain", func() {
				result, err := dao.GetObjectivesByDomain("owl")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetObjectivesByCampaignID returns objectives given campaign ID", func() {
				result, err := dao.GetObjectivesByCampaignID("owl2018")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetObjectiveByID returns objective given objective ID", func() {
				result, err := dao.GetObjectiveByID(objectiveID)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result, ShouldResemble, input)
			})
		})
	})
}
