package dynamo

import (
	"testing"
	"time"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEligibility(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewEligibilityDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("AddEligibility", func() {
			expected := &Eligibility{
				UserID:      "u",
				MilestoneID: "m",
				IsEligible:  true,
			}

			result, err := dao.AddEligibility("u", "m")
			So(err, ShouldBeNil)
			So(result.UserID, ShouldEqual, "u")
			// don't check TTL
			expected.TTL = result.TTL
			So(result, ShouldResemble, expected)
			//soft check to make sure ttl is set properly
			So(result.TTL.After(time.Now().AddDate(0, 0, 89)), ShouldBeTrue)
		})

		Convey("IsUserEligibleForMilestone", func() {
			Convey("With record", func() {
				dao.AddEligibility("u", "m")
				result, err := dao.IsUserEligibleForMilestone("u", "m")
				So(err, ShouldBeNil)
				So(result, ShouldBeTrue)
			})

			Convey("Without record", func() {
				result, err := dao.IsUserEligibleForMilestone("u2", "m2")
				So(err, ShouldBeNil)
				So(result, ShouldBeFalse)
			})
		})
	})
}
