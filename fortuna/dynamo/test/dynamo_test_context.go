package dynamo_test

type (
	DynamoDao interface {
		DeleteTable() error
		CreateTable() error
	}

	DynamoTestContext struct {
		Daos []DynamoDao
	}
)

func (context *DynamoTestContext) RegisterDao(dao DynamoDao) error {
	err := dao.CreateTable()
	if err != nil {
		return err
	}

	context.Daos = append(context.Daos, dao)
	return nil
}

func (context *DynamoTestContext) Cleanup() error {
	for _, dao := range context.Daos {
		err := dao.DeleteTable()
		if err != nil {
			return err
		}
	}

	return nil
}

func (context *DynamoTestContext) Reset() error {
	for _, dao := range context.Daos {
		err := dao.DeleteTable()
		if err != nil {
			return err
		}
		err = dao.CreateTable()
		if err != nil {
			return err
		}
	}

	return nil
}
