package dynamo_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"

	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/mocks"
	gdb "github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
	"go.uber.org/goleak"
)

const (
	TestUserID = "testUserID"
)

func TestDAOs(t *testing.T) {
	Convey("test DAOs", t, func() {
		mockCampaignDAO := new(mocks.ICampaignDAO)
		mockObjectiveDAO := new(mocks.IObjectiveDAO)
		mockMilestoneDAO := new(mocks.IMilestoneDAO)
		mockTriggerDAO := new(mocks.ITriggerDAO)
		daos := dynamo.DAOs{
			CampaignDAO:  mockCampaignDAO,
			ObjectiveDAO: mockObjectiveDAO,
			MilestoneDAO: mockMilestoneDAO,
			TriggerDAO:   mockTriggerDAO,
		}
		testCampaign := "campaign1"
		testObjective := "objective1"
		testMilestone := "milestone1"
		testTrigger := "trigger1"

		campaignInstance := campaign.Campaign{
			ID:          testCampaign,
			AllChannels: true,
		}

		objectiveInstance := campaign.Objective{
			ID:         testObjective,
			CampaignId: testCampaign,
		}

		milestoneInstance := campaign.Milestone{
			ID:          testMilestone,
			ObjectiveID: testObjective,
			CampaignId:  testCampaign,
		}

		triggerInstance := campaign.Trigger{
			ID:         testTrigger,
			CampaignId: testCampaign,
		}

		ignoreRollbar := goleak.IgnoreTopFunction("code.justin.tv/commerce/fortuna/vendor/github.com/heroku/rollbar.NewAsync.func1")
		ignorePoll := goleak.IgnoreTopFunction("internal/poll.runtime_pollWait")
		ignoreNet := goleak.IgnoreTopFunction("net/http.(*persistConn).writeLoop")

		defer goleak.VerifyNoLeaks(t, ignoreRollbar, ignorePoll, ignoreNet)

		Convey("test GetCampaignConfigForDomain", func() {
			Convey("with successful dynamo retrieval", func() {
				mockCampaignDAO.On("GetCampaignsByDomain", Anything).Return([]*campaign.Campaign{&campaignInstance}, nil)
				mockObjectiveDAO.On("GetObjectivesByDomain", Anything).Return([]*campaign.Objective{&objectiveInstance}, nil)
				mockMilestoneDAO.On("GetMilestonesByDomain", Anything).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByDomain", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				result, err := daos.GetCampaignConfigForDomain("domain")

				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				campaigns := result.Campaigns["domain"]
				So(len(campaigns), ShouldEqual, 1)
				So(campaigns[0].ID, ShouldEqual, testCampaign)
				So(len(campaigns[0].Objectives), ShouldEqual, 1)
				So(campaigns[0].Objectives[0].ID, ShouldEqual, testObjective)
				So(len(campaigns[0].Objectives[0].Milestones), ShouldEqual, 1)
				So(campaigns[0].Objectives[0].Milestones[0].ID, ShouldEqual, testMilestone)
			})

			Convey("with unsuccessful dynamo retrieval", func() {
				mockCampaignDAO.On("GetCampaignsByDomain", Anything).Return(nil, errors.New("biblethump10000"))
				mockObjectiveDAO.On("GetObjectivesByDomain", Anything).Return(nil, errors.New("biblethump10000"))
				mockMilestoneDAO.On("GetMilestonesByDomain", Anything).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByDomain", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				result, err := daos.GetCampaignConfigForDomain("domain")

				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})

			Convey("with zero rows, it gracefully returns an empty Configuration", func() {
				mockCampaignDAO.On("GetCampaignsByDomain", Anything).Return([]*campaign.Campaign{}, nil)
				mockObjectiveDAO.On("GetObjectivesByDomain", Anything).Return([]*campaign.Objective{}, nil)
				mockMilestoneDAO.On("GetMilestonesByDomain", Anything).Return([]*campaign.Milestone{}, nil)
				mockTriggerDAO.On("GetTriggersByDomain", Anything).Return([]*campaign.Trigger{}, nil)

				result, err := daos.GetCampaignConfigForDomain("domain")

				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				campaigns, ok := result.Campaigns["domain"]
				So(len(campaigns), ShouldEqual, 0)
				So(ok, ShouldBeFalse)
			})
		})

		Convey("test GetAllChannelsCampaigns", func() {
			Convey("with successful dynamo retrieval", func() {
				mockCampaignDAO.On("GetAllChannelsCampaigns").Return([]*campaign.Campaign{&campaignInstance}, nil)
				mockObjectiveDAO.On("GetObjectivesByCampaignID", testCampaign).Return([]*campaign.Objective{&objectiveInstance}, nil)
				mockMilestoneDAO.On("GetMilestonesByCampaignID", testCampaign).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				campaigns, err := daos.GetAllChannelsCampaigns()

				So(err, ShouldBeNil)
				So(campaigns, ShouldNotBeNil)
				So(len(campaigns), ShouldEqual, 1)
				So(campaigns[0].ID, ShouldEqual, testCampaign)
				So(len(campaigns[0].Objectives), ShouldEqual, 1)
				So(campaigns[0].Objectives[0].ID, ShouldEqual, testObjective)
				So(len(campaigns[0].Objectives[0].Milestones), ShouldEqual, 1)
				So(campaigns[0].Objectives[0].Milestones[0].ID, ShouldEqual, testMilestone)
				So(campaigns[0].Triggers[0].ID, ShouldEqual, testTrigger)
			})

			Convey("with unsuccessful campaign retrieval", func() {
				mockCampaignDAO.On("GetAllChannelsCampaigns").Return(nil, errors.New("biblethump10000"))

				result, err := daos.GetAllChannelsCampaigns()

				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})

			Convey("with unsuccessful child data retrieval", func() {
				mockCampaignDAO.On("GetAllChannelsCampaigns").Return([]*campaign.Campaign{&campaignInstance}, nil)
				mockObjectiveDAO.On("GetObjectivesByCampaignID", testCampaign).Return(nil, errors.New("biblethump10000"))
				mockMilestoneDAO.On("GetMilestonesByCampaignID", testCampaign).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				result, err := daos.GetAllChannelsCampaigns()

				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("test GetCampaignWithChildDataByID", func() {
			Convey("with successful dynamo retrieval", func() {
				mockCampaignDAO.On("GetCampaignByID", Anything).Return(&campaignInstance, nil)
				mockObjectiveDAO.On("GetObjectivesByCampaignID", testCampaign).Return([]*campaign.Objective{&objectiveInstance}, nil)
				mockMilestoneDAO.On("GetMilestonesByCampaignID", testCampaign).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				campaign, err := daos.GetCampaignWithChildDataByID(TestUserID)

				So(err, ShouldBeNil)
				So(campaign, ShouldNotBeNil)
				So(campaign.ID, ShouldEqual, testCampaign)
				So(len(campaign.Objectives), ShouldEqual, 1)
				So(campaign.Objectives[0].ID, ShouldEqual, testObjective)
				So(len(campaign.Objectives[0].Milestones), ShouldEqual, 1)
				So(campaign.Objectives[0].Milestones[0].ID, ShouldEqual, testMilestone)
				So(campaign.Triggers[0].ID, ShouldEqual, testTrigger)
			})

			Convey("with guregu ErrNotFound campaign retrieval", func() {
				mockCampaignDAO.On("GetCampaignByID", Anything).Return(nil, gdb.ErrNotFound)

				campaign, err := daos.GetCampaignWithChildDataByID(TestUserID)

				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, dynamo.ItemNotFoundErr)
				So(campaign, ShouldBeNil)
			})

			Convey("with unsuccessful campaign retrieval", func() {
				mockCampaignDAO.On("GetCampaignByID", Anything).Return(nil, errors.New("test error"))

				campaign, err := daos.GetCampaignWithChildDataByID(TestUserID)

				So(err, ShouldNotBeNil)
				So(campaign, ShouldBeNil)
			})

			Convey("with unsuccessful child objectives data retrieval", func() {
				mockCampaignDAO.On("GetCampaignByID", Anything).Return(&campaignInstance, nil)
				mockObjectiveDAO.On("GetObjectivesByCampaignID", testCampaign).Return(nil, errors.New("test error"))
				mockMilestoneDAO.On("GetMilestonesByCampaignID", testCampaign).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				campaign, err := daos.GetCampaignWithChildDataByID(TestUserID)

				So(err, ShouldNotBeNil)
				So(campaign, ShouldBeNil)
			})

			Convey("with unsuccessful child milestones data retrieval", func() {
				mockCampaignDAO.On("GetCampaignByID", Anything).Return(&campaignInstance, nil)
				mockObjectiveDAO.On("GetObjectivesByCampaignID", testCampaign).Return([]*campaign.Objective{&objectiveInstance}, nil)
				mockMilestoneDAO.On("GetMilestonesByCampaignID", testCampaign).Return(nil, errors.New("test error"))
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return([]*campaign.Trigger{&triggerInstance}, nil)

				campaign, err := daos.GetCampaignWithChildDataByID(TestUserID)

				So(err, ShouldNotBeNil)
				So(campaign, ShouldBeNil)
			})

			Convey("with unsuccessful child triggers data retrieval", func() {
				mockCampaignDAO.On("GetCampaignByID", Anything).Return(&campaignInstance, nil)
				mockObjectiveDAO.On("GetObjectivesByCampaignID", testCampaign).Return([]*campaign.Objective{&objectiveInstance}, nil)
				mockMilestoneDAO.On("GetMilestonesByCampaignID", testCampaign).Return([]*campaign.Milestone{&milestoneInstance}, nil)
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return(nil, errors.New("test error"))

				campaign, err := daos.GetCampaignWithChildDataByID(TestUserID)

				So(err, ShouldNotBeNil)
				So(campaign, ShouldBeNil)
			})
		})
	})
}
