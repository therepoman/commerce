package dynamo

import (
	"sort"

	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	milestonesTableName          = "milestones"
	milestonesHashKey            = "domain"
	milestonesObjectiveIDIndex   = "objectiveId-index"
	milestonesObjectiveIDHashKey = "objectiveId"
	milestonesCampaignIDIndex    = "campaignId-index"
	milestonesCampaignIDHashKey  = "campaignId"
	milestonesMilestoneIDIndex   = "milestoneId-index"
	milestonesMilestoneIDHashKey = "milestoneId"
)

type MilestoneDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

type IMilestoneDAO interface {
	GetMilestonesByDomain(domain string) ([]*campaignCfg.Milestone, error)
	GetMilestonesByObjectiveID(objectiveID string) ([]*campaignCfg.Milestone, error)
	GetMilestonesByCampaignID(objectiveID string) ([]*campaignCfg.Milestone, error)
	GetMilestoneByID(milestoneID string) (*campaignCfg.Milestone, error)
	AddMilestone(input *campaignCfg.Milestone) error
	CreateTable() error
	DeleteTable() error
}

// NewMilestoneDAO return a new instance of MilestoneFetcher
func NewMilestoneDAO(client *dynamo.DB, cfg *config.Configuration) IMilestoneDAO {
	return &MilestoneDAO{
		client: client,
		table:  client.Table(FormatTableName(cfg.EnvironmentPrefix, milestonesTableName)),
	}
}

// AddMilestone Adds a milestone using the milestone config as an input
func (m *MilestoneDAO) AddMilestone(input *campaignCfg.Milestone) error {
	return m.table.Put(input).Run()
}

// GetMilestonesByDomain queries the Dynamo table using domain hash key and returns a slice of the resulted milestones
func (m *MilestoneDAO) GetMilestonesByDomain(domain string) ([]*campaignCfg.Milestone, error) {
	var result []*campaignCfg.Milestone
	err := m.table.Get(milestonesHashKey, domain).All(&result)
	sortMilestonesByParticipationThreshold(result)
	return result, err
}

// GetMilestonesByObjectiveID queries the Dynamo table using GSI on objective id and returns a slice of the resulted milestones
func (m *MilestoneDAO) GetMilestonesByObjectiveID(objectiveID string) ([]*campaignCfg.Milestone, error) {
	var result []*campaignCfg.Milestone
	err := m.table.Get(milestonesObjectiveIDHashKey, objectiveID).Index(milestonesObjectiveIDIndex).All(&result)
	sortMilestonesByParticipationThreshold(result)
	return result, err
}

// GetMilestonesByCampaignID queries the dynamo table using GSI on campaign id and returns a slice of the resulted milestones
func (m *MilestoneDAO) GetMilestonesByCampaignID(campaignID string) ([]*campaignCfg.Milestone, error) {
	var result []*campaignCfg.Milestone
	err := m.table.Get(milestonesCampaignIDHashKey, campaignID).Index(milestonesCampaignIDIndex).All(&result)
	sortMilestonesByParticipationThreshold(result)
	return result, err
}

// GetMilestoneByID queries the Dynamo table using GSI on milestone id and returns the result.
// If no result is found on a successful query, it returns nil without any error.
func (m *MilestoneDAO) GetMilestoneByID(milestoneID string) (*campaignCfg.Milestone, error) {
	var result *campaignCfg.Milestone
	err := m.table.Get(milestonesMilestoneIDHashKey, milestoneID).Index(milestonesMilestoneIDIndex).One(&result)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return result, err
}

// CreateTable for Milestone. FOR TESTING ONLY
func (m *MilestoneDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(m.table.Name(), m.client) {
		return nil
	}
	err := m.client.CreateTable(m.table.Name(), campaignCfg.Milestone{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Milestone. FOR TESTING ONLY
func (m *MilestoneDAO) DeleteTable() error {
	return m.table.DeleteTable().Run()
}

func sortMilestonesByParticipationThreshold(milestones []*campaignCfg.Milestone) {
	sort.Slice(milestones, func(i, j int) bool {
		if milestones[i].HandlerMetadata.SelectionStrategy == milestones[j].HandlerMetadata.SelectionStrategy {
			return milestones[i].Threshold < milestones[j].Threshold
		}
		return milestones[i].HandlerMetadata.SelectionStrategy >= milestones[j].HandlerMetadata.SelectionStrategy
	})
}
