package dynamo

import (
	"testing"

	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTrigger(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewTriggerDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("AddTrigger", func() {
			reward := campaignCfg.Reward{
				ID:   "reward",
				Type: "igc",
				Name: "Crate",
				Metadata: campaignCfg.RewardMetadata{
					SubType: "igc",
				},
				Repeatable: true,
				Quantity:   3,
			}
			triggerID := "hgc.test"
			rewardAttr := campaignCfg.RewardAttributes{
				Rewards: []*campaignCfg.Reward{&reward},
			}
			input := &campaignCfg.Trigger{
				ID:                         triggerID,
				Title:                      "Test",
				Description:                "Test",
				Domain:                     "hgc",
				RecipientRewardAttributes:  rewardAttr,
				BenefactorRewardAttributes: rewardAttr,
				TriggerType:                "CHEER",
				TriggerAmountMin:           100,
				TriggerAmountMax:           200,
				CampaignId:                 "hgc",
				Tokens:                     []*campaignCfg.Token{},
			}

			err := dao.AddTrigger(input)
			So(err, ShouldBeNil)

			Convey("GetTriggerssByDomain returns triggers given domain", func() {
				result, err := dao.GetTriggersByDomain("hgc")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetTriggersByCampaignID returns triggers given campaign ID", func() {
				result, err := dao.GetTriggersByCampaignID("hgc")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetTriggerByID returns trigger given trigger ID", func() {
				result, err := dao.GetTriggerByID(triggerID)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result, ShouldResemble, input)
			})
		})
	})
}
