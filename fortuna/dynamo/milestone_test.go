package dynamo

import (
	"testing"
	"time"

	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMilestone(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewMilestoneDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("AddMilestone", func() {
			reward := campaignCfg.Reward{
				ID:   "reward",
				Type: "emote",
				Name: "emote reward",
				Metadata: campaignCfg.RewardMetadata{
					SubType: "emote",
				},
			}
			rewards := []*campaignCfg.Reward{&reward}
			handlerMetadata := campaignCfg.HandlerMetadata{
				SelectionStrategy: "ALL",
				Rewards:           rewards,
				MilestoneTriggers: []string{},
			}
			objectiveID := "hgc2.individual.or.whatever"
			milestoneID := "hgc2.milestone1"
			input := &campaignCfg.Milestone{
				ID:              milestoneID,
				ActiveStartDate: time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC),
				ActiveEndDate:   time.Date(2088, time.February, 7, 9, 0, 0, 0, time.UTC),
				Domain:          "hgc",
				ObjectiveID:     objectiveID,
				CampaignId:      "hgc2",
				HandlerMetadata: handlerMetadata,
				RequiredSubs:    []string{"AAPOnly"},
			}

			err := dao.AddMilestone(input)
			So(err, ShouldBeNil)

			Convey("GetMilestonesByDomain returns milestones given domain", func() {
				result, err := dao.GetMilestonesByDomain("hgc")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetMilestonesByObjectiveID returns milestones given objective id", func() {
				result, err := dao.GetMilestonesByObjectiveID(objectiveID)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetMilestonesByCampaignID returns milestones given campaign id", func() {
				result, err := dao.GetMilestonesByCampaignID("hgc2")
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 1)
				So(result[0], ShouldResemble, input)
			})

			Convey("GetMilestoneByID returns milestone given id", func() {
				result, err := dao.GetMilestoneByID(milestoneID)
				So(err, ShouldBeNil)
				So(result, ShouldResemble, input)
			})

			Convey("GetMilestoneID finds no milestone given id", func() {
				result, err := dao.GetMilestoneByID("wrong-id")
				So(err, ShouldBeNil)
				So(result, ShouldBeNil)
			})
		})
	})
}
