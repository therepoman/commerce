package dynamo

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

const (
	rewardsTableName = "rewards"

	rewardsUserIDAttribute              = "userId"         // Partition Key
	rewardsScopedRewardIDAttribute      = "scopedRewardId" // Sort Key
	rewardsRewardTypeAttribute          = "rewardType"
	rewardsRewardIDAttribute            = "rewardId"
	rewardsDomainAttribute              = "domain"
	rewardsMilestoneIDAttribute         = "milestoneId"
	rewardsMilestoneTypeAttribute       = "milestoneType"
	rewardsExternalUserIDAttribute      = "externalUserId"
	rewardsFulfillmentStrategyAttribute = "fulfillmentStrategy"
	rewardsFulfillmentStatusAttribute   = "fulfillmentStatus"
	rewardsQuantityAttribute            = "quantity"
	rewardsTriggerIDAttribute           = "triggerId"
	rewardsBatchIDAttribute             = "batchId"
	rewardsCreatedAt                    = "createdAt"

	placeholder = "none"

	// Fulfillment Statuses
	FulfillmentStatusPending = "PENDING"
	FulfillmentStatusSuccess = "FULFILLMENT_SUCCESS"
	FulfillmentStatusFailure = "FULFILLMENT_FAILURE"
)

type Reward struct {
	UserID              string    `dynamo:"userId,hash"`
	ScopedRewardID      string    `dynamo:"scopedRewardId,range"`
	RewardType          string    `dynamo:"rewardType"`
	RewardID            string    `dynamo:"rewardId"`
	Domain              string    `dynamo:"domain"`
	MilestoneID         string    `dynamo:"milestoneId"`
	MilestoneType       string    `dynamo:"milestoneType"`
	ExternalUserID      string    `dynamo:"externalUserId"`
	FulfillmentStrategy string    `dynamo:"fulfillmentStrategy"`
	FulfillmentStatus   string    `dynamo:"fulfillmentStatus"`
	Quantity            int32     `dynamo:"quantity"`
	TriggerID           string    `dynamo:"triggerId"`
	CreatedAt           time.Time `dynamo:"createdAt"`
	// BatchID is used to group a rewards table entry with other entries in order to keep track of
	// which rows were part of the same fulfillment call
	BatchID string    `dynamo:"batchId"`
	TTL     time.Time `dynamo:"ttl"`
}

type RewardDAO struct {
	table   dynamo.Table
	client  *dynamo.DB
	limiter *rate.Limiter
}

type IRewardDAO interface {
	GetUserReward(userID string, rewardType string, rewardID string) (*Reward, error)
	GetUserRewards(userID string, rewardType string, rewardID string) ([]*Reward, error)
	GetUserRewardWithCount(userID string, rewardType string, rewardID string, count int) (*Reward, error)
	GetAllUserRewards(userID string) ([]*Reward, error)
	UpdateReward(input *Reward) (*Reward, error)
	ThrottledUpdateReward(input *Reward) (*Reward, error)
	DeleteUserRewards(userID string, rewardType string, rewardID string) error
	CreateTable() error
	DeleteTable() error
}

// NewRewardDAO creates a new DAO
func NewRewardDAO(db *dynamo.DB, cfg *config.Configuration) IRewardDAO {
	table := db.Table(FormatTableName(cfg.EnvironmentPrefix, rewardsTableName))
	limiter := rate.NewLimiter(rate.Limit(cfg.RewardDynamoWriteRateLimit), cfg.RewardDynamoWriteBurstLimit)

	return &RewardDAO{
		table,
		db,
		limiter,
	}
}

// GetUserReward returns exactly one non-repeatable reward. It returns an error if there is none found.
func (r *RewardDAO) GetUserReward(userID string, rewardType string, rewardID string) (*Reward, error) {
	scopedRewardID := BuildScopedRewardID(rewardType, rewardID)
	var result *Reward
	err := r.table.Get(rewardsUserIDAttribute, userID).
		Range(rewardsScopedRewardIDAttribute, dynamo.Equal, scopedRewardID).
		One(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// GetUserRewards returns a slice of rewards matching the rewardType and rewardID.
// Note: If the specified reward is not repeatable, the slice would only contain one item.
func (r *RewardDAO) GetUserRewards(userID string, rewardType string, rewardID string) ([]*Reward, error) {
	scopedRewardID := BuildScopedRewardID(rewardType, rewardID)
	var result []*Reward
	err := r.table.Get(rewardsUserIDAttribute, userID).
		Range(rewardsScopedRewardIDAttribute, dynamo.BeginsWith, scopedRewardID).
		All(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// GetUserRewardWithCount returns exactly one repeatable reward with specified count. It returns an error if there is none found.
// Note: Count is different from quantity. Count is the current number of reward that the user is on.
func (r *RewardDAO) GetUserRewardWithCount(userID string, rewardType string, rewardID string, count int) (*Reward, error) {
	scopedRewardID := BuildScopedRewardIDWithCount(rewardType, rewardID, count)
	var result *Reward
	err := r.table.Get(rewardsUserIDAttribute, userID).
		Range(rewardsScopedRewardIDAttribute, dynamo.Equal, scopedRewardID).
		One(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// GetUserRewardWithCount returns all the rewards for the user in the rewards table. It returns an error if there is none found.
func (r *RewardDAO) GetAllUserRewards(userID string) ([]*Reward, error) {
	var result []*Reward
	err := r.table.Get(rewardsUserIDAttribute, userID).
		All(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (r *RewardDAO) UpdateReward(input *Reward) (*Reward, error) {
	var result *Reward
	err := r.table.Update(rewardsUserIDAttribute, input.UserID).
		Range(rewardsScopedRewardIDAttribute, input.ScopedRewardID).
		Set(rewardsRewardTypeAttribute, input.RewardType).
		Set(rewardsRewardIDAttribute, input.RewardID).
		Set(rewardsDomainAttribute, input.Domain).
		Set(rewardsMilestoneIDAttribute, usePlaceholderIfEmpty(input.MilestoneID)).
		Set(rewardsMilestoneTypeAttribute, usePlaceholderIfEmpty(input.MilestoneType)).
		Set(rewardsExternalUserIDAttribute, usePlaceholderIfEmpty(input.ExternalUserID)).
		Set(rewardsFulfillmentStrategyAttribute, usePlaceholderIfEmpty(input.FulfillmentStrategy)).
		Set(rewardsFulfillmentStatusAttribute, input.FulfillmentStatus).
		Set(rewardsQuantityAttribute, input.Quantity).
		Set(rewardsTriggerIDAttribute, usePlaceholderIfEmpty(input.TriggerID)).
		Set(rewardsBatchIDAttribute, usePlaceholderIfEmpty(input.BatchID)).
		Set(rewardsCreatedAt, input.CreatedAt).
		// ttl being set for PDMS requirements
		Set(ttl, time.Now().AddDate(0, 0, 90)).
		Value(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// DeleteUserRewards deletes the reward records from the DynamoDB table.
// Warning: This is intended to be run as a cleanup mechanism in tests. Please don't run in production code!
func (r *RewardDAO) DeleteUserRewards(userID string, rewardType string, rewardID string) error {
	rewards, err := r.GetUserRewards(userID, rewardType, rewardID)
	if err != nil {
		return err
	}

	for _, reward := range rewards {
		if err := r.table.Delete(rewardsUserIDAttribute, reward.UserID).Range(rewardsScopedRewardIDAttribute, reward.ScopedRewardID).Run(); err != nil {
			return err
		}
	}

	return nil
}

func (r *RewardDAO) ThrottledUpdateReward(input *Reward) (*Reward, error) {
	r.limiter.Wait(context.Background())
	return r.UpdateReward(input)
}

// CreateTable for Reward. FOR TESTING ONLY
func (r *RewardDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(r.table.Name(), r.client) {
		return nil
	}
	err := r.client.CreateTable(r.table.Name(), Reward{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Reward. FOR TESTING ONLY
func (r *RewardDAO) DeleteTable() error {
	return r.table.DeleteTable().Run()

}

// BuildScopedRewardID builds a scopedRewardID string from the input rewardType and rewardID
func BuildScopedRewardID(rewardType string, rewardID string) string {
	return fmt.Sprintf("%s:%s", rewardType, rewardID)
}

// BuildScopedRewardIDWithCount builds a scopedRewardID string from the input rewardType, rewardID, and count
func BuildScopedRewardIDWithCount(rewardType string, rewardID string, count int) string {
	return fmt.Sprintf("%s:%d", BuildScopedRewardID(rewardType, rewardID), count)
}

// Dynamo doesn't allow empty strings. Return a placeholder string if the input is an empty string
func usePlaceholderIfEmpty(input string) string {
	if input == "" {
		return placeholder
	}
	return input
}
