package dynamo

import (
	"time"

	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	eligibilityTableName            = "eligibilities"
	eligibilityAttributeUserID      = "userId"
	eligibilityAttributeMilestoneID = "milestoneId"
	eligibilityAttributeIsEligible  = "isEligible"
)

// EligibilityDao holds table participants table
type EligibilityDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

// IEligibilityDao interface
type IEligibilityDAO interface {
	IsUserEligibleForMilestone(userID string, milestoneID string) (bool, error)
	AddEligibility(userID string, milestoneID string) (*Eligibility, error)
}

// Eligibility dynamo record struct
type Eligibility struct {
	UserID      string    `dynamo:"userId,hash"`       // Hash Key
	MilestoneID string    `dynamo:"milestoneId,range"` // Sort Key
	IsEligible  bool      `dynamo:"isEligible"`
	TTL         time.Time `dynamo:"ttl"`
}

// NewEligibilityDao creates a new DAO
func NewEligibilityDAO(db *dynamo.DB, cfg *config.Configuration) *EligibilityDAO {
	table := db.Table(FormatTableName(cfg.EnvironmentPrefix, eligibilityTableName))
	return &EligibilityDAO{
		table,
		db,
	}
}

// AddEligibility adds or updates an existing eligibility record
func (dao EligibilityDAO) AddEligibility(userID string, milestoneID string) (*Eligibility, error) {
	var result *Eligibility
	err := dao.table.Update(eligibilityAttributeUserID, userID).
		Range(eligibilityAttributeMilestoneID, milestoneID).
		Set(eligibilityAttributeIsEligible, true).
		// ttl being set for PDMS requirements
		Set(ttl, time.Now().AddDate(0, 0, 90)).
		Value(&result)

	if err != nil {
		return nil, errors.Wrap(err, "Failed to add eligiblity to dynamo table")
	}

	return result, nil
}

// IsUserEligibleForMilestone finds the record and returns it
func (dao EligibilityDAO) IsUserEligibleForMilestone(userID string, milestoneID string) (bool, error) {
	var result *Eligibility
	err := dao.table.
		Get(eligibilityAttributeUserID, userID).
		Range(eligibilityAttributeMilestoneID, dynamo.Equal, milestoneID).
		One(&result)

	// If record is not there, assume ineligibility
	if err == dynamo.ErrNotFound {
		return false, nil
	}

	if err != nil {
		return false, errors.Wrap(err, "Failed to query eligiblity in dynamo table")
	}

	return result.IsEligible, nil
}

// CreateTable for Eligibility. FOR TESTING ONLY
func (dao *EligibilityDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}
	err := dao.client.CreateTable(dao.table.Name(), Eligibility{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

// DeleteTable for Eligibility. FOR TESTING ONLY
func (dao *EligibilityDAO) DeleteTable() error {
	return dao.table.DeleteTable().Run()
}
