package dynamo

import (
	"code.justin.tv/commerce/fortuna/config"

	"github.com/guregu/dynamo"
)

const (
	participantTableName        = "participants"
	participantAttributeTrackID = "trackId"
	participantAttributeUserID  = "userId"
	participantAttributeAmount  = "amount"
)

// ParticipantDao holds table participants table
type ParticipantDao struct {
	table  dynamo.Table
	client *dynamo.DB
}

// Participant dynamo record struct
type Participant struct {
	TrackID string `dynamo:"trackId,hash"` // Hash Key
	UserID  string `dynamo:"userId,range"` // Sort Key
	Amount  int64  `dynamo:"amount"`
}

// NewParticipantDao creates a new DAO
func NewParticipantDao(db *dynamo.DB, cfg *config.Configuration) ParticipantDao {
	table := db.Table(FormatTableName(cfg.EnvironmentPrefix, participantTableName))
	return ParticipantDao{
		table,
		db,
	}
}

// AddParticipant adds or updates an existing participant record
func (dao ParticipantDao) AddParticipant(trackID string, userID string, amount int64) (*Participant, error) {
	var result *Participant
	err := dao.table.Update(participantAttributeTrackID, trackID).
		Range(participantAttributeUserID, userID).
		Add(participantAttributeAmount, amount).
		Value(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// GetAllParticipants returns all participants belonging to the given trackId
func (dao ParticipantDao) GetAllParticipants(trackID string) ([]*Participant, error) {
	var result []*Participant
	err := dao.table.
		Get(participantAttributeTrackID, trackID).
		All(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}
