package dynamo

import (
	campaignCfg "code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	campaignsTableName         = "campaigns"
	campaignsHashKey           = "domain"
	campaignsAllChannelsAttr   = "allChannels"
	campaignsCampaignIDIndex   = "campaignId-index"
	campaignsCampaignIDHashKey = "campaignId"
)

type CampaignDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

type ICampaignDAO interface {
	GetCampaignsByDomain(domain string) ([]*campaignCfg.Campaign, error)
	GetAllChannelsCampaigns() ([]*campaignCfg.Campaign, error)
	GetCampaignByID(campaignID string) (*campaignCfg.Campaign, error)
	AddCampaign(input *campaignCfg.Campaign) error
	CreateTable() error
	DeleteTable() error
}

// NewCampaignDAO returns an instance of CampaignDao
func NewCampaignDAO(client *dynamo.DB, cfg *config.Configuration) ICampaignDAO {
	return &CampaignDAO{
		client: client,
		table:  client.Table(FormatTableName(cfg.EnvironmentPrefix, campaignsTableName)),
	}
}

// AddCampaign Adds a campaign using the campaign config as an input
func (c *CampaignDAO) AddCampaign(input *campaignCfg.Campaign) error {
	return c.table.Put(input).Run()
}

// GetCampaignsByDomain queries the dynamo table using domain hash key and returns a slice of the resulted campaigns
func (c *CampaignDAO) GetCampaignsByDomain(domain string) ([]*campaignCfg.Campaign, error) {
	var result []*campaignCfg.Campaign
	err := c.table.Get(campaignsHashKey, domain).All(&result)
	return result, err
}

// GetAllChannelsCampaigns scans the dynamo table and returns all campaigns with AllChannel set to true.
// Note: This utilizes Scan, rather than Query, which is slower. The result is meant to be heavily cached.
func (c *CampaignDAO) GetAllChannelsCampaigns() ([]*campaignCfg.Campaign, error) {
	var result []*campaignCfg.Campaign
	err := c.table.Scan().Filter("$ = ?", campaignsAllChannelsAttr, true).All(&result)
	return result, err
}

// GetCampaignByID queries the Dynamo table using GSI on campaign id and returns the result.
func (c *CampaignDAO) GetCampaignByID(campaignID string) (*campaignCfg.Campaign, error) {
	var result *campaignCfg.Campaign
	err := c.table.Get(campaignsCampaignIDHashKey, campaignID).Index(campaignsCampaignIDIndex).One(&result)
	return result, err
}

// CreateTable for Campaign. FOR TESTING ONLY
func (c *CampaignDAO) CreateTable() error {
	if dynamoTest.DoesTableExist(c.table.Name(), c.client) {
		return nil
	}
	err := c.client.CreateTable(c.table.Name(), campaignCfg.Campaign{}).
		Provision(10, 5).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}
	return err
}

// DeleteTable for Campaign. FOR TESTING ONLY
func (c *CampaignDAO) DeleteTable() error {
	return c.table.DeleteTable().Run()
}
