package dynamo

import (
	"testing"
	"time"

	dynamoTest "code.justin.tv/commerce/fortuna/dynamo/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestClaimableReward(t *testing.T) {
	Convey("With table", t, func() {
		dynamoContext := dynamoTest.DynamoTestContext{}
		db := dynamoTest.CreateTestClient()
		cfg := dynamoTest.CreateTestConfiguration(testTablePrefix)
		dao := NewClaimableRewardDAO(db, cfg)
		dynamoContext.RegisterDao(dao)
		defer dynamoContext.Cleanup()

		Convey("AddClaimableReward", func() {
			testUserID := "testuser"
			domain := "test.domain"
			rewardType := "igc"
			rewardID1 := "test.reward.1"
			rewardID2 := "test.reward.2"
			timeStamp := time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC)

			input1 := &ClaimableReward{
				UserID:         testUserID,
				ScopedRewardID: BuildScopedClaimableRewardID(domain, rewardType, rewardID1, 1),
				Domain:         domain,
				RewardType:     rewardType,
				RewardID:       rewardID1,
				Quantity:       3,
				TriggerID:      "t1",
				Repeatable:     true,
				CreatedTime:    timeStamp,
			}

			input2 := &ClaimableReward{
				UserID:         testUserID,
				ScopedRewardID: BuildScopedClaimableRewardID(domain, rewardType, rewardID1, 2),
				Domain:         domain,
				RewardType:     rewardType,
				RewardID:       rewardID1,
				Quantity:       1,
				TriggerID:      "t1",
				Repeatable:     true,
				CreatedTime:    timeStamp,
			}

			input3 := &ClaimableReward{
				UserID:         testUserID,
				ScopedRewardID: BuildScopedClaimableRewardID(domain, rewardType, rewardID2, 0),
				Domain:         domain,
				RewardType:     rewardType,
				RewardID:       rewardID1,
				TriggerID:      "t2",
				CreatedTime:    timeStamp,
			}

			input4 := &ClaimableReward{
				UserID:         testUserID,
				ScopedRewardID: BuildScopedClaimableRewardID("other.domain", rewardType, "ignore.me", 1),
				Domain:         "other.domain",
				RewardType:     rewardType,
				RewardID:       "ignore.me",
				Quantity:       3,
				TriggerID:      "t3",
				Repeatable:     true,
				CreatedTime:    timeStamp,
			}

			err := dao.AddClaimableReward(input1)
			So(err, ShouldBeNil)
			err = dao.AddClaimableReward(input2)
			So(err, ShouldBeNil)
			err = dao.AddClaimableReward(input3)
			So(err, ShouldBeNil)
			err = dao.AddClaimableReward(input4)
			So(err, ShouldBeNil)

			Convey("GetUserRewardsForDomain returns all rewards belonging to the domain", func() {
				result, err := dao.GetUserRewardsForDomain(testUserID, domain)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 3)

				input1.TTL = result[0].TTL
				input2.TTL = result[1].TTL
				input3.TTL = result[2].TTL

				So(result[0], ShouldResemble, input1)
				So(result[1], ShouldResemble, input2)
				So(result[2], ShouldResemble, input3)
			})

			Convey("GetUserRewardsByID returns all rewards matching passed attributes", func() {
				result, err := dao.GetUserRewardsByID(testUserID, domain, rewardType, rewardID1)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 2)

				input1.TTL = result[0].TTL
				input2.TTL = result[1].TTL

				So(result[0], ShouldResemble, input1)
				So(result[1], ShouldResemble, input2)
			})

			Convey("Test GetClaimableMap", func() {
				result, err := dao.GetClaimableMapForUser(testUserID, domain)
				So(err, ShouldBeNil)
				So(result, ShouldResemble, map[string]int32{
					rewardID1: 5,
				})
			})

			Convey("DeleteClaimableRewardsForUser deletes all rewards for the user", func() {
				err := dao.DeleteClaimableRewardsForUserAndDomain(testUserID, domain)
				So(err, ShouldBeNil)
				result, err := dao.GetUserRewardsForDomain(testUserID, domain)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 0)
			})
		})
	})
}
