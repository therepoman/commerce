package api

import (
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
)

type CampaignConfig int

const (
	DefaultCampaign CampaignConfig = iota
	CampaignWithInactiveVisibleMilestones
	CampaignWithInactiveHiddenMilestones
	CampaignWithActiveHiddenMilestones
	InactiveCampaign
	StaffGatedCampaign
	WhitelistedCampaign
	StaffGatedAndWhitelistedCampaign
	CollectionObjectiveCampaign
	DeprecatedObjectiveCampaign
	DeprecatedCheerGroupCampaign
)

const (
	OtherDomain                    = "hgc"
	TestUserID                     = "u1"
	TestDomain                     = "owl"
	OWLDomain                      = "owl2018"
	RandomDomain                   = "rnd"
	TestMilestoneID1               = "m1"
	TestMilestoneID2               = "m2"
	TestMilestoneID3               = "m3"
	TestMilestoneID4               = "m4"
	TestMilestoneID5               = "m5"
	TestMilestoneID6               = "m6"
	TestMilestoneID7               = "m7"
	TestMilestonePrimeID           = "mprime"
	TestOwlMilestoneID1            = "owl-m1"
	TestOwlMilestoneID2            = "owl-m2"
	TestRewardID1                  = "r1"
	TestRewardID2                  = "r2"
	TestRewardID3                  = "r3"
	TestRewardID4                  = "r4"
	TestRewardID5                  = "r5"
	TestRewardID6                  = "r6"
	TestRewardBitsID               = "rbits"
	TestRewardName                 = "rewardname"
	TestRewardType                 = "emote"
	TestIndividualHandler          = "IndividualCheerHandler"
	TestGlobalHandler              = "GlobalCheerHandler"
	TestTeamHandler                = "TeamCheerHandler"
	TestSubsHandler                = "SubscriptionHandler"
	TestCollectionHandler          = "CollectionHandler"
	TestChannelID                  = "56789"
	TestTeam1RewardName            = "OWLBOS"
	TestTeam2RewardName            = "OWLDAL"
	TestObjectiveID1               = "test.objective.id.1"
	TestObjectiveID2               = "test.objective.id.2"
	TestObjectiveID3               = "test.objective.id.3"
	TestObjectiveID4               = "test.objective.id.4"
	TestObjectiveID5               = "test.objective.id.5"
	TestTriggerID1                 = "test.trigger.id.1"
	TriggerMilestoneID             = "owl2018.collection.milestone1"
	StaffGatedDomain               = "staff-gated-domain"
	WhitelistedDomain              = "whitelisted-domain"
	StaffGatedAndWhitelistedDomain = "staff-gated-and-whitelisted-domain"
	WhitelistedDomainUser          = "3333"
	CollectionObjectiveDomain      = "CollectionObjectiveDomain"
	DeprecatedObjectiveDomain      = "DeprecatedObjectiveDomain"
	DeprecatedCheerGroupDomain     = "DeprecatedCheerGroupDomain"

	CheerGroupKey1          = "CheerGroup1"
	CheerGroupKey2          = "CheerGroup2"
	DeprecatedCheerGroupKey = "DeprecatedCheerGroup"
)

var (
	ActiveStartDate       = time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC)
	ActiveEndDate         = time.Date(2118, time.February, 7, 9, 0, 0, 0, time.UTC)
	ActiveStartDateFuture = time.Date(3018, time.February, 7, 9, 0, 0, 0, time.UTC)
	ActiveEndDateFuture   = time.Date(3020, time.February, 7, 9, 0, 0, 0, time.UTC)

	VisibleStartDate       = time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC)
	VisibleEndDate         = time.Date(2118, time.February, 7, 9, 0, 0, 0, time.UTC)
	VisibleStartDateFuture = time.Date(3018, time.February, 7, 9, 0, 0, 0, time.UTC)
	VisibleEndDateFuture   = time.Date(3020, time.February, 7, 9, 0, 0, 0, time.UTC)

	CheerGroup1 = &campaign.CheerGroup{
		CheerCodes: map[string]bool{
			"CHEERCODE1": true,
		},
		Name: CheerGroupKey1,
	}

	CheerGroup2 = &campaign.CheerGroup{
		CheerCodes: map[string]bool{
			"CHEERCODE2": true,
		},
		Name: CheerGroupKey2,
	}

	DeprecatedCheerGroup = &campaign.CheerGroup{
		CheerCodes: map[string]bool{
			"DEPRECATEDCHEERCODE": true,
		},
		Name:       DeprecatedCheerGroupKey,
		Deprecated: true,
	}
)

type testDomainKey int

const (
	otherDomain testDomainKey = iota
	testDomain
	owlDomain
	randomDomain
	inactiveVisibleMilestoneDomain
	inactiveHiddenMilestoneDomain
	activeHiddenMilestoneDomain
	inactiveDomain
	staffGatedDomain
	whitelistedDomain
	staffGatedAndWhitelistedDomain
	collectionObjectiveDomain
	oldCheerbombombDomain
	deprecatedObjectiveDomain
	deprecatedCheerGroupDomain
)

func initTestDomains() map[testDomainKey][]*campaign.Campaign {
	return map[testDomainKey][]*campaign.Campaign{
		otherDomain: {
			{
				ID:              OtherDomain,
				Title:           OtherDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           "other-o1",
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     "other-m1",
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		testDomain: {
			{
				ID:              TestDomain,
				Title:           TestDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				CheerGroups: map[string]*campaign.CheerGroup{
					CheerGroupKey1: CheerGroup1,
					CheerGroupKey2: CheerGroup2,
				},
				Objectives: []*campaign.Objective{
					{
						ID:            "hgc-o1",
						Tag:           campaign.ObjectiveTagIndividual,
						EventHandler:  TestIndividualHandler,
						CheerGroupKey: CheerGroupKey1,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID1,
											Name:        "emote 1",
											Type:        "emote",
											Description: "Claimed by u1",
											ImageURL:    "https://lorempixel.com/200/200",
											Rarity:      1,
										},
									},
								},
							},
							{
								ID:                     TestMilestoneID2,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID2,
											Name:        "emote 2",
											Type:        "emote",
											Description: "Not Claimed",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
							{
								ID:                     TestMilestoneID3,
								Threshold:              int64(101),
								ParticipationThreshold: int64(101),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID3,
											Name:        "Owl Badge reward",
											Type:        "badge",
											Description: "Owl Badge reward description",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           "hgc-o2",
						Tag:          campaign.ObjectiveTagInsider,
						EventHandler: TestSubsHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:               TestMilestonePrimeID,
								ActiveStartDate:  ActiveStartDate,
								ActiveEndDate:    ActiveEndDate,
								VisibleStartDate: VisibleStartDate,
								VisibleEndDate:   VisibleEndDate,
								IsPrimeOnly:      true,
								CheckEligibility: true,
								Threshold:        1,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardBitsID,
											Name:        "Owl Prime bits",
											Type:        "bits",
											Description: "Owl prime bits description",
											Quantity:    200,
											VendorType:  "BitsForOWL",
										},
									},
								},
								UserWhiteListEnabled: true,
								UserWhitelist: map[string]bool{
									TestUserID: true,
								},
							},
							{
								ID:               "mprime-old",
								ActiveStartDate:  ActiveStartDate,
								ActiveEndDate:    ActiveStartDate,
								VisibleStartDate: VisibleStartDate,
								VisibleEndDate:   VisibleEndDate,
								IsPrimeOnly:      true,
								CheckEligibility: true,
								Threshold:        1,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "old-bits",
											Name:        "Owl Prime bits",
											Type:        "bits",
											Description: "Owl prime bits description",
											Quantity:    200,
											VendorType:  "BitsForOWL",
										},
									},
								},
								UserWhiteListEnabled: true,
								UserWhitelist: map[string]bool{
									TestUserID: true,
								},
							},
						},
					},
				},
				Triggers: []*campaign.Trigger{
					{
						ID:                        TestTriggerID1,
						TriggerAmountMax:          1000,
						TriggerAmountMin:          100,
						TriggerType:               campaign.TriggerTypeCheer,
						ActiveStartDate:           ActiveStartDate,
						ActiveEndDate:             ActiveEndDate,
						VisibleStartDate:          VisibleStartDate,
						VisibleEndDate:            VisibleEndDate,
						RecipientSelectionFormula: "hgc",
						BenefactorRewardAttributes: campaign.RewardAttributes{
							Rewards: []*campaign.Reward{
								{
									ID:          TestRewardID5,
									Name:        "Some repeatable thing",
									Type:        "igc",
									Description: "description",
									Quantity:    3,
									Repeatable:  true,
								},
							},
						},
						RecipientRewardAttributes: campaign.RewardAttributes{
							Rewards: []*campaign.Reward{
								{
									ID:          TestRewardID6,
									Name:        "Some repeatable thing",
									Type:        "igc",
									Description: "description",
									Quantity:    1,
									Repeatable:  true,
								},
							},
						},
					},
				},
			},
		},

		owlDomain: {
			{
				ID:              OWLDomain,
				Title:           OWLDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           TestObjectiveID2,
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestOwlMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID1,
						Tag:          campaign.ObjectiveTagGlobal,
						EventHandler: TestGlobalHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestOwlMilestoneID2,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r2",
											Name:        "hgc igc",
											Type:        "igc",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID3,
						Tag:          campaign.ObjectiveTagTeam,
						EventHandler: TestTeamHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID5,
								Threshold:              int64(200),
								ParticipationThreshold: int64(100),
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: "REWARDCODE",
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID4,
											Name:        TestTeam1RewardName,
											Type:        "emote",
											Code:        TestTeam1RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
										{
											ID:          TestRewardID5,
											Name:        TestTeam2RewardName,
											Type:        "emote",
											Code:        TestTeam2RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID4,
						Tag:          campaign.ObjectiveTagInsider,
						EventHandler: TestSubsHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID6,
								Threshold:              int64(200),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: "REWARDCODE",
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID5,
											Name:        TestTeam1RewardName,
											Type:        "emote",
											Code:        TestTeam1RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID5,
						Tag:          campaign.ObjectiveTagCollection,
						EventHandler: TestCollectionHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID7,
								Threshold:              int64(200),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: "ALL",
									MilestoneTriggers: []string{TriggerMilestoneID},
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID6,
											Name:        TestTeam1RewardName,
											Type:        "emote",
											Code:        TestTeam1RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		inactiveVisibleMilestoneDomain: {
			{
				ID:              OWLDomain,
				Title:           OWLDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           TestObjectiveID2,
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestOwlMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDateFuture,
								ActiveEndDate:          ActiveEndDateFuture,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID3,
						Tag:          campaign.ObjectiveTagTeam,
						EventHandler: TestTeamHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID5,
								Threshold:              int64(200),
								ParticipationThreshold: int64(100),
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: "REWARDNAME",
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID4,
											Name:        TestTeam1RewardName,
											Type:        "emote",
											Code:        TestTeam1RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		inactiveHiddenMilestoneDomain: {
			{
				ID:              OWLDomain,
				Title:           OWLDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           TestObjectiveID2,
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestOwlMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDateFuture,
								ActiveEndDate:          ActiveEndDateFuture,
								VisibleStartDate:       VisibleStartDateFuture,
								VisibleEndDate:         VisibleEndDateFuture,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID3,
						Tag:          campaign.ObjectiveTagTeam,
						EventHandler: TestTeamHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID5,
								Threshold:              int64(200),
								ParticipationThreshold: int64(100),
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: "REWARDNAME",
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID4,
											Name:        TestTeam1RewardName,
											Type:        "emote",
											Code:        TestTeam1RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		activeHiddenMilestoneDomain: {
			{
				ID:              OWLDomain,
				Title:           OWLDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           TestObjectiveID2,
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     "hgc-m1",
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDateFuture,
								VisibleEndDate:         VisibleEndDateFuture,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
					{
						ID:           TestObjectiveID3,
						Tag:          campaign.ObjectiveTagTeam,
						EventHandler: TestTeamHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID5,
								Threshold:              int64(200),
								ParticipationThreshold: int64(100),
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: "REWARDNAME",
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID4,
											Name:        TestTeam1RewardName,
											Type:        "emote",
											Code:        TestTeam1RewardName,
											Description: "Met by u1",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		randomDomain: {
			{
				ID:              RandomDomain,
				Title:           RandomDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           "rnd-o1",
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						RandomRewards: []*campaign.Reward{
							{
								ID:          "random-r1",
								Name:        "random emote",
								Type:        "emote",
								Description: "Entitle only one",
								ImageURL:    "https://lorempixel.com/200/200",
							},
							{
								ID:          "random-r2",
								Name:        "random emote",
								Type:        "emote",
								Description: "Entitle only one",
								ImageURL:    "https://lorempixel.com/200/200",
							},
							{
								ID:          "random-r3",
								Name:        "hgc emote",
								Type:        "emote",
								Description: "Entitle only one",
								ImageURL:    "https://lorempixel.com/200/200",
							},
						},
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID4,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: campaign.RandomTrackSelectionStrategy,
									Rewards:           []*campaign.Reward{},
								},
							},
							{
								ID:                     "rnd-m2",
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: campaign.RandomTrackSelectionStrategy,
									Rewards:           []*campaign.Reward{},
								},
							},
							{
								ID:                     "rnd-m3",
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									SelectionStrategy: campaign.RandomTrackSelectionStrategy,
									Rewards:           []*campaign.Reward{},
								},
							},
						},
					},
				},
			},
		},

		inactiveDomain: {
			{
				ID:              OWLDomain,
				Title:           OWLDomain,
				ActiveStartDate: ActiveStartDateFuture,
				ActiveEndDate:   ActiveEndDateFuture,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           TestObjectiveID2,
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestOwlMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDateFuture,
								ActiveEndDate:          ActiveEndDateFuture,
								VisibleStartDate:       VisibleStartDateFuture,
								VisibleEndDate:         VisibleEndDateFuture,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
				Triggers: []*campaign.Trigger{
					{
						ID:               TestTriggerID1,
						ActiveStartDate:  ActiveStartDate,
						ActiveEndDate:    ActiveEndDate,
						VisibleStartDate: VisibleStartDate,
						VisibleEndDate:   VisibleEndDate,
					},
				},
			},
		},

		staffGatedDomain: {
			{
				ID:              StaffGatedDomain,
				Title:           StaffGatedDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				StaffOnly:       true,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				CheerGroups: map[string]*campaign.CheerGroup{
					CheerGroupKey1: CheerGroup1,
					CheerGroupKey2: CheerGroup2,
				},
				Objectives: []*campaign.Objective{
					{
						ID:            "hgc-o1",
						Tag:           campaign.ObjectiveTagIndividual,
						EventHandler:  TestIndividualHandler,
						CheerGroupKey: CheerGroupKey1,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID1,
											Name:        "emote 1",
											Type:        "emote",
											Description: "Claimed by u1",
											ImageURL:    "https://lorempixel.com/200/200",
											Rarity:      1,
										},
									},
								},
							},
						},
					},
				},
			},
		},

		whitelistedDomain: {
			{
				ID:                   WhitelistedDomain,
				Title:                WhitelistedDomain,
				ActiveStartDate:      ActiveStartDate,
				ActiveEndDate:        ActiveEndDate,
				UserWhitelistEnabled: true,
				UserWhitelist: map[string]bool{
					WhitelistedDomainUser: true,
				},
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				CheerGroups: map[string]*campaign.CheerGroup{
					CheerGroupKey1: CheerGroup1,
					CheerGroupKey2: CheerGroup2,
				},
				Objectives: []*campaign.Objective{
					{
						ID:            "hgc-o1",
						Tag:           campaign.ObjectiveTagIndividual,
						EventHandler:  TestIndividualHandler,
						CheerGroupKey: CheerGroupKey1,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID1,
											Name:        "emote 1",
											Type:        "emote",
											Description: "Claimed by u1",
											ImageURL:    "https://lorempixel.com/200/200",
											Rarity:      1,
										},
									},
								},
							},
						},
					},
				},
			},
		},

		staffGatedAndWhitelistedDomain: {
			{
				ID:                   StaffGatedAndWhitelistedDomain,
				Title:                StaffGatedAndWhitelistedDomain,
				ActiveStartDate:      ActiveStartDate,
				ActiveEndDate:        ActiveEndDate,
				StaffOnly:            true,
				UserWhitelistEnabled: true,
				UserWhitelist: map[string]bool{
					WhitelistedDomainUser: true,
				},
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				CheerGroups: map[string]*campaign.CheerGroup{
					CheerGroupKey1: CheerGroup1,
					CheerGroupKey2: CheerGroup2,
				},
				Objectives: []*campaign.Objective{
					{
						ID:            "hgc-o1",
						Tag:           campaign.ObjectiveTagIndividual,
						EventHandler:  TestIndividualHandler,
						CheerGroupKey: CheerGroupKey1,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID1,
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          TestRewardID1,
											Name:        "emote 1",
											Type:        "emote",
											Description: "Claimed by u1",
											ImageURL:    "https://lorempixel.com/200/200",
											Rarity:      1,
										},
									},
								},
							},
						},
					},
				},
			},
		},

		collectionObjectiveDomain: {
			{
				ID:              CollectionObjectiveDomain,
				Title:           CollectionObjectiveDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				Objectives: []*campaign.Objective{
					{
						ID:            "hgc-o1",
						Tag:           campaign.ObjectiveTagCollection,
						EventHandler:  TestIndividualHandler,
						CheerGroupKey: CheerGroupKey1,
						Milestones: []*campaign.Milestone{
							{
								ID:                     TestMilestoneID1,
								Threshold:              int64(1),
								ParticipationThreshold: int64(1),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									MilestoneTriggers: []string{
										TestMilestoneID3,
										TestMilestoneID4,
									},
								},
							},
							{
								ID:                     TestMilestoneID2,
								Threshold:              int64(1),
								ParticipationThreshold: int64(1),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									MilestoneTriggers: []string{
										TestMilestoneID1,
										TestMilestoneID3,
										TestMilestoneID5,
									},
								},
							},
						},
					},
				},
			},
		},

		deprecatedObjectiveDomain: {
			{
				ID:              OtherDomain,
				Title:           OtherDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:           "other-o1",
						Deprecated:   true,
						Tag:          campaign.ObjectiveTagIndividual,
						EventHandler: TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     "other-m1",
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		deprecatedCheerGroupDomain: {
			{
				ID:              OtherDomain,
				Title:           OtherDomain,
				ActiveStartDate: ActiveStartDate,
				ActiveEndDate:   ActiveEndDate,
				CheerGroups: map[string]*campaign.CheerGroup{
					DeprecatedCheerGroupKey: DeprecatedCheerGroup,
				},
				ChannelList: map[string]bool{
					TestChannelID: true,
				},
				Objectives: []*campaign.Objective{
					{
						ID:            "other-o1",
						CheerGroupKey: DeprecatedCheerGroupKey,
						Tag:           campaign.ObjectiveTagIndividual,
						EventHandler:  TestIndividualHandler,
						Milestones: []*campaign.Milestone{
							{
								ID:                     "other-m1",
								Threshold:              int64(100),
								ParticipationThreshold: int64(100),
								ActiveStartDate:        ActiveStartDate,
								ActiveEndDate:          ActiveEndDate,
								VisibleStartDate:       VisibleStartDate,
								VisibleEndDate:         VisibleEndDate,
								HandlerMetadata: campaign.HandlerMetadata{
									Rewards: []*campaign.Reward{
										{
											ID:          "hgc-r1",
											Name:        "hgc emote",
											Type:        "emote",
											Description: "Should be ignored",
											ImageURL:    "https://lorempixel.com/200/200",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
}

// InitTestCampaignConfig creates and normalizes a campaign configuration based on the enum specified
func InitTestCampaignConfig(campaignType CampaignConfig) (*campaign.Configuration, error) {
	campaignConfig := &campaign.Configuration{}

	testDomains := initTestDomains()

	switch campaignType {
	case DefaultCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			OtherDomain:  testDomains[otherDomain],
			TestDomain:   testDomains[testDomain],
			OWLDomain:    testDomains[owlDomain],
			RandomDomain: testDomains[randomDomain],
		}
	case CampaignWithInactiveVisibleMilestones:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			OWLDomain: testDomains[inactiveVisibleMilestoneDomain],
		}
	case CampaignWithInactiveHiddenMilestones:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			OWLDomain: testDomains[inactiveHiddenMilestoneDomain],
		}
	case CampaignWithActiveHiddenMilestones:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			OWLDomain: testDomains[activeHiddenMilestoneDomain],
		}
	case InactiveCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			OWLDomain: testDomains[inactiveDomain],
		}
	case StaffGatedCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			StaffGatedDomain: testDomains[staffGatedDomain],
		}
	case WhitelistedCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			WhitelistedDomain: testDomains[whitelistedDomain],
		}
	case StaffGatedAndWhitelistedCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			StaffGatedAndWhitelistedDomain: testDomains[staffGatedAndWhitelistedDomain],
		}
	case CollectionObjectiveCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			CollectionObjectiveDomain: testDomains[collectionObjectiveDomain],
		}
	case DeprecatedObjectiveCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			DeprecatedObjectiveDomain: testDomains[deprecatedObjectiveDomain],
		}
	case DeprecatedCheerGroupCampaign:
		campaignConfig.Campaigns = map[string][]*campaign.Campaign{
			DeprecatedCheerGroupDomain: testDomains[deprecatedCheerGroupDomain],
		}
	}

	err := campaignConfig.DenormalizeData()
	if err != nil {
		return nil, err
	}
	return campaignConfig, nil
}
