package api

import (
	"net/http"

	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"

	"code.justin.tv/commerce/fortuna/middleware"
	ft "code.justin.tv/commerce/fortuna/twirp"
	twirp_statsd "code.justin.tv/common/twirp/hooks/statsd"
	"code.justin.tv/foundation/twitchserver"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	"goji.io"
	"goji.io/pat"
)

// FortunaServer api
type FortunaServer struct {
	AdminAPI
}

// Server struct
type Server struct {
	*goji.Mux
}

// NewServer defines handlers
func (fortunaServer *FortunaServer) NewServer(stats statsd.Statter, s2sClient *callee.Client) (*Server, error) {
	server := twitchserver.NewServer()

	twitchserver.AddDefaultSignalHandlers()
	s := &Server{
		server,
	}

	// middleware
	handlerMetrics := middleware.NewHandlerMetrics(stats)
	s.Use(middleware.PanicLogger)
	s.Use(handlerMetrics.Metrics)

	s.HandleFunc(pat.Get("/"), s.everythingIsFine)
	s.HandleFunc(pat.Get("/health"), s.everythingIsFine)
	s.HandleFunc(pat.Get("/debug/running"), s.everythingIsFine)

	twirpStatsHook := twirp_statsd.NewStatsdServerHooks(stats)
	twirpHandler := ft.NewFortunaServer(fortunaServer, twirpStatsHook, nil)

	s.Handle(pat.Post(ft.FortunaPathPrefix+"*"), s2sClient.CapabilitiesInjectorMiddleware(twirpHandler))

	return s, nil
}

func (s *Server) everythingIsFine(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.WithError(err).Fatalf("Fatal API error")
	}
}

// Deprecated APIs
func (f *FortunaServer) ListRewardsByTag(ctx context.Context, req *ft.ListRewardsRequest) (*ft.ListRewardsByTagResponse, error) {
	return &ft.ListRewardsByTagResponse{}, nil
}
func (f *FortunaServer) ClaimMilestone(ctx context.Context, req *ft.ClaimMilestoneRequest) (*ft.ClaimMilestoneResponse, error) {
	return &ft.ClaimMilestoneResponse{}, nil
}
func (f *FortunaServer) ClaimAllRewards(ctx context.Context, req *ft.ClaimAllRewardsRequest) (*ft.ClaimAllRewardsResponse, error) {
	return &ft.ClaimAllRewardsResponse{}, nil
}
func (f *FortunaServer) DebugClaimMilestone(ctx context.Context, req *ft.ClaimMilestoneRequest) (*ft.ClaimMilestoneResponse, error) {
	return &ft.ClaimMilestoneResponse{}, nil
}
func (f *FortunaServer) GetGlobalProgressionState(ctx context.Context, req *ft.ProgressionStateRequest) (*ft.ProgressionStateResponse, error) {
	return &ft.ProgressionStateResponse{}, nil
}
func (f *FortunaServer) GetUserProgressionState(ctx context.Context, req *ft.ProgressionStateRequest) (*ft.ProgressionStateResponse, error) {
	return &ft.ProgressionStateResponse{}, nil
}
func (f *FortunaServer) GetObjectivesWithProgressionState(ctx context.Context, req *ft.ObjectivesRequest) (*ft.ObjectivesResponse, error) {
	return &ft.ObjectivesResponse{}, nil
}
func (f *FortunaServer) GetChannelInfo(ctx context.Context, req *ft.GetChannelInfoRequest) (*ft.GetChannelInfoResponse, error) {
	return &ft.GetChannelInfoResponse{}, nil
}
func (f *FortunaServer) GetTriggers(ctx context.Context, req *ft.GetTriggersRequest) (*ft.GetTriggersResponse, error) {
	return &ft.GetTriggersResponse{}, nil
}
func (f *FortunaServer) EmitDiscoveryEvent(ctx context.Context, req *ft.EmitDiscoveryEventRequest) (*ft.EmitDiscoveryEventResponse, error) {
	return &ft.EmitDiscoveryEventResponse{}, nil
}
func (f *FortunaServer) GetCampaignById(ctx context.Context, req *ft.GetCampaignByIdRequest) (*ft.GetCampaignByIdResponse, error) {
	return &ft.GetCampaignByIdResponse{}, nil
}
