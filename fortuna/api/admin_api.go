package api

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/rewards"
	ft "code.justin.tv/commerce/fortuna/twirp"
	"code.justin.tv/common/twirp"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	dynamoConst "github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// TODO: Add authentication layer like malachai/s2s (malachai now supports twirp!)

// AdminAPI handles admistrative tasks.
type AdminAPI struct {
	Config    *config.Configuration
	Clients   *clients.Clients
	Fulfiller rewards.IFulfiller
	Utils     rewards.IUtils
	DAOs      *dynamo.DAOs
}

// AdminInvalidateCampaignCache invalidates the cache for one domain and returns the result
// In addition, it also invalidates the AllChannels campaigns
func (c *AdminAPI) AdminInvalidateCampaignCache(ctx context.Context, req *ft.AdminInvalidateCampaignCacheRequest) (*ft.AdminInvalidateCampaignCacheResponse, error) {
	invalidated, err := c.Utils.InvalidateCampaignCacheForDomain(req.Domain)
	if err != nil {
		log.WithField("Domain", req.Domain).WithError(err).Error("failed to invalidate domain cached")
		return nil, twirp.InternalError("failed to invalidate")
	}

	// Also invalidate the all channels campaign cache
	allChannelsInvalidated, err := c.Utils.InvalidateAllChannelsCampaignsCache()
	if err != nil {
		log.WithError(err).Error("failed to invalidate all channels cache")
	}

	return &ft.AdminInvalidateCampaignCacheResponse{
		Invalidated:            invalidated,
		AllChannelsInvalidated: allChannelsInvalidated,
	}, nil
}

// AdminGetCampaigns returns a list of campaigns belonging to the domain directly from dynamo
func (c *AdminAPI) AdminGetCampaigns(ctx context.Context, req *ft.AdminGetCampaignsRequest) (*ft.AdminGetCampaignsResponse, error) {
	var rawCampaigns []*campaign.Campaign
	var err error
	if req.Domain != "" {
		// If Domain is specified, return the uncached list of campaigns for the domain.
		rawCampaigns, err = c.DAOs.CampaignDAO.GetCampaignsByDomain(req.Domain)
	} else if req.ChannelId != "" {
		// If Channel ID is specified instead, return the list of campaigns on the channel just like how the user visiting the channel would see.
		// Note that this can be across multiple domains.
		var campaignConfig *campaign.Configuration
		campaignConfig, err = c.Utils.GetCampaignConfigForChannelID(req.ChannelId)
		if campaignConfig != nil {
			rawCampaigns = []*campaign.Campaign{}
			for _, campaigns := range campaignConfig.Campaigns {
				rawCampaigns = append(rawCampaigns, campaigns...)
			}
		}
	} else {
		return nil, twirp.InvalidArgumentError("domain", "domain or channel_id is required")
	}

	if err != nil {
		log.WithField("Domain", req.Domain).WithError(err).Error("failed to get campaigns")
		return nil, twirp.InternalError("failed to get campaigns")
	}

	campaigns := []*ft.CampaignConfig{}

	for _, rawCampaign := range rawCampaigns {
		campaigns = append(campaigns, convertRawCampaignToProtobufCampaign(rawCampaign))
	}

	return &ft.AdminGetCampaignsResponse{
		Campaigns: campaigns,
	}, nil
}

// AdminGetObjectives returns a list of objectives based on the query directly from dynamo
func (c *AdminAPI) AdminGetObjectives(ctx context.Context, req *ft.AdminGetObjectivesRequest) (*ft.AdminGetObjectivesResponse, error) {
	// For now, only support querying based on parent campaign id
	rawObjectives, err := c.DAOs.ObjectiveDAO.GetObjectivesByCampaignID(req.CampaignId)

	if err != nil {
		log.WithField("CampaignID", req.CampaignId).WithError(err).Error("failed to get objectives")
		return nil, twirp.InternalError("failed to get objectives")
	}

	objectives := []*ft.ObjectiveConfig{}

	for _, rawObjective := range rawObjectives {
		objectives = append(objectives, convertRawObjectiveToProtobufObjective(rawObjective))
	}

	return &ft.AdminGetObjectivesResponse{
		Objectives: objectives,
	}, nil
}

// AdminGetMilestones returns a list of milestones based on the query directly from dynamo
func (c *AdminAPI) AdminGetMilestones(ctx context.Context, req *ft.AdminGetMilestonesRequest) (*ft.AdminGetMilestonesResponse, error) {
	// For now, only support querying based on parent objective id
	rawMilestones, err := c.DAOs.MilestoneDAO.GetMilestonesByObjectiveID(req.ObjectiveId)

	if err != nil {
		log.WithField("ObjectiveID", req.ObjectiveId).WithError(err).Error("failed to get milestones")
		return nil, twirp.InternalError("failed to get milestones")
	}

	milestones := []*ft.MilestoneConfig{}

	for _, rawMilestone := range rawMilestones {
		milestones = append(milestones, convertRawMilestoneToProtobufMilestone(rawMilestone))
	}

	return &ft.AdminGetMilestonesResponse{
		Milestones: milestones,
	}, nil
}

// AdminGetMilestone returns a single milestone based on the query directly from dynamo
func (c *AdminAPI) AdminGetMilestone(ctx context.Context, req *ft.AdminGetMilestoneRequest) (*ft.AdminGetMilestoneResponse, error) {
	rawMilestone, err := c.DAOs.MilestoneDAO.GetMilestoneByID(req.MilestoneId)

	if err == dynamoConst.ErrNotFound || rawMilestone == nil {
		return nil, twirp.InvalidArgumentError("milestone_id", "milestone does not exist")
	} else if err != nil {
		log.WithField("MilestoneID", req.MilestoneId).WithError(err).Error("AdminGetMilestone: failed to get milestone")
		return nil, twirp.InternalError("failed to get milestone")
	}

	milestone := convertRawMilestoneToProtobufMilestone(rawMilestone)

	return &ft.AdminGetMilestoneResponse{
		Milestone: milestone,
	}, nil
}

// AdminGetMilestoneRewards returns a list of rewards based on the query directly from dynamo
func (c *AdminAPI) AdminGetMilestoneRewards(ctx context.Context, req *ft.AdminGetMilestoneRewardsRequest) (*ft.AdminGetMilestoneRewardsResponse, error) {
	rawMilestone, err := c.DAOs.MilestoneDAO.GetMilestoneByID(req.MilestoneId)

	if err != nil {
		log.WithField("MilestoneId", req.MilestoneId).WithError(err).Error("AdminGetMilestoneRewards: failed to get milestone")
		return nil, twirp.InternalError("failed to get milestone")
	}

	if rawMilestone == nil {
		return nil, twirp.InvalidArgumentError("milestone_id", "milestone does not exist")
	}

	rewards := []*ft.RewardConfig{}

	for _, rawReward := range rawMilestone.HandlerMetadata.Rewards {
		rewards = append(rewards, convertRawRewardToProtobufReward(rawReward))
	}

	return &ft.AdminGetMilestoneRewardsResponse{
		Rewards: rewards,
	}, nil
}

// AdminGetTrigger returns a single trigger based on the query directly from dynamo
func (c *AdminAPI) AdminGetTrigger(ctx context.Context, req *ft.AdminGetTriggerRequest) (*ft.AdminGetTriggerResponse, error) {
	rawTrigger, err := c.DAOs.TriggerDAO.GetTriggerByID(req.TriggerId)

	if err == dynamoConst.ErrNotFound || rawTrigger == nil {
		return nil, twirp.InvalidArgumentError("trigger_id", "trigger does not exist")
	} else if err != nil {
		log.WithField("TriggerId", req.TriggerId).WithError(err).Error("AdminGetTrigger: failed to get trigger")
		return nil, twirp.InternalError("failed to get trigger")
	}

	trigger := convertRawTriggerToProtobufTrigger(rawTrigger)

	return &ft.AdminGetTriggerResponse{
		Trigger: trigger,
	}, nil
}

// AdminGetTriggers returns a list of triggers based on the query directly from dynamo
func (c *AdminAPI) AdminGetTriggers(ctx context.Context, req *ft.AdminGetTriggersRequest) (*ft.AdminGetTriggersResponse, error) {
	rawTriggers, err := c.DAOs.TriggerDAO.GetTriggersByCampaignID(req.CampaignId)

	if err != nil {
		log.WithField("CampaignId", req.CampaignId).WithError(err).Error("AdminGetTriggers: failed to get triggers")
		return nil, twirp.InternalError("failed to get triggers")
	}

	triggers := []*ft.TriggerConfig{}
	for _, rawTrigger := range rawTriggers {
		triggers = append(triggers, convertRawTriggerToProtobufTrigger(rawTrigger))
	}

	return &ft.AdminGetTriggersResponse{
		Triggers: triggers,
	}, nil
}

// AdminGetTriggerRewards returns a list of rewards based on the query directly from dynamo
func (c *AdminAPI) AdminGetTriggerRewards(ctx context.Context, req *ft.AdminGetTriggerRewardsRequest) (*ft.AdminGetTriggerRewardsResponse, error) {
	rawTrigger, err := c.DAOs.TriggerDAO.GetTriggerByID(req.TriggerId)

	if err == dynamoConst.ErrNotFound || rawTrigger == nil {
		return nil, twirp.InvalidArgumentError("trigger_id", "trigger does not exist")
	} else if err != nil {
		log.WithField("TriggerId", req.TriggerId).WithError(err).Error("AdminGetTriggerRewards: failed to get trigger")
		return nil, twirp.InternalError("failed to get trigger")
	}

	benefactorRewards := []*ft.RewardConfig{}
	for _, rawBenefactorReward := range rawTrigger.BenefactorRewardAttributes.Rewards {
		benefactorRewards = append(benefactorRewards, convertRawRewardToProtobufReward(rawBenefactorReward))
	}

	recipientRewards := []*ft.RewardConfig{}
	for _, rawRecipientReward := range rawTrigger.RecipientRewardAttributes.Rewards {
		recipientRewards = append(recipientRewards, convertRawRewardToProtobufReward(rawRecipientReward))
	}

	return &ft.AdminGetTriggerRewardsResponse{
		BenefactorRewards: benefactorRewards,
		RecipientRewards:  recipientRewards,
	}, nil
}

// AdminGetRewardsAuditForUser returns a list of rewards based on the user from dynamo
func (c *AdminAPI) AdminGetRewardsAuditForUser(ctx context.Context, req *ft.AdminGetRewardsAuditForUserRequest) (*ft.AdminGetRewardsAuditForUserResponse, error) {
	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("user_id", "user_id is required")
	}

	rawUserAudit, err := c.DAOs.RewardDAO.GetAllUserRewards(req.UserId)

	if err == dynamoConst.ErrNotFound || rawUserAudit == nil {
		return nil, twirp.InvalidArgumentError("user_id", "user_id does not exist")
	} else if err != nil {
		log.WithField("UserID", req.UserId).WithError(err).Error("AdminGetRewardsAuditForUser: failed to get user rewards")
		return nil, twirp.InternalError("failed to get user rewards")
	}

	userRewards := []*ft.UserReward{}
	for _, rawUserReward := range rawUserAudit {
		userRewards = append(userRewards, convertRawUserRewardInfoToProtobufUserRewardMetadata(rawUserReward))
	}

	return &ft.AdminGetRewardsAuditForUserResponse{
		UserRewards: userRewards,
	}, nil
}

// Conversion Functions
func convertRawCampaignToProtobufCampaign(rawCampaign *campaign.Campaign) *ft.CampaignConfig {
	cheerGroups := map[string]*ft.CheerGroupConfig{}

	for key, rawCheerGroup := range rawCampaign.CheerGroups {
		cheerGroup := ft.CheerGroupConfig{
			CheerCodes:           rawCheerGroup.CheerCodes,
			Name:                 rawCheerGroup.Name,
			ImageUrl:             rawCheerGroup.ImageURL,
			Division:             rawCheerGroup.Division,
			UserWhitelistEnabled: rawCheerGroup.UserWhitelistEnabled,
			UserWhitelist:        rawCheerGroup.UserWhitelist,
			Deprecated:           rawCheerGroup.Deprecated,
		}
		cheerGroups[key] = &cheerGroup
	}

	activeStartDate, _ := ptypes.TimestampProto(rawCampaign.ActiveStartDate)
	activeEndDate, _ := ptypes.TimestampProto(rawCampaign.ActiveEndDate)

	return &ft.CampaignConfig{
		Id:                   rawCampaign.ID,
		ActiveStartDate:      activeStartDate,
		ActiveEndDate:        activeEndDate,
		Description:          rawCampaign.Description,
		Title:                rawCampaign.Title,
		ChannelList:          rawCampaign.ChannelList,
		ExcludedChannelList:  rawCampaign.ExcludedChannelList,
		CheerGroups:          cheerGroups,
		StaffOnly:            rawCampaign.StaffOnly,
		UserWhitelistEnabled: rawCampaign.UserWhitelistEnabled,
		UserWhitelist:        rawCampaign.UserWhitelist,
		AllChannels:          rawCampaign.AllChannels,
		Domain:               rawCampaign.Domain,
	}
}

func convertRawObjectiveToProtobufObjective(rawObjective *campaign.Objective) *ft.ObjectiveConfig {
	randomRewards := []*ft.RewardConfig{}
	for _, reward := range rawObjective.RandomRewards {
		randomRewards = append(randomRewards, convertRawRewardToProtobufReward(reward))
	}

	return &ft.ObjectiveConfig{
		Id:                   rawObjective.ID,
		Description:          rawObjective.Description,
		EventHandler:         rawObjective.EventHandler,
		Title:                rawObjective.Title,
		Tag:                  rawObjective.Tag,
		RandomRewards:        randomRewards,
		ShouldAutogrant:      rawObjective.ShouldAutogrant,
		CheerGroupKey:        rawObjective.CheerGroupKey,
		CampaignId:           rawObjective.CampaignId,
		Domain:               rawObjective.Domain,
		UserWhitelistEnabled: rawObjective.UserWhitelistEnabled,
		UserWhitelist:        rawObjective.UserWhitelist,
		Deprecated:           rawObjective.Deprecated,
	}
}

func convertRawMilestoneToProtobufMilestone(rawMilestone *campaign.Milestone) *ft.MilestoneConfig {
	activeStartDate, _ := ptypes.TimestampProto(rawMilestone.ActiveStartDate)
	activeEndDate, _ := ptypes.TimestampProto(rawMilestone.ActiveEndDate)
	visibleStartDate, _ := ptypes.TimestampProto(rawMilestone.VisibleStartDate)
	visibleEndDate, _ := ptypes.TimestampProto(rawMilestone.VisibleEndDate)

	return &ft.MilestoneConfig{
		Id:                     rawMilestone.ID,
		Handler:                rawMilestone.Handler,
		HandlerMetadata:        convertRawHandlerMetadataToProtobufHandlerMetadata(&rawMilestone.HandlerMetadata),
		ActiveStartDate:        activeStartDate,
		ActiveEndDate:          activeEndDate,
		VisibleStartDate:       visibleStartDate,
		VisibleEndDate:         visibleEndDate,
		Threshold:              rawMilestone.Threshold,
		ParticipationThreshold: rawMilestone.ParticipationThreshold,
		UserWhiteListEnabled:   rawMilestone.UserWhiteListEnabled,
		UserWhitelist:          rawMilestone.UserWhitelist,
		AutograntDisabled:      rawMilestone.AutograntDisabled,
		CheckEligibility:       rawMilestone.CheckEligibility,
		CampaignId:             rawMilestone.CampaignId,
		IsPrimeOnly:            rawMilestone.IsPrimeOnly,
		Domain:                 rawMilestone.Domain,
		ObjectiveId:            rawMilestone.ObjectiveID,
	}
}

func convertRawTriggerToProtobufTrigger(rawTrigger *campaign.Trigger) *ft.TriggerConfig {
	activeStartDate, _ := ptypes.TimestampProto(rawTrigger.ActiveStartDate)
	activeEndDate, _ := ptypes.TimestampProto(rawTrigger.ActiveEndDate)
	visibleStartDate, _ := ptypes.TimestampProto(rawTrigger.VisibleStartDate)
	visibleEndDate, _ := ptypes.TimestampProto(rawTrigger.VisibleEndDate)

	return &ft.TriggerConfig{
		Id:                         rawTrigger.ID,
		Title:                      rawTrigger.Title,
		Description:                rawTrigger.Description,
		ActiveStartDate:            activeStartDate,
		ActiveEndDate:              activeEndDate,
		VisibleStartDate:           visibleStartDate,
		VisibleEndDate:             visibleEndDate,
		TriggerAmountMin:           int64(rawTrigger.TriggerAmountMin),
		TriggerAmountMax:           int64(rawTrigger.TriggerAmountMax),
		TriggerType:                string(rawTrigger.TriggerType),
		BenefactorRewardAttributes: convertRawTriggerRewardAttributesToProtobufTriggerRewardAttributes(&rawTrigger.BenefactorRewardAttributes),
		RecipientRewardAttributes:  convertRawTriggerRewardAttributesToProtobufTriggerRewardAttributes(&rawTrigger.RecipientRewardAttributes),
		RecipientSelectionFormula:  rawTrigger.RecipientSelectionFormula,
	}
}

func convertRawRewardToProtobufReward(rawReward *campaign.Reward) *ft.RewardConfig {
	rewardMetadata := ft.RewardMetadataConfig(rawReward.Metadata)

	return &ft.RewardConfig{
		Id:                  rawReward.ID,
		Code:                rawReward.Code,
		Type:                rawReward.Type,
		Name:                rawReward.Name,
		Description:         rawReward.Description,
		ImageUrl:            rawReward.ImageURL,
		FulfillmentStrategy: rawReward.FulfillmentStrategy,
		Rarity:              rawReward.Rarity,
		RewardMetadata:      &rewardMetadata,
		Sku:                 rawReward.SKU,
		VendorType:          rawReward.VendorType,
		Quantity:            rawReward.Quantity,
	}
}

func convertRawHandlerMetadataToProtobufHandlerMetadata(rawHandlerMetadata *campaign.HandlerMetadata) *ft.HandlerMetadataConfig {
	rewards := []*ft.RewardConfig{}
	for _, reward := range rawHandlerMetadata.Rewards {
		rewards = append(rewards, convertRawRewardToProtobufReward(reward))
	}
	return &ft.HandlerMetadataConfig{
		Rewards:           rewards,
		SelectionStrategy: rawHandlerMetadata.SelectionStrategy,
		MinCheerAmount:    int64(rawHandlerMetadata.MinCheerAmount),
		MaxCheerAmount:    int64(rawHandlerMetadata.MaxCheerAmount),
		SelectionIndex:    int64(rawHandlerMetadata.SelectionIndex),
	}
}

func convertRawTriggerRewardAttributesToProtobufTriggerRewardAttributes(rawTriggerRewardAttributes *campaign.RewardAttributes) *ft.TriggerConfigRewardAttributes {
	rewards := []*ft.RewardConfig{}
	for _, reward := range rawTriggerRewardAttributes.Rewards {
		rewards = append(rewards, convertRawRewardToProtobufReward(reward))
	}
	return &ft.TriggerConfigRewardAttributes{
		Rewards:           rewards,
		SelectionStrategy: rawTriggerRewardAttributes.SelectionStrategy,
		SelectionAmount:   int64(rawTriggerRewardAttributes.SelectionAmount),
	}
}

func convertRawUserRewardInfoToProtobufUserRewardMetadata(rawReward *dynamo.Reward) *ft.UserReward {
	var createdAt *timestamp.Timestamp
	if !rawReward.CreatedAt.IsZero() {
		createdAt, _ = ptypes.TimestampProto(rawReward.CreatedAt)
	}
	return &ft.UserReward{
		UserId:              rawReward.UserID,
		ScopedRewardId:      rawReward.ScopedRewardID,
		Domain:              rawReward.Domain,
		ExternalUserId:      rawReward.ExternalUserID,
		FulfillmentStatus:   rawReward.FulfillmentStatus,
		FulfillmentStrategy: rawReward.FulfillmentStrategy,
		MilestoneId:         rawReward.MilestoneID,
		MilestoneType:       rawReward.MilestoneType,
		RewardId:            rawReward.RewardID,
		RewardType:          rawReward.RewardType,
		Quantity:            rawReward.Quantity,
		TriggerId:           rawReward.TriggerID,
		CreatedAt:           createdAt,
	}
}
