package api

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/mocks"
	ft "code.justin.tv/commerce/fortuna/twirp"

	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func TestAdminAPI(t *testing.T) {
	Convey("Test API", t, func() {
		dummyConfig := &config.Configuration{}

		mockFulfiller := new(mocks.IFulfiller)
		mockUtils := new(mocks.IUtils)

		clients := clients.Clients{}

		mockCampaignDAO := new(mocks.ICampaignDAO)
		mockObjectiveDAO := new(mocks.IObjectiveDAO)
		mockMilestoneDAO := new(mocks.IMilestoneDAO)
		mockTriggerDAO := new(mocks.ITriggerDAO)
		mockRewardDAO := new(mocks.IRewardDAO)
		daos := &dynamo.DAOs{
			CampaignDAO:  mockCampaignDAO,
			ObjectiveDAO: mockObjectiveDAO,
			MilestoneDAO: mockMilestoneDAO,
			TriggerDAO:   mockTriggerDAO,
			RewardDAO:    mockRewardDAO,
		}

		api := &AdminAPI{
			Config:    dummyConfig,
			Clients:   &clients,
			DAOs:      daos,
			Fulfiller: mockFulfiller,
			Utils:     mockUtils,
		}

		Convey("Test AdminInvalidateCampaignCache", func() {
			req := &ft.AdminInvalidateCampaignCacheRequest{
				Domain: "domain",
			}

			Convey("with invalidation success", func() {
				mockUtils.On("InvalidateCampaignCacheForDomain", Anything).Return(true, nil)
				mockUtils.On("InvalidateAllChannelsCampaignsCache").Return(true, nil)
				res, err := api.AdminInvalidateCampaignCache(context.Background(), req)
				So(err, ShouldBeNil)
				So(res.Invalidated, ShouldBeTrue)
				So(res.AllChannelsInvalidated, ShouldBeTrue)
			})

			Convey("with error", func() {
				mockUtils.On("InvalidateCampaignCacheForDomain", Anything).Return(false, errors.New("error bro"))
				res, err := api.AdminInvalidateCampaignCache(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetCampaigns", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)

			Convey("with campaigns in dynamo and domain in req", func() {
				mockCampaignDAO.On("GetCampaignsByDomain", Anything).Return(campaignConfig.Campaigns[TestDomain], nil)
				req := ft.AdminGetCampaignsRequest{
					Domain: TestDomain,
				}
				res, err := api.AdminGetCampaigns(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Campaigns[0].Id, ShouldEqual, TestDomain)
			})

			Convey("with campaigns in dynamo and channel id in req", func() {
				mockUtils.On("GetCampaignConfigForChannelID", Anything).Return(campaignConfig, nil)
				expectedCampaigns := []*campaign.Campaign{}
				for _, campaigns := range campaignConfig.Campaigns {
					expectedCampaigns = append(expectedCampaigns, campaigns...)
				}

				req := ft.AdminGetCampaignsRequest{
					ChannelId: "1",
				}
				res, err := api.AdminGetCampaigns(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(len(res.Campaigns), ShouldEqual, len(expectedCampaigns))
			})

			Convey("with error", func() {
				mockCampaignDAO.On("GetCampaignsByDomain", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetCampaignsRequest{
					Domain: TestDomain,
				}
				res, err := api.AdminGetCampaigns(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetObjectves", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			objectives := campaignConfig.Campaigns[TestDomain][0].Objectives

			Convey("with objectives in dynamo", func() {
				mockObjectiveDAO.On("GetObjectivesByCampaignID", Anything).Return(objectives, nil)
				req := ft.AdminGetObjectivesRequest{
					CampaignId: "c1",
				}
				res, err := api.AdminGetObjectives(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Objectives[0].Id, ShouldEqual, objectives[0].ID)
			})

			Convey("with error", func() {
				mockObjectiveDAO.On("GetObjectivesByCampaignID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetObjectivesRequest{
					CampaignId: "c1",
				}
				res, err := api.AdminGetObjectives(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetMilestone", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			milestone := campaignConfig.Campaigns[TestDomain][0].Objectives[0].Milestones[0]

			Convey("with milestone in dynamo", func() {
				mockMilestoneDAO.On("GetMilestoneByID", Anything).Return(milestone, nil)
				req := ft.AdminGetMilestoneRequest{
					MilestoneId: TestMilestoneID1,
				}
				res, err := api.AdminGetMilestone(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Milestone.Id, ShouldEqual, milestone.ID)
			})

			Convey("with error", func() {
				mockMilestoneDAO.On("GetMilestoneByID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetMilestoneRequest{
					MilestoneId: TestMilestoneID1,
				}
				res, err := api.AdminGetMilestone(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetMilestones", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			milestones := campaignConfig.Campaigns[TestDomain][0].Objectives[0].Milestones

			Convey("with milestones in dynamo", func() {
				mockMilestoneDAO.On("GetMilestonesByObjectiveID", Anything).Return(milestones, nil)
				req := ft.AdminGetMilestonesRequest{
					ObjectiveId: "o1",
				}
				res, err := api.AdminGetMilestones(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Milestones[0].Id, ShouldEqual, milestones[0].ID)
			})

			Convey("with error", func() {
				mockMilestoneDAO.On("GetMilestonesByObjectiveID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetMilestonesRequest{
					ObjectiveId: "o1",
				}
				res, err := api.AdminGetMilestones(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetMilestoneRewards", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			milestone := campaignConfig.Campaigns[TestDomain][0].Objectives[0].Milestones[0]

			Convey("with milestones in dynamo", func() {
				mockMilestoneDAO.On("GetMilestoneByID", Anything).Return(milestone, nil)
				req := ft.AdminGetMilestoneRewardsRequest{
					MilestoneId: "m1",
				}
				res, err := api.AdminGetMilestoneRewards(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Rewards[0].Id, ShouldEqual, milestone.HandlerMetadata.Rewards[0].ID)
			})

			Convey("with non existent milestone", func() {
				mockMilestoneDAO.On("GetMilestoneByID", Anything).Return(nil, nil)
				req := ft.AdminGetMilestoneRewardsRequest{
					MilestoneId: "derpderp",
				}
				res, err := api.AdminGetMilestoneRewards(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with error", func() {
				mockMilestoneDAO.On("GetMilestoneByID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetMilestoneRewardsRequest{
					MilestoneId: "m1",
				}
				res, err := api.AdminGetMilestoneRewards(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetTriggerRewards", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			trigger := campaignConfig.Campaigns[TestDomain][0].Triggers[0]

			Convey("with trigger in dynamo", func() {
				mockTriggerDAO.On("GetTriggerByID", Anything).Return(trigger, nil)
				req := ft.AdminGetTriggerRewardsRequest{
					TriggerId: TestTriggerID1,
				}
				res, err := api.AdminGetTriggerRewards(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.BenefactorRewards[0].Id, ShouldEqual, trigger.BenefactorRewardAttributes.Rewards[0].ID)
				So(res.RecipientRewards[0].Id, ShouldEqual, trigger.RecipientRewardAttributes.Rewards[0].ID)
			})

			Convey("with non existent trigger", func() {
				mockTriggerDAO.On("GetTriggerByID", Anything).Return(nil, nil)
				req := ft.AdminGetTriggerRewardsRequest{
					TriggerId: "derpderp",
				}
				res, err := api.AdminGetTriggerRewards(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with error", func() {
				mockTriggerDAO.On("GetTriggerByID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetTriggerRewardsRequest{
					TriggerId: TestTriggerID1,
				}
				res, err := api.AdminGetTriggerRewards(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetTrigger", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			trigger := campaignConfig.Campaigns[TestDomain][0].Triggers[0]

			Convey("with trigger in dynamo", func() {
				mockTriggerDAO.On("GetTriggerByID", Anything).Return(trigger, nil)
				req := ft.AdminGetTriggerRequest{
					TriggerId: TestTriggerID1,
				}
				res, err := api.AdminGetTrigger(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Trigger.Id, ShouldEqual, trigger.ID)
			})

			Convey("with non existent trigger", func() {
				mockTriggerDAO.On("GetTriggerByID", Anything).Return(nil, nil)
				req := ft.AdminGetTriggerRequest{
					TriggerId: "derpderp",
				}
				res, err := api.AdminGetTrigger(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with error", func() {
				mockTriggerDAO.On("GetTriggerByID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetTriggerRequest{
					TriggerId: TestTriggerID1,
				}
				res, err := api.AdminGetTrigger(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetTriggers", func() {
			campaignConfig, campaignErr := InitTestCampaignConfig(DefaultCampaign)
			So(campaignErr, ShouldBeNil)
			triggers := campaignConfig.Campaigns[TestDomain][0].Triggers

			Convey("with trigger in dynamo", func() {
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return(triggers, nil)
				req := ft.AdminGetTriggersRequest{
					CampaignId: TestDomain,
				}
				res, err := api.AdminGetTriggers(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Triggers[0].Id, ShouldEqual, triggers[0].ID)
			})

			Convey("with non existent trigger", func() {
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return([]*campaign.Trigger{}, nil)
				req := ft.AdminGetTriggersRequest{
					CampaignId: "derpderp",
				}
				res, err := api.AdminGetTriggers(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(len(res.Triggers), ShouldEqual, 0)
			})

			Convey("with error", func() {
				mockTriggerDAO.On("GetTriggersByCampaignID", Anything).Return(nil, errors.New("error bro"))
				req := ft.AdminGetTriggersRequest{
					CampaignId: TestDomain,
				}
				res, err := api.AdminGetTriggers(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test AdminGetRewardsAuditForUser", func() {
			nowTime := time.Now().UTC()
			results := []*dynamo.Reward{
				{
					RewardID:  "r1",
					CreatedAt: nowTime,
				},
				{
					RewardID: "r2",
				},
			}

			Convey("with user rewards in dynamo", func() {
				mockRewardDAO.On("GetAllUserRewards", Anything).Return(results, nil)
				req := ft.AdminGetRewardsAuditForUserRequest{
					UserId: "testuser",
				}
				res, err := api.AdminGetRewardsAuditForUser(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(len(res.UserRewards), ShouldEqual, 2)
				So(res.UserRewards[0].RewardId, ShouldEqual, results[0].RewardID)
				So(res.UserRewards[1].RewardId, ShouldEqual, results[1].RewardID)
				expectedCreatedAt, _ := ptypes.TimestampProto(nowTime)
				So(res.UserRewards[0].CreatedAt, ShouldResemble, expectedCreatedAt)
				So(res.UserRewards[1].CreatedAt, ShouldBeNil)
			})

			Convey("with non existent reward", func() {
				mockRewardDAO.On("GetAllUserRewards", Anything).Return([]*dynamo.Reward{}, nil)
				req := ft.AdminGetRewardsAuditForUserRequest{
					UserId: "testuser",
				}
				res, err := api.AdminGetRewardsAuditForUser(context.Background(), &req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(len(res.UserRewards), ShouldEqual, 0)
			})

			Convey("with no userid", func() {
				mockRewardDAO.On("GetAllUserRewards", Anything).Return(nil, nil)
				req := ft.AdminGetRewardsAuditForUserRequest{
					UserId: "",
				}
				res, err := api.AdminGetRewardsAuditForUser(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with error", func() {
				mockRewardDAO.On("GetAllUserRewards", Anything).Return(nil, errors.New("oopsie"))
				req := ft.AdminGetRewardsAuditForUserRequest{
					UserId: "testuser",
				}
				res, err := api.AdminGetRewardsAuditForUser(context.Background(), &req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})
	})
}
