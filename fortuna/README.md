# fortuna
[![codecov](https://codecov.xarth.tv/ghe/commerce/fortuna/branch/master/graph/badge.svg)](https://codecov.xarth.tv/ghe/commerce/fortuna)

| Environment   | Endpoint                                   |
| ------------- |--------------------------------------------|
| Staging       | https://main.us-west-2.beta.fortuna.twitch.a2z.com |
| Production    | https://main.us-west-2.prod.fortuna.twitch.a2z.com |

## Setting up your workspace

### Go
 - Install Go:

```
$ brew install go
```

 - Create a Go workspace folder in your home directory. Ex:

```
$ mkdir /Users/<username>/GoWorkspace
```

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

```
$ export GOPATH=/Users/<username>/GoWorkspace
```

 - Append GOPATH/bin to PATH environment variable

```
$ export PATH=$PATH:$GOPATH/bin
```

 - Clone down Fortuna source within the new commerce folder. Make sure you added your ssh key to Github (https://git.xarth.tv/settings/keys)

```
$ git clone git@git.xarth.tv:commerce/fortuna.git $GOPATH/src/code.justin.tv/commerce/fortuna
```

 - Directory structure will now look like this:

```
$ /Users/<username>/GoWorkspace/src/code.justin.tv/commerce/fortuna
```

### Retool
This project uses [Retool](https://github.com/twitchtv/retool) to ensure collaborators use the same versions of development tools.

Note that running `make setup` will install Retool for you.

 - Install Retool:

 ```
 go get -u github.com/twitchtv/retool
 ```

  - Use Retool to ensure your installed tools match those specified in `tools.json`:

```
retool sync
```

 - Execute a command with a versioned tool:

 ```
retool do dep ensure
 ```

 ### AWS
Running Fortuna locally requires an IAM user with access credentials on the appropriate AWS account. [Ensure you have the AWS CLI installed](https://docs.aws.amazon.com/cli/latest/userguide/installing.html), then set up your IAM user and credentials:

1. In [Isengard](https://isengard.amazon.com), access the Admin console for the Fortuna account you need credentials for (either `twitch-fortuna-aws` for production or `twitch-fortuna-dev` for development).
2. Navigate to [IAM](https://console.aws.amazon.com/iam/home) and visit the Users tab on the left.
3. Click "Add user" in the top left. Enter a username and check "Programmatic access" as the access type. Go to the next step.
4. Attach "AdministratorAccess" permission policy. Complete the creation flow.
5. At the final step, you should be shown the new user's `AWS_ACCESS_KEY` and `AWS_SECRET_ACCESS_KEY`. You'll use these to create your local AWS profile. Note that after this point you cannot view the secret access key again; if you lose the secret access key, you'll have to create new credentials.
6. In your terminal, run `aws configure --profile twitch-fortuna-dev` (or `--profile twitch-fortuna-aws` as appropriate). Enter the access key and secret access key you just created when the CLI prompts you.

## Developing in Fortuna
### Make sure you are running go1.10.7 or above

### Running the service locally
- To run the service locally:

```
$ make dev
```

**Note**: Make sure you have the right AWS credentials profile (**twitch-fortuna-dev**) before running this command. These will be the stored in the `~/.aws/credentials` file. Your twitch-fortuna-dev credentials should either be default or you can specify the credentials as such:

```
$ AWS_PROFILE=twitch-fortuna-dev go run cmd/fortuna/main.go
```

- To perform all the validation and static analysis prior to PR:

```
$ make release
```

### Testing
#### Unit Testing
```
$ make test
```
**Note**: Some of our unit tests rely on a local DynamoDB instance. In order for your tests to run correctly, you must have a default AWS credentials profile (`~/.aws/credentials`). The default profile does not have to have a real access/secret key.

#### Integration Testing
```
$ make test_integration
```
**Note**: Currently, integration tests only run manually on your local machine. You must be on the Twitch VPN to run them.

### Updating Dependencies
Fortuna's dependencies are managed with [`dep`](https://github.com/golang/dep).

#### Installing Dependencies
Run `make dep` to ensure `vendor/` is in the correct state for your configuration.

```
$ make dep
```

**Note**: This process may be slow for the first time if dependencies are cloned or if it hasn't been run in awhile with many changes to fetch.

#### Adding Dependencies
Run `dep ensure -add` to update your configuration with new dependencies

```
$ dep ensure -add github.com/foo/bar github.com/foo/baz
```

`dep` will update `Gopkg.toml`, `Gopkg.lock`, and your `vendor/` files to the latest version unless a version constraint is specified, IE:

```
$ dep ensure -add github.com/foo/bar@1.0.0
```

**Note**: If you do not use `-add`, this package will not get saved in the `gopkg.toml` file

#### Checking Dependencies' Status
You can check if there is a mismatch between the configuration and the state of your project by checking the status.

```
$ dep status
```

#### Removing Dependencies
1. Remove any `import` package statements and usage from the code
2. Remove relevant `[[constraint]]` rules from `Gopkg.toml`
3. Run `dep ensure`

#### Further Reading
- [dep Github](https://github.com/golang/dep)
- [dep FAQ](https://github.com/golang/dep/blob/master/docs/FAQ.md)

### Mocks
- Use [Testify](https://github.com/stretchr/testify) to mock out behaviour in tests
- Use [Mockery](https://github.com/vektra/mockery) to autogenerate mock classes

Example mockery generation:

```
$ mockery -dir=clients/sns -name=ISNSClient
```

### Campaign Configuration

See [campaign-config.md](./campaign/campaign-config.md)

### Step Functions

Fortuna integrates with [AWS Step Functions](https://aws.amazon.com/step-functions/) to implement cheerbomb gift fulfillment,
and potentially other long-running or complicated workflows in the future. State machine definitions live in the `terraform/modules/sfn` directory,
and any changes to our state machines should be made and committed there.

See `lambdas` for an example of how to implement the actual business logic for a lambda modeled within a state machine.

### Updating Step Functions

⚠️ __WARNING__: Try not to update a state machine during a running MegaCommerce campaign. If unavoidable, follow the steps below.

If new logic needs to be added to an existing state machine that requires the creation of a new state, then a new version of the state machine will be needed to be created to avoid backwards compatibility issues. To do this:
	1. Create an updated version of the existing state machine in terraform. The new version of the state machine should include new resources for the new state machine itself, along with a resource for the new state. The state machine definition should also be updated to include the execution of the new state. The new state machine terraform file does not need to include resources for already defined states as they already exist and are defined in the terraform file for the previous state machine. See terraform/modules/sfn/cheerbomb.tf vs terraform/modules/sfn/cheerbomb-v2.tf as an example.
	2. Deploy the terraform changes for the new state machine so it is in place ahead of the Fortuna deployment.
	3. Create a new CloudWatch alarm that monitors the new state machine ahead of the Fortuna deployment.
	4. Update Fortuna to execute the new state machine and bind the new state activity to a handler.
		* Update config/config.go to be aware of the new state.
		* Update the config for each environment in config/ with the ARN of the new state machine begin executing and the new state.
		* Update the manager for the state machine under stepfn_activities/ to bind the newly added state to a handler.  
	5. Deploy Fortuna 

### Terraform Workflow

⚠️ __WARNING__: Running Terraform can be extremely dangerous. It is trivially easy to destroy all the AWS resources in an account, bringing down all the service environments. If you don't know what you're doing, seek help. ⚠️

#### Making updates

1. Make your changes.
    1. Cut a fresh branch from `master`. Isolating Terraform updates from code changes is helpful during the review-and-merge process.
    1. Go to whichever module you need to change (under `terraform/modules`), or create a new one if required.
    1. Edit the resource declarations until you believe they describe the infrastructure you need.
    1. If you created a new module, you'll also need to consume it in `terraform/modules/fortuna.tf`, which describes the main module that each of our environments uses.
    1. Run `terraform plan` to doublecheck the output of your changes as necessary.
    1. Commit. **You should not have applied your changes yet**. If running `terraform plan` made changes to any of your `.tfstate` files, leave those out of this commit.
1. Submit your changes for review.
    1. Open a PR for your branch containing the updated declarations.
    1. In the PR description (or as a comment), paste the output of running `terraform plan` for one of the environments (`terraform/{development,staging,production}`).
    1. Get a member of our team to review your changes and your plan output.
    1. Once your PR is marked "Approved", **don't merge**. You'll apply your changes first.
1. Apply your changes to each environment.
    1. For each environment in `fortuna/terraform/{development,staging,production}`:
        1. Go to the corresponding directory.
        1. Run `terraform get` to pull in any updated modules.
        1. Run `terraform apply`. You will be shown the plan output and asked for approval before the command will actually run. **Read through this carefully to make sure it's about to do what you want.**
        1. After the command exits, Terraform will have pushed updated state files to the remote backend.
    1. If there were any unexpected outputs or errors from the application step, carefully decide whether they require more changes to your Terraform configuration or if they can be ignored. Consult with the Rewards team if you aren't sure.
1. Merge your PR. You're done.
    1. Optional but recommended: verify via AWS console (or other appropriate means) that the resource changes applied as intended.

#### Remote State

We use S3 to store our `.tfstate` files, which helps ensure that all developers work from a synchronized view of our infrastructure
when making Terraform changes. Details of where remote state is stored can be found in each environment's `backend.tf` file.

**It's vitally important that the buckets containing the `.tfstate` files (`twitch-fortuna-dev` for development/staging, `twitch-fortuna-aws` for production)
not be deleted or modified by hand.** Versioning and logging is enabled for these buckets, but it's best not to need to rely on backups.

**Likewise, the `terraform-locks` DynamoDB tables (one each for the `twitch-fortuna-dev` and `twitch-fortuna-aws` accounts) should not be deleted.**

##### Migrating to remote state

1. (Optional) Make backup copies of your local `.tfstate` files, just in case. 
1. Run `terraform init` in each of our environment configuration folders (`terraform/{development,staging,production}`). Terraform should prompt you about migrating to a remote backend.
1. After this completes, you're ready to work with Terraform as normal. You should delete your local `.tfstate` files once the migration is finished.

### Sample Requests

- To send a cheering event, `cmd/_scipt/send_cheering_event`
- To send a sub gift event, `cmd/_scipt/send_subs_event`

### Sandstorm Secret Access
Running Fortuna locally may require your IAM user on the appropriate AWS account to have appropriate access to [Sandstorm](https://dashboard.internal.justin.tv/sandstorm). Here's how to add your user ARN to a Sandstorm role:

1. Find the appropriate AWS ARN for the IAM user who needs access. Use Isengard to access the admin console for the appropriate Fortuna AWS account, then click on the username in the "Users" tab. Look for the "User ARN" value.
2. Find the appropriate role in Sandstorm that you need to add the user to. Go to [Manage Roles](https://dashboard.internal.justin.tv/sandstorm/manage-roles) and search for `fortuna-prod`, `fortuna-staging` or `fortuna-dev` as needed. Click the role in the list to open the edit modal.
3. Paste the IAM user's ARN on a new line at the bottom of the "Allowed ARNs" textfield.
4. Save the edited role.

## Deployment

### Development
Clean Deploy is used for Dev and Staging and Production environments. This is useful if you need to test changes on a development branch before merging changes into master.

[Clean Deploy](https://deploy.xarth.tv/#/commerce/fortuna)

### Rolling back
In the case of a failed or bad deployment, you can rollback to a previous application version using Elastic Beanstalk.

[Elastic Beanstalk Console](https://isengard.amazon.com/federate?account=180265471281&role=pipeline-approver&destination=/elasticbeanstalk/home%3Fregion%3Dus-west-2%23)

More info can be found in our [Runbook](https://docs.google.com/document/d/1JF_WwJWMYJLmh1FWiQFhrq5bE_nZfCmDi-GEJ7zB6vM/edit#).
