package main

import (
	"math/rand"
	"net/http"
	"os"
	"time"

	"code.justin.tv/commerce/fortuna/api"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/s2s"
	"code.justin.tv/commerce/fortuna/s3"
	"code.justin.tv/commerce/fortuna/sqs"
	"code.justin.tv/commerce/fortuna/sqs/eventbus"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/sqs/workers"
	"code.justin.tv/commerce/fortuna/utils/setup"
	"code.justin.tv/foundation/twitchserver"
	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	"code.justin.tv/video/roll"
	"code.justin.tv/video/rollrus"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
)

const (
	shutdownTimeout = 5 * time.Second
	devEnvironment  = "dev"
	prodEnvironment = "prod"
)

func main() {
	environment := config.GetEnvironment()
	statsEnvironment := os.Getenv("STATS_ENVIRONMENT")
	if statsEnvironment == "" {
		statsEnvironment = environment
	}

	logLevel := log.DebugLevel
	if statsEnvironment == prodEnvironment {
		logLevel = log.InfoLevel
	}
	log.SetLevel(logLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

	// Initialize RNG seed
	rand.Seed(time.Now().UTC().UnixNano())

	fortunaConfig, err := setup.SetupFortunaConfiguration()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize fortuna config")
	}
	fortunaConfig.StatsEnvironment = statsEnvironment

	setupRollbar(fortunaConfig)
	s3Client := s3.NewFromDefaultConfig()

	campaignPoller, err := setup.SetupCampaignPoller(fortunaConfig, s3Client)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize campaign config")
	}

	setup.SetupAutoprof(fortunaConfig)

	stats, err := setup.SetupStats(fortunaConfig)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	clients, err := clients.SetupClients(fortunaConfig, campaignPoller.CampaignConfig, stats, s3Client)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize clients")
	}

	daos, err := dynamo.SetupDAOs(fortunaConfig)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize DAOs")
	}

	selector := rewards.NewSelector(campaignPoller.CampaignConfig)
	utils := &rewards.Utils{
		CampaignConfig: campaignPoller.CampaignConfig,
		CampaignHelper: daos,
		Config:         fortunaConfig,
		Clients:        clients,
		DAOs:           daos,
	}

	fulfiller := &rewards.Fulfiller{
		Config:          fortunaConfig,
		CampaignConfig:  campaignPoller.CampaignConfig,
		Clients:         clients,
		RewardsSelector: selector,
		DAOs:            daos,
		Utils:           utils,
		Stats:           stats,
	}

	notifier := &rewards.Notifier{
		Clients: clients,
	}

	handlerInput := handler.NewEventHandlersInput{
		Clients:        clients,
		FortunaConfig:  fortunaConfig,
		CampaignConfig: campaignPoller.CampaignConfig,
		Stats:          stats,
		Fulfiller:      fulfiller,
		Utils:          utils,
		Notifier:       notifier,
	}

	triggerHandlers := handler.NewTriggerHandlers(handlerInput)

	options := &setup.FortunaSetupOptions{
		FortunaConfig:   fortunaConfig,
		CampaignConfig:  campaignPoller.CampaignConfig,
		Stats:           stats,
		Clients:         clients,
		Daos:            daos,
		TriggerHandlers: triggerHandlers,
		Fulfiller:       fulfiller,
		RewardsSelector: selector,
		Utils:           utils,
		Notifier:        notifier,
	}

	workerManagers, eventbusMegaCheerManager, eventbus2FAManager := setupSQSWorkers(options, clients)

	if err := eventbusMegaCheerManager.Listen(); err != nil {
		log.WithError(err).Fatal("Error starting up eventbus megacheer manager")
	}

	if err := eventbus2FAManager.Listen(); err != nil {
		log.WithError(err).Fatal("Error starting up eventbus 2FA reward manager")
	}

	serverErr := startWebServer(options)

	// Stop polling from SQS, stop the workers from pulling new messages, and wait for them to drain.
	log.Info("Shutting down SQS workers...")
	for _, manager := range workerManagers {
		if err = manager.Shutdown(shutdownTimeout); err != nil {
			log.WithError(err).Errorf("Error shutting SQSWorkerManager, Error: %v", manager)
		}
	}

	log.Info("Shutting down eventbus megacheer SQS subscribers")
	eventbusMegaCheerManager.Shutdown()

	log.Info("Shutting down eventbus 2fa reward SQS subscribers")
	eventbus2FAManager.Shutdown()

	log.Info("Shutting down campaign poller...")
	campaignPoller.Shutdown()

	if serverErr != nil {
		log.WithError(serverErr).Fatal("Error during server shutdown")
	}

	log.Info("Shutdown complete.")
}

func setupRollbar(fortunaConfig *config.Configuration) {
	if fortunaConfig.StatsEnvironment == devEnvironment {
		return
	}
	// Load environment variables created by codebuild
	if err := godotenv.Load(); err != nil {
		log.WithError(err).Info("Not loading .env file")
	}
	codeVersion := os.Getenv("CODEBUILD_RESOLVED_SOURCE_VERSION")
	host, err := os.Hostname()
	if err != nil {
		log.WithError(err).Info("Could not retrieve hostname")
	}

	// Setup rollbar hook
	rollbarClient := roll.New(fortunaConfig.RollbarToken, fortunaConfig.StatsEnvironment)
	rollbarClient.SetVersion(codeVersion)
	rollbarClient.SetHost(host)
	log.AddHook(rollrus.NewHook(rollbarClient, log.WarnLevel))
}

// Set up all SQS workers. They will automatically start polling and processing their respective queues. Returns
// a list of managers for the workers
func setupSQSWorkers(options *setup.FortunaSetupOptions, clients *clients.Clients) ([]*sqs.WorkerManager, *eventbus.Manager, *eventbus.Manager) {
	cfg := options.FortunaConfig

	sqsClient := options.Clients.SQSClient

	var workerManagers []*sqs.WorkerManager

	delegator := sqs.NewDelegator(options.Clients, cfg, options.Stats, options.TriggerHandlers, options.Utils)

	megaCheerGiftFulfillmentWorker := &workers.MegaCheerGiftFulfillmentEventsWorker{
		Clients:   options.Clients,
		Fulfiller: options.Fulfiller,
		Notifier:  options.Notifier,
		Utils:     options.Utils,
		Stats:     options.Stats,
	}
	workerManagers = append(workerManagers, sqs.NewWorkerManager(cfg.MegaCheerGiftFulfillmentSQSEventQueueName, cfg.MegaCheerGiftFulfillmentSQSWorkerCount, sqsClient, megaCheerGiftFulfillmentWorker, options.Stats))

	dartReciever := receiver.NewReceiverProtobufClient(cfg.DartEndpointURL, http.DefaultClient)
	backfillWorker := &workers.TwoFactorBackfillWorker{TwoFactor: eventbus.NewTwoFactorWorker(options.Fulfiller, clients.SpadeClient, options.Daos.RewardDAO, dartReciever)}

	workerManagers = append(workerManagers, sqs.NewWorkerManager(cfg.TwoFactorBackfillQueueName, 10, sqsClient, backfillWorker, options.Stats))

	eventbusManager := eventbus.NewManager(cfg, options.Stats, &delegator)
	eventbus2FAManager := eventbus.New2FAManager(cfg, options.Fulfiller, clients.SpadeClient, options.Daos.RewardDAO, dartReciever)

	return workerManagers, &eventbusManager, &eventbus2FAManager
}

// Set up the web server start listening for requests. The main thread will not return out of this method until
// the web server is shut down via a termination signal.
func startWebServer(options *setup.FortunaSetupOptions) error {
	adminAPI := api.AdminAPI{
		Clients:   options.Clients,
		Config:    options.FortunaConfig,
		DAOs:      options.Daos,
		Fulfiller: options.Fulfiller,
		Utils:     options.Utils,
	}

	fortunaServer := api.FortunaServer{
		AdminAPI: adminAPI,
	}

	s2sClient, err := s2s.Init(options.FortunaConfig)
	if err != nil {
		log.Fatal(err)
	}

	server, err := fortunaServer.NewServer(options.Stats, s2sClient)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Starting %s server", options.FortunaConfig.StatsEnvironment)

	serverConfig := twitchserver.NewConfig()
	serverConfig.Statter = options.Stats
	twitchserver.AddDefaultSignalHandlers()
	return twitchserver.ListenAndServe(server, serverConfig)
}
