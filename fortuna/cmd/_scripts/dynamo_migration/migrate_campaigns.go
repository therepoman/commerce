package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	gdb "github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	organizationName = "commerce"
	serviceName      = "fortuna"

	localCampaignConfigPath = "%s/src/code.justin.tv/%s/%s/campaign/%s.json"
	localConfigPath         = "%s/src/code.justin.tv/%s/%s/config/%s.json"
)

var environments = map[string]bool{
	"dev":     true,
	"staging": true,
	"prod":    true,
}

var responses = map[string]bool{
	"y": true,
	"n": true,
}

// Need compiled config first: ./upload_campaign -env=dev -dry
// Usage: go run ./cmd/_scripts/dynamo_migration/migrate_campaigns.go -env=dev -domain=owl2018 -dry
func main() {
	env := flag.String("env", "", "Environment you want to compile and upload campaign configuration for (Required)")
	dryrun := flag.Bool("dry", false, "Setting this flag will not upload anything to dynamo, but it will run the verification (Optional)")
	domainOption := flag.String("domain", "", "Domain you want to upload. if not specified, it will upload all domains (Optional)")
	flag.Parse()

	environment := *env
	verifyOnly := *dryrun
	selectedDomain := *domainOption

	if *env == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	if !environments[*env] {
		fmt.Println("Invalid environment. Available environments: dev, staging, prod")
		os.Exit(1)
	}

	fmt.Printf("Checking configuration file for environment: %s \n", environment)

	// Load json file from the env directory
	path, err := checkCampaignFile(environment)
	if err != nil {
		fmt.Printf("Error loading the campaign file. Please use the \"upload_campaign -env=env -dry\" command to compile the campaign config first.\n")
		os.Exit(1)
	}

	// Validate the produced JSON
	configOptions := &config.Options{
		Environment: environment,
	}

	campaignOptions := &config.Options{
		LocalPathOverride: path,
	}

	cfg := &config.Configuration{}
	campaignCfg := &campaign.Configuration{}
	if err := config.LoadConfigForEnvironment(cfg, configOptions); err != nil {
		fmt.Printf("Error validating config: %s\n", err)
		os.Exit(1)
	}
	if err := config.LoadConfigForEnvironment(campaignCfg, campaignOptions); err != nil {
		fmt.Printf("Error validating campaign config: %+v\n", err)
		os.Exit(1)
	}
	if err := campaignCfg.DenormalizeData(); err != nil {
		fmt.Printf("Error denormalizing campaigns: %s\n", err)
		os.Exit(1)
	}

	fmt.Println("Config files look good")

	// aws
	awsProfileTemplate := "twitch-fortuna-%s"

	var awsProfile string
	if environment == "prod" {
		awsProfile = fmt.Sprintf(awsProfileTemplate, "aws")
	} else {
		awsProfile = fmt.Sprintf(awsProfileTemplate, "dev")
	}

	// Set up db client with proper AWS_PROFILE for the given environment
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Profile: awsProfile,
	}))

	db := gdb.New(sess, &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})

	// DAOs
	campaignDAO := dynamo.NewCampaignDAO(db, cfg)
	objectiveDAO := dynamo.NewObjectiveDAO(db, cfg)
	milestoneDAO := dynamo.NewMilestoneDAO(db, cfg)
	triggerDAO := dynamo.NewTriggerDAO(db, cfg)
	daos := dynamo.DAOs{
		CampaignDAO:  campaignDAO,
		ObjectiveDAO: objectiveDAO,
		MilestoneDAO: milestoneDAO,
		TriggerDAO:   triggerDAO,
	}

	// Actual upload
	if !verifyOnly {
		if !confirm() {
			os.Exit(0)
		}
		if err := upload(daos, campaignCfg, selectedDomain); err != nil {
			fmt.Printf("Encountered an error while uploading to dynamoDB: %v\n", err)
			os.Exit(1)
		}
	} else {
		fmt.Println("Skipping the upload process")
	}

	// Verification
	fmt.Println("Verifying uploaded config against the json file...")
	if err := verify(daos, campaignCfg, selectedDomain); err != nil {
		fmt.Printf("Verification failed: %v\n", err)
		os.Exit(0)
	} else {
		fmt.Println("Looks good")
	}

	// Done
	fmt.Print("k bye\n")
	os.Exit(0)
}

func confirm() bool {
	prompt := fmt.Sprintf("%s %s|[%s]: ", "Everything looks good for migration. Do it?", "y", "n")
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(prompt)
		answer, _ := reader.ReadString('\n')
		answer = strings.ToLower(strings.TrimSpace(answer))
		if answer == "" {
			return false
		}
		if !responses[answer] {
			fmt.Println("Please enter y or n")
			continue
		}
		if answer == "y" || answer == "Y" {
			return true
		}
		if answer == "n" || answer == "N" {
			return false
		}
	}
}

func checkCampaignFile(env string) (string, error) {
	filePath := fmt.Sprintf(localCampaignConfigPath, os.Getenv("GOPATH"), organizationName, serviceName, env)
	_, err := ioutil.ReadFile(filePath)

	if err != nil {
		return "", errors.Wrapf(err, "error reading %v", filePath)
	}

	return filePath, nil
}

func upload(daos dynamo.DAOs, campaignCfg *campaign.Configuration, selectedDomain string) error {
	for domain, campaigns := range campaignCfg.Campaigns {
		if selectedDomain != "" && domain != selectedDomain {
			continue
		}
		fmt.Printf("---------------------- DOMAIN: %s ---------------\n", domain)
		for _, campaign := range campaigns {
			// Process
			fmt.Printf("Uploading campaign row - %s\n", campaign.ID)
			if err := daos.CampaignDAO.AddCampaign(campaign); err != nil {
				return err
			}

			for _, objective := range campaign.Objectives {
				fmt.Printf("\tUploading objective row - %s\n", objective.ID)
				if err := daos.ObjectiveDAO.AddObjective(objective); err != nil {
					return err
				}

				for _, milestone := range objective.Milestones {
					fmt.Printf("\t\tUploading milestone row - %s\n", milestone.ID)
					if err := daos.MilestoneDAO.AddMilestone(milestone); err != nil {
						return err
					}
				}
			}

			for _, trigger := range campaign.Triggers {
				fmt.Printf("\tUploading trigger row - %s\n", trigger.ID)
				if err := daos.TriggerDAO.AddTrigger(trigger); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func verify(daos dynamo.DAOs, campaignCfg *campaign.Configuration, selectedDomain string) error {
	for domain, campaigns := range campaignCfg.Campaigns {
		if selectedDomain != "" && selectedDomain != domain {
			continue
		}

		hydratedCampaignCfg, err := daos.GetCampaignConfigForDomain(domain)
		if err != nil {
			return err
		}

		if len(hydratedCampaignCfg.Campaigns) == 0 {
			fmt.Println("Campaigns are not in dynamo yet (dry run?)")
			return nil
		}

		err = hydratedCampaignCfg.DenormalizeData()
		if err != nil {
			return err
		}

		for hydratedDomain, hydratedCampaigns := range hydratedCampaignCfg.Campaigns {
			if hydratedDomain != domain {
				return fmt.Errorf("hydrated domain is %v which is different from %v", hydratedDomain, domain)
			}

			return compareCampaigns(campaigns, hydratedCampaigns)
		}
	}
	return nil
}

func compareCampaigns(campaigns1 []*campaign.Campaign, campaigns2 []*campaign.Campaign) error {
	// mappify campaigns 2 for faster access
	campaigns2Map := map[string]*campaign.Campaign{}
	for _, cam2 := range campaigns2 {
		campaigns2Map[cam2.ID] = cam2
	}

	count := 0

	for _, cam1 := range campaigns1 {
		cam2, ok := campaigns2Map[cam1.ID]
		if ok {
			if len(cam1.Objectives) != len(cam2.Objectives) {
				return fmt.Errorf("campaigns have different number of objectives campaign_id: %s", cam1.ID)
			}

			if err := compareObjectives(cam1.Objectives, cam2.Objectives); err != nil {
				return fmt.Errorf("Objective validation failed %+v", err)
			}

			if len(cam1.Triggers) != len(cam2.Triggers) {
				return fmt.Errorf("campaigns have different number of triggers campaign_id: %s", cam1.ID)
			}

			if err := compareTriggers(cam1.Triggers, cam2.Triggers); err != nil {
				return fmt.Errorf("Trigger validation failed %+v", err)
			}
			count = count + 1
		}
	}

	if count != len(campaigns1) {
		return errors.New("Two resulted campaigns have inconsistent IDs")
	}
	return nil
}

func compareObjectives(objectives1 []*campaign.Objective, objectives2 []*campaign.Objective) error {
	// mappify objectives 2 for faster access
	objectives2Map := map[string]*campaign.Objective{}
	for _, obj2 := range objectives2 {
		objectives2Map[obj2.ID] = obj2
	}

	count := 0

	for _, obj1 := range objectives1 {
		obj2, ok := objectives2Map[obj1.ID]
		if ok {
			if len(obj1.Milestones) != len(obj2.Milestones) {
				return fmt.Errorf("objectives have different number of milesones objective_id: %s", obj1.ID)
			}

			if err := compareMilestones(obj1.Milestones, obj2.Milestones); err != nil {
				return fmt.Errorf("Milestone validation failed %+v", err)
			}
			count = count + 1
		}
	}

	if count != len(objectives1) {
		return errors.New("Two resulted objectives have inconsistent IDs")
	}
	return nil
}

func compareMilestones(milestones1 []*campaign.Milestone, milestones2 []*campaign.Milestone) error {
	// mappify objectives 2 for faster access
	milestones2Map := map[string]*campaign.Milestone{}
	for _, mil2 := range milestones2 {
		milestones2Map[mil2.ID] = mil2
	}

	count := 0

	for _, mil1 := range milestones1 {
		if _, ok := milestones2Map[mil1.ID]; ok {
			count = count + 1
		}
	}

	if count != len(milestones1) {
		return errors.New("Two resulted milestones have inconsistent IDs")
	}

	return nil
}

func compareTriggers(triggers1 []*campaign.Trigger, triggers2 []*campaign.Trigger) error {
	// mappify triggers 2 for faster access
	triggers2Map := map[string]*campaign.Trigger{}
	for _, trig2 := range triggers2 {
		triggers2Map[trig2.ID] = trig2
	}

	count := 0

	for _, trig1 := range triggers1 {
		if _, ok := triggers2Map[trig1.ID]; ok {
			count = count + 1
		}
	}

	if count != len(triggers1) {
		return errors.New("Two resulted triggers have inconsistent IDs")
	}
	return nil
}
