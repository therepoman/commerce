package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/model"
	log "github.com/sirupsen/logrus"
)

const (
	stagingSubscriptionsBaseURL = "https://main.us-west-2.beta.subscriptions.twitch.a2z.com"
	prodSubscriptionsBaseURL    = "https://main.us-west-2.prod.subscriptions.twitch.a2z.com"
	apiEndpoint                 = "/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelSubscriptions"
)

var (
	channelID    = flag.String("channel", "", "ID of the channel to get subscriptions for")
	productID    = flag.String("product", "", "(optional) ID of a specific ticket product owned by the specified channel to get subscriptions for")
	minStartDate = flag.String("minstart", "", "the minimum date (in RFC 3339 format) for the subscription's start date")
	maxStartDate = flag.String("maxstart", "", "the maximum date (in RFC 3339 format) for the subscription's start date")
	env          = flag.String("env", "dev", "environment to run in")
)

func main() {
	flag.Parse()

	var baseURL string
	switch *env {
	case "prod", "production":
		baseURL = prodSubscriptionsBaseURL
	case "stg", "staging":
		baseURL = stagingSubscriptionsBaseURL
	case "dev", "devo", "development":
		fallthrough
	default:
		baseURL = stagingSubscriptionsBaseURL
	}
	apiURL := baseURL + apiEndpoint

	log.Infof("Starting requests to get channel subscriptions to %s", apiURL)

	resultsFile, err := os.Create("cmd/_scripts/data/get_subscribers_results.txt")
	if err != nil {
		log.Fatal("Couldn't create results file", err)
	}

	limit := 1000
	offset := 0
	numRespSubs := limit
	seenUsers := map[string]bool{}

	for numRespSubs == limit {
		req := model.GetChannelSubscriptionsRequest{
			ChannelID: *channelID,
			Limit:     limit,
			Offset:    offset,
		}

		contextLogger := log.WithField("request", req)

		reqJSON, err := json.Marshal(req)
		if err != nil {
			contextLogger.Fatal("Error marshaling request to json", err)
		}

		resp, err := http.Post(apiURL, "application/json", bytes.NewReader(reqJSON))
		if err != nil {
			contextLogger.Fatal("Post error", err)
		}

		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		bodyString := buf.String()

		if resp.StatusCode != 200 {
			log.WithFields(log.Fields{
				"request": req,
				"status":  resp.Status,
				"body":    bodyString,
			}).Fatal("Non 200 response", err)
		}

		contextLogger.Info("Processed request")

		var subsResp model.GetChannelSubscriptionsResponse
		err = json.Unmarshal(buf.Bytes(), &subsResp)
		if err != nil {
			contextLogger.Fatal("Error unmarshaling request from JSON", err)
		}

		for _, subscription := range subsResp.Subscriptions {
			// If a productID was specified, filter out subs that do not have that productID
			if *productID != "" && subscription.ProductID != *productID {
				continue
			}

			// If min or max start dates were specified, filter out subs whose benefit start does not
			// occur between min and max
			if *minStartDate != "" {
				benefitStart, _ := time.Parse(time.RFC3339, subscription.BenefitStart)
				minBenefitStart, _ := time.Parse(time.RFC3339, *minStartDate)

				if benefitStart.Before(minBenefitStart) {
					continue
				}
			}
			if *maxStartDate != "" {
				benefitStart, _ := time.Parse(time.RFC3339, subscription.BenefitStart)
				maxBenefitStart, _ := time.Parse(time.RFC3339, *maxStartDate)

				if benefitStart.After(maxBenefitStart) {
					continue
				}
			}

			// Sometimes there may be multiple subs for one product for the same user. Skip writing these to the results file.
			if _, ok := seenUsers[subscription.OwnerID]; !ok {
				seenUsers[subscription.OwnerID] = true
			} else {
				continue
			}

			resultsFile.WriteString(fmt.Sprintf("%s\n", subscription.OwnerID))
		}

		numRespSubs = len(subsResp.Subscriptions)
		offset += limit

		resp.Body.Close()

		time.Sleep(50 * time.Millisecond)
	}

	resultsFile.Close()

	log.Infof("Requests complete")
}
