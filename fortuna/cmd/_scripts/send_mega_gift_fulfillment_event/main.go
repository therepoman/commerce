// `send_bits_event` is a `go install`-able scriptlike program to help manually test the `bits_events` SQS code in Fortuna.
// It sends a configurable message to the dev cheering events SQS queue that a locally-running dev instance of Fortuna may ingest.
// Since there's a dev deployment of Fortuna as well, `send_bits_event` allows sending multiple permutations of the same message
// in quick succession to increase the likelihood of a local instance beating the deployment instances to at least one of them.
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/google/uuid"
)

const (
	devQueueURL     = "https://sqs.us-west-2.amazonaws.com/106728537780/dev-megacheer-gift-fulfillment-events-sqs"
	stagingQueueURL = "https://sqs.us-west-2.amazonaws.com/106728537780/staging-megacheer-gift-fulfillment-events-sqs"
	prodQueueURL    = "https://sqs.us-west-2.amazonaws.com/180265471281/prod-megacheer-gift-fulfillment-events-sqs"
)

const (
	defaultAwsProfile = "twitch-fortuna-dev"
	defaultAwsRegion  = "us-west-2"
)

const (
	defaultChannelId       = ""
	defaultUserId          = ""
	defaultEmote           = "owlldn"
	defaultAmount          = 100
	defaultMessage         = "love gaming"
	defaultTimes           = 1
	defaultTriggerID       = ""
	defaultBenefactorLogin = "MissCoded"
	defaultRecipientID     = ""
	defaultDomain          = ""
)

var (
	env             = flag.String("env", "dev", "Environment to target with cheering events")
	channelID       = flag.String("channelID", defaultChannelId, "Channel ID to associate with the cheer event")
	userID          = flag.String("userID", defaultUserId, "User ID to send the cheer event from")
	emote           = flag.String("emote", defaultEmote, "Emote to associate with the cheer event")
	amount          = flag.Uint("amount", defaultAmount, "Amount of bits to cheer with")
	message         = flag.String("message", defaultMessage, "Chat message to associate with cheer")
	times           = flag.Uint("times", defaultTimes, "Number of times to send event (with unique event_id each time).")
	isAnonymous     = flag.Bool("anon", false, "If the cheer is anonymous")
	triggerID       = flag.String("triggerID", defaultTriggerID, "triggerID")
	benefactorLogin = flag.String("benefactorLogin", defaultBenefactorLogin, "benefactorLogin")
	recipientID     = flag.String("recipientID", defaultRecipientID, "recipientID")
	domain          = flag.String("domain", defaultDomain, "domain")
)

func main() {
	flag.Parse()

	var isProd bool
	var queueUrl string
	switch *env {
	case "prod", "production":
		isProd = true
		queueUrl = prodQueueURL
	case "stg", "staging":
		queueUrl = stagingQueueURL
	case "dev", "devo", "development":
		fallthrough
	default:
		queueUrl = devQueueURL
	}

	if *amount == 0 {
		log.Fatal("Amount of bits to cheer must not be zero")
	}
	if *times == 0 {
		log.Fatal("Must send event at least one time")
	}
	if *channelID == "" {
		log.Fatal("Must specify non-empty channelID")
	}
	if *userID == "" {
		log.Fatal("Must specify non-empty userID")
	}
	if *triggerID == "" {
		log.Fatal("Must specify non-empty triggerID")
	}
	if *recipientID == "" {
		log.Fatal("Must specify non-empty recipientID")
	}

	fmt.Printf("Preparing to send message to SQS queue at %v with parameters:\n", queueUrl)
	fmt.Println("")
	fmt.Printf("channelID: %v\n", *channelID)
	fmt.Printf("userID: %v\n", *userID)
	fmt.Printf("emote: %v\n", *emote)
	fmt.Printf("amount: %v\n", *amount)
	fmt.Printf("message: %v\n", *message)
	fmt.Println("")
	fmt.Printf("times: %v\n", *times)
	fmt.Println("")

	if isProd && !confirmProd() {
		fmt.Println("Canceled sending event.")
		os.Exit(1)
	}

	if _, exists := os.LookupEnv("AWS_PROFILE"); !exists {
		fmt.Printf("Using AWS_PROFILE=%v\n", defaultAwsProfile)
		os.Setenv("AWS_PROFILE", defaultAwsProfile)
	}

	awsRegion := defaultAwsRegion
	if envRegion, exists := os.LookupEnv("AWS_DEFAULT_REGION"); exists {
		awsRegion = envRegion
	}

	fmt.Printf("Using AWS region: %v\n", awsRegion)
	fmt.Println("")

	bitsEvent := model.BitsEvent{
		ChannelID: *channelID,
		UserID:    *userID,
		Message:   *message,
		Amount:    int32(*amount),
		EmoteTotals: map[string]int64{
			*emote: int64(*amount),
		},
		IsAnonymous: *isAnonymous,
	}

	giftEvent := sns.MegaCheerGiftFulfillmentEvent{
		BenefactorLogin: *benefactorLogin,
		RecipientID:     *recipientID,
		TriggerID:       *triggerID,
		Domain:          *domain,
	}

	s, err := session.NewSession()
	if err != nil {
		log.Fatalf("Failed to create AWS session: %v", err)
	}

	sqsClient := sqs.New(s, aws.NewConfig().WithRegion(awsRegion))

	var msgAttribute sqs.MessageAttributeValue
	{
		dataType := "Number"
		stringValue := "1"
		msgAttribute = sqs.MessageAttributeValue{
			DataType:    &dataType,
			StringValue: &stringValue,
		}
	}

	for i := 0; i < int(*times); i++ {
		bitsEvent := bitsEvent
		bitsEvent.EventID = uuid.New().String()

		giftEvent := giftEvent
		giftEvent.Event = bitsEvent

		body, err := json.Marshal(giftEvent)
		if err != nil {
			log.Fatal(err)
		}
		bodyString := string(body)

		messageInput := sqs.SendMessageInput{
			MessageBody: &bodyString,
			QueueUrl:    &queueUrl,
			MessageAttributes: map[string]*sqs.MessageAttributeValue{
				"Version": &msgAttribute,
			},
		}

		log.Printf("Sending event %v...\n", i+1)
		_, err = sqsClient.SendMessage(&messageInput)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Message successfully sent.")
	}

	log.Printf("Successfully sent %v messages.\n", *times)
}

func confirmProd() bool {
	prompt := "WARNING! You have chosen to send cheering events to PROD cheering events queue.\nEnter \"yes\" to confirm: "
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)

	answer, _ := reader.ReadString('\n')
	answer = strings.TrimSpace(strings.ToLower(answer))

	return answer == "yes"
}
