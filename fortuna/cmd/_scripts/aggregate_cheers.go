package main

import (
	"encoding/json"
	"io/ioutil"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/model"
)

func main() {
	userCheers := map[string]*model.UserCheer{}

	rawJSON, err := ioutil.ReadFile("cmd/_scripts/data/sample_bits_events.json")
	if err != nil {
		log.Fatalf("Error opening file: %v", err)
	}

	bitsEvents := []model.BitsLog{}
	json.Unmarshal(rawJSON, &bitsEvents)

	for _, bitsEvent := range bitsEvents {
		for team, amount := range bitsEvent.EmoteUses {
			_, exists := userCheers[bitsEvent.UserID]
			if exists {
				_, exists := userCheers[bitsEvent.UserID].TeamTotals[team]
				if exists {
					userCheers[bitsEvent.UserID].TeamTotals[team] += amount
				} else {
					userCheers[bitsEvent.UserID].TeamTotals[team] = amount
				}
			} else {
				userCheers[bitsEvent.UserID] = &model.UserCheer{
					TeamTotals: map[string]int64{
						team: amount,
					},
				}
			}
		}
	}

	peopleJson, _ := json.MarshalIndent(userCheers, "", "  ")
	log.Info(string(peopleJson))
	_ = ioutil.WriteFile("cmd/_scripts/data/aggregate_cheers.json", peopleJson, 0644)
}
