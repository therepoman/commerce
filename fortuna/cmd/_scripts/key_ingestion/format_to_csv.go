package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/urfave/cli"
)

// usage example: go run ./cmd/_scripts/key_ingestion/format_to_csv.go -k ./playerspack.txt -sku rewards-purchase-owl2018-allstars -out ./playerspack.csv
func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "keys, k",
			Usage: "key file",
		},

		cli.StringFlag{
			Name:  "sku",
			Usage: "sku",
		},

		cli.StringFlag{
			Name:  "out",
			Usage: "filepath to output CSV file",
		},
	}

	app.Action = generateFiles
	app.Run(os.Args)

	// Adhoc executions

	//combineCSV()
	//outputSKUs()
}

func generateFiles(c *cli.Context) {
	keyFilename := c.String("k")
	sku := c.String("sku")
	if len(sku) > 40 {
		panic("SKU is longer than 40 chars")
	}
	fmt.Fprintf(os.Stderr, "SKU: %s\n", sku)
	fmt.Fprintf(os.Stderr, "Key file: %s\n", keyFilename)

	keyFile, err := os.Open(keyFilename)
	if err != nil {
		panic(err)
	}

	keyfileScanner := bufio.NewScanner(keyFile)

	csvFile, err := os.Create(c.String("out"))
	if err != nil {
		panic(err)
	}

	csvFile.WriteString("sku,key\n")

	for keyfileScanner.Scan() {
		line := keyfileScanner.Text()
		csvFile.WriteString(fmt.Sprintf("%s,%s\n", sku, line))
	}

	fmt.Fprintf(os.Stderr, "Test keys can be found %s\n", csvFile.Name())
}

func combineCSV() {
	allCSVs, err := ioutil.ReadDir("./hgc-csv/extra")
	if err != nil {
		panic(err)
	}

	resultFile, err := os.Create("hgcextrakeys.csv")
	if err != nil {
		panic(err)
	}

	resultFile.WriteString("sku,key\n")

	for _, csv := range allCSVs {
		csvFile, err := os.Open(fmt.Sprintf("./hgc-csv/extra/%s", csv.Name()))
		if err != nil {
			panic(err)
		}
		fmt.Fprintf(os.Stderr, "Getting %s\n", csvFile.Name())

		keyfileScanner := bufio.NewScanner(csvFile)

		for keyfileScanner.Scan() {
			line := keyfileScanner.Text()
			if strings.Contains(line, "sku,key") {
				continue
			}
			resultFile.WriteString(line + "\n")
		}
	}

	fmt.Fprintf(os.Stderr, "Done: %s\n", resultFile.Name())
}

func outputSKUs() {
	allCSVs, err := ioutil.ReadDir("./hgc-csv/extra")
	if err != nil {
		panic(err)
	}

	resultFile, err := os.Create("hgcskus-extra.txt")
	if err != nil {
		panic(err)
	}

	for _, csv := range allCSVs {
		csvFile, err := os.Open(fmt.Sprintf("./hgc-csv/extra/%s", csv.Name()))
		if err != nil {
			panic(err)
		}
		fmt.Fprintf(os.Stderr, "Getting %s\n", csvFile.Name())

		keyfileScanner := bufio.NewScanner(csvFile)

		for keyfileScanner.Scan() {
			line := keyfileScanner.Text()
			if strings.Contains(line, "sku,key") {
				continue
			}
			resultFile.WriteString(strings.Split(line, ",")[0] + "\n")
			break
		}
	}

	fmt.Fprintf(os.Stderr, "Done: %s\n", resultFile.Name())
}
