package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/model"
)

// Team code to emote set ID mapping
var teamToSetIDMap = map[string]string{
	"owlldn": "605216",
	"owlbos": "605217",
	"owldal": "605218",
	"owlnye": "605219",
	"owlval": "605220",
	"owlsfs": "605221",
	"owlhou": "605222",
	"owlshd": "605223",
	"owlseo": "605224",
	"owlphi": "605225",
	"owlgla": "605226",
	"owlfla": "605227",
}

const topicARN = "arn:aws:sns:us-west-2:077362005997:dev-mako-events"

type Teams struct {
	Data map[string][]string `json:"data"`
}

type SNSClient struct {
	sns *sns.SNS
}

type MakoEvent struct {
	UserID string `json:"user_id" validate:"nonzero"` // Twitch user id
	Scope  string `json:"scope" validate:"nonzero"`   // Scope for this event, registered during onboarding. Must be a valid event scope.
	Key    string `json:"key" validate:"nonzero"`     // The unique key for this event.
	Value  string `json:"value" validate:"nonzero"`   // The value of the event
	Origin string `json:"origin" validate:"nonzero"`  // The source of the event
}

// Script used to back fill team emotes
func main() {
	rawJSON, err := ioutil.ReadFile("cmd/_scripts/data/aggregate_cheers.json")
	if err != nil {
		log.Fatalf("Error opening file: %v", err)
	}

	bitsEvents := map[string]*model.UserCheer{}
	json.Unmarshal(rawJSON, &bitsEvents)

	snsClient := loadClient()
	log.Info("SNS client initialized")
	log.Infof("Publishing to topic: %s", topicARN)

	// Send Mako SNS messages to entitle emotes
	entitlementData := []MakoEvent{}

	for userID, bitsEvent := range bitsEvents {
		for team, amount := range bitsEvent.TeamTotals {
			emoteSetID, exists := teamToSetIDMap[team]
			if !exists {
				log.Errorf("Team %s does not exist in map for user: %s", team, userID)
			} else if amount < 150 {
				log.Info("Cheer threshold not high enough, skipping")
				continue
			}

			makoData := MakoEvent{
				UserID: userID,
				Scope:  "/emote_sets/commerce/campaign/owl2018/",
				Key:    emoteSetID,
				Value:  "grant",
				Origin: "owl2018 team emote back fill",
			}

			makoSNSJson, err := json.Marshal(makoData)
			if err != nil {
				log.WithFields(log.Fields{
					"userID": userID,
				}).WithError(err).
					Error("failure to marshal SNS message for granting Mako item")
			}

			if err := snsClient.PostToTopic(string(makoSNSJson)); err != nil {
				log.Errorf("failed to entitle emote %s for user %v: %v", teamToSetIDMap[team], userID, err)
				continue
			}
			entitlementData = append(entitlementData, makoData)
		}
		log.Infof("entitled emotes %v to user %s", bitsEvent.TeamTotals, userID)
	}

	b, _ := json.MarshalIndent(entitlementData, "", "  ")
	fmt.Println(string(b))

	_ = ioutil.WriteFile("cmd/_scripts/data/mako_entitlements.json", b, 0644)
}

func loadClient() *SNSClient {
	return &SNSClient{
		sns: sns.New(session.New(), &aws.Config{
			Region: aws.String("us-west-2"),
		}),
	}
}

func (s *SNSClient) PostToTopic(msg string) error {
	publishInput := &sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(topicARN),
		MessageAttributes: map[string]*sns.MessageAttributeValue{
			"Version": {
				DataType:    aws.String("Number"),
				StringValue: aws.String("1"),
			},
		},
	}
	_, err := s.sns.Publish(publishInput)
	return err
}
