package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"code.justin.tv/eventbus/client/testhelpers"
	"code.justin.tv/eventbus/schema/pkg/cheer"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/google/uuid"
)

const (
	devQueueUrl     = "https://sqs.us-west-2.amazonaws.com/106728537780/dev-eventbus-fortuna-sqs"
	stagingQueueUrl = "https://sqs.us-west-2.amazonaws.com/106728537780/staging-eventbus-fortuna-sqs"
	prodQueueUrl    = "https://sqs.us-west-2.amazonaws.com/180265471281/prod-eventbus-fortuna-sqs"
)

const (
	defaultAwsProfile = "twitch-fortuna-dev"
	defaultAwsRegion  = "us-west-2"
)

const (
	defaultChannelId = ""
	defaultUserId    = ""
	defaultAmount    = 100
	defaultMessage   = "i like dogs"
)

var (
	env         = flag.String("env", "dev", "Environment to target with cheering events")
	channelID   = flag.String("channelID", defaultChannelId, "Channel ID to associate with the cheer event")
	userID      = flag.String("userID", defaultUserId, "User ID to send the cheer event from")
	amount      = flag.Uint("amount", defaultAmount, "Amount of bits to cheer with")
	message     = flag.String("message", defaultMessage, "Chat message to associate with cheer")
	isAnonymous = flag.Bool("anon", false, "If the cheer is anonymous")
	loopCount   = flag.Int("loopCount", 1, "If you want to cheer multiple times")
)

func main() {
	flag.Parse()

	var isProd bool
	var queueUrl string
	switch *env {
	case "prod", "production":
		isProd = true
		queueUrl = prodQueueUrl
	case "stg", "staging":
		queueUrl = stagingQueueUrl
	case "dev", "devo", "development":
		fallthrough
	default:
		queueUrl = devQueueUrl
	}

	if *amount == 0 {
		log.Fatal("Amount of bits to cheer must not be zero")
	}
	if *channelID == "" {
		log.Fatal("Must specify non-empty channelID")
	}
	if *userID == "" {
		log.Fatal("Must specify non-empty userID")
	}

	fmt.Printf("Preparing to send message to SQS queue at %v with parameters:\n", queueUrl)
	fmt.Println("")
	fmt.Printf("channelID: %v\n", *channelID)
	fmt.Printf("userID: %v\n", *userID)
	fmt.Printf("amount: %v\n", *amount)
	fmt.Printf("message: %v\n", *message)
	fmt.Println("")
	fmt.Println("")

	if isProd && !confirmProd() {
		fmt.Println("Canceled sending event.")
		os.Exit(1)
	}

	if _, exists := os.LookupEnv("AWS_PROFILE"); !exists {
		fmt.Printf("Using AWS_PROFILE=%v\n", defaultAwsProfile)
		os.Setenv("AWS_PROFILE", defaultAwsProfile)
	}

	awsRegion := defaultAwsRegion
	if envRegion, exists := os.LookupEnv("AWS_DEFAULT_REGION"); exists {
		awsRegion = envRegion
	}

	fmt.Printf("Using AWS region: %v\n", awsRegion)
	fmt.Println("")

	s, err := session.NewSession()
	if err != nil {
		log.Fatalf("Failed to create AWS session: %v", err)
	}

	var msgAttribute sqs.MessageAttributeValue
	{
		dataType := "Number"
		stringValue := "1"
		msgAttribute = sqs.MessageAttributeValue{
			DataType:    &dataType,
			StringValue: &stringValue,
		}
	}

	sqsClient := sqs.New(s, aws.NewConfig().WithRegion(awsRegion))

	message := &cheer.CheerCreate{
		BitsTransactionId: uuid.New().String(),
		ToUserId:          *channelID,
		PublicMessage:     *message,
		Bits:              -1 * int64(*amount),
	}

	if *isAnonymous {
		message.From = &cheer.CheerCreate_AnonymousUser{
			AnonymousUser: &cheer.AnonymousUser{},
		}
	} else {
		message.From = &cheer.CheerCreate_User{
			User: &cheer.User{
				Id: *userID,
			},
		}
	}

	raw, err := testhelpers.DefaultBinaryPayload(message)
	if err != nil {
		log.Fatal(err.Error())
	}

	snsMessage := raw.FakeSNSMessage()
	body, err := snsMessage.SQSBody()
	if err != nil {
		log.Fatal(err.Error())
	}

	messageInput := sqs.SendMessageInput{
		MessageBody: &body,
		QueueUrl:    &queueUrl,
		MessageAttributes: map[string]*sqs.MessageAttributeValue{
			"Version": &msgAttribute,
		},
	}

	log.Printf("Sending event %v...\n", 1)

	for i := 0; i < *loopCount; i++ {
		_, err = sqsClient.SendMessage(&messageInput)
		if err != nil {
			log.Fatal(err)
		} else {
			log.Printf("Message %v successfully sent.", i+1)
		}
		time.Sleep(1 * time.Second) // needs the timeout or the service won't take
	}
	log.Println("All Messages successfully sent.")

	// fmt.Printf(`aws sqs send-message --queue-url "https://my-queue" --message-body '%s'`, body)
}

func confirmProd() bool {
	prompt := "WARNING! You have chosen to send cheering events to PROD cheering events queue.\nEnter \"yes\" to confirm: "
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)

	answer, _ := reader.ReadString('\n')
	answer = strings.TrimSpace(strings.ToLower(answer))

	return answer == "yes"
}
