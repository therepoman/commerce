package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/foundation/twitchclient"
)

const (
	devMakoUrl     = "https://main.us-west-2.beta.mako.twitch.a2z.com"
	stagingMakoUrl = "https://main.us-west-2.beta.mako.twitch.a2z.com"
	prodMakoUrl    = "https://main.us-west-2.prod.mako.twitch.a2z.com"
)

const (
	defaultAwsProfile = "twitch-fortuna-dev"
	defaultAwsRegion  = "us-west-2"
)

const (
	defaultUserID = ""
	defaultDomain = ""
)

/*
	Example:
	$ go run ./cmd/_scripts/delete_emotes_for_domain/main.go -env=dev -userID=123456789 -domain=pride_megacommerce_2020
*/
var (
	env    = flag.String("env", "dev", "Environment to target with cheering events")
	userID = flag.String("userID", defaultUserID, "User ID to delete entitlements from")
	domain = flag.String("domain", defaultDomain, "Campaign Domain Name")
)

func main() {
	flag.Parse()

	var isProd bool
	var makoHost string
	switch *env {
	case "prod", "production":
		isProd = true
		makoHost = prodMakoUrl
	case "stg", "staging":
		makoHost = stagingMakoUrl
	case "dev", "devo", "development":
		fallthrough
	default:
		makoHost = devMakoUrl
	}

	if *userID == "" {
		log.Fatal("Must specify non-empty userID")
	}
	if *domain == "" {
		log.Fatal("Must specify non-empty domain")
	}

	fmt.Printf("Preparing to send message to mako %v with parameters:\n", makoHost)
	fmt.Println("")
	fmt.Printf("channelID: %v\n", *domain)
	fmt.Printf("userID: %v\n", *userID)
	fmt.Println("")
	fmt.Println("")

	if _, exists := os.LookupEnv("AWS_PROFILE"); !exists {
		fmt.Printf("Using AWS_PROFILE=%v\n", defaultAwsProfile)
		os.Setenv("AWS_PROFILE", defaultAwsProfile)
	}

	awsRegion := defaultAwsRegion
	if envRegion, exists := os.LookupEnv("AWS_DEFAULT_REGION"); exists {
		awsRegion = envRegion
	}

	fmt.Printf("Using AWS region: %v\n", awsRegion)
	fmt.Println("")

	// Mako
	makoClient := mako_client.NewMakoProtobufClient(makoHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: makoHost,
	}))

	log.Printf("Getting entitlments %v...\n", 1)
	// fetch user emote set entitlements
	request := &mako_client.GetEmoteEntitlementsRequest{
		OwnerId:  *userID,
		OriginId: *domain,
		Group:    mako_client.EmoteGroup_Rewards,
	}

	resp, err := makoClient.GetEmoteEntitlements(context.Background(), request)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Will deleting the following emotes: ")
	for _, entitlement := range resp.Entitlements {
		fmt.Println(entitlement.String())
	}

	if isProd && !confirmProd() {
		fmt.Println("Canceled sending event.")
		os.Exit(1)
	}

	for _, entitlement := range resp.Entitlements {
		req := &mako_client.DeleteEmoteGroupEntitlementsRequest{
			Entitlements: []*mako_client.EmoteGroupEntitlement{
				{
					OwnerId:  *userID,
					ItemId:   entitlement.EmoteId, //not even used, but needed
					OriginId: *domain,
					Group:    mako_client.EmoteGroup_Rewards,
					GroupId:  entitlement.EmoteId, // this is actually being used as item id
					Start:    entitlement.Start,   //not even used, but needed
					End: &mako_client.InfiniteTime{ //not even used, but needed
						IsInfinite: true,
					},
				},
			},
		}
		_, err := makoClient.DeleteEmoteGroupEntitlements(context.Background(), req)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Deleted: " + entitlement.String())
	}

	// fmt.Printf(`aws sqs send-message --queue-url "https://my-queue" --message-body '%s'`, body)
}

func confirmProd() bool {
	prompt := "WARNING! You have chosen to delete emotes in PROD.\nEnter \"yes\" to confirm: "
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)

	answer, _ := reader.ReadString('\n')
	answer = strings.TrimSpace(strings.ToLower(answer))

	return answer == "yes"
}
