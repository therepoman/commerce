package dao

import (
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	gdb "github.com/guregu/dynamo"
)

var DAO *dynamo.ChannelPropertiesDAO

func InitializeDAO(envPrefix, region string) error {
	s, err := session.NewSession()
	if err != nil {
		return err
	}

	db := gdb.New(s, &aws.Config{
		Region: aws.String(region),
	})

	cfg := &config.Configuration{
		EnvironmentPrefix: envPrefix,
	}

	DAO = dynamo.NewChannelPropertiesDAO(db, cfg)
	return nil
}
