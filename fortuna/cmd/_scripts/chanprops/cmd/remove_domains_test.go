package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFilterDomains(t *testing.T) {
	testCases := []struct {
		input []string
		omit  []string

		wantFiltered []string
		wantOmitted  []string
	}{
		{
			input: []string{"a", "b", "c"},
			omit:  []string{"a", "b"},

			wantFiltered: []string{"c"},
			wantOmitted:  []string{"a", "b"},
		},
		{
			input: []string{"a", "b", "c"},
			omit:  []string{"a", "b", "d"},

			wantFiltered: []string{"c"},
			wantOmitted:  []string{"a", "b"},
		},
		{
			input: []string{"a"},
			omit:  []string{"a", "b", "d"},

			wantFiltered: []string(nil),
			wantOmitted:  []string{"a"},
		},
		{
			input: []string{},
			omit:  []string{"a", "b", "d"},

			wantFiltered: []string(nil),
			wantOmitted:  []string(nil),
		},
		{
			input: []string{"a", "b"},
			omit:  []string{},

			wantFiltered: []string{"a", "b"},
			wantOmitted:  []string(nil),
		},
		{
			input: []string{"a", "b"},
			omit:  []string{"a", "b"},

			wantFiltered: []string(nil),
			wantOmitted:  []string{"a", "b"},
		},
	}

	for _, tt := range testCases {
		gotFiltered, gotOmitted := filterDomains(tt.input, tt.omit)
		assert.Equal(t, tt.wantFiltered, gotFiltered)
		assert.Equal(t, tt.wantOmitted, gotOmitted)
	}
}
