package cmd

import (
	"fmt"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/dao"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var removeDomainsCmd = &cobra.Command{
	Use:   "removeDomains <channelID> <domain> [<domain>...]",
	Short: "Remove one or more domains from ChannelProperties for the passed channel ID",
	Long: `Remove one or more domains from the passed channel ID's domains list. Returns an error if there are no
existing ChannelProperties for the passed channel ID. No error is returned if the passed domains are not present
in the existing ChannelProperties domain list.`,
	Args: cobra.MinimumNArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		channelID := args[0]
		if channelID == "" {
			return errors.New("removeDomains must be passed a channel ID argument")
		}

		domainsToRemove := args[1:]
		if len(domainsToRemove) < 1 {
			return errors.New("removeDomains must be passed at least one domain to remove")
		}

		props, err := dao.DAO.GetChannelProperties(channelID)
		if err != nil {
			return err
		}
		if props.ChannelID == "" {
			return errors.Errorf("No ChannelProperties found for channelID=%s", channelID)
		}

		filteredDomains, domainsRemoved := filterDomains(props.Domains, domainsToRemove)
		fmt.Printf("Removed domains: %v\n", domainsRemoved)
		fmt.Println("Updating...")

		updated, err := dao.DAO.UpdateChannelProperties(props.ChannelID, filteredDomains, props.HasPass)
		if err != nil {
			return err
		}

		fmt.Printf("Channel ID: %s\nDomains: %v\nHas pass: %v", updated.ChannelID, updated.Domains, updated.HasPass)

		fmt.Println()
		fmt.Println("Successfully updated ChannelProperties to remove domains.")

		return nil
	},
}

// filterDomains takes an input slice (to filter) and an omit slice (containing elements to remove from the input slice).
// It returns a newly-allocated filtered slice as well as a slice indicating which elements were actually removed.
func filterDomains(input []string, omit []string) ([]string, []string) {
	omitMap := map[string]struct{}{}
	for _, domain := range omit {
		omitMap[domain] = struct{}{}
	}

	var out []string
	var omitted []string

	for _, domain := range input {
		if _, omit := omitMap[domain]; omit {
			omitted = append(omitted, domain)
			continue
		}
		out = append(out, domain)
	}

	return out, omitted
}
