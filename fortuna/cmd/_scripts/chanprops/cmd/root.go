package cmd

import (
	"os"

	"bufio"
	"strings"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/dao"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

const defaultRegion = "us-west-2"

type environmentSetting struct {
	dev, staging, prod bool
}

func (e *environmentSetting) validate() error {
	check := func(settings ...*bool) error {
		var isSet bool

		for _, b := range settings {
			if isSet && *b {
				return errors.New("Only one of --dev/--staging/--prod can be specified.")
			}
			if *b {
				isSet = true
			}
		}

		if !isSet {
			return errors.New("An environment must be specified with one of --dev/--staging/--prod.")
		}

		return nil
	}

	return check(&e.dev, &e.staging, &e.prod)
}

func (e *environmentSetting) getEnvPrefix() string {
	if e.dev {
		return "dev"
	}
	if e.staging {
		return "staging"
	}
	if e.prod {
		return "prod"
	}

	panic("getEnvPrefix() called on unconfigured environmentSetting")
}

var env = environmentSetting{}

var RootCmd = &cobra.Command{
	Use:   "chanprops <subcommand>",
	Short: "Command-line CRUD for Fortuna ChannelProperties",
	Long: `Command-line CRUD for Fortuna ChannelProperties, which associate a channelID to a set of Fortuna domains.
Most channels will associate with a single domain, which enables a custom campaigns UI on the channel page
and allows cheers (or other actions) in that channel to contribute progress toward the reward objectives
in that domain's campaign configuration.'`,
	Args: cobra.MinimumNArgs(1),
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		// Don't validate or do any work for help command
		if cmd.CalledAs() == "help" {
			return nil
		}

		err := env.validate()
		if err != nil {
			return err
		}

		setAWSProfile(env)

		err = dao.InitializeDAO(env.getEnvPrefix(), defaultRegion)
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	RootCmd.PersistentFlags().BoolVar(&env.dev, "dev", false, "Run subcommands targeting Fortuna dev environment")
	RootCmd.PersistentFlags().BoolVar(&env.staging, "staging", false, "Run subcommands targeting Fortuna staging environment")
	RootCmd.PersistentFlags().BoolVar(&env.prod, "prod", false, "Run subcommands targeting Fortuna prod environment")
	RootCmd.AddCommand(getCmd)
	RootCmd.AddCommand(createCmd)
	RootCmd.AddCommand(deleteCmd)
	RootCmd.AddCommand(addDomainsCmd)
	RootCmd.AddCommand(removeDomainsCmd)
}

func setAWSProfile(env environmentSetting) {
	var awsProfile string

	if env.dev || env.staging {
		awsProfile = "twitch-fortuna-dev"
	} else if env.prod {
		awsProfile = "twitch-fortuna-aws"
	} else {
		panic("Invalid environmentSetting passed to setAWSProfile. This should not be possible and indicates a bug.")
	}

	os.Setenv("AWS_PROFILE", awsProfile)
}

// Used to read a y/n response from term before running certain commands
func getConfirmation() error {
	b := bufio.NewReader(os.Stdin)
	answer, err := b.ReadString('\n')
	if err != nil {
		return err
	}

	if strings.TrimSpace(strings.ToLower(answer)) != "y" {
		return errors.New("Aborted.")
	}

	return nil
}
