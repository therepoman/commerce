package cmd

import (
	"fmt"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/dao"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:   "get <channelID>",
	Short: "Get ChannelProperties for a channel ID and print to console",
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		channelID := args[0]
		props, err := dao.DAO.GetChannelProperties(channelID)
		if err != nil {
			return err
		}

		if props.ChannelID == "" {
			// Empty primary key field. No row was returned.
			fmt.Printf("No channel properties configured for channelID=%s.\n", channelID)
			return nil
		}

		fmt.Printf("Channel ID: %s\n", props.ChannelID)
		fmt.Printf("Domains: %v\n", props.Domains)
		fmt.Printf("Has pass: %v\n", props.HasPass)

		return nil
	},
}
