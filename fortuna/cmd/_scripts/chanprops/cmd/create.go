package cmd

import (
	"fmt"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/dao"
	"code.justin.tv/commerce/fortuna/dynamo"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var createCmd = &cobra.Command{
	Use:   "create <channelID> [--domains <domain>[,<domain> ...]] [--hasPass]",
	Short: "Create ChannelProperties for a channel ID",
	Long: `Create ChannelProperties for a channel ID.
"create" will not overwrite an existing record for the passed channel ID and will instead return an error.
Use the --domains flag to optionally initially associate the channel ID with a comma-separated list of domains.
Use the --hasPass flag to optionally mark the channel as possessing a pass or ticket product.`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		channelID := args[0]
		if channelID == "" {
			return errors.New("create must be passed a channel ID argument")
		}

		domains, err := cmd.Flags().GetStringSlice("domains")
		if err != nil {
			return err
		}
		hasPass, err := cmd.Flags().GetBool("hasPass")
		if err != nil {
			return err
		}

		insertion := &dynamo.ChannelProperties{
			ChannelID: channelID,
			Domains:   domains,
			HasPass:   hasPass,
		}

		fmt.Printf("Channel ID: %s\nDomains: %v\nHas pass: %v\n", insertion.ChannelID, insertion.Domains, insertion.HasPass)
		fmt.Println()
		fmt.Println("Create? [y/n]")
		err = getConfirmation()
		if err != nil {
			return err
		}

		err = dao.DAO.CreateChannelProperties(insertion.ChannelID, insertion.Domains, insertion.HasPass)
		if err != nil {
			return err
		}

		fmt.Printf("Creation successful")
		return nil
	},
}

func init() {
	createCmd.Flags().StringSlice("domains", []string{}, "Comma-separated list of domains to associate with channel ID")
	createCmd.Flags().Bool("hasPass", false, "Indicate that channel has a purchasable pass")
}
