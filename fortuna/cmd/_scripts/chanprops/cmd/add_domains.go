package cmd

import (
	"fmt"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/dao"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var addDomainsCmd = &cobra.Command{
	Use:   "addDomains <channelID> <domain> [<domain>...]",
	Short: "Add domains for a channel ID with existing ChannelProperties",
	Long: `Add domains to a channel ID's domain list. Returns an error if the channel ID does not already have
configured ChannelProperties. Any duplicates (either between the existing list and the passed list, or within
the passed list) are removed before the DynamoDB item is updated.`,
	Args: cobra.MinimumNArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		channelID := args[0]
		if channelID == "" {
			return errors.New("addDomains must be passed a channel ID argument")
		}

		domains := args[1:]
		if len(domains) < 1 {
			return errors.New("addDomains must be passed at least one domain to add")
		}

		props, err := dao.DAO.GetChannelProperties(channelID)
		if err != nil {
			return err
		}
		if props.ChannelID == "" {
			return errors.Errorf("addDomains: No ChannelProperties exist for channelID=%s", channelID)
		}

		newDomains := concatWithoutDuplicates(props.Domains, domains)

		updated, err := dao.DAO.UpdateChannelProperties(channelID, newDomains, props.HasPass)

		fmt.Printf("Added domains for channelID=%s:\n", channelID)
		fmt.Println("Updated ChannelProperties:")
		fmt.Printf("Channel ID: %s\nDomains: %v\nHas pass: %v\n", updated.ChannelID, updated.Domains, updated.HasPass)

		return nil
	},
}

func concatWithoutDuplicates(dst []string, src []string) []string {
	var out []string
	added := map[string]struct{}{}

	for _, domain := range dst {
		if _, ok := added[domain]; ok {
			continue
		}
		added[domain] = struct{}{}
		out = append(out, domain)
	}
	for _, domain := range src {
		if _, ok := added[domain]; ok {
			continue
		}
		added[domain] = struct{}{}
		out = append(out, domain)
	}

	return out
}
