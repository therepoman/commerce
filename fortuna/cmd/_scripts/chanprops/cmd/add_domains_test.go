package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConcatWithoutDuplicates(t *testing.T) {
	testCases := []struct {
		dst, src []string
		want     []string
	}{
		{
			dst:  []string{"a", "b"},
			src:  []string{"c", "d"},
			want: []string{"a", "b", "c", "d"},
		},
		{
			dst:  []string{"a", "b"},
			src:  []string{"c", "d", "c", "d"},
			want: []string{"a", "b", "c", "d"},
		},
		{
			dst:  []string{"a", "a", "b", "b"},
			src:  []string{"c", "d"},
			want: []string{"a", "b", "c", "d"},
		},
		{
			dst:  []string{"a", "b"},
			src:  []string{"a", "b", "c", "d"},
			want: []string{"a", "b", "c", "d"},
		},
		{
			dst:  []string{},
			src:  []string{"c", "d"},
			want: []string{"c", "d"},
		},
		{
			dst:  []string{"a", "b"},
			src:  []string{},
			want: []string{"a", "b"},
		},
		{
			dst:  []string{},
			src:  []string{},
			want: []string(nil),
		},
	}

	for _, tt := range testCases {
		have := concatWithoutDuplicates(tt.dst, tt.src)
		assert.Equal(t, have, tt.want)
	}
}
