package cmd

import (
	"fmt"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/dao"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:   "delete <channelID>",
	Short: "Delete ChannelProperties for a channel ID",
	Long:  `Delete ChannelProperties for a channel ID, removing its item in the underlying DynamoDB table.`,
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		channelID := args[0]
		if channelID == "" {
			return errors.New("delete must be passed a channel ID argument")
		}

		props, err := dao.DAO.GetChannelProperties(channelID)
		if err != nil {
			return err
		}
		if props.ChannelID == "" {
			return errors.New("No existing ChannelProperties to delete")
		}

		fmt.Printf("Channel ID: %s\nDomains: %v\nHas pass: %v\n", props.ChannelID, props.Domains, props.HasPass)
		fmt.Println()
		fmt.Println("Delete? [y/n]")
		err = getConfirmation()
		if err != nil {
			return err
		}

		err = dao.DAO.DeleteChannelProperties(channelID)
		if err != nil {
			return err
		}

		fmt.Println("Successfully deleted.")

		return nil
	},
}
