package cmd

import "testing"

func TestEnvironmentSettingValidate(t *testing.T) {
	testCases := []struct {
		env     environmentSetting
		wantErr bool
	}{
		{
			environmentSetting{},
			true,
		},
		{
			environmentSetting{dev: true},
			false,
		},
		{
			environmentSetting{staging: true},
			false,
		},
		{
			environmentSetting{prod: true},
			false,
		},
		{
			environmentSetting{dev: true, staging: true},
			true,
		},
		{
			environmentSetting{dev: true, staging: true, prod: true},
			true,
		},
		{
			environmentSetting{dev: true, prod: true},
			true,
		},
		{
			environmentSetting{staging: true, prod: true},
			true,
		},
	}

	for _, tt := range testCases {
		err := tt.env.validate()
		if (err != nil && !tt.wantErr) || (err == nil && tt.wantErr) {
			t.Fail()
		}
	}
}

func TestEnvironmentSettingPrefix(t *testing.T) {
	testCases := []struct {
		env  environmentSetting
		want string
	}{
		{
			environmentSetting{dev: true},
			"dev",
		},
		{
			environmentSetting{staging: true},
			"staging",
		},
		{
			environmentSetting{prod: true},
			"prod",
		},
	}

	for _, tt := range testCases {
		got := tt.env.getEnvPrefix()
		if got != tt.want {
			t.Fail()
		}
	}
}
