package main

import "code.justin.tv/commerce/fortuna/cmd/_scripts/chanprops/cmd"

type command string

func main() {
	cmd.RootCmd.Execute()
}
