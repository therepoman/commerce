package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"io"
	"os"
	"strings"
	"time"

	"net/http"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/model"
)

// Instructions:
// 1. Update `domain` in this file to correct domain.
// 2. Update `filename` in this file to correct file.
// 3. Change the milestone end dates in campaign config to a future date before fulfilling rewards. Remember to upload the campaign to S3/dynamo.
// 4. Test changes with staff users before running script on all users.
// 5. Alert TFS team and other downstream services.

const (
	stagingFortunaBaseURL = "https://main.us-west-2.beta.fortuna.twitch.a2z.com"
	prodFortunaBaseURL    = "https://main.us-west-2.prod.fortuna.twitch.a2z.com"
	apiEndpoint           = "/twirp/code.justin.tv.commerce.fortuna.Fortuna/ClaimAllRewards"
)

var (
	env      = flag.String("env", "prod", "environment to run in")
	domain   = flag.String("domain", "", "domain to claim rewards for")
	filename = ""
)

// Script used for claiming rewards for users for a given campaign. Will go through and for each user in the given file location will call ClaimAlLRewards
// for the given id. Requests will be throttled by waiting 50ms between each request.
// The file passed in should be a csv file that contains a list of ids, one for each line.
// Not that in order for a reward to be redeemed, the active and visible dates for reward's milestone will need to be in the future.
func main() {
	var baseURL string
	switch *env {
	case "prod", "production":
		baseURL = prodFortunaBaseURL
	case "stg", "staging":
		baseURL = stagingFortunaBaseURL
	case "dev", "devo", "development":
		fallthrough
	default:
		baseURL = stagingFortunaBaseURL
	}
	apiURL := baseURL + apiEndpoint

	log.Infof("Starting redrive to %s", apiURL)

	// File should have the list of userIDs, one per line
	csvFile, err := os.Open(filename)
	if err != nil {
		log.Fatal("Couldn't open file", err)
	}
	reader := csv.NewReader(bufio.NewReader(csvFile))

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal("Couldn't read line", err)
		}

		claimAllRewardsRequest := model.ClaimAllRewardsRequest{
			Domain: *domain,
			UserID: strings.Trim(line[0], " "),
		}

		contextLogger := log.WithField("row", claimAllRewardsRequest)
		log.Debug(strings.Trim(line[0], " "))

		requestJSON, err := json.Marshal(claimAllRewardsRequest)
		if err != nil {
			contextLogger.Fatal("Error marshaling row to json", err)
		}

		resp, err := http.Post(apiURL, "application/json", bytes.NewReader(requestJSON))
		if err != nil {
			contextLogger.Fatal("Post error", err)
		}

		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		bodyString := buf.String()

		if resp.StatusCode == 200 {
			log.WithFields(log.Fields{
				"row":    claimAllRewardsRequest,
				"status": resp.Status,
				"body":   bodyString,
			}).Info("Processed row")
		} else {
			log.WithFields(log.Fields{
				"row":    claimAllRewardsRequest,
				"status": resp.Status,
				"body":   bodyString,
			}).Errorf("Non 200 response", err)
		}

		resp.Body.Close()

		time.Sleep(50 * time.Millisecond)
	}

	log.Infof("Redrive complete")
}
