package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/model"
)

func main() {
	csvFile, _ := os.Open("cmd/_scripts/data/redshift.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var bitsLog []model.BitsLog

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		csvEmoteUses := line[11]
		emoteUses := &model.EmoteUses{}

		err = json.Unmarshal([]byte(csvEmoteUses), emoteUses)
		if err != nil {
			log.Fatal(err)
		}

		bitsLog = append(bitsLog, model.BitsLog{
			EventID:   line[0],
			Timestamp: line[2],
			UserID:    line[3],
			ChannelID: line[4],
			BitsUsed:  line[5],
			Message:   line[8],
			EmoteUses: emoteUses.EmoteUses,
		})
	}

	peopleJson, _ := json.MarshalIndent(bitsLog, "", "  ")
	log.Info(string(peopleJson))
	// Creating a backup JSON file for user data
	_ = ioutil.WriteFile("cmd/_scripts/data/sample_bits_events.json", peopleJson, 0644)
}
