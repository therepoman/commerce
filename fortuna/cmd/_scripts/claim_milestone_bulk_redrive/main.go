package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"net/http"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/fortuna/cmd/_scripts/model"
)

const (
	stagingFortunaBaseURL = "https://main.us-west-2.beta.fortuna.twitch.a2z.com"
	prodFortunaBaseURL    = "https://main.us-west-2.prod.fortuna.twitch.a2z.com"
	apiEndpoint           = "/twirp/code.justin.tv.commerce.fortuna.Fortuna/ClaimMilestone"
	debugAPIEndpoint      = "/twirp/code.justin.tv.commerce.fortuna.Fortuna/DebugClaimMilestone"
)

var (
	debug = flag.Bool("debug", false, "boolean flag for using DebugClaimMilestone instead of ClaimMilestone, which bypasses eligiblity checking logic")
	env   = flag.String("env", "dev", "environment to run in")
)

func main() {
	flag.Parse()

	var baseURL string
	switch *env {
	case "prod", "production":
		baseURL = prodFortunaBaseURL
	case "stg", "staging":
		baseURL = stagingFortunaBaseURL
	case "dev", "devo", "development":
		fallthrough
	default:
		baseURL = stagingFortunaBaseURL
	}

	var apiURL string
	if *debug {
		apiURL = baseURL + debugAPIEndpoint
	} else {
		apiURL = baseURL + apiEndpoint
	}

	log.Infof("Starting redrive to %s", apiURL)

	// File should be in the format of domain,milestoneID,userID
	csvFile, err := os.Open("cmd/_scripts/data/claim_milestone_bulk_redrive.csv")
	if err != nil {
		log.Fatal("Couldn't open file", err)
	}
	reader := csv.NewReader(bufio.NewReader(csvFile))

	resultsFile, err := os.Create("cmd/_scripts/data/claim_milestone_bulk_redrive_results.csv")
	if err != nil {
		log.Fatal("Couldn't create results file", err)
	}

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal("Couldn't read line", err)
		}

		milestoneToClaim := model.ClaimMilestoneRequest{
			Domain:      strings.Trim(line[0], " "),
			MilestoneID: strings.Trim(line[1], " "),
			UserID:      strings.Trim(line[2], " "),
		}

		contextLogger := log.WithField("row", milestoneToClaim)

		milestoneJSON, err := json.Marshal(milestoneToClaim)
		if err != nil {
			contextLogger.Fatal("Error marshaling row to json", err)
		}

		resp, err := http.Post(apiURL, "application/json", bytes.NewReader(milestoneJSON))
		if err != nil {
			contextLogger.Fatal("Post error", err)
		}
		if resp.StatusCode == 200 {
			contextLogger.Info("Processed row")
			resultsFile.WriteString(fmt.Sprintf("%s,SUCCESS\n", milestoneToClaim.UserID))
		} else {
			buf := new(bytes.Buffer)
			buf.ReadFrom(resp.Body)
			bodyString := buf.String()

			log.WithFields(log.Fields{
				"row":    milestoneToClaim,
				"status": resp.Status,
				"body":   bodyString,
			}).Error("Non 200 response", err)

			resultsFile.WriteString(fmt.Sprintf("%s,FAILURE\n", milestoneToClaim.UserID))
		}

		resp.Body.Close()

		time.Sleep(50 * time.Millisecond)
	}

	resultsFile.Close()

	log.Infof("Redrive complete")
}
