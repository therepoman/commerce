package commands

import (
	"github.com/spf13/cobra"
)

// RootCmd ...
var RootCmd = &cobra.Command{
	Use:   "config_controller <subcommand>",
	Short: "Command-line CRUD for managing Fortuna campaign configs",
	Long: `Command-line CRUD for Fortuna Campaign Configs, which managing cheering campaigns for various domains.
Most channels will associate with a single domain, which enables a custom campaigns UI on the channel page
and allows cheers (or other actions) in that channel to contribute progress toward the reward objectives
in that domain's campaign configuration.'`,
	Args: cobra.MinimumNArgs(1),
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		// Don't validate or do any work for help command
		if cmd.CalledAs() == "help" {
			return nil
		}

		return nil
	},
}

func init() {
	RootCmd.AddCommand(createConfigCmd)
	RootCmd.AddCommand(syncConfigCmd)
	RootCmd.AddCommand(syncAllConfigsCmd)
}
