package commands

import (
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var syncConfigCmd = &cobra.Command{
	Use:   "syncConfig <base_config_path> <config_path> [--skipIDs] [--skipChannelList] [--skipWhitelist]",
	Short: "Sync the existing config for the given config file.",
	Long:  `Sync the existing config for the given config file.`,
	Args:  cobra.MinimumNArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		baseConfigPath := args[0]
		configPath := args[1]

		if baseConfigPath == "" {
			return errors.New("sync must be passed a base_config_path argument")
		}

		if configPath == "" {
			return errors.New("sync must be passed a config_path argument")
		}

		skipIDs, err := cmd.Flags().GetBool("skipIDs")
		if err != nil {
			return err
		}

		skipChannelList, err := cmd.Flags().GetBool("skipChannelList")
		if err != nil {
			return err
		}

		skipWhitelist, err := cmd.Flags().GetBool("skipWhitelist")
		if err != nil {
			return err
		}

		baseData, err := readConfig(baseConfigPath)
		if err != nil {
			return err
		}

		configData, err := readConfig(configPath)
		if err != nil {
			return err
		}

		merged, err := syncConfigs(baseData, configData, SyncParams{
			skipIDs:         skipIDs,
			skipChannelList: skipChannelList,
			skipWhitelist:   skipWhitelist,
		})

		return writeConfigToFile(merged, configPath)
	},
}

func init() {
	syncConfigCmd.Flags().Bool("skipIDs", false, "Indicate that ids should not be synced")
	syncConfigCmd.Flags().Bool("skipChannelList", false, "Indicate that channels should not be synced")
	syncConfigCmd.Flags().Bool("skipWhitelist", false, "Indicate that whitelists should not be synced")
}
