package commands

import (
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"github.com/stretchr/testify/assert"
)

func TestReadConfigs(t *testing.T) {
	testCases := []struct {
		input         string
		expectedError error

		expectedOriginal campaign.Configuration
		expectedTest     campaign.Configuration
	}{
		{
			input:         "./hearthstone2018.json",
			expectedError: nil,

			expectedOriginal: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									BadgeSetIds: map[string]bool{
										"testBadgeId": true,
									},
									DiscoveryEventType: "COMMAND_CENTER",
									Preference:         "dal",
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
											RequiredSubs: []string{
												"AAPOnly",
											},
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			expectedTest: campaign.Configuration{},
		},
	}

	for _, tt := range testCases {
		originalData, err := readConfig(tt.input)
		assert.Equal(t, tt.expectedError, err)
		assert.Equal(t, tt.expectedOriginal, originalData)
	}
}

func TestSyncConfigs(t *testing.T) {
	testCases := []struct {
		originalConfig campaign.Configuration
		testConfig     campaign.Configuration
		params         SyncParams
		expectedError  error

		expectedMerged campaign.Configuration
	}{
		{
			originalConfig: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			testConfig: campaign.Configuration{},
			params: SyncParams{
				isTest: true,
			},
			expectedError: nil,

			expectedMerged: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018-test": {
						&campaign.Campaign{
							ID:              "hearthstone2018-test",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018-test",
							Title:           "Hearthstone Global Games 2018-test",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual-test",
									Description:     "Hearthstone Global Games 2018 Cheer Individual-test",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual-test",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1-test",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global-test",
									Description:     "Hearthstone Global Games 2018 Cheer Global-test",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global-test",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1-test",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			originalConfig: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			testConfig: campaign.Configuration{},
			params: SyncParams{
				isTest: false,
			},
			expectedError: nil,

			expectedMerged: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			originalConfig: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone2",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone2",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			testConfig: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			params: SyncParams{
				isTest: false,
			},
			expectedError: nil,

			expectedMerged: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone2",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone2",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			originalConfig: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"42776357": true,
							},
							UserWhitelistEnabled: false,
							UserWhitelist: map[string]bool{
								"184009112": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone2",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone2",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			testConfig: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"1234": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"12345": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},

			params: SyncParams{
				isTest:          false,
				skipChannelList: true,
				skipWhitelist:   true,
			},
			expectedError: nil,

			expectedMerged: campaign.Configuration{
				Campaigns: map[string][]*campaign.Campaign{
					"hearthstone2018": {
						&campaign.Campaign{
							ID:              "hearthstone2018",
							ActiveStartDate: time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
							ActiveEndDate:   time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
							Description:     "Hearthstone Global Games 2018",
							Title:           "Hearthstone Global Games 2018",
							ChannelList: map[string]bool{
								"1234": true,
							},
							UserWhitelistEnabled: true,
							UserWhitelist: map[string]bool{
								"12345": true,
							},
							Objectives: []*campaign.Objective{
								{
									ID:              "hearthstone2018.launch.cheer.individual",
									Description:     "Hearthstone Global Games 2018 Cheer Individual",
									EventHandler:    "IndividualCheerHandler",
									Tag:             "INDIVIDUAL",
									Title:           "Hearthstone Global Games 2018 Cheer Individual",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone1",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
										{
											ID:      "hearthstone2018.launch.cheer.individual.milestone2",
											Handler: "MileStone1Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description: "Common Emote Pack",
														ImageURL:    "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/HGG-emote-Set1.png",
														Type:        "emote",
														ID:          "1119792",
														Code:        "HS18Sorry",
														Name:        "Common Emote Pack",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              300,
											ParticipationThreshold: 300,
										},
									},
								},
								{
									ID:              "hearthstone2018.launch.cheer.global",
									Description:     "Hearthstone Global Games 2018 Cheer Global",
									EventHandler:    "GlobalCheerHandler",
									Title:           "Hearthstone Global Games 2018 Cheer Global",
									Tag:             "GLOBAL",
									ShouldAutogrant: true,
									Milestones: []*campaign.Milestone{
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone1",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
										{
											ID:      "hearthstone2018.launch.cheer.global.milestone2",
											Handler: "MileStone31Handler",
											HandlerMetadata: campaign.HandlerMetadata{
												SelectionStrategy: "ALL",
												Rewards: []*campaign.Reward{
													{
														Description:         "2 Boomsday Packs",
														ImageURL:            "https://d3aqoihi2n8ty8.cloudfront.net/hearthstone2018/rewards/boomsday_pack.png",
														Type:                "igc",
														ID:                  "hearthstone2018-launch-cheer-global-milestone1-reward",
														SKU:                 "rewards-cheering-hgg-2-bommsdaypacks",
														Name:                "2 Boomsday Packs",
														FulfillmentStrategy: "blizzard",
													},
												},
											},
											ActiveStartDate:        time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											ActiveEndDate:          time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											VisibleStartDate:       time.Date(2018, time.September, 17, 16, 0, 0, 0, time.UTC),
											VisibleEndDate:         time.Date(2018, time.November, 05, 18, 0, 0, 0, time.UTC),
											Threshold:              8000000,
											ParticipationThreshold: 700,
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}

	for _, tt := range testCases {
		mergedConfig, err := syncConfigs(tt.originalConfig, tt.testConfig, tt.params)
		assert.Equal(t, tt.expectedError, err)
		assert.Equal(t, tt.expectedMerged, mergedConfig)
	}
}
