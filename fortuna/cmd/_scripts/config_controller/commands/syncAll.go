package commands

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"code.justin.tv/commerce/fortuna/campaign"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var syncAllConfigsCmd = &cobra.Command{
	Use:   "syncAllConfigs <base_config_file> [--skipIDs] [--skipChannelList] [--skipWhitelist]",
	Short: "Sync the existing configs for all environments for the given domain file. Create configs if they do not exist",
	Long:  `Sync the existing configs for all environments for the given domain file. Create configs if they do not exist. params can be passed in to preserve values in child configs.`,
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		baseConfigFile := args[0]

		if baseConfigFile == "" {
			return errors.New("syncTest must be passed a base_config_file argument")
		}

		skipIDs, err := cmd.Flags().GetBool("skipIDs")
		if err != nil {
			return err
		}

		skipChannelList, err := cmd.Flags().GetBool("skipChannelList")
		if err != nil {
			return err
		}

		skipWhitelist, err := cmd.Flags().GetBool("skipWhitelist")
		if err != nil {
			return err
		}

		return syncAllConfigs(baseConfigFile, skipIDs, skipChannelList, skipWhitelist)
	},
}

func init() {
	syncAllConfigsCmd.Flags().Bool("skipIDs", false, "Indicate that ids should not be synced")
	syncAllConfigsCmd.Flags().Bool("skipChannelList", false, "Indicate that channels should not be synced")
	syncAllConfigsCmd.Flags().Bool("skipWhitelist", false, "Indicate that whitelists should not be synced")
}

func syncAllConfigs(filePath string, skipIDs bool, skipChannelList bool, skipWhitelist bool) error {
	baseData, err := readConfig(filePath)
	if err != nil {
		return err
	}

	files, err := getExpectedConfigs(filePath)
	if err != nil {
		return err
	}

	for _, config := range files {
		var params SyncParams
		var configData campaign.Configuration

		if _, err := os.Stat(config.fileName); os.IsNotExist(err) {
			fmt.Printf("%s does not exist, creating\n", config.fileName)

			params = SyncParams{
				isTest: config.isTest,
			}
		} else {
			configData, err = readConfig(config.fileName)
			if err != nil {
				return err
			}

			params = SyncParams{
				skipIDs:         skipIDs,
				skipChannelList: skipChannelList,
				skipWhitelist:   skipWhitelist,
				isTest:          config.isTest,
			}
		}

		merged, err := syncConfigs(baseData, configData, params)
		err = writeConfigToFile(merged, config.fileName)

		if err != nil {
			return err
		}
	}

	return nil
}

type Config struct {
	fileName string
	isTest   bool
}

func getExpectedConfigs(filePath string) ([]Config, error) {
	environments := []string{
		"prod",
		"staging",
		"dev",
	}

	fullPath, err := filepath.Abs(filePath)
	if err != nil {
		return nil, err
	}

	folder := filepath.Dir(fullPath)
	index := strings.LastIndex(folder, "/")

	baseFolder := folder[:index]
	configName := filepath.Base(fullPath)

	var allConfigs []Config

	for _, env := range environments {
		allConfigs = append(allConfigs, Config{
			fileName: getFilePath(baseFolder, env, configName),
			isTest:   false,
		})

		extension := filepath.Ext(configName)
		name := configName[0 : len(configName)-len(extension)]
		testName := fmt.Sprintf("%s-test%s", name, extension)

		allConfigs = append(allConfigs, Config{
			fileName: getFilePath(baseFolder, env, testName),
			isTest:   true,
		})
	}

	return allConfigs, nil
}

func getFilePath(baseFolder string, environment string, fileName string) string {
	return filepath.Join(baseFolder, environment, fileName)
}
