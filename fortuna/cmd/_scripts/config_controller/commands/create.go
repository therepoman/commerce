package commands

import (
	"fmt"
	"os"

	"code.justin.tv/commerce/fortuna/campaign"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var createConfigCmd = &cobra.Command{
	Use:   "createConfig <base_config_path> <config_path> [--createTest]",
	Short: "Create a new test config for the given config file.",
	Long:  `Create a new config for the given config file at the given path. Optionally pass in if a test config should be created.`,
	Args:  cobra.MinimumNArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		baseConfigPath := args[0]
		configPath := args[1]

		createTest, err := cmd.Flags().GetBool("createTest")
		if err != nil {
			return err
		}

		if baseConfigPath == "" {
			return errors.New("syncTest must be passed a base_config_path argument")
		}

		if configPath == "" {
			return errors.New("syncTest must be passed a config_path argument")
		}

		baseData, err := readConfig(baseConfigPath)
		if err != nil {
			return err
		}

		if _, err := os.Stat(configPath); err == nil {
			fmt.Printf("%s already exists", configPath)
			return nil
		}

		var testData campaign.Configuration
		merged, err := syncConfigs(baseData, testData, SyncParams{
			isTest: createTest,
		})

		return writeConfigToFile(merged, configPath)
	},
}

func init() {
	createConfigCmd.Flags().Bool("createTest", false, "Indicate that the create config should be a test config")
}
