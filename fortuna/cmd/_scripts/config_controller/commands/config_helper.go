package commands

import (
	"encoding/json"
	"fmt"
	"path/filepath"

	"code.justin.tv/commerce/fortuna/campaign"

	"io/ioutil"
	"os"

	"github.com/pkg/errors"
)

// SyncParams defines parameters for syncing between configs
type SyncParams struct {
	isTest          bool
	skipIDs         bool
	skipChannelList bool
	skipWhitelist   bool
}

func readConfig(configPath string) (campaign.Configuration, error) {
	file, err := ioutil.ReadFile(configPath)
	if err != nil {
		return campaign.Configuration{}, errors.Wrapf(err, "error reading %v", file)
	}

	var data campaign.Configuration
	if err := json.Unmarshal(file, &data); err != nil {
		return campaign.Configuration{}, err
	}

	return data, nil
}

func syncConfigs(base campaign.Configuration, test campaign.Configuration, params SyncParams) (campaign.Configuration, error) {
	var merged campaign.Configuration
	merged.Campaigns = make(map[string][]*campaign.Campaign)

	for key := range base.Campaigns {
		baseCampaigns := base.Campaigns[key]
		campaigns := test.Campaigns[getKey(key, params.isTest)]

		length := len(baseCampaigns)
		mergedCampaigns := make([]*campaign.Campaign, length)

		for index, baseCampaign := range baseCampaigns {
			var mergedCampaign campaign.Campaign

			var campaign campaign.Campaign
			if len(campaigns) > index {
				campaign = *campaigns[index]
			}

			mergedCampaign.ActiveStartDate = baseCampaign.ActiveStartDate
			mergedCampaign.ActiveEndDate = baseCampaign.ActiveEndDate
			mergedCampaign.AllChannels = baseCampaign.AllChannels

			if params.skipChannelList {
				mergedCampaign.ChannelList = campaign.ChannelList
			} else {
				mergedCampaign.ChannelList = baseCampaign.ChannelList
			}

			mergedCampaign.CheerGroups = baseCampaign.CheerGroups
			mergedCampaign.Description = formatString(baseCampaign.Description, campaign.Description, params.isTest, false)
			mergedCampaign.ExcludedChannelList = baseCampaign.ExcludedChannelList
			mergedCampaign.ID = formatString(baseCampaign.ID, campaign.ID, params.isTest, params.skipIDs)

			mergedCampaign.Objectives = syncObjectives(baseCampaign.Objectives, campaign.Objectives, params)

			mergedCampaign.StaffOnly = baseCampaign.StaffOnly
			mergedCampaign.Title = formatString(baseCampaign.Title, campaign.Title, params.isTest, false)

			mergedCampaign.Triggers = baseCampaign.Triggers

			if params.skipWhitelist {
				mergedCampaign.UserWhitelist = campaign.UserWhitelist
				mergedCampaign.UserWhitelistEnabled = campaign.UserWhitelistEnabled
			} else {
				mergedCampaign.UserWhitelist = baseCampaign.UserWhitelist
				mergedCampaign.UserWhitelistEnabled = baseCampaign.UserWhitelistEnabled
			}

			mergedCampaigns[index] = &mergedCampaign
		}

		merged.Campaigns[getKey(key, params.isTest)] = mergedCampaigns
	}

	return merged, nil
}

func syncObjectives(baseObjectives []*campaign.Objective, objectives []*campaign.Objective, params SyncParams) []*campaign.Objective {
	mergedObjectives := make([]*campaign.Objective, len(baseObjectives))
	for index, baseObjective := range baseObjectives {

		var objective campaign.Objective
		if len(objectives) > index {
			objective = *objectives[index]
		}

		var mergedObjective campaign.Objective

		mergedObjective.CampaignId = baseObjective.CampaignId
		mergedObjective.CheerGroupKey = baseObjective.CheerGroupKey
		mergedObjective.Deprecated = baseObjective.Deprecated
		mergedObjective.Description = formatString(baseObjective.Description, objective.Description, params.isTest, false)
		mergedObjective.EventHandler = baseObjective.EventHandler
		mergedObjective.ID = formatString(baseObjective.ID, objective.ID, params.isTest, params.skipIDs)
		mergedObjective.Milestones = syncMilestones(baseObjective.Milestones, objective.Milestones, params)
		mergedObjective.RandomRewards = baseObjective.RandomRewards
		mergedObjective.ShouldAutogrant = baseObjective.ShouldAutogrant
		mergedObjective.Tag = baseObjective.Tag
		mergedObjective.Title = formatString(baseObjective.Title, objective.Title, params.isTest, false)
		mergedObjective.ContributingMilestoneIds = baseObjective.ContributingMilestoneIds
		mergedObjective.BadgeSetIds = baseObjective.BadgeSetIds
		mergedObjective.DiscoveryEventType = baseObjective.DiscoveryEventType
		mergedObjective.Preference = baseObjective.Preference
		mergedObjective.DisablePubSub = baseObjective.DisablePubSub

		if params.skipWhitelist {
			mergedObjective.UserWhitelist = objective.UserWhitelist
			mergedObjective.UserWhitelistEnabled = objective.UserWhitelistEnabled
		} else {
			mergedObjective.UserWhitelist = baseObjective.UserWhitelist
			mergedObjective.UserWhitelistEnabled = baseObjective.UserWhitelistEnabled
		}

		mergedObjectives[index] = &mergedObjective
	}

	return mergedObjectives
}

func syncMilestones(baseMilestones []*campaign.Milestone, milestones []*campaign.Milestone, params SyncParams) []*campaign.Milestone {
	mergedMilestones := make([]*campaign.Milestone, len(baseMilestones))
	for index, baseMilestone := range baseMilestones {
		var milestone campaign.Milestone
		if len(milestones) > index {
			milestone = *milestones[index]
		}

		var mergedMilestone campaign.Milestone

		mergedMilestone.ActiveEndDate = baseMilestone.ActiveEndDate
		mergedMilestone.ActiveStartDate = baseMilestone.ActiveStartDate
		mergedMilestone.AutograntDisabled = baseMilestone.AutograntDisabled
		mergedMilestone.CheckEligibility = baseMilestone.CheckEligibility
		mergedMilestone.Handler = baseMilestone.Handler
		mergedMilestone.HandlerMetadata = baseMilestone.HandlerMetadata
		mergedMilestone.ID = formatString(baseMilestone.ID, milestone.ID, params.isTest, params.skipIDs)
		mergedMilestone.IsPrimeOnly = baseMilestone.IsPrimeOnly
		mergedMilestone.RequiredSubs = baseMilestone.RequiredSubs
		mergedMilestone.ParticipationThreshold = baseMilestone.ParticipationThreshold
		mergedMilestone.Threshold = baseMilestone.Threshold

		if params.skipWhitelist && milestone.UserWhiteListEnabled {
			mergedMilestone.UserWhitelist = milestone.UserWhitelist
			mergedMilestone.UserWhiteListEnabled = milestone.UserWhiteListEnabled
		} else {
			mergedMilestone.UserWhitelist = baseMilestone.UserWhitelist
			mergedMilestone.UserWhiteListEnabled = baseMilestone.UserWhiteListEnabled
		}

		mergedMilestone.VisibleEndDate = baseMilestone.VisibleEndDate
		mergedMilestone.VisibleStartDate = baseMilestone.VisibleStartDate

		mergedMilestones[index] = &mergedMilestone
	}

	return mergedMilestones
}

func formatTest(base string) string {
	if base != "" {
		return fmt.Sprintf("%s-test", base)
	}

	return base
}

func formatString(base string, config string, test bool, skipFormat bool) string {
	if test {
		return formatTest(base)
	} else if skipFormat && config != "" {
		return config
	}

	return base
}

func getKey(base string, isTest bool) string {
	if isTest {
		return formatTest(base)
	}

	return base
}

func writeConfigToFile(config campaign.Configuration, fileName string) error {
	out, err := json.MarshalIndent(config, "", "  ")
	if err != nil {
		return err
	}

	err = writeFile(fileName, out)
	if err != nil {
		return err
	}

	return nil
}

func writeFile(filePath string, data []byte) error {

	if _, err := os.Stat(filepath.Dir(filePath)); os.IsNotExist(err) {
		os.Mkdir(filepath.Dir(filePath), os.ModePerm)
	}

	if _, err := os.Stat(filePath); err == nil {
		os.Remove(filePath)
		if err != nil {
			return err
		}
	}

	newFile, err := os.Create(filePath)
	if err != nil {
		return err
	}
	newFile.Close()

	err = ioutil.WriteFile(filePath, data, 0666)
	if err != nil {
		return err
	}

	return nil
}
