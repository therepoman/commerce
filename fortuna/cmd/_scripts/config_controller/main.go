package main

import "code.justin.tv/commerce/fortuna/cmd/_scripts/config_controller/commands"

type command string

func main() {
	commands.RootCmd.Execute()
}
