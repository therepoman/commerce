#config_controller

## Introduction
`package _scripts/config_controller` is a command-line utility that automates the syncing of configs. We have found that the syncing of configuration data between configs of different environments and test configurations to be tedious and this script automates the process.

## Overview
`config_controller` is run from the command line. By passing in a base config, other configs will be created and/or synced based up the command run.

## Running the command
Usage:
  config_controller [command]

Available Commands:
  createConfig   Create a new test config for the given config file.
  help           Help about any command
  syncAllConfigs Sync the existing configs for all environments for the given domain file. Create configs if they do not exist
  syncConfig     Sync the existing config for the given config file.

`createConfig` create a new config for the given config file at the given path. Optionally pass in if a test config should be created.
`syncAllConfigs` syncs the existing configs for all environments for the given domain file. It will also configs for the environments and test configs if they do not exist. Params can be passed in to preserve values in child configs if the user does not want those values to be sync. This commands assume the configs will be the structure:
    \dev
    \prod
    \staging
And that the config file passed in will exist in one of those sub-folders.
`syncConfig` syncs the given config with a given base config, with optional parameters to preserve values if the user does not want them synced.
