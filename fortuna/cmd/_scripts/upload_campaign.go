package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/s3"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/pkg/errors"

	diff "github.com/yudai/gojsondiff"
	"github.com/yudai/gojsondiff/formatter"
)

const (
	organizationName = "commerce"
	serviceName      = "fortuna"

	localCampaignConfigPath = "%s/src/code.justin.tv/%s/%s/campaign/%s"

	normalExitCode   = 0
	abnormalExitCode = 1
)

var environments = map[string]bool{
	"dev":     true,
	"test":    true,
	"staging": true,
	"prod":    true,
}

var responses = map[string]bool{
	"y": true,
	"Y": true,
	"n": true,
	"N": true,
}

type Uploader struct {
	S3Client   s3.IS3Client
	FileName   string
	BucketName string
}

func main() {
	env := flag.String("env", "", "Environment you want to compile and upload campaign configuration for (Required)")
	dryrun := flag.Bool("dry", false, "Whether you just want to compile and not upload (Optional)")
	flag.Parse()

	environment := *env
	noUpload := *dryrun

	// Do not upload test.json
	if environment == "test" {
		noUpload = true
	}

	if *env == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	if !environments[*env] {
		fmt.Println("Invalid environment. Available environments: dev, test, staging, prod")
		os.Exit(1)
	}

	fmt.Printf("Loading configuration files for environment: %s \n", environment)

	// Load all json files from the env directory
	data, err := getLocalConfigFiles(environment)
	if err != nil {
		fmt.Printf("Error reading configuration files: %s", err)
		os.Exit(1)
	}

	// combine!
	finalData, err := combineJSONs(data)
	if err != nil {
		fmt.Printf("Error writing the combining files to single json %s", err)
		os.Exit(1)
	}

	// Output the file!
	path, err := writeJSONToFile(finalData, environment)
	if err != nil {
		fmt.Printf("Error writing the compiled json to file %s", err)
		os.Exit(1)
	}

	// Validate the produced JSON
	configOptions := &config.Options{
		Environment:       environment,
		LocalPathOverride: path,
	}

	configuration := &campaign.Configuration{}
	if err := config.LoadConfigForEnvironment(configuration, configOptions); err != nil {
		fmt.Printf("Error validating compiled config: %s", err)
		cleanupAndExit(path, abnormalExitCode)
	}

	fmt.Println("Compile success!")

	if noUpload {
		fmt.Println("Uploading skipped")
		os.Exit(0)
	}

	// Moving on to upload
	compiledData, err := getCompiledConfigFile(path)
	if err != nil {
		fmt.Printf("Error reading configuration file: %s", err)
		cleanupAndExit(path, abnormalExitCode)
	}

	uploader, exists, err := newUploader(environment)
	if err != nil {
		fmt.Printf("Error creating S3 client: %s", err)
		cleanupAndExit(path, abnormalExitCode)
	}

	if exists {
		s3File, err := uploader.fetchFromS3()
		if err != nil {
			fmt.Printf("Error fetching file from S3 %s/%s: %s", uploader.BucketName, uploader.FileName, err)
			cleanupAndExit(path, abnormalExitCode)
		}

		// Show File diff between S3 and local version
		diffString, err := getFileDiff(s3File, compiledData)
		if err != nil {
			fmt.Println(err)
			cleanupAndExit(path, abnormalExitCode)
		}

		if diffString == "" {
			fmt.Println("No changes found, exiting...")
			cleanupAndExit(path, normalExitCode)
		}
		fmt.Print(diffString)
	}

	shouldUpload := confirm()
	if !shouldUpload {
		cleanupAndExit(path, normalExitCode)
	}

	fileLocation, err := uploader.uploadToS3(environment, compiledData)
	if err != nil {
		fmt.Printf("Error uploading to S3 %s/%s: %s", uploader.BucketName, uploader.FileName, err)
		cleanupAndExit(path, abnormalExitCode)
	}

	fmt.Printf("Successfully uploaded campaign configuration to %s.", fileLocation)
	cleanupAndExit(path, normalExitCode)
}

func confirm() bool {
	prompt := fmt.Sprintf("%s %s|[%s]: ", "Continue uploading configuration?", "y", "n")
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(prompt)
		answer, _ := reader.ReadString('\n')
		answer = strings.TrimSpace(answer)
		if answer == "" {
			return false
		}
		if !responses[answer] {
			fmt.Println("Please enter y or n")
			continue
		}
		if answer == "y" || answer == "Y" {
			return true
		}
		if answer == "n" || answer == "N" {
			return false
		}
	}
}

func getCompiledConfigFile(path string) ([]byte, error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, err
	}

	return ioutil.ReadFile(path)
}

func newUploader(env string) (*Uploader, bool, error) {
	awsProfileTemplate := "twitch-fortuna-%s"

	var awsProfile string
	if env == "prod" {
		awsProfile = fmt.Sprintf(awsProfileTemplate, "aws")
	} else {
		awsProfile = fmt.Sprintf(awsProfileTemplate, "dev")
	}

	fileName := fmt.Sprintf("config/%s.json", env)
	bucketName := fmt.Sprintf("fortuna-%s-campaigns", env)

	// Set up S3 client with proper AWS_PROFILE for the given environment
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Profile: awsProfile,
	}))
	s3Client := s3.New(sess, s3.NewDefaultConfig())

	if exists, err := s3Client.BucketExists(bucketName); !exists || err != nil {
		if err != nil {
			return nil, false, fmt.Errorf("bucket does not exist for Campaign object creation: %+v", err)
		}
		return nil, false, fmt.Errorf("bucket %s does not exist for campaignConfig creation", bucketName)
	}

	exists, err := s3Client.FileExists(bucketName, fileName)
	if err != nil {
		return nil, false, fmt.Errorf("could not check if file exists: %s / %s", bucketName, fileName)
	}

	return &Uploader{
		S3Client:   s3Client,
		FileName:   fileName,
		BucketName: bucketName,
	}, exists, nil
}

func (u *Uploader) fetchFromS3() ([]byte, error) {
	return u.S3Client.LoadFile(u.BucketName, u.FileName)
}

func (u *Uploader) uploadToS3(env string, file []byte) (string, error) {
	if err := u.S3Client.UploadFileToBucket(u.BucketName, u.FileName, file); err != nil {
		return "", err
	}
	return fmt.Sprintf("%s/%s", u.BucketName, u.FileName), nil
}

func getFileDiff(fileA []byte, fileB []byte) (string, error) {
	differ := diff.New()
	d, err := differ.Compare(fileA, fileB)
	if err != nil {
		return "", fmt.Errorf("Failed to unmarshal file: %s\n", err.Error())
	}

	var diffString string
	if d.Modified() {
		var aJson map[string]interface{}
		json.Unmarshal(fileA, &aJson)

		cfg := formatter.AsciiFormatterConfig{
			ShowArrayIndex: true,
			Coloring:       true,
		}

		format := formatter.NewAsciiFormatter(aJson, cfg)
		diffString, _ = format.Format(d)
	}

	return diffString, nil
}

/*
/ Compile Related Functions
*/

func getLocalConfigFiles(env string) ([][]byte, error) {
	dirName := fmt.Sprintf(localCampaignConfigPath, os.Getenv("GOPATH"), organizationName, serviceName, env)
	files, err := ioutil.ReadDir(dirName)

	if err != nil {
		return nil, err
	}

	if len(files) == 0 {
		return nil, fmt.Errorf("No files found in the config directory for %s", env)
	}

	var result [][]byte

	for _, f := range files {
		if f.IsDir() || !isJSONFile(f.Name()) {
			continue
		}

		fmt.Println("Loaded " + f.Name())
		filePath := fmt.Sprintf("%s/%s", dirName, f.Name())
		file, err := ioutil.ReadFile(filePath)

		if err != nil {
			return nil, errors.Wrapf(err, "error reading %v", f.Name())
		}

		result = append(result, file)
	}

	return result, nil
}

func combineJSONs(rawFiles [][]byte) (interface{}, error) {
	var JSONs []map[string]interface{}
	for _, raw := range rawFiles {
		var aJSON map[string]interface{}
		if err := json.Unmarshal(raw, &aJSON); err != nil {
			return nil, err
		}
		JSONs = append(JSONs, aJSON)
	}

	firstJSON := JSONs[0]

	if len(JSONs) == 1 {
		return firstJSON, nil
	}

	for i := 1; i < len(JSONs); i++ {
		mergeRecursive(firstJSON, JSONs[i])
	}

	return firstJSON, nil
}

// Slow and dumb way to merge json data...
func mergeRecursive(x1, x2 interface{}) interface{} {
	switch x1 := x1.(type) {
	case []interface{}:
		x2, ok := x2.([]interface{})

		if !ok {
			return x1
		}
		for _, v2 := range x2 {
			x1 = appendOrMerge(x1, v2)
		}
		return x1
	case map[string]interface{}:
		x2, ok := x2.(map[string]interface{})
		if !ok {
			return x1
		}
		for k, v2 := range x2 {
			if v1, ok := x1[k]; ok {
				x1[k] = mergeRecursive(v1, v2)
			} else {
				x1[k] = v2
			}
		}
	case nil:
		x2, ok := x2.(map[string]interface{})
		if ok {
			return x2
		}
	}
	return x1
}

// If the second argument is an object and the array x1 has an object with the same id, merge, otherwise append.
func appendOrMerge(x1 []interface{}, v2 interface{}) []interface{} {
	for k1, v1 := range x1 {
		switch v1 := v1.(type) {
		case map[string]interface{}:
			id1, ok1 := v1["Id"]
			id2, ok2 := v2.(map[string]interface{})["Id"]
			if ok1 && ok2 && id1 == id2 {
				x1[k1] = mergeRecursive(v1, v2)
				return x1
			}
		}
	}
	return append(x1, v2)
}

func writeJSONToFile(data interface{}, env string) (string, error) {
	fileName := env + ".json"
	filePath := fmt.Sprintf(localCampaignConfigPath, os.Getenv("GOPATH"), organizationName, serviceName, fileName)
	JSON, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return "", err
	}

	fmt.Printf("Writing to %s \n", filePath)

	return filePath, ioutil.WriteFile(filePath, JSON, 0666)
}

func isJSONFile(name string) bool {
	return strings.HasSuffix(name, ".json")
}

func cleanupAndExit(path string, exitCode int) {
	fmt.Print("Deleting the compiled file for clean up")
	if err := os.Remove(path); err != nil {
		fmt.Printf("Error deleting the uploaded file from the local storage: %s", err)
	}
	os.Exit(exitCode)
}
