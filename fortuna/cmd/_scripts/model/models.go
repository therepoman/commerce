package model

type BitsLog struct {
	EventID   string           `json:"eventID"`
	Timestamp string           `json:"timestamp"`
	UserID    string           `json:"userID"`
	ChannelID string           `json:"channelID"`
	BitsUsed  string           `json:"bitsUsed"`
	Message   string           `json:"message"`
	EmoteUses map[string]int64 `json:"emoteUses"`
}

type EmoteUses struct {
	EmoteUses map[string]int64
}

type UserCheer struct {
	TeamTotals map[string]int64 `json:"teamTotals"`
}

type ClaimMilestoneRequest struct {
	Domain      string `json:"domain"`
	MilestoneID string `json:"milestone_id"`
	UserID      string `json:"user_id"`
}

type ClaimAllRewardsRequest struct {
	UserID string `json:"user_id"`
	Domain string `json:"domain"`
}

type GetChannelSubscriptionsRequest struct {
	ChannelID string `json:"channel_id"`
	Limit     int    `json:"limit"`
	Offset    int    `json:"offset"`
}

type GetChannelSubscriptionsResponse struct {
	Subscriptions []*Subscription `json:"subscriptions"`
}

type Subscription struct {
	ID           string `json:"id"`
	ChannelID    string `json:"channel_id"`
	OwnerID      string `json:"owner_id"`
	ProductID    string `json:"product_id"`
	OriginID     string `json:"origin_id"`
	BenefitStart string `json:"benefit_start"`
	BenefitEnd   string `json:"benefit_end"`
}
