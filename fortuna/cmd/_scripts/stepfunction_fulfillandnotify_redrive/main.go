package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	fortunaSNS "code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/s3"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sns"
)

const (
	devQueueURL     = "https://sqs.us-west-2.amazonaws.com/106728537780/dev-megacheer-gift-fulfillment-events-sqs"
	stagingQueueURL = "https://sqs.us-west-2.amazonaws.com/106728537780/staging-megacheer-gift-fulfillment-events-sqs"
	prodQueueURL    = "arn:aws:sns:us-west-2:180265471281:prod-megacheer-gift-fulfillment-sns"

	devStateMachineArn     = "arn:aws:states:us-west-2:106728537780:stateMachine:dev-CheerbombGiftFulfillment-v4"
	stagingStateMachineArn = "arn:aws:states:us-west-2:106728537780:stateMachine:staging-CheerbombGiftFulfillment-v4"
	prodStateMachineArn    = "arn:aws:states:us-west-2:180265471281:stateMachine:prod-CheerbombGiftFulfillment-v4"

	devStepFncDocument     = "fortuna-dev-sfn-documents"
	stagingStepFncDocument = "fortuna-staging-sfn-documents"
	prodStepFncDocument    = "fortuna-prod-sfn-documents"
)

type FulfillAndNotifyRewardsInput struct {
	BitsEvent model.BitsEvent
	TriggerID string
	Logins    struct {
		BenefactorLogin string
		ChannelLogin    string
	}
	ChosenChatterIDsDocument string
	Domain                   string
}

const (
	defaultAwsProfile     = "twitch-fortuna-dev"
	defaultAwsRegion      = "us-west-2"
	anonymousCheererLogin = "AnAnonymousCheerer"
	defaultRateLimit      = 1000
)

var (
	env        = flag.String("env", "dev", "Environment to target")
	timeAfter  = flag.String("timeAfter", "", "Redrive all failed step-functions after this time")
	timeBefore = flag.String("timeBefore", "", "Redrive all failed step-functions after this time")
	rateLimit  = flag.Int("rateLimit", defaultRateLimit, "Redrive all failed step-functions after this time")
)

func main() {
	flag.Parse()

	var isProd bool
	var queueURL string
	var stateMachineARN string
	var stepFuncS3 string

	switch *env {
	case "prod", "production":
		isProd = true
		queueURL = prodQueueURL
		stateMachineARN = prodStateMachineArn
		stepFuncS3 = prodStepFncDocument
	case "stg", "staging":
		queueURL = stagingQueueURL
		stateMachineARN = stagingStateMachineArn
		stepFuncS3 = stagingStepFncDocument
	case "dev", "devo", "development":
		fallthrough
	default:
		queueURL = devQueueURL
		stateMachineARN = devStateMachineArn
		stepFuncS3 = devStepFncDocument
	}

	if *timeAfter == "" {
		log.Fatal("Must specify timeAfter")
	}

	if *timeBefore == "" {
		log.Fatal("Must specify timeBefore")
	}

	parsedTimeAfter, err := time.Parse(time.RFC3339, *timeAfter)
	if err != nil {
		log.Fatalf("Failed to parse time: %v", err)
	}

	parsedTimeBefore, err := time.Parse(time.RFC3339, *timeBefore)
	if err != nil {
		log.Fatalf("Failed to parse time: %v", err)
	}

	fmt.Printf("Preparing to send message to SQS queue at %v with parameters:\n", queueURL)
	fmt.Println("")
	fmt.Printf("timeAfter: %v\n", *timeAfter)
	fmt.Println("")
	fmt.Printf("timeBefore: %v\n", *timeBefore)
	fmt.Println("")

	if isProd && !confirmProd() {
		fmt.Println("Canceled sending event.")
		os.Exit(1)
	}

	if _, exists := os.LookupEnv("AWS_PROFILE"); !exists {
		fmt.Printf("Using AWS_PROFILE=%v\n", defaultAwsProfile)
		os.Setenv("AWS_PROFILE", defaultAwsProfile)
	}

	awsRegion := defaultAwsRegion
	if envRegion, exists := os.LookupEnv("AWS_DEFAULT_REGION"); exists {
		awsRegion = envRegion
	}

	fmt.Printf("Using AWS region: %v\n", awsRegion)
	fmt.Println("")

	s, err := session.NewSession()
	if err != nil {
		log.Fatalf("Failed to create AWS session: %v", err)
	}

	s3Client := s3.NewFromDefaultConfig()
	sfnClient := sfn.New(s, aws.NewConfig().WithRegion(awsRegion))

	fmt.Printf("Getting all failed executions after: %v\n", *timeAfter)
	executions := getFailedExecutions(stateMachineARN, parsedTimeAfter, parsedTimeBefore, sfnClient)

	fmt.Printf("Number of failed executions: %v\n", len(executions))
	fmt.Println("")

	fmt.Printf("Getting execution details....")
	fmt.Println("")
	executionDetails := getExecutionDetails(executions, sfnClient)

	if isProd && !confirmSend() {
		fmt.Println("Canceled sending event.")
		os.Exit(1)
	}

	snsClient := sns.New(s, aws.NewConfig().WithRegion(awsRegion))
	count := 0
	for _, triggerInput := range executionDetails {
		benefactorLogin := triggerInput.Logins.BenefactorLogin
		if triggerInput.BitsEvent.IsAnonymous {
			benefactorLogin = anonymousCheererLogin
		}

		// Get the chatters list from S3
		data, err := loadFileFromS3(s3Client, triggerInput.ChosenChatterIDsDocument, stepFuncS3)
		if err != nil {
			log.Fatal(err)
		}

		chatters, err := getChosenChatters(data)
		if err != nil {
			log.Fatal(err)
		}

		// For each recipient, send the gift fulfillment message
		for _, recipient := range chatters {
			giftEvent := fortunaSNS.MegaCheerGiftFulfillmentEvent{
				BenefactorLogin: benefactorLogin,
				RecipientID:     recipient,
				TriggerID:       triggerInput.TriggerID,
				Domain:          triggerInput.Domain,
				Event:           triggerInput.BitsEvent,
			}

			body, err := json.Marshal(giftEvent)
			if err != nil {
				log.Fatal(err)
			}
			bodyString := string(body)

			publishInput := &sns.PublishInput{
				Message:           aws.String(bodyString),
				TopicArn:          aws.String(queueURL),
				MessageAttributes: nil,
			}

			log.Printf("Message that will be sent %v:", publishInput)
			log.Printf("Sending event...\n")
			_, err = snsClient.Publish(publishInput)
			if err != nil {
				fmt.Printf("Error %v", err)
				fmt.Printf("!!!!!Failed to send message, do it manually %v!!!!", publishInput)
				fmt.Println("")
			}
			log.Println("Message successfully sent.")
			duration := time.Duration(*rateLimit)
			time.Sleep(duration * time.Millisecond)
		}

		count = count + 1
		log.Printf("%v out of %v executions done\n", count, len(executionDetails))
		fmt.Println("")
	}
}

func confirmProd() bool {
	prompt := "WARNING! You have chosen to send gift fulfillments events to PROD fulfillments events queue.\nEnter \"yes\" to confirm: "
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)

	answer, _ := reader.ReadString('\n')
	answer = strings.TrimSpace(strings.ToLower(answer))

	return answer == "yes"
}

func confirmSend() bool {
	prompt := "WARNING! You are redriving messages to the prod queue, are you sure?.\nEnter \"yes\" to confirm: "
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)

	answer, _ := reader.ReadString('\n')
	answer = strings.TrimSpace(strings.ToLower(answer))

	return answer == "yes"
}

func getFailedExecutions(stateMachineARN string, parsedTimeAfter time.Time, parsedTimeBefore time.Time, sfnClient *sfn.SFN) []*sfn.ExecutionListItem {
	var executions []*sfn.ExecutionListItem
	stop := false
	failedStatus := "FAILED"
	input := sfn.ListExecutionsInput{
		StateMachineArn: &stateMachineARN,
		StatusFilter:    &failedStatus,
	}

	for !stop {
		fmt.Printf("Getting all failed executions.....")
		fmt.Println("")
		response, err := sfnClient.ListExecutions(&input)

		if err != nil {
			log.Fatal(err)
		}

		for _, execution := range response.Executions {
			if execution.StartDate.After(parsedTimeAfter) && execution.StartDate.Before(parsedTimeBefore) {
				executions = append(executions, execution)
			}
		}

		if response.NextToken != nil {
			input.NextToken = response.NextToken
		} else {
			stop = true
		}
	}

	return executions
}

func getExecutionDetails(executions []*sfn.ExecutionListItem, sfnClient *sfn.SFN) []FulfillAndNotifyRewardsInput {
	var executionDetails []FulfillAndNotifyRewardsInput

	for _, execution := range executions {
		reverseOrder := true
		input := sfn.GetExecutionHistoryInput{
			ExecutionArn: execution.ExecutionArn,
			ReverseOrder: &reverseOrder,
		}

		details, err := sfnClient.GetExecutionHistory(&input)
		if err != nil {
			log.Fatalf("Failed to execution details: %v", err)
		}

		event := findLastFailedState(details.Events)
		if event == nil {
			fmt.Printf("!No failed execution for: %v\n!", *execution.Name)
			fmt.Println("")
		} else {
			// Fun fact, the failed activity state doesn't have the input. You need to go two before to the activity scheduled event.
			startedEvent := findActivityAtIndex(*event.Id-2, details.Events)
			if !strings.Contains(*startedEvent.ActivityScheduledEventDetails.Resource, "FulfillAndNotifyRewards") {
				fmt.Printf("!!!!!Found execution that did not fail on FulfillAndNotifyRewards, look into this manually %v!!!!", *execution.Name)
				fmt.Println("")
			} else {
				var triggerInput FulfillAndNotifyRewardsInput
				err := json.Unmarshal([]byte(*startedEvent.ActivityScheduledEventDetails.Input), &triggerInput)
				if err != nil {
					log.Fatal(err)
				}

				executionDetails = append(executionDetails, triggerInput)
			}
		}
	}

	return executionDetails
}

func loadFileFromS3(s3client s3.IS3Client, fileName string, stepFuncS3 string) ([]byte, error) {
	exists, err := s3client.BucketExists(stepFuncS3)
	if err != nil {
		return nil, err
	}

	if !exists {
		log.Fatal("Bucket does not exist")
	}

	exists, err = s3client.FileExists(stepFuncS3, fileName)
	if err != nil {
		return nil, err
	}

	if !exists {
		log.Fatal("File does not exist:" + fileName)
	}

	return s3client.LoadFile(stepFuncS3, fileName)
}

func getChosenChatters(data []byte) ([]string, error) {
	var chosenChatterIDs []string
	err := json.Unmarshal(data, &chosenChatterIDs)
	if err != nil {
		return nil, err
	}

	return chosenChatterIDs, nil
}

func findLastFailedState(events []*sfn.HistoryEvent) *sfn.HistoryEvent {
	for _, event := range events {
		if *event.Type == "ActivityFailed" {
			return event
		}
	}

	return nil
}

func findActivityAtIndex(id int64, events []*sfn.HistoryEvent) *sfn.HistoryEvent {
	for _, event := range events {
		if *event.Id == id {
			return event
		}
	}

	return nil
}
