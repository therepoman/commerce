package s2s

import (
	"fmt"

	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	log "github.com/sirupsen/logrus"
)

func Init(cfg *config.Configuration) (*callee.Client, error) {
	calleeConfig := callee.Config{
		SupportWildCard: true,
		PassthroughMode: !cfg.S2SEnabled,
	}

	eventsWriterClient, err := events.NewEventLogger(events.Config{}, log.New())

	if err != nil {
		log.Errorf("unable to create malachai events writer client: %v", err)
		return nil, err
	}

	s2sCalleeClient := callee.Client{
		ServiceName:        fmt.Sprintf("fortuna-%s", cfg.EnvironmentPrefix),
		EventsWriterClient: eventsWriterClient,
		Config:             &calleeConfig,
	}

	err = s2sCalleeClient.Start()
	if err != nil {
		log.WithError(err).Error("Failed to create S2S Client")
		return nil, err
	}

	return &s2sCalleeClient, nil
}
