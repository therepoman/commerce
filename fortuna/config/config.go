package config

import (
	"encoding/json"
	"fmt"
	"os"
	"path"

	"gopkg.in/validator.v2"
)

type Options struct {
	Environment        string
	LocalPathOverride  string
	GlobalPathOverride string
}

type Configuration struct {
	AWSRegion string `validate:"nonzero"`
	CacheSize int    `validate:"nonzero"`

	// Cloudwatch
	CloudwatchBufferSize               int     `validate:"nonzero"`
	CloudwatchBatchSize                int     `validate:"nonzero"`
	CloudwatchFlushIntervalSeconds     int     `validate:"nonzero"`
	CloudwatchFlushCheckDelayMs        int     `validate:"nonzero"`
	CloudwatchEmergencyFlushPercentage float64 `validate:"nonzero"`
	CloudwatchRegion                   string  `validate:"nonzero"`

	// Campaign Configuration
	CampaignBucket              string `validate:"nonzero"`
	CampaignRefreshDelaySeconds int    `validate:"nonzero"`

	// Step function configuration
	StepFnPollIntervalMs               int                          `validate:"nonzero"`
	CheerbombTriggerStepFn             CheerbombTriggerStepFnConfig `validate:"nonzero"`
	CheerbombFulfillmentMaxConcurrency int                          `validate:"nonzero"`
	CheerbombFulfillmentHeartbeatMs    int                          `validate:"nonzero"`
	CheerbombFulfillmentTimeoutSeconds int                          `validate:"nonzero"`
	StepFnDocumentS3Bucket             string                       `validate:"nonzero"`

	// EventBusURL
	EventBusURL string `validate:"nonzero"`

	// Rate limits for Reward dynamo table write requests
	// RewardDynamoWriteBurstLimit specifies the max number of immediate unthrottled writes are allowed before rate limit kicks in
	RewardDynamoWriteBurstLimit int `validate:"nonzero"`
	// RewardDynamoWriteRateLimit specifies the number of requests per second for writes/updates to the Rewards dynamo table (after burst limit is exceeded)
	RewardDynamoWriteRateLimit float64 `validate:"nonzero"`

	BadgeServiceMaxIdleConnections            int      `validate:"nonzero"`
	BadgeServiceEndpoint                      string   `validate:"nonzero"`
	BitsSQSEventQueueName                     string   `validate:"nonzero"`
	BitsSQSWorkerCount                        int      `validate:"nonzero"`
	ChattersHost                              string   `validate:"nonzero"`
	ChattersMaxIdleConnections                int      `validate:"nonzero"`
	ChattersTimeoutMilliseconds               int      `validate:"nonzero"`
	ConnectionsHost                           string   `validate:"nonzero"`
	EnvironmentPrefix                         string   `validate:"nonzero"`
	FollowsHost                               string   `validate:"nonzero"`
	FollowsMaxIdleConnections                 int      `validate:"nonzero"`
	FollowsTimeoutMilliseconds                int      `validate:"nonzero"`
	HolidayRewardList                         []string `validate:"nonzero"`
	InMemoryCacheExpiration                   int      `validate:"nonzero"`
	InMemoryCachePurgeDuration                int      `validate:"nonzero"`
	MakoEventsArn                             string   `validate:"nonzero"`
	MakoHost                                  string   `validate:"nonzero"`
	MakoMaxIdleConnections                    int      `validate:"nonzero"`
	NitroHost                                 string   `validate:"nonzero"`
	NitroMaxIdleConnections                   int      `validate:"nonzero"`
	PubSubEndpoint                            string   `validate:"nonzero"`
	PushyARN                                  string   `validate:"nonzero"`
	MegaCheerGiftFulfillmentSNSTopicARN       string   `validate:"nonzero"`
	MegaCheerGiftFulfillmentSQSEventQueueName string   `validate:"nonzero"`
	MegaCheerGiftFulfillmentSQSWorkerCount    int      `validate:"nonzero"`

	CampaignCacheTTLSeconds            int `validate:"nonzero"`
	CampaignAllChannelsCacheTTLSeconds int `validate:"nonzero"`

	RollbarToken                    string `validate:"nonzero"`
	SubscriptionsHost               string `validate:"nonzero"`
	SubscriptionsMaxIdleConnections int    `validate:"nonzero"`
	TMIHost                         string `validate:"nonzero"`
	UsersServiceHost                string `validate:"nonzero"`

	// Downstream Service Timeouts
	MakoTimeoutMilliseconds          int `validate:"nonzero"`
	PubsubTimeoutMilliseconds        int `validate:"nonzero"`
	NitroTimeoutMilliseconds         int `validate:"nonzero"`
	ConnectionsTimeoutMilliseconds   int `validate:"nonzero"`
	SubscriptionsTimeoutMilliseconds int `validate:"nonzero"`
	UsersTimeoutMilliseconds         int `validate:"nonzero"`

	// Feature Flipper Switches
	UseDynamoDBForCampaigns bool

	// Programatically set
	StatsEnvironment string

	// S2S
	S2SEnabled bool

	// 2FA Reward configs
	DartEndpointURL            string `validate:"nonzero"`
	TwoFactorEventBusQueueURL  string `validate:"nonzero"`
	TwoFactorBackfillQueueName string `validate:"nonzero"`
}

type ActivityConfig struct {
	ARN string `validate:"nonzero"`
}

type CheerbombStepFnConfig struct {
	StateMachineARN string `validate:"nonzero"`
}

type CheerbombTriggerStepFnConfig struct {
	StateMachineARN string `validate:"nonzero"`
}

const (
	localConfigFilePath               = "/src/code.justin.tv/%s/%s/config/%s.json"
	globalConfigFilePath              = "/etc/%s/config/%s.json"
	unspecifiedEnvironment            = ""
	devEnvironment                    = "dev"
	LambdaConfigFilePath              = "/config/%s.json"
	LambdaRootPathEnvironmentVariable = "LAMBDA_TASK_ROOT"
	RepoOrganization                  = "commerce"
	RepoService                       = "fortuna"
)

func GetEnvironment() string {
	env := os.Getenv("ENVIRONMENT")
	if env == unspecifiedEnvironment {
		return devEnvironment
	}
	return env
}

func LoadConfigForEnvironment(configuration interface{}, options *Options) error {
	configFilePath, err := getConfigFilePath(options)
	if err != nil {
		return err
	}
	return loadConfig(configFilePath, configuration)
}

func getConfigFilePath(options *Options) (string, error) {
	if lambdaRootPath := os.Getenv(LambdaRootPathEnvironmentVariable); lambdaRootPath != "" {
		lambdaFName := path.Join(lambdaRootPath, fmt.Sprintf(LambdaConfigFilePath, string(GetEnvironment())))
		_, err := os.Stat(lambdaFName)
		return lambdaFName, err
	}

	localFileName := ""
	if options.LocalPathOverride != "" {
		localFileName = options.LocalPathOverride
	} else {
		localFileName = os.Getenv("GOPATH") + getLocalFileFilePath(options)
	}

	if _, err := os.Stat(localFileName); !os.IsNotExist(err) {
		return localFileName, nil
	}

	globalFileName := ""
	if options.GlobalPathOverride != "" {
		globalFileName = options.GlobalPathOverride
	} else {
		globalFileName = fmt.Sprintf(globalConfigFilePath, RepoService, options.Environment)
	}

	if _, err := os.Stat(globalFileName); os.IsNotExist(err) {
		return "", err
	}
	return globalFileName, nil
}

func getLocalFileFilePath(options *Options) string {
	return fmt.Sprintf(localConfigFilePath, RepoOrganization, RepoService, options.Environment)
}

func loadConfig(filePath string, configOptions interface{}) error {
	file, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("failed to open config file: %v", err)
	}

	decoder := json.NewDecoder(file)

	if err := decoder.Decode(configOptions); err != nil {
		return fmt.Errorf("failed to parse configuration file: %v", err)
	}

	if err := validator.Validate(configOptions); err != nil {
		return fmt.Errorf("failed to validate configuration file: %v", err)
	}

	return nil
}
