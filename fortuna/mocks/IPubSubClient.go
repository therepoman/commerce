// Code generated by mockery v1.0.0
package mocks

import mock "github.com/stretchr/testify/mock"

// IPubSubClient is an autogenerated mock type for the IPubSubClient type
type IPubSubClient struct {
	mock.Mock
}

// Publish provides a mock function with given fields: pubsubEntityType, data, topic, topicSuffix
func (_m *IPubSubClient) Publish(pubsubEntityType string, data interface{}, topic string, topicSuffix string) error {
	ret := _m.Called(pubsubEntityType, data, topic, topicSuffix)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, interface{}, string, string) error); ok {
		r0 = rf(pubsubEntityType, data, topic, topicSuffix)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
