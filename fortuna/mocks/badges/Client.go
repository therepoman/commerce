// Code generated by mockery v1.0.0
package mocks

import context "context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/chat/badges/app/models"
import twitchclient "code.justin.tv/foundation/twitchclient"

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// GrantUserBadge provides a mock function with given fields: ctx, userID, badgeSetID, channelID, params, reqOpts
func (_m *Client) GrantUserBadge(ctx context.Context, userID string, badgeSetID string, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, userID, badgeSetID, channelID, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, models.GrantBadgeParams, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, userID, badgeSetID, channelID, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
