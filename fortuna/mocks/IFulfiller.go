// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import campaign "code.justin.tv/commerce/fortuna/campaign"
import mock "github.com/stretchr/testify/mock"
import rewards "code.justin.tv/commerce/fortuna/rewards"

// IFulfiller is an autogenerated mock type for the IFulfiller type
type IFulfiller struct {
	mock.Mock
}

// ClaimMakoReward provides a mock function with given fields: input
func (_m *IFulfiller) ClaimMakoReward(input rewards.MakoRewardInput) error {
	ret := _m.Called(input)

	var r0 error
	if rf, ok := ret.Get(0).(func(rewards.MakoRewardInput) error); ok {
		r0 = rf(input)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ClaimTriggerRewards provides a mock function with given fields: triggerRewards, userID
func (_m *IFulfiller) ClaimTriggerRewards(triggerRewards []*campaign.Reward, userID string) ([]*campaign.Reward, error) {
	ret := _m.Called(triggerRewards, userID)

	var r0 []*campaign.Reward
	if rf, ok := ret.Get(0).(func([]*campaign.Reward, string) []*campaign.Reward); ok {
		r0 = rf(triggerRewards, userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*campaign.Reward)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]*campaign.Reward, string) error); ok {
		r1 = rf(triggerRewards, userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// EntitleTrigger provides a mock function with given fields: trigger, userID, isBenefactor, overrideSelectionAmount
func (_m *IFulfiller) EntitleTrigger(trigger *campaign.Trigger, userID string, isBenefactor bool, overrideSelectionAmount int) ([]*campaign.Reward, []*campaign.Reward, error) {
	ret := _m.Called(trigger, userID, isBenefactor, overrideSelectionAmount)

	var r0 []*campaign.Reward
	if rf, ok := ret.Get(0).(func(*campaign.Trigger, string, bool, int) []*campaign.Reward); ok {
		r0 = rf(trigger, userID, isBenefactor, overrideSelectionAmount)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*campaign.Reward)
		}
	}

	var r1 []*campaign.Reward
	if rf, ok := ret.Get(1).(func(*campaign.Trigger, string, bool, int) []*campaign.Reward); ok {
		r1 = rf(trigger, userID, isBenefactor, overrideSelectionAmount)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).([]*campaign.Reward)
		}
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(*campaign.Trigger, string, bool, int) error); ok {
		r2 = rf(trigger, userID, isBenefactor, overrideSelectionAmount)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}
