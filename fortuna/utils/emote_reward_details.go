package utils

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients/mako"
	"code.justin.tv/commerce/fortuna/clients/pubsub"
	"code.justin.tv/commerce/fortuna/rewards"
	log "github.com/sirupsen/logrus"
)

func GetEmoteRewardDetailsForPubSub(makoClient mako.IMakoClient, rewardsToNotify []*campaign.Reward) ([]pubsub.Content, error) {
	ids := make([]string, 0, len(rewardsToNotify))
	imageURLsByEmoteSetID := map[string]string{}

	for _, reward := range rewardsToNotify {
		if reward.Type != rewards.RewardTypeEmote {
			continue
		}

		ids = append(ids, reward.ID)
		imageURLsByEmoteSetID[reward.ID] = reward.ImageURL
	}

	if len(ids) == 0 {
		return []pubsub.Content{}, nil
	}

	makoRewards, err := makoClient.BulkGetEmoteSetsByIDs(ids)
	if err != nil {
		return nil, err
	}

	contents := make([]pubsub.Content, len(makoRewards))
	for i, makoReward := range makoRewards {
		contents[i] = makoRewardToPubsubContent(makoReward)

		imageURL, ok := imageURLsByEmoteSetID[makoReward.EmoteSetID]
		if !ok || imageURL == "" {
			log.WithFields(log.Fields{
				"emoteSetID": makoReward.EmoteSetID,
				"emoteID":    makoReward.ID,
			}).Warn("Emote reward missing `ImageURL` field. Attempting to generate fallback image URL for PubSub notification")

			imageURL = rewards.GenerateFallbackEmoteImageURL(makoReward.ID)
		}

		contents[i].ImageURL = imageURL
	}

	return contents, nil
}

func makoRewardToPubsubContent(reward mako.RewardItem) pubsub.Content {
	return pubsub.Content{
		Type: reward.ItemType,
		ID:   reward.ID,
		Text: reward.Text,
	}
}
