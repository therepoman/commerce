package setup

import (
	"context"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/s3"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/video/autoprof/profs3"

	"github.com/aws/aws-sdk-go/aws/session"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	organizationName         = "commerce"
	serviceName              = "fortuna"
	localCampaignConfigPath  = "%s/src/code.justin.tv/%s/%s/campaign/%s.json"
	devEnvironment           = "dev"
	prodEnvironment          = "prod"
	canaryStatsEnvironment   = "canary"
	statsNamespace           = "fortuna"
	twitchTelemetryFlush     = 30 * time.Second // Flush metrics every 30 seconds for TwitchTelemetry
	twitchTelemetryMinBuffer = 100000           // Define a min buffer size to ensure there is enough room for metrics while flushing
	primarySubstage          = "primary"
)

type FortunaSetupOptions struct {
	FortunaConfig     *config.Configuration
	CampaignConfig    *campaign.Configuration
	Stats             statsd.Statter
	Clients           *clients.Clients
	Daos              *dynamo.DAOs
	ObjectiveHandlers map[string]handler.ObjectiveHandler
	TriggerHandlers   map[string]handler.TriggerHandler
	Fulfiller         rewards.IFulfiller
	RewardsSelector   rewards.ISelector
	Utils             rewards.IUtils
	Notifier          rewards.INotifier
}

// Set up fortuna service configuration. Returns the built fortuna configuration.
func SetupFortunaConfiguration() (*config.Configuration, error) {
	configOptions := &config.Options{
		Environment: config.GetEnvironment(),
	}

	log.Infof("Loading configuration for environment: %s", config.GetEnvironment())
	cfg := &config.Configuration{}
	err := config.LoadConfigForEnvironment(cfg, configOptions)
	if err != nil {
		return nil, fmt.Errorf("Error loading fortuna config: %v", err)
	}

	return cfg, nil
}

// Set up campaign configuration and denormalize it. Returns the built campaign configuration.
func SetupCampaignPoller(fortunaConfig *config.Configuration, s3Client s3.IS3Client) (*campaign.Poller, error) {
	localCampaignPath := fmt.Sprintf(localCampaignConfigPath, os.Getenv("GOPATH"), organizationName, serviceName, "test")
	campaignConfigOptions := &config.Options{
		LocalPathOverride: localCampaignPath,
	}

	// Attempt to load local test config file if it exists.
	if fortunaConfig.EnvironmentPrefix == devEnvironment {
		campaignCfg := &campaign.Configuration{}
		// hosted dev environment should not a local config file.
		if err := config.LoadConfigForEnvironment(campaignCfg, campaignConfigOptions); err == nil {
			log.Warn("------Loading local test campaign configuration file------")
			if err := campaignCfg.DenormalizeData(); err != nil {
				return nil, fmt.Errorf("Error denormalizing campaign config: %v", err)
			}
			return &campaign.Poller{
				CampaignConfig: campaignCfg,
			}, nil
		} else {
			log.WithError(err).Error("Failed to read test.json")
		}
	}

	campaignPoller, err := campaign.NewPoller(fortunaConfig, s3Client)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating campaign config poller")
	}

	go campaignPoller.RefreshCampaignsAtInterval()

	return campaignPoller, nil
}

func SetupAutoprof(fortunaConfig *config.Configuration) {
	sess, err := session.NewSession(s3.NewDefaultConfig())
	if err != nil {
		log.WithError(err).Error("Could not create new AWS session")
		return
	}

	collector := profs3.Collector{
		S3:       awsS3.New(sess),
		S3Bucket: fmt.Sprintf("fortuna-%s-autoprof", fortunaConfig.EnvironmentPrefix),
		OnError: func(err error) error {
			log.WithError(err).Error("Error setting up autoprof")
			return nil
		},
	}

	go collector.Run(context.Background())
}

// Set up stats objects for use throughout this service. Returns the built statter that should be used everywhere.
func SetupStats(fortunaConfig *config.Configuration) (statsd.Statter, error) {
	// Create the TwitchTelemetry statter
	twitchTelemetryStats := SetupTwitchTelemetryStatter(fortunaConfig)

	// Form a single composite statter from TwitchTelemetry statter(s)
	compositeStatter, err := splatter.NewCompositeStatter([]statsd.Statter{twitchTelemetryStats})
	if err != nil {
		return nil, errors.Wrap(err, "Failed to initialize Composite Statter")
	}
	return compositeStatter, nil
}

func SetupTwitchTelemetryStatter(fortunaConfig *config.Configuration) statsd.Statter {
	// Define a buffer as at least twitchTelemetryMinBuffer
	bufferSize := fortunaConfig.CloudwatchBufferSize
	if bufferSize < twitchTelemetryMinBuffer {
		bufferSize = twitchTelemetryMinBuffer
	}

	// Update the Stage and Substage depending on whether we're using a Canary
	telemetryStage := fortunaConfig.StatsEnvironment
	telemetrySubstage := primarySubstage
	if telemetryStage == canaryStatsEnvironment {
		telemetryStage = prodEnvironment
		telemetrySubstage = canaryStatsEnvironment
	}

	// Construct the TwitchTelemetry Config
	twitchTelemetryConfig := &splatter.BufferedTelemetryConfig{
		FlushPeriod:       twitchTelemetryFlush,
		BufferSize:        bufferSize,
		AggregationPeriod: time.Minute, // Place all metrics into 1 minute buckets
		ServiceName:       statsNamespace,
		AWSRegion:         fortunaConfig.CloudwatchRegion,
		Stage:             telemetryStage,
		Substage:          telemetrySubstage,
		Prefix:            "", // No need for a prefix since service, stage, and substage are all dimensions
	}

	// Construct the buffered CloudWatch TwitchTelemetry sender, no need to filter any stats
	return splatter.NewBufferedTelemetryCloudWatchStatter(twitchTelemetryConfig, map[string]bool{})
}

type LambdaClients struct {
	Clients  *clients.Clients
	Config   *config.Configuration
	Utils    *rewards.Utils
	Notifier *rewards.Notifier
}

func NewClients() (*LambdaClients, statsd.Statter, error) {
	environment := config.GetEnvironment()
	statsEnvironment := os.Getenv("STATS_ENVIRONMENT")
	if statsEnvironment == "" {
		statsEnvironment = environment
	}

	log.Debugf("stats environment %s", statsEnvironment)

	logLevel := log.DebugLevel
	if statsEnvironment == prodEnvironment {
		logLevel = log.InfoLevel
	}
	log.SetLevel(logLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

	fortunaConfig, err := SetupFortunaConfiguration()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize fortuna config")
	}
	fortunaConfig.StatsEnvironment = statsEnvironment

	s3Client := s3.NewFromDefaultConfig()

	campaignPoller, err := SetupCampaignPoller(fortunaConfig, s3Client)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize campaign config")
	}

	SetupAutoprof(fortunaConfig)

	stats, err := SetupStats(fortunaConfig)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	// defer stats.Close()

	clients, err := clients.SetupClients(fortunaConfig, campaignPoller.CampaignConfig, stats, s3Client)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize clients")
	}

	daos, err := dynamo.SetupDAOs(fortunaConfig)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize DAOs")
	}

	utils := &rewards.Utils{
		CampaignConfig: campaignPoller.CampaignConfig,
		CampaignHelper: daos,
		Config:         fortunaConfig,
		Clients:        clients,
		DAOs:           daos,
	}

	notifier := &rewards.Notifier{
		Clients: clients,
	}

	return &LambdaClients{
		Clients:  clients,
		Config:   fortunaConfig,
		Utils:    utils,
		Notifier: notifier,
	}, stats, err
}
