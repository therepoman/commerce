package lambda_utils

import (
	"encoding/json"

	"code.justin.tv/commerce/fortuna/clients"

	"github.com/pkg/errors"
)

func MarshalToJSONString(v interface{}) (string, error) {
	bytes, err := json.Marshal(v)
	if err != nil {
		return "", errors.Wrapf(err, "Failed to marshal value to JSON. Value: %+v", v)
	}

	return string(bytes), nil
}

func GetUnmarshalErr(err error, inputString string) error {
	return errors.Wrapf(err, "Failed to unmarshal input string: %s", inputString)
}

func UploadFileToS3(fileName string, data []byte, clients *clients.Clients, stepFnDocumentS3Bucket string) error {
	exists, err := clients.S3Client.BucketExists(stepFnDocumentS3Bucket)
	if err != nil {
		return err
	}

	if !exists {
		return errors.New("Bucket does not exist")
	}

	return clients.S3Client.UploadFileToBucket(stepFnDocumentS3Bucket, fileName, data)
}

func LoadFileFromS3(fileName string, clients *clients.Clients, stepFnDocumentS3Bucket string) ([]byte, error) {
	exists, err := clients.S3Client.BucketExists(stepFnDocumentS3Bucket)
	if err != nil {
		return nil, err
	}

	if !exists {
		return nil, errors.New("Bucket does not exist")
	}

	exists, err = clients.S3Client.FileExists(stepFnDocumentS3Bucket, fileName)
	if err != nil {
		return nil, err
	}

	if !exists {
		return nil, errors.New("File does not exist:" + fileName)
	}

	return clients.S3Client.LoadFile(stepFnDocumentS3Bucket, fileName)
}

func RemoveDuplicates(elements []string) []string {
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if _, ok := encountered[elements[v]]; !ok {
			encountered[elements[v]] = true
			result = append(result, elements[v])
		}
	}
	return result
}
