package math

// AbsInt32 returns the absolute value of an int32
func AbsInt32(i int32) int32 {
	if i < 0 {
		return -i
	}
	return i
}

// AbsInt64 returns the absolute value of an int64
func AbsInt64(i int64) int64 {
	if i < 0 {
		return -i
	}
	return i
}
