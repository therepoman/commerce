package manager

import "sync"

type cache struct {
	mutex     sync.Mutex
	secretMap map[string]*Secret
}

type cacheAPI interface {
	Delete(key string)
	Get(key string) (secret *Secret)
	Set(key string, value *Secret)
}

func (c *cache) Get(key string) (secret *Secret) {
	if c.secretMap == nil {
		return
	}
	c.mutex.Lock()
	secret = c.secretMap[key]
	c.mutex.Unlock()
	return

}

func (c *cache) Set(key string, value *Secret) {
	if c.secretMap == nil {
		return
	}
	c.mutex.Lock()
	c.secretMap[key] = value
	c.mutex.Unlock()
	return
}

func (c *cache) Delete(key string) {
	if c.secretMap == nil {
		return
	}
	c.mutex.Lock()
	delete(c.secretMap, key)
	c.mutex.Unlock()
	return
}
