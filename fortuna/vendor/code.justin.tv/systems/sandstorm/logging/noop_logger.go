package logging

// NoopLogger implements Logger with no operation
type NoopLogger struct {
}

// Debug ...
func (nop *NoopLogger) Debug(args ...interface{}) {}

// Debugf ...
func (nop *NoopLogger) Debugf(format string, args ...interface{}) {}

// Debugln ...
func (nop *NoopLogger) Debugln(args ...interface{}) {}

// Error ...
func (nop *NoopLogger) Error(args ...interface{}) {}

// Errorf ...
func (nop *NoopLogger) Errorf(format string, args ...interface{}) {}

// Errorln ...
func (nop *NoopLogger) Errorln(args ...interface{}) {}

// Fatal ...
func (nop *NoopLogger) Fatal(args ...interface{}) {}

// Fatalf ...
func (nop *NoopLogger) Fatalf(format string, args ...interface{}) {}

// Fatalln ...
func (nop *NoopLogger) Fatalln(args ...interface{}) {}

// Info ...
func (nop *NoopLogger) Info(args ...interface{}) {}

// Infof ...
func (nop *NoopLogger) Infof(format string, args ...interface{}) {}

// Infoln ...
func (nop *NoopLogger) Infoln(args ...interface{}) {}

// Panic ...
func (nop *NoopLogger) Panic(args ...interface{}) {}

// Panicf ...
func (nop *NoopLogger) Panicf(format string, args ...interface{}) {}

// Panicln ...
func (nop *NoopLogger) Panicln(args ...interface{}) {}

// Print ...
func (nop *NoopLogger) Print(args ...interface{}) {}

// Printf ...
func (nop *NoopLogger) Printf(format string, args ...interface{}) {}

// Println ...
func (nop *NoopLogger) Println(args ...interface{}) {}

// Warn ...
func (nop *NoopLogger) Warn(args ...interface{}) {}

// Warnf ...
func (nop *NoopLogger) Warnf(format string, args ...interface{}) {}

// Warnln ...
func (nop *NoopLogger) Warnln(args ...interface{}) {}
