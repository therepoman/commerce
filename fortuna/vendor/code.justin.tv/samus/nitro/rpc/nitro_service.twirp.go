// Code generated by protoc-gen-twirp v4.9.0, DO NOT EDIT.
// source: rpc/nitro_service.proto

/*
Package nitro is a generated twirp stub package.
This code was generated with code.justin.tv/common/twirp/protoc-gen-twirp v4.9.0.

It is generated from these files:
	rpc/nitro_service.proto
*/
package nitro

import bytes "bytes"
import fmt "fmt"
import ioutil "io/ioutil"
import log "log"
import http "net/http"

import jsonpb "github.com/golang/protobuf/jsonpb"
import proto "github.com/golang/protobuf/proto"
import context "golang.org/x/net/context"
import twirp "code.justin.tv/common/twirp"

// Imports only used by utility functions:
import io "io"
import strconv "strconv"
import strings "strings"
import json "encoding/json"
import url "net/url"
import ctxhttp "golang.org/x/net/context/ctxhttp"

// ===============
// Nitro Interface
// ===============

type Nitro interface {
	GetPremiumStatuses(context.Context, *GetPremiumStatusesRequest) (*GetPremiumStatusesResponse, error)

	GrantPremiumStatus(context.Context, *GrantPremiumStatusRequest) (*GrantPremiumStatusResponse, error)

	CancelPremiumStatus(context.Context, *CancelPremiumStatusRequest) (*CancelPremiumStatusResponse, error)
}

// =====================
// Nitro Protobuf Client
// =====================

type nitroProtobufClient struct {
	urlBase string
	client  *http.Client
}

// NewNitroProtobufClient creates a Protobuf client that implements the Nitro interface.
// It communicates using protobuf messages and can be configured with a custom http.Client.
func NewNitroProtobufClient(addr string, client *http.Client) Nitro {
	return &nitroProtobufClient{
		urlBase: urlBase(addr),
		client:  withoutRedirects(client),
	}
}

func (c *nitroProtobufClient) GetPremiumStatuses(ctx context.Context, in *GetPremiumStatusesRequest) (*GetPremiumStatusesResponse, error) {
	url := c.urlBase + NitroPathPrefix + "GetPremiumStatuses"
	out := new(GetPremiumStatusesResponse)
	err := doProtoRequest(ctx, c.client, url, in, out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *nitroProtobufClient) GrantPremiumStatus(ctx context.Context, in *GrantPremiumStatusRequest) (*GrantPremiumStatusResponse, error) {
	url := c.urlBase + NitroPathPrefix + "GrantPremiumStatus"
	out := new(GrantPremiumStatusResponse)
	err := doProtoRequest(ctx, c.client, url, in, out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *nitroProtobufClient) CancelPremiumStatus(ctx context.Context, in *CancelPremiumStatusRequest) (*CancelPremiumStatusResponse, error) {
	url := c.urlBase + NitroPathPrefix + "CancelPremiumStatus"
	out := new(CancelPremiumStatusResponse)
	err := doProtoRequest(ctx, c.client, url, in, out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// =================
// Nitro JSON Client
// =================

type nitroJSONClient struct {
	urlBase string
	client  *http.Client
}

// NewNitroJSONClient creates a JSON client that implements the Nitro interface.
// It communicates using JSON requests and responses instead of protobuf messages.
func NewNitroJSONClient(addr string, client *http.Client) Nitro {
	return &nitroJSONClient{
		urlBase: urlBase(addr),
		client:  withoutRedirects(client),
	}
}

func (c *nitroJSONClient) GetPremiumStatuses(ctx context.Context, in *GetPremiumStatusesRequest) (*GetPremiumStatusesResponse, error) {
	url := c.urlBase + NitroPathPrefix + "GetPremiumStatuses"
	out := new(GetPremiumStatusesResponse)
	err := doJSONRequest(ctx, c.client, url, in, out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *nitroJSONClient) GrantPremiumStatus(ctx context.Context, in *GrantPremiumStatusRequest) (*GrantPremiumStatusResponse, error) {
	url := c.urlBase + NitroPathPrefix + "GrantPremiumStatus"
	out := new(GrantPremiumStatusResponse)
	err := doJSONRequest(ctx, c.client, url, in, out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *nitroJSONClient) CancelPremiumStatus(ctx context.Context, in *CancelPremiumStatusRequest) (*CancelPremiumStatusResponse, error) {
	url := c.urlBase + NitroPathPrefix + "CancelPremiumStatus"
	out := new(CancelPremiumStatusResponse)
	err := doJSONRequest(ctx, c.client, url, in, out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ====================
// Nitro Server Handler
// ====================

type nitroServer struct {
	Nitro
	hooks     *twirp.ServerHooks
	ctxSource ContextSource
}

func NewNitroServer(svc Nitro, hooks *twirp.ServerHooks, ctxSrc ContextSource) TwirpServer {
	defaultHooks := twirp.NewServerHooks()
	if hooks == nil {
		hooks = defaultHooks
	} else {
		if hooks.RequestReceived == nil {
			hooks.RequestReceived = defaultHooks.RequestReceived
		}
		if hooks.RequestRouted == nil {
			hooks.RequestRouted = defaultHooks.RequestRouted
		}
		if hooks.ResponsePrepared == nil {
			hooks.ResponsePrepared = defaultHooks.ResponsePrepared
		}
		if hooks.ResponseSent == nil {
			hooks.ResponseSent = defaultHooks.ResponseSent
		}
		if hooks.Error == nil {
			hooks.Error = defaultHooks.Error
		}
	}
	if ctxSrc == nil {
		ctxSrc = RequestContextSource
	}

	return &nitroServer{
		Nitro:     svc,
		hooks:     hooks,
		ctxSource: ctxSrc,
	}
}

// writeError writes an HTTP response with a valid Twirp error format, and triggers hooks.
// If err is not a twirp.Error, it will get wrapped with twirp.InternalErrorWith(err)
func (s *nitroServer) writeError(ctx context.Context, resp http.ResponseWriter, err error) {
	// When handling v2 client requests, serve backwards-compatible error responses
	if version := getVersion(ctx); version == "v1" || version == "v2" {
		writeErrorPreV3(ctx, resp, err, s.hooks)
	} else {
		writeErrorPostV3(ctx, resp, err, s.hooks)
	}
}

// NitroPathPrefix is used for all URL paths on a twirp Nitro server.
// Requests are always: POST NitroPathPrefix/method
// It can be used in an HTTP mux to route twirp requests along with non-twirp requests on other routes.
const NitroPathPrefix = "/twirp/code.justin.tv.samus.nitro.Nitro/"

// NitroPathPrefixOld is used to handle requests from v2 Clients.
// If you are using an HTTP mux, please handle this routes as well to upgrade from v2 to v3.
const NitroPathPrefixOld = "/v2/code.justin.tv.samus.nitro.Nitro/"

func (s *nitroServer) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	ctx := s.ctxSource(resp, req)
	ctx = setServerName(ctx, "Nitro")
	ctx = setVersionFromPath(ctx, req.URL.Path)
	ctx = context.WithValue(ctx, "__tw_response_writer", resp) // for SetHTTPResponseHeader
	ctx = s.hooks.RequestReceived(ctx)

	if req.Method != "POST" {
		msg := fmt.Sprintf("unsupported method %q (only POST is allowed)", req.Method)
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.writeError(ctx, resp, twerr)
		return
	}

	switch req.URL.Path {
	case NitroPathPrefix + "GetPremiumStatuses", NitroPathPrefixOld + "GetPremiumStatuses", "/v1/Nitro/GetPremiumStatuses":
		s.serveGetPremiumStatuses(ctx, resp, req)
		return
	case NitroPathPrefix + "GrantPremiumStatus", NitroPathPrefixOld + "GrantPremiumStatus", "/v1/Nitro/GrantPremiumStatus":
		s.serveGrantPremiumStatus(ctx, resp, req)
		return
	case NitroPathPrefix + "CancelPremiumStatus", NitroPathPrefixOld + "CancelPremiumStatus", "/v1/Nitro/CancelPremiumStatus":
		s.serveCancelPremiumStatus(ctx, resp, req)
		return
	default:
		msg := fmt.Sprintf("no handler for path %q", req.URL.Path)
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.writeError(ctx, resp, twerr)
		return
	}
}

func (s *nitroServer) serveGetPremiumStatuses(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	switch req.Header.Get("Content-Type") {
	case "application/json":
		s.serveGetPremiumStatusesJSON(ctx, resp, req)
	case "application/protobuf":
		s.serveGetPremiumStatusesProtobuf(ctx, resp, req)
	default:
		msg := fmt.Sprintf("unexpected Content-Type: %q", req.Header.Get("Content-Type"))
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.writeError(ctx, resp, twerr)
	}
}

func (s *nitroServer) serveGetPremiumStatusesJSON(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "GetPremiumStatuses")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	reqContent := new(GetPremiumStatusesRequest)
	unmarshaler := jsonpb.Unmarshaler{AllowUnknownFields: true}
	if err = unmarshaler.Unmarshal(req.Body, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request json")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *GetPremiumStatusesResponse
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.writeError(ctx, resp, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.GetPremiumStatuses(ctx, reqContent)
	}()

	if err != nil {
		s.writeError(ctx, resp, err)
		return
	}
	if respContent == nil {
		s.writeError(ctx, resp, twirp.InternalError("received a nil *GetPremiumStatusesResponse and nil error while calling GetPremiumStatuses. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	var buf bytes.Buffer
	marshaler := &jsonpb.Marshaler{OrigName: true}
	if err = marshaler.Marshal(&buf, respContent); err != nil {
		err = wrapErr(err, "failed to marshal json response")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(buf.Bytes()); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *nitroServer) serveGetPremiumStatusesProtobuf(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "GetPremiumStatuses")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		err = wrapErr(err, "failed to read request body")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}
	reqContent := new(GetPremiumStatusesRequest)
	if err = proto.Unmarshal(buf, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request proto")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *GetPremiumStatusesResponse
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.writeError(ctx, resp, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.GetPremiumStatuses(ctx, reqContent)
	}()

	if err != nil {
		s.writeError(ctx, resp, err)
		return
	}
	if respContent == nil {
		s.writeError(ctx, resp, twirp.InternalError("received a nil *GetPremiumStatusesResponse and nil error while calling GetPremiumStatuses. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	respBytes, err := proto.Marshal(respContent)
	if err != nil {
		err = wrapErr(err, "failed to marshal proto response")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/protobuf")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(respBytes); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *nitroServer) serveGrantPremiumStatus(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	switch req.Header.Get("Content-Type") {
	case "application/json":
		s.serveGrantPremiumStatusJSON(ctx, resp, req)
	case "application/protobuf":
		s.serveGrantPremiumStatusProtobuf(ctx, resp, req)
	default:
		msg := fmt.Sprintf("unexpected Content-Type: %q", req.Header.Get("Content-Type"))
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.writeError(ctx, resp, twerr)
	}
}

func (s *nitroServer) serveGrantPremiumStatusJSON(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "GrantPremiumStatus")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	reqContent := new(GrantPremiumStatusRequest)
	unmarshaler := jsonpb.Unmarshaler{AllowUnknownFields: true}
	if err = unmarshaler.Unmarshal(req.Body, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request json")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *GrantPremiumStatusResponse
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.writeError(ctx, resp, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.GrantPremiumStatus(ctx, reqContent)
	}()

	if err != nil {
		s.writeError(ctx, resp, err)
		return
	}
	if respContent == nil {
		s.writeError(ctx, resp, twirp.InternalError("received a nil *GrantPremiumStatusResponse and nil error while calling GrantPremiumStatus. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	var buf bytes.Buffer
	marshaler := &jsonpb.Marshaler{OrigName: true}
	if err = marshaler.Marshal(&buf, respContent); err != nil {
		err = wrapErr(err, "failed to marshal json response")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(buf.Bytes()); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *nitroServer) serveGrantPremiumStatusProtobuf(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "GrantPremiumStatus")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		err = wrapErr(err, "failed to read request body")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}
	reqContent := new(GrantPremiumStatusRequest)
	if err = proto.Unmarshal(buf, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request proto")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *GrantPremiumStatusResponse
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.writeError(ctx, resp, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.GrantPremiumStatus(ctx, reqContent)
	}()

	if err != nil {
		s.writeError(ctx, resp, err)
		return
	}
	if respContent == nil {
		s.writeError(ctx, resp, twirp.InternalError("received a nil *GrantPremiumStatusResponse and nil error while calling GrantPremiumStatus. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	respBytes, err := proto.Marshal(respContent)
	if err != nil {
		err = wrapErr(err, "failed to marshal proto response")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/protobuf")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(respBytes); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *nitroServer) serveCancelPremiumStatus(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	switch req.Header.Get("Content-Type") {
	case "application/json":
		s.serveCancelPremiumStatusJSON(ctx, resp, req)
	case "application/protobuf":
		s.serveCancelPremiumStatusProtobuf(ctx, resp, req)
	default:
		msg := fmt.Sprintf("unexpected Content-Type: %q", req.Header.Get("Content-Type"))
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.writeError(ctx, resp, twerr)
	}
}

func (s *nitroServer) serveCancelPremiumStatusJSON(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "CancelPremiumStatus")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	reqContent := new(CancelPremiumStatusRequest)
	unmarshaler := jsonpb.Unmarshaler{AllowUnknownFields: true}
	if err = unmarshaler.Unmarshal(req.Body, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request json")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *CancelPremiumStatusResponse
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.writeError(ctx, resp, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.CancelPremiumStatus(ctx, reqContent)
	}()

	if err != nil {
		s.writeError(ctx, resp, err)
		return
	}
	if respContent == nil {
		s.writeError(ctx, resp, twirp.InternalError("received a nil *CancelPremiumStatusResponse and nil error while calling CancelPremiumStatus. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	var buf bytes.Buffer
	marshaler := &jsonpb.Marshaler{OrigName: true}
	if err = marshaler.Marshal(&buf, respContent); err != nil {
		err = wrapErr(err, "failed to marshal json response")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(buf.Bytes()); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *nitroServer) serveCancelPremiumStatusProtobuf(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "CancelPremiumStatus")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		err = wrapErr(err, "failed to read request body")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}
	reqContent := new(CancelPremiumStatusRequest)
	if err = proto.Unmarshal(buf, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request proto")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *CancelPremiumStatusResponse
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.writeError(ctx, resp, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.CancelPremiumStatus(ctx, reqContent)
	}()

	if err != nil {
		s.writeError(ctx, resp, err)
		return
	}
	if respContent == nil {
		s.writeError(ctx, resp, twirp.InternalError("received a nil *CancelPremiumStatusResponse and nil error while calling CancelPremiumStatus. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	respBytes, err := proto.Marshal(respContent)
	if err != nil {
		err = wrapErr(err, "failed to marshal proto response")
		s.writeError(ctx, resp, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/protobuf")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(respBytes); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *nitroServer) ServiceDescriptor() ([]byte, int) {
	return twirpFileDescriptor0, 0
}

func (s *nitroServer) ProtocGenTwirpVersion() string {
	return "v4.9.0"
}

// =====
// Utils
// =====

// TwirpServer is the interface generated server structs will support: they're
// HTTP handlers with additional methods for accessing metadata about the
// service. Those accessors are a low-level API for building reflection tools.
// Most people can think of TwirpServers as just http.Handlers.
type TwirpServer interface {
	http.Handler
	// ServiceDescriptor returns gzipped bytes describing the .proto file that
	// this service was generated from. Once unzipped, the bytes can be
	// unmarshaled as a
	// github.com/golang/protobuf/protoc-gen-go/descriptor.FileDescriptorProto.
	//
	// The returned integer is the index of this particular service within that
	// FileDescriptorProto's 'Service' slice of ServiceDescriptorProtos. This is a
	// low-level field, expected to be used for reflection.
	ServiceDescriptor() ([]byte, int)
	// ProtocGenTwirpVersion is the semantic version string of the version of
	// twirp used to generate this file.
	ProtocGenTwirpVersion() string
}

// A ContextSource establishes the base context for a Server.
type ContextSource func(http.ResponseWriter, *http.Request) context.Context

var RequestContextSource ContextSource = func(w http.ResponseWriter, req *http.Request) context.Context {
	return req.Context()
}

var BackgroundContextSource ContextSource = func(http.ResponseWriter, *http.Request) context.Context {
	return context.Background()
}

// done returns ctx.Err() if ctx.Done() indicates that the context done
func done(ctx context.Context) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		return nil
	}
}

// WriteError writes an HTTP response with a valid Twirp error format.
// If err is not a twirp.Error, it will get wrapped with twirp.InternalErrorWith(err)
// NOTE: not compatible with old v1/v2 clients (only returns v3+ errors)
func WriteError(resp http.ResponseWriter, err error) {
	writeErrorPostV3(nil, resp, err, nil) // does not trigger server hooks
}

// writeErrorPostV3 writes Twirp v3+ errors in the response.
func writeErrorPostV3(ctx context.Context, resp http.ResponseWriter, err error, hooks *twirp.ServerHooks) {
	// Non-twirp errors are wrapped as Internal (default)
	twerr, ok := err.(twirp.Error)
	if !ok {
		twerr = twirp.InternalErrorWith(err)
	}

	statusCode := twirp.ServerHTTPStatusFromErrorCode(twerr.Code())
	if hooks != nil {
		ctx = setStatusCode(ctx, statusCode)
		ctx = hooks.Error(ctx, twerr)
	}

	resp.Header().Set("Content-Type", "application/json") // Error responses are always JSON (instead of protobuf)
	resp.WriteHeader(statusCode)                          // HTTP response status code

	respBody := marshalErrorToJSON(twerr)
	_, err2 := resp.Write(respBody)
	if err2 != nil {
		log.Printf("unable to send error message %q: %s", twerr, err2)
	}

	if hooks != nil {
		_ = hooks.ResponseSent(ctx)
	}
}

// writeErrorPreV3 writes Twirp v1/v2 backwards-compatible errors in the response.
// This is to make it easy to upgrade to v3, a v3 server keeps serving v2-style errors to v2 clients.
func writeErrorPreV3(ctx context.Context, resp http.ResponseWriter, err error, hooks *twirp.ServerHooks) {
	// Non-twirp errors are wrapped as Unknown 500 (default)
	twerr, ok := err.(twirp.Error)
	if !ok {
		twerr = twirp.NewError(twirp.Unknown, err.Error())
	}

	resp.Header().Set("Content-Type", "text/plain")

	// v2err.Retryable <= v3err.Meta("retryable")
	if twerr.Meta("retryable") != "" {
		resp.Header().Set("Twirp-Error-Retryable", "true")
	}

	// v2err.ErrorID <= v3err.Meta("v2_error_id") if present, otherwise v3err.Code()
	// or "unroutable" if generated from twirp router (BadRoute)
	var v2ErrorID string
	if twerr.Code() == twirp.BadRoute {
		v2ErrorID = "unroutable" // as was generated by previous router
	} else if twerr.Meta("v2_error_id") != "" {
		v2ErrorID = twerr.Meta("v2_error_id")
	} else {
		v2ErrorID = string(twerr.Code()) // the string-format of v3 codes match v2 error ids for the most part: i.e. "not_found", "internal", "unknown", "canceled", etc.
	}
	resp.Header().Set("Twirp-ErrorID", v2ErrorID)

	// v2err.StatusCode <= v3err.Meta("v2_status_code") if present, otherwise same as in v3
	statusCode, err := strconv.Atoi(twerr.Meta("v2_status_code"))
	if err != nil {
		statusCode = twirp.ServerHTTPStatusFromErrorCode(twerr.Code()) // v3 it's the same as v2 for the most part: i.e. "not_found" is 404
	}
	if hooks != nil {
		ctx = setStatusCode(ctx, statusCode)
		ctx = hooks.Error(ctx, twerr)
	}
	resp.WriteHeader(statusCode)

	msg := []byte(twerr.Msg())
	if len(msg) > 1e6 {
		msg = msg[:1e6]
	}
	_, err2 := resp.Write(msg)
	if err2 != nil {
		log.Printf("unable to send error message %q: %s", twerr, err2)
	}

	if hooks != nil {
		_ = hooks.ResponseSent(ctx)
	}
}

// urlBase helps ensure that addr specifies a scheme. If it is unparsable
// as a URL, it returns addr unchanged.
func urlBase(addr string) string {
	// If the addr specifies a scheme, use it. If not, default to
	// http. If url.Parse fails on it, return it unchanged.
	url, err := url.Parse(addr)
	if err != nil {
		return addr
	}
	if url.Scheme == "" {
		url.Scheme = "http"
	}
	return url.String()
}

// setMethodName, setServerName, and setStatusCode are functions to
// expose internal state through contexts. The context keys hardcoded
// in here are not to be used on their own. Twirp provides accessor
// functions which will retrieve these values out and give them the
// correct type for you.
func setMethodName(ctx context.Context, name string) context.Context {
	return context.WithValue(ctx, "__tw_method_name", name)
}
func setServerName(ctx context.Context, name string) context.Context {
	return context.WithValue(ctx, "__tw_service_name", name)
}
func setStatusCode(ctx context.Context, code int) context.Context {
	return context.WithValue(ctx, "__tw_status_code", strconv.Itoa(code))
}

type privateContextKey int

var versionKey = new(privateContextKey)

// setVersionFromPath adds "v1", "v2" or "v3" to the context depending on the path.
func setVersionFromPath(ctx context.Context, path string) context.Context {
	var version string
	if strings.HasPrefix(path, "/v1/") {
		version = "v1"
	} else if strings.HasPrefix(path, "/v2/") {
		version = "v2"
	} else {
		version = "v3"
	}
	return context.WithValue(ctx, versionKey, version)
}

// getVersion returns the version number ("v1", "v2", "v3") or empty string if unset.
func getVersion(ctx context.Context) string {
	v, _ := ctx.Value(versionKey).(string)
	return v
}

// getCustomHTTPReqHeaders retrieves a copy of any headers that are set in
// a context through the twirp.WithHTTPRequestHeaders function.
// If there are no headers set, or if they have the wrong type, nil is returned.
func getCustomHTTPReqHeaders(ctx context.Context) http.Header {
	header, ok := ctx.Value("__tw_request_header").(http.Header)
	if !ok || header == nil {
		return nil
	}
	copied := make(http.Header)
	for k, vv := range header {
		if vv == nil {
			copied[k] = nil
			continue
		}
		copied[k] = make([]string, len(vv))
		copy(copied[k], vv)
	}
	return copied
}

// closebody closes a response or request body and just logs
// any error encountered while closing, since errors are
// considered very unusual.
func closebody(body io.Closer) {
	if err := body.Close(); err != nil {
		log.Printf("error closing body: %q", err)
	}
}

// newRequest makes an http.Request from a client, adding common headers.
func newRequest(ctx context.Context, url string, reqBody io.Reader, contentType string) (*http.Request, error) {
	req, err := http.NewRequest("POST", url, reqBody)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if customHeader := getCustomHTTPReqHeaders(ctx); customHeader != nil {
		req.Header = customHeader
	}
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Twirp-Version", "v4.9.0")
	return req, nil
}

// JSON serialization for errors
type twerrJSON struct {
	Code string            `json:"code"`
	Msg  string            `json:"msg"`
	Meta map[string]string `json:"meta,omitempty"`
}

// marshalErrorToJSON returns JSON from a twirp.Error, that can be used as HTTP error response body.
// If serialization fails, it will use a descriptive Internal error instead.
func marshalErrorToJSON(twerr twirp.Error) []byte {
	// make sure that msg is not too large
	msg := twerr.Msg()
	if len(msg) > 1e6 {
		msg = msg[:1e6]
	}

	tj := twerrJSON{
		Code: string(twerr.Code()),
		Msg:  msg,
		Meta: twerr.MetaMap(),
	}

	buf, err := json.Marshal(&tj)
	if err != nil {
		buf = []byte("{\"type\": \"" + twirp.Internal + "\", \"msg\": \"There was an error but it could not be serialized into JSON\"}") // fallback
	}

	return buf
}

// errorFromResponse builds a twirp.Error from a non-200 HTTP response.
// If the response has a valid serialized Twirp error, then it's returned.
// If not, the response status code is used to generate a similar twirp
// error. See twirpErrorFromIntermediary for more info on intermediary errors.
func errorFromResponse(resp *http.Response) twirp.Error {
	statusCode := resp.StatusCode
	statusText := http.StatusText(statusCode)

	if isHTTPRedirect(statusCode) {
		// Unexpected redirect: it must be an error from an intermediary.
		// Twirp clients dont't follow redirects automatically, Twirp only handles
		// POST requests, redirects should only happen on GET and HEAD requests.
		location := resp.Header.Get("Location")
		msg := fmt.Sprintf("unexpected HTTP status code %d %q received, Location=%q", statusCode, statusText, location)
		return twirpErrorFromIntermediary(statusCode, msg, location)
	}

	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return clientError("failed to read server error response body", err)
	}
	var tj twerrJSON
	if err := json.Unmarshal(respBodyBytes, &tj); err != nil {
		// Invalid JSON response; it must be an error from an intermediary.
		msg := fmt.Sprintf("Error from intermediary with HTTP status code %d %q", statusCode, statusText)
		return twirpErrorFromIntermediary(statusCode, msg, string(respBodyBytes))
	}

	errorCode := twirp.ErrorCode(tj.Code)
	if !twirp.IsValidErrorCode(errorCode) {
		msg := "invalid type returned from server error response: " + tj.Code
		return twirp.InternalError(msg)
	}

	twerr := twirp.NewError(errorCode, tj.Msg)
	for k, v := range tj.Meta {
		twerr = twerr.WithMeta(k, v)
	}
	return twerr
}

// twirpErrorFromIntermediary maps HTTP errors from non-twirp sources to twirp errors.
// The mapping is similar to gRPC: https://github.com/grpc/grpc/blob/master/doc/http-grpc-status-mapping.md.
// Returned twirp Errors have some additional metadata for inspection.
func twirpErrorFromIntermediary(status int, msg string, bodyOrLocation string) twirp.Error {
	var code twirp.ErrorCode
	if isHTTPRedirect(status) { // 3xx
		code = twirp.Internal
	} else {
		switch status {
		case 400: // Bad Request
			code = twirp.Internal
		case 401: // Unauthorized
			code = twirp.Unauthenticated
		case 403: // Forbidden
			code = twirp.PermissionDenied
		case 404: // Not Found
			code = twirp.BadRoute
		case 429, 502, 503, 504: // Too Many Requests, Bad Gateway, Service Unavailable, Gateway Timeout
			code = twirp.Unavailable
		default: // All other codes
			code = twirp.Unknown
		}
	}

	twerr := twirp.NewError(code, msg)
	twerr = twerr.WithMeta("http_error_from_intermediary", "true") // to easily know if this error was from intermediary
	twerr = twerr.WithMeta("status_code", strconv.Itoa(status))
	if isHTTPRedirect(status) {
		twerr = twerr.WithMeta("location", bodyOrLocation)
	} else {
		twerr = twerr.WithMeta("body", bodyOrLocation)
	}
	return twerr
}
func isHTTPRedirect(status int) bool {
	return status >= 300 && status <= 399
}

// wrappedError implements the github.com/pkg/errors.Causer interface, allowing errors to be
// examined for their root cause.
type wrappedError struct {
	msg   string
	cause error
}

func wrapErr(err error, msg string) error { return &wrappedError{msg: msg, cause: err} }
func (e *wrappedError) Cause() error      { return e.cause }
func (e *wrappedError) Error() string     { return e.msg + ": " + e.cause.Error() }

// clientError adds consistency to errors generated in the client
func clientError(desc string, err error) twirp.Error {
	return twirp.InternalErrorWith(wrapErr(err, desc))
}

// badRouteError is used when the twirp server cannot route a request
func badRouteError(msg string, method, url string) twirp.Error {
	err := twirp.NewError(twirp.BadRoute, msg)
	err = err.WithMeta("twirp_invalid_route", method+" "+url)
	return err
}

// The standard library will, by default, redirect requests (including POSTs) if it gets a 302 or
// 303 response, and also 301s in go1.8. It redirects by making a second request, changing the
// method to GET and removing the body. This produces very confusing error messages, so instead we
// set a redirect policy that always errors. This stops Go from executing the redirect.
//
// We have to be a little careful in case the user-provided http.Client has its own CheckRedirect
// policy - if so, we'll run through that policy first.
//
// Because this requires modifying the http.Client, we make a new copy of the client and return it.
func withoutRedirects(in *http.Client) *http.Client {
	copy := *in
	copy.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		if in.CheckRedirect != nil {
			// Run the input's redirect if it exists, in case it has side effects, but ignore any error it
			// returns, since we want to use ErrUseLastResponse.
			err := in.CheckRedirect(req, via)
			_ = err // Silly, but this makes sure generated code passes errcheck -blank, which some people use.
		}
		return http.ErrUseLastResponse
	}
	return &copy
}

// doProtoRequest is common code to make a request to the remote twirp service.
func doProtoRequest(ctx context.Context, client *http.Client, url string, in, out proto.Message) error {
	var err error
	reqBodyBytes, err := proto.Marshal(in)
	if err != nil {
		return clientError("failed to marshal proto request", err)
	}
	reqBody := bytes.NewBuffer(reqBodyBytes)
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	req, err := newRequest(ctx, url, reqBody, "application/protobuf")
	if err != nil {
		return clientError("could not build request", err)
	}
	resp, err := ctxhttp.Do(ctx, client, req)
	if err != nil {
		return clientError("failed to do request", err)
	}
	defer closebody(resp.Body)
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	if resp.StatusCode != 200 {
		return errorFromResponse(resp)
	}

	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return clientError("failed to read response body", err)
	}
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	if err = proto.Unmarshal(respBodyBytes, out); err != nil {
		return clientError("failed to unmarshal proto response", err)
	}
	return nil
}

// doJSONRequest is common code to make a request to the remote twirp service.
func doJSONRequest(ctx context.Context, client *http.Client, url string, in, out proto.Message) error {
	var err error
	reqBody := bytes.NewBuffer(nil)
	marshaler := &jsonpb.Marshaler{OrigName: true}
	if err = marshaler.Marshal(reqBody, in); err != nil {
		return clientError("failed to marshal json request", err)
	}
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	req, err := newRequest(ctx, url, reqBody, "application/json")
	if err != nil {
		return clientError("could not build request", err)
	}
	resp, err := ctxhttp.Do(ctx, client, req)
	if err != nil {
		return clientError("failed to do request", err)
	}
	defer closebody(resp.Body)
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	if resp.StatusCode != 200 {
		return errorFromResponse(resp)
	}

	unmarshaler := jsonpb.Unmarshaler{AllowUnknownFields: true}
	if err = unmarshaler.Unmarshal(resp.Body, out); err != nil {
		return clientError("failed to unmarshal json response", err)
	}
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}
	return nil
}

var twirpFileDescriptor0 = []byte{
	// 372 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xcc, 0x54, 0x4d, 0x4f, 0xc2, 0x40,
	0x10, 0xb5, 0x20, 0x0a, 0x23, 0x92, 0xba, 0x1e, 0x84, 0x7a, 0x31, 0x3d, 0x19, 0x0e, 0x35, 0xc1,
	0xa8, 0x47, 0x23, 0x4a, 0x10, 0x03, 0xa5, 0xa9, 0xe5, 0x80, 0x17, 0x52, 0xca, 0x26, 0xd4, 0xd8,
	0x6e, 0xdd, 0x0f, 0xfc, 0x01, 0x26, 0xde, 0xf4, 0x17, 0xf8, 0x63, 0x8d, 0x4b, 0x43, 0x24, 0xb4,
	0x10, 0xc2, 0xc5, 0xe3, 0x7c, 0xbc, 0x99, 0xf7, 0x66, 0x5f, 0x16, 0x8e, 0x68, 0xe4, 0x9d, 0x85,
	0x3e, 0xa7, 0x64, 0xc0, 0x30, 0x9d, 0xf8, 0x1e, 0x36, 0x22, 0x4a, 0x38, 0x41, 0x9a, 0x47, 0x46,
	0xd8, 0x78, 0x16, 0x8c, 0xfb, 0xa1, 0xc1, 0x27, 0x06, 0x73, 0x03, 0xc1, 0x0c, 0xd9, 0xa9, 0x5f,
	0x43, 0xa5, 0x89, 0xb9, 0x45, 0x71, 0xe0, 0x8b, 0xe0, 0x91, 0xbb, 0x5c, 0x30, 0xcc, 0x6c, 0xfc,
	0x2a, 0x30, 0xe3, 0x48, 0x87, 0x22, 0x7f, 0xf3, 0xb9, 0x37, 0xee, 0x31, 0x4c, 0x5b, 0x77, 0x65,
	0xe5, 0x44, 0x39, 0x2d, 0xd8, 0x73, 0x39, 0xdd, 0x01, 0x2d, 0x69, 0x00, 0x8b, 0x48, 0xc8, 0x30,
	0xd2, 0x20, 0x3f, 0x76, 0x99, 0x23, 0xe8, 0x90, 0x48, 0x74, 0xde, 0x9e, 0xc5, 0x71, 0xcd, 0xa2,
	0x7e, 0x80, 0xcb, 0x99, 0x59, 0x4d, 0xc6, 0xfa, 0xa7, 0x02, 0x95, 0x26, 0x75, 0xc3, 0xf9, 0xc1,
	0x6b, 0xf0, 0x42, 0x6d, 0xd8, 0x8b, 0x28, 0x19, 0x09, 0x8f, 0x9b, 0x6e, 0xbc, 0xa0, 0x54, 0xab,
	0x1a, 0xe9, 0xa7, 0x30, 0xe2, 0x55, 0xd6, 0x14, 0x65, 0xff, 0x85, 0x4b, 0x95, 0x09, 0x74, 0x36,
	0x54, 0xf9, 0xa5, 0x80, 0x76, 0xeb, 0x86, 0x1e, 0x7e, 0xf9, 0x27, 0x32, 0x7b, 0x70, 0x9c, 0xc8,
	0x67, 0x33, 0x9d, 0xd5, 0x07, 0x28, 0xcd, 0x6f, 0x45, 0x79, 0xd8, 0x36, 0xbb, 0x66, 0x43, 0xdd,
	0x42, 0x05, 0xc8, 0x59, 0x76, 0xab, 0xd3, 0x50, 0x15, 0x74, 0x00, 0xfb, 0x4e, 0xcf, 0xae, 0x77,
	0x07, 0x9d, 0xae, 0xe9, 0xdc, 0xb7, 0xfb, 0x6a, 0x06, 0xa9, 0x50, 0x9c, 0xa6, 0xfa, 0x8d, 0x1b,
	0xbb, 0xdd, 0x57, 0xb3, 0xb5, 0xef, 0x2c, 0xe4, 0xcc, 0x5f, 0x21, 0xe8, 0x5d, 0x01, 0xb4, 0x68,
	0x3d, 0x74, 0xb1, 0x4c, 0x7c, 0xaa, 0xd7, 0xb5, 0xcb, 0x75, 0x61, 0xf1, 0x4d, 0x24, 0x8b, 0x05,
	0x6b, 0xac, 0x60, 0x91, 0xe6, 0xec, 0x15, 0x2c, 0xd2, 0x1d, 0xf8, 0xa1, 0xc0, 0x61, 0xc2, 0xcb,
	0xa1, 0xa5, 0xf3, 0xd2, 0xad, 0xa7, 0x5d, 0xad, 0x8d, 0x9b, 0x12, 0xa9, 0xef, 0x3e, 0xe5, 0x64,
	0xd3, 0x70, 0x47, 0xfe, 0x3d, 0xe7, 0x3f, 0x01, 0x00, 0x00, 0xff, 0xff, 0x26, 0x9d, 0xbd, 0x63,
	0x96, 0x04, 0x00, 0x00,
}
