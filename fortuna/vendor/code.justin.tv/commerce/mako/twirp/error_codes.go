package mako_client

// These error codes are meant to be returned within a mako_client.ClientError type.

const (
	// Generic error codes
	ErrCodeMissingRequiredArgument = "MISSING_REQUIRED_ARGUMENT"

	// UploadEmoticon
	ErrCodeInvalidImageDimensions = "INVALID_IMAGE_DIMENSIONS"

	// CreateEmoticon
	ErrCodeCodeNotUnique       = "CODE_NOT_UNIQUE"
	ErrCodeInvalidImageUpload  = "INVALID_IMAGE_UPLOADED"
	ErrCodeInvalidSuffixFormat = "INVALID_SUFFIX_FORMAT"
	ErrCodeImage28IDNotFound   = "IMAGE28ID_NOT_FOUND"
	ErrCodeImage56IDNotFound   = "IMAGE56ID_NOT_FOUND"
	ErrCodeImage112IDNotFound  = "IMAGE112ID_NOT_FOUND"

	// DeleteEmoticon
	ErrUserNotPermitted  = "USER_NOT_PERMITTED"
	ErrEmoteDoesNotExist = "EMOTE_DOES_NOT_EXIST"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
