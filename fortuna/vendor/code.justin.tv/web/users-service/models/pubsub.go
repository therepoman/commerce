package models

import "code.justin.tv/common/yimg"

type UploadImageEvent struct {
	UserID    string      `json:"user_id"`
	Type      string      `json:"type"`
	ImageType string      `json:"image_type"`
	NewImage  yimg.Images `json:"new_image"`
	Status    string      `json:"status"`
	UploadID  string      `json:"upload_id"`
}

type UpdateChannelPropertiesEvent struct {
	ChannelID string  `json:"channel_id"`
	Type      string  `json:"type"`
	Channel   string  `json:"channel"`
	OldStatus *string `json:"old_status"`
	Status    *string `json:"status"`
	OldGame   *string `json:"old_game"`
	Game      *string `json:"game"`
	OldGameID *uint64 `json:"old_game_id"`
	GameID    *uint64 `json:"game_id"`
}
