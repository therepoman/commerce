// NOTE this is a generated file! do not edit!

package user_subscribe_user_notice

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "UserSubscribeUserNoticeCreate"
)

type Create = UserSubscribeUserNoticeCreate

type UserSubscribeUserNoticeCreateHandler func(context.Context, *eventbus.Header, *UserSubscribeUserNoticeCreate) error

func (h UserSubscribeUserNoticeCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserSubscribeUserNoticeCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserSubscribeUserNoticeCreateHandler(mux *eventbus.Mux, f UserSubscribeUserNoticeCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f UserSubscribeUserNoticeCreateHandler) {
	RegisterUserSubscribeUserNoticeCreateHandler(mux, f)
}

func (*UserSubscribeUserNoticeCreate) EventBusName() string {
	return CreateEventType
}
