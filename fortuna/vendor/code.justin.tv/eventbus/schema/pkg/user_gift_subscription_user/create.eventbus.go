// NOTE this is a generated file! do not edit!

package user_gift_subscription_user

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "UserGiftSubscriptionUserCreate"
)

type Create = UserGiftSubscriptionUserCreate

type UserGiftSubscriptionUserCreateHandler func(context.Context, *eventbus.Header, *UserGiftSubscriptionUserCreate) error

func (h UserGiftSubscriptionUserCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserGiftSubscriptionUserCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserGiftSubscriptionUserCreateHandler(mux *eventbus.Mux, f UserGiftSubscriptionUserCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f UserGiftSubscriptionUserCreateHandler) {
	RegisterUserGiftSubscriptionUserCreateHandler(mux, f)
}

func (*UserGiftSubscriptionUserCreate) EventBusName() string {
	return CreateEventType
}
