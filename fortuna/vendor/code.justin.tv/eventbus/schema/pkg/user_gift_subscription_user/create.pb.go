// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user_gift_subscription_user/create.proto

package user_gift_subscription_user

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// SubTier is the enumerated values that a sub is normally.
type SubTier int32

const (
	// default case is an invalid sub tier
	SubTier_SUB_TIER_INVALID SubTier = 0
	// tier 1 subs
	SubTier_SUB_TIER_1 SubTier = 1
	// tier 2 subs
	SubTier_SUB_TIER_2 SubTier = 2
	// tier 3 subs
	SubTier_SUB_TIER_3 SubTier = 3
)

var SubTier_name = map[int32]string{
	0: "SUB_TIER_INVALID",
	1: "SUB_TIER_1",
	2: "SUB_TIER_2",
	3: "SUB_TIER_3",
}
var SubTier_value = map[string]int32{
	"SUB_TIER_INVALID": 0,
	"SUB_TIER_1":       1,
	"SUB_TIER_2":       2,
	"SUB_TIER_3":       3,
}

func (x SubTier) String() string {
	return proto.EnumName(SubTier_name, int32(x))
}
func (SubTier) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_create_6e6c9b5fc3981213, []int{0}
}

// UserGiftSubscriptionUserCreate is triggered when a subscription entitlement has been gifted in the user's channel.
type UserGiftSubscriptionUserCreate struct {
	// the purchaser (i.e. twitch viewer) of the subscription entitlement (i.e. viewer who is subscribing)
	// This value is the anonymous user when the gift is anonymous
	FromUserId string `protobuf:"bytes,1,opt,name=from_user_id,json=fromUserId,proto3" json:"from_user_id,omitempty"`
	// the user/broadcaster that the subscriber is entitled subscription benefits to (i.e. twitch.tv/michael)
	ToUserId string `protobuf:"bytes,2,opt,name=to_user_id,json=toUserId,proto3" json:"to_user_id,omitempty"`
	// The amount of gifts that were sent
	Quantity int64 `protobuf:"varint,3,opt,name=quantity,proto3" json:"quantity,omitempty"`
	// The payments ID that triggered the gift purchase
	OriginId string `protobuf:"bytes,4,opt,name=origin_id,json=originId,proto3" json:"origin_id,omitempty"`
	// The subscription product that's being gifted
	ProductId string `protobuf:"bytes,5,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	// If the gift purchaser is anonymous
	IsAnonymous bool `protobuf:"varint,6,opt,name=is_anonymous,json=isAnonymous,proto3" json:"is_anonymous,omitempty"`
	// The subscription product tier
	Tier                 *Tier    `protobuf:"bytes,7,opt,name=tier,proto3" json:"tier,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserGiftSubscriptionUserCreate) Reset()         { *m = UserGiftSubscriptionUserCreate{} }
func (m *UserGiftSubscriptionUserCreate) String() string { return proto.CompactTextString(m) }
func (*UserGiftSubscriptionUserCreate) ProtoMessage()    {}
func (*UserGiftSubscriptionUserCreate) Descriptor() ([]byte, []int) {
	return fileDescriptor_create_6e6c9b5fc3981213, []int{0}
}
func (m *UserGiftSubscriptionUserCreate) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserGiftSubscriptionUserCreate.Unmarshal(m, b)
}
func (m *UserGiftSubscriptionUserCreate) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserGiftSubscriptionUserCreate.Marshal(b, m, deterministic)
}
func (dst *UserGiftSubscriptionUserCreate) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserGiftSubscriptionUserCreate.Merge(dst, src)
}
func (m *UserGiftSubscriptionUserCreate) XXX_Size() int {
	return xxx_messageInfo_UserGiftSubscriptionUserCreate.Size(m)
}
func (m *UserGiftSubscriptionUserCreate) XXX_DiscardUnknown() {
	xxx_messageInfo_UserGiftSubscriptionUserCreate.DiscardUnknown(m)
}

var xxx_messageInfo_UserGiftSubscriptionUserCreate proto.InternalMessageInfo

func (m *UserGiftSubscriptionUserCreate) GetFromUserId() string {
	if m != nil {
		return m.FromUserId
	}
	return ""
}

func (m *UserGiftSubscriptionUserCreate) GetToUserId() string {
	if m != nil {
		return m.ToUserId
	}
	return ""
}

func (m *UserGiftSubscriptionUserCreate) GetQuantity() int64 {
	if m != nil {
		return m.Quantity
	}
	return 0
}

func (m *UserGiftSubscriptionUserCreate) GetOriginId() string {
	if m != nil {
		return m.OriginId
	}
	return ""
}

func (m *UserGiftSubscriptionUserCreate) GetProductId() string {
	if m != nil {
		return m.ProductId
	}
	return ""
}

func (m *UserGiftSubscriptionUserCreate) GetIsAnonymous() bool {
	if m != nil {
		return m.IsAnonymous
	}
	return false
}

func (m *UserGiftSubscriptionUserCreate) GetTier() *Tier {
	if m != nil {
		return m.Tier
	}
	return nil
}

// Tier the tier value of sub that is attached.
type Tier struct {
	// the value can be either
	// - an enumerated value (tier 1, 2, 3 subs)
	// - a custom string value (OWL ticket product, legacy ticket products)
	//
	// Types that are valid to be assigned to Value:
	//	*Tier_Numeral
	//	*Tier_Custom
	Value                isTier_Value `protobuf_oneof:"value"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *Tier) Reset()         { *m = Tier{} }
func (m *Tier) String() string { return proto.CompactTextString(m) }
func (*Tier) ProtoMessage()    {}
func (*Tier) Descriptor() ([]byte, []int) {
	return fileDescriptor_create_6e6c9b5fc3981213, []int{1}
}
func (m *Tier) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Tier.Unmarshal(m, b)
}
func (m *Tier) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Tier.Marshal(b, m, deterministic)
}
func (dst *Tier) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Tier.Merge(dst, src)
}
func (m *Tier) XXX_Size() int {
	return xxx_messageInfo_Tier.Size(m)
}
func (m *Tier) XXX_DiscardUnknown() {
	xxx_messageInfo_Tier.DiscardUnknown(m)
}

var xxx_messageInfo_Tier proto.InternalMessageInfo

type isTier_Value interface {
	isTier_Value()
}

type Tier_Numeral struct {
	Numeral SubTier `protobuf:"varint,1,opt,name=numeral,proto3,enum=user_gift_subscription_user.SubTier,oneof"`
}

type Tier_Custom struct {
	Custom string `protobuf:"bytes,2,opt,name=custom,proto3,oneof"`
}

func (*Tier_Numeral) isTier_Value() {}

func (*Tier_Custom) isTier_Value() {}

func (m *Tier) GetValue() isTier_Value {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *Tier) GetNumeral() SubTier {
	if x, ok := m.GetValue().(*Tier_Numeral); ok {
		return x.Numeral
	}
	return SubTier_SUB_TIER_INVALID
}

func (m *Tier) GetCustom() string {
	if x, ok := m.GetValue().(*Tier_Custom); ok {
		return x.Custom
	}
	return ""
}

// XXX_OneofFuncs is for the internal use of the proto package.
func (*Tier) XXX_OneofFuncs() (func(msg proto.Message, b *proto.Buffer) error, func(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error), func(msg proto.Message) (n int), []interface{}) {
	return _Tier_OneofMarshaler, _Tier_OneofUnmarshaler, _Tier_OneofSizer, []interface{}{
		(*Tier_Numeral)(nil),
		(*Tier_Custom)(nil),
	}
}

func _Tier_OneofMarshaler(msg proto.Message, b *proto.Buffer) error {
	m := msg.(*Tier)
	// value
	switch x := m.Value.(type) {
	case *Tier_Numeral:
		b.EncodeVarint(1<<3 | proto.WireVarint)
		b.EncodeVarint(uint64(x.Numeral))
	case *Tier_Custom:
		b.EncodeVarint(2<<3 | proto.WireBytes)
		b.EncodeStringBytes(x.Custom)
	case nil:
	default:
		return fmt.Errorf("Tier.Value has unexpected type %T", x)
	}
	return nil
}

func _Tier_OneofUnmarshaler(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error) {
	m := msg.(*Tier)
	switch tag {
	case 1: // value.numeral
		if wire != proto.WireVarint {
			return true, proto.ErrInternalBadWireType
		}
		x, err := b.DecodeVarint()
		m.Value = &Tier_Numeral{SubTier(x)}
		return true, err
	case 2: // value.custom
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		x, err := b.DecodeStringBytes()
		m.Value = &Tier_Custom{x}
		return true, err
	default:
		return false, nil
	}
}

func _Tier_OneofSizer(msg proto.Message) (n int) {
	m := msg.(*Tier)
	// value
	switch x := m.Value.(type) {
	case *Tier_Numeral:
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(x.Numeral))
	case *Tier_Custom:
		n += 1 // tag and wire
		n += proto.SizeVarint(uint64(len(x.Custom)))
		n += len(x.Custom)
	case nil:
	default:
		panic(fmt.Sprintf("proto: unexpected type %T in oneof", x))
	}
	return n
}

func init() {
	proto.RegisterType((*UserGiftSubscriptionUserCreate)(nil), "user_gift_subscription_user.UserGiftSubscriptionUserCreate")
	proto.RegisterType((*Tier)(nil), "user_gift_subscription_user.Tier")
	proto.RegisterEnum("user_gift_subscription_user.SubTier", SubTier_name, SubTier_value)
}

func init() {
	proto.RegisterFile("user_gift_subscription_user/create.proto", fileDescriptor_create_6e6c9b5fc3981213)
}

var fileDescriptor_create_6e6c9b5fc3981213 = []byte{
	// 344 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x92, 0x4d, 0x6b, 0xf2, 0x40,
	0x14, 0x85, 0x8d, 0x5f, 0xd1, 0xab, 0x48, 0x18, 0xde, 0x45, 0x78, 0xad, 0x25, 0x4a, 0x17, 0xa1,
	0x0b, 0x4b, 0x95, 0xee, 0xab, 0x6d, 0xa9, 0x81, 0xd2, 0x42, 0xd4, 0x2e, 0xba, 0x19, 0x62, 0x32,
	0xca, 0x50, 0x93, 0xb1, 0xf3, 0x51, 0xf0, 0x6f, 0xf4, 0x17, 0x97, 0x19, 0xa3, 0xd4, 0x4d, 0x96,
	0xe7, 0x39, 0xe7, 0x5c, 0x98, 0x3b, 0x17, 0x7c, 0x25, 0x08, 0xc7, 0x1b, 0xba, 0x96, 0x58, 0xa8,
	0x95, 0x88, 0x39, 0xdd, 0x49, 0xca, 0x32, 0xac, 0xf1, 0x4d, 0xcc, 0x49, 0x24, 0xc9, 0x70, 0xc7,
	0x99, 0x64, 0xa8, 0x5b, 0x90, 0x1c, 0xfc, 0x94, 0xe1, 0x72, 0x29, 0x08, 0x7f, 0xa6, 0x6b, 0x39,
	0xff, 0xe3, 0x6a, 0xf6, 0x60, 0xa6, 0x20, 0x0f, 0xda, 0x6b, 0xce, 0x52, 0x93, 0xc7, 0x34, 0x71,
	0x2d, 0xcf, 0xf2, 0x9b, 0x21, 0x68, 0xa6, 0x53, 0x41, 0x82, 0x2e, 0x00, 0x24, 0x3b, 0xf9, 0x65,
	0xe3, 0x37, 0x24, 0xcb, 0xdd, 0xff, 0xd0, 0xf8, 0x52, 0x51, 0x26, 0xa9, 0xdc, 0xbb, 0x15, 0xcf,
	0xf2, 0x2b, 0xe1, 0x49, 0xa3, 0x2e, 0x34, 0x19, 0xa7, 0x1b, 0x9a, 0xe9, 0x62, 0xf5, 0x50, 0x3c,
	0x80, 0x20, 0x41, 0x3d, 0x80, 0x1d, 0x67, 0x89, 0x8a, 0xa5, 0x76, 0x6b, 0xc6, 0x6d, 0xe6, 0x24,
	0x48, 0x50, 0x1f, 0xda, 0x54, 0xe0, 0x28, 0x63, 0xd9, 0x3e, 0x65, 0x4a, 0xb8, 0x75, 0xcf, 0xf2,
	0x1b, 0x61, 0x8b, 0x8a, 0xc9, 0x11, 0xa1, 0x3b, 0xa8, 0x4a, 0x4a, 0xb8, 0x6b, 0x7b, 0x96, 0xdf,
	0x1a, 0xf5, 0x87, 0x05, 0x9b, 0x18, 0x2e, 0x28, 0xe1, 0xa1, 0x89, 0x0f, 0x3e, 0xa1, 0xaa, 0x15,
	0xba, 0x07, 0x3b, 0x53, 0x29, 0xe1, 0xd1, 0xd6, 0x3c, 0xba, 0x33, 0xba, 0x2a, 0x9c, 0x30, 0x57,
	0x2b, 0x5d, 0x9b, 0x95, 0xc2, 0x63, 0x0d, 0xb9, 0x50, 0x8f, 0x95, 0x90, 0x2c, 0x3d, 0x6c, 0x65,
	0x56, 0x0a, 0x73, 0x3d, 0xb5, 0xa1, 0xf6, 0x1d, 0x6d, 0x15, 0xb9, 0x7e, 0x03, 0x3b, 0x2f, 0xa2,
	0x7f, 0xe0, 0xcc, 0x97, 0x53, 0xbc, 0x08, 0x9e, 0x42, 0x1c, 0xbc, 0xbe, 0x4f, 0x5e, 0x82, 0x47,
	0xa7, 0x84, 0x3a, 0x00, 0x27, 0x7a, 0xeb, 0x58, 0x67, 0x7a, 0xe4, 0x94, 0xcf, 0xf4, 0xd8, 0xa9,
	0x4c, 0x7b, 0x1f, 0x45, 0x3f, 0xbe, 0xaa, 0x9b, 0xab, 0x18, 0xff, 0x06, 0x00, 0x00, 0xff, 0xff,
	0xe5, 0x58, 0x3c, 0x69, 0x41, 0x02, 0x00, 0x00,
}
