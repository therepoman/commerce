package sqspoller

import "time"

const (
	// These values are defined by the AWS docs at
	// https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_ReceiveMessage.html
	maxMessagesPerPoll   = 10
	receiveWaitTime      = 20 * time.Second
	maxIdsPerBatchDelete = 10

	// These are defined based on app sane limits
	receiveMessageMaxWaitTime = 60 * time.Second
	minAckFrequency           = 100 * time.Millisecond
)
