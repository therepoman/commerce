package cfnversion

import (
	"context"

	"github.com/aws/aws-sdk-go/aws/client"
)

// RequireCheck use this interface to mock out responses to Require in testing
type Fetcher interface {
	Require(context.Context, client.ConfigProvider, string) (bool, error)
}

type Checker struct{}

func NewFetcher() *Checker {
	return &Checker{}
}

func (c *Checker) Require(ctx context.Context, sess client.ConfigProvider, version string) (bool, error) {
	return Require(ctx, sess, version)
}
