package events

import (
	"time"
)

// EventNames and their respective params are defined here
// Add the event as a constant ending in Event

// Event Name and params for comment published
// This is published in Flipper
const CommentPublishedEvent = "comment-published"

type CommentPublishedParams struct {
	CommentID string `json:"comment_id" validate:"nonzero"`
}

// EventName and params for autohost list added (deprecated)
const AutohostListAddedEvent = "autohost_list_added"

// EventName and params for automod word added
const AutomodWordsAddedEvent = "automod_words_added"

type AutomodWordsAddedParams struct {
	ChannelID string        `json:"channel_id" validate:"nonzero"`
	Words     []AutomodWord `json:"moderated_words"`
}

type AutomodWord struct {
	Category  string   `json:"category"`
	Fragments []string `json:"fragments"`
}

// EventName and params for affiliate invite
const AffiliateInviteEvent = "affiliate_invite"

type AffiliateInviteParams struct {
	ChannelID int `json:"channel_id"`
}

// EventName and params for global emotes uploaded
const GlobalEmotesUploadedEvent = "global_emotes_uploaded"

type GlobalEmotesUploadedParams struct {
	UploaderUserName string `json:"uploader_user_name"`
	UploaderEmail    string `json:"uploader_email"`
	RecipientEmail   string `json:"recipient_email"`
	Emote28URL       string `json:"emote_28_url"`
	Emote56URL       string `json:"emote_56_url"`
	Emote128URL      string `json:"emote_128_url"`
}

// EventName and params for emote updated
const EmoteUpdatedEvent = "emote_updated"

type EmoteUpdatedParams struct {
	UserID         string `json:"user_id"`
	PreviousState  string `json:"previous_state"`
	NewState       string `json:"new_state"`
	PreviousPrefix string `json:"previous_prefix"`
	NewPrefix      string `json:"new_prefix"`
}

// EventName and params for emote review email
const EmoteReviewedEvent = "emote_reviewed"

type EmoteReviewedParams struct {
	UserID    string `json:"user_id"`
	State     string `json:"state"`
	Reason    string `json:"reason"`
	EmoteCode string `json:"emote_code"`
	EmoteID   string `json:"emote_id"`
}

// EventName and params for emote prefix review email
const EmotePrefixReviewedEvent = "emote_prefix_reviewed"

type EmotePrefixReviewedParams struct {
	UserID string `json:"user_id"`
	State  string `json:"state"`
	Reason string `json:"reason"`
	Prefix string `json:"prefix"`
}

// EventName and params for badge takedown email
const BadgeRemovedEvent = "badge_removed"

type BadgeRemovedParams struct {
	UserID    string   `json:"user_id"`
	UserEmail string   `json:"user_email"`
	Reason    string   `json:"reason"`
	BadgeType string   `json:"badge_type"`
	BadgeIDs  []string `json:"badge_ids"`
}

// EventName and params for login change
const LoginChangeEvent = "login_change"

type LoginChangeParams struct {
	RecipientID string `json:"recipient_id"`
	Language    string `json:"lang"`
}

// AccountRecoveryEvent is the EventName for the account recovery event
// Published by admin-panel service
// https://git-aws.internal.justin.tv/web/admin-panel
const AccountRecoveryEvent = "account_recovery"

// AccountRecoveryParams defines the blob of information necessary to process
// a password recovery event
type AccountRecoveryParams struct {
	UserID string `json:"user_id"`
}

// PartnerInviteEvent is the EventName for partner invitations
const PartnerInviteEvent = "partner_invite"

// PartnerInviteParams is the params needed to process a partner invite event
type PartnerInviteParams struct {
	ChannelID int `json:"channel_id"`
}

// EventName and params for password reset
const PasswordResetEvent = "passwordreset"

type PasswordResetParams struct {
	RecipientID string `json:"recipient_id"`
	ResetURI    string `json:"reset_uri"`
	Language    string `json:"lang"`
}

// EventName and params for payments refund
const PaymentsRefundEvent = "payments_refund"

type PaymentsRefundParams struct {
	AdminLDAPEmail             string   `json:"admin_ldap_email"` // The admin who triggered refunds
	AdminLDAPLogin             string   `json:"admin_ldap_login"`
	RefundedUserID             string   `json:"refunded_user_id"`
	FailedRefundPaymentIDs     []string `json:"failed_refund_payment_ids"`
	SuccessfulRefundPaymentIDs []string `json:"successful_refund_payment_ids"`
}

// EventName and params for purchase profile report
const PurchaseProfileReportEvent = "purchase_profile_report"

type PurchaseProfileReportParams struct {
	AdminLDAPEmail string `json:"admin_ldap_email"` // The admin who requested the purchase profiles
	AdminLDAPLogin string `json:"admin_ldap_login"`
	CSVURL         string `json:"csv_url"`
}

// EventName and params for ban user
const BanUserEvent = "ban_user"

type BanUserParams struct {
	RecipientID string `json:"id"`
	Type        string `json:"type"`
	Warn        bool   `json:"warn"`
	Reason      string `json:"reason"`
	Description string `json:"description" `
	Duration    int64  `json:"duration" `
	ContentType string `json:"content_type"`
}

// EventName and params for unread whisper notification
const UnreadWhisperEvent = "whisper"

type UnreadWhisperParams struct {
	ThreadID      string    `json:"thread_id"`
	SenderID      string    `json:"sender_id"`
	RecipientID   string    `json:"recipient_id"`
	MessageText   string    `json:"message_text"`
	SentTimestamp time.Time `json:"sent_timestamp"`
}

// CustomPartnerActivation is the EventName for custom partner activations
const CustomPartnerActivation = "custom_partner_activation"

// CustomPartnerActivationParams is the param needed to process a custom partner activation event
type CustomPartnerActivationParams struct {
	ChannelID string `json:"channel_id"`
}

const PartnerAgreementUpdateEvent = "partner_agreement_update"

type PartnerAgreementUpdateParams struct {
	ChannelID int `json:"channel_id"`
}

const AuthorizedBroadcasterEvent = "authorized_broadcaster"

type AuthorizedBroadcasterParams struct {
	ChannelID string `json:"channel_id" validate:"nonzero"`
	UserEmail string `json:"user_email" validate:"nonzero"`
	StreamKey string `json:"stream_key" validate:"nonzero"`
}

// DeveloperWhitelistedEvent is the EventName for developer whitelisting events
const DeveloperWhitelistedEvent = "developer_whitelisted"

// DeveloperWhitelistedParams are the params needed to process a developer whitelist event
type DeveloperWhitelistedParams struct {
	ChannelID string `json:"channel_id" validate:"nonzero"`
}

// EventName and params for unread chatroom mention
const UnreadChatroomMentionEvent = "chatroommention"

type UnreadChatroomMentionParams struct {
	ChannelID string `json:"channel_id" validate:"nonzero"`
	RoomID    string `json:"room_id" validate:"nonzero"`
	RoomName  string `json:"room_name" validate:"nonzero"`
	MessageID string `json:"message_id" validate:"nonzero"`
	SenderID  string `json:"sender_id" validate:"nonzero"`
	// TargetID is deprecated. Use TargetIDs instead.
	TargetID  string    `json:"target_id"`
	TargetIDs []string  `json:"target_ids"`
	Type      string    `json:"type"`
	SentAt    time.Time `json:"sent_at"`
}

// EventName and params for turbo welcome
const TurboWelcomeEvent = "turbo_welcome"

type TurboWelcomeParams struct {
	UserID string `json:"user_id"`
}

const EmailSubscriber = "email_subscriber"

type EmailSubscriberParams struct {
	Title     string `json:"title"`
	Text      string `json:"text"`
	ChannelID string `json:"channel_id"`
}

// EventName and params for mass do-not-renew subscriptions results
const SubscriptionsMassDNREvent = "subscriptions_mass_dnr"

type SubscriptionsMassDNRParams struct {
	AdminLDAPEmail               string   `json:"admin_ldap_email"` // The admin who triggered mass DNR
	TargetProductID              string   `json:"target_product_id"`
	SuccessDNRPurchaseProfileIDs []string `json:"success_dnr_purchase_profile_ids"`
	FailDNRPurchaseProfileIDs    []string `json:"fail_dnr_purchase_profile_ids"`
	Language                     string   `json:"language"`
}

// ForgotUsernameEvent is a constant name used to refer to forgot username emails
const ForgotUsernameEvent = "forgot_username"

// ForgotUsernameParams defines the structure of necessary parameters for triggering
// a "forgot username" email sent to the target address.
type ForgotUsernameParams struct {
	Logins   []string `json:"logins"`
	Email    string   `json:"email"`
	Language string   `json:"language"`
}

// SubscriptionsGiftRefund event name
const SubscriptionsGiftRefundEvent = "subscriptions_gift_refund"

type SubscriptionsGiftRefundParams struct {
	GifterUserID string `json:"gifter_user_id"`
	GifteeUserID string `json:"giftee_user_id"`
	GiftUserID   string `json:"gift_user_id"`
}

// UnexpectedSubGiftRefund event name
const UnexpectedSubGiftRefundEvent = "unexpected_sub_gift_refund"

type UnexpectedSubGiftRefundParams struct {
	GifterUserID string `json:"gifter_user_id"`
	GifteeUserID string `json:"giftee_user_id"`
	GiftUserID   string `json:"gift_user_id"`
}

// AchievementCompleteEvent is used to refer to the achievement complete event
const AchievementCompleteEvent = "achievement_complete"

// AchievementCompleteParams contains the message params for triggering
// an onsite notification for achievement completed events
type AchievementCompleteParams struct {
	ChannelID                string `json:"channel_id"`
	AchievementID            string `json:"achievement_id"`
	AchievementProgressLevel string `json:"achievement_progress_level"`
	BadgeURL                 string `json:"badge_url"`
}

// AchievementQuestCompleteEvent is used to refer to the quest complete event
const AchievementQuestCompleteEvent = "achievement_quest_complete"

// AchievementQuestCompleteParams contains the message params for triggering
// an onsite notification for achievement quest completed events
type AchievementQuestCompleteParams struct {
	ChannelID string `json:"channel_id"`
	QuestID   string `json:"quest_id"`
	BadgeURL  string `json:"badge_url"`
}

// NotificationExperimentsEvent is used to refer to notification experiments events
const NotificationExperimentsEvent = "notification_experiments"

// NotificationExperimentsParams is used to trigger notification experiment jobs
type NotificationExperimentsParams struct {
	JobName          string `json:"job_name" validate:"nonzero"`
	S3BucketName     string `json:"s3_bucket_name" validate:"nonzero"`
	S3Key            string `json:"s3_key" validate:"nonzero"`
	UnstubDeliveries bool   `json:"unstub_deliveries"` // defaults to stubbing for safety
}

// SubGiftReceivedEvent is triggered when a user receives a gift
const SubGiftReceivedEvent = "sub_gift_received"

// SubGiftReceivedParams defines the structure of necessary parameters for triggering
// a "gift received" on-site notification
type SubGiftReceivedParams struct {
	Recipient string `json:"recipient" validate:"nonzero"`
	Sender    string `json:"sender"`
	Product   string `json:"product" validate:"nonzero"`
	Channel   string `json:"channel" validate:"nonzero"`
	Origin    string `json:"origin" validate:"nonzero"`
	TierName  string `json:"tier_name" validate:"nonzero"`
}

// SubscriptionsDNRErrorEvent is used to notify users who had their subscriptions do-not-renew'ed and should manually renew them
const SubscriptionsDNRErrorEvent = "subscriptions_dnr_error"

// Params for triggering email and onsite notifications for "subscription_dnr_error" event
type SubscriptionDNRErrorParams struct {
	UserID   string                 `json:"user_id" validate:"nonzero"`
	Products []*SubscriptionProduct `json:"products"`
}

type SubscriptionProduct struct {
	// The product owner's channel ID
	ChannelID string `json:"channel_id" validate:"nonzero"`
	// The product owner's channel name (will be populated in Pushy)
	ChannelName string `json:"channel_name"`
	// Also known as "short_name", this is the product's display name
	DisplayName string `json:"display_name" validate:"nonzero"`
}

// PremiereStartingSoonEvent triggers when a broadcaster is streaming near a scheduled premiere.
const PremiereStartingSoonEvent = "premiere_starting_soon"

// PremiereFailedNotReadyEvent triggers when near a scheduled premiere, but the video
// is not yet ready to play (likely still processing).
const PremiereFailedNotReadyEvent = "premiere_failed_not_ready"

// PremiereFailedAfterStartingEvent triggers when near a scheduled premiere, but the video
// is not yet ready to play (likely still processing).
const PremiereFailedAfterStartingEvent = "premiere_failed_after_starting"

// CrateGrantEvent is triggered when a user is granted a crate
const CrateGrantEvent = "crategrant"

// CrateGrantParams defines the structure of necessary parameters for triggering
// a "crate granted" on-site notification
type CrateGrantParams struct {
	UserID       string   `json:"tuid" validate:"nonzero"`
	ChannelID    string   `json:"channel_id" validate:"nonzero"`
	Product      string   `json:"product"`
	Origin       string   `json:"origin"`
	CrateIDs     []string `json:"crate_ids"`
	IconImageURL string   `json:"icon_image_url" validate:"nonzero"`
}

// EventName and params for badge_count notification
const BadgeCountEvent = "badge_count"

type BadgeCountParams struct {
	UserID    string `json:"user_id"`
	EventType struct {
		Type  string `json:"Type"`
		Value string `json:"Value"`
	} `json:"event_type"` // inlined struct because snsmodels.MessageAttribute is in an internal package
	EventMessage string `json:"event_message"`
}

// SubscriptionXsollaDNRErrorEvent is used to notify users who had their subscriptions auto do-not-renew'ed and Twitch was
// unable to reinstate them automatically due to invalid or expired payment method on file
const SubscriptionXsollaDNRErrorEvent = "subscription_xsolla_dnr_error"

// Params for triggering email and onsite notifications for "subscription_xsolla_dnr_error" event
type SubscriptionXsollaDNRErrorParams struct {
	UserID   string                 `json:"user_id" validate:"nonzero"`
	Products []*SubscriptionProduct `json:"products"`
	Reason   string                 `json:"reason"`
}

// BitsGrantEvent is triggered when a user is granted Bits
const BitsGrantEvent = "bits_grant"

// BitsGrantParams defines the structure of necessary parameters for triggering
// a "Bits granted" on-site notification
type BitsGrantParams struct {
	UserID       string `json:"user_id" validate:"nonzero"`
	NumBits      int64  `json:"num_bits" validate:"nonzero"`
	Origin       string `json:"origin" validate:"nonzero"`
	IconImageURL string `json:"icon_image_url" validate:"nonzero"`
}

// BitsPromoEvent is triggered when a Bits promo such as holiday discount goes or is about to go live
const BitsPromoEvent = "bits_promo"

// BitsPromoParams defines the structure of necessary parameters for triggering
// a Bits promo on-site notification
type BitsPromoParams struct {
	UserID             string     `json:"user_id" validate:"nonzero"`
	PromoID            string     `json:"promo_id"`
	PromoTitle         string     `json:"promo_title" validate:"nonzero"`
	BitsAmount         int64      `json:"bits_amount"`
	DiscountPercentage int64      `json:"discount_percentage"`
	IconImageURL       string     `json:"icon_image_url" validate:"nonzero"`
	ClickActionURL     string     `json:"click_action_url"`
	StartDate          *time.Time `json:"start_date"`
	EndDate            *time.Time `json:"end_date"`
}

// TeamInviteEvent is the EventName for team invitations
const TeamInviteEvent = "team_invite"

// TeamInviteParams is the params needed to process a team invite event.
type TeamInviteParams struct {
	TeamUserID string `json:"team_user_id"`
	UserID     string `json:"user_id"`
	Type       string `json:"type"`
	TeamName   string `json:"team_name"`
}

// PassGiftReceivedEvent is triggered when a user receives a gift
const PassGiftReceivedEvent = "pass_gift_received" // #nosec

// PassGiftReceivedParams defines the structure of necessary parameters for triggering
// a "gift received" on-site notification (mobile planned)
type PassGiftReceivedParams struct {
	Recipient string `json:"recipient" validate:"nonzero"`
	Sender    string `json:"sender"`
	Channel   string `json:"channel" validate:"nonzero"`
	PassName  string `json:"pass_name" validate:"nonzero"`
}

// DeveloperCompanyAction is triggered when a user's developer company is approved or rejected
const DeveloperCompanyAction = "developer_company_action"

// DeveloperCompanyActionParams defines the structure for triggering a company approved or rejected email
type DeveloperCompanyActionParams struct {
	UserID      string `json:"user_id" validate:"nonzero"`
	EVSKey      string `json:"evs_key"`
	CompanyName string `json:"company_name" validate:"nonzero"`
	Email       string `json:"email"`
	Approved    bool   `json:"approved"`
}

// DeveloperCompanyGameAction is triggered when a user's game application status is updated
const DeveloperCompanyGameAction = "developer_company_game_action"

// DeveloperCompanyGameActionParams defines the structure for triggering a game application update email
type DeveloperCompanyGameActionParams struct {
	UserID   string `json:"user_id" validate:"nonzero"`
	EVSKey   string `json:"evs_key"`
	GameName string `json:"game_name" validate:"nonzero"`
	Email    string `json:"email"`
	Approved bool   `json:"approved"`
}

// DeveloperCompanyUserAction is triggered when a user is added or removed from a developer company
const DeveloperCompanyUserAction = "developer_company_user_action"

// DeveloperCompanyUserActionParams defines the structure for triggering a user added or removed from a developer company email
type DeveloperCompanyUserActionParams struct {
	UserID      string `json:"user_id" validate:"nonzero"`
	EVSKey      string `json:"evs_key"`
	CompanyName string `json:"company_name" validate:"nonzero"`
	Email       string `json:"email"`
	Added       bool   `json:"added"`
}

// VodMuteAppealResolvedEvent is triggered when a mute appeal has been resolved by an admin
const VodMuteAppealResolvedEvent = "vod_mute_appeal_resolved"

// VodMuteTrackAppeal defines the structure for individual track appeal within a VOD appeal.
type VodMuteTrackAppeal struct {
	// The title of the track
	Title string `json:"title"`
	// The performer of the track
	Performer string `json:"performer"`
	// The reason for the track appeal
	Reason string `json:"reason"`
	// Whether the track is still muted. If muted, it was rejected.
	Muted bool `json:"muted"`
	// The localized appeal status generated by i18n
	Status string
}

// VodMuteAppealResolvedParams defines the structure for an appeal resolution notification
type VodMuteAppealResolvedParams struct {
	UserID       string                `json:"user_id"`
	TrackAppeals []*VodMuteTrackAppeal `json:"track_appeals"`
}

// DMCAStrikeEvent is triggered when a user receives a DMCA takedown
const DMCAStrikeEvent = "dmca_strike"

// ContentType flags what type of content is being taken down
type ContentType string

const (
	CLIP       ContentType = "Clip"
	VOD        ContentType = "VOD"
	LiveStream ContentType = "Stream"
)

// DMCAContent is a structure containing all of the information related to a taken down pieces of content
type DMCAContent struct {
	ID          string      `json:"id" validate:"nonzero"`
	Title       string      `json:"title"`
	URL         string      `json:"url"`
	SourceTitle string      `json:"source_title"`
	SourceURL   string      `json:"source_url"`
	CreateTime  time.Time   `json:"submit_time"`
	Type        ContentType `json:"content_type"`
}

// DMCAStrikeParams defines all the fields necessary to send a full dmca takedown notice to a user
type DMCAStrikeParams struct {
	TargetUserID   string        `json:"target_user_id" validate:"nonzero"`
	NoticeID       string        `json:"notice_id" validate:"nonzero"`
	ClaimantUserID string        `json:"claimant_user_id" validate:"nonzero"`
	ClaimantEmail  string        `json:"claimant_email" validate:"nonzero" `
	Claimant       string        `json:"striking_company" validate:"nonzero"`
	ClaimedWork    string        `json:"claimed_work"`
	IssueTime      time.Time     `json:"issue_time"`
	StruckContent  []DMCAContent `json:"struck_content"`
	RelatedContent []DMCAContent `json:"related_content"`
}

// Subtember2018Launch is triggered one off to inform users of the Subtember 2018 promotion
const Subtember2018Launch = "subtember_2018_launch"

// Subtember2018LaunchParams defines fields for Subtember2018Launch
type Subtember2018LaunchParams struct {
	TargetUserID string `json:"target_user_id" validate:"nonzero"`
}

// PrimeGiftSentEvent is triggered when a user gifts a redeemable Twitch Prime offer
const PrimeGiftReceivedEvent = "prime_gift_received_event"

type PrimeGiftReceivedParams struct {
	FromTwitchUserID string `json:"from_twitch_user_id" validate:"nonzero"`
	ToTwitchUserID   string `json:"to_twitch_user_id" validate:"nonzero"`
	OfferID          string `json:"offer_id" validate:"nonzero"`
	OfferTitle       string `json:"offer_title"`
}

// SoftDeleteUser is triggered when a user deactivates their account or requests a hard deletion
const SoftDeleteUserEvent = "soft_delete_user"

type SoftDeleteUserParams struct {
	UserLanguage         string `json:"language"`
	UserID               string `json:"user_id"`
	Login                string `json:"login"`
	EmailAddress         string `json:"email_address" validate:"nonzero"`
	UserRequestedDestroy bool   `json:"user_requested_destroy" validate:"nonzero"`
	Reason               string `json:"reason"`
}

// OWLAAP2018PromotionEvent is triggered to promote Overwatch League 2018 promotional pricing
const OWLAAP2018PromotionEvent = "owl_aap_2018_promo"

type OWLAAP2018PromotionParams struct {
	TargetUserIDs []string `json:"target_user_ids" validate:"nonzero"`
}

const OWLAAP2019PreSeasonEvent = "owl_aap_2019_preseason"

type OWLAAP2019PreSeasonParams struct {
	TargetUserIDs []string `json:"target_user_ids" validate:"nonzero"`
}

// OWLAAP2019LiveEvent is triggered to promote Overwatch League 2019 going live
const OWLAAP2019LiveEvent = "owl_aap_2019_live"

type OWLAAP2019LiveParams struct {
	TargetUserIDs []string `json:"target_user_ids" validate:"nonzero"`
}

// OWLAAP2019RewardsEvent is triggered to remind a user that they can redeem their earned Overwatch League 2019 rewards by buying
// the All-Access Pass
const OWLAAP2019RewardsEvent = "owl_aap_2019_rewards"

type OWLAAP2019RewardsParams struct {
	UserID     string `json:"user_id" validate:"nonzero"`
	NumRewards int    `json:"num_rewards" validate:"nonzero"`
}

// HardDeleteUserEvent is triggered when a user's account is hard deleted
const HardDeleteUserEvent = "hard_delete_user"

type HardDeleteUserParams struct {
	UserLanguage string `json:"language"`
	UserID       string `json:"user_id"`
	Login        string `json:"login"`
	EmailAddress string `json:"email_address"`
}

// StreamSummaryEvent is triggered when a broadcaster's stream summary is ready
const StreamSummaryEvent = "stream_summary"

type QuestData struct {
	Key          string  `json:"key"`
	Days         float64 `json:"days"`
	DaysCap      float64 `json:"days_cap"`
	Followers    float64 `json:"followers"`
	FollowersCap float64 `json:"followers_cap"`
	Hours        float64 `json:"hours"`
	HoursCap     float64 `json:"hours_cap"`
	Viewers      float64 `json:"viewers"`
	ViewersCap   float64 `json:"viewers_cap"`
}

type SnapshotData struct {
	TopClipImageURL        string `json:"top_clip_image_url"`
	TopClipTitle           string `json:"top_clip_title"`
	TopClipURL             string `json:"top_clip_url"`
	TopReferrerDisplayname string `json:"top_referrer_displayname"`
	TopReferrerLogin       string `json:"top_referrer_login"`
	TopReferrerViews       int64  `json:"top_referrer_views"`
}

type StreamData struct {
	AverageViewers   float64       `json:"average_viewers"`
	NewFollowers     int64         `json:"new_followers"`
	NewSubscriptions int64         `json:"new_subscriptions"`
	PeakViewers      int64         `json:"peak_viewers"`
	UniqueViewers    int64         `json:"unique_viewers"`
	StreamDuration   time.Duration `json:"stream_duration"`
}

type StreamSummaryParams struct {
	UserID            string       `json:"user_id"`
	StreamSummaryLink string       `json:"stream_summary_link"`
	RecentStream      StreamData   `json:"recent_stream"`
	PreviousStream    StreamData   `json:"previous_stream"`
	Quest             QuestData    `json:"quest"`
	Snapshot          SnapshotData `json:"snapshot"`
}
