package models

// SelectBadgeParams sets a user's "selected" badge set to the specified badge set
// Same for selecting a badge set globally and selecting a badge set in a channel
type SelectBadgeParams struct {
	BadgeSetID string `json:"badge_set_id"`
}

// UserBadgesRequestParams can retrieve every badge slot for a user
type UserBadgesRequestParams struct {
	// true/false, whether you want the user's global authority badge
	GlobalAuthority bool `json:"global_authority"`
	// channel_id if you want the user's channel authority badge
	// null if you don't want the user's channel authority badge
	ChannelAuthority *int64 `json:"channel_authority"`
	// channel_id if you want the user's subscriber badge
	// null if you don't want the user's subscriber badge
	Subscriber *int64 `json:"subscriber"`
	// true/false, whether you want the user's globally selected badge
	GlobalSelected bool `json:"global_selected"`
	// channel_id if you want the user's channel selected badge
	// null if you don't want the user's channel selected badge
	ChannelSelected *int64 `json:"channel_selected"`
	// a set of user or user/channel properties that are already known by caller
	Metadata Metadata `json:"metadata"`
}

// Metadata is information that the caller already knows about a user
// Every field is optional
type Metadata struct {
	IsStaff      *bool `json:"is_staff"`
	IsAdmin      *bool `json:"is_admin"`
	IsGlobalMod  *bool `json:"is_global_mod"`
	IsTurbo      *bool `json:"is_turbo"`
	IsSubscriber *bool `json:"is_subscriber"`

	ChannelRole *string `json:"channel_role"`
	IsModerator *bool   `json:"is_moderator"` // DEPRECATED: Use Metadata.ChannelRole
}

// Various channel roles for Metadata.ChannelRole:
const (
	ChannelRoleBroadcaster = "broadcaster"
	ChannelRoleModerator   = "moderator"
	ChannelRoleVIP         = "vip"
)

// GrantBadgeParams grants a user a badge version, with an optional end time
// It is intended to be used for endpoints such as /subs and /bits that
// already know the BadgeSetID
type GrantBadgeParams struct {
	BadgeSetVersion string `json:"badge_set_version"`
	EndTimestamp    int64  `json:"end_ts"`
}

// BulkGrantBadgeParams grants badges for multiple user/channel pairs
type BulkGrantBadgeParams struct {
	UserIDs         []int64 `json:"user_ids"`
	ChannelIDs      []int64 `json:"channel_ids"`
	BadgeSetVersion string  `json:"badge_set_version"`
	EndTimestamp    int64   `json:"end_ts"`
}

var (
	FuelBadgeSetIDAnomaly2            = "anomaly-2_1"
	FuelBadgeSetIDAnomalyWarzoneEarth = "anomaly-warzone-earth_1"
	FuelBadgeSetIDThisWarOfMine       = "this-war-of-mine_1"
	FuelBadge1979Revolution           = "1979-revolution_1"
	FuelBadgeAxiomVerge               = "axiom-verge_1"
	FuelBadgeFirewatch                = "firewatch_1"
	FuelBadgeEnterTheGungeon          = "enter-the-gungeon_1"
	FuelBadgeHeavyBullets             = "heavy-bullets_1"
	FuelBadgeJackboxPartyPack         = "jackbox-party-pack_1"
	FuelBadgeKingdomNewLands          = "kingdom-new-lands_1"
	FuelBadgeOkhlos                   = "okhlos_1"
	FuelBadgePsychonauts              = "psychonauts_1"
	FuelBadgeBrokenAge                = "broken-age_1"
	FuelBadgeDarkestDungeon           = "darkest-dungeon_1"
	FuelBadgeTitanSouls               = "titan-souls_1"
	FuelBadgeTyranny                  = "tyranny_1"
	FuelBadgeDevilian                 = "devilian_1"
	FuelBadgeRift                     = "rift_1"
	FuelBadgeBrawlhalla               = "brawlhalla_1"
	FuelBadgeSuperhot                 = "superhot_1"
	FuelBadgeStrafe                   = "strafe_1"
	FuelBadgeHelloNeighbor            = "hello_neighbor_1"
	FuelBadgeDeceit                   = "deceit_1"
	FuelBadgeH1Z1                     = "H1Z1_1"
	FuelBadgeCuphead                  = "cuphead_1"
	FuelBadgeRaidenVDirectorsCut      = "raiden-v-directors-cut_1"
	FuelBadgeFrozenCortext            = "frozen-cortext_1"
	FuelBadgeFrozenSynapse            = "frozen-synapse_1"
	FuelBadgeTheSurge1                = "the-surge_1"
	FuelBadgeTheSurge2                = "the-surge_2"
	FuelBadgeTheSurge3                = "the-surge_3"
	FuelBadgeBubsyTheWoolies          = "bubsy-the-woolies_1"
	FuelBadgeBattlerite               = "battlerite_1"
	FuelBadgeStarbound                = "starbound_1"
	FuelBadgeBattleChefBrigade1       = "battlechefbrigade_1"
	FuelBadgeBattleChefBrigade2       = "battlechefbrigade_2"
	FuelBadgeBattleChefBrigade3       = "battlechefbrigade_3"
	FuelBadgeGettingOverIt1           = "getting-over-it_1"
	FuelBadgeGettingOverIt2           = "getting-over-it_2"
	FuelBadgeInnerspace1              = "innerspace_1"
	FuelBadgeInnerspace2              = "innerspace_2"
	FuelBadgeTreasureAdventureWorld   = "treasure-adventure-world_1"
	FuelBadgeDevilMayCryHD1           = "devil-may-cry-hd_1"
	FuelBadgeDevilMayCryHD2           = "devil-may-cry-hd_2"
	FuelBadgeDevilMayCryHD3           = "devil-may-cry-hd_3"
	FuelBadgeDevilMayCryHD4           = "devil-may-cry-hd_4"
	FuelBadge60Seconds1               = "60-seconds_1"
	FuelBadge60Seconds2               = "60-seconds_2"
	FuelBadge60Seconds3               = "60-seconds_3"
	FuelBadgeDuelyst1                 = "duelyst_1"
	FuelBadgeDuelyst2                 = "duelyst_2"
	FuelBadgeDuelyst3                 = "duelyst_3"
	FuelBadgeDuelyst4                 = "duelyst_4"
	FuelBadgeDuelyst5                 = "duelyst_5"
	FuelBadgeDuelyst6                 = "duelyst_6"
	FuelBadgeDuelyst7                 = "duelyst_7"
	FuelBadgeElderScrollsOnline       = "eso_1"

	OverwatchLeagueInsider1     = "overwatch-league-insider_1"
	OverwatchLeagueInsider2018B = "overwatch-league-insider_2018B"
	OverwatchLeagueInsider2019A = "overwatch-league-insider_2019A"
	OverwatchLeagueInsider2019B = "overwatch-league-insider_2019B"
)

// InvalidateBadgeParams clears the cached image urls
type InvalidateBadgeParams struct {
	UserID     int64 `json:"user_id"`
	OldBadgeID int   `json:"old_badge_id"`
}

// UploadImagesParams uploads images to s3, and sets the display info
// in dynamodb. Versions is a map of badge_set_version to image info
type UploadImagesParams struct {
	Versions map[string]ImageVersion `json:"versions"`
}

// CreateSamusOfferBadgeInfoParams is used to upload images to s3 and set the display info in dynamodb.
// ImageData contains the badge title and the image encoded strings
type CreateSamusOfferBadgeInfoParams struct {
	BadgeSetID string       `json:"badge_set_id"`
	ImageData  ImageVersion `json:"image_data"`
}

// SetSamusOfferBadgeInfoParams is used to uploads images to s3 and set the display info in dynamodb.
// ImageID is the UUID of an existing image in S3, ClickURL is the string of badge click url
type SetSamusOfferBadgeInfoParams struct {
	BadgeSetID string `json:"badge_set_id"`
	BadgeTitle string `json:"title"`
	ImageID    string `json:"image_id"`
	ClickURL   string `json:"click_url"`
}

// ImageVersion contains the display info for a single badge version, including
// The title and the various sizes (in base64-encoded strings)
type ImageVersion struct {
	Title        string `json:"title"`
	ImageBytes1x string `json:"image_bytes_1x"`
	ImageBytes2x string `json:"image_bytes_2x"`
	ImageBytes4x string `json:"image_bytes_4x"`
}

// RemoveImageParams is used to remove a specific version of a channel's sub
// badge image
type RemoveImageParams struct {
	BadgeSetVersion string `json:"badge_set_version"`
}

// RemoveSamusOfferBadgeInfoParams is used to remove a Samus Offer Badge's display info
type RemoveSamusOfferBadgeInfoParams struct {
	BadgeSetID string `json:"badge_set_id"`
}

// BulkRemoveBadgeParams is used to remove a badge from many users at once
type BulkRemoveBadgeParams struct {
	UserIDs []int64 `json:"user_ids"`
}

// BulkDisplayParams allows for lookup of many badge display infos in a single request.
type BulkDisplayParams struct {
	IDs []BadgeID `json:"ids"`
}

const IntelAuthHeader = "IntelAuthorization"
