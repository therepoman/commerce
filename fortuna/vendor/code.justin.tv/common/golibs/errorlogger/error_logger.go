package errorlogger

import "net/http"

// ErrorLogger send errors to external tracking services
type ErrorLogger interface {
	Info(msg string)
	Error(error)
	RequestError(*http.Request, error)
	RequestPanic(*http.Request, interface{})
}
