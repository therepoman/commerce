// Package autoprof provides tools for Go programs to assemble runtime
// profiles of themselves.
package autoprof
