package roll

import (
	"path"
	"reflect"
	"runtime"
	"strings"

	"github.com/pkg/errors"
)

func buildTrace(err error) (trace *apiItemTrace) {
	trace = new(apiItemTrace)

	trace.Exception.Class = errorName(err)
	trace.Exception.Message = err.Error()

	serr, ok := err.(stackTracer)
	if !ok {
		return trace
	}

	for _, frame := range serr.StackTrace() {
		traceFrame := buildTraceFrame(frame)
		if traceFrame == nil {
			continue
		}

		trace.Frames = append(trace.Frames, *traceFrame)
	}

	return trace
}

func buildTraceFrame(frame errors.Frame) (traceFrame *apiItemTraceFrame) {
	// The pkg/errors API is garbage; use runtime instead.
	pc := uintptr(frame) - 1
	fn := runtime.FuncForPC(pc)
	if fn == nil {
		return nil
	}

	file, line := fn.FileLine(pc)
	file = trimGOPATH(fn.Name(), file)
	name := path.Base(fn.Name())

	return &apiItemTraceFrame{
		FileName:   file,
		LineNumber: line,
		Method:     name,
	}
}

func errorName(err error) (name string) {
	// Get the root error.
	err = errors.Cause(err)

	name = reflect.TypeOf(err).String()

	if name == "" {
		name = "panic"
	} else {
		name = strings.TrimPrefix(name, "*")
	}

	return name
}

/*
The trimGOPATH function is taken from: github.com/pkg/errors

Copyright (c) 2015, Dave Cheney <dave@cheney.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

func trimGOPATH(name, file string) string {
	// Here we want to get the source file path relative to the compile time
	// GOPATH. As of Go 1.6.x there is no direct way to know the compiled
	// GOPATH at runtime, but we can infer the number of path segments in the
	// GOPATH. We note that fn.Name() returns the function name qualified by
	// the import path, which does not include the GOPATH. Thus we can trim
	// segments from the beginning of the file path until the number of path
	// separators remaining is one more than the number of path separators in
	// the function name. For example, given:
	//
	//    GOPATH     /home/user
	//    file       /home/user/src/pkg/sub/file.go
	//    fn.Name()  pkg/sub.Type.Method
	//
	// We want to produce:
	//
	//    pkg/sub/file.go
	//
	// From this we can easily see that fn.Name() has one less path separator
	// than our desired output. We count separators from the end of the file
	// path until it finds two more than in the function name and then move
	// one character forward to preserve the initial path segment without a
	// leading separator.
	const sep = "/"
	goal := strings.Count(name, sep) + 2
	i := len(file)
	for n := 0; n < goal; n++ {
		i = strings.LastIndex(file[:i], sep)
		if i == -1 {
			// not enough separators found, set i so that the slice expression
			// below leaves file unmodified
			i = -len(sep)
			break
		}
	}
	// get back to 0 or trim the leading separator
	file = file[i+len(sep):]
	return file
}
