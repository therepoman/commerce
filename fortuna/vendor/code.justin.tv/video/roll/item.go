package roll

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

type Item struct {
	api      apiItem
	endpoint string

	httpClient *http.Client
}

func (i *Item) SetToken(token string) {
	i.api.AccessToken = token
}

func (i *Item) SetEnvironment(env string) {
	i.api.Data.Environment = env
}

func (i *Item) SetEndpoint(endpoint string) {
	i.endpoint = endpoint
}

func (i *Item) SetTitle(title string) {
	i.api.Data.Title = title
}

func (i *Item) SetTimestamp(t time.Time) {
	i.api.Data.Timestamp = t.Unix()
}

func (i *Item) SetPlatform(platform string) {
	i.api.Data.Platform = platform
}

func (i *Item) SetLanguage(language string) {
	i.api.Data.Language = language
}

func (i *Item) SetHost(host string) {
	if i.api.Data.Server == nil {
		i.api.Data.Server = new(apiItemServer)
	}

	i.api.Data.Server.Host = host
}

func (i *Item) SetVersion(version string) {
	i.api.Data.CodeVersion = version
}

func (i *Item) SetClientName(name string) {
	if i.api.Data.Notifier == nil {
		i.api.Data.Notifier = new(apiItemNotifier)
	}

	i.api.Data.Notifier.Name = name
}

func (i *Item) SetClientVersion(version string) {
	if i.api.Data.Notifier == nil {
		i.api.Data.Notifier = new(apiItemNotifier)
	}

	i.api.Data.Notifier.Version = version
}

func (i *Item) SetCustom(key string, value interface{}) {
	if i.api.Data.Custom == nil {
		i.api.Data.Custom = make(apiItemCustom)
	}

	i.api.Data.Custom[key] = value
}

func (i *Item) SetLevel(level Level) {
	i.api.Data.Level = string(level)
}

func (i *Item) SetError(err error) {
	var errs []error

	// Make sure the first error has a stack trace.
	_, ok := err.(stackTracer)
	if !ok {
		err = errors.WithStack(err)
	}

	for {
		// Only add errors with stack traces.
		_, ok = err.(stackTracer)
		if ok {
			errs = append(errs, err)
		}

		// Keep looping for deeper errors.
		cerr, ok := err.(causer)
		if !ok {
			break
		}

		err = cerr.Cause()
		if err == nil {
			break
		}
	}

	i.SetErrors(errs)
}

func (i *Item) SetErrors(errs []error) {
	for _, err := range errs {
		trace := buildTrace(err)
		i.api.Data.Body.TraceChain = append(i.api.Data.Body.TraceChain, trace)
	}
}

func (i *Item) SetRequest(req *http.Request) {
	apiRequest := &apiItemRequest{
		Url:     req.URL.String(),
		Method:  req.Method,
		Headers: make(map[string]string),
		UserIp:  req.RemoteAddr,
	}

	for name, values := range req.Header {
		apiRequest.Headers[name] = values[0]
	}

	i.api.Data.Request = apiRequest
}

func (i *Item) SetMessage(msg string) {
	i.api.Data.Body.Message = &apiItemMessage{
		Body: msg,
	}
}

func (i *Item) SetFingerprint(finger string) {
	i.api.Data.Fingerprint = finger
}

func (i *Item) SetFingerprintAuto() {
	// TODO
}

func (i *Item) SetUUID(uuid string) {
	i.api.Data.UUID = uuid
}

func (i *Item) SetHttpClient(client *http.Client) {
	i.httpClient = client
}

func (i *Item) Send(ctx context.Context) (err error) {
	err = i.api.Verify()
	if err != nil {
		return err
	}

	body, err := json.Marshal(i.api)
	if err != nil {
		return err
	}

	request, err := http.NewRequest("POST", i.endpoint, bytes.NewReader(body))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")
	request = request.WithContext(ctx)

	httpClient := i.httpClient
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	resp, err := httpClient.Do(request)
	if err != nil {
		return err
	}

	defer func() {
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		err = errors.New(resp.Status)
		return err
	}

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var response apiItemResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return err
	}

	if response.Err != 0 {
		return errors.New(response.Message)
	}

	return nil
}
