package connections

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"code.justin.tv/foundation/twitchclient"

	"golang.org/x/net/context"
)

// YoutubeUser specifies the fields of a Youtube user
type YoutubeUser struct {
	TwitchID     string     `json:"twitch_id"`
	YoutubeID    string     `json:"youtube_id"`
	Token        string     `json:"token,omitempty"`
	RefreshToken string     `json:"refresh_token,omitempty"`
	ExpiresOn    int64      `json:"expires_on,omitempty"`
	CreatedOn    *time.Time `json:"created_on,omitempty"`
	UpdatedOn    *time.Time `json:"updated_on,omitempty"`
}

// YoutubeResponse is the response object returned by the server
type YoutubeResponse struct {
	Platform    string        `json:"platform"`
	Connections []YoutubeUser `json:"connections"`
}

// AuthYoutube initiates the OAuth2 flow for connecting a Youtube account
func (c *client) AuthYoutube(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (string, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/youtube/auth", uid)}
	res, err := c.do(ctx, "GET", u.String(), "authYoutube", opts)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", decodeError(res)
	}

	var resp RedirectPathResp
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		return "", ErrUnexpectedResponse
	}

	return resp.Path, nil
}

// GetYoutubeUser retrieves a YoutubeUser given a user ID
func (c *client) GetYoutubeUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*YoutubeUser, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/youtube", uid)}
	res, err := c.do(ctx, "GET", u.String(), "getYoutubeUser", opts)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		var data YoutubeUser
		err = json.NewDecoder(res.Body).Decode(&data)
		return &data, err
	case http.StatusNotFound:
		return nil, ErrNoConnection
	default:
		return nil, decodeError(res)
	}

}

// CallbackYoutube creates a new Youtube connection given a user ID and query
// parameters from the OAuth2 callback. Requires an authorization token.
func (c *client) CallbackYoutube(ctx context.Context, uid string, val url.Values, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/youtube/callback", uid), RawQuery: val.Encode()}
	res, err := c.do(ctx, "GET", u.String(), "postYoutubeUser", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusCreated {
		return decodeError(res)
	}

	return nil
}

// DeleteYoutube deletes a Youtube connection given a user ID. Requires an
// authorization token.
func (c *client) DeleteYoutubeUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/youtube", uid)}
	res, err := c.do(ctx, "DELETE", u.String(), "deleteYoutubeUser", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusNoContent {
		return decodeError(res)
	}

	return nil
}
