package connections

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"code.justin.tv/foundation/twitchclient"

	"golang.org/x/net/context"
)

// BlizzardUser specifies the fields of a Blizzard connection
type BlizzardUser struct {
	TwitchID    string     `json:"twitch_id"`
	BlizzardID  string     `json:"blizzard_id"`
	Region      string     `json:"region,omitempty"`
	Battletag   string     `json:"battletag,omitempty"`
	AccessToken string     `json:"access_token,omitempty"`
	ExpiresAt   *time.Time `json:"expires_at,omitempty"`
	CreatedAt   *time.Time `json:"created_at,omitempty"`
	UpdatedAt   *time.Time `json:"updated_at,omitempty"`
}

// MultipleBlizzardResponse is a response containing a slice of connected
// BlizzardUsers entries
type MultipleBlizzardResponse struct {
	Connected []BlizzardUser
}

// AuthBlizzard gets the Blizzard OAuth authorize URL
func (c *client) AuthBlizzard(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) (string, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/blizzard/auth", uid), RawQuery: values.Encode()}
	res, err := c.do(ctx, "GET", u.String(), "authBlizzard", opts)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", decodeError(res)
	}

	var resp RedirectPathResp
	if err := json.NewDecoder(res.Body).Decode(&resp); err != nil {
		return "", err
	}
	return resp.Path, nil
}

// CallbackBlizzard passes the OAuth callback parameters to Connections to
// insert a new Blizzard connection row
func (c *client) CallbackBlizzard(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/blizzard/callback", uid), RawQuery: values.Encode()}
	res, err := c.do(ctx, "GET", u.String(), "callbackBlizzard", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusCreated {
		return decodeError(res)
	}
	return nil
}

// GetBlizzardUser retrieves a Blizzard user given a user ID
func (c *client) GetBlizzardUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*BlizzardUser, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/blizzard", uid)}
	res, err := c.do(ctx, "GET", u.String(), "getBlizzardUser", opts)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		var data BlizzardUser
		err = json.NewDecoder(res.Body).Decode(&data)
		return &data, err
	case http.StatusNotFound:
		return nil, ErrNoConnection
	default:
		return nil, decodeError(res)
	}
}

// DeleteBlizzardUser deletes a Blizzard connection given a user ID
func (c *client) DeleteBlizzardUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/blizzard", uid)}
	res, err := c.do(ctx, "DELETE", u.String(), "deleteBlizzardUser", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusNoContent {
		return decodeError(res)
	}
	return nil
}

// GetBlizzardUserByBlizzardID gets a BlizzardUser associated with a provided
// Blizzard user ID.  This is most useful to find the Twitch user ID associated with a
// Blizzard user ID.
// If the provided Blizzard ID has not linked at Twitch, will return an error
func (c *client) GetBlizzardUserByBlizzardID(ctx context.Context, id string, opts *twitchclient.ReqOpts) (*BlizzardUser, error) {
	u := &url.URL{Path: "/v2/blizzard"}

	requestParams := ConnectedReq{IDs: []string{id}}

	body, err := json.Marshal(requestParams)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", u.String(), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	merged := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       fmt.Sprintf("service.connections.%s", "getBlizzardUsersByTwitchID"),
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, merged)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		var data MultipleBlizzardResponse
		err = json.NewDecoder(res.Body).Decode(&data)
		if err != nil {
			return nil, err
		}

		if len(data.Connected) == 0 {
			return nil, ErrNoConnection
		}
		return &data.Connected[0], err
	case http.StatusNotFound:
		return nil, ErrNoConnection
	default:
		return nil, decodeError(res)
	}
}
