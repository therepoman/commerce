package iamgenerator

//Authorizer adding a fake authorizer so that we can generate policies.
// TODO: We should either refactor sandstorm policy generator to not do auth
// or come up with a better way to do auth in s2s.
type noOpAuthorizer struct{}

//AuthorizeSecretKeys return nil
func (auth *noOpAuthorizer) AuthorizeSecretKeys(keys []string) (err error) {
	return nil
}

//AuthorizeLDAPGroups anything is fine.
func (auth *noOpAuthorizer) AuthorizeLDAPGroups(ldapGroups []string) (err error) {
	return nil
}

//IsAdmin returns false
func (auth *noOpAuthorizer) IsAdmin() bool {
	return false
}

//AuthorizeNamespace returns nil
func (auth *noOpAuthorizer) AuthorizeNamespace(namespaces string) error {
	return nil
}
