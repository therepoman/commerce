package events

import (
	"code.justin.tv/sse/malachai/pkg/internal/stats/statsiface"
	"github.com/aws/aws-sdk-go/aws/session"
)

// DefaultableEvent is an event that can defaults for another event
type DefaultableEvent interface {
	FillDefaults(other interface{})
}

// EventWriter writes events
type EventWriter interface {
	WriteEvent(interface{})
	WithFields(DefaultableEvent) EventWriter
}

// EventWriterClient describes a client that implements EventWriter
type EventWriterClient interface {
	EventWriter
	Run()
	Init(*session.Session, statsiface.ReporterAPI) error
	Close() error
}
