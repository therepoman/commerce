package caller

import (
	"errors"
	"log"

	"github.com/aws/aws-sdk-go/aws"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/internal/stats"
	"code.justin.tv/sse/malachai/pkg/internal/stats/statsiface"
)

// Config holds config options
type Config struct {
	// optional config to disable secret rotations. if this is set to true,
	// sqs queues will not be created on startup. only disable for short usecases
	// such as lambda, ecs tasks, etc.
	DisableSecretRotationListener bool

	// Deprectated optional configuration option no longer utilized.
	DisableStatsClient bool

	// optional aws config override
	AWSConfigBase *aws.Config

	// for internal use only
	//
	// Environment should always be set to production or left empty
	Region                    string
	Environment               string
	SandstormKMSKeyID         string
	SandstormSecretsTableName string
	SandstormTopicArn         string
	roleArn                   string

	// To be set by NewRoundTripper / NewWithCustomRoundTripper
	// CallerName is the human-readable name of the service. This is used to
	// discover internal configuration options.
	callerName string

	// StatsReporter telemetry stats reporter
	StatsReporter statsiface.ReporterAPI
}

// FillDefaults fills in default configuration options. This is called in
// the roundtripper constructor, so the client does not need to call this
// manually.
func (cfg *Config) FillDefaults() (err error) {
	if cfg.AWSConfigBase == nil {
		cfg.AWSConfigBase = aws.NewConfig()
	}

	if cfg.callerName == "" {
		err = errors.New("callerName is required")
		return
	}

	if cfg.Environment == "" {
		cfg.Environment = "production"
	}

	if cfg.StatsReporter == nil {
		log.Print("No S2S metrics are being logged.  Please set StatsReporter if metrics are desired.")
		cfg.StatsReporter = &stats.NoopReporter{}
	}
	resources, err := config.GetResources(cfg.Environment)
	if err != nil {
		return
	}

	if cfg.Region == "" {
		cfg.Region = resources.Region
	}

	if cfg.SandstormKMSKeyID == "" {
		cfg.SandstormKMSKeyID = resources.SandstormKMSKeyID
	}

	if cfg.SandstormSecretsTableName == "" {
		cfg.SandstormSecretsTableName = resources.SandstormSecretsTableName
	}

	if cfg.SandstormTopicArn == "" {
		cfg.SandstormTopicArn = resources.SandstormTopicArn
	}
	return
}
