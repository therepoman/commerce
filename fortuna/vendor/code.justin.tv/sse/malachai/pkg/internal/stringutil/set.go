package stringutil

// Set is a set of strings
type Set map[string]interface{}

// NewSet returns a new string set from a list
func NewSet(values ...string) Set {
	ss := make(Set)
	for _, value := range values {
		ss.Add(value)
	}
	return ss
}

// Add to the set
func (ss Set) Add(value string) {
	ss[value] = nil
}

// Contains checks for membership
func (ss Set) Contains(value string) bool {
	_, ok := ss[value]
	return ok
}
