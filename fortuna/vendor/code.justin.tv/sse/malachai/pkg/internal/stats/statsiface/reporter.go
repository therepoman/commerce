package statsiface

import "time"

type ReporterAPI interface {
	Report(metricName string, value float64, units string)
	ReportDurationSample(metricName string, duration time.Duration)
}
