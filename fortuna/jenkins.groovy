job {
    name 'commerce-fortuna'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'commerce/fortuna', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
        }
    }

    steps {
        shell './scripts/docker_build.sh'
        shell './scripts/docker_push.sh'
        shell 'manta -v -f  build_lambda.json'
        saveDeployArtifact 'commerce/fortuna-ebextensions', 'deploy/'
        saveDeployArtifact 'commerce/fortuna-lambda', 'lambda_deploy/'
    }
}

job {
    name "commerce-fortuna-coverage"
    using 'TEMPLATE-autobuild'
    scm {
        git {
            remote {
                github 'commerce/fortuna', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        credentialsBinding {
            string 'AWS_ACCESS_KEY', 'jenkins-terraform-aws-access-key'
            string 'AWS_SECRET_KEY', 'jenkins-terraform-aws-secret-key'
        }
        environmentVariables {
            env('GIT_BRANCH', '${GIT_BRANCH}')
            env('GIT_COMMIT', '${GIT_COMMIT}')
            env('GIT_URL', '${GIT_URL}')
        }
    }

    steps {
        shell 'manta -v -f .manta-coverage.json'
    }

    publishers {
        reportQuality('commerce/fortuna', '.manta/coverage', '*.xml')
    }
}

job {
    name "commerce-fortuna-integration-tests"
    using 'TEMPLATE-autobuild'
    scm {
        git {
            remote {
                github 'commerce/fortuna', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-fortuna-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-fortuna-dev-tcs-secret-key'
        }
        environmentVariables {
            env('GIT_BRANCH', '${GIT_BRANCH}')
            env('GIT_COMMIT', '${GIT_COMMIT}')
            env('GIT_URL', '${GIT_URL}')
        }
    }

    steps {
        shell 'sleep 10'
        shell 'manta -v -f .manta-integration-tests.json -e AWS_ACCESS_KEY=$AWS_ACCESS_KEY -e AWS_SECRET_KEY=$AWS_SECRET_KEY'
    }
}

freeStyleJob('commerce-fortuna-prod-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-fortuna-aws-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-fortuna-aws-tcs-secret-key'
        }
        environmentVariables {
            env('GIT_BRANCH', '${GIT_BRANCH}')
            env('GIT_COMMIT', '${GIT_COMMIT}')
            env('GIT_URL', '${GIT_URL}')
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/fortuna-ebextensions'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-fortuna:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy --timeout 30 prod-commerce-fortuna-alb-env""".stripMargin()
        // Deploy Lambdas
        downloadDeployArtifact 'commerce/fortuna-lambda'
        shell 'manta -v -f deploy_lambda.json -e GIT_COMMIT=$GIT_COMMIT -e GIT_BRANCH=$GIT_BRANCH -e ENVIRONMENT=prod -e S3_BUCKET=fortuna-lambdas-prod -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY'
    }
}



freeStyleJob('commerce-fortuna-staging-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-fortuna-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-fortuna-dev-tcs-secret-key'
        }
    }
    environmentVariables {
        env('GIT_BRANCH', '${GIT_BRANCH}')
        env('GIT_COMMIT', '${GIT_COMMIT}')
        env('GIT_URL', '${GIT_URL}')
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/fortuna-ebextensions'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-fortuna:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy --timeout 30 staging-commerce-fortuna-alb-env""".stripMargin()
        // Deploy Lambdas
        downloadDeployArtifact 'commerce/fortuna-lambda'
        shell 'manta -v -f deploy_lambda.json -e GIT_COMMIT=$GIT_COMMIT -e GIT_BRANCH=$GIT_BRANCH -e ENVIRONMENT=staging -e S3_BUCKET=fortuna-lambdas-staging -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY' 
    }
}



