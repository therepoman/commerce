package main

import (
	"context"

	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/utils/setup"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Clients *clients.Clients
}

type output struct {
	BenefactorLogin string `json:"BenefactorLogin"`
	ChannelLogin    string `json:"ChannelLogin"`
}

func (l *lambdaHandler) Handler(ctx context.Context, input handler.TriggerMegaCommerceExecutionInput) (output, error) {
	benefactorID := input.Event.UserID
	channelID := input.Event.ChannelID

	loginMap, err := l.Clients.UsersClient.GetUserLoginsFromIDs([]string{benefactorID, channelID})
	if err != nil {
		return output{}, errors.Wrap(err, "CheerbombExecutor received error from Users service")
	}

	benefactorLogin, ok := loginMap[benefactorID]
	if !ok {
		return output{}, errors.New("Benefactor login was missing from Users service response")
	}
	channelLogin, ok := loginMap[channelID]
	if !ok {
		return output{}, errors.New("Channel login was missing from Users service response")
	}

	output := output{
		BenefactorLogin: benefactorLogin,
		ChannelLogin:    channelLogin,
	}

	return output, nil
}

func main() {
	clients, stats, err := setup.NewClients()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize clients")
	}
	defer stats.Close()

	h := lambdaHandler{
		Clients: clients.Clients,
	}

	lambda.Start(h.Handler)
}
