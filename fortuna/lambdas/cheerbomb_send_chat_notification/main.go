package main

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/pubsub"
	"code.justin.tv/commerce/fortuna/clients/tmi"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"code.justin.tv/commerce/fortuna/utils/lambda"
	"code.justin.tv/commerce/fortuna/utils/setup"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	anAnonymousCheererID = "407665396"
	anAnonymousGifterID  = "274598607"
)

type lambdaHandler struct {
	Clients       *clients.Clients
	ServiceConfig *config.Configuration
	Utils         rewards.IUtils
}

type SendChatNotificationInput struct {
	Event                    model.MegaCommerceEvent `json:"Event"`
	TriggerID                string                  `json:"TriggerID"`
	Logins                   Logins                  `json:"Logins"`
	ChosenChatterIDsDocument string                  `json:"ChosenChatterIDsDocument"`
	Domain                   string                  `json:"Domain"`
}

type Logins struct {
	BenefactorLogin string `json:"BenefactorLogin"`
	ChannelLogin    string `json:"ChannelLogin"`
}

type output struct{}

func (l *lambdaHandler) Handler(ctx context.Context, input SendChatNotificationInput) (output, error) {
	minCheerAmount := 0

	trigger, err := l.Utils.GetTriggerByID(input.TriggerID)
	if err != nil {
		return output{}, errors.Wrapf(err, "Failed to get Trigger %s", input.TriggerID)
	}
	minCheerAmount = trigger.TriggerAmountMin

	// Do not notify chat of this execution if a whitelist is present on the trigger
	if minCheerAmount <= 0 || trigger.UserWhitelistEnabled {
		return output{}, nil
	}

	data, err := lambda_utils.LoadFileFromS3(input.ChosenChatterIDsDocument, l.Clients, l.ServiceConfig.StepFnDocumentS3Bucket)
	if err != nil {
		return output{}, err
	}

	var chosenChatterIDs []string
	err = json.Unmarshal(data, &chosenChatterIDs)
	if err != nil {
		return output{}, err
	}

	numChosenChatters := len(chosenChatterIDs)
	if numChosenChatters > 0 {
		userID := input.Event.UserID
		if input.Event.IsAnonymous {
			if input.Event.TriggerType == campaign.TriggerTypeSubscriptionGift {
				userID = anAnonymousGifterID
			} else {
				userID = anAnonymousCheererID
			}
		}

		chatNotification := tmi.ChatNotification{
			ChannelID:        input.Event.ChannelID,
			UserID:           userID,
			SelectedCount:    strconv.Itoa(numChosenChatters),
			TriggerType:      string(trigger.TriggerType),
			TriggerAmount:    strconv.Itoa(int(input.Event.Amount)),
			TotalRewardCount: strconv.Itoa(input.Event.GetScaledRecipientAmount()),
			Domain:           input.Domain,
		}
		verb := "Cheer"
		switch trigger.TriggerType {
		case campaign.TriggerTypeSubscription:
			verb = "Sub"
		case campaign.TriggerTypeSubscriptionGift:
			verb = "Gift"
		default:
			verb = "Cheer"
		}

		if user, err := l.Clients.UsersClient.GetUserByID(userID); err != nil {
			log.WithFields(log.Fields{
				"userID":  input.Event.UserID,
				"eventID": input.Event.EventID,
			}).WithError(err).Error("CheerbombExecutor failed to get user data")
		} else {
			if trigger.TriggerType != campaign.TriggerTypeSubscription {
				bodyMessage := fmt.Sprintf("%v's %v shared rewards to %d others in Chat!", *user.Displayname, verb, numChosenChatters)
				if err := l.Clients.Notifier.Notify(chatNotification, bodyMessage); err != nil {
					log.WithFields(log.Fields{
						"userID":  input.Event.UserID,
						"eventID": input.Event.EventID,
					}).WithError(err).Error("CheerbombExecutor failed to notify about unlocked gifts")
				}
			}
			pubsubNotification := pubsub.Cheerbomb{
				UserID:           userID,
				DisplayName:      *user.Displayname,
				UserLogin:        *user.Login,
				SelectedCount:    numChosenChatters,
				TriggerType:      string(trigger.TriggerType),
				TriggerAmount:    input.Event.Amount,
				TotalRewardCount: input.Event.GetScaledRecipientAmount(),
				Domain:           input.Domain,
			}

			if err := l.Clients.PubSubClient.Publish(pubsub.CheerbombEventType, pubsubNotification, pubsub.CheerbombEventsTopic, input.Event.ChannelID); err != nil {
				log.WithFields(log.Fields{
					"userID":  input.Event.UserID,
					"eventID": input.Event.EventID,
				}).WithError(err).Error("CheerbombExecutor failed to send pubusub notification about unlocked gifts")
			}
		}
	}

	return output{}, nil
}

func main() {
	clients, stats, err := setup.NewClients()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	h := lambdaHandler{
		Clients:       clients.Clients,
		ServiceConfig: clients.Config,
		Utils:         clients.Utils,
	}

	lambda.Start(h.Handler)
}
