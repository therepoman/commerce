package main

import (
	"context"
	"encoding/json"
	"math/rand"
	"time"

	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"code.justin.tv/commerce/fortuna/utils/lambda"
	"code.justin.tv/commerce/fortuna/utils/setup"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Clients        *clients.Clients
	ChatterChooser chatterchooser.ChatterChooser
	ServiceConfig  *config.Configuration
	Rng            *rand.Rand
}

const (
	emptyOutput                 = "{}"
	OWL2018SelectionFormula     = "OWL2018"
	OWL2019SelectionFormula     = "OWL2019"
	HGC2018SelectionFormula     = "HGC2018"
	SeasonalSelectionFormula    = "seasonal"
	Holiday2019SelectionFormula = "holiday2019"
)

type GetChosenChattersInput struct {
	Event                     model.MegaCommerceEvent `json:"Event"`
	InputLogins               Logins                  `json:"Logins"`
	RecipientSelectionFormula string                  `json:"RecipientSelectionFormula"`
}

type Logins struct {
	BenefactorLogin string `json:"BenefactorLogin"`
	ChannelLogin    string `json:"ChannelLogin"`
}

type getNumChattersToChoose func(chatterchooser.CheerbombParameters) int

type UserDocument struct {
	ChatterLogins []string `json:"chatterLogins"`
	FollowerIDs   []string `json:"followerIDs"`
}

type output struct {
	NumChosenChatters           int    `json:"NumChosenChatters"`
	ChosenChatterLoginsDocument string `json:"ChosenChatterLoginsDocument"`
}

func (l *lambdaHandler) Handler(ctx context.Context, input GetChosenChattersInput) (output, error) {
	if input.RecipientSelectionFormula == "" {
		return output{}, errors.New("Empty RecipientSelectionFormula passed to GetChosenChatters")
	}

	switch input.RecipientSelectionFormula {
	case OWL2018SelectionFormula:
		return l.getChosenChatters(input.Event, input.InputLogins, chatterchooser.GetOwlCheerbombRecipientCount)
	case HGC2018SelectionFormula:
		return l.getChosenChatters(input.Event, input.InputLogins, chatterchooser.GetHgcCheerbombRecipientCount)
	case OWL2019SelectionFormula:
		return l.getChosenChatters(input.Event, input.InputLogins, chatterchooser.GetOwl2019MegaRecipientCount)
	case SeasonalSelectionFormula:
		return l.getChosenChatters(input.Event, input.InputLogins, chatterchooser.GetSeasonalRecipientCount)
	case Holiday2019SelectionFormula:
		return l.getChosenChatters(input.Event, input.InputLogins, chatterchooser.GetHoliday2019RecipientCount)
	default:
		return output{}, errors.New("RecipientSelectionFormula passed to GetChosenChatters is invalid: " + input.RecipientSelectionFormula)
	}
}

func (l *lambdaHandler) getChosenChatters(event model.MegaCommerceEvent, logins Logins, getNumChatters getNumChattersToChoose) (output, error) {
	if logins.ChannelLogin == "" {
		return output{}, errors.New("Empty channel login passed to RetrieveChatterLogins")
	}

	totalChatters, chatterLoginList, err := l.Clients.ChattersClient.GetChannelViewers(logins.ChannelLogin)
	if err != nil {
		return output{}, errors.Wrap(err, "Received error from Chatters when requesting channel viewers")
	}
	if totalChatters == 0 {
		log.WithFields(log.Fields{
			"eventID":         event.EventID,
			"channelLogin":    logins.ChannelLogin,
			"benefactorLogin": logins.BenefactorLogin,
		}).Warn("Cheerbomb.GetChosenChatters received empty response from Chatters")
	}

	// Each campaign is responsible for implementing its own function(getNumChatters) to determine the number of recipients
	chattersParam := chatterchooser.CheerbombParameters{
		Rng:           l.Rng,
		CheerAmount:   int(event.Amount),
		TotalChatters: totalChatters,
		NumberOfGifts: event.GetScaledRecipientAmount(),
	}
	numChattersToChoose := getNumChatters(chattersParam)
	chosenChatterLogins := l.ChatterChooser.Choose(chatterLoginList, totalChatters, numChattersToChoose, logins.BenefactorLogin)

	// Fallback to followers list if there are not enough users in chat
	var chosenFollowerIDs []string
	if numChattersToChoose > len(chosenChatterLogins) {
		numberToRetrieve := numChattersToChoose - len(chosenChatterLogins)
		totalFollowers, followerList, err := l.Clients.FollowsClient.ListFollowers(event.ChannelID, numberToRetrieve)

		if err != nil {
			log.WithFields(log.Fields{
				"eventID":         event.EventID,
				"channelLogin":    logins.ChannelLogin,
				"benefactorLogin": logins.BenefactorLogin,
			}).WithError(err).Error("Cheerbomb.GetChosenChatters received error response from Following-Service")
		} else {
			chosenFollowerIDs = l.ChatterChooser.Choose(followerList, totalFollowers, numberToRetrieve, event.UserID)
		}
	}

	fileName := "triggers.chosenchatters." + event.EventID

	chosenUsers := UserDocument{
		ChatterLogins: chosenChatterLogins,
		FollowerIDs:   chosenFollowerIDs,
	}
	data, err := json.Marshal(chosenUsers)
	if err != nil {
		return output{}, err
	}

	err = lambda_utils.UploadFileToS3(fileName, data, l.Clients, l.ServiceConfig.StepFnDocumentS3Bucket)
	if err != nil {
		return output{}, err
	}

	output := output{
		NumChosenChatters:           len(chosenChatterLogins) + len(chosenFollowerIDs),
		ChosenChatterLoginsDocument: fileName,
	}

	return output, nil
}

func main() {
	clients, stats, err := setup.NewClients()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	h := lambdaHandler{
		Clients:        clients.Clients,
		ChatterChooser: chatterchooser.NewRandomChatterChooser(),
		ServiceConfig:  clients.Config,
		Rng:            rand.New(rand.NewSource(time.Now().UTC().UnixNano())),
	}

	lambda.Start(h.Handler)
}
