package main

import (
	"context"
	"encoding/json"
	"sort"

	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/utils/lambda"
	"code.justin.tv/commerce/fortuna/utils/setup"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Clients       *clients.Clients
	ServiceConfig *config.Configuration
}

type getNumChattersToChoose func(chatterchooser.CheerbombParameters) int

type UserDocument struct {
	ChatterLogins []string `json:"chatterLogins"`
	FollowerIDs   []string `json:"followerIDs"`
}

func (l *lambdaHandler) Handler(ctx context.Context, input string) (string, error) {
	data, err := lambda_utils.LoadFileFromS3(input, l.Clients, l.ServiceConfig.StepFnDocumentS3Bucket)
	if err != nil {
		return "", err
	}

	var chatters UserDocument

	err = json.Unmarshal(data, &chatters)
	if err != nil {
		return "", err
	}

	chosenChatterIDs, err := l.Clients.UsersClient.GetUserIDsFromLogins(chatters.ChatterLogins)
	if err != nil {
		return "", errors.Wrap(err, "Received error from Users client while retrieving IDs from logins")
	}

	// Transform map to slice for output. Reserve capacity based on count of logins sent in request
	output := make([]string, 0, len(chatters.ChatterLogins))
	for _, userID := range chosenChatterIDs {
		output = append(output, userID)
	}

	// Also add follower ids
	for _, userID := range chatters.FollowerIDs {
		output = append(output, userID)
	}

	// Remove potential duplicate IDs
	output = lambda_utils.RemoveDuplicates(output)

	// Sort the ID slice. Not critical to functionality, but useful for testing and predictability.
	sort.Strings(output)

	fileName := input + ".ids"
	data, err = json.Marshal(output)
	if err != nil {
		return "", err
	}

	err = lambda_utils.UploadFileToS3(fileName, data, l.Clients, l.ServiceConfig.StepFnDocumentS3Bucket)
	if err != nil {
		return "", err
	}

	return fileName, nil
}

func main() {
	clients, stats, err := setup.NewClients()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	h := lambdaHandler{
		Clients:       clients.Clients,
		ServiceConfig: clients.Config,
	}

	lambda.Start(h.Handler)
}
