package main

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"code.justin.tv/commerce/fortuna/utils/lambda"
	"code.justin.tv/commerce/fortuna/utils/setup"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Clients       *clients.Clients
	ServiceConfig *config.Configuration
	Utils         rewards.IUtils
}

type FulfillAndNotifyRewardsInput struct {
	Event                    model.MegaCommerceEvent `json:"Event"`
	TriggerID                string                  `json:"TriggerID"`
	Logins                   Logins                  `json:"Logins"`
	ChosenChatterIDsDocument string                  `json:"ChosenChatterIDsDocument"`
	Domain                   string                  `json:"Domain"`
}

type Logins struct {
	BenefactorLogin string `json:"BenefactorLogin"`
	ChannelLogin    string `json:"ChannelLogin"`
}

type output struct{}

const (
	anAnonymousCheererLogin = "AnAnonymousCheerer"
	anAnonymousGifterLogin  = "AnAnonymousGifter"
)

func (l *lambdaHandler) Handler(ctx context.Context, input FulfillAndNotifyRewardsInput) (output, error) {
	trigger, err := l.Utils.GetTriggerByID(input.TriggerID)
	if err != nil {
		return output{}, errors.Wrapf(err, "Failed to get Trigger with ID while fulfilling recipient rewards. TriggerID: %s", input.TriggerID)
	}

	data, err := lambda_utils.LoadFileFromS3(input.ChosenChatterIDsDocument, l.Clients, l.ServiceConfig.StepFnDocumentS3Bucket)
	if err != nil {
		return output{}, err
	}

	var unfilteredChosenChatterIDs []string
	err = json.Unmarshal(data, &unfilteredChosenChatterIDs)
	if err != nil {
		return output{}, err
	}

	var filteredChosenChatterIDs []string
	// If whitelist is enabled, filter out chatters that are not whitelisted
	if trigger.UserWhitelistEnabled {
		log.WithFields(log.Fields{
			"triggerID": input.TriggerID,
			"eventID":   input.Event.EventID,
		}).Debug("Filtering out non-whitelisted recipients")

		for _, chatterID := range unfilteredChosenChatterIDs {
			if trigger.IsUserWhitelisted(chatterID) {
				filteredChosenChatterIDs = append(filteredChosenChatterIDs, chatterID)
			}
		}

		// If the following option is enabled, every id in the UserWhiteList will be granted rewards. Used to simulate chatrooms with more users
		if trigger.LoadTestEnabled {
			for id, ok := range trigger.UserWhitelist {
				if ok {
					filteredChosenChatterIDs = append(filteredChosenChatterIDs, id)
				}
			}
		}
		if len(filteredChosenChatterIDs) == 0 {
			log.Debug("All chosen chatters were filtered out due to whitelist")
			return output{}, nil
		}
	} else if trigger.StaffOnly {
		log.WithFields(log.Fields{
			"triggerID": input.TriggerID,
			"eventID":   input.Event.EventID,
		}).Debug("Filtering out non-staff recipients")

		for _, chatterID := range unfilteredChosenChatterIDs {
			isStaff, err := l.Clients.UsersClient.IsUserStaff(chatterID)
			if err != nil {
				log.WithFields(log.Fields{
					"triggerID": input.TriggerID,
					"eventID":   input.Event.EventID,
				}).WithError(err).Errorf("Received error from Users client while retrieving staff status")
			}

			if isStaff {
				filteredChosenChatterIDs = append(filteredChosenChatterIDs, chatterID)
			}
		}

		if len(filteredChosenChatterIDs) == 0 {
			log.Debug("All chosen chatters were filtered out due to staff only")
			return output{}, nil
		}

	} else {
		filteredChosenChatterIDs = unfilteredChosenChatterIDs
	}

	numRecipients := len(filteredChosenChatterIDs)

	log.WithFields(log.Fields{
		"numRecipients": numRecipients,
		"eventID":       input.Event.EventID,
	}).Debug("Fulfilling and notifying rewards for cheerbomb")

	benefactorLogin := input.Logins.BenefactorLogin
	if input.Event.IsAnonymous {
		if input.Event.TriggerType == campaign.TriggerTypeSubscriptionGift {
			benefactorLogin = anAnonymousGifterLogin
		} else {
			benefactorLogin = anAnonymousCheererLogin
		}
	}

	giftsTotal := input.Event.GetScaledRecipientAmount()
	scaledSelectionAmount := (giftsTotal / numRecipients)
	for idx, chosenChatterID := range filteredChosenChatterIDs {
		// Computes the number of gifts per user if there are not enough users for 1
		if idx < (giftsTotal % numRecipients) {
			scaledSelectionAmount++
		}
		if snsErr := l.sendMegaCheerGiftFulfillmentSns(chosenChatterID, benefactorLogin, trigger.Domain, trigger.ID, scaledSelectionAmount, input.Event); snsErr != nil {
			return output{}, snsErr
		}
		log.WithFields(log.Fields{
			"recipientID": chosenChatterID,
			"eventID":     input.Event.EventID,
		}).Info("Sending sns message to fulfill recipient rewards")
	}

	return output{}, nil
}

func (l *lambdaHandler) sendMegaCheerGiftFulfillmentSns(recipientID string, benefactorLogin string, domain string, triggerID string, selectionAmount int, event model.MegaCommerceEvent) error {
	eventJSON, snsErr := json.Marshal(sns.MegaCheerGiftFulfillmentEvent{
		RecipientID:     recipientID,
		BenefactorLogin: benefactorLogin,
		Domain:          domain,
		TriggerID:       triggerID,
		Event:           event,
		SelectionAmount: selectionAmount,
	})

	if snsErr != nil {
		return errors.Wrapf(snsErr, "Failed to create MegaCheerGiftFulfillmentEvent sns message. {userIDL: %v, triggerID: %v, domain: %v}", recipientID, domain, triggerID)
	}

	if err := l.Clients.SNSClient.PostToTopic(l.ServiceConfig.MegaCheerGiftFulfillmentSNSTopicARN, string(eventJSON)); err != nil {
		return errors.Wrapf(err, "Failed to publish MegaCheerGiftFulfillmentEvent to SNS topic. {userIDL: %v, triggerID: %v, domain: %v}", recipientID, domain, triggerID)
	}

	return nil
}

func main() {
	clients, stats, err := setup.NewClients()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	h := lambdaHandler{
		Clients:       clients.Clients,
		ServiceConfig: clients.Config,
		Utils:         clients.Utils,
	}

	lambda.Start(h.Handler)
}
