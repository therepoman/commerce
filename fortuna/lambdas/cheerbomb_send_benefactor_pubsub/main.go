package main

import (
	"context"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/spade"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"code.justin.tv/commerce/fortuna/utils/setup"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Clients  *clients.Clients
	Utils    rewards.IUtils
	Notifier rewards.INotifier
}

type BenefactorMessageInput struct {
	Event               model.MegaCommerceEvent `json:"Event"`
	ChosenChatters      ChosenChatters          `json:"ChosenChatters"`
	TriggerID           string                  `json:"TriggerID"`
	BenefactorRewardIDs []string                `json:"BenefactorRewardIDs"`
}

type ChosenChatters struct {
	NumChosenChatters           int    `json:"NumChosenChatters"`
	ChosenChatterLoginsDocument string `json:"ChosenChatterLoginsDocument"`
}

type output struct{}

func (l *lambdaHandler) Handler(ctx context.Context, input BenefactorMessageInput) (output, error) {
	if len(input.BenefactorRewardIDs) > 0 {
		if err := l.sendBenefactorPubSub(input); err != nil {
			log.WithFields(log.Fields{
				"benefactorID": input.Event.UserID,
				"channelID":    input.Event.ChannelID,
				"eventID":      input.Event.EventID,
			}).WithError(err).Error("CheerbombHandler failed to publish benefactor reward PubSub notification")
		}
	}

	return output{}, nil
}

func (l *lambdaHandler) sendBenefactorPubSub(input BenefactorMessageInput) error {
	var rewardsToNotify []*campaign.Reward
	trigger, err := l.Utils.GetTriggerByID(input.TriggerID)

	if err != nil {
		err = errors.Wrapf(err, "Failed to get Trigger with ID while sending benefactor Pubsub. TriggerID: %s", input.TriggerID)
		log.WithFields(log.Fields{
			"triggerID": input.TriggerID,
		}).WithError(err).Error("Error sending pubsub for benefactor.")
		return err
	}

	for _, reward := range trigger.BenefactorRewardAttributes.Rewards {
		for _, benefactorRewardID := range input.BenefactorRewardIDs {
			if reward.ID == benefactorRewardID {
				rewardsToNotify = append(rewardsToNotify, reward)
			}
		}
	}

	// Send spade tracking on its own routine to not block any work
	go func() {
		for _, reward := range rewardsToNotify {
			megaRewardEventData := &spade.MegaCheerRewardItemEvent{
				UserID:          input.Event.UserID,
				InitiatorUserID: input.Event.UserID,
				Time:            time.Now().Unix(),
				CheerAmount:     input.Event.Amount,
				Context:         spade.TriggerContext(trigger, true),
				ItemType:        reward.Type,
				ItemValue:       reward.ID,
				ChannelID:       input.Event.ChannelID,
				TransactionID:   input.Event.EventID,
			}

			// TODO: Change to be spade.RewardItemEvent after campaign is over
			if err := l.Clients.SpadeClient.TrackEvent(context.Background(), spade.MegaCheerEventName.String(), megaRewardEventData); err != nil {
				log.WithFields(log.Fields{
					"recipientID":     input.Event.UserID,
					"benefactorLogin": input.Event.UserID,
					"eventID":         input.Event.EventID,
					"reward":          reward.Name,
				}).WithError(err).Error("CheerbombExecutor failed to send initiator reward Spade event")
			}
		}
	}()

	log.WithFields(log.Fields{
		"numRecipients":        input.ChosenChatters.NumChosenChatters,
		"eventID":              input.Event.EventID,
		"benefactorRewardsIDs": input.BenefactorRewardIDs,
		"rewardsToNotify":      rewardsToNotify,
		"channelID":            input.Event.ChannelID,
	}).Debug("Sending benefactor rewards pubsub notification")

	return l.Notifier.SendMegaBenefactorPubSub(
		rewards.BenefactorPubSubInput{
			TransactionID:   input.Event.EventID,
			BenefactorID:    input.Event.UserID,
			TriggerType:     string(trigger.TriggerType),
			TriggerAmount:   input.Event.Amount,
			NumGiftsGiven:   input.ChosenChatters.NumChosenChatters,
			RewardsToNotify: rewardsToNotify,
			ChannelID:       input.Event.ChannelID,
		})
}

func main() {
	clients, stats, err := setup.NewClients()
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize stats")
	}
	defer stats.Close()

	h := lambdaHandler{
		Clients:  clients.Clients,
		Utils:    clients.Utils,
		Notifier: clients.Notifier,
	}

	lambda.Start(h.Handler)
}
