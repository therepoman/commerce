module "fortuna" {
  source = "../modules/fortuna"
  account_name = "twitch-fortuna-dev"
  account_id = "106728537780"
  owner = "twitch-fortuna-dev@amazon.com"
  aws_profile = "twitch-fortuna-dev"
  env = "staging"
  env_full = "staging"
  vpc_id = "vpc-bee65ad8"
  private_subnets = "subnet-ccc35b97,subnet-6006e728,subnet-8c06c9ea"
  sg_id = "sg-f5652f8f"
  instance_type = "t2.micro"
  min_size = 2
  max_size = 4
  root_volume_size = 20
  bits_aws_env = "staging"
  sns_trusted_role_arns = ["arn:aws:iam::106728537780:role/staging-commerce-fortuna-alb"]
  fortuna_log_group = "/aws/elasticbeanstalk/fortuna-beta-alb-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log"
  whitelisted_arns_for_privatelink = [
    "arn:aws:iam::219087926005:root", // admin panel staging
  ]
}
