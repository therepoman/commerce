terraform {
  backend "s3" {
    bucket = "twitch-fortuna-dev"
    key = "tfstate/environments/staging/terraform.tfstate"
    region = "us-west-2"
    profile = "twitch-fortuna-dev"
    encrypt = true
    dynamodb_table = "terraform-locks"
  }
}
