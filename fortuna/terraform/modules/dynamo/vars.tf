# Service configuration
variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}
