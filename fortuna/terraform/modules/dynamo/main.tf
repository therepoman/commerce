// campaigns table
resource "aws_dynamodb_table" "campaign_db" {
  name = "${var.env}_campaigns"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "domain"
  range_key = "campaignId"
  stream_enabled = "true"
  billing_mode = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "domain"
    type = "S"
  }

  attribute {
    name = "campaignId"
    type = "S"
  }

  global_secondary_index {
    name = "campaignId-index"
    hash_key = "campaignId"
    projection_type = "ALL"
  }
}

// participants table (deprecated)
resource "aws_dynamodb_table" "participant_db" {
  name = "${var.env}_participants"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "trackId"
  range_key = "userId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "trackId"
    type = "S"
  }

  attribute {
    name = "userId"
    type = "S"
  }
}

// subscriptions table (deprecated)
resource "aws_dynamodb_table" "subscription_db" {
  name = "${var.env}_subscriptions"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "eventTopic"
  range_key = "campaignId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "eventTopic"
    type = "S"
  }

  attribute {
    name = "campaignId"
    type = "S"
  }
}

// objectives table
resource "aws_dynamodb_table" "objective_db" {
  name = "${var.env}_objectives"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "domain"
  range_key = "objectiveId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "domain"
    type = "S"
  }

  attribute {
    name = "objectiveId"
    type = "S"
  }

  attribute {
    name = "campaignId"
    type = "S"
  }

  global_secondary_index {
    name = "campaignId-index"
    hash_key = "campaignId"
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "objectiveId-index"
    hash_key = "objectiveId"
    projection_type = "ALL"
  }
}

// triggers table
resource "aws_dynamodb_table" "trigger_db" {
  name = "${var.env}_triggers"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "domain"
  range_key = "triggerId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  lifecycle {
    ignore_changes = ["global_secondary_index"]
  }

  attribute {
    name = "domain"
    type = "S"
  }

  attribute {
    name = "triggerId"
    type = "S"
  }

  attribute {
    name = "campaignId"
    type = "S"
  }

  global_secondary_index {
    name = "campaignId-index"
    hash_key = "campaignId"
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "triggerId-index"
    hash_key = "triggerId"
    projection_type = "ALL"
  }
}

// milestones table
resource "aws_dynamodb_table" "milestone_db" {
  name = "${var.env}_milestones"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "domain"
  range_key = "milestoneId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "domain"
    type = "S"
  }

  attribute {
    name = "milestoneId"
    type = "S"
  }

  attribute {
    name = "objectiveId"
    type = "S"
  }

  attribute {
    name = "campaignId"
    type = "S"
  }

  global_secondary_index {
    name = "campaignId-index"
    hash_key = "campaignId"
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "objectiveId-index"
    hash_key = "objectiveId"
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "milestoneId-index"
    hash_key = "milestoneId"
    projection_type = "ALL"
  }
}

// objective tracks table (deprecated)
resource "aws_dynamodb_table" "objective_track_db" {
  name = "${var.env}_objective_tracks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "trackId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "trackId"
    type = "S"
  }
}

// rewards table
resource "aws_dynamodb_table" "rewards_db" {
  name = "${var.env}_rewards"
  billing_mode = "PAY_PER_REQUEST"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "userId"
  range_key = "scopedRewardId"

  attribute {
    name = "userId"
    type = "S"
  }

  attribute {
    name = "scopedRewardId"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

// Promotion eligibilities table
resource "aws_dynamodb_table" "eligibilities_db" {
  name = "${var.env}_eligibilities"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "userId"
  range_key = "milestoneId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "userId"
    type = "S"
  }

  attribute {
    name = "milestoneId"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

// channel properties table
resource "aws_dynamodb_table" "channel_properties_db" {
  name = "${var.env}_channel_properties"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "channelId"
  billing_mode = "PAY_PER_REQUEST"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channelId"
    type = "S"
  }
}

// claimable rewards table
resource "aws_dynamodb_table" "claimable_rewards_db" {
  name = "${var.env}_claimable_rewards"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "userId"
  range_key = "scopedRewardId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "userId"
    type = "S"
  }

  attribute {
    name = "scopedRewardId"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

// atomic counter progress table
resource "aws_dynamodb_table" "progress_db" {
  name = "${var.env}_progress"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "hashId"
  range_key = "rangeId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"


  attribute {
    name = "hashId"
    type = "S"
  }

  attribute {
    name = "rangeId"
    type = "S"
  }
}

// atomic counter participation table
resource "aws_dynamodb_table" "participation_db" {
  name = "${var.env}_participation"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "hashId"
  range_key = "rangeId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "hashId"
    type = "S"
  }

  attribute {
    name = "rangeId"
    type = "S"
  }
}

// atomic counter token progress table
resource "aws_dynamodb_table" "token_progress_db" {
  name = "${var.env}_token_progress"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "hashId"
  range_key = "rangeId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "hashId"
    type = "S"
  }

  attribute {
    name = "rangeId"
    type = "S"
  }
}

// atomic counter token participation table
resource "aws_dynamodb_table" "token_participation_db" {
  name = "${var.env}_token_participation"
  billing_mode = "PAY_PER_REQUEST"
  point_in_time_recovery {
    enabled = true
  }
  hash_key = "hashId"
  range_key = "rangeId"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "hashId"
    type = "S"
  }

  attribute {
    name = "rangeId"
    type = "S"
  }
}

resource "aws_dynamodb_table" "lock_ttl_db" {
  name           = "${var.env}_lock_ttl"
  billing_mode = "PAY_PER_REQUEST"
  hash_key       = "key"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  ttl {
    attribute_name = "TTL"
    enabled        = true
  }

  attribute {
    name = "key"
    type = "S"
  }
}
