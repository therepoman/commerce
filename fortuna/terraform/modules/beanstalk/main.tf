resource "aws_elastic_beanstalk_environment" "bs" {
  count               = "${var.count}"
  name                = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-env"
  description         = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  application         = "${var.eb_application_name}"
  solution_stack_name = "${var.solution_stack_name}"
  tier                = "WebServer"

  tags {
    Environment = "${var.env}"
    Service     = "${var.service}"
    Owner       = "${var.owner}"
  }

  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"

  # Vpc  and network related settings
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "${var.vpc_id}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${var.ec2_subnet_ids}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "${var.elb_subnet_ids}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "${var.associate_public_address}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "${var.elb_scheme}"
  }

  # Application Load Balancer Related Settings
  setting {
    namespace = "aws:elbv2:loadbalancer"
    name      = "SecurityGroups"
    value     = "${var.elb_loadbalancer_security_groups}"
  }

  setting {
    namespace = "aws:elbv2:loadbalancer"
    name      = "AccessLogsS3Enabled"
    value     = "true"
  }

  setting {
    namespace = "aws:elbv2:loadbalancer"
    name      = "AccessLogsS3Bucket"
    value     = "${var.env == "prod" ? "fortuna-elb-access-logs-prod" : "fortuna-elb-access-logs-staging"}"
  }

  setting {
    namespace = "aws:elbv2:listener:default"
    name      = "ListenerEnabled"
    value     = "true"
  }

  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "Protocol"
    value     = "HTTPS"
  }

  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "SSLCertificateArns"
    value     = "${data.aws_acm_certificate.ssl.arn}"
  }

  # ASG configuration settings
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = "${var.min_size}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = "${var.max_size}"
  }

  # ASG launch configuration settings
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${coalesce(var.auto_scaling_lc_iam_instance_profile, aws_iam_instance_profile.bs.name)}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = "${var.auto_scaling_lc_keypair_name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "${var.auto_scaling_lc_instance_type}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "MonitoringInterval"
    value     = "${var.auto_scaling_lc_monitoring_interval}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = "${var.auto_scaling_lc_security_groups}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeType"
    value     = "${var.auto_scaling_lc_root_volume_type}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeSize"
    value     = "${var.auto_scaling_lc_root_volume_size}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeIOPS"
    value     = "${var.auto_scaling_lc_root_volume_iops}"
  }

  # ASG trigger configuration settings
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "MeasureName"
    value     = "${var.asgtrigger_measure_name}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Unit"
    value     = "${var.asgtrigger_unit}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerThreshold"
    value     = "${var.asgtrigger_lower_threshold}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperThreshold"
    value     = "${var.asgtrigger_upper_threshold}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENV_NAME"
    value     = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-env"
  }

  # Environment Variables
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "APP"
    value     = "${var.eb_application_name}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "STATSD_HOST_PORT"
    value     = "${var.statsd_host}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "${var.env}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "STATS_ENVIRONMENT"
    value     = "${var.stats_env}"
  }

  # eb environment settings
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${var.eb_environment_service_role}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "${var.load_balancer_type}"
  }

  # Cloudwatch settings
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = "true"
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = "false"
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "RetentionInDays"
    value     = "60"
  }

  # Managed updates settings
  setting {
    name      = "UpdateLevel"
    namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
    value     = "minor"
  }

  setting {
    name      = "InstanceRefreshEnabled"
    namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
    value     = "false"
  }

  setting {
    name      = "ManagedActionsEnabled"
    namespace = "aws:elasticbeanstalk:managedactions"
    value     = "true"
  }

  setting {
    name      = "PreferredStartTime"
    namespace = "aws:elasticbeanstalk:managedactions"
    value     = "Tue:09:00"
  }
}

data "aws_acm_certificate" "ssl" {
  domain   = "fortuna-${var.is_beta ? (var.env == "prod" ? "canary" : "beta") : var.env}.internal.justin.tv"
  statuses = ["ISSUED"]
}
