resource "aws_iam_role" "bs" {
  count = "${var.count}"
  name  = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  path  = "/"

  assume_role_policy = "${var.is_beta && var.env != "prod" ? data.aws_iam_policy_document.bs_beta_assume_role_policy.json : data.aws_iam_policy_document.bs_assume_role_policy.json}"
}

data "aws_iam_policy_document" "bs_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "bs_beta_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }

    principals {
      identifiers = ["arn:aws:iam::180265471281:root"]
      type        = "AWS"
    }
  }
}

resource "aws_iam_role_policy" "bs" {
  count  = "${var.count}"
  name   = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  role   = "${aws_iam_role.bs.id}"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "BucketAccess",
      "Action": [
        "s3:Get*",
        "s3:List*",
        "s3:PutObject",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::elasticbeanstalk-*",
        "arn:aws:s3:::elasticbeanstalk-*/*",
        "arn:aws:s3:::fortuna-${var.env}*",
        "arn:aws:s3:::fortuna-${var.env}*/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
       "kms:GenerateDataKey*",
       "kms:Decrypt"
      ],
      "Resource": "arn:aws:kms:us-west-2:${var.aws_account_id}:key/*"
    }
  ]
}
 POLICY
}

resource "aws_iam_instance_profile" "bs" {
  count      = "${var.count}"
  depends_on = ["aws_iam_role.bs"]
  name       = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  role       = "${aws_iam_role.bs.name}"
}

// Grant the Instance Profile full cloudwatch access
resource "aws_iam_role_policy_attachment" "cw_policy" {
  count = "${var.count}"
  role = "${aws_iam_role.bs.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy" {
  count = "${var.count}"
  role = "${aws_iam_role.bs.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile S3 Read access
resource "aws_iam_role_policy_attachment" "s3_policy" {
  count = "${var.count}"
  role = "${aws_iam_role.bs.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}

data "aws_cloudformation_export" "eventbus_access_policy_arn" {
  name = "eventbus-access-policy-arn"
}

resource "aws_iam_role_policy_attachment" "eventbus_access_attach" {
  count = "${var.count}"
  role = "${aws_iam_role.bs.name}"
  policy_arn = "${data.aws_cloudformation_export.eventbus_access_policy_arn.value}"
}

data "aws_iam_policy_document" "sandstorm" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/fortuna-${var.env}"
    ]

    effect = "Allow"
  }

  statement {
    sid = "Stmt1473817796000"
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::twitch-sandstorm/sandstorm-agent.rpm"
    ]

    effect = "Allow"
  }
}

// Policy necessary for sandstorm to start properly
resource "aws_iam_role_policy" "sandstorm" {
  count = "${var.count}"
  name = "sandstorm-${var.env}"
  role = "${aws_iam_role.bs.id}"
  policy = "${data.aws_iam_policy_document.sandstorm.json}"
}

data "aws_iam_policy_document" "dynamo_policy" {
  statement {
    actions = [
      "dynamodb:PutItem",
      "dynamodb:Query",
      "dynamodb:Scan",
      "dynamodb:UpdateItem",
      "dynamodb:GetItem",
      "dynamodb:BatchGetItem",
      "dynamodb:DeleteItem"
    ]

    effect = "Allow"

    resources = [
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_campaigns",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_campaigns/index/campaignId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_participants",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_subscriptions",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_objectives",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_objectives/index/objectiveId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_objectives/index/campaignId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_milestones",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_milestones/index/milestoneId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_milestones/index/objectiveId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_milestones/index/campaignId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_triggers",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_triggers/index/triggerId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_triggers/index/campaignId-index",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_objective_tracks",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_rewards",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_eligibilities",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_channel_properties",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_claimable_rewards",
      "arn:aws:dynamodb:us-west-2:${var.aws_account_id}:table/${var.env}_lock_ttl",
    ]
  }
}

// Grant the Instance Profile full dynamo access
resource "aws_iam_role_policy" "dynamo_attachment" {
  count = "${var.count}"
  name = "dynamo-${var.env}"
  role = "${aws_iam_role.bs.name}"
  policy = "${data.aws_iam_policy_document.dynamo_policy.json}"
}

data "aws_iam_policy_document" "s2s_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::180116294062:role/malachai/*",
    ]

    effect = "Allow"
  }
}

// Polich necessary as an s2s caller
resource "aws_iam_role_policy" "s2s_attachment" {
  count = "${var.count}"
  name = "s2s-${var.env}"
  role = "${aws_iam_role.bs.name}"
  policy = "${data.aws_iam_policy_document.s2s_policy.json}"
}

data "aws_iam_policy_document" "sfn_policy" {
  statement {
    actions = [
      "states:DescribeActivity",
      "states:DescribeExecution",
      "states:DescribeStateMachine",
      "states:DescribeStateMachineForExecution",
      "states:GetActivityTask",
      "states:SendTaskFailure",
      "states:SendTaskHeartbeat",
      "states:SendTaskSuccess",
      "states:StartExecution"
    ]

    resources = [
      "*"
    ]

    effect = "Allow"
  }
}

resource "aws_iam_role_policy" "sfn_attachment" {
  count = "${var.count}"
  name = "sfn-${var.env}"
  role = "${aws_iam_role.bs.name}"
  policy = "${data.aws_iam_policy_document.sfn_policy.json}"
}

# Requirement from Twitch Security (https://wiki.xarth.tv/display/SEC/AWS+Systems+Manager+Campaign)
resource "aws_iam_role_policy_attachment" "ecs_ssm_policy_attachment" {
  role       = "${aws_iam_role.bs.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
