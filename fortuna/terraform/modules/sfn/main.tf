# Looking for state machine definitions? Look for a file named after the feature that the state machine supports.
resource "aws_iam_role" "step_function" {
  name               = "${var.env}-StepFunction"
  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.aws_region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "${var.env}-sfn_lambda_execution"
  role   = "${aws_iam_role.step_function.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}