// lambdas
# Step 1: Use bits event userID and channelID to look up login names for benefactor and channel from Users service
module "cheerbomb_retrieve_benefactor_and_channel_logins" {
  source                           = "../lambda"
  env                              = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.security_groups}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "cheerbomb_retrieve_benefactor_and_channel_logins"
  source_path                      = "${path.module}/lambda/cheerbomb_retrieve_benefactor_and_channel_logins/deployment.zip"
  lambda_bucket                    = "${var.lambda_bucket}"
  pager_duty_sns_arn               = "${var.pager_duty_sns_arn}"
}

# Step 2: Retrieve list of chatter logins using channel login and TMI/chatters endpoint,
# then select the lucky ones and return the list of chosen logins
module "cheerbomb_get_chosen_chatters" {
  source                           = "../lambda"
  env                              = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.security_groups}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "cheerbomb_get_chosen_chatters"
  source_path                      = "${path.module}/lambda/cheerbomb_get_chosen_chatters/deployment.zip"
  lambda_bucket                    = "${var.lambda_bucket}"
  pager_duty_sns_arn               = "${var.pager_duty_sns_arn}"
}

# Step 3: Send pubsub notification for benefactor for cheerbomb
module "cheerbomb_send_benefactor_pubsub" {
  source                           = "../lambda"
  env                              = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.security_groups}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "cheerbomb_send_benefactor_pubsub"
  source_path                      = "${path.module}/lambda/cheerbomb_send_benefactor_pubsub/deployment.zip"
  lambda_bucket                    = "${var.lambda_bucket}"
  pager_duty_sns_arn               = "${var.pager_duty_sns_arn}"
}

# Step 4: Use selected list of chatter logins to retrieve lucky chatter IDs from Users service
module "cheerbomb_retrieve_chosen_chatter_ids" {
  source                           = "../lambda"
  env                              = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.security_groups}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "cheerbomb_retrieve_chosen_chatter_ids"
  source_path                      = "${path.module}/lambda/cheerbomb_retrieve_chosen_chatter_ids/deployment.zip"
  lambda_bucket                    = "${var.lambda_bucket}"
  pager_duty_sns_arn               = "${var.pager_duty_sns_arn}"
}

# Step 5: Use list of lucky chatter IDs and trigger's recipient rewards to fulfill appropriate rewards in Fortuna
# and send PubSubs to each user
module "cheerbomb_fulfill_and_notify_rewards" {
  source                           = "../lambda"
  env                              = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.security_groups}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "cheerbomb_fulfill_and_notify_rewards"
  source_path                      = "${path.module}/lambda/cheerbomb_fulfill_and_notify_rewards/deployment.zip"
  lambda_bucket                    = "${var.lambda_bucket}"
  pager_duty_sns_arn               = "${var.pager_duty_sns_arn}"
}

# Step 6: Send a single TMI notification to the channel chat room announcing the cheerbomb and count of gifts
module "cheerbomb_send_chat_notification" {
  source                           = "../lambda"
  env                              = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.security_groups}"]
  subnets                          = "${var.subnets}"
  lambda_name                      = "cheerbomb_send_chat_notification"
  source_path                      = "${path.module}/lambda/cheerbomb_send_chat_notification/deployment.zip"
  lambda_bucket                    = "${var.lambda_bucket}"
  pager_duty_sns_arn               = "${var.pager_duty_sns_arn}"
}

resource "aws_sfn_state_machine" "cheerbomb_gift_fulfillment_v4" {
  name     = "${var.env}-CheerbombGiftFulfillment-v4"
  role_arn = "${aws_iam_role.step_function.arn}"

  definition = <<ENDDEF
{
  "StartAt": "RetrieveBenefactorAndChannelLogins",
  "States": {
    "RetrieveBenefactorAndChannelLogins": {
      "Type": "Task",
      "Resource": "${module.cheerbomb_retrieve_benefactor_and_channel_logins.lambda_arn}",
      "InputPath": "$",
      "ResultPath": "$.Logins",
      "Next": "GetChosenChatters",
      "TimeoutSeconds": 5,
      "Retry": [
        {
          "ErrorEquals": ["States.ALL"]
        }
      ]
    },
    "GetChosenChatters": {
      "Type": "Task",
      "Resource": "${module.cheerbomb_get_chosen_chatters.lambda_arn}",
      "InputPath": "$['Event', 'Logins', 'RecipientSelectionFormula']",
      "ResultPath": "$.ChosenChatters",
      "Next": "SendBenefactorPubSub",
      "TimeoutSeconds": 60,
      "Retry": [
        {
          "ErrorEquals": ["States.ALL"],
          "MaxAttempts": 5
        }
      ]
    },
    "SendBenefactorPubSub": {
      "Type": "Task",
      "Resource": "${module.cheerbomb_send_benefactor_pubsub.lambda_arn}",
      "InputPath": "$['Event', 'ChosenChatters.NumChosenChatters', 'TriggerID', 'BenefactorRewardIDs']",
      "ResultPath": null,
      "Next": "FilterNoChatters",
      "TimeoutSeconds": 5,
      "Retry": [
        {
          "ErrorEquals": ["States.ALL"]
        }
      ],
      "Catch": [
        {
          "ErrorEquals": ["States.ALL"],
          "Next": "FilterNoChatters",
          "ResultPath": "$.SendBenefactorPubSubError"
        }
      ]
    },
    "FilterNoChatters": {
      "Type": "Choice",
      "Default": "RetrieveChosenChatterIDs",
      "Choices": [
          {
              "Variable": "$.ChosenChatters.NumChosenChatters",
              "NumericLessThanEquals": 0,
              "Next": "NoopSucceed"
          }
      ]
    },
    "RetrieveChosenChatterIDs": {
      "Type": "Task",
      "Resource": "${module.cheerbomb_retrieve_chosen_chatter_ids.lambda_arn}",
      "InputPath": "$.ChosenChatters.ChosenChatterLoginsDocument",
      "ResultPath": "$.ChosenChatterIDsDocument",
      "OutputPath": "$['Event', 'TriggerID', 'Logins', 'ChosenChatterIDsDocument', 'Domain']",
      "Next": "ParallelFulfillNotify",
      "TimeoutSeconds": 60,
      "Retry": [
        {
          "ErrorEquals": ["States.ALL"],
          "IntervalSeconds": 60,
          "BackoffRate": 4.0
        }
      ]
    },
    "ParallelFulfillNotify": {
      "Type": "Parallel",
      "Branches": [
        {
          "StartAt": "FulfillAndNotifyRewards",
          "States": {
            "FulfillAndNotifyRewards": {
              "Type": "Task",
              "Resource": "${module.cheerbomb_fulfill_and_notify_rewards.lambda_arn}",
              "ResultPath": null,
              "End": true,
              "TimeoutSeconds": 120,
              "Retry": [
                {
                  "ErrorEquals": ["States.ALL"]
                }
              ]
            }
          }
        },
        {
          "StartAt": "SendChatNotification",
          "States": {
            "SendChatNotification": {
              "Type": "Task",
              "Resource": "${module.cheerbomb_send_chat_notification.lambda_arn}",
              "ResultPath": null,
              "End": true,
              "TimeoutSeconds": 60,
              "Retry": [
                {
                  "ErrorEquals": ["States.ALL"]
                }
              ]
            }
          }
        }
      ],
      "End": true
    },
    "NoopSucceed": {
        "Type": "Succeed"
    }
  }
}
  ENDDEF
}
