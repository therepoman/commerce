variable "aws_profile" {
  description = "AWS profile used to manage resources"
}

variable "aws_region" {
  description = "AWS region to manage resources within"
  default     = "us-west-2"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
  default     = "dev"
}

variable "fortuna_logscan_namespace" {
    default = "fortuna"
}

variable "security_groups" {
  type = "list"
}

variable "subnets" {}

variable "timeout" {
  default = 60
}

variable "memory_size" {
  default = 1024
}

variable "lambda_bucket" {}

variable "pager_duty_sns_arn" {
  type        = "string"
  description = "ARN of the SNS topic set up for Pager Duty alarming"
}


