variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "fortuna_logscan_namespace" {
  default = "FortunaLogs"
  type    = "string"
}

variable "fortuna_log_group" {
  type    = "string"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "pager_duty_sns_arn" {
  type        = "string"
  description = "ARN of the SNS topic set up for Pager Duty alarming"
}
