resource "aws_cloudwatch_metric_alarm" "fortuna_error_alert" {
  alarm_name                = "fortuna-${var.env}-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "fortuna-errors"
  namespace                 = "${var.fortuna_logscan_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  datapoints_to_alarm       = "3"
  threshold                 = "30"
  alarm_description         = "This metric is used for logscan error ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = "${length(var.pager_duty_sns_arn) > 0 ? true : false}"
  alarm_actions             = ["${var.pager_duty_sns_arn}"]
  ok_actions                = ["${var.pager_duty_sns_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "fortuna_panic_alert" {
  alarm_name                = "fortuna-${var.env}-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  metric_name               = "fortuna-panics"
  namespace                 = "${var.fortuna_logscan_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric is used for panic ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = "${length(var.pager_duty_sns_arn) > 0 ? true : false}"
  alarm_actions             = ["${var.pager_duty_sns_arn}"]
  ok_actions                = ["${var.pager_duty_sns_arn}"]
}
