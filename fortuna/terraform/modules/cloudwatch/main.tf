// Logscan
resource "aws_cloudwatch_log_metric_filter" "error_scan" {
  name           = "fortuna-error-scan"
  pattern        = "level=error"
  log_group_name = "${var.fortuna_log_group}"

  metric_transformation {
    name          = "fortuna-errors"
    namespace     = "${var.fortuna_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "fatal_scan" {
  name           = "fortuna-fatal-scan"
  pattern        = "level=fatal"
  log_group_name = "${var.fortuna_log_group}"

  metric_transformation {
    name          = "fortuna-fatals"
    namespace     = "${var.fortuna_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "panic_scan" {
  name           = "fortuna-panic-scan"
  pattern        = "\"panic:\""
  log_group_name = "${var.fortuna_log_group}"

  metric_transformation {
    name          = "fortuna-panics"
    namespace     = "${var.fortuna_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "warning_scan" {
  name           = "fortuna-warning-scan"
  pattern        = "level=warn"
  log_group_name = "${var.fortuna_log_group}"

  metric_transformation {
    name          = "fortuna-warns"
    namespace     = "${var.fortuna_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}