provider "aws" {
  version = "~> 2.16.0"
  region  = "us-west-2"
  profile = "${var.aws_profile}"
}