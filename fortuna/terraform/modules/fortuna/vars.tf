# AWS general Config
variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default = "commerce/fortuna"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "env_full" {
  description = "Full name of environment for privatelinks module (production/staging)"
}

variable "account_name" {
  description = "Name of the AWS account"
}

variable "account_id" {
  description = "AWS account ID"
}

variable "owner" {
  description = "Owner of the account"
}

variable "bits_aws_env" {
  description = "The environment of the twitch-bits-aws account"
  default     = "staging"
}

# VPC Config
variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "sg_id" {
  description = "Id of the security group"
}

# ASG Config
variable "instance_type" {
  description = "EC2 instance type"
}

variable "root_volume_size" {
  description = "Storage capacity of the root Amazon EBS volume in whole GiB"
}

variable "min_size" {
  description = "Minimum ASG fleet size"
}

variable "max_size" {
  description = "Maximum ASG fleet size"
}

variable "asgtrigger_measure_name" {
  description = "Metric used for your Auto Scaling trigger."
  default     = "NetworkOut"
}

variable "asgtrigger_unit" {
  description = "Unit for the trigger measurement, such as Bytes."
  default     = "Percent"
}

variable "asgtrigger_lower_threshold" {
  description = "If the measurement falls below this number for the breach duration, a trigger is fired."
  default     = "5"
}

variable "asgtrigger_upper_threshold" {
  description = "If the measurement is higher than this number for the breach duration, a trigger is fired."
  default     = "60"
}

variable "whitelisted_arns_for_privatelink" {
  description = "the whitelist of arns for privatelinks"
  type        = "list"
}

variable "sns_trusted_role_arns" {
  description = "List of trusted role arns that can publish to sns"
  type = "list"
}

variable "fortuna_log_group" {
  type    = "string"
}