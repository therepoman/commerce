module "bs-dev-alb" {
  source = "../beanstalk"
  count = 1
  aws_profile = "${var.aws_profile}"
  aws_account_id = "${var.account_id}"
  eb_application_name = "fortuna"
  vpc_id = "${var.vpc_id}"
  ec2_subnet_ids = "${var.private_subnets}"
  elb_subnet_ids = "${var.private_subnets}"
  elb_loadbalancer_security_groups = "${var.sg_id}"
  auto_scaling_lc_security_groups = "${var.sg_id}"
  solution_stack_name = "64bit Amazon Linux 2018.03 v2.16.9 running Docker 19.03.13-ce"
  auto_scaling_lc_keypair_name = "fortuna"
  auto_scaling_lc_instance_type = "${var.instance_type}"
  auto_scaling_lc_root_volume_size = "${var.root_volume_size}"
  min_size = "${var.min_size}"
  max_size = "${var.max_size}"
  owner = "${var.owner}"
  service = "${var.service}"
  env = "${var.env}"
  stats_env = "${var.env}"
  associate_public_address = "false"
  asgtrigger_measure_name = "${var.asgtrigger_measure_name}"
  asgtrigger_unit = "${var.asgtrigger_unit}"
  asgtrigger_lower_threshold = "${var.asgtrigger_lower_threshold}"
  asgtrigger_upper_threshold = "${var.asgtrigger_upper_threshold}"
  load_balancer_type = "application"
}

module "eventbus" {
  source = "../eventbus"
  env = "${var.env}"
}

module "sqs" {
  source = "../sqs"
  aws_account_id = "${var.account_id}"
  aws_profile = "${var.aws_profile}"
  env = "${var.env}"
  megacheer_gift_fulfillment_sns_arn = "${module.sns.megacheer_gift_fulfillment_arn}"
}

module "sns" {
  source = "../sns"
  env = "${var.env}"
  trusted_role_arns = "${var.sns_trusted_role_arns}"
}

module "dynamo" {
  source = "../dynamo"
  aws_profile = "${var.aws_profile}"
  env = "${var.env}"
}

module "cloudwatch" {
  source = "../cloudwatch"
  aws_profile = "${var.aws_profile}"
  fortuna_log_group = "${var.fortuna_log_group}"
  env = "${var.env}"
  pager_duty_sns_arn = "${module.sns.pager_duty_sns_arn}"
}

module "s3" {
  source = "../s3"
  aws_profile = "${var.aws_profile}"
  env = "${var.env}"
}

module "sfn" {
  source = "../sfn"
  aws_profile = "${var.aws_profile}"
  env = "${var.env}"
  fortuna_logscan_namespace        = "fortuna"
  security_groups                  = ["${var.sg_id}"]
  subnets                          = "${var.private_subnets}"
  lambda_bucket                    = "${module.s3.lambdas_bucket}"
  pager_duty_sns_arn               = "${module.sns.pager_duty_sns_arn}"
}


module "twitch-inventory" {
  source = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory?ref=v1.0.0"
  account = "${var.aws_profile}"
}
