locals {
  # aws_elastic_beanstalk_environment does not export listen arn for ALBs, so it's hard coded here.
  alb_listener_arns = {
    prod    = "arn:aws:elasticloadbalancing:us-west-2:180265471281:listener/app/awseb-AWSEB-1LZRSNZIAGMF7/6d844299513ea59a/ce42f37a943accb5"
    staging = "arn:aws:elasticloadbalancing:us-west-2:106728537780:listener/app/awseb-AWSEB-BXNKBTJ1ADHI/55a583fac05720f7/61371468c362018f"
  }
  alb_listener_dns = {
    prod    = "internal-awseb-AWSEB-1LZRSNZIAGMF7-1609901693.us-west-2.elb.amazonaws.com"
    staging = "internal-awseb-AWSEB-BXNKBTJ1ADHI-1020980345.us-west-2.elb.amazonaws.com"
  }

  twitchdartreceiver = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-00cc5f5ddd42126a0"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0ce6ec756b1a5acba"
  }

  twitchvxfollowingserviceecs = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-08682995a563a1413"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-02ed47f998f8f23a2"
  }

  env = {
    production = "prod"
    staging    = "beta"
  }
}

resource "aws_alb_listener_certificate" "alb_listener_cert_a2z" {
  certificate_arn = "${module.privatelink-cert.arn}"
  listener_arn    = "${local.alb_listener_arns[var.env]}"
}

module "privatelink-twitchdartreceiver" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"

  name            = "twitchdartreceiver"
  dns             = "us-west-2.${local.env[var.env_full]}.twitchdartreceiver.s.twitch.a2z.com"
  endpoint        = "${local.twitchdartreceiver[var.env_full]}"
  security_groups = ["${var.sg_id}"]
  subnets         = "${split(",", var.private_subnets)}"
  vpc_id          = "${var.vpc_id}"
}

module "privatelink-twitchvxfollowingserviceecs" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"

  name            = "twitchvxfollowingserviceecs"
  dns             = "us-west-2.${local.env[var.env_full]}.twitchvxfollowingserviceecs.s.twitch.a2z.com"
  endpoint        = "${local.twitchvxfollowingserviceecs[var.env_full]}"
  security_groups = ["${var.sg_id}"]
  subnets         = "${split(",", var.private_subnets)}"
  vpc_id          = "${var.vpc_id}"
}

module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "fortuna"
  environment = "${var.env_full}"
}

module "privatelink-cert" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"
  name        = "fortuna"
  environment = "${var.env_full}"
  zone_id     = "${module.privatelink-zone.zone_id}"
  alb_dns     = "${element(module.bs-dev-alb.beanstalk_environment_cname, 0)}"
}

module "privatelink_a2z" {
  source           = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink"
  name             = "fortuna-a2z"
  region           = "${var.aws_region}"
  environment      = "${var.env_full}"
  alb_dns          = "${local.alb_listener_dns[var.env]}"
  cert_arn         = "${module.privatelink-cert.arn}"
  vpc_id           = "${var.vpc_id}"
  subnets          = "${split(",", var.private_subnets)}"
  whitelisted_arns = "${var.whitelisted_arns_for_privatelink}"
}

// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "ldap.twitch.a2z.com"
}