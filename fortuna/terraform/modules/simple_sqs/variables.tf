variable "aws_account_id" {
  description = "AWS account ID"
}

variable "base_queue_name" {
  type = "string"
}

variable "receive_wait_time" {
  type = "string"
  default = 0
}

variable "message_retention_seconds" {
  type = "string"
  default = 1209600
}

variable "max_receive_count" {
  type = "string"
  default = 4
}

variable "permitted_sns_arn" {
  type = "string"
}

variable "environment" {
  type = "string"
}