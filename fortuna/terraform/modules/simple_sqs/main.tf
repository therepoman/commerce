resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = "${aws_sqs_queue.simple_sqs_queue.id}"
  policy = "${data.aws_iam_policy_document.default_queue_policy_doc.json}"
}

data "aws_iam_policy_document" "default_queue_policy_doc" {
  statement {
    sid = "FortunaSimpleSQSSid"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]
      type = "AWS"
    }

    resources = [
      "${aws_sqs_queue.simple_sqs_queue.arn}",
    ]

    condition {
      test = "ArnEquals"
      variable = "aws:SourceArn"

      values = [
        "${var.permitted_sns_arn}",
      ]
    }
  }
}

resource "aws_sqs_queue" "simple_sqs_queue_dlq" {
  message_retention_seconds = "${var.message_retention_seconds}"
  name = "${var.environment}-${var.base_queue_name}-dlq"
  kms_master_key_id = "${aws_kms_alias.simple_sqs_queue_kms_alias_with_env.name}"
  kms_data_key_reuse_period_seconds = 300
}

resource "aws_sqs_queue" "simple_sqs_queue" {
  message_retention_seconds = "${var.message_retention_seconds}"
  name = "${var.environment}-${var.base_queue_name}-sqs"
  receive_wait_time_seconds = "${var.receive_wait_time}"
  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.simple_sqs_queue_dlq.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
  kms_master_key_id = "${aws_kms_alias.simple_sqs_queue_kms_alias_with_env.name}"
  kms_data_key_reuse_period_seconds = 300
}

data "aws_iam_policy_document" "simple_sqs_key_policy" {
  statement {
    sid = "Account IAM Permissions"
    actions = ["kms:*"]
    principals = {
      type = "AWS"
      identifiers = ["arn:aws:iam::${var.aws_account_id}:root"]
    }
    resources = ["*"]
  }

  statement {
    sid = "SNS Permissions for Encrypted SQS"
    actions = ["kms:GenerateDataKey*", "kms:Decrypt"]
    principals = {
      type = "Service"
      identifiers = ["sns.amazonaws.com"]
    }
    resources = ["*"]
  }
}

resource "aws_kms_alias" "simple_sqs_queue_kms_alias_with_env" {
  name = "alias/${var.environment}-${var.base_queue_name}-sqs-kms"
  target_key_id = "${aws_kms_key.simple_sqs_queue_kms.key_id}"
}

resource "aws_kms_key" "simple_sqs_queue_kms" {
  policy = "${data.aws_iam_policy_document.simple_sqs_key_policy.json}"
}

output "simple_queue_arn" {
  value = "${aws_sqs_queue.simple_sqs_queue.arn}"
}