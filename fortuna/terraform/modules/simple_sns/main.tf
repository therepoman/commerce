resource "aws_sns_topic" "simple_sns_topic" {
  name = "${var.environment}-${var.base_topic_name}-sns"
}

resource "aws_sns_topic_policy" "simple_sns_topic_policy" {
  arn = "${aws_sns_topic.simple_sns_topic.arn}"

  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Id": "${aws_sns_topic.simple_sns_topic.arn}/SNSPolicy",
  "Statement": [
    {
      "Sid": "AllowSNSPublish",
      "Effect": "Allow",
      "Principal": {
        "AWS": ${jsonencode(var.trusted_publisher_role_arns)}
      },
      "Action": "SNS:Publish",
      "Resource": "${aws_sns_topic.simple_sns_topic.arn}"
    }
  ]
}
POLICY
}