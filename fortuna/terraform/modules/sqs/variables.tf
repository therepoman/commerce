# Beanstalk general variables
variable "aws_account_id" {
  description = "AWS account ID"
}

variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
  default     = "dev"
}

variable "bits_aws_env" {
  description = "The environment of the twitch-bits-aws account"
  default     = "staging"
}

variable "megacheer_gift_fulfillment_sns_arn" {
  description = "megacheer gift notification SNS topic arn"
}
