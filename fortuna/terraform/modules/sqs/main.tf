resource "aws_sns_topic_subscription" "megacheer_gift_fulfillement_sqs_subscription" {
  topic_arn = "${var.megacheer_gift_fulfillment_sns_arn}"
  protocol  = "sqs"
  endpoint  = "${module.megacheer_gift_fulfillment_sqs.simple_queue_arn}"
}

module "megacheer_gift_fulfillment_sqs" {
  source = "../simple_sqs"
  aws_account_id = "${var.aws_account_id}"
  environment = "${var.env}"
  base_queue_name = "megacheer-gift-fulfillment-events"
  permitted_sns_arn = "${var.megacheer_gift_fulfillment_sns_arn}"
}