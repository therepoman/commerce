resource "aws_iam_role" "lambda_iam_role" {
  name = "${var.lambda_name}_${var.env}_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    effect = "Allow"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

// TODO: policy attachment should be done per-lambda
resource "aws_iam_role_policy_attachment" "vpc_execution_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "dynamo_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "step_function_policy_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

resource "aws_iam_role_policy" "lambda_policy" {
  role   = "${aws_iam_role.lambda_iam_role.name}"
  policy = "${data.aws_iam_policy_document.lambda_policy.json}"
}

resource "aws_lambda_function" "lambda" {
  function_name = "${var.lambda_name}-${var.env}"
  role          = "${aws_iam_role.lambda_iam_role.arn}"
  handler       = "main"
  runtime       = "go1.x"
  timeout       = "${var.timeout}"
  memory_size   = "${var.memory_size}"
  s3_bucket     = "${var.lambda_bucket}"
  s3_key        = "${var.lambda_name}/latest/${var.lambda_name}.zip"

  environment {
    variables = {
      ENVIRONMENT = "${var.env}"
    }
  }

  vpc_config {
    security_group_ids = "${var.security_groups}"
    subnet_ids         = ["${split(",", var.subnets)}"]
  }
}

output "lambda_arn" {
  value = "${aws_lambda_function.lambda.arn}"
}

resource aws_cloudwatch_log_group "logs" {
  name = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
}

resource aws_cloudwatch_log_metric_filter "logscan_errors" {
  log_group_name = "${aws_cloudwatch_log_group.logs.name}"

  metric_transformation {
    name          = "${aws_lambda_function.lambda.function_name}-logscan-errors-${var.env}"
    namespace     = "${var.fortuna_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }

  name    = "${aws_lambda_function.lambda.function_name}-logscan-errors-${var.env}"
  pattern = "level=error"
}

resource aws_cloudwatch_log_metric_filter "logscan_panics" {
  log_group_name = "${aws_cloudwatch_log_group.logs.name}"

  metric_transformation {
    name          = "${aws_lambda_function.lambda.function_name}-logscan-panics-${var.env}"
    namespace     = "${var.fortuna_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }

  name    = "${aws_lambda_function.lambda.function_name}-logscan-panics-${var.env}"
  pattern = "panic"
}

resource "aws_cloudwatch_metric_alarm" "lambda_panic_log_alarm" {
  alarm_name          = "fortuna-${var.env}-${aws_lambda_function.lambda.function_name}-panic-logs-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "${aws_lambda_function.lambda.function_name}-logscan-panics-${var.env}"
  namespace           = "${var.fortuna_logscan_namespace}"
  period              = "60"
  statistic           = "Sum"
  datapoints_to_alarm = "1"
  threshold           = "1"
  alarm_description   = "Panic has occured"
  treat_missing_data  = "notBreaching"
  actions_enabled     = "${length(var.pager_duty_sns_arn) > 0 ? true : false}"
  alarm_actions       = ["${var.pager_duty_sns_arn}"]
  ok_actions          = ["${var.pager_duty_sns_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "lambda_error_log_alarm" {
  alarm_name          = "fortuna-${var.env}-${aws_lambda_function.lambda.function_name}-error-logs-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "${aws_lambda_function.lambda.function_name}-logscan-errors-${var.env}"
  namespace           = "${var.fortuna_logscan_namespace}"
  period              = "300"
  statistic           = "Sum"
  datapoints_to_alarm = "3"
  threshold           = "15"
  alarm_description   = "High volume of error logs"
  treat_missing_data  = "notBreaching"
  actions_enabled     = "${length(var.pager_duty_sns_arn) > 0 ? true : false}"
  alarm_actions       = ["${var.pager_duty_sns_arn}"]
  ok_actions          = ["${var.pager_duty_sns_arn}"]
}
