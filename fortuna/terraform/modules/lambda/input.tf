variable "env" {}

variable "fortuna_logscan_namespace" {}

variable "security_groups" {
  type = "list"
}

variable "subnets" {}

variable "lambda_name" {}

variable "lambda_bucket" {}

variable "source_path" {}

variable "timeout" {
  default = 60
}

variable "memory_size" {
  default = 1024
}

variable "pager_duty_sns_arn" {
  type        = "string"
  description = "ARN of the SNS topic set up for Pager Duty alarming"
}
