resource "aws_s3_bucket" "campaign_config_bucket" {
  bucket = "fortuna-${var.env}-campaigns"
  acl    = "private"

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "autoprof_bucket" {
  bucket = "fortuna-${var.env}-autoprof"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    expiration {
      days = 14
    }
  }
}

resource "aws_s3_bucket" "sfn-documents" {
  bucket = "fortuna-${var.env}-sfn-documents"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    expiration {
      days = 7
    }
  }
}

resource "aws_s3_bucket" "lambdas" {
  bucket = "fortuna-lambdas-${var.env}"
  acl    = "private"
}
