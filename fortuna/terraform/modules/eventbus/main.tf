data "aws_cloudformation_export" "eventbus_sqs_policy_document" {
  name = "eventbus-sqs-policy-document"
}
data "aws_cloudformation_export" "eventbus_kms_master_key_arn" {
  name = "eventbus-kms-master-key-arn"
}

resource "aws_sqs_queue" "eventbus_sqs" {
  name              = "${var.env}-eventbus-fortuna-sqs"
  policy            = "${data.aws_cloudformation_export.eventbus_sqs_policy_document.value}"
  kms_master_key_id = "${data.aws_cloudformation_export.eventbus_kms_master_key_arn.value}"
  redrive_policy    = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.eventbus_dlq.arn}\",\"maxReceiveCount\":5}"
}

resource "aws_sqs_queue" "eventbus_dlq" {
  name              = "${var.env}-eventbus-fortuna-dlq"
  kms_master_key_id = "${data.aws_cloudformation_export.eventbus_kms_master_key_arn.value}"
}