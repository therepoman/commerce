variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
  default     = "dev"
}

variable "trusted_role_arns" {
  description = "List of trusted role arns that can publish to sns."
  type = "list"
}