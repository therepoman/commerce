locals {
  digital-assets-pager-duty-endpoint = {
    staging = ""
    prod    = "https://events.pagerduty.com/integration/4b7833de27cf4e08d0acba0f8593a0a9/enqueue"
  }
}

resource "aws_sns_topic" "pager-duty-sns" {
  name = "digital-assets-pager-duty"
}

resource "aws_sns_topic_policy" "pager-duty-sns-policy" {
  arn = "${aws_sns_topic.pager-duty-sns.arn}"

  policy = "${data.aws_iam_policy_document.pager-duty-sns-policy-document.json}"
}

data "aws_iam_policy_document" "pager-duty-sns-policy-document" {
  statement {
    actions = [
      "SNS:Publish",
    ]

    effect = "Allow"

    principals {
      type = "Service"
      identifiers = ["cloudwatch.amazonaws.com"]
    }

    resources = [
      "${aws_sns_topic.pager-duty-sns.arn}",
    ]

    sid = "Allow-Cloudwatch"
  }
}

resource "aws_sns_topic_subscription" "pager-duty-sns-subscription" {
  count = "${local.digital-assets-pager-duty-endpoint[var.env] == "" ? 0 : 1}"
  topic_arn = "${aws_sns_topic.pager-duty-sns.arn}"
  protocol  = "https"
  endpoint  = "${local.digital-assets-pager-duty-endpoint[var.env]}"
  endpoint_auto_confirms = true
}
