module "twitch_megacheer_gift_fulfillment_sns" {
  source = "../simple_sns"
  environment = "${var.env}"
  base_topic_name = "megacheer-gift-fulfillment"
  trusted_publisher_role_arns = ["${var.trusted_role_arns}"]
}
