output "megacheer_gift_fulfillment_arn" {
   value = "${module.twitch_megacheer_gift_fulfillment_sns.topic_arn}"
}

output "pager_duty_sns_arn" {
   value = "${aws_sns_topic.pager-duty-sns.arn}"
}
