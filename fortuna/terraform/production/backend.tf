terraform {
  backend "s3" {
    bucket = "twitch-fortuna-aws"
    key = "tfstate/environments/production/terraform.tfstate"
    region = "us-west-2"
    profile = "twitch-fortuna-aws"
    encrypt = true
    dynamodb_table = "terraform-locks"
  }
}
