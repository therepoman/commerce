module "fortuna" {
  source = "../modules/fortuna"
  account_name = "twitch-fortuna-aws"
  account_id = "180265471281"
  owner = "twitch-fortuna-aws@amazon.com"
  aws_profile = "twitch-fortuna-aws"
  env = "prod"
  env_full = "production"
  vpc_id = "vpc-0df4486b"
  private_subnets = "subnet-e0d54dbb,subnet-6b39d823,subnet-c2fa35a4"
  sg_id = "sg-af410bd5"
  instance_type = "t2.medium"
  min_size = 4
  max_size = 4
  root_volume_size = 100
  bits_aws_env = "prod"
  sns_trusted_role_arns = ["arn:aws:iam::180265471281:role/prod-commerce-fortuna-alb"]
  fortuna_log_group = "/aws/elasticbeanstalk/prod-commerce-fortuna-alb-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log"
  whitelisted_arns_for_privatelink = [
    "arn:aws:iam::196915980276:root", // admin panel prod
  ]
}

