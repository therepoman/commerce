package dynamo

import (
	"code.justin.tv/commerce/fortuna/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

// Client includes a dynamodb reference
type Client struct {
	DB     *dynamo.DB
	Config *config.Configuration
}

// NewDynamoClient returns a client with a dynamo session
func NewDynamoClient(cfg *config.Configuration) *Client {
	db := dynamo.New(session.New(), &aws.Config{Region: &cfg.AWSRegion})
	return &Client{db, cfg}
}
