package pushy

import (
	"fmt"
	"time"

	"code.justin.tv/chat/pushy/client/events"
	fortunaSNS "code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/clients/wrapper"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	pushyServiceName = "pushy"
	publishAPIName   = "Publish"

	allAccessOrigin = "All-Access Pass"
	iconImageURL    = "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/100/2.png"
)

type Publisher interface {
	SendBitsNotification(userID string, numBits int64) error
}

type Client struct {
	Pushy events.Publisher
	Stats statsd.Statter
}

func NewPushyClient(arn string, stats statsd.Statter) (Publisher, error) {
	sess, _ := session.NewSession(fortunaSNS.NewDefaultConfig())
	snsClient := sns.New(sess)
	pushyClient, err := events.NewPublisher(events.Config{
		DispatchSNSARN: arn,
		Stats:          stats,
		SNSClient:      snsClient,
	})
	if err != nil {
		return nil, err
	}

	return &Client{
		Pushy: pushyClient,
		Stats: stats,
	}, nil
}

func (c *Client) SendBitsNotification(userID string, numBits int64) error {
	defer func(startTime time.Time) {
		c.Stats.TimingDuration(
			getMetricName(publishAPIName),
			time.Since(startTime),
			1.0,
		)
	}(time.Now())

	bitsGrantParams := events.BitsGrantParams{
		UserID:       userID,
		NumBits:      numBits,
		Origin:       allAccessOrigin,
		IconImageURL: iconImageURL,
	}

	requestCtx := wrapper.GenerateRequestContext(pushyServiceName, publishAPIName)
	return c.Pushy.Publish(requestCtx, events.BitsGrantEvent, bitsGrantParams)
}

// SendOWLAAP2019RewardsNotification is used to send a notification reminder to users who did not purchase the OWL 2019 All-Access Pass,
// but are eligible to redeem some rewards on purchase (see https://jira.twitch.com/browse/REWARD-2220)
func (c *Client) SendOWLAAP2019RewardsNotification(userID string, numRewards int) error {
	defer func(startTime time.Time) {
		c.Stats.TimingDuration(
			getMetricName(publishAPIName),
			time.Since(startTime),
			1.0,
		)
	}(time.Now())

	owlAAP2019RewardsParams := events.OWLAAP2019RewardsParams{
		UserID:     userID,
		NumRewards: numRewards,
	}

	requestCtx := wrapper.GenerateRequestContext(pushyServiceName, publishAPIName)
	return c.Pushy.Publish(requestCtx, events.OWLAAP2019RewardsEvent, owlAAP2019RewardsParams)
}

func getMetricName(apiName string) string {
	return fmt.Sprintf("service.%s.%s", pushyServiceName, apiName)
}
