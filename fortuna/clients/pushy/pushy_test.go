package pushy_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/fortuna/clients/pushy"
	"code.justin.tv/commerce/fortuna/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	. "github.com/stretchr/testify/mock"
)

func TestPushy(t *testing.T) {
	Convey("TestPushyClient", t, func() {
		mockStatter := new(mocks.Statter)
		mockStatter.On("TimingDuration", Anything, Anything, Anything).Return(nil)

		mockPublisher := new(mocks.Publisher)
		PushyClient := &pushy.Client{
			Pushy: mockPublisher,
			Stats: mockStatter,
		}

		Convey("NewPushyClient", func() {
			client, err := pushy.NewPushyClient("arn:test", mockStatter)
			So(client, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

		Convey("SendBitsNotification", func() {
			Convey("with successful message", func() {
				mockPublisher.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := PushyClient.SendBitsNotification("123", int64(200))
				So(err, ShouldBeNil)
			})

			Convey("with unsuccessful message", func() {
				mockPublisher.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
				err := PushyClient.SendBitsNotification("123", int64(200))
				So(err, ShouldNotBeNil)
			})
		})

		Convey("SendOWLAAP2019RewardsNotification", func() {
			Convey("with successful message", func() {
				mockPublisher.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := PushyClient.SendOWLAAP2019RewardsNotification("123", 5)
				So(err, ShouldBeNil)
			})

			Convey("with unsuccessful message", func() {
				mockPublisher.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
				err := PushyClient.SendOWLAAP2019RewardsNotification("123", 5)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
