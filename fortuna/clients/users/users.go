package users

import (
	"net/http"

	"code.justin.tv/commerce/fortuna/clients/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	usersModels "code.justin.tv/web/users-service/models"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	serviceName     = "users"
	getUsersAPIName = "GetUsers"
	getUserAPIName  = "GetUser"
)

type IUsersClient interface {
	GetUsers(params *usersModels.FilterParams) (*usersModels.PropertiesResult, error)
	GetUserByID(userID string) (*usersModels.Properties, error)
	GetUserLoginsFromIDs(userIDs []string) (map[string]string, error)
	GetUserIDsFromLogins(userLogins []string) (map[string]string, error)
	IsUserStaff(userID string) (bool, error)
}

type UsersClient struct {
	InternalClient usersclient_internal.InternalClient
}

func NewUsersClient(host string, stats statsd.Statter, timeout int) (*UsersClient, error) {
	if host == "" {
		return &UsersClient{}, errors.New("NewUsersClient requires a non-empty host argument")
	}

	cfg := twitchclient.ClientConf{
		Host:  host,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(serviceName, timeout),
		},
	}

	client, err := usersclient_internal.NewClient(cfg)
	if err != nil {
		return &UsersClient{}, errors.Wrap(err, "Failed to initialize internal Users client")
	}

	return &UsersClient{client}, nil
}

func (u *UsersClient) GetUsers(params *usersModels.FilterParams) (*usersModels.PropertiesResult, error) {
	ctx := wrapper.GenerateRequestContext(serviceName, getUsersAPIName)

	resp, err := u.InternalClient.GetUsers(ctx, params, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Call to UsersService.GetUsers failed")
	}

	return resp, nil
}

// TODO: Add Caching layer
func (u *UsersClient) GetUserByID(userID string) (*usersModels.Properties, error) {
	ctx := wrapper.GenerateRequestContext(serviceName, getUserAPIName)

	resp, err := u.InternalClient.GetUserByID(ctx, userID, nil)
	// Pass unwrapped error if not found so downstream can check with client.IsUserNotFound
	if client.IsUserNotFound(err) {
		return nil, err
	}
	if err != nil {
		return nil, errors.Wrap(err, "Call to UsersService.GetUserByID failed")
	}

	return resp, nil
}

func (u *UsersClient) IsUserStaff(userID string) (bool, error) {
	userProperties, err := u.GetUserByID(userID)
	if err != nil {
		return false, err
	}

	if userProperties.Admin == nil {
		return false, nil
	}

	return *userProperties.Admin, nil
}

func (u *UsersClient) GetUserLoginsFromIDs(userIDs []string) (map[string]string, error) {
	resp, err := u.GetUsers(&usersModels.FilterParams{
		IDs: userIDs,
	})
	if err != nil {
		return nil, err
	}

	out := map[string]string{}
	for _, result := range resp.Results {
		out[result.ID] = *result.Login
	}

	return out, nil
}

func (u *UsersClient) GetUserIDsFromLogins(userLogins []string) (map[string]string, error) {
	resp, err := u.GetUsers(&usersModels.FilterParams{
		Logins: userLogins,
	})
	if err != nil {
		return nil, err
	}

	out := map[string]string{}
	for _, result := range resp.Results {
		out[*result.Login] = result.ID
	}

	return out, nil
}
