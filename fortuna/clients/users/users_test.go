package users_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/clients/users"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUsersClient(t *testing.T) {
	Convey("Test UsersClient", t, func() {
		mockInternalClient := new(mocks.InternalClient)
		mockStats := new(mocks.Statter)
		usersClient := users.UsersClient{
			InternalClient: mockInternalClient,
		}

		mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test GetUsers", func() {
			Convey("happy case", func() {
				mockInternalClient.On("GetUsers", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.PropertiesResult{}, nil)

				_, err := usersClient.GetUsers(&models.FilterParams{})
				So(err, ShouldBeNil)
			})

			Convey("with error from InternalClient", func() {
				mockInternalClient.On("GetUsers", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("test"))

				_, err := usersClient.GetUsers(&models.FilterParams{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test GetUserByID", func() {
			Convey("happy case", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{}, nil)

				_, err := usersClient.GetUserByID("123")
				So(err, ShouldBeNil)
			})

			Convey("with error from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("test"))

				_, err := usersClient.GetUserByID("123")
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test IsUserStaff", func() {
			Convey("happy case", func() {
				trueBool := true
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{
						Admin: &trueBool,
					}, nil)

				isStaff, err := usersClient.IsUserStaff("123")
				So(err, ShouldBeNil)
				So(isStaff, ShouldBeTrue)
			})

			Convey("with nil field from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{}, nil)

				isStaff, err := usersClient.IsUserStaff("123")
				So(err, ShouldBeNil)
				So(isStaff, ShouldBeFalse)
			})

			Convey("with error from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("test"))

				isStaff, err := usersClient.IsUserStaff("123")
				So(err, ShouldNotBeNil)
				So(isStaff, ShouldBeFalse)
			})
		})

		Convey("Test GetUserLoginsFromIDs", func() {
			userOneID := "1"
			userTwoID := "2"
			userOneLogin := "gamerOne"
			userTwoLogin := "gamerTwo"
			userIDs := []string{"1", "2"}

			mockInternalClient.On("GetUsers", mock.Anything, mock.Anything, mock.Anything).
				Return(&models.PropertiesResult{
					Results: []*models.Properties{
						{
							ID:    userOneID,
							Login: &userOneLogin,
						},
						{
							ID:    userTwoID,
							Login: &userTwoLogin,
						},
					},
				}, nil)

			expectedParams := models.FilterParams{
				IDs: userIDs,
			}

			expectedLoginMap := map[string]string{
				userOneID: userOneLogin,
				userTwoID: userTwoLogin,
			}

			actualLoginMap, err := usersClient.GetUserLoginsFromIDs(userIDs)
			So(err, ShouldBeNil)

			So(actualLoginMap, ShouldResemble, expectedLoginMap)

			mockInternalClient.AssertNumberOfCalls(t, "GetUsers", 1)
			So(*mockInternalClient.Calls[0].Arguments[1].(*models.FilterParams), ShouldResemble, expectedParams)
		})

		Convey("Test GetUserIDsFromLogins", func() {
			userOneID := "1"
			userTwoID := "2"
			userOneLogin := "gamerOne"
			userTwoLogin := "gamerTwo"
			userLogins := []string{userOneLogin, userTwoLogin}

			mockInternalClient.On("GetUsers", mock.Anything, mock.Anything, mock.Anything).
				Return(&models.PropertiesResult{
					Results: []*models.Properties{
						{
							ID:    "1",
							Login: &userOneLogin,
						},
						{
							ID:    "2",
							Login: &userTwoLogin,
						},
					},
				}, nil)

			expectedParams := models.FilterParams{
				Logins: userLogins,
			}

			expectedIDMap := map[string]string{
				userOneLogin: userOneID,
				userTwoLogin: userTwoID,
			}

			actualIDMap, err := usersClient.GetUserIDsFromLogins(userLogins)
			So(err, ShouldBeNil)

			So(actualIDMap, ShouldResemble, expectedIDMap)

			mockInternalClient.AssertNumberOfCalls(t, "GetUsers", 1)
			So(*mockInternalClient.Calls[0].Arguments[1].(*models.FilterParams), ShouldResemble, expectedParams)
		})
	})
}
