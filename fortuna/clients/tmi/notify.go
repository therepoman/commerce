package tmi

import (
	"strconv"
	"time"

	"github.com/pkg/errors"
)

const (
	channelIDKey        = "channelID"
	userIDKey           = "userID"
	tmiMsgID            = "rewardgift"
	triggerTypeKey      = "trigger-type"
	triggerAmountKey    = "trigger-amount"
	selectedCountKey    = "selected-count"
	totalRewardCountKey = "total-reward-count"
	domainKey           = "domain"
)

type Notification struct {
	TMIClient Sender
}

type ChatNotification struct {
	ChannelID        string    `json:"channel_id"`
	UserID           string    `json:"user_id"`
	TriggerType      string    `json:"trigger_type"`
	TriggerAmount    string    `json:"bits_amount"`
	SelectedCount    string    `json:"selected_count"`
	TotalRewardCount string    `json:"total_reward_count"`
	TimeReceived     time.Time `json:"time_received"`
	Domain           string    `json:"domain"`
}

type Notifier interface {
	Notify(notification ChatNotification, message string) error
}

func NewNotifier(tmiClient Sender) Notifier {
	return &Notification{
		TMIClient: tmiClient,
	}
}

func (n *Notification) Notify(notification ChatNotification, message string) error {
	userID, err := strconv.Atoi(notification.UserID)
	if err != nil {
		return errors.Wrap(err, "failed to convert userID to integer")
	}

	channelID, err := strconv.Atoi(notification.ChannelID)
	if err != nil {
		return errors.Wrap(err, "failed to convert channelID to integer")
	}

	params := UserNoticeParams{
		SenderUserID:    userID,
		TargetChannelID: channelID,
		MsgID:           tmiMsgID,
		MsgParams: []UserNoticeMsgParam{
			{Key: triggerTypeKey, Value: notification.TriggerType},
			{Key: triggerAmountKey, Value: notification.TriggerAmount},
			{Key: selectedCountKey, Value: notification.SelectedCount},
			{Key: totalRewardCountKey, Value: notification.TotalRewardCount},
			{Key: domainKey, Value: notification.Domain},
		},
		DefaultSystemBody: message,
	}

	return n.TMIClient.SendUserNotice(params)
}
