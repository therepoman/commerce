package tmi_test

import (
	"testing"

	"errors"

	"code.justin.tv/commerce/fortuna/clients/tmi"
	"code.justin.tv/commerce/fortuna/mocks/TMI"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func TestNotify(t *testing.T) {
	Convey("Test Notify", t, func() {
		tmiClient := new(mocks.Sender)
		notifier := tmi.NewNotifier(tmiClient)

		chatNotification := tmi.ChatNotification{
			UserID:        "1",
			ChannelID:     "2",
			SelectedCount: "3",
		}

		Convey("with valid input", func() {
			tmiClient.On("SendUserNotice", Anything).Return(nil)
			err := notifier.Notify(chatNotification, "123")
			So(err, ShouldBeNil)
		})

		Convey("with TMI call failing", func() {
			tmiClient.On("SendUserNotice", Anything).Return(errors.New("test error fail"))
			err := notifier.Notify(chatNotification, "123")
			So(err, ShouldNotBeNil)
		})
	})
}
