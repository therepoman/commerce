package tmi

import (
	"fmt"
	"time"

	"bytes"
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"

	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

const (
	jsonBodyType    = "application/json"
	userNoticeRoute = "/internal/usernotice"
)

const (
	serviceName   = "tmi"
	userNoticeApi = "SendUserNotice"
)

type Sender interface {
	SendUserNotice(params UserNoticeParams) error
}

type Client struct {
	host  string
	Stats statsd.Statter
}

type UserNoticeParams struct {
	SenderUserID      int                  `json:"sender_user_id"`
	TargetChannelID   int                  `json:"target_channel_id"`
	Body              string               `json:"body"`
	MsgID             string               `json:"msg_id"`
	MsgParams         []UserNoticeMsgParam `json:"msg_params"`
	DefaultSystemBody string               `json:"default_system_body"`
}

type UserNoticeMsgParam struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func NewTMIClient(host string, stats statsd.Statter) Sender {
	return &Client{
		host:  host,
		Stats: stats,
	}
}

func (s *Client) SendUserNotice(params UserNoticeParams) error {
	defer func(startTime time.Time) {
		s.Stats.TimingDuration(
			getMetricName(userNoticeApi),
			time.Since(startTime),
			1.0,
		)
	}(time.Now())

	body, err := json.Marshal(params)
	if err != nil {
		wrappedErr := errors.Wrap(err, "TMIClient: Error while marshalling UserNoticeParams")
		log.WithFields(log.Fields{
			"SenderUserID": params.SenderUserID,
		}).WithError(err).Error(wrappedErr)
		return wrappedErr
	}

	url := s.host + userNoticeRoute

	log.WithFields(log.Fields{
		"Body": string(body),
	}).Info("TMIClient: Sending user notice with body")

	resp, err := http.Post(url, jsonBodyType, bytes.NewReader(body))
	if err != nil {
		wrappedErr := errors.Wrap(err, "TMIClient: Error while creating new request for SendUserNotice")
		log.WithFields(log.Fields{
			"SenderUserID": params.SenderUserID,
		}).WithError(err).Error(wrappedErr)
		return wrappedErr
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		errMsg := "TMIClient: UserNotice request failed"
		log.WithFields(log.Fields{
			"HTTP Status":   resp.StatusCode,
			"Response Body": resp.Body,
		}).WithError(err).Error(errMsg)
		return errors.WithStack(errors.New(errMsg))
	}

	return nil
}

func getMetricName(apiName string) string {
	return fmt.Sprintf("service.%s.%s", serviceName, apiName)
}
