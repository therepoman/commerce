package badges

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/net/context"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 1.0
	defaultTimingXactName = "badges"
)

// ClientConfig wraps wraps twitchclient.ClientConf so you can set custom timeout values
type ClientConfig struct {
	// HTTPConfig is the default http configuration
	HTTPConfig twitchclient.ClientConf

	// CtxTimeout is how long the client should wait for a http call before timing out
	CtxTimeout time.Duration
}

// Client is an interface for hitting Badges endpoints
type Client interface {
	// GrantUserBadge grants a badge to a user
	GrantUserBadge(ctx context.Context, userID, badgeSetID string, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
}

// NewClient creates an http client to call Badges endpoints.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	if err != nil {
		return nil, err
	}

	return &clientImpl{http: twitchClient}, nil
}

type clientImpl struct {
	http twitchclient.Client
}

func (c *clientImpl) GrantUserBadge(ctx context.Context, userID, badgeSetID string, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/owl/%s", userID, channelID, badgeSetID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_user_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to GrantUserFuelBadge", resp.StatusCode)
	}

	return nil
}
