package spade

import (
	"fmt"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"

	"golang.org/x/net/context"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/common/spade-client-go/spade"
	log "github.com/sirupsen/logrus"
)

const (
	prodEnvironmentPrefix = "prod"
)

type ClientNoOp struct{}

type RewardItemEvent struct {
	Context     string `json:"context"`
	ItemType    string `json:"item_type"`
	ItemValue   string `json:"item_value"`
	UserID      string `json:"user_id"`
	ChannelID   string `json:"channel_id"`
	ChannelName string `json:"channel"`
	SourceID    string `json:"source_user_id"`
	Time        int64  `json:"time"`
}

// MegaCheerRewardItemEvent is the expected contents of the reward event sent to spade when the
// reward is granted by a mega cheer
type MegaCheerRewardItemEvent struct {
	Time            int64  `json:"time"`
	InitiatorUserID string `json:"initiator_user_id"`
	CheerAmount     int32  `json:"cheer_amount"`
	Context         string `json:"context"`
	ItemType        string `json:"item_type"`
	ItemValue       string `json:"item_value"`
	ChannelID       string `json:"channel_id"`
	UserID          string `json:"user_id"`
	TransactionID   string `json:"transaction_id"`
}

// MegaCheerEvent is sent to spade when a user initiates a MegaCheer event
type MegaCheerEvent struct {
	Time            int64  `json:"time"`
	InitiatorUserID string `json:"initiator_user_id"`
	CheerAmount     int32  `json:"cheer_amount"`
	Context         string `json:"context"`
	ItemType        string `json:"item_type"`
	ItemValue       string `json:"item_value"`
	ChannelID       string `json:"channel_id"`
	UserID          string `json:"user_id"`
	TransactionID   string `json:"transaction_id"`
}

// MegaCheerEvent is sent to spade when a user initiates a MegaCheer event
type TwoFactorEmoteEntitlementEvent struct {
	Time     int64  `json:"time"`
	UserID   string `json:"user_id"`
	Entitled bool   `json:"entitled"`
}

const (
	MegaRewardRecipientString = "%s.recipient"
	MegaRewardInitiatorString = "%s.initiator"
)

// TODO: Change to be domain instead of hardcoded to megacommerce_2019 after campaign is over
func TriggerContext(trigger *campaign.Trigger, isBenefactor bool) string {
	triggerType := ""
	switch trigger.TriggerType {
	case campaign.TriggerTypeCheer:
		triggerType = "megacommerce_2019_cheer"
	case campaign.TriggerTypeSubscription:
		triggerType = "megacommerce_2019_sub"
	case campaign.TriggerTypeSubscriptionGift:
		triggerType = "megacommerce_2019_sub_gift"
	default:
		triggerType = "megacheer"
	}

	if isBenefactor {
		return fmt.Sprintf(MegaRewardInitiatorString, triggerType)
	}

	return fmt.Sprintf(MegaRewardRecipientString, triggerType)
}

// ChallengeCompletedEvent is a Spade event for tracking when a user completes a challenge, regardless
// of whether they were eligible to complete it or not
type ChallengeCompletedEvent struct {
	Time                 int64  `json:"time"`
	ChallengeRecognized  bool   `json:"challenge_recognized"`
	ChallengeID          string `json:"challenge_id"`
	ChallengeName        string `json:"challenge_name"`
	ChallengeDescription string `json:"challenge_description"`
	ChallengeType        string `json:"challenge_type"`
	ChallengeEvent       string `json:"challenge_event"`
	UserID               string `json:"user_id"`
	ChannelID            string `json:"channel_id"`
}

type SpadeEventName string
type SpadeRewardType string

const (
	RewardItemEventName         SpadeEventName = "reward_item_receive"
	MegaCheerEventName          SpadeEventName = "megacheer_event"
	ChallengeCompletedEventName SpadeEventName = "viewer_challenge_completed"

	TypeEmote   SpadeRewardType = "emote"
	TypeBadge   SpadeRewardType = "badge"
	TypeIGC     SpadeRewardType = "igc"
	TypeBits    SpadeRewardType = "bits"
	TypeUnknown SpadeRewardType = "unknown"
)

func NewSpadeClient(cfg *config.Configuration, stats statsd.Statter) (spade.Client, error) {
	// Only send Spade Events in Prod
	// Spade doesn't differentiate between environments, so all data hits Prod Spade
	log.Debugf("Initializing %s Spade Client", cfg.EnvironmentPrefix)

	if cfg.EnvironmentPrefix == prodEnvironmentPrefix {
		spadeClient, err := spade.NewClient(
			spade.InitStatHook(func(name string, httpStatus int, dur time.Duration) {
				stats.TimingDuration(fmt.Sprintf("service.spade.%s.%d", name, httpStatus), dur, 1)
			}),
		)
		if err != nil {
			return nil, errors.Wrap(err, "failed to initialize Spade client")
		}
		return spadeClient, nil
	}

	return &ClientNoOp{}, nil
}

func GetRewardContext(reward *campaign.Reward) string {
	return reward.ObjectiveID
}

func (cn *ClientNoOp) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	return nil
}

func (cn *ClientNoOp) TrackEvents(ctx context.Context, events ...spade.Event) error {
	return nil
}

func (s SpadeEventName) String() string {
	return string(s)
}

func (s SpadeRewardType) String() string {
	return string(s)
}
