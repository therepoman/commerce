package nitro

import (
	"code.justin.tv/commerce/fortuna/clients/wrapper"
	nitro "code.justin.tv/samus/nitro/rpc"
	"github.com/pkg/errors"
)

type NitroClient struct {
	Client nitro.Nitro
}

const (
	ServiceName               = "nitro"
	getPremiumStatusesAPIName = "GetPremiumStatuses"
)

type INitroClient interface {
	GetPrimeStatus(twitchUserID string) (bool, error)
}

// GetPrimeStatus returns a prime status of a given user
func (n *NitroClient) GetPrimeStatus(userId string) (bool, error) {
	requestCtx := wrapper.GenerateRequestContext(ServiceName, getPremiumStatusesAPIName)
	request := nitro.GetPremiumStatusesRequest{
		TwitchUserID: userId,
	}
	response, err := n.Client.GetPremiumStatuses(requestCtx, &request)

	if err != nil {
		return false, errors.Wrap(err, "failed to get premium status")
	}

	return response.GetHasPrime(), nil
}
