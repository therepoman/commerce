package nitro_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/fortuna/clients/nitro"
	"code.justin.tv/commerce/fortuna/mocks"
	nitrorpc "code.justin.tv/samus/nitro/rpc"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func TestNitro(t *testing.T) {
	Convey("TestNitroClient", t, func() {
		mockClient := new(mocks.Nitro)
		nitroClient := nitro.NitroClient{
			Client: mockClient,
		}

		Convey("TestGetPremiumStatuses", func() {
			Convey("with success", func() {
				resp := nitrorpc.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}
				mockClient.On("GetPremiumStatuses", Anything, Anything).Return(&resp, nil)
				isPrime, err := nitroClient.GetPrimeStatus("1")
				So(isPrime, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("with error", func() {
				mockClient.On("GetPremiumStatuses", Anything, Anything).Return(nil, errors.New("test"))
				isPrime, err := nitroClient.GetPrimeStatus("1")
				So(isPrime, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
