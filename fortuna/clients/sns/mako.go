package sns

import (
	"github.com/golang/protobuf/ptypes/timestamp"
)

// MakoEvent sns message
type MakoEvent struct {
	UserID   string               `json:"user_id" validate:"nonzero"` // Twitch user id
	Scope    string               `json:"scope" validate:"nonzero"`   // Scope for this event, registered during onboarding. Must be a valid event scope.
	Key      string               `json:"key" validate:"nonzero"`     // The unique key for this event.
	Value    string               `json:"value" validate:"nonzero"`   // The value of the event
	Origin   string               `json:"origin" validate:"nonzero"`  // The source of the event
	Version  string               `json:"version"`                    // The emote version of the event
	Source   string               `json:"source"`                     // The emote source of the event
	GroupID  string               `json:"group_id"`                   // The emote group of the event
	OwnerID  string               `json:"owner_id"`                   // The emote owner of the event
	StartsAt *timestamp.Timestamp `json:"starts_at"`                  // The start time of the event
	EndsAt   *timestamp.Timestamp `json:"ends_at"`                    // The end time of the event. Used for entitlement expiration.
}

// EventVersionMateria version for Materia
const EventVersionMateria = "2"

// MakoSource for mako
const MakoSource = "REWARDS"
