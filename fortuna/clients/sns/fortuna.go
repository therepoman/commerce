package sns

import "code.justin.tv/commerce/fortuna/sqs/model"

// DiscoveryEvent sns message
type DiscoveryEvent struct {
	UserID    string `json:"user_id" validate:"nonzero"`    // Twitch user id
	ChannelID string `json:"channel_id" validate:"nonzero"` // Channel id this event is related to
	Type      string `json:"type" validate:"nonzero"`       // The event type
}

// CompletedChallengeEvent sns message
type CompletedChallengeEvent struct {
	UserID      string `json:"user_id"`      // The tuid that completed the challenge/milestone
	ObjectiveID string `json:"objective_id"` // The objective id containing the completed milestone
	MilestoneID string `json:"milestone_id"` // The milestone id of the completed milestone
	Domain      string `json:"domain"`       // The domain this milestone belongs to
}

type MegaCheerGiftFulfillmentSNSEvent struct {
	Message string `json:"Message"`
}

type MegaCheerGiftFulfillmentEvent struct {
	BenefactorLogin string                  `json:"benefactorLogin" validate:"nonzero"`
	RecipientID     string                  `json:"recipientID" validate:"nonzero"`
	Domain          string                  `json:"domain" validate:"nonzero"`
	TriggerID       string                  `json:"triggerID" validate:"nonzero"`
	Event           model.MegaCommerceEvent `json:"event" validate:"nonzero"`
	SelectionAmount int                     `json:"selectionAmount" validate:"nonzero"`
}
