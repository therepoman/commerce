package sns

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/pkg/errors"
)

// Client defines SNS struct
type Client struct {
	sns *sns.SNS
}

// ISNSClient defines the SNS client interface
type ISNSClient interface {
	PostToTopic(topic string, msg string) error
	PostToTopicWithMessageAttributes(topic string, msg string, msgAttributes map[string]*sns.MessageAttributeValue) error
}

const (
	defaultRegion = "us-west-2"
)

// MessageAttributesV1 for older messages (mako)
var MessageAttributesV1 = map[string]*sns.MessageAttributeValue{
	"Version": {
		DataType:    aws.String("Number"),
		StringValue: aws.String("1"),
	},
}

// NewDefaultConfig sets up a default region configuration
func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

// NewSnsClient creates new SNS client
func NewSnsClient() ISNSClient {
	return &Client{
		sns: sns.New(session.New(), NewDefaultConfig()),
	}
}

// PostToTopic posts to SNS ARN with message
func (s *Client) PostToTopic(topic string, msg string) error {
	return s.PostToTopicWithMessageAttributes(topic, msg, nil)
}

// PostToTopicWithMessageAttributes posts to SNS ARN with message and message attributes
func (s *Client) PostToTopicWithMessageAttributes(topic string, msg string, msgAttributes map[string]*sns.MessageAttributeValue) error {
	publishInput := &sns.PublishInput{
		Message:           aws.String(msg),
		TopicArn:          aws.String(topic),
		MessageAttributes: msgAttributes,
	}
	_, err := s.sns.Publish(publishInput)
	if err != nil {
		return errors.Wrap(err, "Error posting to topic")
	}
	return nil
}
