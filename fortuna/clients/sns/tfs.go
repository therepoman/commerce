package sns

// TwitchFulfillmentEvent sns message
type TwitchFulfillmentEvent struct {
	IdempotenceKey string            `json:"idempotenceKey"` // IdempotenceKey a key used to uniquely identify this request. It's value should allow for duplicated requests to be detected
	Source         string            `json:"source"`         // Source Who is send the fulfillment request
	SKU            string            `json:"sku"`            // SKU The sku to be fulfilled
	Vendor         string            `json:"vendor"`         // Vendor The vendor of the product
	TUID           string            `json:"tuid"`           // TUID The twitch User ID
	Quantity       int64             `json:"quantity"`       // Quantity how many items to fulfill
	Metadata       map[string]string `json:"metadata"`       // Metadata Extra metadata about this request
}
