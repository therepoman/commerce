package chatters

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"

	"code.justin.tv/chat/chatters/rpc/chatters"
	"code.justin.tv/commerce/fortuna/clients/wrapper"
)

const (
	ServiceName          = "chatters"
	GetChannelViewersApi = "GetChannelViewers"
)

type IChattersClient interface {
	GetChannelViewers(channelLogin string) (int, []string, error)
}

type ChattersClient struct {
	Client chatters.Chatters
}

func (cc *ChattersClient) GetChannelViewers(channelLogin string) (int, []string, error) {
	//if channel == jwp then return the stubbed list
	if strings.ToLower(channelLogin) == "jwp" {
		// DELETE ME: for load testing only!!!
		output := []string{"vibhavb_prod_load_aa1", "vibhavb_prod_load_aa2", "vibhavb_prod_load_aa3", "vibhavb_prod_load_ab0", "vibhavb_prod_load_ab1", "vibhavb_prod_load_ab2", "vibhavb_prod_load_ab3", "vibhavb_prod_load_ac0", "vibhavb_prod_load_ac1", "vibhavb_prod_load_ac2", "vibhavb_prod_load_ac3", "vibhavb_prod_load_ad0", "vibhavb_prod_load_ad1", "vibhavb_prod_load_ad2", "vibhavb_prod_load_ad3", "vibhavb_prod_load_be0", "vibhavb_prod_load_be1", "vibhavb_prod_load_ci2", "vibhavb_prod_load_ci1", "vibhavb_prod_load_ci0", "vibhavb_prod_load_bg3", "vibhavb_prod_load_bg2", "vibhavb_prod_load_bg1", "vibhavb_prod_load_bg0"}
		return len(output), output, nil
	}

	ctx := wrapper.GenerateRequestContext(ServiceName, GetChannelViewersApi)
	resp, err := cc.Client.ListChattersUnsegmented(ctx, &chatters.ListChattersUnsegmentedReq{Login: channelLogin, Limit: 500000, Offset: 0})
	if err != nil {
		wrappedErr := errors.Wrap(err, "ChattersClient: Failed to get chatters data")
		return 0, []string{}, wrappedErr
	}

	return len(resp.Chatters), resp.Chatters, nil
}

func getMetricName(apiName string) string {
	return fmt.Sprintf("service.%s.%s", ServiceName, apiName)
}
