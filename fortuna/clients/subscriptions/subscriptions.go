package subscriptions

import (
	"code.justin.tv/commerce/fortuna/clients/wrapper"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	ServiceName                        = "subscriptions"
	getUserChannelSubscriptionAPIName  = "GetUserChannelSubscription"
	getUserProductSubscriptionsAPIName = "GetUserProductSubscriptions"
	getProductsByIDsAPIName            = "GetProductsByIDs"
)

type ISubscriptionsClient interface {
	GetUserChannelSubscription(userID, channelID string) (*substwirp.GetUserChannelSubscriptionResponse, error)
	GetUserProductSubscriptions(subscriptionProductIDs []string, userID string) (*substwirp.GetUserProductSubscriptionsResponse, error)
	GetProductsByIDs(productIDs []string) (*substwirp.GetProductsByIDsResponse, error)
}

type Client struct {
	SubsClient substwirp.Subscriptions
}

// GetUserChannelSubscription delegates to the Subscriptions API to retrieve an enriched representation of a user's active
// subscription to the channel corresponding to the passed channelID. If the response's `Subscription` field is nil,
// the user does not have an active subscription to the given channel. You may check the response's `Subscription.ProductId`
// field to learn specifically which ticket product the user is entitled to.
func (c *Client) GetUserChannelSubscription(userID, channelID string) (*substwirp.GetUserChannelSubscriptionResponse, error) {
	req := &substwirp.GetUserChannelSubscriptionRequest{
		OwnerId:   userID,
		ChannelId: channelID,
	}

	ctx := wrapper.GenerateRequestContext(ServiceName, getUserChannelSubscriptionAPIName)

	log.Debugf("Calling Subscriptions/GetUserChannelSubscription with userID: %s channelID: %s", userID, channelID)

	res, err := c.SubsClient.GetUserChannelSubscription(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "Subscriptions client call failed")
	}

	return res, nil
}

// GetUserProductSubscriptions calls the subscriptions service to retrieve a set of a user's subscriptions based on subscription product IDs.
// A subscription will be included in the response for every input subscription product ID that the user is entitled to.
func (c *Client) GetUserProductSubscriptions(subscriptionProductIDs []string, userID string) (*substwirp.GetUserProductSubscriptionsResponse, error) {
	req := &substwirp.GetUserProductSubscriptionsRequest{
		OwnerId:    userID,
		ProductIds: subscriptionProductIDs,
	}

	ctx := wrapper.GenerateRequestContext(ServiceName, getUserProductSubscriptionsAPIName)

	log.Debugf("Calling Subscriptions/GetUserProductSubscriptions with userID: %s productIDs: %s", userID, subscriptionProductIDs)

	res, err := c.SubsClient.GetUserProductSubscriptions(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "Subscriptions client call failed")
	}

	return res, nil
}

// GetProductsByIDs calls the subscriptions service to retrieve a set of subscription products by IDs.
func (c *Client) GetProductsByIDs(productIDs []string) (*substwirp.GetProductsByIDsResponse, error) {
	req := &substwirp.GetProductsByIDsRequest{
		ProductIds: productIDs,
	}

	ctx := wrapper.GenerateRequestContext(ServiceName, getProductsByIDsAPIName)

	log.Debugf("Calling Subscriptions/GetProductsByIDs with productIDs: %s", productIDs)

	res, err := c.SubsClient.GetProductsByIDs(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "Subscriptions client call failed")
	}

	return res, nil
}
