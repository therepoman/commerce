package subscriptions_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/fortuna/clients/subscriptions"
	"code.justin.tv/commerce/fortuna/mocks"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	testUserID    = "11111"
	testChannelID = "22222"
	testProductID = "33333"
)

func TestSubscriptions(t *testing.T) {
	Convey("Test SubscriptionsClient", t, func() {
		mockSubscriptionsClient := new(mocks.Subscriptions)
		subscriptionsClient := subscriptions.Client{
			SubsClient: mockSubscriptionsClient,
		}

		Convey("Test GetUserChannelSubscription", func() {
			Convey("With success", func() {
				res := &substwirp.GetUserChannelSubscriptionResponse{}
				mockSubscriptionsClient.On("GetUserChannelSubscription", Anything, Anything).Return(res, nil)

				result, err := subscriptionsClient.GetUserChannelSubscription(testUserID, testChannelID)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
			})
			Convey("With client error", func() {
				mockSubscriptionsClient.On("GetUserChannelSubscription", Anything, Anything).Return(nil, errors.New("test subs error"))

				result, err := subscriptionsClient.GetUserChannelSubscription(testUserID, testChannelID)
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("Test GetUserProductSubscriptions", func() {
			testProductIDs := []string{testProductID}

			Convey("With success", func() {
				res := &substwirp.GetUserProductSubscriptionsResponse{}
				mockSubscriptionsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(res, nil)

				result, err := subscriptionsClient.GetUserProductSubscriptions(testProductIDs, testUserID)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
			})
			Convey("With client error", func() {
				mockSubscriptionsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(nil, errors.New("test subs error"))

				result, err := subscriptionsClient.GetUserProductSubscriptions(testProductIDs, testUserID)
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("Test GetProductsByIDs", func() {
			testProductIDs := []string{testProductID}

			Convey("With success", func() {
				res := &substwirp.GetProductsByIDsResponse{}
				mockSubscriptionsClient.On("GetProductsByIDs", Anything, Anything).Return(res, nil)

				result, err := subscriptionsClient.GetProductsByIDs(testProductIDs)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
			})
			Convey("With client error", func() {
				mockSubscriptionsClient.On("GetProductsByIDs", Anything, Anything).Return(nil, errors.New("test subs error"))

				result, err := subscriptionsClient.GetProductsByIDs(testProductIDs)
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})
	})
}
