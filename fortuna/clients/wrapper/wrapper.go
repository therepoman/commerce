package wrapper

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/foundation/twitchclient"
)

const (
	metricPattern         = "service.%s.%s"
	defaultStatSampleRate = 1.0
	defaultServiceName    = "unknownservice"
	defaultAPIName        = "unknownapi"
)

func GenerateRequestContext(serviceName string, apiName string) context.Context {
	if serviceName == "" {
		log.WithError(errors.New("Service name must not be empty when generating client request context")).Error("Service name must not be empty when generating client request context")
		serviceName = defaultServiceName
	}

	if apiName == "" {
		log.WithError(errors.New("API name must not be empty when generating client request context")).Error("API name must not be empty when generating client request context")
		apiName = defaultAPIName
	}

	reqOpts := twitchclient.ReqOpts{}
	reqOpts.StatName = fmt.Sprintf(metricPattern, serviceName, apiName)
	reqOpts.StatSampleRate = defaultStatSampleRate
	ctx := twitchclient.WithReqOpts(context.Background(), reqOpts)
	return ctx
}
