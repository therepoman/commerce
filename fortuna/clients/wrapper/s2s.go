package wrapper

import (
	"fmt"
	"net/http"

	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// GenerateS2SRoundTripper : returns a slice of functions of length 1, which can be used as a RoundTripperWrappers field for twitchclient.ClientConf construction
// to use as a s2s client config
func GenerateS2SRoundTripper(env string, doneChan chan struct{}, errChan chan struct{}) func(http.RoundTripper) http.RoundTripper {
	var cfg *caller.Config

	wrapperFunction := func(inner http.RoundTripper) (outer http.RoundTripper) {
		outer, err := caller.NewWithCustomRoundTripper(fmt.Sprintf("fortuna-%s", env), cfg, inner, log.New())
		if err != nil {
			log.WithError(err).Error(errors.Wrap(err, "failed to wrap http.RoundTripper with s2s"))
			close(errChan)
		} else {
			close(doneChan)
		}
		return
	}

	return wrapperFunction
}
