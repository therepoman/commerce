package wrapper_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/clients/wrapper"
	. "github.com/smartystreets/goconvey/convey"
)

func TestS2SWrapper(t *testing.T) {
	Convey("GenerateS2SRoundTripper", t, func() {
		// There is not much we can test as the wrappingfuctionality is provided by the s2s library and the resulted function is executed within the twitch http client library
		s2sRoundTripper := wrapper.GenerateS2SRoundTripper("test", make(chan struct{}), make(chan struct{}))
		So(s2sRoundTripper, ShouldNotBeNil)
	})
}
