package wrapper

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGenerateRequestContext(t *testing.T) {
	Convey("Test GenerateRequestContext", t, func() {

		Convey("with valid input", func() {
			ctx := GenerateRequestContext("bar", "foo")
			So(ctx, ShouldNotBeNil)
		})

		Convey("with missing service name", func() {
			ctx := GenerateRequestContext("", "foo")
			So(ctx, ShouldNotBeNil)
		})
		Convey("with missing api name", func() {
			ctx := GenerateRequestContext("bar", "")
			So(ctx, ShouldNotBeNil)
		})

		Convey("with missing both", func() {
			ctx := GenerateRequestContext("", "")
			So(ctx, ShouldNotBeNil)
		})
	})
}
