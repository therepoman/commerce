package mako_test

import (
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/fortuna/clients/mako"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/mako/twirp"
	lru "github.com/hashicorp/golang-lru"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	emoteSetId = "1"
)

func TestMako(t *testing.T) {
	Convey("TestMakoClient", t, func() {
		lruCache, _ := lru.New(1)
		mockMakoClient := new(mocks.Mako)

		makoClient := mako.MakoClient{
			Cache:  lruCache,
			Client: mockMakoClient,
		}

		makoResp := &mako_client.GetEmoticonsByGroupsResponse{
			Groups: []*mako_client.EmoticonGroup{
				{
					Id: "21889",
					Emoticons: []*mako_client.Emoticon{
						{
							Id:   "160148",
							Code: "SeemsMinton",
						},
					},
				},
			},
		}

		Convey("Test GetEmoteSetByID", func() {
			Convey("succeeds", func() {
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(makoResp, nil)
				expected := mako.RewardItem{
					ItemType:   "Emote",
					ID:         "160148",
					Text:       "SeemsMinton",
					EmoteSetID: "21889",
				}

				Convey("with cache miss", func() {
					rewardDetails, err := makoClient.GetEmoteSetByID(emoteSetId)
					mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
					So(rewardDetails, ShouldResemble, expected)
					So(err, ShouldBeNil)
				})

				Convey("with cache hit", func() {
					rewardDetails, err := makoClient.GetEmoteSetByID(emoteSetId)
					mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
					So(rewardDetails, ShouldResemble, expected)
					So(err, ShouldBeNil)
				})
			})
			Convey("with GetEmoticonsByGroups fails", func() {
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("test error fail"))

				rewardDetails, err := makoClient.GetEmoteSetByID(emoteSetId)
				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
				So(rewardDetails, ShouldResemble, mako.RewardItem{})
				So(err, ShouldNotBeNil)
			})
			Convey("with GetEmoticonsByGroups returns empty group", func() {
				ret := &mako_client.GetEmoticonsByGroupsResponse{
					Groups: []*mako_client.EmoticonGroup{},
				}
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(ret, nil)

				rewardDetails, err := makoClient.GetEmoteSetByID(emoteSetId)
				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
				So(rewardDetails, ShouldResemble, mako.RewardItem{})
				So(err, ShouldNotBeNil)
			})
			Convey("with GetEmoticonsByGroups returns empty emote set", func() {
				ret := &mako_client.GetEmoticonsByGroupsResponse{
					Groups: []*mako_client.EmoticonGroup{
						{
							Id:        emoteSetId,
							Emoticons: []*mako_client.Emoticon{},
						},
					},
				}
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(ret, nil)

				rewardDetails, err := makoClient.GetEmoteSetByID(emoteSetId)
				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
				So(rewardDetails, ShouldResemble, mako.RewardItem{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test BulkGetEmoteSetsByIDs", func() {
			Convey("Successful request (all cache misses)", func() {
				emoteSetIDs := []string{"1", "2", "3"}
				expectedRequest := &mako_client.GetEmoticonsByGroupsRequest{
					EmoticonGroupKeys: emoteSetIDs,
				}

				makoResponse := expectedMakoEmoticonsGroupsResponseFor(emoteSetIDs)

				mockMakoClient.On("GetEmoticonsByGroups", Anything, expectedRequest).Return(makoResponse, nil)
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("unexpected request"))

				expectedResult := []mako.RewardItem{
					{
						ItemType:   "Emote",
						ID:         "test-emote-1",
						Text:       "test-emote-code-1",
						EmoteSetID: "1",
					},
					{
						ItemType:   "Emote",
						ID:         "test-emote-2",
						Text:       "test-emote-code-2",
						EmoteSetID: "2",
					},
					{
						ItemType:   "Emote",
						ID:         "test-emote-3",
						Text:       "test-emote-code-3",
						EmoteSetID: "3",
					},
				}

				actualResult, err := makoClient.BulkGetEmoteSetsByIDs(emoteSetIDs)
				So(err, ShouldBeNil)
				So(actualResult, ShouldResemble, expectedResult)

				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
			})

			Convey("Successful request (all cache hits)", func() {
				lruCache, _ := lru.New(3)
				makoClient := &mako.MakoClient{
					Cache:  lruCache,
					Client: mockMakoClient,
				}

				emoteSetIDs := []string{"1", "2", "3"}
				for _, id := range emoteSetIDs {
					lruCache.Add(id, mako.RewardItem{
						ItemType:   "Emote",
						ID:         fmt.Sprintf("test-emote-%s", id),
						Text:       fmt.Sprintf("test-emote-code-%s", id),
						EmoteSetID: id,
					})
				}

				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("unexpected request"))

				expectedResult := []mako.RewardItem{
					{
						ItemType:   "Emote",
						ID:         "test-emote-1",
						Text:       "test-emote-code-1",
						EmoteSetID: "1",
					},
					{
						ItemType:   "Emote",
						ID:         "test-emote-2",
						Text:       "test-emote-code-2",
						EmoteSetID: "2",
					},
					{
						ItemType:   "Emote",
						ID:         "test-emote-3",
						Text:       "test-emote-code-3",
						EmoteSetID: "3",
					},
				}

				actualResult, err := makoClient.BulkGetEmoteSetsByIDs(emoteSetIDs)
				So(err, ShouldBeNil)
				So(actualResult, ShouldResemble, expectedResult)

				// No network activity: all items should be found in cache
				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 0)
			})

			Convey("Successful request (partial cache hit)", func() {
				lruCache, _ := lru.New(3)
				makoClient := &mako.MakoClient{
					Cache:  lruCache,
					Client: mockMakoClient,
				}

				emoteSetIDs := []string{"1", "2", "3"}
				lruCache.Add("2", mako.RewardItem{
					ItemType:   "Emote",
					ID:         "test-emote-2",
					Text:       "test-emote-code-2",
					EmoteSetID: "2",
				})

				expectedMakoRequest := &mako_client.GetEmoticonsByGroupsRequest{
					EmoticonGroupKeys: []string{"1", "3"},
				}

				expectedMakoResponse := expectedMakoEmoticonsGroupsResponseFor([]string{"1", "3"})

				mockMakoClient.On("GetEmoticonsByGroups", Anything, expectedMakoRequest).Return(expectedMakoResponse, nil)
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("unexpected request"))

				expectedResult := []mako.RewardItem{
					{
						ItemType:   "Emote",
						ID:         "test-emote-1",
						Text:       "test-emote-code-1",
						EmoteSetID: "1",
					},
					{
						ItemType:   "Emote",
						ID:         "test-emote-2",
						Text:       "test-emote-code-2",
						EmoteSetID: "2",
					},
					{
						ItemType:   "Emote",
						ID:         "test-emote-3",
						Text:       "test-emote-code-3",
						EmoteSetID: "3",
					},
				}

				actualResult, err := makoClient.BulkGetEmoteSetsByIDs(emoteSetIDs)
				So(err, ShouldBeNil)
				So(actualResult, ShouldResemble, expectedResult)

				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
			})

			Convey("Mako::GetEmoticonsByGroups fails", func() {
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("the system is down"))

				_, err := makoClient.BulkGetEmoteSetsByIDs([]string{"1", "2", "3"})
				So(err, ShouldNotBeNil)

				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
			})

			Convey("with an emoticon group not present in response", func() {
				emoteSetIDs := []string{"1", "2", "3"}

				makoResponse := expectedMakoEmoticonsGroupsResponseFor(emoteSetIDs)
				// Slice one of the groups out
				makoResponse.Groups = makoResponse.Groups[1:]

				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(makoResponse, nil)

				res, err := makoClient.BulkGetEmoteSetsByIDs(emoteSetIDs)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with an empty emoticon group", func() {
				emoteSetIDs := []string{"1", "2", "3"}

				makoResponse := expectedMakoEmoticonsGroupsResponseFor(emoteSetIDs)
				makoResponse.Groups[1].Emoticons = []*mako_client.Emoticon{}

				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(makoResponse, nil)

				res, err := makoClient.BulkGetEmoteSetsByIDs(emoteSetIDs)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with a poisoned cache", func() {
				lruCache, _ := lru.New(1)
				emoteSetIDs := []string{"1", "2"}

				lruCache.Add("2", struct{ taunt string }{taunt: "I'm a naughty struct"})

				makoResponse := expectedMakoEmoticonsGroupsResponseFor(emoteSetIDs)

				expectedMakoRequest := &mako_client.GetEmoticonsByGroupsRequest{
					EmoticonGroupKeys: emoteSetIDs,
				}

				mockMakoClient.On("GetEmoticonsByGroups", Anything, expectedMakoRequest).Return(makoResponse, nil)
				mockMakoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("unexpected request"))

				res, err := makoClient.BulkGetEmoteSetsByIDs(emoteSetIDs)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)

				mockMakoClient.AssertNumberOfCalls(t, "GetEmoticonsByGroups", 1)
			})
		})
	})
}

func expectedMakoEmoticonsGroupsResponseFor(emoteSetIDs []string) *mako_client.GetEmoticonsByGroupsResponse {
	res := &mako_client.GetEmoticonsByGroupsResponse{
		Groups: make([]*mako_client.EmoticonGroup, 0, len(emoteSetIDs)),
	}

	for _, id := range emoteSetIDs {
		res.Groups = append(res.Groups, &mako_client.EmoticonGroup{
			Id: id,
			Emoticons: []*mako_client.Emoticon{
				{
					Id:   fmt.Sprintf("test-emote-%s", id),
					Code: fmt.Sprintf("test-emote-code-%s", id),
				},
			},
		})
	}

	return res
}
