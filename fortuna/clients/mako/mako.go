package mako

import (
	"context"

	"code.justin.tv/commerce/fortuna/clients/wrapper"
	"code.justin.tv/commerce/mako/twirp"
	"github.com/hashicorp/golang-lru"
	"github.com/pkg/errors"

	log "github.com/sirupsen/logrus"
)

const (
	emoteType            = "Emote"
	MakoEmoteScopeFormat = "/emote_sets/commerce/campaign/%s/"

	// Metrics fields
	ServiceName                         = "mako"
	GetEmoteEntitlementsAPIName         = "GetEmoteEntitlements"
	GetEmoticonsByGroupsAPIName         = "GetEmoticonsByGroups"
	DeleteEmoteGroupEntitlementsAPIName = "DeleteEmoteGroupEntitlements"
)

// Helper Data Structs
type RewardItem struct {
	ItemType   string
	ID         string
	Text       string
	EmoteSetID string
}

type MakoClient struct {
	Cache  *lru.Cache
	Client mako_client.Mako
}

type IMakoClient interface {
	GetEmoteSetByID(emoteSetID string) (RewardItem, error)
	BulkGetEmoteSetsByIDs(emoteSetIDs []string) ([]RewardItem, error)
	GetEntitledEmotes(userID string, domain string) (*mako_client.GetEmoteEntitlementsResponse, error)
	DeleteEmoteEntitlements(req *mako_client.DeleteEmoteGroupEntitlementsRequest) (*mako_client.DeleteEmoteGroupEntitlementsResponse, error)
	GetEntitlementMapByDomain(domain string, userID string) (map[string]int32, error)
	GetEntitlements(request *mako_client.GetEmoteEntitlementsRequest, reqCtx context.Context) (*mako_client.GetEmoteEntitlementsResponse, error)
}

func (m *MakoClient) GetEmoteSetByID(emoteSetID string) (RewardItem, error) {
	// Check cache first
	val, found := m.Cache.Get(emoteSetID)
	if found {
		cached, ok := val.(RewardItem)
		if !ok {
			return RewardItem{}, errors.WithStack(errors.New("failed to convert cache value"))
		}
		return cached, nil
	}

	req := &mako_client.GetEmoticonsByGroupsRequest{
		EmoticonGroupKeys: []string{emoteSetID},
	}

	requestCtx := wrapper.GenerateRequestContext(ServiceName, GetEmoticonsByGroupsAPIName)
	resp, err := m.Client.GetEmoticonsByGroups(requestCtx, req)
	if err != nil {
		return RewardItem{}, errors.Wrap(err, "failed to get emoticons by groups")
	}

	if len(resp.Groups) == 0 {
		errorMessage := "Mako emote set response does not contain any emote sets"
		err := errors.New(errorMessage)
		log.WithFields(log.Fields{
			"emoteSetID": emoteSetID,
		}).WithError(err).Error(errorMessage)
		return RewardItem{}, errors.WithStack(err)
	}

	// Get the first emote set in groups since we are only asking for one emote set
	emoteSet := resp.Groups[0]

	if len(emoteSet.Emoticons) == 0 {
		errorMessage := "Mako emote set does not contain any emotes"
		err := errors.New(errorMessage)
		log.WithFields(log.Fields{
			"emoteSetID": emoteSetID,
		}).WithError(err).Error(errorMessage)
		return RewardItem{}, errors.WithStack(err)
	}

	// Get the first emote in the emote set. Assuming that each emote set only has 1 reward item
	emote := emoteSet.Emoticons[0]

	emoteSetDetails := makoEmoticonToRewardItem(emoteSet.Id, emote)
	m.Cache.Add(emoteSetID, emoteSetDetails)

	return emoteSetDetails, nil
}

func (m *MakoClient) BulkGetEmoteSetsByIDs(emoteSetIDs []string) ([]RewardItem, error) {
	rewardMap := make(map[string]RewardItem)
	requestKeys := make([]string, 0, len(emoteSetIDs))

	for _, id := range emoteSetIDs {
		val, found := m.Cache.Get(id)
		if found {
			// Cache hit: put into result map directly
			if cached, ok := val.(RewardItem); ok {
				rewardMap[id] = cached
			} else {
				log.WithField("emoteSetID", id).
					Debug("Encountered unexpected type for cached RewardItem. Deleting cache entry")

				// Cache entry corrupt: delete and re-request this emoticon
				m.Cache.Remove(id)
				requestKeys = append(requestKeys, id)
			}
		} else {
			// Cache miss: add as an emote ID to request from Mako
			requestKeys = append(requestKeys, id)
		}
	}

	if len(requestKeys) > 0 {
		request := &mako_client.GetEmoticonsByGroupsRequest{
			EmoticonGroupKeys: requestKeys,
		}

		reqCtx := wrapper.GenerateRequestContext(ServiceName, GetEmoticonsByGroupsAPIName)

		resp, err := m.Client.GetEmoticonsByGroups(reqCtx, request)
		if err != nil {
			return nil, errors.Wrap(err, "MakoClient failed bulk request for emotes")
		}

		for _, emoticonGroup := range resp.Groups {
			if len(emoticonGroup.Emoticons) < 1 {
				return nil, errors.Errorf("An emoticon group was empty. Emoticon Group ID: %s", emoticonGroup.Id)
			}
			// Note: we assume we're only associating one emoticon to each emoticon group
			rewardItem := makoEmoticonToRewardItem(emoticonGroup.Id, emoticonGroup.Emoticons[0])

			rewardMap[emoticonGroup.Id] = rewardItem
			m.Cache.Add(emoticonGroup.Id, rewardItem)
		}
	}

	out := make([]RewardItem, len(emoteSetIDs))
	for i, id := range emoteSetIDs {
		if reward, ok := rewardMap[id]; ok {
			out[i] = reward
		} else {
			return nil, errors.Errorf("Missing emote information for ID: %s", id)
		}
	}

	return out, nil
}

//GetEntitledEmotes returns emote rewards that are already entitled to the user.
func (m *MakoClient) GetEntitledEmotes(userID string, domain string) (*mako_client.GetEmoteEntitlementsResponse, error) {
	if userID == "" {
		return &mako_client.GetEmoteEntitlementsResponse{}, nil
	}

	// fetch user emote set entitlements
	request := &mako_client.GetEmoteEntitlementsRequest{
		OwnerId:  userID,
		OriginId: domain,
		Group:    mako_client.EmoteGroup_Rewards,
	}

	reqCtx := wrapper.GenerateRequestContext(ServiceName, GetEmoteEntitlementsAPIName)

	resp, err := m.Client.GetEmoteEntitlements(reqCtx, request)
	if err != nil {
		return &mako_client.GetEmoteEntitlementsResponse{}, errors.Wrap(err, "Error while calling Mako GetEmoteEntitlements")
	}

	return resp, nil
}

func (m *MakoClient) GetEntitlements(request *mako_client.GetEmoteEntitlementsRequest, reqCtx context.Context) (*mako_client.GetEmoteEntitlementsResponse, error) {
	resp, err := m.Client.GetEmoteEntitlements(reqCtx, request)
	if err != nil {
		return &mako_client.GetEmoteEntitlementsResponse{}, errors.Wrap(err, "Error while calling Mako GetEmoteEntitlements")
	}

	return resp, nil
}

func (m *MakoClient) GetEntitlementMapByDomain(domain string, userID string) (map[string]int32, error) {
	userEntitlementMap := map[string]int32{}
	resp, err := m.GetEntitledEmotes(userID, domain)
	if err != nil {
		return nil, errors.Wrap(err, "Error while calling Mako GetEmoteEntitlements")
	}

	for _, entitlement := range resp.Entitlements {
		emoteID := entitlement.EmoteId
		userEntitlementMap[emoteID] = 1
	}

	return userEntitlementMap, nil
}

//DeleteEntitledEmote deletes the given user's entitled emoteSetID and returns true if successfully deleted
func (m *MakoClient) DeleteEmoteEntitlements(req *mako_client.DeleteEmoteGroupEntitlementsRequest) (*mako_client.DeleteEmoteGroupEntitlementsResponse, error) {
	reqCtx := wrapper.GenerateRequestContext(ServiceName, DeleteEmoteGroupEntitlementsAPIName)
	return m.Client.DeleteEmoteGroupEntitlements(reqCtx, req)
}

func makoEmoticonToRewardItem(groupID string, emoticon *mako_client.Emoticon) RewardItem {
	return RewardItem{
		ItemType:   emoteType,
		ID:         emoticon.Id,
		Text:       emoticon.Code,
		EmoteSetID: groupID,
	}
}
