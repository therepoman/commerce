package clients

import (
	"net/http"
	"time"

	followsrpc "code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	chattersrpc "code.justin.tv/chat/chatters/rpc/chatters"
	pubClient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients/chatters"
	"code.justin.tv/commerce/fortuna/clients/dynamo"
	"code.justin.tv/commerce/fortuna/clients/follows"
	"code.justin.tv/commerce/fortuna/clients/mako"
	"code.justin.tv/commerce/fortuna/clients/nitro"
	"code.justin.tv/commerce/fortuna/clients/pubsub"
	"code.justin.tv/commerce/fortuna/clients/pushy"
	"code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/clients/spade"
	"code.justin.tv/commerce/fortuna/clients/stepfn"
	"code.justin.tv/commerce/fortuna/clients/subscriptions"
	"code.justin.tv/commerce/fortuna/clients/tmi"
	"code.justin.tv/commerce/fortuna/clients/users"
	"code.justin.tv/commerce/fortuna/clients/wrapper"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/s3"
	mako_client "code.justin.tv/commerce/mako/twirp"
	goSpade "code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	nitroRpc "code.justin.tv/samus/nitro/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	lru "github.com/hashicorp/golang-lru"
	"github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// Clients exposes methods to use other clients
type Clients struct {
	DynamoClient        *dynamo.Client
	InMemoryCache       *cache.Cache
	LRUCache            *lru.Cache
	MakoClient          mako.IMakoClient
	NitroClient         nitro.INitroClient
	Notifier            tmi.Notifier
	PubSubClient        pubsub.IPubSubClient
	PushyClient         pushy.Publisher
	SNSClient           sns.ISNSClient
	SQSClient           sqsiface.SQSAPI
	SpadeClient         goSpade.Client
	StepfnClient        stepfn.IStepfnClient
	SubscriptionsClient subscriptions.ISubscriptionsClient
	ChattersClient      chatters.IChattersClient
	FollowsClient       follows.IFollowsClient
	UsersClient         users.IUsersClient
	S3Client            s3.IS3Client
}

func SetupClients(cfg *config.Configuration, campaignConfig *campaign.Configuration, stats statsd.Statter, s3Client s3.IS3Client) (*Clients, error) {
	lruCache, err := lru.New(cfg.CacheSize)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init lruCache")
	}

	inMemoryCache := cache.New(
		time.Minute*time.Duration(cfg.InMemoryCacheExpiration),
		time.Minute*time.Duration(cfg.InMemoryCachePurgeDuration),
	)

	// Mako
	makoClient := mako_client.NewMakoProtobufClient(cfg.MakoHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: cfg.MakoHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.MakoMaxIdleConnections,
		},
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(mako.ServiceName, cfg.MakoTimeoutMilliseconds),
			wrapper.NewRetryRoundTripWrapper(stats, mako.ServiceName),
		},
	}))

	makoWrapper := &mako.MakoClient{
		Cache:  lruCache,
		Client: makoClient,
	}

	// Dynamo
	dynamoClient := dynamo.NewDynamoClient(cfg)

	// Chatters
	chattersClient := chattersrpc.NewChattersProtobufClient(cfg.ChattersHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: cfg.ChattersHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.ChattersMaxIdleConnections,
		},
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(chatters.ServiceName, cfg.ChattersTimeoutMilliseconds),
			wrapper.NewRetryRoundTripWrapper(stats, chatters.ServiceName),
		},
	}))

	chattersWrapper := &chatters.ChattersClient{
		Client: chattersClient,
	}

	followsClient := followsrpc.NewTwitchVXFollowingServiceECSProtobufClient(cfg.FollowsHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: cfg.FollowsHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.FollowsMaxIdleConnections,
		},
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(follows.ServiceName, cfg.FollowsTimeoutMilliseconds),
			wrapper.NewRetryRoundTripWrapper(stats, follows.ServiceName),
		},
	}))

	followsWrapper := &follows.FollowsClient{
		Client: followsClient,
	}
	// Pub
	pubClient, err := pubClient.NewPubClient(twitchclient.ClientConf{
		Host:  cfg.PubSubEndpoint,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(pubsub.ServiceName, cfg.PubsubTimeoutMilliseconds),
			wrapper.NewRetryRoundTripWrapper(stats, pubsub.ServiceName),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to init pubsub client")
	}

	pubWrapper := &pubsub.PubSubClient{
		Client: pubClient,
	}

	snsClient := sns.NewSnsClient()

	sqsClient := sqs.New(session.New(), aws.NewConfig().WithRegion(cfg.AWSRegion))

	// Spade
	spadeClient, err := spade.NewSpadeClient(cfg, stats)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init spade client")
	}

	log.Debugf("Initializing %s TMI Client", cfg.EnvironmentPrefix)
	// TMI
	tmiClient := tmi.NewTMIClient(cfg.TMIHost, stats)
	tmiNotifierClient := tmi.NewNotifier(tmiClient)

	log.Debugf("Initializing %s Users Client", cfg.EnvironmentPrefix)
	// Users
	usersClient, err := users.NewUsersClient(cfg.UsersServiceHost, stats, cfg.UsersTimeoutMilliseconds)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init users client")
	}

	// Subs
	subscriptionsClient := substwirp.NewSubscriptionsProtobufClient(cfg.SubscriptionsHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: cfg.SubscriptionsHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.SubscriptionsMaxIdleConnections,
		},
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(subscriptions.ServiceName, cfg.SubscriptionsTimeoutMilliseconds),
			wrapper.NewRetryRoundTripWrapper(stats, subscriptions.ServiceName),
		},
	}))

	subscriptionsWrapper := &subscriptions.Client{
		SubsClient: subscriptionsClient,
	}

	log.Debugf("Initializing %s Nitro Client", cfg.EnvironmentPrefix)
	// Nitro
	nitroClient := nitroRpc.NewNitroProtobufClient(cfg.NitroHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: cfg.NitroHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.NitroMaxIdleConnections,
		},
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper(nitro.ServiceName, cfg.NitroTimeoutMilliseconds),
			wrapper.NewRetryRoundTripWrapper(stats, nitro.ServiceName),
		},
	}))

	nitroWrapper := &nitro.NitroClient{
		Client: nitroClient,
	}

	log.Debugf("Initializing %s Pushy Client", cfg.EnvironmentPrefix)
	// Pushy
	pushyClient, err := pushy.NewPushyClient(cfg.PushyARN, stats)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init Pushy client")
	}

	// Stepfn
	stepfnClient := stepfn.NewClient(cfg, stats)

	return &Clients{
		DynamoClient:        dynamoClient,
		InMemoryCache:       inMemoryCache,
		LRUCache:            lruCache,
		MakoClient:          makoWrapper,
		NitroClient:         nitroWrapper,
		Notifier:            tmiNotifierClient,
		PubSubClient:        pubWrapper,
		PushyClient:         pushyClient,
		SNSClient:           snsClient,
		SQSClient:           sqsClient,
		SpadeClient:         spadeClient,
		StepfnClient:        stepfnClient,
		SubscriptionsClient: subscriptionsWrapper,
		ChattersClient:      chattersWrapper,
		FollowsClient:       followsWrapper,
		UsersClient:         usersClient,
		S3Client:            s3Client,
	}, nil
}
