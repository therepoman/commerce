package stepfn

import (
	"encoding/json"

	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/gogogadget/pointers"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

// Metrics fields
const (
	stepfnServiceName     = "stepfn"
	startExecutionAPIName = "StartExecution"
)

type IStepfnClient interface {
	Execute(stateMachineARN string, executionName string, executionInput interface{}) error
}

type Client struct {
	InternalClient sfniface.SFNAPI
	ServiceConfig  *config.Configuration
	Stats          statsd.Statter
}

// Force compiler to check that Client implements IStepfnClient
var _ IStepfnClient = &Client{}

func NewClient(serviceConfig *config.Configuration, stats statsd.Statter) *Client {
	s, _ := session.NewSession()
	sfnClient := sfn.New(s, aws.NewConfig().WithRegion(serviceConfig.AWSRegion))

	return &Client{
		InternalClient: sfnClient,
		ServiceConfig:  serviceConfig,
		Stats:          stats,
	}
}

func (c *Client) Execute(stateMachineARN string, executionName string, executionInput interface{}) error {
	defer func(startTime time.Time) {
		c.Stats.TimingDuration(fmt.Sprintf("service.%s.%s", stepfnServiceName, startExecutionAPIName), time.Since(startTime), 1.0)
	}(time.Now())

	inputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return errors.Wrapf(err, "failed to marshall state machine execution input. stateMachineARN: %s executionInput: %+v", stateMachineARN, executionInput)
	}

	startExecInput := &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputBytes)),
		Name:            &executionName,
		StateMachineArn: &stateMachineARN,
	}
	if _, err := c.InternalClient.StartExecution(startExecInput); err != nil {
		return errors.Wrapf(err, "failed to start state machine execution. stateMachineARN: %s startExecInput: %+v", stateMachineARN, startExecInput)
	}
	return nil
}
