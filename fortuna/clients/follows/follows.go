package follows

import (
	"errors"

	followsrpc "code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	"code.justin.tv/commerce/fortuna/clients/wrapper"
	"github.com/sirupsen/logrus"
)

const (
	ServiceName          = "follows"
	ListFollowersAPIName = "ListFollowers"

	listLimitSize = 600
)

type IFollowsClient interface {
	ListFollowers(channelLogin string, max int) (int, []string, error)
}

type FollowsClient struct {
	Client followsrpc.TwitchVXFollowingServiceECS
}

func (f *FollowsClient) ListFollowers(channelLogin string, max int) (int, []string, error) {
	requestCtx := wrapper.GenerateRequestContext(ServiceName, ListFollowersAPIName)
	var followers []string
	totalCount := 0
	cursor := ""

	for totalCount < max {
		req := &followsrpc.FollowsReq{
			UserId: channelLogin,
			Limit:  listLimitSize,
			Cursor: cursor,
		}
		res, err := f.Client.ListFollowers(requestCtx, req)
		if err != nil {
			return 0, nil, err
		}

		cursor = res.Cursor
		totalCount += len(res.Follows)
		for _, follow := range res.Follows {
			followers = append(followers, follow.FromUserId)
		}

		if len(res.Follows) == 0 {
			errMsg := "not enough followers to load"
			err := errors.New(errMsg)
			logrus.WithFields(logrus.Fields{
				"channelLogin": channelLogin,
				"max":          max,
			}).WithError(err).Info(errMsg)
			break
		}
	}

	return totalCount, followers, nil
}
