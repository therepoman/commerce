package follows_test

import (
	"errors"
	"fmt"
	"testing"

	followsrpc "code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	"code.justin.tv/commerce/fortuna/clients/follows"
	"code.justin.tv/commerce/fortuna/mocks"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	testUserID = "11111"
)

func TestSubscriptions(t *testing.T) {
	Convey("Test FollowsClient", t, func() {
		mockFollowsTwirp := new(mocks.TwitchVXFollowingServiceECS)
		followsClient := follows.FollowsClient{
			Client: mockFollowsTwirp,
		}

		Convey("Test ListFollowers", func() {
			Convey("with success", func() {
				res := &followsrpc.FollowsResp{
					Follows: generateFollows(5),
				}
				mockFollowsTwirp.On("ListFollowers", Anything, Anything).Return(res, nil)

				total, ids, err := followsClient.ListFollowers(testUserID, 2)

				So(err, ShouldBeNil)
				So(total, ShouldEqual, 5)
				So(total, ShouldEqual, len(ids))
				mockFollowsTwirp.AssertNumberOfCalls(t, "ListFollowers", 1)
			})

			Convey("with multiple calls goes past max value", func() {
				res := &followsrpc.FollowsResp{
					Follows: generateFollows(5),
				}
				mockFollowsTwirp.On("ListFollowers", Anything, Anything).Return(res, nil)

				total, ids, err := followsClient.ListFollowers(testUserID, 8)

				So(err, ShouldBeNil)
				So(total, ShouldEqual, 10)
				So(total, ShouldEqual, len(ids))
				mockFollowsTwirp.AssertNumberOfCalls(t, "ListFollowers", 2)
			})

			Convey("with error", func() {
				mockFollowsTwirp.On("ListFollowers", Anything, Anything).Return(nil, errors.New("woops"))
				_, _, err := followsClient.ListFollowers(testUserID, 4)

				So(err, ShouldNotBeNil)
			})
		})
	})
}

func generateFollows(count int) []*followsrpc.FollowResp {
	var follows []*followsrpc.FollowResp
	for i := 0; i < count; i++ {
		follow := &followsrpc.FollowResp{
			FromUserId: fmt.Sprintf("%d", i),
		}
		follows = append(follows, follow)
	}
	return follows
}
