package pubsub_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/fortuna/clients/pubsub"

	"code.justin.tv/commerce/fortuna/mocks"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	emoteSetId = "1"
	testUserId = "1"
)

func TestPubSub(t *testing.T) {
	Convey("TestPubSubClient", t, func() {
		mockPubClient := new(mocks.PubClient)

		pubClient := pubsub.PubSubClient{
			Client: mockPubClient,
		}

		Convey("Test Publish", func() {
			Convey("succeeds", func() {
				mockPubClient.On("Publish", Anything, Anything, Anything, Anything).Return(nil)

				err := pubClient.Publish(pubsub.ProgressType, pubsub.Progress{}, pubsub.CampaignEventsTopic, testUserId)
				So(err, ShouldBeNil)
			})
			Convey("with Publish fails", func() {
				mockPubClient.On("Publish", Anything, Anything, Anything, Anything).Return(errors.New("test error"))

				err := pubClient.Publish(pubsub.RewardType, pubsub.Reward{}, pubsub.UserCampaignEventsTopic, testUserId)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
