package pubsub

import (
	"context"
	"encoding/json"
	"fmt"

	pubClient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	UserCrateEventsTopic = "user-crate-events-v1.%s"
	GiftEventType        = "gift-event"

	CampaignEventsTopic     = "campaign-events.%s"
	UserCampaignEventsTopic = "user-campaign-events.%s"

	RewardType          = "reward"
	MegaCheerRewardType = "mega_cheer_reward"
	ProgressType        = "progress"
	ServiceName         = "pubsub"

	CheerbombEventsTopic = "channel-cheer-events-public-v1.%s"
	CheerbombEventType   = "cheerbomb"
)

// PubSub Structs
type Reward struct {
	TransactionID string    `json:"transactionID"`
	UserID        string    `json:"userID"`
	Benefactor    string    `json:"benefactor"`
	Domain        string    `json:"domain"`
	Contents      []Content `json:"contents"`
	ChannelID     string    `json:"channelID"`
	TriggerType   string    `json:"triggerType"`
}

type Content struct {
	Type     string `json:"type"`
	ID       string `json:"id"`
	Text     string `json:"text"`
	ImageURL string `json:"imageURL"`
	Quantity int32  `json:"quantity"`
}

type Progress struct {
	ObjectiveID string `json:"objectiveID"`
	Progress    int64  `json:"progress"`
}

type Entity struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type BenefactorReward struct {
	TransactionID string         `json:"transactionID"`
	UserID        string         `json:"userID"`
	Benefactor    string         `json:"benefactor"`
	Contents      []Content      `json:"contents"`
	TriggerType   string         `json:"triggerType"`
	TriggerAmount int32          `json:"triggerAmount"`
	Domain        string         `json:"domain"`
	NextMilestone *NextMilestone `json:"nextMilestone"`
	ChannelID     string         `json:"channelID"`
}

type NextMilestone struct {
	ID        string    `json:"id"`
	Amount    int32     `json:"amount"`
	Progress  int64     `json:"progress"`
	Threshold int64     `json:"threshold"`
	Contents  []Content `json:"contents"`
}

type Cheerbomb struct {
	UserID           string `json:"userID"`
	DisplayName      string `json:"displayName"`
	UserLogin        string `json:"userLogin"`
	SelectedCount    int    `json:"selectedCount"`
	TriggerType      string `json:"triggerType"`
	TriggerAmount    int32  `json:"triggerAmount"`
	TotalRewardCount int    `json:"totalRewardCount"`
	Domain           string `json:"domain"`
}

type PubSubClient struct {
	Client pubClient.PubClient
}

type IPubSubClient interface {
	Publish(pubsubEntityType string, data interface{}, topic string, topicSuffix string) error
}

func (p *PubSubClient) Publish(pubsubEntityType string, data interface{}, topic string, topicSuffix string) error {
	marshaled, err := json.Marshal(Entity{
		Type: pubsubEntityType,
		Data: data,
	})
	if err != nil {
		return errors.Wrap(err, "failed to marshal a pubsub message")
	}

	log.WithFields(log.Fields{
		"data": string(marshaled),
	}).Debug("PubSub: Sending Pubsub message")

	fullTopic := fmt.Sprintf(topic, topicSuffix)

	if err := p.Client.Publish(context.Background(), []string{fullTopic}, string(marshaled), nil); err != nil {
		return errors.Wrap(err, "failed to publish a pubsub message")
	}

	log.WithFields(log.Fields{
		"fullTopic": fullTopic,
		"data":      string(marshaled),
	}).Debug("PubSub message sent")

	return nil
}
