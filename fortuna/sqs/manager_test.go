package sqs

import (
	"sync"
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/workers"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const timeout = 1 * time.Second

func TestManager(t *testing.T) {
	Convey("Test manager", t, func() {
		stats, _ := statsd.NewNoopClient()
		mockClient := new(mocks.SQSAPI)
		mockStatter := new(mocks.Statter)
		mockFulfiller := new(mocks.IFulfiller)
		mockUtils := new(mocks.IUtils)

		// Mock Get Queue Url
		getQueueURLOutput := sqs.GetQueueUrlOutput{
			QueueUrl: aws.String("www.notreal.com"),
		}
		mockClient.On("GetQueueUrl", mock.Anything).Return(&getQueueURLOutput, nil)

		// Mock Get Queue Attributes
		getQueueAttributesOutput := sqs.GetQueueAttributesOutput{
			Attributes: map[string]*string{
				"MessageRetentionPeriod": aws.String("3"),
				"VisibilityTimeout":      aws.String("3"),
			},
		}
		mockClient.On("GetQueueAttributes", mock.Anything).Return(&getQueueAttributesOutput, nil)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		// Mock Receive Message Request
		mockClient.On("ReceiveMessageWithContext", mock.Anything, mock.Anything).Return(&sqs.ReceiveMessageOutput{}, nil)

		Convey("When shutting down correctly", func() {
			clients := &clients.Clients{}

			worker := &workers.MegaCheerGiftFulfillmentEventsWorker{
				Clients:   clients,
				Utils:     mockUtils,
				Stats:     mockStatter,
				Fulfiller: mockFulfiller,
			}

			manager := NewWorkerManager("queuename", 2, mockClient, worker, stats)

			So(manager.waitGroup, ShouldNotBeNil)

			err := manager.Shutdown(timeout)

			So(err, ShouldBeNil)
		})
	})
}

func TestWaitWithTimeout(t *testing.T) {
	Convey("Test waitWithTimeout", t, func() {
		Convey("When shutting down correctly", func() {
			var wg sync.WaitGroup
			wg.Add(2)
			go performSleep(100, &wg)
			go performSleep(100, &wg)

			timedout := waitWithTimeout(&wg, timeout)

			So(timedout, ShouldBeFalse)
		})

		Convey("When timing out", func() {
			var wg sync.WaitGroup
			wg.Add(2)
			go performSleep(1500, &wg)
			go performSleep(1500, &wg)

			timedout := waitWithTimeout(&wg, timeout)

			So(timedout, ShouldBeTrue)
		})
	})
}

func performSleep(millisecs time.Duration, wg *sync.WaitGroup) {
	duration := millisecs * time.Millisecond
	time.Sleep(duration)
	wg.Done()
}
