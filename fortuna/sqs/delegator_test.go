package sqs

import (
	"testing"

	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func TestDelegator(t *testing.T) {
	Convey("Test Delegator", t, func() {
		mockStatter := new(mocks.Statter)

		clients := &clients.Clients{}
		config := &config.Configuration{}

		// campaignConfig, err := api.InitTestCampaignConfig(api.DefaultCampaign)
		// So(err, ShouldBeNil)

		// mockEvent := new(mocks.CampaignEvent)

		triggerHandlers := map[string]handler.TriggerHandler{}
		mockUtils := new(mocks.IUtils)

		NewDelegator(clients, config, mockStatter, triggerHandlers, mockUtils)

		mockStatter.On("Inc", Anything, Anything, Anything).Return(nil)
		mockStatter.On("TimingDuration", Anything, Anything, Anything).Return(nil)
	})
}
