package handler

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	TriggerCheerEventHandler            string = "TriggerCheerHandler"
	TriggerSubscriptionEventHandler     string = "TriggerSubscriptionHandler"
	TriggerSubscriptionGiftEventHandler string = "TriggerSubscriptionGiftHandler"
)

// ObjectiveHandler is a special interface that all objective based campaign event handlers use
type ObjectiveHandler interface {
	Handle(event model.CampaignEvent, objective campaign.Objective) error
	GetObjectiveTrackID(objective *campaign.Objective, userID string) (string, error)
}

// TriggerHandler is a special interface that all trigger based campaign event handlers use
type TriggerHandler interface {
	Handle(event model.CampaignEvent, trigger campaign.Trigger) error
}

var EventHandlerStatsNameMap = map[string]string{
	TriggerCheerEventHandler:            "trigger",
	TriggerSubscriptionEventHandler:     "trigger_subscription",
	TriggerSubscriptionGiftEventHandler: "trigger_subscription_gift",
}

var TriggerTypeToHandlerMap = map[campaign.TriggerType]string{
	campaign.TriggerTypeCheer:            TriggerCheerEventHandler,
	campaign.TriggerTypeSubscription:     TriggerSubscriptionEventHandler,
	campaign.TriggerTypeSubscriptionGift: TriggerSubscriptionGiftEventHandler,
}

var ObjectiveHandlersMap map[string]ObjectiveHandler

type NewEventHandlersInput struct {
	Clients        *clients.Clients
	FortunaConfig  *config.Configuration
	CampaignConfig *campaign.Configuration
	Stats          statsd.Statter
	Fulfiller      rewards.IFulfiller
	Utils          rewards.IUtils
	Notifier       rewards.INotifier
}

func NewTriggerHandlers(input NewEventHandlersInput) map[string]TriggerHandler {
	return map[string]TriggerHandler{
		TriggerCheerEventHandler:            NewTriggerCheerHandler(input.Clients, input.Fulfiller, input.Notifier, input.FortunaConfig),
		TriggerSubscriptionEventHandler:     NewTriggerSubscriptionHandler(input.Clients, input.Fulfiller, input.Notifier, input.FortunaConfig),
		TriggerSubscriptionGiftEventHandler: NewTriggerSubscriptionGiftHandler(input.Clients, input.Fulfiller, input.Notifier, input.FortunaConfig),
	}
}
