package handler

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type TriggerSubscriptionGiftHandler struct {
	Clients             *clients.Clients
	MegaCommerceHandler IMegaCommerceHandler
}

func NewTriggerSubscriptionGiftHandler(clients *clients.Clients, fulfiller rewards.IFulfiller, notifier rewards.INotifier, serviceConfig *config.Configuration) TriggerHandler {
	return &TriggerSubscriptionGiftHandler{
		Clients: clients,
		MegaCommerceHandler: &MegaCommerceHandler{
			Clients:        clients,
			Fulfiller:      fulfiller,
			Notifier:       notifier,
			ChatterChooser: chatterchooser.NewRandomChatterChooser(),
			Rng:            rand.New(rand.NewSource(time.Now().UTC().UnixNano())),

			TriggerStateMachineARN: serviceConfig.CheerbombTriggerStepFn.StateMachineARN,
		},
	}
}

func (t *TriggerSubscriptionGiftHandler) Handle(campaignEvent model.CampaignEvent, trigger campaign.Trigger) error {
	if trigger.TriggerType != campaign.TriggerTypeSubscriptionGift {
		log.WithField("eventID", campaignEvent.GetEventID()).Debug("TriggerSubscriptionGiftHandler ignoring non subgift trigger")
		return nil
	}
	event, ok, err := model.ToSubscriptionGiftEvent(campaignEvent)
	if err != nil {
		return errors.Wrap(err, "Failed to convert to subgift event from campaign event")
	}
	if !ok {
		log.WithFields(log.Fields{
			"userID":    event.SenderID,
			"triggerID": trigger.ID,
			"event":     event,
		}).Debug("TriggerSubscriptionGiftHandler ignoring non-subgift event")
		return nil
	}

	return t.MegaCommerceHandler.HandleEvent(event.ToMegaCommerceEvent(), trigger)
}
