package handler_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestTriggerSubscriptionGiftHandler(t *testing.T) {
	Convey("Test TriggerSubscriptionGiftHandler", t, func() {
		mockMegaHandler := new(mocks.IMegaCommerceHandler)
		mockSubscriptions := new(mocks.ISubscriptionsClient)
		mockClients := &clients.Clients{
			SubscriptionsClient: mockSubscriptions,
		}
		triggerSubgiftHandler := handler.TriggerSubscriptionGiftHandler{
			Clients:             mockClients,
			MegaCommerceHandler: mockMegaHandler,
		}
		testTrigger := campaign.Trigger{
			TriggerType: campaign.TriggerTypeSubscriptionGift,
		}
		testEvent := model.SubscriptionGiftEvent{}

		Convey("happy case", func() {
			mockMegaHandler.On("HandleEvent", mock.Anything, testTrigger).Return(nil)

			err := triggerSubgiftHandler.Handle(testEvent, testTrigger)
			So(err, ShouldBeNil)
		})

		Convey("ignores invalid trigger type", func() {
			testTrigger.TriggerType = campaign.TriggerTypeCheer

			err := triggerSubgiftHandler.Handle(testEvent, testTrigger)
			So(err, ShouldBeNil)
			mockMegaHandler.AssertNotCalled(t, "HandleEvent")
		})

		Convey("ignores invalid event", func() {
			err := triggerSubgiftHandler.Handle(model.CheerEvent{}, testTrigger)
			So(err, ShouldBeNil)
			mockMegaHandler.AssertNotCalled(t, "HandleEvent")
		})

		Convey("on MegaCommerce error", func() {
			mockMegaHandler.On("HandleEvent", mock.Anything, testTrigger).Return(errors.New("woops"))

			err := triggerSubgiftHandler.Handle(testEvent, testTrigger)
			So(err, ShouldNotBeNil)
		})
	})
}
