package handler_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestTriggerCheerHandler(t *testing.T) {
	Convey("Test TriggerCheerHandler", t, func() {
		mockMegaHandler := new(mocks.IMegaCommerceHandler)

		triggerCheerHandler := handler.TriggerCheerHandler{
			MegaCommerceHandler: mockMegaHandler,
		}
		testTrigger := campaign.Trigger{
			TriggerType: campaign.TriggerTypeCheer,
		}
		testEvent := model.CheerEvent{
			ChannelID:         "test-channel-id",
			UserID:            "test-user-id",
			BitsTransactionID: "test-event-id",
			Message:           "i love to game",
			Bits:              500,
		}
		Convey("happy case", func() {
			mockMegaHandler.On("HandleEvent", mock.Anything, testTrigger).Return(nil)

			err := triggerCheerHandler.Handle(testEvent, testTrigger)
			So(err, ShouldBeNil)
		})

		Convey("ignores invalid trigger type", func() {
			testTrigger.TriggerType = campaign.TriggerTypeSubscription

			err := triggerCheerHandler.Handle(testEvent, testTrigger)
			So(err, ShouldBeNil)
		})

		Convey("ignores invalid event", func() {
			err := triggerCheerHandler.Handle(model.SubscriptionEvent{}, testTrigger)
			So(err, ShouldBeNil)
		})

		Convey("on MegaCommerce error", func() {
			mockMegaHandler.On("HandleEvent", mock.Anything, testTrigger).Return(errors.New("woops"))

			err := triggerCheerHandler.Handle(testEvent, testTrigger)
			So(err, ShouldNotBeNil)
		})
	})
}
