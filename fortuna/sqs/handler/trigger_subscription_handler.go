package handler

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type TriggerSubscriptionHandler struct {
	MegaCommerceHandler IMegaCommerceHandler
}

func NewTriggerSubscriptionHandler(clients *clients.Clients, fulfiller rewards.IFulfiller, notifier rewards.INotifier, serviceConfig *config.Configuration) TriggerHandler {
	return &TriggerSubscriptionHandler{
		MegaCommerceHandler: &MegaCommerceHandler{
			Clients:        clients,
			Fulfiller:      fulfiller,
			Notifier:       notifier,
			ChatterChooser: chatterchooser.NewRandomChatterChooser(),
			Rng:            rand.New(rand.NewSource(time.Now().UTC().UnixNano())),

			TriggerStateMachineARN: serviceConfig.CheerbombTriggerStepFn.StateMachineARN,
		},
	}
}

func (t *TriggerSubscriptionHandler) Handle(campaignEvent model.CampaignEvent, trigger campaign.Trigger) error {
	if trigger.TriggerType != campaign.TriggerTypeSubscription {
		log.WithField("eventID", campaignEvent.GetEventID()).Debug("TriggerSubscriptionHandler ignoring non subscription trigger")
		return nil
	}
	event, ok, err := model.ToSubscriptionEvent(campaignEvent)
	if err != nil {
		return errors.Wrap(err, "Failed to convert to subscription event from campaign event")
	}

	if !ok {
		log.WithFields(log.Fields{
			"userID":    event.UserID,
			"triggerID": trigger.ID,
			"event":     event,
		}).Debug("TriggerSubscriptionHandler ignoring non-sub event")
		return nil
	}

	return t.MegaCommerceHandler.HandleEvent(event.ToMegaCommerceEvent(), trigger)
}
