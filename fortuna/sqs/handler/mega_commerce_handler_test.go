package handler_test

import (
	"math/rand"
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMegaCommerceHandler(t *testing.T) {
	Convey("Test TestMegaCommerceHandler", t, func() {
		rng := rand.New(rand.NewSource(1337))

		startDate := time.Date(2018, time.January, 1, 0, 0, 0, 0, time.UTC)
		endDate := time.Date(3000, time.January, 1, 0, 0, 0, 0, time.UTC)

		mockFulfiller := new(mocks.IFulfiller)
		mockNotifier := new(mocks.INotifier)
		mockStepfnClient := new(mocks.IStepfnClient)
		mockUsersClient := new(mocks.IUsersClient)
		mockClients := &clients.Clients{
			StepfnClient: mockStepfnClient,
			UsersClient:  mockUsersClient,
		}

		triggerMegaCommerceHandler := handler.MegaCommerceHandler{
			Clients:   mockClients,
			Fulfiller: mockFulfiller,
			Notifier:  mockNotifier,
			ChatterChooser: &chatterchooser.RandomChatterChooser{
				Rng: rng,
			},
			Rng: rng,

			TriggerStateMachineARN: "test-state-machine-arn",
		}

		testUserID := "test-user-id"

		testEvent := model.MegaCommerceEvent{
			ChannelID: "test-channel-id",
			UserID:    testUserID,
			EventID:   "test-event-id",
			Message:   "i love to game",
			Amount:    500,
		}

		testEventLowAmount := model.MegaCommerceEvent{
			ChannelID: "test-channel-id",
			UserID:    testUserID,
			EventID:   "test-event-id",
			Message:   "i love to game",
			Amount:    100,
		}

		testTriggerID := "t1"

		benefactorReward := &campaign.Reward{
			ID:        "benefactor-reward-id",
			TriggerID: testTriggerID,
			Type:      "igc",
			ImageURL:  "cool-image-url-1"}
		recipientReward := &campaign.Reward{
			ID:        "recipient-reward-id",
			TriggerID: testTriggerID,
			Type:      "igc",
			ImageURL:  "cool-image-url-2"}

		testTrigger := campaign.Trigger{
			ID: testTriggerID,
			BenefactorRewardAttributes: campaign.RewardAttributes{
				Rewards: []*campaign.Reward{benefactorReward},
			},
			RecipientRewardAttributes: campaign.RewardAttributes{
				Rewards: []*campaign.Reward{recipientReward},
			},
			ActiveStartDate:           startDate,
			ActiveEndDate:             endDate,
			TriggerType:               campaign.TriggerTypeCheer,
			TriggerAmountMax:          600,
			TriggerAmountMin:          300,
			RecipientSelectionFormula: "formula",
			Domain:                    "d",
		}

		Convey("Test HandleEvent", func() {
			Convey("happy case", func() {
				mockStepfnClient.On("Execute", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockFulfiller.On("EntitleTrigger", mock.Anything, testUserID, true, 0).Return(testTrigger.BenefactorRewardAttributes.Rewards, nil, nil)

				err := triggerMegaCommerceHandler.HandleEvent(testEvent, testTrigger)

				So(err, ShouldBeNil)
				mockStepfnClient.AssertCalled(
					t,
					"Execute",
					triggerMegaCommerceHandler.TriggerStateMachineARN,
					"trigger.test-event-id",
					handler.TriggerMegaCommerceExecutionInput{
						Event:                     testEvent,
						TriggerID:                 testTriggerID,
						BenefactorRewardIDs:       []string{"benefactor-reward-id"},
						RecipientSelectionFormula: "formula",
						Domain:                    "d",
					})
				mockFulfiller.AssertCalled(t, "EntitleTrigger", &testTrigger, testUserID, true, 0)
				mockNotifier.AssertNotCalled(t, "SendMegaBenefactorPubSub")
			})

			Convey("with amount not within range", func() {
				err := triggerMegaCommerceHandler.HandleEvent(testEventLowAmount, testTrigger)
				So(err, ShouldBeNil)
				mockStepfnClient.AssertNotCalled(t, "Execute", 0)
				mockNotifier.AssertNotCalled(t, "SendMegaBenefactorPubSub")
				mockFulfiller.AssertNotCalled(t, "EntitleTrigger")
			})

			Convey("with stepfn failure", func() {
				mockStepfnClient.On("Execute", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("failed"))
				mockFulfiller.On("EntitleTrigger", mock.Anything, testUserID, true, 0).Return(testTrigger.BenefactorRewardAttributes.Rewards, nil, nil)
				mockNotifier.On("SendMegaBenefactorPubSub", mock.Anything).Return(nil)
				err := triggerMegaCommerceHandler.HandleEvent(testEvent, testTrigger)
				So(err, ShouldBeNil)

				mockStepfnClient.AssertNotCalled(t, "Execute", 0)
				mockNotifier.AssertNumberOfCalls(t, "SendMegaBenefactorPubSub", 1)
			})

			Convey("with whitelisting", func() {
				testTrigger.UserWhitelistEnabled = true
				testTrigger.UserWhitelist = map[string]bool{
					testUserID: true,
				}

				Convey("with whitelisted cheerer", func() {
					mockStepfnClient.On("Execute", mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockNotifier.On("SendMegaBenefactorPubSub", mock.Anything).Return(nil)
					mockFulfiller.On("EntitleTrigger", mock.Anything, testUserID, true, 0).Return(testTrigger.BenefactorRewardAttributes.Rewards, nil, nil)
					err := triggerMegaCommerceHandler.HandleEvent(testEvent, testTrigger)

					So(err, ShouldBeNil)
					mockStepfnClient.AssertNumberOfCalls(t, "Execute", 1)
				})

				Convey("with non-whitelisted cheerer", func() {
					testEvent.UserID = "eh"
					err := triggerMegaCommerceHandler.HandleEvent(testEvent, testTrigger)

					So(err, ShouldBeNil)
					mockStepfnClient.AssertNotCalled(t, "Execute")
					mockFulfiller.AssertNotCalled(t, "EntitleTrigger")
				})
			})

			Convey("with staff only", func() {
				testTrigger.StaffOnly = true

				Convey("with staff cheerer", func() {
					mockStepfnClient.On("Execute", mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockNotifier.On("SendMegaBenefactorPubSub", mock.Anything).Return(nil)
					mockFulfiller.On("EntitleTrigger", mock.Anything, testUserID, true, 0).Return(testTrigger.BenefactorRewardAttributes.Rewards, nil, nil)
					mockUsersClient.On("IsUserStaff", testUserID).Return(true, nil)
					err := triggerMegaCommerceHandler.HandleEvent(testEvent, testTrigger)

					So(err, ShouldBeNil)
					mockStepfnClient.AssertNumberOfCalls(t, "Execute", 1)
				})

				Convey("with non-staff cheerer", func() {
					testEvent.UserID = "no"
					mockUsersClient.On("IsUserStaff", "no").Return(false, nil)
					err := triggerMegaCommerceHandler.HandleEvent(testEvent, testTrigger)

					So(err, ShouldBeNil)
					mockStepfnClient.AssertNotCalled(t, "Execute")
					mockFulfiller.AssertNotCalled(t, "EntitleTrigger")
				})
			})
		})
	})
}
