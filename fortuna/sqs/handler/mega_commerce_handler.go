package handler

import (
	"math/rand"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type IMegaCommerceHandler interface {
	HandleEvent(event model.MegaCommerceEvent, trigger campaign.Trigger) error
}

type MegaCommerceHandler struct {
	Clients        *clients.Clients
	Fulfiller      rewards.IFulfiller
	Notifier       rewards.INotifier
	ChatterChooser chatterchooser.ChatterChooser
	Rng            *rand.Rand

	TriggerStateMachineARN string
}

type TriggerMegaCommerceExecutionInput struct {
	Event                     model.MegaCommerceEvent `json:"Event"`
	TriggerID                 string                  `json:"TriggerID"`
	BenefactorRewardIDs       []string                `json:"BenefactorRewardIDs"`
	RecipientSelectionFormula string                  `json:"RecipientSelectionFormula"`
	Domain                    string                  `json:"Domain"`
}

func (t *MegaCommerceHandler) HandleEvent(event model.MegaCommerceEvent, trigger campaign.Trigger) error {
	if !trigger.IsUserWhitelisted(event.UserID) {
		log.WithFields(log.Fields{
			"userID":    event.UserID,
			"triggerID": trigger.ID,
			"event":     event,
		}).Debug("TriggerMegaCommerceHandler ignoring non-whitelisted user")
		return nil
	}

	if trigger.StaffOnly {
		isStaff, err := t.Clients.UsersClient.IsUserStaff(event.UserID)
		if err != nil {
			log.WithFields(log.Fields{
				"userID":    event.UserID,
				"triggerID": trigger.ID,
				"event":     event,
			}).WithError(err).Error("Received error from Users client while retrieving staff status")
			return err
		}
		if !isStaff {
			log.WithFields(log.Fields{
				"userID":    event.UserID,
				"triggerID": trigger.ID,
				"event":     event,
			}).Debug("Trigger not visible to user")
			return nil
		}
	}

	if !triggerFitsAmount(int(event.Amount), trigger) {
		log.WithFields(log.Fields{
			"event":       event,
			"triggerID":   trigger.ID,
			"triggerMin":  trigger.TriggerAmountMin,
			"triggerMax":  trigger.TriggerAmountMax,
			"cheerAmount": event.Amount,
		}).Debugf("TriggerMegaCommerceHandler ignoring cheer event with amount not within range.")
		return nil
	}

	// Execute the trigger
	log.WithFields(log.Fields{
		"userID":      event.UserID,
		"triggerID":   trigger.ID,
		"triggerType": trigger.TriggerType,
		"event":       event,
	}).Info("TriggerCheerHandler handling event")

	claimableRewards, err := t.grantBenefactorRewards(trigger, event, event.UserID)
	if err != nil {
		return errors.Wrap(err, "Trigger failed to handle benefactor rewards")
	}

	var claimableBenefectorRewardIDs = []string{}
	for _, claimableReward := range claimableRewards {
		claimableBenefectorRewardIDs = append(claimableBenefectorRewardIDs, claimableReward.ID)
	}

	if err := t.startStepFn(event, trigger, claimableBenefectorRewardIDs); err != nil {
		log.WithFields(log.Fields{
			"userID":    event.UserID,
			"triggerID": trigger.ID,
			"event":     event,
		}).WithError(err).Error("TriggerMegaCommerceHandler failed to execute trigger gift fulfillment")

		// If the step fn failed, we still want to send a pubsub to the benefactor
		if len(claimableRewards) > 0 {
			// Send pubsub message to the benefactor if anything is granted for that user
			if err := t.Notifier.SendMegaBenefactorPubSub(
				rewards.BenefactorPubSubInput{
					TransactionID:   event.EventID,
					BenefactorID:    event.UserID,
					TriggerType:     string(trigger.TriggerType),
					TriggerAmount:   event.Amount,
					ChannelID:       event.ChannelID,
					NumGiftsGiven:   0,
					RewardsToNotify: claimableRewards,
					Domain:          trigger.Domain}); err != nil {
				log.WithFields(log.Fields{
					"benefactorID": event.UserID,
					"channelID":    event.ChannelID,
					"event":        event,
					"triggerID":    trigger.ID,
				}).WithError(err).Error("TriggerMegaCommerceHandler failed to publish benefactor reward PubSub notification")
			}
		}
	}

	return nil
}

func (t *MegaCommerceHandler) startStepFn(event model.MegaCommerceEvent, trigger campaign.Trigger, grantedBenefectorRewardIDs []string) error {
	// Execution name acts as idempotence key for state machine execution
	stepfnExecutionName := "trigger." + event.EventID

	stepfnExecutionInput := TriggerMegaCommerceExecutionInput{
		Event:                     event,
		TriggerID:                 trigger.ID,
		BenefactorRewardIDs:       grantedBenefectorRewardIDs,
		RecipientSelectionFormula: trigger.RecipientSelectionFormula,
		Domain:                    trigger.Domain,
	}

	return t.Clients.StepfnClient.Execute(t.TriggerStateMachineARN, stepfnExecutionName, stepfnExecutionInput)
}

func (t *MegaCommerceHandler) grantBenefactorRewards(trigger campaign.Trigger, event model.MegaCommerceEvent, userID string) ([]*campaign.Reward, error) {
	log.WithFields(log.Fields{
		"userID":    userID,
		"triggerID": trigger.ID,
	}).Info("TriggerMegaCommerceHandler granting benefactor rewards")

	scaledSelectionAmount := int(event.GetScaledBenefactorAmount()) // Only used for scaled selection counts
	claimableRewards, _, err := t.Fulfiller.EntitleTrigger(&trigger, userID, true, scaledSelectionAmount)

	if err != nil {
		return nil, errors.Wrap(err, "Failed to entitle trigger")
	}

	return claimableRewards, nil
}

func triggerFitsAmount(amount int, trigger campaign.Trigger) bool {
	min, max := trigger.TriggerAmountMin, trigger.TriggerAmountMax

	return amount >= min && (max <= 0 || amount <= max)
}
