package handler

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/chatterchooser"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type TriggerCheerHandler struct {
	MegaCommerceHandler IMegaCommerceHandler
}

func NewTriggerCheerHandler(clients *clients.Clients, fulfiller rewards.IFulfiller, notifier rewards.INotifier, serviceConfig *config.Configuration) TriggerHandler {
	return &TriggerCheerHandler{
		MegaCommerceHandler: &MegaCommerceHandler{
			Clients:        clients,
			Fulfiller:      fulfiller,
			Notifier:       notifier,
			ChatterChooser: chatterchooser.NewRandomChatterChooser(),
			Rng:            rand.New(rand.NewSource(time.Now().UTC().UnixNano())),

			TriggerStateMachineARN: serviceConfig.CheerbombTriggerStepFn.StateMachineARN,
		},
	}
}

func (t *TriggerCheerHandler) Handle(campaignEvent model.CampaignEvent, trigger campaign.Trigger) error {
	if trigger.TriggerType != campaign.TriggerTypeCheer {
		log.WithField("eventID", campaignEvent.GetEventID()).Debug("TriggerCheerHandler ignoring non cheer trigger")
		return nil
	}
	event, ok, err := model.ToCheerEvent(campaignEvent)
	if err != nil {
		return errors.Wrap(err, "Failed to convert to bits event from campaign event")
	}

	if !ok {
		log.WithFields(log.Fields{
			"userID":    event.UserID,
			"triggerID": trigger.ID,
			"event":     event,
		}).Debug("TriggerCheerHandler ignoring non-bits event")
		return nil
	}

	return t.MegaCommerceHandler.HandleEvent(event.ToMegaCommerceEvent(), trigger)
}
