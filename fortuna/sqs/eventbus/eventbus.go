package eventbus

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	goSpade "code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/sqsclient"
	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
)

type Worker interface {
	Register(mux *eventbus.Mux)
}

type Manager struct {
	Config    *config.Configuration
	Workers   []Worker
	BusClient *sqsclient.SQSClient
	Mux       *eventbus.Mux
	MyURL     string
}

func NewManager(cfg *config.Configuration, stats statsd.Statter, processor model.Processor) Manager {
	mux := eventbus.NewMux()
	workers := []Worker{
		NewUserSubscribeUserNoticeWorker(processor, stats),
		NewUserGiftSubscriptionWorker(processor, stats),
		NewCheerWorker(processor, stats),
	}
	for _, worker := range workers {
		worker.Register(mux)
	}

	return Manager{
		Config:  cfg,
		Mux:     mux,
		Workers: workers,
		MyURL:   cfg.EventBusURL,
	}
}

func New2FAManager(cfg *config.Configuration, fulfiller rewards.IFulfiller, spade goSpade.Client, dao dynamo.IRewardDAO, receiver dart.Receiver) Manager {
	mux := eventbus.NewMux()
	workers := []Worker{
		NewTwoFactorWorker(fulfiller, spade, dao, receiver),
	}
	for _, worker := range workers {
		worker.Register(mux)
	}

	return Manager{
		Config:  cfg,
		Mux:     mux,
		Workers: workers,
		MyURL:   cfg.TwoFactorEventBusQueueURL,
	}
}

func (m *Manager) Listen() error {
	if m.BusClient != nil {
		return nil
	}
	sess, err := session.NewSession(aws.NewConfig().WithRegion(m.Config.AWSRegion))
	if err != nil {
		return errors.Wrap(err, "could not create eventbus session")
	}

	client, err := sqsclient.New(sqsclient.Config{
		Session:    sess,
		Dispatcher: m.Mux.Dispatcher(),
		QueueURL:   m.MyURL,
	})
	if err != nil {
		return errors.Wrap(err, "could not create eventbus client")
	}

	m.BusClient = client
	return nil
}

func (m *Manager) Shutdown() {
	if m.BusClient == nil {
		return
	}
	m.BusClient.Shutdown()
	m.BusClient = nil
}
