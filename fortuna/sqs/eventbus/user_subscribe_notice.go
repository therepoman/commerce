package eventbus

import (
	"context"

	"code.justin.tv/commerce/fortuna/sqs/model"
	eventbus "code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/user_subscribe_user_notice"
	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

type UserSubscribeUserNoticeWorker struct {
	Processor model.Processor
	Stats     statsd.Statter
}

func NewUserSubscribeUserNoticeWorker(processor model.Processor, stats statsd.Statter) *UserSubscribeUserNoticeWorker {
	return &UserSubscribeUserNoticeWorker{
		Processor: processor,
		Stats:     stats,
	}
}

func (s *UserSubscribeUserNoticeWorker) Handle(ctx context.Context, h *eventbus.Header, event *schema.Create) error {
	if event.SubscriptionInfo != nil && event.SubscriptionInfo.CreatedBy == "gift" {
		log.WithField("event", event).Debug("ignore gift-related subscription event")
		return nil
	}
	if event.GetUserNotice() == nil {
		log.WithField("event", event).Debug("ignore subscription event without user notice")
		return nil
	}
	var tier *schema.Tier

	switch event.GetUserNotice().(type) {
	case *schema.UserSubscribeUserNoticeCreate_Subscription:
		tier = event.GetSubscription().GetTier()
	case *schema.UserSubscribeUserNoticeCreate_Resubscription:
		tier = event.GetResubscription().GetTier()
	case *schema.UserSubscribeUserNoticeCreate_TierUpgrade:
		tier = event.GetTierUpgrade().GetNewTier()
	case *schema.UserSubscribeUserNoticeCreate_PrimeUpgrade:
		tier = event.GetPrimeUpgrade().GetPaidTier()
	case *schema.UserSubscribeUserNoticeCreate_GiftUpgrade:
		tier = event.GetGiftUpgrade().GetPaidTier()
	case *schema.UserSubscribeUserNoticeCreate_PayItForward:
		tier = event.GetPayItForward().GetTier()
	case *schema.UserSubscribeUserNoticeCreate_ExtendSubscription:
		tier = event.GetExtendSubscription().GetTier()
	default:
		log.WithField("event", event).Debug("ignore subscription event with unsupported user notice")
		return nil
	}

	var productID string
	if event.SubscriptionInfo != nil {
		productID = event.SubscriptionInfo.ProductId
	}

	if tier == nil {
		log.WithField("event", event).Info("ignore subscription event without subscription user notice tier information")
		return nil
	}
	log.WithField("event", event).Info("processing user subscribe user notice event")
	subEvent := model.SubscriptionEvent{
		UserID:    event.FromUserId,
		ChannelID: event.ToUserId,
		Tier:      ConvertNumeralTierToString(tier.String()),
		ProductID: productID,
		MessageID: h.MessageID.String(),
	}
	return s.Processor.Process(subEvent)
}

func (s *UserSubscribeUserNoticeWorker) Register(mux *eventbus.Mux) {
	schema.RegisterUserSubscribeUserNoticeCreateHandler(mux, s.Handle)
}
