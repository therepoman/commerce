package eventbus

import (
	"context"
	"testing"

	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/model"
	eventbus "code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/user_subscribe_user_notice"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserSubscribeUserNoticeWorker(t *testing.T) {
	Convey("Test UserSubscribeUserNoticeWorker", t, func() {
		mockProcessor := new(mocks.Processor)
		mockStatter := new(mocks.Statter)

		ctx := context.Background()
		header := &eventbus.Header{}
		worker := NewUserSubscribeUserNoticeWorker(mockProcessor, mockStatter)
		event := schema.UserSubscribeUserNoticeCreate{
			FromUserId: "user_id",
			ToUserId:   "channel_id",
			SubscriptionInfo: &schema.Subscription{
				Id:        "subscription_id",
				ProductId: "product_id",
				CreatedBy: "purchase",
			},
			UserNotice: &schema.UserSubscribeUserNoticeCreate_Subscription{
				Subscription: &schema.SubscriptionUserNotice{
					Tier: &schema.Tier{
						Value: &schema.Tier_Numeral{
							Numeral: schema.SubTier_SUB_TIER_1,
						},
					},
				},
			},
		}
		expectedEvent := model.SubscriptionEvent{
			UserID:    event.FromUserId,
			ChannelID: event.ToUserId,
			Tier:      "1000",
			ProductID: "product_id",
			MessageID: header.MessageID.String(),
		}

		Convey("Test Handle", func() {
			mockProcessor.On("Process", mock.Anything).Return(nil)
			err := worker.Handle(ctx, header, &event)
			So(err, ShouldBeNil)
			mockProcessor.AssertCalled(t, "Process", expectedEvent)
		})

		Convey("Test nil subscriptionInfo", func() {
			mockProcessor.On("Process", mock.Anything).Return(nil)
			event.SubscriptionInfo = nil
			expectedEvent.ProductID = ""

			err := worker.Handle(ctx, header, &event)
			So(err, ShouldBeNil)
			mockProcessor.AssertCalled(t, "Process", expectedEvent)
		})

		Convey("Test unsupported userNotice", func() {
			event.UserNotice = &schema.UserSubscribeUserNoticeCreate_GiftedSubscription{}

			err := worker.Handle(ctx, header, &event)
			So(err, ShouldBeNil)
		})

		Convey("Test resub userNotice", func() {
			mockProcessor.On("Process", mock.Anything).Return(nil)
			event.UserNotice = &schema.UserSubscribeUserNoticeCreate_Resubscription{
				Resubscription: &schema.SubscriptionUserNotice{
					Tier: &schema.Tier{
						Value: &schema.Tier_Numeral{
							Numeral: schema.SubTier_SUB_TIER_3,
						},
					},
				},
			}
			expectedEvent.Tier = "3000"
			err := worker.Handle(ctx, header, &event)
			So(err, ShouldBeNil)
			mockProcessor.AssertCalled(t, "Process", expectedEvent)
		})

		Convey("Test process error", func() {
			mockProcessor.On("Process", mock.Anything).Return(errors.New("woops"))
			err := worker.Handle(ctx, header, &event)
			So(err, ShouldNotBeNil)
		})
	})
}
