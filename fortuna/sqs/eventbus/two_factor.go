package eventbus

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients/mako"
	"code.justin.tv/commerce/fortuna/clients/spade"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/rewards"
	goSpade "code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"
	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	log "github.com/sirupsen/logrus"
)

type TwoFactorWorker struct {
	Fulfiller    rewards.IFulfiller
	SpadeClient  goSpade.Client
	RewardDAO    dynamo.IRewardDAO
	DartReceiver dart.Receiver
}

////////////////////////////////////////////////
// these will be removed after the backfill
const EntitlementOnly = "EntitlementOnly"
const NotificationOnly = "NotificationOnly"
const ManualUnentitle = "ManualUnentitle"

////////////////////////////////////////////////

const two_factor_reward_id = "300374282"
const two_factor_emote_domain = "2FA"
const qaTwPartnerId = "139075904"
const dartNotificationTypeId = "two_factor_emote_rewards"
const rewardId = "2FA_EMOTE_REWARDS"
const removeRewardId = "2FA_EMOTE_REWARDS_REMOVED"
const two_factor_data_science_event_name = "two_factor_reward_entitlement"
const tenMinutesAgo = -time.Minute * 10

func NewTwoFactorWorker(fulfiller rewards.IFulfiller, spade goSpade.Client, dao dynamo.IRewardDAO, receiver dart.Receiver) *TwoFactorWorker {
	return &TwoFactorWorker{
		Fulfiller:    fulfiller,
		SpadeClient:  spade,
		RewardDAO:    dao,
		DartReceiver: receiver,
	}
}

func (s *TwoFactorWorker) Handle(ctx context.Context, h *eventbus.Header, event *schema.Update) error {
	// for the backfill we're using this header to pass in an eventType if we need to do something outside the normal path
	if h == nil {
		h = &eventbus.Header{}
	}
	if event.AnyEnabled != nil {
		if h.EventType != ManualUnentitle && event.AnyEnabled.Value {
			if h.EventType != NotificationOnly {
				err := s.runMakoRequest(ctx, event.UserId, true)
				if err != nil {
					return err
				}
			}

			if h.EventType == EntitlementOnly {
				log.Infof("Notification sent for user: %s", event.UserId)
				return nil
			}
			metadata := make(map[string]string)
			metadata["user_id"] = event.UserId

			_, err := s.DartReceiver.PublishNotification(ctx, &dart.PublishNotificationRequest{
				Recipient:                      &dart.PublishNotificationRequest_RecipientId{RecipientId: event.UserId},
				NotificationType:               dartNotificationTypeId,
				Metadata:                       metadata,
				DropNotificationBeforeDelivery: false,
			})

			if err != nil {
				log.WithError(err).Error("Error calling Dart for 2FA notification")
			}

			// adds an entry to the admin audit tables
			_, err = s.RewardDAO.UpdateReward(&dynamo.Reward{
				UserID:              event.UserId,
				Quantity:            1,
				CreatedAt:           time.Now(),
				TriggerID:           rewardId,
				Domain:              two_factor_emote_domain,
				MilestoneID:         rewardId,
				BatchID:             rewardId,
				FulfillmentStatus:   "DONE",
				FulfillmentStrategy: "EVENT_BUS_QUEUE",
				MilestoneType:       rewardId,
				RewardID:            rewardId,
				RewardType:          "emote",
				ScopedRewardID:      rewardId,
				ExternalUserID:      "none",
			})

			if err != nil {
				log.WithError(err).Warnf("Error attempting to write admin audit for 2FA Entitlement for user: %s", event.UserId)
			}
		} else {
			err := s.runMakoRequest(ctx, event.UserId, false)
			if err != nil {
				return err
			}
			// updates an entry to the admin audit table that indicates this has been removed
			_, err = s.RewardDAO.UpdateReward(&dynamo.Reward{
				UserID:              event.UserId,
				Quantity:            1,
				CreatedAt:           time.Now(),
				TriggerID:           removeRewardId,
				Domain:              two_factor_emote_domain,
				MilestoneID:         removeRewardId,
				BatchID:             removeRewardId,
				FulfillmentStatus:   "REMOVED",
				FulfillmentStrategy: "EVENT_BUS_QUEUE",
				MilestoneType:       removeRewardId,
				RewardID:            removeRewardId,
				RewardType:          "emote",
				ScopedRewardID:      rewardId,
				ExternalUserID:      "none",
			})
			if err != nil {
				log.WithError(err).Warnf("Error attempting to write admin audit for 2FA Unentitlement for user: %s", event.UserId)
			}
		}
	}
	return nil
}

func (s *TwoFactorWorker) runMakoRequest(ctx context.Context, userId string, enabled bool) error {
	var startsAt, endsAt time.Time
	// Fulfiller checks for startsAt.IsZero() and endsAt.IsZero() and passes nil pointers if it is zero.
	// Mako's direct processor assumes that if endsAt is nil, the end time is infinite
	// https://git.xarth.tv/commerce/mako/blob/5bad3091b3917652c60baff83ea984e5fe2db479/events/direct_processor.go#L84
	// Mako will update or insert any input in place, so this should result in a removal of the entitlement as expected.
	// https://code.amazon.com/packages/Materia/blobs/098ea34cf461cd51e16e1aa8d9e34b7a69513373/--/internal/dynamodb/entitlements.go#L283
	if enabled {
		startsAt = time.Now()
	} else {
		// set the duration start to ten minutes ago and the ends at to now so that this emote updates to expired.
		startsAt = time.Now().Add(tenMinutesAgo)
		endsAt = time.Now()
	}

	input := rewards.MakoRewardInput{
		UserID:    userId,
		StartsAt:  startsAt,
		EndsAt:    endsAt,
		MakoScope: fmt.Sprintf(mako.MakoEmoteScopeFormat, two_factor_emote_domain),
		Reward: &campaign.Reward{
			ID:          two_factor_reward_id,
			MilestoneID: two_factor_emote_domain,
		},
		GroupID:      two_factor_reward_id,
		Domain:       two_factor_emote_domain,
		SpadeContext: "2FA.EmoteReward",
		SpadeType:    spade.TypeEmote,
		OwnerID:      qaTwPartnerId,
	}

	err := s.Fulfiller.ClaimMakoReward(input)

	if err != nil {
		log.WithError(err).Errorf("Error attempting to claim Mako reward for 2FA, user: %s, startsAt: %s, endsAt: %s", userId, startsAt.String(), endsAt.String())
		return err
	}

	go func() {
		err = s.SpadeClient.TrackEvent(ctx, two_factor_data_science_event_name, spade.TwoFactorEmoteEntitlementEvent{
			Time:     time.Now().Unix(),
			Entitled: enabled,
			UserID:   userId,
		})

		if err != nil {
			log.WithError(err).Warnf("Error attempting to send spade event for 2FA Entitlement for user: %s", userId)
		}
	}()

	return nil
}

func (s *TwoFactorWorker) Register(mux *eventbus.Mux) {
	schema.RegisterUserMultiFactorAuthUpdateHandler(mux, s.Handle)
}
