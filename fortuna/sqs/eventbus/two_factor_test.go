package eventbus

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/fortuna/mocks"
	spade_mocks "code.justin.tv/commerce/fortuna/mocks/spade"
	"code.justin.tv/eventbus/schema/pkg/eventbus/change"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"
	"github.com/stretchr/testify/mock"
)
import . "github.com/smartystreets/goconvey/convey"

func TestTwoFactorWorker(t *testing.T) {
	Convey("Test TwoFactorWorker", t, func() {
		mockFulfiller := new(mocks.IFulfiller)
		mockSpade := new(spade_mocks.Client)
		mockRewardDao := new(mocks.IRewardDAO)
		mockReceiver := new(mocks.Receiver)

		worker := NewTwoFactorWorker(mockFulfiller, mockSpade, mockRewardDao, mockReceiver)

		Convey("Test Entitle Fails returns error", func() {
			mockFulfiller.On("ClaimMakoReward", mock.Anything).Return(errors.New("Error"))

			event := &schema.Update{
				UserId:     "user",
				AnyEnabled: &change.BoolChange{Value: true},
			}

			err := worker.Handle(context.Background(), nil, event)

			So(err, ShouldNotBeNil)
		})

		Convey("Test Unentitle Fails returns error", func() {
			mockFulfiller.On("ClaimMakoReward", mock.Anything).Return(errors.New("Error"))

			event := &schema.Update{
				UserId:     "user",
				AnyEnabled: &change.BoolChange{Value: false},
			}

			err := worker.Handle(context.Background(), nil, event)

			So(err, ShouldNotBeNil)
		})

		Convey("Test Entitle Passes when Fulfiller returns no error", func() {
			mockFulfiller.On("ClaimMakoReward", mock.Anything).Return(nil)
			mockSpade.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(nil, nil)
			mockRewardDao.On("UpdateReward", mock.Anything).Return(nil, nil)

			event := &schema.Update{
				UserId:     "user",
				AnyEnabled: &change.BoolChange{Value: true},
			}

			err := worker.Handle(context.Background(), nil, event)

			So(err, ShouldBeNil)
		})

		Convey("Test Entitle Passes when Fulfiller but others fail returns no error", func() {
			mockFulfiller.On("ClaimMakoReward", mock.Anything).Return(nil)
			mockSpade.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Error"))
			mockReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(nil, errors.New("Error"))
			mockRewardDao.On("UpdateReward", mock.Anything).Return(nil, errors.New("Error"))

			event := &schema.Update{
				UserId:     "user",
				AnyEnabled: &change.BoolChange{Value: true},
			}

			err := worker.Handle(context.Background(), nil, event)

			So(err, ShouldBeNil)
		})

		Convey("Test Unetitle Passes when Fulfiller passes but notif not called", func() {
			mockFulfiller.On("ClaimMakoReward", mock.Anything).Return(nil)
			mockSpade.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Error"))
			mockRewardDao.On("UpdateReward", mock.Anything).Return(nil, nil)

			event := &schema.Update{
				UserId:     "user",
				AnyEnabled: &change.BoolChange{Value: false},
			}

			err := worker.Handle(context.Background(), nil, event)

			So(err, ShouldBeNil)
		})
	})

}
