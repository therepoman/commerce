package eventbus

import (
	"context"
	"testing"

	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/sqs/model"
	eventbus "code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/user_gift_subscription_user"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserGiftSubscriptionWorker(t *testing.T) {
	Convey("Test UserGiftSubscriptionWorker", t, func() {
		mockProcessor := new(mocks.Processor)
		mockStatter := new(mocks.Statter)

		ctx := context.Background()
		header := &eventbus.Header{}
		worker := NewUserGiftSubscriptionWorker(mockProcessor, mockStatter)
		event := schema.UserGiftSubscriptionUserCreate{
			FromUserId:  "user_id",
			ToUserId:    "channel_id",
			ProductId:   "product_id",
			OriginId:    "origin_id",
			Quantity:    1,
			IsAnonymous: false,
		}
		expectedEvent := model.SubscriptionGiftEvent{
			AnonGift:     event.IsAnonymous,
			GiftQuantity: int32(event.Quantity),
			OriginID:     event.OriginId,
			ProductID:    event.ProductId,
			SenderID:     event.FromUserId,
			ChannelID:    event.ToUserId,
		}

		Convey("Test Handle", func() {
			mockProcessor.On("Process", mock.Anything).Return(nil)
			err := worker.Handle(ctx, header, &event)
			So(err, ShouldBeNil)
		})

		Convey("Test tier conversion", func() {
			mockProcessor.On("Process", mock.Anything).Return(nil)
			event.Tier = &schema.Tier{
				Value: &schema.Tier_Numeral{
					Numeral: schema.SubTier_SUB_TIER_1,
				},
			}
			expectedEvent.Tier = "1000"

			err := worker.Handle(ctx, header, &event)
			So(err, ShouldBeNil)
			mockProcessor.AssertCalled(t, "Process", expectedEvent)
		})

		Convey("Test process error", func() {
			mockProcessor.On("Process", mock.Anything).Return(errors.New("woops"))
			err := worker.Handle(ctx, header, &event)
			So(err, ShouldNotBeNil)
		})
	})
}
