package eventbus

import (
	"context"

	"code.justin.tv/commerce/fortuna/sqs/model"
	eventbus "code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/user_gift_subscription_user"
	"github.com/cactus/go-statsd-client/statsd"
)

type UserGiftSubscriptionWorker struct {
	Processor model.Processor
	Stats     statsd.Statter
}

func NewUserGiftSubscriptionWorker(processor model.Processor, stats statsd.Statter) *UserGiftSubscriptionWorker {
	return &UserGiftSubscriptionWorker{
		Processor: processor,
		Stats:     stats,
	}
}

func (s *UserGiftSubscriptionWorker) Handle(ctx context.Context, h *eventbus.Header, event *schema.Create) error {
	originID := event.OriginId
	if originID == "" {
		originID = h.MessageID.String()
	}
	subEvent := model.SubscriptionGiftEvent{
		AnonGift:     event.IsAnonymous,
		GiftQuantity: int32(event.Quantity),
		OriginID:     originID,
		ProductID:    event.ProductId,
		SenderID:     event.FromUserId,
		ChannelID:    event.ToUserId,
	}
	if event.Tier != nil {
		subEvent.Tier = ConvertNumeralTierToString(event.Tier.String())
	}
	return s.Processor.Process(subEvent)
}

func (s *UserGiftSubscriptionWorker) Register(mux *eventbus.Mux) {
	schema.RegisterUserGiftSubscriptionUserCreateHandler(mux, s.Handle)
}
