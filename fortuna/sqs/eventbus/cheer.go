package eventbus

import (
	"context"

	"code.justin.tv/commerce/fortuna/sqs/model"
	"code.justin.tv/commerce/fortuna/utils/math"
	eventbus "code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/cheer"

	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

type CheerWorker struct {
	Processor model.Processor
	Stats     statsd.Statter
}

const (
	anonymousCheererID = "407665396"
)

func NewCheerWorker(processor model.Processor, stats statsd.Statter) *CheerWorker {
	return &CheerWorker{
		Processor: processor,
		Stats:     stats,
	}
}

func (s *CheerWorker) Handle(ctx context.Context, h *eventbus.Header, event *schema.CheerCreate) error {
	isAnon := event.GetAnonymousUser() != nil
	userID := event.GetUser().GetId()
	if isAnon {
		userID = anonymousCheererID
	}

	cheerEvent := model.CheerEvent{
		ChannelID:         event.GetToUserId(),
		UserID:            userID,
		Bits:              int32(math.AbsInt64(event.GetBits())), // its bits used, so its negative :')
		BitsTransactionID: event.GetBitsTransactionId(),
		Message:           event.GetPublicMessage(),
		IsAnonymous:       isAnon,
	}

	log.WithField("cheerEvent", event).Info("processing user cheer event")
	return s.Processor.Process(cheerEvent)
}

func (s *CheerWorker) Register(mux *eventbus.Mux) {
	schema.RegisterCheerCreateHandler(mux, s.Handle)
}
