package eventbus

import "strings"

// This is the format eventbus tiers are rendered as strings. e.g tier.string()
var tierMap = map[string]string{
	"numeral:SUB_TIER_INVALID": "1000",
	"numeral:SUB_TIER_1":       "1000",
	"numeral:SUB_TIER_2":       "2000",
	"numeral:SUB_TIER_3":       "3000",
}

func ConvertNumeralTierToString(tier string) string {
	tier = strings.TrimSpace(tier)
	return tierMap[tier]
}
