package eventbus

import (
	"testing"

	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/mocks"
	. "github.com/smartystreets/goconvey/convey"
)

func TestManager(t *testing.T) {
	Convey("Test Manager", t, func() {
		mockProcessor := new(mocks.Processor)
		mockStatter := new(mocks.Statter)
		cfg := &config.Configuration{}

		manager := NewManager(cfg, mockStatter, mockProcessor)

		Convey("Test Shutdown", func() {
			manager.Shutdown()
		})
	})
}
