package workers

import (
	"context"
	"errors"
	"strings"

	"code.justin.tv/commerce/fortuna/sqs/eventbus"
	e "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/eventbus/change"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"

	"github.com/aws/aws-sdk-go/service/sqs"
	log "github.com/sirupsen/logrus"
)

type TwoFactorBackfillWorker struct {
	TwoFactor *eventbus.TwoFactorWorker
}

// todo: kill this after we launch the 2FA product and backfill.
func (t *TwoFactorBackfillWorker) Handle(msg *sqs.Message) error {
	if msg.Body == nil {
		errMsg := "nil message body 2FA Backfill"
		log.Error(errMsg)
		return errors.New(errMsg)
	}
	message := *msg.Body
	// Assume if the message is in a <TUID>:<MessageType> format that we pass that along to the backfill processor
	// this is a quick and dirty throwaway hack that will let us toggle functions if needed, the default being
	// to do the entitlement and send the notification
	s := strings.Split(message, ":")
	if len(s) < 1 {
		log.Errorf("Error parsing backfill entry: %s", message)
	}
	userId := s[0]
	var header e.Header
	if len(s) > 1 {
		header.EventType = s[1]
	}
	err := t.TwoFactor.Handle(context.Background(), &header, &schema.Update{UserId: userId, AnyEnabled: &change.BoolChange{Value: true}})
	if err != nil {
		log.WithError(err).Errorf("Error running 2FA backfill for userId: %s", userId)
		return err
	}
	return nil
}
