package workers

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/clients/spade"
	"code.justin.tv/commerce/fortuna/rewards"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type MegaCheerGiftFulfillmentEventsWorker struct {
	Clients   *clients.Clients
	Utils     rewards.IUtils
	Fulfiller rewards.IFulfiller
	Notifier  rewards.INotifier
	Stats     statsd.Statter
}

const anonymousCheererLogin = "AnAnonymousCheerer"

func (m *MegaCheerGiftFulfillmentEventsWorker) Handle(msg *sqs.Message) error {
	defer func(startTime time.Time) {
		m.Stats.TimingDuration("sqs.worker.MegaCheerGiftFulfillment.latency", time.Since(startTime), 1.0)
	}(time.Now())

	// Unwrap SNS message
	var snsEvent sns.MegaCheerGiftFulfillmentSNSEvent
	if err := json.Unmarshal([]byte(aws.StringValue(msg.Body)), &snsEvent); err != nil {
		return errors.Wrap(err, "MegaCheerGiftFulfillmentEventsWorker failed to deserialize SNS message")
	}

	// Unmarshal SQS Body
	var megaCheerGiftFulfillmentEvent sns.MegaCheerGiftFulfillmentEvent
	if err := json.Unmarshal([]byte(snsEvent.Message), &megaCheerGiftFulfillmentEvent); err != nil {
		return errors.Wrap(err, "MegaCheerGiftFulfillmentEventsWorker failed to deserialize SQS body")
	}

	log.WithFields(log.Fields{
		"userID": megaCheerGiftFulfillmentEvent.RecipientID,
		"event":  megaCheerGiftFulfillmentEvent,
	}).Debug("Successfully read MegaCheerGiftFulfillmentEvent message")

	trigger, err := m.Utils.GetTriggerByID(megaCheerGiftFulfillmentEvent.TriggerID)
	if err != nil {
		return errors.Wrap(err, "MegaCheerGiftFulfillmentEventsWorker failed to get trigger by id")
	}

	if trigger == nil {
		log.WithFields(log.Fields{
			"triggerID": megaCheerGiftFulfillmentEvent.TriggerID,
			"event":     megaCheerGiftFulfillmentEvent,
		}).Error("Cannot fulfill gift event for trigger that does not exist")

		return errors.New("Cannot fulfill gift event for trigger that does not exist")
	}

	return m.FulfillAndNotify(megaCheerGiftFulfillmentEvent, trigger)
}

func (m *MegaCheerGiftFulfillmentEventsWorker) FulfillAndNotify(event sns.MegaCheerGiftFulfillmentEvent, trigger *campaign.Trigger) error {
	if !trigger.IsActive() {
		log.WithFields(log.Fields{
			"triggerID": trigger.ID,
			"event":     event,
		}).Debugf("MegaCheerGiftFulfillmentHandler ignoring inactive trigger")
		return nil
	}

	if !trigger.IsUserWhitelisted(event.RecipientID) {
		log.WithFields(log.Fields{
			"userID":    event.RecipientID,
			"triggerID": trigger.ID,
			"event":     event,
		}).Debug("MegaCheerGiftFulfillmentHandler ignoring non-whitelisted user")
		return nil
	}

	if trigger.StaffOnly {
		isStaff, err := m.Clients.UsersClient.IsUserStaff(event.RecipientID)
		if err != nil {
			log.WithFields(log.Fields{
				"userID":    event.RecipientID,
				"triggerID": trigger.ID,
				"event":     event,
			}).WithError(err).Error("Received error from Users client while retrieving staff status")
			return err
		}
		if !isStaff {
			log.WithFields(log.Fields{
				"userID":    event.RecipientID,
				"triggerID": trigger.ID,
				"event":     event,
			}).Debug("Trigger not visible to user")
			return nil
		}
	}

	// Give gift to recipient
	log.WithFields(log.Fields{
		"userID":    event.RecipientID,
		"triggerID": trigger.ID,
		"event":     event,
	}).Info("MegaCheerGiftFulfillmentHandler handling fulfillment event")

	selectedRewards, _, err := m.Fulfiller.EntitleTrigger(trigger, event.RecipientID, false, event.SelectionAmount)
	if err != nil {
		return errors.Wrap(err, "Failed to entitle trigger")
	}

	if len(selectedRewards) > 0 {
		log.WithFields(log.Fields{
			"userID":    event.RecipientID,
			"triggerID": trigger.ID,
			"event":     event,
			"rewards":   selectedRewards[0].ID,
		}).Info("MegaCheerGiftFulfillmentHandler granted recipient reward")

		benefactorLogin := event.BenefactorLogin
		if event.Event.IsAnonymous {
			benefactorLogin = anonymousCheererLogin
		}

		// Send pubsub message to the recipient if anything is granted for that user
		if err := m.Notifier.SendRecipientPubSub(
			rewards.RecipientPubSubInput{
				TransactionID:   event.Event.EventID,
				BenefactorLogin: benefactorLogin,
				RecipientID:     event.RecipientID,
				ChannelID:       event.Event.ChannelID,
				Domain:          trigger.Domain,
				TriggerType:     string(trigger.TriggerType),
				RewardsToNotify: selectedRewards}); err != nil {
			log.WithFields(log.Fields{
				"event": event,
			}).WithError(err).Error("MegaCheerGiftFulfillmentHandler failed to publish Recipient reward PubSub notification")
		}
	}

	for _, reward := range selectedRewards {
		megaRewardEventData := &spade.MegaCheerRewardItemEvent{
			UserID:          event.RecipientID,
			InitiatorUserID: event.Event.UserID,
			Time:            time.Now().Unix(),
			CheerAmount:     event.Event.Amount,
			Context:         spade.TriggerContext(trigger, false),
			ItemType:        reward.Type,
			ItemValue:       reward.ID,
			ChannelID:       event.Event.ChannelID,
			TransactionID:   event.Event.EventID,
		}

		if err := m.Clients.SpadeClient.TrackEvent(context.Background(), spade.RewardItemEventName.String(), megaRewardEventData); err != nil {
			log.WithFields(log.Fields{
				"recipientID":     event.RecipientID,
				"benefactorLogin": event.Event.UserID,
				"eventID":         event.Event.EventID,
				"reward":          reward.Name,
			}).WithError(err).Error("MegaCheerGiftFulfillmentHandler failed to send recipient reward Spade event")
		}
	}

	return nil
}
