package workers_test

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/mocks"
	spadeMock "code.justin.tv/commerce/fortuna/mocks/spade"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/model"
	"code.justin.tv/commerce/fortuna/sqs/workers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMegaCheerGiftFulfillmentEventsWorker(t *testing.T) {
	Convey("Test MegaCheerGiftFulfillment events worker", t, func() {
		mockStatter := new(mocks.Statter)
		mockSnsClient := new(mocks.ISNSClient)
		mockUtils := new(mocks.IUtils)
		mockPubSubClient := new(mocks.IPubSubClient)
		mockSpadeClient := new(spadeMock.Client)
		mockFulfiller := new(mocks.IFulfiller)
		mockNotifier := new(mocks.INotifier)
		mockUsersClient := new(mocks.IUsersClient)

		clients := &clients.Clients{
			UsersClient:  mockUsersClient,
			PubSubClient: mockPubSubClient,
			SNSClient:    mockSnsClient,
			SpadeClient:  mockSpadeClient,
		}

		worker := &workers.MegaCheerGiftFulfillmentEventsWorker{
			Clients:   clients,
			Fulfiller: mockFulfiller,
			Stats:     mockStatter,
			Utils:     mockUtils,
			Notifier:  mockNotifier,
		}

		mockStatter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("receiving an empty message", func() {
			msgId := "456"
			emptyMsg := &sqs.Message{MessageId: &msgId}
			err := worker.Handle(emptyMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("receiving a corrupted message", func() {
			msgId := "456"
			badMsg := &sqs.Message{
				Body:      aws.String("fghsdjfhsdfhsa38r5hkd8u"),
				MessageId: &msgId,
			}

			err := worker.Handle(badMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("receiving a good message", func() {
			msgId := "456"
			testTriggerID1 := "testTrigger1"
			testRecipient := "test-recipient-id-1"

			testEvent := model.MegaCommerceEvent{
				ChannelID:   "test-channel-id",
				UserID:      "test-user-id",
				EventID:     "test-event-id",
				Message:     "give me cyberpunk 2077 now~",
				Amount:      10,
				TriggerType: campaign.TriggerTypeCheer,
			}

			anonymousTestEvent := model.MegaCommerceEvent{
				ChannelID:   "test-channel-id",
				UserID:      "test-user-id",
				EventID:     "test-event-id",
				Message:     "what if geoff keighley blasted juul",
				Amount:      10,
				IsAnonymous: true,
				TriggerType: campaign.TriggerTypeCheer,
			}

			testReward1 := &campaign.Reward{
				ID:       "test-emote-reward-id",
				Type:     "emote",
				ImageURL: "www.zombo.com",
				Domain:   "domain",
			}

			testReward2 := &campaign.Reward{
				ID:       "test-emote-reward-id2",
				Type:     "emote",
				ImageURL: "www.zombo.com",
				Domain:   "domain",
			}

			megaCheerGiftEvent := sns.MegaCheerGiftFulfillmentEvent{
				RecipientID:     testRecipient,
				BenefactorLogin: "there",
				TriggerID:       testTriggerID1,
				Domain:          "tests",
				Event:           testEvent,
			}

			anonymousMegaCheerGiftEvent := sns.MegaCheerGiftFulfillmentEvent{
				RecipientID:     testRecipient,
				BenefactorLogin: "there",
				TriggerID:       testTriggerID1,
				Domain:          "tests",
				Event:           anonymousTestEvent,
			}

			testTrigger1 := &campaign.Trigger{
				ActiveStartDate: time.Date(2018, time.February, 28, 9, 0, 0, 0, time.UTC),
				ActiveEndDate:   time.Date(2022, time.February, 28, 9, 0, 0, 0, time.UTC),
				RecipientRewardAttributes: campaign.RewardAttributes{
					Rewards: []*campaign.Reward{testReward1, testReward2},
				},
				Domain:      "Domain",
				TriggerType: campaign.TriggerTypeCheer,
			}

			// Create mocked SQS message from SNS
			megaCheerGiftEventJsonString, err := json.Marshal(megaCheerGiftEvent)
			if err != nil {
				panic("Unable to marshal object required for testing")
			}
			megaCheerGiftSNSEvent := sns.MegaCheerGiftFulfillmentSNSEvent{
				Message: string(megaCheerGiftEventJsonString),
			}
			megaCheerGiftSNSJsonString, err := json.Marshal(megaCheerGiftSNSEvent)
			if err != nil {
				panic("Unable to marshal object required for testing")
			}

			msg := &sqs.Message{
				Body:      aws.String(string(megaCheerGiftSNSJsonString)),
				MessageId: &msgId,
			}

			anonymousMegaCheerGiftEventJsonString, err := json.Marshal(anonymousMegaCheerGiftEvent)
			if err != nil {
				panic("Unable to marshal object required for testing")
			}
			anonymousMegaCheerGiftSNSEvent := sns.MegaCheerGiftFulfillmentSNSEvent{
				Message: string(anonymousMegaCheerGiftEventJsonString),
			}
			anonymousMegaCheerGiftSNSJsonString, err := json.Marshal(anonymousMegaCheerGiftSNSEvent)
			if err != nil {
				panic("Unable to marshal object required for testing")
			}

			anonymousMsg := &sqs.Message{
				Body:      aws.String(string(anonymousMegaCheerGiftSNSJsonString)),
				MessageId: &msgId,
			}

			Convey("when trigger does not exist", func() {
				mockUtils.On("GetTriggerByID", mock.Anything).Return(nil, nil)

				err := worker.Handle(msg)
				So(err, ShouldNotBeNil)
			})

			Convey("when trigger errors", func() {
				mockUtils.On("GetTriggerByID", mock.Anything).Return(nil, errors.New("boom"))

				err := worker.Handle(msg)
				So(err, ShouldNotBeNil)
			})

			Convey("it should fulfill and notify rewards", func() {
				mockUtils.On("GetTriggerByID", mock.Anything).Return(testTrigger1, nil)
				mockNotifier.On("SendRecipientPubSub", mock.Anything).Return(nil)
				mockSpadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockFulfiller.On("EntitleTrigger", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*campaign.Reward{testReward1}, nil, nil)

				err := worker.Handle(msg)
				So(err, ShouldBeNil)

				mockNotifier.AssertNumberOfCalls(t, "SendRecipientPubSub", 1)
				mockFulfiller.AssertNumberOfCalls(t, "EntitleTrigger", 1)
			})

			Convey("it should fulfill and notify rewards correctly when anonymous", func() {
				mockUtils.On("GetTriggerByID", mock.Anything).Return(testTrigger1, nil)
				mockNotifier.On("SendRecipientPubSub", mock.Anything).Return(nil)
				mockSpadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockFulfiller.On("EntitleTrigger", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*campaign.Reward{testReward1}, nil, nil)

				err := worker.Handle(anonymousMsg)
				So(err, ShouldBeNil)

				expectedInput := rewards.RecipientPubSubInput{
					TransactionID:   testEvent.EventID,
					RecipientID:     testRecipient,
					BenefactorLogin: "AnAnonymousCheerer",
					RewardsToNotify: []*campaign.Reward{testReward1},
					ChannelID:       testEvent.ChannelID,
					Domain:          "Domain",
					TriggerType:     string(testEvent.TriggerType),
				}

				mockNotifier.AssertNumberOfCalls(t, "SendRecipientPubSub", 1)
				mockFulfiller.AssertNumberOfCalls(t, "EntitleTrigger", 1)
				mockNotifier.AssertCalled(t, "SendRecipientPubSub", expectedInput)
			})

			Convey("it should not fulfill rewards for non-whitelisted users", func() {
				testTrigger1.UserWhitelistEnabled = true
				testTrigger1.UserWhitelist = map[string]bool{"test-recipient": true}

				mockUtils.On("GetTriggerByID", mock.Anything).Return(testTrigger1, nil)
				mockNotifier.On("SendRecipientPubSub", mock.Anything).Return(nil)
				mockSpadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockFulfiller.On("EntitleTrigger", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*campaign.Reward{testReward1}, nil, nil)

				err := worker.Handle(msg)
				So(err, ShouldBeNil)

				mockFulfiller.AssertNumberOfCalls(t, "EntitleTrigger", 0)
				mockNotifier.AssertNumberOfCalls(t, "SendRecipientPubSub", 0)
			})

			Convey("it should not fulfill rewards for non-staff users", func() {
				testTrigger1.StaffOnly = true

				mockUtils.On("GetTriggerByID", mock.Anything).Return(testTrigger1, nil)
				mockNotifier.On("SendRecipientPubSub", mock.Anything).Return(nil)
				mockSpadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockFulfiller.On("EntitleTrigger", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*campaign.Reward{testReward1}, nil, nil)
				mockUsersClient.On("IsUserStaff", mock.Anything).Return(false, nil)

				err := worker.Handle(msg)
				So(err, ShouldBeNil)

				mockFulfiller.AssertNumberOfCalls(t, "EntitleTrigger", 0)
				mockNotifier.AssertNumberOfCalls(t, "SendRecipientPubSub", 0)
			})
		})
	})
}
