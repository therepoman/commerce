package model

// FulfillmentEvent is the message structure from the TFS SQS queue
type FulfillmentEvent struct {
	IdempotenceKey string `json:"idempotenceKey"`
	Tuid           string `json:"tuid"`
	Source         string `json:"source"`
}
