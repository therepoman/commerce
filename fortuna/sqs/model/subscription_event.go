package model

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"gopkg.in/validator.v2"
)

// Event is the Subscription event object from SQS
type SubscriptionEvent struct {
	ChannelID string `json:"channel_id" validate:"nonzero"`
	UserID    string `json:"user_id" validate:"nonzero"`
	MessageID string `json:"msg_id" validate:"nonzero"`
	ProductID string `json:"product_id"`
	Tier      string `json:"tier"`
}

// GetEventID ...
func (s SubscriptionEvent) GetEventID() string {
	return s.MessageID
}

// GetChannelIDs ...
func (s SubscriptionEvent) GetChannelIDs() []string {
	return []string{s.ChannelID}
}

// GetDomain can not retrieve campaign domain based on event
func (s SubscriptionEvent) GetDomain() string {
	return ""
}

// BelongsToCampaign ...
func (s SubscriptionEvent) BelongsToCampaign(campaign *campaign.Campaign) bool {
	if !campaign.AllChannels {
		return campaign.ChannelList[s.ChannelID]
	} else {
		return !campaign.ExcludedChannelList[s.ChannelID]
	}
}

// Validate
func (s SubscriptionEvent) Validate() error {
	return validator.Validate(s)
}

// ToMegaCommerceEvent converts the sub event to a format compatible with the MegaCommerce handler
func (s SubscriptionEvent) ToMegaCommerceEvent() MegaCommerceEvent {
	return MegaCommerceEvent{
		EventID:     s.MessageID,
		ChannelID:   s.ChannelID,
		UserID:      s.UserID,
		Amount:      1,
		TriggerType: campaign.TriggerTypeSubscription,
		Tier:        s.Tier,
	}
}
