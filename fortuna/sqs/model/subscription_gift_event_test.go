package model

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSubscriptionGiftEvent(t *testing.T) {
	Convey("Test SubscriptionGiftEvent", t, func() {
		testCampaign1 := campaign.Campaign{
			ChannelList: map[string]bool{
				"1": true,
			},
		}

		testCampaign3 := campaign.Campaign{
			AllChannels: true,
		}

		testCampaign4 := campaign.Campaign{
			AllChannels: true,
			ExcludedChannelList: map[string]bool{
				"2": true,
			},
		}

		Convey("for SubscriptionGiftEvent that's not in blacklist", func() {
			event := SubscriptionGiftEvent{
				ChannelID: "3",
			}

			belong := event.BelongsToCampaign(&testCampaign4)
			So(belong, ShouldEqual, true)
		})

		Convey("for SubscriptionGiftEvent that's in blacklist", func() {
			event := SubscriptionGiftEvent{
				ChannelID: "2",
			}

			belong := event.BelongsToCampaign(&testCampaign4)
			So(belong, ShouldEqual, false)
		})

		Convey("for SubscriptionGiftEvent against a campaign", func() {
			event := SubscriptionGiftEvent{
				ChannelID: "1",
			}

			belong := event.BelongsToCampaign(&testCampaign1)
			So(belong, ShouldEqual, true)
		})

		Convey("for SubscriptionGiftEvent against a campaign without blacklist", func() {
			event := SubscriptionGiftEvent{
				ChannelID: "3",
			}

			belong := event.BelongsToCampaign(&testCampaign1)
			So(belong, ShouldEqual, false)
		})

		Convey("for SubscriptionGiftEvent against a campaign that has AllChannels set true", func() {
			event := SubscriptionGiftEvent{}

			belong := event.BelongsToCampaign(&testCampaign3)
			So(belong, ShouldEqual, true)
		})
	})
}
