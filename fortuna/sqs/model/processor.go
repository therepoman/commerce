package model

// Processor represents an object that can accept any type of campaign event for processing,
// probably by routing that event to an appropriate specialized handler.
type Processor interface {
	Process(event CampaignEvent) error
}
