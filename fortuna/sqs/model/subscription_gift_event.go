package model

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"gopkg.in/validator.v2"
)

// Event is the Subscription event object from SQS
type SubscriptionGiftEvent struct {
	AnonGift     bool   `json:"anon_gift"`
	ChannelID    string `json:"channel_id" validate:"nonzero"`
	GiftQuantity int32  `json:"gift_quantity"`
	OriginID     string `json:"origin_id"`
	ProductID    string `json:"product_id"`
	SenderID     string `json:"sender_id" validate:"nonzero"`
	Status       string `json:"status"`
	Tier         string `json:"tier"`
}

// GetEventID ...
func (s SubscriptionGiftEvent) GetEventID() string {
	return s.OriginID
}

// GetChannelIDs ...
func (s SubscriptionGiftEvent) GetChannelIDs() []string {
	return []string{s.ChannelID}
}

// GetDomain can not retrieve campaign domain based on event
func (s SubscriptionGiftEvent) GetDomain() string {
	return ""
}

// BelongsToCampaign ...
func (s SubscriptionGiftEvent) BelongsToCampaign(campaign *campaign.Campaign) bool {
	if !campaign.AllChannels {
		return campaign.ChannelList[s.ChannelID]
	} else {
		return !campaign.ExcludedChannelList[s.ChannelID]
	}
}

// Validate
func (s SubscriptionGiftEvent) Validate() error {
	return validator.Validate(s)
}

// ToMegaCommerceEvent converts the sub event to a format compatible with the MegaCommerce handler
func (s SubscriptionGiftEvent) ToMegaCommerceEvent() MegaCommerceEvent {
	return MegaCommerceEvent{
		EventID:     s.OriginID,
		ChannelID:   s.ChannelID,
		UserID:      s.SenderID,
		Amount:      s.GiftQuantity,
		TriggerType: campaign.TriggerTypeSubscriptionGift,
		IsAnonymous: s.AnonGift,
		Tier:        s.Tier,
	}
}
