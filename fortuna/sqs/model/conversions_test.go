package model_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/sqs/model"
	. "github.com/smartystreets/goconvey/convey"
)

type phonyEvent struct{}

func (p phonyEvent) GetEventID() string {
	return "phony"
}
func (p phonyEvent) GetChannelIDs() []string {
	return []string{"phony"}
}
func (p phonyEvent) GetDomain() string {
	return ""
}
func (p phonyEvent) BelongsToCampaign(c *campaign.Campaign) bool {
	return false
}
func (p phonyEvent) Validate() error {
	return nil
}

func TestConversions(t *testing.T) {
	Convey("Test event conversions", t, func() {
		Convey("Test ToCheerEvent", func() {
			Convey("with a valid underlying value", func() {
				cheerEvent := model.CheerEvent{
					BitsTransactionID: "really specific event ID",
				}
				var campaignEvent model.CampaignEvent = cheerEvent

				result, ok, err := model.ToCheerEvent(campaignEvent)
				So(err, ShouldBeNil)
				So(ok, ShouldBeTrue)
				So(result, ShouldResemble, cheerEvent)
			})

			Convey("with an invalid underlying value", func() {
				phonyEvent := phonyEvent{}
				var campaignEvent model.CampaignEvent = phonyEvent

				_, ok, err := model.ToCheerEvent(campaignEvent)
				So(err, ShouldBeNil)
				So(ok, ShouldBeFalse)
			})

			Convey("with a nil CampaignEvent", func() {
				var campaignEvent model.CampaignEvent

				_, ok, err := model.ToCheerEvent(campaignEvent)
				So(err, ShouldNotBeNil)
				So(ok, ShouldBeFalse)
			})
		})

		Convey("Test ToSubscriptionEvent", func() {
			Convey("with a valid underlying value", func() {
				subscriptionEvent := model.SubscriptionEvent{
					ChannelID: "channel_id",
					UserID:    "user_id",
					MessageID: "msg_id",
					ProductID: "product_id",
					Tier:      "1000",
				}
				var campaignEvent model.CampaignEvent = subscriptionEvent

				result, ok, err := model.ToSubscriptionEvent(campaignEvent)
				So(err, ShouldBeNil)
				So(ok, ShouldBeTrue)
				So(result, ShouldResemble, subscriptionEvent)
			})

			Convey("with an invalid underlying value", func() {
				phonyEvent := phonyEvent{}
				var campaignEvent model.CampaignEvent = phonyEvent

				_, ok, err := model.ToSubscriptionEvent(campaignEvent)
				So(err, ShouldBeNil)
				So(ok, ShouldBeFalse)
			})

			Convey("with a nil CampaignEvent", func() {
				var campaignEvent model.CampaignEvent

				_, ok, err := model.ToSubscriptionEvent(campaignEvent)
				So(err, ShouldNotBeNil)
				So(ok, ShouldBeFalse)
			})
		})

		Convey("Test ToSubscriptionGiftEvent", func() {
			Convey("with a valid underlying value", func() {
				subscriptionGiftEvent := model.SubscriptionGiftEvent{
					AnonGift:     false,
					ChannelID:    "channel_id",
					GiftQuantity: 1,
					OriginID:     "origin_id",
					ProductID:    "product_id",
					SenderID:     "sender_id",
					Status:       "status",
					Tier:         "1000",
				}
				var campaignEvent model.CampaignEvent = subscriptionGiftEvent

				result, ok, err := model.ToSubscriptionGiftEvent(campaignEvent)
				So(err, ShouldBeNil)
				So(ok, ShouldBeTrue)
				So(result, ShouldResemble, subscriptionGiftEvent)
			})

			Convey("with an invalid underlying value", func() {
				phonyEvent := phonyEvent{}
				var campaignEvent model.CampaignEvent = phonyEvent

				_, ok, err := model.ToSubscriptionGiftEvent(campaignEvent)
				So(err, ShouldBeNil)
				So(ok, ShouldBeFalse)
			})

			Convey("with a nil CampaignEvent", func() {
				var campaignEvent model.CampaignEvent

				_, ok, err := model.ToSubscriptionGiftEvent(campaignEvent)
				So(err, ShouldNotBeNil)
				So(ok, ShouldBeFalse)
			})
		})

	})
}
