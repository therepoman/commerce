package model

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"gopkg.in/validator.v2"
)

// Event is the Cheer event object from SQS
type CheerEvent struct {
	ChannelID         string `json:"channel_id" validate:"nonzero"`
	UserID            string `json:"user_id" validate:"nonzero"`
	BitsTransactionID string `json:"event_id" validate:"nonzero"`
	Bits              int32  `json:"amount" validate:"nonzero"`
	IsAnonymous       bool   `json:"is_anonymous"`
	Message           string `json:"message"`
}

// Implement CampaignEvent
func (b CheerEvent) GetEventID() string {
	return b.BitsTransactionID
}

func (b CheerEvent) GetChannelIDs() []string {
	return []string{b.ChannelID}
}

// GetDomain can not retrieve campaign domain based on event
func (b CheerEvent) GetDomain() string {
	return ""
}

func (b CheerEvent) BelongsToCampaign(campaign *campaign.Campaign) bool {
	if !campaign.AllChannels {
		return campaign.ChannelList[b.ChannelID]
	} else {
		return !campaign.ExcludedChannelList[b.ChannelID]
	}
}

func (b CheerEvent) Validate() error {
	return validator.Validate(b)
}

func (b CheerEvent) ToMegaCommerceEvent() MegaCommerceEvent {
	return MegaCommerceEvent{
		EventID:     b.BitsTransactionID,
		ChannelID:   b.ChannelID,
		UserID:      b.UserID,
		Amount:      b.Bits,
		TriggerType: campaign.TriggerTypeCheer,
		IsAnonymous: b.IsAnonymous,
		Message:     b.Message,
	}
}
