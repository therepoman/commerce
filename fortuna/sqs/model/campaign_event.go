package model

import "code.justin.tv/commerce/fortuna/campaign"

// CampaignEvent is an unknown event that will be handled appropriately by a specific handler.
// Each campaign event handler should contain knowledge of the expected concrete type of the reward events it receives.
type CampaignEvent interface {
	// GetEventID should return a string that identifies this event. // (ccmartin 3/22/18: Uniqueness is currently recommended, but not a constraint.)
	GetEventID() string
	// GetChannelIDs should return a slice of strings that identify the channels that this event originated from, or pertains to.
	GetChannelIDs() []string
	// GetDomain should return a domain for an event if available
	GetDomain() string
	// BelongsToCampaign should determine whether this event falls into the scope of the passed campaign,
	// typically by comparing channel ID(s) to the campaign's channel list.
	BelongsToCampaign(campaign *campaign.Campaign) bool
	// Validate returns an error if the data does not match the validator flags on the struct
	Validate() error
}
