package model

import (
	"errors"
)

// ToCheerEvent attempts to convert a value of interface CampaignEvent to a concrete CheerEvent value.
// ToCheerEvent treats an incompatible underlying type in CampaignEvent as an acceptable, non-exceptional case,
// and will not return an error; check the second return value to learn whether the type conversion succeeded.
// ToCheerEvent will return an error if it was passed a nil CampaignEvent.
func ToCheerEvent(campaignEvent CampaignEvent) (CheerEvent, bool, error) {
	if campaignEvent == nil {
		return CheerEvent{}, false, errors.New("Cannot convert nil CampaignEvent to CheerEvent")
	}

	cheerEvent, ok := campaignEvent.(CheerEvent)
	if !ok {
		return CheerEvent{}, false, nil
	}

	return cheerEvent, true, nil
}

func ToSubscriptionEvent(campaignEvent CampaignEvent) (SubscriptionEvent, bool, error) {
	if campaignEvent == nil {
		return SubscriptionEvent{}, false, errors.New("Cannot convert nil CampaignEvent to SubscriptionEvent")
	}

	subscriptionEvent, ok := campaignEvent.(SubscriptionEvent)
	if !ok {
		return SubscriptionEvent{}, false, nil
	}

	return subscriptionEvent, true, nil
}

func ToSubscriptionGiftEvent(campaignEvent CampaignEvent) (SubscriptionGiftEvent, bool, error) {
	if campaignEvent == nil {
		return SubscriptionGiftEvent{}, false, errors.New("Cannot convert nil CampaignEvent to SubscriptionGiftEvent")
	}

	subscriptionGiftEvent, ok := campaignEvent.(SubscriptionGiftEvent)
	if !ok {
		return SubscriptionGiftEvent{}, false, nil
	}

	return subscriptionGiftEvent, true, nil
}
