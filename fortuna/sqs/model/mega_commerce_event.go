package model

import (
	"code.justin.tv/commerce/fortuna/campaign"
)

// MegaCommerceEvent is the structure of an event triggered by bits, subscriptions, or subgifts.
type MegaCommerceEvent struct {
	EventID     string               `json:"event_id" validate:"nonzero"`
	ChannelID   string               `json:"channel_id" validate:"nonzero"`
	UserID      string               `json:"user_id" validate:"nonzero"`
	Amount      int32                `json:"amount" validate:"nonzero"`
	TriggerType campaign.TriggerType `json:"trigger_type" validate:"nonzero"`
	IsAnonymous bool                 `json:"is_anonymous"`
	Message     string               `json:"message"` // TODO: Add back nonzero validation after Bits team fix
	Tier        string               `json:"tier"`
}

const (
	SubscriptionTier2 string = "2000"
	SubscriptionTier3 string = "3000"
)

type giftAmounts struct {
	BenefactorAmount int
	RecipientAmount  int
}

var MegaCommerceEventScaledSelections = map[campaign.TriggerType]map[int32]giftAmounts{
	campaign.TriggerTypeCheer: {
		300: {
			BenefactorAmount: 1,
			RecipientAmount:  3,
		},
		500: {
			BenefactorAmount: 2,
			RecipientAmount:  10,
		},
		1000: {
			BenefactorAmount: 5,
			RecipientAmount:  25,
		},
		5000: {
			BenefactorAmount: 100, // set this to something high to ensure "all" rewards are granted at this tier
			RecipientAmount:  150,
		},
	},
	campaign.TriggerTypeSubscription: {
		1: {
			BenefactorAmount: 1,
			RecipientAmount:  5,
		},
	},
	campaign.TriggerTypeSubscriptionGift: {
		1: {
			BenefactorAmount: 1,
			RecipientAmount:  5,
		},
		5: {
			BenefactorAmount: 5,
			RecipientAmount:  25,
		},
		10: {
			BenefactorAmount: 7,
			RecipientAmount:  50,
		},
		20: {
			BenefactorAmount: 100, // set this to something high to ensure "all" rewards are granted at this tier
			RecipientAmount:  100,
		},
		50: {
			BenefactorAmount: 100, // set this to something high to ensure "all" rewards are granted at this tier
			RecipientAmount:  250,
		},
		100: {
			BenefactorAmount: 100, // set this to something high to ensure "all" rewards are granted at this tier
			RecipientAmount:  500,
		},
	},
}

var MegaCommerceEventBonuses = map[campaign.TriggerType]map[string]giftAmounts{
	campaign.TriggerTypeSubscription: {
		SubscriptionTier2: {
			BenefactorAmount: 1,
			RecipientAmount:  5,
		},
		SubscriptionTier3: {
			BenefactorAmount: 2,
			RecipientAmount:  10,
		},
	},
	campaign.TriggerTypeSubscriptionGift: {
		SubscriptionTier2: {
			BenefactorAmount: 1,
			RecipientAmount:  5,
		},
		SubscriptionTier3: {
			BenefactorAmount: 2,
			RecipientAmount:  10,
		},
	},
}

func (e *MegaCommerceEvent) GetScaledBenefactorAmount() int {
	var bonusAmount int
	if bonusTiers, ok := MegaCommerceEventBonuses[e.TriggerType]; ok {
		if bonuses, ok := bonusTiers[e.Tier]; ok {
			bonusAmount = bonuses.BenefactorAmount
		}
	}
	var benefactorGiftCount int
	if selections, ok := MegaCommerceEventScaledSelections[e.TriggerType]; ok {
		maxKey := getMaxKey(e.Amount, selections)
		if selectionAmounts, ok := selections[maxKey]; ok {
			benefactorGiftCount = selectionAmounts.BenefactorAmount
		}
	}

	return benefactorGiftCount + bonusAmount
}

func (e *MegaCommerceEvent) GetScaledRecipientAmount() int {
	var bonusAmount int
	if bonusTiers, ok := MegaCommerceEventBonuses[e.TriggerType]; ok {
		if bonuses, ok := bonusTiers[e.Tier]; ok {
			bonusAmount = bonuses.RecipientAmount
		}
	}
	var recipientAmount int
	if selections, ok := MegaCommerceEventScaledSelections[e.TriggerType]; ok {
		maxKey := getMaxKey(e.Amount, selections)
		if selectionAmounts, ok := selections[maxKey]; ok {
			recipientAmount = selectionAmounts.RecipientAmount
		}
	}

	return recipientAmount + bonusAmount
}

func getMaxKey(amount int32, iterMap map[int32]giftAmounts) int32 {
	var maxKey int32

	for key := range iterMap {
		if amount >= key && key > maxKey {
			maxKey = key
		}
	}
	return maxKey
}
