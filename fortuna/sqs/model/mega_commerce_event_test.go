package model_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/sqs/model"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMegaCommerceEvent(t *testing.T) {
	Convey("MegaCommerceEvent", t, func() {
		event := model.MegaCommerceEvent{
			EventID:   "event_id",
			ChannelID: "channel_id",
			UserID:    "user_id",
			Amount:    0,
		}

		Convey("Test GetScaledBenefactorAmount", func() {
			// See correct values in megaCommerceScaledSelections
			var expectedValue int
			Convey("Cheer based", func() {
				event.TriggerType = campaign.TriggerTypeCheer

				Convey("max cheer amount", func() {
					event.Amount = 5001 // a little more than the max value
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeCheer][5000].BenefactorAmount
				})
			})

			Convey("Subscription based", func() {
				event.TriggerType = campaign.TriggerTypeSubscription

				Convey("max sub amount", func() {
					event.Amount = 1
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscription][1].BenefactorAmount
				})

				Convey("max sub amount with bonus", func() {
					event.Amount = 1
					event.Tier = model.SubscriptionTier2
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscription][1].BenefactorAmount + model.MegaCommerceEventBonuses[campaign.TriggerTypeSubscription][model.SubscriptionTier2].BenefactorAmount
				})
			})

			Convey("Subgift based", func() {
				event.TriggerType = campaign.TriggerTypeSubscriptionGift

				Convey("max subgift amount", func() {
					event.Amount = 101 // a little more than the max value
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscriptionGift][100].BenefactorAmount
				})

				Convey("max subgift amount with bonus", func() {
					event.Amount = 101 // a little more than the max value
					event.Tier = model.SubscriptionTier3
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscriptionGift][100].BenefactorAmount + model.MegaCommerceEventBonuses[campaign.TriggerTypeSubscriptionGift][model.SubscriptionTier3].BenefactorAmount
				})
			})

			amount := event.GetScaledBenefactorAmount()
			So(amount, ShouldEqual, expectedValue)
		})

		Convey("Test GetScaledRecipientAmount", func() {
			// See correct values in megaCommerceScaledSelections
			var expectedValue int
			Convey("Cheer based", func() {
				event.TriggerType = campaign.TriggerTypeCheer

				Convey("max cheer amount", func() {
					event.Amount = 5000 // a little more than the max value
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeCheer][5000].RecipientAmount
				})
			})

			Convey("Subscription based", func() {
				event.TriggerType = campaign.TriggerTypeSubscription

				Convey("max sub amount", func() {
					event.Amount = 1
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscription][1].RecipientAmount
				})

				Convey("max sub amount with bonus", func() {
					event.Amount = 1
					event.Tier = model.SubscriptionTier2
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscription][1].RecipientAmount + model.MegaCommerceEventBonuses[campaign.TriggerTypeSubscription][model.SubscriptionTier2].RecipientAmount
				})
			})

			Convey("Subgift based", func() {
				event.TriggerType = campaign.TriggerTypeSubscriptionGift

				Convey("max subgift amount", func() {
					event.Amount = 101 // a little more than the max value
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscriptionGift][100].RecipientAmount
				})

				Convey("max subgift amount with bonus", func() {
					event.Amount = 101 // a little more than the max value
					event.Tier = model.SubscriptionTier3
					expectedValue = model.MegaCommerceEventScaledSelections[campaign.TriggerTypeSubscriptionGift][100].RecipientAmount + model.MegaCommerceEventBonuses[campaign.TriggerTypeSubscriptionGift][model.SubscriptionTier3].RecipientAmount
				})
			})

			amount := event.GetScaledRecipientAmount()
			So(amount, ShouldEqual, expectedValue)
		})
	})
}
