package model

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSubscriptionEvent(t *testing.T) {
	Convey("Test SubscriptionEvent", t, func() {
		testCampaign1 := campaign.Campaign{
			ChannelList: map[string]bool{
				"1": true,
			},
		}

		testCampaign3 := campaign.Campaign{
			AllChannels: true,
		}

		testCampaign4 := campaign.Campaign{
			AllChannels: true,
			ExcludedChannelList: map[string]bool{
				"2": true,
			},
		}

		Convey("for SubscriptionEvent that's not in blacklist", func() {
			event := SubscriptionEvent{
				ChannelID: "3",
			}

			belong := event.BelongsToCampaign(&testCampaign4)
			So(belong, ShouldEqual, true)
		})

		Convey("for SubscriptionEvent that's in blacklist", func() {
			event := SubscriptionEvent{
				ChannelID: "2",
			}

			belong := event.BelongsToCampaign(&testCampaign4)
			So(belong, ShouldEqual, false)
		})

		Convey("for SubscriptionEvent against a campaign", func() {
			event := SubscriptionEvent{
				ChannelID: "1",
			}

			belong := event.BelongsToCampaign(&testCampaign1)
			So(belong, ShouldEqual, true)
		})

		Convey("for SubscriptionEvent against a campaign without blacklist", func() {
			event := SubscriptionEvent{
				ChannelID: "3",
			}

			belong := event.BelongsToCampaign(&testCampaign1)
			So(belong, ShouldEqual, false)
		})

		Convey("for SubscriptionEvent against a campaign that has AllChannels set true", func() {
			event := SubscriptionEvent{}

			belong := event.BelongsToCampaign(&testCampaign3)
			So(belong, ShouldEqual, true)
		})
	})
}
