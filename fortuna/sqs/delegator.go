package sqs

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/rewards"
	"code.justin.tv/commerce/fortuna/sqs/handler"
	"code.justin.tv/commerce/fortuna/sqs/model"
)

// Delegator processes SQS messages by delegating them to the appropriate handlers
type Delegator struct {
	Clients           *clients.Clients
	Config            *config.Configuration
	Stats             statsd.Statter
	ObjectiveHandlers map[string]handler.ObjectiveHandler
	TriggerHandlers   map[string]handler.TriggerHandler
	Utils             rewards.IUtils
}

func NewDelegator(clients *clients.Clients,
	config *config.Configuration,
	stats statsd.Statter,
	triggerHandlers map[string]handler.TriggerHandler,
	utils rewards.IUtils,
) Delegator {
	return Delegator{
		Clients:         clients,
		Config:          config,
		Stats:           stats,
		TriggerHandlers: triggerHandlers,
		Utils:           utils,
	}
}

// Process processes the input event
func (d *Delegator) Process(event model.CampaignEvent) error {
	if event == nil {
		return errors.New("Cannot process a nil CampaignEvent")
	}

	campaignConfig, err := d.getCampaignConfigForEvent(event)
	if err != nil {
		return errors.Wrap(err, "Delegator failed while getting the campaign config")
	}

	err = event.Validate()
	if err != nil {
		return errors.Wrapf(err, "Delegator failed to validate event message: %+v", event)
	}

	if len(campaignConfig.Campaigns) == 0 {
		log.WithFields(log.Fields{
			"eventID":    event.GetEventID(),
			"channelIDs": event.GetChannelIDs(),
			"domain":     event.GetDomain(),
		}).Debug("Delegator not processing event: no matching campaigns")
		return nil
	}

	// For all domains, for all campaigns, for all objectives, send this event to its handler to be processed.
	domains := campaignConfig.Campaigns
	for _, campaigns := range domains {
		for _, campaign := range campaigns {

			// only send to campaign if the event belongs to it
			if !event.BelongsToCampaign(campaign) {
				log.WithFields(log.Fields{
					"eventID":    event.GetEventID(),
					"campaign":   campaign.ID,
					"channelIDs": event.GetChannelIDs(),
					"domain":     event.GetDomain(),
				}).Debug("Delegator not processing event: channel/domain is not in campaign's list")
				continue
			}

			if !campaign.IsActive() {
				log.WithFields(log.Fields{
					"eventID":    event.GetEventID(),
					"campaign":   campaign.ID,
					"channelIDs": event.GetChannelIDs(),
					"domain":     event.GetDomain(),
				}).Debug("Delegator not sending event to inactive campaign")
				continue
			}

			log.WithFields(log.Fields{
				"eventID":        event.GetEventID(),
				"campaignID":     campaign.ID,
				"campaign title": campaign.Title,
				"channelIDs":     event.GetChannelIDs(),
				"domain":         event.GetDomain(),
			}).Info("Delegator sending event to campaign")

			// Get handlers for triggers
			for _, trigger := range campaign.Triggers {
				if !trigger.IsActive() {
					log.WithFields(log.Fields{
						"eventID":    event.GetEventID(),
						"campaignID": campaign.ID,
						"triggerID":  trigger.ID,
						"channelIDs": event.GetChannelIDs(),
						"domain":     event.GetDomain(),
					}).Info("Delegator not sending event to trigger")
					continue
				}
				if err := d.handleTriggerEvent(event, trigger); err != nil {
					return errors.Wrap(err, "Handler failure")
				}
			}
		}
	}

	return nil
}

func (d *Delegator) handleTriggerEvent(event model.CampaignEvent, trigger *campaign.Trigger) error {
	handlerName, ok := handler.TriggerTypeToHandlerMap[trigger.TriggerType]
	if !ok {
		return errors.WithStack(fmt.Errorf("Trigger type is unknown: %s", trigger.TriggerType))
	}
	eventHandler, ok := d.TriggerHandlers[handlerName]
	if !ok {
		return errors.WithStack(fmt.Errorf("Unable to handle event: %v", event))
	}

	if eventHandlerStatsName, exists := handler.EventHandlerStatsNameMap[handlerName]; exists {
		d.Stats.Inc(fmt.Sprintf("sqs.handler.%s.volume", eventHandlerStatsName), 1, 1.0)
		defer d.reportHandlerStats(time.Now(), eventHandlerStatsName)
	} else {
		log.WithField("handlerName", handlerName).
			WithError(errors.New("Unknown SQS event handler")).
			Errorf("Unable to find Stats name for SQS event handler: %s. No metrics will be collected.", handlerName)
	}

	if err := eventHandler.Handle(event, *trigger); err != nil {
		return errors.Wrap(err, "Event handler failed")
	}

	return nil
}

func (d *Delegator) getCampaignConfigForEvent(event model.CampaignEvent) (*campaign.Configuration, error) {
	domain := event.GetDomain()
	if domain != "" {
		campaignConfig, err := d.Utils.GetCampaignConfigForDomain(domain)
		if err != nil {
			return nil, err
		}
		return campaignConfig, nil
	}

	// HATS events contain multiple channel IDs whereas Bits events contain only one.
	// Since all channels should have the same campaign domain, it is simpler to use the first channel ID.
	// (Checking all ChannelIDs is not possible since it can be 100+ unique channels)
	channelIDs := event.GetChannelIDs()
	channelID := ""
	if len(channelIDs) > 0 {
		channelID = channelIDs[0]
	}
	if channelID != "" {
		campaignConfig, err := d.Utils.GetCampaignConfigForChannelID(channelID)
		if err != nil {
			return nil, err
		}
		return campaignConfig, nil
	}

	// Without a domain/channel to use for lookups, the event does not match any campaigns
	return &campaign.Configuration{
		Campaigns: map[string][]*campaign.Campaign{},
	}, nil
}

func (d *Delegator) reportHandlerStats(startTime time.Time, name string) {
	d.Stats.TimingDuration(fmt.Sprintf("sqs.handler.%s.latency", name), time.Since(startTime), 1.0)
}
