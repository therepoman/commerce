package sqs

import (
	"fmt"
	"sync"
	"time"

	"code.justin.tv/chat/workerqueue"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

func init() {
	workerqueue.SetRecoverFunc(func(err error) error {
		log.WithError(err).Error("Received SQS Worker Error")
		return errors.WithStack(err)
	})
}

type Worker interface {
	Handle(msg *sqs.Message) error
}

type WorkerManager struct {
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
	worker      Worker
}

func NewWorkerManager(queueName string, numWorkers int, client sqsiface.SQSAPI, worker Worker, stats statsd.Statter) *WorkerManager {
	log.Infof("Initializing %s SQS workers...", queueName)

	manager := &WorkerManager{
		stopChannel: make(chan struct{}),
		worker:      worker,
	}

	params := workerqueue.CreateWorkersParams{
		NumWorkers: numWorkers,
		QueueName:  queueName,
		Client:     client,
		Tasks: map[workerqueue.TaskVersion]workerqueue.TaskFn{
			workerqueue.FallbackTaskVersion: manager.worker.Handle,
			workerqueue.TaskVersion(1):      manager.worker.Handle,
		},
	}

	var errCh <-chan error
	var err error
	manager.waitGroup, errCh, err = workerqueue.CreateWorkers(params, manager.stopChannel, stats)
	if err != nil {
		log.WithError(err).Fatalf("Failed to create workers")
	}

	go func() {
		for err := range errCh {
			stats.Inc("SQSPoller.SQSWorkerError", 1, 1.0)
			// This error comes from the chat/workerqueue, which isn't wrapped.
			log.WithError(errors.WithStack(err)).Errorf("Error occurred in SQS worker for queue %s", queueName)
		}
	}()

	log.Infof("Finished initializing %s SQS workers...", queueName)
	return manager
}

func (s *WorkerManager) Shutdown(teardownTimeout time.Duration) error {
	close(s.stopChannel)
	if waitWithTimeout(s.waitGroup, teardownTimeout) {
		return fmt.Errorf("Timed out while waiting for SQS workers to complete")
	}

	return nil
}

func waitWithTimeout(waitGroup *sync.WaitGroup, timeout time.Duration) bool {
	workersDone := make(chan struct{})
	go func() {
		defer close(workersDone)
		waitGroup.Wait()
	}()

	select {
	case <-workersDone:
		return false
	case <-time.After(timeout):
		return true
	}
}
