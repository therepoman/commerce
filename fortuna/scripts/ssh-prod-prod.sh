#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile twitch-fortuna-aws prod-commerce-fortuna-env
popd deploy
