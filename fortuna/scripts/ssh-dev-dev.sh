#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile twitch-fortuna-dev dev-commerce-fortuna-env
popd deploy
