#!/bin/bash -e

# Flags
version=$1

# User Config
s3bucket="fortuna-lambdas-$ENVIRONMENT"
region="us-west-2"
lambdadir=./lambdas

version="${version//\/}"

deploy(){
   name=$1

   aws lambda --region us-west-2 update-function-code \
    --function-name $functionName \
    --s3-bucket $s3bucket \
    --s3-key $name/$version/$name.zip \
    --publish
}

deployall() {
    for path in $lambdadir/*; do
        functionName=$(basename $path-$ENVIRONMENT)
        name=$(basename $path)
        echo "-> [$name] building lambda..."

        deploy $name
    done
}

deployall