#!/bin/bash

set -e

ACCOUNT=$1

case "$ACCOUNT" in
        prod)
            ENVIRONMENTS=(prod)
            ;;
        dev)
            ENVIRONMENTS=(dev staging)
            ;;
        *)
            echo "Unknown account \"$ACCOUNT\", exiting"
            exit 1
esac

cd ./terraform/${ACCOUNT}

for ENVIRONMENT in "${ENVIRONMENTS[@]}"
do
    ROLE=`terraform output ${ENVIRONMENT}-iam_role_arn`

    tcs sandstorm role-flags \
        --owner team-commerce \
        --name commerce-${ENVIRONMENT}-fortuna \
        --secret_key "commerce/fortuna/${ENVIRONMENT}/*" \
        --allowed_arn "${ROLE}" \
        --output ./sandstorm.d/${ACCOUNT}_${ENVIRONMENT}.json

done
