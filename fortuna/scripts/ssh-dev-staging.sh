#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile twitch-fortuna-dev staging-commerce-fortuna-env
popd deploy
