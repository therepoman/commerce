package rewards

import (
	"math/rand"
	"strconv"

	"hash/fnv"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/rewards/params"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type ISelector interface {
	Select(metadata campaign.HandlerMetadata, userID string, milestoneParams params.MilestoneParameters) (selectedRewards []*campaign.Reward, milestoneDiscriminant string, err error)
	SelectTriggerRewards(rewards []*campaign.Reward, userID string, selectionAmount int, selectionStrategy string, idempotenceKey string) (selectedRewards []*campaign.Reward, err error)
}

// Selector selects the reward from the reward set
type Selector struct {
	CampaignConfig *campaign.Configuration
}

func NewSelector(campaignConfig *campaign.Configuration) *Selector {
	return &Selector{
		CampaignConfig: campaignConfig,
	}
}

// Select reward(s) from the input set to give to the user
func (s *Selector) Select(metadata campaign.HandlerMetadata, userID string, milestoneParams params.MilestoneParameters) ([]*campaign.Reward, string, error) {
	log.WithFields(log.Fields{
		"userID":   userID,
		"strategy": metadata.SelectionStrategy,
		"index":    metadata.SelectionIndex,
	}).Debug("Selecting rewards for user")

	strategy := metadata.SelectionStrategy

	switch strategy {
	case campaign.AllSelectionStrategy:
		return metadata.Rewards, "", nil
	case campaign.RewardCodeSelectionStrategy:
		return selectByRewardCode(&metadata, milestoneParams.RewardCode)
	case campaign.RandomTrackSelectionStrategy:
		return selectRandomTrackReward(&metadata, userID)
	case campaign.RandomIndependentSelectionStrategy:
		return selectRandomReward(&metadata, milestoneParams.IdempotenceKey)
	}

	return nil, "", errors.Errorf("Unrecognized selection strategy %v", strategy)
}

// SelectTriggerRewards reward(s) from the input set to give to the user
func (s *Selector) SelectTriggerRewards(rewards []*campaign.Reward, userID string, selectionAmount int, selectionStrategy string, idempotenceKey string) (selectedRewards []*campaign.Reward, err error) {
	log.WithFields(log.Fields{
		"userID":   userID,
		"strategy": selectionStrategy,
	}).Debug("Selecting trigger rewards for user")

	// TODO: Add in RewardCodeSelectionStrategy
	switch selectionStrategy {
	case campaign.AllSelectionStrategy:
		return rewards, nil
	case campaign.RandomTrackSelectionStrategy:
		return selectRandomTrackRewardsFromInput(rewards, selectionAmount, userID)
	case campaign.RandomIndependentSelectionStrategy:
		return selectRandomRewardsFromInput(rewards, selectionAmount, idempotenceKey)
	}

	return nil, errors.Errorf("Unrecognized selection strategy %v", rewards)
}

func selectByRewardCode(metadata *campaign.HandlerMetadata, rewardCode string) ([]*campaign.Reward, string, error) {
	// Iterate through all the rewards in a milestone and find a reward whose code matches the given code.
	// Codes need to be unique per reward in an objective.
	for _, reward := range metadata.Rewards {
		if rewardCode == reward.Code {
			return []*campaign.Reward{reward}, rewardCode, nil
		}
	}
	return nil, "", errors.Errorf("Invalid reward code for selection strategy: %s", rewardCode)
}

func selectRandomTrackReward(metadata *campaign.HandlerMetadata, userID string) ([]*campaign.Reward, string, error) {
	// Seed the RNG with the userID to keep the order of reward selection deterministic
	// across milestones for each user
	userIDInt, err := strconv.ParseInt(userID, 10, 64)
	if err != nil {
		return nil, "", errors.Errorf("Failed to parse userID [%v] into integer: %v", userID, err)
	}
	rng := rand.New(rand.NewSource(userIDInt))

	// do userID-based permutation to get index that should be picked for this milestone
	numRewards := len(metadata.Rewards)
	log.Debugf("Randomly picking from %v rewards", numRewards)
	indexPermutation := rng.Perm(numRewards)
	selectedIndex := indexPermutation[metadata.SelectionIndex]

	selectedReward := metadata.Rewards[selectedIndex]

	log.WithFields(log.Fields{
		"userID":   userID,
		"rewardID": selectedReward.ID,
	}).Debug("Reward has been selected for user")

	return []*campaign.Reward{selectedReward}, "", nil
}

func selectRandomTrackRewardsFromInput(rewards []*campaign.Reward, selectionAmount int, userID string) ([]*campaign.Reward, error) {
	// Seed the RNG with the userID to keep the order of reward selection deterministic
	// across milestones for each user
	userIDInt, err := strconv.ParseInt(userID, 10, 64)
	if err != nil {
		return nil, errors.Errorf("Failed to parse userID [%v] into integer: %v", userID, err)
	}
	rng := rand.New(rand.NewSource(userIDInt))

	if selectionAmount <= 0 {
		log.Error("Attempting to select 0 rewards, configuration issue?")
	}

	// do userID-based permutation to get index that should be picked for this milestone
	numRewards := len(rewards)
	log.Debugf("Randomly picking from %v rewards", numRewards)
	indexPermutation := rng.Perm(numRewards)

	selectedRewards := []*campaign.Reward{}
	for i := 0; i < selectionAmount; i++ {
		selectedReward := rewards[indexPermutation[i]]
		selectedRewards = append(selectedRewards, selectedReward)
		log.WithFields(log.Fields{
			"userID":   userID,
			"rewardID": selectedReward.ID,
		}).Debug("Reward has been selected for user")
	}

	return selectedRewards, nil
}

func selectRandomReward(metadata *campaign.HandlerMetadata, idempotenceKey string) ([]*campaign.Reward, string, error) {
	if idempotenceKey == "" {
		return nil, "", errors.New("Idempotence key must be non-empty string when selecting rewards for milestone with strategy RANDOMINDEPENDENT")
	}

	rng := rand.New(rand.NewSource(hashStringToSeed(idempotenceKey)))

	n := len(metadata.Rewards)
	selectedReward := metadata.Rewards[rng.Intn(n)]

	return []*campaign.Reward{selectedReward}, selectedReward.ID, nil
}

func selectRandomRewardsFromInput(rewards []*campaign.Reward, selectionAmount int, idempotenceKey string) ([]*campaign.Reward, error) {
	if idempotenceKey == "" {
		return nil, errors.New("Idempotence key must be non-empty string when selecting rewards with strategy RANDOMINDEPENDENT")
	}

	if selectionAmount <= 0 {
		log.Error("Attempting to select 0 rewards, configuration issue?")
	}

	rng := rand.New(rand.NewSource(hashStringToSeed(idempotenceKey)))
	var selectedRewards []*campaign.Reward

	for i := 0; i < selectionAmount && len(rewards) > 0; i++ {
		n := len(rewards)
		index := rng.Intn(n)

		selectedReward := rewards[index]
		selectedRewards = append(selectedRewards, selectedReward)

		rewards = append(rewards[:index], rewards[index+1:]...)
	}

	return selectedRewards, nil
}

func hashStringToSeed(s string) int64 {
	hasher := fnv.New64a()
	hasher.Write([]byte(s))
	return int64(hasher.Sum64())
}
