package params

type MilestoneParameters struct {
	RewardCode                   string
	IdempotenceKey               string
	SkipEntitlements             bool
	AllowDuplicateEmotes         bool
	ClearEntitlementsBeforeGrant bool
}

type MilestoneParamSetter func(*MilestoneParameters)

func WithRewardCode(rewardCode string) MilestoneParamSetter {
	return func(params *MilestoneParameters) {
		params.RewardCode = rewardCode
	}
}

func WithIdempotenceKey(idempotenceKey string) MilestoneParamSetter {
	return func(params *MilestoneParameters) {
		params.IdempotenceKey = idempotenceKey
	}
}

func SkipEntitlements() MilestoneParamSetter {
	return func(params *MilestoneParameters) {
		params.SkipEntitlements = true
	}
}

func AllowDuplicateEmotes() MilestoneParamSetter {
	return func(params *MilestoneParameters) {
		params.AllowDuplicateEmotes = true
	}
}

// ClearEntitlementsBeforeGrant is a flag to delete milestone entitlements
// during the current granting process. Used for DebugClaimMilestone.
// TODO: Potentially impliment in ClaimRewards
func ClearEntitlementsBeforeGrant() MilestoneParamSetter {
	return func(params *MilestoneParameters) {
		params.ClearEntitlementsBeforeGrant = true
	}
}
