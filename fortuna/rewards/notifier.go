package rewards

import (
	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/pubsub"
	"github.com/pkg/errors"
)

type INotifier interface {
	SendMegaBenefactorPubSub(input BenefactorPubSubInput) error
	SendRecipientPubSub(input RecipientPubSubInput) error
	GetRewardDetailsForPubSub(rewardsToNotify []*campaign.Reward) ([]pubsub.Content, error)
}

type BenefactorPubSubInput struct {
	TransactionID   string
	BenefactorID    string
	TriggerType     string
	TriggerAmount   int32
	NumGiftsGiven   int
	RewardsToNotify []*campaign.Reward
	ChannelID       string
	Domain          string
}

type RecipientPubSubInput struct {
	TransactionID   string
	RecipientID     string
	BenefactorLogin string
	RewardsToNotify []*campaign.Reward
	ChannelID       string
	Domain          string
	TriggerType     string
}

type Notifier struct {
	Clients *clients.Clients
}

// SendMegaBenefactorPubSub sends the notification of rewards gained through a mega cheer to the benefactor via PubSub client
func (n *Notifier) SendMegaBenefactorPubSub(input BenefactorPubSubInput) error {
	contents, err := n.GetRewardDetailsForPubSub(input.RewardsToNotify)
	if err != nil {
		return errors.Wrapf(err, "Notifier Failed to look up benefactor reward details PubSub notification - benefactorID: %s", input.BenefactorID)
	}

	data := pubsub.BenefactorReward{
		TransactionID: input.TransactionID,
		UserID:        input.BenefactorID,
		Benefactor:    input.BenefactorID, // Pubsub listener is expecting this field to exist
		TriggerType:   input.TriggerType,
		TriggerAmount: input.TriggerAmount,
		Contents:      contents,
		ChannelID:     input.ChannelID,
		Domain:        input.Domain,
	}
	if len(data.Domain) == 0 && len(input.RewardsToNotify) > 0 {
		data.Domain = input.RewardsToNotify[0].Domain
	}

	err = n.Clients.PubSubClient.Publish(pubsub.MegaCheerRewardType, data, pubsub.UserCampaignEventsTopic, input.BenefactorID)
	if err != nil {
		return errors.Wrapf(err, "Notifier Failed to publish benefactor reward PubSub notification - benefactorID: %s", input.BenefactorID)
	}

	return nil
}

// SendRecipientPubSub sends the notification of rewards to the recipient via PubSub client
func (n *Notifier) SendRecipientPubSub(input RecipientPubSubInput) error {
	contents, err := n.GetRewardDetailsForPubSub(input.RewardsToNotify)
	if err != nil {
		return errors.Wrapf(err, "Notifier Failed to look up recipient reward details PubSub notification - recipientID: %s", input.RecipientID)
	}

	data := pubsub.Reward{
		TransactionID: input.TransactionID,
		UserID:        input.RecipientID,
		Benefactor:    input.BenefactorLogin,
		Contents:      contents,
		ChannelID:     input.ChannelID,
		Domain:        input.Domain,
		TriggerType:   input.TriggerType,
	}

	if len(data.Domain) == 0 && len(input.RewardsToNotify) > 0 {
		data.Domain = input.RewardsToNotify[0].Domain
	}

	if err = n.Clients.PubSubClient.Publish(pubsub.GiftEventType, data, pubsub.UserCampaignEventsTopic, input.RecipientID); err != nil {
		return errors.Wrapf(err, "Notifier Failed to publish recipient reward PubSub notification. recipientID: %s", input.RecipientID)
	}

	return nil
}

// GetRewardDetailsForPubSub builds the PubSub message content with rewards details
// Currently it retrieves details for Emotes and IGC rewards, and ignores the rest
func (n *Notifier) GetRewardDetailsForPubSub(rewardsToNotify []*campaign.Reward) ([]pubsub.Content, error) {
	if len(rewardsToNotify) == 0 {
		return []pubsub.Content{}, nil
	}

	contents := make([]pubsub.Content, 0, len(rewardsToNotify))
	for _, reward := range rewardsToNotify {
		contents = append(contents, pubsub.Content{
			Type:     reward.Type,
			ID:       reward.ID,
			Text:     reward.Name,
			ImageURL: GenerateFallbackEmoteImageURL(reward.ID),
			Quantity: 1,
		})
	}

	return contents, nil
}
