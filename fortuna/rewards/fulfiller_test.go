package rewards_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/mocks"
	spadeMock "code.justin.tv/commerce/fortuna/mocks/spade"
	"code.justin.tv/commerce/fortuna/rewards"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	otherDomain    = "hgc"
	testUserID     = "u1"
	testDomain     = "owl2018"
	testTriggerID1 = "t1"
	testChannelID  = "testChannel"
)

// mock API constants
const (
	rewardDaoUpdateReward          = "UpdateReward"
	rewardDaoThrottledUpdateReward = "ThrottledUpdateReward"
)

func TestFulfiller(t *testing.T) {
	Convey("TestFulfiller", t, func() {
		config := &config.Configuration{}

		campaignConfig := &campaign.Configuration{}

		mockSNSClient := new(mocks.ISNSClient)
		mockSQSClient := new(mocks.SQSAPI)
		mockPubSubClient := new(mocks.IPubSubClient)
		mockSpadeClient := new(spadeMock.Client)
		mockMakoClient := new(mocks.IMakoClient)

		clients := &clients.Clients{
			PubSubClient: mockPubSubClient,
			SNSClient:    mockSNSClient,
			SQSClient:    mockSQSClient,
			SpadeClient:  mockSpadeClient,
			MakoClient:   mockMakoClient,
		}

		mockSelector := new(mocks.ISelector)

		mockRewardDao := new(mocks.IRewardDAO)
		mockEligibilityDao := new(mocks.IEligibilityDAO)
		mockClaimableRewardDao := new(mocks.IClaimableRewardDAO)
		mockTriggerDao := new(mocks.ITriggerDAO)
		mockRewardDao.On(rewardDaoUpdateReward, Anything).Return(&dynamo.Reward{}, nil)
		mockRewardDao.On(rewardDaoThrottledUpdateReward, Anything).Return(&dynamo.Reward{}, nil)
		daos := &dynamo.DAOs{
			RewardDAO:          mockRewardDao,
			EligibilityDAO:     mockEligibilityDao,
			ClaimableRewardDAO: mockClaimableRewardDao,
			TriggerDAO:         mockTriggerDao,
		}

		mockUtils := new(mocks.IUtils)
		mockStatter := new(mocks.Statter)

		f := rewards.Fulfiller{
			Config:          config,
			Clients:         clients,
			CampaignConfig:  campaignConfig,
			DAOs:            daos,
			RewardsSelector: mockSelector,
			Utils:           mockUtils,
			Stats:           mockStatter,
		}

		mockSpadeClient.On("TrackEvent", Anything, Anything, Anything).Return(nil)
		mockStatter.On("Inc", Anything, int64(1), float32(1.0)).Return(nil)

		Convey("Test EntitleTrigger", func() {
			Convey("with random strategy ", func() {
				randomTrigger := &campaign.Trigger{
					ID:       "t1",
					Domain:   "r",
					Campaign: &campaign.Campaign{},
					BenefactorRewardAttributes: campaign.RewardAttributes{
						Rewards: []*campaign.Reward{
							{
								Description: "OWLgg",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "1",
								Code:        "OWLgg",
								Name:        "GG",
								Domain:      "r",
							},
							{
								Description: "OWLgg2",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "2",
								Code:        "OWLgg2",
								Name:        "GG2",
								Domain:      "r",
							},
							{
								Description: "OWLgg3",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "3",
								Code:        "OWLgg3",
								Name:        "GG3",
								Domain:      "r",
							},
						},
						SelectionStrategy: "RANDOMINDEPENDENT",
						SelectionAmount:   1,
					},
					RecipientRewardAttributes: campaign.RewardAttributes{
						Rewards: []*campaign.Reward{
							{
								Description: "OWLgg",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "1",
								Code:        "OWLgg",
								Name:        "GG",
								Domain:      "r",
							},
							{
								Description: "OWLgg2",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "2",
								Code:        "OWLgg2",
								Name:        "GG2",
								Domain:      "r",
							},
							{
								Description: "OWLgg3",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "3",
								Code:        "OWLgg3",
								Name:        "GG3",
								Domain:      "r",
							},
						},
						SelectionStrategy: "RANDOMINDEPENDENT",
						SelectionAmount:   1,
					},
				}

				randomMultiTrigger := &campaign.Trigger{
					ID:       "t2",
					Domain:   "r2",
					Campaign: &campaign.Campaign{},
					BenefactorRewardAttributes: campaign.RewardAttributes{
						Rewards: []*campaign.Reward{
							{
								Description: "OWLgg",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "1",
								Code:        "OWLgg",
								Name:        "GG",
								Domain:      "r",
							},
							{
								Description: "OWLgg2",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "2",
								Code:        "OWLgg2",
								Name:        "GG2",
								Domain:      "r",
							},
							{
								Description: "OWLgg3",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "3",
								Code:        "OWLgg3",
								Name:        "GG3",
								Domain:      "r",
							},
						},
						SelectionStrategy: "RANDOMINDEPENDENT",
						SelectionAmount:   2,
					},
					RecipientRewardAttributes: campaign.RewardAttributes{
						Rewards: []*campaign.Reward{
							{
								Description: "OWLgg",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "1",
								Code:        "OWLgg",
								Name:        "GG",
								Domain:      "r",
							},
							{
								Description: "OWLgg2",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "2",
								Code:        "OWLgg2",
								Name:        "GG2",
								Domain:      "r",
							},
							{
								Description: "OWLgg3",
								ImageURL:    "https://static-cdn.jtvnw.net/emoticons/v1/966807/3.0",
								Type:        "emote",
								ID:          "3",
								Code:        "OWLgg3",
								Name:        "GG3",
								Domain:      "r",
							},
						},
						SelectionStrategy: "RANDOMINDEPENDENT",
						SelectionAmount:   1,
					},
				}

				mockClaimableRewardDao.On("AddClaimableReward", Anything).Return(nil)

				mockSNSClient.On("PostToTopic", Anything, Anything).Return(nil)
				mockRewardDao.On("GetUserRewards", Anything, Anything, Anything).Return([]*dynamo.Reward{}, nil)

				Convey("with no previously claimed rewards", func() {
					mockMakoClient.On("GetEntitlementMapByDomain", "r", testUserID).Return(map[string]int32{}, nil)
					mockSNSClient.On("PostToTopicWithMessageAttributes", Anything, Anything, Anything).Return(nil)
					mockUtils.On("SendEmotePubSub", Anything, Anything, Anything).Return(nil)

					Convey("for benefactor", func() {
						mockSelector.On("SelectTriggerRewards", Anything, Anything, 1, Anything, Anything).Return([]*campaign.Reward{randomTrigger.BenefactorRewardAttributes.Rewards[0]}, nil)
						claimableRewards, claimedRewards, err := f.EntitleTrigger(randomTrigger, testUserID, true, 0)
						So(err, ShouldBeNil)
						So(len(claimableRewards), ShouldEqual, 1)
						So(claimableRewards[0].ID, ShouldEqual, "1")
						So(len(claimedRewards), ShouldEqual, 1)

						mockClaimableRewardDao.AssertNumberOfCalls(t, "AddClaimableReward", 1)
					})

					Convey("for benefactor and specific selectionAmount", func() {
						randomTrigger.BenefactorRewardAttributes.SelectionAmountScaled = true
						mockSelector.On("SelectTriggerRewards", Anything, Anything, 3, Anything, Anything).Return(randomTrigger.BenefactorRewardAttributes.Rewards, nil)
						claimableRewards, claimedRewards, err := f.EntitleTrigger(randomTrigger, testUserID, true, 3)
						So(err, ShouldBeNil)
						So(len(claimableRewards), ShouldEqual, 3)
						So(len(claimedRewards), ShouldEqual, 3)

						mockClaimableRewardDao.AssertNumberOfCalls(t, "AddClaimableReward", 3)
					})

					Convey("for recipient", func() {
						mockSelector.On("SelectTriggerRewards", Anything, Anything, 1, Anything, Anything).Return([]*campaign.Reward{randomTrigger.BenefactorRewardAttributes.Rewards[0]}, nil)
						claimableRewards, claimedRewards, err := f.EntitleTrigger(randomTrigger, testUserID, false, 0)
						So(err, ShouldBeNil)
						So(len(claimableRewards), ShouldEqual, 1)
						So(claimableRewards[0].ID, ShouldEqual, "1")
						So(len(claimedRewards), ShouldEqual, 1)

						mockClaimableRewardDao.AssertNumberOfCalls(t, "AddClaimableReward", 1)
					})
				})

				Convey("with no previously claimed rewards and multiple selections", func() {
					mockMakoClient.On("GetEntitlementMapByDomain", "r2", testUserID).Return(map[string]int32{}, nil)
					mockSNSClient.On("PostToTopicWithMessageAttributes", Anything, Anything, Anything).Return(nil)
					mockUtils.On("SendEmotePubSub", Anything, Anything, Anything).Return(nil)

					Convey("for benefactor", func() {
						mockSelector.On("SelectTriggerRewards", Anything, Anything, 2, Anything, Anything).Return([]*campaign.Reward{randomMultiTrigger.BenefactorRewardAttributes.Rewards[0], randomMultiTrigger.BenefactorRewardAttributes.Rewards[1]}, nil)
						claimableRewards, claimedRewards, err := f.EntitleTrigger(randomMultiTrigger, testUserID, true, 0)
						So(err, ShouldBeNil)
						So(len(claimableRewards), ShouldEqual, 2)
						So(claimableRewards[0].ID, ShouldEqual, "1")
						So(claimableRewards[1].ID, ShouldEqual, "2")
						So(len(claimedRewards), ShouldEqual, 2)

						mockClaimableRewardDao.AssertNumberOfCalls(t, "AddClaimableReward", 2)
					})

					Convey("for recipient", func() {
						mockSelector.On("SelectTriggerRewards", Anything, Anything, 1, Anything, Anything).Return([]*campaign.Reward{randomMultiTrigger.BenefactorRewardAttributes.Rewards[0]}, nil)
						claimableRewards, claimedRewards, err := f.EntitleTrigger(randomMultiTrigger, testUserID, false, 0)
						So(err, ShouldBeNil)
						So(len(claimableRewards), ShouldEqual, 1)
						So(claimableRewards[0].ID, ShouldEqual, "1")
						So(len(claimedRewards), ShouldEqual, 1)

						mockClaimableRewardDao.AssertNumberOfCalls(t, "AddClaimableReward", 1)
					})
				})

				Convey("with previously claimed rewards", func() {
					mockMakoClient.On("GetEntitlementMapByDomain", "r", testUserID).Return(map[string]int32{
						"1": 1,
						"3": 1,
					}, nil)
					mockSelector.On("SelectTriggerRewards", []*campaign.Reward{randomTrigger.BenefactorRewardAttributes.Rewards[1]}, Anything, Anything, Anything, Anything).Return([]*campaign.Reward{randomTrigger.BenefactorRewardAttributes.Rewards[1]}, nil)
					mockSNSClient.On("PostToTopicWithMessageAttributes", Anything, Anything, Anything).Return(nil)
					mockUtils.On("SendEmotePubSub", Anything, Anything, Anything).Return(nil)

					claimableRewards, claimedRewards, err := f.EntitleTrigger(randomTrigger, testUserID, true, 0)
					So(err, ShouldBeNil)
					So(len(claimableRewards), ShouldEqual, 1)
					So(claimableRewards[0].ID, ShouldEqual, "2")
					So(len(claimedRewards), ShouldEqual, 1)
					So(claimedRewards[0].ID, ShouldEqual, "2")

					mockClaimableRewardDao.AssertNumberOfCalls(t, "AddClaimableReward", 1)
				})
			})
		})

		Convey("Test HandleTriggerCompletion", func() {
			testRewardID := "testRewardID"
			testRecipientID := "testRecipientID"

			testReward1 := &campaign.Reward{
				ID:       "test-emote-Reward-id",
				Type:     "emote",
				ImageURL: "www.zombo.com",
				Domain:   "Domain",
			}

			testReward2 := &campaign.Reward{
				ID:       "test-emote-Reward-id2",
				Type:     "emote",
				ImageURL: "www.zombo.com",
				Domain:   "Domain",
			}

			testTrigger1 := &campaign.Trigger{
				CampaignId: "campaignID",
				RecipientRewardAttributes: campaign.RewardAttributes{
					Rewards: []*campaign.Reward{testReward1, testReward2},
				},
				Domain: "Domain",
			}

			testClaimMap := map[string]int32{testRewardID: int32(1)}

			Convey("should run completion handler if campaign emote list exists", func() {
				testCampaign := &campaign.Campaign{
					CompleteRewardIDs: map[string]bool{
						testRewardID: true,
					},
				}
				mockUtils.On("GetCampaignByID", Anything).Return(testCampaign, nil)
				mockUtils.On("GetTriggerByID", Anything).Return(testTrigger1, nil)
				mockUtils.On("AddEligibility", testRecipientID, testTrigger1.ID).Return(true, nil)

				expectedCheckCount := 1

				Convey("when eligible", func() {
					testClaimMap = map[string]int32{testRewardID: int32(1)}
				})

				Convey("when ineligible", func() {
					testClaimMap = nil
					expectedCheckCount = 0
				})

				err := f.HandleTriggerCompletion(testTrigger1, testRecipientID, testClaimMap)
				So(err, ShouldBeNil)

				mockUtils.AssertNumberOfCalls(t, "AddEligibility", expectedCheckCount)
			})

			Convey("should error when completion handler eligibility write fails", func() {
				testRewardID := "testRewardID"
				testCampaign := &campaign.Campaign{
					CompleteRewardIDs: map[string]bool{
						testRewardID: true,
					},
				}
				mockUtils.On("GetTriggerByID", Anything).Return(testTrigger1, nil)
				mockUtils.On("GetCampaignByID", Anything).Return(testCampaign, nil)
				mockUtils.On("AddEligibility", testRecipientID, testTrigger1.ID).Return(false, errors.New("woops"))

				err := f.HandleTriggerCompletion(testTrigger1, testRecipientID, testClaimMap)
				So(err, ShouldNotBeNil)

				mockUtils.AssertNumberOfCalls(t, "AddEligibility", 1)
			})
		})

		Convey("Test ClaimTriggerRewards", func() {
			triggerRewards := []*campaign.Reward{
				{
					ID:     "r1",
					Type:   "emote",
					Domain: "d",
				},
				{
					ID:   "r2",
					Type: "emote",
				},
			}
			mockClaimableRewardDao.On("AddClaimableReward", Anything).Return(nil)
			mockRewardDao.On("GetUserRewards", Anything, Anything, Anything).Return([]*dynamo.Reward{}, nil)

			Convey("with no mako entitlements", func() {
				mockMakoClient.On("GetEntitlementMapByDomain", "d", testUserID).Return(map[string]int32{}, nil)
				mockSNSClient.On("PostToTopicWithMessageAttributes", Anything, Anything, Anything).Return(nil)
				mockUtils.On("SendEmotePubSub", Anything, Anything, Anything).Return(nil)
				result, err := f.ClaimTriggerRewards(triggerRewards, testUserID)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 2)
			})

			Convey("with mako entitlements indicating already claimed", func() {
				mockMakoClient.On("GetEntitlementMapByDomain", "d", testUserID).Return(map[string]int32{
					"r1": 1,
					"r2": 1,
				}, nil)
				result, err := f.ClaimTriggerRewards(triggerRewards, testUserID)
				So(err, ShouldBeNil)
				So(len(result), ShouldEqual, 0)
			})

			Convey("with mako error", func() {
				mockMakoClient.On("GetEntitlementMapByDomain", "d", testUserID).Return(nil, errors.New("test"))
				result, err := f.ClaimTriggerRewards(triggerRewards, testUserID)
				So(err, ShouldNotBeNil)
				So(result, ShouldBeNil)
			})
		})

		Convey("Test MarkClaimableReward", func() {
			testReward := &campaign.Reward{
				Type:      "emote",
				ID:        "r1",
				Domain:    "dom",
				TriggerID: "t1",
			}

			Convey("with unnormalized Reward", func() {
				testReward.Domain = ""
				err := f.MarkClaimableReward(testUserID, testReward, false)
				So(err, ShouldNotBeNil)
			})

			Convey("with DAO success", func() {
				mockClaimableRewardDao.On("AddClaimableReward", Anything).Return(nil)
				err := f.MarkClaimableReward(testUserID, testReward, false)
				So(err, ShouldBeNil)
				mockClaimableRewardDao.AssertCalled(t, "AddClaimableReward", Anything)
			})
		})

	})
}

func emptyEntitledEmotesResponse() (map[string]bool, error) {
	userEntitlementMap := map[string]bool{}
	return userEntitlementMap, nil
}
