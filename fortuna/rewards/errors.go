package rewards

import "fmt"

// This error is used when we try to fulfill a reward that requires a connected Battle.Net account, but the user has
// not connected a Battle.Net account yet.
type BattleNetAccountNotConnectedError struct {
	UserID string
}

func (b *BattleNetAccountNotConnectedError) Error() string {
	return fmt.Sprintf("Blizzard Account not linked for user: %v", b.UserID)
}
