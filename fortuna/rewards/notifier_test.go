package rewards_test

import (
	"testing"

	"errors"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/mako"
	"code.justin.tv/commerce/fortuna/clients/pubsub"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/rewards"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const makoBulkGetEmoteSetsByIDs = "BulkGetEmoteSetsByIDs"
const pubsubPublish = "Publish"

func TestNotifier(t *testing.T) {

	rewardsToNotify := []*campaign.Reward{
		{
			ID:       "cool-emote-id",
			Type:     rewards.RewardTypeEmote,
			Name:     "coolEmote",
			ImageURL: "",
		},
		{
			ID:       "cool-emote-id2",
			Type:     rewards.RewardTypeEmote,
			Name:     "coolEmote2",
			ImageURL: "https://static-cdn.jtvnw.net/emoticons/v1/788231/3.0",
		},
	}

	benefactorPubSubInput := rewards.BenefactorPubSubInput{
		BenefactorID:    "benefactorID",
		TriggerType:     "CHEER",
		TriggerAmount:   250,
		NumGiftsGiven:   8,
		RewardsToNotify: rewardsToNotify,
	}

	recipientPubSubInput := rewards.RecipientPubSubInput{
		RecipientID:     "recipientID",
		BenefactorLogin: "benefactorLogin",
		RewardsToNotify: rewardsToNotify,
	}

	makoRewards := []mako.RewardItem{
		{
			ItemType:   "emote",
			ID:         "cool-emote-id",
			Text:       "coolEmote",
			EmoteSetID: "cool-emote-id",
		},
		{
			ItemType:   "emote",
			ID:         "cool-emote-id2",
			Text:       "coolEmote2",
			EmoteSetID: "cool-emote-id2",
		},
	}

	Convey("TestSendMegaBenefactorPubSub", t, func() {
		mockMakoClient := new(mocks.IMakoClient)
		mockPubSubClient := new(mocks.IPubSubClient)

		clients := &clients.Clients{
			MakoClient:   mockMakoClient,
			PubSubClient: mockPubSubClient,
		}

		notifier := rewards.Notifier{
			Clients: clients,
		}

		Convey("it should notify rewards to the benefactor", func() {
			mockPubSubClient.On(pubsubPublish, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := notifier.SendMegaBenefactorPubSub(benefactorPubSubInput)
			So(err, ShouldBeNil)
			mockPubSubClient.AssertNumberOfCalls(t, pubsubPublish, 1)
		})

		Convey("it should fail when the pubsub client fails", func() {
			mockPubSubClient.On(pubsubPublish, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Failed to make PubSub call"))

			err := notifier.SendMegaBenefactorPubSub(benefactorPubSubInput)
			So(err, ShouldNotBeNil)
			mockPubSubClient.AssertNumberOfCalls(t, pubsubPublish, 1)
		})
	})

	Convey("TestSendRecipientPubSub", t, func() {
		mockMakoClient := new(mocks.IMakoClient)
		mockPubSubClient := new(mocks.IPubSubClient)

		clients := &clients.Clients{
			MakoClient:   mockMakoClient,
			PubSubClient: mockPubSubClient,
		}

		notifier := rewards.Notifier{
			Clients: clients,
		}

		Convey("it should notify rewards to the recipient", func() {
			mockPubSubClient.On(pubsubPublish, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := notifier.SendRecipientPubSub(recipientPubSubInput)
			So(err, ShouldBeNil)
			mockPubSubClient.AssertNumberOfCalls(t, pubsubPublish, 1)
		})

		Convey("it should fail when the pubsub client fails", func() {
			mockPubSubClient.On(pubsubPublish, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Failed to make PubSub call"))

			err := notifier.SendRecipientPubSub(recipientPubSubInput)
			So(err, ShouldNotBeNil)
			mockPubSubClient.AssertNumberOfCalls(t, pubsubPublish, 1)
		})
	})

	Convey("TestGetRewardDetailsForPubSub", t, func() {
		notifier := rewards.Notifier{}

		Convey("it should get details for all rewards of supported types", func() {
			expectedContents := []pubsub.Content{
				{
					Type:     makoRewards[0].ItemType,
					ID:       makoRewards[0].ID,
					Text:     makoRewards[0].Text,
					ImageURL: rewards.GenerateFallbackEmoteImageURL(makoRewards[0].ID),
					Quantity: 1,
				},
				{
					Type:     makoRewards[1].ItemType,
					ID:       makoRewards[1].ID,
					Text:     makoRewards[1].Text,
					ImageURL: rewards.GenerateFallbackEmoteImageURL(makoRewards[1].ID),
					Quantity: 1,
				},
			}

			actualContents, err := notifier.GetRewardDetailsForPubSub(rewardsToNotify)
			So(err, ShouldBeNil)
			So(len(actualContents), ShouldEqual, len(rewardsToNotify))
			So(actualContents, ShouldResemble, expectedContents)
		})

		Convey("it should return empty when there is no rewards", func() {
			actualContents, err := notifier.GetRewardDetailsForPubSub([]*campaign.Reward{})
			So(err, ShouldBeNil)
			So(actualContents, ShouldBeEmpty)
		})
	})
}
