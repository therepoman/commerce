package rewards

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/mako"
	"code.justin.tv/commerce/fortuna/clients/sns"
	"code.justin.tv/commerce/fortuna/clients/spade"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	makoGrantValue  = "grant"
	RewardTypeEmote = "emote"
)

type IFulfiller interface {
	// Triggers
	EntitleTrigger(trigger *campaign.Trigger, userID string, isBenefactor bool, overrideSelectionAmount int) ([]*campaign.Reward, []*campaign.Reward, error)
	ClaimTriggerRewards(triggerRewards []*campaign.Reward, userID string) ([]*campaign.Reward, error)
	ClaimMakoReward(input MakoRewardInput) error
}

// Fulfiller validates and grants rewards
type Fulfiller struct {
	Config          *config.Configuration
	CampaignConfig  *campaign.Configuration
	Clients         *clients.Clients
	DAOs            *dynamo.DAOs
	Utils           IUtils
	RewardsSelector ISelector
	Stats           statsd.Statter
}

// EntitleTrigger attempts to claim trigger rewards for the given user
// It passes the right rewards to ClaimRewards, then return its result.
func (f *Fulfiller) EntitleTrigger(trigger *campaign.Trigger, userID string, isBenefactor bool, overrideSelectionAmount int) (claimableRewards []*campaign.Reward, claimedRewards []*campaign.Reward, err error) {
	rewardAttributes := trigger.RecipientRewardAttributes
	if isBenefactor {
		rewardAttributes = trigger.BenefactorRewardAttributes
	}
	rewards := rewardAttributes.Rewards
	selectionStrategy := rewardAttributes.SelectionStrategy
	selectionAmount := rewardAttributes.SelectionAmount
	// Use override selection amount if SelectionAmountScaled is enabled.
	if rewardAttributes.SelectionAmountScaled && overrideSelectionAmount != 0 {
		selectionAmount = overrideSelectionAmount
	}

	// Get the claim map for what rewards the user already has
	claimMap, err := f.Clients.MakoClient.GetEntitlementMapByDomain(trigger.Domain, userID)
	if err != nil {
		return nil, nil, errors.Wrap(err, "Could not get claim map from mako client")
	}

	// Filter out rewards that are claimed and not repeatable
	claimableRewards = []*campaign.Reward{}
	for _, reward := range rewards {
		if claimMap[reward.ID] == 0 {
			claimableRewards = append(claimableRewards, reward)
		}
	}

	// Select the rewards that we should claim from the claimable rewards.
	selectedRewards, err := f.RewardsSelector.SelectTriggerRewards(claimableRewards, userID, selectionAmount, selectionStrategy, createTriggerIdempotenceKey(trigger.ID, userID))
	if err != nil {
		return nil, nil, errors.Wrap(err, "Could not select rewards to be claimed")
	}

	rewardsToBeClaimed := []*campaign.Reward{}
	for _, reward := range selectedRewards {
		f.MarkClaimableReward(userID, reward, !isBenefactor)
		rewardsToBeClaimed = append(rewardsToBeClaimed, reward)
	}

	// Attempt to claim the rewards that should be autogranted.
	claimedRewards, err = f.ClaimRewards(rewardsToBeClaimed, userID, claimMap)
	if err != nil {
		return nil, nil, errors.Wrap(err, "Could not claim all rewards for trigger")
	}

	// Failed to claimed all rewards that should have been autogranted
	if len(claimedRewards) != len(rewardsToBeClaimed) {
		log.WithFields(log.Fields{
			"UserID":       userID,
			"triggerID":    trigger.ID,
			"isBenefactor": isBenefactor,
		}).Errorf("Trigger did not grant all rewards that were selected and should have been autogranted.")
	}

	for _, reward := range claimedRewards {
		claimMap[reward.ID] = reward.GetQuantity()
	}
	if err := f.HandleTriggerCompletion(trigger, userID, claimMap); err != nil {
		return nil, nil, err
	}

	return selectedRewards, claimedRewards, nil
}

// claimEmote grants an emote for Mako
func (f *Fulfiller) claimEmote(userID string, reward *campaign.Reward) error {
	makoInput := MakoRewardInput{
		UserID:       userID,
		Reward:       reward,
		MakoScope:    fmt.Sprintf(mako.MakoEmoteScopeFormat, reward.Domain),
		SpadeType:    spade.TypeEmote,
		SpadeContext: spade.GetRewardContext(reward),
		Domain:       reward.Domain,
		GroupID:      reward.OwnerID,
		OwnerID:      reward.OwnerID,
		StartsAt:     reward.StartsAt,
		EndsAt:       reward.EndsAt,
	}

	err := f.ClaimMakoReward(makoInput)
	if err != nil {
		return errors.Wrap(err, "claimEmote failed")
	}

	f.Stats.Inc("fulfiller.rewardClaim.emote.volume", 1, 1.0)

	return nil
}

// MarkClaimableReward marks a Reward claimable for a user by creating the row in DynamoDB
func (f *Fulfiller) MarkClaimableReward(userID string, reward *campaign.Reward, isGift bool) error {
	if reward.Domain == "" {
		return errors.New("Reward is not denormalized - Domain context is needed")
	}

	scopedID := dynamo.BuildScopedClaimableRewardID(reward.Domain, reward.Type, reward.ID, 0)

	claimableReward := &dynamo.ClaimableReward{
		UserID:         userID,
		ScopedRewardID: scopedID,
		RewardType:     reward.Type,
		RewardID:       reward.ID,
		MilestoneID:    reward.MilestoneID,
		TriggerID:      reward.TriggerID,
		Quantity:       reward.GetQuantity(),
		CreatedTime:    time.Now(),
		IsGift:         isGift,
	}

	if err := f.DAOs.ClaimableRewardDAO.AddClaimableReward(claimableReward); err != nil {
		return errors.Wrap(err, "Could not add claimable Reward record")
	}

	return nil
}

type MakoRewardInput struct {
	UserID       string
	Reward       *campaign.Reward
	MakoScope    string
	SpadeType    spade.SpadeRewardType
	SpadeContext string
	Domain       string
	GroupID      string
	OwnerID      string
	StartsAt     time.Time
	EndsAt       time.Time
}

func (f *Fulfiller) ClaimMakoReward(input MakoRewardInput) error {
	var startTime *timestamp.Timestamp
	var endTime *timestamp.Timestamp
	var err error
	if !input.StartsAt.IsZero() {
		startTime, err = ptypes.TimestampProto(input.StartsAt)

		if err != nil {
			return err
		}
	}

	if !input.EndsAt.IsZero() {
		endTime, err = ptypes.TimestampProto(input.EndsAt)

		if err != nil {
			return err
		}
	}

	event := sns.MakoEvent{
		UserID:   input.UserID,
		Scope:    input.MakoScope,
		Key:      input.Reward.ID,
		Origin:   input.Domain,
		Value:    makoGrantValue,
		Version:  sns.EventVersionMateria,
		Source:   sns.MakoSource,
		GroupID:  input.GroupID,
		OwnerID:  input.OwnerID,
		StartsAt: startTime,
		EndsAt:   endTime,
	}

	eventJSON, err := json.Marshal(event)
	if err != nil {
		return errors.Wrap(err, "Could not marshal Mako event")
	}

	topic := f.Config.MakoEventsArn
	err = f.Clients.SNSClient.PostToTopicWithMessageAttributes(topic, string(eventJSON), sns.MessageAttributesV1)
	if err != nil {
		return errors.Wrap(err, "Failed to post to Mako SNS topic")
	}

	return nil
}

// ClaimTriggerRewards is a wrapper for ClaimRewards that handles the filtering based on mako entitlements
func (f *Fulfiller) ClaimTriggerRewards(triggerRewards []*campaign.Reward, userID string) ([]*campaign.Reward, error) {
	// We do not select rewards with the Reward strategy yet for Triggers.
	// Select rewards based on repeatability and claim map

	if len(triggerRewards) == 0 {
		return []*campaign.Reward{}, nil
	}

	claimMap, err := f.Clients.MakoClient.GetEntitlementMapByDomain(triggerRewards[0].Domain, userID)

	if err != nil {
		return nil, errors.Wrap(err, "Could not get claim map from mako client")
	}

	claimedRewards, err := f.ClaimRewards(triggerRewards, userID, claimMap)
	if err != nil {
		return nil, errors.Wrap(err, "Error while claiming trigger rewards")
	}
	return claimedRewards, nil
}

func (f *Fulfiller) HandleTriggerCompletion(trigger *campaign.Trigger, recipientID string, claimMap map[string]int32) error {
	campaign := trigger.Campaign
	if campaign == nil {
		var err error
		campaign, err = f.Utils.GetCampaignByID(trigger.CampaignId)

		if err != nil {
			log.WithFields(log.Fields{
				"recipientID": recipientID,
			}).WithError(err).Error("MegaCheerGiftFulfillmentHandler could not retrieve campaign")
			return nil
		}
		if campaign == nil {
			log.WithFields(log.Fields{
				"recipientID": recipientID,
			}).Info("MegaCheerGiftFulfillmentHandler trigger does not have campaign object")
			return nil
		}
	}

	if len(campaign.CompleteRewardIDs) == 0 {
		return nil
	}

	for emoteID := range campaign.CompleteRewardIDs {
		if _, ok := claimMap[emoteID]; !ok {
			log.WithFields(log.Fields{
				"recipientID": recipientID,
			}).Info("MegaCheerGiftFulfillmentHandler recipient has not earned all rewards for campaign")
			return nil
		}
	}

	// Recipient has earned all rewards. Mark as eligible
	result, err := f.Utils.AddEligibility(recipientID, campaign.ID)
	if err != nil {
		log.WithFields(log.Fields{
			"triggerID":   trigger.ID,
			"recipientID": recipientID,
		}).WithError(err).Error("MegaCheerGiftFulfillmentHandler failed to set user as eligible for completion")
		return err
	} else if result {
		log.WithFields(log.Fields{
			"UserID":    recipientID,
			"triggerID": trigger.ID,
		}).Info("MegaCheerGiftFulfillmentHandler recipient earned all rewards for campaign")
		// TODO: Add spade event
	}
	return nil
}

// ClaimRewards handles all the business logic for allowing a user to claim a
// group of rewards. This is called by EntitleMilestone if the milestone is set
// to autogrant, and from the ClaimAll api endpoint. ClaimReward is idempotent
// and may be called multiple times for a single Reward/user combination
// This is also called by EntitleTrigger for autogranted rewards.
// ClaimAllRewards also directly calls this with all trigger rewards.
// claimRewardMap argument is the map of Reward ID => claimed quantity in materia
func (f *Fulfiller) ClaimRewards(rewards []*campaign.Reward, userID string, claimedRewardsMap map[string]int32) ([]*campaign.Reward, error) {
	if len(rewards) <= 0 {
		return nil, nil
	}

	claimedRewards := []*campaign.Reward{}
	var multierr *multierror.Error

	for _, reward := range rewards {
		// Check if Reward can be claimed
		if claimedRewardsMap[reward.ID] > 0 {
			// Ignore any rewards that are not available to be claimed
			log.WithFields(log.Fields{
				"UserID":   userID,
				"rewardID": reward.ID,
				"claimed":  claimedRewardsMap[reward.ID],
			}).Info("Reward has already been claimed")
			continue
		}

		// Do claim based on type
		switch reward.Type {
		case RewardTypeEmote:
			err := f.claimEmote(userID, reward)
			if err != nil {
				multierr = multierror.Append(multierr, err)
				continue
			}
		default:
			multierr = multierror.Append(multierr, fmt.Errorf("Could not claim unsupported Reward type - UserID: %v - Reward: %+v", userID, reward))
			continue
		}

		claimedRewards = append(claimedRewards, reward)

		// Save to Reward audit table
		f.createRewardRecordsForUser(reward, userID)
	}

	return claimedRewards, multierr.ErrorOrNil()
}

func (f *Fulfiller) generateRewardRecord(input *dynamo.Reward) {
	// Do it async so we don't block the fulfillment response
	// TODO: it's important that this record gets written since the row count is used to calculate TFS idempotance key.
	// For this reason, we will need to put retry mechanism here. Maybe use https://github.com/avast/retry-go
	go func() {
		log.WithFields(log.Fields{
			"userId":         input.UserID,
			"rewardType":     input.RewardType,
			"rewardId":       input.RewardID,
			"rewardQuantity": input.Quantity,
			"scopedRewardID": input.ScopedRewardID,
		}).Debug("Recording Reward grant for user")

		_, err := f.DAOs.RewardDAO.ThrottledUpdateReward(input)

		if err != nil {
			log.WithFields(log.Fields{
				"UserID":     input.UserID,
				"rewardType": input.RewardType,
				"rewardID":   input.RewardID,
			}).WithError(err).Error("Failed to update Reward table with user Reward grant. Swallowing error and continuing")
		}
	}()
}

// createRewardRecordsForUser is a helper method for creating audit entries in the rewards DynamoDB table.
// count is used to determine what number to append to the DynamoDB ScopedRewardID field in order to differentiate a row from other rows for
// the same Reward / user combo. If multiple entries are to be recorded with one call of this function, then the count serves as the appended
// number for the first recorded row, and the other rows simply increment from this count.
func (f *Fulfiller) createRewardRecordsForUser(reward *campaign.Reward, userID string) {
	inputReward := generateRewardInput(reward, userID)
	f.generateRewardRecord(inputReward)
}

func mapRewardTypeToSpadeType(rewardType string) spade.SpadeRewardType {
	switch rewardType {
	case RewardTypeEmote:
		return spade.TypeBadge
	default:
		log.WithField("Reward.Type", rewardType).WithError(errors.New("Unknown Spade Type")).Error("Tried to map unknown Reward type to spade type")
		return spade.TypeUnknown
	}
}

func generateRewardInput(reward *campaign.Reward, userID string) *dynamo.Reward {
	scopedRewardID := dynamo.BuildScopedRewardID(reward.Type, reward.ID)

	return &dynamo.Reward{
		UserID:              userID,
		ScopedRewardID:      scopedRewardID,
		RewardType:          reward.Type,
		RewardID:            reward.ID,
		Domain:              reward.Domain,
		MilestoneID:         reward.MilestoneID,
		MilestoneType:       reward.ObjectiveTag,
		FulfillmentStrategy: reward.FulfillmentStrategy,
		FulfillmentStatus:   dynamo.FulfillmentStatusPending,
		Quantity:            1,
		TriggerID:           reward.TriggerID,
		CreatedAt:           time.Now().UTC(),
	}
}

func createTriggerIdempotenceKey(triggerID string, userID string) string {
	idempotenceKey := fmt.Sprintf("%s%s%s%s", triggerID, ".", userID, string(time.Now().Unix()))
	return idempotenceKey
}
