package rewards

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/clients/pubsub"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	cache "github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// IUtils provides utility operations for rewards
type IUtils interface {
	SendEmotePubSub(userID string, emoteSetID string, domain string)
	AddEligibility(userID string, id string) (bool, error)
	IsUserEligibleForMilestone(userID string, milestone *campaign.Milestone) (bool, error)
	IsUserEligibleForMilestoneCompletion(userID string, milestone *campaign.Milestone) (bool, error)
	IsPrimeConditionMet(milestone *campaign.Milestone, userID string) (bool, error)
	GetPrimeStatus(userID string) (bool, error)
	GetCampaignConfigForDomain(domain string) (*campaign.Configuration, error)
	GetCampaignConfigForChannelID(channelID string) (*campaign.Configuration, error)
	GetCampaignByID(campaignID string) (*campaign.Campaign, error)
	GetTriggerByID(triggerID string) (*campaign.Trigger, error)
	GetMilestoneByID(milestoneID string) (*campaign.Milestone, error)
	InvalidateCampaignCacheForDomain(domain string) (bool, error)
	InvalidateAllChannelsCampaignsCache() (bool, error)
}

// Utils provides utilities for rewards
type Utils struct {
	Config         *config.Configuration
	CampaignConfig *campaign.Configuration
	CampaignHelper dynamo.CampaignHelper
	Clients        *clients.Clients
	DAOs           *dynamo.DAOs
}

const (
	allChannelsCampaignsCacheKey = "dynamo-campaign-config-all-channels-campaigns"
	milestoneCacheKeyPrefix      = "campaigns.milestone."
	triggerCacheKeyPrefix        = "campaigns.trigger."
	campaignCacheKeyPrefix       = "campaign."
)

var (
	CampaignNotFoundErr = errors.New("Campaign not found.")
)

// SendEmotePubSub is a legacy method for notifying a user about a new emote reward.
// Deprecated: It's probably more appropriate to implement PubSub notifications where needed, or in a separate reusable module,
// than to bundle it with Fulfiller.
func (u *Utils) SendEmotePubSub(userID string, emoteSetID string, domain string) {
	rewardDetails, err := u.Clients.MakoClient.GetEmoteSetByID(emoteSetID)
	if err != nil {
		log.WithFields(log.Fields{
			"userID":     userID,
			"emoteSetID": emoteSetID,
		}).WithError(err).Error("Failed to get emote details to send PubSub message")

		return
	}

	// Send best-effort pubsub message
	data := pubsub.Reward{
		UserID: userID,
		Domain: domain,
		Contents: []pubsub.Content{
			{
				Type:     rewardDetails.ItemType,
				ID:       rewardDetails.ID,
				Text:     rewardDetails.Text,
				ImageURL: GenerateFallbackEmoteImageURL(rewardDetails.ID),
			},
		},
	}
	if err := u.Clients.PubSubClient.Publish(pubsub.RewardType, data, pubsub.UserCampaignEventsTopic, userID); err != nil {
		log.WithFields(log.Fields{
			"userID": userID,
		}).WithError(err).Error("Failed to send Progress PubSub message")
	}
}

// IsUserEligibleForMilestone checks whether the user is eligible or not based on whitelisting / dynamoDB table records / prime status
func (u *Utils) IsUserEligibleForMilestone(userID string, milestone *campaign.Milestone) (bool, error) {
	if !milestone.IsUserWhitelisted(userID) {
		return false, nil
	}

	// If a milestone has the CheckEligibility flag, then having an entry in the milestone eligibility table overrides all other subsequent checks.
	// This is used to support conditions like having to be Prime at the time of cheering / insider pass purchase, but not needing to be Prime when
	// claiming a reward.
	if milestone.CheckEligibility {
		result, err := u.DAOs.EligibilityDAO.IsUserEligibleForMilestone(userID, milestone.ID)
		if err != nil {
			return false, errors.Wrap(err, "failed to get eligibility DynamoDB entry while checking if user is eligible for milestone")
		}

		return result, nil
	}

	// If the milestone does not have the CheckEligibility flag, then the user should be considered eligible if they are
	// eligible for milestone completion.
	return u.IsUserEligibleForMilestoneCompletion(userID, milestone)
}

// IsUserEligibleForMilestoneCompletion checks the user's and milestone's prime and sub status to determine whether the user
// is eligible to complete the milestone
func (u *Utils) IsUserEligibleForMilestoneCompletion(userID string, milestone *campaign.Milestone) (bool, error) {
	if !milestone.IsUserWhitelisted(userID) {
		return false, nil
	}

	primeConditionMet, err := u.IsPrimeConditionMet(milestone, userID)
	if err != nil {
		return false, errors.Wrap(err, "failed to get prime status while checking if user is eligible for milestone")
	}

	requiredSubsConditionMet, err := u.isRequiredSubscriptionsConditionMet(milestone, userID)
	if err != nil {
		return false, errors.Wrap(err, "failed to get subscription status while checking is user is eligible for milestone")
	}

	return primeConditionMet && requiredSubsConditionMet, nil
}

// IsPrimeConditionMet checks whether the milestone is restricted by the user's current prime status
func (u *Utils) IsPrimeConditionMet(milestone *campaign.Milestone, userID string) (bool, error) {
	if !milestone.IsUserWhitelisted(userID) {
		return false, nil
	}

	if milestone.IsPrimeOnly {
		isPrime, err := u.GetPrimeStatus(userID)
		if err != nil {
			log.WithFields(log.Fields{
				"userID":      userID,
				"milestoneID": milestone.ID,
			}).Warn("campaign api could not determine prime status")
			return false, err
		}
		return isPrime, nil
	}
	return true, nil
}

// isRequiredSubscriptionsConditionMet checks whether the milestone is restricted by the user's current subscription status
func (u *Utils) isRequiredSubscriptionsConditionMet(milestone *campaign.Milestone, userID string) (bool, error) {
	subscriptionProductIDs := milestone.RequiredSubs

	if len(subscriptionProductIDs) > 0 {
		resp, err := u.Clients.SubscriptionsClient.GetUserProductSubscriptions(subscriptionProductIDs, userID)
		if err != nil {
			return false, err
		}
		return len(resp.Subscriptions) == len(subscriptionProductIDs), nil
	}

	return true, nil
}

// GetPrimeStatus fetches the prime status from nitro with caching
func (u *Utils) GetPrimeStatus(userID string) (bool, error) {
	const primeStatusCacheKeyPrefix = "is_prime."
	cacheKey := primeStatusCacheKeyPrefix + userID

	cachedStatus, ok := u.Clients.InMemoryCache.Get(cacheKey)
	if ok {
		return cachedStatus.(bool), nil
	}
	isPrime, err := u.Clients.NitroClient.GetPrimeStatus(userID)
	if err != nil {
		return false, err
	}
	u.Clients.InMemoryCache.Set(cacheKey, isPrime, cache.DefaultExpiration)

	return isPrime, nil
}

// GetCampaignConfigForDomain returns the Campaign Config struct based on the the environment setting (either from memory or from dynamo)
func (u *Utils) GetCampaignConfigForDomain(domain string) (*campaign.Configuration, error) {
	if !u.Config.UseDynamoDBForCampaigns {
		return u.CampaignConfig, nil
	}

	cacheKey := GetCampaignCacheKeyForDomain(domain)
	cacheTime := time.Duration(u.Config.CampaignCacheTTLSeconds) * time.Second
	campaignConfig := &campaign.Configuration{}

	if campaignConfig, ok := u.Clients.InMemoryCache.Get(cacheKey); ok {
		return campaignConfig.(*campaign.Configuration), nil
	}

	campaignConfig, err := u.CampaignHelper.GetCampaignConfigForDomain(domain)

	// As a fallback, any error will cause the in-memory data pulled from dynamo to return instead
	if err != nil {
		log.WithError(err).Error("Failed to get data from dynamo. Falling back to in-memory data")
		return u.CampaignConfig, nil
	}

	if err := campaignConfig.DenormalizeData(); err != nil {
		log.WithError(err).Error("Failed to denormalize campaign data from dynamoDB. Falling back to in-memory data")
		return u.CampaignConfig, nil
	}

	u.Clients.InMemoryCache.Set(cacheKey, campaignConfig, cacheTime)

	return campaignConfig, err
}

// GetCampaignConfigForChannelID returns Campaign Config struct by channel id
func (u *Utils) GetCampaignConfigForChannelID(channelID string) (*campaign.Configuration, error) {
	if !u.Config.UseDynamoDBForCampaigns {
		return u.CampaignConfig, nil
	}

	res, err := u.DAOs.ChannelPropertiesDAO.GetChannelProperties(channelID)
	if err != nil {
		// Fallback to in-memory data
		log.WithField("channelID", channelID).WithError(err).Error("Failed to get channel properties. Falling back to in-memory data")
		return u.CampaignConfig, nil
	}

	var campaignConfig *campaign.Configuration

	if len(res.Domains) > 0 {
		domain := res.Domains[0]
		campaignConfig, err = u.GetCampaignConfigForDomain(domain)

		if err != nil {
			return nil, err
		}
	} else {
		campaignConfig = &campaign.Configuration{
			Campaigns: map[string][]*campaign.Campaign{},
		}
	}

	// Append campaigns intended for all channels
	allChannelsCampaigns, err := u.getCachedAllChannelCampaigns()

	if err != nil {
		// Error should not fail the process. Instead do not append the all channels campaigns
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).WithError(err).Error("Failed to get cached all channels campaigns. Falling back to in-memory data")
		// For now, fallback entirely to in-memory data
		return u.CampaignConfig, nil
	}

	for _, allChannelCampaign := range allChannelsCampaigns {
		if _, ok := campaignConfig.Campaigns[allChannelCampaign.Domain]; !ok {
			campaignConfig.Campaigns[allChannelCampaign.Domain] = []*campaign.Campaign{}
		}

		// See if this campaign already exists in the domain bucket. Only append if it isn't
		alreadyExists := false
		for _, channelCampaign := range campaignConfig.Campaigns[allChannelCampaign.Domain] {
			if channelCampaign.ID == allChannelCampaign.ID {
				alreadyExists = true
			}
		}
		if !alreadyExists {
			campaignConfig.Campaigns[allChannelCampaign.Domain] = append(campaignConfig.Campaigns[allChannelCampaign.Domain], allChannelCampaign)
		}
	}

	if err := campaignConfig.DenormalizeData(); err != nil {
		log.WithError(err).Error("Failed to denormalize campaign data from DynamoDB. Falling back to in-memory data")
		return u.CampaignConfig, nil
	}

	return campaignConfig, nil
}

// GetCampaignByID returns a campaign based on the ID
func (u *Utils) GetCampaignByID(campaignID string) (*campaign.Campaign, error) {
	if !u.Config.UseDynamoDBForCampaigns {
		return u.getCampaignByIDFromMemory(campaignID)
	}

	cacheKey := campaignCacheKeyPrefix + campaignID
	cachedCampaign, cacheHit := u.Clients.InMemoryCache.Get(cacheKey)
	if cacheHit {
		if cachedCampaign == nil {
			return nil, CampaignNotFoundErr
		}
		campaign := cachedCampaign.(*campaign.Campaign)
		return campaign, nil
	}

	result, err := u.CampaignHelper.GetCampaignWithChildDataByID(campaignID)

	if err != nil && err != dynamo.ItemNotFoundErr {
		log.WithError(err).Error("Failed to get data from dynamo. Falling back to in-memory data")
		// As a fallback, the in-memory data pulled from S3 to return instead
		return u.getCampaignByIDFromMemory(campaignID)
	} else if err == dynamo.ItemNotFoundErr || result == nil {
		u.Clients.InMemoryCache.Set(cacheKey, nil, cache.DefaultExpiration)
		return nil, CampaignNotFoundErr
	}

	if err := campaign.DenormalizeCampaign(result); err != nil {
		log.WithError(err).Error("Failed to denormalize campaign from DynamoDB. Falling back to in-memory data")
		return u.getCampaignByIDFromMemory(campaignID)
	}

	u.Clients.InMemoryCache.Set(cacheKey, result, cache.DefaultExpiration)

	return result, nil
}

// GetTriggerByID returns a trigger configuration based on the ID
func (u *Utils) GetTriggerByID(triggerID string) (*campaign.Trigger, error) {
	if !u.Config.UseDynamoDBForCampaigns {
		return u.getTriggerByIDFromMemory(triggerID)
	}

	cacheKey := triggerCacheKeyPrefix + triggerID
	cachedTrigger, ok := u.Clients.InMemoryCache.Get(cacheKey)
	if ok {
		trigger := cachedTrigger.(*campaign.Trigger)
		return trigger, nil
	}

	trigger, err := u.DAOs.TriggerDAO.GetTriggerByID(triggerID)

	if err != nil {
		log.WithField("triggerID", triggerID).WithError(err).Error("Failed to get Trigger from DynamoDB. Falling back to in-memory data")
		return u.getTriggerByIDFromMemory(triggerID)
	}

	if trigger == nil {
		return nil, errors.New("No such Trigger in DynamoDB")
	}

	campaign.DenormalizeTriggerRewards(trigger)
	u.Clients.InMemoryCache.Set(cacheKey, trigger, cache.DefaultExpiration)

	return trigger, nil
}

// GetMilestoneByID returns a milestone configuration based on the ID
func (u *Utils) GetMilestoneByID(milestoneID string) (*campaign.Milestone, error) {
	if !u.Config.UseDynamoDBForCampaigns {
		return u.getMilestoneByIDFromMemory(milestoneID)
	}

	cacheKey := milestoneCacheKeyPrefix + milestoneID
	cachedMilestone, ok := u.Clients.InMemoryCache.Get(cacheKey)
	if ok {
		milestone := cachedMilestone.(*campaign.Milestone)
		return milestone, nil
	}

	milestone, err := u.DAOs.MilestoneDAO.GetMilestoneByID(milestoneID)

	if err != nil {
		log.WithField("milestoneID", milestoneID).WithError(err).Error("Failed to get milestone from DynamoDB. Falling back to in-memory data")
		return u.getMilestoneByIDFromMemory(milestoneID)
	}

	if milestone == nil {
		return nil, errors.New("No such Milestone in DynamoDB")
	}

	parentObjective, err := u.DAOs.ObjectiveDAO.GetObjectiveByID(milestone.ObjectiveID)
	if err != nil {
		log.WithField("objectiveID", milestone.ObjectiveID).WithError(err).Error("Failed to get parent objective from DynamoDB. Falling back to in-memory data")
		return u.getMilestoneByIDFromMemory(milestoneID)
	}

	// Denormalize from parent (it is safe to denormalize before caching since there is no circular reference)
	campaign.DenormalizeMilestoneWithParentData(milestone, parentObjective)
	u.Clients.InMemoryCache.Set(cacheKey, milestone, cache.DefaultExpiration)

	return milestone, nil
}

func (u *Utils) getTriggerByIDFromMemory(triggerID string) (*campaign.Trigger, error) {
	trigger, ok := u.CampaignConfig.Triggers[triggerID]
	if !ok {
		return nil, errors.New("Trigger does not exist in memory")
	}
	return trigger, nil
}

func (u *Utils) getCampaignByIDFromMemory(campaignID string) (*campaign.Campaign, error) {
	campaign, ok := u.CampaignConfig.CampaignsByID[campaignID]
	if !ok {
		return nil, CampaignNotFoundErr
	}
	return campaign, nil
}

func (u *Utils) getMilestoneByIDFromMemory(milestoneID string) (*campaign.Milestone, error) {
	milestone, ok := u.CampaignConfig.Milestones[milestoneID]
	if !ok {
		return nil, errors.New("Milestone does not exist in memory")
	}
	return milestone, nil
}

// InvalidateCampaignCacheForDomain deletes the campaign cache for the specified domain and returns
// bool representing whether the key was there or not and error
func (u *Utils) InvalidateCampaignCacheForDomain(domain string) (bool, error) {
	cacheKey := GetCampaignCacheKeyForDomain(domain)
	u.Clients.InMemoryCache.Delete(cacheKey) // Does nothing if key is not in cache
	return true, nil
}

// InvalidateAllChannelsCampaignsCache deletes the cache that stores the list of campaigns with AllChannels set to true
func (u *Utils) InvalidateAllChannelsCampaignsCache() (bool, error) {
	u.Clients.InMemoryCache.Delete(allChannelsCampaignsCacheKey)
	return true, nil
}

func (u *Utils) getCachedAllChannelCampaigns() ([]*campaign.Campaign, error) {
	cacheTime := time.Duration(u.Config.CampaignAllChannelsCacheTTLSeconds) * time.Second
	campaigns := []*campaign.Campaign{}

	if campaigns, ok := u.Clients.InMemoryCache.Get(allChannelsCampaignsCacheKey); ok {
		return campaigns.([]*campaign.Campaign), nil
	}

	campaigns, err := u.CampaignHelper.GetAllChannelsCampaigns()

	if err != nil {
		return nil, errors.Wrap(err, "Failed to get campaign data from dynamo")
	}

	u.Clients.InMemoryCache.Set(allChannelsCampaignsCacheKey, campaigns, cacheTime)

	return campaigns, nil
}

// GetProductID gets the unique id combining milestone ID and reward code
func GetProductID(milestoneID string, rewardCode string) string {
	if rewardCode != "" {
		return milestoneID + "." + rewardCode
	}
	return milestoneID
}

func GenerateFallbackEmoteImageURL(emoteID string) string {
	return fmt.Sprintf("https://static-cdn.jtvnw.net/emoticons/v1/%s/3.0", emoteID)
}

// GetCampaignCacheKeyForDomain returns a cache key used for caching assembled campaign data
func GetCampaignCacheKeyForDomain(domain string) string {
	return "dynamo-campaign-config." + domain
}

func (u *Utils) AddEligibility(userID string, id string) (bool, error) {
	_, err := u.DAOs.EligibilityDAO.AddEligibility(userID, id)
	if err != nil {
		return false, err
	}
	return true, nil
}
