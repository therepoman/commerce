package rewards_test

import (
	"testing"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/rewards"

	"code.justin.tv/commerce/fortuna/rewards/params"
	. "github.com/smartystreets/goconvey/convey"
)

var (
	rewardIDs = []string{"reward1", "reward2", "reward3"}
)

const (
	defaultUserID = "12345"
)

func TestSelector(t *testing.T) {
	Convey("TestSelector", t, func() {
		selector := rewards.Selector{
			CampaignConfig: &campaign.Configuration{},
		}

		Convey("Test Select", func() {
			Convey("with invalid selection strategy", func() {
				metadata := generateMetadata("not-a-real-strategy", 1)

				_, _, err := selector.Select(metadata, defaultUserID, params.MilestoneParameters{})
				So(err, ShouldNotBeNil)
			})

			Convey("with ALL selection strategy", func() {
				metadata := generateMetadata(campaign.AllSelectionStrategy, 1)

				rewards, _, err := selector.Select(metadata, defaultUserID, params.MilestoneParameters{})
				So(err, ShouldBeNil)
				So(len(rewards), ShouldEqual, 3)
			})

			Convey("with RANDOM selection strategy ", func() {

				Convey("with emote reward types", func() {
					Convey("with a bad userID", func() {
						metadata := generateMetadata(campaign.RandomTrackSelectionStrategy, 0)

						_, _, err := selector.Select(metadata, "not-a-real-user-id", params.MilestoneParameters{})
						So(err, ShouldNotBeNil)
					})

					Convey("with multiple good userIDs", func() {

						// runs with different users should return different reward sequences for each user
						user1Rewards := runSelectionForUser(&selector, "1234")
						user2Rewards := runSelectionForUser(&selector, "9999")
						user3Rewards := runSelectionForUser(&selector, "3456")

						So(areRewardListIdentical(user1Rewards, user2Rewards), ShouldBeFalse)
						So(areRewardListIdentical(user1Rewards, user3Rewards), ShouldBeFalse)
						So(areRewardListIdentical(user3Rewards, user2Rewards), ShouldBeFalse)
					})

					Convey("with multiple runs of 1 good userID", func() {

						// multiple runs with the same user should return the same reward sequence every time
						user1Rewards := runSelectionForUser(&selector, "1234")
						user2Rewards := runSelectionForUser(&selector, "1234")
						user3Rewards := runSelectionForUser(&selector, "1234")

						So(areRewardListIdentical(user1Rewards, user2Rewards), ShouldBeTrue)
						So(areRewardListIdentical(user1Rewards, user3Rewards), ShouldBeTrue)
						So(areRewardListIdentical(user3Rewards, user2Rewards), ShouldBeTrue)
					})
				})
			})

			Convey("with CODE selection strategy", func() {
				metadata := generateMetadata(campaign.RewardCodeSelectionStrategy, 0)
				selectedRewards, milestoneDiscriminant, err := selector.Select(metadata, "123", params.MilestoneParameters{
					RewardCode: "OWLSEO",
				})
				So(err, ShouldBeNil)
				So(len(selectedRewards), ShouldEqual, 1)
				So(selectedRewards[0].Name, ShouldEqual, "OWLSEO")
				So(milestoneDiscriminant, ShouldEqual, "OWLSEO")

				Convey("with invalid name", func() {
					selectedRewards, _, err := selector.Select(metadata, "123", params.MilestoneParameters{
						RewardCode: "KAPPA",
					})
					So(err, ShouldNotBeNil)
					So(len(selectedRewards), ShouldEqual, 0)
				})
			})

			Convey("with RANDOMINDEPENDENT selection strategy", func() {
				metadata := generateMetadata(campaign.RandomIndependentSelectionStrategy, 0)

				res, milestoneDiscriminant, err := selector.Select(metadata, defaultUserID, params.MilestoneParameters{
					IdempotenceKey: "hi",
				})
				So(err, ShouldBeNil)
				So(len(res), ShouldEqual, 1)
				So(milestoneDiscriminant, ShouldNotEqual, "")

				timesToHandle := 10
				for i := 0; i < timesToHandle; i++ {
					res, sameDiscriminant, err := selector.Select(metadata, defaultUserID, params.MilestoneParameters{
						IdempotenceKey: "hi",
					})
					So(err, ShouldBeNil)
					So(len(res), ShouldEqual, 1)
					So(sameDiscriminant, ShouldEqual, milestoneDiscriminant)
				}
			})
		})

		Convey("Test SelectTriggerRewards", func() {
			Convey("with invalid selection strategy", func() {
				metadata := generateMetadata("not-a-real-strategy", 1)

				_, _, err := selector.Select(metadata, defaultUserID, params.MilestoneParameters{})
				So(err, ShouldNotBeNil)
			})

			Convey("with ALL selection strategy", func() {
				metadata := generateMetadata(campaign.AllSelectionStrategy, 1)

				rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 0, "ALL", "hi")
				So(err, ShouldBeNil)
				So(len(rewards), ShouldEqual, 3)
			})

			Convey("with RANDOM selection strategy select 1 reward", func() {
				metadata := generateMetadata(campaign.RandomTrackSelectionStrategy, 0)

				// multiple runs with the same user should return the same reward sequence every time
				user1Rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 1, metadata.SelectionStrategy, "hi")
				user2Rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 1, metadata.SelectionStrategy, "hi")
				user3Rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 1, metadata.SelectionStrategy, "hi")
				So(err, ShouldBeNil)
				So(len(user1Rewards), ShouldEqual, 1)

				So(areRewardListIdentical(user1Rewards, user2Rewards), ShouldBeTrue)
				So(areRewardListIdentical(user1Rewards, user3Rewards), ShouldBeTrue)
				So(areRewardListIdentical(user3Rewards, user2Rewards), ShouldBeTrue)
			})

			Convey("with RANDOM selection strategy select multiple reward", func() {
				metadata := generateMetadata(campaign.RandomTrackSelectionStrategy, 0)

				// multiple runs with the same user should return the same reward sequence every time
				user1Rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 3, metadata.SelectionStrategy, "hi")
				user2Rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 3, metadata.SelectionStrategy, "hi")
				user3Rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 3, metadata.SelectionStrategy, "hi")
				So(err, ShouldBeNil)
				So(len(user1Rewards), ShouldEqual, 3)

				So(areRewardListIdentical(user1Rewards, user2Rewards), ShouldBeTrue)
				So(areRewardListIdentical(user1Rewards, user3Rewards), ShouldBeTrue)
				So(areRewardListIdentical(user3Rewards, user2Rewards), ShouldBeTrue)
			})

			Convey("with RANDOMINDEPENDENT selection strategy select 1 reward", func() {
				metadata := generateMetadata(campaign.RandomIndependentSelectionStrategy, 0)

				rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 1, metadata.SelectionStrategy, "hi")
				So(err, ShouldBeNil)
				So(len(rewards), ShouldEqual, 1)
			})

			Convey("with RANDOMINDEPENDENT selection strategy select multiple rewards", func() {
				metadata := generateMetadata(campaign.RandomIndependentSelectionStrategy, 0)

				rewards, err := selector.SelectTriggerRewards(metadata.Rewards, defaultUserID, 2, metadata.SelectionStrategy, "hi")
				So(err, ShouldBeNil)
				So(len(rewards), ShouldEqual, 2)
				So(rewards[0], ShouldNotEqual, rewards[1])
			})
		})
	})
}

func generateMetadata(selectionStrategy string, selectionIndex int) campaign.HandlerMetadata {
	return campaign.HandlerMetadata{
		Rewards: []*campaign.Reward{
			{
				ID:   rewardIDs[0],
				Type: rewards.RewardTypeEmote,
				Name: "OWLSEO",
				Code: "OWLSEO",
			},
			{
				ID:   rewardIDs[1],
				Type: rewards.RewardTypeEmote,
				Name: "OWLDAL",
				Code: "OWLDAL",
			},
			{
				ID:   rewardIDs[2],
				Type: rewards.RewardTypeEmote,
				Name: "POGCHAMP",
				Code: "POGCHAMP",
			},
		},
		SelectionIndex:    selectionIndex,
		SelectionStrategy: selectionStrategy,
	}
}

func areRewardListIdentical(list1 []*campaign.Reward, list2 []*campaign.Reward) bool {
	for i := range list1 {
		if list1[i].ID != list2[i].ID {
			return false
		}
	}
	return true
}

func runSelectionForUser(selector rewards.ISelector, userID string) []*campaign.Reward {
	// pull for first milestone
	firstMetadata := generateMetadata(campaign.RandomTrackSelectionStrategy, 0)
	firstRewards, _, err := selector.Select(firstMetadata, userID, params.MilestoneParameters{})
	So(err, ShouldBeNil)
	So(len(firstRewards), ShouldEqual, 1)
	So(rewardIDs, ShouldContain, firstRewards[0].ID)

	// pull for second milestone
	secondMetadata := generateMetadata(campaign.RandomTrackSelectionStrategy, 1)
	secondRewards, _, err := selector.Select(secondMetadata, userID, params.MilestoneParameters{})
	So(err, ShouldBeNil)
	So(len(secondRewards), ShouldEqual, 1)
	So(rewardIDs, ShouldContain, firstRewards[0].ID)

	// pull for third milestone
	thirdMetadata := generateMetadata(campaign.RandomTrackSelectionStrategy, 2)
	thirdRewards, _, err := selector.Select(thirdMetadata, userID, params.MilestoneParameters{})
	So(err, ShouldBeNil)
	So(len(thirdRewards), ShouldEqual, 1)
	So(rewardIDs, ShouldContain, firstRewards[0].ID)

	// verify no duplicates
	So(firstRewards[0].ID, ShouldNotEqual, secondRewards[0].ID)
	So(firstRewards[0].ID, ShouldNotEqual, thirdRewards[0].ID)
	So(thirdRewards[0].ID, ShouldNotEqual, secondRewards[0].ID)

	return []*campaign.Reward{
		firstRewards[0],
		secondRewards[0],
		thirdRewards[0],
	}
}
