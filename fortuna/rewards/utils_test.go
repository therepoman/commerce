package rewards_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/fortuna/campaign"
	"code.justin.tv/commerce/fortuna/clients"
	"code.justin.tv/commerce/fortuna/config"
	"code.justin.tv/commerce/fortuna/dynamo"
	"code.justin.tv/commerce/fortuna/mocks"
	"code.justin.tv/commerce/fortuna/rewards"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	cache "github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const testMilestoneID = "testMilestoneID"

func TestFulfillerUtils(t *testing.T) {
	Convey("Test FulfillerUtils", t, func() {
		mockEligibilityDao := new(mocks.IEligibilityDAO)
		mockChannelPropertiesDao := new(mocks.IChannelPropertiesDAO)
		mockCampaignDao := new(mocks.ICampaignDAO)
		mockObjectiveDao := new(mocks.IObjectiveDAO)
		mockMilestoneDao := new(mocks.IMilestoneDAO)
		mockSubsClient := new(mocks.ISubscriptionsClient)
		mockTriggerDao := new(mocks.ITriggerDAO)
		daos := &dynamo.DAOs{
			EligibilityDAO:       mockEligibilityDao,
			ChannelPropertiesDAO: mockChannelPropertiesDao,
			CampaignDAO:          mockCampaignDao,
			ObjectiveDAO:         mockObjectiveDao,
			MilestoneDAO:         mockMilestoneDao,
			TriggerDAO:           mockTriggerDao,
		}

		mockNitroClient := new(mocks.INitroClient)
		mockCampaignHelper := new(mocks.CampaignHelper)
		cacheClient := cache.New(1*time.Minute, 10*time.Minute)
		clients := &clients.Clients{
			InMemoryCache:       cacheClient,
			NitroClient:         mockNitroClient,
			SubscriptionsClient: mockSubsClient,
		}

		milestoneInstance := campaign.Milestone{
			ID: "m1",
			HandlerMetadata: campaign.HandlerMetadata{
				MilestoneTriggers: []string{
					"m2",
					"m3",
					"m4",
				},
			},
		}

		milestoneInstance2 := campaign.Milestone{
			ID: "m2",
			HandlerMetadata: campaign.HandlerMetadata{
				MilestoneTriggers: []string{
					"m1",
					"m3",
					"m5",
				},
			},
		}

		objectiveInstance := campaign.Objective{
			ID:         "o1",
			Tag:        campaign.ObjectiveTagCollection,
			Milestones: []*campaign.Milestone{&milestoneInstance, &milestoneInstance2},
		}

		triggerInstance := campaign.Trigger{
			ID: "t1",
		}

		testDomain := "domain1"
		testCampaign := "campaign1"
		campaignInstance := campaign.Campaign{
			ID:         testCampaign,
			Objectives: []*campaign.Objective{&objectiveInstance},
			Triggers:   []*campaign.Trigger{&triggerInstance},
		}

		campaignSlice := []*campaign.Campaign{&campaignInstance}

		campaignConfig := campaign.Configuration{
			Campaigns: map[string][]*campaign.Campaign{testDomain: campaignSlice},
		}

		campaignConfig.DenormalizeData()

		u := rewards.Utils{
			CampaignConfig: &campaignConfig,
			CampaignHelper: mockCampaignHelper,
			Clients:        clients,
			DAOs:           daos,
		}

		eligibilityTestMilestone := campaign.Milestone{
			ID:                     testMilestoneID,
			Threshold:              int64(200),
			ParticipationThreshold: int64(100),
			ActiveStartDate:        time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC),
			ActiveEndDate:          time.Date(2099, time.February, 7, 9, 0, 0, 0, time.UTC),
			CheckEligibility:       true,
			HandlerMetadata: campaign.HandlerMetadata{
				Rewards: []*campaign.Reward{},
			},
			UserWhiteListEnabled: false,
			UserWhitelist:        map[string]bool{"u1": true},
		}

		nonEligibilityTestMilestone := campaign.Milestone{
			ID:                     testMilestoneID,
			Threshold:              int64(200),
			ParticipationThreshold: int64(100),
			CheckEligibility:       false,
			ActiveStartDate:        time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC),
			ActiveEndDate:          time.Date(2099, time.February, 7, 9, 0, 0, 0, time.UTC),
			HandlerMetadata: campaign.HandlerMetadata{
				Rewards: []*campaign.Reward{},
			},
		}

		Convey("Test IsUserEligibleForMilestone", func() {
			Convey("with eligibility", func() {
				mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(true, nil)
				result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
				So(err, ShouldBeNil)
				So(result, ShouldBeTrue)
			})

			Convey("with no eligibility", func() {
				mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(false, nil)
				result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
				So(err, ShouldBeNil)
				So(result, ShouldBeFalse)
			})

			Convey("with non-eligibility-checking milestone", func() {
				milestone := &campaign.Milestone{
					ID:                     testMilestoneID,
					Threshold:              int64(200),
					ParticipationThreshold: int64(100),
					CheckEligibility:       false,
					ActiveStartDate:        time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC),
					ActiveEndDate:          time.Date(2099, time.February, 7, 9, 0, 0, 0, time.UTC),
					HandlerMetadata: campaign.HandlerMetadata{
						Rewards: []*campaign.Reward{},
					},
				}
				result, err := u.IsUserEligibleForMilestone("u1", milestone)
				So(err, ShouldBeNil)
				So(result, ShouldBeTrue)
				mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
			})

			Convey("with dao error", func() {
				mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(false, errors.New("error"))
				result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
				So(err, ShouldNotBeNil)
				So(result, ShouldBeFalse)
			})

			Convey("with whitelisting", func() {
				eligibilityTestMilestone.UserWhiteListEnabled = true
				Convey("and whitelisted user", func() {
					mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(true, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
				})

				Convey("and non-whitelisted user", func() {
					result, err := u.IsUserEligibleForMilestone("u2", &eligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})
			})

			Convey("with prime only milestone, no eligibility check", func() {
				nonEligibilityTestMilestone.IsPrimeOnly = true
				Convey("and non-prime user", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(false, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockNitroClient.AssertNumberOfCalls(t, "GetPrimeStatus", 1)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})

				Convey("and prime user", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(true, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
					mockNitroClient.AssertNumberOfCalls(t, "GetPrimeStatus", 1)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})
			})

			Convey("with prime only milestone, with eligibility check", func() {
				eligibilityTestMilestone.IsPrimeOnly = true
				Convey("and non-prime user", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(false, nil)
					Convey("and eligible user", func() {
						mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(true, nil)
						result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
						So(err, ShouldBeNil)
						So(result, ShouldBeTrue)
						mockNitroClient.AssertNotCalled(t, "GetPrimeStatus")
						mockEligibilityDao.AssertNumberOfCalls(t, "IsUserEligibleForMilestone", 1)
					})

					Convey("and ineligible user", func() {
						mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(false, nil)
						result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
						So(err, ShouldBeNil)
						So(result, ShouldBeFalse)
						mockNitroClient.AssertNotCalled(t, "GetPrimeStatus")
						mockEligibilityDao.AssertNumberOfCalls(t, "IsUserEligibleForMilestone", 1)
					})
				})

				Convey("and prime user", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(true, nil)
					Convey("and eligible user", func() {
						mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(true, nil)
						result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
						So(err, ShouldBeNil)
						So(result, ShouldBeTrue)
						mockNitroClient.AssertNotCalled(t, "GetPrimeStatus")
						mockEligibilityDao.AssertNumberOfCalls(t, "IsUserEligibleForMilestone", 1)
					})

					Convey("and ineligible user", func() {
						mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(false, nil)
						result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
						So(err, ShouldBeNil)
						So(result, ShouldBeFalse)
						mockNitroClient.AssertNotCalled(t, "GetPrimeStatus")
						mockEligibilityDao.AssertNumberOfCalls(t, "IsUserEligibleForMilestone", 1)
					})
				})
			})

			Convey("with requiredSubs milestone, no eligibility check", func() {
				nonEligibilityTestMilestone.RequiredSubs = []string{"testproductID"}
				Convey("and user does not have required sub", func() {
					subsResp := &substwirp.GetUserProductSubscriptionsResponse{
						Subscriptions: []*substwirp.Subscription{},
					}
					mockSubsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(subsResp, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockSubsClient.AssertNumberOfCalls(t, "GetUserProductSubscriptions", 1)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})

				Convey("and user has required sub", func() {
					subsResp := &substwirp.GetUserProductSubscriptionsResponse{
						Subscriptions: []*substwirp.Subscription{{}},
					}
					mockSubsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(subsResp, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
					mockSubsClient.AssertNumberOfCalls(t, "GetUserProductSubscriptions", 1)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})

				Convey("and subs call throws error", func() {
					mockSubsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(nil, errors.New("test subs error"))
					result, err := u.IsUserEligibleForMilestone("u1", &nonEligibilityTestMilestone)
					So(err, ShouldNotBeNil)
					So(result, ShouldBeFalse)
					mockSubsClient.AssertNumberOfCalls(t, "GetUserProductSubscriptions", 1)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})
			})

			Convey("with requiresSubs milestone, with eligibility check", func() {
				eligibilityTestMilestone.RequiredSubs = []string{"testproductID"}
				Convey("and eligible user", func() {
					mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(true, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
					mockNitroClient.AssertNotCalled(t, "GetUserProductSubscriptions")
					mockEligibilityDao.AssertNumberOfCalls(t, "IsUserEligibleForMilestone", 1)
				})

				Convey("and ineligible user", func() {
					mockEligibilityDao.On("IsUserEligibleForMilestone", Anything, Anything).Return(false, nil)
					result, err := u.IsUserEligibleForMilestone("u1", &eligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockNitroClient.AssertNotCalled(t, "GetUserProductSubscriptions")
					mockEligibilityDao.AssertNumberOfCalls(t, "IsUserEligibleForMilestone", 1)
				})
			})
		})

		Convey("Test IsUserEligibleForMilestoneCompletion", func() {
			milestone := &campaign.Milestone{
				ID:                     testMilestoneID,
				Threshold:              int64(200),
				ParticipationThreshold: int64(100),
				CheckEligibility:       false,
				ActiveStartDate:        time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC),
				ActiveEndDate:          time.Date(2099, time.February, 7, 9, 0, 0, 0, time.UTC),
				HandlerMetadata: campaign.HandlerMetadata{
					Rewards: []*campaign.Reward{},
				},
			}
			Convey("with no prime or subs requirement", func() {
				result, err := u.IsUserEligibleForMilestoneCompletion("u1", milestone)
				So(err, ShouldBeNil)
				So(result, ShouldBeTrue)
			})

			Convey("with whitelisting", func() {
				eligibilityTestMilestone.UserWhiteListEnabled = true
				Convey("and whitelisted user", func() {
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &eligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
				})

				Convey("and non-whitelisted user", func() {
					result, err := u.IsUserEligibleForMilestoneCompletion("u2", &eligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockEligibilityDao.AssertNotCalled(t, "IsUserEligibleForMilestone")
				})
			})

			Convey("with prime only milestone", func() {
				nonEligibilityTestMilestone.IsPrimeOnly = true
				Convey("and non-prime user", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(false, nil)
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockNitroClient.AssertNumberOfCalls(t, "GetPrimeStatus", 1)
				})

				Convey("and prime user", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(true, nil)
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
					mockNitroClient.AssertNumberOfCalls(t, "GetPrimeStatus", 1)
				})

				Convey("and nitro throws error", func() {
					mockNitroClient.On("GetPrimeStatus", Anything).Return(false, errors.New("test nitro error"))
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &nonEligibilityTestMilestone)
					So(err, ShouldNotBeNil)
					So(result, ShouldBeFalse)
					mockNitroClient.AssertNumberOfCalls(t, "GetPrimeStatus", 1)
				})
			})

			Convey("with requiredSubs milestone", func() {
				nonEligibilityTestMilestone.RequiredSubs = []string{"testProductID"}
				Convey("and user does not have required sub", func() {
					subsResp := &substwirp.GetUserProductSubscriptionsResponse{
						Subscriptions: []*substwirp.Subscription{},
					}
					mockSubsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(subsResp, nil)
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeFalse)
					mockSubsClient.AssertNumberOfCalls(t, "GetUserProductSubscriptions", 1)
				})

				Convey("and user has required sub", func() {
					subsResp := &substwirp.GetUserProductSubscriptionsResponse{
						Subscriptions: []*substwirp.Subscription{{}},
					}
					mockSubsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(subsResp, nil)
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &nonEligibilityTestMilestone)
					So(err, ShouldBeNil)
					So(result, ShouldBeTrue)
					mockSubsClient.AssertNumberOfCalls(t, "GetUserProductSubscriptions", 1)
				})

				Convey("and subs call throws error", func() {
					mockSubsClient.On("GetUserProductSubscriptions", Anything, Anything).Return(nil, errors.New("test subs error"))
					result, err := u.IsUserEligibleForMilestoneCompletion("u1", &nonEligibilityTestMilestone)
					So(err, ShouldNotBeNil)
					So(result, ShouldBeFalse)
					mockSubsClient.AssertNumberOfCalls(t, "GetUserProductSubscriptions", 1)
				})
			})
		})

		Convey("Test GetCampaignConfigForDomain", func() {
			Convey("with config set to read from memory", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: false,
				}

				result, err := u.GetCampaignConfigForDomain(testDomain)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)

				campaigns, ok := result.Campaigns[testDomain]
				So(ok, ShouldBeTrue)
				So(campaigns[0].ID, ShouldEqual, testCampaign)
			})

			Convey("with config set to read from dynamo", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				mockCampaignHelper.On("GetCampaignConfigForDomain", Anything).Return(&campaignConfig, nil)

				result, err := u.GetCampaignConfigForDomain(testDomain)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignConfigForDomain", 1)
			})

			Convey("with dynamo error falls back to the in-memory data", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				mockCampaignHelper.On("GetCampaignConfigForDomain", Anything).Return(nil, errors.New("error"))

				result, err := u.GetCampaignConfigForDomain(testDomain)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)

				campaigns, ok := result.Campaigns[testDomain]
				So(ok, ShouldBeTrue)
				So(campaigns[0].ID, ShouldEqual, testCampaign)
			})
		})

		Convey("Test GetCampaignConfigForChannelID", func() {

			Convey("with config set to read from memory", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: false,
				}

				result, err := u.GetCampaignConfigForChannelID("1")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)

				campaigns, ok := result.Campaigns[testDomain]
				So(ok, ShouldBeTrue)
				So(campaigns[0].ID, ShouldEqual, testCampaign)
			})

			Convey("with config set to read from dynamoDB", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				channelProperties := dynamo.ChannelProperties{
					Domains: []string{testDomain},
				}

				mockChannelPropertiesDao.On("GetChannelProperties", Anything).Return(&channelProperties, nil)
				mockCampaignHelper.On("GetCampaignConfigForDomain", testDomain).Return(&campaignConfig, nil)
				mockCampaignHelper.On("GetAllChannelsCampaigns").Return(nil, nil)

				result, err := u.GetCampaignConfigForChannelID("1")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				mockChannelPropertiesDao.AssertNumberOfCalls(t, "GetChannelProperties", 1)
			})

			Convey("with all channels campaigns", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				allChannelCampaign := campaign.Campaign{
					ID:          testCampaign,
					AllChannels: true,
				}

				allChannelCampaigns := []*campaign.Campaign{&allChannelCampaign}

				mockChannelPropertiesDao.On("GetChannelProperties", Anything).Return(&dynamo.ChannelProperties{}, nil)
				mockCampaignHelper.On("GetAllChannelsCampaigns").Return(allChannelCampaigns, nil)

				result, err := u.GetCampaignConfigForChannelID("1")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				mockCampaignHelper.AssertNumberOfCalls(t, "GetAllChannelsCampaigns", 1)
			})

			Convey("with dynamo error falls back to the in-memory data", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				mockCampaignHelper.On("GetAllChannelsCampaigns").Return(nil, errors.New("error"))
				mockChannelPropertiesDao.On("GetChannelProperties", Anything).Return(&dynamo.ChannelProperties{}, nil)

				result, err := u.GetCampaignConfigForChannelID("1")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)

				campaigns, ok := result.Campaigns[testDomain]
				So(ok, ShouldBeTrue)
				So(campaigns[0].ID, ShouldEqual, testCampaign)
			})

			Convey("with all channels campaigns that are already in given channels properties", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				milestone := campaign.Milestone{
					ID: "m1",
				}

				objective := campaign.Objective{
					ID:         "o1",
					Milestones: []*campaign.Milestone{&milestone},
				}

				allChannelCampaign := campaign.Campaign{
					ID:          testCampaign,
					AllChannels: true,
					Domain:      testDomain,
					Objectives:  []*campaign.Objective{&objective},
				}

				allChannelCampaigns := []*campaign.Campaign{&allChannelCampaign}
				channelProperties := dynamo.ChannelProperties{
					Domains: []string{testDomain},
				}

				dupeCampaignConfig := campaign.Configuration{}
				dupeCampaignConfig.Campaigns = map[string][]*campaign.Campaign{
					testDomain: allChannelCampaigns,
				}

				mockChannelPropertiesDao.On("GetChannelProperties", Anything).Return(&channelProperties, nil)
				mockCampaignHelper.On("GetAllChannelsCampaigns").Return(allChannelCampaigns, nil)
				mockCampaignHelper.On("GetCampaignConfigForDomain", testDomain).Return(&dupeCampaignConfig, nil)

				result, err := u.GetCampaignConfigForChannelID("1")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
			})
		})

		Convey("Test GetCampaignByID", func() {
			Convey("with config set to read from memory", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: false,
				}
				Convey("with no campaign in config", func() {
					result, err := u.GetCampaignByID("mockFakeID")
					So(err, ShouldEqual, rewards.CampaignNotFoundErr)
					So(result, ShouldBeNil)
					mockCampaignHelper.AssertNotCalled(t, "GetCampaignWithChildDataByID", 0)
				})
				Convey("with campaign in config", func() {
					result, err := u.GetCampaignByID(campaignInstance.ID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, campaignInstance.ID)
					mockCampaignHelper.AssertNotCalled(t, "GetCampaignWithChildDataByID", 0)
				})
			})

			Convey("with config set to read from dynamoDB", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				campaignID := "campaignID"
				dynamoCampaign := campaign.Campaign{
					ID: campaignID,
				}

				Convey("with a match from DAO", func() {
					mockCampaignHelper.On("GetCampaignWithChildDataByID", campaignID).Return(&dynamoCampaign, nil)

					result, err := u.GetCampaignByID(campaignID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, campaignID)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)

					// verify result is cached and we don't call dynamo again when querying for same ID
					result, err = u.GetCampaignByID(campaignID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, campaignID)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)
				})

				Convey("with no result from DAO", func() {
					mockCampaignHelper.On("GetCampaignWithChildDataByID", campaignID).Return(nil, nil)

					result, err := u.GetCampaignByID(campaignID)
					So(err, ShouldEqual, rewards.CampaignNotFoundErr)
					So(result, ShouldBeNil)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)

					// verify nil result is cached and we don't call dynamo again when querying for same ID
					result, err = u.GetCampaignByID(campaignID)
					So(err, ShouldEqual, rewards.CampaignNotFoundErr)
					So(result, ShouldBeNil)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)
				})

				Convey("with ItemNotFound error from DAO", func() {
					mockCampaignHelper.On("GetCampaignWithChildDataByID", campaignID).Return(&dynamoCampaign, dynamo.ItemNotFoundErr)

					result, err := u.GetCampaignByID(campaignID)
					So(err, ShouldEqual, rewards.CampaignNotFoundErr)
					So(result, ShouldBeNil)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)

					// verify nil result is cached and we don't call dynamo again when querying for same ID
					result, err = u.GetCampaignByID(campaignID)
					So(err, ShouldEqual, rewards.CampaignNotFoundErr)
					So(result, ShouldBeNil)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)
				})

				Convey("with unexpected error from DAO falls back to in-memory", func() {
					mockCampaignHelper.On("GetCampaignWithChildDataByID", campaignInstance.ID).Return(nil, errors.New("something failed"))
					result, err := u.GetCampaignByID(campaignInstance.ID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, campaignInstance.ID)
					mockCampaignHelper.AssertNumberOfCalls(t, "GetCampaignWithChildDataByID", 1)
				})
			})
		})

		Convey("Test GetTriggerByID", func() {
			Convey("with config set to read from memory", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: false,
				}

				result, err := u.GetTriggerByID(triggerInstance.ID)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result.ID, ShouldEqual, triggerInstance.ID)
				mockTriggerDao.AssertNotCalled(t, "GetTriggerByID", 0)
			})

			Convey("with config set to read from dynamoDB", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				triggerID := "t2"
				dynamoTrigger := campaign.Trigger{
					ID: triggerID,
				}

				Convey("with a match from DAO", func() {
					mockTriggerDao.On("GetTriggerByID", triggerID).Return(&dynamoTrigger, nil)

					result, err := u.GetTriggerByID(triggerID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, triggerID)
					mockTriggerDao.AssertNumberOfCalls(t, "GetTriggerByID", 1)
				})

				Convey("with no result from DAO", func() {
					mockTriggerDao.On("GetTriggerByID", triggerID).Return(nil, nil)

					result, err := u.GetTriggerByID(triggerID)
					So(err, ShouldNotBeNil)
					So(result, ShouldBeNil)
				})

				Convey("with error from DAO falls back to in-memory", func() {
					mockTriggerDao.On("GetTriggerByID", triggerInstance.ID).Return(nil, errors.New("something failed"))
					result, err := u.GetTriggerByID(triggerInstance.ID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, triggerInstance.ID)
				})
			})
		})

		Convey("Test GetMilestoneByID", func() {
			mockObjective := campaign.Objective{
				ID:  "o1",
				Tag: "GLOBAL",
			}

			mockObjectiveDao.On("GetObjectiveByID", Anything).Return(&mockObjective, nil)

			Convey("with config set to read from memory", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: false,
				}

				result, err := u.GetMilestoneByID("m1")
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result.ID, ShouldEqual, "m1")
				mockMilestoneDao.AssertNotCalled(t, "GetMilestoneByID", 0)
			})

			Convey("with config set to read from dynamoDB", func() {
				u.Config = &config.Configuration{
					UseDynamoDBForCampaigns: true,
				}

				milestoneID := "m2"
				dynamoMilestone := campaign.Milestone{
					ID: milestoneID,
				}

				Convey("with a match from DAO", func() {
					mockMilestoneDao.On("GetMilestoneByID", milestoneID).Return(&dynamoMilestone, nil)

					result, err := u.GetMilestoneByID(milestoneID)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, milestoneID)
					mockMilestoneDao.AssertNumberOfCalls(t, "GetMilestoneByID", 1)
				})

				Convey("with no result from DAO", func() {
					mockMilestoneDao.On("GetMilestoneByID", milestoneID).Return(nil, nil)

					result, err := u.GetMilestoneByID(milestoneID)
					So(err, ShouldNotBeNil)
					So(result, ShouldBeNil)
				})

				Convey("with error from DAO falls back to in-memory", func() {

					mockMilestoneDao.On("GetMilestoneByID", "m1").Return(nil, errors.New("something failed"))
					result, err := u.GetMilestoneByID("m1")
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.ID, ShouldEqual, "m1")
				})
			})
		})

		Convey("Test InvalidateCampaignCacheForDomain", func() {
			Convey("with cache to invalidate", func() {
				result, err := u.InvalidateCampaignCacheForDomain("domain")
				So(err, ShouldBeNil)
				So(result, ShouldBeTrue)
			})
		})

		Convey("Test InvalidateAllChannelsCampaignsCache", func() {
			Convey("with cache to invalidate", func() {
				result, err := u.InvalidateAllChannelsCampaignsCache()
				So(err, ShouldBeNil)
				So(result, ShouldBeTrue)
			})
		})
	})
}
