# Twitch Wall - Visual Video Display

This is a simple web application for showing off Twitch content on the 3x3 wall in the Twitch lobby.  The site only works if you're inside the network.

## Contribution

The quickest way to get up and running is to:
```
npm install
npm start
```
Then open `http://localhost.twitch.tv:3000` in Safari

Safari is currently the only supported browser as it can natively handle the HLS streams that we're embedding.

The application uses [React](https://facebook.github.io/react/) and its [JSX](https://facebook.github.io/react/docs/jsx-in-depth.html) syntax which gets transpiled by [Babel](https://babeljs.io/) into ES5.  While we use ES6 style imports and exports it is just polyfilled [Browserify](https://github.com/substack/node-browserify#usage).

##TODOs
* Transition to multiple layouts
* Handle stream's going down (replace them)
* Add Boxart
* Increase amount of information shown
* Whatever you think of
