job {
  name 'twitch-wall'
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'web/wall', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell 'rm -rf .manta/'
    shell 'manta -v -proxy'
    saveDeployArtifact 'web/wall', '.manta'
  }
}

job {
  name 'twitch-wall-deploy'
  using 'TEMPLATE-deploy'
  steps {
    shell 'courier deploy --repo web/wall --dir /opt/twitch/twitch_wall'
  }
}
