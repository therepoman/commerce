package main

import (
	"flag"
	"log"
	"net/http"
)

func main() {
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/", fs)

	var listen = flag.String("listen", "127.0.0.1:8080", "Host and port to listen on")
	flag.Parse()

	log.Printf("Listening on %s...", *listen)
	http.ListenAndServe(*listen, nil)
}
