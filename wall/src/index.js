import Kraken from "./lib/kraken.js";
import React from "react";
import ReactDOM from "react-dom";
import Mosaic from "./components/mosaic.js";
import {shuffle, randomInt} from "./lib/utils.js";
import settings from "./settings.js";
import './index.css';

var windowLoad = new Promise(function(resolve, reject) {
  window.addEventListener('load', resolve);
});

//TODO: This should be cleaned up a lot more.
windowLoad.then(function(value) {
  let container = document.querySelector('#root');

  Kraken.getFeaturedChannels().then((channels) => {
    channels = channels.map((channel) => {
      return {
        id: channel._id,
        name: channel.channel.name,
        displayName: channel.channel.display_name,
        game: channel.game,
        viewers: channel.viewers
      };
    });
    let fullchannels = shuffle(channels);
    channels = fullchannels.slice(0,9);
    let replacementChannels = fullchannels.slice(9);

    let onReplaceChannel = function (channel) {
      let replaceIdx = channels.indexOf(channel);
      let candidateIdx = randomInt(0, replacementChannels.length);
      let temp = channels[replaceIdx];
      channels[replaceIdx] = replacementChannels[candidateIdx];
      replacementChannels[candidateIdx] = temp;

      render(channels);
    };

    let pendingRender = false;
    var render = function (channels) {
      if (pendingRender) {
        return;
      } else {
        pendingRender = true;
        setTimeout(() => {
          pendingRender = false;
          ReactDOM.render(
            <Mosaic
              channels={channels}
              columns={3}
              onReplaceChannel={onReplaceChannel}
              width={window.innerWidth}
              height={window.innerHeight}
            />,
            container
          );
        },100);
      }
    }

    render(channels);

    window.addEventListener('resize', () => {
      render(channels);
    });

    document.body.addEventListener('click', () => {
      document.body.webkitRequestFullscreen();
    });
  });

  setTimeout(() => {
    window.location.reload();
  }, settings.REFRESH_RATE);
});
