import 'whatwg-fetch';
import settings from '../settings.js';
import { clone } from "./utils.js";

const DEFAULT_HEADERS = new Headers({
  'Accept': 'application/vnd.twitchtv.v3+json',
  'Client-ID': settings.CLIENT_ID,
});

// APIS for accessing kraken for information about channels and games
// These all return a promise
function getFeaturedChannels() {
  return fetch('https://api.twitch.tv/kraken/streams/featured?limit=100', {
    headers: DEFAULT_HEADERS
  }).then((response) => {
    return response.json();
  }).then((resp) => {
    return resp.featured.map((item) => {
      return item.stream;
    })
  });
}

function getAccessTokenForChannel(channel) {
  var krakenUrl = `https://api.twitch.tv/channels/${channel}/access_token`;

  return new Promise(function (resolve, reject) {
    fetch(krakenUrl, {
      headers: DEFAULT_HEADERS
    }).then((response) => {
      resolve(response.json());
    });
  });
}

function getHLSUrlForChannel(channel) {
  var baseParams = {
    player: "twitchweb",
    p: 0,
    type: "any",
    allow_source: "true",
    allow_audio_only: "true"
  };


  var hlsURL = `http://usher.justin.tv/api/channel/hls/${channel.toLowerCase()}.m3u8`;

  return getAccessTokenForChannel(channel).then(function(token) {
    var params = clone(baseParams);
    params.token = encodeURIComponent(token.token);
    params.sig = encodeURIComponent(token.sig);

    var encodedParams = [];
    let keys = Object.keys(params)
    for (let i = 0, len = keys.length; i < len; i++) {
      let key = keys[i];
      encodedParams.push(`${key}=${params[key]}`);
    }
    var URLParams = encodedParams.join('&');
    return `${hlsURL}?${URLParams}`;
  });
}

export default {
  getFeaturedChannels: getFeaturedChannels,
  getHLSUrlForChannel: getHLSUrlForChannel
};
