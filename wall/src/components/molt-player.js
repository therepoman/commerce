import React from 'react';

//TODO: Have this use the embedded version.
//  Cannot currently use the embedded player since setChannel does not work on
//  HTML5 version of the player.
export default class MoltPlayer extends React.Component {
  render() {
    let url = `http://player.twitch.tv/?channel=${this.props.channel}&quality=high&!branding&!channelInfo&!controls&muted&html5`;

    return (
      <iframe src={url} width="100%" height="100%" />
    );
  }
}
