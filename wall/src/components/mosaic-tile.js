import React from 'react';
import HLSPlayer from './hls-player.js';
import MoltPlayer from './molt-player.js';
import ViewerIcon from "./icon-viewer.js";
import settings from '../settings.js';

let Player = HLSPlayer;
if (settings.USE_MOLT) {
  Player = MoltPlayer;
}

export default class MosaicTile extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      onTop: this.props.focused
    };
  }

  componentWillUpdate(newProps) {
    if (newProps.focused && !this.props.focused) {
      // We are becoming focused
      this.setState({
        onTop: true
      });
    }
    if (this.props.focused && !newProps.focused) {
      // We are becoming unfocused
      setTimeout(() => {
        this.setState({
          onTop: false
        });
      }, settings.ANIMATION_LENGTH);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.props.obscured !== prevProps.obscured) && this.props.obscured) {
      // We're obscured, potentially replace ourselves.
      if (Math.random() < settings.CHANCE_TO_REPLACE) {
        setTimeout(() => {
          this.replaceMe();
        }, settings.ANIMATION_LENGTH);
      }
    }
  }

  replaceMe () {
    if (this.props.onReplace) {
      this.props.onReplace(this.props.channel);
    }
  }

  onRightClick (e) {
    this.replaceMe();
    e.preventDefault();
    e.stopPropagation();
  }

  render() {
    let styles = {
      position: 'absolute',
      height: this.props.height,
      width: this.props.width,
      top: this.props.yOffset,
      left: this.props.xOffset,
      zIndex: this.state.onTop ? 1 : 0
    };

    return (
      <div onContextMenu={(e) => this.onRightClick()} className="playerWrapper" style={styles}>
        <Player channel={this.props.channel.name} />
        <div className="meta">
          <div className="info">
            <span className="channel-name">{this.props.channel.displayName}</span>
            <span className="playing">playing</span>
            <span className="channel-game">{this.props.channel.game}</span>
          </div>
          <div className="viewer-count">
            <ViewerIcon />
            <span>{this.props.channel.viewers}</span>
          </div>
        </div>
        <div className="click-catch"></div>
      </div>
    );
  }
}
