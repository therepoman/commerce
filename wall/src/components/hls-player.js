import React from 'react';
import Kraken from '../lib/kraken.js';

export default class Player extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      playlistURL: "",
      loading: true
    };
    this.loadHLS(this.props.channel);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.channel !== this.props.channel) {
      // Clean up old player (fixes memory leaks)
      if (this._player) {
        this._player.pause();
        this._player.src = "";
        this._player.load();
      }
      // New channel.
      this.setState({
        loading: true
      });
      this.loadHLS(nextProps.channel);
    }
  }

  loadHLS (channel) {
    Kraken.getHLSUrlForChannel(channel).then((hlsURL) => {
      this.setState({
        playlistURL: hlsURL,
        loading: false
      });
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="spinner">
        </div>
      );
    } else {
      return (
        <div>
          <video 
            ref={(ref) => this._player = ref}
            autoPlay
            muted="true"
            src={this.state.playlistURL}
          />
        </div>
      );
    }
  }
}
