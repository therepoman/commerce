import React from 'react';
import MosaicTile from './mosaic-tile.js';
import {randomInt} from '../lib/utils.js';
import settings from '../settings.js';

export default class Mosaic extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      focusSize: 2,
      focusIdx: 0
    };
  }

  componentDidMount() {
    this._updateInterval = setInterval(() => {
      this.changeView();
    }, settings.CYCLE_VIEWS_RATE);
  }

  componentWillUnmount() {
    if (this._updateInterval){
      clearInterval(this._updateInterval);
    }
  }

  changeView() {
    if (this.state.isFocused) {
      this.setState({
        isFocused: false
      });
    } else {
      let newFocusIdx = this.state.focusIdx;
      while (newFocusIdx === this.state.focusIdx) {
        newFocusIdx = randomInt(0, this.props.channels.length);
      }
      this.setState({
        isFocused: true,
        focusSize: randomInt(2,4),
        focusIdx: newFocusIdx
      });
    }
  }

  renderPlayer(channel, idx, height, width, focusRows, focusCols) {
    let row = Math.floor(idx / this.props.columns);
    let col = idx % this.props.columns;
    let xOffset = col * width;
    let yOffset = row * height;
    let obscured = false;
    let focused = false;

    if (this.state.isFocused) {
      if (this.state.focusIdx === idx) {
        xOffset = Math.min(...focusCols) * width;
        yOffset = Math.min(...focusRows) * height;
        focused = true;
        width *= this.state.focusSize;
        height *= this.state.focusSize;
      } else {
        if ((focusRows.indexOf(row) >= 0) && (focusCols.indexOf(col) >= 0)) {
          obscured = true;
        }
      }
    }


    return (
      <MosaicTile
        key={channel.id}
        channel={channel}
        height={height}
        width={width}
        xOffset={xOffset}
        yOffset={yOffset}
        obscured={obscured}
        focused={focused}
        onReplace={this.props.onReplaceChannel}
      />
    );
  }

  render() {
    let aspectRatio = this.props.width / this.props.height;
    let isTooTall = aspectRatio < settings.VIDEO_ASPECT_RATIO;
    let tileWidth = this.props.width / this.props.columns;
    let tileHeight = tileWidth * (1 / settings.VIDEO_ASPECT_RATIO);
    let rows = Math.floor(this.props.channels.length / this.props.columns);

    if (isTooTall) {
      tileHeight = this.props.height / rows;
      tileWidth = tileHeight * settings.VIDEO_ASPECT_RATIO;
    }

    let xOffset = ((tileWidth * this.props.columns) - this.props.width) / 2;
    let yOffset = ((tileHeight * rows) - this.props.height) / 2;

    let styles = {
      top: -yOffset,
      left: -xOffset
    };
    
    // Calculate Rows and Cols covered by Focus Video
    let focusRows = [];
    let focusCols = [];
    let focusRow = Math.floor(this.state.focusIdx / this.props.columns);
    let minRow = focusRow;
    if ((focusRow + this.state.focusSize) >= rows) {
      minRow = rows - this.state.focusSize;
    }

    let focusCol = this.state.focusIdx % this.props.columns;
    let minCol = focusCol;
    if ((focusCol + this.state.focusSize) >= this.props.columns) {
      minCol = this.props.columns - this.state.focusSize;
    }

    for (let i = 0; i < this.state.focusSize; i++) {
      focusRows.push(minRow + i);
      focusCols.push(minCol + i);
    }

    return (
      <div className='mosaic' style={styles}>
        {this.props.channels.map((channel, idx) => {
          return this.renderPlayer(channel, idx, tileHeight, tileWidth, focusRows, focusCols);
        })}
      </div>
    );
  }
}

