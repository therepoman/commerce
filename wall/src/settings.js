export default {
  // Allow a video to fill the whole screen
  ALLOW_SCALE_3: true,

  // Aspect Ratio videos should be rendered at
  VIDEO_ASPECT_RATIO: 1920/1080,

  // How often to mix up the layout of the grid (ms)
  CYCLE_VIEWS_RATE: 20 * 1000,

  // How often to give up and just totally refresh the page (ms)
  REFRESH_RATE: 60 * 60 * 1000,

  // If we should use the molt player instead of HLS Video Tags
  USE_MOLT: false,

  // How long transitions between views take (matches CSS)
  ANIMATION_LENGTH: 1000,

  // How likely a video is to be replaced when obscured [0,1]
  // 0 - Never Replace
  // 1 - Always Replace
  CHANCE_TO_REPLACE: 0.1,

  // Twitch Client ID
  CLIENT_ID: "9oeygh96hkf6ur80nbz2q6nn8f3jnbc"
};
