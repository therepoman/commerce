var pickFiles = require('broccoli-static-compiler');
var compileSass = require('broccoli-sass');
var mergeTrees = require('broccoli-merge-trees');
var watchify = require('broccoli-watchify');
var uglify = require('broccoli-uglify-js');
var env = require('broccoli-env').getEnv();

var js = watchify('app', {
  browserify: {
    entries: ['./app.js'],
    debug: true
  },
  outputFile: 'wall.js',
  cache: true,
  init: function(b) {
    b.transform('babelify', {presets: ['es2015', 'react']});
  }
});

if (env === 'production') {
  js = uglify(js);
}

var html = pickFiles('app', {
  srcDir: './',
  destDir: './',
  files: ['index.html']
});

var styles = compileSass(
  ['app/styles/'], 
  'main.scss',
  'app.css'
);

var staticAssets = pickFiles('app', {
  srcDir: 'static',
  destDir: 'static'
});

module.exports = mergeTrees([js, html, styles, staticAssets]);
