package publish_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/publish"
	"code.justin.tv/commerce/prometheus/config"
	sns_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEventSNSPublisher_PublishEvent(t *testing.T) {
	Convey("Given an event SNS publisher", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		cfg.PublishEventSNSTopic = "test-topic"
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		snsClient := new(sns_mock.Client)
		eventSNSPublisher := &publish.EventSNSPublisher{
			Config:    cfg,
			SNSClient: snsClient,
		}

		event := models.Event{
			EventID: "E1",
		}

		Convey("Errors when SNS errors", func() {
			snsClient.On("Publish", mock.Anything, "test-topic", mock.Anything).Return(errors.New("test error"))
			err := eventSNSPublisher.PublishEvent(context.Background(), event)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when SNS succeeds", func() {
			snsClient.On("Publish", mock.Anything, "test-topic", mock.Anything).Return(nil)
			err := eventSNSPublisher.PublishEvent(context.Background(), event)
			So(err, ShouldBeNil)
		})
	})
}
