package publish

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/config"
)

type EventPublisher interface {
	PublishEvent(ctx context.Context, e models.Event) error
}

type EventSNSPublisher struct {
	SNSClient sns.Client     `inject:""`
	Config    *config.Config `inject:""`
}

func (p *EventSNSPublisher) PublishEvent(ctx context.Context, e models.Event) error {
	eJSON, err := json.Marshal(&e)
	if err != nil {
		return err
	}

	return p.SNSClient.Publish(ctx, p.Config.PublishEventSNSTopic, string(eJSON))
}
