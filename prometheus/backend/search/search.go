package search

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	string_utils "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/prometheus/backend/clients/elasticsearch"
	"code.justin.tv/commerce/prometheus/backend/models"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

const (
	DefaultLimit = 10

	filterUsageMetricFormat = "search-filtering-%s"
)

type Searcher interface {
	Search(ctx context.Context, req *prometheus.SearchReq) ([]models.Event, int64, error)
}

type ElasticSearchSearcher struct {
	ESClient elasticsearch.Client `inject:""`
	Statter  statsd.Statter       `inject:""`
}

func containsALL(list []prometheus.EventSubType) bool {
	for _, b := range list {
		if b == prometheus.EventSubType_ALL {
			return true
		}
	}
	return false
}

var sortByFields = map[prometheus.SortBy]bool{
	prometheus.SortBy_EVENT_TIME:              false,
	prometheus.SortBy_EVENT_TYPE:              true,
	prometheus.SortBy_EVENT_ID:                true,
	prometheus.SortBy_ORDER_ID:                true,
	prometheus.SortBy_USER_ID:                 true,
	prometheus.SortBy_USER_LOGIN:              true,
	prometheus.SortBy_CHANNEL_ID:              true,
	prometheus.SortBy_CHANNEL_LOGIN:           true,
	prometheus.SortBy_GIFT_RECIPIENT_ID:       true,
	prometheus.SortBy_GIFT_RECIPIENT_LOGIN:    true,
	prometheus.SortBy_BITS_AMOUNT:             false,
	prometheus.SortBy_CURRENCY_AMOUNT:         false,
	prometheus.SortBy_CURRENCY_CODE:           true,
	prometheus.SortBy_MESSAGE:                 true,
	prometheus.SortBy_PURCHASE_ORDER_STATE:    true,
	prometheus.SortBy_PAYMENT_SERVICE:         true,
	prometheus.SortBy_PAYMENT_GATEWAY:         true,
	prometheus.SortBy_EXTENSION_ID:            true,
	prometheus.SortBy_EXTENSION_SKU:           true,
	prometheus.SortBy_EXTENSION_CREATOR_ID:    true,
	prometheus.SortBy_EXTENSION_CREATOR_LOGIN: true,
	prometheus.SortBy_AMOUNT:                  false,
	prometheus.SortBy_BITS_BALANCE:            false,
	prometheus.SortBy_COUNTRY_CODE:            true,
	prometheus.SortBy_CITY:                    true,
	prometheus.SortBy_POSTAL_CODE:             true,
	prometheus.SortBy_POLL_ID:                 true,
	prometheus.SortBy_POLL_CHOICE_ID:          true,
}

func isTextField(sortBy prometheus.SortBy) bool {
	return sortByFields[sortBy]
}

func (s *ElasticSearchSearcher) statFilterUsage(fieldName string) {
	go func() {
		err := s.Statter.Inc(fmt.Sprintf(filterUsageMetricFormat, fieldName), 1, 1)
		if err != nil {
			log.WithError(err).Error("error statting search filter usage")
		}
	}()
}

func (s *ElasticSearchSearcher) Search(ctx context.Context, req *prometheus.SearchReq) ([]models.Event, int64, error) {
	filters := make([]elastic.Query, 0)

	if strings.NotBlank(req.EventType) {
		filters = append(filters, elastic.NewTermQuery("event_type.keyword", req.EventType))
	}

	switch f := req.SubTypeFilter.(type) {
	case *prometheus.SearchReq_EventSubType:
		s.statFilterUsage("SubType")
		if eventSubType, ok := prometheus.EventSubType_name[int32(f.EventSubType)]; f.EventSubType != prometheus.EventSubType_ALL && ok {
			filters = append(filters, elastic.NewTermQuery("event_sub_type.keyword", eventSubType))
		}
	case *prometheus.SearchReq_EventSubTypesWrapper:
		s.statFilterUsage("ManySubTypes")
		if len(f.EventSubTypesWrapper.EventSubTypes) > 0 && !containsALL(f.EventSubTypesWrapper.EventSubTypes) {
			eventSubTypes := make([]interface{}, len(f.EventSubTypesWrapper.EventSubTypes))
			for i, eventSubType := range f.EventSubTypesWrapper.EventSubTypes {
				eventSubTypes[i] = prometheus.EventSubType_name[int32(eventSubType)]
			}
			filters = append(filters, elastic.NewTermsQuery("event_sub_type.keyword", eventSubTypes...))
		}
	case nil:
		//the field is not set
	default:
		return nil, 0, errors.New("invalid sub_type_filter")
	}

	if strings.NotBlank(req.EventId) {
		s.statFilterUsage("EventId")
		filters = append(filters, elastic.NewTermQuery("event_id.keyword", req.EventId))
	}

	if strings.NotBlank(req.OrderId) {
		s.statFilterUsage("OrderId")
		filters = append(filters, elastic.NewTermQuery("order_id.keyword", req.OrderId))
	}

	if strings.NotBlank(req.CurrencyCode) {
		s.statFilterUsage("CurrencyCode")
		filters = append(filters, elastic.NewTermQuery("currency_code.keyword", req.CurrencyCode))
	}

	if strings.NotBlank(req.UserId) {
		s.statFilterUsage("UserId")
		filters = append(filters, elastic.NewTermQuery("user_id.keyword", req.UserId))
	}

	if strings.NotBlank(req.UserLogin) {
		s.statFilterUsage("UserLogin")
		filters = append(filters, elastic.NewTermQuery("user_login.keyword", req.UserLogin))
	}

	if strings.NotBlank(req.ChannelId) {
		s.statFilterUsage("ChannelId")
		filters = append(filters, elastic.NewTermQuery("channel_id.keyword", req.ChannelId))
	}

	if strings.NotBlank(req.ChannelLogin) {
		s.statFilterUsage("ChannelLogin")
		filters = append(filters, elastic.NewTermQuery("channel_login.keyword", req.ChannelLogin))
	}

	if strings.NotBlank(req.GiftRecipientId) {
		s.statFilterUsage("GiftRecipientId")
		filters = append(filters, elastic.NewTermQuery("gift_recipient_id.keyword", req.GiftRecipientId))
	}

	if strings.NotBlank(req.GiftRecipientLogin) {
		s.statFilterUsage("GiftRecipientLogin")
		filters = append(filters, elastic.NewTermQuery("gift_recipient_login.keyword", req.GiftRecipientLogin))
	}

	if strings.NotBlank(req.Message) {
		s.statFilterUsage("Message")
		filters = append(filters, elastic.NewTermQuery("message", string_utils.ToLower(req.Message)))
	}

	if strings.NotBlank(req.AdminUser) {
		s.statFilterUsage("AdminUser")
		filters = append(filters, elastic.NewTermQuery("admin_user.keyword", req.AdminUser))
	}

	if strings.NotBlank(req.AdminMessage) {
		s.statFilterUsage("AdminMessage")
		filters = append(filters, elastic.NewTermQuery("admin_message", string_utils.ToLower(req.AdminMessage)))
	}

	if strings.NotBlank(req.PurchaseOrderState) {
		s.statFilterUsage("PurchaseOrderState")
		filters = append(filters, elastic.NewTermQuery("purchase_order_state.keyword", req.PurchaseOrderState))
	}

	if strings.NotBlank(req.PaymentService) {
		s.statFilterUsage("PaymentService")
		filters = append(filters, elastic.NewTermQuery("payment_service.keyword", req.PaymentService))
	}

	if strings.NotBlank(req.PaymentGateway) {
		s.statFilterUsage("PaymentGateway")
		filters = append(filters, elastic.NewTermQuery("payment_gateway.keyword", req.PaymentGateway))
	}

	if strings.NotBlank(req.ExtensionId) {
		s.statFilterUsage("ExtensionId")
		filters = append(filters, elastic.NewTermQuery("extension_id.keyword", req.ExtensionId))
	}

	if strings.NotBlank(req.ExtensionSku) {
		s.statFilterUsage("ExtensionSku")
		filters = append(filters, elastic.NewTermQuery("extension_sku.keyword", req.ExtensionSku))
	}

	if strings.NotBlank(req.ExtensionCreatorId) {
		s.statFilterUsage("ExtensionCreatorId")
		filters = append(filters, elastic.NewTermQuery("extension_creator_id.keyword", req.ExtensionCreatorId))
	}

	if strings.NotBlank(req.ExtensionCreatorLogin) {
		s.statFilterUsage("ExtensionCreatorLogin")
		filters = append(filters, elastic.NewTermQuery("extension_creator_login.keyword", req.ExtensionCreatorLogin))
	}

	if strings.NotBlank(req.AnonymousState) {
		s.statFilterUsage("AnonymousState")
		filters = append(filters, elastic.NewTermQuery("anonymous_state.keyword", req.AnonymousState))
	}

	if strings.NotBlank(req.CountryCode) {
		s.statFilterUsage("CountryCode")
		filters = append(filters, elastic.NewTermQuery("country_code.keyword", req.CountryCode))
	}

	if strings.NotBlank(req.City) {
		s.statFilterUsage("City")
		filters = append(filters, elastic.NewTermQuery("city.keyword", req.City))
	}

	if strings.NotBlank(req.PostalCode) {
		s.statFilterUsage("PostalCode")
		filters = append(filters, elastic.NewTermQuery("postal_code.keyword", req.PostalCode))
	}

	if strings.NotBlank(req.PollId) {
		s.statFilterUsage("PollId")
		filters = append(filters, elastic.NewTermQuery("poll_id.keyword", req.PollId))
	}

	if strings.NotBlank(req.PollChoiceId) {
		s.statFilterUsage("PollChoiceId")
		filters = append(filters, elastic.NewTermQuery("poll_choice_id.keyword", req.PollChoiceId))
	}

	if strings.NotBlank(req.HoldId) {
		s.statFilterUsage("HoldId")
		filters = append(filters, elastic.NewTermQuery("hold_id.keyword", req.HoldId))
	}

	if req.LowerBitsAmount != 0 || req.UpperBitsAmount != 0 {
		s.statFilterUsage("BitsAmountRange")

		bitsAmountQuery := elastic.NewRangeQuery("bits_amount")
		if req.LowerBitsAmount != 0 {
			bitsAmountQuery = bitsAmountQuery.Gte(req.LowerBitsAmount)
		}
		if req.UpperBitsAmount != 0 {
			bitsAmountQuery = bitsAmountQuery.Lte(req.UpperBitsAmount)
		}
		filters = append(filters, bitsAmountQuery)
	}

	if req.LowerBitsBalance != 0 || req.UpperBitsBalance != 0 {
		s.statFilterUsage("BitsBalanceRange")

		bitsBalanceQuery := elastic.NewRangeQuery("bits_balance")
		if req.LowerBitsBalance != 0 {
			bitsBalanceQuery = bitsBalanceQuery.Gte(req.LowerBitsBalance)
		}
		if req.UpperBitsBalance != 0 {
			bitsBalanceQuery = bitsBalanceQuery.Lte(req.UpperBitsBalance)
		}
		filters = append(filters, bitsBalanceQuery)
	}

	if req.LowerAmount != 0 || req.UpperAmount != 0 {
		s.statFilterUsage("AmountRange")

		amountQuery := elastic.NewRangeQuery("amount")
		if req.LowerAmount != 0 {
			amountQuery = amountQuery.Gte(req.LowerAmount)
		}
		if req.UpperAmount != 0 {
			amountQuery = amountQuery.Lte(req.UpperAmount)
		}
		filters = append(filters, amountQuery)
	}

	if req.LowerCurrencyAmount != 0 || req.UpperCurrencyAmount != 0 {
		s.statFilterUsage("CurrencyAmountRange")

		currencyAmountQuery := elastic.NewRangeQuery("currency_amount")
		if req.LowerCurrencyAmount != 0 {
			currencyAmountQuery = currencyAmountQuery.Gte(req.LowerCurrencyAmount)
		}
		if req.UpperCurrencyAmount != 0 {
			currencyAmountQuery = currencyAmountQuery.Lte(req.UpperCurrencyAmount)
		}
		filters = append(filters, currencyAmountQuery)
	}

	if req.StartEventTime != nil || req.EndEventTime != nil {
		s.statFilterUsage("EventTimeRange")

		eventTimeQuery := elastic.NewRangeQuery("event_time")
		if req.StartEventTime != nil {
			startEventTime, err := ptypes.Timestamp(req.StartEventTime)
			if err != nil {
				return []models.Event{}, 0, err
			}
			eventTimeQuery = eventTimeQuery.From(startEventTime)
		}

		if req.EndEventTime != nil {
			s.statFilterUsage("EndEventTime")

			endEventTime, err := ptypes.Timestamp(req.EndEventTime)
			if err != nil {
				return []models.Event{}, 0, err
			}
			eventTimeQuery.To(endEventTime)
		}

		filters = append(filters, eventTimeQuery)
	}

	sortByFieldName, ok := prometheus.SortBy_name[int32(req.SortBy)]
	if !ok {
		return nil, 0, errors.New("invalid sort_by")
	}

	//es only supports text sorting on the keyword type
	if isTextField(req.SortBy) {
		sortByFieldName = string_utils.ToLower(sortByFieldName) + ".keyword"
	} else {
		sortByFieldName = string_utils.ToLower(sortByFieldName)
	}

	_, ok = prometheus.SortDirection_name[int32(req.SortDirection)]
	if !ok {
		return nil, 0, errors.New("invalid sort_direction")
	}

	sorter := elastic.NewFieldSort(sortByFieldName)
	if req.SortDirection == prometheus.SortDirection_ASCENDING {
		sorter = sorter.Asc()
	}
	if req.SortDirection == prometheus.SortDirection_DESCENDING {
		sorter = sorter.Desc()
	}

	var limit, offset int
	if req.Limit == 0 {
		limit = DefaultLimit
	} else {
		limit = int(req.Limit)
	}
	offset = int(req.Offset)

	combinedQuery := elastic.NewBoolQuery().Must(filters...)
	result, err := s.ESClient.Search(ctx, models.ESEventIndexName, models.ESEventTypeName, limit, offset, combinedQuery, sorter)
	if err != nil {
		return nil, 0, err
	}

	events := make([]models.Event, 0)

	var event models.Event
	for _, ei := range result.Each(reflect.TypeOf(event)) {
		if e, ok := ei.(models.Event); ok {
			events = append(events, e)
		}
	}

	var total int64
	if result.Hits != nil {
		total = result.Hits.TotalHits
	}

	return events, total, nil
}
