package search_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/search"
	elasticsearch_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/clients/elasticsearch"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"github.com/brildum/testify/mock"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes"
	"github.com/olivere/elastic"
	. "github.com/smartystreets/goconvey/convey"
)

func extractBoolMustTermQueryValue(esClient *elasticsearch_mock.Client, key string) string {
	So(len(esClient.Calls), ShouldEqual, 1)
	searchCall := esClient.Calls[0]

	So(len(searchCall.Arguments), ShouldEqual, 7)
	queryArg := searchCall.Arguments[5]

	combinedQuery, ok := queryArg.(*elastic.BoolQuery)
	So(ok, ShouldBeTrue)

	querySource, err := combinedQuery.Source()
	So(err, ShouldBeNil)

	esQuery1, ok := querySource.(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery2, ok := esQuery1["bool"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery3, ok := esQuery2["must"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery4, ok := esQuery3["term"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	queryValue, ok := esQuery4[key]
	So(ok, ShouldBeTrue)

	queryValueString, ok := queryValue.(string)
	So(ok, ShouldBeTrue)

	return queryValueString
}

func extractBoolMustTermsQueryValue(esClient *elasticsearch_mock.Client, key string) interface{} {
	So(len(esClient.Calls), ShouldEqual, 1)
	searchCall := esClient.Calls[0]

	So(len(searchCall.Arguments), ShouldEqual, 7)
	queryArg := searchCall.Arguments[5]

	combinedQuery, ok := queryArg.(*elastic.BoolQuery)
	So(ok, ShouldBeTrue)

	querySource, err := combinedQuery.Source()
	So(err, ShouldBeNil)

	esQuery1, ok := querySource.(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery2, ok := esQuery1["bool"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery3, ok := esQuery2["must"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery4, ok := esQuery3["terms"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	queryValue, ok := esQuery4[key]
	So(ok, ShouldBeTrue)

	return queryValue
}

func isBoolMustQueryEmpty(esClient *elasticsearch_mock.Client, key string) {
	So(len(esClient.Calls), ShouldEqual, 1)
	searchCall := esClient.Calls[0]

	So(len(searchCall.Arguments), ShouldEqual, 7)
	queryArg := searchCall.Arguments[5]

	combinedQuery, ok := queryArg.(*elastic.BoolQuery)
	So(ok, ShouldBeTrue)

	querySource, err := combinedQuery.Source()
	So(err, ShouldBeNil)

	esQuery1, ok := querySource.(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery2, ok := esQuery1["bool"].(map[string]interface{})
	So(ok, ShouldBeTrue)
	So(esQuery2, ShouldBeEmpty)
}

func extractBoolMustRangeQueryValueInt(esClient *elasticsearch_mock.Client, key1 string, key2 string) int64 {
	So(len(esClient.Calls), ShouldEqual, 1)
	searchCall := esClient.Calls[0]

	So(len(searchCall.Arguments), ShouldEqual, 7)
	queryArg := searchCall.Arguments[5]

	combinedQuery, ok := queryArg.(*elastic.BoolQuery)
	So(ok, ShouldBeTrue)

	querySource, err := combinedQuery.Source()
	So(err, ShouldBeNil)

	esQuery1, ok := querySource.(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery2, ok := esQuery1["bool"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery3, ok := esQuery2["must"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery4, ok := esQuery3["range"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery5, ok := esQuery4[key1].(map[string]interface{})
	So(ok, ShouldBeTrue)

	queryValue, ok := esQuery5[key2]
	So(ok, ShouldBeTrue)

	queryValueInt, ok := queryValue.(int64)
	So(ok, ShouldBeTrue)

	return queryValueInt
}

func extractBoolMustRangeQueryValueFloat(esClient *elasticsearch_mock.Client, key1 string, key2 string) float32 {
	So(len(esClient.Calls), ShouldEqual, 1)
	searchCall := esClient.Calls[0]

	So(len(searchCall.Arguments), ShouldEqual, 7)
	queryArg := searchCall.Arguments[5]

	combinedQuery, ok := queryArg.(*elastic.BoolQuery)
	So(ok, ShouldBeTrue)

	querySource, err := combinedQuery.Source()
	So(err, ShouldBeNil)

	esQuery1, ok := querySource.(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery2, ok := esQuery1["bool"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery3, ok := esQuery2["must"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery4, ok := esQuery3["range"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery5, ok := esQuery4[key1].(map[string]interface{})
	So(ok, ShouldBeTrue)

	queryValue, ok := esQuery5[key2]
	So(ok, ShouldBeTrue)

	queryValueFloat, ok := queryValue.(float32)
	So(ok, ShouldBeTrue)

	return queryValueFloat
}

func extractBoolMustRangeQueryValueTime(esClient *elasticsearch_mock.Client, key1 string, key2 string) time.Time {
	So(len(esClient.Calls), ShouldEqual, 1)
	searchCall := esClient.Calls[0]

	So(len(searchCall.Arguments), ShouldEqual, 7)
	queryArg := searchCall.Arguments[5]

	combinedQuery, ok := queryArg.(*elastic.BoolQuery)
	So(ok, ShouldBeTrue)

	querySource, err := combinedQuery.Source()
	So(err, ShouldBeNil)

	esQuery1, ok := querySource.(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery2, ok := esQuery1["bool"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery3, ok := esQuery2["must"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery4, ok := esQuery3["range"].(map[string]interface{})
	So(ok, ShouldBeTrue)

	esQuery5, ok := esQuery4[key1].(map[string]interface{})
	So(ok, ShouldBeTrue)

	queryValue, ok := esQuery5[key2]
	So(ok, ShouldBeTrue)

	queryValueTime, ok := queryValue.(time.Time)
	So(ok, ShouldBeTrue)

	return queryValueTime
}

func TestElasticSearchSearcher_Search(t *testing.T) {
	Convey("Given a searcher", t, func() {
		esClient := new(elasticsearch_mock.Client)
		searcher := &search.ElasticSearchSearcher{
			ESClient: esClient,
			Statter:  &statsd.NoopClient{},
		}

		Convey("Errors for an invalid sort_by", func() {
			_, _, err := searcher.Search(context.Background(), &prometheus.SearchReq{
				SortBy:        prometheus.SortBy(-1),
				SortDirection: prometheus.SortDirection_ASCENDING,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors for an invalid sort_direction", func() {
			_, _, err := searcher.Search(context.Background(), &prometheus.SearchReq{
				SortBy:        prometheus.SortBy_BITS_AMOUNT,
				SortDirection: prometheus.SortDirection(-1),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid request", func() {
			req := &prometheus.SearchReq{
				SortBy:        prometheus.SortBy_BITS_AMOUNT,
				SortDirection: prometheus.SortDirection_ASCENDING,
			}

			Convey("Errors when es client errors", func() {
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds with empty result when es client succeeds with empty result", func() {
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				events, total, err := searcher.Search(context.Background(), req)
				So(len(events), ShouldEqual, 0)
				So(total, ShouldEqual, 0)
				So(err, ShouldBeNil)
			})

			Convey("Contains correct term query when event_type in request", func() {
				req.EventType = "test-event-type"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "event_type.keyword")
				So(actualTermQuery, ShouldEqual, req.EventType)
			})

			Convey("Contains correct term query when event_sub_type in request", func() {
				req.SubTypeFilter = &prometheus.SearchReq_EventSubType{EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase}
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "event_sub_type.keyword")
				So(actualTermQuery, ShouldEqual, prometheus.EventSubType_name[int32(req.GetEventSubType())])
			})

			Convey("No sub_type_filter applied when ALL event_sub_type in request", func() {
				req.SubTypeFilter = &prometheus.SearchReq_EventSubType{EventSubType: prometheus.EventSubType_ALL}
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				isBoolMustQueryEmpty(esClient, "event_sub_type.keyword")
			})

			Convey("Contains correct terms query when event_sub_types in request", func() {
				req.SubTypeFilter = &prometheus.SearchReq_EventSubTypesWrapper{EventSubTypesWrapper: &prometheus.EventSubTypes{EventSubTypes: []prometheus.EventSubType{prometheus.EventSubType_GiveBitsToBroadcaster}}}
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermsQuery := extractBoolMustTermsQueryValue(esClient, "event_sub_type.keyword")
				eventSubTypes := make([]interface{}, len(req.GetEventSubTypesWrapper().EventSubTypes))
				for i, eventSubType := range req.GetEventSubTypesWrapper().EventSubTypes {
					eventSubTypes[i] = prometheus.EventSubType_name[int32(eventSubType)]
				}
				So(actualTermsQuery, ShouldResemble, eventSubTypes)
			})

			Convey("No sub_type_filter applied when empty event_sub_types in request", func() {
				req.SubTypeFilter = &prometheus.SearchReq_EventSubTypesWrapper{EventSubTypesWrapper: &prometheus.EventSubTypes{EventSubTypes: []prometheus.EventSubType{}}}
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				isBoolMustQueryEmpty(esClient, "event_sub_type.keyword")
			})

			Convey("No sub_type_filter applied when event_sub_types containing ALL in request", func() {
				req.SubTypeFilter = &prometheus.SearchReq_EventSubTypesWrapper{EventSubTypesWrapper: &prometheus.EventSubTypes{EventSubTypes: []prometheus.EventSubType{prometheus.EventSubType_ALL}}}
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				isBoolMustQueryEmpty(esClient, "event_sub_type.keyword")
			})

			Convey("Contains correct term query when event_id in request", func() {
				req.EventId = "test-event-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "event_id.keyword")
				So(actualTermQuery, ShouldEqual, req.EventId)
			})

			Convey("Contains correct term query when user_id in request", func() {
				req.UserId = "test-user-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "user_id.keyword")
				So(actualTermQuery, ShouldEqual, req.UserId)
			})

			Convey("Contains correct term query when user_login in request", func() {
				req.UserLogin = "test-user-login"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "user_login.keyword")
				So(actualTermQuery, ShouldEqual, req.UserLogin)
			})

			Convey("Contains correct term query when channel_id in request", func() {
				req.ChannelId = "test-channel-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "channel_id.keyword")
				So(actualTermQuery, ShouldEqual, req.ChannelId)
			})

			Convey("Contains correct term query when channel_login in request", func() {
				req.ChannelLogin = "test-channel-login"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "channel_login.keyword")
				So(actualTermQuery, ShouldEqual, req.ChannelLogin)
			})

			Convey("Contains correct term query when gift_recipient_id in request", func() {
				req.GiftRecipientId = "test-gift-recipient-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "gift_recipient_id.keyword")
				So(actualTermQuery, ShouldEqual, req.GiftRecipientId)
			})

			Convey("Contains correct term query when gift_recipient_login in request", func() {
				req.GiftRecipientLogin = "test-gift-recipient-login"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "gift_recipient_login.keyword")
				So(actualTermQuery, ShouldEqual, req.GiftRecipientLogin)
			})

			Convey("Contains correct term query when currency_code in request", func() {
				req.CurrencyCode = "test-currency-code"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "currency_code.keyword")
				So(actualTermQuery, ShouldEqual, req.CurrencyCode)
			})

			Convey("Contains correct term query when order_id in request", func() {
				req.OrderId = "test-order-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "order_id.keyword")
				So(actualTermQuery, ShouldEqual, req.OrderId)
			})

			Convey("Contains correct term query when purchase_order_state in request", func() {
				req.PurchaseOrderState = "test-purchase-order-state"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "purchase_order_state.keyword")
				So(actualTermQuery, ShouldEqual, req.PurchaseOrderState)
			})

			Convey("Contains correct term query when payment_service in request", func() {
				req.PaymentService = "test-payment-service"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "payment_service.keyword")
				So(actualTermQuery, ShouldEqual, req.PaymentService)
			})

			Convey("Contains correct term query when payment_gateway in request", func() {
				req.PaymentGateway = "test-payment-gateway"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "payment_gateway.keyword")
				So(actualTermQuery, ShouldEqual, req.PaymentGateway)
			})

			Convey("Contains correct term query when message in request", func() {
				req.Message = "test-message"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "message")
				So(actualTermQuery, ShouldEqual, req.Message)
			})

			Convey("Contains correct term query when admin_user in request", func() {
				req.AdminUser = "test-admin-user"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "admin_user.keyword")
				So(actualTermQuery, ShouldEqual, req.AdminUser)
			})

			Convey("Contains correct term query when admin_message in request", func() {
				req.AdminMessage = "test-admin-message"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "admin_message")
				So(actualTermQuery, ShouldEqual, req.AdminMessage)
			})

			Convey("Contains correct term query when extension_id in request", func() {
				req.ExtensionId = "test-extension-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "extension_id.keyword")
				So(actualTermQuery, ShouldEqual, req.ExtensionId)
			})

			Convey("Contains correct term query when extension_sku in request", func() {
				req.ExtensionSku = "test-extension-sku"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "extension_sku.keyword")
				So(actualTermQuery, ShouldEqual, req.ExtensionSku)
			})

			Convey("Contains correct term query when extension_creator_id in request", func() {
				req.ExtensionCreatorId = "test-extension-creator-id"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "extension_creator_id.keyword")
				So(actualTermQuery, ShouldEqual, req.ExtensionCreatorId)
			})

			Convey("Contains correct term query when extension_creator_login in request", func() {
				req.ExtensionCreatorLogin = "test-extension-creator-login"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "extension_creator_login.keyword")
				So(actualTermQuery, ShouldEqual, req.ExtensionCreatorLogin)
			})

			Convey("Contains correct term query when anonymous_state in request", func() {
				req.AnonymousState = "test-anonymous-state"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "anonymous_state.keyword")
				So(actualTermQuery, ShouldEqual, req.AnonymousState)
			})

			Convey("Contains correct term query when country_code in request", func() {
				req.CountryCode = "test-country-code"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "country_code.keyword")
				So(actualTermQuery, ShouldEqual, req.CountryCode)
			})

			Convey("Contains correct term query when city in request", func() {
				req.City = "test-city"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "city.keyword")
				So(actualTermQuery, ShouldEqual, req.City)
			})

			Convey("Contains correct term query when postal_code in request", func() {
				req.PostalCode = "test-postal-code"
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualTermQuery := extractBoolMustTermQueryValue(esClient, "postal_code.keyword")
				So(actualTermQuery, ShouldEqual, req.PostalCode)
			})

			Convey("Contains correct range query when upper_bits_amount in request", func() {
				req.UpperBitsAmount = 10
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueInt(esClient, "bits_amount", "to")
				So(actualRangeQuery, ShouldEqual, req.UpperBitsAmount)
			})

			Convey("Contains correct range query when lower_bits_amount in request", func() {
				req.LowerBitsAmount = 5
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueInt(esClient, "bits_amount", "from")
				So(actualRangeQuery, ShouldEqual, req.LowerBitsAmount)
			})

			Convey("Contains correct range query when upper_bits_balance in request", func() {
				req.UpperBitsBalance = 1000
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueInt(esClient, "bits_balance", "to")
				So(actualRangeQuery, ShouldEqual, req.UpperBitsBalance)
			})

			Convey("Contains correct range query when lower_bits_balance in request", func() {
				req.LowerBitsBalance = 500
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueInt(esClient, "bits_balance", "from")
				So(actualRangeQuery, ShouldEqual, req.LowerBitsBalance)
			})

			Convey("Contains correct range query when upper_amount in request", func() {
				req.UpperAmount = 20
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueInt(esClient, "amount", "to")
				So(actualRangeQuery, ShouldEqual, req.UpperAmount)
			})

			Convey("Contains correct range query when lower_amount in request", func() {
				req.LowerAmount = 10
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueInt(esClient, "amount", "from")
				So(actualRangeQuery, ShouldEqual, req.LowerAmount)
			})

			Convey("Contains correct range query when upper_currency_amount in request", func() {
				req.UpperCurrencyAmount = 12.34
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueFloat(esClient, "currency_amount", "to")
				So(actualRangeQuery, ShouldEqual, req.UpperCurrencyAmount)
			})

			Convey("Contains correct range query when lower_currency_amount in request", func() {
				req.LowerCurrencyAmount = 43.21
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueFloat(esClient, "currency_amount", "from")
				So(actualRangeQuery, ShouldEqual, req.LowerCurrencyAmount)
			})

			Convey("Contains correct range query when start_event_time in request", func() {
				now := time.Now()
				timestamp, terr := ptypes.TimestampProto(now)
				So(terr, ShouldBeNil)
				req.StartEventTime = timestamp
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueTime(esClient, "event_time", "from")
				So(actualRangeQuery, ShouldEqual, now)
			})

			Convey("Contains correct range query when end_event_time in request", func() {
				now := time.Now()
				timestamp, terr := ptypes.TimestampProto(now)
				So(terr, ShouldBeNil)
				req.EndEventTime = timestamp
				esClient.On("Search", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, search.DefaultLimit, 0, mock.Anything, mock.Anything).Return(&elastic.SearchResult{}, nil)
				_, _, err := searcher.Search(context.Background(), req)
				So(err, ShouldBeNil)
				actualRangeQuery := extractBoolMustRangeQueryValueTime(esClient, "event_time", "to")
				So(actualRangeQuery, ShouldEqual, now)
			})
		})
	})
}
