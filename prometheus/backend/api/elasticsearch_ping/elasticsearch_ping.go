package elasticsearch_ping

import (
	"context"
	"time"

	"code.justin.tv/commerce/prometheus/backend/clients/elasticsearch"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	ESClient elasticsearch.Client `inject:""`
}

const (
	timeout = 50 * time.Millisecond
)

func (api *API) ElasticsearchPing(ctx context.Context, req *prometheus.ElasticsearchPingReq) (*prometheus.ElasticsearchPingResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	err := api.ESClient.Ping(ctx)
	if err != nil {
		log.WithError(err).Error("error calling elasticsearch ping")
		return nil, twirp.InternalError("failed to ping elasticsearch")
	}

	return &prometheus.ElasticsearchPingResp{
		Response: "OK",
	}, nil
}
