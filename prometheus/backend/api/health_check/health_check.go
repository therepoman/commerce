package health_check

import (
	"context"

	prometheus "code.justin.tv/commerce/prometheus/rpc"
)

type API struct {
}

func (api *API) HealthCheck(ctx context.Context, req *prometheus.HealthCheckReq) (*prometheus.HealthCheckResp, error) {
	return &prometheus.HealthCheckResp{
		Response: "OK",
	}, nil
}
