package search

import (
	"context"
	"time"

	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/search"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/edge/headers"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	Searcher search.Searcher `inject:""`
}

const (
	timeout = 500 * time.Millisecond
)

func (api *API) Search(ctx context.Context, req *prometheus.SearchReq) (*prometheus.SearchResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("event_type", "request cannot be nil")
	}

	headerContext := headers.GetContext(ctx)

	log.WithFields(log.Fields{
		"client_id": headerContext.ClientID,
		"client_ip": headerContext.ClientIP,
		"user_id":   headerContext.UserID,
		"req":       *req,
	}).Info("Search request")

	events, total, err := api.Searcher.Search(ctx, req)
	if err != nil {
		log.WithError(err).Error("error searching")
		return nil, twirp.InternalError("error searching")
	}

	return &prometheus.SearchResp{
		Events: models.ToTWIRPModels(events),
		Total:  total,
	}, nil
}
