package search_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/api/search"
	"code.justin.tv/commerce/prometheus/backend/models"
	search_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/search"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/commerce/prometheus/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_Search(t *testing.T) {
	Convey("Given a search API", t, func() {
		searcher := new(search_mock.Searcher)
		api := search.API{
			Searcher: searcher,
		}

		Convey("Errors when request is nil", func() {
			_, err := api.Search(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when searcher errors", func() {
			searcher.On("Search", mock.Anything, mock.Anything).Return(nil, int64(0), errors.New("test error"))
			_, err := api.Search(context.Background(), &prometheus.SearchReq{})
			test.AssertTwirpError(err, twirp.Internal)
		})

		Convey("Succeeds when searcher succeeds", func() {
			searcher.On("Search", mock.Anything, mock.Anything).Return([]models.Event{{EventID: "1"}, {EventID: "2"}}, int64(2), nil)
			resp, err := api.Search(context.Background(), &prometheus.SearchReq{})
			So(err, ShouldBeNil)
			So(len(resp.Events), ShouldEqual, 2)
			So(resp.Total, ShouldEqual, 2)
			So(resp.Events[0].EventId, ShouldEqual, "1")
			So(resp.Events[1].EventId, ShouldEqual, "2")
		})
	})
}
