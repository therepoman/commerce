package publish_event_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/api/publish_event"
	publish_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/publish"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/commerce/prometheus/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_PublishEvent(t *testing.T) {
	Convey("Given a PublishEvent API", t, func() {
		publisher := new(publish_mock.EventPublisher)
		api := &publish_event.API{
			Publisher: publisher,
			Statter:   &statsd.NoopClient{},
		}

		Convey("Errors when the request is nil", func() {
			_, err := api.PublishEvent(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event type is blank", func() {
			_, err := api.PublishEvent(context.Background(), &prometheus.PublishEventReq{
				EventType:    "",
				EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
				EventId:      "event-id",
				EventTime:    &timestamp.Timestamp{Seconds: 10},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event sub type is invalid", func() {
			_, err := api.PublishEvent(context.Background(), &prometheus.PublishEventReq{
				EventType:    "type",
				EventSubType: prometheus.EventSubType(-1),
				EventId:      "event-id",
				EventTime:    &timestamp.Timestamp{Seconds: 10},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event id id blank", func() {
			_, err := api.PublishEvent(context.Background(), &prometheus.PublishEventReq{
				EventType:    "type",
				EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
				EventId:      "",
				EventTime:    &timestamp.Timestamp{Seconds: 10},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event time is nil", func() {
			_, err := api.PublishEvent(context.Background(), &prometheus.PublishEventReq{
				EventType:    "type",
				EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
				EventId:      "event-id",
				EventTime:    nil,
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &prometheus.PublishEventReq{
				EventType:    "type",
				EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
				EventId:      "event-id",
				EventTime:    &timestamp.Timestamp{Seconds: 10},
			}

			Convey("Errors when publisher errors", func() {
				publisher.On("PublishEvent", mock.Anything, mock.Anything).Return(errors.New("test error"))
				_, err := api.PublishEvent(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("Succeeds when publisher succeeds", func() {
				publisher.On("PublishEvent", mock.Anything, mock.Anything).Return(nil)
				_, err := api.PublishEvent(context.Background(), req)
				So(err, ShouldBeNil)
			})
		})
	})
}
