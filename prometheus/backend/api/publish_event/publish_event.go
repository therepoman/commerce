package publish_event

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/publish"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	Publisher publish.EventPublisher `inject:""`
	Statter   statsd.Statter         `inject:""`
}

const (
	timeout = 100 * time.Millisecond
)

func (api *API) PublishEvent(ctx context.Context, req *prometheus.PublishEventReq) (*prometheus.PublishEventResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	var eventSubType string
	var ok bool

	if req == nil {
		return nil, twirp.InvalidArgumentError("event_type", "request cannot be nil")
	} else if strings.Blank(req.EventType) {
		return nil, twirp.InvalidArgumentError("event_type", "event_type cannot be blank")
	} else if eventSubType, ok = prometheus.EventSubType_name[int32(req.EventSubType)]; !ok {
		return nil, twirp.InvalidArgumentError("event_sub_type", "invalid event_sub_type")
	} else if strings.Blank(req.EventId) {
		return nil, twirp.InvalidArgumentError("event_id", "event_id cannot be blank")
	}

	eventTime, err := ptypes.Timestamp(req.EventTime)
	if err != nil {
		return nil, twirp.InvalidArgumentError("event_time", "invalid event time")
	}

	go func() {
		eventTypeErr := api.Statter.Inc(fmt.Sprintf("publish-event-type-%s", req.EventType), 1, 1)
		if eventTypeErr != nil {
			log.WithError(eventTypeErr).Error("error statting publish event type")
		}

		eventSubTypeErr := api.Statter.Inc(fmt.Sprintf("publish-event-subtype-%s", eventSubType), 1, 1)
		if eventSubTypeErr != nil {
			log.WithError(eventSubTypeErr).Error("error statting publish event sub type")
		}
	}()

	event := models.Event{
		EventType:             req.EventType,
		EventSubType:          eventSubType,
		EventID:               req.EventId,
		EventTime:             eventTime,
		UserID:                req.UserId,
		ChannelID:             req.ChannelId,
		AnonymousState:        req.AnonymousState,
		GiftRecipientId:       req.GiftRecipientId,
		BitsAmount:            req.BitsAmount,
		BitsBalance:           req.BitsBalance,
		Amount:                req.Amount,
		CurrencyAmount:        req.CurrencyAmount,
		CurrencyCode:          req.CurrencyCode,
		OrderId:               req.OrderId,
		PurchaseOrderState:    req.PurchaseOrderState,
		PaymentService:        req.PaymentService,
		PaymentGateway:        req.PaymentGateway,
		Message:               req.Message,
		AdminUser:             req.AdminUser,
		AdminMessage:          req.AdminMessage,
		ExtensionId:           req.ExtensionId,
		ExtensionSku:          req.ExtensionSku,
		ExtensionCreatorId:    req.ExtensionCreatorId,
		ExtensionCreatorLogin: req.ExtensionCreatorLogin,
		CountryCode:           req.CountryCode,
		City:                  req.City,
		PostalCode:            req.PostalCode,
		PollID:                req.PollId,
		PollChoiceID:          req.PollChoiceId,
		HoldID:                req.HoldId,
	}

	err = api.Publisher.PublishEvent(ctx, event)
	if err != nil {
		log.WithError(err).WithField("event", event).Error("error publishing event to SNS")
		return nil, twirp.InternalError("error publishing event")
	}

	return &prometheus.PublishEventResp{
		Response: "OK",
	}, nil
}
