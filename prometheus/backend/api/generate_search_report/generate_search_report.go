package generate_search_report

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/search"
	"code.justin.tv/commerce/prometheus/config"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	MaxEntries            = 5000 //The first {MaxEntries} number of the events will be in the report
	S3Prefix              = "https://s3-us-west-2.amazonaws.com"
	LDAPUserContextKey    = "LDAPUser"
	S3PresignedExpiryTime = 5 * time.Minute
)

type API struct {
	Searcher search.Searcher `inject:""`
	S3Client s3.Client       `inject:""`
	Config   *config.Config  `inject:""`
}

func (api *API) GenerateSearchReport(ctx context.Context, req *prometheus.GenerateSearchReportReq) (*prometheus.GenerateSearchReportResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("event_type", "request cannot be nil")
	}

	reportUUID, err := uuid.NewV4()
	if err != nil {
		log.WithError(err).Error("error generating UUID")
		return nil, twirp.InternalError("error generating UUID")
	}
	reportID := reportUUID.String()

	events, total, err := api.Searcher.Search(ctx, req.SearchReq)
	if err != nil {
		log.WithError(err).Error("error searching")
		return nil, twirp.InternalError("error searching")
	}

	if total > MaxEntries {
		total = MaxEntries
	}

	byteBuffer := bytes.NewBuffer([]byte{})
	csvWriter := csv.NewWriter(byteBuffer)

	err = csvWriter.Write(models.GetCSVHeader())
	if err != nil {
		return nil, twirp.InternalError("error creating csv file")
	}

	for index, event := range events {
		if index >= MaxEntries {
			break
		}

		record := event.ToCSVRecord()
		err = csvWriter.Write(record)
		if err != nil {
			return nil, twirp.InternalError("error while generating csv file")
		}
	}
	csvWriter.Flush()

	err = api.S3Client.PutFile(ctx, api.Config.SearchReportsS3Bucket, getS3Key(ctx, reportID), bytes.NewReader(byteBuffer.Bytes()))
	if err != nil {
		return nil, twirp.InternalError("error while saving csv file to s3")
	}

	presignedURL, err := api.getReportURL(ctx, reportID)
	if err != nil {
		return nil, twirp.InternalError("error while generating presigned s3 url")
	}

	return &prometheus.GenerateSearchReportResp{
		ReportS3URL: presignedURL,
		Total:       total,
	}, nil
}

func getS3Key(ctx context.Context, reportID string) string {
	ldapUser, _ := ctx.Value(LDAPUserContextKey).(string)
	if ldapUser != "" {
		return fmt.Sprintf("%s/%s.csv", ldapUser, reportID)
	} else {
		return fmt.Sprintf("anonymous/%s.csv", reportID)
	}
}

func (api *API) getReportURL(ctx context.Context, reportID string) (string, error) {
	return api.S3Client.GetTempSignedURL(ctx, api.Config.SearchReportsS3Bucket, getS3Key(ctx, reportID), S3PresignedExpiryTime)
}
