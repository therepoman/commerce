package generate_search_report

import (
	"context"
	"errors"
	"testing"

	s3_mock "code.justin.tv/commerce/gogogadget/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/config"
	search_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/search"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/commerce/prometheus/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GenerateSearchReport(t *testing.T) {
	Convey("Given a GenerateSearchReport API", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		if err != nil {
			t.Fail()
		}

		searcher := new(search_mock.Searcher)
		s3client := new(s3_mock.Client)
		api := API{
			Searcher: searcher,
			S3Client: s3client,
			Config:   cfg,
		}

		Convey("When the request is nil", func() {
			resp, err := api.GenerateSearchReport(context.Background(), nil)
			So(err, ShouldNotBeNil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
			So(resp, ShouldBeNil)
		})

		Convey("When the request is not nil", func() {
			req := &prometheus.GenerateSearchReportReq{
				SearchReq: &prometheus.SearchReq{
					EventType: "Bits",
					UserId:    "pho-cyclo",
					Limit:     5000,
				},
			}

			Convey("When Searcher returns an error", func() {
				searcher.On("Search", mock.Anything, mock.Anything).Return([]models.Event{}, int64(0), errors.New("you are screwed"))
				resp, err := api.GenerateSearchReport(context.Background(), req)
				So(err, ShouldNotBeNil)
				test.AssertTwirpError(err, twirp.Internal)
				So(resp, ShouldBeNil)
			})

			Convey("When Searcher succeeds", func() {
				searcher.On("Search", mock.Anything, mock.Anything).Return([]models.Event{
					{
						EventType:  "Bits",
						UserID:     "pho-cyclo",
						BitsAmount: int64(100),
						EventID:    "cafe",
					},
					{
						EventType:  "Bits",
						UserID:     "pho-cyclo",
						BitsAmount: int64(100),
						EventID:    "mexican food",
					},
				}, int64(2), nil)

				Convey("When S3 PutFile request fails", func() {
					s3client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Piroshki is not good"))
					resp, err := api.GenerateSearchReport(context.Background(), req)
					So(err, ShouldNotBeNil)
					test.AssertTwirpError(err, twirp.Internal)
					So(resp, ShouldBeNil)
				})

				Convey("When S3 PutFile request succeeds", func() {
					s3client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

					Convey("When S3 GetTempSignedURL fails", func() {
						s3client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("", errors.New("KFC is probably the best chicken"))

						resp, err := api.GenerateSearchReport(context.Background(), req)
						So(err, ShouldNotBeNil)
						test.AssertTwirpError(err, twirp.Internal)
						So(resp, ShouldBeNil)
					})

					Convey("When S3 GetTempSignedURL succeeds", func() {
						s3client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("https://s3.amazon.ohki/your-s3-file-is-here", nil)

						resp, err := api.GenerateSearchReport(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.Total, ShouldEqual, 2)
					})
				})
			})
		})
	})
}
