package publish_events

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/publish"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	Publisher publish.EventPublisher `inject:""`
}

const (
	timeout = 1 * time.Second
)

func (api *API) PublishEvents(ctx context.Context, req *prometheus.PublishEventsReq) (*prometheus.PublishEventsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("events", "request cannot be nil")
	} else if len(req.Events) <= 0 {
		return nil, twirp.InvalidArgumentError("events", "request must have at least one event")
	}

	events := make([]models.Event, 0)
	for _, e := range req.Events {
		var eventSubType string
		var ok bool
		if strings.Blank(e.EventType) {
			return nil, twirp.InvalidArgumentError("event_type", "event_type cannot be blank")
		} else if eventSubType, ok = prometheus.EventSubType_name[int32(e.EventSubType)]; !ok {
			return nil, twirp.InvalidArgumentError("event_sub_type", "invalid event_sub_type")
		} else if strings.Blank(e.EventId) {
			return nil, twirp.InvalidArgumentError("event_id", "event_id cannot be blank")
		}

		eventTime, err := ptypes.Timestamp(e.EventTime)
		if err != nil {
			return nil, twirp.InvalidArgumentError("event_time", "invalid event time")
		}

		event := models.Event{
			EventType:             e.EventType,
			EventSubType:          eventSubType,
			EventID:               e.EventId,
			EventTime:             eventTime,
			UserID:                e.UserId,
			ChannelID:             e.ChannelId,
			GiftRecipientId:       e.GiftRecipientId,
			BitsAmount:            e.BitsAmount,
			Amount:                e.Amount,
			CurrencyAmount:        e.CurrencyAmount,
			CurrencyCode:          e.CurrencyCode,
			OrderId:               e.OrderId,
			PurchaseOrderState:    e.PurchaseOrderState,
			PaymentService:        e.PaymentService,
			PaymentGateway:        e.PaymentGateway,
			Message:               e.Message,
			AdminUser:             e.AdminUser,
			AdminMessage:          e.AdminMessage,
			ExtensionId:           e.ExtensionId,
			ExtensionSku:          e.ExtensionSku,
			ExtensionCreatorId:    e.ExtensionCreatorId,
			ExtensionCreatorLogin: e.ExtensionCreatorLogin,
		}
		events = append(events, event)
	}

	for _, e := range events {
		err := api.Publisher.PublishEvent(ctx, e)
		if err != nil {
			log.WithError(err).Error("error publishing event")
			return nil, twirp.InternalError("error publishing event")
		}
	}

	return &prometheus.PublishEventsResp{
		Response: "OK",
	}, nil
}
