package publish_events_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/api/publish_events"
	publish_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/publish"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/commerce/prometheus/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_PublishEvent(t *testing.T) {
	Convey("Given a PublishEvents API", t, func() {
		publisher := new(publish_mock.EventPublisher)
		api := &publish_events.API{
			Publisher: publisher,
		}

		Convey("Errors when the request is nil", func() {
			_, err := api.PublishEvents(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event type is blank", func() {
			_, err := api.PublishEvents(context.Background(), &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "event-id",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
				},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event sub type is invalid", func() {
			_, err := api.PublishEvents(context.Background(), &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType(-1),
						EventId:      "event-id",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
				},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event id id blank", func() {
			_, err := api.PublishEvents(context.Background(), &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
				},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when event time is nil", func() {
			_, err := api.PublishEvents(context.Background(), &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "event-id",
						EventTime:    nil,
					},
				},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors (without calling publisher) when one event is invalid", func() {
			_, err := api.PublishEvents(context.Background(), &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "1",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "", // invalid
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "3",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
				},
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
			So(len(publisher.Calls), ShouldEqual, 0)
		})

		Convey("Given a valid request with a single event", func() {
			req := &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "event-id",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
				},
			}

			Convey("Errors when publisher errors", func() {
				publisher.On("PublishEvent", mock.Anything, mock.Anything).Return(errors.New("test error"))
				_, err := api.PublishEvents(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
				So(len(publisher.Calls), ShouldEqual, 1)
			})

			Convey("Succeeds when publisher succeeds", func() {
				publisher.On("PublishEvent", mock.Anything, mock.Anything).Return(nil)
				_, err := api.PublishEvents(context.Background(), req)
				So(err, ShouldBeNil)
				So(len(publisher.Calls), ShouldEqual, 1)
			})
		})

		Convey("Given a valid request with multiple events", func() {
			req := &prometheus.PublishEventsReq{
				Events: []*prometheus.PublishEventReq{
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "event-id-1",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "event-id-2",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
					{
						EventType:    "type",
						EventSubType: prometheus.EventSubType_AddBitsToCustomerAccountPurchase,
						EventId:      "event-id-3",
						EventTime:    &timestamp.Timestamp{Seconds: 10},
					},
				},
			}

			Convey("Errors when publisher errors", func() {
				publisher.On("PublishEvent", mock.Anything, mock.Anything).Return(errors.New("test error"))
				_, err := api.PublishEvents(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
				So(len(publisher.Calls), ShouldEqual, 1)
			})

			Convey("Succeeds when publisher succeeds", func() {
				publisher.On("PublishEvent", mock.Anything, mock.Anything).Return(nil)
				_, err := api.PublishEvents(context.Background(), req)
				So(err, ShouldBeNil)
				So(len(publisher.Calls), ShouldEqual, 3)
			})
		})
	})
}
