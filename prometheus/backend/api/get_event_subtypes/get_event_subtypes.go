package get_event_subtypes

import (
	"context"

	prometheus "code.justin.tv/commerce/prometheus/rpc"
)

type API struct {
}

func (api *API) GetEventSubtypes(ctx context.Context, req *prometheus.GetEventSubtypesReq) (*prometheus.GetEventSubtypesResp, error) {
	var subtypes []prometheus.EventSubType

	for i := 0; i < len(prometheus.EventSubType_name); i++ {
		subtypes = append(subtypes, prometheus.EventSubType(i))
	}

	return &prometheus.GetEventSubtypesResp{
		EventSubtypes: subtypes,
	}, nil
}
