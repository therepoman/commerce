package get_event_subtypes_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/api/get_event_subtypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI_GetEventSubtypes(t *testing.T) {
	Convey("Given a GetEventSubtypes API", t, func() {
		api := get_event_subtypes.API{}

		Convey("Returns a non-empty string array when called", func() {
			resp, err := api.GetEventSubtypes(context.Background(), nil)
			So(err, ShouldBeNil)
			So(len(resp.EventSubtypes), ShouldNotEqual, 0)
		})
	})
}
