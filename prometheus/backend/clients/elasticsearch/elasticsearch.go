package elasticsearch

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/prometheus/backend/circuit"
	"code.justin.tv/commerce/prometheus/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	awsauth "github.com/smartystreets/go-aws-auth"
	"golang.org/x/net/proxy"
)

type Client interface {
	Ping(ctx context.Context) error
	PublishEvent(ctx context.Context, indexName string, eventType string, elasticSearchID string, eventJSON string) error
	PublishEventsBulk(ctx context.Context, indexName string, eventType string, bulkRequests []BulkRequest) error
	Search(ctx context.Context, indexName string, eventType string, limit int, offset int, query elastic.Query, sort elastic.Sorter) (*elastic.SearchResult, error)
}

type clientImpl struct {
	esClient   *elastic.Client
	esEndpoint string
}

type awsSigningTransport struct {
	client *http.Client
}

func (a awsSigningTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	return a.client.Do(awsauth.Sign4(req))
}

func NewClient(cfg config.Config) (Client, error) {
	if strings.Blank(cfg.ElasticsearchEndpoint) {
		return &clientImpl{}, nil
	}

	httpTransporter := &http.Transport{}

	if cfg.UseSocksProxy {
		dialer, err := proxy.SOCKS5("tcp", cfg.SocksProxyEndpoint, nil, proxy.Direct)
		if err != nil {
			return nil, err
		}

		//nolint:staticcheck
		httpTransporter.Dial = dialer.Dial
	}
	httpClient := &http.Client{
		Transport: httpTransporter,
	}

	signingClient := &http.Client{
		Transport: awsSigningTransport{
			client: httpClient,
		},
	}

	client, err := elastic.NewSimpleClient(
		elastic.SetURL(cfg.ElasticsearchEndpoint),
		elastic.SetScheme("https"),
		elastic.SetHttpClient(signingClient),
	)

	if err != nil {
		return nil, err
	}

	return &clientImpl{
		esClient:   client,
		esEndpoint: cfg.ElasticsearchEndpoint,
	}, nil
}

func (c *clientImpl) Ping(ctx context.Context) error {
	var code int
	err := hystrix.Do(circuit.ESPing, func() error {
		var err error
		_, code, err = c.esClient.Ping(c.esEndpoint).Do(ctx)
		return err
	}, nil)
	if err != nil {
		return err
	}

	if code != 200 {
		return fmt.Errorf("elasticsearch returned a non-200 status code: %d", code)
	}

	return nil
}

func (c *clientImpl) Search(ctx context.Context, indexName string, eventType string, limit int, offset int, query elastic.Query, sort elastic.Sorter) (*elastic.SearchResult, error) {
	var searchResult *elastic.SearchResult
	err := hystrix.Do(circuit.ESSearch, func() error {
		search := c.esClient.Search(indexName).Size(limit).From(offset)
		if strings.NotBlank(eventType) {
			search = search.Type(eventType)
		}
		if query != nil {
			search = search.Query(query)
		}
		if sort != nil {
			search = search.SortBy(sort)
		}

		var err error
		searchResult, err = search.Do(ctx)
		return err
	}, nil)
	if err != nil {
		return nil, err
	}

	if searchResult == nil {
		return nil, errors.New("received nil search result")
	}

	return searchResult, nil
}

func (c *clientImpl) PublishEvent(ctx context.Context, indexName string, eventType string, elasticSearchID string, eventJSON string) error {
	var result *elastic.IndexResponse
	err := hystrix.Do(circuit.ESPublishEvent, func() error {
		var err error
		result, err = c.esClient.Index().
			Index(indexName).
			Type(eventType).
			Id(elasticSearchID).
			BodyJson(eventJSON).
			Refresh("true").
			Do(ctx)
		return err
	}, nil)

	if err != nil {
		return err
	}

	if result == nil {
		return errors.New("received a nil response from index operation")
	}

	return nil
}

type BulkRequest struct {
	ElasticSearchID string
	Document        interface{}
}

func (c *clientImpl) PublishEventsBulk(ctx context.Context, indexName string, eventType string, bulkRequests []BulkRequest) error {
	bulkService := c.esClient.Bulk()

	for _, bulkRequest := range bulkRequests {
		eBulkRequest := elastic.NewBulkIndexRequest().
			Index(indexName).
			Type(eventType).
			Id(bulkRequest.ElasticSearchID).
			Doc(bulkRequest.Document)
		bulkService = bulkService.Add(eBulkRequest)
	}

	var resp *elastic.BulkResponse
	err := hystrix.Do(circuit.ESPublishEventBulk, func() error {
		var err error
		resp, err = bulkService.Do(ctx)
		return err
	}, nil)
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.New("received a nil response from index operation")
	}

	if resp.Errors {
		failedItems := resp.Failed()
		if len(failedItems) > 0 {
			item := failedItems[0]
			log.WithFields(log.Fields{
				"errorCount": len(failedItems),
				"reason":     item.Error.Reason,
				"code":       item.Status,
			}).Error("bulk publish operation encountered errors, logging first error")
		}
		return errors.New("bulk publish operation encountered errors")
	}
	return nil
}
