package redis

import (
	"time"

	"code.justin.tv/commerce/prometheus/config"
	"github.com/go-redis/redis"
)

type Client interface {
	ExpireAt(key string, tm time.Time) (bool, error)
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
}

type client struct {
	redisClient redis.Cmdable
}

func NewClient(cfg config.Config) Client {

	if cfg.UseRedisClusterMode {
		redisClient := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{cfg.RedisEndpoint},
		})

		return &client{
			redis.Cmdable(redisClient),
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.RedisEndpoint,
	})

	return &client{
		redis.Cmdable(redisClient),
	}
}

func (c *client) ExpireAt(key string, tm time.Time) (bool, error) {
	return c.redisClient.ExpireAt(key, tm).Result()
}

func (c *client) Get(key string) (string, error) {
	return c.redisClient.Get(key).Result()
}

func (c *client) Set(key string, value interface{}, expiration time.Duration) error {
	return c.redisClient.Set(key, value, expiration).Err()
}
