package user

import (
	"code.justin.tv/commerce/prometheus/config"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client/usersclient_internal"
)

func NewClient(cfg config.Config, transport twitchclient.TransportConf) (usersclient_internal.InternalClient, error) {
	userServiceConfig := twitchclient.ClientConf{
		Host:      cfg.UserServiceEndpoint,
		Transport: transport,
	}

	usersServiceClient, err := usersclient_internal.NewClient(userServiceConfig)
	if err != nil {
		return nil, err
	}
	return usersServiceClient, nil
}
