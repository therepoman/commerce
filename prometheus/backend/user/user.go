package user

import (
	"context"
	"regexp"
	"time"

	prometheus_redis "code.justin.tv/commerce/prometheus/backend/clients/redis"
	"code.justin.tv/commerce/prometheus/config"
	"code.justin.tv/web/users-service/client"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
)

const (
	defaultUserIdCache = time.Duration(time.Hour)
)

type Loader interface {
	GetLoginFromID(ctx context.Context, userID string) (string, error)
}

type LoaderImpl struct {
	Config             *config.Config          `inject:""`
	RedisCache         prometheus_redis.Client `inject:""`
	UsersServiceClient users.InternalClient    `inject:""`
}

var testIdRegex, _ = regexp.Compile("(^7357[0-9]{14})|(^perf)") // 18-digit numbers that begin with 7357

/* Determines if a user ID belongs to a test (fake) account */
func IsATestAccount(userId string) bool {
	return testIdRegex.MatchString(userId)
}

var whitelistedTUIDS = map[string]string{
	"0":           "",
	"168267426":   "",
	"43223213214": "",
}

func (l *LoaderImpl) GetLoginFromID(ctx context.Context, userID string) (string, error) {
	if IsATestAccount(userID) {
		return "", nil
	}

	if userID == "" {
		return "", nil
	}

	whitelistedLogin, ok := whitelistedTUIDS[userID]
	if ok {
		return whitelistedLogin, nil
	}

	cachedLogin, err := l.RedisCache.Get(userID)
	if err == nil {
		return cachedLogin, nil
	} else if err != redis.Nil {
		log.WithError(err).WithField("user_id", userID).Error("Error fetching user login from redis")
	}

	user, err := l.UsersServiceClient.GetUserByID(ctx, userID, nil)
	if err != nil {
		if l.Config.AllowInvalidTuids && client.IsUserNotFound(err) {
			log.WithField("user_id", userID).Error("Using empty string as user name because user not found")
			return "", nil
		}
		return "", err
	}

	var login string
	if user != nil && user.Login != nil {
		login = *user.Login
	}

	err = l.RedisCache.Set(userID, login, defaultUserIdCache)
	if err != nil {
		log.WithError(err).WithFields(map[string]interface{}{"user_id": userID, "user_login": login}).Error("Error caching user id and login in redis")
	}
	return login, nil
}
