package user_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/user"
	"code.justin.tv/commerce/prometheus/config"
	redis_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/clients/redis"
	usersclient_internal_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/web/users-service/client"
	"code.justin.tv/web/users-service/models"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetLoginFromID(t *testing.T) {
	Convey("Given a loader", t, func() {
		userServiceClient := new(usersclient_internal_mock.InternalClient)
		redisClient := new(redis_mock.Client)
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		loader := &user.LoaderImpl{
			RedisCache:         redisClient,
			UsersServiceClient: userServiceClient,
			Config:             cfg,
		}

		Convey("Exits early for a test user", func() {
			login, err := loader.GetLoginFromID(context.Background(), "735711111111111111")
			So(login, ShouldEqual, "")
			So(err, ShouldBeNil)
			So(len(userServiceClient.Calls), ShouldEqual, 0)
		})

		Convey("Exits early if given an empty user ID", func() {
			login, err := loader.GetLoginFromID(context.Background(), "")
			So(login, ShouldEqual, "")
			So(err, ShouldBeNil)
			So(len(userServiceClient.Calls), ShouldEqual, 0)
		})

		Convey("Exits early for a cached user", func() {
			redisClient.On("Get", "123").Return("cached-login", nil)
			login, err := loader.GetLoginFromID(context.Background(), "123")
			So(login, ShouldEqual, "cached-login")
			So(err, ShouldBeNil)
			So(len(userServiceClient.Calls), ShouldEqual, 0)
		})

		Convey("Given the user was not found in the cache", func() {
			redisClient.On("Get", mock.Anything).Return("", errors.New("key not found"))

			Convey("Errors when user service errors", func() {
				userServiceClient.On("GetUserByID", mock.Anything, "456", mock.Anything).Return(nil, errors.New("test error"))
				login, err := loader.GetLoginFromID(context.Background(), "456")
				So(login, ShouldEqual, "")
				So(err, ShouldNotBeNil)
				So(len(userServiceClient.Calls), ShouldEqual, 1)
			})

			Convey("Returns and caches when user service succeeds", func() {
				expectedLogin := "login"
				userServiceClient.On("GetUserByID", mock.Anything, "789", mock.Anything).Return(&models.Properties{Login: &expectedLogin}, nil)
				redisClient.On("Set", "789", expectedLogin, mock.Anything).Return(nil)
				login, err := loader.GetLoginFromID(context.Background(), "789")
				So(login, ShouldEqual, expectedLogin)
				So(err, ShouldBeNil)
				So(len(userServiceClient.Calls), ShouldEqual, 1)
				redisCall := redisClient.Calls[1].Method
				cachedUserId := redisClient.Calls[1].Arguments.Get(0)
				cachedUserLogin := redisClient.Calls[1].Arguments.Get(1)
				So(redisCall, ShouldEqual, "Set")
				So(cachedUserId, ShouldEqual, "789")
				So(cachedUserLogin, ShouldEqual, expectedLogin)
			})

			Convey("Returns no errors when redis cache Set fails", func() {
				expectedLogin := "login"
				userServiceClient.On("GetUserByID", mock.Anything, "000", mock.Anything).Return(&models.Properties{Login: &expectedLogin}, nil)
				redisClient.On("Set", "000", expectedLogin, mock.Anything).Return(errors.New("failed to set cache"))
				login, err := loader.GetLoginFromID(context.Background(), "000")
				So(login, ShouldEqual, expectedLogin)
				So(err, ShouldBeNil)
			})
		})
	})

	Convey("Given a loader that allows invalid tuids", t, func() {
		userServiceClient := new(usersclient_internal_mock.InternalClient)
		redisClient := new(redis_mock.Client)
		cfg, err := config.LoadConfig(config.UnitTest)
		cfg.AllowInvalidTuids = true
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		loader := &user.LoaderImpl{
			RedisCache:         redisClient,
			UsersServiceClient: userServiceClient,
			Config:             cfg,
		}

		Convey("Given the user was not found in the cache", func() {
			redisClient.On("Get", mock.Anything).Return("", errors.New("key not found"))

			Convey("Succeeds and returns empty string when user service returns a UserNotFound error", func() {
				userServiceClient.On("GetUserByID", mock.Anything, "456", mock.Anything).Return(nil, &client.UserNotFoundError{})
				login, err := loader.GetLoginFromID(context.Background(), "456")
				So(login, ShouldEqual, "")
				So(err, ShouldBeNil)
				So(len(userServiceClient.Calls), ShouldEqual, 1)
			})

		})
	})
}
