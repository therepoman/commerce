package circuit

import afex "github.com/afex/hystrix-go/hystrix"

const (
	ESPing             = "es_ping"
	ESSearch           = "es_search"
	ESPublishEvent     = "es_publish_event"
	ESPublishEventBulk = "es_publish_event_bulk"

	// Concurrency
	DefaultMaxConcurrent = 600
)

func init() {
	configureTimeout(ESSearch, 500)
	configureTimeout(ESPublishEvent, 100)
	// If this API gets used, we might need to adjust this circuit. As of writing this,
	// I could not see any data of this API being used.
	configureTimeout(ESPing, 100)
	// If this API gets used, we might need to adjust this circuit. As of writing this,
	// I could not see any data of this API being used.
	configureTimeout(ESPublishEventBulk, 1000)
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func ConfigureTimeoutOverride(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func configure(cmd string, timeout int, maxConcurrent int) {
	afex.ConfigureCommand(cmd, afex.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: maxConcurrent,
		ErrorPercentThreshold: afex.DefaultErrorPercentThreshold,
	})
}
