package circuit

import (
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// HystrixMetricsCollector holds information about the circuit state.
// This implementation of MetricCollector is the canonical source of information about the circuit.
// It is used for for all internal hystrix operations
// including circuit health checks and metrics sent to the hystrix dashboard.
//
// Metric Collectors do not need Mutexes as they are updated by circuits within a locked context.
type HystrixMetricsCollector struct {
	Statter statsd.Statter `inject:""`
}

func NewHystrixMetricsCollector() *HystrixMetricsCollector {
	return &HystrixMetricsCollector{}
}

type HystrixCommandMetricsCollector struct {
	name string

	attemptsMetric          string
	errorsMetric            string
	successesMetric         string
	failuresMetric          string
	rejectsMetric           string
	shortCircuitsMetric     string
	timeoutsMetric          string
	fallbackSuccessesMetric string
	fallbackFailuresMetric  string
	contextCancelledMetric  string
	contextDeadlineMetric   string
	totalDurationMetric     string
	runDurationMetric       string

	Statter statsd.Statter
}

func (h *HystrixMetricsCollector) NewHystrixCommandMetricsCollector(name string) metricCollector.MetricCollector {
	statsPrefix := "hystrix." + name

	return &HystrixCommandMetricsCollector{
		name:                    name,
		attemptsMetric:          statsPrefix + ".attempts",
		errorsMetric:            statsPrefix + ".errors",
		failuresMetric:          statsPrefix + ".failures",
		rejectsMetric:           statsPrefix + ".rejects",
		shortCircuitsMetric:     statsPrefix + ".shortCircuits",
		timeoutsMetric:          statsPrefix + ".timeouts",
		fallbackSuccessesMetric: statsPrefix + ".fallbackSuccesses",
		fallbackFailuresMetric:  statsPrefix + ".fallbackFailures",
		contextCancelledMetric:  statsPrefix + ".contextCancelled",
		contextDeadlineMetric:   statsPrefix + ".contextDeadlineExceeded",
		totalDurationMetric:     statsPrefix + ".totalDuration",
		runDurationMetric:       statsPrefix + ".runDuration",
		Statter:                 h.Statter,
	}
}

func (d *HystrixCommandMetricsCollector) Update(r metricCollector.MetricResult) {
	go func() {
		var errs *multierror.Error
		err := d.Statter.Inc(d.attemptsMetric, int64(r.Attempts), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish attempts"))
		}
		err = d.Statter.Inc(d.errorsMetric, int64(r.Errors), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish errors"))
		}
		err = d.Statter.Inc(d.successesMetric, int64(r.Successes), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish successes"))
		}
		err = d.Statter.Inc(d.failuresMetric, int64(r.Failures), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish failures"))
		}
		err = d.Statter.Inc(d.rejectsMetric, int64(r.Rejects), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish rejects"))
		}
		err = d.Statter.Inc(d.shortCircuitsMetric, int64(r.ShortCircuits), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish short circuits"))
		}
		err = d.Statter.Inc(d.timeoutsMetric, int64(r.Timeouts), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish timeouts"))
		}
		err = d.Statter.Inc(d.fallbackSuccessesMetric, int64(r.FallbackSuccesses), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish fallback successes"))
		}
		err = d.Statter.Inc(d.fallbackFailuresMetric, int64(r.FallbackFailures), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish fallback failures"))
		}
		err = d.Statter.Inc(d.contextCancelledMetric, int64(r.ContextCanceled), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish context cancels"))
		}
		err = d.Statter.Inc(d.contextDeadlineMetric, int64(r.ContextDeadlineExceeded), 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish context deadline exceeded"))
		}

		err = d.Statter.TimingDuration(d.totalDurationMetric, r.TotalDuration, 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish total duration"))
		}
		err = d.Statter.TimingDuration(d.runDurationMetric, r.RunDuration, 1.0)
		if err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "failed to publish run duration"))
		}

		if errs != nil && errs.Len() > 0 {
			logrus.WithField("metric", d.name).
				WithError(errs.ErrorOrNil()).
				Warn("failed to publish metrics for hystrix circuit")
		}
	}()
}

// It's a noop because it's all set remotely
func (d *HystrixCommandMetricsCollector) Reset() {}
