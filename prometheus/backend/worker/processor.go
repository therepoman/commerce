package worker

import (
	"context"
	"sync"
	"time"

	"errors"

	"github.com/aws/aws-sdk-go/aws"
	aws_sqs "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	log "github.com/sirupsen/logrus"
)

type Processor interface {
	Shutdown(ctx context.Context) error
}

type MessagesHandler func(ctx context.Context, messages []*aws_sqs.Message) error

type Config struct {
	QueueURL                 string
	MaxMessages              int64
	WaitTimeSeconds          int64
	VisibilityTimeoutSeconds int64
	ProcessingCycleDelay     time.Duration
}

type SQSProcessor struct {
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
	config      Config
	handler     MessagesHandler
	sqsClient   sqsiface.SQSAPI
}

func StartProcessing(cfg Config, client sqsiface.SQSAPI, handler MessagesHandler) Processor {
	p := &SQSProcessor{
		config:      cfg,
		handler:     handler,
		sqsClient:   client,
		stopChannel: make(chan struct{}),
		waitGroup:   &sync.WaitGroup{},
	}
	p.startPolling()
	return p
}

func (p *SQSProcessor) startPolling() {
	go func() {
		log.WithField("queue", p.config.QueueURL).Info("starting polling for queue")
		for {
			select {
			case <-p.stopChannel: // signal for shutting down
				log.WithField("queue", p.config.QueueURL).Info("stopping polling for queue")
				return
			default:
				err := p.receiveMessages()
				if err != nil {
					log.WithField("queue", p.config.QueueURL).WithError(err).Error("error receiving messages from queue")
				}
			}
			time.Sleep(p.config.ProcessingCycleDelay)
		}
	}()
}

func (p *SQSProcessor) receiveMessages() error {
	p.waitGroup.Add(1)
	defer p.waitGroup.Done()

	rmi := &aws_sqs.ReceiveMessageInput{
		QueueUrl:              aws.String(p.config.QueueURL),
		AttributeNames:        []*string{aws.String("All")},
		MessageAttributeNames: []*string{aws.String("All")},
		MaxNumberOfMessages:   aws.Int64(p.config.MaxMessages),
		WaitTimeSeconds:       aws.Int64(p.config.WaitTimeSeconds),
		VisibilityTimeout:     aws.Int64(p.config.VisibilityTimeoutSeconds),
	}
	rmo, err := p.sqsClient.ReceiveMessage(rmi)
	if err != nil {
		return err
	}

	if rmo == nil || len(rmo.Messages) <= 0 {
		return nil
	}
	err = p.handler(context.Background(), rmo.Messages)
	if err != nil {
		return err
	}

	deletions := make([]*aws_sqs.DeleteMessageBatchRequestEntry, 0)
	for _, msg := range rmo.Messages {
		dmbre := &aws_sqs.DeleteMessageBatchRequestEntry{
			Id:            aws.String(*msg.MessageId),
			ReceiptHandle: aws.String(*msg.ReceiptHandle),
		}
		deletions = append(deletions, dmbre)
	}

	_, err = p.sqsClient.DeleteMessageBatch(&aws_sqs.DeleteMessageBatchInput{
		QueueUrl: aws.String(p.config.QueueURL),
		Entries:  deletions,
	})

	return err
}

func (p *SQSProcessor) Shutdown(ctx context.Context) error {
	// closing this channel stops the processor goroutine after the current message processing cycle
	close(p.stopChannel)

	// waits for the processor goroutine to finish its last message processing cycle
	if wait(ctx, p.waitGroup) {
		return errors.New("timed out while waiting for SQS processor to complete")
	}
	return nil
}

func wait(ctx context.Context, wg *sync.WaitGroup) bool {
	processorFinished := make(chan struct{})
	go func() {
		defer close(processorFinished)
		wg.Wait()
	}()

	select {
	case <-processorFinished:
		return false // done, no timeout
	case <-ctx.Done():
		return true // done, timed out
	}
}
