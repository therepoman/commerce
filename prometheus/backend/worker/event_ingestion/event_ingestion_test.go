package event_ingestion_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/worker/event_ingestion"
	ingestion_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/ingestion"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestIngestionHandler_HandleMessages(t *testing.T) {
	Convey("Given an ingestion handler", t, func() {
		ingester := new(ingestion_mock.Ingester)
		handler := &event_ingestion.IngestionHandler{
			Ingester: ingester,
		}

		Convey("Returns early with only nil messages", func() {
			err := handler.HandleMessages(context.Background(), []*sqs.Message{nil, nil})
			So(err, ShouldBeNil)
			So(len(ingester.Calls), ShouldEqual, 0)
		})

		Convey("Returns early with only nil body messages", func() {
			err := handler.HandleMessages(context.Background(), []*sqs.Message{{Body: nil}, {Body: nil}})
			So(err, ShouldBeNil)
			So(len(ingester.Calls), ShouldEqual, 0)
		})

		Convey("Errors when a message body has invalid JSON", func() {
			err := handler.HandleMessages(context.Background(), []*sqs.Message{{Body: nil}, {Body: aws.String("not valid json")}, {Body: nil}})
			So(err, ShouldNotBeNil)
			So(len(ingester.Calls), ShouldEqual, 0)
		})

		Convey("Errors when inner message body has invalid JSON", func() {
			err := handler.HandleMessages(context.Background(), []*sqs.Message{{Body: nil}, {Body: aws.String("{\"Message\":\"not valid json\"}")}, {Body: nil}})
			So(err, ShouldNotBeNil)
			So(len(ingester.Calls), ShouldEqual, 0)
		})

		Convey("Errors when ingester errors", func() {
			ingester.On("IngestBulk", mock.Anything, mock.Anything).Return(errors.New("test error"))
			err := handler.HandleMessages(context.Background(), []*sqs.Message{{Body: nil}, {Body: aws.String("{\"Message\":\"{}\"}")}, {Body: nil}})
			So(err, ShouldNotBeNil)
			So(len(ingester.Calls), ShouldEqual, 1)
		})

		Convey("Succeeds when ingester succeeds", func() {
			ingester.On("IngestBulk", mock.Anything, mock.Anything).Return(nil)
			err := handler.HandleMessages(context.Background(), []*sqs.Message{{Body: nil}, {Body: aws.String("{\"Message\":\"{}\"}")}, {Body: nil}})
			So(err, ShouldBeNil)
			So(len(ingester.Calls), ShouldEqual, 1)
		})
	})
}
