package event_ingestion

import (
	"context"

	"encoding/json"

	"code.justin.tv/commerce/prometheus/backend/ingestion"
	"code.justin.tv/commerce/prometheus/backend/models"
	aws_sqs "github.com/aws/aws-sdk-go/service/sqs"
)

type IngestionHandler struct {
	Ingester ingestion.Ingester `inject:""`
}

func (h *IngestionHandler) HandleMessages(ctx context.Context, messages []*aws_sqs.Message) error {
	events := make([]models.Event, 0)
	for _, msg := range messages {
		if msg == nil || msg.Body == nil {
			continue
		}

		var snsMsg models.SNSMessage
		err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
		if err != nil {
			return err
		}

		var event models.Event
		err = json.Unmarshal([]byte(snsMsg.Message), &event)
		if err != nil {
			return err
		}

		events = append(events, event)
	}

	if len(events) > 0 {
		return h.Ingester.IngestBulk(ctx, events)
	}
	return nil
}
