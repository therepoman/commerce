package ingestion

import (
	"context"

	"code.justin.tv/commerce/prometheus/backend/clients/elasticsearch"
	"code.justin.tv/commerce/prometheus/backend/models"
	"code.justin.tv/commerce/prometheus/backend/user"
	log "github.com/sirupsen/logrus"
)

type Ingester interface {
	Ingest(ctx context.Context, e models.Event) error
	IngestBulk(ctx context.Context, es []models.Event) error
}

type ElasticsearchIngester struct {
	ESClient   elasticsearch.Client `inject:""`
	UserLoader user.Loader          `inject:""`
}

func (i *ElasticsearchIngester) preprocessEvent(ctx context.Context, e models.Event) (models.Event, error) {
	userLogin, err := i.UserLoader.GetLoginFromID(ctx, e.UserID)
	if err != nil {
		log.WithError(err).WithField("user_id", e.UserID).Error("error getting login from user_id")
		return e, err
	}

	channelLogin, err := i.UserLoader.GetLoginFromID(ctx, e.ChannelID)
	if err != nil {
		log.WithError(err).WithField("channel_id", e.ChannelID).Error("error getting login from channel_id")
		return e, err
	}

	giftReceiverLogin, err := i.UserLoader.GetLoginFromID(ctx, e.GiftRecipientId)
	if err != nil {
		log.WithError(err).WithField("gift_recipient_id", e.GiftRecipientId).Error("error getting login from gift_recipient_id")
		return e, err
	}

	extensionCreatorLogin, err := i.UserLoader.GetLoginFromID(ctx, e.ExtensionCreatorId)
	if err != nil {
		log.WithError(err).WithField("extension_creator_id", e.ExtensionCreatorId).Error("error getting login from extension_creator_id")
		return e, err
	}

	e.UserLogin = userLogin
	e.ChannelLogin = channelLogin
	e.GiftRecipientLogin = giftReceiverLogin
	e.ExtensionCreatorLogin = extensionCreatorLogin

	return e, nil

}

func (i *ElasticsearchIngester) Ingest(ctx context.Context, e models.Event) error {
	e, err := i.preprocessEvent(ctx, e)
	if err != nil {
		return err
	}

	eJSON, err := e.ToJSON()
	if err != nil {
		return err
	}

	return i.ESClient.PublishEvent(ctx, models.ESEventIndexName, models.ESEventTypeName, e.ToElasticSearchID(), eJSON)
}

func (i *ElasticsearchIngester) IngestBulk(ctx context.Context, es []models.Event) error {
	bulkRequests := make([]elasticsearch.BulkRequest, 0)
	for _, e := range es {
		e, err := i.preprocessEvent(ctx, e)
		if err != nil {
			return err
		}

		bulkRequests = append(bulkRequests, elasticsearch.BulkRequest{
			ElasticSearchID: e.ToElasticSearchID(),
			Document:        e,
		})
	}

	return i.ESClient.PublishEventsBulk(ctx, models.ESEventIndexName, models.ESEventTypeName, bulkRequests)
}
