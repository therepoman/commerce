package ingestion_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prometheus/backend/ingestion"
	"code.justin.tv/commerce/prometheus/backend/models"
	elasticsearch_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/clients/elasticsearch"
	user_mock "code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/user"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestElasticsearchIngester_Ingest(t *testing.T) {
	Convey("Given an ES Ingester", t, func() {
		esClient := new(elasticsearch_mock.Client)
		userLoader := new(user_mock.Loader)
		esIngester := &ingestion.ElasticsearchIngester{
			ESClient:   esClient,
			UserLoader: userLoader,
		}

		Convey("Errors when User Service errors", func() {
			userLoader.On("GetLoginFromID", mock.Anything, mock.Anything).Return("", errors.New("test error"))
			err := esIngester.Ingest(context.Background(), models.Event{EventType: "T", EventSubType: "S", EventID: "1"})
			So(err, ShouldNotBeNil)
			So(len(userLoader.Calls), ShouldEqual, 1)
			So(len(esClient.Calls), ShouldEqual, 0)
		})

		Convey("Given User Service succeeds", func() {
			userLoader.On("GetLoginFromID", mock.Anything, mock.Anything).Return("login", nil)

			Convey("Errors when ES errors", func() {
				esClient.On("PublishEvent", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, "T:S:1", mock.Anything).Return(errors.New("test error"))
				err := esIngester.Ingest(context.Background(), models.Event{EventType: "T", EventSubType: "S", EventID: "1"})
				So(err, ShouldNotBeNil)
				So(len(userLoader.Calls), ShouldEqual, 4)
			})

			Convey("Succeeds when ES succeeds", func() {
				esClient.On("PublishEvent", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, "T:S:1", mock.Anything).Return(nil)
				err := esIngester.Ingest(context.Background(), models.Event{EventType: "T", EventSubType: "S", EventID: "1"})
				So(err, ShouldBeNil)
				So(len(userLoader.Calls), ShouldEqual, 4)
			})
		})
	})
}

func TestElasticsearchIngester_IngestBulk(t *testing.T) {
	Convey("Given an ES Ingester", t, func() {
		esClient := new(elasticsearch_mock.Client)
		userLoader := new(user_mock.Loader)
		esIngester := &ingestion.ElasticsearchIngester{
			ESClient:   esClient,
			UserLoader: userLoader,
		}

		Convey("Errors when User Service errors", func() {
			userLoader.On("GetLoginFromID", mock.Anything, mock.Anything).Return("", errors.New("test error"))
			err := esIngester.IngestBulk(context.Background(), []models.Event{{EventType: "T", EventSubType: "S", EventID: "1"}})
			So(err, ShouldNotBeNil)
			So(len(userLoader.Calls), ShouldEqual, 1)
			So(len(esClient.Calls), ShouldEqual, 0)
		})

		Convey("Given User Service succeeds", func() {
			userLoader.On("GetLoginFromID", mock.Anything, mock.Anything).Return("login", nil)

			Convey("Errors when ES errors", func() {
				esClient.On("PublishEventsBulk", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, mock.Anything).Return(errors.New("test error"))
				err := esIngester.IngestBulk(context.Background(), []models.Event{{EventType: "T", EventSubType: "S", EventID: "1"}})
				So(err, ShouldNotBeNil)
				So(len(userLoader.Calls), ShouldEqual, 4)
			})

			Convey("Succeeds when ES succeeds", func() {
				esClient.On("PublishEventsBulk", mock.Anything, models.ESEventIndexName, models.ESEventTypeName, mock.Anything).Return(nil)
				err := esIngester.IngestBulk(context.Background(), []models.Event{{EventType: "T", EventSubType: "S", EventID: "1"}})
				So(err, ShouldBeNil)
				So(len(userLoader.Calls), ShouldEqual, 4)
			})
		})
	})
}
