package server

import (
	"context"

	"code.justin.tv/commerce/prometheus/backend/api/elasticsearch_ping"
	"code.justin.tv/commerce/prometheus/backend/api/generate_search_report"
	"code.justin.tv/commerce/prometheus/backend/api/get_event_subtypes"
	"code.justin.tv/commerce/prometheus/backend/api/health_check"
	"code.justin.tv/commerce/prometheus/backend/api/publish_event"
	"code.justin.tv/commerce/prometheus/backend/api/publish_events"
	"code.justin.tv/commerce/prometheus/backend/api/search"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
)

type Server struct {
	HealthCheckAPI          *health_check.API           `inject:""`
	ElasticsearchPingAPI    *elasticsearch_ping.API     `inject:""`
	PublishEventAPI         *publish_event.API          `inject:""`
	PublishEventsAPI        *publish_events.API         `inject:""`
	SearchAPI               *search.API                 `inject:""`
	GetEventSubtypesAPI     *get_event_subtypes.API     `inject:""`
	GenerateSearchReportAPI *generate_search_report.API `inject:""`
}

func (s *Server) HealthCheck(ctx context.Context, req *prometheus.HealthCheckReq) (*prometheus.HealthCheckResp, error) {
	return s.HealthCheckAPI.HealthCheck(ctx, req)
}

func (s *Server) ElasticsearchPing(ctx context.Context, req *prometheus.ElasticsearchPingReq) (*prometheus.ElasticsearchPingResp, error) {
	return s.ElasticsearchPingAPI.ElasticsearchPing(ctx, req)
}

func (s *Server) PublishEvent(ctx context.Context, req *prometheus.PublishEventReq) (*prometheus.PublishEventResp, error) {
	return s.PublishEventAPI.PublishEvent(ctx, req)
}

func (s *Server) PublishEvents(ctx context.Context, req *prometheus.PublishEventsReq) (*prometheus.PublishEventsResp, error) {
	return s.PublishEventsAPI.PublishEvents(ctx, req)
}

func (s *Server) Search(ctx context.Context, req *prometheus.SearchReq) (*prometheus.SearchResp, error) {
	return s.SearchAPI.Search(ctx, req)
}

func (s *Server) GetEventSubtypes(ctx context.Context, req *prometheus.GetEventSubtypesReq) (*prometheus.GetEventSubtypesResp, error) {
	return s.GetEventSubtypesAPI.GetEventSubtypes(ctx, req)
}

func (s *Server) GenerateSearchReport(ctx context.Context, req *prometheus.GenerateSearchReportReq) (*prometheus.GenerateSearchReportResp, error) {
	return s.GenerateSearchReportAPI.GenerateSearchReport(ctx, req)
}
