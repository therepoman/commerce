package models

import (
	"time"
)

type SNSMessage struct {
	Type              string                         `json:"Type"`
	MessageID         string                         `json:"MessageId"`
	Token             string                         `json:"Token,optional"` // Only for subscribe and unsubscribe
	TopicArn          string                         `json:"TopicArn"`
	Subject           string                         `json:"Subject,optional"`      // Only for Notification
	Message           string                         `json:"Message"`               // contains a JSON SQSMessage
	SubscribeURL      string                         `json:"SubscribeURL,optional"` // Only for subscribe and unsubscribe
	Timestamp         time.Time                      `json:"Timestamp"`
	SignatureVersion  string                         `json:"SignatureVersion"`
	Signature         string                         `json:"Signature"`
	SigningCertURL    string                         `json:"SigningCertURL"`
	UnsubscribeURL    string                         `json:"UnsubscribeURL,optional"` // Only for notifications
	MessageAttributes map[string]SNSMessageAttribute `json:"MessageAttributes,optional"`
}

type SNSMessageAttribute struct {
	Type  string `json:"Type"`
	Value string `json:"Value"`
}
