package models

import (
	"encoding/json"
	"fmt"
	"time"

	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

const (
	ESEventTypeName  = "prometheus-event"
	ESEventIndexName = "prometheus-event-index"
)

type Event struct {
	EventType             string    `json:"event_type"`
	EventSubType          string    `json:"event_sub_type"`
	EventID               string    `json:"event_id"`
	EventTime             time.Time `json:"event_time"`
	UserID                string    `json:"user_id"`
	UserLogin             string    `json:"user_login"`
	ChannelID             string    `json:"channel_id"`
	ChannelLogin          string    `json:"channel_login"`
	AnonymousState        string    `json:"anonymous_state"`
	GiftRecipientId       string    `json:"gift_recipient_id"`
	GiftRecipientLogin    string    `json:"gift_recipient_login"`
	BitsAmount            int64     `json:"bits_amount"`
	BitsBalance           int64     `json:"bits_balance"`
	Amount                int64     `json:"amount"`
	CurrencyAmount        float32   `json:"currency_amount"`
	CurrencyCode          string    `json:"currency_code"`
	OrderId               string    `json:"order_id"`
	PurchaseOrderState    string    `json:"purchase_order_state"`
	PaymentService        string    `json:"payment_service"`
	PaymentGateway        string    `json:"payment_gateway"`
	Message               string    `json:"message"`
	AdminUser             string    `json:"admin_user"`
	AdminMessage          string    `json:"admin_message"`
	ExtensionId           string    `json:"extension_id"`
	ExtensionSku          string    `json:"extension_sku"`
	ExtensionCreatorId    string    `json:"extension_creator_id"`
	ExtensionCreatorLogin string    `json:"extension_creator_login"`
	CountryCode           string    `json:"country_code"`
	City                  string    `json:"city"`
	PostalCode            string    `json:"postal_code"`
	PollID                string    `json:"poll_id"`
	PollChoiceID          string    `json:"poll_choice_id"`
	HoldID                string    `json:"hold_id"`
}

func (e Event) ToElasticSearchID() string {
	return fmt.Sprintf("%s:%s:%s", e.EventType, e.EventSubType, e.EventID)
}

func (e Event) ToJSON() (string, error) {
	jsonBytes, err := json.Marshal(&e)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}

func ToTWIRPModels(events []Event) []*prometheus.Event {
	twirpEvents := make([]*prometheus.Event, len(events))
	for i, e := range events {
		twirpEvents[i] = e.ToTWIRPModel()
	}
	return twirpEvents
}

func (e Event) ToTWIRPModel() *prometheus.Event {
	eventTime, err := ptypes.TimestampProto(e.EventTime)
	if err != nil {
		log.WithError(err).WithField("event_time", e.EventTime).Error("invalid event time")
	}

	eventSubTypeTwirp, ok := prometheus.EventSubType_value[e.EventSubType]
	if !ok {
		log.WithError(err).WithField("event_sub_type", e.EventSubType).Error("Error converting event_sub_type back to twirp model")
		eventSubTypeTwirp = 0
	}

	return &prometheus.Event{
		EventType:             e.EventType,
		EventSubType:          prometheus.EventSubType(eventSubTypeTwirp),
		EventId:               e.EventID,
		EventTime:             eventTime,
		UserId:                e.UserID,
		UserLogin:             e.UserLogin,
		ChannelId:             e.ChannelID,
		ChannelLogin:          e.ChannelLogin,
		AnonymousState:        e.AnonymousState,
		GiftRecipientId:       e.GiftRecipientId,
		GiftRecipientLogin:    e.GiftRecipientLogin,
		BitsAmount:            e.BitsAmount,
		BitsBalance:           e.BitsBalance,
		Amount:                e.Amount,
		CurrencyAmount:        e.CurrencyAmount,
		CurrencyCode:          e.CurrencyCode,
		OrderId:               e.OrderId,
		PurchaseOrderState:    e.PurchaseOrderState,
		PaymentService:        e.PaymentService,
		PaymentGateway:        e.PaymentGateway,
		Message:               e.Message,
		AdminUser:             e.AdminUser,
		AdminMessage:          e.AdminMessage,
		ExtensionId:           e.ExtensionId,
		ExtensionCreatorId:    e.ExtensionCreatorId,
		ExtensionSku:          e.ExtensionSku,
		ExtensionCreatorLogin: e.ExtensionCreatorLogin,
		CountryCode:           e.CountryCode,
		City:                  e.City,
		PostalCode:            e.PostalCode,
		PollId:                e.PollID,
		PollChoiceId:          e.PollChoiceID,
		HoldId:                e.HoldID,
	}
}

func GetCSVHeader() []string {
	csvHeader := []string{
		"EventType",
		"EventSubType",
		"EventID",
		"EventTime",
		"UserID",
		"UserLogin",
		"ChannelID",
		"ChannelLogin",
		"AnonymousState",
		"GiftRecipientId",
		"GiftRecipientLogin",
		"BitsAmount",
		"BitsBalance",
		"Amount",
		"CurrencyAmount",
		"CurrencyCode",
		"OrderId",
		"PurchaseOrderState",
		"PaymentService",
		"PaymentGateway",
		"Message",
		"AdminUser",
		"AdminMessage",
		"ExtensionId",
		"ExtensionCreatorId",
		"ExtensionSku",
		"ExtensionCreatorLogin",
		"CountryCode",
		"City",
		"PostalCode",
		"PollID",
		"PollChoiceID",
		"HoldID",
	}

	return csvHeader
}

func (e Event) ToCSVRecord() []string {
	csvRecords := []string{
		e.EventType,
		e.EventSubType,
		e.EventID,
		e.EventTime.String(),
		e.UserID,
		e.UserLogin,
		e.ChannelID,
		e.ChannelLogin,
		e.AnonymousState,
		e.GiftRecipientId,
		e.GiftRecipientLogin,
		fmt.Sprintf("%d", e.BitsAmount),
		fmt.Sprintf("%d", e.BitsBalance),
		fmt.Sprintf("%d", e.Amount),
		fmt.Sprintf("%.2f", e.CurrencyAmount),
		e.CurrencyCode,
		e.OrderId,
		e.PurchaseOrderState,
		e.PaymentService,
		e.PaymentGateway,
		e.Message,
		e.AdminUser,
		e.AdminMessage,
		e.ExtensionId,
		e.ExtensionCreatorId,
		e.ExtensionSku,
		e.ExtensionCreatorLogin,
		e.CountryCode,
		e.City,
		e.PostalCode,
		e.PollID,
		e.PollChoiceID,
		e.HoldID,
	}

	return csvRecords
}
