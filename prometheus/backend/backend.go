package backend

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/prometheus/backend/api/elasticsearch_ping"
	"code.justin.tv/commerce/prometheus/backend/api/generate_search_report"
	"code.justin.tv/commerce/prometheus/backend/api/get_event_subtypes"
	"code.justin.tv/commerce/prometheus/backend/api/health_check"
	"code.justin.tv/commerce/prometheus/backend/api/publish_event"
	"code.justin.tv/commerce/prometheus/backend/api/publish_events"
	search_api "code.justin.tv/commerce/prometheus/backend/api/search"
	"code.justin.tv/commerce/prometheus/backend/circuit"
	"code.justin.tv/commerce/prometheus/backend/clients/elasticsearch"
	"code.justin.tv/commerce/prometheus/backend/clients/redis"
	"code.justin.tv/commerce/prometheus/backend/clients/user"
	"code.justin.tv/commerce/prometheus/backend/ingestion"
	"code.justin.tv/commerce/prometheus/backend/publish"
	"code.justin.tv/commerce/prometheus/backend/search"
	"code.justin.tv/commerce/prometheus/backend/server"
	user_loader "code.justin.tv/commerce/prometheus/backend/user"
	"code.justin.tv/commerce/prometheus/backend/worker"
	"code.justin.tv/commerce/prometheus/backend/worker/event_ingestion"
	"code.justin.tv/commerce/prometheus/config"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/commerce/splatter/cloudwatch"
	"code.justin.tv/foundation/twitchclient"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	log "github.com/sirupsen/logrus"
)

const (
	localhostServerEndpoint    = "http://localhost:8000"
	defaultNumberOfConnections = 200
)

type Terminator interface {
	Shutdown(ctx context.Context) error
}

type Backend struct {
	// RPC Server
	Server prometheus.Prometheus `inject:""`

	// Config
	Config *config.Config `inject:""`

	// Stats
	Stats statsd.Statter `inject:""`

	// AWS Clients
	SQSClient sqsiface.SQSAPI `inject:""`

	// Handlers
	IngestionHandler *event_ingestion.IngestionHandler `inject:""`

	// Worker Terminators
	terminators []Terminator
}

func NewBackend(cfg *config.Config) (*Backend, error) {
	var backend Backend

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	clientHttpTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: defaultNumberOfConnections,
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetricsConfig != nil {
		flusher, err := cloudwatch.NewMetricFlusher(&cloudwatch.MetricFlusherConfig{
			AWSRegion:                      cfg.AWSRegion,
			Namespace:                      fmt.Sprintf("prometheus-%s", cfg.EnvironmentName),
			BufferSize:                     cfg.CloudwatchMetricsConfig.BufferSize,
			BatchSize:                      cfg.CloudwatchMetricsConfig.BatchSize,
			FlushInterval:                  time.Minute * time.Duration(cfg.CloudwatchMetricsConfig.FlushIntervalMinutes),
			FlushPollCheckDelay:            time.Millisecond * time.Duration(cfg.CloudwatchMetricsConfig.FlushCheckDelayMS),
			BufferEmergencyFlushPercentage: cfg.CloudwatchMetricsConfig.EmergencyFlushPercentage,
		})

		if err != nil {
			return nil, err
		}

		logger, err := cloudwatch.NewMetricLogger(cfg.EnvironmentName, flusher)
		if err != nil {
			return nil, err
		}

		cloudwatchStatter, err := splatter.NewCloudwatchStatter(logger, cfg.StatsPrefix)
		if err != nil {
			return nil, err
		}

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	esClient, err := elasticsearch.NewClient(*cfg)
	if err != nil {
		return nil, err
	}

	userService, err := user.NewClient(*cfg, clientHttpTransport)
	if err != nil {
		return nil, err
	}

	awsConfig := &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	}

	awsSess, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	sqsClient := sqs.New(awsSess, awsConfig)
	snsClient := sns.NewDefaultClient()
	redisClient := redis.NewClient(*cfg)
	s3Client := s3.NewDefaultClient()

	hystrixMetrics := circuit.NewHystrixMetricsCollector()

	var graph inject.Graph
	err = graph.Provide(
		// Backend (graph root)
		&inject.Object{Value: &backend},

		// Config
		&inject.Object{Value: cfg},

		// RPC Server
		&inject.Object{Value: &server.Server{}},

		// Stats
		&inject.Object{Value: stats},
		&inject.Object{Value: hystrixMetrics},

		// AWS Clients
		&inject.Object{Value: esClient},
		&inject.Object{Value: snsClient},
		&inject.Object{Value: sqsClient},
		&inject.Object{Value: redisClient},
		&inject.Object{Value: s3Client},

		// APIs
		&inject.Object{Value: &health_check.API{}},
		&inject.Object{Value: &elasticsearch_ping.API{}},
		&inject.Object{Value: &publish_event.API{}},
		&inject.Object{Value: &publish_events.API{}},
		&inject.Object{Value: &search_api.API{}},
		&inject.Object{Value: &get_event_subtypes.API{}},
		&inject.Object{Value: &generate_search_report.API{}},

		// Publisher
		&inject.Object{Value: &publish.EventSNSPublisher{}},

		// Ingestor
		&inject.Object{Value: &ingestion.ElasticsearchIngester{}},

		// Search
		&inject.Object{Value: &search.ElasticSearchSearcher{}},

		// User Service
		&inject.Object{Value: userService},
		&inject.Object{Value: &user_loader.LoaderImpl{}},

		// Handlers
		&inject.Object{Value: &event_ingestion.IngestionHandler{}},
	)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixMetrics.NewHystrixCommandMetricsCollector(name)
	})

	backend.setupWorkers()

	return &backend, nil
}

func (b *Backend) setupWorkers() {
	b.terminators = make([]Terminator, 0)
	if b.Config.EventIngestionSQSUrl != "" {
		b.terminators = append(b.terminators, worker.StartProcessing(worker.Config{
			QueueURL:                 b.Config.EventIngestionSQSUrl,
			MaxMessages:              10,
			WaitTimeSeconds:          10,
			VisibilityTimeoutSeconds: 60,
			ProcessingCycleDelay:     time.Second,
		}, b.SQSClient, b.IngestionHandler.HandleMessages))
	}
}

func (b *Backend) Shutdown(ctx context.Context) {
	for _, terminator := range b.terminators {
		err := terminator.Shutdown(ctx)
		if err != nil {
			log.WithError(err).Error("error shutting down")
		}
	}
}

func (b *Backend) Ping(w http.ResponseWriter, r *http.Request) {
	prometheusClient := prometheus.NewPrometheusProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err := prometheusClient.HealthCheck(r.Context(), &prometheus.HealthCheckReq{})
	if err != nil {
		log.WithError(err).Error("Could not ping prometheus server")
		w.WriteHeader(http.StatusInternalServerError)
		_, innerErr := io.WriteString(w, err.Error())
		if innerErr != nil {
			log.WithError(innerErr).Error("Error writing http response")
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}
