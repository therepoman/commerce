package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/go-yaml/yaml"
	log "github.com/sirupsen/logrus"
)

const (
	LocalConfigFilePath            = "/src/code.justin.tv/commerce/prometheus/config/data/%s.yaml"
	GlobalConfigFilePath           = "/etc/prometheus/config/%s.yaml"
	EnvironmentEnvironmentVariable = "ENVIRONMENT"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")

	Local   = Environment("local")
	Staging = Environment("staging")
	Prod    = Environment("prod")

	Default = Local
)

type Config struct {
	EnvironmentName         string                   `yaml:"environment-name"`
	StatsPrefix             string                   `yaml:"stats-prefix"`
	AWSRegion               string                   `yaml:"aws-region"`
	CloudwatchMetricsConfig *CloudwatchMetricsConfig `yaml:"cloudwatch-metrics-config"`
	DynamoSuffix            string                   `yaml:"dynamo-suffix"`
	ElasticsearchEndpoint   string                   `yaml:"elasticsearch-endpoint"`
	PublishEventSNSTopic    string                   `yaml:"publish-event-sns-topic"`
	EventIngestionSQSUrl    string                   `yaml:"event-ingestion-sqs-url"`
	UseSocksProxy           bool                     `yaml:"use-socks-proxy"`
	SocksProxyEndpoint      string                   `yaml:"socks-proxy-endpoint"`
	UserServiceEndpoint     string                   `yaml:"user-service-endpoint"`
	RedisEndpoint           string                   `yaml:"redis-endpoint"`
	UseRedisClusterMode     bool                     `yaml:"use-redis-cluster-mode"`
	AllowInvalidTuids       bool                     `yaml:"allow-invalid-tuids"`
	SearchReportsS3Bucket   string                   `yaml:"search-reports-s3-bucket"`
	PrometheusEndpoint      string                   `yaml:"prometheus-endpoint"`
}

type CloudwatchMetricsConfig struct {
	BufferSize               int     `yaml:"buffer-size"`
	BatchSize                int     `yaml:"batch-size"`
	FlushIntervalMinutes     int     `yaml:"flush-interval-minutes"`
	FlushCheckDelayMS        int     `yaml:"flush-check-delay-ms"`
	EmergencyFlushPercentage float64 `yaml:"emergency-flush-percentage"`
}

type QueueConfig struct {
	Name       string `yaml:"name"`
	URL        string `yaml:"url"`
	NumWorkers int    `yaml:"num-workers"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath)
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}
