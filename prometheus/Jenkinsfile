properties(
    [
        pipelineTriggers([[$class: "tv.justin.jenkins.twitchgithub.GitHubPushTrigger"]]),
    ]
)

void setBuildStatus(String repo, String message, String state) {
    step([
        $class: "GitHubCommitStatusSetter",
        reposSource: [$class: "ManuallyEnteredRepositorySource", url: "https://git-aws.internal.justin.tv/${repo}"],
        contextSource: [$class: "ManuallyEnteredCommitContextSource", context: "ci/jenkins/build-status"],
        errorHandlers: [[$class: "ChangingBuildStatusErrorHandler", result: "UNSTABLE"]],
        statusResultSource: [ $class: "ConditionalStatusResultSource", results: [[$class: "AnyBuildResult", message: message, state: state]] ]
    ]);
}

pipeline {
    agent any

    environment {
        // General config
        REPO                        = "commerce/prometheus"
        TEST_ENVIRONMENT            = "staging"

        // Staging
        AWS_STAGING_ID             = "prometheus-devo-tcs-access-key"
        AWS_STAGING_SECRET_ID      = "prometheus-devo-tcs-secret-key"
        STAGING_ECR_URL            = "736822310176.dkr.ecr.us-west-2.amazonaws.com"

        // Production
        AWS_PRODUCTION_ID              = "prometheus-prod-tcs-access-key"
        AWS_PRODUCTION_SECRET_ID       = "prometheus-prod-tcs-secret-key"
        PRODUCTION_ECR_URL             = "859821054041.dkr.ecr.us-west-2.amazonaws.com"
    }

    options {
        disableConcurrentBuilds()
        timeout(time: 20, unit: 'MINUTES')
        ansiColor('xterm')
        timestamps()
    }

    stages {
        stage('Setup') {
            steps {
                echo 'Checking out code'
                checkout scm

                echo 'Build Environment'
                sh 'env'
                sh 'docker ps'

                sh 'docker build -f Dockerfile.ci -t builder .'
            }
        }

        stage('Run Lint') {
           steps {
                sh 'docker run -t builder make docker_lint'
           }
        }

        stage('Run Tests') {
           steps {
                sh 'docker run -t builder make unit_test'
           }
        }

        stage('Build Docker Container') {
            steps {
                sh "docker build -f Dockerfile.ecs -t ${REPO} ."
            }
        }

        stage('Push to ECR/S3 when NOT master') {
            when {
                not {
                    branch "master"
                }
            }

            steps {
                withCredentials([
                    string(credentialsId: "${env.AWS_STAGING_ID}", variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: "${env.AWS_STAGING_SECRET_ID}", variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    script {
                        /* ECR Login */
                        sh "aws ecr get-login --no-include-email --region us-west-2 | cut -d' ' -f6 | docker login -u AWS --password-stdin ${STAGING_ECR_URL}"

                        /* Push to ECR - tag latest IF master */
                        sh "docker tag ${REPO} ${STAGING_ECR_URL}/${REPO}:${GIT_COMMIT}"
                        sh "docker push ${STAGING_ECR_URL}/${REPO}:${GIT_COMMIT}"

                        sh "docker tag ${REPO} ${STAGING_ECR_URL}/${REPO}:latest"
                        sh "docker push ${STAGING_ECR_URL}/${REPO}:latest"
                    }
                }
            }
        }

        stage('Push to AWS ECR when master') {
            when {
                branch "master"
            }

            steps {
                /* Staging Account */
                withCredentials([
                    string(credentialsId: AWS_STAGING_ID, variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: AWS_STAGING_SECRET_ID, variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    script {
                        /* ECR Login */
                        sh "aws ecr get-login --no-include-email --region us-west-2 | cut -d' ' -f6 | docker login -u AWS --password-stdin ${STAGING_ECR_URL}"

                        /* Push to ECR - tag latest IF master */
                        sh "docker tag ${REPO} ${STAGING_ECR_URL}/${REPO}:${GIT_COMMIT}"
                        sh "docker push ${STAGING_ECR_URL}/${REPO}:${GIT_COMMIT}"

                        sh "docker tag ${REPO} ${STAGING_ECR_URL}/${REPO}:latest"
                        sh "docker push ${STAGING_ECR_URL}/${REPO}:latest"
                    }
                }

                /* Production Account */
                withCredentials([
                        string(credentialsId: AWS_PRODUCTION_ID, variable: 'AWS_ACCESS_KEY_ID'),
                        string(credentialsId: AWS_PRODUCTION_SECRET_ID, variable: 'AWS_SECRET_ACCESS_KEY'),
                        file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    script {
                        /* ECR Login */
                        sh "aws ecr get-login --no-include-email --region us-west-2 | cut -d' ' -f6 | docker login -u AWS --password-stdin ${PRODUCTION_ECR_URL}"

                        /* Push to ECR - tag latest IF master */
                        sh "docker tag ${REPO} ${PRODUCTION_ECR_URL}/${REPO}:${GIT_COMMIT}"
                        sh "docker push ${PRODUCTION_ECR_URL}/${REPO}:${GIT_COMMIT}"

                        sh "docker tag ${REPO} ${PRODUCTION_ECR_URL}/${REPO}:latest"
                        sh "docker push ${PRODUCTION_ECR_URL}/${REPO}:latest"
                    }
                }
            }
        }
    }

    post {
        success {
            setBuildStatus(REPO, "PR Deployment Successful", "SUCCESS")
        }

        failure {
            setBuildStatus(REPO, "PR Deployment Failed", "FAILURE")
        }
    }
}
