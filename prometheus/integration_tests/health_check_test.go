// +build integration

package integration_tests

import (
	"context"
	"testing"

	prometheus "code.justin.tv/commerce/prometheus/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("Test the health check endpoint", t, func() {
		prometheusClient := GetPrometheusClient()
		_, err := prometheusClient.HealthCheck(context.Background(), &prometheus.HealthCheckReq{})
		So(err, ShouldBeNil)
	})
}
