package integration_tests

import (
	"net/http"
	"os"

	"code.justin.tv/commerce/prometheus/config"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	log "github.com/sirupsen/logrus"
)

func GetPrometheusClient() prometheus.Prometheus {
	environment := os.Getenv("ENVIRONMENT")
	if environment == "" {
		log.Panic("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panic("Please don't run these integration tests against prod!")
	}

	log.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		log.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		log.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	return prometheus.NewPrometheusProtobufClient(cfg.PrometheusEndpoint, http.DefaultClient)
}
