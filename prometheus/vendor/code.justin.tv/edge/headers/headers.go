// Package headers sets and reads custom headers for Twirp services.
package headers

import (
	"context"
	"net"
	"net/http"
	"strings"
)

// Context contains the values pass around in headers.
type Context struct {
	// ClientID is the identifier for the application that triggered the request.
	ClientID string

	// ClientIP is the IP address of the user that triggered the request.
	ClientIP net.IP

	// ForwardedFor is the string value from the X-Forwarded-For or Forwarded-For header
	// that was used to produce the ClientIP value.
	ForwardedFor string

	// RequestID is the unique request identifier created by the calling service.
	RequestID string

	// UserID is the identifier of the user that triggered the request.
	UserID string
}

// contextKey is an un-exported alias of a string type, so that no other package
// accessing the context.Context can modify the contents at this key.
type contextKey string

// key is used to place the Context onto the context.Context, and
// later to read it as well.
const key contextKey = "headers"

// AddContext from the request headers and make it available on the context.Context.
func AddContext(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ff := r.Header.Get("X-Forwarded-For")
		if ff == "" {
			ff = r.Header.Get("Forwarded-For")
		}

		c := Context{
			ClientID:     r.Header.Get("Client-Id"),
			ClientIP:     clientIP(ff),
			ForwardedFor: ff,
			UserID:       r.Header.Get("User-Id"),
			RequestID:    r.Header.Get("Request-Id"),
		}

		// Attach the header context to the request context.
		ctx := r.Context()
		ctx = context.WithValue(ctx, key, c)
		r = r.WithContext(ctx)

		h.ServeHTTP(w, r)
	})
}

// GetContext retrieves the header context from the request context.
func GetContext(ctx context.Context) Context {
	c, _ := ctx.Value(key).(Context)
	return c
}

// clientIP determines the client IP address from a Forwarded-For header value.
func clientIP(forwardedFor string) net.IP {
	if forwardedFor == "" {
		return nil
	}

	var ips []net.IP

	for _, s := range strings.Split(forwardedFor, ",") {
		if ip := net.ParseIP(strings.TrimSpace(s)); ip != nil {
			ips = append(ips, ip)
		}
	}

	return lastPublicIP(ips)
}

// Read the list of IPs from back to front.
func lastPublicIP(ips []net.IP) net.IP {
	if len(ips) == 0 {
		return nil
	}

	for i := len(ips) - 1; i >= 0; i-- {
		ip := ips[i]

		if ip == nil || ip.IsUnspecified() || ip.IsLoopback() {
			continue
		}

		if !isPrivate(ip) {
			return ip
		}
	}

	return nil
}

// isPrivate checks whether ip is a private address, according to RFC 1918 and RFC 4193.
func isPrivate(ip net.IP) bool {
	// IPv4: https://tools.ietf.org/html/rfc1918
	if ip4 := ip.To4(); ip4 != nil {
		return ip4[0] == 10 ||
			(ip4[0] == 172 && ip4[1]&0xf0 == 16) ||
			(ip4[0] == 192 && ip4[1] == 168)
	}

	// IPv6: https://tools.ietf.org/html/rfc4193
	return len(ip) == net.IPv6len && ip[0]&0xfe == 0xfc
}
