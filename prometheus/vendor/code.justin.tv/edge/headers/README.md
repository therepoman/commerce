# headers

> Read values from headers and make them available on the request context.

## Installation

```sh
go get code.justin.tv/edge/headers
```

## Usage

Add the middleware to your Twirp mux instantiation.

```go
server := &haberdasherserver.Server{}
handler := haberdasher.NewHaberdasherServer(server,
    twirp.WithServerPathPrefix("/my/custom/prefix"))

mux := http.NewServeMux()
mux.Handle(handler.PathPrefix(), headers.AddContext(handler))
http.ListenAndServe(":8080", mux)
```

Then, wherever you need the values, get them from the request context.

```go
c := headers.GetContext(ctx)

log.Println("clientID:", c.ClientID)
log.Println("clientIP:", c.ClientIP)
log.Println("requestID", c.RequestID)
log.Println("userID:", c.UserID)
```
