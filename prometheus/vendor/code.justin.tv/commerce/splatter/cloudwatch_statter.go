package splatter

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/splatter/cloudwatch"
	"github.com/cactus/go-statsd-client/statsd"
)

const separator string = "."

// CloudwatchStatter is an adapter to send Cloudwatch metrics through the statsd.Statter
// interface. This can be swapped in as the Statter implementation for libraries like
// twirp, workerqueue, and Goji.
//
// Note: The statsd.Statter interface has sample rate built into the methods. These are
//       discarded by CloudwatchStatter, which does not support specifying sample rate.
type CloudwatchStatter struct {
	metricLogger  cloudwatch.IMetricLogger
	prefix        string
	filteredStats map[string]bool
}

// NewCloudwatchStatter returns a pointer to a new CloudwatchStatter, and an error.
func NewCloudwatchStatter(metricLogger cloudwatch.IMetricLogger, prefix string) (statsd.Statter, error) {
	return NewCloudwatchStatterWithFilter(metricLogger, prefix, map[string]bool{})
}

// NewCloudwatchStatterWithFilter returns a pointer to a new CloudwatchStatter with filtered stats, and an error.
func NewCloudwatchStatterWithFilter(metricLogger cloudwatch.IMetricLogger, prefix string, filteredStats map[string]bool) (statsd.Statter, error) {
	return &CloudwatchStatter{
		metricLogger:  metricLogger,
		prefix:        prefix,
		filteredStats: filteredStats,
	}, nil
}

// Inc increments a statsd count type.
// stat is a string name for the metric.
// value is the integer value
func (s *CloudwatchStatter) Inc(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return s.metricLogger.LogCountMetric(joinPathComponents(s.prefix, stat), value, rate)
}

// Dec decrements a statsd count type.
// stat is a string name for the metric.
// value is the integer value.
func (s *CloudwatchStatter) Dec(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return s.metricLogger.LogCountMetric(joinPathComponents(s.prefix, stat), -value, rate)
}

// Gauge submits/updates a statsd gauge type.
// stat is a string name for the metric.
// value is the integer value.
func (s *CloudwatchStatter) Gauge(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return s.metricLogger.LogGaugeMetric(joinPathComponents(s.prefix, stat), value, rate)
}

// GaugeDelta submits a delta to a statsd gauge.
// Note: This method has not supported in Cloudwatch. No metrics will be recorded,
//       and an error will be returned.
func (s *CloudwatchStatter) GaugeDelta(stat string, value int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return fmt.Errorf("CloudwatchStatter::GaugeDelta is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// Timing submits a statsd timing type.
// stat is a string name for the metric.
// delta is the time duration value in milliseconds
// rate is discarded
func (s *CloudwatchStatter) Timing(stat string, delta int64, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	deltaTime := time.Duration(delta * int64(time.Millisecond))
	return s.metricLogger.LogDurationMetric(joinPathComponents(s.prefix, stat), deltaTime, rate)
}

// TimingDuration submits a statsd timing type.
// stat is a string name for the metric.
// delta is the timing value as time.Duration
func (s *CloudwatchStatter) TimingDuration(stat string, delta time.Duration, rate float32) error {
	if s.filter(stat) {
		return nil
	}
	return s.metricLogger.LogDurationMetric(joinPathComponents(s.prefix, stat), delta, rate)
}

// Set submits a stats set type
// Note: This method has not supported in Cloudwatch. No metrics will be recorded,
//       and an error will be returned.
func (s *CloudwatchStatter) Set(stat string, value string, rate float32) error {
	return fmt.Errorf("CloudwatchStatter::Set is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// SetInt submits a number as a stats set type.
// Note: This method has not supported in Cloudwatch. No metrics will be recorded,
//       and an error will be returned.
func (s *CloudwatchStatter) SetInt(stat string, value int64, rate float32) error {
	return fmt.Errorf("CloudwatchStatter::SetInt is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// Raw submits a preformatted value.
// Note: This method has not supported in Cloudwatch. No metrics will be recorded,
//       and an error will be returned.
func (s *CloudwatchStatter) Raw(stat string, value string, rate float32) error {
	return fmt.Errorf("CloudwatchStatter::Raw is not supported. No metric recorded. - stat: %s - value: %v", stat, value)
}

// SetSamplerFunc sets a sampler function to something other than the default
// Note: This method is not used by the CloudwatchStatter
func (s *CloudwatchStatter) SetSamplerFunc(sampler statsd.SamplerFunc) {}

// SetPrefix sets/updates the statsd client prefix.
// Note: Does not change the prefix of any SubStatters.
func (s *CloudwatchStatter) SetPrefix(prefix string) {
	s.prefix = prefix
}

// NewSubStatter returns a SubStatter with appended prefix
func (s *CloudwatchStatter) NewSubStatter(prefix string) statsd.SubStatter {
	var c *CloudwatchStatter
	if s != nil {
		c = &CloudwatchStatter{
			metricLogger: s.metricLogger,
			prefix:       joinPathComponents(s.prefix, prefix),
		}
	}
	return c
}

// Close closes the connection and cleans up.
func (s *CloudwatchStatter) Close() error {
	// No-op. No shutdown work required for our Cloudwatch client
	return nil
}

func (s *CloudwatchStatter) filter(stat string) bool {
	filtered := s.filteredStats[stat]
	return filtered
}
