package utils

import "time"

func IntValue(i *int) int {
	if i == nil {
		return 0
	} else {
		return *i
	}
}

func BoolValue(b *bool) bool {
	if b == nil {
		return false
	} else {
		return *b
	}
}

func StringValue(s *string) string {
	if s == nil {
		return ""
	} else {
		return *s
	}
}

func TimeValue(t *time.Time) time.Time {
	if t == nil {
		return time.Time{}
	} else {
		return *t
	}
}

func MinValue(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxValue(a, b int) int {
	if a < b {
		return b
	}
	return a
}
