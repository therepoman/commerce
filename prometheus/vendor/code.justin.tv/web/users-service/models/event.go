package models

import (
	"time"
)

// SNSRenameEvent is published to SNS when SetUserProperties would change either
// login or displayname
type SNSRenameEvent struct {
	UserID    string         `json:"user_id"`
	Data      *SNSRenameData `json:"data"`
	Timestamp time.Time      `json:"timestamp"`
}

type SNSRenameData struct {
	Original *SNSRenameProperties `json:"original"`
	Changes  *SNSRenameProperties `json:"changes"`
}

type SNSRenameProperties struct {
	Login       *string `json:"login,omitempty"`
	Displayname *string `json:"displayname,omitempty"`
}

func NewRenameEvent(id string, original *Properties, changes *UpdateableProperties) SNSRenameEvent {
	changeData := SNSRenameData{
		Original: &SNSRenameProperties{
			Login:       original.Login,
			Displayname: original.Displayname,
		},
		Changes: &SNSRenameProperties{
			Login:       changes.NewLogin,
			Displayname: changes.Displayname,
		},
	}

	return SNSRenameEvent{
		UserID:    id,
		Data:      &changeData,
		Timestamp: time.Now(),
	}
}

// SNSUnbanEvent is published to SNS when a user is unbanned (both via a manual
// unban or automatically following a temporary ban).
type SNSUnbanEvent struct {
	UserID      string     `json:"user_id"`
	Timestamp   time.Time  `json:"timestamp"`
	UpdatedUser Properties `json:"updated_user"`
}

type SNSBanEvent struct {
	UserID                  string    `json:"user_id"`
	DmcaViolation           bool      `json:"dmca_violation"`
	TermsOfServiceViolation bool      `json:"terms_of_service_violation"`
	Timestamp               time.Time `json:"timestamp"`
}

type SNSCreationEvent struct {
	UserID      string    `json:"user_id"`
	Login       string    `json:"login"`
	Displayname string    `json:"displayname"`
	Timestamp   time.Time `json:"timestamp"`
}

type SNSUpdateEvent struct {
	UserID    string                `json:"user_id"`
	Original  *Properties           `json:"original"`
	Changed   *UpdateableProperties `json:"changed"`
	Timestamp time.Time             `json:"timestamp"`
}

type SNSUpdateImageEvent struct {
	UserID    string           `json:"user_id"`
	Original  *Properties      `json:"original"`
	Changed   *ImageProperties `json:"changed"`
	Timestamp time.Time        `json:"timestamp"`
}

type SNSChannelUpdateEvent struct {
	Original  *ChannelProperties       `json:"original"`
	Changes   *UpdateChannelProperties `json:"changes"`
	Timestamp time.Time                `json:"timestamp"`
}

type SNSExpireCacheEvent struct {
	Keys      string    `json:"keys"`
	Timestamp time.Time `json:"timestamp"`
}

type SNSBanUserEvent struct {
	RecipientID string `json:"id"`
	Type        string `json:"type"`
	Warn        bool   `json:"warn"`
	Reason      string `json:"reason"`
	Description string `json:"description"`
	Duration    int64  `json:"duration"`
	ContentType string `json:"content_type"`
}

type SNSSoftDeleteEvent struct {
	UserID    string    `json:"user_id"`
	Timestamp time.Time `json:"timestamp"`
}

type SNSHardDeleteEvent struct {
	UserID    string    `json:"user_id"`
	Timestamp time.Time `json:"timestamp"`
}

type SNSHardDeleteEmailEvent struct {
	UserID       string `json:"user_id"`
	UserLanguage string `json:"language"`
	Login        string `json:"login"`
	EmailAddress string `json:"email_address"`
}

type SNSUndeleteEvent struct {
	UserID    string    `json:"user_id"`
	Timestamp time.Time `json:"timestamp"`
}
