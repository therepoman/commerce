package main

import (
	"context"
	"math/rand"
	"os"
	"time"

	"code.justin.tv/commerce/prometheus/backend"
	"code.justin.tv/commerce/prometheus/config"
	prometheus "code.justin.tv/commerce/prometheus/rpc"
	"code.justin.tv/edge/headers"
	log "github.com/sirupsen/logrus"
	twirp_statsd "github.com/twitchtv/twirp/hooks/statsd"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

func getEnv() config.Environment {
	env := os.Getenv(config.EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !config.IsValidEnvironment(config.Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(config.Default))
		return config.Default
	}

	return config.Environment(env)
}

func main() {
	env := getEnv()

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}
	twirpStatsHook := twirp_statsd.NewStatsdServerHooks(be.Stats)
	twirpHandler := prometheus.NewPrometheusServer(be.Server, twirpStatsHook)

	mux := goji.NewMux()

	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(prometheus.PrometheusPathPrefix+"*"), headers.AddContext(twirpHandler))

	goji_graceful.HandleSignals()

	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	be.Shutdown(ctx)
}
