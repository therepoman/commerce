# see "man logrotate" for details
# rotate log files weekly
daily

# keep 2 days worth of backlogs
rotate 2

# create new (empty) log files after rotating old ones
create

# use date as a suffix of the rotated file
dateext

# uncomment this if you want your log files compressed
#compress

# RPM packages drop log rotation information into this directory
include /etc/logrotate.d

/var/log/ {
    missingok
    daily
    create 0600 root utmp
    rotate 1
}

/var/log/eb-docker/containers/eb-current-app/ {
    missingok
    daily
    create 0600 root utmp
    rotate 1
}

/var/lib/docker/containers/*/*.log {
  rotate 7
  daily
  compress
  size=1M
  missingok
  delaycompress
  copytruncate
}
