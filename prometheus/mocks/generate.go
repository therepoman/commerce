package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// GoGoGadget SNS Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/vendor/code.justin.tv/commerce/gogogadget/aws/sns -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/gogogadget/aws/sns -outpkg=sns_mock

// Event Publisher
//go:generate retool do mockery -name=EventPublisher -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/backend/publish -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/publish -outpkg=publish_mock

// Ingester
//go:generate retool do mockery -name=Ingester -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/backend/ingestion -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/ingestion -outpkg=ingestion_mock

// Internal Users Client
//go:generate retool do mockery -name=InternalClient -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/vendor/code.justin.tv/web/users-service/client/usersclient_internal -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/web/users-service/client/usersclient_internal -outpkg=usersclient_internal_mock

// Searcher
//go:generate retool do mockery -name=Searcher -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/backend/search -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/search -outpkg=search_mock

// ElasticSearch Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/backend/clients/elasticsearch -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/clients/elasticsearch -outpkg=elasticsearch_mock

// User Loader
//go:generate retool do mockery -name=Loader -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/backend/user -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/user -outpkg=user_mock

// Redis Client
//go:generate retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prometheus/backend/clients/redis -output=$GOPATH/src/code.justin.tv/commerce/prometheus/mocks/code.justin.tv/commerce/prometheus/backend/clients/redis -outpkg=redis_mock
