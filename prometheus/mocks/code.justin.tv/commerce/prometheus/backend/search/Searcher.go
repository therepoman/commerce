// Code generated by mockery v1.0.0
package search_mock

import (
	context "context"

	models "code.justin.tv/commerce/prometheus/backend/models"
	mock "github.com/stretchr/testify/mock"

	prometheus "code.justin.tv/commerce/prometheus/rpc"
)

// Searcher is an autogenerated mock type for the Searcher type
type Searcher struct {
	mock.Mock
}

// Search provides a mock function with given fields: ctx, req
func (_m *Searcher) Search(ctx context.Context, req *prometheus.SearchReq) ([]models.Event, int64, error) {
	ret := _m.Called(ctx, req)

	var r0 []models.Event
	if rf, ok := ret.Get(0).(func(context.Context, *prometheus.SearchReq) []models.Event); ok {
		r0 = rf(ctx, req)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.Event)
		}
	}

	var r1 int64
	if rf, ok := ret.Get(1).(func(context.Context, *prometheus.SearchReq) int64); ok {
		r1 = rf(ctx, req)
	} else {
		r1 = ret.Get(1).(int64)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, *prometheus.SearchReq) error); ok {
		r2 = rf(ctx, req)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}
