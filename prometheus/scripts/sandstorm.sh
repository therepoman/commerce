#!/bin/bash

set -e

ACCOUNT=$1

case "$ACCOUNT" in
        development)
            ENVIRONMENTS=(staging)
            ;;
        *)
            echo "Unknown account \"$ACCOUNT\", exiting"
            exit 1
esac

cd ./terraform/${ACCOUNT}

for ENVIRONMENT in "${ENVIRONMENTS[@]}"
do
    ROLE=`terraform output ${ENVIRONMENT}-iam_role_arn`

    tcs sandstorm role-flags \
        --owner team-commerce \
        --name commerce-${ENVIRONMENT}-prometheus \
        --secret_key "commerce/prometheus/${ENVIRONMENT}/*" \
        --allowed_arn "${ROLE}" \
        --output ./sandstorm.d/${ACCOUNT}_${ENVIRONMENT}.json

done
