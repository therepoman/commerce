#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile prometheus-devo staging-commerce-prometheus-env
popd deploy
