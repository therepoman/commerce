#!/bin/bash -e

manta -v -f build.json || (echo "Build failed. Please create a build.json manta file. See the script for details." && exit 1)

docker build -t docker-registry.internal.justin.tv/commerce-prometheus:$GIT_COMMIT .
