module "prometheus" {
  source                                 = "../modules/prometheus"
  owner                                  = "twitch-prometheus-aws-prod@amazon.com"
  aws_profile                            = "prometheus-prod"
  region                                 = "us-west-2"
  env                                    = "prod"
  env_full                               = "production"
  vpc_id                                 = "vpc-0e98258b8e57188ef"
  private_subnets                        = "subnet-0845265a09a62b017,subnet-0bafd9874c580fc12,subnet-0b67c3fa1554c0bb5"
  private_cidr_blocks                    = ["10.204.0.0/24", "10.204.1.0/24", "10.204.2.0/24"]
  sg_id                                  = "sg-058656a898e0f0360"
  instance_type                          = "c3.xlarge"
  cname_prefix                           = "prometheus-prod"
  elasticache_replicas_per_node_group    = "1"
  elasticache_num_node_groups            = "3"
  elasticache_instance_type              = "cache.m4.large"
  elasticsearch_instance_count           = "20"
  elasticsearch_cloudwatch_log_group_arn = "arn:aws:logs:us-west-2:859821054041:log-group:/aws/aes/domains/prometheus-es-prod/application-logs"
  min_host_count                         = "8"
  max_host_count                         = "64"
  deployment_max_batch_size              = "3"
  cloudwatch_log_group                   = "/aws/elasticbeanstalk/prod-commerce-prometheus-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log"

  whitelisted_arns_for_privatelink = [
    "arn:aws:iam::021561903526:root", // payday
    "arn:aws:iam::196915980276:root", // admin panel
    "arn:aws:iam::787149559823:root", // graphql
  ]

  ecs_ec2_backed_min_instances = 5
  sandstorm_arn                = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/prometheus-production"
}

terraform {
  backend "s3" {
    bucket  = "prometheus-terraform-prod"
    key     = "tfstate/commerce/prometheus/terraform/prod"
    region  = "us-west-2"
    profile = "prometheus-prod"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "prometheus-prod"
}
