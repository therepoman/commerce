module "prometheus" {
  source                                 = "../modules/prometheus"
  owner                                  = "twitch-prometheus-aws-devo@amazon.com"
  aws_profile                            = "prometheus-devo"
  region                                 = "us-west-2"
  env                                    = "staging"
  env_full                               = "staging"
  vpc_id                                 = "vpc-02c4d6a634d761309"
  private_subnets                        = "subnet-0a8dd28743bcb8378,subnet-02998d2d5f22eeb24,subnet-0d15f2b20dedc50e4"
  private_cidr_blocks                    = ["10.203.252.0/24", "10.203.253.0/24", "10.203.254.0/24"]
  sg_id                                  = "sg-0ac07d3f50f9d417f"
  instance_type                          = "t2.micro"
  cname_prefix                           = "prometheus-staging"
  elasticache_replicas_per_node_group    = "1"
  elasticache_num_node_groups            = "2"
  elasticache_instance_type              = "cache.m3.medium"
  elasticsearch_instance_count           = "3"
  elasticsearch_cloudwatch_log_group_arn = "arn:aws:logs:us-west-2:736822310176:log-group:/aws/aes/domains/prometheus-es-staging/application-logs"
  min_host_count                         = "2"
  max_host_count                         = "4"
  deployment_max_batch_size              = "1"
  cloudwatch_log_group                   = "/aws/elasticbeanstalk/staging-commerce-prometheus-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log"

  whitelisted_arns_for_privatelink = [
    "arn:aws:iam::021561903526:root", // payday
    "arn:aws:iam::219087926005:root", // admin panel
    "arn:aws:iam::645130450452:root", // graphql
  ]

  ecs_ec2_backed_min_instances = 3
  sandstorm_arn                = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/prometheus-staging"
}

terraform {
  backend "s3" {
    bucket  = "prometheus-terraform-devo"
    key     = "tfstate/commerce/prometheus/terraform/staging"
    region  = "us-west-2"
    profile = "prometheus-devo"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "prometheus-devo"
}
