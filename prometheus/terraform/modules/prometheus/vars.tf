variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "region" {
  description = "AWS region to operate in"
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type = "list"
}

variable "sg_id" {
  description = "Id of the security group"
}

variable "instance_type" {
  description = "EC2 instance type"
}

variable "owner" {
  description = "Owner of the account"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/prometheus"
}

variable "service_short_name" {
  description = "Short name of the service being built. Should probably leave this as the default"
  default     = "prometheus"
}

variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "env_full" {
  description = "Whether it's production, development, etc"
}

variable "cname_prefix" {
  description = "Prefix of the cname"
}

variable "eb_environment_service_role" {
  description = "The name of an IAM role that Elastic Beanstalk uses to manage resources for the environment."
  default     = "aws-elasticbeanstalk-service-role"
}

# AWS ElastiCache
variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default = "cache.m4.large"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default = true
}

variable "redis_port" {
  description = "The port used by the redis server"
  default = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "elasticsearch_instance_count" {
  description = "Number of nodes in elasticsearch cluster"
}

variable "elasticsearch_cloudwatch_log_group_arn" {
  description = "Log group where elasticsearch logs are published to"
}

variable "min_host_count" {
  description = "Minimum number of hosts in the beanstalk ASG"
  default     = 1
}

variable "max_host_count" {
  description = "Maximum number of hosts in the beanstalk ASG"
  default     = 4
}

variable "deployment_max_batch_size" {
  description = "the maximum of hosts to be deployed to at a time"
}

variable "cloudwatch_log_group" {}

variable "cloudwatch_log_metric_namespace" {
  default = "PrometheusLogMetrics"
}

variable "whitelisted_arns_for_privatelink" {
  description = "the whitelist of arns for privatelinks"
  type        = "list"
}

variable "sandstorm_arn" {}

variable "ecs_ec2_backed_min_instances" {}
