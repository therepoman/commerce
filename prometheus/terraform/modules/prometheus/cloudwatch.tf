resource "aws_cloudwatch_log_metric_filter" "logscan_errors" {
  name           = "${var.service_short_name}-${var.env}-errors"
  pattern        = "level=error"
  log_group_name = "${var.cloudwatch_log_group}"

  "metric_transformation" {
    name          = "${var.service_short_name}-${var.env}-errors"
    namespace     = "${var.cloudwatch_log_metric_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "logscan_warns" {
  name           = "${var.service_short_name}-${var.env}-warns"
  pattern        = "level=warning"
  log_group_name = "${var.cloudwatch_log_group}"

  "metric_transformation" {
    name          = "${var.service_short_name}-${var.env}-warns"
    namespace     = "${var.cloudwatch_log_metric_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "logscan_panics" {
  name           = "${var.service_short_name}-${var.env}-panics"
  pattern        = "\"panic:\""
  log_group_name = "${var.cloudwatch_log_group}"

  "metric_transformation" {
    name          = "${var.service_short_name}-${var.env}-panics"
    namespace     = "${var.cloudwatch_log_metric_namespace}"
    value         = "1"
    default_value = "0"
  }
}