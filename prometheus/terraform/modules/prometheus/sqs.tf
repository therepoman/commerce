resource "aws_sns_topic" "event_ingestion_topic" {
  name = "${var.env}-event-ingestion"
  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 10,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false
  }
}
EOF
}

module "event_ingestion_queue" {
  source             = "../sqs_queue"
  queue_name         = "${var.env}-event-ingestion-processor"
  subscribe_to_topic = "${aws_sns_topic.event_ingestion_topic.arn}"
}

resource "aws_sns_topic_subscription" "event_ingestion_topic_queue_subscription" {
  topic_arn = "${aws_sns_topic.event_ingestion_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.event_ingestion_queue.queue_arn}"
}
