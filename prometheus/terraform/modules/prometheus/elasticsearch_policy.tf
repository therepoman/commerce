resource "aws_iam_policy_attachment" "elasticsearch_policy_attachment" {
  name       = "elasticsearch_policy_attachment"
  policy_arn = "${aws_iam_policy.elasticsearch_policy.arn}"
  roles      = ["${aws_iam_role.health_service_role.name}", "${module.ecs.service_role}", "${module.ecs.canary_role}", "${module.ecs.cluster_role}"]
}

resource "aws_iam_policy" "elasticsearch_policy" {
  name        = "elasticsearch_policy_${var.service_short_name}"
  description = "communicate with elasticsearch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "es:*",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}
