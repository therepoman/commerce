# roles and policies for running cloudwatch logs agent to send logs to cloudwatch
resource "aws_iam_policy_attachment" "cloudwatch_logs_policy_attachment" {
  name       = "cloudwatch_logs_policy_attachment"
  policy_arn = "${aws_iam_policy.cloudwatch_logs_policy.arn}"
  roles      = ["${aws_iam_role.health_service_role.name}", "${module.ecs.service_role}", "${module.ecs.canary_role}", "${module.ecs.cluster_role}"]
}

resource "aws_iam_policy" "cloudwatch_logs_policy" {
  name        = "cloudwatch_logs_policy_${var.service_short_name}"
  description = "send logs to cloudwatch"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}
