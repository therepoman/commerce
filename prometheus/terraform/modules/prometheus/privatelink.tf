module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "prometheus"
  environment = "${var.env_full}"
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"

  name        = "prometheus"
  environment = "${var.env_full}"
  zone_id     = "${module.privatelink-zone.zone_id}"
  alb_dns     = "${module.ecs.service_alb_dns}"
}

module "privatelink_a2z" {
  source           = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink"
  name             = "prometheus-a2z"
  region           = "${var.region}"
  environment      = "${var.env_full}"
  alb_dns          = "${module.ecs.service_alb_dns}"
  cert_arn         = "${module.privatelink-cert.arn}"
  vpc_id           = "${var.vpc_id}"
  subnets          = "${split(",", var.private_subnets)}"
  whitelisted_arns = "${var.whitelisted_arns_for_privatelink}"
}

// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "ldap.twitch.a2z.com"
}