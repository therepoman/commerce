module "cluster" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//cluster?ref=d305b546d012bc3153b113eac5dd76a93cda42ad"

  name           = "${var.service_short_name}"
  min            = "${var.ecs_ec2_backed_min_instances}"
  vpc_id         = "${var.vpc_id}"
  instance_type  = "c5n.4xlarge"
  environment    = "${var.env}"
  subnets        = "${var.subnet_ids}"
  security_group = "${var.security_group}"
}
