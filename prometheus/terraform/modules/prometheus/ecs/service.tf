locals {
  cpu = {
    prod    = 4096
    staging = 512
  }

  memory = {
    prod    = 4096
    staging = 512
  }
}

module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=adac144cfd38fb5f88079703210475967876a374"

  name   = "${var.service_short_name}-${lower(var.env)}"
  repo   = "${var.service}"
  port   = 8000
  cpu    = "${local.cpu[var.env]}"
  memory = "${local.memory[var.env]}"

  canary_cpu    = "${local.cpu[var.env]}"
  canary_memory = "${local.memory[var.env]}"

  counts = {
    min = "${var.min_task_count}"
    max = "${var.max_task_count}"
  }

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = "${var.env}"
    },
  ]

  cluster        = "${module.cluster.ecs_name}"
  security_group = "${var.security_group}"
  region         = "us-west-2"
  vpc_id         = "${var.vpc_id}"
  environment    = "${var.env}"
  subnets        = "${var.subnet_ids}"
  dns            = "${var.dns}"
  healthcheck    = "/ping"
  http2_enabled  = false
  debug_enabled  = false
}
