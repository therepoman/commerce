resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}

resource "aws_elasticsearch_domain" "es" {
  domain_name           = "prometheus-es-${var.env}"
  elasticsearch_version = "6.2"

  cluster_config {
    instance_type  = "m3.xlarge.elasticsearch"
    instance_count = "${var.elasticsearch_instance_count}"
    dedicated_master_enabled = "true"
    dedicated_master_count = "3"
    dedicated_master_type = "m4.large.elasticsearch"
  }

  vpc_options {
    security_group_ids = "${list(var.sg_id)}"
    subnet_ids         = "${list(element(split(",",var.private_subnets),0))}"
  }

  log_publishing_options {
    log_type                 = "ES_APPLICATION_LOGS"
    cloudwatch_log_group_arn = "${var.elasticsearch_cloudwatch_log_group_arn}"
    enabled                  = "true"
  }
}
