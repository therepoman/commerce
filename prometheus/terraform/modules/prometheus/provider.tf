provider "aws" {
  region     = "us-west-2"
  profile    = "${var.aws_profile}"
  alias      = "${var.aws_profile}"
}
