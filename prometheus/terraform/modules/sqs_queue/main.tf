variable "queue_name" {
  type = "string"
}

variable "receive_wait_time" {
  type    = "string"
  default = 20
}

variable "message_retention_seconds" {
  type    = "string"
  default = 1209600
}

variable "max_receive_count" {
  type    = "string"
  default = 7
}

variable "subscribe_to_topic" {
  type = "string"
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = "${aws_sqs_queue.sqs_queue.id}"
  policy    = "${data.aws_iam_policy_document.queue_policy_doc.json}"
}

data "aws_iam_policy_document" "queue_policy_doc" {
  statement {
    sid = "PrometheusQueueSid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = ["${aws_sqs_queue.sqs_queue.arn}"]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = ["${var.subscribe_to_topic}"]
    }
  }
}

resource "aws_sqs_queue" "sqs_queue_deadletter" {
  message_retention_seconds = "${var.message_retention_seconds}"
  name                      = "${var.queue_name}_deadletter"
}

resource "aws_sqs_queue" "sqs_queue" {
  message_retention_seconds = "${var.message_retention_seconds}"
  name                      = "${var.queue_name}"
  receive_wait_time_seconds = "${var.receive_wait_time}"

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.sqs_queue_deadletter.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
}

output "queue_arn" {
  value = "${aws_sqs_queue.sqs_queue.arn}"
}
