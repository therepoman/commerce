# Prometheus

Twitch Records Searching Service

## Payday VPC Endpoints
| Environment | Endpoint |
| --- | --- |
| Staging | https://prometheus-payday-staging.internal.justin.tv |
| Prod | https://prometheus-payday-prod.internal.justin.tv |

## Admin Panel VPC Endpoints
| Environment | Endpoint |
| --- | --- |
| Staging | https://prometheus-admin-panel-staging.internal.justin.tv |
| Prod | https://prometheus-admin-panel-prod.internal.justin.tv |

## Prometheus VPC Beanstalk Endpoints
| Environment | Endpoint |
| --- | --- |
| Staging | http://prometheus-staging.us-west-2.elasticbeanstalk.com |
| Prod | http://prometheus-prod.us-west-2.elasticbeanstalk.com |

## Internal Endpoints

| Environment | Endpoint |
| --- | --- |
| Staging | http://prometheus-staging.internal.justin.tv |
| Prod | http://prometheus-prod.internal.justin.tv |

These endpoints are configured through the [Twitch DNS tool](https://dashboard.internal.justin.tv/dns).

Each human readable DNS entry points to the corresponding EBS (Elastic Beanstalk) endpoint. 
Since these underlying beanstalk endpoints are in the prometheus VPC - these internal endpoints are probably useless.

## Networking

Due to new systems / networking process changes, the Prometheus AWS accounts / VPCs are not connected to the Twitch backbone like our previous accounts / services.


### Teleport Remote
[Teleport Remote](https://wiki.twitch.com/display/SEC/Teleport+Remote) is a process by which you setup a bastion within your AWS account allowing SSH access into your VPC.

To ssh into a host:
```unix
TC=teleport-remote-twitch-prometheus-devo ssh <host_in_vpc>
``` 

To setup SSH tunneling:
```unix
TC=teleport-remote-twitch-prometheus-devo ssh -L 8080:prometheus-staging.us-west-2.elasticbeanstalk.com:80 <host_in_vpc>
```
This sets up a tunnel such that `localhost:8080` resolves to `prometheus-staging.us-west-2.elasticbeanstalk.com:80`
This is neccessary to allow local Admin Panel to talk to prometheus.

To setup a socks5 proxy:
```unix
TC=teleport-remote-twitch-prometheus-devo ssh -D 8080 <proxy_host_in_vpc>
```

To curl through your socks5 proxy:
```unix
curl --socks5 localhost:8080 <host_in_vpc>
```

Sample curl to a TWIRP endpoint:
```unix
curl --socks5 localhost:8080 --request POST --header "Content-Type: application/json" --data '{}' prometheus-staging.us-west-2.elasticbeanstalk.com/twirp/code.justin.tv.commerce.prometheus.Prometheus/HealthCheck
```

To configure Insomnia to use your socks5 proxy:
- In preferences check `Enable Proxy`
- Under HTTP Proxy:
```unix
socks5h://localhost:8080
```

## Access To Staging Prometheus
When you need to directly call staging Prometheus APIs, follow these steps (it is very different from how
 you normally call staging endpoints in other services).
 
 1. Install necessary tools. Do the following on the terminal (I'm assuming your laptop is Mac). When running these commands, be on Twitch network.
 
     ```
     brew tap --full twitch/security https://twitch-security-packages.s3.amazonaws.com/homebrew.git
     brew update
     brew install twitch-bastion-util
     teleport-bastion enable
     ```
     If you are curious what these are about, see [wiki](https://wiki.twitch.com/display/SEC/Teleport+Bastion).
 2. Make sure to add your local SSH public key on the [Twitch LDAP tool](https://dashboard.internal.justin.tv/ldaptools/edit).
    The local SSH key can be found at `~/.ssh/id_rsa.pub`.
    
 3. Go to staging Prometheus' AWS account and open EC2 console. Hit "Launch Instance".
    You are going to create your own EC2 instance.
 
 4. Select whatever OS you like (should not matter, Linux is fine). Then hit "Next: Configure Instance Details".
 
 5. Hit the "Network" dropdown and select `vpc-02c4d6a634d761309 | twitch-prometheus-devo`.
 
 6. Scroll to the bottom and open "Advanced Details". In the text input area, copy paste the following:
    ```
    users:
      - name: {your-ldap}
        shell: /bin/bash
        sudo: ['ALL=(ALL) NOPASSWD:ALL']
        ssh-authorized-keys:
          - {your-SSH-key}
    ```
    Don't forget to replace {your-ldap} with your ldap and {your-SSH-key} with your SSH key (see above).
    Don't edit anything else, not even a space.
    
  7. Hit "Review and Launch".
  
  8. Now you have created your own EC2 instance. You can optionally name it if you want to by selecting
     your instance -> Tags -> Add/Edit Tags -> Key=Name, Value={name of your box}. From the list of EC2 instances, find yours and copy the
     Private DNS.
     
  9. Run this command to establish an SSH tunnel:
 
        ```
        TC=teleport-remote-twitch-prometheus-devo ssh -L 8080:prometheus-staging.us-west-2.elasticbeanstalk.com:80 <your-ec2-private-DNS>
        ```
  10. Now you have setup a tunnel - what it means is that _any_ traffic to `localhost:8080` on your laptop goes to
      `prometheus-staging.us-west-2.elasticbeanstalk.com:80` (Prometheus staging endpoint) as long as you have the session running.
      
  11. Try an API call (`Search`) to staging Prometheus now to make sure everything went fine. On Insomnia or Postman,
  
      POST Endpoint:
      
      `localhost:8080/twirp/code.justin.tv.commerce.prometheus.Prometheus/Search`
  
      JSON: 
      
      ```
        {
            "event_type": "Bits"
        }
      ```
      

## Dependency Management
Prometheus' dependencies are managed with [dep](https://github.com/golang/dep)
### Add new dependency
After actually importing the new dependency in the source code, run:
```
make dep
```

### Update existing dependency
When you want the latest version of a repository Prometheus is dependent on, run:
```
make dep_update
```