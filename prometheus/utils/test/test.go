package test

import (
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func AssertTwirpError(err error, expectedErrorCode twirp.ErrorCode) {
	So(err, ShouldNotBeNil)
	tErr, isTErr := err.(twirp.Error)
	So(isTErr, ShouldBeTrue)
	So(tErr.Code(), ShouldEqual, expectedErrorCode)
}
