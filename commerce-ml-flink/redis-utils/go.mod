module code.justin.tv/commerce/commerce-ml-flink/redis-lambda-writer

go 1.15

require (
	github.com/go-redis/redis/v8 v8.7.1
)