package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

type kv struct {
	Key   string
	Len int64
	Total int64
	SampleKey string
}

func main() {
	ctx := context.Background()
	var sm = map[string]*kv{}
	var ss []*kv

	userIDArg := ""
	if len(os.Args) == 2 {
		userIDArg = fmt.Sprintf("u2:%s", os.Args[1])
	}

	servers := []string{
		"coe10cmfkugo34z5.c5itg2.clustercfg.usw2.cache.amazonaws.com",
	}

	client := redis.NewClusterClient(&redis.ClusterOptions{Addrs: []string{fmt.Sprintf("%s:6379", servers[0])}})

	var cursor uint64
	var n int
	var totalRemoved uint64
	var scannedKeys uint64
	for {
		var keys []string
		var err error

		if userIDArg != "" {
			keys = []string{userIDArg}
		} else {
			keys, cursor, err = client.Scan(ctx, cursor, "*", 10000).Result()
		}
		if err != nil {
			fmt.Printf("Failed to get scan for keys: %s", err.Error())
			os.Exit(1)
		}
		for _, key := range keys {
			scannedKeys += 1
			keyParts := strings.Split(key, ":")
			if len(keyParts) != 2 {
				client.Del(ctx, key)
				continue
			}
			if keyParts[0] != "u2" {
				client.Del(ctx, key)
				continue
			}

			userID := keyParts[1]

			fields, err := client.HGetAll(ctx, key).Result()
			if err != nil {
				fmt.Printf("Failed to get key value: %s\n", err.Error())
				continue
			}

			if len(fields) == 0 {
				client.Del(ctx, key)
				//fmt.Printf("removing key:  %s", key)
				continue
			}

			fieldsToRemove := []string{}
			for fkey, fval := range fields {
				fkeyParts := strings.Split(fkey, ":")
				if len(fkeyParts) != 2 {
					fieldsToRemove = append(fieldsToRemove, fkey)
					continue
				}

				fvalParts := strings.Split(fval, ",")
				if len(fvalParts) != 2 {
					fieldsToRemove = append(fieldsToRemove, fkey)
					continue
				}
				ttlStr := fvalParts[1]
				ttlLong, err := strconv.ParseInt(ttlStr, 10, 64)
				if err != nil {
					fmt.Printf("failed to parse ttl: %s\n", ttlStr)
					continue
				}
				if time.Now().Sub(time.Unix(ttlLong, 0)) > 1*time.Hour {
					fieldsToRemove = append(fieldsToRemove, fkey)
					continue
				}

				fvalInt, err := strconv.Atoi(fvalParts[1])
				if err != nil {
					fmt.Printf("Failed to convert value %s to int: %s\n", fvalParts[1], err.Error())
					continue
				}

				kvk, ok := sm[userID]
				if ok {
					kvk.Len += 1
					kvk.Total += int64(fvalInt)
				} else {
					kvk = &kv{Key: userID, Len: 1, Total: int64(fvalInt), SampleKey: key}
					ss = append(ss, kvk)
					sm[userID] = kvk
				}
				n++
			}
			if len(fieldsToRemove) > 0 {
				//fmt.Printf("Removed fields from %s\n", key)
				client.HDel(ctx, key, fieldsToRemove...)
				totalRemoved += uint64(len(fieldsToRemove))
			}

			if len(fieldsToRemove) == len(fields) {
				client.HDel(ctx, key)
				//fmt.Printf("removing key:  %s", key)
			}

		}
		if cursor == 0 {
			break
		}
		if scannedKeys % 10000 == 0 {
			fmt.Printf("Scanned %d keys...\n", scannedKeys)
		}
		if len(ss) % 10000 == 0 {
			fmt.Printf("keys found: %d, removed: %d fields\n", len(ss), totalRemoved)
		}
	}
	fmt.Println("by total events")
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Len > ss[j].Len
	})
	for i, v := range ss {
		if i == 20 {
			break
		}
		fmt.Printf("%s : %d. Sample: %s\n", v.Key, v.Len, v.SampleKey)
	}
	fmt.Println("By actions")
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Total > ss[j].Total
	})
	for i, v := range ss {
		if i == 20 {
			break
		}
		fmt.Printf("%s : %d. Sample: %s\n", v.Key, v.Total, v.SampleKey)
	}
	fmt.Printf("total: %d\n", n)
	fmt.Printf("total removed: %d\n", totalRemoved)
}