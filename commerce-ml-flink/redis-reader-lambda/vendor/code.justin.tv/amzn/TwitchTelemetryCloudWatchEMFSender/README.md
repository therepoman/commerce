# `TwitchTelemetryCloudWatchEMFSender`

This package implements a TwitchTelemetry [SampleObserver](https://code.amazon.com/packages/TwitchTelemetry/blobs/581a8486ceacb2390ec6d4f588fc0dcd8abfe60c/--/sample_observer_interface.go) that sends log-based metrics to CloudWatch.
