package logger

type maxMetricDefinitionsError struct{}

func (e maxMetricDefinitionsError) Error() string {
	return "the embedded metric format supports a maximum of 100 metric definition objects"
}

type maxMetricValuesError struct{}

func (e maxMetricValuesError) Error() string {
	return "the embedded metric format supports a maximum of 100 values per key"
}
