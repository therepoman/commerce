package logger

import "sort"

const (
	DimensionService        = "Service"
	DimensionStage          = "Stage"
	DimensionSubstage       = "Substage"
	DimensionRegion         = "Region"
	DimensionProcessAddress = "ProcessAddress"
	DimensionOperation      = "Operation"
	DimensionDependency     = "Dependency"
)

var standardDimensions = []string{
	DimensionService,
	DimensionStage,
	DimensionSubstage,
	DimensionRegion,
	DimensionProcessAddress,
	DimensionOperation,
	DimensionDependency,
}

const millisecondsInASecond = 1000

func truncateDimensions(dimensions map[string]string) map[string]string {
	dimensionsToKeep := make(map[string]string)
	// Prioritize standard dimensions
	for _, standardDimension := range standardDimensions {
		dimValue, exists := dimensions[standardDimension]
		if exists {
			dimensionsToKeep[standardDimension] = dimValue
		}
	}

	var allKeys []string
	for key, _ := range dimensions {
		allKeys = append(allKeys, key)
	}
	sort.Strings(allKeys)

	for _, dimName := range allKeys {
		_, exists := dimensionsToKeep[dimName]
		if !exists {
			dimensionsToKeep[dimName] = dimensions[dimName]
		}
		if len(dimensionsToKeep) == maxDimensions {
			break
		}
	}
	return dimensionsToKeep
}

func copyStore(s *store) store {
	// copy buckets
	newBuckets := buckets{}
	for k, b := range s.buckets {
		newBuckets[k] = b
	}
	// copy bucket keys
	newBucketKeys := make([]string, len(s.bucketKeys))
	copy(newBucketKeys, s.bucketKeys)

	newStore := store{
		namespace:  s.namespace,
		buckets:    newBuckets,
		bucketKeys: newBucketKeys,
		resolution: s.resolution,
	}
	return newStore
}
