package data

type RedisLambdaRequest struct {
	Model     string  `json:"model"`
	UserID    string  `json:"user_id"`
	ChannelID *string `json:"channel_id,omitempty"`
	IP        string  `json:"ip,omitempty"`
	DeviceID  string  `json:"device_id,omitempty"`
}

type PurchaseReputationLowLatencyFeatures struct {
	MinuteWatchChannel           map[string]int    `json:"minute_watch_channel"`
	ChatChannel                  map[string]int    `json:"chat_channel"`
	EventCounts                  map[string]int    `json:"event_counts"`
	Last5IP                      []string          `json:"last_5_ip,omitempty"`
	TotalEvents                  int               `json:"total_events"`
	AdditionalLowLatencyFeatures map[string]string `json:"additional_low_latency_features"`
}
