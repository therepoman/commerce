package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	emf "code.justin.tv/amzn/TwitchTelemetryCloudWatchEMFSender"
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/clients"
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/purchase_rep"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Env                        string
	Cloudwatch                 clients.Cloudwatch
	Redis                      *redis.ClusterClient
	PurchaseRepFeaturesFetcher purchase_rep.PurchaseReputationFeaturesFetcher
	cloudwatchEMF              *telemetry.SampleReporter
}

func (l *lambdaHandler) Setup() error {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	env := os.Getenv("ENV")

	if host == "" || port == "" {
		fmt.Printf("Missing host or port")
		os.Exit(1)
	}

	l.Env = env
	l.Redis = clients.NewRedis(host, port)

	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		log.Fatal("unable to load SDK config: ", err)
	}
	l.Cloudwatch = clients.NewCloudwatch(cloudwatch.NewFromConfig(cfg))
	l.PurchaseRepFeaturesFetcher = purchase_rep.NewPurchaseReputationFeaturesFetcher(l.Env, l.Redis, l.Cloudwatch)

	tPid := &identifier.ProcessIdentifier{
		Service:  "CommerceMLFlink",
		Stage:    env,
		Substage: "Primary",
		Region:   "us-west-2",
		Machine:  "flink-reader-lambda",
	}

	sampleObserver := emf.New(tPid.Service, os.Stdout)
	l.cloudwatchEMF = &telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: *tPid},
		SampleObserver: sampleObserver,
	}

	return nil
}

func (l *lambdaHandler) HandlePurchaseRep(ctx context.Context, r data.RedisLambdaRequest) (data.PurchaseReputationLowLatencyFeatures, error) {
	features, err := l.PurchaseRepFeaturesFetcher.GetLowLatencyFeatures(ctx, r.UserID, r.ChannelID, r.DeviceID, r.IP)
	if err != nil {
		return data.PurchaseReputationLowLatencyFeatures{}, err
	}

	return features, nil
}

func (l *lambdaHandler) HandleRequest(ctx context.Context, r data.RedisLambdaRequest) (interface{}, error) {
	if r.Model == "purchase_rep" {
		startTime := time.Now()

		l.cloudwatchEMF.OperationName = "GetPurchaseReputationLowLatencyFeatures"

		res, err := l.HandlePurchaseRep(ctx, r)

		if err == nil {
			l.cloudwatchEMF.ReportAvailabilitySamples(telemetry.AvailabilityCodeSucccess)
		} else {
			l.cloudwatchEMF.ReportAvailabilitySamples(telemetry.AvailabilityCodeServerError)
		}

		l.cloudwatchEMF.ReportDurationSample(telemetry.MetricDuration, time.Since(startTime))
		l.cloudwatchEMF.Flush()

		return res, err
	}
	return "", fmt.Errorf("invalid model")
}

func main() {
	l := lambdaHandler{}
	if err := l.Setup(); err != nil {
		fmt.Printf("failed to initialize lambda: %s", err.Error())
		os.Exit(1)
	}
	if len(os.Args) == 1 {
		lambda.Start(l.HandleRequest)
	} else {
		logrus.SetOutput(os.Stdout)
		channelID := os.Args[2]
		features, err := l.HandleRequest(context.Background(), data.RedisLambdaRequest{
			Model:     "purchase_rep",
			UserID:    os.Args[1],
			ChannelID: &channelID,
			DeviceID:  os.Args[3],
			IP:        os.Args[4],
		})
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			os.Exit(1)
		}
		b, err := json.MarshalIndent(features, "", "  ")
		if err != nil {
			fmt.Println("error:", err)
		}
		fmt.Printf("Features: %s", b)
	}
}
