package purchase_rep

import (
	"context"
	"errors"
	"fmt"
	"os"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/clients"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
	feature_types "code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/purchase_rep/features"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

const (
//channelsLimit = 10
//	tokensLimit   = 20
)

type PurchaseReputationFeaturesFetcher interface {
	GetLowLatencyFeatures(ctx context.Context, userID string, channelID *string, deviceID string, ip string) (data.PurchaseReputationLowLatencyFeatures, error)
}

type purchaseReputationFeaturesFetcher struct {
	env   string
	redis *redis.ClusterClient
	cw    clients.Cloudwatch
}

type OutdatedInvestigation struct {
	UserID      string
	Key         string
	TimeAdded   int64
	TimeNow     int64
	DiffMinutes float64
}

type ConversionResults struct {
	Errors           []string
	OutdatedFeatures []OutdatedInvestigation
	SumCounts        int
}

func NewPurchaseReputationFeaturesFetcher(env string, r *redis.ClusterClient, cw clients.Cloudwatch) PurchaseReputationFeaturesFetcher {
	return &purchaseReputationFeaturesFetcher{
		env:   env,
		redis: r,
		cw:    cw,
	}
}

func (fetcher *purchaseReputationFeaturesFetcher) GetLowLatencyFeatures(ctx context.Context, userID string, channelID *string, deviceID string, ip string) (data.PurchaseReputationLowLatencyFeatures, error) {
	keyByFeatureGroup := map[feature_types.FeatureGroup]string{
		feature_types.User: fmt.Sprintf("u2:%s", userID),
		// feature_types.Device: fmt.Sprintf("device-prod:%s", deviceID),
		// feature_types.IP:     fmt.Sprintf("ip-prod:%s", ip),
	}
	featureProcessor := feature_types.GetFeatureProcessor()
	for featureGroup, redisKey := range keyByFeatureGroup {
		res, err := fetcher.redis.HGetAll(ctx, redisKey).Result()
		if err != nil {
			if errors.Is(err, os.ErrDeadlineExceeded) {
				logrus.WithError(err).WithFields(logrus.Fields{"userID": userID}).Error("timeout getting data for user")
				return data.PurchaseReputationLowLatencyFeatures{}, nil
			} else {
				logrus.WithError(err).WithFields(logrus.Fields{"userID": userID}).Error("failed to get data for user")
				return data.PurchaseReputationLowLatencyFeatures{}, fmt.Errorf("failed to get data for user")
			}
		}
		for fkey, fval := range res {
			featureProcessor.Process(featureGroup, fkey, fval, channelID, userID, deviceID, ip)
			// if err != nil {
			// 	conversionResult.Errors = append(conversionResult.Errors, err.Error())
			// }
		}
	}

	features := featureProcessor.GetFeatures()
	var strChannelID string
	if channelID != nil {
		strChannelID = *channelID
	}

	logrus.WithFields(logrus.Fields{
		"userID":       userID,
		"channelID":    strChannelID,
		"deviceID":     deviceID,
		"ip":           ip,
		"totalMetrics": features.TotalEvents,
	}).Info("retrieving features")

	if features.TotalEvents == 0 {
		logrus.WithFields(logrus.Fields{
			"userID":           userID,
			"outdatedFeatures": 0,
			"totalMetrics":     0,
		}).Info("no metrics: user has no metrics")
	}

	return features, nil
}
