package features

import (
	"fmt"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/lib"
	"github.com/sirupsen/logrus"
)

type ipFeature struct {
	features map[string]string
}

func createIPFeature() *ipFeature {
	return &ipFeature{
		features: map[string]string{},
	}
}

func (f *ipFeature) Process(context ProcessContext) error {
	if len(context.ip) == 0 || lib.FindIPNet(context.ip) {
		logrus.WithFields(logrus.Fields{
			"userID": context.userID,
			"ip":     context.ip,
		}).Info("ignored ip address since its local network")
		return nil
	}

	f.features[fmt.Sprintf("current_ip_%s", context.fkey)] = context.valParts[0]

	return nil
}

func (f *ipFeature) GetFeatures(features data.PurchaseReputationLowLatencyFeatures) data.PurchaseReputationLowLatencyFeatures {

	for key, val := range f.features {
		features.AdditionalLowLatencyFeatures[key] = val
		features.TotalEvents += 1
	}

	return features
}
