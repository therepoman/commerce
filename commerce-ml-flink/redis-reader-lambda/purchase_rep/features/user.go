package features

import (
	"fmt"
	"strconv"
	"strings"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
)

type userFeature struct {
	MinuteWatchChannel map[string]int
	ChatChannel        map[string]int
	EventCounts        map[string]int
	UserFeatures       map[string]string
	TotalEvents        int
}

func createUserFeature() *userFeature {
	return &userFeature{
		MinuteWatchChannel: map[string]int{},
		ChatChannel:        map[string]int{},
		EventCounts:        map[string]int{},
		UserFeatures:       map[string]string{},
	}
}

func (f *userFeature) Process(context ProcessContext) error {
	if len(context.keyParts) == 1 {
		// new user feature
		featureName := context.keyParts[0]
		featureValue := context.valParts[0]

		f.UserFeatures[fmt.Sprintf("user_%s", featureName)] = featureValue
		return nil
	}

	if len(context.keyParts) > 2 {
		return fmt.Errorf("invalid key format (len : > 2) key: %s, user: %s", context.fkey, context.userID)
	}

	featureName := context.keyParts[0]

	if strings.HasPrefix(featureName, "channel_feature_") {
		return nil
	}

	count, err := strconv.Atoi(context.valParts[0])
	if err != nil {
		return fmt.Errorf("error: invalid format: %+v, %s, %s", context.keyParts[0], err.Error(), context.userID)
	}

	if featureName == "mw" {
		channelID := context.keyParts[1]

		f.MinuteWatchChannel[channelID] += count
		f.TotalEvents += 1

	} else if featureName == "ch" {
		channelID := context.keyParts[1]

		f.ChatChannel[channelID] += count
		f.TotalEvents += 1

	} else if featureName == "ec" {
		token := context.keyParts[1]

		f.EventCounts[token] += count
		f.TotalEvents += 1
	}

	return nil
}

func (f *userFeature) GetFeatures(features data.PurchaseReputationLowLatencyFeatures) data.PurchaseReputationLowLatencyFeatures {
	features.MinuteWatchChannel = f.MinuteWatchChannel
	features.ChatChannel = f.ChatChannel
	features.EventCounts = f.EventCounts
	features.TotalEvents += f.TotalEvents

	for key, count := range f.UserFeatures {
		features.AdditionalLowLatencyFeatures[key] = count
		features.TotalEvents += 1
	}

	return features
}
