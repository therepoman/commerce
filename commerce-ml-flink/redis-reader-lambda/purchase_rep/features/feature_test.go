package features

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFeatureProcessor(t *testing.T) {
	Convey("should correctly parse features", t, func() {
		farAwayTimestamp := 4117328677
		userID := "user1"
		deviceID := "device1"
		ip := "ip1"
		channelID := "channel1"

		fp := GetFeatureProcessor()
		err := fp.Process(User, "fraud_check_count_30d", fmt.Sprintf("1,%d", farAwayTimestamp), &channelID, userID, deviceID, ip)
		So(err, ShouldBeNil)
		err = fp.Process(User, "mw:channel1", fmt.Sprintf("1,%d", farAwayTimestamp), &channelID, userID, deviceID, ip)
		So(err, ShouldBeNil)
		err = fp.Process(User, "channel_feature_channel1:had_prime_sub_lifetime", fmt.Sprintf("1,%d", farAwayTimestamp), &channelID, userID, deviceID, ip)
		So(err, ShouldBeNil)

		features := fp.GetFeatures()
		So(len(features.AdditionalLowLatencyFeatures), ShouldEqual, 3)
		So(features.AdditionalLowLatencyFeatures["user_fraud_check_count_30d"], ShouldEqual, "1")
		So(features.MinuteWatchChannel["channel1"], ShouldEqual, 1)
		So(features.AdditionalLowLatencyFeatures["user_channel_had_prime_sub_lifetime_count"], ShouldEqual, "1")
		So(features.AdditionalLowLatencyFeatures["current_channel_had_prime_sub_lifetime"], ShouldEqual, "1")
	})
}
