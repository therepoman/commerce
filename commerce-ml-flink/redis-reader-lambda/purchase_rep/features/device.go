package features

import (
	"fmt"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
)

type deviceFeature struct {
	features map[string]string
}

func createDeviceFeature() *deviceFeature {
	return &deviceFeature{
		features: map[string]string{},
	}
}

func (f *deviceFeature) Process(context ProcessContext) error {
	if len(context.deviceID) == 0 {
		return nil
	}
	f.features[fmt.Sprintf("current_device_%s", context.fkey)] = context.valParts[0]

	return nil
}

func (f *deviceFeature) GetFeatures(features data.PurchaseReputationLowLatencyFeatures) data.PurchaseReputationLowLatencyFeatures {

	for key, val := range f.features {
		features.AdditionalLowLatencyFeatures[key] = val
		features.TotalEvents += 1
	}

	return features
}
