package features

import (
	"fmt"
	"strconv"
	"strings"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
)

type userChannelFeature struct {
	channelFeatureCount map[string]int
	channelFeatures     map[string]string
	SumCounts           int
}

func createUserChannelFeature() *userChannelFeature {
	return &userChannelFeature{
		channelFeatureCount: map[string]int{},
		channelFeatures:     map[string]string{},
	}
}

func (f *userChannelFeature) Process(context ProcessContext) error {
	if !strings.HasPrefix(context.fkey, "channel_feature_") {
		return nil
	}

	if len(context.keyParts) != 2 {
		return fmt.Errorf("invalid key format (len : != 2) key: %s, user: %s", context.fkey, context.userID)
	}

	featureDefinition := context.keyParts[0]
	featureName := context.keyParts[1]

	channelFeatureParts := strings.Split(featureDefinition, "_")
	if len(channelFeatureParts) != 3 {
		return fmt.Errorf("error: invalid channel feature format (len : != 3) featureName: %s, user: %s", featureDefinition, context.userID)
	}

	featureChannelID := channelFeatureParts[2]
	featureValue := context.valParts[0]

	// legacy issues
	if featureChannelID[0] == '#' {
		return nil
	}

	f.channelFeatureCount[fmt.Sprintf("user_channel_%s_count", featureName)] += 1

	if context.channelID != nil && *context.channelID == featureChannelID {
		f.channelFeatures[fmt.Sprintf("current_channel_%s", featureName)] = featureValue
	}

	return nil
}

func (f *userChannelFeature) GetFeatures(features data.PurchaseReputationLowLatencyFeatures) data.PurchaseReputationLowLatencyFeatures {
	for key, count := range f.channelFeatureCount {
		features.AdditionalLowLatencyFeatures[key] = strconv.Itoa(count)
		features.TotalEvents += 1
	}

	for key, val := range f.channelFeatures {
		features.AdditionalLowLatencyFeatures[key] = val
		features.TotalEvents += 1
	}

	return features
}
