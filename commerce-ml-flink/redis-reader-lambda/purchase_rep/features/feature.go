package features

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-reader/data"
)

type FeatureGroup string

const (
	User   FeatureGroup = "user"
	IP     FeatureGroup = "ip"
	Device FeatureGroup = "device"
)

type ProcessContext struct {
	fkey      string
	fval      string
	keyParts  []string
	valParts  []string
	channelID *string
	userID    string
	deviceID  string
	ip        string
}

type Feature interface {
	Process(context ProcessContext) error
	GetFeatures(features data.PurchaseReputationLowLatencyFeatures) data.PurchaseReputationLowLatencyFeatures
}

type FeatureProcessor struct {
	featureTypes map[FeatureGroup][]Feature
}

func GetFeatureProcessor() *FeatureProcessor {
	return &FeatureProcessor{
		featureTypes: map[FeatureGroup][]Feature{
			User: {
				createUserFeature(),
				createUserChannelFeature(),
			},
			// IP: {
			// 	createIPFeature(),
			// },
			// Device: {
			// 	createDeviceFeature(),
			// },
		},
	}
}

func (fp *FeatureProcessor) Process(group FeatureGroup, fkey string, fval string, channelID *string, userID string, deviceID string, ip string) error {
	keyParts := strings.Split(fkey, ":")
	valParts := strings.Split(fval, ",")

	if len(valParts) != 2 {
		return fmt.Errorf("error: value parts != 2: %s", valParts)
	}

	timeAdded := valParts[1]
	timeAddedLong, err := strconv.ParseInt(timeAdded, 10, 64)
	if err != nil {
		return fmt.Errorf("error: invalid time format: %s, %s", timeAdded, err.Error())
	}
	tm := time.Unix(timeAddedLong, 0)
	diff := time.Since(tm)

	if diff > 1*time.Hour {
		return nil
	}

	for _, featureType := range fp.featureTypes[group] {
		if err := featureType.Process(ProcessContext{
			fkey:      fkey,
			fval:      fval,
			keyParts:  keyParts,
			valParts:  valParts,
			channelID: channelID,
			userID:    userID,
			deviceID:  deviceID,
			ip:        ip,
		}); err != nil {
			return err
		}
	}
	return nil
}

func (fp *FeatureProcessor) GetFeatures() data.PurchaseReputationLowLatencyFeatures {
	features := data.PurchaseReputationLowLatencyFeatures{
		MinuteWatchChannel:           map[string]int{},
		EventCounts:                  map[string]int{},
		ChatChannel:                  map[string]int{},
		TotalEvents:                  0,
		AdditionalLowLatencyFeatures: map[string]string{},
	}
	for _, featureTypes := range fp.featureTypes {
		for _, featureType := range featureTypes {
			features = featureType.GetFeatures(features)
		}
	}

	return features
}
