package purchase_rep

import (
	"fmt"
	"strconv"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGetFeatures(t *testing.T) {
	Convey("for a given timestamp should be true", t, func() {
		tmLong, _ := strconv.ParseInt("1618607386", 10, 64)
		println(tmLong)
		fmt.Println(time.Since((time.Unix(tmLong, 0))) > 1*time.Hour)
	})
}
