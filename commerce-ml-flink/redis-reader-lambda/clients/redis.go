package clients

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 . Redis
type Redis interface {
	ForEachSlave(ctx context.Context, fn func(ctx context.Context, client *redis.Client) error) error
	Scan(ctx context.Context, cursor uint64, match string, count int64) *redis.ScanCmd
}

type redisWrapper struct {
	r redis.ClusterClient
}

func NewRedis(host string, port string) *redis.ClusterClient {
	client := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs:       []string{fmt.Sprintf("%s:%s", host, port)},
		ReadTimeout: 100 * time.Millisecond,
		MaxRetries:  -1,
	})

	fmt.Printf("Connecting to %s:%s\n", host, port)

	return client
}
