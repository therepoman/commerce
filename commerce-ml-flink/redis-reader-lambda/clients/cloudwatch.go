package clients

import (
	"context"
	cw "github.com/aws/aws-sdk-go-v2/service/cloudwatch"
)

type Cloudwatch interface {
	PutMetricData(ctx context.Context, params *cw.PutMetricDataInput, optFns ...func(*cw.Options)) (*cw.PutMetricDataOutput, error)
}

type cloudwatch struct {
	*cw.Client
}

func NewCloudwatch(client *cw.Client) Cloudwatch {
	return &cloudwatch{client}
}