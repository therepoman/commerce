package mockingbird.metrics;

import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import org.apache.flink.metrics.Gauge;
import org.apache.flink.metrics.MetricGroup;

public class CloudWatchGauge extends CloudWatchMetric {
    private final String metricName;
    private final MetricGroup group;
    private final Gauge<Number> metric;

    public CloudWatchGauge(String metricName, Dimension[] dimensions, MetricGroup group, Gauge<Number> gague) {
        super(metricName, dimensions, group, gague);
        this.group = group;
        this.metric = gague;
        this.metricName = metricName;
    }

    @Override
    public StandardUnit getUnit() {
        return StandardUnit.None;
    }

    @Override
    public double getValue() {
        return metric.getValue().doubleValue();
    }

}
