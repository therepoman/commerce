package mockingbird.metrics;

import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import org.apache.flink.metrics.Meter;
import org.apache.flink.metrics.MetricGroup;

public class CloudWatchMeter extends CloudWatchMetric {
    private final String metricName;
    private final MetricGroup group;
    private final Meter metric;

    public CloudWatchMeter(String metricName, Dimension[] dimensions, MetricGroup group, Meter metric) {
        super(metricName, dimensions, group, metric);
        this.group = group;
        this.metric = metric;
        this.metricName = metricName;
    }

    @Override
    public StandardUnit getUnit() {
        return StandardUnit.CountSecond;
    }

    @Override
    public double getValue() {
        return metric.getRate();
    }

}
