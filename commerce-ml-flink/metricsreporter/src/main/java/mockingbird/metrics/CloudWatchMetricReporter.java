package mockingbird.metrics;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import lombok.RequiredArgsConstructor;
import org.apache.flink.metrics.*;
import org.apache.flink.metrics.reporter.MetricReporter;
import org.apache.flink.metrics.reporter.Scheduled;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The reporter will only report metrics with the Operator scope. It supports Counters (Count) and Meters (Count/Second).
 * <p>
 * Counters are reported on closure, while meters are reported on a schedule.
 * The metrics in cloud watch will have two dimensions:
 * <ul>
 *   <li> Job: the job name, and
 *   <li> Operator: the operator name;
 * </ul>
 *
 * <p>
 * Note: the class is abstract in order to remind developers that Flink can only accept a MetricRepoter that has a no-arg constructor.
 * Dev should create a sub-class that participates in the dependency injection framework of the system in order to register the reporter to Flink.
 */



@RequiredArgsConstructor
public abstract class CloudWatchMetricReporter implements Scheduled, MetricReporter {

    private final int CLOUDWATCH_PUTMETRICS_METRICDATA_SIZE_LIMIT = 20;
    protected final AmazonCloudWatch cloudwatch;
    protected final String namespace;
    private final String filters = "task_name,Buffers,ClassLoader,GarbageCollector,Netty";

    private final Map<String, Boolean> metrics = Collections.synchronizedMap(new HashMap<>());
    private final Map<Metric, CloudWatchMetric> reportScheduled = Collections.synchronizedMap(new HashMap<>());
    private final Map<Metric, CloudWatchMetric> reportOnce = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void open(MetricConfig config) {
    }

    @Override
    public void close() {
        report();
    }

    private ArrayList<Dimension> getDimensionsFromComponents(Map<String, String> usefulComponents) {
        ArrayList<Dimension> dimensions = new ArrayList<>();
        if (usefulComponents.containsKey("operator_name") && usefulComponents.containsKey("job_name")) {
            dimensions.add(new Dimension().withName("Job Name").withValue(usefulComponents.get("job_name")));
            dimensions.add(new Dimension().withName("Operator Name").withValue(usefulComponents.get("operator_name")));
        } else if (usefulComponents.containsKey("host")) {
            dimensions.add(new Dimension().withName("Host").withValue(usefulComponents.get("host")));
        }
        return dimensions;
    }

    @Override
    public void notifyOfAddedMetric(Metric metric, String metricName, MetricGroup group) {
        ArrayList<String> components = new ArrayList<>(Arrays.asList(group.getScopeComponents()));
        Map<String, String> usefulComponents = new HashMap<>();
        for (Map.Entry<String, String> e : group.getAllVariables().entrySet()) {
            if (components.contains(e.getValue())) {
                if (!e.getValue().isBlank()) {
                    String k = e.getKey().substring(1, e.getKey().length() - 1);
                    usefulComponents.put(k, e.getValue());
                }
                components.remove(e.getValue());
            }
        }
        components.add(metricName);
        String name = String.join(".", components);

        ArrayList<Dimension> dimensions = getDimensionsFromComponents(usefulComponents);
        if (dimensions.size() == 0) {
            return;
        }

        CloudWatchMetric cwMetric;
        if (metric instanceof Counter) {
            cwMetric = new CloudWatchCounter(name, dimensions.toArray(new Dimension[0]), group, (Counter) metric);
        } else if (metric instanceof Meter) {
            cwMetric = new CloudWatchMeter(name, dimensions.toArray(new Dimension[0]), group, (Meter) metric);
        } else if (metric instanceof Gauge && ((Gauge<?>) metric).getValue() instanceof Number) {
            cwMetric = new CloudWatchGauge(name, dimensions.toArray(new Dimension[0]), group, (Gauge<Number>) metric);
        } else {
            return;
        }

        String key = cwMetric.getDimensionsAsString() + " : " + cwMetric.getMetricName();

        for (String filter : this.filters.split(",")) {
            if (!filter.equals("") && key.contains(filter)) {
                return;
            }
        }
        if (this.metrics.containsKey(key)) {
            return;
        }
        System.out.println(key);
        this.metrics.put(key, true);

        reportScheduled.put(metric, cwMetric);
    }

    @Override
    public void notifyOfRemovedMetric(Metric metric, String metricName, MetricGroup group) {
        if (reportScheduled.containsKey(metric)) {
            CloudWatchMetric cwMetric = reportScheduled.remove(metric);
            reportOnce.put(metric, cwMetric);
        }
    }

    private boolean isOperatorScope(MetricGroup group) {
        return group.getAllVariables().containsKey("<operator_id>");
    }

    @Override
    public void report() {
        List<CloudWatchMetric> toReport = new ArrayList<>();
        toReport.addAll(reportScheduled.values());
        toReport.addAll(reportOnce.values());

        List<List<CloudWatchMetric>> batches = new ArrayList<>();
        List<CloudWatchMetric> batch = new ArrayList<>();
        batches.add(batch);
        int batchSize = 0;
        for (CloudWatchMetric metric : toReport) {
            int size = metric.size();
            if (batchSize + size > CLOUDWATCH_PUTMETRICS_METRICDATA_SIZE_LIMIT) {
                batch = new ArrayList<>();
                batches.add(batch);
                batchSize = 0;
            }
            batch.add(metric);
            batchSize += size;
        }

        for (List<CloudWatchMetric> metrics : batches) {
            if (metrics.isEmpty()) {
                continue;
            }
            List<MetricDatum> metricData = metrics.stream().map(CloudWatchMetric::getMetricData).flatMap(List::stream).collect(Collectors.toList());

            PutMetricDataRequest request = new PutMetricDataRequest().withNamespace(namespace).withMetricData(metricData);
            cloudwatch.putMetricData(request);
            metrics.stream().forEach(m -> reportOnce.remove(m.getMetric()));
        }

    }
}
