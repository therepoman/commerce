package mockingbird.metrics;

import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.MetricGroup;

import java.util.Map;

public class CloudWatchCounter extends CloudWatchMetric {
    private final MetricGroup group;
    private final Counter metric;
    private final String metricName;
    private final String jobName;
    private final String operatorName;

    public CloudWatchCounter(String metricName, Dimension[] dimensions, MetricGroup group, Counter metric) {
        super(metricName, dimensions, group, metric);
        this.group = group;
        this.metric = metric;
        this.metricName = metricName;

        Map<String, String> vars = group.getAllVariables();
        this.jobName = vars.get("<job_name>");
        this.operatorName = vars.get("<operator_name>");
    }

    @Override
    public StandardUnit getUnit() {
        return StandardUnit.Count;
    }

    @Override
    public double getValue() {
        return metric.getCount();
    }
}
