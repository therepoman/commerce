package mockingbird.metrics;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.retry.PredefinedRetryPolicies;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;

public class MockingbirdCloudwatchMetricReporter extends CloudWatchMetricReporter {

    public MockingbirdCloudwatchMetricReporter() {
        super(getCloudWatch(), "Mockingbird");
    }

    public static AmazonCloudWatch getCloudWatch() {
        return AmazonCloudWatchClientBuilder.standard()
                .withClientConfiguration(new ClientConfiguration().withRetryPolicy(PredefinedRetryPolicies.getDefaultRetryPolicy()))
                .withRegion(Regions.US_WEST_2)
                .build();
    }

}