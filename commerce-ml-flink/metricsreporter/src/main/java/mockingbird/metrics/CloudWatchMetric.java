package mockingbird.metrics;

import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import org.apache.flink.metrics.Metric;
import org.apache.flink.metrics.MetricGroup;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class CloudWatchMetric {
    abstract StandardUnit getUnit();

    abstract double getValue();

    private final Dimension[] dimensions;
    private final String metricName;
    private final Metric metric;

    int size() {
        return 1;
    }

    public CloudWatchMetric(String metricName, Dimension[] dimensions, MetricGroup group, Metric metric) {
        this.metric = metric;
        this.metricName = metricName;
        this.dimensions = dimensions;
    }



    Metric getMetric() {
        return this.metric;
    }
    String getMetricName() { return this.metricName; }

    public String getDimensionsAsString() {
        return Arrays.stream(this.dimensions).map(d -> d.getName() + "=" + d.getValue()).collect(Collectors.joining(" "));
    }

    List<MetricDatum> getMetricData() {
        double value = getValue();
        return Arrays.asList(
                new MetricDatum()
                        .withMetricName(metricName)
                        .withDimensions(dimensions)
                        .withUnit(getUnit())
                        .withValue(value));
    }
}
