import {AwsAccountConfig} from "./config";
import {Stack} from "@aws-cdk/core";
import * as ec2 from "@aws-cdk/aws-ec2";

export function getVpc(stack: Stack, accountConfig: AwsAccountConfig): ec2.IVpc {
  return ec2.Vpc.fromVpcAttributes(stack, 'vpc', {
    vpcId: accountConfig.vpcId,
    availabilityZones: [`${accountConfig.region}a`, `${accountConfig.region}b`, `${accountConfig.region}c`],
    privateSubnetIds: [accountConfig.privateSubnet1, accountConfig.privateSubnet2, accountConfig.privateSubnet3],
  });
}

export function getSg(stack: Stack, accountConfig: AwsAccountConfig): ec2.ISecurityGroup {
  return ec2.SecurityGroup.fromSecurityGroupId(stack, 'sg', accountConfig.sg);
}

