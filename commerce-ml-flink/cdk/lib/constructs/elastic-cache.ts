import * as ec2 from '@aws-cdk/aws-ec2';
import * as ec from '@aws-cdk/aws-elasticache';
import * as route53 from '@aws-cdk/aws-route53';
import * as cdk from '@aws-cdk/core';

export interface ElasticCacheResourceProps extends Partial<ec.CfnReplicationGroupProps> {
  /**
   * VPC to attach the Cache to
   */
  vpc: ec2.IVpc;
  sg: ec2.ISecurityGroup;
}

export interface IpWhitelistProps {
  Ip: string;
  Description?: string;
}

export class ElasticCache extends ec.CfnReplicationGroup {
  private vpc: ec2.IVpc;

  constructor(parent: cdk.Construct, name: string, props: ElasticCacheResourceProps) {
    super(parent, name, {
      cacheParameterGroupName: "default.redis6.x.cluster.on",
      autoMinorVersionUpgrade: true,
      automaticFailoverEnabled: true,
      cacheNodeType: 'cache.r6g.xlarge',
      engine: 'redis',
      engineVersion: "6.x",
      numNodeGroups: 16,
      port: 6379, // Set for reference in later areas
      replicasPerNodeGroup: 2,
      securityGroupIds: [props.sg.securityGroupId],
      ...(props as ec.CfnReplicationGroupProps),
    });

    this.cfnOptions.updatePolicy = {
      useOnlineResharding: true,
    };

    this.vpc = props.vpc;

    if (props.cacheSubnetGroupName === undefined) {
      this.setupSubnetGroup();
    }
  }

  public addAlias(zone: route53.IHostedZone, name: string, recordName: string, ttl?: cdk.Duration) {
    new route53.CnameRecord(this, name + 'ElasticCacheAlias', {
      domainName: this.attrPrimaryEndPointAddress,
      zone,
      recordName,
      ttl,
    });

    return this;
  }

  private setupSubnetGroup() {
    const subnetGroup = new ec.CfnSubnetGroup(this, 'PrivateSubnets', {
      description: `Subnet group for ${this.node.id}`,
      subnetIds: this.vpc.privateSubnets.map((subnet) => subnet.subnetId),
    });

    this.cacheSubnetGroupName = subnetGroup.ref;
  }
}