export interface AwsAccountConfig {
    vpcId: string
    sg: string
    privateSubnet1: string;
    privateSubnet2: string;
    privateSubnet3: string;
    region: string;
    keyName: string;
    accountId: string;
    pagerdutyKey: string;
}

interface InstanceConfig {
    instanceType: string;
}

export const awsCommerceScience : AwsAccountConfig = {
    vpcId: 'vpc-07e6129be33d5e802',
    sg: 'sg-044d6c23dff4b86d8',
    privateSubnet1: 'subnet-0c2eb9ceb9f707724',
    privateSubnet2: 'subnet-0d61927ff7db4c939',
    privateSubnet3: 'subnet-0428260edd5a8d7a8',
    region: 'us-west-2',
    keyName: 'commerce-science-aws',
    accountId: '348802193101',
    pagerdutyKey: '7bc91e9856ae4d03d0499493287891e9'

};

