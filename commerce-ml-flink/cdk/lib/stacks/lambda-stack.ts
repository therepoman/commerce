import * as events from "@aws-cdk/aws-events";
import * as targets from "@aws-cdk/aws-events-targets";
import * as iam from "@aws-cdk/aws-iam";
import * as kinesis from "@aws-cdk/aws-kinesis";
import * as lambda from "@aws-cdk/aws-lambda";
import * as lambdaEventSource from '@aws-cdk/aws-lambda-event-sources';
import * as logs from "@aws-cdk/aws-logs";
import { Construct, Duration, Stack } from '@aws-cdk/core';
import * as path from "path";
import { AwsAccountConfig } from "../config";
import { getSg, getVpc } from "../shared";

export interface LambdaStackProps {
  account: AwsAccountConfig,
  env: string,
  redisKinesisStream: string,
  redisClusterHost: string,
  redisClusterPort: string,
}


export class LambdaStack extends Stack {
  constructor(scope: Construct, id: string, props: LambdaStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });

    const stack = this;
    const vpc = getVpc(this, props.account);
    const sg = getSg(this, props.account);

    const lambdaWriter = new lambda.Function(stack, `redis-lambda-${props.env}-writer`, {
      functionName: `redis-lambda-writer-${props.env}`,
      vpc: vpc,
      securityGroup: sg,
      runtime: lambda.Runtime.GO_1_X,
      memorySize: 512,
      handler: "redis-writer",
      code: lambda.Code.fromAsset(path.join(__dirname, "../../../redis-writer-lambda/bin/redis-writer-linux.zip")),
      reservedConcurrentExecutions: 20,
      retryAttempts: 0,
      timeout: Duration.minutes(1),
      description: `Function generated on: ${new Date().toISOString()}`,
      environment: {
        "REDIS_HOST": props.redisClusterHost,
        "REDIS_PORT": props.redisClusterPort,
      }
    });

    const writerAlias = new lambda.Alias(this, 'redis-lambda-writer-alias', {
      aliasName: 'Prod',
      version: lambdaWriter.currentVersion,
    });

    if (!lambdaWriter.role) {
      console.log("lambda doesn't have a role");
      process.exit(1);
    }
    lambdaWriter.role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonElastiCacheFullAccess"));
    writerAlias.addEventSource(new lambdaEventSource.KinesisEventSource(kinesis.Stream.fromStreamArn(this, "kinesis-stream", props.redisKinesisStream), {
      batchSize: 100,
      startingPosition: lambda.StartingPosition.LATEST
    }));

    const lambdaReader = new lambda.Function(stack, `redis-lambda-reader-${props.env}`, {
      functionName: `redis-lambda-reader-${props.env}`,
      vpc: vpc,
      securityGroup: sg,
      runtime: lambda.Runtime.GO_1_X,
      memorySize: 512,
      handler: "redis-reader",
      code: lambda.Code.fromAsset(path.join(__dirname, "../../../redis-reader-lambda/bin/redis-reader-linux.zip")),
      reservedConcurrentExecutions: 20,
      retryAttempts: 0,
      timeout: Duration.minutes(1),
      description: `Function generated on: ${new Date().toISOString()}`,
      environment: {
        "REDIS_HOST": props.redisClusterHost,
        "REDIS_PORT": props.redisClusterPort,
        "ENV": props.env,
      }
    });

    new logs.MetricFilter(this, "lambda_reader_error_metric", {
      logGroup: lambdaReader.logGroup,
      metricNamespace: `CommerceFlinkLambdaReader-${props.env}`,
      metricName: 'LoggedErrors',
      filterPattern: logs.FilterPattern.allTerms("error:"),
    });

    new logs.MetricFilter(this, "lambda_reader_no_values_metric", {
      logGroup: lambdaReader.logGroup,
      metricNamespace: `CommerceFlinkLambdaReader-${props.env}`,
      metricName: 'UserNoMetrics',
      filterPattern: logs.FilterPattern.allTerms("no metrics:"),
    });

    const readerAlias = new lambda.Alias(this, 'redis-lambda-reader-alias', {
      aliasName: 'Prod',
      version: lambdaReader.currentVersion,
    });

    if (!lambdaReader.role) {
      console.log("lambda doesn't have a role");
      process.exit(1);
    }
    lambdaReader.role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("CloudWatchFullAccess"));
    lambdaReader.role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonElastiCacheFullAccess"));
    if (!readerAlias.role) {
      console.log("lambda alias doesn't have a role");
      process.exit(1);
    }
    const lambaReaderInvokerRole = new iam.Role(stack, `personalization-redis-lambda-reader-${props.env}`, {
      roleName: `personalization-lambda-reader-invoke-${props.env}`,
      assumedBy: new iam.CompositePrincipal(
        new iam.AccountPrincipal("808393034690"), // personalization dev
        new iam.AccountPrincipal("426339712469"), // personalization prod
      ),
    });
    readerAlias.grantInvoke(lambaReaderInvokerRole);

    if (props.env == "prod") {
      const lambdaFlinkMonitor = new lambda.Function(stack, `flink-monitor-${props.env}`, {
        functionName: `flink-monitor-${props.env}`,
        vpc: vpc,
        securityGroup: sg,
        runtime: lambda.Runtime.GO_1_X,
        memorySize: 512,
        handler: "flink-monitor",
        code: lambda.Code.fromAsset(path.join(__dirname, "../../../flink-monitor-lambda/bin/flink-monitor-linux.zip")),
        retryAttempts: 0,
        timeout: Duration.minutes(1),
        description: `Function generated on: ${new Date().toISOString()}`,
        environment: {
          "ENV": props.env,
        }
      });

      if (!lambdaFlinkMonitor.role) {
        console.log("lambdaFlinkMonitor doesn't have a role");
        process.exit(1);
      }

      lambdaFlinkMonitor.role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("CloudWatchFullAccess"));

      const readerAlias = new lambda.Alias(this, 'flink-monitor-alias', {
        aliasName: 'Prod',
        version: lambdaFlinkMonitor.currentVersion,
      });

      new events.Rule(this, "flink-monitor-rule", {
        schedule: events.Schedule.rate(Duration.minutes(1)),
        targets: [
          new targets.LambdaFunction(readerAlias)
        ],
      });
    }
  }
}
