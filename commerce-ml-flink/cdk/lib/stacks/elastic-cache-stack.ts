import { Duration, Stack, Construct } from '@aws-cdk/core';
import {AwsAccountConfig} from "../config";
import {getVpc, getSg} from "../shared";
import {ElasticCache} from "../constructs/elastic-cache";
import * as lambda from "@aws-cdk/aws-lambda";
import * as lambdaEventSource from '@aws-cdk/aws-lambda-event-sources';
import * as kinesis from "@aws-cdk/aws-kinesis";
import * as iam from "@aws-cdk/aws-iam";
import * as logs from "@aws-cdk/aws-logs"
import * as path from "path";

export interface KinesisStackProps {
  account: AwsAccountConfig,
  env: string,
}


export class ElasticCacheStack extends Stack {
  public redisCluster: ElasticCache;
  constructor(scope: Construct, id: string, props: KinesisStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });

    const stack = this;
    const vpc = getVpc(this, props.account);
    const sg = getSg(this, props.account);
    this.redisCluster = new ElasticCache(stack, "elastic-cache-cluster", {
      vpc,
      sg,
      replicationGroupDescription: "redis cluster for commerce ml"
    });
  }
}
