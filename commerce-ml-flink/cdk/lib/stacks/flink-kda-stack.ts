import * as iam from "@aws-cdk/aws-iam";
import * as ka from '@aws-cdk/aws-kinesisanalytics-flink';
import * as s3 from "@aws-cdk/aws-s3";
import { Construct, Duration, Stack } from "@aws-cdk/core";
import { AwsAccountConfig } from "../config";
import { getSg, getVpc } from "../shared";

export interface FlinkStackProps {
  account: AwsAccountConfig
  env: string
}


export class FlinkKDAStack extends Stack {
  constructor(scope: Construct, id: string, props: FlinkStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: props.env == 'prod' ? true : false,
    });

    const clusterSize = 128;

    const stack = this;
    const vpc = getVpc(this, props.account);
    const sg = getSg(this, props.account);

    // to create and manage vpc
    const ec2FullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonEC2FullAccess");
    const s3FullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonS3FullAccess");
    const kinesisFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonKinesisFullAccess");
    const elasticCacheFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonElastiCacheFullAccess");
    const cloudwatchFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("CloudWatchFullAccess");
    const cloudwatchLogsFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("CloudWatchLogsFullAccess");

    const jobS3Path = process.env.FLINK_JOB_S3_PATH || '';

    const kdaApp = new ka.Application(stack, 'mockingbird', {
      logLevel: ka.LogLevel.ERROR,
      code: ka.ApplicationCode.fromBucket(s3.Bucket.fromBucketName(stack, "flink-bucket", `commerce-ml-flink-${props.env}`), jobS3Path),
      runtime: ka.Runtime.FLINK_1_11,
      parallelism: clusterSize,
      autoScalingEnabled: false,
      propertyGroups: {
        config: {
          env: props.env,
        }
      },
      checkpointInterval: Duration.seconds(60),
    });
    if (kdaApp.role) {
      kdaApp.role.addManagedPolicy(ec2FullAccess);
      kdaApp.role.addManagedPolicy(s3FullAccess);
      kdaApp.role.addManagedPolicy(kinesisFullAccess);
      kdaApp.role.addManagedPolicy(elasticCacheFullAccess);
      kdaApp.role.addManagedPolicy(cloudwatchFullAccess);
      kdaApp.role.addManagedPolicy(cloudwatchLogsFullAccess);
    }
  }
}
