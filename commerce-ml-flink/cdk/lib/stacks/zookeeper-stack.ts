import {AwsAccountConfig} from "../config";
import {Construct, Stack} from "@aws-cdk/core";
import {getSg, getVpc} from "../shared";
import {Cluster as ZookeeperCluster} from "@twitch-cdk/zookeeper"

export interface FlinkStackProps {
  account: AwsAccountConfig
  env: string
}


export class ZookeeperStack extends Stack {
  zkNodesDnsNames: string[];
  constructor(scope: Construct, id: string, props: FlinkStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });

    const stack = this;
    const vpc = getVpc(this, props.account);
    const sg = getSg(this, props.account);

    const cluster = new ZookeeperCluster(stack, `zookeeper-cluster-${props.env}`, {
      vpc: vpc,
      dnsNamespace: "flink.zookeeper",
    });
    this.zkNodesDnsNames = cluster.zkNodes;
  }
}
