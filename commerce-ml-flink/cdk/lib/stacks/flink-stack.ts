import {AwsAccountConfig} from "../config";
import {Construct, Stack} from "@aws-cdk/core";
import * as iam from "@aws-cdk/aws-iam";
import * as ecs from "@aws-cdk/aws-ecs";
import {getSg, getVpc} from "../shared";
import {Cluster as FlinkCluster, FlinkVersion} from "@twitch-cdk/flink"
import * as path from "path";

export interface FlinkStackProps {
  account: AwsAccountConfig
  env: string
  bucketName: string
  zkNodesDnsNames: string[];
}


export class FlinkStack extends Stack {
  constructor(scope: Construct, id: string, props: FlinkStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });

    const stack = this;
    const vpc = getVpc(this, props.account);
    const sg = getSg(this, props.account);

    const buildArgs: Record<string, string> = {}
    buildArgs["FLINK_VERSION"] = FlinkVersion.V1_11;

    const cluster = new FlinkCluster(stack, `flink-cluster-${props.env}`, {
      dnsNamespace: "flink",
      flinkConf: {
        "state.backend.incremental": "true",
        "state.checkpoints.dir": `s3a://${props.bucketName}/checkpoint-metadata`,
        "state.savepoints.dir": `s3a://${props.bucketName}/savepoints`,
        "high-availability.storageDir": `s3a://${props.bucketName}/recovery`,
        "high-availability.zookeeper.quorum": props.zkNodesDnsNames.join(","),
        "high-availability.cluster-id": "/flink",
        "fs.s3a.acl.default": "BucketOwnerFullControl",
        // "taskmanager.memory.process.size": "140g",
        // "taskmanager.memory.managed.fraction": "0.8",
        // "taskmanager.memory.jvm-metaspace.size": "1g",
        //"taskmanager.memory.jvm-overhead.fraction": "0.5",
        //"jobmanager.memory.jvm-overhead.max": "10g",
        // "metrics.reporter.cloudwatch.class": "mockingbird.metrics.MockingbirdCloudwatchMetricReporter",
        // "metrics.reporter.cloudwatch.interval": "300 SECONDS",
      },
      jobManagerInstances: 2,
      taskManagerInstanceType: "m5.large",
      taskManagerSlots: 1,
      taskManagerInstances: 10,
      vpc: vpc,
      loadBalancerName: `flink-${props.env}`,
      customContainerImage: ecs.ContainerImage.fromAsset(path.join(__dirname, 'image'), {
        buildArgs
      }),
      environment: {
        'ENV': props.env,
      }
    });

    const kinesisFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonKinesisFullAccess");
    const elasticCacheFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonElastiCacheFullAccess");
    const cloudwatchFullAccess = iam.ManagedPolicy.fromAwsManagedPolicyName("CloudWatchFullAccess");
    cluster.jobManagerDefinition.taskRole.addManagedPolicy(kinesisFullAccess);
    cluster.jobManagerDefinition.taskRole.addManagedPolicy(elasticCacheFullAccess);
    cluster.jobManagerDefinition.taskRole.addManagedPolicy(cloudwatchFullAccess);
    cluster.taskManagerDefinition.taskRole.addManagedPolicy(kinesisFullAccess);
    cluster.taskManagerDefinition.taskRole.addManagedPolicy(elasticCacheFullAccess);
    cluster.taskManagerDefinition.taskRole.addManagedPolicy(cloudwatchFullAccess);
    cluster.taskManagerASG.role.addManagedPolicy(kinesisFullAccess);
    cluster.taskManagerASG.role.addManagedPolicy(elasticCacheFullAccess);
    cluster.taskManagerASG.role.addManagedPolicy(cloudwatchFullAccess);
    cluster.taskManagerASG.addSecurityGroup(sg);
    cluster.jobManagerASG.role.addManagedPolicy(kinesisFullAccess);
    cluster.jobManagerASG.role.addManagedPolicy(elasticCacheFullAccess);
    cluster.jobManagerASG.role.addManagedPolicy(cloudwatchFullAccess);
    cluster.jobManagerASG.addSecurityGroup(sg);
  }
}
