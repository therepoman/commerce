import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as cdk from '@aws-cdk/core';
import { latencySecondsToMsMetric, percentMetricByClientServerErrorSuccess, percentMetricByErrorSuccess } from "./common";

const TwitchPersonalizationProd = "426339712469";

function personalizationServiceMetric(label: string, metricName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "TwitchPersonalization",
    metricName: metricName,
    account: TwitchPersonalizationProd,
    region: "us-west-2",
  });
}

function personalizationELBMetric(label: string, metricName: string, elb_name: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "AWS/ApplicationELB",
    metricName: metricName,
    account: TwitchPersonalizationProd,
    region: "us-west-2",
    dimensions: {
      "LoadBalancer": elb_name,
    },
  });
}

function personalizationServiceEndpointMetric(label: string, metricName: string, operationName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "TwitchPersonalization",
    metricName: metricName,
    account: TwitchPersonalizationProd,
    region: "us-west-2",
    dimensions: {
      "Region": "us-west-2",
      "Service": "TwitchPersonalization",
      "Stage": "prod",
      "Substage": "primary",
      "Operation": operationName,
    }
  });
}

function personalizationServiceDependencyMetric(label: string, metricName: string, dependencyName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "TwitchPersonalization",
    metricName: metricName,
    account: TwitchPersonalizationProd,
    region: "us-west-2",
    dimensions: {
      "Region": "us-west-2",
      "Service": "TwitchPersonalization",
      "Stage": "prod",
      "Substage": "primary",
      //"Operation": operationName,
      "Dependency": dependencyName,
    }
  });
}

export function loggedErrors() {
  return new cloudwatch.GraphWidget({
    title: "Logged Errors",
    left: [
      personalizationServiceMetric("LoggedPanics-Primary", "prod-primary-us-west-2-LoggedPanics").with({ statistic: "sum" }),
      personalizationServiceMetric("LoggedErrors-Primary", "prod-primary-us-west-2-LoggedErrors").with({ statistic: "sum" }),
    ],
    right: [
      personalizationServiceMetric("LoggedPanics-Canary", "prod-canary-us-west-2-LoggedPanics").with({ statistic: "sum" }),
      personalizationServiceMetric("LoggedErrors-Canary", "prod-canary-us-west-2-LoggedErrors").with({ statistic: "sum" }),
    ],
  })
}

export function elbStats() {
  const personalizationProdElbName = "app/TwitchP-ALB-8QFZAFP9GDUI/a095ad0815e6d6e2"
  return new cloudwatch.GraphWidget({
    title: "ELB Stats",
    left: [
      personalizationELBMetric("HTTPCode_Target_5XX_Count", "HTTPCode_Target_5XX_Count", personalizationProdElbName).with({ statistic: "sum" }),
      personalizationELBMetric("HTTPCode_ELB_4XX_Count", "HTTPCode_ELB_4XX_Count", personalizationProdElbName).with({ statistic: "sum" }),
    ],
  })
}

export function invocations(operationName: string) {
  const success = personalizationServiceEndpointMetric(`${operationName} success`, "Success", operationName).with({ statistic: "sum" });
  const serverError = personalizationServiceEndpointMetric(`${operationName} servererror`, "ServerError", operationName).with({ statistic: "sum" });
  const clientError = personalizationServiceEndpointMetric(`${operationName} clienterror`, "ClientError", operationName).with({ statistic: "sum" });

  return new cloudwatch.GraphWidget({
    title: `${operationName} Invocations`,
    left: [
      success,
      serverError,
      clientError,
    ], right: [
      percentMetricByClientServerErrorSuccess("Error Rate", success, clientError, serverError),
    ],
    rightYAxis: {
      max: 20,
    },
  });
}

export function latency(operationName: string) {
  return new cloudwatch.GraphWidget({
    title: `${operationName} Latency`,
    left: [
      latencySecondsToMsMetric([
        personalizationServiceEndpointMetric(`${operationName} p99.9`, "Duration", operationName).with({ statistic: "p99.9" }),
        personalizationServiceEndpointMetric(`${operationName} p99`, "Duration", operationName).with({ statistic: "p99" }),
        personalizationServiceEndpointMetric(`${operationName} p90`, "Duration", operationName).with({ statistic: "p90" }),
        personalizationServiceEndpointMetric(`${operationName} p50`, "Duration", operationName).with({ statistic: "p50" }),
      ]),
    ],
  });
}

export function dependencyLatency(title: string, dependencyName: string, scale: boolean) {
  let metrics: cloudwatch.IMetric[] = [
    personalizationServiceDependencyMetric(`${dependencyName} p99.9`, "DependencyDuration", dependencyName).with({ statistic: "p99.9" }),
    personalizationServiceDependencyMetric(`${dependencyName} p99`, "DependencyDuration", dependencyName).with({ statistic: "p99" }),
    personalizationServiceDependencyMetric(`${dependencyName} p90`, "DependencyDuration", dependencyName).with({ statistic: "p90" }),
    personalizationServiceDependencyMetric(`${dependencyName} p50`, "DependencyDuration", dependencyName).with({ statistic: "p50" }),
  ];
  if (scale) {
    metrics = [latencySecondsToMsMetric(metrics)];
  }

  return new cloudwatch.GraphWidget({
    title: title,
    left: metrics,
  });
}

export function dependencyErrors(title: string, dependencyName: string) {
  const successMetric = personalizationServiceDependencyMetric(`${dependencyName} success`, "DependencySuccess", dependencyName).with({ statistic: "sum" });
  const errorMetric = personalizationServiceDependencyMetric(`${dependencyName} errors`, "DependencyServerError", dependencyName).with({ statistic: "sum" });
  return new cloudwatch.GraphWidget({
    title: title,
    left: [
      successMetric,
      errorMetric,
    ], right: [
      percentMetricByErrorSuccess('Error Rate', successMetric, errorMetric),
    ],
    rightYAxis: {
      max: 20,
    },
  });
}