import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import { Construct, Duration } from '@aws-cdk/core';

function kdaMetric(label: string, metricName: string, applicationID: string) {
    return new cloudwatch.Metric({
        label: label,
        period: Duration.seconds(60),
        namespace: "AWS/KinesisAnalytics",
        metricName: metricName,
        dimensions: {
            "Application": applicationID,
        },
    });
}

function customKdaMetric(label: string, metricName: string, applicationID: string, env: string) {
    return new cloudwatch.Metric({
        label: label,
        period: Duration.seconds(60),
        namespace: "AWS/KinesisAnalytics",
        metricName: metricName,
        dimensions: {
            "Application": applicationID,
            "Program": "mockingbird",
            "env": env,
        },
    });
}

export function uptime(title: string, applicationID: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            kdaMetric("downtime", "downtime", applicationID).with({ statistic: "max" }),
        ],
        right: [
            kdaMetric("fullRestarts", "fullRestarts", applicationID).with({ statistic: "max" }),
        ],
    });
}

export function resourceUsage(title: string, applicationID: string) {
    const memoryUsageMetric = kdaMetric("heapMemoryUtilization", "heapMemoryUtilization", applicationID).with({ statistic: "max" });
    return {
        widget: new cloudwatch.GraphWidget({
            title: title,
            left: [
                kdaMetric("cpuUtilization", "cpuUtilization", applicationID).with({ statistic: "max" }),
            ],
            right: [
                memoryUsageMetric,
            ],
        }),
        memoryUsageMetric,
    };
}

export function kpu(title: string, applicationID: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            kdaMetric("kpu", "kpu", applicationID).with({ statistic: "max" }),
        ],
    });
}

export function numRecords(title: string, applicationID: string) {
    const numRecordsInAvg = kdaMetric("numRecordsInPerSecond (avg)", "numRecordsInPerSecond", applicationID).with({ statistic: "avg" });
    const numRecordsOutAvg = kdaMetric("numRecordsOutPerSecond (avg)", "numRecordsOutPerSecond", applicationID).with({ statistic: "avg" });

    return {
        numRecordsInAvg,
        numRecordsOutAvg,
        widget: new cloudwatch.GraphWidget({
            title: title,
            left: [
                numRecordsInAvg,
                numRecordsOutAvg,
                kdaMetric("numRecordsInPerSecond (max)", "numRecordsInPerSecond", applicationID).with({ statistic: "max" }),
                kdaMetric("numRecordsOutPerSecond (max)", "numRecordsOutPerSecond", applicationID).with({ statistic: "max" }),
            ],
        }),
    };
}

export function checkpoints(title: string, applicationID: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            kdaMetric("numberOfFailedCheckpoints", "numberOfFailedCheckpoints", applicationID).with({ statistic: "max" }),
        ],
        right: [
            kdaMetric("lastCheckpointSize", "lastCheckpointSize", applicationID).with({ statistic: "max" }),

        ],
    });
}

export function timeDelay(title: string, applicationID: string, env: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            customKdaMetric("EventDelayWatermark", "EventDelayWatermark", applicationID, env).with({ statistic: "max" }),
            customKdaMetric("EventDelayNow", "EventDelayNow", applicationID, env).with({ statistic: "max" }),
        ],
    });
}

export function redisOperations(title: string, applicationID: string, env: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            customKdaMetric("HSET", "RedisHSET", applicationID, env).with({ statistic: "max" }),
            customKdaMetric("HDEL", "RedisHDEL", applicationID, env).with({ statistic: "max" }),
            customKdaMetric("DEL", "RedisDEL", applicationID, env).with({ statistic: "max" }),
        ],
        leftYAxis: {
            min: 0,
        },
    });
}

export function numRecordsInBelowAvgAlarm(scope: Construct, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "num-records-in-avg-alarm", {
        alarmName: "num-records-in-avg-low",
        alarmDescription: "Number of records to KDA is low, check if kinesis stream is working",
        comparisonOperator: cloudwatch.ComparisonOperator.LESS_THAN_THRESHOLD,
        threshold: 1000,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.BREACHING,
    });
}

export function iteratorAgeIncreasingAlarm(scope: Construct, iteratorName: string, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.hours(6),
    }).createAlarm(scope, `flink-${iteratorName}-iterator-age-increasing-alarm`, {
        alarmName: `flink-${iteratorName}-iterator-age-increasing`,
        alarmDescription: "Iterator Age of spade is increasing",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 500000,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.BREACHING,
    });
}

export function memoryUsageAlarm(scope: Construct, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "kda_memory_usage", {
        alarmName: `kda-memory-usage`,
        alarmDescription: "KDA Memory Usage above threshold",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 80,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
    });
}