import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import { Construct, Duration } from '@aws-cdk/core';
import { addLatenciesMicrosecondsToMsMetric, latencyMicrosecondsToMsMetric } from "./common";

function sagemakerEndpointMetric(label: string, metricName: string, endpointName: string) {
    return new cloudwatch.Metric({
        label: label,
        period: Duration.seconds(60),
        namespace: "/aws/sagemaker/Endpoints",
        metricName: metricName,
        dimensions: {
            'VariantName': 'main',
            'EndpointName': endpointName,
        }
    });
}

function sagemakerEndpointVariantMetric(label: string, metricName: string, endpointName: string) {
    return new cloudwatch.Metric({
        label: label,
        period: Duration.seconds(60),
        namespace: "AWS/SageMaker",
        metricName: metricName,
        dimensions: {
            'VariantName': 'main',
            'EndpointName': endpointName,
        }
    });
}

export function resourceUsage(title: string, endpointName: string) {
    const cpuUtilizationMetric = sagemakerEndpointMetric("CPUUtilization avg", "CPUUtilization", endpointName).with({ statistic: "p50" });
    const memoryUtilizationMetric = sagemakerEndpointMetric("MemoryUtilization avg", "MemoryUtilization", endpointName).with({ statistic: "p50" });
    const diskUtilizationMetric = sagemakerEndpointMetric("DiskUtilization avg", "DiskUtilization", endpointName).with({ statistic: "p50" });

    return {
        cpuUtilizationMetric,
        memoryUtilizationMetric,
        diskUtilizationMetric,
        widget: new cloudwatch.GraphWidget({
            title: title,
            left: [
                sagemakerEndpointMetric("CPUUtilization max", "CPUUtilization", endpointName).with({ statistic: "max" }),
                cpuUtilizationMetric,
            ],
            right: [
                sagemakerEndpointMetric("MemoryUtilization max", "MemoryUtilization", endpointName).with({ statistic: "max" }),
                memoryUtilizationMetric,
                sagemakerEndpointMetric("DiskUtilization max", "DiskUtilization", endpointName).with({ statistic: "max" }),
                diskUtilizationMetric,
            ],
        }),
    };
}

export function latency(title: string, endpointName: string) {
    const p999Latency = addLatenciesMicrosecondsToMsMetric('p99.9', [
        sagemakerEndpointVariantMetric("ModelLatency p99.9", "ModelLatency", endpointName).with({ statistic: "p99.9" }),
        sagemakerEndpointVariantMetric("OverheadLatency p99.9", "OverheadLatency", endpointName).with({ statistic: "p99.9" }),
    ], 2);

    return {
        p999Latency,
        widget: new cloudwatch.GraphWidget({
            title: title,
            left: [
                addLatenciesMicrosecondsToMsMetric('max', [
                    sagemakerEndpointVariantMetric("ModelLatency max", "ModelLatency", endpointName).with({ statistic: "max" }),
                    sagemakerEndpointVariantMetric("OverheadLatency max", "OverheadLatency", endpointName).with({ statistic: "max" }),
                ], 0),
                p999Latency,
                addLatenciesMicrosecondsToMsMetric('p99', [
                    sagemakerEndpointVariantMetric("ModelLatency p99", "ModelLatency", endpointName).with({ statistic: "p99" }),
                    sagemakerEndpointVariantMetric("OverheadLatency p99", "OverheadLatency", endpointName).with({ statistic: "p99" }),
                ], 4),
                addLatenciesMicrosecondsToMsMetric('p90', [
                    sagemakerEndpointVariantMetric("ModelLatency p90", "ModelLatency", endpointName).with({ statistic: "p90" }),
                    sagemakerEndpointVariantMetric("OverheadLatency p90", "OverheadLatency", endpointName).with({ statistic: "p90" }),
                ], 6),
                addLatenciesMicrosecondsToMsMetric('p50', [
                    sagemakerEndpointVariantMetric("ModelLatency p50", "ModelLatency", endpointName).with({ statistic: "p50" }),
                    sagemakerEndpointVariantMetric("OverheadLatency p50", "OverheadLatency", endpointName).with({ statistic: "p50" }),
                ], 8),
            ],
        }),
    };
}

export function latencyBreakdown(title: string, endpointName: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            latencyMicrosecondsToMsMetric([
                sagemakerEndpointVariantMetric("ModelLatency max", "ModelLatency", endpointName).with({ statistic: "max" }),
                sagemakerEndpointVariantMetric("ModelLatency p99.9", "ModelLatency", endpointName).with({ statistic: "p99.9" }),
                sagemakerEndpointVariantMetric("ModelLatency p99", "ModelLatency", endpointName).with({ statistic: "p99" }),
                sagemakerEndpointVariantMetric("ModelLatency p90", "ModelLatency", endpointName).with({ statistic: "p90" }),
                sagemakerEndpointVariantMetric("ModelLatency p50", "ModelLatency", endpointName).with({ statistic: "p50" }),
                sagemakerEndpointVariantMetric("OverheadLatency max", "OverheadLatency", endpointName).with({ statistic: "max" }),
                sagemakerEndpointVariantMetric("OverheadLatency p99.9", "OverheadLatency", endpointName).with({ statistic: "p99.9" }),
                sagemakerEndpointVariantMetric("OverheadLatency p99", "OverheadLatency", endpointName).with({ statistic: "p99" }),
                sagemakerEndpointVariantMetric("OverheadLatency p90", "OverheadLatency", endpointName).with({ statistic: "p90" }),
                sagemakerEndpointVariantMetric("OverheadLatency p50", "OverheadLatency", endpointName).with({ statistic: "p50" }),
            ]),
        ],
    });
}

export function invocations(title: string, endpointName: string) {
    return new cloudwatch.GraphWidget({
        title: title,
        left: [
            sagemakerEndpointVariantMetric("Invocations", "Invocations", endpointName).with({ statistic: "sum" }),
        ],
    });
}

export function errors(title: string, endpointName: string) {
    const metric5xx = sagemakerEndpointVariantMetric("Invocation5XXErrors", "Invocation5XXErrors", endpointName).with({ statistic: "sum" })
    return {
        metric5xx: metric5xx,
        widget: new cloudwatch.GraphWidget({
            title: title,
            left: [
                metric5xx,
                sagemakerEndpointVariantMetric("Invocation4XXErrors", "Invocation4XXErrors", endpointName).with({ statistic: "sum" }),
            ],
        })
    };
}

export function errorsAlarm(scope: Construct, endpointName: string, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "sagemaker_endpoint_errors", {
        alarmName: `${endpointName}-errors`,
        alarmDescription: "Endpoint Errors above threshold",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 5,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
    });
}

export function cpuAlarm(scope: Construct, endpointName: string, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "sagemaker_cpu_usage", {
        alarmName: `${endpointName}-cpu-usage`,
        alarmDescription: "Endpoint CPU usage above threshold",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 48 * 100 * 0.7, // cpu cores * 100 * 70% to alarm
        evaluationPeriods: 4,
        treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
    });
}

export function memoryAlarm(scope: Construct, endpointName: string, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "sagemaker_memory_usage", {
        alarmName: `${endpointName}-memory-usage`,
        alarmDescription: "Endpoint Memory Usage above threshold",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 80,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
    });
}

export function diskAlarm(scope: Construct, endpointName: string, metric: cloudwatch.Metric) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "sagemaker_disk_usage", {
        alarmName: `${endpointName}-disk-usage`,
        alarmDescription: "Endpoint Disk Usage above threshold",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 60,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
    });
}

export function latencyAlarm(scope: Construct, endpointName: string, metric: cloudwatch.MathExpression) {
    return metric.with({
        period: Duration.minutes(5),
    }).createAlarm(scope, "sagemaker_p999_latency", {
        alarmName: `${endpointName}-p999-latency`,
        alarmDescription: "Endpoint Latency above threshold",
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 250,
        evaluationPeriods: 2,
        treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
    });
}
