import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import { Construct, Duration } from '@aws-cdk/core';

function lambdaMetric(label: string, metricName: string, lambdaName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: Duration.seconds(60),
    namespace: "AWS/Lambda",
    metricName: metricName,
    dimensions: {
      "FunctionName": lambdaName,
    },
  });
}

function lambdaReaderMetric(label: string, metricName: string, env: string) {
  return new cloudwatch.Metric({
    label: label,
    period: Duration.seconds(60),
    namespace: "CommerceFlinkLambdaReader",
    metricName: metricName,
    dimensions: {
      "Env": env,
    },
  });
}

function lambdaReaderLogMetric(label: string, metricName: string, env: string) {
  return new cloudwatch.Metric({
    label: label,
    period: Duration.seconds(60),
    namespace: `CommerceFlinkLambdaReader-${env}`,
    metricName: metricName,
  });
}

export function invocations(lambdaName: string) {
  return new cloudwatch.GraphWidget({
    title: `Invocations`,
    left: [
      lambdaMetric("Invocations", "Invocations", lambdaName).with({ statistic: "sum" })
    ],
  });
}

export function duration(lambdaName: string) {
  return new cloudwatch.GraphWidget({
    title: `Duration`,
    left: [
      lambdaMetric("Duration Maximum", "Duration", lambdaName).with({ statistic: "max" }),
      lambdaMetric("Duration p99.9", "Duration", lambdaName).with({ statistic: "p99.9" }),
      lambdaMetric("Duration p99", "Duration", lambdaName).with({ statistic: "p99" }),
      lambdaMetric("Duration p90", "Duration", lambdaName).with({ statistic: "p90" }),
      lambdaMetric("Duration p50", "Duration", lambdaName).with({ statistic: "p50" }),
    ],
  });
}

export function errorCount(lambdaName: string) {
  const errorMetric = lambdaMetric("Errors", "Errors", lambdaName).with({ statistic: "sum" });

  return {
    widget: new cloudwatch.GraphWidget({
      title: `Error Count / Success Rate`,
      left: [new cloudwatch.MathExpression({
        label: "Success Rate",
        expression: "100 - 100 * errors / MAX([errors, invocations])",
        usingMetrics: {
          "invocations": lambdaMetric("Invocations", "Invocations", lambdaName).with({ statistic: "sum" }),
          "errors": errorMetric,
        },
        period: Duration.seconds(60),
      })]
    }),
    errorMetric,
  }
}

export function iteratorAge(lambdaName: string) {
  return new cloudwatch.GraphWidget({
    title: `IteratorAge`,
    left: [
      lambdaMetric("IteratorAge", "IteratorAge", lambdaName).with({ statistic: "max" })
    ],
  });
}

export function concurrentExecutions(lambdaName: string) {
  return new cloudwatch.GraphWidget({
    title: `ConcurrentExecutions`,
    left: [
      lambdaMetric("ConcurrentExecutions", "ConcurrentExecutions", lambdaName).with({ statistic: "max" })
    ],
  });
}

export function lambdaReaderFeatureStats(env: string) {
  return new cloudwatch.GraphWidget({
    title: `Lambda Reader Feature Stats`,
    left: [
      lambdaReaderMetric("Outdated Metrics Per User", "outdatedMetrics", env).with({ statistic: "avg" }),
      lambdaReaderMetric("Outdated Metrics Per User", "outdatedMetrics", env).with({ statistic: "max" }),
      lambdaReaderMetric("Number Metrics Per User", "numMetricsPerUser", env).with({ statistic: "max" }),
      lambdaReaderMetric("Number Metrics Per User", "numMetricsPerUser", env).with({ statistic: "avg" }),
    ],
    right: [
      lambdaReaderMetric("User Sum Counts", "sumCounts", env).with({ statistic: "max" }),
      lambdaReaderMetric("User Sum Counts", "sumCounts", env).with({ statistic: "avg" }),
    ],
  });
}

export function lambdaReaderLogStats(env: string) {
  return new cloudwatch.GraphWidget({
    title: `Lambda Reader Feature Stats`,
    left: [
      lambdaReaderLogMetric("Errors", "LoggedErrors", env).with({ statistic: "sum" }),
    ],
    right: [
      lambdaReaderLogMetric("Users no metrics", "UserNoMetrics", env).with({ statistic: "sum" }),
    ],
  });
}

export function errorsAlarm(scope: Construct, lambdaName: string, metric: cloudwatch.Metric) {
  return metric.with({
    period: Duration.minutes(5),
  }).createAlarm(scope, `${lambdaName}-errors`, {
    alarmName: `${lambdaName}-errors`,
    alarmDescription: "Endpoint Errors above threshold",
    comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
    threshold: 0,
    evaluationPeriods: 2,
    treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
  });
}