import {expressionWidget} from "./common";


export function cpuUsage() {
    return expressionWidget("Cluster - CPU Usage", `SEARCH('{Flink,InstanceId} MetricName="cpu_usage_user" InstanceId=i', 'Maximum', 300)`)
}

export function memoryUsage() {
    return expressionWidget("Cluster - Memory Usage", `SEARCH('{Flink,InstanceId} MetricName="mem_used_percent" InstanceId=i', 'Maximum', 300)`);
}

/*

expressionWidget("Cluster - Job Metrics", `SEARCH('{Mockingbird,"Job Name","Operator Name"} MetricName=PerSecond', 'Average', 300)`)


          new cloudwatch.GraphWidget({
            title: "Consumer Position",
            left: [new cloudwatch.Metric({
              namespace: "Mockingbird",
              metricName: "taskmanager.KinesisConsumer.stream.shardId.millisBehindLatest",
              dimensions: {
                "Job Name": "Mockingbird",
                "Operator Name": "Source: spade-downstream-prod-live-commerce-mw-staging",
              }
            })],
            liveData: true,
          }),

titleWidget("Flink Memory"),
          new cloudwatch.GraphWidget({
            title: "Heap.Used",
            left: [
              new cloudwatch.MathExpression({
                label: 'Heap.Used',
                expression: "SUM(heapused)",
                period: Duration.seconds(300),
                usingMetrics: {
                  "heapused": new cloudwatch.MathExpression({
                    expression: `SEARCH('{Mockingbird,Host} MetricName=JVM.Memory.Heap.Used', 'Maximum', 60)`,
                    period: Duration.seconds(300),
                    usingMetrics: {},
                  }),
                },
              }),
            ],
            liveData: true,
          }),
          new cloudwatch.GraphWidget({
            title: "NonHeap.Committed",
            left: [
              new cloudwatch.MathExpression({
                label: 'NonHeap.Committed',
                expression: "SUM(nonheapcommitted)",
                period: Duration.seconds(300),
                usingMetrics: {
                  "nonheapcommitted": new cloudwatch.MathExpression({
                    expression: `SEARCH('{Mockingbird,Host} MetricName=JVM.Memory.NonHeap.Committed', 'Maximum', 60)`,
                    period: Duration.seconds(300),
                    usingMetrics: {},
                  }),
                },
              }),
            ],
            liveData: true,
          }),
          new cloudwatch.GraphWidget({
            title: "Heap.Committed",
            left: [
              new cloudwatch.MathExpression({
                label: "Heap.Committed",
                expression: "SUM(heapcommitted)",
                period: Duration.seconds(300),
                usingMetrics: {
                  "heapcommitted": new cloudwatch.MathExpression({
                    expression: `SEARCH('{Mockingbird,Host} MetricName=JVM.Memory.Heap.Committed', 'Maximum', 60)`,
                    period: Duration.seconds(300),
                    usingMetrics: {},
                  }),
                },
              }),
            ]
          }),
          new cloudwatch.GraphWidget({
            title: "Direct.MemoryUsed",
            left: [
              new cloudwatch.MathExpression({
                label: "Direct.MemoryUsed",
                expression: "SUM(directmemoryused)",
                period: Duration.seconds(300),
                usingMetrics: {
                  "directmemoryused": new cloudwatch.MathExpression({
                    expression: `SEARCH('{Mockingbird,Host} MetricName=JVM.Memory.Direct.MemoryUsed', 'Maximum', 60)`,
                    usingMetrics: {},
                    period: Duration.seconds(300),
                  }),
                },
              }),
            ]
          }),
          new cloudwatch.GraphWidget({
            title: "Mapped.MemoryUsed",
            left: [
              new cloudwatch.MathExpression({
                label: "Mapped.MemoryUsed",
                expression: "SUM(mappedmemoryused)",
                period: Duration.seconds(300),
                usingMetrics: {
                  "mappedmemoryused": new cloudwatch.MathExpression({
                    expression: `SEARCH('{Mockingbird,Host} MetricName=JVM.Memory.Mapped.MemoryUsed', 'Maximum', 60)`,
                    usingMetrics: {},
                    period: Duration.seconds(300),
                  }),
                },
              })
            ]
          })
 */