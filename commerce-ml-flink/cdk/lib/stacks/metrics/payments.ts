import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as cdk from '@aws-cdk/core';
import { percentMetricByErrorSuccess } from "./common";

const TwitchRevenueProd = "316550374861";

function everdeenMetric(label: string, metricName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "everdeen",
    metricName: metricName,
    account: TwitchRevenueProd,
    region: "us-west-2",
    dimensions: {
      Region: "us-west-2",
      Service: "everdeen",
      Stage: 'production',
    },
  });
}

function everdeenOperationMetric(label: string, operationName: string, metricName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "everdeen",
    metricName: metricName,
    account: TwitchRevenueProd,
    region: "us-west-2",
    dimensions: {
      Region: "us-west-2",
      Service: "everdeen",
      Stage: 'production',
      Operation: operationName,
    },
  });
}

function paymentsServiceMetric(label: string, metricName: string) {
  const prodASG = "awseb-e-aqzvuzqtxn-stack-AWSEBAutoScalingGroup-353HAI7BBZI0";

  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "CWAgent",
    metricName: metricName,
    account: TwitchRevenueProd,
    region: "us-west-2",
    dimensions: {
      AutoScalingGroupName: prodASG,
    },
  });
}

export function blockedPurchases() {
  const successMetric = everdeenOperationMetric(`PurchaseOffer success`, 'PurchaseOffer', `Success`).with({ statistic: "sum" });
  const errorMetric = everdeenMetric("LegacyFraudCheck.Block", "LegacyFraudCheck.Block").with({ statistic: "sum" });
  return new cloudwatch.GraphWidget({
    title: "Everdeen Blocked Purchases",
    left: [
      errorMetric,
    ], right: [
      percentMetricByErrorSuccess('Block Rate', successMetric, errorMetric),
    ],
    rightYAxis: {
      max: 20,
    },
  })
}

export function paymentsInvocations(title: string, operationName: string) {
  const successMetric = paymentsServiceMetric(`${operationName} success`, `payments-service_${operationName}_2XX`).with({ statistic: "sum" })
  const serverErrorMetric = paymentsServiceMetric(`${operationName} server error`, `payments-service_${operationName}_5XX`).with({ statistic: "sum" });
  const clientErrorMetric = paymentsServiceMetric(`${operationName} client error`, `payments-service_${operationName}_4XX`).with({ statistic: "sum" });
  return new cloudwatch.GraphWidget({
    title: title,
    left: [
      successMetric,
      serverErrorMetric,
      clientErrorMetric,
    ],
  });
}

export function everdeenInvocations(title: string, operationName: string) {
  const successMetric = everdeenOperationMetric(`${operationName} success`, operationName, `Success`).with({ statistic: "sum" })
  const serverErrorMetric = everdeenOperationMetric(`${operationName} server error`, operationName, `ServerError`).with({ statistic: "sum" });
  const clientErrorMetric = everdeenOperationMetric(`${operationName} client error`, operationName, `ClientError`).with({ statistic: "sum" });
  return new cloudwatch.GraphWidget({
    title: title,
    left: [
      successMetric,
      serverErrorMetric,
      clientErrorMetric,
    ],
  });
}

export function latency(operationName: string) {
  const metricName = `payments-service_${operationName}_total_duration`;
  return new cloudwatch.GraphWidget({
    title: 'FraudController - CheckoutActions Latency',
    left: [
      paymentsServiceMetric("max", metricName).with({ statistic: "max" }),
      paymentsServiceMetric("p99.9", metricName).with({ statistic: "p99.9" }),
      paymentsServiceMetric("p99", metricName).with({ statistic: "p99" }),
      paymentsServiceMetric("p90", metricName).with({ statistic: "p90" }),
      paymentsServiceMetric("p50", metricName).with({ statistic: "p50" }),
    ],
  });
}

export function fraudRuleTiming(fraudRules: string[], metricName: string) {
  const ruleMetrics = fraudRules.map((name: string) => paymentsServiceMetric(`${name} ${metricName}`, `payments-service_fraud_context_${name}`).with({ statistic: metricName, period: cdk.Duration.minutes(5) }));
  return new cloudwatch.GraphWidget({
    title: `Fraud Rule ${metricName} Timing (${metricName})`,
    left: ruleMetrics,
  });
}

export function fraudFeatureTiming(fraudFeatures: string[], metricName: string) {
  const metrics = fraudFeatures.map((name: string) => paymentsServiceMetric(`${name} ${metricName}`, `payments-service_fraud_feature_${name}`).with({ statistic: metricName, period: cdk.Duration.minutes(5) }));
  return new cloudwatch.GraphWidget({
    title: `Fraud Feature ${metricName} Timing (${metricName})`,
    left: metrics,
  });
}

export function fraudDataTiming(fraudData: string[], metricName: string) {
  const metrics = fraudData.map((name: string) => paymentsServiceMetric(`${name} ${metricName}`, `payments-service_fraud_data_${name}`).with({ statistic: metricName, period: cdk.Duration.minutes(5) }));
  return new cloudwatch.GraphWidget({
    title: `Fraud Data ${metricName} Timing (${metricName})`,
    left: metrics,
  });
}

export function cacheHitRate() {
  const successMetric = paymentsServiceMetric(`Cache Hit`, "payments-service_payments_personalization_cache_hit").with({ statistic: "sum" });
  const errorMetric = paymentsServiceMetric(`Cache Miss`, "payments-service_payments_personalization_cache_miss").with({ statistic: "sum" });

  return new cloudwatch.GraphWidget({
    title: `Personalization Cache`,
    left: [
      successMetric,
      errorMetric,
    ], right: [
      percentMetricByErrorSuccess('Hit Rate', successMetric, errorMetric),
    ],
    rightYAxis: {
      max: 20,
    },
  });
}