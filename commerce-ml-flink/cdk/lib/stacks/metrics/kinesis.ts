import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as cdk from '@aws-cdk/core';

function kinesisMetric(label: string, metricName: string, streamName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "AWS/Kinesis",
    metricName: metricName,
    dimensions: {
      "StreamName": streamName,
    },
  });
}

function kinesisProducerMetric(label: string, metricName: string, streamName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: cdk.Duration.seconds(60),
    namespace: "KinesisProducerLibrary",
    metricName: metricName,
    dimensions: {
      "StreamName": streamName,
    },
  });
}

export function consumptionMetrics(title: string, streamName: string) {
  return new cloudwatch.GraphWidget({
    title: title,
    left: [
      kinesisMetric("records inserted", "PutRecords.Records", streamName).with({ statistic: "sum" }),
      kinesisMetric("records read", "GetRecords.Records", streamName).with({ statistic: "sum" }),
    ],
    right: [
      kinesisMetric("bytes inserted", "PutRecords.Bytes", streamName).with({ statistic: "sum" }),
      kinesisMetric("bytes read", "GetRecords.Bytes", streamName).with({ statistic: "sum" }),
    ],
  });
}

export function errorMetrics(title: string, streamName: string) {
  return new cloudwatch.GraphWidget({
    title: title,
    left: [
      kinesisMetric("ReadProvisionedThroughputExceeded", "ReadProvisionedThroughputExceeded", streamName).with({ statistic: "sum" }),
      kinesisMetric("WriteProvisionedThroughputExceeded", "WriteProvisionedThroughputExceeded", streamName).with({ statistic: "sum" }),
      kinesisMetric("ThrottledRecords", "PutRecords.ThrottledRecords", streamName).with({ statistic: "sum" }),
      kinesisProducerMetric("UserRecordExpired", "UserRecordExpired", streamName).with({ statistic: "sum" }),
    ],
  });
}

export function iteratorAgeMetrics(title: string, streamName: string) {
  const metric = kinesisMetric("IteratorAgeMilliseconds", "GetRecords.IteratorAgeMilliseconds", streamName).with({ statistic: "max" })
  return {
    metric,
    widget: new cloudwatch.GraphWidget({
      title: title,
      left: [
        metric,
      ],
    }),
  };
}