import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import { Construct, Duration } from '@aws-cdk/core';

function redisMetric(label: string, metricName: string) {
  return new cloudwatch.Metric({
    label: label,
    period: Duration.seconds(60),
    namespace: "AWS/ElastiCache",
    metricName: metricName,
  });
}

export function items() {
  return new cloudwatch.GraphWidget({
    title: "Items",
    left: [
      redisMetric("CurrItems", "CurrItems").with({ statistic: "max" }),
    ],
  })
}

export function memoryUsage() {
  const memoryUsageMetric = redisMetric("DatabaseMemoryUsagePercentage", "DatabaseMemoryUsagePercentage").with({ statistic: "max" });
  return {
    widget: new cloudwatch.GraphWidget({
      title: "Memory Usage",
      left: [
        redisMetric("BytesUsedForCache", "BytesUsedForCache").with({ statistic: "max" })
      ],
      right: [
        memoryUsageMetric,
      ]
    }),
    memoryUsageMetric,
  };
}

export function cpuNetwork() {
  return new cloudwatch.GraphWidget({
    title: "CPU / Network",
    left: [
      redisMetric("CPUUtilization", "CPUUtilization").with({ statistic: "max" }),
      redisMetric("EngineCPUUtilization", "EngineCPUUtilization").with({ statistic: "max" }),
    ],
    right: [
      redisMetric("NetworkBytesIn", "NetworkBytesIn").with({ statistic: "sum" }),
      redisMetric("NetworkBytesOut", "NetworkBytesOut").with({ statistic: "sum" }),
    ]
  })
}

export function replication() {
  return new cloudwatch.GraphWidget({
    title: "Replication",
    left: [
      redisMetric("ReplicationBytes", "ReplicationBytes").with({ statistic: "sum" }),
    ],
    right: [
      redisMetric("ReplicationLag", "ReplicationLag").with({ statistic: "max" }),
    ]
  })
}

export function errors() {
  return new cloudwatch.GraphWidget({
    title: "Errors",
    left: [
      redisMetric("NetworkBandwidthInAllowanceExceeded", "NetworkBandwidthInAllowanceExceeded").with({ statistic: "sum" }),
      redisMetric("NetworkBandwidthOutAllowanceExceeded", "NetworkBandwidthOutAllowanceExceeded").with({ statistic: "sum" }),
      redisMetric("NetworkConntrackAllowanceExceeded", "NetworkConntrackAllowanceExceeded").with({ statistic: "sum" }),
      redisMetric("NetworkLinkLocalAllowanceExceeded", "NetworkLinkLocalAllowanceExceeded").with({ statistic: "sum" }),
      redisMetric("NetworkPacketsPerSecondAllowanceExceeded", "NetworkPacketsPerSecondAllowanceExceeded").with({ statistic: "sum" }),
    ],
  })
}

export function connections() {
  return new cloudwatch.GraphWidget({
    title: "Connections",
    left: [
      redisMetric("CurrConnections", "CurrConnections").with({ statistic: "sum" })
    ],
  })
}

export function memoryUsageAlarm(scope: Construct, metric: cloudwatch.Metric) {
  return metric.with({
    period: Duration.minutes(5),
  }).createAlarm(scope, "redis_memory_usage", {
    alarmName: `redis-memory-usage`,
    alarmDescription: "Redis Memory Usage above threshold",
    comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
    threshold: 70,
    evaluationPeriods: 2,
    treatMissingData: cloudwatch.TreatMissingData.NOT_BREACHING,
  });
}