import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import { Duration } from '@aws-cdk/core';

export function expressionMetric(exp: string) {
  return new cloudwatch.MathExpression({
    expression: exp,
    usingMetrics: {},
    label: " ",
  });
}

export function addLatenciesMicrosecondsToMsMetric(label: string, metrics: cloudwatch.Metric[], startingIndex: number): cloudwatch.MathExpression {
  let usingMetrics: { [key: string]: cloudwatch.Metric } = {}
  for (let i = 0; i < metrics.length; ++i) {
    usingMetrics[`m${startingIndex + i}`] = metrics[i];
  }

  return new cloudwatch.MathExpression({
    expression: `m${startingIndex}/1000 + m${startingIndex + 1}/1000`,
    usingMetrics: usingMetrics,
    label: label,
    period: Duration.minutes(1),
  });
}

export function latencyMicrosecondsToMsMetric(metrics: cloudwatch.Metric[]): cloudwatch.IMetric {
  let usingMetrics: { [key: string]: cloudwatch.Metric } = {}
  for (let i = 0; i < metrics.length; ++i) {
    usingMetrics[`m${i}`] = metrics[i];
  }

  return new cloudwatch.MathExpression({
    expression: "METRICS()/1000",
    usingMetrics: usingMetrics,
    label: " ",
    period: Duration.minutes(1),
  });
}

export function latencySecondsToMsMetric(metrics: cloudwatch.IMetric[]): cloudwatch.IMetric {
  let usingMetrics: { [key: string]: cloudwatch.IMetric } = {}
  for (let i = 0; i < metrics.length; ++i) {
    usingMetrics[`m${i}`] = metrics[i];
  }

  return new cloudwatch.MathExpression({
    expression: "METRICS()*1000",
    usingMetrics: usingMetrics,
    label: " ",
    period: Duration.minutes(1),
  });
}

export function percentMetricByErrorSuccess(label: string, successMetric: cloudwatch.Metric, errorMetric: cloudwatch.Metric): cloudwatch.IMetric {
  let usingMetrics = {
    "s": successMetric,
    "e": errorMetric,
  }

  return new cloudwatch.MathExpression({
    expression: "(e/(s+e))*100",
    usingMetrics: usingMetrics,
    label: label,
    period: Duration.minutes(1),
  });
}

export function percentMetricByClientServerErrorSuccess(label: string, successMetric: cloudwatch.Metric, clientErrorMetric: cloudwatch.Metric, serverErrorMetric: cloudwatch.Metric): cloudwatch.IMetric {
  let usingMetrics = {
    "s": successMetric,
    "ec": clientErrorMetric,
    "es": serverErrorMetric,
  }

  return new cloudwatch.MathExpression({
    expression: "((ec+es)/(s+ec+es))*100",
    usingMetrics: usingMetrics,
    label: label,
    period: Duration.minutes(1),
  });
}

export function titleWidget(title: string) {
  return new cloudwatch.TextWidget({
    markdown: `## ${title}`,
    width: 24,
    height: 1,
  });
}

export function markdownWidget(body: string) {
  return new cloudwatch.TextWidget({
    markdown: body,
    width: 24,
  });
}

export function expressionWidget(title: string, exp: string) {
  return new cloudwatch.GraphWidget({
    title: title,
    left: [expressionMetric(exp)],
  });
}

export function alarmWidget(title: string, alarm: cloudwatch.IAlarm) {
  return new cloudwatch.AlarmWidget({
    title,
    alarm,
  })
}