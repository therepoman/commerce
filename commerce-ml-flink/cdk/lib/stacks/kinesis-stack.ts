import { Stack, Construct, Duration } from '@aws-cdk/core';
import * as sns from "@aws-cdk/aws-sns";
import {AwsAccountConfig} from "../config";
import {getVpc, getSg} from "../shared";
import {KinesisStream} from "@twitch-cdk/spade-consumer";
import * as kinesis from '@aws-cdk/aws-kinesis';

export interface KinesisStackProps {
  account: AwsAccountConfig,
  alarmTopic: string,
  env: string,
}


export class KinesisStack extends Stack {
  constructor(scope: Construct, id: string, props: KinesisStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });

    const stack = this;
    new KinesisStream(stack, "ml-kinesis-stream", {
      streamName: `spade-downstream-prod-live-commerce-mw-${props.env}`,
      additionalAlarmTopics: [sns.Topic.fromTopicArn(stack, "alarm-topic", props.alarmTopic)],
      alarmsEnabled: props.env == 'prod',
      shardCount: 24,
    });
    new kinesis.Stream(stack, "redis-stream", {
      streamName: `feature-store-redis-stream-${props.env}`,
      shardCount: 40,
      retentionPeriod: Duration.hours(48),
    });
  }
}
