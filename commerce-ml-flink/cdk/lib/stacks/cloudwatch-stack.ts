import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as cwa from "@aws-cdk/aws-cloudwatch-actions";
import * as sns from "@aws-cdk/aws-sns";
import { Construct, Stack } from '@aws-cdk/core';
import { AwsAccountConfig } from "../config";
import { alarmWidget, markdownWidget, titleWidget } from "./metrics/common";
import * as kdaMetrics from "./metrics/kda";
import * as kinesisMetrics from "./metrics/kinesis";
import * as lambdaMetrics from "./metrics/lambda";
import * as paymentsMetrics from "./metrics/payments";
import * as personalizationMetrics from "./metrics/personalization";
import * as redisMetrics from "./metrics/redis";
import * as sagemakerMetrics from "./metrics/sagemaker";

export interface KinesisStackProps {
  account: AwsAccountConfig,
  env: string,
  alarmTopicArn: string,
  slackTopicArn: string,
}

export class CloudwatchStack extends Stack {
  constructor(scope: Construct, id: string, props: KinesisStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });
    const stack = this;
    const widgets: cloudwatch.IWidget[][] = [];
    const kdaApplicationID = props.env == 'prod' ? "mockingbird790E27E_giaU2gjyXYCv" : "mockingbird790E27E_fhn1bZKsqepS";
    const sagemakerEndpointName = `nominis-master-${props.env}-purrep-realtime`;
    const lambdaReaderName = `redis-lambda-reader-${props.env}`;
    const lambdaWriterName = `redis-lambda-writer-${props.env}`;

    // alarm metrics
    const numRecords = kdaMetrics.numRecords("Records", kdaApplicationID);
    const kdaResourceUsage = kdaMetrics.resourceUsage("CPU/Memory", kdaApplicationID);
    const spadeIteratorAgeMetrics = kinesisMetrics.iteratorAgeMetrics("Spade Kinesis - Iterator Age", `spade-downstream-prod-live-commerce-mw-${props.env}`);
    const sagemakerErrors = sagemakerMetrics.errors("Errors", sagemakerEndpointName);
    const sagemakerResourceUsage = sagemakerMetrics.resourceUsage("Resource Usage", sagemakerEndpointName);
    const sagemakerLatency = sagemakerMetrics.latency("Latency", sagemakerEndpointName);
    const lambdaReaderErrors = lambdaMetrics.errorCount(lambdaReaderName);
    const lambdaWriterErrors = lambdaMetrics.errorCount(lambdaWriterName);
    const redisMemoryUsage = redisMetrics.memoryUsage();

    // alarms
    if (props.env == "prod") {
      const spadeTtlIncreasingAlarm = kdaMetrics.iteratorAgeIncreasingAlarm(stack, 'spade', spadeIteratorAgeMetrics.metric);
      const sagemakerErrorsAlarm = sagemakerMetrics.errorsAlarm(stack, sagemakerEndpointName, sagemakerErrors.metric5xx);
      const sagemakerCpuUsageAlarm = sagemakerMetrics.cpuAlarm(stack, sagemakerEndpointName, sagemakerResourceUsage.cpuUtilizationMetric);
      const sagemakerMemoryUsageAlarm = sagemakerMetrics.memoryAlarm(stack, sagemakerEndpointName, sagemakerResourceUsage.memoryUtilizationMetric);
      const sagemakerDiskUsageAlarm = sagemakerMetrics.diskAlarm(stack, sagemakerEndpointName, sagemakerResourceUsage.diskUtilizationMetric);
      const lambdaReaderErrorsAlarm = lambdaMetrics.errorsAlarm(stack, lambdaReaderName, lambdaReaderErrors.errorMetric);
      // const lambdaWriterErrorsAlarm = lambdaMetrics.errorsAlarm(stack, lambdaWriterName, lambdaWriterErrors.errorMetric);
      //const sagemakerLatencyAlarm = sagemakerMetrics.latencyAlarm(stack, sagemakerEndpointName, sagemakerLatency.p999Latency);
      const kdaMemoryUsageAlarm = kdaMetrics.memoryUsageAlarm(stack, kdaResourceUsage.memoryUsageMetric);
      const redisMemoryUsageAlarm = redisMetrics.memoryUsageAlarm(stack, redisMemoryUsage.memoryUsageMetric);

      const alarms: { [index: string]: cloudwatch.Alarm; } = {
        "Spade Ttl Increasing": spadeTtlIncreasingAlarm,
        "Sagemaker Endpoint Errors": sagemakerErrorsAlarm,
        // "Sagemaker CPU Usage": sagemakerCpuUsageAlarm,
        "Sagemaker Memory Usage": sagemakerMemoryUsageAlarm,
        "Sagemaker Disk Usage": sagemakerDiskUsageAlarm,
        "Lambda Reader Errors": lambdaReaderErrorsAlarm,
        // "Lambda Writer Errors": lambdaWriterErrorsAlarm,
        "KDA Memory Usage": kdaMemoryUsageAlarm,
        "Redis Memory Usage": redisMemoryUsageAlarm,
        //"Sagemaker P99.9 Latency": sagemakerLatencyAlarm,
      };
      const slackTopic = sns.Topic.fromTopicArn(stack, "slack-topic", props.slackTopicArn);
      const alarmTopic = sns.Topic.fromTopicArn(stack, "alarm-topic", props.alarmTopicArn);
      const alarmWidgets: cloudwatch.IWidget[] = [
        titleWidget("Alarms"),
      ];
      for (const title in alarms) {
        alarms[title].addAlarmAction(new cwa.SnsAction(alarmTopic));
        alarms[title].addAlarmAction(new cwa.SnsAction(slackTopic));

        alarmWidgets.push(alarmWidget(title, alarms[title]));
      }
      widgets.push(alarmWidgets);
    }

    widgets.push([
      titleWidget("Sagemaker Endpoints"),
      markdownWidget(`
- [Endpoint](https://us-west-2.console.aws.amazon.com/sagemaker/home?region=us-west-2#/endpoints/nominis-master-${props.env}-purrep-realtime)
- [Log File](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#logsV2:log-groups/log-group/$252Faws$252Fsagemaker$252FEndpoints$252Fnominis-master-${props.env}-purrep-realtime)
      `.trim()),
      sagemakerResourceUsage.widget,
      sagemakerMetrics.invocations("Invocations", sagemakerEndpointName),
      sagemakerErrors.widget,
      sagemakerLatency.widget,
      sagemakerMetrics.latencyBreakdown("Latency Breakdown", sagemakerEndpointName),
    ]);

    widgets.push([
      titleWidget("Kinesis"),
      markdownWidget(`
- [Reader](https://us-west-2.console.aws.amazon.com/kinesis/home?region=us-west-2#/streams/details/spade-downstream-prod-live-commerce-mw-${props.env}/details)
- [Writer](https://us-west-2.console.aws.amazon.com/kinesis/home?region=us-west-2#/streams/details/feature-store-redis-stream-${props.env}/details)
      `.trim()),
      kinesisMetrics.consumptionMetrics("Spade Kinesis - Consumption", `spade-downstream-prod-live-commerce-mw-${props.env}`),
      kinesisMetrics.errorMetrics("Spade Kinesis - Error Metrics", `spade-downstream-prod-live-commerce-mw-${props.env}`),
      spadeIteratorAgeMetrics.widget,
    ]);

    widgets.push([
      titleWidget("KDA Cluster"),
      markdownWidget(`
- [Console](https://us-west-2.console.aws.amazon.com/kinesisanalytics/home?region=us-west-2#/details?applicationName=${kdaApplicationID})
- [Log File](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#logEventViewer:group=commerce-flink-kda-prod-mockingbirdLogGroup5699A881-cOLe4VJEOD8Y;stream=commerce-flink-kda-prod-mockingbirdLogStreamBF39E326-140L6UQCZ6YVQ)
    `.trim()),
      kdaMetrics.uptime("Uptime", kdaApplicationID),
      kdaResourceUsage.widget,
      numRecords.widget,
      kdaMetrics.kpu("KPU", kdaApplicationID),
      kdaMetrics.checkpoints("Checkpoints", kdaApplicationID),
      kdaMetrics.timeDelay("Time Delay", kdaApplicationID, props.env),
      kdaMetrics.redisOperations("Redis Operations", kdaApplicationID, props.env)
    ]);

    // widgets.push([
    //   titleWidget("Redis Lambda Writer"),
    //   lambdaMetrics.invocations(lambdaWriterName),
    //   lambdaMetrics.duration(lambdaWriterName),
    //   lambdaWriterErrors.widget,
    //   lambdaMetrics.iteratorAge(lambdaWriterName),
    //   lambdaMetrics.concurrentExecutions(lambdaWriterName),
    // ]);

    widgets.push([
      titleWidget("Redis Lambda Reader"),
      lambdaMetrics.invocations(lambdaReaderName),
      lambdaMetrics.duration(lambdaReaderName),
      lambdaReaderErrors.widget,
      lambdaMetrics.concurrentExecutions(lambdaReaderName),
      lambdaMetrics.lambdaReaderFeatureStats(props.env),
      lambdaMetrics.lambdaReaderLogStats(props.env),
    ]);

    widgets.push([
      titleWidget("Redis"),
      redisMetrics.items(),
      redisMetrics.cpuNetwork(),
      redisMetrics.replication(),
      redisMetrics.errors(),
      redisMemoryUsage.widget,
      redisMetrics.connections(),
    ]);

    widgets.push([
      titleWidget("Personalization Service"),
      markdownWidget(`
- [Admin Access](https://isengard.amazon.com/federate?account=426339712469&role=Admin)
      `.trim()),
      personalizationMetrics.loggedErrors(),
      personalizationMetrics.invocations("GetPurchaseReputation"),
      personalizationMetrics.latency("GetPurchaseReputation"),
      personalizationMetrics.dependencyLatency("Dependency: Sagemaker Latency", "Sagemaker:nominis-master-prod-purrep-realtime", true),
      personalizationMetrics.dependencyErrors("Dependency: Sagemaker Error Rate", "Sagemaker:nominis-master-prod-purrep-realtime"),
      personalizationMetrics.dependencyLatency("Depdendency: Lambda Latency", "Lambda:redis-lambda-reader-prod:Prod", true),
      personalizationMetrics.dependencyErrors("Depdendency: Lambda Error Rate", "Lambda:redis-lambda-reader-prod:Prod"),
    ]);

    const fraudRules = [
      'user_exceeded_weekly_purchase_velocity_rule?',
      'user_is_whitelisted_rule?',
      'user_exceeded_direct_debit_purchase_velocity_rule?',
      'user_exceeded_velocity_limit_multiple_times_rule?',
      'user_purchasing_on_new_channel_rule?',
      'user_purchasing_from_new_country_rule?',
      'purchase_exceeds_new_credit_card_allowance_rule?',
      'user_purchase_rep_high_risk_rule?',
      'user_purchase_rep_skip_velocity_check_rule?',
      'user_has_verified_spm_rule?',
      'user_exceeded_daily_gift_purchase_rule?',
    ];

    const fraudFeatures = [
      'different_purchase_country',
      'new_channel',
      'user_in_allowlist',
      'purchase_velocity_usd_weekly',
      'payment_method_is_verified',
      'is_direct_debit',
      'has_nami_balance',
    ];

    const fraudData = [
      'pepper_get_user_in_allowlist',
      'has_wallet_credits',
      'exchange_to',
      'get_fraud_rule_behavior_for_user',
      'nami_get_balance',
      'direct_debit_transaction_count',
      'has_prior_order',
      'last_purchase_country',
      'purchase_velocity',
      'find_user_two_fa',
      'get_payout_type',
      'check_two_fa_enabled_spm',
      'get_payment_method_manager_recurly',
      'purchase_new_channel',
    ];

    widgets.push([
      titleWidget("Payments Service"),
      markdownWidget(`
- [LowRisk Access](https://isengard.amazon.com/federate?account=316550374861&role=LowRiskRole)
      `.trim()),
      paymentsMetrics.paymentsInvocations("GetCheckoutActions Invocations", "FraudController_checkout_actions"),
      paymentsMetrics.everdeenInvocations("XsollaConfiguration Invocations", "XsollaConfiguration"),
      paymentsMetrics.everdeenInvocations("PurchaseOffer Invocations", "PurchaseOffer"),
      paymentsMetrics.blockedPurchases(),
      paymentsMetrics.cacheHitRate(),
      paymentsMetrics.latency("FraudController_checkout_actions"),
      paymentsMetrics.fraudRuleTiming(fraudRules, 'p99.9'),
      paymentsMetrics.fraudFeatureTiming(fraudFeatures, 'p99.9'),
      paymentsMetrics.fraudDataTiming(fraudData, 'p99.9'),
    ]);

    new cloudwatch.Dashboard(stack, `flink-dashboard-${props.env}`, {
      dashboardName: `flink-${props.env}`,
      widgets,
    });
  }
}
