import {AwsAccountConfig} from "../config";
import {Construct, Stack} from "@aws-cdk/core";
import * as sns from "@aws-cdk/aws-sns";
import * as s3 from "@aws-cdk/aws-s3";
import * as sns_subscriptions from "@aws-cdk/aws-sns-subscriptions";

export interface FlinkStackProps {
  account: AwsAccountConfig
  env: string
}

export class FlinkBaseStack extends Stack {
  topic: sns.ITopic;
  constructor(scope: Construct, id: string, props: FlinkStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: true,
    });

    const stack = this;
    const appName = 'commerce-flink';
    const name = `${appName}-pagerduty`;
    this.topic = new sns.Topic(stack, name, {
      topicName: name,
    });
    this.topic.addSubscription(new sns_subscriptions.UrlSubscription(
        `https://events.pagerduty.com/integration/${props.account.pagerdutyKey}/enqueue`
    ));
  }
}