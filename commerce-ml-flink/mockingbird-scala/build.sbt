ThisBuild / resolvers ++= Seq(
  "Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/",
  Resolver.mavenLocal
)

name := "mockingbird"
version := "0.3-SNAPSHOT"
organization := "tv.twitch"

ThisBuild / scalaVersion := "2.12.7"
val flinkVersion = "1.11.3"
val log4JVersion = "2.12.1"

val flinkDependencies = Seq(
  "org.apache.flink" %% "flink-clients" % flinkVersion,
  "org.apache.flink" %% "flink-connector-kinesis" % flinkVersion,
  "redis.clients" % "jedis" % "3.6.0",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.12.1",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.12.1",
  "com.amazonaws" % "aws-java-sdk-cloudwatch" % "1.11.881",
  "com.amazonaws" % "aws-kinesisanalytics-runtime" % "1.2.0",
  "javax.xml.bind" % "jaxb-api" % "2.3.0",

  "org.apache.logging.log4j" % "log4j-api" % log4JVersion,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4JVersion % "runtime",
  "org.apache.logging.log4j" % "log4j-core" % log4JVersion % "runtime",

  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % "provided",
  "org.projectlombok" % "lombok" % "1.18.16" % "provided",

  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "org.apache.flink" %% "flink-runtime" % flinkVersion % Test classifier "tests",
  "org.apache.flink" %% "flink-test-utils" % flinkVersion % Test classifier "tests",
  "org.apache.flink" %% "flink-streaming-java" % flinkVersion % Test classifier "tests",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % Test classifier "tests",
)

lazy val root = (project in file(".")).
  settings(
    libraryDependencies ++= flinkDependencies
  )

assembly / mainClass := Some("mockingbird.StreamingJob")
assemblyMergeStrategy in assembly := {
  case "META-INF/io.netty.versions.properties" => MergeStrategy.first
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

javacOptions ++= Seq("-source", "11", "-target", "11")
compileOrder := CompileOrder.JavaThenScala
parallelExecution in ThisBuild := false

// make run command include the provided dependencies
Compile / run  := Defaults.runTask(Compile / fullClasspath,
  Compile / run / mainClass,
  Compile / run / runner
).evaluated

// stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
Compile / run / fork := true
Global / cancelable := true

// exclude Scala library from assembly
assembly / assemblyOption  := (assembly / assemblyOption).value.copy(includeScala = false)
