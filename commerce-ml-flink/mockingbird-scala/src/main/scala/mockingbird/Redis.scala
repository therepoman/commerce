package mockingbird

import mockingbird.config.Config
import org.apache.flink.configuration.Configuration
import org.apache.flink.metrics.{Meter, MeterView}
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import org.apache.flink.util.ExceptionUtils
import org.slf4j.LoggerFactory
import redis.clients.jedis.{HostAndPort, JedisCluster}


object RedisOps extends Enumeration {
  type RedisOp = Value

  val HSET, HDEL, DEL = Value
}

object Redis {
  val meterFrequencySeconds = 60
  case class RedisAction(
                          var command: RedisOps.RedisOp,
                          var key: String,
                          var value: String,
                          var fieldKey: String,
                        ) extends Ordered[RedisAction] {
    def this() {
      this(RedisOps.HSET, "", "", "")
    }
    def compare(that: RedisAction): Int = (this.command, this.key, this.value, this.fieldKey) compare
      (that.command, that.key, that.value, that.fieldKey)
  }




  class RedisSink(hostAndPort: HostAndPort) extends RichSinkFunction[RedisAction] {
    @transient private var hsetCounter: Meter = _
    @transient private var hdelCounter: Meter = _
    @transient private var delCounter: Meter = _
    @transient private var jedis: JedisCluster = _
    override def open(parameters: Configuration): Unit = {
      if (!Config.settings.localMode) {
        jedis = new JedisCluster(hostAndPort)
      }
      hsetCounter = getRuntimeContext
        .getMetricGroup
        .addGroup("kinesisanalytics")
        .addGroup("Program", "mockingbird")
        .addGroup("env", Config.settings.envName)
        .meter("RedisHSET", new MeterView(meterFrequencySeconds))

      hdelCounter = getRuntimeContext
        .getMetricGroup
        .addGroup("kinesisanalytics")
        .addGroup("Program", "mockingbird")
        .addGroup("env", Config.settings.envName)
        .meter("RedisHDEL", new MeterView(meterFrequencySeconds))

      delCounter = getRuntimeContext
        .getMetricGroup
        .addGroup("kinesisanalytics")
        .addGroup("Program", "mockingbird")
        .addGroup("env", Config.settings.envName)
        .meter("RedisDEL", new MeterView(meterFrequencySeconds))
    }

    override def invoke(action: RedisAction, context: SinkFunction.Context[_]): Unit = {
      if (Config.settings.localMode) {
        return
      }
      if (action.command == RedisOps.HSET) {
        jedis.hset(action.key, action.fieldKey, action.value)
        hsetCounter.markEvent()
      } else if (action.command == RedisOps.HDEL) {
        jedis.hdel(action.key, action.fieldKey)
        hdelCounter.markEvent()
      } else if (action.command == RedisOps.DEL) {
        jedis.del(action.key)
        delCounter.markEvent()
      }
    }

    override def close(): Unit = {
      super.close()
      if(jedis != null) jedis.close()
    }
  }

  def getFeatureUpdate(featureGroupName: String, featureGroupId: String, featureName: String, featureValue: Int): RedisAction = {
    RedisAction(RedisOps.HSET, s"${featureGroupName}-${Config.settings.envName}:${featureGroupId}", s"${featureValue},${Timing.currentTimestamp}", featureName)
  }


  def getRemoveFeature(featureGroupName: String, featureGroupId:String, featureName: String): RedisAction = {
    RedisAction(RedisOps.HDEL, s"${featureGroupName}-${Config.settings.envName}:${featureGroupId}", "", featureName)
  }

  def getRemoveFeatureGroup(featureGroupName: String, featureGroupId: String): RedisAction = {
    RedisAction(RedisOps.DEL, s"${featureGroupName}-${Config.settings.envName}:${featureGroupId}", "", "")
  }

  def getUserFeatureUpdate(userId: String, token: String, value: Int): RedisAction = {
    RedisAction(RedisOps.HSET, s"${Config.settings.redisPrefix}:${userId}", s"${value},${Timing.currentTimestamp}", token)
  }

  def getRemoveUserFeature(userId: String, token: String): RedisAction = {
    RedisAction(RedisOps.HDEL, s"${Config.settings.redisPrefix}:${userId}", "", token)
  }

  def getRemoveUser(userId: String): RedisAction = {
    RedisAction(RedisOps.DEL, s"${Config.settings.redisPrefix}:${userId}", "", "")
  }

  def redisSink(): RedisSink = {
    new RedisSink(new HostAndPort(Config.settings.redisHost, 6379))
  }
}
