package mockingbird.config

import com.amazonaws.services.kinesisanalytics.runtime.KinesisAnalyticsRuntime


object Config {
  case class Settings(
                       envName: String,
                       redisPrefix: String,
                       localMode: Boolean,
                       minuteWatchKinesisStreamName: String,
                       redisOutputStreamName: String,
                       readParallelism: Int,
                       redisHost: String,
                       enableUserMetrics: Boolean,
                       enableDeviceMetrics: Boolean,
                       enableIPMetrics: Boolean,
                       enableCheckoutActionMetrics: Boolean,
                       enablePaymentCountMetrics: Boolean
                     )

  private val localSetting: Settings = Settings(
    envName = "local",
    redisPrefix = "u3",
    localMode = true,
    minuteWatchKinesisStreamName = "spade-downstream-prod-live-commerce-mw-staging",
    redisOutputStreamName = "feature-store-redis-stream-staging",
    readParallelism = 10,
    redisHost = "localhost",
    enableUserMetrics = false,
    enableDeviceMetrics = false,
    enableIPMetrics = false,
    enableCheckoutActionMetrics = true,
    enablePaymentCountMetrics = true
  )

  private val stagingSetting: Settings = localSetting.copy(
    envName="staging",
    localMode = false,
    redisHost = "coe10cmfkugo34z5.c5itg2.clustercfg.usw2.cache.amazonaws.com",
    enableUserMetrics = true,
    enableDeviceMetrics = false,
    enableIPMetrics = false,
    enableCheckoutActionMetrics = true,
    enablePaymentCountMetrics = true
  )

  private val prodSettings: Settings = Settings(
    envName="prod",
    redisPrefix = "u2",
    localMode = false,
    minuteWatchKinesisStreamName = "spade-downstream-prod-live-commerce-mw-prod",
    redisOutputStreamName = "feature-store-redis-stream-prod",
    readParallelism = 20,
    redisHost = "coe10cmfkugo34z5.c5itg2.clustercfg.usw2.cache.amazonaws.com",
    enableUserMetrics = true,
    enableDeviceMetrics = false,
    enableIPMetrics = false,
    enableCheckoutActionMetrics = true,
    enablePaymentCountMetrics = true
  )

  var settings: Settings = localSetting

  private val props = KinesisAnalyticsRuntime.getApplicationProperties
  if (props.size() != 0) {
    // running inside KDA
    if (props.get("config").get("env") == "staging") {
      settings = stagingSetting
    } else {
      settings = prodSettings
    }
  } else {
    val env: String = System.getenv("ENV")
    if (env == "staging") {
      settings = stagingSetting
    }
  }
}
