package mockingbird.metrics

import mockingbird.{Spade, Timing}
import mockingbird.config.Config
import org.apache.flink.api.scala.metrics.ScalaGauge
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.util.Collector

class EventDelayNowGagueMapper extends ProcessFunction[Spade.EventRecord,Spade.EventRecord] {
  @transient private var valueToExpose = 0L

  override def open(parameters: Configuration): Unit = {
    getRuntimeContext.getMetricGroup
      .addGroup("kinesisanalytics")
      .addGroup("Program", "mockingbird")
      .addGroup("env", Config.settings.envName)
      .gauge[Long, ScalaGauge[Long]]("EventDelayNow", ScalaGauge[Long]( () => valueToExpose ))
  }

  override def processElement(value: Spade.EventRecord, ctx: ProcessFunction[Spade.EventRecord, Spade.EventRecord]#Context, out: Collector[Spade.EventRecord]): Unit = {
    valueToExpose = Timing.currentTimestampMs - value.fields.time
    out.collect(value)
  }
}