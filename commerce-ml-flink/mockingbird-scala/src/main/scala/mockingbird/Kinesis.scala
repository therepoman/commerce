package mockingbird

import com.fasterxml.jackson.annotation.JsonProperty
import JsonMapper.mapper
import mockingbird.config.Config
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kinesis.config.{AWSConfigConstants, ConsumerConfigConstants}
import org.apache.flink.streaming.connectors.kinesis.{FlinkKinesisConsumer, FlinkKinesisProducer}
import org.apache.flink.streaming.connectors.kinesis.config.ConsumerConfigConstants
import java.time.temporal.ChronoUnit

import java.sql.Timestamp
import java.time.Instant
import java.util.Properties

object Kinesis {

  import org.apache.flink.streaming.connectors.kinesis.KinesisPartitioner
  import java.util.UUID

  final class RandomKinesisPartitioner[T] extends KinesisPartitioner[T] {
    override def getPartitionId(element: T): String = UUID.randomUUID.toString
    override def equals(o: Any): Boolean = o.isInstanceOf[RandomKinesisPartitioner[_]]
    override def hashCode: Int = classOf[RandomKinesisPartitioner[_]].hashCode
  }

  val region = "us-west-2"

  case class KinesisRecord (
                             @JsonProperty("Data") data: String
                           )

  // TODO: refactor ti
  def kinesisConsumer[T: TypeInformation](streamName: String, env: StreamExecutionEnvironment): DataStream[Spade.EventRecord] = {
    val consumerConfig: Properties = new Properties()
    consumerConfig.put(AWSConfigConstants.AWS_REGION, region)

    consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_POSITION, "LATEST")
    consumerConfig.put(ConsumerConfigConstants.SHARD_USE_ADAPTIVE_READS, "true")
    //consumerConfig.put(ConsumerConfigConstants.STREAM_TIMESTAMP_DATE_FORMAT, "yyyy-MM-dd HH:mm:ss.SSS")
    //consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_TIMESTAMP, new Timestamp(Instant.now.minus(1, ChronoUnit.SECONDS).toEpochMilli).toString)
    if (Config.settings.localMode) {
      consumerConfig.put(AWSConfigConstants.AWS_PROFILE_NAME, "twitch-commerce-science-aws")
      consumerConfig.put(AWSConfigConstants.AWS_CREDENTIALS_PROVIDER, "PROFILE")
    }

    val consumer = new FlinkKinesisConsumer(streamName,
      new SimpleStringSchema(),
      consumerConfig)

    val consumerSource = env.addSource(consumer)
      .name(streamName).uid(streamName).setParallelism(Config.settings.readParallelism)

    Kinesis.flattenCompressedStream(consumerSource)
  }

  def kinesisProducer(streamName: String): FlinkKinesisProducer[String] = {
    val kinesisProducerConfig = new Properties()
    kinesisProducerConfig.put(AWSConfigConstants.AWS_REGION, region)
    kinesisProducerConfig.put("MetricGranularity", "stream")
    kinesisProducerConfig.put("MetricsLevel", "summary")
    kinesisProducerConfig.put("LogLevel", "warning")
    if (Config.settings.localMode) {
      kinesisProducerConfig.put(AWSConfigConstants.AWS_PROFILE_NAME, "twitch-commerce-science-aws")
      kinesisProducerConfig.put(AWSConfigConstants.AWS_CREDENTIALS_PROVIDER, "PROFILE")
    }

    val kinesisProducer = new FlinkKinesisProducer[String](new SimpleStringSchema, kinesisProducerConfig)
    kinesisProducer.setFailOnError(false)
    kinesisProducer.setDefaultStream(streamName)
    kinesisProducer.setCustomPartitioner(new RandomKinesisPartitioner())
    kinesisProducer
  }

  def flattenCompressedStream[T](consumer: DataStream[String]): DataStream[Spade.EventRecord] = {
    consumer.flatMap(input => {
      try {
        val decompressed = KinesisCompression.decompress(mapper.readValue(input, classOf[KinesisRecord]).data)
        JsonMapper.mapper.readValue[Seq[Spade.EventRecord]](decompressed)
      } catch {
        case e : Exception => Seq()
      }
    })
    .name("decompress-flatten")
    .uid("decompress-flatten")
  }
}