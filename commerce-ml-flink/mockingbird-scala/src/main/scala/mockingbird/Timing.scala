package mockingbird

import java.time.{Clock, ZonedDateTime, ZoneOffset}

object Timing {
  var clock: Clock = Clock.systemUTC()
  def currentTimestamp: String = ZonedDateTime.now(clock).toEpochSecond.toString
  def currentTimestampMs: Long = ZonedDateTime.now(clock).toInstant.toEpochMilli
  def restore(): Unit = {
    clock = Clock.systemUTC()
  }
}
