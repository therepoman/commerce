package mockingbird

import org.apache.commons.io.IOUtils

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.util.Base64
import java.util.zip.{Deflater, DeflaterInputStream, Inflater, InflaterInputStream}

object KinesisCompression {
  def decompress(compressed: String): String = {
    val decoder = Base64.getDecoder
    val decoded = decoder.decode(compressed.getBytes)
    val inputStream = new ByteArrayInputStream(decoded)
    inputStream.skip(1)
    val stream = new InflaterInputStream(inputStream, new Inflater(true))
    IOUtils.toString(stream, StandardCharsets.UTF_8)
  }

  def compress(str: String): String = {
    val inputStream = new ByteArrayInputStream(str.getBytes())
    val stream = new DeflaterInputStream(inputStream, new Deflater(0, true))
    val compressed = IOUtils.toByteArray(stream)

    import java.io.ByteArrayOutputStream
    val outputStream = new ByteArrayOutputStream
    outputStream.write(Array[Byte](1.toByte))
    outputStream.write(compressed)
    val compressedWithVersion = outputStream.toByteArray

    val encoder = Base64.getEncoder
    new String(encoder.encode(compressedWithVersion), StandardCharsets.UTF_8)
  }
}