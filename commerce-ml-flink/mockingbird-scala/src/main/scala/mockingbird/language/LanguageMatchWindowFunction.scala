//package mockingbird.language
//
//import mockingbird.Redis
//import mockingbird.count.{CustomizedSessionWindowSettings, Keys}
//import mockingbird.tokens.{CountTypes, Generator}
//import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
//import org.apache.flink.streaming.api.functions.KeyedProcessFunction
//import org.apache.flink.util.Collector
//
//import scala.collection.mutable
//
//class LanguageMatchWindowFunction(windowSettings: CustomizedSessionWindowSettings) extends
//    KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] {
//
//    /** The state that is maintained by this process function */
//    lazy val state: ValueState[LanguageMatchMetrics] = getRuntimeContext
//      .getState(new ValueStateDescriptor[LanguageMatchMetrics]("ecState", classOf[LanguageMatchMetrics]))
//
//
//    override def processElement(
//                                 value: CountTypes.ValueType,
//                                 ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
//                                 out: Collector[CountTypes.OutputType]): Unit = {
//
//      val sessionOffset = Generator.generator.reportOffset(windowSettings.sessionGapMs)
//
//      val nextSession = windowSettings.nextSession(ctx.timestamp + sessionOffset)
//
//      val current: LanguageMatchMetrics = state.value match {
//        case null =>
//          LanguageMatchMetrics(
//            userId = value.fields.userId,
//            sessionExpiration = 0,
//            sessionStart = ctx.timestamp,
//            modified = false,
//            languageMap = mutable.Map[String, Int](),
//            languageMatched = true
//          )
//        case lm: LanguageMatchMetrics =>
//          lm
//      }
//
//      // always push session expiration time to now + sessionGap when new events come
//      current.sessionExpiration = nextSession
//      current.add(value.fields.language)
//
//      // schedule reporting the event in the next reporting interval
//      if (current.modified) {
//        val reportOffset = Generator.generator.reportOffset(windowSettings.reportingIntervalsMs)
//        val nextReport = windowSettings.nextReport(ctx.timestamp + reportOffset)
//        ctx.timerService.registerEventTimeTimer(nextReport)
//      }
//
//      // schedule reporting the event in the session expiration time
//      ctx.timerService.registerEventTimeTimer(nextSession)
//
//      // write the state back
//      state.update(current)
//    }
//
//    override def onTimer(
//                          timestamp: Long,
//                          ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#OnTimerContext,
//                          out: Collector[CountTypes.OutputType]): Unit = {
//      state.value match {
//        case lm: LanguageMatchMetrics =>
//          if (lm.modified) {
//            lm.modified = false
////            println(lm.userId, lm.languageMap, lm.languageMatched)
//            out.collect(Redis.getFeatureUpdate(lm.userId, "languageMatched", if (lm.languageMatched) 1 else 0))
//            }
//
//          if (timestamp >= lm.sessionExpiration) {
//            out.collect(Redis.getRemoveFeature(lm.userId, "languageMatched"))
//            state.clear()
//          }
//
//          case _ =>
//      }
//    }
//}
