package mockingbird.language
import scala.collection.mutable

case class LanguageMatchMetrics(var userId: String,
                                var sessionExpiration: Long,
                                var sessionStart: Long,
                                // This part can be removed, we store language pairs but don't use them for now
                                var languageMap: mutable.Map[String, Int],
                                var languageMatched: Boolean,
                                var modified: Boolean)
{
  def this() {
    this("", 0, 0, mutable.Map[String, Int](), true, false)
  }

  def add(language: String): Unit = {
    if(languageMap.size == 0) modified = true // initialization, so always report first time

    if(languageMap.contains(language)) languageMap(language) += 1
    else languageMap.put(language, 1)

    // once it turns to false, we should never fire another report until the session expires
    if (languageMatched == false) return

    val languageSplit = language.split(":")
    if (languageSplit.length >= 2 && (languageSplit(0) != languageSplit(1))) {
      languageMatched = false
      modified = true
    }
    }

  def sessionDuration(timestamp: Long, shiftInSeconds: Int): Long = {
    (timestamp - sessionStart) - shiftInSeconds * 1000
  }
  def sessionDurationInMinutes(timestamp: Long, shiftInSeconds: Int): String = {
    val duration = sessionDuration(timestamp, shiftInSeconds)
    "%.2f".format(duration.toFloat / (60 * 1000)) + " minutes"
  }
}
