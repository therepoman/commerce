package mockingbird

import com.fasterxml.jackson.annotation.{JsonProperty, JsonSubTypes, JsonTypeInfo}

import java.time.{ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

object Spade {
  val ZONE_ID: ZoneId = ZoneId.of("UTC")
  val DTF: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZONE_ID)

  sealed trait SpadeEvent {
    def filter: Boolean
    def userId: String
    def deviceId: String
    def ip: String
    def country: String
    def region: String
    def toUserToken: String
    def timeUtc: String
    def clientTimeUtc: String
    def time: Long = ZonedDateTime.parse(timeUtc, DTF).toInstant.toEpochMilli
    def clientTime: Long = ZonedDateTime.parse(clientTimeUtc.replaceAll("([.]\\d{0,3})", ""), DTF).toInstant.toEpochMilli
    def language: String = "" // default to be empty for events that don't have language fields
    def checkoutSource: String = "" // this means source from checkout_action_check
  }

  def hasValue(featureValue: String): Boolean =  (featureValue != null) && (featureValue != "")

  case class EventRecord(
                          @JsonProperty("Name") var name: String,
                          @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "Name")
                          @JsonSubTypes(Array(
                            new JsonSubTypes.Type(value = classOf[MinuteWatchedEvent], name = "minute-watched"),
                            new JsonSubTypes.Type(value = classOf[CheckoutSession], name = "checkout_session"),
                            new JsonSubTypes.Type(value = classOf[SubscribeButton], name = "subscribe_button"),
                            new JsonSubTypes.Type(value = classOf[BitsCardInteraction], name = "bits_card_interaction"),
                            new JsonSubTypes.Type(value = classOf[PaymentGatewayResponse], name = "payment_gateway_response"),
                            new JsonSubTypes.Type(value = classOf[LoginSuccess], name = "login_success"),
                            new JsonSubTypes.Type(value = classOf[Chat], name = "chat"),
                            new JsonSubTypes.Type(value = classOf[PageView], name = "pageview"),
                            new JsonSubTypes.Type(value = classOf[CheckoutActionCheck], name = "checkout_action_check"),
                            new JsonSubTypes.Type(value = classOf[ProductPayment], name = "product_payment")
                          ))
                          @JsonProperty("Fields") var fields: SpadeEvent
                        ) {
    def this() {
      this("", new MinuteWatchedEvent())
    }
  }

  case class MinuteWatchedEvent(
                                 @JsonProperty("channel_id") var channelId: String,
                                 @JsonProperty("user_id") var userId: String,
                                 @JsonProperty("ip") var ip: String,
                                 @JsonProperty("device_id") var deviceId: String,
                                 @JsonProperty("time_utc") var timeUtc: String,
                                 @JsonProperty("client_time_utc") var clientTimeUtc: String,
                                 @JsonProperty("region") var region: String,
                                 @JsonProperty("country") var country: String,
                               ) extends SpadeEvent {

    def this() {
      this("", "", "", "", "", "", "", "")
    }

    override def filter: Boolean =
      hasValue(this.channelId) &&
      hasValue(this.timeUtc) &&
      hasValue(this.channelId) &&
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    // not used for minute watch event
    override def toUserToken = s"mw:${channelId}"
  }

  case class CheckoutSession(
                              @JsonProperty("sub_event") var subEvent: String,
                              @JsonProperty("user_id") var userId: String,
                              @JsonProperty("ip") var ip: String,
                              @JsonProperty("device_id") var deviceId: String,
                              @JsonProperty("time_utc") var timeUtc: String,
                              @JsonProperty("client_time_utc") var clientTimeUtc: String,
                              @JsonProperty("received_language") var receivedLanguage: String,
                              @JsonProperty("preferred_language") var preferredLanguage: String,
                              @JsonProperty("region") var region: String,
                              @JsonProperty("country") var country: String,
                            ) extends SpadeEvent {
    def this() {
      this("", "", "", "", "", "", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.subEvent) &&
      hasValue(this.timeUtc) &&
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    override def toUserToken = s"ec:checkout_session.${this.subEvent}"
    override def language: String = s"${this.receivedLanguage.take(2).toLowerCase()}:${this.preferredLanguage.take(2).toLowerCase()}"
  }

  case class SubscribeButton(
                              var action: String,
                              @JsonProperty("user_id") var userId: String,
                              @JsonProperty("ip") var ip: String,
                              @JsonProperty("device_id") var deviceId: String,
                              @JsonProperty("time_utc") var timeUtc: String,
                              @JsonProperty("client_time_utc") var clientTimeUtc: String,
                              @JsonProperty("received_language") var receivedLanguage: String,
                              @JsonProperty("preferred_language") var preferredLanguage: String,
                              @JsonProperty("region") var region: String,
                              @JsonProperty("country") var country: String,
                            ) extends SpadeEvent {

    def this() {
      this("", "", "", "", "", "", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.action) &&
      hasValue(this.timeUtc) &&
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    override def toUserToken = s"ec:subscribe_button.${this.action}"
    override def language: String = s"${this.receivedLanguage.take(2).toLowerCase()}:${this.preferredLanguage.take(2).toLowerCase()}"
  }

  case class BitsCardInteraction(
                                  @JsonProperty("action_name") var actionName: String,
                                  @JsonProperty("user_id") var userId: String,
                                  @JsonProperty("ip") var ip: String,
                                  @JsonProperty("device_id") var deviceId: String,
                                  @JsonProperty("time_utc") var timeUtc: String,
                                  @JsonProperty("client_time_utc") var clientTimeUtc: String,
                                  @JsonProperty("received_language") var receivedLanguage: String,
                                  @JsonProperty("preferred_language") var preferredLanguage: String,
                                  @JsonProperty("region") var region: String,
                                  @JsonProperty("country") var country: String,
                                ) extends SpadeEvent {
    def this() {
      this("", "", "", "", "", "", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.actionName) &&
      hasValue(this.timeUtc) &&
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    override def toUserToken = s"ec:bits_card_interaction.${this.actionName}"
    override def language: String = s"${this.receivedLanguage.take(2).toLowerCase()}:${this.preferredLanguage.take(2).toLowerCase()}"
  }

  case class PaymentGatewayResponse(
                                     @JsonProperty("user_id") var userId: String,
                                     @JsonProperty("time_utc") var timeUtc: String,
                                     @JsonProperty("client_time_utc") var clientTimeUtc: String,
                                   ) extends SpadeEvent {
    def this() {
      this("", "", "")
    }

    def filter: Boolean =
      hasValue(this.userId)

    override def toUserToken: String = s"ec:payment_gateway_response"
    override def ip: String = ""
    override def deviceId: String = ""
    override def country: String = ""
    override def region: String = ""
  }

  case class LoginSuccess(
                           @JsonProperty("user_id") var userId: String,
                           @JsonProperty("ip") var ip: String,
                           @JsonProperty("device_id") var deviceId: String,
                           @JsonProperty("time_utc") var timeUtc: String,
                           @JsonProperty("client_time_utc") var clientTimeUtc: String,
                           @JsonProperty("received_language") var receivedLanguage: String,
                           @JsonProperty("preferred_language") var preferredLanguage: String,
                           @JsonProperty("region") var region: String,
                           @JsonProperty("country") var country: String,
                         ) extends SpadeEvent {
    def this() {
      this("", "", "", "", "", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    override def toUserToken = s"ec:login_success"
    override def language: String = s"${this.receivedLanguage.take(2).toLowerCase()}:${this.preferredLanguage.take(2).toLowerCase()}"
  }

  case class Chat(
                   @JsonProperty("channel_id") var channelId: String,
                   @JsonProperty("user_id") var userId: String,
                   @JsonProperty("ip") var ip: String,
                   @JsonProperty("device_id") var deviceId: String,
                   @JsonProperty("time_utc") var timeUtc: String,
                   @JsonProperty("client_time_utc") var clientTimeUtc: String,
                   @JsonProperty("region") var region: String,
                   @JsonProperty("country") var country: String,
                 ) extends SpadeEvent {
    def this() {
      this("", "", "", "", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.channelId) &&
      hasValue(this.timeUtc) &&
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    override def toUserToken = s"ch:${channelId}"
  }

  case class PageView(
                       @JsonProperty("location") var location: String,
                       @JsonProperty("user_id") var userId: String,
                       @JsonProperty("ip") var ip: String,
                       @JsonProperty("device_id") var deviceId: String,
                       @JsonProperty("time_utc") var timeUtc: String,
                       @JsonProperty("client_time_utc") var clientTimeUtc: String,
                       @JsonProperty("received_language") var receivedLanguage: String,
                       @JsonProperty("preferred_language") var preferredLanguage: String,
                       @JsonProperty("region") var region: String,
                       @JsonProperty("country") var country: String,
                 ) extends SpadeEvent {
    def this() {
      this("", "", "", "", "", "", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.location) &&
      hasValue(this.timeUtc) &&
      hasValue(this.userId) &&
      hasValue(this.ip) &&
      hasValue(this.deviceId) &&
      hasValue(this.region) &&
      hasValue(this.country)

    override def toUserToken = s"ec:pageview.${location}"
    override def language: String = s"${this.receivedLanguage.take(2).toLowerCase()}:${this.preferredLanguage.take(2).toLowerCase()}"
  }

  case class CheckoutActionCheck(
                                  @JsonProperty("time_utc") var timeUtc: String,
                                  @JsonProperty("purchaser_id") var userId: String,
                                  @JsonProperty("device_id") var deviceId: String,
                                  @JsonProperty("source") var source: String,
                                  @JsonProperty("country_code") var countryCode: String
                ) extends SpadeEvent {
    def this() {
      this("", "", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.userId) &&
      hasValue(this.timeUtc)

    override def toUserToken: String = ""
    override def checkoutSource: String = this.source
    override def country: String = this.countryCode
    override def region: String = ""
    override def ip: String = "" // ip of server-side events is meaningless
    override def clientTimeUtc: String = ""
  }

  case class ProductPayment(
                 @JsonProperty("time_utc") var timeUtc: String,
                 @JsonProperty("purchaser_user_id") var userId: String,
                 @JsonProperty("checkout_session_id") var checkoutSessionId: String,
                 @JsonProperty("platform") var platform: String
               ) extends SpadeEvent {
    def this() {
      this("", "", "", "")
    }

    def filter: Boolean =
      hasValue(this.userId) &&
      hasValue(this.timeUtc) &&
      hasValue(this.checkoutSessionId)

    override def toUserToken: String = ""
    override def ip: String = ""
    override def country: String = ""
    override def region: String = ""
    override def deviceId: String = ""
    override def clientTimeUtc: String = ""
  }
}
