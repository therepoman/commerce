package mockingbird.count

case class AggregateCountMetrics(var keyField: String,
                                 var id: String,
                                 var sessionExpiration: Long,
                                 var sessionStart: Long,
                                 var nextReport: Long,
                                 var nextRefresh: Long,
                                 var jitter: Long,
                                 var count: Int,
                                 var modified: Boolean,
                                 var modifiedAt: Long){
  def this() {
        this("", "", 0, 0, 0, 0, 0, 0, false, 0)
      }

  def add(timestamp: Long): Unit = {
    count += 1
    modified = true
    modifiedAt = timestamp
  }

  def aggCount(): Int = count
}