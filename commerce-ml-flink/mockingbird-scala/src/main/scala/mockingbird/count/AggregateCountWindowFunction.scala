package mockingbird.count

import mockingbird.Redis
import mockingbird.tokens.{CountTypes, Generator}
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.util.Collector

class AggregateCountWindowFunction(windowSettings: CustomizedSessionWindowSettings,
                                   keyField: String) extends
  KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] {


  /** The state that is maintained by this process function */
  lazy val state: ValueState[AggregateCountMetrics] = getRuntimeContext
    .getState(new ValueStateDescriptor[AggregateCountMetrics]("ecState", classOf[AggregateCountMetrics]))

  def currentTime(
                   ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                 ): Long = {
    if (windowSettings.UseProcessingTimeTimers) {
      ctx.timerService.currentProcessingTime
    } else {
      ctx.timestamp
    }
  }

  def registerProcessingOrEventTimeTimer(
                                          ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                                          timestamp: Long
                                        ): Unit = {
    if (windowSettings.UseProcessingTimeTimers) {
      ctx.timerService.registerProcessingTimeTimer(timestamp)
    } else {
      ctx.timerService.registerEventTimeTimer(timestamp)

    }
  }

  override def processElement(
                               value: CountTypes.ValueType,
                               ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                               out: Collector[CountTypes.OutputType]): Unit = {


    val current: AggregateCountMetrics = state.value match {
      case null =>

        val jitter = Generator.generator.jitter(5 * 1000) // jitter 5s
        val id: String = keyField match {
          case GroupByKeys.userId => value.fields.userId
          case _ => throw new Exception("Specified key field not found")}

        val nextReport = windowSettings.nextReport(currentTime(ctx) + jitter)
        val nextRefresh = windowSettings.nextRefresh(currentTime(ctx)+ jitter)
        val sessionExpiration = windowSettings.nextSession(currentTime(ctx) + jitter)

        registerProcessingOrEventTimeTimer(ctx, nextRefresh)
        registerProcessingOrEventTimeTimer(ctx, nextReport)
        registerProcessingOrEventTimeTimer(ctx, sessionExpiration)

        AggregateCountMetrics(
          keyField = keyField,
          id = id,
          sessionExpiration = sessionExpiration,
          sessionStart = currentTime(ctx),
          nextReport = nextReport,
          nextRefresh = nextRefresh,
          jitter = jitter,
          count = 0,
          modified = false,
          modifiedAt = 0
        )

      case acm: AggregateCountMetrics =>
        acm
    }

    current.add(currentTime(ctx) + current.jitter)

    // schedule reporting the event in the next reporting interval
    // processing an element means there's always update for the aggregate value
    // so we need to register a report time to write it
    if (ctx.timestamp >= current.nextReport) {
      current.nextReport = windowSettings.nextReport(currentTime(ctx) + current.jitter)
      registerProcessingOrEventTimeTimer(ctx, current.nextReport)
    }

    // write the state back
    state.update(current)
  }

  override def onTimer(
                        timestamp: Long,
                        ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#OnTimerContext,
                        out: Collector[CountTypes.OutputType]): Unit = {
    state.value match {
      case acm: AggregateCountMetrics =>
        if (timestamp >= acm.sessionExpiration) {
          val nextExpiration = windowSettings.nextSession(acm.modifiedAt)
          if (nextExpiration > timestamp) {
            acm.sessionExpiration = nextExpiration
            registerProcessingOrEventTimeTimer(ctx, acm.sessionExpiration)
          }
          else {
            out.collect(Redis.getRemoveUserFeature(acm.id, "payment_count"))
            state.clear()
            return
          }
        }

        if (acm.modified && timestamp >= acm.nextReport) {
          out.collect(Redis.getUserFeatureUpdate(acm.id, "payment_count", acm.aggCount()))
          acm.modified = false
        }

        if (timestamp >= acm.nextRefresh) {
          out.collect(Redis.getUserFeatureUpdate(acm.id, "payment_count", acm.aggCount()))
          acm.nextRefresh = windowSettings.nextRefresh(acm.nextRefresh)
          registerProcessingOrEventTimeTimer(ctx, acm.nextRefresh)
        }

        state.update(acm)

      case _ =>
    }
  }
}