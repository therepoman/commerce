package mockingbird.count

case class CustomizedSessionWindowSettings(
                                         SessionGapMinutes: Int,
                                         RefreshIntervalMinutes: Int,
                                         ReportingIntervalSeconds: Int,
                                         UseProcessingTimeTimers: Boolean = false
                                         ) {
  val reportingIntervalsMs: Int = ReportingIntervalSeconds * 1000
  val sessionGapMs: Int = SessionGapMinutes * 60 * 1000
  val refreshIntervalMs: Int = RefreshIntervalMinutes * 60 * 1000

  def nextReport(currentTimestamp: Long): Long = currentTimestamp + reportingIntervalsMs
  def nextSession(currentTimestamp: Long): Long = currentTimestamp + sessionGapMs
  def nextRefresh(currentTimestamp: Long): Long = currentTimestamp + refreshIntervalMs
}
