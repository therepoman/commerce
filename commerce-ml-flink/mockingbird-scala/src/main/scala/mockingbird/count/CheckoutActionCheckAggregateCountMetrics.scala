package mockingbird.count

import scala.collection.mutable

case class CheckoutActionCheckAggregateCountMetrics(var keyField: String,
                                                    var id: String,
                                                    var checkoutModified: Boolean,
                                                    var purchaseOfferModified: Boolean,
                                                    var countryModified: Boolean,
                                                    var sessionExpiration: Long,
                                                    var sessionStart: Long,
                                                    var nextReport: Long,
                                                    var nextRefresh: Long,
                                                    var jitter: Long,
                                                    var countCheckout: Int,
                                                    var countPurchaseOffer: Int,
                                                    var countrySet: mutable.Set[String],
                                                    var modifiedAt: Long){
  def this() {
        this("", "", false, false, false, 0, 0, 0, 0, 0, 0, 0, mutable.Set(), 0)
      }

  def add(source: String, country: String, timestamp: Long): Unit = {
    checkoutModified = true
    modifiedAt = timestamp
    countCheckout += 1
    if (source == "PurchaseOffer") {
      purchaseOfferModified = true
      countPurchaseOffer += 1
    }
    if (!countrySet.contains(country)) {
      countrySet.add(country)
      countryModified = true
    }
  }

  def aggCheckoutCount(): Int = countCheckout
  def aggPurchaseOfferCount(): Int = countPurchaseOffer
  def aggCountryCount(): Int = countrySet.size
}