package mockingbird.count

import mockingbird.Redis
import mockingbird.tokens.{CountTypes, Generator}
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.util.Collector

import scala.collection.mutable

class DistinctUserCountWindowFunction(windowSettings: CustomizedSessionWindowSettings,
                                      keyField: String) extends
  KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] {


  /** The state that is maintained by this process function */
  lazy val state: ValueState[DistinctUserCountMetrics] = getRuntimeContext
    .getState(new ValueStateDescriptor[DistinctUserCountMetrics]("ecState", classOf[DistinctUserCountMetrics]))


  override def processElement(
                               value: CountTypes.ValueType,
                               ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                               out: Collector[CountTypes.OutputType]): Unit = {


    val current: DistinctUserCountMetrics = state.value match {
      case null =>

        val jitter = Generator.generator.jitter(60 * 1000) // jitter 60s
        val id: String = keyField match {
          case GroupByKeys.deviceId => value.fields.deviceId
          case GroupByKeys.ip => value.fields.ip
          case _ => throw new Exception("Specified key field not found")}

        val nextReport = windowSettings.nextReport(ctx.timestamp + jitter)
        val nextRefresh = windowSettings.nextRefresh(ctx.timestamp + jitter)
        ctx.timerService.registerEventTimeTimer(nextRefresh) // register next refresh and never touch it again in processElement

        DistinctUserCountMetrics(
          keyField = keyField,
          id = id,
          sessionExpiration = 0, // set later
          sessionStart = ctx.timestamp,
          modified = false,
          userIdSet = mutable.Set[String](),
          nextReport = nextReport,
          nextRefresh = nextRefresh,
          jitter = jitter
        )
      case dm: DistinctUserCountMetrics =>
        dm
    }

    // always push session expiration time to now + sessionGap when a new event comes an delete previous timer
    ctx.timerService.deleteEventTimeTimer(current.sessionExpiration)
    current.sessionExpiration = windowSettings.nextSession(ctx.timestamp + current.jitter)
    ctx.timerService.registerEventTimeTimer(current.sessionExpiration)

    val setModified: Boolean = current.add(value.fields.userId)

    // schedule reporting the event in the next reporting interval
    if (ctx.timestamp >= current.nextReport) {
      current.nextReport = windowSettings.nextReport(current.nextReport)
    }

    // if the userIdSet is modified, register the next reporting time
    if (setModified) {
      ctx.timerService.registerEventTimeTimer(current.nextReport)
    }

    // write the state back
    state.update(current)
  }

  override def onTimer(
                        timestamp: Long,
                        ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#OnTimerContext,
                        out: Collector[CountTypes.OutputType]): Unit = {
    state.value match {
      case dm: DistinctUserCountMetrics =>

        if (timestamp >= dm.sessionExpiration) {
          out.collect(Redis.getRemoveFeatureGroup(keyField, dm.id))
          state.clear()
          return
        }

        if (dm.modified && timestamp >= dm.nextReport){
          out.collect(Redis.getFeatureUpdate(keyField, dm.id, "user_count", dm.userIdCount()))
          dm.modified = false
        }


        if (timestamp >= dm.nextRefresh) {
          out.collect(Redis.getFeatureUpdate(keyField, dm.id, "user_count", dm.userIdCount()))
          dm.nextRefresh = windowSettings.nextRefresh(dm.nextRefresh)
          ctx.timerService.registerEventTimeTimer(dm.nextRefresh)
        }

        state.update(dm)

      case _ =>
    }
  }
}

