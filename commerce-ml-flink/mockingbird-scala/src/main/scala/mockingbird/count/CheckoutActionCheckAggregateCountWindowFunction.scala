package mockingbird.count

import mockingbird.Redis
import mockingbird.tokens.{CountTypes, Generator}
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.util.Collector

import scala.collection.mutable

/** This is mainly used to do a running total for checkout action check. Not ready to be generically applied to
 * other events but with small modifications it should work*/
class CheckoutActionCheckAggregateCountWindowFunction(windowSettings: CustomizedSessionWindowSettings,
                                                      keyField: String) extends
  KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] {


  /** The state that is maintained by this process function */
  lazy val state: ValueState[CheckoutActionCheckAggregateCountMetrics] = getRuntimeContext
    .getState(new ValueStateDescriptor[CheckoutActionCheckAggregateCountMetrics]("ecState", classOf[CheckoutActionCheckAggregateCountMetrics]))

  def currentTime(
                   ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                 ): Long = {
    if (windowSettings.UseProcessingTimeTimers) {
      ctx.timerService.currentProcessingTime
    } else {
      ctx.timestamp
    }
  }

  def registerProcessingOrEventTimeTimer(
                                          ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                                          timestamp: Long
                                        ): Unit = {
    if (windowSettings.UseProcessingTimeTimers) {
      ctx.timerService.registerProcessingTimeTimer(timestamp)
    } else {
      ctx.timerService.registerEventTimeTimer(timestamp)

    }
  }

  override def processElement(
                               value: CountTypes.ValueType,
                               ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                               out: Collector[CountTypes.OutputType]): Unit = {


    val current: CheckoutActionCheckAggregateCountMetrics = state.value match {
      case null =>

        val jitter = Generator.generator.jitter(5 * 1000) // jitter 5s
        val id: String = keyField match {
          case GroupByKeys.userId => value.fields.userId
          case _ => throw new Exception("Specified key field not found")}

        val nextReport = windowSettings.nextReport(currentTime(ctx) + jitter)
        val nextRefresh = windowSettings.nextRefresh(currentTime(ctx) + jitter)
        val sessionExpiration = windowSettings.nextSession(currentTime(ctx) + jitter)

        registerProcessingOrEventTimeTimer(ctx, nextReport)
        registerProcessingOrEventTimeTimer(ctx, nextRefresh)
        registerProcessingOrEventTimeTimer(ctx, sessionExpiration)

        CheckoutActionCheckAggregateCountMetrics(
          keyField = keyField,
          id = id,
          sessionExpiration = sessionExpiration,
          sessionStart = ctx.timestamp,
          checkoutModified = false,
          purchaseOfferModified = false,
          countryModified = false,
          nextReport = nextReport,
          nextRefresh = nextRefresh,
          jitter = jitter,
          countCheckout = 0,
          countPurchaseOffer = 0,
          countrySet = mutable.Set(),
          modifiedAt = 0
        )

      case acm: CheckoutActionCheckAggregateCountMetrics =>
        acm
    }

    current.add(value.fields.checkoutSource, value.fields.country, currentTime(ctx) + current.jitter)

    // schedule reporting the event in the next reporting interval
    // processing an element means there's always update for the aggregate value
    // so we need to register a report time to write it
    if (ctx.timestamp >= current.nextReport) {
      current.nextReport = windowSettings.nextReport(currentTime(ctx) + current.jitter)
      registerProcessingOrEventTimeTimer(ctx, current.nextReport)
    }

    // write the state back
    state.update(current)
  }

  override def onTimer(
                        timestamp: Long,
                        ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#OnTimerContext,
                        out: Collector[CountTypes.OutputType]): Unit = {
    state.value match {
      case acm: CheckoutActionCheckAggregateCountMetrics =>
        if (timestamp >= acm.sessionExpiration) {
          val nextExpiration = windowSettings.nextSession(acm.modifiedAt)
          if (nextExpiration > timestamp) {
            acm.sessionExpiration = nextExpiration
            registerProcessingOrEventTimeTimer(ctx, acm.sessionExpiration)
          } else {
          out.collect(Redis.getRemoveUserFeature(acm.id, "session_count"))
          out.collect(Redis.getRemoveUserFeature(acm.id, "purchase_count"))
          out.collect(Redis.getRemoveUserFeature(acm.id, "country_count"))
          state.clear()
          return
          }
        }

        if (acm.checkoutModified && timestamp >= acm.nextReport) {
          out.collect(Redis.getUserFeatureUpdate(acm.id, "session_count", acm.aggCheckoutCount()))
          acm.checkoutModified = false
        }

        if (acm.purchaseOfferModified && timestamp >= acm.nextReport) {
          out.collect(Redis.getUserFeatureUpdate(acm.id, "purchase_count", acm.aggPurchaseOfferCount()))
          acm.purchaseOfferModified = false
        }

        if (acm.countryModified && timestamp >= acm.nextReport) {
          out.collect(Redis.getUserFeatureUpdate(acm.id, "country_count", acm.aggCountryCount()))
          acm.countryModified = false
        }


        if (timestamp >= acm.nextRefresh) {
          out.collect(Redis.getUserFeatureUpdate(acm.id, "session_count", acm.aggCheckoutCount()))
          out.collect(Redis.getUserFeatureUpdate(acm.id, "purchase_count", acm.aggPurchaseOfferCount()))
          out.collect(Redis.getUserFeatureUpdate(acm.id, "country_count", acm.aggCountryCount()))
          acm.nextRefresh = windowSettings.nextRefresh(acm.nextRefresh)
          registerProcessingOrEventTimeTimer(ctx, acm.nextRefresh)
        }

        state.update(acm)

      case _ =>
    }
  }
}