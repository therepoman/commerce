package mockingbird.count

import scala.collection.mutable

case class DistinctUserCountMetrics(var keyField: String,
                                    var id: String,
                                    var modified: Boolean,
                                    var sessionExpiration: Long,
                                    var sessionStart: Long,
                                    var userIdSet: mutable.Set[String],
                                    var nextReport: Long,
                                    var nextRefresh: Long,
                                    var jitter: Long){
  def this() {
    this("", "", false, 0, 0, mutable.Set(), 0, 0, 0)
  }

  def add(userId: String): Boolean = {
    if (!userIdSet.contains(userId)) {
      userIdSet.add(userId)
      modified = true
      true
    }
    else false
  }
  def userIdCount(): Int = {
    userIdSet.size
  }

  def sessionDuration(timestamp: Long, shiftInSeconds: Int): Long = {
    (timestamp - sessionStart) - shiftInSeconds * 1000
  }
  def sessionDurationInMinutes(timestamp: Long, shiftInSeconds: Int): String = {
    val duration = sessionDuration(timestamp, shiftInSeconds)
    "%.2f".format(duration.toFloat / (60 * 1000)) + " minutes"
  }
}
