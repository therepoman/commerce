package mockingbird

import mockingbird.config.Config
import mockingbird.metrics.{EventDelayNowGagueMapper, EventDelayUserTimeGagueMapper, EventDelayWatermarkGagueMapper}
import mockingbird.tokens.{CountTypes, FeatureValue, UserMetrics, UserTokenWindowFunction, UserTokenWindowSettings}
import org.apache.flink.api.common.eventtime.{SerializableTimestampAssigner, WatermarkStrategy}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._

import java.time.Duration
import mockingbird.count.{AggregateCountWindowFunction, CheckoutActionCheckAggregateCountWindowFunction, CustomizedSessionWindowSettings, DistinctUserCountWindowFunction, GroupByKeys}

object StreamingJob {
  private val CHECKPOINT_INTERVAL_MS = 60000L // 1 second
  private val CHECKPOINT_TIMEOUT_MS = 60 * 60 * 1000L // 60 minutes


  def main(args: Array[String]) {
    val env = if (!Config.settings.localMode) {
      val env2 = StreamExecutionEnvironment.getExecutionEnvironment
      env2.enableCheckpointing(CHECKPOINT_INTERVAL_MS)
      env2.getCheckpointConfig.setCheckpointTimeout(CHECKPOINT_TIMEOUT_MS)
      env2
    } else {
      val env2 = StreamExecutionEnvironment.createLocalEnvironment(10)
      env2
    }
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.registerType(classOf[Spade.EventRecord])
    env.registerType(classOf[Spade.MinuteWatchedEvent])
    env.registerType(classOf[Spade.Chat])
    env.registerType(classOf[Spade.PageView])
    env.registerType(classOf[Spade.BitsCardInteraction])
    env.registerType(classOf[Spade.CheckoutSession])
    env.registerType(classOf[Spade.LoginSuccess])
    env.registerType(classOf[Spade.PaymentGatewayResponse])
    env.registerType(classOf[Spade.SubscribeButton])
    env.registerType(classOf[Spade.CheckoutActionCheck])
    env.registerType(classOf[Spade.ProductPayment])
    env.registerType(classOf[UserMetrics])
    env.registerType(classOf[FeatureValue])


    val sinkName = s"sink-${Config.settings.redisOutputStreamName}"
    val inputStream = Kinesis.kinesisConsumer[Spade.EventRecord](
      Config.settings.minuteWatchKinesisStreamName,
      env,
    )

    val baseStream = inputStream
      .filter(e => e.fields.filter)
      .uid("baseFilter").name("baseFilter")
      .process(new EventDelayWatermarkGagueMapper)
      .process(new EventDelayNowGagueMapper)
      .process(new EventDelayUserTimeGagueMapper)
      .assignTimestampsAndWatermarks(WatermarkStrategy
        .forBoundedOutOfOrderness(Duration.ofSeconds(30))
        .withTimestampAssigner(new SerializableTimestampAssigner[Spade.EventRecord] {
          override def extractTimestamp(element: Spade.EventRecord, recordTimestamp: Long): Long = element.fields.time
        })
      )

    var unionStream: DataStream[CountTypes.OutputType] = null

    if (Config.settings.enableUserMetrics) {
      val userIdTokenStream = baseStream
        .keyBy(se => se.fields.userId)
        .process(new UserTokenWindowFunction(UserTokenWindowSettings(
          WindowSizeMinutes = 60,
          SlideSizeMinutes = 10,
          ReportingIntervalSeconds = 10,
          UseProcessingTimeTimers = true
        )))

      unionStream = if (unionStream == null) userIdTokenStream else unionStream.union(userIdTokenStream)
    }

    if (Config.settings.enableIPMetrics) {
      val ipStream = baseStream.keyBy(se => se.fields.ip)
        .process(new DistinctUserCountWindowFunction(
          CustomizedSessionWindowSettings(
            SessionGapMinutes = 60,
            // TODO: not turn this and device metrics on this time. But these two have much higher volumes,
            // can redis handle the write frequency?
            RefreshIntervalMinutes =  50,
            ReportingIntervalSeconds = 10),
          GroupByKeys.ip
        ))
      unionStream = if (unionStream == null) ipStream else unionStream.union(ipStream)
    }

    if (Config.settings.enableDeviceMetrics) {
      val deviceIdStream = baseStream
        .filter(se => se.fields.deviceId != null && se.fields.deviceId != (""))
        .uid("filterDevice").name("filterDevice")
        .keyBy(se => se.fields.deviceId)
        .process(new DistinctUserCountWindowFunction(
          CustomizedSessionWindowSettings(SessionGapMinutes = 60,
            RefreshIntervalMinutes =  50,
            ReportingIntervalSeconds = 10),
          GroupByKeys.deviceId
        ))
      unionStream = if (unionStream == null) deviceIdStream else unionStream.union(deviceIdStream)
    }

    if (Config.settings.enableCheckoutActionMetrics) {
      val checkoutActionStream = baseStream
              .filter(se => {
                se.name == "checkout_action_check"
    })
        .uid("checkoutEventsOnly").name("checkoutEventsOnly")
        .keyBy(se => se.fields.userId)
        .process(new CheckoutActionCheckAggregateCountWindowFunction(CustomizedSessionWindowSettings(
          SessionGapMinutes = 60 * 24,
          RefreshIntervalMinutes = 50,
          ReportingIntervalSeconds = 10,
          UseProcessingTimeTimers = true),
          GroupByKeys.userId))

      unionStream = if (unionStream == null) checkoutActionStream else unionStream.union(checkoutActionStream)
    }

    if (Config.settings.enablePaymentCountMetrics) {
      val paymentSuccessStream = baseStream
        .filter(se => {
          se.name == "product_payment" && se.fields.filter
        })
        .uid("paymentEventsOnly").name("paymentEventsOnly")
        .keyBy(se => se.fields.userId)
        .process(new AggregateCountWindowFunction(CustomizedSessionWindowSettings(
          SessionGapMinutes = 60 * 24,
          RefreshIntervalMinutes = 50,
          ReportingIntervalSeconds = 10,
          UseProcessingTimeTimers = true),
          GroupByKeys.userId))

      unionStream = if (unionStream == null) paymentSuccessStream else unionStream.union(paymentSuccessStream)
    }

    unionStream.addSink(Redis.redisSink()).name(sinkName).uid(sinkName)
    env.execute("Mockingbird")
  }
}
