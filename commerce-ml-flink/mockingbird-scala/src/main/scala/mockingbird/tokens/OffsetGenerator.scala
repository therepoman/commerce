package mockingbird.tokens

trait OffsetGenerator {
  def jitter(maxTimeMS: Int): Long
}

class RandomOffset extends OffsetGenerator {
  lazy val generator = new scala.util.Random()
  def jitter(maxTimeMS: Int): Long = generator.nextInt(maxTimeMS)
}

class FixedOffset extends OffsetGenerator {
  override def jitter(maxTimeMS: Int): Long = 5 * 1000 // 5 seconds
}

object Generator {
  var generator: OffsetGenerator = new RandomOffset()
}