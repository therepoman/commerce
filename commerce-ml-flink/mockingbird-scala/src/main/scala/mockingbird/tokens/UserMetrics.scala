package mockingbird.tokens

import scala.collection.mutable

case class UserMetrics (
                         var userId: String,
                         var nextSlideStart: Long,
                         var nextReport: Long,
                         var currentTimeSlot: Long,
                         var featureCount: mutable.Map[String, FeatureValue],
                         var report: mutable.Set[String],
                         var lastTimestamp: Long
                       ) {
  def this() {
    this("", 0, 0, 0, mutable.Map(), mutable.Set(), 0)
  }

  def expireOld(currentTimeSlot: Long, amountOfTimeSlots: Int): Boolean= {
    val modifiedFeatures = featureCount.keys.filter( token => featureCount(token).expireOld(currentTimeSlot, amountOfTimeSlots))
    modifiedFeatures.foreach( key => {
      if (featureCount(key).historicalValue.isEmpty) {
        featureCount -= key // remove key
      }
      report += key
    })
    modifiedFeatures.nonEmpty
  }

  def add(token: String, window: Long): Unit = {
    this.featureCount.get(token) match {
      case Some(fv: FeatureValue) => fv.add(window)
      case None =>
        val fv = new FeatureValue()
        fv.add(window)
        this.featureCount(token) = fv
    }
    this.report += token
  }
}