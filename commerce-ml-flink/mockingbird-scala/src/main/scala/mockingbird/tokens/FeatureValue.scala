package mockingbird.tokens

import scala.collection.mutable

case class FeatureValue (var currentValue: Int, var historicalValue: mutable.Map[Long, Int]) {
  def this() {
    this(0, mutable.Map())
  }
  def add(timeSlot: Long): Unit = {
    if (!historicalValue.contains(timeSlot)) {
      historicalValue.put(timeSlot, 1)
    } else {
      historicalValue(timeSlot) += 1
    }
    currentValue += 1
  }
  def expireOld(currentTimeSlot: Long, amountOfTimeSlots: Int): Boolean = {
    val timeSlotsToRemove = historicalValue.keys.filter(key => key < (currentTimeSlot - amountOfTimeSlots))

    timeSlotsToRemove.foreach( key => {
      currentValue -= historicalValue(key)
      historicalValue -= key
    })
    timeSlotsToRemove.nonEmpty
  }
}