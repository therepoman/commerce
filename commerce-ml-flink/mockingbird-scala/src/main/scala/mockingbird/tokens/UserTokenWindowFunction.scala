package mockingbird.tokens

import mockingbird.config.Config
import mockingbird.{Redis, Spade}
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.configuration.Configuration
import org.apache.flink.metrics.{Meter, MeterView}
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.util.Collector

import scala.collection.mutable
object CountTypes {
  type KeyType = String
  type ValueType = Spade.EventRecord
  type OutputType = Redis.RedisAction
}

/**
 * The implementation of the ProcessFunction that maintains the count and timeouts
 */
class UserTokenWindowFunction(windowSettings: UserTokenWindowSettings) extends KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] {

  @transient private var lateEventsCounter: Meter = _
  @transient private val LATE_EVENT_THRESHOLD_SEC = 30
  @transient private val meterFrequencySeconds = 60

  /** The state that is maintained by this process function */
  lazy val state: ValueState[UserMetrics] = getRuntimeContext
    .getState(new ValueStateDescriptor[UserMetrics]("ecState", classOf[UserMetrics]))

  override def open(parameters: Configuration): Unit = {
    lateEventsCounter = getRuntimeContext
      .getMetricGroup
      .addGroup("kinesisanalytics")
      .addGroup("Program", "mockingbird")
      .addGroup("env", Config.settings.envName)
      .meter("UserTokenLateEvents", new MeterView(meterFrequencySeconds))
  }


  def currentTime(
                   ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                 ): Long = {
    if (windowSettings.UseProcessingTimeTimers) {
      ctx.timerService.currentProcessingTime
    } else {
      ctx.timestamp
    }
  }

  def registerProcessingOrEventTimeTimer(
                                       ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                                       timestamp: Long
                                     ): Unit = {
    if (windowSettings.UseProcessingTimeTimers) {
      ctx.timerService.registerProcessingTimeTimer(timestamp)
    } else {
      ctx.timerService.registerEventTimeTimer(timestamp)

    }
  }

  override def processElement(
                               value: CountTypes.ValueType,
                               ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context,
                               out: Collector[CountTypes.OutputType]): Unit = {

    // initialize or retrieve/update the state
    val current: UserMetrics = state.value match {
      case null =>
        val jitter = Generator.generator.jitter(windowSettings.ReportingIntervalMs)
        val nextSlideStart = windowSettings.nextSlide(ctx.timestamp + jitter)
        val nextReport = windowSettings.nextReport(currentTime(ctx) + jitter)

        // schedule reporting the event in the next reporting interval
        registerProcessingOrEventTimeTimer(ctx, nextReport)
        // schedule reporting the event in the next window interval
        ctx.timerService.registerEventTimeTimer(nextSlideStart)

        UserMetrics(
          userId = value.fields.userId,
          nextReport = nextReport,
          nextSlideStart = nextSlideStart,
          currentTimeSlot = 0,
          featureCount = mutable.Map(),
          report = mutable.Set(),
          lastTimestamp = ctx.timestamp
        )
      case um: UserMetrics =>
        val diff = state.value.lastTimestamp - ctx.timestamp
        if (diff > LATE_EVENT_THRESHOLD_SEC) {
          lateEventsCounter.markEvent()
          return
        }
        um.lastTimestamp = ctx.timestamp
        um
    }
    current.add(value.fields.toUserToken, current.currentTimeSlot)

    if (ctx.timestamp >= current.nextReport) {
      val jitter = Generator.generator.jitter(windowSettings.ReportingIntervalMs)
      current.nextReport = windowSettings.nextReport(currentTime(ctx) + jitter)
      registerProcessingOrEventTimeTimer(ctx, current.nextReport)
    }

    // write the state back
    state.update(current)
  }

  override def onTimer(
                        timestamp: Long,
                        ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#OnTimerContext,
                        out: Collector[CountTypes.OutputType]): Unit = {

    state.value match {
      case um: UserMetrics =>
        if (timestamp >= um.nextSlideStart) {
          um.expireOld(um.currentTimeSlot, windowSettings.amountOfTimeSlots)
          um.nextSlideStart = windowSettings.nextSlide(um.nextSlideStart)
          um.currentTimeSlot += 1
          ctx.timerService.registerEventTimeTimer(um.nextSlideStart)
        }

        if (timestamp >= um.nextReport) {
          um.report.foreach(token => {
            if (um.featureCount.contains(token)) {
              out.collect(Redis.getUserFeatureUpdate(um.userId, token, um.featureCount(token).currentValue))
            } else {
              out.collect(Redis.getRemoveUserFeature(um.userId, token))
            }
          })
          um.report.clear
        }

        if (um.featureCount.isEmpty) {
          state.clear()
        } else {
          state.update(um)
        }

      case _ =>
    }
  }
}
