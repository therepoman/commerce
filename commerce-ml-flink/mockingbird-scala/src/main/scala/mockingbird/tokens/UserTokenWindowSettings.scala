package mockingbird.tokens

import java.util.Calendar

case class UserTokenWindowSettings(
                                           WindowSizeMinutes: Int,
                                           SlideSizeMinutes: Int,
                                           ReportingIntervalSeconds: Int,
                                           UseProcessingTimeTimers: Boolean = false
                                         ) {
  val SlideSizeMs: Int = SlideSizeMinutes * 60 * 1000
  val ReportingIntervalMs: Int = ReportingIntervalSeconds * 1000

  def nextReport(currentTimestamp: Long): Long = currentTimestamp + ReportingIntervalMs
  def nextSlide(currentTimestamp: Long): Long = currentTimestamp + SlideSizeMs
  def amountOfTimeSlots: Int = WindowSizeMinutes / SlideSizeMinutes
}