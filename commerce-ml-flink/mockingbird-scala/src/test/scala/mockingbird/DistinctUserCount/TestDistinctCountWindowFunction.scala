package mockingbird.DistinctUserCount

import mockingbird.Redis.RedisAction

import java.time.{Clock, Instant, LocalDate, ZoneOffset}
import mockingbird.count.{CustomizedSessionWindowSettings, DistinctUserCountWindowFunction}
import mockingbird.tokens._
import mockingbird.{Redis, Spade, Timing}
import org.apache.flink.api.common.typeinfo.Types
import org.apache.flink.streaming.api.operators.KeyedProcessOperator
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness
import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.collection.JavaConverters._


class TestDistinctCountWindowFunction extends AnyFlatSpec with Matchers with BeforeAndAfter {
  "window" should "add days correctly" in {
    val start = Instant.parse("2019-01-01T12:00:00Z").toEpochMilli

    val windowSettings = CustomizedSessionWindowSettings(
      SessionGapMinutes = 60,
      RefreshIntervalMinutes = 50,
      ReportingIntervalSeconds = 60
    )

    val nextReport = windowSettings.nextReport(start)
    val nextRefresh = windowSettings.nextRefresh(start)
    val sessionExpiration = windowSettings.nextSession(start)
    start should equal(Instant.parse("2019-01-01T12:00:00Z").toEpochMilli)
    nextReport should equal(Instant.parse("2019-01-01T12:01:00Z").toEpochMilli)
    nextRefresh should equal(Instant.parse("2019-01-01T12:50:00Z").toEpochMilli)
    sessionExpiration should equal((Instant.parse("2019-01-01T13:00:00Z").toEpochMilli))
  }

  private var testHarness: KeyedOneInputStreamOperatorTestHarness[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] = _
  private var LOCAL_DATE: LocalDate = LocalDate.of(1989, 1, 13)

  before {
    testHarness = new KeyedOneInputStreamOperatorTestHarness[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType](
      new KeyedProcessOperator(new DistinctUserCountWindowFunction(CustomizedSessionWindowSettings(
        SessionGapMinutes = 60,
        RefreshIntervalMinutes = 55,
        ReportingIntervalSeconds = 30,
      ), keyField = "ip")), v => v.fields.ip, Types.STRING
    )
    Timing.clock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneOffset.UTC).toInstant, ZoneOffset.UTC)
    Generator.generator = new FixedOffset()
  }

  after {
    Timing.restore()
  }

  class ItemsComparison {
    var expectedItems: List[RedisAction] = List()

    def addItem(expectedItem: RedisAction): Unit = {
      expectedItems = expectedItems :+ expectedItem
    }

    def compare(itemCount: Int, actualItems: Seq[RedisAction]): Unit = {
      val totalItems = expectedItems.length - itemCount - 1
      val thisExpectedItems = expectedItems.takeRight(itemCount)
      val thisActualItems = actualItems.takeRight(itemCount)

      thisExpectedItems.foreach(e => assert(thisActualItems.count(a => a == e) == 1))
    }
  }

  "DistinctCountWindowFunction" should "work" in {
    var timeStr = "2021-01-01T12:00:00Z"
    var time = Instant.parse(timeStr).toEpochMilli
    testHarness.open()
    testHarness.processWatermark(time)


    val expectedItems = new ItemsComparison()
    
    // 15 seconds in
    timeStr = "2021-01-01T12:00:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user1", "ip1", "deviceId1", timeStr, timeStr, "region1", "country1")), time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user1", "ip1", "deviceId1", timeStr, timeStr, "region1", "country1")), time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user2", "ip2", "deviceId2", timeStr, timeStr, "region2", "country2")), time)
    
    // 2 minutes in
    timeStr = "2021-01-01T12:02:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getFeatureUpdate("ip", "ip1", "user_count", 1))
    expectedItems.addItem(Redis.getFeatureUpdate("ip", "ip2", "user_count", 1))
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)

    // 1 hour in, the session isn't expired but got refreshed
    timeStr = "2021-01-01T12:56:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)

    expectedItems.addItem(Redis.getFeatureUpdate("ip", "ip1", "user_count", 1))
    expectedItems.addItem(Redis.getFeatureUpdate("ip", "ip2", "user_count", 1))
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)

    // 2 hours in, the session is expired
    timeStr = "2021-01-01T14:00:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getRemoveFeatureGroup("ip", "ip1"))
    expectedItems.addItem(Redis.getRemoveFeatureGroup("ip", "ip2"))
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)


    // 3 hours in, a new session starts
    timeStr = "2021-01-01T15:00:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user1", "ip1", "deviceId1", timeStr, timeStr, "region1", "country1")), time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user2", "ip1", "deviceId1", timeStr, timeStr, "region1", "country1")), time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user3", "ip2", "deviceId1", timeStr, timeStr, "region1", "country1")), time)


    // 3 hours and 10 minutes in
    timeStr = "2021-01-01T15:10:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getFeatureUpdate("ip", "ip1", "user_count", 2))
    expectedItems.addItem(Redis.getFeatureUpdate("ip", "ip2", "user_count", 1))
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)

  }

}