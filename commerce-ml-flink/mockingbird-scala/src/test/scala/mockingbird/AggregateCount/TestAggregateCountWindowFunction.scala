package mockingbird.AggregateCount

import java.time.{Clock, Instant, LocalDate, ZoneOffset}

import mockingbird.Redis.RedisAction
import mockingbird.count.{AggregateCountWindowFunction, CustomizedSessionWindowSettings}
import mockingbird.tokens.{CountTypes, _}
import mockingbird.{Redis, Spade, Timing}
import org.apache.flink.api.common.typeinfo.Types
import org.apache.flink.streaming.api.operators.KeyedProcessOperator
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness
import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.collection.JavaConverters._

class TestAggregateCountWindowFunction extends AnyFlatSpec with Matchers with BeforeAndAfter {
  private var testHarness: KeyedOneInputStreamOperatorTestHarness[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] = _
  private var LOCAL_DATE: LocalDate = LocalDate.of(1989, 1, 13)

  before {
    testHarness = new KeyedOneInputStreamOperatorTestHarness[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType](
      new KeyedProcessOperator(new AggregateCountWindowFunction(CustomizedSessionWindowSettings(
        SessionGapMinutes = 60 * 24,
        RefreshIntervalMinutes = 55,
        ReportingIntervalSeconds = 10,
      ), keyField = "user")), v => v.fields.userId, Types.STRING
    )
    Timing.clock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneOffset.UTC).toInstant, ZoneOffset.UTC)
    Generator.generator = new FixedOffset()
  }

  after {
    Timing.restore()
  }

  class ItemsComparison {
    var expectedItems: List[RedisAction] = List()

    def addItem(expectedItem: RedisAction): Unit = {
      expectedItems = expectedItems :+ expectedItem
    }

    def compare(itemCount: Int, actualItems: Seq[RedisAction]): Unit = {
      val totalItems = expectedItems.length - itemCount - 1
      val thisExpectedItems = expectedItems.takeRight(itemCount)
      val thisActualItems = actualItems.takeRight(itemCount)

//      thisActualItems.foreach(s => println(s))
//      println()
//      thisExpectedItems.foreach(s => println(s))
//      println()
//      println()

      assert(thisExpectedItems.sorted == thisActualItems.sorted)
    }
  }

  "AggregateCountWindowFunction" should "work" in {
    var timeStr = "2021-01-01T12:00:00Z"
    var time = Instant.parse(timeStr).toEpochMilli
    testHarness.open()
    testHarness.processWatermark(time)

    val expectedItems = new ItemsComparison()

    // 15 seconds in
    timeStr = "2021-01-01T12:00:15Z"
    time = Instant.parse(timeStr).toEpochMilli

    testHarness.processElement(Spade.EventRecord("product_payment", Spade.ProductPayment(timeStr, "user1", "checkout_session1", "platform")), time)
    testHarness.processElement(Spade.EventRecord("product_payment", Spade.ProductPayment(timeStr, "user1", "checkout_session1", "platform")), time)
    testHarness.processElement(Spade.EventRecord("product_payment", Spade.ProductPayment(timeStr, "user2", "checkout_session1", "platform")), time)

    // 2 minutes in
    timeStr = "2021-01-01T12:02:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "payment_count", 2))
    expectedItems.addItem(Redis.getUserFeatureUpdate("user2", "payment_count", 1))

    expectedItems.compare(2, testHarness.extractOutputValues.asScala)

    // 1 hour in, the session isn't expired but got refreshed
    timeStr = "2021-01-01T12:56:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)

    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "payment_count", 2))
    expectedItems.addItem(Redis.getUserFeatureUpdate("user2", "payment_count", 1))
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)

    // 25 hours in, the session is expired
    timeStr = "2021-01-02T13:00:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getRemoveUserFeature("user1", "payment_count"))
    expectedItems.addItem(Redis.getRemoveUserFeature("user2", "payment_count"))
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)


    // 15 minutes in for a new day
    timeStr = "2021-01-02T14:15:00Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processElement(Spade.EventRecord("product_payment", Spade.ProductPayment(timeStr, "user1", "checkout_session1", "platform")), time)
    timeStr = "2021-01-02T14:15:10Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processElement(Spade.EventRecord("product_payment", Spade.ProductPayment(timeStr, "user1", "checkout_session1", "platform")), time)

    // 20 minutes in
    timeStr = "2021-01-02T14:20:00Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "payment_count", 2))
    expectedItems.compare(1, testHarness.extractOutputValues.asScala)

    // 24 hours in
    timeStr = "2021-01-03T14:10:00Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processElement(Spade.EventRecord("product_payment", Spade.ProductPayment(timeStr, "user1", "checkout_session1", "platform")), time)
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "payment_count", 3)) // once from the new addition
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "payment_count", 3)) // once from the session refresh

    timeStr = "2021-01-03T14:30:00Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.compare(2, testHarness.extractOutputValues.asScala)

  }
}
