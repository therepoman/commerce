package mockingbird

import org.apache.flink.streaming.api.scala._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TestKinesis extends AnyFlatSpec with Matchers {
  "flattenCompressedStream" should "convert a stream into array of records" in {
    val mwEvent = Spade.MinuteWatchedEvent(userId = "user1", channelId = "channel1", timeUtc = "", clientTimeUtc = "",
    deviceId = "", ip = "", region = "", country = "")
    // prepare data
    val mw = Spade.EventRecord(name="minute-watched", fields=Spade.MinuteWatchedEvent("channel1", "user1", "", "", "", "", "", ""))
    val mwJson = JsonMapper.mapper.writeValueAsString(Seq(mw))
    val compressedRecord = KinesisCompression.compress(mwJson)
    val kinesisEventJson = JsonMapper.mapper.writeValueAsString(Kinesis.KinesisRecord(data=compressedRecord))

    // prepare stream
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val dataStream: DataStream[String] = env.fromElements(kinesisEventJson)

    // test
    val outputDataStream: DataStream[Spade.EventRecord] = Kinesis.flattenCompressedStream(dataStream)
    //val output = outputDataStream.executeAndCollect().toArray
    //output.length should equal(1)
    //output(0).name should equal ("minute-watched")
  }
}
