package mockingbird

import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.time.temporal.{ChronoField}

class TestSpade extends AnyFlatSpec with Matchers with BeforeAndAfter {

  "DTF" should "properly parse client time" in {
    val tm = Spade.DTF.parse("2021-08-18 21:51:04")
    tm.get(ChronoField.YEAR) should equal(2021)

    val mw = new Spade.MinuteWatchedEvent()
    mw.clientTimeUtc = "2021-08-18 20:55:30.994"
    mw.clientTime should equal(1629320130000L)
  }
}
