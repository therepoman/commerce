package mockingbird.tokens

import mockingbird.Redis.RedisAction
import mockingbird.{Redis, Spade, Timing}
import org.apache.flink.api.common.typeinfo.Types
import org.apache.flink.streaming.api.operators.KeyedProcessOperator
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness
import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.time.{Clock, Instant, LocalDate, ZoneOffset}
import scala.collection.JavaConverters._


class TestUserTokenWindowFunction extends AnyFlatSpec with Matchers with BeforeAndAfter {
  "windows" should "add days correctly" in {
    val start = Instant.parse("2019-01-01T12:17:15Z").toEpochMilli

    val windowSettings = UserTokenWindowSettings(
      WindowSizeMinutes = 60,
      SlideSizeMinutes = 10,
      ReportingIntervalSeconds = 5 * 60,
    )

    val nextReport = windowSettings.nextReport(start)
    val nextSlide = windowSettings.nextSlide(start)
    start should equal(Instant.parse("2019-01-01T12:17:15Z").toEpochMilli)
    nextReport should equal(Instant.parse("2019-01-01T12:22:15Z").toEpochMilli)
    nextSlide should equal(Instant.parse("2019-01-01T12:27:15Z").toEpochMilli)
  }

  private var testHarness: KeyedOneInputStreamOperatorTestHarness[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] = _
  private val LOCAL_DATE: LocalDate = LocalDate.of(1989, 1, 13)

  before {
    testHarness = new KeyedOneInputStreamOperatorTestHarness[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType](
      new KeyedProcessOperator(new UserTokenWindowFunction(UserTokenWindowSettings(
        WindowSizeMinutes = 60,
        SlideSizeMinutes = 10,
        ReportingIntervalSeconds = 5 * 60,
      ))), v => v.fields.userId, Types.STRING
    )
    Timing.clock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneOffset.UTC).toInstant, ZoneOffset.UTC)
    Generator.generator = new FixedOffset()
  }

  after {
    Timing.restore()
  }

  class ItemsComparison {
    var expectedItems: List[RedisAction] = List()

    def addItem(expectedItem: RedisAction): Unit = {
      expectedItems = expectedItems :+ expectedItem
    }

    def compare(actualItems: Seq[RedisAction]): Unit = {
      actualItems.length should equal(expectedItems.size)
      actualItems.indices.foreach(i => actualItems(i) should equal(expectedItems(i)))
    }
  }

  "UserTokenWindowFunction" should "work" in {
    var timeStr = "2021-01-01T12:00:00Z"
    var time = Instant.parse(timeStr).toEpochMilli
    testHarness.open()
    testHarness.processWatermark(time)

    val expectedItems = new ItemsComparison()

    // add 15 seconds
    timeStr = "2021-01-01T12:00:15Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user1", "ip1", "deviceId1", timeStr, timeStr, "region1", "country1")), time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user1", "ip1", "deviceId1", timeStr, timeStr, "region1", "country1")), time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user2", "ip2", "deviceId2", timeStr, timeStr, "region2", "country2")), time)

    // add 5 minutes
    timeStr = "2021-01-01T12:05:40Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)

    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "mw:channel1", 2))
    expectedItems.addItem(Redis.getUserFeatureUpdate("user2", "mw:channel1", 1))

    expectedItems.compare(testHarness.extractOutputValues.asScala)
    testHarness.processElement(Spade.EventRecord("chat", Spade.Chat("channel1", "user1", "ip2", "deviceId2", timeStr, timeStr, "region2", "country2")), time)

    // add 5 minutes
    timeStr = "2021-01-01T12:10:45Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "ch:channel1", 1))
    expectedItems.compare(testHarness.extractOutputValues.asScala)

    // add more items
    timeStr = "2021-01-01T12:20:40Z"
    time = Instant.parse(timeStr).toEpochMilli
    testHarness.processWatermark(time)
    testHarness.processElement(Spade.EventRecord("minute-watch", Spade.MinuteWatchedEvent("channel1", "user1", "ip2", "deviceId2", timeStr, timeStr, "region2", "country2")), time)
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "mw:channel1", 3))

    time = Instant.parse("2021-01-01T12:30:40Z").toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.compare(testHarness.extractOutputValues.asScala)

    // add 1 hour
    time = Instant.parse("2021-01-01T12:40:40Z").toEpochMilli
    testHarness.processWatermark(time)
    time = Instant.parse("2021-01-01T12:50:40Z").toEpochMilli
    testHarness.processWatermark(time)
    time = Instant.parse("2021-01-01T13:00:40Z").toEpochMilli
    testHarness.processWatermark(time)
    time = Instant.parse("2021-01-01T13:10:40Z").toEpochMilli
    testHarness.processWatermark(time)
    time = Instant.parse("2021-01-01T13:20:40Z").toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getUserFeatureUpdate("user1", "mw:channel1", 1))
    expectedItems.addItem(Redis.getRemoveUserFeature("user1", "ch:channel1"))
    expectedItems.addItem(Redis.getRemoveUserFeature("user2", "mw:channel1"))
    expectedItems.compare(testHarness.extractOutputValues.asScala)

    // add 10 minutes
    time = Instant.parse("2021-01-01T13:40:40Z").toEpochMilli
    testHarness.processWatermark(time)
    expectedItems.addItem(Redis.getRemoveUserFeature("user1", "mw:channel1"))
    expectedItems.compare(testHarness.extractOutputValues.asScala)


    testHarness.numKeyedStateEntries() should equal(0)
  }
}
