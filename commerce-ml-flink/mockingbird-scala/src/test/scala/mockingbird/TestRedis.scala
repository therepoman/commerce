package mockingbird


import mockingbird.Redis.RedisAction
import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.time.{Clock, LocalDate, ZoneOffset}

class TestRedis extends AnyFlatSpec with Matchers with BeforeAndAfter {

  val LOCAL_DATE: LocalDate = LocalDate.of(1989, 1, 13)

  before {
    Timing.clock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneOffset.UTC).toInstant, ZoneOffset.UTC)
  }

  after {
    Timing.restore()
  }

  "Redis output" should "match the format" in {
    val output = Redis.getFeatureUpdate("user", "user1", "ch:1", 5)
    val env: String = config.Config.settings.envName
    output should equal (RedisAction(
      command = RedisOps.HSET,
      key = f"user-${env}:user1",
      fieldKey = "ch:1",
      value = "5,600652800"
    ))
  }

  "Redis deprecated output" should "match the deprecated format" in {
    val output = Redis.getUserFeatureUpdate("user1", "ch:1", 5)
    output should equal (RedisAction(
      command = RedisOps.HSET,
      key = f"u3:user1",
      fieldKey = "ch:1",
      value = "5,600652800"
    ))
  }
}
