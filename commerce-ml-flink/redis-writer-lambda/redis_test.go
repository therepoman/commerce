package main

import (
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-writer/clients/clientsfakes"
	"context"
	"github.com/go-redis/redis/v8"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type FakeRedisClient struct {
	Pipeliner redis.Pipeliner
}

func (c FakeRedisClient) Pipelined(ctx context.Context, fn func(redis.Pipeliner) error) ([]redis.Cmder, error) {
	return nil, fn(c.Pipeliner)
}

func TestRedis(t *testing.T) {
	Convey("given record with ttl", t, func() {
		ctx := context.Background()
		fakePipeliner := &clientsfakes.FakePipeliner{}

		err := pipelineRedisCommands(ctx, FakeRedisClient{Pipeliner: fakePipeliner}, []RedisAction{
			{
				Command:       "HSET",
				Key:           "testkey",
				Value:         5,
				AdditionalKey: "fkey1",
			},{
				Command:       "HSET",
				Key:           "testkey",
				Value:         5,
				AdditionalKey: "fkey2",
				TtlSeconds:    0,
			},
		})
		So(err, ShouldBeNil)
		So(fakePipeliner.HSetCallCount(), ShouldEqual, 2)
		_, key, args := fakePipeliner.HSetArgsForCall(0)
		So(key, ShouldEqual, "testkey")
		So(args[0], ShouldEqual, "fkey1")
		So(args[1], ShouldEqual, 5)
	})
}