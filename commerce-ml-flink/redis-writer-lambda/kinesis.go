package main

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/awslabs/kinesis-aggregation/go/deaggregator"
)

func getRedisActionsFromRedis(records []events.KinesisEventRecord) ([]RedisAction, error)  {
	kinesisRecords := make([]*kinesis.Record, 0)
	for _, record := range records {
		record := record
		kinesisRecords = append(kinesisRecords, &kinesis.Record{
			ApproximateArrivalTimestamp: &record.Kinesis.ApproximateArrivalTimestamp.Time,
			Data:                        record.Kinesis.Data,
			EncryptionType:              &record.Kinesis.EncryptionType,
			PartitionKey:                &record.Kinesis.PartitionKey,
			SequenceNumber:              &record.Kinesis.SequenceNumber,
		})
	}
	deagg, err := deaggregator.DeaggregateRecords(kinesisRecords)
	if err != nil {
		return nil, fmt.Errorf("failed to de-aggregate records: %s", err.Error())
	}
	actions := make([]RedisAction, len(deagg))
	for i, r := range deagg {
		err := json.Unmarshal(r.Data, &actions[i])
		if err != nil {
			return nil, fmt.Errorf("Failed to json unmarshal kinesis event: %s, err: %s", string(r.Data), err.Error())
		}
	}
	return actions, nil
}