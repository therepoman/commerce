package main

import (
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-writer/clients"
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
)

type RedisCommand string

const(
	HSET RedisCommand = "HSET"
	SET RedisCommand = "SET"
	ZADD RedisCommand = "ZADD"
	ZTRIM RedisCommand = "ZTRIM"
)

type RedisAction struct {
	Command RedisCommand `json:"command"`
	Key string  `json:"key"`
	Value interface{} `json:"value"`
	AdditionalKey string `json:"additional_key"`
	TtlSeconds int `json:"ttl_seconds"`
}

func pipelineRedisCommands(ctx context.Context, redisClient clients.Redis, actions []RedisAction) error {
	cmds, err := redisClient.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, action := range actions {
			if action.Command == HSET {
				pipe.HSet(ctx, action.Key, action.AdditionalKey, action.Value)
			} else if action.Command == "HDEL" {
				pipe.HDel(ctx, action.Key, action.AdditionalKey)
			} else if action.Command == "DEL" {
				pipe.Del(ctx, action.Key)
			} else {
				fmt.Printf("Error: invalid command: %s", string(action.Command))
			}
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed to run Redis pipeline: %s", err.Error())
	}
	for _, cmd := range cmds {
		if cmd.Err() != nil {
			return fmt.Errorf("redis command we ran failed: %s", cmd.Err().Error())
		}
	}
	return nil
}