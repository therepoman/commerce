package main

import (
	"code.justin.tv/commerce/commerce-ml-flink/redis-lambda-writer/clients"
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"os"
)

type LambdaHandler struct {
	Redis          clients.Redis
}

func (l *LambdaHandler) Setup() error {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	if host == "" || port == "" {
		fmt.Printf("Missing host or port")
		os.Exit(1)
	}

	l.Redis = clients.NewRedis(host, port)
	fmt.Printf("Connecting to %s:%s\n", host, port)

	return nil
}

func (l *LambdaHandler) HandleRequest(ctx context.Context, event events.KinesisEvent) (string, error) {
	actions, err := getRedisActionsFromRedis(event.Records)
	if err != nil {
		return "", err
	}
	err = pipelineRedisCommands(ctx, l.Redis, actions)
	return "", err
}

func main() {
	l := LambdaHandler{}
	if err := l.Setup(); err != nil {
		fmt.Printf("failed to initialize lambda: %s", err.Error())
		os.Exit(1)
	}
	lambda.Start(l.HandleRequest)
}
