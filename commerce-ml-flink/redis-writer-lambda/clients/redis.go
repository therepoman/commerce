package clients

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

//counterfeiter:generate github.com/go-redis/redis/v8.Pipeliner
//counterfeiter:generate . Redis
type Redis interface {
	Pipelined(ctx context.Context, fn func(redis.Pipeliner) error) ([]redis.Cmder, error)
}

func NewRedis(host string, port string) Redis {
	client := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: []string{fmt.Sprintf("%s:%s", host, port)},
		WriteTimeout: 1 * time.Minute,
	})

	fmt.Printf("Connecting to %s:%s\n", host, port)

	return client
}
