module code.justin.tv/commerce/commerce-ml-flink/redis-lambda-writer

go 1.15

require (
	github.com/aws/aws-lambda-go v1.22.0
	github.com/aws/aws-sdk-go v1.19.48
	github.com/awslabs/kinesis-aggregation/go v0.0.0-20210222131425-398fbd4b430d
	github.com/go-redis/redis/v8 v8.7.1
	github.com/maxbrunsfeld/counterfeiter/v6 v6.4.1
	github.com/smartystreets/goconvey v1.6.4
)
