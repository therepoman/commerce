# Commerce Real-Time Machine Learning Infrastructure

- [Runbook](https://docs.google.com/document/d/1sAhfHFvBd7HbV4vH5YSpuM-Z3jKzHJOBAdaBksWAjnE/edit#)
- [Cloudwatch Dashboard (Midway Login)](https://cloudwatch.amazonaws.com/dashboard.html?dashboard=flink-prod&context=eyJSIjoidXMt[…]LVJlYWRPbmx5QWNjZXNzLUFMTC1DRjdIMTFQMCIsIk0iOiJTU08ifQ==)
- [Catalog](https://catalog.xarth.tv/services/901/details)

This has multiple components, each one is described below.

## CDK Infrastructure
The 'cdk' folder contains the code to spin up the infrastructure using AWS CDK.

## Apache Flink - Mockingbird job
This is Purchase Reputation real-time data collection Flink job.

## Redis Lambda Reader
The Reader allows Personalization Service to read from Redis cross account.

# Deployment
The long term-goal is to have all packages deployed using Jenkins. For now only the lambads and the cloudwatch cdk stack are deployed automatically.

## Deploying a new version of mockingbird
Note: KDA does not always manage to seamlessly update flink jobs from snapshot. We should update the Flink job infrequently for now until we improved this situation.

* Run `make build-flink-job`, then `make upload-flink-s3` - take note of the filename that was uploaded to s3.
* Change the filename here: `cdk/lib/stacks/flink-kda-stack.ts` to match the new date/time of the upload:

```code
kda.ApplicationCode.fromBucket(s3.Bucket.fromBucketName(stack, "flink-bucket", "commerce-ml-flink-prod"), 'mockingbird-assembly-0.3-SNAPSHOT-2021-04-21.21:36:39.jar'),
```

* Run `make deploy_kda_prod` and monitor the Spade Consumption dashboard, and the Flink dashboard. If the job managed to resume and is running, then all is well. if not, manually restart using the information below.

## Manually restart mockingbird

