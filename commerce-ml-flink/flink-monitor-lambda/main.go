package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	emf "code.justin.tv/amzn/TwitchTelemetryCloudWatchEMFSender"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch/types"
	"github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	Env           string
	Cloudwatch    *cloudwatch.Client
	cloudwatchEMF *telemetry.SampleReporter
}

func (l *lambdaHandler) Setup() error {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		log.Fatal("unable to load SDK config: ", err)
	}
	l.Cloudwatch = cloudwatch.NewFromConfig(cfg)

	tPid := &identifier.ProcessIdentifier{
		Service:  "CommerceMLFlink",
		Stage:    "prod",
		Substage: "Primary",
		Region:   "us-west-2",
		Machine:  "flink-monitor-lambda",
	}

	sampleObserver := emf.New(tPid.Service, os.Stdout)
	l.cloudwatchEMF = &telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: *tPid},
		SampleObserver: sampleObserver,
	}

	return nil
}

func (l *lambdaHandler) reportFlink(ctx context.Context) error {
	startTime := time.Now().Add(-6 * time.Minute)
	endTime := time.Now().Add(-5 * time.Minute)
	metricData, err := l.Cloudwatch.GetMetricData(ctx, &cloudwatch.GetMetricDataInput{
		StartTime: &startTime,
		EndTime:   &endTime,
		MetricDataQueries: []types.MetricDataQuery{
			{
				Id: aws.String("downtime"),
				MetricStat: &types.MetricStat{

					Metric: &types.Metric{
						Namespace:  aws.String("AWS/KinesisAnalytics"),
						MetricName: aws.String("downtime"),
						Dimensions: []types.Dimension{{
							Name:  aws.String("Application"),
							Value: aws.String("mockingbird790E27E_giaU2gjyXYCv"),
						}},
					},
					Period: aws.Int32(60),
					Stat:   aws.String("Maximum"),
				},
			}, {
				Id: aws.String("iteratorage"),
				MetricStat: &types.MetricStat{

					Metric: &types.Metric{
						Namespace:  aws.String("AWS/Kinesis"),
						MetricName: aws.String("GetRecords.IteratorAgeMilliseconds"),
						Dimensions: []types.Dimension{{
							Name:  aws.String("StreamName"),
							Value: aws.String("spade-downstream-prod-live-commerce-mw-prod"),
						}},
					},
					Period: aws.Int32(60),
					Stat:   aws.String("Maximum"),
				},
			},
		},
	})

	if err != nil {
		return err
	}

	var iteratorAge, downtime float64

	for _, data := range metricData.MetricDataResults {
		id := *data.Id
		if id == "iteratorage" {
			iteratorAge = data.Values[0]
		} else if id == "downtime" {
			downtime = data.Values[0]
		} else {
			return fmt.Errorf("received unknown metric: %s", id)
		}
	}

	l.cloudwatchEMF.Timestamp = metricData.MetricDataResults[0].Timestamps[0]
	l.cloudwatchEMF.OperationName = "Flink Uptime"

	if iteratorAge < 5000 && downtime == 0 {
		l.cloudwatchEMF.Report(telemetry.MetricSuccess, 1, telemetry.UnitCount)
		l.cloudwatchEMF.Report(telemetry.MetricServerError, 0, telemetry.UnitCount)
	} else {
		l.cloudwatchEMF.Report(telemetry.MetricSuccess, 0, telemetry.UnitCount)
		l.cloudwatchEMF.Report(telemetry.MetricServerError, 1, telemetry.UnitCount)
	}
	l.cloudwatchEMF.Report(telemetry.MetricClientError, 0, telemetry.UnitCount)

	l.cloudwatchEMF.Flush()
	return nil
}

func (l *lambdaHandler) reportSagemaker(ctx context.Context) error {
	startTime := time.Now().Add(-6 * time.Minute)
	endTime := time.Now().Add(-5 * time.Minute)
	metricData, err := l.Cloudwatch.GetMetricData(ctx, &cloudwatch.GetMetricDataInput{
		StartTime: &startTime,
		EndTime:   &endTime,
		MetricDataQueries: []types.MetricDataQuery{
			{
				Id: aws.String("p99"),
				MetricStat: &types.MetricStat{
					Metric: &types.Metric{
						Namespace:  aws.String("AWS/SageMaker"),
						MetricName: aws.String("ModelLatency"),
						Dimensions: []types.Dimension{{
							Name:  aws.String("EndpointName"),
							Value: aws.String("nominis-master-prod-purrep-realtime"),
						}, {
							Name:  aws.String("VariantName"),
							Value: aws.String("main"),
						}},
					},
					Period: aws.Int32(60),
					Stat:   aws.String("p99"),
					Unit:   types.StandardUnitMicroseconds,
				},
			}, {
				Id: aws.String("serverError"),
				MetricStat: &types.MetricStat{
					Metric: &types.Metric{
						Namespace:  aws.String("AWS/SageMaker"),
						MetricName: aws.String("Invocation5XXErrors"),
						Dimensions: []types.Dimension{{
							Name:  aws.String("EndpointName"),
							Value: aws.String("nominis-master-prod-purrep-realtime"),
						}, {
							Name:  aws.String("VariantName"),
							Value: aws.String("main"),
						}},
					},
					Period: aws.Int32(60),
					Stat:   aws.String("Sum"),
				},
			}, {
				Id: aws.String("clientError"),
				MetricStat: &types.MetricStat{
					Metric: &types.Metric{
						Namespace:  aws.String("AWS/SageMaker"),
						MetricName: aws.String("Invocation4XXErrors"),
						Dimensions: []types.Dimension{{
							Name:  aws.String("EndpointName"),
							Value: aws.String("nominis-master-prod-purrep-realtime"),
						}, {
							Name:  aws.String("VariantName"),
							Value: aws.String("main"),
						}},
					},
					Period: aws.Int32(60),
					Stat:   aws.String("Sum"),
				},
			}, {
				Id: aws.String("inv"),
				MetricStat: &types.MetricStat{
					Metric: &types.Metric{
						Namespace:  aws.String("AWS/SageMaker"),
						MetricName: aws.String("Invocations"),
						Dimensions: []types.Dimension{{
							Name:  aws.String("EndpointName"),
							Value: aws.String("nominis-master-prod-purrep-realtime"),
						}, {
							Name:  aws.String("VariantName"),
							Value: aws.String("main"),
						}},
					},
					Period: aws.Int32(60),
					Stat:   aws.String("Sum"),
				},
			},
		},
	})

	if err != nil {
		return err
	}

	var serverErrors, clientErrors, p99, inv float64
	for _, data := range metricData.MetricDataResults {
		for _, value := range data.Values {
			id := *data.Id
			if id == "p99" {
				p99 = value / 1000.0 / 1000.0
			} else if id == "inv" {
				inv = value
			} else if id == "serverError" {
				serverErrors = value
			} else if id == "clientError" {
				clientErrors = value
			} else {
				return fmt.Errorf("received unknown metric: %s", id)
			}
		}
	}

	l.cloudwatchEMF.Timestamp = metricData.MetricDataResults[0].Timestamps[0]
	l.cloudwatchEMF.OperationName = "Sagemaker"

	l.cloudwatchEMF.Report(telemetry.MetricSuccess, inv-serverErrors-clientErrors, telemetry.UnitCount)
	l.cloudwatchEMF.Report(telemetry.MetricServerError, serverErrors, telemetry.UnitCount)
	l.cloudwatchEMF.Report(telemetry.MetricDuration, p99, telemetry.UnitSeconds)
	l.cloudwatchEMF.Report(telemetry.MetricClientError, clientErrors, telemetry.UnitCount)

	l.cloudwatchEMF.Flush()
	return nil
}

func (l *lambdaHandler) HandleMonitor(ctx context.Context) error {
	if err := l.reportFlink(ctx); err != nil {
		return err
	}

	if err := l.reportSagemaker(ctx); err != nil {
		return err
	}

	return nil
}

func (l *lambdaHandler) HandleRequest(ctx context.Context, r interface{}) (interface{}, error) {
	return "", l.HandleMonitor(ctx)
}

func main() {
	l := lambdaHandler{}
	if err := l.Setup(); err != nil {
		fmt.Printf("failed to initialize lambda: %s", err.Error())
		os.Exit(1)
	}
	if len(os.Args) == 1 {
		lambda.Start(l.HandleRequest)
	} else {
		logrus.SetOutput(os.Stdout)
		err := l.HandleMonitor(context.Background())
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			os.Exit(1)
		}
	}
}
