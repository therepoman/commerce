module code.justin.tv/commerce/commerce-ml-flink/flink-monitor-lambda

go 1.15

require (
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f // indirect
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20210505000720-0c0013548d03
	code.justin.tv/amzn/TwitchTelemetryCloudWatchEMFSender v0.0.0-20210701221039-26de46a71485
	github.com/aws/aws-lambda-go v1.22.0
	github.com/aws/aws-sdk-go-v2 v1.3.2
	github.com/aws/aws-sdk-go-v2/config v1.1.6
	github.com/aws/aws-sdk-go-v2/service/cloudwatch v1.3.1
	github.com/awslabs/kinesis-aggregation/go v0.0.0-20210222131425-398fbd4b430d // indirect
	github.com/go-redis/redis/v8 v8.7.1 // indirect
	github.com/maxbrunsfeld/counterfeiter/v6 v6.2.3
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
)
