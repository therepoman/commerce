package telemetry

import (
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
)

// NewNoOpSampleReporter creates a SampleReporter configured to do nothing.
// It is suitable for use in unit tests, where components require a
// SampleReporter but metrics do not need to be collected as part of the test.
func NewNoOpSampleReporter() *SampleReporter {
	return &SampleReporter{
		SampleBuilder: SampleBuilder{
			ProcessIdentifier: identifier.ProcessIdentifier{
				LaunchID: "noop",
				Machine:  "noop",
				Region:   "noop",
				Service:  "noop",
				Stage:    "noop",
				Substage: "noop",
				Version:  "noop",
			},
			Dimensions: DimensionSet{},
		},
		SampleObserver: &noOpSampleObserver{},
		Logger:         &noOpLogger{},
	}
}

type noOpSampleObserver struct {
}

func (o *noOpSampleObserver) ObserveSample(sample *Sample) {}

func (o *noOpSampleObserver) Flush() {}

func (o *noOpSampleObserver) Stop() {}

type noOpLogger struct{}

func (l *noOpLogger) Log(msg string, keyvals ...interface{}) {}
