package TwitchTelemetryCloudWatchEMFSender

import (
	"io"
	"sync/atomic"
	"time"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/amzn/TwitchTelemetryCloudWatchEMFSender/logger"
)

// Logger represents the interface that defines an EMF logger
type Logger interface {
	PutMetric(sample *telemetry.Sample)
	Flush()
}

// CloudWatchEMFSender implements a Twitch Telemetry Sample Observer for emitting log-based metrics to CloudWatch
type CloudWatchEMFSender struct {
	logger  Logger
	stopped uint32
}

// New creates a thread-safe CloudWatchEMF sender
func New(service string, sink io.Writer) *CloudWatchEMFSender {
	emfLogger := logger.New(sink, time.Minute, service)
	return &CloudWatchEMFSender{
		logger: emfLogger,
	}
}

// ObserveSample batches the given twitch telemetry sample
func (sender *CloudWatchEMFSender) ObserveSample(sample *telemetry.Sample) {
	if !sender.isStopped() {
		sender.logger.PutMetric(sample)
	}
}

// Stop causes all subsequent samples to be ignored
func (sender *CloudWatchEMFSender) Stop() {
	sender.Flush()
	atomic.StoreUint32(&sender.stopped, 1)
}

// Flush flushes the logger
func (sender *CloudWatchEMFSender) Flush() {
	sender.logger.Flush()
}

// isStopped returns true if the sender is stopped, returns false otherwise
func (sender *CloudWatchEMFSender) isStopped() bool {
	return sender.stopped == 1
}
