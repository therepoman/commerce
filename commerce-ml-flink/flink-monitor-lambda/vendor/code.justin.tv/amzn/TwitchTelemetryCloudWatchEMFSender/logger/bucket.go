package logger

import (
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"encoding/json"
	"sort"
	"time"
)

// bucket stores twitch telemetry samples with the same truncated timestamp, dimensions and rollup dimensions
// bucket stores all the data required to generate one EMF log statement
type bucket struct {
	metrics          map[string][]telemetry.Sample
	metricKeys       []string
	dimensions       map[string]string
	rollupDimensions [][]string
	timestamp        int64
}

// buckets maps all active buckets with their corresponding keys
type buckets map[string]*bucket

// newBucket initializes a new bucket with the given sample and time resolution.
func newBucket(s *telemetry.Sample, resolution time.Duration) *bucket {
	metrics := make(map[string][]telemetry.Sample)
	metrics[s.MetricID.Name] = []telemetry.Sample{*s}
	return &bucket{
		metrics:          metrics,
		metricKeys:       []string{s.MetricID.Name},
		dimensions:       s.MetricID.Dimensions,
		rollupDimensions: s.RollupDimensions,
		timestamp:        s.Timestamp.Truncate(resolution).Unix() * millisecondsInASecond,
	}
}

// putMetric adds the given sample to the bucket.
func (bucket *bucket) putMetric(sample *telemetry.Sample) {
	metricName := sample.MetricID.Name
	datums, exist := bucket.metrics[metricName]
	if exist {
		bucket.metrics[metricName] = append(datums, *sample)
	} else {
		bucket.metrics[metricName] = []telemetry.Sample{*sample}
		bucket.metricKeys = append(bucket.metricKeys, metricName)
	}
}

// serialize serializes the bucket into one EMF log statement
func (bucket *bucket) serialize(namespace string) []byte {
	root := make(map[string]interface{})
	root[emfMetadataLabel] = _aws{
		Timestamp:         bucket.timestamp,
		CloudWatchMetrics: bucket.generateCloudWatchMetricDirectives(namespace),
	}
	for key, value := range bucket.dimensions {
		root[key] = value
	}
	for metricName, metrics := range bucket.metrics {
		metricValues := make([]float64, 0, len(metrics))
		for _, metric := range metrics {
			metricValues = append(metricValues, metric.Value)
		}
		root[metricName] = metricValues
	}
	log, _ := json.Marshal(root)
	return log
}

// numberOfUniqueMetrics returns the number of unique metrics present in the bucket
// Uniqueness is determined by the name of the metric
func (bucket *bucket) numberOfUniqueMetrics() int {
	return len(bucket.metricKeys)
}

func (bucket *bucket) numberOfMetricValues(metricName string) int {
	metricValues, exists := bucket.metrics[metricName]
	if !exists {
		return 0
	}
	return len(metricValues)
}

// generateCloudWatchDimensionSetArray generates the CloudWatch DimensionSet array for the bucket
func (bucket *bucket) generateCloudWatchDimensionSetArray() [][]string {
	if len(bucket.dimensions) == 0 {
		return [][]string{}
	}

	var allDimensionKeys []string
	for dimensionKey, _ := range bucket.dimensions {
		allDimensionKeys = append(allDimensionKeys, dimensionKey)
	}
	sort.Strings(allDimensionKeys)

	dimensionSetArray := [][]string{allDimensionKeys}
	dimensionSet := make(map[string]bool, len(allDimensionKeys))

	// populate dimension set array based on rollup dimensions
	for _, rollup := range bucket.rollupDimensions {
		for _, key := range allDimensionKeys {
			dimensionSet[key] = true
		}
		for _, key := range rollup {
			dimensionSet[key] = false
		}
		dimensions := make([]string, 0, len(allDimensionKeys))

		for _, key := range allDimensionKeys {
			includeKey := dimensionSet[key]
			if !includeKey {
				continue
			}
			dimensions = append(dimensions, key)
		}

		if len(dimensions) > 0 {
			dimensionSetArray = append(dimensionSetArray, dimensions)
		}
	}
	return dimensionSetArray
}

// generateCloudWatchMetricDirectives generates the CloudWatch MetricDirectives array for the bucket
// The length of the MetricDirective slice it returns is always one
func (bucket *bucket) generateCloudWatchMetricDirectives(namespace string) []metricDirective {
	// generate metric definitions
	var metricDefinitions []metricDefinition
	for _, key := range bucket.metricKeys {
		metrics := bucket.metrics[key]
		metricDefinitions = append(metricDefinitions, metricDefinition{
			Name: metrics[0].MetricID.Name,
			Unit: metrics[0].Unit,
		})
	}

	// generate CloudWatch DimensionSet Array from rollup
	dimensionSetArray := bucket.generateCloudWatchDimensionSetArray()
	directive := metricDirective{
		Namespace:  namespace,
		Dimensions: dimensionSetArray,
		Metrics:    metricDefinitions,
	}

	return []metricDirective{directive}
}
