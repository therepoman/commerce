package logger

const (
	maxDimensions        = 9
	maxMetricDefinitions = 100
	maxMetricValues      = 100
	emfMetadataLabel     = "_aws"
)

type _aws struct {
	Timestamp         int64
	CloudWatchMetrics []metricDirective
}

type metricDefinition struct {
	Name string
	Unit string
}

type metricDirective struct {
	Namespace  string
	Dimensions [][]string
	Metrics    []metricDefinition
}
