package logger

import (
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"errors"
	"io"
	"time"
)

// Logger represents a thread-safe logger that writes log-based metrics to its registered sink
type Logger struct {
	store *store
	sink  io.Writer
}

// New instantiates a new Logger
func New(sink io.Writer, resolution time.Duration, namespace string) *Logger {
	return &Logger{
		store: newStore(namespace, resolution),
		sink:  sink,
	}
}

// PutMetric batches the metric on the loggers internal store
func (l *Logger) PutMetric(sample *telemetry.Sample) {
	if len(sample.MetricID.Dimensions) > maxDimensions {
		sample.MetricID.Dimensions = truncateDimensions(sample.MetricID.Dimensions)
	}
	err := l.store.putMetric(sample)
	if err != nil && (errors.Is(err, maxMetricDefinitionsError{}) || errors.Is(err, maxMetricValuesError{})) {
		l.Flush()
		l.store.putMetric(sample)
	}
}

// Flush writes the batched log-based metrics to the loggers registered sink
func (l *Logger) Flush() {
	logs := l.store.serialize()
	l.sink.Write(logs)
}
