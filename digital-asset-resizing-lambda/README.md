# The Digital Asset Resizing Lambda
This is the project that contains the Lambda that we us for managing resizing flows for Digital Assets, 
it is planned to be used for Emotes in [Mako][git-mako] and Badges in [Badge Service][git-badges]. 
It is intended to hooked up to the [SNS topic][upload-service-notification-docs] used by [Upload Service][git-upload-service] 
for completion and then a configured Redis instance.

[git-mako]: https://git-aws.internal.justin.tv/commerce/mako "Mako"
[git-badges]: https://git-aws.internal.justin.tv/chat/badges "Badge Service"
[git-upload-service]: https://git-aws.internal.justin.tv/web/upload-service "Upload Service"
[upload-service-notification-docs]: https://wiki.twitch.com/display/PS/Upload+Service+Notifications "Upload Service SNS Docs"