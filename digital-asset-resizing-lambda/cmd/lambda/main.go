package main

import (
	"context"
	"encoding/json"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/web/upload-service/models"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(ctx context.Context, snsEvent events.SNSEvent) {
	for _, record := range snsEvent.Records {
		callbackMsg := getCallbackMessage(record)
		fmt.Printf("[%s %s %s] Message with Upload ID: %s and Status: %s \n", record.EventSource, record.SNS.Timestamp, record.SNS.MessageID, callbackMsg.UploadID, callbackMsg.StatusName)
	}
}

func main() {
	lambda.Start(handler)
}

func getCallbackMessage(snsRecord events.SNSEventRecord) *models.SNSCallback {
	snsEntity := snsRecord.SNS
	callbackMsg := &models.SNSCallback{}

	err := json.Unmarshal([]byte(snsEntity.Message), &callbackMsg)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"msgID": snsEntity.MessageID,
			"msg":   snsEntity.Message,
		}).Panic("Failed to deserialize SNS message from Upload Service")
	}

	return callbackMsg
}
