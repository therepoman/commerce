package main

import (
	"testing"

	"github.com/aws/aws-lambda-go/events"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambda_getCallbackMessage(t *testing.T) {
	Convey("with nothing!", t, func() {
		Convey("when getting message with an empty message", func() {
			defer func() {
				So(recover(), ShouldNotBeNil) // It should panic on receiving an empty body.
			}()
			So(getCallbackMessage(events.SNSEventRecord{
				SNS: events.SNSEntity{
					Message: "",
				},
			}), ShouldBeNil)
		})

		Convey("when getting message with a malformed message body", func() {
			defer func() {
				So(recover(), ShouldNotBeNil) // It should panic on receiving a bad body.
			}()
			So(getCallbackMessage(events.SNSEventRecord{
				SNS: events.SNSEntity{
					Message: "${&%",
				},
			}), ShouldBeNil)
		})

		Convey("when getting message with an incomplete message body", func() {
			defer func() {
				So(recover(), ShouldBeNil) // It should not panic!
			}()

			callback := getCallbackMessage(events.SNSEventRecord{
				SNS: events.SNSEntity{
					Message: "{\"upload_id\":\"6f26e9dd-8486-4f2f-9abd-20a00cf1555555\"}",
				},
			})

			So(callback, ShouldNotBeNil)
			So(callback.UploadID, ShouldEqual, "6f26e9dd-8486-4f2f-9abd-20a00cf1555555")
		})
	})
}
