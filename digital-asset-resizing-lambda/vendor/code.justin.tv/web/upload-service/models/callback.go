package models

import "code.justin.tv/web/upload-service/rpc/uploader"

type SNSCallback struct {
	UploadID   string                `json:"upload_id"`
	Outputs    []uploader.OutputInfo `json:"outputs,omitempty"`
	Data       []byte                `json:"data,omitempty"`
	Status     int64                 `json:"status"`
	StatusName string                `json:"status_name"`
	DataStr    string                `json:"data_str,omitempty"`
}

// Deprecated: use rpc generated uploader.OutputInfo instead. The JSON formats are identical, and this struct
// will be kept to ensure compatibility.
type OutputInfo struct {
	Path         string `json:"path,omitempty"`
	Name         string `json:"name,omitempty"`
	NameTemplate string `json:"name_template,omitempty"`
	Format       string `json:"format,omitempty"`
	Width        uint   `json:"width,omitempty"`
	Height       uint   `json:"height,omitempty"`
	Dimensions   string `json:"dimensions,omitempty"`
	FileSize     int64  `json:"file_size,omitempty"`
}
