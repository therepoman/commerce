package models

// These are strings that are returned with the IS_IMAGE_VALIDATION_FAILED status code,
// providing more information
const (
	// The image was of the correct format, but contained un-parseable data.
	// May be a prefix of the status message.
	CorruptedImage = "Corrupted image"

	// A zero-byte file was uploaded.
	// Guaranteed to be exactly equal to the status message.
	EmptyFile = "Empty file"
)
