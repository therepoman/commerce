package transformations

import "code.justin.tv/web/upload-service/rpc/uploader"

type Crop struct {
	Top    int  `dynamodbav:"top"`
	Left   int  `dynamodbav:"left"`
	Width  uint `dynamodbav:"width"`
	Height uint `dynamodbav:"height"`
}

func (crop *Crop) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_Crop{
			Crop: &uploader.Crop{
				Top:    uint32(crop.Top),
				Left:   uint32(crop.Left),
				Width:  uint32(crop.Width),
				Height: uint32(crop.Height),
			},
		},
	}
}
