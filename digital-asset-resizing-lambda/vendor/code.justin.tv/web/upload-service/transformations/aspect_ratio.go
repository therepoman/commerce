package transformations

import "code.justin.tv/web/upload-service/rpc/uploader"

type AspectRatio struct {
	Ratio float64
}

func (ar *AspectRatio) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_AspectRatio{
			AspectRatio: &uploader.AspectRatio{
				Ratio: ar.Ratio,
			},
		},
	}
}
