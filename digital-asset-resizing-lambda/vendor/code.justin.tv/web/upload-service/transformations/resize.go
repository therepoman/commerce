package transformations

import "code.justin.tv/web/upload-service/rpc/uploader"

type ResizeDimensions struct {
	Height uint `dynamodbav:"height"`
	Width  uint `dynamodbav:"width"`
}

type ResizePercentage struct {
	Percent uint `dynamodbav:"percent"`
}

func (resize *ResizeDimensions) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_Resize{
			Resize: &uploader.Resize{
				Size: &uploader.Resize_Dimensions{
					Dimensions: &uploader.Dimensions{
						Width:  uint32(resize.Width),
						Height: uint32(resize.Height),
					},
				},
			},
		},
	}
}

func (resize *ResizePercentage) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_Resize{
			Resize: &uploader.Resize{
				Size: &uploader.Resize_Percent{
					Percent: uint32(resize.Percent),
				},
			},
		},
	}
}
