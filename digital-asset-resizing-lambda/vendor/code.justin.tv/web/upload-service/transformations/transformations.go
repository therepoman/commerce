package transformations

import (
	"code.justin.tv/web/upload-service/rpc/uploader"
)

type Transformation interface {
	AsProto() *uploader.Transformation
}
