package transformations

import (
	"fmt"
	"mime"
	"strings"
)

const (
	UIDParam    = "{{UploadID}}"
	ExtParam    = "{{Extension}}"
	DimParam    = "{{Dimensions}}"
	WidthParam  = "{{Width}}"
	HeightParam = "{{Height}}"
)

type FileInfo struct {
	UploadID string
	Size     int64
	IsImage  bool
	Format   string
	Width    uint
	Height   uint

	// Params contains a map for the filename parameter expansion
	Params map[string]string
}

func NewFileInfo(uploadID string) *FileInfo {
	return &FileInfo{
		UploadID: uploadID,
		Params: map[string]string{
			UIDParam:    uploadID,
			ExtParam:    "",
			DimParam:    "",
			WidthParam:  "",
			HeightParam: "",
		},
	}
}

func (f *FileInfo) SetFormat(format string) {
	f.Format = format
	f.Params[ExtParam] = "." + format
}

func (f *FileInfo) SetWidth(width uint) {
	f.Width = width
	f.Params[DimParam] = fmt.Sprintf("%dx%d", f.Width, f.Height)
	f.Params[WidthParam] = fmt.Sprintf("%d", f.Width)
}

func (f *FileInfo) SetHeight(height uint) {
	f.Height = height
	f.Params[DimParam] = fmt.Sprintf("%dx%d", f.Width, f.Height)
	f.Params[HeightParam] = fmt.Sprintf("%d", f.Height)
}

func (f *FileInfo) ExecTemplate(template string) string {
	if template == "" {
		return f.UploadID
	}

	for key, value := range f.Params {
		template = strings.Replace(template, key, value, -1)
	}

	return template
}

func (f *FileInfo) MimeType() string {
	ext := "." + f.Format
	return mime.TypeByExtension(ext)
}
