package redis

import (
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/go-redis/redis/v7"
)

type ImageSize string

const (
	ImageOriginalID ImageSize = "originalImageID"
	Image1xID       ImageSize = "image1xID"
	Image2xID       ImageSize = "image2xID"
	Image4xID       ImageSize = "image4xID"

	Key = "auto-resize-state.%s"
	TTL = 24 * time.Hour
)

type ResizeStateManager interface {
	SetResizeState(imageSize ImageSize, imageID, originalImageID string)
	GetResizeState(imageID string) (*ImageSize, *string)
}

type resizeStateManager struct {
	redisClient *redis.Client
}

type resizeData struct {
	ImageId         string
	OriginalImageID string
	ImageSize       ImageSize
}

func NewResizeStateManager(redisHost string) ResizeStateManager {
	client := redis.NewClient(&redis.Options{
		Addr: redisHost,
	})

	pong, err := client.Ping().Result()
	if err != nil {
		log.WithField("pong", pong).WithError(err).Error("Could not initialize Redis connection.")
		return nil
	}

	return &resizeStateManager{
		redisClient: client,
	}
}

func (r *resizeStateManager) SetResizeState(imageSize ImageSize, imageID, originalImageID string) {
	resizeData := resizeData{
		ImageId:         imageID,
		OriginalImageID: originalImageID,
		ImageSize:       imageSize,
	}

	data, err := json.Marshal(resizeData)
	if err != nil {
		log.WithField("resizeData", resizeData).WithError(err).Error("Failed to serialize data for storage in Redis.")
		return
	}

	err = r.redisClient.Set(key(imageID), data, TTL).Err()
	if err != nil {
		log.WithField("resizeData", resizeData).WithError(err).Error("Failed to store resize data into Redis")
	}
}

func (r *resizeStateManager) GetResizeState(imageID string) (*ImageSize, *string) {
	result, err := r.redisClient.Get(key(imageID)).Result()
	if err != nil && err != redis.Nil {
		log.WithField("result", result).WithError(err).Error("Failed to get data from redis.")
		return nil, nil
	}

	// no data in cache, just bail
	if len(result) == 0 || err == redis.Nil {
		return nil, nil
	}

	resizeData := resizeData{}
	err = json.Unmarshal([]byte(result), &resizeData)
	if err != nil {
		log.WithField("result", result).WithError(err).Error("Failed to deserialize data for storage in Redis.")
		return nil, nil
	}
	return &resizeData.ImageSize, &resizeData.OriginalImageID
}

func key(imageID string) string {
	return fmt.Sprintf(Key, imageID)
}
