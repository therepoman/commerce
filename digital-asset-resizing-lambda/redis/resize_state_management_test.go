package redis

import (
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v7"
	. "github.com/smartystreets/goconvey/convey"
)

func createTestRedisClient() (*redis.Client, *miniredis.Miniredis) {
	server, err := miniredis.Run()
	if err != nil {
		return nil, nil
	}

	client := redis.NewClient(&redis.Options{
		Addr: server.Addr(),
	})

	return client, server
}

func TestResizeStateManager_StateManagement(t *testing.T) {
	client, server := createTestRedisClient()
	defer server.Close()

	originalImageID := "OR_GIN_IN_AL"

	Convey("with a manager", t, func() {
		manager := resizeStateManager{
			redisClient: client,
		}

		Convey("getting with an empty cache does nothing", func() {
			originalID, state := manager.GetResizeState(originalImageID)
			So(originalID, ShouldBeNil)
			So(state, ShouldBeNil)
		})

		Convey("setting original image id with an empty cache", func() {
			manager.SetResizeState(ImageOriginalID, originalImageID, originalImageID)

			Convey("getting that original image id by original image id we just set", func() {
				state, originalID := manager.GetResizeState(originalImageID)

				So(*originalID, ShouldEqual, originalImageID)
				So(*state, ShouldEqual, ImageOriginalID)
			})

			Convey("setting original image id with the value already set", func() {
				originalImageID := "NEW ORIGINAL IMAGE ID"
				manager.SetResizeState(ImageOriginalID, originalImageID, originalImageID)

				Convey("getting that original image id by original image id with a new image id", func() {
					state, originalID := manager.GetResizeState(originalImageID)

					So(*originalID, ShouldEqual, originalImageID)
					So(*state, ShouldEqual, ImageOriginalID)
				})
			})
		})

		Convey("setting non-original image id with an empty cache", func() {
			twoXImageID := "2X Image ID"
			manager.SetResizeState(Image2xID, twoXImageID, originalImageID)

			Convey("getting that original image id by 2x image id we just set", func() {
				state, originalID := manager.GetResizeState(twoXImageID)

				So(*originalID, ShouldEqual, originalImageID)
				So(*state, ShouldEqual, Image2xID)
			})
		})
	})
}
