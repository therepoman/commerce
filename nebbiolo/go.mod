module code.justin.tv/commerce/nebbiolo

require (
	code.justin.tv/amzn/TwitchS2S2 v1.0.4
	code.justin.tv/amzn/TwitchS2SJWTAlgorithms v0.0.0-20191216181634-802bd37e3a44 // indirect
	code.justin.tv/chat/pushy v1.8.1
	code.justin.tv/growth/sqsmessage v5.0.0+incompatible
	code.justin.tv/hygienic/metricscactusstatsd v1.1.0 // indirect
	code.justin.tv/sse/jwt v0.0.0-20191217182030-0079fb760437 // indirect
	github.com/BurntSushi/toml v0.3.1
	github.com/aws/aws-lambda-go v1.18.0 // indirect
	github.com/aws/aws-sdk-go v1.19.26
	github.com/bmizerany/perks v0.0.0-20141205001514-d9a9656a3a4b // indirect
	github.com/cactus/go-statsd-client v3.1.1+incompatible
	github.com/dgryski/go-gk v0.0.0-20140819190930-201884a44051 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.3.0 // indirect
	github.com/influxdata/tdigest v0.0.0-20181121200506-bf2b5ad3c0a9 // indirect
	github.com/mailru/easyjson v0.0.0-20180823135443-60711f1a8329 // indirect
	github.com/pkg/errors v0.9.1
	github.com/streadway/quantile v0.0.0-20150917103942-b0c588724d25 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/tsenart/vegeta v12.1.0+incompatible
	golang.org/x/exp v0.0.0-20190912063710-ac5d2bfcbfe0
	gonum.org/v1/gonum v0.0.0-20190201152626-c07f678f3f61 // indirect
	gonum.org/v1/netlib v0.0.0-20190119082159-9be13e02fd56 // indirect
)

go 1.13
