package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"

	s2s2c7s "code.justin.tv/amzn/TwitchS2S2/c7s"
	s2s2 "code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/commerce/nebbiolo/attack"
	"code.justin.tv/commerce/nebbiolo/plan"
	vegeta "github.com/tsenart/vegeta/lib"
)

var (
	proxyFlag        = flag.String("proxy", "", "")
	planPathFlag     = flag.String("plan", "", "")
	s2s2IdentityFlag = flag.String("s2s2", "", "")
)

func main() {
	flag.Parse()

	var planPaths []string

	if *planPathFlag != "" {
		planPaths = append(planPaths, *planPathFlag)
		fmt.Println("-plan flag is deprecated; please use positional plan arguments instead of the -plan flag:")
		fmt.Println("  $ nebbiolo ", *planPathFlag, flag.Args())
	}

	planPaths = append(planPaths, flag.Args()...)

	var plans []plan.Plan
	for _, path := range planPaths {
		planBytes, err := ioutil.ReadFile(path)
		if err != nil {
			fmt.Printf("errored while reading plan: %v\n", err)
			os.Exit(1)
		}

		parsedPlan, err := plan.Parse(string(planBytes))
		if err != nil {
			fmt.Printf("could not parse plan: %v\n", err)
			os.Exit(1)
		}

		plans = append(plans, parsedPlan)
	}

	mergedPlan := plan.Merge(plans)

	log.Println("Successfully parsed plan.")

	log.Println("Starting attack...")

	var attackOpts []func(*vegeta.Attacker)

	if *s2s2IdentityFlag != "" {

		fmt.Printf("create s2s client: %s\n", *s2s2IdentityFlag)
		s2s2Client, err := s2s2.New(&s2s2.Options{
			Config: &s2s2c7s.Config{
				ClientServiceName: *s2s2IdentityFlag,
			},
		})
		if err != nil {
			fmt.Printf("could not create s2s client: %v\n", err)
			os.Exit(1)
		}

		client := &http.Client{
			Timeout: 30 * time.Second,
			Transport: s2s2Client.RoundTripper(s2s2Client.HTTPClient(&http.Client{
				Timeout: 30 * time.Second, // Pulled 30 seconds from vegetas client config
			}))}

		attackOpts = append(attackOpts, vegeta.Client(client))
	}

	if *proxyFlag != "" {
		log.Println("Setting proxy...")
		proxyURL, _ := url.Parse(*proxyFlag)
		attackOpts = append(attackOpts, vegeta.Proxy(http.ProxyURL(proxyURL)))
		log.Printf("Set up proxy at %v\n", proxyURL)
	}

	executor := attack.NewExecutor()

	go func() {
		<-trapSigint()
		fmt.Println("Received SIGINT, killing test run...")
		executor.Kill()
	}()

	// create target generators based on each endpoint.request
	metrics, err := executor.RunPlan(mergedPlan, attackOpts)

	log.Println("Attack over!")

	r := vegeta.NewTextReporter(metrics)
	err = r.Report(os.Stdout)
	if err != nil {
		fmt.Println("err while printing report: ", err)
		os.Exit(1)
	}

	log.Println("Done.")

	os.Exit(0)
}

func trapSigint() <-chan os.Signal {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	return c
}
