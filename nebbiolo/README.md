# Nebbiolo
The load test driver that lets you relax 🍷

Nebbiolo offers:
* Plan-driven load testing, declared with TOML. Never write a bespoke binary again.
* Configurable load curves (ramp-up/ramp-down).
* Templatized request bodies with random data.
* HTTP (JSON) and SQS support.

```toml
[[endpoint]]
path = "https://nioh-production.internal.justin.tv/twirp/nioh.API/GetChannelRestriction"

[[endpoint.request]]
body = '''
{
    "channel_id": "${data.channel_id}"
}
'''

[[endpoint.request.load_curve]]
start_rps = 0
end_rps = 16000
duration = "20m"

[[endpoint.request.load_curve]]
start_rps = 16000
end_rps = 16000
duration = "10m"

[data]
channel_id = [
    "123",
    "456",
]
```

## Overview
Nebbiolo allows you to write a plan for a load test ahead-of-time, then pass that plan to the binary for
automatic execution. The plan format allows you to configure a load curve for any set of HTTP endpoints and SQS queues,
which each can also have multiple request bodies.

Benefits of a plan-based load test workflow include:
* Plans are easy to share, reuse, and archive. Load tests become repeatable without complicated command invocations or bash scripts.
* Reduced cognitive burden for load test operators. No need to intervene to raise or lower TPS. Set it all up beforehand and sit back while it runs.
* Mergeable plans let you use one curated data set for as many different load tests as you want, simply by passing an argument.

Nebbiolo can test HTTP endpoints (oriented toward Twirp endpoints with POST method and JSON bodies) and SQS queues.
Both HTTP and SQS targets allow interpolating random data into their request bodies, making it easy to define and tweak data-dependent
load tests. There is also a built-in random TUID generator and UUID generator, and more random generators are planned.

## Getting Nebbiolo

### Locally
Install the `nebbiolo` command by running `go install code.justin.tv/commerce/nebbiolo/cmd/nebbiolo@latest`.

### In the cloud
It's sometimes convenient to run Nebbiolo in a cloud instance within a VPC instead of locally. Nebbiolo is easy to cross-compile for Linux:

```shell
$ go install code.justin.tv/commerce/nebbiolo/cmd/nebbiolo@latest
$ env GOOS=linux go build -o nebbiolo code.justin.tv/commerce/nebbiolo/cmd/nebbiolo
```

Then, use your favorite remote file transfer program (`scp` is a classic one) to put the Nebbiolo binary and your plan files on your cloud instance.

## Planning a Nebbiolo load test
Nebbiolo load tests are declared by "plans", which are [TOML](https://github.com/toml-lang/toml) files.

See the `examples/` directory for a variety of example Nebbiolo plans.

A plan may have up to three top-level elements: the `endpoint` list, the `sqs` list, and the `data` table.

### Endpoints
Each item in the `endpoint` list configures an HTTP endpoint that will receive load. Each `endpoint` has two fields:

1. `path`: The `path` field is a URL (as a string) that defines where requests will be sent and must include the scheme (`http://` or `https://`).
2. `request`: A list of request items defining types of requests that will be sent to the endpoint.
Each request item defines a body template and load curve and optional parameters for method type and path_data appended to the URL. This allows you to 
group multiple request templates under a single endpoint with independent load patterns. Requests have two required fields: `body`, a string that defines the template for request's body, 
and `load_curve`, a list of load segments documented below. There are also two optional parameters, method type for defining a different http method to send the request with and path_data
for appending additional data to the url.

### SQS queues
Each item in the `sqs` list configures an SQS queue that will receive messages as part of the load test. Each `sqs` has five fields:

1. `queue_name`: The name of the queue as a string. You can get this from the AWS console.
1. `account_id`: The numerical account ID of the account that the queue is in as a string. You can get this from the AWS console.
1. `aws_profile`: The name of an AWS profile as a string that will be used to retrieve credentials for the SQS client.
1. `aws_region`: The string specifying the AWS region where the queue you're load testing resides.
1. `message`: A list of message items defining types of messages that will be sent to the queue. Messages have three fields:
`body`, a string that defines the template for the message's body; `attrs`:, a table of strings defining attributes that will be attached to the message,
and `load_curve`, a list of load segments documented below.

### Load curves
A key component of a Nebbiolo plan is each request's "load curve". The items of a request's or message's `load_curve` list
control how much TPS Nebbiolo sends and how long it sends it for. Each segment of the load curve declares a starting RPS, an ending RPS, and a duration,
then Nebbiolo calculates how to smoothly ramp between those rates over the course of the duration. The order of load curve segments is significant,
as they're run serially.

### The data table and interpolating values
The third top-level field of a Nebbiolo plan is the `data` table. This table maps strings to lists of strings; each key
of the `data` table becomes available for interpolation in any request or message body in the plan as a whole.

For example, if your data table looks like this:
```toml
[data]
smiley = [":)", ":(", "<):)"]
```
You could then use an interpolation in a body template like this:
```toml
[[endpoint.request]]
body = """
{
  "face": "${data.smiley}"
}
"""
```
and Nebbiolo will randomly select a string from `data.smiley` at runtime to substitute for the interpolation at runtime.
Any unrecognized `data` key will be left uninterpolated.

### Method Types
Nebbiolo by default will send every request as a POST request. If you need to send a different method type, in the endpoint request
you can specify a different method type.

```toml
[[endpoint.request]]
method = "GET"
```

#### Builtins
In addition to user-provided data lists, Nebbiolo offers a `builtin` namespace for interpolations that are defined
in Nebbiolo itself.

Current available builtins:\
random TUID generator, invoked like so: `${builtin.random_tuid}`\
random UUID generator, invoked like so: `${builtin.random_uuid}`

### Plan merging
Nebbiolo plans are passed as arguments to the command, and Nebbiolo accepts multiple plan files at once.
When multiple plans are passed, they're merged according to a couple of simple rules:
1. All endpoints and messages from any passed plan will be run as-is. Duplicates are allowed, and load will be additive.
2. Data tables across plans are combined according to key, forming a single list for each key as the source for interpolations.

Plan merging allows you to separate large data lists from your endpoint/queue configuration when that's convenient, 
while still allowing small tests to be completely contained in a single plan.

## Running a load test
Once you have your plan(s), invoke Nebbiolo with a path to each plan file as an argument.

Here's an example invocation merging two plans and a common data list: `nebbiolo ./plan1.toml ./plan2.toml ./data/channel_ids.toml`.

If you need to send requests through a proxy, add the `-proxy=<url>` flag: `nebbiolo -proxy=socks5://localhost:8080 my-plan.toml`. The proxy scheme must be one of the schemes supported by the standard [(http.Transport).Proxy](https://golang.org/pkg/net/http/#Transport) field.

Nebbiolo is currently pretty quiet on stdout. It will print a line every time it begins a new segment of a load curve for each request,
then will print an aggregation of metrics when the load test finishes (or after a graceful shutdown triggered by Ctrl-C/SIGINT). 

#### Open file descriptor limit
For high loads you may run into a `too many files open` error, indicating a low max number of file descriptors. This can be increased using `ulimit`, but it's advised to read more on file descriptor limits in general to find a reasonable limit before updating it, as well as any other additional changes that might be necessary for your machine.

```bash
ulimit -n <FILE_DESCRIPTOR_LIMIT>
```
