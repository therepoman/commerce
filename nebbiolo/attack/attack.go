package attack

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/pkg/errors"

	"github.com/aws/aws-sdk-go/aws/session"
	sqs "github.com/aws/aws-sdk-go/service/sqs"

	"code.justin.tv/commerce/nebbiolo/attack/interpolation"

	"code.justin.tv/commerce/nebbiolo/plan"
	"github.com/cactus/go-statsd-client/statsd"
	vegeta "github.com/tsenart/vegeta/lib"

	"code.justin.tv/growth/sqsmessage/fastpublisher"
	"code.justin.tv/growth/sqsmessage/publisher"
)

type syncMetrics struct {
	*vegeta.Metrics
	*sync.Mutex
}

type Executor interface {
	Kill()
	RunPlan(p plan.Plan, attackOpts []func(*vegeta.Attacker)) (*vegeta.Metrics, error)
}

type executorImpl struct {
	stopChan chan struct{}
	sync.Once
}

func NewExecutor() Executor {
	return &executorImpl{
		stopChan: make(chan struct{}),
	}
}

func (e *executorImpl) Kill() {
	e.Once.Do(func() {
		close(e.stopChan)
	})
}

func (e *executorImpl) RunPlan(p plan.Plan, attackOpts []func(*vegeta.Attacker)) (*vegeta.Metrics, error) {
	select {
	case <-e.stopChan:
		panic("called RunPlan on a killed Executor")
	default:
	}

	var wg sync.WaitGroup

	metrics := syncMetrics{
		Metrics: new(vegeta.Metrics),
		Mutex:   new(sync.Mutex),
	}

	attacker := vegeta.NewAttacker(attackOpts...)

	{
		var requests []plan.Request
		for _, endpoint := range p.Endpoints {
			requests = append(requests, endpoint.Requests...)
		}
		wg.Add(len(requests))
	}
	{
		var messages []plan.Message
		for _, queue := range p.SQSQueues {
			messages = append(messages, queue.Messages...)
		}
		wg.Add(len(messages))
	}

	for _, endpoint := range p.Endpoints {
		for _, req := range endpoint.Requests {
			go e.runRequestAttackPlan(wg.Done, attacker, endpoint.Path, req, p.GetRandomDataProvider(), metrics)
		}
	}
	senders := []fastpublisher.SenderAPI{}
	for _, queue := range p.SQSQueues {
		for _, msg := range queue.Messages {
			if queue.QueueURL != "" && queue.QueueName == "" && queue.AWSAccount == "" {
				accountId, queueName, err := parseQueueURL(queue.QueueURL)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error parsing queue URL: %v\n", err)
					os.Exit(1)
				}
				queue.QueueName = queueName
				queue.AWSAccount = accountId
			}
			sess := session.Must(session.NewSession(&aws.Config{
				Region: aws.String(queue.AWSRegion),
			}))
			sqsClient := sqs.New(sess)
			basePublisher, err := createBasePublisher(publisher.PublisherConfig{
				SQS:       sqsClient,
				QueueName: queue.QueueName,
				AccountID: queue.AWSAccount,
				Stats:     &statsd.NoopClient{},
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error creating base publisher: %v\n", err)
				os.Exit(1)
			}
			fastPublisher, err := fastpublisher.NewSender(
				fastpublisher.NewSenderInput{
					SQSPublisher: basePublisher,
					Stats:        &statsd.NoopClient{},
					QueueName:    queue.QueueName,
					Logger:       &fastpublisherLogger{},
				},
				fastpublisher.WithMaxWorkersCap(5000),
				fastpublisher.WithErrorHandler(buildFastPublisherErrHandler()),
			)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error creating fast publisher: %v\n", err)
				os.Exit(1)
			}
			err = fastPublisher.Setup()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error setting up fast publisher: %v\n", err)
				os.Exit(1)
			}
			senders = append(senders, fastPublisher)
			go fastPublisher.Start()

			go e.runSQSAttackPlan(wg.Done, fastPublisher, queue.QueueName, msg, p.GetRandomDataProvider())
		}
	}

	wg.Wait()

	metrics.Metrics.Close()
	for _, q := range senders {
		q.Close()
	}

	return metrics.Metrics, nil
}

type fastpublisherLogger struct {
}

func (*fastpublisherLogger) Log(in ...interface{}) {
	fmt.Fprintf(os.Stderr, "fast publisher log: %v\n", in)
}

func createBasePublisher(conf publisher.PublisherConfig) (publisher.PublisherAPI, error) {
	basePublisher, err := publisher.NewPublisher(conf)

	if err != nil {
		return nil, errors.Wrapf(err, "creating new publisher for queue_name: %s", conf.QueueName)
	}

	if err := basePublisher.Initialize(); err != nil {
		return nil, errors.Wrapf(err, "initializing new publisher for queue_name: %s", conf.QueueName)
	}

	return basePublisher, nil
}

func buildFastPublisherErrHandler() func(smbErr *publisher.SendMessageBatchError) {
	return func(smbErr *publisher.SendMessageBatchError) {
		fmt.Fprintf(os.Stderr, "fast publisher err %s", smbErr)
	}
}

func (e *executorImpl) runRequestAttackPlan(done func(), attacker *vegeta.Attacker, url string, request plan.Request, dataProvider plan.DataProvider, metrics syncMetrics) {
	defer done()

	log.Printf("Running request attack plan. URL: %s\n", url)

	targeter := requestTargeter(url, request, dataProvider)

	for _, loadStep := range request.LoadCurve {
		select {
		case <-e.stopChan:
			return
		default:
		}

		for res := range e.runLoadStep(runLoadStepInput{loadStep, attacker, targeter}) {
			metrics.Lock()
			metrics.Add(res)
			metrics.Unlock()
		}
	}
}

func (e *executorImpl) runSQSAttackPlan(done func(), queue fastpublisher.SenderAPI, queueName string, msg plan.Message, dataProvider plan.DataProvider) {
	if done != nil {
		defer done()
	}

	interpolator := interpolation.NewInterpolator([]byte(msg.Body))
	interpolator.HandleKind("data", dataProviderStrangler{dataProvider})

	awsAttrs := make(map[string]*sqs.MessageAttributeValue)
	for k, v := range msg.Attrs {
		attr := &sqs.MessageAttributeValue{}
		attr.SetDataType("String")
		attr.SetStringValue(v)
		awsAttrs[k] = attr
	}

	log.Printf("Running SQS attack plan. Queue Name: %s\n", queueName)

	for _, loadStep := range msg.LoadCurve {
		select {
		case <-e.stopChan:
			return
		default:
		}

		rv := calcRampValues(loadStep)

		for i := 0; i < rv.segments; i++ {
			select {
			case <-e.stopChan:
				return
			default:
			}

			rps := int(loadStep.StartRPS) + int(math.Round(rv.rpsDelta*float64(i)))
			tick := time.NewTicker(time.Second / time.Duration(rps))
			endSegment := time.After(rv.segmentDur)

			log.Printf("Running SQS load step.\n  Queue Name: %s\n  RPS: %d\n  Duration: %v", queueName, rps, rv.segmentDur)

		inner:
			for {
				select {
				case <-e.stopChan:
					break inner

				case <-endSegment:
					break inner

				case <-tick.C:
					body := interpolator.Execute()
					err := queue.Send(body)
					if err != nil {
						fmt.Fprintf(os.Stderr, "Error sending message to SQS: %v\n", err)
					}
				}
			}

			tick.Stop()
		}

	}
}

type runLoadStepInput struct {
	step     plan.LoadStep
	attacker *vegeta.Attacker
	targeter vegeta.Targeter
}

func (e *executorImpl) runLoadStep(input runLoadStepInput) <-chan *vegeta.Result {
	out := make(chan *vegeta.Result)

	rv := calcRampValues(input.step)

	log.Printf("Running load step. Intervals: %v  rpsDeltaPerStep: %v\n", rv.segments, rv.rpsDelta)

	go func() {
		defer close(out)

		for i := 0; i < int(rv.segments); i++ {
			select {
			case <-e.stopChan:
				return
			default:
			}

			rps := int(input.step.StartRPS) + int(math.Round(rv.rpsDelta*float64(i)))
			if rps < 1 {
				rps = 1
			}

			attackResultChan := input.attacker.Attack(input.targeter, vegeta.Rate{Freq: rps, Per: time.Second}, rv.segmentDur, "")

		loop:
			for {
				select {
				case res, ok := <-attackResultChan:
					if !ok {
						break loop
					}
					out <- res

				case <-e.stopChan:
					input.attacker.Stop()
					break loop
				}
			}
		}
	}()

	return out
}

type rampValues struct {
	segments   int
	rpsDelta   float64
	segmentDur time.Duration
}

func calcRampValues(step plan.LoadStep) rampValues {
	out := rampValues{}
	out.segmentDur = time.Duration(step.Duration) / 100
	if out.segmentDur < 1*time.Second {
		out.segmentDur = 1 * time.Second
	}
	out.segments = int(step.Duration / plan.LoadDuration(out.segmentDur))
	if out.segments < 1 {
		out.segments = 1
	}
	out.rpsDelta = (float64(step.EndRPS) - float64(step.StartRPS)) / float64(out.segments)
	return out
}

type dataProviderStrangler struct{ plan.DataProvider }

func (d dataProviderStrangler) GetValue(_kind, key string) ([]byte, bool) {
	str, ok := d.DataProvider.GetValue(key)
	return []byte(str), ok
}

func requestTargeter(url string, request plan.Request, dataProvider plan.DataProvider) vegeta.Targeter {
	interpolator := interpolation.NewInterpolator([]byte(request.Body))
	interpolator.HandleKind("data", dataProviderStrangler{dataProvider})

	urlInterpolator := interpolation.NewInterpolator([]byte(request.PathData))
	urlInterpolator.HandleKind("data", dataProviderStrangler{dataProvider})

	return func(tgt *vegeta.Target) error {
		targetURL := url

		if request.PathData != "" {
			urlData := urlInterpolator.Execute()
			targetURL = url + "/" + string(urlData)
		}
		tgt.URL = targetURL

		tgt.Header = http.Header{"Content-Type": {"application/json"}}

		method := request.Method
		if method == "" {
			method = http.MethodPost
		}
		tgt.Method = method

		if tgt.Method == http.MethodPost {
			tgt.Body = interpolator.Execute()
		}

		return nil
	}
}
func parseQueueURL(queueURL string) (string, string, error) {
	parsedQueueURL, err := url.Parse(queueURL)
	if err != nil {
		return "", "", err
	}
	accountId, queueName := path.Split(parsedQueueURL.Path)
	accountId = strings.Trim(accountId, "/")
	return accountId, queueName, nil
}
