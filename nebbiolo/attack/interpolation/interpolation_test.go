package interpolation

import (
	"io/ioutil"
	"reflect"
	"testing"
)

type testProvider struct {
	shouldInterpolate bool
}

func (t testProvider) GetValue(kind, key string) ([]byte, bool) {
	return []byte("interpolated" + ":" + kind + ":" + key), t.shouldInterpolate
}

func TestInterpolator(t *testing.T) {
	setupInterpolator := func(template []byte) Interpolator {
		i := NewInterpolator(template)
		i.HandleKind("kind_1", testProvider{true})
		i.HandleKind("kind_2", testProvider{true})
		i.HandleKind("uninterp_kind", testProvider{false})
		return i
	}

	testCases := []struct {
		description  string
		templatePath string
		expectedPath string
	}{
		{
			description:  "no variables",
			templatePath: `./testdata/no_variables/template.txt`,
			expectedPath: `./testdata/no_variables/expected.txt`,
		},
		{
			description:  "happy, one variable",
			templatePath: `./testdata/one_variable/template.txt`,
			expectedPath: `./testdata/one_variable/expected.txt`,
		},
		{
			description:  "happy, interleaved variables including uninterpreted",
			templatePath: `./testdata/interleaved/template.txt`,
			expectedPath: `./testdata/interleaved/expected.txt`,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			tplBytes, err := ioutil.ReadFile(tt.templatePath)
			if err != nil {
				t.Fatal(err)
			}
			expectedBytes, err := ioutil.ReadFile(tt.expectedPath)
			if err != nil {
				t.Fatal(err)
			}

			i := setupInterpolator(tplBytes)

			got := i.Execute()
			if !reflect.DeepEqual(got, expectedBytes) {
				t.Errorf("interpolated bytes not as expected.\n  wanted: %s\n  got: %s\n", string(expectedBytes), string(got))
			}
		})
	}
}
