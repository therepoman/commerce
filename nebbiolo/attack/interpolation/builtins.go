package interpolation

import (
	"bytes"
	"github.com/google/uuid"
	"math/rand"
	"time"
)

type builtinProvider struct{}

var builtinProviderInstance = builtinProvider{}

func (b builtinProvider) GetValue(kind, key string) ([]byte, bool) {
	switch key {
	case "random_tuid":
		return randomTuid(), true
	case "random_uuid":
		return []byte(uuid.NewString()), true
	}

	return nil, false
}

const nums = "0123456789"

func randomTuid() []byte {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	out := make([]byte, 6+r.Intn(3))
	for ix := range out {
		out[ix] = nums[r.Intn(len(nums))]
	}

	// Trim 0s from left, which can be problematic.
	out = bytes.TrimLeft(out, "0")

	return out
}
