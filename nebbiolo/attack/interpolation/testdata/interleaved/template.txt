{
    "my_key": "${kind_1.val_1}",
    "my_other_key": {
        "nested_key": ["nested_value1", "${kind_2.val_1}", 3]
    },
    "uninterpreted": "${uninterp_kind.val_1}",
    "another_key": "${kind_1.val_2}",
    "unquoted": ${kind_1.val_1},
    "list": ["${kind_2.val_2}", "${kind_1.val_3}", "${uninterp_kind.val_2}", "${kind_2.val_3}"]
}
