package interpolation

import "regexp"

type slot struct {
	start, end int
	kind       string
	key        string
}

type pattern struct {
	kind string
	re   *regexp.Regexp
}

func patternForKind(kind string) pattern {
	return pattern{
		kind: kind,
		re:   regexp.MustCompile(`[^$]?(\${` + kind + `\.(.+?)})`),
	}
}

func (p pattern) parseSlots(template []byte) []slot {
	var out []slot

	matchIndexSets := p.re.FindAllSubmatchIndex(template, -1)

	for _, m := range matchIndexSets {
		out = append(out, slot{
			start: m[2],
			end:   m[3],
			key:   string(template[m[4]:m[5]]),
			kind:  p.kind,
		})
	}

	return out
}

