package interpolation

import (
	"fmt"
	"sort"
)

type Provider interface {
	GetValue(kind, key string) (data []byte, ok bool)
}

type Interpolator interface {
	HandleKind(kind string, provider Provider)
	Execute() []byte
}

func NewInterpolator(template []byte) Interpolator {
	i := &interpolator{template: template, providersByKind: map[string]Provider{}}
	i.HandleKind("builtin", builtinProviderInstance)
	return i
}

type interpolator struct {
	template        []byte
	slots           []slot
	providersByKind map[string]Provider
}

func (i *interpolator) addPattern(p pattern) {
	slots := p.parseSlots(i.template)
	i.slots = append(i.slots, slots...)
	sort.Slice(i.slots, func(a, b int) bool {
		return i.slots[a].start < i.slots[b].start
	})
}

func (i *interpolator) HandleKind(kind string, provider Provider) {
	if _, present := i.providersByKind[kind]; present {
		panic(fmt.Sprintf("kind %s was added twice to interpolator", kind))
	}

	i.addPattern(patternForKind(kind))
	i.providersByKind[kind] = provider
}

func (i *interpolator) Execute() []byte {
	out := make([]byte, 0, len(i.template)*2)

	var cursor int

	for _, slot := range i.slots {
		var value []byte
		var shouldInterpolate bool

		value, shouldInterpolate = i.providersByKind[slot.kind].GetValue(slot.kind, slot.key)
		if shouldInterpolate {
			out = append(out, i.template[cursor:slot.start]...)
			out = append(out, value...)
			cursor = slot.end
		}
	}

	out = append(out, i.template[cursor:]...)

	return out
}
