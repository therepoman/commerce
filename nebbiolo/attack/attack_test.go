package attack

import (
	"testing"
	"time"

	"code.justin.tv/commerce/nebbiolo/plan"
	"github.com/stretchr/testify/assert"
)

func TestCalcRampValues(t *testing.T) {
	testCases := []struct {
		description string
		step        plan.LoadStep
		expected    rampValues
	}{
		{
			description: "integral ramp",
			step: plan.LoadStep{
				StartRPS: 0,
				EndRPS:   100,
				Duration: plan.LoadDuration(100 * time.Second),
			},
			expected: rampValues{
				segments:   100,
				rpsDelta:   1,
				segmentDur: 1 * time.Second,
			},
		},
		{
			description: "fractional ramp",
			step: plan.LoadStep{
				StartRPS: 0,
				EndRPS:   5,
				Duration: plan.LoadDuration(200 * time.Second),
			},
			expected: rampValues{
				segments:   100,
				rpsDelta:   0.05,
				segmentDur: 2 * time.Second,
			},
		},
		{
			description: "negative ramp",
			step: plan.LoadStep{
				StartRPS: 100,
				EndRPS:   50,
				Duration: plan.LoadDuration(100 * time.Second),
			},
			expected: rampValues{
				segments:   100,
				rpsDelta:   -0.5,
				segmentDur: 1 * time.Second,
			},
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			got := calcRampValues(tt.step)
			assert.InDelta(t, tt.expected.rpsDelta, got.rpsDelta, 0.01, "rpsDelta should be within tolerance")
			assert.Equal(t, tt.expected.segments, got.segments, "segments should be equal")
			assert.InDelta(t, tt.expected.segmentDur, got.segmentDur, 5000, "segmentDur should be within 5ms")
		})
	}
}

func Test_parseQueueURL(t *testing.T) {
	type args struct {
		queueURL string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		want1   string
		wantErr bool
	}{
		{
			name: "simple test",
			args: args{
				queueURL: "https://sqs.us-east-2.amazonaws.com/123456789012/MyQueue",
			},
			want:    "123456789012",
			want1:   "MyQueue",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := parseQueueURL(tt.args.queueURL)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseQueueURL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("parseQueueURL() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("parseQueueURL() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
