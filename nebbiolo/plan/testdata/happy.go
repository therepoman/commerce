package testdata

import (
	"time"

	"code.justin.tv/commerce/nebbiolo/plan"
)

var HappyPlan = plan.Plan{
	Endpoints: []plan.Endpoint{
		{
			Path: "https://nioh-production.internal.justin.tv/twirp/nioh.API/GetChannelRestriction",
			Requests: []plan.Request{
				{
					Body: `{"channel_id": "278538813"}`,
					LoadCurve: []plan.LoadStep{
						{
							StartRPS: 0,
							EndRPS:   16000,
							Duration: plan.LoadDuration(20 * time.Minute),
						},
						{
							StartRPS: 16000,
							EndRPS:   16000,
							Duration: plan.LoadDuration(10 * time.Minute),
						},
					},
				},
			},
		},
	},
	SQSQueues: []plan.SQS{
		{
			QueueURL: "sqs://some-queue-url",
			Messages: []plan.Message{
				{
					Body: `{"channel_id": "278538813"}`,
					Attrs: map[string]string{
						"a": "attr 1",
						"b": "attr 2",
					},
					LoadCurve: []plan.LoadStep{
						{
							StartRPS: 0,
							EndRPS:   150,
							Duration: plan.LoadDuration(20 * time.Minute),
						},
						{
							StartRPS: 150,
							EndRPS:   150,
							Duration: plan.LoadDuration(5 * time.Minute),
						},
						{
							StartRPS: 150,
							EndRPS:   0,
							Duration: plan.LoadDuration(5 * time.Minute),
						},
					},
				},
			},
		},
	},
}
