package plan

func Merge(plans []Plan) Plan {
	out := Plan{}
	out.Data = make(map[string][]string)

	for _, p := range plans {
		out.Endpoints = append(out.Endpoints, p.Endpoints...)
		out.SQSQueues = append(out.SQSQueues, p.SQSQueues...)
		for dk, ds := range p.Data {
			out.Data[dk] = append(out.Data[dk], ds...)
		}
	}

	return out
}
