package plan

import (
	"errors"
	"fmt"
	"github.com/BurntSushi/toml"
)

func Parse(input string) (Plan, error) {
	var out Plan
	resp, err := toml.Decode(input, &out)
	if err != nil {
		return out, err
	}
	if len(resp.Undecoded()) > 0 {
		return out, errors.New(fmt.Sprintf("toml file has undecoded fields: %v", resp.Undecoded()))
	}

	return out, err
}
