package plan_test

import (
	"io/ioutil"
	"testing"

	"code.justin.tv/commerce/nebbiolo/plan"

	"code.justin.tv/commerce/nebbiolo/plan/testdata"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	testCases := []struct {
		description   string
		inputFilepath string
		expected      plan.Plan
		expectedErr   string
	}{
		{
			description:   "happy, simple case",
			inputFilepath: "./testdata/happy.toml",
			expected:      testdata.HappyPlan,
		},
		{
			description:   "sad, simple case",
			inputFilepath: "./testdata/sad.toml",
			expectedErr:   "toml file has undecoded fields",
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			b, err := ioutil.ReadFile(tt.inputFilepath)
			if err != nil {
				t.Fatal(err)
			}

			parsed, err := plan.Parse(string(b))
			expectError := len(tt.expectedErr) > 0
			assert.Equal(t, expectError, err != nil, err)
			if expectError {
				assert.Contains(t, err.Error(), tt.expectedErr)
			} else {
				assert.Equal(t, tt.expected, parsed)
			}

		})
	}
}
