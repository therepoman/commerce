package plan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMerge(t *testing.T) {
	endpoint := func() Endpoint {
		return Endpoint{
			Path:     "test-endpoint",
			Requests: []Request{},
		}
	}
	sqs := func() SQS {
		return SQS{
			QueueURL:   "test-url",
			AWSProfile: "test-profile",
			AWSRegion:  "test-region",
			Messages:   []Message{},
		}
	}

	testCases := []struct {
		description string
		in          []Plan
		expected    Plan
	}{

		{
			description: "one plan returns same plan",
			in: []Plan{
				{
					Endpoints: []Endpoint{endpoint()},
					SQSQueues: []SQS{sqs()},
					Data: map[string][]string{
						"test-key": []string{"test-datum"},
					},
				},
			},
			expected: Plan{
				Endpoints: []Endpoint{endpoint()},
				SQSQueues: []SQS{sqs()},
				Data: map[string][]string{
					"test-key": []string{"test-datum"},
				},
			},
		},

		{
			description: "merging disjoint plans",
			in: []Plan{
				{
					Endpoints: []Endpoint{{Path: "endpoint-one"}, {Path: "endpoint-two"}},
					SQSQueues: []SQS{{QueueURL: "queue-one"}},
					Data: map[string][]string{
						"key-one": []string{"datum-one"},
					},
				},
				{
					Endpoints: []Endpoint{{Path: "endpoint-three"}},
					SQSQueues: []SQS{{QueueURL: "queue-two"}, {QueueURL: "queue-three"}},
					Data: map[string][]string{
						"key-two": []string{"datum-two", "datum-three"},
					},
				},
			},
			expected: Plan{
				Endpoints: []Endpoint{{Path: "endpoint-one"}, {Path: "endpoint-two"}, {Path: "endpoint-three"}},
				SQSQueues: []SQS{{QueueURL: "queue-one"}, {QueueURL: "queue-two"}, {QueueURL: "queue-three"}},
				Data: map[string][]string{
					"key-one": []string{"datum-one"},
					"key-two": []string{"datum-two", "datum-three"},
				},
			},
		},
		{
			description: "merging overlapping data",
			in: []Plan{
				{
					Endpoints: []Endpoint{{Path: "endpoint-one"}},
					Data: map[string][]string{
						"key-one": []string{"datum-one"},
					},
				},
				{
					Data: map[string][]string{
						"key-one": []string{"datum-two", "datum-three"},
					},
				},
			},
			expected: Plan{
				Endpoints: []Endpoint{{Path: "endpoint-one"}},
				Data: map[string][]string{
					"key-one": []string{"datum-one", "datum-two", "datum-three"},
				},
			},
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			assert.Equal(t, tt.expected, Merge(tt.in))
		})
	}
}
