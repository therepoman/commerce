package plan

import (
	"time"

	"golang.org/x/exp/rand"
)

type Plan struct {
	Endpoints []Endpoint          `toml:"endpoint"`
	SQSQueues []SQS               `toml:"sqs"`
	Data      map[string][]string // TODO: richer type
}

type Endpoint struct {
	Path     string
	Requests []Request `toml:"request"`
}

type Request struct {
	Body      string     // TODO: richer type - or have a Vegeta target generator instead of the body itself
	LoadCurve []LoadStep `toml:"load_curve"`
	Method    string     // GET, POST. etc. If not specified will use POST.
	PathData  string     `toml:"path_data"` // Data appended to PATH (for HTTP requests).
}

type SQS struct {
	QueueURL   string    `toml:"queue_url"`
	QueueName  string    `toml:"queue_name"`
	AWSAccount string    `toml:"account_id"`
	AWSProfile string    `toml:"aws_profile"`
	AWSRegion  string    `toml:"aws_region"`
	Messages   []Message `toml:"message"`
}

type Message struct {
	Attrs     map[string]string
	Body      string
	LoadCurve []LoadStep `toml:"load_curve"`
}

type LoadStep struct {
	StartRPS uint `toml:"start_rps"`
	EndRPS   uint `toml:"end_rps"`
	Duration LoadDuration
}

type LoadDuration time.Duration

func (d *LoadDuration) UnmarshalText(text []byte) error {
	parsed, err := time.ParseDuration(string(text))
	*d = LoadDuration(parsed)
	return err
}

type DataProvider interface {
	GetValue(path string) (string, bool)
}

type randomDataProviderImpl struct {
	dataPools map[string][]string
}

func (r randomDataProviderImpl) GetValue(path string) (string, bool) {
	list := r.dataPools[path]
	if len(list) < 1 {
		return "", false
	}

	src := &rand.PCGSource{}
	src.Seed(uint64(time.Now().UnixNano()))

	randProvider := rand.New(src)
	ix := randProvider.Intn(len(list))

	return list[ix], true
}

func (p Plan) GetRandomDataProvider() DataProvider {
	return randomDataProviderImpl{
		dataPools: p.Data,
	}
}
