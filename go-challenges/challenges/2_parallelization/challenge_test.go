package challenge_2

import (
	"testing"
	"time"
)

// errgroup is one of the most terse utilities are disposal for orchestrating parallelization. errgroup is a great option
// for scripts (where performance does not matter) or production code where you don't want to fail fast (meaning you are okay
// with waiting for all subtasks to complete, even if one fails).
//
// Task: Refactor `ParallelizeMe` in challenge.go to call `taskA`, `taskB`, and `taskC` in parallel.
//
// Rules:
//   * All tests should pass
//   * Parallelization should be orchestrated with errgroup
//
// Reading: https://godoc.org/golang.org/x/sync/errgroup
func TestErrGroupParallelization(t *testing.T) {
	start := time.Now()

	audit := ParallelizeMe()

	dur := time.Since(start)

	// ParallelizeMe should take about 1 second if parallelized. Give an extra 100ms to ensure not flaky.
	if dur > time.Second+(time.Millisecond*100) {
		t.Fail()
	}

	if !audit.taskA.done || audit.taskA.err != nil {
		t.Fail()
	}

	if !audit.taskB.done || audit.taskB.err != nil {
		t.Fail()
	}

	if !audit.taskC.done || audit.taskC.err == nil {
		t.Fail()
	}
}
