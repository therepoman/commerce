package challenge_2

import (
	"errors"
	"time"
)

type audit struct {
	taskA auditItem
	taskB auditItem
	taskC auditItem
}

type auditItem struct {
	done bool
	err  error
}

func taskA() error {
	time.Sleep(time.Second)

	return nil
}

func taskB() error {
	time.Sleep(time.Second)

	return nil
}

func taskC() error {
	time.Sleep(time.Second)

	return errors.New("ouch")
}

func ParallelizeMe() audit {
	out := audit{}

	out.taskA.err = taskA()
	out.taskA.done = true

	out.taskB.err = taskB()
	out.taskB.done = true

	out.taskC.err = taskC()
	out.taskC.done = true

	return out
}
