package challenge_7

import (
	"testing"
)

// In 3_test_fakes, we alluded to a different way to model errors - this challenge introduces error types.
// You may also recall that the Twitch Golang Training course covered this topic as "sentinel errors vs error types".
//
// Differences vs sentinel errors
//   * When checking error equality, we compare types rather than values (less coupled with the specific implementation, as an Error interface can have multiple implementors)
//   * Can easily include meta data about the error without clouding the public API
//
// Task: Refactor the UserDB#Get method (see challenge.go) and the accompanying test suite to use type errors. Bonus
// points if you refactor the below to use a table test.
//
// Rules:
//   * All tests should pass
//   * The following error types should be created with the associated meta fields (ErrorType: {meta data definition})
//      * AccessDeniedError: {Reason: string explanation of why user does not have access to the resource}
//      * RateLimitExceededError: {RequestCount: int describing how many requests user has made in the last minute}
//      * NotFoundError: No additional meta data is required
//   * Your tests should verify that the error type meta data is set where appropriate (you can hard code their respective values)
//   * Do not modify the core orchestration/flow control (i.e. UserDB#Get's primary flow control should be a switch on `id`). You may modify the code within each case block.
//
// Reading: https://blog.golang.org/error-handling-and-go#TOC_2.
func TestUserDB(t *testing.T) {
	t.Run("it should return user a", func(t *testing.T) {
		userDB := NewUserDB()
		user, err := userDB.Get("a")

		if err != nil {
			t.Error("unexpected error for user a")
			return
		}

		if user.Name != "Glitch" {
			t.Error("unexpected name for user a")
		}
	})

	t.Run("it should return an access denied error for user b", func(t *testing.T) {
		userDB := NewUserDB()
		_, err := userDB.Get("b")

		if err == nil {
			t.Error("expected error for user b")
			return
		}

		if err != AccessDeniedError {
			t.Errorf("expected error for user b: %s", err.Error())
		}
	})

	t.Run("it should return a rate limit error for user c", func(t *testing.T) {
		userDB := NewUserDB()
		_, err := userDB.Get("c")

		if err == nil {
			t.Error("expected error for user c")
			return
		}

		if err != RateLimitExceededError {
			t.Errorf("expected error for user c: %s", err.Error())
		}
	})

	t.Run("it should return a not found error for any other user", func(t *testing.T) {
		userDB := NewUserDB()
		_, err := userDB.Get("d")

		if err == nil {
			t.Error("expected error for user d")
			return
		}

		if err != NotFoundError {
			t.Errorf("expected error for user d: %s", err.Error())
		}
	})
}
