package challenge_7

import (
	"errors"
)

var (
	NotFoundError = errors.New("NOT_FOUND")
	AccessDeniedError = errors.New("ACCESS_DENIED")
	RateLimitExceededError = errors.New("LIMIT_EXCEEDED")
)

type User struct {
	Name string
}

type UserDB interface {
	Get(id string) (User, error)
}

type userDB struct{}

func NewUserDB() UserDB {
	return &userDB{}
}

func (b *userDB) Get(id string) (User, error) {
	switch id {
	case "a":
		return User{
			Name: "Glitch",
		}, nil
	case "b":
		return User{}, AccessDeniedError
	case "c":
		return User{}, RateLimitExceededError
	default:
		return User{}, NotFoundError
	}
}
