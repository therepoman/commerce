package challenge_3

import (
	"testing"
)

func newFakeDB() *fakeDB {
	return &fakeDB{
		users: map[string]int{},
	}
}

type fakeDB struct {
	users map[string]int
}

// In addition to the benefits provided by mocks (i.e. atomic tests), test fakes aim to:
//  * Require fewer test changes when the backing implementation changes
//  * Be reusable across multiple consumers
//  * Be closer to the implementation than a mock, giving you higher confidence that the production code will work as expected
//
// Task: Write a fake that implements DB (see challenge.go)
//
// Rules:
//   * All tests should pass
//   * Write a fake that implements DB (attach methods to the fakeDB struct in this file)
//   * Fake should return `NotFoundError` when a key is not found
//      * !Note! A subsequent challenge will go over a better way to model errors in Go. We used the "simpler"
//        method here to avoid introducing multiple concepts at once.
//
// Reading: https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da
func TestFakes(t *testing.T) {
	// Ensures that fake implementation meets rules
	t.Run("the db implementation returns a NotFoundError when a key is not found", func(t *testing.T) {
		db := newFakeDB()
		/**
		Ensures fake implements NotFoundError
		*/
		_, err := db.Get("someunknownkey")
		if err != NotFoundError {
			t.Fail()
		}
	})

	t.Run("the BitsBalance implementation returns a 0 balance when a key is not found", func(t *testing.T) {
		db := newFakeDB()
		bitsBalance := NewBitsBalance(db)

		balance, err := bitsBalance.GetBitsBalance("foo")
		if err != nil {
			t.Fail()
		}
		if balance != 0 {
			t.Fail()
		}
	})

	t.Run("we can retrieve previously set values", func(t *testing.T) {
		db := newFakeDB()
		bitsBalance := NewBitsBalance(db)

		err := bitsBalance.IncrementBitsBalance("bar", 1)
		if err != nil {
			t.Fail()
		}

		balance, err := bitsBalance.GetBitsBalance("bar")
		if err != nil {
			t.Fail()
		}
		if balance != 1 {
			t.Fail()
		}
	})

	t.Run("we should properly increment balances", func(t *testing.T) {
		db := newFakeDB()
		bitsBalance := NewBitsBalance(db)

		err := bitsBalance.IncrementBitsBalance("bar", 1)
		if err != nil {
			t.Fail()
		}

		err = bitsBalance.IncrementBitsBalance("bar", 2)
		if err != nil {
			t.Fail()
		}

		balance, err := bitsBalance.GetBitsBalance("bar")
		if err != nil {
			t.Fail()
		}
		if balance != 3 {
			t.Fail()
		}
	})

	t.Run("balances are properly namespaced by tuid", func(t *testing.T) {
		db := newFakeDB()
		bitsBalance := NewBitsBalance(db)

		err := bitsBalance.IncrementBitsBalance("bar", 1)
		if err != nil {
			t.Fail()
		}

		balance, err := bitsBalance.GetBitsBalance("foo")
		if err != nil {
			t.Fail()
		}
		if balance != 0 {
			t.Fail()
		}
	})
}
