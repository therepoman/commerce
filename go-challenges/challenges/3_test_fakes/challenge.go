package challenge_3

import (
	"errors"
)

var (
	NotFoundError = errors.New("NOT_FOUND")
)

type DB interface {
	Get(key string) (currentVal int, err error)
	Set(key string, val int) error
}

type BitsBalance interface {
	GetBitsBalance(tuid string) (balance int, err error)
	IncrementBitsBalance(tuid string, amountToAdd int) error
}

type bitsBalanceImpl struct {
	db DB
}

func NewBitsBalance(db DB) BitsBalance {
	return &bitsBalanceImpl{
		db: db,
	}
}

func (b *bitsBalanceImpl) GetBitsBalance(tuid string) (currentVal int, err error) {
	val, err := b.db.Get(tuid)
	if err == NotFoundError {
		return 0, nil
	}

	return val, err
}

func (b *bitsBalanceImpl) IncrementBitsBalance(tuid string, amountToAdd int) error {
	currentBalance, err := b.GetBitsBalance(tuid)
	if err != nil {
		return err
	}

	return b.db.Set(tuid, currentBalance+amountToAdd)
}
