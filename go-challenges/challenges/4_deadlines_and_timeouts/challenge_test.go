package challenge_4

import (
	"testing"
	"time"
)

// From the API docs:
//	"Package context defines the Context type, which carries deadlines, cancellation signals,
//	 and other request-scoped values across API boundaries and between processes"
//
// Commerce had/has a number of services/APIs that have p99.9+ of > 10s. This is bad from a customer experience
//  perspective (it is better to fail fast), and is wasteful/potentially harmful for both our up and downstreams.
//
// Task: Refactor GetSubscriptionCount (see challenge.go) such that it takes no longer than 1 second to return.
//
// Rules:
//   * All tests should pass
//   * GetSubscriptionCount should timeout and return if its implementation take longer than 1 second
//   * Must not remove calls to time.Sleep()
//   * Solution should use context deadlines as the primary mechanism for enforcing timeouts
//   * `getCount` should not be called once context is cancelled
//
// Reading: https://golang.org/pkg/context/
func TestContextDeadline(t *testing.T) {
	t.Run("it should return the subscription count when the deadline is met", func(t *testing.T) {
		getCountCalled := false
		spy := func() {
			getCountCalled = true
		}

		counter := NewSubscriptionCounter(spy, 0)
		count, err := counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if !getCountCalled {
			t.Fail()
		}
	})

	t.Run("it should return an error in about a second if the deadline is exceeded, and getCount should not be called", func(t *testing.T) {
		getCountCalled := false
		spy := func() {
			getCountCalled = true
		}

		counter := NewSubscriptionCounter(spy, time.Second*5)

		start := time.Now()
		_, err := counter.GetSubscriptionCount("123")
		runDur := time.Since(start)

		if err == nil {
			t.Fail()
		}
		if getCountCalled {
			t.Fail()
		}
		// If context deadline is working as expected, it should take about a second. Adding a
		// small amount of wiggle to reduce flakiness.
		if runDur > (time.Millisecond * 1100) {
			t.Fail()
		}
	})
}
