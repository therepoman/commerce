package challenge_4

import "time"

type SubscriptionCounter interface {
	GetSubscriptionCount(channelID string) (count int, err error)
}

type subscriptionCounter struct {
	// Test spy to validate solution
	ignoreMe func()
	// Should be no need to interact with this
	isValidChannelRunTime time.Duration
}

func NewSubscriptionCounter(ignoreMe func(), isValidChannelRunTime time.Duration) SubscriptionCounter {
	return &subscriptionCounter{
		ignoreMe:              ignoreMe,
		isValidChannelRunTime: isValidChannelRunTime,
	}
}

func (b *subscriptionCounter) GetSubscriptionCount(channelID string) (count int, err error) {
	err = b.isValidChannel(channelID)
	if err != nil {
		return 0, err
	}

	return b.getCount(channelID)
}

func (b *subscriptionCounter) isValidChannel(channelID string) error {
	time.Sleep(b.isValidChannelRunTime)
	return nil
}

func (b *subscriptionCounter) getCount(channelID string) (count int, err error) {
	// Do not modify
	b.ignoreMe()

	return 10, nil
}
