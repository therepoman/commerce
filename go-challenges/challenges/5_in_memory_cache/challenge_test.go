package challenge_5

import (
	"testing"
	"time"
)

// We do all sorts of caching at Twitch, but most of it still requires a network hop (e.g. DAX, Redis). A host cache
// (aka memory cache) can be very valuable to reduce latency, as well as reduce stress on downstream systems/infrastructure.
//
// Task: Add a host cache in front of the `getCount` call within GetSubscriptionCount (see challenge.go)
//
// Rules:
//   * All tests should pass
//   * Access to our cache should be thread safe, use sync.Map for your backing data store
//   * Cache entries must be deleted 2 seconds after they are created - you may choose to do cleanup either:
//     * on read or
//     * have a background process that occasionally sweeps for expired keys
//   * Our downstream database should not be called if the cache is set
//   * You must not use a 3P library - this exercise is also meant to expand your Go knowledge
//
// Reading: https://golang.org/pkg/sync/#Map
func TestHostCache(t *testing.T) {
	t.Run("it should return the subscription count", func(t *testing.T) {
		getCountCalled := false
		spy := func() {
			getCountCalled = true
		}

		counter := NewSubscriptionCounter(spy)
		count, err := counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if !getCountCalled {
			t.Fail()
		}
	})

	t.Run("it should return the subscription count on subsequent requests for the same channel, but only hit the db once", func(t *testing.T) {
		getCountCallCount := 0
		spy := func() {
			getCountCallCount += 1
		}

		// uncached
		counter := NewSubscriptionCounter(spy)
		count, err := counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if getCountCallCount != 1 {
			t.Fail()
		}

		// cached
		count, err = counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if getCountCallCount != 1 {
			t.Fail()
		}
	})

	t.Run("it should cache values for multiple channels", func(t *testing.T) {
		getCountCallCount := 0
		spy := func() {
			getCountCallCount += 1
		}

		counter := NewSubscriptionCounter(spy)
		// uncached - channel 123
		count, err := counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if getCountCallCount != 1 {
			t.Fail()
		}

		// uncached - channel 456
		count, err = counter.GetSubscriptionCount("456")
		if err != nil {
			t.Fail()
		}
		if count != 7 {
			t.Fail()
		}
		if getCountCallCount != 2 {
			t.Fail()
		}

		// cached - channel 123
		count, err = counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if getCountCallCount != 2 {
			t.Fail()
		}

		// cached - channel 456
		count, err = counter.GetSubscriptionCount("456")
		if err != nil {
			t.Fail()
		}
		if count != 7 {
			t.Fail()
		}
		if getCountCallCount != 2 {
			t.Fail()
		}
	})

	t.Run("it should expire cache entries after 2 seconds", func(t *testing.T) {
		getCountCallCount := 0
		spy := func() {
			getCountCallCount += 1
		}

		counter := NewSubscriptionCounter(spy)
		// uncached - channel 123
		count, err := counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if getCountCallCount != 1 {
			t.Fail()
		}

		// Cache should expire after 2 seconds, so waiting 3 seconds should result in the next request for channel 123
		// going through to the db
		time.Sleep(time.Second * 3)

		// uncached - channel 123
		count, err = counter.GetSubscriptionCount("123")
		if err != nil {
			t.Fail()
		}
		if count != 10 {
			t.Fail()
		}
		if getCountCallCount != 2 {
			t.Fail()
		}
	})
}
