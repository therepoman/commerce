package challenge_5

import (
	"errors"
)

var subscriptionCounts = map[string]int{
	"123": 10,
	"456": 7,
}

type SubscriptionCounter interface {
	GetSubscriptionCount(channelID string) (count int, err error)
}

type subscriptionCounter struct {
	// Test spy to validate solution
	ignoreMe func()
}

func NewSubscriptionCounter(ignoreMe func()) SubscriptionCounter {
	return &subscriptionCounter{
		ignoreMe: ignoreMe,
	}
}

func (b *subscriptionCounter) GetSubscriptionCount(channelID string) (count int, err error) {
	return b.getCount(channelID)
}

func (b *subscriptionCounter) getCount(channelID string) (count int, err error) {
	// Do not modify
	b.ignoreMe()

	// Pretend this is a super expensive call, we must cache it
	count, ok := subscriptionCounts[channelID]
	if !ok {
		return 0, errors.New("channel not found")
	}

	return count, nil
}
