package challenge_6

import (
	"testing"
)

// In challenge_5, we used sync.Map as our cache's backing data store. This worked great for the use case, but sync.Map
// has some drawbacks as our needs become more complex. It is:
//   * Not type safe (meaning read access requires a type assertion)
//   * Not safe for concurrent writes (e.g. concurrent access with Read -> Write -> Read)
//
// Task: Refactor the Accumulate method (see challenge.go) to use an ordinary map in conjunction with a Mutex.
//
// Rules:
//   * All tests should pass
//   * Data store should be a map (not sync.Map)
//   * Map access should be gated by a sync.Mutex
//   * Do not modify the core orchestration (goroutines, wait groups, and for-loops)
//
// Reading: https://golang.org/pkg/sync/#Map
func TestParallelAccumulator(t *testing.T) {
	t.Run("it should return the accumulated value", func(t *testing.T) {
		accumulator := NewParallelAccumulator()
		value := accumulator.Accumulate()

		if value != 2000 {
			t.Errorf("expected 2000, got %d", value)
		}
	})
}
