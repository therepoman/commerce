package challenge_6

import (
	"sync"
)

const (
	key = "contentiousKey"
)

type ParallelAccumulator interface {
	Accumulate() int
}

type parallelAccumulator struct{}

func NewParallelAccumulator() ParallelAccumulator {
	return &parallelAccumulator{}
}

func (b *parallelAccumulator) Accumulate() int {
	values := sync.Map{}
	wg := sync.WaitGroup{}
	wg.Add(2)

	values.Store(key, 0)

	go func() {
		for i := 0; i < 1000; i++ {
			cached, _ := values.Load(key)
			currentValue, _ := cached.(int)
			values.Store(key, currentValue+1)
		}

		wg.Done()
	}()

	go func() {
		for i := 0; i < 1000; i++ {
			cached, _ := values.Load(key)
			currentValue, _ := cached.(int)
			values.Store(key, currentValue+1)
		}

		wg.Done()
	}()

	wg.Wait()

	cached, _ := values.Load(key)
	currentValue, _ := cached.(int)

	return currentValue
}
