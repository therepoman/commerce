package challenge_1

import "testing"

// Table Driven Testing is an effective way to structure your tests. If written correctly, they:
//   * make it much faster to add/remove tests
//   * removes much of the hard to manage nesting that we see with Convey
//   * allows assertions and test setup (e.g mocks) to be defined in a single place, making refactoring much simpler
//
// Task: Refactor `TestIsEven` into a table test within the `TestIsEven_Table` function.
//
// Rules:
//   * All the same test cases should be covered
//   * All tests should pass
//   * You should use the `testCase` struct to define the properties needed for your table test
//     * `testCase` should include a description of the test
//     * `testCase` should not include any function properties
//   * Each test case should run within a `t.Run` closure
//   * You should define your test cases within the `testCases` slice
//
// Reading: https://github.com/golang/go/wiki/TableDrivenTests
func TestIsEven(t *testing.T) {
	even := IsEven(-2)
	if !even {
		t.Fail()
	}

	even = IsEven(-1)
	if even {
		t.Fail()
	}

	even = IsEven(0)
	if !even {
		t.Fail()
	}

	even = IsEven(1)
	if even {
		t.Fail()
	}

	even = IsEven(2)
	if !even {
		t.Fail()
	}
}

// Implement Me
func TestIsEven_Table(t *testing.T) {
	// Uncomment Me
	//type testCase struct {}
	//var testCases = []testCase{}
}
