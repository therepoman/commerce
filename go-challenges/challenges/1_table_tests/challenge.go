package challenge_1

func IsEven(i int) bool {
	return i%2 == 0
}
