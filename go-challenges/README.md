# go-challenges

## Instructions

All challenge entry points are in their respective `*_test.go` file

## Initial

* Pull this repo
* Create a branch with a name matching your email alias
* Solve challenge
* Push
* Open PR w/ your name in the title (this will allow us to easily check build statuses)

## When new challenges are added

* Merge mainline into the same branch that you created previously
* Solve the new problem(s)
* Push
