import { loadConfiguration } from "../../cdk/lib/config";
import { confirm_request, spawn_with_stdout } from '../lib/exec';

export const command = 'destroy';
export const desc = 'Destroy an application';
export const builder = {
  desktop_name: {
    description: 'the name of the desktop to destroy',
    demandOption: true
  },
  force: {
    type: 'boolean',
    alias: 'f',
    default: false,
    description: 'force deletion - do not ask for confirmation'
  }
}

export interface DeployCommandOpts {
  desktop_name: string
  force: boolean
}

export async function handler({ desktop_name, force }: DeployCommandOpts) {
  if (!desktop_name) {
    console.log("please enter desktop name");
    process.exit(1);
  }

  if (!force) {
    const answer = await confirm_request("are you sure? (yes/[no]): ");
    if (answer.toLowerCase() != 'y' && answer.toLowerCase() != "yes") {
      console.log("aborting...");
      process.exit(1);
    }
  }

  const username = process.env.USER!;
  const config = loadConfiguration(`${username}_${desktop_name}.yaml`);

  let profile_cmd = ['--profile', config.account.accountName];
  if (username == "jenkins") {
    profile_cmd = [];
  }

  await spawn_with_stdout(`npx`, ['cdk', 'destroy', '*-desktop', ...profile_cmd, '-f'], {
    USER: username,
    DESKTOP_NAME: desktop_name,
  });
}