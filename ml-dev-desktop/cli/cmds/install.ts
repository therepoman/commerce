import { exec_show_stdout } from '../lib/exec';
import * as yargs from 'yargs';
import {getDevDesktopInstance} from "../lib/aws";
import {loadConfiguration} from "../../cdk/lib/config";


interface InstallCommandOpts {
  desktop_name: string
  role: string
}

export const command = 'install [role]';
export const desc = 'install software into dev desktop using ansible';
export const builder = {
  desktop_name: {
    describe: 'the name of the desktop to deploy',
    demandOption: true
  },
  role: {
    describe: 'the ansible role to install',
    default: 'all',
  }
};

export async function handler({desktop_name, role}: InstallCommandOpts) {
  const username = process.env.USER!;
  const configName = `${username}_${desktop_name}`;
  const config = loadConfiguration(`${configName}.yaml`);
  const instanceId = await getDevDesktopInstance(`${username}-${desktop_name}-desktop`, config.account.accountName, username != "jenkins");
  if (!instanceId) {
    console.log("didnt find host");
    process.exit(1);
  }

  let profile_env: object = {PROFILE: config.account.accountName};
  if (username == "jenkins") {
    profile_env = {};
  }

  await exec_show_stdout(`ansible-playbook -i dev-desktop, -e 'install_role=${role} config_name=${configName}' --ssh-common-args='-i ${config.bastion_private_key_path}' -vvvv ./playbooks/dynamic.yaml`, {
    ...profile_env,
    HOST: instanceId,
    PRIVATE_KEY: config.bastion_private_key_path,
  });
}