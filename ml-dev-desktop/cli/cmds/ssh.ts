import * as path from "path";
import { loadConfiguration } from "../../cdk/lib/config";
import { getDevDesktopInstance } from "../lib/aws";
import { spawn_with_stdout } from "../lib/exec";

function resolveHome(filepath: string) {
  if (filepath[0] === "~") {
    return path.join(process.env.HOME!, filepath.slice(1));
  }
  return filepath;
}

interface InstallCommandOpts {
  desktop_name: string;
  role: string;
  user?: string;
}

export const command = "ssh";
export const desc = "ssh into dev desktop using ansible";
export const builder = {
  user: {
    describe: "override the name of the user",
  },
  desktop_name: {
    describe: "the name of the desktop to deploy",
    demandOption: true,
  },
};

export async function handler({
  desktop_name,
  role,
  user: overrideUser,
}: InstallCommandOpts) {
  let username = process.env.USER!;
  if (overrideUser) {
    username = overrideUser;
  }
  const configName = `${username}_${desktop_name}`;
  const config = loadConfiguration(`${configName}.yaml`);
  const instanceId = await getDevDesktopInstance(
    `${username}-${desktop_name}-desktop`,
    config.account.accountName,
    true
  );
  if (!instanceId) {
    console.log("didnt find host");
    process.exit(1);
  }

  console.log(`connecting to ${instanceId}`);

  await spawn_with_stdout(
    "ssh",
    [
      "dev-desktop",
      "-i",
      resolveHome(config.bastion_private_key_path),
      "-L",
      "8000:localhost:80",
      "-L",
      "8022:localhost:22",
      "-L",
      "6379:coe10cmfkugo34z5.c5itg2.clustercfg.usw2.cache.amazonaws.com:6379",
      "-L",
      "8089:localhost:8089", // locust
      "-L",
      "8090:localhost:8090", // locust
      "-L",
      "8091:localhost:8091", // locust
      "-L",
      "8092:localhost:8092", // locust
    ],
    {
      HOST: instanceId,
      PROFILE: config.account.accountName,
    }
  );
}
