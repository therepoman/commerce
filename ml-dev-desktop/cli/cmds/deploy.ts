import {exec_show_stdout, spawn_with_stdout} from '../lib/exec';
import {loadConfiguration} from "../../cdk/lib/config";

export const command = 'deploy';
export const desc = 'Deploy an application';
export const builder = {
  desktop_name: {
    describe: 'the name of the desktop to deploy',
    demandOption: true
  }
}

export interface DeployCommandOpts {
  desktop_name: string
}

export async function handler({desktop_name}: DeployCommandOpts) {
  if (!desktop_name) {
    console.log("please enter desktop name");
    process.exit(1);
  }
  const username = process.env.USER!;
  const config = loadConfiguration(`${username}_${desktop_name}.yaml`);

  let profile_cmd = ['--profile', config.account.accountName];
  if (username == "jenkins") {
    profile_cmd = [];
  }

  await spawn_with_stdout(`npx`, [`cdk`, `deploy`, '*-desktop', ...profile_cmd, '--require-approval', 'never'], {
    USER: username,
    DESKTOP_NAME: desktop_name,
  });
}