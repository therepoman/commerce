import * as child_process from 'child_process';
import * as readline from 'readline';
import * as util from 'util';

export function spawn_with_stdout(cmd: string, args: string[], env: NodeJS.ProcessEnv = {}): Promise<void> {
  return new Promise((resolve, reject) => {
    const childProcess = child_process.spawn(cmd, args, {
      stdio: [process.stdin, process.stdout, process.stderr], env: {
        ...process.env,
        ...env,
      }
    });
    childProcess.once('exit', (code: number, signal: string) => {
      if (code === 0) {
        resolve(undefined);
      } else {
        reject(new Error('Exit with error code: ' + code));
      }
    });
    childProcess.once('error', (err: Error) => {
      reject(err);
    });
  });
}

export function exec_show_stdout(cmd: string, env: NodeJS.ProcessEnv = {}): Promise<void> {
  return new Promise<void>((success, failure) => {
    console.log(process.cwd())
    const res = child_process.spawn(cmd, [], {
      shell: true,
      env: {
        ...process.env,
        ...env,
      }
    });

    res.stdout.on('data', (data: string) => {
      console.log(data.toString());
    });

    res.stderr.on('data', function (data) {
      console.log(data.toString());
    });

    res.on('exit', function (code) {
      if (code == 0) {
        success();
      } else {
        failure();
      }
    });
  });
}

export const exec_get_stdout = util.promisify(child_process.exec);

export function confirm_request(msg: string): Promise<string> {
  const rl = readline.createInterface(process.stdin, process.stdout);
  return new Promise((resolve, reject) => {
    rl.question(msg, function (answer) {
      resolve(answer);
    });
  });
}