import {EC2Client, DescribeInstancesCommand, EC2ClientConfig} from "@aws-sdk/client-ec2";
import { fromProcess } from "@aws-sdk/credential-provider-process";

export async function getDevDesktopInstance(name: string, profileName: string, useProfile: boolean) {
  let profileCredentials: EC2ClientConfig = {credentials: fromProcess({
    profile: profileName,
  })}
  if (!useProfile) {
    profileCredentials = {};
  }
  const client = new EC2Client(profileCredentials);
  const cmd = new DescribeInstancesCommand({
    Filters: [{
      Name: 'tag:Name',
      Values: [name],
    }, {
      Name: 'instance-state-name',
      Values: ['running']
    }],
  });
  const response = await client.send(cmd);
  if (!response || !response.Reservations || !response.Reservations[0].Instances) {
    return "";
  }

  return response.Reservations[0].Instances[0].InstanceId;
}