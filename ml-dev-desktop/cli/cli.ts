#!/usr/bin/env -S npx ts-node

import * as yargs from 'yargs';

yargs
  .commandDir('cmds', {
    extensions: ['js','ts']
  })
  .demandCommand()
  .help()
  .argv