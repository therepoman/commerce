#!/usr/bin/env -S npx ts-node

import { App } from 'monocdk';
import 'source-map-support/register';
import { DevDesktopStack } from '../lib/dev-desktop-stack';
import {loadConfiguration} from "../lib/config";

if (!process.env.USER) {
    console.log("Missing username");
    process.exit(1);
}

if (!process.env.DESKTOP_NAME) {
    console.log("missing desktop name");
    process.exit(1);
}

const desktopName = process.env.DESKTOP_NAME!;
const username = process.env.USER!;
const prefix = `${username}-${desktopName}`;

const config = loadConfiguration(`${username}_${desktopName}.yaml`);

if (!config) {
    console.log(`Config cannot be found`);
    process.exit(1);
}

const app = new App({
    autoSynth: true
});

new DevDesktopStack(app, `${prefix}-desktop`, {
    prefix,
    rootDeviceSize: 256,
    config,
});
