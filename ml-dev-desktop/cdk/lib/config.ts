import * as yaml from 'js-yaml';
import * as fs from 'fs';
import * as path from 'path';

interface AwsAccountConfig {
  accountName: string;
  vpcId: string
  sg: string
  privateSubnet1: string;
  privateSubnet2: string;
  privateSubnet3: string;
  region: string;
  keyName: string;
  accountId: string;
}

interface InstanceConfig {
  instanceType: string;
}

export interface Config {
  account: AwsAccountConfig,
  instance: InstanceConfig,
  bastion_private_key_path: string,
}

export function loadConfiguration(fileName: string): Config {
  let fileContents = fs.readFileSync(path.join(__dirname, `../../config/${fileName}`), 'utf8');
  let data = yaml.load(fileContents);
  return data as Config;
}
