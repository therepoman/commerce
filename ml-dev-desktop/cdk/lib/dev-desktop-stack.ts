import { aws_ec2 as ec2, aws_iam as iam, Construct, Stack } from 'monocdk';
import {Config} from "./config";

export interface DevDesktopProps {
  prefix: string;
  rootDeviceSize: number;
  config: Config;
}


export class DevDesktopStack extends Stack {
  constructor(scope: Construct, id: string, props: DevDesktopProps) {
    const config = props.config;

    super(scope, id, {
      env: {
        region: config.account.region,
        account: config.account.accountId.toString(),
      },
    });

    const stack = this;
    const name = `${props.prefix}-desktop`;

    const role = new iam.Role(this, 'dev-machine-role', {
      roleName: `${name}-role`,
      assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName("AdministratorAccess"),
      ]
    });

    const vpc = ec2.Vpc.fromVpcAttributes(this, 'vpc', {
      vpcId: config.account.vpcId,
      availabilityZones: [`${config.account.region}a`, `${config.account.region}b`, `${config.account.region}c`],
      privateSubnetIds: [config.account.privateSubnet1, config.account.privateSubnet2, config.account.privateSubnet3],
    });

    const sg = ec2.SecurityGroup.fromSecurityGroupId(this, 'sg', config.account.sg);
    let mlImage = new ec2.LookupMachineImage({name: 'ubuntu/images/hvm-ssd/ubuntu-groovy-20.10*', owners: ['099720109477'], filters: {
      architecture: ['x86_64'],
    }});

    const instance = new ec2.Instance(this, name, {
      machineImage: mlImage,
      instanceType: new ec2.InstanceType(config.instance.instanceType),
      vpc,
      securityGroup: sg,
      role,
      keyName: config.account.keyName,
      instanceName: name,
      blockDevices: [
        {
          deviceName: "/dev/sda1",
          volume: {
            ebsDevice: {
              volumeSize: props.rootDeviceSize,
              deleteOnTermination: true,
            }
          },
        }
      ],
    });

    instance.userData.addCommands(
      "apt-get -y update",
      "apt-get -y upgrade",
      "apt-get -y install build-essential",
    );
  }
}
