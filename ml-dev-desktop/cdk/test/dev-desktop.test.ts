import { expect as expectCDK, MatchStyle, matchTemplate } from '@aws-cdk/assert';
import { App } from 'monocdk-experiment';
import { DevDesktopStack } from '../lib/dev-desktop-stack';

test('Empty Stack', () => {
  const app = new App();
  // WHEN
  const stack = new DevDesktopStack(app, 'dev-stack', {
    machineName: 'dev',
  });
  // THEN
  expectCDK(stack).to(matchTemplate({
    "Resources": {}
  }, MatchStyle.EXACT))
});
