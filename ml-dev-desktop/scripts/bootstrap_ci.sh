#!/usr/bin/env bash
set -x

pip install --upgrade awscli
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
dpkg -i session-manager-plugin.deb
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" | tee /etc/apt/sources.list.d/yarn.list
apt update && apt install -y yarn jq ansible
mkdir ~/.ssh
aws secretsmanager get-secret-value --secret-id ssh_key_private | jq -r '.SecretString' > ~/.ssh/commerce-science-aws.pem
chmod 400 ~/.ssh/commerce-science-aws.pem
cat <<EOF > ~/.ssh/config
Host dev-desktop
  ProxyCommand sh -c "aws ssm start-session --target \$HOST --document-name AWS-StartSSHSession --parameters 'portNumber=%p'"
  User ubuntu
EOF
npm install