# ML Dev Machine

How to setup guide: https://docs.google.com/document/d/1O0_SGG1CYv4qPKnCxmIDS0c8iJQbbNMNQCvUmVFS4qI/edit

## Latest updates:
Dev Desktop sets up nginx to direct various ports to internal services. To support this add the following to your /etc/hosts:
```bash
> sudo vim /etc/hosts
```

```
127.0.0.1       flink.devdesktop
127.0.0.1       jupyter.devdesktop
127.0.0.1       panel.devdesktop
127.0.0.1       dash.devdesktop
127.0.0.1       omnisci.devdesktop
```