if (document.querySelector('li.selected')) {
  const selectedParent = document.querySelector('li.selected').closest('ul');
  selectedParent.classList.toggle('collapsed');
  selectedParent.classList.add('expanded');
}

const nav = document.querySelector('#main_nav');

nav.addEventListener('click', function (event) {
  if (event.target.classList.contains('heading-level-1')) {
    event.target.nextElementSibling.classList.toggle('collapsed');
    event.target.nextElementSibling.classList.toggle('expanded');
  }
});
