//get headings on page
const content = document.querySelector('.content');
var headings = content.querySelectorAll('h2,h3');
//don't do any of this if there aren't multiple headings on the page
if (headings.length > 1) {
  //add nav box
  const el = document.querySelector('#page-nav');
  el.insertAdjacentHTML('beforeend', '<div class="sidebar"><div class="sidebartoc"><span class="sidebarheading">Contents</span><ul data-gumshoe class="innerstep"></ul></div></div>');
  //get each heading on the page
  Array.prototype.forEach.call(headings, function(el, i) {
  //add heading link to top nav box
  const innerstep = document.querySelector('ul.innerstep');
  innerstep.insertAdjacentHTML('beforeend', '<li><a data-scroll class="topiclink navlinks" href="#' + el.getAttribute('id') + '">' + el.textContent + '</a></li>');
  });
}
//Add anchor.js headings
(function () {
  'use strict';
  anchors.options.placement = 'left';
  anchors.add('.content > h2, .content > h3, .content > h4, .content > h5, .content > h6');
})();