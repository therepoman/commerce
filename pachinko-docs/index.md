---
title: "Pachinko"
page-category: "searchable"
description: "The root page for the docs site."
---

Pachinko is a distributed Token balance tracking service for all of Twitch's needs.

## Endpoints

### Bits Endpoints

| Environment | Endpoint |
| --- | --- |
| Staging | https://bits.staging.pachinko.internal.justin.tv |
| Prod | https://bits.prod.pachinko.internal.justin.tv |

### CoPo Endpoints
Community Points: chat/copo

| Environment | Endpoint |
| --- | --- |
| Staging | https://cp.staging.pachinko.internal.justin.tv |
| Prod | https://cp.prod.pachinko.internal.justin.tv |

### Stickers Endpoints

| Environment | Endpoint |
| --- | --- |
| Staging | https://sticker.staging.pachinko.internal.justin.tv |
| Prod | https://sticker.prod.pachinko.internal.justin.tv |

### Sub Tokens Endpoints
Sub Tokens: subs/galleon

| Environment | Endpoint |
| --- | --- |
| Staging | https://st.staging.pachinko.internal.justin.tv |
| Prod | https://st.prod.pachinko.internal.justin.tv |

## Teleport Bastion Clusters
You can verify the status of Teleport Bastion Clusters [here](https://dashboard.bastion.xarth.tv/status)

| Environment | Cluster |
| --- | --- |
| Staging | twitch-pachinko-devo |
| Prod | twitch-pachinko-prod |


These endpoints are configured through the [Twitch DNS tool](https://dashboard.xarth.tv/dns).

Each human readable DNS entry points to the corresponding internal ALB endpoint.

## Retool
Retool manages versioning of the tools used by this repo. Individual tool source is stored in the _tools/ folder and their versions are stored in the tools.json file.

The source code for retool itself is also checked in under the _tools directory.

To install retool and build the correct version of all tools simply run:

```
make install_retool
```

To add a new tool:
```
retool add [repo] [commit]
```

## Dep
Dep is the definitive go dependency management tool.

To pull new dependencies into the vendor directory, make use of the dependency in the code and then run:
```sh
make dep
```

## Twirp
[Twirp](https://git.xarth.tv/common/twirp) is a Twitch internal RPC system for service to service communication.

To add a new API, define the requests and responses in the `pachinko-service.proto` file and then run:
```sh
make generate_twirp
```

## Docker
Once you're sshed into a pachinko EC2 host

Change to root (since docker runs as root):

```
sudo su
```

List out running docker containers:

```
docker ps
```

Open a shell with a given container:

```
docker exec -it [container-id] bash
```

## Terraform
Terraform is used to manage all AWS resources (beanstalk, dynamodb, etc).

To apply terraform changes navigate to the directory corresponding to the environment you want to modify (ex: `terraform/staging`).

First initialize your terraform environment.
```
terraform init
```

Next, generate a plan file.
```
terraform plan -out newconfig.plan
```

Once you've reviewed the plan and are confident in your change, apply it:
```
terraform apply "newconfig.plan"
```
