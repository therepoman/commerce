---
title: "Adding a New Tenant to Twitch's Global Infrastructure"
page-category: "searchable"
description: "The steps necessary to hook up the new tenant domain to the tenant."
---

Now that our new tenant exists in our infrastructure, it's time to allow our new tenant to call us. We need to do a few things to allow this to happen.

### Add the DNS for the new tenant

Go to the [DNS tool](https://dashboard.xarth.tv/dns/) (you might have to authenticate to the site before this link works). You'll want to add an entry for both the staging and prod ALBs, formated like

```bash
{tenantName}.{staging | prod}.pachinko.internal.justin.tv
```

You need to match the stage exactly like this, or else the certificate for the ALB will not be valid for the DNS name. You'll want to make an `internal` CNAME entry that has a `300` timeout, with the value being the DNS of the internal ALB that was created in Terraform. Make sure to save the entry before leaving the console.

### Peer Pachinko to the tenant's AWS account.

In order for Pachinko to be called from the tenant's AWS account, it will need to be VPC peered. Follow the directions [here](https://wiki.twitch.com/display/SYS/AWS+VPC+Peering+at+Twitch) on how to peer VPCs here at Twitch. You will need to do this for both staging and production accounts.

Make sure to confirm that you can curl the other service from your AWS account, and that the tenant can curl our `ping` endpoint from their account.