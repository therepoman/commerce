---
title: "Adding a New Tenant Domain"
page-category: "searchable"
description: "Adding a new Tenant domain to the Pachinko codebase."
---

Before we can go creating any resources, you'll need to add the new domain to our code so that when we start up a new ECS task, our system will recognize the tenant.

[Here is a sample PR of changes needed to be made.](https://git.xarth.tv/commerce/pachinko/pull/65) Have your changes reviewed and approved by the Bits team as well as from the tenant's team. 

### Create new entry in `Domain` Twirp Enum

You'll need to first create a new `Domain` in our Twirp API. This domain is used around our system to determine that your tenant indeed exists.

Here's some sample protobuf code:
```protobuf
enum Domain {
    ...
    TENANT_NAME = X;
}
```
Make sure the tenant name is in all caps and the number X is greater than the greatest value already in the enum.

Ensure to run `make generate_twirp` to actually update the Twirp generated Go code to check in.

### Create new entries in our config data

Creating an entry in the `tenants` section will make sure that we have resource identifier and balance update notifications configured for each tenant. Only include the balance update notification topic if the tenant wants to utilize it.

Here's some sample YAML:
```yaml
tenants:
  tenant_name:
    resource-identifier: tn
``` 
This example does not include the balance updates SNS topic, which can be added later when that value is constructed via terraform.

### Merge your change to master

After you have your PR reviewed and approved, merge your PR to master. This will kick off the master branch build, which updates our staging / prod ECR docker images. This will ensure that the latest Docker image in both staging and production will be current with the latest tenants. You will need to do this before you run terraform.