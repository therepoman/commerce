---
title: "Adding New Tenant Infrastructure"
page-category: "searchable"
description: "The steps for adding a new tenant's infrastructure to Pachinko."
---

You will need to create the tenant's infrastructure as well, since Pachinko is a cell based architecture style.

### Add new tenant to lambda push / deploy scripts

in the files `scripts/push-lambda.sh` and `scripts/deploy-lambdas.sh` in the root project directory, you will need to add a new tenant entry in each of these so that we push / deploy our lambdas correctly.

[Here is a sample PR of changes needed to be made.](https://git.xarth.tv/commerce/pachinko/pull/67) Have your changes reviewed and approved by the Bits team.

### Create new resources in Terraform

Add a new tenant in the `tenants.tf` file in the `pachinko` module. Mostly just copy / paste an existing entry, and just replace the `tenant_name` with what you have determined to be the resource identifier in the config.

[Here is a sample PR of changes needed to be made.](https://git.xarth.tv/commerce/pachinko/pull/68) Have your changes reviewed and approved by the Bits team.

### Apply your changes in Terraform, but fail a little bit

After your PR is approved, apply your changes on both staging and production. This might take a while, since a lot of infrastructure needs to be made. You might need to run plan / apply multiple times, as some resources might time out while creating.

This will fail to create the lambda, since we haven't pushed the code up to the S3 bucket yet. But, we couldn't quite merge our other code that pushes the code to the S3 bucket yet since it didn't exist.

### Merge your PR for lambda push / deploy scripts

Now we can merge the PR that modifies the push / deploy scripts, since the S3 bucket we want to push the code to now exists. Let master build all the way, such that it pushes the code to both staging and production.

### Apply your changes in Terraform again

Now your terraform plan / apply will succeed, since the S3 bucket / key exists remotely. Wait until it's done, which shouldn't be long since it should only be creating a few resources.

### Merge you PR for Terraform changes

Now that your terraform changes are complete, go ahead and merge the PR.

### Add the new tenant to the deployment `Jenkinsfile`

We need to add the new tenant to the deployment Jenkins job, which is located at `jenkins/Jenkinsfile`, so that we can deploy to the staging / prod ECS services.

Here's an example of what to append onto each stage's parallel step:

```
stage('Deploy {Tenant} {Stage}') {
    environment {
        SERVICE = "pachinko-{resourceIdentifier}"
        TASK = "pachinko-{resourceIdentifier}"
    }

    steps {
        sh "./jenkins/deploy.sh"
    }
}
```

[Here is a sample PR of changes needed to be made.](https://git.xarth.tv/commerce/pachinko/pull/70) Have your changes reviewed and approved by the Bits team.

Merge it to master and now you'll be able to deploy updates to staging / prod.