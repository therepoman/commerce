---
title: "Fetching Token Balances"
page-category: "searchable"
description: "How to fetch token balances for an owner."
---

When you fetch a balance, you are fetching the balance for the specified `ownerID` and `groupID` (or multiple in the case of the `GetTokens` API) with any holds applied to that balance. The balance will always be calculated in this manner. There are multiple ways to fetch a balance as specified below.

## Fetching a Single Balance

When you want to fetch just a single balance, the API `GetToken` will provide you with what you need. You can call it with a provided `ownerID` and `groupID` and it will retrieve the balance for you, if it does indeed exist.

Sample code
```golang
response, err := pachinkoClient.GetToken(ctx, &pachinko.GetTokenReq{
    OwnerId: ownerID,
    GroupId: groupID,
}
```

This API call will attempt to find the balance in the domain specified by the endpoint you provided. If found, it will return the balance in the field `Balance` on the response object.

This will return a 400 error if no `ownerID` or `groupID` is provided in the API request. Any error that occurs during the lookup will return a 500 error.

## Fetching All Balances for an Owner

When you want to fetch all balances for an owner, the API `GetTokens` will provide you work for you. You can call it with an `ownerID` and it will retrieve all balances that user owns in the domain.

Sample code
```golang
response, err := pachinkoClient.GetTokens(ctx, &pachinko.GetTokensReq{
    OwnerId: ownerID,
}
```

This API call will attempt to find all balances in the domain for the specified `ownerID`. If balances are found, it will be returned as a list in the field `Tokens` on the response object.

This will return a 400 error if no `ownerID` is provided. Any error that occurs during the lookup will return a 500 error.