---
title: "Transactional Model"
page-category: "searchable"
description: "Pachinko's Transactional Model description."
---

Pachinko works on a transactional model, where operations that modify the balance (except adding tokens to a balance) must execute a transaction created within Pachinko. A transaction is made up of two API requests, `StartTransaction` and `CommitTransaction`.

## StartTransaction

The `StartTransaction` API takes some information you provide, and approves what you're trying to do to a token balance. 

Sample Code
```golang
resp, err := pachinkoClient.StartTransaction(ctx, &pachinko.StartTransactionReq{
    Operation: operationEnum,
    TransactionId: optionalTransactionID,
    // additional operation parameters
})
```

Some things to note about starting a transaction

1. The operation is required, and can only be provided via one of the valid enums defined in the `Operation` enum structure.
1. `TransactionId` is not required. If one is not provided, then Pachinko will create a Transaction ID for you.
1. When you call `StartTransaction`, it can return a couple of different ways. 
    * If your transaction failed to create because of a server side error, you would be returned a 5xx error and a `nil` response object. You are able to retry this request with the same `TransactionId` provided if you provided one in the request.
    * If your transaction failed to create due to basic validation errors, it would return a 4xx style error with the error response. (we want to get rid of this response and move all responses to the later validation error response)
    * If your transaction failed to create due to complex validation errors, it will return 200 response, with no `TransactionId` set and the field `ValidationError` is set with the offending token and what the error was.
    * If your transaction is created successfully, it will return a 200 response with the `TransactionId` returned that was started
1. Transactions once created have a short life while in flight, so once a transaction has been successfully created, it should be committed promptly afterwards to ensure that your operation is successful.

## CommitTransaction

The `CommitTransaction` API takes an existing, non-committed `TransactionId` and executes the operation that was approved.

Sample Code
```golang
resp, err := pachinkoClient.CommitTransaction(ctx, &pachinko.CommitTransactionReq{
    TransactionId: transactionId,
})
```

Some things to note about committing a transaction:

1. Before the transaction is executed, we ensure that the transaction exists, that the transaction is indeed not committed yet, and that the transaction is still in flight. If a transaction is not valid, it will return a 4xx style error.
1. If an error occurs while processing, a 5xx error will be returned. You are able to retry this error as long as the transaction is still in flight.
1. When the transaction is executed, all operations that are performed obtain a write lock to perform the operation. When the operation has succeeded, the transaction record will be marked as committed asynchronously, and the updated balances will be returned to the the caller.
1. After we successfully executed the transaction, if a balance update SNS topic is specified in the tenant config, a balance update notification will be published to the topic for asynchronous consumption with the transaction ID that was processed.