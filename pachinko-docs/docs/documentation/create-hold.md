---
title: "Creating a Hold"
page-category: "searchable"
description: "How to create a hold on a balance."
---

In order to create a hold on a balance, you need to call `StartTransaction` with the operation `CREATE_HOLD`.

Sample Code
```golang
resp, err := pachinkoClient.StartTransaction(ctx, &pachinko.StartTransactionReq{
   Operation: pachinko.Operation_CREATE_HOLD,
   Tokens: []*pachinko.Tokens{
        // Tokens to consume
   }
   ExpiresAt: expireTime,
})
```

Some notes about how to use this API:

1. All tokens to consume must be of the same `OwnerId`. If they are not, an error will be returned.
1. All tokens must have unique `GroupId` in the list. You cannot provide duplicate `GroupId`s in the list.
1. You must provide at least 1 `Token` object in the `Tokens` field. You can provide up to 10 `Token` objects in the list.
1. You cannot create a hold of more tokens than a balance has, including it's in flight transactions. Otherwise, it will return an error.
1. The `ExpiresAt` field is optional, but if not provided it will default to 30 minutes. 
1. If successful, this API will return the `TransactionId` of the transaction record Pachinko has recorded.

After your `StartTransaction` call succeeds, you will need to call `CommitTransaction` with the corresponding `TransactionId` returned from your `StartTransaction` call.
