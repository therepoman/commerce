---
title: "Consuming Tokens"
page-category: "searchable"
description: "How to consume tokens in Pachinko."
---

In order to consume tokens, you need to call `StartTransaction` with the operation `CONSUME_TOKENS`.

Sample Code
```golang
resp, err := pachinkoClient.StartTransaction(ctx, &pachinko.StartTransactionReq{
   Operation: pachinko.Operation_CONSUME,
   Tokens: []*pachinko.Token{
        // Tokens to create holds on
   }
})
```

Some notes about how to use this API:

1. All tokens to consume must be of the same `OwnerId`. If they are not, an error will be returned.
1. All tokens must have unique `GroupId` in the list. You cannot provide duplicate `GroupId`s in the list.
1. The token `Quantity` field is the amount you would like to consume from the balance. This number must be greater than 0.
1. You must provide at least 1 `Token` object in the `Tokens` field. You can provide up to 10 `Token` objects in the list.
1. You cannot consume more tokens than a balance has, including it's in flight transactions. Otherwise, it will return an error.
1. If successful, this API will return the `TransactionId` of the transaction record Pachinko has recorded.

After your `StartTransaction` call succeeds, you will need to call `CommitTransaction` with the corresponding `TransactionId` returned from your `StartTransaction` call.
