---
title: "Adding Tokens"
page-category: "searchable"
description: "How to add tokens to a balance in Pachinko"
---

Adding to an owner's balance is as easy in Pachinko, and is just an API call away. You can only increment one balance at a time through this API call.

Sample code
```golang
resp, err := pachinkoClient.AddTokens(ctx, &pachinko.AddTokensReq{
    TransactionId: someUuid,
    Token: &pachinko.Token{
        OwnerId: ownerID,
        GroupId: groupID,
        Quantity: quantity,
    },
}
```

When crafting this response, here are some things to keep in mind:

1. The transaction ID provided must be unique. This ID will be recorded to our transactional ledger for auditing purposes.
1. The `Token` object provided is the desired quantity of tokens you would like to increase in the balance. Only positive values are accepted here.

The response object contains the updated Token Balance after the addition, so if your balance was 10 before, then you added 10 to it, the balance object returned would specify the `Quantity` as 20. 

This API returns an error if the request body is invalid, or the transaction ID provided has already been committed to the ledger. Any other errors that occur will return 5xx based errors.

After we successfully add tokens to a balance, if a balance update SNS topic is specified in the tenant config, a balance update notification will be published to the topic for asynchronous consumption with the transaction ID that was processed.
