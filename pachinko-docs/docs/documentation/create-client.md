---
title: "Creating a Client"
page-category: "searchable"
description: "How to create a Pachinko client in your service"
---

## Basic Client Construction

Pachinko is like any other Twirp client. All you'll need to do is define a new client like

```golang
client := pachinko.NewPachinkoProtobufClient(endpoint, httpClient)
```

Where `endpoint` is your tenant specific endpoint for your domain, and the `httpClient` is just an interface that implements this interface: 

```golang
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}
```

Something like `http.Client` implements this interface, and if you need something default without any parameterization you can just use `http.DefaultClient`

## Circuit Wrapping

We would highly suggest wrapping calls to Pachinko, or any other service you're using, with some kind of circuit breaker, so that you don't end up slamming our service when we are having an outage. You can use some of these libraries to help you out:
 
 * [afex/hystrix-go](github.com/afex/hystrix-go/hystrix) a simple, open source hystrix implementation brought to you by Netflix.
 * [cep21/circuit](https://github.com/cep21/circuit) a simple, open source hystrix implementation brought to you by an employee of Twitch