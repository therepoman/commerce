---
title: "Finalizing a Hold"
page-category: "searchable"
description: "How to finalize a hold to remove the balance."
---

In order to finalize a hold on a balance, you need to call `StartTransaction` with the operation `FINALIZE_HOLD`. 

Sample Code
```golang
resp, err := pachinkoClient.StartTransaction(ctx, &pachinko.StartTransactionReq{
   Operation: pachinko.Operation_FINALIZE_HOLD,
   OperandTransactionId: holdTransactionID,
})
```

Some notes about how to use this API:

1. You must provide an `OperandTransactionId` that was a hold created from a committed `CREATE_HOLD` transaction. If this transaction does not exist, it will return an error.
1. If successful, this API will return the `TransactionId` of the transaction record Pachinko has recorded.

After your `StartTransaction` call succeeds, you will need to call `CommitTransaction` with the corresponding `TransactionId` returned from your `StartTransaction` call.
