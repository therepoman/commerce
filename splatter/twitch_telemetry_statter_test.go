package splatter

import (
	"testing"

	"code.justin.tv/amzn/TwitchTelemetry"
	. "github.com/smartystreets/goconvey/convey"
	"sync"
	"time"
)

type testUnbufferedSampleObserver struct {
	distributions []*telemetry.Distribution
	mux           sync.Mutex
}

func (s *testUnbufferedSampleObserver) FlushWithoutBuffering(distributions []*telemetry.Distribution) {
	s.mux.Lock()
	defer s.mux.Unlock()
	s.distributions = append(s.distributions, distributions...)
}

func (s *testUnbufferedSampleObserver) getDistribution() []*telemetry.Distribution {
	s.mux.Lock()
	defer s.mux.Unlock()
	distCp := make([]*telemetry.Distribution, len(s.distributions))
	copy(distCp, s.distributions)
	return distCp
}

func TestTwitchTelemetryStatter(t *testing.T) {
	Convey("Given a TwitchTelemetryStatter", t, func() {
		config := &BufferedTelemetryConfig{
			FlushPeriod:       time.Second, // Flushing every second so unit tests run faster
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			ServiceName:       "TestTwitchTelemetryStatter",
			AWSRegion:         "us-west-2",
			Stage:             "test",
			Substage:          "primary",
			Prefix:            "testprefix",
		}

		filtered := map[string]bool{
			"filtered": true,
		}

		sampleObserver := &testUnbufferedSampleObserver{distributions: []*telemetry.Distribution{}}
		bufferedReporter := telemetry.NewBufferedAggregator(config.FlushPeriod, config.BufferSize, config.AggregationPeriod, sampleObserver, nil)
		sampleReporter := telemetry.SampleReporter{
			SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: constructTPid(config)},
			SampleObserver: bufferedReporter,
		}

		statter := TwitchTelemetryStatter{
			SampleReporter:          sampleReporter,
			BufferedTelemetryConfig: config,
			FilteredStats:           filtered,
		}

		Convey("When calling Inc", func() {
			Convey("Adding metrics succeeds", func() {
				err := statter.Inc(stat, value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 1)
			})

			Convey("When metric should be filtered", func() {
				err := statter.Inc("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling Dec", func() {
			Convey("Adding metrics succeeds", func() {
				err := statter.Dec(stat, value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 1)
			})

			Convey("When metric should be filtered", func() {
				err := statter.Dec("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling Gauge", func() {
			Convey("Adding metrics succeeds", func() {
				err := statter.Gauge(stat, value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 1)
			})

			Convey("When metric should be filtered", func() {
				err := statter.Gauge("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling GaugeDelta", func() {
			Convey("Method should be unsupported", func() {
				err := statter.GaugeDelta(stat, value, rate)
				So(err, ShouldNotBeNil)
			})

			Convey("When metric should be filtered", func() {
				err := statter.GaugeDelta("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling Timing", func() {
			Convey("Adding metrics succeeds", func() {
				err := statter.Timing(stat, value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 1)
			})

			Convey("When metric should be filtered", func() {
				err := statter.Timing("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling TimingDuration", func() {
			Convey("Adding metrics succeeds", func() {
				err := statter.TimingDuration(stat, value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 1)
			})

			Convey("When metric should be filtered", func() {
				err := statter.TimingDuration("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling Set", func() {
			Convey("Method should be unsupported", func() {
				err := statter.Set(stat, "value", rate)
				So(err, ShouldNotBeNil)
			})

			Convey("When metric should be filtered", func() {
				err := statter.Set("filtered", "value", rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling SetInt", func() {
			Convey("Method should be unsupported", func() {
				err := statter.SetInt(stat, value, rate)
				So(err, ShouldNotBeNil)
			})

			Convey("When metric should be filtered", func() {
				err := statter.SetInt("filtered", value, rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling Raw", func() {
			Convey("Method should be unsupported", func() {
				err := statter.Raw(stat, "value", rate)
				So(err, ShouldNotBeNil)
			})

			Convey("When metric should be filtered", func() {
				err := statter.Raw("filtered", "value", rate)
				So(err, ShouldBeNil)
				time.Sleep(2 * time.Second)
				So(len(sampleObserver.getDistribution()), ShouldEqual, 0)
			})
		})

		Convey("When calling NewSubStatter", func() {
			subStatter := statter.NewSubStatter("secondprefix")
			So(subStatter, ShouldNotBeNil)

			Convey("The original config should not have changed", func() {
				So(config.Prefix, ShouldEqual, "testprefix")
			})
		})

		Convey("When calling Close", func() {
			err := statter.Close()
			So(err, ShouldBeNil)
		})
	})
}
