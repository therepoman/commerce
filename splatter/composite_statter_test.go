package splatter

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/splatter/mocks"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	stat  = "foo"
	value = 34
	rate  = 1.0
)

func TestCompositeStatter(t *testing.T) {
	Convey("Test Composite Statter", t, func() {
		mockStatter1 := new(mocks.Statter)
		mockStatter2 := new(mocks.Statter)

		compositeStatter, err := NewCompositeStatter([]statsd.Statter{mockStatter1, mockStatter2})
		So(err, ShouldBeNil)

		Convey("Test Inc", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Inc", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Inc", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Inc(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
			})

			Convey("First Statter Errors", func() {
				mockStatter1.On("Inc", Anything, Anything, Anything).Return(errors.New("test"))
				mockStatter2.On("Inc", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Inc(stat, value, rate)

				So(err, ShouldNotBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
			})

			Convey("Second Statter Errors", func() {
				mockStatter1.On("Inc", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Inc", Anything, Anything, Anything).Return(errors.New("test"))

				err := compositeStatter.Inc(stat, value, rate)

				So(err, ShouldNotBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
			})

			Convey("Both Statters Error", func() {
				mockStatter1.On("Inc", Anything, Anything, Anything).Return(errors.New("test"))
				mockStatter2.On("Inc", Anything, Anything, Anything).Return(errors.New("test"))

				err := compositeStatter.Inc(stat, value, rate)

				So(err, ShouldNotBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Inc", 1), ShouldBeTrue)
			})
		})

		Convey("Test Dec", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Dec", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Dec", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Dec(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Dec", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Dec", 1), ShouldBeTrue)
			})
		})

		Convey("Test Gauge", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Gauge", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Gauge", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Gauge(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Gauge", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Gauge", 1), ShouldBeTrue)
			})
		})

		Convey("Test GaugeDelta", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("GaugeDelta", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("GaugeDelta", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.GaugeDelta(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "GaugeDelta", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "GaugeDelta", 1), ShouldBeTrue)
			})
		})

		Convey("Test Timing", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Timing", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Timing", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Timing(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Timing", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Timing", 1), ShouldBeTrue)
			})
		})

		Convey("Test TimingDuration", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("TimingDuration", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("TimingDuration", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.TimingDuration(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "TimingDuration", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "TimingDuration", 1), ShouldBeTrue)
			})
		})

		Convey("Test Set", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Set", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Set", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Set(stat, "foobar", rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Set", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Set", 1), ShouldBeTrue)
			})
		})

		Convey("Test SetInt", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("SetInt", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("SetInt", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.SetInt(stat, value, rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "SetInt", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "SetInt", 1), ShouldBeTrue)
			})
		})

		Convey("Test Raw", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Raw", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Raw", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Raw(stat, "foobar", rate)

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Raw", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Raw", 1), ShouldBeTrue)
			})
		})

		Convey("Test SetPrefix", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("SetPrefix", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("SetPrefix", Anything, Anything, Anything).Return(nil)

				compositeStatter.SetPrefix("prefix")

				So(mockStatter1.AssertNumberOfCalls(t, "SetPrefix", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "SetPrefix", 1), ShouldBeTrue)
			})
		})

		Convey("Test NewSubStatter", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("NewSubStatter", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("NewSubStatter", Anything, Anything, Anything).Return(nil)

				subStatter := compositeStatter.NewSubStatter("prefix")

				So(subStatter, ShouldNotBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "NewSubStatter", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "NewSubStatter", 1), ShouldBeTrue)
			})
		})

		Convey("Test Close", func() {
			Convey("Happy Case", func() {
				mockStatter1.On("Close", Anything, Anything, Anything).Return(nil)
				mockStatter2.On("Close", Anything, Anything, Anything).Return(nil)

				err := compositeStatter.Close()

				So(err, ShouldBeNil)
				So(mockStatter1.AssertNumberOfCalls(t, "Close", 1), ShouldBeTrue)
				So(mockStatter2.AssertNumberOfCalls(t, "Close", 1), ShouldBeTrue)
			})
		})
	})
}
