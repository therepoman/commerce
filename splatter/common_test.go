package splatter

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	prefix = "prefix"
	suffix = "suffix"
)

func TestJoinPathComponents(t *testing.T) {
	Convey("Test Join Path Components", t, func() {

		Convey("Empty both", func() {
			joined := joinPathComponents("", "")
			So(joined, ShouldEqual, "")
		})

		Convey("Empty suffix", func() {
			joined := joinPathComponents(prefix, "")
			So(joined, ShouldEqual, prefix)
		})

		Convey("Empty prefix", func() {
			joined := joinPathComponents("", suffix)
			So(joined, ShouldEqual, suffix)
		})

		Convey("Present suffix and prefix", func() {
			joined := joinPathComponents(prefix, suffix)
			So(joined, ShouldEqual, prefix+"."+suffix)
		})

		Convey("Trim suffix", func() {
			trimSuffix := ".suffix"
			joined := joinPathComponents(prefix, trimSuffix)
			So(joined, ShouldEqual, prefix+"."+suffix)
		})
	})
}
