package splatter

import (
	"fmt"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/twitchtv/twirp"
	hooks "github.com/twitchtv/twirp/hooks/statsd"
)

// NewStatsdServerHooks wraps the statter to avoid namespace collision.
func NewStatsdServerHooks(statter statsd.Statter) *twirp.ServerHooks {
	return hooks.NewStatsdServerHooks(&twirpStatter{statter})
}

// twirpStatter wraps the two methods that ServerHooks uses.
type twirpStatter struct {
	statsd.Statter
}

// Inc prefixed with "counter".
func (t *twirpStatter) Inc(stat string, value int64, rate float32) error {
	return t.Statter.Inc(fmt.Sprintf("counter.%s", stat), value, rate)
}

// TimingDuration prefixed with "timing".
func (t *twirpStatter) TimingDuration(stat string, duration time.Duration, rate float32) error {
	return t.Statter.TimingDuration(fmt.Sprintf("timing.%s", stat), duration, rate)
}
