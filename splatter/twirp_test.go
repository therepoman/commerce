package splatter

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/splatter/mocks"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTwirpStatter(t *testing.T) {
	Convey("Given a Statter", t, func() {
		mockStatter := &mocks.Statter{}
		statter := twirpStatter{mockStatter}
		Convey("When calling Inc should have counter prefix", func() {
			mockStatter.On("Inc", "counter.foo", int64(11), float32(.13)).Return(nil)
			statter.Inc("foo", 11, .13)
			mockStatter.AssertExpectations(t)
		})

		Convey("When calling TimingDuration should have timing prefix", func() {
			mockStatter.On("TimingDuration", "timing.foo", time.Duration(11*time.Minute), float32(.13)).Return(nil)
			statter.TimingDuration("foo", time.Duration(11*time.Minute), .13)
			mockStatter.AssertExpectations(t)
		})

		Convey("When calling Timing should have no prefix", func() {
			mockStatter.On("Timing", "foo", int64(11), float32(.13)).Return(nil)
			statter.Timing("foo", 11, .13)
			mockStatter.AssertExpectations(t)
		})
	})
}

func TestNewStatsdServerHooks(t *testing.T) {
	Convey("Given hooks", t, func() {
		mockStatter := &mocks.Statter{}
		hooks := NewStatsdServerHooks(mockStatter)
		Convey("When calling RequestReceived should increment counter", func() {
			mockStatter.On("Inc", "counter.twirp.total.requests", int64(1), float32(1.0)).Return(nil)
			hooks.RequestReceived(context.Background())
			mockStatter.AssertExpectations(t)
		})
	})
}
