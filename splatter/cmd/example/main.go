package main

import (
	"time"

	"code.justin.tv/commerce/splatter"
	log "github.com/sirupsen/logrus"

	"github.com/cactus/go-statsd-client/statsd"
)

const (
	statsPrefix = "splatter.example"
)

func main() {
	log.SetLevel(log.DebugLevel)

	// Set up TwitchTelemetry CloudWatch metric statter
	buffConfig := &splatter.BufferedTelemetryConfig{
		FlushPeriod:       30 * time.Second,  // How often to send metrics
		BufferSize:        100000,            // Size of the buffer
		AggregationPeriod: time.Minute,       // Aggregation period of metrics, e.g. 1 minute metrics
		ServiceName:       "TwitchMyService", // Name of your service
		AWSRegion:         "us-west-2",       // AWS Region of your service
		Stage:             "production",      // Main stage of your service (e.g. production, development, etc.)
		Substage:          "canary",          // A substage for the stage (e.g. primary, canary, etc.)
		Prefix:            statsPrefix,       // Optional prefix for all metrics
	}

	// Create a Statter shim for CloudWatch using the given config and an optional map of metrics to ignore
	// If metrics are in the map as "someMetric" -> true, then "someMetric" will not be reported
	telemetryStatter := splatter.NewBufferedTelemetryCloudWatchStatter(buffConfig, map[string]bool{})

	// Create a standard statsd Statter to send metrics to the statsd endpoint
	statsdStatter, err := statsd.NewClient("statsd.internal.justin.tv:8125", statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to connect to statsd endpoint. Make sure you are on JTV-SFO network if running locally.")
	}

	// Create a splatter.LogStatter to send metrics to the logs for debugging
	logStatter, err := splatter.NewLogStatter(statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Log Statter")
	}

	// Build a slice of statters that you want to inject into a splatter.CompositeStatter.
	statters := []statsd.Statter{telemetryStatter, statsdStatter, logStatter}

	// Create a splatter.CompositeStatter and inject all of the Statters into it! All of the individual Statters will be called.
	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Composite Statter")
	}
	defer stats.Close() // This will close all of the child Statters

	// Congrats, now we have a single Statter interface that sends to multiple destinations. Lets send some metrics!
	runSomeMetrics(stats)
}

// Call the stats interface with a few samples
func runSomeMetrics(stats statsd.Statter) {
	stats.Inc("mycounter", 1, 1.0)
	stats.Inc("myothercounter", 4, 0.5)
	latency := time.Millisecond * time.Duration(350)
	stats.TimingDuration("mylatency", latency, 1.0)
}
