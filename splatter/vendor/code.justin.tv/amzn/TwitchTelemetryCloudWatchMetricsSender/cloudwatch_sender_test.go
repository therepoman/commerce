package cloudwatch

import (
	"net/http"
	"testing"
	"time"

	"fmt"
	"math/rand"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	testService            = "TwitchTelemetryCWTestService"
	testStage              = "Test"
	testSubstage           = "Substage"
	testRegion             = "TestRegion"
	testAPI                = "TestAPI"
	testDependency         = "TestDep"
	testExtraDimName       = "TestDim"
	testExtraDimVal        = "TestVal"
	testMetricName         = "CWMetricTest"
	testValMin             = 0.0
	testValMax             = 123.4
	testIgnoreVal          = ""
	testNoRollup           = "noRollup"
	testRollupOper         = "rollupOper"
	testRollupOperDep      = "rollupOperDep"
	testRollupOperDepExtra = "rollupOperDepExtra"
)

type mockCWClient struct {
	cloudwatchiface.CloudWatchAPI
	processedInput []*cloudwatch.PutMetricDataInput
}

func (m *mockCWClient) PutMetricDataWithContext(ctx aws.Context, input *cloudwatch.PutMetricDataInput, opt ...request.Option) (*cloudwatch.PutMetricDataOutput, error) {
	m.processedInput = append(m.processedInput, input)
	return nil, nil
}

type mockCWSmallRequestClient struct {
	cloudwatchiface.CloudWatchAPI
	processedInput   []*cloudwatch.PutMetricDataInput
	allowedInputSize int
}

func (m *mockCWSmallRequestClient) PutMetricDataWithContext(ctx aws.Context, input *cloudwatch.PutMetricDataInput, opt ...request.Option) (*cloudwatch.PutMetricDataOutput, error) {
	if len(input.MetricData) > m.allowedInputSize {
		return nil, requestSizeError
	}
	m.processedInput = append(m.processedInput, input)
	return nil, nil
}

func TestCWSender(t *testing.T) {

	processIdentifier := &identifier.ProcessIdentifier{
		Service:  testService,
		Stage:    testStage,
		Substage: testSubstage,
		Region:   testRegion,
	}

	Convey("Given 2 new metric samples", t, func() {
		currTime := time.Now()
		period := time.Minute
		truncatedTime := currTime.Truncate(period)
		// Create twp samples, observe them, and flush them
		minSample := createTestSampleWithRollups(currTime, testValMin)
		maxSample := createTestSampleWithRollups(currTime, testValMax)

		Convey("And a mock buffered CloudWatch client", func() {
			mockCW := &mockCWClient{processedInput: make([]*cloudwatch.PutMetricDataInput, 0)}
			common := &CommonSender{
				client:            mockCW,
				processIdentifier: processIdentifier,
			}
			cw := &Buffered{
				CommonSender: common,
				aggregator:   telemetry.NewAggregator(period),
				stopSignal:   make(chan bool),
			}

			Convey("When samples are observed", func() {
				cw.ObserveSample(&minSample)
				cw.ObserveSample(&maxSample)
				// This has to be done outside the convey or it is run multiple times
				cw.Flush()

				// Obtain the CW input list
				inputList := mockCW.processedInput
				cwTestVerification(inputList, truncatedTime, "manual flush case")
			})

			Convey("When no samples are observed", func() {
				cw.Flush()

				Convey("Then CloudWatch is not called.", func() {
					So(mockCW.processedInput, ShouldBeEmpty)
				})
			})
		})

		Convey("And a mock unbuffered CloudWatch client", func() {
			mockCW := &mockCWClient{processedInput: make([]*cloudwatch.PutMetricDataInput, 0)}
			common := &CommonSender{
				client:            mockCW,
				processIdentifier: processIdentifier,
			}
			cw := &Unbuffered{
				CommonSender: common,
			}

			Convey("When samples are send", func() {
				// Normally, we use a BufferedAggregator for calling the Unbuffered sender. However, since we're testing
				// the functionality of the CloudWatch sender, we will instead generate a distribution from a standard
				// Aggregator and manually call the Unbuffered sender's FlushWithoutBuffering method
				aggregator := telemetry.NewAggregator(period)
				aggregator.AggregateSample(&minSample)
				aggregator.AggregateSample(&maxSample)
				distribution := aggregator.Flush()
				cw.FlushWithoutBuffering(distribution)

				// Obtain the CW input list
				inputList := mockCW.processedInput
				cwTestVerification(inputList, truncatedTime, "manual flush case")
			})

			Convey("When no samples are observed", func() {
				cw.FlushWithoutBuffering([]*telemetry.Distribution{})

				Convey("Then CloudWatch is not called.", func() {
					So(mockCW.processedInput, ShouldBeEmpty)
				})
			})
		})
	})
}

func TestBufferedRun(t *testing.T) {
	processIdentifier := &identifier.ProcessIdentifier{
		Service:  testService,
		Stage:    testStage,
		Substage: testSubstage,
		Region:   testRegion,
	}

	Convey("Given 2 new metric samples", t, func() {
		currTime := time.Now()
		period := time.Minute
		truncatedTime := currTime.Truncate(period)
		// Create twp samples, observe them, and flush them
		minSample := createTestSampleWithRollups(currTime, testValMin)
		maxSample := createTestSampleWithRollups(currTime, testValMax)

		Convey("And a mock buffered CloudWatch client", func() {
			mockCW := &mockCWClient{processedInput: make([]*cloudwatch.PutMetricDataInput, 0)}
			common := &CommonSender{
				client:            mockCW,
				processIdentifier: processIdentifier,
			}
			flushPeriod := 5 * time.Second
			cw := &Buffered{
				CommonSender:  common,
				aggregator:    telemetry.NewAggregator(period),
				stopSignal:    make(chan bool),
				nextAutoFlush: time.Now(),
				flushPeriod:   flushPeriod,
			}

			Convey("Validate the auto-flush behavior", func() {
				cw.run()

				// Sleep for 2 second to create an offset between the auto-flush and normal (manual) flush
				time.Sleep(2 * time.Second)

				// Observe and flush samples, verifying they are flushed right away
				cw.ObserveSample(&minSample)
				cw.ObserveSample(&maxSample)
				cw.Flush()
				inputList := mockCW.processedInput
				cwTestVerification(inputList, truncatedTime, "manual flush case")

				mockCW.processedInput = make([]*cloudwatch.PutMetricDataInput, 0)
				inputList = mockCW.processedInput

				// Observe the same samples again, but don't flush manually. processedInput should still be empty
				cw.ObserveSample(&minSample)
				cw.ObserveSample(&maxSample)
				time.Sleep(flushPeriod)
				inputList = mockCW.processedInput
				So(inputList, ShouldBeEmpty)

				// Sleep for the additional time, verifying things have now been flushed
				time.Sleep(flushPeriod)
				inputList = mockCW.processedInput
				cwTestVerification(inputList, truncatedTime, "auto flush case")
			})
		})
	})
}

func cwTestVerification(inputList []*cloudwatch.PutMetricDataInput, truncatedTime time.Time, testCase string) {
	Convey(fmt.Sprintf("Validate the input to CloudWatch for %v", testCase), func() {
		// Ensure CloudWatch was only called once

		So(len(inputList), ShouldEqual, 1)
		input := inputList[0]
		Convey("Validate the CloudWatch namespace", func() {
			So(input.Namespace, ShouldResemble, aws.String(testService))
		})

		Convey("Validate the list of datums", func() {
			datums := input.MetricData

			So(datums, ShouldNotBeEmpty)
			// Since the two samples are the same except for values, there are only 6 total metrics (because of rollups)
			So(len(datums), ShouldEqual, 4)

			Convey("Validate the actual individual datums", func() {
				Convey("Validate the common components", func() {
					for _, metric := range datums {
						So(*metric.MetricName, ShouldEqual, testMetricName)
						So(metric.Timestamp.Format(time.RFC3339), ShouldEqual, truncatedTime.UTC().Format(time.RFC3339))
						So(*metric.Unit, ShouldEqual, telemetry.UnitCount)
						So(metric.Value, ShouldBeNil)
						So(metric.StorageResolution, ShouldBeNil)
						So(metric.StatisticValues, ShouldNotBeNil)
						So(*metric.StatisticValues.Sum, ShouldEqual, testValMax+testValMin)
						So(*metric.StatisticValues.Maximum, ShouldEqual, testValMax)
						So(*metric.StatisticValues.Minimum, ShouldEqual, testValMin)
						So(*metric.StatisticValues.SampleCount, ShouldEqual, 2)
						So(len(metric.Values), ShouldEqual, 2)
						So(*metric.Values[0], ShouldEqual, testValMin)
						// The approximate value of 123.4 is ~123.12 because bucket indexes are ints
						So(*metric.Values[1], ShouldBeBetween, 123, 124)
						So(len(metric.Counts), ShouldEqual, 2)
						So(*metric.Counts[0], ShouldEqual, 1)
						So(*metric.Counts[1], ShouldEqual, 1)
					}
				})

				Convey("Validate the differences (the dimensions)", func() {
					// Manually construct the rollups
					noRollup := createTestDimensions(testAPI, testDependency, testExtraDimVal)
					rollupOper := createTestDimensions(testIgnoreVal, testDependency, testExtraDimVal)
					rollupOperDep := createTestDimensions(testIgnoreVal, testIgnoreVal, testExtraDimVal)
					rollupOperDepExtra := createTestDimensions(testIgnoreVal, testIgnoreVal, testIgnoreVal)

					// Create a map with the rollups to keep track of the number of times each one is found
					dimMap := make(map[string]int)
					dimMap[testNoRollup] = 0
					dimMap[testRollupOper] = 0
					dimMap[testRollupOperDep] = 0
					dimMap[testRollupOperDepExtra] = 0

					// Determine the number of times each list of dimensions (rollups) appears
					for _, metric := range datums {
						if verifyCWDims(metric.Dimensions, noRollup) {
							dimMap[testNoRollup] = dimMap[testNoRollup] + 1
						} else if verifyCWDims(metric.Dimensions, rollupOper) {
							dimMap[testRollupOper] = dimMap[testRollupOper] + 1
						} else if verifyCWDims(metric.Dimensions, rollupOperDep) {
							dimMap[testRollupOperDep] = dimMap[testRollupOperDep] + 1
						} else if verifyCWDims(metric.Dimensions, rollupOperDepExtra) {
							dimMap[testRollupOperDepExtra] = dimMap[testRollupOperDepExtra] + 1
						} else {
							// Ensure we immediately break if any of dimensions exist
							So(false, ShouldBeTrue)
						}
					}
					// Now, validate that each item appeared the expected number of times by validating the number
					// of occurrences of each dimension
					So(len(dimMap), ShouldEqual, 4)
					So(dimMap[testNoRollup], ShouldEqual, 1)
					So(dimMap[testRollupOper], ShouldEqual, 1)
					So(dimMap[testRollupOperDep], ShouldEqual, 1)
					So(dimMap[testRollupOperDepExtra], ShouldEqual, 1)
				})
			})
		})
	})
}

func TestCWSenderRequestHalvingEvenSamples(t *testing.T) {
	Convey("Given 4 new metric samples with 3 rollups each (16 total unique metrics)", t, func() {
		// Try clients with a different number of allowed input sizes
		for i := 1; i < 18; i++ {
			Convey(fmt.Sprintf("And a mock CloudWatch client that allows %d datums per request", i), func() {
				var expectedCalls []int
				if i >= 8 && i < 16 {
					// For i >= 8, i < 16; the total samples need to be split one time (2^1 calls of 8 each)
					expectedCalls = []int{8, 8}
				} else if i >= 4 && i < 8 {
					// For i >= 4, i < 8; the total samples need to be split two times each (2^2 calls of 4 each)
					expectedCalls = []int{4, 4, 4, 4}
				} else if i >= 2 && i < 4 {
					// For i >= 2, i < 4; the total samples need to be split three times each (2^3 calls of 2 each)
					expectedCalls = []int{2, 2, 2, 2, 2, 2, 2, 2}
				} else if i == 1 {
					// For i = 1; the total samples need to be split four times each (2^4 calls)
					expectedCalls = []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
				} else {
					// For everything else, there's no splitting
					expectedCalls = []int{16}
				}
				verifyCorrectHalving(true, i, 4, expectedCalls)
			})
		}
	})
}

func TestCWSenderRequestHalvingOddSamples(t *testing.T) {
	Convey("Given 11 new metric samples with no rollups each (11 total unique metrics)", t, func() {
		// Try clients with a different number of allowed input sizes
		for i := 1; i < 13; i++ {
			Convey(fmt.Sprintf("And a mock CloudWatch client that allows %d datums per request", i), func() {
				var expectedCalls []int
				if i >= 6 && i < 11 {
					// For i >= 6, i < 11; the total samples need to be split to create calls of size (6, 5)
					expectedCalls = []int{6, 5}
				} else if i == 5 {
					// For i = 5; the total samples need to be split to create calls of size (3, 3, 5)
					expectedCalls = []int{3, 3, 5}
				} else if i == 3 || i == 4 {
					// For i = 3 or 4; the total samples need to be split to create calls of size (3, 3, 3, 2)
					expectedCalls = []int{3, 3, 3, 2}
				} else if i == 2 {
					// For i = 2; the total samples need to be split to create calls of size (2, 1, 2, 1, 2, 1, 2)
					expectedCalls = []int{2, 1, 2, 1, 2, 1, 2}
				} else if i == 1 {
					// For i = 1; the total samples need to be split to 11 calls of 1 element each
					for elem := 0; elem < 11; elem++ {
						expectedCalls = []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
					}
				} else {
					// For everything else, there's no splitting
					expectedCalls = []int{11}
				}
				verifyCorrectHalving(false, i, 11, expectedCalls)
			})
		}
	})
}

func TestDefaultHttpTimeoutUntouched(t *testing.T) {
	defaultTimeout := http.DefaultClient.Timeout
	processIdentifier := &identifier.ProcessIdentifier{
		Service:  testService,
		Stage:    testStage,
		Substage: testSubstage,
		Region:   testRegion,
	}
	_ = New(processIdentifier, nil)

	if http.DefaultClient.Timeout != defaultTimeout {
		t.Error("default timeout should not be modified")
	}
}

func verifyCorrectHalving(rollup bool, allowedInputSize int, numberOfSamples int, expectedCalls []int) {
	processIdentifier := &identifier.ProcessIdentifier{
		Service:  testService,
		Stage:    testStage,
		Substage: testSubstage,
		Region:   testRegion,
	}

	mockCW := &mockCWSmallRequestClient{processedInput: make([]*cloudwatch.PutMetricDataInput, 0), allowedInputSize: allowedInputSize}
	common := &CommonSender{
		client:            mockCW,
		processIdentifier: processIdentifier,
	}
	cw := &Buffered{
		CommonSender: common,
		aggregator:   telemetry.NewAggregator(time.Minute),
		stopSignal:   make(chan bool),
	}

	for sampleNum := 0; sampleNum < numberOfSamples; sampleNum++ {
		newSample := createCustomNamedTestSample(fmt.Sprintf("SomeMetric-%d", sampleNum), time.Now(), float64(rand.Intn(1000)), rollup)
		cw.ObserveSample(&newSample)
	}

	cw.Flush()
	// Verify there were the expected number of calls
	So(len(mockCW.processedInput), ShouldEqual, len(expectedCalls))
	// Verify each call had the expected number of elements
	for i, expectedCallNumber := range expectedCalls {
		So(len(mockCW.processedInput[i].MetricData), ShouldEqual, expectedCallNumber)
	}
}

func createCustomNamedTestSample(name string, currTime time.Time, value float64, rollups bool) telemetry.Sample {
	testSample := telemetry.Sample{}
	testSample.MetricID = *telemetry.NewMetricID(name)

	// Create initial dimensions
	testSample.MetricID.AddDimension(telemetry.DimensionService, testService)
	testSample.MetricID.AddDimension(telemetry.DimensionStage, testStage)
	testSample.MetricID.AddDimension(telemetry.DimensionSubstage, testSubstage)
	testSample.MetricID.AddDimension(telemetry.DimensionRegion, testRegion)
	testSample.MetricID.AddDimension(telemetry.DimensionOperation, testAPI)
	testSample.MetricID.AddDimension(telemetry.DimensionDependency, testDependency)
	testSample.MetricID.AddDimension(testExtraDimName, testExtraDimVal)

	allDimensionRollups := []string{}
	allDimensionRollups = append(allDimensionRollups, telemetry.DimensionOperation)
	allDimensionRollups = append(allDimensionRollups, telemetry.DimensionDependency)
	allDimensionRollups = append(allDimensionRollups, testExtraDimName)

	// Create additional dimensions
	testSample.RollupDimensions = [][]string{}
	if rollups {
		for rollupNum := 0; rollupNum < len(allDimensionRollups); rollupNum++ {
			testSample.RollupDimensions = append(testSample.RollupDimensions, []string{})
			testSample.RollupDimensions[rollupNum] = append(testSample.RollupDimensions[rollupNum], allDimensionRollups[:rollupNum+1]...)
		}
	}

	// Finalize the metric
	testSample.Value = value
	testSample.Unit = telemetry.UnitCount
	testSample.Timestamp = currTime
	return testSample
}

func createTestSampleWithRollups(currTime time.Time, value float64) telemetry.Sample {
	return createCustomNamedTestSample(testMetricName, currTime, value, true)
}

func createTestDimensions(operation, dependency, extraDim string) map[*cloudwatch.Dimension]int {
	cwDims := make(map[*cloudwatch.Dimension]int)
	cwDims[createTestDimension(telemetry.DimensionService, testService)] = 1
	cwDims[createTestDimension(telemetry.DimensionStage, testStage)] = 1
	cwDims[createTestDimension(telemetry.DimensionSubstage, testSubstage)] = 1
	cwDims[createTestDimension(telemetry.DimensionRegion, testRegion)] = 1
	if operation != "" {
		cwDims[createTestDimension(telemetry.DimensionOperation, operation)] = 1
	}
	if dependency != "" {
		cwDims[createTestDimension(telemetry.DimensionDependency, dependency)] = 1
	}
	if extraDim != "" {
		cwDims[createTestDimension(testExtraDimName, extraDim)] = 1
	}
	return cwDims
}

func createTestDimension(name string, value string) *cloudwatch.Dimension {
	cwDim := cloudwatch.Dimension{}
	cwDim.SetName(name)
	cwDim.SetValue(value)
	return &cwDim
}

func verifyCWDims(dimList []*cloudwatch.Dimension, dimMap map[*cloudwatch.Dimension]int) bool {
	// Ensure the map and the list have the same length
	if len(dimList) != len(dimMap) {
		return false
	}

	// Ensure every value is in the map and has only been accessed the correct number of times
	// This is obv not sure great, but we need a deep equals since the keys are pointers to structs with pointers
	// and equality fails otherwise
	for _, dim := range dimList {
		foundMatch := false
		for key, value := range dimMap {
			// Perform a deep equals and ensure the value has never been accessed before
			if awsutil.DeepEqual(key, dim) && value == 1 {
				// Enforce that the value has already been checked
				dimMap[key] = 2
				foundMatch = true
			}
		}
		if foundMatch == false {
			// Reset the map
			for key := range dimMap {
				dimMap[key] = 1
			}
			return false
		}
	}

	// Reset the map
	for key := range dimMap {
		dimMap[key] = 1
	}
	return true
}
