package mws

import "code.justin.tv/video/mwsclient"

// Common metric string values provided for convenience and consistency.
// These are not the only values supported by MWS/PMET, but using them will produce
// metrics consistent with standard Amazon service metrics.
const (
	PeriodOneMinute  = "OneMinute"
	PeriodFiveMinute = "FiveMinute"
	PeriodOneHour    = "OneHour"

	StageTest = "Test"

	MetricTime = "Time"

	NamespaceNetwork = "Amazon/Schema/Network"
	NamespaceService = "Amazon/Schema/Service"

	NoValue     = "NONE"
	RollupValue = "ALL"

	ZeroBucket = "Zeros"
)

var defaultMetricReport = mwsclient.MetricReport{
	// We will almost always want the service schema.  Use "Amazon/Schema/Network" for network data.
	Namespace: NamespaceService,
}

var defaultMetricMetadata = mwsclient.MetricMetadata{
	// Ensure stage is set
	Stage: StageTest,
	// These value should never be changed. They are no longer used, but are required.
	MinPeriod: PeriodOneMinute,
	MaxPeriod: PeriodOneHour,
}

var defaultMetricDimensions = mwsclient.ServiceSchemaDimensions{
	DataSet:     StageTest,
	HostGroup:   NoValue,
	Host:        NoValue,
	MethodName:  NoValue,
	Client:      NoValue,
	MetricClass: NoValue,
	Instance:    NoValue,
}

// newMetricReport creates a new MetricReport with provided metadata
func newMetricReport(customerID, host, environment, stage string) mwsclient.MetricReport {
	var metadata = defaultMetricMetadata

	// Set optional values (these can all be empty strings)
	metadata.CustomerID = customerID
	metadata.Host = host
	metadata.Environment = environment

	// PartitionId should be the host
	metadata.PartitionID = host

	// Specify the stage if possible
	if stage != "" {
		metadata.Stage = stage
	}

	// Construct a base report with no metrics
	var report = defaultMetricReport
	report.Metadata = &metadata
	report.Metrics = []*mwsclient.Metric{}
	return report
}

// newSingleDefaultMetric creates a new single Metric with default dimensions
func newSingleDefaultMetric(metricName string) mwsclient.Metric {
	var metric mwsclient.Metric
	metric.MetricName = metricName
	metric.Dimensions = &defaultMetricDimensions
	metric.Values = make([]float64, 0, 1)
	return metric
}
