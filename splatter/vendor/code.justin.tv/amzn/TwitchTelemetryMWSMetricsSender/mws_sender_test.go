package mws

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/video/mwsclient"
	"github.com/aws/aws-sdk-go/aws/request"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	testService      = "TwitchTelemetryMWSTestService"
	testStage        = "Test"
	testSubstage     = "Substage"
	testRegion       = "TestRegion"
	testAPI          = "TestAPI"
	testDependency   = "TestDep"
	testExtraDimName = "TestDim"
	testExtraDimVal  = "TestVal"
	testMetricName   = "MWSMetricTest"
	testValMin       = 0.0
	testValMax       = 123.4
)

type mockMWSClient struct {
	processedInput []*mwsclient.PutMetricDataForAggregationInput
}

func (m *mockMWSClient) PutMetricDataForAggregation(ctx context.Context, input *mwsclient.PutMetricDataForAggregationInput, opts ...request.Option) (*mwsclient.PutMetricDataForAggregationOutput, error) {
	m.processedInput = append(m.processedInput, input)
	return nil, nil
}

func TestMWSSender(t *testing.T) {

	processIdentifier := &identifier.ProcessIdentifier{
		Service:  testService,
		Stage:    testStage,
		Substage: testSubstage,
		Region:   testRegion,
	}

	Convey("Given 2 new metric samples", t, func() {
		currTime := time.Now()
		period := time.Minute
		truncatedTime := currTime.Truncate(period)
		minSample := createTestSample(currTime, testValMin)
		maxSample := createTestSample(currTime, testValMax)

		Convey("And a mock buffered MWS client", func() {
			mockMWS := &mockMWSClient{processedInput: make([]*mwsclient.PutMetricDataForAggregationInput, 0)}
			common := &CommonSender{
				client:            mockMWS,
				processIdentifier: processIdentifier,
			}
			mws := &Buffered{
				CommonSender: common,
				aggregator:   telemetry.NewAggregator(period),
				stopSignal:   make(chan bool),
			}

			Convey("When samples are observed", func() {
				mws.ObserveSample(&minSample)
				mws.ObserveSample(&maxSample)
				mws.Flush()

				// Obtain the MWS input list
				inputList := mockMWS.processedInput
				mwsTestVerification(inputList, truncatedTime)
			})

			Convey("When no samples are observed", func() {
				mws.Flush()

				Convey("Then MWS is not called.", func() {
					So(mockMWS.processedInput, ShouldBeEmpty)
				})
			})
		})

		Convey("And a mock unbuffered MWS client", func() {
			mockMWS := &mockMWSClient{processedInput: make([]*mwsclient.PutMetricDataForAggregationInput, 0)}
			common := &CommonSender{
				client:            mockMWS,
				processIdentifier: processIdentifier,
			}
			mws := &Unbuffered{
				CommonSender: common,
			}

			Convey("When samples are send", func() {
				// Normally, we use a BufferedAggregator for calling the Unbuffered sender. However, since we're testing
				// the functionality of the MWS sender, we will instead generate a distribution from a standard
				// Aggregator and manually call the Unbuffered sender's FlushWithoutBuffering method
				aggregator := telemetry.NewAggregator(period)
				aggregator.AggregateSample(&minSample)
				aggregator.AggregateSample(&maxSample)
				distribution := aggregator.Flush()
				mws.FlushWithoutBuffering(distribution)

				// Obtain the MWS input list
				inputList := mockMWS.processedInput
				mwsTestVerification(inputList, truncatedTime)
			})

			Convey("When no samples are observed", func() {
				mws.FlushWithoutBuffering([]*telemetry.Distribution{})

				Convey("Then MWS is not called.", func() {
					So(mockMWS.processedInput, ShouldBeEmpty)
				})
			})
		})
	})
}

func mwsTestVerification(inputList []*mwsclient.PutMetricDataForAggregationInput, truncatedTime time.Time) {
	Convey("Validate the input to MWS", func() {
		// Ensure MWS was only called once
		So(len(inputList), ShouldEqual, 1)
		input := inputList[0]

		Convey("Validate the metric report", func() {
			reports := input.MetricReports
			So(reports, ShouldNotBeEmpty)
			So(len(reports), ShouldEqual, 1)
			report := reports[0]
			So(report, ShouldNotBeEmpty)

			Convey("Validate the namespace", func() {
				So(report.Namespace, ShouldEqual, NamespaceService)
			})

			Convey("Validate the metadata", func() {
				So(report.Metadata.PartitionID, ShouldEqual, NoInfo)
				So(report.Metadata.MinPeriod, ShouldEqual, PeriodOneMinute)
				So(report.Metadata.MaxPeriod, ShouldEqual, PeriodOneHour)
				So(report.Metadata.Environment, ShouldEqual, testService)
				So(report.Metadata.CustomerID, ShouldEqual, NoInfo)
				So(report.Metadata.Host, ShouldEqual, NoInfo)
				So(report.Metadata.Stage, ShouldEqual, testStage)
			})

			Convey("Validate the actual metrics", func() {
				// Validate metrics
				So(report.Metrics, ShouldNotBeEmpty)
				// Since the two samples are the same except for values, there are only 6 total metrics (because of rollups)
				So(len(report.Metrics), ShouldEqual, 5)

				Convey("Validate the common components", func() {
					for _, metric := range report.Metrics {
						So(metric.Weights, ShouldBeEmpty)
						So(metric.Values, ShouldBeEmpty)
						So(metric.AggregationType, ShouldBeEmpty)
						So(metric.Unit, ShouldEqual, telemetry.UnitCount)
						So(metric.Timestamp.Format(time.RFC3339), ShouldEqual, truncatedTime.UTC().Format(time.RFC3339))
						So(metric.MetricName, ShouldEqual, testMetricName)
						So(metric.Distribution, ShouldNotBeEmpty)
						So(metric.Distribution.Sum, ShouldEqual, testValMin+testValMax)
						So(metric.Distribution.Maximum, ShouldEqual, testValMax)
						So(metric.Distribution.Minimum, ShouldEqual, testValMin)
						So(metric.Distribution.SampleCount, ShouldEqual, 2)
						So(metric.Distribution.SEH1, ShouldNotBeEmpty)
						So(len(metric.Distribution.SEH1), ShouldEqual, 2)
						// This was pre-computed to make sure the correct bucket is used
						So(metric.Distribution.SEH1["50"], ShouldEqual, 1)
						So(metric.Distribution.SEH1[ZeroBucket], ShouldEqual, 1)
					}
				})

				Convey("Validate the differences (the dimensions)", func() {
					// Manually construct the rollups
					allDims := createTestDimensions(testSubstage, testAPI, telemetry.DimensionDependency+":"+testDependency, testExtraDimName+":"+testExtraDimVal)
					rollupSub := createTestDimensions("ALL", testAPI, telemetry.DimensionDependency+":"+testDependency, testExtraDimName+":"+testExtraDimVal)
					rollupSubOper := createTestDimensions("ALL", RollupValue, telemetry.DimensionDependency+":"+testDependency, testExtraDimName+":"+testExtraDimVal)
					rollupSubOperDep := createTestDimensions("ALL", RollupValue, telemetry.DimensionDependency+":"+RollupValue, testExtraDimName+":"+testExtraDimVal)
					rollupSubOperExtra := createTestDimensions("ALL", RollupValue, telemetry.DimensionDependency+":"+RollupValue, testExtraDimName+":"+RollupValue)
					// Create a map with the rollups to keep track of the number of times each one is found
					dimMap := make(map[mwsclient.ServiceSchemaDimensions]int)
					dimMap[allDims] = 0
					dimMap[rollupSub] = 0
					dimMap[rollupSubOper] = 0
					dimMap[rollupSubOperDep] = 0
					dimMap[rollupSubOperExtra] = 0
					// Determine the number of times each rollup appears
					for _, metric := range report.Metrics {
						// This is some pretty gross conversation we need to do to map from an isSchemaDimensions interface to a
						// ServiceSchemaDimensions struct. This also enables us to validate the underlying structure as opposed
						// to the memory addresses
						dim := *metric.Dimensions.(*mwsclient.ServiceSchemaDimensions)
						dimMap[dim] = dimMap[dim] + 1
					}
					// Now, validate that each item appeared the expected number of times by validating the number of occurrences
					// of each dimension
					So(dimMap[allDims], ShouldEqual, 1)
					So(dimMap[rollupSub], ShouldEqual, 1)
					So(dimMap[rollupSubOper], ShouldEqual, 1)
					So(dimMap[rollupSubOperDep], ShouldEqual, 1)
					So(dimMap[rollupSubOperExtra], ShouldEqual, 1)
				})
			})
		})
	})
}

func createTestSample(currTime time.Time, value float64) telemetry.Sample {
	testSample := telemetry.Sample{}
	testSample.MetricID = *telemetry.NewMetricID(testMetricName)

	// Create initial dimensions
	testSample.MetricID.AddDimension(telemetry.DimensionService, testService)
	testSample.MetricID.AddDimension(telemetry.DimensionStage, testStage)
	testSample.MetricID.AddDimension(telemetry.DimensionSubstage, testSubstage)
	testSample.MetricID.AddDimension(telemetry.DimensionRegion, testRegion)
	testSample.MetricID.AddDimension(telemetry.DimensionOperation, testAPI)
	testSample.MetricID.AddDimension(telemetry.DimensionDependency, testDependency)
	testSample.MetricID.AddDimension(testExtraDimName, testExtraDimVal)

	allDimensionRollups := []string{}
	allDimensionRollups = append(allDimensionRollups, telemetry.DimensionSubstage)
	allDimensionRollups = append(allDimensionRollups, telemetry.DimensionOperation)
	allDimensionRollups = append(allDimensionRollups, telemetry.DimensionDependency)
	allDimensionRollups = append(allDimensionRollups, testExtraDimName)

	// Create additional dimensions
	testSample.RollupDimensions = [][]string{}
	for rollupNum := 0; rollupNum < len(allDimensionRollups); rollupNum++ {
		testSample.RollupDimensions = append(testSample.RollupDimensions, []string{})
		testSample.RollupDimensions[rollupNum] = append(testSample.RollupDimensions[rollupNum], allDimensionRollups[:rollupNum+1]...)
	}

	// Finalize the metric
	testSample.Value = value
	testSample.Unit = telemetry.UnitCount
	testSample.Timestamp = currTime
	return testSample
}

func createTestDimensions(hostgroup, methodName, metricClass, instance string) mwsclient.ServiceSchemaDimensions {
	testDims := mwsclient.ServiceSchemaDimensions{
		DataSet:     testStage,
		Marketplace: testRegion,
		HostGroup:   hostgroup,
		Host:        NoValue,
		ServiceName: testService,
		MethodName:  methodName,
		Client:      NoValue,
		MetricClass: metricClass,
		Instance:    instance,
	}
	return testDims
}
