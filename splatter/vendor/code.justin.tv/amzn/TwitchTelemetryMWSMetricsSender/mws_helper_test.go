package mws

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

func TestNewMetricReport(t *testing.T) {
	Convey("Given new metric report parameters", t, func() {
		var customerID, host, environment, stage = "customerId", "host", "environment", "stage"
		Convey("Validate when using a valid stage", func() {
			metricReport := newMetricReport(customerID, host, environment, stage)
			So(metricReport.Metrics, ShouldBeEmpty)
			So(metricReport.Namespace, ShouldEqual, NamespaceService)
			So(metricReport.Metadata.Stage, ShouldEqual, stage)
			So(metricReport.Metadata.Host, ShouldEqual, host)
			So(metricReport.Metadata.CustomerID, ShouldEqual, customerID)
			So(metricReport.Metadata.Environment, ShouldEqual, environment)
			So(metricReport.Metadata.MaxPeriod, ShouldEqual, PeriodOneHour)
			So(metricReport.Metadata.MinPeriod, ShouldEqual, PeriodOneMinute)
			So(metricReport.Metadata.PartitionID, ShouldEqual, host)
		})
		Convey("Validate when using an invalid stage", func() {
			metricReport := newMetricReport(customerID, host, environment, "")
			So(metricReport.Metrics, ShouldBeEmpty)
			So(metricReport.Namespace, ShouldEqual, NamespaceService)
			So(metricReport.Metadata.Stage, ShouldEqual, StageTest)
			So(metricReport.Metadata.Host, ShouldEqual, host)
			So(metricReport.Metadata.CustomerID, ShouldEqual, customerID)
			So(metricReport.Metadata.Environment, ShouldEqual, environment)
			So(metricReport.Metadata.MaxPeriod, ShouldEqual, PeriodOneHour)
			So(metricReport.Metadata.MinPeriod, ShouldEqual, PeriodOneMinute)
			So(metricReport.Metadata.PartitionID, ShouldEqual, host)
		})
	})
}

func TestNewSingleDefaultMetric(t *testing.T) {
	Convey("Validate a new default metric", t, func() {
		metricName := "someMetric"
		metric := newSingleDefaultMetric(metricName)
		So(metric.Distribution, ShouldBeNil)
		So(metric.Dimensions, ShouldEqual, &defaultMetricDimensions)
		So(metric.AggregationType, ShouldBeEmpty)
		So(metric.Unit, ShouldBeEmpty)
		So(metric.Timestamp.Format(time.RFC3339), ShouldEqual, time.Time{}.Format(time.RFC3339))
		So(metric.MetricName, ShouldEqual, metricName)
		So(metric.Values, ShouldBeEmpty)
		So(metric.Weights, ShouldBeEmpty)
	})
}
