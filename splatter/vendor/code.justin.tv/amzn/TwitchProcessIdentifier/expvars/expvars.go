package expvars

import (
	"expvar"
	"sync"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
)

var (
	processIdentifierMu sync.Mutex
	processIdentifier   *identifier.ProcessIdentifier
)

func init() {
	expvar.Publish("ProcessIdentifier", expvar.Func(func() interface{} {
		processIdentifierMu.Lock()
		defer processIdentifierMu.Unlock()

		return processIdentifier
	}))
}

// Publish updates the ProcessIdentifier expvar with the provided identifier instance.
func Publish(pid *identifier.ProcessIdentifier) {
	processIdentifierMu.Lock()
	defer processIdentifierMu.Unlock()

	processIdentifier = new(identifier.ProcessIdentifier)
	*processIdentifier = *pid
}
