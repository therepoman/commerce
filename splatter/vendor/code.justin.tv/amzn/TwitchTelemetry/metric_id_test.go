package telemetry

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMetricID(t *testing.T) {
	Convey("Given a MetricID", t, func() {
		mid := NewMetricID("Foobar")
		Convey("And a dimension is added using AddDimension", func() {
			mid.AddDimension("CoolName", "CoolValue")
			Convey("Then the MetricID should now contain that dimension", func() {
				So(mid.Dimensions["CoolName"], ShouldResemble, "CoolValue")
			})
		})
		Convey("And a dimension is added using WithDimension", func() {
			mid2 := mid.WithDimension("AwesomeName", "AwesomeValue")
			Convey("Then the MetricID should now contain that dimension", func() {
				So(mid.Dimensions["AwesomeName"], ShouldResemble, "AwesomeValue")
			})
			Convey("Then the MetricID return itself", func() {
				So(mid2, ShouldEqual, mid)
			})
		})
	})
}
