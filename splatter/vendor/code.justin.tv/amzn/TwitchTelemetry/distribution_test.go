package telemetry

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDistribution(t *testing.T) {
	Convey("Given a new distribution from a Sample", t, func() {
		metricID := NewMetricID("MyMetric")
		metricID.AddDimension("CoolName", "CoolValue")
		metricID.AddDimension("AwesomeName", "AwesomeValue")
		metricID.AddDimension("SuperName", "SuperValue")
		s := &Sample{
			Value:    float64(1.2),
			MetricID: *metricID,
			RollupDimensions: [][]string{
				[]string{"CoolName"},
				[]string{"AwesomeName", "CoolName"},
			},
			Timestamp: time.Now(),
			Unit:      UnitCount,
		}

		dist := NewDistributionFromSample(s)

		Convey("Then the distributions min, max, and sum should equal the sample value", func() {
			So(dist.Maximum, ShouldEqual, float64(1.2))
			So(dist.Minimum, ShouldEqual, float64(1.2))
			So(dist.Sum, ShouldEqual, float64(1.2))
		})
		Convey("Then the sample count should be 1", func() {
			So(dist.SampleCount, ShouldEqual, 1)
		})

		Convey("And a second and third sample are added to the distribution", func() {
			s2 := &Sample{
				Value:    float64(1.0),
				MetricID: *metricID,
				RollupDimensions: [][]string{
					[]string{"CoolName"},
					[]string{"AwesomeName", "CoolName"},
				},
				Timestamp: time.Now(),
				Unit:      UnitCount,
			}
			s3 := &Sample{
				Value:    float64(2.7),
				MetricID: *metricID,
				RollupDimensions: [][]string{
					[]string{"CoolName"},
					[]string{"AwesomeName", "CoolName"},
				},
				Timestamp: time.Now(),
				Unit:      UnitCount,
			}

			dist.AddSample(s2)
			dist.AddSample(s3)

			Convey("Then the distribution's min, max, and sum should get updated", func() {
				So(dist.Maximum, ShouldEqual, float64(2.7))
				So(dist.Minimum, ShouldEqual, float64(1.0))
				So(dist.Sum, ShouldEqual, float64(4.9))
			})

			Convey("Then the distribution's sample count should increase to 3", func() {
				So(dist.SampleCount, ShouldEqual, 3)
			})
		})
	})
}
