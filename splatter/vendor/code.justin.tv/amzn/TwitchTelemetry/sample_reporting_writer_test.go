package telemetry

import (
	"bytes"
	"testing"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSampleReportingWriter(t *testing.T) {
	Convey("Given a SampleReportingWriter", t, func() {
		mockWriter := &bytes.Buffer{}
		mockObserver := &mockSampleObserver{}
		reporter := &SampleReporter{
			SampleObserver: mockObserver,
			SampleBuilder: SampleBuilder{
				ProcessIdentifier: identifier.ProcessIdentifier{
					Service: "TestService",
				},
			},
		}
		writer := &SampleReportingWriter{
			Writer:                 mockWriter,
			Reporter:               reporter,
			BytesWrittenMetricName: "BytesMetric",
			DurationMetricName:     "DurationMetric",
		}

		testValue := "TestValueSentToWriter"
		testValueLenBytes := len([]byte(testValue))

		Convey("When data is written", func() {
			n, err := writer.Write([]byte(testValue))

			Convey("Then it should delegate to the writer", func() {
				So(err, ShouldBeNil)
				So(n, ShouldEqual, testValueLenBytes)
				So(mockWriter.String(), ShouldEqual, testValue)
			})

			metrics := mockObserver.ToMap()

			Convey("Then it should report a byte metric", func() {
				value, ok := metrics["BytesMetric"]
				So(ok, ShouldBeTrue)
				So(value, ShouldEqual, testValueLenBytes)
			})

			Convey("Then it should report a duration metric", func() {
				_, ok := metrics["DurationMetric"]
				So(ok, ShouldBeTrue)
			})
		})

		Convey("Without a Writer", func() {
			writer.Writer = nil

			Convey("When data is written", func() {
				_, err := writer.Write([]byte(testValue))

				Convey("Then it should return an error", func() {
					So(err, ShouldNotBeNil)
				})

				Convey("Then it should not report metrics", func() {
					So(mockObserver.ToMap(), ShouldBeEmpty)
				})
			})
		})

		Convey("Without a Reporter", func() {
			writer.Reporter = nil

			Convey("When data is written", func() {
				n, err := writer.Write([]byte(testValue))

				Convey("Then it should delegate to the writer", func() {
					So(err, ShouldBeNil)
					So(n, ShouldEqual, testValueLenBytes)
					So(mockWriter.String(), ShouldEqual, testValue)
				})

				Convey("Then it should not report metrics", func() {
					So(mockObserver.ToMap(), ShouldBeEmpty)
				})
			})
		})
	})
}
