package telemetry

import (
	"fmt"
	"sync"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

type mockUnbufferedSender struct {
	mux          sync.Mutex
	distribution []*Distribution
	shouldSleep  bool
}

func (m *mockUnbufferedSender) FlushWithoutBuffering(distributions []*Distribution) {
	m.mux.Lock()
	defer m.mux.Unlock()
	m.distribution = append(m.distribution, distributions...)
	if m.shouldSleep {
		time.Sleep(4 * time.Second)
	}
}

func (m *mockUnbufferedSender) getDistribution() []*Distribution {
	m.mux.Lock()
	defer m.mux.Unlock()
	distCp := make([]*Distribution, len(m.distribution))
	copy(distCp, m.distribution)
	return distCp
}

// testLogger is a helper struct to verify the dropped metric behavior
type testLogger struct {
	mux       sync.Mutex
	messages  []string
	keyValMap map[string]string
}

func (tLog *testLogger) Log(msg string, keyvals ...interface{}) {
	tLog.mux.Lock()
	defer tLog.mux.Unlock()
	// Note the msg
	tLog.messages = append(tLog.messages, msg)

	// Add the keyvals to a map
	for i := 0; i < len(keyvals); i += 2 {
		var key, value interface{}
		if i == len(keyvals)-1 {
			key, value = "no_key", keyvals[i]
		} else {
			key, value = keyvals[i], keyvals[i+1]
		}
		tLog.keyValMap[fmt.Sprint(key)] = fmt.Sprint(value)
	}
}

func (tLog *testLogger) getMessages() []string {
	tLog.mux.Lock()
	defer tLog.mux.Unlock()
	msgCp := make([]string, len(tLog.messages))
	copy(msgCp, tLog.messages)
	return msgCp
}

func (tLog *testLogger) getKeyValMap() map[string]string {
	tLog.mux.Lock()
	defer tLog.mux.Unlock()
	keyValCp := make(map[string]string, len(tLog.keyValMap))
	for k, v := range tLog.keyValMap {
		keyValCp[k] = v
	}
	return keyValCp
}

func TestBufferedAggregatorSameId(t *testing.T) {
	Convey("Given a one-minute buffered aggregator with a 2 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(2*time.Second, 10000, time.Minute, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("With multiple samples with the same id", func() {
			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("Then the aggregator should combine them and automatically send to the sender", func() {
				// Sleep for 4 seconds
				time.Sleep(4 * time.Second)
				distributions := mockSender.getDistribution()
				So(len(distributions), ShouldEqual, 1)
				So(distributions[0].SampleCount, ShouldEqual, sampleCount)
			})
		})
	})
}

func TestBufferedAggregatorDifferentId(t *testing.T) {
	Convey("Given a one-minute buffered aggregator with a 2 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(2*time.Second, 10000, time.Minute, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("With multiple samples with different ids", func() {
			sampleCount := 5
			for i := 0; i < sampleCount; i++ {
				reporter.Report(fmt.Sprintf("TestMetricName%v", i), float64(1), "Count")
			}
			reporter.ProcessIdentifier.Stage = "TestStage"
			for i := 0; i < sampleCount; i++ {
				reporter.Report(fmt.Sprintf("TestMetricName%v", i), float64(1), "Count")
			}

			Convey("Then the aggregator should combine them and automatically send to the sender", func() {
				// Sleep for 4 seconds
				time.Sleep(4 * time.Second)
				distributions := mockSender.getDistribution()
				So(len(distributions), ShouldEqual, sampleCount*2)
				for i := 0; i < sampleCount*2; i++ {
					So(distributions[i].SampleCount, ShouldEqual, 1)
				}
			})
		})
	})
}

func TestBufferedAggregatorSameTimestampMinute(t *testing.T) {
	Convey("Given a one-minute buffered aggregator with a 2 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(2*time.Second, 10000, time.Minute, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("With multiple samples that have timestamps in the same minute", func() {
			reporter.Timestamp = reporter.Timestamp.Truncate(time.Minute)

			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Timestamp = reporter.Timestamp.Add(time.Second)
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("Then the aggregator should combine them", func() {
				// Sleep for 4 seconds
				time.Sleep(4 * time.Second)
				distributions := mockSender.getDistribution()
				So(len(distributions), ShouldEqual, 1)
				So(distributions[0].SampleCount, ShouldEqual, sampleCount)
			})
		})
	})
}

func TestBufferedAggregatorDifferentTimestampMinutes(t *testing.T) {
	Convey("Given a one-minute buffered aggregator with a 2 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(2*time.Second, 10000, time.Minute, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("With multiple samples that have timestamps in different minutes", func() {
			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Timestamp = reporter.Timestamp.Add(time.Minute)
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("Then the aggregator should not combine them", func() {
				// Sleep for 4 seconds
				time.Sleep(4 * time.Second)
				distributions := mockSender.getDistribution()
				So(len(distributions), ShouldEqual, sampleCount)
				for i := 0; i < sampleCount; i++ {
					So(distributions[i].SampleCount, ShouldEqual, 1)
				}
			})
		})
	})
}

func TestBufferedAggregatorSamplesInParallel(t *testing.T) {
	Convey("Given a one-minute buffered aggregator with a 4 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		// Make a *very* large buffer here for our test
		aggregator := NewBufferedAggregator(4*time.Second, 8000000, time.Minute, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("When samples are added in parallel", func() {
			addSample := func(parameters []interface{}) {
				reporter.Report("TestMetricName", float64(1), "Count")
			}
			runner := &loadRunner{Concurrency: 4, TestDuration: 2 * time.Second, Task: addSample}
			runner.Run()

			invocations := runner.GetInvocations()
			fmt.Printf("(%v samples, %.2f per second)", invocations, float64(invocations)/runner.TestDuration.Seconds())
			So(invocations, ShouldBeGreaterThan, 50)

			Convey("Then all samples should be accounted for", func() {
				// Sleep for 10 seconds
				time.Sleep(10 * time.Second)
				distributions := mockSender.getDistribution()
				// Samples can be spread across multiple flushes
				So(len(distributions), ShouldBeBetweenOrEqual, 1, 3)
				// Check the number of obtained samples
				var totalCount int32
				for i := 0; i < len(distributions); i++ {
					totalCount += distributions[i].SampleCount
				}
				So(totalCount, ShouldEqual, invocations)
			})
		})
	})
}

func TestBufferedAggregatorTimestampVerification(t *testing.T) {
	Convey("Given a ten-second buffered aggregator with a 2 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(2*time.Second, 10000, 10*time.Second, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		timestamps := map[string]string{
			"2018-01-01T00:00:00+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:01+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:30+00:00": "2018-01-01T00:00:30+00:00",
			"2018-01-01T00:00:59+00:00": "2018-01-01T00:00:50+00:00",
			"2018-01-01T00:01:00+00:00": "2018-01-01T00:01:00+00:00",
		}
		verifyBufferedAggregationTimestamps(mockSender, reporter, timestamps)
	})
	Convey("Given buffered aggregator with no aggregation with a 2 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(2*time.Second, 10000, 0, mockSender, nil)
		defer aggregator.Stop()

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		timestamps := map[string]string{
			"2018-01-01T00:00:00+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:01+00:00": "2018-01-01T00:00:01+00:00",
			"2018-01-01T00:00:30+00:00": "2018-01-01T00:00:30+00:00",
			"2018-01-01T00:00:59+00:00": "2018-01-01T00:00:59+00:00",
			"2018-01-01T00:01:00+00:00": "2018-01-01T00:01:00+00:00",
		}
		verifyBufferedAggregationTimestamps(mockSender, reporter, timestamps)
	})
}

func TestBufferedAggregatorClose(t *testing.T) {
	// Chose a long flush time compared to other tests so we can close before we flush
	Convey("Given a one-minute buffered aggregator with a 30 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(30*time.Second, 10000, time.Minute, mockSender, nil)

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("With multiple samples with the same id", func() {
			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("When the aggregator is closed before it flushes", func() {
				aggregator.Stop()

				Convey("Then the aggregator should combine them and automatically send to the sender", func() {
					distributions := mockSender.getDistribution()
					So(len(distributions), ShouldEqual, 1)
					So(distributions[0].SampleCount, ShouldEqual, sampleCount)

					Convey("And additional observed samples are discarded without panics", func() {
						// Add a new sample
						reporter.Report("TestMetricName", float64(1), "Count")
						// Verify the distribution is unchanged
						newDist := mockSender.getDistribution()
						So(len(newDist), ShouldEqual, 1)
						So(newDist[0].SampleCount, ShouldEqual, sampleCount)
					})
				})
			})
		})
	})
}

func TestBufferedAggregatorFlush(t *testing.T) {
	// Chose a long flush time compared to other tests so we can explicitly flush
	Convey("Given a one-minute buffered aggregator with a 30 second flush period, a mock unbuffered sender, and a reporter", t, func() {
		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		aggregator := NewBufferedAggregator(30*time.Second, 10000, time.Minute, mockSender, nil)

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("When the aggregator is flushed explicitly without any pending data", func() {
			aggregator.Flush()

			Convey("Then the aggregator should send an empty distribution set to the sender", func() {
				distributions := mockSender.getDistribution()
				So(len(distributions), ShouldEqual, 0)
			})
		})

		Convey("With multiple samples with the same id", func() {
			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("When the aggregator is flushed explicitly", func() {
				aggregator.Flush()

				Convey("Then the aggregator should combine them and automatically send to the sender", func() {
					distributions := mockSender.getDistribution()
					So(len(distributions), ShouldEqual, 1)
					So(distributions[0].SampleCount, ShouldEqual, sampleCount)
				})

				Convey("With more samples with the same id added later", func() {
					sampleCount := 5
					for i := 0; i < sampleCount; i++ {
						reporter.Report("TestMetricName", float64(1), "Count")
					}

					Convey("When the aggregator is flushed explicitly", func() {
						aggregator.Flush()

						Convey("Then the aggregator should combine them and automatically send to the sender", func() {
							distributions := mockSender.getDistribution()
							So(len(distributions), ShouldEqual, 2)
							So(distributions[1].SampleCount, ShouldEqual, sampleCount)
						})
					})
				})
			})
		})
	})
}

func TestBufferedAggregatorDroppedMetricBehaviorDuringBurst(t *testing.T) {
	Convey("Given a buffered aggregator with a very small buffer (1)", t, func() {

		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0)}
		mockLogger := &testLogger{
			messages:  []string{},
			keyValMap: map[string]string{},
		}
		aggregator := NewBufferedAggregator(10*time.Second, 1000, 10*time.Second, mockSender, mockLogger)

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("When samples are added in parallel", func() {
			addSample := func(parameters []interface{}) {
				reporter.Report("TestMetricName", float64(1), "Count")
			}
			runner := &loadRunner{Concurrency: 4, TestDuration: 2 * time.Second, Task: addSample}
			runner.Run()

			invocations := runner.GetInvocations()
			fmt.Printf("(%v samples, %.2f per second)", invocations, float64(invocations)/runner.TestDuration.Seconds())
			So(invocations, ShouldBeGreaterThan, 50)
			aggregator.Stop()

			Convey("Then the correct info should be logged", func() {
				// There should be a single logged message and it should be our expected dropped metric error
				messages := mockLogger.getMessages()
				So(len(messages), ShouldEqual, 1)
				So(messages[0], ShouldEqual, droppedMetricErrorMessage)
				keyValMap := mockLogger.getKeyValMap()
				So(len(keyValMap), ShouldEqual, 2)
				So(keyValMap[droppedMetricKey], ShouldNotBeEmpty)
				So(keyValMap[timestampKey], ShouldNotBeEmpty)
			})
		})
	})
}

func TestBufferedAggregatorDoesntBlockOnSender(t *testing.T) {
	Convey("Given a buffered aggregator with a sender that sleeps too long", t, func() {

		mockSender := &mockUnbufferedSender{distribution: make([]*Distribution, 0), shouldSleep: true}
		mockLogger := &testLogger{
			messages:  []string{},
			keyValMap: map[string]string{},
		}
		aggregator := NewBufferedAggregator(time.Second, 1, time.Second, mockSender, mockLogger)

		reporter := &SampleReporter{}
		reporter.SampleObserver = aggregator
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("When samples are observed over several flushes", func() {
			reporter.Report("TestMetricName1", float64(1), "Count")
			time.Sleep(time.Second)
			reporter.Report("TestMetricName2", float64(1), "Count")
			time.Sleep(time.Second)
			reporter.Report("TestMetricName3", float64(1), "Count")
			aggregator.Stop()

			Convey("Then the samples should be passed to the sender", func() {
				distributions := mockSender.getDistribution()

				// Samples can be spread across multiple flushes
				So(len(distributions), ShouldBeBetweenOrEqual, 1, 3)
				// Check the number of obtained samples
				var totalCount int32
				for i := 0; i < len(distributions); i++ {
					totalCount += distributions[i].SampleCount
				}
				So(totalCount, ShouldEqual, 3)
			})
		})
	})
}

func verifyBufferedAggregationTimestamps(mockUnbufferedSender *mockUnbufferedSender, reporter *SampleReporter, timestamps map[string]string) {
	var err error
	for timestamp, expected := range timestamps {
		Convey(fmt.Sprintf("With a timestamp of %v", timestamp), func() {
			reporter.Timestamp, err = time.Parse(time.RFC3339, timestamp)
			So(err, ShouldBeNil)

			Convey("When aggregated", func() {
				reporter.Report("TestMetricName", float64(1), "Count")

				// Sleep for 4 seconds
				time.Sleep(4 * time.Second)
				distributions := mockUnbufferedSender.getDistribution()

				Convey(fmt.Sprintf("Then the timestamp should be %v", expected), func() {
					truncatedTimestamp, err := time.Parse(time.RFC3339, expected)
					So(err, ShouldBeNil)
					So(len(distributions), ShouldEqual, 1)
					So(distributions[0].Timestamp.Format(time.RFC3339), ShouldEqual, truncatedTimestamp.Format(time.RFC3339))
				})
			})
		})
	}
}
