package telemetry

import (
	"math"
	"math/rand"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSEH1(t *testing.T) {
	Convey("Given an SEH1 histogram", t, func() {
		seh1 := NewSEH1()
		Convey("Then the histogram should be empty", func() {
			So(len(seh1.Histogram), ShouldEqual, 0)
			So(seh1.ZeroBucket, ShouldEqual, 0)
		})
		Convey("When a zero valued sample is added", func() {
			s := &Sample{
				Value: float64(0),
			}
			seh1.Include(s)
			Convey("Then the zero bucket should increment", func() {
				So(seh1.ZeroBucket, ShouldEqual, 1)
			})
		})
		Convey("When a negative valued sample is added", func() {
			s := &Sample{
				Value: float64(-5),
			}
			seh1.Include(s)
			Convey("Then the zero bucket should increment", func() {
				So(seh1.ZeroBucket, ShouldEqual, 1)
			})
		})
		Convey("When a positive valued sample is added", func() {
			s := &Sample{
				Value: float64(1.2345),
			}
			seh1.Include(s)
			Convey("Then the appropriate bucket should be inremented", func() {
				So(seh1.Histogram[computeBucket(s.Value)], ShouldEqual, 1)
				Convey("And when the sample is added 3 more times", func() {
					seh1.Include(s)
					seh1.Include(s)
					seh1.Include(s)
					Convey("Then the bucket should increment 3 more times", func() {
						So(seh1.Histogram[computeBucket(s.Value)], ShouldEqual, 4)
					})
				})
			})
		})
		Convey("Then it should be able to accurately convert bin number to value and back", func() {
			// -7000 to 7000 is from https://code.amazon.com/packages/MetricDataIO/blobs/c780d0a131cc850812585f704fb94ccd1e2fff46/--/src/com/amazon/metricdata/io/distribution/Seh1Helper.java#L15
			var binNumber int32
			for binNumber = -7000; binNumber < 7000; binNumber++ {
				value := seh1.ApproximateOriginalValue(binNumber)
				actualBinNumber := computeBucket(value)
				So(actualBinNumber, ShouldEqual, binNumber)
			}
		})
	})

	Convey("Given 2 SEH1 histograms", t, func() {
		seh1A := NewSEH1()
		seh1B := NewSEH1()
		Convey("When several samples are added to both histograms", func() {
			s1 := &Sample{
				Value: float64(1.2345),
			}
			s2 := &Sample{
				Value: float64(1.2345),
			}
			s3 := &Sample{
				Value: float64(2.3456),
			}
			s4 := &Sample{
				Value: float64(3.4567),
			}
			s5 := &Sample{
				Value: float64(3.4567),
			}
			seh1A.Include(s1)
			seh1A.Include(s2)
			seh1A.Include(s4)

			seh1B.Include(s3)
			seh1B.Include(s5)
			Convey("Then their individual histogram counts should be correct", func() {
				So(seh1A.Histogram[computeBucket(s1.Value)], ShouldEqual, 2)
				So(seh1A.Histogram[computeBucket(s2.Value)], ShouldEqual, 2) //dupe
				So(seh1A.Histogram[computeBucket(s3.Value)], ShouldEqual, 0)
				So(seh1A.Histogram[computeBucket(s4.Value)], ShouldEqual, 1)
				So(seh1A.Histogram[computeBucket(s5.Value)], ShouldEqual, 1) //dupe

				So(seh1B.Histogram[computeBucket(s1.Value)], ShouldEqual, 0)
				So(seh1B.Histogram[computeBucket(s2.Value)], ShouldEqual, 0) //dupe
				So(seh1B.Histogram[computeBucket(s3.Value)], ShouldEqual, 1)
				So(seh1B.Histogram[computeBucket(s4.Value)], ShouldEqual, 1)
				So(seh1B.Histogram[computeBucket(s5.Value)], ShouldEqual, 1) //dupe

				Convey("And when the union of the histograms is taken", func() {
					seh1A.Union(seh1B)
					Convey("Then one histogram should have the data from both", func() {
						So(seh1A.Histogram[computeBucket(s1.Value)], ShouldEqual, 2)
						So(seh1A.Histogram[computeBucket(s2.Value)], ShouldEqual, 2) //dupe
						So(seh1A.Histogram[computeBucket(s3.Value)], ShouldEqual, 1)
						So(seh1A.Histogram[computeBucket(s4.Value)], ShouldEqual, 2)
						So(seh1A.Histogram[computeBucket(s5.Value)], ShouldEqual, 2) //dupe
					})
					Convey("Then the histogram passed as an argument should not be modified", func() {
						So(seh1B.Histogram[computeBucket(s1.Value)], ShouldEqual, 0)
						So(seh1B.Histogram[computeBucket(s2.Value)], ShouldEqual, 0) //dupe
						So(seh1B.Histogram[computeBucket(s3.Value)], ShouldEqual, 1)
						So(seh1B.Histogram[computeBucket(s4.Value)], ShouldEqual, 1)
						So(seh1B.Histogram[computeBucket(s5.Value)], ShouldEqual, 1) //dupe
					})
				})
			})
		})
	})

	Convey("Given an SEH1 histogram", t, func() {
		seh1 := NewSEH1()
		Convey("And 100 random sample values", func() {
			values := make([]float64, 100)
			for i := range values {
				values[i] = rand.Float64()
				// add a small increment just so we don't get any zeroes...
				values[i] += float64(0.001)
			}
			Convey("When the bucket is computed for each sample", func() {
				buckets := make([]int32, 100)
				for i := range buckets {
					buckets[i] = computeBucket(values[i])
				}

				// the following tests ensure that for any sample value V, it is always the case that
				// approximateOriginalValue(bucket(V)) ~= V < approximateOriginalValue(bucket(V)+1)

				Convey("Then the reverse bucket computation should always be less than a bucket width from the original value", func() {
					for i := range buckets {
						bucketWidth := math.Pow(math.E, (float64(buckets[i]+1))*float64(math.Log(1+0.1))) -
							math.Pow(math.E, (float64(buckets[i]))*float64(math.Log(1+0.1)))
						approxValue := seh1.ApproximateOriginalValue(buckets[i])
						So(values[i], ShouldAlmostEqual, approxValue, bucketWidth)
					}
				})

				Convey("Then the reverse bucket computation for the next bucket should always be greater than the original value", func() {
					for i := range buckets {
						largerApproxValue := seh1.ApproximateOriginalValue(buckets[i] + 1)
						So(values[i], ShouldBeLessThan, largerApproxValue)
					}
				})
			})
		})
	})
}
