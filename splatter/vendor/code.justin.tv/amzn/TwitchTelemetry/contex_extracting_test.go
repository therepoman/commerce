package telemetry

import (
	. "github.com/smartystreets/goconvey/convey"

	"context"
	"testing"
	"time"
)

const (
	testOperation        = "testOperation"
	testAnotherOperation = "testAnotherOperation"
)

func TestContextExtracting(t *testing.T) {
	Convey("Given a context", t, func() {
		ctx := context.Background()
		Convey("And a SampleReporter", func() {
			reporter := SampleReporter{}
			Convey("When no operation has been set in the context", func() {
				Convey("Ensure nothing breaks", func() {
					var newReporter SampleReporter
					shouldWork := func() { newReporter = SampleReporterWithContext(reporter, ctx) }
					So(shouldWork, ShouldNotPanic)
					So(newReporter, ShouldNotBeNil)
					So(newReporter.OperationName, ShouldEqual, MetricValueUnknownOperation)
				})
			})
			Convey("When an operation has been set in the context", func() {
				updatedCtx := ContextWithOperationName(ctx, testOperation)
				Convey("When using SampleReporterWithContext", func() {
					newReporter := SampleReporterWithContext(reporter, updatedCtx)
					Convey("Ensure the new SampleReporter has the expected value", func() {
						So(newReporter, ShouldNotBeNil)
						So(newReporter.OperationName, ShouldEqual, testOperation)
					})
				})
				Convey("When a new operation has been set in the context", func() {
					updatedCtxAgain := ContextWithOperationName(updatedCtx, testAnotherOperation)
					Convey("When using SampleReporterWithContext", func() {
						newReporter := SampleReporterWithContext(reporter, updatedCtxAgain)
						Convey("Ensure the new SampleReporter has the expected value", func() {
							So(newReporter, ShouldNotBeNil)
							So(newReporter.OperationName, ShouldEqual, testAnotherOperation)
						})
					})
				})
			})
			Convey("When a timestamp has been set in the context", func() {
				now := time.Now()
				later := now.Add(time.Second)
				updatedCtx := ContextWithTimestamp(ctx, now)
				Convey("When using SampleReporterWithContext", func() {
					newReporter := SampleReporterWithContext(reporter, updatedCtx)
					Convey("Ensure the new SampleReporter has the expected value", func() {
						So(newReporter, ShouldNotBeNil)
						So(newReporter.Timestamp.Equal(now), ShouldBeTrue)
					})
				})
				Convey("When a new timestamp has been set in the context", func() {
					updatedCtxAgain := ContextWithTimestamp(updatedCtx, later)
					Convey("When using SampleReporterWithContext", func() {
						newReporter := SampleReporterWithContext(reporter, updatedCtxAgain)
						Convey("Ensure the new SampleReporter has the expected value", func() {
							So(newReporter, ShouldNotBeNil)
							So(newReporter.Timestamp.Equal(later), ShouldBeTrue)
						})
					})
				})
			})
		})
	})
}
