package telemetry

import (
	"strconv"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
)

func TestSampleBuilder(t *testing.T) {
	processIdentifier := identifier.ProcessIdentifier{
		Service:  "TestService",
		Stage:    "TestStage",
		Substage: "TestSubstage",
		Region:   "TestRegion",
		Machine:  "TestMachine",
		LaunchID: "TestLaunchID",
		Version:  "Version",
	}

	dependencyProcessIdentifier := identifier.ProcessIdentifier{
		Service:  "DependencyService",
		Stage:    "DependencyStage",
		Substage: "DependencySubstage",
		Region:   "DependencyRegion",
	}

	Convey("Given a sample builder", t, func() {
		builder := SampleBuilder{}

		Convey("Without the required service name", func() {
			So(builder.ProcessIdentifier.Service, ShouldBeEmpty)

			Convey("When a sample is built", func() {
				So(builder.ProcessIdentifier.Service, ShouldBeEmpty)
				_, err := builder.Build("TestMetric", 123, "Count")

				Convey("Then it should fail", func() {
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("With a full process identifier", func() {
			builder.ProcessIdentifier = processIdentifier

			Convey("When a sample is built", func() {
				sample, err := builder.Build("TestMetric", 123, "Count")
				So(err, ShouldBeNil)

				Convey("Then it should have default service dimensions and rollups", func() {
					So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{
						"Service":  "TestService",
						"Stage":    "TestStage",
						"Substage": "TestSubstage",
						"Region":   "TestRegion",
					})
					So(sample.RollupDimensions, ShouldResemble, [][]string{
						[]string{"Substage"},
					})
				})
			})

			Convey("With cross-region rollup enabled", func() {
				builder.EnableCrossRegionRollup = true

				Convey("When a sample is built", func() {
					sample, err := builder.Build("TestMetric", 123, "Count")
					So(err, ShouldBeNil)

					Convey("Then it should have the region rollup", func() {
						So(sample.RollupDimensions, ShouldResemble, [][]string{
							[]string{"Substage"},
							[]string{"Substage", "Region"},
						})
					})
				})
			})

			Convey("And an operation name", func() {
				builder.OperationName = "TestOp"

				Convey("When a sample is built", func() {
					sample, err := builder.Build("TestMetric", 123, "Count")
					So(err, ShouldBeNil)

					Convey("Then it should have default service dimensions and rollups", func() {
						So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{
							"Service":   "TestService",
							"Stage":     "TestStage",
							"Substage":  "TestSubstage",
							"Region":    "TestRegion",
							"Operation": "TestOp",
						})
						So(sample.RollupDimensions, ShouldResemble, [][]string{
							[]string{"Operation"},
							[]string{"Operation", "Substage"},
						})
					})

					Convey("And process address enabled", func() {
						builder.EnableProcessAddressDimension = true

						Convey("When a sample is built", func() {
							sample, err := builder.Build("TestMetric", 123, "Count")
							So(err, ShouldBeNil)

							Convey("Then it should have the process address dimension and rollups", func() {
								So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{
									"Service":        "TestService",
									"Stage":          "TestStage",
									"Substage":       "TestSubstage",
									"Region":         "TestRegion",
									"Operation":      "TestOp",
									"ProcessAddress": "TestMachine",
								})
								So(sample.RollupDimensions, ShouldResemble, [][]string{
									[]string{"ProcessAddress"},
									[]string{"ProcessAddress", "Operation"},
									[]string{"ProcessAddress", "Operation", "Substage"},
								})
							})
						})
					})
				})

				Convey("And dimension overrides", func() {
					builder.Dimensions = DimensionSet{
						"Service":       "OverrideService",
						"Substage":      "",
						"TestDimension": "TestValue",
					}

					Convey("When a sample is built", func() {
						sample, err := builder.Build("TestMetric", 123, "Count")
						So(err, ShouldBeNil)

						Convey("Then it should have dimension overrides and different rollups", func() {
							So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{
								"Service":       "OverrideService",
								"Stage":         "TestStage",
								"Region":        "TestRegion",
								"Operation":     "TestOp",
								"TestDimension": "TestValue",
							})
							So(sample.RollupDimensions, ShouldResemble, [][]string{
								[]string{"TestDimension"},
								[]string{"TestDimension", "Operation"},
							})
						})
					})

					Convey("With cross-region rollup enabled", func() {
						builder.EnableCrossRegionRollup = true

						Convey("When a sample is built", func() {
							sample, err := builder.Build("TestMetric", 123, "Count")
							So(err, ShouldBeNil)

							Convey("Then it should have the region rollup", func() {
								So(sample.RollupDimensions, ShouldResemble, [][]string{
									[]string{"TestDimension"},
									[]string{"TestDimension", "Operation"},
									[]string{"TestDimension", "Operation", "Region"},
								})
							})
						})
					})
				})
			})
		})

		Convey("With the minimum fields to build a sample", func() {
			builder.ProcessIdentifier.Service = processIdentifier.Service

			Convey("When a sample is built", func() {
				sample, err := builder.Build("TestMetric", 123, "Count")
				So(err, ShouldBeNil)

				Convey("Then it should have no default dimensions and rollups", func() {
					So(sample.MetricID, ShouldResemble, MetricID{
						Name:       "TestMetric",
						Dimensions: DimensionSet{"Service": "TestService"},
					})
					So(sample.RollupDimensions, ShouldBeEmpty)
					So(time.Now().Sub(sample.Timestamp).Seconds(), ShouldBeBetween, -10, 10)
					So(sample.Value, ShouldEqual, 123)
					So(sample.Unit, ShouldEqual, "Count")
				})
			})

			Convey("When a duration sample is built", func() {
				sample, err := builder.BuildDurationSample("TestMetric", time.Duration(time.Minute))
				So(err, ShouldBeNil)

				Convey("Then it should have no default dimensions and rollups", func() {
					So(sample.MetricID, ShouldResemble, MetricID{
						Name:       "TestMetric",
						Dimensions: DimensionSet{"Service": "TestService"},
					})
					So(sample.RollupDimensions, ShouldBeEmpty)
					So(time.Now().Sub(sample.Timestamp).Seconds(), ShouldBeBetween, -10, 10)
					So(sample.Value, ShouldEqual, 60)
					So(sample.Unit, ShouldEqual, "Seconds")
				})
			})

			Convey("When success availability samples are built", func() {
				samples, err := builder.BuildAvailabilitySamples(AvailabilityCodeSucccess)
				So(err, ShouldBeNil)

				Convey("Then it should have no default dimensions and rollups", func() {
					for _, sample := range samples {
						So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{"Service": "TestService"})
						So(sample.RollupDimensions, ShouldBeEmpty)
						So(time.Now().Sub(sample.Timestamp).Seconds(), ShouldBeBetween, -10, 10)
					}
				})

				Convey("Then it should build 3 samples with a count of 1 for success", func() {
					So(len(samples), ShouldEqual, 3)

					So(samples[0].MetricID.Name, ShouldEqual, MetricSuccess)
					So(samples[0].Value, ShouldEqual, 1)
					So(samples[0].Unit, ShouldEqual, "Count")

					So(samples[1].MetricID.Name, ShouldEqual, MetricClientError)
					So(samples[1].Value, ShouldEqual, 0)
					So(samples[1].Unit, ShouldEqual, "Count")

					So(samples[2].MetricID.Name, ShouldEqual, MetricServerError)
					So(samples[2].Value, ShouldEqual, 0)
					So(samples[2].Unit, ShouldEqual, "Count")
				})
			})

			Convey("When client error availability samples are built", func() {
				samples, err := builder.BuildAvailabilitySamples(AvailabilityCodeClientError)
				So(err, ShouldBeNil)

				Convey("Then it should build 3 samples with a count of 1 for client error", func() {
					So(len(samples), ShouldEqual, 3)

					So(samples[0].MetricID.Name, ShouldEqual, MetricSuccess)
					So(samples[0].Value, ShouldEqual, 0)

					So(samples[1].MetricID.Name, ShouldEqual, MetricClientError)
					So(samples[1].Value, ShouldEqual, 1)

					So(samples[2].MetricID.Name, ShouldEqual, MetricServerError)
					So(samples[2].Value, ShouldEqual, 0)
				})
			})

			Convey("When server error availability samples are built", func() {
				samples, err := builder.BuildAvailabilitySamples(AvailabilityCodeServerError)
				So(err, ShouldBeNil)

				Convey("Then it should build 3 samples with a count of 1 for server error", func() {
					So(len(samples), ShouldEqual, 3)

					So(samples[0].MetricID.Name, ShouldEqual, MetricSuccess)
					So(samples[0].Value, ShouldEqual, 0)

					So(samples[1].MetricID.Name, ShouldEqual, MetricClientError)
					So(samples[1].Value, ShouldEqual, 0)

					So(samples[2].MetricID.Name, ShouldEqual, MetricServerError)
					So(samples[2].Value, ShouldEqual, 1)
				})
			})

			Convey("With process address enabled", func() {
				builder.EnableProcessAddressDimension = true

				Convey("When a sample is built", func() {
					sample, err := builder.Build("TestMetric", 123, "Count")
					So(err, ShouldBeNil)

					Convey("Then it should have no ProcessAddress Dimension or rollups", func() {
						So(sample.MetricID, ShouldResemble, MetricID{
							Name:       "TestMetric",
							Dimensions: DimensionSet{"Service": "TestService"},
						})
						So(sample.RollupDimensions, ShouldBeEmpty)
					})
				})
			})

			Convey("With a dependency identifier", func() {
				builder.DependencyProcessIdentifier = dependencyProcessIdentifier

				Convey("When a sample is built", func() {
					sample, err := builder.Build("TestMetric", 123, "Count")
					So(err, ShouldBeNil)

					Convey("Then it should have default dependency dimension and no rollup", func() {
						So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{
							"Service":    "TestService",
							"Dependency": "DependencyService:Unknown",
						})
						So(sample.RollupDimensions, ShouldBeEmpty)
					})
				})

				Convey("And a dependency operation name", func() {
					builder.DependencyOperationName = "TestDependencyOp"

					Convey("When a sample is built", func() {
						sample, err := builder.Build("TestMetric", 123, "Count")
						So(err, ShouldBeNil)

						Convey("Then it should have default dependency dimension and no rollup", func() {
							So(sample.MetricID.Dimensions, ShouldResemble, DimensionSet{
								"Service":    "TestService",
								"Dependency": "DependencyService:TestDependencyOp",
							})
							So(sample.RollupDimensions, ShouldBeEmpty)
						})
					})

					Convey("When success availability samples are built", func() {
						samples, err := builder.BuildAvailabilitySamples(AvailabilityCodeSucccess)
						So(err, ShouldBeNil)

						Convey("Then it should build 3 samples with a count of 1 for success", func() {
							So(len(samples), ShouldEqual, 3)

							So(samples[0].MetricID.Name, ShouldEqual, MetricDependencySuccess)
							So(samples[0].Value, ShouldEqual, 1)
							So(samples[0].Unit, ShouldEqual, "Count")

							So(samples[1].MetricID.Name, ShouldEqual, MetricDependencyClientError)
							So(samples[1].Value, ShouldEqual, 0)
							So(samples[1].Unit, ShouldEqual, "Count")

							So(samples[2].MetricID.Name, ShouldEqual, MetricDependencyServerError)
							So(samples[2].Value, ShouldEqual, 0)
							So(samples[2].Unit, ShouldEqual, "Count")
						})
					})

					Convey("When client error availability samples are built", func() {
						samples, err := builder.BuildAvailabilitySamples(AvailabilityCodeClientError)
						So(err, ShouldBeNil)

						Convey("Then it should build 3 samples with a count of 1 for client error", func() {
							So(len(samples), ShouldEqual, 3)

							So(samples[0].MetricID.Name, ShouldEqual, MetricDependencySuccess)
							So(samples[0].Value, ShouldEqual, 0)

							So(samples[1].MetricID.Name, ShouldEqual, MetricDependencyClientError)
							So(samples[1].Value, ShouldEqual, 1)

							So(samples[2].MetricID.Name, ShouldEqual, MetricDependencyServerError)
							So(samples[2].Value, ShouldEqual, 0)
						})
					})

					Convey("When server error availability samples are built", func() {
						samples, err := builder.BuildAvailabilitySamples(AvailabilityCodeServerError)
						So(err, ShouldBeNil)

						Convey("Then it should build 3 samples with a count of 1 for server error", func() {
							So(len(samples), ShouldEqual, 3)

							So(samples[0].MetricID.Name, ShouldEqual, MetricDependencySuccess)
							So(samples[0].Value, ShouldEqual, 0)

							So(samples[1].MetricID.Name, ShouldEqual, MetricDependencyClientError)
							So(samples[1].Value, ShouldEqual, 0)

							So(samples[2].MetricID.Name, ShouldEqual, MetricDependencyServerError)
							So(samples[2].Value, ShouldEqual, 1)
						})
					})
				})
			})

			Convey("With a timestamp", func() {
				builder.Timestamp = time.Now().Add(time.Duration(-time.Minute))

				Convey("When a sample is built", func() {
					sample, err := builder.Build("TestMetric", 123, "Count")
					So(err, ShouldBeNil)

					Convey("Then it should have the timestamp", func() {
						So(sample.Timestamp, ShouldResemble, builder.Timestamp)
					})
				})
			})

			Convey("With too many dimensions", func() {
				builder.Dimensions = DimensionSet{}
				for i := 0; i < 10; i++ {
					builder.Dimensions[strconv.Itoa(i)] = "test"
				}

				Convey("When a sample is built", func() {
					_, err := builder.Build("TestMetric", 123, "Count")

					Convey("Then it should fail", func() {
						So(err, ShouldNotBeNil)
					})
				})
			})
		})
	})
}
