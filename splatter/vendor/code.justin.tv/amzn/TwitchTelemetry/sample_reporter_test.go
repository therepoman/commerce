package telemetry

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSampleReporter(t *testing.T) {
	Convey("Given a sample reporter", t, func() {
		reporter := SampleReporter{}
		mockObserver := &mockSampleObserver{}
		reporter.SampleObserver = mockObserver

		Convey("With the minimum fields to build a sample", func() {
			reporter.ProcessIdentifier.Service = "TestService"

			Convey("When Report is called", func() {
				reporter.Report("TestMetric1", 123, "Count")

				Convey("Then a sample should be sent to the observer", func() {
					So(len(mockObserver.Samples), ShouldEqual, 1)
					So(mockObserver.Samples[0].MetricID.Name, ShouldEqual, "TestMetric1")
					So(mockObserver.Samples[0].Value, ShouldEqual, 123)
					So(mockObserver.Samples[0].Unit, ShouldEqual, "Count")
				})
			})

			Convey("When ReportDurationSample is called", func() {
				reporter.ReportDurationSample("TestMetric2", time.Duration(time.Minute))

				Convey("Then a sample should be sent to the observer", func() {
					So(len(mockObserver.Samples), ShouldEqual, 1)
					So(mockObserver.Samples[0].MetricID.Name, ShouldEqual, "TestMetric2")
					So(mockObserver.Samples[0].Value, ShouldEqual, 60)
					So(mockObserver.Samples[0].Unit, ShouldEqual, "Seconds")
				})
			})

			Convey("When ReportAvailabilitySamples is called", func() {
				reporter.ReportAvailabilitySamples(AvailabilityCodeSucccess)

				Convey("Then three samples should be sent to the observer", func() {
					So(len(mockObserver.Samples), ShouldEqual, 3)
					So(mockObserver.ToMap(), ShouldResemble, map[string]float64{
						"Success":     1,
						"ClientError": 0,
						"ServerError": 0,
					})
				})
			})
		})

		Convey("Without the required service name", func() {
			So(reporter.ProcessIdentifier.Service, ShouldBeEmpty)

			Convey("When Report is called", func() {
				reporter.Report("TestMetric1", 123, "Count")

				Convey("Then it should fail and not report any samples", func() {
					So(mockObserver.Samples, ShouldBeEmpty)
				})
			})

			Convey("When ReportDurationSample is called", func() {
				reporter.ReportDurationSample("TestMetric2", time.Duration(time.Minute))

				Convey("Then it should fail and not report any samples", func() {
					So(mockObserver.Samples, ShouldBeEmpty)
				})
			})

			Convey("When ReportAvailabilitySamples is called", func() {
				reporter.ReportAvailabilitySamples(AvailabilityCodeSucccess)

				Convey("Then it should fail and not report any samples", func() {
					So(mockObserver.Samples, ShouldBeEmpty)
				})
			})
		})
	})
}

type mockSampleObserver struct {
	Samples []*Sample
}

func (observer *mockSampleObserver) ObserveSample(sample *Sample) {
	observer.Samples = append(observer.Samples, sample)
}

func (observer *mockSampleObserver) Flush() {
}

func (observer *mockSampleObserver) Stop() {
}

func (observer *mockSampleObserver) BufferedFlush(distributions []*Distribution) {
}

func (observer *mockSampleObserver) ToMap() map[string]float64 {
	metrics := make(map[string]float64)
	for _, sample := range observer.Samples {
		metrics[sample.MetricID.Name] = sample.Value
	}
	return metrics
}
