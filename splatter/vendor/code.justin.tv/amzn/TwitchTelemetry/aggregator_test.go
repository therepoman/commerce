package telemetry

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"code.justin.tv/amzn/TwitchProcessIdentifier"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAggregator(t *testing.T) {
	Convey("Given a one-minute aggregator and reporter", t, func() {
		aggregator := NewAggregator(time.Minute)
		reporter := &SampleReporter{}
		reporter.SampleObserver = &aggregatingObserver{aggregator: aggregator}
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		Convey("With multiple samples with the same id", func() {
			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("Then the aggregator should combine them", func() {
				distributions := aggregator.getDistributions()
				So(len(distributions), ShouldEqual, 1)
				So(distributions[0].SampleCount, ShouldEqual, sampleCount)
			})

			Convey("And it is flushed", func() {
				distributions := aggregator.Flush()

				Convey("Then all distributions are removed", func() {
					So(len(distributions), ShouldEqual, 1)
					So(aggregator.getDistributions(), ShouldBeEmpty)
				})
			})

			Convey("And it is reset", func() {
				aggregator.Reset()

				Convey("Then all distributions are removed", func() {
					So(aggregator.getDistributions(), ShouldBeEmpty)
				})
			})
		})

		Convey("With multiple samples with different ids", func() {
			sampleCount := 5
			for i := 0; i < sampleCount; i++ {
				reporter.Report(fmt.Sprintf("TestMetricName%v", i), float64(1), "Count")
			}
			reporter.ProcessIdentifier.Stage = "TestStage"
			for i := 0; i < sampleCount; i++ {
				reporter.Report(fmt.Sprintf("TestMetricName%v", i), float64(1), "Count")
			}

			Convey("Then the aggregator should combine them", func() {
				distributions := aggregator.getDistributions()
				So(len(distributions), ShouldEqual, sampleCount*2)
			})
		})

		Convey("With multiple samples that have timestamps in the same minute", func() {
			reporter.Timestamp = reporter.Timestamp.Truncate(time.Minute)

			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Timestamp = reporter.Timestamp.Add(time.Second)
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("Then the aggregator should combine them", func() {
				distributions := aggregator.getDistributions()
				So(len(distributions), ShouldEqual, 1)
				So(distributions[0].SampleCount, ShouldEqual, sampleCount)
			})
		})

		Convey("With multiple samples that have timestamps in different minutes", func() {
			sampleCount := 10
			for i := 0; i < sampleCount; i++ {
				reporter.Timestamp = reporter.Timestamp.Add(time.Minute)
				reporter.Report("TestMetricName", float64(1), "Count")
			}

			Convey("Then the aggregator should not combine them", func() {
				distributions := aggregator.getDistributions()
				So(len(distributions), ShouldEqual, sampleCount)
			})
		})

		Convey("When samples are added in parallel", func() {
			addSample := func(parameters []interface{}) {
				reporter.Report("TestMetricName", float64(1), "Count")
			}
			runner := &loadRunner{Concurrency: 4, TestDuration: 2 * time.Second, Task: addSample}
			runner.Run()

			invocations := runner.GetInvocations()
			fmt.Printf("(%v samples, %.2f per second)", invocations, float64(invocations)/runner.TestDuration.Seconds())
			So(invocations, ShouldBeGreaterThan, 50)

			Convey("Then all samples should be accounted for", func() {
				distributions := aggregator.getDistributions()
				So(len(distributions), ShouldEqual, 1)
				So(distributions[0].SampleCount, ShouldEqual, invocations)
			})
		})

		timestamps := map[string]string{
			"2018-01-01T00:00:00+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:01+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:30+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:59+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:01:00+00:00": "2018-01-01T00:01:00+00:00",
		}
		verifyAggregationTimestamps(aggregator, reporter, timestamps)
	})
	Convey("Given a ten-second aggregator and reporter", t, func() {
		aggregator := NewAggregator(10 * time.Second)
		reporter := &SampleReporter{}
		reporter.SampleObserver = &aggregatingObserver{aggregator: aggregator}
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		timestamps := map[string]string{
			"2018-01-01T00:00:00+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:01+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:30+00:00": "2018-01-01T00:00:30+00:00",
			"2018-01-01T00:00:59+00:00": "2018-01-01T00:00:50+00:00",
			"2018-01-01T00:01:00+00:00": "2018-01-01T00:01:00+00:00",
		}
		verifyAggregationTimestamps(aggregator, reporter, timestamps)
	})
	Convey("Given an aggregator with no aggregationPeriod and a reporter", t, func() {
		aggregator := NewAggregator(0)
		reporter := &SampleReporter{}
		reporter.SampleObserver = &aggregatingObserver{aggregator: aggregator}
		reporter.ProcessIdentifier.Service = "TestService"
		reporter.Timestamp = time.Now()

		timestamps := map[string]string{
			"2018-01-01T00:00:00+00:00": "2018-01-01T00:00:00+00:00",
			"2018-01-01T00:00:01+00:00": "2018-01-01T00:00:01+00:00",
			"2018-01-01T00:00:30+00:00": "2018-01-01T00:00:30+00:00",
			"2018-01-01T00:00:59+00:00": "2018-01-01T00:00:59+00:00",
			"2018-01-01T00:01:00+00:00": "2018-01-01T00:01:00+00:00",
		}
		verifyAggregationTimestamps(aggregator, reporter, timestamps)
	})
}

func verifyAggregationTimestamps(aggregator *Aggregator, reporter *SampleReporter, timestamps map[string]string) {
	var err error
	for timestamp, expected := range timestamps {
		Convey(fmt.Sprintf("With a timestamp of %v", timestamp), func() {
			reporter.Timestamp, err = time.Parse(time.RFC3339, timestamp)
			So(err, ShouldBeNil)

			Convey("When aggregated", func() {
				reporter.Report("TestMetricName", float64(1), "Count")
				distributions := aggregator.getDistributions()

				Convey(fmt.Sprintf("Then the timestamp should be %v", expected), func() {
					truncatedTimestamp, err := time.Parse(time.RFC3339, expected)
					So(err, ShouldBeNil)
					So(len(distributions), ShouldEqual, 1)
					So(distributions[0].Timestamp.Format(time.RFC3339), ShouldEqual, truncatedTimestamp.Format(time.RFC3339))
				})
			})
		})
	}
}

func BenchmarkAggregating100Samples(b *testing.B) {
	b.StopTimer()
	var samples []*Sample
	builder := SampleBuilder{
		ProcessIdentifier: identifier.ProcessIdentifier{
			Service:  "TestService",
			Stage:    "test",
			Substage: "onebox",
			Region:   "us-west-2",
		},
		OperationName: "OperationName",
	}

	// An arbitrarily selected distribution of 40 unique metric ids.
	for i := 0; i < 40; i++ {
		sample, err := builder.Build(fmt.Sprintf("TestMetricName%v", i), float64(1), "Count")
		if err != nil {
			b.Error("unexpected error building sample:", err)
		}
		samples = append(samples, sample)
	}
	// With 20 that have multiple values
	for i := 0; i < 20; i++ {
		sample, err := builder.Build(fmt.Sprintf("TestMetricName%v", i), float64(1), "Count")
		if err != nil {
			b.Error("unexpected error building sample:", err)
		}
		samples = append(samples, sample)
	}
	// And one that has a lot of values
	for i := 0; i < 40; i++ {
		sample, err := builder.Build(fmt.Sprintf("TestMetricName1"), float64(1), "Count")
		if err != nil {
			b.Error("unexpected error building sample:", err)
		}
		samples = append(samples, sample)
	}

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		aggregator := NewAggregator(time.Minute)
		for _, sample := range samples {
			aggregator.AggregateSample(sample)
		}
	}
}

type aggregatingObserver struct {
	aggregator *Aggregator
}

func (observer *aggregatingObserver) ObserveSample(sample *Sample) {
	observer.aggregator.AggregateSample(sample)
}
func (observer *aggregatingObserver) Flush()                                      {}
func (observer *aggregatingObserver) Stop()                                       {}
func (observer *aggregatingObserver) BufferedFlush(distributions []*Distribution) {}

// loadRunner runs tasks concurrently with optional setup and cleanup for each thread.
type loadRunner struct {
	Concurrency          int
	TestDuration         time.Duration
	Task                 func([]interface{})
	ThreadContextSetup   func(index int) []interface{}
	ThreadContextCleanup func(index int, parameters []interface{})
	Invocations          int64
}

// Run runs the tasks.
func (runner *loadRunner) Run() {
	testEndTime := time.Now().Add(runner.TestDuration)
	var waitgroup sync.WaitGroup
	for i := 0; i < runner.Concurrency; i++ {
		waitgroup.Add(1)
		threadID := i
		go func() {
			defer waitgroup.Done()
			var threadParameters []interface{}
			if runner.ThreadContextSetup != nil {
				threadParameters = runner.ThreadContextSetup(threadID)
			}
			for time.Now().Before(testEndTime) {
				runner.Task(threadParameters)
				atomic.AddInt64(&runner.Invocations, 1)
			}
			if runner.ThreadContextCleanup != nil {
				runner.ThreadContextCleanup(threadID, threadParameters)
			}
		}()
	}
	waitgroup.Wait()
}

// GetInvocations returns the number of times the task was invoked.
func (runner *loadRunner) GetInvocations() int64 {
	return atomic.LoadInt64(&runner.Invocations)
}
