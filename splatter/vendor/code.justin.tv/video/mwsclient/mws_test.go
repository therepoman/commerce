package mwsclient

import (
	"compress/gzip"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
)

func TestResolveEndpoint(t *testing.T) {
	t.Run("bad api", func(t *testing.T) {
		_, err := resolveEndpoint("kinesis", "us-west-2")
		if err == nil {
			t.Fatalf("expected error")
		}
	})
	t.Run("bad region", func(t *testing.T) {
		_, err := resolveEndpoint(serviceName, "bogus")
		if err == nil {
			t.Fatalf("expected error")
		}
	})
	t.Run("PDX", func(t *testing.T) {
		ep, err := resolveEndpoint(serviceName, "us-west-2")
		if err != nil {
			t.Fatalf("resolveEndpoint; err = %v", err)
		}
		if have, want := ep.URL, "https://monitor-api-public-pdx.amazon.com"; have != want {
			t.Errorf("URL; %q != %q", have, want)
		}
		if have, want := ep.SigningMethod, "v4"; have != want {
			t.Errorf("SigningMethod; %q != %q", have, want)
		}
		if have, want := ep.SigningName, "monitor-api"; have != want {
			t.Errorf("SigningName; %q != %q", have, want)
		}
		if have, want := ep.SigningRegion, "us-west-2"; have != want {
			t.Errorf("SigningRegion; %q != %q", have, want)
		}
	})
	t.Run("TITAN", func(t *testing.T) {
		ep, err := resolveEndpoint(serviceName, "us-titan")
		if err != nil {
			t.Fatalf("resolveEndpoint; err = %v", err)
		}
		if have, want := ep.URL, "https://monitor-api-public-titan.amazon.com"; have != want {
			t.Errorf("URL; %q != %q", have, want)
		}
		if have, want := ep.SigningMethod, "v4"; have != want {
			t.Errorf("SigningMethod; %q != %q", have, want)
		}
		if have, want := ep.SigningName, "monitor-api"; have != want {
			t.Errorf("SigningName; %q != %q", have, want)
		}
		if have, want := ep.SigningRegion, "us-seattle"; have != want {
			t.Errorf("SigningRegion; %q != %q", have, want)
		}
	})
}

func TestBuildZipJSON(t *testing.T) {
	t.Run("", func(t *testing.T) {
		input := &PutMetricDataForAggregationInput{
			MetricReports: []*MetricReport{
				&MetricReport{
					Metadata: &MetricMetadata{
						MinPeriod: "OneMinute",
					},
				},
			},
		}
		req := &request.Request{
			Params: input,
			HTTPRequest: &http.Request{
				Header: make(http.Header),
			},
		}
		buildZipJSON(req)
		if req.Error != nil {
			t.Fatalf("req.Error; err = %v", req.Error)
		}
		if have, want := req.HTTPRequest.Header.Get("Content-Type"), "application/x-gzip"; have != want {
			t.Errorf("Content-Type; %q != %q", have, want)
		}

		gr, err := gzip.NewReader(req.HTTPRequest.Body)
		if err != nil {
			t.Fatalf("gzip.NewReader; err = %v", err)
		}
		bodyBytes, err := ioutil.ReadAll(gr)
		if err != nil {
			t.Fatalf("Body.Read; err = %v", err)
		}
		var in2 PutMetricDataForAggregationInput
		err = json.Unmarshal(bodyBytes, &in2)
		if err != nil {
			t.Fatalf("json.Unmarshal; err = %v", err)
		}
		if have, want := &in2, input; !reflect.DeepEqual(have, want) {
			j2, _ := json.Marshal(&in2)
			t.Errorf("Params;\n%q\n!=\n%q\n", j2, bodyBytes)
		}
	})
}

func TestUnmarshalMetaXML(t *testing.T) {
	t.Run("", func(t *testing.T) {
		var output PutMetricDataForAggregationOutput
		req := &request.Request{
			Data: &output,
			HTTPResponse: &http.Response{
				Body: ioutil.NopCloser(strings.NewReader(`<?xml version="1.0"?>
<PutMetricDataForAggregationResponse xmlns="http://mws.amazonaws.com/doc/2007-07-07/"><NumberOfAttempted>1</NumberOfAttempted><NumberOfCommitted>1</NumberOfCommitted></PutMetricDataForAggregationResponse>`)),
			},
		}
		unmarshalMetaXML(req)
		if req.Error != nil {
			t.Fatalf("req.Error; err = %v", req.Error)
		}
		if have, want := output.NumberOfAttempted, 1; have != want {
			t.Errorf("NumberOfAttempted; %d != %d", have, want)
		}
		if have, want := output.NumberOfCommitted, 1; have != want {
			t.Errorf("NumberOfCommitted; %d != %d", have, want)
		}
	})
}

func TestUnmarshalErrorXML(t *testing.T) {
	t.Run("", func(t *testing.T) {
		req := &request.Request{
			HTTPResponse: &http.Response{
				StatusCode: http.StatusUnauthorized,
				Body: ioutil.NopCloser(strings.NewReader(`<?xml version="1.0"?>
<Response><Errors><Error><Code>AuthFailure</Code><Message>AWS was not able to validate the provided access credentials (TokenExpired)
=== MWS generated canonical string ===
POST
/
Action=PutMetricDataForAggregation&amp;Version=2007-07-07
content-length:370
content-type:application/x-gzip
host:monitor-api-public-pdx.amazon.com
x-amz-date:20180110T062618Z
x-amz-security-token:FQoDYXdzEOX

content-length;content-type;host;x-amz-date;x-amz-security-token
584c838a9b455322bd99f56d396a8dfcd3920a95af01ed9b27b26dbe1b27e366
=== MWS generated string to sign ===
AWS4-HMAC-SHA256
20180110T062618Z
20180110/us-west-2/monitor-api/aws4_request
638838bd0a09a87ccd4de144a6cbd635523bbde4380441108f1bb6314d5f1c03
=== HTTP request signature V4 details ===
X-AMZ-DATE: 20180110T062618Z
Algorithm: AWS4-HMAC-SHA256
Credential: ASIAIJWMEFF35XSJJQUA/20180110/us-west-2/monitor-api/aws4_request
SignedHeaders: [content-length, content-type, host, x-amz-date, x-amz-security-token]
Signature: d60cf0620717bbbf941813a0fa86ff3a21eca7ec3908829e972eb6e09763d4cf

</Message></Error></Errors><RequestID>494a6a10-7b9a-4d6b-a22e-f2e200dae9b7</RequestID></Response>`)),
			},
		}
		unmarshalMetaXML(req)
		if req.Error == nil {
			t.Fatalf("expected req.Error")
		}
		unmarshalErrorXML(req)
		fail, ok := req.Error.(awserr.RequestFailure)
		if !ok {
			t.Fatalf("req.Error type; %T != awserr.RequestFailure", req.Error)
		}
		if have, want := fail.StatusCode(), http.StatusUnauthorized; have != want {
			t.Errorf("req.Error.StatusCode; %d != %d", have, want)
		}
		if have, want := fail.Code(), "AuthFailure"; have != want {
			t.Errorf("req.Error.Code; %q != %q", have, want)
		}
		if have, want := fail.RequestID(), "494a6a10-7b9a-4d6b-a22e-f2e200dae9b7"; have != want {
			t.Errorf("req.Error.RequestID; %q != %q", have, want)
		}
	})
}
