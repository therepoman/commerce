freeStyleJob('commerce-hats') {
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'commerce/hats', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }

  wrappers {
    sshAgent 'git-aws-read-key'
    preBuildCleanup()
    timestamps()
    credentialsBinding {
        file('COURIERD_PRIVATE_KEY', 'courierd')
        file('AWS_CONFIG_FILE', 'aws_config')
    }
  }

  steps {
    shell 'scripts/build.sh'
    shell 'scripts/push.sh'
    saveDeployArtifact 'commerce/hats-ebextensions', 'deploy/'
    shell 'find . -name "coverage.out"'
  }

  publishers {
	reportQuality('commerce/hats', '.manta')
  }
}

freeStyleJob('commerce-hats-prod-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('AWS_CONFIG_FILE', 'aws_config')
            file('AWS_DEPLOY_FILE', 'commerce-hats-prod-deploy')
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/hats-ebextensions'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_CONFIG_FILE=\$AWS_DEPLOY_FILE
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-hats:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                | sed -i "s/hats/hats_prod/" .elasticbeanstalk/config.yml
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy prod-commerce-hats-prod-env --timeout 90""".stripMargin()
    }
}

freeStyleJob('commerce-hats-staging-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('AWS_CONFIG_FILE', 'aws_config')
            file('AWS_DEPLOY_FILE', 'commerce-hats-staging-deploy')
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/hats-ebextensions'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_CONFIG_FILE=\$AWS_DEPLOY_FILE
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-hats:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                | sed -i "s/hats/hats_staging/" .elasticbeanstalk/config.yml
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy staging-commerce-hats-staging-env --timeout 90""".stripMargin()
    }
}