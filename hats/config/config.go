package config

import (
	"os"

	"code.justin.tv/common/appconfig-go/appconfig"
	"code.justin.tv/common/appconfig-go/resolvers"
	"github.com/pkg/errors"
)

const (
	NSStatsd    = "Statsd"
	NSSandstorm = "Standstorm"
	NSHats      = "HATS"
)

type Config struct {
	SandstormConfig  SandstormConfig
	HatsConfig       HatsConfig
	StatsdConfig     StatsdConfig
	RollbarConfig    RollbarConfig
	CloudwatchConfig CloudwatchConfig
}

type SandstormConfig struct {
	RoleArn     string `acg:"Sandstorm:RoleArn"`
	Environment string `acg:"Sandstorm:Environment"`
}

type HatsConfig struct {
	AWSRegion  string `acg:"HATS:AWSRegion"`
	ServerPort string `acg:"HATS:ServerPort"`
}

type StatsdConfig struct {
	URL    string `acg:"Statsd:URL"`
	Prefix string `acg:"Statsd:Prefix"`
	Noop   bool   `acg:"Statsd:Noop"`
}

type RollbarConfig struct {
	SandstormKey string `acg:"Rollbar:SandstormKey"`
	Environment  string `acg:"Rollbar:Environment"`
}

type CloudwatchConfig struct {
	BatchSize            int64  `acg:"Cloudwatch:BatchSize"`
	BufferSize           int64  `acg:"Cloudwatch:BufferSize"`
	Region               string `acg:"Cloudwatch:Region"`
	TwitchTelemetryFlush int64  `acg:"Cloudwatch:TwitchTelemetryFlush"`
	Environment          string `acg:"Cloudwatch:Environment"`
}

func resolveAppconfig() (*appconfig.AppConfig, error) {

	configDir := os.Getenv("CONFIG_DIR")
	if configDir == "" {
		return nil, errors.New("Can't start server without CONFIG_DIR set")
	}
	region := os.Getenv("REGION")

	if region == "" {
		return nil, errors.New("Can't start server without REGION set")
	}
	stage := os.Getenv("ENVIRONMENT")
	if stage == "" {
		return nil, errors.New("Can't start server without ENVIRONMENT set")
	}

	set, err := resolvers.GlobResolve(configDir, "config.acg")
	if err != nil {
		return nil, err
	}
	return set.NewAppConfig(region, stage), nil
}

func Load() (Config, error) {
	config := Config{}
	acg, err := resolveAppconfig()
	if err != nil {
		return config, err
	}

	err = acg.Fill(&config.SandstormConfig)
	if err != nil {
		return config, err
	}

	err = acg.Fill(&config.HatsConfig)
	if err != nil {
		return config, err
	}

	err = acg.Fill(&config.StatsdConfig)
	if err != nil {
		return config, err
	}

	err = acg.Fill(&config.RollbarConfig)
	if err != nil {
		return config, err
	}

	err = acg.Fill(&config.CloudwatchConfig)
	if err != nil {
		return config, err
	}

	return config, nil
}
