// via https://git-aws.internal.justin.tv/achou/privatelink/tree/master/terraform
variable "account_id" {
  type        = "string"
  description = "AWS account the PrivateLink should be setup."
}

variable "aws_region" {
  type        = "string"
  default     = "us-west-2"
  description = "AWS region the PrivateLink should be setup."
}

variable "app_name" {
  type        = "string"
  description = "Application name to create the PrivateLink setup."
}

variable "environment" {
  type        = "string"
  default     = "development"
  description = "Environment PrivateLink setup is for."
}

variable "internal_alb_dns_name" {
  type        = "string"
  description = "DNS name for the load balancer."
}

variable "vpc" {
  type        = "string"
  description = "VPC the PrivateLink should be setup."
}

variable "subnet_a" {
  type        = "string"
  description = "First subnet the PrivateLink should be setup"
}

variable "subnet_b" {
  type        = "string"
  description = "Second subnet the PrivateLink should be setup"
}

variable "subnet_c" {
  type        = "string"
  description = "Third subnet the PrivateLink should be setup"
}

variable "vpc_endpoint_service_allowed_principals" {
  type        = "list"
  description = "The ARNs that are whitelisted to access the VPC endpoint service"
}
