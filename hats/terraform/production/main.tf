variable "aws_region" {
  default = "us-west-2"
}

variable "environment" {
  default = "prod"
}

variable "aws_profile" {
  default = "twitch-hats-aws"
}

variable "systems_env" {
  default = "prod"
}

variable "lb_dns_name" {
  default = "internal-awseb-e-v-AWSEBLoa-MJDPFVGM600U-705223355.us-west-2.elb.amazonaws.com"
}
terraform {
  backend "s3" {
    bucket  = "twitch-hats-aws"
    key     = "tfstate/hats/terraform/prod/hats/terraform.tfstate"
    region  = "us-west-2"
    profile = "twitch-hats-aws"
  }
}

module "hats" {
  source        = "../modules/hats"
  aws_region    = "${var.aws_region}"
  aws_profile   = "${var.aws_profile}"
  systems_env   = "${var.systems_env}"
  env           = "${var.environment}"
  service       = "hats"
  email_addr    = "twitch-hats-aws@amazon.com"
  instance_type = "c4.large"

  entitlement_changes_subscribers = [
    "603200399373", // twitch-chat-aws
    "180265471281", // twitch-fortuna-aws
    "471167244615", // twitch-copo-prod
    "663583029956", // API edge 1
    "435147058744", // API edge 2
    "534808279643",
  ] // Events history service

  entitlement_changes_publishers = [
    "641044725657", // twitch-web-aws subscriptions
    "522398581884", // twitch-subs-aws
    "389947071127", // twitch-destiny-aws
  ]

  elb_min_instances = 6
  elb_max_instances = 20
}

module "network-admin" {
  source  = "git::git+ssh://git@git-aws.internal.justin.tv/terraform-modules/network-admin-role.git//?ref=master"
  account = "twitch-hats-aws"
}
# Get current STS caller identity for AWS account ID

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "systems" {
  backend = "s3"

  config {
    bucket  = "${var.aws_profile}"
    key     = "tfstate/systems/terraform/${var.systems_env}/${var.aws_profile}/terraform.tfstate"
    region  = "${var.aws_region}"
    profile = "${var.aws_profile}"
  }
}

module "privatelink" {
  source                = "../modules/privatelink"
  account_id            = "${data.aws_caller_identity.current.account_id}"
  app_name              = "hats"
  environment           = "${var.environment}"
  internal_alb_dns_name = "${var.lb_dns_name}"
  vpc                   = "${data.terraform_remote_state.systems.vpc_id}"
  subnet_a              = "${data.terraform_remote_state.systems.private_subnets[0]}"
  subnet_b              = "${data.terraform_remote_state.systems.private_subnets[1]}"
  subnet_c              = "${data.terraform_remote_state.systems.private_subnets[2]}"

  vpc_endpoint_service_allowed_principals = [
    "arn:aws:iam::726281605084:root", // boba-dev
    "arn:aws:iam::737882373154:root", // boba-prod
  ]
}
