package telemetry

// DimensionSet holds a mapping from dimension names to
// dimension values
type DimensionSet map[string]string

// MetricID defines a unique and fully qualified name for a
// timeseries metric. Contains a metric name and it's
// dimensions
type MetricID struct {
	Name       string
	Dimensions DimensionSet
}

// NewMetricID creates a new metric from a metric name.
// Dimensions can then be added to the metric with the
// AddDimension and WithDimension methods
func NewMetricID(name string) *MetricID {
	return &MetricID{
		Name:       name,
		Dimensions: make(DimensionSet),
	}
}

// AddDimension adds a dimension to a given MetricID
func (m *MetricID) AddDimension(name, value string) {
	m.Dimensions[name] = value
}

// WithDimension adds a dimension to a given MetricID and also
// returns the Metric
func (m *MetricID) WithDimension(name, value string) *MetricID {
	m.AddDimension(name, value)
	return m
}
