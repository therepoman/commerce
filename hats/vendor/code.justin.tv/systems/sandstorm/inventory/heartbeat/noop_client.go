package heartbeat

import (
	"context"
)

// NoopClient satisfies the heartbeat API
type NoopClient struct {
}

// UpdateHeartbeat ...
func (NoopClient) UpdateHeartbeat(secret *Secret) {}

// Start ...
func (NoopClient) Start() {}

// FlushHeartbeat ...
func (NoopClient) FlushHeartbeat(ctx context.Context) {}

// SendHeartbeat ...
func (NoopClient) SendHeartbeat() {}

// Stop ...
func (NoopClient) Stop() error {
	return nil
}
