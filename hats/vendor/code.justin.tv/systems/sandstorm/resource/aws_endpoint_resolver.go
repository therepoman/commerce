package resource

import "github.com/aws/aws-sdk-go/aws/endpoints"

// AWSEndpointResolver implements endpoints.Resolver
type AWSEndpointResolver struct {
}

// EndpointFor implements endpoints.Resolver
func (r AWSEndpointResolver) EndpointFor(service, region string, optFns ...func(*endpoints.Options)) (endpoints.ResolvedEndpoint, error) {
	if service == endpoints.StsServiceID && region == endpoints.UsWest2RegionID {
		return endpoints.ResolvedEndpoint{
			URL:           "https://sts.us-west-2.amazonaws.com",
			SigningRegion: region,
		}, nil
	}

	return endpoints.DefaultResolver().EndpointFor(service, region, optFns...)
}
