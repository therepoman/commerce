package resource

// Stack is a sandstorm stack
type Stack interface {
	AdminRoleARN() string
	AgentTestingRoleARN() string
	AWSRegion() string
	AccountID() string
	AuditTableName() string
	CloudwatchRoleARN() string
	Environment() string // TODO: depreciate
	HeartbeatsTableName() string
	KMSPrimaryKey() KMSKey
	InventoryAdminRoleARN() string
	InventoryRoleARN() string
	InventoryStatusURL() string
	NamespaceTableName() string
	PutHeartbeatLambdaFunctionName() string
	ProxyURL() string
	SecretsQueueNamePrefix() string
	SecretsTableName() string
	SecretsTopicArn() string
}
