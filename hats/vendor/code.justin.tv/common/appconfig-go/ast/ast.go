package ast

import (
	"log"
	"strconv"

	"strings"

	"fmt"

	"code.justin.tv/common/appconfig-go/parser"
	"github.com/antlr/antlr4/runtime/Go/antlr"
)

const (
	OperatorEqual  = "="
	OperatorAppend = "+="
)

type AppConfigListener struct {
	Tree     *AppConfigTree
	Rule     *AppConfigRule
	ObjKey   *string
	Selector []string
}

// VisitTerminal is called when a terminal node is visited.
func (s *AppConfigListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *AppConfigListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *AppConfigListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *AppConfigListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterFile is called when production file is entered.
func (s *AppConfigListener) EnterFile(ctx *parser.FileContext) {}

// EnterRecord is called when production record is entered.
func (s *AppConfigListener) EnterRecord(ctx *parser.RecordContext) {
	s.Rule = &AppConfigRule{
		Keys:      nil,
		Selectors: nil,
		Namespace: "",
		Operator:  "",
	}

	if ctx.Selectors() == nil {
		return
	}

	s.Selector = []string{}
	s.Rule.Keys = []string{}
}

// ExitRecord is called when production record is exited.
func (s *AppConfigListener) ExitRecord(ctx *parser.RecordContext) {
	if len(s.Rule.Keys) == 0 {
		return
	}
	s.Rule.Operator = ctx.OPERATOR().GetText()

	s.Tree.Rules = append(s.Tree.Rules, s.Rule)
	if s.Rule.Selectors == nil {
		s.Rule.Selectors = s.Selector
	}
}

// EnterSelectors is called when production selectors is entered.
func (s *AppConfigListener) EnterSelectors(ctx *parser.SelectorsContext) {}

// ExitSelectors is called when production selectors is exited.
func (s *AppConfigListener) ExitSelectors(ctx *parser.SelectorsContext) {
	if s.Rule.Selectors == nil {
		s.Rule.Selectors = []string{}
	}
	for _, selector := range ctx.AllSelector() {
		s.Rule.Selectors = append(s.Rule.Selectors, selector.GetText())
		s.Selector = append(s.Selector, selector.GetText())
	}
}

// EnterSelector is called when production selector is entered.
func (s *AppConfigListener) EnterSelector(ctx *parser.SelectorContext) {}

// ExitSelector is called when production selector is exited.
func (s *AppConfigListener) ExitSelector(ctx *parser.SelectorContext) {}

// EnterKeyspec is called when production keyspec is entered.
func (s *AppConfigListener) EnterKeyspec(ctx *parser.KeyspecContext) {}

// ExitKeyspec is called when production keyspec is exited.
func (s *AppConfigListener) ExitKeyspec(ctx *parser.KeyspecContext) {}

// EnterNamespace is called when production namespace is entered.
func (s *AppConfigListener) EnterNamespace(ctx *parser.NamespaceContext) {
	s.Rule.Namespace = ctx.GetText()
}

// ExitNamespace is called when production namespace is exited.
func (s *AppConfigListener) ExitNamespace(ctx *parser.NamespaceContext) {}

// EnterKeys is called when production keys is entered.
func (s *AppConfigListener) EnterKeys(ctx *parser.KeysContext) {}

// ExitKeys is called when production keys is exited.
func (s *AppConfigListener) ExitKeys(ctx *parser.KeysContext) {
	for _, key := range ctx.AllIDENTIFIER() {
		s.Rule.Keys = append(s.Rule.Keys, key.GetText())
	}
}

func getValue(ctx *parser.ValueContext) AppConfigValue {
	value := AppConfigValue{}
	if ctx.NUMBER() != nil {
		strI := ctx.NUMBER().GetText()
		i, err := strconv.ParseInt(strI, 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		value.IntValue = &i
	} else if ctx.STRING() != nil {
		txt := ctx.STRING().GetText()
		txt = txt[1:]
		txt = txt[0 : len(txt)-1]
		value.StrValue = &txt
	} else if ctx.BOOL() != nil {
		txt := ctx.BOOL().GetText()
		txt = strings.ToLower(txt)
		tValue := false
		if txt == "true" {
			tValue = true
		}
		value.BoolValue = &tValue
	}
	return value
}

// EnterValue is called when production value is entered.
func (s *AppConfigListener) EnterValue(ctx *parser.ValueContext) {
	value := getValue(ctx)
	if s.Rule.ArrayValue != nil {
		s.Rule.ArrayValue.Array = append(s.Rule.ArrayValue.Array, &value)
		return
	}
	if s.Rule.HashValue != nil {
		s.Rule.HashValue.Hash[*s.ObjKey] = value
		return
	}

	if value.IntValue != nil {
		s.Rule.IntValue = value.IntValue
	} else if value.StrValue != nil {
		s.Rule.StrValue = value.StrValue
	} else if value.BoolValue != nil {
		s.Rule.BoolValue = value.BoolValue
	}
}

// ExitValue is called when production value is exited.
func (s *AppConfigListener) ExitValue(ctx *parser.ValueContext) {}

// EnterArray is called when production array is entered.
func (s *AppConfigListener) EnterArray(ctx *parser.ArrayContext) {
	s.Rule.ArrayValue = &AppConfigArrayValue{
		Array: []*AppConfigValue{},
	}
}

// ExitArray is called when production array is exited.
func (s *AppConfigListener) ExitArray(ctx *parser.ArrayContext) {}

// EnterObj is called when production obj is entered.
func (s *AppConfigListener) EnterObj(ctx *parser.ObjContext) {
	s.Rule.HashValue = &AppConfigHashValue{
		Hash: make(map[string]AppConfigValue),
	}
}

// ExitObj is called when production obj is exited.
func (s *AppConfigListener) ExitObj(ctx *parser.ObjContext) {}

// EnterPair is called when production pair is entered.
func (s *AppConfigListener) EnterPair(ctx *parser.PairContext) {
	txt := ctx.STRING().GetText()
	txt = txt[1:]
	txt = txt[0 : len(txt)-1]
	s.ObjKey = &txt
}

// ExitPair is called when production pair is exited.
func (s *AppConfigListener) ExitPair(ctx *parser.PairContext) {}

// ExitFile is called when production file is exited.
func (s *AppConfigListener) ExitFile(ctx *parser.FileContext) {
}

type AppConfigTree struct {
	Rules []*AppConfigRule
}

type AppConfigRule struct {
	Selectors []string
	Namespace string
	Keys      []string
	Operator  string
	// Values
	StrValue   *string
	IntValue   *int64
	ArrayValue *AppConfigArrayValue
	HashValue  *AppConfigHashValue
	BoolValue  *bool
}

func (this *AppConfigRule) String() string {
	selectors := strings.Join(this.Selectors, ".")
	keys := strings.Join(this.Keys, ".")
	strValue := ""
	if this.IntValue != nil {
		strValue = fmt.Sprintf("%d", *this.IntValue)
	} else if this.StrValue != nil {
		strValue = fmt.Sprintf("\"%s\"", *this.StrValue)
	} else if this.BoolValue != nil {
		if *this.BoolValue {
			strValue = "true"
		} else {
			strValue = "false"
		}
	}
	return fmt.Sprintf("%s:%s:%s = %s;", selectors, this.Namespace, keys, strValue)
}

type AppConfigArrayValue struct {
	Array []*AppConfigValue
}

type AppConfigHashValue struct {
	Hash map[string]AppConfigValue
}

type AppConfigValue struct {
	StrValue  *string
	IntValue  *int64
	BoolValue *bool
}

type AppConfigParser struct {
}

func NewAppConfigParser() *AppConfigParser {
	return &AppConfigParser{}
}

func (this *AppConfigParser) Parse(data string) (*AppConfigTree, error) {
	listener := &AppConfigListener{
		Tree: &AppConfigTree{
			Rules: []*AppConfigRule{},
		},
	}

	lexer := parser.NewAppConfigLexer(antlr.NewInputStream(data))

	stream := antlr.NewCommonTokenStream(lexer, 0)
	stream.Fill()

	acParser := parser.NewAppConfigParser(stream)

	tree := acParser.File()

	walker := antlr.NewParseTreeWalker()
	walker.Walk(listener, tree)

	return listener.Tree, nil
}
