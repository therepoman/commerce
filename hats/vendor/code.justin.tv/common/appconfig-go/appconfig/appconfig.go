package appconfig

import (
	"errors"
	"fmt"
	"strings"

	"reflect"

	"code.justin.tv/common/appconfig-go/__vendor/github.com/fatih/structs"
	"code.justin.tv/common/appconfig-go/ast"
)

type AppConfigSet struct {
	Rules []*ast.AppConfigRule
}

func NewAppConfigSet(rules ...[]*ast.AppConfigRule) *AppConfigSet {
	set := &AppConfigSet{
		Rules: []*ast.AppConfigRule{},
	}
	for _, ruleSet := range rules {
		for _, rule := range ruleSet {
			set.Add(rule)
		}
	}
	return set
}

func (this *AppConfigSet) NewAppConfig(selectors ...string) *AppConfig {
	return &AppConfig{
		Rules:     this.Rules,
		Selectors: selectors,
	}
}

func (this *AppConfigSet) String() string {
	strOut := ""

	for _, r := range this.Rules {
		strOut += r.String() + "\n"
	}

	return strOut
}

func (this *AppConfigSet) Add(rule *ast.AppConfigRule) {
	this.Rules = append(this.Rules, rule)
}

type AppConfig struct {
	Rules     []*ast.AppConfigRule
	Selectors []string
}

func (this *AppConfig) MatchingRule(namespace string, keys ...string) *ast.AppConfigRule {
	matches := []*ast.AppConfigRule{}
RULESEARCH:
	for _, r := range this.Rules {
		if r.Namespace != namespace {
			continue
		}
		if len(r.Keys) != len(keys) {
			continue
		}

		for i := 0; i < len(r.Keys); i++ {
			if r.Keys[i] != keys[i] {
				continue RULESEARCH
			}
		}

		matches = append(matches, r)
	}

	if len(matches) == 0 {
		return nil
	}

	var match *ast.AppConfigRule
	score := -1

RULES:
	for _, rule := range matches {
		thisScore := 0
		selectors := rule.Selectors
		if len(selectors) != len(this.Selectors) { // mismatch == 0 score
			thisScore = 0
		} else {
			for i := 0; i < len(selectors); i++ {
				ruleSelector := selectors[i]
				mySelector := this.Selectors[i]
				if ruleSelector == "*" {
					continue
				}
				if ruleSelector == mySelector {
					thisScore++
					continue
				}
				continue RULES
			}
		}
		if thisScore > score {
			match = rule
			score = thisScore
		}
	}
	return match
}

func (this *AppConfig) HasValue(namespace string, keys ...string) bool {
	match := this.MatchingRule(namespace, keys...)
	return match != nil
}

func (this *AppConfig) int64(namespace string, keys ...string) *int64 {
	match := this.MatchingRule(namespace, keys...)
	if match == nil {
		return nil
	}
	return match.IntValue
}

func (this *AppConfig) string(namespace string, keys ...string) *string {
	match := this.MatchingRule(namespace, keys...)
	if match == nil {
		return nil
	}
	return match.StrValue
}

func (this *AppConfig) bool(namespace string, keys ...string) *bool {
	match := this.MatchingRule(namespace, keys...)
	if match == nil {
		return nil
	}
	return match.BoolValue
}

func (this *AppConfig) Fill(i interface{}) error {
	s := structs.New(i)
	for _, field := range s.Fields() {
		acg := field.Tag("acg")
		if acg == "" {
			continue
		}
		parts := strings.Split(acg, ":")
		if len(parts) != 2 {
			strErr := fmt.Sprintf(
				"Struct tag must provide a Key, tag: \"%s\", Type: \"%s\"",
				acg,
				s.Name())
			return errors.New(strErr)
		}

		ns := parts[0]
		keys := strings.Split(parts[1], ".")

		if !this.HasValue(ns, keys...) {
			strErr := fmt.Sprintf(
				"No value in config for required value \"%s\"",
				acg)
			return errors.New(strErr)
		}

		if field.Kind() == reflect.String {
			val := this.string(ns, keys...)
			if val == nil {
				continue
			}
			field.Set(*val)
		} else if field.Kind() == reflect.Bool {
			val := this.bool(ns, keys...)
			if val == nil {
				continue
			}
			field.Set(*val)
		} else if field.Kind() == reflect.Int64 {
			val := this.int64(ns, keys...)
			if val == nil {
				continue
			}
			field.Set(*val)
		}
	}

	return nil
}
