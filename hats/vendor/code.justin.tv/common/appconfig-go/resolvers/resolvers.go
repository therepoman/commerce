package resolvers

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"strings"

	"code.justin.tv/common/appconfig-go/appconfig"
	"code.justin.tv/common/appconfig-go/ast"
)

func GlobResolve(dir string, types string) (*appconfig.AppConfigSet, error) {
	set := appconfig.NewAppConfigSet()

	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if f.IsDir() {
			return nil
		}

		if !strings.HasSuffix(path, types) {
			return nil
		}

		buf, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		tree, err := ast.NewAppConfigParser().Parse(string(buf))

		for _, rule := range tree.Rules {
			set.Add(rule)
		}
		return nil
	})

	return set, err
}

func FileResolve(file string) (*appconfig.AppConfigSet, error) {
	set := appconfig.NewAppConfigSet()
	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	tree, err := ast.NewAppConfigParser().Parse(string(buf))

	for _, rule := range tree.Rules {
		set.Add(rule)
	}
	return set, nil
}
