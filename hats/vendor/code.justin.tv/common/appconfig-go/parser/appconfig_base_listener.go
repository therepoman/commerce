// Generated from AppConfig.g4 by ANTLR 4.7.

package parser // AppConfig

import "github.com/antlr/antlr4/runtime/Go/antlr"

// BaseAppConfigListener is a complete listener for a parse tree produced by AppConfigParser.
type BaseAppConfigListener struct{}

var _ AppConfigListener = &BaseAppConfigListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseAppConfigListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseAppConfigListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseAppConfigListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseAppConfigListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterFile is called when production file is entered.
func (s *BaseAppConfigListener) EnterFile(ctx *FileContext) {}

// ExitFile is called when production file is exited.
func (s *BaseAppConfigListener) ExitFile(ctx *FileContext) {}

// EnterRecord is called when production record is entered.
func (s *BaseAppConfigListener) EnterRecord(ctx *RecordContext) {}

// ExitRecord is called when production record is exited.
func (s *BaseAppConfigListener) ExitRecord(ctx *RecordContext) {}

// EnterSelectors is called when production selectors is entered.
func (s *BaseAppConfigListener) EnterSelectors(ctx *SelectorsContext) {}

// ExitSelectors is called when production selectors is exited.
func (s *BaseAppConfigListener) ExitSelectors(ctx *SelectorsContext) {}

// EnterSelector is called when production selector is entered.
func (s *BaseAppConfigListener) EnterSelector(ctx *SelectorContext) {}

// ExitSelector is called when production selector is exited.
func (s *BaseAppConfigListener) ExitSelector(ctx *SelectorContext) {}

// EnterKeyspec is called when production keyspec is entered.
func (s *BaseAppConfigListener) EnterKeyspec(ctx *KeyspecContext) {}

// ExitKeyspec is called when production keyspec is exited.
func (s *BaseAppConfigListener) ExitKeyspec(ctx *KeyspecContext) {}

// EnterNamespace is called when production namespace is entered.
func (s *BaseAppConfigListener) EnterNamespace(ctx *NamespaceContext) {}

// ExitNamespace is called when production namespace is exited.
func (s *BaseAppConfigListener) ExitNamespace(ctx *NamespaceContext) {}

// EnterKeys is called when production keys is entered.
func (s *BaseAppConfigListener) EnterKeys(ctx *KeysContext) {}

// ExitKeys is called when production keys is exited.
func (s *BaseAppConfigListener) ExitKeys(ctx *KeysContext) {}

// EnterValue is called when production value is entered.
func (s *BaseAppConfigListener) EnterValue(ctx *ValueContext) {}

// ExitValue is called when production value is exited.
func (s *BaseAppConfigListener) ExitValue(ctx *ValueContext) {}

// EnterArray is called when production array is entered.
func (s *BaseAppConfigListener) EnterArray(ctx *ArrayContext) {}

// ExitArray is called when production array is exited.
func (s *BaseAppConfigListener) ExitArray(ctx *ArrayContext) {}

// EnterObj is called when production obj is entered.
func (s *BaseAppConfigListener) EnterObj(ctx *ObjContext) {}

// ExitObj is called when production obj is exited.
func (s *BaseAppConfigListener) ExitObj(ctx *ObjContext) {}

// EnterPair is called when production pair is entered.
func (s *BaseAppConfigListener) EnterPair(ctx *PairContext) {}

// ExitPair is called when production pair is exited.
func (s *BaseAppConfigListener) ExitPair(ctx *PairContext) {}
