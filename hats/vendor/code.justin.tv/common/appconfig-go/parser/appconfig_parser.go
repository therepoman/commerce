// Generated from AppConfig.g4 by ANTLR 4.7.

package parser // AppConfig

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 19, 119,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 3, 2,
	7, 2, 26, 10, 2, 12, 2, 14, 2, 29, 11, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 3, 42, 10, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 5, 3, 49, 10, 3, 5, 3, 51, 10, 3, 3, 4, 3, 4, 3, 4, 7, 4, 56,
	10, 4, 12, 4, 14, 4, 59, 11, 4, 3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3, 6, 3,
	7, 3, 7, 3, 8, 3, 8, 3, 8, 7, 8, 72, 10, 8, 12, 8, 14, 8, 75, 11, 8, 3,
	9, 3, 9, 3, 9, 3, 9, 3, 9, 3, 9, 5, 9, 83, 10, 9, 3, 10, 3, 10, 3, 10,
	3, 10, 7, 10, 89, 10, 10, 12, 10, 14, 10, 92, 11, 10, 3, 10, 3, 10, 3,
	10, 3, 10, 5, 10, 98, 10, 10, 3, 11, 3, 11, 3, 11, 3, 11, 7, 11, 104, 10,
	11, 12, 11, 14, 11, 107, 11, 11, 3, 11, 3, 11, 3, 11, 3, 11, 5, 11, 113,
	10, 11, 3, 12, 3, 12, 3, 12, 3, 12, 3, 12, 2, 2, 13, 2, 4, 6, 8, 10, 12,
	14, 16, 18, 20, 22, 2, 3, 4, 2, 7, 7, 14, 14, 2, 123, 2, 27, 3, 2, 2, 2,
	4, 50, 3, 2, 2, 2, 6, 52, 3, 2, 2, 2, 8, 60, 3, 2, 2, 2, 10, 62, 3, 2,
	2, 2, 12, 66, 3, 2, 2, 2, 14, 68, 3, 2, 2, 2, 16, 82, 3, 2, 2, 2, 18, 97,
	3, 2, 2, 2, 20, 112, 3, 2, 2, 2, 22, 114, 3, 2, 2, 2, 24, 26, 5, 4, 3,
	2, 25, 24, 3, 2, 2, 2, 26, 29, 3, 2, 2, 2, 27, 25, 3, 2, 2, 2, 27, 28,
	3, 2, 2, 2, 28, 3, 3, 2, 2, 2, 29, 27, 3, 2, 2, 2, 30, 31, 5, 6, 4, 2,
	31, 32, 7, 3, 2, 2, 32, 33, 7, 4, 2, 2, 33, 51, 3, 2, 2, 2, 34, 35, 5,
	6, 4, 2, 35, 36, 7, 3, 2, 2, 36, 37, 5, 10, 6, 2, 37, 38, 7, 15, 2, 2,
	38, 39, 5, 16, 9, 2, 39, 41, 7, 5, 2, 2, 40, 42, 7, 4, 2, 2, 41, 40, 3,
	2, 2, 2, 41, 42, 3, 2, 2, 2, 42, 51, 3, 2, 2, 2, 43, 44, 5, 10, 6, 2, 44,
	45, 7, 15, 2, 2, 45, 46, 5, 16, 9, 2, 46, 48, 7, 5, 2, 2, 47, 49, 7, 4,
	2, 2, 48, 47, 3, 2, 2, 2, 48, 49, 3, 2, 2, 2, 49, 51, 3, 2, 2, 2, 50, 30,
	3, 2, 2, 2, 50, 34, 3, 2, 2, 2, 50, 43, 3, 2, 2, 2, 51, 5, 3, 2, 2, 2,
	52, 57, 5, 8, 5, 2, 53, 54, 7, 6, 2, 2, 54, 56, 5, 8, 5, 2, 55, 53, 3,
	2, 2, 2, 56, 59, 3, 2, 2, 2, 57, 55, 3, 2, 2, 2, 57, 58, 3, 2, 2, 2, 58,
	7, 3, 2, 2, 2, 59, 57, 3, 2, 2, 2, 60, 61, 9, 2, 2, 2, 61, 9, 3, 2, 2,
	2, 62, 63, 5, 12, 7, 2, 63, 64, 7, 3, 2, 2, 64, 65, 5, 14, 8, 2, 65, 11,
	3, 2, 2, 2, 66, 67, 7, 14, 2, 2, 67, 13, 3, 2, 2, 2, 68, 73, 7, 14, 2,
	2, 69, 70, 7, 6, 2, 2, 70, 72, 7, 14, 2, 2, 71, 69, 3, 2, 2, 2, 72, 75,
	3, 2, 2, 2, 73, 71, 3, 2, 2, 2, 73, 74, 3, 2, 2, 2, 74, 15, 3, 2, 2, 2,
	75, 73, 3, 2, 2, 2, 76, 83, 7, 13, 2, 2, 77, 83, 7, 17, 2, 2, 78, 83, 7,
	18, 2, 2, 79, 83, 7, 16, 2, 2, 80, 83, 5, 18, 10, 2, 81, 83, 5, 20, 11,
	2, 82, 76, 3, 2, 2, 2, 82, 77, 3, 2, 2, 2, 82, 78, 3, 2, 2, 2, 82, 79,
	3, 2, 2, 2, 82, 80, 3, 2, 2, 2, 82, 81, 3, 2, 2, 2, 83, 17, 3, 2, 2, 2,
	84, 85, 7, 8, 2, 2, 85, 90, 5, 16, 9, 2, 86, 87, 7, 9, 2, 2, 87, 89, 5,
	16, 9, 2, 88, 86, 3, 2, 2, 2, 89, 92, 3, 2, 2, 2, 90, 88, 3, 2, 2, 2, 90,
	91, 3, 2, 2, 2, 91, 93, 3, 2, 2, 2, 92, 90, 3, 2, 2, 2, 93, 94, 7, 10,
	2, 2, 94, 98, 3, 2, 2, 2, 95, 96, 7, 8, 2, 2, 96, 98, 7, 10, 2, 2, 97,
	84, 3, 2, 2, 2, 97, 95, 3, 2, 2, 2, 98, 19, 3, 2, 2, 2, 99, 100, 7, 11,
	2, 2, 100, 105, 5, 22, 12, 2, 101, 102, 7, 9, 2, 2, 102, 104, 5, 22, 12,
	2, 103, 101, 3, 2, 2, 2, 104, 107, 3, 2, 2, 2, 105, 103, 3, 2, 2, 2, 105,
	106, 3, 2, 2, 2, 106, 108, 3, 2, 2, 2, 107, 105, 3, 2, 2, 2, 108, 109,
	7, 12, 2, 2, 109, 113, 3, 2, 2, 2, 110, 111, 7, 11, 2, 2, 111, 113, 7,
	12, 2, 2, 112, 99, 3, 2, 2, 2, 112, 110, 3, 2, 2, 2, 113, 21, 3, 2, 2,
	2, 114, 115, 7, 18, 2, 2, 115, 116, 7, 3, 2, 2, 116, 117, 5, 16, 9, 2,
	117, 23, 3, 2, 2, 2, 13, 27, 41, 48, 50, 57, 73, 82, 90, 97, 105, 112,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "':'", "'\n'", "';'", "'.'", "'*'", "'['", "','", "']'", "'{'", "'}'",
	"", "", "'='",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "BOOL", "IDENTIFIER", "OPERATOR",
	"NUMBER", "INT", "STRING", "WS",
}

var ruleNames = []string{
	"file", "record", "selectors", "selector", "keyspec", "namespace", "keys",
	"value", "array", "obj", "pair",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type AppConfigParser struct {
	*antlr.BaseParser
}

func NewAppConfigParser(input antlr.TokenStream) *AppConfigParser {
	this := new(AppConfigParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "AppConfig.g4"

	return this
}

// AppConfigParser tokens.
const (
	AppConfigParserEOF        = antlr.TokenEOF
	AppConfigParserT__0       = 1
	AppConfigParserT__1       = 2
	AppConfigParserT__2       = 3
	AppConfigParserT__3       = 4
	AppConfigParserT__4       = 5
	AppConfigParserT__5       = 6
	AppConfigParserT__6       = 7
	AppConfigParserT__7       = 8
	AppConfigParserT__8       = 9
	AppConfigParserT__9       = 10
	AppConfigParserBOOL       = 11
	AppConfigParserIDENTIFIER = 12
	AppConfigParserOPERATOR   = 13
	AppConfigParserNUMBER     = 14
	AppConfigParserINT        = 15
	AppConfigParserSTRING     = 16
	AppConfigParserWS         = 17
)

// AppConfigParser rules.
const (
	AppConfigParserRULE_file      = 0
	AppConfigParserRULE_record    = 1
	AppConfigParserRULE_selectors = 2
	AppConfigParserRULE_selector  = 3
	AppConfigParserRULE_keyspec   = 4
	AppConfigParserRULE_namespace = 5
	AppConfigParserRULE_keys      = 6
	AppConfigParserRULE_value     = 7
	AppConfigParserRULE_array     = 8
	AppConfigParserRULE_obj       = 9
	AppConfigParserRULE_pair      = 10
)

// IFileContext is an interface to support dynamic dispatch.
type IFileContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFileContext differentiates from other interfaces.
	IsFileContext()
}

type FileContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFileContext() *FileContext {
	var p = new(FileContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_file
	return p
}

func (*FileContext) IsFileContext() {}

func NewFileContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FileContext {
	var p = new(FileContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_file

	return p
}

func (s *FileContext) GetParser() antlr.Parser { return s.parser }

func (s *FileContext) AllRecord() []IRecordContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IRecordContext)(nil)).Elem())
	var tst = make([]IRecordContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IRecordContext)
		}
	}

	return tst
}

func (s *FileContext) Record(i int) IRecordContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRecordContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IRecordContext)
}

func (s *FileContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FileContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FileContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterFile(s)
	}
}

func (s *FileContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitFile(s)
	}
}

func (p *AppConfigParser) File() (localctx IFileContext) {
	localctx = NewFileContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, AppConfigParserRULE_file)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(25)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == AppConfigParserT__4 || _la == AppConfigParserIDENTIFIER {
		{
			p.SetState(22)
			p.Record()
		}

		p.SetState(27)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IRecordContext is an interface to support dynamic dispatch.
type IRecordContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRecordContext differentiates from other interfaces.
	IsRecordContext()
}

type RecordContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRecordContext() *RecordContext {
	var p = new(RecordContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_record
	return p
}

func (*RecordContext) IsRecordContext() {}

func NewRecordContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RecordContext {
	var p = new(RecordContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_record

	return p
}

func (s *RecordContext) GetParser() antlr.Parser { return s.parser }

func (s *RecordContext) Selectors() ISelectorsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISelectorsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISelectorsContext)
}

func (s *RecordContext) Keyspec() IKeyspecContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IKeyspecContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IKeyspecContext)
}

func (s *RecordContext) OPERATOR() antlr.TerminalNode {
	return s.GetToken(AppConfigParserOPERATOR, 0)
}

func (s *RecordContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *RecordContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RecordContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RecordContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterRecord(s)
	}
}

func (s *RecordContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitRecord(s)
	}
}

func (p *AppConfigParser) Record() (localctx IRecordContext) {
	localctx = NewRecordContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, AppConfigParserRULE_record)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(48)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 3, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(28)
			p.Selectors()
		}
		{
			p.SetState(29)
			p.Match(AppConfigParserT__0)
		}
		{
			p.SetState(30)
			p.Match(AppConfigParserT__1)
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(32)
			p.Selectors()
		}
		{
			p.SetState(33)
			p.Match(AppConfigParserT__0)
		}
		{
			p.SetState(34)
			p.Keyspec()
		}
		{
			p.SetState(35)
			p.Match(AppConfigParserOPERATOR)
		}
		{
			p.SetState(36)
			p.Value()
		}
		{
			p.SetState(37)
			p.Match(AppConfigParserT__2)
		}
		p.SetState(39)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == AppConfigParserT__1 {
			{
				p.SetState(38)
				p.Match(AppConfigParserT__1)
			}

		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(41)
			p.Keyspec()
		}
		{
			p.SetState(42)
			p.Match(AppConfigParserOPERATOR)
		}
		{
			p.SetState(43)
			p.Value()
		}
		{
			p.SetState(44)
			p.Match(AppConfigParserT__2)
		}
		p.SetState(46)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == AppConfigParserT__1 {
			{
				p.SetState(45)
				p.Match(AppConfigParserT__1)
			}

		}

	}

	return localctx
}

// ISelectorsContext is an interface to support dynamic dispatch.
type ISelectorsContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSelectorsContext differentiates from other interfaces.
	IsSelectorsContext()
}

type SelectorsContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySelectorsContext() *SelectorsContext {
	var p = new(SelectorsContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_selectors
	return p
}

func (*SelectorsContext) IsSelectorsContext() {}

func NewSelectorsContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SelectorsContext {
	var p = new(SelectorsContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_selectors

	return p
}

func (s *SelectorsContext) GetParser() antlr.Parser { return s.parser }

func (s *SelectorsContext) AllSelector() []ISelectorContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*ISelectorContext)(nil)).Elem())
	var tst = make([]ISelectorContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(ISelectorContext)
		}
	}

	return tst
}

func (s *SelectorsContext) Selector(i int) ISelectorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISelectorContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(ISelectorContext)
}

func (s *SelectorsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SelectorsContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SelectorsContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterSelectors(s)
	}
}

func (s *SelectorsContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitSelectors(s)
	}
}

func (p *AppConfigParser) Selectors() (localctx ISelectorsContext) {
	localctx = NewSelectorsContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, AppConfigParserRULE_selectors)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(50)
		p.Selector()
	}
	p.SetState(55)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == AppConfigParserT__3 {
		{
			p.SetState(51)
			p.Match(AppConfigParserT__3)
		}
		{
			p.SetState(52)
			p.Selector()
		}

		p.SetState(57)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// ISelectorContext is an interface to support dynamic dispatch.
type ISelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSelectorContext differentiates from other interfaces.
	IsSelectorContext()
}

type SelectorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySelectorContext() *SelectorContext {
	var p = new(SelectorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_selector
	return p
}

func (*SelectorContext) IsSelectorContext() {}

func NewSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SelectorContext {
	var p = new(SelectorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_selector

	return p
}

func (s *SelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *SelectorContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(AppConfigParserIDENTIFIER, 0)
}

func (s *SelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SelectorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterSelector(s)
	}
}

func (s *SelectorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitSelector(s)
	}
}

func (p *AppConfigParser) Selector() (localctx ISelectorContext) {
	localctx = NewSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, AppConfigParserRULE_selector)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(58)
	_la = p.GetTokenStream().LA(1)

	if !(_la == AppConfigParserT__4 || _la == AppConfigParserIDENTIFIER) {
		p.GetErrorHandler().RecoverInline(p)
	} else {
		p.GetErrorHandler().ReportMatch(p)
		p.Consume()
	}

	return localctx
}

// IKeyspecContext is an interface to support dynamic dispatch.
type IKeyspecContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsKeyspecContext differentiates from other interfaces.
	IsKeyspecContext()
}

type KeyspecContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyKeyspecContext() *KeyspecContext {
	var p = new(KeyspecContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_keyspec
	return p
}

func (*KeyspecContext) IsKeyspecContext() {}

func NewKeyspecContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *KeyspecContext {
	var p = new(KeyspecContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_keyspec

	return p
}

func (s *KeyspecContext) GetParser() antlr.Parser { return s.parser }

func (s *KeyspecContext) Namespace() INamespaceContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*INamespaceContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(INamespaceContext)
}

func (s *KeyspecContext) Keys() IKeysContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IKeysContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IKeysContext)
}

func (s *KeyspecContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *KeyspecContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *KeyspecContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterKeyspec(s)
	}
}

func (s *KeyspecContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitKeyspec(s)
	}
}

func (p *AppConfigParser) Keyspec() (localctx IKeyspecContext) {
	localctx = NewKeyspecContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, AppConfigParserRULE_keyspec)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(60)
		p.Namespace()
	}
	{
		p.SetState(61)
		p.Match(AppConfigParserT__0)
	}
	{
		p.SetState(62)
		p.Keys()
	}

	return localctx
}

// INamespaceContext is an interface to support dynamic dispatch.
type INamespaceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsNamespaceContext differentiates from other interfaces.
	IsNamespaceContext()
}

type NamespaceContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyNamespaceContext() *NamespaceContext {
	var p = new(NamespaceContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_namespace
	return p
}

func (*NamespaceContext) IsNamespaceContext() {}

func NewNamespaceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *NamespaceContext {
	var p = new(NamespaceContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_namespace

	return p
}

func (s *NamespaceContext) GetParser() antlr.Parser { return s.parser }

func (s *NamespaceContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(AppConfigParserIDENTIFIER, 0)
}

func (s *NamespaceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NamespaceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *NamespaceContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterNamespace(s)
	}
}

func (s *NamespaceContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitNamespace(s)
	}
}

func (p *AppConfigParser) Namespace() (localctx INamespaceContext) {
	localctx = NewNamespaceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, AppConfigParserRULE_namespace)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(64)
		p.Match(AppConfigParserIDENTIFIER)
	}

	return localctx
}

// IKeysContext is an interface to support dynamic dispatch.
type IKeysContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsKeysContext differentiates from other interfaces.
	IsKeysContext()
}

type KeysContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyKeysContext() *KeysContext {
	var p = new(KeysContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_keys
	return p
}

func (*KeysContext) IsKeysContext() {}

func NewKeysContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *KeysContext {
	var p = new(KeysContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_keys

	return p
}

func (s *KeysContext) GetParser() antlr.Parser { return s.parser }

func (s *KeysContext) AllIDENTIFIER() []antlr.TerminalNode {
	return s.GetTokens(AppConfigParserIDENTIFIER)
}

func (s *KeysContext) IDENTIFIER(i int) antlr.TerminalNode {
	return s.GetToken(AppConfigParserIDENTIFIER, i)
}

func (s *KeysContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *KeysContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *KeysContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterKeys(s)
	}
}

func (s *KeysContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitKeys(s)
	}
}

func (p *AppConfigParser) Keys() (localctx IKeysContext) {
	localctx = NewKeysContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, AppConfigParserRULE_keys)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(66)
		p.Match(AppConfigParserIDENTIFIER)
	}
	p.SetState(71)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == AppConfigParserT__3 {
		{
			p.SetState(67)
			p.Match(AppConfigParserT__3)
		}
		{
			p.SetState(68)
			p.Match(AppConfigParserIDENTIFIER)
		}

		p.SetState(73)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IValueContext is an interface to support dynamic dispatch.
type IValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsValueContext differentiates from other interfaces.
	IsValueContext()
}

type ValueContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyValueContext() *ValueContext {
	var p = new(ValueContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_value
	return p
}

func (*ValueContext) IsValueContext() {}

func NewValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ValueContext {
	var p = new(ValueContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_value

	return p
}

func (s *ValueContext) GetParser() antlr.Parser { return s.parser }

func (s *ValueContext) BOOL() antlr.TerminalNode {
	return s.GetToken(AppConfigParserBOOL, 0)
}

func (s *ValueContext) INT() antlr.TerminalNode {
	return s.GetToken(AppConfigParserINT, 0)
}

func (s *ValueContext) STRING() antlr.TerminalNode {
	return s.GetToken(AppConfigParserSTRING, 0)
}

func (s *ValueContext) NUMBER() antlr.TerminalNode {
	return s.GetToken(AppConfigParserNUMBER, 0)
}

func (s *ValueContext) Array() IArrayContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IArrayContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IArrayContext)
}

func (s *ValueContext) Obj() IObjContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IObjContext)
}

func (s *ValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ValueContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterValue(s)
	}
}

func (s *ValueContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitValue(s)
	}
}

func (p *AppConfigParser) Value() (localctx IValueContext) {
	localctx = NewValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, AppConfigParserRULE_value)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(80)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case AppConfigParserBOOL:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(74)
			p.Match(AppConfigParserBOOL)
		}

	case AppConfigParserINT:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(75)
			p.Match(AppConfigParserINT)
		}

	case AppConfigParserSTRING:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(76)
			p.Match(AppConfigParserSTRING)
		}

	case AppConfigParserNUMBER:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(77)
			p.Match(AppConfigParserNUMBER)
		}

	case AppConfigParserT__5:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(78)
			p.Array()
		}

	case AppConfigParserT__8:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(79)
			p.Obj()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IArrayContext is an interface to support dynamic dispatch.
type IArrayContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsArrayContext differentiates from other interfaces.
	IsArrayContext()
}

type ArrayContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyArrayContext() *ArrayContext {
	var p = new(ArrayContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_array
	return p
}

func (*ArrayContext) IsArrayContext() {}

func NewArrayContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ArrayContext {
	var p = new(ArrayContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_array

	return p
}

func (s *ArrayContext) GetParser() antlr.Parser { return s.parser }

func (s *ArrayContext) AllValue() []IValueContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IValueContext)(nil)).Elem())
	var tst = make([]IValueContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IValueContext)
		}
	}

	return tst
}

func (s *ArrayContext) Value(i int) IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *ArrayContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ArrayContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ArrayContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterArray(s)
	}
}

func (s *ArrayContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitArray(s)
	}
}

func (p *AppConfigParser) Array() (localctx IArrayContext) {
	localctx = NewArrayContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, AppConfigParserRULE_array)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(95)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 8, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(82)
			p.Match(AppConfigParserT__5)
		}
		{
			p.SetState(83)
			p.Value()
		}
		p.SetState(88)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == AppConfigParserT__6 {
			{
				p.SetState(84)
				p.Match(AppConfigParserT__6)
			}
			{
				p.SetState(85)
				p.Value()
			}

			p.SetState(90)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}
		{
			p.SetState(91)
			p.Match(AppConfigParserT__7)
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(93)
			p.Match(AppConfigParserT__5)
		}
		{
			p.SetState(94)
			p.Match(AppConfigParserT__7)
		}

	}

	return localctx
}

// IObjContext is an interface to support dynamic dispatch.
type IObjContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsObjContext differentiates from other interfaces.
	IsObjContext()
}

type ObjContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyObjContext() *ObjContext {
	var p = new(ObjContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_obj
	return p
}

func (*ObjContext) IsObjContext() {}

func NewObjContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjContext {
	var p = new(ObjContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_obj

	return p
}

func (s *ObjContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjContext) AllPair() []IPairContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IPairContext)(nil)).Elem())
	var tst = make([]IPairContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IPairContext)
		}
	}

	return tst
}

func (s *ObjContext) Pair(i int) IPairContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPairContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IPairContext)
}

func (s *ObjContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterObj(s)
	}
}

func (s *ObjContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitObj(s)
	}
}

func (p *AppConfigParser) Obj() (localctx IObjContext) {
	localctx = NewObjContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, AppConfigParserRULE_obj)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(110)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 10, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(97)
			p.Match(AppConfigParserT__8)
		}
		{
			p.SetState(98)
			p.Pair()
		}
		p.SetState(103)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == AppConfigParserT__6 {
			{
				p.SetState(99)
				p.Match(AppConfigParserT__6)
			}
			{
				p.SetState(100)
				p.Pair()
			}

			p.SetState(105)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}
		{
			p.SetState(106)
			p.Match(AppConfigParserT__9)
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(108)
			p.Match(AppConfigParserT__8)
		}
		{
			p.SetState(109)
			p.Match(AppConfigParserT__9)
		}

	}

	return localctx
}

// IPairContext is an interface to support dynamic dispatch.
type IPairContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPairContext differentiates from other interfaces.
	IsPairContext()
}

type PairContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPairContext() *PairContext {
	var p = new(PairContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = AppConfigParserRULE_pair
	return p
}

func (*PairContext) IsPairContext() {}

func NewPairContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PairContext {
	var p = new(PairContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = AppConfigParserRULE_pair

	return p
}

func (s *PairContext) GetParser() antlr.Parser { return s.parser }

func (s *PairContext) STRING() antlr.TerminalNode {
	return s.GetToken(AppConfigParserSTRING, 0)
}

func (s *PairContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *PairContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PairContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PairContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.EnterPair(s)
	}
}

func (s *PairContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(AppConfigListener); ok {
		listenerT.ExitPair(s)
	}
}

func (p *AppConfigParser) Pair() (localctx IPairContext) {
	localctx = NewPairContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, AppConfigParserRULE_pair)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(112)
		p.Match(AppConfigParserSTRING)
	}
	{
		p.SetState(113)
		p.Match(AppConfigParserT__0)
	}
	{
		p.SetState(114)
		p.Value()
	}

	return localctx
}
