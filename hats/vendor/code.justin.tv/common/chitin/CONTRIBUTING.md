Contributing to Chitin
===

Filing issues
---

We may be able to help you immediately in the #trace slack channel.

Some issues live on GitHub Enterprise
[in this repo](https://git-aws.internal.justin.tv/common/chitin/issues),
some live there
[in the release/trace repo](https://git-aws.internal.justin.tv/release/trace/issues).

Contributing code
---

We do code review via pull requests.

This repo includes a number of automated tests - please make sure they
continue to pass.
The tests are set up to run on several versions on Go, so pushing your branch
to GHE and allowing Jenkins to run the tests may be the easiest way to check.
The CI system will send a message to the #trace slack channel when a build
completes, both on success and on failure.

Code should pass `gofmt` and `go vet`. Passing
[`errcheck`](https://godoc.org/github.com/kisielk/errcheck) and doing well
under [`golint`](https://godoc.org/github.com/golang/lint/golint) are goals,
though they're not currently enforced.
