Go-Test-Dynamo
==============
Allows you to quickly and easily start a local dynamo db. Downloads, if not present, the current testing dynamo implementation from AWS's S3 account and unwraps it in $GOPATH. Then starts one dynamo per process. With tests this generally means one per package.

Prereqs
-------
* A Java Runtime... Sorry but that's how dynamo db local is packaged.
* An internet connection for the first run.


Usage
-----
With the basic test package you can just make a TestMain either in it's own
test file in the package or in an existing test file. Here's an example:

<pre>
package go_test_dynamo_test

import (
	"testing"
	"code.justin.tv/common/go_test_dynamo"
)

func TestMain(m *testing.M) {
	ddb := go_test_dynamo.Instance()
	code := m.Run()
	err := ddb.Shutdown()
	if err != nil {
		os.Exit(1)
	}
	os.Exit(code)
}
</pre>

This code will create a local dynamo, downloading it if it's not ready and fire it up
on it's own unique open port. You can then access the dynamo from:

<pre>
go_test_dynamo.Instance().Dynamo
</pre>

We suggest you create your tables new for every test case and clean them up with a:

<pre>
defer go_test_dynamo.Instance().Cleanup()
</pre>

At the top of your tests. This will keep tests from stomping on each other.

NOTE: You must use the same credentials and region as the dynamo testing dynamo exposes if you need to make your own Dynamo. Otherwise
dynamo db local will assume it's a different account and give you different tests of tables
