package go_test_dynamo

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"strconv"
	"strings"
	"syscall"
	"time"

	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

// TestingDynamo is a holder struct for handles to running a local dynamodb in another
// process. Use the go_test_dynamo.Instance() method for most access.
type TestingDynamo struct {
	Config  *aws.Config
	Dynamo  dynamodbiface.DynamoDBAPI
	Port    int
	process *exec.Cmd
}

var instance *TestingDynamo

// Instance gets you your current testing dynamo singleton. One per process.
func Instance() *TestingDynamo {
	if instance == nil {
		instance = NewTestingDynamo()
	}
	return instance
}

// URL returns the URL to get to the local dynamo
func (this *TestingDynamo) URL() string {
	return fmt.Sprintf("http://localhost:%d", this.Port)
}

func fmissing(fname string) bool {
	fi, err := os.Stat(fname)
	switch {
	case err != nil:
		return os.IsNotExist(err)
	case fi.IsDir():
		return false
	default:
		return false
	}
}

func ddbdir() string {
	strdir := strings.Split(os.Getenv("GOPATH"), ":")[0]
	return path.Join(strdir, "dynamodb_local")
}

func extractTGZ(reader io.Reader, toDir string) {
	gzf, err := gzip.NewReader(reader)
	if err != nil {
		log.Fatal("Unable to extract dynamo", err)
	}

	tarReader := tar.NewReader(gzf)

	for {
		header, err := tarReader.Next()

		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		name := header.Name

		switch header.Typeflag {
		case tar.TypeDir:
			continue
		case tar.TypeReg:
			fname := path.Join(toDir, name)
			dirname := path.Dir(fname)
			err := os.MkdirAll(dirname, 0777)
			if err != nil {
				log.Fatal("Error making directory", err)
			}
			out, err := os.OpenFile(fname, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0777)
			if err != nil {
				log.Fatal("Error opening dynamo file for write", err)
			}
			defer safeClose(out)
			_, err = io.Copy(out, tarReader)
			if err != nil {
				log.Fatal("Error extracting DynamoDB Local", err)
			}
		default:
		}
	}
}

func safeClose(c io.Closer) {
	err := c.Close()
	if err != nil {
		log.Fatal("Error closing", err)
	}
}

func ensureDynamoPackage() {
	strdir := ddbdir()
	if strdir == "dynamodb_local" {
		log.Print("GOPATH not set, using relative directory for local dynamo")
	}

	if fmissing(strdir) {
		log.Print("Local Dynamo Directory Doesn't Exist, Downloading To", strdir)
		resp, err := http.Get("https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz")
		if err != nil {
			log.Fatal("Unable to download dynamo", err)
		}
		extractTGZ(resp.Body, strdir)
	}
}

// NewTestingDynamo is exposed if you need to create another dynamo for
// some reason... like if you're building and testing this library.
func NewTestingDynamo() *TestingDynamo {
	ensureDynamoPackage()
	dynamo := &TestingDynamo{
		Dynamo: nil,
	}
	dynamo.Start()
	return dynamo
}

func (this *TestingDynamo) running() bool {
	return this.process != nil
}

// Cleanup removes all tables from the current dynamo. Use
// this with a
// defer go_test_dynamo.Instance().Cleanup()
// at the top of your test methods
func (this *TestingDynamo) Cleanup() {
	if !this.running() {
		return
	}

	tables, err := this.Dynamo.ListTables(&dynamodb.ListTablesInput{})
	if err != nil {
		return
	}

	for _, tableName := range tables.TableNames {
		_, err := this.Dynamo.DeleteTable(&dynamodb.DeleteTableInput{
			TableName: tableName,
		})
		if err != nil {
			return
		}
	}
}

func getFreePort() (int, error) {
	// this function uses port 0 to have the OS assign an open port
	// which we use to start dynamo on to isolate each run
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return 0, err // hack instead of real error handling for now
	}
	defer safeClose(l)
	strAddr := l.Addr().String()
	parts := strings.Split(strAddr, ":")
	strPort := parts[len(parts)-1]
	port, err := strconv.ParseInt(strPort, 10, 32)
	if err != nil {
		return 0, err
	}
	return int(port), nil
}

// Shutdown kills the external process and waits for termination.
// Call this from your suite or from a per-package TestMain(m *testing.M) method
func (this *TestingDynamo) Shutdown() error {
	pid := this.process.Process.Pid
	err := this.process.Process.Kill()
	if err != nil {
		return err
	}
	for i := 0; i < 4; i++ {
		state, err := this.process.Process.Wait()
		if err != nil || state.Exited() {
			log.Printf("Shutdown DDB Local At %d\n", pid)
			this.process = nil
			return nil
		}
		time.Sleep(500 * time.Millisecond)
	}
	return fmt.Errorf("Failed to exit DDB Local At PID %d", pid)
}

// Start creates and starts the external process if needed.
// Generally you don't need to call this but it's exposed if
// you have a need.
func (this *TestingDynamo) Start() {
	if this.running() {
		return
	}
	port, err := getFreePort()
	if err != nil {
		log.Fatal("Error getting a free port")
		return
	}
	this.Port = port
	dynamoJar := fmt.Sprintf("-Djava.library.path=%s/DynamoDBLocal_lib", ddbdir())
	fmt.Fprintln(os.Stderr, "Loading dynamo from ", dynamoJar)
	this.process = exec.Command(
		"java",
		dynamoJar,
		"-jar",
		fmt.Sprintf("%s/DynamoDBLocal.jar", ddbdir()),
		"-port",
		strconv.Itoa(this.Port),
		"-inMemory")

	err = this.process.Start()
	if err != nil {
		log.Fatal("Error starting dynamo ", err, "... Did you install a java dev kit on the path?")
	}

	// Add a signal handler to allow termination

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL, syscall.SIGSTOP)

	go func() {
		<-sigs
		log.Print("Shutdown recieved, killing dynamo")
		err := this.Shutdown()
		if err != nil {
			log.Fatal("Error killing dynamo, ignoring", err)
		}
	}()

	// create and return the dynamo client
	this.Config = &aws.Config{
		Region:      aws.String("us-east-1"),
		Credentials: credentials.NewStaticCredentials("key", "secret;", ""),
	}
	realdynamo := dynamodb.New(session.New(), this.Config)
	this.Dynamo = realdynamo
	realdynamo.Endpoint = fmt.Sprintf("http://localhost:%d", this.Port)

	tables := &dynamodb.ListTablesInput{}

	success := false
	for i := 0; i < 30; i++ {
		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*500)
		defer cancel()
		_, err := this.Dynamo.ListTablesWithContext(ctx, tables)
		if err == nil {
			success = true
			break
		}
	}

	if !success {
		err := this.process.Process.Kill()
		if err != nil {
			log.Fatal("Error killing dynamo, ignoring", err)
		}
		log.Fatal("Timed out starting dynamo")
	}

	log.Printf("Local Dyanmo Started At PID: %d\n", this.process.Process.Pid)
}
