#!/bin/bash -e

manta -v || (echo "Build failed. Please create a .manta.json manta file. See the script for details." && exit 1)

docker build -t docker-registry.internal.justin.tv/commerce-hats:$GIT_COMMIT .
