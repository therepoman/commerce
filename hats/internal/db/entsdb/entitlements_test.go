package entsdb_test

import (
	"context"

	"time"

	entsdb "code.justin.tv/commerce/hats/internal/db/entsdb"
	"code.justin.tv/commerce/hats/test"
	"code.justin.tv/common/go_test_dynamo"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

/*
Tests for entitlements
NOTE: This is mainly used for simple tests around dynamo. Because HATS uses
functional / integration tests, we try to put all use cases up in functional tests
instead of down in unit tests.

Unit tests stay for edge cases that are difficult to test otherwise.
*/

var _ = Describe("Entitlements DAO With Dyamo", func() {
	var (
		edao                                 *entsdb.EntitlementsDAO
		dbapi                                dynamodbiface.DynamoDBAPI
		clock                                test.ITestingClock
		ownerID, productID, domainID, origin string
		ctx                                  context.Context
		start, end                           time.Time
		quantity                             int64
	)

	BeforeEach(func() {
		dbapi = go_test_dynamo.Instance().Dynamo
		_, err := dbapi.ListTables(&dynamodb.ListTablesInput{})
		Expect(err).To(BeNil())
		clock = test.NewTestingClock()
		edao, err = entsdb.NewEntitlementsDAO(dbapi)
		Expect(err).To(BeNil())
		Expect(edao.CreateTable()).To(BeNil())

		ctx = context.Background()

		ownerID = test.OwnerID()
		productID = test.ProductID()
		domainID = test.DomainID()
		origin = test.OriginID()
		start = clock.NowAdd(-time.Hour * 48)
		end = clock.NowAdd(time.Hour * 48)
		quantity = 5
	})

	Describe("with no entitlements", func() {
		It("Should have the table", func() {
			result, err := dbapi.ListTables(&dynamodb.ListTablesInput{})
			Expect(err).To(BeNil())
			Expect(result.TableNames).ToNot(BeEmpty())
			Expect(*result.TableNames[0]).To(BeEquivalentTo(entsdb.EntitlementsTableName))
		})

		It("Should not fetch an entitlement", func() {
			entitlements, err := edao.GetEntitlements(ctx, ownerID, &productID, &domainID, &origin)
			Expect(err).To(BeNil())
			Expect(entitlements).To(BeEmpty())
		})

		It("Should not find by PD", func() {
			entitlements, err := edao.ListEntitlementsByProductDomain(ctx, productID, domainID)
			Expect(err).To(BeNil())
			Expect(entitlements).To(BeEmpty())
		})
	})
	Describe("with direct entitlement", func() {
		var (
			actualEntitlement, expectedEntitlement *entsdb.Entitlement
			err                                    error
		)

		BeforeEach(func() {
			expectedEntitlement = &entsdb.Entitlement{
				OwnerID:   ownerID,
				ProductID: productID,
				DomainID:  domainID,
				Origin:    origin,
				Start:     &start,
				End:       &end,
				Quantity:  nil,
			}

			actualEntitlement, err = edao.CreateEntitlement(
				ctx,
				expectedEntitlement.OwnerID,
				expectedEntitlement.ProductID,
				expectedEntitlement.DomainID,
				expectedEntitlement.Origin,
				&start,
				&end,
				nil)
			Expect(err).To(BeNil())
			expectedEntitlement.ID = actualEntitlement.ID
		})

		It("Should get the entitlement", func() {
			entitlements, err := edao.GetEntitlements(ctx, ownerID, &productID, &domainID, &origin)
			Expect(err).To(BeNil())
			Expect(entitlements).ToNot(BeEmpty())
			ent := entitlements[0]
			Expect(ent).To(test.DAOEntitlementEqual(expectedEntitlement))
		})

		It("Should list entitlment by PD", func() {
			entitlements, err := edao.ListEntitlementsByProductDomain(ctx, productID, domainID)
			Expect(err).To(BeNil())
			Expect(entitlements).ToNot(BeEmpty())
			Expect(entitlements[0]).To(test.DAOEntitlementEqual(expectedEntitlement))
		})

		Describe("deleting entitlement", func() {
			BeforeEach(func() {
				err := edao.DeleteEntitlements(
					ctx,
					actualEntitlement.OwnerID,
					actualEntitlement.ProductID,
					actualEntitlement.DomainID,
					actualEntitlement.Origin)
				Expect(err).To(BeNil())
			})
			It("Should not get the entitlement", func() {
				entitlements, err := edao.GetEntitlements(ctx, ownerID, &productID, &domainID, &origin)
				Expect(err).To(BeNil())
				Expect(entitlements).To(BeEmpty())
			})
			It("Should not fail deleting deleted item", func() {
				err := edao.DeleteEntitlements(
					ctx,
					actualEntitlement.OwnerID,
					actualEntitlement.ProductID,
					actualEntitlement.DomainID,
					actualEntitlement.Origin)
				Expect(err).To(BeNil())
			})
		})
	})

	Describe("with direct entitlement with no dates", func() {
		It("Should create", func() {
			_, err := edao.CreateEntitlement(
				ctx,
				ownerID,
				productID,
				domainID,
				origin,
				nil,
				nil,
				nil)
			Expect(err).To(BeNil())
		})
	})

	AfterEach(func() {
		go_test_dynamo.Instance().Cleanup()
	})

	Describe("with direct entitlement with no dates", func() {
		It("Should create", func() {
			_, err := edao.CreateEntitlement(
				ctx,
				ownerID,
				productID,
				domainID,
				origin,
				nil,
				nil,
				nil)
			Expect(err).To(BeNil())
		})
	})

	AfterEach(func() {
		go_test_dynamo.Instance().Cleanup()
	})

	Describe("with consumable entitlement with quantity", func() {
		var (
			actualEntitlement, expectedEntitlement *entsdb.Entitlement
			remainingQuantity                      int64
			notExistingID                          string
			err                                    error
		)

		BeforeEach(func() {
			expectedEntitlement = &entsdb.Entitlement{
				OwnerID:   ownerID,
				ProductID: productID,
				DomainID:  domainID,
				Origin:    origin,
				Start:     &start,
				End:       &end,
				Quantity:  &quantity,
			}

			actualEntitlement, err = edao.CreateEntitlement(
				ctx,
				expectedEntitlement.OwnerID,
				expectedEntitlement.ProductID,
				expectedEntitlement.DomainID,
				expectedEntitlement.Origin,
				&start,
				&end,
				&quantity)
			Expect(err).To(BeNil())
			expectedEntitlement.ID = actualEntitlement.ID
			remainingQuantity = quantity
			notExistingID = "some ID"
		})

		It("Should get the entitlement", func() {
			entitlements, err := edao.GetEntitlements(ctx, ownerID, &productID, &domainID, &origin)
			Expect(err).To(BeNil())
			Expect(entitlements).ToNot(BeEmpty())
			ent := entitlements[0]
			Expect(ent).To(test.DAOEntitlementEqual(expectedEntitlement))
			Expect(ent.Quantity).To(test.QuantityEq(expectedEntitlement.Quantity))
		})

		It("Should consume part of the entitlement", func() {
			By("Not failing on consumption")
			consumingQuantity := int64(3)
			consumedQuantity, err := edao.ConsumeEntitlementForQuantity(ctx, ownerID, expectedEntitlement.ID, consumingQuantity)
			Expect(err).To(BeNil())
			Expect(&consumedQuantity).To(test.QuantityEq(&consumingQuantity))
			remainingQuantity = remainingQuantity - consumedQuantity

			By("Retrieving updated quantity")
			entitlements, err := edao.GetEntitlements(ctx, ownerID, &productID, &domainID, &origin)
			Expect(err).To(BeNil())
			Expect(entitlements).ToNot(BeEmpty())
			ent := entitlements[0]
			Expect(ent).To(test.DAOEntitlementEqual(expectedEntitlement))
			Expect(ent.Quantity).To(test.QuantityEq(&remainingQuantity))
		})

		It("Shouldn't overconsume the entitlement", func() {
			By("Not consuming quantity")
			consumingQuantity := quantity + 1
			consumedQuantity, err := edao.ConsumeEntitlementForQuantity(ctx, ownerID, expectedEntitlement.ID, consumingQuantity)
			Expect(err).To(BeNil())
			expectedConsumption := int64(0)
			Expect(&consumedQuantity).To(test.QuantityEq(&expectedConsumption))

			By("Should get the entitlement with updated quantity")
			entitlements, err := edao.GetEntitlements(ctx, ownerID, &productID, &domainID, &origin)
			Expect(err).To(BeNil())
			Expect(entitlements).ToNot(BeEmpty())
			ent := entitlements[0]
			Expect(ent).To(test.DAOEntitlementEqual(expectedEntitlement))
			Expect(ent.Quantity).To(test.QuantityEq(&quantity))
		})

		It("Shouldn't consume from deleted item", func() {
			consumingQuantity := quantity + 1
			consumedQuantity, err := edao.ConsumeEntitlementForQuantity(ctx, ownerID, notExistingID, consumingQuantity)
			Expect(err).To(BeNil())
			expectedConsumption := int64(0)
			Expect(&consumedQuantity).To(test.QuantityEq(&expectedConsumption))
		})
	})

	AfterEach(func() {
		go_test_dynamo.Instance().Cleanup()
	})
})
