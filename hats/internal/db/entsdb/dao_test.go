package entsdb_test

import (
	"log"
	"os"
	"testing"

	"code.justin.tv/common/go_test_dynamo"
	"github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
)

// Setup dynamo to run before all of the tests in the dao package, remember to clear you tables
func TestMain(m *testing.M) {
	tddb := go_test_dynamo.Instance()
	code := m.Run()
	if tddb.Shutdown() != nil {
		log.Fatal("Error shutting down dynamo")
	}
	os.Exit(code)
}

func TestSuite(t *testing.T) {
	gomega.RegisterFailHandler(ginkgo.Fail)
	ginkgo.RunSpecs(t, "DAO Suite")
}
