package entsdb

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/chat/timing"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	ddb "github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

const (
	EntitlementsTableName = "entitlements"

	EntitlementsColumnOwnerid   = "o"
	EntitlementsColumnID        = "i"
	EntitlementsColumnProductID = "p"
	EntitlementsColumnDomainID  = "d"
	EntitlementsColumnOrigin    = "r"
	EntitlementsColumnPD        = "pd"
	EntitlementsColumnOPDR      = "opdr"
	EntitlementsColumnStart     = "s"
	EntitlementsColumnEnd       = "e"
	EntitlementsColumnQuantity  = "q"

	EntitlementsIDXPD   = "IDX-pd"
	EntitlementsIDXOPDR = "IDX-odpr"

	XactGroupName = "entsdb"
)

type EntitlementsDAOInterface interface {
	CreateTable() error
	GetEntitlements(ctx context.Context, ownerID string, productID, domainID, origin *string) ([]*Entitlement, error)
	ListEntitlementsByProductDomain(ctx context.Context, productID, domainID string) ([]*Entitlement, error)
	CreateEntitlement(ctx context.Context, owner, product, domain, origin string, start, end *time.Time, quantity *int64) (*Entitlement, error)
	ConsumeEntitlementForQuantity(ctx context.Context, owner, id string, quantity int64) (int64, error)
	DeleteEntitlement(ctx context.Context, ownerID, id string) (*Entitlement, error)
	DeleteEntitlements(ctx context.Context, ownerID, productID, domainID, originID string) error
}

type EntitlementsDAO struct {
	ddb dynamodbiface.DynamoDBAPI
}

func NewEntitlementsDAO(ddb dynamodbiface.DynamoDBAPI) (*EntitlementsDAO, error) {
	return &EntitlementsDAO{
		ddb: ddb,
	}, nil
}

type Entitlement struct {
	OwnerID string
	ID      string

	ProductID string
	DomainID  string
	Origin    string
	Start     *time.Time
	End       *time.Time

	Quantity *int64
}

func ProductDomainKey(productID, domainID string) string {
	return fmt.Sprintf("%s:%s", productID, domainID)
}

func OwnerProductDomainOriginKey(ownerID, productID, domainID, origin string) string {
	return fmt.Sprintf("%s:%s:%s:%s", ownerID, productID, domainID, origin)
}

func parseDate(s *string) *time.Time {
	if s == nil {
		return nil
	}
	if *s == "" {
		return nil
	}
	unix, err := strconv.ParseInt(*s, 10, 64)
	if err != nil {
		// NEED METRIC
		log.Error(err)
		return nil
	}
	ts := time.Unix(unix, 0)
	return &ts
}

func serializeDate(t *time.Time) *string {
	if t == nil {
		return nil
	}
	return aws.String(fmt.Sprintf("%d", t.Unix()))
}

func parseDateFromAttr(key string, data map[string]*ddb.AttributeValue) *time.Time {
	if data[key] == nil {
		return nil
	}
	return parseDate(data[key].S)
}

func serializeInt64(i *int64) *string {
	if i == nil {
		return nil
	}
	return aws.String(strconv.FormatInt(*i, 10))
}

func parseInt64(key string, data map[string]*ddb.AttributeValue) *int64 {
	if data[key] == nil {
		return nil
	}
	str := *data[key].N
	ivalue, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return nil
	}
	return &ivalue
}

func convertItemsToEntitlements(items []map[string]*ddb.AttributeValue, ments []*Entitlement) []*Entitlement {
	for _, item := range items {
		ments = append(ments, convertItemToEntitlement(item))
	}
	return ments
}

func convertItemToEntitlement(item map[string]*ddb.AttributeValue) *Entitlement {
	return &Entitlement{
		OwnerID:   *item[EntitlementsColumnOwnerid].S,
		Origin:    *item[EntitlementsColumnOrigin].S,
		ID:        *item[EntitlementsColumnID].S,
		ProductID: *item[EntitlementsColumnProductID].S,
		DomainID:  *item[EntitlementsColumnDomainID].S,
		Start:     parseDateFromAttr(EntitlementsColumnStart, item),
		End:       parseDateFromAttr(EntitlementsColumnEnd, item),
		Quantity:  parseInt64(EntitlementsColumnQuantity, item),
	}

}
func (e *EntitlementsDAO) startTiming(ctx context.Context, queryID string) *timing.SubXact {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(fmt.Sprintf("%s.%s", XactGroupName, queryID))
		sub.Start()
		return sub
	}
	return nil
}

func (e *EntitlementsDAO) endTiming(sub *timing.SubXact) {
	if sub != nil {
		sub.End()
	}
}

func (this *EntitlementsDAO) CreateTable() error {
	ctr := &ddb.CreateTableInput{
		TableName: aws.String(EntitlementsTableName),
		KeySchema: []*ddb.KeySchemaElement{
			{
				AttributeName: aws.String(EntitlementsColumnOwnerid),
				KeyType:       aws.String(ddb.KeyTypeHash),
			},
			{
				AttributeName: aws.String(EntitlementsColumnID),
				KeyType:       aws.String(ddb.KeyTypeRange),
			},
		},
		ProvisionedThroughput: &ddb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*ddb.AttributeDefinition{
			{
				AttributeName: aws.String(EntitlementsColumnOwnerid),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(EntitlementsColumnID),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(EntitlementsColumnPD),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(EntitlementsColumnOPDR),
				AttributeType: aws.String("S"),
			},
		},
		GlobalSecondaryIndexes: []*ddb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(EntitlementsIDXPD),
				KeySchema: []*ddb.KeySchemaElement{
					{
						AttributeName: aws.String(EntitlementsColumnPD),
						KeyType:       aws.String(ddb.KeyTypeHash),
					},
				},
				ProvisionedThroughput: &ddb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(10),
					WriteCapacityUnits: aws.Int64(10),
				},
				Projection: &ddb.Projection{
					ProjectionType: aws.String(ddb.ProjectionTypeAll),
				},
			},
			{
				IndexName: aws.String(EntitlementsIDXOPDR),
				KeySchema: []*ddb.KeySchemaElement{
					{
						AttributeName: aws.String(EntitlementsColumnOPDR),
						KeyType:       aws.String(ddb.KeyTypeHash),
					},
				},
				ProvisionedThroughput: &ddb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(10),
					WriteCapacityUnits: aws.Int64(10),
				},
				Projection: &ddb.Projection{
					ProjectionType: aws.String(ddb.ProjectionTypeInclude),
					NonKeyAttributes: []*string{
						aws.String(EntitlementsColumnOwnerid),
						aws.String(EntitlementsColumnID),
					},
				},
			},
		},
	}

	_, err := this.ddb.CreateTable(ctr)
	return err
}

func (this *EntitlementsDAO) GetEntitlements(ctx context.Context, ownerID string, productID, domainID, origin *string) ([]*Entitlement, error) {
	const queryID = "GetEntitlements"
	sub := this.startTiming(ctx, queryID)
	defer this.endTiming(sub)

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	ments := make([]*Entitlement, 0, 1024)

	query := &ddb.QueryInput{
		TableName: aws.String(EntitlementsTableName),

		KeyConditions: map[string]*ddb.Condition{
			EntitlementsColumnOwnerid: {
				AttributeValueList: []*ddb.AttributeValue{
					{
						S: aws.String(ownerID),
					},
				},
				ComparisonOperator: aws.String(ddb.ComparisonOperatorEq),
			},
		},
	}

	filters := make([]string, 0, 3)
	values := make([]*string, 0, 3)
	keys := make([]string, 0, 3)

	if origin != nil {
		filters = append(filters, fmt.Sprintf("%s = :%s", EntitlementsColumnOrigin, EntitlementsColumnOrigin))
		values = append(values, origin)
		keys = append(keys, EntitlementsColumnOrigin)
	}
	if productID != nil {
		filters = append(filters, fmt.Sprintf("%s = :%s", EntitlementsColumnProductID, EntitlementsColumnProductID))
		values = append(values, productID)
		keys = append(keys, EntitlementsColumnProductID)
	}
	if domainID != nil {
		filters = append(filters, fmt.Sprintf("%s = :%s", EntitlementsColumnDomainID, EntitlementsColumnDomainID))
		values = append(values, domainID)
		keys = append(keys, EntitlementsColumnDomainID)
	}

	if len(filters) > 0 {
		query.FilterExpression = aws.String(strings.Join(filters, " AND "))
		query.ExpressionAttributeValues = map[string]*ddb.AttributeValue{}
		for i := 0; i < len(keys); i++ {
			key := fmt.Sprintf(":%s", keys[i])
			query.ExpressionAttributeValues[key] = &ddb.AttributeValue{S: values[i]}
		}
	}

	results, err := this.ddb.QueryWithContext(ctx, query)
	ments = convertItemsToEntitlements(results.Items, ments)

	lastEvaluatedKey := results.LastEvaluatedKey
	// if lastEvaluatedKey is not empty, possible more entitlements in dynamodb to query until lastEvaluatedKey returns empty
	for lastEvaluatedKey != nil && len(lastEvaluatedKey) > 0 {
		query.ExclusiveStartKey = lastEvaluatedKey
		res, err := this.ddb.QueryWithContext(ctx, query)
		if err != nil {
			return ments, err
		}
		ments = convertItemsToEntitlements(res.Items, ments)
		lastEvaluatedKey = res.LastEvaluatedKey
	}

	return ments, err
}

func (this *EntitlementsDAO) ListEntitlementsByProductDomain(ctx context.Context, productID, domainID string) ([]*Entitlement, error) {
	const queryID = "ListEntitlementsByProductDomain"
	sub := this.startTiming(ctx, queryID)
	defer this.endTiming(sub)

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	entitlements := make([]*Entitlement, 0, 1024)

	epd := ProductDomainKey(productID, domainID)
	query := &ddb.QueryInput{
		TableName: aws.String(EntitlementsTableName),
		IndexName: aws.String(EntitlementsIDXPD),

		KeyConditions: map[string]*ddb.Condition{
			EntitlementsColumnPD: {
				AttributeValueList: []*ddb.AttributeValue{
					{
						S: aws.String(epd),
					},
				},
				ComparisonOperator: aws.String(ddb.ComparisonOperatorEq),
			},
		},
	}

	results, err := this.ddb.QueryWithContext(ctx, query)

	entitlements = convertItemsToEntitlements(results.Items, entitlements)

	return entitlements, err
}

func (this *EntitlementsDAO) CreateEntitlement(ctx context.Context, owner, product, domain, origin string, start, end *time.Time, quantity *int64) (*Entitlement, error) {
	const queryID = "CreateEntitlement"
	sub := this.startTiming(ctx, queryID)
	defer this.endTiming(sub)

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	entitlement := &Entitlement{
		ID:        uuid.NewV4().String(),
		OwnerID:   owner,
		ProductID: product,
		DomainID:  domain,
		Origin:    origin,
		Start:     start,
		End:       end,
		Quantity:  quantity,
	}

	put := &ddb.PutItemInput{
		TableName: aws.String(EntitlementsTableName),
		Item: map[string]*ddb.AttributeValue{
			EntitlementsColumnOwnerid: {
				S: aws.String(entitlement.OwnerID),
			},
			EntitlementsColumnOrigin: {
				S: aws.String(entitlement.Origin),
			},
			EntitlementsColumnID: {
				S: aws.String(entitlement.ID),
			},
			EntitlementsColumnProductID: {
				S: aws.String(entitlement.ProductID),
			},
			EntitlementsColumnDomainID: {
				S: aws.String(entitlement.DomainID),
			},
			EntitlementsColumnPD: {
				S: aws.String(ProductDomainKey(product, domain)),
			},
			EntitlementsColumnOPDR: {
				S: aws.String(OwnerProductDomainOriginKey(owner, product, domain, origin)),
			},
		},
	}

	if start != nil {
		put.Item[EntitlementsColumnStart] = &ddb.AttributeValue{
			S: serializeDate(start),
		}
	}
	if end != nil {
		put.Item[EntitlementsColumnEnd] = &ddb.AttributeValue{
			S: serializeDate(end),
		}
	}

	if entitlement.Quantity != nil {
		put.Item[EntitlementsColumnQuantity] = &ddb.AttributeValue{
			N: serializeInt64(entitlement.Quantity),
		}
	}

	_, err := this.ddb.PutItemWithContext(ctx, put)
	if err != nil {
		return nil, err
	}
	return entitlement, err
}

func (this *EntitlementsDAO) ConsumeEntitlementForQuantity(ctx context.Context, owner, id string, quantity int64) (int64, error) {
	const queryID = "ConsumeEntitlement"
	sub := this.startTiming(ctx, queryID)
	defer this.endTiming(sub)

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	update := &ddb.UpdateItemInput{
		TableName: aws.String(EntitlementsTableName),
		Key: map[string]*ddb.AttributeValue{
			EntitlementsColumnOwnerid: {
				S: aws.String(owner),
			},
			EntitlementsColumnID: {
				S: aws.String(id),
			},
		},
		UpdateExpression:    aws.String("SET #quantity = #quantity - :quantity"),
		ConditionExpression: aws.String("#quantity >= :quantity"),
		ExpressionAttributeNames: map[string]*string{
			"#quantity": aws.String(EntitlementsColumnQuantity),
		},
		ExpressionAttributeValues: map[string]*ddb.AttributeValue{
			":quantity": {
				N: serializeInt64(&quantity),
			},
		},
	}
	_, err := this.ddb.UpdateItemWithContext(ctx, update)
	if err != nil {
		if awsError, ok := err.(awserr.Error); ok {
			// If the item was updated with lower quantity or even deleted, don't fail
			if awsError.Code() == ddb.ErrCodeConditionalCheckFailedException {
				return 0, nil
			}
		}
		return 0, err
	}

	return quantity, nil
}

func (this *EntitlementsDAO) DeleteEntitlement(ctx context.Context, ownerID, id string) (*Entitlement, error) {
	delete := &ddb.DeleteItemInput{
		TableName: aws.String(EntitlementsTableName),
		Key: map[string]*ddb.AttributeValue{
			EntitlementsColumnOwnerid: {
				S: aws.String(ownerID),
			},
			EntitlementsColumnID: {
				S: aws.String(id),
			},
		},
		ReturnValues: aws.String("ALL_OLD"),
	}
	deleteOutput, err := this.ddb.DeleteItemWithContext(ctx, delete)
	if err != nil {
		return nil, err
	}

	// If the item was already deleted, don't fail
	if deleteOutput == nil || deleteOutput.Attributes == nil {
		return nil, nil
	}

	deleted := convertItemToEntitlement(deleteOutput.Attributes)

	return deleted, nil
}

func (this *EntitlementsDAO) DeleteEntitlements(ctx context.Context, ownerID, productID, domainID, originID string) error {
	const queryID = "DeleteEntitlement"
	sub := this.startTiming(ctx, queryID)
	defer this.endTiming(sub)

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	query := &ddb.QueryInput{
		TableName: aws.String(EntitlementsTableName),
		IndexName: aws.String(EntitlementsIDXOPDR),

		KeyConditions: map[string]*ddb.Condition{
			EntitlementsColumnOPDR: {
				AttributeValueList: []*ddb.AttributeValue{
					{
						S: aws.String(OwnerProductDomainOriginKey(ownerID, productID, domainID, originID)),
					},
				},
				ComparisonOperator: aws.String(ddb.ComparisonOperatorEq),
			},
		},
	}

	results, err := this.ddb.Query(query)

	for _, result := range results.Items {
		// NEED Delete Count Metric
		// NEED Robustness
		ownid := *result[EntitlementsColumnOwnerid].S
		id := *result[EntitlementsColumnID].S
		_, err = this.DeleteEntitlement(ctx, ownid, id)
		if err != nil {
			return err
		}

	}
	return nil
}
