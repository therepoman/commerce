// Code generated by counterfeiter. DO NOT EDIT.
package mocks

import (
	"context"
	"sync"
	"time"

	"code.justin.tv/commerce/hats/internal/db/entsdb"
)

type FakeEntitlementsDAOInterface struct {
	CreateTableStub        func() error
	createTableMutex       sync.RWMutex
	createTableArgsForCall []struct{}
	createTableReturns     struct {
		result1 error
	}
	createTableReturnsOnCall map[int]struct {
		result1 error
	}
	GetEntitlementsStub        func(ctx context.Context, ownerID string, productID, domainID, origin *string) ([]*entsdb.Entitlement, error)
	getEntitlementsMutex       sync.RWMutex
	getEntitlementsArgsForCall []struct {
		ctx       context.Context
		ownerID   string
		productID *string
		domainID  *string
		origin    *string
	}
	getEntitlementsReturns struct {
		result1 []*entsdb.Entitlement
		result2 error
	}
	getEntitlementsReturnsOnCall map[int]struct {
		result1 []*entsdb.Entitlement
		result2 error
	}
	ListEntitlementsByProductDomainStub        func(ctx context.Context, productID, domainID string) ([]*entsdb.Entitlement, error)
	listEntitlementsByProductDomainMutex       sync.RWMutex
	listEntitlementsByProductDomainArgsForCall []struct {
		ctx       context.Context
		productID string
		domainID  string
	}
	listEntitlementsByProductDomainReturns struct {
		result1 []*entsdb.Entitlement
		result2 error
	}
	listEntitlementsByProductDomainReturnsOnCall map[int]struct {
		result1 []*entsdb.Entitlement
		result2 error
	}
	CreateEntitlementStub        func(ctx context.Context, owner, product, domain, origin string, start, end *time.Time, quantity *int64) (*entsdb.Entitlement, error)
	createEntitlementMutex       sync.RWMutex
	createEntitlementArgsForCall []struct {
		ctx      context.Context
		owner    string
		product  string
		domain   string
		origin   string
		start    *time.Time
		end      *time.Time
		quantity *int64
	}
	createEntitlementReturns struct {
		result1 *entsdb.Entitlement
		result2 error
	}
	createEntitlementReturnsOnCall map[int]struct {
		result1 *entsdb.Entitlement
		result2 error
	}
	ConsumeEntitlementForQuantityStub        func(ctx context.Context, owner, id string, quantity int64) (int64, error)
	consumeEntitlementForQuantityMutex       sync.RWMutex
	consumeEntitlementForQuantityArgsForCall []struct {
		ctx      context.Context
		owner    string
		id       string
		quantity int64
	}
	consumeEntitlementForQuantityReturns struct {
		result1 int64
		result2 error
	}
	consumeEntitlementForQuantityReturnsOnCall map[int]struct {
		result1 int64
		result2 error
	}
	DeleteEntitlementStub        func(ctx context.Context, ownerID, id string) (*entsdb.Entitlement, error)
	deleteEntitlementMutex       sync.RWMutex
	deleteEntitlementArgsForCall []struct {
		ctx     context.Context
		ownerID string
		id      string
	}
	deleteEntitlementReturns struct {
		result1 *entsdb.Entitlement
		result2 error
	}
	deleteEntitlementReturnsOnCall map[int]struct {
		result1 *entsdb.Entitlement
		result2 error
	}
	DeleteEntitlementsStub        func(ctx context.Context, ownerID, productID, domainID, originID string) error
	deleteEntitlementsMutex       sync.RWMutex
	deleteEntitlementsArgsForCall []struct {
		ctx       context.Context
		ownerID   string
		productID string
		domainID  string
		originID  string
	}
	deleteEntitlementsReturns struct {
		result1 error
	}
	deleteEntitlementsReturnsOnCall map[int]struct {
		result1 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeEntitlementsDAOInterface) CreateTable() error {
	fake.createTableMutex.Lock()
	ret, specificReturn := fake.createTableReturnsOnCall[len(fake.createTableArgsForCall)]
	fake.createTableArgsForCall = append(fake.createTableArgsForCall, struct{}{})
	fake.recordInvocation("CreateTable", []interface{}{})
	fake.createTableMutex.Unlock()
	if fake.CreateTableStub != nil {
		return fake.CreateTableStub()
	}
	if specificReturn {
		return ret.result1
	}
	return fake.createTableReturns.result1
}

func (fake *FakeEntitlementsDAOInterface) CreateTableCallCount() int {
	fake.createTableMutex.RLock()
	defer fake.createTableMutex.RUnlock()
	return len(fake.createTableArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) CreateTableReturns(result1 error) {
	fake.CreateTableStub = nil
	fake.createTableReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeEntitlementsDAOInterface) CreateTableReturnsOnCall(i int, result1 error) {
	fake.CreateTableStub = nil
	if fake.createTableReturnsOnCall == nil {
		fake.createTableReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.createTableReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeEntitlementsDAOInterface) GetEntitlements(ctx context.Context, ownerID string, productID *string, domainID *string, origin *string) ([]*entsdb.Entitlement, error) {
	fake.getEntitlementsMutex.Lock()
	ret, specificReturn := fake.getEntitlementsReturnsOnCall[len(fake.getEntitlementsArgsForCall)]
	fake.getEntitlementsArgsForCall = append(fake.getEntitlementsArgsForCall, struct {
		ctx       context.Context
		ownerID   string
		productID *string
		domainID  *string
		origin    *string
	}{ctx, ownerID, productID, domainID, origin})
	fake.recordInvocation("GetEntitlements", []interface{}{ctx, ownerID, productID, domainID, origin})
	fake.getEntitlementsMutex.Unlock()
	if fake.GetEntitlementsStub != nil {
		return fake.GetEntitlementsStub(ctx, ownerID, productID, domainID, origin)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fake.getEntitlementsReturns.result1, fake.getEntitlementsReturns.result2
}

func (fake *FakeEntitlementsDAOInterface) GetEntitlementsCallCount() int {
	fake.getEntitlementsMutex.RLock()
	defer fake.getEntitlementsMutex.RUnlock()
	return len(fake.getEntitlementsArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) GetEntitlementsArgsForCall(i int) (context.Context, string, *string, *string, *string) {
	fake.getEntitlementsMutex.RLock()
	defer fake.getEntitlementsMutex.RUnlock()
	return fake.getEntitlementsArgsForCall[i].ctx, fake.getEntitlementsArgsForCall[i].ownerID, fake.getEntitlementsArgsForCall[i].productID, fake.getEntitlementsArgsForCall[i].domainID, fake.getEntitlementsArgsForCall[i].origin
}

func (fake *FakeEntitlementsDAOInterface) GetEntitlementsReturns(result1 []*entsdb.Entitlement, result2 error) {
	fake.GetEntitlementsStub = nil
	fake.getEntitlementsReturns = struct {
		result1 []*entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) GetEntitlementsReturnsOnCall(i int, result1 []*entsdb.Entitlement, result2 error) {
	fake.GetEntitlementsStub = nil
	if fake.getEntitlementsReturnsOnCall == nil {
		fake.getEntitlementsReturnsOnCall = make(map[int]struct {
			result1 []*entsdb.Entitlement
			result2 error
		})
	}
	fake.getEntitlementsReturnsOnCall[i] = struct {
		result1 []*entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) ListEntitlementsByProductDomain(ctx context.Context, productID string, domainID string) ([]*entsdb.Entitlement, error) {
	fake.listEntitlementsByProductDomainMutex.Lock()
	ret, specificReturn := fake.listEntitlementsByProductDomainReturnsOnCall[len(fake.listEntitlementsByProductDomainArgsForCall)]
	fake.listEntitlementsByProductDomainArgsForCall = append(fake.listEntitlementsByProductDomainArgsForCall, struct {
		ctx       context.Context
		productID string
		domainID  string
	}{ctx, productID, domainID})
	fake.recordInvocation("ListEntitlementsByProductDomain", []interface{}{ctx, productID, domainID})
	fake.listEntitlementsByProductDomainMutex.Unlock()
	if fake.ListEntitlementsByProductDomainStub != nil {
		return fake.ListEntitlementsByProductDomainStub(ctx, productID, domainID)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fake.listEntitlementsByProductDomainReturns.result1, fake.listEntitlementsByProductDomainReturns.result2
}

func (fake *FakeEntitlementsDAOInterface) ListEntitlementsByProductDomainCallCount() int {
	fake.listEntitlementsByProductDomainMutex.RLock()
	defer fake.listEntitlementsByProductDomainMutex.RUnlock()
	return len(fake.listEntitlementsByProductDomainArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) ListEntitlementsByProductDomainArgsForCall(i int) (context.Context, string, string) {
	fake.listEntitlementsByProductDomainMutex.RLock()
	defer fake.listEntitlementsByProductDomainMutex.RUnlock()
	return fake.listEntitlementsByProductDomainArgsForCall[i].ctx, fake.listEntitlementsByProductDomainArgsForCall[i].productID, fake.listEntitlementsByProductDomainArgsForCall[i].domainID
}

func (fake *FakeEntitlementsDAOInterface) ListEntitlementsByProductDomainReturns(result1 []*entsdb.Entitlement, result2 error) {
	fake.ListEntitlementsByProductDomainStub = nil
	fake.listEntitlementsByProductDomainReturns = struct {
		result1 []*entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) ListEntitlementsByProductDomainReturnsOnCall(i int, result1 []*entsdb.Entitlement, result2 error) {
	fake.ListEntitlementsByProductDomainStub = nil
	if fake.listEntitlementsByProductDomainReturnsOnCall == nil {
		fake.listEntitlementsByProductDomainReturnsOnCall = make(map[int]struct {
			result1 []*entsdb.Entitlement
			result2 error
		})
	}
	fake.listEntitlementsByProductDomainReturnsOnCall[i] = struct {
		result1 []*entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) CreateEntitlement(ctx context.Context, owner string, product string, domain string, origin string, start *time.Time, end *time.Time, quantity *int64) (*entsdb.Entitlement, error) {
	fake.createEntitlementMutex.Lock()
	ret, specificReturn := fake.createEntitlementReturnsOnCall[len(fake.createEntitlementArgsForCall)]
	fake.createEntitlementArgsForCall = append(fake.createEntitlementArgsForCall, struct {
		ctx      context.Context
		owner    string
		product  string
		domain   string
		origin   string
		start    *time.Time
		end      *time.Time
		quantity *int64
	}{ctx, owner, product, domain, origin, start, end, quantity})
	fake.recordInvocation("CreateEntitlement", []interface{}{ctx, owner, product, domain, origin, start, end, quantity})
	fake.createEntitlementMutex.Unlock()
	if fake.CreateEntitlementStub != nil {
		return fake.CreateEntitlementStub(ctx, owner, product, domain, origin, start, end, quantity)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fake.createEntitlementReturns.result1, fake.createEntitlementReturns.result2
}

func (fake *FakeEntitlementsDAOInterface) CreateEntitlementCallCount() int {
	fake.createEntitlementMutex.RLock()
	defer fake.createEntitlementMutex.RUnlock()
	return len(fake.createEntitlementArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) CreateEntitlementArgsForCall(i int) (context.Context, string, string, string, string, *time.Time, *time.Time, *int64) {
	fake.createEntitlementMutex.RLock()
	defer fake.createEntitlementMutex.RUnlock()
	return fake.createEntitlementArgsForCall[i].ctx, fake.createEntitlementArgsForCall[i].owner, fake.createEntitlementArgsForCall[i].product, fake.createEntitlementArgsForCall[i].domain, fake.createEntitlementArgsForCall[i].origin, fake.createEntitlementArgsForCall[i].start, fake.createEntitlementArgsForCall[i].end, fake.createEntitlementArgsForCall[i].quantity
}

func (fake *FakeEntitlementsDAOInterface) CreateEntitlementReturns(result1 *entsdb.Entitlement, result2 error) {
	fake.CreateEntitlementStub = nil
	fake.createEntitlementReturns = struct {
		result1 *entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) CreateEntitlementReturnsOnCall(i int, result1 *entsdb.Entitlement, result2 error) {
	fake.CreateEntitlementStub = nil
	if fake.createEntitlementReturnsOnCall == nil {
		fake.createEntitlementReturnsOnCall = make(map[int]struct {
			result1 *entsdb.Entitlement
			result2 error
		})
	}
	fake.createEntitlementReturnsOnCall[i] = struct {
		result1 *entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) ConsumeEntitlementForQuantity(ctx context.Context, owner string, id string, quantity int64) (int64, error) {
	fake.consumeEntitlementForQuantityMutex.Lock()
	ret, specificReturn := fake.consumeEntitlementForQuantityReturnsOnCall[len(fake.consumeEntitlementForQuantityArgsForCall)]
	fake.consumeEntitlementForQuantityArgsForCall = append(fake.consumeEntitlementForQuantityArgsForCall, struct {
		ctx      context.Context
		owner    string
		id       string
		quantity int64
	}{ctx, owner, id, quantity})
	fake.recordInvocation("ConsumeEntitlementForQuantity", []interface{}{ctx, owner, id, quantity})
	fake.consumeEntitlementForQuantityMutex.Unlock()
	if fake.ConsumeEntitlementForQuantityStub != nil {
		return fake.ConsumeEntitlementForQuantityStub(ctx, owner, id, quantity)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fake.consumeEntitlementForQuantityReturns.result1, fake.consumeEntitlementForQuantityReturns.result2
}

func (fake *FakeEntitlementsDAOInterface) ConsumeEntitlementForQuantityCallCount() int {
	fake.consumeEntitlementForQuantityMutex.RLock()
	defer fake.consumeEntitlementForQuantityMutex.RUnlock()
	return len(fake.consumeEntitlementForQuantityArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) ConsumeEntitlementForQuantityArgsForCall(i int) (context.Context, string, string, int64) {
	fake.consumeEntitlementForQuantityMutex.RLock()
	defer fake.consumeEntitlementForQuantityMutex.RUnlock()
	return fake.consumeEntitlementForQuantityArgsForCall[i].ctx, fake.consumeEntitlementForQuantityArgsForCall[i].owner, fake.consumeEntitlementForQuantityArgsForCall[i].id, fake.consumeEntitlementForQuantityArgsForCall[i].quantity
}

func (fake *FakeEntitlementsDAOInterface) ConsumeEntitlementForQuantityReturns(result1 int64, result2 error) {
	fake.ConsumeEntitlementForQuantityStub = nil
	fake.consumeEntitlementForQuantityReturns = struct {
		result1 int64
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) ConsumeEntitlementForQuantityReturnsOnCall(i int, result1 int64, result2 error) {
	fake.ConsumeEntitlementForQuantityStub = nil
	if fake.consumeEntitlementForQuantityReturnsOnCall == nil {
		fake.consumeEntitlementForQuantityReturnsOnCall = make(map[int]struct {
			result1 int64
			result2 error
		})
	}
	fake.consumeEntitlementForQuantityReturnsOnCall[i] = struct {
		result1 int64
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlement(ctx context.Context, ownerID string, id string) (*entsdb.Entitlement, error) {
	fake.deleteEntitlementMutex.Lock()
	ret, specificReturn := fake.deleteEntitlementReturnsOnCall[len(fake.deleteEntitlementArgsForCall)]
	fake.deleteEntitlementArgsForCall = append(fake.deleteEntitlementArgsForCall, struct {
		ctx     context.Context
		ownerID string
		id      string
	}{ctx, ownerID, id})
	fake.recordInvocation("DeleteEntitlement", []interface{}{ctx, ownerID, id})
	fake.deleteEntitlementMutex.Unlock()
	if fake.DeleteEntitlementStub != nil {
		return fake.DeleteEntitlementStub(ctx, ownerID, id)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fake.deleteEntitlementReturns.result1, fake.deleteEntitlementReturns.result2
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementCallCount() int {
	fake.deleteEntitlementMutex.RLock()
	defer fake.deleteEntitlementMutex.RUnlock()
	return len(fake.deleteEntitlementArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementArgsForCall(i int) (context.Context, string, string) {
	fake.deleteEntitlementMutex.RLock()
	defer fake.deleteEntitlementMutex.RUnlock()
	return fake.deleteEntitlementArgsForCall[i].ctx, fake.deleteEntitlementArgsForCall[i].ownerID, fake.deleteEntitlementArgsForCall[i].id
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementReturns(result1 *entsdb.Entitlement, result2 error) {
	fake.DeleteEntitlementStub = nil
	fake.deleteEntitlementReturns = struct {
		result1 *entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementReturnsOnCall(i int, result1 *entsdb.Entitlement, result2 error) {
	fake.DeleteEntitlementStub = nil
	if fake.deleteEntitlementReturnsOnCall == nil {
		fake.deleteEntitlementReturnsOnCall = make(map[int]struct {
			result1 *entsdb.Entitlement
			result2 error
		})
	}
	fake.deleteEntitlementReturnsOnCall[i] = struct {
		result1 *entsdb.Entitlement
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlements(ctx context.Context, ownerID string, productID string, domainID string, originID string) error {
	fake.deleteEntitlementsMutex.Lock()
	ret, specificReturn := fake.deleteEntitlementsReturnsOnCall[len(fake.deleteEntitlementsArgsForCall)]
	fake.deleteEntitlementsArgsForCall = append(fake.deleteEntitlementsArgsForCall, struct {
		ctx       context.Context
		ownerID   string
		productID string
		domainID  string
		originID  string
	}{ctx, ownerID, productID, domainID, originID})
	fake.recordInvocation("DeleteEntitlements", []interface{}{ctx, ownerID, productID, domainID, originID})
	fake.deleteEntitlementsMutex.Unlock()
	if fake.DeleteEntitlementsStub != nil {
		return fake.DeleteEntitlementsStub(ctx, ownerID, productID, domainID, originID)
	}
	if specificReturn {
		return ret.result1
	}
	return fake.deleteEntitlementsReturns.result1
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementsCallCount() int {
	fake.deleteEntitlementsMutex.RLock()
	defer fake.deleteEntitlementsMutex.RUnlock()
	return len(fake.deleteEntitlementsArgsForCall)
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementsArgsForCall(i int) (context.Context, string, string, string, string) {
	fake.deleteEntitlementsMutex.RLock()
	defer fake.deleteEntitlementsMutex.RUnlock()
	return fake.deleteEntitlementsArgsForCall[i].ctx, fake.deleteEntitlementsArgsForCall[i].ownerID, fake.deleteEntitlementsArgsForCall[i].productID, fake.deleteEntitlementsArgsForCall[i].domainID, fake.deleteEntitlementsArgsForCall[i].originID
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementsReturns(result1 error) {
	fake.DeleteEntitlementsStub = nil
	fake.deleteEntitlementsReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeEntitlementsDAOInterface) DeleteEntitlementsReturnsOnCall(i int, result1 error) {
	fake.DeleteEntitlementsStub = nil
	if fake.deleteEntitlementsReturnsOnCall == nil {
		fake.deleteEntitlementsReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.deleteEntitlementsReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeEntitlementsDAOInterface) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.createTableMutex.RLock()
	defer fake.createTableMutex.RUnlock()
	fake.getEntitlementsMutex.RLock()
	defer fake.getEntitlementsMutex.RUnlock()
	fake.listEntitlementsByProductDomainMutex.RLock()
	defer fake.listEntitlementsByProductDomainMutex.RUnlock()
	fake.createEntitlementMutex.RLock()
	defer fake.createEntitlementMutex.RUnlock()
	fake.consumeEntitlementForQuantityMutex.RLock()
	defer fake.consumeEntitlementForQuantityMutex.RUnlock()
	fake.deleteEntitlementMutex.RLock()
	defer fake.deleteEntitlementMutex.RUnlock()
	fake.deleteEntitlementsMutex.RLock()
	defer fake.deleteEntitlementsMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeEntitlementsDAOInterface) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ entsdb.EntitlementsDAOInterface = new(FakeEntitlementsDAOInterface)
