package twents

import (
	"context"

	"code.justin.tv/commerce/hats/internal/app/ents"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/common/twirp"
)

func (api *EntitlementsAPI) GetEntitlements(ctx context.Context, request *hats_api.GetEntitlementsRequest) (result *hats_api.GetEntitlementsResponse, err error) {
	returnEnts, err := api.Entitlements.GetEntitlements(ctx, convertEntitlementKeysToApp(request.GetKeys()))
	if err != nil {
		switch err {
		case ents.ErrInvalidProductId:
			return nil, hats_api.NewClientError(
				twirp.NewError(twirp.InvalidArgument, "Expecting positive quantity number"),
				hats_api.ErrCodeInvalidProductId)
		default:
			return nil, err
		}
	}
	return &hats_api.GetEntitlementsResponse{
		Entitlements: convertEntitlementsFromApp(returnEnts),
	}, nil
}
