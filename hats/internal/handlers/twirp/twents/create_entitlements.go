package twents

import (
	"context"

	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
)

func (api *EntitlementsAPI) CreateEntitlement(ctx context.Context, request *hats_api.CreateEntitlementRequest) (*hats_api.CreateEntitlementResponse, error) {
	createdEnts, err := api.Entitlements.CreateEntitlements(ctx, convertEntitlementsToApp(request.GetEntitlements()))
	if err != nil {
		return nil, err
	}
	return &hats_api.CreateEntitlementResponse{
		Entitlements: convertEntitlementsFromApp(createdEnts),
	}, nil
}
