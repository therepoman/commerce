package twents

import (
	"context"

	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
)

func (api *EntitlementsAPI) ListEntitlementsByProductDomain(ctx context.Context, request *hats_api.ListEntitlementsByProductDomainRequest) (*hats_api.ListEntitlementsByProductDomainResponse, error) {
	ents, err := api.Entitlements.ListEntitlementsByProductDomain(ctx, request.ProductId, request.DomainId)
	if err != nil {
		return nil, err
	}
	return &hats_api.ListEntitlementsByProductDomainResponse{
		Entitlements: convertEntitlementsFromApp(ents),
	}, nil
}
