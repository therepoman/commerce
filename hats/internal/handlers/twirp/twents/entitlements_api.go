package twents

import (
	"net/http"
	"time"

	"code.justin.tv/commerce/hats/internal/app"
	"code.justin.tv/commerce/hats/internal/app/ents"
	"code.justin.tv/commerce/hats/internal/clients"
	"code.justin.tv/commerce/hats/pkg/twirphooks"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/common/twirp"
	google_protobuf "github.com/golang/protobuf/ptypes/timestamp"
)

const (
	DomainSubOnlyChat = "subOnlyChat"
	DomainSubs        = "subs"
	DomainTurbo       = "turbo"
	ProductTurbo      = "turbo"
	DomainPrime       = "prime"
	ProductPrime      = "prime"
)

type EntitlementsAPI struct {
	Entitlements ents.IEntitlements
}

func NewEntitlementsAPIHandler(c *clients.Clients, a *app.App) http.Handler {
	entitlementsAPI := &EntitlementsAPI{
		Entitlements: a.Entitlements,
	}

	hooks := twirp.ChainHooks(twirphooks.XactHooks(c.Statter), twirphooks.ErrorHooks(c.Logger))
	return hats_api.NewEntitlementsServer(entitlementsAPI, hooks, nil)
}

func convertEntitlementToApp(ent *hats_api.Entitlement) *ents.Entitlement {
	return &ents.Entitlement{
		ID:        ent.GetId(),
		DomainID:  ent.GetDomain(),
		Origin:    ent.GetOrigin(),
		OwnerID:   ent.GetOwner(),
		ProductID: ent.GetProductId(),
		Start:     TimeFromProtobufTime(ent.GetStart()),
		End:       TimeFromProtobufTime(ent.GetEnd()),
		Quantity:  Iptr(ent.GetQuantity()),
	}
}

func convertEntitlementsToApp(entitlements []*hats_api.Entitlement) []*ents.Entitlement {
	convertedEnts := make([]*ents.Entitlement, len(entitlements))
	for i, ent := range entitlements {
		convertedEnts[i] = convertEntitlementToApp(ent)
	}
	return convertedEnts
}

func converEntitlementKeyToApp(key *hats_api.EntitlementKey) *ents.EntitlementKey {
	return &ents.EntitlementKey{
		DomainID:  sptr(key.GetDomain()),
		OriginID:  sptr(key.GetOrigin()),
		OwnerID:   key.GetOwnerId(),
		ProductID: sptr(key.GetProductId()),
	}
}

func convertEntitlementKeysToApp(keys []*hats_api.EntitlementKey) []*ents.EntitlementKey {
	convertedKeys := make([]*ents.EntitlementKey, len(keys))
	for i, key := range keys {
		convertedKeys[i] = converEntitlementKeyToApp(key)
	}
	return convertedKeys
}

func convertEntitlementFromApp(ent *ents.Entitlement) *hats_api.Entitlement {
	return &hats_api.Entitlement{
		Id:        ent.ID,
		Domain:    ent.DomainID,
		Origin:    ent.Origin,
		Owner:     ent.OwnerID,
		ProductId: ent.ProductID,
		Start:     TimeToProtobufTime(ent.Start),
		End:       TimeToProtobufTime(ent.End),
		Quantity:  Ivalue(ent.Quantity),
	}
}

func convertEntitlementsFromApp(entitlements []*ents.Entitlement) []*hats_api.Entitlement {
	convertedEnts := make([]*hats_api.Entitlement, len(entitlements))
	for i, ent := range entitlements {
		convertedEnts[i] = convertEntitlementFromApp(ent)
	}
	return convertedEnts
}

func TimeFromProtobufTime(pbts *hats_api.TimestampValue) *time.Time {
	if pbts == nil {
		return nil
	}
	ts := time.Unix(pbts.Value.Seconds, 0)
	return &ts
}

func TimeToProtobufTime(t *time.Time) *hats_api.TimestampValue {
	if t == nil {
		return nil
	}
	return &hats_api.TimestampValue{
		Value: &google_protobuf.Timestamp{
			Seconds: t.Unix(),
			Nanos:   0,
		},
	}
}

func convertEntitlementOPDToAppParams(consume *hats_api.EntitlementOPD) *ents.ConsumeEntitlementParams {
	return &ents.ConsumeEntitlementParams{
		OwnerID:   consume.GetOwnerId(),
		ProductID: consume.GetProductId(),
		DomainID:  consume.GetDomain(),
	}
}

func convertEntitlementsOPDOToAppParams(deletes []*hats_api.EntitlementOPDO) []*ents.DeleteEntitlementParams {
	params := make([]*ents.DeleteEntitlementParams, len(deletes))
	for i, delete := range deletes {
		params[i] = convertEntitlementOPDOToAppParams(delete)
	}
	return params
}

func convertEntitlementOPDOToAppParams(delete *hats_api.EntitlementOPDO) *ents.DeleteEntitlementParams {
	return &ents.DeleteEntitlementParams{
		DomainID:  delete.GetDomain(),
		OriginID:  delete.GetOrigin(),
		OwnerID:   delete.GetOwnerId(),
		ProductID: delete.GetProductId(),
	}
}

func convertConsumeStrategy(strategy hats_api.ConsumeStrategy) ents.ConsumeStrategy {
	return ents.ConsumeStrategy(strategy)
}

func convertConsumePriority(priority hats_api.ConsumePriority) ents.ConsumePriority {
	return ents.ConsumePriority(priority)
}

func sptr(v *hats_api.StringValue) *string {
	if v == nil {
		return nil
	}
	str := v.Value
	return &str
}

func Iptr(v *hats_api.IntValue) *int64 {
	if v == nil {
		return nil
	}
	int := v.Value
	return &int
}

func Ivalue(iptr *int64) *hats_api.IntValue {
	if iptr == nil {
		return nil
	}
	return &hats_api.IntValue{
		Value: *iptr,
	}
}
