package twents

import (
	"context"

	"code.justin.tv/commerce/hats/internal/app/ents"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/common/twirp"
)

func (api *EntitlementsAPI) ConsumeEntitlementsByOPD(ctx context.Context, request *hats_api.ConsumeEntitlementsByOPDRequest) (*hats_api.ConsumeEntitlementsByOPDResponse, error) {
	consumed, err := api.Entitlements.ConsumeEntitlements(ctx, convertEntitlementOPDToAppParams(request.GetConsume()),
		request.GetQuantity(), convertConsumeStrategy(request.GetConsumeStrategy()),
		convertConsumePriority(request.GetConsumePriority()))
	if err != nil {
		switch err {
		case ents.ErrInvalidQuantity:
			return nil, hats_api.NewClientError(
				twirp.NewError(twirp.InvalidArgument, "Expecting positive quantity number"),
				hats_api.ErrCodeInvalidQuantity)
		case ents.ErrNotEnoughQuantity:
			return nil, hats_api.NewClientError(
				twirp.NewError(twirp.InvalidArgument, "Not enough quantity available"),
				hats_api.ErrCodeNotEnoughQuantity)
		default:
			return nil, err
		}
	}
	return &hats_api.ConsumeEntitlementsByOPDResponse{
		Consumed:          request.GetConsume(),
		QuantityConsumed:  consumed,
		QuantityRequested: request.GetQuantity(),
	}, nil
}
