package twents

import (
	"context"

	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
)

func (api *EntitlementsAPI) DeleteEntitlementsByOPDO(ctx context.Context, request *hats_api.DeleteEntitlementsByOPDORequest) (*hats_api.DeleteEntitlementsByOPDOResponse, error) {
	err := api.Entitlements.DeleteEntitlements(ctx, convertEntitlementsOPDOToAppParams(request.GetDeletes()))
	if err != nil {
		return nil, err
	}
	return &hats_api.DeleteEntitlementsByOPDOResponse{
		Deleted: request.GetDeletes(),
	}, nil
}
