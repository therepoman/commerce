package ents

import (
	"context"
	"fmt"
	"sort"

	"time"

	"code.justin.tv/commerce/hats/internal/clients"
	"code.justin.tv/commerce/hats/internal/db/entsdb"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	DomainSubOnlyChat = "subOnlyChat"
	DomainSubs        = "subs"
	DomainTurbo       = "turbo"
	ProductTurbo      = "turbo"
	DomainPrime       = "prime"
	ProductPrime      = "prime"
)

var (
	ErrInvalidProductId  = errors.New("invalid product id")
	ErrInvalidQuantity   = errors.New("invalid quantity")
	ErrNotEnoughQuantity = errors.New("not enough quantity")
)

type Entitlement struct {
	OwnerID string
	ID      string

	ProductID string
	DomainID  string
	Origin    string
	Start     *time.Time
	End       *time.Time

	Quantity *int64
}

type EntitlementKey struct {
	OwnerID   string
	ProductID *string
	DomainID  *string
	OriginID  *string
}

// IEntitlements used in App struct for mocking
type IEntitlements interface {
	GetEntitlements(ctx context.Context, keys []*EntitlementKey) ([]*Entitlement, error)
	ListEntitlementsByProductDomain(ctx context.Context, productID, domainID string) ([]*Entitlement, error)
	CreateEntitlements(ctx context.Context, ents []*Entitlement) ([]*Entitlement, error)
	DeleteEntitlements(ctx context.Context, keys []*DeleteEntitlementParams) error
	ConsumeEntitlements(ctx context.Context, keys *ConsumeEntitlementParams, quantity int64, consumeStrategy ConsumeStrategy, consumePriority ConsumePriority) (int64, error)
}

// Entitlements business logic for fetching, creating, and deleting entitlements
type Entitlements struct {
	EntitlementsDB entsdb.EntitlementsDAOInterface
	Statter        statsd.Statter
	Logger         clients.Logger
}

func NewEntitlements(c *clients.Clients, statter statsd.Statter, logger clients.Logger) (*Entitlements, error) {
	return &Entitlements{
		EntitlementsDB: c.EntitlementsDB,
		Statter:        statter,
		Logger:         logger,
	}, nil
}

func convertFromDBEntitlement(dbEnt *entsdb.Entitlement) *Entitlement {
	return &Entitlement{
		OwnerID:   dbEnt.OwnerID,
		ID:        dbEnt.ID,
		ProductID: dbEnt.ProductID,
		DomainID:  dbEnt.DomainID,
		Origin:    dbEnt.Origin,
		Start:     dbEnt.Start,
		End:       dbEnt.End,
		Quantity:  dbEnt.Quantity,
	}
}

func convertFromDBEntitlements(dbEnts []*entsdb.Entitlement) []*Entitlement {
	ents := make([]*Entitlement, len(dbEnts))
	for i, dbEnt := range dbEnts {
		ents[i] = convertFromDBEntitlement(dbEnt)
	}
	return ents
}

func filterEntitlements(ents []*Entitlement) []*Entitlement {
	filtered := make([]*Entitlement, 0)
	now := time.Now()
	for _, ent := range ents {
		if ent.Start != nil && ent.Start.After(now) {
			continue
		}
		if ent.End != nil && ent.End.Before(now) {
			continue
		}
		filtered = append(filtered, ent)
	}
	return filtered
}

func (e *Entitlements) GetEntitlements(ctx context.Context, keys []*EntitlementKey) ([]*Entitlement, error) {
	result := make([]*Entitlement, 0)
	for _, key := range keys {
		if key != nil {
			ents, err := e.checkEntitlement(ctx, *key)
			if err != nil {
				return nil, err
			}
			result = append(result, ents...)
		}
	}
	return result, nil
}

func (e *Entitlements) ListEntitlementsByProductDomain(ctx context.Context, productID, domainID string) ([]*Entitlement, error) {
	dbEnts, err := e.EntitlementsDB.ListEntitlementsByProductDomain(ctx, productID, domainID)
	if err != nil {
		return nil, err
	}
	return filterEntitlements(convertFromDBEntitlements(dbEnts)), nil
}

func (e *Entitlements) CreateEntitlements(ctx context.Context, ents []*Entitlement) ([]*Entitlement, error) {
	createdEnts := make([]*Entitlement, len(ents))
	for i, ent := range ents {
		created, err := e.EntitlementsDB.CreateEntitlement(ctx, ent.OwnerID, ent.ProductID, ent.DomainID, ent.Origin, ent.Start, ent.End, ent.Quantity)
		if err != nil {
			return nil, err
		}
		createdEnts[i] = convertFromDBEntitlement(created)
	}
	return createdEnts, nil
}

type DeleteEntitlementParams struct {
	OwnerID   string
	ProductID string
	DomainID  string
	OriginID  string
}

func (e *Entitlements) DeleteEntitlements(ctx context.Context, deletes []*DeleteEntitlementParams) error {
	for _, delete := range deletes {
		err := e.EntitlementsDB.DeleteEntitlements(ctx, delete.OwnerID, delete.ProductID, delete.DomainID, delete.OriginID)
		if err != nil {
			return err
		}
	}
	return nil
}

type ConsumeEntitlementParams struct {
	OwnerID   string
	ProductID string
	DomainID  string
}

type ConsumeStrategy int32

const (
	MOST_AVAILABLE     ConsumeStrategy = 0
	QUANTITY_REQUESTED ConsumeStrategy = 1
)

type ConsumePriority int32

const (
	EXPIRES_FIRST           ConsumePriority = 0
	OLDEST_FIRST            ConsumePriority = 1
	SMALLEST_QUANTITY_FIRST ConsumePriority = 2
)

func filterConsumableEntitlements(ents []*Entitlement) []*Entitlement {
	filteredEntitlements := filterEntitlements(ents)
	filtered := make([]*Entitlement, 0)
	for _, ent := range filteredEntitlements {
		if ent.Quantity == nil {
			continue
		}
		filtered = append(filtered, ent)
	}
	return filtered
}

func minQuantity(value1, value2 int64) int64 {
	if value1 < value2 {
		return value1
	}
	return value2
}

func sortConsumableEntitlements(ents []*Entitlement, consumePriority ConsumePriority) {
	switch consumePriority {
	case EXPIRES_FIRST:
		sortByExpiringFirst(ents)
	case OLDEST_FIRST:
		sortByOldestFirst(ents)
	case SMALLEST_QUANTITY_FIRST:
		sortBySmallestQuantity(ents)
	}
}

// Consider nil end time as never expiring
func sortByExpiringFirst(ents []*Entitlement) {
	sort.Slice(ents, func(i int, j int) bool {
		iTime := ents[i].End
		jTime := ents[j].End
		if iTime == nil {
			return jTime == nil
		}
		if jTime == nil {
			return true
		}
		return iTime.Before(*jTime)
	})
}

// Consider nil start time as older
func sortByOldestFirst(ents []*Entitlement) {
	sort.Slice(ents, func(i int, j int) bool {
		iTime := ents[i].Start
		jTime := ents[j].Start
		if iTime == nil {
			return true
		}
		if jTime == nil {
			return iTime == nil
		}
		return iTime.Before(*jTime)
	})
}

func sortBySmallestQuantity(ents []*Entitlement) {
	sort.Slice(ents, func(i int, j int) bool {
		iQuantity := ents[i].Quantity
		jQuantity := ents[j].Quantity
		return *iQuantity < *jQuantity
	})
}

func (e *Entitlements) ConsumeEntitlements(ctx context.Context, consume *ConsumeEntitlementParams, quantity int64, consumeStrategy ConsumeStrategy, consumePriority ConsumePriority) (int64, error) {
	if quantity <= 0 {
		return 0, ErrInvalidQuantity
	}

	keys := []*EntitlementKey{
		{
			OwnerID:   consume.OwnerID,
			ProductID: &consume.ProductID,
			DomainID:  &consume.DomainID,
		},
	}
	entitlements, err := e.GetEntitlements(ctx, keys)
	if err != nil {
		return 0.0, err
	}

	consumableEntitlements := filterConsumableEntitlements(entitlements)
	if consumeStrategy == QUANTITY_REQUESTED {
		availableQuantity := int64(0)
		for _, consumableEntitlement := range consumableEntitlements {
			availableQuantity += *consumableEntitlement.Quantity
		}
		if availableQuantity < quantity {
			return 0, ErrNotEnoughQuantity
		}
	}

	sortConsumableEntitlements(consumableEntitlements, consumePriority)

	totalConsumed := int64(0)
	consumedMap := make(map[*Entitlement]int64)
	// TODO SUBS-1215: Revisit error logic
	for _, entitlement := range consumableEntitlements {
		var consumed int64
		var err error
		consumingQuantity := minQuantity(quantity-totalConsumed, *entitlement.Quantity)
		if *entitlement.Quantity > consumingQuantity {
			consumed, err = e.EntitlementsDB.ConsumeEntitlementForQuantity(ctx, entitlement.OwnerID, entitlement.ID, consumingQuantity)
		} else {
			var deleted *entsdb.Entitlement
			deleted, err = e.EntitlementsDB.DeleteEntitlement(ctx, entitlement.OwnerID, entitlement.ID)
			if deleted != nil && deleted.Quantity != nil {
				consumed = *deleted.Quantity
			}
		}
		if err != nil {
			return totalConsumed, err
		}
		consumedMap[entitlement] = consumed
		totalConsumed += consumed
		if totalConsumed == quantity {
			break
		}
	}

	if consumeStrategy == QUANTITY_REQUESTED && totalConsumed != quantity {
		for entitlement, consumed := range consumedMap {
			if consumed > 0 {
				restoring := consumed
				_, err := e.EntitlementsDB.CreateEntitlement(ctx, entitlement.OwnerID, entitlement.ProductID, entitlement.DomainID, entitlement.Origin, entitlement.Start, entitlement.End, &restoring)
				if err != nil {
					return totalConsumed, err
				}
				totalConsumed -= consumed
			}
		}
	}

	return totalConsumed, nil
}

func (e *Entitlements) checkEntitlement(ctx context.Context, key EntitlementKey) ([]*Entitlement, error) {
	domain := ""
	if key.DomainID != nil {
		domain = *key.DomainID
	}

	err := e.Statter.Inc(fmt.Sprintf("endpoint.GetEntitlements.domain.%s", domain), 1, 1.0)
	if err != nil {
		err = errors.Wrap(err, "sending domain metrics")
		e.Logger.WithError(err).Error(err)
	}

	return e.checkDynamo(ctx, key)
}

func (e *Entitlements) checkDynamo(ctx context.Context, key EntitlementKey) ([]*Entitlement, error) {
	entitlements, err := e.EntitlementsDB.GetEntitlements(ctx, key.OwnerID, key.ProductID, key.DomainID, key.OriginID)
	if err != nil {
		return nil, err
	}
	return filterEntitlements(convertFromDBEntitlements(entitlements)), nil
}
