package app

import (
	"log"

	"code.justin.tv/commerce/hats/internal/app/ents"
	"code.justin.tv/commerce/hats/internal/clients"
	"github.com/pkg/errors"
)

// App contains the business logic of HATS
type App struct {
	Entitlements ents.IEntitlements
}

// InitializeApp initalizes the business logic structs inside App and returns a pointer to the App
// struct
func InitializeApp(c *clients.Clients) (*App, error) {
	e, err := ents.NewEntitlements(c, c.Statter, c.Logger)
	if err != nil {
		log.Fatal(errors.Wrap(err, "failed to initialize entitlements"))
	}

	return &App{
		Entitlements: e,
	}, nil
}
