package clients

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/hats/pkg/logrusadapter"
	"code.justin.tv/commerce/splatter"

	"github.com/heroku/rollrus"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"code.justin.tv/commerce/hats/config"
	"code.justin.tv/commerce/hats/internal/db/entsdb"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	namespace = "hats"

	twitchTelemetryMinBuffer = 100000 // Define a min buffer size to ensure there is enough room for metrics while flushing
)

type Clients struct {
	Logger         Logger
	Statter        statsd.Statter
	EntitlementsDB entsdb.EntitlementsDAOInterface
}

type Logger interface {
	logrus.FieldLogger
	LogError(err error)
}

func InitializeClients(cfg *config.Config, l *logrusadapter.LogrusAdapter) (*Clients, error) {
	sandstorm, err := newSandstormManager(cfg.SandstormConfig)
	if err != nil {
		l.Fatal(errors.Wrap(err, "failed to initialize sandstorm manager"))
	}

	err = setupRollbar(cfg, sandstorm, l)
	if err != nil {
		l.Fatal(errors.Wrap(err, "failed to setup rollbar hook"))
	}

	stats, err := newStats(*cfg, l)
	if err != nil {
		l.Fatal(errors.Wrap(err, "failed to initialize statsd client"))
	}

	edb, err := newEntitlementsDBClient(cfg.HatsConfig)
	if err != nil {
		l.Fatal(errors.Wrap(err, "failed to initialize entsdb"))
	}

	return &Clients{
		Logger:         l,
		Statter:        stats,
		EntitlementsDB: edb,
	}, nil
}

// NewSandstormManager connects to sandstorm and returns a sandstorm manager which can be used to
// retreive secrets.
func newSandstormManager(cfg config.SandstormConfig) (*manager.Manager, error) {
	sandstormArn := cfg.RoleArn

	awsConfig := &aws.Config{Region: aws.String("us-west-2")}
	stsclient := sts.New(session.New(awsConfig))
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      sandstormArn,
		Client:       stsclient,
	}
	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)

	config := manager.Config{
		AWSConfig: awsConfig,
		TableName: "sandstorm-production",
		KeyID:     "alias/sandstorm-production",
	}

	m := manager.New(config)

	return m, nil
}

func newStats(cfg config.Config, l Logger) (statsd.Statter, error) {
	if cfg.StatsdConfig.Noop {
		stats, err := statsd.NewNoopClient()
		return stats, err
	}

	var statters []statsd.Statter

	telemetryStatter := setupTwitchTelemetryStatter(cfg)
	statsdStatter, err := setupStatsdStatter(cfg, l)
	if err != nil {
		return nil, err
	}

	statters = append(statters, telemetryStatter, statsdStatter)

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	return stats, nil
}

func setupStatsdStatter(cfg config.Config, l Logger) (statsd.Statter, error) {
	statsdStatter, err := statsd.NewBufferedClient(cfg.StatsdConfig.URL, fmt.Sprintf("%s.%s", cfg.StatsdConfig.Prefix, "all"), 0, 0)
	if err != nil {
		l.Errorf("Failed to connect to statsd endpoint. Make sure you are on JTV-SFO network if running locally.")
		l.Fatalf("Failed to initialize statsd client: %s", err)
		return nil, errors.Wrap(err, "failed to initialize statsd client")
	}

	return statsdStatter, nil
}

func setupTwitchTelemetryStatter(cfg config.Config) statsd.Statter {
	bufferSize := cfg.CloudwatchConfig.BufferSize
	if bufferSize < twitchTelemetryMinBuffer {
		bufferSize = twitchTelemetryMinBuffer
	}

	twitchTelemetryConfig := &splatter.BufferedTelemetryConfig{
		FlushPeriod:       time.Duration(cfg.CloudwatchConfig.TwitchTelemetryFlush) * time.Second,
		BufferSize:        int(bufferSize),
		AggregationPeriod: time.Minute,
		ServiceName:       namespace,
		AWSRegion:         cfg.CloudwatchConfig.Region,
		Stage:             cfg.CloudwatchConfig.Environment,
		Substage:          cfg.CloudwatchConfig.Environment,
		Prefix:            "", // no need for prefix
	}
	return splatter.NewBufferedTelemetryCloudWatchStatter(twitchTelemetryConfig, map[string]bool{})
}

func newEntitlementsDBClient(cfg config.HatsConfig) (*entsdb.EntitlementsDAO, error) {
	awsConfig := &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	}
	awsSession, err := session.NewSession(awsConfig)
	if err != nil {
		return nil, err
	}

	dynamo := dynamodb.New(awsSession)
	return entsdb.NewEntitlementsDAO(dynamo)
}

func setupRollbar(cfg *config.Config, sandstorm *manager.Manager, logger *logrusadapter.LogrusAdapter) error {
	sandstormEnv := cfg.SandstormConfig.Environment

	// Fetch rollbarToken from sandstorm
	rollbarToken, err := sandstorm.Get(fmt.Sprintf("commerce/hats/%s/%s", sandstormEnv, cfg.RollbarConfig.SandstormKey))
	if err != nil {
		logger.Error("Failed to retreive rollbarToken from sandstorm")
		return err
	}

	// Add rollbar hook to logger
	rollbarHook := rollrus.NewHook(string(rollbarToken.Plaintext), cfg.RollbarConfig.Environment)
	logger.Hooks.Add(rollbarHook)

	return nil
}
