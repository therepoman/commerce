package setup

import (
	"log"
	"net/http"

	"code.justin.tv/commerce/hats/internal/app"
	"code.justin.tv/commerce/hats/internal/clients"
	"code.justin.tv/commerce/hats/internal/handlers/twirp/twents"
	"code.justin.tv/commerce/hats/pkg/middleware"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
)

func HTTPHandler(c *clients.Clients, a *app.App) http.Handler {
	mux := http.NewServeMux()

	// Add health check to mux
	mux.HandleFunc("/health", runHealthCheck)

	// Handle pprof routes
	mux.Handle("/debug/", http.DefaultServeMux)

	// Add entitlements twirp handlers to mux
	entitlementsTwirpHandler := twents.NewEntitlementsAPIHandler(c, a)
	mux.Handle(hats_api.EntitlementsPathPrefix, entitlementsTwirpHandler)

	// Add panic recovery middleware
	handler := middleware.DecoratePanicRecovery(mux, c.Logger)

	return handler
}

func runHealthCheck(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Println("OK response failed")
	}
}
