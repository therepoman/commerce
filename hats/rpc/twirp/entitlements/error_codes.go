package hats_api

// These error codes are meant to be returned within a hats_api.ClientError type.

const (
	ErrCodeNotEnoughQuantity = "NOT_ENOUGH_QUANTITY"
	ErrCodeInvalidProductId  = "INVALID_PRODUCT_ID"
	ErrCodeInvalidQuantity   = "INVALID_QUANTITY"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
