package test

import (
	"math/rand"
	"time"

	uuid "github.com/satori/go.uuid"
)

func randstr() string {
	return uuid.NewV4().String()
}

func OwnerID() string {
	return "tuid-" + randstr()
}

func ProductID() string {
	return "productid-" + randstr()
}

func DomainID() string {
	return "domainid-" + randstr()
}

func OriginID() string {
	return "originid-" + randstr()
}

func Quantity() int64 {
	rand.Seed(time.Now().Unix())
	return rand.Int63n(10)
}

type ITestingClock interface {
	NowAdd(duration time.Duration) time.Time
}

type TestingClock struct {
	now time.Time
}

func NewTestingClock() *TestingClock {
	return &TestingClock{
		now: time.Now(),
	}
}

func (this *TestingClock) Now() time.Time {
	return this.now
}

func (this *TestingClock) NowAdd(duration time.Duration) time.Time {
	return this.now.Add(duration)
}

type NoOpTestingClock struct {
}

func NewNoopTestingClock() *NoOpTestingClock {
	return &NoOpTestingClock{}
}

func (this *NoOpTestingClock) Now() time.Time {
	return time.Now()
}

func (this *NoOpTestingClock) NowAdd(duration time.Duration) time.Time {
	return time.Now().Add(duration)
}
