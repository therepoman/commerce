package test

import (
	"fmt"
	"strings"
	"time"

	"errors"

	"code.justin.tv/commerce/hats/internal/db/entsdb"
	"code.justin.tv/commerce/hats/internal/handlers/twirp/twents"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/onsi/gomega/types"
)

func DAOEntitlementEqual(ent *entsdb.Entitlement) types.GomegaMatcher {
	return &daoEntitlementEqual{
		expected:   ent,
		mismatches: []string{},
	}
}

type daoEntitlementEqual struct {
	expected   *entsdb.Entitlement
	mismatches []string
}

func (this *daoEntitlementEqual) mismatch(s string) {
	this.mismatches = append(this.mismatches, s)
}

func (this *daoEntitlementEqual) strMismatch(field, s1, s2 string) {
	if s1 != s2 {
		this.mismatch(fmt.Sprintf("%s: %s != %s", field, s1, s2))
	}
}

func (this *daoEntitlementEqual) dateMismatch(field string, t1, t2 time.Time) {
	t2u := t2.Unix()
	t1u := t1.Unix()
	if t1u != t2u {
		this.mismatch(fmt.Sprintf("%s: %d != %d", field, t1u, t2u))
	}
}

func (this *daoEntitlementEqual) Match(actual interface{}) (success bool, err error) {
	ent, ok := actual.(*entsdb.Entitlement)
	if !ok {
		return false, fmt.Errorf("DaoEntitlementEqual expects *entsdb.Entitlement")
	}

	if ent == nil {
		return false, fmt.Errorf("Expected entitlement to not be nil")
	}

	this.strMismatch("ID", ent.ID, this.expected.ID)
	this.strMismatch("OwnerID", ent.OwnerID, this.expected.OwnerID)
	this.strMismatch("ProductID", ent.ProductID, this.expected.ProductID)
	this.strMismatch("DomainID", ent.DomainID, this.expected.DomainID)
	this.strMismatch("Origin", ent.Origin, this.expected.Origin)

	return len(this.mismatches) == 0, nil
}

func (this *daoEntitlementEqual) FailureMessage(actual interface{}) string {
	return fmt.Sprintf("Expected entitlements to equal: %s", strings.Join(this.mismatches, "\n\t*"))
}

func (this *daoEntitlementEqual) NegatedFailureMessage(actual interface{}) string {
	return "Expected entitlmenets not to be equal but were"
}

func tmOrTs(i interface{}) (*time.Time, error) {
	tm, ok := i.(time.Time)
	if ok {
		return &tm, nil
	}
	tmp, ok := i.(*time.Time)
	if ok {
		return tmp, nil
	}
	ts, ok := i.(*hats_api.TimestampValue)
	if ok {
		return twents.TimeFromProtobufTime(ts), nil
	}
	return nil, errors.New("TimeEQ takes a *time.Time or a *hats_api.TimestampValue")
}

func TimeEq(expected interface{}) types.GomegaMatcher {
	tm, err := tmOrTs(expected)
	if err != nil {
		panic(err)
	}

	return &timeEq{
		expected:   tm,
		failureMsg: nil,
	}
}

type timeEq struct {
	expected   *time.Time
	failureMsg *string
}

func (this *timeEq) Match(actualInterface interface{}) (success bool, err error) {
	actual, err := tmOrTs(actualInterface)
	if err != nil {
		return false, err
	}
	if this.expected == nil && actual != nil {
		this.failureMsg = aws.String(fmt.Sprintf("Expect nil but was %s", actual))
	} else if actual == nil && this.expected != nil {
		this.failureMsg = aws.String(fmt.Sprintf("Expected %s but was nil", this.expected))
	} else if this.expected != nil && actual != nil && this.expected.Unix() != actual.Unix() {
		this.failureMsg = aws.String(fmt.Sprintf("Expected %s but was %s", this.expected, actual))
	}

	return this.failureMsg == nil, nil
}

func (this *timeEq) FailureMessage(actual interface{}) string {
	if this.failureMsg == nil {
		return ""
	}
	return *this.failureMsg
}

func (this *timeEq) NegatedFailureMessage(actual interface{}) string {
	return "Expected times to be not equal but were"
}

func intPtrOrIntValue(i interface{}) (*int64, error) {
	intPtr, ok := i.(*int64)
	if ok {
		return intPtr, nil
	}
	intValue, ok := i.(*hats_api.IntValue)
	if ok {
		return twents.Iptr(intValue), nil
	}
	return nil, errors.New("QuantityEq takes a *int64 or a *hats_api.IntValue")
}

func QuantityEq(expected *int64) types.GomegaMatcher {
	return &quantityEq{
		expected:   expected,
		failureMsg: nil,
	}
}

type quantityEq struct {
	expected   *int64
	failureMsg *string
}

func (this *quantityEq) Match(actualInterface interface{}) (success bool, err error) {
	actual, err := intPtrOrIntValue(actualInterface)
	if err != nil {
		return false, err
	}
	if this.expected == nil && actual != nil {
		this.failureMsg = aws.String(fmt.Sprintf("Expect nil but was %v", *actual))
	} else if actual == nil && this.expected != nil {
		this.failureMsg = aws.String(fmt.Sprintf("Expected %v but was nil", *this.expected))
	} else if this.expected != nil && actual != nil && *this.expected != *actual {
		this.failureMsg = aws.String(fmt.Sprintf("Expected %v but was %v", *this.expected, *actual))
	}

	return this.failureMsg == nil, nil
}

func (this *quantityEq) FailureMessage(actual interface{}) string {
	if this.failureMsg == nil {
		return ""
	}
	return *this.failureMsg
}

func (this *quantityEq) NegatedFailureMessage(actual interface{}) string {
	return "Expected quantities to be not equal but were"
}
