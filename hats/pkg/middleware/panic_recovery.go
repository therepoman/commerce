package middleware

import (
	"errors"
	"fmt"
	"net/http"
)

type errorLogger interface {
	LogError(err error)
}

func DecoratePanicRecovery(handler http.Handler, logger errorLogger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			// Just need to recover and log the error.
			// Twirp handles sending back the http response.
			if r := recover(); r != nil {
				errMsg := fmt.Sprintf("panic recovered: %v", r)
				logger.LogError(errors.New(errMsg))
			}
		}()
		handler.ServeHTTP(w, r)
	})
}
