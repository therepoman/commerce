package logrusadapter

import (
	"github.com/sirupsen/logrus"
)

type LogrusAdapter struct {
	*logrus.Logger
}

func New(logger *logrus.Logger) LogrusAdapter {
	return LogrusAdapter{logger}
}

func (l LogrusAdapter) LogError(err error) {
	l.WithError(err).Error(err.Error())
}
