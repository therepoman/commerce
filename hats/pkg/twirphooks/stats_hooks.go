package twirphooks

import (
	"context"
	"net/http"
	"strconv"

	"code.justin.tv/chat/timing"
	"code.justin.tv/common/twirp"
	"github.com/cactus/go-statsd-client/statsd"
)

// XactHooks returns twirp hooks for reporting stats from xact groups
func XactHooks(stats statsd.Statter) *twirp.ServerHooks {
	hooks := twirp.NewServerHooks()

	// When a request is routed to a method, start xact timings.
	hooks.RequestRouted = func(ctx context.Context) context.Context {
		methodName, ok := twirp.MethodName(ctx)
		if !ok || methodName == "" {
			return ctx
		}

		// Attach xact to ctx
		xact := &timing.Xact{Stats: stats}
		xact.Start()
		xact.AddName("endpoint." + methodName)
		ctx = timing.XactContext(ctx, xact)

		return ctx
	}

	// When a request has written all bytes, end xact timings.
	hooks.ResponseSent = func(ctx context.Context) context.Context {
		// Get status code
		statusCode, statusCodeOK := twirp.StatusCode(ctx)
		if !statusCodeOK || statusCode == "" {
			// If no status code was explicitly written, default to 500 Internal Server Error.
			statusCode = strconv.Itoa(http.StatusInternalServerError)
		}

		// Report xact
		xact, ok := timing.XactFromContext(ctx)
		if ok {
			xact.End(statusCode)
		}

		return ctx
	}
	return hooks
}
