package twirphooks

import (
	"context"

	"code.justin.tv/common/twirp"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

func ErrorHooks(logger logrus.FieldLogger) *twirp.ServerHooks {
	hooks := twirp.NewServerHooks()
	hooks.Error = func(ctx context.Context, err twirp.Error) context.Context {
		// Convert twirp err meta to logrus fields
		metaMap := err.MetaMap()
		fields := make(logrus.Fields)
		for key, val := range metaMap {
			fields[key] = val
		}

		// full_error_msg is the full error message since only the error message of the cause
		// will be reported by WithError(cause)
		fields["full_error_msg"] = err.Error()

		// Get method name
		methodName, ok := twirp.MethodName(ctx)
		if ok {
			fields["method_name"] = methodName
		}

		// Log error
		switch err.Code() {
		case twirp.Internal, twirp.Unknown, twirp.Unimplemented, twirp.Unavailable, twirp.DataLoss:
			// Rollrus unwraps the error all the way -- but we want to unwrap until the last error
			// with a stack trace attached to it.
			causeWithStack := getCauseWithStack(err)
			logger.WithFields(fields).WithError(causeWithStack).Error(err)
		default:
			logger.WithFields(fields).Warn(err)
		}
		return ctx
	}
	return hooks
}

func getCauseWithStack(err error) error {
	type causer interface {
		Cause() error
	}
	type stackTracer interface {
		StackTrace() errors.StackTrace
	}

	cause := err.(error)
	causeWithStack := err.(error)
	for {
		cErr, ok := cause.(causer)
		if !ok {
			break
		}
		cause = cErr.Cause()
		if cause == nil {
			break
		}
		_, ok = cause.(stackTracer)
		if !ok {
			continue
		}
		causeWithStack = cErr.Cause()
	}
	return causeWithStack
}
