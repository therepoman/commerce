package main

import (
	"context"
	"log"

	"net/http"

	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
)

func main() {
	api := hats_api.NewEntitlementsProtobufClient("http://hats.prod.us-west2.justin.tv", &http.Client{})

	productID := "7236692"
	domainID := "subOnlyChat"
	req := &hats_api.GetEntitlementsRequest{
		Keys: []*hats_api.EntitlementKey{
			{
				OwnerId:   "45776283",
				ProductId: &hats_api.StringValue{Value: productID},
				Domain:    &hats_api.StringValue{Value: domainID},
			},
		},
	}
	resp, err := api.GetEntitlements(context.Background(), req)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Result: %s", resp.Entitlements)
}
