package main

import (
	"flag"
	"io/ioutil"
	"log"

	"code.justin.tv/common/appconfig-go/resolvers"
)

func main() {
	dirP := flag.String("in", "", "directory to scan")
	outP := flag.String("out", "", "output file")

	flag.Parse()

	if dirP == nil {
		log.Fatal("Dir missing")
		return
	}
	if outP == nil {
		log.Fatal("Output missing")
		return
	}

	dir := *dirP
	out := *outP

	log.Println("Walking ", dir)
	log.Println("Outputting", out)

	set, err := resolvers.GlobResolve(dir, ".acg")
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Loaded ", len(set.Rules), " Rules")

	data := set.String()
	err = ioutil.WriteFile(out, []byte(data), 0644)
	if err != nil {
		log.Fatal(err)
	}
}
