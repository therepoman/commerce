package appconfig

import "code.justin.tv/common/appconfig-go/ast"

type AppConfigSet struct {
	Rules []*ast.AppConfigRule
}

func NewAppConfigSet(rules ...[]*ast.AppConfigRule) *AppConfigSet {
	set := &AppConfigSet{
		Rules: []*ast.AppConfigRule{},
	}
	for _, ruleSet := range rules {
		for _, rule := range ruleSet {
			set.Add(rule)
		}
	}
	return set
}

func (this *AppConfigSet) NewAppConfig(selectors ...string) *AppConfig {
	return &AppConfig{
		Rules:     this.Rules,
		Selectors: selectors,
	}
}

func (this *AppConfigSet) String() string {
	strOut := ""

	for _, r := range this.Rules {
		strOut += r.String() + "\n"
	}

	return strOut
}

func (this *AppConfigSet) Add(rule *ast.AppConfigRule) {
	this.Rules = append(this.Rules, rule)
}

type AppConfig struct {
	Rules     []*ast.AppConfigRule
	Selectors []string
}

func (this *AppConfig) MatchingRule(namespace string, keys ...string) *ast.AppConfigRule {

	matches := []*ast.AppConfigRule{}
RULESEARCH:
	for _, r := range this.Rules {
		if r.Namespace != namespace {
			continue
		}
		if len(r.Keys) != len(keys) {
			continue
		}

		for i := 0; i < len(r.Keys); i++ {
			if r.Keys[i] != keys[i] {
				continue RULESEARCH
			}
		}

		matches = append(matches, r)
	}

	if len(matches) == 0 {
		return nil
	}

	var match *ast.AppConfigRule
	score := -1

	for _, rule := range matches {
		thisScore := 0
		selectors := rule.Selectors
		if len(selectors) != len(this.Selectors) { // mismatch == 0 score
			thisScore = 0
		} else {
			for i := 0; i < len(selectors); i++ {
				if this.Selectors[i] == "*" {
					continue
				}
				if selectors[i] == this.Selectors[i] {
					thisScore++
				}
			}
		}
		if thisScore > score {
			match = rule
			score = thisScore
		}
	}
	return match
}

func (this *AppConfig) Int(namespace string, keys ...string) *int64 {
	match := this.MatchingRule(namespace, keys...)
	if match == nil {
		return nil
	}
	return match.IntValue
}

func (this *AppConfig) String(namespace string, keys ...string) *string {
	match := this.MatchingRule(namespace, keys...)
	if match == nil {
		return nil
	}
	return match.StrValue
}

func (this *AppConfig) Bool(namespace string, keys ...string) *bool {
	match := this.MatchingRule(namespace, keys...)
	if match == nil {
		return nil
	}
	return match.BoolValue
}
