// Generated from AppConfig.g4 by ANTLR 4.7.

package parser // AppConfig

import "github.com/antlr/antlr4/runtime/Go/antlr"

// AppConfigListener is a complete listener for a parse tree produced by AppConfigParser.
type AppConfigListener interface {
	antlr.ParseTreeListener

	// EnterFile is called when entering the file production.
	EnterFile(c *FileContext)

	// EnterRecord is called when entering the record production.
	EnterRecord(c *RecordContext)

	// EnterSelectors is called when entering the selectors production.
	EnterSelectors(c *SelectorsContext)

	// EnterSelector is called when entering the selector production.
	EnterSelector(c *SelectorContext)

	// EnterKeyspec is called when entering the keyspec production.
	EnterKeyspec(c *KeyspecContext)

	// EnterNamespace is called when entering the namespace production.
	EnterNamespace(c *NamespaceContext)

	// EnterKeys is called when entering the keys production.
	EnterKeys(c *KeysContext)

	// EnterValue is called when entering the value production.
	EnterValue(c *ValueContext)

	// EnterArray is called when entering the array production.
	EnterArray(c *ArrayContext)

	// EnterObj is called when entering the obj production.
	EnterObj(c *ObjContext)

	// EnterPair is called when entering the pair production.
	EnterPair(c *PairContext)

	// ExitFile is called when exiting the file production.
	ExitFile(c *FileContext)

	// ExitRecord is called when exiting the record production.
	ExitRecord(c *RecordContext)

	// ExitSelectors is called when exiting the selectors production.
	ExitSelectors(c *SelectorsContext)

	// ExitSelector is called when exiting the selector production.
	ExitSelector(c *SelectorContext)

	// ExitKeyspec is called when exiting the keyspec production.
	ExitKeyspec(c *KeyspecContext)

	// ExitNamespace is called when exiting the namespace production.
	ExitNamespace(c *NamespaceContext)

	// ExitKeys is called when exiting the keys production.
	ExitKeys(c *KeysContext)

	// ExitValue is called when exiting the value production.
	ExitValue(c *ValueContext)

	// ExitArray is called when exiting the array production.
	ExitArray(c *ArrayContext)

	// ExitObj is called when exiting the obj production.
	ExitObj(c *ObjContext)

	// ExitPair is called when exiting the pair production.
	ExitPair(c *PairContext)
}
