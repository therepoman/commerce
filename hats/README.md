HATS
====
Hats Asa Twitch Service

Hats is the twitch entitlment service.

An entitlement is:
A thing (usually a user) owns a thing (item) for a reason (origin).

Development
===========
To get started either make a new gopath:

```
mkdir ~/ws-hats && cd ~/ws-hats && export GOPATH=`pwd` && export PATH=$GOPATH/bin:$PATH
```

Or just cd into your favorite gopath and set it up.

Now grab the project:

```
git clone git@git-aws.internal.justin.tv:commerce/hats.git $GOPATH/src/code.justin.tv/commerce/hats
```

And finally go into the project folder:

```
$GOPATH/src/code.justin.tv/commerce/hats
```

Building
========

Generally we make with
```
make
```

Which will grab all dependencies and then build mocks, lints, tests, and the executables.

Integration Tests
=================
After a successful make but before push to master, do the following:

```
make local_integration
```

This will run a majority of the functional tests against your
local server to ensure things are running.

In the future this will be built into make release.

## Deployment

1. Deploy your feature branch to staging using the [deployment tool](https://clean-deploy.internal.justin.tv/#/commerce/hats).
2. Perform necessary tests.
3. Open a pull request.
4. Once PR is approved, squash and merge your feature branch into `master`.
5. Ensure that your build succeeds on `master`.
6. Deploy `master` to production using the [deployment tool](https://clean-deploy.internal.justin.tv/#/commerce/hats).

Account Setup
=============

AWS Setup
---------

In order to have access to credentials served by Sandstorm, you must have set up your AWS credentials
inside ~/.aws. For information on how to do this [see 'aws configure'](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html).

Sandstorm Setup
---------------
In order for your AWS IAM user to have access to Sandstorm credentials, you must have someone authorize
your IAM to the `hats-dev` role using the [Sandstorm dashboard](https://dashboard.internal.justin.tv/sandstorm).
Select "Manage roles" on the left sidebar and find the role `hats-dev`. After selecting `hats-dev`,
add your IAM ARN to the list of "Allowed ARNs".


RETOOL
======

We vendor all of our go tooling with retool. https://github.com/twitchtv/retool. If the build process
requires a new go executable to run, make sure to retool add it. Also make sure in the Makefile
to reference it from $(TOOLS)/bin as you will then get the project local tool and not accidentally grab one from
another project in the GOPATH.

NON-GO Tools
============
We need to vendor any tool or at least a method to get them for any non-go tool we have.
An example of this is protoc. We should not assume that it is intalled on the machine. If
you add one you must add a script for installing it like install_proto.bash which needs to work on
both mac and linux. This needs to be referenced from ```make setup```.

Failure to do this will anger the gaffo and also make new devs startup be crappy.

Manta
-----
If you have an issue with the build you may need to run it with manta. Make sure you are in your gopath.

```
go get code.justin.tv/dta/manta/cmd/manta
```

Now you can run the build with:

```
manta -f build.json
```

Terraform
=========

Account Setup
-------------

You will need to go to isengard and create users for the following aws accounts:
* prod: twitch-hats-aws
* staging: twitch-hats-dev

Create a user under the developer role (basically admin) and get the aws credentials into
a section of your aws credentials file with the same name of the account.aah

DNS Account Setup
-----------------
You will need to create a DNS account as well... so get a twitch-science-aws credential following the instructions on https://wiki.twitch.com/display/ENG/AWS+account+requests.

You only need the dns edit role

Once you have it put it in your ~/.aws/credentials file in the
[twitch-dns] section
