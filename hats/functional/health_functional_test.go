package functional_test

import (
	"io/ioutil"
	"net/http"

	. "github.com/onsi/gomega"
)

var _ = Describe("Health Tests", func() {
	Functionals()
	It("Should Work", func() {
		req, err := http.NewRequest(http.MethodGet, "/health", nil)
		Expect(err).To(BeNil())

		result, err := HttpClient().Do(req)
		Expect(err).To(BeNil())

		Expect(result.StatusCode).To(BeEquivalentTo(200))

		data, err := ioutil.ReadAll(result.Body)
		Expect(err).To(BeNil())
		Expect(string(data)).To(BeEquivalentTo("OK"))
	})
})
