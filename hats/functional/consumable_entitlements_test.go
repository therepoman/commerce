package functional_test

import (
	"time"

	"code.justin.tv/commerce/hats/internal/db/entsdb"
	"code.justin.tv/commerce/hats/internal/handlers/twirp/twents"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/commerce/hats/test"
	. "github.com/onsi/gomega"
)

var _ = Describe("Consumable Entitlements", func() {
	var (
		tuid, product, domain, origin        string
		quantity1, quantity2                 int64
		lastHour, nextHour, lastDay, nextDay time.Time
	)
	BeforeEach(func() {
		tuid = test.OwnerID()
		product = test.ProductID()
		domain = test.DomainID()
		origin = test.OriginID()

		quantity1 = 1
		quantity2 = 2

		lastHour = Clock().NowAdd(-time.Hour)
		nextHour = Clock().NowAdd(time.Hour)
		lastDay = Clock().NowAdd(-time.Hour * 24)
		nextDay = Clock().NowAdd(time.Hour * 24)
	})

	Functionals()

	Describe("User", func() {
		Describe("No Entitlement", func() {
			It("Should not have entitlement", func() {
				result, err := getEntitlements(tuid, &product, &domain, &origin)

				Expect(err).To(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
			It("Should not find entitlements by PD", func() {
				result, err := listEntitlementsByPD(product, domain)
				Expect(err).To(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
		})

		Describe("With Consumable Entitlement", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, nil, nil, &quantity2)
				Expect(err).To(BeNil())
			})

			It("Should have entitlement", func() {
				Expect(getEntitlementsNoErr(tuid, &product, &domain, nil)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, &quantity2))
			})

			It("Should find entitlement by PD", func() {
				Expect(listEntitlementsByPDNoErr(product, domain)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, &quantity2))
			})
		})

		Describe("Consuming negative number quantity", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, nil, nil, &quantity2)
				Expect(err).To(BeNil())
			})

			It("Shouldn't consume any quantity", func() {
				consuming := int64(-1)
				_, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).NotTo(BeNil())
				assertHATSError(err, hats_api.ErrCodeInvalidQuantity)
			})
		})

		Describe("Consuming across multiple entitlements", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, nil, nil, &quantity2)
				Expect(err).To(BeNil())
				_, err = createEntitlement(tuid, product, domain, origin, nil, nil, &quantity2)
				Expect(err).To(BeNil())
				_, err = createEntitlement(tuid, product, domain, origin, nil, nil, &quantity2)
				Expect(err).To(BeNil())
			})

			It("Should consume in each entitlement depending of their individual quantity", func() {
				By("Consuming requested quantity")
				consuming := 3*quantity2 - 1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Have the correct number of entitlement / quantity left")
				remainingQuantity := int64(1)
				Expect(getEntitlementsNoErr(tuid, &product, &domain, &origin)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, &remainingQuantity))
			})
		})

		Describe("Consuming Entitlements by ConsumeStrategy", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, nil, nil, &quantity2)
				Expect(err).To(BeNil())
			})

			It("Should consume requested quantity (ConsumeStrategy: Most_Available)", func() {
				By("Consuming requested quantity")
				consuming := quantity2 - 1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving the updated quantity")
				remainingQuantity := int64(1)
				Expect(getEntitlementsNoErr(tuid, &product, &domain, &origin)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, &remainingQuantity))
			})

			It("Should consume available quantity (ConsumeStrategy: Most_Available)", func() {
				By("Consuming available quantity")
				consuming := quantity2 + 1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&quantity2))

				By("Retriving no product")
				result, err := getEntitlements(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})

			It("Should consume requested quantity (ConsumeStrategy: Quantity_Requested)", func() {
				By("Consuming requested quantity")
				consuming := quantity2 - 1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_QUANTITY_REQUESTED)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving the updated quantity")
				remainingQuantity := int64(1)
				Expect(getEntitlementsNoErr(tuid, &product, &domain, &origin)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, &remainingQuantity))
			})

			It("Should consume available quantity (ConsumeStrategy: Quantity_Requested)", func() {
				By("Not consuming available quantity")
				consuming := quantity2 + 1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_QUANTITY_REQUESTED)
				Expect(err).ToNot(BeNil())
				assertHATSError(err, hats_api.ErrCodeNotEnoughQuantity)
				Expect(resultConsume).To(BeNil())

				By("Retriving the product")
				result, err := getEntitlements(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(result.Entitlements).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, &quantity2))
			})
		})

		Describe("Consuming Entitlements by ConsumePriority", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1)
				Expect(err).To(BeNil())
				_, err = createEntitlement(tuid, product, domain, origin, &lastDay, &nextDay, &quantity2)
				Expect(err).To(BeNil())
				_, err = createEntitlement(tuid, product, domain, origin, nil, nil, &quantity1)
				Expect(err).To(BeNil())
			})

			It("Consume with ConsumePriority: Expires_First", func() {
				By("Consuming first expiring")
				consuming := quantity1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving remaining entitlements")
				entitlements := getEntitlementsNoErr(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(len(entitlements)).To(BeEquivalentTo(2))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, &lastDay, &nextDay, &quantity2))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, nil, nil, &quantity1))

				By("Consuming second expiring")
				consuming = quantity2
				resultConsume, err = consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed = resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving remaining entitlements")
				entitlements = getEntitlementsNoErr(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(len(entitlements)).To(BeEquivalentTo(1))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, &lastDay, &nextDay, &quantity2))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, nil, nil, &quantity1))
			})

			It("Consume with ConsumePriority: Oldest_First", func() {
				By("Consuming first starting")
				consuming := quantity1
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_OLDEST_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving remaining entitlements")
				entitlements := getEntitlementsNoErr(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(len(entitlements)).To(BeEquivalentTo(2))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, &lastDay, &nextDay, &quantity2))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, nil, nil, &quantity1))

				By("Consuming second starting")
				consuming = quantity2
				resultConsume, err = consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_OLDEST_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed = resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving remaining entitlements")
				entitlements = getEntitlementsNoErr(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(len(entitlements)).To(BeEquivalentTo(1))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, &lastDay, &nextDay, &quantity2))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, nil, nil, &quantity1))
			})

			It("Consume with ConsumePriority: Smallest_Quantity_First", func() {
				By("Consuming 2 small quantity first")
				consuming := quantity2
				resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_SMALLEST_QUANTITY_FIRST, hats_api.ConsumeStrategy_MOST_AVAILABLE)
				Expect(err).To(BeNil())
				consumed := resultConsume.GetQuantityConsumed()
				Expect(&consumed).To(test.QuantityEq(&consuming))

				By("Retriving remaining entitlements")
				entitlements := getEntitlementsNoErr(tuid, &product, &domain, &origin)
				Expect(err).To(BeNil())
				Expect(len(entitlements)).To(BeEquivalentTo(1))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
				Expect(entitlements).To(ExistingEntitlementInList(tuid, product, domain, origin, &lastDay, &nextDay, &quantity2))
				Expect(entitlements).NotTo(ExistingEntitlementInList(tuid, product, domain, origin, nil, nil, &quantity1))
			})
		})
	})
})

var _ = Describe("Consumable Entitlements with Mocks", func() {
	var (
		tuid, product, domain, origin, origin2 string
		quantity1, quantity2, quantity3        int64
		lastHour, nextHour, lastDay, nextDay   time.Time
	)
	BeforeEach(func() {
		tuid = test.OwnerID()
		product = test.ProductID()
		domain = test.DomainID()
		origin = test.OriginID()
		origin2 = test.OriginID()

		quantity1 = 1
		quantity2 = 2
		quantity3 = 3

		lastHour = Clock().NowAdd(-time.Hour)
		nextHour = Clock().NowAdd(time.Hour)
		lastDay = Clock().NowAdd(-time.Hour * 24)
		nextDay = Clock().NowAdd(time.Hour * 24)
	})

	FunctionalsWithMockedEntsDAO()
	Describe("Consume with deletion for ConsumeStrategy Quantity_Requested", func() {
		// Test will make believe there are 2 entitlements with each a quantity of 3.
		// On deletion, it will say one had a quantity of 1 left, the other was already deleted.
		// It should kickoff the rollback
		NBeforeEach("*", func() {
			ents := []*entsdb.Entitlement{
				{
					OwnerID:   tuid,
					ID:        origin,
					ProductID: product,
					DomainID:  domain,
					Origin:    origin,
					Start:     &lastHour,
					End:       &nextHour,
					Quantity:  &quantity3,
				}, {
					OwnerID:   tuid,
					ID:        origin2,
					ProductID: product,
					DomainID:  domain,
					Origin:    origin2,
					Start:     &lastDay,
					End:       &nextDay,
					Quantity:  &quantity3,
				},
			}
			MockedEntitlementsDAOInterface().GetEntitlementsReturns(ents, nil)

			ent := &entsdb.Entitlement{
				Quantity: &quantity1,
			}
			MockedEntitlementsDAOInterface().DeleteEntitlementReturnsOnCall(0, ent, nil)
			MockedEntitlementsDAOInterface().DeleteEntitlementReturnsOnCall(1, nil, nil)
		})

		NIt("*", "Should rollback deletion when quantity changed", func() {
			By("Not consuming requested quantity")
			consuming := quantity3
			resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_QUANTITY_REQUESTED)
			Expect(err).To(BeNil())
			expected := int64(0)
			consumed := resultConsume.GetQuantityConsumed()
			Expect(&consumed).To(test.QuantityEq(&expected))

			By("Recreating items that got deleted")
			Expect(MockedEntitlementsDAOInterface().CreateEntitlementCallCount()).To(Equal(1))
			_, calledOwner, calledProduct, calledDomain, calledOrigin, calledStart, calledEnd, calledQuantity := MockedEntitlementsDAOInterface().CreateEntitlementArgsForCall(0)
			ent := []*hats_api.Entitlement{
				{
					Owner:     calledOwner,
					Id:        origin2,
					ProductId: calledProduct,
					Domain:    calledDomain,
					Origin:    calledOrigin,
					Start:     twents.TimeToProtobufTime(calledStart),
					End:       twents.TimeToProtobufTime(calledEnd),
					Quantity:  twents.Ivalue(calledQuantity),
				},
			}
			Expect(ent).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
		})
	})

	FunctionalsWithMockedEntsDAO()
	Describe("Consume with consumption for ConsumeStrategy Quantity_Requested", func() {
		// Test will make believe there are 2 entitlements with each a quantity of 3.
		// On consumption, it will say each had a quantity of 1 consumed, the other none.
		// It should kickoff the rollback
		NBeforeEach("*", func() {
			ents := []*entsdb.Entitlement{
				{
					OwnerID:   tuid,
					ID:        origin,
					ProductID: product,
					DomainID:  domain,
					Origin:    origin,
					Start:     &lastHour,
					End:       &nextHour,
					Quantity:  &quantity3,
				}, {
					OwnerID:   tuid,
					ID:        origin2,
					ProductID: product,
					DomainID:  domain,
					Origin:    origin2,
					Start:     &lastDay,
					End:       &nextDay,
					Quantity:  &quantity3,
				},
			}
			MockedEntitlementsDAOInterface().GetEntitlementsReturns(ents, nil)

			MockedEntitlementsDAOInterface().ConsumeEntitlementForQuantityReturnsOnCall(0, 1, nil)
			MockedEntitlementsDAOInterface().ConsumeEntitlementForQuantityReturnsOnCall(1, 0, nil)
		})

		NIt("*", "Should rollback consumption when quantity changed", func() {
			By("Not consuming requested quantity")
			consuming := quantity2
			resultConsume, err := consumeEntitlementsByOPD(tuid, product, domain, consuming, hats_api.ConsumePriority_EXPIRES_FIRST, hats_api.ConsumeStrategy_QUANTITY_REQUESTED)
			Expect(err).To(BeNil())
			expected := int64(0)
			consumed := resultConsume.GetQuantityConsumed()
			Expect(&consumed).To(test.QuantityEq(&expected))

			By("Recreating items that got consumed")
			Expect(MockedEntitlementsDAOInterface().CreateEntitlementCallCount()).To(Equal(1))
			_, calledOwner, calledProduct, calledDomain, calledOrigin, calledStart, calledEnd, calledQuantity := MockedEntitlementsDAOInterface().CreateEntitlementArgsForCall(0)
			ent := []*hats_api.Entitlement{
				{
					Owner:     calledOwner,
					Id:        origin2,
					ProductId: calledProduct,
					Domain:    calledDomain,
					Origin:    calledOrigin,
					Start:     twents.TimeToProtobufTime(calledStart),
					End:       twents.TimeToProtobufTime(calledEnd),
					Quantity:  twents.Ivalue(calledQuantity),
				},
			}
			Expect(ent).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, &lastHour, &nextHour, &quantity1))
		})
	})
})
