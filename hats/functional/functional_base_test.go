package functional_test

import (
	"context"
	"testing"
	"time"

	"net/http"
	"net/http/httptest"

	"log"

	"code.justin.tv/commerce/hats/internal/app"
	"code.justin.tv/commerce/hats/internal/clients"
	entsdb "code.justin.tv/commerce/hats/internal/db/entsdb"
	mock_ents_db "code.justin.tv/commerce/hats/internal/db/entsdb/mocks"
	"code.justin.tv/commerce/hats/internal/handlers/twirp/twents"
	"code.justin.tv/commerce/hats/internal/setup"
	"code.justin.tv/commerce/hats/pkg/logrusadapter"
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"

	"os"
	"regexp"

	"fmt"

	"net/url"

	"os/exec"
	"path"

	"code.justin.tv/commerce/hats/test"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/onsi/ginkgo"
)

type EntitlementsClientFunc func() hats_api.Entitlements
type HttpClientFunc func() *http.Client
type Shutdown func() error

var (
	EntitlementsClient EntitlementsClientFunc
	HttpClient         HttpClientFunc
	Shutdowns          []Shutdown
	clock              test.ITestingClock
	edao               entsdb.EntitlementsDAOInterface
)

func Clock() test.ITestingClock {
	return clock
}

func TestMain(m *testing.M) {
	Shutdowns = make([]Shutdown, 0, 8)
	if runIntegrations("*") {
		SetupIntegrations()
	} else {
		SetupFunctionals()
	}

	code := m.Run()

	for _, shutdown := range Shutdowns {
		err := shutdown()
		if err != nil {
			log.Fatal(err)
		}
	}

	os.Exit(code)
}

func TestSuite(t *testing.T) {
	RegisterFailHandler(ginkgo.Fail)
	ginkgo.RunSpecs(t, "Functional Suite")
}

type LocalDirectServer struct {
	Mux     http.Handler
	BaseURL string
}

func (this *LocalDirectServer) RoundTrip(request *http.Request) (*http.Response, error) {
	rw := httptest.NewRecorder()
	this.Mux.ServeHTTP(rw, request)
	rw.Flush()
	return rw.Result(), nil
}

func (this *LocalDirectServer) HttpClient() *http.Client {
	return &http.Client{Transport: this}
}

func NewLocalDirectServer(c *clients.Clients, a *app.App) (*LocalDirectServer, error) {
	handler := setup.HTTPHandler(c, a)
	return &LocalDirectServer{
		Mux:     handler,
		BaseURL: "http://localhost",
	}, nil
}

func stv(s *string) *hats_api.StringValue {
	if s == nil {
		return nil
	}
	return &hats_api.StringValue{Value: *s}
}

func getEntitlementsWithClient(user string, productId, domain, origin *string, eClient hats_api.Entitlements) (*hats_api.GetEntitlementsResponse, error) {
	req := &hats_api.GetEntitlementsRequest{
		Keys: []*hats_api.EntitlementKey{
			{
				OwnerId:   user,
				ProductId: stv(productId),
				Domain:    stv(domain),
				Origin:    stv(origin),
			},
		},
	}
	return eClient.GetEntitlements(context.Background(), req)
}

func getEntitlements(user string, productId, domain, origin *string) (*hats_api.GetEntitlementsResponse, error) {
	return getEntitlementsWithClient(user, productId, domain, origin, EntitlementsClient())
}

func getEntitlementsNoErr(user string, productId, domain, origin *string) []*hats_api.Entitlement {
	result, err := getEntitlementsWithClient(user, productId, domain, origin, EntitlementsClient())
	Expect(err).To(BeNil())
	return result.Entitlements
}

func listEntitlementsByPDNoErr(productID, domainID string) []*hats_api.Entitlement {
	response, err := listEntitlementsByPD(productID, domainID)
	if err != nil {
		return nil
	}
	return response.Entitlements
}

func listEntitlementsByPD(productID, domainID string) (*hats_api.ListEntitlementsByProductDomainResponse, error) {
	request := &hats_api.ListEntitlementsByProductDomainRequest{
		ProductId: productID,
		DomainId:  domainID,
	}
	return EntitlementsClient().ListEntitlementsByProductDomain(context.Background(), request)
}

func assertCreateEntitlement(user, productId, domain, origin string, start, end *time.Time, quantity *int64) *hats_api.Entitlement {
	result, err := createEntitlement(user, productId, domain, origin, start, end, quantity)
	Expect(err).To(BeNil())
	Expect(len(result.Entitlements)).To(BeEquivalentTo(1))
	return result.Entitlements[0]
}

func createEntitlement(user, productId, domain, origin string, start, end *time.Time, quantity *int64) (*hats_api.CreateEntitlementResponse, error) {
	request := &hats_api.CreateEntitlementRequest{
		Entitlements: []*hats_api.Entitlement{
			{
				Owner:     user,
				ProductId: productId,
				Domain:    domain,
				Origin:    origin,
				Start:     twents.TimeToProtobufTime(start),
				End:       twents.TimeToProtobufTime(end),
				Quantity:  twents.Ivalue(quantity),
			},
		},
	}
	return EntitlementsClient().CreateEntitlement(context.Background(), request)
}

func deleteEntitlementByOPDO(ownerId, productId, domainId, originId string) (*hats_api.DeleteEntitlementsByOPDOResponse, error) {
	request := &hats_api.DeleteEntitlementsByOPDORequest{
		Deletes: []*hats_api.EntitlementOPDO{
			{
				OwnerId:   ownerId,
				ProductId: productId,
				Domain:    domainId,
				Origin:    originId,
			},
		},
	}
	return EntitlementsClient().DeleteEntitlementsByOPDO(context.Background(), request)
}

func consumeEntitlementsByOPD(ownerId, productId, domain string, quantity int64, consumePriority hats_api.ConsumePriority, consumeStrategy hats_api.ConsumeStrategy) (*hats_api.ConsumeEntitlementsByOPDResponse, error) {
	request := &hats_api.ConsumeEntitlementsByOPDRequest{
		Consume: &hats_api.EntitlementOPD{
			OwnerId:   ownerId,
			ProductId: productId,
			Domain:    domain,
		},
		Quantity:        quantity,
		ConsumePriority: consumePriority,
		ConsumeStrategy: consumeStrategy,
	}
	return EntitlementsClient().ConsumeEntitlementsByOPD(context.Background(), request)
}

func assertHATSError(err error, errorCode string) {
	var clientError hats_api.ClientError
	parseErr := hats_api.ParseClientError(err, &clientError)
	Expect(parseErr).To(BeNil())

	Expect(clientError.ErrorCode).To(BeEquivalentTo(errorCode))
}

type UrlAmmendingRoundTripper struct {
	BaseUrl *url.URL
	RTBase  http.RoundTripper
}

func NewUrlAmmendingRoundTripper(baseURL string) *UrlAmmendingRoundTripper {
	u, err := url.Parse(baseURL)
	if err != nil {
		log.Fatal(err)
	}
	return &UrlAmmendingRoundTripper{
		BaseUrl: u,
		RTBase:  &http.Transport{},
	}
}

func (this *UrlAmmendingRoundTripper) RoundTrip(request *http.Request) (*http.Response, error) {
	request.URL.Host = this.BaseUrl.Host
	request.URL.Scheme = this.BaseUrl.Scheme
	request.URL.RawPath = fmt.Sprintf("%s%s", this.BaseUrl.RawPath, request.URL.RawPath)
	return this.RTBase.RoundTrip(request)
}

func SetupFunctionals() {
	tddb := go_test_dynamo.Instance()
	Shutdowns = append(Shutdowns, func() error {
		return tddb.Shutdown()
	})
}

func FunctionalsWithMockedEntsDAO() {
	functionals(true)
}

func Functionals() {
	functionals(false)
}

func functionals(mockedEntsDao bool) {
	ginkgo.BeforeEach(func() {
		if runIntegrations("*") {
			return
		}
		clock = test.NewTestingClock()

		if mockedEntsDao {
			edao = &mock_ents_db.FakeEntitlementsDAOInterface{}
		} else {
			localEDAO, err := entsdb.NewEntitlementsDAO(go_test_dynamo.Instance().Dynamo)
			if err != nil {
				panic(err)
			}
			err = localEDAO.CreateTable()
			if err != nil {
				panic(err)
			}
			edao = localEDAO
		}

		statter, err := statsd.NewNoopClient()
		if err != nil {
			panic(err)
		}

		c := &clients.Clients{
			EntitlementsDB: edao,
			Statter:        statter,
			Logger:         logrusadapter.New(logrus.New()),
		}

		a, err := app.InitializeApp(c)
		if err != nil {
			panic(err)
		}

		lds, err := NewLocalDirectServer(c, a)
		if err != nil {
			log.Fatal(err)
		}
		HttpClient = func() *http.Client {
			return lds.HttpClient()
		}
		EntitlementsClient = func() hats_api.Entitlements {
			return hats_api.NewEntitlementsProtobufClient(lds.BaseURL, lds.HttpClient())
		}
	})
	ginkgo.AfterEach(func() {
		if runIntegrations("*") {
			return
		}
		go_test_dynamo.Instance().Cleanup()
	})
}

func MockedEntitlementsDAOInterface() *mock_ents_db.FakeEntitlementsDAOInterface {
	if dao, ok := edao.(*mock_ents_db.FakeEntitlementsDAOInterface); ok {
		return dao
	}
	return nil
}

func SetupBootstrappedIntegration() string {
	curdir, err := os.Getwd()
	if err != nil {
		log.Fatal("error getting wd", err)
	}

	binary := os.Getenv("INTEGRATION_BINARY")
	if binary == "" {
		binary = path.Join(os.Getenv("GOPATH"), "bin", "hats")
	}

	cmd := exec.Cmd{
		Dir:    path.Dir(curdir),
		Path:   binary,
		Env:    nil,
		Args:   nil,
		Stdin:  nil,
		Stderr: os.Stderr,
		Stdout: os.Stdout,
	}
	go func() {
		err := cmd.Start()
		if err != nil {
			log.Fatal(err)
		}
	}()

	Shutdowns = append(Shutdowns, func() error {
		return cmd.Process.Kill()
	})

	return "http://localhost:8000"
}

func SetupIntegrations() {
	bootstrap := os.Getenv("INTEGRATION_BOOTSTRAP")
	endpoint := ""
	if bootstrap != "" {
		endpoint = SetupBootstrappedIntegration()
	} else {
		endpoint = os.Getenv("INTEGRATION_SERVER_ENDPOINT")
	}

	if endpoint == "" {
		log.Fatal("You must set INTEGRATION_SERVER_ENDPOINT or INTEGRATION_BOOTSTRAP when running integration tests")
	}

	HttpClient = func() *http.Client {
		return &http.Client{
			Transport: NewUrlAmmendingRoundTripper(endpoint),
		}
	}
	EntitlementsClient = func() hats_api.Entitlements {
		return hats_api.NewEntitlementsProtobufClient(endpoint, HttpClient())
	}
	clock = test.NewNoopTestingClock()

	// Waiting for server to start
	for i := 0; i < 20; i++ {
		req, err := http.NewRequest(http.MethodGet, "/health", nil)
		res, err := HttpClient().Do(req)
		if err == nil && res.StatusCode == 200 {
			return
		}
		time.Sleep(1000 * time.Millisecond)
	}
	log.Fatal("Timed out acquiring server")
}

// GINKGO Integration test integration

// Add some functions in ginkgo dsl style methods to differentiate
// things that should or shouldn't happen in integration mode

func runIntegrations(environments string) bool {
	env := os.Getenv("INTEGRATION_TEST_ENVIRONMENT")
	execute := false
	if env == "" {
		execute = false
	} else if environments == "*" {
		execute = true
	} else if environments == env {
		execute = true
	} else if match, err := regexp.MatchString(environments, env); match && err == nil {
		execute = true
	} else {
		execute = false
	}
	return execute
}

// Don't run this test in integration mode (only in functional mode)
func NIt(environments, text string, body interface{}, timeout ...float64) bool {
	if runIntegrations(environments) {
		log.Printf("Skipping %s, not marked for integration environment %s", text, environments)
		return true
	}
	return It(text, body, timeout...)
}

// Run this before each only in functional testing
func NBeforeEach(environments string, body interface{}, timeout ...float64) bool {
	if runIntegrations(environments) {
		return true
	}
	return ginkgo.BeforeEach(body, timeout...)
}

func BeforeEach(body interface{}, timeout ...float64) bool {
	return ginkgo.BeforeEach(body, timeout...)
}

func BeforeEachAlways(body interface{}, timeout ...float64) bool {
	return ginkgo.BeforeEach(body, timeout...)
}

func AfterEach(body interface{}, timeout ...float64) bool {
	if runIntegrations("*") {
		return true
	}
	return ginkgo.AfterEach(body, timeout...)
}

// Ginkgo Wrappers
// To make functional and integration tests seamless we
// create our own wrapper for Ginkgo functions and copy the dsl
// below. If there is a function missing from ginkgo, shim it below.

func Describe(text string, body func()) bool {
	return ginkgo.DescribeSkip(text, 1, body)
}

func It(text string, body interface{}, timeout ...float64) bool {
	return ginkgo.ItSkip(text, 1, body, timeout...)
}

func PIt(text string, body ...interface{}) bool {
	return ginkgo.PItSkip(text, 1, body...)
}

func By(text string) {
	ginkgo.By(text)
}
