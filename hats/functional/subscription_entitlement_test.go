package functional_test

import (
	"time"

	"code.justin.tv/commerce/hats/test"
	. "github.com/onsi/gomega"
)

var _ = Describe("Subscription Entitlements", func() {
	var (
		tuid, product, domain, origin           string
		tomorrow, yesterday, nextWeek, lastWeek time.Time
	)
	Functionals()

	BeforeEach(func() {
		tuid = test.OwnerID()
		product = test.ProductID()
		domain = test.DomainID()
		origin = test.OriginID()

		tomorrow = Clock().NowAdd(time.Hour * 48)
		yesterday = Clock().NowAdd(-time.Hour * 48)
		nextWeek = Clock().NowAdd(-time.Hour * 24 * 7)
		lastWeek = Clock().NowAdd(-time.Hour * 24 * 7)
	})

	Describe("working dynamo", func() {
		Describe("Started Non Expired Entitlement", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, &yesterday, &tomorrow, nil)
				Expect(err).To(BeNil())
			})
			It("Should have entitlement", func() {
				resp, err := getEntitlements(tuid, &product, &domain, nil)
				Expect(err).To(BeNil())
				Expect(resp.Entitlements).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, &yesterday, &tomorrow, nil))
			})
		})

		Describe("Not Started Entitlement", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, &tomorrow, nil, nil)
				Expect(err).To(BeNil())
			})
			It("Should not have entitlement", func() {
				result, err := getEntitlements(tuid, &product, &domain, &origin)

				Expect(err).To(BeNil())
				Expect(result).ToNot(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
		})

		Describe("Not Started, Not Expired Entitlement", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, &tomorrow, &nextWeek, nil)
				Expect(err).To(BeNil())
			})
			It("Should not have entitlement", func() {
				result, err := getEntitlements(tuid, &product, &domain, &origin)

				Expect(err).To(BeNil())
				Expect(result).ToNot(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
		})

		Describe("Expired Entitlement", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, &lastWeek, &yesterday, nil)
				Expect(err).To(BeNil())
			})
			It("Should not have entitlement", func() {
				result, err := getEntitlements(tuid, &product, &domain, &origin)

				Expect(err).To(BeNil())
				Expect(result).ToNot(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
		})

		Describe("Expired w/o Start", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, nil, &yesterday, nil)
				Expect(err).To(BeNil())
			})
			It("Should not have entitlement", func() {
				result, err := getEntitlements(tuid, &product, &domain, &origin)

				Expect(err).To(BeNil())
				Expect(result).ToNot(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
		})
	})
})
