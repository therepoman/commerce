package functional_test

import (
	"fmt"
	"strings"
	"time"

	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/commerce/hats/test"
	"github.com/onsi/gomega/types"
)

func streq(s string, sp *string) bool {
	if sp == nil {
		return false
	}
	actual := *sp
	return s == actual
}

func EquivalentEntitlement(owner, productID, domainID, origin string, start, end *time.Time, quantity *int64) types.GomegaMatcher {
	return &equivalentEntitlementMatcher{
		owner:     owner,
		domainID:  domainID,
		productID: productID,
		origin:    origin,
		start:     start,
		end:       end,
		quantity:  quantity,
		fails:     make([]string, 0, 5),
	}
}

type equivalentEntitlementMatcher struct {
	owner     string
	domainID  string
	productID string
	origin    string
	start     *time.Time
	end       *time.Time
	quantity  *int64
	fails     []string
}

func (matcher *equivalentEntitlementMatcher) Match(actual interface{}) (success bool, err error) {
	ent, ok := actual.(*hats_api.Entitlement)
	if !ok {
		return false, fmt.Errorf("EquivalentEntitlement matcher expects an *hats_api.Entitlement")
	}

	if ent == nil {
		matcher.fails = append(matcher.fails, "\t*Expected entitement to not be nil")
	}

	if ent.Owner != matcher.owner {
		matcher.fails = append(matcher.fails, fmt.Sprintf("\t*Expected owner to be %s not %s", matcher.owner, ent.Owner))
	}

	if ent.Domain != matcher.domainID {
		matcher.fails = append(matcher.fails, fmt.Sprintf("\t*Expected domain to be %s not %s", matcher.domainID, ent.Domain))
	}

	if ent.ProductId != matcher.productID {
		matcher.fails = append(matcher.fails, fmt.Sprintf("\t*Expected product to be %s not %s", matcher.productID, ent.ProductId))
	}

	if ent.Origin != matcher.origin {
		matcher.fails = append(matcher.fails, fmt.Sprintf("\t*Expected origin to be %s not %s", matcher.origin, ent.Origin))
	}

	startMatcher := test.TimeEq(matcher.start)
	matched, err := startMatcher.Match(ent.Start)
	if err != nil {
		matcher.fails = append(matcher.fails, fmt.Sprintf("Failed match on start time: %s", err))
	} else if !matched {
		matcher.fails = append(matcher.fails, fmt.Sprintf("Failed match on start time: %s", startMatcher.FailureMessage(nil)))
	}

	endMatcher := test.TimeEq(matcher.start)
	matched, err = endMatcher.Match(ent.Start)
	if err != nil {
		matcher.fails = append(matcher.fails, fmt.Sprintf("Failed match on start time: %s", err))
	} else if !matched {
		matcher.fails = append(matcher.fails, fmt.Sprintf("Failed match on start time: %s", endMatcher.FailureMessage(nil)))
	}

	quantityMatcher := test.QuantityEq(matcher.quantity)
	matched, err = quantityMatcher.Match(ent.Quantity)
	if err != nil {
		matcher.fails = append(matcher.fails, fmt.Sprintf("Failed match on quantity: %s", err))
	} else if !matched {
		matcher.fails = append(matcher.fails, fmt.Sprintf("Failed match on quantity: %s", quantityMatcher.FailureMessage(nil)))
	}

	return len(matcher.fails) == 0, nil
}

func (matcher *equivalentEntitlementMatcher) FailureMessage(actual interface{}) string {
	return fmt.Sprintf("Expected entitlement to be equivalent but wasn't: %s", strings.Join(matcher.fails, "\n"))
}

func (matcher *equivalentEntitlementMatcher) NegatedFailureMessage(actual interface{}) string {
	return "Expected entitlements not to be equal, but were"
}

func OnlyEquivalentEntitlement(owner, productID, domainID, origin string, start, end *time.Time, quantity *int64) types.GomegaMatcher {
	return &onlyEquivalentEntitlementMatcher{
		owner:          owner,
		domainID:       domainID,
		productID:      productID,
		origin:         origin,
		start:          start,
		end:            end,
		quantity:       quantity,
		failureMessage: "",
	}
}

type onlyEquivalentEntitlementMatcher struct {
	owner          string
	domainID       string
	productID      string
	origin         string
	start          *time.Time
	end            *time.Time
	quantity       *int64
	failureMessage string
}

func (matcher *onlyEquivalentEntitlementMatcher) Match(actual interface{}) (success bool, err error) {
	ents, ok := actual.([]*hats_api.Entitlement)
	if !ok {
		return false, fmt.Errorf("OnlyEquivalentEntitlement matcher expects an []*hats_api.Entitlement")
	}

	l := len(ents)
	if l != 1 {
		return false, fmt.Errorf("Expected only one entitlement, got %d", l)
	}

	equivalentEntitlementMatcher := EquivalentEntitlement(matcher.owner, matcher.productID, matcher.domainID, matcher.origin, matcher.start, matcher.end, matcher.quantity)
	matched, err := equivalentEntitlementMatcher.Match(ents[0])
	matcher.failureMessage = equivalentEntitlementMatcher.FailureMessage(nil)
	return matched, err
}

func (matcher *onlyEquivalentEntitlementMatcher) FailureMessage(actual interface{}) string {
	return fmt.Sprintf("Expected entitlement to be equivalent but wasn't: %s", matcher.failureMessage)
}

func (matcher *onlyEquivalentEntitlementMatcher) NegatedFailureMessage(actual interface{}) string {
	return "Expected entitlements not to be equal, but were"
}

func ExistingEntitlementInList(owner, productID, domainID, origin string, start, end *time.Time, quantity *int64) types.GomegaMatcher {
	return &existingEntitlementInList{
		owner:          owner,
		domainID:       domainID,
		productID:      productID,
		origin:         origin,
		start:          start,
		end:            end,
		quantity:       quantity,
		failureMessage: "",
	}
}

type existingEntitlementInList struct {
	owner          string
	domainID       string
	productID      string
	origin         string
	start          *time.Time
	end            *time.Time
	quantity       *int64
	failureMessage string
}

func (matcher *existingEntitlementInList) Match(actual interface{}) (success bool, err error) {
	ents, ok := actual.([]*hats_api.Entitlement)
	if !ok {
		return false, fmt.Errorf("ExistingEntitlementInList matcher expects an []*hats_api.Entitlement")
	}

	for _, ent := range ents {
		equivalentEntitlementMatcher := EquivalentEntitlement(matcher.owner, matcher.productID, matcher.domainID, matcher.origin, matcher.start, matcher.end, matcher.quantity)
		matched, err := equivalentEntitlementMatcher.Match(ent)
		if matched || err != nil {
			return matched, err
		}
	}
	return false, nil
}

func (matcher *existingEntitlementInList) FailureMessage(actual interface{}) string {
	return fmt.Sprintf("Expected entitlement to be found in list but wasn't: %s", matcher.failureMessage)
}

func (matcher *existingEntitlementInList) NegatedFailureMessage(actual interface{}) string {
	return "Expected entitlement not to be found, but was"
}
