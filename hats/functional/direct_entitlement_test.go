package functional_test

import (
	hats_api "code.justin.tv/commerce/hats/rpc/twirp/entitlements"
	"code.justin.tv/commerce/hats/test"
	. "github.com/onsi/gomega"
)

var _ = Describe("Direct Entitlements", func() {
	var (
		tuid, product, domain, origin string
	)
	BeforeEach(func() {
		tuid = test.OwnerID()
		product = test.ProductID()
		domain = test.DomainID()
		origin = test.OriginID()
	})

	Functionals()

	Describe("User", func() {
		Describe("No Entitlement", func() {
			It("Should not have entitlement", func() {
				result, err := getEntitlements(tuid, &product, &domain, &origin)

				Expect(err).To(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
			It("Should not find entitlements by PD", func() {
				result, err := listEntitlementsByPD(product, domain)
				Expect(err).To(BeNil())
				Expect(result.Entitlements).To(BeEmpty())
			})
		})

		Describe("With Direct Entitlement", func() {
			BeforeEach(func() {
				_, err := createEntitlement(tuid, product, domain, origin, nil, nil, nil)
				Expect(err).To(BeNil())
			})

			It("Should have entitlement", func() {
				Expect(getEntitlementsNoErr(tuid, &product, &domain, nil)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
			Describe("deleting", func() {
				It("should return the deleted item", func() {
					response, err := deleteEntitlementByOPDO(tuid, product, domain, origin)
					Expect(err).To(BeNil())

					Expect(len(response.Deleted)).To(BeEquivalentTo(1))
					Expect(response.Deleted[0]).To(BeEquivalentTo(&hats_api.EntitlementOPDO{
						OwnerId:   tuid,
						ProductId: product,
						Domain:    domain,
						Origin:    origin,
					}))
				})
			})
			It("Should find entitlement by PD", func() {
				Expect(listEntitlementsByPDNoErr(product, domain)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
		})

		Describe("With different origins", func() {
			origin2 := test.OriginID()
			BeforeEach(func() {
				assertCreateEntitlement(tuid, product, domain, origin, nil, nil, nil)
				assertCreateEntitlement(tuid, product, domain, origin2, nil, nil, nil)
			})
			It("Should only delete one entitlement", func() {
				_, err := deleteEntitlementByOPDO(tuid, product, domain, origin2)
				Expect(err).To(BeNil())

				Expect(getEntitlementsNoErr(tuid, &product, &domain, nil)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
			It("Should only get one entitlement with full match", func() {
				Expect(getEntitlementsNoErr(tuid, &product, &domain, &origin)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
		})

		Describe("With different productIds", func() {
			product2 := test.ProductID()
			BeforeEach(func() {
				assertCreateEntitlement(tuid, product, domain, origin, nil, nil, nil)
				assertCreateEntitlement(tuid, product2, domain, origin, nil, nil, nil)
			})
			It("Should only delete one entitlement", func() {
				_, err := deleteEntitlementByOPDO(tuid, product2, domain, origin)
				Expect(err).To(BeNil())

				Expect(getEntitlementsNoErr(tuid, &product, &domain, nil)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
			It("Should only get one entitlement with full match", func() {
				Expect(getEntitlementsNoErr(tuid, &product, &domain, &origin)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
		})

		Describe("With different domains", func() {
			domain2 := test.DomainID()
			BeforeEach(func() {
				assertCreateEntitlement(tuid, product, domain, origin, nil, nil, nil)
				assertCreateEntitlement(tuid, product, domain2, origin, nil, nil, nil)
			})
			It("Should only delete one entitlement", func() {
				_, err := deleteEntitlementByOPDO(tuid, product, domain2, origin)
				Expect(err).To(BeNil())

				Expect(getEntitlementsNoErr(tuid, &product, &domain, nil)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
			It("Should only get one entitlement with full match", func() {
				Expect(getEntitlementsNoErr(tuid, &product, &domain, &origin)).To(OnlyEquivalentEntitlement(tuid, product, domain, origin, nil, nil, nil))
			})
		})
	})

	Describe("Multiple users same product", func() {
		var (
			tuid2 string
		)
		BeforeEachAlways(func() {
			tuid2 = test.OwnerID()
		})
		BeforeEach(func() {
			assertCreateEntitlement(tuid, product, domain, origin, nil, nil, nil)
			assertCreateEntitlement(tuid2, product, domain, origin, nil, nil, nil)
		})

		Describe("List by PD", func() {
			It("Should have both entitlements", func() {
				results := listEntitlementsByPDNoErr(product, domain)
				Expect(len(results)).To(Equal(2))
			})
		})
	})
})
