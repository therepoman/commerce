# The newline at the end of this file is extremely important.  Cron won't run without it.
0 23 * * * root /usr/sbin/logrotate /usr/etc/logrotate.conf > /dev/null
