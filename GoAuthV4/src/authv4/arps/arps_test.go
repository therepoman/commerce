package arps

// Review code coverage in: build/brazil-documentation/coverage

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"encoding/base64"

	"net/http"
	"net/http/httptest"

	"code.justin.tv/commerce/GoAuthV4/src/authv4"

	"code.justin.tv/commerce/OdinLocalGoClient/src/odin"
)

var (

	// set to true for verbose logging
	debug = false

	// enable tests, init() will set to false if we cannot setup
	arpsEnabled = true
	initErrMsg  = "" // save here so we can use t.Log()

	region       = "us-east-1"
	service      = "maestroproxy"                      // your  ARPS whitelisted service here
	arpsMset     = "com.amazon.vpc.maestroproxy.creds" // your service's IAM account creds here
	arpsEndpoint = "aatb.us-east-1.amazonaws.com"

	// list the client IAM accountIds for whitelisting, set to nil if no whitelisting
	whitelist = []string{"797360199340"} // for this test this is the IAM account above for the service (as it is being used for client to test)

	accessKey, secretKey string // set by Odin

	arpsAuthz                 *ARPSAuthorizer
	arpsAuthzSpecialWhitelist *ARPSAuthorizer

	mockARPSEndpointPort     = 6666
	mockARPSEndpointCertFile = "./testdata/cert.pem"
	mockARPSEndpointKeyFile  = "./testdata/key.pem"
)

func init() {

	var err error

	accessKey, secretKey, err = odin.CredentialPair(arpsMset)
	if err != nil {
		initErrMsg = err.Error()
		arpsEnabled = false
		return
	}

	arpsAuthz = NewARPSAuthorizer(region, service, accessKey, secretKey, arpsEndpoint, whitelist)

	// has whitelist but with fake ids to generate whitelist failures
	arpsAuthzSpecialWhitelist = NewARPSAuthorizer(region, service, accessKey, secretKey, arpsEndpoint, []string{"foo"})

	// start a mock endpoint using https
	go http.ListenAndServeTLS(fmt.Sprintf("localhost:%d", mockARPSEndpointPort), mockARPSEndpointCertFile, mockARPSEndpointKeyFile, nil)
}

type TestLogger struct {
	logger *log.Logger
}

func NewTestLogger() *TestLogger {
	return &TestLogger{log.New(os.Stdout, "[test] ", 0)}
}
func (l *TestLogger) Debugf(format string, params ...interface{}) {
	if debug {
		l.logger.Printf(format, params...)
	}
}

func arpsTestsEnabled(t *testing.T) bool {
	if !arpsEnabled {
		t.Log("ARPS tests not initialized, skipping")
	}
	return arpsEnabled
}

func TestARPSEnabled(t *testing.T) {

	if !arpsTestsEnabled(t) {
		t.Log("Error in init(): ", initErrMsg)
		return
	}

	if debug {
		_, today := authv4.NewSigningDay(time.Now())
		signingKey := authv4.GetSigningKey(today, secretKey, region, service)
		t.Log(fmt.Sprintf("Target Signing Key: %s\n", base64.StdEncoding.EncodeToString(signingKey)))
	}

}

func TestARPSAuthenticate(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	var au *authv4.AuthenticatedClient // used to test caching

	logger := NewTestLogger()
	arpsAuthz.SetDebugLogger(logger)

	// exercise logger
	arpsAuthz.DebugLoggerDisable()
	if arpsAuthz.DebugLoggerIsEnabled() {
		t.Error(errors.New("Logger should be disabled"))
	}
	arpsAuthz.DebugLoggerEnable()
	if !arpsAuthz.DebugLoggerIsEnabled() {
		t.Error(errors.New("Logger should be enabled"))
	}

	handler := func(w http.ResponseWriter, r *http.Request) {

		var err error

		success := "true"

		// validate the request
		au, err = arpsAuthz.Authenticate(r)
		if err != nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	// do it 10 times, tests caching
	for i := 0; i < 10; i++ {

		req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
		if err != nil {
			t.Fatal(err)
		}

		signer.Sign(req)

		w := httptest.NewRecorder()
		handler(w, req)

		if w.Code != http.StatusOK {
			t.Fatal("Bad status: ", w.Code)
		}

		success := w.Body.String()

		if success != "true" {
			t.Error("Error, round trip failed: ", success)
		}

	}

	// test that we re-used the same cached Authenticated User
	if au == nil || au.RequestCount() < 2 {
		t.Error("Caching failed")
	}

}

func TestARPSWhitelist(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	logger := NewTestLogger()
	arpsAuthzSpecialWhitelist.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := arpsAuthzSpecialWhitelist.Authenticate(r)
		if err != nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
	if err != nil {
		t.Error(err)
	}

	signer.Sign(req)

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != 401 {
		t.Fatal("Bad status: ", w.Code)
	}
}

func TestARPSUnAuthenticated(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	logger := NewTestLogger()
	arpsAuthz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := arpsAuthz.Authenticate(r)
		if err != nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	// fake account, should not work
	signer := authv4.NewSigner(region, service, "TestAccessKey", "bar", nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
	if err != nil {
		t.Fatal(err)
	}

	signer.Sign(req)

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != 401 {
		t.Fatal("Bad status: ", w.Code)
	}

}

func TestARPSUnsigned(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	logger := NewTestLogger()
	arpsAuthz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := arpsAuthz.Authenticate(r)
		if err != nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
	if err != nil {
		t.Fatal(err)
	}

	// DO NOT SIGN

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != 401 {
		t.Fatal("Bad status: ", w.Code)
	}

}

func TestARPSExpires(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	var au *authv4.AuthenticatedClient // used to test caching

	logger := NewTestLogger()
	arpsAuthz.SetDebugLogger(logger)

	// exercise cache (and reset)
	cacheSize := 20
	arpsAuthz.SetCacheExpiration(time.Hour * 24)
	if arpsAuthz.CacheExpiration() != time.Hour*24 {
		t.Error(errors.New("Cache expiration is not correct"))
	}
	arpsAuthz.SetCacheSize(cacheSize) // resets, needed for below test
	if arpsAuthz.CacheCapacity() != cacheSize {
		t.Error(errors.New("Cache size is not correct"))
	}
	if arpsAuthz.CacheSize() != 0 { // should be empty
		t.Error(errors.New("Cache size is not correct"))
	}

	handler := func(w http.ResponseWriter, r *http.Request) {

		var err error

		success := "true"

		// validate the request
		au, err = arpsAuthz.Authenticate(r)
		if err != nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	// do it 10 times, tests caching
	for i := 0; i < 10; i++ {

		req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
		if err != nil {
			t.Fatal(err)
		}

		signer.Sign(req)

		// expire immediately!
		req.Header.Add("X-Amz-Expires", "0")

		w := httptest.NewRecorder()
		handler(w, req)

		if w.Code != http.StatusOK {
			t.Fatal("Bad status: ", w.Code)
		}

		success := w.Body.String()

		if success != "true" {
			t.Error("Error, round trip failed: ", success)
		}
	}

	// test that we did NOT re-use the same cached Authenticated User
	if au == nil || au.RequestCount() > 1 {
		t.Error("Caching failed. Count should be 1 was ", au.RequestCount())
	}

}

func TestARPSBadEndpoint1(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	// host does not exist...
	badAuthz := NewARPSAuthorizer(region, service, accessKey, secretKey, "badendpoint.amazon.com", nil)

	logger := NewTestLogger()
	badAuthz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		var err error

		success := "true"

		// validate the request
		_, err = badAuthz.Authenticate(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
	if err != nil {
		t.Fatal(err)
	}

	signer.Sign(req)

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusUnauthorized || !strings.Contains(w.Body.String(), "no such host") {
		t.Fatal("Bad status: ", w.Code, " ", w.Body.String())
	}

}

func TestARPSBadEndpoint2(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	// bad url
	badurl := `foo.com/?@#$%^&+=/,?><";:\|][{} =@#$%^&+=/,?><";:\|][{}`
	badAuthz := NewARPSAuthorizer(region, service, accessKey, secretKey, badurl, nil)

	logger := NewTestLogger()
	badAuthz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		var err error

		success := "true"

		// validate the request
		_, err = badAuthz.Authenticate(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		// send it
		w.Write([]byte(success))
	}

	signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
	if err != nil {
		t.Fatal(err)
	}

	signer.Sign(req)

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusUnauthorized || !strings.Contains(w.Body.String(), "invalid URL") {
		t.Fatal("Bad status: ", w.Code, " ", w.Body.String())
	}

}

func badJson(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "This is not JSON!")
}

func badAccountId(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "{}")
}

func badCryptoOffloadKey(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `{ "Subject": { "RootUser":{ "Id":"1234"} } }`)
}

func TestARPSBadEndpoint3(t *testing.T) {

	if !arpsTestsEnabled(t) {
		return
	}

	// URLs to use to generate bad output
	errCases := []string{"badJson", "badAccountId", "badCryptoOffloadKey"}
	// Expected fragments of error messages
	errMsg := []string{"invalid character", "Cannot find accountId", "Cannot find signing key"}

	// for each case above create a handler that will provide a repsonse to introduce the error
	http.HandleFunc("/badJson/rpc", badJson)
	http.HandleFunc("/badAccountId/rpc", badAccountId)
	http.HandleFunc("/badCryptoOffloadKey/rpc", badCryptoOffloadKey)

	for i, errCase := range errCases {

		badurl := fmt.Sprintf("localhost:%d/%s", mockARPSEndpointPort, errCase)
		badAuthz := NewARPSAuthorizer(region, service, accessKey, secretKey, badurl, nil)

		logger := NewTestLogger()
		badAuthz.SetDebugLogger(logger)

		handler := func(w http.ResponseWriter, r *http.Request) {

			var err error

			success := "true"

			// validate the request
			_, err = badAuthz.Authenticate(r)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}

			// send it
			w.Write([]byte(success))
		}

		signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)

		signer.SetDebugLogger(logger)

		req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
		if err != nil {
			t.Fatal(err)
		}

		signer.Sign(req)

		w := httptest.NewRecorder()
		handler(w, req)

		if w.Code != http.StatusUnauthorized || !strings.Contains(w.Body.String(), errMsg[i]) {
			t.Error(errCase, "- Bad status: ", w.Code, " ", w.Body.String())
		}

	}
}
