package authv4

import (
	"reflect"
	"sync"
)

// This is an interface for writing debug logs using Printf style function
type DebugLogger interface {
	Debugf(format string, params ...interface{})
}

// Logger object that you can enable/disable
type Logger struct {
	lock        sync.RWMutex // to protect internal volatile structures
	debug       bool
	debugLogger DebugLogger
}

func NewLogger() *Logger {
	return &Logger{sync.RWMutex{}, false, nil}
}

//  Passing an object that implements Debugf() as the debugLogger will log detail information during Authenticate()
func (logger *Logger) SetDebugLogger(debugLogger DebugLogger) {

	// Lock WHOLE function
	logger.lock.Lock()
	defer logger.lock.Unlock()

	// detect nil loggers (which is expensive so we have the 'debug' flag too)
	logger.debug = false
	if debugLogger != nil && !reflect.ValueOf(debugLogger).IsNil() {
		logger.debugLogger = debugLogger
		logger.debug = true
	} else {
		logger.debugLogger = nil
	}
}

func (logger *Logger) DebugLoggerEnable() {

	// Lock WHOLE function
	logger.lock.Lock()
	defer logger.lock.Unlock()

	if logger.debugLogger != nil && !reflect.ValueOf(logger.debugLogger).IsNil() {
		logger.debug = true
	}
}

func (logger *Logger) DebugLoggerDisable() {
	logger.debug = false
}

func (logger *Logger) DebugLoggerIsEnabled() bool {
	return logger.debug
}

func (logger *Logger) Debugf(format string, params ...interface{}) {
	if logger.debug {

		// START CRITICAL SECTION
		logger.lock.Lock()

		if logger.debug { // must check again!
			logger.debugLogger.Debugf(format, params...)
		}

		logger.lock.Unlock()
		// END CRITICAL SECTION

	}
}
