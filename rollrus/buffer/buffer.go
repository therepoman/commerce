package buffer

import (
	"io"

	"code.justin.tv/commerce/logrus"
)

type Buffer interface {
	io.Closer
	Next() bool
	Push(entry *logrus.Entry)
	Value() *logrus.Entry
}
