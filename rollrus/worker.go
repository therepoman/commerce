package rollrus

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/pkg/errors"
	log "code.justin.tv/commerce/logrus"
	"github.com/stvp/roll"
)

// wellKnownErrorFields are fields that are expected to be of type `error`
// in priority order.
var wellKnownErrorFields = []string{
	"err", "error",
}

type job struct {
	client roll.Client
	entry  *log.Entry
}

func (j job) sendToRollbar() {
	entry := j.entry

	if entry == nil {
		return
	}

	cause, trace := extractError(entry.Data)
	if cause == nil {
		cause = fmt.Errorf(entry.Message)
	}

	m := convertFields(entry.Data)
	if _, exists := m["time"]; !exists {
		m["time"] = entry.Time.Format(time.RFC3339)
	}

	err := j.report(entry, cause, m, trace)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not send entry to rollbar: %v\n", err)
	}
}

func (j *job) report(entry *log.Entry, cause error, m map[string]string, trace []uintptr) (err error) {
	if entry.Level == log.FatalLevel ||
		entry.Level == log.PanicLevel ||
		entry.Level == log.ErrorLevel ||
		entry.Level == log.WarnLevel {

		e := fmt.Errorf(entry.Message)

		if len(trace) == 0 {
			switch entry.Level {
			case log.FatalLevel, log.PanicLevel:
				_, err = j.client.Critical(e, m)
			case log.ErrorLevel:
				_, err = j.client.Error(e, m)
			case log.WarnLevel:
				_, err = j.client.Warning(e, m)
			}
		} else {
			switch entry.Level {
			case log.FatalLevel, log.PanicLevel:
				_, err = j.client.CriticalStack(e, trace, m)
			case log.ErrorLevel:
				_, err = j.client.ErrorStack(e, trace, m)
			case log.WarnLevel:
				_, err = j.client.WarningStack(e, trace, m)
			}
		}
	} else {
		switch entry.Level {
		case log.InfoLevel:
			_, err = j.client.Info(entry.Message, m)
		case log.DebugLevel:
			_, err = j.client.Debug(entry.Message, m)
		default:
			err = fmt.Errorf("Unknown level: %s", entry.Level)
		}
	}
	return err
}

type worker struct {
	workerPool chan chan job
	jobChannel chan job
	shutDown   chan struct{}
	wg         *sync.WaitGroup
}

func newWorker(workerPool chan chan job, shutdown chan struct{}, wg *sync.WaitGroup) *worker {
	return &worker{
		workerPool: workerPool,
		shutDown:   shutdown,
		jobChannel: make(chan job),
		wg:         wg,
	}
}

func (w *worker) Work() {
	go func() {
		defer w.wg.Done()
		for {
			w.workerPool <- w.jobChannel
			select {
			case job := <-w.jobChannel:
				job.sendToRollbar()
			case <-w.shutDown:
				return
			}
		}
	}()
}

// extractError attempts to extract an error from a well known field, err or error
func extractError(fields log.Fields) (cause error, trace []uintptr) {
	type stackTracer interface {
		StackTrace() errors.StackTrace
	}

	for _, f := range wellKnownErrorFields {
		e, ok := fields[f]
		if !ok {
			continue
		}
		err, ok := e.(error)
		if !ok {
			continue
		}

		cause = errors.Cause(err)
		tracer, ok := err.(stackTracer)
		if ok {
			trace = copyStackTrace(tracer.StackTrace())
		}
		return
	}
	return
}

func copyStackTrace(trace errors.StackTrace) (out []uintptr) {
	for _, frame := range trace {
		out = append(out, uintptr(frame))
	}
	return
}
