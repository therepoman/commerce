package backend

import (
	"errors"
	"io"
	"net/http"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/aws/sns"
	sqsWorker "code.justin.tv/commerce/gogogadget/aws/sqs/worker"
	"code.justin.tv/commerce/phanto/backend/api/admin_get_key_status"
	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/create_key_pool"
	"code.justin.tv/commerce/phanto/backend/api/create_product"
	"code.justin.tv/commerce/phanto/backend/api/create_product_type"
	"code.justin.tv/commerce/phanto/backend/api/generate_key_batch"
	"code.justin.tv/commerce/phanto/backend/api/generate_key_pool_report"
	"code.justin.tv/commerce/phanto/backend/api/get_all_key_pools"
	"code.justin.tv/commerce/phanto/backend/api/get_all_product_types"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batch_download_info"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batch_status"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batches"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batches_s2s"
	"code.justin.tv/commerce/phanto/backend/api/get_key_handler_group"
	"code.justin.tv/commerce/phanto/backend/api/get_key_handler_groups"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_report_download_info"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_reports"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pools_by_handler"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status_base"
	"code.justin.tv/commerce/phanto/backend/api/get_products"
	"code.justin.tv/commerce/phanto/backend/api/helix_get_key_status"
	"code.justin.tv/commerce/phanto/backend/api/helix_redeem_key"
	"code.justin.tv/commerce/phanto/backend/api/helix_redeem_key_by_batch"
	"code.justin.tv/commerce/phanto/backend/api/invalidate_key_batch"
	"code.justin.tv/commerce/phanto/backend/api/invalidate_key_pool"
	"code.justin.tv/commerce/phanto/backend/api/invalidate_keys"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_base"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_for_load_testing"
	"code.justin.tv/commerce/phanto/backend/api/update_key_batch"
	"code.justin.tv/commerce/phanto/backend/api/update_key_handler_group"
	"code.justin.tv/commerce/phanto/backend/api/update_key_pool"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/clients/datascience"
	"code.justin.tv/commerce/phanto/backend/clients/petozi"
	"code.justin.tv/commerce/phanto/backend/clients/redis"
	dynamo_call_audit_record "code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	dynamo_key "code.justin.tv/commerce/phanto/backend/dynamo/key"
	dynamo_key_batch "code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	dynamo_key_handler_group "code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	dynamo_key_pool "code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	dynamo_key_pool_metadata "code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	dynamo_product "code.justin.tv/commerce/phanto/backend/dynamo/product"
	dynamo_product_type "code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	dynamo_report "code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/backend/eligibility"
	"code.justin.tv/commerce/phanto/backend/geoip/loader"
	"code.justin.tv/commerce/phanto/backend/geoip/locator"
	"code.justin.tv/commerce/phanto/backend/geoip/refresher"
	"code.justin.tv/commerce/phanto/backend/idempotency"
	"code.justin.tv/commerce/phanto/backend/key"
	key_authorization "code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/backend/key/maker"
	"code.justin.tv/commerce/phanto/backend/key_batch"
	key_batch_authorization "code.justin.tv/commerce/phanto/backend/key_batch/authorization"
	"code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/backend/key_pool"
	key_pool_authorization "code.justin.tv/commerce/phanto/backend/key_pool/authorization"
	"code.justin.tv/commerce/phanto/backend/product"
	"code.justin.tv/commerce/phanto/backend/product_type"
	"code.justin.tv/commerce/phanto/backend/server"
	"code.justin.tv/commerce/phanto/backend/tenants"
	"code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/backend/worker"
	"code.justin.tv/commerce/phanto/backend/worker/key_generation"
	"code.justin.tv/commerce/phanto/backend/worker/report_generation"
	"code.justin.tv/commerce/phanto/config"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	log "github.com/sirupsen/logrus"
)

const (
	localhostServerEndpoint    = "http://localhost:8000"
	defaultNumberOfConnections = 200
)

type Backend struct {
	// RPC Server
	Server phanto.Phanto `inject:""`

	// SQS Workers
	KeyGenerationWorker    *key_generation.Worker    `inject:""`
	ReportGenerationWorker *report_generation.Worker `inject:""`

	// Config
	Config *config.Config `inject:""`

	// Stats
	Stats statsd.Statter `inject:""`

	// SQS Client
	SQS sqsiface.SQSAPI `inject:""`

	// Spade Client
	SpadeClient datascience.SpadeClient `inject:""`

	// Worker Terminators
	terminators []worker.NamedTerminator
}

func NewBackend(cfg *config.Config) (*Backend, error) {
	var backend Backend

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetricsConfig != nil {
		flusher := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.AWSRegion,
			ServiceName:       "phanto",
			Stage:             cfg.EnvironmentName,
			FlushPeriod:       30 * time.Second,
			BufferSize:        cfg.CloudwatchMetricsConfig.BufferSize,
			AggregationPeriod: time.Minute,
			Substage:          "primary",
		}, map[string]bool{})

		statters = append(statters, flusher)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	keyPoolDAO := dynamo_key_pool.NewDAO(sess, cfg.DynamoSuffix)
	keyBatchDAO := dynamo_key_batch.NewDAO(sess, cfg.DynamoSuffix)
	keyDAO := dynamo_key.NewDAO(sess, cfg.DynamoSuffix)
	productDAO := dynamo_product.NewDAO(sess, cfg.DynamoSuffix)
	productTypeDAO := dynamo_product_type.NewDAO(sess, cfg.DynamoSuffix)
	callAuditRecordDAO := dynamo_call_audit_record.NewDAO(sess, cfg.DynamoSuffix)
	reportDAO := dynamo_report.NewDAO(sess, cfg.DynamoSuffix)
	keyPoolMetadataDAO := dynamo_key_pool_metadata.NewDAO(sess, cfg.DynamoSuffix)
	keyHandlerGroupDAO := dynamo_key_handler_group.NewDAO(sess, cfg.DynamoSuffix)

	geoIPRefresher := refresher.NewGeoIPRefresher()

	redisClient := redis.NewClient(*cfg)
	petoziClient := petozi.NewClient(*cfg)
	spadeClient, err := datascience.NewSpadeClient(cfg.EnvironmentName, stats)
	if err != nil {
		return nil, err
	}

	usersServiceClient, err := usersclient_internal.NewClient(twitchclient.ClientConf{
		Host: cfg.UserServiceEndpoint,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: defaultNumberOfConnections,
		},
	})
	if err != nil {
		return nil, err
	}

	var graph inject.Graph
	err = graph.Provide(
		// Backend (graph root)
		&inject.Object{Value: &backend},

		// Config
		&inject.Object{Value: cfg},

		// RPC Server
		&inject.Object{Value: &server.Server{}},

		// Stats
		&inject.Object{Value: stats},

		// APIs
		&inject.Object{Value: &create_product.API{}},
		&inject.Object{Value: &create_product_type.API{}},
		&inject.Object{Value: &create_key_pool.API{}},
		&inject.Object{Value: &generate_key_batch.API{}},
		&inject.Object{Value: &get_key_batch_status.API{}},
		&inject.Object{Value: &redeem_key_base.APIImpl{}},
		&inject.Object{Value: &redeem_key.API{}},
		&inject.Object{Value: &redeem_key_for_load_testing.API{}},
		&inject.Object{Value: &get_key_status_base.APIImpl{}},
		&inject.Object{Value: &get_key_status.API{}},
		&inject.Object{Value: &invalidate_keys.API{}},
		&inject.Object{Value: &invalidate_key_batch.API{}},
		&inject.Object{Value: &invalidate_key_pool.API{}},
		&inject.Object{Value: &get_key_batch_download_info.API{}},
		&inject.Object{Value: &get_all_key_pools.API{}},
		&inject.Object{Value: &get_key_batches.API{}},
		&inject.Object{Value: &get_key_batches_s2s.API{}},
		&inject.Object{Value: &get_key_pools_by_handler.API{}},
		&inject.Object{Value: &get_all_product_types.API{}},
		&inject.Object{Value: &get_products.API{}},
		&inject.Object{Value: &get_key_pool_reports.API{}},
		&inject.Object{Value: &generate_key_pool_report.API{}},
		&inject.Object{Value: &get_key_pool_report_download_info.API{}},
		&inject.Object{Value: &update_key_pool.API{}},
		&inject.Object{Value: &update_key_batch.API{}},
		&inject.Object{Value: &helix_get_key_status.API{}},
		&inject.Object{Value: &helix_redeem_key.API{}},
		&inject.Object{Value: &get_key_handler_group.API{}},
		&inject.Object{Value: &get_key_handler_groups.API{}},
		&inject.Object{Value: &update_key_handler_group.API{}},
		&inject.Object{Value: &base.Container{}},
		&inject.Object{Value: &redeem_key_by_batch_base.APIImpl{}},
		&inject.Object{Value: &helix_redeem_key_by_batch.API{}},
		&inject.Object{Value: &admin_get_key_status.API{}},

		// AWS Clients
		&inject.Object{Value: sns.NewDefaultClient()},
		&inject.Object{Value: NewSQSClient(sess, cfg)},
		&inject.Object{Value: s3.NewDefaultClient()},
		&inject.Object{Value: kms.NewDefaultKMSClient()},

		// Dynamo Tables
		&inject.Object{Value: keyBatchDAO},
		&inject.Object{Value: keyPoolDAO},
		&inject.Object{Value: keyDAO},
		&inject.Object{Value: productDAO},
		&inject.Object{Value: productTypeDAO},
		&inject.Object{Value: callAuditRecordDAO},
		&inject.Object{Value: reportDAO},
		&inject.Object{Value: keyPoolMetadataDAO},
		&inject.Object{Value: keyHandlerGroupDAO},

		// Key
		&inject.Object{Value: key.NewClaimer()},
		&inject.Object{Value: &maker.KeyMakerImpl{}},
		&inject.Object{Value: key_set.NewChecker()},
		&inject.Object{Value: key_set.NewEditor()},

		// Validators
		&inject.Object{Value: key.NewValidator(), Name: "keyValidator"},
		&inject.Object{Value: key_batch.NewValidator(), Name: "keyBatchValidator"},
		&inject.Object{Value: key_pool.NewValidator(), Name: "keyPoolValidator"},
		&inject.Object{Value: product.NewValidator(), Name: "productValidator"},
		&inject.Object{Value: product_type.NewValidator(), Name: "productTypeValidator"},

		// Authorization
		&inject.Object{Value: key_batch_authorization.NewHandlerAuthorizer()},
		&inject.Object{Value: key_pool_authorization.NewUserAuthorizer(), Name: "keyPoolUserAuthorizer"},
		&inject.Object{Value: key_pool_authorization.NewHandlerAuthorizer()},
		&inject.Object{Value: key_authorization.NewAuthorizer()},

		// Auditor
		&inject.Object{Value: &audit.DynamoAuditor{}},

		// SQS workers
		&inject.Object{Value: &key_generation.Worker{}},
		&inject.Object{Value: &report_generation.Worker{}},

		// GeoIP
		&inject.Object{Value: geoIPRefresher},
		&inject.Object{Value: &loader.MaxMindS3Loader{}},
		&inject.Object{Value: &locator.MaxMindLocator{}},

		// Clients
		&inject.Object{Value: redisClient},
		&inject.Object{Value: usersServiceClient},
		&inject.Object{Value: petoziClient, Name: "bitsProductGetter"},
		&inject.Object{Value: spadeClient},

		// Throttler
		&inject.Object{Value: throttle.NewThrottler()},

		// Tenants
		&inject.Object{Value: tenants.NewTenantMap()},

		// Eligibility
		&inject.Object{Value: eligibility.NewEligibilityChecker()},

		// Idempotency
		&inject.Object{Value: idempotency.NewChecker()},
	)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	err = geoIPRefresher.Init()
	if err != nil {
		return nil, err
	}

	graceful.Go(geoIPRefresher.RefreshAtInterval)

	backend.setupWorkers()

	return &backend, nil
}

func (b *Backend) setupWorkers() {
	b.terminators = b.setupSQSWorker(b.terminators, b.Config.KeyGenerationJobQueueConfig, b.KeyGenerationWorker)
	b.terminators = b.setupSQSWorker(b.terminators, b.Config.ReportGenerationJobQueueConfig, b.ReportGenerationWorker)
}

func (b *Backend) setupSQSWorker(terminators []worker.NamedTerminator, queueConfig *config.QueueConfig, worker sqsWorker.Worker) []worker.NamedTerminator {
	if queueConfig != nil && queueConfig.Name != "" {
		terminators = append(terminators, sqsWorker.NewSQSWorkerManager(queueConfig.Name, queueConfig.NumWorkers, 1, b.SQS, worker, b.Stats))
	}
	return terminators
}

func (b *Backend) Ping(w http.ResponseWriter, r *http.Request) {
	phantoClient := phanto.NewPhantoProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err := phantoClient.HealthCheck(r.Context(), &phanto.HealthCheckReq{})
	if err != nil {
		log.WithError(err).Error("Could not ping phanto server")
		w.WriteHeader(http.StatusInternalServerError)
		_, innerErr := io.WriteString(w, err.Error())
		if innerErr != nil {
			log.WithError(innerErr).Error("Error writing http response")
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func NewSQSClient(sess *session.Session, cfg *config.Config) sqsiface.SQSAPI {
	return sqs.New(sess, &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
}
