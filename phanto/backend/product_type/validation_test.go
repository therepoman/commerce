package product_type

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a key validator", t, func() {
		dao := new(product_type_mock.DAO)

		v := &validator{
			DAO: dao,
		}

		productType := "1231231231123"
		snsTopic := "walrusTopic"

		Convey("When we error fetching the product from Dynamo", func() {
			dao.On("GetProductType", productType).Return(nil, errors.New("THE WALRUS STRIKES"))

			Convey("We should return an internal server error", func() {
				claimData, err := v.Validate(productType)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we do not have a record for the product in Dynamo", func() {
			dao.On("GetProductType", productType).Return(nil, nil)

			Convey("We should return a not found error", func() {
				claimData, err := v.Validate(productType)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we find the product record in Dynamo that is active", func() {
			productRecord := &product_type.ProductType{
				ProductType: productType,
				SNSTopic:    snsTopic,
				Description: "asdfasdf",
			}

			dao.On("GetProductType", productType).Return(productRecord, nil)

			Convey("We should return that it is valid", func() {
				claimData, err := v.Validate(productType)
				So(claimData.IsValid, ShouldBeTrue)
				So(claimData.ProductType, ShouldEqual, productType)
				So(claimData.SNSTopic, ShouldEqual, snsTopic)
				So(err, ShouldBeNil)
			})
		})
	})
}
