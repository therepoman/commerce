package product_type

import (
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"github.com/pkg/errors"
)

type Validator interface {
	Validate(productType string) (claimData data.Validation, err error)
}

type validator struct {
	DAO product_type.DAO `inject:""`
}

func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(productType string) (claimData data.Validation, err error) {
	claimData.IsValid = false

	dynamoProduct, err := v.DAO.GetProductType(productType)
	if err != nil {
		return claimData, errors.Wrap(err, "downstream dependency error getting product type")
	}
	if dynamoProduct == nil {
		return claimData, errors.New("no product type found")
	}

	claimData.IsValid = true
	claimData.ProductType = dynamoProduct.ProductType
	claimData.SNSTopic = dynamoProduct.SNSTopic

	return
}
