package key_pool

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	key_pool_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	product_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/product"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a key validator", t, func() {
		ctx := context.Background()
		dao := new(key_pool_mock.DAO)
		productValidator := new(product_mock.Validator)

		v := &validator{
			DAO:              dao,
			ProductValidator: productValidator,
		}

		keyPoolID := "1231231231123"
		productType := "asdfasdf"
		sku := "123123123123123"

		Convey("When we error fetching the key batch from Dynamo", func() {
			dao.On("GetKeyPool", keyPoolID).Return(nil, errors.New("THE WALRUS STRIKES"))

			Convey("We should return an internal server error", func() {
				claimData, err := v.Validate(ctx, keyPoolID)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we do not have a record for the key pool in Dynamo", func() {
			dao.On("GetKeyPool", keyPoolID).Return(nil, nil)

			Convey("We should return a not found error", func() {
				claimData, err := v.Validate(ctx, keyPoolID)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we find the key pool record in Dynamo that is not active", func() {
			keyPoolRecord := &key_pool.KeyPool{
				PoolID:        keyPoolID,
				KeyPoolStatus: status.KeyPoolStatusInactive,
				ProductType:   productType,
				SKU:           sku,
				StartDate:     time.Now().Add(-1 * time.Hour),
				EndDate:       time.Now().Add(1 * time.Hour),
			}

			dao.On("GetKeyPool", keyPoolID).Return(keyPoolRecord, nil)

			snsTopic := "walrusTopic"
			productDescription := "Spend a day with a walrus"

			productClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           snsTopic,
				ProductType:        productType,
				ProductDescription: productDescription,
				SKU:                sku,
			}

			dao.On("GetKeyPool", keyPoolID).Return(keyPoolRecord, nil)

			Convey("when we error validating the key pool associated with the key batch", func() {
				productClaimData.IsValid = false

				productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, keyPoolID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the key pool associated with it is invalid", func() {
				productClaimData.IsValid = false

				productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, keyPoolID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the key pool associated is valid", func() {
				productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, nil)

				Convey("We should return that it is not valid", func() {
					claimData, err := v.Validate(ctx, keyPoolID)

					So(claimData.IsValid, ShouldBeFalse)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, productDescription)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(claimData.KeyPoolID, ShouldEqual, keyPoolID)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("When we find the key pool record in Dynamo that can be claimed", func() {
			keyPoolRecord := &key_pool.KeyPool{
				PoolID:        keyPoolID,
				KeyPoolStatus: status.KeyPoolStatusActive,
				ProductType:   productType,
				SKU:           sku,
				StartDate:     time.Now().Add(-1 * time.Hour),
				EndDate:       time.Now().Add(1 * time.Hour),
			}

			snsTopic := "walrusTopic"
			productDescription := "Spend a day with a walrus"

			productClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           snsTopic,
				ProductType:        productType,
				ProductDescription: productDescription,
				SKU:                sku,
			}

			dao.On("GetKeyPool", keyPoolID).Return(keyPoolRecord, nil)

			Convey("when we error validating the key pool associated with the key batch", func() {
				productClaimData.IsValid = false

				productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, keyPoolID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the key pool associated with it is invalid", func() {
				productClaimData.IsValid = false

				productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, keyPoolID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the key pool associated is valid", func() {
				productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, nil)

				Convey("We should return that it is valid", func() {
					claimData, err := v.Validate(ctx, keyPoolID)

					So(claimData.IsValid, ShouldBeTrue)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, productDescription)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(claimData.KeyPoolID, ShouldEqual, keyPoolID)
					So(err, ShouldBeNil)
				})
			})

		})

		Convey("When dynamo returns a key pool record whose start time has not been reached", func() {
			keyPoolRecord := &key_pool.KeyPool{
				PoolID:        keyPoolID,
				KeyPoolStatus: status.KeyPoolStatusActive,
				ProductType:   productType,
				SKU:           sku,
				StartDate:     time.Now().Add(2 * time.Hour),
				EndDate:       time.Now().Add(3 * time.Hour),
			}
			dao.On("GetKeyPool", keyPoolID).Return(keyPoolRecord, nil)

			productClaimData := data.Validation{
				IsValid:     true,
				ProductType: productType,
				SKU:         sku,
			}
			productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, nil)

			Convey("We should return that it is not valid", func() {
				claimData, err := v.Validate(ctx, keyPoolID)
				So(err, ShouldBeNil)
				So(claimData.IsValid, ShouldBeFalse)
			})
		})

		Convey("When dynamo returns a key pool record whose end time has passed", func() {
			keyPoolRecord := &key_pool.KeyPool{
				PoolID:        keyPoolID,
				KeyPoolStatus: status.KeyPoolStatusActive,
				ProductType:   productType,
				SKU:           sku,
				StartDate:     time.Now().Add(-2 * time.Hour),
				EndDate:       time.Now().Add(-1 * time.Hour),
			}
			dao.On("GetKeyPool", keyPoolID).Return(keyPoolRecord, nil)

			productClaimData := data.Validation{
				IsValid:     true,
				ProductType: productType,
				SKU:         sku,
			}
			productValidator.On("Validate", ctx, productType, sku).Return(productClaimData, nil)

			Convey("We should return that it is not valid", func() {
				claimData, err := v.Validate(ctx, keyPoolID)
				So(err, ShouldBeNil)
				So(claimData.IsValid, ShouldBeFalse)
			})
		})
	})
}
