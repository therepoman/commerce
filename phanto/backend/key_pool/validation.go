package key_pool

import (
	"context"
	"time"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/product"
	"github.com/pkg/errors"
)

type Validator interface {
	Validate(ctx context.Context, keyPoolID string) (claimData data.Validation, err error)
}

type validator struct {
	ProductValidator product.Validator `inject:"productValidator"`
	DAO              key_pool.DAO      `inject:""`
}

func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(ctx context.Context, keyPoolID string) (claimData data.Validation, err error) {
	claimData.IsValid = false

	dynamoKeyPool, err := v.DAO.GetKeyPool(keyPoolID)
	if err != nil {
		return claimData, errors.Wrap(err, "downstream dependency error getting key pool")
	}
	if dynamoKeyPool == nil {
		return claimData, errors.New("no key pool found")
	}

	productClaimData, err := v.ProductValidator.Validate(ctx, dynamoKeyPool.ProductType, dynamoKeyPool.SKU)
	if err != nil || !productClaimData.IsValid {
		return claimData, err
	}

	claimData = productClaimData
	claimData.KeyPoolID = dynamoKeyPool.PoolID
	claimData.StartDate = dynamoKeyPool.StartDate
	claimData.EndDate = dynamoKeyPool.EndDate

	if !dynamoKeyPool.KeyPoolStatus.IsActive() {
		claimData.IsValid = false
		return claimData, nil
	}

	if dynamoKeyPool.StartDate.After(time.Now()) {
		claimData.IsValid = false
		return claimData, nil
	}

	if dynamoKeyPool.EndDate.Before(time.Now()) {
		claimData.IsValid = false
		return claimData, nil
	}

	return claimData, nil
}
