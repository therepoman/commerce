package authorization

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/authorization"
)

type UserAuthorizer interface {
	Authorize(ctx context.Context, userID string) bool
}

type userAuthorizer struct {
}

func NewUserAuthorizer() UserAuthorizer {
	return &userAuthorizer{}
}

func (uv *userAuthorizer) Authorize(ctx context.Context, userID string) bool {
	authedUserID, ok := ctx.Value(authorization.AuthenticatedUserIDContextKey).(string)
	if !ok {
		// no authed user, so is not authorized
		return false
	}

	return authedUserID == userID
}
