package authorization

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"github.com/go-errors/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHandlerAuthorizer_AuthorizeByTUID(t *testing.T) {
	Convey("Given a key pool handler authorizer", t, func() {
		keyHandlerDAO := new(key_handler_group_mock.DAO)

		authorizer := &handlerAuthorizer{
			KeyHandlerGroupDAO: keyHandlerDAO,
		}

		Convey("Given there is no authed user", func() {
			ctx := context.Background()

			Convey("then we return that the user is not authorized", func() {
				So(authorizer.AuthorizeByTUID(ctx, &key_pool.KeyPool{}), ShouldBeFalse)
			})
		})

		Convey("Given there is an authed user", func() {
			userID := "123123123"

			ctx := context.Background()
			ctx = context.WithValue(ctx, authorization.AuthenticatedUserIDContextKey, userID)

			Convey("When the key pool is nil", func() {

				Convey("then we return that the user is not authorized", func() {
					So(authorizer.AuthorizeByTUID(ctx, nil), ShouldBeFalse)
				})
			})

			Convey("when there is no handler", func() {
				keyPool := &key_pool.KeyPool{}

				Convey("then we return that the user is not authorized", func() {
					So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeFalse)
				})
			})

			Convey("when there is a legacy handler", func() {
				keyPool := &key_pool.KeyPool{
					Handler: userID,
				}

				Convey("when the user is not the key pool handler", func() {
					ctx = context.WithValue(ctx, authorization.AuthenticatedUserIDContextKey, "asdfasdfasdf")

					Convey("then we return that the user is not authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the user is the key pool handler", func() {
					ctx = context.WithValue(ctx, authorization.AuthenticatedUserIDContextKey, userID)

					Convey("then we return that the user is authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeTrue)
					})
				})
			})

			Convey("when there is a handler group id", func() {
				keyPool := &key_pool.KeyPool{
					HandlerGroupID: "test-group-1",
				}

				Convey("when the handler group dao errors", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(nil, errors.New("test error"))

					Convey("then we return that the user is not authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the handler group dao returns nil", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(nil, nil)

					Convey("then we return that the user is not authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the handler group dao returns a group without the user", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(&key_handler_group.KeyHandlerGroup{
						AuthorizedTUIDs: map[string]bool{"1": true, "2": true},
					}, nil)

					Convey("then we return that the user is not authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the handler group dao returns a group with the user", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(&key_handler_group.KeyHandlerGroup{
						AuthorizedTUIDs: map[string]bool{"1": true, userID: true, "3": true},
					}, nil)

					Convey("then we return that the user is authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyPool), ShouldBeTrue)
					})
				})
			})
		})
	})
}

func TestHandlerAuthorizer_AuthorizeByClientID(t *testing.T) {
	Convey("Given a key pool handler authorizer", t, func() {
		keyHandlerDAO := new(key_handler_group_mock.DAO)

		authorizer := &handlerAuthorizer{
			KeyHandlerGroupDAO: keyHandlerDAO,
		}

		Convey("Given there is no client id", func() {
			ctx := context.Background()

			Convey("then we return that the request is not authorized", func() {
				So(authorizer.AuthorizeByClientID(ctx, &key_pool.KeyPool{}), ShouldBeFalse)
			})
		})

		Convey("Given there is a client id", func() {
			clientID := "my-favorite-client"

			ctx := context.Background()
			ctx = context.WithValue(ctx, middleware.ClientIDContextKey, clientID)

			Convey("When the key pool is nil", func() {

				Convey("then we return that the request is not authorized", func() {
					So(authorizer.AuthorizeByClientID(ctx, nil), ShouldBeFalse)
				})
			})

			Convey("when there is no handler group id", func() {
				keyPool := &key_pool.KeyPool{}

				Convey("then we return that the client id is not authorized", func() {
					So(authorizer.AuthorizeByClientID(ctx, keyPool), ShouldBeFalse)
				})
			})

			Convey("when there is a handler group id", func() {
				keyPool := &key_pool.KeyPool{
					HandlerGroupID: "test-group-1",
				}

				Convey("when the handler group dao errors", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(nil, errors.New("test error"))

					Convey("then we return that the request is not authorized", func() {
						So(authorizer.AuthorizeByClientID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the handler group dao returns nil", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(nil, nil)

					Convey("then we return that the request is not authorized", func() {
						So(authorizer.AuthorizeByClientID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the handler group dao returns a group without the client id", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(&key_handler_group.KeyHandlerGroup{
						AuthorizedClientIDs: map[string]bool{"1": true, "2": true},
					}, nil)

					Convey("then we return that the request is not authorized", func() {
						So(authorizer.AuthorizeByClientID(ctx, keyPool), ShouldBeFalse)
					})
				})

				Convey("when the handler group dao returns a group with the client-id", func() {
					keyHandlerDAO.On("GetKeyHandlerGroup", "test-group-1").Return(&key_handler_group.KeyHandlerGroup{
						AuthorizedClientIDs: map[string]bool{"1": true, clientID: true, "3": true},
					}, nil)

					Convey("then we return that the request is authorized", func() {
						So(authorizer.AuthorizeByClientID(ctx, keyPool), ShouldBeTrue)
					})
				})
			})
		})
	})
}
