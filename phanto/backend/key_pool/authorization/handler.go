package authorization

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/middleware"
)

type HandlerAuthorizer interface {
	AuthorizeByClientID(ctx context.Context, keyPool *key_pool.KeyPool) bool
	AuthorizeByTUID(ctx context.Context, keyPool *key_pool.KeyPool) bool
}

type handlerAuthorizer struct {
	KeyHandlerGroupDAO key_handler_group.DAO `inject:""`
}

func NewHandlerAuthorizer() HandlerAuthorizer {
	return &handlerAuthorizer{}
}

func (a *handlerAuthorizer) AuthorizeByClientID(ctx context.Context, keyPool *key_pool.KeyPool) bool {
	clientID, ok := ctx.Value(middleware.ClientIDContextKey).(string)
	if !ok {
		// no client id, so is not authorized
		return false
	}

	if keyPool == nil {
		return false
	}

	if strings.Blank(keyPool.HandlerGroupID) {
		return false
	}

	group, err := a.KeyHandlerGroupDAO.GetKeyHandlerGroup(keyPool.HandlerGroupID)
	if err != nil {
		return false
	}

	if group == nil {
		return false
	}

	_, ok = group.AuthorizedClientIDs[clientID]
	return ok
}

func (a *handlerAuthorizer) AuthorizeByTUID(ctx context.Context, keyPool *key_pool.KeyPool) bool {
	authedUserID, ok := ctx.Value(authorization.AuthenticatedUserIDContextKey).(string)
	if !ok {
		// no authed user, so is not authorized
		return false
	}

	if keyPool == nil {
		return false
	}

	if strings.NotBlank(keyPool.HandlerGroupID) {
		group, err := a.KeyHandlerGroupDAO.GetKeyHandlerGroup(keyPool.HandlerGroupID)
		if err != nil {
			return false
		}

		if group == nil {
			return false
		}

		_, ok := group.AuthorizedTUIDs[authedUserID]
		return ok
	} else if strings.NotBlank(keyPool.Handler) {
		// This is the old method for authorizing key pool access
		// We still perform the check in order to be backwards compatible with legacy key pools
		return authedUserID == keyPool.Handler
	}

	return false
}
