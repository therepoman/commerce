package status

import (
	"errors"
)

const (
	KeyPoolStatusActive      = KeyPoolStatus("ACTIVE")
	KeyPoolStatusInactive    = KeyPoolStatus("INACTIVE")
	KeyPoolStatusInvalidated = KeyPoolStatus("INVALIDATED")
)

var Statuses = map[KeyPoolStatus]interface{}{
	KeyPoolStatusActive:      nil,
	KeyPoolStatusInactive:    nil,
	KeyPoolStatusInvalidated: nil,
}

type KeyPoolStatus string

func FromString(statusStr string) (KeyPoolStatus, error) {
	_, ok := Statuses[KeyPoolStatus(statusStr)]
	if !ok {
		return KeyPoolStatus(""), errors.New("invalid key pool status")
	}
	return KeyPoolStatus(statusStr), nil
}

func (s KeyPoolStatus) String() string {
	return string(s)
}

func (s KeyPoolStatus) IsActive() bool {
	return s != KeyPoolStatusInactive && s != KeyPoolStatusInvalidated
}
