package key_pool

import (
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/golang/protobuf/ptypes"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	getAllLimit = 10
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetKeyPool(poolID string) (*KeyPool, error)
	CreateNew(keyPool *KeyPool) error
	CreateOrUpdate(keyPool *KeyPool) error
	GetKeyPools(cursor string) ([]*KeyPool, string, error)
	GetKeyPoolsByHandler(handler string, cursor string) ([]*KeyPool, string, error)
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("key-pools-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type KeyPool struct {
	PoolID         string               `dynamo:"pool_id"`
	KeyPoolStatus  status.KeyPoolStatus `dynamo:"key_pool_status"`
	ProductType    string               `dynamo:"product_type"`
	SKU            string               `dynamo:"sku"`
	Description    string               `dynamo:"description"`
	Handler        string               `dynamo:"handler"`
	HandlerGroupID string               `dynamo:"handler_group_id"`
	RedemptionMode string               `dynamo:"redemption_mode"`
	StartDate      time.Time            `dynamo:"start_date"`
	EndDate        time.Time            `dynamo:"end_date"`
}

func (dao *daoImpl) GetKeyPool(poolID string) (*KeyPool, error) {
	var keyPool KeyPool
	err := dao.Get("pool_id", poolID).Consistent(true).One(&keyPool)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &keyPool, err
}

func (dao *daoImpl) CreateNew(keyPool *KeyPool) error {
	if keyPool == nil {
		return errors.New("cannot create nil key pool")
	}

	return dao.Put(*keyPool).If("attribute_not_exists(pool_id)").Run()
}

func (dao *daoImpl) CreateOrUpdate(keyPool *KeyPool) error {
	if keyPool == nil {
		return errors.New("cannot create/update nil key pool")
	}

	return dao.Put(*keyPool).Run()
}

func (dao *daoImpl) GetKeyPools(cursor string) ([]*KeyPool, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"pool_id": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	keyPools := make([]*KeyPool, 0, getAllLimit)
	newPagingKey, err := dao.Scan().
		StartFrom(pagingKey).
		Limit(getAllLimit).
		AllWithLastEvaluatedKey(&keyPools)
	if err != nil {
		return nil, "", err
	}

	var newCursor string
	poolIDAttribute, ok := newPagingKey["pool_id"]
	if ok && poolIDAttribute != nil {
		newCursor = pointers.StringOrDefault(poolIDAttribute.S, "")
	}

	return keyPools, newCursor, nil
}

func (dao *daoImpl) GetKeyPoolsByHandler(handler string, cursor string) ([]*KeyPool, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"handler": &dynamodb.AttributeValue{
				S: aws.String(handler),
			},
			"pool_id": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	keyPools := make([]*KeyPool, 0, getAllLimit)
	newPagingKey, err := dao.Get("handler", handler).
		Index(fmt.Sprintf("key-pools-handler-gsi-%s", dao.suffix)).
		StartFrom(pagingKey).
		Limit(getAllLimit).
		AllWithLastEvaluatedKey(&keyPools)
	if err != nil {
		return nil, "", err
	}

	var newCursor string
	poolIDAttribute, ok := newPagingKey["pool_id"]
	if ok && poolIDAttribute != nil {
		newCursor = pointers.StringOrDefault(poolIDAttribute.S, "")
	}

	return keyPools, newCursor, nil
}

func ToTwirpModel(keyPools []*KeyPool) []*phanto.KeyPool {
	resp := make([]*phanto.KeyPool, 0, len(keyPools))

	for _, keyPool := range keyPools {
		if keyPool != nil {
			startDate, err := ptypes.TimestampProto(keyPool.StartDate)
			if err != nil {
				log.WithError(err).WithField("pool_id", keyPool.PoolID).Error("invalid start date in dynamo record")
			}

			endDate, err := ptypes.TimestampProto(keyPool.EndDate)
			if err != nil {
				log.WithError(err).WithField("pool_id", keyPool.PoolID).Error("invalid end date in dynamo record")
			}

			redemptionMode := phanto.RedemptionMode_REDEEM_BY_KEY
			redemptionModeInt, ok := phanto.RedemptionMode_value[keyPool.RedemptionMode]
			if ok {
				redemptionMode = phanto.RedemptionMode(redemptionModeInt)
			}

			resp = append(resp, &phanto.KeyPool{
				ProductType:    keyPool.ProductType,
				Sku:            keyPool.SKU,
				Description:    keyPool.Description,
				PoolId:         keyPool.PoolID,
				Status:         keyPool.KeyPoolStatus.String(),
				Handler:        keyPool.Handler,
				StartDate:      startDate,
				EndDate:        endDate,
				HandlerGroupId: keyPool.HandlerGroupID,
				RedemptionMode: redemptionMode,
			})
		}
	}
	return resp
}
