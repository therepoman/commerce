package key_pool_test

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	. "github.com/smartystreets/goconvey/convey"
)

func TestKeyPoolDAO_CRUD(t *testing.T) {
	Convey("Given a key pool DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := key_pool.NewDAO(sess, dynamo.TestTablesSuffix)

		poolID := random.String(32)
		Convey("Doesn't find a non-existent key pool", func() {
			k, err := dao.GetKeyPool(poolID)
			So(err, ShouldBeNil)
			So(k, ShouldBeNil)
		})

		Convey("Succeeds on CreateNew", func() {
			kp1 := &key_pool.KeyPool{
				PoolID:        poolID,
				KeyPoolStatus: status.KeyPoolStatus(random.String(32)),
			}
			err := dao.CreateNew(kp1)
			So(err, ShouldBeNil)

			Convey("Errors on subsequent attempts to CreateNew the same item", func() {
				err = dao.CreateNew(kp1)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds on GetKeyPool", func() {
				actualKP1, err := dao.GetKeyPool(poolID)
				So(err, ShouldBeNil)
				So(reflect.DeepEqual(kp1, actualKP1), ShouldBeTrue)
			})

			Convey("Succeeds on CreateOrUpdate", func() {
				kp1.KeyPoolStatus = status.KeyPoolStatus(random.String(32))
				err = dao.CreateOrUpdate(kp1)
				So(err, ShouldBeNil)

				Convey("Succeeds on GetKeyPool", func() {
					actualKP1, err := dao.GetKeyPool(poolID)
					So(err, ShouldBeNil)
					So(reflect.DeepEqual(kp1, actualKP1), ShouldBeTrue)
				})
			})
		})
	})
}

func TestKeyPoolDAO_GetKeyPoolsByHandler(t *testing.T) {
	Convey("Given a key pool DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := key_pool.NewDAO(sess, dynamo.TestTablesSuffix)
		handler := random.String(16)

		Convey("GetKeyPoolsByHandler finds nothing for non-existent handler", func() {
			kps, _, err := dao.GetKeyPoolsByHandler(handler, "")
			So(err, ShouldBeNil)
			So(len(kps), ShouldEqual, 0)
		})

		Convey("When one pool is added with the handler", func() {
			kp1 := &key_pool.KeyPool{
				PoolID:        random.String(32),
				KeyPoolStatus: status.KeyPoolStatusActive,
				Handler:       handler,
			}
			err := dao.CreateNew(kp1)
			So(err, ShouldBeNil)

			Convey("GetKeyPoolsByHandler finds the one pool", func() {
				kps, _, err := dao.GetKeyPoolsByHandler(handler, "")
				So(err, ShouldBeNil)
				So(len(kps), ShouldEqual, 1)
			})

			Convey("When a second pool is added with the handler", func() {
				kp2 := &key_pool.KeyPool{
					PoolID:        random.String(32),
					KeyPoolStatus: status.KeyPoolStatusActive,
					Handler:       handler,
				}
				err := dao.CreateNew(kp2)
				So(err, ShouldBeNil)

				Convey("GetKeyBatchesByPool finds the two pools", func() {
					kps, _, err := dao.GetKeyPoolsByHandler(handler, "")
					So(err, ShouldBeNil)
					So(len(kps), ShouldEqual, 2)
				})

				Convey("When 10 more pools are added with the hanler", func() {
					for i := 0; i < 10; i++ {
						kp := &key_pool.KeyPool{
							PoolID:        random.String(32),
							KeyPoolStatus: status.KeyPoolStatusActive,
							Handler:       handler,
						}
						err := dao.CreateNew(kp)
						So(err, ShouldBeNil)
					}

					Convey("Two calls to GetKeyPoolsByHandler gets all pools", func() {
						pools1, cursor1, err := dao.GetKeyPoolsByHandler(handler, "")
						So(len(pools1), ShouldEqual, 10)
						So(err, ShouldBeNil)

						batches2, _, err := dao.GetKeyPoolsByHandler(handler, cursor1)
						So(len(batches2), ShouldEqual, 2)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
