package call_audit_record

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

const (
	PageSize = 10
	ttlDays  = 90
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetCallAuditRecord(callID string) (*CallAuditRecord, error)
	CreateNew(callAuditRecord *CallAuditRecord) error
	CreateOrUpdate(callAuditRecord *CallAuditRecord) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("call-audit-records-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type CallAuditRecord struct {
	CallID              string        `dynamo:"call_id"`
	Operation           string        `dynamo:"operation"`
	Timestamp           time.Time     `dynamo:"timestamp"`
	Latency             time.Duration `dynamo:"latency"`
	LDAPUser            string        `dynamo:"ldap_user"`
	AuthenticatedUserID string        `dynamo:"authenticated_user_id"`
	RequestHeadersJSON  string        `dynamo:"headers_json"`
	RequestJSON         string        `dynamo:"request_json"`
	ResponseJSON        string        `dynamo:"response_json"`
	ResponseError       string        `dynamo:"response_error"`
	TTL                 int64         `dynamo:"ttl"`
}

func (dao *daoImpl) GetCallAuditRecord(callID string) (*CallAuditRecord, error) {
	var callAuditRecord CallAuditRecord
	err := dao.Table.Get("call_id", callID).One(&callAuditRecord)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &callAuditRecord, err
}

func (dao *daoImpl) CreateNew(callAuditRecord *CallAuditRecord) error {
	if callAuditRecord == nil {
		return errors.New("cannot create nil call audit record")
	}

	if callAuditRecord.TTL <= 0 {
		callAuditRecord.TTL = callAuditRecord.Timestamp.AddDate(0, 0, ttlDays).Unix()
	}

	return dao.Table.Put(*callAuditRecord).If("attribute_not_exists(call_id)").Run()
}

func (dao *daoImpl) CreateOrUpdate(callAuditRecord *CallAuditRecord) error {
	if callAuditRecord == nil {
		return errors.New("cannot create/update nil call audit record")
	}
	if callAuditRecord.TTL <= 0 {
		callAuditRecord.TTL = callAuditRecord.Timestamp.AddDate(0, 0, ttlDays).Unix()
	}

	return dao.Table.Put(*callAuditRecord).Run()
}
