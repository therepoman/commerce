package key_handler_group

import (
	"errors"
	"fmt"
	"sort"

	"code.justin.tv/commerce/phanto/rpc"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

const (
	getAllLimit = 10
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetKeyHandlerGroup(handlerGroupID string) (*KeyHandlerGroup, error)
	CreateNew(keyHandlerGroup *KeyHandlerGroup) error
	CreateOrUpdate(keyHandlerGroup *KeyHandlerGroup) error
	GetKeyHandlerGroups(cursor string) ([]*KeyHandlerGroup, string, error)
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("key-handler-groups-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type KeyHandlerGroup struct {
	HandlerGroupID      string          `dynamo:"handler_group_id"`
	Description         string          `dynamo:"description"`
	AuthorizedTUIDs     map[string]bool `dynamo:"authorized_tuids"`
	AuthorizedClientIDs map[string]bool `dynamo:"authorized_client_ids"`
}

func (dao *daoImpl) GetKeyHandlerGroup(handlerGroupID string) (*KeyHandlerGroup, error) {
	var keyHandlerGroup KeyHandlerGroup
	err := dao.Get("handler_group_id", handlerGroupID).Consistent(true).One(&keyHandlerGroup)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &keyHandlerGroup, err
}

func (dao *daoImpl) CreateNew(keyHandlerGroup *KeyHandlerGroup) error {
	if keyHandlerGroup == nil {
		return errors.New("cannot create nil key handler group")
	}

	return dao.Put(*keyHandlerGroup).If("attribute_not_exists(handler_group_id)").Run()
}

func (dao *daoImpl) CreateOrUpdate(keyHandlerGroup *KeyHandlerGroup) error {
	if keyHandlerGroup == nil {
		return errors.New("cannot create/update nil key handler group")
	}

	return dao.Put(*keyHandlerGroup).Run()
}

func (dao *daoImpl) GetKeyHandlerGroups(cursor string) ([]*KeyHandlerGroup, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"handler_group_id": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	keyHandlerGroups := make([]*KeyHandlerGroup, 0, getAllLimit)
	newPagingKey, err := dao.Scan().
		StartFrom(pagingKey).
		Limit(getAllLimit).
		AllWithLastEvaluatedKey(&keyHandlerGroups)
	if err != nil {
		return nil, "", err
	}

	var newCursor string
	handlerGroupIDAttribute, ok := newPagingKey["handler_group_id"]
	if ok && handlerGroupIDAttribute != nil {
		newCursor = pointers.StringOrDefault(handlerGroupIDAttribute.S, "")
	}

	return keyHandlerGroups, newCursor, nil
}

func ToKeyHandlerGroup(group *KeyHandlerGroup) *phanto.KeyHandlerGroup {
	if group == nil {
		return nil
	}

	tuidList := make([]string, 0)
	for tuid := range group.AuthorizedTUIDs {
		tuidList = append(tuidList, tuid)
	}
	sort.Strings(tuidList)

	clientIDList := make([]string, 0)
	for clientID := range group.AuthorizedClientIDs {
		clientIDList = append(clientIDList, clientID)
	}
	sort.Strings(clientIDList)

	return &phanto.KeyHandlerGroup{
		HandlerGroupId:      group.HandlerGroupID,
		Description:         group.Description,
		AuthorizedTuids:     tuidList,
		AuthorizedClientIds: clientIDList,
	}
}

func ToKeyHandlerGroupsTwirpModel(groups []*KeyHandlerGroup) []*phanto.KeyHandlerGroup {
	resp := make([]*phanto.KeyHandlerGroup, 0, len(groups))
	for _, group := range groups {
		twirpGroup := ToKeyHandlerGroup(group)
		if twirpGroup == nil {
			continue
		}
		resp = append(resp, twirpGroup)
	}
	return resp
}
