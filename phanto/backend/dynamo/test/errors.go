package test

import (
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/pkg/errors"
)

func NewAwsErr(code string) awserr.RequestFailure {
	err := errors.New("WALRUS STRIKE")

	awsErr := awserr.New(code, "WALRUS STRIKE", err)

	return awserr.NewRequestFailure(awsErr, 400, "")
}
