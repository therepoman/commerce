package product_type

import (
	"errors"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

const (
	PageSize = 10
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetProductType(productType string) (*ProductType, error)
	CreateNew(productType *ProductType) error
	CreateOrUpdate(productType *ProductType) error
	GetProductTypes(cursor string) ([]*ProductType, string, error)
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("product-types-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type ProductType struct {
	ProductType string `dynamo:"product_type"`
	Description string `dynamo:"description"`
	SNSTopic    string `dynamo:"sns_topic"`
	TenantURL   string `dynamo:"tenant_url"`
}

func (dao *daoImpl) GetProductType(productType string) (*ProductType, error) {
	var pt ProductType
	err := dao.Get("product_type", productType).Consistent(true).One(&pt)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &pt, err
}

func (dao *daoImpl) CreateNew(productType *ProductType) error {
	if productType == nil {
		return errors.New("cannot create nil product type")
	}

	return dao.Put(*productType).If("attribute_not_exists(product_type)").Run()
}

func (dao *daoImpl) CreateOrUpdate(productType *ProductType) error {
	if productType == nil {
		return errors.New("cannot create/update nil product type")
	}

	return dao.Put(*productType).Run()
}

func (dao *daoImpl) GetProductTypes(cursor string) ([]*ProductType, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"product_type": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	productTypes := make([]*ProductType, 0, PageSize)
	newPagingKey, err := dao.Scan().
		StartFrom(pagingKey).
		Limit(PageSize).
		AllWithLastEvaluatedKey(&productTypes)
	if err != nil {
		return nil, "", err
	}

	var newCursor string
	productTypeAttribute, ok := newPagingKey["product_type"]
	if ok && productTypeAttribute != nil {
		newCursor = pointers.StringOrDefault(productTypeAttribute.S, "")
	}

	return productTypes, newCursor, nil
}

func ToTwirpModel(productTypes []*ProductType) []*phanto.ProductType {
	resp := make([]*phanto.ProductType, 0, len(productTypes))
	for _, productType := range productTypes {
		if productType != nil {
			resp = append(resp, &phanto.ProductType{
				ProductType: productType.ProductType,
				SnsTopic:    productType.SNSTopic,
				Description: productType.Description,
				TenantUrl:   productType.TenantURL,
			})
		}
	}
	return resp
}
