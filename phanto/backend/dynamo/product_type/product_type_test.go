package product_type_test

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	. "github.com/smartystreets/goconvey/convey"
)

func TestProductTypeDAO_CRUD(t *testing.T) {
	Convey("Given a product type DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := product_type.NewDAO(sess, dynamo.TestTablesSuffix)

		productType := random.String(32)
		Convey("Doesn't find a non-existent product type", func() {
			pt, err := dao.GetProductType(productType)
			So(err, ShouldBeNil)
			So(pt, ShouldBeNil)
		})

		Convey("Succeeds on CreateNew", func() {
			pt1 := &product_type.ProductType{
				ProductType: productType,
				Description: random.String(32),
			}
			err := dao.CreateNew(pt1)
			So(err, ShouldBeNil)

			Convey("Errors on subsequent attempts to CreateNew the same item", func() {
				err = dao.CreateNew(pt1)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds on GetProductType", func() {
				actualPT1, err := dao.GetProductType(productType)
				So(err, ShouldBeNil)
				So(reflect.DeepEqual(pt1, actualPT1), ShouldBeTrue)
			})

			Convey("Succeeds on CreateOrUpdate", func() {
				pt1.Description = random.String(32)
				err = dao.CreateOrUpdate(pt1)
				So(err, ShouldBeNil)

				Convey("Succeeds on GetProductType", func() {
					actualPT1, err := dao.GetProductType(productType)
					So(err, ShouldBeNil)
					So(reflect.DeepEqual(pt1, actualPT1), ShouldBeTrue)
				})
			})
		})
	})
}

func TestProductTypeDAO_GetProductTypes(t *testing.T) {
	Convey("Given a product type DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := product_type.NewDAO(sess, dynamo.TestTablesSuffix)

		Convey("Get Product Types call returns a page of product types", func() {
			productTypes1, cursor1, err1 := dao.GetProductTypes("")
			So(err1, ShouldBeNil)
			So(len(productTypes1), ShouldEqual, product_type.PageSize)
			So(cursor1, ShouldNotBeBlank)

			Convey("Get Product Types with cursor call returns the next page of product types", func() {
				productTypes2, cursor2, err2 := dao.GetProductTypes(cursor1)
				So(err2, ShouldBeNil)
				So(productTypes1[0].ProductType, ShouldNotEqual, productTypes2[0].ProductType)
				So(cursor1, ShouldNotEqual, cursor2)
			})
		})
	})
}
