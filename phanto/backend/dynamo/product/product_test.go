package product_test

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo"
	"code.justin.tv/commerce/phanto/backend/dynamo/product"
	. "github.com/smartystreets/goconvey/convey"
)

func TestProductDAO_CRUD(t *testing.T) {
	Convey("Given a product DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := product.NewDAO(sess, dynamo.TestTablesSuffix)

		productType := random.String(32)
		sku := random.String(32)
		Convey("Doesn't find a non-existent product", func() {
			p, err := dao.GetProduct(productType, sku)
			So(err, ShouldBeNil)
			So(p, ShouldBeNil)
		})

		Convey("Succeeds on CreateNew", func() {
			p1 := &product.Product{
				ProductType: productType,
				SKU:         sku,
				Description: random.String(32),
			}
			err := dao.CreateNew(p1)
			So(err, ShouldBeNil)

			Convey("Errors on subsequent attempts to CreateNew the same item", func() {
				err = dao.CreateNew(p1)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds on GetProduct", func() {
				actualP1, err := dao.GetProduct(productType, sku)
				So(err, ShouldBeNil)
				So(reflect.DeepEqual(p1, actualP1), ShouldBeTrue)
			})

			Convey("Succeeds on CreateOrUpdate", func() {
				p1.Description = random.String(32)
				err = dao.CreateOrUpdate(p1)
				So(err, ShouldBeNil)

				Convey("Succeeds on GetProduct", func() {
					actualP1, err := dao.GetProduct(productType, sku)
					So(err, ShouldBeNil)
					So(reflect.DeepEqual(p1, actualP1), ShouldBeTrue)
				})
			})
		})
	})
}

func TestProductDAO_GetProducts(t *testing.T) {
	Convey("Given a product DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := product.NewDAO(sess, dynamo.TestTablesSuffix)

		productType := random.String(32)
		Convey("Get products call returns empty for empty product type", func() {
			products, _, err := dao.GetProducts(productType, "")
			So(err, ShouldBeNil)
			So(len(products), ShouldEqual, 0)
		})

		Convey("When there is exactly one page worth of products", func() {
			for i := 0; i < product.PageSize; i++ {
				err := dao.CreateNew(&product.Product{
					ProductType: productType,
					SKU:         random.String(32),
				})
				So(err, ShouldBeNil)
			}

			Convey("Get products call returns the full page", func() {
				products, cursor, err := dao.GetProducts(productType, "")
				So(err, ShouldBeNil)
				So(len(products), ShouldEqual, product.PageSize)
				So(cursor, ShouldBeBlank)
			})

			Convey("When one more product is added", func() {
				err := dao.CreateNew(&product.Product{
					ProductType: productType,
					SKU:         random.String(32),
				})
				So(err, ShouldBeNil)

				Convey("Two get products calls returns all products", func() {
					products1, cursor1, err1 := dao.GetProducts(productType, "")
					So(err1, ShouldBeNil)
					So(len(products1), ShouldEqual, product.PageSize)
					So(cursor1, ShouldNotBeBlank)

					products2, cursor2, err2 := dao.GetProducts(productType, cursor1)
					So(err2, ShouldBeNil)
					So(len(products2), ShouldEqual, 1)
					So(cursor2, ShouldBeBlank)
				})
			})
		})
	})
}
