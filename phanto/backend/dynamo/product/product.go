package product

import (
	"errors"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

const (
	PageSize = 10
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetProduct(productType string, sku string) (*Product, error)
	CreateNew(product *Product) error
	CreateOrUpdate(product *Product) error
	GetProducts(productType string, cursor string) ([]*Product, string, error)
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("products-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type Product struct {
	ProductType string `dynamo:"product_type"`
	SKU         string `dynamo:"sku"`
	Description string `dynamo:"description"`
}

func (dao *daoImpl) GetProduct(productType string, sku string) (*Product, error) {
	var product Product
	err := dao.Get("product_type", productType).
		Consistent(true).
		Range("sku", dynamo.Equal, sku).
		One(&product)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &product, err
}

func (dao *daoImpl) CreateNew(product *Product) error {
	if product == nil {
		return errors.New("cannot create nil product")
	}

	return dao.Put(*product).If("attribute_not_exists(sku)").Run()
}

func (dao *daoImpl) CreateOrUpdate(product *Product) error {
	if product == nil {
		return errors.New("cannot create/update nil product")
	}

	return dao.Put(*product).Run()
}

func (dao *daoImpl) GetProducts(productType string, cursor string) ([]*Product, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"product_type": &dynamodb.AttributeValue{
				S: aws.String(productType),
			},
			"sku": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	var products = make([]*Product, 0, PageSize)
	newPagingKey, err := dao.Get("product_type", productType).
		StartFrom(pagingKey).
		Limit(PageSize).
		AllWithLastEvaluatedKey(&products)
	if err == dynamo.ErrNotFound {
		return []*Product{}, "", nil
	}

	var newCursor string
	skuAttribute, ok := newPagingKey["sku"]
	if ok && skuAttribute != nil {
		newCursor = pointers.StringOrDefault(skuAttribute.S, "")
	}

	return products, newCursor, err
}

func ToTwirpModel(products []*Product) []*phanto.Product {
	resp := make([]*phanto.Product, 0, len(products))
	for _, product := range products {
		if product != nil {
			resp = append(resp, &phanto.Product{
				ProductType: product.ProductType,
				Sku:         product.SKU,
				Description: product.Description,
			})
		}
	}
	return resp
}
