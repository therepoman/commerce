package utils

import "github.com/aws/aws-sdk-go/aws/awserr"

func IsConditionalCheckErr(err error) bool {
	if ae, ok := err.(awserr.RequestFailure); ok {
		return ae.Code() == "ConditionalCheckFailedException"
	}
	return false
}
