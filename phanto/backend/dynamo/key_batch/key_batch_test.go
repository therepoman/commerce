package key_batch_test

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	. "github.com/smartystreets/goconvey/convey"
)

func TestKeyBatchDAO_CRUD(t *testing.T) {
	Convey("Given a key batch DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := key_batch.NewDAO(sess, dynamo.TestTablesSuffix)

		batchID := random.String(32)
		Convey("Doesn't find a non-existent key batch", func() {
			k, err := dao.GetKeyBatch(batchID)
			So(err, ShouldBeNil)
			So(k, ShouldBeNil)
		})

		Convey("Succeeds on CreateNew", func() {
			kb1 := &key_batch.KeyBatch{
				BatchID:                batchID,
				PoolID:                 random.String(32),
				KeyBatchStatus:         status.KeyBatchStatusInactive,
				KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
			}
			err := dao.CreateNew(kb1)
			So(err, ShouldBeNil)

			Convey("Errors on subsequent attempts to CreateNew the same item", func() {
				err = dao.CreateNew(kb1)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds on GetKeyBatch", func() {
				actualKB1, err := dao.GetKeyBatch(batchID)
				So(err, ShouldBeNil)
				So(reflect.DeepEqual(kb1, actualKB1), ShouldBeTrue)
			})

			Convey("Succeeds on CreateOrUpdate", func() {
				kb1.KeyBatchWorkflowStatus = status.KeyBatchWorkflowStatusInProgress
				err = dao.CreateOrUpdate(kb1)
				So(err, ShouldBeNil)

				Convey("Succeeds on GetKeyBatch", func() {
					actualKB1, err := dao.GetKeyBatch(batchID)
					So(err, ShouldBeNil)
					So(reflect.DeepEqual(kb1, actualKB1), ShouldBeTrue)
				})
			})
		})
	})
}

func TestKeyBatch_GetKeysByPool(t *testing.T) {
	Convey("Given a key batch DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := key_batch.NewDAO(sess, dynamo.TestTablesSuffix)

		poolID := random.String(32)
		Convey("GetKeyBatchesByPool finds nothing for non-existent pool", func() {
			ks, _, err := dao.GetKeyBatchesByPool(poolID, "")
			So(err, ShouldBeNil)
			So(len(ks), ShouldEqual, 0)
		})

		Convey("When one batch is added to the pool", func() {
			kb1 := &key_batch.KeyBatch{
				PoolID:                 poolID,
				BatchID:                random.String(32),
				KeyBatchStatus:         status.KeyBatchStatusInactive,
				KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
			}
			err := dao.CreateNew(kb1)
			So(err, ShouldBeNil)

			Convey("GetKeyBatchesByPool finds the one batch", func() {
				kbs, _, err := dao.GetKeyBatchesByPool(poolID, "")
				So(err, ShouldBeNil)
				So(len(kbs), ShouldEqual, 1)
			})

			Convey("When a second batch is added to the pool", func() {
				kb2 := &key_batch.KeyBatch{
					PoolID:                 poolID,
					BatchID:                random.String(32),
					KeyBatchStatus:         status.KeyBatchStatusInactive,
					KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
				}
				err := dao.CreateNew(kb2)
				So(err, ShouldBeNil)

				Convey("GetKeyBatchesByPool finds the two batches", func() {
					kbs, _, err := dao.GetKeyBatchesByPool(poolID, "")
					So(err, ShouldBeNil)
					So(len(kbs), ShouldEqual, 2)
				})

				Convey("When 10 more batches are added to the pool", func() {
					for i := 0; i < 10; i++ {
						kb := &key_batch.KeyBatch{
							PoolID:                 poolID,
							BatchID:                random.String(32),
							KeyBatchStatus:         status.KeyBatchStatusInactive,
							KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
						}
						err := dao.CreateNew(kb)
						So(err, ShouldBeNil)
					}

					Convey("Two calls to GetKeyBatchesByPool gets all batches", func() {
						batches1, cursor1, err := dao.GetKeyBatchesByPool(poolID, "")
						So(len(batches1), ShouldEqual, 10)
						So(err, ShouldBeNil)

						batches2, _, err := dao.GetKeyBatchesByPool(poolID, cursor1)
						So(len(batches2), ShouldEqual, 2)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
