package key_batch

import (
	"errors"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

const (
	getByPoolLimit = 10
)

type DAO interface {
	GetKeyBatch(batchID string) (*KeyBatch, error)
	GetKeyBatchesByPool(poolID string, cursor string) ([]*KeyBatch, string, error)
	CreateNew(keyBatch *KeyBatch) error
	CreateOrUpdate(keyBatch *KeyBatch) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("key-batches-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type KeyBatch struct {
	BatchID                string                        `dynamo:"batch_id"`
	PoolID                 string                        `dynamo:"pool_id"`
	KeyBatchWorkflowStatus status.KeyBatchWorkflowStatus `dynamo:"key_batch_workflow_status"`
	KeyBatchStatus         status.KeyBatchStatus         `dynamo:"key_batch_status"`
	NumberOfKeys           int64                         `dynamo:"number_of_keys"`
	Progress               float64                       `dynamo:"progress"`
	Cipher                 []byte                        `dynamo:"cipher"`
}

func (dao *daoImpl) GetKeyBatch(batchID string) (*KeyBatch, error) {
	var keyBatch KeyBatch
	err := dao.Get("batch_id", batchID).Consistent(true).One(&keyBatch)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &keyBatch, err
}

func (dao *daoImpl) GetKeyBatchesByPool(poolID string, cursor string) ([]*KeyBatch, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"pool_id": &dynamodb.AttributeValue{
				S: aws.String(poolID),
			},
			"batch_id": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	var keyBatches []*KeyBatch
	newPagingKey, err := dao.Get("pool_id", poolID).
		Index(fmt.Sprintf("key-batches-pool-id-gsi-%s", dao.suffix)).
		StartFrom(pagingKey).
		Limit(getByPoolLimit).
		AllWithLastEvaluatedKey(&keyBatches)
	if err == dynamo.ErrNotFound {
		return []*KeyBatch{}, "", nil
	}

	var newCursor string
	batchIDAttribute, ok := newPagingKey["batch_id"]
	if ok && batchIDAttribute != nil {
		newCursor = pointers.StringOrDefault(batchIDAttribute.S, "")
	}

	return keyBatches, newCursor, err
}

func (dao *daoImpl) CreateNew(keyBatch *KeyBatch) error {
	if keyBatch == nil {
		return errors.New("cannot create nil key batch")
	}

	return dao.Put(*keyBatch).If("attribute_not_exists(batch_id)").Run()
}

func (dao *daoImpl) CreateOrUpdate(keyBatch *KeyBatch) error {
	if keyBatch == nil {
		return errors.New("cannot create/update nil key batch")
	}

	return dao.Put(*keyBatch).Run()
}

func ToTwirpModel(keyBatches []*KeyBatch) []*phanto.KeyBatch {
	resp := make([]*phanto.KeyBatch, 0, len(keyBatches))
	for _, keyBatch := range keyBatches {
		if keyBatch != nil {
			resp = append(resp, &phanto.KeyBatch{
				PoolId:         keyBatch.PoolID,
				BatchId:        keyBatch.BatchID,
				NumberOfKeys:   keyBatch.NumberOfKeys,
				Status:         keyBatch.KeyBatchStatus.String(),
				WorkflowStatus: keyBatch.KeyBatchWorkflowStatus.String(),
			})
		}
	}
	return resp
}
