package key_pool_metadata

import (
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

const (
	PageSize = 10
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	CreateNew(keyPoolMetadata *KeyPoolMetadata) error
	CreateOrUpdate(keyPoolMetaData *KeyPoolMetadata) error
	GetKeyPoolMetadata(poolID string, cursor string) ([]*KeyPoolMetadata, string, error)
	DeleteKeyPoolMetadata(poolID string, metadataKey string) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("key-pool-metadata-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type KeyPoolMetadata struct {
	PoolID        string    `dynamo:"pool_id"`
	MetadataKey   string    `dynamo:"metadata_key"`
	MetadataValue string    `dynamo:"metadata_value"`
	CreateDate    time.Time `dynamo:"create_date"`
}

func (dao *daoImpl) CreateNew(keyPoolMetadata *KeyPoolMetadata) error {
	if keyPoolMetadata == nil {
		return errors.New("cannot create nil key pool metadata")
	}

	return dao.Put(*keyPoolMetadata).If("attribute_not_exists(metadata_key)").Run()
}

func (dao *daoImpl) CreateOrUpdate(keyPoolMetadata *KeyPoolMetadata) error {
	if keyPoolMetadata == nil {
		return errors.New("cannot create/update nil key pool metadata")
	}

	return dao.Put(*keyPoolMetadata).Run()
}

func (dao *daoImpl) GetKeyPoolMetadata(poolID string, cursor string) ([]*KeyPoolMetadata, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"pool_id": &dynamodb.AttributeValue{
				S: aws.String(poolID),
			},
			"metadata_key": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	var keyPoolMetadatas = make([]*KeyPoolMetadata, 0, PageSize)
	newPagingKey, err := dao.Get("pool_id", poolID).
		Consistent(true).
		StartFrom(pagingKey).
		Limit(PageSize).
		AllWithLastEvaluatedKey(&keyPoolMetadatas)
	if err == dynamo.ErrNotFound {
		return []*KeyPoolMetadata{}, "", nil
	}

	var newCursor string
	metadataAttribute, ok := newPagingKey["metadata_key"]
	if ok && metadataAttribute != nil {
		newCursor = pointers.StringOrDefault(metadataAttribute.S, "")
	}

	return keyPoolMetadatas, newCursor, err
}

func (dao *daoImpl) DeleteKeyPoolMetadata(poolID string, metadataKey string) error {
	return dao.Delete("pool_id", poolID).
		Range("metadata_key", metadataKey).
		Run()
}

func ToTwirpModel(keyPoolMetadata []*KeyPoolMetadata) []*phanto.KeyPoolMetadata {
	resp := make([]*phanto.KeyPoolMetadata, 0, len(keyPoolMetadata))

	for _, md := range keyPoolMetadata {
		if md == nil {
			continue
		}

		resp = append(resp, &phanto.KeyPoolMetadata{
			MetadataKey:   md.MetadataKey,
			MetadataValue: md.MetadataValue,
		})
	}
	return resp
}
