package key_test

import (
	"reflect"
	"testing"

	crypto_random "code.justin.tv/commerce/gogogadget/crypto/random"
	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo"
	dynamo_key "code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	. "github.com/smartystreets/goconvey/convey"
)

func TestKeyDAO_CRUD(t *testing.T) {
	Convey("Given a key DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := dynamo_key.NewDAO(sess, dynamo.TestTablesSuffix)

		keyCode := code.Raw(random.String(32))
		keyHash, err := keyCode.Hash()
		So(err, ShouldBeNil)

		Convey("Doesn't find a non-existent key", func() {
			k, err := dao.GetKey(keyHash)
			So(err, ShouldBeNil)
			So(k, ShouldBeNil)
		})

		Convey("Succeeds on CreateNew", func() {
			k1 := &dynamo_key.Key{
				KeyCode:   keyHash,
				BatchID:   random.String(32),
				KeyStatus: status.KeyStatus(random.String(32)),
			}
			err := dao.CreateNew(k1)
			So(err, ShouldBeNil)

			Convey("Errors on subsequent attempts to CreateNew the same item", func() {
				err = dao.CreateNew(k1)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds on GetKey", func() {
				actualK1, err := dao.GetKey(keyHash)
				So(err, ShouldBeNil)
				So(reflect.DeepEqual(k1, actualK1), ShouldBeTrue)
			})

			Convey("Succeeds on CreateOrUpdate", func() {
				k1.KeyStatus = status.KeyStatus(random.String(32))
				err = dao.CreateOrUpdate(k1)
				So(err, ShouldBeNil)

				Convey("Succeeds on GetKey", func() {
					actualK1, err := dao.GetKey(keyHash)
					So(err, ShouldBeNil)
					So(reflect.DeepEqual(k1, actualK1), ShouldBeTrue)
				})
			})
		})
	})
}

func TestKeyDAO_GetKeysByBatch(t *testing.T) {
	Convey("Given a key DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := dynamo_key.NewDAO(sess, dynamo.TestTablesSuffix)
		keyCode := code.Raw(random.String(32))
		keyHash, err := keyCode.Hash()
		So(err, ShouldBeNil)

		batchID := random.String(32)
		Convey("GetKeysByBatch finds nothing for non-existent batch", func() {
			ks, cursor, err := dao.GetKeysByBatch(batchID, "")
			So(err, ShouldBeNil)
			So(len(ks), ShouldEqual, 0)
			So(cursor, ShouldBeBlank)
		})

		Convey("When one key is added to the batch", func() {
			k1 := &dynamo_key.Key{
				BatchID:   batchID,
				KeyCode:   keyHash,
				KeyStatus: status.KeyStatus(random.String(32)),
			}
			err := dao.CreateNew(k1)
			So(err, ShouldBeNil)

			Convey("GetKeysByBatch finds the one key", func() {
				ks, cursor, err := dao.GetKeysByBatch(batchID, "")
				So(err, ShouldBeNil)
				So(len(ks), ShouldEqual, 1)
				So(cursor, ShouldBeBlank)
			})

			Convey("When the batch contains a full page of keys", func() {
				for i := 0; i < dynamo_key.PageSize-1; i++ {
					keyCode = code.Raw(random.String(32))
					keyHash, err := keyCode.Hash()
					So(err, ShouldBeNil)
					k := &dynamo_key.Key{
						BatchID:   batchID,
						KeyCode:   keyHash,
						KeyStatus: status.KeyStatus(random.String(32)),
					}
					err = dao.CreateNew(k)
					So(err, ShouldBeNil)
				}

				Convey("GetKeysByBatch finds the entire page of keys", func() {
					ks, cursor, err := dao.GetKeysByBatch(batchID, "")
					So(err, ShouldBeNil)
					So(len(ks), ShouldEqual, dynamo_key.PageSize)
					So(cursor, ShouldBeBlank)
				})

				Convey("When one more key is added to the batch", func() {
					keyCode = code.Raw(random.String(32))
					keyHash, err := keyCode.Hash()
					So(err, ShouldBeNil)
					k := &dynamo_key.Key{
						BatchID:   batchID,
						KeyCode:   keyHash,
						KeyStatus: status.KeyStatus(random.String(32)),
					}
					err = dao.CreateNew(k)
					So(err, ShouldBeNil)

					Convey("GetKeysByBatch can paginate through all keys", func() {
						ks, cursor, err := dao.GetKeysByBatch(batchID, "")
						So(err, ShouldBeNil)
						So(len(ks), ShouldEqual, dynamo_key.PageSize)
						So(cursor, ShouldNotBeBlank)

						ks2, cursor2, err2 := dao.GetKeysByBatch(batchID, cursor)
						So(err2, ShouldBeNil)
						So(len(ks2), ShouldEqual, 1)
						So(cursor2, ShouldBeBlank)
					})
				})
			})
		})
	})
}

func TestDaoImpl_GetKeysClaimedByUser(t *testing.T) {
	Convey("Given a key DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := dynamo_key.NewDAO(sess, dynamo.TestTablesSuffix)
		keyCode := code.Raw(random.String(32))
		keyHash, err := keyCode.Hash()
		So(err, ShouldBeNil)

		claimedBy := random.String(32)
		Convey("GetKeysClaimedByUser finds nothing when there's no claimed keys for the user", func() {
			ks, cursor, err := dao.GetKeysClaimedByUser(claimedBy, "")
			So(err, ShouldBeNil)
			So(len(ks), ShouldEqual, 0)
			So(cursor, ShouldBeBlank)
		})

		Convey("When one key is claimed by the user", func() {
			k1 := &dynamo_key.Key{
				ClaimedBy: claimedBy,
				KeyCode:   keyHash,
				KeyStatus: status.KeyStatus(random.String(32)),
			}
			err := dao.CreateNew(k1)
			So(err, ShouldBeNil)

			Convey("GetKeysClaimedByUser finds the one key", func() {
				ks, cursor, err := dao.GetKeysClaimedByUser(claimedBy, "")
				So(err, ShouldBeNil)
				So(len(ks), ShouldEqual, 1)
				So(cursor, ShouldBeBlank)
			})

			Convey("When the user has claimed a full page of keys", func() {
				for i := 0; i < dynamo_key.PageSize-1; i++ {
					keyCode = code.Raw(random.String(32))
					keyHash, err := keyCode.Hash()
					So(err, ShouldBeNil)
					k := &dynamo_key.Key{
						ClaimedBy: claimedBy,
						KeyCode:   keyHash,
						KeyStatus: status.KeyStatus(random.String(32)),
					}
					err = dao.CreateNew(k)
					So(err, ShouldBeNil)
				}

				Convey("GetKeysByBatch finds the entire page of keys", func() {
					ks, cursor, err := dao.GetKeysClaimedByUser(claimedBy, "")
					So(err, ShouldBeNil)
					So(len(ks), ShouldEqual, dynamo_key.PageSize)
					So(cursor, ShouldBeBlank)
				})

				Convey("When the user has claimed more than a full page of keys", func() {
					keyCode = code.Raw(random.String(32))
					keyHash, err := keyCode.Hash()
					So(err, ShouldBeNil)
					k := &dynamo_key.Key{
						ClaimedBy: claimedBy,
						KeyCode:   keyHash,
						KeyStatus: status.KeyStatus(random.String(32)),
					}
					err = dao.CreateNew(k)
					So(err, ShouldBeNil)

					Convey("GetKeysClaimedByUser can paginate through all keys", func() {
						ks, cursor, err := dao.GetKeysClaimedByUser(claimedBy, "")
						So(err, ShouldBeNil)
						So(len(ks), ShouldEqual, dynamo_key.PageSize)
						So(cursor, ShouldNotBeBlank)

						ks2, cursor2, err2 := dao.GetKeysClaimedByUser(claimedBy, cursor)
						So(err2, ShouldBeNil)
						So(len(ks2), ShouldEqual, 1)
						So(cursor2, ShouldBeBlank)
					})
				})
			})
		})
	})
}

func TestKeyDAO_UpdateIfExists(t *testing.T) {
	Convey("Given a key DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := dynamo_key.NewDAO(sess, dynamo.TestTablesSuffix)

		keyCode := "123123123123"
		keyHash, err := code.Raw(keyCode).Hash()
		So(err, ShouldBeNil)

		Convey("Given a no key updates", func() {
			var keyUpdates *dynamo_key.Key = nil

			Convey("Should return an error", func() {
				err := dao.UpdateIfExists(keyUpdates)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given both a key code and a status", func() {
			keyUpdates := &dynamo_key.Key{
				KeyCode:   keyHash,
				KeyStatus: status.KeyStatusClaimed,
			}

			Convey("Should be successful upon database update", func() {
				err := dao.UpdateIfExists(keyUpdates)
				So(err, ShouldBeNil)
			})
		})

		Convey("Should be successful upon database update with claimed by", func() {
			claimedBy := "walrusoftime"
			keyUpdates := &dynamo_key.Key{
				KeyCode:   keyHash,
				KeyStatus: status.KeyStatusClaimed,
				ClaimedBy: claimedBy,
			}

			Convey("Should be successful upon database update", func() {
				err := dao.UpdateIfExists(keyUpdates)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestKeyDAO_GetKeys(t *testing.T) {
	Convey("Given a key DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := dynamo_key.NewDAO(sess, dynamo.TestTablesSuffix)

		Convey("Given a nil list of keys", func() {
			var keyCodes []code.RawHashed = nil

			Convey("Should return an no keys and no error", func() {
				records, err := dao.GetKeys(keyCodes)
				So(len(records), ShouldEqual, 0)
				So(err, ShouldBeNil)
			})
		})

		Convey("Given an empty list of keys", func() {
			Convey("Should return an no keys and no error", func() {
				records, err := dao.GetKeys([]code.RawHashed{})
				So(len(records), ShouldEqual, 0)
				So(err, ShouldBeNil)
			})
		})

		Convey("Given an non-empty list of keys", func() {
			keyCodes := generateKeyCodeList(10)

			Convey("When none of the items are in the database", func() {
				Convey("When we fetch the records we should return nothing", func() {
					records, err := dao.GetKeys(keyCodes)
					So(len(records), ShouldEqual, 0)
					So(err, ShouldBeNil)
				})
			})

			Convey("When some of the items are in the database", func() {
				for _, keyCode := range keyCodes[:5] {
					dao.CreateNew(&dynamo_key.Key{
						KeyCode:   keyCode,
						BatchID:   "test",
						KeyStatus: status.KeyStatusUnclaimed,
					})
				}

				Convey("When we fetch the records we should return only the records that exist", func() {
					records, err := dao.GetKeys(keyCodes)
					So(len(records), ShouldEqual, 5)
					So(err, ShouldBeNil)
				})
			})

			Convey("When all of the items are in the database", func() {
				for _, keyCode := range keyCodes {
					dao.CreateNew(&dynamo_key.Key{
						KeyCode:   keyCode,
						BatchID:   "test",
						KeyStatus: status.KeyStatusUnclaimed,
					})
				}

				Convey("When we fetch the records we should return all the records that exist", func() {
					records, err := dao.GetKeys(keyCodes)
					So(len(records), ShouldEqual, 10)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("Given an non-empty list of keys that is larger than 1 batch", func() {
			keyCodes := generateKeyCodeList(400)

			Convey("When none of the items are in the database", func() {
				Convey("When we fetch the records we should return nothing", func() {
					records, err := dao.GetKeys(keyCodes)
					So(len(records), ShouldEqual, 0)
					So(err, ShouldBeNil)
				})
			})

			Convey("When some of the items are in the database", func() {
				for _, keyCode := range keyCodes[:5] {
					dao.CreateNew(&dynamo_key.Key{
						KeyCode:   keyCode,
						BatchID:   "test",
						KeyStatus: status.KeyStatusUnclaimed,
					})
				}

				Convey("When we fetch the records we should return only the records that exist", func() {
					records, err := dao.GetKeys(keyCodes)
					So(len(records), ShouldEqual, 5)
					So(err, ShouldBeNil)
				})
			})

			Convey("When all of the items are in the database", func() {
				for _, keyCode := range keyCodes {
					dao.CreateNew(&dynamo_key.Key{
						KeyCode:   keyCode,
						BatchID:   "test",
						KeyStatus: status.KeyStatusUnclaimed,
					})
				}

				Convey("When we fetch the records we should return all the records that exist", func() {
					records, err := dao.GetKeys(keyCodes)
					So(len(records), ShouldEqual, 400)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestKeyDAO_UpdateBatch(t *testing.T) {
	Convey("Given a key DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := dynamo_key.NewDAO(sess, dynamo.TestTablesSuffix)

		Convey("Given a nil list of keys", func() {
			var keyCodes []dynamo_key.Key = nil

			Convey("Should return an no keys and no error", func() {
				err := dao.UpdateBatch(keyCodes)
				So(err, ShouldBeNil)
			})
		})

		Convey("Given an empty list of keys", func() {
			Convey("Should return an no keys and no error", func() {
				err := dao.UpdateBatch([]dynamo_key.Key{})
				So(err, ShouldBeNil)
			})
		})

		Convey("Given an non-empty list of keys", func() {
			keyCodes := generateKeyList(10)

			Convey("When none of the items are in the database", func() {
				Convey("When we fetch the records we should return nothing", func() {
					err := dao.UpdateBatch(keyCodes)
					So(err, ShouldBeNil)
				})
			})

			Convey("When some of the items are in the database", func() {
				for _, keyCode := range keyCodes[:5] {
					dao.CreateNew(&keyCode)
				}

				Convey("When we fetch the records we should return only the records that exist", func() {
					err := dao.UpdateBatch(keyCodes)
					So(err, ShouldBeNil)
				})
			})

			Convey("When all of the items are in the database", func() {
				for _, keyCode := range keyCodes {
					dao.CreateNew(&keyCode)
				}

				Convey("When we fetch the records we should return all the records that exist", func() {
					err := dao.UpdateBatch(keyCodes)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("Given an non-empty list of keys that is larger than 1 batch", func() {
			keyCodes := generateKeyList(400)

			Convey("When none of the items are in the database", func() {
				Convey("When we fetch the records we should return nothing", func() {
					err := dao.UpdateBatch(keyCodes)
					So(err, ShouldBeNil)
				})
			})

			Convey("When some of the items are in the database", func() {
				for _, keyCode := range keyCodes[:5] {
					dao.CreateNew(&keyCode)
				}

				Convey("When we fetch the records we should return only the records that exist", func() {
					err := dao.UpdateBatch(keyCodes)
					So(err, ShouldBeNil)
				})
			})

			Convey("When all of the items are in the database", func() {
				for _, keyCode := range keyCodes {
					dao.CreateNew(&keyCode)
				}

				Convey("When we fetch the records we should return all the records that exist", func() {
					err := dao.UpdateBatch(keyCodes)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func generateKeyCodeList(n int) []code.RawHashed {
	keyCodes := make([]code.RawHashed, 0, n)
	for i := 0; i < n; i++ {
		keyCode, _ := crypto_random.RandomString("ABCDEFGHIJKMNPQRSTUVWXYZ23456789", 15)
		keyHash, _ := code.Raw(keyCode).Hash()
		keyCodes = append(keyCodes, keyHash)
	}
	return keyCodes
}

func generateKeyList(n int) []dynamo_key.Key {
	keyCodes := make([]dynamo_key.Key, 0, n)
	for i := 0; i < n; i++ {
		keyCode, _ := crypto_random.RandomString("ABCDEFGHIJKMNPQRSTUVWXYZ23456789", 15)
		keyHash, _ := code.Raw(keyCode).Hash()
		keyCodes = append(keyCodes, dynamo_key.Key{
			KeyCode:   keyHash,
			KeyStatus: status.KeyStatusUnclaimed,
			BatchID:   "test",
		})
	}
	return keyCodes
}
