package key

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	PageSize            = 10
	dynamoReadBatchMax  = 100
	dynamoWriteBatchMax = 25
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetKey(keyCode code.RawHashed) (*Key, error)
	GetKeys(keyCodes []code.RawHashed) ([]Key, error)
	GetKeysByBatch(batchID string, cursor string) ([]Key, string, error)
	GetKeysClaimedByUser(userID string, cursor string) ([]Key, string, error)
	CreateNew(key *Key) error
	CreateOrUpdate(key *Key) error
	UpdateIfExists(key *Key) error
	UpdateBatch(keys []Key) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("keys-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type Key struct {
	KeyCode        code.RawHashed   `dynamo:"key_code"`
	BatchID        string           `dynamo:"batch_id"`
	KeyStatus      status.KeyStatus `dynamo:"key_status"`
	ClaimedBy      string           `dynamo:"claimed_by"`
	ClaimerIP      string           `dynamo:"claimer_ip"`
	ClaimerCountry string           `dynamo:"claimer_country"`
	ClaimerState   string           `dynamo:"claimer_state"`
	ClaimerZip     string           `dynamo:"claimer_zip"`
	ClaimTime      time.Time        `dynamo:"claim_time"`
	RedeemType     string           `dynamo:"redeem_type"`
}

func (dao *daoImpl) GetKey(keyCode code.RawHashed) (*Key, error) {
	var k Key
	err := dao.Get("key_code", keyCode).Consistent(true).One(&k)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &k, err
}

func (dao *daoImpl) GetKeys(keyCodes []code.RawHashed) ([]Key, error) {
	// it's not wrong to try and read nothing, we just want to short circuit and exit.
	if keyCodes == nil || len(keyCodes) < 1 {
		return []Key{}, nil
	}

	dynamoKeys := make([]dynamo.Keyed, 0, len(keyCodes))
	for _, key := range keyCodes {
		dynamoKeys = append(dynamoKeys, dynamo.Keys{key})
	}

	result := make([]Key, 0)
	keysForBatch := make([]dynamo.Keyed, 0)
	resultForBatch := make([]Key, 0)

	for i, dynamoKey := range dynamoKeys {
		keysForBatch = append(keysForBatch, dynamoKey)
		if len(keysForBatch) == dynamoReadBatchMax || i == len(dynamoKeys)-1 {
			err := dao.Batch("key_code").Get(keysForBatch...).All(&resultForBatch)
			if err != nil {
				return []Key{}, err
			}

			result = append(result, resultForBatch...)

			keysForBatch = make([]dynamo.Keyed, 0)
			resultForBatch = make([]Key, 0)
		}
	}

	return result, nil
}

func (dao *daoImpl) GetKeysByBatch(batchID string, cursor string) ([]Key, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"batch_id": &dynamodb.AttributeValue{
				S: aws.String(batchID),
			},
			"key_code": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	var keys []Key
	newPagingKey, err := dao.Get("batch_id", batchID).
		Index(fmt.Sprintf("keys-batch-id-gsi-%s", dao.suffix)).
		StartFrom(pagingKey).
		Limit(PageSize).
		AllWithLastEvaluatedKey(&keys)
	if err == dynamo.ErrNotFound {
		return []Key{}, "", nil
	}

	var newCursor string
	keyCodeAttribute, ok := newPagingKey["key_code"]
	if ok && keyCodeAttribute != nil {
		newCursor = pointers.StringOrDefault(keyCodeAttribute.S, "")
	}
	return keys, newCursor, err
}

func (dao *daoImpl) GetKeysClaimedByUser(userID string, cursor string) ([]Key, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"claimed_by": &dynamodb.AttributeValue{
				S: aws.String(userID),
			},
			"key_code": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	var keys []Key
	newPagingKey, err := dao.Get("claimed_by", userID).
		Index(fmt.Sprintf("keys-claimed-by-gsi-%s", dao.suffix)).
		StartFrom(pagingKey).
		Limit(PageSize).
		AllWithLastEvaluatedKey(&keys)
	if err == dynamo.ErrNotFound {
		return []Key{}, "", nil
	}

	var newCursor string
	keyCodeAttribute, ok := newPagingKey["key_code"]
	if ok && keyCodeAttribute != nil {
		newCursor = pointers.StringOrDefault(keyCodeAttribute.S, "")
	}
	return keys, newCursor, err
}

func (dao *daoImpl) CreateNew(key *Key) error {
	if key == nil {
		return errors.New("cannot create nil key")
	}

	return dao.Put(*key).If("attribute_not_exists(key_code)").Run()
}

func (dao *daoImpl) CreateOrUpdate(key *Key) error {
	if key == nil {
		return errors.New("cannot create/update nil key")
	}

	return dao.Put(*key).Run()
}

func (dao *daoImpl) UpdateIfExists(key *Key) error {
	if key == nil {
		return errors.New("cannot create/update nil key")
	}

	updateCall := dao.Update("key_code", key.KeyCode).If("attribute_exists(key_code) and key_status = ?", status.KeyStatusUnclaimed)
	if !strings.Blank(key.KeyStatus.String()) {
		updateCall.Set("key_status", key.KeyStatus)
	}
	if !strings.Blank(key.ClaimedBy) {
		updateCall.Set("claimed_by", key.ClaimedBy)
	}
	if !strings.Blank(key.ClaimerIP) {
		updateCall.Set("claimer_ip", key.ClaimerIP)
	}
	if !strings.Blank(key.ClaimerCountry) {
		updateCall.Set("claimer_country", key.ClaimerCountry)
	}
	if !strings.Blank(key.ClaimerState) {
		updateCall.Set("claimer_state", key.ClaimerState)
	}
	if !strings.Blank(key.ClaimerZip) {
		updateCall.Set("claimer_zip", key.ClaimerZip)
	}
	if key.ClaimTime.After(time.Time{}) {
		updateCall.Set("claim_time", key.ClaimTime)
	}
	if !strings.Blank(key.RedeemType) {
		updateCall.Set("redeem_type", key.RedeemType)
	}

	return updateCall.Run()
}

func (dao *daoImpl) UpdateBatch(keys []Key) error {
	// it's not wrong to try and write nothing, we just want to short circuit and exit.
	if keys == nil || len(keys) < 1 {
		return nil
	}

	itemBatch := make([]interface{}, 0, len(keys))
	updatedEntries := 0
	for i, k := range keys {
		itemBatch = append(itemBatch, k)

		if len(itemBatch) == dynamoWriteBatchMax || i == len(keys)-1 {
			wrote, err := dao.Batch().Write().Put(itemBatch...).Run()
			if err != nil {
				return err
			}

			updatedEntries += wrote
			itemBatch = make([]interface{}, 0)
		}
	}

	if updatedEntries != len(keys) {
		return errors.Errorf("unexpected number of keys updated. expected: %v actual: %v", len(keys), updatedEntries)
	}

	return nil
}
