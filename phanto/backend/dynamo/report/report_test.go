package report_test

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo"
	"code.justin.tv/commerce/phanto/backend/dynamo/report"
	. "github.com/smartystreets/goconvey/convey"
)

func TestReportDAO_CRUD(t *testing.T) {
	Convey("Given a report DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := report.NewDAO(sess, dynamo.TestTablesSuffix)

		reportID := random.String(32)
		Convey("Doesn't find a non-existent report", func() {
			rep, err := dao.GetReport(reportID)
			So(err, ShouldBeNil)
			So(rep, ShouldBeNil)
		})

		Convey("Succeeds on CreateNew", func() {
			rpt1 := &report.Report{
				ReportID:    reportID,
				PoolID:      random.String(32),
				RequestedBy: random.String(32),
			}
			err := dao.CreateNew(rpt1)
			So(err, ShouldBeNil)

			Convey("Errors on subsequent attempts to CreateNew the same item", func() {
				err = dao.CreateNew(rpt1)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds on GetReport", func() {
				actualRPT1, err := dao.GetReport(reportID)
				So(err, ShouldBeNil)
				So(reflect.DeepEqual(rpt1, actualRPT1), ShouldBeTrue)
			})

			Convey("Succeeds on CreateOrUpdate", func() {
				rpt1.RequestedBy = random.String(32)
				err = dao.CreateOrUpdate(rpt1)
				So(err, ShouldBeNil)

				Convey("Succeeds on GetReport", func() {
					actualRPT1, err := dao.GetReport(reportID)
					So(err, ShouldBeNil)
					So(reflect.DeepEqual(rpt1, actualRPT1), ShouldBeTrue)
				})
			})
		})
	})
}

func TestReportDAO_GetReportsByPool(t *testing.T) {
	Convey("Given a report DAO", t, func() {
		sess := dynamo.GetAWSSession(t)
		dao := report.NewDAO(sess, dynamo.TestTablesSuffix)

		poolID := random.String(32)
		Convey("DAO returns empty when there are no reports for the pool", func() {
			reps, cursor, err := dao.GetReportsByPool(poolID, "")
			So(err, ShouldBeNil)
			So(len(reps), ShouldEqual, 0)
			So(cursor, ShouldBeBlank)
		})

		Convey("When one report is added to the pool", func() {
			rpt1 := &report.Report{
				ReportID: random.String(32),
				PoolID:   poolID,
			}
			err := dao.CreateNew(rpt1)
			So(err, ShouldBeNil)

			Convey("DAO returns just that element", func() {
				reports, cursor, err := dao.GetReportsByPool(poolID, "")
				So(err, ShouldBeNil)
				So(len(reports), ShouldEqual, 1)
				So(cursor, ShouldBeBlank)
			})

			Convey("When one page worth of reports are in the pool", func() {
				for i := 0; i < report.PageSize-1; i++ {
					rpt := &report.Report{
						ReportID: random.String(32),
						PoolID:   poolID,
					}
					err := dao.CreateNew(rpt)
					So(err, ShouldBeNil)
				}

				Convey("DAO returns a full page", func() {
					reports, cursor, err := dao.GetReportsByPool(poolID, "")
					So(err, ShouldBeNil)
					So(len(reports), ShouldEqual, 10)
					So(cursor, ShouldBeBlank)

				})

				Convey("When one more report is added to the pool", func() {
					rpt := &report.Report{
						ReportID: random.String(32),
						PoolID:   poolID,
					}
					err := dao.CreateNew(rpt)
					So(err, ShouldBeNil)

					Convey("DAO can be paginated", func() {
						reports, cursor, err := dao.GetReportsByPool(poolID, "")
						So(err, ShouldBeNil)
						So(len(reports), ShouldEqual, 10)
						So(cursor, ShouldNotBeBlank)

						reports2, cursor2, err2 := dao.GetReportsByPool(poolID, cursor)
						So(err2, ShouldBeNil)
						So(len(reports2), ShouldEqual, 1)
						So(cursor2, ShouldBeBlank)
					})
				})
			})
		})
	})
}
