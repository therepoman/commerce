package report

import (
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/golang/protobuf/ptypes"
	"github.com/guregu/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	PageSize = 10
)

type ReportType string

const (
	ReportTypeKeyPool = ReportType("KEY_POOL_REPORT")
)

type ReportStatus string

const (
	ReportStatusRequested  = ReportStatus("REQUESTED")
	ReportStatusInProgress = ReportStatus("IN_PROGRESS")
	ReportStatusFinished   = ReportStatus("FINISHED")
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetReport(reportID string) (*Report, error)
	CreateNew(report *Report) error
	CreateOrUpdate(report *Report) error
	GetReportsByPool(poolID string, cursor string) ([]*Report, string, error)
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("reports-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type Report struct {
	ReportID     string       `dynamo:"report_id"`
	ReportType   ReportType   `dynamo:"report_type"`
	PoolID       string       `dynamo:"pool_id"`
	RequestedBy  string       `dynamo:"requested_by"`
	RequestedAt  time.Time    `dynamo:"requested_at"`
	ReportStatus ReportStatus `dynamo:"report_status"`
	Cipher       []byte       `dynamo:"cipher"`
}

func (dao *daoImpl) GetReport(reportID string) (*Report, error) {
	var r Report
	err := dao.Get("report_id", reportID).Consistent(true).One(&r)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &r, err
}

func (dao *daoImpl) CreateNew(report *Report) error {
	if report == nil {
		return errors.New("cannot create nil report")
	}

	return dao.Put(*report).If("attribute_not_exists(report_id)").Run()
}

func (dao *daoImpl) CreateOrUpdate(report *Report) error {
	if report == nil {
		return errors.New("cannot create/update nil report")
	}

	return dao.Put(*report).Run()
}

func (dao *daoImpl) GetReportsByPool(poolID string, cursor string) ([]*Report, string, error) {
	var pagingKey dynamo.PagingKey
	if strings.NotBlank(cursor) {
		pagingKey = dynamo.PagingKey{
			"pool_id": &dynamodb.AttributeValue{
				S: aws.String(poolID),
			},
			"report_id": &dynamodb.AttributeValue{
				S: aws.String(cursor),
			},
		}
	}

	var reports []*Report
	newPagingKey, err := dao.Get("pool_id", poolID).
		Index(fmt.Sprintf("reports-pool-id-gsi-%s", dao.suffix)).
		StartFrom(pagingKey).
		Limit(PageSize).
		AllWithLastEvaluatedKey(&reports)
	if err == dynamo.ErrNotFound {
		return []*Report{}, "", nil
	}

	var newCursor string
	reportIDAttribute, ok := newPagingKey["report_id"]
	if ok && reportIDAttribute != nil {
		newCursor = pointers.StringOrDefault(reportIDAttribute.S, "")
	}

	return reports, newCursor, err
}

func ToKeyPoolReportTwirpModel(reports []*Report) []*phanto.KeyPoolReport {
	resp := make([]*phanto.KeyPoolReport, 0, len(reports))

	for _, report := range reports {
		if report != nil {
			requestedAt, err := ptypes.TimestampProto(report.RequestedAt)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{"pool_id": report.PoolID, "report_id": report.ReportID}).Error("invalid requested_at time in dynamo record")
			}

			resp = append(resp, &phanto.KeyPoolReport{
				PoolId:      report.PoolID,
				ReportId:    report.ReportID,
				RequestedAt: requestedAt,
				RequestedBy: report.RequestedBy,
				Status:      string(report.ReportStatus),
			})
		}
	}
	return resp
}
