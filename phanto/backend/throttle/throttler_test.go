package throttle

import (
	"fmt"
	"math"
	"testing"
	"time"

	rd "code.justin.tv/commerce/phanto/backend/clients/redis"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/github.com/cactus/go-statsd-client/statsd"
	"github.com/alicebob/miniredis"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestRedisThrottle(t *testing.T) {
	Convey("Given a redis client", t, func() {
		server, err := miniredis.Run()
		if err != nil {
			panic(fmt.Sprintf("Could not start redis server. %v", err))
		}
		defer server.Close()

		redisClient := rd.NewClient(config.Config{
			RedisEndpoint:       server.Addr(),
			UseRedisClusterMode: false,
		})

		statter := new(statsd_mock.Statter)

		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Given a throttler", func() {
			throttler := throttler{
				RedisClient: redisClient,
				Stats:       statter,
			}

			Convey("When we call the throttler at ~1 TPS", func() {
				Convey("one time should not throttle", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 1, 1, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeFalse)
					So(throttleResponse.ThrottledDuration, ShouldEqual, time.Second/2)
				})

				Convey("one hundred times should not trigger throttling for the next call", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 1, 100, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeFalse)
					So(throttleResponse.ThrottledDuration, ShouldEqual, 1*time.Second)
				})

				Convey("one hundred and one times should not throttle", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 1, 101, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeFalse)
					So(throttleResponse.ThrottledDuration, ShouldEqual, 1*time.Second)
				})
			})

			Convey("When we call the throttler at ~2 TPS", func() {
				Convey("one time should not throttle", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 2, 1, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeFalse)
					So(throttleResponse.ThrottledDuration, ShouldEqual, time.Second/2)
				})

				Convey("one hundred times should trigger throttling for the next call", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 2, 100, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeFalse)
					So(throttleResponse.ThrottledDuration, ShouldEqual, 1*time.Second)
				})

				Convey("one hundred and one times should throttle", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 2, 101, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeTrue)
					So(throttleResponse.ThrottledDuration, ShouldEqual, 1*time.Second+500*time.Millisecond)
				})
			})

			Convey("When we call the throttler faster than 2TPS", func() {
				Convey("one time", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 3, 1, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeFalse)
				})

				Convey("more than one time", func() {
					throttleResponse, err := callThrottlerAtFrequency(server, &throttler, 3, 2, "AAAAA-AAAAA-AAAAA")

					So(err, ShouldBeEmpty)
					So(throttleResponse.IsThrottled, ShouldBeTrue)
				})
			})
		})
	})
}

func callThrottlerAtFrequency(server *miniredis.Miniredis, throttler Throttler, frequency float64, times int, key string) (ThrottleResponse, error) {
	isEverThrottled := false
	maxThrottleDuration := float64(0)
	for t := times; t > 0; t-- {
		throttleResponse, err := throttler.ThrottleExponentially(key)

		if err != nil {
			return throttleResponse, err
		}

		if throttleResponse.IsThrottled {
			isEverThrottled = true
		}

		maxThrottleDuration = math.Max(float64(throttleResponse.ThrottledDuration), maxThrottleDuration)

		/* miniredis doesn't obey TTLs because it's for unit tests,
		*  so we have to fast forward time each call
		 */
		server.FastForward(time.Duration(float64(1*time.Second) / frequency))
	}
	return ThrottleResponse{
		IsThrottled:       isEverThrottled,
		ThrottledDuration: time.Duration(maxThrottleDuration),
	}, nil
}
