package throttle

import (
	"fmt"
	"math"
	"strconv"
	"time"

	"code.justin.tv/commerce/phanto/backend/clients/redis"
	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

const (
	maxExponentialThrottle              = time.Duration(24*time.Hour) / time.Duration(time.Second) //max of one day in seconds
	consecutiveAttemptsDampeningFactor  = float64(0.01)                                            //allow ~50-100 attempts before throttling kicks in
	consecutiveAttemptsIdentifier       = "ConsecutiveAttempts"
	defaultExpiration                   = time.Second
	defaultConsecutiveAttemptExpiration = time.Minute
	exponentiallyThrottledMetricName    = "exponentially_throttled_request"
	regularlyThrottledMetricName        = "regularly_throttled_request"
	baseThrottledMetricName             = "throttled_request"
)

type ThrottleResponse struct {
	IsThrottled       bool
	ThrottledDuration time.Duration
}

type Throttler interface {
	Throttle(key string, expiration time.Duration) (bool, error)
	ThrottleExponentially(key string) (ThrottleResponse, error)
}

type throttler struct {
	RedisClient redis.Client   `inject:""`
	Stats       statsd.Statter `inject:""`
}

func NewThrottler() Throttler {
	throttler := &throttler{}

	return throttler
}

func (t *throttler) Throttle(key string, expiration time.Duration) (bool, error) {
	val, err := t.RedisClient.Get(key)

	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error getting redis value")
		return true, err
	}

	if val != "" {
		go t.countThrottledRequest(regularlyThrottledMetricName)

		return true, nil
	}

	err = t.RedisClient.Set(key, "true", expiration)
	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error setting redis value")
		return false, err
	}

	return false, nil
}

/* 	We store the exponent of the exponential function as the value in Redis.
*	If a user calls an exponentially throttled API again before the redis cache
*	expires, then we increment the exponent (to a max of one day), and restart the
*	timer.
*
*   We also store the number of consecutive (within a minute of each other) attempts
*   in redis, and factor that into the timeout function of the exponent's expiration.
*
*   See the determineExpiration function below or the unit tests for more detail
 */
func (t *throttler) ThrottleExponentially(key string) (ThrottleResponse, error) {
	expirationStr, err := t.RedisClient.Get(key)

	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error getting redis value")
		return ThrottleResponse{
			IsThrottled:       true,
			ThrottledDuration: defaultExpiration,
		}, err
	}

	isThrottled := false
	newExponent := uint64(0)

	//if there is an expiration set in redis
	if expirationStr != "" {
		//redis only lets you store strings, so parse into an int64
		expiration, err := strconv.ParseUint(expirationStr, 10, 64)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"key":        key,
				"expiration": expirationStr,
			}).Error("Error parsing redis value to uint64")
			return ThrottleResponse{
				IsThrottled:       true,
				ThrottledDuration: defaultExpiration,
			}, err
		}

		//increment the exponent up to some max (one day)
		if expiration < uint64(math.Log2(float64(maxExponentialThrottle))) {
			newExponent = expiration + 1
		}
		isThrottled = true
	}

	consecutiveAttemptsStr, err := t.RedisClient.Get(formatConsecutiveAttempts(key))

	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error getting redis value")
		return ThrottleResponse{
			IsThrottled:       true,
			ThrottledDuration: defaultExpiration,
		}, err
	}

	//And then add the number of consecutive attempts from redis, otherwise 0
	consecutiveAttempts := uint64(0)

	//if there is a consecutive attempts counter in redis
	if consecutiveAttemptsStr != "" {
		//redis only lets you store strings, so parse into an int64
		consecutiveAttempts, err = strconv.ParseUint(consecutiveAttemptsStr, 10, 64)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"key":              key,
				"consecutiveTries": consecutiveAttemptsStr,
			}).Error("Error parsing redis value to uint64")
			return ThrottleResponse{
				IsThrottled:       true,
				ThrottledDuration: defaultExpiration,
			}, err
		}
	}

	//Always start the number of consecutive attempts at one (this one) plus the stored value in redis
	newConsecutiveAttempts := 1 + consecutiveAttempts

	//set the new counter with a minute default until we no longer consider your tries consecutive
	err = t.RedisClient.Set(formatConsecutiveAttempts(key), newConsecutiveAttempts, defaultConsecutiveAttemptExpiration)

	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error setting redis value")
		return ThrottleResponse{
			IsThrottled:       true,
			ThrottledDuration: defaultExpiration,
		}, err
	}

	newExpiration := determineExpiration(newExponent, newConsecutiveAttempts)

	err = t.RedisClient.Set(key, newExponent, newExpiration)

	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error setting redis value")
		return ThrottleResponse{
			IsThrottled:       true,
			ThrottledDuration: newExpiration,
		}, err
	}

	if isThrottled {
		go t.countThrottledRequest(exponentiallyThrottledMetricName)
	}

	return ThrottleResponse{
		IsThrottled:       isThrottled,
		ThrottledDuration: newExpiration,
	}, nil
}

func (t *throttler) countThrottledRequest(metricName string) {
	err := t.Stats.Inc(metricName, int64(1), float32(1))
	if err != nil {
		log.WithField("metricName", metricName).WithError(err).Error("Error counting throttled request")
	}
	err = t.Stats.Inc(baseThrottledMetricName, int64(1), float32(1))
	if err != nil {
		log.WithField("metricName", baseThrottledMetricName).WithError(err).Error("Error counting throttled request")
	}
}

func formatConsecutiveAttempts(key string) string {
	return fmt.Sprintf("%s.%s", key, consecutiveAttemptsIdentifier)
}

func determineExpiration(newExponent uint64, newConsecutiveAttempts uint64) time.Duration {
	/* Every request while already throttled doubles your expiration up to a max value
	* As you count up more consecutive tries, each try will make you more likely to
	* become throttled.
	*
	* We use a function like f = 2^x + 0.01y where x is number of attempts after being throttled
	* and y is number of consecutive attempts in general. f represents the expiration of the
	* throttling, hence why it grows exponentially once x is triggered to begin increasing.
	 */
	consecutiveAttemptsFactor := float64(newConsecutiveAttempts) * consecutiveAttemptsDampeningFactor
	frequencyFactor := math.Pow(2.0, float64(newExponent))

	numSeconds := math.Min(frequencyFactor+consecutiveAttemptsFactor, float64(maxExponentialThrottle))

	expiration := time.Duration(numSeconds) * (time.Second / 2)

	return expiration
}
