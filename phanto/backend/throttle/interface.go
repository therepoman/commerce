package throttle

import (
	"time"
)

type IThrottler interface {
	Throttle(key string, expiration time.Duration) (bool, error)
	ThrottleExponentially(key string) (ThrottleResponse, error)
}
