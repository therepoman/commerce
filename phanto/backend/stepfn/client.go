package stepfn

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/phanto/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/pkg/errors"
)

type Client interface {
	Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error
}

type client struct {
	InternalClient sfniface.SFNAPI
}

func NewClient(config *config.Config) Client {
	s, _ := session.NewSession()
	sfnClient := sfn.New(s, aws.NewConfig().WithRegion(config.AWSRegion).WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint))

	return &client{
		InternalClient: sfnClient,
	}
}

func (c *client) Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error {
	inputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return errors.Wrapf(err, "failed to marshall state machine execution input. stateMachineARN: %s executionInput: %+v", stateMachineARN, executionInput)
	}

	startExecInput := &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputBytes)),
		Name:            &executionName,
		StateMachineArn: &stateMachineARN,
	}
	if _, err := c.InternalClient.StartExecutionWithContext(ctx, startExecInput); err != nil {
		return err
	}
	return nil
}
