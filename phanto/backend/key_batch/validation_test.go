package key_batch

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	key_batch_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	key_pool_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_pool"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a key validator", t, func() {
		ctx := context.Background()
		dao := new(key_batch_mock.DAO)
		poolValidator := new(key_pool_mock.Validator)

		v := &validator{
			DAO:           dao,
			PoolValidator: poolValidator,
		}

		keyBatchID := "1231231231123"
		keyPoolID := "123123123"

		Convey("When we error fetching the key batch from Dynamo", func() {
			dao.On("GetKeyBatch", keyBatchID).Return(nil, errors.New("THE WALRUS STRIKES"))

			Convey("We should return an internal server error", func() {
				claimData, err := v.Validate(ctx, keyBatchID)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we do not have a record for the key batch in Dynamo", func() {
			dao.On("GetKeyBatch", keyBatchID).Return(nil, nil)

			Convey("We should return a not found error", func() {
				claimData, err := v.Validate(ctx, keyBatchID)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we find the key batch record in Dynamo that is not active", func() {
			keyBatchRecord := &key_batch.KeyBatch{
				BatchID:                keyBatchID,
				KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusFailed,
				KeyBatchStatus:         status.KeyBatchStatusInactive,
				NumberOfKeys:           10000,
				PoolID:                 keyPoolID,
			}

			dao.On("GetKeyBatch", keyBatchID).Return(keyBatchRecord, nil)

			snsTopic := "walrusTopic"
			productType := "asdfasdf"
			sku := "123123123123123"
			productDescription := "Spend a day with a walrus"

			keyPoolClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           snsTopic,
				ProductType:        productType,
				ProductDescription: productDescription,
				KeyPoolID:          keyPoolID,
				SKU:                sku,
			}

			Convey("when we error validating the key pool associated with the key batch", func() {
				keyPoolClaimData.IsValid = false

				poolValidator.On("Validate", ctx, keyPoolID).Return(keyPoolClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, keyBatchID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the key pool associated with it is invalid", func() {
				keyPoolClaimData.IsValid = false

				poolValidator.On("Validate", ctx, keyPoolID).Return(keyPoolClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, keyBatchID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the key pool associated is valid", func() {
				poolValidator.On("Validate", ctx, keyPoolID).Return(keyPoolClaimData, nil)

				Convey("We should return that the batch is not valid", func() {
					claimData, err := v.Validate(ctx, keyBatchID)

					So(claimData.IsValid, ShouldBeFalse)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, productDescription)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(claimData.KeyPoolID, ShouldEqual, keyPoolID)
					So(claimData.KeyBatchID, ShouldEqual, keyBatchID)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("When we find the key record in Dynamo that can be claimed", func() {
			keyBatchRecord := &key_batch.KeyBatch{
				BatchID:                keyBatchID,
				KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusSucceeded,
				KeyBatchStatus:         status.KeyBatchStatusActive,
				NumberOfKeys:           10000,
				PoolID:                 keyPoolID,
			}

			dao.On("GetKeyBatch", keyBatchID).Return(keyBatchRecord, nil)

			snsTopic := "walrusTopic"
			productType := "asdfasdf"
			sku := "123123123123123"
			productDescription := "Spend a day with a walrus"

			keyPoolClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           snsTopic,
				ProductType:        productType,
				ProductDescription: productDescription,
				KeyPoolID:          keyPoolID,
				SKU:                sku,
			}

			Convey("when we error validating the key pool associated with the key batch", func() {
				keyPoolClaimData.IsValid = false

				poolValidator.On("Validate", ctx, keyPoolID).Return(keyPoolClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, keyBatchID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the key pool associated with it is invalid", func() {
				keyPoolClaimData.IsValid = false

				poolValidator.On("Validate", ctx, keyPoolID).Return(keyPoolClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, keyBatchID)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the key pool associated is valid", func() {
				poolValidator.On("Validate", ctx, keyPoolID).Return(keyPoolClaimData, nil)

				Convey("We should return that the batch is valid", func() {
					claimData, err := v.Validate(ctx, keyBatchID)

					So(claimData.IsValid, ShouldBeTrue)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, productDescription)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(claimData.KeyPoolID, ShouldEqual, keyPoolID)
					So(claimData.KeyBatchID, ShouldEqual, keyBatchID)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
