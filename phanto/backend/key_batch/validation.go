package key_batch

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_pool"
	"github.com/pkg/errors"
)

type Validator interface {
	Validate(ctx context.Context, keyBatchID string) (claimData data.Validation, err error)
}

type validator struct {
	PoolValidator key_pool.Validator `inject:"keyPoolValidator"`
	DAO           key_batch.DAO      `inject:""`
}

func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(ctx context.Context, keyBatchID string) (claimData data.Validation, err error) {
	claimData.IsValid = false

	dynamoKeyBatch, err := v.DAO.GetKeyBatch(keyBatchID)
	if err != nil {
		return claimData, errors.Wrap(err, "downstream dependency error getting Key Batch status")
	}
	if dynamoKeyBatch == nil {
		return claimData, errors.New("no key batch is associated with requested Key Batch ID")
	}

	keyPoolClaimData, err := v.PoolValidator.Validate(ctx, dynamoKeyBatch.PoolID)
	if err != nil || !keyPoolClaimData.IsValid {
		return claimData, err
	}

	claimData = keyPoolClaimData
	claimData.KeyBatchID = dynamoKeyBatch.BatchID

	if !dynamoKeyBatch.KeyBatchStatus.IsActive() || !dynamoKeyBatch.KeyBatchWorkflowStatus.IsActive() {
		claimData.IsValid = false
		return
	}

	return
}
