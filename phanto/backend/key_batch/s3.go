package key_batch

import (
	"fmt"
)

func GetEncryptedS3Key(poolID string, batchID string) string {
	return fmt.Sprintf("%s/%s_encrypted.csv", poolID, batchID)
}

func GePlaintextS3Key(poolID string, batchID string) string {
	return fmt.Sprintf("%s/%s_plaintext.csv", poolID, batchID)
}
