package authorization

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	key_pool_authorization "code.justin.tv/commerce/phanto/backend/key_pool/authorization"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/config"
)

type HandlerAuthorizer interface {
	AuthorizeByTUID(ctx context.Context, keyBatch *key_batch.KeyBatch) bool
	AuthorizeByClientID(ctx context.Context, keyBatch *key_batch.KeyBatch) bool
}

type handlerAuthorizer struct {
	Config            *config.Config                           `inject:""`
	KeyPoolDAO        key_pool.DAO                             `inject:""`
	KeyPoolAuthorizer key_pool_authorization.HandlerAuthorizer `inject:""`
}

func NewHandlerAuthorizer() HandlerAuthorizer {
	return &handlerAuthorizer{}
}

func (a *handlerAuthorizer) AuthorizeByClientID(ctx context.Context, keyBatch *key_batch.KeyBatch) bool {
	_, ok := ctx.Value(middleware.ClientIDContextKey).(string)
	if !ok {
		// no client id, so is not authorized
		return false
	}

	if keyBatch == nil {
		return false
	}

	keyPool, err := a.KeyPoolDAO.GetKeyPool(keyBatch.PoolID)
	if keyPool == nil || err != nil {
		return false
	}

	return a.KeyPoolAuthorizer.AuthorizeByClientID(ctx, keyPool)
}

func (a *handlerAuthorizer) AuthorizeByTUID(ctx context.Context, keyBatch *key_batch.KeyBatch) bool {
	_, ok := ctx.Value(authorization.AuthenticatedUserIDContextKey).(string)
	if !ok {
		// no authed user, so is not authorized
		return false
	}

	if keyBatch == nil {
		return false
	}

	keyPool, err := a.KeyPoolDAO.GetKeyPool(keyBatch.PoolID)
	if keyPool == nil || err != nil {
		return false
	}

	return a.KeyPoolAuthorizer.AuthorizeByTUID(ctx, keyPool)
}
