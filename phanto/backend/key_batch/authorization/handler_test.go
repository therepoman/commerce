package authorization

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_pool/authorization"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHandlerAuthorizer_AuthorizeByTUID(t *testing.T) {
	Convey("Given a key batch handler authorizer", t, func() {
		dao := new(key_pool_mock.DAO)
		poolAuthorizer := new(authorization_mock.HandlerAuthorizer)
		authorizer := &handlerAuthorizer{
			KeyPoolDAO:        dao,
			KeyPoolAuthorizer: poolAuthorizer,
		}

		Convey("Given there is no authed user", func() {
			ctx := context.Background()

			Convey("then we return that the user is not authorized", func() {
				So(authorizer.AuthorizeByTUID(ctx, nil), ShouldBeFalse)
			})
		})

		Convey("Given there is an authed user", func() {
			userID := "123123123"

			ctx := context.Background()
			ctx = context.WithValue(ctx, authorization.AuthenticatedUserIDContextKey, userID)

			Convey("Given the key batch is nil", func() {
				var keyBatch *key_batch.KeyBatch = nil

				Convey("then we return that the user is not authorized", func() {
					So(authorizer.AuthorizeByTUID(ctx, keyBatch), ShouldBeFalse)
				})
			})

			Convey("Given a non nil key batch", func() {
				batchID := "batchID"
				poolID := "poolID"

				keyBatch := &key_batch.KeyBatch{
					BatchID: batchID,
					PoolID:  poolID,
				}

				Convey("When the key pool DAO returns an error fetching the key pool", func() {
					dao.On("GetKeyPool", poolID).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("then we return that the user is not authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyBatch), ShouldBeFalse)
					})
				})

				Convey("When the key pool is not found", func() {
					dao.On("GetKeyPool", poolID).Return(nil, nil)

					Convey("then we return that the user is not authorized", func() {
						So(authorizer.AuthorizeByTUID(ctx, keyBatch), ShouldBeFalse)
					})
				})

				Convey("when we find a valid key pool", func() {
					keyPool := &key_pool.KeyPool{
						PoolID:  poolID,
						Handler: userID,
					}

					dao.On("GetKeyPool", poolID).Return(keyPool, nil)

					Convey("when the key pool authorizer returns false", func() {
						poolAuthorizer.On("AuthorizeByTUID", mock.Anything, mock.Anything).Return(false)

						Convey("then we return that the user is not authorized", func() {
							So(authorizer.AuthorizeByTUID(ctx, keyBatch), ShouldBeFalse)
						})
					})

					Convey("when the key pool authorizer returns true", func() {
						poolAuthorizer.On("AuthorizeByTUID", mock.Anything, mock.Anything).Return(true)

						Convey("then we return that the user is authorized", func() {
							So(authorizer.AuthorizeByTUID(ctx, keyBatch), ShouldBeTrue)
						})
					})
				})
			})

		})
	})
}

func TestHandlerAuthorizer_AuthorizeByClientID(t *testing.T) {
	Convey("Given a key batch handler authorizer", t, func() {
		dao := new(key_pool_mock.DAO)
		poolAuthorizer := new(authorization_mock.HandlerAuthorizer)
		authorizer := &handlerAuthorizer{
			KeyPoolDAO:        dao,
			KeyPoolAuthorizer: poolAuthorizer,
		}

		Convey("Given there is no client id", func() {
			ctx := context.Background()

			Convey("then we return that the request is not authorized", func() {
				So(authorizer.AuthorizeByClientID(ctx, nil), ShouldBeFalse)
			})
		})

		Convey("Given there is a client id", func() {
			clientID := "123123123"

			ctx := context.Background()
			ctx = context.WithValue(ctx, middleware.ClientIDContextKey, clientID)

			Convey("Given the key batch is nil", func() {
				var keyBatch *key_batch.KeyBatch = nil

				Convey("then we return that the request is not authorized", func() {
					So(authorizer.AuthorizeByClientID(ctx, keyBatch), ShouldBeFalse)
				})
			})

			Convey("Given a non nil key batch", func() {
				batchID := "batchID"
				poolID := "poolID"

				keyBatch := &key_batch.KeyBatch{
					BatchID: batchID,
					PoolID:  poolID,
				}

				Convey("When the key pool DAO returns an error fetching the key pool", func() {
					dao.On("GetKeyPool", poolID).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("then we return that the request is not authorized", func() {
						So(authorizer.AuthorizeByClientID(ctx, keyBatch), ShouldBeFalse)
					})
				})

				Convey("When the key pool is not found", func() {
					dao.On("GetKeyPool", poolID).Return(nil, nil)

					Convey("then we return that the request is not authorized", func() {
						So(authorizer.AuthorizeByClientID(ctx, keyBatch), ShouldBeFalse)
					})
				})

				Convey("when we find a valid key pool", func() {
					keyPool := &key_pool.KeyPool{
						PoolID:         poolID,
						HandlerGroupID: "handler",
					}

					dao.On("GetKeyPool", poolID).Return(keyPool, nil)

					Convey("when the key pool authorizer returns false", func() {
						poolAuthorizer.On("AuthorizeByClientID", mock.Anything, mock.Anything).Return(false)

						Convey("then we return that the request is not authorized", func() {
							So(authorizer.AuthorizeByClientID(ctx, keyBatch), ShouldBeFalse)
						})
					})

					Convey("when the key pool authorizer returns true", func() {
						poolAuthorizer.On("AuthorizeByClientID", mock.Anything, mock.Anything).Return(true)

						Convey("then we return that the request is authorized", func() {
							So(authorizer.AuthorizeByClientID(ctx, keyBatch), ShouldBeTrue)
						})
					})
				})
			})
		})
	})
}
