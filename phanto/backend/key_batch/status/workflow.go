package status

const (
	KeyBatchWorkflowStatusCreated    = KeyBatchWorkflowStatus("CREATED")
	KeyBatchWorkflowStatusInProgress = KeyBatchWorkflowStatus("IN_PROGRESS")
	KeyBatchWorkflowStatusSucceeded  = KeyBatchWorkflowStatus("SUCCEEDED")
	KeyBatchWorkflowStatusFailed     = KeyBatchWorkflowStatus("FAILED")
)

type KeyBatchWorkflowStatus string

func (s KeyBatchWorkflowStatus) String() string {
	return string(s)
}

func (s KeyBatchWorkflowStatus) IsActive() bool {
	return s == KeyBatchWorkflowStatusSucceeded
}
