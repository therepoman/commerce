package status

const (
	KeyBatchStatusActive      = KeyBatchStatus("ACTIVE")
	KeyBatchStatusInactive    = KeyBatchStatus("INACTIVE")
	KeyBatchStatusInvalidated = KeyBatchStatus("INVALIDATED")
)

var Statuses = map[KeyBatchStatus]interface{}{
	KeyBatchStatusActive:      nil,
	KeyBatchStatusInactive:    nil,
	KeyBatchStatusInvalidated: nil,
}

type KeyBatchStatus string

func (s KeyBatchStatus) String() string {
	return string(s)
}

func (s KeyBatchStatus) IsActive() bool {
	return s != KeyBatchStatusInactive && s != KeyBatchStatusInvalidated
}
