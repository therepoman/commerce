package key_set

import (
	"errors"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/clients/redis"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
)

type Checker interface {
	DoesBatchIDUseRedisKeySet(batchID string) (bool, error)
	DoesPoolIDUseRedisKeySet(poolID string) (bool, error)
	DoesPoolUseRedisKeySet(keyPool *key_pool.KeyPool) bool
	GetKeyCount(batchID string) (int64, error)
}

type checker struct {
	KeyBatchDAO key_batch.DAO `inject:""`
	KeyPoolDAO  key_pool.DAO  `inject:""`
	RedisClient redis.Client  `inject:""`
}

func NewChecker() Checker {
	return &checker{}
}

func (c *checker) DoesBatchIDUseRedisKeySet(batchID string) (bool, error) {
	if strings.Blank(batchID) {
		return false, nil
	}

	keyBatch, err := c.KeyBatchDAO.GetKeyBatch(batchID)
	if err != nil {
		return false, err
	}

	if keyBatch == nil {
		return false, errors.New("key batch not found")
	}

	return c.DoesPoolIDUseRedisKeySet(keyBatch.PoolID)
}

func (c *checker) DoesPoolIDUseRedisKeySet(poolID string) (bool, error) {
	if strings.Blank(poolID) {
		return false, nil
	}

	keyPool, err := c.KeyPoolDAO.GetKeyPool(poolID)
	if err != nil {
		return false, err
	}

	if keyPool == nil {
		return false, errors.New("key pool not found")
	}

	return c.DoesPoolUseRedisKeySet(keyPool), nil
}

func (c *checker) DoesPoolUseRedisKeySet(keyPool *key_pool.KeyPool) bool {
	if keyPool == nil {
		return false
	}

	redemptionModeInt, ok := phanto.RedemptionMode_value[keyPool.RedemptionMode]
	if !ok {
		return false
	}

	redemptionMode := phanto.RedemptionMode(redemptionModeInt)
	return redemptionMode == phanto.RedemptionMode_REDEEM_BY_BATCH_ID
}

func (c *checker) GetKeyCount(batchID string) (int64, error) {
	return c.RedisClient.CountSetMembers(redisSetKey(batchID))
}
