package key_set

import (
	"fmt"

	"code.justin.tv/commerce/phanto/backend/clients/redis"
	"code.justin.tv/commerce/phanto/backend/key/code"
)

type Editor interface {
	AddToKeySet(batchID string, keys []code.RawHashed) error
	RemoveFromKeySet(batchID string, keys []code.RawHashed) error
	PopRandomKeyFromKeySet(batchID string) (code.RawHashed, error)
}

type editor struct {
	RedisClient redis.Client `inject:""`
}

func NewEditor() Editor {
	return &editor{}
}

func redisSetKey(batchID string) string {
	return fmt.Sprintf("key-set.%s", batchID)
}

func (e *editor) AddToKeySet(batchID string, keys []code.RawHashed) error {
	return e.RedisClient.AddToSet(redisSetKey(batchID), toStringSlice(keys))
}

func (e *editor) RemoveFromKeySet(batchID string, keys []code.RawHashed) error {
	return e.RedisClient.RemoveFromSet(redisSetKey(batchID), toStringSlice(keys))
}

func (e *editor) PopRandomKeyFromKeySet(batchID string) (code.RawHashed, error) {
	key, err := e.RedisClient.PopRandomMemberFromSet(redisSetKey(batchID))
	if err != nil {
		return "", err
	}
	return code.RawHashed(key), nil
}

func toStringSlice(keys []code.RawHashed) []string {
	keysStr := make([]string, len(keys))
	for i, key := range keys {
		keysStr[i] = string(key)
	}
	return keysStr
}
