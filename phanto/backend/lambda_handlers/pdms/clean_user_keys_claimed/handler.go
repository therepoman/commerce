package clean_user_keys_claimed

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"github.com/pkg/errors"
)

type lambdaHandler struct {
	KeysDAO key.DAO
}

func NewLambdaHandler(keysDAO key.DAO) *lambdaHandler {
	return &lambdaHandler{
		KeysDAO: keysDAO,
	}
}

func (h *lambdaHandler) Handle(ctx context.Context, input *pdms.CleanUserKeysClaimedRequest) (*pdms.CleanUserKeysClaimedResponse, error) {
	err := pdms.IsValid(input)
	if err != nil {
		return nil, err
	}

	allKeys := make([]key.Key, 0)

	for _, userID := range input.UserIDs {
		var keyCursor string
		var keys []key.Key
		for {
			keys, keyCursor, err = h.KeysDAO.GetKeysClaimedByUser(userID, keyCursor)
			if err != nil {
				return nil, errors.Wrap(err, "error getting keys from claimed_by")
			}
			for i := range keys {
				keys[i].ClaimedBy = ""
			}
			allKeys = append(allKeys, keys...)
			if strings.Blank(keyCursor) {
				break
			}
		}
	}

	if input.IsDryRun {
		logrus.WithField("keysToClean", len(allKeys)).Info("dry run, not cleaning keys in DB")
	} else {
		err = h.KeysDAO.UpdateBatch(allKeys)
		if err != nil {
			return nil, err
		}
	}
	return &pdms.CleanUserKeysClaimedResponse{
		UserDeletionResponseData: pdms.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
