package clean_user_keys_claimed

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	key_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a lambda handler", t, func() {
		keysDAO := new(key_mock.DAO)

		h := NewLambdaHandler(keysDAO)

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}

		Convey("returns error", func() {
			var input *pdms.CleanUserKeysClaimedRequest

			Convey("when the input is invalid", func() {
				input = &pdms.CleanUserKeysClaimedRequest{}
			})

			Convey("when the keys DAO fails to look up keys", func() {
				input = &pdms.CleanUserKeysClaimedRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keysDAO.On("GetKeysClaimedByUser", userID, "").Return(nil, "", errors.New("WALRUS STRIKE"))
			})

			Convey("when the keys DAO fails to update the records", func() {
				input = &pdms.CleanUserKeysClaimedRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keyHash, _ := code.Raw(random.String(32)).Hash()
				keyRecord := key.Key{
					KeyCode:   keyHash,
					ClaimedBy: userID,
				}
				keysDAO.On("GetKeysClaimedByUser", userID, "").Return([]key.Key{
					keyRecord,
				}, "", nil)
				keysDAO.On("UpdateBatch", mock.MatchedBy(isKeyBatchCleanOfClaimedBy)).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := h.Handle(ctx, input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *pdms.CleanUserKeysClaimedRequest
			var numberOfCalls int

			Convey("when it's a dry run", func() {
				input = &pdms.CleanUserKeysClaimedRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:  userIDs,
						IsDryRun: true,
					},
				}
				keyHash, _ := code.Raw(random.String(32)).Hash()
				keyRecord := key.Key{
					KeyCode:   keyHash,
					ClaimedBy: userID,
				}
				keysDAO.On("GetKeysClaimedByUser", userID, "").Return([]key.Key{
					keyRecord,
				}, "", nil)
				numberOfCalls = 0
			})

			Convey("when it's a real run", func() {
				input = &pdms.CleanUserKeysClaimedRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keyHash, _ := code.Raw(random.String(32)).Hash()
				keyRecord := key.Key{
					KeyCode:   keyHash,
					ClaimedBy: userID,
				}
				keysDAO.On("GetKeysClaimedByUser", userID, "").Return([]key.Key{
					keyRecord,
				}, "", nil)
				keysDAO.On("UpdateBatch", mock.MatchedBy(isKeyBatchCleanOfClaimedBy)).Return(nil)
				numberOfCalls = 1
			})

			resp, err := h.Handle(ctx, input)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, userIDs)
			keysDAO.AssertNumberOfCalls(t, "UpdateBatch", numberOfCalls)
		})
	})
}

func isKeyBatchCleanOfClaimedBy(keys []key.Key) bool {
	for _, k := range keys {
		if k.ClaimedBy != "" {
			return false
		}
	}
	return true
}
