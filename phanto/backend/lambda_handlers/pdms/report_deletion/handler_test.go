package report_deletion

import (
	"context"
	"errors"
	"testing"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	pdms_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/amzn/PDMSLambdaTwirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a PDMS report deletion handler", t, func() {
		pdmsClient := new(pdms_mock.PDMSService)
		handler := &lambdaHandler{
			PDMS: pdmsClient,
		}

		ctx := context.Background()
		userID := "123123123"

		Convey("returns error", func() {
			var input *pdms.ReportDeletionRequest

			Convey("when the input is nil", func() {
				input = nil
			})

			Convey("when the input contains no user ID", func() {
				input = &pdms.ReportDeletionRequest{}
			})

			Convey("when we get an error from PDMS when reporting deletion is enabled", func() {
				input = &pdms.ReportDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.On("ReportDeletion", ctx, mock.MatchedBy(func(req *pdms_service.ReportDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID
				})).Return(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := handler.Handle(ctx, input)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *pdms.ReportDeletionRequest
			var numberOfCalls int

			Convey("when we successfully call PDMS with valid input", func() {
				input = &pdms.ReportDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.On("ReportDeletion", ctx, mock.MatchedBy(func(req *pdms_service.ReportDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID
				})).Return(&pdms_service.ReportDeletionPayload{}, nil)
				numberOfCalls = 1
			})

			Convey("when it is a dry run", func() {
				input = &pdms.ReportDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:  []string{userID},
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when reporting deletion is disabled and it's not a dry run", func() {
				input = &pdms.ReportDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: []string{userID},
					},
				}
				numberOfCalls = 0
			})

			resp, err := handler.Handle(ctx, input)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, []string{userID})
			pdmsClient.AssertNumberOfCalls(t, "ReportDeletion", numberOfCalls)
		})
	})
}
