package report_deletion

import (
	"context"
	"time"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

type lambdaHandler struct {
	PDMS pdms_service.PDMSService `inject:"pdms"`
}

func NewLambdaHandler(pdmsClient pdms_service.PDMSService) *lambdaHandler {
	return &lambdaHandler{
		PDMS: pdmsClient,
	}
}

func (h *lambdaHandler) Handle(ctx context.Context, input *pdms.ReportDeletionRequest) (*pdms.ReportDeletionResponse, error) {
	err := pdms.IsValid(input)
	if err != nil {
		return nil, err
	}

	timeOfDeletion := time.Now()

	if input.IsDryRun {
		logrus.Info("Dry Run, not reporting deletion to PDMS")
	} else if !input.ReportDeletion {
		logrus.Info("not reporting deletion to PDMS due to input has it disabled")
	} else {
		timeOfDeletionTwirp, err := ptypes.TimestampProto(timeOfDeletion)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert timestamp of deletion to ptypes timestamp")
		}

		for _, userID := range input.UserIDs {
			_, err = h.PDMS.ReportDeletion(ctx, &pdms_service.ReportDeletionRequest{
				UserId:    userID,
				ServiceId: pdms.PhantoServiceCatalogID,
				Timestamp: timeOfDeletionTwirp,
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to report deletion to PDMS")
			}
		}
	}

	return &pdms.ReportDeletionResponse{
		UserDeletionResponseData: pdms.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
		Timestamp: timeOfDeletion,
	}, nil
}
