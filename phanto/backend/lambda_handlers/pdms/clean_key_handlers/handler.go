package clean_key_handlers

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"github.com/pkg/errors"
)

type lambdaHandler struct {
	KeyHandlersDAO key_handler_group.DAO
}

func NewLambdaHandler(keyHandlersDAO key_handler_group.DAO) *lambdaHandler {
	return &lambdaHandler{
		KeyHandlersDAO: keyHandlersDAO,
	}
}

func (h *lambdaHandler) Handle(ctx context.Context, input *pdms.CleanKeyHandlersRequest) (*pdms.CleanKeyHandlersResponse, error) {
	err := pdms.IsValid(input)
	if err != nil {
		return nil, err
	}

	keyHandlerGroupsToUpdate := make([]*key_handler_group.KeyHandlerGroup, 0)

	var keyCursor string
	var keyHandlerGroups []*key_handler_group.KeyHandlerGroup
	for {
		keyHandlerGroups, keyCursor, err = h.KeyHandlersDAO.GetKeyHandlerGroups(keyCursor)
		if err != nil {
			return nil, errors.Wrap(err, "error getting keyHandlerGroups")
		}
		for _, khg := range keyHandlerGroups {
			keyHandlerGroup := khg
			if len(keyHandlerGroup.AuthorizedTUIDs) > 0 {
				foundUser := false
				for _, userID := range input.UserIDs {
					if _, ok := keyHandlerGroup.AuthorizedTUIDs[userID]; ok {
						foundUser = true
						delete(keyHandlerGroup.AuthorizedTUIDs, userID)
					}
				}
				if foundUser {
					keyHandlerGroupsToUpdate = append(keyHandlerGroupsToUpdate, keyHandlerGroup)
				}
			}
		}

		if strings.Blank(keyCursor) {
			break
		}
	}

	if input.IsDryRun {
		logrus.WithField("keysToClean", len(keyHandlerGroupsToUpdate)).Info("dry run, not cleaning keys in DB")
	} else {
		for _, keyHandlerGroup := range keyHandlerGroupsToUpdate {
			err = h.KeyHandlersDAO.CreateOrUpdate(keyHandlerGroup)
			if err != nil {
				return nil, err
			}
		}
	}
	return &pdms.CleanKeyHandlersResponse{
		UserDeletionResponseData: pdms.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
