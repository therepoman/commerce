package clean_key_handlers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	key_handler_group_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a lambda handler", t, func() {
		keyHandlerDAO := new(key_handler_group_mock.DAO)

		h := &lambdaHandler{
			KeyHandlersDAO: keyHandlerDAO,
		}

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}

		Convey("returns error", func() {
			var input *pdms.CleanKeyHandlersRequest

			Convey("when the input is invalid", func() {
				input = &pdms.CleanKeyHandlersRequest{}
			})

			Convey("when we fail to lookup key handlers in the DB", func() {
				input = &pdms.CleanKeyHandlersRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keyHandlerDAO.On("GetKeyHandlerGroups", "").Return(nil, "", errors.New("WALRUS STRIKE"))
			})

			Convey("when we update key handlers in the DB", func() {
				input = &pdms.CleanKeyHandlersRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keyHandlers := []*key_handler_group.KeyHandlerGroup{{
					AuthorizedTUIDs: map[string]bool{
						userID: true,
					},
				}}
				keyHandlerDAO.On("GetKeyHandlerGroups", "").Return(keyHandlers, "", nil)
				keyHandlerDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := h.Handle(ctx, input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *pdms.CleanKeyHandlersRequest
			var numberOfCalls int

			Convey("user is not found", func() {
				input = &pdms.CleanKeyHandlersRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keyHandlers := []*key_handler_group.KeyHandlerGroup{{
					AuthorizedTUIDs: map[string]bool{
						"432432432": true,
					},
				}}
				keyHandlerDAO.On("GetKeyHandlerGroups", "").Return(keyHandlers, "", nil)
				numberOfCalls = 0
			})

			Convey("user is found, dry run", func() {
				input = &pdms.CleanKeyHandlersRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:  userIDs,
						IsDryRun: true,
					},
				}
				keyHandlers := []*key_handler_group.KeyHandlerGroup{{
					AuthorizedTUIDs: map[string]bool{
						userID: true,
					},
				}}
				keyHandlerDAO.On("GetKeyHandlerGroups", "").Return(keyHandlers, "", nil)
				numberOfCalls = 0
			})

			Convey("user is found, real run", func() {
				input = &pdms.CleanKeyHandlersRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				keyHandlers := []*key_handler_group.KeyHandlerGroup{{
					AuthorizedTUIDs: map[string]bool{
						userID: true,
					},
				}}
				keyHandlerDAO.On("GetKeyHandlerGroups", "").Return(keyHandlers, "", nil)
				numberOfCalls = 1
				keyHandlerDAO.On("CreateOrUpdate", mock.Anything).Return(nil)
			})

			resp, err := h.Handle(ctx, input)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, userIDs)
			keyHandlerDAO.AssertNumberOfCalls(t, "CreateOrUpdate", numberOfCalls)
		})
	})
}
