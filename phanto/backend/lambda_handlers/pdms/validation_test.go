package pdms

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsValid(t *testing.T) {
	Convey("given a validation function for PDMS step function input", t, func() {
		Convey("returns error when", func() {
			// a struct that implements the UserDeletionRequest interface
			var input *CleanUserKeysClaimedRequest

			Convey("input is nil", func() {
				input = nil
			})
			Convey("list of user IDs is empty", func() {
				input = &CleanUserKeysClaimedRequest{}
			})
			Convey("list of user IDs contains empty IDs", func() {
				input = &CleanUserKeysClaimedRequest{
					UserDeletionRequestData: UserDeletionRequestData{
						UserIDs: []string{"123123123", ""},
					},
				}
			})

			err := IsValid(input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when", func() {
			var input *CleanUserKeysClaimedRequest

			Convey("input is valid", func() {
				input = &CleanUserKeysClaimedRequest{
					UserDeletionRequestData: UserDeletionRequestData{
						UserIDs: []string{"123123123", "456456456"},
					},
				}
			})

			err := IsValid(input)
			So(err, ShouldBeNil)
		})
	})
}
