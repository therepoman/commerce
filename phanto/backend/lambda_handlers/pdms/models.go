package pdms

import "time"

const (
	PhantoServiceCatalogID = "369"
)

type UserDeletionRequest interface {
	GetUserDeletionRequestData() *UserDeletionRequestData
}

type UserDeletionRequestData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type UserDeletionResponseData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type StartUserDeletionRequest struct {
	UserDeletionRequestData
}

func (r *StartUserDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanUserKeysClaimedRequest struct {
	UserDeletionRequestData
}

func (r *CleanUserKeysClaimedRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanUserKeysClaimedResponse struct {
	UserDeletionResponseData
}

type CleanKeyHandlersRequest struct {
	UserDeletionRequestData
}

func (r *CleanKeyHandlersRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanKeyHandlersResponse struct {
	UserDeletionResponseData
}

type ReportDeletionRequest struct {
	UserDeletionRequestData
}

func (r *ReportDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type ReportDeletionResponse struct {
	UserDeletionResponseData
	Timestamp time.Time `json:"timestamp"`
}
