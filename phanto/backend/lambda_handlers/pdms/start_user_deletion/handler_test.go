package start_user_deletion

import (
	"context"
	"errors"
	"fmt"
	"testing"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"code.justin.tv/commerce/phanto/config"
	pdms_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/amzn/PDMSLambdaTwirp"
	stepfn_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/stepfn"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a start user deletion handler", t, func() {
		pdmsClient := new(pdms_mock.PDMSService)
		sfnClient := new(stepfn_mock.Client)
		cfg := &config.Config{
			StepFn: config.StepFnConfig{
				UserDeletionStepFn: config.StepFnStateMachineConfig{
					StateMachineARN: "walrus_deletion_func",
				},
			},
			PDMS: config.PDMS{
				DryRun:         false,
				ReportDeletion: true,
			},
		}

		h := &lambdaHandler{
			PDMS:      pdmsClient,
			SFNClient: sfnClient,
			Config:    cfg,
		}

		ctx := context.Background()
		userID := "123123123"
		executionName := fmt.Sprintf(executionNameFormat, userID)

		Convey("returns nil", func() {
			Convey("when we successfully start the step function and promise deletion", func() {
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(nil)
				pdmsClient.On("PromiseDeletion", ctx, mock.MatchedBy(func(req *pdms_service.PromiseDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID && !req.AutoResolve
				})).Return(nil, nil)
			})

			Convey("when we have already started the step function and then promise deletion", func() {
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				pdmsClient.On("PromiseDeletion", ctx, mock.MatchedBy(func(req *pdms_service.PromiseDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID && !req.AutoResolve
				})).Return(nil, nil)
			})

			Convey("when we have already started the step function and also have promised deletion", func() {
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				pdmsClient.On("PromiseDeletion", ctx, mock.MatchedBy(func(req *pdms_service.PromiseDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID && !req.AutoResolve
				})).Return(nil, twirp.NewError(twirp.AlreadyExists, "WALRUS STRIKE"))
			})

			Convey("when we successfully start the step function and don't promise deletion since it's a dry run", func() {
				h.Config.PDMS.DryRun = true
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(nil)
			})

			Convey("when we successfully start the step function and don't promise deletion since it's disabled", func() {
				h.Config.PDMS.ReportDeletion = false
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(nil)
			})

			err := h.Handle(ctx, nil, &user.Destroy{
				UserId: userID,
			})

			So(err, ShouldBeNil)
		})

		Convey("returns error", func() {
			Convey("when we fail to invoke the step function", func() {
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(errors.New("WALRUS STRIKE"))
			})

			Convey("when we successfully start the step function and fail to promise deletion", func() {
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(nil)
				pdmsClient.On("PromiseDeletion", ctx, mock.MatchedBy(func(req *pdms_service.PromiseDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID && !req.AutoResolve
				})).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we have already started the step function and fail to promise deletion", func() {
				sfnClient.On("Execute", ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
					UserDeletionRequestData: pdms.UserDeletionRequestData{
						UserIDs:        []string{userID},
						IsDryRun:       h.Config.PDMS.DryRun,
						ReportDeletion: h.Config.PDMS.ReportDeletion,
					},
				}).Return(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				pdmsClient.On("PromiseDeletion", ctx, mock.MatchedBy(func(req *pdms_service.PromiseDeletionRequest) bool {
					return req.UserId == userID && req.ServiceId == pdms.PhantoServiceCatalogID && !req.AutoResolve
				})).Return(nil, errors.New("WALRUS STRIKE"))
			})

			err := h.Handle(ctx, nil, &user.Destroy{
				UserId: userID,
			})

			So(err, ShouldNotBeNil)
		})
	})
}
