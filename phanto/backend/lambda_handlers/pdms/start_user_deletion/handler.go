package start_user_deletion

import (
	"context"
	"fmt"
	"time"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"code.justin.tv/commerce/phanto/backend/stepfn"
	"code.justin.tv/commerce/phanto/config"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

const (
	promiseDeleteNumOfDays = 30 * 24 * time.Hour // this value may change depending on recommendations from PDMS
	executionNameFormat    = "%s.delete"
)

type lambdaHandler struct {
	PDMS      pdms_service.PDMSService
	SFNClient stepfn.Client
	Config    *config.Config
}

func NewLambdaHandler(pdmsClient pdms_service.PDMSService, sfnClient stepfn.Client, cfg *config.Config) *lambdaHandler {
	return &lambdaHandler{
		PDMS:      pdmsClient,
		SFNClient: sfnClient,
		Config:    cfg,
	}
}

func (h *lambdaHandler) Handle(ctx context.Context, header *eventbus.Header, event *user.Destroy) error {
	log.WithField("userID", event.UserId).Info("Starting User Destroy Step Function")

	executionName := fmt.Sprintf(executionNameFormat, event.UserId)
	err := h.SFNClient.Execute(ctx, h.Config.StepFn.UserDeletionStepFn.StateMachineARN, executionName, &pdms.StartUserDeletionRequest{
		UserDeletionRequestData: pdms.UserDeletionRequestData{
			UserIDs:        []string{event.UserId},
			IsDryRun:       h.Config.PDMS.DryRun,
			ReportDeletion: h.Config.PDMS.ReportDeletion,
		},
	})
	if err != nil {
		var existingExecutionError bool
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				existingExecutionError = true
			}
		}

		if existingExecutionError {
			log.WithField("userID", event.UserId).Info("found existing User Destroy SFN execution, skipping.")
		} else {
			log.WithField("userID", event.UserId).WithError(err).Error("error creating User Destroy SFN execution")
			return err
		}
	}
	timeOfDeletionPromise := time.Now().Add(promiseDeleteNumOfDays)
	timeOfDeletionPromiseTwirp, err := ptypes.TimestampProto(timeOfDeletionPromise)
	if err != nil {
		return err
	}
	log := log.WithFields(log.Fields{
		"userID":    event.UserId,
		"timestamp": timeOfDeletionPromise,
	})
	if h.Config.PDMS.DryRun {
		log.Info("Not sending to PDMS to promise deletion due to Dry Run")
		return nil
	} else if !h.Config.PDMS.ReportDeletion {
		log.Info("Not sending to PDMS to promise deletion due to reporting deletion being disabled")
		return nil
	} else {
		log.Info("Sending PromiseDeletion to PDMS")
		_, err = h.PDMS.PromiseDeletion(ctx, &pdms_service.PromiseDeletionRequest{
			UserId:      event.UserId,
			ServiceId:   pdms.PhantoServiceCatalogID,
			Timestamp:   timeOfDeletionPromiseTwirp,
			AutoResolve: false,
		})

		if err == nil {
			return nil
		}
		// PDMSClientWrapper's `promiseDeletion` function returns an HttpError wrapping a twirp error.
		if twirpError, ok := err.(twirp.Error); ok {
			if twirpError.Code() == twirp.AlreadyExists {
				log.Info("User has already been deleted in PDMS")
				return nil
			}
		}

		log.WithError(err).Error("error sending user PromiseDeletion to PDMS")
		return err
	}
}
