package key_generation

import (
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/crypto/encryption/aes"
	dynamo_key_batch "code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/maker"
	"code.justin.tv/commerce/phanto/backend/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/config"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
)

type Job struct {
	BatchID string `json:"batch_id"`
}

func JobFromJSON(jsonStr string) (*Job, error) {
	var job Job
	err := json.Unmarshal([]byte(jsonStr), &job)
	if err != nil {
		return nil, err
	}
	return &job, nil
}

type Worker struct {
	Config        *config.Config       `inject:""`
	S3Client      s3.Client            `inject:""`
	KeyBatchDAO   dynamo_key_batch.DAO `inject:""`
	KeyPoolDAO    key_pool.DAO         `inject:""`
	KeyMaker      maker.KeyMaker       `inject:""`
	KMSClient     kms.Client           `inject:""`
	KeySetEditor  key_set.Editor       `inject:""`
	KeySetChecker key_set.Checker      `inject:""`
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx := context.Background()

	snsMsg, err := sns.Extract(msg)
	if err != nil {
		return err
	}

	job, err := JobFromJSON(snsMsg.Message)
	if err != nil {
		return errors.Wrap(err, "error extracting job from sns json")
	}

	batch, err := w.KeyBatchDAO.GetKeyBatch(job.BatchID)
	if err != nil {
		return errors.Wrap(err, "error looking up batch")
	}

	if batch == nil {
		return errors.New("batch not found")
	}

	if batch.KeyBatchWorkflowStatus != status.KeyBatchWorkflowStatusCreated {
		return errors.New("batch not in correct status to start")
	}

	pool, err := w.KeyPoolDAO.GetKeyPool(batch.PoolID)
	if err != nil {
		return errors.Wrap(err, "error looking up pool")
	}

	if pool == nil {
		return errors.New("pool not found")
	}

	batch.KeyBatchWorkflowStatus = status.KeyBatchWorkflowStatusInProgress
	err = w.KeyBatchDAO.CreateOrUpdate(batch)
	if err != nil {
		return errors.Wrap(err, "error marking key batch in progress")
	}

	hashedKeys := make([]code.RawHashed, batch.NumberOfKeys)
	byteBuffer := new(bytes.Buffer)
	csvWriter := csv.NewWriter(byteBuffer)
	for i := int64(0); i < batch.NumberOfKeys; i++ {
		key, hashedKey, err := w.KeyMaker.MakeKey(batch.BatchID)
		if err != nil {
			return errors.Wrap(err, "error making key")
		}
		err = csvWriter.Write([]string{string(key.Format())})
		if err != nil {
			return errors.Wrap(err, "error writing key to csv")
		}
		hashedKeys[i] = hashedKey
	}
	csvWriter.Flush()

	if w.KeySetChecker.DoesPoolUseRedisKeySet(pool) {
		err = w.KeySetEditor.AddToKeySet(batch.BatchID, hashedKeys)
		if err != nil {
			return errors.Wrap(err, "error adding key to key_set")
		}
	}

	dataKeyOut, err := w.KMSClient.GenerateDataKey(w.Config.KMSMasterKeyARN, kms.KeySpecAES256, map[string]string{
		"batch_id": batch.BatchID,
	})
	if err != nil {
		return errors.Wrap(err, "error generating kms data key")
	}
	if dataKeyOut == nil {
		return errors.New("kms returned nil response")
	}

	encrypted, err := aes.Encrypt(dataKeyOut.Plaintext, byteBuffer.Bytes())
	if err != nil {
		return errors.Wrap(err, "error encrypting key csv file")
	}

	err = w.S3Client.PutFile(ctx, w.Config.KeyBatchS3Bucket, key_batch.GetEncryptedS3Key(pool.PoolID, batch.BatchID), bytes.NewReader(encrypted))
	if err != nil {
		return errors.Wrap(err, "error saving key file to s3")
	}

	batch.Cipher = dataKeyOut.CiphertextBlob
	batch.KeyBatchWorkflowStatus = status.KeyBatchWorkflowStatusSucceeded
	batch.KeyBatchStatus = status.KeyBatchStatusActive
	err = w.KeyBatchDAO.CreateOrUpdate(batch)
	if err != nil {
		return errors.Wrap(err, "error marking key batch complete")
	}

	return nil
}
