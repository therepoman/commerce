package key_generation_test

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/backend/worker/key_generation"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/maker"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func SNSMessageToJSON(msg *sns.Message) string {
	msgBytes, err := json.Marshal(msg)
	So(err, ShouldBeNil)
	return string(msgBytes)
}

func JobToJSON(job *key_generation.Job) string {
	jobBytes, err := json.Marshal(job)
	So(err, ShouldBeNil)
	return string(jobBytes)
}

func TestWorker_Handle(t *testing.T) {
	Convey("Given a key generation worker", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		s3Client := new(s3_mock.Client)
		keyBatchDAO := new(key_batch_mock.DAO)
		keyPoolDAO := new(key_pool_mock.DAO)
		keyMaker := new(maker_mock.KeyMaker)
		kmsClient := new(kms_mock.Client)
		keySetEditor := new(key_set_mock.Editor)
		keySetChecker := new(key_set_mock.Checker)

		worker := &key_generation.Worker{
			Config:        cfg,
			S3Client:      s3Client,
			KeyPoolDAO:    keyPoolDAO,
			KeyBatchDAO:   keyBatchDAO,
			KeyMaker:      keyMaker,
			KMSClient:     kmsClient,
			KeySetEditor:  keySetEditor,
			KeySetChecker: keySetChecker,
		}

		Convey("Errors when the SQS msg is nil", func() {
			err := worker.Handle(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the SQS body is nil", func() {
			err := worker.Handle(&sqs.Message{
				Body: nil,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the SQS body is invalid json", func() {
			err := worker.Handle(&sqs.Message{
				Body: pointers.StringP("not valid json"),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the SNS message body contains invalid json", func() {
			err := worker.Handle(&sqs.Message{
				Body: pointers.StringP(SNSMessageToJSON(&sns.Message{
					Message: "not valid json",
				})),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid SQS message", func() {
			batchID := "test-batch-id"
			sqsMsg := &sqs.Message{
				Body: pointers.StringP(SNSMessageToJSON(&sns.Message{
					Message: JobToJSON(&key_generation.Job{
						BatchID: batchID,
					}),
				})),
			}

			Convey("Errors when key batch dao errors on get", func() {
				keyBatchDAO.On("GetKeyBatch", batchID).Return(nil, errors.New("test error"))
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when key batch dao returns nil", func() {
				keyBatchDAO.On("GetKeyBatch", batchID).Return(nil, nil)
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when key batch dao returns key batch in wrong status", func() {
				keyBatchDAO.On("GetKeyBatch", batchID).Return(&key_batch.KeyBatch{
					BatchID:                batchID,
					KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusInProgress,
				}, nil)
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("When key batch dao returns a batch in the correct status", func() {
				poolID := "test-pool-id"
				numKeys := int64(3)
				batch := &key_batch.KeyBatch{
					BatchID:                batchID,
					PoolID:                 poolID,
					KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
					NumberOfKeys:           numKeys,
				}
				keyBatchDAO.On("GetKeyBatch", batchID).Return(batch, nil)

				Convey("When key pool dao errors on get", func() {
					keyPoolDAO.On("GetKeyPool", poolID).Return(nil, errors.New("test error"))
					err := worker.Handle(sqsMsg)
					So(err, ShouldNotBeNil)
				})

				Convey("When key pool dao returns nil pool", func() {
					keyPoolDAO.On("GetKeyPool", poolID).Return(nil, nil)
					err := worker.Handle(sqsMsg)
					So(err, ShouldNotBeNil)
				})

				Convey("When key pool dao returns a valid pool", func() {
					pool := &key_pool.KeyPool{
						PoolID: poolID,
					}
					keyPoolDAO.On("GetKeyPool", poolID).Return(pool, nil)

					Convey("Errors when key batch dao first update fails", func() {
						keyBatchDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error")).Once()
						err := worker.Handle(sqsMsg)
						So(err, ShouldNotBeNil)
					})

					Convey("When key batch dao first update succeeds", func() {
						keyBatchDAO.On("CreateOrUpdate", mock.Anything).Return(nil).Once()

						Convey("Errors when key maker errors", func() {
							keyMaker.On("MakeKey", batchID).Return(code.Raw(""), code.RawHashed(""), errors.New("test error"))
							err := worker.Handle(sqsMsg)
							So(err, ShouldNotBeNil)
						})

						Convey("When key maker returns keys", func() {
							keyMaker.On("MakeKey", batchID).Return(code.Raw("abc"), code.RawHashed("def"), nil)

							Convey("When key set checker says UsesRedisKeySet is false", func() {
								keySetChecker.On("DoesPoolUseRedisKeySet", pool).Return(false)

								Convey("When KMS errors", func() {
									kmsClient.On("GenerateDataKey", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
									err := worker.Handle(sqsMsg)
									So(err, ShouldNotBeNil)
								})

								Convey("When KMS succeeds", func() {
									kmsClient.On("GenerateDataKey", mock.Anything, mock.Anything, mock.Anything).Return(&kms.GenerateDataKeyOutput{
										CiphertextBlob: []byte("test-ciphertext-blob-12345"),
										Plaintext:      []byte("test-plaintext-key-12345"),
									}, nil)

									Convey("When S3 put errors", func() {
										s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
										err := worker.Handle(sqsMsg)
										So(err, ShouldNotBeNil)
									})

									Convey("When S3 put succeeds", func() {
										s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

										Convey("Errors when key batch dao second update fails", func() {
											keyBatchDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error")).Once()
											err := worker.Handle(sqsMsg)
											So(err, ShouldNotBeNil)
											So(len(keyBatchDAO.Calls), ShouldEqual, 3)
										})

										Convey("Succeeds when key batch dao second update succeeds", func() {
											keyBatchDAO.On("CreateOrUpdate", mock.Anything).Return(nil).Once()
											err := worker.Handle(sqsMsg)
											So(err, ShouldBeNil)
											So(len(keyMaker.Calls), ShouldEqual, numKeys)
											So(len(keyBatchDAO.Calls), ShouldEqual, 3)
										})
									})
								})
							})

							Convey("When key set checker says UsesRedisKeySet is true", func() {
								keySetChecker.On("DoesPoolUseRedisKeySet", pool).Return(true)

								Convey("When AddToKeySet errors", func() {
									keySetEditor.On("AddToKeySet", mock.Anything, mock.Anything).Return(errors.New("test error"))
									err := worker.Handle(sqsMsg)
									So(err, ShouldNotBeNil)
								})

								Convey("When AddToKeySet succeeds", func() {
									keySetEditor.On("AddToKeySet", mock.Anything, mock.Anything).Return(nil)

									Convey("When KMS errors", func() {
										kmsClient.On("GenerateDataKey", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
										err := worker.Handle(sqsMsg)
										So(err, ShouldNotBeNil)
									})

									Convey("When KMS succeeds", func() {
										kmsClient.On("GenerateDataKey", mock.Anything, mock.Anything, mock.Anything).Return(&kms.GenerateDataKeyOutput{
											CiphertextBlob: []byte("test-ciphertext-blob-12345"),
											Plaintext:      []byte("test-plaintext-key-12345"),
										}, nil)

										Convey("When S3 put errors", func() {
											s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
											err := worker.Handle(sqsMsg)
											So(err, ShouldNotBeNil)
										})

										Convey("When S3 put succeeds", func() {
											s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

											Convey("Errors when key batch dao second update fails", func() {
												keyBatchDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error")).Once()
												err := worker.Handle(sqsMsg)
												So(err, ShouldNotBeNil)
												So(len(keyBatchDAO.Calls), ShouldEqual, 3)
											})

											Convey("Succeeds when key batch dao second update succeeds", func() {
												keyBatchDAO.On("CreateOrUpdate", mock.Anything).Return(nil).Once()
												err := worker.Handle(sqsMsg)
												So(err, ShouldBeNil)
												So(len(keyMaker.Calls), ShouldEqual, numKeys)
												So(len(keyBatchDAO.Calls), ShouldEqual, 3)
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
