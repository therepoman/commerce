package report_generation

import (
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/crypto/encryption/aes"
	"code.justin.tv/commerce/gogogadget/strings"
	dynamo_key "code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/config"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
)

type Job struct {
	ReportID string `json:"report_id"`
}

func JobFromJSON(jsonStr string) (*Job, error) {
	var job Job
	err := json.Unmarshal([]byte(jsonStr), &job)
	if err != nil {
		return nil, err
	}
	return &job, nil
}

type Worker struct {
	Config       *config.Config        `inject:""`
	BatchDAO     key_batch.DAO         `inject:""`
	ReportDAO    report.DAO            `inject:""`
	KeyDAO       dynamo_key.DAO        `inject:""`
	KeyValidator key.Validator         `inject:"keyValidator"`
	S3Client     s3.Client             `inject:""`
	KMSClient    kms.Client            `inject:""`
	MetaDataDAO  key_pool_metadata.DAO `inject:""`
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx := context.Background()

	snsMsg, err := sns.Extract(msg)
	if err != nil {
		return err
	}

	job, err := JobFromJSON(snsMsg.Message)
	if err != nil {
		return errors.Wrap(err, "error extracting job from sns json")
	}

	rep, err := w.ReportDAO.GetReport(job.ReportID)
	if err != nil {
		return errors.Wrap(err, "error getting report from dynamo report DAO")
	}

	if rep == nil {
		return errors.New("report not found in dynamo")
	}

	if rep.ReportStatus != report.ReportStatusRequested && rep.ReportStatus != report.ReportStatusInProgress {
		return errors.New("report dynamo record not in correct state")
	}

	if rep.ReportType != report.ReportTypeKeyPool {
		return errors.New("received a request for a report of an unsupported type")
	}

	poolID := rep.PoolID
	if strings.Blank(poolID) {
		return errors.New("pool id in report request cannot be blank")
	}

	rep.ReportStatus = report.ReportStatusInProgress
	err = w.ReportDAO.CreateOrUpdate(rep)
	if err != nil {
		return errors.Wrap(err, "error marking report in progress")
	}

	allBatches := make([]*key_batch.KeyBatch, 0)
	var batches []*key_batch.KeyBatch
	var cursor string
	for {
		batches, cursor, err = w.BatchDAO.GetKeyBatchesByPool(poolID, cursor)
		if err != nil {
			return errors.Wrap(err, "error getting batches from pool_id")
		}
		allBatches = append(allBatches, batches...)
		if strings.Blank(cursor) {
			break
		}
	}

	allKeys := make([]dynamo_key.Key, 0)
	for _, b := range allBatches {
		if b == nil || strings.Blank(b.BatchID) {
			continue
		}

		var keyCursor string
		var keys []dynamo_key.Key
		for {
			keys, keyCursor, err = w.KeyDAO.GetKeysByBatch(b.BatchID, keyCursor)
			if err != nil {
				return errors.Wrap(err, "error getting keys from batch_id")
			}
			allKeys = append(allKeys, keys...)
			if strings.Blank(keyCursor) {
				break
			}
		}
	}

	dynamoMetaData, _, err := w.MetaDataDAO.GetKeyPoolMetadata(poolID, "")
	if err != nil {
		return errors.Wrap(err, "error getting key pool metadata")
	}

	byteBuffer := bytes.NewBuffer([]byte{})
	csvWriter := csv.NewWriter(byteBuffer)

	err = csvWriter.Write(makeCSVHeader(dynamoMetaData))
	if err != nil {
		return errors.Wrap(err, "error writing header to csv")
	}

	for _, k := range allKeys {
		record, err := w.keyToCSVRecord(ctx, k, dynamoMetaData)
		if err != nil {
			return err
		}

		err = csvWriter.Write(record)
		if err != nil {
			return errors.Wrap(err, "error writing to csv")
		}
	}
	csvWriter.Flush()

	dataKeyOut, err := w.KMSClient.GenerateDataKey(w.Config.KMSMasterKeyARN, kms.KeySpecAES256, map[string]string{
		"report_id": rep.ReportID,
	})
	if err != nil {
		return errors.Wrap(err, "error generating kms data key")
	}
	if dataKeyOut == nil {
		return errors.New("kms returned nil response")
	}

	encrypted, err := aes.Encrypt(dataKeyOut.Plaintext, byteBuffer.Bytes())
	if err != nil {
		return errors.Wrap(err, "error encrypting key csv file")
	}

	err = w.S3Client.PutFile(ctx, w.Config.ReportsS3Bucket, GetEncryptedS3Key(job.ReportID), bytes.NewReader(encrypted))
	if err != nil {
		return errors.Wrap(err, "error saving key file to s3")
	}

	rep.Cipher = dataKeyOut.CiphertextBlob
	rep.ReportStatus = report.ReportStatusFinished
	err = w.ReportDAO.CreateOrUpdate(rep)
	if err != nil {
		return errors.Wrap(err, "error marking report finished")
	}

	return nil
}

func GetPlaintextS3Key(reportID string) string {
	return fmt.Sprintf("%s/%s_plaintext_report.csv", reportID, reportID)
}

func GetEncryptedS3Key(reportID string) string {
	return fmt.Sprintf("%s/%s_encrypted_report.csv", reportID, reportID)
}

func makeCSVHeader(keyPoolMetaData []*key_pool_metadata.KeyPoolMetadata) []string {
	csvHeader := []string{
		"Hashed Key Code",
		"Status",
		"Claimer UserID",
		"Claimer Country",
		"Claimer State",
		"Claimer Zip",
		"Claim Time",
		"Valid",
		"Product Type",
		"Product",
		"Valid Start Date",
		"Valid End Date",
		"Batch ID",
		"Pool ID",
	}

	for _, metaData := range keyPoolMetaData {
		csvHeader = append(csvHeader, metaData.MetadataKey)
	}

	return csvHeader
}

func (w *Worker) keyToCSVRecord(ctx context.Context, key dynamo_key.Key, keyPoolMetaData []*key_pool_metadata.KeyPoolMetadata) ([]string, error) {
	data, err := w.KeyValidator.Validate(ctx, key.KeyCode)
	if err != nil {
		return []string{}, errors.Wrap(err, "error validating key code")
	}

	csvFields := []string{
		string(key.KeyCode),
		data.KeyStatus.String(),
		data.ClaimerUserID,
		data.ClaimerCountry,
		data.ClaimerState,
		data.ClaimerZip,
		timeOrBlank(data.ClaimTime),
		strconv.FormatBool(data.IsValid),
		data.ProductType,
		data.SKU,
		timeOrBlank(data.StartDate),
		timeOrBlank(data.EndDate),
		data.KeyBatchID,
		data.KeyPoolID,
	}

	for _, metaData := range keyPoolMetaData {
		csvFields = append(csvFields, metaData.MetadataValue)
	}

	return csvFields, nil
}

func timeOrBlank(d time.Time) string {
	if d.After(time.Time{}) {
		return d.String()
	}
	return ""
}
