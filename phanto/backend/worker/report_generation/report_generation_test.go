package report_generation_test

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/phanto/backend/data"
	dynamo_key "code.justin.tv/commerce/phanto/backend/dynamo/key"
	dynamo_batch "code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	dynamo_report "code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/worker/report_generation"
	"code.justin.tv/commerce/phanto/config"
	kms_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/kms"
	s3_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	dynamo_key_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key"
	key_batch_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	dynamo_key_pool_metadta_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	report_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/report"
	key_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func SNSMessageToJSON(msg *sns.Message) string {
	msgBytes, err := json.Marshal(msg)
	So(err, ShouldBeNil)
	return string(msgBytes)
}

func JobToJSON(job *report_generation.Job) string {
	jobBytes, err := json.Marshal(job)
	So(err, ShouldBeNil)
	return string(jobBytes)
}

func TestWorker_Handle(t *testing.T) {
	Convey("Given a worker", t, func() {
		ctx := context.Background()
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)

		poolID := "test-pool-id"
		batchDAO := new(key_batch_mock.DAO)
		reportDAO := new(report_mock.DAO)
		keyDAO := new(dynamo_key_mock.DAO)
		keyValidator := new(key_mock.Validator)
		s3Client := new(s3_mock.Client)
		kmsClient := new(kms_mock.Client)
		keyPoolMetadataDAO := new(dynamo_key_pool_metadta_mock.DAO)

		worker := &report_generation.Worker{
			Config:       cfg,
			BatchDAO:     batchDAO,
			ReportDAO:    reportDAO,
			KeyDAO:       keyDAO,
			KeyValidator: keyValidator,
			S3Client:     s3Client,
			KMSClient:    kmsClient,
			MetaDataDAO:  keyPoolMetadataDAO,
		}

		Convey("Errors when the SQS msg is nil", func() {
			err := worker.Handle(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the SQS body is nil", func() {
			err := worker.Handle(&sqs.Message{
				Body: nil,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the SQS body is invalid json", func() {
			err := worker.Handle(&sqs.Message{
				Body: pointers.StringP("not valid json"),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the SNS message body contains invalid json", func() {
			err := worker.Handle(&sqs.Message{
				Body: pointers.StringP(SNSMessageToJSON(&sns.Message{
					Message: "not valid json",
				})),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid SQS message", func() {
			reportID := "test-report-id"
			sqsMsg := &sqs.Message{
				Body: pointers.StringP(SNSMessageToJSON(&sns.Message{
					Message: JobToJSON(&report_generation.Job{
						ReportID: reportID,
					}),
				})),
			}

			Convey("Errors when report dao errors on get", func() {
				reportDAO.On("GetReport", reportID).Return(nil, errors.New("test error")).Once()
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when report dao returns nil", func() {
				reportDAO.On("GetReport", reportID).Return(nil, nil).Once()
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when report dao returns report in incorrect state", func() {
				reportDAO.On("GetReport", reportID).Return(&dynamo_report.Report{
					ReportStatus: dynamo_report.ReportStatus("invalid status"),
					ReportType:   dynamo_report.ReportTypeKeyPool,
				}, nil).Once()
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when report dao returns report of unsupported type", func() {
				reportDAO.On("GetReport", reportID).Return(&dynamo_report.Report{
					ReportStatus: dynamo_report.ReportStatusRequested,
					ReportType:   dynamo_report.ReportType("unsupported type"),
				}, nil).Once()
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when report dao returns report with blank pool id", func() {
				reportDAO.On("GetReport", reportID).Return(&dynamo_report.Report{
					ReportStatus: dynamo_report.ReportStatusRequested,
					ReportType:   dynamo_report.ReportTypeKeyPool,
					PoolID:       "",
				}, nil).Once()
				err := worker.Handle(sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("When report dao returns a valid report", func() {
				reportDAO.On("GetReport", reportID).Return(&dynamo_report.Report{
					ReportStatus: dynamo_report.ReportStatusRequested,
					ReportType:   dynamo_report.ReportTypeKeyPool,
					PoolID:       poolID,
				}, nil)

				Convey("Errors when report dao fails to update to in-progress", func() {
					reportDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error")).Once()
					err := worker.Handle(sqsMsg)
					So(err, ShouldNotBeNil)
				})

				Convey("When report dao succeeds in updating to in-progress", func() {
					reportDAO.On("CreateOrUpdate", mock.Anything).Return(nil).Once()

					Convey("Errors when GetKeyBatchesByPool dao errors on first call", func() {
						batchDAO.On("GetKeyBatchesByPool", poolID, "").Return(nil, "", errors.New("test error")).Once()
						err := worker.Handle(sqsMsg)
						So(err, ShouldNotBeNil)
					})

					Convey("When GetKeyBatchesByPool succeeds on first call with a cursor", func() {
						batchCursor := "batch-cursor-1"
						batchDAO.On("GetKeyBatchesByPool", poolID, "").Return([]*dynamo_batch.KeyBatch{{BatchID: "1"}}, batchCursor, nil).Once()

						Convey("Errors when GetKeyBatchesByPool errors on second call", func() {
							batchDAO.On("GetKeyBatchesByPool", poolID, batchCursor).Return(nil, "", errors.New("test error")).Once()
							err := worker.Handle(sqsMsg)
							So(err, ShouldNotBeNil)
						})

						Convey("When GetKeyBatchesByPool succeeds on second call with no cursor", func() {
							batchDAO.On("GetKeyBatchesByPool", poolID, batchCursor).Return([]*dynamo_batch.KeyBatch{{BatchID: "2"}}, "", nil).Once()

							Convey("Errors when GetKeysByBatch (first batch) errors on first call", func() {
								keyDAO.On("GetKeysByBatch", "1", "").Return(nil, "", errors.New("test error")).Once()
								err := worker.Handle(sqsMsg)
								So(err, ShouldNotBeNil)
							})

							Convey("When GetKeysByBatch (first batch) succeeds on first call with a cursor", func() {
								keyCursor1 := "key-cursor-1"
								keyDAO.On("GetKeysByBatch", "1", "").Return([]dynamo_key.Key{{KeyCode: "1"}, {KeyCode: "2"}}, keyCursor1, nil).Once()

								Convey("Errors when GetKeysByBatch (first batch) errors on second call", func() {
									keyDAO.On("GetKeysByBatch", "1", keyCursor1).Return(nil, "", errors.New("test error")).Once()
									err := worker.Handle(sqsMsg)
									So(err, ShouldNotBeNil)
								})

								Convey("When GetKeysByBatch (first batch) succeeds on second call with no cursor", func() {
									keyDAO.On("GetKeysByBatch", "1", keyCursor1).Return([]dynamo_key.Key{{KeyCode: "3"}, {KeyCode: "4"}}, "", nil).Once()

									Convey("Errors when GetKeysByBatch (second batch) errors on first call for", func() {
										keyDAO.On("GetKeysByBatch", "2", "").Return(nil, "", errors.New("test error")).Once()
										err := worker.Handle(sqsMsg)
										So(err, ShouldNotBeNil)
									})

									Convey("When GetKeysByBatch (second batch) succeeds on first call with a cursor", func() {
										keyCursor2 := "key-cursor-2"
										keyDAO.On("GetKeysByBatch", "2", "").Return([]dynamo_key.Key{{KeyCode: "5"}, {KeyCode: "6"}}, keyCursor2, nil).Once()

										Convey("Errors when GetKeysByBatch (second batch) errors on second call", func() {
											keyDAO.On("GetKeysByBatch", "2", keyCursor2).Return(nil, "", errors.New("test error")).Once()
											err := worker.Handle(sqsMsg)
											So(err, ShouldNotBeNil)
										})

										Convey("When GetKeysByBatch (second batch) succeeds on second call with no cursor", func() {
											keyDAO.On("GetKeysByBatch", "2", keyCursor2).Return([]dynamo_key.Key{{KeyCode: "7"}, {KeyCode: "8"}}, "", nil).Once()

											Convey("Errors when GetKeyPoolMetadata errors", func() {
												keyPoolMetadataDAO.On("GetKeyPoolMetadata", poolID, "").Return(nil, "", errors.New("test error"))
												err := worker.Handle(sqsMsg)
												So(err, ShouldNotBeNil)
											})

											Convey("When GetKeyPoolMetadata succeeds", func() {
												keyPoolMetadataDAO.On("GetKeyPoolMetadata", poolID, "").Return([]*key_pool_metadata.KeyPoolMetadata{{PoolID: poolID}}, "", nil)

												Convey("Errors when validator errors on first key", func() {
													keyValidator.On("Validate", ctx, code.RawHashed("1")).Return(data.Validation{IsValid: false}, errors.New("validation error 1")).Once()
													err := worker.Handle(sqsMsg)
													So(err, ShouldNotBeNil)
												})

												Convey("When validator succeeds on first key", func() {
													keyValidator.On("Validate", ctx, code.RawHashed("1")).Return(data.Validation{
														IsValid: true,
													}, nil).Once()

													Convey("Errors when validator errors on second key", func() {
														keyValidator.On("Validate", ctx, code.RawHashed("2")).Return(data.Validation{IsValid: false}, errors.New("validation error 2")).Once()
														err := worker.Handle(sqsMsg)
														So(err, ShouldNotBeNil)
													})

													Convey("When validator succeeds on all keys", func() {
														keyValidator.On("Validate", ctx, mock.Anything).Return(data.Validation{
															IsValid: true,
														}, nil)

														Convey("When KMS errors", func() {
															kmsClient.On("GenerateDataKey", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
															err := worker.Handle(sqsMsg)
															So(err, ShouldNotBeNil)
														})

														Convey("When KMS succeeds", func() {
															kmsClient.On("GenerateDataKey", mock.Anything, mock.Anything, mock.Anything).Return(&kms.GenerateDataKeyOutput{
																CiphertextBlob: []byte("test-ciphertext-blob-12345"),
																Plaintext:      []byte("test-plaintext-key-12345"),
															}, nil)

															s3Client.On("PutFile", mock.Anything, mock.Anything, report_generation.GetPlaintextS3Key(reportID), mock.Anything).Return(nil).Once()

															Convey("Errors when S3 encrypted write errors", func() {
																s3Client.On("PutFile", mock.Anything, mock.Anything, report_generation.GetEncryptedS3Key(reportID), mock.Anything).Return(errors.New("test error"))
																err := worker.Handle(sqsMsg)
																So(err, ShouldNotBeNil)
															})

															Convey("When S3 write encrypted succeeds", func() {
																s3Client.On("PutFile", mock.Anything, mock.Anything, report_generation.GetEncryptedS3Key(reportID), mock.Anything).Return(nil).Once()

																Convey("Errors when dao errors marking report finished", func() {
																	reportDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
																	err := worker.Handle(sqsMsg)
																	So(err, ShouldNotBeNil)

																})

																Convey("Succeeds dao succeeds marking report finished succeeds", func() {
																	reportDAO.On("CreateOrUpdate", mock.Anything).Return(nil)
																	err := worker.Handle(sqsMsg)
																	So(err, ShouldBeNil)
																})
															})
														})
													})
												})
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
