package loader

import (
	"context"
	"errors"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/geoip"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/config"
	log "github.com/sirupsen/logrus"
)

const (
	MaxMindDBS3Key = "GeoIP2-City.mmdb"
)

type Loader interface {
	Load(ctx context.Context) (geoip.Locator, error)
}

type MaxMindS3Loader struct {
	Config   *config.Config `inject:""`
	S3Client s3.Client      `inject:""`
}

func (mmsl *MaxMindS3Loader) Load(ctx context.Context) (geoip.Locator, error) {
	if strings.Blank(mmsl.Config.MaxMindDBS3Bucket) {
		log.Error("Config does not contain MaxMindDBS3Bucket - falling back to no-op locator")
		return geoip.NewNoOpLocator(), nil
	}

	getResp, err := mmsl.S3Client.GetFile(ctx, mmsl.Config.MaxMindDBS3Bucket, MaxMindDBS3Key)
	if err != nil {
		return nil, err
	}

	if !getResp.Exists {
		return nil, errors.New("S3 MaxMindDB file does not exist")
	}

	locator, err := geoip.NewLocatorFromMaxMindDBFileContents(getResp.File)
	if err != nil {
		return nil, err
	}

	return locator, nil
}
