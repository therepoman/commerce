package loader_test

import (
	"context"
	"errors"
	"net"
	"testing"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/phanto/backend/geoip/loader"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxMindS3Loader_Load(t *testing.T) {
	Convey("Given a MaxMindS3Loader", t, func() {
		s3Client := new(s3_mock.Client)
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		loader := &loader.MaxMindS3Loader{
			Config:   cfg,
			S3Client: s3Client,
		}

		Convey("Falls back to no-op locator when S3 bucket is blank", func() {
			cfg.MaxMindDBS3Bucket = ""

			locator, err := loader.Load(context.Background())
			So(err, ShouldBeNil)

			data, err := locator.Locate(net.ParseIP("127.0.0.1"))
			So(err, ShouldBeNil)
			So(data.CountryISOCode(), ShouldEqual, "")
			So(data.StateISOCode(), ShouldEqual, "")
		})

		Convey("When config has an S3 bucket key", func() {
			cfg.MaxMindDBS3Bucket = "test-max-mind-db-s3-bucket"

			Convey("Errors when S3 fails to load", func() {
				s3Client.On("GetFile", mock.Anything, mock.Anything, mock.Anything).Return(s3.GetFileResponse{}, errors.New("test error"))
				_, err := loader.Load(context.Background())
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when S3 file does not exist", func() {
				s3Client.On("GetFile", mock.Anything, mock.Anything, mock.Anything).Return(s3.GetFileResponse{Exists: false}, nil)
				_, err := loader.Load(context.Background())
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when MMDB S3 file is invalid", func() {
				s3Client.On("GetFile", mock.Anything, mock.Anything, mock.Anything).Return(s3.GetFileResponse{
					Exists: true,
					File:   []byte("not a valid file"),
				}, nil)
				_, err := loader.Load(context.Background())
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when MMDB S3 file is valid", func() {
				mmdbBytes, err := test.LoadTestFile("GeoIP2-City-Test.mmdb")
				So(err, ShouldBeNil)

				s3Client.On("GetFile", mock.Anything, mock.Anything, mock.Anything).Return(s3.GetFileResponse{
					Exists: true,
					File:   mmdbBytes,
				}, nil)
				locator, err := loader.Load(context.Background())
				So(err, ShouldBeNil)
				So(locator, ShouldNotBeNil)
			})
		})
	})
}
