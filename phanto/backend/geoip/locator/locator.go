package locator

import (
	"errors"
	"net"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/geoip/refresher"
)

type IP string
type Country string
type State string
type Zip string

type Data struct {
	IP      IP
	Country Country
	State   State
	Zip     Zip
}

type Locator interface {
	Locate(clientIP string) (Data, error)
}

type MaxMindLocator struct {
	Refresher refresher.Refresher `inject:""`
}

func (mml *MaxMindLocator) Locate(clientIP string) (Data, error) {
	if strings.Blank(clientIP) {
		return Data{}, errors.New("cannot locate a blank ip")
	}

	ip := net.ParseIP(clientIP)
	data, err := mml.Refresher.Get().Locate(ip)
	if err != nil {
		return Data{}, err
	}

	return Data{
		IP:      IP(clientIP),
		Country: Country(data.CountryISOCode()),
		State:   State(data.StateISOCode()),
		Zip:     Zip(data.Postal.Code),
	}, nil
}
