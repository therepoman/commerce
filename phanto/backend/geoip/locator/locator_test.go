package locator_test

import (
	"errors"
	"testing"

	go_go_gadget_geoip "code.justin.tv/commerce/gogogadget/geoip"
	"code.justin.tv/commerce/phanto/backend/geoip/locator"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/geoip"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/geoip/refresher"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxMindLocator_Locate(t *testing.T) {
	Convey("Given a locator", t, func() {
		goGoGadgetGeoIPLocator := new(geoip_mock.Locator)
		refresher := new(refresher_mock.Refresher)

		refresher.On("Get").Return(goGoGadgetGeoIPLocator)

		mml := locator.MaxMindLocator{
			Refresher: refresher,
		}

		Convey("Errors when client-ip is blank", func() {
			_, err := mml.Locate("")
			So(err, ShouldNotBeNil)
		})

		Convey("Given a client-ip", func() {
			clientIP := "127.0.0.1"

			Convey("Errors when go go gadget geoip locator errors", func() {
				goGoGadgetGeoIPLocator.On("Locate", mock.Anything).Return(go_go_gadget_geoip.Data{}, errors.New("test error"))
				_, err := mml.Locate(clientIP)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when go go gadget geoip locator succeeds", func() {
				goGoGadgetGeoIPLocator.On("Locate", mock.Anything).Return(go_go_gadget_geoip.Data{
					Country:      go_go_gadget_geoip.Country{ISOCode: "US"},
					Subdivisions: []go_go_gadget_geoip.Subdivision{{ISOCode: "WA"}},
				}, nil)
				data, err := mml.Locate(clientIP)
				So(err, ShouldBeNil)
				So(data.IP, ShouldEqual, locator.IP("127.0.0.1"))
				So(data.Country, ShouldEqual, locator.Country("US"))
				So(data.State, ShouldEqual, locator.State("WA"))
			})
		})
	})
}
