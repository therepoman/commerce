package refresher

import (
	"context"
	"math/rand"
	"sync"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/gogogadget/geoip"
	"code.justin.tv/commerce/phanto/backend/geoip/loader"
	log "github.com/sirupsen/logrus"
)

const (
	refreshDelayMaxRandomOffset = time.Minute * 5
	refreshDelay                = time.Hour * 1
)

type Refresher interface {
	Get() geoip.Locator
	Init() error
	RefreshAtInterval()
}

type refresherImpl struct {
	mutex   *sync.RWMutex
	locator geoip.Locator
	Loader  loader.Loader `inject:""`
}

func NewGeoIPRefresher() Refresher {
	mutex := &sync.RWMutex{}
	return &refresherImpl{
		mutex: mutex,
	}
}

func (r *refresherImpl) Get() geoip.Locator {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	return r.locator
}

func (r *refresherImpl) Init() error {
	return r.refreshLocator()
}

func (r *refresherImpl) refreshLocator() error {
	newLocator, err := r.Loader.Load(context.Background())
	if err != nil {
		return err
	}

	r.mutex.Lock()
	defer r.mutex.Unlock()

	if r.locator != nil {
		err = r.locator.Close()
		if err != nil {
			log.WithError(err).Error("Error closing old locator")
		}
	}

	r.locator = newLocator
	return nil
}

func (r *refresherImpl) RefreshAtInterval() {
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := r.refreshLocator()
			if err != nil {
				log.WithError(err).Error("Error refreshing locator")
			}
		}
	}
}
