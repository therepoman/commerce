package redis

import (
	"time"

	"code.justin.tv/commerce/phanto/config"
	"github.com/go-redis/redis"
)

type Client interface {
	Ping() error
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
	AddToSet(setKey string, members []string) error
	RemoveFromSet(setKey string, members []string) error
	PopRandomMemberFromSet(setKey string) (string, error)
	CountSetMembers(setKey string) (int64, error)
}

type client struct {
	redisClient redis.Cmdable
}

func NewClient(cfg config.Config) Client {

	if cfg.UseRedisClusterMode {
		redisClient := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{cfg.RedisEndpoint},
		})

		return &client{
			redis.Cmdable(redisClient),
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.RedisEndpoint,
	})

	return &client{
		redis.Cmdable(redisClient),
	}
}

func (c *client) Ping() error {
	_, err := c.redisClient.Ping().Result()
	return err
}

func (c *client) Get(key string) (string, error) {
	value, err := c.redisClient.Get(key).Result()
	if err != nil && err != redis.Nil {
		return "", err
	}
	return value, nil
}

func (c *client) Set(key string, value interface{}, expiration time.Duration) error {
	return c.redisClient.Set(key, value, expiration).Err()
}

func (c *client) AddToSet(setKey string, members []string) error {
	return c.redisClient.SAdd(setKey, members).Err()
}

func (c *client) RemoveFromSet(setKey string, members []string) error {
	return c.redisClient.SRem(setKey, members).Err()
}

func (c *client) PopRandomMemberFromSet(setKey string) (string, error) {
	removedMember, err := c.redisClient.SPop(setKey).Result()
	if err != nil {
		return "", err
	}
	return removedMember, nil
}

func (c *client) CountSetMembers(setKey string) (int64, error) {
	count, err := c.redisClient.SCard(setKey).Result()
	if err != nil {
		return 0, err
	}
	return count, nil
}
