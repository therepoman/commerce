package datascience

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/hygienic/distconf"
	"code.justin.tv/hygienic/spade"
	"code.justin.tv/hygienic/spade/spadedconf"
	"code.justin.tv/hygienic/spade/spademetrics/statsdmetrics"
	"github.com/cactus/go-statsd-client/statsd"
)

// instruction:
// to add new datascience event, define the event name/struct in events.go, then implement in SpadeClient.
// You also might want to add the new Send method to noop client for unit test.

type SpadeClient interface {
	SendPhantoGenerateKeyBatchEvent(event PhantoGenerateKeyBatchEvent)
	Shutdown() error
}

type spadeClientImpl struct {
	client      spade.Client
	environment string
}

func NewSpadeClient(environment string, statter statsd.Statter) (SpadeClient, error) {
	var dconf distconf.Distconf
	config := spadedconf.Config{}
	err := config.Load(&dconf)
	if err != nil {
		return nil, err
	}

	spadeClient := spade.Client{
		Config: &config,
		Reporter: &statsdmetrics.Reporter{
			Statter: statter,
		},
	}

	err = spadeClient.Setup()
	if err != nil {
		return nil, err
	}

	// Must call Start() to drain the channel
	go func() {
		err := spadeClient.Start()
		if err != nil {
			fmt.Printf("error starting spade client: %v", err)
		}
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	err = spadeClient.VerifyEndpoint(ctx)
	if err != nil {
		spadeClient.Close()
		return nil, err
	}

	return &spadeClientImpl{
		client:      spadeClient,
		environment: environment,
	}, nil
}

func (s *spadeClientImpl) SendPhantoGenerateKeyBatchEvent(event PhantoGenerateKeyBatchEvent) {
	s.client.QueueEvents(spade.Event{
		Name:       phanto_generate_key_batch,
		Properties: event,
	})
}

func (s *spadeClientImpl) Shutdown() error {
	return s.client.Close()
}
