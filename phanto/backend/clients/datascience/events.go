package datascience

import "time"

const (
	phanto_generate_key_batch = "phanto_generate_key_batch"
)

type PhantoGenerateKeyBatchEvent struct {
	Environment            string    `json:"environment"`
	KeyPoolID              string    `json:"key_pool_id"`
	ProductType            string    `json:"product_type"`
	SKU                    string    `json:"sku"`
	RedemptionMode         string    `json:"redemption_mode"`
	Description            string    `json:"description"`
	HandlerGroupID         string    `json:"handler_group_id"`
	LegacyHandler          string    `json:"legacy_handler"`
	KeyPoolStartDate       time.Time `json:"key_pool_start_date"`
	KeyPoolEndDate         time.Time `json:"key_pool_end_date"`
	KeyBatchID             string    `json:"key_batch_id"`
	KeyBatchKeyCount       int64     `json:"key_batch_key_count"`
	KeyBatchWorkflowStatus string    `json:"key_batch_workflow_status"`
	KeyBatchStatus         string    `json:"key_batch_status"`
	LDAPUser               string    `json:"ldap_user"`
}
