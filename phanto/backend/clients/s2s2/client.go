package s2s2

import (
	"os"
	"time"

	twitchlogging "code.justin.tv/amzn/TwitchLogging"
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	cw "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"
	twitchtelemetrymiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

const (
	// Default settings
	metricFlushInterval     = 30 * time.Second
	metricBufferSize        = 100000
	metricAggregationPeriod = time.Minute
)

func NewS2S2Client(cfg *config.Config, logger twitchlogging.Logger) (*s2s2.S2S2, error) {
	return s2s2.New(&s2s2.Options{
		Config: &c7s.Config{
			ClientServiceName:   cfg.S2SServiceName,
			ServiceOrigins:      cfg.PhantoEndpoint,
			EnableAccessLogging: true,
		},
		OperationStarter: &operation.Starter{
			OpMonitors: []operation.OpMonitor{
				&twitchtelemetrymiddleware.OperationMonitor{
					SampleReporter: getSampleReporter(cfg),
				},
			},
		},
		Logger: logger,
	})
}

func getSampleReporter(cfg *config.Config) telemetry.SampleReporter {
	pid := &identifier.ProcessIdentifier{
		Service:  "phanto",
		Region:   cfg.AWSRegion,
		Stage:    cfg.EnvironmentName,
		Machine:  getHostName(),
		Substage: getSubstage(cfg),
	}
	sender := cw.NewUnbuffered(pid, nil)
	sampleObserver := telemetry.NewBufferedAggregator(metricFlushInterval, metricBufferSize, metricAggregationPeriod, sender, nil)
	sampleReporter := telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: *pid},
		SampleObserver: sampleObserver,
	}
	return sampleReporter
}

func getHostName() string {
	hostname := "unknown"
	if envHostname, err := os.Hostname(); err == nil {
		hostname = envHostname
	}
	return hostname
}

func getSubstage(cfg *config.Config) string {
	if config.IsCanary() {
		return "canary"
	}
	return cfg.EnvironmentName
}
