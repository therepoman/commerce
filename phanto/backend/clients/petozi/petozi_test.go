package petozi

import (
	"context"
	"errors"
	"testing"

	rpc "code.justin.tv/commerce/petozi/rpc"
	petozi_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPetoziClient_getBitsProduct(t *testing.T) {
	ctx := context.Background()

	Convey("Given a Petozi Client", t, func() {
		petoziRPC := new(petozi_mock.Petozi)
		petoziClient := &client{
			petoziClient: petoziRPC,
		}

		productName := "some-bits-product"
		req := &rpc.GetBitsProductReq{
			Id:       productName,
			Platform: rpc.Platform_PHANTO,
		}
		Convey("Given Error from calling Petozi", func() {
			petoziRPC.On("GetBitsProduct", ctx, req).Return(nil, errors.New("ERROR"))
			bitsProduct, err := petoziClient.getBitsProduct(ctx, productName)
			So(err, ShouldNotBeNil)
			So(bitsProduct, ShouldBeNil)
		})

		Convey("Given Bits Product does not exist", func() {
			petoziRPC.On("GetBitsProduct", ctx, req).Return(&rpc.GetBitsProductResp{}, nil)
			bitsProduct, err := petoziClient.getBitsProduct(ctx, productName)
			So(err, ShouldBeNil)
			So(bitsProduct, ShouldBeNil)
		})

		Convey("Given Bits Product exists", func() {
			petoziRPC.On("GetBitsProduct", ctx, req).Return(&rpc.GetBitsProductResp{
				BitsProduct: &rpc.BitsProduct{
					Id: productName,
				},
			}, nil)
			bitsProduct, err := petoziClient.getBitsProduct(ctx, productName)
			So(err, ShouldBeNil)
			So(bitsProduct, ShouldNotBeNil)
			So(bitsProduct.ProductType, ShouldEqual, bitsType)
			So(bitsProduct.SKU, ShouldEqual, productName)
		})
	})
}
