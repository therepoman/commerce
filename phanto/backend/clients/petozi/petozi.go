package petozi

import (
	"context"
	"net/http"
	"time"

	rpc "code.justin.tv/commerce/petozi/rpc"
	phanto_product "code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/backend/product"
	"code.justin.tv/commerce/phanto/config"
	"github.com/cenkalti/backoff"
)

const bitsType = product.ProductTypeBits

type Client interface {
	product.ProductGetter
}

type client struct {
	petoziClient rpc.Petozi
}

func NewClient(cfg config.Config) Client {
	return &client{
		petoziClient: rpc.NewPetoziProtobufClient(cfg.PetoziEndpoint, http.DefaultClient),
	}
}

// To conform to Business Logic layer interface
func (c *client) GetProduct(ctx context.Context, bitsProduct string) (*phanto_product.Product, error) {
	return c.getBitsProduct(ctx, bitsProduct)
}

func (c *client) getBitsProduct(ctx context.Context, bitsProductStr string) (*phanto_product.Product, error) {
	bo := backoff.WithMaxRetries(GetNewBackoff(), 3)
	var err error
	var bitsProduct *phanto_product.Product
	backoffFunc := func() error {
		resp, err := c.petoziClient.GetBitsProduct(ctx, &rpc.GetBitsProductReq{
			Id:       bitsProductStr,
			Platform: rpc.Platform_PHANTO,
		})
		if err != nil {
			return err
		}
		if resp.BitsProduct != nil {
			bitsProduct = &phanto_product.Product{
				ProductType: bitsType,
				SKU:         resp.BitsProduct.Id,
				Description: resp.BitsProduct.DisplayName,
			}
		}
		return nil
	}
	err = backoff.Retry(backoffFunc, bo)
	return bitsProduct, err
}

func GetNewBackoff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = 500 * time.Millisecond
	exponetial.MaxElapsedTime = 5 * time.Second
	exponetial.Multiplier = 2
	return exponetial
}
