package idempotency

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	redis_client "code.justin.tv/commerce/phanto/backend/clients/redis"
	"github.com/go-redis/redis"
)

const (
	idempotencyValue = "REQUESTED"
	idempotencyTime  = time.Hour
)

type Checker interface {
	IsNewRequest(ctx context.Context, batchID string, idempotencyKey string) (bool, error)
}

type checker struct {
	RedisClient redis_client.Client `inject:""`
}

func NewChecker() Checker {
	return &checker{}
}

func redisKey(batchID string, idempotencyKey string) string {
	return fmt.Sprintf("%s.%s", batchID, idempotencyKey)
}

func (c *checker) IsNewRequest(ctx context.Context, batchID string, idempotencyKey string) (isNew bool, err error) {
	key := redisKey(batchID, idempotencyKey)

	defer func() {
		if isNew {
			err = c.RedisClient.Set(key, idempotencyValue, idempotencyTime)
			if err != nil {
				isNew = false
			}
		}
	}()

	keyValue, err := c.RedisClient.Get(key)
	if err != nil {
		if err == redis.Nil {
			return true, nil
		}
		return false, err
	}

	return strings.Blank(keyValue), nil
}
