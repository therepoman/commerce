package audit

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
)

type Operation string

const (
	CreateKeyPoolOperation                = Operation("CreateKeyPool")
	CreateProductOperation                = Operation("CreateProduct")
	CreateProductTypeOperation            = Operation("CreateProductType")
	GenerateKeyBatchTypeOperation         = Operation("GenerateKeyBatch")
	GenerateKeyPoolReportTypeOperation    = Operation("GenerateKeyPoolReport")
	GetAllKeyPoolsOperation               = Operation("GetAllKeyPools")
	GetAllProductTypesOperation           = Operation("GetAllProductTypes")
	GetKeyBatchDownloadInfoOperation      = Operation("GetKeyBatchDownloadInfo")
	GetKeyBatchStatusOperation            = Operation("GetKeyBatchStatus")
	GetKeyBatchesOperation                = Operation("GetKeyBatches")
	GetKeyBatchesS2SOperation             = Operation("GetKeyBatchesS2S")
	GetKeyPoolReportsOperation            = Operation("GetKeyPoolReportsHandler")
	GetKeyPoolsByHandlerOperation         = Operation("GetKeyPoolsByHandler")
	GetKeyStatusOperation                 = Operation("GetKeyStatus")
	GetProductsOperation                  = Operation("GetProducts")
	InvalidateKeyBatchOperation           = Operation("InvalidateKeyBatch")
	InvalidateKeyPoolOperation            = Operation("InvalidateKeyPool")
	InvalidateKeysOperation               = Operation("InvalidateKeys")
	RedeemKeyOperation                    = Operation("RedeemKey")
	RedeemKeyForLoadTestingOperation      = Operation("RedeemKeyForLoadTesting")
	GetKeyPoolReportDownloadInfoOperation = Operation("GetKeyPoolReportDownloadInfo")
	GetKeyPoolMetadataOperation           = Operation("GetKeyPoolMetadata")
	CreateKeyPoolMetadataOperation        = Operation("CreateKeyPoolMetadata")
	DeleteKeyPoolMetadataOperation        = Operation("DeleteKeyPoolMetadata")
	UpdateKeyPoolOperation                = Operation("UpdateKeyPool")
	UpdateKeyBatchOperation               = Operation("UpdateKeyBatch")
	HelixGetKeyStatusOperation            = Operation("HelixGetKeyStatus")
	HelixRedeemKeyOperation               = Operation("HelixRedeemKey")
	UpdateKeyHandlerGroup                 = Operation("UpdateKeyHandlerGroup")
	GetKeyHandlerGroup                    = Operation("GetKeyHandlerGroup")
	GetKeyHandlerGroups                   = Operation("GetKeyHandlerGroups")
	RedeemKeyByBatchOperation             = Operation("RedeemKeyByBatch")
	HelixRedeemKeyByBatchOperation        = Operation("HelixRedeemKeyByBatch")
)

type Auditor interface {
	AuditRequest(ctx context.Context, req *Request) (record *call_audit_record.CallAuditRecord, err error)
	AuditResponse(ctx context.Context, record *call_audit_record.CallAuditRecord, resp *Response)
}

type DynamoAuditor struct {
	CallAuditRecordDAO call_audit_record.DAO `inject:""`
}

type Request struct {
	Operation Operation
	Body      interface{}
}

type Response struct {
	Body  interface{}
	Error error
}

func (da *DynamoAuditor) AuditRequest(ctx context.Context, req *Request) (record *call_audit_record.CallAuditRecord, err error) {
	defer func() {
		if err != nil {
			log.WithError(err).Error("error auditing request")
		}
	}()

	timestamp := time.Now()

	if req == nil {
		return nil, errors.New("received nil request to audit")
	}

	authenticatedUserID, _ := ctx.Value(authorization.AuthenticatedUserIDContextKey).(string)
	ldapUser, _ := ctx.Value(middleware.LDAPUserContextKey).(string)
	headers, _ := ctx.Value(middleware.RequestHeadersContextKey).(middleware.RequestHeaders)

	headerJSON, err := json.Marshal(headers)
	if err != nil {
		return nil, err
	}

	reqJSON, err := json.Marshal(req.Body)
	if err != nil {
		return nil, err
	}

	callUUID, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	auditRecord := &call_audit_record.CallAuditRecord{
		CallID:              callUUID.String(),
		Operation:           string(req.Operation),
		Timestamp:           timestamp,
		LDAPUser:            ldapUser,
		AuthenticatedUserID: authenticatedUserID,
		RequestHeadersJSON:  string(headerJSON),
		RequestJSON:         string(reqJSON),
	}
	err = da.CallAuditRecordDAO.CreateNew(auditRecord)
	if err != nil {
		return nil, err
	}

	return auditRecord, nil
}

func (da *DynamoAuditor) AuditResponse(ctx context.Context, record *call_audit_record.CallAuditRecord, resp *Response) {
	var err error

	defer func() {
		if err != nil {
			log.WithError(err).Error("error auditing response")
		}
	}()

	if record == nil {
		err = errors.New("received a nil audit record")
		return
	}

	if resp == nil {
		err = errors.New("received nil response to audit")
		return
	}

	if strings.Blank(record.CallID) {
		err = errors.New("received a blank call id")
		return
	}

	respJSON, err := json.Marshal(resp.Body)
	if err != nil {
		return
	}

	record.Latency = time.Since(record.Timestamp)
	record.ResponseJSON = string(respJSON)
	if resp.Error != nil {
		record.ResponseError = resp.Error.Error()
	}
	err = da.CallAuditRecordDAO.CreateOrUpdate(record)
}
