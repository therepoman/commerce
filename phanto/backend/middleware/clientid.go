package middleware

import (
	"context"
	"net/http"
)

const (
	ClientIDHeader     = "Twitch-Client-Id"
	ClientIDContextKey = "ClientID"
)

func NewClientIDMiddleware() func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r != nil {
				clientID := r.Header.Get(ClientIDHeader)
				ctx := context.WithValue(r.Context(), ClientIDContextKey, clientID)
				r = r.WithContext(ctx)
			}
			h.ServeHTTP(w, r)
		})
	}
}
