package middleware

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	log "github.com/sirupsen/logrus"
)

const (
	sandstormServiceName = "phanto"

	assumeRoleDuration     = 900 * time.Second
	assumeRoleExpiryWindow = 10 * time.Second

	cartmanAlgorithm      = "ES256"
	cartmanVisageAudience = "code.justin.tv/web/cartman"
	cartmanVisageIssuer   = "code.justin.tv/web/cartman"
)

func NewAuthorizationMiddleware(config *config.Config) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		awsConfig := &aws.Config{Region: aws.String(config.AWSRegion)}
		awsSession, err := session.NewSession(awsConfig)
		if err != nil {
			log.WithError(err).Fatal("error initializing aws session")
			panic(err)
		}

		stsClient := sts.New(awsSession)
		arp := &stscreds.AssumeRoleProvider{
			Duration:     assumeRoleDuration,
			ExpiryWindow: assumeRoleExpiryWindow,
			RoleARN:      config.SandstormRoleARN,
			Client:       stsClient,
		}
		creds := credentials.NewCredentials(arp)
		awsConfig.WithCredentials(creds)

		sandstormManager := manager.New(manager.Config{
			AWSConfig:   awsConfig,
			ServiceName: sandstormServiceName,
		})

		cartmanPublicKey, err := sandstormManager.Get(config.SandstormECCPublicKey)
		if err != nil {
			log.WithError(err).Error("Error fetching cartman public key. Bailing")
			panic(err)
		}

		cartmanDecoder, err := goauthorization.NewDecoder(cartmanAlgorithm, cartmanVisageAudience, cartmanVisageIssuer, cartmanPublicKey.Plaintext)
		if err != nil {
			log.WithError(err).Fatal("error initializing cartmanDecoder")
		}

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token, err := cartmanDecoder.ParseToken(r)
			if err != nil {
				h.ServeHTTP(w, r) // no auth if token failed to parse
				return
			}

			err = cartmanDecoder.Validate(token, nil)
			if err != nil {
				h.ServeHTTP(w, r) // no auth if token failed to validate
				return
			}

			userID := token.GetSubject()
			ctx := context.WithValue(r.Context(), authorization.AuthenticatedUserIDContextKey, userID)
			r = r.WithContext(ctx)

			h.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
