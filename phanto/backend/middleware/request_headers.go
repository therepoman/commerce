package middleware

import (
	"context"
	"net/http"
)

const (
	RequestHeadersContextKey = "RequestHeaders"
)

type RequestHeaders struct {
	Method     string      `json:"method"`
	Url        string      `json:"url"`
	RemoteAddr string      `json:"remote_addr"`
	Headers    http.Header `json:"headers"`
}

func NewRequestHeadersMiddleware() func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r != nil {
				ctx := context.WithValue(r.Context(), RequestHeadersContextKey, RequestHeaders{
					Method:     r.Method,
					Url:        r.URL.String(),
					RemoteAddr: r.RemoteAddr,
					Headers:    r.Header,
				})
				r = r.WithContext(ctx)
			}
			h.ServeHTTP(w, r)
		})
	}
}
