package middleware

import (
	"context"
	"net/http"
	"strings"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/config"
)

const (
	IntegrationTestTUIDHeader           = "Integration-Test-Tuid"
	IntegrationTestThrottleConfigHeader = "Integration-Test-Throttle-Config"
)

func NewIntegrationTestMiddleware(config *config.Config) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r != nil {
				if !config.IsProd() {
					integrationTestTUID := r.Header.Get(IntegrationTestTUIDHeader)
					throttleConfig := r.Header.Get(IntegrationTestThrottleConfigHeader)

					if strings.EqualFold(throttleConfig, throttle.ConfigDisabled) {
						r = r.WithContext(context.WithValue(r.Context(), throttle.ConfigContextKey, throttle.ConfigDisabled))
					}
					if string_utils.NotBlank(integrationTestTUID) {
						r = r.WithContext(context.WithValue(r.Context(), authorization.AuthenticatedUserIDContextKey, integrationTestTUID))
					}
				}
			}
			h.ServeHTTP(w, r)
		})
	}
}
