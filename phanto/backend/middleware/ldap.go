package middleware

import (
	"context"
	"net/http"
)

const (
	LDAPHeader         = "Twitch-LDAP-Login"
	LDAPUserContextKey = "LDAPUser"
)

func NewLDAPMiddleware() func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r != nil {
				ldapLogin := r.Header.Get(LDAPHeader)
				ctx := context.WithValue(r.Context(), LDAPUserContextKey, ldapLogin)
				r = r.WithContext(ctx)
			}
			h.ServeHTTP(w, r)
		})
	}
}
