package key

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	claim_data "code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/geoip/locator"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/backend/product/fulfillment"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	UUIDNamespace             = "d4811844-0b28-4183-9358-721855445ccb"
	snsFulfillmentErrorMetric = "sns-fulfillment-errors"
)

type Claimer interface {
	Claim(ctx context.Context, userID string, clientIP string, claimData claim_data.Validation, redeemType base.RedeemType) error
}

type claimer struct {
	DAO          key.DAO         `inject:""`
	SNSClient    sns.Client      `inject:""`
	GeoIPLocator locator.Locator `inject:""`
	Stats        statsd.Statter  `inject:""`
}

func NewClaimer() Claimer {
	return &claimer{}
}

func (c *claimer) Claim(ctx context.Context, userID string, clientIP string, claimData claim_data.Validation, redeemType base.RedeemType) error {
	if strings.Blank(string(claimData.KeyCode)) {
		return errors.New("cannot update claim status with empty key code")
	}
	if strings.Blank(claimData.SKU) {
		return errors.New("cannot update claim status with empty product SKU")
	}
	if strings.Blank(claimData.SNSTopic) {
		return errors.New("cannot update claim status with empty SNS topic")
	}
	if strings.Blank(clientIP) {
		return errors.New("cannot claim without a clientIP")
	}
	if !base.IsValidRedeemType(redeemType) {
		return errors.New("invalid redeem type")
	}

	data, err := c.GeoIPLocator.Locate(clientIP)
	if err != nil {
		return err
	}

	err = c.DAO.UpdateIfExists(&key.Key{
		KeyCode:        claimData.KeyCode,
		KeyStatus:      status.KeyStatusClaimed,
		ClaimedBy:      userID,
		ClaimerIP:      string(data.IP),
		ClaimerCountry: string(data.Country),
		ClaimerState:   string(data.State),
		ClaimerZip:     string(data.Zip),
		ClaimTime:      time.Now(),
		RedeemType:     string(redeemType),
	})

	if err != nil {
		return errors.Wrap(err, "downstream dependency error while claiming key")
	}

	claimID, err := ToClaimID(claimData.KeyCode)
	if err != nil {
		return err
	}

	notificationBody := &fulfillment.SNS{
		UserID:  userID,
		SKU:     claimData.SKU,
		ClaimID: claimID,
	}

	notificationBytes, err := json.Marshal(notificationBody)
	if err != nil {
		return errors.Wrap(err, "could not marshal notification SNS")
	}

	go func() {
		err := c.Stats.Inc(fmt.Sprintf("claim-code-%s", claimData.ProductType), 1, 1.0)
		if err != nil {
			log.WithField("productType", claimData.ProductType).WithError(err).Error("error sending product-specific claim metric")
		}
	}()

	go func() {
		err := c.SNSClient.Publish(context.Background(), claimData.SNSTopic, string(notificationBytes))
		if err != nil {
			log.WithFields(log.Fields{
				"HashedKeyCode": string(claimData.KeyCode),
				"UserID":        userID,
				"RedeemType":    string(redeemType),
				"ProductType":   claimData.ProductType,
				"SKU":           claimData.SKU,
			}).WithError(err).Error("Key was marked claimed, but failed to send SNS fulfillment message")

			statsErr := c.Stats.Inc(snsFulfillmentErrorMetric, 1, 1.0)
			if statsErr != nil {
				log.WithError(statsErr).Error("error sending sns fulfillment error metric")
			}
		} else {
			statsErr := c.Stats.Inc(snsFulfillmentErrorMetric, 0, 1.0)
			if statsErr != nil {
				log.WithError(statsErr).Error("error sending sns fulfillment success metric")
			}
		}
	}()

	return nil
}

func ToClaimID(key code.RawHashed) (string, error) {
	uuidNamespace, err := uuid.FromString(UUIDNamespace)
	if err != nil {
		return "", errors.Wrap(err, "downstream dependency error while claiming key")
	}

	claimUUID := uuid.NewV5(uuidNamespace, string(key))

	return claimUUID.String(), nil
}
