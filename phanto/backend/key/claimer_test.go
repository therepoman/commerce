package key

import (
	"context"
	"encoding/json"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/geoip/locator"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/backend/product/fulfillment"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/geoip/locator"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/github.com/cactus/go-statsd-client/statsd"
	"github.com/brildum/testify/mock"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestClaimer_Claim(t *testing.T) {
	Convey("Given a Key Claimer", t, func() {
		dao := new(key_mock.DAO)
		snsClient := new(sns_mock.Client)
		loc := new(locator_mock.Locator)
		stats := new(statsd_mock.Statter)

		c := &claimer{
			DAO:          dao,
			SNSClient:    snsClient,
			GeoIPLocator: loc,
			Stats:        stats,
		}

		userID := "123123123"
		sku := "asdfasdfasdfasdf"
		keyCode := "123123123123123"
		snsTopic := "walrusTopic"
		clientIP := "127.0.0.1"
		redeemType := base.RedeemType_StandardByKey

		keyCodeHashed, err := code.Raw(keyCode).Hash()
		So(err, ShouldBeNil)

		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Given a blank key code", func() {
			claimData := data.Validation{
				KeyCode: "",
			}

			Convey("When we try to claim, we should return an error", func() {
				err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given a blank product SKU", func() {
			claimData := data.Validation{
				KeyCode: keyCodeHashed,
				SKU:     "",
			}

			Convey("When we try to claim, we should return an error", func() {
				err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given a blank SNS topic", func() {
			claimData := data.Validation{
				KeyCode:  keyCodeHashed,
				SKU:      sku,
				SNSTopic: "",
			}

			Convey("When we try to claim, we should return an error", func() {
				err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given a blank client IP ", func() {
			claimData := data.Validation{
				KeyCode:  keyCodeHashed,
				SKU:      sku,
				SNSTopic: snsTopic,
			}

			clientIP = ""

			Convey("When we try to claim, we should return an error", func() {
				err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an invalid redeem_type", func() {
			claimData := data.Validation{
				KeyCode:  keyCodeHashed,
				SKU:      sku,
				SNSTopic: snsTopic,
			}

			redeemType = base.RedeemType("invalid_redeem_type")

			Convey("When we try to claim, we should return an error", func() {
				err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given a both non-empty key code and SNS topic", func() {
			claimData := data.Validation{
				KeyCode:  keyCodeHashed,
				SKU:      sku,
				SNSTopic: snsTopic,
			}

			Convey("When the locator errors", func() {
				loc.On("Locate", mock.Anything).Return(locator.Data{}, errors.New("test error"))

				Convey("When we try to claim, we should return an internal server error", func() {
					err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When the locator succeeds", func() {
				loc.On("Locate", mock.Anything).Return(locator.Data{
					IP:      locator.IP("192.168.1.1"),
					Country: locator.Country("US"),
					State:   locator.State("WA"),
				}, nil)

				Convey("When the dynamo update fails", func() {
					dao.On("UpdateIfExists", mock.Anything).Return(errors.New("THE WALRUS STRIKES"))

					Convey("When we try to claim, we should return an internal server error", func() {
						err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("When the dynamo update succeeds", func() {
					uuidNamespace, err := uuid.FromString(UUIDNamespace)
					if err != nil {
						t.Fail()
					}

					claimUUID := uuid.NewV5(uuidNamespace, string(claimData.KeyCode))

					snsNotification := &fulfillment.SNS{
						UserID:  userID,
						SKU:     sku,
						ClaimID: claimUUID.String(),
					}

					snsBody, _ := json.Marshal(snsNotification)

					dao.On("UpdateIfExists", mock.Anything).Return(nil)
					Convey("When the sns publish fails", func() {
						ctx := context.Background()

						snsClient.On("Publish", mock.Anything, snsTopic, string(snsBody)).Return(errors.New("THE WALRUS STRIKES"))

						Convey("When we try to claim, we should return nil", func() {
							err := c.Claim(ctx, userID, clientIP, claimData, redeemType)

							So(err, ShouldBeNil)
							time.Sleep(time.Millisecond * 100) // await goroutine
							snsClient.AssertCalled(t, "Publish", mock.Anything, mock.Anything, mock.Anything)
						})
					})

					Convey("When the sns publish succeeds", func() {
						snsClient.On("Publish", mock.Anything, snsTopic, string(snsBody)).Return(nil)

						Convey("When we try to claim, we should return nil for success", func() {
							err := c.Claim(context.Background(), userID, clientIP, claimData, redeemType)
							So(err, ShouldBeNil)

							time.Sleep(time.Millisecond * 100) // await goroutine
							snsClient.AssertCalled(t, "Publish", mock.Anything, mock.Anything, mock.Anything)

							Convey("Make sure dynamo updated the right data", func() {
								updatedKey, ok := dao.Calls[0].Arguments.Get(0).(*key.Key)
								So(ok, ShouldBeTrue)
								So(updatedKey.KeyCode, ShouldEqual, keyCodeHashed)
								So(updatedKey.KeyStatus, ShouldEqual, status.KeyStatusClaimed)
								So(updatedKey.ClaimedBy, ShouldEqual, userID)
								So(updatedKey.ClaimerIP, ShouldEqual, "192.168.1.1")
								So(updatedKey.ClaimerCountry, ShouldEqual, "US")
								So(updatedKey.ClaimerState, ShouldEqual, "WA")
							})
						})
					})
				})
			})
		})
	})
}
