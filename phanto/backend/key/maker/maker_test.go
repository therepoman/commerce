package maker_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/key/maker"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestKeyMakerImpl_MakeKey(t *testing.T) {
	Convey("Given a KeyMaker", t, func() {
		keyDao := new(key_mock.DAO)
		keyMaker := maker.KeyMakerImpl{
			KeyDAO: keyDao,
		}
		batchID := "test-batch-id"

		Convey("Errors when DAO errors", func() {
			keyDao.On("CreateNew", mock.Anything).Return(errors.New("test error"))
			_, _, err := keyMaker.MakeKey(batchID)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when DAO succeeds", func() {
			keyDao.On("CreateNew", mock.Anything).Return(nil)
			raw, hashed, err := keyMaker.MakeKey(batchID)
			So(err, ShouldBeNil)
			So(string(raw), ShouldNotBeBlank)
			So(string(hashed), ShouldNotBeBlank)
		})
	})
}
