package maker

import (
	"code.justin.tv/commerce/gogogadget/crypto/random"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"github.com/pkg/errors"
)

const (
	keyCodeSize    = 15
	keyAlphabetSet = "ABCDEFGHIJKMNPQRSTUVWXYZ23456789" // no O/0, no 1/L

)

type KeyMakerImpl struct {
	KeyDAO key.DAO `inject:""`
}

type KeyMaker interface {
	MakeKey(batchID string) (code.Raw, code.RawHashed, error)
}

func (km *KeyMakerImpl) MakeKey(batchID string) (code.Raw, code.RawHashed, error) {
	keyCode, err := randomKeyCode()
	if err != nil {
		return "", "", errors.Wrap(err, "error generating code")
	}
	rawKeyCode := code.Raw(keyCode)

	hashedKeyCode, err := rawKeyCode.Hash()
	if err != nil {
		return "", "", errors.Wrap(err, "error hashing code")
	}

	k := &key.Key{
		KeyCode:   hashedKeyCode,
		BatchID:   batchID,
		KeyStatus: status.KeyStatusUnclaimed,
	}

	err = km.KeyDAO.CreateNew(k)
	if err != nil {
		return "", "", errors.Wrap(err, "error creating key in dynamo")
	}

	return rawKeyCode, hashedKeyCode, nil
}

func randomKeyCode() (string, error) {
	return random.RandomString(keyAlphabetSet, keyCodeSize)
}
