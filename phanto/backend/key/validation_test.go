package key

import (
	"context"
	"errors"
	"testing"
	"time"

	eligibility_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/eligibility"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	key_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key"
	key_batch_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a key validator", t, func() {
		ctx := context.Background()
		dao := new(key_mock.DAO)
		batchValidator := new(key_batch_mock.Validator)

		v := &validator{
			DAO:            dao,
			BatchValidator: batchValidator,
		}

		keyCode := code.Formatted("1231231231123")
		keyHash, err := keyCode.Raw().Hash()
		So(err, ShouldBeNil)
		keyBatchID := "123123"

		Convey("When we error fetching the key from Dynamo", func() {
			dao.On("GetKey", keyHash).Return(nil, errors.New("THE WALRUS STRIKES"))

			Convey("We should return an internal server error", func() {
				claimData, err := v.Validate(ctx, keyHash)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we do not have a record for the key in Dynamo", func() {
			dao.On("GetKey", keyHash).Return(nil, nil)

			Convey("We should return a not found error", func() {
				claimData, err := v.Validate(ctx, keyHash)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we find the key record in Dynamo that cannot be claimed", func() {
			keyRecord := &key.Key{
				KeyCode:   keyHash,
				BatchID:   keyBatchID,
				KeyStatus: status.KeyStatusClaimed,
			}

			dao.On("GetKey", keyHash).Return(keyRecord, nil)

			snsTopic := "walrusTopic"
			productType := "asdfasdf"
			sku := "123123123123123"
			keyPoolID := "123123123"
			productDescription := "Spend a day with a walrus"

			keyBatchClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           snsTopic,
				ProductType:        productType,
				ProductDescription: productDescription,
				KeyPoolID:          keyPoolID,
				KeyBatchID:         keyBatchID,
				SKU:                sku,
			}

			Convey("when we error validating the key batch associated with the key code", func() {
				keyBatchClaimData.IsValid = false

				batchValidator.On("Validate", ctx, keyBatchID).Return(keyBatchClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, keyHash)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the key batch associated with it is invalid", func() {
				keyBatchClaimData.IsValid = false

				batchValidator.On("Validate", ctx, keyBatchID).Return(keyBatchClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, keyHash)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the key batch associated is valid", func() {
				batchValidator.On("Validate", ctx, keyBatchID).Return(keyBatchClaimData, nil)

				Convey("We should return that the key is not valid", func() {
					claimData, err := v.Validate(ctx, keyHash)

					So(claimData.IsValid, ShouldBeFalse)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, productDescription)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(claimData.KeyPoolID, ShouldEqual, keyPoolID)
					So(claimData.KeyBatchID, ShouldEqual, keyBatchID)
					So(claimData.KeyCode, ShouldEqual, keyHash)
					So(claimData.KeyStatus, ShouldEqual, status.KeyStatusClaimed)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("When we find the key record in Dynamo that can be claimed", func() {
			keyRecord := &key.Key{
				KeyCode:   keyHash,
				BatchID:   keyBatchID,
				KeyStatus: status.KeyStatusUnclaimed,
			}

			dao.On("GetKey", keyHash).Return(keyRecord, nil)

			snsTopic := "walrusTopic"
			productType := "asdfasdf"
			sku := "123123123123123"
			keyPoolID := "123123123"
			productDescription := "Spend a day with a walrus"

			keyBatchClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           snsTopic,
				ProductType:        productType,
				ProductDescription: productDescription,
				KeyPoolID:          keyPoolID,
				KeyBatchID:         keyBatchID,
				SKU:                sku,
			}

			Convey("when we error validating the key batch associated with the key code", func() {
				keyBatchClaimData.IsValid = false

				batchValidator.On("Validate", ctx, keyBatchID).Return(keyBatchClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, keyHash)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the key batch associated with it is invalid", func() {
				keyBatchClaimData.IsValid = false

				batchValidator.On("Validate", ctx, keyBatchID).Return(keyBatchClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, keyHash)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the key batch associated is valid", func() {
				batchValidator.On("Validate", ctx, keyBatchID).Return(keyBatchClaimData, nil)

				Convey("We should return that the key is valid", func() {
					claimData, err := v.Validate(ctx, keyHash)

					So(claimData.IsValid, ShouldBeTrue)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, productDescription)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(claimData.KeyPoolID, ShouldEqual, keyPoolID)
					So(claimData.KeyBatchID, ShouldEqual, keyBatchID)
					So(claimData.KeyCode, ShouldEqual, keyHash)
					So(claimData.KeyStatus, ShouldEqual, status.KeyStatusUnclaimed)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestValidator_ValidateForUser(t *testing.T) {
	Convey("GIVEN a key validator", t, func() {

		mockDao := new(key_mock.DAO)
		mockValidator := new(key_batch_mock.Validator)
		mockEligibilityChecker := new(eligibility_mock.EligibilityChecker)

		v := &validator{
			DAO:            mockDao,
			BatchValidator: mockValidator,
			Eligibility:    mockEligibilityChecker,
		}

		keyCode := code.Formatted("1231231231123")
		keyHash, err := keyCode.Raw().Hash()
		So(err, ShouldBeNil)
		keyBatchID := "123123"
		productType := "testProductType"

		userID := "43231"

		ctx := context.Background()

		Convey("WHEN unable to get key", func() {
			mockDao.On("GetKey", keyHash).Return(nil, errors.New("mock error"))
			Convey("THEN return an error", func() {
				claimData, err := v.ValidateForUser(ctx, keyHash, userID)
				So(err, ShouldBeError)
				So(claimData.IsValid, ShouldBeFalse)
				mockEligibilityChecker.AssertNotCalled(t, "IsUserEligibleToRedeemKey", mock.Anything, mock.Anything, mock.Anything)
			})
		})

		Convey("WHEN dynamo key is nil", func() {
			mockDao.On("GetKey", keyHash).Return(nil, nil)
			Convey("THEN return an error", func() {
				claimData, err := v.ValidateForUser(ctx, keyHash, userID)
				So(err, ShouldBeError)
				So(claimData.IsValid, ShouldBeFalse)
				mockEligibilityChecker.AssertNotCalled(t, "IsUserEligibleToRedeemKey", mock.Anything, mock.Anything, mock.Anything)
			})
		})

		Convey("GIVEN the key is found successfully", func() {
			mockDao.On("GetKey", keyHash).Return(&key.Key{
				KeyCode:   keyHash,
				BatchID:   keyBatchID,
				KeyStatus: status.KeyStatusUnclaimed,
			}, nil)

			Convey("WHEN key batch validator returns an error", func() {
				mockValidator.On("Validate", ctx, keyBatchID).Return(data.Validation{}, errors.New("mock error"))
				Convey("THEN return an error", func() {
					claimData, err := v.ValidateForUser(ctx, keyHash, userID)
					So(err, ShouldBeError)
					So(claimData.IsValid, ShouldBeFalse)
					mockEligibilityChecker.AssertNotCalled(t, "IsUserEligibleToRedeemKey", mock.Anything, mock.Anything, mock.Anything)
				})
			})

			Convey("WHEN key batch validator returns isValid:false", func() {
				mockValidator.On("Validate", ctx, keyBatchID).Return(data.Validation{
					IsValid: false,
				}, nil)
				Convey("THEN return without calling IsUserEligibleToRedeemKey", func() {
					claimData, err := v.ValidateForUser(ctx, keyHash, userID)
					So(err, ShouldBeNil)
					So(claimData.IsValid, ShouldBeFalse)
					So(claimData.KeyStatus, ShouldEqual, status.KeyStatusInvalidated)
				})
			})

			Convey("GIVEN key batch validator has no error", func() {
				mockValidator.On("Validate", ctx, keyBatchID).Return(data.Validation{
					IsValid:     true,
					KeyBatchID:  keyBatchID,
					StartDate:   time.Now().Add(-1 * time.Hour),
					EndDate:     time.Now().Add(1 * time.Hour),
					ProductType: productType,
				}, nil)

				Convey("WHEN IsUserEligibleToRedeemKey returns error", func() {
					mockEligibilityChecker.On("IsUserEligibleToRedeemKey", mock.Anything, mock.Anything, mock.Anything).Return(false, errors.New("mock error"))
					Convey("THEN return error", func() {
						claimData, err := v.ValidateForUser(ctx, keyHash, userID)
						So(err, ShouldBeError)
						So(claimData.IsValid, ShouldBeFalse)
						So(claimData.KeyStatus, ShouldEqual, status.KeyStatusIneligible)
					})
				})

				Convey("WHEN IsUserEligibleToRedeemKey returns false", func() {
					mockEligibilityChecker.On("IsUserEligibleToRedeemKey", mock.Anything, mock.Anything, mock.Anything).Return(false, nil)
					Convey("THEN return error", func() {
						claimData, err := v.ValidateForUser(ctx, keyHash, userID)
						So(err, ShouldBeNil)
						So(claimData.IsValid, ShouldBeFalse)
						So(claimData.KeyStatus, ShouldEqual, status.KeyStatusIneligible)
					})
				})

				Convey("WHEN IsUserEligibleToRedeemKey returns true", func() {
					mockEligibilityChecker.On("IsUserEligibleToRedeemKey", mock.Anything, mock.Anything, mock.Anything).Return(true, nil)
					Convey("THEN return error", func() {
						claimData, err := v.ValidateForUser(ctx, keyHash, userID)
						So(err, ShouldBeNil)
						So(claimData.IsValid, ShouldBeTrue)
						So(claimData.KeyStatus, ShouldEqual, status.KeyStatusUnclaimed)
					})
				})
			})
		})
	})
}
