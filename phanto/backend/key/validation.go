package key

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/data"
	dynamo_key "code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/eligibility"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/backend/key_batch"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type Validator interface {
	Validate(ctx context.Context, keyCode code.RawHashed) (claimData data.Validation, err error)
	ValidateForUser(ctx context.Context, keyCode code.RawHashed, userID string) (claimData data.Validation, err error)
}

type validator struct {
	BatchValidator key_batch.Validator            `inject:"keyBatchValidator"`
	DAO            dynamo_key.DAO                 `inject:""`
	Eligibility    eligibility.EligibilityChecker `inject:""`
}

func NewValidator() Validator {
	return &validator{}
}

var NotFoundError = errors.New("no key is associated with requested key code")

func (v *validator) Validate(ctx context.Context, keyCode code.RawHashed) (claimData data.Validation, err error) {
	claimData.IsValid = false

	dynamoKey, err := v.DAO.GetKey(keyCode)
	if err != nil {
		return claimData, errors.Wrap(err, "downstream dependency error getting Key status")
	}
	if dynamoKey == nil {
		return claimData, NotFoundError
	}

	keyBatchClaimData, err := v.BatchValidator.Validate(ctx, dynamoKey.BatchID)
	if err != nil {
		return claimData, err
	}

	claimData = keyBatchClaimData
	claimData.KeyCode = dynamoKey.KeyCode
	claimData.KeyStatus = dynamoKey.KeyStatus
	claimData.ClaimerUserID = dynamoKey.ClaimedBy
	claimData.ClaimerCountry = dynamoKey.ClaimerCountry
	claimData.ClaimerState = dynamoKey.ClaimerState
	claimData.ClaimerZip = dynamoKey.ClaimerZip
	claimData.ClaimTime = dynamoKey.ClaimTime

	if !claimData.IsValid && claimData.KeyStatus == status.KeyStatusUnclaimed {
		claimData.KeyStatus = status.KeyStatusInvalidated
	}

	if !dynamoKey.KeyStatus.CanBeClaimed() {
		claimData.IsValid = false
	}

	return
}

func (v *validator) ValidateForUser(ctx context.Context, keyCode code.RawHashed, userID string) (claimData data.Validation, err error) {
	claimData, err = v.Validate(ctx, keyCode)
	// No need to check for eligibility if we already had an error or determined the claim was not valid.
	if !claimData.IsValid || err != nil {
		return
	}

	// Check for eligibility
	isEligible, err := v.Eligibility.IsUserEligibleToRedeemKey(ctx, userID, claimData)
	if err != nil {
		log.WithError(err).Error("Error checking eligibility")
	}
	if !isEligible || err != nil {
		claimData.KeyStatus = status.KeyStatusIneligible
		claimData.IsValid = false
	}

	return
}
