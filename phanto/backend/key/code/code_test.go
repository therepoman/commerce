package code

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRawCode_Format(t *testing.T) {
	Convey("Test formatting various raw codes", t, func() {
		Convey("When code is empty", func() {
			So(Raw("").Format(), ShouldEqual, "")
		})
		Convey("When code is 1 char", func() {
			So(Raw("A").Format(), ShouldEqual, "A")
		})
		Convey("When code is 4 chars", func() {
			So(Raw("ABCD").Format(), ShouldEqual, "ABCD")
		})
		Convey("When code is 5 chars", func() {
			So(Raw("ABCDE").Format(), ShouldEqual, "ABCDE")
		})
		Convey("When code is 6 chars", func() {
			So(Raw("ABCDEF").Format(), ShouldEqual, "ABCDE-F")
		})
		Convey("When code is 10 chars", func() {
			So(Raw("ABCDEFGHIJ").Format(), ShouldEqual, "ABCDE-FGHIJ")
		})
		Convey("When code is 11 chars", func() {
			So(Raw("ABCDEFGHIJK").Format(), ShouldEqual, "ABCDE-FGHIJ-K")
		})
		Convey("When code is 15 chars", func() {
			So(Raw("ABCDEFGHIJKLMNO").Format(), ShouldEqual, "ABCDE-FGHIJ-KLMNO")
		})
	})
}

func TestFormattedCode_Raw(t *testing.T) {
	Convey("Test converting to raw various formatted codes", t, func() {
		Convey("When code is empty", func() {
			So(Formatted("").Raw(), ShouldEqual, "")
		})
		Convey("When code is 1 char", func() {
			So(Formatted("A").Raw(), ShouldEqual, "A")
		})
		Convey("When code is 5 chars", func() {
			So(Formatted("ABCDE").Raw(), ShouldEqual, "ABCDE")
		})
		Convey("When code is 6 chars with one dash", func() {
			So(Formatted("ABCDE-F").Raw(), ShouldEqual, "ABCDEF")
		})
		Convey("When code is 10 chars with one dash", func() {
			So(Formatted("ABCDE-FGHIJ").Raw(), ShouldEqual, "ABCDEFGHIJ")
		})
		Convey("When code is fully formatted with 15 chars", func() {
			So(Formatted("ABCDE-FGHIJ-KLMNO").Raw(), ShouldEqual, "ABCDEFGHIJKLMNO")
		})
		Convey("When code is 15 chars raw", func() {
			So(Formatted("ABCDEFGHIJKLMNO").Raw(), ShouldEqual, "ABCDEFGHIJKLMNO")
		})
		Convey("When code starts and ends with white space", func() {
			So(Formatted(" \t \n ABCDE-FGHIJ-KLMNO \t \n ").Raw(), ShouldEqual, "ABCDEFGHIJKLMNO")
		})
		Convey("When code contains lowercase characters", func() {
			So(Formatted("ABCDe-12345-KlMnO").Raw(), ShouldEqual, "ABCDE12345KLMNO")
		})
	})
}

func TestFormattedCode_IsValid(t *testing.T) {
	Convey("Test validation on various formatted codes", t, func() {
		Convey("When code is empty", func() {
			So(Formatted("").IsValid(), ShouldBeFalse)
		})
		Convey("When code is 1 char", func() {
			So(Formatted("A").IsValid(), ShouldBeFalse)
		})
		Convey("When code is 5 chars", func() {
			So(Formatted("ABCDE").IsValid(), ShouldBeFalse)
		})
		Convey("When code is 6 chars with one dash", func() {
			So(Formatted("ABCDE-F").IsValid(), ShouldBeFalse)
		})
		Convey("When code is 10 chars with one dash", func() {
			So(Formatted("ABCDE-FGHIJ").IsValid(), ShouldBeFalse)
		})
		Convey("When code is fully formatted with 15 chars", func() {
			So(Formatted("ABCDE-FGHIJ-KLMNO").IsValid(), ShouldBeTrue)
		})
		Convey("When code is 15 chars raw", func() {
			So(Formatted("ABCDEFGHIJKLMNO").IsValid(), ShouldBeTrue)
		})
		Convey("When code starts and ends with white space", func() {
			So(Formatted(" \t \n ABCDE-FGHIJ-KLMNO \t \n ").IsValid(), ShouldBeTrue)
		})
		Convey("When code contains lowercase characters", func() {
			So(Formatted("ABCDe-12345-KlMnO").IsValid(), ShouldBeTrue)
		})
		Convey("When code contains an invalid separator", func() {
			So(Formatted("ABCDE~FGHIJ~KLMNO").IsValid(), ShouldBeFalse)
		})
		Convey("When code contains incorrectly sized groups", func() {
			So(Formatted("ABCDEF-GHIJ-KLMNO").IsValid(), ShouldBeFalse)
		})
		Convey("When code contains only one set of dashes", func() {
			So(Formatted("ABCDE-FGHIJKLMNO").IsValid(), ShouldBeTrue)
		})
		Convey("When code contains too many sets", func() {
			So(Formatted("ABCDE-FGHIJ-KLMNO-PQRST").IsValid(), ShouldBeFalse)
		})
	})
}
