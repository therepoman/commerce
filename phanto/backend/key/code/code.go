package code

import (
	"crypto/sha1"
	"fmt"
	"io"
	"regexp"
	"strings"
)

var formattedCodeRegex *regexp.Regexp

func init() {
	formattedCodeRegex = regexp.MustCompile(`^\s*[a-zA-Z0-9]{5}-?[a-zA-Z0-9]{5}-?[a-zA-Z0-9]{5}\s*$`)
}

const separator = "-"

type RawHashed string

type Raw string

type Formatted string

func (raw Raw) Format() Formatted {
	formattedKeyCode := ""
	for i := 0; i < len(raw); i++ {
		if i > 0 && i%5 == 0 {
			formattedKeyCode += separator
		}
		formattedKeyCode += string(raw[i])
	}
	return Formatted(formattedKeyCode)
}

func (f Formatted) IsValid() bool {
	return formattedCodeRegex.MatchString(string(f))
}

func (f Formatted) Raw() Raw {
	return Raw(sanitizeKey(string(f)))
}

func (raw Raw) Hash() (RawHashed, error) {
	hash := sha1.New()
	_, err := io.WriteString(hash, string(raw))
	if err != nil {
		return "", err
	}
	return RawHashed(fmt.Sprintf("%x", hash.Sum(nil))), nil
}

func sanitizeKey(key string) string {
	key = strings.TrimSpace(key)
	key = strings.Replace(key, separator, "", -1)
	key = strings.ToUpper(key)
	return key
}
