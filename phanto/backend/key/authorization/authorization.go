package authorization

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/authorization"
)

type Authorizer interface {
	AuthenticatedUserMatches(ctx context.Context, userID string) bool
	RequestIsAuthenticated(ctx context.Context) bool
	GetAuthenticatedUser(ctx context.Context) string
}

type authorizerImp struct {
}

func NewAuthorizer() Authorizer {
	return &authorizerImp{}
}

func (a *authorizerImp) AuthenticatedUserMatches(ctx context.Context, userID string) bool {
	authedUserID := a.GetAuthenticatedUser(ctx)
	if strings.Blank(authedUserID) {
		// no authed user, so is not authorized
		return false
	}
	return authedUserID == userID
}

func (a *authorizerImp) RequestIsAuthenticated(ctx context.Context) bool {
	authedUserID := a.GetAuthenticatedUser(ctx)
	return !strings.Blank(authedUserID)
}

func (a *authorizerImp) GetAuthenticatedUser(ctx context.Context) string {
	authedUserID, ok := ctx.Value(authorization.AuthenticatedUserIDContextKey).(string)
	if !ok {
		return ""
	}
	return authedUserID
}
