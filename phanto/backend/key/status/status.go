package status

const (
	KeyStatusClaimed     = KeyStatus("claimed")
	KeyStatusUnclaimed   = KeyStatus("unclaimed")
	KeyStatusInvalidated = KeyStatus("invalidated")
	KeyStatusIneligible  = KeyStatus("ineligible")
)

type KeyStatus string

func (s KeyStatus) String() string {
	return string(s)
}

func (s KeyStatus) CanBeClaimed() bool {
	return s != KeyStatusClaimed && s != KeyStatusInvalidated && s != KeyStatusIneligible
}
