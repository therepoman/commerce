package server

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/api/admin_get_key_status"
	"code.justin.tv/commerce/phanto/backend/api/create_key_pool"
	"code.justin.tv/commerce/phanto/backend/api/create_key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/api/create_product"
	"code.justin.tv/commerce/phanto/backend/api/create_product_type"
	"code.justin.tv/commerce/phanto/backend/api/delete_key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/api/generate_key_batch"
	"code.justin.tv/commerce/phanto/backend/api/generate_key_pool_report"
	"code.justin.tv/commerce/phanto/backend/api/get_all_key_pools"
	"code.justin.tv/commerce/phanto/backend/api/get_all_product_types"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batch_download_info"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batch_status"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batches"
	"code.justin.tv/commerce/phanto/backend/api/get_key_batches_s2s"
	"code.justin.tv/commerce/phanto/backend/api/get_key_handler_group"
	"code.justin.tv/commerce/phanto/backend/api/get_key_handler_groups"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_report_download_info"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_reports"
	"code.justin.tv/commerce/phanto/backend/api/get_key_pools_by_handler"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status"
	"code.justin.tv/commerce/phanto/backend/api/get_products"
	"code.justin.tv/commerce/phanto/backend/api/helix_get_key_status"
	"code.justin.tv/commerce/phanto/backend/api/helix_redeem_key"
	"code.justin.tv/commerce/phanto/backend/api/helix_redeem_key_by_batch"
	"code.justin.tv/commerce/phanto/backend/api/invalidate_key_batch"
	"code.justin.tv/commerce/phanto/backend/api/invalidate_key_pool"
	"code.justin.tv/commerce/phanto/backend/api/invalidate_keys"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_for_load_testing"
	"code.justin.tv/commerce/phanto/backend/api/update_key_batch"
	"code.justin.tv/commerce/phanto/backend/api/update_key_handler_group"
	"code.justin.tv/commerce/phanto/backend/api/update_key_pool"
	"code.justin.tv/commerce/phanto/rpc"
)

type Server struct {
	CreateKeyPoolAPI                *create_key_pool.API                   `inject:""`
	CreateProductAPI                *create_product.API                    `inject:""`
	CreateProductTypeAPI            *create_product_type.API               `inject:""`
	GenerateKeyBatchAPI             *generate_key_batch.API                `inject:""`
	GetKeyBatchStatusAPI            *get_key_batch_status.API              `inject:""`
	RedeemKeyAPI                    *redeem_key.API                        `inject:""`
	RedeemKeyForLoadTestingAPI      *redeem_key_for_load_testing.API       `inject:""`
	GetKeyStatusAPI                 *get_key_status.API                    `inject:""`
	InvalidateKeysAPI               *invalidate_keys.API                   `inject:""`
	InvalidateKeyBatchAPI           *invalidate_key_batch.API              `inject:""`
	InvalidateKeyPoolAPI            *invalidate_key_pool.API               `inject:""`
	GetKeyBatchDownloadInfoAPI      *get_key_batch_download_info.API       `inject:""`
	GetAllKeyPoolsAPI               *get_all_key_pools.API                 `inject:""`
	GetKeyBatchesAPI                *get_key_batches.API                   `inject:""`
	GetKeyBatchesS2SAPI             *get_key_batches_s2s.API               `inject:""`
	GetKeyPoolsByHandlerAPI         *get_key_pools_by_handler.API          `inject:""`
	GetAllProductTypesAPI           *get_all_product_types.API             `inject:""`
	GetProductsAPI                  *get_products.API                      `inject:""`
	GetKeyPoolReportsAPI            *get_key_pool_reports.API              `inject:""`
	GenerateKeyPoolReportAPI        *generate_key_pool_report.API          `inject:""`
	GetKeyPoolReportDownloadInfoAPI *get_key_pool_report_download_info.API `inject:""`
	GetKeyPoolMetadataAPI           *get_key_pool_metadata.API             `inject:""`
	CreateKeyPoolMetadataAPI        *create_key_pool_metadata.API          `inject:""`
	DeleteKeyPoolMetadataAPI        *delete_key_pool_metadata.API          `inject:""`
	UpdateKeyPoolAPI                *update_key_pool.API                   `inject:""`
	UpdateKeyBatchAPI               *update_key_batch.API                  `inject:""`
	HelixGetKeyStatusAPI            *helix_get_key_status.API              `inject:""`
	HelixRedeemKeyAPI               *helix_redeem_key.API                  `inject:""`
	GetKeyHandlerGroupAPI           *get_key_handler_group.API             `inject:""`
	GetKeyHandlerGroupsAPI          *get_key_handler_groups.API            `inject:""`
	UpdateKeyHandlerGroupAPI        *update_key_handler_group.API          `inject:""`
	HelixRedeemKeyByBatchAPI        *helix_redeem_key_by_batch.API         `inject:""`
	AdminGetKeyStatusAPI            *admin_get_key_status.API              `inject:""`
}

func (s *Server) HealthCheck(ctx context.Context, req *phanto.HealthCheckReq) (*phanto.HealthCheckResp, error) {
	return &phanto.HealthCheckResp{}, nil
}

func (s *Server) CreateProduct(ctx context.Context, req *phanto.CreateProductReq) (*phanto.CreateProductResp, error) {
	return s.CreateProductAPI.CreateProduct(ctx, req)
}

func (s *Server) CreateProductType(ctx context.Context, req *phanto.CreateProductTypeReq) (*phanto.CreateProductTypeResp, error) {
	return s.CreateProductTypeAPI.CreateProductType(ctx, req)
}

func (s *Server) CreateKeyPool(ctx context.Context, req *phanto.CreateKeyPoolReq) (*phanto.CreateKeyPoolResp, error) {
	return s.CreateKeyPoolAPI.CreateKeyPool(ctx, req)
}

func (s *Server) GenerateKeyBatch(ctx context.Context, req *phanto.GenerateKeyBatchReq) (*phanto.GenerateKeyBatchResp, error) {
	return s.GenerateKeyBatchAPI.GenerateKeyBatch(ctx, req)
}

func (s *Server) GetKeyBatchStatus(ctx context.Context, req *phanto.GetKeyBatchStatusReq) (*phanto.GetKeyBatchStatusResp, error) {
	return s.GetKeyBatchStatusAPI.GetKeyBatchStatus(ctx, req)
}

func (s *Server) GetKeyBatchDownloadInfo(ctx context.Context, req *phanto.GetKeyBatchDownloadInfoReq) (*phanto.GetKeyBatchDownloadInfoResp, error) {
	return s.GetKeyBatchDownloadInfoAPI.GetKeyBatchDownloadInfo(ctx, req)
}

func (s *Server) GetKeyStatus(ctx context.Context, req *phanto.GetKeyStatusReq) (*phanto.GetKeyStatusResp, error) {
	return s.GetKeyStatusAPI.GetKeyStatus(ctx, req)
}

func (s *Server) RedeemKey(ctx context.Context, req *phanto.RedeemKeyReq) (*phanto.RedeemKeyResp, error) {
	return s.RedeemKeyAPI.RedeemKey(ctx, req)
}

func (s *Server) RedeemKeyForLoadTesting(ctx context.Context, req *phanto.RedeemKeyReq) (*phanto.RedeemKeyResp, error) {
	return s.RedeemKeyForLoadTestingAPI.RedeemKeyForLoadTesting(ctx, req)
}

func (s *Server) InvalidateKeys(ctx context.Context, req *phanto.InvalidateKeysReq) (*phanto.InvalidateKeysResp, error) {
	return s.InvalidateKeysAPI.InvalidateKeys(ctx, req)
}

func (s *Server) InvalidateKeyBatch(ctx context.Context, req *phanto.InvalidateKeyBatchReq) (*phanto.InvalidateKeyBatchResp, error) {
	return s.InvalidateKeyBatchAPI.InvalidateKeyBatch(ctx, req)
}

func (s *Server) InvalidateKeyPool(ctx context.Context, req *phanto.InvalidateKeyPoolReq) (*phanto.InvalidateKeyPoolResp, error) {
	return s.InvalidateKeyPoolAPI.InvalidateKeyPool(ctx, req)
}

func (s *Server) GetAllKeyPools(ctx context.Context, req *phanto.GetAllKeyPoolsReq) (*phanto.GetAllKeyPoolsResp, error) {
	return s.GetAllKeyPoolsAPI.GetAllKeyPools(ctx, req)
}

func (s *Server) GetKeyBatches(ctx context.Context, req *phanto.GetKeyBatchesReq) (*phanto.GetKeyBatchesResp, error) {
	return s.GetKeyBatchesAPI.GetKeyBatches(ctx, req)
}

func (s *Server) GetKeyBatchesS2S(ctx context.Context, req *phanto.GetKeyBatchesReq) (*phanto.GetKeyBatchesResp, error) {
	return s.GetKeyBatchesS2SAPI.GetKeyBatches(ctx, req)
}

func (s *Server) GetKeyPoolsByHandler(ctx context.Context, req *phanto.GetKeyPoolsByHandlerReq) (*phanto.GetKeyPoolsByHandlerResp, error) {
	return s.GetKeyPoolsByHandlerAPI.GetKeyPoolsByHandler(ctx, req)
}

func (s *Server) GetAllProductTypes(ctx context.Context, req *phanto.GetAllProductTypesReq) (*phanto.GetAllProductTypesResp, error) {
	return s.GetAllProductTypesAPI.GetAllProductTypes(ctx, req)
}

func (s *Server) GetProducts(ctx context.Context, req *phanto.GetProductsReq) (*phanto.GetProductsResp, error) {
	return s.GetProductsAPI.GetProducts(ctx, req)
}

func (s *Server) GetKeyPoolReports(ctx context.Context, req *phanto.GetKeyPoolReportsReq) (*phanto.GetKeyPoolReportsResp, error) {
	return s.GetKeyPoolReportsAPI.GetKeyPoolReports(ctx, req)
}

func (s *Server) GenerateKeyPoolReport(ctx context.Context, req *phanto.GenerateKeyPoolReportReq) (*phanto.GenerateKeyPoolReportResp, error) {
	return s.GenerateKeyPoolReportAPI.GenerateKeyPoolReport(ctx, req)
}

func (s *Server) GetKeyPoolReportDownloadInfo(ctx context.Context, req *phanto.GetKeyPoolReportDownloadInfoReq) (*phanto.GetKeyPoolReportDownloadInfoResp, error) {
	return s.GetKeyPoolReportDownloadInfoAPI.GetKeyPoolReportDownloadInfo(ctx, req)
}

func (s *Server) GetKeyPoolMetadata(ctx context.Context, req *phanto.GetKeyPoolMetadataReq) (*phanto.GetKeyPoolMetadataResp, error) {
	return s.GetKeyPoolMetadataAPI.GetKeyPoolMetadata(ctx, req)
}

func (s *Server) CreateKeyPoolMetadata(ctx context.Context, req *phanto.CreateKeyPoolMetadataReq) (*phanto.CreateKeyPoolMetadataResp, error) {
	return s.CreateKeyPoolMetadataAPI.CreateKeyPoolMetadata(ctx, req)
}

func (s *Server) DeleteKeyPoolMetadata(ctx context.Context, req *phanto.DeleteKeyPoolMetadataReq) (*phanto.DeleteKeyPoolMetadataResp, error) {
	return s.DeleteKeyPoolMetadataAPI.DeleteKeyPoolMetadata(ctx, req)
}

func (s *Server) UpdateKeyPool(ctx context.Context, req *phanto.UpdateKeyPoolReq) (*phanto.UpdateKeyPoolResp, error) {
	return s.UpdateKeyPoolAPI.UpdatePool(ctx, req)
}

func (s *Server) UpdateKeyBatch(ctx context.Context, req *phanto.UpdateKeyBatchReq) (*phanto.UpdateKeyBatchResp, error) {
	return s.UpdateKeyBatchAPI.UpdateKeyBatch(ctx, req)
}

func (s *Server) HelixGetKeyStatus(ctx context.Context, req *phanto.HelixGetKeyStatusReq) (resp *phanto.HelixGetKeyStatusResp, err error) {
	return s.HelixGetKeyStatusAPI.HelixGetKeyStatus(ctx, req)
}

func (s *Server) HelixRedeemKey(ctx context.Context, req *phanto.HelixRedeemKeyReq) (*phanto.HelixRedeemKeyResp, error) {
	return s.HelixRedeemKeyAPI.HelixRedeemKey(ctx, req)
}

func (s *Server) GetKeyHandlerGroup(ctx context.Context, req *phanto.GetKeyHandlerGroupReq) (*phanto.GetKeyHandlerGroupResp, error) {
	return s.GetKeyHandlerGroupAPI.GetKeyHandlerGroup(ctx, req)
}

func (s *Server) GetKeyHandlerGroups(ctx context.Context, req *phanto.GetKeyHandlerGroupsReq) (*phanto.GetKeyHandlerGroupsResp, error) {
	return s.GetKeyHandlerGroupsAPI.GetKeyHandlerGroups(ctx, req)
}

func (s *Server) UpdateKeyHandlerGroup(ctx context.Context, req *phanto.UpdateKeyHandlerGroupsReq) (*phanto.UpdateKeyHandlerGroupsResp, error) {
	return s.UpdateKeyHandlerGroupAPI.UpdateKeyHandlerGroup(ctx, req)
}

func (s *Server) HelixRedeemKeyByBatch(ctx context.Context, req *phanto.HelixRedeemKeyByBatchReq) (*phanto.HelixRedeemKeyByBatchResp, error) {
	return s.HelixRedeemKeyByBatchAPI.HelixRedeemKeyByBatch(ctx, req)
}

func (s *Server) AdminGetKeyStatus(ctx context.Context, req *phanto.AdminGetKeyStatusReq) (*phanto.AdminGetKeyStatusResp, error) {
	return s.AdminGetKeyStatusAPI.AdminGetKeyStatus(ctx, req)
}
