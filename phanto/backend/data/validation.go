package data

import (
	"time"

	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
)

type Validation struct {
	IsValid            bool
	KeyCode            code.RawHashed
	KeyStatus          status.KeyStatus
	ClaimerUserID      string
	ClaimerCountry     string
	ClaimerState       string
	ClaimerZip         string
	ClaimTime          time.Time
	KeyBatchID         string
	KeyPoolID          string
	ProductType        string
	ProductDescription string
	SKU                string
	SNSTopic           string
	StartDate          time.Time
	EndDate            time.Time
}
