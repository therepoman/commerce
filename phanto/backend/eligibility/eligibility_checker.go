package eligibility

import (
	"context"
	"time"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/tenants"
	phanto_tenant "code.justin.tv/commerce/phanto/rpc/tenant"
	"github.com/pkg/errors"
)

type EligibilityChecker interface {
	IsUserEligibleToRedeemKey(ctx context.Context, userID string, claimData data.Validation) (bool, error)
}

type checker struct {
	TenantMap tenants.TenantMap `inject:""`
}

func NewEligibilityChecker() EligibilityChecker {
	c := &checker{}
	return c
}

func (c *checker) IsUserEligibleToRedeemKey(ctx context.Context, userID string, claimData data.Validation) (bool, error) {

	client, err := c.TenantMap.Get(claimData.ProductType)
	if err != nil {
		return false, errors.Wrapf(err, "getting PhantoTenant client for product type: %v", claimData.ProductType)
	}

	// Backwards compatibility - if no tenant is configured, assume all users are eligible.
	if client == nil {
		return true, nil
	}

	// Call the client to determine eligibility.
	req := &phanto_tenant.IsUserEligibleToRedeemKeyRequest{
		UserId: userID,
		Sku:    claimData.SKU,
	}
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	resp, err := client.IsUserEligibleToRedeemKey(ctx, req)
	if err != nil {
		return false, errors.Wrap(err, "checking key eligibility")
	}

	return resp.IsEligible, nil
}
