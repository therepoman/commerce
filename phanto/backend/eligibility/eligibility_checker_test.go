package eligibility

import (
	"context"
	"testing"

	phanto_tenant_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/rpc/tenant"
	phanto_tenant "code.justin.tv/commerce/phanto/rpc/tenant"

	"code.justin.tv/commerce/phanto/backend/data"
	tenants_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/tenants"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"

	. "github.com/smartystreets/goconvey/convey"
)

func TestEligibilityChecker_IsUserEligibleToRedeemKey(t *testing.T) {
	Convey("TestEligibilityChecker_IsUserEligibleToRedeemKey", t, func() {

		mockPhantoTenant := new(phanto_tenant_mock.PhantoTenant)
		mockTenantMap := new(tenants_mock.TenantMap)
		c := checker{
			TenantMap: mockTenantMap,
		}
		ctx := context.Background()
		userId := "123456"

		Convey("GIVEN get tenant map returns error WHEN IsUserEligibleToRedeemKey THEN return false, error", func() {
			mockTenantMap.On("Get", mock.Anything).Return(nil, errors.New("mocked error"))

			isEligible, err := c.IsUserEligibleToRedeemKey(ctx, userId, data.Validation{})
			So(err, ShouldBeError)
			So(isEligible, ShouldBeFalse)
		})

		Convey("GIVEN get tenant map returns nil WHEN IsUserEligibleToRedeemKey THEN return true, nil", func() {
			mockTenantMap.On("Get", mock.Anything).Return(nil, nil)

			isEligible, err := c.IsUserEligibleToRedeemKey(ctx, userId, data.Validation{})
			So(err, ShouldBeNil)
			So(isEligible, ShouldBeTrue)
		})

		Convey("GIVEN get tenant map returns valid client WHEN IsUserEligibleToRedeemKey", func() {
			mockTenantMap.On("Get", mock.Anything).Return(mockPhantoTenant, nil)

			Convey("GIVEN error WHEN client.IsUserEligibleToRedeemKey THEN return false, error", func() {
				mockPhantoTenant.On("IsUserEligibleToRedeemKey", mock.Anything, mock.Anything).Return(nil, errors.New("mocked error"))

				isEligible, err := c.IsUserEligibleToRedeemKey(ctx, userId, data.Validation{})
				So(err, ShouldBeError)
				So(isEligible, ShouldBeFalse)
			})

			Convey("GIVEN isEligible is true WHEN client.IsUserEligibleToRedeemKey THEN return true, nil", func() {
				mockPhantoTenant.On("IsUserEligibleToRedeemKey", mock.Anything, mock.Anything).Return(&phanto_tenant.IsUserEligibleToRedeemKeyResponse{
					IsEligible: true,
				}, nil)

				isEligible, err := c.IsUserEligibleToRedeemKey(ctx, userId, data.Validation{})
				So(err, ShouldBeNil)
				So(isEligible, ShouldBeTrue)
			})

			Convey("GIVEN isEligible is false WHEN client.IsUserEligibleToRedeemKey THEN return false, nil", func() {
				mockPhantoTenant.On("IsUserEligibleToRedeemKey", mock.Anything, mock.Anything).Return(&phanto_tenant.IsUserEligibleToRedeemKeyResponse{
					IsEligible: false,
				}, nil)

				isEligible, err := c.IsUserEligibleToRedeemKey(ctx, userId, data.Validation{})
				So(err, ShouldBeNil)
				So(isEligible, ShouldBeFalse)
			})
		})
	})
}
