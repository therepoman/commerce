package product

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/backend/product_type"
	"github.com/pkg/errors"
)

const (
	ProductTypeBits = "bits"
)

type Validator interface {
	Validate(ctx context.Context, productType string, sku string) (claimData data.Validation, err error)
}

type ProductGetter interface {
	GetProduct(ctx context.Context, sku string) (*product.Product, error)
}

type validator struct {
	ProductTypeValidator product_type.Validator `inject:"productTypeValidator"`
	DAO                  product.DAO            `inject:""`
	BitsProductGetter    ProductGetter          `inject:"bitsProductGetter"`
}

func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(ctx context.Context, productType string, sku string) (claimData data.Validation, err error) {
	claimData.IsValid = false

	switch productType {
	case ProductTypeBits:
		bitsProduct, err := v.BitsProductGetter.GetProduct(ctx, sku)
		if err != nil {
			return claimData, errors.Wrap(err, fmt.Sprintf("downstream dependency error: getting Bits Product '%s'", sku))
		}
		if bitsProduct == nil {
			return claimData, errors.New(fmt.Sprintf("Bits Product '%s' does not exist in Bits Catalog", sku))
		}
		productTypeClaimData, err := v.ProductTypeValidator.Validate(ProductTypeBits)
		if err != nil || !productTypeClaimData.IsValid {
			return claimData, err
		}
		claimData = productTypeClaimData
		claimData.SKU = bitsProduct.SKU
		claimData.ProductDescription = bitsProduct.Description
	default:
		dynamoProduct, err := v.DAO.GetProduct(productType, sku)
		if err != nil {
			return claimData, errors.Wrap(err, fmt.Sprintf("downstream dependency error: getting product '%s' from Dynamo", sku))
		}
		if dynamoProduct == nil {
			return claimData, errors.New(fmt.Sprintf("product '%s' not found in Dynamo", sku))
		}
		productTypeClaimData, err := v.ProductTypeValidator.Validate(dynamoProduct.ProductType)
		if err != nil || !productTypeClaimData.IsValid {
			return claimData, err
		}
		claimData = productTypeClaimData
		claimData.SKU = dynamoProduct.SKU
		claimData.ProductDescription = dynamoProduct.Description
	}

	return
}
