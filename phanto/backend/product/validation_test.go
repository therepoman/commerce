package product

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/product"
	petozi_client_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/clients/petozi"
	product_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product"
	product_type_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/product_type"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a key validator", t, func() {
		ctx := context.Background()
		dao := new(product_mock.DAO)
		productTypeValidator := new(product_type_mock.Validator)
		bitsProductGetter := new(petozi_client_mock.Client)

		v := &validator{
			DAO:                  dao,
			ProductTypeValidator: productTypeValidator,
			BitsProductGetter:    bitsProductGetter,
		}

		productType := "1231231231123"
		sku := "asdfasdf"
		description := "Spend a day with a walrus"

		Convey("When we error fetching the product from Dynamo", func() {
			dao.On("GetProduct", productType, sku).Return(nil, errors.New("THE WALRUS STRIKES"))

			Convey("We should return an internal server error", func() {
				claimData, err := v.Validate(ctx, productType, sku)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we do not have a record for the product in Dynamo", func() {
			dao.On("GetProduct", productType, sku).Return(nil, nil)

			Convey("We should return a not found error", func() {
				claimData, err := v.Validate(ctx, productType, sku)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we find the product record in Dynamo that is active", func() {
			productRecord := &product.Product{
				ProductType: productType,
				SKU:         sku,
				Description: description,
			}

			snsTopic := "walrusTopic"

			productTypeClaimData := data.Validation{
				IsValid:     true,
				SNSTopic:    snsTopic,
				ProductType: productType,
			}
			dao.On("GetProduct", productType, sku).Return(productRecord, nil)
			Convey("when we error validating the product type associated with the product", func() {
				productTypeClaimData.IsValid = false

				productTypeValidator.On("Validate", productType).Return(productTypeClaimData, errors.New("WALRUS STRIKE"))

				Convey("We should return a not found error", func() {
					claimData, err := v.Validate(ctx, productType, sku)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When when the product type associated with it is invalid", func() {
				productTypeClaimData.IsValid = false

				productTypeValidator.On("Validate", productType).Return(productTypeClaimData, nil)

				Convey("We should return that it is not valid with no error", func() {
					claimData, err := v.Validate(ctx, productType, sku)

					So(claimData.IsValid, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("When the product type associated is valid", func() {
				productTypeValidator.On("Validate", productType).Return(productTypeClaimData, nil)

				Convey("We should return that it is valid", func() {
					claimData, err := v.Validate(ctx, productType, sku)

					So(claimData.IsValid, ShouldBeTrue)
					So(claimData.ProductType, ShouldEqual, productType)
					So(claimData.ProductDescription, ShouldEqual, description)
					So(claimData.SNSTopic, ShouldEqual, snsTopic)
					So(claimData.SKU, ShouldEqual, sku)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("(BITS) When we find the product record in Dynamo that is active", func() {
			productType := "bits"
			productRecord := &product.Product{
				ProductType: productType,
				SKU:         sku,
				Description: description,
			}

			snsTopic := "walrusTopic"

			productTypeClaimData := data.Validation{
				IsValid:     true,
				SNSTopic:    snsTopic,
				ProductType: productType,
			}
			dao.On("GetProduct", productType, sku).Return(productRecord, nil)

			Convey("(BITS) When call to Petozi errored", func() {
				bitsProductGetter.On("GetProduct", ctx, sku).Return(nil, errors.New("ERROR"))
				claimData, err := v.Validate(ctx, productType, sku)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})

			Convey("(BITS) When SKU cannot be found as Bits product from Petozi", func() {
				bitsProductGetter.On("GetProduct", ctx, sku).Return(nil, nil)
				claimData, err := v.Validate(ctx, productType, sku)

				So(claimData.IsValid, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})

			Convey("(BITS) When SKU was found as Bits product from Petozi and everything else passed", func() {
				bitsProductGetter.On("GetProduct", ctx, sku).Return(productRecord, nil)
				productTypeValidator.On("Validate", productType).Return(productTypeClaimData, nil)
				claimData, err := v.Validate(ctx, productType, sku)

				So(claimData.IsValid, ShouldBeTrue)
				So(claimData.ProductType, ShouldEqual, productType)
				So(claimData.ProductDescription, ShouldEqual, description)
				So(claimData.SNSTopic, ShouldEqual, snsTopic)
				So(claimData.SKU, ShouldEqual, sku)
				So(err, ShouldBeNil)
			})

		})
	})
}
