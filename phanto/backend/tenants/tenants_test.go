package tenants

import (
	"sync"
	"testing"

	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/rpc/tenant"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestTenantMap_Get(t *testing.T) {
	Convey("TestTenantMap_Get", t, func() {

		mockPhantoTenant := new(phanto_tenant_mock.PhantoTenant)
		mockDao := new(product_type_mock.DAO)
		tMap := tenantMap{
			Tenants:        new(sync.Map),
			ProductTypeDAO: mockDao,
		}

		Convey("GIVEN client exists in map WHEN Get THEN return existing client", func() {
			tMap.setMap("doesExist", mockPhantoTenant)

			client, err := tMap.Get("doesExist")
			So(err, ShouldBeNil)
			So(client, ShouldEqual, mockPhantoTenant)
			mockDao.AssertNotCalled(t, "GetProductType", mock.Anything)
		})

		Convey("GIVEN no client exists in map WHEN Get", func() {

			productType := "doesntExist"

			Convey("GIVEN product type lookup has error WHEN GetProductType THEN return error", func() {
				mockDao.On("GetProductType", mock.Anything).Return(nil, errors.New("mocked error"))

				client, err := tMap.Get(productType)
				So(err, ShouldBeError)
				So(client, ShouldBeNil)
			})

			Convey("GIVEN product type with no Tenant URL WHEN GetProductType THEN return nil", func() {
				mockDao.On("GetProductType", mock.Anything).Return(&product_type.ProductType{}, nil)

				client, err := tMap.Get(productType)
				So(err, ShouldBeNil)
				So(client, ShouldBeNil)
			})

			Convey("GIVEN product type with malformed Tenant URL WHEN GetProductType THEN return error", func() {
				mockDao.On("GetProductType", mock.Anything).Return(&product_type.ProductType{
					TenantURL: "this is not a URL",
				}, nil)

				client, err := tMap.Get(productType)
				So(err, ShouldBeError)
				So(client, ShouldBeNil)
			})

			Convey("GIVEN product type with valid URL WHEN GetProductType THEN instantiate, save and return new client", func() {
				mockDao.On("GetProductType", mock.Anything).Return(&product_type.ProductType{
					TenantURL: "http://localhost",
				}, nil)

				_, ok := tMap.getMap(productType)
				So(ok, ShouldBeFalse)

				client, err := tMap.Get(productType)
				So(err, ShouldBeNil)
				So(client, ShouldNotBeNil)
				_, ok = tMap.getMap(productType)
				So(ok, ShouldBeTrue)
			})
		})
	})
}
