package tenants

import (
	"net/http"
	"net/url"
	"sync"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	phanto_tenant "code.justin.tv/commerce/phanto/rpc/tenant"
	"github.com/pkg/errors"
)

type TenantMap interface {
	Get(productType string) (phanto_tenant.PhantoTenant, error)
}

type tenantMap struct {
	Tenants        *sync.Map
	ProductTypeDAO product_type.DAO `inject:""`
}

func NewTenantMap() TenantMap {
	t := &tenantMap{}
	t.Tenants = new(sync.Map)
	return t
}

func (t *tenantMap) Get(productType string) (phanto_tenant.PhantoTenant, error) {
	// Look up the client and return it if it exists.
	if client, ok := t.getMap(productType); ok {
		return client, nil
	}

	// Try to instantiate a client if one doesn't exist.
	// First, find the product type in the database.
	dynamoProductType, err := t.ProductTypeDAO.GetProductType(productType)
	if err != nil {
		return nil, errors.Wrapf(err, "while creating a PhantoTenant client, unable to get productType %v", productType)
	}

	// Then, get the endpoint if one is configured.
	if strings.Blank(dynamoProductType.TenantURL) {
		// No endpoint configured - cache this fact so we don't have to hit the DB again
		t.setMap(productType, nil)

		// return nil, nil since there was no client or error
		return nil, nil
	}

	// Make sure it looks like a valid URL
	if _, err := url.ParseRequestURI(dynamoProductType.TenantURL); err != nil {
		return nil, errors.Wrapf(err, "invalid tenant URL for product type: %v", productType)
	}

	// Instantiate the client with this endpoint.
	client := phanto_tenant.NewPhantoTenantProtobufClient(dynamoProductType.TenantURL, http.DefaultClient)

	// Save it for future requests
	t.setMap(productType, client)

	return client, nil
}

func (t *tenantMap) getMap(key string) (phanto_tenant.PhantoTenant, bool) {
	clientInterface, ok := t.Tenants.Load(key)
	if !ok {
		return nil, false
	}

	client, ok := clientInterface.(phanto_tenant.PhantoTenant)
	if !ok {
		return nil, false
	}

	return client, true
}

func (t *tenantMap) setMap(key string, tenant phanto_tenant.PhantoTenant) {
	t.Tenants.Store(key, tenant)
}
