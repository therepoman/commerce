package get_key_status_base

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authentication "code.justin.tv/commerce/phanto/backend/api/base/authentication"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type APIImpl struct {
	BaseContainer *base.Container `inject:""`
	Validator     key.Validator   `inject:"keyValidator"`
	Auditor       audit.Auditor   `inject:""`
}

type GetKeyStatusOptions struct {
	AuthenticationFunc base_authentication.Func
	AuthorizationFunc  base_authorization.Func
	ThrottleFunc       base_throttle.Func
	PerformAuditing    bool
	AuditOperation     audit.Operation
}

type API interface {
	GetKeyStatusBase(ctx context.Context, req *phanto.GetKeyStatusReq, opt GetKeyStatusOptions) (keyValidationData *data.Validation, auditRecord *call_audit_record.CallAuditRecord, err error)
}

func (api *APIImpl) GetKeyStatusBase(ctx context.Context, req *phanto.GetKeyStatusReq, opt GetKeyStatusOptions) (keyValidationData *data.Validation, auditRecord *call_audit_record.CallAuditRecord, err error) {
	if req == nil {
		return nil, nil, twirp.InvalidArgumentError("Key", "Request body cannot be empty")
	} else if strings.Blank(req.Key) {
		return nil, nil, twirp.InvalidArgumentError("Key", "Key cannot be blank")
	}

	formattedCode := code.Formatted(req.Key)
	if !formattedCode.IsValid() {
		return nil, nil, twirp.InvalidArgumentError("Key", "Key not in correct format")
	}

	err = opt.AuthorizationFunc(ctx, *api.BaseContainer, "")
	if err != nil {
		return nil, nil, err
	}

	authedUserID, err := opt.AuthenticationFunc(ctx, *api.BaseContainer)
	if err != nil {
		return nil, nil, err
	}

	err = opt.ThrottleFunc(ctx, *api.BaseContainer, authedUserID)
	if err != nil {
		return nil, nil, err
	}

	if opt.PerformAuditing {
		auditRecord, err = api.Auditor.AuditRequest(ctx, &audit.Request{Operation: opt.AuditOperation, Body: req})
		if err != nil {
			return nil, nil, twirp.InternalErrorWith(err)
		}
	}

	keyHashed, err := formattedCode.Raw().Hash()
	if err != nil {
		log.WithError(err).Error("error hashing key")
		return nil, auditRecord, twirp.InternalError("Error getting key status")
	}

	claimData, err := api.Validator.ValidateForUser(ctx, keyHashed, authedUserID)
	if err == key.NotFoundError {
		return nil, auditRecord, twirp.NotFoundError(err.Error())
	} else if err != nil {
		log.WithError(err).Error("Could not fetch key from dynamo")
		return nil, auditRecord, twirp.InternalErrorWith(err)
	}

	return &claimData, auditRecord, nil
}
