package get_key_status_base_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authentication "code.justin.tv/commerce/phanto/backend/api/base/authentication"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status_base"
	authorization_const "code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyStatusBase(t *testing.T) {
	Convey("Given a key status API", t, func() {
		keyValidator := new(key_mock.Validator)
		authorizer := new(authorization_mock.Authorizer)
		auditor := new(audit_mock.Auditor)
		throttler := new(throttle_mock.IThrottler)

		api := &get_key_status_base.APIImpl{
			BaseContainer: &base.Container{
				Authorizer: authorizer,
				Throttler:  throttler,
			},
			Validator: keyValidator,
			Auditor:   auditor,
		}

		keyCode := "asdfa-sdfas-dfasd"
		userId := "authentic-ohki"

		opt := get_key_status_base.GetKeyStatusOptions{
			AuthenticationFunc: base_authentication.FixedUserAuthentication(userId),
			AuthorizationFunc:  base_authorization.NoAuthorization,
			ThrottleFunc:       base_throttle.NoThrottle,
			PerformAuditing:    false,
		}

		Convey("Given a nil request", func() {
			var req *phanto.GetKeyStatusReq = nil

			Convey("We should return an error", func() {
				_, _, err := api.GetKeyStatusBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an empty key", func() {
			req := &phanto.GetKeyStatusReq{
				Key: "",
			}

			Convey("We should return an error", func() {
				_, _, err := api.GetKeyStatusBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an invalid key", func() {
			req := &phanto.GetKeyStatusReq{
				Key: "NOT-A-VALID-KEY",
			}

			Convey("We should return an error", func() {
				_, _, err := api.GetKeyStatusBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with a key", func() {
			ctx := context.WithValue(context.Background(), authorization_const.AuthenticatedUserIDContextKey, userId)

			req := &phanto.GetKeyStatusReq{
				Key: keyCode,
			}

			hashedCode, err := code.Formatted(keyCode).Raw().Hash()
			So(err, ShouldBeNil)

			keyClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           "walrusTopic",
				ProductType:        "productType",
				ProductDescription: "productDescription",
				KeyPoolID:          "keyPoolID",
				KeyBatchID:         "keyBatchID",
				KeyCode:            hashedCode,
				SKU:                "sku",
				KeyStatus:          status.KeyStatusUnclaimed,
			}

			Convey("When the validator errors with an unknown error", func() {
				keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, errors.New("WALRUS STRIKE!!!"))

				Convey("we should return an internal error", func() {
					_, _, err := api.GetKeyStatusBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.Internal)
				})
			})

			Convey("When the validator errors with a not found error", func() {
				keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, key.NotFoundError)

				Convey("we should return a not found error", func() {
					_, _, err := api.GetKeyStatusBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.NotFound)
				})
			})

			Convey("When the validator returns successfully", func() {
				keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, nil)

				Convey("We should return a valid response", func() {
					result, _, err := api.GetKeyStatusBase(ctx, req, opt)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.SKU, ShouldEqual, keyClaimData.SKU)
					So(result.ProductDescription, ShouldEqual, keyClaimData.ProductDescription)
					So(string(result.KeyStatus), ShouldEqual, string(keyClaimData.KeyStatus))
					So(result.KeyBatchID, ShouldEqual, keyClaimData.KeyBatchID)
					So(result.KeyPoolID, ShouldEqual, keyClaimData.KeyPoolID)
					So(result.ProductType, ShouldEqual, keyClaimData.ProductType)
				})
			})
		})
	})
}
