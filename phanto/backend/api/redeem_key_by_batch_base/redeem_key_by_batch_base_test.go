package redeem_key_by_batch_base_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/idempotency"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/authorization"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPIImpl_RedeemKeyByBatchBase(t *testing.T) {
	Convey("Given a redeem key by batch base api", t, func() {
		validator := new(key_mock.Validator)
		claimer := new(key_mock.Claimer)
		keyBatchAuthorizer := new(authorization_mock.HandlerAuthorizer)
		auditor := new(audit_mock.Auditor)
		keySetChecker := new(key_set_mock.Checker)
		keySetEditor := new(key_set_mock.Editor)
		keyBatchDAO := new(key_batch_mock.DAO)
		idempotencyChecker := new(idempotency_mock.Checker)

		api := &redeem_key_by_batch_base.APIImpl{
			Validator:          validator,
			Claimer:            claimer,
			Auditor:            auditor,
			KeySetChecker:      keySetChecker,
			KeySetEditor:       keySetEditor,
			KeyBatchAuthorizer: keyBatchAuthorizer,
			KeyBatchDAO:        keyBatchDAO,
			IdempotencyChecker: idempotencyChecker,
		}

		Convey("Errors if request is nil", func() {
			_, _, _, err := api.RedeemKeyByBatchBase(context.Background(), nil, redeem_key_by_batch_base.RedeemKeyByBatchOptions{})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors if batch id is blank", func() {
			_, _, _, err := api.RedeemKeyByBatchBase(context.Background(), &phanto.RedeemKeyByBatchReq{
				BatchId: "",
			}, redeem_key_by_batch_base.RedeemKeyByBatchOptions{})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors if user id is blank", func() {
			_, _, _, err := api.RedeemKeyByBatchBase(context.Background(), &phanto.RedeemKeyByBatchReq{
				BatchId: "batch-id",
				UserId:  "",
			}, redeem_key_by_batch_base.RedeemKeyByBatchOptions{})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors if user ip is blank", func() {
			_, _, _, err := api.RedeemKeyByBatchBase(context.Background(), &phanto.RedeemKeyByBatchReq{
				BatchId: "batch-id",
				UserId:  "user-id",
				UserIp:  "",
			}, redeem_key_by_batch_base.RedeemKeyByBatchOptions{})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors if redeem type is invalid", func() {
			_, _, _, err := api.RedeemKeyByBatchBase(context.Background(), &phanto.RedeemKeyByBatchReq{
				BatchId: "batch-id",
				UserId:  "user-id",
				UserIp:  "127.0.0.1",
			}, redeem_key_by_batch_base.RedeemKeyByBatchOptions{
				RedeemType: base.RedeemType("invalid-redeem-type"),
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			ctx := context.Background()
			req := &phanto.RedeemKeyByBatchReq{
				BatchId:        "batch-id",
				UserId:         "user-id",
				UserIp:         "127.0.0.1",
				IdempotencyKey: "idempotency-key",
			}
			opt := redeem_key_by_batch_base.RedeemKeyByBatchOptions{
				RedeemType: base.RedeemType_HelixByBatchAPI,
			}

			Convey("Errors when IdempotencyChecker errors", func() {
				idempotencyChecker.On("IsNewRequest", ctx, req.BatchId, req.IdempotencyKey).Return(false, errors.New("test error"))
				_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("Errors when IdempotencyCheckers says this is an old request", func() {
				idempotencyChecker.On("IsNewRequest", ctx, req.BatchId, req.IdempotencyKey).Return(false, nil)
				_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
				test.AssertTwirpError(err, twirp.FailedPrecondition)
			})

			Convey("When IdempotencyCheckers says this is a new request", func() {
				idempotencyChecker.On("IsNewRequest", ctx, req.BatchId, req.IdempotencyKey).Return(true, nil)

				Convey("Errors when KeyBatchDAO errors", func() {
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(nil, errors.New("test error"))
					_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Errors when KeyBatchDAO returns nil", func() {
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(nil, nil)
					_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})

				Convey("When get key batch returns", func() {
					poolID := "pool-id"
					batch := &key_batch.KeyBatch{
						BatchID: req.BatchId,
						PoolID:  poolID,
					}
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(batch, nil)

					Convey("Errors when authorizer says not authorized", func() {
						keyBatchAuthorizer.On("AuthorizeByClientID", ctx, batch).Return(false)
						_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
						test.AssertTwirpError(err, twirp.PermissionDenied)
					})

					Convey("When authorizer says authorized", func() {
						keyBatchAuthorizer.On("AuthorizeByClientID", ctx, batch).Return(true)

						Convey("Errors when key set checker errors", func() {
							keySetChecker.On("DoesPoolIDUseRedisKeySet", poolID).Return(false, errors.New("test error"))
							_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
							test.AssertTwirpError(err, twirp.Internal)
						})

						Convey("When key set checker says key pool does not use key set", func() {
							keySetChecker.On("DoesPoolIDUseRedisKeySet", poolID).Return(false, nil)
							_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
							test.AssertTwirpError(err, twirp.FailedPrecondition)
						})

						Convey("When key set checker says key pool does use key set", func() {
							keySetChecker.On("DoesPoolIDUseRedisKeySet", poolID).Return(true, nil)

							Convey("Errors when get key count errors", func() {
								keySetChecker.On("GetKeyCount", req.BatchId).Return(int64(0), errors.New("test-error"))
								_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
								test.AssertTwirpError(err, twirp.Internal)
							})

							Convey("Errors when get key count returns zero keys", func() {
								keySetChecker.On("GetKeyCount", req.BatchId).Return(int64(0), nil)
								_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
								So(err, ShouldEqual, redeem_key_by_batch_base.NoRemainingKeysError)
							})

							Convey("When get key count returns two keys", func() {
								keySetChecker.On("GetKeyCount", req.BatchId).Return(int64(2), nil)

								Convey("Errors when key set editor pop errors", func() {
									keySetEditor.On("PopRandomKeyFromKeySet", req.BatchId).Return(code.RawHashed(""), errors.New("test error"))
									_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
									test.AssertTwirpError(err, twirp.Internal)
								})

								Convey("Errors when key set editor pop returns a redis nil error", func() {
									keySetEditor.On("PopRandomKeyFromKeySet", req.BatchId).Return(code.RawHashed(""), redis.Nil)
									_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
									So(err, ShouldEqual, redeem_key_by_batch_base.NoRemainingKeysError)
								})

								Convey("When key set pop succeeds", func() {
									hashedCode := code.RawHashed("some-hashed-code")
									keySetEditor.On("PopRandomKeyFromKeySet", req.BatchId).Return(hashedCode, nil)

									Convey("Errors when validator returns key not found error", func() {
										validator.On("ValidateForUser", ctx, hashedCode, req.UserId).Return(data.Validation{}, key.NotFoundError)
										_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
										test.AssertTwirpError(err, twirp.Internal)
									})

									Convey("Errors when validator returns a different error", func() {
										validator.On("ValidateForUser", ctx, hashedCode, req.UserId).Return(data.Validation{}, errors.New("test error"))
										_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
										test.AssertTwirpError(err, twirp.Internal)
									})

									Convey("Errors when validator returns claim data that is not valid", func() {
										validator.On("ValidateForUser", ctx, hashedCode, req.UserId).Return(data.Validation{
											IsValid: false,
										}, nil)
										_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
										test.AssertTwirpError(err, twirp.FailedPrecondition)
									})

									Convey("When validator returns claim data that is valid", func() {
										v := data.Validation{
											IsValid: true,
										}
										validator.On("ValidateForUser", ctx, hashedCode, req.UserId).Return(v, nil)

										Convey("Errors when claimer errors", func() {
											claimer.On("Claim", ctx, req.UserId, req.UserIp, v, base.RedeemType_HelixByBatchAPI).Return(errors.New("test error"))
											_, _, _, err := api.RedeemKeyByBatchBase(ctx, req, opt)
											test.AssertTwirpError(err, twirp.Internal)
										})

										Convey("Succeeds when claimer succeeds", func() {
											claimer.On("Claim", ctx, req.UserId, req.UserIp, v, base.RedeemType_HelixByBatchAPI).Return(nil)
											claimData, _, newKeyCount, err := api.RedeemKeyByBatchBase(ctx, req, opt)
											So(err, ShouldBeNil)
											So(claimData, ShouldNotBeNil)
											So(claimData.IsValid, ShouldBeTrue)
											So(newKeyCount, ShouldEqual, 1)
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
