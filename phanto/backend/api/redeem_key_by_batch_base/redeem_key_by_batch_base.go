package redeem_key_by_batch_base

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/idempotency"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key_batch/authorization"
	"code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/go-redis/redis"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type APIImpl struct {
	BaseContainer      *base.Container                 `inject:""`
	Validator          key.Validator                   `inject:"keyValidator"`
	Claimer            key.Claimer                     `inject:""`
	Auditor            audit.Auditor                   `inject:""`
	KeySetChecker      key_set.Checker                 `inject:""`
	KeySetEditor       key_set.Editor                  `inject:""`
	KeyBatchAuthorizer authorization.HandlerAuthorizer `inject:""`
	KeyBatchDAO        key_batch.DAO                   `inject:""`
	IdempotencyChecker idempotency.Checker             `inject:""`
}

type RedeemKeyByBatchOptions struct {
	PerformAuditing bool
	AuditOperation  audit.Operation
	RedeemType      base.RedeemType
}

var NoRemainingKeysError = twirp.NewError(twirp.FailedPrecondition, "Batch does not have any remaining unused keys")
var BatchDoesNotSupportError = twirp.NewError(twirp.FailedPrecondition, "Batch does not support this redemption method")
var ClientNotAuthorizedError = twirp.NewError(twirp.PermissionDenied, "Client does not have access to batch")
var DuplicateRequestError = twirp.NewError(twirp.FailedPrecondition, "Duplicate request")

var KnownError = map[error]interface{}{
	NoRemainingKeysError:     nil,
	BatchDoesNotSupportError: nil,
	ClientNotAuthorizedError: nil,
	DuplicateRequestError:    nil,
}

type API interface {
	RedeemKeyByBatchBase(ctx context.Context, req *phanto.RedeemKeyByBatchReq, opt RedeemKeyByBatchOptions) (keyValidationData *data.Validation, auditRecord *call_audit_record.CallAuditRecord, newKeyCount int64, err error)
}

func (api *APIImpl) RedeemKeyByBatchBase(ctx context.Context, req *phanto.RedeemKeyByBatchReq, opt RedeemKeyByBatchOptions) (keyValidationData *data.Validation, auditRecord *call_audit_record.CallAuditRecord, newKeyCount int64, err error) {
	if req == nil {
		return nil, nil, 0, twirp.InvalidArgumentError("BatchId", "Request body cannot be empty")
	} else if strings.Blank(req.BatchId) {
		return nil, nil, 0, twirp.InvalidArgumentError("BatchId", "BatchId cannot be blank")
	} else if strings.Blank(req.UserId) {
		return nil, nil, 0, twirp.InvalidArgumentError("UserId", "UserId cannot be blank")
	} else if strings.Blank(req.UserIp) {
		return nil, nil, 0, twirp.InvalidArgumentError("UserIp", "UserIp cannot be blank")
	} else if !base.IsValidRedeemType(opt.RedeemType) {
		return nil, nil, 0, twirp.InvalidArgumentError("RedeemType", "Invalid redemption type")
	}

	if opt.PerformAuditing {
		auditRecord, err = api.Auditor.AuditRequest(ctx, &audit.Request{Operation: opt.AuditOperation, Body: req})
		if err != nil {
			return nil, nil, 0, twirp.InternalErrorWith(err)
		}
	}

	idempotencyKey := req.IdempotencyKey
	if strings.Blank(idempotencyKey) {
		idempotencyKeyUUID, err := uuid.NewV4()
		if err != nil {
			return nil, nil, 0, twirp.InternalErrorWith(err)
		}
		idempotencyKey = idempotencyKeyUUID.String()
	}

	newRequest, err := api.IdempotencyChecker.IsNewRequest(ctx, req.BatchId, idempotencyKey)
	if err != nil {
		return nil, nil, 0, twirp.InternalErrorWith(err)
	}

	if !newRequest {
		return nil, nil, 0, DuplicateRequestError
	}

	keyBatch, err := api.KeyBatchDAO.GetKeyBatch(req.BatchId)
	if err != nil {
		log.WithError(err).Error("Error getting batch id from dynamo")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	}

	if keyBatch == nil {
		return nil, auditRecord, 0, twirp.InvalidArgumentError("BatchId", "BatchId not found")
	}

	if !api.KeyBatchAuthorizer.AuthorizeByClientID(ctx, keyBatch) {
		return nil, auditRecord, 0, ClientNotAuthorizedError
	}

	usesRedisKeySet, err := api.KeySetChecker.DoesPoolIDUseRedisKeySet(keyBatch.PoolID)
	if err != nil {
		log.WithError(err).Error("Error checking batch id for key set")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	}

	if !usesRedisKeySet {
		return nil, auditRecord, 0, BatchDoesNotSupportError
	}

	keyCount, err := api.KeySetChecker.GetKeyCount(req.BatchId)
	if err != nil {
		log.WithError(err).Error("error getting key count from redis key set")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	}

	if keyCount <= 0 {
		return nil, auditRecord, 0, NoRemainingKeysError
	}

	hashedKey, err := api.KeySetEditor.PopRandomKeyFromKeySet(req.BatchId)
	if err == redis.Nil {
		return nil, auditRecord, 0, NoRemainingKeysError
	} else if err != nil {
		log.WithError(err).Error("Error popping key from keyset")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	}

	claimData, err := api.Validator.ValidateForUser(ctx, hashedKey, req.UserId)
	if err == key.NotFoundError {
		log.WithError(err).Error("Key from key set was not found")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	} else if err != nil {
		log.WithError(err).Error("Could not fetch key from dynamo")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	}

	if !claimData.IsValid {
		return &claimData, auditRecord, 0, twirp.NewError(twirp.FailedPrecondition, "Key is not valid for claiming")
	}

	err = api.Claimer.Claim(ctx, req.UserId, req.UserIp, claimData, opt.RedeemType)
	if err != nil {
		log.WithError(err).Error("Could not claim key code")
		return nil, auditRecord, 0, twirp.InternalErrorWith(err)
	}

	return &claimData, auditRecord, keyCount - 1, nil
}
