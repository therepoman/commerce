package get_key_pools_by_handler_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_pools_by_handler"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_pool/authorization"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyPoolsByHandler(t *testing.T) {
	Convey("Given an API struct", t, func() {
		keyPoolDAO := new(key_pool_mock.DAO)
		authorizer := new(authorization_mock.UserAuthorizer)
		auditor := new(audit_mock.Auditor)

		api := &get_key_pools_by_handler.API{
			KeyPoolDAO: keyPoolDAO,
			Authorizer: authorizer,
			Auditor:    auditor,
		}

		handler := "test-handler"
		cursor := "test-cursor"

		Convey("Errors when given a nil request", func() {
			_, err := api.GetKeyPoolsByHandler(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when given a blank handler", func() {
			_, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
				Handler: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When the user is not authorized", func() {
			ctx := context.Background()

			authorizer.On("Authorize", ctx, handler).Return(false)

			Convey("We should return an error", func() {
				_, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
					Handler: handler,
					Cursor:  cursor,
				})
				test.AssertTwirpError(err, twirp.PermissionDenied)
			})
		})

		Convey("When the user is authorized", func() {
			ctx := context.Background()

			authorizer.On("Authorize", ctx, handler).Return(true)

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
					Handler: handler,
					Cursor:  cursor,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when KeyPoolDAO returns an error", func() {
					keyPoolDAO.On("GetKeyPoolsByHandler", handler, cursor).Return(nil, "", errors.New("test error"))
					_, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
						Handler: handler,
						Cursor:  cursor,
					})
					So(err, ShouldNotBeNil)
				})

				Convey("Succeeds when KeyPoolDAO returns empty", func() {
					keyPoolDAO.On("GetKeyPoolsByHandler", handler, cursor).Return([]*key_pool.KeyPool{}, "", nil)
					resp, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
						Handler: handler,
						Cursor:  cursor,
					})
					So(err, ShouldBeNil)
					So(len(resp.KeyPools), ShouldEqual, 0)
				})

				Convey("Succeeds when KeyPoolDAO returns single entry", func() {
					keyPoolDAO.On("GetKeyPoolsByHandler", handler, cursor).Return([]*key_pool.KeyPool{{PoolID: "P1"}}, "P1", nil)
					resp, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
						Handler: handler,
						Cursor:  cursor,
					})
					So(err, ShouldBeNil)
					So(len(resp.KeyPools), ShouldEqual, 1)
					So(resp.KeyPools[0].PoolId, ShouldEqual, "P1")
					So(resp.Cursor, ShouldEqual, "P1")
				})

				Convey("Succeeds when KeyPoolDAO returns two entries", func() {
					keyPoolDAO.On("GetKeyPoolsByHandler", handler, cursor).Return([]*key_pool.KeyPool{{PoolID: "P1"}, {PoolID: "P2"}}, "P2", nil)
					resp, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
						Handler: handler,
						Cursor:  cursor,
					})
					So(err, ShouldBeNil)
					So(len(resp.KeyPools), ShouldEqual, 2)
					So(resp.KeyPools[0].PoolId, ShouldEqual, "P1")
					So(resp.KeyPools[1].PoolId, ShouldEqual, "P2")
					So(resp.Cursor, ShouldEqual, "P2")
				})

				Convey("Succeeds when KeyPoolDAO is called with a cursor", func() {
					cursor := "P25"
					keyPoolDAO.On("GetKeyPoolsByHandler", handler, cursor).Return([]*key_pool.KeyPool{{PoolID: "P26"}}, "P26", nil)
					resp, err := api.GetKeyPoolsByHandler(context.Background(), &phanto.GetKeyPoolsByHandlerReq{
						Handler: handler,
						Cursor:  cursor,
					})
					So(err, ShouldBeNil)
					So(len(resp.KeyPools), ShouldEqual, 1)
					So(resp.KeyPools[0].PoolId, ShouldEqual, "P26")
					So(resp.Cursor, ShouldEqual, "P26")
				})
			})
		})
	})
}
