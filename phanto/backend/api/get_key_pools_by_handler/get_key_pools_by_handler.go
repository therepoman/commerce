package get_key_pools_by_handler

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/authorization"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolDAO key_pool.DAO                 `inject:""`
	Authorizer authorization.UserAuthorizer `inject:"keyPoolUserAuthorizer"`
	Auditor    audit.Auditor                `inject:""`
}

func (api *API) GetKeyPoolsByHandler(ctx context.Context, req *phanto.GetKeyPoolsByHandlerReq) (resp *phanto.GetKeyPoolsByHandlerResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Handler", "Request body cannot be nil")
	} else if strings.Blank(req.Handler) {
		return nil, twirp.InvalidArgumentError("Handler", "Handler cannot be blank")
	}

	if !api.Authorizer.Authorize(ctx, req.Handler) {
		return nil, twirp.NewError(twirp.PermissionDenied, "user does not have permission")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyPoolsByHandlerOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoPools, newCursor, err := api.KeyPoolDAO.GetKeyPoolsByHandler(req.Handler, req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key pools from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key pools")
	}

	respPools := key_pool.ToTwirpModel(dynamoPools)
	return &phanto.GetKeyPoolsByHandlerResp{
		KeyPools: respPools,
		Cursor:   newCursor,
	}, nil
}
