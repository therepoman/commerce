package get_key_handler_groups

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_handler_group.DAO `inject:""`
	Auditor audit.Auditor         `inject:""`
}

func (api *API) GetKeyHandlerGroups(ctx context.Context, req *phanto.GetKeyHandlerGroupsReq) (resp *phanto.GetKeyHandlerGroupsResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("HandlerGroupID", "Request body cannot be nil")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyHandlerGroups, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoGroups, newCursor, err := api.DAO.GetKeyHandlerGroups(req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key handler groups from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key handler groups")
	}

	return &phanto.GetKeyHandlerGroupsResp{
		KeyHandlerGroups: key_handler_group.ToKeyHandlerGroupsTwirpModel(dynamoGroups),
		Cursor:           newCursor,
	}, nil
}
