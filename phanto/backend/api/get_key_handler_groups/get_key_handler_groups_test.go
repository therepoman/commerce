package get_key_handler_groups_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_handler_groups"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyHandlerGroups(t *testing.T) {
	Convey("Given an API struct", t, func() {
		dao := new(key_handler_group_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &get_key_handler_groups.API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Errors when given a nil request", func() {
			_, err := api.GetKeyHandlerGroups(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetKeyHandlerGroups(context.Background(), &phanto.GetKeyHandlerGroupsReq{})
			test.AssertTwirpError(err, twirp.Internal)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when DAO returns an error", func() {
				dao.On("GetKeyHandlerGroups", "").Return(nil, "", errors.New("test error"))
				_, err := api.GetKeyHandlerGroups(context.Background(), &phanto.GetKeyHandlerGroupsReq{})
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when DAO returns empty", func() {
				dao.On("GetKeyHandlerGroups", "").Return([]*key_handler_group.KeyHandlerGroup{}, "", nil)
				resp, err := api.GetKeyHandlerGroups(context.Background(), &phanto.GetKeyHandlerGroupsReq{})
				So(err, ShouldBeNil)
				So(len(resp.KeyHandlerGroups), ShouldEqual, 0)
			})

			Convey("Succeeds when DAO returns single entry", func() {
				dao.On("GetKeyHandlerGroups", "").Return([]*key_handler_group.KeyHandlerGroup{{HandlerGroupID: "HG1"}}, "HG1", nil)
				resp, err := api.GetKeyHandlerGroups(context.Background(), &phanto.GetKeyHandlerGroupsReq{})
				So(err, ShouldBeNil)
				So(len(resp.KeyHandlerGroups), ShouldEqual, 1)
				So(resp.KeyHandlerGroups[0].HandlerGroupId, ShouldEqual, "HG1")
				So(resp.Cursor, ShouldEqual, "HG1")
			})

			Convey("Succeeds when KeyPoolDAO returns two entries", func() {
				dao.On("GetKeyHandlerGroups", "").Return([]*key_handler_group.KeyHandlerGroup{{HandlerGroupID: "HG1"}, {HandlerGroupID: "HG2"}}, "HG2", nil)
				resp, err := api.GetKeyHandlerGroups(context.Background(), &phanto.GetKeyHandlerGroupsReq{})
				So(err, ShouldBeNil)
				So(len(resp.KeyHandlerGroups), ShouldEqual, 2)
				So(resp.KeyHandlerGroups[0].HandlerGroupId, ShouldEqual, "HG1")
				So(resp.KeyHandlerGroups[1].HandlerGroupId, ShouldEqual, "HG2")
				So(resp.Cursor, ShouldEqual, "HG2")
			})

			Convey("Succeeds when KeyPoolDAO is called with a cursor", func() {
				dao.On("GetKeyHandlerGroups", "HG25").Return([]*key_handler_group.KeyHandlerGroup{{HandlerGroupID: "HG26"}}, "HG26", nil)
				resp, err := api.GetKeyHandlerGroups(context.Background(), &phanto.GetKeyHandlerGroupsReq{
					Cursor: "HG25",
				})
				So(err, ShouldBeNil)
				So(len(resp.KeyHandlerGroups), ShouldEqual, 1)
				So(resp.KeyHandlerGroups[0].HandlerGroupId, ShouldEqual, "HG26")
				So(resp.Cursor, ShouldEqual, "HG26")
			})
		})
	})
}
