package helix_get_key_status

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authentication "code.justin.tv/commerce/phanto/backend/api/base/authentication"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status_base"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/twitchtv/twirp"
)

type API struct {
	BaseContainer *base.Container         `inject:""`
	BaseAPI       get_key_status_base.API `inject:""`
	Auditor       audit.Auditor           `inject:""`
}

func (api *API) HelixGetKeyStatus(ctx context.Context, req *phanto.HelixGetKeyStatusReq) (resp *phanto.HelixGetKeyStatusResp, err error) {
	if !helix.IsWhitelistedHelixClient(ctx) {
		return nil, twirp.NewError(twirp.PermissionDenied, "Request from unknown client")
	}

	if req == nil {
		return nil, twirp.InvalidArgumentError("Keys", "Request body cannot be empty")
	} else if strings.Blank(req.UserId) {
		return nil, twirp.InvalidArgumentError("UserId", "UserId cannot be blank")
	} else if len(req.Keys) <= 0 {
		return nil, twirp.InvalidArgumentError("Keys", "Request must contain at least one key")
	} else if len(req.Keys) > 20 {
		return nil, twirp.InvalidArgumentError("Keys", "Request can contain no more than 20 keys")
	}

	err = base_throttle.ThrottleUserExponentiallyByKeyFunc("HelixGetKeyStatus")(ctx, *api.BaseContainer, req.UserId)
	if err != nil {
		return nil, err
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.HelixGetKeyStatusOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	results := make([]*phanto.HelixKeyResp, 0)
	for _, key := range req.Keys {
		claimData, _, err := api.BaseAPI.GetKeyStatusBase(ctx, &phanto.GetKeyStatusReq{
			Key: key,
		}, get_key_status_base.GetKeyStatusOptions{
			AuthenticationFunc: base_authentication.FixedUserAuthentication(req.UserId),
			AuthorizationFunc:  base_authorization.NoAuthorization,
			ThrottleFunc:       base_throttle.NoThrottle,
			PerformAuditing:    false,
		})

		results = append(results, &phanto.HelixKeyResp{
			Key:    key,
			Status: helix.GetKeyStatus(false, claimData, err),
		})
	}

	return &phanto.HelixGetKeyStatusResp{
		Results: results,
	}, nil
}
