package invalidate_keys

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_InvalidateKeys(t *testing.T) {
	Convey("Given an Invalidate Keys API", t, func() {
		keyDAO := new(key_mock.DAO)
		keyBatchDAO := new(key_batch_mock.DAO)
		keyPoolDAO := new(key_pool_mock.DAO)
		auditor := new(audit_mock.Auditor)
		keySetEditor := new(key_set_mock.Editor)
		keySetChecker := new(key_set_mock.Checker)
		api := API{
			KeyDAO:        keyDAO,
			KeyBatchDAO:   keyBatchDAO,
			KeyPoolDAO:    keyPoolDAO,
			KeySetEditor:  keySetEditor,
			KeySetChecker: keySetChecker,
			Auditor:       auditor,
		}

		Convey("Given a nil request", func() {
			var req *phanto.InvalidateKeysReq = nil

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeys(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with nil keys", func() {
			req := &phanto.InvalidateKeysReq{
				Keys: nil,
			}

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeys(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with empty keys", func() {
			req := &phanto.InvalidateKeysReq{
				Keys: []string{},
			}

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeys(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with one invalid key", func() {
			req := &phanto.InvalidateKeysReq{
				Keys: []string{"NOT-A-VALID-KEY"},
			}

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeys(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with one valid key and one invalid key", func() {
			req := &phanto.InvalidateKeysReq{
				Keys: []string{"0THIS-KEYIS-VALID", "NOT-A-VALID-KEY"},
			}

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeys(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with valid keys", func() {
			keyCode1 := "ABCDE12345FGHI1"
			keyCode2 := "ABCDE12345FGHI2"

			req := &phanto.InvalidateKeysReq{
				Keys: []string{keyCode1, keyCode2},
			}

			hashedCode1, err := code.Raw(keyCode1).Hash()
			So(err, ShouldBeNil)

			hashedCode2, err := code.Raw(keyCode2).Hash()
			So(err, ShouldBeNil)

			hashedKeys := []code.RawHashed{hashedCode1, hashedCode2}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.InvalidateKeys(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("When we error looking up keys", func() {
					keyDAO.On("GetKeys", hashedKeys).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("We should return an error", func() {
						_, err := api.InvalidateKeys(context.Background(), req)
						test.AssertTwirpError(err, twirp.Internal)
					})
				})

				Convey("When dynamo returns the wrong number of keys", func() {
					dynamoKeys := []key.Key{
						{KeyCode: hashedCode1, KeyStatus: status.KeyStatusUnclaimed, BatchID: "test"},
					}
					keyDAO.On("GetKeys", hashedKeys).Return(dynamoKeys, nil)

					Convey("We should return an error", func() {
						_, err := api.InvalidateKeys(context.Background(), req)
						test.AssertTwirpError(err, twirp.InvalidArgument)
					})
				})

				Convey("When dynamo returns keys in different batches", func() {
					dynamoKeys := []key.Key{
						{KeyCode: hashedCode1, KeyStatus: status.KeyStatusUnclaimed, BatchID: "batch-1"},
						{KeyCode: hashedCode2, KeyStatus: status.KeyStatusUnclaimed, BatchID: "batch-2"},
					}
					keyDAO.On("GetKeys", hashedKeys).Return(dynamoKeys, nil)

					Convey("We should return an error", func() {
						_, err := api.InvalidateKeys(context.Background(), req)
						test.AssertTwirpError(err, twirp.InvalidArgument)
					})
				})

				Convey("When we find keys in Dynamo", func() {
					batchID := "test-batch"

					dynamoKeys := []key.Key{
						{KeyCode: hashedCode1, KeyStatus: status.KeyStatusUnclaimed, BatchID: batchID},
						{KeyCode: hashedCode2, KeyStatus: status.KeyStatusUnclaimed, BatchID: batchID},
					}

					keyDAO.On("GetKeys", hashedKeys).Return(dynamoKeys, nil)

					invalidatedKeys := []key.Key{
						{KeyCode: hashedCode1, KeyStatus: status.KeyStatusInvalidated, BatchID: batchID},
						{KeyCode: hashedCode2, KeyStatus: status.KeyStatusInvalidated, BatchID: batchID},
					}

					Convey("When KeySetChecker errors", func() {
						keySetChecker.On("DoesBatchIDUseRedisKeySet", batchID).Return(false, errors.New("test error"))

						Convey("We should return an error", func() {
							_, err := api.InvalidateKeys(context.Background(), req)
							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("When KeySetChecker says UsesRedisKeySet is false", func() {
						keySetChecker.On("DoesBatchIDUseRedisKeySet", batchID).Return(false, nil)

						Convey("When we error on updating the records", func() {
							keyDAO.On("UpdateBatch", invalidatedKeys).Return(errors.New("WALRUS STRIKE"))

							Convey("We should return an error", func() {
								_, err := api.InvalidateKeys(context.Background(), req)
								test.AssertTwirpError(err, twirp.Internal)
							})
						})

						Convey("When we succeed on updating the records", func() {
							keyDAO.On("UpdateBatch", invalidatedKeys).Return(nil)

							Convey("We should return success", func() {
								resp, err := api.InvalidateKeys(context.Background(), req)
								So(resp, ShouldNotBeNil)
								So(err, ShouldBeNil)
							})
						})
					})

					Convey("When KeySetChecker says UsesRedisKeySet is true", func() {
						keySetChecker.On("DoesBatchIDUseRedisKeySet", batchID).Return(true, nil)

						Convey("When RemoveFromKeySet errors", func() {
							keySetEditor.On("RemoveFromKeySet", batchID, hashedKeys).Return(errors.New("test error"))

							Convey("We should return an error", func() {
								_, err := api.InvalidateKeys(context.Background(), req)
								test.AssertTwirpError(err, twirp.Internal)
							})
						})

						Convey("When RemoveFromKeySet succeeds", func() {
							keySetEditor.On("RemoveFromKeySet", batchID, hashedKeys).Return(nil)

							Convey("When we error on updating the records", func() {
								keyDAO.On("UpdateBatch", invalidatedKeys).Return(errors.New("WALRUS STRIKE"))

								Convey("We should return an error", func() {
									_, err := api.InvalidateKeys(context.Background(), req)
									test.AssertTwirpError(err, twirp.Internal)
								})
							})

							Convey("When we succeed on updating the records", func() {
								keyDAO.On("UpdateBatch", invalidatedKeys).Return(nil)

								Convey("We should return success", func() {
									resp, err := api.InvalidateKeys(context.Background(), req)
									So(resp, ShouldNotBeNil)
									So(err, ShouldBeNil)
								})
							})
						})
					})

				})
			})
		})
	})
}
