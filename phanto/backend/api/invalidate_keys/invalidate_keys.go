package invalidate_keys

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyDAO        key.DAO         `inject:""`
	KeyBatchDAO   key_batch.DAO   `inject:""`
	KeyPoolDAO    key_pool.DAO    `inject:""`
	Auditor       audit.Auditor   `inject:""`
	KeySetChecker key_set.Checker `inject:""`
	KeySetEditor  key_set.Editor  `inject:""`
}

func (api *API) InvalidateKeys(ctx context.Context, req *phanto.InvalidateKeysReq) (resp *phanto.InvalidateKeysResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Keys", "Request body cannot be empty")
	} else if req.Keys == nil {
		return nil, twirp.InvalidArgumentError("Keys", "missing array of keys to invalidate")
	} else if len(req.Keys) < 1 {
		return nil, twirp.InvalidArgumentError("Keys", "must have at least 1 key to invalidate")
	}

	hashedKeys := make([]code.RawHashed, 0)
	for _, reqKey := range req.Keys {
		formattedKey := code.Formatted(reqKey)
		if !formattedKey.IsValid() {
			return nil, twirp.InvalidArgumentError("Keys", "contained an incorrectly formatted key")
		}

		hashedKey, err := formattedKey.Raw().Hash()
		if err != nil {
			log.WithError(err).Error("Error hashing key code")
			return nil, twirp.InternalError("Could not invalidate keys")
		}
		hashedKeys = append(hashedKeys, hashedKey)
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.InvalidateKeysOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoKeys, err := api.KeyDAO.GetKeys(hashedKeys)
	if err != nil {
		log.WithError(err).Error("downstream dependency error looking up keys")
		return nil, twirp.InternalError("downstream dependency error looking up keys")
	}

	if len(dynamoKeys) != len(req.Keys) {
		return nil, twirp.InvalidArgumentError("Keys", "some requested keys do not exist")
	}

	if !areBatchIDsUnique(dynamoKeys) {
		return nil, twirp.InvalidArgumentError("Keys", "keys must all be from the same batch")
	}

	batchID := dynamoKeys[0].BatchID

	usesKeySet, err := api.KeySetChecker.DoesBatchIDUseRedisKeySet(batchID)
	if err != nil {
		return nil, twirp.InternalError("downstream dependency error when invalidating keys")
	}

	if usesKeySet {
		err = api.KeySetEditor.RemoveFromKeySet(batchID, hashedKeys)
		if err != nil {
			return nil, twirp.InternalError("downstream dependency error when invalidating keys")
		}
	}

	invalidatedKeys := make([]key.Key, 0)
	for _, dynamoKey := range dynamoKeys {
		invalidatedKey := dynamoKey
		invalidatedKey.KeyStatus = status.KeyStatusInvalidated
		invalidatedKeys = append(invalidatedKeys, invalidatedKey)
	}

	err = api.KeyDAO.UpdateBatch(invalidatedKeys)
	if err != nil {
		log.WithError(err).Error("downstream dependency error when invalidating keys")
		return nil, twirp.InternalError("downstream dependency error when invalidating keys")
	}

	return &phanto.InvalidateKeysResp{}, nil
}

func areBatchIDsUnique(keys []key.Key) bool {
	batchID := keys[0].BatchID
	for _, k := range keys {
		if k.BatchID != batchID {
			return false
		}
	}
	return true
}
