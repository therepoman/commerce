package invalidate_key_batch

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_InvalidateKeys(t *testing.T) {
	Convey("Given an Invalidate Key Batch API", t, func() {
		dao := new(key_batch_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Given a nil request", func() {
			var req *phanto.InvalidateKeyBatchReq = nil

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeyBatch(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request an empty batch ID", func() {
			req := &phanto.InvalidateKeyBatchReq{
				BatchId: "",
			}

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeyBatch(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with valid batch ID", func() {
			batchID := "asdfasdfasdf"

			req := &phanto.InvalidateKeyBatchReq{
				BatchId: batchID,
			}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.InvalidateKeyBatch(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("When we error looking up the batch", func() {
					dao.On("GetKeyBatch", batchID).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("We should return an error", func() {
						_, err := api.InvalidateKeyBatch(context.Background(), req)

						test.AssertTwirpError(err, twirp.Internal)
					})
				})

				Convey("When we find batch in Dynamo", func() {
					poolID := "poolID"

					dynamoKeyBatch := &key_batch.KeyBatch{
						BatchID:                batchID,
						PoolID:                 poolID,
						KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
						NumberOfKeys:           1000,
					}

					dao.On("GetKeyBatch", batchID).Return(dynamoKeyBatch, nil)

					Convey("When we error on updating the batch", func() {
						dynamoKeyBatch.KeyBatchStatus = status.KeyBatchStatusInvalidated
						dao.On("CreateOrUpdate", dynamoKeyBatch).Return(errors.New("WALRUS STRIKE"))

						Convey("We should return an error", func() {
							_, err := api.InvalidateKeyBatch(context.Background(), req)

							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("When we succeed on updating the batch", func() {
						dynamoKeyBatch.KeyBatchStatus = status.KeyBatchStatusInvalidated
						dao.On("CreateOrUpdate", dynamoKeyBatch).Return(nil)

						Convey("We should return success", func() {
							resp, err := api.InvalidateKeyBatch(context.Background(), req)

							So(resp, ShouldNotBeNil)
							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
