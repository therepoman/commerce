package invalidate_key_batch

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_batch.DAO `inject:""`
	Auditor audit.Auditor `inject:""`
}

func (api *API) InvalidateKeyBatch(ctx context.Context, req *phanto.InvalidateKeyBatchReq) (resp *phanto.InvalidateKeyBatchResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("BatchID", "Request body cannot be empty")
	} else if strings.Blank(req.BatchId) {
		return nil, twirp.InvalidArgumentError("BatchID", "missing batch ID to invalidate")
	}

	req.BatchId = go_strings.TrimSpace(req.BatchId)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.InvalidateKeyBatchOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	batch, err := api.DAO.GetKeyBatch(req.BatchId)
	if err != nil {
		log.WithError(err).Error("downstream dependency error while getting batch")
		return nil, twirp.InternalError("downstream dependency error while getting batch")
	}

	batch.KeyBatchStatus = status.KeyBatchStatusInvalidated

	err = api.DAO.CreateOrUpdate(batch)
	if err != nil {
		log.WithError(err).Error("downstream dependency error while updating batch")
		return nil, twirp.InternalError("downstream dependency error while updating batch")
	}

	return &phanto.InvalidateKeyBatchResp{}, nil
}
