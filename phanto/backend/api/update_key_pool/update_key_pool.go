package update_key_pool

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_pool.DAO  `inject:""`
	Auditor audit.Auditor `inject:""`
}

func (api *API) UpdatePool(ctx context.Context, req *phanto.UpdateKeyPoolReq) (resp *phanto.UpdateKeyPoolResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolId", "request cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolId", "pool id cannot be blank")
	}

	req.PoolId = go_strings.TrimSpace(req.PoolId)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.UpdateKeyPoolOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	keyPool, err := api.DAO.GetKeyPool(req.PoolId)
	if err != nil {
		log.WithError(err).Error("error getting key pool from dynamo")
		return nil, twirp.InternalErrorWith(err)
	}

	if keyPool == nil {
		return nil, twirp.InvalidArgumentError("PoolId", "pool not found")
	}

	if strings.NotBlank(req.Status) {
		newStatus, err := status.FromString(req.Status)
		if err != nil {
			return nil, twirp.InvalidArgumentError("Status", "invalid key pool status")
		}
		keyPool.KeyPoolStatus = newStatus
	}

	startDate, err := ptypes.Timestamp(req.StartDate)
	if err == nil {
		keyPool.StartDate = startDate
	}

	endDate, err := ptypes.Timestamp(req.EndDate)
	if err == nil {
		keyPool.EndDate = endDate
	}

	err = api.DAO.CreateOrUpdate(keyPool)
	if err != nil {
		log.WithError(err).Error("could not update key pool to dynamo")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.UpdateKeyPoolResp{}, nil
}
