package update_key_pool_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/update_key_pool"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAPI_UpdatePool(t *testing.T) {
	Convey("Given an update key pool API", t, func() {
		dao := new(key_pool_mock.DAO)
		auditor := new(audit_mock.Auditor)

		api := update_key_pool.API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Errors on a nil request", func() {
			_, err := api.UpdatePool(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank pool ID", func() {
			_, err := api.UpdatePool(context.Background(), &phanto.UpdateKeyPoolReq{
				PoolId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid request", func() {

			poolID := "test-pool-id"
			req := &phanto.UpdateKeyPoolReq{
				PoolId: poolID,
			}

			Convey("Errors when auditing fails", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.UpdatePool(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("When auditing succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when dynamo get errors", func() {
					dao.On("GetKeyPool", poolID).Return(nil, errors.New("test error"))
					_, err := api.UpdatePool(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when dynamo returns nil", func() {
					dao.On("GetKeyPool", poolID).Return(nil, nil)
					_, err := api.UpdatePool(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("When dynamo get succeeds", func() {
					oldKeyPool := &key_pool.KeyPool{
						PoolID:        poolID,
						StartDate:     time.Now(),
						EndDate:       time.Now(),
						KeyPoolStatus: status.KeyPoolStatusActive,
					}
					dao.On("GetKeyPool", poolID).Return(oldKeyPool, nil)

					Convey("Errors when trying to update to an invalid status", func() {
						req.Status = "this is not a valid key pool status"
						_, err := api.UpdatePool(context.Background(), req)
						So(err, ShouldNotBeNil)
					})

					Convey("Errors when dynamo update errors", func() {
						dao.On("CreateOrUpdate", oldKeyPool).Return(errors.New("test error"))
						_, err := api.UpdatePool(context.Background(), req)
						So(err, ShouldNotBeNil)
					})

					Convey("When dynamo update succeeds", func() {
						dao.On("CreateOrUpdate", oldKeyPool).Return(nil)

						Convey("Succeeds when not changing anything", func() {
							_, err := api.UpdatePool(context.Background(), req)
							So(err, ShouldBeNil)
						})

						Convey("Succeeds when changing start date", func() {
							newStartDateTime := time.Now().Add(time.Hour)
							newStartDate, _ := ptypes.TimestampProto(newStartDateTime)

							req.StartDate = newStartDate
							_, err := api.UpdatePool(context.Background(), req)
							So(err, ShouldBeNil)

							newKeyPool, ok := dao.Calls[1].Arguments.Get(0).(*key_pool.KeyPool)
							So(ok, ShouldBeTrue)
							So(newKeyPool.StartDate, ShouldEqual, newStartDateTime)
						})

						Convey("Succeeds when changing end date", func() {
							newEndDateTime := time.Now().Add(time.Hour)
							newEndDate, _ := ptypes.TimestampProto(newEndDateTime)

							req.EndDate = newEndDate
							_, err := api.UpdatePool(context.Background(), req)
							So(err, ShouldBeNil)

							newKeyPool, ok := dao.Calls[1].Arguments.Get(0).(*key_pool.KeyPool)
							So(ok, ShouldBeTrue)
							So(newKeyPool.EndDate, ShouldEqual, newEndDateTime)
						})

						Convey("Succeeds when changing status", func() {
							newStatus := status.KeyPoolStatusInactive
							req.Status = string(newStatus)
							_, err := api.UpdatePool(context.Background(), req)
							So(err, ShouldBeNil)

							newKeyPool, ok := dao.Calls[1].Arguments.Get(0).(*key_pool.KeyPool)
							So(ok, ShouldBeTrue)
							So(newKeyPool.KeyPoolStatus, ShouldEqual, newStatus)
						})

						Convey("Succeeds when changing all fields", func() {
							newStartDateTime := time.Now().Add(time.Hour)
							newStartDate, _ := ptypes.TimestampProto(newStartDateTime)
							req.StartDate = newStartDate

							newEndDateTime := time.Now().Add(time.Hour)
							newEndDate, _ := ptypes.TimestampProto(newEndDateTime)
							req.EndDate = newEndDate

							newStatus := status.KeyPoolStatusInactive
							req.Status = string(newStatus)

							_, err := api.UpdatePool(context.Background(), req)
							So(err, ShouldBeNil)

							newKeyPool, ok := dao.Calls[1].Arguments.Get(0).(*key_pool.KeyPool)
							So(ok, ShouldBeTrue)

							So(newKeyPool.StartDate, ShouldEqual, newStartDateTime)
							So(newKeyPool.EndDate, ShouldEqual, newEndDateTime)
							So(newKeyPool.KeyPoolStatus, ShouldEqual, newStatus)
						})
					})
				})
			})
		})
	})
}
