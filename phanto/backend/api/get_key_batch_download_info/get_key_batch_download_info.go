package get_key_batch_download_info

import (
	"context"
	"encoding/base64"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	dynamo_key_batch "code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/authorization"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	Config            *config.Config                  `inject:""`
	KeyBatchDAO       dynamo_key_batch.DAO            `inject:""`
	KMSClient         kms.Client                      `inject:""`
	S3Client          s3.Client                       `inject:""`
	HandlerAuthorizer authorization.HandlerAuthorizer `inject:""`
	Auditor           audit.Auditor                   `inject:""`
}

const ExpiryTime = 5 * time.Minute

func (api *API) GetKeyBatchDownloadInfo(ctx context.Context, req *phanto.GetKeyBatchDownloadInfoReq) (resp *phanto.GetKeyBatchDownloadInfoResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("BatchID", "Request body cannot be empty")
	} else if strings.Blank(req.BatchId) {
		return nil, twirp.InvalidArgumentError("BatchID", "BatchID cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyBatchDownloadInfoOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	keyBatch, err := api.KeyBatchDAO.GetKeyBatch(req.BatchId)
	if err != nil {
		log.WithError(err).Error("Error getting key batch from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key batch download info")
	}

	if keyBatch == nil {
		return nil, twirp.InvalidArgumentError("BatchID", "Batch does not exist")
	}

	if !api.HandlerAuthorizer.AuthorizeByTUID(ctx, keyBatch) {
		return nil, twirp.NewError(twirp.PermissionDenied, "User is not authorized to get key batch download info")
	}

	decryptOut, err := api.KMSClient.Decrypt(keyBatch.Cipher, map[string]string{
		"batch_id": keyBatch.BatchID,
	})
	if err != nil {
		log.WithError(err).Error("Error decrypting cipher from keyBatch")
		return nil, twirp.InternalError("Downstream dependency error getting key batch download info")
	}
	if decryptOut == nil {
		log.WithError(err).Error("KMS returned nil response decrypting keyBatch cipher")
		return nil, twirp.InternalError("Downstream dependency error getting key batch download info")
	}

	url, err := api.S3Client.GetTempSignedURL(ctx, api.Config.KeyBatchS3Bucket, key_batch.GetEncryptedS3Key(keyBatch.PoolID, keyBatch.BatchID), ExpiryTime)
	if err != nil {
		log.WithError(err).Error("Error getting temp url from S3")
		return nil, twirp.InternalError("Downstream dependency error getting key batch download info")
	}

	return &phanto.GetKeyBatchDownloadInfoResp{
		DecryptionKey: base64.StdEncoding.EncodeToString(decryptOut.Plaintext),
		DownloadUrl:   url,
	}, nil
}
