package get_key_batch_download_info_test

import (
	"context"
	"encoding/base64"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_batch_download_info"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	dynamo_key_batch "code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/authorization"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	aws_kms "github.com/aws/aws-sdk-go/service/kms"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyBatchDownloadInfo(t *testing.T) {
	Convey("Given an API struct", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		s3Client := new(s3_mock.Client)
		keyBatchDAO := new(key_batch_mock.DAO)
		kmsClient := new(kms_mock.Client)
		handlerAuthorizer := new(authorization_mock.HandlerAuthorizer)
		auditor := new(audit_mock.Auditor)

		api := &get_key_batch_download_info.API{
			KeyBatchDAO:       keyBatchDAO,
			Config:            cfg,
			KMSClient:         kmsClient,
			S3Client:          s3Client,
			HandlerAuthorizer: handlerAuthorizer,
			Auditor:           auditor,
		}

		batchID := "test-batch-id"
		poolID := "test-pool-id"
		cipher := []byte("test-cipher-12345")
		plaintext := []byte("test-plaintext-12345")
		s3URL := "http://test-url.s3.com?signature=testsignature"
		Convey("Errors when request is nil", func() {
			_, err := api.GetKeyBatchDownloadInfo(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		ctx := context.Background()

		Convey("Errors when request batch id is blank", func() {
			_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
				BatchId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
				BatchId: batchID,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when key batch dao errors", func() {
				keyBatchDAO.On("GetKeyBatch", batchID).Return(nil, errors.New("test error"))
				_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
					BatchId: batchID,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when key batch dao returns nil batch", func() {
				keyBatchDAO.On("GetKeyBatch", batchID).Return(nil, nil)
				_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
					BatchId: batchID,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("When key batch dao returns a batch", func() {
				keyBatch := &dynamo_key_batch.KeyBatch{
					BatchID: batchID,
					PoolID:  poolID,
					Cipher:  cipher,
				}

				keyBatchDAO.On("GetKeyBatch", batchID).Return(keyBatch, nil)

				Convey("When the user is not authorized to download the key batch", func() {
					handlerAuthorizer.On("AuthorizeByTUID", ctx, keyBatch).Return(false)

					Convey("we should return permission denied", func() {
						_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
							BatchId: batchID,
						})
						test.AssertTwirpError(err, twirp.PermissionDenied)
					})
				})

				Convey("When the user is authorized to download the key batch", func() {
					handlerAuthorizer.On("AuthorizeByTUID", ctx, keyBatch).Return(true)

					Convey("Errors when kms decrypt errors", func() {
						kmsClient.On("Decrypt", cipher, map[string]string{
							"batch_id": batchID,
						}).Return(nil, errors.New("test error"))
						_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
							BatchId: batchID,
						})
						So(err, ShouldNotBeNil)
					})

					Convey("Errors when kms decrypt returns nil resp", func() {
						kmsClient.On("Decrypt", cipher, map[string]string{
							"batch_id": batchID,
						}).Return(nil, nil)
						_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
							BatchId: batchID,
						})
						So(err, ShouldNotBeNil)
					})

					Convey("When kms decrypt returns resp", func() {
						kmsClient.On("Decrypt", cipher, map[string]string{
							"batch_id": batchID,
						}).Return(&aws_kms.DecryptOutput{
							Plaintext: plaintext,
						}, nil)

						Convey("Errors when S3 url gen errors", func() {
							s3Client.On("GetTempSignedURL", mock.Anything, cfg.KeyBatchS3Bucket, key_batch.GetEncryptedS3Key(poolID, batchID), get_key_batch_download_info.ExpiryTime).Return("", errors.New("test error"))
							_, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
								BatchId: batchID,
							})
							So(err, ShouldNotBeNil)
						})

						Convey("Succeeds when S3 url gen returns url", func() {
							s3Client.On("GetTempSignedURL", mock.Anything, cfg.KeyBatchS3Bucket, key_batch.GetEncryptedS3Key(poolID, batchID), get_key_batch_download_info.ExpiryTime).Return(s3URL, nil)
							resp, err := api.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
								BatchId: batchID,
							})
							So(err, ShouldBeNil)
							So(resp.DownloadUrl, ShouldEqual, s3URL)
							So(resp.DecryptionKey, ShouldEqual, base64.StdEncoding.EncodeToString(plaintext))
						})
					})
				})
			})
		})
	})
}
