package redeem_key_base_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	base_restriction "code.justin.tv/commerce/phanto/backend/api/base/restriction"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_base"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_RedeemKey(t *testing.T) {
	Convey("Given a Redeem Key API handler", t, func() {
		validator := new(key_mock.Validator)
		claimer := new(key_mock.Claimer)
		authorizer := new(authorization_mock.Authorizer)
		auditor := new(audit_mock.Auditor)
		throttler := new(throttle_mock.IThrottler)

		api := &redeem_key_base.APIImpl{
			BaseContainer: &base.Container{
				Authorizer: authorizer,
				Throttler:  throttler,
			},
			Validator: validator,
			Claimer:   claimer,
			Auditor:   auditor,
		}

		keyCode := "ABCDE12345FGHIJ"
		keyHash, err := code.Raw(keyCode).Hash()
		So(err, ShouldBeNil)
		userID := "123123123"
		clientIP := "127.0.0.1"

		opt := redeem_key_base.RedeemKeyOptions{
			AuthorizationFunc: base_authorization.NoAuthorization,
			ThrottleFunc:      base_throttle.NoThrottle,
			RestrictionFunc:   base_restriction.NoRestriction,
			PerformAuditing:   false,
			RedeemType:        base.RedeemType_StandardByKey,
		}

		Convey("Given a nil request", func() {
			var req *phanto.RedeemKeyReq = nil
			Convey("We should return an invalid argument error", func() {
				_, _, err := api.RedeemKeyBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request without a key code", func() {
			req := &phanto.RedeemKeyReq{}
			Convey("We should return an invalid argument error", func() {
				_, _, err := api.RedeemKeyBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request without a userID", func() {
			req := &phanto.RedeemKeyReq{
				Key:      keyCode,
				ClientIp: clientIP,
			}
			Convey("We should return an invalid argument error", func() {
				_, _, err := api.RedeemKeyBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request without a clientIP", func() {
			req := &phanto.RedeemKeyReq{
				Key:    keyCode,
				UserId: userID,
			}
			Convey("We should return an invalid argument error", func() {
				_, _, err := api.RedeemKeyBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an invalid key", func() {
			req := &phanto.RedeemKeyReq{
				Key:      "NOT-A-VALID-KEY",
				UserId:   userID,
				ClientIp: clientIP,
			}

			Convey("We should return an invalid argument error", func() {
				_, _, err := api.RedeemKeyBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an invalid redeem type", func() {
			req := &phanto.RedeemKeyReq{
				Key:      keyCode,
				UserId:   userID,
				ClientIp: clientIP,
			}

			opt.RedeemType = base.RedeemType("not_valid_redeem_type")
			Convey("We should return an invalid argument error", func() {
				_, _, err := api.RedeemKeyBase(context.Background(), req, opt)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with a key code", func() {
			req := &phanto.RedeemKeyReq{
				Key:      keyCode,
				UserId:   userID,
				ClientIp: clientIP,
			}

			ctx := context.Background()

			keyClaimData := data.Validation{
				IsValid:     true,
				SNSTopic:    "walrusTopic",
				ProductType: "productType",
				KeyPoolID:   "keyPoolID",
				KeyBatchID:  "keyBatchID",
				KeyCode:     keyHash,
				SKU:         "sku",
			}

			Convey("When we get an unknown error on validating the key code", func() {
				keyClaimData.IsValid = false
				validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, errors.New("WALRUS"))
				Convey("We should return the error from the validator", func() {
					_, _, err := api.RedeemKeyBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.Internal)
				})
			})

			Convey("When we get a not found error on validating the key code", func() {
				keyClaimData.IsValid = false
				validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, key.NotFoundError)
				Convey("We should return the error from the validator", func() {
					_, _, err := api.RedeemKeyBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.NotFound)
				})
			})

			Convey("Given a key code that is invalid", func() {
				keyClaimData.IsValid = false
				validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, nil)
				Convey("We should return a failed precondition error", func() {
					_, _, err := api.RedeemKeyBase(ctx, req, opt)
					test.AssertTwirpError(err, twirp.FailedPrecondition)
				})
			})

			Convey("Given a key code that is valid", func() {
				validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, nil)

				Convey("When an error occurs on claiming the key code", func() {
					claimer.On("Claim", ctx, userID, clientIP, keyClaimData, opt.RedeemType).Return(errors.New("WALRUS"))
					Convey("We should return the error from the claimer", func() {
						_, _, err := api.RedeemKeyBase(ctx, req, opt)
						test.AssertTwirpError(err, twirp.Unavailable)
					})
				})

				Convey("when claiming the key code is successful", func() {
					claimer.On("Claim", ctx, userID, clientIP, keyClaimData, opt.RedeemType).Return(nil)

					Convey("We should return a successful response", func() {
						resp, _, err := api.RedeemKeyBase(ctx, req, opt)

						So(resp, ShouldNotBeNil)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
