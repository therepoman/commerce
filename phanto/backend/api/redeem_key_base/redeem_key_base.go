package redeem_key_base

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	base_restriction "code.justin.tv/commerce/phanto/backend/api/base/restriction"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type APIImpl struct {
	BaseContainer *base.Container `inject:""`
	Validator     key.Validator   `inject:"keyValidator"`
	Claimer       key.Claimer     `inject:""`
	Auditor       audit.Auditor   `inject:""`
}

type RedeemKeyOptions struct {
	AuthorizationFunc base_authorization.Func
	ThrottleFunc      base_throttle.Func
	RestrictionFunc   base_restriction.Func
	PerformAuditing   bool
	AuditOperation    audit.Operation
	RedeemType        base.RedeemType
}

type API interface {
	RedeemKeyBase(ctx context.Context, req *phanto.RedeemKeyReq, opt RedeemKeyOptions) (keyValidationData *data.Validation, auditRecord *call_audit_record.CallAuditRecord, err error)
}

func (api *APIImpl) RedeemKeyBase(ctx context.Context, req *phanto.RedeemKeyReq, opt RedeemKeyOptions) (keyValidationData *data.Validation, auditRecord *call_audit_record.CallAuditRecord, err error) {
	if req == nil {
		return nil, nil, twirp.InvalidArgumentError("Key", "Request body cannot be empty")
	} else if strings.Blank(req.Key) {
		return nil, nil, twirp.InvalidArgumentError("Key", "Key cannot be blank")
	} else if strings.Blank(req.UserId) {
		return nil, nil, twirp.InvalidArgumentError("UserId", "UserId cannot be blank")
	} else if strings.Blank(req.ClientIp) {
		return nil, nil, twirp.InvalidArgumentError("ClientIp", "ClientIp cannot be blank")
	} else if !base.IsValidRedeemType(opt.RedeemType) {
		return nil, nil, twirp.InvalidArgumentError("RedeemType", "Invalid redemption type")
	}

	formattedCode := code.Formatted(req.Key)
	if !formattedCode.IsValid() {
		return nil, nil, twirp.InvalidArgumentError("Key", "Key not in correct format")
	}

	err = opt.AuthorizationFunc(ctx, *api.BaseContainer, req.UserId)
	if err != nil {
		return nil, nil, err
	}

	err = opt.ThrottleFunc(ctx, *api.BaseContainer, req.UserId)
	if err != nil {
		return nil, nil, err
	}

	if opt.PerformAuditing {
		auditRecord, err = api.Auditor.AuditRequest(ctx, &audit.Request{Operation: opt.AuditOperation, Body: req})
		if err != nil {
			return nil, nil, twirp.InternalErrorWith(err)
		}
	}

	keyCode, err := formattedCode.Raw().Hash()
	if err != nil {
		log.WithError(err).Error("Error hashing key code")
		return nil, auditRecord, twirp.InternalError("Could not claim code")
	}

	claimData, err := api.Validator.ValidateForUser(ctx, keyCode, req.UserId)
	if err == key.NotFoundError {
		return nil, auditRecord, twirp.NotFoundError(err.Error())
	} else if err != nil {
		log.WithError(err).Error("Could not fetch key from dynamo")
		return nil, auditRecord, twirp.InternalErrorWith(err)
	}

	if !claimData.IsValid {
		return &claimData, auditRecord, twirp.NewError(twirp.FailedPrecondition, "Key is not valid for claiming")
	}

	err = opt.RestrictionFunc(ctx, *api.BaseContainer, claimData)
	if err != nil {
		return nil, auditRecord, err
	}

	err = api.Claimer.Claim(ctx, req.UserId, req.ClientIp, claimData, opt.RedeemType)
	if err != nil {
		log.WithError(err).Error("Could not claim key code")
		return nil, auditRecord, twirp.NewError(twirp.Unavailable, err.Error())
	}

	return &claimData, auditRecord, nil
}
