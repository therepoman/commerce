package get_all_product_types

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	ProductTypeDAO product_type.DAO `inject:""`
	Auditor        audit.Auditor    `inject:""`
}

func (api *API) GetAllProductTypes(ctx context.Context, req *phanto.GetAllProductTypesReq) (resp *phanto.GetAllProductTypesResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Cursor", "Request body cannot be empty")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetAllProductTypesOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	productTypes, cursor, err := api.ProductTypeDAO.GetProductTypes(req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting product types from dynamo")
		return nil, twirp.InternalError("Error getting product types")
	}

	return &phanto.GetAllProductTypesResp{
		ProductTypes: product_type.ToTwirpModel(productTypes),
		Cursor:       cursor,
	}, nil
}
