package get_all_product_types_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_all_product_types"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetAllProductTypes(t *testing.T) {
	Convey("Given a Get All Product Types API", t, func() {
		productTypeDAO := new(product_type_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := get_all_product_types.API{
			ProductTypeDAO: productTypeDAO,
			Auditor:        auditor,
		}

		cursor := "test-cursor"

		Convey("Errors when req is nil", func() {
			_, err := api.GetAllProductTypes(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetAllProductTypes(context.Background(), &phanto.GetAllProductTypesReq{})
			test.AssertTwirpError(err, twirp.Internal)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when DAO errors", func() {
				productTypeDAO.On("GetProductTypes", cursor).Return(nil, "", errors.New("test error"))
				_, err := api.GetAllProductTypes(context.Background(), &phanto.GetAllProductTypesReq{
					Cursor: cursor,
				})
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("Succeeds when DAO succeeds", func() {
				newCursor := "new-test-cursor"
				productTypeDAO.On("GetProductTypes", cursor).Return([]*product_type.ProductType{
					{ProductType: "pt1"},
					{ProductType: "pt2"},
				}, newCursor, nil)
				resp, err := api.GetAllProductTypes(context.Background(), &phanto.GetAllProductTypesReq{
					Cursor: cursor,
				})
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Cursor, ShouldEqual, newCursor)
				So(resp.ProductTypes[0].ProductType, ShouldEqual, "pt1")
				So(resp.ProductTypes[1].ProductType, ShouldEqual, "pt2")
			})
		})
	})
}
