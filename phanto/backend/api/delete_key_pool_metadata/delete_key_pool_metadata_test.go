package delete_key_pool_metadata_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/delete_key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_DeleteKeyPoolMetadata(t *testing.T) {
	Convey("Given an API", t, func() {
		keyPoolMetadataDAO := new(key_pool_metadata_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &delete_key_pool_metadata.API{
			KeyPoolMetadataDAO: keyPoolMetadataDAO,
			Auditor:            auditor,
		}

		Convey("Errors when request is nil", func() {
			_, err := api.DeleteKeyPoolMetadata(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when pool id is blank", func() {
			_, err := api.DeleteKeyPoolMetadata(context.Background(), &phanto.DeleteKeyPoolMetadataReq{
				PoolId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when metadata key is blank", func() {
			_, err := api.DeleteKeyPoolMetadata(context.Background(), &phanto.DeleteKeyPoolMetadataReq{
				PoolId:      "test-pool-id",
				MetadataKey: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.DeleteKeyPoolMetadataReq{
				PoolId:      "test-pool-id",
				MetadataKey: "test-metadata",
			}

			Convey("Errors when auditor errors", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.DeleteKeyPoolMetadata(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when DAO errors", func() {
					keyPoolMetadataDAO.On("DeleteKeyPoolMetadata", req.PoolId, req.MetadataKey).Return(errors.New("test-error"))
					_, err := api.DeleteKeyPoolMetadata(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Succeeds when DAO succeeds", func() {
					keyPoolMetadataDAO.On("DeleteKeyPoolMetadata", req.PoolId, req.MetadataKey).Return(nil)
					_, err := api.DeleteKeyPoolMetadata(context.Background(), req)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
