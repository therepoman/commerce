package delete_key_pool_metadata

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolMetadataDAO key_pool_metadata.DAO `inject:""`
	Auditor            audit.Auditor         `inject:""`
}

func (api *API) DeleteKeyPoolMetadata(ctx context.Context, req *phanto.DeleteKeyPoolMetadataReq) (resp *phanto.DeleteKeyPoolMetadataResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolId", "request cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolId", "pool id cannot be blank")
	} else if strings.Blank(req.MetadataKey) {
		return nil, twirp.InvalidArgumentError("MetadataKey", "metadata key cannot be blank")
	}

	req.PoolId = go_strings.TrimSpace(req.PoolId)
	req.MetadataKey = go_strings.TrimSpace(req.MetadataKey)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.DeleteKeyPoolMetadataOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	err = api.KeyPoolMetadataDAO.DeleteKeyPoolMetadata(req.PoolId, req.MetadataKey)
	if err != nil {
		log.WithError(err).Error("error deleting key pool metadata")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.DeleteKeyPoolMetadataResp{}, nil
}
