package redeem_key_for_load_testing

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	base_restriction "code.justin.tv/commerce/phanto/backend/api/base/restriction"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_base"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/rpc"
)

type API struct {
	BaseAPI redeem_key_base.API `inject:""`
	Auditor audit.Auditor       `inject:""`
}

func (api *API) RedeemKeyForLoadTesting(ctx context.Context, req *phanto.RedeemKeyReq) (resp *phanto.RedeemKeyResp, err error) {
	_, auditRecord, err := api.BaseAPI.RedeemKeyBase(ctx, req, redeem_key_base.RedeemKeyOptions{
		AuthorizationFunc: base_authorization.NoAuthorization,
		ThrottleFunc:      base_throttle.NoThrottle,
		RestrictionFunc:   base_restriction.RestrictByProductType("load-test"),
		PerformAuditing:   true,
		AuditOperation:    audit.RedeemKeyForLoadTestingOperation,
		RedeemType:        base.RedeemType_LoadTestByKey,
	})

	defer func() {
		if auditRecord != nil {
			api.Auditor.AuditResponse(ctx, auditRecord, &audit.Response{Body: resp, Error: err})
		}
	}()

	if err != nil {
		return nil, err
	}

	return &phanto.RedeemKeyResp{}, nil
}
