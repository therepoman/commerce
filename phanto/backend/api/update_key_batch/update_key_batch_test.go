package update_key_batch_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/update_key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAPI_UpdateBatch(t *testing.T) {
	Convey("Given an update key batch API", t, func() {
		dao := new(key_batch_mock.DAO)
		auditor := new(audit_mock.Auditor)

		api := update_key_batch.API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Errors on a nil request", func() {
			_, err := api.UpdateKeyBatch(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank batch ID", func() {
			_, err := api.UpdateKeyBatch(context.Background(), &phanto.UpdateKeyBatchReq{
				BatchId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank status", func() {
			_, err := api.UpdateKeyBatch(context.Background(), &phanto.UpdateKeyBatchReq{
				BatchId: "test-batch-id",
				Status:  "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on an invalid status", func() {
			_, err := api.UpdateKeyBatch(context.Background(), &phanto.UpdateKeyBatchReq{
				BatchId: "test-batch-id",
				Status:  "NOT_VALID_STATUS",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid request", func() {

			batchID := "test-batch-id"
			req := &phanto.UpdateKeyBatchReq{
				BatchId: batchID,
				Status:  "ACTIVE",
			}

			Convey("Errors when auditing fails", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.UpdateKeyBatch(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("When auditing succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when dynamo get errors", func() {
					dao.On("GetKeyBatch", batchID).Return(nil, errors.New("test error"))
					_, err := api.UpdateKeyBatch(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when dynamo returns nil", func() {
					dao.On("GetKeyBatch", batchID).Return(nil, nil)
					_, err := api.UpdateKeyBatch(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("When dynamo get succeeds", func() {
					oldKeyBatch := &key_batch.KeyBatch{
						PoolID:         "test-pool-id",
						BatchID:        batchID,
						KeyBatchStatus: status.KeyBatchStatusActive,
					}
					dao.On("GetKeyBatch", batchID).Return(oldKeyBatch, nil)

					Convey("Errors when dynamo update errors", func() {
						dao.On("CreateOrUpdate", oldKeyBatch).Return(errors.New("test error"))
						_, err := api.UpdateKeyBatch(context.Background(), req)
						So(err, ShouldNotBeNil)
					})

					Convey("Succeeds when dynamo update succeeds", func() {
						dao.On("CreateOrUpdate", oldKeyBatch).Return(nil)
						_, err := api.UpdateKeyBatch(context.Background(), req)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
