package update_key_batch

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_batch.DAO `inject:""`
	Auditor audit.Auditor `inject:""`
}

func (api *API) UpdateKeyBatch(ctx context.Context, req *phanto.UpdateKeyBatchReq) (resp *phanto.UpdateKeyBatchResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("BatchId", "request cannot be nil")
	} else if strings.Blank(req.BatchId) {
		return nil, twirp.InvalidArgumentError("BatchId", "batch id cannot be blank")
	} else if strings.Blank(req.Status) {
		return nil, twirp.InvalidArgumentError("Status", "status cannot be blank")
	}

	req.BatchId = go_strings.TrimSpace(req.BatchId)

	_, validStatus := status.Statuses[status.KeyBatchStatus(req.Status)]
	if !validStatus {
		return nil, twirp.InvalidArgumentError("Status", "invalid status")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.UpdateKeyBatchOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	keyBatch, err := api.DAO.GetKeyBatch(req.BatchId)
	if err != nil {
		log.WithError(err).Error("error getting key batch from dynamo")
		return nil, twirp.InternalErrorWith(err)
	}

	if keyBatch == nil {
		return nil, twirp.InvalidArgumentError("BatchId", "batch not found")
	}

	keyBatch.KeyBatchStatus = status.KeyBatchStatus(req.Status)
	err = api.DAO.CreateOrUpdate(keyBatch)
	if err != nil {
		log.WithError(err).Error("could not update key batch to dynamo")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.UpdateKeyBatchResp{}, nil
}
