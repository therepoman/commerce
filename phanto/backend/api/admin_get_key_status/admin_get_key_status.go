package admin_get_key_status

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"github.com/twitchtv/twirp"
)

type API struct {
	Validator key.Validator `inject:"keyValidator"`
}

func (api *API) AdminGetKeyStatus(ctx context.Context, req *phanto.AdminGetKeyStatusReq) (resp *phanto.AdminGetKeyStatusResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Key", "Request body cannot be empty")
	} else if strings.Blank(req.Key) {
		return nil, twirp.InvalidArgumentError("Key", "Key cannot be blank")
	}

	formattedCode := code.Formatted(req.Key)
	if !formattedCode.IsValid() {
		return nil, twirp.InvalidArgumentError("Key", "Key not in correct format")
	}

	keyHashed, err := formattedCode.Raw().Hash()
	if err != nil {
		return nil, twirp.InternalError("Error getting key status")
	}

	data, err := api.Validator.Validate(ctx, keyHashed)
	if err != nil {
		return nil, err
	}

	claimID, err := key.ToClaimID(keyHashed)
	if err != nil {
		return nil, err
	}

	return &phanto.AdminGetKeyStatusResp{
		ClaimedBy: data.ClaimerUserID,
		ClaimId:   claimID,
	}, err
}
