package admin_get_key_status_test

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/admin_get_key_status"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/key/code"
	key_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_CreateKeyPool(t *testing.T) {
	Convey("Given a create key pool API", t, func() {
		ctx := context.Background()
		keyValidator := new(key_mock.Validator)
		keyCode := "asdfa-sdfas-dfasd"

		api := admin_get_key_status.API{
			Validator: keyValidator,
		}

		Convey("Given a nil request", func() {
			var req *phanto.AdminGetKeyStatusReq = nil

			Convey("Then we should return an error", func() {
				_, err := api.AdminGetKeyStatus(ctx, req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank key", func() {
			req := &phanto.AdminGetKeyStatusReq{}

			Convey("Then we should return an error", func() {
				_, err := api.AdminGetKeyStatus(ctx, req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given an invalid formatted key", func() {
			req := &phanto.AdminGetKeyStatusReq{
				Key: " asx ",
			}

			Convey("Then we should return an error", func() {
				_, err := api.AdminGetKeyStatus(ctx, req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with a key", func() {
			req := &phanto.AdminGetKeyStatusReq{
				Key: keyCode,
			}

			hashCode, err := code.Formatted(keyCode).Raw().Hash()
			So(err, ShouldBeNil)

			failedKeyClaimData := data.Validation{
				IsValid: false,
			}

			Convey("If Key fails to validate, it should fail", func() {
				keyValidator.On("Validate", ctx, hashCode).Return(failedKeyClaimData, fmt.Errorf("failed to validate"))
				_, err := api.AdminGetKeyStatus(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("When key succeeds in validation", func() {
				keyClaimData := data.Validation{
					IsValid: true,
				}

				keyValidator.On("Validate", ctx, hashCode).Return(keyClaimData, nil)
				_, err := api.AdminGetKeyStatus(ctx, req)
				So(err, ShouldBeNil)
			})
		})
	})
}
