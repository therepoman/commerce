package redeem_key_test

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_base"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_RedeemKey(t *testing.T) {
	Convey("Given a Redeem Key API handler", t, func() {
		validator := new(key_mock.Validator)
		claimer := new(key_mock.Claimer)
		authorizer := new(authorization_mock.Authorizer)
		auditor := new(audit_mock.Auditor)
		throttler := new(throttle_mock.IThrottler)

		baseAPI := &redeem_key_base.APIImpl{
			BaseContainer: &base.Container{
				Authorizer: authorizer,
				Throttler:  throttler,
			},
			Validator: validator,
			Claimer:   claimer,
			Auditor:   auditor,
		}

		api := redeem_key.API{
			BaseAPI: baseAPI,
			Auditor: auditor,
		}

		keyCode := "ABCDE12345FGHIJ"
		keyHash, err := code.Raw(keyCode).Hash()
		So(err, ShouldBeNil)
		userID := "123123123"
		clientIP := "127.0.0.1"

		Convey("Given a nil request", func() {
			var req *phanto.RedeemKeyReq = nil

			Convey("We should return an invalid argument error", func() {
				_, err := api.RedeemKey(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request without a key code", func() {
			req := &phanto.RedeemKeyReq{}

			Convey("We should return an invalid argument error", func() {
				_, err := api.RedeemKey(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request without a userID", func() {
			req := &phanto.RedeemKeyReq{
				Key:      keyCode,
				ClientIp: clientIP,
			}

			Convey("We should return an invalid argument error", func() {
				_, err := api.RedeemKey(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request without a clientIP", func() {
			req := &phanto.RedeemKeyReq{
				Key:    keyCode,
				UserId: userID,
			}

			Convey("We should return an invalid argument error", func() {
				_, err := api.RedeemKey(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an invalid key", func() {
			req := &phanto.RedeemKeyReq{
				Key:      "NOT-A-VALID-KEY",
				UserId:   userID,
				ClientIp: clientIP,
			}

			Convey("We should return an invalid argument error", func() {
				_, err := api.RedeemKey(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with a key code", func() {
			req := &phanto.RedeemKeyReq{
				Key:      keyCode,
				UserId:   userID,
				ClientIp: clientIP,
			}

			ctx := context.Background()

			keyClaimData := data.Validation{
				IsValid:     true,
				SNSTopic:    "walrusTopic",
				ProductType: "productType",
				KeyPoolID:   "keyPoolID",
				KeyBatchID:  "keyBatchID",
				KeyCode:     keyHash,
				SKU:         "sku",
			}

			Convey("When the user is not authorized to claim the key code", func() {
				authorizer.On("AuthenticatedUserMatches", ctx, userID).Return(false)

				Convey("We should return an invalid argument error", func() {
					_, err := api.RedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.PermissionDenied)
				})
			})

			Convey("When the user is authorized to claim the key code", func() {
				authorizer.On("AuthenticatedUserMatches", ctx, userID).Return(true)

				Convey("When throttler experiences an error getting value from given key", func() {
					throttler.On("ThrottleExponentially", fmt.Sprintf("RedeemKey.%s", userID)).Return(throttle.ThrottleResponse{IsThrottled: true}, errors.New("test redis get error"))
					_, err := api.RedeemKey(ctx, req)

					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Errors when the request gets throttled", func() {
					throttler.On("ThrottleExponentially", fmt.Sprintf("RedeemKey.%s", userID)).Return(throttle.ThrottleResponse{IsThrottled: true}, nil)
					_, err := api.RedeemKey(ctx, req)

					test.AssertTwirpError(err, twirp.ResourceExhausted)
				})

				Convey("When the request doesn't get throttled", func() {
					throttler.On("ThrottleExponentially", fmt.Sprintf("RedeemKey.%s", userID)).Return(throttle.ThrottleResponse{IsThrottled: false}, nil)

					Convey("Errors when the auditor returns an error", func() {
						auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
						_, err := api.RedeemKey(ctx, req)
						test.AssertTwirpError(err, twirp.Internal)
					})

					Convey("When the auditor succeeds", func() {
						auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
						auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

						Convey("When we get an unknown error on validating the key code", func() {
							keyClaimData.IsValid = false
							validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, errors.New("WALRUS"))
							Convey("We should return the error from the validator", func() {
								_, err := api.RedeemKey(ctx, req)
								test.AssertTwirpError(err, twirp.Internal)
							})
						})

						Convey("When we get a not found error on validating the key code", func() {
							keyClaimData.IsValid = false
							validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, key.NotFoundError)
							Convey("We should return the error from the validator", func() {
								_, err := api.RedeemKey(ctx, req)
								test.AssertTwirpError(err, twirp.NotFound)
							})
						})

						Convey("Given a key code that is invalid", func() {
							keyClaimData.IsValid = false
							validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, nil)
							Convey("We should return a failed precondition error", func() {
								_, err := api.RedeemKey(ctx, req)
								test.AssertTwirpError(err, twirp.FailedPrecondition)
							})
						})

						Convey("Given a key code that is valid", func() {
							validator.On("ValidateForUser", mock.Anything, keyHash, mock.Anything).Return(keyClaimData, nil)

							Convey("When an error occurs on claiming the key code", func() {
								claimer.On("Claim", ctx, userID, clientIP, keyClaimData, base.RedeemType_StandardByKey).Return(errors.New("WALRUS"))

								Convey("We should return the error from the claimer", func() {
									_, err := api.RedeemKey(ctx, req)

									test.AssertTwirpError(err, twirp.Unavailable)
								})
							})

							Convey("when claiming the key code is successful", func() {
								claimer.On("Claim", ctx, userID, clientIP, keyClaimData, base.RedeemType_StandardByKey).Return(nil)

								Convey("We should return a successful response", func() {
									resp, err := api.RedeemKey(ctx, req)

									So(resp, ShouldNotBeNil)
									So(err, ShouldBeNil)
								})
							})
						})
					})
				})
			})
		})
	})
}
