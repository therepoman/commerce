package helix_redeem_key_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/crypto/random"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	"code.justin.tv/commerce/phanto/backend/api/helix_redeem_key"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_base"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"code.justin.tv/web/users-service/models"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func expectResultContains(resp *phanto.HelixRedeemKeyResp, key string, expectedStatus phanto.KeyStatus) {
	So(resp, ShouldNotBeNil)
	if resp != nil {
		var helixKeyResp *phanto.HelixKeyResp

		for _, result := range resp.Results {
			if result != nil && result.Key == key {
				helixKeyResp = result
				break
			}
		}

		So(helixKeyResp, ShouldNotBeNil)
		if helixKeyResp != nil {
			So(helixKeyResp.Key, ShouldEqual, key)
			So(helixKeyResp.Status, ShouldEqual, expectedStatus)
		}
	}
}

func makeKeyArray(n int) []string {
	keys := make([]string, 0)
	for i := 0; i < n; i++ {
		k, err := random.RandomString(random.DefaultAlphabet, 15)
		So(err, ShouldBeNil)
		keys = append(keys, k)
	}
	return keys
}

func TestAPI_HelixRedeemKey(t *testing.T) {
	Convey("Given a helix redeem key API", t, func() {
		ctx := context.Background()

		keyValidator := new(key_mock.Validator)
		authorizer := new(authorization_mock.Authorizer)
		auditor := new(audit_mock.Auditor)
		throttler := new(throttle_mock.IThrottler)
		claimer := new(key_mock.Claimer)
		userClient := new(usersclient_internal_mock.InternalClient)

		api := helix_redeem_key.API{
			BaseAPI: &redeem_key_base.APIImpl{
				BaseContainer: &base.Container{
					Authorizer: authorizer,
					Throttler:  throttler,
				},
				Validator: keyValidator,
				Auditor:   auditor,
				Claimer:   claimer,
			},
			BaseContainer: &base.Container{
				Authorizer: authorizer,
				Throttler:  throttler,
			},
			Auditor:            auditor,
			UsersServiceClient: userClient,
		}

		keyCode := "ABCDE-FGHIJ-KLMNO"
		userId := "test-user"
		clientIP := "127.0.0.1"

		Convey("Given a request from a non-whitelisted client", func() {
			ctx = context.WithValue(ctx, middleware.ClientIDContextKey, "Evil Corp")
			_, err := api.HelixRedeemKey(ctx, &phanto.HelixRedeemKeyReq{})
			test.AssertTwirpError(err, twirp.PermissionDenied)
		})

		Convey("Givent a request from a whitelisted client", func() {
			ctx = context.WithValue(ctx, middleware.ClientIDContextKey, helix.TestClientID)

			Convey("Given a nil request", func() {
				var req *phanto.HelixRedeemKeyReq = nil

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})
			})

			Convey("Given a request with a blank userID", func() {
				req := &phanto.HelixRedeemKeyReq{
					UserId: "",
				}

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})
			})

			Convey("Given a request with a blank client ip", func() {
				req := &phanto.HelixRedeemKeyReq{
					UserId:   userId,
					ClientIp: "",
				}

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})
			})

			Convey("Given a request with an empty key array", func() {
				req := &phanto.HelixRedeemKeyReq{
					UserId:   "test-user",
					ClientIp: clientIP,
					Keys:     []string{},
				}

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})
			})

			Convey("Given a request with a too large key array", func() {
				req := &phanto.HelixRedeemKeyReq{
					UserId:   "test-user",
					ClientIp: clientIP,
					Keys:     makeKeyArray(21),
				}

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})
			})

			Convey("When throttler errors", func() {
				req := &phanto.HelixRedeemKeyReq{
					UserId:   "test-user",
					ClientIp: clientIP,
					Keys:     makeKeyArray(1),
				}
				throttler.On("ThrottleExponentially", fmt.Sprintf("HelixRedeemKey.%s", userId)).Return(throttle.ThrottleResponse{IsThrottled: true}, errors.New("test redis get error"))

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.Internal)
				})
			})

			Convey("When throttler throttles the user", func() {
				req := &phanto.HelixRedeemKeyReq{
					UserId:   "test-user",
					ClientIp: clientIP,
					Keys:     makeKeyArray(1),
				}
				throttler.On("ThrottleExponentially", fmt.Sprintf("HelixRedeemKey.%s", userId)).Return(throttle.ThrottleResponse{IsThrottled: true}, nil)

				Convey("We should return an error", func() {
					_, err := api.HelixRedeemKey(ctx, req)
					test.AssertTwirpError(err, twirp.ResourceExhausted)
				})
			})

			Convey("When the user is not throttled", func() {
				throttler.On("ThrottleExponentially", mock.Anything).Return(throttle.ThrottleResponse{IsThrottled: false}, nil)

				Convey("Errors when the auditor returns an error", func() {
					req := &phanto.HelixRedeemKeyReq{
						UserId:   "test-user",
						ClientIp: clientIP,
						Keys:     makeKeyArray(1),
					}
					auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

					Convey("We should return an error", func() {
						_, err := api.HelixRedeemKey(ctx, req)
						test.AssertTwirpError(err, twirp.Internal)
					})
				})

				Convey("When the auditor succeeds", func() {
					auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
					auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

					Convey("Errors when user service errors", func() {
						userClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))

						req := &phanto.HelixRedeemKeyReq{
							UserId:   userId,
							ClientIp: clientIP,
							Keys:     []string{keyCode},
						}
						Convey("We should return an internal error status", func() {
							_, err := api.HelixRedeemKey(ctx, req)
							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("Errors when user service doesn't find user", func() {
						userClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

						req := &phanto.HelixRedeemKeyReq{
							UserId:   userId,
							ClientIp: clientIP,
							Keys:     []string{keyCode},
						}
						Convey("We should return a not found error status", func() {
							_, err := api.HelixRedeemKey(ctx, req)
							test.AssertTwirpError(err, twirp.NotFound)
						})
					})

					Convey("When user service returns a user", func() {
						userClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
							RemoteIP: pointers.StringP("127.0.0.1"),
						}, nil)

						Convey("Given a request with an invalid key", func() {
							k := "NOT-A-VALID-KEY"
							req := &phanto.HelixRedeemKeyReq{
								UserId:   "test-user",
								ClientIp: clientIP,
								Keys:     []string{k},
							}

							Convey("Response should contain incorrect format result", func() {
								resp, err := api.HelixRedeemKey(ctx, req)
								So(err, ShouldBeNil)
								expectResultContains(resp, k, phanto.KeyStatus_INCORRECT_FORMAT)
							})
						})

						Convey("Given a request with a single key", func() {
							req := &phanto.HelixRedeemKeyReq{
								UserId:   userId,
								ClientIp: clientIP,
								Keys:     []string{keyCode},
							}

							hashedCode, err := code.Formatted(keyCode).Raw().Hash()
							So(err, ShouldBeNil)

							keyClaimData := data.Validation{
								IsValid:   true,
								KeyStatus: status.KeyStatusUnclaimed,
							}

							Convey("When the validator errors with an unknown error", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, errors.New("not a twirp error"))

								Convey("We should return an internal error status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_INTERNAL_ERROR)
								})
							})

							Convey("When the validator errors with a key not found error", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, key.NotFoundError)

								Convey("We should return a not found status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_NOT_FOUND)
								})
							})

							Convey("When the validator returns an unused key whose start date is in the future", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(1 * time.Hour),
									EndDate:   time.Now().Add(2 * time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								Convey("We should return an expired status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_EXPIRED)
								})
							})

							Convey("When the validator returns an unused key whose end date is in the past", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-2 * time.Hour),
									EndDate:   time.Now().Add(-1 * time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								Convey("We should return an expired status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_EXPIRED)
								})
							})

							Convey("When the validator returns an already claimed key", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusClaimed,
								}, nil)

								Convey("We should return an already claimed status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_ALREADY_CLAIMED)
								})
							})

							Convey("When the validator returns an invalidated key", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusInvalidated,
								}, nil)

								Convey("We should return an inactive status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_INACTIVE)
								})
							})

							Convey("When the validator returns an invalid unclaimed key", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								Convey("We should return an inactive status", func() {
									resp, err := api.HelixRedeemKey(ctx, req)
									So(err, ShouldBeNil)
									expectResultContains(resp, keyCode, phanto.KeyStatus_INACTIVE)
								})
							})

							Convey("When the validator returns an unused key", func() {
								keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(data.Validation{
									IsValid:   true,
									StartDate: time.Now().Add(-time.Hour),
									EndDate:   time.Now().Add(time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								Convey("When claimer errors during claiming", func() {
									claimer.On("Claim", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test-error"))

									Convey("We should return an internal error status", func() {
										resp, err := api.HelixRedeemKey(ctx, req)
										So(err, ShouldBeNil)
										expectResultContains(resp, keyCode, phanto.KeyStatus_INTERNAL_ERROR)
									})
								})

								Convey("When claimer succeeds in claiming", func() {
									claimer.On("Claim", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

									Convey("We should return a successfully claimed status", func() {
										resp, err := api.HelixRedeemKey(ctx, req)
										So(err, ShouldBeNil)
										expectResultContains(resp, keyCode, phanto.KeyStatus_SUCCESSFULLY_REDEEMED)
									})
								})
							})

							Convey("When the request contains multiple keys", func() {
								req.Keys = []string{
									"NOT-VALID",         // 0: this one will be incorrect format
									"11111-11111-11111", // 1: this one will internal error
									"22222-22222-22222", // 2: this one will be successfully claimed
									"33333-33333-33333", // 3: this one will be successfully claimed
									"44444-44444-44444", // 4: this one will be expired
									"55555-55555-55555", // 5: this one will be inactive
									"66666-66666-66666", // 6: this one will be not found
									"77777-77777-77777", // 7 :this one will be ineligible
								}

								hashedCode1, err := code.Formatted(req.Keys[1]).Raw().Hash()
								So(err, ShouldBeNil)
								hashedCode2, err := code.Formatted(req.Keys[2]).Raw().Hash()
								So(err, ShouldBeNil)
								hashedCode3, err := code.Formatted(req.Keys[3]).Raw().Hash()
								So(err, ShouldBeNil)
								hashedCode4, err := code.Formatted(req.Keys[4]).Raw().Hash()
								So(err, ShouldBeNil)
								hashedCode5, err := code.Formatted(req.Keys[5]).Raw().Hash()
								So(err, ShouldBeNil)
								hashedCode6, err := code.Formatted(req.Keys[6]).Raw().Hash()
								So(err, ShouldBeNil)
								hashedCode7, err := code.Formatted(req.Keys[7]).Raw().Hash()
								So(err, ShouldBeNil)

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode1, mock.Anything).Return(data.Validation{}, errors.New("internal error"))

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode2, mock.Anything).Return(data.Validation{
									IsValid:   true,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode3, mock.Anything).Return(data.Validation{
									IsValid:   true,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode4, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-2 * time.Hour),
									EndDate:   time.Now().Add(-1 * time.Hour),
									KeyStatus: status.KeyStatusUnclaimed,
								}, nil)

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode5, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusInvalidated,
								}, nil)

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode6, mock.Anything).Return(data.Validation{}, key.NotFoundError)

								keyValidator.On("ValidateForUser", mock.Anything, hashedCode7, mock.Anything).Return(data.Validation{
									IsValid:   false,
									StartDate: time.Now().Add(-1 * time.Hour),
									EndDate:   time.Now().Add(1 * time.Hour),
									KeyStatus: status.KeyStatusIneligible,
								}, nil)

								claimer.On("Claim", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

								resp, err := api.HelixRedeemKey(ctx, req)
								So(err, ShouldBeNil)

								expectResultContains(resp, req.Keys[0], phanto.KeyStatus_INCORRECT_FORMAT)
								expectResultContains(resp, req.Keys[1], phanto.KeyStatus_INTERNAL_ERROR)
								expectResultContains(resp, req.Keys[2], phanto.KeyStatus_SUCCESSFULLY_REDEEMED)
								expectResultContains(resp, req.Keys[3], phanto.KeyStatus_SUCCESSFULLY_REDEEMED)
								expectResultContains(resp, req.Keys[4], phanto.KeyStatus_EXPIRED)
								expectResultContains(resp, req.Keys[5], phanto.KeyStatus_INACTIVE)
								expectResultContains(resp, req.Keys[6], phanto.KeyStatus_NOT_FOUND)
								expectResultContains(resp, req.Keys[7], phanto.KeyStatus_USER_NOT_ELIGIBLE)
							})
						})
					})
				})
			})
		})
	})
}
