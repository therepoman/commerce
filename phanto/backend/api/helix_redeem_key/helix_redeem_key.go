package helix_redeem_key

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	base_restriction "code.justin.tv/commerce/phanto/backend/api/base/restriction"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_base"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	BaseContainer      *base.Container                     `inject:""`
	BaseAPI            redeem_key_base.API                 `inject:""`
	Auditor            audit.Auditor                       `inject:""`
	UsersServiceClient usersclient_internal.InternalClient `inject:""`
}

func (api *API) HelixRedeemKey(ctx context.Context, req *phanto.HelixRedeemKeyReq) (resp *phanto.HelixRedeemKeyResp, err error) {
	if !helix.IsWhitelistedHelixClient(ctx) {
		return nil, twirp.NewError(twirp.PermissionDenied, "Request from unknown client")
	}

	if req == nil {
		return nil, twirp.InvalidArgumentError("Keys", "Request body cannot be empty")
	} else if strings.Blank(req.UserId) {
		return nil, twirp.InvalidArgumentError("UserId", "UserId cannot be blank")
	} else if len(req.Keys) <= 0 {
		return nil, twirp.InvalidArgumentError("Keys", "Request must contain at least one key")
	} else if len(req.Keys) > 20 {
		return nil, twirp.InvalidArgumentError("Keys", "Request can contain no more than 20 keys")
	}

	err = base_throttle.ThrottleUserExponentiallyByKeyFunc("HelixRedeemKey")(ctx, *api.BaseContainer, req.UserId)
	if err != nil {
		return nil, err
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.HelixRedeemKeyOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	userProps, err := api.UsersServiceClient.GetUserByID(ctx, req.UserId, nil)
	if err != nil {
		log.WithError(err).Error("error looking up userID in user service")
		return nil, twirp.InternalErrorWith(err)
	}

	if userProps == nil {
		return nil, twirp.NotFoundError("user not found")
	}

	userIP := pointers.StringOrDefault(userProps.RemoteIP, "")

	results := make([]*phanto.HelixKeyResp, 0)
	for _, key := range req.Keys {
		claimData, _, err := api.BaseAPI.RedeemKeyBase(ctx, &phanto.RedeemKeyReq{
			Key:      key,
			UserId:   req.UserId,
			ClientIp: userIP,
		}, redeem_key_base.RedeemKeyOptions{
			AuthorizationFunc: base_authorization.NoAuthorization,
			ThrottleFunc:      base_throttle.NoThrottle,
			RestrictionFunc:   base_restriction.NoRestriction,
			PerformAuditing:   false,
			RedeemType:        base.RedeemType_HelixByKeyAPI,
		})

		results = append(results, &phanto.HelixKeyResp{
			Key:    key,
			Status: helix.GetKeyStatus(true, claimData, err),
		})
	}

	return &phanto.HelixRedeemKeyResp{
		Results: results,
	}, nil
}
