package get_key_status

import (
	"context"

	base_authentication "code.justin.tv/commerce/phanto/backend/api/base/authentication"
	base_authorization "code.justin.tv/commerce/phanto/backend/api/base/authorization"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status_base"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/rpc"
)

type API struct {
	BaseAPI get_key_status_base.API `inject:""`
	Auditor audit.Auditor           `inject:""`
}

func (api *API) GetKeyStatus(ctx context.Context, req *phanto.GetKeyStatusReq) (resp *phanto.GetKeyStatusResp, err error) {
	keyData, auditRecord, err := api.BaseAPI.GetKeyStatusBase(ctx, req, get_key_status_base.GetKeyStatusOptions{
		AuthorizationFunc:  base_authorization.RequestIsAuthenticated,
		AuthenticationFunc: base_authentication.GetAuthenticatedUserFromContext,
		ThrottleFunc:       base_throttle.ThrottleUserExponentiallyByKeyFunc("GetKeyStatus"),
		PerformAuditing:    true,
		AuditOperation:     audit.GetKeyStatusOperation,
	})

	defer func() {
		if auditRecord != nil {
			api.Auditor.AuditResponse(ctx, auditRecord, &audit.Response{Body: resp, Error: err})
		}
	}()

	if err != nil {
		return nil, err
	}

	return &phanto.GetKeyStatusResp{
		ProductType: keyData.ProductType,
		Description: keyData.ProductDescription,
		Sku:         keyData.SKU,
		PoolId:      keyData.KeyPoolID,
		BatchId:     keyData.KeyBatchID,
		Status:      string(keyData.KeyStatus),
	}, err
}
