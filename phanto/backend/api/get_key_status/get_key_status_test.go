package get_key_status

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/get_key_status_base"
	authorization_const "code.justin.tv/commerce/phanto/backend/authorization"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/key/code"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyStatus(t *testing.T) {
	Convey("Given a key status API", t, func() {
		keyValidator := new(key_mock.Validator)
		authorizer := new(authorization_mock.Authorizer)
		auditor := new(audit_mock.Auditor)
		throttler := new(throttle_mock.IThrottler)

		api := API{
			BaseAPI: &get_key_status_base.APIImpl{
				BaseContainer: &base.Container{
					Authorizer: authorizer,
					Throttler:  throttler,
				},
				Validator: keyValidator,
				Auditor:   auditor,
			},
			Auditor: auditor,
		}

		keyCode := "asdfa-sdfas-dfasd"
		userId := "authentic-ohki"

		Convey("Given a nil request", func() {
			var req *phanto.GetKeyStatusReq = nil

			Convey("We should return an error", func() {
				_, err := api.GetKeyStatus(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an empty key", func() {
			req := &phanto.GetKeyStatusReq{
				Key: "",
			}

			Convey("We should return an error", func() {
				_, err := api.GetKeyStatus(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with an invalid key", func() {
			req := &phanto.GetKeyStatusReq{
				Key: "NOT-A-VALID-KEY",
			}

			Convey("We should return an error", func() {
				_, err := api.GetKeyStatus(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with a key", func() {
			ctx := context.WithValue(context.Background(), authorization_const.AuthenticatedUserIDContextKey, userId)

			req := &phanto.GetKeyStatusReq{
				Key: keyCode,
			}

			hashedCode, err := code.Formatted(keyCode).Raw().Hash()
			So(err, ShouldBeNil)

			keyClaimData := data.Validation{
				IsValid:            true,
				SNSTopic:           "walrusTopic",
				ProductType:        "productType",
				ProductDescription: "productDescription",
				KeyPoolID:          "keyPoolID",
				KeyBatchID:         "keyBatchID",
				KeyCode:            hashedCode,
				SKU:                "sku",
				KeyStatus:          status.KeyStatusUnclaimed,
			}

			Convey("When there is no user authenticated", func() {
				authorizer.On("RequestIsAuthenticated", ctx).Return(false)

				Convey("We should return an error", func() {
					_, err := api.GetKeyStatus(ctx, req)

					test.AssertTwirpError(err, twirp.PermissionDenied)
				})
			})

			Convey("When there is an authenticated user", func() {
				authorizer.On("RequestIsAuthenticated", ctx).Return(true)
				authorizer.On("GetAuthenticatedUser", ctx).Return(userId)

				Convey("When throttler experiences an error getting value from given key", func() {
					throttler.On("ThrottleExponentially", fmt.Sprintf("GetKeyStatus.%s", userId)).Return(throttle.ThrottleResponse{IsThrottled: true}, errors.New("test redis get error"))
					_, err := api.GetKeyStatus(ctx, req)

					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("When the request gets throttled", func() {
					throttler.On("ThrottleExponentially", fmt.Sprintf("GetKeyStatus.%s", userId)).Return(throttle.ThrottleResponse{IsThrottled: true}, nil)
					_, err := api.GetKeyStatus(ctx, req)

					test.AssertTwirpError(err, twirp.ResourceExhausted)
				})

				Convey("When the request doesn't get throttled", func() {
					throttler.On("ThrottleExponentially", fmt.Sprintf("GetKeyStatus.%s", userId)).Return(throttle.ThrottleResponse{IsThrottled: false}, nil)

					Convey("Errors when the auditor returns an error", func() {
						auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
						_, err := api.GetKeyStatus(ctx, req)
						test.AssertTwirpError(err, twirp.Internal)
					})

					Convey("When the auditor succeeds", func() {
						auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
						auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

						Convey("When the validator errors with an unknown error", func() {
							keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, errors.New("WALRUS STRIKE!!!"))

							Convey("we should return an internal error", func() {
								_, err := api.GetKeyStatus(ctx, req)
								test.AssertTwirpError(err, twirp.Internal)
							})
						})

						Convey("When the validator errors with a not found error", func() {
							keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, key.NotFoundError)

							Convey("we should return a not found error", func() {
								_, err := api.GetKeyStatus(ctx, req)
								test.AssertTwirpError(err, twirp.NotFound)
							})
						})

						Convey("When the validator returns successfully", func() {
							keyValidator.On("ValidateForUser", mock.Anything, hashedCode, mock.Anything).Return(keyClaimData, nil)

							Convey("We should return a valid response", func() {
								resp, err := api.GetKeyStatus(ctx, req)

								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)
								So(resp.Sku, ShouldEqual, keyClaimData.SKU)
								So(resp.Description, ShouldEqual, keyClaimData.ProductDescription)
								So(resp.Status, ShouldEqual, string(keyClaimData.KeyStatus))
								So(resp.BatchId, ShouldEqual, keyClaimData.KeyBatchID)
								So(resp.PoolId, ShouldEqual, keyClaimData.KeyPoolID)
								So(resp.ProductType, ShouldEqual, keyClaimData.ProductType)
							})
						})
					})
				})
			})
		})
	})
}
