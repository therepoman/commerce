package helix_redeem_key_by_batch

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	base_throttle "code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/key_batch/key_set"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	BaseContainer      *base.Container                     `inject:""`
	Auditor            audit.Auditor                       `inject:""`
	BaseAPI            redeem_key_by_batch_base.API        `inject:""`
	UsersServiceClient usersclient_internal.InternalClient `inject:""`
	KeySetChecker      key_set.Checker                     `inject:""`
}

func (api *API) HelixRedeemKeyByBatch(ctx context.Context, req *phanto.HelixRedeemKeyByBatchReq) (resp *phanto.HelixRedeemKeyByBatchResp, err error) {
	if !helix.IsWhitelistedHelixClient(ctx) {
		return nil, twirp.NewError(twirp.PermissionDenied, "Request from unknown client")
	}

	if req == nil {
		return nil, twirp.InvalidArgumentError("Key", "Request body cannot be empty")
	} else if strings.Blank(req.BatchId) {
		return nil, twirp.InvalidArgumentError("BatchId", "BatchId cannot be blank")
	} else if strings.Blank(req.UserId) {
		return nil, twirp.InvalidArgumentError("UserId", "UserId cannot be blank")
	}

	err = base_throttle.ThrottleUserExponentiallyByKeyFunc("HelixRedeemKeyByBatch")(ctx, *api.BaseContainer, req.UserId)
	if err != nil {
		return nil, err
	}

	userProps, err := api.UsersServiceClient.GetUserByID(ctx, req.UserId, nil)
	if err != nil {
		log.WithError(err).Error("error looking up userID in user service")
		return nil, twirp.InternalErrorWith(err)
	}

	if userProps == nil {
		return nil, twirp.InvalidArgumentError("UserId", "UserId not found")
	}

	userIP := pointers.StringOrDefault(userProps.RemoteIP, "")

	claimData, auditRecord, newKeyCount, err := api.BaseAPI.RedeemKeyByBatchBase(ctx, &phanto.RedeemKeyByBatchReq{
		UserId:         req.UserId,
		BatchId:        req.BatchId,
		UserIp:         userIP,
		IdempotencyKey: req.IdempotencyKey,
	}, redeem_key_by_batch_base.RedeemKeyByBatchOptions{
		PerformAuditing: true,
		AuditOperation:  audit.HelixRedeemKeyByBatchOperation,
		RedeemType:      base.RedeemType_HelixByBatchAPI,
	})
	defer func() { api.Auditor.AuditResponse(ctx, auditRecord, &audit.Response{Body: resp, Error: err}) }()

	_, isKnownError := redeem_key_by_batch_base.KnownError[err]
	if isKnownError {
		return nil, err
	}

	status := helix.GetKeyStatus(true, claimData, err)
	if status == phanto.KeyStatus_INTERNAL_ERROR {
		log.WithError(err).Error("unknown error redeeming key by batch")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.HelixRedeemKeyByBatchResp{
		Status:        status,
		KeysRemaining: newKeyCount,
	}, nil
}
