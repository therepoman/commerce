package helix_redeem_key_by_batch_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	"code.justin.tv/commerce/phanto/backend/api/helix_redeem_key_by_batch"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_HelixRedeemKeyByBatch(t *testing.T) {
	Convey("Given a helix redeem key by batch api", t, func() {
		baseAPI := new(redeem_key_by_batch_base_mock.API)
		auditor := new(audit_mock.Auditor)
		userClient := new(usersclient_internal_mock.InternalClient)
		throttler := new(throttle_mock.IThrottler)
		api := &helix_redeem_key_by_batch.API{
			BaseContainer: &base.Container{
				Throttler: throttler,
			},
			Auditor:            auditor,
			BaseAPI:            baseAPI,
			UsersServiceClient: userClient,
		}

		auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Errors when called by a non-whitelisted client", func() {
			ctx := context.WithValue(context.Background(), middleware.ClientIDContextKey, "Evil Inc. Client ID")
			_, err := api.HelixRedeemKeyByBatch(ctx, &phanto.HelixRedeemKeyByBatchReq{})
			test.AssertTwirpError(err, twirp.PermissionDenied)
		})

		Convey("Given a whitelisted client", func() {
			ctx := context.WithValue(context.Background(), middleware.ClientIDContextKey, helix.TestClientID)

			Convey("Errors when request is nil", func() {
				_, err := api.HelixRedeemKeyByBatch(ctx, nil)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})

			Convey("Errors when batchId is blank", func() {
				_, err := api.HelixRedeemKeyByBatch(ctx, &phanto.HelixRedeemKeyByBatchReq{
					BatchId: "",
				})
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})

			Convey("Errors when userId is blank", func() {
				_, err := api.HelixRedeemKeyByBatch(ctx, &phanto.HelixRedeemKeyByBatchReq{
					BatchId: "batch-id",
					UserId:  "",
				})
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})

			Convey("Given a valid request", func() {
				req := &phanto.HelixRedeemKeyByBatchReq{
					BatchId: "batch-id",
					UserId:  "user-id",
				}

				Convey("Errors when throttled", func() {
					throttler.On("ThrottleExponentially", mock.Anything).Return(throttle.ThrottleResponse{
						IsThrottled: true,
					}, nil)
					_, err := api.HelixRedeemKeyByBatch(ctx, req)
					test.AssertTwirpError(err, twirp.ResourceExhausted)
				})

				Convey("When not throttled", func() {
					throttler.On("ThrottleExponentially", mock.Anything).Return(throttle.ThrottleResponse{
						IsThrottled: false,
					}, nil)

					Convey("Errors when user service errors", func() {
						userClient.On("GetUserByID", ctx, req.UserId, mock.Anything).Return(nil, errors.New("test error"))
						_, err := api.HelixRedeemKeyByBatch(ctx, req)
						test.AssertTwirpError(err, twirp.Internal)
					})

					Convey("Errors when user service returns nil", func() {
						userClient.On("GetUserByID", ctx, req.UserId, mock.Anything).Return(nil, nil)
						_, err := api.HelixRedeemKeyByBatch(ctx, req)
						test.AssertTwirpError(err, twirp.InvalidArgument)
					})

					Convey("When user service returns a user", func() {
						userClient.On("GetUserByID", ctx, req.UserId, mock.Anything).Return(&models.Properties{
							RemoteIP: pointers.StringP("127.0.0.1"),
						}, nil)

						Convey("Errors when base api returns an unknown error", func() {
							baseAPI.On("RedeemKeyByBatchBase", ctx, mock.Anything, mock.Anything).Return(nil, nil, int64(0), errors.New("test error"))
							_, err := api.HelixRedeemKeyByBatch(ctx, req)
							test.AssertTwirpError(err, twirp.Internal)
						})

						Convey("Succeeds with the correct status when base api returns an expected failure error", func() {
							baseAPI.On("RedeemKeyByBatchBase", ctx, mock.Anything, mock.Anything).Return(nil, nil, int64(0), redeem_key_by_batch_base.NoRemainingKeysError)
							_, err := api.HelixRedeemKeyByBatch(ctx, req)
							So(err, ShouldNotBeNil)
							So(err, ShouldEqual, redeem_key_by_batch_base.NoRemainingKeysError)
						})

						Convey("Succeeds with the correct status when base api returns an expected key status error", func() {
							baseAPI.On("RedeemKeyByBatchBase", ctx, mock.Anything, mock.Anything).Return(nil, nil, int64(0), twirp.NotFoundError("key not found"))
							resp, err := api.HelixRedeemKeyByBatch(ctx, req)
							So(err, ShouldBeNil)
							So(resp.Status, ShouldEqual, phanto.KeyStatus_NOT_FOUND)
						})

						Convey("Succeeds with the correct status when base api returns successfully", func() {
							baseAPI.On("RedeemKeyByBatchBase", ctx, mock.Anything, mock.Anything).Return(&data.Validation{}, nil, int64(15), nil)
							resp, err := api.HelixRedeemKeyByBatch(ctx, req)
							So(err, ShouldBeNil)
							So(resp.Status, ShouldEqual, phanto.KeyStatus_SUCCESSFULLY_REDEEMED)
							So(resp.KeysRemaining, ShouldEqual, 15)
						})
					})
				})
			})
		})
	})
}
