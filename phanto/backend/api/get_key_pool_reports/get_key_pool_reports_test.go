package get_key_pool_reports_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_reports"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyPoolReports(t *testing.T) {
	Convey("Given an API struct", t, func() {
		auditor := new(audit_mock.Auditor)
		reportDAO := new(report_mock.DAO)
		api := &get_key_pool_reports.API{
			ReportDAO: reportDAO,
			Auditor:   auditor,
		}

		Convey("Errors when request is nil", func() {
			_, err := api.GetKeyPoolReports(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when pool_id is blank", func() {
			_, err := api.GetKeyPoolReports(context.Background(), &phanto.GetKeyPoolReportsReq{
				PoolId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.GetKeyPoolReportsReq{
				PoolId: "test-pool-id",
			}

			Convey("Errors when auditor errors", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.GetKeyPoolReports(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when dynamo DAO errors", func() {
					reportDAO.On("GetReportsByPool", req.PoolId, "").Return(nil, "", errors.New("test-error"))
					_, err := api.GetKeyPoolReports(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Succeeds when dynamo DAO returns empty array", func() {
					reportDAO.On("GetReportsByPool", req.PoolId, "").Return([]*report.Report{}, "", nil)
					resp, err := api.GetKeyPoolReports(context.Background(), req)
					So(err, ShouldBeNil)
					So(len(resp.KeyPoolReports), ShouldEqual, 0)
					So(resp.Cursor, ShouldBeBlank)
				})

				Convey("Succeeds when dynamo DAO returns non-empty array", func() {
					reportDAO.On("GetReportsByPool", req.PoolId, "").Return([]*report.Report{{ReportID: "1"}, {ReportID: "2"}}, "cursor-1", nil)
					resp, err := api.GetKeyPoolReports(context.Background(), req)
					So(err, ShouldBeNil)
					So(len(resp.KeyPoolReports), ShouldEqual, 2)
					So(resp.Cursor, ShouldEqual, "cursor-1")
				})
			})
		})
	})
}
