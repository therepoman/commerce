package get_key_pool_reports

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	ReportDAO report.DAO    `inject:""`
	Auditor   audit.Auditor `inject:""`
}

func (api *API) GetKeyPoolReports(ctx context.Context, req *phanto.GetKeyPoolReportsReq) (resp *phanto.GetKeyPoolReportsResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "Request body cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "Pool id cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyPoolReportsOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoReports, newCursor, err := api.ReportDAO.GetReportsByPool(req.PoolId, req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key pool reports from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key pool reports")
	}

	return &phanto.GetKeyPoolReportsResp{
		Cursor:         newCursor,
		KeyPoolReports: report.ToKeyPoolReportTwirpModel(dynamoReports),
	}, nil
}
