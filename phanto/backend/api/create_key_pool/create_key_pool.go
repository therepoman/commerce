package create_key_pool

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/dynamo/utils"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/backend/product"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	ProductValidator product.Validator     `inject:"productValidator"`
	KeyPoolDAO       key_pool.DAO          `inject:""`
	HandlerGroupDAO  key_handler_group.DAO `inject:""`
	Auditor          audit.Auditor         `inject:""`
}

func (api *API) CreateKeyPool(ctx context.Context, req *phanto.CreateKeyPoolReq) (resp *phanto.CreateKeyPoolResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolId", "request cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolId", "pool id cannot be blank")
	} else if strings.Blank(req.ProductType) {
		return nil, twirp.InvalidArgumentError("ProductType", "product type cannot be blank")
	} else if strings.Blank(req.Sku) {
		return nil, twirp.InvalidArgumentError("Sku", "product sku cannot be blank")
	} else if strings.Blank(req.Description) {
		return nil, twirp.InvalidArgumentError("Description", "description cannot be blank")
	}

	redemptionModeName, ok := phanto.RedemptionMode_name[int32(req.RedemptionMode)]
	if !ok {
		return nil, twirp.InvalidArgumentError("RedemptionMode", "invalid redemption mode")
	}

	handlerSet := strings.NotBlank(req.Handler)
	handlerGroupSet := strings.NotBlank(req.HandlerGroupId)

	if handlerSet && handlerGroupSet {
		return nil, twirp.InvalidArgumentError("Handler", "both handler and handler group id cannot be present")
	}

	if !handlerSet && !handlerGroupSet {
		return nil, twirp.InvalidArgumentError("Handler", "either handler or handler group must be present")
	}

	req.PoolId = go_strings.TrimSpace(req.PoolId)
	req.ProductType = go_strings.TrimSpace(req.ProductType)
	req.Sku = go_strings.TrimSpace(req.Sku)

	startDate, err := ptypes.Timestamp(req.StartDate)
	if err != nil {
		return nil, twirp.InvalidArgumentError("StartDate", "invalid start date")
	}

	endDate, err := ptypes.Timestamp(req.EndDate)
	if err != nil {
		return nil, twirp.InvalidArgumentError("EndDate", "invalid end date")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.CreateKeyPoolOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	validationData, err := api.ProductValidator.Validate(ctx, req.ProductType, req.Sku)
	if err != nil {
		log.WithError(err).Error("downstream dependency error while validating product")
		return nil, twirp.InternalErrorWith(err)
	}

	if !validationData.IsValid {
		return nil, twirp.InvalidArgumentError("ProductType", "product is invalid")
	}

	if strings.NotBlank(req.HandlerGroupId) {
		group, err := api.HandlerGroupDAO.GetKeyHandlerGroup(req.HandlerGroupId)
		if err != nil {
			log.WithError(err).WithField("HandlerGroupID", req.HandlerGroupId).Error("error getting key handler group from dynamo")
			return nil, twirp.InternalError("downstream dependency error creating key pool")
		}

		if group == nil {
			log.WithError(err).WithField("HandlerGroupID", req.HandlerGroupId).Error("key handler group not found in dynamo")
			return nil, twirp.InvalidArgumentError("HandlerGroupID", "handler group does not exist")
		}
	}

	newKeyPool := &key_pool.KeyPool{
		PoolID:         req.PoolId,
		ProductType:    req.ProductType,
		SKU:            req.Sku,
		KeyPoolStatus:  status.KeyPoolStatusActive,
		Description:    req.Description,
		Handler:        req.Handler,
		HandlerGroupID: req.HandlerGroupId,
		RedemptionMode: redemptionModeName,
		StartDate:      startDate,
		EndDate:        endDate,
	}

	err = api.KeyPoolDAO.CreateNew(newKeyPool)
	if utils.IsConditionalCheckErr(err) {
		return nil, twirp.NewError(twirp.FailedPrecondition, "key pool already exists")
	}

	if err != nil {
		log.WithError(err).Error("could not write new key pool to database")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.CreateKeyPoolResp{}, nil
}
