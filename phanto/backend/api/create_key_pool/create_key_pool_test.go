package create_key_pool_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/create_key_pool"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	aws_test "code.justin.tv/commerce/phanto/backend/dynamo/test"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	audit_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	key_handler_group_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	key_pool_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	product_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/product"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_CreateKeyPool(t *testing.T) {
	Convey("Given a create key pool API", t, func() {
		ctx := context.Background()
		productValidator := new(product_mock.Validator)
		keyPoolDAO := new(key_pool_mock.DAO)
		auditor := new(audit_mock.Auditor)
		handlerGroupDAO := new(key_handler_group_mock.DAO)
		api := create_key_pool.API{
			ProductValidator: productValidator,
			KeyPoolDAO:       keyPoolDAO,
			HandlerGroupDAO:  handlerGroupDAO,
			Auditor:          auditor,
		}

		Convey("Given a nil request", func() {
			var req *phanto.CreateKeyPoolReq = nil

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank pool ID", func() {
			req := &phanto.CreateKeyPoolReq{}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank product type", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId: "asdfasdfasdf",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank sku", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:      "asdfasdfasdf",
				ProductType: "walrus",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank description", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:      "asdfasdfasdf",
				ProductType: "walrus",
				Sku:         "test-sku",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank handler and handler group", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:      "asdfasdfasdf",
				ProductType: "walrus",
				Sku:         "test-sku",
				Description: "test description",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a set handler and a set handler group", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:         "asdfasdfasdf",
				ProductType:    "walrus",
				Sku:            "test-sku",
				Description:    "test description",
				Handler:        "test-handler",
				HandlerGroupId: "test-handler-group",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a nil start date", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:      "asdfasdfasdf",
				ProductType: "walrus",
				Sku:         "test-sku",
				Description: "test description",
				Handler:     "test handler",
				StartDate:   nil,
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a nil end date", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:      "asdfasdfasdf",
				ProductType: "walrus",
				Sku:         "test-sku",
				Description: "test description",
				Handler:     "test handler",
				StartDate:   ptypes.TimestampNow(),
				EndDate:     nil,
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given an invalid redemption mode", func() {
			req := &phanto.CreateKeyPoolReq{
				PoolId:         "asdfasdfasdf",
				ProductType:    "walrus",
				Sku:            "test-sku",
				Description:    "test description",
				Handler:        "test handler",
				StartDate:      ptypes.TimestampNow(),
				EndDate:        ptypes.TimestampNow(),
				RedemptionMode: phanto.RedemptionMode(9999),
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a valid request", func() {
			poolID := "walrusPool"
			productType := "walrus"
			sku := "walrusSKU"
			desc := "This is the sku to redeem one walrus hug"
			handlerGroupID := "handler-group-id"
			startDate := ptypes.TimestampNow()
			endDate := ptypes.TimestampNow()
			startDateTime, _ := ptypes.Timestamp(startDate)
			endDateTime, _ := ptypes.Timestamp(endDate)

			req := &phanto.CreateKeyPoolReq{
				PoolId:         poolID,
				ProductType:    productType,
				Sku:            sku,
				Description:    desc,
				HandlerGroupId: handlerGroupID,
				StartDate:      startDate,
				EndDate:        endDate,
				RedemptionMode: phanto.RedemptionMode_REDEEM_BY_KEY,
			}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.CreateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("When the validator returns an error", func() {
					productValidator.On("Validate", ctx, productType, sku).Return(data.Validation{IsValid: false}, errors.New("WALRUS STRIKE"))

					Convey("Then we should return an error", func() {
						_, err := api.CreateKeyPool(context.Background(), req)

						test.AssertTwirpError(err, twirp.Internal)
					})
				})

				Convey("When the validator says the product type is invalid", func() {
					productValidator.On("Validate", ctx, productType, sku).Return(data.Validation{IsValid: false}, nil)

					Convey("Then we should return an error", func() {
						_, err := api.CreateKeyPool(context.Background(), req)

						test.AssertTwirpError(err, twirp.InvalidArgument)
					})
				})

				Convey("When the validator says the product type is valid", func() {
					validationData := data.Validation{
						IsValid:     true,
						ProductType: productType,
						SKU:         sku,
					}

					productValidator.On("Validate", ctx, productType, sku).Return(validationData, nil)

					Convey("When the handler group DAO errors", func() {
						handlerGroupDAO.On("GetKeyHandlerGroup", handlerGroupID).Return(nil, errors.New("test error"))

						Convey("Then we should return an error", func() {
							_, err := api.CreateKeyPool(context.Background(), req)
							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("When the handler group DAO returns nil", func() {
						handlerGroupDAO.On("GetKeyHandlerGroup", handlerGroupID).Return(nil, nil)

						Convey("Then we should return an error", func() {
							_, err := api.CreateKeyPool(context.Background(), req)
							test.AssertTwirpError(err, twirp.InvalidArgument)
						})
					})

					Convey("When the handler group DAO returns", func() {
						handlerGroupDAO.On("GetKeyHandlerGroup", handlerGroupID).Return(&key_handler_group.KeyHandlerGroup{
							HandlerGroupID: handlerGroupID,
						}, nil)

						ctx := context.Background()
						newKeyPoolItem := &key_pool.KeyPool{
							PoolID:         poolID,
							ProductType:    productType,
							SKU:            sku,
							KeyPoolStatus:  status.KeyPoolStatusActive,
							Description:    desc,
							HandlerGroupID: handlerGroupID,
							StartDate:      startDateTime,
							EndDate:        endDateTime,
							RedemptionMode: "REDEEM_BY_KEY",
						}

						Convey("When the database fails to write the new key pool", func() {
							keyPoolDAO.On("CreateNew", newKeyPoolItem).Return(errors.New("WALRUS STRIKE"))

							Convey("Then we should return an error", func() {
								_, err := api.CreateKeyPool(ctx, req)

								test.AssertTwirpError(err, twirp.Internal)
							})
						})

						Convey("When the database returns that the item already exists", func() {
							keyPoolDAO.On("CreateNew", newKeyPoolItem).Return(aws_test.NewAwsErr("ConditionalCheckFailedException"))

							Convey("Then we should return an error", func() {
								_, err := api.CreateKeyPool(ctx, req)

								test.AssertTwirpError(err, twirp.FailedPrecondition)
							})
						})

						Convey("When we succeed in writing the new key pool to the database", func() {
							keyPoolDAO.On("CreateNew", newKeyPoolItem).Return(nil)

							Convey("Then we should return a success", func() {
								resp, err := api.CreateKeyPool(ctx, req)

								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)
							})
						})
					})
				})
			})
		})
	})
}
