package get_key_pool_report_download_info

import (
	"context"
	"encoding/base64"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/backend/worker/report_generation"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO       report.DAO     `inject:""`
	Auditor   audit.Auditor  `inject:""`
	KMSClient kms.Client     `inject:""`
	S3Client  s3.Client      `inject:""`
	Config    *config.Config `inject:""`
}

const ExpiryTime = 5 * time.Minute

func (api *API) GetKeyPoolReportDownloadInfo(ctx context.Context, req *phanto.GetKeyPoolReportDownloadInfoReq) (resp *phanto.GetKeyPoolReportDownloadInfoResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("ReportID", "Request body cannot be empty")
	} else if strings.Blank(req.ReportId) {
		return nil, twirp.InvalidArgumentError("ReportID", "Report ID cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyPoolReportDownloadInfoOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalError("Downstream dependency error getting key pool report download info")
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	rep, err := api.DAO.GetReport(req.ReportId)
	if err != nil {
		log.WithField("ReportID", req.ReportId).WithError(err).Error("error getting report")
		return nil, twirp.InternalErrorWith(err)
	}

	if rep == nil {
		return nil, twirp.InvalidArgumentError("ReportID", "Report does not exist")
	}

	if rep.ReportStatus != report.ReportStatusFinished {
		return nil, twirp.InvalidArgumentError("ReportID", "Report not in correct status")
	}

	decryptOut, err := api.KMSClient.Decrypt(rep.Cipher, map[string]string{
		"report_id": rep.ReportID,
	})
	if err != nil {
		log.WithError(err).Error("Error decrypting cipher from report")
		return nil, twirp.InternalError("Downstream dependency error getting key pool report download info")
	}
	if decryptOut == nil {
		log.WithError(err).Error("KMS returned nil response decrypting report cipher")
		return nil, twirp.InternalError("Downstream dependency error getting key pool report download info")
	}

	url, err := api.S3Client.GetTempSignedURL(ctx, api.Config.ReportsS3Bucket, report_generation.GetEncryptedS3Key(rep.ReportID), ExpiryTime)
	if err != nil {
		log.WithError(err).Error("Error getting temp url from S3")
		return nil, twirp.InternalError("Downstream dependency error getting key pool report download info")
	}

	return &phanto.GetKeyPoolReportDownloadInfoResp{
		DecryptionKey: base64.StdEncoding.EncodeToString(decryptOut.Plaintext),
		DownloadUrl:   url,
	}, nil
}
