package get_key_pool_report_download_info_test

import (
	"context"
	"encoding/base64"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_report_download_info"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	dynamo_report "code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/backend/worker/report_generation"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/kms"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/rpc"
	aws_kms "github.com/aws/aws-sdk-go/service/kms"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAPI_GetKeyPoolReportDownloadInfo(t *testing.T) {
	Convey("Given an API struct", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		s3Client := new(s3_mock.Client)
		kmsClient := new(kms_mock.Client)
		auditor := new(audit_mock.Auditor)
		reportDAO := new(report_mock.DAO)

		api := &get_key_pool_report_download_info.API{
			Config:    cfg,
			KMSClient: kmsClient,
			S3Client:  s3Client,
			Auditor:   auditor,
			DAO:       reportDAO,
		}

		reportID := "test-report-id"
		cipher := []byte("test-cipher-12345")
		plaintext := []byte("test-plaintext-12345")
		s3URL := "http://test-url.s3.com?signature=testsignature"

		Convey("Errors when request is nil", func() {
			_, err := api.GetKeyPoolReportDownloadInfo(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		ctx := context.Background()

		Convey("Errors when request report id is blank", func() {
			_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
				ReportId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
				ReportId: reportID,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when report dao errors", func() {
				reportDAO.On("GetReport", reportID).Return(nil, errors.New("test error"))
				_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
					ReportId: reportID,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when report dao returns nil report", func() {
				reportDAO.On("GetReport", reportID).Return(nil, nil)
				_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
					ReportId: reportID,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when report dao returns a report in the wrong status", func() {
				report := &dynamo_report.Report{
					ReportID:     reportID,
					Cipher:       cipher,
					ReportStatus: dynamo_report.ReportStatusInProgress,
				}
				reportDAO.On("GetReport", reportID).Return(report, nil)

				_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
					ReportId: reportID,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("When report dao returns a valid report", func() {
				report := &dynamo_report.Report{
					ReportID:     reportID,
					Cipher:       cipher,
					ReportStatus: dynamo_report.ReportStatusFinished,
				}
				reportDAO.On("GetReport", reportID).Return(report, nil)

				Convey("Errors when kms decrypt errors", func() {
					kmsClient.On("Decrypt", cipher, map[string]string{
						"report_id": reportID,
					}).Return(nil, errors.New("test error"))
					_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
						ReportId: reportID,
					})
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when kms decrypt returns nil resp", func() {
					kmsClient.On("Decrypt", cipher, map[string]string{
						"report_id": reportID,
					}).Return(nil, nil)
					_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
						ReportId: reportID,
					})
					So(err, ShouldNotBeNil)
				})

				Convey("When kms decrypt returns resp", func() {
					kmsClient.On("Decrypt", cipher, map[string]string{
						"report_id": reportID,
					}).Return(&aws_kms.DecryptOutput{
						Plaintext: plaintext,
					}, nil)

					Convey("Errors when S3 url gen errors", func() {
						s3Client.On("GetTempSignedURL", mock.Anything, cfg.ReportsS3Bucket, report_generation.GetEncryptedS3Key(reportID), get_key_pool_report_download_info.ExpiryTime).Return("", errors.New("test error"))
						_, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
							ReportId: reportID,
						})
						So(err, ShouldNotBeNil)
					})

					Convey("Succeeds when S3 url gen returns url", func() {
						s3Client.On("GetTempSignedURL", mock.Anything, cfg.ReportsS3Bucket, report_generation.GetEncryptedS3Key(reportID), get_key_pool_report_download_info.ExpiryTime).Return(s3URL, nil)
						resp, err := api.GetKeyPoolReportDownloadInfo(ctx, &phanto.GetKeyPoolReportDownloadInfoReq{
							ReportId: reportID,
						})
						So(err, ShouldBeNil)
						So(resp.DownloadUrl, ShouldEqual, s3URL)
						So(resp.DecryptionKey, ShouldEqual, base64.StdEncoding.EncodeToString(plaintext))
					})
				})
			})
		})
	})
}
