package generate_key_batch_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/generate_key_batch"
	"code.justin.tv/commerce/phanto/backend/clients/datascience"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/config"
	sns_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	audit_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	key_batch_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	key_pool_mock "code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPIImpl_GenerateKeyBatch(t *testing.T) {
	Convey("Given an API struct", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		snsClient := new(sns_mock.Client)
		keyPoolDAO := new(key_pool_mock.DAO)
		keyBatchDAO := new(key_batch_mock.DAO)
		auditor := new(audit_mock.Auditor)
		spade := datascience.NewSpadeNoopClient()

		api := &generate_key_batch.API{
			Config:      cfg,
			SNSClient:   snsClient,
			KeyPoolDAO:  keyPoolDAO,
			KeyBatchDAO: keyBatchDAO,
			Auditor:     auditor,
			SpadeClient: spade,
		}

		Convey("Errors when given a nil request", func() {
			_, err := api.GenerateKeyBatch(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when BatchID is blank", func() {
			_, err := api.GenerateKeyBatch(context.Background(), &phanto.GenerateKeyBatchReq{
				BatchId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when PoolID is blank", func() {
			_, err := api.GenerateKeyBatch(context.Background(), &phanto.GenerateKeyBatchReq{
				BatchId: "test-batch-id",
				PoolId:  "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when NumberOfKeys is zero", func() {
			_, err := api.GenerateKeyBatch(context.Background(), &phanto.GenerateKeyBatchReq{
				BatchId:      "test-batch-id",
				PoolId:       "test-pool-id",
				NumberOfKeys: 0,
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.GenerateKeyBatchReq{
				BatchId:      "test-batch-id",
				PoolId:       "test-pool-id",
				NumberOfKeys: 10,
			}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err = api.GenerateKeyBatch(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when KeyBatchDAO returns an error", func() {
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(nil, errors.New("test error"))
					_, err = api.GenerateKeyBatch(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Errors when KeyBatchDAO returns non-matching batch", func() {
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(&key_batch.KeyBatch{
						BatchID:      req.BatchId,
						PoolID:       "test-pool-id-2",
						NumberOfKeys: req.NumberOfKeys,
					}, nil)
					_, err = api.GenerateKeyBatch(context.Background(), req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})

				Convey("Returns early when KeyBatchDAO returns matching batch", func() {
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(&key_batch.KeyBatch{
						BatchID:      req.BatchId,
						PoolID:       req.PoolId,
						NumberOfKeys: req.NumberOfKeys,
					}, nil)
					resp, err := api.GenerateKeyBatch(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp.BatchId, ShouldEqual, req.BatchId)
				})

				Convey("When KeyBatchDAO returns nil", func() {
					keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(nil, nil)

					Convey("Errors when KeyPoolDAO returns an error", func() {
						keyPoolDAO.On("GetKeyPool", req.PoolId).Return(nil, errors.New("test error"))
						_, err = api.GenerateKeyBatch(context.Background(), req)
						test.AssertTwirpError(err, twirp.Internal)
					})

					Convey("Errors when KeyPoolDAO returns nil pool", func() {
						keyPoolDAO.On("GetKeyPool", req.PoolId).Return(nil, nil)
						_, err = api.GenerateKeyBatch(context.Background(), req)
						test.AssertTwirpError(err, twirp.InvalidArgument)
					})

					Convey("When KeyPoolDAO returns a valid pool", func() {
						pool := &key_pool.KeyPool{
							PoolID: "test-pool-id",
						}
						keyPoolDAO.On("GetKeyPool", req.PoolId).Return(pool, nil)

						Convey("Errors when KeyBatchDAO CreateNew errors", func() {
							keyBatchDAO.On("CreateNew", mock.Anything).Return(errors.New("test error"))
							_, err = api.GenerateKeyBatch(context.Background(), req)
							test.AssertTwirpError(err, twirp.Internal)
						})

						Convey("When KeyBatchDAO CreateNewSucceeds", func() {
							keyBatchDAO.On("CreateNew", mock.Anything).Return(nil)

							Convey("Errors when SNSClient publish errors", func() {
								snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
								_, err = api.GenerateKeyBatch(context.Background(), req)
								test.AssertTwirpError(err, twirp.Internal)
							})

							Convey("Succeeds when SNSClient publish succeeds", func() {
								snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)
								resp, err := api.GenerateKeyBatch(context.Background(), req)
								So(err, ShouldBeNil)
								So(resp.BatchId, ShouldEqual, req.BatchId)
							})
						})
					})
				})
			})
		})
	})
}
