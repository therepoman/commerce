package generate_key_batch

import (
	"context"
	"encoding/json"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/clients/datascience"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/backend/worker/key_generation"
	"code.justin.tv/commerce/phanto/config"
	phanto "code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	Config      *config.Config          `inject:""`
	SNSClient   sns.Client              `inject:""`
	KeyPoolDAO  key_pool.DAO            `inject:""`
	KeyBatchDAO key_batch.DAO           `inject:""`
	Auditor     audit.Auditor           `inject:""`
	SpadeClient datascience.SpadeClient `inject:""`
}

func (api *API) GenerateKeyBatch(ctx context.Context, req *phanto.GenerateKeyBatchReq) (resp *phanto.GenerateKeyBatchResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("BatchID", "Request body cannot be empty")
	} else if strings.Blank(req.BatchId) {
		return nil, twirp.InvalidArgumentError("BatchID", "BatchID cannot be blank")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "PoolID cannot be blank")
	} else if req.NumberOfKeys <= 0 {
		return nil, twirp.InvalidArgumentError("NumberOfKeys", "NumberOfKeys must be a positive integer")
	}

	req.BatchId = go_strings.TrimSpace(req.BatchId)
	req.PoolId = go_strings.TrimSpace(req.PoolId)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GenerateKeyBatchTypeOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	keyBatch, err := api.KeyBatchDAO.GetKeyBatch(req.BatchId)
	if err != nil {
		log.WithError(err).Error("Error getting KeyBatch from dynamo")
		return nil, twirp.InternalError("Downstream dependency error generating KeyBatch")
	}

	if keyBatch != nil {
		if keyBatch.PoolID == req.PoolId && keyBatch.NumberOfKeys == req.NumberOfKeys {
			return &phanto.GenerateKeyBatchResp{
				BatchId: req.BatchId,
			}, nil
		}
		return nil, twirp.InvalidArgumentError("BatchID", "Existing batch with different values")
	}

	keyPool, err := api.KeyPoolDAO.GetKeyPool(req.PoolId)
	if err != nil {
		log.WithError(err).Error("Error getting KeyPool from dynamo")
		return nil, twirp.InternalError("Downstream dependency error generating KeyBatch")
	}

	if keyPool == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "KeyPool does not exist for PoolID")
	}

	batch := &key_batch.KeyBatch{
		BatchID:                req.BatchId,
		PoolID:                 req.PoolId,
		KeyBatchStatus:         status.KeyBatchStatusInactive,
		KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusCreated,
		NumberOfKeys:           req.NumberOfKeys,
		Progress:               0,
	}
	err = api.KeyBatchDAO.CreateNew(batch)
	if err != nil {
		log.WithError(err).Error("Error creating KeyBatch in dynamo")
		return nil, twirp.InternalError("Downstream dependency error generating KeyBatch")
	}

	keyGenerationJob := key_generation.Job{
		BatchID: req.BatchId,
	}

	jobBytes, err := json.Marshal(&keyGenerationJob)
	if err != nil {
		log.WithError(err).Error("Error converting KeyGenerationJob to JSON")
		return nil, twirp.InternalError("Downstream dependency error generating KeyBatch")
	}

	ldapUser, _ := ctx.Value(middleware.LDAPUserContextKey).(string)
	api.SpadeClient.SendPhantoGenerateKeyBatchEvent(datascience.PhantoGenerateKeyBatchEvent{
		Environment:            api.Config.EnvironmentName,
		KeyPoolID:              keyPool.PoolID,
		ProductType:            keyPool.ProductType,
		SKU:                    keyPool.SKU,
		RedemptionMode:         keyPool.RedemptionMode,
		Description:            keyPool.Description,
		HandlerGroupID:         keyPool.HandlerGroupID,
		LegacyHandler:          keyPool.Handler,
		KeyPoolStartDate:       keyPool.StartDate,
		KeyPoolEndDate:         keyPool.EndDate,
		KeyBatchID:             batch.BatchID,
		KeyBatchKeyCount:       batch.NumberOfKeys,
		KeyBatchWorkflowStatus: string(batch.KeyBatchWorkflowStatus),
		KeyBatchStatus:         string(batch.KeyBatchStatus),
		LDAPUser:               ldapUser,
	})

	err = api.SNSClient.Publish(ctx, api.Config.KeyGenerationJobSNSTopic, string(jobBytes))
	if err != nil {
		log.WithError(err).Error("Error posting KeyBatch request to SNS")
		return nil, twirp.InternalError("Downstream dependency error generating KeyBatch")
	}

	return &phanto.GenerateKeyBatchResp{
		BatchId: req.BatchId,
	}, nil
}
