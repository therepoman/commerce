package get_key_batches_s2s

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyBatchDAO key_batch.DAO `inject:""`
	Auditor     audit.Auditor `inject:""`
}

func (api *API) GetKeyBatches(ctx context.Context, req *phanto.GetKeyBatchesReq) (resp *phanto.GetKeyBatchesResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "Request body cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "Pool id cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyBatchesS2SOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoBatches, newCursor, err := api.KeyBatchDAO.GetKeyBatchesByPool(req.PoolId, req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key batches from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key batches")
	}

	respBatches := key_batch.ToTwirpModel(dynamoBatches)
	return &phanto.GetKeyBatchesResp{
		KeyBatches: respBatches,
		Cursor:     newCursor,
	}, nil
}
