package get_key_batches_s2s_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_batches_s2s"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI_GetKeyBatches(t *testing.T) {
	Convey("Given an API struct", t, func() {
		keyBatchDAO := new(key_batch_mock.DAO)
		auditor := new(audit_mock.Auditor)

		api := &get_key_batches_s2s.API{
			KeyBatchDAO: keyBatchDAO,
			Auditor:     auditor,
		}

		poolID := "test-pool-id"
		cursor := ""

		Convey("Errors when given a nil request", func() {
			_, err := api.GetKeyBatches(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when pool id is blank", func() {
			_, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
				PoolId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
				PoolId: poolID,
				Cursor: cursor,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when key batch DAO errors", func() {
				keyBatchDAO.On("GetKeyBatchesByPool", poolID, cursor).Return(nil, "", errors.New("test error"))
				_, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
					PoolId: poolID,
					Cursor: cursor,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when key batch DAO returns no batches", func() {
				keyBatchDAO.On("GetKeyBatchesByPool", poolID, cursor).Return([]*key_batch.KeyBatch{}, "", nil)
				resp, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
					PoolId: poolID,
					Cursor: cursor,
				})
				So(err, ShouldBeNil)
				So(len(resp.KeyBatches), ShouldEqual, 0)
				So(resp.Cursor, ShouldEqual, "")
			})

			Convey("Succeeds when key batch DAO returns one batch", func() {
				keyBatchDAO.On("GetKeyBatchesByPool", poolID, cursor).Return([]*key_batch.KeyBatch{{BatchID: "B1"}}, "B1", nil)
				resp, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
					PoolId: poolID,
					Cursor: cursor,
				})
				So(err, ShouldBeNil)
				So(len(resp.KeyBatches), ShouldEqual, 1)
				So(resp.KeyBatches[0].BatchId, ShouldEqual, "B1")
				So(resp.Cursor, ShouldEqual, "B1")
			})

			Convey("Succeeds when key batch DAO returns two batches", func() {
				keyBatchDAO.On("GetKeyBatchesByPool", poolID, cursor).Return([]*key_batch.KeyBatch{{BatchID: "B1"}, {BatchID: "B2"}}, "B2", nil)
				resp, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
					PoolId: poolID,
					Cursor: cursor,
				})
				So(err, ShouldBeNil)
				So(len(resp.KeyBatches), ShouldEqual, 2)
				So(resp.KeyBatches[0].BatchId, ShouldEqual, "B1")
				So(resp.KeyBatches[1].BatchId, ShouldEqual, "B2")
				So(resp.Cursor, ShouldEqual, "B2")
			})

			Convey("Succeeds when key batch DAO is called with a cursor", func() {
				cursor = "B10"
				keyBatchDAO.On("GetKeyBatchesByPool", poolID, cursor).Return([]*key_batch.KeyBatch{{BatchID: "B11"}}, "B11", nil)
				resp, err := api.GetKeyBatches(context.Background(), &phanto.GetKeyBatchesReq{
					PoolId: poolID,
					Cursor: cursor,
				})
				So(err, ShouldBeNil)
				So(len(resp.KeyBatches), ShouldEqual, 1)
				So(resp.KeyBatches[0].BatchId, ShouldEqual, "B11")
				So(resp.Cursor, ShouldEqual, "B11")
			})
		})
	})
}
