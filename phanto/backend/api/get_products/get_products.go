package get_products

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	ProductDAO product.DAO   `inject:""`
	Auditor    audit.Auditor `inject:""`
}

func (api *API) GetProducts(ctx context.Context, req *phanto.GetProductsReq) (resp *phanto.GetProductsResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("ProductType", "Request body cannot be empty")
	} else if strings.Blank(req.ProductType) {
		return nil, twirp.InvalidArgumentError("ProductType", "Product type cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetProductsOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	products, cursor, err := api.ProductDAO.GetProducts(req.ProductType, req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting products from dynamo")
		return nil, twirp.InternalError("Error getting products")
	}

	return &phanto.GetProductsResp{
		Products: product.ToTwirpModel(products),
		Cursor:   cursor,
	}, nil
}
