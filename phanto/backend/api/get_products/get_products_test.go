package get_products_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_products"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetProducts(t *testing.T) {
	Convey("Given a Get All Product Types API", t, func() {
		productDAO := new(product_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := get_products.API{
			ProductDAO: productDAO,
			Auditor:    auditor,
		}

		cursor := "test-cursor"
		productType := "test-product-type"

		Convey("Errors when called with a nil request", func() {
			_, err := api.GetProducts(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when called with a blank product type", func() {
			_, err := api.GetProducts(context.Background(), &phanto.GetProductsReq{
				ProductType: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetProducts(context.Background(), &phanto.GetProductsReq{
				ProductType: productType,
				Cursor:      cursor,
			})
			test.AssertTwirpError(err, twirp.Internal)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when DAO errors", func() {
				productDAO.On("GetProducts", productType, cursor).Return(nil, "", errors.New("test error"))
				_, err := api.GetProducts(context.Background(), &phanto.GetProductsReq{
					ProductType: productType,
					Cursor:      cursor,
				})
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("Succeeds when DAO succeeds", func() {
				newCursor := "new-test-cursor"
				productDAO.On("GetProducts", productType, cursor).Return([]*product.Product{
					{ProductType: "pt", SKU: "s1"},
					{ProductType: "pt", SKU: "s2"},
				}, newCursor, nil)
				resp, err := api.GetProducts(context.Background(), &phanto.GetProductsReq{
					ProductType: productType,
					Cursor:      cursor,
				})
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Cursor, ShouldEqual, newCursor)
				So(resp.Products[0].Sku, ShouldEqual, "s1")
				So(resp.Products[1].Sku, ShouldEqual, "s2")
			})
		})
	})
}
