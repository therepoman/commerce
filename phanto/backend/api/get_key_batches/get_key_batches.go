package get_key_batches

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/authorization"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolDAO  key_pool.DAO                    `inject:""`
	KeyBatchDAO key_batch.DAO                   `inject:""`
	Authorizer  authorization.HandlerAuthorizer `inject:""`
	Auditor     audit.Auditor                   `inject:""`
}

func (api *API) GetKeyBatches(ctx context.Context, req *phanto.GetKeyBatchesReq) (resp *phanto.GetKeyBatchesResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "Request body cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "Pool id cannot be blank")
	}

	keyPool, err := api.KeyPoolDAO.GetKeyPool(req.PoolId)
	if err != nil {
		log.WithField("pool_id", req.PoolId).WithError(err).Error("Error getting key pool from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key batches")
	}

	if keyPool == nil {
		log.WithField("pool_id", req.PoolId).Error("Key pool not found in dynamo")
		return nil, twirp.InvalidArgumentError("PoolID", "Key pool does not exist")
	}

	if !api.Authorizer.AuthorizeByTUID(ctx, keyPool) {
		return nil, twirp.NewError(twirp.PermissionDenied, "user does not have permission to get key batches")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyBatchesOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoBatches, newCursor, err := api.KeyBatchDAO.GetKeyBatchesByPool(req.PoolId, req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key batches from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key batches")
	}

	respBatches := key_batch.ToTwirpModel(dynamoBatches)
	return &phanto.GetKeyBatchesResp{
		KeyBatches: respBatches,
		Cursor:     newCursor,
	}, nil
}
