package get_key_batch_status

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolDAO  key_pool.DAO  `inject:""`
	KeyBatchDAO key_batch.DAO `inject:""`
	Auditor     audit.Auditor `inject:""`
}

func (api *API) GetKeyBatchStatus(ctx context.Context, req *phanto.GetKeyBatchStatusReq) (resp *phanto.GetKeyBatchStatusResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("BatchID", "Request body cannot be empty")
	} else if strings.Blank(req.BatchId) {
		return nil, twirp.InvalidArgumentError("BatchID", "BatchID cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyBatchStatusOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	batch, err := api.KeyBatchDAO.GetKeyBatch(req.BatchId)
	if err != nil {
		log.WithError(err).Error("Error getting KeyBatch from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting KeyBatch status")
	}

	if batch == nil {
		return nil, twirp.InvalidArgumentError("BatchID", "No batch found")
	}

	return &phanto.GetKeyBatchStatusResp{
		WorkflowStatus: batch.KeyBatchWorkflowStatus.String(),
		Status:         batch.KeyBatchStatus.String(),
		Progress:       batch.Progress,
	}, nil
}
