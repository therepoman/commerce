package get_key_batch_status_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_batch_status"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPIImpl_GenerateKeyBatch(t *testing.T) {
	Convey("Given an API struct", t, func() {
		keyPoolDAO := new(key_pool_mock.DAO)
		keyBatchDAO := new(key_batch_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &get_key_batch_status.API{
			KeyPoolDAO:  keyPoolDAO,
			KeyBatchDAO: keyBatchDAO,
			Auditor:     auditor,
		}

		Convey("Errors when given a nil request", func() {
			_, err := api.GetKeyBatchStatus(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when BatchID is blank", func() {
			_, err := api.GetKeyBatchStatus(context.Background(), &phanto.GetKeyBatchStatusReq{
				BatchId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)

			Convey("Given a valid request", func() {
				req := &phanto.GetKeyBatchStatusReq{
					BatchId: "test-batch-id",
				}

				Convey("Errors when the auditor returns an error", func() {
					auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
					_, err = api.GetKeyBatchStatus(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("When the auditor succeeds", func() {
					auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
					auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

					Convey("Errors when KeyBatchDAO returns an error", func() {
						keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(nil, errors.New("test error"))
						_, err = api.GetKeyBatchStatus(context.Background(), req)
						test.AssertTwirpError(err, twirp.Internal)
					})

					Convey("Errors when KeyBatchDAO returns nil batch", func() {
						keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(nil, nil)
						_, err = api.GetKeyBatchStatus(context.Background(), req)
						test.AssertTwirpError(err, twirp.InvalidArgument)
					})

					Convey("Succeeds when KeyBatchDAO returns batch", func() {
						keyBatchDAO.On("GetKeyBatch", req.BatchId).Return(&key_batch.KeyBatch{
							BatchID:                req.BatchId,
							Progress:               15.5,
							KeyBatchWorkflowStatus: status.KeyBatchWorkflowStatusInProgress,
							KeyBatchStatus:         status.KeyBatchStatusInactive,
						}, nil)
						resp, err := api.GetKeyBatchStatus(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, status.KeyBatchStatusInactive.String())
						So(resp.WorkflowStatus, ShouldEqual, status.KeyBatchWorkflowStatusInProgress.String())
						So(resp.Progress, ShouldEqual, 15.5)
					})
				})
			})
		})
	})
}
