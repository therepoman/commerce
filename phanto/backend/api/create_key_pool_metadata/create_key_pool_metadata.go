package create_key_pool_metadata

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolMetadataDAO key_pool_metadata.DAO `inject:""`
	Auditor            audit.Auditor         `inject:""`
}

func (api *API) CreateKeyPoolMetadata(ctx context.Context, req *phanto.CreateKeyPoolMetadataReq) (resp *phanto.CreateKeyPoolMetadataResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolId", "request cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolId", "pool id cannot be blank")
	} else if strings.Blank(req.MetadataKey) {
		return nil, twirp.InvalidArgumentError("MetadataKey", "metadata key cannot be blank")
	} else if strings.Blank(req.MetadataValue) {
		return nil, twirp.InvalidArgumentError("MetadataValue", "metadata value cannot be blank")
	}

	req.MetadataKey = go_strings.TrimSpace(req.MetadataKey)
	req.MetadataValue = go_strings.TrimSpace(req.MetadataValue)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.CreateKeyPoolMetadataOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	err = api.KeyPoolMetadataDAO.CreateNew(&key_pool_metadata.KeyPoolMetadata{
		PoolID:        req.PoolId,
		MetadataKey:   req.MetadataKey,
		MetadataValue: req.MetadataValue,
	})
	if err != nil {
		log.WithError(err).Error("error creating new key pool metadata")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.CreateKeyPoolMetadataResp{}, nil
}
