package create_key_pool_metadata_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/create_key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_CreateKeyPoolMetadata(t *testing.T) {
	Convey("Given an API", t, func() {
		keyPoolMetadataDAO := new(key_pool_metadata_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &create_key_pool_metadata.API{
			KeyPoolMetadataDAO: keyPoolMetadataDAO,
			Auditor:            auditor,
		}

		Convey("Errors when request is nil", func() {
			_, err := api.CreateKeyPoolMetadata(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when request poolID is blank", func() {
			_, err := api.CreateKeyPoolMetadata(context.Background(), &phanto.CreateKeyPoolMetadataReq{
				PoolId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when request metadata key is blank", func() {
			_, err := api.CreateKeyPoolMetadata(context.Background(), &phanto.CreateKeyPoolMetadataReq{
				PoolId:      "test-pool-id",
				MetadataKey: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when request metadata value is blank", func() {
			_, err := api.CreateKeyPoolMetadata(context.Background(), &phanto.CreateKeyPoolMetadataReq{
				PoolId:        "test-pool-id",
				MetadataKey:   "test-metadata-key",
				MetadataValue: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.CreateKeyPoolMetadataReq{
				PoolId:        "test-pool-id",
				MetadataKey:   "test-metadata-key",
				MetadataValue: "test-metadata-value",
			}

			Convey("Errors when auditor call fails", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.CreateKeyPoolMetadata(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When auditor call succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when DAO errors", func() {
					keyPoolMetadataDAO.On("CreateNew", mock.Anything).Return(errors.New("test-error"))
					_, err := api.CreateKeyPoolMetadata(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("When DAO succeeds", func() {
					keyPoolMetadataDAO.On("CreateNew", mock.Anything).Return(nil)
					resp, err := api.CreateKeyPoolMetadata(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
				})
			})
		})
	})
}
