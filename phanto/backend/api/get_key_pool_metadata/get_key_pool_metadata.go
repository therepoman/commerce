package get_key_pool_metadata

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolMetadataDAO key_pool_metadata.DAO `inject:""`
	Auditor            audit.Auditor         `inject:""`
}

func (api *API) GetKeyPoolMetadata(ctx context.Context, req *phanto.GetKeyPoolMetadataReq) (resp *phanto.GetKeyPoolMetadataResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "Request body cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "Pool id cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyPoolMetadataOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	dynamoPoolMetadata, newCursor, err := api.KeyPoolMetadataDAO.GetKeyPoolMetadata(req.PoolId, req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key pool metadata from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key pool metadata")
	}

	respKeyPoolMetadata := key_pool_metadata.ToTwirpModel(dynamoPoolMetadata)
	return &phanto.GetKeyPoolMetadataResp{
		KeyPoolMetadata: respKeyPoolMetadata,
		Cursor:          newCursor,
	}, nil
}
