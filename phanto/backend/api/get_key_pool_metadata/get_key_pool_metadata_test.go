package get_key_pool_metadata_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_pool_metadata"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyPoolMetadata(t *testing.T) {
	Convey("Given an API", t, func() {
		keyPoolMetadataDAO := new(key_pool_metadata_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &get_key_pool_metadata.API{
			KeyPoolMetadataDAO: keyPoolMetadataDAO,
			Auditor:            auditor,
		}

		Convey("Errors when request is nil", func() {
			_, err := api.GetKeyPoolMetadata(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when pool id is blank", func() {
			_, err := api.GetKeyPoolMetadata(context.Background(), &phanto.GetKeyPoolMetadataReq{
				PoolId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.GetKeyPoolMetadataReq{
				PoolId: "test-pool-id",
				Cursor: "test-cursor",
			}

			Convey("Errors when auditor errors", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.GetKeyPoolMetadata(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when DAO errors", func() {
					keyPoolMetadataDAO.On("GetKeyPoolMetadata", req.PoolId, req.Cursor).Return([]*key_pool_metadata.KeyPoolMetadata{}, "", errors.New("test-error"))
					_, err := api.GetKeyPoolMetadata(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Succeeds when DAO returns empty list", func() {
					keyPoolMetadataDAO.On("GetKeyPoolMetadata", req.PoolId, req.Cursor).Return([]*key_pool_metadata.KeyPoolMetadata{}, "", nil)
					resp, err := api.GetKeyPoolMetadata(context.Background(), req)
					So(err, ShouldBeNil)
					So(len(resp.KeyPoolMetadata), ShouldEqual, 0)
				})

				Convey("Succeeds when DAO returns list of one element", func() {
					keyPoolMetadataDAO.On("GetKeyPoolMetadata", req.PoolId, req.Cursor).Return([]*key_pool_metadata.KeyPoolMetadata{{MetadataKey: "k1", MetadataValue: "v1"}}, "", nil)
					resp, err := api.GetKeyPoolMetadata(context.Background(), req)
					So(err, ShouldBeNil)
					So(len(resp.KeyPoolMetadata), ShouldEqual, 1)
					So(resp.KeyPoolMetadata[0].MetadataKey, ShouldEqual, "k1")
					So(resp.KeyPoolMetadata[0].MetadataValue, ShouldEqual, "v1")
				})

				Convey("Succeeds when DAO returns list of two elements", func() {
					keyPoolMetadataDAO.On("GetKeyPoolMetadata", req.PoolId, req.Cursor).Return([]*key_pool_metadata.KeyPoolMetadata{{MetadataKey: "k1", MetadataValue: "v1"}, {MetadataKey: "k2", MetadataValue: "v2"}}, "c1", nil)
					resp, err := api.GetKeyPoolMetadata(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp.Cursor, ShouldEqual, "c1")
					So(len(resp.KeyPoolMetadata), ShouldEqual, 2)
					So(resp.KeyPoolMetadata[0].MetadataKey, ShouldEqual, "k1")
					So(resp.KeyPoolMetadata[0].MetadataValue, ShouldEqual, "v1")
					So(resp.KeyPoolMetadata[1].MetadataKey, ShouldEqual, "k2")
					So(resp.KeyPoolMetadata[1].MetadataValue, ShouldEqual, "v2")
				})
			})
		})
	})
}
