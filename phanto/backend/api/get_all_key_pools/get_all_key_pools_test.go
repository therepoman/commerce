package get_all_key_pools_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_all_key_pools"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetAllKeyPools(t *testing.T) {
	Convey("Given an API struct", t, func() {
		keyPoolDAO := new(key_pool_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &get_all_key_pools.API{
			KeyPoolDAO: keyPoolDAO,
			Auditor:    auditor,
		}

		Convey("Errors when given a nil request", func() {
			_, err := api.GetAllKeyPools(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the auditor returns an error", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			_, err := api.GetAllKeyPools(context.Background(), &phanto.GetAllKeyPoolsReq{})
			test.AssertTwirpError(err, twirp.Internal)
		})

		Convey("When the auditor succeeds", func() {
			auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
			auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when KeyPoolDAO returns an error", func() {
				keyPoolDAO.On("GetKeyPools", "").Return(nil, "", errors.New("test error"))
				_, err := api.GetAllKeyPools(context.Background(), &phanto.GetAllKeyPoolsReq{})
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when KeyPoolDAO returns empty", func() {
				keyPoolDAO.On("GetKeyPools", "").Return([]*key_pool.KeyPool{}, "", nil)
				resp, err := api.GetAllKeyPools(context.Background(), &phanto.GetAllKeyPoolsReq{})
				So(err, ShouldBeNil)
				So(len(resp.KeyPools), ShouldEqual, 0)
			})

			Convey("Succeeds when KeyPoolDAO returns single entry", func() {
				keyPoolDAO.On("GetKeyPools", "").Return([]*key_pool.KeyPool{{PoolID: "P1"}}, "P1", nil)
				resp, err := api.GetAllKeyPools(context.Background(), &phanto.GetAllKeyPoolsReq{})
				So(err, ShouldBeNil)
				So(len(resp.KeyPools), ShouldEqual, 1)
				So(resp.KeyPools[0].PoolId, ShouldEqual, "P1")
				So(resp.KeyPools[0].RedemptionMode, ShouldEqual, phanto.RedemptionMode_REDEEM_BY_KEY)
				So(resp.Cursor, ShouldEqual, "P1")
			})

			Convey("Succeeds when KeyPoolDAO returns two entries", func() {
				keyPoolDAO.On("GetKeyPools", "").Return([]*key_pool.KeyPool{{PoolID: "P1"}, {PoolID: "P2"}}, "P2", nil)
				resp, err := api.GetAllKeyPools(context.Background(), &phanto.GetAllKeyPoolsReq{})
				So(err, ShouldBeNil)
				So(len(resp.KeyPools), ShouldEqual, 2)
				So(resp.KeyPools[0].PoolId, ShouldEqual, "P1")
				So(resp.KeyPools[1].PoolId, ShouldEqual, "P2")
				So(resp.Cursor, ShouldEqual, "P2")
			})

			Convey("Succeeds when KeyPoolDAO is called with a cursor", func() {
				keyPoolDAO.On("GetKeyPools", "P25").Return([]*key_pool.KeyPool{{PoolID: "P26"}}, "P26", nil)
				resp, err := api.GetAllKeyPools(context.Background(), &phanto.GetAllKeyPoolsReq{
					Cursor: "P25",
				})
				So(err, ShouldBeNil)
				So(len(resp.KeyPools), ShouldEqual, 1)
				So(resp.KeyPools[0].PoolId, ShouldEqual, "P26")
				So(resp.Cursor, ShouldEqual, "P26")
			})
		})
	})
}
