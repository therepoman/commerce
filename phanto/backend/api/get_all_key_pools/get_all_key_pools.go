package get_all_key_pools

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyPoolDAO key_pool.DAO  `inject:""`
	Auditor    audit.Auditor `inject:""`
}

func (api *API) GetAllKeyPools(ctx context.Context, req *phanto.GetAllKeyPoolsReq) (resp *phanto.GetAllKeyPoolsResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Cursor", "Request body cannot be empty")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetAllKeyPoolsOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	keyPools, cursor, err := api.KeyPoolDAO.GetKeyPools(req.Cursor)
	if err != nil {
		log.WithError(err).Error("Error getting key pools from dynamo")
		return nil, twirp.InternalError("Error getting key pools")
	}

	return &phanto.GetAllKeyPoolsResp{
		Cursor:   cursor,
		KeyPools: key_pool.ToTwirpModel(keyPools),
	}, nil
}
