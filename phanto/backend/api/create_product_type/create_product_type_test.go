package create_product_type_test

import (
	"context"
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/create_product_type"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	aws_test "code.justin.tv/commerce/phanto/backend/dynamo/test"
	"code.justin.tv/commerce/phanto/backend/product/fulfillment"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_CreateProductType(t *testing.T) {
	Convey("Given a Create Product Type API", t, func() {
		dao := new(product_type_mock.DAO)
		snsClient := new(sns_mock.Client)
		auditor := new(audit_mock.Auditor)

		api := create_product_type.API{
			DAO:     dao,
			SNS:     snsClient,
			Auditor: auditor,
		}

		Convey("Given a nil request", func() {
			var req *phanto.CreateProductTypeReq = nil

			Convey("Then we should return an error", func() {
				_, err := api.CreateProductType(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank product type", func() {
			req := &phanto.CreateProductTypeReq{}

			Convey("Then we should return an error", func() {
				_, err := api.CreateProductType(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank sns topic", func() {
			req := &phanto.CreateProductTypeReq{
				ProductType: "walrus",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateProductType(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a valid creation request", func() {
			ctx := context.Background()

			productType := "walrus"
			snsTopic := "walrusTopic"

			req := &phanto.CreateProductTypeReq{
				ProductType: productType,
				SnsTopic:    snsTopic,
			}

			newDynamoProductType := &product_type.ProductType{
				ProductType: productType,
				SNSTopic:    snsTopic,
			}

			validationBody := &fulfillment.SNS{
				UserID: create_product_type.TestUser,
				SKU:    create_product_type.TestSKU,
			}

			validationBytes, _ := json.Marshal(validationBody)

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.CreateProductType(ctx, req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("When the SNS client fails to post a validation message", func() {
					snsClient.On("Publish", ctx, snsTopic, string(validationBytes)).Return(errors.New("WALRUS STRIKE"))

					Convey("Then we should return an error", func() {
						_, err := api.CreateProductType(ctx, req)

						test.AssertTwirpError(err, twirp.FailedPrecondition)
					})
				})

				Convey("when the SNS client succeeds to post a validation message", func() {
					snsClient.On("Publish", ctx, snsTopic, string(validationBytes)).Return(nil)

					Convey("When the database fails to write the new product type", func() {
						dao.On("CreateNew", newDynamoProductType).Return(errors.New("WALRUS STRIKE"))

						Convey("Then we should return an error", func() {
							_, err := api.CreateProductType(ctx, req)

							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("When the database returns that the item already exists", func() {
						dao.On("CreateNew", newDynamoProductType).Return(aws_test.NewAwsErr("ConditionalCheckFailedException"))

						Convey("Then we should return an error", func() {
							_, err := api.CreateProductType(ctx, req)

							test.AssertTwirpError(err, twirp.FailedPrecondition)
						})
					})

					Convey("When we succeed in writing the new product type to the database", func() {
						dao.On("CreateNew", newDynamoProductType).Return(nil)

						Convey("Then we should return a success", func() {
							resp, err := api.CreateProductType(ctx, req)

							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
						})
					})
				})
			})
		})
	})
}
