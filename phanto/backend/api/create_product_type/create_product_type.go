package create_product_type

import (
	"context"
	"encoding/json"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/backend/dynamo/utils"
	"code.justin.tv/commerce/phanto/backend/product/fulfillment"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	TestUser = "test_user"
	TestSKU  = "test_sku"
)

type API struct {
	DAO     product_type.DAO `inject:""`
	SNS     sns.Client       `inject:""`
	Auditor audit.Auditor    `inject:""`
}

func (api *API) CreateProductType(ctx context.Context, req *phanto.CreateProductTypeReq) (resp *phanto.CreateProductTypeResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("ProductType", "request cannot be nil")
	} else if strings.Blank(req.ProductType) {
		return nil, twirp.InvalidArgumentError("ProductType", "product type cannot be blank")
	} else if strings.Blank(req.SnsTopic) {
		return nil, twirp.InvalidArgumentError("SnsTopic", "sns topic cannot be blank")
	}

	req.ProductType = go_strings.TrimSpace(req.ProductType)
	req.SnsTopic = go_strings.TrimSpace(req.SnsTopic)
	req.TenantUrl = go_strings.TrimSpace(req.TenantUrl)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.CreateProductTypeOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	notificationBody := &fulfillment.SNS{
		UserID: TestUser,
		SKU:    TestSKU,
	}

	notificationBytes, err := json.Marshal(notificationBody)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	err = api.SNS.Publish(ctx, req.SnsTopic, string(notificationBytes))
	if err != nil {
		log.WithError(err).Info("could not publish to provided SNS topic on creation of product type")
		return nil, twirp.NewError(twirp.FailedPrecondition, "could not publish to provided SNS topic")
	}

	newProductType := &product_type.ProductType{
		ProductType: req.ProductType,
		SNSTopic:    req.SnsTopic,
		Description: req.Description,
		TenantURL:   req.TenantUrl,
	}

	err = api.DAO.CreateNew(newProductType)
	if utils.IsConditionalCheckErr(err) {
		return nil, twirp.NewError(twirp.FailedPrecondition, "product type already exists")
	}

	if err != nil {
		log.WithError(err).Error("could not create new product type")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.CreateProductTypeResp{}, nil
}
