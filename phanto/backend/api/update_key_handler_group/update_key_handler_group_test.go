package update_key_handler_group_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/update_key_handler_group"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAPI_UpdateKeyHandlerGroup(t *testing.T) {
	Convey("Given an update key pool API", t, func() {
		auditor := new(audit_mock.Auditor)
		dao := new(key_handler_group_mock.DAO)

		api := update_key_handler_group.API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Errors on a nil request", func() {
			_, err := api.UpdateKeyHandlerGroup(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank handler group ID", func() {
			_, err := api.UpdateKeyHandlerGroup(context.Background(), &phanto.UpdateKeyHandlerGroupsReq{
				HandlerGroupId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank description", func() {
			_, err := api.UpdateKeyHandlerGroup(context.Background(), &phanto.UpdateKeyHandlerGroupsReq{
				HandlerGroupId: "test-id",
				Description:    "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank tuid", func() {
			_, err := api.UpdateKeyHandlerGroup(context.Background(), &phanto.UpdateKeyHandlerGroupsReq{
				HandlerGroupId:  "test-id",
				Description:     "test description",
				AuthorizedTuids: []string{"1", " ", "2"},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on a blank client id", func() {
			_, err := api.UpdateKeyHandlerGroup(context.Background(), &phanto.UpdateKeyHandlerGroupsReq{
				HandlerGroupId:      "test-id",
				Description:         "test description",
				AuthorizedTuids:     []string{"1", "2"},
				AuthorizedClientIds: []string{"1", " ", "2"},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid request", func() {
			req := &phanto.UpdateKeyHandlerGroupsReq{
				HandlerGroupId:      "test-id",
				Description:         "test description",
				AuthorizedTuids:     []string{"1", "2"},
				AuthorizedClientIds: []string{"1", "2"},
			}

			Convey("Errors when auditing fails", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.UpdateKeyHandlerGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("When auditing succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when dynamo update errors", func() {
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					_, err := api.UpdateKeyHandlerGroup(context.Background(), req)
					So(err, ShouldNotBeNil)
				})

				Convey("When dynamo update succeeds", func() {
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)

					Convey("Succeed ", func() {
						_, err := api.UpdateKeyHandlerGroup(context.Background(), req)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
