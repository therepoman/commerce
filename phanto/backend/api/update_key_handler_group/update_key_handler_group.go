package update_key_handler_group

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_handler_group.DAO `inject:""`
	Auditor audit.Auditor         `inject:""`
}

func (api *API) UpdateKeyHandlerGroup(ctx context.Context, req *phanto.UpdateKeyHandlerGroupsReq) (resp *phanto.UpdateKeyHandlerGroupsResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("HandlerGroupID", "request cannot be nil")
	} else if strings.Blank(req.HandlerGroupId) {
		return nil, twirp.InvalidArgumentError("HandlerGroupID", "handler group id cannot be blank")
	} else if strings.Blank(req.Description) {
		return nil, twirp.InvalidArgumentError("Description", "description cannot be blank")
	}

	tuidMap := make(map[string]bool)
	for _, tuid := range req.AuthorizedTuids {
		tuid = go_strings.TrimSpace(tuid)
		if strings.Blank(tuid) {
			return nil, twirp.InvalidArgumentError("AuthorizedTUIDs", "tuid cannot be blank")
		}
		tuidMap[tuid] = true
	}

	clientIDMap := make(map[string]bool)
	for _, clientID := range req.AuthorizedClientIds {
		clientID = go_strings.TrimSpace(clientID)
		if strings.Blank(clientID) {
			return nil, twirp.InvalidArgumentError("AuthorizedClientIDs", "client id cannot be blank")
		}
		clientIDMap[clientID] = true
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.UpdateKeyHandlerGroup, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	err = api.DAO.CreateOrUpdate(&key_handler_group.KeyHandlerGroup{
		HandlerGroupID:      req.HandlerGroupId,
		Description:         req.Description,
		AuthorizedTUIDs:     tuidMap,
		AuthorizedClientIDs: clientIDMap,
	})
	if err != nil {
		log.WithError(err).Error("error getting key handler group from dynamo")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.UpdateKeyHandlerGroupsResp{}, nil
}
