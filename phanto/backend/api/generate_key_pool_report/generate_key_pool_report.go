package generate_key_pool_report

import (
	"context"
	"encoding/json"
	go_strings "strings"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/backend/worker/report_generation"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	Config    *config.Config `inject:""`
	ReportDAO report.DAO     `inject:""`
	Auditor   audit.Auditor  `inject:""`
	SNSClient sns.Client     `inject:""`
}

func (api *API) GenerateKeyPoolReport(ctx context.Context, req *phanto.GenerateKeyPoolReportReq) (resp *phanto.GenerateKeyPoolReportResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "Request body cannot be nil")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "Pool id cannot be blank")
	}

	req.PoolId = go_strings.TrimSpace(req.PoolId)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GenerateKeyPoolReportTypeOperation, Body: req})
	if err != nil {
		log.WithError(err).Error("error auditing operation")
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	reportUUID, err := uuid.NewV4()
	if err != nil {
		log.WithError(err).Error("error generating UUID")
		return nil, twirp.InternalErrorWith(err)
	}

	reportID := reportUUID.String()
	reportType := report.ReportTypeKeyPool
	ldapUser, _ := ctx.Value(middleware.LDAPUserContextKey).(string)

	err = api.ReportDAO.CreateOrUpdate(&report.Report{
		ReportID:     reportID,
		ReportType:   reportType,
		ReportStatus: report.ReportStatusRequested,
		PoolID:       req.PoolId,
		RequestedBy:  ldapUser,
		RequestedAt:  time.Now(),
	})
	if err != nil {
		log.WithError(err).Error("Error converting ReportGenerationJob to JSON")
		return nil, twirp.InternalError("Downstream dependency error generating key pool report")
	}

	reportGenerationJob := report_generation.Job{
		ReportID: reportID,
	}

	jobBytes, err := json.Marshal(&reportGenerationJob)
	if err != nil {
		log.WithError(err).Error("Error converting ReportGenerationJob to JSON")
		return nil, twirp.InternalError("Downstream dependency error generating key pool report")
	}

	err = api.SNSClient.Publish(ctx, api.Config.ReportGenerationJobSNSTopic, string(jobBytes))
	if err != nil {
		log.WithError(err).Error("Error publishing to SNS topic")
		return nil, twirp.InternalError("Downstream dependency error generating key pool report")
	}

	return &phanto.GenerateKeyPoolReportResp{
		ReportId: reportID,
		PoolId:   req.PoolId,
	}, nil
}
