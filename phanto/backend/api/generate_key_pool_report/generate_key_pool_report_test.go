package generate_key_pool_report_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/generate_key_pool_report"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/report"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GenerateKeyPoolReport(t *testing.T) {
	Convey("Given an API struct", t, func() {
		auditor := new(audit_mock.Auditor)
		reportDAO := new(report_mock.DAO)
		snsClient := new(sns_mock.Client)
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)

		api := &generate_key_pool_report.API{
			ReportDAO: reportDAO,
			Auditor:   auditor,
			SNSClient: snsClient,
			Config:    cfg,
		}

		Convey("Errors when request is nil", func() {
			_, err := api.GenerateKeyPoolReport(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when pool_id is blank", func() {
			_, err := api.GenerateKeyPoolReport(context.Background(), &phanto.GenerateKeyPoolReportReq{
				PoolId: "",
			})
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.GenerateKeyPoolReportReq{
				PoolId: "test-pool-id",
			}

			Convey("Errors when auditor errors", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.GenerateKeyPoolReport(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when report DAO update errors", func() {
					reportDAO.On("CreateOrUpdate", mock.Anything).Return(errors.New("test-error"))
					_, err := api.GenerateKeyPoolReport(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("When report DAO update succeeds", func() {
					reportDAO.On("CreateOrUpdate", mock.Anything).Return(nil)

					Convey("Errors when SNS publish errors", func() {
						snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test-error"))
						_, err := api.GenerateKeyPoolReport(context.Background(), req)
						test.AssertTwirpError(err, twirp.Internal)
					})

					Convey("Succeeds when SNS publish succeeds", func() {
						snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)
						resp, err := api.GenerateKeyPoolReport(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.PoolId, ShouldEqual, req.PoolId)
						So(resp.ReportId, ShouldNotBeBlank)
					})
				})
			})
		})
	})
}
