package get_key_handler_group

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_handler_group.DAO `inject:""`
	Auditor audit.Auditor         `inject:""`
}

func (api *API) GetKeyHandlerGroup(ctx context.Context, req *phanto.GetKeyHandlerGroupReq) (resp *phanto.GetKeyHandlerGroupResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("HandlerGroupID", "Request body cannot be nil")
	} else if strings.Blank(req.HandlerGroupId) {
		return nil, twirp.InvalidArgumentError("HandlerGroupID", "HandlerGroupID cannot be blank")
	}

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.GetKeyHandlerGroup, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	handlerGroup, err := api.DAO.GetKeyHandlerGroup(req.HandlerGroupId)
	if err != nil {
		log.WithError(err).Error("Error getting key handler group from dynamo")
		return nil, twirp.InternalError("Downstream dependency error getting key handler group")
	}

	if handlerGroup == nil {
		return nil, twirp.InvalidArgumentError("HandlerGroupID", "HandlerGroup not found with that id")
	}

	return &phanto.GetKeyHandlerGroupResp{
		KeyHandlerGroup: key_handler_group.ToKeyHandlerGroup(handlerGroup),
	}, nil
}
