package get_key_handler_group_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/get_key_handler_group"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_GetKeyHandlerGroups(t *testing.T) {
	Convey("Given an API struct", t, func() {
		dao := new(key_handler_group_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := &get_key_handler_group.API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Errors when given a nil request", func() {
			_, err := api.GetKeyHandlerGroup(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Errors when given a blank handler group_", func() {
			_, err := api.GetKeyHandlerGroup(context.Background(), nil)
			test.AssertTwirpError(err, twirp.InvalidArgument)
		})

		Convey("Given a valid request", func() {
			req := &phanto.GetKeyHandlerGroupReq{
				HandlerGroupId: "handler-group-id",
			}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.GetKeyHandlerGroup(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("Errors when DAO returns an error", func() {
					dao.On("GetKeyHandlerGroup", req.HandlerGroupId).Return(nil, errors.New("test error"))
					_, err := api.GetKeyHandlerGroup(context.Background(), req)
					test.AssertTwirpError(err, twirp.Internal)
				})

				Convey("Errors when DAO returns empty", func() {
					dao.On("GetKeyHandlerGroup", req.HandlerGroupId).Return(nil, nil)
					_, err := api.GetKeyHandlerGroup(context.Background(), req)
					test.AssertTwirpError(err, twirp.InvalidArgument)
				})

				Convey("Succeeds when DAO returns single entry", func() {
					dao.On("GetKeyHandlerGroup", req.HandlerGroupId).Return(&key_handler_group.KeyHandlerGroup{
						HandlerGroupID:      req.HandlerGroupId,
						Description:         "test-description",
						AuthorizedClientIDs: map[string]bool{"test-client-id": true},
						AuthorizedTUIDs:     map[string]bool{"test-tuid": true},
					}, nil)
					resp, err := api.GetKeyHandlerGroup(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp.KeyHandlerGroup.Description, ShouldEqual, "test-description")
					So(resp.KeyHandlerGroup.AuthorizedClientIds[0], ShouldEqual, "test-client-id")
					So(resp.KeyHandlerGroup.AuthorizedTuids[0], ShouldEqual, "test-tuid")
				})
			})
		})
	})
}
