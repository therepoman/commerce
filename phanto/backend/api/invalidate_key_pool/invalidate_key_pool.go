package invalidate_key_pool

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	DAO     key_pool.DAO  `inject:""`
	Auditor audit.Auditor `inject:""`
}

func (api *API) InvalidateKeyPool(ctx context.Context, req *phanto.InvalidateKeyPoolReq) (resp *phanto.InvalidateKeyPoolResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("PoolID", "Request body cannot be empty")
	} else if strings.Blank(req.PoolId) {
		return nil, twirp.InvalidArgumentError("PoolID", "missing pool ID to invalidate")
	}

	req.PoolId = go_strings.TrimSpace(req.PoolId)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.InvalidateKeyPoolOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	batch, err := api.DAO.GetKeyPool(req.PoolId)
	if err != nil {
		log.WithError(err).Error("downstream dependency error while getting pool")
		return nil, twirp.InternalError("downstream dependency error while getting pool")
	}

	batch.KeyPoolStatus = status.KeyPoolStatusInvalidated

	err = api.DAO.CreateOrUpdate(batch)
	if err != nil {
		log.WithError(err).Error("downstream dependency error while updating pool")
		return nil, twirp.InternalError("downstream dependency error while updating pool")
	}

	return &phanto.InvalidateKeyPoolResp{}, nil
}
