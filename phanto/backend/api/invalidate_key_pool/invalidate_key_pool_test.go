package invalidate_key_pool

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	"code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_InvalidateKeyPool(t *testing.T) {
	Convey("Given an Invalidate Key Batch API", t, func() {
		dao := new(key_pool_mock.DAO)
		auditor := new(audit_mock.Auditor)
		api := API{
			DAO:     dao,
			Auditor: auditor,
		}

		Convey("Given a nil request", func() {
			var req *phanto.InvalidateKeyPoolReq = nil

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeyPool(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request an empty batch ID", func() {
			req := &phanto.InvalidateKeyPoolReq{
				PoolId: "",
			}

			Convey("We should return an error", func() {
				_, err := api.InvalidateKeyPool(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a request with valid batch ID", func() {
			poolID := "asdfasdfasdf"

			req := &phanto.InvalidateKeyPoolReq{
				PoolId: poolID,
			}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.InvalidateKeyPool(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("When we error looking up the batch", func() {
					dao.On("GetKeyPool", poolID).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("We should return an error", func() {
						_, err := api.InvalidateKeyPool(context.Background(), req)
						test.AssertTwirpError(err, twirp.Internal)
					})
				})

				Convey("When we find batch in Dynamo", func() {
					productType := "productFoo"
					sku := "sku"

					dynamoKeyPool := &key_pool.KeyPool{
						PoolID:        poolID,
						KeyPoolStatus: status.KeyPoolStatusActive,
						ProductType:   productType,
						SKU:           sku,
					}

					dao.On("GetKeyPool", poolID).Return(dynamoKeyPool, nil)

					Convey("When we error on updating the batch", func() {
						dynamoKeyPool.KeyPoolStatus = status.KeyPoolStatusInvalidated
						dao.On("CreateOrUpdate", dynamoKeyPool).Return(errors.New("WALRUS STRIKE"))

						Convey("We should return an error", func() {
							_, err := api.InvalidateKeyPool(context.Background(), req)

							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("When we succeed on updating the batch", func() {
						dynamoKeyPool.KeyPoolStatus = status.KeyPoolStatusInvalidated
						dao.On("CreateOrUpdate", dynamoKeyPool).Return(nil)

						Convey("We should return success", func() {
							resp, err := api.InvalidateKeyPool(context.Background(), req)

							So(resp, ShouldNotBeNil)
							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
