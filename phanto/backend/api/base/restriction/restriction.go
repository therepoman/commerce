package restriction

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"code.justin.tv/commerce/phanto/backend/data"
	"github.com/twitchtv/twirp"
)

type Func func(ctx context.Context, c base.Container, claimData data.Validation) error

var NoRestriction = Func(func(ctx context.Context, c base.Container, claimData data.Validation) error { return nil })

func RestrictByProductType(requiredProductType string) Func {
	return Func(func(ctx context.Context, c base.Container, claimData data.Validation) error {
		if claimData.ProductType != requiredProductType {
			return twirp.NewError(twirp.FailedPrecondition, "Key does not belong to the required product type")
		}
		return nil
	})
}
