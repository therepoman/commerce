package authorization

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"github.com/twitchtv/twirp"
)

type Func func(ctx context.Context, c base.Container, userID string) error

var NoAuthorization = Func(func(ctx context.Context, c base.Container, userID string) error { return nil })

var UserMatchesRequestAuthenticatedUser = Func(func(ctx context.Context, c base.Container, userID string) error {
	if !c.Authorizer.AuthenticatedUserMatches(ctx, userID) {
		return twirp.NewError(twirp.PermissionDenied, "user not authorized")
	}
	return nil
})

var RequestIsAuthenticated = Func(func(ctx context.Context, c base.Container, userID string) error {
	if !c.Authorizer.RequestIsAuthenticated(ctx) {
		return twirp.NewError(twirp.PermissionDenied, "user not authorized")
	}
	return nil
})
