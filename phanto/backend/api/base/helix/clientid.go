package helix

import (
	"context"

	"code.justin.tv/commerce/phanto/backend/middleware"
)

const (
	TestClientID            = "7geyhz5dz87nibnfhi931iy3k7mxtn" // SephCodeRedemptionTest
	OperaEventClientID      = "xqdr3blb8nxv66bxvmg7je1x2c80da"
	GrabLabsClientID        = "yloy702gtxn2ihhjkgiz7yfg4xwxau"
	ForteClientID           = "kco9v6keam7b27l813oha0wpjgdvrr"
	EpicClientID            = "eqthe8by4jp92v9jfcbqkpp4hcwy7l"
	BlockbusterClientID     = "suon0jc21e0wvzv3q3zwsotnybel8w" // Glitchcon bits giveaway
	EMAPartnershipsClientID = "38vgexoqgqnr0n0ey7s6fa61kfdexg" // EMA Partnerships team
)

var whitelistedHelixClientIDs = map[string]interface{}{
	TestClientID:            nil,
	OperaEventClientID:      nil,
	GrabLabsClientID:        nil,
	ForteClientID:           nil,
	EpicClientID:            nil,
	BlockbusterClientID:     nil,
	EMAPartnershipsClientID: nil,
}

func IsWhitelistedHelixClient(ctx context.Context) bool {
	clientID, ok := ctx.Value(middleware.ClientIDContextKey).(string)
	if !ok {
		return false
	}
	_, ok = whitelistedHelixClientIDs[clientID]
	return ok
}
