package helix

import (
	"time"

	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/key/status"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/twitchtv/twirp"
)

func GetKeyStatus(isRedeemOperation bool, claimData *data.Validation, err error) phanto.KeyStatus {
	if err == nil && isRedeemOperation {
		return phanto.KeyStatus_SUCCESSFULLY_REDEEMED
	}

	if err != nil {
		knownErr, keyStatus := getKeyStatusFromError(err)
		if knownErr {
			return keyStatus
		}
	}

	if claimData != nil {
		return getKeyStatusFromClaimData(*claimData)
	}

	return phanto.KeyStatus_INTERNAL_ERROR
}

func getKeyStatusFromError(err error) (knownErr bool, keyStatus phanto.KeyStatus) {
	twirpErr, ok := err.(twirp.Error)
	if ok {
		switch twirpErr.Code() {
		case twirp.InvalidArgument:
			return true, phanto.KeyStatus_INCORRECT_FORMAT
		case twirp.NotFound:
			return true, phanto.KeyStatus_NOT_FOUND
		}
	}
	return false, keyStatus
}

func getKeyStatusFromClaimData(claimData data.Validation) phanto.KeyStatus {
	if claimData.KeyStatus == status.KeyStatusClaimed {
		return phanto.KeyStatus_ALREADY_CLAIMED
	} else if claimData.KeyStatus == status.KeyStatusIneligible {
		return phanto.KeyStatus_USER_NOT_ELIGIBLE
	} else if claimData.StartDate.After(time.Now()) || claimData.EndDate.Before(time.Now()) {
		return phanto.KeyStatus_EXPIRED
	} else if !claimData.IsValid {
		return phanto.KeyStatus_INACTIVE
	} else {
		return phanto.KeyStatus_UNUSED
	}
}
