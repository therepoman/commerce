package base

import (
	"code.justin.tv/commerce/phanto/backend/key/authorization"
	"code.justin.tv/commerce/phanto/backend/throttle"
)

type Container struct {
	Authorizer authorization.Authorizer `inject:""`
	Throttler  throttle.IThrottler      `inject:""`
}

type RedeemType string

const (
	RedeemType_StandardByKey   = RedeemType("StandardRedemption")
	RedeemType_LoadTestByKey   = RedeemType("LoadTestRedemption")
	RedeemType_HelixByKeyAPI   = RedeemType("HelixAPIRedemption")
	RedeemType_HelixByBatchAPI = RedeemType("HelixAPIByBatchRedemption")
)

var RedeemTypes = map[RedeemType]interface{}{
	RedeemType_StandardByKey:   nil,
	RedeemType_LoadTestByKey:   nil,
	RedeemType_HelixByKeyAPI:   nil,
	RedeemType_HelixByBatchAPI: nil,
}

func IsValidRedeemType(t RedeemType) bool {
	_, ok := RedeemTypes[t]
	return ok
}
