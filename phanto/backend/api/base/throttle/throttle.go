package throttle

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/base"
	"github.com/twitchtv/twirp"
)

const (
	ConfigContextKey = "throttle-config"
	ConfigDisabled   = "disabled"
)

type Func func(ctx context.Context, c base.Container, userID string) error

var NoThrottle = Func(func(ctx context.Context, c base.Container, userID string) error { return nil })

func ThrottleUserByKeyFunc(throttleKeyPrefix string) Func {
	return Func(func(ctx context.Context, c base.Container, userID string) error {
		if isThrottleDisabledRequest(ctx) {
			return nil
		}

		throttled, err := c.Throttler.Throttle(fmt.Sprintf("%s.%s", throttleKeyPrefix, userID), 1*time.Second)
		if err != nil {
			return twirp.NewError(twirp.Internal, err.Error())
		}

		if throttled {
			return twirp.NewError(twirp.ResourceExhausted, "Request throttled")
		}
		return nil
	})
}

func ThrottleUserExponentiallyByKeyFunc(throttleKeyPrefix string) Func {
	return Func(func(ctx context.Context, c base.Container, userID string) error {
		if isThrottleDisabledRequest(ctx) {
			return nil
		}

		throttleResponse, err := c.Throttler.ThrottleExponentially(fmt.Sprintf("%s.%s", throttleKeyPrefix, userID))
		if err != nil {
			return twirp.NewError(twirp.Internal, err.Error())
		}

		if throttleResponse.IsThrottled {
			return twirp.NewError(twirp.ResourceExhausted, "Request throttled")
		}
		return nil
	})
}

func isThrottleDisabledRequest(ctx context.Context) bool {
	cfg := ctx.Value(ConfigContextKey)
	val, ok := cfg.(string)
	return ok && val == ConfigDisabled
}
