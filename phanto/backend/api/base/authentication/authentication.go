package authentication

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/api/base"
	"github.com/twitchtv/twirp"
)

type Func func(ctx context.Context, c base.Container) (string, error)

var GetAuthenticatedUserFromContext = Func(func(ctx context.Context, c base.Container) (string, error) {
	authedUserID := c.Authorizer.GetAuthenticatedUser(ctx)

	if strings.Blank(authedUserID) {
		return "", twirp.NewError(twirp.PermissionDenied, "not authenticated")
	}
	return authedUserID, nil
})

func FixedUserAuthentication(authenticatedUser string) Func {
	return Func(func(ctx context.Context, c base.Container) (string, error) {
		return authenticatedUser, nil
	})
}
