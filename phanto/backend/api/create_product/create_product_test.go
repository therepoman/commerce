package create_product_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/backend/api/create_product"
	"code.justin.tv/commerce/phanto/backend/data"
	"code.justin.tv/commerce/phanto/backend/dynamo/call_audit_record"
	dynamo_product "code.justin.tv/commerce/phanto/backend/dynamo/product"
	aws_test "code.justin.tv/commerce/phanto/backend/dynamo/test"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/product_type"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phanto/utils/test"
	"github.com/brildum/testify/mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestAPI_CreateProduct(t *testing.T) {
	Convey("Given a create product API", t, func() {
		productTypeValidator := new(product_type_mock.Validator)
		dao := new(product_mock.DAO)
		auditor := new(audit_mock.Auditor)

		api := create_product.API{
			ProductTypeValidator: productTypeValidator,
			ProductDAO:           dao,
			Auditor:              auditor,
		}

		Convey("Given a nil request", func() {
			var req *phanto.CreateProductReq = nil

			Convey("Then we should return an error", func() {
				_, err := api.CreateProduct(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank product type", func() {
			req := &phanto.CreateProductReq{}

			Convey("Then we should return an error", func() {
				_, err := api.CreateProduct(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a blank sku", func() {
			req := &phanto.CreateProductReq{
				ProductType: "walrus",
			}

			Convey("Then we should return an error", func() {
				_, err := api.CreateProduct(context.Background(), req)

				test.AssertTwirpError(err, twirp.InvalidArgument)
			})
		})

		Convey("Given a valid request", func() {
			productType := "walrus"
			sku := "walrusSKU"
			desc := "This is the sku to redeem one walrus hug"

			req := &phanto.CreateProductReq{
				ProductType: productType,
				Sku:         sku,
				Description: desc,
			}

			Convey("Errors when the auditor returns an error", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.CreateProduct(context.Background(), req)
				test.AssertTwirpError(err, twirp.Internal)
			})

			Convey("When the auditor succeeds", func() {
				auditor.On("AuditRequest", mock.Anything, mock.Anything).Return(&call_audit_record.CallAuditRecord{CallID: "test-call"}, nil)
				auditor.On("AuditResponse", mock.Anything, mock.Anything, mock.Anything).Return()

				Convey("When the validator returns an error", func() {
					productTypeValidator.On("Validate", productType).Return(data.Validation{IsValid: false}, errors.New("WALRUS STRIKE"))

					Convey("Then we should return an error", func() {
						_, err := api.CreateProduct(context.Background(), req)

						test.AssertTwirpError(err, twirp.Internal)
					})
				})

				Convey("When the validator says the product type is invalid", func() {
					productTypeValidator.On("Validate", productType).Return(data.Validation{IsValid: false}, nil)

					Convey("Then we should return an error", func() {
						_, err := api.CreateProduct(context.Background(), req)

						test.AssertTwirpError(err, twirp.InvalidArgument)
					})
				})

				Convey("When the validator says the product type is valid", func() {
					validationData := data.Validation{
						IsValid:     true,
						ProductType: productType,
					}

					productTypeValidator.On("Validate", productType).Return(validationData, nil)

					ctx := context.Background()
					newDynamoProductType := &dynamo_product.Product{
						ProductType: productType,
						SKU:         sku,
						Description: desc,
					}

					Convey("When the database fails to write the new product", func() {
						dao.On("CreateNew", newDynamoProductType).Return(errors.New("WALRUS STRIKE"))

						Convey("Then we should return an error", func() {
							_, err := api.CreateProduct(ctx, req)

							test.AssertTwirpError(err, twirp.Internal)
						})
					})

					Convey("When the database returns that the item already exists", func() {
						dao.On("CreateNew", newDynamoProductType).Return(aws_test.NewAwsErr("ConditionalCheckFailedException"))

						Convey("Then we should return an error", func() {
							_, err := api.CreateProduct(ctx, req)

							test.AssertTwirpError(err, twirp.FailedPrecondition)
						})
					})

					Convey("When we succeed in writing the new product to the database", func() {
						dao.On("CreateNew", newDynamoProductType).Return(nil)

						Convey("Then we should return a success", func() {
							resp, err := api.CreateProduct(ctx, req)

							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
						})
					})
				})
			})
		})
	})
}
