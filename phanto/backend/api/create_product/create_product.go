package create_product

import (
	"context"
	go_strings "strings"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/phanto/backend/audit"
	"code.justin.tv/commerce/phanto/backend/dynamo/product"
	"code.justin.tv/commerce/phanto/backend/dynamo/utils"
	"code.justin.tv/commerce/phanto/backend/product_type"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	ProductTypeValidator product_type.Validator `inject:"productTypeValidator"`
	ProductDAO           product.DAO            `inject:""`
	Auditor              audit.Auditor          `inject:""`
}

func (api *API) CreateProduct(ctx context.Context, req *phanto.CreateProductReq) (resp *phanto.CreateProductResp, err error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("ProductType", "request body cannot be nil")
	} else if strings.Blank(req.ProductType) {
		return nil, twirp.InvalidArgumentError("ProductType", "product type cannot be blank")
	} else if strings.Blank(req.Sku) {
		return nil, twirp.InvalidArgumentError("Sku", "product sku cannot be blank")
	}

	req.ProductType = go_strings.TrimSpace(req.ProductType)
	req.Sku = go_strings.TrimSpace(req.Sku)

	audited, err := api.Auditor.AuditRequest(ctx, &audit.Request{Operation: audit.CreateProductOperation, Body: req})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	defer func() { api.Auditor.AuditResponse(ctx, audited, &audit.Response{Body: resp, Error: err}) }()

	validationData, err := api.ProductTypeValidator.Validate(req.ProductType)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if !validationData.IsValid {
		return nil, twirp.InvalidArgumentError("ProductType", "product type is invalid")
	}

	newProduct := &product.Product{
		ProductType: req.ProductType,
		SKU:         req.Sku,
		Description: req.Description,
	}

	err = api.ProductDAO.CreateNew(newProduct)
	if utils.IsConditionalCheckErr(err) {
		return nil, twirp.NewError(twirp.FailedPrecondition, "product type sku combination already exist")
	}

	if err != nil {
		logrus.WithError(err).Error("downstream dependency error on creating new product")
		return nil, twirp.InternalErrorWith(err)
	}

	return &phanto.CreateProductResp{}, nil
}
