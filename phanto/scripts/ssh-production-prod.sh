#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile phanto-prod prod-commerce-phanto-env
popd deploy
