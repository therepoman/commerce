#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile phanto-devo staging-commerce-phanto-env
popd deploy
