package main

import (
	"math/rand"
	"net/http"
	"os"
	"strings"
	"syscall"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend"
	"code.justin.tv/commerce/phanto/backend/clients/cloudwatchlogger"
	"code.justin.tv/commerce/phanto/backend/clients/s2s2"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/config"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

const (
	malachaiEnvironment = "production"
)

var s2sDisabledOperations = map[string]interface{}{
	"HealthCheck":             nil,
	"RedeemKey":               nil,
	"RedeemKeyForLoadTesting": nil,
	"GetKeyStatus":            nil,
	"GetKeyBatchDownloadInfo": nil,
	"GetKeyPoolsByHandler":    nil,
	"GetKeyBatches":           nil,
	"HelixGetKeyStatus":       nil,
	"HelixRedeemKey":          nil,
	"HelixRedeemKeyByBatch":   nil,
}

func getEnv() config.Environment {
	env := os.Getenv(config.EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !config.IsValidEnvironment(config.Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(config.Default))
		return config.Default
	}

	return config.Environment(env)
}

func main() {
	env := getEnv()

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.Stats))
	twirpHandler := phanto.NewPhantoServer(be.Server, twirpStatsHook)

	mux := goji.NewMux()
	mux.Use(middleware.NewLDAPMiddleware())
	mux.Use(middleware.NewClientIDMiddleware())
	mux.Use(middleware.NewRequestHeadersMiddleware())

	var s2sTwirpHandler http.Handler
	if cfg.S2SEnabled {
		eventsWriterClient, err := events.NewEventLogger(events.Config{
			Environment:     malachaiEnvironment,
			BufferSizeLimit: 0,
		}, log.New())
		if err != nil {
			log.WithError(err).Panic("Error initializing event logger for S2S")
		}
		s2sCalleeClient := &callee.Client{
			ServiceName:        cfg.S2SServiceName,
			Logger:             log.New(),
			EventsWriterClient: eventsWriterClient,
		}
		err = s2sCalleeClient.Start()
		if err != nil {
			log.WithError(err).Panic("Error initializing S2S client")
		}

		s2sTwirpHandler = s2sCalleeClient.RequestValidatorMiddleware(twirpHandler)
	} else {
		s2sTwirpHandler = twirpHandler
	}

	var s2s2logger cloudwatchlogger.CloudWatchLogger
	if cfg.S2S2Enabled {
		logGroupName := cfg.S2SAuthLogGroupName
		s2s2logger, err = cloudwatchlogger.NewCloudWatchLogClient(cfg.AWSRegion, logGroupName)
		if err != nil {
			logrus.WithError(err).Panic("Error initializing cloudwatch logger for S2S2")
		}
		adaptedLogger := cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, logGroupName)
		s2s2Client, err := s2s2.NewS2S2Client(cfg, adaptedLogger)
		if err != nil {
			logrus.WithError(err).Panic("Error initializing S2S2 client")
		}

		var s2s2TwirpHandler http.Handler
		if cfg.S2S2DisablePassthrough {
			s2s2TwirpHandler = s2s2Client.RequireAuthentication(twirpHandler)
		} else {
			s2s2TwirpHandler = s2s2Client.RequireAuthentication(twirpHandler).RecordMetricsOnly()
		}
		mux.Handle(pat.Post(phanto.PhantoPathPrefix+"RedeemKey"), s2s2TwirpHandler)
	} else {
		s2s2logger = cloudwatchlogger.NewCloudWatchLogNoopClient()
	}

	mux.Use(middleware.NewIntegrationTestMiddleware(cfg))
	if cfg.CartmanAuthEnabled {
		mux.Use(middleware.NewAuthorizationMiddleware(cfg))
	}

	mux.HandleFunc(pat.Get("/"), be.Ping)
	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(phanto.PhantoPathPrefix+"*"), NewSelectiveS2SHandler(twirpHandler, s2sTwirpHandler))

	// Makes goji respect SIGINT, SIGTERM, and SIGKILL for graceful shutdown
	goji_graceful.AddSignal(syscall.SIGTERM, syscall.SIGKILL)
	goji_graceful.HandleSignals()

	// When goji begins the shutdown process
	goji_graceful.PreHook(func() {
		log.Info("Initiated shutdown process")
		err := be.SpadeClient.Shutdown()
		if err != nil {
			log.WithError(err).Error("error shutting down spade client")
		}
		s2s2logger.Shutdown()
	})
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	goji_graceful.Wait()

	log.Info("Shutdown complete")
}

func NewSelectiveS2SHandler(nonS2SHandler http.Handler, s2sHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if ShouldUseS2S(r) {
			s2sHandler.ServeHTTP(w, r)
		} else {
			nonS2SHandler.ServeHTTP(w, r)
		}
	})
}

func ShouldUseS2S(r *http.Request) bool {
	if r == nil {
		return true
	}
	if r.URL == nil {
		return true
	}
	path := r.URL.Path
	operation := strings.TrimPrefix(path, phanto.PhantoPathPrefix)

	_, isPresent := s2sDisabledOperations[operation]
	return !isPresent
}
