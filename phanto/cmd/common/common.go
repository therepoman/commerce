package common

import (
	"bufio"
	"encoding/csv"
	"os"
)

func SetAWSProfile(profileName string) {
	err := os.Setenv("AWS_PROFILE", profileName)
	if err != nil {
		panic(err)
	}
}

func SetAWSRegion(region string) {
	err := os.Setenv("AWS_DEFAULT_REGION", region)
	if err != nil {
		panic(err)
	}
}

func GetGoPath() string {
	return os.Getenv("GOPATH")
}

func ReadCSV(filePath string) [][]string {
	fullPath := GetGoPath() + "/src/code.justin.tv/commerce/phanto/cmd/" + filePath
	csvFile, err := os.Open(fullPath)
	if err != nil {
		panic(err)
	}

	reader := csv.NewReader(bufio.NewReader(csvFile))
	rows, err := reader.ReadAll()
	if err != nil {
		panic(err)
	}

	return rows
}

func CreateCSV(fileName string) (*os.File, *csv.Writer) {
	csvFile, err := os.Create(GetGoPath() + "/src/code.justin.tv/commerce/phanto/cmd/" + fileName)
	if err != nil {
		panic(err)
	}
	csvWriter := csv.NewWriter(csvFile)
	return csvFile, csvWriter
}
