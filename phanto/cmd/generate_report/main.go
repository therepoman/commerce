package main

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/sirupsen/logrus"
)

const (
	phantoEndpoint = "https://main.us-west-2.beta.phanto.twitch.a2z.com"
	poolID         = "test-1002"
)

func main() {
	var cfg *caller.Config
	logger := logrus.New()
	rt, err := caller.NewRoundTripper("admin-panel-staging", cfg, logger)
	if err != nil {
		panic(err)
	}

	client := &http.Client{
		Transport: rt,
	}
	phantoClient := phanto.NewPhantoProtobufClient(phantoEndpoint, client)
	phantoResp, err := phantoClient.GenerateKeyPoolReport(context.Background(), &phanto.GenerateKeyPoolReportReq{
		PoolId: poolID,
	})
	if err != nil {
		panic(err)
	}

	fmt.Println(phantoResp.ReportId)
}
