package main

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	dynamo_key "code.justin.tv/commerce/phanto/backend/dynamo/key"
	dynamo_key_batch "code.justin.tv/commerce/phanto/backend/dynamo/key_batch"
	dynamo_key_pool "code.justin.tv/commerce/phanto/backend/dynamo/key_pool"
	dynamo_product_type "code.justin.tv/commerce/phanto/backend/dynamo/product_type"
	"code.justin.tv/commerce/phanto/backend/key"
	"code.justin.tv/commerce/phanto/backend/product/fulfillment"
	"code.justin.tv/commerce/phanto/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	log "github.com/sirupsen/logrus"
)

const (
	env           = "prod"
	hashedKeyCode = "b6ef7dee72ef49a401f78ad221315a4c2ce37bc1"
	dryrun        = true
)

func main() {
	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		log.WithError(err).Panic(err)
	}

	keyDAO := dynamo_key.NewDAO(sess, cfg.DynamoSuffix)
	keyBatchDAO := dynamo_key_batch.NewDAO(sess, cfg.DynamoSuffix)
	keyPoolDAO := dynamo_key_pool.NewDAO(sess, cfg.DynamoSuffix)
	productTypeDAO := dynamo_product_type.NewDAO(sess, cfg.DynamoSuffix)

	k, err := keyDAO.GetKey(hashedKeyCode)
	if err != nil {
		log.WithError(err).Panic(err)
	}

	log.Infof("%v", k)

	batch, err := keyBatchDAO.GetKeyBatch(k.BatchID)
	if err != nil {
		log.WithError(err).Panic(err)
	}

	pool, err := keyPoolDAO.GetKeyPool(batch.PoolID)
	if err != nil {
		log.WithError(err).Panic(err)
	}

	pt, err := productTypeDAO.GetProductType(pool.ProductType)
	if err != nil {
		log.WithError(err).Panic(err)
	}

	log.Infof("SNSTopic %s", pt.SNSTopic)

	claimID, err := key.ToClaimID(hashedKeyCode)
	if err != nil {
		log.WithError(err).Panic(err)
	}

	notificationBody := &fulfillment.SNS{
		UserID:  k.ClaimedBy,
		SKU:     pool.SKU,
		ClaimID: claimID,
	}

	notificationBytes, err := json.Marshal(notificationBody)
	if err != nil {
		log.WithError(err).Panic(err, "could not marshal notification SNS")
	}

	log.Infof("%s", notificationBytes)

	if !dryrun {
		client := sns.NewDefaultClient()
		err = client.Publish(context.Background(), pt.SNSTopic, string(notificationBytes))
		if err != nil {
			log.WithError(err).Panic(err, "Failed to publish to SNSTopic")
		}
	}
}
