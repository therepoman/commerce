### Fulfilling a key via SNS Topic

This is script can be run when a key claim failed to publish to the SNS topic. 
It can be used to resolve the following [Carnaval Alarm for SNSFulfillmentErrors](https://carnaval.amazon.com/v1/viewObject.do?type=monitor&name=Twitch.Phanto.Prod.SNSFulfillmentErrors.Low).

When this alarm fires, check the Phanto logs for an error message like:
```
level=error msg="Key was marked claimed, but failed to send SNS fulfillment message" HashedKeyCode=b6ef7dee72ef49a401f78ad221315a4c2ce37bc1 ProductType=bits RedeemType=StandardRedemption SKU=phanto.vodaphone.50 UserID=126352200"
phanto-prod/phanto-prod/a293674d-1629-492e-a3f3-4886afb44523

level=error msg="Could not claim key code" error="could not publish SNS notification"
```

This error will include the `HashedKeyCode` which you will need to run the script. 
It also includes the `userId` and `SKU` which you can use to verify the script is generating the right output.
 
#### Usage
 ```
go run ./cmd/fulfill_key_sns/main.go
```

Make sure to set the `dryrun` flag in the script to true to check the output first. 

#### Verification that the user got their Bits
Take the `claimID` from the output and verify that transaction exists in the Payday `prod_transactions` table. 
You can also use Admin Panel to check transaction records. 
