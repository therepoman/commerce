package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"

	"code.justin.tv/commerce/gogogadget/crypto/encryption/aes"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/twitchtv/twirp"
)

const (
	phantoEndpoint = "https://main.us-west-2.beta.phanto.twitch.a2z.com"
	batchID        = "3bde4b12-a25f-48bb-a51d-bf9111d05f2f"
)

func main() {
	ctx, err := twirp.WithHTTPRequestHeaders(context.Background(), http.Header{
		middleware.IntegrationTestTUIDHeader: []string{"108707191"},
	})
	if err != nil {
		panic(err)
	}

	phantoClient := phanto.NewPhantoProtobufClient(phantoEndpoint, http.DefaultClient)
	phantoResp, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
		BatchId: batchID,
	})
	if err != nil {
		panic(err)
	}

	resp, err := http.Get(phantoResp.DownloadUrl)
	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	decryptionKey, err := base64.StdEncoding.DecodeString(phantoResp.DecryptionKey)
	if err != nil {
		panic(err)
	}

	result, err := aes.Decrypt(decryptionKey, body)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(result))
}
