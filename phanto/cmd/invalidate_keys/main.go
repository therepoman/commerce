package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"
	"unicode"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/cmd/common"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
)

const (
	awsProfile       = "phanto-prod"
	awsRegion        = "us-west-2"
	phantoEndpoint   = "https://main.us-west-2.prod.phanto.twitch.a2z.com"
	batchSize        = 100
	maxKeysPerWorker = 25000

	inputFile          = "invalidate_keys/keys.csv"
	outputFileTemplate = "invalidate_keys/invalidation_progress_%d.csv"
	errorFileTemplate  = "invalidate_keys/invalidation_errors_%d.csv"
	dryRun             = true
)

// What does the script do?
// Invalidates a set of phanto keys from a given csv input file .
//
// Why do we need it?
// We occasionally get requests to invalidate specific keys for specific business reasons.
// For example, a third party may accidentally leak a large amount of keys publicly and need them invalidated.
//
// How does it do it?
// 1. Reads keys from the input file
// 2. Cleans the keys to remove any non letter/digit characters
// 3. Starts up several workers in their own go routine. Each worker handles a subset of the keys
// 4. Each worker in parallel begins working through its keys:
//    a. Validates the key has not been redeemed (via a phanto call)
//    b. Invalidates the key (via a phanto call)
//    c. Writes the key to a progress output file
// 5. Wait for all workers to complete
func main() {
	common.SetAWSProfile(awsProfile)
	common.SetAWSRegion(awsRegion)

	// Read the keys from the input
	rows := common.ReadCSV(inputFile)

	// Split the keys up for the individual workers
	workerKeys := make([][]string, 0)
	workerKeys = append(workerKeys, make([]string, 0))
	currentWorkerIndex := 0
	for _, row := range rows {
		// clean the key by removing all non letter/number characters
		key := strings.TrimFunc(row[0], func(r rune) bool {
			return !unicode.IsNumber(r) && !unicode.IsLetter(r)
		})
		if len(workerKeys[currentWorkerIndex]) >= maxKeysPerWorker {
			// worker is full, start a new worker
			workerKeys = append(workerKeys, make([]string, 0))
			currentWorkerIndex++
		}
		workerKeys[currentWorkerIndex] = append(workerKeys[currentWorkerIndex], key)
	}

	// Start the workers
	wg := sync.WaitGroup{}
	fmt.Printf("Going to start %d workers... \n", len(workerKeys))
	for workerIdx, keys := range workerKeys {
		wg.Add(1)
		thisWorkerIDX := workerIdx
		thisKeys := keys
		go func() {
			invalidateKeys(thisWorkerIDX+1, thisKeys)
			wg.Done()
		}()
	}

	// Wait for all the workers to finish
	fmt.Println("Waiting on workers...")
	wg.Wait()
	fmt.Println("All workers have finished!")
}

func invalidateKeys(workerNum int, keys []string) {
	var cfg *caller.Config
	logger := logrus.New()
	rt, err := caller.NewRoundTripper("phanto-production", cfg, logger)
	if err != nil {
		panic(err)
	}
	client := &http.Client{
		Transport: rt,
	}
	phantoClient := phanto.NewPhantoProtobufClient(phantoEndpoint, client)

	// Create a progress file to keep track of which keys have been invalidated
	progressFile, progressCSVWriter := common.CreateCSV(fmt.Sprintf(outputFileTemplate, workerNum))
	defer progressFile.Close()
	defer progressCSVWriter.Flush()

	// Create an errors file to keep track of which keys have failed to invalidate
	errorsFile, errorsCSVWriter := common.CreateCSV(fmt.Sprintf(errorFileTemplate, workerNum))
	defer errorsFile.Close()
	defer errorsCSVWriter.Flush()

	batchStartTime := time.Now()
	total := len(keys)
	fmt.Printf("[Worker %d] Going to invalidate %d keys in worker \n", workerNum, total)
	for i, key := range keys {
		shouldInvalidate := true

		// Make sure the key hasn't been claimed
		getKeyStatusResp, err := phantoClient.AdminGetKeyStatus(context.Background(), &phanto.AdminGetKeyStatusReq{
			Key: key,
		})
		if err != nil {
			fmt.Printf("[Worker %d] Failed to get key status: %s \n", workerNum, key)
			errorsCSVWriter.Write([]string{key, "failed to get key status"})
			shouldInvalidate = false
		}
		if getKeyStatusResp.ClaimedBy != "" {
			fmt.Printf("[Worker %d] key already claimed: %s", workerNum, key)
			errorsCSVWriter.Write([]string{key, "key already claimed"})
			shouldInvalidate = false
		}

		if shouldInvalidate {
			if !dryRun {
				// perform the key invalidation
				_, err = phantoClient.InvalidateKeys(context.Background(), &phanto.InvalidateKeysReq{
					Keys: []string{key},
				})
				if err != nil {
					fmt.Printf("[Worker %d] key failed to invalidate: %v \n", workerNum, key)
					errorsCSVWriter.Write([]string{key, "invalidate call failed"})
				}
				progressCSVWriter.Write([]string{key})
				fmt.Printf("[Worker %d] Succesfully invalidated key: %v \n", workerNum, key)
			} else {
				fmt.Printf("[Worker %d] Skipping key invalidation for %v due to dry run... \n", workerNum, key)
			}
		}

		// log / time the worker's progress
		if (i != 0 && i%batchSize == 0) || i == (total-1) {
			fmt.Printf("[Worker %d] Batch process time: %f seconds", workerNum, time.Since(batchStartTime).Seconds())
			fmt.Printf("[Worker %d] Progress: %v \n", workerNum, float64(i+1)/float64(total))
			batchStartTime = time.Now()
		}
	}
}
