package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/go-yaml/yaml"
	log "github.com/sirupsen/logrus"
)

const (
	LocalConfigFilePath  = "/src/code.justin.tv/commerce/phanto/config/data/%s.yaml"
	GlobalConfigFilePath = "/etc/phanto/config/%s.yaml"
	LambdaConfigFilePath = "/config/data/%s.yaml"

	EnvironmentEnvironmentVariable    = "ENVIRONMENT"
	LambdaRootPathEnvironmentVariable = "LAMBDA_TASK_ROOT"
	CanaryEnvironmentVariable         = "CANARY"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")

	Local      = Environment("local")
	Staging    = Environment("staging")
	StagingNLB = Environment("staging-nlb")
	Prod       = Environment("prod")
	Default    = Local
)

type Config struct {
	EnvironmentName                string                   `yaml:"environment-name"`
	AWSRegion                      string                   `yaml:"aws-region"`
	CloudwatchMetricsConfig        *CloudwatchMetricsConfig `yaml:"cloudwatch-metrics-config"`
	KeyGenerationJobSNSTopic       string                   `yaml:"key-generation-job-sns-topic"`
	DynamoSuffix                   string                   `yaml:"dynamo-suffix"`
	KeyGenerationJobQueueConfig    *QueueConfig             `yaml:"key-generation-job-queue"`
	KeyBatchS3Bucket               string                   `yaml:"key-batches-s3-bucket"`
	KMSMasterKeyARN                string                   `yaml:"kms-master-key-arn"`
	SandstormRoleARN               string                   `yaml:"sandstorm-role-arn"`
	SandstormECCPublicKey          string                   `yaml:"sandstorm-ecc-public-key"`
	S2SServiceName                 string                   `yaml:"s2s-service-name"`
	CartmanAuthEnabled             bool                     `yaml:"cartman-auth-enabled"`
	S2SEnabled                     bool                     `yaml:"s2s-enabled"`
	S2S2Enabled                    bool                     `yaml:"s2s2-enabled"`
	S2S2DisablePassthrough         bool                     `yaml:"s2s2-disable-passthrough"`
	S2SAuthLogGroupName            string                   `yaml:"s2s-auth-log-group-name"`
	MaxMindDBS3Bucket              string                   `yaml:"max-mind-db-s3-bucket"`
	ReportGenerationJobSNSTopic    string                   `yaml:"report-generation-job-sns-topic"`
	ReportsS3Bucket                string                   `yaml:"reports-s3-bucket"`
	ReportGenerationJobQueueConfig *QueueConfig             `yaml:"report-generation-job-queue"`
	UseRedisClusterMode            bool                     `yaml:"use-redis-cluster-mode"`
	RedisEndpoint                  string                   `yaml:"redis-endpoint"`
	PhantoEndpoint                 string                   `yaml:"phanto-endpoint"`
	PetoziEndpoint                 string                   `yaml:"petozi-endpoint"`
	UserServiceEndpoint            string                   `yaml:"user-service-endpoint"`
	PDMS                           PDMS                     `yaml:"pdms"`
	StepFn                         StepFnConfig             `yaml:"step-fn"`
}

type StepFnConfig struct {
	UserDeletionStepFn StepFnStateMachineConfig `yaml:"user-deletion-step-fn"`
}

type StepFnStateMachineConfig struct {
	StateMachineARN string `yaml:"state-machine-arn"`
}

type PDMS struct {
	LambdaARN      string `yaml:"lambda-arn"`
	CallerRole     string `yaml:"caller-role"`
	DryRun         bool   `yaml:"dryrun"`
	ReportDeletion bool   `yaml:"report-deletion"`
}

type CloudwatchMetricsConfig struct {
	BufferSize               int     `yaml:"buffer-size"`
	BatchSize                int     `yaml:"batch-size"`
	FlushIntervalMinutes     int     `yaml:"flush-interval-minutes"`
	FlushCheckDelayMS        int     `yaml:"flush-check-delay-ms"`
	EmergencyFlushPercentage float64 `yaml:"emergency-flush-percentage"`
}

type QueueConfig struct {
	Name       string `yaml:"name"`
	URL        string `yaml:"url"`
	NumWorkers int    `yaml:"num-workers"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, StagingNLB: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath)
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	if lambdaRootPath := os.Getenv(LambdaRootPathEnvironmentVariable); lambdaRootPath != "" {
		lambdaFName := path.Join(lambdaRootPath, fmt.Sprintf(LambdaConfigFilePath, baseFilename))
		_, err := os.Stat(lambdaFName)
		return lambdaFName, err
	}
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func (c *Config) IsProd() bool {
	return Environment(c.EnvironmentName) == Prod
}

func IsCanary() bool {
	return strings.EqualFold(os.Getenv(CanaryEnvironmentVariable), "true")
}
