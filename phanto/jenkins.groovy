job {
    name 'commerce-phanto'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'commerce/phanto', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
        }
    }

    steps {
        shell './scripts/docker_build.sh'
        shell './scripts/docker_push.sh'
        saveDeployArtifact 'commerce/phanto-deploy', 'deploy'
    }
}

job {
    name 'commerce-phanto-coverage'
    using 'TEMPLATE-autobuild'

    scm {
        git {
            remote {
                github 'commerce/phanto', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        credentialsBinding {
            string 'AWS_ACCESS_KEY', 'jenkins-terraform-aws-access-key'
            string 'AWS_SECRET_KEY', 'jenkins-terraform-aws-secret-key'
        }
        environmentVariables {
            env('GIT_BRANCH', '${GIT_BRANCH}')
            env('GIT_COMMIT', '${GIT_COMMIT}')
            env('GIT_URL', '${GIT_URL}')
        }
    }

    steps {
        shell 'manta -v -f .manta-coverage.json'
    }

    publishers {
        reportQuality('commerce/phanto', '.manta/coverage', '*.xml')
    }
}

job {
    name 'commerce-phanto-integration-tests'

    parameters {
        stringParam 'GIT_COMMIT'
        stringParam 'ENVIRONMENT'
    }

    configure { project ->
        project / builders / 'com.cloudbees.jenkins.GitHubSetCommitStatusBuilder'
    }

    wrappers {
        credentialsBinding {
            string 'AWS_ACCESS_KEY', 'phanto-devo-tcs-access-key'
            string 'AWS_SECRET_KEY', 'phanto-devo-tcs-secret-key'
        }
        preBuildCleanup()
        timestamps()
    }

    scm {
        git {
            branch('$GIT_COMMIT')
            remote {
                github 'commerce/phanto', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
        }
    }

    steps {
        shell 'sleep 10'
        shell 'manta -v -f .manta_integration.json -e ENVIRONMENT=$ENVIRONMENT -e AWS_ACCESS_KEY=$AWS_ACCESS_KEY -e AWS_SECRET_KEY=$AWS_SECRET_KEY'
    }

    publishers {
        githubCommitNotifier()
    }
}
