// Code generated by mockery v1.0.0
package authorization_mock

import context "context"
import mock "github.com/stretchr/testify/mock"

// Authorizer is an autogenerated mock type for the Authorizer type
type Authorizer struct {
	mock.Mock
}

// AuthenticatedUserMatches provides a mock function with given fields: ctx, userID
func (_m *Authorizer) AuthenticatedUserMatches(ctx context.Context, userID string) bool {
	ret := _m.Called(ctx, userID)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, string) bool); ok {
		r0 = rf(ctx, userID)
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// GetAuthenticatedUser provides a mock function with given fields: ctx
func (_m *Authorizer) GetAuthenticatedUser(ctx context.Context) string {
	ret := _m.Called(ctx)

	var r0 string
	if rf, ok := ret.Get(0).(func(context.Context) string); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// RequestIsAuthenticated provides a mock function with given fields: ctx
func (_m *Authorizer) RequestIsAuthenticated(ctx context.Context) bool {
	ret := _m.Called(ctx)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context) bool); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}
