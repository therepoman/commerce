// Code generated by mockery v2.2.1. DO NOT EDIT.

package stepfn_mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// Execute provides a mock function with given fields: ctx, stateMachineARN, executionName, executionInput
func (_m *Client) Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error {
	ret := _m.Called(ctx, stateMachineARN, executionName, executionInput)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, interface{}) error); ok {
		r0 = rf(ctx, stateMachineARN, executionName, executionInput)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
