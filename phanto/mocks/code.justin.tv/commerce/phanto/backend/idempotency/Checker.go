// Code generated by mockery v1.0.0
package idempotency_mock

import context "context"

import mock "github.com/stretchr/testify/mock"

// Checker is an autogenerated mock type for the Checker type
type Checker struct {
	mock.Mock
}

// IsNewRequest provides a mock function with given fields: ctx, batchID, idempotencyKey
func (_m *Checker) IsNewRequest(ctx context.Context, batchID string, idempotencyKey string) (bool, error) {
	ret := _m.Called(ctx, batchID, idempotencyKey)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, string, string) bool); ok {
		r0 = rf(ctx, batchID, idempotencyKey)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, batchID, idempotencyKey)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
