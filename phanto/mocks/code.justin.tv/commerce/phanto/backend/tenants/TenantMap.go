// Code generated by mockery v1.0.0
package tenants_mock

import mock "github.com/stretchr/testify/mock"
import phanto_tenant "code.justin.tv/commerce/phanto/rpc/tenant"

// TenantMap is an autogenerated mock type for the TenantMap type
type TenantMap struct {
	mock.Mock
}

// Get provides a mock function with given fields: productType
func (_m *TenantMap) Get(productType string) (phanto_tenant.PhantoTenant, error) {
	ret := _m.Called(productType)

	var r0 phanto_tenant.PhantoTenant
	if rf, ok := ret.Get(0).(func(string) phanto_tenant.PhantoTenant); ok {
		r0 = rf(productType)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(phanto_tenant.PhantoTenant)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(productType)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
