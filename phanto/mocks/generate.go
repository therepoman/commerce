package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// Idempotency Checker
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Checker -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/idempotency -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/idempotency -outpkg=idempotency_mock

// Key Set Editor
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Editor -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_batch/key_set -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/key_set -outpkg=key_set_mock

// Key Set Checker
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Checker -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_batch/key_set -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/key_set -outpkg=key_set_mock

// Internal User Service Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=InternalClient -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/web/users-service/client/usersclient_internal -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/web/users-service/client/usersclient_internal -outpkg=usersclient_internal_mock

// GoGoGadget SNS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/commerce/gogogadget/aws/sns -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/sns -outpkg=sns_mock

// Key Pool DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/key_pool -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool -outpkg=key_pool_mock

// Key Batch DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/key_batch -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_batch -outpkg=key_batch_mock

// Key DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/key -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key -outpkg=key_mock

// Product DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/product -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product -outpkg=product_mock

// Product Type DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/product_type -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/product_type -outpkg=product_type_mock

// Report DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/report -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/report -outpkg=report_mock

// Key Pool Metadata DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata -outpkg=key_pool_metadata_mock

// Key Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key -outpkg=key_mock

// Key Claimer
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Claimer -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key -outpkg=key_mock

// Key Batch Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_batch -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch -outpkg=key_batch_mock

// Key Pool Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_pool -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_pool -outpkg=key_pool_mock

// Product Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/product -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/product -outpkg=product_mock

// Product Type Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/product_type -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/product_type -outpkg=product_type_mock

// Key Maker
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=KeyMaker -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key/maker -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/maker -outpkg=maker_mock

// S3 Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/commerce/gogogadget/aws/s3 -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/s3 -outpkg=s3_mock

// KMS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/commerce/gogogadget/aws/kms -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/aws/kms -outpkg=kms_mock

// Key Authorizer
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Authorizer -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key/authorization -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key/authorization -outpkg=authorization_mock

// Key Batch Handler Authorizer
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=HandlerAuthorizer -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_batch/authorization -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_batch/authorization -outpkg=authorization_mock

// Key Pool Handler Authorizer
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=HandlerAuthorizer -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_pool/authorization -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_pool/authorization -outpkg=authorization_mock

// Key Pool User Authorizer
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=UserAuthorizer -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/key_pool/authorization -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/key_pool/authorization -outpkg=authorization_mock

// Auditor
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Auditor -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/audit -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/audit -outpkg=audit_mock

// GeoIP Locator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Locator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/geoip/locator -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/geoip/locator -outpkg=locator_mock

// GeoIP Refresher
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Refresher -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/geoip/refresher -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/geoip/refresher -outpkg=refresher_mock

// GoGoGadget GeoIP Locator
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Locator -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/commerce/gogogadget/geoip -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/gogogadget/geoip -outpkg=geoip_mock

// Key Pool Metadata DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_pool_metadata  -outpkg=key_pool_metadata_mock

// Throttler
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=IThrottler -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/throttle -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/throttle -outpkg=throttle_mock

// Stats
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/github.com/cactus/go-statsd-client/statsd -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/github.com/cactus/go-statsd-client/statsd -outpkg=statsd_mock

// Key Handler Group DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/dynamo/key_handler_group -outpkg=key_handler_group_mock

// Tenant Map
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=TenantMap -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/tenants -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/tenants -outpkg=tenants_mock

// Eligibility Checker
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=EligibilityChecker -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/eligibility -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/eligibility -outpkg=eligibility_mock

// Phanto Tenant Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=PhantoTenant -dir=$GOPATH/src/code.justin.tv/commerce/phanto/rpc/tenant -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/rpc/tenant -outpkg=phanto_tenant_mock

// Redeem Key By Batch Base API
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=API -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base -outpkg=redeem_key_by_batch_base_mock

// Petozi RPC
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Petozi -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/commerce/petozi/rpc -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/petozi/rpc -outpkg=petozi_rpc_mock

// Petozi Wrapper Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/phanto/backend/clients/petozi -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/clients/petozi -outpkg=petozi_client_mock

// PDMS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=PDMSService -dir=$GOPATH/src/code.justin.tv/commerce/phanto/vendor/code.justin.tv/amzn/PDMSLambdaTwirp -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/amzn/PDMSLambdaTwirp -outpkg=pdms_mock

// StepFn Client
//go:generate $GOPATH/src/code.justin.tv/commerce/phanto/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/phanto/clients/stepfn -output=$GOPATH/src/code.justin.tv/commerce/phanto/mocks/code.justin.tv/commerce/phanto/backend/clients/stepfn -outpkg=stepfn_mock
