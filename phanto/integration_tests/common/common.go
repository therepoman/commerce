package common

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/csv"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"time"

	"code.justin.tv/commerce/gogogadget/crypto/encryption/aes"
	"code.justin.tv/commerce/phanto/backend/api/base/throttle"
	"code.justin.tv/commerce/phanto/backend/middleware"
	"code.justin.tv/commerce/phanto/config"
	"code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

const (
	KeyStatus_Unclaimed = "unclaimed"
	KeyStatus_Claimed   = "claimed"
	KeyStatus_Inactive  = "invalidated"

	TestClientIP          = "127.0.0.1"
	TestTUID              = "108707191"
	TestDescription       = "test description"
	TestProductType       = "integration-test"
	TestSKU               = "test-sku-1"
	keyRegex              = "^[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}$"
	workflowStatusSuccess = "SUCCEEDED"

	delayBetweenRequests = 10 * time.Millisecond
)

func GetPhantoClient(useS2S bool) phanto.Phanto {
	environment := os.Getenv("ENVIRONMENT")
	if environment == "" {
		log.Panic("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panic("Please don't run these integration tests against prod!")
	}

	log.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		log.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		log.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	httpClient := http.DefaultClient
	if useS2S {
		var s2sConfig *caller.Config
		logger := log.New()
		rt, err := caller.NewRoundTripper(cfg.S2SServiceName, s2sConfig, logger)
		if err != nil {
			panic(err)
		}

		httpClient = &http.Client{
			Transport: rt,
		}
	}

	return phanto.NewPhantoProtobufClient(cfg.PhantoEndpoint, httpClient)
}

func GetAuthenticatedContext(disableThrottling bool, authedUser string) context.Context {
	throttleHeader := ""
	if disableThrottling {
		throttleHeader = throttle.ConfigDisabled
	}

	ctx, err := twirp.WithHTTPRequestHeaders(context.Background(), http.Header{
		middleware.IntegrationTestTUIDHeader:           []string{authedUser},
		middleware.IntegrationTestThrottleConfigHeader: []string{throttleHeader},
	})
	So(err, ShouldBeNil)
	return ctx
}

func GetHelixAPIContext(disableThrottling bool, clientID string) context.Context {
	throttleHeader := ""
	if disableThrottling {
		throttleHeader = throttle.ConfigDisabled
	}

	ctx, err := twirp.WithHTTPRequestHeaders(context.Background(), http.Header{
		middleware.IntegrationTestThrottleConfigHeader: []string{throttleHeader},
		middleware.ClientIDHeader:                      []string{clientID},
	})
	So(err, ShouldBeNil)
	return ctx
}

func DownloadAndDecrypt(downloadURL string, base64DecryptionKey string) *csv.Reader {
	httpClient := http.DefaultClient
	s3DLResp, err := httpClient.Get(downloadURL)
	So(err, ShouldBeNil)

	s3DlBytes, err := ioutil.ReadAll(s3DLResp.Body)
	So(err, ShouldBeNil)

	decryptionKey, err := base64.StdEncoding.DecodeString(base64DecryptionKey)
	So(err, ShouldBeNil)

	decryptedBytes, err := aes.Decrypt(decryptionKey, s3DlBytes)
	So(err, ShouldBeNil)

	return csv.NewReader(bytes.NewReader(decryptedBytes))
}

func AssertCorrectKeyFormat(key string) {
	keyRegexp := regexp.MustCompile(keyRegex)
	So(keyRegexp.MatchString(key), ShouldBeTrue)
}

func TwirpTime(t time.Time) *timestamp.Timestamp {
	tt, err := ptypes.TimestampProto(t)
	So(err, ShouldBeNil)
	return tt
}

func CreateHandlerGroup(ctx context.Context, phantoClient phanto.Phanto, authorizedTuids []string, authorizedClientIDs []string) string {
	groupIDUUID, err := uuid.NewV4()
	So(err, ShouldBeNil)
	groupID := groupIDUUID.String()

	_, err = phantoClient.UpdateKeyHandlerGroup(ctx, &phanto.UpdateKeyHandlerGroupsReq{
		HandlerGroupId:      groupID,
		Description:         TestDescription,
		AuthorizedTuids:     authorizedTuids,
		AuthorizedClientIds: authorizedClientIDs,
	})
	So(err, ShouldBeNil)
	time.Sleep(delayBetweenRequests)
	return groupID
}

func UpdateHandlerGroup(ctx context.Context, phantoClient phanto.Phanto, groupID string, authorizedTuids []string, authorizedClientIDs []string) {
	_, err := phantoClient.UpdateKeyHandlerGroup(ctx, &phanto.UpdateKeyHandlerGroupsReq{
		HandlerGroupId:      groupID,
		Description:         TestDescription,
		AuthorizedTuids:     authorizedTuids,
		AuthorizedClientIds: authorizedClientIDs,
	})
	So(err, ShouldBeNil)
	time.Sleep(delayBetweenRequests)
}

func CreateKeyPool(ctx context.Context, phantoClient phanto.Phanto, startTime time.Time, endTime time.Time, handler string, handlerGroupID string, mode phanto.RedemptionMode) string {
	poolIDUUID, err := uuid.NewV4()
	So(err, ShouldBeNil)
	poolID := poolIDUUID.String()

	_, err = phantoClient.CreateKeyPool(ctx, &phanto.CreateKeyPoolReq{
		PoolId:         poolID,
		ProductType:    TestProductType,
		Sku:            TestSKU,
		Description:    TestDescription,
		Handler:        handler,
		HandlerGroupId: handlerGroupID,
		StartDate:      TwirpTime(startTime),
		EndDate:        TwirpTime(endTime),
		RedemptionMode: mode,
	})
	So(err, ShouldBeNil)
	time.Sleep(delayBetweenRequests)
	return poolID
}

func StartGeneratingKeyBatch(ctx context.Context, phantoClient phanto.Phanto, poolID string, numKeysToGenerate int64) string {
	batchIDUUID, err := uuid.NewV4()
	So(err, ShouldBeNil)
	batchID := batchIDUUID.String()

	_, err = phantoClient.GenerateKeyBatch(ctx, &phanto.GenerateKeyBatchReq{
		PoolId:       poolID,
		BatchId:      batchID,
		NumberOfKeys: numKeysToGenerate,
	})
	So(err, ShouldBeNil)
	time.Sleep(delayBetweenRequests)
	return batchID
}

func WaitForKeyBatchToFinishGenerating(ctx context.Context, phantoClient phanto.Phanto, batchID string) {
	generateStartTime := time.Now()
	for {
		resp, err := phantoClient.GetKeyBatchStatus(ctx, &phanto.GetKeyBatchStatusReq{
			BatchId: batchID,
		})
		So(err, ShouldBeNil)

		if resp.WorkflowStatus == workflowStatusSuccess {
			break
		}

		if time.Since(generateStartTime) > 120*time.Second {
			So(errors.New("keys did not generate successfully in time"), ShouldBeNil)
			break
		}
		time.Sleep(2 * time.Second)
	}
	time.Sleep(delayBetweenRequests)
}

func CreateKeyPoolGenerateKeysAndDownload(ctx context.Context, phantoClient phanto.Phanto, startTime time.Time, endTime time.Time, numKeys int64) (string, string, []string) {
	poolID := CreateKeyPool(ctx, phantoClient, startTime, endTime, TestTUID, "", phanto.RedemptionMode_REDEEM_BY_KEY)
	batchID := StartGeneratingKeyBatch(ctx, phantoClient, poolID, numKeys)
	WaitForKeyBatchToFinishGenerating(ctx, phantoClient, batchID)

	resp, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
		BatchId: batchID,
	})
	So(err, ShouldBeNil)
	time.Sleep(delayBetweenRequests)

	keyCSVReader := DownloadAndDecrypt(resp.DownloadUrl, resp.DecryptionKey)
	records, err := keyCSVReader.ReadAll()
	So(err, ShouldBeNil)
	So(len(records), ShouldEqual, numKeys)

	keys := make([]string, 0)
	for _, record := range records {
		So(len(record), ShouldEqual, 1)
		key := record[0]
		AssertCorrectKeyFormat(key)
		keys = append(keys, key)
	}

	return poolID, batchID, keys
}

func CreateBatchRedeemableKeyPoolAndGenerateKeys(ctx context.Context, phantoClient phanto.Phanto, handlerGroupID string, startTime time.Time, endTime time.Time, numKeys int64) (string, string) {
	poolID := CreateKeyPool(ctx, phantoClient, startTime, endTime, "", handlerGroupID, phanto.RedemptionMode_REDEEM_BY_BATCH_ID)
	batchID := StartGeneratingKeyBatch(ctx, phantoClient, poolID, numKeys)
	WaitForKeyBatchToFinishGenerating(ctx, phantoClient, batchID)
	return poolID, batchID
}

func AssertKeyStatus(ctx context.Context, phantoClient phanto.Phanto, key string, expectedStatus string) {
	resp, err := phantoClient.GetKeyStatus(ctx, &phanto.GetKeyStatusReq{
		Key: key,
	})
	So(err, ShouldBeNil)
	So(resp.Status, ShouldEqual, expectedStatus)
	time.Sleep(delayBetweenRequests)
}

func RedeemKey(ctx context.Context, phantoClient phanto.Phanto, key string) error {
	_, err := phantoClient.RedeemKey(ctx, &phanto.RedeemKeyReq{
		Key:      key,
		UserId:   TestTUID,
		ClientIp: TestClientIP,
	})
	time.Sleep(delayBetweenRequests)
	return err
}

func RedeemKeyByBatchAndAssertSuccess(ctx context.Context, phantoClient phanto.Phanto, batchID string, expectedStatus phanto.KeyStatus, idempotencyKey string, expectedRemainingCount int64) {
	resp, err := phantoClient.HelixRedeemKeyByBatch(ctx, &phanto.HelixRedeemKeyByBatchReq{
		BatchId:        batchID,
		UserId:         TestTUID,
		IdempotencyKey: idempotencyKey,
	})
	So(err, ShouldBeNil)
	So(resp.Status, ShouldEqual, expectedStatus)
	So(resp.KeysRemaining, ShouldEqual, expectedRemainingCount)
	time.Sleep(delayBetweenRequests)
}

func RedeemKeyByBatchAndAssertError(ctx context.Context, phantoClient phanto.Phanto, batchID string, idempotencyKey string, expectedError error) {
	_, err := phantoClient.HelixRedeemKeyByBatch(ctx, &phanto.HelixRedeemKeyByBatchReq{
		BatchId:        batchID,
		UserId:         TestTUID,
		IdempotencyKey: idempotencyKey,
	})
	So(err, ShouldNotBeNil)
	So(err.Error(), ShouldEqual, expectedError.Error())
	time.Sleep(delayBetweenRequests)
}

type RedeemByBatchResp struct {
	Resp  *phanto.HelixRedeemKeyByBatchResp
	Error error
}

func RedeemKeyByBatchInParallel(ctx context.Context, phantoClient phanto.Phanto, batchID string, numberOfCalls int) chan RedeemByBatchResp {
	respChan := make(chan RedeemByBatchResp, numberOfCalls)
	for i := 0; i < numberOfCalls; i++ {
		go func() {
			resp, err := phantoClient.HelixRedeemKeyByBatch(ctx, &phanto.HelixRedeemKeyByBatchReq{
				BatchId:        batchID,
				UserId:         TestTUID,
				IdempotencyKey: "",
			})
			respChan <- RedeemByBatchResp{
				Resp:  resp,
				Error: err,
			}

		}()
	}
	return respChan
}
