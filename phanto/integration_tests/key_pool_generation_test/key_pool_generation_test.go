// +build integration

package key_pool_generation_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/product"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	existingProductType    = common.TestProductType
	nonExistingProductType = "does_not_exist"

	bitsProductType         = product.ProductTypeBits
	existingBitsProductType = "1_bits"
	nonExistingBitsProduct  = "this-bits-product-does-not-exist"

	handler              = common.TestTUID
	handlerGroupID       = ""
	delayBetweenRequests = 10 * time.Millisecond
)

func TestKeyPoolGenerationSuccess(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("if product type is an existing one in Phanto", func() {
			poolIDUUID, err := uuid.NewV4()
			So(err, ShouldBeNil)
			poolID := poolIDUUID.String()

			_, err = phantoClient.CreateKeyPool(ctx, &phanto.CreateKeyPoolReq{
				PoolId:         poolID,
				ProductType:    existingProductType,
				Sku:            common.TestSKU,
				Description:    common.TestDescription,
				Handler:        handler,
				HandlerGroupId: handlerGroupID,
				StartDate:      common.TwirpTime(time.Now().Add(-1 * time.Hour)),
				EndDate:        common.TwirpTime(time.Now().Add(1 * time.Hour)),
				RedemptionMode: phanto.RedemptionMode_REDEEM_BY_KEY,
			})
			So(err, ShouldBeNil)
			time.Sleep(delayBetweenRequests)
		})
	})
}

func TestKeyPoolGenerationFail(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("if product type is not an existing one in Phanto", func() {
			poolIDUUID, err := uuid.NewV4()
			So(err, ShouldBeNil)
			poolID := poolIDUUID.String()

			_, err = phantoClient.CreateKeyPool(ctx, &phanto.CreateKeyPoolReq{
				PoolId:         poolID,
				ProductType:    nonExistingProductType,
				Sku:            common.TestSKU,
				Description:    common.TestDescription,
				Handler:        handler,
				HandlerGroupId: handlerGroupID,
				StartDate:      common.TwirpTime(time.Now().Add(-1 * time.Hour)),
				EndDate:        common.TwirpTime(time.Now().Add(1 * time.Hour)),
				RedemptionMode: phanto.RedemptionMode_REDEEM_BY_KEY,
			})
			So(err, ShouldNotBeNil)
			time.Sleep(delayBetweenRequests)
		})
	})
}

func TestBitsKeyPoolGenerationSuccess(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("if product type is Bits and SKU exists in Bits Catalog", func() {
			poolIDUUID, err := uuid.NewV4()
			So(err, ShouldBeNil)
			poolID := poolIDUUID.String()

			_, err = phantoClient.CreateKeyPool(ctx, &phanto.CreateKeyPoolReq{
				PoolId:         poolID,
				ProductType:    bitsProductType,
				Sku:            existingBitsProductType,
				Description:    common.TestDescription,
				Handler:        handler,
				HandlerGroupId: handlerGroupID,
				StartDate:      common.TwirpTime(time.Now().Add(-1 * time.Hour)),
				EndDate:        common.TwirpTime(time.Now().Add(1 * time.Hour)),
				RedemptionMode: phanto.RedemptionMode_REDEEM_BY_KEY,
			})
			So(err, ShouldBeNil)
			time.Sleep(delayBetweenRequests)
		})
	})
}

func TestBitsKeyPoolGenerationFail(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("if product type is Bits and SKU does not exist in Bits Catalog", func() {
			poolIDUUID, err := uuid.NewV4()
			So(err, ShouldBeNil)
			poolID := poolIDUUID.String()

			_, err = phantoClient.CreateKeyPool(ctx, &phanto.CreateKeyPoolReq{
				PoolId:         poolID,
				ProductType:    bitsProductType,
				Sku:            nonExistingBitsProduct,
				Description:    common.TestDescription,
				Handler:        handler,
				HandlerGroupId: handlerGroupID,
				StartDate:      common.TwirpTime(time.Now().Add(-1 * time.Hour)),
				EndDate:        common.TwirpTime(time.Now().Add(1 * time.Hour)),
				RedemptionMode: phanto.RedemptionMode_REDEEM_BY_KEY,
			})
			So(err, ShouldNotBeNil)
			time.Sleep(delayBetweenRequests)
		})
	})
}
