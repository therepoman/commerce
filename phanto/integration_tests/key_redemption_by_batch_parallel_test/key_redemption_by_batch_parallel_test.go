// +build integration

package key_redemption_by_batch_parallel_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 50
	testClientID      = helix.TestClientID
)

func TestKeyRedemptionByBatchParallel(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetHelixAPIContext(true, testClientID)

		Convey("Handler group should create successfully", func() {
			handlerGroupID := common.CreateHandlerGroup(ctx, phantoClient, []string{}, []string{"client-id-1", testClientID, "client-id-3"})

			Convey("Keys should successfully generate ", func() {
				_, batchID := common.CreateBatchRedeemableKeyPoolAndGenerateKeys(ctx, phantoClient, handlerGroupID, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

				Convey("Start redeem by batch calls in parallel", func() {
					redeemByBatchResp := common.RedeemKeyByBatchInParallel(ctx, phantoClient, batchID, numKeysToGenerate*2)

					timeout, cancel := context.WithTimeout(context.Background(), 15*time.Second)
					defer cancel()

					successCount := 0
					noKeysRemainingErrorCount := 0
					for i := 0; i < numKeysToGenerate*2; i++ {
						select {
						case resp := <-redeemByBatchResp:
							if resp.Error != nil {
								if resp.Error.Error() == redeem_key_by_batch_base.NoRemainingKeysError.Error() {
									noKeysRemainingErrorCount++
								} else {
									// Fail due to unexpected error
									So(resp.Error, ShouldBeNil)
								}
							} else {
								successCount++
							}
						case <-timeout.Done():
							// Fail due to timeout
							So(errors.New("timed out waiting on responses"), ShouldBeNil)
						}
					}

					// Half the requests should have succeeded
					So(successCount, ShouldEqual, numKeysToGenerate)

					// Half the requests should have errored due to insufficient remaining keys
					So(noKeysRemainingErrorCount, ShouldEqual, numKeysToGenerate)
				})
			})
		})
	})
}
