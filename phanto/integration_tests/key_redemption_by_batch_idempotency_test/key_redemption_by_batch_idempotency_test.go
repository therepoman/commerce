// +build integration

package key_redemption_by_batch_idempotency_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 2
	testClientID      = helix.TestClientID
)

func TestKeyRedemptionByBatchIdempotency(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetHelixAPIContext(true, testClientID)

		Convey("Handler group should create successfully", func() {
			handlerGroupID := common.CreateHandlerGroup(ctx, phantoClient, []string{}, []string{testClientID})

			Convey("Keys should successfully generate ", func() {
				_, batchID := common.CreateBatchRedeemableKeyPoolAndGenerateKeys(ctx, phantoClient, handlerGroupID, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

				Convey("First key should be claimable with an idempotency key", func() {
					idempotencyKeyUUID, err := uuid.NewV4()
					So(err, ShouldBeNil)
					idempotencyKey := idempotencyKeyUUID.String()

					common.RedeemKeyByBatchAndAssertSuccess(ctx, phantoClient, batchID, phanto.KeyStatus_SUCCESSFULLY_REDEEMED, idempotencyKey, numKeysToGenerate-1)

					Convey("Subsequent claims with the same idempotency key should fail", func() {
						for i := 0; i < 1; i++ {
							common.RedeemKeyByBatchAndAssertError(ctx, phantoClient, batchID, idempotencyKey, redeem_key_by_batch_base.DuplicateRequestError)
						}

						Convey("Second key can be claimed with a new idempotency key", func() {
							idempotencyKey2UUID, err := uuid.NewV4()
							So(err, ShouldBeNil)
							idempotencyKey2 := idempotencyKey2UUID.String()

							common.RedeemKeyByBatchAndAssertSuccess(ctx, phantoClient, batchID, phanto.KeyStatus_SUCCESSFULLY_REDEEMED, idempotencyKey2, numKeysToGenerate-2)
						})
					})
				})
			})
		})
	})
}
