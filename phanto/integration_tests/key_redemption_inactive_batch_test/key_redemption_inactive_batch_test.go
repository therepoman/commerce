// +build integration

package key_redemption_inactive_batch_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/key_batch/status"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 3
)

func TestKeyRedemptionInactiveBatch(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("Keys should successfully generate and download", func() {
			_, batchID, keys := common.CreateKeyPoolGenerateKeysAndDownload(ctx, phantoClient, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

			Convey("Get key status should indicate that all keys are unused", func() {
				for _, key := range keys {
					common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Unclaimed)
				}

				Convey("Batch should successfully be deactivated", func() {
					_, err := phantoClient.UpdateKeyBatch(ctx, &phanto.UpdateKeyBatchReq{
						BatchId: batchID,
						Status:  string(status.KeyBatchStatusInactive),
					})
					So(err, ShouldBeNil)

					Convey("Get key status should indicate that all keys are inactive", func() {
						for _, key := range keys {
							common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Inactive)
						}

						Convey("Redeem key should error for each key", func() {
							for _, key := range keys {
								err := common.RedeemKey(ctx, phantoClient, key)
								So(err, ShouldNotBeNil)
							}
						})
					})
				})
			})
		})
	})
}
