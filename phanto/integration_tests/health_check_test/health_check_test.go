// +build integration

package health_check_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(false)

		Convey("Test the health check endpoint", func() {
			_, err := phantoClient.HealthCheck(context.Background(), &phanto.HealthCheckReq{})
			So(err, ShouldBeNil)
		})
	})
}
