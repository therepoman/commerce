// +build integration

package key_redemption_happy_case_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/integration_tests/common"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 3
)

func TestKeyRedemptionHappyCase(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("Keys should successfully generate and download", func() {
			_, _, keys := common.CreateKeyPoolGenerateKeysAndDownload(ctx, phantoClient, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

			Convey("Get key status should indicate that all keys are unused", func() {
				for _, key := range keys {
					common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Unclaimed)
				}

				Convey("Redeem key should work for each key", func() {
					for _, key := range keys {
						err := common.RedeemKey(ctx, phantoClient, key)
						So(err, ShouldBeNil)
					}

					Convey("Get key status should indicate that all keys are claimed", func() {
						for _, key := range keys {
							common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Claimed)
						}

						Convey("Redeem key should error for each key", func() {
							for _, key := range keys {
								err := common.RedeemKey(ctx, phantoClient, key)
								So(err, ShouldNotBeNil)
							}
						})
					})
				})
			})
		})
	})
}
