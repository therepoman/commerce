// +build integration

package key_download_auth_test

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 300
)

func TestKeyGeneration_LegacyAuth(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)

		Convey("Key pool (authed with a legacy handler) should create successfully", func() {
			poolID := common.CreateKeyPool(context.Background(), phantoClient, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), common.TestTUID, "", phanto.RedemptionMode_REDEEM_BY_KEY)

			Convey("Key batch generation should begin successfully", func() {
				batchID := common.StartGeneratingKeyBatch(context.Background(), phantoClient, poolID, numKeysToGenerate)

				Convey("Keys should finish generating successfully", func() {
					common.WaitForKeyBatchToFinishGenerating(context.Background(), phantoClient, batchID)

					Convey("Get key batch download info call should error for the wrong handler", func() {
						ctx := common.GetAuthenticatedContext(true, "12345678")
						_, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
							BatchId: batchID,
						})
						So(err, ShouldNotBeNil)
					})

					Convey("Get key batch download info call should work when authed as the handler", func() {
						ctx := common.GetAuthenticatedContext(true, common.TestTUID)
						resp, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
							BatchId: batchID,
						})
						So(err, ShouldBeNil)

						Convey("Downloading and decrypting key file should work", func() {
							keysReader := common.DownloadAndDecrypt(resp.DownloadUrl, resp.DecryptionKey)

							Convey("All keys should be present in file", func() {
								records, err := keysReader.ReadAll()
								So(err, ShouldBeNil)
								So(len(records), ShouldEqual, numKeysToGenerate)
								for _, record := range records {
									So(len(record), ShouldEqual, 1)
									common.AssertCorrectKeyFormat(record[0])
								}
							})
						})
					})
				})
			})
		})
	})
}

func TestKeyGeneration_HandlerGroupAuth(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		testTuid1 := "123456"
		testTuid2 := "654321"
		phantoClient := common.GetPhantoClient(true)

		Convey("Handler group should create successfully", func() {
			groupID := common.CreateHandlerGroup(context.Background(), phantoClient, []string{testTuid1, testTuid2}, []string{})

			Convey("Key pool (authed with group handler) should create successfully", func() {
				poolID := common.CreateKeyPool(context.Background(), phantoClient, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), "", groupID, phanto.RedemptionMode_REDEEM_BY_KEY)

				Convey("Key batch generation should begin successfully", func() {
					batchID := common.StartGeneratingKeyBatch(context.Background(), phantoClient, poolID, numKeysToGenerate)

					Convey("Keys should finish generating successfully", func() {
						common.WaitForKeyBatchToFinishGenerating(context.Background(), phantoClient, batchID)

						Convey("Get key batch download info call should error for user not in group", func() {
							ctx := common.GetAuthenticatedContext(true, "9999999")
							_, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
								BatchId: batchID,
							})
							So(err, ShouldNotBeNil)
						})

						Convey("Get key batch download info call should work when authed as group member #1", func() {
							ctx := common.GetAuthenticatedContext(true, testTuid1)
							resp, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
								BatchId: batchID,
							})
							So(err, ShouldBeNil)

							Convey("Downloading and decrypting key file should work", func() {
								keysReader := common.DownloadAndDecrypt(resp.DownloadUrl, resp.DecryptionKey)

								Convey("All keys should be present in file", func() {
									records, err := keysReader.ReadAll()
									So(err, ShouldBeNil)
									So(len(records), ShouldEqual, numKeysToGenerate)
									for _, record := range records {
										So(len(record), ShouldEqual, 1)
										common.AssertCorrectKeyFormat(record[0])
									}
								})
							})
						})

						Convey("Get key batch download info call should work when authed as group member #2", func() {
							ctx := common.GetAuthenticatedContext(true, testTuid2)
							resp, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
								BatchId: batchID,
							})
							So(err, ShouldBeNil)

							Convey("Downloading and decrypting key file should work", func() {
								keysReader := common.DownloadAndDecrypt(resp.DownloadUrl, resp.DecryptionKey)

								Convey("All keys should be present in file", func() {
									records, err := keysReader.ReadAll()
									So(err, ShouldBeNil)
									So(len(records), ShouldEqual, numKeysToGenerate)
									for _, record := range records {
										So(len(record), ShouldEqual, 1)
										common.AssertCorrectKeyFormat(record[0])
									}
								})
							})
						})
					})
				})
			})
		})
	})
}
