// +build integration

package key_redemption_date_range_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/key_pool/status"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 3
)

func TestKeyRedemptionDateRange(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("Keys should successfully generate and download", func() {
			poolID, _, keys := common.CreateKeyPoolGenerateKeysAndDownload(ctx, phantoClient, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

			Convey("Get key status should indicate that all keys are unused", func() {
				for _, key := range keys {
					common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Unclaimed)
				}

				Convey("Pool should successfully be updated with a start date in the future", func() {
					_, err := phantoClient.UpdateKeyPool(ctx, &phanto.UpdateKeyPoolReq{
						PoolId:    poolID,
						Status:    string(status.KeyPoolStatusActive),
						StartDate: common.TwirpTime(time.Now().Add(time.Hour)),
						EndDate:   common.TwirpTime(time.Now().Add(2 * time.Hour)),
					})
					So(err, ShouldBeNil)

					Convey("Get key status should indicate that all keys are inactive", func() {
						for _, key := range keys {
							common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Inactive)
						}

						Convey("Redeem key should error for each key", func() {
							for _, key := range keys {
								err := common.RedeemKey(ctx, phantoClient, key)
								So(err, ShouldNotBeNil)
							}

							Convey("Pool should successfully be updated with an end date in the past", func() {
								_, err := phantoClient.UpdateKeyPool(ctx, &phanto.UpdateKeyPoolReq{
									PoolId:    poolID,
									Status:    string(status.KeyPoolStatusActive),
									StartDate: common.TwirpTime(time.Now().Add(-2 * time.Hour)),
									EndDate:   common.TwirpTime(time.Now().Add(-1 * time.Hour)),
								})
								So(err, ShouldBeNil)

								Convey("Get key status should indicate that all keys are inactive", func() {
									for _, key := range keys {
										common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Inactive)
									}

									Convey("Redeem key should error for each key", func() {
										for _, key := range keys {
											err := common.RedeemKey(ctx, phantoClient, key)
											So(err, ShouldNotBeNil)
										}

										Convey("Pool should successfully be updated with a valid date range", func() {
											_, err := phantoClient.UpdateKeyPool(ctx, &phanto.UpdateKeyPoolReq{
												PoolId:    poolID,
												Status:    string(status.KeyPoolStatusActive),
												StartDate: common.TwirpTime(time.Now().Add(-1 * time.Hour)),
												EndDate:   common.TwirpTime(time.Now().Add(1 * time.Hour)),
											})
											So(err, ShouldBeNil)

											Convey("Get key status should indicate that all keys are unclaimed", func() {
												for _, key := range keys {
													common.AssertKeyStatus(ctx, phantoClient, key, common.KeyStatus_Unclaimed)
												}

												Convey("Redeem key should succeed for each key", func() {
													for _, key := range keys {
														err := common.RedeemKey(ctx, phantoClient, key)
														So(err, ShouldBeNil)
													}
												})
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
