// +build integration

package key_redemption_by_batch_serial_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 100
	testClientID      = helix.TestClientID
)

func TestKeyRedemptionByBatchSerial(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetHelixAPIContext(true, testClientID)

		Convey("Handler group should create successfully", func() {
			handlerGroupID := common.CreateHandlerGroup(ctx, phantoClient, []string{}, []string{"client-id-1", testClientID, "client-id-3"})

			Convey("Keys should successfully generate ", func() {
				_, batchID := common.CreateBatchRedeemableKeyPoolAndGenerateKeys(ctx, phantoClient, handlerGroupID, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

				Convey("All keys should be claimable one at a time", func() {
					for i := int64(0); i < numKeysToGenerate; i++ {
						common.RedeemKeyByBatchAndAssertSuccess(ctx, phantoClient, batchID, phanto.KeyStatus_SUCCESSFULLY_REDEEMED, "", numKeysToGenerate-i-1)
					}

					Convey("Subsequent claims should fail", func() {
						for i := 0; i < numKeysToGenerate; i++ {
							common.RedeemKeyByBatchAndAssertError(ctx, phantoClient, batchID, "", redeem_key_by_batch_base.NoRemainingKeysError)
						}
					})
				})
			})
		})
	})
}
