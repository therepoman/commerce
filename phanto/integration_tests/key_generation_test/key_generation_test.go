// +build integration

package key_generation_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 300
)

func TestKeyGeneration(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetAuthenticatedContext(true, common.TestTUID)

		Convey("Key pool should create successfully", func() {
			poolID := common.CreateKeyPool(ctx, phantoClient, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), common.TestTUID, "", phanto.RedemptionMode_REDEEM_BY_KEY)

			Convey("Key batch generation should begin successfully", func() {
				batchID := common.StartGeneratingKeyBatch(ctx, phantoClient, poolID, numKeysToGenerate)

				Convey("Keys should finish generating successfully", func() {
					common.WaitForKeyBatchToFinishGenerating(ctx, phantoClient, batchID)

					Convey("Get key batch download info call should work", func() {
						resp, err := phantoClient.GetKeyBatchDownloadInfo(ctx, &phanto.GetKeyBatchDownloadInfoReq{
							BatchId: batchID,
						})
						So(err, ShouldBeNil)

						Convey("Downloading and decrypting key file should work", func() {
							keysReader := common.DownloadAndDecrypt(resp.DownloadUrl, resp.DecryptionKey)

							Convey("All keys should be present in file", func() {
								records, err := keysReader.ReadAll()
								So(err, ShouldBeNil)
								So(len(records), ShouldEqual, numKeysToGenerate)
								for _, record := range records {
									So(len(record), ShouldEqual, 1)
									common.AssertCorrectKeyFormat(record[0])
								}
							})
						})
					})
				})
			})
		})
	})
}
