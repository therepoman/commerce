// +build integration

package key_redemption_by_batch_auth_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/phanto/backend/api/base/helix"
	"code.justin.tv/commerce/phanto/backend/api/redeem_key_by_batch_base"
	"code.justin.tv/commerce/phanto/integration_tests/common"
	"code.justin.tv/commerce/phanto/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numKeysToGenerate = 5
	testClientID      = helix.TestClientID
)

func TestKeyRedemptionByBatchAuth(t *testing.T) {
	Convey("Given a phanto client", t, func() {
		phantoClient := common.GetPhantoClient(true)
		ctx := common.GetHelixAPIContext(true, testClientID)

		Convey("Handler group should create successfully", func() {
			handlerGroupID := common.CreateHandlerGroup(ctx, phantoClient, []string{}, []string{})

			Convey("Keys should successfully generate ", func() {
				_, batchID := common.CreateBatchRedeemableKeyPoolAndGenerateKeys(ctx, phantoClient, handlerGroupID, time.Now().Add(-1*time.Hour), time.Now().Add(1*time.Hour), numKeysToGenerate)

				Convey("Should get auth error attempting to claim keys", func() {
					for i := 0; i < numKeysToGenerate; i++ {
						common.RedeemKeyByBatchAndAssertError(ctx, phantoClient, batchID, "", redeem_key_by_batch_base.ClientNotAuthorizedError)
					}

					Convey("Handler should be updated with more clientIDs", func() {
						common.UpdateHandlerGroup(ctx, phantoClient, handlerGroupID, []string{}, []string{"test-client-id-1", "test-client-id-2"})

						Convey("Should still get auth error attempting to claim keys", func() {
							for i := 0; i < numKeysToGenerate; i++ {
								common.RedeemKeyByBatchAndAssertError(ctx, phantoClient, batchID, "", redeem_key_by_batch_base.ClientNotAuthorizedError)
							}

							Convey("Handler should be updated with the right clientID", func() {
								common.UpdateHandlerGroup(ctx, phantoClient, handlerGroupID, []string{}, []string{"test-client-id-1", testClientID, "test-client-id-2"})

								Convey("Should now be able to claim keys", func() {
									for i := int64(0); i < numKeysToGenerate; i++ {
										common.RedeemKeyByBatchAndAssertSuccess(ctx, phantoClient, batchID, phanto.KeyStatus_SUCCESSFULLY_REDEEMED, "", numKeysToGenerate-i-1)
									}

									Convey("Subsequent claims should fail", func() {
										for i := 0; i < numKeysToGenerate; i++ {
											common.RedeemKeyByBatchAndAssertError(ctx, phantoClient, batchID, "", redeem_key_by_batch_base.NoRemainingKeysError)
										}
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
