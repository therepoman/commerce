#!/bin/bash

set -e

DEPLOYER_VERSION="latest"

if [ -z "$SERVICE" ]
then
    echo "$$SERVICE is required!"
    exit 1
fi

if [ -z "$SHA" ]
then
    echo "$$SHA is required!"
    exit 1
fi

if [ -z "$CLUSTER" ]
then
    echo "$$CLUSTER is required!"
    exit 1
fi

if [ -z "$REGION" ]
then
    echo "$$REGION is required!"
    exit 1
fi

if [ -z "$AWS_ACCESS_KEY_ID" ]
then
    echo "$$AWS_ACCESS_KEY_ID is required!"
    exit 1
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]
then
    echo "$$AWS_SECRET_ACCESS_KEY is required!"
    exit 1
fi

echo "Deploying to $ENVIRONMENT..."

docker run \
 -e AWS_ACCESS_KEY=$AWS_ACCESS_KEY_ID \
 -e AWS_SECRET_KEY=$AWS_SECRET_ACCESS_KEY \
 docker.pkgs.xarth.tv/subs/deployer:$DEPLOYER_VERSION \
    -service=$SERVICE \
    -cluster=$CLUSTER \
    -task=$SERVICE \
    -region=$REGION \
    -tag=$SHA
