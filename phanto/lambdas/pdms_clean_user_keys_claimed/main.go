package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/dynamo/key"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms/clean_user_keys_claimed"
	"code.justin.tv/commerce/phanto/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

var environment string
var dryrun bool
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "CleanUserKeysClaimed"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringSliceFlag{
			Name:  "userIDs",
			Usage: "Runtime user IDs to clean user claimed keys for. Only works if local is set to true.",
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually call Dynamo to clean the user key claim records",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		log.WithError(err).Panic("error creating aws session")
	}

	keysDAO := key.NewDAO(sess, cfg.DynamoSuffix)

	handler := clean_user_keys_claimed.NewLambdaHandler(keysDAO)

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := handler.Handle(ctx, &pdms.CleanUserKeysClaimedRequest{
			UserDeletionRequestData: pdms.UserDeletionRequestData{
				UserIDs:  c.StringSlice("userIDs"),
				IsDryRun: dryrun,
			},
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(handler.Handle)
	}
}
