package main

import (
	"context"
	"net/http"
	"os"
	"time"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	twirplambdatransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms"
	"code.justin.tv/commerce/phanto/backend/lambda_handlers/pdms/report_deletion"
	"code.justin.tv/commerce/phanto/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	lambda_service "github.com/aws/aws-sdk-go/service/lambda"
	"github.com/codegangsta/cli"
)

var environment string
var dryrun bool
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "PDMSReportDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringSliceFlag{
			Name:  "userIDs",
			Usage: "Runtime user IDs to report for. Only works if local is set to true.",
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually call PDMS to report the user deletion.",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	// dedicating a session for just PDMS client
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: 10 * time.Second},
		},
	}))
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	creds := stscreds.NewCredentials(sess, cfg.PDMS.CallerRole)
	pdmsLambdaCli := lambda_service.New(sess, &aws.Config{Credentials: creds})
	pdmsTransport := twirplambdatransport.NewClient(pdmsLambdaCli, cfg.PDMS.LambdaARN)
	pdmsClient := pdms_service.NewPDMSServiceProtobufClient("", pdmsTransport)

	handler := report_deletion.NewLambdaHandler(pdmsClient)

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := handler.Handle(ctx, &pdms.ReportDeletionRequest{
			UserDeletionRequestData: pdms.UserDeletionRequestData{
				UserIDs:  c.StringSlice("userIDs"),
				IsDryRun: dryrun,
			},
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(handler.Handle)
	}
}
