![Phanto](/assets/phanto.png) ![Key](/assets/key.png) ![Phanto](/assets/phanto.png) ![Key](/assets/key.png) ![Phanto](/assets/phanto.png) ![Key](/assets/key.png) ![Phanto](/assets/phanto.png)

# Phanto

Twitch Product Key Service

[![Build Status](https://jenkins.internal.justin.tv/buildStatus/icon?job=commerce-phanto)](https://jenkins.internal.justin.tv/job/commerce-phanto/)
[![Go Report Card](https://goreportcard.internal.justin.tv/badge/code.justin.tv/commerce/phanto)](https://goreportcard.internal.justin.tv/report/code.justin.tv/commerce/phanto)

## Endpoints

| Environment | Endpoint |
| --- | --- |
| Staging | https://main.us-west-2.beta.phanto.twitch.a2z.com |
| Prod | https://main.us-west-2.prod.phanto.twitch.a2z.com |

## Retool
Retool manages versioning of the tools used by this repo. Individual tool source is stored in the _tools/ folder and their versions are stored in the tools.json file.

The source code for retool itself is also checked in under the _tools directory.

To install retool and build the correct version of all tools simply run:

```
make install_retool
```

To add a new tool:
```
_tools/bin/retool add [repo] [commit]
```

## Dep
~~Godep~~ ~~Glide~~ Dep is the definitive go dependency management tool.

To pull new dependencies into the vendor directory, make use of the dependency in the code and then run:
```sh
make dep
```

## Twirp
[Twirp](https://git.xarth.tv/common/twirp) is a Twitch internal RPC system for service to service communication.

To add a new API, define the requests and responses in the `phanto-service.proto` file and then run:
```sh
make generate_twirp
```

## Docker
Once you're sshed into a phanto EC2 host

Change to root (since docker runs as root):

```
sudo su
```

List out running docker containers:

```
docker ps
```

Open a shell with a given container:

```
docker exec -it [container-id] bash
```

## Terraform
Terraform is used to manage all AWS resources (beanstalk, dynamodb, etc).

To apply terraform changes navigate to the directory corresponding to the environment you want to modify (ex: `terraform/staging`).

First initialize your terraform environment.
```
terraform init
```

Next, generate a plan file.
```
terraform plan -out newconfig.plan
```

Once you've reviewed the plan and are confident in your change, apply it:
```
terraform apply "newconfig.plan"
```

![Phanto](/assets/phanto.png) ![Key](/assets/key.png) ![Phanto](/assets/phanto.png) ![Key](/assets/key.png) ![Phanto](/assets/phanto.png) ![Key](/assets/key.png) ![Phanto](/assets/phanto.png)
