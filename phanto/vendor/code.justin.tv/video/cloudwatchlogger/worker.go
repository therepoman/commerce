package cloudwatchlogs

import (
	"context"
	"fmt"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	cwl "github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	cwliface "github.com/aws/aws-sdk-go/service/cloudwatchlogs/cloudwatchlogsiface"
)

type cloudwatchLogsWorkerHooks struct {
	eventsSentSuccess func(input *cwl.PutLogEventsInput)
	eventsSentFailure func(input *cwl.PutLogEventsInput, err error)
}

type cloudwatchLogsWorkerConfig struct {
	cloudwatchLogs cwliface.CloudWatchLogsAPI

	logGroupName  string
	logStreamName string

	eventsBatchChannel <-chan []*cwl.InputLogEvent

	hooks *cloudwatchLogsWorkerHooks
}

type cloudwatchLogsWorker struct {
	config *cloudwatchLogsWorkerConfig

	sequenceTokenLock sync.Mutex
	sequenceToken     string
}

func newCloudwatchLogsWorker(c *cloudwatchLogsWorkerConfig, sequenceToken string) (*cloudwatchLogsWorker, error) {
	cwlw := &cloudwatchLogsWorker{
		config:        c,
		sequenceToken: sequenceToken,
	}

	return cwlw, nil
}

func (w *cloudwatchLogsWorker) Run(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			return fmt.Errorf("exiting cloudwatch logs worker: %w", ctx.Err())
		case batch := <-w.config.eventsBatchChannel:
			w.send(ctx, batch)
		}
	}
}

func (w *cloudwatchLogsWorker) send(ctx context.Context, batch []*cwl.InputLogEvent) error {
	w.sequenceTokenLock.Lock()
	defer w.sequenceTokenLock.Unlock()

	putInput := &cwl.PutLogEventsInput{
		LogEvents:     batch,
		LogGroupName:  aws.String(w.config.logGroupName),
		LogStreamName: aws.String(w.config.logStreamName),
	}

	if w.sequenceToken != "" {
		putInput.SequenceToken = aws.String(w.sequenceToken)
	}

	out, err := w.config.cloudwatchLogs.PutLogEventsWithContext(ctx, putInput)
	if err == nil {
		w.sequenceToken = aws.StringValue(out.NextSequenceToken)
		w.config.hooks.eventsSentSuccess(putInput)

		return nil
	} else {
		w.config.hooks.eventsSentFailure(putInput, err)

		// check if it's one of the cloudwatch logs error types which returns
		// an expected sequence token in the response
		if e, ok := err.(*cwl.DataAlreadyAcceptedException); ok {
			newST := aws.StringValue(e.ExpectedSequenceToken)
			w.sequenceToken = newST
		} else if e, ok := err.(*cwl.InvalidSequenceTokenException); ok {
			newST := aws.StringValue(e.ExpectedSequenceToken)
			w.sequenceToken = newST
		}

		return err
	}
}
