package cloudwatchlogs

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	cwl "github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	cwliface "github.com/aws/aws-sdk-go/service/cloudwatchlogs/cloudwatchlogsiface"

	"code.justin.tv/video/invoker"
)

const (
	invalidSequenceToken = "invalidSequenceToken"
	invalidMessage       = "invalidMessage"
	invalidTimestamp     = int64(0)
)

const (
	MaxBatchEventCount      = 10000
	maxBatchBytes           = 1048576
	batchEventOverheadBytes = 26
	shutdownTimeout         = 5 * time.Second
	createWorkersTimeout    = 3 * time.Minute
)

// Config
type EventHooks struct {
	InputDroppedBufferExceeded func(messages []string)
	BatchDroppedBufferExceeded func(batch []*cwl.InputLogEvent)
	EventsSentSuccess          func(input *cwl.PutLogEventsInput)
	EventsSentFailure          func(input *cwl.PutLogEventsInput, err error)
}

func (h *EventHooks) fill() {
	if h.InputDroppedBufferExceeded == nil {
		h.InputDroppedBufferExceeded = func([]string) {}
	}

	if h.BatchDroppedBufferExceeded == nil {
		h.BatchDroppedBufferExceeded = func([]*cwl.InputLogEvent) {}
	}

	if h.EventsSentSuccess == nil {
		h.EventsSentSuccess = func(input *cwl.PutLogEventsInput) {}
	}

	if h.EventsSentFailure == nil {
		h.EventsSentFailure = func(input *cwl.PutLogEventsInput, err error) {}
	}
}

type Config struct {
	// Interfaces
	CloudwatchLogs cwliface.CloudWatchLogsAPI

	// Config
	LogGroupName              string
	InstanceIdentifier        string
	CloudwatchLogsWorkerCount int
	InputBufferLength         int
	BatchBufferLength         int
	MaxBatchSize              int
	MaxBatchDuration          time.Duration

	// Hooks
	EventHooks EventHooks
}

func (c *Config) validate() error {
	if c.CloudwatchLogsWorkerCount < 1 {
		return fmt.Errorf("invalid worker count")
	}
	return nil
}

func (c *Config) fill() {
	if c.MaxBatchSize > MaxBatchEventCount || c.MaxBatchSize <= 0 {
		c.MaxBatchSize = MaxBatchEventCount
	}

	if c.MaxBatchDuration > 24*time.Hour {
		c.MaxBatchDuration = 24 * time.Hour
	}

	c.EventHooks.fill()
}

// Sender
type Sender struct {
	config *Config

	inputChannel   chan *string
	batchedChannel chan []*cwl.InputLogEvent

	sendWorkers map[string]*cloudwatchLogsWorker
}

// NewSender creates a new sender
func NewSender(c *Config) (*Sender, error) {
	if err := c.validate(); err != nil {
		return nil, fmt.Errorf("config failed validation: %w", err)
	}
	c.fill()

	s := &Sender{
		config:         c,
		inputChannel:   make(chan *string, c.InputBufferLength),
		batchedChannel: make(chan []*cwl.InputLogEvent, c.BatchBufferLength),
		sendWorkers:    map[string]*cloudwatchLogsWorker{},
	}

	createWorkersCtx, createWorkersCancel := context.WithTimeout(context.Background(), createWorkersTimeout)
	defer createWorkersCancel()

	// fetch sequence tokens
	sequenceTokens, err := s.fetchSequenceTokens(createWorkersCtx, c.LogGroupName)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch sequence tokens: %w", err)
	}

	// the cloudwatchlogs sender spins up a bunch of workers because writing to a cloudwatch
	// logs stream requires a sequenceToken and is therefore a sequential process. The horizontally
	// scalable unit of cloudwatch logs for additional write capacity is a log stream within a log group.
	// the sender spins up a preconfigured number of workers for each log group, each of which
	// owns and writes to a single log stream.
	for logStreamName, sequenceToken := range sequenceTokens {
		workerConfig := &cloudwatchLogsWorkerConfig{
			cloudwatchLogs:     c.CloudwatchLogs,
			logGroupName:       c.LogGroupName,
			logStreamName:      logStreamName,
			eventsBatchChannel: s.batchedChannel,
			hooks: &cloudwatchLogsWorkerHooks{
				eventsSentSuccess: c.EventHooks.EventsSentSuccess,
				eventsSentFailure: c.EventHooks.EventsSentFailure,
			},
		}

		worker, err := newCloudwatchLogsWorker(workerConfig, sequenceToken)
		if err != nil {
			return nil, fmt.Errorf("failed to create cw logs worker: %w", err)
		}

		s.sendWorkers[logStreamName] = worker
	}

	return s, nil
}

// Run function for the sender. Must be called to start processing
func (s *Sender) Run(ctx context.Context) error {
	inv := invoker.New()
	inv.Add(s.batchInput)

	for _, worker := range s.sendWorkers {
		inv.Add(worker.Run)
	}

	return inv.Run(ctx)
}

// Add messages to the batcher queue to be sent
func (s *Sender) Send(ctx context.Context, messages []string) error {
	for idx, m := range messages {
		select {
		case <-ctx.Done():
			s.config.EventHooks.InputDroppedBufferExceeded(messages[idx:])
			return ctx.Err()
		case s.inputChannel <- aws.String(m):
		}
	}

	return nil
}

func (s *Sender) batchInput(ctx context.Context) error {
	batch := []*cwl.InputLogEvent{}
	batchFirstEventTime := time.Time{}
	batchSizeBytes := 0

	timedFlushTimer := time.NewTimer(0)
	if !timedFlushTimer.Stop() && len(timedFlushTimer.C) > 0 {
		<-timedFlushTimer.C
	}

	for {
		select {
		case <-ctx.Done():
			flushContext, flushCancel := context.WithTimeout(context.Background(), shutdownTimeout)
			defer flushCancel()

			// grab the first send worker we can get
			for _, worker := range s.sendWorkers {
				err := worker.send(flushContext, batch)
				if err != nil {
					return fmt.Errorf("failed final flush: %w", err)
				}

				break
			}

			return fmt.Errorf("exiting batch runner: %w", ctx.Err())
		case <-timedFlushTimer.C:
			if !timedFlushTimer.Stop() && len(timedFlushTimer.C) > 0 {
				<-timedFlushTimer.C
			}

			// pass batch to worker without blocking
			select {
			case s.batchedChannel <- batch:
			default:
				s.config.EventHooks.BatchDroppedBufferExceeded(batch)
			}

			// reset batch
			batch = []*cwl.InputLogEvent{}
			batchFirstEventTime = time.Time{}
			batchSizeBytes = 0
		case message := <-s.inputChannel:
			// three batch exceeded conditions are:
			// 1. batch event count > max
			// 2. batch bytes len > max
			// 3. batch cannot have events taking place over a day between them (handled in the previous select)
			if len(batch)+1 > s.config.MaxBatchSize || batchSizeBytes+len(aws.StringValue(message)) > maxBatchBytes {
				// pass batch to worker without blocking
				select {
				case s.batchedChannel <- batch:
				default:
					s.config.EventHooks.BatchDroppedBufferExceeded(batch)
				}

				// reset batch
				batch = []*cwl.InputLogEvent{}
				batchFirstEventTime = time.Time{}
				batchSizeBytes = 0
				if !timedFlushTimer.Stop() && len(timedFlushTimer.C) > 0 {
					<-timedFlushTimer.C
				}
			}

			// add current message to the batch
			now := time.Now()
			event := &cwl.InputLogEvent{
				Message:   message,
				Timestamp: aws.Int64(now.UnixNano() / int64(time.Millisecond)),
			}

			batch = append(batch, event)
			batchSizeBytes += len(aws.StringValue(message)) + batchEventOverheadBytes

			// if this is the first message in a new batch
			if batchFirstEventTime.IsZero() {
				batchFirstEventTime = now

				if !timedFlushTimer.Stop() && len(timedFlushTimer.C) > 0 {
					<-timedFlushTimer.C
				}
				timedFlushTimer.Reset(s.config.MaxBatchDuration)
			}
		}
	}
}

// fetch starting sequence tokens for every worker log stream and create the worker log
// stream if necessary. returns a map of log stream name to sequence token
func (s *Sender) fetchSequenceTokens(ctx context.Context, logGroupName string) (map[string]string, error) {
	sequenceTokens := map[string]string{}

	// fetch sequence tokens for streams which exist
	sequenceTokenInv := invoker.New()
	for idx := 0; idx < s.config.CloudwatchLogsWorkerCount; idx++ {
		// generate log stream name
		logStreamName := s.config.InstanceIdentifier
		if idx > 0 {
			logStreamName += fmt.Sprintf("/%d", idx)
		}
		sequenceTokens[logStreamName] = invalidSequenceToken

		// add fill function
		sequenceTokenInv.Add(s.fillSequenceTokenRunnable(logGroupName, logStreamName, sequenceTokens))
	}
	err := sequenceTokenInv.Run(ctx)
	if err != nil {
		return sequenceTokens, fmt.Errorf("failed to fill sequence tokens: %w", err)
	}

	// create log streams for those which don't exist
	streamsToCreate := []string{}
	for logStreamName, sequenceToken := range sequenceTokens {
		if sequenceToken != invalidSequenceToken {
			continue
		}

		streamsToCreate = append(streamsToCreate, logStreamName)
		sequenceTokens[logStreamName] = ""
	}

	if len(streamsToCreate) > 0 {
		streamInv := invoker.New()
		for _, logStreamName := range streamsToCreate {
			streamInv.Add(s.createLogStreamRunnable(logGroupName, logStreamName))
		}

		err = streamInv.Run(ctx)
		if err != nil {
			return sequenceTokens, fmt.Errorf("failed to create log streams: %w", err)
		}
	}

	return sequenceTokens, nil
}

func (s *Sender) fillSequenceTokenRunnable(logGroupName string, logStreamName string, sequenceTokens map[string]string) invoker.Task {
	return func(ctx context.Context) error {
		in := &cwl.PutLogEventsInput{
			LogGroupName:  aws.String(logGroupName),
			LogStreamName: aws.String(logStreamName),
			LogEvents: []*cwl.InputLogEvent{
				&cwl.InputLogEvent{
					Message:   aws.String(invalidMessage),
					Timestamp: aws.Int64(invalidTimestamp),
				},
			},
			SequenceToken: aws.String(invalidSequenceToken), // intentionally bad SequenceToken
		}

		o, err := s.config.CloudwatchLogs.PutLogEvents(in)
		if err == nil {
			// weird but okay
			sequenceTokens[logStreamName] = aws.StringValue(o.NextSequenceToken)
		} else {
			// parse error. if it matches a type which returns a sequencetoken return it otherwise error
			if e, ok := err.(*cwl.DataAlreadyAcceptedException); ok {
				sequenceTokens[logStreamName] = aws.StringValue(e.ExpectedSequenceToken)
			} else if e, ok := err.(*cwl.InvalidSequenceTokenException); ok {
				sequenceTokens[logStreamName] = aws.StringValue(e.ExpectedSequenceToken)
			}
		}

		return nil
	}
}

func (s *Sender) createLogStreamRunnable(logGroupName string, logStreamName string) invoker.Task {
	return func(ctx context.Context) error {
		createIn := &cwl.CreateLogStreamInput{
			LogGroupName:  aws.String(logGroupName),
			LogStreamName: aws.String(logStreamName),
		}

		_, err := s.config.CloudwatchLogs.CreateLogStreamWithContext(ctx, createIn)
		if err != nil {
			return fmt.Errorf("failed to create log stream [%s]: %w", logStreamName, err)
		}

		return nil
	}
}
