package spadedconf

import (
	"time"

	"code.justin.tv/hygienic/distconf"
	"code.justin.tv/hygienic/spade"
)

// Config allows configuring a Spade client using distconf.
type Config struct {
	SpadeHost           *distconf.Str
	BatchSize           *distconf.Int
	Timeout             *distconf.Duration
	BacklogSize         *distconf.Int
	Concurrency         *distconf.Int
	CombineSmallBatches *distconf.Bool
}

var _ spade.Config = &Config{}

// Load initializes a Config instance using the given distconf.
func (c *Config) Load(dconf *distconf.Distconf) error {
	c.SpadeHost = dconf.Str("spade.endpoint", "https://spade.internal.di.twitch.a2z.com")
	c.BatchSize = dconf.Int("spade.batch-size", 32)
	c.Timeout = dconf.Duration("spade.timeout", time.Second*10)
	c.BacklogSize = dconf.Int("spade.backlog-size", 1024)
	c.Concurrency = dconf.Int("spade.concurrency", 1)
	c.CombineSmallBatches = dconf.Bool("spade.combine-small-batches", false)
	return nil
}

// GetSpadeHost returns a URL to the Spade server that tracking events should be sent to.
func (c *Config) GetSpadeHost() string {
	return c.SpadeHost.Get()
}

// GetBatchSize returns the number of tracking events that the client will send in a single request to the
// Spade server.
func (c *Config) GetBatchSize() int64 {
	return c.BatchSize.Get()
}

// GetTimeout returns the amount of time the client will wait for a request to the Spade server to complete.
func (c *Config) GetTimeout() time.Duration {
	return c.Timeout.Get()
}

// GetBacklogSize returns the number of tracking events the Spade client will queue up before dropping events.
func (c *Config) GetBacklogSize() int64 {
	return c.BacklogSize.Get()
}

// GetConcurrency returns the number of goroutines used to process queued events.
func (c *Config) GetConcurrency() int64 {
	return c.Concurrency.Get()
}

// CombineSmallBatchesEnabled indicates whether the feature to collect small batches before sending to Spade
// is enabled.
func (c *Config) CombineSmallBatchesEnabled() bool {
	return c.CombineSmallBatches.Get()
}
