package splatter

import (
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/cactus/go-statsd-client/statsd"
)

// CompositeStatter takes a slice of statsd.Statters and will log metrics to all of them.
// This allows metrics to be sent to multiple destinations while still using the same
// Statter interface.
//
// An example usage would be to input a standard statsd.Statter implementation and a
// TwitchTelemetry statter and have this CompositeStatter send the metrics to both destinations.
type CompositeStatter struct {
	statters []statsd.Statter
}

// NewCompositeStatter returns a pointer to a new CompositeStatter, and an error.
func NewCompositeStatter(statters []statsd.Statter) (statsd.Statter, error) {
	return &CompositeStatter{
		statters: statters,
	}, nil
}

// Inc increments a statsd count type.
// stat is a string name for the metric.
// value is the integer value
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) Inc(stat string, value int64, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Inc(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// Dec decrements a statsd count type.
// stat is a string name for the metric.
// value is the integer value.
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) Dec(stat string, value int64, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Dec(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// Gauge submits/updates a statsd gauge type.
// stat is a string name for the metric.
// value is the integer value.
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) Gauge(stat string, value int64, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Gauge(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// GaugeDelta submits a delta to a statsd gauge.
// stat is the string name for the metric.
// value is the (positive or negative) change.
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) GaugeDelta(stat string, value int64, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.GaugeDelta(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// Timing submits a statsd timing type.
// stat is a string name for the metric.
// delta is the time duration value in milliseconds
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) Timing(stat string, delta int64, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Timing(stat, delta, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// TimingDuration submits a statsd timing type.
// stat is a string name for the metric.
// delta is the timing value as time.Duration
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) TimingDuration(stat string, delta time.Duration, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.TimingDuration(stat, delta, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// Set submits a stats set type
// stat is a string name for the metric.
// value is the string value
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) Set(stat string, value string, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Set(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// SetInt submits a number as a stats set type.
// stat is a string name for the metric.
// value is the integer value
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) SetInt(stat string, value int64, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.SetInt(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// Raw submits a preformatted value.
// stat is the string name for the metric.
// value is a preformatted "raw" value string.
// rate is the sample rate (0.0 to 1.0).
func (s *CompositeStatter) Raw(stat string, value string, rate float32) error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Raw(stat, value, rate)
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

// SetSamplerFunc sets a sampler function to something other than the default
// sampler is a function that determines whether the metric is
// to be accepted, or discarded.
// An example use case is for submitted pre-sampled metrics.
func (s *CompositeStatter) SetSamplerFunc(sampler statsd.SamplerFunc) {
	for _, statter := range s.statters {
		subStatter, ok := statter.(statsd.SubStatter)
		if !ok {
			log.Errorf("Unable to convert Statter to SubStatter: %v", statter)
			continue
		}
		subStatter.SetSamplerFunc(sampler)
	}
}

// SetPrefix sets/updates the statsd client prefix.
// Note: Does not change the prefix of any SubStatters.
func (s *CompositeStatter) SetPrefix(prefix string) {
	for _, statter := range s.statters {
		statter.SetPrefix(prefix)
	}
}

// NewSubStatter returns a SubStatter with appended prefix
func (s *CompositeStatter) NewSubStatter(prefix string) statsd.SubStatter {
	var c *CompositeStatter
	if s != nil {

		newStatters := []statsd.Statter{}
		for _, statter := range s.statters {
			newSubStatter := statter.NewSubStatter(prefix)
			newStatter, ok := newSubStatter.(statsd.Statter)
			if !ok {
				log.Errorf("Unable to create SubStatter from Statter: %v", newSubStatter)
				continue
			}
			newStatters = append(newStatters, newStatter)
		}

		c = &CompositeStatter{
			statters: newStatters,
		}
	}
	return c
}

// Close closes the connection and cleans up.
func (s *CompositeStatter) Close() error {
	var firstErr error
	for _, statter := range s.statters {
		err := statter.Close()
		if err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}
