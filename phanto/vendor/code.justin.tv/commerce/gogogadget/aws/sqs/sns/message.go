package sns

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"
)

type Message struct {
	Type              string                      `json:"Type"`
	MessageId         string                      `json:"MessageId"`
	Token             string                      `json:"Token,optional"`
	TopicArn          string                      `json:"TopicArn"`
	Subject           string                      `json:"Subject,optional"`
	Message           string                      `json:"Message"`
	SubscribeURL      string                      `json:"SubscribeURL,optional"`
	Timestamp         time.Time                   `json:"Timestamp"`
	SignatureVersion  string                      `json:"SignatureVersion"`
	Signature         string                      `json:"Signature"`
	SigningCertURL    string                      `json:"SigningCertURL"`
	UnsubscribeURL    string                      `json:"UnsubscribeURL,optional"`
	MessageAttributes map[string]MessageAttribute `json:"MessageAttributes,optional"`
}

type MessageAttribute struct {
	Type  string `json:"Type"`
	Value string `json:"Value"`
}

func FromJSON(jsonStr string) (*Message, error) {
	var msg Message
	err := json.Unmarshal([]byte(jsonStr), &msg)
	if err != nil {
		return nil, err
	}
	return &msg, nil
}

func Extract(sqsMsg *sqs.Message) (Message, error) {
	if sqsMsg == nil || sqsMsg.Body == nil {
		return Message{}, errors.New("received nil sqs message")
	}

	snsMsg, err := FromJSON(*sqsMsg.Body)
	if err != nil {
		return Message{}, errors.New("error decoding json sqs body")
	}

	if snsMsg == nil {
		return Message{}, errors.New("decoded nil sns message from sqs body")
	}

	return *snsMsg, nil
}
