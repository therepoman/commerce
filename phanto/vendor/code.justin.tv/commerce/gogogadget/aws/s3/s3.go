package s3

import (
	"context"
	"io"
	"io/ioutil"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	defaultEndpoint         = "s3-us-west-2.amazonaws.com"
	defaultRegion           = "us-west-2"
	defaultS3ForcePathStyle = true
)

type Client interface {
	GetFile(ctx context.Context, bucket string, key string) (GetFileResponse, error)
	PutFile(ctx context.Context, bucket string, key string, file io.ReadSeeker) error
	GetTempSignedURL(ctx context.Context, bucket string, key string, expiresIn time.Duration) (string, error)
	PutFileWithExpirationDate(ctx context.Context, bucket string, key string, file io.ReadSeeker, expirationDate *time.Time) error
}

type SDKClientWrapper struct {
	SDKClient *s3.S3
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region:           aws.String(defaultRegion),
		Endpoint:         aws.String(defaultEndpoint),
		S3ForcePathStyle: aws.Bool(defaultS3ForcePathStyle),
	}
}

func NewDefaultClient() Client {
	return &SDKClientWrapper{
		SDKClient: s3.New(session.New(), NewDefaultConfig()),
	}
}

func NewClientFromSession(sess *session.Session, cfg *aws.Config) Client {
	return &SDKClientWrapper{
		SDKClient: s3.New(sess, cfg),
	}
}

type GetFileResponse struct {
	File   []byte
	Exists bool
}

func fileNotFoundResponse() GetFileResponse {
	return GetFileResponse{
		Exists: false,
	}
}

func (c *SDKClientWrapper) GetTempSignedURL(ctx context.Context, bucket string, key string, expiresIn time.Duration) (string, error) {
	req, _ := c.SDKClient.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	url, err := req.Presign(expiresIn)
	if err != nil {
		return "", err
	}
	return url, nil
}

func (c *SDKClientWrapper) GetFile(ctx context.Context, bucket string, key string) (GetFileResponse, error) {
	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	out, err := c.SDKClient.GetObjectWithContext(ctx, input)
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == s3.ErrCodeNoSuchKey {
			return fileNotFoundResponse(), nil
		}
		return fileNotFoundResponse(), err
	}

	defer out.Body.Close()
	bytes, err := ioutil.ReadAll(out.Body)
	if err != nil {
		return fileNotFoundResponse(), err
	}

	return GetFileResponse{
		File:   bytes,
		Exists: true,
	}, nil
}

func (c *SDKClientWrapper) PutFile(ctx context.Context, bucket string, key string, file io.ReadSeeker) error {
	input := &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   file,
	}
	_, err := c.SDKClient.PutObjectWithContext(ctx, input)
	return err
}

func (c *SDKClientWrapper) PutFileWithExpirationDate(ctx context.Context, bucket string, key string, file io.ReadSeeker, expirationDate *time.Time) error {
	input := &s3.PutObjectInput{
		Bucket:  aws.String(bucket),
		Key:     aws.String(key),
		Body:    file,
		Expires: expirationDate,
	}
	_, err := c.SDKClient.PutObjectWithContext(ctx, input)
	return err
}
