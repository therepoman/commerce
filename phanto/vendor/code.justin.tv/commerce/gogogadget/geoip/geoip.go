package geoip

import (
	"net"

	"github.com/oschwald/maxminddb-golang"
)

const (
	US_ISO = "US"
)

type Locator interface {
	Locate(ip net.IP) (Data, error)
	Verify() error
	Close() error
}

type noOpLocator struct {
}

func (l *noOpLocator) Locate(ip net.IP) (Data, error) {
	return Data{}, nil
}

func (l *noOpLocator) Verify() error {
	return nil
}

func (l *noOpLocator) Close() error {
	return nil
}

func NewNoOpLocator() Locator {
	return &noOpLocator{}
}

type maxMindLocator struct {
	reader *maxminddb.Reader
}

type Names struct {
	English string `maxminddb:"en"`
}

type Location struct {
	Latitude  float64 `maxminddb:"latitude"`
	Longitude float64 `maxminddb:"longitude"`
}

type Postal struct {
	Code string `maxminddb:"code"`
}

type Country struct {
	ISOCode string `maxminddb:"iso_code"`
	Names   Names  `maxminddb:"names"`
}

type City struct {
	GeonameID int64 `maxminddb:"geoname_id"`
	Names     Names `maxminddb:"names"`
}

type Subdivision struct {
	GeonameID int64  `maxminddb:"geoname_id"`
	ISOCode   string `maxminddb:"iso_code"`
	Names     Names  `maxminddb:"names"`
}

func (d Data) CountryISOCode() string {
	return d.Country.ISOCode
}

func (d Data) StateISOCode() string {
	if d.Country.ISOCode == US_ISO && len(d.Subdivisions) >= 1 {
		return d.Subdivisions[0].ISOCode
	}
	return ""
}

type Data struct {
	Location     Location      `maxminddb:"location"`
	Postal       Postal        `maxminddb:"postal"`
	Country      Country       `maxminddb:"country"`
	City         City          `maxminddb:"city"`
	Subdivisions []Subdivision `maxminddb:"subdivisions"`
}

func NewLocatorFromMaxMindDBFilePath(filename string) (Locator, error) {
	reader, err := maxminddb.Open(filename)
	if err != nil {
		return nil, err
	}

	return &maxMindLocator{
		reader: reader,
	}, nil

}

func NewLocatorFromMaxMindDBFileContents(fileContents []byte) (Locator, error) {
	reader, err := maxminddb.FromBytes(fileContents)
	if err != nil {
		return nil, err
	}

	return &maxMindLocator{
		reader: reader,
	}, nil

}

func (l *maxMindLocator) Locate(ip net.IP) (Data, error) {
	var data Data
	err := l.reader.Lookup(ip, &data)
	return data, err
}

func (l *maxMindLocator) Verify() error {
	return l.reader.Verify()
}

func (l *maxMindLocator) Close() error {
	return l.reader.Close()
}
