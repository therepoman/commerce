package retrycount

import (
	"context"
)

type key string

const retryCountKey = key("RetryCount")

func Set(ctx context.Context, count int) context.Context {
	return context.WithValue(ctx, retryCountKey, count)
}

func Count(ctx context.Context) (i int, ok bool) {
	i, ok = ctx.Value(retryCountKey).(int)
	return i, ok
}
