//go:generate protoc -I "." -I "./vendor" --go_out=. ./internal/wire/header.proto

// package eventbus is the base package for the Event Bus Go client.
package eventbus
