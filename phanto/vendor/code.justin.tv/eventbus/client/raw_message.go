package eventbus

import (
	"time"

	uuid "github.com/gofrs/uuid"
)

// RawMessage is a partially decoded message coming from a subscriber implementation.
type RawMessage struct {
	Header  *Header
	Payload []byte
}

// Header describes some aspects of the message
type Header struct {
	// A v4 UUID that is assigned by the publisher.
	// This UUID can be used to implement your own deduplication logic
	MessageID uuid.UUID

	// The event type encapsulated in this message, as defined in the schema library.
	EventType string

	// When this message was created by the publisher library.
	CreatedAt time.Time

	// Environment (optional, can be empty) the environment this event was published from (local, development, staging, production)
	Environment string
}
