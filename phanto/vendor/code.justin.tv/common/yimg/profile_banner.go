package yimg

import (
	"fmt"
)

const profileBannerFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/%s-profile_banner-{{.Uid}}-{{.Size}}.{{.Format}}"
const profileBannerNewFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/{{.Uid}}-profile_banner-{{.Size}}.{{.Format}}"

var ProfileBannerSizes = []string{"480"}

// ProfileBanners converts database yaml into an Images map for the provided username
func ProfileBanners(data []byte, username string) (Images, error) {
	return parse(fmt.Sprintf(profileBannerFormat, username), profileBannerNewFormat, data, ProfileBannerSizes, byHeight)
}

func ProfileBannersToString(images *Images) (*string, error) {
	return images.Marshal(nil)
}

func ProfileBannersName(image Image, username string) (string, error) {
	if image.NewFormat {
		return fmt.Sprintf("%s-profile_banner-%s.%s", image.Uid, image.Size, image.Format), image.validate()
	}
	return fmt.Sprintf("%s-profile_banner-%s-%s.%s", username, image.Uid, image.Size, image.Format), image.validate()
}
