package goauthorization

import (
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/common/jwt"
	"code.justin.tv/common/jwt/claim"
)

// Decoder implements the methods of a decoder
type Decoder interface {
	Decode(string) (*AuthorizationToken, error)
	ParseToken(*http.Request) (*AuthorizationToken, error)
	Validate(*AuthorizationToken, CapabilityClaims) error
}

type decoderImpl struct {
	aud string
	iss string
	alg jwt.Algorithm
	hdr jwt.Header
}

// NewDecoder instantiates a new Decoder
func NewDecoder(alg, aud, iss string, val []byte) (Decoder, error) {
	dec := &decoderImpl{
		aud: aud,
		iss: iss,
	}

	switch alg {
	case hmacAlg:
		dec.alg = jwt.HS512(val)
	case rsaAlg:
		key, err := jwt.ParseRSAPublicKey(val)
		if err != nil {
			return dec, err
		}
		dec.alg = jwt.RSAValidateOnly(jwt.RS256, key)
	case eccAlg:
		key, err := jwt.ParseECPublicKeyFromPEM(val)
		if err != nil {
			return dec, err
		}
		dec.alg = jwt.ECDSAValidateOnly(jwt.ES256, key)
	}
	dec.hdr = jwt.NewHeader(dec.alg)

	return dec, nil
}

// Decode unmarshals a serialized authorization token and validates basic
// claims like expiration and not before time
func (d *decoderImpl) Decode(serialized string) (*AuthorizationToken, error) {
	tkn := AuthorizationToken{Algorithm: d.alg, Header: d.hdr}
	err := jwt.DecodeAndValidate(&tkn.Header, &tkn.Claims, tkn.Algorithm, []byte(serialized))
	return &tkn, err
}

// ParseToken extracts an authorization token from the "Twitch-Authorization"
// header
func (d *decoderImpl) ParseToken(r *http.Request) (*AuthorizationToken, error) {
	serialized := r.Header.Get("Twitch-Authorization")
	if serialized == "" {
		return nil, ErrNoAuthorizationToken
	}
	return d.Decode(serialized)
}

type expValid struct {
	Exp claim.Exp
}

type nbfValid struct {
	Nbf claim.Nbf
}

// Validate validates the default claims and given capabilities
func (d *decoderImpl) Validate(t *AuthorizationToken, capabilities CapabilityClaims) error {
	validatedClaims := t.validateClaims(d.aud, d.iss)
	validatedCaps := t.validateCapabilities(capabilities)

	var err error
	if err = validatedClaims; err != nil {
		return err
	}
	err = validatedCaps
	return err
}

func (t *AuthorizationToken) validateClaims(aud, iss string) error {
	if !claim.Validate(expValid{Exp: t.Claims.Expires}) {
		return fmt.Errorf("%s: %v", ErrExpiredToken, time.Time(t.Claims.Expires))
	}
	if !claim.Validate(nbfValid{Nbf: t.Claims.NotBefore}) {
		return fmt.Errorf("%s: %v", ErrInvalidNbf, time.Time(t.Claims.NotBefore))
	}
	if !t.validAudience(aud) {
		return fmt.Errorf("%s: expected %v, got %v", ErrInvalidAudience, aud, t.Claims.Audience)
	}
	if string(t.Claims.Issuer) != iss {
		return fmt.Errorf("%s: expected %v, got %v", ErrInvalidIssuer, iss, t.Claims.Issuer)
	}

	return nil
}

func (t *AuthorizationToken) validateCapabilities(capabilities CapabilityClaims) error {
	for capName, capArgs := range capabilities {
		tokenAuthorizations := t.Claims.Authorizations[capName]
		if tokenAuthorizations == nil {
			return fmt.Errorf("%s: %s", ErrMissingCapability, capName)
		}
		for arg, argv := range capArgs {
			tokenArg := tokenAuthorizations[arg]

			var compare interface{}
			switch tokenArg.(type) {
			case float64:
				compare = int(tokenArg.(float64))
			default:
				compare = tokenArg
			}

			if compare != argv {
				return fmt.Errorf("%s: expected %v, got %v", ErrInvalidCapability, argv, tokenAuthorizations[arg])
			}
		}
	}

	return nil
}

func (t *AuthorizationToken) validAudience(audience string) bool {
	for _, v := range t.Claims.Audience {
		if v == audience {
			return true
		}
	}
	return false
}
