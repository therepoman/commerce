package s2stoken

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

func parseAuthServerError(r io.Reader) string {
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return "connection closed: " + err.Error()
	}

	var ase struct {
		Message string `json:"message"`
	}
	if err := json.Unmarshal(bs, &ase); err == nil && ase.Message != "" {
		return ase.Message
	}

	return string(bs)
}
