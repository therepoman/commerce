package s2s2err

import "fmt"

//S2S2ErrorURL is the Url for the s2s2 error page with the generated section link, section is added in the string method"
const S2S2ErrorURL = "https://wiki.twitch.com/display/SSE/S2S2+Errors#S2S2Errors-"

//S2S2ErrorString returns the original error wrapped with the S2S2Error help page URL
func S2S2ErrorString(message string, section string) string {

	msg := fmt.Sprintf("%s Error: Please check for solutions at: %s%s", section, S2S2ErrorURL, section)
	if message != "" {
		msg += fmt.Sprintf(" caused by: %s", message)
	}
	return msg
}
