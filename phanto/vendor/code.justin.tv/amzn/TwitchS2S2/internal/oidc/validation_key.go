package oidc

// ValidationKey implements the interface required to validate an access token
// sent by a subject.
type ValidationKey interface {
	Size() int
	Name() string
	Validate(value, signature []byte) error
}
