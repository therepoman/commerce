package s2s2

import (
	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/internal/authorization"
	"code.justin.tv/amzn/TwitchS2S2/internal/token"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/s2s2dicallee"
)

// AuthorizedSubject is the interface that authenticated and authorized callers
// should implement.
type AuthorizedSubject interface {
	ID() string
	Scope() Scope
	TokenID() string

	In(...Subject) bool
}

// Scope is the scope interface that AuthorizedSubject returns.
type Scope interface {
	IsSuperSetOf(scopes []string) bool
}

type authorizedSubject struct {
	authorization.Subject
	scope   token.Scope
	tokenID string
}

func (as *authorizedSubject) Scope() Scope {
	return as.scope
}

func (as *authorizedSubject) TokenID() string {
	return as.tokenID
}

func (as *authorizedSubject) In(ids ...Subject) bool {
	for _, svc := range ids {
		if as.ID() == svc.id() {
			return true
		}
	}
	return false
}

type distributedIdentitiesAuthorizedSubject struct {
	s2s2dicallee.AuthenticatedSubject
	cfg *c7s.Config
}

// ID() returns ID in format similar to
// https://prod.s2s2identities.twitch.a2z.com/twitch/s2s2-di-integration/prod.json
func (di *distributedIdentitiesAuthorizedSubject) ID() string {
	return distributedIdentityURI(di.cfg.IdentityOrigin, di.Domain(), di.Service(), di.Stage())
}

// return empty scope
func (di *distributedIdentitiesAuthorizedSubject) Scope() Scope {
	return token.NewScope()
}

func (di *distributedIdentitiesAuthorizedSubject) TokenID() string {
	return ""
}

func (di *distributedIdentitiesAuthorizedSubject) In(ids ...Subject) bool {
	for _, svc := range ids {
		if di.ID() == svc.id() {
			return true
		}
	}
	return false
}
