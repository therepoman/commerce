package s2s2

import "fmt"

// distributedIdentityURI returns a canonical URI for a distributed idenities
// service.
func distributedIdentityURI(
	identityOrigin,
	domain,
	service,
	stage string,
) string {
	return fmt.Sprintf("%s/%s/%s/%s.json", identityOrigin, domain, service, stage)
}
