package es256

import (
	"encoding/json"
	"math/big"
)

type bigInt struct {
	*big.Int
}

func (b bigInt) MarshalJSON() ([]byte, error) {
	// return out a zero padded b64url encoded bigint
	myInt := make([]byte, 32)
	num := b.Int.Bytes()

	copy(myInt[32-len(num):], num)

	return json.Marshal(myInt)
}

func (b *bigInt) UnmarshalJSON(inputNum []byte) error {
	var raw []byte
	if err := json.Unmarshal(inputNum, &raw); err != nil {
		return err
	}

	b.Int = big.NewInt(0).SetBytes(raw[:32])
	return nil
}
