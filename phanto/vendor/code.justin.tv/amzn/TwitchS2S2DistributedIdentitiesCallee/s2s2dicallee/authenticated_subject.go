package s2s2dicallee

import (
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/service"
)

// twitchDomain is the domain for all Twitch services
const twitchDomain = "twitch"

// AuthenticatedSubject is what is returned by Validate
type AuthenticatedSubject interface {
	Domain() string
	Service() string
	Stage() string
	IsTwitchService(service, stage string) bool
}

type authenticatedSubject struct {
	service service.Service
}

func (s authenticatedSubject) Domain() string {
	return s.service.Domain
}

func (s authenticatedSubject) Service() string {
	return s.service.Name
}

func (s authenticatedSubject) Stage() string {
	return s.service.Stage
}

// IsTwitchService is a helper for matching to twitch service domain and service
// stage combinations
func (s authenticatedSubject) IsTwitchService(serviceName, serviceStage string) bool {
	return s.service == service.Service{Domain: twitchDomain, Name: serviceName, Stage: serviceStage}
}
