package validation

import (
	"context"
	"crypto/ecdsa"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/cert/certiface"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/opwrap"
	"code.justin.tv/sse/jwt"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

// ErrMissingX5UHeader is returned if the token does nothave x5u header
var ErrMissingX5UHeader = errors.New("x5u header missing from the token")

// Validations validates authentication
type Validations struct {
	Certificates certiface.CertificatesAPI
	// Set of web origins this server represents
	ServerOrigins *Origins

	OperationStarter *operation.Starter
}

// Validate validates an authentication presented.
//
// Cert is identified by the x5u in the token header.
func (v *Validations) Validate(ctx context.Context, token []byte) (_ *Validation, err error) {

	ctx, op := v.OperationStarter.StartOp(ctx, opwrap.ValidateAuthToken)
	defer opwrap.EndWithError(op, err)

	x5u, err := v.getX5U(token)
	if err != nil {
		return nil, err
	}

	if x5u == "" {
		return nil, ErrMissingX5UHeader
	}

	svc, publicKey, err := v.Certificates.Get(ctx, x5u)
	if err != nil {
		return nil, err
	}

	var claims struct {
		Audience   string   `json:"aud"`
		Expiration unixTime `json:"exp"`
		NotBefore  unixTime `json:"nbf"`
	}
	if err := jwt.DecodeAndValidate(
		new(json.RawMessage),
		&claims,
		jwt.ES384(&ecdsa.PrivateKey{PublicKey: *publicKey}),
		token,
	); err != nil {
		return nil, err
	}

	audience, err := parseWebOrigin(claims.Audience)
	if err != nil {
		return nil, err
	}

	validation := &Validation{
		Subject:    *svc,
		Expiration: time.Time(claims.Expiration),
		NotBefore:  time.Time(claims.NotBefore),
	}

	if err := validation.ValidateTimeClaims(); err != nil {
		return nil, err
	}

	if !v.ServerOrigins.Contains(audience) {
		return nil, &ClaimValidationError{
			Field:   "aud",
			Message: fmt.Sprintf("%s does not contain %v", v.ServerOrigins, audience),
		}
	}

	return validation, nil
}

func (v *Validations) getX5U(token []byte) (string, error) {
	var header struct {
		X5U string `json:"x5u"`
	}
	jwtRaw, err := jwt.Parse(token)
	if err != nil {
		return "", err
	}

	if err := jwtRaw.Decode(&header, new(json.RawMessage)); err != nil {
		return "", err
	}

	return header.X5U, nil
}
