package logutil

import (
	"net/http"

	logging "code.justin.tv/amzn/TwitchLogging"
)

// Logger contains all messages we write using this package
type Logger struct {
	Logger logging.Logger
}

// LogAnonymousRequest is when an anonymous request is received in metrics only
// mode.
func (l *Logger) LogAnonymousRequest(r *http.Request) {
	l.Logger.Log(
		"AnonymousRequestReceived",
		"client_ip", r.RemoteAddr,
		"x_forwarded_for", r.Header.Get("X-Forwarded-For"),
		"x_forwarded_proto", r.Header.Get("X-Forwarded-Proto"),
		"x_forwarded_port", r.Header.Get("X-Forwarded-Port"),
	)
}

// LogUnknownError is called when an unknown error happens
func (l *Logger) LogUnknownError(err error) {
	l.Logger.Log("UnknownError", "err", err.Error())
}

// LogUncachedX5U is called a certificate is presented that we are not
// configured for
func (l *Logger) LogUncachedX5U(x5u string) {
	l.Logger.Log("LogUncachedX5U", "x5u", x5u)
}
