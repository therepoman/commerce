job {
  name 'chat-workerqueue'
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'chat/workerqueue', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }

  steps {
    shell 'rm -rf .manta/'
    shell 'manta -v'
  }
}
