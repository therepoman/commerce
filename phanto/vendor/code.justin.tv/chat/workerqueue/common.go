package workerqueue

import (
	"github.com/aws/aws-sdk-go/service/sqs"
)

// TaskFn defines the function to be performed on the task data.
type TaskFn func(msg *sqs.Message) error

// TaskVersion is a version number to assign to SQS messages and worker TaskFn's.
// A worker will use the task that matches the version of a message it receives.
type TaskVersion int

const (
	// The FallbackTaskVersion of a task is used as a fallback if
	// there is no task that matches the version of the job.
	FallbackTaskVersion = TaskVersion(-1)
)
