package fheventiface

import (
	"context"

	"github.com/aws/aws-sdk-go/service/firehose"
)

// EventWriterAPI ...
type EventWriterAPI interface {
	PutRecordBatchRetriedWithContext(context.Context, *firehose.PutRecordBatchInput) error
}
