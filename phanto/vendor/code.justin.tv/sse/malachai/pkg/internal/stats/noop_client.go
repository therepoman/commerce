package stats

import (
	"time"
)

// NoopClient is a client that does no operations
type NoopClient struct {
}

// WithSuccessStatter ...
func (c *NoopClient) WithSuccessStatter(
	metricNamePrefix string,
	dimensions []*Dimension,
	fn Func,
) (err error) {
	err = fn()
	return
}

// Run ...
func (c *NoopClient) Run() {
}

// Stop ...
func (c *NoopClient) Stop(timeout time.Duration) (err error) {
	return
}
