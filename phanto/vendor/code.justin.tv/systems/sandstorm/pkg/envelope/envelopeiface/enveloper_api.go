package envelopeiface

import "code.justin.tv/systems/sandstorm/pkg/envelope"

// Enveloper describes the envelope interface
type Enveloper interface {
	Seal(ctxt map[string]string, plaintext []byte) (encryptedDataKey envelope.EncryptedDataKey, ciphertext []byte, err error)
	Open(encryptedDataKey envelope.EncryptedDataKey, ciphertext []byte, ctxt map[string]string) ([]byte, error)
}
