package test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

const (
	LocalTestDataFilePath = "/src/code.justin.tv/commerce/phanto/test-data/%s"
)

func AssertTwirpError(err error, expectedErrorCode twirp.ErrorCode) {
	So(err, ShouldNotBeNil)
	tErr, isTErr := err.(twirp.Error)
	So(isTErr, ShouldBeTrue)
	So(tErr.Code(), ShouldEqual, expectedErrorCode)
}

func LoadTestFile(fileName string) ([]byte, error) {
	filePath, err := getTestFilePath(fileName)
	if err != nil {
		return nil, err
	}

	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing test file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return fileBytes, nil
}

func getTestFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalTestDataFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	return "", errors.New("Could not find file")
}
