module "phanto" {
  source                               = "../modules/phanto"
  aws_profile                          = "${var.aws_profile}"
  aws_account_id                       = "${var.aws_account_id}"
  env                                  = "${var.env}"
  vpc_id                               = "${var.vpc_id}"
  private_subnets                      = "${var.private_subnets}"
  private_cidr_blocks                  = "${var.private_cidr_blocks}"
  sg_id                                = "${var.sg_id}"
  autoscaling_role                     = "arn:aws:iam::748615074154:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_min_read_capacity             = 50
  dynamo_min_write_capacity            = 50
  elasticache_replicas_per_node_group  = "1"
  elasticache_num_node_groups          = "2"
  elasticache_instance_type            = "cache.r4.large"
  ecs_ec2_backed_min_instances         = 5
  ecs_logs_cloudwatch_log_group        = "phanto-prod"
  ecs_canary_logs_cloudwatch_log_group = "phanto-prod-canary"
  sandstorm_arn                        = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/phanto-production"
  whitelisted_arns_for_a2z_privatelink = "${var.whitelisted_arns_for_a2z_privatelink}"
  s2s2_auth_logs_retention_in_days     = 30
}

terraform {
  backend "s3" {
    bucket  = "phanto-terraform-prod"
    key     = "tfstate/commerce/phanto/terraform/prod"
    region  = "us-west-2"
    profile = "phanto-prod"
  }
}

provider "aws" {
  version = "2.57.0"
  region  = "us-west-2"
  profile = "phanto-prod"
}

module "network-admin-role" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/network-admin-role?ref=1dc0fc2c77353e7f9c1978c2a21ab64552d51f7c"
  region  = "us-west-2"
  account = "phanto-prod"
}

module "twitch-inventory" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory.git?ref=a7207ce935fce975ee739198bcd7e2df80419680"
  account = "phanto-prod"
}
