variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
  default     = "phanto-prod"
}

variable "aws_account_id" {
  description = "AWS Account ID"
  default     = "748615074154"
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
  default     = "vpc-0ff9911a29ec350bb"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
  default     = "subnet-0cc5377bdc8311620,subnet-0a3f4cf4ef4e7422a,subnet-07d111487505541c8"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = "list"
  default     = ["10.203.162.0/24", "10.203.161.0/24", "10.203.160.0/24"]
}

variable "sg_id" {
  description = "Id of the security group"
  default     = "sg-083d6fec53c5fadf9"
}

variable "owner" {
  description = "Owner of the account"
  default     = "twitch-phanto-aws-prod@amazon.com"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/phanto"
}

variable "env" {
  description = "Whether it's production, dev, etc"
  default     = "prod"
}

variable "cname_prefix" {
  description = "Prefix of the cname"
  default     = "phanto-prod"
}

variable "whitelisted_arns_for_a2z_privatelink" {
  type = "list"

  default = [
    "arn:aws:iam::787149559823:root", // graphql-prod
    "arn:aws:iam::028439334451:root", // helix-prod
    "arn:aws:iam::196915980276:root", // admin panel prod
  ]
}
