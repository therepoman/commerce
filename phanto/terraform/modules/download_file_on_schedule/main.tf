variable "short_name" {
  type = "string"
}

variable "source_file_url" {
  type = "string"
}

variable "destination_bucket" {
  type = "string"
}

variable "destination_key" {
  type = "string"
}

variable "download_to_s3_lambda_arn" {
  type = "string"
}

resource "aws_cloudwatch_event_rule" "download_event_hourly_rule" {
  name                = "download-to-s3-hourly-event-rule-${var.short_name}"
  description         = "Download to s3 hourly event rule for ${var.short_name}"
  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "lambda_download_hourly_target" {
  rule      = "${aws_cloudwatch_event_rule.download_event_hourly_rule.name}"
  target_id = "download-to-s3-hourly-lambda-target-${var.short_name}"
  arn       = "${var.download_to_s3_lambda_arn}"
  input = <<EOF
{
  "source_file_url" : "${var.source_file_url}",
  "destination_bucket" : "${var.destination_bucket}",
  "destination_key" : "${var.destination_key}"
}
EOF
}
