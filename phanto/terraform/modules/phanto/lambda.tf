module "dynamo_backup_lambda" {
  source = "../dynamo_backup_lambda"
}

module "download_file_to_s3_lambda" {
  source          = "../download_file_to_s3_lambda"
  private_subnets = "${var.private_subnets}"
  sg_id           = "${var.sg_id}"
}

module "pdms_clean_user_keys_claimed_lambda" {
  source                           = "./lambda"
  env                              = "${var.env}"
  phanto_logscan_namespace        = "phanto-logs"
  security_groups                  = ["${var.sg_id}"]
  subnets                          = "${var.private_subnets}"
  lambda_name                      = "pdms_clean_user_keys_claimed"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
}

module "pdms_clean_key_handlers_lambda" {
  source                           = "./lambda"
  env                              = "${var.env}"
  phanto_logscan_namespace        = "phanto-logs"
  security_groups                  = ["${var.sg_id}"]
  subnets                          = "${var.private_subnets}"
  lambda_name                      = "pdms_clean_key_handlers"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
}

module "pdms_report_deletion_lambda" {
  source                           = "./lambda"
  env                              = "${var.env}"
  phanto_logscan_namespace        = "phanto-logs"
  security_groups                  = ["${var.sg_id}"]
  subnets                          = "${var.private_subnets}"
  lambda_name                      = "pdms_report_deletion"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
}

module "pdms_start_user_deletion_lambda" {
  source                           = "./lambda"
  env                              = "${var.env}"
  phanto_logscan_namespace        = "phanto-logs"
  security_groups                  = ["${var.sg_id}"]
  subnets                          = "${var.private_subnets}"
  lambda_name                      = "pdms_start_user_deletion"
  lambda_bucket                    = "${aws_s3_bucket.lambdas.bucket}"
  concurrent_executions            = 1
}

resource "aws_lambda_event_source_mapping" "pdms_start_user_deletion_queue_event_source" {
  event_source_arn = "${aws_sqs_queue.eventbus_user_deletion_queue.arn}"
  function_name    = "${module.pdms_start_user_deletion_lambda.lambda_name}"
  batch_size       = 1
}