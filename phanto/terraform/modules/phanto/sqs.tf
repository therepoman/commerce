resource "aws_sqs_queue" "eventbus_user_deletion_queue" {
  name                       = "eventbus-phanto-user-deletion-queue"
  policy                     = "${aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]}"
  kms_master_key_id          = "${aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]}"
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.eventbus_user_deletion_queue_dlq.arn}\",\"maxReceiveCount\":5}"
  message_retention_seconds  = 1209600
  visibility_timeout_seconds = 60
}

resource "aws_sqs_queue" "eventbus_user_deletion_queue_dlq" {
  name                      = "eventbus-phanto-user-deletion-queue-dlq"
  message_retention_seconds = 1209600
  kms_master_key_id         = "${aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]}"
}
