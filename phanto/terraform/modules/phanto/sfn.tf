module "sfn" {
  source      = "./sfn"
  env         = "${var.env}"
  aws_profile = "${var.aws_profile}"
}

module "pdms_user_deletion_sfn" {
  source                                          = "./sfn/pdms_user_deletion_v1"
  env                                             = "${var.env}"
  region                                          = "${var.aws_region}"
  pdms_clean_user_claimed_keys_lambda_arn         = "${module.pdms_clean_user_keys_claimed_lambda.lambda_arn}"
  pdms_clean_key_handlers_lambda_arn              = "${module.pdms_clean_key_handlers_lambda.lambda_arn}"
  pdms_report_deletion_lambda_arn                 = "${module.pdms_report_deletion_lambda.lambda_arn}"
}
