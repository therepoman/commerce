provider "aws" {
  region     = "us-west-2"
  profile    = "${var.aws_profile}"
  alias      = "${var.aws_profile}"
}

provider "aws" {
  alias   = "virginia"
  region  = "us-east-1"
  profile = "${var.aws_profile}"
}
