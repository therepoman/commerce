data "http" "eventbus_cloudformation" {
  url = "https://eventbus-setup.s3-us-west-2.amazonaws.com/cloudformation.yaml"
}

resource "aws_cloudformation_stack" "eventbus" {
  name          = "EventBus"
  capabilities  = ["CAPABILITY_NAMED_IAM"]
  template_body = "${data.http.eventbus_cloudformation.body}"
}

resource "aws_iam_role_policy_attachment" "eventbus_start_user_deletion_lambda_access_attach" {
  role       = "${module.pdms_start_user_deletion_lambda.role_name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}