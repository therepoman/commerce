module "ecs" {
  source                       = "ecs"
  vpc_id                       = "${var.vpc_id}"
  subnet_ids                   = "${var.private_subnets}"
  security_group               = "${var.sg_id}"
  service                      = "${var.service}"
  service_short_name           = "${var.service_short_name}"
  env                          = "${var.env}"
  nlb_dns                      = "${module.privatelink-cert.dns}"
  redis_sec_group_id           = "${aws_security_group.phanto_redis_security_group.id}"
  ecs_ec2_backed_min_instances = "${var.ecs_ec2_backed_min_instances}"
  sandstorm_arn                = "${var.sandstorm_arn}"
}