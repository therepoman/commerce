resource "aws_iam_role" "replication" {
  name = "phanto_reports_s3_bucket_replication_role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "replication" {
  name = "phanto_reports_s3_bucket_replication_policy"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.phanto_reports_s3_bucket.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.phanto_reports_s3_bucket.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.phanto_reports_s3_bucket_replication_destination.arn}/*"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "replication" {
  role       = "${aws_iam_role.replication.name}"
  policy_arn = "${aws_iam_policy.replication.arn}"
}


resource "aws_s3_bucket" "phanto_reports_s3_bucket_replication_destination" {
  bucket = "phanto-reports-${var.env}-gbl-replication"
  provider = "aws.virginia"

  versioning {
    enabled = true
  }
}


resource "aws_s3_bucket" "phanto_reports_s3_bucket" {
  bucket = "phanto-reports-${var.env}"
  acl    = "private"

  tags {
    Environment = "${var.env}"
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id = "ttl-90-days"

    enabled = true

    expiration {
      days = 90
    }
  }

  replication_configuration {
    role = "${aws_iam_role.replication.arn}"

    rules {
      status = "Enabled"
      prefix = ""

      destination {
        bucket        = "${aws_s3_bucket.phanto_reports_s3_bucket_replication_destination.arn}"
        storage_class = "STANDARD"
      }
    }
  }
}

resource "aws_s3_bucket" "lambdas" {
  bucket = "phanto-lambdas-${var.env}"
  acl    = "private"
}
