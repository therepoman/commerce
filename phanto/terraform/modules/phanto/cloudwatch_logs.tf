// S2S Auth Logs for SOX Compliance
locals {
  s2s_auth_logs_name = "${var.service_short_name}-${var.env}-s2s-auth-logs"
}

resource "aws_cloudwatch_log_group" "s2s_auth_logs" {
  name              = "${local.s2s_auth_logs_name}"
  retention_in_days = "${var.s2s2_auth_logs_retention_in_days}"
  kms_key_id        = "${aws_kms_key.s2s_auth_logs.arn}"
}

resource "aws_cloudwatch_log_subscription_filter" "s2s_auth_log_funnel_subscription" {
  count           = "${var.env == "prod" ? 1 : 0}"
  name            = "${local.s2s_auth_logs_name}_subscription-filter"
  log_group_name  = "${local.s2s_auth_logs_name}"
  filter_pattern  = ""
  destination_arn = "${aws_cloudformation_stack.twitch_security_log_funnel.outputs["LogFunnelARN"]}"
  role_arn        = "${aws_cloudformation_stack.twitch_security_log_funnel.outputs["CWLRoleARN"]}"
}
