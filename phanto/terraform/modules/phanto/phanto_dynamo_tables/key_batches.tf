resource "aws_dynamodb_table" "key_batches_table" {
  name           = "key-batches-${var.suffix}"
  hash_key       = "batch_id"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  global_secondary_index {
    name            = "key-batches-pool-id-gsi-${var.suffix}"
    hash_key        = "pool_id"
    range_key       = "batch_id"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  "attribute" {
    name = "batch_id"
    type = "S"
  }

  "attribute" {
    name = "pool_id"
    type = "S"
  }
}

module "key_batches_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "key-batches-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "key_batches_table_pool_id_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "key-batches-${var.suffix}"
  index_name         = "key-batches-pool-id-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "key_batches_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "key-batches-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
