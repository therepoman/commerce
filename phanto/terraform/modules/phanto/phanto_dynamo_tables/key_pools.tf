resource "aws_dynamodb_table" "key_pools_table" {
  name           = "key-pools-${var.suffix}"
  hash_key       = "pool_id"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  global_secondary_index {
    name            = "key-pools-handler-gsi-${var.suffix}"
    hash_key        = "handler"
    range_key       = "pool_id"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  "attribute" {
    name = "pool_id"
    type = "S"
  }

  "attribute" {
    name = "handler"
    type = "S"
  }
}

module "key_pools_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "key-pools-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "key_pools_table_handler_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "key-pools-${var.suffix}"
  index_name         = "key-pools-handler-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "key_pools_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "key-pools-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
