resource "aws_dynamodb_table" "key_handler_groups_table" {
  name           = "key-handler-groups-${var.suffix}"
  hash_key       = "handler_group_id"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "handler_group_id"
    type = "S"
  }
}

module "key_handler_groups_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "key-handler-groups-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "key_handler_groups_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "key-handler-groups-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}