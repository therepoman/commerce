resource "aws_dynamodb_table" "key_pool_metadata_table" {
  name           = "key-pool-metadata-${var.suffix}"
  hash_key       = "pool_id"
  range_key      = "metadata_key"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "pool_id"
    type = "S"
  }

  "attribute" {
    name = "metadata_key"
    type = "S"
  }
}

module "key_pool_metadata_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "key-pool-metadata-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "key_pool_metadata_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "key-pool-metadata-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}