resource "aws_dynamodb_table" "products_table" {
  name           = "products-${var.suffix}"
  hash_key       = "product_type"
  range_key      = "sku"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "product_type"
    type = "S"
  }

  "attribute" {
    name = "sku"
    type = "S"
  }
}

module "products_table_autoscaling" {
  source            = "../../dynamo_table_autoscaling"
  table_name        = "products-${var.suffix}"
  autoscaling_role  = "${var.autoscaling_role}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
}

module "products_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "products-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
