resource "aws_dynamodb_table" "reports_table" {
  name           = "reports-${var.suffix}"
  hash_key       = "report_id"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "pool_id"
    type = "S"
  }

  "attribute" {
    name = "report_id"
    type = "S"
  }

  global_secondary_index {
    name            = "reports-pool-id-gsi-${var.suffix}"
    hash_key        = "pool_id"
    range_key       = "report_id"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }
}

module "reports_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "reports-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "reports_table_pool_id_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "reports-${var.suffix}"
  index_name         = "reports-pool-id-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "reports_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "reports-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
