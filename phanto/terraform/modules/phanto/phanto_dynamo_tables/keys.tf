resource "aws_dynamodb_table" "keys_table" {
  name           = "keys-${var.suffix}"
  hash_key       = "key_code"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  global_secondary_index {
    name            = "keys-batch-id-gsi-${var.suffix}"
    hash_key        = "batch_id"
    range_key       = "key_code"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  global_secondary_index {
    name            = "keys-claimed-by-gsi-${var.suffix}"
    hash_key        = "claimed_by"
    range_key       = "key_code"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  "attribute" {
    name = "key_code"
    type = "S"
  }

  "attribute" {
    name = "batch_id"
    type = "S"
  }

  attribute {
    name = "claimed_by"
    type = "S"
  }
}

module "keys_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "keys-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "keys_table_batch_id_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "keys-${var.suffix}"
  index_name         = "keys-batch-id-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "keys_table_claimed_by_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "keys-${var.suffix}"
  index_name         = "keys-claimed-by-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "keys_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "keys-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
