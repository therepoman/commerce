resource "aws_dynamodb_table" "call_audit_records_table" {
  name           = "call-audit-records-${var.suffix}"
  hash_key       = "call_id"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  global_secondary_index {
    name            = "call-audit-records-operation-gsi-${var.suffix}"
    hash_key        = "operation"
    range_key       = "timestamp"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  global_secondary_index {
    name            = "call-audit-records-ldap-user-gsi-${var.suffix}"
    hash_key        = "ldap_user"
    range_key       = "timestamp"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  global_secondary_index {
    name            = "call-audit-records-authenticated-user-id-gsi-${var.suffix}"
    hash_key        = "authenticated_user_id"
    range_key       = "timestamp"
    projection_type = "ALL"
    read_capacity   = "${var.min_read_capacity}"
    write_capacity  = "${var.min_write_capacity}"
  }

  "attribute" {
    name = "call_id"
    type = "S"
  }

  "attribute" {
    name = "operation"
    type = "S"
  }

  "attribute" {
    name = "ldap_user"
    type = "S"
  }

  "attribute" {
    name = "authenticated_user_id"
    type = "S"
  }

  "attribute" {
    name = "timestamp"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled = true
  }
}

module "call_audit_records_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "call-audit-records-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "call_audit_records_table_operation_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "call-audit-records-${var.suffix}"
  index_name         = "call-audit-records-operation-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "call_audit_records_table_ldap_user_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "call-audit-records-${var.suffix}"
  index_name         = "call-audit-records-ldap-user-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "call_audit_records_table_authenticated_user_id_gsi_autoscaling" {
  source             = "../../dynamo_gsi_autoscaling"
  table_name         = "call-audit-records-${var.suffix}"
  index_name         = "call-audit-records-authenticated-user-id-gsi-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}

module "call_audit_records_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "call-audit-records-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
