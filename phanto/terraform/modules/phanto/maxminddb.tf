resource "aws_s3_bucket" "maxminddb_s3_bucket" {
  bucket = "phanto-maxminddb-${var.env}"
  acl    = "private"

  tags {
    Environment = "${var.env}"
  }
}

module "maxminddb_scheduled_download" {
  source                    = "../download_file_on_schedule"
  short_name                = "maxminddb"
  source_file_url           = "https://pkgs.xarth.tv/artifactory/ip-twitch-geoip/current/GeoIP2-City.mmdb"
  destination_bucket        = "${aws_s3_bucket.maxminddb_s3_bucket.bucket}"
  destination_key           = "GeoIP2-City.mmdb"
  download_to_s3_lambda_arn = "${module.download_file_to_s3_lambda.lambda_arn}"
}
