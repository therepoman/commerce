resource "aws_elasticache_replication_group" "phanto_redis" {
  automatic_failover_enabled    = "${var.elasticache_auto_failover}"
  replication_group_id          = "phanto-${var.env}"
  replication_group_description = "phanto redis cluster"
  engine                        = "redis"
  engine_version                = "4.0.10"
  node_type                     = "${var.elasticache_instance_type}"
  parameter_group_name          = "${aws_elasticache_parameter_group.phanto_redis_params.name}"
  port                          = "${var.redis_port}"
  subnet_group_name             = "${aws_elasticache_subnet_group.phanto_redis_subnet_group.name}"
  security_group_ids            = ["${aws_security_group.phanto_redis_security_group.id}"]

  cluster_mode {
    replicas_per_node_group     = "${var.elasticache_replicas_per_node_group}"
    num_node_groups             = "${var.elasticache_num_node_groups}"
  }
}

resource "aws_elasticache_parameter_group" "phanto_redis_params" {
  family = "redis4.0"
  name   = "phanto-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }
}

resource "aws_elasticache_subnet_group" "phanto_redis_subnet_group" {
  name       = "phanto-${var.env}-cache"
  subnet_ids = ["${split(",", var.private_subnets)}"]
}

resource "aws_security_group" "phanto_redis_security_group" {
  name        = "phanto-${var.env}-redis"
  vpc_id      = "${var.vpc_id}"
  description = "Allows communication with redis server"

  ingress {
    from_port   = "${var.redis_port}"
    protocol    = "tcp"
    to_port     = "${var.redis_port}"
    cidr_blocks = "${var.private_cidr_blocks}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "phanto-${var.env}-redis"
  }
}
