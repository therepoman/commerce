resource "aws_sns_topic" "key_generation_jobs_topic" {
  name = "key-generation-jobs-${var.suffix}"
}

resource "aws_sqs_queue" "key_generation_jobs_queue" {
  name                       = "key-generation-jobs-processing-${var.suffix}"
  visibility_timeout_seconds = "3600"
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.key_generation_jobs_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "key_generation_jobs_queue_dlq" {
  name = "key-generation-jobs-processing-dlq-${var.suffix}"
}

resource "aws_sns_topic_subscription" "key_generation_jobs_topic_queue_subscription" {
  topic_arn = "${aws_sns_topic.key_generation_jobs_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${aws_sqs_queue.key_generation_jobs_queue.arn}"
}

resource "aws_sqs_queue_policy" "key_generation_job_sqs_policy" {
  queue_url = "${aws_sqs_queue.key_generation_jobs_queue.id}"

  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Id": "key_generation_job_sqs_policy",
    "Statement":[
      {
        "Sid": "AllowTopicsFromOtherTwitchAWSAccountsToSendMessages",
        "Effect": "Allow",
        "Principal": "*",
        "Action": "SQS:SendMessage",
        "Resource": "${aws_sqs_queue.key_generation_jobs_queue.arn}",
        "Condition": {
          "ArnEquals": {
            "aws:SourceArn": "${aws_sns_topic.key_generation_jobs_topic.arn}"
          }
        }
      }
    ]
  }
  EOF
}
