variable "env" {}

variable "phanto_logscan_namespace" {}

variable "security_groups" {
  type = "list"
}

variable "subnets" {}

variable "lambda_name" {}

variable "lambda_bucket" {}

variable "timeout" {
  default = 60
}

variable "memory_size" {
  default = 1024
}

variable "concurrent_executions" {
  default = -1
}
