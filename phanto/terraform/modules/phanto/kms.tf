resource "aws_kms_key" "phanto_master_kms_key" {
  description = "Phanto master kms key for generating key batch data encryption keys"
}

resource "aws_kms_alias" "phanto_master_kms_key_alias" {
  name          = "alias/phanto-master-key-${var.env}"
  target_key_id = "${aws_kms_key.phanto_master_kms_key.key_id}"
}

// kms key for s2s auth log group for SOX
resource "aws_kms_key" "s2s_auth_logs" {
  description         = "${var.service_short_name}-${var.env} cloudwatch s2s auth logs encryption"
  enable_key_rotation = true

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.aws_account_id}:root",
          "arn:aws:iam::${var.aws_account_id}:role/admin"
        ]
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Enable CloudWatch S2S Auth Logs Encryption",
      "Effect": "Allow",
      "Principal": {
        "Service": "logs.${var.aws_region}.amazonaws.com"
      },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*",
      "Condition": {
          "ArnEquals": {
              "kms:EncryptionContext:aws:logs:arn": "arn:aws:logs:${var.aws_region}:${var.aws_account_id}:log-group:${local.s2s_auth_logs_name}"
          }
      }
    }
  ]
}
EOF
}

resource "aws_kms_alias" "s2s_auth_logs" {
  name          = "alias/${var.service_short_name}-${var.env}/s2s-auth-logs"
  target_key_id = "${aws_kms_key.s2s_auth_logs.key_id}"
}
