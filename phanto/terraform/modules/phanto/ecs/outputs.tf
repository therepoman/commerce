output "service_alb_dns" {
  value = "${module.service.dns}"
}

output "cert_arn" {
  value = "${module.service.cert}"
}

output "service_role" {
  value = "${module.service.service_role}"
}

output "canary_role" {
  value = "${module.service.canary_role}"
}

output "cluster_role" {
  value = "${module.cluster.ecs_instance_role_name}"
}