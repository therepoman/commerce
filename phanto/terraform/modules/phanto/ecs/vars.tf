# Beanstalk general variables
variable "service" {
  description = "All lower­case, in  team/service/[role] format. This tag should indicate the server’s function and what is running on it;  role is optional but highly recommended."
}

variable "service_short_name" {}

# Note: All of the variables below are used to define EB environment configuration.
# If the description is not sufficient, you may find additional information and context
# at: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
variable "vpc_id" {
  description = "VPC ID used for the environment."
}

variable "subnet_ids" {
  description = "Comma-separated string of vpc security groups to be used by instances."
}

variable "security_group" {
  description = "Assign one or more security groups that you created to the load balancer."
}

variable "redis_sec_group_id" {}

variable "min_task_count" {
  description = "Minimum number of tasks in the ECS service"
  default     = "1"
}

variable "max_task_count" {
  description = "Maximum number of tasks in the ECS service"
  default     = "4"
}

variable "env" {}

variable "ecs_ec2_backed_min_instances" {
  default = 5
}

variable "sandstorm_arn" {
  description = "Amazon Resource Number of the sandstorm"
}

variable "nlb_dns" {}
