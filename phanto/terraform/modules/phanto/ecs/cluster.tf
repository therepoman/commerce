module "cluster" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//cluster?ref=536258c22b8c03110e14799f464f0f6be3b8a28a"

  name           = "${var.service_short_name}"
  min            = "${var.ecs_ec2_backed_min_instances}"
  vpc_id         = "${var.vpc_id}"
  instance_type  = "c5n.4xlarge"
  environment    = "${var.env}"
  subnets        = "${var.subnet_ids}"
  security_group = "${var.security_group}"
}
