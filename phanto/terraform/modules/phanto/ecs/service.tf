locals {
  cpu = {
    prod    = 4096
    staging = 512
  }

  memory = {
    prod    = 4096
    staging = 512
  }
}

module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=adac144cfd38fb5f88079703210475967876a374"

  name   = "${var.service_short_name}-${lower(var.env)}"
  repo   = "${var.service}"
  port   = 8000
  cpu    = "${local.cpu[var.env]}"
  memory = "${local.memory[var.env]}"

  canary_cpu    = "${local.cpu[var.env]}"
  canary_memory = "${local.memory[var.env]}"

  counts = {
    min = "${var.min_task_count}"
    max = "${var.max_task_count}"
  }

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = "${var.env == "staging" ? "staging-nlb" : var.env}"
    },
    {
      name  = "AWS_STS_REGIONAL_ENDPOINTS"
      value = "regional"
    },
  ]

  cluster        = "${module.cluster.ecs_name}"
  security_group = "${var.security_group}"
  region         = "us-west-2"
  vpc_id         = "${var.vpc_id}"
  environment    = "${var.env}"
  subnets        = "${var.subnet_ids}"
  dns            = "${var.nlb_dns}"
  healthcheck    = "/ping"
  http2_enabled  = false
  debug_enabled  = false
}

// For S2S2
resource "aws_iam_policy" "s2s2_iam_policy" {
  name        = "${var.service_short_name}_${lower(var.env)}_s2s2_iam_policy"
  description = "required IAM permission to use S2S2"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "execute-api:Invoke"
            ],
            "Resource": [
                "arn:aws:execute-api:*:985585625942:*"
            ],
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_service_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "${aws_iam_policy.s2s2_iam_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_canary_attachment" {
  role       = "${module.service.canary_role}"
  policy_arn = "${aws_iam_policy.s2s2_iam_policy.arn}"
}