variable "role_name" {
  description = "the name of the role to attach policies to"
}

variable "sandstorm_arn" {
  description = "Amazon Resource Number of the the sandstorm"
}