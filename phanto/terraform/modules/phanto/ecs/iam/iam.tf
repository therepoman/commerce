resource aws_iam_role_policy "ecs_task_role_sandstorm_policy" {
  name = "sandstorm-policy"
  role = "${var.role_name}"

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "${var.sandstorm_arn}",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource aws_iam_role_policy "ecs_task_role_s2s_policy" {
  name = "s2s-policy"
  role = "${var.role_name}"

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "arn:aws:iam::180116294062:role/malachai/*",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource aws_iam_role_policy_attachment "ecs_task_role_dynamo_access" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource aws_iam_role_policy_attachment "cloudwatch_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  role       = "${var.role_name}"
}

resource aws_iam_role_policy "kms_policy" {
  name = "kms-policy"
  role = "${var.role_name}"

  policy = <<EOT
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Action": [
            "kms:GenerateDataKey",
            "kms:Decrypt"
        ],
        "Resource": "*"
    }
}
EOT
} 