module "phanto_alarms" {
  source                                = "phanto_alarms"
  ecs_logs_cloudwatch_log_group         = "${var.ecs_logs_cloudwatch_log_group}"
  ecs_canary_logs_cloudwatch_log_group  = "${var.ecs_canary_logs_cloudwatch_log_group}"
}