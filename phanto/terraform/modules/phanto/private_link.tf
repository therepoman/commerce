module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name        = "${var.service_short_name}"
  environment = "${var.env == "prod" ? "production" : var.env}"
}

module "privatelink-cert" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name        = "${var.service_short_name}"
  environment = "${var.env == "prod" ? "production" : var.env}"
  zone_id     = "${module.privatelink-zone.zone_id}"
  alb_dns     = "${module.ecs.service_alb_dns}"
}

module "privatelink_a2z" {
  source           = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name             = "${var.service_short_name}"
  region           = "${var.aws_region}"
  environment      = "${var.env}"
  alb_dns          = "${module.ecs.service_alb_dns}"
  cert_arn         = "${module.privatelink-cert.arn}"
  vpc_id           = "${var.vpc_id}"
  subnets          = "${split(",", var.private_subnets)}"
  whitelisted_arns = "${var.whitelisted_arns_for_a2z_privatelink}"
}

// privatelinks with downstreams

locals {
  petozi_vpce = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-07b64710c641bdf45"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-0e5642c1438fcc295"
  }

  petozi_dns = {
    staging = "main.us-west-2.beta.petozi.twitch.a2z.com"
    prod    = "main.us-west-2.prod.petozi.twitch.a2z.com"
  }

  users_service_vpce = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-05dd975d62ccc04fe"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-044e493c84b7dc984"
  }

  users_service_dns = {
    staging = "dev.users-service.twitch.a2z.com"
    prod    = "prod.users-service.twitch.a2z.com"
  }
}

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "ldap.twitch.a2z.com"
}

module "privatelink-petozi" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name            = "petozi"
  endpoint        = "${local.petozi_vpce[var.env]}"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "${local.petozi_dns[var.env]}"
}

module "privatelink-users-service" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name            = "users-service"
  endpoint        = "${local.users_service_vpce[var.env]}"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "${local.users_service_dns[var.env]}"
}

module "privatelink-spade" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=5ff49ed9d38812064501f06947e0cbf51044016f"
  name            = "spade"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-080f691601ce37dfb"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "spade.internal.di.twitch.a2z.com"
}

// artifactory (can't use terracode module because vpc endpoint resource is not exported)
locals {
  artifactory_name          = "artifactory-xarth"
  artifactory_endpoint      = "com.amazonaws.vpce.us-west-2.vpce-svc-0ae1454184d60ff60"
  artifactory_dns           = "pkgs.xarth.tv"
  artifactory_dns_subdomain = "*.pkgs.xarth.tv"
}

resource "aws_route53_zone" "artifactory_private_hosted_zone" {
  name = "${local.artifactory_dns}"

  vpc {
    vpc_id = "${var.vpc_id}"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "artifactory_endpoint" {
  service_name        = "${local.artifactory_endpoint}"
  security_group_ids  = ["${var.sg_id}"]
  subnet_ids          = "${split(",", var.private_subnets)}"
  vpc_id              = "${var.vpc_id}"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Name = "${local.artifactory_name}"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "artifactory_endpoint_record" {
  zone_id = "${aws_route53_zone.artifactory_private_hosted_zone.zone_id}"
  name    = "${local.artifactory_dns}"
  type    = "A"

  alias {
    name                   = "${lookup(aws_vpc_endpoint.artifactory_endpoint.dns_entry[0], "dns_name")}"
    zone_id                = "${lookup(aws_vpc_endpoint.artifactory_endpoint.dns_entry[0], "hosted_zone_id")}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "artifactory_endpoint_record_subdomain" {
  zone_id = "${aws_route53_zone.artifactory_private_hosted_zone.zone_id}"
  name    = "${local.artifactory_dns_subdomain}"
  type    = "A"

  alias {
    name                   = "${lookup(aws_vpc_endpoint.artifactory_endpoint.dns_entry[0], "dns_name")}"
    zone_id                = "${lookup(aws_vpc_endpoint.artifactory_endpoint.dns_entry[0], "hosted_zone_id")}"
    evaluate_target_health = false
  }
}
