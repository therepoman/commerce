resource "aws_cloudwatch_log_metric_filter" "ecs_error_scan" {
  name           = "phanto-ecs-error-scan"
  pattern        = "level=error"
  log_group_name = "${var.ecs_logs_cloudwatch_log_group}"

  metric_transformation {
    name          = "phanto-ecs-errors"
    namespace     = "${var.logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "ecs_panic_scan" {
  name           = "phanto-ecs-panic-scan"
  pattern        = "?\"panic:\" ?panic.go"
  log_group_name = "${var.ecs_logs_cloudwatch_log_group}"

  metric_transformation {
    name          = "phanto-ecs-panics"
    namespace     = "${var.logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "canary_error_scan" {
  name           = "phanto-ecs-canary-error-scan"
  pattern        = "level=error"
  log_group_name = "${var.ecs_canary_logs_cloudwatch_log_group}"

  metric_transformation {
    name          = "phanto-ecs-canary-errors"
    namespace     = "${var.logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "canary_panic_scan" {
  name           = "phanto-ecs-canary-panic-scan"
  pattern        = "?\"panic:\" ?panic.go"
  log_group_name = "${var.ecs_canary_logs_cloudwatch_log_group}"

  metric_transformation {
    name          = "phanto-ecs-canary-panics"
    namespace     = "${var.logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_metric_alarm" "ecs_errors" {
  alarm_name                = "phanto-ecs-logscan-error"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "phanto-ecs-errors"
  namespace                 = "${var.logscan_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  alarm_description         = "High volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "ecs_panics" {
  alarm_name                = "phanto-ecs-logscan-panic"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "phanto-ecs-panics"
  namespace                 = "${var.logscan_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "Phanto ECS is panicking!"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "ecs_canary_errors" {
  alarm_name                = "phanto-ecs-canary-logscan-error"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "phanto-ecs-canary-errors"
  namespace                 = "${var.logscan_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "High volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "ecs_canary_panics" {
  alarm_name                = "phanto-ecs-canary-logscan-panic"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "phanto-ecs-canary-panics"
  namespace                 = "${var.logscan_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "Phanto ECS canary is panicking!"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}
