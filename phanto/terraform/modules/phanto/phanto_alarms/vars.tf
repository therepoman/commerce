variable "logscan_namespace" {
  default = "phanto-logs"
  type    = "string"
}

variable "ecs_logs_cloudwatch_log_group" {
  description = "Cloudwatch log group in which ECS application logs are written"
  type        = "string"
}

variable "ecs_canary_logs_cloudwatch_log_group" {
  description = "Cloudwatch log group in which ECS canary application logs are written"
  type        = "string"
}
