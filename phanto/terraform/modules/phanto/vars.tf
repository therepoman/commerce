variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "aws_region" {
  description = "The region in which the AWS account lives"
  default     = "us-west-2"
}

variable "aws_account_id" {
  type = "string"
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "sg_id" {
  description = "Id of the security group"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/phanto"
}

variable "service_short_name" {
  description = "Short name of the service being built. Should probably leave this as the default"
  default     = "phanto"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "eb_environment_service_role" {
  description = "The name of an IAM role that Elastic Beanstalk uses to manage resources for the environment."
  default     = "aws-elasticbeanstalk-service-role"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "dynamo_min_read_capacity" {
  description = "Min read capacity for dynamodb tables"
}

variable "dynamo_min_write_capacity" {
  description = "Min write capacity for dynamodb tables"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

# AWS ElastiCache
variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m4.large"
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = "list"
}

variable "sandstorm_arn" {}

variable "ecs_ec2_backed_min_instances" {}

variable "ecs_logs_cloudwatch_log_group" {
  description = "Cloudwatch log group in which ECS application logs are written"
  type        = "string"
}

variable "ecs_canary_logs_cloudwatch_log_group" {
  description = "Cloudwatch log group in which ECS canary application logs are written"
  type        = "string"
}

variable "whitelisted_arns_for_a2z_privatelink" {
  type = "list"
}

variable "s2s2_auth_logs_retention_in_days" {
  default     = 7
  description = "S2s Auth Log Retention Period, defaults to 7 days"
}
