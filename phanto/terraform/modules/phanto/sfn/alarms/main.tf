variable "state_machine_name" {
  type = "string"
}

variable "state_machine_arn" {
  type = "string"
}

variable "env" {
  type = "string"
}

variable "log_name" {
  type = "string"
}

resource "aws_cloudwatch_metric_alarm" "state_function_executions_throttled_alarm" {
  alarm_name                = "phanto-${var.env}-${var.state_machine_name}-executions-throttled-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/States"
  metric_name               = "ExecutionsThrottled"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.state_machine_name} executions throttled"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = "${var.state_machine_arn}"
  }
}

resource "aws_cloudwatch_metric_alarm" "state_function_executions_failed_alarm" {
  alarm_name                = "phanto-${var.env}-${var.state_machine_name}-executions-failed-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/States"
  metric_name               = "ExecutionsFailed"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.state_machine_name} executions failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = "${var.state_machine_arn}"
  }
}

resource "aws_cloudwatch_metric_alarm" "pdms_dlq_count_warning" {
  count                     = 1
  alarm_name                = "phanto-${var.env}-sqs-pdms-dlq-count-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "PDMS DLQ has at least 1 message"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "eventbus-phanto-user-deletion-queue-dlq"
  }
}