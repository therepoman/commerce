variable "region" {
  description = "The region where AWS resources live"
}

variable "pdms_clean_user_claimed_keys_lambda_arn" {
  description = "The ARN of the PDMS Clean User Claimed Keys lambda"
}

variable "pdms_clean_key_handlers_lambda_arn" {
  description = "The ARN of the PDMS Clean Key Handlers lambda"
}

variable "pdms_report_deletion_lambda_arn" {
  description = "The ARN of the PDMS Report Deletion lambda"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}
