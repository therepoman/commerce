resource "aws_iam_role" "pdms_user_deletion_v1_sfn" {
  name = "pdms_user_deletion_v1"

  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "pdms_user_deletion_v1_sfn_lambda_execution"
  role   = "${aws_iam_role.pdms_user_deletion_v1_sfn.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}

resource "aws_sfn_state_machine" "pdms_user_deletion_v1_sfn_state_machine" {
  name     = "pdms_user_deletion_v1_state_machine"
  role_arn = "${aws_iam_role.pdms_user_deletion_v1_sfn.arn}"

  definition = <<EOF
  {
    "Comment": "Handles Eventbus UserDestroy event",
    "StartAt": "ParallelDeletes",
    "States": {
      "ParallelDeletes": {
        "Type": "Parallel",
        "Next": "ReportDeletion",
        "ResultPath": "$.parallelOutput",
        "Branches": [
          ${local.pdms_clean_user_claimed_keys_step},
          ${local.pdms_clean_key_handlers_step}
        ],
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }]
      },
      "ReportDeletion": {
        "Type": "Task",
        "End": true,
        "Resource": "${var.pdms_report_deletion_lambda_arn}",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }],
        "Retry": ${local.default_retry}
      },
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      }
    }
  }
EOF
}

locals {
  pdms_clean_user_claimed_keys_step = <<EOF
  {
    "StartAt": "CleanUserClaimedKeys",
    "States": {
      "CleanUserClaimedKeys": {
        "Comment": "Clean user ID's claimed keys from keys DB",
        "Type": "Task",
        "Resource": "${var.pdms_clean_user_claimed_keys_lambda_arn}",
        "ResultPath": "$.cleanUserClaimedKeyss",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "CleanUserClaimedKeysFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "CleanUserClaimedKeysFailState": {
        "Comment": "Failed to clean keys claimed for user IDs",
        "Type": "Fail"
      }
    }
  }
EOF

  pdms_clean_key_handlers_step = <<EOF
  {
    "StartAt": "CleanKeyHandlers",
    "States": {
      "CleanKeyHandlers": {
        "Comment": "Cleaning key handlers from key handlers DB",
        "Type": "Task",
        "Resource": "${var.pdms_clean_key_handlers_lambda_arn}",
        "ResultPath": "$.cleanKeyHandlers",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "CleanKeyHandlersFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "CleanKeyHandlersFailState": {
        "Comment": "Failed to clean key handlers for user IDs",
        "Type": "Fail"
      }
    }
  }
EOF
}

module "pdms_user_deletion_v1_alarms" {
  source = "../alarms"

  env                = "${var.env}"
  state_machine_name = "${aws_sfn_state_machine.pdms_user_deletion_v1_sfn_state_machine.name}"
  state_machine_arn  = "${aws_sfn_state_machine.pdms_user_deletion_v1_sfn_state_machine.id}"
  log_name           = "/aws/lambda/pdms_user_deletion"
}

locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF
}
