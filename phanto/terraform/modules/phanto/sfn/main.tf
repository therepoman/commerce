# Looking for state machine definitions? Look for a file named after the feature that the state machine supports.

# Empty IAM role for step functions. If we start using Lambdas, we'll need to add InvokeFunction permission
resource "aws_iam_role" "step_function" {
  name               = "${var.env}-StepFunction"
  assume_role_policy = "${data.aws_iam_policy_document.step_function_assume_role_policy.json}"
}

data "aws_iam_policy_document" "step_function_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["${data.aws_iam_role.admin.arn}"]
      type        = "AWS"
    }
  }
}

data "aws_iam_role" "admin" {
  name = "admin"
}
