variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "sg_id" {
  description = "Id of the security group"
}

resource "aws_iam_role" "download_file_to_s3_lambda" {
  name = "download_file_to_s3_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "download_file_to_s3_lambda" {
  function_name    = "download_file_to_s3"
  filename         = "${path.module}/deployment.zip"
  source_code_hash = "${base64sha256(file("${path.module}/deployment.zip"))}"
  role             = "${aws_iam_role.download_file_to_s3_lambda.arn}"
  handler          = "main"
  runtime          = "go1.x"
  timeout          = 300
  memory_size      = 512

  vpc_config {
    security_group_ids = "${list(var.sg_id)}"
    subnet_ids         = "${split(",",var.private_subnets)}"
  }
}

output "lambda_arn" {
  value = "${aws_lambda_function.download_file_to_s3_lambda.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.download_file_to_s3_lambda.function_name}"
  principal     = "events.amazonaws.com"
}

resource "aws_iam_policy" "download_to_s3_write_cloudwatch_logs_policy" {
  name        = "download_to_s3_write_cloudwatch_logs_policy"
  description = "Write logs to cloudwatch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:*:*:*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "download_to_s3_write_cloudwatch_logs_policy_attachment" {
  name       = "download_to_s3_write_cloudwatch_logs_policy_attachment"
  policy_arn = "${aws_iam_policy.download_to_s3_write_cloudwatch_logs_policy.arn}"
  roles      = ["${aws_iam_role.download_file_to_s3_lambda.name}"]
}

resource "aws_iam_policy" "download_to_s3_write_to_s3_policy" {
  name        = "download_to_s3_write_to_s3_policy"
  description = "writes files to s3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "download_to_s3_write_to_s3_policy_attachment" {
  name       = "download_to_s3_rite_to_s3_policy"
  policy_arn = "${aws_iam_policy.download_to_s3_write_to_s3_policy.arn}"
  roles      = ["${aws_iam_role.download_file_to_s3_lambda.name}"]
}

resource "aws_iam_policy" "download_to_s3_run_in_vpc_policy" {
  name        = "download_to_s3_run_in_vpc_policy"
  description = "allows lambda to run in vpc"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeNetworkInterfaces",
        "ec2:CreateNetworkInterface",
        "ec2:DeleteNetworkInterface"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "download_to_s3_run_in_vpc_policy_attachment" {
  name       = "download_to_s3_run_in_vpc_policy"
  policy_arn = "${aws_iam_policy.download_to_s3_run_in_vpc_policy.arn}"
  roles      = ["${aws_iam_role.download_file_to_s3_lambda.name}"]
}
