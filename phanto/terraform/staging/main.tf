module "phanto" {
  source                               = "../modules/phanto"
  aws_profile                          = "${var.aws_profile}"
  aws_account_id                       = "${var.aws_account_id}"
  env                                  = "${var.env}"
  vpc_id                               = "${var.vpc_id}"
  private_subnets                      = "${var.private_subnets}"
  private_cidr_blocks                  = "${var.private_cidr_blocks}"
  sg_id                                = "${var.sg_id}"
  autoscaling_role                     = "arn:aws:iam::374339732326:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_min_read_capacity             = 5
  dynamo_min_write_capacity            = 5
  elasticache_replicas_per_node_group  = "1"
  elasticache_num_node_groups          = "2"
  elasticache_instance_type            = "cache.m3.medium"
  ecs_ec2_backed_min_instances         = 3
  ecs_logs_cloudwatch_log_group        = "phanto-staging"
  ecs_canary_logs_cloudwatch_log_group = "phanto-staging-canary"
  sandstorm_arn                        = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/phanto-staging"
  whitelisted_arns_for_a2z_privatelink = "${var.whitelisted_arns_for_a2z_privatelink}"
}

terraform {
  backend "s3" {
    bucket  = "phanto-terraform-devo"
    key     = "tfstate/commerce/phanto/terraform/staging"
    region  = "us-west-2"
    profile = "phanto-devo"
  }
}

provider "aws" {
  version = "2.57.0"
  region  = "us-west-2"
  profile = "phanto-devo"
}

module "network-admin-role" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/network-admin-role?ref=1dc0fc2c77353e7f9c1978c2a21ab64552d51f7c"
  region  = "us-west-2"
  account = "phanto-devo"
}

module "twitch-inventory" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory.git?ref=a7207ce935fce975ee739198bcd7e2df80419680"
  account = "phanto-devo"
}
