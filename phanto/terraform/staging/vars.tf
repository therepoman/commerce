variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
  default     = "phanto-devo"
}

variable "aws_account_id" {
  description = "AWS Account ID"
  default     = "374339732326"
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
  default     = "vpc-09be928d9d4044257"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
  default     = "subnet-0acd7d6a2b92443fd,subnet-0fbdb6b2a795ed0bf,subnet-0acaf415a99f40746"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = "list"
  default     = ["10.203.158.0/24", "10.203.157.0/24", "10.203.156.0/24"]
}

variable "sg_id" {
  description = "Id of the security group"
  default     = "sg-0e956094100c09d79"
}

variable "instance_type" {
  description = "EC2 instance type"
  default     = "t3.medium"
}

variable "owner" {
  description = "Owner of the account"
  default     = "twitch-phanto-aws-devo@amazon.com"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/phanto"
}

variable "env" {
  description = "Whether it's production, dev, etc"
  default     = "staging"
}

variable "env_full" {
  description = "Whether it's production, development, etc"
  default     = "staging"
}

variable "cname_prefix" {
  description = "Prefix of the cname"
  default     = "phanto-staging"
}

variable "whitelisted_arns_for_a2z_privatelink" {
  type = "list"

  default = [
    "arn:aws:iam::645130450452:root", // graphql-dev
    "arn:aws:iam::327140220177:root", // helix-dev
    "arn:aws:iam::219087926005:root", // admin panel dev
    "arn:aws:iam::043714768218:root", // Jenkins prod for integration test
  ]
}
