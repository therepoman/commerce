package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/codegangsta/cli"
	"golang.org/x/net/proxy"
)

var environment string

func main() {
	app := cli.NewApp()
	app.Name = "Bits Types Backfiller"
	app.Usage = "Backfills a list of entries that are Bits Types into Petozi"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
	}

	app.Action = backfillBitsTypes

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func backfillBitsTypes(c *cli.Context) {
	logrus.Infof("Loading config: %s", c.String("e"))

	cfg, err := config.LoadConfig(config.GetOrDefaultEnvironment(c.String("e")))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
		panic(err)
	}

	logrus.Infof("starting backfill for %s", cfg.Environment.LongName)

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	fileBytes, err := ioutil.ReadFile(fmt.Sprintf("%s/cmd/bits_types_backfill/data/%s/bits-types.csv", dir, cfg.Environment.LongName))
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	dialer, _ := proxy.SOCKS5("tcp", "127.0.0.1:8080", nil, proxy.Direct)

	client := petozi.NewPetoziProtobufClient(cfg.Environment.Endpoint, &http.Client{
		Transport: &http.Transport{
			Dial: dialer.Dial,
		},
	})

	_, err = client.HealthCheck(context.Background(), &petozi.HealthCheckReq{})
	if err != nil {
		logrus.WithError(err).Fatal("Cannot reach Petozi host. Maybe check your SOCKS proxy.")
		panic(err)
	}

	for _, row := range rows {
		isPaid := strings.EqualFold(row[2], "TRUE")
		isPromotional := strings.EqualFold(row[3], "TRUE")

		costPerBitsUsc, err := strconv.ParseFloat(row[4], 64)
		if err != nil {
			logrus.WithField("bitsType", row[0]).WithError(err).Fatal("could not parse cost per bits")
			continue
		}

		req := &petozi.PutBitsTypeReq{
			BitsType: &petozi.BitsType{
				Id:                    row[0],
				DisplayName:           row[1],
				IsPaid:                isPaid,
				IsPromotional:         isPromotional,
				CostPerBitsUsc:        costPerBitsUsc,
				EntitlementSourceType: petozi.EntitlementSourceType(petozi.EntitlementSourceType_value[row[5]]),
			},
		}

		ctx := context.Background()
		_, err = client.PutBitsType(ctx, req)
		if err != nil {
			logrus.WithField("bitsType", row[0]).WithError(err).Error("could not put bits type")
		}
	}

	logrus.Infof("finished backfill for %s", cfg.Environment.LongName)
}
