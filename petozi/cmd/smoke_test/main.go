package main

import (
	"context"
	"net/http"
	"time"

	petozi "code.justin.tv/commerce/petozi/rpc"

	log "code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

const (
	smokeTestTimeout        = 10 * time.Second
	healthCheckAttemptDelay = 500 * time.Millisecond
	localhostTwirpEndpoint  = "http://localhost:8080"
)

type SmokeTest struct {
	PetoziClient petozi.Petozi
}

func main() {
	log.Println("Starting smoke test")
	petoziClient := petozi.NewPetoziProtobufClient(localhostTwirpEndpoint, http.DefaultClient)
	smokeTest := SmokeTest{
		PetoziClient: petoziClient,
	}

	start := time.Now()
	for time.Since(start) < smokeTestTimeout {
		err := smokeTest.performHealthCheck()
		if err == nil {
			log.Println("Smoke test pinged health check successfully")
			return
		}

		log.Println(err)
		time.Sleep(healthCheckAttemptDelay)
	}
	log.Panicln("Smoke test failed all attempts to ping health check")
}

func (st *SmokeTest) performHealthCheck() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	log.Println("Pinging healthcheck API...")

	resp, err := st.PetoziClient.HealthCheck(ctx, &petozi.HealthCheckReq{})
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.New("receive nil response from health check")
	}

	return nil
}
