package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	ggstrings "code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/codegangsta/cli"
	"github.com/golang/protobuf/ptypes"
	"golang.org/x/net/proxy"
)

var environment string

func main() {
	app := cli.NewApp()
	app.Name = "Bits Products Backfiller"
	app.Usage = "Backfills a list of entries that are Bits Products into Petozi"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
	}

	app.Action = backfillBitsProducts

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func backfillBitsProducts(c *cli.Context) {
	logrus.Infof("Loading config: %s", c.String("e"))

	cfg, err := config.LoadConfig(config.GetOrDefaultEnvironment(c.String("e")))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
		panic(err)
	}

	logrus.Infof("starting backfill for %s", cfg.Environment.LongName)

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	fileBytes, err := ioutil.ReadFile(fmt.Sprintf("%s/cmd/bits_products_backfill/data/%s/bits-products.csv", dir, cfg.Environment.LongName))
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	dialer, _ := proxy.SOCKS5("tcp", "127.0.0.1:8080", nil, proxy.Direct)

	client := petozi.NewPetoziProtobufClient(cfg.Environment.Endpoint, &http.Client{
		Transport: &http.Transport{
			Dial: dialer.Dial,
		},
	})

	_, err = client.HealthCheck(context.Background(), &petozi.HealthCheckReq{})
	if err != nil {
		logrus.WithError(err).Fatal("Cannot reach Petozi host. Maybe check your SOCKS proxy.")
		panic(err)
	}

	for _, row := range rows {
		fields := logrus.Fields{"platform": row[0], "product": row[1]}

		quantity, err := strconv.ParseInt(row[4], 10, 64)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Fatal("could not parse bits quantity")
			continue
		}

		maxPurchasableQuantity, err := strconv.ParseInt(row[9], 10, 64)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Fatal("could not parse max purchasable quantity")
			continue
		}

		promotional := strings.EqualFold(row[5], "TRUE")
		var promo *petozi.Promo
		if promotional {
			startParsed, err := time.Parse("2006/01/02 15:04 MST", row[12])
			if err != nil {
				logrus.WithFields(fields).WithError(err).Fatal("could not parse start date from csv")
				continue
			}

			start, err := ptypes.TimestampProto(startParsed)
			if err != nil {
				logrus.WithFields(fields).WithError(err).Fatal("could not convert start date to proto time")
				continue
			}

			endParsed, err := time.Parse("2006/01/02 15:04 MST", row[10])
			if err != nil {
				logrus.WithFields(fields).WithError(err).Fatal("could not parse end date from csv")
				continue
			}

			end, err := ptypes.TimestampProto(endParsed)
			if err != nil {
				logrus.WithFields(fields).WithError(err).Fatal("could not convert end date to proto time")
				continue
			}

			promo = &petozi.Promo{
				Id:    row[11],
				Type:  petozi.PromoType(petozi.EntitlementSourceType_value[strings.ToUpper(row[13])]),
				Start: start,
				End:   end,
			}
		}

		pricingID := "NO_PRICING_ID"
		if !ggstrings.Blank(row[18]) {
			pricingID = row[18]
		}

		showWhenLoggedOut := strings.EqualFold(row[19], "TRUE")

		req := &petozi.PutBitsProductReq{
			NewBitsProduct: &petozi.BitsProduct{
				Platform:               petozi.Platform(petozi.Platform_value[row[0]]),
				Id:                     row[1],
				DisplayName:            row[2],
				BitsTypeId:             row[3],
				Quantity:               uint64(quantity),
				MaxPurchasableQuantity: uint32(maxPurchasableQuantity),
				Promo:                  promo,
				PricingId:              pricingID,
				ShowWhenLoggedOut:      showWhenLoggedOut,
			},
		}

		ctx := context.Background()
		_, err = client.PutBitsProduct(ctx, req)
		if err != nil {
			logrus.WithFields(fields).WithError(err).Error("could not put bits product")
		}
	}

	logrus.Infof("finished backfill for %s", cfg.Environment.LongName)
}
