// +build integration

package bits_products

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/petozi/integration_tests/common"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	testingBitsProductFormat = "INTEG_TEST_%s"
	testingBitsType          = "CB_0"
)

func TestBitsProducts(t *testing.T) {
	Convey("Given a petozi client", t, func() {
		client := common.GetPetoziClient()

		bitsProductID := generateIntegTestBitsProduct()

		Convey("when we fail to create a valid bits product", func() {
			resp, err := client.PutBitsProduct(context.Background(), &petozi.PutBitsProductReq{
				NewBitsProduct: &petozi.BitsProduct{
					Platform:               petozi.Platform_AMAZON,
					Id:                     bitsProductID,
					DisplayName:            "WALRUS TESTING BITS",
					Quantity:               322,
					MaxPurchasableQuantity: 1,
					BitsTypeId:             testingBitsType,
				},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "invalid_argument")

			Convey("when we succeed to create a valid bits product", func() {
				resp, err := client.PutBitsProduct(context.Background(), &petozi.PutBitsProductReq{
					NewBitsProduct: &petozi.BitsProduct{
						Platform:               petozi.Platform_AMAZON,
						Id:                     bitsProductID,
						DisplayName:            "WALRUS TESTING BITS",
						Quantity:               322,
						MaxPurchasableQuantity: 1,
						BitsTypeId:             testingBitsType,
						PricingId:              "FAKE_PRICE",
					},
				})

				So(resp, ShouldNotBeNil)
				So(err, ShouldBeNil)

				Convey("when we succeed to update a valid bits product", func() {
					resp, err := client.PutBitsProduct(context.Background(), &petozi.PutBitsProductReq{
						NewBitsProduct: &petozi.BitsProduct{
							Platform:               petozi.Platform_AMAZON,
							Id:                     bitsProductID,
							DisplayName:            "WALRUS TESTING BITS",
							Quantity:               644,
							MaxPurchasableQuantity: 1,
							BitsTypeId:             testingBitsType,
							PricingId:              "FAKE_PRICE",
						},
					})

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)

					productResp, err := client.GetBitsProduct(context.Background(), &petozi.GetBitsProductReq{
						Platform: petozi.Platform_AMAZON,
						Id:       bitsProductID,
					})

					So(err, ShouldBeNil)
					So(productResp, ShouldNotBeNil)
					So(productResp.BitsProduct, ShouldNotBeNil)
					So(productResp.BitsProduct.Quantity, ShouldEqual, uint64(644))
				})
			})
		})
	})
}

func generateIntegTestBitsProduct() string {
	return fmt.Sprintf(testingBitsProductFormat, random.String(16))
}
