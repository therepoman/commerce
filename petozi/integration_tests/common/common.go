package common

import (
	"net/http"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

func GetPetoziClient() petozi.Petozi {
	envVar := os.Getenv(config.EnvironmentEnvironmentVariable)
	if envVar != "" {
		logrus.Infof("Found ENVIRONMENT environment variable: %s", envVar)
	}

	env := config.GetOrDefaultEnvironment(envVar)

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	logrus.Infof("Loaded config: %s", cfg.Environment.Name)

	if env == config.Prod {
		logrus.Panic("Please don't run these integration tests against prod!")
	}

	return petozi.NewPetoziProtobufClient(cfg.Environment.Endpoint, http.DefaultClient)
}
