// +build integration

package bits_types

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/petozi/integration_tests/common"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	testingBitsTypeFormat = "INTEG_TEST_%s"
)

func TestBitsTypes(t *testing.T) {
	Convey("Given a petozi client", t, func() {
		client := common.GetPetoziClient()

		bitsTypeID := generateIntegTestBitsType()

		Convey("when we fail to create a valid bits type", func() {
			resp, err := client.PutBitsType(context.Background(), &petozi.PutBitsTypeReq{
				BitsType: &petozi.BitsType{
					Id:                    bitsTypeID,
					DisplayName:           "WALRUS TESTING BITS",
					EntitlementSourceType: petozi.EntitlementSourceType(-1),
				},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "invalid_argument")

			Convey("when we succeed to create a valid bits type", func() {
				resp, err := client.PutBitsType(context.Background(), &petozi.PutBitsTypeReq{
					BitsType: &petozi.BitsType{
						Id:                    bitsTypeID,
						DisplayName:           "WALRUS TESTING BITS",
						EntitlementSourceType: petozi.EntitlementSourceType_DIRECT_ENTITLEMENT,
					},
				})

				So(resp, ShouldNotBeNil)
				So(err, ShouldBeNil)

				Convey("when we succeed to update a valid bits type", func() {
					resp, err := client.PutBitsType(context.Background(), &petozi.PutBitsTypeReq{
						BitsType: &petozi.BitsType{
							Id:                    bitsTypeID,
							DisplayName:           "WALRUS TESTING BITS",
							CostPerBitsUsc:        3.22,
							EntitlementSourceType: petozi.EntitlementSourceType_DIRECT_ENTITLEMENT,
						},
					})

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)

					bitsTypeResp, err := client.GetBitsType(context.Background(), &petozi.GetBitsTypeReq{
						Id: bitsTypeID,
					})

					So(err, ShouldBeNil)
					So(bitsTypeResp, ShouldNotBeNil)
					So(bitsTypeResp.BitsType, ShouldNotBeNil)
					So(bitsTypeResp.BitsType.CostPerBitsUsc, ShouldEqual, 3.22)
				})
			})
		})
	})
}

func generateIntegTestBitsType() string {
	return fmt.Sprintf(testingBitsTypeFormat, random.String(16))
}
