region = "us-west-2"

environment = "staging"

environment_short = "staging"

aws_profile = "petozi-devo"

account_id = "447289724554"

vpc_id = "vpc-0e76c38d9ffa83032"

security_group = "sg-079c266012edc644d"

subnets = "subnet-05ef2897bf83eefa6,subnet-06dbcc612e1c5ff64,subnet-0b19bb502453842aa"

cidr_block = "10.205.176.0/22"