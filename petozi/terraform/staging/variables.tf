variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "aws_profile" {
}

variable "account_id" {
}

variable "vpc_id" {
}

variable "region" {
}

variable "security_group" {
}

variable "environment" {
}

variable "environment_short" {
}

variable "subnets" {
}

variable "cidr_block" {
}

provider "aws" {
  version    = "2.44"
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
}

