module "petozi" {
  source = "../petozi"
  commit = "latest"

  name                = "petozi"
  security_group      = var.security_group
  region              = var.region
  vpc_id              = var.vpc_id
  environment         = var.environment
  environment_short   = var.environment_short
  subnets             = var.subnets
  account_id          = var.account_id
  vpc_cidr_block      = var.cidr_block
  cpu                 = "2048"
  memory              = "4096"
  min_count           = "25"
  redis_instance_type = "cache.m4.large"

  bits_types_min_read  = 5
  bits_types_min_write = 5

  bits_products_min_read  = 5
  bits_products_min_write = 5
}

terraform {
  backend "s3" {
    bucket  = "twitch-petozi-aws-prod"
    key     = "tfstate/commerce/petozi/terraform/production"
    region  = "us-west-2"
    profile = "petozi-prod"
  }
}

