region = "us-west-2"

environment = "production"

environment_short = "prod"

aws_profile = "petozi-prod"

account_id = "919883070396"

vpc_id = "vpc-030de45023a90732b"

security_group = "sg-0a873be57dede2eaa"

subnets = "subnet-004cbdf07e3dd181b,subnet-0855a8718f597c21a,subnet-0d40d1f39d08f9fd4"

cidr_block = "10.205.180.0/22"
