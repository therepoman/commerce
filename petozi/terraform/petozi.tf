locals {
  bucket = {
    staging = "twitch-petozi-aws-devo"
    prod = "twitch-petozi-aws-prod"
  }


}

module "petozi" {
  source = "petozi"
  commit = "latest"

  name           = "petozi"
  security_group = var.security_group
  region         = var.region
  vpc_id         = var.vpc_id
  environment    = var.environment
  subnets        = var.subnets
}

terraform {
  backend "s3" {
    bucket  = "twitch-petozi-aws-devo"
    key     = "tfstate/commerce/petozi/terraform/staging"
    region  = "us-west-2"
    profile = "pachinko-devo"
  }
}
