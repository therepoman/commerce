region = "us-west-2"

environment = "staging"

aws_profile = "petozi-devo"

vpc_id = "vpc-0e76c38d9ffa83032"

security_group = "sg-079c266012edc644d"

subnets = "subnet-05ef2897bf83eefa6,subnet-06dbcc612e1c5ff64,subnet-0b19bb502453842aa"
