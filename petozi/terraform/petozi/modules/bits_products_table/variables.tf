variable "suffix" {
  description = "Suffix at the end of dynamo table names"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}

variable "min_gsi_read_capacity" {
  description = "Minimum dynamodb GSI read capacity"
}

variable "min_gsi_write_capacity" {
  description = "Minimum dynamodb GSI write capacity"
}

variable "dynamo_backup_lambda_arn" {
  description = "ARN for lambda function that performs dynamo backups"
}

