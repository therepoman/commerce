resource "aws_dynamodb_table" "bits_products_table" {
  name             = "bits-products-${var.suffix}"
  hash_key         = "platform"
  range_key        = "productId"
  read_capacity    = var.min_read_capacity
  write_capacity   = var.min_write_capacity
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "platform"
    type = "N"
  }

  attribute {
    name = "productId"
    type = "S"
  }

  global_secondary_index {
    hash_key        = "productId"
    name            = "bits-products-product-id-gsi-${var.suffix}"
    projection_type = "ALL"
    read_capacity   = var.min_gsi_read_capacity
    write_capacity  = var.min_gsi_write_capacity
  }
}

module "bits_products_table_product_id_gsi_autoscaling" {
  source             = "../dynamo_table_autoscaling"
  table_name         = "bits-products-${var.suffix}/index/bits-products-product-id-gsi-${var.suffix}"
  table_kind         = "index"
  min_read_capacity  = var.min_gsi_read_capacity
  min_write_capacity = var.min_gsi_write_capacity
  autoscaling_role   = var.autoscaling_role
}

module "bits_products_table_autoscaling" {
  source             = "../dynamo_table_autoscaling"
  table_name         = "bits-products-${var.suffix}"
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  autoscaling_role   = var.autoscaling_role
}

module "bits_products_scheduled_backups" {
  source                   = "../scheduled_dynamo_backup_event"
  table_name               = "bits-products-${var.suffix}"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}

