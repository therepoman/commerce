variable "queue_name" {
  description = "the name of the sqs queue"
}

variable "receive_wait_time" {
  type    = string
  default = 20
}

variable "message_retention_seconds" {
  type    = string
  default = 1209600
}

variable "max_receive_count" {
  default = 4
}

variable "delay_seconds" {
  default = 0
}

variable "send_allowed_resource" {
  default = []
  type    = list(string)
}

variable "visibility_timeout_seconds" {
  default = 30
}

resource "aws_sqs_queue" "sqs_queue" {
  message_retention_seconds  = var.message_retention_seconds
  name                       = var.queue_name
  receive_wait_time_seconds  = var.receive_wait_time
  delay_seconds              = var.delay_seconds
  visibility_timeout_seconds = var.visibility_timeout_seconds

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.sqs_queue_deadletter.arn}",
  "maxReceiveCount":     ${var.max_receive_count}
}
EOF

}

resource "aws_sqs_queue" "sqs_queue_deadletter" {
  name                      = "${var.queue_name}-deadletter"
  message_retention_seconds = var.message_retention_seconds
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = aws_sqs_queue.sqs_queue.id
  policy    = data.aws_iam_policy_document.queue_policy_doc.json
}

data "aws_iam_policy_document" "queue_policy_doc" {
  statement {
    sid = "${var.queue_name}-Sid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [aws_sqs_queue.sqs_queue.arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = var.send_allowed_resource
    }
  }
}

output "queue_arn" {
  value = aws_sqs_queue.sqs_queue.arn
}

output "queue_url" {
  value = aws_sqs_queue.sqs_queue.id
}
