resource "aws_dynamodb_table" "bits_types_table" {
  name             = "bits-types-${var.suffix}"
  hash_key         = "id"
  read_capacity    = var.min_read_capacity
  write_capacity   = var.min_write_capacity
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "id"
    type = "S"
  }
}

module "bits_types_table_autoscaling" {
  source             = "../dynamo_table_autoscaling"
  table_name         = "bits-types-${var.suffix}"
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  autoscaling_role   = var.autoscaling_role
}

module "bits_types_scheduled_backups" {
  source                   = "../scheduled_dynamo_backup_event"
  table_name               = "bits-types-${var.suffix}"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}

