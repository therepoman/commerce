variable "table_name" {
  type = string
}

variable "table_kind" {
  description = "Either table or index depending on table kind being targeted"
  default     = "table"
}

variable "autoscaling_role" {
  type = string
}

variable "min_read_capacity" {
  type    = string
  default = "5"
}

variable "max_read_capacity" {
  type    = string
  default = "10000"
}

variable "min_write_capacity" {
  type    = string
  default = "5"
}

variable "max_write_capacity" {
  type    = string
  default = "10000"
}

variable "read_utilization" {
  type    = string
  default = "50"
}

variable "write_utilization" {
  type    = string
  default = "50"
}

resource "aws_appautoscaling_target" "dynamodb_read_target" {
  max_capacity       = var.max_read_capacity
  min_capacity       = var.min_read_capacity
  resource_id        = "table/${var.table_name}"
  role_arn           = var.autoscaling_role
  scalable_dimension = "dynamodb:${var.table_kind}:ReadCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_read_policy" {
  name               = "DynamoDBReadCapacityUtilization:${aws_appautoscaling_target.dynamodb_read_target.resource_id}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.dynamodb_read_target.resource_id
  scalable_dimension = aws_appautoscaling_target.dynamodb_read_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.dynamodb_read_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "DynamoDBReadCapacityUtilization"
    }

    target_value = 70
  }
  depends_on = [aws_appautoscaling_target.dynamodb_read_target]
}

resource "aws_appautoscaling_target" "dynamodb_write_target" {
  max_capacity       = var.max_write_capacity
  min_capacity       = var.min_write_capacity
  resource_id        = "table/${var.table_name}"
  role_arn           = var.autoscaling_role
  scalable_dimension = "dynamodb:${var.table_kind}:WriteCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_write_policy" {
  name               = "DynamoDBWriteCapacityUtilization:${aws_appautoscaling_target.dynamodb_write_target.resource_id}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.dynamodb_write_target.resource_id
  scalable_dimension = aws_appautoscaling_target.dynamodb_write_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.dynamodb_write_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "DynamoDBWriteCapacityUtilization"
    }

    target_value = 70
  }
  depends_on = [aws_appautoscaling_target.dynamodb_write_target]
}

