module "bits_types_table" {
  source                   = "./modules/bits_types_table"
  suffix                   = var.environment
  min_read_capacity        = var.bits_types_min_read
  min_write_capacity       = var.bits_types_min_write
  autoscaling_role         = "arn:aws:iam::${var.account_id}:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_backup_lambda_arn = module.dynamo_backup_lambda.lambda_arn
}

module "bits_products_table" {
  source                   = "./modules/bits_products_table"
  suffix                   = var.environment
  min_read_capacity        = var.bits_products_min_read
  min_write_capacity       = var.bits_products_min_write
  min_gsi_read_capacity    = var.bits_products_min_read
  min_gsi_write_capacity   = var.bits_products_min_write
  autoscaling_role         = "arn:aws:iam::${var.account_id}:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_backup_lambda_arn = module.dynamo_backup_lambda.lambda_arn
}

resource "aws_sns_topic" "db_export_glue_errors" {
  name = "petozi-dbexport-${var.environment}-error-sns"
}

module "glue-dynamo" {
  source        = "git::ssh://git@git.xarth.tv/stats/db-s3-glue.git?ref=b68f867b6f9e8b32372d3388d896d2c4b018a4f5"
  database_type = "dynamodb"
  job_name      = "petozi-dbexport-${var.environment}-job"
  cluster_name  = "bits-products-${var.environment}"
  table_config = {
    "bits-products-${var.environment}" = <<EOF
      {
        "dpu_count": 1,
        "read_ratio": 0.2,
        "version": 0,
        "tahoe_view_name": "bits_products_${var.environment}",
        "schema": [
          {"name": "platform", "type": "bigint"},
          {"name": "productId", "type": "string"},
          {"name": "bitsType", "type": "string"},
          {"name": "displayName", "type": "string"},
          {"name": "maxQuantity", "type": "bigint"},
          {"name": "pricingId", "type": "string"},
          {"name": "quantity", "type": "bigint"},
          {"name": "shownWhenLoggedOut", "type": "boolean"},
          {"name": "promo", "type": "string"},
          {"name": "offerId", "type": "string"}
        ]
      }
EOF

  }

  # For Parquet, all fields need to be present. Due to the schema-less nature of DynamoDB,
  # missing fields can happen on a per document-basis. This is why the script needs to
  # add missing fields.
  cleaning_code = <<CODE
from ast import literal_eval
def ensure_fields(rec):
    for f, t in schema:
        if f not in rec:
            rec[f] = None
    return rec
return Map.apply(frame=frame, f=ensure_fields)
CODE


  dynamodb_splits_count      = "1"
  create_s3_output_bucket    = 1
  s3_output_bucket           = "petozi-dbexport-${var.environment}-output-bucket"
  s3_output_key              = "petozi-dbexport-${var.environment}-output-key"
  create_s3_script_bucket    = 1
  s3_script_bucket           = "petozi-dbexport-${var.environment}-script-bucket"
  error_sns_topic_name       = aws_sns_topic.db_export_glue_errors.name
  account_number             = var.account_id
  vpc_id                     = ""
  subnet_id                  = ""
  availability_zone          = ""
  rds_subnet_group           = ""
  cluster_username           = ""
  db_password_parameter_name = ""
  db_password_key_id         = ""
  api_key_parameter_name     = aws_ssm_parameter.tahoe_api_password.name
  api_key_kms_key_id         = aws_kms_alias.tahoe_api.target_key_id
  tahoe_producer_name        = "petozi${var.environment}dbexport"
  tahoe_producer_role_arn    = "arn:aws:iam::331582574546:role/producer-petozi${var.environment}dbexport"
  trigger_schedule           = "0 0,8 * * ? *"
}

output "s3_kms_key" {
  value = module.glue-dynamo.s3_kms_key
}

output "s3_output_bucket" {
  value = module.glue-dynamo.s3_output_bucket
}

output "glue_role" {
  value = module.glue-dynamo.glue_role
}

module "glue-dynamo-bits-types" {
  source = "git::ssh://git@git.xarth.tv/stats/db-s3-glue.git?ref=b68f867b6f9e8b32372d3388d896d2c4b018a4f5"
  database_type = "dynamodb"
  job_name = "petozi-dbexport-${var.environment}-bits-types-job"
  cluster_name = "bits-types-${var.environment}"
  table_config = {
    "bits-types-${var.environment}" = <<EOF
      {
        "dpu_count": 1,
        "read_ratio": 0.2,
        "version": 1,
        "tahoe_view_name": "bits_types_${var.environment}",
        "schema": [
          {"name": "id", "type": "string"},
          {"name": "costPerBitsUSC", "type": "float"},
          {"name": "displayName", "type": "string"},
          {"name": "entitlementSourceType", "type": "bigint"},
          {"name": "paid", "type": "boolean"},
          {"name": "promotional", "type": "boolean"},
          {"name": "businessAttribution", "type": "bigint"},
          {"name": "spendOrder", "type": "bigint"}
        ]
      }
EOF
  }

  # For Parquet, all fields need to be present. Due to the schema-less nature of DynamoDB,
  # missing fields can happen on a per document-basis. This is why the script needs to
  # add missing fields.
  cleaning_code = <<CODE
from ast import literal_eval
def ensure_fields(rec):
    for f, t in schema:
        if f not in rec:
            rec[f] = None
    return rec
frame = frame.resolveChoice(specs = [("costPerBitsUSC", "cast:double")])
return Map.apply(frame=frame, f=ensure_fields)
CODE

  dynamodb_splits_count      = "1"
  create_s3_output_bucket    = 0
  s3_output_bucket           = "petozi-dbexport-${var.environment}-output-bucket"
  s3_output_key              = "petozi-dbexport-${var.environment}-output-key"
  s3_output_kms_key_arn      = module.glue-dynamo.s3_kms_key
  create_s3_script_bucket    = 0
  s3_script_bucket           = "petozi-dbexport-${var.environment}-script-bucket"
  error_sns_topic_name       = "petozi-dbexport-${var.environment}-error-sns"
  account_number             = var.account_id
  vpc_id                     = ""
  subnet_id                  = ""
  availability_zone          = ""
  rds_subnet_group           = ""
  cluster_username           = ""
  db_password_parameter_name = ""
  db_password_key_id         = ""
  api_key_parameter_name     = aws_ssm_parameter.tahoe_api_password.name
  api_key_kms_key_id         = aws_kms_alias.tahoe_api.target_key_id
  tahoe_producer_name        = "petozi${var.environment}dbexport"
  tahoe_producer_role_arn    = "arn:aws:iam::331582574546:role/producer-petozi${var.environment}dbexport"
  trigger_schedule           = "0 0,8 * * ? *"
}

