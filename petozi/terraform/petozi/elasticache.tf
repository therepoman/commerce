locals {
  replicas = {
    production = 5
    staging    = 1
  }

  nodes = {
    production = 2
    staging    = 1
  }
}

module "redis_cluster" {
  source             = "git::git+ssh://git@git.xarth.tv/subs/terracode//terraform-12/redis?ref=0afb9b2c9d94728d8c759e2656920fc55edc6299"
  name               = var.name
  environment        = var.environment_short
  instance_type      = var.redis_instance_type
  subnets            = var.subnets
  security_groups    = aws_security_group.main.id
  node_groups        = local.nodes[var.environment]
  replicas_per_group = local.replicas[var.environment]
}

resource "aws_security_group" "main" {
  name        = "petozi-${var.environment}-redis"
  vpc_id      = var.vpc_id
  description = "Allows communication with redis server"

  ingress {
    from_port   = "6379"
    protocol    = "tcp"
    to_port     = "6379"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "petozi-${var.environment}-redis"
  }
}

