module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone?ref=0afb9b2c9d94728d8c759e2656920fc55edc6299"
  name        = "petozi"
  environment = var.environment
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=0afb9b2c9d94728d8c759e2656920fc55edc6299"

  name        = "petozi"
  environment = var.environment
  zone_id     = module.privatelink-zone.zone_id
  alb_dns     = aws_alb.alb.dns_name
}

// TODO: import endpoint service and nlb->alb lambda into terraform

// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0afb9b2c9d94728d8c759e2656920fc55edc6299"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = [var.security_group]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.subnets)
  dns             = "ldap.twitch.a2z.com"
}