// Create resources according to the Wiki
// https://wiki.twitch.com/display/PS/How+to+Configure+a+VPC+Resolver+for+Internal+Twitch+DNS
locals {
  jtv_dns_ips = {
    prod = [
      "10.203.126.62",
      "10.203.124.218",
      "10.203.125.211",
      "10.203.126.205",
    ]
    staging = [
      "10.204.232.126",
      "10.204.233.37",
      "10.204.234.157",
      "10.204.233.199",
    ]
  }
}

resource "aws_route53_resolver_rule_association" "fwd_justin_tv_dns_cache_rule_association" {
  resolver_rule_id = aws_route53_resolver_rule.fwd_justin_tv_dns_cache.id
  vpc_id           = var.vpc_id
}

resource "aws_route53_resolver_rule" "fwd_justin_tv_dns_cache" {
  domain_name          = "justin.tv"
  name                 = "fwd_justin_tv_dns_cache"
  rule_type            = "FORWARD"
  resolver_endpoint_id = aws_route53_resolver_endpoint.outbound_endpoint.id

  target_ip {
    ip = local.jtv_dns_ips[var.environment_short][0]
  }

  target_ip {
    ip = local.jtv_dns_ips[var.environment_short][1]
  }

  target_ip {
    ip = local.jtv_dns_ips[var.environment_short][2]
  }

  target_ip {
    ip = local.jtv_dns_ips[var.environment_short][3]
  }
}

resource "aws_route53_resolver_endpoint" "outbound_endpoint" {
  name      = "${var.environment_short}-petozi-vpc-outbound-dns"
  direction = "OUTBOUND"

  security_group_ids = [
    var.security_group,
  ]

  ip_address {
    subnet_id = split(",", var.subnets)[0]
  }

  ip_address {
    subnet_id = split(",", var.subnets)[1]
  }

  ip_address {
    subnet_id = split(",", var.subnets)[2]
  }
}

// Point DNS look up to AmazonProvidedDNS (route 53)
resource "aws_vpc_dhcp_options" "dns_resolver" {
  domain_name         = "${var.region}.compute.internal"
  domain_name_servers = ["AmazonProvidedDNS"]
  // https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/set-time.html
  ntp_servers = ["169.254.169.123"]
  tags = {
    Name = "AmazonProvidedDNS"
  }
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = var.vpc_id
  dhcp_options_id = aws_vpc_dhcp_options.dns_resolver.id
}