locals {
  account_id = {
    production = "919883070396"
    staging    = "447289724554"
  }

  counts = {
    production = 1
    staging    = 20
  }
}

resource "aws_cloudwatch_log_group" "cw_logs" {
  name              = "${var.name}-ECSLogGroup"
  retention_in_days = 60
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${lower(var.name)}-cluster"
}

data "template_file" "container_def" {
  template = file("${path.module}/container_defs.json")
  vars = {
    name       = var.name
    account_id = local.account_id[var.environment]
    region     = var.region
    log_group  = aws_cloudwatch_log_group.cw_logs.name
    env        = jsonencode(var.environment_short)
  }
}

resource "aws_ecs_task_definition" "ecs_task" {
  family                   = var.name
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  requires_compatibilities = ["FARGATE", "EC2"]
  cpu                      = var.cpu
  memory                   = var.memory

  lifecycle {
    create_before_destroy = true
  }

  container_definitions = data.template_file.container_def.rendered
}

resource "aws_ecs_service" "ecs_service" {
  desired_count                      = 1
  name                               = "${var.name}-service"
  task_definition                    = aws_ecs_task_definition.ecs_task.arn
  cluster                            = aws_ecs_cluster.ecs_cluster.arn
  deployment_minimum_healthy_percent = 100
  launch_type                        = "FARGATE"
  platform_version                   = "1.3.0"
  health_check_grace_period_seconds  = "60"

  load_balancer {
    target_group_arn = aws_alb_target_group.ssl_target.arn
    container_name   = var.name
    container_port   = 8080
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      desired_count,
      task_definition,
    ]
  }

  network_configuration {
    subnets          = split(",", var.subnets)
    security_groups  = [var.security_group, aws_security_group.main.id]
    assign_public_ip = false
  }
}

resource "aws_appautoscaling_target" "ecs_app_autoscaling_target" {
  max_capacity       = var.max_count
  min_capacity       = var.min_count
  resource_id        = "service/${aws_ecs_cluster.ecs_cluster.name}/${aws_ecs_service.ecs_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  role_arn           = aws_iam_role.ecs_autoscaling_role.arn
}

resource "aws_appautoscaling_policy" "ecs_app_autoscaling_policy" {
  name        = "${var.max_count}-ecs-tracker"
  policy_type = "TargetTrackingScaling"
  target_tracking_scaling_policy_configuration {
    scale_in_cooldown  = 60
    scale_out_cooldown = 60
    target_value       = 50
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }
  resource_id        = aws_appautoscaling_target.ecs_app_autoscaling_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_app_autoscaling_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_app_autoscaling_target.service_namespace
}

