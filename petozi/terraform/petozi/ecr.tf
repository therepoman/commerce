resource "aws_ecr_repository" "main" {
  name = "commerce/${lower(var.name)}"
}

resource "aws_ecr_lifecycle_policy" "ecr_lifecyle_policy" {
  repository = aws_ecr_repository.main.name

  policy = <<EOT
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "Only keep untagged images for 20 days",
      "selection": {
        "tagStatus": "untagged",
        "countType": "sinceImagePushed",
        "countUnit": "days",
        "countNumber": 20
      },
      "action": { "type": "expire" }
    },
    {
      "rulePriority": 2,
      "description": "Keep only 100 app tags",
      "selection": {
        "tagStatus": "tagged",
        "countType": "imageCountMoreThan",
        "tagPrefixList": [ "app-" ],
        "countNumber": 100
      },
      "action": { "type": "expire" }
    },
    {
      "rulePriority": 3,
      "description": "Keep only 15 builder tags",
      "selection": {
        "tagStatus": "tagged",
        "countType": "imageCountMoreThan",
        "tagPrefixList": [ "builder-" ],
        "countNumber": 15
      },
      "action": { "type": "expire" }
    },
    {
      "rulePriority": 4,
      "description": "Keep only 10 integration testing tags",
      "selection": {
        "tagStatus": "tagged",
        "countType": "imageCountMoreThan",
        "tagPrefixList": [ "MaxIntegrationTags-" ],
        "countNumber": 10
      },
      "action": { "type": "expire" }
    },
    {
      "rulePriority": 5,
      "description": "Keep only 20 other tagged tags",
      "selection": {
        "tagStatus": "any",
        "countType": "imageCountMoreThan",
        "countNumber": 20
      },
      "action": { "type": "expire" }
    }
  ]
}
EOT

}

