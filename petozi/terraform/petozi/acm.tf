resource "aws_acm_certificate" "ssl_cert" {
  domain_name       = "*.${var.environment}.${lower(var.name)}.internal.justin.tv"
  validation_method = "DNS"
}

