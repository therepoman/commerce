resource "aws_cloudwatch_metric_alarm" "logscan_errors_alert_alarm" {
  alarm_name                = "petozi-${var.environment_short}-logscan-errors-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "petozi-errors"
  namespace                 = "petozi-logs"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "100"
  alarm_description         = "Alarms when we have a significant amount of errors in our logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "logscan_errors_warning_alarm" {
  alarm_name                = "petozi-${var.environment_short}-logscan-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "petozi-errors"
  namespace                 = "petozi-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "15"
  alarm_description         = "Alarms when we have a slight increase of errors in our logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_alert_alarm" {
  alarm_name                = "petozi-${var.environment_short}-logscan-panics-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  metric_name               = "petozi-panics"
  namespace                 = "petozi-logs"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "25"
  alarm_description         = "Alarms when we have a significant amount of panics in our logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_warning_alarm" {
  alarm_name                = "petozi-${var.environment_short}-logscan-panics-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "petozi-panics"
  namespace                 = "petozi-logs"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "Alarms when we have a slight increase in panics in our logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "dynamo_export_glue_error_queue_not_empty" {
  alarm_name          = "dynamo_export_glue_error_queue_not_empty"
  alarm_description   = "Alarms when Dynamo export glue job errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = aws_sns_topic.db_export_glue_errors.name
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  treat_missing_data = "notBreaching"
}