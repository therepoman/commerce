resource "aws_iam_role" "ecs_autoscaling_role" {
  name               = "${var.name}-ecs-scaler"
  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": { "Service": "application-autoscaling.amazonaws.com"},
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "ecs_autoscaling_role_policy" {
  name   = "${var.name}-ecs-scaler"
  role   = aws_iam_role.ecs_autoscaling_role.id
  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "application-autoscaling:*",
        "cloudwatch:DescribeAlarms",
        "cloudwatch:PutMetricAlarm",
        "ecs:DescribeServices",
        "ecs:UpdateService"
      ],
      "Resource": "*"
    }
  ]
}
EOT

}

resource "aws_iam_role" "ecr_pusher_role" {
  name               = "${var.name}-ecr-pusher"
  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${aws_iam_role.pipeline.arn}",
          "arn:aws:iam::946879801923:role/Admin"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "ecr_pusher_role_policy" {
  name   = "${var.name}-ecr-pusher"
  role   = aws_iam_role.ecr_pusher_role.id
  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "iam:PassRole"
      ],
      "Effect": "Allow",
      "Resource": "${aws_iam_role.ecs_autoscaling_role.arn}"
    },
    {
      "Action": [
        "ecr:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_ecr_repository.main.arn}"
    },
    {
      "Action": [
        "autoscaling:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOT

}

resource "aws_iam_role" "ecs_task_execution_role" {
  name               = "${var.name}-ecs-task-execution"
  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_role.ecr_pusher_role.arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_ecs_attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_cloudwatch_attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsReadOnlyAccess"
}

resource "aws_iam_role" "ecs_task_role" {
  name               = "${var.name}-ecs-task"
  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_role.ecr_pusher_role.arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "ecs_task_role_s2s_policy" {
  name   = "${var.name}-s2s-policy"
  role   = aws_iam_role.ecs_task_role.id
  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "arn:aws:iam::180116294062:role/malachai/*",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy_attachment" "ecs_task_role_dynamo_access" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// Grant the Instance Profile full Firehose access
resource "aws_iam_role_policy_attachment" "firehose_policy_attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFirehoseFullAccess"
}

// Grant the Instance Profile Read only Elasticache Permissions
resource "aws_iam_role_policy_attachment" "elasticache_policy_attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonElastiCacheReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "cloudwatch_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  role       = aws_iam_role.ecs_task_role.name
}

resource "aws_iam_role_policy_attachment" "attach_ssm_access_policy" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role" "pipeline" {
  name               = "${var.name}-pipeline"
  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::919883070396:user/JenkinsLimitedUser"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "pipeline_assume_app_roles" {
  role   = aws_iam_role.pipeline.name
  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "arn:aws:iam::*:role/*pachinko*",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "pipeline_builder_push" {
  role   = aws_iam_role.pipeline.name
  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "*",
      "Action": "ecr:*"
    }
  ]
}
EOT

}

resource "aws_iam_instance_profile" "ecs_instance_profile" {
  name = "${var.name}-ecs-instance-profile"
  role = aws_iam_role.ecs_instance_role.name
}

resource "aws_iam_role" "ecs_instance_role" {
  name               = "${var.name}-ecs-instance-role"
  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
  role       = aws_iam_role.ecs_instance_role.name
}

