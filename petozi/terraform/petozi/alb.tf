resource "aws_alb" "alb" {
  name               = "${var.name}-alb"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [var.security_group, aws_security_group.lb.id]
  subnets            = split(",", var.subnets)

  enable_deletion_protection = true

  tags = {
    Environment = var.environment
  }
}

resource "aws_security_group" "lb" {
  name        = "${var.name}-alb"
  description = "controls access to the ALB"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = "443"
    to_port     = "443"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_alb_target_group" "ssl_target" {
  name                 = "${var.name}-ssl-target"
  target_type          = "ip"
  port                 = 1
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  deregistration_delay = 20

  health_check {
    path = "/ping"
  }
}

resource "aws_alb_listener" "ssl_listener" {
  load_balancer_arn = aws_alb.alb.arn
  protocol          = "HTTPS"
  port              = 443
  certificate_arn   = aws_acm_certificate.ssl_cert.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.ssl_target.arn
  }
}

resource "aws_alb_listener_certificate" "alb_listener_cert" {
  certificate_arn = aws_acm_certificate.ssl_cert.arn
  listener_arn    = aws_alb_listener.ssl_listener.arn
}

resource "aws_alb_listener_certificate" "alb_listener_cert_a2z" {
  certificate_arn = module.privatelink-cert.arn
  listener_arn    = aws_alb_listener.ssl_listener.arn
}

resource "aws_alb_listener_rule" "alb_listener_rule" {
  listener_arn = aws_alb_listener.ssl_listener.arn
  condition {
    path_pattern {
      values = ["/*"]
    }
  }
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.ssl_target.arn
  }
}

