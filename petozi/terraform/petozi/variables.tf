variable "environment" {
  type        = string
  description = "e.g., production/staging"
}

variable "environment_short" {
  type        = string
  description = "e.g., prod/staging"
}

variable "commit" {
  type        = string
  description = "git sha commit of the service"
}

variable "security_group" {
  type        = string
  description = "Security group to attach to the ECS services"
}

variable "region" {
  type        = string
  description = "AWS region (e.g., us-west-2)"
}

variable "vpc_id" {
  type        = string
  description = "Private network to create resources in"
}

variable "name" {
  type        = string
  default     = "Destiny"
  description = "Optional namespace to group resources with"
}

variable "account_id" {
  description = "the account ID used for auto filling in some roles"
}

variable "subnets" {
  type = string
}

variable "min_count" {
  default     = 3
  description = "Minimum number of Chronobreak containers"
}

variable "max_count" {
  default     = 300
  description = "Maximum number of Chronobreak containers"
}

variable "bits_types_min_read" {
  description = "Minimum read capacity for bits types dynamo table"
}

variable "bits_types_min_write" {
  description = "Minimum write capacity for bits types dynamo table"
}

variable "bits_products_min_read" {
  description = "Minimum read capacity for bits products dynamo table"
}

variable "bits_products_min_write" {
  description = "Minimum write capacity for bits products dynamo table"
}

variable "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
}

variable "redis_instance_type" {
  description = "The redis instance type"
}

variable "cpu" {
  default     = "256"
  description = "The CPU value for Fargate Task"
}

variable "memory" {
  default     = "512"
  description = "The Memory value for Fargate Task"
}

