# sqs to store SNS topic publishes
module "dynamo_export_glue_error_queue" {
  source                = "./modules/sqs"
  queue_name            = aws_sns_topic.db_export_glue_errors.name
  send_allowed_resource = [aws_sns_topic.db_export_glue_errors.arn]
}

resource "aws_sns_topic_subscription" "db_export_glue_errors" {
  topic_arn = aws_sns_topic.db_export_glue_errors.arn
  protocol  = "sqs"
  endpoint  = module.dynamo_export_glue_error_queue.queue_arn
}

resource "aws_sqs_queue_policy" "db_export_glue_errors" {
  queue_url = module.dynamo_export_glue_error_queue.queue_url
  policy    = data.aws_iam_policy_document.db_export_glue_errors.json
}

data "aws_iam_policy_document" "db_export_glue_errors" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    actions = [
      "SQS:SendMessage"
    ]
    resources = [module.dynamo_export_glue_error_queue.queue_arn]
    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values = [
        aws_sns_topic.db_export_glue_errors.arn
      ]
    }
  }
}
