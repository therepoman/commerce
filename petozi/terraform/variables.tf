variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "aws_profile" {}

variable "vpc_id" {}

variable "region" {}

variable "security_group" {}

variable "environment" {}

variable "subnets" {}

provider "aws" {
  version    = "2.44"
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
}
