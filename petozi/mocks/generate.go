package mocks

// Bits Types Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/bits_types -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types -outpkg=bits_types_mock

// Bits Types Creator
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Creator -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/bits_types -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types -outpkg=bits_types_mock

// Bits Types Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/bits_types -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types -outpkg=bits_types_mock

// Bits Types DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/dynamo/bits_types -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_types -outpkg=bits_types_mock

// Bits Products DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/dynamo/bits_products -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_products -outpkg=bits_products_mock

// Bits Types Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/cache/bits_types -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/cache/bits_types -outpkg=bits_types_mock

// Bits Products Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/cache/bits_products -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/cache/bits_products -outpkg=bits_products_mock

// Bits Products Creator
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Creator -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/bits_products -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products -outpkg=bits_products_mock

// Bits Products Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/bits_products -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products -outpkg=bits_products_mock

// Bits Products Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/bits_products -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products -outpkg=bits_products_mock

// Get Bits Product Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Validation -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/api/get_bits_product -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/api/get_bits_product -outpkg=get_bits_product_mock

// Put Bits Product Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Validation -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/api/put_bits_product -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/api/put_bits_product -outpkg=put_bits_product_mock

// Put Bits Type Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Validation -dir=$GOPATH/src/code.justin.tv/commerce/petozi/backend/api/put_bits_type -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/api/put_bits_type -outpkg=put_bits_type_mock

// Redis Cmdable
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Cmdable -dir=$GOPATH/src/code.justin.tv/commerce/petozi/vendor/github.com/go-redis/redis -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/github.com/go-redis/redis -outpkg=redis_mock

// Statsd Statter
//go:generate $GOPATH/src/code.justin.tv/commerce/petozi/_tools/bin/retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/petozi/vendor/github.com/cactus/go-statsd-client/statsd -output=$GOPATH/src/code.justin.tv/commerce/petozi/mocks/github.com/cactus/go-statsd-client/statsd -outpkg=statsd_mock
