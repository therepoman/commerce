package mws

import (
	"context"
	"strconv"
	"sync"
	"time"

	"sort"
	"strings"

	"code.justin.tv/amzn/TwitchLogging"
	"code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/video/mwsclient"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"log"
)

const (
	// NoInfo represents metric report/metric data fields that are required but empty
	NoInfo = ""
	// Maximum number of retries should be 3
	maxRetries = 3
	// Timeout in seconds for requests to MWS (after timeoutSeconds, the request will be cancelled)
	timeoutSeconds = 5
)

// MWSClientAPI represents the interface for calling the MWS PutMetricDataForAggregation endpoint
type MWSClientAPI interface {
	PutMetricDataForAggregation(context.Context, *mwsclient.PutMetricDataForAggregationInput, ...request.Option) (*mwsclient.PutMetricDataForAggregationOutput, error)
}

// Buffered represents a buffered MWS sender (commonly used with a Telemetry.SampleObserver)
type Buffered struct {
	*CommonSender
	aggregator *telemetry.Aggregator
	stopSignal chan bool
	stopWait   sync.WaitGroup
}

// Unbuffered represents an unbuffered MWS sender (commonly used with a Telemetry.BufferedAggregator)
type Unbuffered struct {
	*CommonSender
}

// CommonSender contains the common struct components and functions for MWS senders
type CommonSender struct {
	client            MWSClientAPI
	processIdentifier *identifier.ProcessIdentifier
	logger            logging.Logger
}

// New returns a new standalone MWS client that performs its own flushing
func New(processIdentifier *identifier.ProcessIdentifier, logger logging.Logger) telemetry.SampleObserver {
	sess := createNewSession(processIdentifier.Region)

	common := &CommonSender{
		client:            mwsclient.New(sess),
		processIdentifier: processIdentifier,
		logger:            logger,
	}
	mws := &Buffered{
		CommonSender: common,
		aggregator:   telemetry.NewAggregator(time.Minute),
		stopSignal:   make(chan bool),
	}

	mws.run()
	return mws
}

// sleeps for 30 seconds at a time, then flushes aggregated sample data to MWS
func (mwsBuff *Buffered) run() {
	// Add a counter to the waitgroup. Done() will be called on the waitgroup when the MWS
	// client is asked to stop
	mwsBuff.stopWait.Add(1)
	go func() {
		ticker := time.NewTicker(30 * time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				mwsBuff.Flush()
			case <-mwsBuff.stopSignal:
				mwsBuff.Flush()
				mwsBuff.stopWait.Done()
				return
			}
		}
	}()
}

// Flush synchronously flushes current data to MWS
func (mwsBuff *Buffered) Flush() {
	// Pull the distributions from the sample aggregator
	if mwsBuff.aggregator != nil {
		distributions := mwsBuff.aggregator.Flush()
		mwsBuff.commonFlush(distributions)
	}
}

// Stop terminates the wait and flush loop that continuously sends metrics to MWS on a 30 second interval
// This is a synchronous operation, and current metrics will be flushed before this method returns
func (mwsBuff *Buffered) Stop() {
	// send into the channel to initiate the stop
	mwsBuff.stopSignal <- true
	mwsBuff.stopWait.Wait()
}

// ObserveSample takes a sample and aggregates it into the set of data to send to MWS
func (mwsBuff *Buffered) ObserveSample(sample *telemetry.Sample) {
	// Ingest the sample into the Aggregator
	if mwsBuff.aggregator != nil {
		mwsBuff.aggregator.AggregateSample(sample)
	}
}

// NewUnbuffered returns a new MWS client that needs to be run within a separate BufferedAggregator as it does not
// main a buffer/ any aggregation or flushing logic on its own
func NewUnbuffered(processIdentifier *identifier.ProcessIdentifier, logger logging.Logger) telemetry.SampleUnbufferedObserver {
	sess := createNewSession(processIdentifier.Region)
	common := &CommonSender{
		client:            mwsclient.New(sess),
		processIdentifier: processIdentifier,
		logger:            logger,
	}
	return &Unbuffered{
		CommonSender: common,
	}
}

// FlushWithoutBuffering flushes without buffering the flush
func (mwsUnbuff *Unbuffered) FlushWithoutBuffering(distributions []*telemetry.Distribution) {
	mwsUnbuff.commonFlush(distributions)
}

// commonFlush is a helper function to runs the same functionality for both buffered and non-buffered flushes
func (mws *CommonSender) commonFlush(distributions []*telemetry.Distribution) {
	// Convert the collected and aggregated samples into a usable format
	mwsMetricReport := mws.constructMetricReport(distributions)
	// Send the metrics to the MWS endpoint
	mws.sendMetrics(mwsMetricReport)
}

// sendMetrics sends MWS (PMET) metric data to the MWS endpoint
func (mws *CommonSender) sendMetrics(metricReports []*mwsclient.MetricReport) {
	if metricReports == nil {
		return
	}

	// We don't care about paginating at this time, since request size can be 10 million bytes and unlimited metrics
	// See https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation
	input := mwsclient.PutMetricDataForAggregationInput{
		MetricReports: metricReports,
	}

	// Set a timeout for the MWS call
	ctx, cancel := context.WithTimeout(context.Background(), timeoutSeconds*time.Second)
	defer cancel()
	_, err := mws.client.PutMetricDataForAggregation(ctx, &input)
	if err != nil {
		if mws.logger != nil {
			mws.logger.Log("Failed to publish metrics", "err", err.Error())
		} else {
			log.Printf("Failed to publish metrics: %v", err)
		}
	}
}

// constructMetricReport constructs a metric report from an internal collection
func (mws *CommonSender) constructMetricReport(distributions []*telemetry.Distribution) []*mwsclient.MetricReport {
	if len(distributions) == 0 {
		return nil
	}

	// Create a new set of metric reports
	metricReports := []*mwsclient.MetricReport{}
	// For now, just put everything in a single metric report
	report := newMetricReport(NoInfo, NoInfo, mws.processIdentifier.Service, mws.processIdentifier.Stage)

	// Construct all metrics
	allDatum := []*mwsclient.Metric{}
	for _, dist := range distributions {
		newDatums := mws.constructNewDatums(dist)
		if len(newDatums) == 0 {
			continue
		}
		// Append the list of mws datums created by the telemetry datum to the overall list of mws datums
		allDatum = append(allDatum, newDatums...)
	}

	// Add all metrics to the same metric report, and add the metric report to the list of metric reports (only 1 needed)
	report.Metrics = append(report.Metrics, allDatum...)
	metricReports = append(metricReports, &report)
	return metricReports
}

// constructNewDatums constructs a new MWS/PEMT metric datum from an internal distribution
func (mws *CommonSender) constructNewDatums(distribution *telemetry.Distribution) []*mwsclient.Metric {
	// Ensure there's a metric name
	if distribution.MetricID.Name == "" {
		return nil
	}
	// A single telemetry datum has all related dimensions. These need to be broken into separate MWS
	// datums to allow for rollups (since MWS does not do this automatically)
	// This means n dimensions in the dimension list for one internal metric = n separate CW datums
	mwsDatums := []*mwsclient.Metric{}

	// Create the single MWS datum that will serve as the base for every other datum
	baseDatum := newSingleDefaultMetric(distribution.MetricID.Name)
	baseDatum.Timestamp = distribution.Timestamp.UTC()
	baseDatum.Unit = distribution.Unit

	// convert the internal telemetry.Distribution into the MWS distribution format (mostly the same except MWS format marshals properly to JSON)
	mwsDistribution := mwsclient.Distribution{
		SampleCount: int(distribution.SampleCount),
		Minimum:     distribution.Minimum,
		Maximum:     distribution.Maximum,
		Sum:         distribution.Sum,
	}

	// Construct the histogram (since MWS requires strings as keys) if possible
	histogram := make(map[string]int)
	seh1 := distribution.SEH1
	for k, v := range seh1.Histogram {
		// MWS requires a string -> int mapping
		histogram[strconv.Itoa(int(k))] = int(v)
	}
	// Add the final "Zeros" bucket
	if seh1.ZeroBucket > 0 {
		histogram[ZeroBucket] = seh1.ZeroBucket
	}
	mwsDistribution.SEH1 = histogram

	// Set the distribution
	baseDatum.Distribution = &mwsDistribution

	// Samples have dimensions and rollups
	// Start by determining the base dimensions (this will be used for a single datum)
	defaultDims := mws.constructDimensions(distribution.MetricID.Dimensions)
	// Use the baseDatum to construct a new datum
	defaultMetric := baseDatum
	defaultMetric.Dimensions = &defaultDims
	mwsDatums = append(mwsDatums, &defaultMetric)

	// Construct the appropriate roll ups
	for _, rollupDims := range distribution.RollupDimensions {
		rollupDim := mws.createRollupDimensions(distribution.MetricID.Dimensions, rollupDims)
		rollupMetric := baseDatum
		rollupMetric.Dimensions = &rollupDim
		mwsDatums = append(mwsDatums, &rollupMetric)
	}

	return mwsDatums
}

// createRollupDimensions takes a map of dimensions and rollupDimensions and constructs the appropriate MWS dimensions
func (mws *CommonSender) createRollupDimensions(allDims telemetry.DimensionSet, rollups []string) mwsclient.ServiceSchemaDimensions {
	// Copy the original map
	dims := make(telemetry.DimensionSet)
	for name, value := range allDims {
		dims[name] = value
	}

	// Replace all the rollup dimensions from the original dimension map with 'ALL'. This is used to make it clear
	// that the dimension was rolled up to (as opposed to simply missing)
	for _, name := range rollups {
		dims[name] = RollupValue
	}
	// Convert the updated dimension map into MWS dimensions
	return mws.constructDimensions(dims)
}

// constructDimensions takes an internal list of dimensions and converts it into a list of MWS dimensions
func (mws *CommonSender) constructDimensions(dimensions telemetry.DimensionSet) mwsclient.ServiceSchemaDimensions {
	mwsDims := defaultMetricDimensions

	// Custom dimensions is made as multiple concatenations
	customDimList := []string{}

	// Map dimensions to their PMET counterparts
	for name, value := range dimensions {
		// Ensure the dimensions are valid (require both a name and a value)
		if (name == "") || (value == "") {
			continue
		}

		// See common dimension cardinality wiki for common definitions
		// https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality
		switch name {
		case telemetry.DimensionService:
			mwsDims.ServiceName = value
		case telemetry.DimensionStage:
			mwsDims.DataSet = value
		case telemetry.DimensionSubstage:
			mwsDims.HostGroup = value
		case telemetry.DimensionRegion:
			mwsDims.Marketplace = value
		case telemetry.DimensionOperation:
			mwsDims.MethodName = value
		case telemetry.DimensionDependency:
			// Create a concatenation to make it clear we're dealing with a dependency
			mwsDims.MetricClass = telemetry.DimensionDependency + ":" + value
		case telemetry.DimensionProcessAddress:
			mwsDims.Host = value
		default:
			// TODO: Add Client when TwitchTelemetry has these dimensions
			// Combine all custom dimensions into a single list
			customDimList = append(customDimList, name+":"+value)
		}
	}

	// If we have any custom dimensions...
	if len(customDimList) > 0 {
		// Sort the dimensions and concatenation the strings
		sort.Strings(customDimList)
		mwsDims.Instance = strings.Join(customDimList, "_")
	}

	// Verify that the two required dimensions are there, and do a last ditch effort to fix
	if mwsDims.Marketplace == "" {
		mwsDims.Marketplace = mws.processIdentifier.Region
	}
	if mwsDims.ServiceName == "" {
		mwsDims.ServiceName = mws.processIdentifier.Service
	}
	return mwsDims
}

// createNewSession construct a new AWS session (required for signing for clients like MWS)
func createNewSession(region string) *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:     aws.String(region),
			MaxRetries: aws.Int(maxRetries),
		},
	}))
}
