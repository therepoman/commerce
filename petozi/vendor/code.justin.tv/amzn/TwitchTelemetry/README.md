# `TwitchTelemetry`

This package represents the core of the TwitchTelemetry packages. It defines what samples actually are, as well as 
standardizing dimension and metric names. It additionally provides an interface for observing metrics and aggregation
functionality (including the main definition of SEH1 histograms).

We strongly recommend reading the [Fulton Metrics Doc](https://docs.fulton.twitch.a2z.com/docs/metrics.html) for more
information on this package and for diagrams on how it relates to the other TwitchTelemetry packages.

## Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchTelemetry`

## Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel