package ttlcache

import (
	"runtime"
	"sync"
	"time"
)

type Cache interface {
	Get(k string) interface{}
	GetAll() map[string]interface{}
	Len() int
	Set(k string, v interface{}, ttl time.Duration)
	Delete(k string)
}

type item struct {
	value    interface{}
	expireAt time.Time
}

type cache struct {
	items           sync.Map
	stopChan        chan bool
	cleanupInterval time.Duration
}

func NewCache(cleanupInterval time.Duration) Cache {
	c := &cache{
		items:           sync.Map{},
		stopChan:        make(chan bool),
		cleanupInterval: cleanupInterval,
	}
	c.runAsyncJanitor()
	runtime.SetFinalizer(c, func(c *cache) {
		c.stopChan <- true
	})
	return c
}

func (c *cache) runAsyncJanitor() {
	go func(ca *cache) {
		ticker := time.Tick(ca.cleanupInterval)
		for {
			select {
			case _ = <-ticker:
				ca.cleanup()
			case _ = <-ca.stopChan:
				break
			}
		}
	}(c)
}

func (c *cache) cleanup() {
	now := time.Now()
	c.items.Range(func(k, v interface{}) bool {
		cached, ok := c.items.Load(k)
		if !ok {
			return true
		}

		i, ok := cached.(item)
		if !ok {
			c.items.Delete(k)
			return true
		}

		if now.After(i.expireAt) {
			c.items.Delete(k)
		}

		return true
	})
}

func (c *cache) Len() int {
	count := 0
	c.items.Range(func(k, v interface{}) bool {
		count++
		return true
	})
	return count
}

func (c *cache) Set(k string, v interface{}, ttl time.Duration) {
	c.items.Store(k, item{
		value:    v,
		expireAt: time.Now().Add(ttl),
	})
}

func (c *cache) Get(k string) interface{} {
	cached, ok := c.items.Load(k)
	if !ok {
		return nil
	}

	i, ok := cached.(item)
	if !ok {
		return nil
	}

	return i.value
}

func (c *cache) GetAll() map[string]interface{} {
	items := make(map[string]interface{})

	c.items.Range(func(k, v interface{}) bool {
		key, ok := k.(string)
		if !ok {
			return true
		}

		i, ok := v.(item)
		if !ok {
			return true
		}

		items[key] = i.value
		return true
	})
	return items
}

func (c *cache) Delete(k string) {
	c.items.Delete(k)
}
