package splatter

import "strings"

const (
	separator = "."
)

// joinPathComponents is a helper that ensures we combine path components with a dot
// when it's appropriate to do so; prefix is the existing prefix and suffix is
// the new component being added.
//
// It returns the joined prefix.
func joinPathComponents(prefix, suffix string) string {
	suffix = strings.TrimLeft(suffix, separator)
	if prefix != "" && suffix != "" {
		pathFields := []string{prefix, suffix}
		return strings.Join(pathFields, separator)
	}
	return prefix + suffix
}
