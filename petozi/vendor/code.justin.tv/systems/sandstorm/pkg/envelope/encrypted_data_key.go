package envelope

// EncryptedDataKey ...
type EncryptedDataKey struct {
	KeyARN string
	Region string
	Value  []byte
}
