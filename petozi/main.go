package main

import (
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend"
	"code.justin.tv/commerce/petozi/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

var s2sEnabledOperations = map[string]bool{
	"AddTokens":     false,
	"GetTokens":     false,
	"ConsumeTokens": false,
	"HealthCheck":   false,
}

func getEnv() config.Environment {
	env := os.Getenv(config.EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	return config.GetOrDefaultEnvironment(env)
}

func main() {
	env := getEnv()

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.Environment.Name)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.Statter()))
	twirpHandler := petozi.NewPetoziServer(be.TwirpServer(), twirpStatsHook)
	defer func() {
		err := be.Statter().Close()
		if err != nil {
			log.WithError(err).Error("could not close statter")
		}
	}()

	var s2sTwirpHandler http.Handler
	if cfg.Auth != nil && cfg.Auth.S2S != nil && cfg.Auth.S2S.IsEnabled {
		eventsWriterClient, err := events.NewEventLogger(events.Config{
			Environment:     "production",
			BufferSizeLimit: 0,
		}, log.New())
		if err != nil {
			log.WithError(err).Panic("Error initializing event logger for S2S")
		}
		s2sCalleeClient := &callee.Client{
			ServiceName:        cfg.Auth.S2S.ServiceName,
			Logger:             log.New(),
			EventsWriterClient: eventsWriterClient,
		}
		err = s2sCalleeClient.Start()
		if err != nil {
			log.WithError(err).Panic("Error initializing S2S client")
		}
		s2sTwirpHandler = s2sCalleeClient.RequestValidatorMiddleware(twirpHandler)
	} else {
		s2sTwirpHandler = twirpHandler
	}

	mux := goji.NewMux()

	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(petozi.PetoziPathPrefix+"*"), selectiveS2SHandler(twirpHandler, s2sTwirpHandler))

	goji_graceful.HandleSignals()
	err = goji_graceful.ListenAndServe(":8080", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")
}

func selectiveS2SHandler(nonS2SHandler http.Handler, s2sHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if shouldUseS2S(r) {
			s2sHandler.ServeHTTP(w, r)
		} else {
			nonS2SHandler.ServeHTTP(w, r)
		}
	})
}

func shouldUseS2S(r *http.Request) bool {
	if r == nil {
		return false
	}
	if r.URL == nil {
		return false
	}
	path := r.URL.Path
	operation := strings.TrimPrefix(path, petozi.PetoziPathPrefix)

	enabled, isPresent := s2sEnabledOperations[operation]
	return isPresent && enabled
}
