package backend

import (
	"errors"
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_product"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_products"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_type"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_types"
	"code.justin.tv/commerce/petozi/backend/api/put_bits_product"
	"code.justin.tv/commerce/petozi/backend/api/put_bits_type"
	"code.justin.tv/commerce/petozi/backend/api/reset_caches"
	"code.justin.tv/commerce/petozi/backend/api/validation/promo"
	"code.justin.tv/commerce/petozi/backend/bits_products"
	"code.justin.tv/commerce/petozi/backend/bits_types"
	products_cache "code.justin.tv/commerce/petozi/backend/cache/bits_products"
	bits_types_cache "code.justin.tv/commerce/petozi/backend/cache/bits_types"
	products_dao "code.justin.tv/commerce/petozi/backend/dynamo/bits_products"
	bits_types_dao "code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	"code.justin.tv/commerce/petozi/backend/server"
	"code.justin.tv/commerce/petozi/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"code.justin.tv/commerce/splatter"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/go-redis/redis"
)

const (
	localhostServerEndpoint = "http://localhost:8080"
)

type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() petozi.Petozi
	Statter() statsd.Statter
}

type backend struct {
	Server petozi.Petozi `inject:""`

	Config *config.Config `inject:""`

	Stats statsd.Statter `inject:""`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetrics != nil {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.Environment.AWSRegion,
			ServiceName:       "petozi",
			Stage:             cfg.Environment.Name,
			Substage:          "primary",
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.Environment.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	var redisClient redis.Cmdable = &redis.Client{}
	if cfg.Redis != nil && !strings.Blank(cfg.Redis.Endpoint) {
		redisClient = redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{
				cfg.Redis.Endpoint,
			},
			MaxRedirects: 3,
			ReadOnly:     true,
		})
	}

	deps := []*inject.Object{
		// Backend (graph root)
		{Value: b},

		// Config
		{Value: cfg},

		// Stats
		{Value: stats},

		// Server
		{Value: server.NewServer()},

		// Cache
		{Value: redisClient},
		{Value: products_cache.NewCache()},
		{Value: bits_types_cache.NewCache()},

		// DAOs
		{Value: bits_types_dao.NewDAO(sess)},
		{Value: products_dao.NewDAO(sess)},

		// API
		{Value: get_bits_product.NewAPI()},
		{Value: get_bits_products.NewAPI()},
		{Value: put_bits_product.NewAPI()},
		{Value: get_bits_type.NewAPI()},
		{Value: get_bits_types.NewAPI()},
		{Value: put_bits_type.NewAPI()},
		{Value: reset_caches.NewAPI()},

		// API Validation
		{Value: put_bits_product.NewValidation()},
		{Value: get_bits_product.NewValidation()},
		{Value: promo.NewValidation()},
		{Value: get_bits_type.NewValidation()},
		{Value: put_bits_type.NewValidation()},

		// Bits Products
		{Value: bits_products.NewCreator()},
		{Value: bits_products.NewFetcher()},
		{Value: bits_products.NewGetter()},

		// Bits Types
		{Value: bits_types.NewFetcher()},
		{Value: bits_types.NewGetter()},
		{Value: bits_types.NewCreator()},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (b *backend) Ping(w http.ResponseWriter, r *http.Request) {
	petoziClient := petozi.NewPetoziProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err := petoziClient.HealthCheck(r.Context(), &petozi.HealthCheckReq{})
	if err != nil {
		log.WithError(err).Error("Could not ping petozi server")
		w.WriteHeader(http.StatusInternalServerError)
		_, innerErr := io.WriteString(w, err.Error())
		if innerErr != nil {
			log.WithError(innerErr).Error("Error writing http response")
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func (b *backend) TwirpServer() petozi.Petozi {
	return b.Server
}

func (b *backend) Statter() statsd.Statter {
	return b.Stats
}
