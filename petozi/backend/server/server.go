package server

import (
	"context"

	"code.justin.tv/commerce/petozi/backend/api/get_bits_product"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_products"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_type"
	"code.justin.tv/commerce/petozi/backend/api/get_bits_types"
	"code.justin.tv/commerce/petozi/backend/api/put_bits_product"
	"code.justin.tv/commerce/petozi/backend/api/put_bits_type"
	"code.justin.tv/commerce/petozi/backend/api/reset_caches"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type server struct {
	GetBitsProductAPI  get_bits_product.API  `inject:""`
	GetBitsProductsAPI get_bits_products.API `inject:""`
	PutBitsProductAPI  put_bits_product.API  `inject:""`

	GetBitsTypeAPI  get_bits_type.API  `inject:""`
	GetBitsTypesAPI get_bits_types.API `inject:""`
	PutBitsTypeAPI  put_bits_type.API  `inject:""`

	ResetCachesAPI reset_caches.API `inject:""`
}

func NewServer() petozi.Petozi {
	return &server{}
}

func (s *server) HealthCheck(ctx context.Context, req *petozi.HealthCheckReq) (*petozi.HealthCheckResp, error) {
	return &petozi.HealthCheckResp{}, nil
}

func (s *server) PutBitsProduct(ctx context.Context, req *petozi.PutBitsProductReq) (*petozi.PutBitsProductResp, error) {
	return s.PutBitsProductAPI.Put(ctx, req)
}

func (s *server) GetBitsProduct(ctx context.Context, req *petozi.GetBitsProductReq) (*petozi.GetBitsProductResp, error) {
	return s.GetBitsProductAPI.Get(ctx, req)
}

func (s *server) GetBitsProducts(ctx context.Context, req *petozi.GetBitsProductsReq) (*petozi.GetBitsProductsResp, error) {
	return s.GetBitsProductsAPI.GetAll(ctx, req)
}

func (s *server) PutBitsType(ctx context.Context, req *petozi.PutBitsTypeReq) (*petozi.PutBitsTypeResp, error) {
	return s.PutBitsTypeAPI.Put(ctx, req)
}

func (s *server) GetBitsType(ctx context.Context, req *petozi.GetBitsTypeReq) (*petozi.GetBitsTypeResp, error) {
	return s.GetBitsTypeAPI.Get(ctx, req)
}

func (s *server) GetBitsTypes(ctx context.Context, req *petozi.GetBitsTypesReq) (*petozi.GetBitsTypesResp, error) {
	return s.GetBitsTypesAPI.GetAll(ctx, req)
}

func (s *server) ResetCaches(ctx context.Context, req *petozi.ResetCachesReq) (*petozi.ResetCachesResp, error) {
	return s.ResetCachesAPI.Reset(ctx, req)
}
