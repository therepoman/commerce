package bits_types

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	redis_mock "code.justin.tv/commerce/petozi/mocks/github.com/go-redis/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_Get(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)

		c := &cache{
			Redis: redisClient,
		}

		id := "walrusBits"

		Convey("when the redis client errors", func() {
			resp := redis.NewStringResult("", errors.New("ERROR"))
			redisClient.On("HGet", bitsTypesCacheKey, id).Return(resp)
			result := c.Get(id)

			So(result, ShouldBeNil)
		})

		Convey("when the redis client returns a blank value", func() {
			resp := redis.NewStringResult("", nil)
			redisClient.On("HGet", bitsTypesCacheKey, id).Return(resp)
			result := c.Get(id)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			resp := redis.NewStringResult("this is invalid json &*!#(", nil)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("HGet", bitsTypesCacheKey, id).Return(resp)
			redisClient.On("HDel", bitsTypesCacheKey, id).Return(delResp)

			result := c.Get(id)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", bitsTypesCacheKey, id)
			})
		})

		Convey("we should return bits products", func() {
			cachedValue := &dynamo.BitsType{
				ID: id,
			}
			cachedBytes, err := json.Marshal(cachedValue)

			if err != nil {
				logrus.WithError(err).Error("Error marshaling bits products")
				t.Fail()
			}

			resp := redis.NewStringResult(string(cachedBytes), nil)
			redisClient.On("HGet", bitsTypesCacheKey, id).Return(resp)

			returnValue := c.Get(id)

			So(returnValue, ShouldResemble, cachedValue)
		})
	})
}

func TestCache_GetAll(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)

		c := &cache{
			Redis: redisClient,
		}

		id := "walrusBits"

		Convey("when the redis client errors", func() {
			resp := redis.NewStringStringMapResult(nil, errors.New("ERROR"))
			redisClient.On("HGetAll", bitsTypesCacheKey).Return(resp)
			result := c.GetAll()

			So(result, ShouldHaveLength, 0)
		})

		Convey("when the redis client returns a blank value", func() {
			resultMap := map[string]string{
				id: "",
			}
			resp := redis.NewStringStringMapResult(resultMap, nil)
			redisClient.On("HGetAll", bitsTypesCacheKey).Return(resp)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("HDel", bitsTypesCacheKey, id).Return(delResp)
			result := c.GetAll()

			Convey("we should return nil", func() {
				So(result, ShouldHaveLength, 0)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", bitsTypesCacheKey, id)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			resultMap := map[string]string{
				id: "this is invalid json &*!#(",
			}
			resp := redis.NewStringStringMapResult(resultMap, nil)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("HGetAll", bitsTypesCacheKey).Return(resp)
			redisClient.On("HDel", bitsTypesCacheKey, id).Return(delResp)

			result := c.GetAll()

			Convey("we should return nil", func() {
				So(result, ShouldHaveLength, 0)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", bitsTypesCacheKey, id)
			})
		})

		Convey("we should return bits products", func() {
			cachedValue := &dynamo.BitsType{
				ID: id,
			}
			cachedBytes, err := json.Marshal(cachedValue)

			if err != nil {
				logrus.WithError(err).Error("Error marshaling bits products")
				t.Fail()
			}

			resultMap := map[string]string{
				id: string(cachedBytes),
			}
			resp := redis.NewStringStringMapResult(resultMap, nil)
			redisClient.On("HGetAll", bitsTypesCacheKey).Return(resp)

			returnValue := c.GetAll()

			So(returnValue, ShouldHaveLength, 1)
			So(returnValue[0], ShouldResemble, cachedValue)
		})
	})
}

func TestCache_Put(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)

		c := &cache{
			Redis: redisClient,
		}

		id := "walrusBits"

		bitsType := &dynamo.BitsType{
			ID: id,
		}

		Convey("when the redis client returns an error", func() {
			Convey("we should return nothing", func() {
				cachedBytes, err := json.Marshal(bitsType)

				if err != nil {
					logrus.WithError(err).Error("Error marshaling settings")
					t.Fail()
				}

				redisClient.On("HSet", bitsTypesCacheKey, id, string(cachedBytes)).Return(redis.NewBoolCmd(errors.New("ERROR")))

				c.Put(bitsType)

				redisClient.AssertCalled(t, "HSet", bitsTypesCacheKey, id, string(cachedBytes))
			})
		})

		Convey("when the redis client does not return an error", func() {
			Convey("we should return nothing", func() {
				cachedBytes, err := json.Marshal(bitsType)

				if err != nil {
					logrus.WithError(err).Error("Error marshaling settings")
					t.Fail()
				}

				redisClient.On("HSet", bitsTypesCacheKey, id, string(cachedBytes)).Return(redis.NewBoolCmd(nil))

				c.Put(bitsType)

				redisClient.AssertCalled(t, "HSet", bitsTypesCacheKey, id, string(cachedBytes))
			})
		})
	})
}

func TestCache_PutAll(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)

		c := &cache{
			Redis: redisClient,
		}

		id := "walrusBits"

		bitsType := &dynamo.BitsType{
			ID: id,
		}

		cachedBytes, err := json.Marshal(bitsType)

		if err != nil {
			logrus.WithError(err).Error("Error marshaling settings")
			t.Fail()
		}

		putMap := map[string]interface{}{
			id: string(cachedBytes),
		}

		Convey("when the redis client returns an error", func() {
			Convey("we should return nothing", func() {
				redisClient.On("HMSet", bitsTypesCacheKey, putMap).Return(redis.NewStatusCmd(errors.New("ERROR")))

				c.PutAll([]*dynamo.BitsType{bitsType})

				redisClient.AssertCalled(t, "HMSet", bitsTypesCacheKey, putMap)
			})
		})

		Convey("when the redis client does not return an error", func() {
			Convey("we should return nothing", func() {
				redisClient.On("HMSet", bitsTypesCacheKey, putMap).Return(redis.NewStatusCmd(nil))

				c.PutAll([]*dynamo.BitsType{bitsType})

				redisClient.AssertCalled(t, "HMSet", bitsTypesCacheKey, putMap)
			})
		})
	})
}
