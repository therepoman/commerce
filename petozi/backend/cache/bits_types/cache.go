package bits_types

import (
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	"github.com/go-redis/redis"
)

const (
	bitsTypesCacheKey = "bits-types"
)

type Cache interface {
	Get(id string) *dynamo.BitsType
	GetAll() []*dynamo.BitsType
	Put(bitsType *dynamo.BitsType)
	PutAll(bitsTypes []*dynamo.BitsType)
	Blow()
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	Redis redis.Cmdable `inject:""`
}

func (c *cache) Get(id string) *dynamo.BitsType {
	cachedProduct, err := c.Redis.HGet(bitsTypesCacheKey, id).Result()

	if err != nil && err != redis.Nil {
		logrus.WithError(err).Error("could not fetch bitsType from cache")
		return nil
	}

	if strings.Blank(cachedProduct) {
		return nil
	}

	var bitsType dynamo.BitsType
	err = json.Unmarshal([]byte(cachedProduct), &bitsType)
	if err != nil {
		logrus.WithError(err).Error("could not unmarshal bitsType entry from cache")
		c.Redis.HDel(bitsTypesCacheKey, id)
		return nil
	}

	return &bitsType
}

func (c *cache) GetAll() []*dynamo.BitsType {
	bitsTypes := make([]*dynamo.BitsType, 0)

	cachedTypes, err := c.Redis.HGetAll(bitsTypesCacheKey).Result()
	if err != nil && err != redis.Nil {
		logrus.WithError(err).Error("could not fetch all bitsTypes from cache")
		return bitsTypes
	}

	for key, value := range cachedTypes {
		if strings.Blank(value) {
			c.Redis.HDel(bitsTypesCacheKey, key)
			continue
		}

		var bitsType dynamo.BitsType
		err = json.Unmarshal([]byte(value), &bitsType)
		if err != nil {
			logrus.WithError(err).Error("could not unmarshal bitsType entry from cache")
			c.Redis.HDel(bitsTypesCacheKey, key)
			continue
		}

		bitsTypes = append(bitsTypes, &bitsType)
	}

	return bitsTypes
}

func (c *cache) Put(bitsType *dynamo.BitsType) {
	out, err := json.Marshal(&bitsType)
	if err != nil {
		logrus.WithError(err).Error("could not marshal bitsTypes entry for cache")
	}

	err = c.Redis.HSet(bitsTypesCacheKey, bitsType.ID, string(out)).Err()
	if err != nil {
		logrus.WithError(err).Error("could not set bitsTypes entry in cache")
	}
}

func (c *cache) PutAll(bitsTypes []*dynamo.BitsType) {
	valuesToPut := map[string]interface{}{}
	for _, bitsType := range bitsTypes {
		out, err := json.Marshal(&bitsType)
		if err != nil {
			logrus.WithError(err).Error("could not marshal bitsTypes entry for cache")
		}

		valuesToPut[bitsType.ID] = string(out)
	}

	err := c.Redis.HMSet(bitsTypesCacheKey, valuesToPut).Err()
	if err != nil {
		logrus.WithError(err).Error("could not set bitsTypes entry in cache")
	}
}

func (c *cache) Blow() {
	if err := c.Redis.Del(bitsTypesCacheKey).Err(); err != nil && err != redis.Nil {
		logrus.WithError(err).Error("could not blow bits types cache")
	}
}
