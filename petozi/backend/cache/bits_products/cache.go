package bits_products

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-redis/redis"
)

const (
	bitsProductsCacheKey = "bits-products"
	bitsProductsHashKey  = "%s.%s"
)

type Cache interface {
	Get(platform petozi.Platform, productID string) *dynamo.BitsProduct
	GetAll() []*dynamo.BitsProduct
	Put(product *dynamo.BitsProduct)
	PutAll(products []*dynamo.BitsProduct)
	Blow()
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	Redis   redis.Cmdable  `inject:""`
	Statter statsd.Statter `inject:""`
}

func hashKey(platform petozi.Platform, productID string) string {
	return fmt.Sprintf(bitsProductsHashKey, platform.String(), productID)
}

func (c *cache) Get(platform petozi.Platform, productID string) *dynamo.BitsProduct {
	startTime := time.Now()
	cachedProduct, err := c.Redis.HGet(bitsProductsCacheKey, hashKey(platform, productID)).Result()
	defer func() {
		err := c.Statter.TimingDuration("get-bits-products-cache-latency", time.Since(startTime), 1.0)
		if err != nil {
			logrus.WithError(err).Error("could not log bits products cache get all products latency")
		}
	}()

	if err != nil && err != redis.Nil {
		logrus.WithError(err).Error("could not fetch product from cache")
		return nil
	}

	if strings.Blank(cachedProduct) {
		return nil
	}

	var product dynamo.BitsProduct
	err = json.Unmarshal([]byte(cachedProduct), &product)
	if err != nil {
		logrus.WithError(err).Error("could not unmarshal product entry from cache")
		c.Redis.HDel(bitsProductsCacheKey, hashKey(platform, productID))
		return nil
	}

	return &product
}

func (c *cache) GetAll() []*dynamo.BitsProduct {
	products := make([]*dynamo.BitsProduct, 0)

	startTime := time.Now()
	cachedProducts, err := c.Redis.HGetAll(bitsProductsCacheKey).Result()
	defer func() {
		err := c.Statter.TimingDuration("get-all-bits-products-cache-latency", time.Since(startTime), 1.0)
		if err != nil {
			logrus.WithError(err).Error("could not log  get all bits products cache products latency")
		}
	}()
	if err != nil && err != redis.Nil {
		logrus.WithError(err).Error("could not fetch all products from cache")
		return products
	}

	for key, value := range cachedProducts {
		if strings.Blank(value) {
			c.Redis.HDel(bitsProductsCacheKey, key)
			continue
		}

		var product dynamo.BitsProduct
		err = json.Unmarshal([]byte(value), &product)
		if err != nil {
			logrus.WithError(err).Error("could not unmarshal product entry from cache")
			c.Redis.HDel(bitsProductsCacheKey, key)
			continue
		}

		products = append(products, &product)
	}

	return products
}

func (c *cache) Put(product *dynamo.BitsProduct) {
	out, err := json.Marshal(&product)
	if err != nil {
		logrus.WithError(err).Error("could not marshal products entry for cache")
	}

	err = c.Redis.HSet(bitsProductsCacheKey, hashKey(product.Platform, product.ProductID), string(out)).Err()
	if err != nil {
		logrus.WithError(err).Error("could not set products entry in cache")
	}
}

func (c *cache) PutAll(products []*dynamo.BitsProduct) {
	valuesToPut := map[string]interface{}{}
	for _, product := range products {
		out, err := json.Marshal(&product)
		if err != nil {
			logrus.WithError(err).Error("could not marshal products entry for cache")
		}

		valuesToPut[hashKey(product.Platform, product.ProductID)] = string(out)
	}

	err := c.Redis.HMSet(bitsProductsCacheKey, valuesToPut).Err()
	if err != nil {
		logrus.WithError(err).Error("could not set products entry in cache")
	}
}

func (c *cache) Blow() {
	if err := c.Redis.Del(bitsProductsCacheKey).Err(); err != nil && err != redis.Nil {
		logrus.WithError(err).Error("could not blow the bits products cache")
	}
}
