package bits_products

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	statsd_mock "code.justin.tv/commerce/petozi/mocks/github.com/cactus/go-statsd-client/statsd"
	redis_mock "code.justin.tv/commerce/petozi/mocks/github.com/go-redis/redis"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)
		statter := new(statsd_mock.Statter)

		c := &cache{
			Redis:   redisClient,
			Statter: statter,
		}

		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		hashKey := hashKey(platform, productID)
		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("when the redis client errors", func() {
			resp := redis.NewStringResult("", errors.New("ERROR"))
			redisClient.On("HGet", bitsProductsCacheKey, hashKey).Return(resp)
			result := c.Get(platform, productID)

			So(result, ShouldBeNil)
		})

		Convey("when the redis client returns a blank value", func() {
			resp := redis.NewStringResult("", nil)
			redisClient.On("HGet", bitsProductsCacheKey, hashKey).Return(resp)
			result := c.Get(platform, productID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			resp := redis.NewStringResult("this is invalid json &*!#(", nil)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("HGet", bitsProductsCacheKey, hashKey).Return(resp)
			redisClient.On("HDel", bitsProductsCacheKey, hashKey).Return(delResp)

			result := c.Get(platform, productID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", bitsProductsCacheKey, hashKey)
			})
		})

		Convey("we should return bits products", func() {
			cachedValue := &dynamo.BitsProduct{
				Platform:  platform,
				ProductID: productID,
			}
			cachedBytes, err := json.Marshal(cachedValue)

			if err != nil {
				logrus.WithError(err).Error("Error marshaling bits products")
				t.Fail()
			}

			resp := redis.NewStringResult(string(cachedBytes), nil)
			redisClient.On("HGet", bitsProductsCacheKey, hashKey).Return(resp)

			returnValue := c.Get(platform, productID)

			So(returnValue, ShouldResemble, cachedValue)
		})
	})
}

func TestCache_GetAll(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)
		statter := new(statsd_mock.Statter)

		c := &cache{
			Redis:   redisClient,
			Statter: statter,
		}

		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		hashKey := hashKey(platform, productID)
		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("when the redis client errors", func() {
			resp := redis.NewStringStringMapResult(nil, errors.New("ERROR"))
			redisClient.On("HGetAll", bitsProductsCacheKey).Return(resp)
			result := c.GetAll()

			So(result, ShouldHaveLength, 0)
		})

		Convey("when the redis client returns a blank value", func() {
			resultMap := map[string]string{
				hashKey: "",
			}
			resp := redis.NewStringStringMapResult(resultMap, nil)
			redisClient.On("HGetAll", bitsProductsCacheKey).Return(resp)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("HDel", bitsProductsCacheKey, hashKey).Return(delResp)
			result := c.GetAll()

			Convey("we should return nil", func() {
				So(result, ShouldHaveLength, 0)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", bitsProductsCacheKey, hashKey)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			resultMap := map[string]string{
				hashKey: "this is invalid json &*!#(",
			}
			resp := redis.NewStringStringMapResult(resultMap, nil)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("HGetAll", bitsProductsCacheKey).Return(resp)
			redisClient.On("HDel", bitsProductsCacheKey, hashKey).Return(delResp)

			result := c.GetAll()

			Convey("we should return nil", func() {
				So(result, ShouldHaveLength, 0)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "HDel", bitsProductsCacheKey, hashKey)
			})
		})

		Convey("we should return bits products", func() {
			cachedValue := &dynamo.BitsProduct{
				Platform:  platform,
				ProductID: productID,
			}
			cachedBytes, err := json.Marshal(cachedValue)

			if err != nil {
				logrus.WithError(err).Error("Error marshaling bits products")
				t.Fail()
			}

			resultMap := map[string]string{
				hashKey: string(cachedBytes),
			}
			resp := redis.NewStringStringMapResult(resultMap, nil)
			redisClient.On("HGetAll", bitsProductsCacheKey).Return(resp)

			returnValue := c.GetAll()

			So(returnValue, ShouldHaveLength, 1)
			So(returnValue[0], ShouldResemble, cachedValue)
		})
	})
}

func TestCache_Put(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)

		c := &cache{
			Redis: redisClient,
		}

		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		hashKey := hashKey(platform, productID)

		product := &dynamo.BitsProduct{
			Platform:  platform,
			ProductID: productID,
		}

		Convey("when the redis client returns an error", func() {
			Convey("we should return nothing", func() {
				cachedBytes, err := json.Marshal(product)

				if err != nil {
					logrus.WithError(err).Error("Error marshaling settings")
					t.Fail()
				}

				redisClient.On("HSet", bitsProductsCacheKey, hashKey, string(cachedBytes)).Return(redis.NewBoolCmd(errors.New("ERROR")))

				c.Put(product)

				redisClient.AssertCalled(t, "HSet", bitsProductsCacheKey, hashKey, string(cachedBytes))
			})
		})

		Convey("when the redis client does not return an error", func() {
			Convey("we should return nothing", func() {
				cachedBytes, err := json.Marshal(product)

				if err != nil {
					logrus.WithError(err).Error("Error marshaling settings")
					t.Fail()
				}

				redisClient.On("HSet", bitsProductsCacheKey, hashKey, string(cachedBytes)).Return(redis.NewBoolCmd(nil))

				c.Put(product)

				redisClient.AssertCalled(t, "HSet", bitsProductsCacheKey, hashKey, string(cachedBytes))
			})
		})
	})
}

func TestCache_PutAll(t *testing.T) {
	Convey("given a bits products cache", t, func() {
		redisClient := new(redis_mock.Cmdable)

		c := &cache{
			Redis: redisClient,
		}

		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		hashKey := hashKey(platform, productID)

		product := &dynamo.BitsProduct{
			Platform:  platform,
			ProductID: productID,
		}

		cachedBytes, err := json.Marshal(product)

		if err != nil {
			logrus.WithError(err).Error("Error marshaling settings")
			t.Fail()
		}

		putMap := map[string]interface{}{
			hashKey: string(cachedBytes),
		}

		Convey("when the redis client returns an error", func() {
			Convey("we should return nothing", func() {
				redisClient.On("HMSet", bitsProductsCacheKey, putMap).Return(redis.NewStatusCmd(errors.New("ERROR")))

				c.PutAll([]*dynamo.BitsProduct{product})

				redisClient.AssertCalled(t, "HMSet", bitsProductsCacheKey, putMap)
			})
		})

		Convey("when the redis client does not return an error", func() {
			Convey("we should return nothing", func() {
				redisClient.On("HMSet", bitsProductsCacheKey, putMap).Return(redis.NewStatusCmd(nil))

				c.PutAll([]*dynamo.BitsProduct{product})

				redisClient.AssertCalled(t, "HMSet", bitsProductsCacheKey, putMap)
			})
		})
	})
}
