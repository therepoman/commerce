package bits_types

import (
	"context"
	"errors"
	"testing"

	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_Get(t *testing.T) {
	Convey("GetAll", t, func() {
		mockFetcher := new(bits_types_mock.Fetcher)

		getter := &getter{
			Fetcher: mockFetcher,
		}

		ctx := context.Background()

		record := BitsTypes()[0]

		Convey("when fetcher retrives bits types", func() {
			mockFetcher.On("Fetch", ctx, record.ID).Return(record, nil)

			types, err := getter.Get(ctx, record.ID)
			So(err, ShouldBeNil)
			So(types, ShouldResemble, TwirpBitsTypes()[0])
		})

		Convey("when fetcher fails", func() {
			mockFetcher.On("Fetch", ctx, record.ID).Return(nil, errors.New("some error"))

			_, err := getter.Get(ctx, record.ID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestGetter_GetAll(t *testing.T) {
	Convey("GetAll", t, func() {
		mockFetcher := new(bits_types_mock.Fetcher)

		getter := &getter{
			Fetcher: mockFetcher,
		}

		ctx := context.Background()

		Convey("when fetcher retrives bits types", func() {
			mockFetcher.On("FetchAll", ctx).Return(BitsTypes(), nil)

			types, err := getter.GetAll(ctx)
			So(err, ShouldBeNil)
			So(types, ShouldResemble, TwirpBitsTypes())
		})

		Convey("when fetcher fails", func() {
			mockFetcher.On("FetchAll", ctx).Return(nil, errors.New("some error"))

			_, err := getter.GetAll(ctx)
			So(err, ShouldNotBeNil)
		})
	})
}
