package bits_types

import (
	"context"
	"errors"
	"testing"

	bits_types_cache_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/cache/bits_types"
	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("GetAll", t, func() {
		mockDAO := new(bits_types_mock.DAO)
		cache := new(bits_types_cache_mock.Cache)

		fetcher := &fetcher{
			DAO:   mockDAO,
			Cache: cache,
		}

		ctx := context.Background()

		record := BitsTypes()[0]

		Convey("when the cache has records in it", func() {
			cache.On("Get", record.ID).Return(record)

			Convey("then we return the cached records", func() {
				bitsType, err := fetcher.Fetch(ctx, record.ID)

				So(err, ShouldBeNil)
				So(bitsType, ShouldResemble, record)
			})
		})

		Convey("when the cache has no records", func() {
			cache.On("Get", record.ID).Return(nil)

			Convey("when DAO fails", func() {
				mockDAO.On("Get", ctx, record.ID).Return(nil, errors.New("dynamo is gone"))

				_, err := fetcher.Fetch(ctx, record.ID)
				So(err, ShouldNotBeNil)
			})

			Convey("when the DAO succeeds", func() {
				cache.On("Put", record).Return()

				Convey("when the DAO has no bits types", func() {
					mockDAO.On("Get", ctx, record.ID).Return(nil, nil)

					bitsType, err := fetcher.Fetch(ctx, record.ID)
					So(err, ShouldBeNil)
					So(bitsType, ShouldBeNil)
				})

				Convey("when DAO retrieves bits types", func() {
					mockDAO.On("Get", ctx, record.ID).Return(record, nil)

					bitsType, err := fetcher.Fetch(ctx, record.ID)
					So(err, ShouldBeNil)
					So(bitsType, ShouldResemble, record)
				})
			})
		})
	})
}

func TestFetcher_FetchAll(t *testing.T) {
	Convey("GetAll", t, func() {
		mockDAO := new(bits_types_mock.DAO)
		cache := new(bits_types_cache_mock.Cache)

		fetcher := &fetcher{
			DAO:   mockDAO,
			Cache: cache,
		}

		ctx := context.Background()

		Convey("when the cache has records in it", func() {
			cache.On("GetAll").Return(BitsTypes())

			Convey("then we return the cached records", func() {
				types, err := fetcher.FetchAll(ctx)

				So(err, ShouldBeNil)
				So(types, ShouldResemble, BitsTypes())
			})
		})

		Convey("when the cache has no records", func() {
			cache.On("GetAll").Return(nil)

			Convey("when DAO fails", func() {
				mockDAO.On("GetAll", ctx).Return(nil, errors.New("dynamo is gone"))

				_, err := fetcher.FetchAll(ctx)
				So(err, ShouldNotBeNil)
			})

			Convey("when the DAO succeeds", func() {
				cache.On("PutAll", BitsTypes()).Return()

				Convey("when the DAO has no bits types", func() {
					mockDAO.On("GetAll", ctx).Return(nil, nil)

					types, err := fetcher.FetchAll(ctx)
					So(err, ShouldBeNil)
					So(types, ShouldBeNil)
				})

				Convey("when DAO retrieves bits types", func() {
					mockDAO.On("GetAll", ctx).Return(BitsTypes(), nil)

					types, err := fetcher.FetchAll(ctx)
					So(err, ShouldBeNil)
					So(types, ShouldResemble, BitsTypes())
				})
			})
		})
	})
}
