package bits_types

import (
	"context"

	bits_types_cache "code.justin.tv/commerce/petozi/backend/cache/bits_types"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
)

type Fetcher interface {
	Fetch(ctx context.Context, id string) (*dynamo.BitsType, error)
	FetchAll(ctx context.Context) ([]*dynamo.BitsType, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	DAO   bits_types.DAO         `inject:""`
	Cache bits_types_cache.Cache `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, id string) (*dynamo.BitsType, error) {
	cachedType := f.Cache.Get(id)
	if cachedType != nil {
		return cachedType, nil
	}

	dbType, err := f.DAO.Get(ctx, id)
	if err != nil {
		return nil, err
	}

	if dbType != nil {
		go f.Cache.Put(dbType)
	}

	return dbType, nil
}

func (f *fetcher) FetchAll(ctx context.Context) ([]*dynamo.BitsType, error) {
	cachedTypes := f.Cache.GetAll()
	if len(cachedTypes) > 0 {
		return cachedTypes, nil
	}

	dbTypes, err := f.DAO.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	if len(dbTypes) > 0 {
		go f.Cache.PutAll(dbTypes)
	}

	return dbTypes, err
}
