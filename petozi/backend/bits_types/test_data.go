package bits_types

import (
	"code.justin.tv/commerce/petozi/backend/dynamo"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

func BitsTypes() []*dynamo.BitsType {
	return []*dynamo.BitsType{
		&dynamo.BitsType{
			ID:                    "bits-type-1",
			DisplayName:           "bits type 1",
			IsPaid:                true,
			IsPromotional:         true,
			CostPerBitsUSC:        1,
			EntitlementSourceType: petozi.EntitlementSourceType_IN_APP_PURCHASE,
			BusinessAttribution:   petozi.BusinessAttribution_PAYMENT_TRANSACTION,
		},
		&dynamo.BitsType{
			ID:                    "bits-type-2",
			DisplayName:           "bits type 2",
			IsPaid:                false,
			IsPromotional:         false,
			CostPerBitsUSC:        2,
			EntitlementSourceType: petozi.EntitlementSourceType_WEB_PURCHASE,
			BusinessAttribution:   petozi.BusinessAttribution_PAYMENT_TRANSACTION,
		},
		&dynamo.BitsType{
			ID:                    "bits-type-3",
			DisplayName:           "bits type 3",
			IsPaid:                true,
			IsPromotional:         false,
			CostPerBitsUSC:        3,
			EntitlementSourceType: petozi.EntitlementSourceType_DIRECT_ENTITLEMENT,
			BusinessAttribution:   petozi.BusinessAttribution_CUSTOMER_SERVICE_GOODWILL,
		},
	}
}

func TwirpBitsTypes() []*petozi.BitsType {
	return []*petozi.BitsType{
		&petozi.BitsType{
			Id:                    "bits-type-1",
			DisplayName:           "bits type 1",
			IsPaid:                true,
			IsPromotional:         true,
			CostPerBitsUsc:        1,
			EntitlementSourceType: petozi.EntitlementSourceType_IN_APP_PURCHASE,
			BusinessAttribution:   petozi.BusinessAttribution_PAYMENT_TRANSACTION,
		},
		&petozi.BitsType{
			Id:                    "bits-type-2",
			DisplayName:           "bits type 2",
			IsPaid:                false,
			IsPromotional:         false,
			CostPerBitsUsc:        2,
			EntitlementSourceType: petozi.EntitlementSourceType_WEB_PURCHASE,
			BusinessAttribution:   petozi.BusinessAttribution_PAYMENT_TRANSACTION,
		},
		&petozi.BitsType{
			Id:                    "bits-type-3",
			DisplayName:           "bits type 3",
			IsPaid:                true,
			IsPromotional:         false,
			CostPerBitsUsc:        3,
			EntitlementSourceType: petozi.EntitlementSourceType_DIRECT_ENTITLEMENT,
			BusinessAttribution:   petozi.BusinessAttribution_CUSTOMER_SERVICE_GOODWILL,
		},
	}
}
