package bits_types

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/petozi/backend/dynamo"
	bits_types_cache_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/cache/bits_types"
	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCreator(t *testing.T) {
	Convey("Create", t, func() {
		mockDAO := new(bits_types_mock.DAO)
		cache := new(bits_types_cache_mock.Cache)

		creator := &creator{
			DAO:   mockDAO,
			Cache: cache,
		}

		twirpBitsType := TwirpBitsTypes()[0]
		bitsType := BitsTypes()[0]

		ctx := context.Background()

		Convey("with a valid bits type", func() {
			mockDAO.On("Update", ctx, bitsType).Return(nil)

			Convey("when the fetch of bits products fails", func() {
				mockDAO.On("GetAll", ctx).Return(nil, errors.New("WALRUS STRIKE"))

				err := creator.Create(ctx, twirpBitsType)
				So(err, ShouldNotBeNil)
			})

			Convey("when the fetch of bits products succeeds", func() {
				mockDAO.On("GetAll", ctx).Return([]*dynamo.BitsType{bitsType}, nil)
				cache.On("PutAll", mock.Anything).Return()

				err := creator.Create(ctx, twirpBitsType)
				So(err, ShouldBeNil)
			})
		})

		Convey("when the update fails", func() {
			mockDAO.On("Update", ctx, bitsType).Return(errors.New("WALRUS STRIKE"))

			err := creator.Create(ctx, twirpBitsType)
			So(err, ShouldNotBeNil)
		})

		Convey("with nil", func() {
			err := creator.Create(ctx, nil)
			So(err, ShouldNotBeNil)
		})

	})
}
