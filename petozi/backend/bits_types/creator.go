package bits_types

import (
	"context"
	"errors"

	"code.justin.tv/commerce/logrus"
	bits_types_cache "code.justin.tv/commerce/petozi/backend/cache/bits_types"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type Creator interface {
	Create(ctx context.Context, bitsType *petozi.BitsType) error
}

func NewCreator() Creator {
	return &creator{}
}

type creator struct {
	DAO   bits_types.DAO         `inject:""`
	Cache bits_types_cache.Cache `inject:""`
}

func (c *creator) Create(ctx context.Context, bitsType *petozi.BitsType) error {
	if bitsType == nil {
		return errors.New("bitsType is missing")
	}

	newBitsType := dynamo.ToDynamoBitsType(bitsType)

	err := c.DAO.Update(ctx, newBitsType)
	if err != nil {
		return err
	}

	types, err := c.DAO.GetAll(ctx)
	if err != nil {
		logrus.WithError(err).Error("failed to fetch all types while reseting caches")
		return err
	}
	c.Cache.PutAll(types)
	return nil
}
