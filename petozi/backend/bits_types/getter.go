package bits_types

import (
	"context"

	"code.justin.tv/commerce/petozi/backend/dynamo"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type Getter interface {
	Get(ctx context.Context, id string) (*petozi.BitsType, error)
	GetAll(ctx context.Context) ([]*petozi.BitsType, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func (g *getter) Get(ctx context.Context, id string) (*petozi.BitsType, error) {
	bitsType, err := g.Fetcher.Fetch(ctx, id)
	if err != nil {
		return nil, err
	}

	return dynamo.FromDynamoBitsType(bitsType), nil
}

func (g *getter) GetAll(ctx context.Context) ([]*petozi.BitsType, error) {
	types, err := g.Fetcher.FetchAll(ctx)
	if err != nil {
		return nil, err
	}

	twirpTypes := make([]*petozi.BitsType, 0, len(types))
	for _, t := range types {
		twirpTypes = append(twirpTypes, dynamo.FromDynamoBitsType(t))
	}

	return twirpTypes, nil
}
