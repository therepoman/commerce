package bits_products

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/petozi/backend/dynamo"
	products_cache_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/cache/bits_products"
	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGod_LetThereBeLight(t *testing.T) {
	Convey("given a bits products creator", t, func() {
		dao := new(bits_products_mock.DAO)
		cache := new(products_cache_mock.Cache)

		c := &god{
			DAO:   dao,
			Cache: cache,
		}

		newProduct := &petozi.BitsProduct{
			Id:                     "walrusProduct",
			Platform:               petozi.Platform_AMAZON,
			PricingId:              "walrusPriceID",
			BitsTypeId:             "walrusBitsType",
			DisplayName:            "Walrus Product",
			Quantity:               1,
			MaxPurchasableQuantity: 1,
		}

		ctx := context.Background()

		Convey("when the update fails", func() {
			dao.On("Update", ctx, dynamo.ToDynamoBitsProduct(newProduct)).Return(errors.New("WALRUS STRIKE"))

			err := c.LetThereBeLight(ctx, newProduct)
			So(err, ShouldNotBeNil)
		})

		Convey("when the update succeeds", func() {
			dao.On("Update", ctx, dynamo.ToDynamoBitsProduct(newProduct)).Return(nil)

			Convey("when we fail to fetch all the products", func() {
				dao.On("GetAll", ctx).Return(nil, errors.New("WALRUS STRIKE"))

				err := c.LetThereBeLight(ctx, newProduct)
				So(err, ShouldNotBeNil)
			})

			Convey("when we succeed to fetch all the products", func() {
				dao.On("GetAll", ctx).Return([]*dynamo.BitsProduct{dynamo.ToDynamoBitsProduct(newProduct)}, nil)

				cache.On("PutAll", mock.Anything).Return()
				err := c.LetThereBeLight(ctx, newProduct)
				So(err, ShouldBeNil)
			})
		})
	})
}
