package bits_products

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	products_cache_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/cache/bits_products"
	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a bits products fetcher", t, func() {
		dao := new(bits_products_mock.DAO)
		redisCache := new(products_cache_mock.Cache)

		f := &fetcher{
			DAO:           dao,
			RedisCache:    redisCache,
			InMemoryCache: ttlcache.NewCache(inMemoryCacheCleanupInterval),
		}

		ctx := context.Background()
		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		product := &dynamo.BitsProduct{
			Platform:  platform,
			ProductID: productID,
		}

		Convey("when the product was in the redis cache", func() {
			redisCache.On("Get", platform, productID).Return(product)

			Convey("then we return the cached product", func() {
				p, err := f.Fetch(ctx, platform, productID)
				So(err, ShouldBeNil)
				So(p, ShouldNotBeNil)
				So(p.ProductID, ShouldEqual, productID)
				So(p.Platform, ShouldEqual, platform)
			})
		})

		Convey("when the product was not in the redis cache", func() {
			redisCache.On("Get", platform, productID).Return(nil)

			Convey("when the dao fails", func() {
				dao.On("Get", ctx, platform, productID).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					p, err := f.Fetch(ctx, platform, productID)
					So(err, ShouldNotBeNil)
					So(p, ShouldBeNil)
				})
			})

			Convey("when the dao succeeds", func() {
				redisCache.On("Put", product).Return()

				Convey("when the dao does not return a product", func() {
					dao.On("Get", ctx, platform, productID).Return(nil, nil)

					Convey("then we should return the nil product", func() {
						p, err := f.Fetch(ctx, platform, productID)
						So(err, ShouldBeNil)
						So(p, ShouldBeNil)
					})
				})

				Convey("when the dao returns a product", func() {
					dao.On("Get", ctx, platform, productID).Return(product, nil)

					Convey("then we should return the product", func() {
						p, err := f.Fetch(ctx, platform, productID)
						So(err, ShouldBeNil)
						So(p, ShouldNotBeNil)
						So(p.ProductID, ShouldEqual, productID)
						So(p.Platform, ShouldEqual, platform)
					})
				})
			})
		})
	})
}

func countCalls(calls []mock.Call, method string) int {
	count := 0
	for _, call := range calls {
		if call.Method == method {
			count++
		}
	}
	return count
}

func TestFetcher_FetchAll(t *testing.T) {
	Convey("given a bits products fetcher", t, func() {
		dao := new(bits_products_mock.DAO)
		redisCache := new(products_cache_mock.Cache)

		f := &fetcher{
			DAO:           dao,
			RedisCache:    redisCache,
			InMemoryCache: ttlcache.NewCache(inMemoryCacheCleanupInterval),
		}

		ctx := context.Background()
		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		products := []*dynamo.BitsProduct{
			{
				Platform:  platform,
				ProductID: productID,
			},
		}

		Convey("when the redis cache returns products", func() {
			redisCache.On("GetAll").Return(products)

			Convey("then we return the cached products", func() {
				ps, err := f.FetchAll(ctx, true)
				So(err, ShouldBeNil)
				So(ps, ShouldNotBeNil)
				So(ps, ShouldHaveLength, 1)
				So(ps[0].ProductID, ShouldEqual, productID)
				So(ps[0].Platform, ShouldEqual, platform)

				Convey("a subsequent request without the in-mem param uses the redis cache again", func() {
					ps, err := f.FetchAll(ctx, false)
					So(err, ShouldBeNil)
					So(ps, ShouldNotBeNil)
					So(ps, ShouldHaveLength, 1)
					So(ps[0].ProductID, ShouldEqual, productID)
					So(ps[0].Platform, ShouldEqual, platform)
					So(countCalls(redisCache.Calls, "GetAll"), ShouldEqual, 2)
				})

				Convey("a subsequent request with the in-mem param uses the in memory cache", func() {
					ps, err := f.FetchAll(ctx, true)
					So(err, ShouldBeNil)
					So(ps, ShouldNotBeNil)
					So(ps, ShouldHaveLength, 1)
					So(ps[0].ProductID, ShouldEqual, productID)
					So(ps[0].Platform, ShouldEqual, platform)
					So(countCalls(redisCache.Calls, "GetAll"), ShouldEqual, 1)
				})
			})
		})

		Convey("when the redis cache returns nothing", func() {
			redisCache.On("GetAll").Return(nil)

			Convey("when the dao fails", func() {
				dao.On("GetAll", ctx).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					products, err := f.FetchAll(ctx, false)
					So(err, ShouldNotBeNil)
					So(products, ShouldBeNil)
				})
			})

			Convey("when the dao succeeds", func() {
				redisCache.On("PutAll", products).Return()

				Convey("when the dao returns nothing", func() {
					dao.On("GetAll", ctx).Return(nil, nil)

					Convey("then we return nothing", func() {
						ps, err := f.FetchAll(ctx, false)
						So(err, ShouldBeNil)
						So(ps, ShouldBeNil)
					})
				})

				Convey("when the dao returns products", func() {
					dao.On("GetAll", ctx).Return(products, nil)

					Convey("then we should return the products", func() {
						ps, err := f.FetchAll(ctx, true)
						So(err, ShouldBeNil)
						So(ps, ShouldNotBeNil)
						So(ps, ShouldHaveLength, 1)
						So(ps[0].ProductID, ShouldEqual, productID)
						So(ps[0].Platform, ShouldEqual, platform)

						Convey("a subsequent request without the in-mem param uses the redis cache", func() {
							ps, err := f.FetchAll(ctx, false)
							So(err, ShouldBeNil)
							So(ps, ShouldNotBeNil)
							So(ps, ShouldHaveLength, 1)
							So(ps[0].ProductID, ShouldEqual, productID)
							So(ps[0].Platform, ShouldEqual, platform)
							So(countCalls(redisCache.Calls, "GetAll"), ShouldEqual, 2)
						})

						Convey("a subsequent request with the in-mem param uses the in memory cache", func() {
							ps, err := f.FetchAll(ctx, true)
							So(err, ShouldBeNil)
							So(ps, ShouldNotBeNil)
							So(ps, ShouldHaveLength, 1)
							So(ps[0].ProductID, ShouldEqual, productID)
							So(ps[0].Platform, ShouldEqual, platform)
							So(countCalls(redisCache.Calls, "GetAll"), ShouldEqual, 1)
						})
					})
				})
			})
		})
	})
}
