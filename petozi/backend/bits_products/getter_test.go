package bits_products

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/petozi/backend/dynamo"
	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a bits products getter", t, func() {
		fetcher := new(bits_products_mock.Fetcher)

		g := &getter{
			Fetcher: fetcher,
		}

		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		ctx := context.Background()

		Convey("when the dao fails", func() {
			fetcher.On("Fetch", ctx, platform, productID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				product, err := g.Get(ctx, platform, productID)
				So(err, ShouldNotBeNil)
				So(product, ShouldBeNil)
			})
		})

		Convey("when the dao succeeds", func() {
			fetcher.On("Fetch", ctx, platform, productID).Return(&dynamo.BitsProduct{
				Platform:  platform,
				ProductID: productID,
			}, nil)

			Convey("then we should return the product", func() {
				product, err := g.Get(ctx, platform, productID)
				So(err, ShouldBeNil)
				So(product, ShouldNotBeNil)
				So(product.Id, ShouldEqual, productID)
				So(product.Platform, ShouldEqual, platform)
			})
		})
	})
}

func TestGetter_GetAll(t *testing.T) {
	Convey("given a bits products getter", t, func() {
		fetcher := new(bits_products_mock.Fetcher)

		g := &getter{
			Fetcher: fetcher,
		}

		platform := petozi.Platform_AMAZON
		productID := "walrusProduct"

		ctx := context.Background()

		Convey("when the dao fails", func() {
			fetcher.On("FetchAll", ctx, false).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				products, err := g.GetAll(ctx, false)
				So(err, ShouldNotBeNil)
				So(products, ShouldBeNil)
			})
		})

		Convey("when the dao succeeds", func() {
			fetcher.On("FetchAll", ctx, false).Return([]*dynamo.BitsProduct{
				{
					Platform:  platform,
					ProductID: productID,
				},
			}, nil)

			Convey("then we should return the products", func() {
				products, err := g.GetAll(ctx, false)
				So(err, ShouldBeNil)
				So(products, ShouldNotBeNil)
				So(products, ShouldHaveLength, 1)
				So(products[0].Id, ShouldEqual, productID)
				So(products[0].Platform, ShouldEqual, platform)
			})
		})
	})
}
