package bits_products

import (
	"context"

	petozi "code.justin.tv/commerce/petozi/rpc"
)

type Getter interface {
	Get(ctx context.Context, platform petozi.Platform, productID string) (*petozi.BitsProduct, error)
	GetAll(ctx context.Context, useInMemoryCache bool) ([]*petozi.BitsProduct, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func (g *getter) GetAll(ctx context.Context, useInMemoryCache bool) ([]*petozi.BitsProduct, error) {
	dynamoProducts, err := g.Fetcher.FetchAll(ctx, useInMemoryCache)
	if err != nil || len(dynamoProducts) < 1 {
		return nil, err
	}

	products := make([]*petozi.BitsProduct, 0)
	for _, p := range dynamoProducts {
		products = append(products, p.ToTwirpBitsProduct())
	}

	return products, nil
}

func (g *getter) Get(ctx context.Context, platform petozi.Platform, productID string) (*petozi.BitsProduct, error) {
	product, err := g.Fetcher.Fetch(ctx, platform, productID)
	if err != nil || product == nil {
		return nil, err
	}

	return product.ToTwirpBitsProduct(), nil
}
