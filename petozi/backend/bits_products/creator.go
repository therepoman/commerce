package bits_products

import (
	"context"

	"code.justin.tv/commerce/logrus"
	products_cache "code.justin.tv/commerce/petozi/backend/cache/bits_products"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/backend/dynamo/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type Creator interface {
	LetThereBeLight(ctx context.Context, product *petozi.BitsProduct) error
}

func NewCreator() Creator {
	return &god{}
}

type god struct {
	DAO   bits_products.DAO    `inject:""`
	Cache products_cache.Cache `inject:""`
}

func (g *god) LetThereBeLight(ctx context.Context, product *petozi.BitsProduct) error {
	newProductEntry := dynamo.ToDynamoBitsProduct(product)

	err := g.DAO.Update(ctx, newProductEntry)
	if err != nil {
		return err
	}

	products, err := g.DAO.GetAll(ctx)
	if err != nil {
		logrus.WithError(err).Error("failed to fetch all products while reseting caches")
		return err
	}
	g.Cache.PutAll(products)

	return nil
}
