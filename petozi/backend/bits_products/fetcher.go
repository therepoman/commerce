package bits_products

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	products_cache "code.justin.tv/commerce/petozi/backend/cache/bits_products"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/backend/dynamo/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

const (
	inMemoryCacheTTL             = time.Minute
	inMemoryCacheCleanupInterval = 10 * time.Second
	inMemoryCacheKey             = "bits-products"
)

type Fetcher interface {
	Fetch(ctx context.Context, platform petozi.Platform, productID string) (*dynamo.BitsProduct, error)
	FetchAll(ctx context.Context, useInMemoryCache bool) ([]*dynamo.BitsProduct, error)
}

func NewFetcher() Fetcher {
	return &fetcher{
		InMemoryCache: ttlcache.NewCache(inMemoryCacheCleanupInterval),
	}
}

type fetcher struct {
	InMemoryCache ttlcache.Cache
	RedisCache    products_cache.Cache `inject:""`
	DAO           bits_products.DAO    `inject:""`
}

func (f *fetcher) FetchAll(ctx context.Context, useInMemoryCache bool) ([]*dynamo.BitsProduct, error) {
	if useInMemoryCache {
		memCachedProductsI := f.InMemoryCache.Get(inMemoryCacheKey)
		if memCachedProductsI != nil {
			memCachedProducts, ok := memCachedProductsI.([]*dynamo.BitsProduct)
			if ok && len(memCachedProducts) > 0 {
				return memCachedProducts, nil
			}
		}
	}

	redisCachedProducts := f.RedisCache.GetAll()
	if len(redisCachedProducts) > 0 {
		if useInMemoryCache {
			f.InMemoryCache.Set(inMemoryCacheKey, redisCachedProducts, inMemoryCacheTTL)
		}
		return redisCachedProducts, nil
	}

	dbProducts, err := f.DAO.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	if len(dbProducts) > 0 {
		go f.RedisCache.PutAll(dbProducts)
		if useInMemoryCache {
			f.InMemoryCache.Set(inMemoryCacheKey, dbProducts, inMemoryCacheTTL)
		}
	}

	return dbProducts, nil
}

func (f *fetcher) Fetch(ctx context.Context, platform petozi.Platform, productID string) (*dynamo.BitsProduct, error) {
	cachedProduct := f.RedisCache.Get(platform, productID)
	if cachedProduct != nil {
		return cachedProduct, nil
	}

	dbProduct, err := f.DAO.Get(ctx, platform, productID)
	if err != nil {
		return nil, err
	}

	if dbProduct != nil {
		go f.RedisCache.Put(dbProduct)
	}

	return dbProduct, nil
}
