package get_bits_products

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	GetAll(ctx context.Context, req *petozi.GetBitsProductsReq) (*petozi.GetBitsProductsResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Getter bits_products.Getter `inject:""`
}

func (a *api) GetAll(ctx context.Context, req *petozi.GetBitsProductsReq) (*petozi.GetBitsProductsResp, error) {
	products, err := a.Getter.GetAll(ctx, req.UseInMemoryCache)

	if err != nil {
		logrus.WithError(err).Error("failed to fetch bits products")
		return nil, twirp.InternalErrorWith(err)
	}

	return &petozi.GetBitsProductsResp{
		BitsProducts: products,
	}, nil
}
