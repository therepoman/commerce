package get_bits_products

import (
	"context"
	"errors"
	"testing"

	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_GetAll(t *testing.T) {
	Convey("given a get bits products API", t, func() {
		getter := new(bits_products_mock.Getter)

		a := &api{
			Getter: getter,
		}

		req := &petozi.GetBitsProductsReq{}
		ctx := context.Background()

		Convey("when we fail to get the products", func() {
			getter.On("GetAll", ctx, false).Return(nil, errors.New("WALRUS STRIKE"))

			_, err := a.GetAll(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("when we succeed to get the products", func() {
			getter.On("GetAll", ctx, false).Return([]*petozi.BitsProduct{
				{
					Id:       "walrusProduct",
					Platform: petozi.Platform_AMAZON,
				},
			}, nil)

			resp, err := a.GetAll(context.Background(), req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.BitsProducts, ShouldNotBeNil)
			So(resp.BitsProducts, ShouldHaveLength, 1)
		})
	})
}
