package put_bits_type

import (
	"context"

	"code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Put(ctx context.Context, req *petozi.PutBitsTypeReq) (*petozi.PutBitsTypeResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validation Validation         `inject:""`
	Creator    bits_types.Creator `inject:""`
}

func (a *api) Put(ctx context.Context, req *petozi.PutBitsTypeReq) (*petozi.PutBitsTypeResp, error) {
	err := a.Validation.IsValid(req)
	if err != nil {
		return nil, err
	}

	err = a.Creator.Create(ctx, req.BitsType)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &petozi.PutBitsTypeResp{}, nil
}
