package put_bits_type

import (
	"testing"

	"code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidation(t *testing.T) {
	Convey("isValid", t, func() {

		v := validation{}
		req := &petozi.PutBitsTypeReq{
			BitsType: bits_types.TwirpBitsTypes()[0],
		}

		Convey("with a valid request", func() {
			err := v.IsValid(req)
			So(err, ShouldBeNil)
		})

		Convey("with an invalid request", func() {
			Convey("missing id", func() {
				req.BitsType.Id = ""
			})

			Convey("missing display name", func() {
				req.BitsType.DisplayName = ""
			})

			Convey("invalid entitlement source name", func() {
				req.BitsType.EntitlementSourceType = petozi.EntitlementSourceType(int32(12345))
			})

			Convey("invalid business attribution", func() {
				req.BitsType.BusinessAttribution = petozi.BusinessAttribution(-12)
			})

			err := v.IsValid(req)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "invalid_argument")
		})
	})
}
