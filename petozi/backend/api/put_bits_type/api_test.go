package put_bits_type

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/petozi/backend/bits_types"
	put_bits_type_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/api/put_bits_type"
	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPutBitsType(t *testing.T) {
	Convey("PutBitsType", t, func() {
		mockCreator := new(bits_types_mock.Creator)
		mockValidation := new(put_bits_type_mock.Validation)

		api := &api{
			Creator:    mockCreator,
			Validation: mockValidation,
		}

		req := &petozi.PutBitsTypeReq{
			BitsType: bits_types.TwirpBitsTypes()[0],
		}

		ctx := context.Background()

		Convey("with a valid request", func() {
			mockValidation.On("IsValid", mock.Anything).Return(nil)

			Convey("when creator succeeds", func() {
				mockCreator.On("Create", ctx, bits_types.TwirpBitsTypes()[0]).Return(nil)

				resp, err := api.Put(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})

			Convey("when creator fails", func() {
				mockCreator.On("Create", ctx, bits_types.TwirpBitsTypes()[0]).Return(errors.New("oh no"))

				_, err := api.Put(ctx, req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("with an invalid request", func() {
			mockValidation.On("IsValid", mock.Anything).Return(errors.New("must not exceed 5mph in the parking lot"))

			_, err := api.Put(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
