package put_bits_type

import (
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

const (
	MaxBitsTypeIDLength = 50
)

type Validation interface {
	IsValid(req *petozi.PutBitsTypeReq) error
}

func NewValidation() Validation {
	return &validation{}
}

type validation struct {
}

func (v *validation) IsValid(req *petozi.PutBitsTypeReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "body is missing")
	}

	if req.BitsType == nil {
		return twirp.InvalidArgumentError("bits_type", "is missing")
	}

	if strings.Blank(req.BitsType.Id) {
		return twirp.InvalidArgumentError("id", "is missing")
	}

	if len(req.BitsType.Id) > MaxBitsTypeIDLength {
		return twirp.InvalidArgumentError("id", fmt.Sprintf("must be less than %d characters", MaxBitsTypeIDLength))
	}

	if strings.Blank(req.BitsType.DisplayName) {
		return twirp.InvalidArgumentError("display_name", "is missing")
	}

	_, ok := petozi.EntitlementSourceType_name[int32(req.BitsType.EntitlementSourceType)]
	if !ok {
		return twirp.InvalidArgumentError("entitlement_source_type", "s not valid")
	}

	_, ok = petozi.BusinessAttribution_name[int32(req.BitsType.BusinessAttribution)]
	if !ok {
		return twirp.InvalidArgumentError("business_attribution", "is not valid")
	}

	return nil
}
