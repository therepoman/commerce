package get_bits_type

import (
	"context"

	"code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *petozi.GetBitsTypeReq) (*petozi.GetBitsTypeResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validation Validation        `inject:""`
	Getter     bits_types.Getter `inject:""`
}

func (a *api) Get(ctx context.Context, req *petozi.GetBitsTypeReq) (*petozi.GetBitsTypeResp, error) {
	err := a.Validation.IsValid(req)
	if err != nil {
		return nil, err
	}

	bitsType, err := a.Getter.Get(ctx, req.Id)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &petozi.GetBitsTypeResp{
		BitsType: bitsType,
	}, nil
}
