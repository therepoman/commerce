package get_bits_type

import (
	"context"
	"errors"
	"testing"

	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_Get(t *testing.T) {
	Convey("given a get bits product API", t, func() {
		getter := new(bits_types_mock.Getter)

		a := &api{
			Validation: NewValidation(),
			Getter:     getter,
		}

		ctx := context.Background()

		Convey("when the request is invalid", func() {
			_, err := a.Get(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &petozi.GetBitsTypeReq{
				Id: "walrusBits",
			}

			Convey("when we fail to get the bits type", func() {
				getter.On("Get", ctx, req.Id).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					_, err := a.Get(context.Background(), req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when we succeed to get the bits type", func() {
				getter.On("Get", ctx, req.Id).Return(&petozi.BitsType{
					Id: req.Id,
				}, nil)

				Convey("then we return the bits type", func() {
					resp, err := a.Get(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.BitsType, ShouldNotBeNil)
					So(resp.BitsType.Id, ShouldEqual, req.Id)
				})
			})
		})
	})
}
