package get_bits_type

import (
	"code.justin.tv/commerce/gogogadget/strings"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type Validation interface {
	IsValid(req *petozi.GetBitsTypeReq) error
}

func NewValidation() Validation {
	return &validation{}
}

type validation struct{}

func (v *validation) IsValid(req *petozi.GetBitsTypeReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request body", "is missing")
	}

	if strings.Blank(req.Id) {
		return twirp.InvalidArgumentError("id", "is missing")
	}

	return nil
}
