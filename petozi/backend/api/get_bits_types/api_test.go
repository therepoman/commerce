package get_bits_types

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/petozi/backend/bits_types"
	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetBitsTypes(t *testing.T) {
	Convey("GetBitsTypes", t, func() {
		mockGetter := new(bits_types_mock.Getter)

		api := &api{
			Getter: mockGetter,
		}

		ctx := context.Background()

		Convey("with a valid request", func() {

			req := &petozi.GetBitsTypesReq{}

			Convey("when getter retrives bits types", func() {
				mockGetter.On("GetAll", ctx).Return(bits_types.TwirpBitsTypes(), nil)

				resp, err := api.GetAll(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.BitsTypes, ShouldResemble, bits_types.TwirpBitsTypes())
			})

			Convey("when getter fails", func() {
				mockGetter.On("GetAll", ctx).Return(nil, errors.New("some error"))

				_, err := api.GetAll(ctx, req)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
