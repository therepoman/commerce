package get_bits_types

import (
	"context"

	"code.justin.tv/commerce/petozi/backend/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	GetAll(ctx context.Context, req *petozi.GetBitsTypesReq) (*petozi.GetBitsTypesResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Getter bits_types.Getter `inject:""`
}

func (a *api) GetAll(ctx context.Context, req *petozi.GetBitsTypesReq) (*petozi.GetBitsTypesResp, error) {
	types, err := a.Getter.GetAll(ctx)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &petozi.GetBitsTypesResp{
		BitsTypes: types,
	}, nil
}
