package reset_caches

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/cache/bits_products"
	"code.justin.tv/commerce/petozi/backend/cache/bits_types"
	products_dynamo "code.justin.tv/commerce/petozi/backend/dynamo/bits_products"
	types_dynamo "code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Reset(ctx context.Context, req *petozi.ResetCachesReq) (*petozi.ResetCachesResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	ProductsCache bits_products.Cache `inject:""`
	TypesCache    bits_types.Cache    `inject:""`
	ProductsDAO   products_dynamo.DAO `inject:""`
	TypesDAO      types_dynamo.DAO    `inject:""`
}

func (a *api) Reset(ctx context.Context, req *petozi.ResetCachesReq) (*petozi.ResetCachesResp, error) {
	a.ProductsCache.Blow()
	a.TypesCache.Blow()

	products, err := a.ProductsDAO.GetAll(ctx)
	if err != nil {
		logrus.WithError(err).Error("failed to fetch all products while reseting caches")
		return nil, twirp.InternalErrorWith(err)
	}
	a.ProductsCache.PutAll(products)

	types, err := a.TypesDAO.GetAll(ctx)
	if err != nil {
		logrus.WithError(err).Error("failed to fetch all types while reseting caches")
		return nil, twirp.InternalErrorWith(err)
	}
	a.TypesCache.PutAll(types)

	return &petozi.ResetCachesResp{}, nil
}
