package get_bits_product

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *petozi.GetBitsProductReq) (*petozi.GetBitsProductResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validation Validation           `inject:""`
	Getter     bits_products.Getter `inject:""`
}

func (a *api) Get(ctx context.Context, req *petozi.GetBitsProductReq) (*petozi.GetBitsProductResp, error) {
	err := a.Validation.IsValid(req)
	if err != nil {
		return nil, err
	}

	product, err := a.Getter.Get(ctx, req.Platform, req.Id)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"platform":  req.Platform,
			"productID": req.Id,
		}).WithError(err).Error("failed to get product")
		return nil, twirp.InternalErrorWith(err)
	}

	return &petozi.GetBitsProductResp{
		BitsProduct: product,
	}, nil
}
