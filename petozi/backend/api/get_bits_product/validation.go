package get_bits_product

import (
	"code.justin.tv/commerce/gogogadget/strings"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type Validation interface {
	IsValid(req *petozi.GetBitsProductReq) error
}

func NewValidation() Validation {
	return &validation{}
}

type validation struct{}

func (v *validation) IsValid(req *petozi.GetBitsProductReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "body is missing")
	}

	if strings.Blank(req.Id) {
		return twirp.InvalidArgumentError("id", "is missing")
	}

	_, ok := petozi.Platform_name[int32(req.Platform)]

	if !ok {
		return twirp.InvalidArgumentError("platform", "is not valid")
	}

	return nil
}
