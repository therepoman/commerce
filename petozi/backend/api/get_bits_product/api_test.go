package get_bits_product

import (
	"context"
	"errors"
	"testing"

	get_bits_product_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/api/get_bits_product"
	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_Get(t *testing.T) {
	Convey("given a get bits product API", t, func() {
		validation := new(get_bits_product_mock.Validation)
		getter := new(bits_products_mock.Getter)

		a := &api{
			Validation: validation,
			Getter:     getter,
		}

		req := &petozi.GetBitsProductReq{
			Platform: petozi.Platform_AMAZON,
			Id:       "walrusProduct",
		}

		ctx := context.Background()

		Convey("when the request is invalid", func() {
			validation.On("IsValid", req).Return(errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				_, err := a.Get(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is valid", func() {
			validation.On("IsValid", req).Return(nil)

			Convey("when we fail to get the product", func() {
				getter.On("Get", ctx, req.Platform, req.Id).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					_, err := a.Get(context.Background(), req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when we succeed to get the product", func() {
				getter.On("Get", ctx, req.Platform, req.Id).Return(&petozi.BitsProduct{
					Id:       req.Id,
					Platform: req.Platform,
				}, nil)

				Convey("then we return the product", func() {
					resp, err := a.Get(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.BitsProduct, ShouldNotBeNil)
					So(resp.BitsProduct.Platform, ShouldEqual, req.Platform)
					So(resp.BitsProduct.Id, ShouldEqual, req.Id)
				})
			})
		})
	})
}
