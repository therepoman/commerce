package get_bits_product

import (
	"testing"

	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidation_IsValid(t *testing.T) {
	Convey("given a put bits product API validator", t, func() {
		v := validation{}

		Convey("if the request body is nil we fail", func() {
			err := v.IsValid(nil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: request body is missing")
		})

		Convey("when we do have a request body", func() {
			req := &petozi.GetBitsProductReq{}

			Convey("when we do not have a product ID we fail", func() {
				err := v.IsValid(req)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: id is missing")
			})

			Convey("when we have a product ID", func() {
				req.Id = "walrusProduct"

				Convey("when the platform is invalid we fail", func() {
					req.Platform = petozi.Platform(-1)
					err := v.IsValid(req)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "twirp error invalid_argument: platform is not valid")
				})

				Convey("when the platform is valid we return nil", func() {
					req.Platform = petozi.Platform_AMAZON
					err := v.IsValid(req)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
