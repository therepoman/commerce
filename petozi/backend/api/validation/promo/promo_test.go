package promo

import (
	"testing"
	"time"

	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidation_IsValid(t *testing.T) {
	Convey("given a put bits product API validator", t, func() {
		v := NewValidation()

		Convey("when the promo is nil, it's valid so return nil", func() {
			err := v.IsValid(nil)
			So(err, ShouldBeNil)
		})

		Convey("when the promo is not nil", func() {
			promo := &petozi.Promo{}

			Convey("when the promo id is blank, we should fail", func() {
				err := v.IsValid(promo)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: promo id is missing")
			})

			Convey("when the promo id is not nil", func() {
				promo.Id = "walrusPromo"

				Convey("when the promo type is not valid, we should fail", func() {
					promo.Type = petozi.PromoType(-1)
					err := v.IsValid(promo)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "twirp error invalid_argument: promo type is not valid")
				})

				Convey("when the promo type is valid", func() {
					promo.Type = petozi.PromoType_FIRST_TIME_PURCHASE

					Convey("when the promo start is not valid, we should fail", func() {
						err := v.IsValid(promo)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "twirp error invalid_argument: promo start is missing")
					})

					Convey("when the promo start is valid", func() {
						startTime, _ := ptypes.TimestampProto(time.Now())
						promo.Start = startTime

						Convey("when the promo end is not valid, we should fail", func() {
							err := v.IsValid(promo)
							So(err, ShouldNotBeNil)
							So(err.Error(), ShouldEqual, "twirp error invalid_argument: promo end is missing")
						})

						Convey("when the promo end is valid", func() {
							endTime, _ := ptypes.TimestampProto(time.Now().Add(time.Hour * 5))
							promo.End = endTime

							err := v.IsValid(promo)
							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
