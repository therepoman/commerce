package promo

import (
	"code.justin.tv/commerce/gogogadget/strings"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type Validation interface {
	IsValid(promo *petozi.Promo) error
}

func NewValidation() Validation {
	return &validation{}
}

type validation struct{}

func (v *validation) IsValid(promo *petozi.Promo) error {
	// A nil promo is valid, since it means there is no promo configured.
	if promo == nil {
		return nil
	}

	if strings.Blank(promo.Id) {
		return twirp.InvalidArgumentError("promo id", "is missing")
	}

	_, ok := petozi.PromoType_name[int32(promo.Type)]
	if !ok {
		return twirp.InvalidArgumentError("promo type", "is not valid")
	}

	if promo.Start == nil {
		return twirp.InvalidArgumentError("promo start", "is missing")
	}

	if promo.End == nil {
		return twirp.InvalidArgumentError("promo end", "is missing")
	}

	return nil
}
