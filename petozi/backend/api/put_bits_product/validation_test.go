package put_bits_product

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/petozi/backend/api/validation/promo"
	"code.justin.tv/commerce/petozi/backend/dynamo"
	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products"
	bits_types_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestValidation_IsValid(t *testing.T) {
	Convey("given a put bits product API validator", t, func() {
		bitsTypesDAO := new(bits_types_mock.DAO)
		bitsProductGetter := new(bits_products_mock.Getter)

		v := validation{
			BitsTypesDAO:    bitsTypesDAO,
			ProductsGetter:  bitsProductGetter,
			PromoValidation: promo.NewValidation(),
		}

		ctx := context.Background()

		Convey("fail if the request is nil", func() {
			err := v.IsValid(ctx, nil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: request body is missing")
		})

		Convey("when the request body is not nil", func() {
			req := &petozi.PutBitsProductReq{}

			Convey("when there is no product to put we should fail", func() {
				err := v.IsValid(ctx, req)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "twirp error invalid_argument: new_bits_product is missing")
			})

			Convey("when the product struct is not nil", func() {
				req.NewBitsProduct = &petozi.BitsProduct{}

				Convey("when there is no product ID we should fail", func() {
					err := v.IsValid(ctx, req)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "twirp error invalid_argument: id is missing")
				})

				Convey("when the product ID is set", func() {
					req.NewBitsProduct.Id = "walrusProduct"

					Convey("when the platform is invalid we should fail", func() {
						req.NewBitsProduct.Platform = petozi.Platform(-1)
						err := v.IsValid(ctx, req)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldEqual, "twirp error invalid_argument: platform is not valid")
					})

					Convey("when the platform is set", func() {
						req.NewBitsProduct.Platform = petozi.Platform_AMAZON

						Convey("when the bits type is missing we should fail", func() {
							err := v.IsValid(ctx, req)
							So(err, ShouldNotBeNil)
							So(err.Error(), ShouldEqual, "twirp error invalid_argument: bits_type_id is missing")
						})

						Convey("when the bits type is set", func() {
							req.NewBitsProduct.BitsTypeId = "walrus"

							Convey("when we fail on looking up the bits type we should fail", func() {
								bitsTypesDAO.On("Get", ctx, req.NewBitsProduct.BitsTypeId).Return(nil, errors.New("WALRUS STRIKE"))
								err := v.IsValid(ctx, req)
								So(err, ShouldNotBeNil)
								So(err.Error(), ShouldEqual, "twirp error internal: WALRUS STRIKE")
							})

							Convey("when the bits type is missing we should fail", func() {
								bitsTypesDAO.On("Get", ctx, req.NewBitsProduct.BitsTypeId).Return(nil, nil)
								err := v.IsValid(ctx, req)
								So(err, ShouldNotBeNil)
								So(err.Error(), ShouldEqual, "twirp error invalid_argument: bits_type_id is not valid")
							})

							Convey("when the bits type is valid in the database", func() {
								bitsTypesDAO.On("Get", ctx, req.NewBitsProduct.BitsTypeId).Return(&dynamo.BitsType{ID: req.NewBitsProduct.Id}, nil)

								Convey("when the display name is invalid we should fail", func() {
									err := v.IsValid(ctx, req)
									So(err, ShouldNotBeNil)
									So(err.Error(), ShouldEqual, "twirp error invalid_argument: display_name is missing")
								})

								Convey("when the product ID is set", func() {
									req.NewBitsProduct.DisplayName = "Walrus Product"

									Convey("when the quantity is less than 1 we should fail", func() {
										err := v.IsValid(ctx, req)
										So(err, ShouldNotBeNil)
										So(err.Error(), ShouldEqual, "twirp error invalid_argument: quantity should be greater than 0")
									})

									Convey("when the quantity is set greater than 0", func() {
										req.NewBitsProduct.Quantity = 1

										Convey("when the max purchasable quantity is less than 1 we should fail", func() {
											err := v.IsValid(ctx, req)
											So(err, ShouldNotBeNil)
											So(err.Error(), ShouldEqual, "twirp error invalid_argument: max_purchasable_quantity should be greater than 0")
										})

										Convey("when the max purchasable quantity is set greater than 0", func() {
											req.NewBitsProduct.MaxPurchasableQuantity = 1

											Convey("when the promo is set and is invalid", func() {
												req.NewBitsProduct.Promo = &petozi.Promo{}
												err := v.IsValid(ctx, req)
												So(err, ShouldNotBeNil)
												So(err.Error(), ShouldEqual, "twirp error invalid_argument: promo id is missing")
											})

											Convey("when the promo is set and is valid", func() {
												req.NewBitsProduct.Promo = nil

												Convey("when the pricing id is invalid", func() {
													err := v.IsValid(ctx, req)
													So(err, ShouldNotBeNil)
													So(err.Error(), ShouldEqual, "twirp error invalid_argument: pricing_id is missing")
												})

												Convey("when the pricing id is valid", func() {
													req.NewBitsProduct.PricingId = "walrusPrice"

													Convey("when the pricing ID is not assigned a value from payments", func() {
														req.NewBitsProduct.PricingId = NoPricingIDValue
													})

													Convey("when the pricing ID is assigned a value from payments", func() {
														Convey("when we fail to get products", func() {
															bitsProductGetter.On("GetAll", ctx, false).Return(nil, errors.New("WALRUS STRIKE"))
															err := v.IsValid(ctx, req)
															So(err, ShouldNotBeNil)
															So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
														})

														Convey("when the pricing ID is used already", func() {
															bitsProductGetter.On("GetAll", ctx, false).Return([]*petozi.BitsProduct{
																{Id: "otherProduct", PricingId: "walrusPrice"},
															}, nil)
															err := v.IsValid(ctx, req)
															So(err, ShouldNotBeNil)
															So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
															So(err.Error(), ShouldEqual, "twirp error invalid_argument: pricing_id is already attached to existing product otherProduct")
														})

														Convey("when the pricing ID is used already, but it's the same product ID", func() {
															bitsProductGetter.On("GetAll", ctx, false).Return([]*petozi.BitsProduct{
																{Id: "walrusProduct", PricingId: "walrusPrice"},
															}, nil)

															Convey("then we should return valid", func() {
																err := v.IsValid(ctx, req)
																So(err, ShouldBeNil)
															})
														})

														Convey("when the pricing ID is not used", func() {
															bitsProductGetter.On("GetAll", ctx, false).Return([]*petozi.BitsProduct{
																{Id: "otherProduct", PricingId: "otherPrice"},
																{Id: "fooBarProduct", PricingId: "fooBarPrice"},
															}, nil)

															Convey("then we should return valid", func() {
																err := v.IsValid(ctx, req)
																So(err, ShouldBeNil)
															})
														})
													})
												})
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
