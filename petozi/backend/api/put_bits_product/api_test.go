package put_bits_product

import (
	"context"
	"errors"
	"testing"

	put_bits_product_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/api/put_bits_product"
	bits_products_mock "code.justin.tv/commerce/petozi/mocks/code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestApi_Put(t *testing.T) {
	Convey("given a put bits product API", t, func() {
		validation := new(put_bits_product_mock.Validation)
		creator := new(bits_products_mock.Creator)

		a := &api{
			Creator:    creator,
			Validation: validation,
		}

		newProduct := &petozi.BitsProduct{
			Id:                     "walrusProduct",
			Platform:               petozi.Platform_AMAZON,
			PricingId:              "walrusPriceID",
			BitsTypeId:             "walrusBitsType",
			DisplayName:            "Walrus Product",
			Quantity:               1,
			MaxPurchasableQuantity: 1,
		}

		req := &petozi.PutBitsProductReq{
			NewBitsProduct: newProduct,
		}

		ctx := context.Background()

		Convey("when validation fails, we return an error", func() {
			validation.On("IsValid", ctx, req).Return(errors.New("WALRUS STRIKE"))

			_, err := a.Put(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("when the validation succeeds", func() {
			validation.On("IsValid", ctx, req).Return(nil)

			Convey("when the creator fails, we return the error", func() {
				creator.On("LetThereBeLight", ctx, newProduct).Return(errors.New("WALRUS STRIKE"))

				_, err := a.Put(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("when the creator succeeds", func() {
				creator.On("LetThereBeLight", ctx, newProduct).Return(nil)

				resp, err := a.Put(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})
	})
}
