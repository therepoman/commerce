package put_bits_product

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/backend/bits_products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Put(ctx context.Context, req *petozi.PutBitsProductReq) (*petozi.PutBitsProductResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Validation Validation            `inject:""`
	Creator    bits_products.Creator `inject:""`
}

func (a *api) Put(ctx context.Context, req *petozi.PutBitsProductReq) (*petozi.PutBitsProductResp, error) {
	err := a.Validation.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	err = a.Creator.LetThereBeLight(ctx, req.NewBitsProduct)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"product": req.NewBitsProduct,
		}).WithError(err).Error("could not create new bits product")
		return nil, twirp.InternalErrorWith(err)
	}

	return &petozi.PutBitsProductResp{}, nil
}
