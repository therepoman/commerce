package put_bits_product

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/petozi/backend/api/validation/promo"
	"code.justin.tv/commerce/petozi/backend/bits_products"
	"code.justin.tv/commerce/petozi/backend/dynamo/bits_types"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

const (
	MaxBitsProductIDLength = 50
	NoPricingIDValue       = "NO_PRICING_ID"
)

type Validation interface {
	IsValid(ctx context.Context, req *petozi.PutBitsProductReq) error
}

func NewValidation() Validation {
	return &validation{}
}

type validation struct {
	BitsTypesDAO    bits_types.DAO       `inject:""`
	ProductsGetter  bits_products.Getter `inject:""`
	PromoValidation promo.Validation     `inject:""`
}

func (v *validation) IsValid(ctx context.Context, req *petozi.PutBitsProductReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "body is missing")
	}

	if req.NewBitsProduct == nil {
		return twirp.InvalidArgumentError("new_bits_product", "is missing")
	}

	if strings.Blank(req.NewBitsProduct.Id) {
		return twirp.InvalidArgumentError("id", "is missing")
	}

	if len(req.NewBitsProduct.Id) > MaxBitsProductIDLength {
		return twirp.InvalidArgumentError("id", fmt.Sprintf("must be less than %d characters", MaxBitsProductIDLength))
	}

	_, ok := petozi.Platform_name[int32(req.NewBitsProduct.Platform)]

	if !ok {
		return twirp.InvalidArgumentError("platform", "is not valid")
	}

	if strings.Blank(req.NewBitsProduct.BitsTypeId) {
		return twirp.InvalidArgumentError("bits_type_id", "is missing")
	}

	bitsType, err := v.BitsTypesDAO.Get(ctx, req.NewBitsProduct.BitsTypeId)
	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	if bitsType == nil {
		return twirp.InvalidArgumentError("bits_type_id", "is not valid")
	}

	if strings.Blank(req.NewBitsProduct.DisplayName) {
		return twirp.InvalidArgumentError("display_name", "is missing")
	}

	if req.NewBitsProduct.Quantity < 1 {
		return twirp.InvalidArgumentError("quantity", "should be greater than 0")
	}

	if req.NewBitsProduct.MaxPurchasableQuantity < 1 {
		return twirp.InvalidArgumentError("max_purchasable_quantity", "should be greater than 0")
	}

	err = v.PromoValidation.IsValid(req.NewBitsProduct.Promo)
	if err != nil {
		return err
	}

	if strings.Blank(req.NewBitsProduct.PricingId) {
		return twirp.InvalidArgumentError("pricing_id", "is missing")
	}

	if req.NewBitsProduct.PricingId != NoPricingIDValue {
		products, err := v.ProductsGetter.GetAll(ctx, false)
		if err != nil {
			return twirp.InternalErrorWith(err)
		}

		for _, product := range products {
			// we should error when the pricing ID matches another product, and the product ID is not the same.
			if product.PricingId == req.NewBitsProduct.PricingId && product.Id != req.NewBitsProduct.Id {
				return twirp.InvalidArgumentError("pricing_id", fmt.Sprintf("is already attached to existing product %v", product.Id))
			}
		}
	}

	return nil
}
