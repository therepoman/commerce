package bits_products

import (
	"context"
	"fmt"

	db "code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

const (
	tableFormat = "bits-products-%s"

	hashKey  = "platform"
	rangeKey = "productId"
)

type DAO interface {
	Get(ctx context.Context, platform petozi.Platform, productID string) (*db.BitsProduct, error)
	GetAll(ctx context.Context) ([]*db.BitsProduct, error)
	Create(ctx context.Context, bitsProduct *db.BitsProduct) error
	Update(ctx context.Context, bitsProduct *db.BitsProduct) error
}

func NewDAO(sess *session.Session) DAO {
	return &dao{
		Session: dynamo.New(sess),
	}
}

type dao struct {
	Session *dynamo.DB
	Config  *config.Config `inject:""`
}

func (d *dao) Create(ctx context.Context, bitsProduct *db.BitsProduct) error {
	return d.Session.Table(d.tableName()).Put(bitsProduct).If("attribute_not_exists(productId)").RunWithContext(ctx)
}

func (d *dao) Update(ctx context.Context, bitsProduct *db.BitsProduct) error {
	return d.Session.Table(d.tableName()).Put(bitsProduct).RunWithContext(ctx)
}

func (d *dao) Get(ctx context.Context, platform petozi.Platform, productID string) (*db.BitsProduct, error) {
	var product db.BitsProduct

	err := d.Session.Table(d.tableName()).
		Get(hashKey, platform).
		Range(rangeKey, dynamo.Equal, productID).
		OneWithContext(ctx, &product)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &product, err
}

func (d *dao) GetAll(ctx context.Context) ([]*db.BitsProduct, error) {
	var products []*db.BitsProduct

	err := d.Session.Table(d.tableName()).
		Scan().
		AllWithContext(ctx, &products)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return products, err
}

func (d *dao) tableName() string {
	return fmt.Sprintf(tableFormat, d.Config.Environment.DynamoSuffix)
}
