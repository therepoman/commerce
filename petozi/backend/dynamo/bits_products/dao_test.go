package bits_products

import (
	"context"
	"testing"

	"code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/config"
	"code.justin.tv/commerce/petozi/config/environment"
	petozi "code.justin.tv/commerce/petozi/rpc"
	db "github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDao_Operations(t *testing.T) {
	Convey("given a bits products dao", t, func() {
		session := dynamo.GetAWSSession(t)
		dao := &dao{
			Session: db.New(session),
			Config: &config.Config{
				Environment: &environment.Config{
					DynamoSuffix: dynamo.TestTablesSuffix,
				},
			},
		}

		ctx := context.Background()

		Convey("we should return nil for a non existent bits product", func() {
			bitsType, err := dao.Get(ctx, petozi.Platform_AMAZON, "random")
			So(err, ShouldNotBeNil)
			So(bitsType, ShouldBeNil)
		})
	})
}
