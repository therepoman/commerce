package dynamo

import (
	"time"

	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/golang/protobuf/ptypes"
)

// The Bits Type table model, which stores information about bits types.
type BitsType struct {
	ID                    string                       `dynamo:"id"`                    // The bits type ID, it's the hash key.
	DisplayName           string                       `dynamo:"displayName"`           // The display name of the bits type. It's human readable.
	IsPaid                bool                         `dynamo:"paid"`                  // Flag that tells us if this bits type can be purchased.
	IsPromotional         bool                         `dynamo:"promotional"`           // Flag that tells us if this bits type is promotional.
	CostPerBitsUSC        float64                      `dynamo:"costPerBitsUSC"`        // The cost per bits that is determined in US cents.
	EntitlementSourceType petozi.EntitlementSourceType `dynamo:"entitlementSourceType"` // The entitlement source of the bits type. It's like where it's entitled from within Twitch.
	SpendOrder            int                          `dynamo:"spendOrder"`            // Deprecated field from FABS about the order to spend the bits.
	BusinessAttribution   petozi.BusinessAttribution   `dynamo:"businessAttribution"`   // The business attribution of the bits type.
}

// The Bits Products table model, which stores information about bits products.
type BitsProduct struct {
	Platform               petozi.Platform `dynamo:"platform"`               // The plaform this is for, normally it's the payment platform or PHANTO for key types
	ProductID              string          `dynamo:"productId"`              // The product ID, which is unique across the platform.
	PricingID              string          `dynamo:"pricingId"`              // The pricing ID, which we use to get info from Flo.
	DisplayName            string          `dynamo:"displayName"`            // The display name, which is the human readable thing for displaying what this is.
	BitsType               string          `dynamo:"bitsType"`               // The bits type that's associated with this product.
	Quantity               int             `dynamo:"quantity"`               // The quantity of Bits that this product entitles.
	MaxQuantity            int             `dynamo:"maxQuantity"`            // The maximum quantity that can be acquired in one transaction.
	Promo                  *Promo          `dynamo:"promo"`                  // Promotion information about the product. Nil when it's not set.
	IsShownWhenLoggedOut   bool            `dynamo:"shownWhenLoggedOut"`     // Flag that tells us if we should display this product when we have no logged in user.
	OfferID                string          `dynamo:"offerId"`                // A payments owned offer mapping
	RestrictedToCurrencies []string        `dynamo:"restrictedToCurrencies"` // Allowlist of currencies able to purchase this product.
}

func (b *BitsProduct) ToTwirpBitsProduct() *petozi.BitsProduct {
	p := &petozi.BitsProduct{
		Id:                     b.ProductID,
		Platform:               b.Platform,
		PricingId:              b.PricingID,
		BitsTypeId:             b.BitsType,
		DisplayName:            b.DisplayName,
		Quantity:               uint64(b.Quantity),
		MaxPurchasableQuantity: uint32(b.MaxQuantity),
		ShowWhenLoggedOut:      b.IsShownWhenLoggedOut,
		OfferId:                b.OfferID,
		RestrictedToCurrencies: b.RestrictedToCurrencies,
	}

	if b.Promo != nil {
		p.Promo = b.Promo.ToTwirpPromo()
	}

	return p
}

func ToDynamoBitsProduct(product *petozi.BitsProduct) *BitsProduct {
	p := &BitsProduct{
		Platform:               product.Platform,
		ProductID:              product.Id,
		BitsType:               product.BitsTypeId,
		DisplayName:            product.DisplayName,
		IsShownWhenLoggedOut:   product.ShowWhenLoggedOut,
		PricingID:              product.PricingId,
		Quantity:               int(product.Quantity),
		MaxQuantity:            int(product.MaxPurchasableQuantity),
		Promo:                  ToDynamoPromo(product.Promo),
		OfferID:                product.OfferId,
		RestrictedToCurrencies: product.RestrictedToCurrencies,
	}

	return p
}

// The Promo struct, which contains information about promotional products.
type Promo struct {
	ID        string           `dynamo:"id"`    // The promotion ID, which identifies what the promo is.
	Type      petozi.PromoType `dynamo:"type"`  // The type of promo this is, like if it's single purchase.
	Start     time.Time        `dynamo:"start"` // The time the promo starts, if it has a start date.
	End       time.Time        `dynamo:"end"`   // The time the promo ends, if it has an end date.
	Whitelist *[]string        `dynamo:"whitelist"`
}

func (p *Promo) ToTwirpPromo() *petozi.Promo {
	start, _ := ptypes.TimestampProto(p.Start)

	end, _ := ptypes.TimestampProto(p.End)

	var whitelist []string
	if p.Whitelist != nil {
		whitelist = *p.Whitelist
	}

	return &petozi.Promo{
		Id:             p.ID,
		Type:           p.Type,
		Start:          start,
		End:            end,
		WhitelistedIds: whitelist,
	}
}

func ToDynamoPromo(promo *petozi.Promo) *Promo {
	if promo == nil {
		return nil
	}

	p := &Promo{
		ID:        promo.Id,
		Type:      promo.Type,
		Whitelist: &promo.WhitelistedIds,
	}

	var startErr error
	var start, end time.Time
	if promo.Start != nil {
		start, startErr = ptypes.Timestamp(promo.Start)
		if startErr == nil {
			p.Start = start
		}
	}

	var endErr error
	if promo.End != nil {
		end, endErr = ptypes.Timestamp(promo.End)
		if endErr == nil {
			p.End = end
		}
	}

	return p
}

func ToDynamoBitsType(bitsType *petozi.BitsType) *BitsType {
	return &BitsType{
		ID:                    bitsType.Id,
		DisplayName:           bitsType.DisplayName,
		IsPaid:                bitsType.IsPaid,
		IsPromotional:         bitsType.IsPromotional,
		CostPerBitsUSC:        bitsType.CostPerBitsUsc,
		EntitlementSourceType: bitsType.EntitlementSourceType,
		SpendOrder:            int(bitsType.SpendOrder),
		BusinessAttribution:   bitsType.BusinessAttribution,
	}
}

func FromDynamoBitsType(bitsType *BitsType) *petozi.BitsType {
	return &petozi.BitsType{
		Id:                    bitsType.ID,
		DisplayName:           bitsType.DisplayName,
		IsPaid:                bitsType.IsPaid,
		IsPromotional:         bitsType.IsPromotional,
		CostPerBitsUsc:        bitsType.CostPerBitsUSC,
		EntitlementSourceType: bitsType.EntitlementSourceType,
		SpendOrder:            uint32(bitsType.SpendOrder),
		BusinessAttribution:   bitsType.BusinessAttribution,
	}
}
