package bits_types

import (
	"context"
	"fmt"

	db "code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

const (
	tableFormat = "bits-types-%s"

	hashKey = "id"
)

type DAO interface {
	Get(ctx context.Context, id string) (*db.BitsType, error)
	GetAll(ctx context.Context) ([]*db.BitsType, error)
	Create(ctx context.Context, bitsType *db.BitsType) error
	Update(ctx context.Context, bitsType *db.BitsType) error
}

type dao struct {
	Session *dynamo.DB
	Config  *config.Config `inject:""`
}

func NewDAO(sess *session.Session) DAO {
	return &dao{
		Session: dynamo.New(sess),
	}
}

func (d *dao) Get(ctx context.Context, id string) (*db.BitsType, error) {
	var bitsType db.BitsType

	err := d.Session.Table(d.tableName()).
		Get(hashKey, id).
		OneWithContext(ctx, &bitsType)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &bitsType, err
}

func (d *dao) GetAll(ctx context.Context) ([]*db.BitsType, error) {
	var bitsTypes []*db.BitsType

	err := d.Session.Table(d.tableName()).
		Scan().
		AllWithContext(ctx, &bitsTypes)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return bitsTypes, err
}

func (d *dao) Create(ctx context.Context, bitsType *db.BitsType) error {
	return d.Session.Table(d.tableName()).Put(bitsType).If("attribute_not_exists(id)").RunWithContext(ctx)
}

func (d *dao) Update(ctx context.Context, bitsType *db.BitsType) error {
	return d.Session.Table(d.tableName()).Put(bitsType).RunWithContext(ctx)
}

func (d *dao) tableName() string {
	return fmt.Sprintf(tableFormat, d.Config.Environment.DynamoSuffix)
}
