package bits_types

import (
	"context"
	"testing"

	"code.justin.tv/commerce/petozi/backend/dynamo"
	"code.justin.tv/commerce/petozi/config"
	"code.justin.tv/commerce/petozi/config/environment"
	db "github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDao_Operations(t *testing.T) {
	Convey("given a bits types dao", t, func() {
		session := dynamo.GetAWSSession(t)
		dao := &dao{
			Session: db.New(session),
			Config: &config.Config{
				Environment: &environment.Config{
					DynamoSuffix: dynamo.TestTablesSuffix,
				},
			},
		}

		ctx := context.Background()

		Convey("we should return nil for a non existent bit type", func() {
			bitsType, err := dao.Get(ctx, "random")
			So(err, ShouldNotBeNil)
			So(bitsType, ShouldBeNil)
		})
	})
}
