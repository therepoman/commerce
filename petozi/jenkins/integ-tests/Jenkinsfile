import groovy.json.JsonSlurper
import groovy.json.JsonOutput

void setBuildStatus(String repo, String message, String state) {
    step([
        $class: "GitHubCommitStatusSetter",
        reposSource: [$class: "ManuallyEnteredRepositorySource", url: "https://git.xarth.tv/${repo}"],
        contextSource: [$class: "ManuallyEnteredCommitContextSource", context: "ci/jenkins/build-status"],
        errorHandlers: [[$class: "ChangingBuildStatusErrorHandler", result: "UNSTABLE"]],
        statusResultSource: [ $class: "ConditionalStatusResultSource", results: [[$class: "AnyBuildResult", message: message, state: state]] ]
    ]);
}

pipeline {
    agent any

    parameters {
        string(name: 'GIT_COMMIT', defaultValue: '', description: 'Git Sha', )
        string(name: 'ENVIRONMENT', defaultValue: '', description: 'Environment', )
        string(name: 'SKADI_ID', defaultValue: '', description: 'Environment', )
    }

    options {
        disableConcurrentBuilds()
        timeout(time: 8, unit: 'MINUTES')
        ansiColor('xterm')
        timestamps()
    }

    environment {
        REPO = "commerce/petozi"
        REGION = "us-west-2"
        ENVIRONMENT = "${params.ENVIRONMENT}"
    }

    stages {
        stage("Run Integration Tests") {
            when {
                expression { params.ENVIRONMENT == 'staging' }
            }

            steps {
                sh 'docker build -f Dockerfile.ci -t builder .'
                sh "docker run -e ENVIRONMENT=$ENVIRONMENT -t builder make go_integration_tests"
            }

            post {
                success {
                    setBuildStatus(REPO, "Integration Test Run Successful", "SUCCESS")
                }

                failure {
                    setBuildStatus(REPO, "Integration Test Run Failed", "FAILURE")
                }
            }
        }
    }
}