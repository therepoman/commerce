package config

import (
	"fmt"
	"io/ioutil"
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/petozi/config/auth"
	"code.justin.tv/commerce/petozi/config/cloudwatch"
	"code.justin.tv/commerce/petozi/config/environment"
	"code.justin.tv/commerce/petozi/config/redis"
	"github.com/go-yaml/yaml"
)

const (
	LocalConfigFilePath            = "/src/code.justin.tv/commerce/petozi/config/data/%s.yaml"
	GlobalConfigFilePath           = "/etc/petozi/config/%s.yaml"
	EnvironmentEnvironmentVariable = "ENVIRONMENT"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")
	Local     = Environment("local")
	Staging   = Environment("staging")
	Prod      = Environment("prod")
	Default   = Local
)

type Config struct {
	// Local variable to store env type we have
	environment Environment

	// The configuration of the environment, like name / region
	Environment *environment.Config `yaml:"environment"`

	// The configuration for things like sandstorm, S2S, etc.
	Auth *auth.Config `yaml:"auth"`

	// Configuration for cloudwatch metrics.
	CloudwatchMetrics *cloudwatch.Config `yaml:"cloudwatch"`

	// Configuration for setting up redis.
	Redis *redis.Config `yaml:"redis"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func GetOrDefaultEnvironment(env string) Environment {
	if !isValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}

func (c *Config) GetEnvironment() Environment {
	return c.environment
}

func isValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !isValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}

	cfg, err := loadConfig(filePath)
	if err != nil {
		return nil, err
	}

	cfg.environment = env

	return cfg, err
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}
