package environment

type Config struct {
	Name            string `yaml:"name"`
	LongName        string `yaml:"long-name"`
	AWSRegion       string `yaml:"aws-region"`
	DynamoSuffix    string `yaml:"dynamo-suffix"`
	Endpoint        string `yaml:"endpoint"`
	WorkersDisabled bool   `yaml:"workers-disabled"`
}
