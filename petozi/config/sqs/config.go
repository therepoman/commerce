package sqs

type Config struct {
	Name       string `yaml:"name"`
	NumWorkers int    `yaml:"num-workers"`
}
