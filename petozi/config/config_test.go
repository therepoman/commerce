package config

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLoadConfig(t *testing.T) {
	Convey("The unit test config is loadable", t, func() {
		cfg, err := LoadConfig(UnitTest)
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		So(cfg.Environment.Name, ShouldEqual, "unit-test")
	})
}
