package auth

type Config struct {
	Sandstorm          *Sandstorm `yaml:"sandstorm"`
	S2S                *S2S       `yaml:"s2s-service-name"`
	CartmanAuthEnabled bool       `yaml:"cartman-auth-enabled"`
}

type Sandstorm struct {
	RoleARN      string `yaml:"role-arn"`
	ECCPublicKey string `yaml:"ecc-public-key"`
}

type S2S struct {
	ServiceName string `yaml:"service-name"`
	IsEnabled   bool   `yaml:"enabled"`
}
