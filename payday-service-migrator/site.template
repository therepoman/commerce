upstream existing_service {
    jdomain ${NEW_HOSTNAME} port=80 max_ips=32 interval=30;
    keepalive 32;
}

server {
    listen 80;
    resolver ${DNS_HOST};

    error_log /usr/local/nginx/logs/existing/error.log;

    proxy_http_version 1.1;
    proxy_set_header   Connection "";

    proxy_connect_timeout 3s;
    proxy_send_timeout    1s;
    proxy_read_timeout    15s;
    proxy_buffering off; # The client is always an ELB so buffering responses should not be necessary
    keepalive_requests 1000000; # ELB doesn't need to keep re-establishing connections

    proxy_pass_request_headers on;
    proxy_redirect  off;

    client_max_body_size 30M; # Set high to avoid weird issues

    location / {
        proxy_pass http://existing_service;
        break;
    }
}
