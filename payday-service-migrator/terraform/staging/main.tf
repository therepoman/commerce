variable "service" { default = "payday" }
variable "profile" {}
variable "region" {}
variable "environment" {}
variable "asg_min_size" {}
variable "asg_max_size" {}
variable "instance_type" {}
variable "deploy_batch_size_type" {}
variable "deploy_batch_size" {}
variable "deploy_timeout" {}

provider "aws" {
    profile = "${var.profile}"
    region = "${var.region}"
}

resource "aws_iam_role" "app" {
  name = "${var.service}-${var.environment}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

module "beanstalk" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/identity/tf_beanstalk.git//environment_single"
  env = "${var.environment}"
  vpc_id = "vpc-4cfeb629"
  ec2_subnet_ids = "subnet-3dc3c44a,subnet-ada8f5f4,subnet-0e86646a"
  elb_subnet_ids = "subnet-3dc3c44a,subnet-ada8f5f4,subnet-0e86646a"
  elb_loadbalancer_security_groups = "sg-e56db982"
  auto_scaling_lc_security_groups = "sg-61bcd905"
  asg_min_size = "${var.asg_min_size}"
  asg_max_size = "${var.asg_max_size}"
  iam_role_id = "${aws_iam_role.app.id}"
  iam_role_arn = "${aws_iam_role.app.arn}"
  app_name = "${var.service}"
  instance_type = "${var.instance_type}"
  aws_profile = "${var.profile}"
  aws_region = "${var.region}"
  solution_stack_name = "64bit Amazon Linux 2017.09 v2.8.1 running Docker 17.06.2-ce"
  deploy_batch_size_type = "${var.deploy_batch_size_type}"
  deploy_batch_size = "${var.deploy_batch_size}"
  deploy_timeout = "${var.deploy_timeout}"
  service = "com/payday-migrator"
  healthcheck_url = "/health-check"
}
