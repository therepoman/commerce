# Setup SSH access to EC2 instances based on LDAP group
files:
  "/etc/sudoers.d/twitch-ldap" :
    mode: "440"
    owner: root
    group: root
    content: |
      %infra ALL=(ALL) NOPASSWD:ALL

  "/etc/ldap.conf" :
    mode: "444"
    owner: root
    group: root
    content: |
      base dc=justin,dc=tv
      uri ldaps://ldap-vip.internal.justin.tv/
      ldap_version 3
      pam_password md5
      TLS_REQCERT never

  "/usr/local/bin/ssh-ldap-pubkey-wrapper-wrapper" :
    mode: "755"
    owner: root
    group: root
    content: |
      #!/bin/sh
      # Adds the local bin to path and calls the wrapper
      export PATH=/usr/local/bin:$PATH
      /usr/local/bin/ssh-ldap-pubkey-wrapper $@

packages:
  yum:
    openldap-clients: []
    openldap-devel: []
    nss-pam-ldapd: []
    gcc: []
  python:
    ssh-ldap-pubkey: []

commands:
  01-setup_authconfig:
    command: authconfig --enableldap --enableldapauth --ldapserver='ldaps://ldap-vip.internal.justin.tv' --ldapbasedn='dc=justin,dc=tv' --update

  02-backup_nslcd_config:
    command: sed -i.bak '/^ssl /d' /etc/nslcd.conf

  03-update_nslcd_config:
    command: echo 'TLS_REQCERT never' >> /etc/nslcd.conf

  04-reload_nslcd:
    command: service nslcd restart

  05-update_openldap_config:
    command: echo 'TLS_REQCERT never' >> /etc/openldap/ldap.conf

  06-update_authconfig:
    command: authconfig --enablemkhomedir --update

  07-enable_ssh_password_auth:
    command: sed -i.bak 's/^PasswordAuthentication .*/PasswordAuthentication yes/' /etc/ssh/sshd_config

  08-increase_ssh_max_tries:
    command: sed -i.bak 's/^#MaxAuthTries.*/MaxAuthTries 10/g' /etc/ssh/sshd_config

  09-remove_auth_keys_config:
    command: sed '/AuthorizedKeysCommand/d' /etc/ssh/sshd_config

  10-enable_ldap_pubkey_lookup:
    command: echo -e '\nAuthorizedKeysCommand /usr/local/bin/ssh-ldap-pubkey-wrapper-wrapper\nAuthorizedKeysCommandUser nobody' >> /etc/ssh/sshd_config

  11-reload_sshd:
    command: service sshd reload
