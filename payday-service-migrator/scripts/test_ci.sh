#!/bin/bash

MIGRATOR_NAME=com-payday-migrator.dev.us-west2.justin.tv

if [[ "$ENVIRONMENT" == "prod" ]]; then
    MIGRATOR_NAME=new-new-com-payday-migrator.prod.us-west2.justin.tv
fi

./test_env.sh "$MIGRATOR_NAME"
