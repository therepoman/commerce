#!/bin/bash -x

SERVICE_NAME=$1
PREVIOUS=$(cat previous.txt)

echo "ROLLING BACK ${PREVIOUS}"

if [[ -z $PREVIOUS ]]; then echo "no previous version found in previous.txt"; exit 1; fi
if [[ $PREVIOUS == "None" ]]; then echo "None previous version found in previous.txt"; exit 1; fi 

# Ensure environment is ready
i="0"

while [ $i -lt 6 ]; do
    if eb status ${SERVICE_NAME} | grep -q "Ready"; then
        eb deploy ${SERVICE_NAME} --version "$PREVIOUS" --timeout 15; exit 1
    fi

    (( i++ ))
    echo "Waiting for environment to be ready..."
    sleep 15
done

echo "Environment failed to become READY to rollback. Rollback failed."
exit 1
