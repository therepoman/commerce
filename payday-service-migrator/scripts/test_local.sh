#!/bin/bash -e

echo "verifying .ebextensions"
ruby -e "require 'yaml'; Dir.foreach('./.ebextensions/') do |fname| next if fname == '.' or fname == '..'; YAML.load_file('./.ebextensions/'+fname) end"

echo "verifying nginx build"

docker-compose build
docker-compose up -d

sleep 3

#./scripts/test_env.sh "localhost:7781"
read -p "Enter a character" -n 1

docker-compose stop
docker-compose rm -f
