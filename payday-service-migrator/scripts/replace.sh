#!/bin/bash

docker run --rm -v $PWD:/home -w /home ubuntu /bin/bash -c "find . -type f | grep -v \"\.terraform\" | grep -v \"\.git\" | xargs sed -f sed.txt -i"
