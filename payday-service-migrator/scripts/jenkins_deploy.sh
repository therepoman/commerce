#!/bin/bash -ex

function eb-deploy() {
  APP=$1

  rm -f previous.txt

  eb status $APP | sed -e '/^  Deployed/!d' -e 's/^  Deployed Version: //' > previous.txt

  rm -f failed

  eb deploy $APP --label "$LABEL" --timeout 10 || touch failed

  ./test_ci.sh || touch failed

  if [[ -e failed ]]; then
    ./rollback.sh $APP;
  fi
}

export REGION="us-west-2"
set +x
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY
export PYTHONUNBUFFERED=1
set -x
export IMAGE=docker-registry.internal.justin.tv/commerce/payday:$GIT_COMMIT
TIMESTAMP=$(TZ=US/Pacific date -u +"%Y-%m-%dT%H:%M:%SZ")
export LABEL="${TIMESTAMP:0:19}_${GIT_COMMIT:0:10}"

cat <<EOF > Dockerrun.aws.json
{
  "AWSEBDockerrunVersion": "1",
  "Image": {
    "Name": "$IMAGE",
    "Update": "false"
  },
  "Ports": [
    {
      "ContainerPort": "80"
    }
  ],
  "Volumes": [
    {
      "ContainerDirectory": "/var/app",
      "HostDirectory": "/var/app"
    },
    {
      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
    }
  ]
}
EOF

export APP_NAME="${ENVIRONMENT}-com-payday-migrator"
export SERVICE_NAME="${ENVIRONMENT}-com-payday-migrator-server"

mkdir -p .elasticbeanstalk
cat <<EOF > .elasticbeanstalk/config.yml
global:
  application_name: ${APP_NAME}
  default_region: ${REGION}
option_settings:
  - namespace: aws:elasticbeanstalk:command
    option_name: Timeout
    value: 1800

EOF

cp -r config/${ENVIRONMENT}/. .ebextensions/

eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region $REGION

eb-deploy "${SERVICE_NAME}"
