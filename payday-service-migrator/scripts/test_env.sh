#!/bin/bash -e

MIGRATOR_DNS=$1

curl -v "${MIGRATOR_DNS}/health-check" || (echo "failed migrator test" && exit 1)
