# nginx elb migrator

Assists in migrating traffic from one ELB to another. This was created to migrate users-service from twitch-web-aws to twitch-users-service-prod. The migration procedure is:

0. Follow the configuration information below to configure nginx and create infrastructure.
1. In batches, add these NGINX instances to your ELB and remove old instances from the ELB to increase traffic to your new stack.
3. After all traffic is migrated successfully, flip DNS.

## Configuration

Things that need to be modified to work with your service:

Create a new paydaysitory and copy these files in. It is not recommeneded to fork the repository because jenkins hooks may not work. Changes values in [sed.txt](./sed.txt) to match your desired values and run:

```bash
./scripts/replace.sh
```

### Manual steps

To spin up the infrastructure with terraform, make the following changes to staging and prod (prod links used):

- Change bucket, key, and profile in [backend](./terraform/staging/main.tf#L3-L6)
- Change bucket, key, and profile for [networking remote state](./terraform/staging/main.tf#L29-L31). If you don't want to use remote state for networking info, or can't, you can manually add them [here](./terraform/staging/main.tf#L60-L63)

To spin up terraform:

```bash
cd ./terraform/prod
terraform init
terraform plan
terraform apply
```

If you need the instances to listen on a custom port for your ELB, [add or change it here](./.ebextensions/nginx.config#L51).

## Questions

Reach out to @kevipike on slack.
