freeStyleJob('commerce-payday') {
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'commerce/payday-service-migrator', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell 'make ci'
    saveDeployArtifact 'commerce/payday', './artifact'
  }
}

[staging: [
    access_key: "payday-deployment-access-key",
    secret_key: "payday-deployment-secret-key"
],prod: [
    access_key: "payday-deployment-access-key",
    secret_key: "payday-deployment-secret-key"
]].each { environment, values ->
  job {
    name "commerce-payday-${environment}"
    using 'TEMPLATE-deploy-aws'
    wrappers {
        credentialsBinding {
          file('COURIERD_PRIVATE_KEY', 'courierd')
          file('AWS_CONFIG_FILE', 'aws_config')
          string 'AWS_ACCESS_KEY', "${values.access_key}"
          string 'AWS_SECRET_KEY', "${values.secret_key}"
        }
    }

    steps {
      downloadDeployArtifact "commerce/payday"
      shell 'rm *.manifest*'
      shell "./jenkins_deploy.sh"
    }
  }
}
