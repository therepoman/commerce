# Contents
This project contains the generated C# code for the protobuf schemas found in the [SoftwareDistributionSchemas](https://code.amazon.com/packages/SoftwareDistributionSchemas)

# Updating
To update the generated Manifest.cs file in generated-src:

1. Download protoc (the protobuf compiler) from the [Protobuf project site](https://developers.google.com/protocol-buffers/docs/downloads)
2. Download the manifest.proto protobuf schema file from the [above package](https://code.amazon.com/packages/SoftwareDistributionSchemas/blobs/mainline/--/proto/manifest.proto?raw=1)
3. Use the protoc executable to generate the C# code as describe [here](https://developers.google.com/protocol-buffers/docs/csharptutorial#compiling-your-protocol-buffers)

e.g.

```
C:\Users\micellis\Downloads\protoc-3.3.0-win32\bin\protoc.exe -I C:\Users\micellis\Downloads --csharp_out=C:\Users\micellis\Documents\src\TwitchApp\FuelMCP\Fuel.Manifest\generated-src C:\Users\micellis\Downloads\manifest.proto
```
