﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Amazon.Fuel.Common.Contracts;
using Microsoft.EntityFrameworkCore;
//using Xunit;

namespace SqliteEntityDbMigration.Tests
{
    public class MigrationTest
    {
        public static readonly string GameInstallInfoDbFilename_v0 = @"..\..\..\SampleDbs\test.installinfo.v0.sqlite";  // has 6 valid entries
        public static readonly string GameProductInfoDbFilename_v0 = @"..\..\..\SampleDbs\test.productinfo.v0.sqlite";  // has 20 valid entries

        // TODO : get xunit working
        public static class Assert
        {
            public static void True(bool val)
            {
                if (!val)
                {
                    var str = $"Test failed:\n{Environment.StackTrace}";
                    Console.Error.WriteLine(str);
                    Debug.Fail(str);
                }
            }

            public static void Equal(int a, int b) => True(a.Equals(b));
        }

        //[Fact]
        public void GameProductInfoMigration_CanCreateNewDb()
        {
            var dbFile = GetTempFileName();
            CreateNewDb<GameProductInfoDbContext>(dbFile);
            PopulateProductInfoDb(dbFile, entryCount: 25);
            ReadProductInfoDb(dbFile, expectedEntries: 25);
            File.Delete(dbFile);
        }

        //[Fact]
        public void GameInstallInfoMigration_CanCreateNewDb()
        {
            var dbFile = GetTempFileName();
            CreateNewDb<GameInstallInfoDbContext>(dbFile);
            PopulateInstallInfoDb(
                dbFile, 
                testCultures: new List<string>() { "en-US", "ar-SA" }, // test gregorian and non-gregorian
                entryCount: 25);
            ReadInstallInfoDb(dbFile, expectedEntries: 25);
            File.Delete(dbFile);
        }

        //[Fact]
        public void ProductInfoMigration_PassesThroughV1()
        {
            var dbFile = GetTempFileName();
            File.Copy(GameProductInfoDbFilename_v0, dbFile);
            MigrateExistingDb<GameProductInfoDbContext>(dbFile, expectedMigrations: 1);
            File.Delete(dbFile);
        }

        //[Fact]
        public void GameInstallInfoMigration_PassesThroughV1()
        {
            var dbFile = GetTempFileName();
            File.Copy(GameInstallInfoDbFilename_v0, dbFile);
            MigrateExistingDb<GameInstallInfoDbContext>(dbFile, expectedMigrations: 1);
            File.Delete(dbFile);
        }

        static string GetTempFileName() => Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".sqlite");

        void CreateNewDb<TDbContext>(string dbFile) where TDbContext : TestableDbContext, new()
        {
            Console.WriteLine($"Create DB... ({dbFile})");

            using (var db = new TDbContext())
            {
                db.DbFilename = dbFile;
                db.Database.Migrate();
            }

            Assert.True(File.Exists(dbFile));
        }

        void MigrateExistingDb<TDbContext>(string dbFile, int expectedMigrations) where TDbContext : TestableDbContext, new()
        {
            Console.WriteLine($"Migrate DB... ({dbFile})");

            using (var db = new TDbContext())
            {
                db.DbFilename = dbFile;
                if (File.Exists(dbFile))
                {
                    var migrations = db.Database.GetPendingMigrations().ToArray();
                    Console.WriteLine($"Pending migrations: '{string.Join(",", migrations)}'");
                    Assert.Equal(migrations.Length, expectedMigrations);
                    db.Database.Migrate();
                }
            }
        }

        private class TestData
        {
            public string Id;
            public DateTime Dt;
        }

        void PopulateInstallInfoDb(string dbFile, List<string> testCultures, int randomDayRange = 365, int entryCount = 25)
        {
            Console.WriteLine("Populate InstallInfo DB...");

            var testData = new List<TestData>();
            using (var db = new GameInstallInfoDbContext())
            {
                db.DbFilename = dbFile;
                for (int i = 0; i < entryCount; ++i)
                {
                    var culture = testCultures[i % testCultures.Count];
                    CultureInfo.CurrentCulture = new CultureInfo(culture);

                    var dt = DateTime.UtcNow.AddDays(-randomDayRange/2.0 + new Random().NextDouble() * randomDayRange);
                    testData.Add(new TestData() { Dt = dt, Id = Guid.NewGuid().ToString() });
                    db.DbSet.Add(new GameInstallInfo() { Id = testData.Last().Id, InstallDateAsDateTime = testData.Last().Dt});
                }

                db.SaveChanges();
            }

            Console.WriteLine("Testing populated data...");

            using (var db = new GameInstallInfoDbContext())
            {
                db.DbFilename = dbFile;
                var elements = db.DbSet.OrderBy(a => a.Id).ToList();
                var expected = testData.OrderBy(a => a.Id).ToList();
                Assert.Equal(elements.Count, expected.Count);
                for (int i = 0; i < elements.Count; ++i)
                {
                    Assert.True(expected[i].Id.Equals(elements[i].Id));

                    // test reading for all cultures
                    foreach (var culture in testCultures)
                    {
                        CultureInfo.CurrentCulture = new CultureInfo(culture);

                        // ensure a sub-second accuracy granularity for serialization
                        var delta = Math.Abs(expected[i].Dt.Subtract(elements[i].InstallDateAsDateTime).TotalSeconds);
                        Assert.True(delta < 1.0);
                    }
                }
            }
        }

        void PopulateProductInfoDb(string dbFile, int entryCount = 25)
        {
            Console.WriteLine("Populate ProductInfo DB...");

            var testData = new List<TestData>();
            using (var db = new GameProductInfoDbContext())
            {
                db.DbFilename = dbFile;
                for (int i = 0; i < entryCount; ++i)
                {
                    testData.Add(new TestData() { Dt = DateTime.UtcNow, Id = Guid.NewGuid().ToString() });
                    db.DbSet.Add(new GameProductInfo() { Id = testData.Last().Id });
                }

                db.SaveChanges();
            }

            using (var db = new GameProductInfoDbContext())
            {
                db.DbFilename = dbFile;
                var elements = db.DbSet.OrderBy(a => a.Id).ToList();
                var expected = testData.OrderBy(a => a.Id).ToList();
                Assert.Equal(elements.Count, expected.Count);
                for (int i = 0; i < elements.Count; ++i)
                {
                    Assert.True(expected[i].Id.Equals(elements[i].Id));
                }
            }

        }

        void ReadInstallInfoDb(string dbFile, int expectedEntries)
        {
            Console.WriteLine("Read InstallInfo DB...");
            using (var db = new GameInstallInfoDbContext())
            {
                db.DbFilename = dbFile;
                foreach (var gi in db.DbSet)
                {
                    Console.WriteLine(gi.Id);
                }

                Assert.Equal(db.DbSet.ToArray().Count(), expectedEntries);
            }
        }

        void ReadProductInfoDb(string dbFile, int expectedEntries)
        {
            Console.WriteLine("Read ProductInfo DB...");
            using (var db = new GameProductInfoDbContext())
            {
                db.DbFilename = dbFile;
                foreach (var gi in db.DbSet)
                {
                    Console.WriteLine(gi.Id);
                }

                Assert.Equal(db.DbSet.ToArray().Count(), expectedEntries);
            }
        }
    }
}
