﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameInstallInfo
{
    [DbContext(typeof(GameInstallInfoDbContext))]
    partial class GameInstallInfoDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.3");

            modelBuilder.Entity("Amazon.Fuel.Common.Contracts.GameInstallInfo", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("InstallDate");

                    b.Property<string>("InstallDirectory");

                    b.Property<string>("InstallVersion");

                    b.Property<string>("InstallVersionName");

                    b.Property<bool>("Installed");

                    b.Property<string>("LastKnownLatestVersion");

                    b.Property<string>("LastKnownLatestVersionTimestamp");

                    b.Property<string>("LastUpdated");

                    b.Property<string>("LastPlayed");

                    b.Property<string>("ProductAsin");

                    b.Property<string>("ProductTitle");

                    b.HasKey("Id");

                    b.ToTable("DbSet");
                });
        }
    }
}
