﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameInstallInfo
{
    public partial class GameInstallInfo_v1 : Migration
    {
        // See custom migrations
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Sqlite doesn't handle column mod (nullability), so we need to handle a custom table migration
            // to change our DateTimes to be nullable
            var migrationQueryStr = @"PRAGMA foreign_keys = 0;
 
              CREATE TABLE IF NOT EXISTS DbSet (
                Id                                        TEXT NOT NULL CONSTRAINT ""PK_DbSet"" PRIMARY KEY,
                InstallDate                               TEXT,
                InstallDirectory                          TEXT,
                InstallVersion                            TEXT,
                InstallVersionName                        TEXT,
                Installed                                 INTEGER NOT NULL,
                LastKnownLatestVersion                    TEXT,
                LastKnownLatestVersionTimestamp           TEXT,
                LastUpdated                               TEXT,
                LastPlayed                                TEXT,
                ProductAsin                               TEXT,
                ProductTitle                              TEXT
              );

              CREATE TABLE DbSet_temp AS SELECT *
                                            FROM DbSet;
              
              DROP TABLE DbSet;
              
              CREATE TABLE DbSet (
                Id                                        TEXT NOT NULL CONSTRAINT ""PK_DbSet"" PRIMARY KEY,
                InstallDate                               TEXT,
                InstallDirectory                          TEXT,
                InstallVersion                            TEXT,
                InstallVersionName                        TEXT,
                Installed                                 INTEGER NOT NULL,
                LastKnownLatestVersion                    TEXT,
                LastKnownLatestVersionTimestamp           TEXT,
                LastUpdated                               TEXT,
                LastPlayed                                TEXT,
                ProductAsin                               TEXT,
                ProductTitle                              TEXT
              );
              
              INSERT INTO DbSet 
              (
                Id,
                InstallDate,
                InstallDirectory,
                InstallVersion,
                InstallVersionName,
                Installed,
                LastKnownLatestVersion,
                LastKnownLatestVersionTimestamp,
                LastUpdated,
                LastPlayed,
                ProductAsin,
                ProductTitle
              )

              SELECT Id,
                InstallDate,
                InstallDirectory,
                InstallVersion,
                InstallVersionName,
                Installed,
                LastKnownLatestVersion,
                LastKnownLatestVersionTimestamp,
                LastUpdated,
                LastPlayed,
                ProductAsin,
                ProductTitle
              FROM DbSet_temp;
              
              DROP TABLE DbSet_temp;
              
              PRAGMA foreign_keys = 1;";

            migrationBuilder.Sql(migrationQueryStr);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
