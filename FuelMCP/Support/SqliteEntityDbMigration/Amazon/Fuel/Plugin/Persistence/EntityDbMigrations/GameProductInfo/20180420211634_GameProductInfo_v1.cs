﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameProductInfo
{
    public partial class GameProductInfo_v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Sqlite doesn't handle column drops, so we need to handle a custom table migration
            migrationBuilder.Sql(
                @"PRAGMA foreign_keys = 0;
 
              CREATE TABLE IF NOT EXISTS DbSet (
                Id                 TEXT NOT NULL CONSTRAINT ""PK_DbSet"" PRIMARY KEY,
                DateTime           TEXT,
                Background         TEXT,
                Background2        TEXT,
                IsDeveloper        INTEGER NOT NULL,
                ProductAsin        TEXT,
                ProductAsinVersion TEXT,
                ProductDescription TEXT,
                ProductDomain      TEXT,
                ProductIconUrl     TEXT,
                ProductIdStr       TEXT,
                ProductLine        TEXT,
                ProductPublisher   TEXT,
                ProductSku         TEXT,
                ProductTitle       TEXT,
                ScreenshotsJson    TEXT,
                State              TEXT,
                VideosJson         TEXT
              );

              CREATE TABLE DbSet_temp AS SELECT *
                                            FROM DbSet;
              
              DROP TABLE DbSet;
              
              CREATE TABLE DbSet (
                Id                 TEXT NOT NULL CONSTRAINT ""PK_DbSet"" PRIMARY KEY,
                DateTime           TEXT,
                Background         TEXT,
                Background2        TEXT,
                IsDeveloper        INTEGER NOT NULL,
                ProductAsin        TEXT,
                ProductAsinVersion TEXT,
                ProductDescription TEXT,
                ProductDomain      TEXT,
                ProductIconUrl     TEXT,
                ProductIdStr       TEXT,
                ProductLine        TEXT,
                ProductPublisher   TEXT,
                ProductSku         TEXT,
                ProductTitle       TEXT,
                ScreenshotsJson    TEXT,
                State              TEXT,
                VideosJson         TEXT
              );
              
              INSERT INTO DbSet 
              (
                Id,
                DateTime,
                Background,        
                Background2,       
                IsDeveloper,       
                ProductAsin,       
                ProductAsinVersion,
                ProductDescription,
                ProductDomain,
                ProductIconUrl,
                ProductIdStr,
                ProductLine,
                ProductPublisher,
                ProductSku,
                ProductTitle,
                ScreenshotsJson,
                State,
                VideosJson        
              )

              SELECT Id,
                DateTime,
                Background,        
                Background2,       
                IsDeveloper,       
                ProductAsin,       
                ProductAsinVersion,
                ProductDescription,
                ProductDomain,
                ProductIconUrl,
                ProductIdStr,
                ProductLine,
                ProductPublisher,
                ProductSku,
                ProductTitle,
                ScreenshotsJson,
                State,
                VideosJson        
              FROM DbSet_temp;
              
              DROP TABLE DbSet_temp;
              
              PRAGMA foreign_keys = 1;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
