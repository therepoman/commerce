﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SqliteEntityDbMigration.Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameProductInfo
{
    public partial class GameProductInfo_v0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbSet",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Background = table.Column<string>(nullable: true),
                    Background2 = table.Column<string>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false),
                    IsDeveloper = table.Column<bool>(nullable: false),
                    ProductAsin = table.Column<string>(nullable: true),
                    ProductAsinVersion = table.Column<string>(nullable: true),
                    ProductDescription = table.Column<string>(nullable: true),
                    ProductDomain = table.Column<string>(nullable: true),
                    ProductIconUrl = table.Column<string>(nullable: true),
                    ProductIdStr = table.Column<string>(nullable: true),
                    ProductLine = table.Column<string>(nullable: true),
                    ProductPublisher = table.Column<string>(nullable: true),
                    ProductSku = table.Column<string>(nullable: true),
                    ProductTitle = table.Column<string>(nullable: true),
                    ScreenshotsJson = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    VideosJson = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbSet", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbSet");
        }
    }
}
