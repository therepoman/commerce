﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Amazon.Fuel.Common.Contracts;

namespace SqliteEntityDbMigration.Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameInstallInfo
{
    [DbContext(typeof(GameInstallInfoDbContext))]
    [Migration("20180420211534_GameInstallInfo_v0")]
    partial class GameInstallInfo_v0
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.3");

            modelBuilder.Entity("Amazon.Fuel.Common.Contracts.GameInstallInfo", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("InstallDate");

                    b.Property<string>("InstallDirectory");

                    b.Property<string>("InstallVersion");

                    b.Property<string>("InstallVersionName");

                    b.Property<bool>("Installed");

                    b.Property<string>("LastKnownLatestVersion");

                    b.Property<DateTime>("LastKnownLatestVersionTimestamp");

                    b.Property<DateTime>("LastPlayed");

                    b.Property<DateTime>("LastUpdated");

                    b.Property<string>("ProductAsin");

                    b.Property<string>("ProductTitle");

                    b.HasKey("Id");

                    b.ToTable("DbSet");
                });
        }
    }
}
