﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SqliteEntityDbMigration.Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameInstallInfo
{
    public partial class GameInstallInfo_v0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbSet",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    InstallDate = table.Column<DateTime>(nullable: false),
                    InstallDirectory = table.Column<string>(nullable: true),
                    InstallVersion = table.Column<string>(nullable: true),
                    InstallVersionName = table.Column<string>(nullable: true),
                    Installed = table.Column<bool>(nullable: false),
                    LastKnownLatestVersion = table.Column<string>(nullable: true),
                    LastKnownLatestVersionTimestamp = table.Column<DateTime>(nullable: false),
                    LastPlayed = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    ProductAsin = table.Column<string>(nullable: true),
                    ProductTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbSet", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbSet");
        }
    }
}
