﻿using System;
using Equ;

namespace Amazon.Fuel.Common.Contracts
{
    public class ProductId : MemberwiseEquatable<ProductId>
    {
        public ProductId(string id) { this.Id = id; }
        public string Id { get; }

        public override String ToString()
        {
            return $"ProductId[{Id}]";
        }
    }
}