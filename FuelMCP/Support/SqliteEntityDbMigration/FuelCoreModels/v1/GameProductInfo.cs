﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Amazon.Fuel.Common.Utilities;
using Newtonsoft.Json;

namespace Amazon.Fuel.Common.Contracts
{
    // NOTE: see Support\SqliteEntityDbMigration\readme.txt when altering this object

    public class GameProductInfo : PersistentEntry
    {
        // Config
        public override string Type => "entitlement";
        public override string StorageContext => "GameProductInfo";

        // This is retained for rollback compatibility, and can safely be removed once this line is pushed to beta/prod
        private const string DefaultDateTime = "0001-01-01 00:00:00";
        public string DateTime { get; set; } = DefaultDateTime;

        // Const
        public static readonly string STATE_LIVE = "LIVE";

        public bool IsDeveloper { get; set; }

        //Catalog Data:
        public string ProductTitle { get; set; }
        public string ProductAsin { get; set; }
        public string ProductAsinVersion { get; set; }
        public ProductId ProductId => new ProductId(ProductIdStr);
        public string ProductDescription { get; set; }
        public string ProductDomain { get; set; }
        public string ProductIconUrl { get; set; }
        public string ProductLine { get; set; }
        public string ProductSku { get; set; }
        public string ProductPublisher { get; set; }
        public string State { get; set; }
        public string ProductIdStr { get; set; }
        public string Background { get; set; }
        public string Background2 { get; set; }

        public string VideosJson { get; set; }
        [NotMapped]
        public IList<string> Videos
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<string>>(VideosJson) ?? new List<string>();
                }
                catch (Exception e)
                {
                    FuelLog.Error(e, "[Game] Unable to parse JSON", new { VideosJson = VideosJson });
                    return new List<string>();
                }
            }
            set
            {
                VideosJson = JsonConvert.SerializeObject(value);
            }
        }

        public string ScreenshotsJson { get; set; }

        [NotMapped]
        public IList<string> Screenshots
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<string>>(ScreenshotsJson) ?? new List<string>();
                }
                catch (Exception e)
                {
                    FuelLog.Error(e, "[Game] Unable to parse JSON", new { ScreenshotsJson = ScreenshotsJson });
                    return new List<string>();
                }
            }
            set
            {
                ScreenshotsJson = JsonConvert.SerializeObject(value);
            }
        }
    }
}