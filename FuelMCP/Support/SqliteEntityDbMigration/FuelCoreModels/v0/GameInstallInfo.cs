﻿using System;
using System.IO;

namespace Amazon.Fuel.Common.Contracts
{
    public class GameInstallInfo : PersistentEntry
    {
        public DateTime LastKnownLatestVersionTimestamp { get; set; } = DateTime.MinValue;
        public string LastKnownLatestVersion { get; set; } = "";
        public DateTime LastUpdated { get; set; } = DateTime.MinValue;

        // Config
        public override string Type => "install";
        public override string StorageContext => "GameInstallInfo";

        //Install Status:
        public bool Installed { get; set; }
        public string InstallVersion { get; set; }
        public string InstallVersionName { get; set; }
        public string InstallDirectory { get; set; }
        public string LibraryDirectory => Directory.GetParent(InstallDirectory.TrimEnd(Path.PathSeparator)).FullName;

        public DateTime InstallDate { get; set; }
        public DateTime LastPlayed { get; set; }

        public ProductId ProductId => new ProductId(Id);

        // NOTE: these may be null in older schemas
        public string ProductTitle { get; set; } = null;
        public string ProductAsin { get; set; } = null;
    }
}