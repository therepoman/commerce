﻿using System.ComponentModel.DataAnnotations;
using Equ;

namespace Amazon.Fuel.Common.Contracts
{
    public abstract class PersistentEntry : MemberwiseEquatable<PersistentEntry>
    {
        [Key]
        public string Id { get; set; }
        public abstract string Type { get; }
        public abstract string StorageContext { get; }
    }
}
