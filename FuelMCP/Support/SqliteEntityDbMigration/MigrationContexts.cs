﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IDbFileProvider
    {
        string DbFilename { get; set; }
    }

    public class TestableDbContext : DbContext, IDbFileProvider
    {
        public string DbFilename { get; set; }
    }

    public class GameInstallInfoDbContext : TestableDbContext
    {
        public DbSet<GameInstallInfo> DbSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={DbFilename};");
        }
    }

    public class GameProductInfoDbContext : TestableDbContext
    {
        public DbSet<GameProductInfo> DbSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={DbFilename};");
        }
    }
}
