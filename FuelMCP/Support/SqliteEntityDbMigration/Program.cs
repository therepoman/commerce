﻿using System;
using SqliteEntityDbMigration.Tests;

public class Program
{
    static void Main(string[] args)
    {
        var test = new MigrationTest();

        test.GameProductInfoMigration_CanCreateNewDb();
        test.GameInstallInfoMigration_CanCreateNewDb();
        test.ProductInfoMigration_PassesThroughV1();
        test.GameInstallInfoMigration_PassesThroughV1();

        Console.WriteLine("Press any key to exit...");
        Console.ReadKey();
    }
}
