using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Entitlement.Models
{
    public class SyncGoodsResponse
    {
        public Entitlement[] Entitlements;
    }

    public class Entitlement
    {
        public String ChannelId;
        public String Id;
        public Product Product;
        public String State;
        
    }

    public class Product
    {
        public String Asin;
        public String AsinVersion;
        public String Id;
        public ProductDetail ProductDetail;
        public String ProductLine;
        public String Sku;
        public String Title;
        public String Type;
        public String VendorId;
        public String Description;
        public String DomainId;
        public String ProductTitle;

    }

    public class ProductDetail
    {
        public Details Details;
        public String IconUrl;
    }

    public class Details
    {
        public String BackgroundUrl1;
        public String BackgroundUrl2;
        public String Publisher;
        public String[] Screenshots;
        public String[] Videos;
    }


}
