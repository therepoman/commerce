﻿using System;

namespace Amazon.Fuel.Plugin.Entitlement.Models
{
    public class SdsException : Exception
    {

    }

    public class NotEntitledSdsException : SdsException
    {
        public NotEntitledSdsException(string entitlementId)
        {
            EntitlementId = entitlementId;
        }

        public string EntitlementId { get; private set; }
    }


}
