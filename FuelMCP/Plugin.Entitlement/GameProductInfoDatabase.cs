﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Serilog;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Exceptions;

namespace Amazon.Fuel.Plugin.Entitlement
{
    public class GameProductInfoDatabase : SqliteDatabase<GameProductInfo, GameProductInfoDbContext>, IEntitlementsDatabase
    {
        private readonly IEntitlementService _entitlementService;
        private readonly SemaphoreSlim _entitlementsLock = new SemaphoreSlim(1);

        public GameProductInfoDatabase(
            IPersistence persistence,
            IMetricsPlugin metrics,
            IEntitlementService entitlementService,
            IMessengerHub hub,
            IOSDriver osDriver) : base(persistence, hub, metrics, osDriver)
        {
            _entitlementService = entitlementService;

            // remove all when logout occurs
            hub.Subscribe<TwitchUserLogoutMessage>(msg => { RemoveAll(); });
        }

        public List<GameProductInfo> ActiveEntitlements
        {
            get
            {
                List<GameProductInfo> searchResults;

                _entitlementsLock.Wait();
                try
                {
                    searchResults = FindEntriesByFilter(e => e.State == GameProductInfo.STATE_LIVE).ToList();
                }
                finally
                {
                    _entitlementsLock.Release();
                }

                if ((searchResults?.Count ?? 0) == 0)
                {
                    return new List<GameProductInfo>();
                }

                Dictionary<string, GameProductInfo> results = new Dictionary<string, GameProductInfo>();
                foreach (GameProductInfo game in searchResults)
                {
                    var key = game.ProductId.Id;
                    if (results.ContainsKey(key))
                    {
                        if (game.IsDeveloper)
                        {
                            results[key] = game;
                        }
                        continue;
                    }
                    results[key] = game;
                }

                return new List<GameProductInfo>(results.Values);
            }
        }

        public async Task<bool> SyncGoods()
        {
            try
            {
                await _entitlementsLock.WaitAsync();
                try
                {
                    var entitledGames = await _entitlementService.SyncGoodsWithRetry();
                    ReplaceAll(entitledGames.ProductInfos);
                }
                finally
                {
                    _entitlementsLock.Release();
                }

                return true;
            }
            catch (TwitchAuthException)
            {
                // caller should handle Twitch Auth Exceptions
                throw;
            }
            catch (Exception ex)
            {
                Log.Warning(ex, "[LIB] Error finding entitled games");
                return false;
            }
        }
        public GameProductInfo Get(ProductId productId)
        {
            List<GameProductInfo> searchResults;
            _entitlementsLock.Wait();
            try
            {
                searchResults = FindEntriesByFilter(e => e.State == GameProductInfo.STATE_LIVE && e.ProductId == productId).ToList();
            }
            finally
            {
                _entitlementsLock.Release();
            }

            if ((searchResults?.Count ?? 0) == 0)
            {
                return null;
            }

            return searchResults.FirstOrDefault(e => e.IsDeveloper) ?? searchResults.FirstOrDefault();
        }
    }
}
