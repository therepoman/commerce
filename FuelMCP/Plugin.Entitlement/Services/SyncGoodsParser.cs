using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Plugin.Entitlement.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace Amazon.Fuel.Plugin.Entitlement.Services
{
    public class SyncGoodsParser
    {
        private readonly IMetricsPlugin _metrics;
        private readonly string[] GAME_PRODUCT_LINE_FILTER = {
            "Twitch:FuelGame",
            "Sonic:Game"
        };
        public const string DEV_CHANNEL = "38202913-4105-448D-B903-9777F8E4A940";

        public SyncGoodsParser(IMetricsPlugin metrics)
        {
            _metrics = metrics;
        }

        public List<GameProductInfo> Parse(string data)
        {
            List<GameProductInfo> output = new List<GameProductInfo>();

            if (data == null)
            {
                return output;
            }

            SyncGoodsResponse responseObj = JsonConvert.DeserializeObject<SyncGoodsResponse>(data);
            if (responseObj == null)
            {
                return output;
            }

            var entitlements = responseObj.Entitlements;
            if (entitlements == null || entitlements.Length == 0)
            {
                Log.Information($"[AES] No entitlements found");
                _metrics.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_Empty, 1);
                return output;
            }
            else
            {
                _metrics.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_Empty, 0);
            }

            foreach (var entitlement in entitlements)
            {
                string type = entitlement.Product.Type;

                bool isEntitlement = "Entitlement".Equals(type, StringComparison.InvariantCultureIgnoreCase);
                var channelId = entitlement.ChannelId;
                bool isDeveloper = DEV_CHANNEL.Equals(channelId?.ToString());

                //We only care for Live products that are also an entitlement type.
                if (isEntitlement == false)
                {
                    continue;
                }

                var prod = entitlement.Product;

                if (prod?.ProductLine == null || !GAME_PRODUCT_LINE_FILTER.Contains((string)prod.ProductLine))
                {
                    continue;
                }

                string pid = prod.Id;
                var productId = new ProductId(pid);

                GameProductInfo outEntitlement = null;

                string productIconUrl = prod.ProductDetail?.IconUrl;
                if (productIconUrl == null)
                {
                    Log.Warning($"[AES] No product icon for entitlement title:'{prod.ProductTitle}' productid:'{prod.Id}',sku:'{prod.Sku}");
                }

                try
                {
                    string entitlementId = entitlement.Id;

                    outEntitlement = new GameProductInfo()
                    {
                        Id = entitlementId,   // NOTE: this is always unique per purchase instance
                        ProductIdStr = productId.Id,
                        ProductTitle = prod.Title ?? productId.Id ?? "unknown product",
                        ProductAsin = prod.Asin,
                        ProductAsinVersion = prod.AsinVersion,
                        ProductIconUrl = productIconUrl,
                        ProductDescription = prod.Description,
                        ProductDomain = prod.DomainId ?? "{00000000-0000-0000-0000-000000000000}",
                        ProductLine = prod.ProductLine,
                        ProductSku = prod.Sku,
                        ProductPublisher = prod.ProductDetail?.Details?.Publisher ?? prod.VendorId ?? "Unknown Publisher",
                        State = entitlement.State,
                        IsDeveloper = isDeveloper,
                        Videos = prod.ProductDetail?.Details?.Videos?.Where(url => !url.Equals("null")).ToArray(),
                        Screenshots = prod.ProductDetail?.Details?.Screenshots,
                        Background = prod.ProductDetail?.Details?.BackgroundUrl1,
                        Background2 = prod.ProductDetail?.Details?.BackgroundUrl2
                    };

                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_Invalid, 0);
                }
                catch( Exception ex )
                {                        
                    Log.Error(ex, $"[AES] Parsing error encountered in ADS good, ignoring title:'{outEntitlement?.ProductTitle}' productid:'{prod?.Id}',sku:'{prod?.Sku}");
                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_Invalid, 1);
                    continue;
                }

                // ensure that fields are valid
                if (string.IsNullOrEmpty(outEntitlement.Id))
                {
                    Log.Error($"[AES] Null id encountered in ADS good, ignoring title:'{outEntitlement?.ProductTitle}' productid:'{outEntitlement?.ProductId}',sku:'{outEntitlement?.ProductSku}");
                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_MissingId, 1);
                    continue;
                } else
                {
                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_MissingId, 0);
                }

                output.Add(outEntitlement);
            }
            _metrics.AddCounter(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_ParseSuccess);

            return output;
        }

        //ADG Lists are maps where the keys are "0", "1", etc. We will loop through the values and add them to a List
        //This assumes the ADG List is a list of strings.
        private IList<string> DeserializeAdgList(dynamic dynamicAdgList, string entitlementId)
        {
            try
            {
                IEnumerable<KeyValuePair<string, JToken>> adgMap = dynamicAdgList;
                //Create a List of the values in the map. If the list is empty, it will actually have just one item "None", so make sure to filter that out and return an empty list.
                var adgList = adgMap?.Select(keyValuePair => keyValuePair.Value.ToString()).ToList();
                if (adgList == null || (adgList.Count == 1 && "None".Equals(adgList[0])))
                {
                    return new List<string>();
                }
                return adgList;
            }
            catch (Exception e)
            {
                Log.Error(e, $"[SyncGoodsParser] Issue when parsing the SyncGoods response and trying to deserialize an ADG List. adgGoodID={entitlementId}");
                return null;
            }
        }
    }
}