﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using System.Net;
using System.Net.Http;
using Serilog;
using Amazon.Fuel.Plugin.Entitlement.Messages;
using Amazon.Fuel.Common.Utilities;
using System.Threading;
using Amazon.Fuel.Common.Extensions;
using Amazon.Fuel.Plugin.Download;
using Amazon.Fuel.SoftwareDistributionService.Client;
using Amazon.Fuel.SoftwareDistributionService.Model;
using Amazon.Fuel.Common.Contracts.Manifest;
using Tv.Twitch.Fuel.Manifest;
using Sds = Tv.Twitch.Fuel.Sds;
using Polly;

namespace Amazon.Fuel.Plugin.Entitlement
{
    public class EntitlementPlugin : IEntitlementPlugin
    {
        private readonly int MAX_RETRY = 5;

        private readonly IMessengerHub _messengerHub;
        private readonly ITwitchLoginPlugin _twitchLoginPlugin;
        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly ISoftwareDistributionServiceClient _sdsClient;
        private readonly HttpClient _httpClient;
        private readonly IManifestStorage _manifestStorage;

        public EntitlementPlugin(
            IMessengerHub messengerHub,
            ITwitchLoginPlugin twitchLoginPlugin, 
            ISoftwareDistributionServiceClient sds, 
            ISoftwareDistributionServiceClient sdsClient, 
            HttpMessageHandler httpMessageHandler,
            IManifestStorage manifestStorage)
        {
            _messengerHub = messengerHub;
            _twitchLoginPlugin = twitchLoginPlugin;
            _sds = sds;
            _sdsClient = sdsClient;
            _httpClient = new HttpClient(httpMessageHandler);
            _manifestStorage = manifestStorage;
        }

        private async Task<ManifestWithVersion> DownloadManifestDataAsync(string adgGoodId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken))
        {
            Log.Information($"EntitlementPlugin.DownloadManifestDataV2Async({adgGoodId}, {adgProductId})");
            //Call SDS to get the various download URLs for the download manifest.
            var request = new GetDownloadManifestV2Request(adgGoodId);
            var result = await _sds.GetDownloadManifestV2(request, cancelToken);
            if (result?.downloadUrls == null || result.downloadUrls.Count == 0)
            {
                Log.Error(
                    $"EntitlementPlugin.DownloadManifestDataV2Async({adgGoodId} ,{adgProductId}) - SDS did not return any manifest download URLs");
            }
            //Each download URL is for the same file but different locations (s3, cloudfront, etc).
            foreach (var manifestDownloadUrl in result.downloadUrls)
            {
                try
                {
                    var manifestData = await FetchManifest(adgProductId, result.versionId, manifestDownloadUrl);
                    if (manifestData == null)
                    {
                        throw new Exception("Couldn't retrieve manifest");
                    }

                    return new ManifestWithVersion
                    {
                        SdsVersionId = result.versionId,
                        ManifestData = manifestData,
                        TotalInstallSizeInBytes = manifestData.ComputeTotalSizeInBytes()
                    };
                }
                catch (Exception e)
                {
                    //Unable to download or parse the manifest, try the next download URL.
                    Log.Error(e, 
                        $"EntitlementPlugin.DownloadManifestDataV2Async({adgGoodId}, {adgProductId}) - Unable to download/parse manifest from {manifestDownloadUrl}");
                }
            }
            //Unsuccessfull in downloading/parsing the manifest, return null;
            return null;
        }

        /// <summary>
        /// Get the cached manifest, if exist, otherwise download, cache and return.
        /// </summary>
        /// <param name="versionId"></param>
        /// <param name="manifestDownloadUrl">Only used if cached doesn't exist</param>
        /// <returns></returns>
        private async Task<Sds.Manifest> FetchManifest(ProductId prodId, string versionId, string manifestDownloadUrl)
        {
            var manifest = await _manifestStorage.Get(prodId, versionId);
            if (manifest != null)
            {
                return manifest;
            }

            FuelLog.Info($"Fetching manifest", new {manifestDownloadUrl, prodId, versionId });

            // fetch a new one and cache it
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(manifestDownloadUrl));
            var response = await _httpClient.SendAsync(request);
            var responseStream = await response.Content.ReadAsStreamAsync();
            await _manifestStorage.PutRaw(prodId, versionId, responseStream, verifyAfterStorage: true);

            return await _manifestStorage.Get(prodId, versionId);
        }

        /// <summary>
        /// Given an adgGoodId, Downloads the manifest, parses it from XML into a manifest object.
        /// Retries using await until it actually downloads.
        /// </summary>
        /// <param name="adgGoodId"></param>
        /// <param name="adgProductId"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        public async Task<ManifestWithVersion> DownloadManifestAsync(string adgGoodId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken))
        {
            Log.Information($"EntitlementPlugin.DownloadManifestAsync({adgGoodId})");
            ManifestWithVersion output = null;

            if ( adgGoodId == null )
            {
                Log.Error("[ENT] [DownloadManifestAsync] adgGoodId is null");
                return null;
            }

            _twitchLoginPlugin.EnsureLoggedIn();

            _messengerHub.Publish(new MessageDownloadingManifestStarted(this, adgGoodId));

            /*
             * Calls DownloadManifestDataAsync.
             * Retries using exponental backoff if cancellation was request or the result was null.
             * Publishes a MessageDownloadingManifestError on each retry attempt.
             */
            PolicyResult<ManifestWithVersion> result = await Policy
                .HandleResult<ManifestWithVersion>(r => !cancelToken.IsCancellationRequested && r == null)
                .WaitAndRetryAsync(MAX_RETRY,
                    retryAttempt => TimeSpan.FromMilliseconds(500 * Math.Pow(retryAttempt, 1.5)),
                    onRetry: (exception, delayFor, retryAttempt, context) =>
                    {
                        Log.Warning($"[ENT] ManifestWithVersion could not be found for game, retrying ({retryAttempt}/{MAX_RETRY})");
                        _messengerHub.Publish(new MessageDownloadingManifestError(this, adgGoodId, (long) delayFor.TotalMilliseconds, retryAttempt, cancelToken));
                    })
                .ExecuteAndCaptureAsync(async () =>
                {
                    return await DownloadManifestDataAsync(adgGoodId, adgProductId, cancelToken);
                });

            Exception ex = result.FinalException;
            if (ex != null && !(ex is OperationCanceledException))
            {
                throw ex;
            }

            if (!cancelToken.IsCancellationRequested)
            {
                output = result.Result;
            }

            _messengerHub.Publish(new MessageDownloadingManifestFinished(this, adgGoodId, success: output != null));
            return output;
        }
       
        // Returns a Dictionary<adgProductId, versionId>
        public async Task<InstallationRequirements> GetInstallationRequirements(string entitlementId, ProductId adgProductId,
            CancellationToken cancelToken = default(CancellationToken))
        {
            try
            {
                var manifest = await DownloadManifestAsync(entitlementId, adgProductId, cancelToken);
                var downloadSize = manifest.TotalInstallSizeInBytes;
                return new InstallationRequirements
                {
                    RequiredDiskSpaceForFullInstall = InstallUtil.Instance.GetRequiredDiskSpaceForInstall(isUpdate: false, downloadSize: downloadSize),
                    RequiredDiskSpaceForUpdate = InstallUtil.Instance.GetRequiredDiskSpaceForInstall(isUpdate: true, downloadSize: downloadSize),
                    Result = InstallationRequirements.EResult.Success
                };
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Unable to retrieve installation space requirements");
                return new InstallationRequirements()
                {
                    Result = InstallationRequirements.EResult.Failure
                };
            }
        }

        public async Task<IDictionary<string, string>> CurrentVersionIdsForAdgProductIds(IList<string> adgProductIds)
        {
            return await CurrentVersionIdsForAdgProductIdsWithRelogin(adgProductIds, allowRelogin: true);
        }

        private async Task<IDictionary<string, string>> CurrentVersionIdsForAdgProductIdsWithRelogin(IList<string> adgProductIds, bool allowRelogin)
        { 
            Log.Information($"EntitlementPlugin.CurrentVersionIdsForAdgProductIds([{string.Join<string>(", ", adgProductIds)}])");
            try
            {
                var currentVersions = new Dictionary<string, string>();
                var request = new GetVersionsRequest(adgProductIds);
                var result = await _sdsClient.GetVersions(request);

                foreach (var error in result.errors)
                {
                    Log.Error(
                        $"Unable to get SDS Version for ADG Product ID \"{error.adgProductId}\" - [{error.errorType}]\"{error.message}\"");
                }

                foreach (var version in result.versions)
                {
                    currentVersions.Add(version.adgProductId, version.versionId);
                }

                return currentVersions;
            }
            catch (Exception e) 
                when ( e is NotAuthorizedException 
                    || e is NotAuthorizedGetVersionException )
            {
                FuelLog.Error(e, "Coral returned not authorized exception");
                throw;
            }
            catch (Exception e)
            {
                Log.Error(e, $"EntitlementPlugin.CurrentVersionIdsForAdgProductIds Exception thrown while calling SoftwareDistributionService.GetVersions - {e.Message} ");
                return new Dictionary<string, string>();
            }
        }
    }
}
