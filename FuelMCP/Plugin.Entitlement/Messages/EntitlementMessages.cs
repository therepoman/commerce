﻿using System.Threading;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Entitlement.Messages
{
    public class EntitlementMessage : ITinyMessage
    {
        public object Sender { get; set; }
        public string EntitlementId { get; set; }

        public EntitlementMessage( object sender, string entitlementId)
        {
            this.Sender = sender;
            this.EntitlementId = entitlementId;
        }
    }

    public class MessageDownloadingManifestStarted : EntitlementMessage
    {
        public MessageDownloadingManifestStarted(object sender, string entitlementId) : base(sender, entitlementId)
        {
        }
    }

    public class MessageDownloadingManifestError : EntitlementMessage
    {
        public long DelayForMS { get; set; }
        public int AttemptCount { get; set; }
        public CancellationToken Token { get; set; }

        public MessageDownloadingManifestError(object sender, string entitlementId, long delayFor, int attemptCount, CancellationToken token) : base(sender, entitlementId)
        {
            this.DelayForMS = delayFor;
            AttemptCount = attemptCount;
            Token = token;
        }
    }

    public class MessageDownloadingManifestFinished : EntitlementMessage
    {
        public bool Success { get; set; }

        public MessageDownloadingManifestFinished(object sender, string entitlementId, bool success) : base(sender, entitlementId)
        {
            this.Success = success;
        }
    }
}
