﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Exceptions;
using Amazon.Fuel.Common.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace Clients.Kraken
{
    public class ReloggableKrakenClient : KrakenClient
    {
        public ReloggableKrakenClient(string clientid, string endpoint) : base(clientid, endpoint)
        {
        }

        public override async Task<KrakenUser> GetUserForOauth(ITwitchTokenProvider ttp)
        {
            try
            {
                return await WrapAggregateHandler<KrakenUser>(async () => await base.GetUserForOauth(ttp));
            }
            catch (RetryableTwitchAuthException firstEx)
            {
                FuelLog.Warn("Retryable error in kraken client, trying again", new {message = firstEx.Message});
                try
                {
                    Thread.Sleep(500);
                    return await WrapAggregateHandler<KrakenUser>(async () => await base.GetUserForOauth(ttp));
                }
                catch (TwitchAuthException secondEx)
                {
                    FuelLog.Error(secondEx, "TwitchAuthException in kraken client after retry");
                    throw secondEx;
                }
                catch (Exception secondEx)
                {
                    FuelLog.Error(secondEx, "Unexpected Exception in kraken client after retry");
                    throw;
                }
            }
            catch (Exception secondEx)
            {
                FuelLog.Error(secondEx, "Unexpected Exception in kraken client");
                throw;
            }
        }

        public override async Task<bool> IsValid(ITwitchTokenProvider ttp)
        {
            return await IsValid(ttp);
        }

        private async Task<T> WrapAggregateHandler<T>(Func<Task<T>> t)
        {
            try
            {
                return await t();
            }
            catch (AggregateException ex)
            {
                ex.Flatten();
                if (ex.InnerException != null)
                {
                    throw ex.InnerException;
                }
                throw;
            }
        }
    }

    public class KrakenClient
    {
        private static string AUTHORIZTION_HEADER = "Authorization";
        private static string CLIENT_ID_HEADER = "Client-ID";
        private static string ACCEPT_HEADER = "Accept";
        private static string API_ID = "application/vnd.twitchtv.v5+json";
        private readonly string clientid;
        
        private readonly string _endpoint;

        public KrakenClient(string clientid, string endpoint)
        {
            if (String.IsNullOrEmpty(clientid) || String.IsNullOrEmpty(endpoint))
            {
                throw new ArgumentException("Must pass clientid and endpoint");
            }
            _endpoint = endpoint;
            this.clientid = clientid;
        }

        public virtual async Task<KrakenUser> GetUserForOauth(ITwitchTokenProvider ttp)
        {
            System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                var uri = new Uri($"{this._endpoint}/user?api_client_id_killswitch=true");
                var request = new HttpRequestMessage
                {
                    RequestUri = uri,
                    Method = HttpMethod.Get,
                    Headers =
                    {
                        {ACCEPT_HEADER, API_ID},
                        {CLIENT_ID_HEADER, clientid },
                        {AUTHORIZTION_HEADER, $"OAuth {ttp.GetAndValidateAccessToken()}"},
                    },
                };

                var response = await client.SendAsync(request).ConfigureAwait(false);

                if (new List<HttpStatusCode> { HttpStatusCode.BadRequest, HttpStatusCode.Unauthorized }.Contains(response.StatusCode))
                {
                    throw new RetryableTwitchAuthException(TwitchAuthException.EType.BadResponse, response.StatusCode);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new TwitchAuthException(TwitchAuthException.EType.BadResponse, response.StatusCode);
                }

                try
                {
                    var user = JsonConvert.DeserializeObject<KrakenUser>(response.Content.ReadAsStringAsync().Result);
                    return user;
                }
                catch (Exception ex)
                {
                    throw new TwitchAuthException(TwitchAuthException.EType.InvalidJson, response.StatusCode, detail: ex.Message);
                }
            }
        }

        public virtual async Task<bool> IsValid(ITwitchTokenProvider ttp)
        {
            using (var client = new HttpClient())
            {
                var uri = new Uri($"{this._endpoint}/?api_client_id_killswitch=true");
                var request = new HttpRequestMessage
                {
                    RequestUri = uri,
                    Method = HttpMethod.Get,
                    Headers =
                    {
                        {ACCEPT_HEADER, API_ID},
                        {CLIENT_ID_HEADER, clientid },
                        {AUTHORIZTION_HEADER, $"OAuth {ttp.GetAccessToken()}"},
                    },
                };

                var response = await client.SendAsync(request);

                if (new List<HttpStatusCode> { HttpStatusCode.BadRequest, HttpStatusCode.Unauthorized }.Contains(response.StatusCode))
                {
                    throw new RetryableTwitchAuthException(TwitchAuthException.EType.BadResponse, response.StatusCode);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new TwitchAuthException(TwitchAuthException.EType.BadResponse, response.StatusCode);
                }

                var identity = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync()) as JObject;
                var identyValue = identity?.GetValue("identified") as JValue;

                if (identyValue != null && identyValue.Value is bool)
                {
                    return (bool)identyValue?.Value;
                }

                return false;
            }
        }
    }

    // Maps https://github.com/justintv/Twitch-API/blob/master/v3_resources/users.md#get-user case and _'s are important, don't change
    public class KrakenUser
    {
        public string type;
        public string name;
        public string created_at;
        public string updated_at;
        public Dictionary<string, string> _links;
        public string logo;
        public string _id;
        public string display_name;
        public string email;
        public bool partnered;
        public string bio;
        public Dictionary<string, bool> notifications;
    }
}
