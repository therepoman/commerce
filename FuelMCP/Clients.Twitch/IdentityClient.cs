﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Exceptions;
using Newtonsoft.Json;
using Serilog;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Utilities;

namespace Clients.Kraken
{

    public class ReloggableIdentityClient : IdentityClient
    {
        public ReloggableIdentityClient(string clientid, string endpoint) : base(clientid, endpoint)
        {
        }

        public override async Task<TokenResponse> FetchGameToken(string clientId, ITwitchTokenProvider ttp, IEnumerable<string> scopes)
        {
            return await base.FetchGameToken(clientId, ttp, scopes);
        }
    }

    public class IdentityClient
    {
        private readonly string base_url;
        // https://dev.twitch.tv/docs/authentication#revoking-access-tokens
        private static string TWITCH_REVOKE_URI = "https://id.twitch.tv/oauth2/revoke";
        private static string CLIENT_ID_HEADER = "Client-ID";
        private static string TWITCH_API_V5_ACCEPT = "application/vnd.twitchtv.v5+json";
        private string clientid;

        public IdentityClient(string clientid, string endpoint)
        {
            if (String.IsNullOrEmpty(clientid) || String.IsNullOrEmpty(endpoint))
            {
                throw new ArgumentException("Must pass clientid and endpoint");
            }
            this.base_url = endpoint;
            this.clientid = clientid;
        }
        
        public virtual async Task<TokenResponse> FetchGameToken(string clientId, ITwitchTokenProvider ttp, IEnumerable<string> scopes)
        {
            System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                clientId = HttpUtility.UrlEncode(clientId);

                var scopesStr = string.Join(" ", scopes);
                var uri = new Uri($"{base_url}/kraken/fuel/exchange");
                var content = new FormUrlEncodedContent(new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("game_client_id", clientId), 
                    new KeyValuePair<string, string>("scope", scopesStr),
                });

                var request = new HttpRequestMessage
                {
                    RequestUri = uri,
                    Method = HttpMethod.Post,
                    Content = content,
                    Headers =
                    {
                        {CLIENT_ID_HEADER, clientid },
                    },
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("OAuth", ttp.GetAndValidateAccessToken());
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(TWITCH_API_V5_ACCEPT));

                var response = await client.SendAsync(request).ConfigureAwait(false); /* Stop deadlock */

                if (new List<HttpStatusCode>{HttpStatusCode.BadRequest, HttpStatusCode.Unauthorized}.Contains(response.StatusCode))
                {
                    throw new RetryableTwitchAuthException(RetryableTwitchAuthException.EType.BadResponse, response.StatusCode);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new TwitchAuthException(RetryableTwitchAuthException.EType.BadResponse, response.StatusCode);
                }

                var tokenResponse =
                    JsonConvert.DeserializeObject<TokenResponse>(response.Content.ReadAsStringAsync().Result);
                return tokenResponse;
                // If you need to check the token with https://api.twitch.tv/kraken?oauth_token=TOKEN
            }
        }

        public virtual async Task Revoke(string token)
        {
            using (var client = new HttpClient())
            {
                Log.Information("Revoking refresh token");
                var encodedToken = HttpUtility.UrlEncode(token);
                var encodedFuelClientId = HttpUtility.UrlEncode(Constants.ClientId);
                var uri = new Uri(TWITCH_REVOKE_URI + $"?client_id={encodedFuelClientId}&token={encodedToken}");
                
                var request = new HttpRequestMessage
                {
                    RequestUri = uri,
                    Method = HttpMethod.Post
                };

                HttpResponseMessage response = null;
                try
                {
                    response = await client.SendAsync(request).ConfigureAwait(false);
                }
                catch (HttpRequestException ex)
                {
                    Log.Error(ex, "HttpRequestException, network offline?");
                    throw new NetworkOfflineException(ex);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Log.Error("Bad response status code returned: " + response.StatusCode);
                    throw new TwitchAuthException(TwitchAuthException.EType.BadResponse, response.StatusCode);
                }
            }
        }
    }

    public class RefreshResponse
    {
        //{"scope":["channel_read","user_read"],"access_token":"MEDIUM_STRING","refresh_token":"LONG_STRING"}
        public string access_token;
        public string refresh_token;
        public List<string> scope;
    }

    public class TokenResponse
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
    }
}
