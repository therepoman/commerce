﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.TwitchLogin
{
    public class DefaultCurseTokenProvider : ICurseTokenProvider
    {
        private readonly ITwitchUserStoragePlugin _twitchUserStorage;

        public DefaultCurseTokenProvider(ITwitchUserStoragePlugin twitchUserStorage)
        {
            _twitchUserStorage = twitchUserStorage;
        }
        
        public string Token
        {
            get
            {
                return _twitchUserStorage?.User?.AccessToken;
            }
        }
    }
}
