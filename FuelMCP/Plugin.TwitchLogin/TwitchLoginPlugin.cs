﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Clients.Kraken;
using Amazon.Fuel.Common.Exceptions;
using Serilog;
using Amazon.Fuel.Common.Utilities;
using Polly;
using System.Net;

namespace Amazon.Fuel.Plugin.TwitchLogin
{
    /// <summary>
    /// Plugin which is responsible for getting the currently logged in user or prompting the user for login
    /// </summary>
    public class TwitchLoginPlugin : ITwitchLoginPlugin
    {
        public static int MAX_RETRIES = 3;

        private readonly IMessengerHub _hub;
        private readonly KrakenClient _krakenClient;
        private readonly IdentityClient _identity;
        private readonly ITwitchUserStoragePlugin _twitchUserStorage;

        private readonly object _userLock = new { };

        public TwitchLoginPlugin(
            IMessengerHub tinyMessengerHub,
            KrakenClient krakenClient,
            IdentityClient identityClient,
            ITwitchUserStoragePlugin twitchUserStorage)
        {
            Debug.Assert(krakenClient != null);
            Debug.Assert(identityClient != null);
            _hub = tinyMessengerHub;
            _krakenClient = krakenClient;
            _identity = identityClient;
            _twitchUserStorage = twitchUserStorage;
        }

        public string GetAndValidateAccessToken()
        {
            EnsureLoggedIn();
            return _twitchUserStorage.User.AccessToken;
        }

        public void EnsureLoggedIn()
        {
            if ( string.IsNullOrEmpty(_twitchUserStorage.User?.AccessToken) )
            {
                FuelLog.Warn("EnsureLoggedIn() : User does not appear to be logged in");
                throw new TwitchAuthException(TwitchAuthException.EType.TokenInvalid);
            }
        }

        public string GetGameToken(string clientid, IEnumerable<string> scopes)
        {
            var response = WebExceptionPolicy().Execute(
                () => _identity.FetchGameToken(clientid, this, scopes).Result);

            return response.AccessToken;
        }

        public void SetAuthTokens(string accessToken, bool allowRefreshUserData = true)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                Log.Error("Empty access token passed to SetAuthTokens()");
                throw new TwitchAuthException(TwitchAuthException.EType.TokenInvalid);
            }

            bool wasLoggedIn = IsLoggedIn();

            lock (_userLock)
            {
                var user = _twitchUserStorage.User ?? new LocalTwitchUser();
                user.AccessToken = accessToken;
                _twitchUserStorage.User = user;

                if (allowRefreshUserData
                    && (user.Tuid == null
                        || user.Username == null
                        || user.DisplayName == null
                        || user.Logo == null))
                {
                    RefreshUserData();
                }
            }

            DeviceMappingUpdater.CheckAdd(_twitchUserStorage.User);

            if (!wasLoggedIn)
            {
                _hub.Publish(new TwitchUserLoginMessage(this));
            }
        }

        private void RefreshUserData()
        { 
            // first update to support token provider service
            var user = _twitchUserStorage.User;

            var login = _krakenClient.GetUserForOauth(this).GetAwaiter().GetResult();

            if (login == null)
            {
                // our refresh token is invalid, if we still can't get kraken results at this point 
                Log.Error("[TLG] Couldn't get login info from Kraken (will have null info)");
            }

            // resync with latest (in case token is updated)
            user.Tuid = login?._id;
            user.Username = login?.name;
            user.DisplayName = login?.display_name;
            user.Logo = login?.logo;

            _twitchUserStorage.User = user;
        }

        public bool UserHasAccessToken()
        {
            return _twitchUserStorage.User?.AccessToken != null;
        } 

        public bool IsLoggedIn(string optionalCurseUserId = null)
        {
            lock (_userLock)
            {
                // check for wrong session id
                if (!string.IsNullOrEmpty(optionalCurseUserId)
                    && optionalCurseUserId != CurseUserIdWithoutValidation())
                {
                    return false;
                }

                return UserHasAccessToken();
            }
        }

        public string GetAccessToken()
        {
            lock (_userLock)
            {
                return _twitchUserStorage.User?.AccessToken;
            }
        }

        public string CurseUserIdWithoutValidation()
        {
            lock (_userLock)
            {
                return _twitchUserStorage.User?.SessionUserId;
            }
        }

        public void SetCurseUserId(string sessionUserId)
        {
            lock (_userLock)
            {
                var user = _twitchUserStorage.User ?? new LocalTwitchUser();
                user.SessionUserId = sessionUserId;
                _twitchUserStorage.User = user;
            }
        }

        public string GetTuid()
        {
            lock (_userLock)
            {
                return _twitchUserStorage.User?.Tuid;
            }
        }

        public void InvalidateAccessToken(bool revoke = false)
        {
            if (_twitchUserStorage.User == null )
            {
                return;
            }

            lock (_userLock)
            {
                if (_twitchUserStorage.User == null)
                {
                    return;
                }

                var u = _twitchUserStorage.User;

                if (u.AccessToken != null && revoke)
                {
                    try
                    {
                        _identity.Revoke(u.AccessToken).Wait();
                    }
                    catch (Exception e)
                    {
                        FuelLog.Warn(e, "Failed to revoke access token as requested. Deleting anyway.");
                    }
                }

                u.AccessToken = null;
                _twitchUserStorage.User = u;
            }
        }

        public void InvalidateLogin()
        {
            lock (_userLock)
            {
                InvalidateAccessToken(revoke: true);
                _twitchUserStorage.Remove();
            }

            _hub.Publish(new TwitchUserLogoutMessage(this));
        }

        private Policy WebExceptionPolicy()
        {
            return Policy
                .Handle<WebException>()
                .Or<RetryableTwitchAuthException>(ex => ex.Type == TwitchAuthException.EType.BadResponse)
                .WaitAndRetry(MAX_RETRIES,
                    retryAttempt => TimeSpan.FromMilliseconds(100 * Math.Pow(retryAttempt, 1.5)),
                    onRetry: (lastResult, delayFor, context) =>
                    {
                        FuelLog.Info("Failed to exchange for game token, retrying", new { lastResult, MAX_RETRIES, delayFor });
                    });
        }
    }
}
