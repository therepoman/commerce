﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Common.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.TwitchLogin
{
    internal static class DeviceMappingUpdater
    {
        private const string LAMBDA_URL = @"https://xjmeanyr58.execute-api.us-west-2.amazonaws.com/prod/DeviceMappings";

        static bool _deviceMappingsAdded = false;

        public static void CheckAdd(LocalTwitchUser user)
        {
            if (_deviceMappingsAdded)
            {
                return;
            }

            new Task(async () =>
            {
                try
                {
                    await Add(user);
                }
                catch (Exception ex)
                {
                    FuelLog.Warn(ex, "Exception thrown when attempting to update device mappings");
                }
            }).Start();
        }

        private static async Task Add(LocalTwitchUser user)
        {
            using (var client = new HttpClient())
            {
                var uri = new Uri(LAMBDA_URL);
                var keyPairs = new Dictionary<string, string>();
                keyPairs.Add("tuid", user?.Tuid);
                keyPairs.Add("dsi", DeviceInfo.DSN);
                keyPairs.Add("dsi_name", Environment.MachineName);
                var dtStr = "";

                try
                {
                    // attempt to push into en-US, otherwise just keep as other culture on fail
                    dtStr = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                }
                catch (Exception)
                {
                    dtStr = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                }

                keyPairs.Add("last_login", dtStr);

                var content = new StringContent(JsonConvert.SerializeObject(keyPairs), UnicodeEncoding.UTF8, "application/json");
                var request = new HttpRequestMessage
                {
                    RequestUri = uri,
                    Method = HttpMethod.Post,
                    Content = content,
                };

                HttpResponseMessage response = null;
                try
                {
                    response = await client.SendAsync(request).ConfigureAwait(false);
                }
                catch (HttpRequestException ex)
                {
                    FuelLog.Warn(ex, "HttpRequestException thrown attempting to update device mappings");
                }

                if (response?.StatusCode == HttpStatusCode.OK)
                {
                    _deviceMappingsAdded = true;
                }
                else
                { 
                    var reason = await response?.Content?.ReadAsStringAsync();
                    FuelLog.Warn($"Failed to update device mappings, Status: {response?.StatusCode}, Reason: {reason}");
                }
            }
        }

    }
}
