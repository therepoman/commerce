#include "MessagePart.h"

using namespace Fuel;

MessagePart::MessagePart(uint32_t size, uint8_t* buffer) :
m_size(size),
m_buffer(buffer)
{
}

const uint8_t* MessagePart::GetBuffer() const
{
	return m_buffer.get();
}

uint32_t MessagePart::GetSize() const
{
	return m_size;
}