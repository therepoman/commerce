#include<Windows.h>

#include "Message.h"
#include "FuelHeader_generated.h"

using namespace Fuel;

Message::Message(int type, MessagePart&& bodyPart) : m_bodyPart(std::move(bodyPart)) {
	m_type = type;
	m_requestId = CreateRequestId();
	m_headerPart = CreateHeader(m_type, m_requestId);
}

Message::Message(const Message& source, int type, MessagePart&& body) :
	m_bodyPart(std::move(body))
{
	m_type = type;
	m_requestId = source.GetRequestId();
	m_headerPart = CreateHeader(m_type, m_requestId);
}

Message::Message(int type, uint32_t bodySize, uint8_t* bodyBuffer) : m_bodyPart(bodySize, bodyBuffer) {
	m_type = type;
	m_requestId = CreateRequestId();
	m_headerPart = CreateHeader(m_type, m_requestId);
}

Message::Message(MessagePart&& headerPart, uint32_t bodySize, uint8_t* bodyBuffer) :
m_headerPart(std::forward<MessagePart>(headerPart)),
m_bodyPart(bodySize, bodyBuffer)
{
	ParseHeader();
}

Message::Message(MessagePart&& headerPart, MessagePart&& bodyPart) :
m_headerPart(std::move(headerPart)),
m_bodyPart(std::move(bodyPart))
{
	ParseHeader();
}

Message::Message(Message&& rhs) :
m_headerPart(std::move(rhs.m_headerPart)),
m_bodyPart(std::move(rhs.m_bodyPart))
{
	ParseHeader();
}

void Message::ParseHeader() {
	if (m_headerPart.IsEmpty())
	{
		m_type = 0;
	}
	else
	{
		const Fuel::Messaging::MessageHeader* messageHeader = Fuel::Messaging::GetMessageHeader(m_headerPart.GetBuffer());
		m_requestId = messageHeader->requestId()->str();
		m_type = messageHeader->type();
	}
}

MessagePart Message::CreateHeader(int type, std::string requestId)
{
	std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());
	auto fbbRequestId = fbb->CreateString(requestId);
	auto offset = Fuel::Messaging::CreateMessageHeader(*fbb, fbbRequestId, 0, type);
	Fuel::Messaging::FinishMessageHeaderBuffer(*fbb, offset);

	// TODO: [FUEL-940] This is being done because the FBB manages the buffer memory itself
	// Alternatively in the future to avoid the copy we could create a derived
	// class to hold the FBB or the special flatbuffers::unique_ptr_t
	uint32_t size = fbb->GetSize();
	uint8_t* buffer = new uint8_t[size];
	memcpy(buffer, fbb->GetBufferPointer(), size);

	return MessagePart(size, buffer);
}

std::string Message::CreateRequestId() {
	UUID uuid = { 0 };

	// Create uuid
	UuidCreate(&uuid);

	// Convert uuid to string
	RPC_CSTR szUuid = NULL;
	if (UuidToStringA(&uuid, &szUuid) == RPC_S_OK)
	{
		std::string uuidStr((char*)szUuid);
		RpcStringFreeA(&szUuid);
		return std::move(uuidStr);
	}
	else {
		throw new HeaderCreationException("Could not genereate request id.");
	}
}

int Message::GetType() const {
	return m_type;
}

const std::string& Message::GetRequestId() const {
	return m_requestId;
}

const MessagePart& Message::GetBody() const
{
	return m_bodyPart;
}

const MessagePart& Message::GetHeader() const
{
	return m_headerPart;
}