#pragma once

#include <stdint.h>
#include <algorithm>
#include <memory>

namespace Fuel
{
	class MessagePart
	{
	public:
		MessagePart() : MessagePart(0, nullptr) {};
		MessagePart(uint32_t size, uint8_t* buffer);

		// Disallow copying, only moving
		MessagePart(const MessagePart&) = delete;
		MessagePart& operator=(const MessagePart& rhs) = delete;
		MessagePart(MessagePart&& rhs) : m_size(rhs.GetSize()), m_buffer(std::move(rhs.m_buffer))
		{
		}
		MessagePart& operator=(MessagePart&& rhs)
		{
			if (this != &rhs)
			{
				m_size = rhs.m_size;
				m_buffer = std::move(rhs.m_buffer);
			}
			return *this;
		}

		bool IsEmpty() const { return m_size == 0; }
		uint32_t GetSize() const;
		const uint8_t* GetBuffer() const;
	private:
		uint32_t m_size;
		std::unique_ptr<uint8_t> m_buffer;
	};
}