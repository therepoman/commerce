#pragma once

#include <chrono>
#include "Types.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			 * Represents the purchase of consumable content, entitlement content or
			 * subscription content, as well as the renewal of a subscription. See
			 * {@link ProductType} for more information on each type.
			 */
			class Receipt
			{
			public:
				/**
				 * Returns a receipt ID which is a unique identifier of a purchase. It can be used from an external server to validate
				 * a purchase.
				 *
				 * @return Receipt ID, a string
				 */
				inline const Fuel::String& GetReceiptId() const { return m_receiptId; }
				/**
				 * Sets a receipt ID which is a unique identifier of a purchase. It can be used from an external server to validate
				 * a purchase. Used internally.
				 *
				 * @param receiptId, a string.
				 */
				inline void SetReceiptId(const Fuel::String& receiptId) { m_receiptId = receiptId; }

				/**
				 * Returns the SKU of the product purchased with this receipt. Note that while this is populated when calling
				 * "GetPurchaseUpdates", it does not get populated after a purchase call (but will be in the future).
				 *
				 * @return SKU, a string
				 */
				inline const Fuel::String& GetSku() const { return m_sku; }
				/**
				 * Sets the SKU of the product purchased with this receipt. Used internally.
				 *
				 * @param sku, a string
				 */
				inline void SetSku(const Fuel::String& sku) { m_sku = sku;  }

				/**
				* Returns the type of the product purchased with this receipt.
				*
				* @return The product type.
				*/
				inline ProductType GetProductType() const { return m_productType; }
				/**
				* Sets the type of the product purchased with this receipt. Used internally.
				*
				* @param productType The product type.
				*/
				inline void SetProductType(ProductType productType) { m_productType = productType; }

				/**
				* Returns the purchase date for the product purchased.
				*
				* @return Purchase Date, a {@link Fuel::DateTime} object.
				*/
				inline const Fuel::DateTime& GetPurchaseDate() const { return m_purchaseDate; }
				/**
				* Sets the purchase date for the product purchased.
				*
				* @param purchaseDate, a {@link Fuel::DateTime} object.
				*/
				inline void SetPurchaseDate(const Fuel::DateTime& purchaseDate) { m_purchaseDate = purchaseDate; }

				/**
				 * Returns the cancel date for the purchase, or null if it is not canceled.
				 *
				 * @return Cancel date, a {@link Fuel::DateTime} object, or null if it is not canceled.
				 */
				inline const Fuel::DateTime& GetCancelDate() const { return m_cancelDate; }
				/**
				 * Sets the cancel date for the purchase, or null if it is not canceled.
				 *
				 * @param cancelDate, a {@link Fuel::DateTime} object, or null if it is not canceled.
				 */
				inline void SetCancelDate(const Fuel::DateTime& cancelDate) { m_cancelDate = cancelDate; }
			private:
				Fuel::String m_receiptId;
				Fuel::String m_sku;
				Fuel::String m_state;

				ProductType m_productType;

				Fuel::DateTime m_purchaseDate;
				Fuel::DateTime m_cancelDate;
			};
		}
	}
}
