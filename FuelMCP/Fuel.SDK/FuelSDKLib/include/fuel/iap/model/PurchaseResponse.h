#pragma once

#include "Types.h"

#include "Receipt.h"

namespace Fuel
{
	class Message;

	namespace Iap
	{
		namespace Model
		{
			enum class PurchaseResponseType
			{
				Sucessful, /**< Purchase was successfully completed */
				Failed, /**< Purchase was unsuccessful */
				InvalidSku, /**< SKU provided was invalid */
				AlreadyPurchased, /**< Item was already purchased by the viewer */
				NotSupported /**< Purchase operation is not supported */
			};

			/**
			 * Represents the result of a call to {@link IapClient#Purchase()}.
			 */
			class PurchaseResponse
			{
			public:
				/**
				 * Returns a {@link Receipt} if the result is successful,
				 * otherwise null.
				 *
				 * @return Receipt or null.
				 */
				const Receipt& GetReceipt() const { return m_receipt; };
				/**
				 * Sets the {@link Receipt} object if the result is successful,
				 * otherwise null. Used internally.
				 *
				 * @param receipt Receipt or null.
				 */
				void SetReceipt(const Receipt& receipt) { m_receipt = receipt; }
			private:
				Receipt m_receipt;
			};
		}
	}
}
