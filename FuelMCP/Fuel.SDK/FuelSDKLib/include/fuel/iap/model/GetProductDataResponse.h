#pragma once

#include <vector>
#include <map>

#include "Types.h"

#include "Product.h"

namespace Fuel
{
	class Message;

	namespace Iap
	{
		namespace Model
		{
			/**
			 * Represents the result of a call to {@link IapClient#GetProductData()}.
			 */
			class GetProductDataResponse
			{
			public:
				/**
				 * Returns the set of SKUs provided in the original request to
				 * {@link IapClient#GetProductData()} for which product data is
				 * not available. Product data can be unavailable if the provided SKU is not
				 * defined or if there was a problem retrieving the data.
				 *
				 * @return A vector of SKUs provided in the original request for which
				 *         product data is unavailable.
				 */
				inline const std::vector<Sku>& GetUnavailableSkus() const { return m_unavailableSkus; }
				/**
				 * Sets the SKUs provided in the original request to
				 * {@link IapClient#GetProductData()} for which product data is
				 * not available. Product data can be unavailable if the provided SKU is not
				 * defined or if there was a problem retrieving the data. Internally used.
				 *
				 * @param unavailableSkus The vector of SKUs provided in the original request for which
				 *        product data is unavailable.
				 */
				inline void SetUnavailableSkus(const std::vector<Sku>& unavailableSkus) { m_unavailableSkus = unavailableSkus; }
				inline void SetUnavailableSkus(std::vector<Sku>&& unavailableSkus) { m_unavailableSkus = unavailableSkus; }

				/**
				 * Returns product data, keyed by SKU.
				 *
				 * @return Product data, keyed by SKU.
				 */
				inline const std::map<Sku, Product>& GetProducts() const { return m_products; }
				/**
				 * Sets product data, keyed by SKU. Internally used.
				 *
				 * @param products Product data, keyed by SKU.
				 */
				inline void SetProducts(const std::map<Sku, Product>& products) { m_products = products; }
				inline void SetProducts(std::map<Sku, Product>&& products) { m_products = products; }
			private:
				std::vector<Sku> m_unavailableSkus;
				std::map<Sku, Product> m_products;
			};
		}
	}
}
