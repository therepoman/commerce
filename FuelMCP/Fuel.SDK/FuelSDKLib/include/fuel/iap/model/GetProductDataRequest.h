#pragma once

#include <vector>

#include "Types.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents a request to {@link IapClient#GetProductData()}.
			*/
			class GetProductDataRequest 
			{
			public:
				/**
				* Returns one or more SKUs
				* @return Returns a vector of SKUs
				*/
				inline const std::vector<Sku>& GetSkus() const { return m_skus; }

				/**
				* Sets one or more SKUs
				* @param skus, A vector of SKUs
				*/
				inline void SetSkus(const std::vector<Sku>& skus) { m_skus = skus; }
				inline void SetSkus(std::vector<Sku>&& skus) { m_skus = skus; }

			private:
				std::vector<Sku> m_skus;
			};
		}
	}
}