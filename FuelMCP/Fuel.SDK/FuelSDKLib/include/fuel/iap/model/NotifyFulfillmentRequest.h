#pragma once

#include "Types.h"
#include "Credentials.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents a request to {@link IapClient#NotifyFulfillment()}.
			*/
			class NotifyFulfillmentRequest
			{
			public:
				/**
				* Gets the receipt ID, a unique identifer of a purchase. It can be used from an
				* external server to validate a purchase.
				*
				* @return Receipt ID, a string
				*/
				inline const Fuel::String& GetReceiptId() const { return m_receiptId; }
				/**
				* Sets the receipt ID, a unique identifer of a purchase. It can be used from an
				* external server to validate a purchase.
				*
				* @param receiptId, a string
				*/
				inline void SetReceipt(const Fuel::String& receiptId) { m_receiptId = receiptId; }

				/**
				* Gets the fulfillment result
				*
				* @return A {@link FulfillmentResult} enum
				*/
				inline FulfillmentResult GetFulfillmentResult() const { return m_fulfillmentResult; }
				/**
				* Sets the fulfillment result
				*
				* @param fulfillmentResult, A {@link FulfillmentResult} enum
				*/
				inline void SetFulfillmentResult(FulfillmentResult fulfillmentResult) { m_fulfillmentResult = fulfillmentResult; }
			private:
				Fuel::String m_receiptId;
				FulfillmentResult m_fulfillmentResult;
			};
		}
	}
}