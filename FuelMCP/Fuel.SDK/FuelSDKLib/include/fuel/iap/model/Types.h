#pragma once

#include <string>
#include "../../core/Types.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			typedef Fuel::String Sku;

			/**
			* Represents a sync point (a specific time when purchase updates were synced with the backend
			* service), which contains:
			*
			* A sync token - An opaque string that the backend services creates and uses to determine
			* which purchase updates were synced.
			*
			* A timestamp - The time when the sync token was created.
			*/
			class SyncPointData
			{
			public:
				/**
				* Gets the token
				*
				* @return Sync token, a string
				*/
				const String& GetSyncToken() const { return m_syncToken; }
				/**
				* Sets the token
				*
				* @param syncToken, a string
				*/
				void SetSyncToken(const String& syncToken) { m_syncToken = syncToken; }

				/**
				* Gets the token's timestamp
				*
				* @return The timestamp, a {@link Fuel::DateTime} object
				*/
				const DateTime& GetTimestamp() const { return m_timestamp; }
				/**
				* Sets the token's timestamp
				*
				* @return timestamp, a {@link Fuel::DateTime} object
				*/
				void SetTimestamp(const DateTime& timestamp) { m_timestamp = timestamp; }
			private:
				String m_syncToken;
				DateTime m_timestamp;
			};

			enum class ProductType
			{
				Consumable = 0, /**< Can be purchased multiple times by a customer. */
				Entitlement = 1, /**< Can be purchased only once bya customer. */
				Subscription = 2, /**< Can be used only for a specified period of time. Subscriptions can be renewed. */
				Unknown = 1000 /**< Unknown product type. Used for error handling on the back end. */
			};

			/**
			* Represents the fulfillment result for an in-app purchase. Used in a {@link NotifyFulfillmentRequest}
			*/
			enum class FulfillmentResult
			{
				Fulfilled, /**< App successfully fulfilled the purchase */
				Unavailable /**< Purchase can never be fulfilled by the app */
			};
		}
	}
}
