#pragma once

#include "Types.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents a request to {@link IapClient#GetSession()}.
			*/
			class GetSessionRequest
			{
			public:
				/**
				* Gets the game developer's client id. See
				* https://github.com/justintv/Twitch-API/blob/master/authentication.md for
				* discussion of the client id.
				*
				* @return client id, a string
				*/
				inline const String& GetClientId() const { return m_clientId; }

				/**
				* Sets the game developer's client id. See
				* https://github.com/justintv/Twitch-API/blob/master/authentication.md for
				* discussion of the client id.
				*
				* @param clientId, a string
				*/
				inline void SetClientId(const String clientId) { m_clientId = clientId; }
			private:
				String m_clientId;
			};
		}
	}
}