#pragma once

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents the Twitch user logged into the Twitch launcher
			*/
			class TwitchUser
			{
			public:
				/**
				* Gets a Twitch OAuth token specific to the game (linked to the game developer's client ID)
				* that identifies the Twitch user logged into the launcher and the client. This token may be
				* given to the SDK through the {@link IapClient#AddCredentials()} API or used with Twitch web
				* services.
				*
				* @return the token, or empty string if none was found
				*/
				const String& GetAccessToken() const { return m_accessToken; }
				/**
				* Sets a Twitch OAuth token specific to the game (linked to the game developer's client ID)
				* that identifies the Twitch user logged into the launcher and the client. Used internally.
				*
				* @param accessToken, a string
				*/
				void SetAccessToken(const String& accessToken) { m_accessToken = accessToken; }

				/**
				* Gets the Twitch user's display image.
				*
				* @return a URL to the display image, or empty string if none found
				*/
				const String& GetLogo() const { return m_logo; }
				/**
				* Sets the Twitch user's display image. Used internally.
				*
				* @param logo, a URL to the display image
				*/
				void SetLogo(const String& logo) { m_logo = logo; }

				/**
				* Gets the Twitch user's username.
				*
				* @return the Twitch user's username or empty string if none found
				*/
				const String& GetUsername() const { return m_username; }
				/**
				* Sets the Twitch user's username. Used internally.
				*
				* @param username, a string
				*/
				void SetUsername(const String& username) { m_username = username; }

				/**
				* Gets the Twitch user's display name.
				*
				* @return the Twitch user's display name or empty string if none found
				*/
				const String& GetDisplayName() const { return m_displayName; }
				/**
				* Sets the Twitch user's display name. Used internally.
				*
				* @param displayname, a string
				*/
				void SetDisplayName(const String& displayName) { m_displayName = displayName; }

				/**
				* Gets the Twitch user's id (tuid).
				*
				* @return the Twitch user's id or empty string if none found
				*/
				const String& GetTuid() const { return m_tuid; }
				/**
				* Sets the Twitch user's id (tuid).
				*
				* @param tuid, a string
				*/
				void SetTuid(const String& tuid) { m_tuid = tuid; }
			private:
				String m_accessToken;
				String m_logo;
				String m_username;
				String m_displayName;
				String m_tuid;
			};
		}
	}
}