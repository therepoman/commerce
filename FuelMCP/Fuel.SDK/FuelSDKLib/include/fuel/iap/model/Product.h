#pragma once

#include <string>

#include "Types.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			 * Represents an in-app product, such as consumable content, entitlement content,
			 * or subscription content. Retrieve product data by using
			 * {@link IapClient#GetProductData()}.
			 */
			class Product
			{
			public:
				/**
				 * Returns the stock-keeping unit (SKU) of the product. A SKU is a unique
				 * identifier for a purchasable product. A SKU is unique to each developer
				 * account with the Amazon Appstore. It is a case-sensitive
				 * string containing only alphanumerics,
				 * underscores, periods, and dashes. It must be 150 characters or less.
				 *
				 * @return The product SKU.
				 */
				inline const Sku& GetSku() const { return m_sku; }
				/**
				 * Sets the stock-keeping unit (SKU) of the product. A SKU is a unique
				 * identifier for a purchasable product. A SKU is unique to each developer
				 * account with the Amazon Appstore. It is a case-sensitive
				 * string containing only alphanumerics,
				 * underscores, periods, and dashes. It must be 150 characters or less.
				 *
				 * @param sku The product SKU.
				 */
				inline void SetSku(const Sku& sku) { m_sku = sku; }
				inline void SetSku(Sku&& sku) { m_sku = sku; }

				/**
				 * Returns the localized description of the product. If no description for
				 * the locale, returns the default (US English).
				 *
				 * @return The localized product description.
				 */
				inline const Fuel::String& GetDescription() const { return m_description; }
				/**
				 * Sets the localized description of the product. If no description for
				 * the locale, returns the default (US English).
				 *
				 * @param description The localized product description.
				 */
				inline void SetDescription(const Fuel::String& description) { m_description = description; }
				inline void SetDescription(Fuel::String&& description) { m_description = description; }

				/**
				 * Returns the localized description of the product. If no description for
				 * the locale, returns the default (US English).
				 *
				 * @return The localized product description.
				 */
				inline const Fuel::String& GetPrice() const { return m_price; }
				/**
				 * Sets the localized description of the product. If no description for
				 * the locale, returns the default (US English).
				 *
				 * @param price The localized product description.
				 */
				inline void SetPrice(const Fuel::String& price) { m_price = price; }
				inline void SetPrice(Fuel::String&& price) { m_price = price; }

				/**
				 * Returns the url of the small icon.
				 *
				 * @return The small icon URL for the product.
				 */
				inline const Fuel::String& GetSmallIconUrl() const { return m_smallIconUrl; }
				/**
				 * Sets the url of the small icon.
				 *
				 * @param smallIconUrl The small icon URL for the product.
				 */
				inline void SetSmallIconUrl(const Fuel::String& smallIconUrl) { m_smallIconUrl = smallIconUrl; }
				inline void SetSmallIconUrl(Fuel::String&& smallIconUrl) { m_smallIconUrl = smallIconUrl; }

				/**
				 * Returns the localized title of the product. If no title for the locale,
				 * returns the default (US English).
				 *
				 * @return The localized product title.
				 */
				inline const Fuel::String& GetTitle() const { return m_title; }
				/**
				 * Sets the localized title of the product. If no title for the locale,
				 * returns the default (US English).
				 *
				 * @param title The localized product title.
				 */
				inline void SetTitle(const Fuel::String& title) { m_title = title; }
				inline void SetTitle(Fuel::String&& title) { m_title = title; }

				/**
				 * Returns the type of product.
				 *
				 * @return The product type.
				 */
				inline ProductType GetProductType() const { return m_productType; }
				/**
				 * Sets the type of product.
				 *
				 * @param productType The product type.
				 */
				inline void SetProductType(ProductType productType) { m_productType = productType; }
			private:
				Sku m_sku;
				Fuel::String m_description;
				Fuel::String m_price;
				Fuel::String m_smallIconUrl;
				Fuel::String m_title;
				ProductType m_productType;
			};
		}
	}
}
