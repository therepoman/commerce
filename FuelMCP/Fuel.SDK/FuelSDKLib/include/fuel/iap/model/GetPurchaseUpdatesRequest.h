#pragma once

#include "Types.h"
#include "Credentials.h"

namespace Fuel
{
	class Message;

	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents a request to {@link IapClient#GetPurchaseUpdates()}, in which the user requests
			* purchase updates based on a prior sync point. A sync point is a specific time when purchase
			* updates were synced with the backend service.
			*/
			class GetPurchaseUpdatesRequest
			{
			public:
				/**
				* Returns the sync point
				* @return Returns a {@link SyncPointData} object
				*/
				const SyncPointData& GetSyncPoint() const { return m_syncPoint; }

				/**
				* Sets the sync point
				* @param syncPoint, a {@link SyncPointData} object
				*/
				void SetSyncPoint(const SyncPointData& syncPoint) { m_syncPoint = syncPoint; }
			private:
				SyncPointData m_syncPoint;
			};
		}
	}
}
