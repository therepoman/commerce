#pragma once

namespace Fuel
{
	namespace Iap
	{
		/**
		 * A list of error cases returned the the IapClient.
		 */
		enum class IapErrors : int
		{
			// From core errors
			SUCCESS = 0, /**< Operation completed successfully */
			MISSING_DLL = 1, /**< SDK DLL was missing */
			CORRUPT_DLL = 2, /**< SDK DLL is corrupted */
			INCOMPATIBLE_SDK_VERSION = 3, /**< SDK DLL is incompatible with the library */
			NOT_INITIALIZED = 4, /**< Initialize was not called */
			ALREADY_INITIALIZED = 5, /**< Request was already initialized; call destroy before initializing again */
			INVALID_RENDERER = 6, /**< An invalid renderer type was provided */
			INVALID_ARGUMENT = 7, /**< One or more arguments were set to NULL */
			INTERNAL_FAILURE = 8, /**< Unknown internal failure */
			REQUEST_TOO_LARGE = 9, /**< The size of the request was too large */
			INVALID_SIGNATURE = 10, /**< Unable to validate the SDK DLL signature */
			ACCESS_DENIED = 11, /**< Access to the API was denied; e.g., if the game cannot be verified */
			TIMED_OUT = 12, /**< A timeout exception occurred. This could be due to a bad network connection */
			HOOK_FAILED = 13, /**< Failed to setup the hooks necessary to draw the overlay. This could be due to incompatible DirectX dll*/

			// IAP specific errors
			CREDENTIALS_REQUIRED = 100, /**< Identifying and bearer credentials were missing */
			INVALID_CREDENTIALS = 101, /**< Identifying and bearer credentials were invalid */
			CANCELLED = 102, /**< User cancelled the request */
			PURCHASE_PROCESSING = 103 /*< The purchase is still processing. Once it completes, use {@link IapClient#GetPurchaseUpdates()} to get the receipt ID.*/
		};
	}
}