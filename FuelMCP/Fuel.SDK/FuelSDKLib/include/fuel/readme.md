## Usage ## {#mainpage}

The FuelSDK is packaged in two folders.
- include - all .h files required by the client game
- fuel - all binary files used at runtime

From the client game's perspective, the Fuel Commerce SDK is a header only library. One .h file is added to the game's code to
establish a connection to the SDK's .dll in the fuel folder.

### IapClient Integration ###
-# Add an include directory entry to the project for the FuelSDK's include path
-# Add `#include <fuel_impl/IapClient.h>` to at least one .cpp file in the project
-# Add a post build step to copy the contents of the FuelSDK's bin directory to a child directory fuel in the Game's directory

### IapClient Usage ###
Documentation on IapClient is located [here](@ref Fuel::Iap::IapClient "IapClient").

```
// Create an IapClient instance
auto createResponse = Fuel::Iap::IapClient::Create("VENDOR_ID", "1.0", Fuel::RendererType::DIRECT_X_11);
if (!createResponse.IsSuccess())
{
	std::cout << createResponse.GetError().GetMessage().c_str() << std::endl;
	return;
}

// extract the IapClient pointer from the response
auto iapClient = createResponse.GetResponseWithOwnership();

// request product data
std::vector<Fuel::Iap::Model::Sku> skus = { L"SKU-1234" };
Fuel::Iap::Model::GetProductDataRequest request;
request.SetSkus(skus);

auto getProductDataResponse = iapClient->GetProductData(request);
if (!getProductDataResponse.IsSuccess())
{
	std::cout << getProductDataResponse.GetError().GetMessage().c_str() << std::endl;
	return;
}
```


## Dependencies ##

Both the x86 and x64 MS Visual C++ 2013 runtime libraries are needed.  They can be installed from https://www.microsoft.com/en-us/download/details.aspx?id=40784.
