#pragma once

#include "types.h"

namespace Fuel
{
	/*
	 * Provides a container to return a response and possibly an error from a
	 * request. A success flag indicates whether the Outcome was created with a
	 * response or an error. See {@link IapClient} to see how Outcome is used
	 * as a wrapper.
	 */
	template<class R, typename E>
	class Outcome
	{
		R m_response;
		E m_error;
		bool m_success;
	public:
		/*
		 * Creates a new Outcome with a specific response.
		 * @param response The response wrapped in the new Outcome
		 */
		Outcome(R&& response) :
			m_response(std::forward<R>(response)),
			m_success(true)
		{}

		/*
		 * Creates a new Outcome with an error.
		 * @param error The error wrapped in the new Outcome
		 */
		Outcome(E&& error) :
			m_error(std::forward<E>(error)),
			m_success(false)
		{}

		Outcome(Outcome&& rhs) :
			m_response(std::move(rhs.m_response)),
			m_error(std::move(rhs.m_error)),
			m_success(rhs.m_success)
		{}

		// don't allow copy constructors or equals operators
		Outcome(const Outcome& rhs) = delete;
		Outcome& operator=(const Outcome& rhs) = delete;

		/*
		 * Retrieves the Outcome's response
		 * @return A const reference to the response
		 */
		const R& GetResponse() const { return m_response; }

		/*
		 * Retrieves the Outcome's error
		 * @return A const reference to the error
		 */
		const E& GetError() const { return m_error; }

		/*
		 * Retrieves an rvalue reference to the contained response. This rvalue
		 * reference can be used to move the Response, if it supports move semantics.
		 * @return An rvalue reference to the contained response.
		 */
		R&& GetResponseWithOwnership() { return std::move(m_response); }

		/*
		 * Checks success state of the outcome
		 * @return Returns true if the Outcome was created with a response, false otherwise.
		 */
		bool IsSuccess() const { return m_success; }
	};
}
