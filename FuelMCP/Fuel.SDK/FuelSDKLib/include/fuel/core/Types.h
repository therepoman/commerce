#pragma once

#include <string>
#include <chrono>

namespace Fuel
{
	typedef std::wstring String;
	typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> DateTime;

	enum RendererType {
		DIRECT_X_11, DIRECT_X_12
	};
}