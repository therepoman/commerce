#pragma once

#include "Types.h"

namespace Fuel
{
	/*
	 * Represents a list of shared error codes that can be returned from the SDK.
	 */
	enum class ResultCodes : int
	{
		SUCCESS = 0, /**< Operation completed successfully */

		// initialization failures
		MISSING_DLL = 1, /**< SDK DLL was missing */
		CORRUPT_DLL = 2, /**< SDK DLL is corrupted */
		INCOMPATIBLE_SDK_VERSION = 3, /**< SDK DLL is incompatible with the library */
		NOT_INITIALIZED = 4, /**< Initialize was not called */
		ALREADY_INITIALIZED = 5, /**< Request was already initialized; call destroy before initializing again */
		INVALID_RENDERER = 6, /**< An invalid renderer type was provided */
		INVALID_SIGNATURE = 10, /**< Unable to validate the SDK DLL signature */
		ACCESS_DENIED = 11, /**< Access to the API was denied; e.g., if the game cannot be verified */
		TIMED_OUT = 12, /**< A timeout exception occurred. This could be due to a bad network connection */
		HOOK_FAILED = 13, /**< Failed to setup the hooks necessary to draw the overlay. This could be due to incompatible DirectX dll*/

		// general failures
		INVALID_ARGUMENT = 7, /**< One or more arguments were set to NULL */
		INTERNAL_FAILURE = 8, /**< Unknown internal failure */
		REQUEST_TOO_LARGE = 9 /**< The size of the request was too large */
	};

	/*
	 * Represents a common error structure encoding the failed result of a request.
	 */
	template<typename ErrorType>
	class Error
	{
		ErrorType m_type;
		Fuel::String m_message;
		bool m_retryable;
	public:
		Error() :
			m_retryable(false)
		{}

		Error(ErrorType type, const Fuel::String& message, bool retryable) :
			m_type(type),
			m_message(message),
			m_retryable(retryable)
		{}

		/*
		 * Retrieves the type of the error.
		 * @return A value indicating the type of error. Often an enum class.
		 */
		ErrorType GetType() const { return m_type; }

		/*
		 * Retrieves the string error message attached to the error's type
		 * @return A string containing a human readable error message.
		 */
		const Fuel::String& GetMessage() const { return m_message; }

		/*
		 * Determines if the request might succeed if it were tried again
		 * @return A boolean flag indicating if the request should be retried.
		 */
		bool IsRetryable() const { return m_retryable; }
	};
}
