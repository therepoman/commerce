#pragma once

#include <memory>

#include "core/Outcome.h"
#include "core/Error.h"

#include "iap/Errors.h"
#include "iap/model/Credentials.h"
#include "iap/model/GetProductDataRequest.h"
#include "iap/model/GetProductDataResponse.h"
#include "iap/model/PurchaseRequest.h"
#include "iap/model/PurchaseResponse.h"
#include "iap/model/GetPurchaseUpdatesRequest.h"
#include "iap/model/GetPurchaseUpdatesResponse.h"
#include "iap/model/NotifyFulfillmentRequest.h"
#include "iap/model/NotifyFulfillmentResponse.h"
#include "iap/model/GetSessionRequest.h"
#include "iap/model/GetSessionResponse.h"

namespace Fuel
{
	// Major and minor number should be increment when there is a compatibility effecting change
	// Build and revision number will automatically be incremented by the build server
	const std::string SDK_VERSION = "1.0.0.0";

	namespace Iap
	{
		/**
		 * Presents the primary API for interacting with the IAP framework.
		 */
		class IapClient
		{
		public:
			~IapClient();

			/**
			 * Outcome wrapping a std::unique_ptr containing an IapClient.
			 */
			using CreateIapClientOutcome = Outcome<std::unique_ptr<IapClient>, Error<ResultCodes>>;
			/**
			 * Initializes a new instance of the SDK using a specific game version.
			 * Take ownership of the std::unique_ptr using std::move.
			 *
			 * @return Outcome containing a std::unique_ptr of an IapClient.
			 */
			static CreateIapClientOutcome Create(const std::string& vendorId, const std::string& gameVersion, Fuel::RendererType rendererType);

			/**
			 * Adds credentials to the service. Credentials are keyed by their type, so setting
			 * credentials of a specific type will overwrite previous credentials of that type.
			 *
			 * @param credentials Credentials object
			 */
			void AddCredentials(const Fuel::Iap::Model::Credentials& credentials);
			
			/**
			 * Outcome wrapping a Model::GetProductDataResponse.
			 */
			using GetProductDataOutcome = Outcome<Model::GetProductDataResponse, Error<IapErrors>>;
			/**
			 * Initiates a request to retrieve item data of up to one-hundred SKUs. The information retrieved
			 * can be used when displaying products that the developer is selling to the user, or to validate
			 * products that the developer has in their store.
			 *
			 * @param request GetProductDataRequest object
			 * @return Outcome containing the Model::GetProductDataResponse which holds the information
			 *         about the requested SKUs.
			 */
			GetProductDataOutcome GetProductData(const Model::GetProductDataRequest& request) const;

			/**
			 * Outcome wrapping a Model::PurchaseResponse.
			 */
			using PurchaseOutcome = Outcome<Model::PurchaseResponse, Error<IapErrors>>;
			/**
			 * Initiates a purchase-flow for a product.
			 *
			 * During the purchase-flow, an overlay is displayed. Once the purchase is complete, the
			 * developer must fulfill the orders if possible, then mark the receipts as fulfilled or
			 * unavailable using IapClient::NotifyFulfillment.
			 *
			 * Purchase must be called on a separate thread, so the game can continue running on the
			 * render thread while the purchase is being completed. For the duration of the Purchase call,
			 * mouse and keyboard input is obtained using WH_MOUSE and WH_KEYBOARD hooks.  The relevant
			 * input messages are consumed by the hooks and will not reach the game's message loop.
			 * If the game is hooking at a lower level than this (e.g. using raw input or a library that
			 * does so), the game should ignore mouse and keyboard input for the duration of the
			 * Purchase method invocation, to avoid acting on input that was not intended for the game.
			 *
			 * For DirectX, the overlay is rendered by hooking IDXGISwapChain::Present. If the swap chain
			 * buffer size does not match the client area of the target window, the overlay may appear
			 * stretched or blurry.
			 *
			 * @param request PurchaseRequest object
			 * @return Outcome containing the Model::PurchaseResponse which holds the user's purchases
			 */
			PurchaseOutcome Purchase(const Model::PurchaseRequest& request) const;

			/**
			 * Outcome wrapping a Model::GetPurchaseUpdatesResponse.
			 */
			using GetPurchaseUpdatesOutcome = Outcome<Model::GetPurchaseUpdatesResponse, Error<IapErrors>>;
			/**
			 * Initiates a request to retrieve updates about items the customer has purchased or canceled.
			 * The request includes a sync token, an opaque string that the backend services creat and use to
			 * determine which purchase updates were synced. The first time GetPurchaseUpdates is called, the
			 * {@link SyncPointData} object inside the request should have an empty sync token. The response
			 * will contain a new SyncPointData object, which can be used in the request of the next
			 * call to GetPurchaseUpdates. In each subsequent call to GetPurchaseUpdates, the SyncPointData
			 * object returned in the reponse of the last call can be used in the request for the next.
			 *
			 * Once the developer has all the receipts, they can fulfill or revoke those orders, if they have
			 * not done so previously. Then the developer should mark the order as fulfilled or unavailable,
			 * using NotifyFulfillment. Once a consumable order is marked as fulfilled, it no longer is returned
			 * by GetPurchaseUpdates.
			 *
			 * @param request GetPurchaseUpdatesRequest object
			 * @return Outcome containing the Model::GetPurchaseUpdatesResponse which holds the updates of items
			 *         that a user has purchased and/or canceled since the last sync point. Depends on whether the
			 *         sync point has an empty sync token:
			 *          - If yes, returns every purchase update recorded, up until the time the request was made
			 *          - If no, returns every purchase from the last GetPurchaseUpdates request based on the
			 *            specified sync point, up until the request was made.
			 */
			GetPurchaseUpdatesOutcome GetPurchaseUpdates(const Model::GetPurchaseUpdatesRequest& request) const;

			/**
			 * Outcome of NotifyFulfillment.
			 */
			using NotifyFulfillmentOutcome = Outcome<Model::NotifyFulfillmentResponse, Error<IapErrors>>;
			/**
			* Marks whether the user's order was successfully fulfilled by the developer.
			*
			* @param request NotifyFulfillmentRequest object
			* @return Outcome containing the Model::NotifyFulfillment response
			*/
			NotifyFulfillmentOutcome NotifyFulfillment(const Model::NotifyFulfillmentRequest& request) const;

			/**
			* Outcome of GetSession
			*/
			using GetSessionOutcome = Outcome<Model::GetSessionResponse, Error<IapErrors>>;
			/**
			* Get's the current game session if launched from the Twitch launcher.
			* Can be used to get the game entitlement as well as information about the current Twitch user
			* including an OAuth access token.
			*
			* @param request GetSessionRequest object
			* @return Outcome containing the Model::GetSession response which contains the game session
			*         from the Twitch Launcher.
			*/
			GetSessionOutcome GetSession(const Model::GetSessionRequest& request) const;
			
		private:
			class Impl;
			IapClient(std::unique_ptr<Impl>&& pImpl);
			std::unique_ptr<Impl> m_pImpl;
		};
	}
}