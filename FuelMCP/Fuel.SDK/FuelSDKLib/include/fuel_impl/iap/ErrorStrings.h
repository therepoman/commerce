#pragma once

#include "../../fuel/core/Types.h"
#include "../../fuel/iap/Errors.h"
#include "../core/ErrorStrings.h"

namespace Fuel
{
	namespace Iap
	{
		inline const wchar_t* GetErrorString(IapErrors errorCode)
		{
			switch (errorCode)
			{
				case IapErrors::CANCELLED:
					return L"Purchase cancelled";
				case IapErrors::CREDENTIALS_REQUIRED:
					return L"Credentials required";
				case IapErrors::INVALID_CREDENTIALS:
					return L"Invalid credentials";
				case IapErrors::PURCHASE_PROCESSING:
					return L"Purchase still processing";
			}

			return Fuel::GetErrorString((Fuel::ResultCodes)errorCode);
		}
	}
}