#pragma once

#include "../DllWrapper.h"
#include "ModelConverter.h"

#include "../core/ErrorStrings.h"
#include "ErrorStrings.h"

namespace Fuel
{
	namespace Iap
	{
		class IapClient::Impl
		{
		public:
			Impl(std::unique_ptr<Internal::DllWrapper>& dllWrapper) : 
				m_dllWrapper(std::move(dllWrapper))
			{
			}

			void AddCredentials(const Fuel::Iap::Model::Credentials& credentials);

			GetProductDataOutcome GetProductData(const Model::GetProductDataRequest& request) const;
			PurchaseOutcome Purchase(const Model::PurchaseRequest& request) const;
			GetPurchaseUpdatesOutcome GetPurchaseUpdates(const Model::GetPurchaseUpdatesRequest& request) const;
			NotifyFulfillmentOutcome NotifyFulfillment(const Model::NotifyFulfillmentRequest& request) const;
			GetSessionOutcome GetSession(const Model::GetSessionRequest& request) const;
		private:
			template<class T> 
			static T BuildErrorOutcome(Iap::IapErrors error, bool retryable = false)
			{
				return T(Fuel::Error<Fuel::Iap::IapErrors>(
					error,
					GetErrorString(error),
					retryable));
			}

			std::unique_ptr<Internal::DllWrapper> m_dllWrapper;
			Fuel::Iap::Model::CredentialsMap m_credentials;
			ModelConverter m_converter;
		};

		inline IapClient::CreateIapClientOutcome IapClient::Create(const std::string& vendorId, const std::string& gameVersion, Fuel::RendererType rendererType)
		{
			std::unique_ptr<Internal::DllWrapper> dllWrapper;
			auto wrapperStatus = Internal::DllWrapper::Create(vendorId, gameVersion, Fuel::SDK_VERSION, rendererType, dllWrapper);
			if (wrapperStatus != ResultCodes::SUCCESS)
			{
				return IapClient::CreateIapClientOutcome(Error<ResultCodes>(wrapperStatus, Fuel::GetErrorString(wrapperStatus), false));
			}

			// create the IapClient and it's Impl
			std::unique_ptr<IapClient::Impl> impl(new IapClient::Impl(dllWrapper));
			std::unique_ptr<IapClient> client(new IapClient(std::move(impl)));

			return IapClient::CreateIapClientOutcome(std::move(client));
		}

		inline IapClient::IapClient(std::unique_ptr<IapClient::Impl>&& pImpl) :
			m_pImpl(std::move(pImpl))
		{
		}

		// default implementation of destructor placed here to avoid a compile error with std::unique_ptr
		// using an incomplete type
		inline IapClient::~IapClient() = default;

		inline void IapClient::AddCredentials(const Fuel::Iap::Model::Credentials& credentials)
		{
			m_pImpl->AddCredentials(credentials);
		}

		inline IapClient::GetProductDataOutcome IapClient::GetProductData(const Model::GetProductDataRequest& request) const
		{
			return m_pImpl->GetProductData(request);
		}

		inline IapClient::PurchaseOutcome IapClient::Purchase(const Model::PurchaseRequest& request) const
		{
			return m_pImpl->Purchase(request);
		}

		inline IapClient::GetPurchaseUpdatesOutcome IapClient::GetPurchaseUpdates(const Model::GetPurchaseUpdatesRequest& request) const
		{
			return m_pImpl->GetPurchaseUpdates(request);
		}

		inline IapClient::NotifyFulfillmentOutcome IapClient::NotifyFulfillment(const Model::NotifyFulfillmentRequest& request) const
		{
			return m_pImpl->NotifyFulfillment(request);
		}

		inline IapClient::GetSessionOutcome IapClient::GetSession(const Model::GetSessionRequest& request) const
		{
			return m_pImpl->GetSession(request);
		}
	}
}

using namespace Fuel::Iap;

inline void IapClient::Impl::AddCredentials(const Fuel::Iap::Model::Credentials& credentials)
{
	auto iterator = m_credentials.find(credentials.GetType());
	if (iterator != m_credentials.end())
	{
		(*iterator).second = credentials;
	}
	else
	{
		m_credentials.insert(Fuel::Iap::Model::CredentialsMap::value_type(credentials.GetType(), credentials));
	}
}

inline IapClient::PurchaseOutcome IapClient::Impl::Purchase(const Model::PurchaseRequest& request) const
{
	// validate credentials were provided
	if (m_credentials.size() == 0)
	{
		return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::CREDENTIALS_REQUIRED);
	}

	auto fbb = m_converter.ToBuffer(m_credentials, request);

	Internal::DllWrapper::ResponseCallbackWrapper responseData;
	auto result = m_dllWrapper->SendRequestResponse(Fuel::Messaging::Message::Message_PurchaseRequest,
		fbb->GetSize(),
		fbb->GetBufferPointer(),
		responseData);
	if (result != Fuel::ResultCodes::SUCCESS)
	{
		return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	responseData.Wait();
	if (responseData.GetType() == Messaging::Message::Message_FatalError)
	{
		return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	auto response = m_converter.ToMessage<Fuel::Messaging::PurchaseResponse>(Messaging::Message::Message_PurchaseResponse, responseData.GetData());
	switch (response->status())
	{
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_Success:
			return IapClient::PurchaseOutcome(m_converter.ToModel(response));
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_CredentialsRequired:
			return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::CREDENTIALS_REQUIRED);
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_InvalidCredentials:
			return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::INVALID_CREDENTIALS);
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_Cancelled:
			return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::CANCELLED);
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_InvalidArgument:
			return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::INVALID_ARGUMENT);
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_Processing:
			return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::PURCHASE_PROCESSING);
		default:
			return BuildErrorOutcome<IapClient::PurchaseOutcome>(IapErrors::INTERNAL_FAILURE, true);
	}
}

inline IapClient::GetProductDataOutcome IapClient::Impl::GetProductData(const Model::GetProductDataRequest & request) const
{
	auto fbb = m_converter.ToBuffer(m_credentials, request);

	Internal::DllWrapper::ResponseCallbackWrapper responseData;
	auto result = m_dllWrapper->SendRequestResponse(Fuel::Messaging::Message::Message_GetProductDataRequest, fbb->GetSize(), fbb->GetBufferPointer(), responseData);
	if (result != Fuel::ResultCodes::SUCCESS)
	{
		return BuildErrorOutcome<IapClient::GetProductDataOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	responseData.Wait();
	if (responseData.GetType() == Messaging::Message::Message_FatalError)
	{
		return BuildErrorOutcome<IapClient::GetProductDataOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	auto response = m_converter.ToMessage<Fuel::Messaging::GetProductDataResponse>(Messaging::Message::Message_GetProductDataResponse, responseData.GetData());
	switch (response->status())
	{
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_Success:
			return IapClient::GetProductDataOutcome(m_converter.ToModel(response));
		case Messaging::GetProductDataResponseStatus::GetProductDataResponseStatus_CredentialsRequired:
			return BuildErrorOutcome<IapClient::GetProductDataOutcome>(IapErrors::CREDENTIALS_REQUIRED);
		case Messaging::GetProductDataResponseStatus::GetProductDataResponseStatus_InvalidCredentials:
			return BuildErrorOutcome<IapClient::GetProductDataOutcome>(IapErrors::INVALID_CREDENTIALS);
		case Messaging::GetProductDataResponseStatus::GetProductDataResponseStatus_InvalidArgument:
			return BuildErrorOutcome<IapClient::GetProductDataOutcome>(IapErrors::INVALID_ARGUMENT);
		default:
			return BuildErrorOutcome<IapClient::GetProductDataOutcome>(IapErrors::INTERNAL_FAILURE, true);
	}
}

inline IapClient::GetPurchaseUpdatesOutcome IapClient::Impl::GetPurchaseUpdates(const Model::GetPurchaseUpdatesRequest& request) const
{
	auto fbb = m_converter.ToBuffer(m_credentials, request);

	Internal::DllWrapper::ResponseCallbackWrapper responseData;

	auto result = m_dllWrapper->SendRequestResponse(Fuel::Messaging::Message::Message_GetPurchaseUpdatesRequest, fbb->GetSize(), fbb->GetBufferPointer(), responseData);
	if (result != Fuel::ResultCodes::SUCCESS)
	{
		return BuildErrorOutcome<IapClient::GetPurchaseUpdatesOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	responseData.Wait();
	if (responseData.GetType() == Messaging::Message::Message_FatalError)
	{
		return BuildErrorOutcome<IapClient::GetPurchaseUpdatesOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	auto response = m_converter.ToMessage<Fuel::Messaging::GetPurchaseUpdatesResponse>(Messaging::Message::Message_GetPurchaseUpdatesResponse, responseData.GetData());
	switch (response->status())
	{
		case Messaging::GetPurchaseUpdatesResponseStatus::GetPurchaseUpdatesResponseStatus_Success:
			return IapClient::GetPurchaseUpdatesOutcome(m_converter.ToModel(response));
		case Messaging::GetPurchaseUpdatesResponseStatus::GetPurchaseUpdatesResponseStatus_InvalidCredentials:
			return BuildErrorOutcome<IapClient::GetPurchaseUpdatesOutcome>(IapErrors::INVALID_CREDENTIALS);
		case Messaging::GetPurchaseUpdatesResponseStatus::GetPurchaseUpdatesResponseStatus_CredentialsRequired:
			return BuildErrorOutcome<IapClient::GetPurchaseUpdatesOutcome>(IapErrors::CREDENTIALS_REQUIRED);
		case Messaging::GetPurchaseUpdatesResponseStatus::GetPurchaseUpdatesResponseStatus_TimedOut:
			return BuildErrorOutcome<IapClient::GetPurchaseUpdatesOutcome>(IapErrors::TIMED_OUT, true);
		default:
			return BuildErrorOutcome<IapClient::GetPurchaseUpdatesOutcome>(IapErrors::INTERNAL_FAILURE, true);
	}
}

inline IapClient::NotifyFulfillmentOutcome IapClient::Impl::NotifyFulfillment(const Model::NotifyFulfillmentRequest& request) const
{
	// validate credentials were provided
	if (m_credentials.size() == 0)
	{
		return IapClient::NotifyFulfillmentOutcome(Fuel::Error<Fuel::Iap::IapErrors>(
			IapErrors::CREDENTIALS_REQUIRED,
			L"Credentials are required for this call",
			false));
	}

	Internal::DllWrapper::ResponseCallbackWrapper responseData;
	
	auto fbb = m_converter.ToBuffer(m_credentials, request);

	auto result = m_dllWrapper->SendRequestResponse(Fuel::Messaging::Message::Message_NotifyFulfillmentRequest,
		fbb->GetSize(),
		fbb->GetBufferPointer(),
		responseData);
	if (result != Fuel::ResultCodes::SUCCESS)
	{
		return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	responseData.Wait();
	if (responseData.GetType() == Messaging::Message::Message_FatalError)
	{
		return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	auto response = m_converter.ToMessage<Fuel::Messaging::NotifyFulfillmentResponse>(Messaging::Message::Message_NotifyFulfillmentResponse, responseData.GetData());
	switch (response->status())
	{
		case Messaging::PurchaseResponseStatus::PurchaseResponseStatus_Success:
			return IapClient::NotifyFulfillmentOutcome(m_converter.ToModel(response));
		case Messaging::NotifyFulfillmentResponseStatus::NotifyFulfillmentResponseStatus_InvalidCredentials:
			return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::INVALID_CREDENTIALS);
		case Messaging::NotifyFulfillmentResponseStatus::NotifyFulfillmentResponseStatus_CredentialsRequired:
			return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::CREDENTIALS_REQUIRED);
		case Messaging::NotifyFulfillmentResponseStatus::NotifyFulfillmentResponseStatus_InvalidArgument:
			return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::INVALID_ARGUMENT);
		case Messaging::NotifyFulfillmentResponseStatus::NotifyFulfillmentResponseStatus_NonRetryableError:
			return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::INTERNAL_FAILURE);
		default:
			return BuildErrorOutcome<IapClient::NotifyFulfillmentOutcome>(IapErrors::INTERNAL_FAILURE, true);
	}
}

inline IapClient::GetSessionOutcome IapClient::Impl::GetSession(const Model::GetSessionRequest& request) const
{
	auto fbb = m_converter.ToBuffer(m_credentials, request);

	Internal::DllWrapper::ResponseCallbackWrapper responseData;

	auto result = m_dllWrapper->SendRequestResponse(Fuel::Messaging::Message::Message_GetSessionRequest,
		fbb->GetSize(),
		fbb->GetBufferPointer(),
		responseData);

	if (result != Fuel::ResultCodes::SUCCESS)
	{
		return BuildErrorOutcome<IapClient::GetSessionOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	responseData.Wait();
	if (responseData.GetType() == Messaging::Message::Message_FatalError)
	{
		return BuildErrorOutcome<IapClient::GetSessionOutcome>(IapErrors::INTERNAL_FAILURE);
	}

	auto response = m_converter.ToMessage<Fuel::Messaging::GetSessionResponse>(Messaging::Message::Message_GetSessionResponse, responseData.GetData());
	
	switch (response->status())
	{
	case Messaging::GetSessionResponseStatus::GetSessionResponseStatus_Success:
		return IapClient::GetSessionOutcome(m_converter.ToModel(response));
	case Messaging::GetSessionResponseStatus::GetSessionResponseStatus_AccessDenied:
		return BuildErrorOutcome<IapClient::GetSessionOutcome>(IapErrors::ACCESS_DENIED, false);
	default:
		return BuildErrorOutcome<IapClient::GetSessionOutcome>(IapErrors::INTERNAL_FAILURE, true);
	}
}