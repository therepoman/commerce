#pragma once

#include <string>
#include <codecvt>
#include "Fuel_generated.h"

#include "../../fuel/iap/model/GetProductDataRequest.h"
#include "../../fuel/iap/model/GetProductDataResponse.h"
#include "../../fuel/iap/model/PurchaseRequest.h"
#include "../../fuel/iap/model/PurchaseResponse.h"
#include "../../fuel/iap/model/Product.h"
#include "../../fuel/iap/model/Receipt.h"

namespace Fuel
{
	namespace Iap
	{
		class ModelConverter
		{
		public:
			template <class T>
			const T* ToMessage(Fuel::Messaging::Message message, const uint8_t* data) const
			{
				auto messageRoot = Fuel::Messaging::GetMessageRoot(data);
				if (messageRoot != NULL && messageRoot->message_type() == message)
				{
					return reinterpret_cast<const T*>(messageRoot->message());
				}
				return NULL;
			}

			// Methods to get specific FlatBuffer messages out of the serialized Message
			std::unique_ptr<flatbuffers::FlatBufferBuilder> ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::GetProductDataRequest& request) const;
			std::unique_ptr<flatbuffers::FlatBufferBuilder> ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::PurchaseRequest& modelRequest) const;
			std::unique_ptr<flatbuffers::FlatBufferBuilder> ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::NotifyFulfillmentRequest& request) const;
			std::unique_ptr<flatbuffers::FlatBufferBuilder> ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::GetPurchaseUpdatesRequest& request) const;
			std::unique_ptr<flatbuffers::FlatBufferBuilder> ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::GetSessionRequest& request) const;
			flatbuffers::Offset<Fuel::Messaging::CredentialsCache> ModelConverter::ToBuffer(flatbuffers::FlatBufferBuilder& fbb, const Fuel::Iap::Model::CredentialsMap& credentials) const;
			
			// Methods to convert between API classes and FlatBuffer classes.
			Fuel::Iap::Model::GetProductDataResponse ToModel(const Fuel::Messaging::GetProductDataResponse *bufferGetProductDataResponse) const;
			Fuel::Iap::Model::PurchaseResponse ToModel(const Fuel::Messaging::PurchaseResponse *bufferPurchaseResponse) const;
			Fuel::Iap::Model::Product ToModel(const Fuel::Messaging::Product *bufferProduct) const;
			Fuel::Iap::Model::ProductType ToModel(Fuel::Messaging::ProductType productType) const;
			Fuel::Iap::Model::Receipt ToModel(const Fuel::Messaging::Receipt *bufferReceipt) const;
			Fuel::Iap::Model::NotifyFulfillmentResponse ToModel(const Fuel::Messaging::NotifyFulfillmentResponse *bufferNotifyFulfillmentResponse) const;
			Fuel::Iap::Model::GetPurchaseUpdatesResponse ToModel(const Fuel::Messaging::GetPurchaseUpdatesResponse *buffer) const;
			Fuel::Iap::Model::TwitchUser ToModel(const Fuel::Messaging::TwitchUser *buffer) const;
			Fuel::Iap::Model::GetSessionResponse ToModel(const Fuel::Messaging::GetSessionResponse *buffer) const;

			// Methods to convert between standard data types.
			
			std::wstring ToModelWithNullAsBlank(const flatbuffers::String *str) const;
			std::string ToBuffer(const std::wstring &str) const;
		private:
			// Used to convert between char and wchar strings.
			mutable std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> m_utf8_16_converter;

		};
	}
}
using namespace Fuel::Iap;
inline std::unique_ptr<flatbuffers::FlatBufferBuilder> ModelConverter::ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::GetProductDataRequest& modelRequest) const
{
	std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());
	flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<flatbuffers::String>>> skuVector;
	if (!modelRequest.GetSkus().empty())
	{
		std::unique_ptr<flatbuffers::Offset<flatbuffers::String>[]> skuStrings(new flatbuffers::Offset<flatbuffers::String>[modelRequest.GetSkus().size()]);
		for (unsigned int i = 0; i < modelRequest.GetSkus().size(); i++) {
			skuStrings[i] = fbb->CreateString(ModelConverter::ToBuffer(modelRequest.GetSkus()[i]));
		}
		skuVector = fbb->CreateVector(skuStrings.get(), modelRequest.GetSkus().size());
	}

	auto request = Fuel::Messaging::CreateGetProductDataRequest(*fbb, 
		ToBuffer(*fbb, credentials),
		skuVector);

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(*fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_GetProductDataRequest);
	messageRootBuilder.add_message(request.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(*fbb, messageRoot);

	return fbb;
}

inline std::unique_ptr<flatbuffers::FlatBufferBuilder> ModelConverter::ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::PurchaseRequest& modelRequest) const
{
	std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());

	auto request = Fuel::Messaging::CreatePurchaseRequest(*fbb,
		ModelConverter::ToBuffer(*fbb, credentials),
		fbb->CreateString(ModelConverter::ToBuffer(modelRequest.GetSku())));

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(*fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_PurchaseRequest);
	messageRootBuilder.add_message(request.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(*fbb, messageRoot);

	return fbb;
}

inline std::unique_ptr<flatbuffers::FlatBufferBuilder> ModelConverter::ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::NotifyFulfillmentRequest& modelRequest) const
{
	std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());

	auto receiptId = fbb->CreateString(ModelConverter::ToBuffer(modelRequest.GetReceiptId()));

	auto request = Fuel::Messaging::CreateNotifyFulfillmentRequest(*fbb, 
		ModelConverter::ToBuffer(*fbb, credentials),
		receiptId, (Fuel::Messaging::FulfillmentResult)modelRequest.GetFulfillmentResult());

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(*fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message_NotifyFulfillmentRequest);
	messageRootBuilder.add_message(request.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(*fbb, messageRoot);

	return fbb;

}

inline std::unique_ptr<flatbuffers::FlatBufferBuilder> ModelConverter::ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::GetPurchaseUpdatesRequest& modelRequest) const
{
	std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());
	Fuel::Iap::Model::SyncPointData data = modelRequest.GetSyncPoint();

	auto syncToken = fbb->CreateString(ModelConverter::ToBuffer(data.GetSyncToken()));
	uint64_t syncTimestamp = data.GetTimestamp().time_since_epoch().count();
	auto syncPoint = Fuel::Messaging::CreateSyncPointData(*fbb, syncToken, syncTimestamp);
	auto request = Fuel::Messaging::CreateGetPurchaseUpdatesRequest(*fbb, ModelConverter::ToBuffer(*fbb, credentials), syncPoint);

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(*fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_GetPurchaseUpdatesRequest);
	messageRootBuilder.add_message(request.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(*fbb, messageRoot);

	return fbb;
}

inline std::unique_ptr<flatbuffers::FlatBufferBuilder> ModelConverter::ToBuffer(const Fuel::Iap::Model::CredentialsMap& credentials, const Fuel::Iap::Model::GetSessionRequest& modelRequest) const
{
	std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());
	auto clientId = fbb->CreateString(ModelConverter::ToBuffer(modelRequest.GetClientId()));
	auto request = Fuel::Messaging::CreateGetSessionRequest(*fbb, clientId);

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(*fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_GetSessionRequest);
	messageRootBuilder.add_message(request.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(*fbb, messageRoot);

	return fbb;
}

inline flatbuffers::Offset<Fuel::Messaging::CredentialsCache> ModelConverter::ToBuffer(flatbuffers::FlatBufferBuilder& fbb, const Fuel::Iap::Model::CredentialsMap& credentials) const
{
	std::vector<flatbuffers::Offset<Fuel::Messaging::Credential>> result(credentials.size());

	std::transform(credentials.begin(), credentials.end(), result.begin(), 
		[this, &fbb](const Fuel::Iap::Model::CredentialsMap::value_type& cred) {
			return Fuel::Messaging::CreateCredential(fbb,
				fbb.CreateString(ModelConverter::ToBuffer(cred.second.GetType())),
				fbb.CreateString(ModelConverter::ToBuffer(cred.second.GetIdentifier())),
				fbb.CreateString(ModelConverter::ToBuffer(cred.second.GetToken())));
		});

	return Fuel::Messaging::CreateCredentialsCache(fbb, fbb.CreateVector(result));
}

inline Fuel::Iap::Model::Product ModelConverter::ToModel(const Fuel::Messaging::Product *bufferProduct) const
{
	Fuel::Iap::Model::Product modelProduct;

	modelProduct.SetDescription(ModelConverter::ToModelWithNullAsBlank(bufferProduct->description()));
	modelProduct.SetPrice(ModelConverter::ToModelWithNullAsBlank(bufferProduct->price()));
	modelProduct.SetProductType(ModelConverter::ToModel(bufferProduct->productType()));
	modelProduct.SetSku(ModelConverter::ToModelWithNullAsBlank(bufferProduct->sku()));
	modelProduct.SetSmallIconUrl(ModelConverter::ToModelWithNullAsBlank(bufferProduct->smallIconUrl()));
	modelProduct.SetTitle(ModelConverter::ToModelWithNullAsBlank(bufferProduct->title()));


	return modelProduct;
}

inline std::wstring ModelConverter::ToModelWithNullAsBlank(const flatbuffers::String *str) const
{
	if (str == NULL) {
		return std::wstring();
	}
	return m_utf8_16_converter.from_bytes(str->str());
}

inline std::string ModelConverter::ToBuffer(const std::wstring &str) const
{
	return m_utf8_16_converter.to_bytes(str);
}

inline Fuel::Iap::Model::GetProductDataResponse ModelConverter::ToModel(const Fuel::Messaging::GetProductDataResponse *bufferGetProductDataResponse) const
{
	Fuel::Iap::Model::GetProductDataResponse result;

	// Pointer to someplace inside the buffer, no need to deallocate.
	auto response = bufferGetProductDataResponse;
	if (response != nullptr)
	{
		auto unavailableSkus = response->unavailableSkus();
		std::vector<Fuel::Iap::Model::Sku> skus;
		if (unavailableSkus != NULL)
		{
			for (unsigned int i = 0; i < unavailableSkus->size(); i++)
			{
				skus.push_back(ModelConverter::ToModelWithNullAsBlank(unavailableSkus->Get(i)));
			}
		}
		result.SetUnavailableSkus(std::move(skus));

		auto products = response->products();
		std::map<Fuel::Iap::Model::Sku, Fuel::Iap::Model::Product> resultProducts;
		if (products != NULL) {
			for (unsigned int i = 0; i < products->size(); i++)
			{
				auto product = ModelConverter::ToModel(products->Get(i));
				resultProducts.emplace(std::make_pair(product.GetSku(), product));
			}
		}
		result.SetProducts(std::move(resultProducts));
	}

	return result;
}

inline Fuel::Iap::Model::PurchaseResponse ModelConverter::ToModel(const Fuel::Messaging::PurchaseResponse *bufferPurchaseResponse) const
{
	Fuel::Iap::Model::PurchaseResponse result;

	auto response = bufferPurchaseResponse;
	if (response != NULL)
	{
		auto receipt = response->receipt();
		if (receipt != NULL)
		{
			result.SetReceipt(ModelConverter::ToModel(receipt));
		}
	}

	return result;
}

inline Fuel::Iap::Model::ProductType ModelConverter::ToModel(Fuel::Messaging::ProductType productType) const
{
	switch (productType)
	{
	case Fuel::Messaging::ProductType::ProductType_Consumable: return Fuel::Iap::Model::ProductType::Consumable;
	case Fuel::Messaging::ProductType::ProductType_Entitlement:  return Fuel::Iap::Model::ProductType::Entitlement;
	case Fuel::Messaging::ProductType::ProductType_Subscription:  return Fuel::Iap::Model::ProductType::Subscription;
	default:
		return Fuel::Iap::Model::ProductType::Unknown;
	}
}

inline Fuel::Iap::Model::Receipt ModelConverter::ToModel(const Fuel::Messaging::Receipt *bufferReceipt) const
{
	Fuel::Iap::Model::Receipt modelReceipt;
	
	modelReceipt.SetPurchaseDate(Fuel::DateTime(std::chrono::duration<uint64_t>(bufferReceipt->purchaseDate())));
	modelReceipt.SetCancelDate(Fuel::DateTime(std::chrono::duration<uint64_t>(bufferReceipt->cancelDate())));
	modelReceipt.SetReceiptId(ModelConverter::ToModelWithNullAsBlank(bufferReceipt->receiptId()));
	modelReceipt.SetSku(ModelConverter::ToModelWithNullAsBlank(bufferReceipt->sku()));
	modelReceipt.SetProductType(ModelConverter::ToModel(bufferReceipt->productType()));

	return modelReceipt;
}

inline Fuel::Iap::Model::TwitchUser ModelConverter::ToModel(const Fuel::Messaging::TwitchUser *bufferTwitchUser) const
{
	Fuel::Iap::Model::TwitchUser twitchUser;

	twitchUser.SetAccessToken(ModelConverter::ToModelWithNullAsBlank(bufferTwitchUser->accessToken()));
	twitchUser.SetDisplayName(ModelConverter::ToModelWithNullAsBlank(bufferTwitchUser->displayName()));
	twitchUser.SetLogo(ModelConverter::ToModelWithNullAsBlank(bufferTwitchUser->logo()));
	twitchUser.SetTuid(ModelConverter::ToModelWithNullAsBlank(bufferTwitchUser->tuid()));
	twitchUser.SetUsername(ModelConverter::ToModelWithNullAsBlank(bufferTwitchUser->username()));

	return twitchUser;
}

inline Fuel::Iap::Model::NotifyFulfillmentResponse ModelConverter::ToModel(const Fuel::Messaging::NotifyFulfillmentResponse *bufferNotifyFulfillmentResponse) const
{
	Fuel::Iap::Model::NotifyFulfillmentResponse response;

	return response;
}

inline Fuel::Iap::Model::GetPurchaseUpdatesResponse ModelConverter::ToModel(const Fuel::Messaging::GetPurchaseUpdatesResponse *buffer) const
{
	Fuel::Iap::Model::GetPurchaseUpdatesResponse response;

	Fuel::Iap::Model::SyncPointData syncPoint;
	syncPoint.SetSyncToken(ModelConverter::ToModelWithNullAsBlank(buffer->syncPoint()->syncToken()));
	syncPoint.SetTimestamp(Fuel::DateTime(std::chrono::duration<uint64_t>(buffer->syncPoint()->timestamp())));
	response.SetSyncPoint(syncPoint);

	auto updates = buffer->updates();
	if (updates != NULL)
	{
		std::vector<Fuel::Iap::Model::Receipt> receipts;
		for (unsigned int i = 0; i < updates->size(); i++)
		{
			auto update = ModelConverter::ToModel(updates->Get(i));
			receipts.push_back(update);
		}
		response.SetReceipts(receipts);
	}

	return response;
}

inline Fuel::Iap::Model::GetSessionResponse ModelConverter::ToModel(const Fuel::Messaging::GetSessionResponse *buffer) const
{
	Fuel::Iap::Model::GetSessionResponse response;

	if (buffer != NULL) {
		auto bufferTwitchUser = buffer->user();
		if (bufferTwitchUser != NULL) {
			response.SetUser(ModelConverter::ToModel(buffer->user()));
		}
		response.SetEntitlement(ModelConverter::ToModelWithNullAsBlank(buffer->entitlement()));
	}

	return response;
}