#pragma once

#include "../../fuel/core/Types.h"
#include "../../fuel/core/Error.h"

namespace Fuel
{
	inline const wchar_t* GetErrorString(ResultCodes errorCode)
	{
		switch (errorCode)
		{
			case ResultCodes::SUCCESS:
				return L"Operation completed successfully";
			case ResultCodes::MISSING_DLL:
				return L"FuelSDK Dll was not found";
			case ResultCodes::CORRUPT_DLL:
				return L"FuelSDK Dll was found, but is has been corrupted. Please reinstall the Fuel SDK";
			case ResultCodes::INCOMPATIBLE_SDK_VERSION:
				return L"The current version of the FuelSDK is not compatible with this game. Please install the latest version";
			case ResultCodes::NOT_INITIALIZED:
				return L"FuelSDK Dll is not initialized, call the Initialze function";
			case ResultCodes::ALREADY_INITIALIZED:
				return L"FuelSDK Dll is already initialized";
			case ResultCodes::INVALID_RENDERER:
				return L"The renderer you provided was invalid. Please provide a valid renderer.";
			case ResultCodes::INVALID_SIGNATURE:
				return L"FuelSDK Dll was found, but the signature is invalid.  Please reinstall the Fuel SDK";
			case ResultCodes::INVALID_ARGUMENT:
				return L"One or more arguments passed in were set to NULL";
			case ResultCodes::INTERNAL_FAILURE:
				return L"Unexpected error";
			case ResultCodes::REQUEST_TOO_LARGE:
				return L"The size of request made was too large";
			case ResultCodes::TIMED_OUT:
				return L"The request timed out";
			case ResultCodes::HOOK_FAILED:
				return L"Failed to create hooks necessary to draw overlay.";
			default:
				return L"Unknown Error Code";
		}
	}
}
