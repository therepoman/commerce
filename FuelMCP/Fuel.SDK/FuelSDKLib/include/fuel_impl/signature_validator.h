#pragma once

#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <Softpub.h>
#include <wintrust.h>
#include <chrono>
#include <ctime>
#include <string>
#include <vector>
#include <windows.h>
#include <bcrypt.h>

// Link with the Wintrust.lib file.
#pragma comment (lib, "wintrust")
#pragma comment (lib, "crypt32")

namespace Fuel
{
	namespace Internal
	{
		class SignatureValidator
		{
		public:
			/**
			* Verifies the file is signed by someone trusted, and that the signature is correct
			*/
			static bool VerifyEmbeddedSignature(std::wstring sourceFile)
			{
				// Don't pop up any windows
				const HWND window_mode = reinterpret_cast<HWND>(INVALID_HANDLE_VALUE);

				// Verify file & certificates using the Microsoft Authenticode Policy
				// Provider.
				GUID WVTPolicyGUID = WINTRUST_ACTION_GENERIC_VERIFY_V2;

				// Initialize the WINTRUST_FILE_INFO structure.
				WINTRUST_FILE_INFO FileData;
				memset(&FileData, 0, sizeof(FileData));
				FileData.cbStruct = sizeof(WINTRUST_FILE_INFO);
				FileData.pcwszFilePath = sourceFile.c_str();
				FileData.hFile = NULL;
				FileData.pgKnownSubject = NULL;

				WINTRUST_DATA WinTrustData;

				// Initialize the WinVerifyTrust input data structure.

				// Default all fields to 0.
				memset(&WinTrustData, 0, sizeof(WinTrustData));

				WinTrustData.cbStruct = sizeof(WinTrustData);
				WinTrustData.pPolicyCallbackData = NULL; // Use default code signing EKU
				WinTrustData.pSIPClientData = NULL; // No data to pass to SIP
				WinTrustData.dwUIChoice = WTD_UI_NONE;	// Disable WVT UI
				WinTrustData.dwUIContext = 0; // Not applicable if no UI
				WinTrustData.pwszURLReference = NULL; // Not used

				// No revocation checking
				WinTrustData.fdwRevocationChecks = WTD_REVOKE_NONE;
				WinTrustData.dwProvFlags = WTD_REVOCATION_CHECK_NONE;

				// Check a file, this file
				WinTrustData.dwUnionChoice = WTD_CHOICE_FILE;
				WinTrustData.pFile = &FileData;

				// Verify Action, must make subsequent call with the CLOSE action to free handle
				WinTrustData.dwStateAction = WTD_STATEACTION_VERIFY;
				WinTrustData.hWVTStateData = NULL; // Verify action sets this

				LONG result = WinVerifyTrust(window_mode, &WVTPolicyGUID, &WinTrustData);

				bool verified = result == ERROR_SUCCESS;
				if (verified)
				{
					// Verify that the signature was for Amazon or Twitch
					verified = VerifySigner(WinTrustData);
				}

				// Any hWVTStateData must be released by a call with close.
				WinTrustData.dwStateAction = WTD_STATEACTION_CLOSE;
				WinVerifyTrust(NULL, &WVTPolicyGUID, &WinTrustData);

				return verified;
			}

		private:
			static const int NUM_OF_AUTHORIZED = 2;
			static const int HASH_SIZE = 20;

			static bool VerifySigner(WINTRUST_DATA WinTrustData)
			{
				HANDLE hWVTStateData = WinTrustData.hWVTStateData;
				CRYPT_PROVIDER_DATA * provData = WTHelperProvDataFromStateData(hWVTStateData);
				if (provData == NULL)
				{
					return false;
				}

				// Look down each possible certificate chain
				for (int i = 0; i < provData->csSigners; i++)
				{
					CRYPT_PROVIDER_SGNR * pasSigners = WTHelperGetProvSignerFromChain(provData, i, false, 0);
					if (pasSigners == NULL || pasSigners->csCertChain <= 0)
					{
						return false;
					}

					// Grab the first certificate in the chain (Should contain who the certificate is issued to)
					CRYPT_PROVIDER_CERT * pasCert = WTHelperGetProvCertFromChain(pasSigners, 0);
					PCCERT_CONTEXT context = pasCert->pCert;

					int cbsize = CertNameToStr(context->dwCertEncodingType, &(context->pCertInfo->Subject), CERT_SIMPLE_NAME_STR, NULL, 0);
					wchar_t * subject = (wchar_t *)malloc(cbsize * sizeof(wchar_t));
					CertNameToStrW(context->dwCertEncodingType, &(context->pCertInfo->Subject), CERT_SIMPLE_NAME_STR, subject, cbsize);

					bool subjectVerified = VerifySubject(subject);
					free(subject);

					if (!subjectVerified)
					{
						continue;
					}

					// No need to check the first certificate as it can't be a CA.
					for (int j = 1; j < pasSigners->csCertChain; j++)
					{
						CRYPT_PROVIDER_CERT * rootCert = WTHelperGetProvCertFromChain(pasSigners, j);
						PCCERT_CONTEXT rootContext = rootCert->pCert;
						DWORD hashSize;

						CryptHashCertificate(0, CALG_SHA1, 0, rootContext->pbCertEncoded, rootContext->cbCertEncoded, NULL, &hashSize);
						BYTE * thumbprint = (BYTE *)malloc(hashSize * sizeof(BYTE));
						CryptHashCertificate(0, CALG_SHA1, 0, rootContext->pbCertEncoded, rootContext->cbCertEncoded, thumbprint, &hashSize);

						bool thumbprintVerified = VerifyThumbprint(thumbprint);
						free(thumbprint);

						if (thumbprintVerified)
						{
							return true;
						}
					}
				}
				
				return false;
			}

			static bool VerifySubject(wchar_t * subject)
			{
				// Check if the CA was issued to either Twitch or Amazon
				wchar_t * authorizedSubjects[NUM_OF_AUTHORIZED] = {
					L"US, California, San Francisco, \"Twitch Interactive, Inc.\", \"Twitch Interactive, Inc.\"",
					L"US, Washington, Seattle, Amazon Services LLC, Software Services, Amazon Services LLC" };

				for (int i = 0; i < NUM_OF_AUTHORIZED; i++)
				{
					if (wcscmp(subject, authorizedSubjects[i]) == 0)
					{
						return true;
					}
				}

				return false;
			}

			static bool VerifyThumbprint(BYTE * thumbprint)
			{
				BYTE authorizedCAs[NUM_OF_AUTHORIZED][HASH_SIZE] = {
					{ 0x05, 0x63, 0xb8, 0x63, 0x0d, 0x62, 0xd7, 0x5a, 0xbb, 0xc8, 0xab, 0x1e, 0x4b, 0xdf, 0xb5, 0xa8, 0x99, 0xb2, 0x4d, 0x43 }, // DigiCert Assured ID Root CA, issues Twitch's cert
					{ 0x4E, 0xB6, 0xD5, 0x78, 0x49, 0x9B, 0x1C, 0xCF, 0x5F, 0x58, 0x1E, 0xAD, 0x56, 0xBE, 0x3D, 0x9B, 0x67, 0x44, 0xA5, 0xE5 } }; // VeriSign Class 3 Public Primary Certification Authority - G5, issues Amazon's cert

				for (int i = 0; i < NUM_OF_AUTHORIZED; i++)
				{
					if (memcmp(thumbprint, authorizedCAs[i], HASH_SIZE) == 0)
					{
						return true;
					}
				}

				return false;
			}
		};
	}
}
