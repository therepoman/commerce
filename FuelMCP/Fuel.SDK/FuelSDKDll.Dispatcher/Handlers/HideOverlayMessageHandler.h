#include "IMessageHandler.h"
#include "OverlayManager.h"

#include "../InputHook.h"

namespace Fuel {
	class HideOverlayMessageHandler : public IMessageHandler {
	public:
		HideOverlayMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager);
		HideOverlayMessageHandler(const HideOverlayMessageHandler&) = delete; // We should never be copying these.
		HideOverlayMessageHandler& operator=(HideOverlayMessageHandler rhs) = delete; // Or assigning these.
		~HideOverlayMessageHandler();

		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName();
	private:
		std::shared_ptr<OverlayManager> m_overlayManager;
	};
}