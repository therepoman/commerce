#pragma once

#include "Message.h"
#include "../IMessageDispatcher.h"

namespace Fuel {
	class IMessageHandler {
	public:
		virtual ~IMessageHandler();
		virtual void Handle(Fuel::IMessageDispatcher& dispatcher, Message& message) = 0;
		virtual const char* GetName() = 0;
	};
}
