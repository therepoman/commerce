#include <map>

#include "IMessageHandler.h"
#include "../IMessageDispatcherCallbackFunction.h"

namespace Fuel {
	class DefaultCallbackHandler : public IMessageHandler {
	public:
		DefaultCallbackHandler() {};
		DefaultCallbackHandler(const DefaultCallbackHandler&) = delete; // We should never be copying these.
		DefaultCallbackHandler& operator=(DefaultCallbackHandler rhs) = delete; // Or assigning these.
		~DefaultCallbackHandler() { CancelAllPendingRequests(); };

		void FatalError();
		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName() { return "DefaultCallbackHandler"; }
		void AddCallback(std::string id, MessageReceivedFunction callback);
	private:
		void CancelAllPendingRequests();
		std::map<std::string, MessageReceivedFunction> m_completionMap;
	};
}
