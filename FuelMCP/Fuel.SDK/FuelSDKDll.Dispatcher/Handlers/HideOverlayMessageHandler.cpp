#include "HideOverlayMessageHandler.h"

using namespace Fuel;

HideOverlayMessageHandler::HideOverlayMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager) : m_overlayManager(overlayManager) {}

HideOverlayMessageHandler::~HideOverlayMessageHandler() {}

void HideOverlayMessageHandler::Handle(IMessageDispatcher& dispatcher, Message& message)
{
	const Messaging::MessageRoot* root = Messaging::GetMessageRoot(message.GetBody().GetBuffer());
	if (root->message_type() != Messaging::Message_HideOverlay)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->error("HideOverlayMessageHandler got invalid message type {0}",
			root->message_type());
		return;
	}

	const Messaging::HideOverlay* update = (const Messaging::HideOverlay*) root->message();
	m_overlayManager->Hide(update->instanceId());
}

const char* Fuel::HideOverlayMessageHandler::GetName() {
	return "HideOverlayHandler";
}



