#include "UpdateOverlayMessageHandler.h"
#include "logger.h"

using namespace Fuel;

#pragma pack(push)  /* push current alignment to stack */
#pragma pack(1)     /* set alignment to 1 byte boundary */

struct RawOverlayBody
{
	uint32_t InstanceId;
	uint32_t X;
	uint32_t Y;
	uint32_t Width;
	uint32_t Height;
	uint32_t DirtyRectCount;
};

struct RawOverlayDirtyRect
{
	uint32_t X;
	uint32_t Y;
	uint32_t Width;
	uint32_t Height;
};

#pragma pack(pop) 

OverlayUpdateWrapper::OverlayUpdateWrapper(Message&& message) :
	m_message(std::move(message))
{
	const MessagePart& body = m_message.GetBody();
	const uint8_t* bodyBuffer = body.GetBuffer();

	// read the width, height, and rect count
	RawOverlayBody* overlayBody = (RawOverlayBody*)bodyBuffer;
	m_instanceId = overlayBody->InstanceId;
	m_x = overlayBody->X;
	m_y = overlayBody->Y;
	m_width = overlayBody->Width;
	m_height = overlayBody->Height;

	uint32_t dirtyRectCount = overlayBody->DirtyRectCount;

	// read each rect
	uint8_t* bufferIterator = (uint8_t*)bodyBuffer + sizeof(RawOverlayBody);
	for (uint32_t i = 0; i < dirtyRectCount; i++)
	{
		RawOverlayDirtyRect* dirtyRect = (RawOverlayDirtyRect*)bufferIterator;

		OverlayDirtyRect result;
		result.X = dirtyRect->X;
		result.Y = dirtyRect->Y;
		result.Width = dirtyRect->Width;
		result.Height = dirtyRect->Height;
		result.TextureSize = dirtyRect->Width * dirtyRect->Height * 4;
		result.Texture = bufferIterator + sizeof(RawOverlayDirtyRect);

		m_dirtyRects.push_back(result);

		bufferIterator += sizeof(RawOverlayDirtyRect) + result.TextureSize;
	}

	m_isValid = true;
}

UpdateOverlayMessageHandler::UpdateOverlayMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager) : m_overlayManager(overlayManager) {}

UpdateOverlayMessageHandler::~UpdateOverlayMessageHandler() {}

void UpdateOverlayMessageHandler::Handle(IMessageDispatcher& dispatcher, Message& message)
{
	auto overlayUpdate = std::shared_ptr<IOverlayUpdate>(new OverlayUpdateWrapper(std::move(message)));
	m_overlayManager->UpdateTexture(overlayUpdate);
}

const char* UpdateOverlayMessageHandler::GetName() {
	return "UpdateOverlayHandler";
}
