#include "IMessageHandler.h"
#include "IMessageTransport.h"

namespace Fuel {
	class TransportHandler : public IMessageHandler {
	public:
		TransportHandler(std::function<void(const Fuel::Message&)> sendMessge);
		TransportHandler(const TransportHandler&) = delete; // We should never be copying these.
		TransportHandler& operator=(TransportHandler rhs) = delete; // Or assigning these.
		~TransportHandler() {};

		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName() { return "TransportMessageHandler"; }
	private:
		std::function<void(const Fuel::Message&)> m_sendMessage;
	};
}