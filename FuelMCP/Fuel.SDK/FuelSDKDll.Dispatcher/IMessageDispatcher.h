#pragma once

#include "IMessageDispatcherCallbackFunction.h"
#include <fuel/IapClient.h>

namespace Fuel
{
	struct IMessageDispatcher
	{
		virtual void HandleMessage(Message& message) = 0;
		virtual void AddCallback(std::string id, MessageReceivedFunction callback) = 0;
	};
}
