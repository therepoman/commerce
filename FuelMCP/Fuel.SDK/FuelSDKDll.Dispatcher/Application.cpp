#include "Application.h"

#include "MessageTransport.h"

#include "Handlers/ShowOverlayMessageHandler.h"
#include "Handlers/HideOverlayMessageHandler.h"
#include "Handlers/UpdateOverlayMessageHandler.h"
#include "Handlers/UpdateCursorMessageHandler.h"
#include "Handlers/FuelPumpInitializedHandler.h"

#include "Handlers/TransportHandler.h"

#include "DefaultRestartStrategy.h"

using namespace Fuel;

Application::Application(Fuel::RendererType renderType, const char* vendorId, const char* gameVersion, const char* sdkVersion) :
	MessagePump(std::unique_ptr<IRestartStrategy>(new DefaultRestartStrategy()), vendorId, gameVersion, sdkVersion)
{
	overlayFactory = Fuel::IOverlayFactory::GetInstance((Fuel::RendererType)renderType);
	overlayManager = std::make_shared<Fuel::OverlayManager>(overlayFactory);
	overlayFactory->SetOverlayManager(overlayManager);

	// This instance of InputHook will be kept alive by this shared_ptr.
	inputHook = Fuel::Input::InputHook::Instance();
	inputHook->SetOverlayManager(overlayManager);

	// To handle callbacks for request-responses
	defaultCallbackHandler.reset(new Fuel::DefaultCallbackHandler());

	handlers[Fuel::Messaging::Message_ShowOverlay] = std::make_unique<Fuel::ShowOverlayMessageHandler>(overlayManager); 
	handlers[Fuel::Messaging::Message_HideOverlay] = std::make_unique<Fuel::HideOverlayMessageHandler>(overlayManager); 
	handlers[Fuel::Messaging::Message_UpdateOverlay] = std::make_unique<Fuel::UpdateOverlayMessageHandler>(overlayManager); 
	handlers[Fuel::Messaging::Message_UpdateCursor] = std::make_unique<Fuel::UpdateCursorMessageHandler>(overlayManager); 

	handlers[Fuel::Messaging::Message_SetWindowSize] = std::make_unique<Fuel::TransportHandler>(std::bind(&MessagePump::SendRequest, this, std::placeholders::_1)); 
	handlers[Fuel::Messaging::Message_KeyboardEvent] = std::make_unique<Fuel::TransportHandler>(std::bind(&MessagePump::SendRequest, this, std::placeholders::_1)); 
	handlers[Fuel::Messaging::Message_MouseEvent] = std::make_unique<Fuel::TransportHandler>(std::bind(&MessagePump::SendRequest, this, std::placeholders::_1)); 
	handlers[Fuel::Messaging::Message_MouseMoveEvent] = std::make_unique<Fuel::TransportHandler>(std::bind(&MessagePump::SendRequest, this, std::placeholders::_1)); 
	handlers[Fuel::Messaging::Message_MouseWheelEvent] = std::make_unique<Fuel::TransportHandler>(std::bind(&MessagePump::SendRequest, this, std::placeholders::_1)); 
	handlers[Fuel::Messaging::Message_CloseOverlayEvent] = std::make_unique<Fuel::TransportHandler>(std::bind(&MessagePump::SendRequest, this, std::placeholders::_1));

	overlayMessageHandler.reset(new Fuel::OverlayMessageHandler());
	overlayManager->SetResizeCallback([this](Fuel::BufferSize &bufferSize){ overlayMessageHandler->sendWindowResize(bufferSize); });
	overlayManager->SetOverlayFailureCallback([this](IOverlayInstance &instance) { overlayMessageHandler->sendOverlayFailureEvent(instance); });
	overlayManager->SetEnableInputHooksCallback([](HWND hWnd){ Fuel::Input::InputHook::Instance()->InitHook(hWnd); });
	overlayManager->SetDisableInputHooksCallback([](){ Fuel::Input::InputHook::Instance()->EndHook(); });

	dispatcher.reset(new Fuel::CMessageDispatcher(defaultCallbackHandler, handlers));

	inputHook->SetDispatcher(dispatcher);

	Start();
}

Application::~Application()
{
	Stop();
}

std::unique_ptr<Fuel::IMessageTransport> Application::CreateTransport()
{
	return std::unique_ptr<Fuel::IMessageTransport>(
		new Fuel::MessageTransport());
}

Fuel::IMessageDispatcher& Application::Dispatcher()
{
	return *dispatcher;
}

void Application::TransportReset(const Fuel::MessageTransportFatalError& error)
{
	overlayManager->HideAll();
	defaultCallbackHandler->FatalError();
}
