#include <chrono>
#include <fstream>

#include "CMessageDispatcher.h"
#include "MessageTransport.h"
#include "FuelHeader_generated.h"

#include "InputHook.h"

#include "logger.h"

using namespace Fuel;

void CMessageDispatcher::HandleMessage(Message& message) {
	const MessagePart& headerPart = message.GetHeader();

	if (!headerPart.IsEmpty()) 
	{
		const Fuel::Messaging::MessageHeader* messageHeader = Fuel::Messaging::GetMessageHeader(headerPart.GetBuffer());
		std::string requestId = messageHeader->requestId()->str();

		Fuel::Messaging::Message type = (Fuel::Messaging::Message) messageHeader->type();
		Logger::Instance(FUEL_MESSAGING_LOGGER)->info("Request {0} received. Searching for handler for type {1}", requestId, Fuel::Messaging::EnumNameMessage(type));
		auto requestPair = m_handlers.find(type);
		if (requestPair != m_handlers.end())
		{
			Logger::Instance(FUEL_MESSAGING_LOGGER)->info("Handler {0} found to accept {1}", requestPair->second->GetName(), requestId);
			requestPair->second->Handle(*this, message);
		}
		else
		{
			Logger::Instance(FUEL_MESSAGING_LOGGER)->info("Could not find handler for type {0}. Trying default handler.", Fuel::Messaging::EnumNameMessage(type));
			m_defaultHandler->Handle(*this, message);
		}
	}
}

void CMessageDispatcher::AddCallback(std::string id, MessageReceivedFunction callback)
{
	m_defaultHandler->AddCallback(id, callback);
}

CMessageDispatcher::CMessageDispatcher(const std::shared_ptr<DefaultCallbackHandler>& defaultHandler, const std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>>& handlers) :
m_defaultHandler(defaultHandler),
m_handlers(handlers)
{
}

CMessageDispatcher::~CMessageDispatcher() {
}
