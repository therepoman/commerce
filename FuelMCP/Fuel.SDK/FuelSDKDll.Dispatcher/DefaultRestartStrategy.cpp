#include "DefaultRestartStrategy.h"

using namespace Fuel;

DefaultRestartStrategy::DefaultRestartStrategy() :
	m_restartCount(0)
{
}

DefaultRestartStrategy::~DefaultRestartStrategy()
{
}

int DefaultRestartStrategy::InitializeTimeoutMilliseconds()
{
	return 60000;
}

void DefaultRestartStrategy::TransportReset()
{
	m_restartCount++;
}

bool DefaultRestartStrategy::ShouldRestart() const
{
	return m_restartCount < 3;
}
