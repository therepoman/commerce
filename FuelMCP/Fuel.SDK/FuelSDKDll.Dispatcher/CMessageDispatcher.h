#pragma once

#include <map>
#include <future>
#include <mutex>

#include "IMessageTransport.h"
#include "IMessageDispatcher.h"
#include "Handlers/IMessageHandler.h"
#include "Handlers/DefaultCallbackHandler.h"

#include "Fuel_generated.h"

namespace Fuel
{
	class MessageTransport;

	class CMessageDispatcher : public IMessageDispatcher
	{
	public:
		CMessageDispatcher(const std::shared_ptr<DefaultCallbackHandler>& defaultHandler, const std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>>& handlers);
		~CMessageDispatcher();

		void HandleMessage(Message& message);
		void AddCallback(std::string id, MessageReceivedFunction callback);
	private:
		const std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>>& m_handlers;
		const std::shared_ptr<DefaultCallbackHandler>& m_defaultHandler;
	};
}
