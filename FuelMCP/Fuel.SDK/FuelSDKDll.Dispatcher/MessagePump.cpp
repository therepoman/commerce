#include "MessagePump.h"

#include "MessageTransport.h"

#include "Fuel_generated.h"

#include "logger.h"

#include <cassert>
#include <functional>
#include <future>

using namespace Fuel;

MessagePump::MessagePump(std::unique_ptr<IRestartStrategy>&& restartStrategy, const char* vendorId, const char* gameVersion, const char* sdkVersion) :
	m_restartStrategy(std::move(restartStrategy)),
	m_isPolling(false),
	m_initMessage(CreateInitializeMessage(vendorId, gameVersion, sdkVersion))
{
}

MessagePump::~MessagePump()
{
	assert(m_isPolling == false);
}

void MessagePump::Start()
{
	std::lock_guard<std::mutex> lock(m_transportMutex);

	StartTransportIfNeeded();
}

void MessagePump::Stop()
{
	std::lock_guard<std::mutex> lock(m_transportMutex);
	
	m_isPolling = false;

	// send the shutdown message if the transport is still running
	if (m_transport)
	{
		// send directly through transport to allow deadlock due to multiple lock guards
		m_transport->WriteMessage(CreateShutdownMessage());

		// abort any pending reads
		m_transport->AbortRead();
	}

	// wait for the polling thread to shut down
	if (m_pollingThread && m_pollingThread->joinable())
	{
		m_pollingThread->join();
	}
	
	// clear the transport so it's ready for a recreation if needed
	m_transport.reset();
}

void MessagePump::SendRequest(const Fuel::Message& message)
{
	std::lock_guard<std::mutex> lock(m_transportMutex);

	StartTransportIfNeeded();

	m_transport->WriteMessage(message);
}

void MessagePump::SendRequestResponse(const Fuel::Message& message, std::function<void (int, uint32_t, const uint8_t*)> callback)
{
	std::lock_guard<std::mutex> lock(m_transportMutex);

	StartTransportIfNeeded();

	Dispatcher().AddCallback(message.GetRequestId(), [callback](Fuel::Message& message)
	{
		const Fuel::MessagePart& bodyPart = message.GetBody();
		callback(message.GetType(), bodyPart.GetSize(), bodyPart.GetBuffer());
	});

	m_transport->WriteMessage(std::move(message));
}

void MessagePump::StartTransportIfNeeded()
{
	while (!m_transport && m_restartStrategy->ShouldRestart())
	{
		StartInternal();
	}
	if (!m_transport)
	{
		throw Fuel::MessageTransportFatalError();
	}
}

void MessagePump::StartInternal()
{
	if (m_pollingThread && m_pollingThread->joinable()) {
		m_pollingThread->join();
	}

	try {
		m_transport = CreateTransport();

		m_transport->WriteMessage(m_initMessage);

		{
			std::promise<bool> initialized;
			std::thread readThread([this, &initialized]() {
				try 
				{
					auto message = m_transport->ReadMessage();
					if (message.IsEmpty() || message.GetType() != Fuel::Messaging::Message::Message_InitializeResponse)
					{
						initialized.set_value(false);
					}
					else
					{
						initialized.set_value(true);
					}
				}
				catch (const std::exception& e)
				{
					Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Exception initializing FuelPump: {0}", e.what());
					initialized.set_value(false);
				}
			});

			auto future = initialized.get_future();
			std::future_status status = future.wait_for(std::chrono::milliseconds(m_restartStrategy->InitializeTimeoutMilliseconds()));

			if (status != std::future_status::ready)
			{
				m_transport->AbortRead();
			}

			bool result = future.get();
			readThread.join();
			if (status == std::future_status::timeout)
			{
				throw Fuel::MessageTransportFatalError("Timed out initializing message transport");
			}

			if (!result) {
				throw Fuel::MessageTransportFatalError("Initialization failed. Returned false");
			}
			
		}

		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Message transport initialized. Begin polling loop...");
		m_pollingThread.reset(new std::thread([this](){ this->PollingLoop(); }));
	}
	catch (Fuel::MessageTransportFatalError err)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Fatal error in MessageTransport: {0}", err.what());

		// destroy the transport object
		m_transport.reset();

		// signal the derived class there was a reset in the transport
		m_restartStrategy->TransportReset();
		TransportReset(err);
	}
}


void MessagePump::TransportFailure(const Fuel::MessageTransportFatalError& error)
{
	std::lock_guard<std::mutex> lock(m_transportMutex);

	// destroy the transport object
	m_transport.reset();

	// signal the derived class there was a reset in the transport
	m_restartStrategy->TransportReset();
	TransportReset(error);
}

void MessagePump::PollingLoop()
{
	try
	{
		m_isPolling = true;
		while (m_isPolling) 
		{
			auto message = m_transport->ReadMessage();
			Fuel::Logger::Instance(FUEL_MESSAGING_LOGGER)->info("Read message of type {0} for request {1}", Fuel::Messaging::EnumNameMessage((Fuel::Messaging::Message)message.GetType()), message.GetRequestId());

			if (m_isPolling && !message.IsEmpty())
			{
				Dispatcher().HandleMessage(message);
			}
		}
	}
	catch (Fuel::MessageTransportFatalError err)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Fatal error in MessageTransport: {0}", err.what());

		// Only raise this as a transport error if we're not actively shutting down
		if (m_isPolling)
		{
			this->TransportFailure(err);
		} 

	}
	catch (...)
	{
		// TODO: FUEL-856 handle this and make sure the polling code keeps running.  Logging this exception looks like it might take 
		// some changes to how we define our exceptions.  This will be done as part of the FUEL-856 task
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Transport worker critical error");
	}

	m_isPolling = false;
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Transport worker thread exiting");
}

Fuel::Message MessagePump::CreateInitializeMessage(const char* vendorId, const char* gameVersion, const char* sdkVersion)
{
	flatbuffers::FlatBufferBuilder fbb;

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Creating Initialize Message");

	auto messageRoot = Fuel::Messaging::CreateMessageRoot(fbb,
		Fuel::Messaging::FuelResponseType::FuelResponseType_Success,
		Fuel::Messaging::Message::Message_InitializeRequest,
		Fuel::Messaging::CreateInitializeRequest(fbb,
			fbb.CreateString(gameVersion),
			fbb.CreateString(sdkVersion),
			fbb.CreateString(vendorId)).Union());
	fbb.Finish(messageRoot);

	// TODO: [FUEL-940] This is being done because the FBB manages the buffer memory itself
	// Alternatively in the future to avoid the copy we could create a derived
	// class to hold the FBB or the special flatbuffers::unique_ptr_t
	uint8_t* buffer = new uint8_t[fbb.GetSize()];
	memcpy(buffer, fbb.GetBufferPointer(), fbb.GetSize());
	return Fuel::Message(Fuel::Messaging::Message::Message_InitializeRequest,
		(uint32_t)fbb.GetSize(),
		(uint8_t*)buffer);
}

Fuel::Message MessagePump::CreateShutdownMessage()
{
	flatbuffers::FlatBufferBuilder fbb;

	auto messageRoot = Fuel::Messaging::CreateMessageRoot(fbb,
		Fuel::Messaging::FuelResponseType::FuelResponseType_Success,
		Fuel::Messaging::Message::Message_ShutdownRequest,
		Fuel::Messaging::CreateShutdownRequest(fbb).Union());
	fbb.Finish(messageRoot);

	// TODO: [FUEL-940] This is being done because the FBB manages the buffer memory itself
	// Alternatively in the future to avoid the copy we could create a derived
	// class to hold the FBB or the special flatbuffers::unique_ptr_t
	uint8_t* buffer = new uint8_t[fbb.GetSize()];
	memcpy(buffer, fbb.GetBufferPointer(), fbb.GetSize());
	return Fuel::Message(Fuel::Messaging::Message::Message_ShutdownRequest,
		(uint32_t)fbb.GetSize(),
		(uint8_t*)buffer);
}
