#pragma once

#include "MessagePump.h"

namespace Fuel 
{
	class DefaultRestartStrategy : public IRestartStrategy
	{
		int m_restartCount;
	public:
		DefaultRestartStrategy();
		virtual ~DefaultRestartStrategy();
		
		int InitializeTimeoutMilliseconds() override;
		void TransportReset() override;
		bool ShouldRestart() const override;
	};
}

