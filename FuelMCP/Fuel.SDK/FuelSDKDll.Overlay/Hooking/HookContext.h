namespace Fuel {
	namespace Hooking {
		class HookContext {
		public:
			static HookContext* Instance();
			void Initialize();
		private:
			HookContext() :s_Init(0) {};
			~HookContext();
			long s_Init;
			static HookContext* m_pInstance;
		};
	}
}