#include "DX12OverlayFactory.h"
#include "DX12D2DOverlayInstance.h"
#include "DX12HookSetup.h"
#include "logger.h"

using namespace Fuel::DX12;

typedef HRESULT(__stdcall *PresentType)(IDXGISwapChain *, UINT, UINT);
typedef HRESULT(__stdcall *ResizeType)(IDXGISwapChain *, UINT, UINT, UINT, DXGI_FORMAT, UINT);

static HRESULT __stdcall dx12OverlayPresent(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags)
{
	return DX12OverlayFactory::Instance()->Present(pSwapChain, SyncInterval, Flags);
}

static HRESULT __stdcall dx12OverlayResize(IDXGISwapChain *pSwapChain, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags)
{
	return DX12OverlayFactory::Instance()->Resize(pSwapChain, BufferCount, Width, Height, NewFormat, SwapChainFlags);
}

// MinHook requires a function pointer, which has to be static.
// That means a static singleton is needed, and so this method.
// Using a static weak pointer allows the SDK to mostly own the instance lifetime by holding onto the primary shared_ptr,
// and allow the static methods to temporarily hold the instance alive while they're busy.
std::shared_ptr<DX12OverlayFactory> DX12OverlayFactory::Instance() {
	static std::weak_ptr<DX12OverlayFactory> s_instance;
	static std::mutex s_initializationMutex;

	std::shared_ptr<DX12OverlayFactory> result = s_instance.lock();
	if (!result)
	{
		std::lock_guard<std::mutex> lock(s_initializationMutex);
		std::shared_ptr<DX12OverlayFactory> instanceAfterLock = s_instance.lock();
		if (instanceAfterLock)
		{
			result = std::move(instanceAfterLock);
		}
		else
		{
			std::shared_ptr<DX12OverlayFactory> newInstance(new DX12OverlayFactory());
			result = std::move(newInstance);
			s_instance = result;
		}
	}
	return std::move(result);
}

DX12OverlayFactory::DX12OverlayFactory() : m_presentHook(), m_resizeHook()
{
	// We get the DX data upon construction, as this will not change during execution (unless you can change the DLL mid-run).
	// Further, querying this data in the middle of a render cycle is not a terrific idea given how many DX objects need to be constructed to do so.
	GetLibData(&libData);
}

DX12OverlayFactory::~DX12OverlayFactory()
{
	Release();
}

void DX12OverlayFactory::Init()
{
	if (m_hooksEnabled)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("DX12 Render hooks are already set up.");
	}
	else
	{
		// Save current cursor.
		ULONG_PTR prev = SetClassLongPtr(GetForegroundWindow(), GCLP_HCURSOR, (LONG_PTR)LoadCursor(NULL, IDC_ARROW));
		m_prevCursor = (HCURSOR)prev;

		// Install the hooks
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Setting up DirectX12 hooks.");
		m_presentHook.setup(libData.presentFunction, reinterpret_cast<voidFunc>(dx12OverlayPresent));
		m_resizeHook.setup(libData.resizeFunction, reinterpret_cast<voidFunc>(dx12OverlayResize));
		m_hooksEnabled = true;
	}
}

void DX12OverlayFactory::Release()
{
	if (m_hooksEnabled)
	{
		m_hooksEnabled = false;
		
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Releasing DirectX12 hooks.");
		m_presentHook.restore();
		m_resizeHook.restore();
		m_hWnd = 0;

		m_swapChain.Detach();

		// Reload saved cursor.
		SetClassLongPtr(GetForegroundWindow(), GCLP_HCURSOR, (LONG_PTR)m_prevCursor);
	}
	else
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Can't hide overlay, it's not currently visible.");
	}
}

std::unique_ptr<Fuel::IOverlayInstance> DX12OverlayFactory::CreateOverlay(int instanceId, const RECT &location, int zIndex)
{
	std::unique_ptr<IOverlayInstance> instance(new DX12D2DOverlayInstance(instanceId, location, zIndex, libData));
	return std::move(instance);
}

bool DX12OverlayFactory::getBufferSize(Fuel::BufferSize &bufferSize)
{
	if (!m_swapChain)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Fuel::DX12::GetBufferSize: No swap chain.");
		return false;
	}

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	HRESULT getDescResult = m_swapChain->GetDesc(&swapChainDesc);
	if (FAILED(getDescResult))
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Fuel::DX12::GetBufferSize: GetDesc failed with code {0}", getDescResult);
		return false;
	}

	bufferSize.width = swapChainDesc.BufferDesc.Width;
	bufferSize.height = swapChainDesc.BufferDesc.Height;
	return true;
}

HRESULT DX12OverlayFactory::Present(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags)
{
	static std::mutex s_hWndInitializationMutex;
	std::shared_ptr<OverlayManager> overlayManager = m_overlayManager.lock();

	if (overlayManager)
	{
		if (!m_hWnd)
		{
			// init only once
			std::lock_guard<std::mutex> lock(s_hWndInitializationMutex);
			if (!m_hWnd)
			{
				m_swapChain = pSwapChain;

				DXGI_SWAP_CHAIN_DESC swapChainDesc;
				if (SUCCEEDED(pSwapChain->GetDesc(&swapChainDesc)))
				{
					overlayManager->SetHWnd(swapChainDesc.OutputWindow);
					m_hWnd = swapChainDesc.OutputWindow;
					BufferSize bufferSize;
					bufferSize.width = swapChainDesc.BufferDesc.Width;
					bufferSize.height = swapChainDesc.BufferDesc.Height;
					overlayManager->SetBufferSize(bufferSize);
				}
				else
				{
					Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("DX12: Unable to get swap chain description");
				}
			}
		}

		overlayManager->Render([this, pSwapChain](IOverlayInstance &instance) -> HRESULT {
			DX12D2DOverlayInstance *dx12Instance = dynamic_cast<DX12D2DOverlayInstance*>(&instance);
			if (dx12Instance)
			{
				return dx12Instance->Render(pSwapChain);
			}
			return S_OK;
		});
	}

	// call real present
	PresentType oPresent = (PresentType)m_presentHook.m_originalFunc;
	HRESULT hr = oPresent(pSwapChain, SyncInterval, Flags);
	return hr;
}

HRESULT DX12OverlayFactory::Resize(IDXGISwapChain *pSwapChain, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags)
{
	std::shared_ptr<OverlayManager> overlayManager = m_overlayManager.lock();

	Release();
	if (overlayManager)
	{
		overlayManager->Release();
	}

	ResizeType oResize = (ResizeType)m_resizeHook.m_originalFunc;
	HRESULT result = oResize(pSwapChain, BufferCount, Width, Height, NewFormat, SwapChainFlags);

	Init();

	if (overlayManager)
	{
		overlayManager->Reinitialize();
		BufferSize bufferSize;
		bufferSize.width = Width;
		bufferSize.height = Height;
		overlayManager->Resize(bufferSize);
	}

	return result;
}
