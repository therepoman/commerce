#include <memory>
#include <d3d11_1.h>
#include <directxmath.h>
#include <functional>
#include <wrl.h>

#include "DX12HookSetup.h"
#include "../IOverlayFactory.h"

using Microsoft::WRL::ComPtr;

namespace Fuel {
	namespace DX12 {
		class DX12OverlayFactory : public Fuel::IOverlayFactory {
		public:
			static std::shared_ptr<DX12OverlayFactory> Instance();
			virtual ~DX12OverlayFactory();

			virtual void Init();
			virtual void Release();
			
			virtual std::unique_ptr<IOverlayInstance> CreateOverlay(int instanceId, const RECT &location, int zIndex);

			virtual bool getBufferSize(BufferSize &bufferSize);

			HRESULT Present(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags);
			HRESULT Resize(IDXGISwapChain *pSwapChain, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);
		private:
			DX12OverlayFactory();

			//DX12 LibData
			DX12LibData libData;

			// Hooking variables.
			Fuel::Hooking::Hook m_presentHook;
			Fuel::Hooking::Hook m_resizeHook;
			bool m_hooksEnabled = false;

			// State variables.
			HCURSOR m_prevCursor;
			
			// Handle to our rendering window.
			HWND m_hWnd = 0;

			// Reference to the swap chain we are maintaining.
			ComPtr<IDXGISwapChain> m_swapChain;
		};
	}
}
