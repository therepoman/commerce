#include "DX12D2DOverlayInstance.h"
#include "MutexTryLock.h"

using namespace Fuel::DX12;

DX12D2DOverlayInstance::DX12D2DOverlayInstance(int instanceId, const RECT &location, int zIndex, DX12LibData libData) :
	IOverlayInstance(instanceId, location, zIndex),
#ifdef _DEBUG
	m_frameSkipLoggingLimiter(20, FUEL_DLL_LOGGER,
		"Too many frames skipped, disabling logging of this message"),
#endif
	m_textureErrorLoggingLimiter(20, FUEL_DLL_LOGGER,
		"Too many texture errors, disabling logging of this message")
{
	m_libData = libData;
}

DX12D2DOverlayInstance::~DX12D2DOverlayInstance()
{
	Release();
}

void DX12D2DOverlayInstance::Release()
{
	// Disable rendering during release.
	std::lock_guard<std::mutex> lock(m_render_mutex);

	WaitForGpu();
	m_renderEnabled = false;

	m_pD3D11DeviceContext->ClearState();
	m_pD3D11DeviceContext->Flush();
	m_pD3D11DeviceContext.Reset();

	m_fence.Reset();
	m_pD3D11On12Device.Reset();
	m_pD3D12Device.Reset();
	m_pD2DFactory.Reset();
	m_pD2DDevice.Reset();
	m_pD2DDeviceContext.Reset();
	m_pDWriteFactory.Reset();
	m_pRtvHeap.Reset();
	m_pOverlayBitmap.Reset();

	for (int i = 0; i < m_frameCount; ++i) {
		m_vecRenderTargets[i].Reset();
		m_vecWrappedBackBuffers[i].Reset();
		m_vecD2DRenderTargets[i].Reset();
		m_vecCommandAllocators[i].Reset();
	}
	m_frameCount = 0;
	m_frameIndex = -1;
}

HRESULT DX12D2DOverlayInstance::Reinitialize()
{
	std::lock_guard<std::mutex> lock(m_render_mutex);
	return ReinitializeUnsynchronized();
}

HRESULT DX12D2DOverlayInstance::ReinitializeUnsynchronized()
{
	m_pSwapChain->GetDevice(__uuidof(ID3D12Device), (void**)m_pD3D12Device.GetAddressOf());

	// Now we try and fetch the command queue from within the swpachain. This is done by offsetting the chain's memory address by 0xd0, which is the location where the
	// associated command queue's address is located.
	uint64_t swapChainAddress = (uint64_t)m_pSwapChain;
	uint64_t offset = m_libData.commandQueueOffset;
	uint64_t fetchQueueAddress = swapChainAddress + offset;

	// Address is written in memory in little endian ordering
	uint32_t upperBits = *reinterpret_cast<uint32_t *>(fetchQueueAddress + 0x4);
	uint32_t lowerBits = *reinterpret_cast<uint32_t *>(fetchQueueAddress);
	uint64_t calculatedQueueAddress = 0;

	// Put in the upper bits of the queue address.
	calculatedQueueAddress += upperBits;
	calculatedQueueAddress = calculatedQueueAddress << 32;
	calculatedQueueAddress += lowerBits;
	void* queueBlob = (void*)calculatedQueueAddress;

	m_pCommandQueue = (ID3D12CommandQueue*)queueBlob;

	UINT d3d11DeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
	D2D1_FACTORY_OPTIONS d2dFactoryOptions = {};
#if defined(_DEBUG)
	// Enable the D2D debug layer.
	d2dFactoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;

	// Enable the D3D11 debug layer.
	d3d11DeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;

	// Enable the D3D12 debug layer.
	{
		ComPtr<ID3D12Debug> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
		}
	}
#endif

	HRESULT hr;

	// Create an 11 device wrapped around the 12 device and share
	// 12's command queue.
	ComPtr<ID3D11Device> d3d11Device;
	hr = D3D11On12CreateDevice(
		m_pD3D12Device.Get(),
		d3d11DeviceFlags,
		nullptr,
		0,
		reinterpret_cast<IUnknown**>(&m_pCommandQueue),
		1,
		0,
		&d3d11Device,
		m_pD3D11DeviceContext.GetAddressOf(),
		nullptr
	);

	// Query the 11On12 device from the 11 device.
	d3d11Device.As(&m_pD3D11On12Device);

	// Create D2D/DWrite components.
	{
		D2D1_DEVICE_CONTEXT_OPTIONS deviceOptions = D2D1_DEVICE_CONTEXT_OPTIONS_NONE;
		ThrowIfFailed(
			D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,__uuidof(ID2D1Factory3), &d2dFactoryOptions, &m_pD2DFactory));
		ComPtr<IDXGIDevice> dxgiDevice;
		ThrowIfFailed(m_pD3D11On12Device.As(&dxgiDevice));
		ThrowIfFailed(m_pD2DFactory->CreateDevice(dxgiDevice.Get(), &m_pD2DDevice));
		ThrowIfFailed(m_pD2DDevice->CreateDeviceContext(deviceOptions, &m_pD2DDeviceContext));
		ThrowIfFailed(DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), &m_pDWriteFactory));
	}

	// Query the desktop's dpi settings, which will be used to create
	// D2D's render targets.
	float dpiX;
	float dpiY;
	m_pD2DFactory->GetDesktopDpi(&dpiX, &dpiY);
	D2D1_BITMAP_PROPERTIES1 bitmapProperties = D2D1::BitmapProperties1(
		D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
		dpiX,
		dpiY
	);

	// We need the frame count and the current back buffer frame index
	IDXGISwapChain3* pSwapChain = (IDXGISwapChain3 *)m_pSwapChain;
	DXGI_SWAP_CHAIN_DESC1 swapChainDescription;
	pSwapChain->GetDesc1(&swapChainDescription);
	m_frameIndex = pSwapChain->GetCurrentBackBufferIndex();
	m_frameCount = swapChainDescription.BufferCount;

	// Now that we know the number of frames and the size of the swap chain,
	// we can create the objects that needed those values.
	// First, the fence values for each frame. These should all start at 0.
	m_fenceValues = new UINT64[m_frameCount];
	for (int i = 0; i < m_frameCount; i++) {
		m_fenceValues[i] = 0;
	}
	ThrowIfFailed(m_pD3D12Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(m_fence.GetAddressOf())));

	// Resize vectors
	m_vecRenderTargets.resize(m_frameCount);
	m_vecWrappedBackBuffers.resize(m_frameCount);
	m_vecD2DRenderTargets.resize(m_frameCount);
	m_vecCommandAllocators.resize(m_frameCount);

	// Create descriptor heaps.
	{
		// Describe and create a render target view (RTV) descriptor heap.
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		rtvHeapDesc.NumDescriptors = m_frameCount;
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		ThrowIfFailed(m_pD3D12Device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_pRtvHeap)));

		m_rtvDescriptorSize = m_pD3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	}

	// Create frame resources.
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_pRtvHeap->GetCPUDescriptorHandleForHeapStart());

		// Create a RTV, D2D render target, and a command allocator for each frame.
		for (UINT n = 0; n < m_frameCount; n++)
		{
			ThrowIfFailed(m_pSwapChain->GetBuffer(n, IID_PPV_ARGS(&m_vecRenderTargets[n])));
			m_pD3D12Device->CreateRenderTargetView(m_vecRenderTargets[n].Get(), nullptr, rtvHandle);

			// Create a wrapped 11On12 resource of this back buffer. Since we are 
			// rendering all D3D12 content first and then all D2D content, we specify 
			// the In resource state as RENDER_TARGET - because D3D12 will have last 
			// used it in this state - and the Out resource state as PRESENT. When 
			// ReleaseWrappedResources() is called on the 11On12 device, the resource 
			// will be transitioned to the PRESENT state.
			D3D11_RESOURCE_FLAGS d3d11Flags = { D3D11_BIND_RENDER_TARGET };
			ThrowIfFailed(m_pD3D11On12Device->CreateWrappedResource(
				m_vecRenderTargets[n].Get(),
				&d3d11Flags,
				D3D12_RESOURCE_STATE_RENDER_TARGET,
				D3D12_RESOURCE_STATE_PRESENT,
				IID_PPV_ARGS(&m_vecWrappedBackBuffers[n])
			));

			// Create a render target for D2D to draw directly to this back buffer.
			ComPtr<IDXGISurface> surface;
			ThrowIfFailed(m_vecWrappedBackBuffers[n].As(&surface));
			ThrowIfFailed(m_pD2DDeviceContext->CreateBitmapFromDxgiSurface(
				surface.Get(),
				&bitmapProperties,
				&m_vecD2DRenderTargets[n]
			));

			rtvHandle.Offset(1, m_rtvDescriptorSize);

			ThrowIfFailed(
				m_pD3D12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_vecCommandAllocators[n])));
		}
	}

	m_renderEnabled = true;
	return S_OK;
}

HRESULT DX12D2DOverlayInstance::Render(IDXGISwapChain *pSwapChain)
{
	Fuel::MutexTryLock renderLock(m_render_mutex);

	if (!renderLock.isLocked())
	{
		// Another thread is initializing or tearing down graphics resources, can't render.
#ifdef _DEBUG
		if (m_frameSkipLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Render mutex is locked, skipping frame.");
		}
#endif
		return S_OK; // It will eventually recover.
	}

	if (!m_pBackBuffer)
	{
		// Texture hasn't been set to anything yet, no reason to render anything.
#ifdef _DEBUG
		if (m_frameSkipLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Texture has not yet been updated, skipping frame.");
		}
#endif
		return S_OK; // It will eventually recover.
	}

	if (!m_renderEnabled) {
		m_pSwapChain = pSwapChain;

		HRESULT hr = ReinitializeUnsynchronized();
		if (FAILED(hr)) {
			return hr;
		}
	}

	// Acquire our wrapped render target resource for the current back buffer.
	m_pD3D11On12Device->AcquireWrappedResources(m_vecWrappedBackBuffers[m_frameIndex].GetAddressOf(), 1);

	// Render text directly to the back buffer.
	m_pD2DDeviceContext->SetTarget(m_vecD2DRenderTargets[m_frameIndex].Get());
	m_pD2DDeviceContext->BeginDraw();
	m_pD2DDeviceContext->SetTransform(D2D1::Matrix3x2F::Identity());

	bool createBitmap;
	if (m_pOverlayBitmap) {
		D2D1_SIZE_F bpSz = m_pOverlayBitmap->GetSize();
		createBitmap = bpSz.width != m_backBufferWidth || bpSz.height != m_backBufferHeight ||
			m_location.left != m_backBufferX || m_location.top != m_backBufferY;
		if (createBitmap)
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Overlay bitmap resize ({0}, {1}, {2}, {3}) -> ({4}, {5}, {6}, {7})",
				m_location.left, m_location.top, bpSz.width, bpSz.height,
				m_backBufferX, m_backBufferY, m_backBufferWidth, m_backBufferHeight);
		}
	}
	else {
		createBitmap = true;
	}

	if (createBitmap) {
		HRESULT hr = CreateOverlayBitmap();
		if (FAILED(hr)) {
			if (m_textureErrorLoggingLimiter.acquire())
			{
				Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to recreate bitmap.");
			}
			return hr;
		}
	}

	if (m_backBufferChanged || createBitmap) {
		HRESULT hr = m_pOverlayBitmap->CopyFromMemory(NULL, m_pBackBuffer.get(), m_backBufferWidth * 4);
		if (FAILED(hr)) {
			if (m_textureErrorLoggingLimiter.acquire())
			{
				Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to copy backbuffer to bitmap.");
			}
			return hr;
		}

		// update the position of the overlay window
		SetRect(&m_location, m_backBufferX, m_backBufferY, m_backBufferX + m_backBufferWidth, m_backBufferY + m_backBufferHeight);

		m_backBufferChanged = false;
	}

	// draw bitmap
	D2D1_RECT_F dest;
	dest.top = m_backBufferY;
	dest.left = m_backBufferY;
	dest.bottom = m_backBufferY + m_backBufferHeight;
	dest.right = m_backBufferX + m_backBufferWidth;
	m_pD2DDeviceContext->DrawBitmap(m_pOverlayBitmap.Get(), dest);

	ThrowIfFailed(m_pD2DDeviceContext->EndDraw());

	// Release our wrapped render target resource. Releasing 
	// transitions the back buffer resource to the state specified
	// as the OutState when the wrapped resource was created.
	m_pD3D11On12Device->ReleaseWrappedResources(m_vecWrappedBackBuffers[m_frameIndex].GetAddressOf(), 1);

	// Flush to submit the 11 command list to the shared command queue.
	m_pD3D11DeviceContext->Flush();

	// Move to the next frame.
	MoveToNextFrame();

	return S_OK;
}

void DX12D2DOverlayInstance::UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update)
{
	Fuel::MutexTryLock renderLock(m_render_mutex);
	if (!m_pBackBuffer || update->getWidth() != m_backBufferWidth || update->getHeight() != m_backBufferHeight) {
		m_backBufferX = update->getX();
		m_backBufferY = update->getY();
		m_backBufferWidth = update->getWidth();
		m_backBufferHeight = update->getHeight();
		m_backBufferSize = m_backBufferWidth * m_backBufferHeight * 4;
		m_pBackBuffer.reset(new uint8_t[m_backBufferSize]);
	}

	bool fullBufferUpdate = false;
	if (update->GetDirtyRectCount() == 1) {
		const OverlayDirtyRect& rect = update->GetDirtyRect(0);
		if (rect.Width == m_backBufferWidth && rect.Height == m_backBufferHeight)
		{
			fullBufferUpdate = true;
		}
	}


	if (fullBufferUpdate)
	{
		const OverlayDirtyRect& dirtyRect = update->GetDirtyRect(0);
		memcpy_s(m_pBackBuffer.get(), m_backBufferSize, dirtyRect.Texture, m_backBufferSize);
	}
	else
	{
		// update the rects
		for (int i = 0; i < (int)update->GetDirtyRectCount(); i++)
		{
			const OverlayDirtyRect& dirtyRect = update->GetDirtyRect(i);
			uint8_t* buffer = m_pBackBuffer.get();
			uint8_t* bufferEnd = buffer + m_backBufferSize;

			uint32_t destWidth = update->getWidth() * 4;
			uint32_t srcWidth = dirtyRect.Width * 4;

			uint8_t* currentSrc = dirtyRect.Texture;
			uint8_t* currentDest = buffer + (dirtyRect.Y * destWidth) + (dirtyRect.X * 4);
			for (int line = 0; line < (int)dirtyRect.Height; line++, currentDest += destWidth, currentSrc += srcWidth)
			{
				memcpy_s(currentDest, bufferEnd - currentDest, currentSrc, srcWidth);
			}
		}
	}

	m_backBufferChanged = true;
}

HRESULT DX12D2DOverlayInstance::CreateOverlayBitmap()
{
	m_pOverlayBitmap.Reset();

	// size of bitmap
	D2D1_SIZE_U size;
	size.height = m_backBufferHeight;
	size.width = m_backBufferWidth;
	// properties of bitmap
	D2D1_PIXEL_FORMAT pixelFormat = D2D1::PixelFormat(
		DXGI_FORMAT_B8G8R8A8_UNORM,
		D2D1_ALPHA_MODE_PREMULTIPLIED
	);
	D2D1_BITMAP_PROPERTIES properties;
	properties.pixelFormat = pixelFormat;
	float dpiX;
	float dpiY;
	m_pD2DFactory->GetDesktopDpi(&dpiX, &dpiY);
	properties.dpiX = dpiX;
	properties.dpiY = dpiY;

	return m_pD2DDeviceContext->CreateBitmap(
		size,
		properties,
		&m_pOverlayBitmap
	);
}

void DX12D2DOverlayInstance::MoveToNextFrame()
{
	// Here we basically want to synchronize properly and wait until it is safe to move to the next frame.
	// Schedule a Signal command in the queue.
	const UINT64 currentFenceValue = m_fenceValues[m_frameIndex];
	ThrowIfFailed(m_pCommandQueue->Signal(m_fence.Get(), currentFenceValue));

	// Update the frame index.
	m_frameIndex = (m_frameIndex + 1) % m_frameCount;

	// If the next frame is not ready to be rendered yet, wait until it is ready.
	if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
	{
		ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
		WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
	}

	// Set the fence value for the next frame.
	m_fenceValues[m_frameIndex] = currentFenceValue + 1;
}

void DX12D2DOverlayInstance::WaitForGpu()
{
	// Schedule a Signal command in the queue.
	ThrowIfFailed(m_pCommandQueue->Signal(m_fence.Get(), m_fenceValues[m_frameIndex]));

	// Wait until the fence has been processed.
	ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
	WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);

	// Increment the fence value for the current frame.
	m_fenceValues[m_frameIndex]++;
}