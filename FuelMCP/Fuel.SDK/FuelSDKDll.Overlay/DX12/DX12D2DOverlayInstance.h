#pragma once

#include <dxgi1_4.h>
#include <d3d12.h>
#include "d3dx12.h"
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <d3d11on12.h>
#include <d2d1_3.h>
#include <dwrite.h>

#include <wrl.h>
#include <mutex>

#include "../IOverlayInstance.h"
#include "DX12HookSetup.h"
#include "LoggingLimiter.h"

// Note that while ComPtr is used to manage the lifetime of resources on the CPU,
// it has no understanding of the lifetime of resources on the GPU. Apps must account
// for the GPU lifetime of resources to avoid destroying objects that may still be
// referenced by the GPU.
using Microsoft::WRL::ComPtr;

inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		throw std::exception();
	}
}

namespace Fuel {
	namespace DX12 {
		class DX12D2DOverlayInstance : public IOverlayInstance {
		public:
			DX12D2DOverlayInstance(int instanceId, const RECT &location, int zIndex, DX12LibData libData);
			virtual ~DX12D2DOverlayInstance();

			void UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update);

			void Release();
			HRESULT Reinitialize();
			HRESULT ReinitializeUnsynchronized();
			HRESULT Render(IDXGISwapChain *pSwapChain);

		private:
			DX12LibData m_libData;

			IDXGISwapChain* m_pSwapChain;
			ID3D12CommandQueue* m_pCommandQueue;
			UINT m_frameIndex = 0;
			UINT m_frameCount = 0;
			UINT m_rtvDescriptorSize;

			// DirectX 12 Synchronization
			HANDLE m_fenceEvent;
			UINT64* m_fenceValues;
			ComPtr<ID3D12Fence> m_fence;

			ComPtr<ID3D11On12Device> m_pD3D11On12Device;
			ComPtr<ID3D12Device> m_pD3D12Device;
			ComPtr<ID3D11DeviceContext> m_pD3D11DeviceContext;
			ComPtr<ID2D1Factory3> m_pD2DFactory;
			ComPtr<ID2D1Device2> m_pD2DDevice;
			ComPtr<ID2D1DeviceContext2> m_pD2DDeviceContext;
			ComPtr<IDWriteFactory> m_pDWriteFactory;
			ComPtr<ID3D12DescriptorHeap> m_pRtvHeap;

			std::vector<ComPtr<ID3D12Resource>> m_vecRenderTargets;
			std::vector<ComPtr<ID3D11Resource>> m_vecWrappedBackBuffers;
			std::vector<ComPtr<ID2D1Bitmap1>> m_vecD2DRenderTargets;
			std::vector<ComPtr<ID3D12CommandAllocator>> m_vecCommandAllocators;

			// Overlay Image data
			bool m_backBufferChanged;
			uint32_t m_backBufferX;
			uint32_t m_backBufferY;
			uint32_t m_backBufferWidth;
			uint32_t m_backBufferHeight;
			uint32_t m_backBufferSize;
			std::unique_ptr<uint8_t[]> m_pBackBuffer;
			ComPtr<ID2D1Bitmap> m_pOverlayBitmap;

			std::mutex m_render_mutex;
			bool m_renderEnabled = false;

#ifdef _DEBUG
			// Frame skips are normal, this is only useful for debug logging.
			LoggingLimiter m_frameSkipLoggingLimiter;
#endif
			LoggingLimiter m_textureErrorLoggingLimiter;

			void MoveToNextFrame();
			void WaitForGpu();
			HRESULT CreateOverlayBitmap();
		};
	}
}