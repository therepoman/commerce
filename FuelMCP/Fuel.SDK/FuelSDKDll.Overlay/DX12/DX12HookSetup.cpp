#include <dxgi1_4.h>
#include <wrl.h>

#include "DX12HookSetup.h"
#include "DX12Helper.h"
#include  "../IOverlayInstance.h"

typedef HRESULT(__stdcall *D3D11CreateDeviceAndSwapChainType)(IDXGIAdapter *, D3D_DRIVER_TYPE, HMODULE Software, UINT Flags, const D3D_FEATURE_LEVEL *, UINT, UINT, const DXGI_SWAP_CHAIN_DESC *, IDXGISwapChain **, ID3D11Device **, D3D_FEATURE_LEVEL *, ID3D11DeviceContext **);
typedef HRESULT(__stdcall *CreateDXGIFactory1Type)(REFIID, void **);

using Microsoft::WRL::ComPtr;

int GetDX12FnOffsetInModule(voidFunc fnptr, wchar_t *refmodulepath, unsigned int refmodulepathLen, const std::string &logPrefix, const std::string &fnName) {
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Getting FN offset in module.");
	HMODULE hModule = NULL;

	if (!GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, (LPCWSTR)fnptr, &hModule)) {
		return -1;
	}

	const bool bInit = refmodulepath[0] == '\0';
	if (bInit) {
		GetModuleFileNameW(hModule, refmodulepath, refmodulepathLen);
	}
	else {
		wchar_t modulename[MAX_PATH];
		GetModuleFileNameW(hModule, modulename, ARRAY_NUM_ELEMENTS(modulename));
		if (_wcsicmp(modulename, refmodulepath) != 0) {
			std::wstring modulenameStr(modulename);
			std::string s1(modulenameStr.begin(), modulenameStr.end());
			std::wstring refmodulepathStr(refmodulepath);
			std::string s2(refmodulepathStr.begin(), refmodulepathStr.end());
			return -2;
		}
	}

	unsigned char *fn = reinterpret_cast<unsigned char *>(fnptr);
	unsigned char *base = reinterpret_cast<unsigned char *>(hModule);
	unsigned long off = static_cast<unsigned long>(fn - base);

	return static_cast<int>(off);
}

// Extract offsets for various functions in the IDXGISwapChain
// interface that need to be hooked.
void Fuel::DX12::GetLibData(DX12LibData* libData)
{
	HMODULE hDXGI = LoadLibrary(L"DXGI.DLL");
	wchar_t wcFileName[MAX_PATH];

	if (hDXGI != NULL) {
		GetModuleFileNameW(hDXGI, wcFileName, ARRAY_NUM_ELEMENTS(wcFileName));

		HWND hwnd = CreateWindowW(L"STATIC", L"SDK DXGI12 Window", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT, 640, 480, 0,
			NULL, NULL, 0);

		// Ultimately, what we're looking to do here is find the module offset for a couple of swap chain functions.
		// In order to do this, we need to create a factory, adapter, device, command queue and swap chain. Once these are all created,
		// we can examine the swap chain for the data we need.

		// First load up the procedure from the DLL to create a factory.
		CreateDXGIFactory1Type pCreateDXGIFactory1Type = reinterpret_cast<CreateDXGIFactory1Type>(GetProcAddress(hDXGI, "CreateDXGIFactory1"));

		// Next we declare all the objects we will need for this. Note that while ComPtr's are CPU objects, these resources need 
		// to stay in scope until the command list that references them has finished executing on the GPU. Since in our case,
		// we will not be rendering anything on these, we can safely let them be deleted once the function scope completes.
		ComPtr<IDXGIFactory4> loadedFactory;
		ComPtr<IDXGIAdapter1> loadedHardwareAdapter;
		ComPtr<ID3D12Device> loadedDevice;
		ComPtr<ID3D12CommandQueue> loadedCommandQueue;
		ComPtr<IDXGISwapChain1> loadedSwapChain;
		ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&loadedFactory)));

		// Find a suitable adapter.
		ComPtr<IDXGIAdapter1> adapter;
		for (UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != loadedFactory->EnumAdapters1(adapterIndex, &adapter); ++adapterIndex)
		{
			DXGI_ADAPTER_DESC1 desc;
			adapter->GetDesc1(&desc);

			if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			{
				// Don't select the Basic Render Driver adapter.
				continue;
			}

			// Check to see if the adapter supports Direct3D 12.
			if (SUCCEEDED(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr)))
			{
				break;
			}
		}

		loadedHardwareAdapter = adapter.Detach();

		// Now the we have an adapter, we can create a device, command queue, and finally, swap chain (what we want).
		ThrowIfFailed(D3D12CreateDevice(
			loadedHardwareAdapter.Get(),
			D3D_FEATURE_LEVEL_11_0,
			IID_PPV_ARGS(&loadedDevice)
		));

		// Describe and create the command queue.
		D3D12_COMMAND_QUEUE_DESC queueDesc = {};
		queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
		queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

		ThrowIfFailed(loadedDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&loadedCommandQueue)));

		// Describe the swap chain. (We probably don't need to do a lot of this....)
		DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
		swapChainDesc.BufferCount = 3;
		swapChainDesc.Width = 640;
		swapChainDesc.Height = 480;
		swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		swapChainDesc.SampleDesc.Count = 1;

		// Finally create the swap chain. (Probably need to check the non-hwnd case as well).
		ThrowIfFailed(loadedFactory->CreateSwapChainForHwnd(
			loadedCommandQueue.Get(),		// Swap chain needs the queue so that it can force a flush on it.
			hwnd,
			&swapChainDesc,
			nullptr,
			nullptr,
			&loadedSwapChain
		));

		// Try and find command queue offset
		uint64_t queueOffset;
		uint64_t swapChainAddress = (uint64_t)loadedSwapChain.Get();
		uint64_t commandQueueAddress = (uint64_t)loadedCommandQueue.Get();

		// This will search the next ~8 KiB for the queue pointer in the swap chain.
		// If we find it we should then be able to reliable retrieve the queue from
		// other swap chain objects of the same type.
		// The limit was chosen as a relatively "safe" limit found while debugging,
		// but the current location being well within.
		libData->commandQueueOffset = OFFSET_NOT_FOUND;
		for (queueOffset = 0; queueOffset < 1000; queueOffset += sizeof(uint64_t)) {
			uint64_t data = *reinterpret_cast<uint64_t *>(swapChainAddress + queueOffset);
			if (data == commandQueueAddress) {
				libData->commandQueueOffset = queueOffset;
				break;
			}
		}

		if (libData->commandQueueOffset == OFFSET_NOT_FOUND) {
			throw Fuel::DirectXHookCreationFailed();
		}

		// For VC++ the vtable is located at the base addr. of the object and each function entry is a single pointer. Since p.e. the base classes
		// of IDXGISwapChain have a total of 8 functions the 8+Xth entry points to the Xth added function in the derived interface.
		void ***vtbl = (void ***)loadedSwapChain.Get();

		void *pPresent = (*vtbl)[8];
		int offset = GetDX12FnOffsetInModule(reinterpret_cast<voidFunc>(pPresent), wcFileName, ARRAY_NUM_ELEMENTS(wcFileName), "D3D12", "Present");
		if (offset >= 0) {
			unsigned char* raw = (unsigned char*)hDXGI;
			libData->presentFunction = (voidFunc)(raw + offset);
		}

		void *pResizeBuffers = (*vtbl)[8 + 5];
		offset = GetDX12FnOffsetInModule(reinterpret_cast<voidFunc>(pResizeBuffers), wcFileName, ARRAY_NUM_ELEMENTS(wcFileName), "D3D12", "ResizeBuffers");
		if (offset >= 0) {
			unsigned char* raw = (unsigned char*)hDXGI;
			libData->resizeFunction = (voidFunc)(raw + offset);
		}

		FreeLibrary(hDXGI);
		DestroyWindow(hwnd);
	}
	else {
		FreeLibrary(hDXGI);
	}
}