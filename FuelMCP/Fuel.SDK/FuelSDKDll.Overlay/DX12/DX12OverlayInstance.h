#include <d3d12.h>
#include <DirectXMath.h>
#include <dxgi1_4.h>
#include <wrl.h>

#include <mutex>
#include <vector>

#include "../IOverlayInstance.h"
#include "DX12HookSetup.h"

using Microsoft::WRL::ComPtr;

namespace Fuel {
	namespace DX12 {
		class DX12OverlayInstance : public IOverlayInstance {
		public:
			DX12OverlayInstance(int instanceId, const RECT &location, int zIndex, DX12LibData libData);
			~DX12OverlayInstance();

			HRESULT RenderOverlay(IDXGISwapChain* pSwapChain);

			HRESULT Reinitialize();
			void Release();

			void UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update);
		private:
			static const UINT TexturePixelSizeInBytes = 4;

		    // Vertex definition.
			struct Vertex
			{
				DirectX::XMFLOAT3 position;
				DirectX::XMFLOAT2 uv;
			};

			// Helper function for resolving the full path of assets.
			std::wstring GetAssetFullPath(LPCWSTR assetName)
			{
				return m_assetsPath + assetName;
			}

			// TMP -- Maintaining a copy of the TEX data here makes no sense, its already in a resource. We should be directly updating that instead of re-applying this shit every time.
			std::vector<UINT8> m_textureData;

			// Variables queried from swap chain
			UINT m_frameCount;
			UINT m_width;
			UINT m_height;
			UINT m_textureWidth;
			UINT m_textureHeight;

			// Texture management objects.
			ComPtr<ID3D12CommandAllocator> m_textureUploadCommandAllocator;
			ComPtr<ID3D12GraphicsCommandList> m_textureUploadCommandList;
			ComPtr<ID3D12Resource> m_textureUploadResource;

			// Rendering objects
			D3D12_VIEWPORT m_viewport;
			D3D12_RECT m_scissorRect;
			ComPtr<ID3D12Device> m_d3d12Device;
			std::vector<ComPtr<ID3D12Resource>> m_renderTargets;
			std::vector<ComPtr<ID3D12CommandAllocator>> m_commandAllocators;
			ComPtr<ID3D12RootSignature> m_rootSignature;
			ComPtr<ID3D12PipelineState> m_pipelineState;
			ComPtr<ID3D12GraphicsCommandList> m_commandList;

			// Resources
			ComPtr<ID3D12DescriptorHeap> m_rtvHeap;
			ComPtr<ID3D12DescriptorHeap> m_srvHeap;
			UINT m_rtvDescriptorSize;
			UINT m_srvDescriptorSize;
			ComPtr<ID3D12Resource> m_vertexBuffer;
			ComPtr<ID3D12Resource> m_texture;
			D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;

			// Root assets path.
			std::wstring m_assetsPath;

			// The command queue fetched from the swap chain's raw data...fairly flimsy....
			ComPtr<ID3D12CommandQueue> m_fetchedCommandQueue;

			// Synchronization objects.
			std::mutex m_render_mutex;
			UINT m_frameIndex;
			HANDLE m_fenceEvent;
			ComPtr<ID3D12Fence> m_fence;
			UINT64* m_fenceValues;

			bool m_initialized;
			bool m_updated;

			// Information about D3D12 lib
			DX12LibData m_libData;

			// Rendering helper functions.
			std::vector<UINT8> GenerateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update);
			void UpdateTextureInternal(std::shared_ptr<Fuel::IOverlayUpdate> update);
			void PopulateCommandList();

			// Synchronization helper functions.
			void WaitForGpu();
			void MoveToNextFrame();

			// The internal initialization function, called from within render.
			void Initialize(IDXGISwapChain* pSwapChain);

			// Creation helpers.
			void CreateFrameResources(IDXGISwapChain* pSwapChain);
			void CreateDescriptorHeaps();
			void CreateRootSignature();
			void CreateCommandLists();
			void CreateFences();
			void CreatePSO();
			void CreateMeshBuffers();
			void CreateTexture();
		};
	}
}