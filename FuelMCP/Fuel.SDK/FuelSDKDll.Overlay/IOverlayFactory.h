#pragma once

#include <Windows.h>
#include <functional>
#include <memory>
#include "CommonOverlayTypes.h"
#include "IOverlayInstance.h"
#include "OverlayManager.h"
#include "fuel/core/Types.h"

namespace Fuel {
	class OverlayManager;

	class IOverlayFactory
	{
	public:
		IOverlayFactory();
		virtual ~IOverlayFactory();
		static std::shared_ptr<Fuel::IOverlayFactory> GetInstance(Fuel::RendererType type);

		virtual void Init() = 0;
		virtual void Release() = 0;

		virtual std::unique_ptr<IOverlayInstance> CreateOverlay(int instanceId, const RECT &location, int zIndex) = 0;

		virtual bool getBufferSize(BufferSize &bufferSize) = 0;

		void SetOverlayManager(const std::shared_ptr<OverlayManager> &overlayManager);
	protected:
		// Weak reference to avoid circular shared pointers.
		std::weak_ptr<OverlayManager> m_overlayManager;
	};
}
