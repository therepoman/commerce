#include "IOverlayFactory.h"
#include "DX11/DX11OverlayFactory.h"

#ifdef FUEL_DX_12
	#include "DX12/DX12OverlayFactory.h"
#endif

Fuel::IOverlayFactory::IOverlayFactory()
{
}

Fuel::IOverlayFactory::~IOverlayFactory()
{
	Release();
}

std::shared_ptr<Fuel::IOverlayFactory> Fuel::IOverlayFactory::GetInstance(Fuel::RendererType type) {
	if (type == Fuel::RendererType::DIRECT_X_11) {
		std::shared_ptr<Fuel::DX11::DX11OverlayFactory> instance = Fuel::DX11::DX11OverlayFactory::Instance();
		return std::move(instance);
	}
#ifdef FUEL_DX_12
	else if (type == Fuel::RendererType::DIRECT_X_12) {
		std::shared_ptr<Fuel::DX12::DX12OverlayFactory> instance = Fuel::DX12::DX12OverlayFactory::Instance();
		return std::move(instance);
	}
#endif
	else {
		return Fuel::DX11::DX11OverlayFactory::Instance();
	}
}

void Fuel::IOverlayFactory::SetOverlayManager(const std::shared_ptr<OverlayManager> &overlayManager)
{
	m_overlayManager = overlayManager;
}

void Fuel::IOverlayFactory::Release()
{
}
