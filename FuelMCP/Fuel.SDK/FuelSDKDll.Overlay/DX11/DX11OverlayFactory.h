#include <memory>
#include <d3d11_1.h>
#include <directxmath.h>
#include <functional>

#include "DX11OverlayInstance.h"
#include "../IOverlayFactory.h"

namespace Fuel {
	namespace DX11 {
		class DX11OverlayFactory : public Fuel::IOverlayFactory {
		public:
			static std::shared_ptr<DX11OverlayFactory> Instance();
			virtual ~DX11OverlayFactory();

			virtual void Init();
			virtual void Release();
			
			virtual std::unique_ptr<IOverlayInstance> CreateOverlay(int instanceId, const RECT &location, int zIndex);

			virtual bool getBufferSize(BufferSize &bufferSize);

			HRESULT Present(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags);
			HRESULT Resize(IDXGISwapChain *pSwapChain, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);

			voidFunc GetOriginalPresentFunction();
			voidFunc GetOriginalResizeFunction();
		private:
			DX11OverlayFactory();
			Fuel::Hooking::Hook m_presentHook;
			Fuel::Hooking::Hook m_resizeHook;
			LONG m_overlayInit = 0;
			HWND m_hWnd = 0;
			CComPtr<IDXGISwapChain> m_swapChain;
			bool m_hooksEnabled = false;
			HCURSOR m_prevCursor;
			LoggingLimiter m_getHwndErrorLoggingLimiter;
		};
	}
}
