#pragma once

#include <d3d11_1.h>
#include <directxmath.h>
#include <mutex>
#include "D11StateBlock.h"
#include "../IOverlayInstance.h"
#include "../IOverlayUpdate.h"
#include "../CommonOverlayTypes.h"
#include "LoggingLimiter.h"
#include <atlbase.h>

namespace Fuel {
	namespace DX11 {
		class DX11OverlayInstance : public IOverlayInstance
		{
		public:
			DX11OverlayInstance(int instanceId, const RECT &location, int zIndex);
			virtual ~DX11OverlayInstance();

			void UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update);

			void Release();
			HRESULT Reinitialize();
			HRESULT Render(IDXGISwapChain *pSwapChain);
		private:
			HRESULT ReinitializeUnsynchronized();
		
			bool m_backBufferChanged;
			uint32_t m_backBufferX;
			uint32_t m_backBufferY;
			uint32_t m_backBufferWidth;
			uint32_t m_backBufferHeight;
			uint32_t m_backBufferSize;
			std::unique_ptr<uint8_t[]> m_pBackBuffer;

			CComPtr<ID3D11Buffer> m_pVertexBuffer;
			CComPtr<ID3D11Buffer> m_pIndexBuffer;
			CComPtr<ID3D11Buffer> m_pCBProjection;
			CComPtr<ID3D11SamplerState> m_pSamplerLinear;
			CComPtr<ID3D11InputLayout> m_pInputLayout;
			CComPtr<ID3D11Texture2D> m_pTexture;
			CComPtr<ID3D11ShaderResourceView> m_pTextureRV;
			CComPtr<ID3D11VertexShader> m_pVertexShader;
			CComPtr<ID3D11PixelShader> m_pPixelShader;
			DirectX::XMMATRIX *m_projection;
			CComPtr<ID3D11Device> m_pDevice;
			CComPtr<ID3D11DeviceContext> m_pDeviceContext;
			CComPtr<IDXGISwapChain> m_pSwapChain;
			bool m_bDeferredContext = false;
			std::unique_ptr<D11StateBlock> m_pOriginalStateBlock;
			std::unique_ptr<D11StateBlock> m_pMyStateBlock;
			D3D11_VIEWPORT vp;
			CComPtr<ID3D11RenderTargetView> m_pRenderTargetView;

			HRESULT DX11OverlayInstance::NewTexture(unsigned int x, unsigned int y, unsigned int w, unsigned int h);

			std::mutex m_render_mutex;
			bool m_renderEnabled = false;
			bool m_textureInitialized = false;
#ifdef _DEBUG
			// Frame skips are normal, this is only useful for debug logging.
			LoggingLimiter m_frameSkipLoggingLimiter;
#endif
			LoggingLimiter m_textureErrorLoggingLimiter;
			LoggingLimiter m_initializeErrorLoggingLimiter;
		};
	}
}
