#pragma once

#include "Hook.h"
#include <d3d11_1.h>
#include "../CommonOverlayTypes.h"

#define ARRAY_NUM_ELEMENTS(x) (sizeof(x)/sizeof((x)[0]))

typedef void *(*voidFunc)();

namespace Fuel {
	namespace DX11 {
		const int MODULEFILEPATH_BUFLEN = 2048;

		struct DXGIData {
			// Filepath of the module the offsets are for
			wchar_t wcFileName[MODULEFILEPATH_BUFLEN];
			int iOffsetPresent;
			int iOffsetResize;
		};

		void SetupSwapChainHooks(Fuel::Hooking::Hook *presentHook, voidFunc overlayPresent, Fuel::Hooking::Hook *resizeHook, voidFunc overlayResize);

		bool GetBufferSize(IDXGISwapChain *swapChain, BufferSize &bufferSize);
	}
}