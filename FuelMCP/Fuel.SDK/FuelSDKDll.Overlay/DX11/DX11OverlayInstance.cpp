#include "DX11OverlayInstance.h"
#include "DDSTextureLoader.h"
#include "logger.h"
#include "MutexTryLock.h"

#include "../overlayVS.h"
#include "../overlayPS.h"

using namespace DirectX;
using namespace Fuel::DX11;

struct OverlayVertex
{
	XMFLOAT3 Pos;
	XMFLOAT2 Tex;
};

struct CBProjection
{
	XMMATRIX mProjection;
};

DX11OverlayInstance::DX11OverlayInstance(int instanceId, const RECT &location, int zIndex) :
    IOverlayInstance(instanceId, location, zIndex),
#ifdef _DEBUG
	m_frameSkipLoggingLimiter(20, FUEL_DLL_LOGGER,
		"Too many frames skipped, disabling logging of this message"),
#endif
	m_textureErrorLoggingLimiter(20, FUEL_DLL_LOGGER,
		"Too many texture errors, disabling logging of this message"),
	m_initializeErrorLoggingLimiter(20, FUEL_DLL_LOGGER,
		"Too many initialization errors, disabling logging of this message")
{
	m_pOriginalStateBlock = nullptr;
	m_pMyStateBlock = nullptr;

	// This struct needs to be aligned 16, see declaration.
	m_projection = (DirectX::XMMATRIX*)_aligned_malloc(sizeof(DirectX::XMMATRIX), 16);

	// initialize the back buffer to empty
	m_backBufferChanged = false;
	m_backBufferX = 0;
	m_backBufferY = 0;
	m_backBufferWidth = 0;
	m_backBufferHeight = 0;
}

DX11OverlayInstance::~DX11OverlayInstance()
{
	Release();

	_aligned_free(m_projection);
	m_projection = nullptr;
}

void DX11OverlayInstance::Release()
{
	// Disable rendering during release.
	std::lock_guard<std::mutex> lock(m_render_mutex);

	m_renderEnabled = false;
	m_textureInitialized = false;

	m_pOriginalStateBlock.reset();
	m_pMyStateBlock.reset();
	if (m_pDeviceContext)
	{
		m_pDeviceContext->OMSetRenderTargets(0, NULL, NULL);
	}

	m_pVertexBuffer.Release();
	m_pIndexBuffer.Release();
	m_pCBProjection.Release();
	m_pSamplerLinear.Release();
	m_pInputLayout.Release();
	m_pTexture.Release();
	m_pTextureRV.Release();
	m_pVertexShader.Release();
	m_pPixelShader.Release();
	m_pRenderTargetView.Release();

	m_pDeviceContext.Release();
	m_pDevice.Release();
}

HRESULT DX11OverlayInstance::NewTexture(unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	HRESULT hr;

	// Textures contain garbage after creation.  This line is needed to avoid rendering the garbage after resizing if the pump is slow to send an update.
	m_textureInitialized = false;

	m_pTexture.Release();
	m_pTextureRV.Release();
	m_pVertexBuffer.Release();

	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Width = w;
	desc.Height = h;
	desc.MipLevels = desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = m_pDevice->CreateTexture2D(&desc, NULL, &m_pTexture);

	if (FAILED(hr)) {
		if (m_textureErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Failed to create texture.");
		}
		return hr;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = desc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = desc.MipLevels;

	hr = m_pDevice->CreateShaderResourceView(m_pTexture, &srvDesc, &m_pTextureRV);
	if (FAILED(hr)) {
		if (m_textureErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Failed to create resource view.");
		}
		return hr;
	}

	BufferSize bufferSize;
	if (!GetBufferSize(m_pSwapChain, bufferSize))
	{
		if (m_textureErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Failed to get swap chain buffer size.");
		}
		return -1;
	}

	float left = x - ((float)bufferSize.width / 2.0f);
	float right = left + w;
	float top = ((float)bufferSize.height / 2.0f) - y;
	float bottom = top - h;

	OverlayVertex vertices[] =
	{
		{ XMFLOAT3(left, top, 10.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(right, bottom, 10.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(left, bottom, 10.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(left, top, 10.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(right, top, 10.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(right, bottom, 10.0f), XMFLOAT2(1.0f, 1.0f) }
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(OverlayVertex)* 6;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;
	hr = m_pDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer);
	if (FAILED(hr))
		return hr;

	SetRect(&m_location, x, y, x + w, y + h);

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Overlay texture created. Size {0}x{1}", w, h);
	return S_OK;
}

HRESULT DX11OverlayInstance::Reinitialize()
{
	std::lock_guard<std::mutex> lock(m_render_mutex);
	return ReinitializeUnsynchronized();
}

HRESULT DX11OverlayInstance::ReinitializeUnsynchronized()
{
	if (!m_pSwapChain)
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Reinitialize called before swap chain was initialized");
		}
		return -1;
	}

	// Clean up anything left over from a previously failed initialize.
	m_pDevice.Release();
	m_pDeviceContext.Release();
	m_pRenderTargetView.Release();
	m_pVertexShader.Release();
	m_pPixelShader.Release();
	m_pSamplerLinear.Release();
	m_pIndexBuffer.Release();
	m_pCBProjection.Release();
	m_pInputLayout.Release();

	HRESULT hr = m_pSwapChain->GetDevice(__uuidof(ID3D11Device), (void**)&m_pDevice);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Unable to get device from swap chain. Code {0}", hr);
		}
		return hr;
	}

	CComPtr<ID3D11Texture2D> backBuffer;
	hr = m_pSwapChain->GetBuffer(0, __uuidof(*backBuffer), (LPVOID*)&backBuffer);
	if (FAILED(hr)) {
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: pSwapChain->GetBuffer failure!");
		}
		return hr;
	}

	hr = m_pDevice->CreateDeferredContext(0, &m_pDeviceContext);
	// We may be unable to create a deferred context, in that case we fall
	// back to using an immediate context and saving and restoring
	// state
	if (FAILED(hr) || !m_pDeviceContext) {
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Failed to create DeferredContext. Getting ImmediateContext");
		}
		m_pDevice->GetImmediateContext(&m_pDeviceContext);

		// capture original state
		m_pOriginalStateBlock = std::make_unique<D11StateBlock>(m_pDeviceContext);
		m_pMyStateBlock = std::make_unique<D11StateBlock>(m_pDeviceContext);
		m_pOriginalStateBlock->Capture();

		m_bDeferredContext = false;
	}
	else {
		m_bDeferredContext = true;
	}

	D3D11_TEXTURE2D_DESC backBufferSurfaceDesc;
	backBuffer->GetDesc(&backBufferSurfaceDesc);

	memset(&vp, 0, sizeof(vp));
	vp.Width = static_cast<float>(backBufferSurfaceDesc.Width);
	vp.Height = static_cast<float>(backBufferSurfaceDesc.Height);
	vp.MinDepth = 0;
	vp.MaxDepth = 1;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	m_pDeviceContext->RSSetViewports(1, &vp);

	hr = m_pDevice->CreateRenderTargetView(backBuffer, NULL, &m_pRenderTargetView);
	if (FAILED(hr)) {
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: pDevice->CreateRenderTargetView failed!");
		}
		return hr;
	}

	m_pDeviceContext->OMSetRenderTargets(1, &(m_pRenderTargetView.p), NULL);

	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			
	CComPtr<ID3D11BlendState> blendState;
	hr = m_pDevice->CreateBlendState(&blendDesc, &blendState);
	if (FAILED(hr)) {
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: pDevice->CreateRenderTargetView failed!");
		}
		return hr;
	}
	m_pDeviceContext->OMSetBlendState(blendState, NULL, 0xffffffff);

	// Get client width and height
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	hr = m_pSwapChain->GetDesc(&swapChainDesc);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Failed to get client width and height");
		}
		return hr;
	}

	HWND hWnd = swapChainDesc.OutputWindow;
	RECT rect;
	if (!GetClientRect(hWnd, &rect))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("D3D11: Failed to GetClientRect");
		}
		return -1;
	}
	float viewWidth = (float)(rect.right - rect.left);
	float viewHeight = (float)(rect.bottom - rect.top);

	// Create the vertex shader
	hr = m_pDevice->CreateVertexShader(overlayVS, sizeof(overlayVS), nullptr, &m_pVertexShader);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create overlay vertex shader.");
		}
		return hr;
	}

	// Create the pixel shader
	hr = m_pDevice->CreatePixelShader(overlayPS, sizeof(overlayPS), nullptr, &m_pPixelShader);
	if (FAILED(hr)) {
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create overlay pixel shader.");
		}
		return hr;
	}

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = m_pDevice->CreateSamplerState(&sampDesc, &m_pSamplerLinear);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create sampler state.");
		}
		return hr;
	}

	DWORD indices[] = { 0, 1, 2, 3, 4, 5 };
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(DWORD)* 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = indices;
	hr = m_pDevice->CreateBuffer(&bd, &InitData, &m_pIndexBuffer);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create index buffer.");
		}
		return hr;
	}

	// Create the constant buffers
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(CBProjection);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = m_pDevice->CreateBuffer(&bd, nullptr, &m_pCBProjection);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create projection buffer.");
		}
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = m_pDevice->CreateInputLayout(layout, numElements, overlayVS,
		sizeof(overlayVS), &m_pInputLayout);
	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create input layout.");
		}
		return hr;
	}

	if (!IsIconic(hWnd))
		*m_projection = XMMatrixOrthographicLH(viewWidth, viewHeight, 0.0f, 100.0f);

	hr = NewTexture(m_location.left, m_location.top, (int)(m_location.right - m_location.left), (int)((m_location.bottom - m_location.top)));

	if (FAILED(hr))
	{
		if (m_initializeErrorLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to create texture or vertex buffer.");
		}
		return hr;
	}

	if (!m_bDeferredContext) {
		m_pMyStateBlock->Capture();
		m_pOriginalStateBlock->Apply();
	}

	m_renderEnabled = true;

	return S_OK;
}

void DX11OverlayInstance::UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update)
{
	Fuel::MutexTryLock renderLock(m_render_mutex);
	if (!m_pBackBuffer || update->getWidth() != m_backBufferWidth || update->getHeight() != m_backBufferHeight) {
		m_backBufferX = update->getX();
		m_backBufferY = update->getY();
		m_backBufferWidth = update->getWidth();
		m_backBufferHeight = update->getHeight();
		m_backBufferSize = m_backBufferWidth * m_backBufferHeight * 4;
		m_pBackBuffer.reset(new uint8_t[m_backBufferSize]);
	}

	bool fullBufferUpdate = false;
	if (update->GetDirtyRectCount() == 1) {
		const OverlayDirtyRect& rect = update->GetDirtyRect(0);
		if (rect.Width == m_backBufferWidth && rect.Height == m_backBufferHeight)
		{
			fullBufferUpdate = true;
		}
	}


	if (fullBufferUpdate) 
	{
		const OverlayDirtyRect& dirtyRect = update->GetDirtyRect(0);
		memcpy_s(m_pBackBuffer.get(), m_backBufferSize, dirtyRect.Texture, m_backBufferSize);
	}
	else
	{
		// update the rects
		for (int i = 0; i < (int) update->GetDirtyRectCount(); i++)
		{
			const OverlayDirtyRect& dirtyRect = update->GetDirtyRect(i);
			uint8_t* buffer = m_pBackBuffer.get();
			uint8_t* bufferEnd = buffer + m_backBufferSize;

			uint32_t destWidth = update->getWidth() * 4;
			uint32_t srcWidth = dirtyRect.Width * 4;

			uint8_t* currentSrc = dirtyRect.Texture;
			uint8_t* currentDest = buffer + (dirtyRect.Y * destWidth) + (dirtyRect.X * 4);
			for (int line = 0; line < (int) dirtyRect.Height; line++, currentDest += destWidth, currentSrc += srcWidth)
			{
				memcpy_s(currentDest, bufferEnd - currentDest, currentSrc, srcWidth);
			}
		}
	}

	m_backBufferChanged = true;

}

HRESULT DX11OverlayInstance::Render(IDXGISwapChain *pSwapChain)
{
	Fuel::MutexTryLock renderLock(m_render_mutex);

	if (!renderLock.isLocked())
	{
		// Another thread is initializing or tearing down graphics resources, can't render.
#ifdef _DEBUG
		if (m_frameSkipLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Render mutex is locked, skipping frame.");
		}
#endif
		return S_OK; // It will eventually recover.
	}

	if (!m_renderEnabled)
	{
		m_pSwapChain = pSwapChain;

		HRESULT hr = ReinitializeUnsynchronized();
		if (FAILED(hr))
		{
			// Unable to initialize, can't render.  ReinitializeUnsynchronized will log the error.
			return hr;
		}
	}

	if (!m_pBackBuffer)
	{
		// Texture hasn't been set to anything yet, no reason to render anything.
#ifdef _DEBUG
		if (m_frameSkipLoggingLimiter.acquire())
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Texture has not yet been updated, skipping frame.");
		}
#endif
		return S_OK; // It will eventually recover.
	}
	
	bool createTexture;
	if (m_pTexture)
	{
		D3D11_TEXTURE2D_DESC desc;
		m_pTexture->GetDesc(&desc);
		createTexture = desc.Width != m_backBufferWidth || desc.Height != m_backBufferHeight ||
			m_location.left != m_backBufferX || m_location.top != m_backBufferY;
		if (createTexture)
		{
			Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Overlay texture resize ({0}, {1}, {2}, {3}) -> ({4}, {5}, {6}, {7})",
				m_location.left, m_location.top, desc.Width, desc.Height,
				m_backBufferX, m_backBufferY, m_backBufferWidth, m_backBufferHeight);
		}
	}
	else
	{
		createTexture = true;
	}

	if (createTexture)
	{
		HRESULT hr = NewTexture(m_backBufferX, m_backBufferY, m_backBufferWidth, m_backBufferHeight);
		if (FAILED(hr))
		{
			if (m_textureErrorLoggingLimiter.acquire())
			{
				Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to recreate texture.");
			}
			return hr;
		}
	}

	if (m_backBufferChanged || createTexture)
	{
		// lock the texture and write the backbuffer in one large chunk
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		HRESULT hr = m_pDeviceContext->Map(m_pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (FAILED(hr))
		{
			if (m_textureErrorLoggingLimiter.acquire())
			{
				Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to map texture.");
			}
			return hr;
		}
		memcpy_s(mappedResource.pData, m_backBufferSize, m_pBackBuffer.get(), m_backBufferSize);
		
		m_pDeviceContext->Unmap(m_pTexture, 0);

		// update the position of the overlay window
		SetRect(&m_location, m_backBufferX, m_backBufferY, m_backBufferX + m_backBufferWidth, m_backBufferY + m_backBufferHeight);

		m_backBufferChanged = false;
	}

	m_textureInitialized = true;

	unsigned int stride;
	unsigned int offset;

	stride = sizeof(OverlayVertex);
	offset = 0;

	if (!m_bDeferredContext) {
		m_pOriginalStateBlock->Capture();
		m_pMyStateBlock->Apply();
	}

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	m_pDeviceContext->IASetVertexBuffers(0, 1, &(m_pVertexBuffer.p), &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	m_pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// setup projection matrix
	CBProjection cb;
	cb.mProjection = *m_projection;
	m_pDeviceContext->UpdateSubresource(m_pCBProjection, 0, nullptr, &cb, 0, 0);

	// Set the vertex and pixel shaders that will be used to render this triangle.
	m_pDeviceContext->IASetInputLayout(m_pInputLayout);

	m_pDeviceContext->VSSetShader(m_pVertexShader, NULL, 0);
	m_pDeviceContext->VSSetConstantBuffers(0, 1, &(m_pCBProjection.p));

	// Set the sampler state in the pixel shader.
	m_pDeviceContext->PSSetShader(m_pPixelShader, NULL, 0);
	m_pDeviceContext->PSSetConstantBuffers(0, 1, &(m_pCBProjection.p));
	m_pDeviceContext->PSSetShaderResources(0, 1, &(m_pTextureRV.p));
	m_pDeviceContext->PSSetSamplers(0, 1, &(m_pSamplerLinear.p));

	// Render the triangle.
	m_pDeviceContext->DrawIndexed(6, 0, 0);

	if (m_bDeferredContext)
	{
		CComPtr<ID3D11CommandList> commandList;
		HRESULT hr = m_pDeviceContext->FinishCommandList(TRUE, &commandList);
		if (FAILED(hr))
		{
			if (m_textureErrorLoggingLimiter.acquire())
			{
				Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to finish command list.");
			}
			return hr;
		}
		CComPtr<ID3D11DeviceContext> ctx;
		m_pDevice->GetImmediateContext(&ctx);
		ctx->ExecuteCommandList(commandList, TRUE);
	}
	else
	{
		m_pDeviceContext->Flush();
		m_pMyStateBlock->Capture();
		m_pOriginalStateBlock->Apply();
	}

	return S_OK;
}

