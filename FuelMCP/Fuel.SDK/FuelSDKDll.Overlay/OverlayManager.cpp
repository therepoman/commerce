#include "OverlayManager.h"

#include "logger.h"

Fuel::OverlayManager::OverlayManager(const std::shared_ptr<IOverlayFactory> &overlayFactory) : m_overlayFactory(overlayFactory)
{
}

Fuel::OverlayManager::~OverlayManager()
{
}

void Fuel::OverlayManager::SetResizeCallback(RESIZE_CALLBACK callback)
{
	m_resizeCallback = callback;
}

void Fuel::OverlayManager::SetEnableInputHooksCallback(ENABLE_INPUT_HOOKS_CALLBACK callback)
{
	m_enableInputHooksCallback = callback;
}

void Fuel::OverlayManager::SetDisableInputHooksCallback(DISABLE_INPUT_HOOKS_CALLBACK callback)
{
	m_disableInputHooksCallback = callback;
}

void Fuel::OverlayManager::SetOverlayFailureCallback(OVERLAY_FAILURE_CALLBACK callback)
{
	m_overlayFailureCallback = callback;
}

void Fuel::OverlayManager::SetHWnd(HWND hWnd)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	m_hWnd = hWnd;
	if (!m_inputHooksEnabled && !m_instances.empty() && m_enableInputHooksCallback)
	{
		m_enableInputHooksCallback(m_hWnd);
		m_inputHooksEnabled = true;
	}
}

void Fuel::OverlayManager::SetBufferSize(BufferSize &bufferSize)
{
	if (m_resizeCallback)
	{
		m_resizeCallback(bufferSize);
		m_bufferSize = bufferSize;
	}
}

void Fuel::OverlayManager::Show(int instanceId, RECT rect, int zIndex)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	bool firstInstance = m_instances.empty();
	std::unique_ptr<IOverlayInstance> &instance = m_instances[instanceId];
	if (instance)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Overlay is already visible.");
	}
	else
	{
		if (firstInstance)
		{
			m_overlayFactory->Init();
			if (m_hWnd && m_enableInputHooksCallback)
			{
				m_enableInputHooksCallback(m_hWnd);
				m_inputHooksEnabled = true;
			}
		}
		instance = m_overlayFactory->CreateOverlay(instanceId, rect, zIndex);
		UpdateRenderOrder();

		BufferSize bufferSize;
		if (m_resizeCallback && m_overlayFactory && m_overlayFactory->getBufferSize(bufferSize))
		{
			m_resizeCallback(bufferSize);
		}
	}
}

void Fuel::OverlayManager::Hide(int instanceId)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	if (m_instances.find(instanceId) != m_instances.end())
	{
		m_instances.erase(instanceId);
		UpdateRenderOrder();
		if (m_instances.empty())
		{
			m_overlayFactory->Release();
			if (m_disableInputHooksCallback)
			{
				m_disableInputHooksCallback();
				m_inputHooksEnabled = false;
			}
		}
	}
	else
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Can't hide overlay, it's not currently visible.");
	}
}

void Fuel::OverlayManager::HideAll()
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	if (!m_instances.empty())
	{
		m_instances.clear();
		UpdateRenderOrder();
		m_overlayFactory->Release();
		if (m_disableInputHooksCallback)
		{
			m_disableInputHooksCallback();
			m_inputHooksEnabled = false;
		}
	}
}

void Fuel::OverlayManager::UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	auto iterator = m_instances.find(update->getInstanceId());
	if (iterator != m_instances.end() && iterator->second)
	{
		iterator->second->UpdateTexture(update);
	}
	else
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Overlay {0} not found, unable to update texture.", update->getInstanceId());
	}
}

void Fuel::OverlayManager::UpdateCursor(int type)
{
	if (m_hWnd)
	{
		SetClassLongPtr(m_hWnd, GCLP_HCURSOR, (LONG_PTR)type);
	}
}

void Fuel::OverlayManager::Render(INSTANCE_HANDLER handler)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	for (auto iterator = m_instanceRenderOrder.begin(); iterator != m_instanceRenderOrder.end(); ++iterator)
	{
		HRESULT hr = handler(**iterator);
		if (FAILED(hr) && m_overlayFailureCallback)
		{
			m_overlayFailureCallback(**iterator);
		}
	}
}

void Fuel::OverlayManager::Resize(BufferSize &bufferSize)
{
	if (m_resizeCallback)
	{
		m_resizeCallback(bufferSize);
		m_bufferSize = bufferSize;
	}
}

void Fuel::OverlayManager::Release()
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	for (auto iterator = m_instances.begin(); iterator != m_instances.end(); ++iterator)
	{
		iterator->second->Release();
	}
}

void Fuel::OverlayManager::Reinitialize()
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	for (auto iterator = m_instances.begin(); iterator != m_instances.end(); ++iterator)
	{
		HRESULT hr = iterator->second->Reinitialize();
		if (FAILED(hr) && m_overlayFailureCallback)
		{
			m_overlayFailureCallback(*(iterator->second));
		}
	}
}

bool Fuel::OverlayManager::GetOverlayAtLocation(int x, int y, OverlayDesc &overlayDesc)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	for (auto reverseIterator = m_instanceRenderOrder.rbegin(); reverseIterator != m_instanceRenderOrder.rend(); ++reverseIterator)
	{
		const RECT &rect = (*reverseIterator)->GetLocation();
		if (x >= rect.left && y >= rect.top &&
			x < rect.right && y < rect.bottom)
		{
			overlayDesc.instanceId = (*reverseIterator)->GetInstanceId();
			overlayDesc.location = rect;
			return true;
		}
	}

	return false;
}

bool Fuel::OverlayManager::GetOverlay(int instanceId, OverlayDesc &overlayDesc)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);
	auto iterator = m_instances.find(instanceId);
	if (iterator != m_instances.end() && iterator->second)
	{
		overlayDesc.instanceId = instanceId;
		overlayDesc.location = iterator->second->GetLocation();
		return true;
	}
	else
	{
		return false;
	}

}

bool Fuel::OverlayManager::GetFirstOverlay(OverlayDesc &overlayDesc)
{
	std::lock_guard<std::mutex> lock(m_instances_mutex);

	auto iterator = m_instances.begin();
	if (iterator == m_instances.end())
	{
		return false;
	}

	overlayDesc.instanceId = iterator->first;
	overlayDesc.location = iterator->second->GetLocation();
	return true;
}

bool Fuel::OverlayManager::GetBufferSize(BufferSize &bufferSize)
{
	if (m_bufferSize.width > 0 && m_bufferSize.height > 0)
	{
		bufferSize = m_bufferSize;
		return true;
	}

	return false;
}

void Fuel::OverlayManager::UpdateRenderOrder()
{
	m_instanceRenderOrder.clear();
	for (auto iterator = m_instances.begin(); iterator != m_instances.end(); ++iterator)
	{
		m_instanceRenderOrder.push_back(iterator->second.get());
	}
	m_instanceRenderOrder.sort(CompareInstanceZIndex);
}

bool Fuel::OverlayManager::CompareInstanceZIndex(const IOverlayInstance* first, const IOverlayInstance* second)
{
	return first->GetZIndex() < second->GetZIndex();
}
