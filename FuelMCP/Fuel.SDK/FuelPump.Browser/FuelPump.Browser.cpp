#include <windows.h>
#include "FuelPump.CefApp.h"


int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	int result(0);
	CefMainArgs main_args(hInstance);
	CefRefPtr<MyApp> app(new MyApp);

	// CefExecuteProcess returns -1 for the host process
	CefExecuteProcess(main_args, app.get(), nullptr);

	return result;
}


