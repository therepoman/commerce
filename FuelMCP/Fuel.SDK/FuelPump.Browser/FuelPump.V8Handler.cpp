#include "FuelPump.V8Handler.h"

CEFV8Handler::CEFV8Handler(CefRefPtr<CefBrowser> browser) :
m_browser(browser)
{}

bool CEFV8Handler::Execute(const CefString& name,
	CefRefPtr<CefV8Value> object,
	const CefV8ValueList& arguments,
	CefRefPtr<CefV8Value>& retval,
	CefString& exception)
{
	if (name.ToString() == "sendCefEvent") {
		if (arguments.size() == 2 && arguments[0]->IsString() && arguments[1]->IsString())
		{
			return SendCefEvent(arguments[0]->GetStringValue(), arguments[1]->GetStringValue());
		} else if (arguments.size() == 2 && arguments[0]->IsString() && arguments[1]->IsNull()) {
			return SendCefEvent(arguments[0]->GetStringValue());
		}
	}
	else if (name.ToString() == "openInBrowser")
	{
		if (arguments.size() == 1 && arguments[0]->IsString())
		{
			return OpenInBrowser(arguments[0]->GetStringValue());
		}
	}

	return false;
}

bool CEFV8Handler::SendCefEvent(std::string eventName) 
{
	return SendCefEvent(eventName, "");
}

bool CEFV8Handler::SendCefEvent(std::string eventName, std::string eventData)
{
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("javascript_cef_event");
	CefRefPtr<CefListValue> args = msg->GetArgumentList();
	args->SetSize(2);
	args->SetString(0, eventName);
	args->SetString(1, eventData);

	return m_browser->SendProcessMessage(PID_BROWSER, msg);
}

bool CEFV8Handler::OpenInBrowser(std::string url)
{
	CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("open_in_browser");
	msg->GetArgumentList()->SetString(0, url);

	return m_browser->SendProcessMessage(PID_BROWSER, msg);
}
