#include "stdafx.h"
#include "gtest/gtest.h"

#include "CMessageDispatcher.h"

using namespace Fuel;

class MockSharedState
{	
	int m_callCount;
public:
	MockSharedState() :
		m_callCount(0)
	{}

	void Increment()
	{
		m_callCount++;
	}

	int GetCallCount() const
	{
		return m_callCount;
	}

	void VerifyCallCount(int expected) const
	{
		EXPECT_EQ(expected, m_callCount);
	}
};

class MockHandler : public IMessageHandler
{
	std::string m_name;
	MockSharedState* m_sharedState;
public:
	MockHandler(const char* name, MockSharedState* sharedState) :
		m_name(name),
		m_sharedState(sharedState)
	{
	}

	void Handle(Fuel::IMessageDispatcher& dispatcher, Message& message)
	{
		m_sharedState->Increment();
	}

	const char* GetName()
	{
		return m_name.c_str();
	}
};


// dispatcher - don't use enum for message types, use int
// dispatcher - have a way to deal with unhandled exceptions and abort messages

TEST(DispatcherTests, RouteToRegisteredHandler)
{
	MockSharedState sharedState;

	std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>> handlers;
	handlers[Fuel::Messaging::Message::Message_FatalError] = std::unique_ptr<MockHandler>(new MockHandler("Mock", &sharedState));

	CMessageDispatcher dispatcher(
		std::unique_ptr<DefaultCallbackHandler>(new DefaultCallbackHandler()),
		handlers);

	dispatcher.HandleMessage(Fuel::Message(
		Fuel::Messaging::Message::Message_FatalError,
		0, new uint8_t[0]));
		
	sharedState.VerifyCallCount(1);
}

TEST(DispatcherTests, RequestResponseSuccessful)
{
	MockSharedState sharedState;

	std::shared_ptr<DefaultCallbackHandler> defaultHandler(new DefaultCallbackHandler());
	std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>> handlers;
	MockHandler mockHandler("Mock", &sharedState);
		
	CMessageDispatcher dispatcher(defaultHandler, handlers);

	Message source(
		Fuel::Messaging::Message::Message_InitializeRequest,
		0, new uint8_t[0]);

	defaultHandler->AddCallback(source.GetRequestId(), 
		[&mockHandler, &dispatcher](Message& message) { mockHandler.Handle(dispatcher, message); });

	dispatcher.HandleMessage(Fuel::Message(source, 
		Fuel::Messaging::Message::Message_InitializeRequest,
		MessagePart(0, new uint8_t[0])));
		
	sharedState.VerifyCallCount(1);
}

TEST(DispatcherTests, RequestResponseRepeat)
{
	MockSharedState sharedState;

	std::shared_ptr<DefaultCallbackHandler> defaultHandler(new DefaultCallbackHandler());
	std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>> handlers;
	MockHandler mockHandler("Mock", &sharedState);
		
	CMessageDispatcher dispatcher(defaultHandler, handlers);

	Message source(
		Fuel::Messaging::Message::Message_InitializeRequest,
		0, new uint8_t[0]);

	defaultHandler->AddCallback(source.GetRequestId(), 
		[&mockHandler, &dispatcher](Message& message) { mockHandler.Handle(dispatcher, message); });

	dispatcher.HandleMessage(Fuel::Message(source, 
		Fuel::Messaging::Message::Message_InitializeRequest,
		MessagePart(0, new uint8_t[0])));

	dispatcher.HandleMessage(Fuel::Message(source, 
		Fuel::Messaging::Message::Message_InitializeRequest,
		MessagePart(0, new uint8_t[0])));
		
	sharedState.VerifyCallCount(1); // make sure only the first message was returned
}

TEST(DispatcherTests, RequestResponseNotFound)
{
	MockSharedState sharedState;

	std::shared_ptr<DefaultCallbackHandler> defaultHandler(new DefaultCallbackHandler());
	std::map<Fuel::Messaging::Message, std::unique_ptr<IMessageHandler>> handlers;
	MockHandler mockHandler("Mock", &sharedState);
		
	CMessageDispatcher dispatcher(defaultHandler, handlers);

	Message source(
		Fuel::Messaging::Message::Message_InitializeRequest,
		0, new uint8_t[0]);

	bool invalidCallbackEnabled = true;
	defaultHandler->AddCallback("INVALID",
		[&mockHandler, &dispatcher, &invalidCallbackEnabled](Message& message) { if (invalidCallbackEnabled) mockHandler.Handle(dispatcher, message); });

	dispatcher.HandleMessage(Fuel::Message(source, 
		Fuel::Messaging::Message::Message_InitializeRequest,
		MessagePart(0, new uint8_t[0])));
		
	sharedState.VerifyCallCount(0);

	// The dispatcher will call the invalid callback in the destructor.
	// Turn it off to avoid crashing the test.
	invalidCallbackEnabled = false;
}

// default handler - fatal error - don't like how this is tied to a specific set of messages

