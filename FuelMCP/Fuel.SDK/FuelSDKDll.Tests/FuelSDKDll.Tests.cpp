// FuelSDKDll.Tests.cpp : Defines the entry point for the unit tests console application.

#include "stdafx.h"

#include <thread>
#include <chrono>
#include <string>
#include <memory>
#include <fstream>

#include "gtest/gtest.h"
#include <windows.h>

#include "../FuelSDKDll.Dispatcher/IMessageDispatcher.h" // changed to avoid a collision with a windows .h file

struct LibraryDeleter
{
	typedef HINSTANCE pointer;

	void operator() (HINSTANCE library)
	{
		if (library != NULL)
		{
			FreeLibrary(library);
		}
	}
};

using unique_library = std::unique_ptr<HINSTANCE, LibraryDeleter>;

unique_library LoadSdkDll()
{
	#ifdef _M_IX86
	return unique_library(LoadLibrary(TEXT("FuelSDK_x86.dll")));
	#elif _M_AMD64
	return unique_library(LoadLibrary(TEXT("FuelSDK_x64.dll")));
	#else
	#error Only supports x86 or x64 builds.
	#endif
}

extern "C"
{
	typedef void (*ResponseCallback)(void* userData, int type, int size, void* data);
}
typedef int(*InitializeFunction)(const char*, const char*, const char*, int);
typedef int(*DestroyFunction)();
typedef int(*SendRequestFunction)(int, int, void*);
typedef int(*SendRequestResponseFunction)(int, int, void*, ResponseCallback, void*);
static void Callback(void* userData, int type, int size, void* data) {}

const static int FIVE_MB = 5 * 1024 * 1024;

TEST(FuelSDKDllTests, Incompatible) 
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	// Checks if the current SDK version is compatible with DLL's version.
	auto result = initializeFunction("VENDOR_ID", "1.0", "NOT_A_VALID_VERSION", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_TRUE(result == (int)Fuel::ResultCodes::INCOMPATIBLE_SDK_VERSION);
}

TEST(FuelSDKDllTests, StartAndStop) 
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0.12.2016", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS
}

TEST(FuelSDKDllTests, InvalidArguments)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult1 = initializeFunction(NULL, "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult1, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT

	auto initResult2 = initializeFunction("VENDOR_ID", NULL, "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult2, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT

	auto initResult3 = initializeFunction("VENDOR_ID", "1.0", NULL, (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult3, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT
}

TEST(FuelSDKDllTests, InvalidRendererType)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", -100);
	ASSERT_EQ(initResult, (int)Fuel::ResultCodes::INVALID_RENDERER); // INVALID_RENDERER
}

TEST(FuelSDKDllTests, InitializeTwice) 
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS
	
	auto secondInitResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(secondInitResult, (int)Fuel::ResultCodes::ALREADY_INITIALIZED); // ALREADY_INITIALIZED

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS
}

TEST(FuelSDKDllTests, SendRequest_NotInitialized)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto sendRequestFunction = (SendRequestFunction)GetProcAddress(hInstanceLib.get(), "SendRequest");
	ASSERT_FALSE(sendRequestFunction == NULL);

	auto result = sendRequestFunction(0, 0, "Data");
	ASSERT_EQ(result, (int)Fuel::ResultCodes::NOT_INITIALIZED); // NOT_INITIALIZED
}

TEST(FuelSDKDllTests, SendRequest_InvalidArguments)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS

	auto sendRequestFunction = (SendRequestFunction)GetProcAddress(hInstanceLib.get(), "SendRequest");
	ASSERT_FALSE(sendRequestFunction == NULL);

	auto result = sendRequestFunction(0, 0, nullptr);
	ASSERT_EQ(result, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS
}

TEST(FuelSDKDllTests, SendRequest_RequestTooLarge)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS

	auto sendRequestFunction = (SendRequestFunction)GetProcAddress(hInstanceLib.get(), "SendRequest");
	ASSERT_FALSE(sendRequestFunction == NULL);

	auto result = sendRequestFunction(0, FIVE_MB, "Data");
	ASSERT_EQ(result, (int)Fuel::ResultCodes::REQUEST_TOO_LARGE); // REQUEST_TOO_LARGE

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS
}

TEST(FuelSDKDllTests, SendRequestResponse_NotInitialized)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto sendRequestResponseFunction = (SendRequestResponseFunction)GetProcAddress(hInstanceLib.get(), "SendRequestResponse");
	ASSERT_FALSE(sendRequestResponseFunction == NULL);

	auto result = sendRequestResponseFunction(0, 0, "Data", Callback, "User Data");
	ASSERT_EQ(result, (int)Fuel::ResultCodes::NOT_INITIALIZED); // NOT_INITIALIZED
}

TEST(FuelSDKDllTests, SendRequestResponse_InvalidArguments)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS

	auto sendRequestResponseFunction = (SendRequestResponseFunction)GetProcAddress(hInstanceLib.get(), "SendRequestResponse");
	ASSERT_FALSE(sendRequestResponseFunction == NULL);

	auto result1 = sendRequestResponseFunction(0, 0, nullptr, Callback, "User Data");
	ASSERT_EQ(result1, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT

	auto result2 = sendRequestResponseFunction(0, 0, "Data", nullptr, "User Data");
	ASSERT_EQ(result2, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT

	auto result3 = sendRequestResponseFunction(0, 0, "Data", Callback, nullptr);
	ASSERT_EQ(result3, (int)Fuel::ResultCodes::INVALID_ARGUMENT); // INVALID_ARGUMENT

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS
}

TEST(FuelSDKDllTests, SendRequestResponse_RequestTooLarge)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS

	auto sendRequestResponseFunction = (SendRequestResponseFunction)GetProcAddress(hInstanceLib.get(), "SendRequestResponse");
	ASSERT_FALSE(sendRequestResponseFunction == NULL);

	auto result = sendRequestResponseFunction(0, FIVE_MB, "Data", Callback, "User Data");
	ASSERT_EQ(result, (int)Fuel::ResultCodes::REQUEST_TOO_LARGE); // REQUEST_TOO_LARGE

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS
}

TEST(FuelSDKDllTests, Destroy_NotInitialized)
{
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto result = destroyFunction();
	ASSERT_EQ(result, (int)Fuel::ResultCodes::NOT_INITIALIZED); // NOT_INITIALIZED
}

TEST(FuelSDKDllTests, FileHandleInheritance)
{
	// Open a file handle and create the file.
	std::ofstream testFile;
	testFile.open("test.txt");
	testFile << "Test";

	// Initialize the SDK.
	auto hInstanceLib = LoadSdkDll();
	ASSERT_FALSE(hInstanceLib.get() == NULL);

	auto initializeFunction = (InitializeFunction)GetProcAddress(hInstanceLib.get(), "Initialize");
	ASSERT_FALSE(initializeFunction == NULL);

	auto initResult = initializeFunction("VENDOR_ID", "1.0", "1.0", (int)Fuel::RendererType::DIRECT_X_11);
	ASSERT_EQ(initResult, 0); // SUCCESS

	// Close the handle and do something that will fail if the file handle was inherited and not closed by the FuelPump.
	testFile.close();
	ASSERT_FALSE(std::rename("test.txt", "test2.txt"));

	// Cleanup
	auto destroyFunction = (DestroyFunction)GetProcAddress(hInstanceLib.get(), "Destroy");
	ASSERT_FALSE(destroyFunction == NULL);

	auto destroyResult = destroyFunction();
	ASSERT_EQ(destroyResult, 0); // SUCCESS

	std::remove("test2.txt");
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	return 0;
}
