#include "stdafx.h"
#include "gtest/gtest.h"

#include "MockTransport.h"

#include "IMessageDispatcher.h"
#include "CMessageDispatcher.h"

#include "MessagePump.h"

#include "Fuel_generated.h"

#include <future>
#include <chrono>

using namespace Fuel;

class CountRestartStrategy : public Fuel::IRestartStrategy
{
	int m_restartCount;
	int m_maxRestartCount;
public:
	CountRestartStrategy(int maxRestartCount) :
		m_maxRestartCount(maxRestartCount),
		m_restartCount(0)
	{}

	int InitializeTimeoutMilliseconds() override
	{
		return 500;
	}

	virtual void TransportReset() override 
	{
		m_restartCount++;
	}

	virtual bool ShouldRestart() const override 
	{
		return m_restartCount < m_maxRestartCount;
	}
};

class TestApplication : public MessagePump
{
	MockTransport* m_transport;
	std::map<Fuel::Messaging::Message, std::unique_ptr<Fuel::IMessageHandler>> m_handlers;
	int m_resetCount;

public:
	std::shared_ptr<Fuel::DefaultCallbackHandler> DefaultCallbackHandler;
	Fuel::CMessageDispatcher InternalDispatcher;

	TestApplication(int retryCount, MockTransport* transport) :
		m_transport(transport),
		m_resetCount(0),
		DefaultCallbackHandler(new Fuel::DefaultCallbackHandler()),
		InternalDispatcher(DefaultCallbackHandler, m_handlers),
		MessagePump(std::unique_ptr<Fuel::IRestartStrategy>(new CountRestartStrategy(retryCount)), "VendorId", "1.0", "1.0")
	{
		Start();
	}

	~TestApplication()
	{
		Stop();
	}

	int GetResetCount() const
	{
		return m_resetCount;
	}

	std::unique_ptr<Fuel::IMessageTransport> CreateTransport() override
	{
		return m_transport->CreateTransport();
	}

	Fuel::IMessageDispatcher& Dispatcher()
	{
		return InternalDispatcher;
	}

	void TransportReset(const Fuel::MessageTransportFatalError& error)
	{
		m_resetCount++;
	}
};

Fuel::Message CreateMessage()
{
	return Fuel::Message(100, 4, new uint8_t[4] { 1, 2, 3, 4 });
}

Fuel::Message CreateInitializeResponse()
{
	return Fuel::Message((int)Fuel::Messaging::Message::Message_InitializeResponse, 0, new uint8_t[0]);
}

Fuel::Message CreateFatalResponse()
{
	return Fuel::Message((int)Fuel::Messaging::Message::Message_FatalError, 0, new uint8_t[0]);
}

TEST(MessagePumpTests, SendMessage)
{
	MockTransport transport;
	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);

		application.SendRequest(CreateMessage());
	}

	EXPECT_EQ(3, transport.GetReceivedMessages().size()); // init, sent, shutdown
	EXPECT_EQ(100, transport.GetReceivedMessage(1).GetType());
}

TEST(MessagePumpTests, SendMessage_FatalError)
{
	MockTransport transport;
	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);

		transport.FailOnNextWrite();
		ASSERT_THROW(application.SendRequest(CreateMessage()), Fuel::MessageTransportFatalError);
	}
}

TEST(MessagePumpTests, SendRequestResponse)
{
	MockTransport transport;
	std::promise<int> messageType;
	auto future = messageType.get_future();

	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);

		auto message = CreateMessage();
		application.SendRequestResponse(message, [&messageType](int type, uint32_t size, const uint8_t* data) {
			messageType.set_value(type);
		});

		transport.AddMessageToSend(std::move(message));

		// wait a little while for the polling thread to process the message
		auto result = future.wait_for(std::chrono::seconds(1));
		EXPECT_EQ(std::future_status::ready, result);
	}

	EXPECT_EQ(100, future.get());
}

TEST(MessagePumpTests, SendRequestResponse_FatalError)
{
	MockTransport transport;

	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);
		transport.FailOnNextWrite();
		ASSERT_THROW(application.SendRequestResponse(CreateMessage(), [](int type, uint32_t size, const uint8_t* data) {}), Fuel::MessageTransportFatalError);
	}
}

TEST(MessagePumpTests, StopAndVerifyInitAndShutdownMessage)
{
	MockTransport transport;
	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);
	}

	EXPECT_EQ(2, transport.GetReceivedMessages().size()); // init, send
	EXPECT_EQ((int)Fuel::Messaging::Message::Message_InitializeRequest, transport.GetReceivedMessage(0).GetType());
	EXPECT_EQ((int)Fuel::Messaging::Message::Message_ShutdownRequest, transport.GetReceivedMessage(1).GetType());
}

TEST(MessagePumpTests, SendRequestResponseAborted)
{
	MockTransport transport;
	std::promise<int> messageType;
	auto future = messageType.get_future();

	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);

		application.SendRequestResponse(CreateMessage(), [&messageType](int type, uint32_t size, const uint8_t* data) {
			messageType.set_value(type);
		});
	}

	EXPECT_EQ(std::future_status::ready, future.wait_for(std::chrono::seconds(1)));
	EXPECT_EQ((int)Fuel::Messaging::Message::Message_FatalError, future.get());
}

TEST(MessagePumpTests, ReceiveFailure)
{
	MockTransport transport;

	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(1, &transport);

		transport.FailOnNextRead();

		// give the polling thread time to set the reset count
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		EXPECT_EQ(1, application.GetResetCount());
	}
}

TEST(MessagePumpTests, ReceiveFailureWithRecovery)
{
	MockTransport transport;
	std::promise<int> messageType;
	auto future = messageType.get_future();

	{
		transport.AddMessageToSend(CreateInitializeResponse());
		TestApplication application(2, &transport);

		transport.FailOnNextRead();

		// give the polling thread time to fail
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		auto message = CreateMessage();
		transport.AddMessageToSend(CreateInitializeResponse()); // add init response for the restart
		application.SendRequestResponse(message, [&messageType](int type, uint32_t size, const uint8_t* data) {
			messageType.set_value(type);
		});

		transport.AddMessageToSend(std::move(message));
		EXPECT_EQ(std::future_status::ready, future.wait_for(std::chrono::seconds(1)));
	}

	EXPECT_EQ(2, transport.GetCreatedCount()); // verify two instances of the delegating transport were created
	EXPECT_EQ(100, future.get()); // verify the message response type
}

TEST(MessagePumpTests, FailTooManyTimes)
{
	MockTransport transport;

	try
	{
		transport.AddMessageToSend(CreateFatalResponse());
		transport.AddMessageToSend(CreateFatalResponse());
		TestApplication application(2, &transport);

		FAIL();
	}
	catch (Fuel::MessageTransportFatalError err)
	{
		EXPECT_EQ(2, transport.GetCreatedCount());
	}
}

TEST(MessagePumpTests, InitTimeout)
{
	MockTransport transport;

	try
	{
		TestApplication application(1, &transport);

		FAIL();
	}
	catch (Fuel::MessageTransportFatalError err)
	{
		SUCCEED();
	}
}

