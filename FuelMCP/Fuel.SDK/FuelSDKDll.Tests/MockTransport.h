#pragma once

#include <list>
#include <mutex>
#include <queue>
#include <mutex>
#include <condition_variable>

#include "IMessageTransport.h"
#include "Message.h"

class DelegatingTransport : public Fuel::IMessageTransport
{ 
	Fuel::IMessageTransport* m_transport;
public:
	DelegatingTransport(Fuel::IMessageTransport* transport) :
		m_transport(transport)
	{
	}

	void AbortRead() const override { m_transport->AbortRead(); }
	Fuel::Message ReadMessage() const override { return m_transport->ReadMessage(); }
	void WriteMessage(const Fuel::Message& m) const override { m_transport->WriteMessage(m); }
};

class MockTransport : public Fuel::IMessageTransport
{
	mutable std::condition_variable m_notEmpty;
	mutable std::mutex data_mutex;
	mutable std::deque<Fuel::Message> m_messagesToSend;
	mutable std::list<Fuel::Message> m_receivedMessages;

	mutable bool m_failOnNextWrite;
	mutable bool m_failOnNextRead;

	int m_createdCount;

public:
	MockTransport() :
		m_failOnNextWrite(false),
		m_failOnNextRead(false)
	{
	}

	std::unique_ptr<Fuel::IMessageTransport> CreateTransport()
	{
		m_createdCount++;

		return std::unique_ptr<Fuel::IMessageTransport>(new DelegatingTransport(this));
	}

	void FailOnNextWrite()
	{
		m_failOnNextWrite = true;
	}

	void FailOnNextRead()
	{
		std::unique_lock<std::mutex> lock(data_mutex);
		m_failOnNextRead = true;
		m_notEmpty.notify_one();
	}

	const std::list<Fuel::Message>& GetReceivedMessages() const
	{
		return m_receivedMessages;
	}

	const Fuel::Message& GetReceivedMessage(int index) const
	{
		auto it = m_receivedMessages.begin();
		std::advance(it, index);
		return *it;
	}

	int GetCreatedCount()
	{
		return m_createdCount;
	}

	void AddMessageToSend(Fuel::Message&& message)
	{
		std::unique_lock<std::mutex> lock(data_mutex);
		m_messagesToSend.emplace_back(std::move(message));
		m_notEmpty.notify_one();
	}

	void AbortRead() const override
	{
		// enqueue empty message
		std::unique_lock<std::mutex> lock(data_mutex);
		m_messagesToSend.emplace_back(
			Fuel::Message(
				Fuel::MessagePart(), 
				Fuel::MessagePart()));
		m_notEmpty.notify_one();
	}

	Fuel::Message ReadMessage() const override
	{
		std::unique_lock<std::mutex> lock(data_mutex);

		if (m_messagesToSend.empty() || !m_failOnNextRead) {
			m_notEmpty.wait(lock, [this]() { return !m_messagesToSend.empty() || m_failOnNextRead; });
		}

		if (m_failOnNextRead) 
		{
			m_failOnNextRead = false;
			throw Fuel::MessageTransportFatalError();
		}

		Fuel::Message& m = m_messagesToSend.front();

		// copy header
		auto headerSize = m.GetHeader().GetSize();
		auto headerData = new uint8_t[headerSize];
		memcpy(headerData, m.GetHeader().GetBuffer(), headerSize);

		// copy body
		auto bodySize = m.GetBody().GetSize();
		auto bodyData = new uint8_t[bodySize];
		memcpy(bodyData, m.GetBody().GetBuffer(), bodySize);

		m_messagesToSend.pop_front();
		return Fuel::Message(
			Fuel::MessagePart(headerSize, headerData),
			Fuel::MessagePart(bodySize, bodyData));
	}

	void WriteMessage(const Fuel::Message& m) const override
	{
		if (m_failOnNextWrite)
		{
			m_failOnNextWrite = false;
			throw Fuel::MessageTransportFatalError();
		}

		// copy header
		auto headerSize = m.GetHeader().GetSize();
		auto headerData = new uint8_t[headerSize];
		memcpy(headerData, m.GetHeader().GetBuffer(), headerSize);

		// copy body
		auto bodySize = m.GetBody().GetSize();
		auto bodyData = new uint8_t[bodySize];
		memcpy(bodyData, m.GetBody().GetBuffer(), bodySize);

		m_receivedMessages.emplace_back(Fuel::Message(
			Fuel::MessagePart(headerSize, headerData),
			Fuel::MessagePart(bodySize, bodyData)));
	}
};

