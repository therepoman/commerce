#include "stdafx.h"
#include "gtest/gtest.h"

#include <iostream>
#include <fstream>
#include "Logger.h"

TEST(LoggerTests, UseLoggerBeforeInit)
{
	Fuel::Logger::Cleanup();

	// These should not cause problems, but will have no effect.
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->warn("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Test");
}

TEST(LoggerTests, UseLoggerAfterInit)
{
	Fuel::Logger::Cleanup();
	Fuel::Logger::Initialize();

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->warn("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Test");

	Fuel::Logger::Cleanup();
}


TEST(LoggerTests, UseInvalidLoggerName)
{
	Fuel::Logger::Cleanup();
	Fuel::Logger::Initialize();

	Fuel::Logger::Instance("InvalidLoggerName")->debug("Test");
	Fuel::Logger::Instance("InvalidLoggerName")->info("Test");
	Fuel::Logger::Instance("InvalidLoggerName")->warn("Test");
	Fuel::Logger::Instance("InvalidLoggerName")->error("Test");
	Fuel::Logger::Instance("InvalidLoggerName")->critical("Test");

	Fuel::Logger::Cleanup();
}

TEST(LoggerTests, DoubleInit)
{
	Fuel::Logger::Cleanup();
	Fuel::Logger::Initialize();
	Fuel::Logger::Initialize();

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->warn("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Test");

	Fuel::Logger::Cleanup();
}

TEST(LoggerTests, UnableToOpenFile)
{
	Fuel::Logger::Cleanup();

	// Open the file to prevent the logger from doing so.
	std::string filename = FUEL_DLL_FILENAME + "." + FUEL_DLL_FILE_EXTENSION;
	std::ofstream logfile;
	logfile.open(filename);

	// These should not cause problems, but will have no effect.
	Fuel::Logger::Initialize();
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->warn("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Test");
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Test");
	Fuel::Logger::Cleanup();

	logfile.close();
}
