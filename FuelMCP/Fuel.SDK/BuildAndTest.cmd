@echo off

echo Building solution in Debug x86 configuration...
msbuild Fuel.sln -t:Rebuild -p:Configuration=Debug -p:Platform="x86" > build_debug_x86.log
if %ERRORLEVEL% NEQ 0 (
	echo Building solution failed
	exit /b %ERRORLEVEL%
)

echo Building solution in Debug x64 configuration...
msbuild Fuel.sln -t:Rebuild -p:Configuration=Debug -p:Platform="x64" > build_debug_x64.log
if %ERRORLEVEL% NEQ 0 (
	echo Building solution failed
	exit /b %ERRORLEVEL%
)

echo Building solution in Release x86 configuration...
msbuild Fuel.sln -t:Rebuild -p:Configuration=Release -p:Platform="x86" > build_release_x86.log
if %ERRORLEVEL% NEQ 0 (
	echo Building solution failed
	exit /b %ERRORLEVEL%
)

echo Building solution in Release x64 configuration...
msbuild Fuel.sln -t:Rebuild -p:Configuration=Release -p:Platform="x64" > build_release_x64.log
if %ERRORLEVEL% NEQ 0 (
	echo Building solution failed
	exit /b %ERRORLEVEL%
)

echo Running FuelPump.Tests tests...
mstest /testcontainer:FuelPump.Tests/bin/Debug/FuelPump.Tests.dll /noresults /nologo > build_debug_fuelpump_tests.log
if %ERRORLEVEL% NEQ 0 (
	echo Running FuelPump.Tests tests failed
	exit /b %ERRORLEVEL%
)

echo Running FuelSDKDLL.Tests tests...
bin\Debug\Win32\fuel\FuelSDKDLL.Tests.exe > build_debug_fuelsdkdll_tests.log
if %ERRORLEVEL% NEQ 0 (
	echo Running FuelSDKDLL.Tests tests failed
	exit /b %ERRORLEVEL%
)

echo Running FuelPump.Tests tests...
mstest /testcontainer:FuelPump.Tests/bin/Release/FuelPump.Tests.dll /noresults /nologo > build_release_fuelpump_tests.log
if %ERRORLEVEL% NEQ 0 (
	echo Running FuelPump.Tests tests failed
	exit /b %ERRORLEVEL%
)

echo Running FuelSDKDLL.Tests tests...
bin\Release\Win32\fuel\FuelSDKDLL.Tests.exe > build_release_fuelsdkdll_tests.log
if %ERRORLEVEL% NEQ 0 (
	echo Running FuelSDKDLL.Tests tests failed
	exit /b %ERRORLEVEL%
)

echo.
echo Safe to commit