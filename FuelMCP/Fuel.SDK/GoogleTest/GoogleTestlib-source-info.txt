GoogleTest.lib was compiled from the Googletest brazil package, branch Googletest-1.7.0.
https://code.amazon.com/packages/Googletest/commits/1de2f608ff034f9a7a1a3887847a83229bf8f8f6

This was done by creating a visual studio library project with no precompiled headers, adding gtest-all.cc, setting the include path to googletest\include;googletest and building it.
