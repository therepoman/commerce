#include "MutexTryLock.h"

Fuel::MutexTryLock::MutexTryLock(std::mutex &mutex)
{
	m_mutex = &mutex;
	m_locked = m_mutex->try_lock();
}

Fuel::MutexTryLock::~MutexTryLock()
{
	if (m_locked)
	{
		m_mutex->unlock();
	}
}

bool Fuel::MutexTryLock::isLocked()
{
	return m_locked;
}
