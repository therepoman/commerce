#include <fstream>
#include <memory>

#include "logger.h"

using namespace Fuel;

bool Logger::s_initialized = false;
std::map<std::string, std::shared_ptr<Logger>> Logger::s_loggers;
std::mutex Logger::s_mutex;

Logger::Logger(const std::string &loggerName) : m_loggerName(loggerName)
{
}

std::shared_ptr<Logger> Logger::Instance(const std::string &loggerName)
{
	std::lock_guard<std::mutex> lock(s_mutex);

	auto logger = s_loggers.find(loggerName);
	if (logger != s_loggers.end())
	{
		return logger->second;
	}

	logger = s_loggers.find(FUEL_DLL_LOGGER);
	if (logger != s_loggers.end())
	{
		logger->second->warn("No logger found for '{0}'", loggerName);
		return logger->second;
	}
	
	std::shared_ptr<Logger> stubLogger(new Logger(FUEL_DLL_LOGGER));
	return stubLogger;
}

void Logger::registerLogger(const std::string &loggerName, std::shared_ptr<timespan_file_sink> sink, spdlog::level::level_enum level)
{
	std::vector<spdlog::sink_ptr> sinks;
	sinks.push_back(sink);

#ifdef _DEBUG
	// Also log to debug window.
	std::shared_ptr<DebugSink> debugSink = std::make_shared<DebugSink>();
	sinks.push_back(std::make_shared<DebugSink>());
#endif

	auto logger = std::make_shared<spdlog::logger>(loggerName, begin(sinks), end(sinks));
	spdlog::register_logger(logger);
	logger->set_level(level);
	std::unique_ptr<Logger> loggerInstance(new Logger(loggerName));
	s_loggers[loggerName] = std::move(loggerInstance);
}

void Logger::Initialize()
{
	std::lock_guard<std::mutex> lock(s_mutex);
	if (!s_initialized)
	{
		// Create a timespan file sink with 30min rotation time
		std::shared_ptr<timespan_file_sink> sink;
		try
		{
			wchar_t buf[MAX_PATH];
			ExpandEnvironmentStrings(FUEL_DLL_LOG_FOLDER.c_str(), buf, MAX_PATH);
			CreateDirectory(buf, NULL);

			std::wstring wBuf(buf);
			std::string logFolder(wBuf.begin(), wBuf.end());

			sink = std::make_shared<timespan_file_sink>(logFolder + FUEL_DLL_FILENAME, FUEL_DLL_FILE_EXTENSION, 30, true);
		}
		catch (spdlog::spdlog_ex e)
		{
			handleLoggerError("Unable to initialize logger", e);
			return;
		}

		// Create multiple loggers for more control over logging levels.
#ifdef _DEBUG
		registerLogger(FUEL_DLL_LOGGER, sink, spdlog::level::debug);
		registerLogger(FUEL_MESSAGING_LOGGER, sink, spdlog::level::debug);
#else
		registerLogger(FUEL_DLL_LOGGER, sink, spdlog::level::info);
		registerLogger(FUEL_MESSAGING_LOGGER, sink, spdlog::level::info);
#endif

		spdlog::set_async_mode(512);
		spdlog::set_pattern("%C%m%d:%H%M%S Fuel SDK: %L DLL:%n::%v");

		s_initialized = true;
	}
}

void Logger::Cleanup() {
	std::lock_guard<std::mutex> lock(s_mutex);
	s_loggers.clear();
	spdlog::drop_all();
	s_initialized = false;
}

void Logger::handleLoggerError(const char* message, spdlog::spdlog_ex e)
{
#ifdef _DEBUG
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> utf8_16_converter;
	std::string logMessage = message;
	logMessage += ": ";
	logMessage += e.what();
	logMessage += "\r\n";
	std::wstring wmessage = utf8_16_converter.from_bytes(logMessage);
	OutputDebugString(wmessage.c_str());
#endif
}
