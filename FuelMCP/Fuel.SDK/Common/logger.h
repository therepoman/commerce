#pragma once

#include "spdlog/spdlog.h"
#include <codecvt>
#include <algorithm>

const std::wstring FUEL_DLL_LOG_FOLDER(L"%appdata%\\Twitch\\Fuel\\SdkLogs");
const std::string FUEL_DLL_FILENAME("\\FuelDll");
const std::string FUEL_DLL_FILE_EXTENSION("log");
const std::string FUEL_DLL_LOGGER("fuel");
const std::string FUEL_MESSAGING_LOGGER("messaging");

namespace Fuel {

	/*
	* Rotating file sink based on time. rotates every 30 mins
	*/
	class timespan_file_sink :public spdlog::sinks::base_sink < std::mutex >
	{
	public:
		//create timespan file sink which rotates on given timespan
		timespan_file_sink(
			const std::string& base_filename,
			const std::string& extension,
			int timespan_mins,
			bool force_flush = false) : _base_filename(base_filename),
			_extension(extension),
			_timespan_m(timespan_mins),
			_file_helper(force_flush)
		{
			if (timespan_mins < 0)
				_timespan_m = 30;
			_rotation_tp = std::chrono::system_clock::now();
			_file_helper.open(_base_filename + "." + _extension);
		}

		void flush() override
		{
			_file_helper.flush();
		}

	protected:
		void _sink_it(const spdlog::details::log_msg& msg) override
		{
			if (std::chrono::system_clock::now() >= _rotation_tp)
			{
				_file_helper.close();
				std::rename(_file_helper.filename().c_str(), calc_filename(_base_filename, _extension).c_str());
				_file_helper.open(_base_filename + "." + _extension);

				_rotation_tp = _next_rotation_tp();
			}
			_file_helper.write(msg);
		}

	private:
		std::chrono::system_clock::time_point _next_rotation_tp()
		{
			return std::chrono::system_clock::now() + std::chrono::minutes(_timespan_m);
		}

		//Create filename for the form basename.YYYY-MM-DD_HH-mm.extension
		static std::string calc_filename(const std::string& basename, const std::string& extension)
		{
			std::tm tm = spdlog::details::os::localtime();
			fmt::MemoryWriter w;
			w.write("{}_{:04d}-{:02d}-{:02d}_{:02d}-{:02d}.{}", basename, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, extension);
			return w.str();
		}

		std::string _base_filename;
		std::string _extension;
		int _timespan_m;
		std::chrono::system_clock::time_point _rotation_tp;
		spdlog::details::file_helper _file_helper;
	};

	class Logger {
	public:
		static std::shared_ptr<Logger> Instance(const std::string &loggerName);
		static void Initialize();
		static void Cleanup();

		// These function needs to remain in the .h so that the linker can properly find the instantiated template functions.
		/**
		   Lowest logging level. Only seen in Debug builds.
		*/
		template<typename ... Args> void debug(const char* fmt, const Args&... arguments) {
			log(spdlog::level::debug, fmt, arguments...);
		}

		/**
		   2nd lowest priority logging level. Will be seen in debug and possibly 3P developer debug builds.
		*/
		template<typename ... Args> void info(const char* fmt, const Args&... arguments) {
			log(spdlog::level::info, fmt, arguments...);
		}

		/**
		   Middle priority logging level. Will be seen in debug and possibly 3P developer debug builds.
		*/
		template<typename ... Args> void warn(const char* fmt, const Args&... arguments) {
			log(spdlog::level::warn, fmt, arguments...);
		}

		/**
		   2nd highest priority logging level. Will be seen in every build.
		*/
		template<typename ... Args> void error(const char* fmt, const Args&... arguments) {
			log(spdlog::level::err, fmt, arguments...);
		}

		/**
		   Highest priority logging level. Will be seen in every build.
		*/
		template<typename ... Args> void critical(const char* fmt, const Args&... arguments) {
			log(spdlog::level::critical, fmt, arguments...);
		}

	private:

		template<typename ... Args> void log(spdlog::level::level_enum level, const char* fmt, const Args&... arguments)
		{
			try
			{
				std::string colonlessFmt(fmt);
				std::replace(colonlessFmt.begin(), colonlessFmt.end(), ':', ';');

				auto logger = spdlog::get(m_loggerName);
				if (logger)
				{
					switch (level)
					{
					case spdlog::level::debug:    logger->debug(colonlessFmt.c_str(), arguments...);    break;
					case spdlog::level::info:     logger->info(colonlessFmt.c_str(), arguments...);     break;
					case spdlog::level::warn:     logger->warn(colonlessFmt.c_str(), arguments...);     break;
					case spdlog::level::err:      logger->error(colonlessFmt.c_str(), arguments...);    break;
					case spdlog::level::critical: logger->critical(colonlessFmt.c_str(), arguments...); break;
					default: logger->error("Unknown log level {0}", level); break;
					}
				}
#ifdef _DEBUG
				else
				{
					OutputDebugString(L"No logger returned from spdlog.  Make sure to initialize Fuel::Logger before using it.\r\n");
				}
#endif
			}
			catch (spdlog::spdlog_ex e)
			{
				handleLoggerError("Logger error", e);
			}
		}
		
		Logger(const std::string &loggerName);
		static void handleLoggerError(const char* message, spdlog::spdlog_ex e);
		static void registerLogger(const std::string &loggerName, std::shared_ptr<timespan_file_sink> sink, spdlog::level::level_enum level);
		static std::mutex s_mutex;
		static bool s_initialized;
		static std::map<std::string, std::shared_ptr<Logger>> s_loggers;
		const std::string m_loggerName;
	};

	// spdlog sink that outputs to the debug window.
	class DebugSink : public spdlog::sinks::base_sink<std::mutex>
	{
	public:
		DebugSink() {}
	protected:
		void _sink_it(const spdlog::details::log_msg& msg) override
		{
			std::string message;
			message.append(msg.formatted.data(), msg.formatted.size());
			std::wstring wmessage = m_utf8_16_converter.from_bytes(message);
			OutputDebugString(wmessage.c_str());
		}

		void flush() override {}
	private:
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> m_utf8_16_converter;
	};
}
