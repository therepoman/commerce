#pragma once
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>

#include <SDKDDKVer.h>

#define FUELDLL_API extern "C" __declspec(dllexport)

extern "C"
{
	typedef void (*ResponseCallback)(void* userData, int type, int size, void* data);
}
FUELDLL_API int Initialize(const char* vendorId, const char* gameVersion, const char* sdkVersion, int renderType);
FUELDLL_API int Destroy();

FUELDLL_API int SendRequest(int type, int size, void* data);
FUELDLL_API int SendRequestResponse(int type, int size, void* data, ResponseCallback callback, void* userData);

