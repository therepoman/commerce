// FuelSDKDll.cpp : Defines the exported functions for the DLL application.
#include "FuelDll.h"
#include "Logger.h"
#include <vector>

#include "Application.h"

#include "MessageTransport.h"

std::unique_ptr<Fuel::Application> g_application;
std::mutex g_dllLock;

// A vector of all SDK versions this DLL is compatible with
const std::vector<std::string> compatibleSDKVersions = { "1.0" };

// Used for debugging purposes
const std::string DLL_VERSION = "1.0";

// 5MB max data size
const static int MAX_SIZE = 5 * 1024 * 1024;

int ValidateVersion(const std::string& sdkVersion)
{
	// Remove revision and build number from version
	std::string version = sdkVersion.substr(0, sdkVersion.find('.', sdkVersion.find('.') + 1));

	for (unsigned int i = 0; i < compatibleSDKVersions.size(); i++)
	{
		if (compatibleSDKVersions[i].compare(version) == 0)
		{
			return 0;
		}
	}

	return (int)Fuel::ResultCodes::INCOMPATIBLE_SDK_VERSION;
}

int ValidateRenderType(Fuel::RendererType renderType)
{
	switch (renderType)
	{
	case Fuel::RendererType::DIRECT_X_11:
	case Fuel::RendererType::DIRECT_X_12:
		return 0;
	default:
		return (int)Fuel::ResultCodes::INVALID_RENDERER;
	}
}

FUELDLL_API int Initialize(const char* vendorId, const char* gameVersion, const char* sdkVersion, int renderType)
{
	Fuel::Logger::Initialize();

	// Check the input for NULLs
	if (gameVersion == nullptr || sdkVersion == nullptr || vendorId == nullptr)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Initialization failed: Invalid input");
		return (int)Fuel::ResultCodes::INVALID_ARGUMENT;
	}

	// Validate renderType
	auto check = ValidateRenderType((Fuel::RendererType)renderType);
	if (check != 0)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Initialization failed: Invalid renderer type");
		return check;
	}

	// Verify the version
	auto result = ValidateVersion(sdkVersion);
	if (result != 0)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Initialization failed: Incompatible SDK version {0}", sdkVersion);
		return result;
	}

	std::lock_guard<std::mutex> lock(g_dllLock);

	if (g_application) 
	{
		return (int)Fuel::ResultCodes::ALREADY_INITIALIZED;
	}

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Initializing SDK");

	try
	{
		g_application = std::make_unique<Fuel::Application>((Fuel::RendererType)renderType, vendorId, gameVersion, sdkVersion);
	}
	catch (Fuel::PipeCreationFailedException ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Initialization failed: PipeCreationFailedException");
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}
	catch (Fuel::SetHandleInfoFailedException ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Initialization failed: SetHandleInfoFailedException");
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}
	catch (Fuel::MessageTransportFatalError ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Initialization failed: MessageTransportFatalError");
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}
	catch (Fuel::DirectXHookCreationFailed ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Failed to initialize the direct x overlay");
		return (int)Fuel::ResultCodes::HOOK_FAILED;
	}
	catch (std::runtime_error ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("Unknown exception during initialize");
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}

	return (int)Fuel::ResultCodes::SUCCESS;
}

FUELDLL_API int Destroy()
{
	std::lock_guard<std::mutex> lock(g_dllLock);

	if (!g_application) 
	{
		return (int)Fuel::ResultCodes::NOT_INITIALIZED;
	}

	g_application.reset();

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->info("Shutting down Dll");
	Fuel::Logger::Cleanup();
	return 0;
}

FUELDLL_API int SendRequest(int type, int size, void* data)
{
	if (data == nullptr)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("SendRequest failed: Invalid input");
		return (int)Fuel::ResultCodes::INVALID_ARGUMENT;
	}

	if (size >= MAX_SIZE)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("SendRequest failed: Request size too large");
		return (int)Fuel::ResultCodes::REQUEST_TOO_LARGE;
	}

	std::lock_guard<std::mutex> lock(g_dllLock);

	if (!g_application) 
	{
		return (int)Fuel::ResultCodes::NOT_INITIALIZED;
	}

	try {
		uint8_t* buffer = new uint8_t[size];
		memcpy(buffer, data, size);

		g_application->SendRequest(
			Fuel::Message(type, (uint32_t)size, (uint8_t*)buffer));
	}
	catch (Fuel::Message::HeaderCreationException) {
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}
	catch (Fuel::MessageTransportFatalError ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("SendRequest failed: MessageTransportFatalError");
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}

	return (int)Fuel::ResultCodes::SUCCESS;
}

FUELDLL_API int SendRequestResponse(int type, int size, void* data, ResponseCallback callback, void* userData)
{
	if (data == nullptr || userData == nullptr || callback == nullptr)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("SendRequestResponse failed: Invalid input");
		return (int)Fuel::ResultCodes::INVALID_ARGUMENT;
	}

	if (size >= MAX_SIZE)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("SendRequestResponse failed: Request size too large");
		return (int)Fuel::ResultCodes::REQUEST_TOO_LARGE;
	}

	std::lock_guard<std::mutex> lock(g_dllLock);
	if (!g_application) 
	{
		return (int)Fuel::ResultCodes::NOT_INITIALIZED;
	}

	try {
		uint8_t* buffer = new uint8_t[size];
		memcpy(buffer, data, size);

		g_application->SendRequestResponse(
			Fuel::Message(type, (uint32_t)size, (uint8_t*)buffer),
			[userData, callback](int type, uint32_t size, const uint8_t* buffer) {
				callback(userData, type, (int)size, (void*)buffer);
			});
	}
	catch (Fuel::Message::HeaderCreationException) {
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}
	catch (Fuel::MessageTransportFatalError ex)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->critical("SendRequestResponse failed: MessageTransportFatalError");
		return (int)Fuel::ResultCodes::INTERNAL_FAILURE;
	}

	return (int)Fuel::ResultCodes::SUCCESS;
}
