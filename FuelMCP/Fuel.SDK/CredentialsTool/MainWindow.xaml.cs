﻿using System;
using System.Threading;
using System.Windows;
using FuelPump.Providers.Credentials;
using FuelPump.Metrics;
using Ninject;
using Ninject.Modules;
using FuelPump.Activation;
using Amazon.Auth.Map.Services;
using Amazon.Auth.Map;
using Amazon.Auth.Map.Services.Panda;

namespace CredentialsTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        protected CredsProvider provider;
        protected IPandaService pandaService;

        //
        // Window and handlers.
        //
            
        public MainWindow()
        {
            InitializeComponent();
            provider = new CredsProvider();

            var modules = new NinjectModule[]
            {
                new DependencyModule()
            };

            var kernel = new StandardKernel(modules);
            provider = kernel.Get<CredsProvider>();
            pandaService = kernel.Get<PandaService>();
        }

        // Load from saved credentials.
        private void Load_Click(object sender, RoutedEventArgs e)
        {
            this.AmazonToken.Text = "";
            this.AmazonKey.Text = "";
            this.RefreshToken.Text = "";
            this.JSONContents.Text = "";
            this.FileLocation.Text = "";

            Thread.Sleep(200); // Pause long enough to notice the clear.   

            Credentials creds = null;
            try
            {
                BearerCredentials identifyingCredentials = new BearerCredentials(this.ExternalTokenName.Text, this.ExternalTokenIdentifier.Text);
                creds = provider.GetCredentials(identifyingCredentials);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().Name + " thrown while trying to load credentials: " + ex.StackTrace);
                return;
            }

            this.RefreshToken.Text = creds.AmazonCredentials.RefreshToken;

            UpdateFileLocation();
        }

        // Save the JSON.  
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BearerCredentials identifyingCredentials = new BearerCredentials(this.ExternalTokenName.Text, this.ExternalTokenIdentifier.Text);
                AmazonCredentials amazonCredentials = new AmazonCredentials(this.RefreshToken.Text);

                provider.SetCredentials(identifyingCredentials, new Credentials(amazonCredentials));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            UpdateFileLocation();
            MessageBox.Show("credentials saved to " + this.FileLocation.Text);
        }

        // Show the JSON that would be saved.
        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            UpdateJson();
            UpdateFileLocation();
        }

        // Exit the program.
        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        // Clear out the fields except for the identifier.
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            this.AmazonToken.Text = "";
            this.AmazonKey.Text = "";
            this.RefreshToken.Text = "";
            this.JSONContents.Text = "";
            this.FileLocation.Text = "";
        }

        // Deregister the device matching the loaded creds.
        private void Deregister_Click(object sender, RoutedEventArgs e)
        {
            if(String.IsNullOrWhiteSpace(this.RefreshToken.Text))
            {
                MessageBox.Show("You must have a loaded refresh token to deregister.");
                return; 
            }

            DeregisterDevice(this.RefreshToken.Text);
        }

        //
        // Utility functions.
        //

        private void UpdateFileLocation()
        {
            this.FileLocation.Text = provider.GetStorageLocationForIdentifyingCredentials(new BearerCredentials(this.ExternalTokenName.Text, this.ExternalTokenIdentifier.Text));
        }

        private void UpdateJson()
        {
            try
            {
                BearerCredentials externalCreds = new BearerCredentials(this.ExternalTokenName.Text, this.ExternalTokenIdentifier.Text);
                AmazonCredentials amazonCreds = new AmazonCredentials(this.RefreshToken.Text);

                string jsonContent = provider.GetSerializedContents(externalCreds, new Credentials(amazonCreds));
                this.JSONContents.Text = jsonContent;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void DeregisterDevice(string refreshToken)
        {
            string accessToken;
            try
            {
                PandaRefreshAccessTokenResponse response = await pandaService.RefreshAccessTokenAsync(new AmazonRefreshToken(refreshToken));
                accessToken = response.access_token;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().Name + " thrown while trying to refresh access token: " + ex.StackTrace);
                return;
            }

            try
            {
                PandaDeregisterDeviceResponse response = pandaService.DeregisterDeviceAsync(new AmazonAccessToken(accessToken)).Result;

                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show("device deregistered and credentials nulled out");
                }

                if (response.Error != null)
                {
                    MessageBox.Show(response.ErrorType + " deregistering:\n\nDetails: " + response.ErrorDetails);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().Name + " thrown while trying to deregister device: " + ex.StackTrace);
                return;
            }

        }
    }

    // Don't record metrics
    class NullMetricsQueue : IMetricQueue
    {
        public void RecordEvent(MetricEvent metricEvent)
        {
        }
    }

    // Get access to the internal methods of the credentials provider.
    public class CredsProvider : RegistryCredentialsProvider
    {
        public CredsProvider() : base(new FuelPump.Metrics.MetricEventFactory("","",new NullMetricsQueue()))
        {
        }

        public new string GetSerializedContents(BearerCredentials identifyingCredentials, Credentials credentials)
        {
            return base.GetSerializedContents(identifyingCredentials, credentials);
        }

        public new string GetStorageLocationForIdentifyingCredentials(BearerCredentials identifyingCredentials)
        {
            // This is a hack to represent where the registry token will be found in a way that's useful to humans.
            return "HKEY_CURRENT_USER\\Software\\Amazon\\" + base.GetStorageLocationForIdentifyingCredentials(identifyingCredentials);
        }

        public new Credentials GetCredentials(BearerCredentials identifyingCredentials)
        {
            return base.GetCredentials(identifyingCredentials);
        }
    }
}
