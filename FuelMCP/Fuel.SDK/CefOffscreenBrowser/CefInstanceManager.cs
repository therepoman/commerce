﻿using CefSharp;
using CefSharp.OffScreen;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefOffscreenBrowser
{
    public class CefInstanceManager : IDisposable 
    {
        public class UnknownCefInstanceException : System.Exception
        {
            public UnknownCefInstanceException() : base() { }
            public UnknownCefInstanceException(string message) : base(message) { }
            public UnknownCefInstanceException(string message, System.Exception inner) : base(message, inner) { }
        }

        Dictionary<string, CefInstance> instances;

        public static string INSTANCE_ID = "THE INSTANCE !";

        public CefInstanceManager()
        {
            instances = new Dictionary<string, CefInstance>();
        }

        private void checkIfInstanceExists(string id)
        {
            if (!instances.ContainsKey(id))
            {
                throw new UnknownCefInstanceException();
            }
        }

        public void AddScreenDataUpdateCallback(string id, CefInstance.NewScreenDataCallback callback)
        {
            checkIfInstanceExists(id);
            instances[id].AddScreenDataUpdateCallback(callback);
        }

        public void Navigate(string id, string url)
        {
            checkIfInstanceExists(id);
            instances[id].Navigate(url);
        }

        public void SendKey(string id, KeyEventType type, int keyCode)
        {
            checkIfInstanceExists(id);
            instances[id].SendKey(type, keyCode);
        }

        public void SendMouseEvent(string id, int x, int y, MouseButtonType type, bool mouseUp, int clickCount)
        {
            checkIfInstanceExists(id);
            instances[id].SendMouseEvent(x, y, type, mouseUp, clickCount);
        }

        public void SendMouseMoveEvent(string id, int x, int y, bool leaving, CefEventFlags flags)
        {
            checkIfInstanceExists(id);
            instances[id].SendMouseMoveEvent(x, y, leaving, flags);
        }

        public void SendMouseWheelEvent(string id, int x, int y, int deltaX, int deltaY, CefEventFlags flags)
        {
            checkIfInstanceExists(id);
            instances[id].SendMouseWheelEvent(x, y, deltaX, deltaY, flags);
        }

        public void SetWindowSize(int w, int h)
        {
            foreach (KeyValuePair<string, CefInstance> entry in instances)
            {
                entry.Value.SetWindowSize(w, h);
            }
        }

        public string CreateInstance(string jsObjectName, Object jsObject)
        {
            if (!instances.ContainsKey(INSTANCE_ID))
            {
                //TODO :: Do something like this once we support multiple instances.
                //string instanceId = System.Guid.NewGuid().ToString();
                //instances.Add(instanceId, new CefInstance(startUrl, callback));
                instances.Add(INSTANCE_ID, new CefInstance(jsObjectName, jsObject));
                return INSTANCE_ID;
            }
            else
            {
                return INSTANCE_ID;
            }
            
        }

        public void DestroyInstance(string id)
        {
            if (instances.ContainsKey(id))
            {
                instances[id].Destroy();
                instances.Remove(id);
            }
        }

        public void Dispose()
        {
            foreach (var instance in instances)
            {
                instance.Value.Destroy();
            }
            instances.Clear();
        }
    }
}
