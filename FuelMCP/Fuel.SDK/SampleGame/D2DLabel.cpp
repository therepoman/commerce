#include "stdafx.h"
#include "D2DLabel.h"
#include <stdexcept>

D2DLabel::D2DLabel(int x, int y, int width, int height, int fontSize, float dpiX, float dpiY)
	: ILabel(x, y, width, height, fontSize, dpiX, dpiY)
{
	m_layout.left = (float)x;
	m_layout.top = (float)y;
	m_layout.right = (float)x + width;
	m_layout.bottom = (float)y + height;
}

D2DLabel::~D2DLabel()
{
	release();
}

void D2DLabel::init(ID2D1RenderTarget *d2dRenderTarget)
{
	HRESULT hr = d2dRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), &m_backgroundBrush);
	if (FAILED(hr)) throw std::runtime_error("Failed to create background brush");

	hr = d2dRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &m_textBrush);
	if (FAILED(hr)) throw std::runtime_error("Failed to create text brush");

	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&m_writeFactory));
	if (FAILED(hr)) throw std::runtime_error("Failed to create write factory");

	hr = m_writeFactory->CreateTextFormat(L"Arial", NULL, DWRITE_FONT_WEIGHT_REGULAR, DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL, m_fontSize, L"en-us", &m_textFormat);
}

void D2DLabel::release()
{
	m_backgroundBrush.Release();
	m_textBrush.Release();
	m_writeFactory.Release();
	m_textFormat.Release();
}

void D2DLabel::render(ID2D1RenderTarget *d2dRenderTarget)
{
	auto content = GetDisplayContent();
	d2dRenderTarget->FillRectangle(&m_layout, m_backgroundBrush);
	d2dRenderTarget->DrawTextW(content.c_str(), content.length(), m_textFormat, m_layout, m_textBrush);
}
