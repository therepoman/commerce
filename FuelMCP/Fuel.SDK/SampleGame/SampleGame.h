#pragma once

#include <windows.h>
#include <dinput.h>
#include <mutex>
#include "DX11GameRenderer.h"
#include <fuel_impl/IapClient.h>
#include "FpsTimer.h"

#define VENDOR_ID "TWITC"
#define TEST_SKU_1 L"BT00LXSS06"

enum InputType
{
	MessageLoop,
	DirectInput,
	LAST_INPUT_TYPE, // Needs to be last in the list for iterating over the enum.
};

enum WindowResizeType
{
	None,
	FullRestart,
	SwapChain,
	LAST_WINDOW_RESIZE_TYPE, // Needs to be last in the list for iterating over the enum.
};

class SampleGame
{
public:
	SampleGame(HINSTANCE hInstance, int nCmdShow);
	~SampleGame();

	LRESULT WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	int run();
private:
	void initRenderer();
	void releaseRenderer();

	void handleDirectInput();
	void handleMouseInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void render();
	void resize();
	void updateButtonText();

	void beginPurchase();
	void getProductData();
	void getPurchaseUpdates();
	void notifyFulfillment();

	void startSdk();

	std::wstring toUTCString(std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> timePoint);
	std::wstring toString(std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> timePoint);
	void appendResponse(std::wstring &message, IapClient::GetPurchaseUpdatesOutcome &response);

	HINSTANCE m_hInstance;
	int m_nCmdShow;
	HWND m_hWnd;
	std::unique_ptr<IGameRenderer, void(*)(IGameRenderer*)> m_renderer;
	std::unique_ptr<Fuel::Iap::IapClient> m_sdk;
	std::thread m_purchaseThread;
	std::shared_ptr<IButton> m_getProductDataButton;
	std::shared_ptr<IButton> m_purchaseButton;
	std::shared_ptr<IButton> m_getPurchaseUpdatesButton;
	std::shared_ptr<IButton> m_notifyFulfillmentButton;
	std::shared_ptr<IButton> m_sdkButton;
	std::shared_ptr<IButton> m_fpsLimitButton;
	std::shared_ptr<IButton> m_inputButton;
	std::shared_ptr<IButton> m_resizeButton;
	std::shared_ptr<ILabel> m_logLabel;
	bool m_recreateDeviceOnResize = false;
	bool m_needsResize = false;
	InputType m_inputType;
	WindowResizeType m_resizeType = SwapChain;
	FpsTimer m_fpsTimer;
	std::wstring m_fpsButtonTitle;
	int m_lastDisplayedFps = 0;

	std::mutex m_inputMutex;
	CComPtr<IDirectInput8W> m_directInput;
	CComPtr<IDirectInputDevice8W> m_directInputKeyboard;
	CComPtr<IDirectInputDevice8W> m_directInputMouse;
	uint8_t m_keyboardState[256];
	DIMOUSESTATE m_mouseState;
};
