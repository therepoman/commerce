#pragma once

#include "IButton.h"
#include "ILabel.h"

class IGameRenderer
{
public:
	IGameRenderer(HWND hWnd);
	virtual ~IGameRenderer();

	virtual std::shared_ptr<IButton> createButton(const WCHAR *m_title, int x, int y, int width, int height, int fontSize) = 0;
	virtual std::shared_ptr<ILabel> createLabel(int x, int y, int width, int height, int fontSize) = 0;

	virtual void init() = 0;
	virtual void render() = 0;
	virtual void resize() = 0;
protected:
	HWND m_hWnd;
};
