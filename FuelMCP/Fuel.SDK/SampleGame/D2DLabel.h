#pragma once

#include "ILabel.h"
#include <d2d1.h>
#include <dwrite.h>
#include <atlbase.h>

class D2DLabel : public ILabel
{
public:
	D2DLabel(int x, int y, int width, int height, int fontSize, float dpiX, float dpiY);
	virtual ~D2DLabel();

	void init(ID2D1RenderTarget *d2dRenderTarget);
	void release();

	void render(ID2D1RenderTarget *d2dRenderTarget);
private:
	D2D1_RECT_F m_layout;

	CComPtr<ID2D1SolidColorBrush> m_backgroundBrush;
	CComPtr<ID2D1SolidColorBrush> m_textBrush;
	CComPtr<IDWriteFactory> m_writeFactory;
	CComPtr<IDWriteTextFormat> m_textFormat;
};
