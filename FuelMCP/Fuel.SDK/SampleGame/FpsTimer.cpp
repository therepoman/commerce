#include "stdafx.h"
#include "FpsTimer.h"
#include <codecvt>

FpsTimer::FpsTimer()
{
	m_timer = CreateWaitableTimer(NULL, FALSE, NULL);
}

FpsTimer::~FpsTimer()
{
	CloseHandle(m_timer);
}

int FpsTimer::GetFpsLimit()
{
	return m_fps;
}

void FpsTimer::SetFpsLimit(int fps)
{
	m_fps = fps;
	if (m_fps > 0)
	{
		LARGE_INTEGER liDueTime;
		long periodMillis = (long)(1000.0 / m_fps);
		liDueTime.QuadPart = (LONGLONG)periodMillis * 10000LL;
		SetWaitableTimer(m_timer, &liDueTime, periodMillis, NULL, NULL, 0);
	}
}

void FpsTimer::Wait()
{
	ULONGLONG now = GetTickCount64();
	if (m_lastFpsCheckTime == 0)
	{
		m_lastFpsCheckTime = now;
	}
	else if (m_lastFpsCheckTime + 1000 < now)
	{
		m_lastFps = (int)round((double)m_frameCount / (now - m_lastFpsCheckTime) * 1000.0);
		m_frameCount = 0;
		m_lastFpsCheckTime = now;
	}
	else
	{
		m_frameCount++;
	}

	if (m_fps > 0)
	{
		WaitForSingleObject(m_timer, 5000);
	}
}

int FpsTimer::GetMeasuredFps()
{
	return m_lastFps;
}
