#pragma once

#include <windows.h>

class FpsTimer
{
public:
	FpsTimer();
	~FpsTimer();

	int GetFpsLimit();
	void SetFpsLimit(int fps);

	void Wait();
	int GetMeasuredFps();
private:
	int m_fps = 0;
	HANDLE m_timer = NULL;

	int m_lastFps = 0;
	int m_frameCount = 0;
	ULONGLONG m_lastFpsCheckTime = 0;
};
