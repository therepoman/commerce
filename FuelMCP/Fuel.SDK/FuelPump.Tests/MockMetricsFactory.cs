﻿using FuelPump.Metrics;
using Moq;
using System.Collections.Generic;
using static FuelPump.Metrics.MetricEventFactory;

namespace FuelPump.Tests
{
    public class MockMetricsFactory
    {
        public Mock<MetricEventFactory> Factory { get; private set; }
        public List<Mock<MetricEvent>> MetricEvents { get; private set; }
        public List<Mock<TimerDataPoint>> Timers { get; private set; }

        public MockMetricsFactory()
        {
            Factory = new Mock<MetricEventFactory>("foo", "foo", null);
            MetricEvents = new List<Mock<MetricEvent>>();
            Timers = new List<Mock<TimerDataPoint>>();

            Factory.Setup(mock => mock.Create()).Returns(() =>
            {
                var metricEvent = new Mock<MetricEvent>("foo", "foo");
                MetricEvents.Add(metricEvent);

                metricEvent.Setup(mock => mock.StartTimer(It.IsAny<string>())).Returns((string name) =>
                {
                    var mockTimer = new Mock<TimerDataPoint>("foo");
                    Timers.Add(mockTimer);
                    return mockTimer.Object;
                });

                var scope = new Mock<MetricEventScope>(null, null);
                scope.SetupGet(mock => mock.MetricEvent).Returns(metricEvent.Object);
                return scope.Object;
            });
        }
    }
}
