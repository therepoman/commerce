﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using FlatBuffers;
using Messages = Fuel.Messaging;

using FuelPump;
using System.Linq;
using System.Threading.Tasks;

using Moq;

namespace FuelPump.Tests
{
    [TestClass]
    public class MessageDispatcherTests
    {
        // handle request timeouts
        
        [TestMethod]
        public async Task MessageDispatcher_RouteUntypedRequest()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var message = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.InitializeRequest, 0),
                CreateTestInitializeRequest("gameversion", "sdkversion"));

            MessageContext capturedContext = null;
            Message capturedMessage = null;
            dispatcher.RegisterHandler<Messages.InitializeRequest>(async (MessageContext c, Message m) =>
            {
                await Task.Delay(1); // create a context switch

                capturedContext = c;
                capturedMessage = m;
            });

            await dispatcher.ProcessMessage(message);

            Assert.IsNotNull(capturedContext);
            Assert.IsNotNull(capturedMessage);
            Assert.AreEqual("1234", capturedContext.RequestId);
            Assert.AreEqual(0, capturedContext.Retry);
            Assert.AreSame(message, capturedMessage);
        }

        [TestMethod]
        public async Task MessageDispatcher_RouteUntypedRequestResponse()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var requestMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataRequest, 0),
                CreateGetProductDataRequest(new [] { "sku" }));

            var responseMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataResponse, 0),
                CreateGetProductDataResponse(new [] { "sku" }, new [] { "unavailable" }));

            MessageContext capturedRequestContext = null;
            Message capturedRequestMessage = null;
            dispatcher.RegisterHandler<Messages.GetProductDataRequest, Messages.GetProductDataResponse>(async (MessageContext c, Message m) =>
            {
                await Task.Delay(1); // create a context switch

                capturedRequestContext = c;
                capturedRequestMessage = m;

                return responseMessage;
            });

            MessageContext capturedResponseContext = null;
            Message capturedResponseMessage = null;
            dispatcher.RegisterHandler<Messages.GetProductDataResponse>(async (MessageContext c, Message m) =>
            {
                await Task.Delay(1); // create a context switch

                capturedResponseContext = c;
                capturedResponseMessage = m;
            });

            await dispatcher.ProcessMessage(requestMessage);

            // validate request message
            Assert.IsNotNull(capturedRequestContext);
            Assert.IsNotNull(capturedRequestMessage);
            Assert.AreEqual("1234", capturedRequestContext.RequestId);
            Assert.AreEqual(0, capturedRequestContext.Retry);
            Assert.AreSame(requestMessage, capturedRequestMessage);

            // validate response message
            Assert.IsNotNull(capturedResponseContext);
            Assert.IsNotNull(capturedResponseMessage);
            Assert.AreEqual("1234", capturedResponseContext.RequestId);
            Assert.AreEqual(0, capturedResponseContext.Retry);
            Assert.AreSame(responseMessage, capturedResponseMessage);
        }

        [TestMethod]
        public async Task MessageDispatcher_RouteTypedRequest()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var message = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.InitializeRequest, 0),
                CreateTestInitializeRequest("gameversion", "sdkversion"));

            MessageContext capturedContext = null;
            Messages.InitializeRequest capturedMessage = null;
            dispatcher.RegisterHandler<Messages.InitializeRequest>(async (MessageContext c, Messages.InitializeRequest m) =>
            {
                await Task.Delay(1); // create a context switch

                capturedContext = c;
                capturedMessage = m;
            });

            await dispatcher.ProcessMessage(message);

            Assert.IsNotNull(capturedContext);
            Assert.IsNotNull(capturedMessage);
            Assert.AreEqual("1234", capturedContext.RequestId);
            Assert.AreEqual(0, capturedContext.Retry);
            Assert.AreEqual("gameversion", capturedMessage.GameVersion);
            Assert.AreEqual("sdkversion", capturedMessage.SdkVersion);
        }

        [TestMethod]
        public async Task MessageDispatcher_RouteTypedRequestResponse()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var requestMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataRequest, 0),
                CreateGetProductDataRequest(new[] { "sku" }));

            MessageContext capturedRequestContext = null;
            Messages.GetProductDataRequest capturedRequestMessage = null;
            dispatcher.RegisterHandler<Messages.GetProductDataRequest, Messages.GetProductDataResponse>(async (MessageContext c, Messages.GetProductDataRequest m) =>
            {
                await Task.Delay(1); // create a context switch

                capturedRequestContext = c;
                capturedRequestMessage = m;

                return new MessageFactory<Messages.GetProductDataResponse>(
                    (builder) => CreateGetProductDataResponse(builder, new[] { "sku" }, new[] { "unavailable" }));
            });

            MessageContext capturedResponseContext = null;
            Messages.GetProductDataResponse capturedResponseMessage = null;
            dispatcher.RegisterHandler<Messages.GetProductDataResponse>(async (MessageContext c, Messages.GetProductDataResponse m) =>
            {
                await Task.Delay(1); // create a context switch

                capturedResponseContext = c;
                capturedResponseMessage = m;
            });

            await dispatcher.ProcessMessage(requestMessage);

            // validate request message
            Assert.IsNotNull(capturedRequestContext);
            Assert.IsNotNull(capturedRequestMessage);
            Assert.AreEqual("1234", capturedRequestContext.RequestId);
            Assert.AreEqual(0, capturedRequestContext.Retry);
            Assert.AreEqual(1, capturedRequestMessage.SkusLength);
            Assert.AreEqual("sku", capturedRequestMessage.GetSkus(0));

            // validate response message
            Assert.IsNotNull(capturedResponseContext);
            Assert.IsNotNull(capturedResponseMessage);
            Assert.AreEqual("1234", capturedResponseContext.RequestId);
            Assert.AreEqual(0, capturedResponseContext.Retry);
            Assert.AreEqual(1, capturedResponseMessage.ProductsLength);
            Assert.AreEqual("sku", capturedResponseMessage.GetProducts(0).Sku);
            Assert.AreEqual(1, capturedResponseMessage.UnavailableSkusLength);
            Assert.AreEqual("unavailable", capturedResponseMessage.GetUnavailableSkus(0));
        }

        [TestMethod]
        public async Task MessageDispatcher_UnhandledMessage_Request()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var message = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.InitializeRequest),
                CreateTestInitializeRequest("gameversion", "sdkversion"));

            Message unhandledMessage = null; 
            dispatcher.UnhandledMessage += (object sender, UnhandledMessageEventArgs e) =>
            {
                unhandledMessage = e.Message;
            };

            await dispatcher.ProcessMessage(message);

            Assert.IsNotNull(unhandledMessage);
            Assert.AreSame(message, unhandledMessage);
        }

        [TestMethod]
        public async Task MessageDispatcher_UnhandledMessage_Response()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var requestMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataRequest),
                CreateGetProductDataRequest(new[] { "sku" }));

            var responseMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataResponse),
                CreateGetProductDataResponse(new[] { "sku" }, new[] { "unavailable" }));

            dispatcher.RegisterHandler<Messages.GetProductDataRequest, Messages.GetProductDataResponse>(async (MessageContext c, Message m) =>
            {
                await Task.Delay(1); // create a context switch

                return responseMessage;
            });

            Message unhandledMessage = null; 
            dispatcher.UnhandledMessage += (object sender, UnhandledMessageEventArgs e) =>
            {
                unhandledMessage = e.Message;
            };

            await dispatcher.ProcessMessage(requestMessage);

            // validate unhandled message
            Assert.IsNotNull(unhandledMessage);
            Assert.AreSame(responseMessage, unhandledMessage);
        }

        [TestMethod]
        public async Task MessageDispatcher_UnhandledException_UntypedRequest()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var message = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.InitializeRequest),
                CreateTestInitializeRequest("gameversion", "sdkversion"));

            dispatcher.RegisterHandler<Messages.InitializeRequest>(async (MessageContext c, Message m) =>
            {
                await Task.Delay(1); // create a context switch

                throw new ApplicationException("Test Exception");
            });

            Message capturedMessage = null;
            Exception capturedException = null;
            dispatcher.UnhandledException += (object sender, HandlerExceptionEventArgs e) =>
            {
                capturedMessage = e.Message;
                capturedException = e.Exception;
            };

            await dispatcher.ProcessMessage(message);

            Assert.IsNotNull(capturedMessage);
            Assert.IsNotNull(capturedException);
            Assert.AreSame(message, capturedMessage);
            Assert.AreEqual("Test Exception", capturedException.Message);
        }

        [TestMethod]
        public async Task MessageDispatcher_UnhandledException_UntypedRequestResponse()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var requestMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataRequest),
                CreateGetProductDataRequest(new[] { "sku" }));

            dispatcher.RegisterHandler<Messages.GetProductDataRequest, Messages.GetProductDataResponse>(async (MessageContext c, Message m) =>
            {
                await Task.Delay(1); // create a context switch

                throw new ApplicationException("Test Exception");
            });

            Message capturedMessage = null;
            Exception capturedException = null;
            dispatcher.UnhandledException += (object sender, HandlerExceptionEventArgs e) =>
            {
                capturedMessage = e.Message;
                capturedException = e.Exception;
            };

            await dispatcher.ProcessMessage(requestMessage);

            Assert.IsNotNull(capturedMessage);
            Assert.IsNotNull(capturedException);
            Assert.AreSame(requestMessage, capturedMessage);
            Assert.AreEqual("Test Exception", capturedException.Message);
        }

        [TestMethod]
        public async Task MessageDispatcher_UnhandledException_TypedRequest()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var message = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.InitializeRequest),
                CreateTestInitializeRequest("gameversion", "sdkversion"));

            dispatcher.RegisterHandler<Messages.InitializeRequest>(async (MessageContext c, Messages.InitializeRequest m) =>
            {
                await Task.Delay(1); // create a context switch

                throw new ApplicationException("Test Exception");
            });

            Message capturedMessage = null;
            Exception capturedException = null;
            dispatcher.UnhandledException += (object sender, HandlerExceptionEventArgs e) =>
            {
                capturedMessage = e.Message;
                capturedException = e.Exception;
            };

            await dispatcher.ProcessMessage(message);

            Assert.IsNotNull(capturedMessage);
            Assert.IsNotNull(capturedException);
            Assert.AreSame(message, capturedMessage);
            Assert.AreEqual("Test Exception", capturedException.Message);
        }

        [TestMethod]
        public async Task MessageDispatcher_UnhandledException_TypedRequestResponse()
        {
            var dispatcher = new FuelPump.MessageDispatcher();

            var requestMessage = new FuelPump.Message(
                CreateHeader("1234", Fuel.Messaging.Message.GetProductDataRequest),
                CreateGetProductDataRequest(new[] { "sku" }));

            dispatcher.RegisterHandler<Messages.GetProductDataRequest, Messages.GetProductDataResponse>(async (MessageContext c, Messages.GetProductDataRequest m) =>
            {
                await Task.Delay(1); // create a context switch

                throw new ApplicationException("Test Exception");
            });

            Message capturedMessage = null;
            Exception capturedException = null;
            dispatcher.UnhandledException += (object sender, HandlerExceptionEventArgs e) =>
            {
                capturedMessage = e.Message;
                capturedException = e.Exception;
            };

            await dispatcher.ProcessMessage(requestMessage);

            Assert.IsNotNull(capturedMessage);
            Assert.IsNotNull(capturedException);
            Assert.AreSame(requestMessage, capturedMessage);
            Assert.AreEqual("Test Exception", capturedException.Message);
        }

        private byte[] CreateHeader(string requestId, Fuel.Messaging.Message type)
        {
            return CreateHeader(requestId, type, 0);
        }

        private byte[] CreateHeader(string requestId, Fuel.Messaging.Message type, int retry)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);
            var header = Messages.MessageHeader.CreateMessageHeader(builder, builder.CreateString(requestId), retry, (int)type);

            builder.Finish(header.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateTestInitializeRequest(string gameVersion, string sdkVersion)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var request = Messages.InitializeRequest.CreateInitializeRequest(builder, builder.CreateString(gameVersion), builder.CreateString(sdkVersion));
            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.InitializeRequest, request.Value);

            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateGetProductDataRequest(string[] skus)
        { 
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var skusVector = Messages.GetProductDataRequest.CreateSkusVector(builder,
                skus.Select(sku => builder.CreateString(sku)).ToArray());

            var request = Messages.GetProductDataRequest.CreateGetProductDataRequest(builder, 
                Messages.CredentialsCache.CreateCredentialsCache(builder,
                    Messages.CredentialsCache.CreateCredentialsVector(builder, new FlatBuffers.Offset<Messages.Credential>[0])),
                skusVector);

            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.GetProductDataRequest, request.Value);

            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }

        private Offset<Messages.GetProductDataResponse> CreateGetProductDataResponse(FlatBuffers.FlatBufferBuilder builder, string[] productSkus, string[] unavailableSkus)
        {
            var skusVector = Messages.GetProductDataResponse.CreateUnavailableSkusVector(builder, 
                unavailableSkus.Select(sku => builder.CreateString(sku)).ToArray());

            var productsVector = Messages.GetProductDataResponse.CreateProductsVector(builder,
                productSkus.Select(sku => Messages.Product.CreateProduct(builder,
                    builder.CreateString(sku),
                    builder.CreateString(sku + " Description"),
                    builder.CreateString("1.00"),
                    builder.CreateString("http://somewhere"),
                    builder.CreateString(sku + " Title"),
                    Messages.ProductType.Consumable)).ToArray());

            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, skusVector, productsVector);
        }

        private byte[] CreateGetProductDataResponse(string[] productSkus, string[] unavailableSkus)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var skusVector = Messages.GetProductDataResponse.CreateUnavailableSkusVector(builder, 
                unavailableSkus.Select(sku => builder.CreateString(sku)).ToArray());

            var productsVector = Messages.GetProductDataResponse.CreateProductsVector(builder,
                productSkus.Select(sku => Messages.Product.CreateProduct(builder,
                    builder.CreateString(sku),
                    builder.CreateString(sku + " Description"),
                    builder.CreateString("1.00"),
                    builder.CreateString("http://somewhere"),
                    builder.CreateString(sku + " Title"),
                    Messages.ProductType.Consumable)).ToArray());

            var request = Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, skusVector, productsVector);
            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.GetProductDataResponse, request.Value);
            
            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }
    }
}
