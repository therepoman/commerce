﻿using FuelPump.Metrics;
using FuelPump.Services.Config;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static FuelPump.Metrics.MetricEventFactory;

namespace FuelPump.Tests
{
    [TestClass]
    public class ConfigHandlerTests
    {
        HttpResponseMessage successfulResponse;
        HttpResponseMessage successfulResponse2;
        HttpResponseMessage invalidResponse;
        MockHttpMessageHandler handler;
        Mock<ConfigHandlerWithGetterForProtectedField> configHandler;
        MockMetricsFactory metricsFactory;
        FuelConfig bootstrapConfig;

        [TestInitialize]
        public void Setup()
        {
            // Configure the logger and then add the trace appender so that it logs to the
            // Visual Studio output window when tests are run with debugging.
            BasicConfigurator.Configure();

            var patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = "%d [%t] %-5p %m%n";
            patternLayout.ActivateOptions();

            var traceAppender = new TraceAppender();
            traceAppender.Layout = patternLayout;
            traceAppender.ActivateOptions();

            ((Hierarchy)LogManager.GetRepository()).Root.AddAppender(traceAppender);

            var remoteConfig = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config1));
            remoteConfig.configSettings.file = "http://remote";
            remoteConfig.AssertValid();
            successfulResponse = new HttpResponseMessage(HttpStatusCode.OK);
            successfulResponse.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(remoteConfig)));

            var remoteConfig2 = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config1));
            remoteConfig2.configSettings.file = "http://remote2";
            remoteConfig2.AssertValid();
            successfulResponse2 = new HttpResponseMessage(HttpStatusCode.OK);
            successfulResponse2.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(remoteConfig2)));

            var invalidConfig = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config1));
            invalidConfig.configSettings.file = "invalid";
            try
            {
                invalidConfig.AssertValid();
                Assert.Fail("The config should be invalid");
            }
            catch (InvalidConfigurationException)
            {
                // The invalid config is working.
            }
            invalidResponse = new HttpResponseMessage(HttpStatusCode.OK);
            invalidResponse.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(invalidConfig)));

            handler = new MockHttpMessageHandler();

            metricsFactory = new MockMetricsFactory();
            configHandler = new Mock<ConfigHandlerWithGetterForProtectedField>(
                new Lazy<MetricEventFactory>(() => metricsFactory.Factory.Object), new HttpClient(handler));
            configHandler.CallBase = true;
            bootstrapConfig = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config1));
            bootstrapConfig.configSettings.file = "http://bootstrap";
            bootstrapConfig.AssertValid();
            configHandler.Protected().Setup<string>("GetBootstrapConfig").Returns(JsonConvert.SerializeObject(bootstrapConfig));
        }

        [TestMethod]
        public void ConfigHandlerTests_LoadRemoteConfigOnInitialize()
        {
            var response = handler.SetupResponse();
            handler.SetupResponse().SetResult(successfulResponse);
            configHandler.Object.Initialize();
            var config = Task.Run<FuelConfig>(() => configHandler.Object.GetConfig());

            // Wait a while for getConfig to either return (fail) or get blocked (success).
            Thread.Sleep(100);

            // It should be blocked on the mocked network call.
            Assert.IsFalse(config.IsCompleted);

            // Complete the mocked network call.
            response.SetResult(successfulResponse);

            // GetConfig should return the remote config now.
            config.Wait(5000);
            Assert.IsTrue(config.IsCompleted);
            Assert.AreEqual("http://remote", config.Result.configSettings.file);

            // It should have requested the url from the bootstrap config, then the remote config
            Assert.AreEqual("http://bootstrap/", handler.Requests[0].RequestUri.ToString());
            Assert.AreEqual("http://remote/", handler.Requests[1].RequestUri.ToString());

            // It should record the load time and no error counters.
            Assert.AreEqual(1, metricsFactory.MetricEvents.Count);
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Never);
            Assert.AreEqual(1, metricsFactory.Timers.Count);
        }

        [TestMethod]
        public void ConfigHandlerTests_SecondRemoteConfigFails()
        {
            handler.SetupResponse().SetResult(successfulResponse);
            handler.SetupResponse().SetResult(invalidResponse);
            configHandler.Object.Initialize();
            var config = Task.Run<FuelConfig>(() => configHandler.Object.GetConfig());

            config.Wait(1000);
            Assert.IsTrue(config.IsCompleted);
            Assert.AreEqual("http://remote", config.Result.configSettings.file);

            // It should have requested the url from the bootstrap config, then the remote config
            Assert.AreEqual("http://bootstrap/", handler.Requests[0].RequestUri.ToString());
            Assert.AreEqual("http://remote/", handler.Requests[1].RequestUri.ToString());
            
            // It should record the load time and one error counter.
            Assert.AreEqual(1, metricsFactory.MetricEvents.Count);
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Once());
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter("Service_Failure_LoadConfig"), Times.Once());
            Assert.AreEqual(1, metricsFactory.Timers.Count);
        }

        [TestMethod]
        public void ConfigHandlerTests_RemoteConfigException()
        {
            handler.SetupResponse().SetException(new ApplicationException("Expected exception"));
            configHandler.Object.Initialize();
            var config = configHandler.Object.GetConfig();
            Assert.AreEqual("http://bootstrap", config.configSettings.file);

            // It should record the load time and one error counter.
            Assert.AreEqual(1, metricsFactory.MetricEvents.Count);
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Once());
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter("Service_Failure_LoadConfig"), Times.Once());
            Assert.AreEqual(1, metricsFactory.Timers.Count);
        }

        [TestMethod]
        public void ConfigHandlerTests_InvalidRemoteConfig()
        {
            var start = DateTime.Now;
            configHandler.Protected().Setup<DateTime>("GetCurrentTime").Returns(start);

            handler.SetupResponse().SetResult(invalidResponse); // url from bootstrap
            var response2 = handler.SetupResponse(); // url from bootstrap, second attempt
            handler.SetupResponse().SetResult(successfulResponse); // url from remote
            configHandler.Object.Initialize();

            Assert.AreEqual("http://bootstrap", configHandler.Object.GetConfig().configSettings.file);
            AssertWithinJitter(start, bootstrapConfig.configSettings.fallbackDurationSeconds,
                bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

            var startConfigExpired = start.AddDays(7);
            configHandler.Protected().Setup<DateTime>("GetCurrentTime").Returns(startConfigExpired);

            // Triggers refresh, but returns bootstrap config without waiting to see if it will work.
            Assert.AreEqual("http://bootstrap", configHandler.Object.GetConfig().configSettings.file);

            // Make the mock network call successful and wait for the response to be processed.
            response2.SetResult(successfulResponse);
            WaitForConfigToLoad("http://remote");
            AssertWithinJitter(startConfigExpired, bootstrapConfig.configSettings.refreshIntervalSeconds,
                bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

            // It should record the load time twice and one error counter.
            Assert.AreEqual(2, metricsFactory.MetricEvents.Count);
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Once());
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter("Service_Failure_LoadConfig"), Times.Once());
            metricsFactory.MetricEvents[1].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Never());
            Assert.AreEqual(2, metricsFactory.Timers.Count);
        }

        [TestMethod]
        public void ConfigHandlerTests_RemoteConfigCausesFailures()
        {
            var start = DateTime.Now;
            configHandler.Protected().Setup<DateTime>("GetCurrentTime").Returns(start);

            // Successfully load the remote config.
            handler.SetupResponse().SetResult(successfulResponse); // url from bootstrap
            handler.SetupResponse().SetResult(successfulResponse); // url from remote
            handler.SetupResponse().SetResult(successfulResponse); // url from bootstrap
            handler.SetupResponse().SetResult(successfulResponse); // url from remote
            configHandler.Object.Initialize();
            var remoteConfig = configHandler.Object.GetConfig();

            // Trigger fallback
            for (int i = 0; i < remoteConfig.configSettings.maxFailuresUntilFallback; i++)
            {
                // Remote settings are still provided.
                Assert.AreEqual("http://remote", configHandler.Object.GetConfig().configSettings.file);
                AssertWithinJitter(start, bootstrapConfig.configSettings.refreshIntervalSeconds,
                    bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

                // A call fails.
                configHandler.Object.ReportApiCallOutcome("SomeApi", false);
            }

            // Bootstrap config is provided after sufficient failures.
            Assert.AreEqual("http://bootstrap", configHandler.Object.GetConfig().configSettings.file);

            // It should wait for the fallback duration instead of the refresh interval.
            AssertWithinJitter(start, bootstrapConfig.configSettings.fallbackDurationSeconds,
                bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

            // Advance time to trigger refresh.
            var reloadTime = start.AddDays(7);
            configHandler.Protected().Setup<DateTime>("GetCurrentTime").Returns(reloadTime);

            // Triggers refresh, but returns bootstrap config without waiting to see if it will work.
            Assert.AreEqual("http://bootstrap", configHandler.Object.GetConfig().configSettings.file);

            // Wait for the response to be processed.
            WaitForConfigToLoad("http://remote");
            AssertWithinJitter(reloadTime, bootstrapConfig.configSettings.refreshIntervalSeconds,
                bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

            // It should record the load time twice and no error counters.
            Assert.AreEqual(2, metricsFactory.MetricEvents.Count);
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Never());
            metricsFactory.MetricEvents[1].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Never());
            Assert.AreEqual(2, metricsFactory.Timers.Count);
        }

        [TestMethod]
        public void ConfigHandlerTests_RefreshConfig()
        {
            handler.SetupResponse().SetResult(successfulResponse); // url from bootstrap
            handler.SetupResponse().SetResult(successfulResponse); // url from remote

            var response2 = handler.SetupResponse(); // url from bootstrap
            handler.SetupResponse().SetResult(successfulResponse2); // url from remote

            var start = DateTime.Now;
            configHandler.Protected().Setup<DateTime>("GetCurrentTime").Returns(start);

            // Successfully load the remote config.
            configHandler.Object.Initialize();
            var remoteConfig = configHandler.Object.GetConfig();
            AssertWithinJitter(start, bootstrapConfig.configSettings.refreshIntervalSeconds,
                bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

            // Advance time to trigger refresh.
            var reloadTime = start.AddDays(7);
            configHandler.Protected().Setup<DateTime>("GetCurrentTime").Returns(reloadTime);

            // Triggers refresh, but returns the old config without waiting to see if it will work.
            Assert.AreEqual("http://remote", configHandler.Object.GetConfig().configSettings.file);

            // Wait for the response to be processed.
            response2.SetResult(successfulResponse);
            WaitForConfigToLoad("http://remote2");
            AssertWithinJitter(reloadTime, bootstrapConfig.configSettings.refreshIntervalSeconds,
                bootstrapConfig.configSettings.jitterMultiplier, configHandler.Object.GetNextConfigReload());

            // It should record the load time twice and no error counters.
            Assert.AreEqual(2, metricsFactory.MetricEvents.Count);
            metricsFactory.MetricEvents[0].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Never());
            metricsFactory.MetricEvents[1].Verify(mock => mock.AddCounter(It.IsAny<string>()), Times.Never());
            Assert.AreEqual(2, metricsFactory.Timers.Count);
        }

        private void WaitForConfigToLoad(string expectedConfigUrl)
        {
            string currentUrl = null;
            for (int i = 0; i < 10; i++)
            {
                currentUrl = configHandler.Object.GetConfig().configSettings.file;
                if (currentUrl == expectedConfigUrl)
                {
                    return;
                }
                Thread.Sleep(100);
            }

            Assert.Fail("It should have loaded a new config containing " + expectedConfigUrl + ", but got " + currentUrl + " instead");
        }

        private void AssertWithinJitter(DateTime reference, double? duration, double? jitterMultiplier, DateTime? other)
        {
            Assert.IsNotNull(other);

            var minExpectedTime = reference.AddSeconds(duration.Value);
            var maxExpectedTime = reference.AddSeconds(duration.Value * (1 + jitterMultiplier.Value));

            Assert.IsTrue(other.Value.CompareTo(minExpectedTime) >= 0, other.Value + " should be equal to or after " + minExpectedTime);
            Assert.IsTrue(other.Value.CompareTo(maxExpectedTime) <= 0, other.Value + " should be equal to or before " + maxExpectedTime);
        }

        public class ConfigHandlerWithGetterForProtectedField : ConfigHandler
        {
            public ConfigHandlerWithGetterForProtectedField(Lazy<MetricEventFactory> metricsFactory, HttpClient httpClient) : base(metricsFactory, httpClient)
            {
            }

            public DateTime? GetNextConfigReload()
            {
                return nextConfigReload;
            }
        }
    }
}
