﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Tests.Metrics
{
    public class MockMetricsQueue : FuelPump.Metrics.IMetricQueue
    {
        List<FuelPump.Metrics.MetricEvent> m_events = new List<FuelPump.Metrics.MetricEvent>();

        public void RecordEvent(FuelPump.Metrics.MetricEvent metricEvent)
        {
            m_events.Add(metricEvent);
        }

        public void Flush() { }

        public bool HasDataPoint(string name)
        {
            return m_events.SelectMany(evt => evt.Serialize().DataPoints).Any(dataPoint => dataPoint.Name == name);
        }
    }
}
