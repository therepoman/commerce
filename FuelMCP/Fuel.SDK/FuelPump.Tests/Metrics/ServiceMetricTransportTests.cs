﻿using FuelPump.Metrics;
using FuelPump.Services.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Tests.Metrics
{
    [TestClass]
    public class ServiceMetricTransportTests
    {
        [TestMethod]
        [Ignore] // skipped for now to avoid sending real metrics everytime a test runs
        public async Task Metrics_ServiceTransport_Simple()
        {
            ServiceMetricTransport transport = new ServiceMetricTransport(
                createConfig("https://device-metrics-us-2.amazon.com", "/metricsBatch"),
                "AOFR8BROH3RC3",
                "TEST_DSN",
                "v1.0");

            var metric1 = new MetricEvent("Fuel", "FuelPump");
            var counter1 = metric1.AddCounter("Startup");
            counter1.Increment(1);

            var shutdown1 = metric1.AddCounter("Shutdown");
            shutdown1.Increment(1);

            await transport.TransmitMetricBatch(new[]
            {
                metric1,
                metric1
            });
        }

        private ConfigHandler createConfig(string endpoint, string path)
        {
            var config = new FuelConfig();
            config.metrics.endpoint = endpoint;
            config.metrics.path = path;

            var mock = new Mock<ConfigHandler>();
            mock.Setup(configHandler => configHandler.GetConfig()).Returns(config);

            return mock.Object;
        }
    }
}
