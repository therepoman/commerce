﻿using FuelPump.Metrics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    [TestClass]
    public class MetricEventTests
    {
        [TestMethod]
        public void Metrics_Event_Serialize()
        {
            var metricsEvent = new MetricEvent("program", "source");
            var message = metricsEvent.Serialize();

            Assert.AreEqual("program", message.Program, "program");
            Assert.AreEqual("source", message.Source, "source");
        }

        [TestMethod]
        public void Metrics_CounterEvents_Serialize()
        {
            var metricsEvent = new MetricEvent("program", "source");

            var counter = metricsEvent.AddCounter("Test");

            var message = metricsEvent.Serialize();

            Assert.AreEqual(1, message.DataPoints.Count);
            var counterMessage = message.DataPoints.First();

            Assert.AreEqual("Test", counterMessage.Name);
            Assert.AreEqual("COUNTER", counterMessage.Type);
        }

        [TestMethod]
        public void Metrics_CounterEvents_Increment()
        {
            var metricsEvent = new MetricEvent("program", "source");

            var counter = metricsEvent.AddCounter("Test");
            counter.Increment(1);
            counter.Increment(2);

            var message = metricsEvent.Serialize();

            Assert.AreEqual(1, message.DataPoints.Count);
            var counterMessage = message.DataPoints.First();

            Assert.AreEqual("3", counterMessage.Value);
            Assert.AreEqual(0, counterMessage.Samples);
        }

        [TestMethod]
        public void Metrics_TimerEvents()
        {
            var metricsEvent = new MetricEvent("program", "source");

            using (var counter = metricsEvent.StartTimer("Test"))
            {
                Thread.Sleep(1000);
            }

            var message = metricsEvent.Serialize();

            Assert.AreEqual(1, message.DataPoints.Count);
            var timerMessage = message.DataPoints.First();

            Assert.IsTrue(double.Parse(timerMessage.Value) >= 1.0);
            Assert.AreEqual(1, timerMessage.Samples);
        }
    }
}

