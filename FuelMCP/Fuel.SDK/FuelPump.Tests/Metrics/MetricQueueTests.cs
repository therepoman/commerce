﻿using FuelPump.Metrics;
using FuelPump.Services.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    [TestClass]
    public class MetricQueueTests
    {
        private class MockMetricTransport : IMetricTransport
        {
            public List<IEnumerable<MetricEvent>> MetricBatches { get; private set; }
            private object m_lock = new object();

            public MockMetricTransport()
            {
                MetricBatches = new List<IEnumerable<MetricEvent>>();
            }

            public Task TransmitMetricBatch(IEnumerable<MetricEvent> metricEvents)
            {
                lock (m_lock)
                {
                    MetricBatches.Add(metricEvents);
                }
                return Task.FromResult(true);
            }

            public int GetEventCount()
            {
                return MetricBatches.Select(batch => batch.Count()).Sum();
            }
        }

        [TestMethod]
        public void Metrics_Queue_NoTransmission()
        {
            var transport = new MockMetricTransport();

            var metricsEvent = new MetricEvent("program", "source");

            var queue = new MetricQueue(createConfig(10, TimeSpan.Zero), transport);
            queue.RecordEvent(metricsEvent);

            Assert.AreEqual(0, transport.MetricBatches.Count);
        }

        [TestMethod]
        public void Metrics_Queue_MultipleTransmissions()
        {
            var transport = new MockMetricTransport();

            var metricsEvent = new MetricEvent("program", "source");

            var queue = new MetricQueue(createConfig(2, TimeSpan.Zero), transport);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);

            queue.Flush();

            Assert.AreEqual(4, transport.GetEventCount());
        }

        [TestMethod]
        public void Metrics_Queue_PartialTransmissions()
        {
            var transport = new MockMetricTransport();

            var metricsEvent = new MetricEvent("program", "source");

            var queue = new MetricQueue(createConfig(2, TimeSpan.Zero), transport);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);

            queue.Flush();

            Assert.AreEqual(3, transport.GetEventCount());
        }

        [TestMethod]
        public void Metrics_Queue_LargeTransmissions()
        {
            var transport = new MockMetricTransport();

            var metricsEvent = new MetricEvent("program", "source");

            var queue = new MetricQueue(createConfig(2, TimeSpan.Zero), transport);

            int batchSize = 1000;
            int batches = 100;
            var tasks = Enumerable.Range(0, batches).Select(_ =>
            {
                return Task.Run(() =>
                {
                    for (int i = 0; i < batchSize; i++)
                    {
                        queue.RecordEvent(metricsEvent);
                    }
                });
            });

            Task.WaitAll(tasks.ToArray());
            queue.Flush();

            Assert.AreEqual(batchSize * batches, transport.GetEventCount());
        }

        [TestMethod]
        public void Metrics_Queue_TimedTransmission()
        {
            var transport = new MockMetricTransport();

            var metricsEvent = new MetricEvent("program", "source");

            var queue = new MetricQueue(createConfig(100, TimeSpan.FromMilliseconds(500)), transport);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);

            Thread.Sleep(1000);

            Assert.AreEqual(4, transport.GetEventCount());
        }

        [TestMethod]
        public void Metrics_Queue_LargeTimedTransmissions()
        {
            int batchSize = 100;
            int batches = 100;

            var transport = new MockMetricTransport();
            var metricsEvent = new MetricEvent("program", "source");
            using (var queue = new MetricQueue(createConfig(2, TimeSpan.FromMilliseconds(100)), transport))
            {
                var tasks = Enumerable.Range(0, batches).Select(_ =>
                {
                    return Task.Run(() =>
                    {
                        for (int i = 0; i < batchSize; i++)
                        {
                            queue.RecordEvent(metricsEvent);
                            Thread.Sleep(1);
                        }
                    });
                });
                Task.WaitAll(tasks.ToArray());

                queue.Flush();
            }

            Assert.AreEqual(batchSize * batches, transport.GetEventCount());
        }

        private ConfigHandler createConfig(int maxQueueSize, TimeSpan transmissionInterval)
        {
            var config = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(Properties.Resources.Config1));
            config.metricsMaxQueueSize = maxQueueSize;
            config.metricsTransmissionIntervalSeconds = transmissionInterval.TotalSeconds;
            config.metrics.enabled = true;

            var mock = new Mock<ConfigHandler>(new Lazy<MetricEventFactory>(() => new MockMetricsFactory().Factory.Object));
            mock.Setup(configHandler => configHandler.GetConfig()).Returns(config);
            mock.Setup(configHandler => configHandler.TryGetConfig()).Returns(config);
            mock.Protected().Setup<string>("GetBootstrapConfig").Returns(Encoding.UTF8.GetString(Properties.Resources.Config1));

            return mock.Object;
        }
    }
}
