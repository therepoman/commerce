﻿using FuelPump.Metrics;
using FuelPump.Monitoring;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        /// <summary>
        /// Simulates a simple start and stop to the FuelPump to test an end to end call.
        /// </summary>
        [TestMethod]
        [TestCategory("Integration")]
        [Timeout(60000)]
        [DeploymentItem("OffscreenBrowser.dll")]
        [DeploymentItem("LibCef.dll")]
        public void RequestResponseTest()
        {
            var metricsQueue = new Mock<FuelPump.Metrics.IMetricQueue>();
            var monitoring = new Mock<Monitoring.IMonitoringProvider>();

            // setup write/read pipes
            using (var writePipe = new AnonymousPipeServerStream(PipeDirection.Out))
            using (var readPipe = new AnonymousPipeServerStream(PipeDirection.In))
            {
                var modules = new NinjectModule[] 
                {
                    new Activation.TransportModule(
                        writePipe.GetClientHandleAsString(), 
                        readPipe.GetClientHandleAsString()),
                    new Activation.DependencyModule()
                };

                using (var kernel = new Ninject.StandardKernel(modules))
                {
                    kernel.Unbind<IMetricQueue>();
                    kernel.Unbind<IMonitoringProvider>();
                    kernel.Bind<IMetricQueue>().ToConstant(metricsQueue.Object).InSingletonScope();
                    kernel.Bind<IMonitoringProvider>().ToConstant(monitoring.Object).InSingletonScope();

                    using (var program = kernel.Get<MessagePump>())
                    {
                        var result = program.Start();

                        // send the initialize message
                        {
                            string requestId = "1337";
                            var header = CreateHeader(requestId, (int)Fuel.Messaging.Message.InitializeRequest);
                            var request = CreateInitializeRequest();

                            writePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                            writePipe.Write(header, 0, header.Length);
                            writePipe.Write(BitConverter.GetBytes(request.Length), 0, 4);
                            writePipe.Write(request, 0, request.Length);
                        }

                        // read the initialize message
                        using (var reader = new BinaryReader(readPipe, Encoding.Default, true))
                        {
                            int headerSize = reader.ReadInt32();
                            byte[] headerData = new byte[headerSize];
                            reader.Read(headerData, 0, headerSize);

                            int messageSize = reader.ReadInt32();
                            byte[] messageData = new byte[messageSize];
                            reader.Read(messageData, 0, messageSize);

                            var responseHeader = Fuel.Messaging.MessageHeader.GetRootAsMessageHeader(new FlatBuffers.ByteBuffer(headerData));
                            var messageRoot = Fuel.Messaging.MessageRoot.GetRootAsMessageRoot(new FlatBuffers.ByteBuffer(messageData));
                            var responsePayload = messageRoot.GetMessage<Fuel.Messaging.InitializeResponse>(new Fuel.Messaging.InitializeResponse());

                            Assert.AreEqual("1337", responseHeader.RequestId);
                            Assert.AreEqual((int)Fuel.Messaging.Message.InitializeResponse, responseHeader.Type);
                            Assert.IsNotNull(responsePayload);
                        }

                        // setup the shutdown message
                        {
                            string requestId = "1338";
                            var header = CreateHeader(requestId, (int)Fuel.Messaging.Message.ShutdownRequest);
                            var request = CreateShutdownRequest();

                            // send the shutdown message
                            writePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                            writePipe.Write(header, 0, header.Length);
                            writePipe.Write(BitConverter.GetBytes(request.Length), 0, 4);
                            writePipe.Write(request, 0, request.Length);
                        }

                        result.Wait();
                    }
                }
            }
        }

        private byte[] CreateHeader(string requestId, int type)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);
            var header = Messages.MessageHeader.CreateMessageHeader(builder, builder.CreateString(requestId), type);

            builder.Finish(header.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateInitializeRequest()
        { 
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var request = Messages.InitializeRequest.CreateInitializeRequest(builder,
                builder.CreateString("1.0"),
                builder.CreateString("1.0"));

            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.InitializeRequest, request.Value);
            
            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateShutdownRequest()
        { 
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            Messages.ShutdownRequest.StartShutdownRequest(builder);
            var request = Messages.ShutdownRequest.EndShutdownRequest(builder);

            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.ShutdownRequest, request.Value);
            
            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }
    }
}
