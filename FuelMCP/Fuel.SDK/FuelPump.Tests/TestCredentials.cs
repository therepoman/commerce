﻿using FuelPump.Providers.Credentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    class TestCredentials
    {
        public static readonly string twitchToken = "twitch oauth token";
        public static readonly string twitchName = "twitch";
        public static readonly string twitchIdentifier = "tuid";
        public static readonly BearerCredentials twitchCredentials = new BearerCredentials(twitchName, twitchIdentifier, twitchToken);

        public static readonly string amazonRefreshToken = "Amazon oauth bearer token";
        public static readonly AmazonCredentials amazonCredentials = new AmazonCredentials(amazonRefreshToken);
    }
}
