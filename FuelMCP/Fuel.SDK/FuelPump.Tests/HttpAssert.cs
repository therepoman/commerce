﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    public static class HttpAssert
    {
        public static void HasHeader(HttpRequestMessage request, string name, string expectedValue, string message = null)
        {
            IEnumerable<string> actualValues = null;
            if (request.Content != null)
            {
                request.Content.Headers.TryGetValues(name, out actualValues);
            }

            if (actualValues == null)
            {
                request.Headers.TryGetValues(name, out actualValues);
            }

            if (message == null) 
            {
                Assert.IsNotNull(actualValues);
                Assert.AreEqual(1, actualValues.Count());
                Assert.AreEqual(expectedValue, actualValues.First(), message);
            }
            else 
            {
                Assert.IsNotNull(actualValues, message);
                Assert.AreEqual(1, actualValues.Count(), message);
                Assert.AreEqual(expectedValue, actualValues.First(), message);
            }
        }

        public static void HasHeader(HttpRequestMessage request, string name, string message = null)
        {
            IEnumerable<string> actualValues = null;
            if (request.Content != null)
            {
                request.Content.Headers.TryGetValues(name, out actualValues);
            }

            if (actualValues == null)
            {
                request.Headers.TryGetValues(name, out actualValues);
            }

            if (message == null) 
            {
                Assert.IsNotNull(actualValues);
            }
            else 
            {
                Assert.IsNotNull(actualValues, message);
            }
        }
        
        public static void HasNoHeader(HttpRequestMessage request, string name, string message = null)
        {
            IEnumerable<string> actualValues = null;
            if (request.Content != null)
            {
                request.Content.Headers.TryGetValues(name, out actualValues);
            }

            if (actualValues == null)
            {
                request.Headers.TryGetValues(name, out actualValues);
            }

            if (message == null) 
            {
                Assert.IsNull(actualValues);
            }
            else 
            {
                Assert.IsNull(actualValues, message);
            }
        }
    }
}
