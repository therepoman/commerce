﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Tests
{
    [TestClass]
    public class MessageTransportTests
    {
        [TestMethod]
        public async Task MessageTransport_Read_Successful()
        {
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // write the raw data to the pipe
                var header = CreateHeader("1337");
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });

                // send the GetProductData message
                serverWritePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                serverWritePipe.Write(header, 0, header.Length);
                serverWritePipe.Write(BitConverter.GetBytes(request.Length), 0, 4);
                serverWritePipe.Write(request, 0, request.Length);
                
                // start the read
                var readTask = transport.ReadMessage(CancellationToken.None);

                // wait and verify the results
                var message = await readTask;
                var payload = message.GetMessage<Fuel.Messaging.GetProductDataRequest>();

                Assert.AreEqual("1337", message.RequestId);
                Assert.AreEqual(1, payload.SkusLength);
                Assert.AreEqual("ABC123", payload.GetSkus(0));
            });
        }

        [TestMethod]
        [ExpectedException(typeof(EndOfStreamException))]
        public async Task MessageTransport_Read_PipeClosed_Exception()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // kill the server pipe
                Task.Run(() =>
                {
                    Thread.Sleep(100);
                    serverWritePipe.Close();
                });

                // start the read
                var readTask = transport.ReadMessage(CancellationToken.None);

                // wait and verify the results
                await readTask;
            });
        }

        [TestMethod]
        public async Task MessageTransport_Read_PipeClosed_Disposed()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // kill the server pipe
                Task.Run(() =>
                {
                    Thread.Sleep(100);
                    serverWritePipe.Close();
                });

                // start the read
                var readTask = transport.ReadMessage(CancellationToken.None);

                // wait and verify the results
                try
                {
                    await readTask;
                }
                catch { } // ignore exception

                Assert.IsTrue(transport.IsDisposed());
            });
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public async Task MessageTransport_Read_OnDisposed()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // dispose the transport
                transport.Dispose();

                // start the read
                await transport.ReadMessage(CancellationToken.None);
            });
        }

        [TestMethod]
        [ExpectedException(typeof(OperationCanceledException))]
        public async Task MessageTransport_Read_CancelOnHeaderSize()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // cancel the read operation
                var cancellationToken = new CancellationTokenSource();
                cancellationToken.Cancel();

                // start the read
                var readTask = transport.ReadMessage(cancellationToken.Token);

                // verify results
                await readTask;
            });
        }

        [TestMethod]
        [ExpectedException(typeof(OperationCanceledException))]
        public async Task MessageTransport_Read_CancelOnHeader()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // write the raw data to the pipe
                string requestId = "1337";
                var header = CreateHeader(requestId);
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });

                // send the GetProductData message
                serverWritePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);

                // cancel after a second
                var cancellationToken = new CancellationTokenSource();
                cancellationToken.CancelAfter(100);

                // start the read
                var readTask = transport.ReadMessage(cancellationToken.Token);

                // verify results
                await readTask;
            });
        }

        [TestMethod]
        [ExpectedException(typeof(OperationCanceledException))]
        public async Task MessageTransport_Read_CancelOnPayloadSize()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // write the raw data to the pipe
                string requestId = "1337";
                var header = CreateHeader(requestId);
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });

                // send the GetProductData message
                serverWritePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                serverWritePipe.Write(header, 0, header.Length);

                // cancel after a second
                var cancellationToken = new CancellationTokenSource();
                cancellationToken.CancelAfter(100);

                // start the read
                var readTask = transport.ReadMessage(cancellationToken.Token);

                // cancel the read operation
                cancellationToken.Cancel();

                // verify results
                await readTask;
            });
        }

        [TestMethod]
        [ExpectedException(typeof(OperationCanceledException))]
        public async Task MessageTransport_Read_CancelOnPayload()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // write the raw data to the pipe
                string requestId = "1337";
                var header = CreateHeader(requestId);
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });

                // send the GetProductData message
                serverWritePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                serverWritePipe.Write(header, 0, header.Length);
                serverWritePipe.Write(BitConverter.GetBytes(request.Length), 0, 4);

                var cancellationToken = new CancellationTokenSource();
                cancellationToken.CancelAfter(100);

                // start the read
                var readTask = transport.ReadMessage(cancellationToken.Token);

                // verify results
                await readTask;
            });
        }

        [TestMethod]
        [ExpectedException(typeof(MessageTooLargeException))]
        public async Task MessageTransport_Read_TooLarge()
        {
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // write the raw data to the pipe
                var header = CreateHeader("1337");
                var request = new byte[51 * 1024 * 1024];

                // send the GetProductData message
                Task.Run(() =>
                {
                    Thread.Sleep(100);
                    try
                    {
                        serverWritePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                        serverWritePipe.Write(header, 0, header.Length);
                        serverWritePipe.Write(BitConverter.GetBytes(request.Length), 0, 4);
                        serverWritePipe.Write(request, 0, request.Length);
                    }
                    catch { } // forcing the pipe closed will cause a failure, but that's not the test case
                });
                
                // start the read
                var readTask = transport.ReadMessage(CancellationToken.None);

                // wait and verify the results
                await readTask;
            });
        }
        
        [TestMethod]
        public async Task MessageTransport_Read_TooLarge_Disposed()
        {
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // write the raw data to the pipe
                var header = CreateHeader("1337");
                var request = new byte[51 * 1024 * 1024];

                // send the GetProductData message
                Task.Run(() =>
                {
                    Thread.Sleep(100);
                    try
                    {
                        serverWritePipe.Write(BitConverter.GetBytes(header.Length), 0, 4);
                        serverWritePipe.Write(header, 0, header.Length);
                        serverWritePipe.Write(BitConverter.GetBytes(request.Length), 0, 4);
                        serverWritePipe.Write(request, 0, request.Length);
                    }
                    catch { } // forcing the pipe closed will cause a failure, but that's not the test case
                });
                
                // start the read
                var readTask = transport.ReadMessage(CancellationToken.None);
                // wait and verify the results
                try
                {
                    await readTask;
                }
                catch { }

                // verify the transport is disposed
                Assert.IsTrue(transport.IsDisposed());
            });
        }


        [TestMethod]
        public async Task MessageTransport_Write_Successful()
        {
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // create the write message
                var header = CreateHeader("1337");
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });
                var message = new Message(header, request);

                // write the message
                transport.WriteMessage(message);

                // read the message from the pipe
                var pipeReader = new BinaryReader(serverReadPipe);
                var headerSize = pipeReader.ReadInt32();
                var headerData = pipeReader.ReadBytes(headerSize);
                var requestSize = pipeReader.ReadInt32();
                var requestData = pipeReader.ReadBytes(requestSize);

                // validate the header
                var resultMessage = new Message(headerData, requestData);
                var resultPayload = resultMessage.GetMessage<Messages.GetProductDataRequest>();

                Assert.AreEqual("1337", resultMessage.RequestId);
                Assert.AreEqual(1, resultPayload.SkusLength);
                Assert.AreEqual("ABC123", resultPayload.GetSkus(0));
            });
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public async Task MessageTransport_Write_PipeClosed_Exception()
        {
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // kill the server read pipe
                serverReadPipe.Close();

                // create the write message
                var header = CreateHeader("1337");
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });
                var message = new Message(header, request);

                // write the message
                transport.WriteMessage(message);
            });
        }

        [TestMethod]
        public async Task MessageTransport_Write_PipeClosed_Disposed()
        {
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // kill the server read pipe
                serverReadPipe.Close();

                // create the write message
                var header = CreateHeader("1337");
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });
                var message = new Message(header, request);

                // write the message
                try
                {
                    transport.WriteMessage(message);
                }
                catch { } // ignore exception in this test

                Assert.IsTrue(transport.IsDisposed());
            });
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public async Task MessageTransport_Write_OnDisposed()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // dispose the transport
                transport.Dispose();

                // create the write message
                var header = CreateHeader("1337");
                var request = CreateGetProductDataRequest(1, new[] { "ABC123" });
                var message = new Message(header, request);

                // start the write
                transport.WriteMessage(message);
            });
        }

        [TestMethod]
        [ExpectedException(typeof(MessageTooLargeException))]
        public async Task MessageTransport_Write_TooLarge()
        { 
            await RunTransportTest(async (serverReadPipe, serverWritePipe, transport) =>
            {
                // create the write message
                var header = CreateHeader("1337");
                var request = new byte[101 * 1024 * 1024];
                var message = new Message(header, request);

                // start the write
                transport.WriteMessage(message);
            });
        }

        private async Task RunTransportTest(Func<Stream, Stream, MessageTransport, Task> test)
        { 
            using (var writePipe = new AnonymousPipeServerStream(PipeDirection.Out))
            using (var readPipe = new AnonymousPipeServerStream(PipeDirection.In))
            using (var transport = new MessageTransport(
                    writePipe.GetClientHandleAsString(),
                    readPipe.GetClientHandleAsString()))
            {
                await test(readPipe, writePipe, transport);
            }
        }

        private byte[] CreateHeader(string requestId)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);
            var header = Messages.MessageHeader.CreateMessageHeader(builder, builder.CreateString(requestId));

            builder.Finish(header.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateGetProductDataRequest(int requestId, string[] skus)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var skusVector = Messages.GetProductDataRequest.CreateSkusVector(builder,
                skus.Select(sku => builder.CreateString(sku)).ToArray());

            var request = Messages.GetProductDataRequest.CreateGetProductDataRequest(builder, 
                Messages.CredentialsCache.CreateCredentialsCache(builder, 
                    Messages.CredentialsCache.CreateCredentialsVector(builder, new FlatBuffers.Offset<Messages.Credential>[0])),
                skusVector);
            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.GetProductDataRequest, request.Value);

            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }
    }
}
