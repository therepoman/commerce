﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Tests
{
    [TestClass]
    public class MessageTests
    {
        [TestMethod]
        public async Task MessagePump_GetMessageTypeForSplitMessage()
        {
            var payload = CreateMessage();

            int chunkSize = 1;
            var part1 = new byte[chunkSize];
            Buffer.BlockCopy(payload, 0, part1, 0, chunkSize);
            var part2 = new byte[payload.Length - chunkSize];
            Buffer.BlockCopy(payload, chunkSize, part2, 0, part2.Length);

            var message = new Message(CreateHeader("1234", (int)Fuel.Messaging.Message.ShowOverlay), part1, part2);
            Assert.AreEqual(payload.Length, message.GetPayloadLength());
            Assert.AreEqual(typeof(Fuel.Messaging.ShowOverlay), message.GetMessageType());
        }

        private byte[] CreateHeader(string requestId, int type)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);
            var header = Messages.MessageHeader.CreateMessageHeader(builder, builder.CreateString(requestId), 0, type);

            builder.Finish(header.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateMessage()
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var request = Messages.ShowOverlay.CreateShowOverlay(builder);
            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.ShowOverlay, request.Value);

            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }
    }
}
