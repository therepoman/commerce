﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using FuelPump.Providers.Credentials;
using FuelPump.Services;
using FuelPump.Services.Config;
using Moq;
using System.Threading;
using System.Net;
using Newtonsoft.Json;
using FuelPump.Metrics;
using Moq.Protected;

namespace FuelPump.Tests
{
    [TestClass]
    public class AbstractServiceClientTests
    {
        public class FakeServiceClient : AbstractServiceClient
        {
            public FakeServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler, MockHttpMessageHandler handler) : 
                base(credentialsProvider, configHandler, new HttpClient(handler)) 
            { 
            }

            public Task<int> DoWork(int data, BearerCredentials identifyingCredentials, CancellationToken cancellationToken)
            {
                var config = new ApiConfig();
                config.endpoint = "https://some.random.service.com";
                config.serviceNamespace = "namespace";
                config.serviceName = "ServiceName";
                config.operationName = "operation.test";

                return CallAsync(new RequestContext<int, int>(
                    config.operationName, 
                    1, 
                    (request) => data.ToString(),
                    (response) => 1),
                    config,
                    cancellationToken, 
                    identifyingCredentials);
            }
        }

        BearerCredentials twitchCredentials = TestCredentials.twitchCredentials;
        AmazonCredentials amazonCredentials = TestCredentials.amazonCredentials;

        private Mock<FuelConfig> mockConfig;
        private Mock<ConfigHandler> mockConfigHandler;
        private MockHttpMessageHandler m_mockHttpMessageHandler = new MockHttpMessageHandler();
        private FakeServiceClient client;

        [TestInitialize]
        public void setup()
        {
            var credentialsProvider = new Mock<AbstractCredentialsProvider>();
            credentialsProvider.Setup(x => x.GetCredentials(twitchCredentials)).Returns(new Credentials(amazonCredentials));

            mockConfigHandler = new Mock<ConfigHandler>(new Lazy<MetricEventFactory>(() => new MockMetricsFactory().Factory.Object));
            var bootstrapConfig = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config1));
            bootstrapConfig.AssertValid();
            mockConfigHandler.Protected().Setup<string>("GetBootstrapConfig").Returns(JsonConvert.SerializeObject(bootstrapConfig));
            mockConfig = new Mock<FuelConfig>();
            mockConfig.Setup(x => x.IsFeatureOn(It.IsAny<string>())).Returns(false);
            mockConfigHandler.Setup(x => x.GetConfig()).Returns(mockConfig.Object);

            client = new FakeServiceClient(credentialsProvider.Object, mockConfigHandler.Object, m_mockHttpMessageHandler);
        }

        [TestMethod]
        public async Task AbstractServiceClient_VerifyBearerCredentials()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(new byte[0])
            });

            var result = await client.DoWork(1, twitchCredentials, new CancellationToken());

            HttpAssert.HasHeader(m_mockHttpMessageHandler.Requests[0], "X-ADG-Oauth-Headers", "X-ADG-Oauth-Token-twitch");
            HttpAssert.HasHeader(m_mockHttpMessageHandler.Requests[0], "X-ADG-Oauth-Token-twitch", twitchCredentials.Token);
        }
        
        [TestMethod]
        public async Task AbstractServiceClient_VerifyAmazonTokenPresent()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(new byte[0])
            });

            mockConfig.Setup(x => x.IsFeatureOn(FuelPump.Services.Config.Features.AMZN_OAUTH)).Returns(true);

            var result = await client.DoWork(1, twitchCredentials, new CancellationToken());
        }

        [TestMethod]
        public async Task AbstractServiceClient_VerifyAmazonTokenNotPresent()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(new byte[0])
            });

            mockConfig.Setup(x => x.IsFeatureOn(FuelPump.Services.Config.Features.AMZN_OAUTH)).Returns(false);

            var result = await client.DoWork(1, twitchCredentials, new CancellationToken());
        }

        [TestMethod] 
        public async Task AbstractServiceClient_VerifyNoCreds()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(new byte[0])
            });

            var result = await client.DoWork(1, null, new CancellationToken());

            HttpAssert.HasNoHeader(m_mockHttpMessageHandler.Requests[0], "X-ADG-Oauth-Headers");
            HttpAssert.HasNoHeader(m_mockHttpMessageHandler.Requests[0], "X-ADG-Oauth-Token-twitch");
        }

        [TestMethod]
        [ExpectedException(typeof(CoralException))]
        public async Task AbstractServiceClient_CoralError()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("{ \"__type\": \"com.amazon.example.Exception\", \"Message\": \"Some example error.\"}")
            });

            var result = await client.DoWork(1, null, new CancellationToken());
        }

        [TestMethod]
        [ExpectedException(typeof(CoralException))]
        public async Task AbstractServiceClient_CoralErrorNoMessage()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("{ \"__type\": \"com.amazon.example.Exception\"}")
            });

            var result = await client.DoWork(1, null, new CancellationToken());
        }

        /// <summary>
        /// This shouldn't happen if the Coral service is behaving properly but if we hit an error from the load balancer for example it could still happen
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(CoralException))]
        public async Task AbstractServiceClient_CoralErrorEmptyBody()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("")
            });

            var result = await client.DoWork(1, null, new CancellationToken());
        }
    }
}
