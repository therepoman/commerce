﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public class MetricEvent
    {
        private Dictionary<string, DataPoint> m_dataPoints = new Dictionary<string, DataPoint>();

        public MetricEvent(string program, string source)
        {
            Program = program;
            Source = source;
        }

        public string Program { get; private set; }
        public string Source { get; private set; }

        public void SetCounter(string name, int value = 1)
        {
            var result = new CounterDataPoint(name);
            m_dataPoints.Add(name, result);
            result.Increment(value);
        }

        public virtual CounterDataPoint AddCounter(string name)
        {
            var result = new CounterDataPoint(name);
            m_dataPoints.Add(name, result);
            return result;
        }

        public MetricEventMessage Serialize()
        {
            return new MetricEventMessage
            {
                Program = this.Program,
                Source = this.Source,
                Timestamp = ((long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds / 1000L),
                DataPoints = m_dataPoints.Select(dataPoint => dataPoint.Value.Serialize()).ToList()
            };
        }

        public virtual TimerDataPoint StartTimer(string name)
        { 
            var result = new TimerDataPoint(name);
            m_dataPoints.Add(name, result);
            return result;
        }

        // appendString(s)
        // removeString
    }
}
