﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public class CounterDataPoint : DataPoint
    {
        int m_value = 0;

        internal CounterDataPoint(string name) :
            base(name)
        {
        }

        public void Increment(int amount)
        {
            m_value += amount;
        }

        public override DataPointMessage Serialize()
        {
            return new DataPointMessage
            {
                Name = this.Name,
                Type = "COUNTER",
                Value = this.m_value.ToString(),
                Samples = 0
            };
        }
    }
}
