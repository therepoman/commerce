﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public class TimerDataPoint : DataPoint, IDisposable
    {
        DateTime m_startTime;
        DateTime m_endTime;

        Boolean m_completed;

        public TimerDataPoint(string name) :
            base(name)
        {
            m_startTime = DateTime.UtcNow;
            m_completed = false;
        }

        public void Stop()
        {
            if(!m_completed)
            {      
                m_endTime = DateTime.UtcNow;
                m_completed = true;
            }
        }

        public void Dispose()
        {
            Stop();
        }

        public override DataPointMessage Serialize()
        {
            return new DataPointMessage 
            { 
                Name = this.Name,
                Type = "TIMER",
                Value = (m_endTime - m_startTime).TotalMilliseconds.ToString(),
                Samples = 1
            };
        } 
    }
}
