﻿using FuelPump.Services.Config;
using Ninject;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public class MetricQueue : Disposable, IMetricQueue
    {
        object m_transmissionLock = new object();

        Timer m_timer;
        ConfigHandler m_configHandler = null;
        IMetricTransport m_transport;

        ConcurrentQueue<MetricEvent> m_activeQueue = new ConcurrentQueue<MetricEvent>();
        TaskList m_transportTasks = new TaskList();

        public MetricQueue(ConfigHandler configHandler, IMetricTransport transport)
        {
            m_configHandler = configHandler;
            m_transport = transport;

            m_timer = new Timer(_ => TransmitBatch(), null, Timeout.Infinite, Timeout.Infinite);

            // The interval is only configured once.  It won't change until the SDK is restarted.
            // The timer is configured asynchronously to avoid a circular dependency.
            // Metrics can't be sent over the network until the config has been loaded.
            // Metrics are reported while the config is loaded.
            // This class will queue the metrics and send them later once the config has been loaded.
            Task.Run(() =>
            {
                var intervalMilliseconds = (long)(configHandler.GetConfig().metricsTransmissionIntervalSeconds * 1000.0);
                m_timer.Change(intervalMilliseconds, intervalMilliseconds);
            });
        }

        public void RecordEvent(MetricEvent metricEvent)
        {
            var config = m_configHandler != null ? m_configHandler.TryGetConfig() : null;

            if (config == null)
            {
                // Queue a limited number of events while waiting for initialization to complete.
                if (m_activeQueue.Count < 100)
                {
                    m_activeQueue.Enqueue(metricEvent);
                }
                return;
            }

            if (!config.metrics.IsEnabled())
            {
                return;
            }

            m_activeQueue.Enqueue(metricEvent);

            // locking to avoid multiple transmissions is done in the 
            // TransmitBatch method
            if (m_activeQueue.Count >= config.metricsMaxQueueSize)
            {
                TransmitBatch(() => m_activeQueue.Count >= config.metricsMaxQueueSize);
            }
        }

        public void Flush()
        {
            m_timer.Change(Timeout.Infinite, Timeout.Infinite);
            TransmitBatch();
            m_transportTasks.WaitForAll();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Flush();
            }
        }

        private void TransmitBatch(Func<bool> predicate = null)
        {
            var config = m_configHandler != null ? m_configHandler.TryGetConfig() : null;

            if (config == null || !config.metrics.IsEnabled())
            {
                return;
            }

            var batch = GetBatch(predicate);

            if (batch != null && batch.Count > 0)
            {
                m_transportTasks.AddTask(Task.Run(() =>
                {
                    m_transport.TransmitMetricBatch(batch).Wait();
                }));
            }
        }

        private List<MetricEvent> GetBatch(Func<bool> predicate = null)
        {
            lock (m_transmissionLock)
            {
                // double check the condition after the lock in case
                // another thread already transmitted the batch
                if (predicate != null && !predicate())
                {
                    return null;
                }

                MetricEvent tempEvent;
                var transmissionList = new List<MetricEvent>();
                while (m_activeQueue.TryDequeue(out tempEvent))
                {
                    transmissionList.Add(tempEvent);
                }
                return transmissionList;
            }
        }
    }
}
