﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public abstract class DataPoint
    {
        protected DataPoint(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        public abstract DataPointMessage Serialize();
    }
}