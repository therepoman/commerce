﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public class MetricEventFactory
    {
        private string m_program;
        private string m_source;
        private IMetricQueue m_queue;

        public MetricEventFactory(string program, string source, IMetricQueue queue)
        {
            m_program = program;
            m_source = source;
            m_queue = queue;
        }

        public class MetricEventScope : Disposable
        {
            IMetricQueue m_queue;

            public MetricEventScope(MetricEvent metricEvent, IMetricQueue queue)
            {
                MetricEvent = metricEvent;
                Ignore = false;
                Recorded = false;
                m_queue = queue;
            }

            public virtual MetricEvent MetricEvent { get; private set; }
            private Boolean Recorded;
            public Boolean Ignore { get; set; }

            public void RecordEvents()
            {
                if(!Recorded && !Ignore) {
                    m_queue.RecordEvent(MetricEvent);
                    Recorded = true;
                }
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    RecordEvents();
                }
            }
        }

        public virtual MetricEventScope Create()
        {
            return new MetricEventScope(new MetricEvent(m_program, m_source), m_queue);
        }

        public void AddCounter(string name)
        {
            var metric = new MetricEvent(m_program, m_source);
            metric.AddCounter(name).Increment(1);

            m_queue.RecordEvent(metric);
        }

        public void FlushMetrics()
        {
            m_queue.Flush();
        }
    }
}
