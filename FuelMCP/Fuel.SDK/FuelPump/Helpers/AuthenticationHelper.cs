﻿using Amazon.Auth.Map;
using Amazon.Auth.Map.Services;
using Amazon.Auth.Map.Services.Panda;
using FuelPump.Overlay;
using FuelPump.Providers.Credentials;
using FuelPump.Services.Config;
using Messages = Fuel.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Org.BouncyCastle.Security;
using System.IO;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto;
using System.Runtime.Remoting;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Globalization;
using Microsoft.CSharp.RuntimeBinder;

using Microsoft.CSharp.RuntimeBinder;
using System.Web;

namespace FuelPump.Helpers
{
    public class AuthenticationHelper
    {
        public class CblCancelledException : ApplicationException
        {
            public CblCancelledException() : base("User cancelled the CBL process.") { }
        }

        public class UnlinkedAccountsException : ApplicationException
        {
            public UnlinkedAccountsException() : base("The Twitch and Amazon accounts provided are not linked.") { }
        }

        public class AuthenticationFailedException : ApplicationException
        {
            public AuthenticationFailedException(string message) : base("Authentication failed during " + message) { }
        }

        public class TwitchAuthenticationFailedException : AuthenticationFailedException
        {
            public TwitchAuthenticationFailedException() : base("Twitch Authentication") { }
        }

        public class AmazonAuthenticationFailedException : AuthenticationFailedException
        {
            public AmazonAuthenticationFailedException() : base("Amazon Authentication") { }
            public AmazonAuthenticationFailedException(string message) : base("Amazon Authentication : " + message) { }
        }

        protected AbstractCredentialsProvider CredentialsProvider;
        protected ConfigHandler configHandler;
        protected PandaService pandaService;
        protected AppInfo appInfo;

        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(AuthenticationHelper));

        // Auth keys
        private static readonly string APP_NAME_KEY = "app_name";
        private static readonly string APP_VERSION_KEY = "app_version";
        private static readonly string SOURCE_TOKEN_KEY = "source_token";
        private static readonly string SOURCE_TOKEN_TYPE_KEY = "source_token_type";
        private static readonly string REQUESTED_TOKEN_TYPE_KEY = "requested_token_type";
        private static readonly string DOMAIN_KEY = "domain";

        // Auth key values.
        private static readonly string DMS_TOKEN_TYPE = "dms_token";
        private static readonly string REFRESH_TOKEN_TYPE = "refresh_token";
        private static readonly string AUTH_COOKIES_TYPE = "auth_cookies";

        private static readonly string NO_BODY = "";

        public AuthenticationHelper(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler, PandaService pandaService)
        {
            CredentialsProvider = credentialsProvider;
            this.configHandler = configHandler;
            this.pandaService = pandaService;
            appInfo = new AppInfo();
        }

        public async Task<AmazonCredentials> GetAmazonCredentials(CefInstance instance, BearerCredentials identifyingCredentials)
        {
            // This is created early so that events aren't missed if they happen before the main flow
            // finishes setting up everything, and the overlay will be able to react immediately as long
            // as all the slow work is done using CefInstance.WrapPageLoad.
            var overlayEvents = instance.RegisterOverlayEvents();

            try
            {
                // Get credentials from the cred store, if there are any.
                var userCredentials = CredentialsProvider.GetCredentials(identifyingCredentials);
                return userCredentials.AmazonCredentials;
            }
            catch (CredentialsNotFoundException) { }

            // Add device info and twitchId to the HttpHeader
            Dictionary<string, string> deviceInfo = new Dictionary<string, string>();
            deviceInfo.Add("domain", "Device");
            deviceInfo.Add("device_id", appInfo.DeviceName);
            deviceInfo.Add("app_name", appInfo.Name);
            deviceInfo.Add("app_version", appInfo.Version);
            deviceInfo.Add("device_model", appInfo.DeviceModel);
            deviceInfo.Add("os_version", appInfo.OsVersion);
            deviceInfo.Add("device_type", appInfo.DeviceTypeId);
            deviceInfo.Add("device_serial", appInfo.DeviceSerialId);

            var authTask = instance.WaitForAllEvents(CefInstance.AUTH_EVENT_NAME);
            for (int i = 0; i <= configHandler.GetConfig().urlPathBlacklistMaxRedirects && !authTask.IsCompleted; i++)
            {
                Task onBlacklistedUrl = instance.WaitForBlacklistedUrl();

                // Credentials not found, starting CBL workflow
                instance.Navigate(configHandler.GetConfig().cblPage.getUri("regData=" + HttpUtility.UrlEncode(JsonConvert.SerializeObject(deviceInfo))));

                var completedTask = await Task.WhenAny(authTask, onBlacklistedUrl);
                if (completedTask == onBlacklistedUrl)
                {
                    LOG.Info("A blacklisted URL was requested.  Reloading CBL landing page instead.");
                }
            }

            if (!authTask.IsCompleted)
            {
                throw new AmazonAuthenticationFailedException("BadURL");
            }

            CefResult[] result;
            try
            {
                result = await CefInstance.WrapPageLoad(authTask, overlayEvents);
            }
            catch (OperationCanceledException)
            {
                throw new CblCancelledException();
            }
            catch (Exception)
            {
                throw new AmazonAuthenticationFailedException("CBL");
            }

            if (result != null && result[0].EventName == CefInstance.AUTH_EVENT_NAME)
            {
                var authJson = result[0].EventData;
                dynamic data = JsonConvert.DeserializeObject<dynamic>(authJson);
                AmazonCredentials amazonCredentials = new AmazonCredentials(Convert.ToString(data.refreshToken));
                CredentialsProvider.SetCredentials(identifyingCredentials, new Credentials(amazonCredentials));

                return amazonCredentials;
            } else
            {
                throw new AmazonAuthenticationFailedException("CBL");
            }
        }

        public async Task InjectAmazonCookies (CefInstance cefInstance, AmazonCredentials amazonCredentials) 
        {
            var cookieUrl = configHandler.GetConfig().cookie.getUri();

            try
            {
                List<Cookie> cookies = await GetAuthCookies(amazonCredentials.RefreshToken);
                foreach (var cookie in cookies)
                {
                    if (!cefInstance.SetCookie(cookieUrl, cookie.Name, cookie.Value, cookie.Domain, cookie.Path, cookie.Secure, cookie.HttpOnly, cookie.Expires))
                    {
                        throw new AmazonAuthenticationFailedException("SetCookie");
                    }
                }
            } catch (AmazonAuthenticationFailedException e)
            {
                throw e;
            }

            return;
        }

        public async Task<List<Cookie>> GetAuthCookies(string refreshToken)
        {
            List<Cookie> authCookies = new List<Cookie>();
            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { APP_NAME_KEY,  appInfo.Name },
                    { APP_VERSION_KEY, appInfo.Version },
                    { SOURCE_TOKEN_KEY, refreshToken },
                    { SOURCE_TOKEN_TYPE_KEY, REFRESH_TOKEN_TYPE },
                    { REQUESTED_TOKEN_TYPE_KEY, AUTH_COOKIES_TYPE },
                    { DOMAIN_KEY, configHandler.GetConfig().cookiesDomain }
                };

                var content = new FormUrlEncodedContent(values);
                var tokenExchangeUrl = configHandler.GetConfig().tokenExchange.getUri();
                var response = await client.PostAsync(tokenExchangeUrl, content);
                var responseString = await response.Content.ReadAsStringAsync();

                try {
                    dynamic data = JsonConvert.DeserializeObject<dynamic>(responseString);
                    JObject cookies = data.response.tokens.cookies;

                    foreach (var cookie in cookies)
                    {
                        string domain = cookie.Key;
                        JToken cookieArray = cookie.Value;

                        foreach (var cookieValue in cookieArray)
                        {
                            Cookie authCookie = new Cookie((string)cookieValue["Name"], (string)cookieValue["Value"], (string)cookieValue["Path"], domain);
                            authCookie.Expires = DateTime.Parse((string)cookieValue["Expires"], new CultureInfo("en-US")).ToUniversalTime();
                            authCookie.Secure = (Boolean)cookieValue["Secure"];
                            authCookie.HttpOnly = (Boolean)cookieValue["HttpOnly"];
                            authCookies.Add(authCookie);
                        }
                    }

                    return authCookies;
                }
                catch (RuntimeBinderException)
                {
                    throw new AmazonAuthenticationFailedException("The refresh token could not be exchanged for authentication cookies.");
                }
            }
        }

        public async Task<Boolean> ClearCredentialsCacheIfCredentialsAreBad(BearerCredentials identifyingCredentials, AmazonCredentials amazonCredentials)
        {
            try
            {
                await GetAuthCookies(amazonCredentials.RefreshToken);
                return false;
            } catch (AmazonAuthenticationFailedException)
            {
                ClearCredentialsCache(identifyingCredentials);
                return true;
            }
        }

        /* 
         * Clears the local store of the provided identifying credentials. This will restart the CBL workflow for the user to whom these credentials belong.
         */
        public void ClearCredentialsCache(BearerCredentials identifyingCredentials)
        {
            CredentialsProvider.RemoveCredentials(identifyingCredentials);
            LOG.Info("Removed locally stored credentials.");
            return;
        }

        public async Task Deregister(AmazonCredentials amazonCredentials, BearerCredentials identifyingCredentials)
        {
            string accessToken = null;
            var refreshToken = amazonCredentials.RefreshToken;
            try
            {
                var response = await pandaService.RefreshAccessTokenAsync(new AmazonRefreshToken(refreshToken));
                accessToken = response.access_token;
            }
            catch (Exception ex)
            {
                LOG.Error("Failed to refresh access token.", ex);
            }

            if (accessToken != null)
            {
                try
                {
                    var response = await pandaService.DeregisterDeviceAsync(new AmazonAccessToken(accessToken));
                    if (!response.IsSuccessStatusCode)
                    {
                        LOG.Error("Failed to deregister.");
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("Failed to deregister.", ex);
                }
            }

            // The token was successfully deregistered, or it is bad, or the service is temporarily down.
            // Either way, removing the locally stored credentials will restart the CBL workflow.
            ClearCredentialsCache(identifyingCredentials);
        }
    }
}
