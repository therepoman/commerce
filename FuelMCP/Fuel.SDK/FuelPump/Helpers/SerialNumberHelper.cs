﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Amazon.Auth.Map;

namespace FuelPump.Helpers
{
    /**
     * Serial number generation code mostly stolen from CloudDriveDesktop logic.
     * The gross copy/paste is justified here because we don't want to inherit
     * from Cloud Drive logic only to have our serial number formats messed with 
     * if they change their serial number generating logic.
     * 
     * All of this is very much Windows-only.
    */
    class SerialNumberHelper
    {
        private static string GetSerialNumberPath() { return Path.Combine(new AppInfo().LocalStoragePath, "serial-number"); }

        private static string GetStoredSerialNumber()
        {
            var serialNumberPath = GetSerialNumberPath();

            try
            {
                if (File.Exists(serialNumberPath)) return File.ReadAllText(serialNumberPath);
            }
            catch { }

            return String.Empty;
        }

        private static void StoreSerialNumber(string serialNumber)
        {
            serialNumber = serialNumber.Replace("-", "").ToLowerInvariant();
            var serialNumberPath = GetSerialNumberPath();

            try
            {
                if (File.Exists(serialNumberPath))
                    File.Delete(serialNumberPath);

                File.WriteAllText(serialNumberPath, serialNumber);
            }
            catch { }
        }


        public static string GetSerialNumber()
        {
            var serialNumber = GetStoredSerialNumber();

            // try to retrieve the statically stored device id
            if (!String.IsNullOrWhiteSpace(serialNumber) && serialNumber.Length == 32)
                return serialNumber;

            try
            {
                // hardware UUID is set by motherboard manufacturer
                serialNumber = Amazon.Auth.Map.Windows.DeviceInfo.HardwareUuid;
            }
            catch { }

            try
            {
                // if that fails we try to use the main disk serial number
                if (String.IsNullOrEmpty(serialNumber))
                    serialNumber = Amazon.Auth.Map.Windows.DeviceInfo.MainDiskSerialNumber.ToHashString();
            }
            catch { }

            try
            {
                // if that fails we use the machine GUID which is used for cryptography
                if (String.IsNullOrEmpty(serialNumber))
                    serialNumber = Amazon.Auth.Map.Windows.DeviceInfo.MachineGuid;
            }
            catch { }

            // if everything else fails then we create a random GUID
            if (String.IsNullOrEmpty(serialNumber))
                serialNumber = Guid.NewGuid().ToString("n");

            // we combine that with the OS username so that the multiple accounts can signin on the same computer
            serialNumber = (System.Environment.UserName + serialNumber).ToHashString();

            // then we store that for quick retrieval
            StoreSerialNumber(serialNumber);

            return serialNumber;
        }
    }

}

