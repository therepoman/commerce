﻿using FuelPump.Services;
using FuelPump.Services.Config;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace FuelPump.Helpers
{
    public class ValidationHelper
    {
        // Max length is currently 40 bytes.  https://w.amazon.com/index.php/OLS/Manual/SKU
        // Setting this to 100 characters so that it will reject values that are clearly incorrect and it's unlikely to need to be changed.
        private static readonly int MAX_SKU_LENGTH = 100;

        private ConfigHandler configHandler;

        public ValidationHelper(ConfigHandler configHandler)
        {
            this.configHandler = configHandler;
        }

        public bool IsSkuValid(string sku)
        {
            if (sku.Length > MAX_SKU_LENGTH)
            {
                return false;
            }

            // https://w.amazon.com/index.php/OLS/Manual/SKU SKUs don't appear to have any real restrictions other than length and it's parsable as UTF-8.
            // getProductDetails and purchase both handle UTF-8 and will properly encode stuff like newlines, but truncate strings at the first null.
            return true;
        }
    }
}
