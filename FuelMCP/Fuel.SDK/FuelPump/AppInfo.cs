﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using FuelPump.Helpers;

using Amazon.Auth.Map;

namespace FuelPump
{
    // AppInfo contains configuration information that may be system-dependent or require processing.  Constant values
    // or OS-independent data goes in Constants.cs.
    public class AppInfo : IAppInfo, Amazon.Diagnostics.Spectator.IAppInfo
    {
        private static readonly Lazy<string> SerialNumber;

        public string HttpUserAgentName { get { return Constants.InternalAppName; } }
        public string Name { get { return Constants.AppName; } }
        public bool RegisterDeviceNameWithPanda { get { return false; } }

        static AppInfo()
        {
            SerialNumber = new Lazy<string>(SerialNumberHelper.GetSerialNumber, LazyThreadSafetyMode.ExecutionAndPublication);
        }

        public string DeviceName { 
            get 
            { 
#if WIN
                return System.Environment.MachineName; 
#endif
            } 
        }

        public string DeviceSerialId {
            get
            {
                return SerialNumberHelper.GetSerialNumber();
            }
        }

        public string DefaultLocalStoragePath
        {
            get
            { 
#if WIN
                return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), Constants.AppName);
#endif
            }
        }

        public string OverrideLocalStoragePath { get; set; }

        public string LocalStoragePath
        {
            get
            {
                // Use defaults if unset.
                return OverrideLocalStoragePath ?? DefaultLocalStoragePath;
            }
        }

        public string Version 
        { 
            get 
            {
                return typeof(Program).Assembly.GetName().Version.ToString();
            } 
        }

        public string UserAgent
        {
            get
            {
                return "FuelSDK/release-" + Version;
            }
        }

        public string DeviceTypeId { 
            get 
            { 
#if WIN
                return Constants.WindowsDeviceType; 
#endif
            } 
        }

        public string UserName
        {
            get { return System.Environment.UserName; }
        }

        public string DeviceModel
        {
            get
            {
#if WIN
                return Constants.WindowsDeviceModel;
#endif
            }
        }

        public string OsVersion
        {
            get
            {
                return System.Environment.OSVersion.Version.ToString();
            }
        }

        public string SpectatorName
        {
            get
            {
                return "tv.twitch.fuel.win.sdk";
            }
        }

        public string PmetDomain
        {
            get
            {
                return "Fuel";
            }
        }

        public string PmetSource
        {
            get
            {
                return "FuelSdk";
            }
        }
    }
}
