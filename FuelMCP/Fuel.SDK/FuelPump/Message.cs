﻿using Fuel.Messaging;
using System.Diagnostics;
using System;
using System.IO;

namespace FuelPump
{
    public class Message
    {
        MessageRoot m_message = null;
        MessageHeader m_messageHeader;
        byte[][] m_buffers;
        byte[] m_headerBuffer;

        public Message(byte[] headerData, params byte[][] data)
        {
            m_buffers = data;
            m_headerBuffer = headerData;
            m_messageHeader = MessageHeader.GetRootAsMessageHeader(new FlatBuffers.ByteBuffer(headerData));
            if (m_buffers.Length == 1)
            {
                m_message = MessageRoot.GetRootAsMessageRoot(new FlatBuffers.ByteBuffer(m_buffers[0]));
            }
        }

        public int GetPayloadLength() 
        {
            int length = 0;
            foreach (byte[] buffer in m_buffers)
            {
                length += buffer.Length;
            }
            return length;
        }

        public void WritePayload(Stream stream)
        {
            foreach (byte[] buffer in m_buffers)
            {
                stream.Write(buffer, 0, buffer.Length);
            }
        }

        public byte[] GetHeaderBuffer()
        {
            return m_headerBuffer;
        }

        public int Retry
        {
            get { return m_messageHeader.Retry; }
        }

        public string RequestId 
        {
            get { return m_messageHeader.RequestId; } 
        }

        /// <summary>
        /// Interprets the payload as a flatbuffer, with a side-effect of internally concatenating it if needed,
        /// and returns the embedded message type from it.
        /// </summary>
        /// <remarks>
        /// If the message payload was passed to the constructor as multiple chunks, this method will cause them
        /// to internally be concatenated on the first call.  At the time this was added, multi-part messages are
        /// only created for the purpose of efficient writing to the transport stream and GetMessageType isn't called.
        /// GetMessageType is called on single-part messages that have been read from the transport stream so they
        /// can be routed correctly.
        /// </remarks>
        /// <returns></returns>
        public Type GetMessageType()
        {
            // Need to combine payloads for flat buffers to work.
            if (m_message == null)
            {
                byte[] concatenatedPayload = new byte[GetPayloadLength()];
                int index = 0;
                foreach (byte[] buffer in m_buffers)
                {
                    Buffer.BlockCopy(buffer, 0, concatenatedPayload, index, buffer.Length);
                    index += buffer.Length;
                }
                m_buffers = new byte[1][];
                m_buffers[0] = concatenatedPayload;
                m_message = MessageRoot.GetRootAsMessageRoot(new FlatBuffers.ByteBuffer(m_buffers[0]));
            }

            // TODO: [FUEL-812] iterate the enum values and put results in a dictionary for faster lookup
            return Type.GetType("Fuel.Messaging." + m_message.MessageType);
        }

        public TMessage GetMessage<TMessage>() where TMessage : FlatBuffers.Table, new()
        {
            Debug.Assert(typeof(TMessage) == GetMessageType());

            return m_message.GetMessage<TMessage>(new TMessage());
        }
    }
}
