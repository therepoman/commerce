﻿using System;
using System.Text.RegularExpressions;

namespace FuelPump.Services.Config
{
    public class ApiConfig
    {
        public static readonly Regex ADD_PORT_REGEX = new Regex("^([^:]+://[^/?]+)(?<!:[0-9]+)([/?].*)?$");
        private static readonly int MIN_RETRIES = 0;
        private static readonly int MAX_RETRIES = 10;
        private static readonly int MIN_RETRY_DELAY_SECONDS = 0;
        private static readonly double MIN_TIMEOUT_SECONDS = 0.5;

        public class DisabledOperationException : ApplicationException
        {
            public DisabledOperationException(string message) : base(message)
            {
            }
        }

        // Add new fields to the merge below and validation if needed.  Also update FuelConfigTests and the test json files.
        public bool? enabled { private get; set; }
        public string endpoint { get; set; }
        public string operationName { get; set; }
        public string path { get; set; }
        public int? retries { get; set; }
        public double? retryDelaySeconds { get; set; }
        public string serviceName { get; set; }
        public string serviceNamespace { get; set; }
        public double? timeoutSeconds { get; set; }
        // Add new fields to the merge below and validation if needed.  Also update FuelConfigTests and the test json files.

        /// <summary>
        /// Interprets a null enabled field as true since it's unlikely for an API to be disabled.
        /// </summary>
        public bool IsEnabled()
        {
            return enabled ?? true;
        }

        /// <summary>
        /// Validates the endpoint and path, combines them with the optional query parameters, and returns the result as string.
        /// </summary>
        /// <exception cref="InvalidConfigurationException"/>
        public string getUri(string query = null)
        {
            try
            {
                var builder = new UriBuilder(endpoint);
                builder.Path = path;
                if (builder.Scheme != "https")
                {
                    throw new InvalidConfigurationException("Unsupported scheme '" + builder.Scheme + "'");
                }

                if (query != null)
                {
                    builder.Query = query;
                }

                var result = builder.Uri.ToString();

                // development.amazon.com requires :443 to be part of the url even though that is the default
                // port for https.  Without a port, it returns 403 forbidden for the token exchange.
                // UriBuilder/Uri automatically strips the default port, so it has to be re-added afterward.
                if (endpoint.EndsWith(":443"))
                {
                    var match = ADD_PORT_REGEX.Match(result);
                    if (match.Success)
                    {
                        result = match.Groups[1] + ":443" + match.Groups[2];
                    }
                }
                return result;
            }
            catch (InvalidConfigurationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new InvalidConfigurationException("Failed to build URI", ex);
            }
        }

        public void AssertValid()
        {
            // Verify that the url can be built.
            // Everything else is at least sometimes optional.
            getUri();
        }

        public void AssertValidForCoralService()
        {
            // Verify that the url can be built.
            getUri();

            if (string.IsNullOrWhiteSpace(operationName))
            {
                throw new InvalidConfigurationException("Missing operation name");
            }

            if (string.IsNullOrWhiteSpace(serviceName))
            {
                throw new InvalidConfigurationException("Missing service name");
            }

            if (string.IsNullOrWhiteSpace(serviceNamespace))
            {
                throw new InvalidConfigurationException("Missing service namespace");
            }
        }

        /// <summary>
        /// Copies non-blank fields from the provided config, and leaves the other fields as-is.
        /// </summary>
        public void Merge(ApiConfig other)
        {
            if (other == null)
            {
                return;
            }

            if (other.enabled != null)
            {
                enabled = other.enabled;
            }

            if (!string.IsNullOrWhiteSpace(other.endpoint))
            {
                endpoint = other.endpoint;
            }

            if (!string.IsNullOrWhiteSpace(other.operationName))
            {
                operationName = other.operationName;
            }

            if (!string.IsNullOrWhiteSpace(other.path))
            {
                path = other.path;
            }

            if (other.retries != null && other.retries >= MIN_RETRIES && other.retries <= MAX_RETRIES)
            {
                retries = other.retries;
            }

            if (other.retryDelaySeconds != null && other.retryDelaySeconds >= MIN_RETRY_DELAY_SECONDS)
            {
                retryDelaySeconds = other.retryDelaySeconds;
            }

            if (!string.IsNullOrWhiteSpace(other.serviceName))
            {
                serviceName = other.serviceName;
            }

            if (!string.IsNullOrWhiteSpace(other.serviceNamespace))
            {
                serviceNamespace = other.serviceNamespace;
            }

            if (other.timeoutSeconds != null && other.timeoutSeconds >= MIN_TIMEOUT_SECONDS)
            {
                timeoutSeconds = other.timeoutSeconds;
            }
        }
    }
}
