﻿using FuelPump.Metrics;
using FuelPump.Properties;
using Newtonsoft.Json;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static FuelPump.Metrics.MetricEventFactory;

namespace FuelPump.Services.Config
{
    public class ConfigHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(ConfigHandler));

        private FuelConfig bootstrapConfig;
        private FuelConfig remoteConfig = null;
        protected DateTime? nextConfigReload = null;
        private TaskCompletionSource<bool> initialLoad = new TaskCompletionSource<bool>();
        private Task reloadConfigTask;
        private Lazy<MetricEventFactory> metricsFactory;
        protected HttpClient httpClient;
        private Dictionary<string, ApiCallMetric> callMetrics = new Dictionary<string, ApiCallMetric>();
        private object configLock = new object();

        private class ApiCallMetric
        {
            public int successCount = 0;
            public int failureCount = 0;
        }

        [Inject]
        public ConfigHandler(Lazy<MetricEventFactory> metricsFactory) : this(metricsFactory, null)
        {
        }

        public ConfigHandler(Lazy<MetricEventFactory> metricsFactory, HttpClient httpClient)
        {
            bootstrapConfig = JsonConvert.DeserializeObject<FuelConfig>(GetBootstrapConfig());
            bootstrapConfig.AssertValid();
            this.httpClient = httpClient ?? new HttpClient(new LoggingHandler(new HttpClientHandler()));
            this.metricsFactory = metricsFactory;
        }

        /// <summary>
        /// Load the config asynchronously.
        /// The program will run this after the everything has been fully initialized.
        /// Lazy<> isn't smart enough to distinguish between the thread initializing Lazy<> and a worker thread
        //// that wants to block on the result, and will throw an exception if the worker thread blocks too soon.
        /// Also, Ninject dependency resolution is at least to some extent not threadsafe and can instantiate
        /// multiple of a singleton if two threads try to do it at the same time.  This is probably a bug in Ninject.
        /// </summary>
        public void Initialize()
        {
            reloadConfigTask = LoadConfig();
            reloadConfigTask.ContinueWith(completedTask => initialLoad.TrySetResult(true));
        }

        public virtual FuelConfig GetConfig()
        {
            var config = TryGetConfig();
            if (config != null)
            {
                return config;
            }

            initialLoad.Task.Wait();

            lock (configLock)
            {
                return remoteConfig != null ? remoteConfig : bootstrapConfig;
            }
        }

        public virtual FuelConfig TryGetConfig()
        {
            if (initialLoad.Task.IsCompleted)
            {
                lock (configLock)
                {
                    if (nextConfigReload != null && nextConfigReload.Value.CompareTo(GetCurrentTime()) < 0 && reloadConfigTask.IsCompleted)
                    {
                        reloadConfigTask = LoadConfig();
                    }

                    return remoteConfig != null ? remoteConfig : bootstrapConfig;
                }
            }

            return null;
        }

        /// <summary>
        /// See documentation on IsPossiblyBadConfiguration.
        /// 
        /// If failures are consistently reported for an operation, then the whole config will fall back to bootstrap
        /// values for a while.
        /// </summary>
        /// <param name="name">The name of the operation.</param>
        /// <param name="success">
        /// This class only cares about configuration, and doesn't care if the game requested an invalid SKU.
        /// This parameter should be true if the config looks to be correct, and false if the configuration
        /// may have caused the call to fail.
        /// </param>
        public virtual void ReportApiCallOutcome(string name, bool success)
        {
            lock (configLock)
            {
                if (!callMetrics.ContainsKey(name))
                {
                    callMetrics[name] = new ApiCallMetric();
                }

                var metrics = callMetrics[name];
                if (success)
                {
                    metrics.successCount++;
                }
                else
                {
                    metrics.failureCount++;
                    if (remoteConfig != null && metrics.successCount == 0 && metrics.failureCount >= remoteConfig.configSettings.maxFailuresUntilFallback)
                    {
                        LOG.Error("Falling back to bootstrap config.");
                        double duration = (double) remoteConfig.configSettings.fallbackDurationSeconds;

                        remoteConfig = null;
                        callMetrics.Clear();
                        nextConfigReload = GetCurrentTime().AddSeconds(duration);
                    }
                }
            }
        }

        /// <summary>
        /// See https://w.amazon.com/index.php/Respawn/SDK/TechDesign#Lost_Clients
        /// 
        /// This method tries to decide how likely it is that an exception was caused by a bad config.
        /// The goal is to fallback when the config is bad to avoid lost clients, but also avoid thrashing.
        /// Transient network issues can cause a lot of different types of exceptions, so the
        /// ReportApiCallOutcome method above will only trigger the fallback if calls are consistently
        /// failing due to something that looks like a configuration issue.  If at least one call has
        /// succeeded in reaching something that looks like the correct service, then the config probably
        /// isn't bad and fallback won't happen.
        /// 
        /// See also how ADGCoralServiceClient decides what to report.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool IsPossiblyBadConfiguration(Exception e)
        {
            // Look through all the nested exceptions for a WebException and check why it failed.
            Exception innerException = e;
            for (int i = 0; i < 10 && innerException != null; i++)
            {
                // Definitely bad configuration.
                if (innerException is InvalidConfigurationException)
                {
                    return true;
                }

                var webException = innerException as WebException;
                if (webException != null)
                {
                    // NameResolutionFailure is more than likely a bad configuration, but could also be caused
                    // by the local DNS being broken.  If the local DNS is broken, they've got worse things to
                    // worry about than falling back to bootstrap config.
                    return webException.Status == WebExceptionStatus.NameResolutionFailure ||
                           webException.Status == WebExceptionStatus.Timeout;
                }
                innerException = innerException.InnerException;
            }

            return false;
        }

        private async Task LoadConfig()
        {
            try
            {
                if (bootstrapConfig.configSettings == null || string.IsNullOrWhiteSpace(bootstrapConfig.configSettings.file))
                {
                    LOG.Debug("Bootstrap config does not contain configSettings.file");
                    return;
                }

                using (var metrics = metricsFactory.Value.Create())
                {
                    using (var timer = metrics.MetricEvent.StartTimer(FuelPumpMetrics.Config.LoadConfig))
                    {
                        var configTask = GetRemoteConfig(metrics, bootstrapConfig.configSettings.file, true)
                            .ContinueWith(async (task) => {
                                try
                                {
                                    // Will throw if it failed.
                                    var result = await task;

                                    // Start using the new config.  This may happen after the timeout below.
                                    lock (configLock)
                                    {
                                        remoteConfig = result;
                                        ScheduleReloadAfterSuccess();
                                    }
                                    LOG.Debug("Installed remote config");
                                }
                                catch (Exception ex)
                                {
                                    LOG.Error("Failed to load config from " + bootstrapConfig.configSettings.file, ex);
                                    metrics.MetricEvent.AddCounter(FuelPumpMetrics.Config.LoadConfigFailure);
                                    ScheduleReloadAfterFallback();
                                }
                            });

                        // Wait for the config to be loaded or just use the bootstrap config if it times out.
                        var timeoutTask = Task.Delay((int)(bootstrapConfig.configSettings.initializationTimeoutSeconds * 1000));
                        var firstTaskToComplete = await Task.WhenAny(configTask, timeoutTask);
                        if (firstTaskToComplete == timeoutTask)
                        {
                            metrics.MetricEvent.AddCounter(FuelPumpMetrics.Config.LoadConfigTimeout);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Failed to load config from " + bootstrapConfig.configSettings.file, ex);
                ScheduleReloadAfterFallback();
            }
        }

        protected virtual string GetBootstrapConfig()
        {
            return Encoding.UTF8.GetString(Resources.BootstrapConfig);
        }

        protected virtual DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }

        private void ScheduleReloadAfterFallback()
        {
            lock (configLock)
            {
                var config = remoteConfig != null ? remoteConfig : bootstrapConfig;
                double duration = (double)config.configSettings.fallbackDurationSeconds;
                double jitter = 1 + (double)config.configSettings.jitterMultiplier * new Random().NextDouble();
                nextConfigReload = GetCurrentTime().AddSeconds(duration * jitter);
            }
        }

        private void ScheduleReloadAfterSuccess()
        {
            lock (configLock)
            {
                var config = remoteConfig != null ? remoteConfig : bootstrapConfig;
                double duration = (double)config.configSettings.refreshIntervalSeconds;
                double jitter = 1 + (double)config.configSettings.jitterMultiplier * new Random().NextDouble();
                nextConfigReload = GetCurrentTime().AddSeconds(duration * jitter);
            }
        }

        /// <exception cref="ApplicationException"/>
        /// <exception cref="InvalidConfigurationException"/>
        private async Task<FuelConfig> GetRemoteConfig(MetricEventScope metrics, string url, bool firstAttempt)
        {
            LOG.DebugFormat("Loading config from {0}", url);
            var response = await httpClient.GetAsync(url);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApplicationException(url + " returned status code " + response.StatusCode);
            }

            byte[] responseContent = await response.Content.ReadAsByteArrayAsync();
            var config = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(responseContent));

            var mergedConfig = JsonConvert.DeserializeObject<FuelConfig>(GetBootstrapConfig());
            mergedConfig.Merge(config);
            mergedConfig.AssertValid();

            if (firstAttempt && mergedConfig.configSettings.file != url)
            {
                try
                {
                    return await GetRemoteConfig(metrics, mergedConfig.configSettings.file, false);
                }
                catch (Exception e)
                {
                    LOG.Error("Failed to load config from " + mergedConfig.configSettings.file, e);
                    metrics.MetricEvent.AddCounter(FuelPumpMetrics.Config.LoadConfigFailure);
                }
            }

            LOG.Debug("Loaded config from " + url);
            return mergedConfig;
        }
    }
}
