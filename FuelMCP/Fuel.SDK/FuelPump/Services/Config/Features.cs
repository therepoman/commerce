﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.Config
{
    public class Features
    {
        public static ISet<string> ALL = new HashSet<string>(new string[] {
            AMZN_OAUTH
        });

        public static readonly string AMZN_OAUTH = "amazonOAuth";
    }
}
