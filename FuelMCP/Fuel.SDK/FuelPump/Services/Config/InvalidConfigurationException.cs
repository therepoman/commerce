﻿using System;

namespace FuelPump.Services.Config
{
    public class InvalidConfigurationException : ApplicationException
    {
        public InvalidConfigurationException(string message) : base(message)
        {
        }

        public InvalidConfigurationException(string message, Exception ex) : base(message, ex)
        {
        }
    }
}
