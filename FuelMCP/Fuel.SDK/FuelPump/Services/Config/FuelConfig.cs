﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FuelPump.Services.Config
{
    [DataContract]
    public class FuelConfig
    {
        private static readonly double MIN_PAGE_LOAD_TIMEOUT_SECONDS = 1;
        private static readonly int MIN_QUEUE_SIZE = 0;
        private static readonly double MIN_TRANSMISSION_INTERVAL_SECONDS = 5;

        // List of config names to validate.
        private enum ApiConfigNames {
            cblPage,
            cookie,
            getProductListDetails,
            metrics,
            purchasePage,
            unlinkedAccountsPage,
            expiredCredentialsPage,
            syncGoods,
            tokenExchange,
            validateTwitchToken
        };

        [DataMember]
        public ConfigSettings configSettings { get; set; }

        [DataMember]
        public string cookiesDomain { get; set; }

        [DataMember]
        public List<string> discoveryProductBlacklist { get; set; }

        [DataMember]
        public List<string> entitlementProductBlacklist { get; set; }

        [DataMember]
        public int? metricsMaxQueueSize { get; set; }

        [DataMember]
        public double? metricsTransmissionIntervalSeconds { get; set; }

        [DataMember]
        public List<UriFilter> openInBrowserWhitelist { get; set; }

        [DataMember]
        public List<UriFilter> overlayUriWhitelist { get; set; }

        [DataMember]
        public double? pageLoadTimeoutSeconds { get; set; }

        [DataMember]
        public List<UriFilter> restartCblBlacklist { get; set; }

        [DataMember]
        public int? urlPathBlacklistMaxRedirects { get; set; }

        [DataMember]
        public Dictionary<string, ApiConfig> apiConfig;

        [DataMember]
        private Dictionary<string, Boolean> features;

        // Add new API config keys to the enum above, and add validation below if needed.  Also update FuelConfigTests and the test json files.
        public ApiConfig cblPage { get { return apiConfig[ApiConfigNames.cblPage.ToString()]; } }
        public ApiConfig cookie { get { return apiConfig[ApiConfigNames.cookie.ToString()]; } }
        public ApiConfig getProductListDetails { get { return apiConfig[ApiConfigNames.getProductListDetails.ToString()]; } }
        public ApiConfig metrics { get { return apiConfig[ApiConfigNames.metrics.ToString()]; } }
        public ApiConfig purchasePage { get { return apiConfig[ApiConfigNames.purchasePage.ToString()]; } }
        public ApiConfig unlinkedAccountsPage { get { return apiConfig[ApiConfigNames.unlinkedAccountsPage.ToString()]; } }
        public ApiConfig expiredCredentialsPage { get { return apiConfig[ApiConfigNames.expiredCredentialsPage.ToString()]; } }
        public ApiConfig syncGoods { get { return apiConfig[ApiConfigNames.syncGoods.ToString()]; } }
        public ApiConfig tokenExchange { get { return apiConfig[ApiConfigNames.tokenExchange.ToString()]; } }
        public ApiConfig validateTwitchToken { get { return apiConfig[ApiConfigNames.validateTwitchToken.ToString()]; } }
        // Add new API config keys to the enum above, and add validation below if needed.  Also update FuelConfigTests and the test json files.

        public void AssertValid()
        {
            if (configSettings == null)
            {
                throw new InvalidConfigurationException("Missing config settings.");
            }
            configSettings.AssertValid();

            if (string.IsNullOrWhiteSpace(cookiesDomain))
            {
                throw new InvalidConfigurationException("Missing cookies domain.");
            }

            foreach (var apiConfigName in Enum.GetNames(typeof(ApiConfigNames)))
            {
                if (!apiConfig.ContainsKey(apiConfigName))
                {
                    throw new InvalidConfigurationException("Missing api config for " + apiConfigName);
                }

                try
                {
                    apiConfig[apiConfigName].AssertValid();
                }
                catch(Exception ex)
                {
                    // Add key name to exception.
                    throw new InvalidConfigurationException("Invalid api config for " + apiConfigName, ex);
                }
            }

            getProductListDetails.AssertValidForCoralService();
            syncGoods.AssertValidForCoralService();

            if (metricsMaxQueueSize == null || metricsMaxQueueSize < MIN_QUEUE_SIZE)
            {
                throw new InvalidConfigurationException("Missing or invalid metricsMaxQueueSize.");
            }

            if (metricsTransmissionIntervalSeconds == null || metricsTransmissionIntervalSeconds < MIN_TRANSMISSION_INTERVAL_SECONDS)
            {
                throw new InvalidConfigurationException("Missing or invalid metricsTransmissionIntervalSeconds.");
            }

            if (openInBrowserWhitelist == null || openInBrowserWhitelist.Count == 0)
            {
                throw new InvalidConfigurationException("Missing openInBrowserWhitelist.");
            }

            if (overlayUriWhitelist == null || overlayUriWhitelist.Count == 0)
            {
                throw new InvalidConfigurationException("Missing overlayUriWhitelist.");
            }

            if (pageLoadTimeoutSeconds == null || pageLoadTimeoutSeconds < MIN_PAGE_LOAD_TIMEOUT_SECONDS)
            {
                throw new InvalidConfigurationException("Missing or invalid pageLoadTimeoutSeconds.");
            }

            if (restartCblBlacklist == null || restartCblBlacklist.Count == 0)
            {
                throw new InvalidConfigurationException("Missing restartCblBlacklist.");
            }

            if (urlPathBlacklistMaxRedirects == null || urlPathBlacklistMaxRedirects < 0)
            {
                throw new InvalidConfigurationException("Missing or invalid urlPathBlacklistMaxRedirects.");
            }
        }

        /// <summary>
        /// Copies non-blank fields from the provided config, and leaves the other fields as-is.
        /// </summary>
        public void Merge(FuelConfig other)
        {
            if (other == null)
            {
                return;
            }

            if (other.configSettings != null)
            {
                if (configSettings == null)
                {
                    configSettings = new ConfigSettings();
                }

                configSettings.Merge(other.configSettings);
            }

            if (!string.IsNullOrWhiteSpace(other.cookiesDomain))
            {
                cookiesDomain = other.cookiesDomain;
            }

            if (other.discoveryProductBlacklist != null && other.discoveryProductBlacklist.Count > 0)
            {
                discoveryProductBlacklist = new List<string>(other.discoveryProductBlacklist);
            }

            if (other.entitlementProductBlacklist != null && other.entitlementProductBlacklist.Count > 0)
            {
                entitlementProductBlacklist = new List<string>(other.entitlementProductBlacklist);
            }

            if (other.metricsMaxQueueSize != null)
            {
                metricsMaxQueueSize = other.metricsMaxQueueSize;
            }

            if (other.metricsTransmissionIntervalSeconds != null)
            {
                metricsTransmissionIntervalSeconds = other.metricsTransmissionIntervalSeconds;
            }

            if (other.apiConfig != null)
            {
                if (apiConfig == null)
                {
                    apiConfig = new Dictionary<string, ApiConfig>();
                }

                foreach (var entry in other.apiConfig)
                {
                    if (!apiConfig.ContainsKey(entry.Key))
                    {
                        apiConfig[entry.Key] = new ApiConfig();
                    }

                    apiConfig[entry.Key].Merge(entry.Value);
                }
            }

            if (other.openInBrowserWhitelist != null && other.openInBrowserWhitelist.Count > 0)
            {
                openInBrowserWhitelist = new List<UriFilter>(other.openInBrowserWhitelist);
            }

            if (other.overlayUriWhitelist != null && other.overlayUriWhitelist.Count > 0)
            {
                overlayUriWhitelist = new List<UriFilter>(other.overlayUriWhitelist);
            }

            if (other.pageLoadTimeoutSeconds != null && other.pageLoadTimeoutSeconds >= MIN_PAGE_LOAD_TIMEOUT_SECONDS)
            {
                pageLoadTimeoutSeconds = other.pageLoadTimeoutSeconds;
            }

            if (other.restartCblBlacklist != null && other.restartCblBlacklist.Count > 0)
            {
                restartCblBlacklist = new List<UriFilter>(other.restartCblBlacklist);
            }

            if (other.urlPathBlacklistMaxRedirects != null && other.urlPathBlacklistMaxRedirects >= 0)
            {
                urlPathBlacklistMaxRedirects = other.urlPathBlacklistMaxRedirects;
            }

            if (other.features != null && other.features.Count >= 0)
            {
                if (features == null)
                {
                    features = new Dictionary<string, bool>();
                }

                var featureNames = Features.ALL;
                foreach (var entry in other.features)
                {
                    if (featureNames.Contains(entry.Key)) features[entry.Key] = entry.Value;
                }
            }
        }

        public bool IsWhitelistedForOverlay(Uri uri)
        {
            if (overlayUriWhitelist == null || overlayUriWhitelist.Count == 0)
            {
                return false;
            }

            foreach (var filter in overlayUriWhitelist)
            {
                if (filter.IsMatch(uri))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsWhitelistedForOpenInBrowser(Uri uri)
        {
            if (openInBrowserWhitelist == null || openInBrowserWhitelist.Count == 0)
            {
                return false;
            }

            foreach (var filter in openInBrowserWhitelist)
            {
                if (filter.IsMatch(uri))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsRestartCblBlacklisted(Uri uri)
        {
            if (restartCblBlacklist == null || restartCblBlacklist.Count == 0)
            {
                return false;
            }

            foreach (var filter in restartCblBlacklist)
            {
                if (filter.IsMatch(uri))
                {
                    return true;
                }
            }

            return false;
        }

        public virtual bool IsFeatureOn(string featureName)
        {
            if (features == null || !features.ContainsKey(featureName))
            {
                return false;
            }

            return features[featureName];
        }
    }
}
