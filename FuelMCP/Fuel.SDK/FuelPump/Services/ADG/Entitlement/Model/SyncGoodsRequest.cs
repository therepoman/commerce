﻿using FuelPump.Services.ADG.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Entitlement.Model
{
    class SyncGoodsRequest : ClientRequest
    {
        public SyncPointData startSync { get; set; }
    }
}
