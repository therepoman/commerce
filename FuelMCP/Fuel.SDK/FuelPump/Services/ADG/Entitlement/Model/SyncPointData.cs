﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Entitlement.Model
{
    class SyncPointData
    {
        public string syncToken { get; set; }
        public ulong syncPoint { get; set; }
    }
}
