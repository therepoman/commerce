﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class DigitalGoodTransaction
    {
        public string instanceId { get; set; }
        public string txId { get; set; }
        public long? txTimestamp { get; set; }
        public TransactionTypeEnum txType { get; set; }
        public string externalTxId { get; set; }
        public string externalTxType { get; set; }
        public string customerId { get; set; }
        public string ownerId { get; set; }
        public string holderId { get; set; }
    }
}
