﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class ProductDetail
    {
        public string productIconUrl { get; set; }
        public string category { get; set; }
        public Currency listPrice { get; set;}
        public Currency currentPrice { get; set; }
        public string compatibleProductVersion { get; set; }
        public string latestProductVersion { get; set; }
        public bool isCompatible { get; set; }
        public bool isPurchasable { get; set; }
        public Dictionary<string, Dictionary<string, string>> details { get; set; }
    }
}
