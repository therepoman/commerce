﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class ClientRequest
    {
        public ClientInfo client { get; set; }
        public CustomerLocalePrefs preferredLocale { get; set; }
        public string language { get; set; } // language code
    }
}
