﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class DigitalGoodOrigin
    {
        public string originId { get; set; }
        public long? originTimestamp { get; set; }
        public string originType { get; set; }
    }
}
