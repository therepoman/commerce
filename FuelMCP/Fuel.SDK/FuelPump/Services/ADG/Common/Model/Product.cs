﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class Product
    {
        public string id { get; set; } // uuid
        public Vendor vendor { get; set; }
        public string sku { get; set; }
        public string asin { get; set; }
        public int? asinVersion { get; set; }
        public string type { get; set; } // ProductTypeEnum
        public string productTitle { get; set; }
        public string productDescription { get; set; }
        public string productLine { get; set; } // ProductLineEnum
        public Domain productDomain { get; set; }
        public ProductDetail productDetails { get; set; }
    }
}
