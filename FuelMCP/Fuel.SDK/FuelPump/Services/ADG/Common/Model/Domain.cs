﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class Domain
    {
        public string id { get; set; } // uuid
        public string name { get; set; }
        public ADGEntity owner { get; set; }
    }
}
