﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Order.Model
{
    public class SetReceiptsFulfillmentStatusRequest
    {
        public List<ReceiptUpdate> receiptUpdates { get; set; }
    }
}
