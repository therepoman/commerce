﻿namespace FuelPump.Services.ADG.Order.Model
{
    public class ReceiptUpdate
    {
        public string receiptId { get; set; }
        public SetReceiptsFulfillmentStatusEnum status { get; set; }
    }
}
