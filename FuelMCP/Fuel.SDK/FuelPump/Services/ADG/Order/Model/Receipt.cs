﻿namespace FuelPump.Services.ADG.Order.Model
{
    public class Receipt
    {
        public string receiptId { get; set; }
        public bool? valid { get; set; }
        public string requestResult { get; set; } // OrderingRequestResultEnum Coral enum
        public string refundError { get; set; } // RefundOrderErrorEnum Coral enum
        public OrderItem item { get; set; }
    }
}
