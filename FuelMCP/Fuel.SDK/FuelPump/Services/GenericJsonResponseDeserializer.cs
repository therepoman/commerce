﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text;

namespace FuelPump.Services
{
    /**
     * Generic deserializer that deserializes JSON into an object using JSON.NET library
     */
    class GenericJsonResponseDeserializer<T>
    {
        public T Deserialize(byte[] json)
        {
            string jsonString = Encoding.UTF8.GetString(json);
            return JsonConvert.DeserializeObject<T>(jsonString, new StringEnumConverter());
        }
    }
}
