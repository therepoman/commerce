﻿using System.Net.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FuelPump.Services
{
    class LoggingHandler : DelegatingHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger("Wire");

        public LoggingHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            LOG.Debug("Request:");
            LOG.Debug(request.ToString());
            if (request.Content != null)
            {
                LOG.Debug(await request.Content.ReadAsStringAsync());
            }

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            LOG.Debug("Response:");
            LOG.Debug(response.ToString());
            if (response.Content != null)
            {
                LOG.Debug(await response.Content.ReadAsStringAsync());
            }

            return response;
        }
    }
}
