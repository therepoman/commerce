﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FuelPump.Providers.Credentials;

namespace FuelPump.Services
{
    public class RequestContext<TRequestType, TResponseType>
    {
        public delegate TResponseType ResponseDeserializer<TResponseType>(byte[] response);
        public delegate string RequestSerializer<TRequestType>(TRequestType request);

        public readonly TRequestType Request;
        public readonly ResponseDeserializer<TResponseType> Deserializer;
        public readonly RequestSerializer<TRequestType> Serializer;
        public readonly HttpMethod Method = HttpMethod.Post; // always POST for now
        public readonly string Operation;

        public RequestContext(string Operation, TRequestType Request, RequestSerializer<TRequestType> Serializer, ResponseDeserializer<TResponseType> Deserializer)
        {
            this.Operation = Operation;
            this.Request = Request;
            this.Deserializer = Deserializer;
            this.Serializer = Serializer;
        }
    }
}
