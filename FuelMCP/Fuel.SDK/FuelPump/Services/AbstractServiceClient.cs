﻿using FuelPump.Services.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FuelPump.Providers.Credentials;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto;
using System.IO;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Newtonsoft.Json;

namespace FuelPump.Services
{
    /**
     * Abstract service client for a Coral service. Uses the RPCv1 protocol
     */
    public class AbstractServiceClient
    {
        private static AppInfo m_appInfo = new AppInfo();

        private HttpClient Client;

        private AbstractCredentialsProvider CredentialsProvider;
        private ConfigHandler ConfigHandler;

        protected AbstractServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler, HttpClient client = null)
        {
            this.Client = client ?? new HttpClient(new LoggingHandler(new HttpClientHandler()));
            this.Client.DefaultRequestHeaders.Add("User-Agent", m_appInfo.UserAgent);
            this.CredentialsProvider = credentialsProvider;
            this.ConfigHandler = configHandler;
        }

        /// <summary>
        /// Uses Coral RPCv1 protocol to call the service. Expects a succesful response or a coral error response.
        /// </summary>
        /// <typeparam name="TRequest">Type of request object</typeparam>
        /// <typeparam name="TResponse">Type of response object</typeparam>
        /// <param name="context">The request context</param>
        /// <param name="cancellationToken">The cancellation token for the request</param>
        /// <returns>A response object or throws CoralException</returns>
        protected async virtual Task<TResponse> CallAsync<TRequest, TResponse>(RequestContext<TRequest, TResponse> context, ApiConfig apiConfig, CancellationToken cancellationToken, BearerCredentials identifyingCredentials)
        {
            apiConfig.AssertValidForCoralService();
            var body = context.Serializer(context.Request);
            var content = new StringContent(body, Encoding.UTF8, "application/json");
            HttpContentHeaders headers = content.Headers;
            headers.Add("Content-Encoding", "amz-1.0");
            headers.Add("X-Amz-Target", string.Format("{0}.{1}.{2}", apiConfig.serviceNamespace, apiConfig.serviceName, context.Operation));

            if (identifyingCredentials != null)
            {
                AssembleAuthHeaders(ref headers, identifyingCredentials);     
            }

            HttpResponseMessage response = await Client.PostAsync(apiConfig.getUri(), content);

            if (response.IsSuccessStatusCode)
            {
                byte[] responseContent = await response.Content.ReadAsByteArrayAsync();
                return context.Deserializer(responseContent);
            }
            else
            {
                string responseString = await response.Content.ReadAsStringAsync();                
                string errorType = null;
                string errorMessage = null;

                try
                {
                    dynamic errorJson = JsonConvert.DeserializeObject<dynamic>(responseString);
                    errorType = errorJson.__type;
                    errorMessage = errorJson.message;
                } catch (Exception)
                {
                }

                throw new CoralException(errorType, errorMessage);
            }
        }

        // Set identification headers:
        // - Special headers (MAP/ADP/DMS token)
        // - Oauth credentials 
        // - Special header listing all the oauth credential headers
        protected void AssembleAuthHeaders(ref HttpContentHeaders headers, BearerCredentials identifyingCredentials)
        {
            List<string> oauthHeaderNames = new List<string>();
            if (!String.IsNullOrWhiteSpace(identifyingCredentials.Header))
            {
                headers.Add(identifyingCredentials.Header, identifyingCredentials.Token);
                oauthHeaderNames.Add(identifyingCredentials.Header);
            }

            if (oauthHeaderNames.Count > 0)
            {
                headers.Add("X-ADG-Oauth-Headers", oauthHeaderNames);
            }
        }
    }
}