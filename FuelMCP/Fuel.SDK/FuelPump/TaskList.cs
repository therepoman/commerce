﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    class TaskList
    {
        List<Task> m_tasks = new List<Task>();
        object m_lock = new object();
        

        public void AddTask(Task task)
        {
            // locking list to avoid race condition that leads to a null entry
            lock (m_lock)
            { 
                m_tasks.Add(task);
            }
            task.ContinueWith(RemoveTask);
        }

        public void WaitForAll()
        {
            Task.WaitAll(m_tasks.ToArray());
        }

        public void WaitForAll(int millisecondsTimeout)
        {
            Task.WaitAll(m_tasks.ToArray(), millisecondsTimeout);
        }

        private void RemoveTask(Task task)
        {
            // locking list to avoid race condition that leads to a null entry
            lock (m_lock)
            {
                m_tasks.Remove(task);
            }
        }
    }
}
