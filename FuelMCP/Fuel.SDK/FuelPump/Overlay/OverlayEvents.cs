﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Overlay
{
    public class OverlayEvents
    {
        public Task<CefResult> CloseButtonClicked { get; }
        public Task<CefResult> RedirectTask { get; }
        public Task<CefLoadError> PageLoadError { get; }
        public Task PageLoadTimeout { get; }
        public Task OverlayFailure { get; }

        public OverlayEvents(Task<CefResult> CloseButtonClicked, Task<CefResult> RedirectTask, Task<CefLoadError> PageLoadError,
            Task PageLoadTimeout, Task OverlayFailure)
        {
            this.CloseButtonClicked = CloseButtonClicked;
            this.RedirectTask = RedirectTask;
            this.PageLoadError = PageLoadError;
            this.PageLoadTimeout = PageLoadTimeout;
            this.OverlayFailure = OverlayFailure;
        }
    }
}
