﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Overlay
{
    public class CefResult
    {
        private string m_eventName, m_eventData;

        public CefResult(string eventName, string eventData)
        {
            m_eventName = eventName;
            m_eventData = eventData;
        }

        public string EventName
        {
            get { return m_eventName; }
        }

        public string EventData
        {
            get { return m_eventData; }
        }
    }
}
