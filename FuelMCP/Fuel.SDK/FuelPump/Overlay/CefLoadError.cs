﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Overlay
{
    public class CefLoadError
    {
        public int ErrorCode { get; }
        public string ErrorText { get; }
        public string FailedURL { get; }

        public CefLoadError(int code, string text, string url)
        {
            ErrorCode = code;
            ErrorText = text;
            FailedURL = url;
        }
    }
}
