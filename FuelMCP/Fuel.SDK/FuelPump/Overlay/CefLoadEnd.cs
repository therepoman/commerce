﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Overlay
{
    public class CefLoadEnd
    {
        public int HttpStatusCode { get; }

        public CefLoadEnd(int statusCode)
        {
            HttpStatusCode = statusCode;
        }
    }
}
