﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Providers.Credentials
{
    class TwitchCredentials : BearerCredentials
    {
        public static readonly string TYPE = "twitch";

        public TwitchCredentials(string identifier, string token) : base(TYPE, identifier, token)
        {
        }

        public override string Header
        {
            get { return "TwitchOAuth"; }
        }
    }
}
