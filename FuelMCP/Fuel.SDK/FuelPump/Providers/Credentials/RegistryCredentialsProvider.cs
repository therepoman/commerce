﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FuelPump.Helpers;
using Amazon.Auth.Map;
using Amazon.Auth.Map.Windows;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FuelPump.Providers.Credentials
{
    public class RegistryCredentialsProvider : AbstractCredentialsProvider
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(RegistryCredentialsProvider));

        private Metrics.MetricEventFactory m_metrics;

        public RegistryCredentialsProvider(Metrics.MetricEventFactory metrics)
        {
            m_metrics = metrics;
        }

        protected override string GetStorageLocationForIdentifyingCredentials(BearerCredentials identifyingCredentials)
        {
            return (identifyingCredentials.Type + identifyingCredentials.Identifier).ToHashString();
        }

        // Use the Registry token provider.  AppInfo isn't used in this case.
        protected override ITokenProvider GetTokenProvider(AppInfo appInfo)
        {
            return new RegisteryTokenProvider();
        }

        // Get credentials from the store.
        public override Credentials GetCredentials(BearerCredentials identifyingCredentials)
        {
            if (identifyingCredentials == null) throw new CredentialsNotFoundException("cannot look up credentials for null/whitespace identifier");

            using (var metricScope = m_metrics.Create())
            using (var timing = metricScope.MetricEvent.StartTimer(FuelPumpMetrics.CredentialsLookup))
            {
                try
                {
                    // Credentials to be returned.
                    Dictionary<String, BearerCredentials> creds = new Dictionary<String, BearerCredentials>();

                    LOG.Info("getting all credentials; identifer name: [" + identifyingCredentials.Type + "]");

                    // To use an alternate key name for the registry entry, we have to set a static field in the RegisteryTokenProvider.  This
                    // introduces the risk of multiple threads trying to read or write that member simultaneously, granting access to a different 
                    // user's creds or storing someone else's creds for a current user.  
                    //
                    // The risk of both of these is extremely low bordering on nonexistent, given the fact that the class is process-local
                    // and any Fuel Pump process will be used by a single user.  That being said, prudence demands using a lock so that even in the
                    // unlikely eventuality of simultaneous access to this member, we deadlock instead of stomp.
                    CredStoreContents contents;
                    lock (RegisteryTokenProvider.RefreshTokenDatastoreKey)
                    {
                        // Prep the registry token provider to look in the right place.
                        RegisteryTokenProvider.RefreshTokenDatastoreKey = GetStorageLocationForIdentifyingCredentials(identifyingCredentials);

                        // Get the registry entry and deserialize.
                        contents = LoadCredsContents(GetAppInfo());
                    }

                    // Verification of contents with the identifying token and machine data.  Exceptions thrown by the method itself.
                    VerifyContents(contents, identifyingCredentials);

                    // If we got here, everything's copacetic.  Pack the creds into the return object
                    return new Credentials(contents.AmazonCredentials);
                }
                catch
                {
                    metricScope.MetricEvent.SetCounter(FuelPumpMetrics.CredentialsLookupMiss);
                    throw;
                }
            }
        }

        // Store a list of credentials accessed by a specified identifying credential.
        public override void SetCredentials(BearerCredentials identifyingCredentials, Credentials credentials)
        {
            if (identifyingCredentials == null || String.IsNullOrWhiteSpace(identifyingCredentials.Type) || String.IsNullOrWhiteSpace(identifyingCredentials.Identifier)) throw new InvalidCredentialsException("credentials identifier must be a valid string");
            if (credentials == null) throw new InvalidCredentialsException("no credentials provided");

            try
            {
                AmazonRefreshToken token = new AmazonRefreshToken(GetSerializedContents(identifyingCredentials, credentials));

                // See explanation of lock in GetCredentials above.
                lock (RegisteryTokenProvider.RefreshTokenDatastoreKey)
                {
                    RegisteryTokenProvider.RefreshTokenDatastoreKey = GetStorageLocationForIdentifyingCredentials(identifyingCredentials);
                    GetTokenProvider(GetAppInfo()).SetRefreshToken(token); // Broken out for readability.
                }
            }
            catch (Exception e)
            {
                string errMsg = "failed to store credentials";
                LOG.Error(errMsg, e); // Provide details in the logs.
                throw new CredentialStorageException(errMsg);
            }
        }

        public override void RemoveCredentials(BearerCredentials identifyingCredentials)
        {
            if (identifyingCredentials == null || String.IsNullOrWhiteSpace(identifyingCredentials.Type) || String.IsNullOrWhiteSpace(identifyingCredentials.Identifier))
            {
                throw new InvalidCredentialsException("credentials identifier must be a valid string");
            }

            try
            {
                // See explanation of lock in GetCredentials above.
                lock (RegisteryTokenProvider.RefreshTokenDatastoreKey)
                {
                    RegisteryTokenProvider.RefreshTokenDatastoreKey = GetStorageLocationForIdentifyingCredentials(identifyingCredentials);
                    GetTokenProvider(GetAppInfo()).RemoveStoredTokens();
                }
            }
            catch (Exception e)
            {
                string errMsg = "failed to remove credentials";
                LOG.Error(errMsg, e);
                throw new CredentialStorageException(errMsg);
            }
        }
    }
}
