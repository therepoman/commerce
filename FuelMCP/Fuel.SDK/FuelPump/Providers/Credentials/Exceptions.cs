﻿using System;

namespace FuelPump.Providers.Credentials
{
    // Thrown on a lookup that returns no credentials or provides bad lookup data.
    public class CredentialsNotFoundException : Exception
    {
        public CredentialsNotFoundException(String message) : base(message)
        {
        }

        public CredentialsNotFoundException(String message, Exception cause) : base(message, cause)
        {
        }
    }

    // Thrown when storing credentials using bad data.
    public class InvalidCredentialsException : Exception
    {
        public InvalidCredentialsException(String message) : base(message)
        {
        }
    }

    // Thrown when credentials can't be stored for some reason.
    public class CredentialStorageException : Exception
    {
        public CredentialStorageException(String message) : base(message)
        {
        }
    }

    // Thrown when unrecognized credentials or wrong number of credentials are provided.
    public class UnsupportedCredentialsException : Exception
    {
        public UnsupportedCredentialsException(String message) : base(message)
        {
        }
    }
}
