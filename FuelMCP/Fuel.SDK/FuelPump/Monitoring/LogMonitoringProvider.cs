﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Monitoring
{
    public class LogMonitoringProvider : IMonitoringProvider
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(LogMonitoringProvider));

        public void LogStartupFailure(Exception ex)
        {
            LOG.Fatal("Failure to start", ex);
        }

        public void LogRuntimeFailure(Exception ex)
        {
            LOG.Fatal("Failure at runtime", ex);
        }

        public void LogUnhandledHandlerException(Exception ex)
        {
            LOG.Fatal("Failure in handler", ex);
        }

        public void LogUnhandledMessage(Message message)
        {
            LOG.Warn("Message type " + message.GetMessageType().Name + " is unknown");
        }
    }
}
