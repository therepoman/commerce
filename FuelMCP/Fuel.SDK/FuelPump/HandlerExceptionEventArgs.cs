﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    public class HandlerExceptionEventArgs : EventArgs
    {
        public HandlerExceptionEventArgs(Message message, Exception ex)
        {
            Message = message;
            Exception = ex;
        }

        public Message Message { get; private set; }
        public Exception Exception { get; private set; }
    }
}
