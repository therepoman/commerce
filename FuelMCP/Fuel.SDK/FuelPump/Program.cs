﻿using FuelPump.Logging;
using FuelPump.Services.Config;
using Ninject;
using Ninject.Modules;
using System;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;

namespace FuelPump
{
    public class Program
    {
        private const int SUCCESS = (int)Result.TERMINATED_SUCCESSFULLY;
        private const int INSUFFICIENT_ARGS = (int)Result.FAILED_INSUFFICIENT_ARGS;
        private const int CEF_FAILED = (int)Result.CEF_INITIALIZATION_FAILED;

        private const string OFFSCREEN_BROWSER_DLL = "OffscreenBrowser.dll";

        [DllImport(OFFSCREEN_BROWSER_DLL)]
        [return: MarshalAs(UnmanagedType.I1)]
        static extern bool CEFStart([MarshalAs(UnmanagedType.LPWStr)]string userAgent);

        [DllImport(OFFSCREEN_BROWSER_DLL)]
        static extern void CEFStop();

        [DllImport("kernel32")]
        static extern bool AllocConsole();

        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(Program));

        public static AppInfo AppInfo = new AppInfo();
        public static bool CefStarted { get; private set; }

        public static Metrics.MetricEventFactory Metrics;
        private static Ninject.StandardKernel Kernel;

        private enum Result : int
        {
            TERMINATED_SUCCESSFULLY = 0,
            FAILED_INSUFFICIENT_ARGS,
            CEF_INITIALIZATION_FAILED,
        }

        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        static int Main(string[] args)
        {
#if DEBUG
            AllocConsole();
#endif
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionLogger);

            if (args.Length < 2)
            {
                LOG.Debug("Insuffient number of arguments.");
                return INSUFFICIENT_ARGS;
            }
            string readHandle = args[0];
            string writeHandle = args[1];

            LOG.InfoFormat("Starting Fuel Pump. Read Handle : '{0}' Write Handle : '{1}' DSN : '{2}'", readHandle, writeHandle, AppInfo.DeviceSerialId);

            var modules = new NinjectModule[]
                    {
                        new Activation.TransportModule(readHandle, writeHandle),
                        new Activation.DependencyModule()
                    };
            Kernel = new Ninject.StandardKernel(modules);
            Metrics = Kernel.Get<Metrics.MetricEventFactory>();

            try
            {
                try
                {
                    CefStarted = CEFStart(AppInfo.UserAgent);

                    if (!CefStarted)
                    {
                        LOG.Error("Cef failed to initialize.");
                    }
                }
                catch (Exception e)
                {
                    LOG.Error("Cef failed to initialize.", e);
                    CefStarted = false;
                }
                

                try
                {
                    using (var program = Kernel.Get<MessagePump>())
                    {
                        // Initialize the ConfigHandler after everything else to work around a Ninject bug.
                        // See documentation of ConfigHandler.Initialize.
                        Kernel.Get<ConfigHandler>().Initialize();

                        Timer dllLogTimer = null;
                        var dllLogUploader = new AsyncSpectatorUploader(AppInfo, Constants.LOG_PATH, $"FuelDll_*.log");
                        // We have to poll since we don't know exactly when the dll rolls its logs
                        dllLogTimer = new Timer(_ => {
                            dllLogUploader.EnqueuePrevious();
                            dllLogTimer.Change((int) new TimeSpan(0, 32, 0).TotalMilliseconds, Timeout.Infinite);
                        }, null, (int)new TimeSpan(0, 32, 0).TotalMilliseconds, Timeout.Infinite);

                        var result = program.Start();
                        result.Wait();
                        return result.Result ? SUCCESS : INSUFFICIENT_ARGS;
                    }
                }
                finally
                {
                    try
                    {
                        if (CefStarted) CEFStop();
                    }
                    catch (Exception e)
                    {
                        LOG.Warn("Failed to stop CEF", e);
                    }
                    LOG.InfoFormat("Stopping Fuel Pump.");
                }
            }
            // catch all logger to make sure it goes to logs as well as console
            catch (Exception ex)
            {
                LOG.Error("Unexpected error", ex);
                return CEF_FAILED;
            }
        }

        static void UnhandledExceptionLogger(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = args.ExceptionObject as Exception;
            LOG.Fatal("Unhandled exception:", e);
            Metrics.AddCounter(FuelPumpMetrics.UnhandledException);
        }
    }
}
