﻿using FuelPump.Overlay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Handlers
{
    class OverlayMessageHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(OverlayMessageHandler));

        CefInstanceManager m_cef_manager;

        public OverlayMessageHandler(CefInstanceManager manager)
        {
            m_cef_manager = manager;
        }

        public async Task SetWindowSize(MessageContext context, Messages.SetWindowSize message)
        {
            m_cef_manager.SetSize(message.Width, message.Height);
        }

        public async Task OverlayFailureEvent(MessageContext context, Messages.OverlayFailureEvent message)
        {
            try
            {
                m_cef_manager.OnOverlayFailure(message.InstanceId);
            }
            catch (CefInstanceManager.UnknownCefInstanceException)
            {
                LOG.Debug("OverlayFailureEvent received for non-existant cef instance " + message.InstanceId);
            }
        }
    }
}
