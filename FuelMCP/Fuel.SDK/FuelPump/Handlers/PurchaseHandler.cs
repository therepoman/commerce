using FuelPump.Helpers;
using FuelPump.Overlay;
using FuelPump.Providers.Credentials;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Messages = Fuel.Messaging;
using System.Diagnostics;
using FuelPump.Services;
using FuelPump.Services.ADG.Order;
using FuelPump.Services.ADG.Order.Model;
using FuelPump.Services.Config;
using FuelPump.Metrics;
using static FuelPump.Helpers.AuthenticationHelper;

namespace FuelPump.Handlers
{
    public class PurchaseHandler : AbstractHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(PurchaseHandler));
        public static readonly string PROCESSING_EVENT_NAME = "Processing";

        private AuthenticationHelper authenticationHelper;
        private ValidationHelper validationHelper;
        private MetricEventFactory metricEventFactory;
        private IApplicationContext AppContext;
        CefInstanceManager cefManager;
        AppInfo appInfo;

        public PurchaseHandler(Metrics.MetricEventFactory metrics, CefInstanceManager manager, AppInfo appInfo,
            AuthenticationHelper authHelper, ValidationHelper validationHelper, 
            IApplicationContext context, ConfigHandler configHandler) : base(configHandler)
        {
            metricEventFactory = metrics;
            cefManager = manager;
            this.appInfo = appInfo;
            authenticationHelper = authHelper;
            this.validationHelper = validationHelper;
            AppContext = context;
        }

        public async Task<MessageFactory<Messages.PurchaseResponse>> Purchase(
            MessageContext context,
            Messages.PurchaseRequest message)
        {
            // SONIC-7038 disable ADGOrderService integration (this should never be called)
            return await Task.FromResult(CreateResponse(Messages.PurchaseResponseStatus.Failed));
        }

        private async Task<bool> HandleBadCredentials(CefInstance instance, BearerCredentials identifyingCredentials, AmazonCredentials amazonCredentials, string url)
        {

            if (url.EndsWith("/logout"))
            {
                // The user explicitly logged out. Therefore, we need to clear the cache, so the user can get new ones.
                authenticationHelper.ClearCredentialsCache(identifyingCredentials);
                return false;
            }

            // Clear the credentials cache if the creds are bad.
            Boolean badCreds = await authenticationHelper.ClearCredentialsCacheIfCredentialsAreBad(identifyingCredentials, amazonCredentials);

            var overlayEvents = instance.RegisterOverlayEvents();
            if (!badCreds)
            {
                // The credentials were not bad, so the user should be able to successfully retry.
                instance.Navigate(configHandler.GetConfig().expiredCredentialsPage.getUri());
                try
                {
                    await CefInstance.WrapPageLoad(overlayEvents.CloseButtonClicked, overlayEvents);
                }
                catch (OperationCanceledException)
                {
                    // Nothing to do, user clicked the close button.
                }
                catch (Exception ex)
                {
                    LOG.Error("The token expired page also failed", ex);
                }
            }

            return true;
        }

        private void ToggleOverlay(MessageContext context, bool showOverlay, CefInstance instance)
        {
            FlatBuffers.FlatBufferBuilder headerBuilder = new FlatBuffers.FlatBufferBuilder(1);
            string RequestId = context.RequestId;
            var headerRoot = Fuel.Messaging.MessageHeader.CreateMessageHeader(headerBuilder,
                requestId: headerBuilder.CreateString(RequestId),
                type: (int)(showOverlay ? Fuel.Messaging.Message.ShowOverlay : Fuel.Messaging.Message.HideOverlay));
            headerBuilder.Finish(headerRoot.Value);

            FlatBuffers.FlatBufferBuilder dataBuilder = new FlatBuffers.FlatBufferBuilder(1);

            if (showOverlay)
            {
                Rectangle rect = instance.GetRect();
                var payload = Messages.ShowOverlay.CreateShowOverlay(dataBuilder,
                    instanceId: instance.GetInstanceId(),
                    x: rect.X,
                    y: rect.Y,
                    width: rect.Width,
                    height: rect.Height);
                var payloadRoot = Messages.MessageRoot.CreateMessageRoot(dataBuilder,
                    message_type: Messages.Message.ShowOverlay,
                    message: payload.Value);
                dataBuilder.Finish(payloadRoot.Value);
            }
            else
            {
                var payload = Messages.HideOverlay.CreateHideOverlay(dataBuilder, instanceId: instance.GetInstanceId());
                var payloadRoot = Messages.MessageRoot.CreateMessageRoot(dataBuilder,
                    message_type: Messages.Message.HideOverlay,
                    message: payload.Value);
                dataBuilder.Finish(payloadRoot.Value);
            }

            Message toggleOverlayMessage = new Message(headerBuilder.SizedByteArray(), dataBuilder.SizedByteArray());
            context.ProcessMessage(toggleOverlayMessage);
        }

        private MessageFactory<Messages.PurchaseResponse> CreateResponse(Messages.PurchaseResponseStatus status)
        {
            return new MessageFactory<Messages.PurchaseResponse>(
                builder =>
                {
                    return Messages.PurchaseResponse.CreatePurchaseResponse(builder, status: status);
                });
        }
    }
}
