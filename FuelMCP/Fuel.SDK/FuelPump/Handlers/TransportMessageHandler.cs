﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Handlers
{
    public class TransportMessageHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(TransportMessageHandler));
        IMessageTransport m_transport;

        public TransportMessageHandler(IMessageTransport transport)
        {
            m_transport = transport;
        }

        public Task SendMessage(MessageContext context, Message message)
        {
            // write failures throw an exception that will lead to recovery of the transport connection
            m_transport.WriteMessage(message);

            return Task.FromResult(false); // just return a completed task
        }
    }
}
