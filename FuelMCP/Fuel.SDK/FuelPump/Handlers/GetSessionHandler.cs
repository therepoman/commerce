﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Threading.Tasks;

using Messages = Fuel.Messaging;
using FuelPump.Services.Config;
using FuelPump.Metrics;

namespace FuelPump.Handlers
{
    public class GetSessionHandler : AbstractHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(GetSessionHandler));

        private readonly Metrics.MetricEventFactory Metrics;
        private readonly ITwitchLoginPlugin _twitchLogin;
        private readonly ITwitchUserStoragePlugin _userStorage;

        public GetSessionHandler(MetricEventFactory metrics, ConfigHandler configHandler, ITwitchLoginPlugin twitchLogin, ITwitchUserStoragePlugin userStorage) : base(configHandler)
        {
            Metrics = metrics;
            _twitchLogin = twitchLogin;
            _userStorage = userStorage;
        }

        public Task<MessageFactory<Messages.GetSessionResponse>> GetSession(MessageContext context, Messages.GetSessionRequest message)
        {
            return Task.Run(new Func<MessageFactory<Messages.GetSessionResponse>>(() =>
            {
                using (var metricScope = Metrics.Create())
                using (var timer = metricScope.MetricEvent.StartTimer(FuelPumpMetrics.ServiceCall.GetSession))
                {
                    metricScope.MetricEvent.AddCounter(FuelPumpMetrics.APICall.GetSession);

                    TwitchCredentialSet credentialSet = null;
                    LocalTwitchUser twitchUser = null;
                    string gameAccessToken = null;

                    try
                    {
                        credentialSet = _userStorage.GetTwitchCredentialSet(message.ClientId);
                        twitchUser = _userStorage.User;
                        gameAccessToken = _twitchLogin.GetGameToken(message.ClientId, credentialSet.Scopes);
                    }
                    catch (Exception e)
                    {
                        LOG.Error("Failed to get token", e);
                        metricScope.MetricEvent.SetCounter(FuelPumpMetrics.ServiceCall.GetSessionFailure);
                        return new MessageFactory<Messages.GetSessionResponse>(builder =>
                        {
                            return Messages.GetSessionResponse.CreateGetSessionResponse(builder, status: Messages.GetSessionResponseStatus.Failed);
                        });
                    }

                    return new MessageFactory<Messages.GetSessionResponse>(builder =>
                    {
                        FlatBuffers.Offset<Messages.TwitchUser> twitchUserOffset;
                        twitchUserOffset = Messages.TwitchUser.CreateTwitchUser(builder,
                            builder.CreateString(gameAccessToken ?? ""),
                            builder.CreateString(twitchUser?.Logo ?? ""),
                            builder.CreateString(twitchUser?.Username ?? ""),
                            builder.CreateString(twitchUser?.DisplayName ?? ""),
                            builder.CreateString(twitchUser?.Tuid ?? ""),
                            ToEpochTime(DateTime.UtcNow));
                        var entitlementOffset = builder.CreateString(credentialSet?.Entitlement);

                        return Messages.GetSessionResponse.CreateGetSessionResponse(builder, twitchUserOffset, entitlementOffset,
                            Messages.GetSessionResponseStatus.Success);
                    });
                }
            }));
        }
    }
}
