﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    public static class Constants
    {
        public const string AppName = "Fuel SDK";
        public const string InternalAppName = "FuelSDK";

#if WIN
        public const string WindowsDeviceType = "AOFR8BROH3RC3"; // See https://dms360.amazon.com/device_type_registry/AOFR8BROH3RC3
        public const string WindowsDeviceModel = "Windows"; // See AmazonAuthMap/src/Amazon.Auth.Map.Shared/Environment.cs

        public readonly static string LOG_PATH = Environment.ExpandEnvironmentVariables("%appdata%\\Twitch\\Fuel\\SdkLogs");
#endif
    }
}
