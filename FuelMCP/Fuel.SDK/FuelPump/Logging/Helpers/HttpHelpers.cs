﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amazon.Diagnostics.Spectator.Shared.Helpers
{
    internal static class HttpHelpers
    {
        public static Uri GetUri(string name, string baseAddress)
        {
            var baseUri = new UriBuilder(baseAddress);
            var queryToAppend = "key=" + name;

            if (!string.IsNullOrWhiteSpace(baseUri.Query))
            {
                baseUri.Query = baseUri.Query.Substring(1) + "&" + queryToAppend;
            }
            else
            {
                baseUri.Query = queryToAppend;
            }

            return baseUri.Uri;
        }
    }
}
