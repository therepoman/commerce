using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Amazon.Diagnostics.Spectator.Shared;
using Amazon.Diagnostics.Spectator.Shared.Helpers;
using log4net.Util;

namespace Amazon.Diagnostics.Spectator
{
    public class PmetUploader : DetUploader
    {
        private readonly static Type declaredType = typeof(PmetUploader);
        protected readonly IDomainProvider DomainProvider;

        public PmetUploader(HttpClient httpClient, IAppInfo appInfo, IDomainProvider domainProvider)
            : base(httpClient, appInfo)
        {
            Require.IsNotNull(domainProvider, "domainProvider cannot be null");
            DomainProvider = domainProvider;
        }

        public override async Task<HttpRequestMessage> CreateRequestAsync(string filePath)
        {
            var fileName = Path.GetFileName(filePath);
            HttpRequestMessage request;

            string systemVersion = _appInfo.Version;
            string domain = DomainProvider.GetDomain();
            string regionMap = DomainProvider.GetAmazonOfficalRegion();

            if (!string.IsNullOrWhiteSpace(domain))
                domain = domain.Replace("amazon", "");

            try
            {
                using (var fileStream = File.Open(filePath, FileMode.Open))
                {
                    var logHeader =
                        string.Format
                        (
                            "[Metadata]{0}" +
                            "LogType: Metrics{0}" +
                            "DeviceTypeId: {1}{0}" +
                            "{0}" +
                            "[Software Versions]{0}" +
                            "{2}: {3}{0}" +
                            "{0}" +
                            "[States]{0}" +
                            "Region: {4}{0}" +
                            "Domain: {5}{0}" +
                            "{0}" +
                            "[Events]{0}",
                            UnixNewLine,
                            _appInfo.SpectatorName,
                            _appInfo.DeviceTypeId,
                            systemVersion,
                            regionMap,
                            domain
                        );

                    var logHeaderData = Encoding.UTF8.GetBytes(logHeader);

                    long length = 0;

                    if (fileStream != null && logHeaderData != null)
                    {
                        length = fileStream.Length + logHeaderData.Length;
                    }

                    var bodyPreamble =
                        string.Format
                        (
                            "MFBS/1.0 1{0}" +
                            "Content-Type: Text/Plain{0}" +
                            "Content-Encoding: UTF8{0}" +
                            "Content-Length: {1}{0}" +
                            "{0}" +
                            "{0}",
                            UnixNewLine,
                            length
                        );

                    var bodyPreambleData = Encoding.UTF8.GetBytes(bodyPreamble);

                    // bodyPreambleData is sent first but is calcuated second as we need to include the size of
                    //  the log logFile header that we generate here
                    var requestHeader = new byte[bodyPreambleData.Length + logHeaderData.Length];
                    Buffer.BlockCopy(bodyPreambleData, 0, requestHeader, 0, bodyPreambleData.Length);
                    Buffer.BlockCopy(logHeaderData, 0, requestHeader, bodyPreambleData.Length, logHeaderData.Length);

                    var logData = new byte[fileStream.Length];

                    // casting here is OK because we cap the size of a log logFile to 1MB
                    var nRead = await fileStream.ReadAsync(logData, 0, (int) fileStream.Length);

                    if (nRead != fileStream.Length)
                    {
                        LogLog.Debug(declaredType, "Did not read entire log file while uploading Pmet");
                    }

                    var requestData = new byte[requestHeader.Length + nRead];
                    Buffer.BlockCopy(requestHeader, 0, requestData, 0, requestHeader.Length);
                    Buffer.BlockCopy(logData, 0, requestData, requestHeader.Length, nRead);

                    request = new HttpRequestMessage(HttpMethod.Post, HttpHelpers.GetUri(fileName, BaseAddress))
                    {
                        Content = new ByteArrayContent(requestData)
                    };

                    request.Headers.Add("X-DSN", _appInfo.DeviceSerialId);
                    request.Headers.Add("X-DeviceType", _appInfo.DeviceTypeId);
#if DEBUG
                    request.Headers.Add("X-DeviceFirmwareVersion", "dev");
#else
                    request.Headers.Add("X-DeviceFirmwareVersion", systemVersion);
#endif
                    request.Headers.Add("X-Content-Type", "KindleLogFormat-1.0");
                    request.Headers.Add("User-Agent", _appInfo.HttpUserAgentName);
                }
            }
            catch (IOException exception)
            {
                request = null;
                LogLog.Error(declaredType, $"Error while trying to upload {filePath} to Pmet metrics", exception);
            }
            return request;
        }
    }
}