﻿using System.Threading.Tasks;
namespace Amazon.Diagnostics.Spectator
{
	/// <summary>
	/// IDET uploader.
	/// Interface for uploading logs to Device Event Tracker
	/// https://w.amazon.com/index.php/DeviceMetricsAndLogging/DeviceEventTracker
	/// </summary>
    public interface IDetUploader
    {
		/// <summary>
		/// Uploads the log to Device Event Tracker.
		/// </summary>
		/// <returns>True if successful.</returns>
		/// <param name="fileName">File name.</param>
		/// <param name="deleteFileAfterUploading">If set to <c>true</c> delete file after uploading.</param>
        Task<bool> UploadLogToDet(string fileName, bool deleteFileAfterUploading=true);
    }
}