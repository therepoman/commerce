﻿
namespace Amazon.Diagnostics.Spectator
{
    public interface IAppInfo
    {
        string SpectatorName { get; }

        string PmetDomain { get; }

        string PmetSource { get; }

        string DeviceSerialId { get; }

        string DeviceTypeId { get; }

        string HttpUserAgentName { get; }

        string Version { get; }
    }
}