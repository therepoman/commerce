﻿using log4net.Appender;
using log4net.Core;
using log4net.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Logging
{
    /// <summary>
    /// A customized implemented of the standard RollingFileAppender that
    /// contains some custom logic for our project.
    /// </summary>
    class FuelRollingFileAppender : RollingFileAppender
    {
        private readonly static Type declaringType = typeof(RollingFileAppender);

        private DateTime _rollNext;
        private readonly AsyncSpectatorUploader _uploader;

        public FuelRollingFileAppender()
        {
            _rollNext = CalculateRollNextDateTime(DateTime.Now);
            _uploader = new AsyncSpectatorUploader(new AppInfo(), Constants.LOG_PATH, $"FuelPump.log_*.log");
        }

        private DateTime CalculateRollNextDateTime(DateTime from)
        {
            return from.AddMinutes(30.0d);
        }

        protected override void AdjustFileBeforeAppend()
        {
            DateTime now = DateTime.Now;

            if (now >= _rollNext)
            {
                _rollNext = CalculateRollNextDateTime(now);

                // We include the Date + Timestamp (including minutes) so base will take care of the rest
                base.AdjustFileBeforeAppend();

                // Can't get the roll over file name easily so just queue up any rolled over files
                _uploader.EnqueuePrevious();
            }
        }
    }
}