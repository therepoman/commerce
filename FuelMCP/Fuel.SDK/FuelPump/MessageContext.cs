﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    public class MessageContext
    {
        private static readonly int MAX_REQUEST_ID_LENGTH = 1024;
        string m_requestId;
        MessageDispatcher m_dispatcher;
        int m_retryCounter;

        public MessageContext(string requestId, int retryCounter, MessageDispatcher dispatcher)
        {
            if (requestId.Length > MAX_REQUEST_ID_LENGTH)
            {
                throw new ArgumentException("Request id is too long (" + requestId.Length + " characters)");
            }
            m_requestId = requestId;
            m_dispatcher = dispatcher;
            m_retryCounter = retryCounter;
        }

        public string RequestId { get { return m_requestId; } }
        public int Retry { get { return m_retryCounter; } }

        public byte[] BuildResponseHeader(int responseType)
        { 
            FlatBuffers.FlatBufferBuilder headerBuilder = new FlatBuffers.FlatBufferBuilder(1);
            var headerRoot = Fuel.Messaging.MessageHeader.CreateMessageHeader(headerBuilder, headerBuilder.CreateString(RequestId), m_retryCounter, responseType);
            headerBuilder.Finish(headerRoot.Value);
            return headerBuilder.SizedByteArray();
        }

        public Task ProcessMessage(Message msg)
        {
            return m_dispatcher.ProcessMessage(msg);
        }
    }
}
