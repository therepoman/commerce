﻿using Fuel.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump
{
    public interface IMessageTransportWriter : IDisposable
    {
        void Write(int data);
        void Write(byte[] data);
        void Write(IntPtr ptr, uint size);
    }

    public interface IMessageTransport : IDisposable
    {
        /// <summary>
        /// Writes a message to the transport 
        /// </summary>
        /// <param name="message">The message</param>
        /// <exception cref="System.IO.IOException">Raised when the transport connection is closed.</exception>
        void WriteMessage(Message message);

        IMessageTransportWriter WriteUntypedMessge(byte[] header, uint size); 

        /// <summary>
        /// Attempts to read a message from the transport
        /// </summary>
        /// <returns>The message received</returns>
        /// <exception cref="System.IO.EndOfStreamException">Raised when there is a problem reading from the transport or the transport connection is closed.</exception>
        /// <exception cref="System.OperationCancelledException">Raised when the read is cancelled.</exception>
        Task<Message> ReadMessage(CancellationToken cancellationToken);

        /// <summary>
        /// Indicates the transport has been explicitly disposed or a connection failure occurred
        /// </summary>
        bool IsDisposed();
    }
}
