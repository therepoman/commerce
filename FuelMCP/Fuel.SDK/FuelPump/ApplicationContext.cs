﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    public interface IApplicationContext
    {
        string VendorId { get; set; }
    }
    public class ApplicationContext : IApplicationContext
    {
        private string vendorId = null;
        public string VendorId
        {
            get
            {
                if (vendorId == null)
                {
                    throw new InvalidOperationException("VendorId not set. Was init sent?");
                }

                return vendorId;
            }

            set
            {
                if (vendorId != null)
                {
                    throw new InvalidOperationException("VendorId already set.");
                }
                vendorId = value;
            }
        }
    }
}
