﻿using Ninject;

namespace FuelPump.Activation
{
    internal class DispatcherProvider : Ninject.Activation.Provider<MessageDispatcher>
    {
        [Inject]
        public Handlers.TransportMessageHandler TransportHandler { get; set; }

        [Inject]
        public Handlers.GetProductDataHandler GetProductDataHandler { get; set; }

        [Inject]
        public Handlers.PurchaseHandler PurchaseHandler { get; set; }

        [Inject]
        public Handlers.GetPurchaseUpdatesHandler GetPurchaseUpdatesHandler { get; set; }

        [Inject]
        public Handlers.InputHandler InputHandler { get; set; }

        [Inject]
        public Handlers.OverlayMessageHandler OverlayMessageHandler { get; set; }

        [Inject]
        public Handlers.GetSessionHandler GetSessionHandler { get; set; }

        protected override MessageDispatcher CreateInstance(Ninject.Activation.IContext context)
        {
            var dispatcher = new MessageDispatcher();

            // GetProductData Messages
            dispatcher.RegisterHandler<Fuel.Messaging.GetProductDataRequest, Fuel.Messaging.GetProductDataResponse>(GetProductDataHandler.GetProductData);
            dispatcher.RegisterHandler<Fuel.Messaging.GetProductDataResponse>(TransportHandler.SendMessage);

            // Purchase Messages
            dispatcher.RegisterHandler<Fuel.Messaging.PurchaseRequest, Fuel.Messaging.PurchaseResponse>(PurchaseHandler.Purchase);
            dispatcher.RegisterHandler<Fuel.Messaging.PurchaseResponse>(TransportHandler.SendMessage);

            // GetPurchaseUpdates Messages
            dispatcher.RegisterHandler<Fuel.Messaging.GetPurchaseUpdatesRequest, Fuel.Messaging.GetPurchaseUpdatesResponse>(GetPurchaseUpdatesHandler.GetPurchaseUpdates);
            dispatcher.RegisterHandler<Fuel.Messaging.GetPurchaseUpdatesResponse>(TransportHandler.SendMessage);

            // NotifyFulfillment Messages
            dispatcher.RegisterHandler<Fuel.Messaging.NotifyFulfillmentResponse>(TransportHandler.SendMessage);

            // Overlay Interface Messages
            dispatcher.RegisterHandler<Fuel.Messaging.KeyboardEvent>(InputHandler.KeyboardEvent);
            dispatcher.RegisterHandler<Fuel.Messaging.MouseEvent>(InputHandler.MouseEvent);
            dispatcher.RegisterHandler<Fuel.Messaging.MouseMoveEvent>(InputHandler.MouseMoveEvent);
            dispatcher.RegisterHandler<Fuel.Messaging.MouseWheelEvent>(InputHandler.MouseWheelEvent);
            dispatcher.RegisterHandler<Fuel.Messaging.CloseOverlayEvent>(InputHandler.CloseOverlayEvent);
            dispatcher.RegisterHandler<Fuel.Messaging.ShowOverlay>(TransportHandler.SendMessage);
            dispatcher.RegisterHandler<Fuel.Messaging.HideOverlay>(TransportHandler.SendMessage);

            // Overlay Messages
            dispatcher.RegisterHandler<Fuel.Messaging.SetWindowSize>(OverlayMessageHandler.SetWindowSize);
            dispatcher.RegisterHandler<Fuel.Messaging.OverlayFailureEvent>(OverlayMessageHandler.OverlayFailureEvent);

            dispatcher.RegisterHandler<Fuel.Messaging.GetSessionRequest, Fuel.Messaging.GetSessionResponse>(GetSessionHandler.GetSession);
            dispatcher.RegisterHandler<Fuel.Messaging.GetSessionResponse>(TransportHandler.SendMessage);

            return dispatcher;
        }
    }
}
