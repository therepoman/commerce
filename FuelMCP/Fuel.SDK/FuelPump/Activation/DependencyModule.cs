﻿using FuelPump.Metrics;
using FuelPump.Monitoring;
using FuelPump.Overlay;
using FuelPump.Helpers;
using FuelPump.Services.Config;
using Ninject;
using Ninject.Modules;
using System;
using System.Net.Http;
using Amazon.Auth.Map;
using Amazon.Auth.Map.Services;
using Amazon.Auth.Map.Services.Panda;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Plugin.TwitchUserStorage;
using Amazon.Fuel.Plugin.SecureStorage;
using Clients.Kraken;
using Amazon.Fuel.Plugin.TwitchLogin;
using Amazon.Fuel.Plugin.Metrics;
using Clients.Twitch.Spade.Client;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;

namespace FuelPump.Activation
{
    public class DependencyModule : NinjectModule
    {
        private static readonly AppInfo APP_INFO = new AppInfo();

        public override void Load()
        {
            Bind<IOSDriver>().To<WindowsFileSystemDriver>()
                .InSingletonScope()
                .OnActivation(x =>
                {
                    x.IsDevMode = Convert.ToBoolean(System.Environment.GetEnvironmentVariable("FUEL_DEV_MODE"));
                });
            Bind<ISecureStorageProtectionPolicy>().To<SecureStorageDsapiProtectionPolicy>();
            Bind<PersistencePlugin>().To<UserPersistencePlugin>().InSingletonScope();
            Bind<IMetricsPlugin>().ToMethod(context =>
                new MetricsPlugin("AOFR8BROH3RC3", DateTime.UtcNow, new SpadeClient(new SpadeHttpClient()))
            ).InSingletonScope();
            Bind<IMessengerHub>().To<MessengerHub>().InSingletonScope();
            Bind<IClock>().To<Clock>().InSingletonScope();
            Bind<IdentityClient>().ToSelf()
                .InSingletonScope()
                .WithConstructorArgument("clientid", "rqbo1jq09v4pl4d16t85b3j4krvrvz") 
                .WithConstructorArgument("endpoint", "https://api.twitch.tv");
            Bind<KrakenClient>().ToSelf()
                .InSingletonScope()
                .WithConstructorArgument("endpoint", "https://api.twitch.tv/kraken")
                .WithConstructorArgument("clientid", "rqbo1jq09v4pl4d16t85b3j4krvrvz");
            Bind<ISecureStoragePlugin>().To<SecureSQLiteStorage>()
                .InSingletonScope()
                .OnActivation(store => store.Init());
            Bind<ICurseTokenProvider>().To<DefaultCurseTokenProvider>().InSingletonScope();
            Bind<ITwitchTokenProvider, ITwitchTokenProvider>().To<TwitchLoginPlugin>().InSingletonScope();
            Bind<ITwitchUserStoragePlugin>().To<TwitchUserStoragePlugin>().InSingletonScope();

            Bind<ITwitchLoginPlugin>().To<TwitchLoginPlugin>().InSingletonScope();

            Bind<CefInstanceManager>().ToSelf().InSingletonScope();

            Bind<IMonitoringProvider>()
                .To<LogMonitoringProvider>()
                .InSingletonScope();

            Bind<IMetricQueue, MetricQueue>()
                .To<MetricQueue>()
                .InSingletonScope();

            Bind<IMetricTransport, ServiceMetricTransport>()
                .To<ServiceMetricTransport>()
                .InSingletonScope()
                .WithConstructorArgument("deviceType", APP_INFO.DeviceTypeId)
                .WithConstructorArgument("deviceSerialNumber", APP_INFO.DeviceSerialId)
                .WithConstructorArgument("softwareVersion", APP_INFO.Version);

            Bind<MetricEventFactory>()
                .ToSelf()
                .InSingletonScope()
                .WithConstructorArgument("program", "Fuel")
                .WithConstructorArgument("source", "FuelPump");

            Bind(typeof(Lazy<MetricEventFactory>))
                .ToMethod(ctx => new Lazy<MetricEventFactory>(() => ctx.Kernel.Get<MetricEventFactory>(),true))
                .InSingletonScope();

            Bind<Providers.Credentials.AbstractCredentialsProvider>()
                .To<Providers.Credentials.RegistryCredentialsProvider>()
                .InSingletonScope();

            Bind<MessageDispatcher>().ToProvider<DispatcherProvider>().InSingletonScope();

            Bind<IApplicationContext>()
                .To<ApplicationContext>()
                .InSingletonScope();

            Bind<PandaService>()
                .ToMethod(context =>
                    new PandaService(
                        new HttpClient(),
                        new Log4NetILogger(log4net.LogManager.GetLogger(typeof(IPandaService))),
                        APP_INFO,
                        new Amazon.Auth.Map.Environment(),
                        new DomainProvider(new Localization()),
                        new Localization(),
                        new PandaSettings()
                    ))
                .InSingletonScope();

            Bind<ConfigHandler>().ToSelf().InSingletonScope();
        }
    }
}
