﻿#pragma once

#include "include/cef_scheme.h"
#include "include/wrapper/cef_helpers.h"

class CEFSchemeHandler : public CefResourceHandler
{
public:
	CEFSchemeHandler();
	virtual bool ProcessRequest(CefRefPtr<CefRequest> request,
		CefRefPtr<CefCallback> callback) OVERRIDE;
	virtual void GetResponseHeaders(CefRefPtr<CefResponse> response,
		int64& response_length,
		CefString& redirectUrl) OVERRIDE;
	virtual void Cancel() OVERRIDE {};
	virtual bool ReadResponse(void* data_out,
		int bytes_to_read,
		int& bytes_read,
		CefRefPtr<CefCallback> callback) OVERRIDE;

private:
	std::string data;
	std::string mime_type;
	size_t offset;

	IMPLEMENT_REFCOUNTING(CEFSchemeHandler);
};

class CEFSchemeHandlerFactory : public CefSchemeHandlerFactory
{
public:
	virtual CefRefPtr<CefResourceHandler> Create(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		const CefString& scheme_name,
		CefRefPtr<CefRequest> request) OVERRIDE
	{
		return new CEFSchemeHandler();
	}

	IMPLEMENT_REFCOUNTING(CEFSchemeHandlerFactory);
};