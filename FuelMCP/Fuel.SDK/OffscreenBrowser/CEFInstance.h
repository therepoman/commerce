#pragma once

#include <future>

#include "include/cef_app.h"
#include "include/cef_render_handler.h"
#include "include/internal/cef_types_wrappers.h"
#include "FuelCefTypes.h"

typedef void(*OnDrawInternal)(int, int, DirtyRect*, int, const void*, int, int);
class CEFClient;

class CEFInstance
{
public:
	static CEFInstance * Create(int instanceId, OnDrawInternal onDraw, int x, int y, int width, int height);
	void Destroy();

	void Navigate(wchar_t* url);
	void LoadStaticContent(wchar_t* content, wchar_t* url);
	void Resize(int width, int height);
	void KeyboardEvent(cef_key_event_type_t type, int key, bool ctrlDown);
	void MouseClickEvent(int x, int y, cef_mouse_button_type_t type, bool mouseUp, int clickCount);
	void MouseMoveEvent(int x, int y, bool mouseLeave, bool leftMouseDown);
	void MouseWheelEvent(int x, int y, int deltaX, int deltaY);
	void AddDataToHeader(wchar_t* key, wchar_t* value);
	void RemoveDataFromHeader(wchar_t* key);
	bool SetCookie(wchar_t* url, wchar_t* cookieName, wchar_t* cookieValue, wchar_t* cookieDomain, wchar_t* cookiePath, bool secure, bool httpOnly, int expirationDayOfMonth, int expirationDayOfWeek, int expirationMonth, int expirationYear, int expirationHour, int expirationMinute, int expirationSecond, int expirationMillisecond);

private:
	CEFInstance(CefRefPtr<CEFClient> browser);
	CefRefPtr<CEFClient> m_browser;
};
