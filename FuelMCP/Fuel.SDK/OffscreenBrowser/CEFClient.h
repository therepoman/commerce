#pragma once

#include <future>

#include "include/cef_app.h"
#include "include/cef_client.h"
#include "FuelCefTypes.h"

typedef void(*OnDrawInternal)(int, int, DirtyRect*, int, const void*, int, int);

class CEFClientCreationFailedException : public std::runtime_error {
public:
	CEFClientCreationFailedException(const std::string error) : std::runtime_error("CEFClient failed to create due to an error that occured in " + error) {};
};

class CEFClient :
	public CefClient,
	public CefRenderHandler,
	public CefLifeSpanHandler,
	public CefLoadHandler,
	public CefRequestHandler,
	public CefJSDialogHandler,
	public CefContextMenuHandler
{
public:
	static CefRefPtr<CEFClient> Create(int instance, OnDrawInternal onDraw, CefRect rect);
	CefRefPtr<CefBrowser> GetBrowser() { return m_browser; }
	void Destroy();

	// CefClient
	virtual CefRefPtr<CefRenderHandler> GetRenderHandler() OVERRIDE;
	virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() OVERRIDE;
	virtual CefRefPtr<CefLoadHandler> GetLoadHandler() OVERRIDE { return this; }
	virtual CefRefPtr<CefRequestHandler> GetRequestHandler() OVERRIDE { return this; }
	virtual CefRefPtr<CefJSDialogHandler> GetJSDialogHandler() OVERRIDE { return this; }
	virtual CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() OVERRIDE { return this; }
	virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
		CefProcessId source_process,
		CefRefPtr<CefProcessMessage> message) OVERRIDE;

	// CefLifeSpanHandler
	virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser) OVERRIDE;
	virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) OVERRIDE;
	virtual bool OnBeforePopup(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		const CefString& target_url,
		const CefString& target_frame_name,
		CefLifeSpanHandler::WindowOpenDisposition target_disposition,
		bool user_gesture,
		const CefPopupFeatures& popupFeatures,
		CefWindowInfo& windowInfo,
		CefRefPtr<CefClient>& client,
		CefBrowserSettings& settings,
		bool* no_javascript_access) OVERRIDE;

	// CefRenderHandler
	virtual bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) OVERRIDE;
	virtual void OnPaint(CefRefPtr<CefBrowser> browser,
		PaintElementType type,
		const RectList& dirtyRects,
		const void* buffer,
		int width,
		int height) OVERRIDE;
	virtual void OnCursorChange(CefRefPtr<CefBrowser> browser,
		CefCursorHandle cursor,
		CefRenderHandler::CursorType type,
		const CefCursorInfo& custom_cursor_info) OVERRIDE;

	// CefLoadHandler
	virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int httpStatusCode) OVERRIDE;
	virtual void OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
		CefLoadHandler::ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl) OVERRIDE;

	// CefRequestHandler
	virtual bool OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
		CefRefPtr<CefRequest> request, bool is_redirect) OVERRIDE;
	virtual CefRequestHandler::ReturnValue OnBeforeResourceLoad(
		CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		CefRefPtr<CefRequest> request,
		CefRefPtr<CefRequestCallback> callback) OVERRIDE;

	// CefJSDialogHandler
	virtual bool OnJSDialog(CefRefPtr<CefBrowser> browser, const CefString& origin_url,
		JSDialogType dialog_type, const CefString& message_text, const CefString& default_prompt_text,
		CefRefPtr<CefJSDialogCallback> callback, bool& suppress_message) OVERRIDE;

	virtual bool OnBeforeUnloadDialog(CefRefPtr<CefBrowser> browser, const CefString& message_text,
		bool is_reload, CefRefPtr<CefJSDialogCallback> callback) OVERRIDE;

	// CefContextMenuHandler
	virtual void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
		CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model);

	// CEFClient Methods
	void Resize(int width, int height);
	void AddDataToHeader(CefString key, CefString value);
	void RemoveDataFromHeader(CefString key);

	IMPLEMENT_REFCOUNTING(CEFClient);

private:
	CEFClient(int instance, OnDrawInternal onDraw, CefRect rect);
	~CEFClient()
	{
	}

	int m_instance;
	CefRefPtr<CefBrowser> m_browser;
	CefRect m_size;
	CefRequest::HeaderMap m_hMap;
	bool m_loading;

	OnDrawInternal m_onDraw;

	std::promise<void> m_browserReady;
	std::promise<void> m_browserDestroyed;
};

