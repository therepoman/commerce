// OffscreenBrowser.cpp : Defines the exported functions for the DLL application.
//
#include "CEFInstance.h"
#include "CEFFunctions.h"
#include "CEFApp.h"

#include <cstdio>
#include <codecvt>

#include "include/wrapper/cef_helpers.h"
#include "fuel_impl/signature_validator.h"

static const int FAILED = 0;

extern "C" __declspec(dllexport) void CEFOnDraw(OnDraw callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnDraw(callback);
}

extern "C" __declspec(dllexport) void CEFOnEvent(OnEvent callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnEvent(callback);
}

extern "C" __declspec(dllexport) void CEFOnCursorChanged(OnCursorChanged callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnCursorChanged(callback);
}

extern "C" __declspec(dllexport) void CEFOnBeforeBrowse(OnBeforeBrowse callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnBeforeBrowse(callback);
}

extern "C" __declspec(dllexport) void CEFOnBeforeResourceLoad(OnBeforeResourceLoad callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnBeforeResourceLoad(callback);
}

extern "C" __declspec(dllexport) void CEFOnLoadEnd(OnLoadEnd callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnLoadEnd(callback);
}

extern "C" __declspec(dllexport) void CEFOnLoadError(OnLoadError callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnLoadError(callback);
}

extern "C" __declspec(dllexport) void CEFOnOpenInBrowser(OnOpenInBrowser callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnOpenInBrowser(callback);
}

extern "C" __declspec(dllexport) void CEFOnLoadLocalFile(OnLoadLocalFile callback)
{
	Fuel::CEF::CEFFunctions::Instance().SetOnLoadLocalFile(callback);
}

void InternalOnDraw(int instance, int dirtyRectCount, DirtyRect* rects, int size, const void* buffer, int width, int height)
{
	Fuel::CEF::CEFFunctions::Instance().CallOnDraw(instance, dirtyRectCount, rects, size, buffer, width, height);
}

extern "C" __declspec(dllexport) void CEFNavigate(intptr_t instance, wchar_t* url)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->Navigate(url);
}

extern "C" __declspec(dllexport) void CEFLoadStaticContent(intptr_t instance, wchar_t* content, wchar_t* url)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->LoadStaticContent(content, url);
}

extern "C" __declspec(dllexport) void CEFResize(intptr_t instance, int width, int height)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->Resize(width, height);
}

extern "C" __declspec(dllexport) bool CEFStart(wchar_t* userAgent)
{
	CefEnableHighDPISupport(); 
	CefMainArgs mainArgs;

	// Set CEF as windowless
	CefSettings settings;
	settings.windowless_rendering_enabled = true;
	settings.multi_threaded_message_loop = true;
	CefString(&settings.user_agent).FromString16(userAgent);

	// Construct path to the browser exe
	HMODULE hModule = NULL;
	GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		(LPCTSTR)(CEFStart),
		&hModule);
	TCHAR filePath[_MAX_PATH];
	GetModuleFileName(hModule, filePath, _MAX_PATH);
	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR fname[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s(filePath, drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);
	_tmakepath_s(filePath, _MAX_PATH, drive, dir, TEXT("FuelPump.Browser"), TEXT(".exe"));

	CefString(&settings.browser_subprocess_path).FromWString(filePath);

#ifdef _DEBUG
	settings.remote_debugging_port = 3835;
	settings.ignore_certificate_errors = true;
#endif

	// Initialize the CEF application.
	return CefInitialize(mainArgs, settings, new CEFApp(), nullptr);
}

extern "C" __declspec(dllexport) intptr_t CEFCreate(int instanceId, int x, int y, int width, int height)
{
	auto result = CEFInstance::Create(instanceId, InternalOnDraw, x, y, width, height);

	if (result == nullptr)
	{
		return FAILED;
	}

	return reinterpret_cast<intptr_t>(result);
}

extern "C" __declspec(dllexport) void CEFDestroy(intptr_t instance)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->Destroy();
}

extern "C" __declspec(dllexport) void CEFStop()
{
	CefShutdown();
}

extern "C" __declspec(dllexport) void CEFKeyboardEvent(intptr_t instance, int type, int key, bool ctrlDown)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);

	cef_key_event_type_t cefType = (cef_key_event_type_t)type;
	result->KeyboardEvent(cefType, key, ctrlDown);
}

extern "C" __declspec(dllexport) void CEFMouseClickEvent(intptr_t instance, int x, int y, int type, bool mouseUp, int clickCount)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);

	cef_mouse_button_type_t cefType = (cef_mouse_button_type_t)type;
	result->MouseClickEvent(x, y, cefType, mouseUp, clickCount);
}

extern "C" __declspec(dllexport) void CEFMouseMoveEvent(intptr_t instance, int x, int y, bool mouseLeave, bool leftMouseDown)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->MouseMoveEvent(x, y, mouseLeave, leftMouseDown);
}

extern "C" __declspec(dllexport) void CEFMouseWheelEvent(intptr_t instance, int x, int y, int deltaX, int deltaY)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->MouseWheelEvent(x, y, deltaX, deltaY);
}

extern "C" __declspec(dllexport) void CEFAddDataToHeader(intptr_t instance, wchar_t* key, wchar_t* value)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->AddDataToHeader(key, value);
}

extern "C" __declspec(dllexport) void CEFRemoveDataFromHeader(intptr_t instance, wchar_t* key)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	result->RemoveDataFromHeader(key);
}

extern "C" __declspec(dllexport) bool CEFSetCookie(intptr_t instance, wchar_t* url, wchar_t* cookieName, wchar_t* cookieValue, wchar_t* cookieDomain, wchar_t* cookiePath, bool secure, bool httpOnly, int expirationDayOfMonth, int expirationDayOfWeek, int expirationMonth, int expirationYear, int expirationHour, int expirationMinute, int expirationSecond, int expirationMillisecond)
{
	auto result = reinterpret_cast<CEFInstance*>(instance);
	return result->SetCookie(url, cookieName, cookieValue, cookieDomain, cookiePath, secure, httpOnly, expirationDayOfMonth, expirationDayOfWeek, expirationMonth, expirationYear, expirationHour, expirationMinute, expirationSecond, expirationMillisecond);
}