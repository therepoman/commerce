// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "CEFApp.h"
#include "CEFSchemeHandler.h"

void CEFApp::OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line)
{
	command_line->AppendSwitch("--disable-extensions");
	command_line->AppendSwitch("--disable-gpu");
	command_line->AppendSwitch("--disable-gpu-compositing");
	command_line->AppendSwitch("--transparent-painting-enabled");
	command_line->AppendSwitch("--enable-begin-frame-scheduling");
}

void CEFApp::OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar)
{
	registrar->AddCustomScheme("local", true, false, false);
}

CefRefPtr<CefBrowserProcessHandler> CEFApp::GetBrowserProcessHandler()
{
	return this;
}

void CEFApp::OnContextInitialized()
{
	CefRegisterSchemeHandlerFactory("local", "", new CEFSchemeHandlerFactory());
}