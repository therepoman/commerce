#include "FuelCefTypes.h"
#include "include/cef_request_handler.h"

namespace Fuel {
	namespace CEF {
		class CEFFunctions {
		public:
			static CEFFunctions& Instance();
			void SetOnDraw(OnDraw draw) { m_onDrawCallback = draw; }
			void SetOnEvent(OnEvent callback) { m_onEventCallback = callback; }
			void SetOnCursorChanged(OnCursorChanged cursor) { m_onCursorChangedCallback = cursor; }
			void SetOnBeforeBrowse(OnBeforeBrowse callback) { m_onBeforeBrowseCallback = callback; }
			void SetOnBeforeResourceLoad(OnBeforeResourceLoad callback) { m_onBeforeResourceLoadCallback = callback; }
			void SetOnLoadEnd(OnLoadEnd callback) { m_onLoadEnd = callback; }
			void SetOnLoadError(OnLoadError callback) { m_onLoadError = callback; }
			void SetOnOpenInBrowser(OnOpenInBrowser callback) { m_onOpenInBrowser = callback; }
			void SetOnLoadLocalFile(OnLoadLocalFile callback) { m_onLoadLocalFile = callback; }
			void CallOnDraw(int ptr, int dirtyRectCount, DirtyRect* rects, int size, const void* buffer, int width, int height) {
				if (m_onDrawCallback) {
					m_onDrawCallback(ptr, dirtyRectCount, rects, size, buffer, width, height);
				}
			}
			void CallOnEvent(int ptr, const wchar_t* name, const wchar_t* data) {
				if (m_onEventCallback) {
					m_onEventCallback(ptr, name, data);
				}
			}
			void CallOnCursorChanged(int ptr, int type) {
				if (m_onCursorChangedCallback) {
					m_onCursorChangedCallback(ptr, type);
				}
			}
			void CallOnBeforeBrowse(int ptr) {
				if (m_onBeforeBrowseCallback) {
					m_onBeforeBrowseCallback(ptr);
				}
			}
			int CallOnBeforeResourceLoad(int ptr, const wchar_t* url) {
				if (m_onBeforeResourceLoadCallback) {
					return m_onBeforeResourceLoadCallback(ptr, url);
				}
				return CefRequestHandler::ReturnValue::RV_CONTINUE;
			}
			void CallOnLoadEnd(int ptr, int httpStatusCode) {
				if (m_onLoadEnd) {
					m_onLoadEnd(ptr, httpStatusCode);
				}
			}
			void CallOnLoadError(int ptr, int errorCode, const wchar_t* errorText, const wchar_t* failedUrl) {
				if (m_onLoadError) {
					m_onLoadError(ptr, errorCode, errorText, failedUrl);
				}
			}
			void CallOnOpenInBrowser(int ptr, const wchar_t* url) {
				if (m_onOpenInBrowser) {
					m_onOpenInBrowser(ptr, url);
				}
			}
			wchar_t * CallOnLoadLocalFile(const wchar_t* file) {
				if (m_onLoadLocalFile) {
					return m_onLoadLocalFile(file);
				}
				return nullptr;
			}
		private:
			CEFFunctions();
			~CEFFunctions();
			OnDraw m_onDrawCallback;
			OnEvent m_onEventCallback;
			OnCursorChanged m_onCursorChangedCallback;
			OnBeforeBrowse m_onBeforeBrowseCallback;
			OnBeforeResourceLoad m_onBeforeResourceLoadCallback;
			OnLoadEnd m_onLoadEnd;
			OnLoadError m_onLoadError;
			OnOpenInBrowser m_onOpenInBrowser;
			OnLoadLocalFile m_onLoadLocalFile;
		};
	}
}