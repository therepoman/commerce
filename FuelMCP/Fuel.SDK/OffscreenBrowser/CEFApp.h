#pragma once

#include "include/cef_app.h"
#include "include/cef_scheme.h"

class CEFApp : public CefApp, public CefBrowserProcessHandler
{
public:
	virtual void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line);
	virtual void OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar) OVERRIDE;
	virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() OVERRIDE;
	virtual void OnContextInitialized() OVERRIDE;

	IMPLEMENT_REFCOUNTING(CEFApp);
};

