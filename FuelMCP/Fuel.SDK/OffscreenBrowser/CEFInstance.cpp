#include "CEFInstance.h"
#include "CEFClient.h"

CEFInstance * CEFInstance::Create(int instanceId, OnDrawInternal onDraw, int x, int y, int width, int height)
{
	try
	{
		CefRefPtr<CEFClient> browser = CEFClient::Create(instanceId, onDraw, CefRect(x, y, width, height));

		return new CEFInstance(browser);
	}
	catch (CEFClientCreationFailedException ex)
	{
		return nullptr;
	}
}

void CEFInstance::Destroy()
{
	m_browser->Destroy();
	m_browser = nullptr;
}

CEFInstance::CEFInstance(CefRefPtr<CEFClient> browser) :
m_browser(browser)
{}

void CEFInstance::Navigate(wchar_t* url)
{
	m_browser->GetBrowser()->GetMainFrame()->LoadURL(url);
}

void CEFInstance::LoadStaticContent(wchar_t* content, wchar_t* url)
{
	m_browser->GetBrowser()->GetMainFrame()->LoadStringW(content, url);
}

void CEFInstance::Resize(int width, int height)
{
	m_browser->Resize(width, height);

	// Force OnPaint to be called.
	// The overlay texture is destroyed on window resize and needs an update even if the CEF size hasn't changed.
	m_browser->GetBrowser()->GetHost()->Invalidate(PET_VIEW);
}

void CEFInstance::KeyboardEvent(cef_key_event_type_t type, int keyCode, bool ctrlDown)
{
	_cef_key_event_t cefKeyEvent;
	cefKeyEvent.type = type;
	cefKeyEvent.is_system_key = false;
	cefKeyEvent.modifiers = cef_event_flags_t::EVENTFLAG_NONE;
	cefKeyEvent.windows_key_code = keyCode;
	cefKeyEvent.native_key_code = keyCode;

	if (ctrlDown)
	{
		cefKeyEvent.modifiers |= cef_event_flags_t::EVENTFLAG_CONTROL_DOWN;
	}

	CefKeyEvent keyEvent;
	keyEvent.AttachTo(cefKeyEvent);
	m_browser->GetBrowser()->GetHost()->SendKeyEvent(keyEvent);
}

void CEFInstance::MouseClickEvent(int x, int y, cef_mouse_button_type_t type, bool mouseUp, int clickCount)
{
	_cef_mouse_event_t cefMouseEvent;
	cefMouseEvent.modifiers = cef_event_flags_t::EVENTFLAG_NONE;
	cefMouseEvent.x = x;
	cefMouseEvent.y = y;

	CefMouseEvent mouseEvent;
	mouseEvent.AttachTo(cefMouseEvent);
	m_browser->GetBrowser()->GetHost()->SendMouseClickEvent(mouseEvent, type, mouseUp, clickCount);
}

void CEFInstance::MouseMoveEvent(int x, int y, bool mouseLeave, bool leftMouseDown)
{
	_cef_mouse_event_t cefMouseEvent;
	cefMouseEvent.modifiers = cef_event_flags_t::EVENTFLAG_NONE;
	cefMouseEvent.x = x;
	cefMouseEvent.y = y;

	if (leftMouseDown)
	{
		cefMouseEvent.modifiers |= cef_event_flags_t::EVENTFLAG_LEFT_MOUSE_BUTTON;
	}

	m_browser->GetBrowser()->GetHost()->SendMouseMoveEvent(cefMouseEvent, mouseLeave);
}

void CEFInstance::MouseWheelEvent(int x, int y, int deltaX, int deltaY)
{
	_cef_mouse_event_t cefMouseEvent;
	cefMouseEvent.modifiers = cef_event_flags_t::EVENTFLAG_NONE;
	cefMouseEvent.x = x;
	cefMouseEvent.y = y;

	m_browser->GetBrowser()->GetHost()->SendMouseWheelEvent(cefMouseEvent, deltaX, deltaY);
}

void CEFInstance::AddDataToHeader(wchar_t* key, wchar_t* value)
{
	CefString keyString(key);
	CefString valueString(value);
	m_browser->AddDataToHeader(keyString, valueString);
}

void CEFInstance::RemoveDataFromHeader(wchar_t* key)
{
	CefString keyString(key);
	m_browser->RemoveDataFromHeader(keyString);
}

bool CEFInstance::SetCookie(wchar_t* url, wchar_t* cookieName, wchar_t* cookieValue, wchar_t* cookieDomain, wchar_t* cookiePath, bool secure, bool httpOnly, int expirationDayOfMonth, int expirationDayOfWeek, int expirationMonth, int expirationYear, int expirationHour, int expirationMinute, int expirationSecond, int expirationMillisecond)
{
	CefRefPtr<CefCookieManager> cookieManager = CefCookieManager::GetGlobalManager(nullptr);
	CefCookie cookie;
	CefString(&cookie.name) = cookieName;
	CefString(&cookie.value) = cookieValue;
	CefString(&cookie.domain) = cookieDomain;
	CefString(&cookie.path) = cookiePath;
	cookie.httponly = httpOnly;
	cookie.secure = secure;
	cef_time_t expiration;
	expiration.day_of_month = expirationDayOfMonth;
	expiration.day_of_week = expirationDayOfWeek;
	expiration.hour = expirationHour;
	expiration.millisecond = expirationMillisecond;
	expiration.minute = expirationMinute;
	expiration.month = expirationMonth;
	expiration.second = expirationSecond;
	expiration.year = expirationYear;
	cookie.expires = expiration;
	return cookieManager->SetCookie(url, cookie, nullptr);
}
