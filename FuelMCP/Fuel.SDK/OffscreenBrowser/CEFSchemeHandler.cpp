﻿#include "CEFSchemeHandler.h"
#include "CEFFunctions.h"

CEFSchemeHandler::CEFSchemeHandler() : offset(0)
{
}

bool CEFSchemeHandler::ProcessRequest(CefRefPtr<CefRequest> request,
	CefRefPtr<CefCallback> callback)
{
	bool handled = false;

	std::string url = request->GetURL();
	if (strstr(url.c_str(), "amazon__v273230536_.png") != NULL)
	{
		// TODO: [FUEL-1442] Use the custom local scheme to get Amazon logo
	}
	else if (strstr(url.c_str(), "cef-application.css") != NULL)
	{
		CefString fileToLoad("cef-application.css");
		wchar_t * file = Fuel::CEF::CEFFunctions::Instance().CallOnLoadLocalFile(fileToLoad.c_str());

		if (file != nullptr)
		{
			fileToLoad = file;
			data = fileToLoad.ToString();
			mime_type = "text/css";
			handled = true;
		}
	}

	if (handled)
	{
		callback->Continue();
		return true;
	}

	return false;
}

void CEFSchemeHandler::GetResponseHeaders(CefRefPtr<CefResponse> response,
	int64& response_length,
	CefString& redirectUrl)
{
	response->SetMimeType(mime_type);
	response->SetStatus(200);

	response_length = data.length();
}

bool CEFSchemeHandler::ReadResponse(void* data_out,
	int bytes_to_read,
	int& bytes_read,
	CefRefPtr<CefCallback> callback)
{
	bytes_read = 0;

	if (offset < data.length())
	{
		size_t transfer_size = (size_t)bytes_to_read < (data.length() - offset) ? (size_t)bytes_to_read : (data.length() - offset);
		std::memcpy(data_out, data.c_str() + offset, transfer_size);
		offset += transfer_size;

		bytes_read = transfer_size;
		return true;
	}
	return false;
}