﻿using System;
using System.Collections.Generic;
using Amazon.MCP.Plugin.Launcher.Models;
using Fuel.Coral.Client;
using Fuel.Ingest.Core;
using Fuel.Ingest.Core.Workflow;
using Fuel.Ingest.Core.Command;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionService.Client;

namespace Fuel.Ingest.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonLaunchConfigProvider = new JsonLaunchConfigProvider();
            var environment = new IngestEnvironment();
            var clientFactory = new CoralClientFactory();
            var sdasFactory = new SoftwareDistributionAdminServiceClientFactory(clientFactory);
            var workflowFactory = new WorkflowFactory(sdasFactory);

            var commands = new List<ICommand>
            {
                new ValidateCommand(jsonLaunchConfigProvider),
                new IngestVersionCommand(jsonLaunchConfigProvider, environment, workflowFactory),
                new ActivateVersionCommand(environment, workflowFactory),
                new ListVersionsCommand(environment, workflowFactory),
                new ListChannelsCommand(environment, workflowFactory),
                new ListProductsCommand(environment, workflowFactory),
                new DownloadVersionCommand(environment, workflowFactory),
                new VersionCommand()
            };
            var result = new IngestApp(commands).Exec(args);
            if (result.ExitCode != 0)
            {
                Console.WriteLine(result.ErrorMessage);
                if (args.Length == 0) commands.ForEach(command => Console.WriteLine($"  {command.Command}"));
                Environment.Exit(result.ExitCode);
            }
        }
    }
}
