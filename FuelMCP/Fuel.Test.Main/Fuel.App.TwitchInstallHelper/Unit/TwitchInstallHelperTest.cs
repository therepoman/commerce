﻿using System;
using System.IO;
using System.Threading;
using Amazon.Fuel.App.TwitchInstallHelper.Commands;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Entitlement;
using Moq;
using Xunit;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Tv.Twitch.Fuel.Manifest;

namespace Amazon.Test.Main.Fuel.App.TwitchInstallHelper.Test.Unit
{
    class TestableGameProductInfoDatabase : GameProductInfoDatabase
    {
        public TestableGameProductInfoDatabase(
            IPersistence persistence, 
            IEntitlementService entitlementService, 
            IMessengerHub hub, 
            IMetricsPlugin metrics,
            IOSDriver osDriver) 
            : base(persistence, metrics, entitlementService, hub, osDriver)
        {
        }
    }

    public class TwitchInstallHelperTest : IDisposable
    {
        private string _tempDir;
        private Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        private Mock<IFileSystemUtilities> _fileSystemUtil = new Mock<IFileSystemUtilities>();
        private Mock<ILaunchConfigProvider> _launchConfigProvider = new Mock<ILaunchConfigProvider>();
        private Mock<ILibraryPlugin> _library = new Mock<ILibraryPlugin>();
        private Mock<IGamePrereqInstaller> _prereqInstaller = new Mock<IGamePrereqInstaller>();
        private Mock<IManifestStorage> _manifestStorage = new Mock<IManifestStorage>();
        private Mock<IInstalledGamesPlugin> _installedGames = new Mock<IInstalledGamesPlugin>();
        private Mock<IManifestDeserializer> _manifestSerializer = new Mock<IManifestDeserializer>();

        public TwitchInstallHelperTest()
        {
            _tempDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        }

        [Fact]
        public void CommandRunner_TimesOut()
        {
            var newPath = Path.Combine(_tempDir, "CommandRunner_TimesOut");
            bool wasTimeout = false;
            _osDriver.Setup(d => d.PrimaryLibraryRoot).Returns(() =>
            {
                Thread.Sleep(50);
                return newPath;
            });

            var cmd = new Amazon.Fuel.App.TwitchInstallHelper.Commands.ElevatedCommand_CreatePrimaryDirectoryWithFullPerms(_osDriver.Object, _fileSystemUtil.Object)
            {
                TimeoutMS = 20
            };
            ElevatedCommandRunner<InstallHelperIpcMessage>.Execute(cmd, out wasTimeout);
            Assert.True(wasTimeout);
        }

        [Fact]
        public void CreateDir_CreatesPath()
        {
            var newPath = Path.Combine(_tempDir, "CreateDir_CreatesPath");
            bool wasTimeout = false;
            _fileSystemUtil.Setup(d => d.DirectoryExists(newPath)).Returns(false);
            _osDriver.Setup(d => d.PrimaryLibraryRoot).Returns(newPath);
            _osDriver.Setup(d => d.CreateDirectoryWithAllUsersPermissions(newPath, false));
            var cmd = new ElevatedCommand_CreatePrimaryDirectoryWithFullPerms(_osDriver.Object, _fileSystemUtil.Object);
            var result = ElevatedCommandRunner<InstallHelperIpcMessage>.Execute(cmd, out wasTimeout);
            Assert.False(wasTimeout);
            Assert.True(result is IpcCreateGamesLibraryPrimaryPathComplete);
            Assert.Equal(((IpcCreateGamesLibraryPrimaryPathComplete)result).Result, IpcCreateGamesLibraryPrimaryPathComplete.EResult.Succeeded);
            _osDriver.Verify(d => d.CreateDirectoryWithAllUsersPermissions(newPath, false));
        }

        [Fact(Skip = "Requires admin")]
        public void ADMINREQ_EnsureInitReq_SetsAllUserAccess()
        {
            var newPath = Path.Combine(_tempDir, "ADMINREQ_EnsureInitReq_SetsAllUserAccess");
            bool wasTimeout = false;
            _fileSystemUtil.Setup(d => d.DirectoryExists(newPath)).Returns(false);

            var realOsDriver = new WindowsFileSystemDriver();

            _osDriver.Setup(d => d.SystemDataPathFull).Returns(newPath);
            _osDriver.Setup(d => d.EnsureDirectoryHasAllUsersPermissions(newPath))
                .Callback(() => { realOsDriver.EnsureDirectoryHasAllUsersPermissions(newPath); });

            var cmd = new ElevatedCommand_EnsureInitialRequirements(_osDriver.Object);
            var result = ElevatedCommandRunner<InstallHelperIpcMessage>.Execute(cmd, out wasTimeout);
            Assert.False(wasTimeout);
            Assert.True(result is IpcEnsureInitialRequirementsComplete);
            Assert.Equal(((IpcEnsureInitialRequirementsComplete)result).Result, IpcEnsureInitialRequirementsComplete.EResult.Succeeded);
            Assert.True(realOsDriver.DirectoryHasAllUsersPermissions(newPath));
        }

        [Fact]
        public void CreateDir_SkipsExistingPath()
        {
            var newPath = Path.Combine(_tempDir, "CreateDir_SkipsExistingPath");
            _fileSystemUtil.Setup(f => f.DirectoryExists(newPath)).Returns(true);
            var cmd = new ElevatedCommand_CreatePrimaryDirectoryWithFullPerms(_osDriver.Object, _fileSystemUtil.Object);
            bool wasTimeout = false;
            var result = ElevatedCommandRunner<InstallHelperIpcMessage>.Execute(cmd, out wasTimeout);
            Assert.False(wasTimeout);
            Assert.True(result is IpcCreateGamesLibraryPrimaryPathComplete);
            Assert.Equal(((IpcCreateGamesLibraryPrimaryPathComplete)result).Result, IpcCreateGamesLibraryPrimaryPathComplete.EResult.Succeeded);
            _fileSystemUtil.Verify(d => d.CreateDirectory(newPath), Times.Never);
            _osDriver.Verify(d => d.CreateDirectoryWithAllUsersPermissions(newPath, true), Times.Never);
        }

        [Fact]
        public void CreateDir_ErrsOnNoPermissions()
        {
            var newPath = Path.Combine(_tempDir, "CreateDir_ErrsOnNoPermissions");
            var cmd = new ElevatedCommand_CreatePrimaryDirectoryWithFullPerms(_osDriver.Object, _fileSystemUtil.Object);
            bool wasTimeout = false;
            _osDriver.Setup(d => d.CreateDirectoryWithAllUsersPermissions(It.IsAny<string>(), false)).Throws(new UnauthorizedAccessException());
            var result = ElevatedCommandRunner<InstallHelperIpcMessage>.Execute(cmd, out wasTimeout);
            Assert.False(wasTimeout);
            Assert.True(result is IpcCreateGamesLibraryPrimaryPathComplete);
            Assert.Equal(((IpcCreateGamesLibraryPrimaryPathComplete)result).Result, IpcCreateGamesLibraryPrimaryPathComplete.EResult.Failure_NoAccess);
        }

        [Fact]
        public void InstallPrereqs_FailsOnMissingConfig()
        {
            var newPath = Path.Combine(_tempDir, "InstallPrereqs_FailsOnMissingConfig");
            var newFile = Path.Combine(newPath, "fuel.json");
            var newManifestFile = Path.Combine(newPath, "test.manifest");
            
            _fileSystemUtil.Setup(fs => fs.FileExists(newFile)).Returns(false);
            _fileSystemUtil.Setup(fs => fs.FileExists(newManifestFile)).Returns(true);

            var cmd = new ElevatedCommand_InstallPrereq(
                newManifestFile, newPath, 
                _launchConfigProvider.Object, 
                _fileSystemUtil.Object, 
                _manifestSerializer.Object,
                _prereqInstaller.Object);

            bool wasTimeout = false;

            var result = ElevatedCommandRunner<InstallHelperIpcMessage>.Execute(cmd, out wasTimeout);
            Assert.False(wasTimeout);

            _fileSystemUtil.Verify(fs => fs.FileExists(newFile), Times.Once);

            Assert.True(result is IpcInstallPrereqsComplete);
            Assert.Equal(((IpcInstallPrereqsComplete)result).Result, IpcInstallPrereqsComplete.EResult.Failure_MissingConfigFile);
        }

        public void Dispose()
        {
            try
            {
                Directory.Delete(_tempDir, true);
            }
            catch (Exception e)
            {
            }
        }

    }
}
