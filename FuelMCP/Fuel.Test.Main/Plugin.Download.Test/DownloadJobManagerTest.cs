﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download;
using Amazon.Fuel.Plugin.Download.Interfaces;
using Amazon.Fuel.Plugin.Download.Models;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.SoftwareDistributionService.Client;
using Moq;
using Tv.Twitch.Fuel.Sds;
using Xunit;

namespace Amazon.Test.Main.Plugin.Download.Test
{
    public class DownloadJobManagerTest
    {
        private readonly Mock<IEntitlementPlugin> _entitlementPluginMock = new Mock<IEntitlementPlugin>();
        private readonly Mock<IMessengerHub> _messengerHubMock = new Mock<IMessengerHub>();
        private readonly Mock<ISecureStoragePlugin> _secureStoragePluginMock = new Mock<ISecureStoragePlugin>();
        private readonly Mock<IMetricsPlugin> _metricsPluginMock = new Mock<IMetricsPlugin>();
        private readonly Mock<ISoftwareDistributionServiceClient> _sdsClientMock = new Mock<ISoftwareDistributionServiceClient>();
        private readonly Mock<ITwitchTokenProvider> _twitchLoginPluginMock = new Mock<ITwitchTokenProvider>();
        private readonly Mock<IInstallUtil> _installUtilMock = new Mock<IInstallUtil>();
        private readonly Mock<IDownloadJob> _downloadJobMock = new Mock<IDownloadJob>();
        private readonly Mock<IHashCache> _hashCacheMock = new Mock<IHashCache>();
        private readonly Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();

        private readonly Mock<IDownloadJob> _downloadJob = new Mock<IDownloadJob>();
        private readonly Mock<IDownloadJobFactory> _jobFactory = new Mock<IDownloadJobFactory>();
        private readonly string _testEntitlementId = "entitlementId";
        private readonly ProductId _testProdId = new ProductId("productId");

        public DownloadJobManagerTest()
        {
            _jobFactory.Setup(factory => factory.CreateJob(
                _sdsClientMock.Object,
                _twitchLoginPluginMock.Object,
                It.IsAny<IDownloadJobManager>(),
                It.IsAny<IInstallUtil>(),
                It.IsAny<bool>(),
                _testEntitlementId,
                _testProdId,
                It.IsAny<string>(),
                It.IsAny<IMetricsPlugin>(),
                It.IsAny<IOSDriver>(),
                It.IsAny<IMessengerHub>(),
                It.IsAny<Manifest>(),
                EDownloadSource.Primary,
                It.IsAny<CancellationToken>())).Returns(_downloadJob.Object);

            _installUtilMock.Setup(installUtil => installUtil.AvailableDiskSpace(It.IsAny<string>()))
                .Returns(1000);
            _installUtilMock.Setup(installUtil => installUtil.GetRequiredDiskSpaceForInstall(It.IsAny<bool>(), It.IsAny<long>()))
                .Returns(0);

            _metricsPluginMock.Setup(m => m.AddDiscreteValue(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));
        }

        [Fact]
        public void DownloadJobAsync_ReturnsNotEnoughDiskSpace_GivenNotEnoughDiskSpace()
        {
            var expectedResult = EDownloadJobResult.NotEnoughDiskSpace;
            long availableDiskSpace = 10000;
            long requiredDiskSpace = availableDiskSpace * 2;
            _installUtilMock.Setup(installUtil => installUtil.AvailableDiskSpace(It.IsAny<string>()))
                .Returns(availableDiskSpace);
            _installUtilMock.Setup(installUtil => installUtil.GetRequiredDiskSpaceForInstall(It.IsAny<bool>(), It.IsAny<long>()))
                .Returns(requiredDiskSpace);

            var downloadJobManager = new DownloadJobManager(
                entitlementPlugin: _entitlementPluginMock.Object,
                hub: _messengerHubMock.Object,
                metrics: _metricsPluginMock.Object,
                sds: _sdsClientMock.Object,
                twitchTokenProvider: _twitchLoginPluginMock.Object,
                installUtil: _installUtilMock.Object,
                downloadJobFactory: _jobFactory.Object,
                osDriver: _osDriver.Object);

            var actualResult = downloadJobManager.DownloadWithRetryAsync(
                isUpdate: true,
                retryWaitSec: new int[] {0, 0},
                entitlementId: _testEntitlementId,
                productId: _testProdId,
                folder: Path.GetTempPath(),
                previousManifest: null,
                cancellationToken: default(CancellationToken),
                metrics: _metricsPluginMock.Object,
                hub: _messengerHubMock.Object).GetAwaiter().GetResult();

            Assert.Equal(expectedResult, actualResult.Result);
        }

        [Fact]
        public void DownloadJobAsync_RetriesAfterFailure()
        {
            var downloadJobManager = new DownloadJobManager(
                entitlementPlugin: _entitlementPluginMock.Object,
                hub: _messengerHubMock.Object,
                metrics: _metricsPluginMock.Object,
                sds: _sdsClientMock.Object,
                twitchTokenProvider: _twitchLoginPluginMock.Object,
                installUtil: _installUtilMock.Object,
                downloadJobFactory: _jobFactory.Object,
                osDriver: _osDriver.Object);

            int attempt = 0;
            _downloadJob.Setup(job => job.QueuedFiles).Returns(new List<IDownloadJobQueuedFile>());
            _downloadJob.Setup(job => job.RunJobAsync()).Returns(() =>
            {
                var result = new DownloadJobResult();
                if (attempt++ == 0)
                {
                    result.Result = EDownloadJobResult.GeneralFailure;
                    result.DownloadResultCounts.Add(EDownloadResult.PatchFailure, 1);
                }
                return Task.FromResult(result);
            });

            var actualResult = downloadJobManager.DownloadWithRetryAsync(
                isUpdate: true,
                retryWaitSec: new int[] { 0, 0 },
                entitlementId: _testEntitlementId,
                productId: _testProdId,
                folder: Path.GetTempPath(),
                previousManifest: null,
                cancellationToken: default(CancellationToken),
                metrics: _metricsPluginMock.Object,
                hub: _messengerHubMock.Object).GetAwaiter().GetResult();

            Assert.Equal(actualResult.Result, EDownloadJobResult.Success);
            _downloadJob.Verify(job => job.RunJobAsync(), Times.Exactly(2));
        }
    }
}
