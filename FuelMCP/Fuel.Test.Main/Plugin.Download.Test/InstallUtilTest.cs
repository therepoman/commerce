﻿using Amazon.Fuel.Plugin.Download;
using Amazon.Test.Main.Utilities;
using Xunit;

namespace Amazon.Test.Main.Plugin.Download.Test
{
    public class InstallUtilTest
    {
        [Fact]
        public void AvailableDiskSpace_ReturnsValidSize_GivenValidPath()
        {
            var installUtil = InstallUtil.Instance;
            var availableDiskSpace = installUtil.AvailableDiskSpace(path: TestData.TestDataDir());
            Assert.NotEqual(-1, availableDiskSpace);
        }

        [Fact]
        public void AvailableDiskSpace_ReturnsInvalidSize_GivenValidPath()
        {
            var installUtil = InstallUtil.Instance;
            var availableDiskSpace = installUtil.AvailableDiskSpace(path: ".");
            Assert.Equal(-1, availableDiskSpace);
        }

        [Fact]
        public void GetRequiredDiskSpaceForInstall_ReturnsDownloadSize_GivenFullInstall()
        {
            var downloadSize = 10000;
            var expectedRequiredDiskSpace = downloadSize;
            var actualRequiredDiskSpace =
                InstallUtil.Instance.GetRequiredDiskSpaceForInstall(isUpdate: false, downloadSize: downloadSize);
            Assert.Equal(expectedRequiredDiskSpace, actualRequiredDiskSpace);
        }

        [Fact]
        public void GetRequiredDiskSpaceForInstall_ReturnsBufferedDownloadSize_GivenUpdate()
        {
            var downloadSize = 10000;
            var expectedRequiredDiskSpace = downloadSize * 1.5;
            var actualRequiredDiskSpace =
                InstallUtil.Instance.GetRequiredDiskSpaceForInstall(isUpdate: true, downloadSize: downloadSize);
            Assert.Equal(expectedRequiredDiskSpace, actualRequiredDiskSpace);
        }
    }
}
