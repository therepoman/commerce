﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using Amazon.MCP.Plugin.Download.Models;
using Amazon.MCP.Plugin.Download.Models.Interfaces;
using Amazon.MCP.Test.Main.Mocks;
using Moq;
using SharpBits.Base;
using Xunit;

namespace Amazon.MCP.Test.Main
{
    public class DownloadHubTest
    {
        [Fact]
        public void CurrentDownloadsTest()
        {
            var hub = GetTestableDownloadHub();
            Assert.Equal(new JavaScriptSerializer().Serialize(hub.GetCurrentDownloads()), "{}");
        }

        [Fact]
        public void BitsConversionTest()
        {
            using (ShimsContext.Create())
            {
                var mockBitsJob = new Mock<BitsJob>();
                mockBitsJob.Setup(m => m.Progress.BytesTransferred).Returns(0);
                mockBitsJob.Setup(m => m.Progress.BytesTotal).Returns(100);
                mockBitsJob.Setup(m => m.State.ToString()).Returns("In Progress");
                var dd = DownloadDetails.FromBitsJob(mockBitsJob.Object);
                Assert.Equal(dd.DownloadStatus, "In Progress");
            }
        }

        private TestableDownloadHub GetTestableDownloadHub()
        {
            var mockCurrentDownloads = new Mock<ICurrentDownloads>();
            mockCurrentDownloads.Setup(m => m.GetCurrentDownloads()).Returns(new Dictionary<string, DownloadDetails>
            {
                
            });
            return new TestableDownloadHub(mockCurrentDownloads);
        }
    }
}
