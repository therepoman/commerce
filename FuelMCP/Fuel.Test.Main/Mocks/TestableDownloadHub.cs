﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Amazon.MCP.Plugin.Download.Hubs;
using Amazon.MCP.Plugin.Download.Models.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;

namespace Amazon.MCP.Test.Main.Mocks
{
    class TestableDownloadHub : DownloadHub
    {
        public TestableDownloadHub(Mock<ICurrentDownloads> currentDownloads) : base(currentDownloads.Object)
        {
            //const string connectionId = "1234";
            //const string hubName = "Chat";
            //var mockConnection = new Mock<IConnection>();
            //var mockUser = new Mock<IPrincipal>();
            //var mockCookies = new Mock<IRequestCookieCollection>();

            //var mockRequest = new Mock<IRequest>();
            //mockRequest.Setup(r => r.User).Returns(mockUser.Object);
            //mockRequest.Setup(r => r.Cookies).Returns(mockCookies.Object);

            //Clients = new ClientAgent(mockConnection.Object, hubName);
            //Context = new HubCallerContext(mockRequest.Object, connectionId);

            //var trackingDictionary = new TrackingDictionary();
            //Caller = new StatefulSignalAgent(mockConnection.Object, connectionId, hubName, trackingDictionary);  
        }
    }
}
