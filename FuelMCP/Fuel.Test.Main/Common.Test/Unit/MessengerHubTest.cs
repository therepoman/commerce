﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Implementations;
using Moq;
using TinyMessenger;
using Xunit;

namespace Amazon.Test.Main.Common.Test
{
    public class MessengerHubTest
    {
        [Fact]
        public void AllMethods_WhenCalled_CallAppropriateTinyMessengerMethod()
        {
            var fakeMessage = new Mock<ITinyMessage>();
            var fakeTinyHub = new Mock<ITinyMessengerHub>();
            var messengerHub = new MessengerHub(fakeTinyHub.Object);

            messengerHub.Publish(fakeMessage.Object);
            fakeTinyHub.Verify(m => m.Publish(It.IsAny<ITinyMessage>()), Times.Once);

            messengerHub.PublishAsync(fakeMessage.Object);
            fakeTinyHub.Verify(m => m.PublishAsync(It.IsAny<ITinyMessage>()), Times.Once);

            messengerHub.Subscribe<ITinyMessage>(t => { });
            fakeTinyHub.Verify(m => m.Subscribe(It.IsAny<Action<ITinyMessage>>()), Times.Once);

            messengerHub.Unsubscribe<ITinyMessage>(null);
            fakeTinyHub.Verify(m => m.Unsubscribe<ITinyMessage>(It.IsAny<TinyMessageSubscriptionToken>()), Times.Once);
        }
    }
}
