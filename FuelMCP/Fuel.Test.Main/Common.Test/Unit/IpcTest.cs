﻿using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Messages;
using Xunit;

namespace Amazon.Test.Main.Common.Test.Unit
{
    public class IpcTest
    {
        public class TestReflectedIpcMessage : IpcMessage
        {
            public string TestDerivedString = "derived_string";
            public int TestDerivedInt = int.MaxValue;

            public TestReflectedIpcMessage(object sender) : base(sender) { }
        }

        [Fact]
        public void IPCMessageReflection_ShouldSerializeAndDeserialize()
        {
            var msg = new TestReflectedIpcMessage(this);
            var bytes = msg.GetBytes();
            var decoded = IpcMessage.Decode(this, bytes);
            Assert.True(decoded is TestReflectedIpcMessage);
            Assert.True(decoded.Equals(new TestReflectedIpcMessage(this)));
        }
    }
}
