﻿using Amazon.Fuel.Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Amazon.Test.Main.Common.Test.Unit
{
    public class FileCompareUtilTest : IDisposable
    {
        public string _randoPath;

        public FileCompareUtilTest()
        {
            _randoPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
        }

        [Fact]
        public void FileCompareWorks()
        {
            // just a dumb file compare func, so I bundled all this into one test so that we don't bog down units on FS access
            Directory.CreateDirectory(_randoPath);
            var filePathA = Path.Combine(_randoPath, "fileA.txt");
            var filePathB = Path.Combine(_randoPath, "fileB.txt");

            // file missing (exception bleeds through)
            var testStr = "test";
            File.WriteAllText(filePathA, testStr);
            Assert.Throws<FileNotFoundException>(() => FileCompareUtil.BinaryCompare(filePathA, filePathB));

            // same (pass)
            File.WriteAllText(filePathB, testStr);
            Assert.True(FileCompareUtil.BinaryCompare(filePathA, filePathB));

            // one char diff
            File.WriteAllText(filePathB, "tesT");
            Assert.False(FileCompareUtil.BinaryCompare(filePathA, filePathB));

            // one byte append diff
            File.WriteAllText(filePathA, testStr + "A");
            Assert.False(FileCompareUtil.BinaryCompare(filePathA, filePathB));
        }


        public void Dispose()
        {
            try
            {
                Directory.Delete(_randoPath, true);
            }
            catch { }
        }
    }
}
