﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Utilities;
using Xunit;

namespace Amazon.Test.Main.Common.Test.Unit
{
    public class LogicUtilitiesTest
    {
        [Fact]
        public void TestDoWithRetryFailuresAsync()
        {
            int runCount = 0;

            // failure
            foreach (var retryCount in new int[] {0, 1, 2})
            {
                runCount = 0;
                var success = LogicUtilities.DoWithRetryAsync(
                    taskWithAttemptCount: async (attempts) =>
                    {
                        ++runCount;
                        return false;
                    },
                    maxRetryCount: retryCount,
                    retryWaitSec: new int[] {0},
                    cancelToken: null).Result;

                Assert.Equal(runCount - 1, retryCount);
                Assert.False(success);
            }
        }

        public void TestDoWithRetrySuccessesAsync()
        {
            // success
            int runCount = 0;
            var success2 = LogicUtilities.DoWithRetryAsync(
                taskWithAttemptCount: async (attempts) =>
                {
                    ++runCount;
                    return attempts == 2;
                },
                maxRetryCount: 1,
                retryWaitSec: new int[] {0},
                cancelToken: null).Result;

            Assert.True(success2);
            Assert.Equal(runCount, 2);
        }

        public void TestDoWithRetryCancellationAsync()
        {
            // cancellation
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            int runCount = 0;
            var success3 = LogicUtilities.DoWithRetryAsync(
                taskWithAttemptCount: async (attempts) =>
                {
                    ++runCount;
                    if (attempts == 1)
                    {
                        tokenSource.Cancel();
                    }
                    return attempts == 2;
                },
                maxRetryCount: 1,
                retryWaitSec: new int[] { 0 },
                cancelToken: tokenSource.Token).Result;

            Assert.True(success3);
            Assert.Equal(runCount, 1);

        }

        [Fact]
        public void TestDictGetValueOrDefault()
        {
            var d = new Dictionary<string, int>();
            d["testA"] = 2;
            Assert.Equal(d.GetValueOrDefault("testA"), 2);
            Assert.Equal(d.GetValueOrDefault("testB"), 0);
            Assert.Equal(d.GetValueOrDefault("testB", 1), 1);
        }

    }
}
