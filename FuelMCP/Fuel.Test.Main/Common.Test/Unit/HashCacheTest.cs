﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Plugin.Persistence;
using Xunit;

namespace Amazon.Test.Main.Common.Test.Unit
{
    public class HashCacheTest
    {
        Mock<ISecureStoragePlugin> _storage = new Mock<ISecureStoragePlugin>();

        [Fact]
        public void GetsDataWhenTimestampSame()
        {
            TestRetrieval(timestampIsDifferent: false);
        }

        [Fact]
        public void GetsNullWhenTimestampDifferent()
        {
            TestRetrieval(timestampIsDifferent: true);
        }

        private void TestRetrieval(bool timestampIsDifferent)
        { 
            string randomFile = Path.Combine(Path.GetTempPath(), "file.exe");
            File.WriteAllText(randomFile, "test");

            string expectedHash = GenerateRandomHash(256);
            string storageBlob = null;

            _storage.Setup(s => s.PutAll(HashCache.StorageContext, It.IsAny<IEnumerable<SecureStorageItem>>()))
                .Callback<string, SecureStorageItem[]>((context, items) =>
                {
                    storageBlob = items[0].Value;
                });
            _storage.Setup(s => s.Get(HashCache.StorageContext, randomFile)).Returns(() => storageBlob);

            var hc = new HashCache(_storage.Object, new MessengerHub());
            hc.UpdateCache(randomFile, expectedHash);

            if (timestampIsDifferent)
            {
                File.WriteAllText(randomFile, "changed");
            }

            var retrievedHash = hc.GetCachedFileHash(randomFile);
            var retryCount = 0;
            //It can take up to 1 second for the cache to update
            while (retrievedHash == null && retryCount < 11)
            {
                Thread.Sleep(100);
                retrievedHash = hc.GetCachedFileHash(randomFile);
                retryCount++;
            }
            if (timestampIsDifferent)
            {
                Assert.Null(retrievedHash);
            }
            else
            {
                Assert.True(retrievedHash == expectedHash);
            }

            try { File.Delete(randomFile); } catch { }
        }

        private string GenerateRandomHash(int hashByteCount)
        {
            var randomBytes = new byte[hashByteCount];
            new Random().NextBytes(randomBytes);
            return Encoding.UTF8.GetString(randomBytes);
        }

        [Theory,
             InlineData(0),
             InlineData(256),
             InlineData(512)]
        public void VerifyHashDataSerialization(int hashByteCount)
        {
            var hashData = new HashCache.HashCacheData()
            {
                Hash = GenerateRandomHash(hashByteCount),
                LastTimeStamp = DateTime.UtcNow
            };

            var serialized = hashData.Serialize();
            var deserialized = HashCache.HashCacheData.Deserialize(serialized);

            Assert.True(hashData.Hash == deserialized.Hash);
            Assert.True(hashData.LastTimeStamp == deserialized.LastTimeStamp);
        }
    }
}
