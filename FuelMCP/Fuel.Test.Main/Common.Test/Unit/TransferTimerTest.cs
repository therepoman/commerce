﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Amazon.Test.Main.Common.Test.Unit
{    
    public class TransferTimerTest 
    {
        private class TransferSource : ITransferedBytes
        {
            public long bytes = 0;
            public long BytesPerTick = 1024 * 1024;

            public long ReceivedBytes { get; set; }

            public void Tick()
            {
                ReceivedBytes += BytesPerTick;
            }
        }

        [Fact]
        public void TransferTestTicks()
        {
            var source = new TransferSource();

            TransferTimer timer = new TransferTimer(source, 1000, 1.0f, 120, false);
            timer.IsEnabled = true;
            source.Tick();
            timer.Tick();

            Assert.Equal(1, timer.BytesReceivedHistory.Count);

            for (int i = 0; i < 1000; i++)
            {
                source.Tick();
                timer.Tick();
            }

            for (int i = 0; i < timer.BytesReceivedHistory.Count; i++)
            {
                Assert.Equal(source.BytesPerTick, timer.BytesReceivedHistory[i]);
            }
        }

        [Fact]
        public void TransferTestCancel()
        {
            var source = new TransferSource();

            TransferTimer timer = new TransferTimer(source, 1000, 1.0f, 120, false);
            timer.IsEnabled = true;
            source.Tick();
            timer.Tick();

            Assert.Equal(1, timer.BytesReceivedHistory.Count);

            timer.Dispose();
            for (int i = 0; i < 1000; i++)
            {
                source.Tick();
                timer.Tick();
            }

            Assert.Equal(1, timer.BytesReceivedHistory.Count);
        }

        [Fact]
        public void TransferTestEstimate()
        {
            var source = new TransferSource();

            TransferTimer timer = new TransferTimer(source, 1000, 0, 120, false);
            timer.IsEnabled = true;

            for (int i = 0; i < 1000; i++)
            {
                source.Tick();
                timer.Tick();

                long bLeft = 1000 * 1024 * 1024- source.ReceivedBytes;

                double timeLeft = timer.EstimateSecLeft(bLeft);
                Assert.Equal(1000 - i - 1, timeLeft, 1);
                Assert.Equal(1, timer.MegaBytesPerSec);
            }
        }

        /// <summary>
        /// If we run the timer twice as often, things should be twice as fast
        /// </summary>
        [Fact]
        public void TransferTestEstimate2x()
        {
            var source = new TransferSource();

            TransferTimer timer = new TransferTimer(source, 500, 0, 120, false);
            timer.IsEnabled = true;

            for (int i = 0; i < 1000; i++)
            {
                source.Tick();
                timer.Tick();

                long bLeft = 1000 * 1024 * 1024 - source.ReceivedBytes;
                double timeLeft = timer.EstimateSecLeft(bLeft);
                Assert.Equal(500 - (i * 0.5) - (1 * 0.5), timeLeft, 1);

                Assert.Equal(2, timer.MegaBytesPerSec);
            }
        }

        /// <summary>
        /// If we run the timer 4x as often, things should be 4x as fast
        /// </summary>
        [Fact]
        public void TransferTestEstimate4x()
        {
            var source = new TransferSource();

            TransferTimer timer = new TransferTimer(source, 250, 0, 120, false);
            timer.IsEnabled = true;

            for (int i = 0; i < 1000; i++)
            {
                source.Tick();
                timer.Tick();

                long bLeft = 1000 * 1024 * 1024 - source.ReceivedBytes;
                double timeLeft = timer.EstimateSecLeft(bLeft);
                Assert.Equal((1000 * 0.25) - (i * 0.25) - (1 * 0.25), timeLeft, 1);

                Assert.Equal(4, timer.MegaBytesPerSec);
            }
        }

    }
}
