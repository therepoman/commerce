﻿using System;
using System.Globalization;
using Amazon.Fuel.Common;
using Xunit;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Test.Main.Common.Test.Unit
{
    public class StringUtilTest
    {
        [Fact]
        public void VerifyToAscii()
        {
            Assert.Equal("Thireshan Reddy", "Thirèshàn Rèddy".ToAscii());
            Assert.Equal("Handel", "Händel".ToAscii());
        }
    }
}
