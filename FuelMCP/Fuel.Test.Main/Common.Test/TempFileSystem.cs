﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Test.Main.Common.Test
{
    public class TempFileSystem : IDisposable
    {
        public string TempTestFolderName = Guid.NewGuid().ToString();

        public string TempFolder;
        public System.IO.DirectoryInfo TempDir;

        public TempFileSystem( Type owner)
        {
            TempTestFolderName = owner.Name + "_" + TempTestFolderName;
            TempFolder = System.IO.Path.Combine(Environment.CurrentDirectory, "temp", TempTestFolderName);
            TempDir = new System.IO.DirectoryInfo(TempFolder);
        }

        ~TempFileSystem()
        {
            CleanupFiles();
        }

        public void Dispose()
        {
            CleanupFiles();
        }

        internal void CleanupFiles()
        {
            if (TempDir != null && TempDir.Exists)
            {
                TempDir.Delete(true);

                while (System.IO.Directory.Exists(TempDir.ToString()))
                {
                }
            }
        }
    }
}
