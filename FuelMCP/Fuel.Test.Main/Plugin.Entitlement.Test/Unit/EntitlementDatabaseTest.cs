﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Entitlement;
using Amazon.Fuel.Plugin.Entitlement.Services;
using Amazon.Fuel.Plugin.Persistence.Models;
using Amazon.Test.Main.Plugin.Launcher.Test;
using Amazon.Test.Main.Utilities;
using Lucene.Net.Documents;
using Moq;
using Xunit;

namespace Amazon.Test.Main.Plugin.Entitlement.Test.Unit
{
    class TestableGameProductInfoDatabase : GameProductInfoDatabase
    {
        public TestableGameProductInfoDatabase(
            IPersistence persistence, 
            IEntitlementService entitlementService, 
            IMessengerHub hub, 
            IMetricsPlugin metrics,
            IOSDriver osDriver) 
            : base(persistence, metrics, entitlementService, hub, osDriver)
        {
        }
    }

    public class EntitlementDatabaseTest
    {
        private TestableGameProductInfoDatabase db;
        private string dir;
        private TestablePersistence persistence;
        private Mock<IEntitlementService> entitlementService = new Mock<IEntitlementService>();
        private Mock<IMessengerHub> messengerHub = new Mock<IMessengerHub>();
        private Mock<IMetricsPlugin> metricsPlugin = new Mock<IMetricsPlugin>();
        private Mock<IOSDriver> osDriver = new Mock<IOSDriver>();

        public EntitlementDatabaseTest()
        {
            dir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            persistence = new TestablePersistence(dir);
            db = new TestableGameProductInfoDatabase(persistence, entitlementService.Object, messengerHub.Object, metricsPlugin.Object, osDriver.Object);
        }
        public void Dispose()
        {
            try
            {
                Directory.Delete(dir, true);
            }
            catch (Exception e)
            {
            }
        }

        [Fact]
        public void GetByProductIdFetchesByProductId()
        {
            var g1 = Factories.ValidLiveGameProductInfo();

            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Read)).Returns(true);
            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Write)).Returns(true);

            db.AddOrReplaceById(g1);

            var all = db.All;
            Assert.Equal(1, all.Count);

            Assert.Equal(g1, db.Get(g1.ProductId));
        }

        [Fact]
        public void GetByProductIdFetchesDeveloper()
        {
            var g1 = Factories.ValidLiveGameProductInfo();
            var g2 = Factories.ValidLiveGameProductInfo();
            g2.IsDeveloper = true;
            g2.ProductIdStr = g1.ProductIdStr;

            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Read)).Returns(true);
            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Write)).Returns(true);

            db.AddOrReplaceById(g1);
            db.AddOrReplaceById(g2);

            Assert.Equal(g2, db.Get(g1.ProductId));
        }
        
        [Fact]
        public void AllActiveOnlyFetchesLive()
        {
            var g1 = Factories.ValidLiveGameProductInfo();
            var g2 = Factories.ValidGameProductInfo();
            
            db.AddOrReplaceById(g1);
            db.AddOrReplaceById(g2);

            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Read)).Returns(true);
            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Write)).Returns(true);

            var allResults = db.All;
            Assert.Equal(2, allResults.Count);
            
            var result = db.ActiveEntitlements;
            Assert.Equal(1, result.Count);
            Assert.Contains(g1, result);
        }

        [Fact]
        public void AllWithBothNonDeveloperAndDeveloperReturnsDeveloper()
        {
            var g1 = Factories.ValidLiveGameProductInfo();
            var g2 = Factories.ValidLiveGameProductInfo();
            g2.IsDeveloper = true;
            g2.ProductIdStr = g1.ProductIdStr;

            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Read)).Returns(true);
            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Write)).Returns(true);

            db.AddOrReplaceById(g1);
            db.AddOrReplaceById(g2);

            var allResults = db.All;
            Assert.Equal(2, allResults.Count);

            var result = db.ActiveEntitlements;
            Assert.Equal(1, result.Count);
            Assert.Contains(g2, result);
        }

        [Fact]
        public void DuplicateProductsOnlyGetsFirst()
        {
            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Read)).Returns(true);
            osDriver.Setup(d => d.HasPermissions(It.IsAny<string>(), FileSystemRights.Write)).Returns(true);

            var g1 = Factories.ValidLiveGameProductInfo();
            var e1 = g1.Id;
            db.AddOrReplaceById(g1);
            var e2 = Factories.ValidEntitlement();
            g1.Id = e2;
            db.AddOrReplaceById(g1);

            var allResults = db.All;
            Assert.Equal(2, allResults.Count);

            var result = db.ActiveEntitlements;
            Assert.Equal(1, result.Count);
            var ids = result.Select(r => r.Id);
            Assert.True(ids.Contains(e1) || ids.Contains(e2));
        }

        [Fact]
        public async void SyncGoodsSyncsToDatabase()
        {
            var g1 = Factories.ValidLiveGameProductInfo();
            entitlementService.Setup(g => g.SyncGoodsWithRetry(null)).ReturnsAsync(new SyncGoodsResult() { ProductInfos = new List<GameProductInfo> { g1 } });

            await db.SyncGoods();
            
            var result = db.ActiveEntitlements;
            Assert.Equal(1, result.Count);
            Assert.Contains(g1, result);
        }

        [Fact]
        public void SyncGoodsWithRevoke()
        {
            var live = Factories.ValidLiveGameProductInfo();
            var revoked = Factories.Copy<GameProductInfo>(live, l =>
            {
                l.State = "REVOKED";
            });
            
            entitlementService.Setup(g => g.SyncGoodsWithRetry(null)).ReturnsAsync(new SyncGoodsResult() { ProductInfos = new List<GameProductInfo> { live, revoked } });

        }

        [Fact]
        public async void SyncGoods_RevokedAndEntitled_GetsEntitled()
        {
            string testData = TestData.ReadTestFile(Path.Combine("Plugin.Entitlement.Test", "Unit", "FLCL-643.json"));
            var productInfo = new SyncGoodsParser(new LaunchGameTest.TestableMetrics()).Parse(testData);
            entitlementService.Setup(e => e.SyncGoodsWithRetry(null)).ReturnsAsync(new SyncGoodsResult
            {
                Result = SyncGoodsResult.EResult.Success,
                ProductInfos = productInfo,
            });

            var productId = new ProductId("d229a310-2468-4f0c-b49b-4a6dcdd47809");
            Assert.True(await db.SyncGoods());
            var singleFetch = db.Get(productId);
            Assert.NotNull(singleFetch);
            Assert.Equal(GameProductInfo.STATE_LIVE, singleFetch.State);

            var results = db.ActiveEntitlements;
            Assert.Equal(1, results.Count);

            var gameProductInfo = results[0];
            Assert.Equal(productId, gameProductInfo.ProductId);
            Assert.Equal(GameProductInfo.STATE_LIVE, gameProductInfo.State);
        }
    }
}
