﻿using System.IO;
using System.Linq;
using System.Net.Http;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Entitlement;
using Moq;
using Xunit;
using Amazon.Fuel.Common.Contracts.Manifest;
using System.Xml.Linq;
using Amazon.Fuel.SoftwareDistributionService.Client;
using Amazon.Fuel.SoftwareDistributionService.Model;
using System.Threading;
using Amazon.Fuel.Common.Exceptions;
using Amazon.Test.Main.Fuel.Manifest;
using Google.Protobuf;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Test.Main.Plugin.Entitlement.Test.Unit
{
    public class EntitlementPluginTest
    {
        [Fact (Skip="bug in moq nullable types, on ln 26?")]
        public void EntitlementPlugin_DownloadManifest_Succeeds()
        {
            var messenger = new Mock<IMessengerHub>();
            var sds = new Mock<ISoftwareDistributionServiceClient>();
            var sdsClient = new Mock<ISoftwareDistributionServiceClient>();
            var mockTwitchLoginPlugin = new Mock<ITwitchLoginPlugin>();
            var manifestStorage = new Mock<IManifestStorage>();
            var httpMessageHandler = new Mock<HttpMessageHandler>();
            var expectedManifest = TestManifestGenerator.RandomManifest();
            var tr = new StreamReader(new MemoryStream());
            expectedManifest.WriteTo(new CodedOutputStream(tr.BaseStream));
            var expectedManifestData = tr.ReadToEnd();
            var expectedResult = new GetDownloadManifestResult("test-channel-id", "test-version-id", expectedManifestData);
            var adgProductId = new ProductId("testadgproductId");
            var entitlementId = "123";
        
            mockTwitchLoginPlugin.Setup(t => t.IsLoggedIn(null)).Returns(true);
        
            sds.Setup(k => k.GetDownloadManifest(
                new GetDownloadManifestRequest(entitlementId), new CancellationToken()))
                .ReturnsAsync(expectedResult);
        
            var entitlementPlugin = new EntitlementPlugin(messenger.Object, mockTwitchLoginPlugin.Object, sds.Object, sdsClient.Object, httpMessageHandler.Object, manifestStorage.Object);
        
            var manifestResult = entitlementPlugin.DownloadManifestAsync(entitlementId, adgProductId).Result;
            Assert.Equal(expectedManifest.Packages.First().Files.Count, manifestResult.ManifestData.Packages.First().Files.Count);
        }

        [Fact]
        public void EntitlementPlugin_DownloadManifest_LoginFails()
        {
            var messenger = new Mock<IMessengerHub>();
            var sds = new Mock<ISoftwareDistributionServiceClient>();
            var sdsClient = new Mock<ISoftwareDistributionServiceClient>();
            var mockTwitchLoginPlugin = new Mock<ITwitchLoginPlugin>();
            var manifestStorage = new Mock<IManifestStorage>();
            var httpMessageHandler = new Mock<HttpMessageHandler>();
            var expectedManifest = TestManifestGenerator.RandomManifest();
            var tr = new StreamReader(new MemoryStream());
            expectedManifest.WriteTo(new CodedOutputStream(tr.BaseStream));
            var expectedManifestData = tr.ReadToEnd();
            var expectedResult = new GetDownloadManifestResult("test-channel-id", "test-version-id", expectedManifestData);
            var adgProductId = new ProductId("testadgproductId");
            var entitlementId = "123";
        
            mockTwitchLoginPlugin.Setup(t => t.EnsureLoggedIn()).Throws<TwitchAuthException>();
        
            sds.Setup(k => k.GetDownloadManifest(new GetDownloadManifestRequest(entitlementId), default(CancellationToken)))
                .ReturnsAsync(expectedResult);
        
            var entitlementPlugin = new EntitlementPlugin(messenger.Object, mockTwitchLoginPlugin.Object, sds.Object, sdsClient.Object, httpMessageHandler.Object, manifestStorage.Object);
        
            Assert.Throws<TwitchAuthException>(() => entitlementPlugin.DownloadManifestAsync(entitlementId, adgProductId).GetAwaiter().GetResult());
        
            //TODO: Is there a way to test the full download call?  It locks up since there is no way to cancel without
            //a real MessengerHub
        }

    }
}
