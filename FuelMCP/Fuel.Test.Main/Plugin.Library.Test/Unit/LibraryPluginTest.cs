﻿using System.Collections.Generic;
using System.Linq;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Library;
using Moq;
using Xunit;
using Amazon.Test.Main.Utilities;
using Amazon.Fuel.Common.Data;
using System;
using System.IO;
using Amazon.Fuel.Common.IPC;

namespace Amazon.Test.Main.Plugin.Library.Test.Unit
{
    public class LibraryPluginTest
    {
        private Mock<IMessengerHub> messenger = new Mock<IMessengerHub>();
        private Mock<IEntitlementsDatabase> entitlementDb = new Mock<IEntitlementsDatabase>();
        private Mock<ISecureStoragePlugin> storage = new Mock<ISecureStoragePlugin>();
        private Mock<IMetricsPlugin> metrics = new Mock<IMetricsPlugin>();
        private Mock<IProcessLauncher> procLauncher = new Mock<IProcessLauncher>();
        private Mock<ILaunchConfigProvider> launchConfigProvider = new Mock<ILaunchConfigProvider>();
        private Mock<IInstalledGamesPlugin> installedGames = new Mock<IInstalledGamesPlugin>();
        private Mock<IHelperLauncherProvider> helperLauncherProvider = new Mock<IHelperLauncherProvider>();
        private Mock<IRegisterInstallation> reg = new Mock<IRegisterInstallation>();
        private Mock<IInstalledGameSynchronizer> _installedGameSynchronizer = new Mock<IInstalledGameSynchronizer>();
        private Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        private Mock<IShortcutManager> shortcutMgr = new Mock<IShortcutManager>();
        private Mock<IInstallHelperIpcController> installHelperIpcController = new Mock<IInstallHelperIpcController>();
        private Mock<IManifestStorage> manifestStorage = new Mock<IManifestStorage>();
        private GameInstaller gameInstaller;
        private LibraryPlugin libraryPlugin;
        private GamePrereqInstaller gamePrereqInstaller;

        public LibraryPluginTest()
        {
            libraryPlugin = new LibraryPlugin(
                messenger.Object,
                entitlementDb.Object,
                installedGames.Object,
                _installedGameSynchronizer.Object);

            gameInstaller = new GameInstaller(
                libraryPlugin,
                messenger.Object,
                metrics.Object,
                launchConfigProvider.Object,
                shortcutMgr.Object,
                installHelperIpcController.Object,
                installedGames.Object,
                manifestStorage.Object,
                entitlementDb.Object
            );

            gamePrereqInstaller = new GamePrereqInstaller(
                storage.Object,
                procLauncher.Object);

        }

        [Fact]
        public void GetGame_NoResult()
        {
            Assert.Null(libraryPlugin.GetGame(Factories.ValidProductId()));
        }

        [Fact]
        public void GetByProductId()
        {
            var pid = Factories.ValidProductId();
            var gpi = Factories.ValidGameProductInfo();
            var expected = new GameMapper().Convert(gpi);

            entitlementDb.Setup(d => d.Get(pid)).Returns(gpi);

            Assert.Equal(expected, libraryPlugin.GetGame(pid));
        }

        // TODO: this is broken until Factories.ValidInstallHelperSession() gets fixed
        [Theory(Skip = "Factories.ValidInstallHelperSession needs proxy wrapper"),
            InlineData(true),
            InlineData(false)]
        public void InstallsShortcuts(bool installDesktopShortcut)
        {
            var gpi = Factories.ValidGameProductInfo();
            var validSession = Factories.ValidInstallHelperSession();
            GameInstallInfo installGame = Factories.ValidGameInstallInfo(gpi);

            LaunchConfig cfg = new LaunchConfig();

            _osDriver.Setup(o => o.IsDevMode).Returns(true);
            _osDriver.Setup(o => o.GameInstallHelperPath).Returns(Path.GetTempFileName());
            entitlementDb.Setup(d => d.Get(new ProductId(installGame.Id))).Returns(gpi);
            launchConfigProvider.Setup(c => c.GetLaunchConfig(It.IsAny<string>(), It.IsAny<string>())).Returns(cfg);

            procLauncher.Setup(
                l => l.LaunchProcess(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string[]>(),
                    true,
                    It.IsAny<int[]>(),
                    It.IsAny<Dictionary<string, string>>(), //null,
                    ELaunchUAC.RUN_WITH_CURRENT_PRIVS__THIS_ALLOWS_ELEVATION,
                    false,
                    false))
                .Returns(new LaunchResult() { ExitCode = 0, Status = LaunchResult.EStatus.Success });
            metrics.Setup(m => m.AddCounter(It.IsAny<string>(), It.IsAny<string>()));
            installedGames.Setup(i => i.Save(installGame));
            installedGames.Setup(i => i.Get(new ProductId(installGame.Id))).Returns(installGame);
            shortcutMgr.Setup(s => s.TryCreateShortcuts(gpi, installGame, installDesktopShortcut)).Returns(true);

            var ret = gameInstaller.InstallGame(
                new ProductId(installGame.Id),
                installGame,
                helperLauncherProvider: helperLauncherProvider.Object,
                productVersionId: installGame.InstallVersion,
                installFlags: EInstallGameFlags.All 
                                & (installDesktopShortcut ? EInstallGameFlags.All : ~EInstallGameFlags.CreateDesktopShortcut));

            Assert.True(ret == EInstallUpdateProductResult.Succeeded);
            shortcutMgr.Verify(s => s.TryCreateShortcuts(gpi, installGame, installDesktopShortcut), Times.Once);
        }

        [Fact]
        public void LibraryPlugin_GetGamesLibraryTest()
        {
            var gpis = new List<GameProductInfo>
            {
                Factories.ValidGameProductInfo(),
            };
            entitlementDb.Setup(d => d.ActiveEntitlements).Returns(gpis);

            var mapper = new GameMapper();
            var expected = gpis.Select(gpi => mapper.Convert(gpi)).ToList<Game>();

            Assert.Equal(expected, libraryPlugin.OwnedGames);
        }

        [Fact]
        public void PrereqArgsEncode()
        {
            var dir = @"c:\1\2";
            var exe = @"3.exe";
            var tests = new Dictionary<string[], string>()
            {
                {new string[] {}, $@"{dir}\{exe}?lastRunV2"},
                {new string[] {"", "test", ""}, $@"{dir}\{exe}??test??lastRunV2"},
                {new string[] {"?a?", "b?b?", "?c?c?c"}, $@"{dir}\{exe}?%3Fa%3F?b%3Fb%3F?%3Fc%3Fc%3Fc?lastRunV2"},
            };

            foreach (var test in tests)
            {
                var id = gamePrereqInstaller.GeneratePrereqSpecifier(new Executable() {Command = exe, Args = test.Key}, dir);
                Assert.Equal(id, test.Value);
            }
        }

        [Fact]
        public void TryRefreshGamesListAsync_InvalidateBadVersions()
        {
            var invalidVersionIdx = 2;
            var sampleSize = 10;
            var games = new List<GameProductInfo>();
            for (int i = 0; i < sampleSize; ++i)
            {
                var gameProductInfo = Factories.ValidGameProductInfo();
                var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
                installInfo.Installed = true;
                installedGames.Setup(p => p.Get(installInfo.ProductId)).Returns(installInfo);
                installedGames.Setup(p => p.CheckUpToDateByInstallInfo(installInfo, ERetrieveVersionCondition.ForceUpdate))
                    .ReturnsAsync(i == invalidVersionIdx ? ECheckUpToDateResult.RemoteVersionInvalid : ECheckUpToDateResult.UpToDate);
                games.Add(gameProductInfo);
            }

            entitlementDb.Setup(d => d.ActiveEntitlements).Returns(games);
            var refreshed = libraryPlugin.TryRefreshGamesListAsync(ELibraryRefreshType.Full).Result;

            Assert.Equal(sampleSize - 1, refreshed.Games.Count);
            Assert.False(refreshed.Games.ToList().Any(g => g.Key == games[invalidVersionIdx].ProductId));
        }

        // $ jbilas -- func no longer public, wrap in another unit test
        //[Fact]
        //public void LibraryPlugin_UpdateGameInstallState()
        //{
        //    var gpi = Factories.validGameProductInfo();
        //    entitlementDb.Setup(m => m.Get("asin")).Returns(gpi);
        //
        //    var id = "asin";
        //    GameInstallInfo installGame = new GameInstallInfo()
        //    {
        //        Installed = true,
        //        InstallVersion = null,
        //        InstallDirectory = null,
        //    };
        //
        //    libraryPlugin.UpdateGameInstallState(new ProductId(id), installGame);
        //
        //    installedGames.Setup(i => i.Save(It.IsAny<GameInstallInfo>()));
        //}
    }

    public class GameMapperTest
    {
        [Fact]
        public void MapsToGameFromGPI()
        {
            var gpi = Factories.ValidGameProductInfo();
            var game = new GameMapper().Convert(gpi);
            var expectedGame = new Game(
                title: gpi.ProductTitle,
                productId: gpi.ProductId,
                productSku: gpi.ProductSku,
                entitlementId: gpi.Id,
                isDeveloper: gpi.IsDeveloper,
                productIconUri: gpi.ProductIconUrl,
                screenshotUrls: gpi.Screenshots,
                videoUrls: gpi.Videos,
                backgroundUrl: gpi.Background,
                backgroundUrl2: gpi.Background2);
            Assert.Equal(expectedGame, game);
        }
    }
}
