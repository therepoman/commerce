﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sds = Tv.Twitch.Fuel.Sds;
using Xunit;
using Tv.Twitch.Fuel.Manifest;
using System.IO;
using Amazon.Test.Main.Utilities;

namespace Amazon.Test.Main.Fuel.Manifest
{
    public class ManifestSerializationTest
    {
        ManifestSerialization serializer = new ManifestSerialization();

        //[Fact]
        // use this to inspect a specific manifest. 
        public void ManifestDecode()
        {
            var file = @"C:\ProgramData\Twitch Dev\Games\GameData\8195457d-1d32-4d9d-aaa5-d2afe3086e92\Manifests\d12969fb-3186-41eb-860f-ef959b4c345d.manifest";
            var stream = File.Open(file, FileMode.Open);
            var manifest = serializer.Deserialize(stream).GetAwaiter().GetResult();
            Debugger.Break();
        }

        [Fact]
        public async Task ManifestSerializer_DeserializeCompressedManifest()
        {
            // A very small manifest compressed with XZ (LZMA2)
            var serializedManifestBytes = TestData.ReadTestFileBytes(Path.Combine("Fuel.Manifest", "test_lzma.manifest"));

            using (MemoryStream stream = new MemoryStream())
            {
                await stream.WriteAsync(serializedManifestBytes, 0, serializedManifestBytes.Length);
                stream.Seek(0, SeekOrigin.Begin);
                Sds.Manifest manifest = await serializer.Deserialize(stream);
                Assert.True(manifest.Packages.Count > 0);
            }
        }

        [Fact]
        public async Task ManifestSerializer_DeserializeCompressedManifestBadSignature()
        {
            // A very small manifest compressed with XZ (LZMA2)
            var serializedManifestBytes = TestData.ReadTestFileBytes(Path.Combine("Fuel.Manifest", "test_lzma_badsig.manifest"));

            using (MemoryStream stream = new MemoryStream())
            {
                await stream.WriteAsync(serializedManifestBytes, 0, serializedManifestBytes.Length);
                stream.Seek(0, SeekOrigin.Begin);

                await Assert.ThrowsAsync<BadManifestSignature>(async () => await serializer.Deserialize(stream));
            }
        }
    }
}
