﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tv.Twitch.Fuel.Manifest;
using Xunit;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Test.Main.Fuel.Manifest
{
    public class ManifestComparatorTest
    {
        [Fact]
        public void ManifestComparator_Compare()
        {
            Sds.Package oldPkg = new Sds.Package();
            Sds.Package newPkg = new Sds.Package();
            HashSet<string> removedFiles = new HashSet<string>();
            HashSet<string> newFiles = new HashSet<string>();
            HashSet<string> updatedFiles = new HashSet<string>();
            foreach (int i in Enumerable.Range(0, 100))
            {
                // make the manifests have some removed, new, common, and changed files
                int what = i % 4;
                Sds.File randFile = TestManifestGenerator.RandomFile();
                switch (what)
                {
                    case 0: // unchanged file
                        oldPkg.Files.Add(randFile);
                        newPkg.Files.Add(randFile);
                        break;
                    case 1: // removed file
                        removedFiles.Add(randFile.Path);
                        oldPkg.Files.Add(randFile);
                        break;
                    case 2: // new file
                        newFiles.Add(randFile.Path);
                        newPkg.Files.Add(randFile);
                        break;
                    case 3: // changed file
                        updatedFiles.Add(randFile.Path);
                        oldPkg.Files.Add(randFile);
                        Sds.File newVersion = new Sds.File(randFile);
                        newVersion.Hash = TestManifestGenerator.RandomHash();
                        newPkg.Files.Add(newVersion);
                        break;
                }
            }
            Sds.Manifest oldManifest = new Sds.Manifest();
            oldManifest.Packages.Add(oldPkg);
            Sds.Manifest newManifest = new Sds.Manifest();
            newManifest.Packages.Add(newPkg);

            ManifestComparator compartor = new ManifestComparator();
            ManifestComparison comparison = compartor.Compare(oldManifest, newManifest);

            Assert.Equal(newFiles.Count, Enumerable.Count(comparison.New));
            Assert.Equal(removedFiles.Count, Enumerable.Count(comparison.Removed));
            Assert.Equal(updatedFiles.Count, Enumerable.Count(comparison.Updated));

            foreach (Sds.File file in comparison.New) Assert.True(newFiles.Contains(file.Path));
            foreach (Sds.File file in comparison.New) Assert.True(newFiles.Contains(file.Path));
            foreach (Sds.File file in comparison.New) Assert.True(newFiles.Contains(file.Path));
        }
    }
}
