﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.IO;
using Amazon.Fuel.Plugin.Persistence;
using Xunit;

using Sds = Tv.Twitch.Fuel.Sds;
using Amazon.Test.Main.Utilities;

namespace Amazon.Test.Main.Fuel.Manifest
{
    public class ManifestStorageV2Test
    {
        [Fact]
        public void ManifestStorage_InitNonExistentPath()
        {
            Assert.Throws(typeof(ManifestStorageInitException), () => new ManifestStorage(new TestPersistence("\\\\non-existent\\path"), null));
        }

        [Fact]
        public void ManifestStorage_InitNullPath()
        {
            Assert.Throws(typeof(ManifestStorageInitException), () => new ManifestStorage(new TestPersistence(null), null));
        }

        [Fact]
        public void ManifestStorage_InitBlankPath()
        {
            Assert.Throws(typeof(ManifestStorageInitException), () => new ManifestStorage(new TestPersistence(""), null));
        }

        [Fact]
        public void ManifestStorage_InitNewDirectory()
        {
            new ManifestStorage(new TestPersistence(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString())), null);
        }

        [Fact]
        public void ManifestStorage_EmptyGetsNull()
        {
            PerformTest((storage) =>
            {
                var value = storage.Get(new ProductId(Guid.NewGuid().ToString()), Guid.NewGuid().ToString()).GetAwaiter().GetResult();
                Assert.Null(value);
            });
        }

        [Fact]
        public void ManifestStorage_EmptyGetSet()
        {
            PerformTest((storage) =>
            {
                var serializedManifestBytes = TestData.ReadTestFileBytes(Path.Combine("Fuel.Manifest", "test_lzma.manifest"));

                var versionId = Guid.NewGuid().ToString();
                var productId = new ProductId(Guid.NewGuid().ToString());

                using (var memStream = new MemoryStream(serializedManifestBytes))
                {
                    bool stored = storage.PutRaw(productId, versionId, memStream, true).GetAwaiter().GetResult();
                    Assert.True(stored);
                    Sds.Manifest fromStorage = storage.Get(productId, versionId).GetAwaiter().GetResult();
                    Assert.NotNull(fromStorage);
                }
            });
        }

        [Fact]
        public void ManifestStorage_RoundTrip()
        {
            var serializedManifestBytes = TestData.ReadTestFileBytes(Path.Combine("Fuel.Manifest", "test_lzma.manifest"));

            var storageId = Guid.NewGuid().ToString();
            var productId = new ProductId(Guid.NewGuid().ToString());
            var versionId = Guid.NewGuid().ToString();

            PerformTest(storageId, (storage) =>
            {
                using (var memStream = new MemoryStream(serializedManifestBytes))
                {
                    bool stored = storage.PutRaw(productId, versionId, memStream, true).GetAwaiter().GetResult();
                    Assert.True(stored);
                }
            }, false);

            PerformTest(storageId, (storage) =>
            {
                Sds.Manifest fromStorage = storage.Get(productId, versionId).GetAwaiter().GetResult();
                Assert.NotNull(fromStorage);
            });
        }

        [Fact]
        public void ManifestStorage_DeleteDeletes()
        {
            PerformTest((storage) =>
            {
                var serializedManifestBytes = TestData.ReadTestFileBytes(Path.Combine("Fuel.Manifest", "test_lzma.manifest"));
                var versionId = Guid.NewGuid().ToString();
                var prodId = new ProductId(Guid.NewGuid().ToString());

                using (var memStream = new MemoryStream(serializedManifestBytes))
                {
                    Assert.True(storage.PutRaw(prodId, versionId, memStream, true).GetAwaiter().GetResult());
                    Assert.NotNull(storage.Get(prodId, versionId).GetAwaiter().GetResult());
                    Assert.True(storage.Delete(prodId, versionId));
                    Assert.Null(storage.Get(prodId, versionId).GetAwaiter().GetResult());
                }
            });
        }

        [Fact]
        public void ManifestStorage_NullManifestPut()
        {
            PerformTest((storage) =>
            {
                try
                {
                    bool stored = storage.PutRaw(new ProductId(Guid.NewGuid().ToString()), Guid.NewGuid().ToString(), null, true).GetAwaiter().GetResult();
                    // Ensure the test goes to the catch block
                    Assert.True(false, "test failed, no exception");
                }
                catch (ArgumentNullException e)
                {
                    Assert.IsType(typeof(ArgumentNullException), e);
                }
            });
        }

        [Fact]
        public void ManifestStorage_EmptyManifestPutRaw()
        {
            PerformTest((storage) =>
            {
                try
                {
                    using (var stream = new MemoryStream())
                    {
                        bool stored = storage.PutRaw(new ProductId(Guid.NewGuid().ToString()), Guid.NewGuid().ToString(), null, true).GetAwaiter().GetResult();
                        // Ensure the test goes to the catch block
                        Assert.True(false, "test failed, no exception");
                    }
                }
                catch (ArgumentNullException e)
                {
                    Assert.IsType(typeof(ArgumentNullException), e);
                }
            });
        }

        private void PerformTest(Action<ManifestStorage> test)
        {
            PerformTest(Guid.NewGuid().ToString(), test);
        }

        private void PerformTest(string storageId, Action<ManifestStorage> test, bool cleanAfter = true)
        {
            IPersistence persistence = new TestPersistence(Path.Combine(Path.GetTempPath(), storageId.ToString()));
            try
            {
                ManifestStorage storage = new ManifestStorage(persistence, null);
                test.Invoke(storage);
            }
            finally
            {
                if (cleanAfter) Directory.Delete(persistence.DataRoot(), recursive: true);
            }
        }
    }

    class TestPersistence : IPersistence
    {
        private readonly string _root;
        public TestPersistence(string root)
        {
            _root = root;
        }

        public void ClearData()
        {
            Directory.Delete(_root, true);
        }

        public string DataRoot()
        {
            return _root;
        }
    }
}
