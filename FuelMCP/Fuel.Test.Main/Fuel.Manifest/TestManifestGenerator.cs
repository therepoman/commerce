﻿using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Test.Main.Fuel.Manifest
{
    class TestManifestGenerator
    {
        private static Random RAND = new Random();

        public static Sds.Manifest RandomManifest()
        {
            Sds.Manifest manifest = new Sds.Manifest();

            foreach (int i in Enumerable.Range(1, 10))
            {
                manifest.Packages.Add(RandomPackage());
            }

            return manifest;
        }

        public static Sds.Package RandomPackage()
        {
            Sds.Package package = new Sds.Package();

            foreach (int i in Enumerable.Range(1, 500))
            {
                package.Files.Add(RandomFile());
            }

            return package;
        }

        public static Sds.File RandomFile()
        {
            Sds.File file = new Sds.File();
            file.Path = Guid.NewGuid().ToString();
            file.System = RAND.Next(1) == 0;
            file.Size = RAND.Next(40960);
            file.Mode = Convert.ToUInt32(RAND.Next(Convert.ToInt32("777", 8)));
            file.Hidden = RAND.Next(1) == 0;
            file.Hash = RandomHash();
            return file;
        }

        public static Sds.Hash RandomHash()
        {
            Sds.Hash hash = new Sds.Hash();
            hash.Algorithm = Sds.HashAlgorithm.Sha256;
            byte[] hashValue = new byte[32];
            RAND.NextBytes(hashValue);
            hash.Value = ByteString.CopyFrom(hashValue);
            return hash;
        }
    }
}
