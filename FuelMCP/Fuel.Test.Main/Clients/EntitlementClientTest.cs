﻿
using System.Linq;
using Amazon.Fuel.Plugin.Entitlement.Services;
using Xunit;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Test.Main.Utilities;
using Moq;

namespace Amazon.Test.Main.Clients
{
    public class EntitlementClientTest
    {
        private Mock<IMetricsPlugin> metrics;
        private SyncGoodsParser parser;

        public EntitlementClientTest()
        {
            metrics = new Mock<IMetricsPlugin>();
            parser = new SyncGoodsParser(metrics.Object);
        }

        [Fact]
        public void ParseSyncGoods()
        {
            var data = @"{ ""goods"":
  [{""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""633041da-be42-496d-b7a3-e8e410dfbfd0"",
    ""modified"":1471302473.136,
    ""origin"":{""originTimestamp"":1471302473.136, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""relay.test"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""633041da-be42-496d-b7a3-e8e410dfbfd0"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""453c581f-9935-4e1a-887d-3aa7937b0af5"",
    ""modified"":1471311452.841,
    ""origin"":{""originTimestamp"":1471311452.841, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""relay.test"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""453c581f-9935-4e1a-887d-3aa7937b0af5"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""798c989c-ddbc-4fcc-b73c-969ca27d8a23"",
    ""modified"":1471311473.566,
    ""origin"":{""originTimestamp"":1471311473.566, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""relay.test"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""798c989c-ddbc-4fcc-b73c-969ca27d8a23"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""14b25bfc-ee68-4f92-bdb8-d78b891ff878"",
    ""modified"":1471311497.945,
    ""origin"":{""originTimestamp"":1471311497.945, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""relay.test"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""14b25bfc-ee68-4f92-bdb8-d78b891ff878"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""7770f043-0f90-4271-aac0-82fd78bfee3d"",
    ""modified"":1471311508.524,
    ""origin"":{""originTimestamp"":1471311508.524, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""relay.test"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""7770f043-0f90-4271-aac0-82fd78bfee3d"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""66aa228e-ac03-4395-a509-82af6574c7de"",
    ""modified"":1472158047.982,
    ""origin"":{""originTimestamp"":1472158047.982, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""9999991"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""66aa228e-ac03-4395-a509-82af6574c7de"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""31be29e0-4c39-46c3-a94a-75585cce16af"",
    ""modified"":1472158053.005,
    ""origin"":{""originTimestamp"":1472158053.005, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""9999992"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""31be29e0-4c39-46c3-a94a-75585cce16af"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""9b206a8a-1e47-44ec-a302-75af65fa757b"",
    ""modified"":1473303635.991,
    ""origin"":{""originTimestamp"":1473303635.991, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""FuelSampleGame"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""9b206a8a-1e47-44ec-a302-75af65fa757b"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""},
   {""__type"":""com.amazon.adg.common.model#DigitalGood"",
    ""channel"":{""id"":""foo.channel""},
    ""customerId"":""amzn1.twitch.128902625"",
    ""domain"":{""id"":""foo.domain""},
    ""hidden"":false,
    ""id"":""3df0a957-f9ea-4f2d-8ffe-e18d531c9980"",
    ""modified"":1473390866.627,
    ""origin"":{""originTimestamp"":1473390866.627, ""originType"":""ORDER""},
    ""ownerId"":""amzn1.twitch.128902625"",
    ""product"":{""asinVersion"":0, ""id"":""relay.dev"", ""type"":""Entitlement"", ""productLine"": ""Twitch:FuelGame""},
    ""receiptId"":""3df0a957-f9ea-4f2d-8ffe-e18d531c9980"",
    ""state"":""LIVE"",
    ""transactionState"":""ORIGIN""}]
 }";

            var result = parser.Parse(data);
            Assert.NotNull(result);
            Assert.Equal(9, result.Count);

            var entitlementIds = result.Select(r => r.Id);
            Assert.Equal(9, entitlementIds.Count());

            var productIds = result.Select(r => r.ProductId).Distinct();
            Assert.Equal(5, productIds.Count());

            var titles = result.Select(r => r.ProductTitle).Distinct();
            Assert.DoesNotContain("unknown product", titles);
            Assert.Contains("relay.test", titles);
            Assert.Contains("9999991", titles);
            Assert.Contains("9999992", titles);
            Assert.Contains("relay.dev", titles);
        }

        [Fact]
        public void ParseGoodWithProductInfo()
        {
            var expectedData = @"{ ""goods"":
                [{
                    ""__type"":""com.amazon.adg.common.model#DigitalGood"",
                    ""channel"":{""id"":""foo.channel""},
                    ""customerId"":""amzn1.twitch.107304773"",
                    ""domain"":{""id"":""foo.domain""},
                    ""hidden"":false,
                    ""id"":""7dd26383-333b-4c37-8bbf-05d18115f88d"",
                    ""modified"":1.47017537673E9,
                    ""origin"":{""originTimestamp"":1.47017537673E9,""originType"":""ORDER""},
                    ""ownerId"":""amzn1.twitch.107304773"",
                    ""product"":
                    {
                        ""asin"": ""B01J5WAJBS"",
                        ""asinVersion"": 0,
                        ""id"": ""4-testTwitchProduct"",
                        ""productDescription"": ""This is a full description with some simple markup applied. <br/><b>Like bold text</b>"",
                        ""productDetails"": 
                        {
                            ""details"": 
                            {
                                ""videos"": 
                                {
                                    ""0"": ""https://www.twitch.tv/videos/133202883""
                                },
                                ""screenshots"": 
                                {
                                    ""0"": ""https://images-na.ssl-images-amazon.com/images/I/A15t1eN9L5L.jpg"",
                                    ""1"": ""https://images-na.ssl-images-amazon.com/images/I/A1+q5s77ZEL.jpg""
                                },
                                ""background"": 
                                {
                                    ""url"": ""https://images-na.ssl-images-amazon.com/images/I/A1s5Dn4PbcL.jpg""
                                }
                            }
                        },
                        ""productDomain"":
                        {
                            ""id"": ""fabdfab7-0000-0000-0000-355b2f900000"",
                            ""name"": null,
                            ""owner"": null
                        },
                        ""productLine"": ""Twitch:FuelGame"",
                        ""productTitle"": ""Twitch Test Game"",
                        ""sku"": ""B01J5WAJBS"",
                        ""type"": ""Entitlement"",
                        ""vendor"":
                        {
                            ""id"": ""TWITC"",
                            ""masVendorCode"": null,
                            ""masVendorId"": null,
                            ""name"": null
                        }
                    }
                }
            ]}";
            
            var output = parser.Parse(expectedData);

            Assert.Equal(1, output.Count);
            var gpi = output[0];
            Assert.Equal("7dd26383-333b-4c37-8bbf-05d18115f88d", gpi.Id);
            Assert.Equal("B01J5WAJBS", gpi.ProductAsin);
            Assert.Equal("0", gpi.ProductAsinVersion);
            Assert.Equal("This is a full description with some simple markup applied. <br/><b>Like bold text</b>", gpi.ProductDescription);
            Assert.Equal(null, gpi.ProductIconUrl);
            Assert.Equal("fabdfab7-0000-0000-0000-355b2f900000", gpi.ProductDomain);
            Assert.Equal("Twitch:FuelGame", gpi.ProductLine);
            Assert.Equal("Twitch Test Game", gpi.ProductTitle);
            Assert.Equal("B01J5WAJBS", gpi.ProductSku);
            Assert.Equal("TWITC", gpi.ProductPublisher);
            Assert.False(gpi.IsDeveloper);
            Assert.NotNull(gpi.VideosJson);
            Assert.NotNull(gpi.Videos);
            Assert.NotEmpty(gpi.Videos);
            Assert.NotNull(gpi.ScreenshotsJson);
            Assert.NotNull(gpi.Screenshots);
            Assert.NotEmpty(gpi.Screenshots);
            Assert.NotNull(gpi.Background);
            Assert.NotEmpty(gpi.Background);

            metrics.Verify(m => m.AddCounter(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_ParseSuccess));
        }

        [Fact]
        public void ParseGoodWithBadProductLine()
        {
            var expectedData = @"{ ""goods"":
                [{
                    ""__type"":""com.amazon.adg.common.model#DigitalGood"",
                    ""channel"":{""id"":""foo.channel""},
                    ""customerId"":""amzn1.twitch.107304773"",
                    ""domain"":{""id"":""foo.domain""},
                    ""hidden"":false,
                    ""id"":""7dd26383-333b-4c37-8bbf-05d18115f88d"",
                    ""modified"":1.47017537673E9,
                    ""origin"":{""originTimestamp"":1.47017537673E9,""originType"":""ORDER""},
                    ""ownerId"":""amzn1.twitch.107304773"",
                    ""product"":
                    {
                        ""asin"": ""B01J5WAJBS"",
                        ""asinVersion"": 0,
                        ""id"": ""4-testTwitchProduct"",
                        ""productDescription"": ""This is a full description with some simple markup applied. <br/><b>Like bold text</b>"",
                        ""productDetails"": {},
                        ""productDomain"":
                        {
                            ""id"": ""fabdfab7-0000-0000-0000-355b2f900000"",
                            ""name"": null,
                            ""owner"": null
                        },
                        ""productLine"": ""BAD_PRODUCT_LINE"",
                        ""productTitle"": ""Twitch Test Game"",
                        ""sku"": ""B01J5WAJBS"",
                        ""type"": ""Entitlement"",
                        ""vendor"":
                        {
                            ""id"": ""TWITC"",
                            ""masVendorCode"": null,
                            ""masVendorId"": null,
                            ""name"": null
                        }
                    }
                }
            ]}";
            
            var output = parser.Parse(expectedData);

            Assert.Equal(0, output.Count);
            metrics.Verify(m => m.AddCounter(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_ParseSuccess));
        }


        [Fact]
        public void ParseGoodWithNoProductLine()
        {
            var expectedData = @"{ ""goods"":
                [{
                    ""__type"":""com.amazon.adg.common.model#DigitalGood"",
                    ""channel"":{""id"":""foo.channel""},
                    ""customerId"":""amzn1.twitch.107304773"",
                    ""domain"":{""id"":""foo.domain""},
                    ""hidden"":false,
                    ""id"":""7dd26383-333b-4c37-8bbf-05d18115f88d"",
                    ""modified"":1.47017537673E9,
                    ""origin"":{""originTimestamp"":1.47017537673E9,""originType"":""ORDER""},
                    ""ownerId"":""amzn1.twitch.107304773"",
                    ""product"":
                    {
                        ""asin"": ""B01J5WAJBS"",
                        ""asinVersion"": 0,
                        ""id"": ""4-testTwitchProduct"",
                        ""productDescription"": ""This is a full description with some simple markup applied. <br/><b>Like bold text</b>"",
                        ""productDetails"": {},
                        ""productDomain"":
                        {
                            ""id"": ""fabdfab7-0000-0000-0000-355b2f900000"",
                            ""name"": null,
                            ""owner"": null
                        },
                        ""productTitle"": ""Twitch Test Game"",
                        ""sku"": ""B01J5WAJBS"",
                        ""type"": ""Entitlement"",
                        ""vendor"":
                        {
                            ""id"": ""TWITC"",
                            ""masVendorCode"": null,
                            ""masVendorId"": null,
                            ""name"": null
                        }
                    }
                }
            ]}";
            
            var output = parser.Parse(expectedData);

            Assert.Equal(0, output.Count);
            metrics.Verify(m => m.AddCounter(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_ParseSuccess));
        }

        [Fact]
        public void ParseSyncGoods_WithNoEntitlements()
        {
            var expectedData = @"{ ""goods"":
                []}";

            var output = parser.Parse(expectedData);

            Assert.Equal(0, output.Count);
            metrics.Verify(m => m.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_Empty, 1));
        }

        [Fact]
        public void ParseSyncGoods_NullEntitlements()
        {
            var expectedData = @"{}";

            var output = parser.Parse(expectedData);

            Assert.Equal(0, output.Count);
            metrics.Verify(m => m.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_Empty, 1));
        }

        [Fact]
        public void ParseSyncGoods_EmptyEntitlementId()
        {
            var expectedData = @"{ ""goods"":
                [{
                    ""__type"":""com.amazon.adg.common.model#DigitalGood"",
                    ""channel"":{""id"":""foo.channel""},
                    ""customerId"":""amzn1.twitch.107304773"",
                    ""domain"":{""id"":""foo.domain""},
                    ""hidden"":false,
                    ""modified"":1.47017537673E9,
                    ""origin"":{""originTimestamp"":1.47017537673E9,""originType"":""ORDER""},
                    ""ownerId"":""amzn1.twitch.107304773"",
                    ""product"":
                    {
                        ""asin"": ""B01J5WAJBS"",
                        ""asinVersion"": 0,
                        ""id"": ""4-testTwitchProduct"",
                        ""productDescription"": ""This is a full description with some simple markup applied. <br/><b>Like bold text</b>"",
                        ""productDetails"": {},
                        ""productDomain"":
                        {
                            ""id"": ""fabdfab7-0000-0000-0000-355b2f900000"",
                            ""name"": null,
                            ""owner"": null
                        },
                        ""productLine"": ""Twitch:FuelGame"",
                        ""productTitle"": ""Twitch Test Game"",
                        ""sku"": ""B01J5WAJBS"",
                        ""type"": ""Entitlement"",
                        ""vendor"":
                        {
                            ""id"": ""TWITC"",
                            ""masVendorCode"": null,
                            ""masVendorId"": null,
                            ""name"": null
                        }
                    }
                }
            ]}";

            var output = parser.Parse(expectedData);

            Assert.Equal(0, output.Count);
            metrics.Verify(m => m.AddDiscreteValue(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_MissingId, 1));
            metrics.Verify(m => m.AddCounter(MetricStrings.SOURCE_Entitlements, MetricStrings.METRIC_ParseSuccess));
        }

        [Fact]
        public void DeveloperEntitlement()
        {
            var expectedData = @"{ ""goods"":
                [{
                    ""__type"":""com.amazon.adg.common.model#DigitalGood"",
                    ""channel"":{""id"":""38202913-4105-448D-B903-9777F8E4A940""},
                    ""customerId"":""amzn1.twitch.107304773"",
                    ""domain"":{""id"":""foo.domain""},
                    ""hidden"":false,
                    ""id"":""7dd26383-333b-4c37-8bbf-05d18115f88d"",
                    ""modified"":1.47017537673E9,
                    ""origin"":{""originTimestamp"":1.47017537673E9,""originType"":""ORDER""},
                    ""ownerId"":""amzn1.twitch.107304773"",
                    ""product"":
                    {
                        ""asin"": ""B01J5WAJBS"",
                        ""asinVersion"": 0,
                        ""id"": ""4-testTwitchProduct"",
                        ""productDescription"": ""This is a full description with some simple markup applied. <br/><b>Like bold text</b>"",
                        ""productDetails"": {},
                        ""productDomain"":
                        {
                            ""id"": ""fabdfab7-0000-0000-0000-355b2f900000"",
                            ""name"": null,
                            ""owner"": null
                        },
                        ""productLine"": ""Twitch:FuelGame"",
                        ""productTitle"": ""Twitch Test Game"",
                        ""sku"": ""B01J5WAJBS"",
                        ""type"": ""Entitlement"",
                        ""vendor"":
                        {
                            ""id"": ""TWITC"",
                            ""masVendorCode"": null,
                            ""masVendorId"": null,
                            ""name"": null
                        }
                    }
                }
            ]}";

            var output = parser.Parse(expectedData);

            Assert.Equal(1, output.Count);
            Assert.True(output[0].IsDeveloper);
        }

        [Fact]
        public void RevokedAndUnrevokedEntitlements()
        {
            var data = TestData.ReadTestFile(@"Utilities/RevokedUnrevoked.json");

        }
    }
}
