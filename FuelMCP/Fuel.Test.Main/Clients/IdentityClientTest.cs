﻿using System.Collections.Generic;
using Clients.Kraken;
using Xunit;
using Moq;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Test.Main.Clients
{
    public class IdentityClientTest
    {
        private IdentityClient _client;
        private KrakenClient _kraken;
        private Mock<ITwitchTokenProvider> _provider;

        public IdentityClientTest()
        {
            Mock<IAppConfigPlugin> appConfig = new Mock<IAppConfigPlugin>();
            this._client = new IdentityClient("FILL_IN", "https://api.twitch.tv");
            this._kraken = new KrakenClient("FILL_IN", "https://api.twitch.tv/kraken");
            this._provider = new Mock<ITwitchTokenProvider>();

        }

        [Fact (Skip = "Use when needed, requires secret info")]
        public async void FetchProductId()
        {
            var scopes = new List<string> {"user_read"};
            TokenResponse response = await _client.FetchGameToken("FILL_IN", _provider.Object, scopes);
            Assert.NotNull(response.AccessToken);
        }

        [Fact(Skip = "Use when needed, requires secret info")]
        public async void Revoke()
        {
            var refresh_token = "FILL_IN";
            await _client.Revoke(refresh_token);
            var valid = await _kraken.IsValid(_provider.Object);
            Assert.False(valid);
        }
    }
}
