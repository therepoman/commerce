﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Exceptions;
using Clients.Kraken;
using Moq;
using Xunit;

namespace Amazon.Test.Main.Clients
{
    public class KrakenClientTest
    {
        private KrakenClient client;
        private Mock<ITwitchTokenProvider> provider;

        public KrakenClientTest()
        {
            client = new KrakenClient("clientid", "https://api.twitch.tv/kraken");
            provider = new Mock<ITwitchTokenProvider>();
        }

        [Fact]
        public void InvalidAuthToken()
        {
            var ex = Assert.ThrowsAsync<TwitchAuthException>(() => client.GetUserForOauth(provider.Object));
        }

        [Fact (Skip = "Token expired")]
        public async void GetAuth()
        {
            var login = await client.GetUserForOauth(provider.Object);
            
            Assert.Equal("grogenaut", login.name);
            Assert.Equal("128902625", login._id);
        }
    }
}
