﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Amazon.Fuel.Plugin.Uninstall;
using Amazon.Test.Main.Utilities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Amazon.Test.Main.Plugin.Uninstall.Test.Unit
{
    public class UninstallTest
    {
        private readonly Mock<IRegisterInstallation> _registerInstall = new Mock<IRegisterInstallation>();
        private readonly Mock<IInstalledGamesPlugin> _installedGames = new Mock<IInstalledGamesPlugin>();
        private readonly Mock<IOSServicesDriver> _osServicesDriver = new Mock<IOSServicesDriver>();
        private readonly Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        private readonly Mock<IMessengerHub> _hub = new Mock<IMessengerHub>();
        private readonly Mock<IShortcutManager> _shortcutManager = new Mock<IShortcutManager>();
        private readonly Mock<ISecureStoragePlugin> _secureStorage = new Mock<ISecureStoragePlugin>();
        private readonly Mock<ITwitchUserStoragePlugin> _twitchUserStorage = new Mock<ITwitchUserStoragePlugin>();
        private readonly Mock<IMetricsPlugin> _metrics = new Mock<IMetricsPlugin>();
        private readonly Mock<IManifestStorage> _manifestStorage = new Mock<IManifestStorage>();

        public UninstallPlugin ValidPlugin()
        {
            return new UninstallPlugin(
                _registerInstall.Object,
                _installedGames.Object,
                _osServicesDriver.Object,
                _osDriver.Object,
                _hub.Object,
                _shortcutManager.Object,
                _secureStorage.Object,
                _twitchUserStorage.Object,
                _metrics.Object,
                _manifestStorage.Object);
        }

        [Fact]
        public void Uninstall_DeleteFiles_NoManifest_SkipsWhenNoDirectory()
        {
            var installInfo = Factories.ValidGameInstallInfo(Factories.ValidGameProductInfo());
            installInfo.InstallDirectory = null;
            _installedGames.Setup(i => i.Get(installInfo.ProductId)).Returns(installInfo);
            _manifestStorage.Setup(m => m.Get(installInfo.ProductId, installInfo.InstallVersion)).ReturnsAsync(null);
            var uninstall = ValidPlugin();

            uninstall.DeleteGameFiles(installInfo.ProductId);
            _osDriver.Verify(o => o.TryDeleteProtected(It.IsAny<string>(), true), Times.Never);
        }

        [Theory,
            InlineData(@"C:\", false),
            InlineData(@"C:\asdf", false),
            InlineData(@"c:\program files (x86)", false),
            InlineData(@"c:\windows", false),
            InlineData(@"c:\program files (x86)\TwiTch\games Library\a_test_game", true),
            InlineData(@"d:\twitCh\games Library\a_test_game", true)]
        public void Uninstall_DeleteFiles_NoManifest_ProtectsDirs(string testDir, bool valid)
        {
            var installInfo = Factories.ValidGameInstallInfo(Factories.ValidGameProductInfo());
            installInfo.InstallDirectory = testDir;
            _installedGames.Setup(i => i.Get(installInfo.ProductId)).Returns(installInfo);
            _manifestStorage.Setup(m => m.Get(installInfo.ProductId, installInfo.InstallVersion)).ReturnsAsync(null);

            // set these to real things
            _osDriver.Setup(o => o.PrimaryLibraryRoot).Returns(new WindowsFileSystemDriver().PrimaryLibraryRoot);
            _osDriver.Setup(o => o.GetSecondaryLibraryRoot(It.IsAny<string>())).Returns((string str) => new WindowsFileSystemDriver().GetSecondaryLibraryRoot(str));

            var uninstall = ValidPlugin();

            uninstall.DeleteGameFiles(installInfo.ProductId);
            Func<Times> times = Times.Never;
            if (valid)
            {
                times = Times.Once;
            }
            _osDriver.Verify(o => o.TryDeleteProtected(It.IsAny<string>(), true), times);
        }
    }
}
