﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.MCP.Common.Messages;
using Clients.Kraken;
using Ploeh.AutoFixture;
using Amazon.MCP.Common.Contracts;
using AutoMapper;
using Fuel.Ingest.Core.Models;
using System.Dynamic;
using Fuel.Ingest.Core.Coral.Models;
using Newtonsoft.Json.Linq;

namespace Amazon.MCP.Test.Main.Utilities
{
    public class Factories
    {
        private static Fixture instance;

        public static Fixture Fixture
        {
            get
            {
                if (instance == null)
                {
                    instance = new Fixture();
                }
                return instance;
            }
        }

        public static TwitchUserLogin validLogin()
        {
            return Fixture.Create<TwitchUserLogin>();
        }

        public static string validChannelId()
        {
            return Fixture.Create<string>("channel_id");
        }

        public static string validUsername()
        {
            return Fixture.Create<string>("username");
        }

        public static string validTuid()
        {
            return Fixture.Create<string>("tuid");
        }

        public static string validOauth()
        {
            return Fixture.Create<string>("oauth");
        }

        public static KrakenUser validKrakenUser()
        {
            return Fixture.Create<KrakenUser>();
        }

        public static UserLog validUserLog()
        {
            return Fixture.Create<UserLog>();
        }

        public static string validDisplayName()
        {
            return Fixture.Create<string>("display_name");
        }

        public static string validLogo()
        {
            return Fixture.Create<string>("logo");
        }

        public static string validEntitlement()
        {
            return Fixture.Create<string>("valid.entitlement.id");
        }

        public static DispsableFile DisposableFile()
        {
            return new DispsableFile();
        }

        public static RefreshResponse validRefreshResponse()
        {
            return Fixture.Create<RefreshResponse>();
        }

        public static RefreshResponse validRefreshResponseWithRefreshToken(string refresh_token)
        {
            return Fixture.Build<RefreshResponse>().With(rr => rr.refresh_token, refresh_token).Create();
        }

        public static LocalTwitchUser validLocalTwitchUser()
        {
            return Fixture.Create<LocalTwitchUser>();
        }

        public static string validClientId()
        {
            return Fixture.Create<string>("client_id");
        }

        public static string validRefreshToken()
        {
            return Fixture.Create<string>("refresh_token");
        }

        public static int validPid()
        {
            return Fixture.Create<int>();
        }

        public static int validMaxThreads()
        {
            return Fixture.Create<int>();
        }

        public static IEnumerable<string> validScopes()
        {
            return Fixture.CreateMany<string>(2);
        }

        public static TokenResponse validTokenResponse()
        {
            return Fixture.Create<TokenResponse>();
        }

        public static LaunchResult launchResultSuccess()
        {
            return new LaunchResult
            {
                Process = null,
                Status = LaunchResult.EStatus.Success,
            };
        }

        public static GameInstallInfo validGameInstallInfo()
        {
            return new GameInstallInfo
            {
                Installed = false,
                InstallVersion = null,
            };
        }

        public static GameProductInfo validLiveGameProductInfo()
        {
            return Fixture.Build<GameProductInfo>()
                .With(g => g.State, GameProductInfo.STATE_LIVE)
                .With(g => g.IsDeveloper, false).Create();
        }

        public static GameProductInfo validGameProductInfo()
        {
            return Fixture.Create<GameProductInfo>();
        }

        public static ProductId validProductId()
        {
            return Fixture.Create<ProductId>();
        }

        public static string validVersionId()
        {
            return Fixture.Create<string>("version_id");
        }

        public static string validVersionName()
        {
            return Fixture.Create<string>("version_name");
        }

        public static string validStatusMessage()
        {
            return Fixture.Create<string>("status_message");
        }

        public static List<string> validFileList()
        {
            return new List<string> { Fixture.Create<string>(), Fixture.Create<string>() };
        }

        public static CreateVersionResult validCreateVersionResponse()
        {
            dynamic createVersionResponse = new ExpandoObject();
            createVersionResponse.version = new ExpandoObject();
            createVersionResponse.version.description = validVersionDescription();
            createVersionResponse.version.versionName = validVersionName();
            createVersionResponse.version.status = validVersionStatus();
            createVersionResponse.version.productId = validProductId().ToString();
            createVersionResponse.version.versionId = validVersionId();
            createVersionResponse.version.uploadManifest = validUrl();
            createVersionResponse.version.statusMessage = validStatusMessage();

            return new CreateVersionResult(createVersionResponse);
        }

        public static ListProductVersionsResult validListProductVersionsResponse(bool WithLastVersion)
        {
            dynamic listProductVersionsResponse = new ExpandoObject();
            listProductVersionsResponse.lastEvaluatedVersionName = (WithLastVersion ? Fixture.Create<string>() : null);

            dynamic versionRef1 = new ExpandoObject();
            versionRef1.versionId = Fixture.Create<string>();
            versionRef1.versionName = Fixture.Create<string>();
            versionRef1.description = Fixture.Create<string>();
            versionRef1.status = Fixture.Create<VersionStatus>();

            dynamic versionRef2 = new ExpandoObject();
            versionRef2.versionId = Fixture.Create<string>();
            versionRef2.versionName = Fixture.Create<string>();
            versionRef2.description = Fixture.Create<string>();
            versionRef2.status = Fixture.Create<VersionStatus>();

            listProductVersionsResponse.versions = JArray.FromObject(new[] { versionRef1, versionRef2 });

            return new ListProductVersionsResult(listProductVersionsResponse);
        }

        public static dynamic CoralExceptionJson(string type, string message)
        {
            dynamic exception = new ExpandoObject();
            exception.__type = type;
            exception.message = message;
            return exception;
        }

        public static string ValidCoralExceptionType()
        {
            return "tv.twitch.exception.Service#TestException";
        }

        public static string ValidCoralExceptionMessage()
        {
            return "test message exception";
        }

        // Created to match the manifest.json in Fuel.Ingest\TestData\ValidUploadManifest
        public static UploadManifest validUploadManifest()
        {
            SignedUrl url1 = new SignedUrl("https://example.com/foo", DateTime.Parse("2017-01-08T00:34:12.236Z"));
            SignedUrl url2 = new SignedUrl("https://example.com/foo2", DateTime.Parse("2017-01-08T00:35:23.517Z"));

            UploadManifestFile file1 = new UploadManifestFile("fuel.json", validS3Bucket(), url1);
            UploadManifestFile file2 = new UploadManifestFile("Engine\\Shaders\\StandaloneRenderer\\OpenGL\\SplashVertexShader.glsl", validS3Bucket(), url2);

            List<UploadManifestFile> files = new List<UploadManifestFile>() { file1, file2 };

            return new UploadManifest(files);
        }

        public static Game validGame()
        {
            return new Game(
                Fixture.Create<string>(),
                Fixture.Create<ProductId>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                false);
        }

        public static Game developerGame()
        {
            return new Game(
                Fixture.Create<string>(),
                Fixture.Create<ProductId>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                true);
        }

        public static GameProductInfoV1 downgrade(GameProductInfo gpiCurrent)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<GameProductInfo, GameProductInfoV1>());
            var mapper = config.CreateMapper();
            var g1 = mapper.Map<GameProductInfoV1>(gpiCurrent);
            return g1;
        }

        public static DisposableDirectory DisposableDirectory()
        {
            return new DisposableDirectory();
        }

        public static T copy<T>(T original, Action<T> func)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<T, T>());
            var mapper = config.CreateMapper();
            var copy = mapper.Map<T>(original);
            func(copy);
            return copy;
        }

        public static string validUrl()
        {
            return Fixture.Create<Uri>().ToString();
        }

        public static string validVersionDescription()
        {
            return Fixture.Create<string>();
        }

        public static VersionStatus validVersionStatus()
        {
            return Fixture.Create<VersionStatus>();
        }

        public static string validServiceNamespace()
        {
            return Fixture.Create<string>();
        }

        public static string validOperation()
        {
            return Fixture.Create<string>();
        }
        
        public static string validS3Bucket()
        {
            return Fixture.Create<string>();
        }

        public static string validIngestionXml()
        {
            return File.ReadAllText(TestData.GetPathToTestResource("Fuel.Ingest", "TestData", "test-ingest-manifest.xml"));
        }
    }

    public class DispsableFile : IDisposable
    {
        private string file;

        public DispsableFile()
        {
            this.file = Path.GetTempFileName();
        }

        public string Get()
        {
            return file;
        }

        public void Dispose()
        {
            try
            {
                File.Delete(this.file);
            }
            catch (Exception)
            {
                
            }
        }
    }

    public class DisposableDirectory : IDisposable
    {
        public DisposableDirectory()
        {
            this.Dir = Path.GetTempFileName() + Factories.validVersionId();
            Directory.CreateDirectory(Dir);
        }

        public string Dir { get; }

        public void Dispose()
        {
            Directory.Delete(Dir, true);
        }
    }
}
 
