﻿using System;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Test.Main.Utilities
{
    public class TestableClock : IClock
    {
        private DateTime now;

        public TestableClock(int year, int month, int day)
        {
            this.now = new DateTime(year, month, day);
        }

        public DateTime Now()
        {
            return this.now;
        }

        public void AddMinutes(int minutes)
        {
            this.now = this.now.AddMinutes(minutes);
        }
    }
}
