using System;
using System.IO;
using Amazon.Fuel.Common.Implementations;

namespace Amazon.Test.Main.Utilities
{
    public class TestableFileSystemUtilities : FileSystemUtilities
    {
        public bool CreateDirFailsOnPermissions { get; set; } = false;
        public bool FileExistAlwaysFails { get; set; } = false;
        public bool DirExistAlwaysFails { get; set; } = false;

        public override DirectoryInfo CreateDirectory(string dir)
        {
            if (CreateDirFailsOnPermissions)
                throw new UnauthorizedAccessException();

            return base.CreateDirectory(dir);
        }

        public override bool FileExists(string filePath)
        {
            if (FileExistAlwaysFails)
                return false;

            return base.FileExists(filePath);
        }

        public override bool DirectoryExists(string path)
        {
            if (DirExistAlwaysFails)
                return false;

            return base.DirectoryExists(path);
        } 
    }
}