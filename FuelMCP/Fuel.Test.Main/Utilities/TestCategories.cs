﻿using System.ComponentModel;

namespace Amazon.Test.Main.Utilities
{
    /**
     * Test groupings, let us enable or disable categories of tests or run them in groups.
     */
    public class LocalOnly : CategoryAttribute
    {
        /**
         * Only run these tests on the dev box. The build box will skip them as they may have problems running there
         */
        public LocalOnly() : base("FUEL.LocalOnly") {}
    }
}
