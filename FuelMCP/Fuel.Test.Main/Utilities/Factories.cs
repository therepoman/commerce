﻿using System;
using System.Collections.Generic;
using System.IO;
using Ploeh.AutoFixture;
using Amazon.Fuel.Common.Contracts;
using AutoMapper;
using Amazon.Fuel.Common.IPC;
using Curse.Tools.IPC;
using Moq;

namespace Amazon.Test.Main.Utilities
{
    public class Factories
    {
        private static Fixture _instance;

        public static Fixture Fixture => _instance ?? (_instance = new Fixture());

        public static string ValidUsername()
        {
            return Fixture.Create("username");
        }


        public static string ValidOauth()
        {
            return Fixture.Create("oauth");
        }


        public static string ValidEntitlement()
        {
            return Fixture.Create("valid.entitlement.id");
        }

        public static LocalTwitchUser ValidLocalTwitchUser()
        {
            return Fixture.Create<LocalTwitchUser>();
        }

        public static string ValidClientId()
        {
            return Fixture.Create("client_id");
        }

        public static int ValidPid()
        {
            return Fixture.Create<int>();
        }

        public static IEnumerable<string> ValidScopes()
        {
            return Fixture.CreateMany<string>(2);
        }

        public static LaunchResult LaunchResultSuccess()
        {
            return new LaunchResult
            {
                Process = null,
                Status = LaunchResult.EStatus.Success,
            };
        }

        public static GameInstallInfo ValidGameInstallInfo(GameProductInfo gameInfo, bool nullFields = false)
        {
            var guid = Guid.NewGuid();
            return new GameInstallInfo
            {
                Installed = true,
                InstallVersion = nullFields ? null : $"inst_version_{guid}",
                InstallVersionName = nullFields ? null : "inst_version_name",
                InstallDirectory = nullFields ? null : $@"c:\test\{guid}",
                ProductTitle = nullFields ? null : $"inst_version_{guid}",
                ProductAsin = nullFields ? null : "B00005Q8UG",
                Id = gameInfo.ProductId.Id
            };
        }

        public static GameInstallInfoLuceneDeprecated ValidGameInstallInfoLuceneDeprecated(GameProductInfo gameInfo, bool nullFields = false)
        {
            var guid = Guid.NewGuid();
            return new GameInstallInfoLuceneDeprecated
            {
                Installed = true,
                InstallVersion = nullFields ? null : $"inst_version_{guid}",
                InstallVersionName = nullFields ? null : "inst_version_name",
                InstallDirectory = nullFields ? null : $@"c:\test\{guid}",
                ProductTitle = nullFields ? null : $"inst_version_{guid}",
                ShortcutLocations = nullFields ? null : new List<string>() { $@"c:\{guid}", $@"c:\{guid}\{guid}" },
                Id = gameInfo.ProductId.Id
            };
        }

        public static GameProductInfo ValidLiveGameProductInfo()
        {
            return Fixture.Build<GameProductInfo>()
                .With(g => g.State, GameProductInfo.STATE_LIVE)
                .With(g => g.IsDeveloper, false).Create();
        }

        public static InstallHelperSession ValidInstallHelperSession()
        {
            return new InstallHelperSession()
            {
                ProcessId = 10,
                PipeId = "unique_" + new Guid().ToString(),
                IpcServer = (new Mock<BufferedIPC>()).Object,   // TODO : make this testable
                StatusAfterLaunch = LaunchResult.EStatus.Success
            };
        }

        public static GameProductInfo ValidGameProductInfo()
        {
            return Fixture.Create<GameProductInfo>();
        }

        public static ProductId ValidProductId()
        {
            return Fixture.Create<ProductId>();
        }

        public static string ValidVersionId()
        {
            return Fixture.Create("version_id");
        }

        public static Game ValidGame()
        {
            return new Game(
                Fixture.Create<string>(),
                Fixture.Create<ProductId>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                false,
                Fixture.Create<IList<string>>(),
                Fixture.Create<IList<string>>(),
                Fixture.Create<string>(),
                Fixture.Create<string>());
        }

        public static Game DeveloperGame()
        {
            return new Game(
                Fixture.Create<string>(),
                Fixture.Create<ProductId>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                Fixture.Create<string>(),
                true,
                Fixture.Create<IList<string>>(),
                Fixture.Create<IList<string>>(),
                Fixture.Create<string>(),
                Fixture.Create<string>());
        }

        public static T Copy<T>(T original, Action<T> func)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<T, T>());
            var mapper = config.CreateMapper();
            var copy = mapper.Map<T>(original);
            func(copy);
            return copy;
        }
    }

    public class DispsableFile : IDisposable
    {
        private readonly string _file;

        public DispsableFile()
        {
            _file = Path.GetTempFileName();
        }

        public string Get()
        {
            return _file;
        }

        public void Dispose()
        {
            try
            {
                File.Delete(_file);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }

    public class DisposableDirectory : IDisposable
    {
        public DisposableDirectory()
        {
            Dir = Path.GetTempFileName() + Factories.ValidVersionId();
            Directory.CreateDirectory(Dir);
        }

        public string Dir { get; }

        public void Dispose()
        {
            Directory.Delete(Dir, true);
        }
    }
}
 
