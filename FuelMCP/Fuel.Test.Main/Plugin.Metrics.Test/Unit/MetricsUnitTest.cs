﻿using System;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Metrics;
using Amazon.Fuel.Plugin.Metrics.Models;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Amazon.Fuel.Common.Contracts.Config;
using Clients.Twitch.Spade.Client;

namespace Amazon.Test.Main.Plugin.Metrics.Test.Unit
{
    public class MetricsUnitTest
    {
        const string TEST_DEVICE_ID = "A2T5CWKK734D2D";
        private readonly ISpadeClient _spadeClient = new NullSpadeClient();
        private class MockMetricTransport : IMetricTransport
        {
            public List<IEnumerable<MetricScope>> MetricBatches { get; private set; }
            private object m_lock = new object();

            public MockMetricTransport()
            {
                MetricBatches = new List<IEnumerable<MetricScope>>();
            }

            public ETransmitResult TransmitMetricBatch(string postAddr, IEnumerable<MetricScope> metricEvents)
            {
                lock (m_lock)
                {
                    MetricBatches.Add(metricEvents);
                }
                return ETransmitResult.Success;
            }

            public void Dispose() { }

            public int GetEventCount()
            {
                return MetricBatches.Select(batch => batch.Count()).Sum();
            }
        }

        ServiceMetricTransport CreateTransport()
        {
            return new ServiceMetricTransport( deviceType: TEST_DEVICE_ID,
                                                deviceSerialNumber: "TEST_DSN",
                                                softwareVersion: "v1.0");
        }

        MetricQueue CreateMetricQueue(IMetricTransport transport = null, 
                                        int maxQueueSize = 100, 
                                        float transmissionIntervalSeconds = 1.0f)
        {
            if (transport == null)
            {
                transport = CreateTransport();
            }

            return new MetricQueue(
                                postAddr: "https://device-metrics-us-2.amazon.com/metricsBatch",
                                intervalSeconds: transmissionIntervalSeconds,
                                isEnabled: true,
                                maxQueueSize: maxQueueSize,
                                transport: transport);
        }

        public MetricsPlugin CreateMetricsPlugin()
        {
            var hub = new Mock<IMessengerHub>();
            var cfg = new Mock<IAppConfigPlugin>();
            return new MetricsPlugin("key", DateTime.UtcNow, _spadeClient);
        }

        [Fact]
        public void MetricsPlugin_TestConstrainField()
        {
            var transport = CreateTransport();

            Assert.Equal("_product:blah_", transport.ConstrainField(@"[product:blah]", "test"));
            Assert.Equal("_product:blah_", transport.ConstrainField(@"\product:blah\", "test"));
            Assert.Equal("_product:blah_", transport.ConstrainField(@"_product:blah_", "test"));
            Assert.Equal(" product:blah ", transport.ConstrainField(@" product:blah ", "test"));
            Assert.Equal("@product:blah@", transport.ConstrainField(@"@product:blah@", "test"));
            Assert.Equal("@prOduCt:blAh@", transport.ConstrainField(@"@prOduCt:blAh@", "test"));
            Assert.Equal("@product:Handel@", transport.ConstrainField(@"@product:Händel@", "test"));
            Assert.Equal(299, transport.ConstrainField(new string('x', 301), "test").Length);
        }


        [Fact]
        public void MetricsPlugin_AddCounter()
        {
            var plugin = CreateMetricsPlugin();

            plugin.AddCounter("UnitTestCounter", "UnitTestMetricC");
        }


        [Fact]
        public void Metrics_Queue_NoTransmission()
        {
            var transport = new MockMetricTransport();
            var queue = CreateMetricQueue(transport, 
                                            maxQueueSize: 10, 
                                            transmissionIntervalSeconds: 0.0f);
            var metricsEvent = new MetricScope("program", "source", queue);
            queue.RecordEvent(metricsEvent);

            Assert.Equal(0, transport.MetricBatches.Count);
        }

        [Fact]
        public void Metrics_Queue_MultipleTransmissions()
        {
            var transport = new MockMetricTransport();
            var queue = CreateMetricQueue(transport,
                                            maxQueueSize: 2, 
                                            transmissionIntervalSeconds: 0.0f);
            var metricsEvent = new MetricScope("program", "source", queue);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);

            queue.Wait();

            Assert.Equal(4, transport.GetEventCount());
        }

        [Fact]
        public void Metrics_Queue_PartialTransmissions()
        {
            var transport = new MockMetricTransport();
            var queue = CreateMetricQueue(transport,
                                            maxQueueSize: 2,
                                            transmissionIntervalSeconds: 0.0f);
            var metricsEvent = new MetricScope("program", "source", queue);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);

            queue.Wait();

            Assert.Equal(3, transport.GetEventCount());
        }

        [Fact]
        public void Metrics_Queue_LargeTransmissions()
        {
            var transport = new MockMetricTransport();
            var queue = CreateMetricQueue(transport,
                                            maxQueueSize: 2,
                                            transmissionIntervalSeconds: 0.0f);
            var metricsEvent = new MetricScope("program", "source", queue);
            int batchSize = 1000;
            int batches = 100;
            var tasks = Enumerable.Range(0, batches).Select(_ =>
            {
                return Task.Run(() =>
                {
                    for (int i = 0; i < batchSize; i++)
                    {
                        queue.RecordEvent(metricsEvent);
                    }
                });
            });

            Task.WaitAll(tasks.ToArray());
            queue.Wait();

            Assert.Equal(batchSize * batches, transport.GetEventCount());
        }

        [Fact]
        public void Metrics_Queue_TimedTransmission()
        {
            var transport = new MockMetricTransport();
            var queue = CreateMetricQueue(transport, 
                                            maxQueueSize: 100, 
                                            transmissionIntervalSeconds: 0.5f);
            var metricsEvent = new MetricScope("program", "source", queue);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);
            queue.RecordEvent(metricsEvent);

            Thread.Sleep(1000);

            Assert.Equal(4, transport.GetEventCount());
        }

        [Fact]
        public void Metrics_Queue_LargeTimedTransmissions()
        {
            int batchSize = 100;
            int batches = 100;
            var transport = new MockMetricTransport();

            using (var queue = CreateMetricQueue(transport,
                                            maxQueueSize: 2,
                                            transmissionIntervalSeconds: 0.1f))
            {

                var metricsEvent = new MetricScope("program", "source", queue);
                var tasks = Enumerable.Range(0, batches).Select(_ =>
                {
                    return Task.Run(() =>
                    {
                        for (int i = 0; i < batchSize; i++)
                        {
                            queue.RecordEvent(metricsEvent);
                            Thread.Sleep(1);
                        }
                    });
                });
                Task.WaitAll(tasks.ToArray());

                queue.Wait();
            }

            Assert.Equal(batchSize * batches, transport.GetEventCount());
        }
    }
}
