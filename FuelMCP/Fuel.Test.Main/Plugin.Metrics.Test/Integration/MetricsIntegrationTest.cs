﻿using System;
using System.Threading.Tasks;
using Moq;
using Amazon.Fuel.Common.Contracts;
using Xunit;
using Amazon.Fuel.Plugin.Metrics;
using Amazon.Fuel.Plugin.Metrics.Models;
using Clients.Twitch.Spade.Client;
using Clients.Twitch.Spade.Model;

namespace Amazon.Test.Main.Plugin.Metrics.Test.Integration
{
    public class MetricsIntegrationTest
    {
        public MetricsPlugin CreateMetricsPlugin()
        {
            return new MetricsPlugin("key", DateTime.UtcNow, new NullSpadeClient());
        }


        // NOTE: I don't include these in normal suite as they spam metrics
        //[Fact]
        public void MetricsPlugin_TransportBatch()
        {
            var plugin = CreateMetricsPlugin();

            var metric1 = new MetricScope("FuelMCP", "UnitTestTransport", null);
            metric1.IncrementCounter("UnitTestMetricA");
            metric1.IncrementCounter("UnitTestMetricB");

            var result = plugin.TransmitMetricBatch(new[] { metric1, metric1 });
            Assert.True(result == ETransmitResult.Success);
        }
    }
}
