﻿using System;
using System.Collections.Generic;
using Amazon.Fuel.Common.Contracts;
using Amazon.Test.Main.Utilities;
using Amazon.Fuel.Coral.Model;
using Amazon.Fuel.Plugin.InstalledGames;
using Moq;
using Xunit;
using System.IO;

namespace Amazon.Test.Main.Plugin.InstalledGames.Test
{
    public class InstalledGamesPluginTest
    {
        private InstalledGamesPlugin cut;
        private Mock<IGameInstallInfoDatabase> db = new Mock<IGameInstallInfoDatabase>();
        private Mock<IEntitlementPlugin> entitlementClient = new Mock<IEntitlementPlugin>();
        private Mock<IEntitlementsDatabase> entitlementDb = new Mock<IEntitlementsDatabase>();
        private Mock<ILaunchConfigProvider> launchConfigProvider = new Mock<ILaunchConfigProvider>();
        private Mock<IProcessLauncher> processLauncher = new Mock<IProcessLauncher>();
        private Mock<IClock> clock = new Mock<IClock>();
        private Mock<IMessengerHub> hub = new Mock<IMessengerHub>();

        public InstalledGamesPluginTest()
        {
            cut = new InstalledGamesPlugin(
                db.Object,
                entitlementClient.Object,
                entitlementDb.Object,
                launchConfigProvider.Object,
                processLauncher.Object,
                clock.Object,
                hub.Object);
        }

        [Fact]
        public void Save_PassesThroughWhenInstalled()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
            installInfo.Installed = true;
            installInfo.InstallDirectory = Path.GetTempPath();

            cut.Save(installInfo);

            db.Verify(d => d.AddOrReplaceById(installInfo));
        }

        [Fact]
        public void All()
        {
            var gameProductInfo1 = Factories.ValidGameProductInfo();
            var installInfo1 = Factories.ValidGameInstallInfo(gameProductInfo1);
            var gameProductInfo2 = Factories.ValidGameProductInfo();
            var installInfo2 = Factories.ValidGameInstallInfo(gameProductInfo2);
            var infos = new List<GameInstallInfo> { installInfo1, installInfo2 };
            db.Setup(d => d.All).Returns(infos);

            Assert.Equal(infos, cut.All);
        }

        [Fact]
        public void IsInstalled_Missing()
        {
            Assert.False(cut.IsInstalled(Factories.ValidProductId()));
        }

        [Fact]
        public void IsInstalled_NotInstalled()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            info.Installed = false;
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);

            Assert.False(cut.IsInstalled(new ProductId(info.Id)));
        }

        [Fact]
        public void IsInstalled_Installed()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            info.Installed = true;
            info.InstallDirectory = TestData.TestDataDir();
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);

            Assert.True(cut.IsInstalled(new ProductId(info.Id)));
        }

        [Fact]
        public void GetPassesThrough()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);

            Assert.Equal(info, cut.Get(new ProductId(info.Id)));
        }

        [Fact]
        public void IsUpToDateByProductId_NoProductInDb()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            db.Setup(d => d.TryFindById(info.Id)).Returns(default(GameInstallInfo));

            var result = cut.CheckUpToDateByProductId(info.ProductId, ERetrieveVersionCondition.Offline).Result;
            Assert.Equal(ECheckUpToDateResult.NoProductInDb, result);
        }

        [Fact]
        public void IsUpToDateByInstallInfo_NotInstalled()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
            installInfo.Installed = false;
            entitlementDb.Setup(d => d.Get(installInfo.ProductId)).Returns(gameProductInfo);

            var result = cut.CheckUpToDateByInstallInfo(installInfo, ERetrieveVersionCondition.Offline).Result;
            Assert.Equal(ECheckUpToDateResult.NotInstalled, result);
        }

        [Fact]
        public void CheckUpToDateByInstallInfo_returnsEntitlementNotFound_givenNoEntitlementDBEntry()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
            installInfo.Installed = true;

            var result = cut.CheckUpToDateByInstallInfo(installInfo, ERetrieveVersionCondition.Offline).Result;
            Assert.Equal(ECheckUpToDateResult.EntitlementNotFound, result);
        }

        [Fact]
        public void CheckUpToDateByProductId_returnsRemoteVersionInvalid_givenCoralException()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
            var prodInfo = Factories.ValidGameProductInfo();
            installInfo.Installed = true;
            installInfo.Id = "pid";

            entitlementDb.Setup(d => d.Get(installInfo.ProductId)).Returns(prodInfo);
            db.Setup(d => d.TryFindById(installInfo.Id)).Returns(installInfo);

            entitlementClient.Setup(d => d.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>()))
                .ThrowsAsync(new UndefinedServiceException(""));

            var result = cut.CheckUpToDateByProductId(installInfo.ProductId, ERetrieveVersionCondition.ForceUpdate).Result;
            Assert.Equal(result, ECheckUpToDateResult.RemoteVersionInvalid);
        }

        [Fact]
        public void CheckUpToDateByProductId_returnsRemoteVersionInvalid_givenNoVersionReturnedFromService()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
            installInfo.Installed = true;
            installInfo.Id = "pid";
            var emptyVersionIds = new Dictionary<string, string>();

            entitlementDb.Setup(d => d.Get(installInfo.ProductId)).Returns(gameProductInfo);
            db.Setup(d => d.TryFindById(installInfo.Id)).Returns(installInfo);

            entitlementClient.Setup(d => d.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>())).ReturnsAsync(emptyVersionIds);

            var result = cut.CheckUpToDateByProductId(installInfo.ProductId, ERetrieveVersionCondition.ForceUpdate).Result;
            Assert.Equal(result, ECheckUpToDateResult.RemoteVersionInvalid);
        }

        [Fact]
        public void IsUpToDateByInstallInfo_CachingExpiration()
        {
            var cachedVersion = "cachedVersion";
            var uncachedVersion = "uncachedVersion";

            var gameProductInfo = Factories.ValidGameProductInfo();
            var installInfo = Factories.ValidGameInstallInfo(gameProductInfo);
            installInfo.Installed = true;
            var cachedVersionIds = new Dictionary<string, string> { {installInfo.ProductId.Id, cachedVersion} };
            var uncachedVersionIds = new Dictionary<string, string> { { installInfo.ProductId.Id, uncachedVersion } };


            entitlementDb.Setup(d => d.Get(installInfo.ProductId)).Returns(gameProductInfo);
            db.Setup(d => d.TryFindById(installInfo.Id)).Returns(installInfo);

            // set to cachedVersion
            entitlementClient.Setup(d => d.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>())).ReturnsAsync(cachedVersionIds);
            installInfo.InstallVersion = cachedVersion;

            var result = cut.CheckUpToDateByProductId(installInfo.ProductId, ERetrieveVersionCondition.UpdateIfExpired).Result;
            Assert.Equal(result, ECheckUpToDateResult.UpToDate);

            // set to uncachedVersion
            entitlementClient.Setup(d => d.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>())).ReturnsAsync(uncachedVersionIds);

            result = cut.CheckUpToDateByProductId(installInfo.ProductId, ERetrieveVersionCondition.UpdateIfExpired).Result;
            Assert.Equal(result, ECheckUpToDateResult.UpToDate);

            // set clock ahead (expire)
            clock.Setup(c => c.Now()).Returns(DateTime.UtcNow + cut.UpdateCheckExpiration + TimeSpan.FromSeconds(1));

            // now should need update
            result = cut.CheckUpToDateByProductId(installInfo.ProductId, ERetrieveVersionCondition.UpdateIfExpired).Result;
            Assert.Equal(result, ECheckUpToDateResult.NeedsUpdate);
        }

        [Fact]
        public void GameMissing_NotUpToDate()
        {
            Assert.False(cut.CheckUpToDateByProductId(Factories.ValidProductId(), ERetrieveVersionCondition.ForceUpdate).Result == ECheckUpToDateResult.NeedsUpdate);
        }

        [Fact]
        public void GameInstalled_NotUpToDate()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            info.Installed = false;
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);
            
            Assert.False(cut.CheckUpToDateByProductId(new ProductId(info.Id), ERetrieveVersionCondition.ForceUpdate).Result == ECheckUpToDateResult.NeedsUpdate);
        }

        [Fact]
        public void NoEntitlement_NotUpToDate()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            info.Installed = false;
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);
            entitlementDb.Setup(d => d.Get(info.ProductId)).Returns((GameProductInfo)null);

            Assert.False(cut.CheckUpToDateByProductId(new ProductId(info.Id), ERetrieveVersionCondition.ForceUpdate).Result == ECheckUpToDateResult.NoProductInDb);
        }

        [Fact]
        public void GameWithDifferentVersion_NotUpToDate()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            info.Installed = true;
            info.InstallVersion = "1.0";
            info.Id = gameProductInfo.Id;
            var newVersion = "2.0";
            var expectedVersions = new Dictionary<string, string> { { info.Id, newVersion } };
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);
            entitlementDb.Setup(d => d.Get(info.ProductId)).Returns(gameProductInfo);
            entitlementClient.Setup(e => e.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>()))
                .ReturnsAsync(expectedVersions);

            var expected = ECheckUpToDateResult.NeedsUpdate;
            var actual =
                cut.CheckUpToDateByProductId(new ProductId(info.Id), ERetrieveVersionCondition.ForceUpdate).Result;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GameWithSameVersion_UpToDate()
        {
            var gameProductInfo = Factories.ValidGameProductInfo();
            var info = Factories.ValidGameInstallInfo(gameProductInfo);
            info.Installed = true;
            info.InstallVersion = "1.0";
            var expectedVersions = new Dictionary<string, string> {{info.Id, info.InstallVersion}};
            db.Setup(d => d.TryFindById(info.Id)).Returns(info);
            entitlementDb.Setup(d => d.Get(info.ProductId)).Returns(gameProductInfo);
            entitlementClient.Setup(e => e.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>()))
                .ReturnsAsync(expectedVersions);

            var expected = ECheckUpToDateResult.UpToDate;
            var actual =
                cut.CheckUpToDateByProductId(new ProductId(info.Id), ERetrieveVersionCondition.ForceUpdate).Result;
            Assert.Equal(expected, actual);
        }
    }
}
