using System;
using System.IO;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.InstalledGames;
using Moq;
using Xunit;

namespace Amazon.Test.Main.Plugin.InstalledGames.Test
{
    public class InstalledGameValidatorTest : IDisposable
    {
        private InstalledGameValidator cut;
        private Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        
        private readonly string libraryBaseDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

        public InstalledGameValidatorTest()
        {
            cut = new InstalledGameValidator(_osDriver.Object);
        }

        [Fact]
        public void PassesValidGame()
        {
            checkValidate(isValid: true);
        }

        [Fact]
        public void DetectsMissingJson()
        {
            checkValidate(isValid: false, hasJson: false);
        }

        private void checkValidate(bool isValid, bool hasJson = true)
        {
            var id = Guid.NewGuid().ToString();
            var libraryDir = Path.Combine(libraryBaseDir, Path.GetRandomFileName());

            string gameDataPath = Path.Combine(libraryDir, id);
            Directory.CreateDirectory(gameDataPath);
            string gamePath = Path.Combine(libraryDir, id);
            Directory.CreateDirectory(gamePath);

            if (hasJson)
                File.CreateText(Path.Combine(gamePath, Constants.Paths.FuelGameConfigFileName)).Close();

            Assert.True(cut.IsGameInstallValid(libraryDir, id) == isValid);
        }

        public void Dispose()
        {
            Directory.Delete(libraryBaseDir, true);
        }
    }
}