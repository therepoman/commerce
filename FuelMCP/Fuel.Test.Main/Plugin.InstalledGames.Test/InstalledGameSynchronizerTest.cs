using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.Fuel.Common.Contracts;
using Amazon.Test.Main.Utilities;
using Amazon.Fuel.Plugin.InstalledGames;
using Moq;
using Xunit;

namespace Amazon.Test.Main.Plugin.InstalledGames.Test
{
    public class InstalledGameSynchronizerTest : IDisposable
    {
        private InstalledGameSynchronizer cut;
        private readonly Mock<IGameInstallInfoDatabase> _installInfoDb = new Mock<IGameInstallInfoDatabase>();
        private readonly Mock<IInstalledGameValidator> _installedGameValidator = new Mock<IInstalledGameValidator>();
        private readonly Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        private readonly Mock<IUninstallPlugin> _uninstall = new Mock<IUninstallPlugin>();
        private readonly Mock<IMetricsPlugin> _metrics = new Mock<IMetricsPlugin>();

        private readonly string libraryBaseDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

        public InstalledGameSynchronizerTest()
        {
            _osDriver.Setup(d => d.PrimaryLibraryRoot).Returns(Path.Combine(libraryBaseDir, "primary"));

            cut = new InstalledGameSynchronizer(
                _installInfoDb.Object, 
                _installedGameValidator.Object, 
                _osDriver.Object,
                _metrics.Object,
                _uninstall.Object);
        }

        private List<GameInstallInfo> prepGames(int count, bool usePrimaryDrive)
        {
            // creates directories for discovery testing
            var ret = new List<GameInstallInfo>();
            for (int i = 0; i < count; ++i)
            {
                var info = Factories.ValidGameInstallInfo(Factories.ValidGameProductInfo());
                info.InstallDirectory = Path.Combine(libraryBaseDir, usePrimaryDrive ? "primary" : "secondary", info.Id);
                Directory.CreateDirectory(Path.Combine(info.LibraryDirectory, info.Id));
                Directory.CreateDirectory(Path.Combine(info.LibraryDirectory, "_data", info.Id));
                ret.Add(info);
            }

            return ret;
        }

        [Theory,
            InlineData(false),
            InlineData(true)]
        public void DiscoversInvalidGameInfosFromExistingDirs(bool isOnPrimaryDrive)
        {
            // prep three games, under dirs 1x primary 1x secondary, and wherever our game is
            var dbGamesPrimary = prepGames(2, usePrimaryDrive: true);
            var dbGamesSecondary = prepGames(2, usePrimaryDrive: false);

            var removedInfo = (isOnPrimaryDrive ? dbGamesPrimary : dbGamesSecondary).Last();
            var dbGames = Enumerable.Union(dbGamesPrimary, dbGamesSecondary);

            _installInfoDb.Setup(db => db.All).Returns(dbGames.ToList());
            _installInfoDb.Setup(db => db.AwaitingCleanup)
                          .Returns(dbGames.Where(g => !g.Installed).ToList());

            _installedGameValidator.Setup(db => db.IsGameInstallValid(
                    It.IsAny<string>(),
                    It.IsIn(dbGames.Where(g => g != removedInfo).Select(g => g.Id))))
                .Returns(true);

            _installedGameValidator.Setup(db => db.IsGameInstallValid(
                    It.IsAny<string>(),
                    It.Is<string>(s => s == removedInfo.Id)))
                .Returns(false);

            var removed = cut.RemoveInvalidGames();

            Assert.True(removed.Count == 1);
            Assert.True(removed.First() == removedInfo.Id);
            Assert.True(!removedInfo.Installed);
        }

        public void Dispose()
        {
            Directory.Delete(libraryBaseDir, true);
        }
    }
}