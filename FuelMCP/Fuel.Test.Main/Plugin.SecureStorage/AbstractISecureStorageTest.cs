﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Amazon.Test.Main.Plugin.SecureStorage
{
    public abstract class AbstractISecureStorageTest
    {
        protected abstract ISecureStoragePlugin CreateInstance();

        protected readonly string context = "ISecureStorageTest";

        [Fact]
        public void EmptyGetsNull()
        {
            using (var storage = CreateInstance())
            {
                var value = storage.Get(context, "missing");
                Assert.Null(value);
            }
        }

        [Fact]
        public void EmptyGetSet()
        {
            var key = "key";
            var value = "value";

            using (var storage = CreateInstance())
            {
                storage.Put(context, key, value);

                Assert.Equal(value, storage.Get(context, key));

                // Cleanup
                storage.Remove(context);
            } 
        }

        [Fact]
        public void RoundTrip()
        {
            var key = "key";
            var value = "value";

            using (var storage = CreateInstance())
            {
                storage.Put(context, key, value);
            }

            using (var storage = CreateInstance())
            {
                Assert.Equal(value, storage.Get(context, key));

                // cleanup
                storage.Remove(context);
            }
        }

        [Fact]
        public void RemoveRemoves()
        {
            var key = "key";
            var value = "value";

            using (var storage = CreateInstance())
            {
                storage.Put(context, key, value);
                storage.Remove(context, key);
            }

            using (var storage = CreateInstance())
            {
                Assert.Equal(null, storage.Get(context, key));

                // Cleanup
                storage.Remove(context);
            }
        }

        [Fact]
        public void LargeValue()
        {
            var key = "largeValue";
            int numBytes = 1024 * 4096;

            byte[] value = new byte[numBytes];
            string sValue = Encoding.UTF8.GetString(value);

            using (var storage = CreateInstance())
            {
                storage.Put(context, key, sValue);

                Assert.Equal(numBytes, Encoding.UTF8.GetBytes(storage.Get(context, key)).Length);

                // Cleanup
                storage.Remove(context);
            }
        }

        [Fact]
        public void TestContext()
        {
            var key = "key";
            var value = "value";

            using (var storage = CreateInstance())
            {
                storage.Put("context1", key, value);

                Assert.Equal(value, storage.Get("context1", key));
                Assert.Null(storage.Get("context2", key));

                storage.Remove("context2", key);

                Assert.Equal(value, storage.Get("context1", key));

                // cleanup
                storage.Remove("context1");
                storage.Remove("context2");
            }
        }

        [Fact]
        public void TestRemoveContext()
        {
            using (var storage = CreateInstance())
            {
                storage.Put(context, "key1", "value1");
                storage.Put(context, "key2", "value2");
                storage.Put("context3", "key3", "value3");

                storage.Remove(context);

                Assert.Null(storage.Get(context, "key1"));
                Assert.Null(storage.Get(context, "key2"));
                Assert.Equal("value3", storage.Get("context3", "key3"));

                // Cleanup
                storage.Remove("context3");
            }
        }

        [Fact]
        public void TestNullContextGet()
        {
            using (var storage = CreateInstance())
            {
                Assert.Throws(typeof(ArgumentNullException), () => { storage.Get(null, "key"); });
            }
        }

        [Fact]
        public void TestNullContextPut()
        {
            using (var storage = CreateInstance())
            {
                Assert.Throws(typeof(ArgumentNullException), () => { storage.Put(null, "key", "value"); });
            }
        }

        [Fact]
        public void TestNullKeyGet()
        {
            using (var storage = CreateInstance())
            {
                Assert.Throws(typeof(ArgumentNullException), () => { storage.Get("context", null); });
            }
        }

        [Fact]
        public void TestNullKeyPut()
        {
            using (var storage = CreateInstance())
            {
                Assert.Throws(typeof(ArgumentNullException), () => { storage.Put("context", null, "value"); });
            }
        }

        [Fact]
        public void TestNullValuePut()
        {
            using (var storage = CreateInstance())
            {
                storage.Put(context, "null value", null);
                Assert.Null(storage.Get(context, "null value"));

                // cleanup
                storage.Remove(context);
            }
        }
    }
}
