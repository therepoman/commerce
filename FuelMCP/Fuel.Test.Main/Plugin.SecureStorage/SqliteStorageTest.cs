﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.SecureStorage;
using System.IO;

namespace Amazon.Test.Main.Plugin.SecureStorage
{
    public class SqliteStorageTest : AbstractISecureStorageTest
    {
        protected override ISecureStoragePlugin CreateInstance()
        {
            var storage = new SecureSQLiteStorage("SqliteStorageTest.sqlite");
            storage.Init();
            return storage;
        }
    }
}
