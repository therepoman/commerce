﻿using System;
using System.Linq;
using System.Collections.Generic;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.TwitchUserStorage;
using Amazon.Test.Main.Utilities;
using Moq;
using Xunit;
using Amazon.Fuel.Common;

namespace Amazon.Test.Main.Plugin.TwitchUserStorage.Test
{
    public class TwitchUserStoragePluginTest
    {
        [Fact]
        public void Get()
        {
            var ss = new Mock<ISecureStoragePlugin>();

            var twitchUser = Factories.ValidLocalTwitchUser();

            ss.Setup(s => s.Get(TwitchUserStoragePlugin.STORAGE_CONTEXT, TwitchUserStoragePlugin.KEY_OAUTH)).Returns(twitchUser.AccessToken);
            ss.Setup(s => s.Get(TwitchUserStoragePlugin.STORAGE_CONTEXT, TwitchUserStoragePlugin.KEY_TUID)).Returns(twitchUser.Tuid);
            ss.Setup(s => s.Get(TwitchUserStoragePlugin.STORAGE_CONTEXT, TwitchUserStoragePlugin.KEY_USERNAME)).Returns(twitchUser.Username);
            ss.Setup(s => s.Get(TwitchUserStoragePlugin.STORAGE_CONTEXT, TwitchUserStoragePlugin.KEY_SESSIONUSERID)).Returns(twitchUser.SessionUserId);
            ss.Setup(s => s.Get(TwitchUserStoragePlugin.STORAGE_CONTEXT, TwitchUserStoragePlugin.KEY_DISPLAY_NAME))
                .Returns(twitchUser.DisplayName);
            ss.Setup(s => s.Get(TwitchUserStoragePlugin.STORAGE_CONTEXT, TwitchUserStoragePlugin.KEY_LOGO)).Returns(twitchUser.Logo);

            var actual = new TwitchUserStoragePlugin(ss.Object).User;

            Assert.Equal(twitchUser, actual);
        }

        [Fact]
        public void Set()
        {
            var ss = new Mock<ISecureStoragePlugin>();

            var twitchUser = Factories.ValidLocalTwitchUser();
            new TwitchUserStoragePlugin(ss.Object).User = twitchUser;

            ss.Verify(s => s.PutAll(TwitchUserStoragePlugin.STORAGE_CONTEXT, It.Is<SecureStorageItem[]>(
                v => v.First(e => e.Key == TwitchUserStoragePlugin.KEY_OAUTH).Value == twitchUser.AccessToken
                    && v.First(e => e.Key == TwitchUserStoragePlugin.KEY_TUID).Value == twitchUser.Tuid
                    && v.First(e => e.Key == TwitchUserStoragePlugin.KEY_USERNAME).Value == twitchUser.Username
                    && v.First(e => e.Key == TwitchUserStoragePlugin.KEY_DISPLAY_NAME).Value == twitchUser.DisplayName
                    && v.First(e => e.Key == TwitchUserStoragePlugin.KEY_LOGO).Value == twitchUser.Logo
                    && v.First(e => e.Key == TwitchUserStoragePlugin.KEY_SESSIONUSERID).Value == twitchUser.SessionUserId)));
        }

        [Fact]
        public void Remove()
        {
            var ss = new Mock<ISecureStoragePlugin>();
            var cut = new TwitchUserStoragePlugin(ss.Object);

            cut.Remove();

            ss.Verify(s => s.Remove(TwitchUserStoragePlugin.STORAGE_CONTEXT, null));
        }

        [Fact]
        public void StoreOtherGameCredentialsEmpty()
        {
            ITwitchUserStoragePlugin cut = new TwitchUserStoragePlugin(new InMemoryStorage());

            TwitchCredentialSet set = cut.GetTwitchCredentialSet("client_id");
            Assert.Null(set);
        }

        [Fact]
        public void GetSetGameCredential()
        {
            ITwitchUserStoragePlugin cut = new TwitchUserStoragePlugin(new InMemoryStorage());

            var expected = new TwitchCredentialSet
            {
                AccessToken = Factories.ValidOauth(),
                Created = DateTime.Now.ToLocalTime(),
                Pid = Factories.ValidPid(),
                Entitlement = Factories.ValidEntitlement(),
                Scopes = Factories.ValidScopes()
            };

            var clientId = Factories.ValidClientId();

            cut.SetTwitchCredentialSet(clientId, expected);
            var actual = cut.GetTwitchCredentialSet(clientId);

            Assert.Equal(expected, actual);
        }
    }

    public class InMemoryStorage : Disposable, ISecureStoragePlugin
    {
        private Dictionary<string, Dictionary<string, string>> _data = new Dictionary<string, Dictionary<string, string>>();

        public bool Remove(string context, string key = null)
        {
            if (key == null)
            {
                _data.Remove(context);
                return true;
            }

            if (_data.ContainsKey(context))
            {
                _data[context].Remove(key);
                return true;
            }
            return false;
        }

        public void Put(string context, string key, string value)
        {
            if (!_data.ContainsKey(context))
            {
                _data[context] = new Dictionary<string, string>();
            }
            _data[context][key] = value;
        }

        public void PutAll(string context, IEnumerable<SecureStorageItem> items)
        {
            foreach (var item in items)
            {
                Put(context, item?.Key, item?.Value);
            }
        }

        public string Get(string context, string key)
        {
            if (!_data.ContainsKey(context))
            {
                return null;
            }
            return _data[context][key];
        }

        // wipe and clear are identical
        public void Clear() => Wipe();

        public void Wipe()
        {
            _data.Clear();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _data = null;
        }
    }
}
