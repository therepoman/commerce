﻿using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.App.TwitchInstallHelper;
using Xunit;

namespace Amazon.Test.Main.Configurations
{
    public class ShortcutGeneratorTest
    {
        //[Fact]
        //public void LauncherShortcutCreatorPassesThrough()
        //{
        //    var shortcutGenerator = new Mock<IShortcutDriver>();
        //    var os = new Mock<IOSDriver>();
        //    var cut = new ShortcutGenerator(shortcutGenerator.Object, os.Object);
        //
        //    var baseName = "BaseName";
        //    var description = "description";
        //    var appLocation = "AppLocation";
        //    os.SetupGet(o => o.AppLocation).Returns("AppLocation");
        //    var launcherArgs = new LauncherAppArgs
        //    {
        //        Mode = LauncherAppArgs.Modes.Run,
        //        ProductId = "ProductId",
        //    };
        //
        //    cut.CreateShortcut(ShortcutLocation.Desktop, baseName, description, launcherArgs);
        //
        //    var expectedArgs = string.Join(" ", launcherArgs.Args);
        //
        //    shortcutGenerator.Verify<string>(s => s.CreateShortcut(
        //        ShortcutLocation.Desktop,
        //        baseName,
        //        appLocation,
        //        description,
        //        expectedArgs,
        //        null,
        //        0));
        //}
    }

    public class LauncherAppParserTest
    {
        private TwitchInstallHelperArgParser parser;

        public LauncherAppParserTest()
        {
            parser = new TwitchInstallHelperArgParser();
        }

        [Fact]
        public void TestArgsToCmdline()
        {
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "a b c", "d", "e" }), "\"a b c\" d e");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "ab\"c", "\\", "d" }), "ab\\\"c \\ d");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "ab\"c", " \\", "d" }), "ab\\\"c \" \\\\\" d");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "a\\\\\\b", "de fg", "h" }), "a\\\\\\b \"de fg\" h");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "a\\\"b", "c", "d" }), "a\\\\\\\"b c d");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "a\\\\b c", "d", "e" }), "\"a\\\\b c\" d e");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "a\\\\b\\ c", "d", "e" }), "\"a\\\\b\\ c\" d e");
            Assert.Equal(CLIUtilities.ArgsToCmdline(new string[] { "ab", "" }), "ab \"\"");
        }
    }
}
