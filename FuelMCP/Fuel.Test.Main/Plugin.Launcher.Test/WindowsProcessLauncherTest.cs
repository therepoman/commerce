﻿using System;
using System.Diagnostics;
using System.IO;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Launcher.Models;
using Amazon.Test.Main.Utilities;
using Xunit;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Moq;

namespace Amazon.Test.Main.Plugin.Launcher.Test
{
    [LocalOnly]
    public class WindowsProcessLauncherTest
    {
        private readonly static string TEST_APP = "../../../../MCP.Test.SimpleTestApp/bin/MCP.Test.SimpleTestApp.exe";
        
        private WindowsProcessLauncher cut;
        private string fullPath;

        public WindowsProcessLauncherTest()
        {
            var metrics = new Mock<IMetricsPlugin>();
            var osDriver = new Mock<IOSDriver>();
            cut = new WindowsProcessLauncher(new LaunchUnelevatedServiceNoop(), metrics.Object, osDriver.Object);
            var processes = Process.GetProcessesByName("MCP.Test.SimpleTestApp");
            foreach (var process in processes)
            {
                try
                {
                    process.Kill();
                }
                catch (Exception e) { }
            }

            fullPath = new FileInfo(TEST_APP).FullName;
        }

        [Fact (Skip="Fails on build box due to concurrency")]
        public void ProcessDoesntExist()
        {
            var result = cut.LaunchProcess("Doesn't Exist", "DoesntExist.exe", null, new string[] {});
            Assert.Equal(LaunchResult.EStatus.FileNotFound, result.Status);
        }

        [Fact(Skip = "Fails on build box due to concurrency")]
        public void AlreadyRunning()
        {
            var previous = Process.Start(fullPath, "stay_open");

            try
            {
                var result = cut.LaunchProcessOrFocus("Already Running", null, fullPath, new string[] {});
                Assert.Equal(LaunchResult.EStatus.AlreadyRunning, result.Status);
            }
            finally
            {
                try
                {
                    previous.Kill();
                }
                catch (Exception) { }
            }
        }

        [Fact(Skip = "Fails on build box due to concurrency")]
        public void AlreadyRunning_NullArgs()
        {
            var previous = Process.Start(fullPath, "stay_open");

            try
            {
                var result = cut.LaunchProcess("Already Running", null, fullPath, null);
                Assert.Equal(LaunchResult.EStatus.Success, result.Status);
            }
            finally
            {
                try
                {
                    previous.Kill();
                }
                catch (Exception e) { }
            }
        }

        [Fact(Skip = "Fails on build box due to concurrency")]
        public void Success()
        {
            var runIn = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var result = cut.LaunchProcess("Success", runIn, fullPath, new string[] {"one", "two", "three"});

            Assert.Equal(result.Status, LaunchResult.EStatus.Success);
            Assert.NotNull(result.Process);
        }
    }
}
