﻿using System.Collections.Generic;
using System.IO;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Plugin.Launcher.Models;
using Amazon.Test.Main.Utilities;
using Xunit;

namespace Amazon.Test.Main.Plugin.Launcher.Test
{

    public class JsonLaunchConfigProviderTest
    {
        private JsonLaunchConfigProvider _provider;

        public JsonLaunchConfigProviderTest()
        {
            _provider = new JsonLaunchConfigProvider();
        }

        [Fact]
        public void CanParseWithSchemaVersion()
        {
            var sv = SchemaVersionObject.FromJson(TestData.ReadTestFile("Plugin.Launcher.Test/fuelV2.json"));
            Assert.Equal("2", sv.SchemaVersion);
        }

        [Fact]
        public void CheckFlags()
        {
            var sv = SchemaVersionObject.FromJson(TestData.ReadTestFile("Plugin.Launcher.Test/fuelV2.json"));
            Assert.Equal("2", sv.SchemaVersion);
        }

        LaunchConfig generateV2Json() => new LaunchConfig
        {
            Main = new MainExecutable
            {
                Args = new string[] { },
                Command = "Bin64\\Relay_x64.exe",
                PostRunLink = "https://amazongamestudios.au1.qualtrics.com/SE/?SID=SV_2iBiYjZQIy7cnGt",
                AuthScopes = new List<string>
                {
                    "user_read",
                    "channel_read",
                },
                ClientId = "client_id",
            },

            PostInstall = new List<Executable> {
                new Executable
                {
                    Command = "DirectX/DXSETUP.exe",
                    Args = new string[] { "/silent" },
                    ValidReturns = new int[] { 0 }
                },
                new Executable
                {
                    Command = "vcredist_x64.exe",
                    Args = new string[] { "/q", "/norestart" },
                    ValidReturns = new int[] { 0, 5100 },
                    AlwaysRun = true
                }
            },
        };


        [Fact]
        public void V2ParseAndConverts_NoFlags()
        {
            var expected = generateV2Json();

            var launchConfig = _provider.GetLaunchConfig(Path.Combine(TestData.TestDataDir(), "Plugin.Launcher.Test"), "fuelV2_noflags.json");
            Assert.Equal(expected, launchConfig);
            Assert.False(launchConfig.HasFlag(LaunchConfigFlags.KillIfSuspended));
        }

        [Fact]
        public void V2ParseAndConverts_Flags()
        {
            var expected = generateV2Json();
            expected.Flags = new List<string>
            {
                "kill_if_suspended"
            };

            var launchConfig = _provider.GetLaunchConfig(Path.Combine(TestData.TestDataDir(), "Plugin.Launcher.Test"), "fuelV2.json");
            Assert.Equal(expected, launchConfig);
            Assert.True(launchConfig.HasFlag(LaunchConfigFlags.KillIfSuspended));
        }

        [Fact]
        public void V2ParseAndConverts_NoWorkingPathOverride()
        {
            var expected = generateV2Json();

            var launchConfig = _provider.GetLaunchConfig(Path.Combine(TestData.TestDataDir(), "Plugin.Launcher.Test"), "fuelV2_noflags.json");
            Assert.Equal(expected, launchConfig);
            Assert.True(string.IsNullOrEmpty(launchConfig.Main.WorkingSubdirOverride));
        }

        [Fact]
        public void V2ParseAndConverts_WorkingPathOverride()
        {
            var expected = generateV2Json();
            var expectedSubdir = @"some\test\subdir";
            expected.Main.WorkingSubdirOverride = expectedSubdir;

            var launchConfig = _provider.GetLaunchConfig(Path.Combine(TestData.TestDataDir(), "Plugin.Launcher.Test"), "fuelV2_workingDir.json");
            Assert.Equal(expected, launchConfig);
            Assert.True(launchConfig.Main.WorkingSubdirOverride == expectedSubdir);
        }
    }
}