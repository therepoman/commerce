﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Launcher.Messages;
using Amazon.Fuel.Plugin.Launcher.Models;
using Amazon.Test.Main.Common.Test;
using Autofac;
using Moq;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Messages;
using Amazon.Test.Main.Utilities;
using Xunit;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Amazon.Test.Main.Fuel.Manifest;
using Clients.Twitch.Spade.Model;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Test.Main.Plugin.Launcher.Test
{
    public class LaunchGameTest
    {
        private readonly TempFileSystem fs;
        private readonly Mock<IMessengerHub> msg = new Mock<IMessengerHub>();
        private readonly Mock<IEntitlementPlugin> entitlementPlugin = new Mock<IEntitlementPlugin>();
        private readonly Mock<IEntitlementsDatabase> entitlementDb = new Mock<IEntitlementsDatabase>();
        private readonly Mock<IDownloadPlugin> downloadPlugin = new Mock<IDownloadPlugin>();
        private readonly Mock<ILibraryPlugin> libraryPlugin = new Mock<ILibraryPlugin>();
        private readonly Mock<IGameInstaller> gameInstaller = new Mock<IGameInstaller>();
        private readonly Mock<IGamePrereqInstaller> prereqInstaller = new Mock<IGamePrereqInstaller>();
        private readonly Mock<IProcessLauncher> processLauncher = new Mock<IProcessLauncher>();
        private readonly Mock<ITwitchLoginPlugin> twitchLoginPlugin = new Mock<ITwitchLoginPlugin>();
        private readonly Mock<ILaunchConfigProvider> launchConfig = new Mock<ILaunchConfigProvider>();
        private readonly Mock<IShortcutManager> shortcutMgr = new Mock<IShortcutManager>();
        private readonly Mock<IOSServicesDriver> osServicesDriver = new Mock<IOSServicesDriver>();
        private readonly Mock<IHelperLauncherProvider> helperLauncherProvider = new Mock<IHelperLauncherProvider>();
        private readonly TestableClock clock = new TestableClock(2015, 8, 24);
        private readonly Mock<ITwitchUserStoragePlugin> twitchUserStorage = new Mock<ITwitchUserStoragePlugin>();
        private readonly Mock<IInstalledGamesPlugin> install = new Mock<IInstalledGamesPlugin>(); 
        private readonly Mock<IUninstallPlugin> uninstall = new Mock<IUninstallPlugin>();
        private readonly Mock<IOSDriver> osDriver = new Mock<IOSDriver>();
        private readonly Mock<IManifestStorage> manifestStorage = new Mock<IManifestStorage>();
        private readonly Mock<IInstalledGamesPlugin> installedGames = new Mock<IInstalledGamesPlugin>();
        private readonly Mock<IDeferredDeletionService> deferredDeletionService = new Mock<IDeferredDeletionService>();
        private readonly Mock<IInstallHelperIpcController> installHelperIpcController = new Mock<IInstallHelperIpcController>();
        
        private LauncherServices launcher;
        private FuelGameInstallerService installer;
        private IMetricsPlugin metrics = new TestableMetrics();

        public LaunchGameTest()
        {
            fs = new TempFileSystem(GetType());

            launcher = new LauncherServices(
                osDriver.Object,
                msg.Object,
                metrics,
                entitlementDb.Object,
                entitlementPlugin.Object,
                libraryPlugin.Object,
                processLauncher.Object,
                twitchLoginPlugin.Object,
                launchConfig.Object,
                shortcutMgr.Object,
                clock,
                twitchUserStorage.Object,
                osServicesDriver.Object,
                install.Object);

            installer = new FuelGameInstallerService(
                osDriver.Object,
                msg.Object,
                metrics,
                entitlementDb.Object,
                downloadPlugin.Object,
                libraryPlugin.Object,
                gameInstaller.Object,
                prereqInstaller.Object,
                processLauncher.Object,
                osServicesDriver.Object,
                uninstall.Object,
                manifestStorage.Object,
                launcher,
                installedGames.Object,
                launchConfig.Object,
                deferredDeletionService.Object,
                installHelperIpcController.Object);
        }

        /// <summary>
        /// Args are not replaced yet here:
        /// </summary>
        /// <returns></returns>
        private LaunchConfig GenSampleConfig()
        {
            var output = new LaunchConfig();
            output.PostInstall.Add(new Executable{ Command = "sample.exe", Args = new string[] { "-silent" }});

            output.Main = new MainExecutable
            {
                Command = "Game.Exe",
                Args = new string[] {"-oauth","{OAuthToken}","-entitlementId","{EntitlementId}"},
                PostRunLink = @"http://www.amazon.com/",
                AuthScopes = {"user_read"},
                ClientId = Factories.ValidClientId(),
            };

            return output;
        }

        // TODO: this is broken until Factories.ValidInstallHelperSession() is fixed
        [Fact(Skip = "Factories.ValidInstallHelperSession needs proxy wrapper")]
        public async void LaunchGameTest_Succeeds()
        {
            var game = Factories.ValidGame();
            var prod = Factories.ValidGameProductInfo();
            var expectedManifest = TestManifestGenerator.RandomManifest();
            var expectedLaunchConfig = GenSampleConfig();

            var tempDir = fs.TempFolder;
            var installInfo = new GameInstallInfo { InstallDirectory = tempDir};
            var manifestWithVersion = new ManifestWithVersion() {ManifestData = expectedManifest};

            var ipcSession = Factories.ValidInstallHelperSession();

            manifestStorage.Setup(storage =>
                storage.PutRaw(game.ProductId, It.IsAny<string>(), It.IsAny<Stream>(), It.IsAny<bool>())).ReturnsAsync(true);
            entitlementPlugin.Setup(e => e.DownloadManifestAsync(game.EntitlementId, game.ProductId, default(CancellationToken))).ReturnsAsync(manifestWithVersion);
            entitlementDb.Setup(e => e.Get(game.ProductId)).Returns(prod);
            downloadPlugin.Setup(d => d.DownloadProductWithRetryASync(It.IsAny<ProductId>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int[]>(), It.IsAny<bool>(), It.IsAny<Sds.Manifest>(), It.IsAny<CancellationToken>()))
                          .ReturnsAsync(new DownloadProductResult() {Result = EDownloadJobResult.Success, ManifestWithVersion = manifestWithVersion });
            gameInstaller.Setup(l => l.InstallGame(game.ProductId, installInfo, helperLauncherProvider.Object, manifestWithVersion.SdsVersionId, EInstallGameFlags.All, default(CancellationToken))).Returns(EInstallUpdateProductResult.Succeeded);
            libraryPlugin.Setup(l => l.GetGameInstallInfo(game.ProductId)).Returns(installInfo);
            libraryPlugin.Setup(l => l.GameRegistered(game.ProductId)).Returns(true);
            libraryPlugin.Setup(l => l.GetGame(game.ProductId)).Returns(game);
            var cachedGames = new Dictionary<ProductId, GameStateInfo>();
            cachedGames[game.ProductId] = new GameStateInfo();
            libraryPlugin.Setup(l => l.CachedGameStates).Returns(cachedGames);
            uninstall.Setup(u => u.TryEnsureUninstallerExeInCommonLocation()).Returns(true);

            launchConfig.Setup(lc => lc.GetLaunchConfig(fs.TempFolder, "fuel.json")).Returns(expectedLaunchConfig);
            
            installer.InstallOrUpdateProduct(game.ProductId, tempDir, isUpdate: false, installDesktopShortcut: true);
            //msg.Verify(h => h.Publish(It.Is<MessageLaunchGameFailed>(m => game.EntitlementId == m.EntitlementId)),
            //    Times.Once());
            msg.Verify(h => h.Publish( It.Is<MessageInstallUpdateProductSucceeded>(m => game.ProductId == m.Content)),
                Times.Once());
        }

        public class TestableMetrics : IMetricsPlugin
        {
            public void Dispose() { }
            public void FlushQueue() { }
            public void AddCounter(string source, string name, string pivotKey, string pivotValue) { }
            public void AddCounter(string source, string name) { }
            public void AddDiscreteValue(string source, string name, int value, string pivotKey, string pivotValue) { }
            public void AddDiscreteValue(string source, string name, int value) { }
            public void AddTimerValue(string source, string name, double value) { }
            public void AddAppStartTimer(string source, string name, string pivotKey, string pivotValue) { }
            public void AddAppStartTimer(string source, string name) { }
            public IMetricsTimer StartTimer(string source, string name)
            {
                return new Mock<IMetricsTimer>().Object;
            }

            public void SendSpadeEvent(ISpadeEvent spadeEvent) { }

            public IMetricScope CreateScope(string source)
            {
                return new Mock<IMetricScope>().Object;
            }
        }

        // TODO Factories.ValidInstallHelperSession() will fail, create testable proxy object wrapping BufferedIPC 
        [Fact(Skip = "Factories.ValidInstallHelperSession needs proxy wrapper")]
        public async void LaunchGameTest_LaunchesProcess()
        {
            var game = Factories.ValidGame();
            var version = Factories.ValidVersionId();
            var tempDir = fs.TempFolder;
            var installInfo = new GameInstallInfo
            {
                InstallDirectory = tempDir,
                Installed = true,
                InstallVersion = version
            };
            var expectedVersionIds = new Dictionary<string, string> {{game.ProductId.Id, version}};
            var expectedLaunchConfig = GenSampleConfig();
            var gameAuthToken = Factories.ValidOauth();
            var processLaunchResult = Factories.LaunchResultSuccess();
            var ipcSession = Factories.ValidInstallHelperSession();

            processLaunchResult.Process = Process.GetCurrentProcess();
                // WHEEEE careful what you do with this you could kill the vm :)

            entitlementPlugin.Setup(e => e.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>())).ReturnsAsync(expectedVersionIds);
            gameInstaller.Setup(l => l.InstallGame(game.ProductId, installInfo, helperLauncherProvider.Object, version, EInstallGameFlags.All, default(CancellationToken)))
                .Returns(EInstallUpdateProductResult.Succeeded);
            libraryPlugin.Setup(l => l.GameRegistered(game.ProductId)).Returns(true);
            libraryPlugin.Setup(l => l.GetGameInstallInfo(game.ProductId)).Returns(installInfo);
            libraryPlugin.Setup(l => l.GetGame(game.ProductId)).Returns(game);
            launchConfig.Setup(l => l.GetLaunchConfig(tempDir, "fuel.json")).Returns(expectedLaunchConfig);
            processLauncher.Setup(s => s.LaunchGame(null, tempDir, expectedLaunchConfig, null, false))
                .Returns(processLaunchResult);

            install.Setup(
                    i => i.CheckUpToDateByInstallInfo(installInfo, ERetrieveVersionCondition.UpdateIfExpired))
                .ReturnsAsync(ECheckUpToDateResult.UpToDate);

            twitchLoginPlugin.Setup(
                    t => t.GetGameToken(expectedLaunchConfig.Main.ClientId, expectedLaunchConfig.Main.AuthScopes))
                .Returns(gameAuthToken);

            launchConfig.Setup(lc => lc.GetLaunchConfig(fs.TempFolder, "fuel.json")).Returns(expectedLaunchConfig);
            twitchLoginPlugin.Setup(tw => tw.GetAndValidateAccessToken()).Returns("FAKETOKEN");

            var result = await launcher.LaunchProduct(game.ProductId, null, false, true, null);
            Assert.Equal(ELaunchProductResult.Succeeded, result);

            processLauncher.Verify(s => s.LaunchGame(null, tempDir, expectedLaunchConfig, null, false));

            // Verify we stuffed credentials for the sdk to hand to the game
            var exectedTwitchCredentials = new TwitchCredentialSet
            {
                Pid = processLaunchResult.Process.Id,
                AccessToken = gameAuthToken,
                Entitlement = game.EntitlementId,
                Created = clock.Now(),
                Scopes = expectedLaunchConfig.Main.AuthScopes
            };
            twitchUserStorage.Verify(
                t => t.SetTwitchCredentialSet(expectedLaunchConfig.Main.ClientId, exectedTwitchCredentials));
        }

        [Fact]
        public async void LaunchGameTest_EntitlementMissing()
        {
            Game validGame = Factories.ValidGame();
            libraryPlugin.Setup(l => l.GetGame(validGame.ProductId)).Returns(() => null);

            var result = await launcher.LaunchProduct(validGame.ProductId, null, false, true, null);
            Assert.Equal(ELaunchProductResult.NotEntitled, result);
        }

        [Fact]
        public async void LaunchGameTest_FetchRemoteIfEntitlementMissing()
        {
            Game validGame = Factories.ValidGame();
            Game dbGame = null;

            libraryPlugin.Setup(l => l.GetGame(validGame.ProductId)).Returns(() => dbGame);
            entitlementDb.Setup(e => e.SyncGoods()).Callback(() => { dbGame = validGame; }).ReturnsAsync(true);

            var result = await launcher.LaunchProduct(validGame.ProductId, null, false, true, null);
            Assert.NotEqual(ELaunchProductResult.NotEntitled, result);
        }

        [Fact]
        public async void LaunchGame_DeveloperWithDir_DoesntInstall()
        {
            var devDir = Path.Combine("some", "directory");
            var game = Factories.DeveloperGame();
            var version = Factories.ValidVersionId();
            var expectedLaunchConfig = GenSampleConfig();
            var gameAuthToken = Factories.ValidOauth();
            var processLaunchResult = Factories.LaunchResultSuccess();
            var expectedVersionIds = new Dictionary<string, string> { { game.ProductId.Id, version } };
            processLaunchResult.Process = Process.GetCurrentProcess(); // WHEEEE careful what you do with this you could kill the vm :)

            entitlementPlugin.Setup(e => e.CurrentVersionIdsForAdgProductIds(It.IsAny<IList<string>>())).ReturnsAsync(expectedVersionIds);
            libraryPlugin.Setup(l => l.GameRegistered(game.ProductId)).Returns(true);
            libraryPlugin.Setup(l => l.GetGame(game.ProductId)).Returns(game);
            processLauncher.Setup(s => s.LaunchGame(null, devDir, expectedLaunchConfig, null, false)).Returns(processLaunchResult);

            launchConfig.Setup(l => l.GetLaunchConfig(devDir, "fuel.json")).Returns(expectedLaunchConfig);
            twitchLoginPlugin.Setup(
                t => t.GetGameToken(expectedLaunchConfig.Main.ClientId, expectedLaunchConfig.Main.AuthScopes))
                .Returns(gameAuthToken);

            launchConfig.Setup(lc => lc.GetLaunchConfig(devDir, "fuel.json")).Returns(expectedLaunchConfig);
            twitchLoginPlugin.Setup(tw => tw.GetAndValidateAccessToken()).Returns("FAKETOKEN");

            // Verify we stuffed credentials for the sdk to hand to the game
            var exectedTwitchCredentials = new TwitchCredentialSet
            {
                Pid = processLaunchResult.Process.Id,
                AccessToken = gameAuthToken,
                Entitlement = game.EntitlementId,
                Created = clock.Now(),
                Scopes = expectedLaunchConfig.Main.AuthScopes
            };
            await launcher.LaunchProduct(game.ProductId, null, false, false, developerDir: devDir);

            twitchUserStorage.Verify(t => t.SetTwitchCredentialSet(expectedLaunchConfig.Main.ClientId, exectedTwitchCredentials));
        }

        [Theory,
            InlineData("<"),
            InlineData(">"),
            InlineData(":"),
            InlineData("/"),
            InlineData(@"\"),
            InlineData("|"),
            InlineData("?"),
            InlineData("*"),
            InlineData(" "),
            InlineData("The Walking Dead: Season 2")]
        public void LaunchGameTest_CreateWindowsShortcuts(string testName)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<WindowsFileSystemDriver>()
                .As<IOSDriver>()
                .OnActivating(e => e.Instance.IsDevMode = true)
                .SingleInstance();
            builder.RegisterType<WindowsShortcutDriver>().As<IShortcutDriver>().SingleInstance();
            var container = builder.Build();
            
            var shortcutGen = container.Resolve<IShortcutDriver>();
        
            string name = $"LaunchGameTest_CreateWindowsShortcut_{testName}";
        
            // NOTE: shortcut will point to dll for unit tests (no exe)
            var result = shortcutGen.TryCreateUrl(
                            location: ShortcutLocation.Temp,
                            shortcutBaseName: name,
                            targetUrl: @"http://www.google.com",
                            description: "test description",
                            iconLocation:@"c:\windows\notepad.exe");
        
            Assert.True(File.Exists(result));
            File.Delete(result);
        }
    }
}
