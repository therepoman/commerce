﻿using Amazon.Fuel.Common.CommandLineParser;
using Xunit;

namespace Amazon.Test.Main.ApplicationDataRemover
{
    public class AppDataRemoverParserTest
    {
        [Fact]
        public void DefaultsToAllWithConfirmAndNoDevMode()
        {
            var parser = new AppDataRemoverParser();
            var args = new string[] { };

            var expected = new AppDataRemoverArgs
            {
                Mode = AppDataRemoverArgs.Modes.AllWithConfirm,
                DevMode = false
            };
            Assert.Equal(expected, parser.Parse(args));
        }

        [Fact]
        public void DevModeArg()
        {
            var parser = new AppDataRemoverParser();
            var args = new string[] { "-m", "all", "--dev_mode" };

            var expected = new AppDataRemoverArgs
            {
                Mode = AppDataRemoverArgs.Modes.All,
                DevMode = true
            };
            Assert.Equal(expected, parser.Parse(args));
        }

        [Theory,
            InlineData(AppDataRemoverArgs.Modes.Game)]
        public void NoProductIdFails(AppDataRemoverArgs.Modes modes)
        {
            var parser = new AppDataRemoverParser();
            var args = new string[] { "-m", modes.ToString() };

            var expected = new AppDataRemoverArgs
            {
                Mode = modes,
            };
            var ex = Assert.Throws<InvalidCliException>(() => parser.Parse(args));
            Assert.Equal($"{modes} requires a productid", ex.Message);
        }

        [Theory,
            InlineData(AppDataRemoverArgs.Modes.Game, "product_id")]
        public void ProductIdPasses(AppDataRemoverArgs.Modes modes, string productId)
        {
            var parser = new AppDataRemoverParser();
            var args = new string[] { "-m", modes.ToString(), "-p", productId };

            var expected = new AppDataRemoverArgs
            {
                Mode = modes,
                ProductId = productId,
            };
            var result = parser.Parse(args);
            Assert.Equal(expected, result);
        }

        [Theory,
            InlineData(AppDataRemoverArgs.Modes.All)]
        public void AllPasses(AppDataRemoverArgs.Modes modes)
        {
            var parser = new AppDataRemoverParser();
            var args = new[] { "-m", modes.ToString() };

            var expected = new AppDataRemoverArgs
            {
                Mode = modes
            };

            var result = parser.Parse(args);
            Assert.Equal(expected, result);
        }

        [Theory,
            InlineData(AppDataRemoverArgs.Modes.Game),
            InlineData(AppDataRemoverArgs.Modes.All)]
        public void ToString_AllTypes(AppDataRemoverArgs.Modes modes)
        {
            var l1 = new[] { "-m", "Game"};
            var laa1 = new AppDataRemoverArgs
            {
                Mode = AppDataRemoverArgs.Modes.Game
            };
            Assert.Equal(l1, laa1.Args);
            Assert.Equal(string.Join(" ", l1), laa1.ArgsString);

            var l2 = new[] { "-m", "All" };
            var laa2 = new AppDataRemoverArgs
            {
                Mode = AppDataRemoverArgs.Modes.All
            };
            Assert.Equal(l2, laa2.Args);
            Assert.Equal(string.Join(" ", l2), laa2.ArgsString);
        }

        [Theory,
            InlineData(AppDataRemoverArgs.Modes.All, "en-US")]
        public void SetLanguageParam(AppDataRemoverArgs.Modes modes, string languageCode)
        {
            var parser = new AppDataRemoverParser();
            var args = new[] { "-m", modes.ToString(), "-l", languageCode };

            var expected = new AppDataRemoverArgs
            {
                Mode = modes,
                LanguageCode = languageCode
            };

            var result = parser.Parse(args);
            Assert.Equal(expected, result);
        }
    }
}
