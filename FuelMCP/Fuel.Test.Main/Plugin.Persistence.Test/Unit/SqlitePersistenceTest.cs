﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.Plugin.Persistence.Models;
using Castle.Core.Internal;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Xunit;
using Moq;

namespace Amazon.Test.Main.Plugin.Persistence.Test.Unit
{
    public class SqlitePersistenceTest : IDisposable
    {
        private TestablePersistence persistence;
        private string dir;
        private SqliteDatabase<TestEntry, TestEntryDbContext> db;
        private Mock<IMessengerHub> messengerHub = new Mock<IMessengerHub>();
        private Mock<IMetricsPlugin> metrics = new Mock<IMetricsPlugin>();
        private Mock<IOSDriver> osDriver = new Mock<IOSDriver>();

        public SqlitePersistenceTest()
        {
            dir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            persistence = new TestablePersistence(dir);
            db = new SqliteDatabase<TestEntry, TestEntryDbContext>(persistence, messengerHub.Object, metrics.Object, osDriver.Object);

            osDriver.Setup(os => os.HasPermissions(SqliteDatabase<TestEntry, TestEntryDbContext>.GetSqlDbFile(persistence), FileSystemRights.Write)).Returns(true);
            osDriver.Setup(os => os.HasPermissions(SqliteDatabase<TestEntry, TestEntryDbContext>.GetSqlDbFile(persistence), FileSystemRights.Read)).Returns(true);
        }

        public void Dispose()
        {
            try
            {
                if (persistence != null)
                {
                    persistence.Dispose();
                }
                Directory.Delete(dir, true);
            }
            catch (Exception e)
            {
            }
        }

        [Fact]
        public void Persistence_AddAndAllTest()
        {
            TestEntry entryOne = new TestEntry();
            entryOne.Id = "12345";
            entryOne.MyBoolean = false;
            entryOne.MyInt = 5;
            entryOne.MyFloat = 3.5F;
            entryOne.MyString = "entryOne";
            entryOne.MyDate = DateTime.Now;

            TestEntry entryTwo = new TestEntry();
            entryTwo.Id = "abcdef";
            entryTwo.MyBoolean = true;
            entryTwo.MyInt = 10;
            entryTwo.MyFloat = 5.5F;
            entryTwo.MyString = "entryTwo";
            entryTwo.MyDate = DateTime.Today;
            db.AddOrReplaceById(entryOne);
            db.AddOrReplaceById(entryTwo);

            // Check to make sure both were added properly
            List<TestEntry> dbOutput = db.All;
            Assert.Equal(2, dbOutput.Count);
        }

        [Fact]
        public void GetEmptyTest()
        {
            Assert.Null(db.TryFindById("Doesn't Exist"));
        }

        [Fact]
        public void AddRemove()
        {
            TestEntry entryOne = new TestEntry
            {
                Id = "abcdef",
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };
            db.AddOrReplaceById(entryOne);
            db.Remove(entryOne);

            Assert.Null(db.TryFindById(entryOne.Id));
        }

        [Fact]
        public void GetTest()
        {
            TestEntry entryOne = new TestEntry
            {
                Id = "abcdef",
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            TestEntry entryTwo = new TestEntry
            {
                Id = "12345",
                MyBoolean = false,
                MyInt = 10,
                MyFloat = 5.5F,
                MyString = "entryTwo",
                MyDate = DateTime.Today
            };
            
            db.AddOrReplaceById(entryOne);
            db.AddOrReplaceById(entryTwo);

            TestEntry loadedEntryOne = db.TryFindById("abcdef");
            Assert.Equal(entryOne, loadedEntryOne);
            TestEntry loadedEntryTwo = db.TryFindById("12345");
            Assert.Equal(entryTwo, loadedEntryTwo);
        }

        [Fact]
        public void GetLowerTest()
        {
            string _id = "ThisTestsForUpperAndLowerCase";
            TestEntry entryOne = new TestEntry
            {
                Id = _id,
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            db.AddOrReplaceById(entryOne);

            TestEntry loadedEntryOne = db.TryFindById(_id);
            Assert.Equal(entryOne, loadedEntryOne);
        }

        [Fact]
        public void GetStopWordsTest()
        {
            var stopWords = new System.String[] { "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with" };
            string _id = String.Join(" ", stopWords);
            TestEntry entryOne = new TestEntry
            {
                Id = _id,
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            db.AddOrReplaceById(entryOne);

            TestEntry loadedEntryOne = db.TryFindById(_id);
            Assert.Equal(entryOne, loadedEntryOne);
        }

        /// <summary>
        /// Tests that the standard Lucene filter is not being applied.
        /// </summary>
        [Fact]
        public void GetTestForStandardFilter()
        {
            string id = "Michael's standard filter test. A.C.R.O.N.Y.M.";
            TestEntry entryOne = new TestEntry
            {
                Id = id,
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            db.AddOrReplaceById(entryOne);

            var loadedEntryOne = db.TryFindById(id);
            Assert.Equal(entryOne, loadedEntryOne);
        }

        [Fact]
        public void GetBySingleQuery()
        {
            string id = "Michael's standard filter test. A.C.R.O.N.Y.M.";
            TestEntry entryOne = new TestEntry
            {
                Id = id,
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            db.AddOrReplaceById(entryOne);

            //var term = new Term(nameof(TestEntry.MyString), entryOne.MyString);
            //var query = new TermQuery(term);
            //var loadedEntryOne = db.Get(query);
            //Assert.Equal(entryOne, loadedEntryOne);
            //
            //loadedEntryOne = db.Get(term);
            //Assert.Equal(entryOne, loadedEntryOne);
        }

        [Fact]
        public void GetByBooleanQuery()
        {
            string id = "Michael's standard filter test. A.C.R.O.N.Y.M.";
            TestEntry entryOne = new TestEntry
            {
                Id = id,
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            db.AddOrReplaceById(entryOne);

            //var query = NumericRangeQuery.NewIntRange(nameof(TestEntry.MyBoolean), 1, 1, true, true);
            //var loadedEntryOne = db.Get(query);
            //Assert.Equal(entryOne, loadedEntryOne);
        }

        [Fact]
        public void GetByMultipleQuery()
        {
            string id = "Michael's standard filter test. A.C.R.O.N.Y.M.";
            TestEntry entryOne = new TestEntry
            {
                Id = id,
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            db.AddOrReplaceById(entryOne);

            var idTerm = new Term(nameof(PersistentEntry.Id), id);
            var stringTerm = new Term(nameof(TestEntry.MyString), "entryOne");
            var query = new BooleanQuery {{new TermQuery(idTerm), Occur.MUST}, {new TermQuery(stringTerm), Occur.MUST}};

            //var loadedEntryOne = db.Get(query);
            //Assert.Equal(entryOne, loadedEntryOne);
        }



        [Fact]
        public void AllEmpty()
        {
            Assert.Empty(db.All);
        }

        [Fact]
        public void AllTest()
        {
            TestEntry entryOne = new TestEntry
            {
                Id = "abcdef",
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };

            TestEntry entryTwo = new TestEntry
            {
                Id = "12345",
                MyBoolean = false,
                MyInt = 10,
                MyFloat = 5.5F,
                MyString = "entryTwo",
                MyDate = DateTime.Today
            };
            
            db.AddOrReplaceById(entryOne);
            db.AddOrReplaceById(entryTwo);

            List<TestEntry> dbOutput = db.All;
            Assert.Equal(2, dbOutput.Count);
            TestEntry loadedEntryOne;
            TestEntry loadedEntryTwo;
            if (dbOutput[0].Id.Equals("abcdef"))
            {
                loadedEntryOne = dbOutput[0];
                loadedEntryTwo = dbOutput[1];
            }
            else
            {
                loadedEntryOne = dbOutput[1];
                loadedEntryTwo = dbOutput[0];
            }
            
            Assert.Equal(entryOne, loadedEntryOne);
            Assert.Equal(entryTwo, loadedEntryTwo);
        }

        [Fact]
        public void UpdateTest()
        {
            TestEntry entryOne = new TestEntry
            {
                Id = "abcdef",
                MyBoolean = true,
                MyInt = 5,
                MyFloat = 3.5F,
                MyString = "entryOne",
                MyDate = DateTime.Now
            };
            
            db.AddOrReplaceById(entryOne);

            TestEntry loadedEntryOne = db.TryFindById("abcdef");
            Assert.Equal(entryOne, loadedEntryOne);

            // Has the same Id as entryOne, should update
            TestEntry entryTwo = new TestEntry
            {
                Id = "abcdef",
                MyBoolean = false,
                MyInt = 10,
                MyFloat = 5.5F,
                MyString = "entryTwo",
                MyDate = DateTime.Today
            };

            db.AddOrReplaceById(entryTwo);

            TestEntry loadedEntryTwo = db.TryFindById("abcdef");
            Assert.Equal(entryTwo, loadedEntryTwo);
        }

        [Fact]
        public void AddIncompleteTest()
        {
            TestEntry entryOne = new TestEntry
            {
                Id = "abcdef",
                MyBoolean = true
            };
            
            db.AddOrReplaceById(entryOne);

            TestEntry loadedEntry = db.TryFindById("abcdef");
            Assert.Equal(entryOne, loadedEntry);
        }

        [Fact]
        public void TwoCanWrite()
        {
            var id = "one";
            var db2 = new SqliteDatabase<TestEntry, TestEntryDbContext>(persistence, messengerHub.Object, metrics.Object, osDriver.Object);
            TestEntry t1 = new TestEntry {Id = id, MyString = "one"};
            TestEntry t2 = new TestEntry { Id = id, MyString = "Updated" };

            // Write to first DB and flush
            db.AddOrReplaceById(t1);
            Assert.Equal(t1, db.TryFindById(id));

            // Write conflict to 2nd db and fetch
            db2.AddOrReplaceById(t2);
            Assert.Equal(t2, db2.TryFindById(id));
          
            // Verify that db1 gets t2's write
            Assert.Equal(t2, db.TryFindById(id));

            // Overwrite from db1 again
            db.AddOrReplaceById(t1);

            // Verify that t2 gets it
            Assert.Equal(t1, db2.TryFindById(id));
        }

        [Fact]
        public void FilterByTypes()
        {
            TestEntry t1 = new TestEntry { Id = "id1", MyString = "one" };
            TestEntry t2 = new TestEntry { Id = "id2", MyString = "Updated" };

            db.AddOrReplaceById(t1);
            db.AddOrReplaceById(t2);

            //var results = db.AllByQuery(new TermQuery(new Term("MyString", "Updated")));
            //Assert.Equal(1, results.Count);
            //Assert.Equal(t2, results[0]);
        }

        [Fact]
        public void ReplaceAllTest()
        {
            TestEntry t1 = new TestEntry { Id = "id1", MyString = "one" };
            TestEntry t2 = new TestEntry { Id = "id2", MyString = "two" };
            TestEntry t3 = new TestEntry { Id = "id3", MyString = "three" };

            db.AddOrReplaceById(t1);
            db.AddOrReplaceById(t2);

            List<TestEntry> tList = new List<TestEntry>();
            tList.Add(t3);

            db.ReplaceAll(tList);
            var results = db.All;
            Assert.Equal(1, results.Count);
            Assert.Equal(t3, results[0]);
        }

        [Fact]
        public void RemoveManyById()
        {
            TestEntry t1 = new TestEntry { Id = "id1", MyString = "one" };
            TestEntry t2 = new TestEntry { Id = "id2", MyString = "two" };
            TestEntry t3 = new TestEntry { Id = "id3", MyString = "three" };

            db.AddOrReplaceById(t1);
            db.AddOrReplaceById(t2);
            db.AddOrReplaceById(t3);

            List<string> tList = new List<string>();
            tList.Add(t1.Id);
            tList.Add(t3.Id);

            db.RemoveManyById(tList);
            var results = db.All;
            Assert.Equal(1, results.Count);
            Assert.Equal(t2, results[0]);
        }

        [Fact]
        public void MTLockingHandlesAsyncAccess()
        {
            TestEntry t1 = new TestEntry { Id = "id1", MyString = "one" };
            TestEntry t2 = new TestEntry { Id = "id2", MyString = "two" };
            TestEntry t3 = new TestEntry { Id = "id3", MyString = "three" };

            db.AddOrReplaceById(t1);
            db.AddOrReplaceById(t2);
            db.AddOrReplaceById(t3);

            TestEntry t4 = new TestEntry { Id = "id4", MyString = "four" };

            int iter = 25;
            var guids = new string[iter];
            Parallel.For(0, iter, i =>
            {
                try { Thread.CurrentThread.Name = $"test{i}"; } catch { }

                guids[i] = Guid.NewGuid().ToString();
                TestEntry tUnique = new TestEntry { Id = guids[i], MyString = "unique" };
                db.AddOrReplaceById(tUnique);
                db.Remove(t3);
                db.AddOrReplaceById(t4);
            });

            var count = db.All.Count;
            Assert.Equal(count, iter + 3); // had 3, added 1, removed 1 static, added 'iter' uniques
            Assert.Null(db.TryFindById(t3.Id));
            Assert.NotNull(db.TryFindById(t4.Id));
            foreach (var guid in guids)
            {
                Assert.NotNull(db.TryFindById(guid));
            }
        }
    }
}
