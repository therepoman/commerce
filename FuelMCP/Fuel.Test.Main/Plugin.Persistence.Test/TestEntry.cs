﻿using System;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Amazon.Test.Main.Plugin.Persistence.Test
{
    class TestEntry : PersistentEntry
    {
        public override string Type => "testentry";
        public override string StorageContext => "TestEntry";

        public bool MyBoolean { get; set; }
        public int MyInt { get; set; }
        public string MyString { get; set; }
        public float MyFloat { get; set; }
        public DateTime MyDate { get; set; }
    }

    class TestEntryDbContext : SqliteDbContext<TestEntry>
    {
        public TestEntryDbContext() : base(new DbContextOptionsBuilder<SqliteDbContext<TestEntry>>().Options) { }
    }
}
