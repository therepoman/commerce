using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Test.Main.Utilities;
using Amazon.Fuel.Common.Contracts.Manifest;
using Moq;
using Xunit;

namespace Amazon.Test.Main.Plugin.Persistence.Test.Unit
{
    public class LuceneMigrationTest : IDisposable
    {
        private string _baseDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

        private readonly Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        private readonly Mock<IMessengerHub> _messengerHub = new Mock<IMessengerHub>();
        private readonly Mock<IMetricsPlugin> _metrics = new Mock<IMetricsPlugin>();
        private readonly Mock<IInstalledGameValidator> _gameInstallValidator = new Mock<IInstalledGameValidator>();
        private readonly Mock<ISecureStoragePlugin> _secureStoragePlugin = new Mock<ISecureStoragePlugin>();
        private readonly IPersistence _systemPersistence;

        public LuceneMigrationTest()
        {
            _osDriver.Setup(s => s.AppDataPathFull).Returns(Path.Combine(_baseDir, "user_space"));
            _osDriver.Setup(s => s.AppDataPathFullDeprecated).Returns(Path.Combine(_baseDir, "user_space_deprecated"));
            
            _osDriver.Setup(s => s.SystemDataPathFull).Returns(Path.Combine(_baseDir, "system_space"));
            _osDriver.Setup(s => s.SystemDataPathFullDeprecated).Returns(Path.Combine(_baseDir, "system_space_deprecated"));
            
            _systemPersistence = new SystemPersistencePlugin(_osDriver.Object);
        }

        private LuceneDatabase<T> createLuceneDb<T>(bool isReadonly) where T : PersistentEntry, new()
        {
            var ret = new LuceneDatabase<T>(
                new LuceneSystemPersistencePlugin(_osDriver.Object),
                _messengerHub.Object,
                _metrics.Object);
            ret.ReadonlyMode = isReadonly;
            return ret;
        }

        private GameInstallInfoDatabase createGameInstallDb() 
            => new GameInstallInfoDatabase(
                _systemPersistence, 
                _messengerHub.Object, 
                _metrics.Object,
                _osDriver.Object);

        private LuceneDbMigrator createMigrator(GameInstallInfoDatabase tgtDb = null)
            => new LuceneDbMigrator(
                tgtGameInstallInfoDb: tgtDb ?? createGameInstallDb(),
                hub: _messengerHub.Object,
                metrics: _metrics.Object,
                osDriver: _osDriver.Object,
                secureStorage: _secureStoragePlugin.Object);

        private LuceneDatabase<GameInstallInfoLuceneDeprecated> createPopulatedLuceneDb(int entryCount, bool hasNulls)
        {
            var oldDb = createLuceneDb<GameInstallInfoLuceneDeprecated>(isReadonly: false);

            for (var i = 0; i < entryCount; ++i)
            {
                var pi = Factories.ValidGameProductInfo();
                pi.ProductIdStr = $"prodid_{i}";
                oldDb.AddOrReplaceById(Factories.ValidGameInstallInfoLuceneDeprecated(pi, nullFields: hasNulls));
            }

            return oldDb;
        }

        [Theory,
         InlineData(0),                       
         InlineData(1),      
         InlineData(10)]
        public void MigratesOldSystemData(int entryCount)
        {
            var oldDb = createPopulatedLuceneDb(entryCount, hasNulls: false);

            var expectedEntries = oldDb.All;
            oldDb.Dispose(); // close connection
            Assert.True(expectedEntries.Count == entryCount); // sanity

            var newDb = createGameInstallDb();
            var migrator = createMigrator(newDb);
            migrator.TryMigrateOldSystemDb();
            var actualEntries = newDb.All;
            Assert.True(actualEntries.Count == entryCount);
            for (int i = 0; i < entryCount; ++i)
            {
                var expected = expectedEntries[i].ToGameInstallInfo();
                Assert.True(expected.Equals(actualEntries[i]));
            }
        }

        public void MigratesOldSystemDataAllowsNulls()
        {
            var entryCount = 3;
            var oldDb = createPopulatedLuceneDb(entryCount: entryCount, hasNulls: true);

            var expectedEntries = oldDb.All;
            oldDb.All.ForEach(e =>
            {
                e.InstallDirectory = Path.GetTempPath();
                oldDb.AddOrReplaceById(e);
            });
            oldDb.Dispose(); // close connection
            Assert.True(expectedEntries.Count == entryCount); // sanity

            var newDb = createGameInstallDb();
            var migrator = createMigrator(newDb);
            migrator.TryMigrateOldSystemDb();
            var actualEntries = newDb.All;
            Assert.True(actualEntries.Count == entryCount);
            for (int i = 0; i < entryCount; ++i)
            {
                var expected = expectedEntries[i].ToGameInstallInfo();
                Assert.True(expected.Equals(actualEntries[i]));
            }
        }

        public void MigratesOldSystemDataSkipsEmptyInstallDirs()
        {
            var entryCount = 3;
            var oldDb = createPopulatedLuceneDb(entryCount: entryCount, hasNulls: true);

            var expectedEntries = oldDb.All;
            oldDb.All[1].InstallDirectory = Path.GetTempPath();
            oldDb.AddOrReplaceById(oldDb.All[1]);
            oldDb.Dispose(); // close connection
            Assert.True(expectedEntries.Count == entryCount); // sanity

            var newDb = createGameInstallDb();
            var migrator = createMigrator(newDb);
            migrator.TryMigrateOldSystemDb();
            var actualEntries = newDb.All;
            Assert.True(actualEntries.Count == entryCount);
            for (int i = 0; i < entryCount; ++i)
            {
                var expected = expectedEntries[i].ToGameInstallInfo();
                Assert.True(expected.Equals(actualEntries[i]));
            }
        }

        [Fact]
        public void RemovesOldSystemData()
        {
            var persist = new LuceneSystemPersistencePlugin(_osDriver.Object);
            if (!Directory.Exists(persist.DataRoot()))
            {
                Directory.CreateDirectory(persist.DataRoot());
            }

            var migrator = createMigrator();
            migrator.TryRemoveOldSystemData();
            _osDriver.Verify(d => d.TryDeleteProtected(persist.DataRoot(), true));
        }

        [Fact]
        public void RemovesOldUserData()
        {
            var persist = new LuceneUserPersistencePlugin(_osDriver.Object);
            if (!Directory.Exists(persist.DataRoot()))
            {
                Directory.CreateDirectory(persist.DataRoot());
            }

            var migrator = createMigrator();
            migrator.TryRemoveOldUserData();
            _osDriver.Verify(d => d.TryDeleteProtected(persist.DataRoot(), true));
        }

        [Fact]
        public void SavesGameInstallInfos()
        {
            var oldDb = createPopulatedLuceneDb(entryCount: 1, hasNulls: false);
            var newDb = createGameInstallDb();
            var migrator = createMigrator(newDb);
            migrator.TryMigrateOldSystemDb();
        }

        public void Dispose()
        {
            try
            {
                Directory.Delete(_baseDir, true);
            }
            catch { }
        }

    }
}