using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Amazon.MCP.Common.Contracts;
using Amazon.MCP.Plugin.Persistence;
using Amazon.MCP.Test.Main.Common.Test.Unit;
using MCP.Common.Contracts.Manifest;
using Moq;
using Xunit;

namespace Amazon.MCP.Test.Main.Plugin.Persistence.Test.Unit
{
    public class ManifestFileSystemStorageTest : IDisposable
    {
        private string _baseDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        private Mock<IPersistence> _systemPersistence = new Mock<IPersistence>();
        private Mock<IOSDriver> _osDriver = new Mock<IOSDriver>();
        private ManifestFileSystemStorage _manifestStorage;

        public ManifestFileSystemStorageTest()
        {
            _osDriver.Setup(d => d.GameDataPath).Returns(Path.Combine(_baseDir, "gamedata"));
            _systemPersistence.Setup(p => p.DataRoot()).Returns(Path.Combine(_baseDir, "system"));
            _manifestStorage = new ManifestFileSystemStorage(_osDriver.Object);
        }

        [Fact]
        public void ReturnsNullOnNoManifest()
        {
            var retrieveManifest = _manifestStorage.TryGet("no_manifest");
            Assert.Null(retrieveManifest);
        }

        [Fact]
        public void CanStoreAndRetrieveManifest()
        {
            var testManifest = ManifestTest.BuildManifest();
            string testId = $"{Guid.NewGuid()}";
            _manifestStorage.Put(id: testId, entity: testManifest);

            var retrieveManifest = _manifestStorage.TryGet(id: testId);
            Assert.True(retrieveManifest != null);
            Assert.True(retrieveManifest.Equals(testManifest));
        }

        [Fact]
        public void RemovesManifest()
        {
            var testManifest = ManifestTest.BuildManifest();
            string testId = $"{Guid.NewGuid()}";
            string path = Path.Combine(_baseDir, testId);
            _manifestStorage.Put(testId, testManifest);

            _manifestStorage.Delete(testId);
            Assert.False(File.Exists(Path.Combine(_baseDir, testId)));
        }

        public void Dispose()
        {
            try
            {
                Directory.Delete(_baseDir, true);
            }
            catch { }
        }
    }
}