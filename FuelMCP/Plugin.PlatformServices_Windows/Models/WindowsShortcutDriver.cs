﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using Amazon.Fuel.Common.Utilities;
using Serilog;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows.Models
{
    public class WindowsShortcutDriver : IShortcutDriver
    {
        private readonly IOSDriver _osDriver;

        private const string TWITCH_FOLDER_NAME = "Twitch Games";
        private const char INVALID_FS_CHAR_REPLACEMENT = ' ';   // replace invalid FIleSystem characters with this one

        private const string SHORTCUT_EXT = "lnk";
        private const string SHORTCUT_URL_EXT = "url";

        public WindowsShortcutDriver(IOSDriver osDriver)
        {
            _osDriver = osDriver;
        }

        private static string GetSafeShortcutFileName(string shortcutPath, string shortcutBaseName, string extension)
        {
            var safeBaseName = shortcutBaseName;
            foreach (var c in Path.GetInvalidFileNameChars())
            {
                safeBaseName = safeBaseName.Replace(c, INVALID_FS_CHAR_REPLACEMENT);
            }

            if (safeBaseName != shortcutBaseName)
            {
                Log.Information($"Shortcut renamed from '{shortcutBaseName}' to '{safeBaseName}' (replace invalid characters)");
            }

            return Path.Combine(shortcutPath, $"{safeBaseName}.{extension}");
        }

        private static string BasePath(IOSDriver osDriver, ShortcutLocation location)
        {
            switch (location)
            {
                case ShortcutLocation.ShortcutFolder:   return Path.Combine(osDriver.UserStartMenuPath);
                case ShortcutLocation.Desktop:          return osDriver.UserDesktopPath;
                case ShortcutLocation.Temp:             return System.IO.Path.GetTempPath();
                case ShortcutLocation.Twitch:           return Path.Combine(BasePath(osDriver, ShortcutLocation.ShortcutFolder), TWITCH_FOLDER_NAME);
                default:
                    Debug.Fail("Unhandled shortcutlocation {location}");
                    goto case ShortcutLocation.Twitch;
            }
        }

        public string TryCreateUrl(
            ShortcutLocation location,
            string shortcutBaseName,
            string targetUrl,
            string description,
            string iconLocation = null,
            int iconOffset = 0)
        {
            // this bit of threading fun is necessary, due to the IUniformresourceLocatorW interface being STA
            string output = "";
            Thread thread = new Thread(() => output = WindowsShortcutDriver.TryCreateUrlSTA(
                _osDriver,
                location,
                shortcutBaseName,
                targetUrl,
                description,
                iconLocation,
                iconOffset));

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            while (thread.IsAlive)
            {
                Thread.Sleep(50);
            }

            return output;
        }

        public string GetShortcutFileName(ShortcutLocation location, string shortcutBaseName)
        {
            var baseDir = WindowsShortcutDriver.BasePath(_osDriver, location);
            return GetSafeShortcutFileName(baseDir, shortcutBaseName, SHORTCUT_URL_EXT);
        } 

        public static string TryCreateUrlSTA(
            IOSDriver osDriver,
            ShortcutLocation location,
            string shortcutBaseName,
            string targetUrl,
            string description,
            string iconLocation = null,
            int iconOffset = 0)
        {
            try
            {
                Log.Information($"WindowsShortcutDriver.CreateUrl({location}, {shortcutBaseName}, {targetUrl}, {description}, {iconLocation}, {iconOffset})");
                var baseDir = WindowsShortcutDriver.BasePath(osDriver, location);
                Directory.CreateDirectory(baseDir);

                var fullPath = GetSafeShortcutFileName(baseDir, shortcutBaseName, SHORTCUT_URL_EXT);
                File.Delete(fullPath);

                InternetShortcut shortcut = new InternetShortcut();

                int hr = ((IUniformResourceLocator)shortcut).SetURL(targetUrl, 0);
                if (hr != 0)
                {
                    Log.Error($"URL generation failed (errcode:{hr})");
                }

                if (!string.IsNullOrEmpty(iconLocation))
                {
                    ((IShellLink)shortcut).SetIconLocation(iconLocation, iconOffset);
                }

                if (!string.IsNullOrEmpty(description))
                {
                    ((IShellLink)shortcut).SetDescription(description);
                }

                ((IPersistFile)shortcut).Save(fullPath, true);

                return fullPath;
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Unable to create shortcut");
                return null;
            }
        }

        public string CreateShortcut(
            ShortcutLocation location, 
            string shortcutBaseName, 
            string targetFile, 
            string description,
            string arguments = null, 
            string iconLocation = null, 
            int iconOffset = 0)
        {
            Log.Information($"WindowsShortcutDriver.GetSafeShortcutFileName({location}, {shortcutBaseName}, {targetFile}, {description}, SKIP, {iconLocation}, {iconOffset})");
            var baseDir = BasePath(_osDriver, location);
            Directory.CreateDirectory(baseDir);

            var fullPath = GetSafeShortcutFileName(baseDir, shortcutBaseName, SHORTCUT_EXT);
            IShellLink link = (IShellLink)new ShellLink();

            link.SetDescription(description);
            link.SetPath(targetFile);
            link.SetWorkingDirectory(Path.GetDirectoryName(targetFile));

            if (iconLocation != null)
            {
                link.SetIconLocation(iconLocation, iconOffset);
            }

            if (arguments != null)
            {
                link.SetArguments(arguments);
            }

            File.Delete(fullPath);

            IPersistFile file = (IPersistFile)link;
            file.Save(fullPath, false);
            return fullPath;
        }

        public void RemoveShortcut(string shortcutPath)
        {
            Log.Information($"WindowsShortcutDriver.RemoveShortcut({shortcutPath})");
            if (File.Exists(shortcutPath))
            {
                File.Delete(shortcutPath);
            }
        }

        #region URL_FILE_INTEROP

        /// <summary>The IShellLink interface allows Shell links to be created, modified, and resolved</summary>
        [ComImport(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("000214F9-0000-0000-C000-000000000046")]
        interface IShellLinkW
        {
            /// <summary>Retrieves the path and file name of a Shell link object</summary>
            void GetPath([Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out int/*UnsafeNativeMethods.WIN32_FIND_DATAW*/ pfd, int/*UnsafeNativeMethods.SLGP_FLAGS*/ fFlags);
            /// <summary>Retrieves the list of item identifiers for a Shell link object</summary>
            void GetIDList(out IntPtr ppidl);
            /// <summary>Sets the pointer to an item identifier list (PIDL) for a Shell link object.</summary>
            void SetIDList(IntPtr pidl);
            /// <summary>Retrieves the description string for a Shell link object</summary>
            void GetDescription([Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
            /// <summary>Sets the description for a Shell link object. The description can be any application-defined string</summary>
            void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
            /// <summary>Retrieves the name of the working directory for a Shell link object</summary>
            void GetWorkingDirectory([Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
            /// <summary>Sets the name of the working directory for a Shell link object</summary>
            void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
            /// <summary>Retrieves the command-line arguments associated with a Shell link object</summary>
            void GetArguments([Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
            /// <summary>Sets the command-line arguments for a Shell link object</summary>
            void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
            /// <summary>Retrieves the hot key for a Shell link object</summary>
            void GetHotkey(out short pwHotkey);
            /// <summary>Sets a hot key for a Shell link object</summary>
            void SetHotkey(short wHotkey);
            /// <summary>Retrieves the show command for a Shell link object</summary>
            void GetShowCmd(out int piShowCmd);
            /// <summary>Sets the show command for a Shell link object. The show command sets the initial show state of the window.</summary>
            void SetShowCmd(int iShowCmd);
            /// <summary>Retrieves the location (path and index) of the icon for a Shell link object</summary>
            void GetIconLocation([Out(), MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath,
                int cchIconPath, out int piIcon);
            /// <summary>Sets the location (path and index) of the icon for a Shell link object</summary>
            void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
            /// <summary>Sets the relative path to the Shell link object</summary>
            void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
            /// <summary>Attempts to find the target of a Shell link, even if it has been moved or renamed</summary>
            void Resolve(IntPtr hwnd, int/*UnsafeNativeMethods.SLR_FLAGS*/ fFlags);
            /// <summary>Sets the path and file name of a Shell link object</summary>
            void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
        }



        //-----------------------------------------------------------------------------   
        [StructLayout(LayoutKind.Sequential)]
        public class URLINVOKECOMMANDINFO
        {
            public URLINVOKECOMMANDINFO() { }
            public uint dwcbSize;
            public uint dwFlags;
            public IntPtr hwndParent;
            public string pcszVerb;
        }

        public enum URL_SETURL : uint
        {
            IURL_SETURL_FL_GUESS_PROTOCOL = 0x0001, // Guess protocol if missing
            IURL_SETURL_FL_USE_DEFAULT_PROTOCOL = 0x0002, // Use default protocol if missing
        }

        [ComImport(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("CABB0DA0-DA57-11CF-9974-0020AFD79762")]
        public interface IUniformResourceLocator
        {
            [PreserveSig]
            int SetURL([MarshalAs(UnmanagedType.LPWStr)] string pcszURL, uint dwInFlags);

            [PreserveSig]
            int GetURL(out IntPtr ppszURL);

            void InvokeCommand(ref URLINVOKECOMMANDINFO pURLCommandInfo);
        }

        [ComImport, Guid("0000010B-0000-0000-C000-000000000046"),
        InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IPersistFile
        {
            void GetClassID(out Guid pClassID);
            [PreserveSig]
            int IsDirty();
            [PreserveSig]
            int Load([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, int dwMode);
            void Save([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, [MarshalAs(UnmanagedType.Bool)] bool fRemember);
            void SaveCompleted([MarshalAs(UnmanagedType.LPWStr)] string pszFileName);
            void GetCurFile(out IntPtr ppszFileName);
        }

        [ComImport, Guid("FBF23B40-E3F0-101B-8488-00AA003E56F8")]
        class InternetShortcut { }
        #endregion // URL_FILE_INTEROP

        #region LNK_FILE_INTEROP
        [ComImport]
        [Guid("00021401-0000-0000-C000-000000000046")]
        internal class ShellLink
        {
        }


        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("000214F9-0000-0000-C000-000000000046")]
        internal interface IShellLink
        {
            void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out IntPtr pfd, int fFlags);
            void GetIDList(out IntPtr ppidl);
            void SetIDList(IntPtr pidl);
            void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
            void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
            void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
            void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
            void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
            void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
            void GetHotkey(out short pwHotkey);
            void SetHotkey(short wHotkey);
            void GetShowCmd(out int piShowCmd);
            void SetShowCmd(int iShowCmd);
            void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath, int cchIconPath, out int piIcon);
            void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
            void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
            void Resolve(IntPtr hwnd, int fFlags);
            void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
        }
        #endregion // LNK_FILE_INTEROP
    }
}
