﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Amazon.Fuel.Common.Contracts;
using Serilog;
using System.Security.AccessControl;
using Amazon.Fuel.Common;
using System.Reflection;
using System.Security.Principal;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows.Models
{
    public class WindowsFileSystemDriver : IOSDriver
    {
        public bool IsDevMode { get; set; } = false;

        public string AppDataPathParent => IsDevMode ? Constants.Paths.AppDataPathParentDevMode : Constants.Paths.AppDataPathParentRelease;

        public string ApplicationDataOverride { get; set; }

        public virtual string EnvironmentApplicationData
            => ApplicationDataOverride ?? Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public virtual string EnvironmentCommonApplicationData
            => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

        public string AppDataPathFull
            => Path.Combine(EnvironmentApplicationData, AppDataPathParent, Constants.Paths.AppDataPathFuel);

        public string CurseLogDirectory
            => Path.Combine(EnvironmentApplicationData, AppDataPathParent, Constants.Paths.TwitchAppLogDir);

        [Obsolete("Lucene will be removed")]
        public string AppDataPathFullDeprecated
            => Path.Combine(EnvironmentApplicationData, AppDataPathParent, Constants.Paths.AppDataPathFuelDeprecated);

        [Obsolete("Lucene will be removed")]
        public string SystemDataPathFullDeprecated
            => Path.Combine(EnvironmentCommonApplicationData, AppDataPathParent, Constants.Paths.AppDataPathFuelDeprecated);

        public string SystemDataPathFull
            => Path.Combine(EnvironmentCommonApplicationData, 
                            AppDataPathParent, 
                            Constants.Paths.AppDataPathFuel);

        public string GameDataPath
            => Path.Combine(SystemDataPathFull, Constants.Paths.GameDataSubdir);

        public string PrimaryLibraryRoot
        {
            get
            {
                var progFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                return Path.Combine(progFiles, Constants.Paths.PrimaryLibraryPathSubdir);
            }
        }

        public string GetSecondaryLibraryRoot(string pathRoot) => Path.Combine(pathRoot, Constants.Paths.SecondaryLibraryPathSubdir);

        public string[] AllLibraryRoots
        {
            get
            {
                var ret = new List<string>();
                var primary = PrimaryLibraryRoot;

                ret.Add(primary);

                var basePathRoot = Path.GetPathRoot(primary);

                try
                {
                    if (!string.IsNullOrEmpty(basePathRoot))
                    {
                        DriveInfo basePathInfo = new DriveInfo(basePathRoot);
                        foreach (var drive in DriveInfo.GetDrives())
                        {
                            if (basePathInfo.Name == drive.Name
                                || !drive.IsReady
                                || drive.DriveType == DriveType.CDRom
                                || drive.DriveType == DriveType.Network
                                || drive.DriveType == DriveType.Ram)
                            {
                                continue;
                            }

                            ret.Add(GetSecondaryLibraryRoot(drive.RootDirectory.FullName));
                        }
                    }
                } catch (Exception ex)
                {
                    FuelLog.Error(ex, "Encountered Exception while trying to read drive.");
                }

                return ret.ToArray();
            }
        }

        public string CommonUninstallerExePath
            => Path.Combine(SystemDataPathFull, Constants.Paths.TwitchGameRemoverSubdir, Constants.Paths.TwitchGameRemoverExeName);

        public void EnsureSystemDataPathExists()
        {
            try
            {
                if (!Directory.Exists(SystemDataPathFull))
                {
                    CreateDirectoryWithAllUsersPermissions(SystemDataPathFull, setPermissionsOnTopmostNewDirectory: true);
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e, $"Couldn't create or set permissions to directory at '{SystemDataPathFull}'");
                throw;
            }
        }

        public void EnsureGameDataPathExists()
        {
            EnsureSystemDataPathExists();
            try
            {
                if (!Directory.Exists(GameDataPath))
                {
                    Directory.CreateDirectory(GameDataPath);
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e, $"Couldn't create directory at '{GameDataPath}'");
                throw;
            }
        }

        public string AppBasePath
        {
            get
            {
                var executable = Assembly.GetEntryAssembly().Location;
                return Directory.GetParent(executable).FullName;
            }
        }

        public string GameInstallHelperPath => Path.Combine(AppBasePath, Constants.Paths.FuelBinarySourcePath, Constants.Paths.HelpersPath, Constants.Paths.TwitchGameInstallHelperExeName);
        public string GameRemoverSourcePath => Path.Combine(AppBasePath, Constants.Paths.FuelBinarySourcePath, Constants.Paths.HelpersPath, Constants.Paths.TwitchGameRemoverExeName);

        public virtual string UserDesktopPath => Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        public string CommonDesktopPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
        public string CommonStartMenuPath => getSpecialPath(CSIDL_COMMON_STARTMENU);
        public string UserStartMenuPath => getSpecialPath(CSIDL_USER_STARTMENU);

        public string SdkLocation => Path.Combine(AppBasePath, Constants.Paths.FuelBinarySourcePath, Constants.Paths.SdkPath);

        string getSpecialPath(int pathId)
        {
            Log.Information($"WindowsFileSystemDriver.getSpecialPath({pathId})");
            StringBuilder sb = new StringBuilder(MAX_PATH);
            SHGetSpecialFolderPath(IntPtr.Zero, sb, pathId, false);
            return Path.Combine(sb.ToString(), "Programs");
        }

        const int MAX_PATH = 260;
        private const int CSIDL_COMMON_STARTMENU = 0x16;
        private const int CSIDL_USER_STARTMENU = 0x0B;

        [DllImport("shell32.dll")]
        static extern bool SHGetSpecialFolderPath(
            IntPtr hwndOwner, 
            [Out] StringBuilder lpszPath, 
            int nFolder, 
            bool fCreate);

        public void CreateDirectoryWithAllUsersPermissions(string directory, bool setPermissionsOnTopmostNewDirectory)
        {
            var setPermissionsDir = directory;
            if (!Directory.Exists(directory))
            {
                FuelLog.Info("Creating new dir with all user permissions", new { directory, setPermissionsOnTopmostNewDirectory });

                if (!setPermissionsOnTopmostNewDirectory)
                {
                    Directory.CreateDirectory(directory);
                }
                else
                {
                    // walk up the paths until you find one that doesn't exist, create that with full permissions, then create final
                    while (true)
                    {
                        var parentDir = Directory.GetParent(setPermissionsDir)?.FullName;

                        if (string.IsNullOrEmpty(parentDir))
                        {
                            break;
                        }

                        if (Directory.Exists(parentDir))
                        {
                            break;
                        }

                        setPermissionsDir = parentDir;
                    }

                    Directory.CreateDirectory(setPermissionsDir);
                }
            }

            FuelLog.Info("Ensuring directory has all user permissions", new { directory, setPermissionsOnTopmostNewDirectory, setPermissionsDir });
            EnsureDirectoryHasAllUsersPermissions(setPermissionsDir);

            if (setPermissionsOnTopmostNewDirectory
                && !Directory.Exists(directory))
            {
                // permissions should inherit
                FuelLog.Info("Creating new subdirectory of topmost permissions directory", new { directory });
                Directory.CreateDirectory(directory);
            }

        }

        private FileSystemAccessRule CreateAllUserAccessRule(bool isDirectory)
        { 
            return new FileSystemAccessRule(
                identity: new System.Security.Principal.SecurityIdentifier(
                    System.Security.Principal.WellKnownSidType.BuiltinUsersSid, 
                    null),
                fileSystemRights: FileSystemRights.FullControl,
                inheritanceFlags: isDirectory ? InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit
                                              : InheritanceFlags.None,
                propagationFlags: PropagationFlags.None,
                type: AccessControlType.Allow);
        }

        public void EnsureDirectoryHasAllUsersPermissions(string directory)
        {
            if (!DirectoryHasAllUsersPermissions(directory))
            {
                FuelLog.Info($"Adding rule for all user access to directory", new { directory });
                var security = Directory.GetAccessControl(directory);
                security.AddAccessRule(CreateAllUserAccessRule(isDirectory: true));
                Directory.SetAccessControl(directory, security);
            }
        }

        private bool SecurityHasAllUsersPermissions(FileSystemSecurity security, bool isDirectory)
        {
            var existingRules = security.GetAccessRules(
                includeExplicit: true,
                includeInherited: true,
                targetType: typeof(System.Security.Principal.SecurityIdentifier));

            var wantRule = CreateAllUserAccessRule(isDirectory: true);
            foreach (FileSystemAccessRule existingRule in existingRules)
            {
                if (existingRule != null
                    && existingRule.IdentityReference.Value == wantRule.IdentityReference.Value
                    && existingRule.FileSystemRights == wantRule.FileSystemRights
                    && existingRule.InheritanceFlags == wantRule.InheritanceFlags
                    && existingRule.PropagationFlags == wantRule.PropagationFlags
                    && existingRule.AccessControlType == wantRule.AccessControlType)
                {
                    return true;
                }
            }

            return false;
        }

        public bool DirectoryHasAllUsersPermissions(string directory)
        {
            try
            {
                return SecurityHasAllUsersPermissions(Directory.GetAccessControl(directory), isDirectory: true);
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't determine permissions on directory", new { directory });
            }

            return false;
        }

        public bool FileHasAllUsersPermissions(string file)
        {
            try
            {
                return SecurityHasAllUsersPermissions(File.GetAccessControl(file), isDirectory: false);
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't determine permissions on file", new { file });
            }

            return false;
        }

        public void EnsureFileHasAllUsersPermissions(string file)
        {
            if (!FileHasAllUsersPermissions(file))
            {
                FuelLog.Info($"Adding rule for all user access to file", new { file });
                var security = File.GetAccessControl(file);
                security.AddAccessRule(CreateAllUserAccessRule(isDirectory: false));
                File.SetAccessControl(file, security);
            }
        }

        public bool TryDeleteProtected(string path, bool recurse)
        {
            try
            {
                // add some paranoia safeties here ...
                if (Directory.GetParent(path) == null)
                {
                    Log.Error($"TryRecursiveDeleteProtected(): Trying to delete root! '{path}'");
                    return false;
                }

                var checkDir = path.TrimEnd(new char[] {' ', Path.PathSeparator});

                foreach (Environment.SpecialFolder folderType in Enum.GetValues(typeof(Environment.SpecialFolder)))
                {
                    var protectPath = Environment.GetFolderPath(folderType);
                    if (string.IsNullOrEmpty(protectPath))
                    {
                        continue;
                    }

                    if (checkDir.EndsWith(protectPath))
                    {
                        Log.Error(
                            $"TryRecursiveDeleteProtected(): Trying to delete special folder '{folderType}':'{path}', aborted");
                        return false;
                    }
                }

                if ( !recurse )
                {
                    // remove local files first
                    foreach (var file in Directory.GetFiles(path) )
                    {
                        File.Delete(file);
                    }

                    if (Directory.GetDirectories(path).Length > 0)
                    {
                        FuelLog.Warn("Can't remove directory (not empty)", new { path });
                        return false;
                    }
                }

                Directory.Delete(path, recursive: recurse);

                return true;
            }
            catch (DirectoryNotFoundException)
            {
                // fine
                return true;
            } 
            catch (IOException ex)
            {
                Log.Error(ex, $"TryRecursiveDeleteProtected(): Couldn't delete directory at '{path}'");
                return false;
            }
        }

        public void WipeData()
        {
            Log.Information($"WipeData");
            try
            {
                Directory.Delete(AppDataPathFull, true);
            }
            catch (IOException e)
            {
                Log.Information(e, $"Unable to remove {AppDataPathFull}");
            }

            try
            {
                Directory.Delete(SystemDataPathFull, true);
            }
            catch (IOException e)
            {
                Log.Information(e, $"Unable to remove {SystemDataPathFull}");
            }
        }

        public bool HasPermissions(string path, FileSystemRights rights)
        {
            try
            {
                var fi = new FileInfo(path);
                var identity = WindowsIdentity.GetCurrent();
                var acls = fi.GetAccessControl().GetAccessRules(true, true, typeof(SecurityIdentifier));
                var groups = identity.Groups;

                bool ret = false;

                Func<FileSystemRights, FileSystemAccessRule, bool> isRule = (right, rule) =>
                    (((int) right & (int) rule.FileSystemRights) == (int) right);

                foreach (var acl in acls)
                {
                    var rule = (FileSystemAccessRule) acl;
                    if (identity.User.Equals(rule.IdentityReference)
                        || groups.Any(g => g.Equals(rule.IdentityReference)))
                    {
                        if (!isRule(rights, rule))
                            continue;

                        if (AccessControlType.Deny.Equals(rule.AccessControlType))
                        {
                            return false;
                        }

                        if (AccessControlType.Allow.Equals(rule.AccessControlType))
                        {
                            ret = true;
                        }
                    }
                }

                return ret;
            }
            catch (UnauthorizedAccessException)
            {
                FuelLog.Error("Unauthorized access trying to obtain permissions", new { path, rights });
                return false;
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't retrieve permissions", new {path, rights});
                return false;
            }
        }

        public string DescribePermissions(string path)
        {
            try
            {
                var perm = new List<string>();
                foreach (var e in Enum.GetNames(typeof(FileSystemRights)))
                {
                    FileSystemRights fsr;
                    if (Enum.TryParse(e, out fsr))
                    {
                        if (HasPermissions(path, fsr))
                        {
                            perm.Add(e);
                        }
                    }
                }

                return perm.Count == 0 ? "None" : string.Join(",", perm);
            }
            catch
            {
                return "[couldn't_retrieve]";
            }
        }
    }
}
