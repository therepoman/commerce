﻿using System;
using System.Globalization;
using System.Reflection;
using Amazon.Fuel.Common.Contracts;
using Microsoft.Win32;
using Serilog;
using Amazon.Fuel.Common.CommandLineParser;
using Amazon.Fuel.Common;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows.Models
{
    public class WindowsRegisterInstallation : IRegisterInstallation
    {
        public const string UninstallRegistryLocation = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";

        private readonly IOSDriver _osDriver;

        public WindowsRegisterInstallation(IOSDriver osDriver)
        {
            _osDriver = osDriver;
        }

        public bool RegisterUninstaller(
            ProductId productId,
            GameProductInfo productInfo,
            GameInstallInfo installInfo,
            long estimatedSize,
            string iconLocation,
            int iconOffset = 0)
        {
            Log.Information($"WindowsRegisterInstallation.RegisterUninstaller({productId}, {productInfo.ProductId}, {installInfo?.InstallDirectory}, {estimatedSize}, {iconLocation}, {iconOffset}");
            try
            {
                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(UninstallRegistryLocation, true))
                {
                    if (parent == null)
                    {
                        return false;
                    }

                    RegistryKey key = null;

                    try
                    {
                        string guidText = "{" + productId.Id.ToUpper() + "}";
                        key = parent.OpenSubKey(guidText, true) ?? parent.CreateSubKey(guidText);

                        if (key == null)
                        {
                            return false;
                        }

                        Assembly asm = Assembly.GetEntryAssembly();
                        Version v = asm.GetName().Version;

                        string exe = $"\"{_osDriver.CommonUninstallerExePath.Replace("\\", "\\\\")}\""; 
                        string displayName = productInfo.ProductTitle ?? "Unknown";
                        string appVersion = installInfo.InstallVersion ?? "v.Unknown";
                        string displayVersion = installInfo.InstallVersionName ?? string.Empty;
                        string publisher = productInfo.ProductPublisher ?? "Unknown Publisher";
                        var dt = installInfo.InstallDateAsDateTime == DateTime.MinValue ? DateTime.UtcNow : installInfo.InstallDateAsDateTime;
                        string installDate = dt.ToLocalTime().ToString(CultureInfo.CurrentCulture);
                        string installLocation = installInfo.InstallDirectory;
                        AppDataRemoverArgs args = new AppDataRemoverArgs
                        {
                            Mode = AppDataRemoverArgs.Modes.Game,
                            ProductId = productId.Id,
                            DevMode = _osDriver.IsDevMode
                        };
                        // TODO: FLCL-172 -- Set "DisplayIcon" value in registry key when icons are available

                        key.SetValue("DisplayName", displayName);
                        key.SetValue("ApplicationVersion", appVersion);
                        key.SetValue("DisplayVersion", displayVersion);
                        key.SetValue("Publisher", publisher);
                        key.SetValue("InstallDate", installDate);
                        key.SetValue("InstallLocation", installLocation);
                        key.SetValue("UninstallString", $"{exe} {args.ArgsString}");
                        key.SetValue("EstimatedSize", estimatedSize / 1024, RegistryValueKind.DWord);
                        key.SetValue("DisplayIcon", iconLocation);

                        // $$ jbilas -- support versioning?
                        
                        return true;
                    }
                    finally
                    {
                        key?.Close();
                    }
                }
            }
            catch ( Exception ex )
            {
                //This can happen if we are not elevated. 
                //In that case, the game won't get installed to add/remove programs.
                Log.Error("Could not register game: " + ex.Message);                
            }

            return false;
        }

        public bool UnregisterUninstaller(ProductId productId)
        {
            Log.Information($"WindowsRegisterInstallation.UnregisterUninstaller({productId}");
            try
            {
                using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(UninstallRegistryLocation, true))
                {
                    if (parent == null)
                    {
                        return false;
                    }

                    RegistryKey key = null;

                    try
                    {
                        string guidText = "{" + productId.Id.ToUpper() + "}";
                        key = parent.OpenSubKey(guidText, true);

                        if (key == null)
                        {
                            // try old format
                            guidText = productId.Id;
                            key = parent.OpenSubKey(guidText, true);

                            if (key == null)
                            {
                                return false;
                            }
                        }

                        parent.DeleteSubKey(guidText);
                        return true;
                    }
                    finally
                    {
                        key?.Close();
                    }
                }                
            }
            catch (Exception ex)
            {
                //This can happen if we are not elevated. 
                //In that case, the game won't get installed to add/remove programs.
                Log.Warning("Could not unregister game: " + ex.Message);
            }
            return false;
        }
    }
}
