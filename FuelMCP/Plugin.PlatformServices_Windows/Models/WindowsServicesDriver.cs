﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using System.Security.Principal;
using Serilog;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows.Models
{
    public class WindowsServicesDriver : IOSServicesDriver
    {
        public void PreventSleep()
        {
            Log.Information($"WindowsServicesDriver.PreventSleep()");
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED | EXECUTION_STATE.ES_AWAYMODE_REQUIRED | EXECUTION_STATE.ES_CONTINUOUS);
        }

        public void AllowSleep()
        {
            Log.Information($"WindowsServicesDriver.AllowSleep()");
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        public SecurityIdentifier UseridForUsername(string username)
        {
            Log.Information($"WindowsServicesDriver.UseridForUsername()");
            var account = new NTAccount(username);
            return (SecurityIdentifier) account.Translate(typeof(SecurityIdentifier));
        }

        public string CurrentUser {
            get { return WindowsIdentity.GetCurrent().Name; }
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
            // Legacy flag, should not be used.
            // ES_USER_PRESENT = 0x00000004
        }
    }
}
