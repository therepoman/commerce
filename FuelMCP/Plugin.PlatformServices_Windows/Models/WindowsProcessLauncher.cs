﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Logging.Events;
using Amazon.Fuel.Common.Utilities;
using System.Linq;
using System.Collections;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows.Models
{
    public interface IDeelevatedLaunchService
    {
        LaunchResult LaunchUnelevated(string launchFolder, string pathToFile, bool waitForExit, Dictionary<string, string> envVars, string argsStr);
    }

    public class LaunchUnelevatedServiceNoop : IDeelevatedLaunchService
    {
        public LaunchResult LaunchUnelevated(
            string launchFolder, 
            string pathToFile, 
            bool waitForExit,
            Dictionary<string, string> envVars, 
            string argsStr)
        {
            Debug.Fail("Not supported");
            return new LaunchResult() {Status = LaunchResult.EStatus.DeEscalateFailed};
        }
    }

    public class WindowsProcessLauncher : IProcessLauncher
    {
        private readonly IDeelevatedLaunchService _deelevatedLaunchService;
        private readonly IMetricsPlugin _metrics;
        private readonly IOSDriver _osDriver;

        public WindowsProcessLauncher(
            IDeelevatedLaunchService deelevatedLaunchService, 
            IMetricsPlugin metrics,
            IOSDriver osDriver)
        {
            _deelevatedLaunchService = deelevatedLaunchService;
            _metrics = metrics;
            _osDriver = osDriver;
        }

        private const string ENV_FUEL_DIR = "FUEL_DIR";
        private const string ENV_FUEL_DEV_MODE = "FUEL_DEV_MODE";
        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);
        
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        private bool isElevated()
        {
            Log.Information($"WindowsProcessLauncher.isElevated()");
            // Is UAS Diabled? Then they're not elevated:
            var uacKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System");
            if (uacKey == null)
            {
                Log.Information($"[WPL]UAC Registry Key (System) Doeesn't Exist, assuming not elevated: {Win32PInvoke.LastError()}");
                return false;
            }

            var value = uacKey.GetValue("EnableLUA");
            if (value == null)
            {
                Log.Information($"[WPL]UAC Registry value Doeesn't Exist, assuming not elevated: {Win32PInvoke.LastError()}");
                return false;
            }

            if (!(value is int))
            {
                Log.Information($"[WPL]UAC Registry value is not an int, assuming not elevated: {Win32PInvoke.LastError()}");
                return false;
            }

            if ((int)value == 0)
            {
                Log.Information($"[WPL]UAC Registry Key == 0, assuming not elevated: {Win32PInvoke.LastError()}");
                return false;
            }

            IntPtr hToken = IntPtr.Zero;
            IntPtr hProcess = Win32PInvoke.GetCurrentProcess();
            if (hProcess == IntPtr.Zero)
            {
                Log.Information($"[WPL]Error getting current process, assuming not elevated: {Win32PInvoke.LastError()}");
                return false;
            }

            if (!Win32PInvoke.OpenProcessToken(hProcess, Win32PInvoke.TOKEN_QUERY, out hToken))
            {
                Log.Information($"[WPL]Error opening process information for current process, assuming not admin: {Win32PInvoke.LastError()}");
                return false;
            }

            try
            {
                Win32PInvoke.TOKEN_ELEVATION te;
                te.TokenIsElevated = 0;

                Int32 size = Marshal.SizeOf(te);
                IntPtr tePtr = Marshal.AllocHGlobal(size);

                try
                {
                    Marshal.StructureToPtr(te, tePtr, true);
                    UInt32 returnedLength = 0;

                    if (!Win32PInvoke.GetTokenInformation(hToken, Win32PInvoke.TOKEN_INFORMATION_CLASS.TokenElevation, tePtr, (UInt32) size, out returnedLength))
                    {
                        Log.Information($"[WPL]Unable to get Token Information, assuming not admin: {Win32PInvoke.LastError()}");
                        return false;
                    }

                    if (size != returnedLength)
                    {
                        Log.Information($"[WPL]Size mismatch on return, assuming not admin: {Win32PInvoke.LastError()}");
                        return false;
                    }

                    te = (Win32PInvoke.TOKEN_ELEVATION)Marshal.PtrToStructure(tePtr, typeof(Win32PInvoke.TOKEN_ELEVATION));
                }
                finally
                {
                    Marshal.FreeHGlobal(tePtr);
                }
                return te.TokenIsElevated != 0;
            }
            finally
            {
                Win32PInvoke.CloseHandle(hToken);
            }
        }

        private Process[] getProcessesByName(string pathToFile)
        {
            var imageName = System.IO.Path.GetFileNameWithoutExtension(pathToFile);
            return Process.GetProcessesByName(imageName);
        }

        public bool TryKillSuspendedProcesses(string pathToFile)
        {
            var procs = getProcessesByName(pathToFile);
            if (procs == null)
            {
                return false;
            }

            bool ret = false;
            foreach (var proc in procs)
            {
                // kill if no threads
                bool killProc = true;

                foreach (ProcessThread thread in proc.Threads)
                {
                    if (thread.ThreadState != System.Diagnostics.ThreadState.Standby)
                    {
                        killProc = false;
                    }
                }

                if (killProc)
                {
                    proc.Kill();
                    ret = true;
                }
            }

            return ret;
        }

        public bool IsRunning(string pathToFile)
        {
            return getProcessesByName(pathToFile).Length > 0;
        }


        public bool IsRunning(string pathToFile, out Process outProcess )
        {
            var procs = getProcessesByName(pathToFile);
            if ( procs.Length == 0 )
            {
                outProcess = null;
                return false;
            }

            outProcess = procs.First();
            procs.Skip(1).ToList().ForEach(p => p.Dispose());
            return true;
        }

        public async Task WaitOnASync(string pathToFile, int checkIntervalMs = 200)
        {
            while ( IsRunning(pathToFile) )
            {
                await Task.Delay(checkIntervalMs);
            }
        }

        private Process[] findOtherProcessesByName(string name)
        {
            var processList = Process.GetProcessesByName(name);
            if (processList == null)
            {
                Log.Information("[WPL] GetProcessesByName Result was Null");
                return new Process[] {};
            }
            if (processList.Length <= 0)
            {
                Log.Information($"[WPL] GetProcessByName returned no results for {name}");
                return new Process[] {};
            }

            Log.Information($"[WPL] Found {processList.Length} matching processes for {name}");

            var currentId = Process.GetCurrentProcess().Id;
            return processList.Where(p => p.Id != currentId).ToArray();
        }

        public bool FocusOrQuitIfRunning(string processName)
        {
            var processes = findOtherProcessesByName(processName);
            if (processes.Length == 0)
            {
                Log.Information($"[WPL] FocusOrQuitIfRunning: No other processes for name {processName}");
                return false;
            }
            return Focus(processes);
        }

        private bool Focus(Process[] list)
        {
            foreach (var process in list)
            {
                IntPtr handle = IntPtr.Zero;
                try
                {
                    handle = process.MainWindowHandle;
                }
                catch (Exception) { }   

                if (handle == IntPtr.Zero)
                {
                    Log.Information($"[WPL] Process {process.Id} image {process} has no main window handle");
                    continue;
                }

                Log.Information($"[WPL] Focusing Process {process?.Id}: {process?.ProcessName}");
                
                try
                {
                    // To tell a UAC window to restore you have to send a message, you can't use the ShowWindow method
                    const int WM_SYSCOMMAND = 0x112;
                    const int SC_RESTORE = 0xF120;
                    SendMessage(handle, WM_SYSCOMMAND, new IntPtr(SC_RESTORE), IntPtr.Zero);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Unable to send restore window message");
                }
                try
                {
                    SetForegroundWindow(handle);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Unable to set foreground window");
                }
                return true;
            }
            return false;
        }


        public bool Focus(string pathToFile)
        {

            Log.Information($"WindowsProcessLauncher.Focus({pathToFile})");
            var imageName = System.IO.Path.GetFileNameWithoutExtension(pathToFile);

            var processes = findOtherProcessesByName(imageName);

            return Focus(processes);
        }

        public LaunchResult LaunchGame(
            string sdkLocation, 
            string rootFolder, 
            LaunchConfig cfg, 
            string[] customArgs = null,
            bool productArgsIgnoreAppCfg = false)
        {
            Log.Information($"WindowsProcessLauncher.LaunchGame({sdkLocation}, {rootFolder}, {cfg?.ToJson()}, {customArgs})");
            if (cfg.Main == null)
            {
                return null;
            }

            var fileToRun = System.IO.Path.Combine(rootFolder, cfg.Main.Command);
            var envVars = new Dictionary<string, string> {{ENV_FUEL_DIR, sdkLocation}};
            var launchFolder = rootFolder;

            if (_osDriver.IsDevMode)
            {
                envVars.Add(ENV_FUEL_DEV_MODE, _osDriver.IsDevMode.ToString());
            }

            var args = new string[] { };

            if (!productArgsIgnoreAppCfg)
            {
                Array.Resize(ref args, cfg.Main.Args.Length);
                cfg.Main.Args.CopyTo(args, 0);
            }

            if ( ( customArgs?.Length ?? 0 ) > 0 )
            {
                // append
                Array.Resize(ref args, args.Length + customArgs.Length);
                customArgs.CopyTo(args, args.Length - customArgs.Length);
            }

            if (!string.IsNullOrEmpty(cfg.Main.WorkingSubdirOverride))
            {
                FuelLog.Info("Overriding working path for launch", new { WorkingPathOverride = cfg.Main.WorkingSubdirOverride });
                launchFolder = Path.Combine(launchFolder, cfg.Main.WorkingSubdirOverride);
            }

            var result = LaunchProcessOrFocus("LaunchGame", launchFolder, fileToRun, args, envVars);

            if (result.Status != LaunchResult.EStatus.Success)
            {
                _metrics.AddCounter(MetricStrings.SOURCE_LaunchResultDetail, result.Status.ToString());
            }

            var postRunLink = cfg.Main.PostRunLink;

            if (result.Status == LaunchResult.EStatus.ThrewException)
            {
                Log.Error(result.Exception, $"[{cfg.Main.Command}] Exception thrown when attempting to launch the game");
                return result;
            }
            else if (result.Status != LaunchResult.EStatus.Success)
            {
                Log.Error($"[{cfg.Main.Command}] Couldn't Launch, Status: {result.Status}");
                return result;
            }

            try
            {
                //If configured, run any links after the process closes
                if (string.IsNullOrEmpty(postRunLink) == false)
                {
                    result.Process.Exited += (sender, e) =>
                    {
                        Uri uriResult;
                        bool isValidUrl = Uri.TryCreate(postRunLink, UriKind.Absolute, out uriResult)
                            && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

                        if (isValidUrl)
                        {
                            LaunchLink(postRunLink);
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status = LaunchResult.EStatus.LaunchFailure;
                Log.Error($"[{cfg.Main.Command}] {ex.Message}");
            }

            return result;
        }

        public LaunchResult LaunchProcessOrFocus(
            string description, 
            string  launchFolder, 
            string  pathToFile,
            string[] args, 
            Dictionary<string, string> envVars = null,
            ELaunchUAC eLaunchUac = ELaunchUAC.FORCE_REMOVE_UAC,
            bool bringToForeground = true,
            bool hideWindow = false)
        {
            LaunchResult result;
            Log.Information($"WindowsProcessLauncher.LaunchProcessOrFocus({description}, {launchFolder}, {pathToFile}, eLaunchUac:{eLaunchUac}, bringToForeground:{bringToForeground})");
            Process runningProcess;
            
            if (IsRunning(pathToFile, out runningProcess))
            {
                Focus(pathToFile);
                result = new LaunchResult
                {
                    Status = LaunchResult.EStatus.AlreadyRunning,
                    Process = runningProcess
                };

                if (bringToForeground)
                {
                    Focus(new Process[] { result.Process });
                }
            }
            else
            {
                result = LaunchProcess(description, launchFolder, pathToFile, args, envVars: envVars, eLaunchUac: eLaunchUac, hideWindow: hideWindow);
            }

            return result;
        }

        public async Task<bool> TryWaitOnProcessMainWndAndFocusAsync(Process process, float maxWaitSec = 0.0f, int checkIntervalMs = 50)
        {
            var timer = new Stopwatch();
            timer.Start();
            while (( maxWaitSec <= 0.0f || timer.Elapsed.TotalSeconds < maxWaitSec )
                    && !process.HasExited)
            {
                if (process.MainWindowHandle != IntPtr.Zero)
                {
                    SetForegroundWindow(process.MainWindowHandle);
                    return true;
                }

                await Task.Delay(checkIntervalMs);
            }

            return false;
        }

        private static class Win32ErrCodes
        {
            public const int ERROR_EXE_MACHINE_TYPE_MISMATCH = 0x000000D8;
        }
        
        public LaunchResult LaunchProcess(
            string description, 
            string launchFolder, 
            string pathToFile, 
            string[] args, 
            bool waitForExit = false, 
            int[] validateAgainstReturnCodes = null,
            Dictionary<string, string> envVars = null,
            ELaunchUAC eLaunchUac = ELaunchUAC.FORCE_REMOVE_UAC,
            bool bringToForeground = true,
            bool hideWindow = false)
        {
            LaunchResult result;
            Log.Information($"WindowsProcessLauncher.LaunchProcess({description}, {launchFolder}, {pathToFile}, SKIP, {waitForExit}, {validateAgainstReturnCodes}, SKIP, {eLaunchUac}, {hideWindow})");
            if (args == null)
            {
                args = new string[] { };
            }
            if (!System.IO.File.Exists(pathToFile))
            {
                result = new LaunchResult();
                result.Status = LaunchResult.EStatus.FileNotFound;
                Log.Error(string.Format("[{0}] {1} not found", description, pathToFile));
                return result;
            }
            
            var argsStr = CLIUtilities.ArgsToCmdline(args);
            if (!isElevated())
            {
                result = LaunchAsSameUser(launchFolder, pathToFile, waitForExit, envVars, argsStr, hideWindow: hideWindow);
            }
            else if (eLaunchUac == ELaunchUAC.FORCE_REMOVE_UAC)
            {
                Log.Information($"Launching de-elevated");
                if (hideWindow)
                {
                    Log.Warning("Launch hidden not supported in this mode");
                }

                result = _deelevatedLaunchService.LaunchUnelevated(launchFolder, pathToFile, waitForExit, envVars, argsStr);
                if (result.Status != LaunchResult.EStatus.Success)
                {
                    Log.Warning($"De-elevated launch failure, LastError: {Win32PInvoke.LastError()}");
                }

                if (result.DelevationFailure)
                {
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_LaunchDeelevationFailure);
                }
            }
            else
            {
                result = LaunchAsSameUser(launchFolder, pathToFile, waitForExit, envVars, argsStr, hideWindow: hideWindow);
            }

            if (result.Status != LaunchResult.EStatus.Success)
            {
                bool exited = false;
                int id = -1;
                try
                {
                    // API can throw exceptions, being a bit paranoid here
                    exited = result.Process?.HasExited ?? false;
                    id = result.Process?.Id ?? -1;
                }
                catch (Exception e)
                {
                    Log.Error(e, "Couldn't get process exited/id");
                }

                Log.Error($"LaunchProcess result failure: status:{result.Status}, delevationFailure:{result.DelevationFailure}, exitCode:{result.ExitCode}, processExited:{exited}, processId:{id}");
                if (result.Status == LaunchResult.EStatus.ThrewException)
                {
                    Log.Error(result.Exception, "LaunchProcess exception detail");
                }
            }

            // did we try to run a 64-bit app in 32-bit OS?
            var win32ex = result.Exception as System.ComponentModel.Win32Exception;
            if (win32ex != null)
            {
                Log.Warning($"Architecture mismatch detected, 64bit:'{Environment.Is64BitOperatingSystem}', err:'{win32ex.NativeErrorCode}', msg:'{win32ex.Message}'");

                if (win32ex.NativeErrorCode == Win32ErrCodes.ERROR_EXE_MACHINE_TYPE_MISMATCH)
                {
                    result.Status = LaunchResult.EStatus.WrongArchitecture;
                }
            }

            if (result.Status == LaunchResult.EStatus.Success
                && waitForExit
                && validateAgainstReturnCodes?.Length > 0
                && !validateAgainstReturnCodes.Contains(result.ExitCode))
            {
                string retCodes = validateAgainstReturnCodes.Select(r => r.ToString() + " ").Aggregate((a, b) => a += b + " ");
                Log.Information($"Return code failed check: {result.ExitCode}, expecting one of {retCodes}");
                result.Status = LaunchResult.EStatus.ReturnCodeError;
            }

            if (bringToForeground && result?.Process != null)
            {
                Log.Information("Focusing process");
                Focus(new Process[] { result.Process});
            }

            return result;
        }

        private LaunchResult LaunchAsSameUser(
            string launchFolder, 
            string pathToFile, 
            bool waitForExit,
            Dictionary<string, string> envVars, 
            string argsStr,
            bool hideWindow = false)
        {
            Log.Information($"WindowsProcessLauncher.LaunchAsSameUser({launchFolder}, {pathToFile}, {waitForExit}, SKIP, SKIP, {hideWindow})");
            LaunchResult result = new LaunchResult();
            ProcessStartInfo start = new ProcessStartInfo(pathToFile, argsStr);

            if (hideWindow)
            {
                start.WindowStyle = ProcessWindowStyle.Hidden;
            }

            if (launchFolder != null)
            {
                start.WorkingDirectory = launchFolder;
            }

            if (envVars != null)
            {
                start.UseShellExecute = false; // must disable for setting env variable

                // NOTE: there is an issue with ProcessStartInfo.EnvironmentVariables, where two variables of different cases can
                //  potentially exist in the system environment var store and cause the index operation to throw an ArgumentException
                //  within the nested components. Interestingly, because it builds up an internal cache, only the first call will cause
                //  the thrown exception, but a second call will use the cached version and succeed (though it will be missing all elements
                //  after the duplicate element is encountered). So we try twice, to ensure that SDK vars are set (though it is in a
                //  somewhat unstable state, due to other variables, including potentially the path variable, to be missing).

                //  issue discussed here: https://github.com/dotnet/corefx/issues/13146

                //  A better solution may be to detect this issue and clean up by rebuilding based on Environment.GetEnvironmentVariables(),
                //  which is unaffected by this issue.
                for ( int attempt = 0 ; attempt < 2 ; ++attempt)
                {
                    try
                    {
                        foreach (var keyValuePair in envVars)
                        {
                            start.EnvironmentVariables[keyValuePair.Key] = keyValuePair.Value;
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        FuelLog.Error(ex, "couldn't access environment variables", new { attempt });
                    }
                }
            }

            try
            {
                if (_osDriver.IsDevMode)
                {
                    FuelLog.Info("Run process",
                        new
                        {
                            file = start.FileName,
                            args = start.Arguments,
                            shellExecute = start.UseShellExecute,
                            env = !start.UseShellExecute ? start.EnvironmentVariables : null
                        });
                }
                result.Process = Process.Start(start);
                result.Process.EnableRaisingEvents = true;

                if (waitForExit)
                {
                    result.Process.WaitForExit();
                    result.ExitCode = result.Process.ExitCode;
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Error during launch", new { file = start.FileName });

                var win32Ex = ex as System.ComponentModel.Win32Exception;

                // ERROR_CANCELLED (UAC denial, http://www.pinvoke.net/default.aspx/credui/CredUIPromptForCredentials.html)
                result.Status = isUacDenial(ex) ? LaunchResult.EStatus.UacRequestRejected : LaunchResult.EStatus.ThrewException;

                result.Exception = ex;
                return result;
            }

            if (result.Process != null)
            {
                result.Status = LaunchResult.EStatus.Success;
            }
            else
            {
                result.Status = LaunchResult.EStatus.LaunchFailure;
            }

            return result;
        }

        private bool isUacDenial(Exception ex) => (ex as System.ComponentModel.Win32Exception)?.NativeErrorCode == 1223;

        public bool LaunchLink(string url)
        {
            Log.Information($"WindowsProcessLauncher.LaunchLink({url})");
            ProcessStartInfo start = new ProcessStartInfo("explorer", "\"" + url + "\"");
            Process.Start(start);
            return true;
        }
    }

    internal static class Win32PInvoke
    {
        #region elevation_win32_bindings
        // ALL OF THIS IS FOR RUNNING DEELEVATED

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CloseHandle(IntPtr hHandle);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out UInt32 lpdwProcessId);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool OpenProcessToken(IntPtr ProcessHandle, UInt32 DesiredAccess, out IntPtr TokenHandle);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct STARTUPINFO
        {
            public uint cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        public const UInt32 PROCESS_QUERY_INFORMATION = 0x0400;

        public const UInt32 STANDARD_RIGHTS_REQUIRED = 0x000F0000;
        public const UInt32 STANDARD_RIGHTS_READ = 0x00020000;
        public const UInt32 TOKEN_ASSIGN_PRIMARY = 0x0001;
        public const UInt32 TOKEN_DUPLICATE = 0x0002;
        public const UInt32 TOKEN_IMPERSONATE = 0x0004;
        public const UInt32 TOKEN_QUERY = 0x0008;
        public const UInt32 TOKEN_QUERY_SOURCE = 0x0010;
        public const UInt32 TOKEN_ADJUST_PRIVILEGES = 0x0020;
        public const UInt32 TOKEN_ADJUST_GROUPS = 0x0040;
        public const UInt32 TOKEN_ADJUST_DEFAULT = 0x0080;
        public const UInt32 TOKEN_ADJUST_SESSIONID = 0x0100;
        public const UInt32 TOKEN_READ = (STANDARD_RIGHTS_READ | TOKEN_QUERY);
        public const UInt32 TOKEN_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | TOKEN_ASSIGN_PRIMARY |
            TOKEN_DUPLICATE | TOKEN_IMPERSONATE | TOKEN_QUERY | TOKEN_QUERY_SOURCE |
            TOKEN_ADJUST_PRIVILEGES | TOKEN_ADJUST_GROUPS | TOKEN_ADJUST_DEFAULT |
            TOKEN_ADJUST_SESSIONID);

        // the version, the sample is built upon:
        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern uint FormatMessage(uint dwFlags, IntPtr lpSource,
           uint dwMessageId, uint dwLanguageId, ref IntPtr lpBuffer,
           uint nSize, IntPtr pArguments);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr LocalFree(IntPtr hMem);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern uint GetProcessId(IntPtr process);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr GetCurrentProcess();

        public struct TOKEN_ELEVATION
        {
            public UInt32 TokenIsElevated;
        }

        public enum TOKEN_ELEVATION_TYPE
        {
            TokenElevationTypeDefault = 1,
            TokenElevationTypeFull = 2,
            TokenElevationTypeLimited = 3
        }

        public enum TOKEN_INFORMATION_CLASS
        {
            TokenUser = 1,
            TokenGroups = 2,
            TokenPrivileges = 3,
            TokenOwner = 4,
            TokenPrimaryGroup = 5,
            TokenDefaultDacl = 6,
            TokenSource = 7,
            TokenType = 8,
            TokenImpersonationLevel = 9,
            TokenStatistics = 10,
            TokenRestrictedSids = 11,
            TokenSessionId = 12,
            TokenGroupsAndPrivileges = 13,
            TokenSessionReference = 14,
            TokenSandBoxInert = 15,
            TokenAuditPolicy = 16,
            TokenOrigin = 17,
            TokenElevationType = 18,
            TokenLinkedToken = 19,
            TokenElevation = 20,
            TokenHasRestrictions = 21,
            TokenAccessInformation = 22,
            TokenVirtualizationAllowed = 23,
            TokenVirtualizationEnabled = 24,
            TokenIntegrityLevel = 25,
            TokenUIAccess = 26,
            TokenMandatoryPolicy = 27,
            TokenLogonSid = 28,
            MaxTokenInfoClass = 29  // MaxTokenInfoClass should always be the last enum
        }

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetTokenInformation(
            IntPtr TokenHandle,
            TOKEN_INFORMATION_CLASS TokenInformationClass,
            IntPtr TokenInformation,
            uint TokenInformationLength,
            out uint ReturnLength);

        // END: ALL OF THIS IS FOR RUNNING DEELEVATED
        #endregion elevation_win32_bindings

        const uint FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
        const uint FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
        const uint FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;
        const uint FORMAT_MESSAGE_ARGUMENT_ARRAY = 0x00002000;
        const uint FORMAT_MESSAGE_FROM_STRING = 0x00000400;

        public static string LastError()
        {
            int nLastError = Marshal.GetLastWin32Error();

            IntPtr lpMsgBuf = IntPtr.Zero;

            uint dwChars = FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                IntPtr.Zero,
                (uint)nLastError,
                0, // Default language
                ref lpMsgBuf,
                0,
                IntPtr.Zero);
            if (dwChars == 0)
            {
                return null;
            }

            string sRet = Marshal.PtrToStringAnsi(lpMsgBuf);

            // Free the buffer.
            lpMsgBuf = LocalFree(lpMsgBuf);
            return sRet;
        }
    }

}
