﻿using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Extensions;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using static Amazon.Fuel.Plugin.PlatformServices_Windows.NativeMethods;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows
{
    /// <summary>
    /// This implementation impersonates the parent process' security context.
    /// 
    /// This only works if the current user has admin privs. It should only be used from the UAC process.
    /// </summary>
    public class ParentProcessImpersonationPlugin : Disposable
    {
        private SafeTokenHandle _tokenHandle;
        private WindowsIdentity _impersonatedIdentity;
        private ProfileInfo _impersonatedProfile;

        public ParentProcessImpersonationPlugin()
        {
            _impersonatedProfile = new ProfileInfo();

            LoadImpersonatedIdentity();
        }

        public void RunImpersonated(Action lambda)
        {
            using (WindowsImpersonationContext impersonatedUser = _impersonatedIdentity.Impersonate())
            {
                lambda.Invoke();
            }
        }

        public T RunImpersonated<T>(Func<T> lambda)
        {
            using (WindowsImpersonationContext impersonatedUser = _impersonatedIdentity.Impersonate())
            {
                return lambda.Invoke();
            }
        }

        public IntPtr ImpersonatedUserRegistryHive
        {
            get
            {
                return _impersonatedProfile.hProfile;
            }
        }

        private void LoadImpersonatedIdentity()
        {
            SafeTokenHandle parentTokenHandle;
            bool success = false;

            try
            {
                success = OpenProcessToken(Process.GetCurrentProcess().Parent().Handle,
                    TOKEN_DUPLICATE | TOKEN_QUERY | TOKEN_IMPERSONATE,
                    out parentTokenHandle);
            }
            catch (Exception ex)
            {
                throw new ImpersonationException("Unable to open parent's token (exception during win32 call): " + Marshal.GetLastWin32Error(), ex);
            }

            if (!success)
            {
                throw new ImpersonationException("Unable to open parent's token: " + Marshal.GetLastWin32Error());
            }

            using (parentTokenHandle)
            {
                success = DuplicateToken(parentTokenHandle.DangerousGetHandle(),
                    SECURITY_IMPERSONATION_LEVEL,
                    out _tokenHandle);
                if (!success)
                {
                    throw new ImpersonationException("Unable to duplicate the token: " + Marshal.GetLastWin32Error());
                }
                _impersonatedIdentity = new WindowsIdentity(_tokenHandle.DangerousGetHandle());

                _impersonatedProfile = new ProfileInfo();
                _impersonatedProfile.dwSize = Marshal.SizeOf(_impersonatedProfile);
                _impersonatedProfile.lpUserName = _impersonatedIdentity.Name;
                _impersonatedProfile.dwFlags = 1;
                success = LoadUserProfile(_tokenHandle.DangerousGetHandle(), ref _impersonatedProfile);
                if (!success)
                {
                    throw new ImpersonationException("Failed to load user's profile: " + Marshal.GetLastWin32Error());
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _impersonatedIdentity.Dispose();
                _tokenHandle.Dispose();
            }
        }
    }
}
