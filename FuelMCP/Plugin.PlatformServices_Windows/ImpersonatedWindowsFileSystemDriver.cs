﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows
{
    public class ImpersonatedWindowsFileSystemDriver : WindowsFileSystemDriver
    {
        private readonly ParentProcessImpersonationPlugin _impersonator;

        public ImpersonatedWindowsFileSystemDriver(ParentProcessImpersonationPlugin impersonator)
        {
            _impersonator = impersonator;
        }

        public override string EnvironmentApplicationData {
            get
            {
                // This works because base.EnvironmentApplicationData is computed and not a constant
                return _impersonator.RunImpersonated(() => base.EnvironmentApplicationData);
            }
        }

        public override string UserDesktopPath
        {
            get
            {
                // This works because base.UserDesktopPath is computed and not a constant
                return _impersonator.RunImpersonated(() => base.UserDesktopPath);
            }
        }
    }
}
