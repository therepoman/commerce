﻿using System;
using System.Runtime.InteropServices;

namespace Amazon.Fuel.Plugin.PlatformServices_Windows
{
    public class NativeMethods
    {
        public const int TOKEN_DUPLICATE = (0x0002);
        public const int TOKEN_QUERY = (0x0008);
        public const int TOKEN_IMPERSONATE = (0x0004);
        // https://msdn.microsoft.com/en-us/library/windows/desktop/aa379572(v=vs.85).aspx
        public const int SECURITY_IMPERSONATION_LEVEL = 2;

        [DllImport("advapi32", SetLastError = true)]
        public static extern bool OpenProcessToken(IntPtr ProcessHandle, int DesiredAccess, out SafeTokenHandle TokenHandle);

        [DllImport("advapi32", SetLastError = true)]
        public static extern bool DuplicateToken(IntPtr ExistingTokenHandle, int ImpersonationLevel, out SafeTokenHandle DuplicateTokenHandle);

        [StructLayout(LayoutKind.Sequential)]
        public struct ProfileInfo
        {
            public int dwSize;
            public int dwFlags;
            [MarshalAs(UnmanagedType.LPTStr)]
            public String lpUserName;
            [MarshalAs(UnmanagedType.LPTStr)]
            public String lpProfilePath;
            [MarshalAs(UnmanagedType.LPTStr)]
            public String lpDefaultPath;
            [MarshalAs(UnmanagedType.LPTStr)]
            public String lpServerName;
            [MarshalAs(UnmanagedType.LPTStr)]
            public String lpPolicyPath;
            public IntPtr hProfile;
        }

        [DllImport("userenv.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool LoadUserProfile(IntPtr hToken, ref ProfileInfo lpProfileInfo);
    }
}
