using System;
using System.Linq;
using System.IO;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using System.Threading.Tasks;
using Polly;
using System.Threading;
using Amazon.Fuel.Common.Messages;
using System.Collections.Generic;
using Amazon.Fuel.Common;

namespace Amazon.Fuel.Plugin.Uninstall
{
    public class DeferredDeletionService : IDeferredDeletionService
    {
        public const string MetricsSource_DeferredDeletion = "DeferredDeletion";

        // note that we're blocked on this wait after install cancellation (~15 seconds max)
        public readonly int[] DeleteRenameAttemptIntervalsMs = new int[] { 250, 500, 750, 1000 };
        public const int MaxDeleteRenameRetries = 15;
        public const string DeletePathName = "_deleteMe";

        public const string STORAGE_CONTEXT = "MCP.Plugin.Uninstall";
        public const string DeleteDbKey = "deferred_delete_dirs";
        public const char DbValueDelim = ',';

        private readonly IOSDriver _osDriver;
        private readonly ISecureStoragePlugin _secureStorage;
        private readonly IMetricsPlugin _metrics;

        private CancellationTokenSource _abortToken = new CancellationTokenSource();
        private readonly object _deleteMeDirLock = new object();
        private readonly object _dbKeyLock = new object();

        public DeferredDeletionService(IOSDriver osDriver, ISecureStoragePlugin secureStorage, IMessengerHub hub, IMetricsPlugin metrics)
        {
            _osDriver = osDriver;
            _secureStorage = secureStorage;
            _metrics = metrics;

            hub.Subscribe<FuelShutdownMessage>(m => _abortToken.Cancel());
        }

        public void FlagForDeferredDelete(string path, bool deleteNow = true)
        {
            if ( path.Contains(DbValueDelim))
            {
                throw new ArgumentException($"path shouldn't have {DbValueDelim} char");
            }

            lock (_dbKeyLock)
            {
                var vals = DeferredDeletePaths;
                if (vals.Contains(path, StringComparer.InvariantCultureIgnoreCase))
                {
                    // already here
                    return;
                }

                FuelLog.Info("Adding deferred delete path", new { path });
                vals.Add(path);
                DeferredDeletePaths = vals;
            }

            if ( deleteNow )
            {
                // note: this is blocking for up to a max timing above
                MoveAndDeleteDeferredPaths();
            }
        }

        public bool TryUnflagForDeferredDelete(string path)
        {
            if (path.Contains(DbValueDelim))
            {
                throw new ArgumentException($"path shouldn't have {DbValueDelim} char");
            }

            lock (_dbKeyLock)
            {
                var vals = DeferredDeletePaths;
                if (!vals.Contains(path, StringComparer.InvariantCultureIgnoreCase))
                {
                    // not in list
                    return false;
                }

                FuelLog.Info("Removing deferred delete path", new { path });
                vals.Remove(path);
                DeferredDeletePaths = vals;
                return true;
            }
        }

        public void StartMoveAndDeleteDeferredPathsAsTask()
        {
            Task.Run(() =>
            {
                MoveAndDeleteDeferredPaths();
            });
        }

        public void MoveAndDeleteDeferredPaths()
        {
            List<string> attemptedPaths = new List<string>();
            while ( true )
            {
                if (_abortToken.IsCancellationRequested)
                {
                    break;
                }

                string deletePath = null;
                try
                {
                    List<string> paths;
                    lock (_dbKeyLock)
                    {
                        // NOTE: this can cause an exception to throw from the sql DB
                        paths = DeferredDeletePaths;
                    }

                    deletePath = paths.Where(p => !attemptedPaths.Contains(p)).FirstOrDefault();
                    if (deletePath == default(string))
                    {
                        break;
                    }

                    attemptedPaths.Add(deletePath);

                    if (!Directory.Exists(deletePath))
                    {
                        FuelLog.Info("Removing deferred deletion path (doesn't exist)", new { deletePath });
                        TryUnflagForDeferredDelete(deletePath);
                        continue;
                    }

                    // bogus dir safety
                    var rootPathTrimmed = Directory.GetParent(deletePath).FullName.TrimEnd(Path.DirectorySeparatorChar);
                    if (!rootPathTrimmed.EndsWith(Constants.Paths.PrimaryLibraryPathSubdir)
                        && !rootPathTrimmed.EndsWith(Constants.Paths.SecondaryLibraryPathSubdir))
                    {
                        FuelLog.Info("Removing deferred deletion path (doesn't appear to be part of library)", new { deletePath });
                        TryUnflagForDeferredDelete(deletePath);
                        continue;
                    }

                    var tgtPathRoot = Path.Combine(rootPathTrimmed, DeletePathName);
                    var productId = Path.GetFileName(deletePath);
                    bool moved = false;
                    bool skipped = false;

                    if (_abortToken.IsCancellationRequested)
                    {
                        break;
                    }

                    // lock delete me dir from being deleted while we're populating it
                    lock (_deleteMeDirLock)
                    {
                        if (_abortToken.IsCancellationRequested)
                        {
                            break;
                        }

                        if (!Directory.Exists(tgtPathRoot))
                        {
                            FuelLog.Info($"Creating deferred delete root dir", new { tgtPathRoot });
                            try
                            {
                                Directory.CreateDirectory(tgtPathRoot);
                            }
                            catch (Exception ex)
                            {
                                // try again later
                                FuelLog.Error(ex, "Couldn't create deletion path", new { tgtPathRoot });
                                _metrics.AddCounter(MetricsSource_DeferredDeletion, "CreateDeleteFolderFailed");
                                continue;
                            }
                        }

                        try
                        {
                            // files may still be in use, retry until not blocking anymore
                            Policy
                                .Handle<IOException>()
                                .WaitAndRetry(MaxDeleteRenameRetries,
                                    retryAttempt => TimeSpan.FromMilliseconds(DeleteRenameAttemptIntervalsMs[Math.Min(retryAttempt, DeleteRenameAttemptIntervalsMs.Length - 1)]))
                                .Execute(cancelToken =>
                                {
                                    List<string> checkPaths;
                                    lock (_dbKeyLock)
                                    {
                                        checkPaths = DeferredDeletePaths;
                                    }

                                    if ( !checkPaths.Contains(deletePath))
                                    {
                                        // no longer trying to delete (someone started an install task?)
                                        FuelLog.Info("Path removed from delete queue", new { deletePath });
                                        skipped = true;
                                        return;
                                    }

                                    if (cancelToken.IsCancellationRequested)
                                    {
                                        return;
                                    }

                                    int i = -1;
                                    var moveTargetPath = "";
                                    do
                                    {
                                        ++i;
                                        moveTargetPath = Path.Combine(tgtPathRoot, $"{productId}_{i}");
                                    } while (Directory.Exists(moveTargetPath));

                                    FuelLog.Info($"Moving deferred delete dir", new { productId, deletePath, tgtPathRoot });

                                    Directory.Move(deletePath, moveTargetPath);
                                    moved = true;
                                }, cancellationToken: _abortToken.Token);
                        }
                        catch (Exception ex)
                        {
                            // try again later
                            FuelLog.Error(ex, "Couldn't move deferred delete dir", new { productId, deletePath, tgtPathRoot });
                        }

                        if (moved)
                        {
                            TryUnflagForDeferredDelete(deletePath);
                        }
                        else if ( !skipped && !_abortToken.IsCancellationRequested )
                        {
                            // try again later
                            FuelLog.Error($"Couldn't move deferred delete dir", new { productId, deletePath, tgtPathRoot });
                            _metrics.AddCounter(MetricsSource_DeferredDeletion, "MoveFolderFailed");
                        }
                    }
                }
                catch (Exception ex)
                {
                    FuelLog.Error(ex, $"Couldn't move deferred delete dir", new { deletePath });
                    // try again later
                    break;
                }
            }

            if (!_abortToken.IsCancellationRequested)
            {
                // files are out of the way, let this behavior complete async (don't wait on deletion)
                StartRemoveDeleteMeAsTask();
            }
        }

        public async void StartRemoveDeleteMeAsTask()
        {
            await Task.Run(() =>
            {
                RemoveDeleteMe();
            });
        }

        public void RemoveDeleteMe()
        {
            var roots = _osDriver.AllLibraryRoots;

            Array.ForEach(roots, root =>
            {
                if (_abortToken.IsCancellationRequested)
                {
                    return;
                }

                try
                {
                    string deleteMeDirectory = Path.Combine(root, DeletePathName);
                    if (Directory.Exists(deleteMeDirectory))
                    {
                        if ( _abortToken.IsCancellationRequested )
                        {
                            return;
                        }

                        lock (_deleteMeDirLock)
                        {
                            if (_abortToken.IsCancellationRequested)
                            {
                                return;
                            }

                            if (Directory.Exists(deleteMeDirectory))
                            {
                                FuelLog.Info($"Removing {DeletePathName} in {root}");
                                try
                                {
                                    Directory.Delete(deleteMeDirectory, true);
                                }
                                catch (Exception ex)
                                {
                                    FuelLog.Error(ex, "Exception while removing deferred deletion files from root", new { DeletePathName, root });
                                    _metrics.AddCounter(MetricsSource_DeferredDeletion, "UnknownExceptionDeletingFolder");
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    FuelLog.Error(ex, "Exception while removing deferred deletion files from root", new { root });
                    _metrics.AddCounter(MetricsSource_DeferredDeletion, "UnknownExceptionDeletingRootFolder");
                }
            });
        }

        private List<string> DeferredDeletePaths
        {
            get
            {
                return (_secureStorage.Get(STORAGE_CONTEXT, DeleteDbKey)?.Split(DbValueDelim) ?? new string[0])
                    .Where(i => !string.IsNullOrEmpty(i))
                    .ToList();
            }
            set
            {
                _secureStorage.Put(STORAGE_CONTEXT, DeleteDbKey, string.Join($"{DbValueDelim}", value));
            }
        }

    }
}