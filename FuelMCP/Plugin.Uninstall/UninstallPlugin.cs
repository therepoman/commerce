﻿using System;
using System.IO;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Serilog;
using System.Linq;
using Amazon.Fuel.Common;
using Microsoft.Win32;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Clients.Twitch.Spade.Client;
using Clients.Twitch.Spade.Model.Event;
using Sds=Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Plugin.Uninstall
{
    public class UninstallPlugin : IUninstallPlugin
    {
        private readonly IRegisterInstallation _registerInstall;
        private readonly IInstalledGamesPlugin _installedGames;
        private readonly IOSServicesDriver _osServicesDriver;
        private readonly IMessengerHub _hub;
        private readonly IShortcutManager _shortcutManager;
        private readonly IOSDriver _osDriver;
        private readonly ITwitchUserStoragePlugin _twitchUserStorage;
        private readonly IMetricsPlugin _metrics;
        private readonly IManifestStorage _manifestStorage;

        private readonly string _storageContext = "MCP.Plugin.Uninstall";

        public UninstallPlugin(
            IRegisterInstallation registerInstall, 
            IInstalledGamesPlugin installedGames, 
            IOSServicesDriver osServicesDriver,
            IOSDriver osDriver,
            IMessengerHub hub, 
            IShortcutManager shortcutManager, 
            ISecureStoragePlugin secureStorage,
            ITwitchUserStoragePlugin twitchUserStorage,
            IMetricsPlugin metrics,
            IManifestStorage manifestStorage)
        {
            _registerInstall = registerInstall;
            _installedGames = installedGames;
            _osServicesDriver = osServicesDriver;
            _osDriver = osDriver;
            _hub = hub;
            _shortcutManager = shortcutManager;
            _twitchUserStorage = twitchUserStorage;
            _metrics = metrics;
            _manifestStorage = manifestStorage;
        }

        public void DeleteDBDataFromDisk()
        {
            // brute force
            _osDriver.WipeData();
        }

        public bool IsRunning(ProductId productId)
        { 
            return _installedGames.IsRunning(productId);
        }

        public void RemoveUser(string user)
        {
            tryRemoveAppData(user);
        }

        public bool TryUninstallAll()
        {
            try
            {
                foreach (var info in _installedGames.All)
                {
                    try
                    {
                        DeleteGame(info.ProductId);
                    }
                    catch (Exception e)
                    {
                        Log.Error(e, $"Couldn't uninstall game '{info.Id}'");
                    }
                }

                DeleteDBDataFromDisk();

                _metrics.AddCounter(
                    $"Uninstall:All",
                    MetricStrings.METRIC_Success);

                _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos, MetricStrings.METRIC_Success);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Couldn't uninstall all users");
                return false;
            }
        }

        private void tryRemoveAppData(string user)
        {
            if (user.Contains("\\"))
            {
                user = user.Substring(user.LastIndexOf("\\", StringComparison.Ordinal) + 1);
            }
            try
            {
                Directory.Delete($"C:\\Users\\{user}\\AppData\\Roaming\\Twitch", true);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error removing appdata");
            }
        }

        public bool TryEnsureUninstallerExeInCommonLocation()
        {
            try
            {
                var targetExe = _osDriver.CommonUninstallerExePath;
                var targetDir = Directory.GetParent(_osDriver.CommonUninstallerExePath).FullName;

                if (!Directory.Exists(targetDir))
                {
                    Directory.CreateDirectory(targetDir);
                }

                var sourceExe = _osDriver.GameRemoverSourcePath;
                if (!File.Exists(sourceExe))
                {
                    throw new Exception($"Couldn't find uninstaller exe at {sourceExe}");
                }

                if (!File.Exists(targetExe)
                    || !FileCompareUtil.BinaryCompare(sourceExe, targetExe) )
                {
                    Log.Information("Uninstaller TwitchApp exe appears different from common exe, updating");
                    try
                    {
                        if (File.Exists(targetExe))
                        {
                            // just recopy the file to ensure it is up to date
                            File.Delete(targetExe);
                        }

                        File.Copy(sourceExe, targetExe);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, $"Couldn't copy uninstaller {sourceExe} => {targetExe}, file in use?");
                        throw;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Couldn't copy uninstaller or uninstaller dependency files",
                    new
                    {
                        srcExists =_osDriver.GameRemoverSourcePath,
                        srcPermissions = _osDriver.DescribePermissions(_osDriver.GameRemoverSourcePath),
                        tgtExists = File.Exists(_osDriver.CommonUninstallerExePath),
                        tgtPermissions = _osDriver.DescribePermissions(_osDriver.CommonUninstallerExePath)
                    });
                return false;
            }
        }

        public void DeleteGame(ProductId adgProductId)
        {
            GameInstallInfo installInfo = null;
            string sdsVersionId = null;
            string asin = null;
            var transactionId = Guid.NewGuid().ToString();

            try
            {
                installInfo = _installedGames?.Get(adgProductId);
            }
            catch (Exception e)
            {
                Log.Error(e, $"Unexpected exception thrown while trying to gather data for Spade event");
            }

            if (installInfo != null)
            {
                sdsVersionId = installInfo.InstallVersion;
                asin = installInfo.ProductAsin;
                installInfo.Installed = false;
                installInfo.InstallVersion = null;
                installInfo.InstallVersionName = null;
            }

            SendFuelLauncherActionEvent(adgProductId, asin, sdsVersionId, FuelLauncherActionType.GameUninstallStart, transactionId);


            // unregister from DB
            _installedGames.Save(installInfo);

            var hitErrors = false;
            foreach (var action in new Action[]
            {
                () => DeleteUninstallerOSLink(adgProductId),
                () => DeleteGameShortcuts(adgProductId),
                () => DeleteGameFiles(adgProductId),
                () => DeleteHashcache(adgProductId)
            })
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    hitErrors = true;
                    Log.Error(e, $"Unexpected exception thrown while tring to delete adgProductId[{ adgProductId?.Id }]");
                }
            }

            if (hitErrors)
            {
                SendFuelLauncherActionErrorEvent(adgProductId, asin, sdsVersionId, "Unknown", FuelLauncherErrorType.GameUninstallFailure, transactionId);
            }

            SendFuelLauncherActionEvent(adgProductId, asin, sdsVersionId, FuelLauncherActionType.GameUninstallFinish, transactionId);
        }

        public void DeleteGameFilesByManifest(Sds.Manifest manifest, string installDir)
        {
            if ( manifest == null)
            {
                throw new ArgumentNullException("manifest");
            }

            if (installDir != null
                && Directory.Exists(installDir)
                && Path.GetDirectoryName(installDir) != null)
            {
                var packageCount = manifest.Packages.Count;
                for (int i = 0; i < packageCount; ++i)
                {
                    var manifestPackage = manifest.Packages[i];
                    foreach (var file in manifestPackage.Files)
                    {
                        var fullPath = Path.Combine(installDir, file.Path);
                        if (File.Exists(fullPath) == true)
                        {
                            try
                            {
                                File.Delete(fullPath);
                            }
                            catch (Exception e)
                            {
                                // condense exceptions to avoid stack spam
                                FuelLog.Error("Couldn't delete file", new { fullPath, exception=e.ToString() });
                            }
                        }
                    }
                }

                FileUtilities.DeleteEmptyFolders(installDir);

                if (!Directory.EnumerateFileSystemEntries(installDir).Any())
                {
                    Directory.Delete(installDir);
                }
            }
        }

        public void DeleteGameFiles(ProductId productId)
        {
            try
            {
                Log.Information($"[UIN] Uninstalling game files for '{productId.Id}'");
                var installInfo = _installedGames.Get(productId);

                if (installInfo == null)
                {
                    Log.Warning($"Couldn't find InstallInfo for '{productId}'");

                    // TODO should we attempt to find installed files and delete?
                    return;
                }

                var manifest = _manifestStorage.Get(productId, installInfo.InstallVersion).GetAwaiter().GetResult();
                if (manifest == null)
                {
                    _metrics.AddCounter("Uninstall", "NoManifest");

                    var dir = installInfo.InstallDirectory;
                    Log.Warning($"No manifest found for game, removing full dir", new { installInfo.Id, dir});

                    // sanity
                    if ( string.IsNullOrEmpty(dir) )
                    {
                        FuelLog.Error($"Install info directory is null/empty, can't delete dir", new { installInfo.Id, dir });
                        return;
                    }

                    var parent = Directory.GetParent(dir)?.FullName;
                    if ( string.IsNullOrEmpty(parent)
                        || ( !parent.Equals(_osDriver.PrimaryLibraryRoot, StringComparison.InvariantCultureIgnoreCase)
                            && !parent.Equals(_osDriver.GetSecondaryLibraryRoot(Path.GetPathRoot(dir)), StringComparison.InvariantCultureIgnoreCase)))
                    {
                        FuelLog.Error($"Install info directory doesn't appear to be valid library directory, skipping delete dir", new { installInfo.Id, dir });
                        return;
                    }

                    if ( !_osDriver.TryDeleteProtected(installInfo.InstallDirectory, recurse: true) )
                    {
                        FuelLog.Error($"Install info directory deletion failed", new { installInfo.Id, dir });
                    }
                }
                else
                {
                    DeleteGameFilesByManifest(manifest: manifest,
                                            installDir: installInfo.InstallDirectory);
                }

                installInfo.InstallVersion = null;
                installInfo.InstallVersionName = null;

                // unregister from DB
                _installedGames.Save(installInfo);

                // finally, message the app
                _hub.Publish(new InstallStateChangedMessage());
                _hub.Publish(new GameDeletedMessage());
            }
            catch (Exception e)
            {
                Log.Error(e, $"Error removing game for {productId.Id}");
            }
        }

        public void DeleteUninstallerOSLink(ProductId productId)
        {
            // remove the any uninstaller links in the platform
            _registerInstall.UnregisterUninstaller(productId);
        }

        public void DeleteGameShortcuts(ProductId productId)
        {
            var installInfo = _installedGames.Get(productId);
            if (installInfo == null)
            {
                Log.Warning($"Couldn't remove shortcuts for '{productId}', product info missing");
            }
            else
            {
                _shortcutManager.RemoveShortcuts(installInfo);
            }
        }

        public void DeleteHashcache(ProductId productId)
        {
            var path = Path.Combine(_osDriver.GameDataPath, productId.Id, Constants.Paths.SqliteDatabaseSubDir);
            FuelLog.Info("Removing hashcache sqlite db", new { path });
            _osDriver.TryDeleteProtected(path, recurse: false);
        }

        private void SendFuelLauncherActionEvent(ProductId adgProductId, string asin, string sdsVersionId, FuelLauncherActionType actionType, string transactionId)
        {
            var deviceId = DeviceInfo.DSN;
            var tuid = _twitchUserStorage?.User?.Tuid;
            var clientTime = SpadeClient.ClientTimeNow();
            _metrics.SendSpadeEvent(new FuelLauncherAction(deviceId, tuid, clientTime, asin, adgProductId?.Id, sdsVersionId, actionType, transactionId));
        }

        private void SendFuelLauncherActionErrorEvent(ProductId adgProductId, string asin, string sdsVersionId, string errorReason, FuelLauncherErrorType errorType, string transactionId)
        {
            var deviceId = DeviceInfo.DSN;
            var tuid = _twitchUserStorage?.User?.Tuid;
            var clientTime = SpadeClient.ClientTimeNow();
            _metrics.SendSpadeEvent(new FuelLauncherError(deviceId, tuid, clientTime, asin, adgProductId?.Id, sdsVersionId, errorReason, errorType, transactionId));
        }
    }
}
