﻿using System;
using Amazon.Fuel.Common.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using Amazon.Fuel.Common.Utilities;
using Serilog;

namespace Amazon.Fuel.Plugin.TwitchUserStorage
{
    public class TwitchUserStoragePlugin : ITwitchUserStoragePlugin
    {
        private readonly ISecureStoragePlugin _secureStoragePlugin;

        public const string KEY_DATE = "date";
        public const string KEY_DISPLAY_NAME = "display_name";
        public const string KEY_LOGO = "logo";
        public const string KEY_PID = "pid";
        public const string KEY_OAUTH = "oauth";
        public const string KEY_REFRESH = "refresh";
        public const string KEY_TUID = "tuid";
        public const string KEY_USERNAME = "username";
        public const string KEY_SESSIONUSERID = "sessionUserId";
        public const string KEY_ENTITLEMENT = "entitlement";
        public const string KEY_SCOPES = "scopes";
        public const string KEY_CURSE_TOKEN = "curseToken";

        public const string STORAGE_CONTEXT = "MCP.Plugin.TwitchLogin";
        public const string FORMAT = "yyyy/MM/dd HH:mm:ss.fffffffzz";

        public TwitchUserStoragePlugin(ISecureStoragePlugin secureStoragePlugin)
        {
            _secureStoragePlugin = secureStoragePlugin;
        }

        public LocalTwitchUser User
        {
            get
            {
                try
                {
                    return new LocalTwitchUser
                    {
                        AccessToken = Get(KEY_OAUTH),
                        Tuid = Get(KEY_TUID),
                        Username = Get(KEY_USERNAME),
                        DisplayName = Get(KEY_DISPLAY_NAME),
                        Logo = Get(KEY_LOGO),
                        SessionUserId = Get(KEY_SESSIONUSERID),
                    };
                }
                catch (CryptographicException ex)
                {
                    Log.Error(ex, "Crypto error retrieving user info");
                }
                catch ( Exception ex)
                {
                    Log.Error(ex, "Error retrieving user info");
                }

                return null;
            }

            set
            {
                _secureStoragePlugin.PutAll(STORAGE_CONTEXT, 
                    new SecureStorageItem[]
                    {
                        new SecureStorageItem(KEY_OAUTH, value.AccessToken),
                        new SecureStorageItem(KEY_TUID, value.Tuid),
                        new SecureStorageItem(KEY_USERNAME, value.Username),
                        new SecureStorageItem(KEY_DISPLAY_NAME, value.DisplayName),
                        new SecureStorageItem(KEY_LOGO, value.Logo),
                        new SecureStorageItem(KEY_SESSIONUSERID, value.SessionUserId),
                    });
            }
        }

        private string Get(string key)
        {
            return _secureStoragePlugin.Get(STORAGE_CONTEXT, key);
        }

        public virtual void Remove()
        {
            try
            {
                _secureStoragePlugin.Remove(STORAGE_CONTEXT);
            }
            catch (Exception e)
            {
                // make this more fault tolerant
                FuelLog.Warn("Couldn't remove user storage from secure storage", new {ex = e.Message});
            }
        }

        public TwitchCredentialSet GetTwitchCredentialSet(string clientId)
        {
            if (Get(key(clientId, KEY_OAUTH)) == null)
            {
                return null;
            }
            return new TwitchCredentialSet
            {
                AccessToken = Get(key(clientId, KEY_OAUTH)),
                Created = DateTime.ParseExact(Get(key(clientId, KEY_DATE)), FORMAT, null),
                Pid = Int32.Parse(Get(key(clientId, KEY_PID))),
                Entitlement = Get(key(clientId, KEY_ENTITLEMENT)),
                Scopes = StringToScopes(Get(key(clientId, KEY_SCOPES)))
            };
        }

        public void SetTwitchCredentialSet(string clientId, TwitchCredentialSet credentials)
        {
            _secureStoragePlugin.PutAll(STORAGE_CONTEXT,
                new SecureStorageItem[]
                {
                    new SecureStorageItem(key(clientId, KEY_OAUTH), credentials.AccessToken),
                    new SecureStorageItem(key(clientId, KEY_DATE), credentials.Created.ToLocalTime().ToString(FORMAT)),

                    new SecureStorageItem(key(clientId, KEY_PID), "" + credentials.Pid),
                    new SecureStorageItem(key(clientId, KEY_ENTITLEMENT), credentials.Entitlement),
                    new SecureStorageItem(key(clientId, KEY_SCOPES), ScopesToString(credentials.Scopes)),
                });
        }

        private string key(string clientid, string part)
        {
            return $"{clientid}.{part}";
        }

        private string ScopesToString(IEnumerable<string> scopes)
        {
            return string.Join(",", scopes);
        }

        private IEnumerable<string> StringToScopes(string strScopes)
        {
            return new List<string>(strScopes.Split(','));
        }
    }
}
