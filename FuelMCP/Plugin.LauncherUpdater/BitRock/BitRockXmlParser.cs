﻿using System.Xml;
using Serilog;

namespace Amazon.MCP.Plugin.LauncherUpdater.BitRock
{
    public class BitRockXmlParser
    {
        private static readonly string VersionNode = "version";
        private static readonly string UrlNode = "url";
        private static readonly string PlatformFileNode = "platformFile";
        private static readonly string PlatformNode = "platform";
        private static readonly string FilenameNode = "filename";

        /// <summary>
        /// Will extract the URL for the installer appropriate for the specified platform.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        public string ExtractInstallerUrl(string platform, XmlDocument doc)
        {
            if (doc == null)
            {
                Log.Error($"Called ExtractInstallerUrl({platform},null) but doc is not allowed to be null.");
                return null;
            }

            string result = null;
            string urlBase = ExtractStringValueByNodeName(UrlNode, doc);

            if (string.IsNullOrEmpty(urlBase))
            {
                Log.Error($"URL Base is null or empty for platform {platform}");
                return null;
            }

            XmlNodeList nodes = doc.GetElementsByTagName(PlatformFileNode);
            if (nodes.Count == 0)
            {
                Log.Error($"Xml tag {PlatformFileNode} expected at least 1 time, but appears {nodes.Count} times");
                return null;
            }

            foreach (XmlNode node in nodes)
            {
                if (node.ChildNodes.Count != 2)
                {
                    Log.Error($"Xml tag {node.Name} expects exactly 2 children, but has {node.ChildNodes.Count}.");
                    return null;
                }
                string curPlatformName = ExtractByNameFromNodeList(node.ChildNodes, PlatformNode);
                if (platform == curPlatformName)
                {
                    var filename = ExtractByNameFromNodeList(node.ChildNodes, FilenameNode);
                    if (string.IsNullOrEmpty(filename))
                    {
                        Log.Error($"Filename for platform {platform} is null or empty");
                        return null;
                    }
                    result = urlBase + filename;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Extract the version string from the xml document.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public string ExtractVersion(XmlDocument doc)
        {
            return ExtractStringValueByNodeName(VersionNode, doc);
        }

        /// <summary>
        /// Returns the value of an XML tag by node name.  Returns null or empty string to indicate problems.
        /// Only appropriate for nodes whose name appears exactly one time in the xml document.
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        public string ExtractStringValueByNodeName(string tagName, XmlDocument doc)
        {
            if (doc == null)
            {
                Log.Error($"Called ExtractStringValueByName({tagName},null) but doc not allowed to be null.");
                return null;
            }

            string result = null;
            XmlNodeList nodes = doc.GetElementsByTagName(tagName);
            if (nodes.Count != 1)
            {
                Log.Error($"Xml tags expected exactly 1 time, but tag {tagName} appears {nodes.Count} times.");
                return result;
            }

            XmlNode targetNode = nodes[0];
            if (targetNode.ChildNodes.Count == 0 || string.IsNullOrEmpty(targetNode.FirstChild.Value))
            {
                Log.Error($"Xml tag {tagName} has no children, meaning no value for us to use.");
                return result;
            }

            return targetNode.FirstChild.Value;
        }

        /// <summary>
        /// Extracts a node by name from the input node list.
        /// </summary>
        /// <param name="nodeList"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public string ExtractByNameFromNodeList(XmlNodeList nodeList, string nodeName)
        {
            if (nodeList == null || string.IsNullOrEmpty(nodeName))
            {
                Log.Error(
                    $"Called ExtractByNameFromNodeList( nodeList: {nodeList}, nodeName: {nodeName} ), but neither arg may be null or empty");
                return null;
            }

            foreach (XmlNode node in nodeList)
            {
                if (node == null)
                {
                    return null;
                }
                if (node.Name == nodeName)
                {
                    return node.FirstChild?.Value;
                }
            }
            return null;
        }
    }
}
