﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Amazon.MCP.Common.Messages;

namespace Amazon.MCP.Plugin.LauncherUpdater.UX
{
    /// <summary>
    /// Interaction logic for LauncherUpdateWindow.xaml
    /// </summary>
    public partial class LauncherUpdateWindow : Window
    {
        public LauncherUpdateWindow(string text)
        {
            InitializeComponent();
            txtBlk.Text = "";
        }

        private void btnOkButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
