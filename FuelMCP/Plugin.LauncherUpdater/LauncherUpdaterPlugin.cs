﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using Serilog;

namespace Amazon.MCP.Plugin.LauncherUpdater
{
    public class LauncherUpdaterPlugin
    {
        private const string CmdFileName = "cmd.exe";
        private static readonly string PathToUpdater = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\autoupdate.exe";

        private bool _isUpdateAvailable = false;
        
        public bool RunUpdater()
        {
            CheckForUpdates();
            return _isUpdateAvailable;
        }

        private void CheckForUpdates()
        {
            Log.Information("About to update the Launcher");

            // Silently checks for updates to check if any are available w/o downloading
            var exitCode = ExecuteCommandInHiddenWindowSync("--mode unattended",
                $"{PathToUpdater}");

            switch (exitCode)
            {
                case 0:
                    Log.Information("Update is available.");
                    var result = MessageBox.Show("An update to the Twitch Launcher is required to continue. Would you like to update now?", "Update", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if ( result == MessageBoxResult.Yes )
                    {
                        RunAutoUpdater();
                    }

                    _isUpdateAvailable = true;
                    break;
                case 1:
                    Log.Information("No update was found.");
                    break;
                case 2:
                    Log.Warning("Error connecting to remote server or invalid XML file while checking for updates");
                    break;
                default:
                    Log.Warning("Checking for updates resulted in unexpected exit code. Exit Code: " + exitCode);
                    MessageBox.Show("There was an issue while checking for updates. Try again later", "Update", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
            }
        }

        private static void RunAutoUpdater()
        {
            ExecuteCommandInHiddenWindowAsync("--mode unattended --unattendedmodebehavior download --unattendedmodeui minimalWithDialogs", PathToUpdater, true);
        }

        /// <summary>
        /// Executes the input args in a hidden window asynchrounously.
        /// You may pass an event handler that will trigger when the process exits.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="filename"></param>
        /// <param name="elevate"></param>
        /// <param name="exitHandler"></param>
        public static void ExecuteCommandInHiddenWindowAsync(string args, string filename = CmdFileName, bool elevate = false, EventHandler exitHandler = null)
        {
            if (filename != CmdFileName && !File.Exists(filename))
            {
                return;
            }

            //See http://stackoverflow.com/questions/1469764/run-args-prompt-commands
            Process process = BuildHiddenWindowProcess(filename, args, elevate);

            if (exitHandler != null)
            {
                process.EnableRaisingEvents = true;
                process.Exited += exitHandler;
            }

            process.Start();
        }

        /// <summary>
        /// Executes the input args in a hidden window synchrounously.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="filename">the process to run, defaults to cmd if not specified</param>
        /// <param name="elevate"></param>
        /// <returns></returns>
        public static int ExecuteCommandInHiddenWindowSync(string args, string filename = CmdFileName, bool elevate = false)
        {
            if (filename != CmdFileName && !File.Exists(filename))
            {
                return -1;
            }

            Process process = BuildHiddenWindowProcess(filename, args, elevate);
            process.Start();
            process.WaitForExit();
            return process.ExitCode;
        }

        /// <summary>
        /// Executes the args in a new process in a non-blocking manner.  Process will live beyond the lifetime of the
        /// spawning application if needed, such as when launching a new installer and killing the original app.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="args"></param>
        /// <param name="elevate"></param>
        /// <returns></returns>
        private static Process BuildHiddenWindowProcess(string filename, string args, bool elevate)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = filename,
                Verb = elevate ? "runas" : null,
                Arguments = args
            };

            process.StartInfo = startInfo;

            return process;
        }
    }
}