﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.CommandLineParser;
using System;
using System.Diagnostics;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.App.ApplicationDataRemover.Resources;
using Serilog;

namespace Amazon.Fuel.App.ApplicationDataRemover
{
    /// <summary>
    /// UninstallationController is responsible for launching UI and providing uninstallation services.
    /// </summary>
    public class UninstallController
    {
        private readonly IMessengerHub _hub;
        private readonly IUninstallPlugin _uninstall;
        private readonly IInstalledGamesPlugin _installedGames;
        private readonly IMetricsPlugin _metrics;

        public UninstallController(
            IMessengerHub hub,
            IUninstallPlugin uninstall,
            IInstalledGamesPlugin installedGames,
            IMetricsPlugin metrics)
        {
            _uninstall = uninstall;
            _hub = hub;
            _installedGames = installedGames;
            _metrics = metrics;
        }

        public void Start(AppDataRemoverArgs.Modes mode, ProductId productId = null)
        {
            Log.Information($"[UNI] Start({mode},{productId})");

            try
            {
                switch (mode)
                {
                    case AppDataRemoverArgs.Modes.GameSilent:
                        UninstallGame(productId, mode);
                        break;
                    case AppDataRemoverArgs.Modes.Game:
                        UninstallGame(productId, mode);
                        break;
                    case AppDataRemoverArgs.Modes.AllWithConfirm:
                        var dlg = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog
                        {
                            Caption = UIStrings.UNINST_ConfirmTitle,
                            InstructionText = UIStrings.UNINST_ConfirmMsg,
                            Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Warning,
                            StandardButtons =
                                Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Yes |
                                Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Cancel,
                        };

                        if ( dlg.Show() == Microsoft.WindowsAPICodePack.Dialogs.TaskDialogResult.Yes )
                        {
                            goto case AppDataRemoverArgs.Modes.All;
                        }
                        break;

                    case AppDataRemoverArgs.Modes.All:
                        _uninstall.TryUninstallAll();
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Exception encountered during uninstall '{ex.Message}'");
                _metrics.AddCounter(
                    $"Uninstall:{mode.ToString()}",
                    MetricStrings.METRIC_Failure);
                _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos, MetricStrings.METRIC_UnexpectedFailure);
                _metrics.AddCounter(MetricStrings.SOURCE_UninstallUnexpectedFailures, MetricStrings.METRIC_GenericFailure);
                killApp(EUninstallResult.Failed);
            }

            killApp(EUninstallResult.Success);
        }

        private void UninstallGame(ProductId productId, AppDataRemoverArgs.Modes mode)
        {
            var sw = Stopwatch.StartNew();
            GameInstallInfo installInfo = null;
            try
            {
                bool showUI = mode != AppDataRemoverArgs.Modes.GameSilent;

                Log.Information($"[UNI] UninstallGame({productId})");

                if (productId == null)
                {
                    throw new ArgumentException($"No product ID provided");
                }

                try
                {
                    installInfo = _installedGames.Get(productId);
                }
                catch (Exception e)
                {
                    Log.Error(e, $"[UNI] Error accessing DB");
                }

                if (installInfo?.Installed ?? false)
                {
                    if (installInfo.ProductId != productId)
                    {
                        Log.Error(
                            $"Product id mismatch in installed game DB. Expected: {productId}, encountered: {installInfo.ProductId}");
                    }

                    // NOTE: this will sometimes be null/empty on old installs
                    var gameTitle = installInfo.ProductTitle;

                    if (!showUI
                        || PromptUserToUninstall(gameTitle, installInfo.InstallDirectory))
                    {
                        while (_uninstall.IsRunning(productId))
                        {
                            if (!showUI)
                            {
                                // consider it a failure
                                _metrics.AddCounter(
                                    $"Uninstall:{mode.ToString()}",
                                    MetricStrings.METRIC_Failure);

                                _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos,
                                    MetricStrings.METRIC_ExpectedFailure);
                                _metrics.AddCounter(MetricStrings.SOURCE_UninstallExpectedFailure,
                                    MetricStrings.METRIC_IsRunning);
                                killApp(EUninstallResult.Failed_IsRunning);
                            }
                            else
                            {
                                var isRunningDlg = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog
                                {
                                    Caption = UIStrings.UNINST_GameRunningTitle,
                                    // NOTE: Game is not localized, but this is something of an edge case (that we get this error AND the title isn't defined/old title)
                                    InstructionText = string.Format(UIStrings.UNINST_GameRunningMsg,
                                        !string.IsNullOrEmpty(gameTitle) ? gameTitle : "Game"),
                                    Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Error,
                                    StandardButtons =
                                        Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Retry |
                                        Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Cancel,
                                };

                                bool cancelApp = isRunningDlg.Show() ==
                                                 Microsoft.WindowsAPICodePack.Dialogs.TaskDialogResult.Cancel;
                                if (cancelApp)
                                {
                                    _metrics.AddCounter(
                                        $"Uninstall:{mode.ToString()}",
                                        MetricStrings.METRIC_Cancel,
                                        MetricStrings.PIVOT_ProductId,
                                        productId.Id);

                                    _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos, MetricStrings.METRIC_Cancel);

                                    killApp(EUninstallResult.Cancelled);
                                }
                            }
                        }

                        _uninstall.DeleteGame(productId);
                        _metrics.AddCounter(
                            $"Uninstall:{mode.ToString()}",
                            MetricStrings.METRIC_Success,
                            MetricStrings.PIVOT_ProductId,
                            productId.Id);

                        _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos, MetricStrings.METRIC_Success);

                        // [FLCL-267] -- any failure cases here to report? Seems like there should be (file locked b/c of playing, etc.)

                        var instructionText = string.Format(!string.IsNullOrEmpty(gameTitle)
                                ? UIStrings.UNINST_SuccessMsg
                                : UIStrings.UNINST_SuccessMsgNoGameTitle,
                            gameTitle);

                        if (showUI)
                        {
                            var dlg = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog
                            {
                                Caption = UIStrings.UNINST_SuccessTitle,
                                InstructionText = instructionText,
                                Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Information,
                                StandardButtons = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Ok,
                            };
                            dlg.Show();
                        }
                    }
                    else
                    {
                        _metrics.AddCounter(
                            $"Uninstall:{mode.ToString()}",
                            MetricStrings.METRIC_Cancel,
                            MetricStrings.PIVOT_ProductId,
                            productId.Id);

                        _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos, MetricStrings.METRIC_Cancel);
                        killApp(EUninstallResult.Cancelled);
                    }
                }
                else
                {
                    var metricFailure = installInfo != null
                        ? MetricStrings.METRIC_UninstallNotInstalled
                        : MetricStrings.METRIC_UninstallInfoMissing;

                    _metrics.AddCounter(
                        $"Uninstall:{mode.ToString()}",
                        metricFailure,
                        MetricStrings.PIVOT_ProductId,
                        productId.Id);

                    _metrics.AddCounter(MetricStrings.SOURCE_UninstallQos, MetricStrings.METRIC_UnexpectedFailure);
                    _metrics.AddCounter(MetricStrings.SOURCE_UninstallUnexpectedFailures, metricFailure);

                    _uninstall.DeleteGame(productId);
                    _uninstall.DeleteUninstallerOSLink(productId);

                    if (showUI)
                    {
                        var dlg = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog
                        {
                            Caption = UIStrings.UNINST_ErrorUnrecognizedTitle,
                            InstructionText = string.Format(UIStrings.UNINST_ErrorUnrecognizedMsg, productId),
                            Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Warning,
                            StandardButtons = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Ok,
                        };
                        dlg.Show();
                    }
                    killApp(EUninstallResult.Failed);
                }
            }
            finally
            {
                _metrics.AddTimerValue(
                    PmetUtilities.makeValidPmetString(
                        $"{MetricStrings.SOURCE_UninstallGame}:{installInfo?.ProductTitle}"),
                    MetricStrings.METRIC_TotalTimeInMs, sw.ElapsedMilliseconds);
            }
        }

        private void killApp(EUninstallResult exitCode)
        {
            Log.Information($"[UNI] killApp()");
            _hub.Publish(new FuelShutdownMessage((int)exitCode));
            Environment.Exit((int)exitCode);
        }

        /// <summary>
        /// Asks the user if they wish to uninstall a game.
        /// </summary>
        /// <param name="gameTitle"></param>
        /// <param name="installDir"></param>
        /// <returns>True if the user wishes to uninstall the game</returns>
        private static bool PromptUserToUninstall(string gameTitle, string installDir)
        {
            Log.Information($"[UNI] PromptUserToUninstall()");

            var hasTitle = !string.IsNullOrEmpty(gameTitle);
            var caption = hasTitle ? string.Format(UIStrings.UNINST_PromptTitle, gameTitle)
                                   : UIStrings.UNINST_PromptTitleNoGameTitle;

            var instructionText = hasTitle ? string.Format(UIStrings.UNINST_PromptMsg, gameTitle, installDir)
                                           : UIStrings.UNINST_PromptMsgNoGameTitle;

            Microsoft.WindowsAPICodePack.Dialogs.TaskDialog dlg = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog
            {
                Caption = caption,
                Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Warning,
                InstructionText = instructionText,
                StandardButtons =
                    Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Yes |
                    Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Cancel,
                
            };

            var result = dlg.Show();
            return result == Microsoft.WindowsAPICodePack.Dialogs.TaskDialogResult.Yes;
        }
    }
}
