﻿using Amazon.Fuel.Common.CommandLineParser;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.AppConfig;
using Amazon.Fuel.Plugin.Launcher.Models;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Amazon.Fuel.Plugin.SecureStorage;
using Amazon.Fuel.Plugin.Uninstall;
using Autofac;
using Amazon.Fuel.Configurations.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Amazon.Fuel.AppCore;
using System.Threading;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Plugin.Metrics;
using Serilog;
using System.Globalization;
using Microsoft.WindowsAPICodePack.Dialogs;
using Amazon.Fuel.Common;
using Amazon.Fuel.Plugin.PlatformServices_Windows;
using Amazon.Fuel.Plugin.Logger;
using System.IO;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.Plugin.TwitchUserStorage;
using Clients.Twitch.Spade.Client;
using Amazon.Fuel.App.ApplicationDataRemover.Resources;

namespace Amazon.Fuel.App.ApplicationDataRemover
{
    public class NoOpEntitlementService : IEntitlementService
    {
        public Task<SyncGoodsResult> SyncGoods(CancellationToken? cancellationToken = null) { throw new NotImplementedException(); }
        public Task<SyncGoodsResult> SyncGoodsWithRetry(CancellationToken? cancellationToken = null) { throw new NotImplementedException(); }
    }


    public class NoOpEntitlementPlugin : IEntitlementPlugin
    {
        public Task<ManifestWithVersion> DownloadManifestAsync(string entitlementId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken)) { throw new NotImplementedException(); }
        public Task<InstallationRequirements> GetInstallationRequirements(string entitlementId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken)) { throw new NotImplementedException(); }

        public Task<IDictionary<string, string>> CurrentVersionIdsForAdgProductIds(IList<string> adgProductIds ) { throw new NotImplementedException(); }
    }

    class Program
    {
        static int Main(string[] args)
        {
            var spadeClient = new SpadeClient(new SpadeHttpClient());

            AppDataRemoverArgs appDataRemoverArgs = null;
            try
            {
                appDataRemoverArgs = new AppDataRemoverParser().Parse(args);
            }
            catch (InvalidCliException)
            {
                return (int)EUninstallResult.Failed_InvalidArgs;
            }

            IMetricsPlugin metrics = null;
            if (appDataRemoverArgs.DevMode)
            {
                metrics = new NullMetrics();
            }
            else
            {
                metrics = new MetricsPlugin(Constants.AmazonMetricsId, DateTime.UtcNow, spadeClient);
            }

            ParentProcessImpersonationPlugin impersonator = null;
            try
            {
                impersonator = new ParentProcessImpersonationPlugin();
            }
            catch (ImpersonationException e)
            {
                metrics.AddCounter("TwitchUninstaller", "ImpersonationException");

                // we don't absolutely need impersonation (only used for shortcut removal), just keep going w/o it
                impersonator = null;
            }

#if DEBUG
            if (appDataRemoverArgs.AttachDebugger)
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
                else
                {
                    Debugger.Launch();
                }
            }
#endif

            IOSDriver osDriver = null;
            if ( impersonator != null )
            {
                osDriver = new ImpersonatedWindowsFileSystemDriver(impersonator)
                {
                    IsDevMode = appDataRemoverArgs.DevMode
                };
            }
            else
            {
                osDriver = new WindowsFileSystemDriver()
                {
                    IsDevMode = appDataRemoverArgs.DevMode
                };
            }

            // Initialize curse logger
            Curse.Logging.LoggerConfig loggerConfig = new Curse.Logging.LoggerConfig(dir: osDriver.CurseLogDirectory, name: "Fuel");
            Curse.Logging.Logger.Init(loggerConfig);
            Curse.Logging.LogCategory curseLogger = new Curse.Logging.LogCategory("FuelAppDataRemover");

            // TODO : prefix logs
            var fuelLoggerConfig = new LoggerBaseConfig(osDriver, curseLogger, devMode: osDriver.IsDevMode, loggerPrefix: ""/*"uninstall-"*/);

            var baseContextServices = new BaseContextServices();

            var crash = new UncaughtExceptionHandler(
                metrics: metrics,
                attachExceptionTrackers: true,
                uploadMetricsAndData: !osDriver.IsDevMode);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(curseLogger).As<Curse.Logging.LogCategory>().SingleInstance();
            builder.RegisterInstance(osDriver).As<IOSDriver>().SingleInstance();
            builder.RegisterInstance(fuelLoggerConfig).As<ILoggerBaseConfig>().SingleInstance();
            baseContextServices.Build(builder);

            Log.Information("Application started with arguments", new { ProcessName = Process.GetCurrentProcess()?.ProcessName, Args = string.Join(",", args) });

            new BaseContext(crash, metrics).Build(builder);
            new PersistenceContext().Build(builder);
            new StorageContext().Build(builder);

            builder.RegisterType<AppConfigPlugin>().As<IAppConfigPlugin>().SingleInstance();
            builder.RegisterType<MessengerHub>().As<IMessengerHub>().SingleInstance();
            builder.RegisterType<ShortcutManager>().As<IShortcutManager>().SingleInstance();
            builder.RegisterType<WindowsShortcutDriver>().As<IShortcutDriver>().SingleInstance();
            builder.RegisterType<WindowsFileSystemDriver>()
                .As<IOSDriver>()
                .OnActivating(e => e.Instance.IsDevMode = osDriver.IsDevMode)
                .SingleInstance();
            builder.RegisterType<WindowsProcessLauncher>().As<IProcessLauncher>().SingleInstance();
            builder.RegisterType<LaunchUnelevatedServiceNoop>().As<IDeelevatedLaunchService>().SingleInstance();
            builder.RegisterType<JsonLaunchConfigProvider>().As<ILaunchConfigProvider>().SingleInstance();
            builder.RegisterType<WindowsServicesDriver>().As<IOSServicesDriver>().SingleInstance();

            // $ jbilas -- this shouldn't be necessry .. used by EntitlementsDatabase
            builder.RegisterType<NoOpEntitlementService>().As<IEntitlementService>().SingleInstance();
            // $ jbilas -- this shouldn't be necessry .. used by InstalledGamesPlugin
            builder.RegisterType<NoOpEntitlementPlugin>().As<IEntitlementPlugin>().SingleInstance();

            builder.RegisterType<WindowsRegisterInstallation>().As<IRegisterInstallation>().SingleInstance();
            builder.RegisterType<UninstallPlugin>().As<IUninstallPlugin>().SingleInstance();
            builder.RegisterType<DeferredDeletionService>().As<IDeferredDeletionService>().SingleInstance();
            
            builder.RegisterType<LuceneDbMigrator>().As<ILuceneDbMigrator>().SingleInstance();
            builder.Register(ctx =>
                    new ManifestStorage(ctx.Resolve<SystemPersistencePlugin>(), ctx.Resolve<IMetricsPlugin>()))
                .As<IManifestStorage>().SingleInstance();
            builder.RegisterType<UninstallController>().SingleInstance();

            IContainer container = null;

            try
            {
                Log.Information($"building container");
                container = builder.Build();
                Log.Information($"container build successful");

                foreach (var instance in container.Resolve<IEnumerable<IStartablePostContainerBuild>>())
                {
                    Log.Information($"Starting component: {instance}");
                    instance.StartPostContainerBuild();
                }
            }
            catch (Exception ex) // some issues can occur here from the IStartablePostContainerBuild plugins
            {
                // logging should be available (even if autofac failed), let's log the error
                Log.Fatal(ex, $"Exception hit during Autofac {ex.Message}");
                var dlg = new TaskDialog
                {
                    Caption = UIStrings.ERR_StartupFailureTitle,
                    InstructionText = UIStrings.ERR_StartupFailure,
                    Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Error,
                    StandardButtons = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Ok,
                };
                dlg.Show();
                container?.Resolve<IMessengerHub>()?.Publish(new FuelShutdownMessage(1000));
                throw;   // in case we aren't shut down
            }

            AppDomain.CurrentDomain.ProcessExit += (s, a) =>
            {
                Log.Information("Process exiting. Flushing metrics.");
                metrics.FlushQueue();
            };

            container.Resolve<IMessengerHub>().Subscribe<FuelShutdownMessage>((msg) =>
            {
                Log.Information("Fuel shutdown message received. Flushing metrics.");
                metrics.FlushQueue();
            });

            try
            {
                var languageCode = appDataRemoverArgs.LanguageCode ?? CultureInfo.CurrentUICulture.Name;
                UIStrings.Culture = CultureInfo.CreateSpecificCulture(languageCode);
            }
            catch (CultureNotFoundException)
            {
                Log.Information($"Invalid language code: {appDataRemoverArgs.LanguageCode}, defaulting to local machine UI culture");
            }

            metrics.AddCounter(MetricStrings.SOURCE_Mode, $"Uninstall:{appDataRemoverArgs.Mode}");

            // ensure that our system persistence root exists with all user permissions before we do anything
            container.Resolve<IOSDriver>().EnsureSystemDataPathExists();

            // migrate Lucene->Sqlite, if necessary
            container.Resolve<ILuceneDbMigrator>().TryEnsureDbsMigrated();

            container.Resolve<UninstallController>().Start(appDataRemoverArgs.Mode, new ProductId(appDataRemoverArgs.ProductId));

            return (int)EUninstallResult.Success;
        }
    }

    public class StorageContext
    {
        public void Build(ContainerBuilder builder)
        {
            // DAOs
            builder.RegisterType<TwitchUserStoragePlugin>().As<ITwitchUserStoragePlugin>().SingleInstance();
        }
    }
}
