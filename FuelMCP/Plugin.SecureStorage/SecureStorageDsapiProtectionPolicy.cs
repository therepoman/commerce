﻿using System;
using System.Security.Cryptography;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.SecureStorage
{
    public class SecureStorageDsapiProtectionPolicy : ISecureStorageProtectionPolicy
    {
        public byte[] ProtectBytes(Byte[] unprotectedBytes)
            => ProtectedData.Protect(
                unprotectedBytes,
                null,
                DataProtectionScope.CurrentUser);

        public byte[] UnprotectBytes(Byte[] protectedBytes)
            => ProtectedData.Unprotect(
                protectedBytes,
                null,
                DataProtectionScope.CurrentUser);
    }
}