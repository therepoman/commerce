﻿using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Logging.Events;
using Amazon.Fuel.Common.Utilities;
using Polly;

namespace Amazon.Fuel.Plugin.SecureStorage
{
    public class SecureSQLiteStorage : Disposable, ISecureStoragePlugin
    {
        // In the case of a DB schema change:
        // 1. Increment this
        // 2. Fix the Init() method to not set PRAGMA user_version=1 always when connecting
        // 3. Check current DB user_version and perform necessary migration steps
        // 4. Set PRAGMA user_version to new version once migrated
        private const int USER_VERSION = 1;
        private const string TABLE = "secure_storage";

        private const string METRIC_SOURCE = "SecureSQLiteStorage";

        private const int PUTALL_TIMEOUT_MS = 5 * 60 * 1000; // 5 minute timeout on puts
        private const int GET_TIMEOUT_MS = 5 * 60 * 1000; // 5 minute timeout on gets

        private const int DbOpFailureRetries = 3;
        private const int DbOpFailureWaitBaseMs = 300;
        private const int BusyTimeoutMs = 30000;
        private const double DbOpFailureWaitRamp = 1.5;

        private readonly object _initLock = new object();
        private readonly string _dbFile;
        private readonly IMetricsPlugin _metrics;
        private readonly ISecureStorageProtectionPolicy _protectionPolicy;
        private readonly string _dbDir;

        private bool _tablesCreated = false;

        public SecureSQLiteStorage(
            PersistencePlugin persistence, 
            IMetricsPlugin metrics, 
            ISecureStorageProtectionPolicy protectionPolicy,
            string dbSubdir = Constants.Paths.SqliteDatabaseSubDir, 
            string dbFilename = Constants.Paths.SqliteSecureStorageFilename)
        {
            _dbDir = Path.Combine(persistence.DataRoot(), dbSubdir);
            _dbFile = Path.Combine(_dbDir, dbFilename);

            _metrics = metrics;
            _protectionPolicy = protectionPolicy;
            _metrics?.AddDiscreteValue(METRIC_SOURCE, "DbInitialized", 0);
        }

        public SecureSQLiteStorage(string dbFile)
        {
            _dbFile = dbFile;
            _dbDir = Path.GetDirectoryName(_dbFile);
        }

        public bool Init()
        {
            try
            {
                if (!string.IsNullOrEmpty(_dbDir)
                    && !Directory.Exists(_dbDir))
                {
                    Directory.CreateDirectory(_dbDir);
                }
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "InitFailed");
                Log.Fatal(e, "Failed to create directory for database");
                throw new SecureStorageCriticalException("Failed to create directory for DB", e);
            }

            try
            {
                WrapRetryPolicy(EnsureDbTables);
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Failed to initialize secure storage db");
                _metrics?.AddCounter(METRIC_SOURCE, "InitFailed");
                throw new SecureStorageCriticalException("Failed to initialize database", e);
            }

            return true;
        }

        public string ConvertToSqlitePath(string path)
            => path.StartsWith(@"\\") ? $@"\\{path}" : path;

        private void EnsureDbTables()
        {
            if (!_tablesCreated)
            {
                // double-checked locking
                lock (_initLock)
                {
                    if (_tablesCreated) return;

                    _tablesCreated = true;

                    var sqlDbFile = ConvertToSqlitePath(_dbFile);
                    if (!File.Exists(_dbFile))
                    {
                        FuelLog.Info("Creating secure DB", new { dbFile=_dbFile, sqliteFile=sqlDbFile });
                        SQLiteConnection.CreateFile(sqlDbFile);
                    }

                    WrapRetryPolicy(() =>
                    {
                        using (var dbConn = CreateConnection(sqlDbFile))
                        {
                            CreateTables(dbConn);
                            _metrics?.AddDiscreteValue(METRIC_SOURCE, "DbInitialized", 1);
                        }
                    });
                }
            }
        }

        private SQLiteConnection CreateConnection(string sqlDbFile)
        {
            var ret = new SQLiteConnection($"Data Source={sqlDbFile};Version=3;PRAGMA user_version={USER_VERSION};PRAGMA busy_timeout={BusyTimeoutMs};");
            ret.Open();
            return ret;
        }

        // creates a single sqlite db table for secure_storage, housing the context/key to value (blob) pair
        private void CreateTables(SQLiteConnection dbConn)
        {
            using (var command = new SQLiteCommand(dbConn))
            {
                command.CommandText =
                    $@"CREATE TABLE IF NOT EXISTS {TABLE}(
                            context TEXT,
                            key TEXT,
                            value BLOB,
                            CONSTRAINT {TABLE}_ctx_key_idx UNIQUE (context, key) ON CONFLICT REPLACE
                    );";
                command.ExecuteNonQuery();
            }
        }

        public void Clear()
        {
            try
            {
                WrapRetryPolicy(() =>
                {
                    UsingMTSafeTransaction((dbConn, transaction) =>
                    {
                        //Start a transaction so we can batch send the inserts
                        using (new SQLiteCommand(
                            "delete from {TABLE}",
                            dbConn,
                            transaction)) { }
                    }, commitTransaction: true, timeoutMs: PUTALL_TIMEOUT_MS);
                }, allowDeleteAndRetry: false);
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "PutFailed");
                throw new SecureStorageException("Put failed", e);
            }
        }

        /// <summary>
        /// Creates a temporary DB connection and transaction (thread safety) and cleans up after call.
        /// NOTE: DB must be successfully initialized before making this call.
        /// </summary>
        /// <param name="timeoutMs">If non-zero, will timeout if the entire operation takes more than n milliseconds</param>
        private void UsingMTSafeTransaction(
            Action<SQLiteConnection, SQLiteTransaction> lambda, 
            bool commitTransaction,
            int timeoutMs = 0)
        {
            Stopwatch timeoutWatch = new Stopwatch();
            timeoutWatch.Start();

            // NOTE: you lose some performance in this operation. Consider pooling the connections if this
            // becomes an issue
            var tempDbConn = CreateConnection(ConvertToSqlitePath(_dbFile));
            SQLiteTransaction transaction = null;
            try
            {
                while (transaction == null)
                {
                    try
                    {
                        transaction = tempDbConn.BeginTransaction();
                    }
                    catch (SQLiteException ex)
                    {
                        if (ex.ResultCode != SQLiteErrorCode.Locked && ex.ResultCode != SQLiteErrorCode.Busy)
                        {
                            throw;
                        }

                        if (timeoutMs > 0 && timeoutWatch.Elapsed.Milliseconds >= timeoutMs)
                        {
                            FuelLog.Error(ex, "DB operation timeout");
                            _metrics?.AddCounter(METRIC_SOURCE, "DbTimeout");
                            throw new SecureStorageException($"Operation timeout after {timeoutMs} MS", ex);
                        }

                        // else wait for lock
                        Thread.Sleep(10 + new Random().Next() % 10);
                    }
                }

                lambda(tempDbConn, transaction);

                if (!commitTransaction) return;

                //Commit the inserts as a batch to the database.
                while (true)
                {
                    try
                    {
                        transaction.Commit();
                        break;
                    }
                    catch (SQLiteException ex)
                    {
                        if (ex.ResultCode != SQLiteErrorCode.Locked && ex.ResultCode != SQLiteErrorCode.Busy)
                        {
                            throw;
                        }

                        if (timeoutMs > 0 && timeoutWatch.Elapsed.Milliseconds >= timeoutMs)
                        {
                            _metrics?.AddCounter(METRIC_SOURCE, "DbTimeout");
                            FuelLog.Error("DB operation timeout");
                            throw new SecureStorageException($"Operation timeout after {timeoutMs} MS", null);
                        }

                        // else wait for lock
                        Thread.Sleep(10 + new Random().Next() % 10);
                    }
                }
            }
            finally
            {
                transaction?.Dispose();
                tempDbConn?.Close();
                tempDbConn?.Dispose();
            }
        }

        public string Get(string context, string key)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            try
            {
                string ret = null;
                WrapRetryPolicy(() =>
                {
                    UsingMTSafeTransaction((dbConn, transaction) =>
                    {
                        using (var command = new SQLiteCommand(
                            $"SELECT value FROM {TABLE} WHERE context = @context AND key = @key;",
                            dbConn,
                            transaction))
                        {
                            command.Parameters.AddWithValue("@context", context);
                            command.Parameters.AddWithValue("@key", key);

                            using (var reader = command.ExecuteReader())
                            {
                                if (!reader.Read())
                                {
                                    ret = null;
                                    return;
                                }

                                ret = Encoding.UTF8.GetString(_protectionPolicy.UnprotectBytes(GetBytes(reader)));
                            }
                        }
                    }, commitTransaction: false, timeoutMs: GET_TIMEOUT_MS);
                }, allowDeleteAndRetry: false);
                return ret;
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "GetFailed");
                throw new SecureStorageException("Get failed", e);
            }
        }

        public void PutAll(string context, IEnumerable<SecureStorageItem> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            try
            {
                WrapRetryPolicy(() =>
                {
                    UsingMTSafeTransaction((dbConn, transaction) =>
                    {
                        //Start a transaction so we can batch send the inserts
                        using (var command = new SQLiteCommand(
                            $"INSERT INTO {TABLE} (context,key,value) VALUES (@context, @key, @value);", dbConn,
                            transaction))
                        {
                            //re-using SQLiteCommand object because it was significantly more performant than initializing a new one every time.
                            foreach (var item in items)
                            {
                                //If there is no value to write, skip
                                if (item == null)
                                {
                                    continue;
                                }

                                //If value is null, remove the key
                                if (item.Value == null)
                                {
                                    RemoveInternal(context: context, key: item.Key, dbConn: dbConn);
                                    continue;
                                }

                                byte[] protectedData = _protectionPolicy.ProtectBytes(Encoding.UTF8.GetBytes(item.Value));
                                command.Parameters.AddWithValue("@context", context);
                                command.Parameters.AddWithValue("@key", item.Key);
                                command.Parameters.AddWithValue("@value", protectedData);
                                command.ExecuteNonQuery();
                            }
                        }
                    }, commitTransaction: true, timeoutMs: PUTALL_TIMEOUT_MS);
                }, allowDeleteAndRetry: false);
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "PutFailed");
                throw new SecureStorageException("Put failed", e);
            }
        }

        public void Put(string context, string key, string value)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            SecureStorageItem storageItem = new SecureStorageItem(key, value);
            PutAll(context, new[] {storageItem});
        }

        public bool Remove(string context, string key = null)
        {
            using (var dbConn = CreateConnection(ConvertToSqlitePath(_dbFile)))
            {
                return RemoveInternal(context: context, key: key, dbConn: dbConn);
            }
        }

        private bool RemoveInternal(string context, string key, SQLiteConnection dbConn)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            try
            {
                bool ret = false;
                WrapRetryPolicy(() =>
                {
                    using (var command = new SQLiteCommand(dbConn))
                    {
                        StringBuilder sql = new StringBuilder($"DELETE FROM {TABLE} WHERE context = @context");
                        command.Parameters.AddWithValue("@context", context);
                        if (key != null)
                        {
                            sql.Append(" AND key = @key");
                            command.Parameters.AddWithValue("@key", key);
                        }
                        sql.Append(";");
                        command.CommandText = sql.ToString();

                        ret = command.ExecuteNonQuery() > 0;
                    }
                }, allowDeleteAndRetry: false);
                return ret;
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "RemoveFailed");
                throw new SecureStorageException("remove failed", e);
            }
        }

        public void Wipe()
        {
            try
            {
                WrapRetryPolicy(() =>
                {
                    using (var dbConn = CreateConnection(ConvertToSqlitePath(_dbFile)))
                    using (var command = new SQLiteCommand($"DELETE FROM {TABLE};", dbConn))
                    {
                        command.ExecuteNonQuery();
                    }
                }, allowDeleteAndRetry: false);
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "WipeFailed");
                throw new SecureStorageException("wipe failed", e);
            }
        }

        private byte[] GetBytes(SQLiteDataReader reader)
        {
            const int CHUNK_SIZE = 2 * 1024;
            byte[] buffer = new byte[CHUNK_SIZE];
            long bytesRead;
            long fieldOffset = 0;
            using (MemoryStream stream = new MemoryStream())
            {
                while ((bytesRead = reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, (int)bytesRead);
                    fieldOffset += bytesRead;
                }
                return stream.ToArray();
            }
        }

        // will wipe out DB on critical failures, so that we can recreate and resume the session
        private void WrapRetryPolicy(Action innerDbAction, bool allowDeleteAndRetry = true)
        {
            try
            {
                Policy
                    .Handle<SecureStorageException>()
                    .WaitAndRetry(
                        retryCount: DbOpFailureRetries,
                        sleepDurationProvider: retryAttempt =>
                            TimeSpan.FromMilliseconds(DbOpFailureWaitBaseMs *
                                                      Math.Pow(retryAttempt, DbOpFailureWaitRamp)),
                        onRetry: (e, ts) =>
                        {
                            FuelLog.Error(e, $"Exception hit during action in secure DB, retrying",
                                new {waitSec = ts.Seconds});
                            _metrics.AddCounter(METRIC_SOURCE, $"SecureDbRetryable{e.GetType().Name}");
                        })
                    .Execute(innerDbAction);
            }
            catch (Exception e)
            {
                FuelLog.Error(e, $"Exception hit during action in secure DB, data may have been lost", new {allowRetry = allowDeleteAndRetry});
                _metrics.AddCounter(METRIC_SOURCE, $"SecureDbNotRetryable{e.GetType().Name}");

                Log.Write(CrashReportEvent.Create(Constants.CrashScope.App, (allowDeleteAndRetry) ? Constants.CrashType.Error : Constants.CrashType.Fault, e, $"Unknown Secure storage exception accessing sqlite, {e.Message} " + (allowDeleteAndRetry ? "" : "(unrecoverable)")));

                if (allowDeleteAndRetry
                    && ForceErase())
                {
                    EnsureDbTables();
                    _metrics.AddCounter(METRIC_SOURCE, $"SecureDbFailureWiped");
                    WrapRetryPolicy(innerDbAction, allowDeleteAndRetry: false);
                }
                else
                {
                    _metrics.AddCounter(METRIC_SOURCE, $"SecureDbFailureFatal");
                    // can't handle
                    throw new SecureStorageCriticalException("Failed to access secure storage DB", e);
                }
            }
        }

        private bool ForceErase()
        {
            // TODO : https://jira.twitch.com/browse/FLCL-1518 surface error to user
            FuelLog.Warn("Requesting delete secure storage database", new { name = _dbFile });
            _metrics.AddCounter(METRIC_SOURCE, "ForceErase");

            try
            {
                if (File.Exists(_dbFile))
                {
                    bool fileExists = File.Exists(_dbFile);
                    FuelLog.Info("Deleting secure storage DB, if exist", new { fileExists });
                    if (fileExists)
                    {
                        FuelLog.Info("Deleting file (via FS)", new { name = _dbFile });
                        File.Delete(_dbFile);
                    }
                }
                else
                {
                    FuelLog.Warn("Couldn't find file for deletion", new { name = _dbFile });
                    _metrics.AddCounter(METRIC_SOURCE, "ForceEraseNotFound");
                    // NOTE: no point in retrying if we have no file found to recreate
                    return false;
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Couldn't delete database (no permissions or locked?)");
                _metrics.AddDiscreteValue(METRIC_SOURCE, "ForceEraseFail", 1);
                return false;
            }

            FuelLog.Info("Secure storage DB deleted successfully");
            _metrics.AddDiscreteValue(METRIC_SOURCE, "ForceEraseFail", 0);
            return true;
        }

        protected override void Dispose(bool disposing)
        {
        }
    }
}
