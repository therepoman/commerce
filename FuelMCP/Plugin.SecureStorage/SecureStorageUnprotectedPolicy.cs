﻿using System;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.SecureStorage
{
    public class SecureStorageUnprotectedPolicy : ISecureStorageProtectionPolicy
    {
        public byte[] ProtectBytes(Byte[] unprotectedBytes) => unprotectedBytes;
        public byte[] UnprotectBytes(Byte[] protectedBytes) => protectedBytes;
    }
}