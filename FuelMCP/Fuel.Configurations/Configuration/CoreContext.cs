﻿using System;
using System.IO;
using System.Net.Http;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Configuration;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Plugin.Download;
using Amazon.Fuel.Plugin.Entitlement;
using Amazon.Fuel.Plugin.Entitlement.Services;
using Amazon.Fuel.Plugin.Launcher.Models;
using Amazon.Fuel.Plugin.Library;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.Plugin.SecureStorage;
using Amazon.Fuel.Plugin.TwitchUserStorage;
using Autofac;
using Clients.Kraken;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Launcher;
using Amazon.Fuel.Plugin.TwitchLogin;
using Amazon.Fuel.Plugin.Uninstall;
using Amazon.Fuel.Coral.Client;
using Amazon.Fuel.SoftwareDistributionService.Client;
using Serilog;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Plugin.InstalledGames;
using IAppInfo = Ext.Auth.Map.IAppInfo;

namespace Amazon.Fuel.Configurations.Configuration
{
    // NOTE: keep this as minimal as possible, if we crash in here we can't log it
    public class BaseContextServices
    {
        public const int LogRollSeconds = 60 * 30;    // 30 minutes
        public const string LogFormat = "{Timestamp:G} [{Level:u3}-T{ThreadId}] {Message}{NewLine}{Exception}";

        public readonly Clock _clock;
        public readonly FileSystemUtilities _fs;

        public BaseContextServices()
        {
            // initialize all top level dependency components
            _clock = new Clock();
            _fs = new FileSystemUtilities();
        }

        public void Build(ContainerBuilder builder)
        {
            builder.RegisterInstance(_clock).As<IClock>().SingleInstance();
            builder.RegisterInstance(_fs).As<IFileSystemUtilities>().SingleInstance();
        }
    }

    public class BaseContext
    {
        private IUncaughtExceptionHandler _crash;
        private IMetricsPlugin _metrics;

        public BaseContext(IUncaughtExceptionHandler crash, IMetricsPlugin metrics)
        {
            _crash = crash;
            _metrics = metrics;
        }

        public void Build(ContainerBuilder builder)
        {
            // Other
            builder.RegisterType<AppInfo>().AsSelf().As<IAppInfo>().As<IAppInfo>().SingleInstance();
            builder.RegisterType<JsonLaunchConfigProvider>().As<ILaunchConfigProvider>().SingleInstance();
            builder.RegisterInstance(_crash).As<IUncaughtExceptionHandler>().SingleInstance();
            builder.RegisterInstance(_metrics).As<IMetricsPlugin>().SingleInstance();
        }
    }

    public class CoreContext
    {
        public void Build(
            ContainerBuilder builder,
            Action<object> loggerCfgBuilderCallback = null)
        {
            // Core plugins
            builder.RegisterType<DownloadPlugin>().As<IDownloadPlugin>().SingleInstance();
            builder.RegisterType<EntitlementPlugin>().As<IEntitlementPlugin>().SingleInstance();
            builder.RegisterType<FuelGameInstallerService>().As<IFuelGameInstallerService>().SingleInstance();
            builder.RegisterType<LauncherServices>().As<ILauncherServices>().SingleInstance();
            builder.RegisterType<LibraryPlugin>().As<ILibraryPlugin>().SingleInstance();
            builder.RegisterType<GameInstaller>().As<IGameInstaller>().SingleInstance();
            builder.RegisterType<GamePrereqInstaller>().As<IGamePrereqInstaller>().SingleInstance();
            builder.RegisterType<UninstallPlugin>().As<IUninstallPlugin>().SingleInstance();
            builder.RegisterType<DeferredDeletionService>().As<IDeferredDeletionService>().SingleInstance();
            builder.RegisterType<MessengerHub>().As<IMessengerHub>().SingleInstance();
            builder.RegisterType<TwitchLoginPlugin>().As<ITwitchLoginPlugin>().As<ITwitchTokenProvider>().SingleInstance();
            builder.RegisterType<DefaultCurseTokenProvider>().As<ICurseTokenProvider>().SingleInstance();
            builder.RegisterType<ShortcutManager>().As<IShortcutManager>().SingleInstance();
            builder.RegisterType<InstallHelperIpcController>().As<IInstallHelperIpcController>().SingleInstance();
        }
    }

    public class PersistenceContext
    {
        public void Build(ContainerBuilder builder)
        {
            builder.RegisterType<UserPersistencePlugin>().As<IPersistence>().AsSelf().SingleInstance();
            builder.RegisterType<SystemPersistencePlugin>().As<IPersistence>().AsSelf().SingleInstance();
            builder.RegisterType<LuceneSystemPersistencePlugin>().As<IPersistence>().AsSelf().SingleInstance();
            builder.RegisterType<HashCache>().As<IHashCache>().AsSelf().SingleInstance();

            builder.Register((c, p) => new SecureSQLiteStorage(
                    c.Resolve<UserPersistencePlugin>(),
                    c.Resolve<IMetricsPlugin>(),
                    new SecureStorageDsapiProtectionPolicy()))
                .OnActivating(e => e.Instance.Init())
                .As<SecureSQLiteStorage>()
                .As<ISecureStoragePlugin>()
                .SingleInstance();
            builder.Register((c, p) => new GameProductInfoDatabase(c.Resolve<UserPersistencePlugin>(),
                                                               c.Resolve<IMetricsPlugin>(),
                                                               c.Resolve<IEntitlementService>(), 
                                                               c.Resolve<IMessengerHub>(),
                                                               c.Resolve<IOSDriver>()))
                .As<IEntitlementsDatabase>()
                .SingleInstance();

            builder.Register((c, p) => 
                    new GameInstallInfoDatabase(
                        c.Resolve<SystemPersistencePlugin>(), 
                        c.Resolve<IMessengerHub>(), 
                        c.Resolve<IMetricsPlugin>(),
                        c.Resolve<IOSDriver>()))
                .As<IGameInstallInfoDatabase>()
                .SingleInstance();

            builder.RegisterType<InstalledGameValidator>().As<IInstalledGameValidator>().SingleInstance();
            builder.RegisterType<InstalledGameSynchronizer>().As<IInstalledGameSynchronizer>().SingleInstance();
            builder.RegisterType<InstalledGamesPlugin>().As<IInstalledGamesPlugin>().SingleInstance();
        }
    }

    public class WindowsContext
    {
        public void Build(ContainerBuilder builder, bool includeLaunchUnelevatedService, bool devMode, string appDataOverride = null)
        {
            builder.RegisterType<WindowsProcessLauncher>().As<IProcessLauncher>().SingleInstance();
            if (includeLaunchUnelevatedService)
            { 
                builder.RegisterType<LaunchUnelevatedServiceNoop>().As<IDeelevatedLaunchService>().SingleInstance();
            }
            builder.RegisterType<WindowsFileSystemDriver>()
                .As<IOSDriver>()
                .OnActivating(e =>
                {
                    e.Instance.IsDevMode = devMode;
                    e.Instance.ApplicationDataOverride = appDataOverride;
                })
                .SingleInstance();
            builder.RegisterType<WindowsShortcutDriver>().As<IShortcutDriver>().SingleInstance();
            builder.RegisterType<WindowsRegisterInstallation>().As<IRegisterInstallation>().SingleInstance();
            builder.RegisterType<WindowsServicesDriver>().As<IOSServicesDriver>().SingleInstance();
        }
    }

    public class ClientsContext
    {
        public void Build(
            ContainerBuilder builder, 
            string twitch, 
            string clientid,
            int[] retrySeconds)
        {
            AppInfo info = new AppInfo();
            builder.RegisterType<AmazonEntitlementService>().As<IEntitlementService>().InstancePerDependency();
            builder.RegisterType<CoralClientFactory>().As<CoralClientFactory>().InstancePerDependency();
            builder.RegisterType<SoftwareDistributionServiceClient>()
                .As<ISoftwareDistributionServiceClient>()
                .WithParameter("serviceUrl", SoftwareDistributionServiceClient.ProdUrl)
                .WithParameter("useragent", $"{info.SpectatorName}/{info.Version}")
                .InstancePerDependency();
            builder.RegisterType<ReloggableKrakenClient>()
                .As<KrakenClient>()
                .WithParameter("endpoint", $"{twitch}/kraken")
                .WithParameter("clientid", clientid).SingleInstance();
            builder.RegisterType<ReloggableIdentityClient>()
                .As<IdentityClient>()
                .WithParameter("endpoint", $"{twitch}")
                .WithParameter("clientid", clientid).SingleInstance();
            builder.RegisterType<HttpClientHandler>()
                .As<HttpMessageHandler>()
                .InstancePerDependency();
        }
    }

    public class StorageContext
    {
        public void Build(ContainerBuilder builder)
        {
            builder.RegisterType<LuceneDbMigrator>().As<ILuceneDbMigrator>().SingleInstance();
            builder.Register(ctx =>
                    new ManifestStorage(ctx.Resolve<SystemPersistencePlugin>(), ctx.Resolve<IMetricsPlugin>()))
                .As<IManifestStorage>().SingleInstance();

            // DAOs
            builder.RegisterType<TwitchUserStoragePlugin>().As<ITwitchUserStoragePlugin>().SingleInstance();
        }
    }
}
