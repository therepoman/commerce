﻿using Amazon.Fuel.Common.Contracts;
using Clients.Twitch.Spade.Model;

namespace Amazon.Fuel.Configurations.Configuration
{
    public class NullMetricTimerDataPoint : IMetricTimerDataPoint
    {
        public void Dispose() { }
        public void Stop() { }
    }

    public class NullMetricScope : IMetricScope
    {
        public void IncrementCounter(string name) { }
        public void SetCounterValue(string name, int value) { }
        public void AddCounterValue(string name, int value) { }

        public void Dispose() { }

        public IMetricTimerDataPoint StartTimer(string name)
        {
            return new NullMetricTimerDataPoint();
        }
    }

    public class NullMetrics : IMetricsPlugin
    {
        public void Dispose() { }
        public void FlushQueue() { }
        public void AddCounter(string source, string name, string pivotKey, string pivotValue) { }
        public void AddCounter(string source, string name) { }
        public void AddDiscreteValue(string source, string name, int value, string pivotKey, string pivotValue) { }
        public void AddDiscreteValue(string source, string name, int value) { }
        public void AddTimerValue(string source, string name, double value) { }
        public void AddAppStartTimer(string source, string name, string pivotKey, string pivotValue) { }
        public void AddAppStartTimer(string source, string name) { }
        public IMetricsTimer StartTimer(string source, string name)
        {
            return new NullMetricsTimer();
        }

        public void SendSpadeEvent(ISpadeEvent spadeEvent) { }

        public IMetricScope CreateScope(string source)
        {
            return new NullMetricScope();
        }
    }

    public class NullMetricsTimer : IMetricsTimer
    {
        public void Stop() { }
        public void Dispose() { }
    }
}