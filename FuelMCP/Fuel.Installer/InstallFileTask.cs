﻿using Fuel.SoftwareDistributionService.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fuel.Installer
{
    internal class InstallFileJob
    {
        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly string _sourceHash;
        private readonly string _targetHash;
        private readonly IProgress<int> _progress;

        public InstallFileJob(ISoftwareDistributionServiceClient sds, string sourceHash, string targetHash, IProgress<int> progress)
        {
            _sds = sds;
            _sourceHash = sourceHash;
            _targetHash = targetHash;
            _progress = progress;
        }

        public async Task Begin()
        {
            
        }
    }
}
