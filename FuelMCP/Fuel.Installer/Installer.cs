﻿using Amazon.MCP.Common.Contracts;
using Fuel.SoftwareDistributionService.Client;
using Fuel.SoftwareDistributionService.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tv.Twitch.Fuel.Manifest;

using Sds = Tv.Twitch.Fuel.Sds;

namespace Fuel.Installer
{
    public interface IInstaller
    {
        Task<InstallProgress> InstallOrUpdate(string adgProductId,
            string libraryPath,
            IProgress<InstallProgress> progress,
            CancellationToken cancellation = default(CancellationToken));

        Task<InstallProgress> InstallPrerequisites(string adgProductId,
            IProgress<InstallProgress> progress,
            CancellationToken cancellation = default(CancellationToken));
    }

    public class InstallProgress
    {
        public int Percentage { get; internal set; }
        public string Message { get; internal set; }
    }

    public class FuelInstaller : IInstaller
    {
        private readonly IManifestStorage _manifests;
        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly IManifestComparator _manifestComparator;

        public FuelInstaller(IManifestStorage manifestStorage, ISoftwareDistributionServiceClient sds, IManifestComparator manifestComparator)
        {
            _manifests = manifestStorage;
            _sds = sds;
            _manifestComparator = manifestComparator;
        }

        public async Task<InstallProgress> InstallOrUpdate(string adgProductId,
            string libraryPath,
            IProgress<InstallProgress> progress,
            CancellationToken cancellation = default(CancellationToken))
        {
            // Retrieve current target version id
            string targetVersion = await GetTargetVersionId(adgProductId, cancellation);

            // Retrieve manifests (from cache, or service if necessary...)
            Sds.Manifest sourceManifest = null; // TODO actually need to look this up if update
            Sds.Manifest targetManifest = await _manifests.Get(new ProductId(adgProductId), targetVersion);

            // If update, compare manifests or new manifest against local copy.
            ManifestComparison manifestComparison = _manifestComparator.Compare(sourceManifest, targetManifest);

            // Build list of patches needed

            // Request patches in batches from service

            // Download and apply patches

            // Done

            throw new NotImplementedException();
        }

        public async Task<InstallProgress> InstallPrerequisites(string adgProductId,
            IProgress<InstallProgress> progress,
            CancellationToken cancellation = default(CancellationToken))
        {
            // Retrieve manifest and verify signature
            //Sds.Manifest manifest = await _manifests.Get(adgProductId);

            // Verify fuel.json

            // Parse prerequests

            // Install necessary prereqs

            // Done

            throw new NotImplementedException();
        }

        private async Task<string> GetTargetVersionId(string adgProductId, CancellationToken cancellation)
        {
            // This should hit a cache instead of just hitting the service all the time
            GetVersionsRequest request = new GetVersionsRequest(new string[] { adgProductId });
            GetVersionsResult result = await _sds.GetVersions(request, cancellation);

            if (result.errors.Count > 0)
            {
                // TODO Translate result.Errors[0] into an exception, throw
            }

            return result.versions[0].versionId;
        }
    }
}
