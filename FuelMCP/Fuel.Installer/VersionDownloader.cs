﻿using Amazon.MCP.Common.Utilities;
using Fuel.SoftwareDistributionService.Client;
using Fuel.SoftwareDistributionService.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

using Sds = Tv.Twitch.Fuel.Sds;

namespace Fuel.Installer
{
    public class DownloadProgress
    {
        public long TotalDownload { get; internal set; }

        public long Completed { get; internal set; }
    }

    public class VersionDownloader
    {
        private static readonly IList<DeltaEncoding> SUPPORTED_ENCODINGS = new ReadOnlyCollection<DeltaEncoding>(new List<DeltaEncoding>
        {
            DeltaEncoding.NONE
        });
        // TODO what is the correct batch size? Do we want some sort of server-side config for this?
        private const int GET_PATCHES_BATCH_SIZE = 25;

        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly string _adgGoodId;
        private readonly string _channelId;
        private readonly string _versionId;
        private readonly string _installPath;
        private readonly Sds.Manifest _targetManifest;
        private readonly Sds.Manifest _existingManifest;
        private readonly string _tempDir;
        private readonly IProgress<DownloadProgress> _progress;
        private readonly CancellationToken _cancellationToken;

        // For progress reporting
        private object _progressLock = new object();
        private long _totalDownload = 0L;
        private long _completedSoFar = 0L;

        /// <summary>
        /// Create a new version downloader
        /// </summary>
        /// <param name="installPath">root the completed files should be placed in</param>
        /// <param name="targetManifest">the target version's manifest (what to download)</param>
        /// <param name="existingManifest">the manifest of any existing files</param>
        /// <param name="tempDir">where to store working temp files</param>
        public VersionDownloader(ISoftwareDistributionServiceClient sds,
            string adgGoodId,
            string channelId,
            string versionId,
            string installPath, 
            Sds.Manifest targetManifest,
            IProgress<DownloadProgress> progress = null,
            CancellationToken cancellationToken = default(CancellationToken),
            Sds.Manifest existingManifest = null,
            string tempDir = null)
        {
            _sds = sds;
            _adgGoodId = adgGoodId;
            _channelId = channelId;
            _versionId = versionId;
            _installPath = installPath;
            _targetManifest = targetManifest;
            _existingManifest = existingManifest;
            _tempDir = tempDir ?? MakeRandomTempDir();
            _progress = progress;
            _cancellationToken = cancellationToken;
        }

        public void Begin()
        {
            // This will compute the source hash if the file exists and we are missing it
            var computeSourceHashesBlock = new TransformBlock<InstallFileTask, InstallFileTask>(
                installFileTask =>
                {
                    if (installFileTask.SourceHash != null || !File.Exists(installFileTask.FilePath))
                    {
                        return installFileTask; // no work to be done
                    }

                    try
                    {
                        string sha256hash = ShaUtil.ComputeHash(installFileTask.FilePath);
                        installFileTask.SourceHash = sha256hash;
                    }
                    catch (Exception)
                    {
                        // TODO handle exceptions
                        throw;
                    }

                    return installFileTask;
                });

            var batchGetPatch = new BatchBlock<InstallFileTask>(GET_PATCHES_BATCH_SIZE);

            // Take the batches and get the signed URLs
            var getPatchesBlock = new TransformManyBlock<InstallFileTask[], DownloadPatchFileTask>(
                async installFileTasks =>
                {
                    try
                    {
                        return await GetPatches(installFileTasks);
                    }
                    catch (Exception)
                    {
                        throw;
                        // TODO handle exceptions
                        //return new ConcurrentQueue<DownloadPatchFileTask>();
                    }
                });

            // Downloads a file using one of the provided sources
            var downloadFilesBlock = new TransformBlock<DownloadPatchFileTask, ApplyPatchFileTask>(
                downloadTask =>
                {
                    try
                    {
                        return DownloadPatch(downloadTask);
                    }
                    catch(Exception)
                    {
                        throw;
                        // TODO handle exceptions
                        //return null;
                    }
                });

            var applyPatchBlock = new ActionBlock<ApplyPatchFileTask>(
                applyPatchTask =>
                {
                    try
                    {
                        if (applyPatchTask.PatchType == DeltaEncoding.NONE)
                        {
                            // Move the file into place.
                            File.Move(applyPatchTask.PatchPath, applyPatchTask.FilePath);
                        }
                        else
                        {
                            // TODO support patching
                            throw new NotImplementedException();
                        }
                    }
                    catch (Exception)
                    {
                        // TODO exception handling
                        throw;
                    }
                });

            // Link all the blocks
            computeSourceHashesBlock.LinkTo(batchGetPatch,
                // If the files haven't changed we won't pass the task along along
                installFileTask => !installFileTask.TargetHash.Equals(installFileTask.SourceHash));
            batchGetPatch.LinkTo(getPatchesBlock);
            getPatchesBlock.LinkTo(downloadFilesBlock);
            downloadFilesBlock.LinkTo(applyPatchBlock);

            // Create completion tasks https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/walkthrough-creating-a-dataflow-pipeline
            computeSourceHashesBlock.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock)batchGetPatch).Fault(t.Exception);
                else batchGetPatch.Complete();
            });
            batchGetPatch.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock)getPatchesBlock).Fault(t.Exception);
                else getPatchesBlock.Complete();
            });
            getPatchesBlock.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock)downloadFilesBlock).Fault(t.Exception);
                else downloadFilesBlock.Complete();
            });
            downloadFilesBlock.Completion.ContinueWith(t =>
            {
                if (t.IsFaulted) ((IDataflowBlock)applyPatchBlock).Fault(t.Exception);
                else applyPatchBlock.Complete();
            });

            PostInstallFileTasks(computeSourceHashesBlock);

            computeSourceHashesBlock.Complete();

            applyPatchBlock.Completion.Wait();
        }

        private async Task<IEnumerable<DownloadPatchFileTask>> GetPatches(InstallFileTask[] installFileTasks, CancellationToken cancellationToken = default(CancellationToken))
        {
            IEnumerable<HashPair> hashes = installFileTasks.Select(ift => new HashPair
            {
                sourceHash = new Hash {
                    value = ift.SourceHash,
                    algorithm = HashAlgorithm.SHA256
                },
                targetHash = new Hash
                {
                    value = ift.TargetHash,
                    algorithm = HashAlgorithm.SHA256
                }
            });
            GetPatchesRequest request = new GetPatchesRequest(
                channelId: _channelId,
                versionId: _versionId,
                fileHashes: hashes,
                deltaEncodings: SUPPORTED_ENCODINGS,
                adgGoodId: _adgGoodId
            );

            GetPatchesResponse response = await _sds.GetPatches(request, cancellationToken);

            return response.patches.Select(sdsPatch => new DownloadPatchFileTask
            {
                PatchHash = sdsPatch.hash.value,
                // We could create a dictionary but if batches are small not sure the object creation is worth it?
                SourceHash = installFileTasks.First(ift => sdsPatch.hash.value.Equals(ift.TargetHash)).SourceHash,
                TargetHash = sdsPatch.hash.value, // TODO this is actually the patches hash, but we only support original files so same thing
                FilePath = installFileTasks.First(ift => sdsPatch.hash.value.Equals(ift.TargetHash)).FilePath,
                PatchType = sdsPatch.type,
                PatchSources = sdsPatch.downloadUrls
            });
        }

        private ApplyPatchFileTask DownloadPatch(DownloadPatchFileTask downloadTask)
        {
            string patchPath = Path.Combine(_installPath, Path.GetRandomFileName(), ".fuelpatch");
            using (var client = new WebClient())
            {
                // Need to handle errors and retries and alternate sources....so much TODO.
                client.DownloadFile(downloadTask.PatchSources.First(), patchPath);

                // Verify patches hash
                string actualPatchHash = ShaUtil.ComputeHash(patchPath);
                if (!actualPatchHash.Equals(downloadTask.PatchHash)) throw new Exception("Uh-oh! patch verification failed bro!");
            }

            // TODO progress reporting

            return new ApplyPatchFileTask
            {
                SourceHash = downloadTask.SourceHash,
                TargetHash = downloadTask.TargetHash,
                PatchPath = patchPath,
                PatchType = downloadTask.PatchType,
                FilePath = downloadTask.FilePath,
            };
        }

        private void PostInstallFileTasks(ITargetBlock<InstallFileTask> target)
        {
            // Today we only support a single package
            IDictionary<string, Sds.File> existingFilesByPath;
            if (_existingManifest == null)
            {
                existingFilesByPath = new Dictionary<string, Sds.File>();
            }
            else
            {
                Sds.Package existingPackage = _existingManifest.Packages[0];
                existingFilesByPath = existingPackage.Files.ToDictionary(f => f.Path);
            }

            Sds.Package targetPackage = _targetManifest.Packages[0];
            IDictionary<string, Sds.File> targetFilesByPath = targetPackage.Files.ToDictionary(f => f.Path);

            // Post target files. We don't care if the files haven't changed as the pipeline will handle these
            targetFilesByPath
                .Select(e =>
                {
                    Sds.File existingFile = existingFilesByPath.ContainsKey(e.Key) ? existingFilesByPath[e.Key] : null;
                    Sds.File targetFile = e.Value;

                    lock (_progressLock)
                    {
                        _totalDownload += targetFile.Size;
                    }

                    return new InstallFileTask
                    {
                        SourceHash = existingFile != null ? GetSha256HashString(existingFile.Hash) : null,
                        TargetHash = GetSha256HashString(targetFile.Hash),
                        FilePath = Path.Combine(_installPath, targetFile.Path)
                    };
                }).ToList().ForEach(ift => target.Post(ift));

            // TODO We need to handle removed files as well...
            //existingFilesByPath
            //    .Where(e => !targetFilesByPath.ContainsKey(e.Key))
            //    .Select(e => e.Value);
        }

        private string GetSha256HashString(Sds.Hash hash)
        {
            string hashString = BitConverter.ToString(hash.Value.Skip(hash.Value.Length - 32).ToArray()).Replace("-", "").ToLower();

            hashString = hashString.Length == 62 ? "00" + hashString : hashString;

            if (hashString.Length != 64)
            {
                throw new Exception("fuck");
            }

            return hashString;
        }

        private string MakeRandomTempDir()
        {
            string randomTempPath = Path.Combine(_installPath, Path.GetRandomFileName(), ".fuelinstall.tmp");
            DirectoryInfo dirInfo = Directory.CreateDirectory(randomTempPath);
            dirInfo.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            return randomTempPath;
        }
    }

    class InstallFileTask
    {
        public string SourceHash { get; set; }
        public string TargetHash { get; set; }
        public string FilePath { get; set; }
    }

    class DownloadPatchFileTask
    {
        public string SourceHash { get; set; }
        public string TargetHash { get; set; }
        public string PatchHash { get; set; }
        public string FilePath { get; set; }
        public DeltaEncoding PatchType { get; set; }
        public IEnumerable<string> PatchSources { get; set; }
    }

    class ApplyPatchFileTask
    {
        public string SourceHash { get; set; }
        public string TargetHash { get; set; }
        public string FilePath { get; set; }
        public string PatchPath { get; set; }
        public DeltaEncoding PatchType { get; set; }
    }
}
