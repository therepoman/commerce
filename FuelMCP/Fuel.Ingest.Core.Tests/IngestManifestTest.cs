﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fuel.Ingest.Core.Models;
using Fuel.Ingest.Core.Tests.Utilities;
using Xunit;

namespace Fuel.Ingest.Core.Tests
{
    public class IngestManifestTest
    {
        private readonly string _gameDir = TestData.GetPathToTestResource("TestData", "ValidGame");
        private readonly List<string> _filePaths;
        private readonly UploadManifest _uploadManifest;
        private readonly IngestManifest _ingestManifest;
        private readonly string _s3Bucket = "daf84722-e9e2-40f4-abe1-6029251353bf";
        public IngestManifestTest()
        {
            _filePaths = new FileScanner().ScanProductFiles(_gameDir);
            var uploadManifestFiles = _filePaths.Select(filePath =>
            {
                var relativePath = filePath.Replace(_gameDir + "\\", String.Empty);
                return new UploadManifestFile(relativePath, _s3Bucket, null);
            }).ToList();
            _uploadManifest = new UploadManifest(uploadManifestFiles);
            _ingestManifest = new IngestManifest(_uploadManifest, "random-version-id-123456", _gameDir);
        }
        [Fact]
        public void ToXml_ShouldReturnExpectedXml()
        {
            string expected = File.ReadAllText(TestData.GetPathToTestResource("TestData", "test-ingest-manifest.xml"));
            string actual = _ingestManifest.ToXml();
            Assert.Equal(expected, actual);
        }
    }
}
