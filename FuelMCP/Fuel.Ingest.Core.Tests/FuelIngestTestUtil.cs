﻿using Castle.Core.Internal;
using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Fuel.Ingest.Core.Tests
{
    public class FuelIngestTestUtil
    {
        /// <summary>
        /// Creates an array of command line args for the given object which has Properties annotated with the [Option] Attribute
        /// </summary>
        /// <param name="argsList">objects which have Properties annotated with the [Option] Attribute</param>
        /// <returns>array of command line args e.g. ["-p", "TestProductId", ...]</returns>
        public static string[] ToCommandLineArgs(params object[] argsList)
        {
            var cmdArgs = new List<string>();
            argsList.ForEach(args =>
            {
                //Iterate through all the properties of IngestVersionArgs which have the [Option] attribute 
                //and add the flag + value for every non-null property.
                args.GetType().GetProperties().Where(prop => prop.HasAttribute<OptionAttribute>()).ForEach(
                    prop =>
                    {
                        var val = Convert.ToString(prop.GetValue(args));
                        if (string.IsNullOrEmpty(val)) return;
                        var option = prop.GetCustomAttribute<OptionAttribute>();
                        cmdArgs.Add("-" + option.ShortName); //-p
                        cmdArgs.Add(val); //TestProductId
                    });
            });
            return cmdArgs.ToArray(); // ["-p", "TestProductId", ...]
        }
    }
}
