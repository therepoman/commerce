﻿using System.Collections.Generic;
using Fuel.Ingest.Core;
using Moq;
using Xunit;

namespace Amazon.MCP.Test.Main.Fuel.Ingest
{
    public class IngestAppTest
    {
        [Fact]
        public void NoCommandsDoesNothing()
        {
            var result = new IngestApp(new List<ICommand>()).Exec(new string[] {});
            Assert.NotNull(result);
            Assert.Equal(1, result.ExitCode);
            Assert.Equal("Must provide a command", result.ErrorMessage);
        }

        [Fact]
        public void NoMappedCommand()
        {
            var result = new IngestApp(new List<ICommand>()).Exec(new string[] { "command" });
            Assert.NotNull(result);
            Assert.Equal(1, result.ExitCode);
            Assert.Equal("No such command: command", result.ErrorMessage);
        }

        [Fact]
        public void InvokeCommand()
        {
            var command = new Mock<ICommand>();
            command.SetupGet(c => c.Command).Returns("command");
            command.Setup(c => c.Exec(new string[] {})).Returns(InvokeResult.Success);

            var result = new IngestApp(new List<ICommand>() {command.Object}).Exec(new string[] { "command" });

            Assert.Equal(InvokeResult.Success, result);
        }
    }
}