﻿using System.Collections.Generic;
using System.IO;

namespace Fuel.Ingest.Core.Tests.Utilities
{
    class TestData
    {
        public static string ReadTestFile(string file)
        {
            file = Path.Combine(TestDataDir(), file);
            return File.ReadAllText(file);
        }

        public static string TestDataDir()
        {
            return Path.GetFullPath(Path.Combine("..\\.."));
        }

        public static string GetPathToTestResource(params string[] paths)
        {
            var targetPaths = new List<string>() { TestDataDir() };
            targetPaths.AddRange(paths);
            return Path.Combine(targetPaths.ToArray());
        }
    }
}