﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.MCP.Common.Contracts;
using Fuel.SoftwareDistributionAdminService.Models;
using Ploeh.AutoFixture;
using Fuel.Ingest.Core.Models;

namespace Fuel.Ingest.Core.Tests.Utilities
{
    public class Factories
    {
        private static Fixture _instance;

        public static Fixture Fixture => _instance ?? (_instance = new Fixture());
        

        public static string ValidOauth()
        {
            return Fixture.Create("oauth");
        }

        public static int ValidMaxThreads()
        {
            return Fixture.Create<int>();
        }

        public static ProductId ValidProductId()
        {
            return Fixture.Create<ProductId>();
        }

        public static string ValidVersionId()
        {
            return Fixture.Create("version_id");
        }

        public static string ValidVersionName()
        {
            return Fixture.Create("version_name");
        }

        public static string ValidChannelName()
        {
            return Fixture.Create("channel_name");
        }

        public static string ValidStatusMessage()
        {
            return Fixture.Create("status_message");
        }

        public static List<string> ValidFileList()
        {
            return new List<string> { "fuel.json", "Engine\\Shaders\\StandaloneRenderer\\OpenGL\\SplashVertexShader.glsl" };
        }

        public static CreateVersionResult ValidCreateVersionResponse()
        {
            var version = new VersionDetails(ValidProductId().Id, ValidVersionId(), ValidVersionName(), ValidDescription(), ValidVersionStatus(), ValidStatusMessage(), ValidUrl());
            return new CreateVersionResult(version);
        }

        public static ListProductVersionsResult ValidListProductVersionsResponse(bool withLastVersion)
        {
            var lastEvaluatedVersionName = (withLastVersion ? Fixture.Create<string>() : null);
            var versionRef1 = new VersionRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<VersionStatus>());
            var versionRef2 = new VersionRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<VersionStatus>());
            var versions = new List<VersionRef>
            {
                versionRef1,
                versionRef2
            };

            return new ListProductVersionsResult(versions, lastEvaluatedVersionName);
        }

        public static ListProductChannelsResult ValidListProductChannelsResponse(bool withLastChannel)
        {
            var lastEvaluatedChannelName = (withLastChannel ? Fixture.Create<string>() : null);
            var channelRef1 = new ChannelRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>());
            var channelRef2 = new ChannelRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>());
            var channels = new List<ChannelRef>
            {
                channelRef1,
                channelRef2
            };

            return new ListProductChannelsResult(channels, lastEvaluatedChannelName);
        }

        public static ListProductsResult ValidListProductsResponse(bool withLastProduct)
        {
            var lastEvaluatedProductName = (withLastProduct ? Fixture.Create<string>() : null);
            var productRef1 = new ProductRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>());
            var productRef2 = new ProductRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>());
            var products = new List<ProductRef>
            {
                productRef1,
                productRef2
            };

            return new ListProductsResult(products, lastEvaluatedProductName);
        }
        
        // Created to match the manifest.json in Fuel.Ingest.Core.Tests\TestData\ValidUploadManifest\manifest.json
        public static UploadManifest ValidUploadManifest()
        {
            SignedUrl url1 = new SignedUrl("https://example.com/foo", DateTime.Parse("2017-01-08T00:34:12.236Z"));
            SignedUrl url2 = new SignedUrl("https://example.com/foo2", DateTime.Parse("2017-01-08T00:35:23.517Z"));

            UploadManifestFile file1 = new UploadManifestFile("fuel.json", ValidS3Bucket(), url1);
            UploadManifestFile file2 = new UploadManifestFile("Engine\\Shaders\\StandaloneRenderer\\OpenGL\\SplashVertexShader.glsl", ValidS3Bucket(), url2);

            List<UploadManifestFile> files = new List<UploadManifestFile>() { file1, file2 };

            return new UploadManifest(files);
        }

        public static UploadManifest InvalidUploadManifestSizesDoNotMatch()
        {
            SignedUrl url1 = new SignedUrl("https://example.com/foo", DateTime.Parse("2017-01-08T00:34:12.236Z"));

            UploadManifestFile file1 = new UploadManifestFile("fuel.json", ValidS3Bucket(), url1);

            List<UploadManifestFile> files = new List<UploadManifestFile>() { file1 };

            return new UploadManifest(files);
        }

        public static UploadManifest InvalidUploadManifestFilesDoNotMatch()
        {
            SignedUrl url1 = new SignedUrl("https://example.com/foo", DateTime.Parse("2017-01-08T00:34:12.236Z"));
            SignedUrl url2 = new SignedUrl("https://example.com/foo2", DateTime.Parse("2017-01-08T00:35:23.517Z"));

            UploadManifestFile file1 = new UploadManifestFile("fuel.json", ValidS3Bucket(), url1);
            UploadManifestFile file2 = new UploadManifestFile("Access\\To\\Invalid\\Resource.exe", ValidS3Bucket(), url2);

            List<UploadManifestFile> files = new List<UploadManifestFile>() { file1, file2 };

            return new UploadManifest(files);
        }

        public static DisposableDirectory DisposableDirectory()
        {
            return new DisposableDirectory();
        }

        public static string ValidUrl()
        {
            return Fixture.Create<Uri>().ToString();
        }

        public static string ValidDescription()
        {
            return Fixture.Create<string>();
        }

        public static VersionStatus ValidVersionStatus()
        {
            return Fixture.Create<VersionStatus>();
        }

        public static string ValidS3Bucket()
        {
            return Fixture.Create<string>();
        }

        public static string ValidIngestionXml()
        {
            return File.ReadAllText(TestData.GetPathToTestResource("TestData", "test-ingest-manifest.xml"));
        }

        public static string ValidChannelId()
        {
            return Fixture.Create<string>();
        }
    }

    public class DisposableDirectory : IDisposable
    {
        public DisposableDirectory()
        {
            Dir = Path.GetTempFileName() + Factories.ValidVersionId();
            Directory.CreateDirectory(Dir);
        }

        public string Dir { get; }

        public void Dispose()
        {
            Directory.Delete(Dir, true);
        }
    }
}

