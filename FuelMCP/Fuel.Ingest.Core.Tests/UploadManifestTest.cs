﻿using System.IO;
using Fuel.Ingest.Core.Models;
using Fuel.Ingest.Core.Tests.Utilities;
using Newtonsoft.Json;
using Xunit;

namespace Fuel.Ingest.Core.Tests
{
    public class UploadManifestTest
    {
        private readonly string _validManifest;

        public UploadManifestTest()
        {
            _validManifest = TestData.ReadTestFile(Path.Combine("TestData", "ValidUploadManifest", "manifest.json"));
        }

        [Fact]
        public void DeserializeObject_ShouldDeserializeJsonToUploadManifestObjectSuccessfully_GivenValidJson()
        {
            var manifest = JsonConvert.DeserializeObject<UploadManifest>(_validManifest);
            Assert.True(manifest.Files.Count == 2);

            var validateManifest = Factories.ValidUploadManifest();

            UploadManifestFile file1 = manifest.Files[0];
            UploadManifestFile file2 = manifest.Files[1];
            Assert.Equal(file1.Path, validateManifest.Files[0].Path);
            Assert.Equal(file2.Path, validateManifest.Files[1].Path);
            Assert.Equal(file1.SignedUrl.expires, validateManifest.Files[0].SignedUrl.expires.ToUniversalTime());
            Assert.Equal(file2.SignedUrl.expires, validateManifest.Files[1].SignedUrl.expires.ToUniversalTime());
            Assert.Equal(file1.SignedUrl.url, validateManifest.Files[0].SignedUrl.url);
            Assert.Equal(file2.SignedUrl.url, validateManifest.Files[1].SignedUrl.url);
        }
    }
}
