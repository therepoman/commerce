﻿using System;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Workflow
{
    public class ActivateVersionWorkflowTest
    {
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
        private readonly ActivateVersionWorkflow _activateVersionWorkflow;
        private readonly static string _channelId = Factories.ValidChannelId();
        private readonly static string _versionId = Factories.ValidVersionId();
        private readonly ActivateVersionArgs _args = new ActivateVersionArgs
        {
            ChannelId = _channelId,
            VersionId = _versionId,
            TwitchOAuthToken = Factories.ValidOauth(),
        };

        public ActivateVersionWorkflowTest()
        {
            _activateVersionWorkflow = new ActivateVersionWorkflow(_softwareDistributionAdminServiceClient.Object);
        }

        [Fact]
        public void ActivateVersion_ReturnsTrue_GivenWorkflowSuccess()
        {
            var actualChannelId = String.Empty;
            var actualVersionId = String.Empty;
            _softwareDistributionAdminServiceClient.Setup(c => c.SetChannelActiveVersion(It.IsAny<SetChannelActiveVersionRequest>()))
                .Callback<SetChannelActiveVersionRequest>(r =>
                {
                    actualChannelId = r.channelId;
                    actualVersionId = r.versionId;
                });

            _activateVersionWorkflow.ActivateVersion(_args);

            _softwareDistributionAdminServiceClient.Verify(c => c.SetChannelActiveVersion(It.IsAny<SetChannelActiveVersionRequest>()));
            Assert.Equal(_channelId, actualChannelId);
            Assert.Equal(_versionId, actualVersionId);
        }

        [Fact]
        public void ActivateVersion_ReturnsFalse_GivenException()
        {
            _softwareDistributionAdminServiceClient.Setup(c => c.SetChannelActiveVersion(It.IsAny<SetChannelActiveVersionRequest>()))
                .Throws<Exception>();

            Assert.False(_activateVersionWorkflow.ActivateVersion(_args));
        }
    }
}
