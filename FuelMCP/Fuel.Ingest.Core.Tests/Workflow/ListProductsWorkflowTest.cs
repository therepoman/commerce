﻿using System;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Workflow
{
    public class ListProductsWorkflowTest
    {
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
        private readonly IListProductsWorkflow _listProductsWorkflow;
        private readonly static string _productId = Factories.ValidProductId().ToString();
        private readonly ListProductsArgs _args = new ListProductsArgs
        {
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private const string SdasNamespace =
            "com.amazonaws.gearbox.softwaredistribution.admin.service.model.SoftwareDistributionAdminService";

        public ListProductsWorkflowTest()
        {
            _listProductsWorkflow = new ListProductsWorkflow(_softwareDistributionAdminServiceClient.Object);
        }

        [Fact]
        public void ListProducts_ShouldCallListProductsTwice()
        {
            _softwareDistributionAdminServiceClient
                .SetupSequence(s => s.ListProducts(It.IsAny<ListProductsRequest>()))
                .Returns(Factories.ValidListProductsResponse(true))
                .Returns(Factories.ValidListProductsResponse(false));

            Assert.True(_listProductsWorkflow.ListProducts(_args));
            _softwareDistributionAdminServiceClient.Verify(s => s.ListProducts(It.IsAny<ListProductsRequest>()), Times.Exactly(2));
        }

        [Fact]
        public void ListProducts_ReturnsFalse_GivenException()
        {
            _softwareDistributionAdminServiceClient.Setup(s => s.ListProducts(It.IsAny<ListProductsRequest>()))
                .Throws<Exception>();

            Assert.False(_listProductsWorkflow.ListProducts(_args));
        }
    }
}
