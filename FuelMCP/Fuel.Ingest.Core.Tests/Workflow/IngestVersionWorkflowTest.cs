﻿using System;
using System.Threading.Tasks;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Models;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Workflow
{
    public class IngestVersionWorkflowTest
    {
        private readonly IngestVersionWorkflow _ingestVersionWorkflow;
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
        private readonly int _maxThreads = Factories.ValidMaxThreads();
        private readonly Mock<FileUploader> _fileUploader = new Mock<FileUploader>();
        private readonly Mock<FileDownloader> _fileDownloader = new Mock<FileDownloader>();
        private readonly Mock<FileScanner> _fileScanner = new Mock<FileScanner>();
        private readonly Mock<IngestionProcess> _ingestionProcess;
        private readonly Mock<IngestManifestConstructor> _ingestManifestConstructor = new Mock<IngestManifestConstructor>();
        private readonly Mock<IngestManifest> _ingestManifest = new Mock<IngestManifest>(null, null, null);
        private readonly IngestVersionArgs _args = new IngestVersionArgs
        {
            VersionName = Factories.ValidVersionName(),
            ProductId = Factories.ValidProductId().ToString(),
            Directory = TestData.GetPathToTestResource("TestData", "ValidGame"),
            Description = Factories.ValidDescription(),
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private const string SdasNamespace =
            "com.amazonaws.gearbox.softwaredistribution.admin.service.model.SoftwareDistributionAdminService";

        public IngestVersionWorkflowTest()
        {
            _ingestionProcess = new Mock<IngestionProcess>(_softwareDistributionAdminServiceClient.Object);
            _ingestVersionWorkflow = new IngestVersionWorkflow(_softwareDistributionAdminServiceClient.Object, _maxThreads, _fileUploader.Object,
                _fileDownloader.Object, _fileScanner.Object, _ingestionProcess.Object, _ingestManifestConstructor.Object);
        }

        [Fact]
        public void IngestVersion_ReturnsTrue_GivenWorkflowSuccess()
        {
            _fileScanner
                .Setup(f => f.ScanProductFiles(It.IsAny<string>()))
                .Returns(Factories.ValidFileList());
            _softwareDistributionAdminServiceClient
                .Setup(s => s.CreateVersion(It.IsAny<CreateVersionRequest>()))
                .Returns(Factories.ValidCreateVersionResponse());
            _fileDownloader
                .Setup(f => f.Download(It.IsAny<string>()))
                .Returns(Factories.ValidUploadManifest());
            _fileUploader
                .Setup(f => f.Upload(It.IsAny<UploadManifest>(), TestData.TestDataDir(), _maxThreads))
                .Returns(Task.FromResult(true));
            _ingestManifestConstructor
                .Setup(i => i.CreateIngestManifest(It.IsAny<UploadManifest>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(_ingestManifest.Object);
            _ingestionProcess
                .Setup(i => i.WaitForIngestionToResolve(It.IsAny<string>()))
                .Returns(true);

            Assert.True(_ingestVersionWorkflow.IngestVersion(TestData.TestDataDir(), _args));

            _ingestionProcess
                .Verify(i => i.StartIngestion(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Fact]
        public void IngestVersion_ReturnsFalse_GivenInvalidIngestionManifestSizesDoNotMatch()
        {
            _fileScanner
                .Setup(f => f.ScanProductFiles(It.IsAny<string>()))
                .Returns(Factories.ValidFileList());
            _softwareDistributionAdminServiceClient
                .Setup(s => s.CreateVersion(It.IsAny<CreateVersionRequest>()))
                .Returns(Factories.ValidCreateVersionResponse());
            _fileDownloader
                .Setup(f => f.Download(It.IsAny<string>()))
                .Returns(Factories.InvalidUploadManifestSizesDoNotMatch());

            Assert.False(_ingestVersionWorkflow.IngestVersion(TestData.TestDataDir(), _args));
        }

        [Fact]
        public void IngestVersion_ReturnsFalse_GivenInvalidIngestionManifestFilesDoNotMatch()
        {
            _fileScanner
                .Setup(f => f.ScanProductFiles(It.IsAny<string>()))
                .Returns(Factories.ValidFileList());
            _softwareDistributionAdminServiceClient
                .Setup(s => s.CreateVersion(It.IsAny<CreateVersionRequest>()))
                .Returns(Factories.ValidCreateVersionResponse());
            _fileDownloader
                .Setup(f => f.Download(It.IsAny<string>()))
                .Returns(Factories.InvalidUploadManifestFilesDoNotMatch());

            Assert.False(_ingestVersionWorkflow.IngestVersion(TestData.TestDataDir(), _args));
        }

        [Fact]
        public void IngestVersion_ReturnsFalse_GivenExceptionDuringWorkflow()
        {
            _softwareDistributionAdminServiceClient
                .Setup(s => s.CreateVersion(It.IsAny<CreateVersionRequest>()))
                .Throws<Exception>();
            _fileUploader
                .Setup(f => f.Upload(It.IsAny<UploadManifest>(), TestData.TestDataDir(), _maxThreads))
                .Throws<Exception>();
            _fileDownloader
                .Setup(f => f.Download(It.IsAny<string>()))
                .Throws<Exception>();
            _fileScanner
                .Setup(f => f.ScanProductFiles(It.IsAny<string>()))
                .Throws<Exception>();
            Assert.False(_ingestVersionWorkflow.IngestVersion(TestData.TestDataDir(), _args));
        }
    }
}
