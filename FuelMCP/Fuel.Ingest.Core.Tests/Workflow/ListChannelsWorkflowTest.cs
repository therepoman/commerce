﻿using System;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Workflow
{
    public class ListChannelsWorkflowTest
    {
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
        private readonly IListChannelsWorkflow _listChannelsWorkflow;
        private static readonly string ProductId = Factories.ValidProductId().ToString();
        private readonly ListChannelsArgs _args = new ListChannelsArgs
        {
            ProductId = ProductId,
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private const string SdasNamespace =
            "com.amazonaws.gearbox.softwaredistribution.admin.service.model.SoftwareDistributionAdminService";

        public ListChannelsWorkflowTest()
        {
            _listChannelsWorkflow = new ListChannelsWorkflow(_softwareDistributionAdminServiceClient.Object);
        }

        [Fact]
        public void ListChannels_ShouldCallListProductChannelsTwice()
        {
            _softwareDistributionAdminServiceClient
                .SetupSequence(s => s.ListProductChannels(It.IsAny<ListProductChannelsRequest>()))
                .Returns(Factories.ValidListProductChannelsResponse(true))
                .Returns(Factories.ValidListProductChannelsResponse(false));

            Assert.True(_listChannelsWorkflow.ListChannels(_args));
            _softwareDistributionAdminServiceClient.Verify(s => s.ListProductChannels(It.IsAny<ListProductChannelsRequest>()), Times.Exactly(2));
        }

        [Fact]
        public void ListChannels_ReturnsFalse_GivenException()
        {
            _softwareDistributionAdminServiceClient.Setup(s => s.ListProductChannels(It.IsAny<ListProductChannelsRequest>()))
                .Throws<Exception>();

            Assert.False(_listChannelsWorkflow.ListChannels(_args));
        }
    }
}
