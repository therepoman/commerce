﻿using System;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Workflow
{
    public class ListVersionsWorkflowTest
    {
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
        private readonly IListVersionsWorkflow _listVersionsWorkflow;
        private readonly static string _productId = Factories.ValidProductId().ToString();
        private readonly ListVersionsArgs _args = new ListVersionsArgs
        {
            ProductId = _productId,
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private const string SdasNamespace =
            "com.amazonaws.gearbox.softwaredistribution.admin.service.model.SoftwareDistributionAdminService";

        public ListVersionsWorkflowTest()
        {
            _listVersionsWorkflow = new ListVersionsWorkflow(_softwareDistributionAdminServiceClient.Object);
        }

        [Fact]
        public void ListVersions_ShouldCallListProductVersionsTwice()
        {
            _softwareDistributionAdminServiceClient
                .SetupSequence(s => s.ListProductVersions(It.IsAny<ListProductVersionsRequest>()))
                .Returns(Factories.ValidListProductVersionsResponse(true))
                .Returns(Factories.ValidListProductVersionsResponse(false));

            Assert.True(_listVersionsWorkflow.ListVersions(_args));
            _softwareDistributionAdminServiceClient.Verify(s => s.ListProductVersions(It.IsAny<ListProductVersionsRequest>()), Times.Exactly(2));
        }

        [Fact]
        public void ListVersions_ReturnsFalse_GivenException()
        {
            _softwareDistributionAdminServiceClient.Setup(s => s.ListProductVersions(It.IsAny<ListProductVersionsRequest>()))
                .Throws<Exception>();

            Assert.False(_listVersionsWorkflow.ListVersions(_args));
        }
    }
}
