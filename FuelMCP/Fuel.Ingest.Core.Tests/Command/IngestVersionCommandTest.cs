﻿using Amazon.MCP.Plugin.Launcher.Models;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Fuel.SoftwareDistributionAdminService.Client;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Command
{
    public class IngestVersionCommandTest
    {
        private readonly IngestVersionCommand _command;
        private readonly Mock<IngestEnvironment> _environment = new Mock<IngestEnvironment>();
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
        private readonly Mock<IWorkflowFactory> _workflowFactory = new Mock<IWorkflowFactory>();
        private readonly Mock<IIngestVersionWorkflow> _workflow = new Mock<IIngestVersionWorkflow>();

        private readonly IngestVersionArgs _args = new IngestVersionArgs
        {
            Threads = Factories.ValidMaxThreads(),
            VersionName = Factories.ValidVersionName(),
            ProductId = Factories.ValidProductId().ToString(),
            Directory = TestData.GetPathToTestResource("TestData", "ValidGame"),
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private readonly DebugCommandArgs _debugArgs = new DebugCommandArgs
        {
            TargetIngestionServiceUrl = Factories.ValidUrl()
        };

        public IngestVersionCommandTest()
        {
            var launchConfigProvider = new JsonLaunchConfigProvider();
            _workflowFactory.Setup(factory => factory.CreateIngestVersionWorkflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(_workflow.Object);
            _command = new IngestVersionCommand(launchConfigProvider, _environment.Object, _workflowFactory.Object);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoArguments()
        {
            var result = _command.Exec(new string[] { });
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingDirectoryArgument()
        {
            _args.Directory = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingProductIdArgument()
        {
            _args.ProductId = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingVersionNameArgument()
        {
            _args.VersionName = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingOAuthTokenArgumentAndOAuthTokenSystemProperty()
        {
            _args.TwitchOAuthToken = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Must define the --oauthtoken argument or system property 'TWITCH_OAUTH_TOKEN'"), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoSuchDirectory()
        {
            _args.Directory = TestData.GetPathToTestResource("No", "Such", "Directory");
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail(@"No such directory " + _args.Directory), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenDirectoryWithNoFuelJson()
        {
            using (var dd = Factories.DisposableDirectory())
            {
                _args.Directory = dd.Dir;
                var result = _command.Exec(getCommandArgs());
                Assert.Equal(InvokeResult.Fail("Missing fuel.json"), result);
            }
        }

        [Fact]
        public void Exec_ShouldFail_GivenDirectoryWithInvalidFuelJson()
        {
            _args.Directory = TestData.GetPathToTestResource("TestData", "InvalidFuelJson");
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Exception of type 'Amazon.MCP.Common.Exceptions.InvalidLaunchConfigException' was thrown."), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenInvalidUrl()
        {
            var invalidUrl = "/::/invalid/.url./";
            var failureCode = 1;
            _debugArgs.TargetIngestionServiceUrl = invalidUrl;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldFail_GivenFailedWorkflowExecution()
        {
            var expected = InvokeResult.Fail();
            _workflow.Setup(workflow => workflow.IngestVersion(It.IsAny<string>(), It.IsAny<IngestVersionArgs>())).Returns(false);

            var actual = _command.Exec(getCommandArgs());
            Assert.Equal(expected.ExitCode, actual.ExitCode);
        }

        [Fact]
        public void Exec_ShouldSucceed_GivenSuccessfulWorkflowExecution()
        {
            var expected = InvokeResult.Success;
            _workflow.Setup(workflow => workflow.IngestVersion(It.IsAny<string>(), It.IsAny<IngestVersionArgs>())).Returns(true);

            var actual = _command.Exec(getCommandArgs());
            Assert.Equal(expected, actual);
        }

        private string[] getCommandArgs()
        {
            return FuelIngestTestUtil.ToCommandLineArgs(_args, _debugArgs);
        }
    }
}
