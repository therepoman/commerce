﻿using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Command
{
    public class ActivateVersionCommandTest
    {
        private readonly ActivateVersionCommand _command;
        private readonly Mock<IWorkflowFactory> _workflowFactory = new Mock<IWorkflowFactory>();
        private readonly Mock<IActivateVersionWorkflow> _workflow = new Mock<IActivateVersionWorkflow>();
        private readonly Mock<IngestEnvironment> _environment = new Mock<IngestEnvironment>();
        private readonly ActivateVersionArgs _args = new ActivateVersionArgs
        {
            ChannelId = Factories.ValidChannelId(),
            VersionId = Factories.ValidVersionId(),
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private readonly DebugCommandArgs _debugArgs = new DebugCommandArgs
        {
            TargetIngestionServiceUrl = Factories.ValidUrl()
        };

        public ActivateVersionCommandTest()
        {
            _workflowFactory.Setup(factory => factory.CreateActivateVersionWorkflow(It.IsAny<string>(), It.IsAny<string>())).Returns(_workflow.Object);
            _command = new ActivateVersionCommand(_environment.Object, _workflowFactory.Object);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoArguments()
        {
            var result = _command.Exec(new string[] { });
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingChannelIdArgument()
        {
            _args.ChannelId = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingVersionIdArgument()
        {
            _args.VersionId = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingOAuthTokenArgumentAndOAuthTokenSystemProperty()
        {
            _args.TwitchOAuthToken = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Must define the --oauthtoken argument or system property 'TWITCH_OAUTH_TOKEN'"), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenInvalidUrl()
        {
            var invalidUrl = "/::/invalid/.url./";
            var failureCode = 1;
            _debugArgs.TargetIngestionServiceUrl = invalidUrl;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldSucceed_GivenSuccessfulWorkflowExecution()
        {
            _workflow.Setup(workflow => workflow.ActivateVersion(It.IsAny<ActivateVersionArgs>()))
                .Returns(true);

            var actual = _command.Exec(getCommandArgs());

            _workflow.Verify(w => w.ActivateVersion(It.IsAny<ActivateVersionArgs>()));
            Assert.Equal(InvokeResult.Success, actual);
        }

        [Fact]
        public void Exec_ShouldFail_GivenFailedWorkflowExecution()
        {
            var failureCode = 1;
            _workflow.Setup(workflow => workflow.ActivateVersion(It.IsAny<ActivateVersionArgs>()))
                .Returns(false);

            var actual = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, actual.ExitCode);
        }

        private string[] getCommandArgs()
        {
            return FuelIngestTestUtil.ToCommandLineArgs(_args, _debugArgs);
        }
    }
}
