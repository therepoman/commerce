﻿using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Command
{
    public class ListChannelsCommandTest
    {
        private readonly ListChannelsCommand _command;
        private readonly Mock<IWorkflowFactory> _workflowFactory = new Mock<IWorkflowFactory>();
        private readonly Mock<IListChannelsWorkflow> _workflow = new Mock<IListChannelsWorkflow>();
        private readonly Mock<IngestEnvironment> _environment = new Mock<IngestEnvironment>();
        private readonly ListChannelsArgs _args = new ListChannelsArgs
        {
            ProductId = Factories.ValidProductId().ToString(),
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private readonly DebugCommandArgs _debugArgs = new DebugCommandArgs
        {
            TargetIngestionServiceUrl = Factories.ValidUrl()
        };

        public ListChannelsCommandTest()
        {
            _workflowFactory.Setup(w => w.CreateListChannelsWorkflow(It.IsAny<string>(), It.IsAny<string>())).Returns(_workflow.Object);
            _command = new ListChannelsCommand(_environment.Object, _workflowFactory.Object);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoArguments()
        {
            var result = _command.Exec(new string[] { });
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingProductIdArgument()
        {
            _args.ProductId = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingOAuthTokenArgumentAndOAuthTokenSystemProperty()
        {
            _args.TwitchOAuthToken = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Must define the --oauthtoken argument or system property 'TWITCH_OAUTH_TOKEN'"), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenInvalidUrl()
        {
            var invalidUrl = "/::/invalid/.url./";
            var failureCode = 1;
            _debugArgs.TargetIngestionServiceUrl = invalidUrl;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldSucceed_GivenSuccessfulWorkflowExecution()
        {
            _workflow.Setup(workflow => workflow.ListChannels(It.IsAny<ListChannelsArgs>()))
                .Returns(true);

            var actual = _command.Exec(getCommandArgs());
            _workflow.Verify(w => w.ListChannels(It.IsAny<ListChannelsArgs>()));
            Assert.Equal(InvokeResult.Success, actual);
        }

        [Fact]
        public void Exec_ShouldFail_GivenFailedWorkflowExecution()
        {
            var failureCode = 1;
            _workflow.Setup(workflow => workflow.ListChannels(It.IsAny<ListChannelsArgs>()))
                .Returns(false);

            var actual = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, actual.ExitCode);
        }

        private string[] getCommandArgs()
        {
            return FuelIngestTestUtil.ToCommandLineArgs(_args, _debugArgs);
        }
    }
}
