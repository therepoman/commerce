﻿using Amazon.MCP.Plugin.Launcher.Models;
using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Command
{
    public class ValidateCommandTest
    {
        private readonly ValidateCommand _command;

        private readonly ValidateArgs _args = new ValidateArgs()
        {
            Directory = TestData.GetPathToTestResource("TestData", "ValidFuelJson"),
        };

        public ValidateCommandTest()
        {
            var launchConfigProvider = new JsonLaunchConfigProvider();
            _command = new ValidateCommand(launchConfigProvider);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoDirectoryArg()
        {
            var expectedExitCode = 1;
            _args.Directory = null;
            var result = _command.Exec(FuelIngestTestUtil.ToCommandLineArgs(_args));
            Assert.Equal(expectedExitCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoSuchDirectory()
        {
            _args.Directory = TestData.GetPathToTestResource("No", "Such", "Directory");
            var result = _command.Exec(FuelIngestTestUtil.ToCommandLineArgs(_args));
            Assert.Equal(InvokeResult.Fail(@"No such directory " + _args.Directory), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenDirectoryWithNoFuelJson()
        {
            using (var dd = Factories.DisposableDirectory())
            {
                _args.Directory = dd.Dir;
                var result = _command.Exec(FuelIngestTestUtil.ToCommandLineArgs(_args));
                Assert.Equal(InvokeResult.Fail("Missing fuel.json"), result);
            }
        }

        [Fact]
        public void Exec_ShouldFail_GivenDirectoryWithInvalidFuelJson()
        {
            _args.Directory = TestData.GetPathToTestResource("TestData", "InvalidFuelJson");
            var result = _command.Exec(FuelIngestTestUtil.ToCommandLineArgs(_args));
            Assert.Equal(InvokeResult.Fail("Exception of type 'Amazon.MCP.Common.Exceptions.InvalidLaunchConfigException' was thrown."), result);
        }
    }
}
