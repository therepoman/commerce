﻿using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Command
{
    public class ListVersionsCommandTest
    {
        private readonly ListVersionsCommand _command;
        private readonly Mock<IWorkflowFactory> _workflowFactory = new Mock<IWorkflowFactory>();
        private readonly Mock<IListVersionsWorkflow> _workflow = new Mock<IListVersionsWorkflow>();
        private readonly Mock<IngestEnvironment> _environment = new Mock<IngestEnvironment>();
        private readonly ListVersionsArgs _args = new ListVersionsArgs
        {
            ProductId = Factories.ValidProductId().ToString(),
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private readonly DebugCommandArgs _debugArgs = new DebugCommandArgs
        {
            TargetIngestionServiceUrl = Factories.ValidUrl()
        };

        public ListVersionsCommandTest()
        {
            _workflowFactory.Setup(w => w.CreateListVersionsWorkflow(It.IsAny<string>(), It.IsAny<string>())).Returns(_workflow.Object);
            _command = new ListVersionsCommand(_environment.Object, _workflowFactory.Object);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoArguments()
        {
            var result = _command.Exec(new string[] { });
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingProductIdArgument()
        {
            _args.ProductId = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Missing Arguments"), result);
        }

        [Fact]
        public void Exec_ShouldFail_MissingOAuthTokenArgumentAndOAuthTokenSystemProperty()
        {
            _args.TwitchOAuthToken = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Must define the --oauthtoken argument or system property 'TWITCH_OAUTH_TOKEN'"), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenInvalidUrl()
        {
            var invalidUrl = "/::/invalid/.url./";
            var failureCode = 1;
            _debugArgs.TargetIngestionServiceUrl = invalidUrl;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldSucceed_GivenSuccessfulWorkflowExecution()
        {
            _workflow.Setup(workflow => workflow.ListVersions(It.IsAny<ListVersionsArgs>()))
                .Returns(true);

            var actual = _command.Exec(getCommandArgs());
            _workflow.Verify(w => w.ListVersions(It.IsAny<ListVersionsArgs>()));
            Assert.Equal(InvokeResult.Success, actual);
        }

        [Fact]
        public void Exec_ShouldFail_GivenFailedWorkflowExecution()
        {
            var failureCode = 1;
            _workflow.Setup(workflow => workflow.ListVersions(It.IsAny<ListVersionsArgs>()))
                .Returns(false);

            var actual = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, actual.ExitCode);
        }

        private string[] getCommandArgs()
        {
            return FuelIngestTestUtil.ToCommandLineArgs(_args, _debugArgs);
        }
    }
}
