﻿using Fuel.Ingest.Core.Command;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.Ingest.Core.Workflow;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests.Command
{
    public class ListProductsCommandTest
    {
        private readonly ListProductsCommand _command;
        private readonly Mock<IWorkflowFactory> _workflowFactory = new Mock<IWorkflowFactory>();
        private readonly Mock<IListProductsWorkflow> _workflow = new Mock<IListProductsWorkflow>();
        private readonly Mock<IngestEnvironment> _environment = new Mock<IngestEnvironment>();
        private readonly ListProductsArgs _args = new ListProductsArgs
        {
            TwitchOAuthToken = Factories.ValidOauth(),
        };
        private readonly DebugCommandArgs _debugArgs = new DebugCommandArgs
        {
            TargetIngestionServiceUrl = Factories.ValidUrl()
        };

        public ListProductsCommandTest()
        {
            _workflowFactory.Setup(w => w.CreateListProductsWorkflow(It.IsAny<string>(), It.IsAny<string>())).Returns(_workflow.Object);
            _command = new ListProductsCommand(_environment.Object, _workflowFactory.Object);
        }

        [Fact]
        public void Exec_ShouldFail_GivenNoArguments()
        {
            var failureCode = 1;
            var result = _command.Exec(new string[] { });
            Assert.Equal(failureCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldFail_MissingOAuthTokenArgumentAndOAuthTokenSystemProperty()
        {
            _args.TwitchOAuthToken = null;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(InvokeResult.Fail("Must define the --oauthtoken argument or system property 'TWITCH_OAUTH_TOKEN'"), result);
        }

        [Fact]
        public void Exec_ShouldFail_GivenInvalidUrl()
        {
            var invalidUrl = "/::/invalid/.url./";
            var failureCode = 1;
            _debugArgs.TargetIngestionServiceUrl = invalidUrl;
            var result = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, result.ExitCode);
        }

        [Fact]
        public void Exec_ShouldSucceed_GivenSuccessfulWorkflowExecution()
        {
            _workflow.Setup(workflow => workflow.ListProducts(It.IsAny<ListProductsArgs>()))
                .Returns(true);

            var actual = _command.Exec(getCommandArgs());
            _workflow.Verify(w => w.ListProducts(It.IsAny<ListProductsArgs>()));
            Assert.Equal(InvokeResult.Success, actual);
        }

        [Fact]
        public void Exec_ShouldFail_GivenFailedWorkflowExecution()
        {
            var failureCode = 1;
            _workflow.Setup(workflow => workflow.ListProducts(It.IsAny<ListProductsArgs>()))
                .Returns(false);

            var actual = _command.Exec(getCommandArgs());
            Assert.Equal(failureCode, actual.ExitCode);
        }

        private string[] getCommandArgs()
        {
            return FuelIngestTestUtil.ToCommandLineArgs(_args, _debugArgs);
        }
    }
}
