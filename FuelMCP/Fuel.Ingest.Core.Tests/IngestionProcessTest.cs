﻿using System;
using Fuel.Ingest.Core.Tests.Utilities;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Moq;
using Xunit;

namespace Fuel.Ingest.Core.Tests
{
    public class IngestionProcessTest
    {
        private readonly Mock<ISoftwareDistributionAdminServiceClient> _softwareDistributionAdminServiceClient;
        private readonly IngestionProcess _ingestionProcess;
        private readonly string _versionId = Factories.ValidVersionId();
        private readonly string _ingestManifestXml = Factories.ValidIngestionXml();
        public IngestionProcessTest()
        {
            _softwareDistributionAdminServiceClient = new Mock<ISoftwareDistributionAdminServiceClient>();
            _ingestionProcess = new IngestionProcess(_softwareDistributionAdminServiceClient.Object);
        }

        [Fact]
        public void StartIngestion_ShouldCallAttachVersionIngestionManifest()
        {
            var actualVersionId = String.Empty;
            var actualIngestManifestXml = String.Empty;
            _softwareDistributionAdminServiceClient.Setup(c => c.AttachVersionIngestionManifest(It.IsAny<AttachVersionIngestionManifestRequest>()))
                .Callback<AttachVersionIngestionManifestRequest>(r =>
                {
                    actualVersionId = r.versionId;
                    actualIngestManifestXml = r.manifest;
                });

            _ingestionProcess.StartIngestion(_versionId, _ingestManifestXml);

            _softwareDistributionAdminServiceClient.Verify(c => c.AttachVersionIngestionManifest(It.IsAny<AttachVersionIngestionManifestRequest>()));
            Assert.Equal(_versionId, actualVersionId);
            Assert.Equal(_ingestManifestXml, actualIngestManifestXml);
        }

        [Fact]
        public void WaitForIngestionToResolve_ShouldReturnTrueIfIngestionIsSuccess()
        {
            var expectedSuccess = true;
            var actualVersionId = String.Empty;
            _softwareDistributionAdminServiceClient.Setup(c => c.DescribeVersion(It.IsAny<DescribeVersionRequest>()))
                .Callback<DescribeVersionRequest>(r =>
                {
                    actualVersionId = r.versionId;
                }).Returns(CreateDescribeVersionResult(VersionStatus.AVAILABLE));

            var actualSuccess = _ingestionProcess.WaitForIngestionToResolve(_versionId);

            _softwareDistributionAdminServiceClient.Verify(c => c.DescribeVersion(It.IsAny<DescribeVersionRequest>()));
            Assert.Equal(_versionId, actualVersionId);
            Assert.Equal(expectedSuccess, actualSuccess);
        }

        private DescribeVersionResult CreateDescribeVersionResult(VersionStatus status)
        {
            var version = new VersionDetails(Factories.ValidProductId().Id, Factories.ValidVersionId(), Factories.ValidVersionName(), Factories.ValidDescription(), status, Factories.ValidStatusMessage(), Factories.ValidUrl());
            return new DescribeVersionResult(version);
        }
    }
}
