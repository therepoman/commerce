﻿using Fuel.Ingest.Core;
using Xunit;
using Assert = Xunit.Assert;

namespace Amazon.MCP.Test.Main.Fuel.Ingest.ArgsParsers
{
    public class IngestArgsParserTest
    {
        private IngestArgsParser parser;

        public IngestArgsParserTest()
        {
            parser = new IngestArgsParser();
        }

        [Fact]
        public void ParseNoArgsSetsError()
        {
            var result = parser.Parse(new string[] {});
            Assert.NotNull(result);
            Assert.Null(result.Command);
            Assert.Empty(result.OtherArgs);
            Assert.Equal("Must provide a command", result.ErrorMessage);
        }

        [Fact]
        public void ParseCommandOnly()
        {
            var result = parser.Parse(new[] {"command"});
            Assert.NotNull(result);
            Assert.Equal("command", result.Command);
            Assert.Empty(result.OtherArgs);
            Assert.Null(result.ErrorMessage);
        }

        [Fact]
        public void ParseCommandWithExtraArgs()
        {
            var result = parser.Parse(new[] { "command", "some", "other", "args" });
            Assert.NotNull(result);
            Assert.Equal("command", result.Command);
            Assert.Equal(new[] {"some", "other", "args"}, result.OtherArgs);
            Assert.Null(result.ErrorMessage);
        }
    }
}
