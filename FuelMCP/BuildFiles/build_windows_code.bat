setlocal
set fuel_build_workdir=%1
set fuel_build_config=%2
set desktop_build_config=%3

if "%fuel_build_workdir%" == "" set fuel_build_workdir=..\..
if "%fuel_build_config%" == "" set fuel_build_config=debug
if "%desktop_build_config%" == "" set desktop_build_config=devbuild

set buildconfig=%ProgramFiles(x86)%\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsMSBuildCmd.bat
if not exist "%buildconfig%" set buildconfig=%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Professional\Common7\Tools\VsMSBuildCmd.bat
call "%buildconfig%"

pushd %fuel_build_workdir%\FuelMCP
msbuild SolutionFiles\FuelCore.sln /t:FuelLauncher\Fuel_App_TwitchInstallHelper:Clean,FuelLauncher\Fuel_App_TwitchInstallHelper:Rebuild /p:Configuration=%fuel_build_config% "/p:Platform=x86"
popd

pushd %fuel_build_workdir%\desktop
msbuild Curse.Radium.Windows.sln /t:Curse_Radium_Windows:Clean,Curse_Radium_Windows:Rebuild /p:Configuration=%desktop_build_config% "/p:Platform=Any CPU"
popd

:done
endlocal

