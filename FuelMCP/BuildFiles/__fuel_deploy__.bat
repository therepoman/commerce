@echo off

echo.
echo ---------------------------------
echo -         FUEL DEPLOYER         -
echo ---------------------------------
echo.

echo Ensure that this directory one below the repo top parent (dir should have 'desktop' sibling)

pause

set REDIST_DIR=..\desktop\Redistributables-Windows\Fuel
set DEPS_DIR=..\desktop\Dependencies\Fuel

echo.
echo .txt>exclude.txt
echo .bat>>exclude.txt
echo .gitignore>>exclude.txt

if NOT EXIST %REDIST_DIR% goto :skip_redist

if EXIST .gitignore del .gitignore
move %REDIST_DIR%\.gitignore .
rmdir /s /q %REDIST_DIR%
mkdir %REDIST_DIR%
move .gitignore %REDIST_DIR%\.gitignore
xcopy /e /y /exclude:exclude.txt . %REDIST_DIR%

:skip_redist

echo.

if NOT EXIST %DEPS_DIR% goto :skip_deps

echo .exe>>exclude.txt
echo .pdb>>exclude.txt

if EXIST .gitignore del .gitignore
move %DEPS_DIR%\.gitignore .
rmdir /s /q %DEPS_DIR%
mkdir %DEPS_DIR%
move .gitignore %DEPS_DIR%\.gitignore
xcopy /y /exclude:exclude.txt . %DEPS_DIR%

:skip_deps

echo.
echo DONE!
echo.

echo CHANGED FILES:
echo.
pushd ..\desktop
git status
popd

echo.
echo NOTE: Make sure that there are no added or deleted files above that you weren't expecting!
echo.

pause