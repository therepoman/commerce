import os, shutil, argparse, sys
from subprocess import call

parser = argparse.ArgumentParser(description='Prep live.')
parser.add_argument('--repoPathBase', dest='repoPathBase', required=False, default='..\..', type=str, help='Path to repo base (includes all repo dirs, defaults to ..\..)')
parser.add_argument('--liveTargetTag', dest='liveTargetTag', required=True, default='', type=str, help='Live target tag (should generally be yy_mm_dd of target date)')
parser.add_argument('--nocommit', dest='nocommit', required=False, default=False, type=bool, help='If true, no changes will be made (will print actions)')
parser.add_argument('--nopush', dest='nopush', required=False, default=False, type=bool, help='If true, no changes will be made to server (will print actions)')
args = parser.parse_args()

def queryYesNo(question, default="yes"):
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]

def callEx(callArgs):
    print('Call: ' + ' '.join(callArgs))
    if not args.nocommit:
    	call(callArgs)

def chdirEx(callArgs):
    print('Chdir: ' + callArgs)
    os.chdir(callArgs)

print('repoPathBase:' + args.repoPathBase, 'liveTargetTag:' + args.liveTargetTag, 'nocommit:' + str(args.nocommit))
targetBranch = 'Features/Fuel/' + args.liveTargetTag
print('target branch is ' + targetBranch)

olddir = os.curdir
chdirEx(args.repoPathBase)

print('')
print('*********************************')
print('Sync repos')
print('*********************************')
print('')

print('')
print('WARNING: make sure you have no uncommitted work, any modified files will be reverted!')
print('')

os.system('pause')  

for subdir in filter(lambda x: os.path.isdir(os.path.join('.', x)), os.listdir('.')):
    print('Resyncing repo at ' + subdir)
    chdirEx(subdir)
    callEx(['git', 'add', '--all'])
    callEx(['git', 'reset', '--hard'])
    callEx(['git', 'pull'])
    chdirEx('..')

print('')
print('*********************************')
print('Copy paste to Slack (TODO: Slack integration .. someday)')
print('')
print('Prepping beta/live branch')
print('`'+targetBranch+'`')
print('')
print('*********************************')
print('')

os.system('pause')  

print('')
print('*********************************')
print('Creating branches')
print('*********************************')
print('')

for repo in ['desktop', 'fuelmcp', 'laguna']:
    chdirEx(repo)
    callEx(['git', 'checkout', 'Features/Fuel/Next'])
    callEx(['git', 'pull'])
    callEx(['git', 'checkout', '-b', targetBranch])
    callEx(['git', 'checkout', targetBranch])
    chdirEx('..')

print('')
print('*********************************')
print('Create local build')
print('*********************************')
print('')

callEx(['FuelMCP\\BuildFiles\\build_windows_all.bat', '.', 'debug', 'DevBuild'])
os.system('start desktop\\Curse.Radium.Windows\\bin\\Debug')

print('')
print('*********************************')
print('Test local build')
print('')
print('--> Ensure that games library tab displays and that you can install, play and uninstall a game')
print('')
success = queryYesNo('BVT pass?','no')
print('*********************************')
print('')

if not success:
    print('exiting..')
    exit(1)

print('')
print('*********************************')
print('Push branch')
print('*********************************')

for repo in ['desktop', 'fuelmcp', 'laguna']:
    chdirEx(repo)
    if not args.nopush:
        callEx(['git', 'push', '-u', targetBranch])
    chdirEx('..')

print('')
print('*********************************')
print('')
print('Next steps:')
print('')
print('1) Start a build off new branch (' +targetBranch+ ') on teamcity')
print('2) Test new build once complete')
print('3) Download artifact binaries')
print('4) In desktop repo, deploy binaries to Dependencies/Fuel and Redistributables-Windows/Fuel')
print('5) Build local and test')
print('6) Git commit/push to newly created branch with commit text "Fuel Refresh x.x.x.x", where x.x.x.x is binary version')
print('7) Create PRs for desktop/laguna/fuelmcp branch '+targetBranch+'against Features/Fuel/Next')
print('8) Add "needs beta" tag to all PRs and set the target milestone')
print('9) Post links to PRs in Slack at #fuel-client-team and notify @nick')
print('10) Have a drink..')
print('')
print('*********************************')
print('')

os.chdir(olddir)
