
setlocal
set fuel_build_workdir=%1
set fuel_build_config=%2
set desktop_build_config=%3

if "%fuel_build_workdir%" == "" set fuel_build_workdir=..\..
if "%fuel_build_config%" == "" set fuel_build_config=debug
if "%desktop_build_config%" == "" set desktop_build_config=devbuild

call "%fuel_build_workdir%\FuelMCP\BuildFiles\build_windows_code.bat" %fuel_build_workdir% %fuel_build_config% %desktop_build_config%

pushd %fuel_build_workdir%\laguna
call dist_desktop_release.bat
popd

pushd %fuel_build_workdir%\desktop\tools
call build_dat.bat
popd

pushd %fuel_build_workdir%\desktop
mkdir Curse.Radium.Windows\bin\Debug\Assets
del Curse.Radium.Windows\bin\Debug\Assets\client.dat
copy Redistributables-Shared\Assets\client.dat Curse.Radium.Windows\bin\Debug\Assets\client.dat
REM call build_installer.bat test.exe
popd