using System;
using System.Threading;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Library.Messages;
using Serilog;

namespace Amazon.Fuel.Plugin.Library
{
    /// <summary>
    /// Includes installation services for individual Fuel games, such as install shortcuts, update installinfo DBs,
    /// register add/remove programs, etc.
    /// </summary>
    public class GameInstaller : IGameInstaller
    {
        private const int IpcRegisterInstallerTimeoutSec = 60;

        private readonly ILibraryPlugin _library;
        private readonly IMessengerHub _messengerHub;
        private readonly IMetricsPlugin _metrics;
        private readonly ILaunchConfigProvider _launchConfigProvider;
        private readonly IShortcutManager _shortcutManager;
        private readonly IInstallHelperIpcController _installHelperController;
        private readonly IInstalledGamesPlugin _installedGames;
        private readonly IManifestStorage _manifestStorage;
        private readonly IEntitlementsDatabase _entitlementDb;

        public GameInstaller(
            ILibraryPlugin library,
            IMessengerHub messengerHub,
            IMetricsPlugin metrics,
            ILaunchConfigProvider launchConfigProvider,
            IShortcutManager shortcutManager,
            IInstallHelperIpcController installHelperController,
            IInstalledGamesPlugin installedGames,
            IManifestStorage manifestStorage,
            IEntitlementsDatabase entitlementDb)
        {
            _library = library;
            _messengerHub = messengerHub;
            _metrics = metrics;
            _launchConfigProvider = launchConfigProvider;
            _shortcutManager = shortcutManager;
            _installHelperController = installHelperController;
            _installedGames = installedGames;
            _manifestStorage = manifestStorage;
            _entitlementDb = entitlementDb;
        }

        public virtual EInstallUpdateProductResult InstallGame(
            ProductId productId, 
            GameInstallInfo installInfo, 
            IHelperLauncherProvider helperLauncherProvider,
            string productVersionId,
            EInstallGameFlags installFlags,
            CancellationToken cancelToken = default(CancellationToken))
        {
            FuelLog.Info("Install game", new { productId, installInfo.InstallDirectory, cancelToken, downloadedVersionId = productVersionId });

            var cfg = _launchConfigProvider.GetLaunchConfig(installInfo.InstallDirectory);
            if (cfg == null)
            {
                FuelLog.Error("Failed to load launch config for game", new {productId.Id, installInfo.InstallDirectory});
                return EInstallUpdateProductResult.ConfigLoadFailure;
            }

            _messengerHub.Publish(new GameInstallingMessage(this, productId));

            var success = InstallPrereqs(
                installInfo: installInfo, 
                installHelperSession: helperLauncherProvider.InstallHelperSession, 
                downloadedVersionId: productVersionId);

            if (cancelToken.IsCancellationRequested)
            {
                return EInstallUpdateProductResult.Cancelled;
            }
            else if ( !success )
            {
                return EInstallUpdateProductResult.PostInstallFailure;
            }

            MarkAsInstalled(installInfo, productVersionId);

            if (installFlags.HasFlag(EInstallGameFlags.RegisterUninstaller))
            {
                // if the uninstaller registration or shortcuts fail, the game is still installed (don't block on failure)
                RegisterUninstaller(
                    productId: productId,
                    installHelperSession: helperLauncherProvider.InstallHelperSession);
            }

            if (installFlags.HasFlag(EInstallGameFlags.CreateDesktopShortcut)
                || installFlags.HasFlag(EInstallGameFlags.CreateStartMenuShortcut))
            { 
                TryCreateShortcutsForProduct(
                    productId: productId, 
                    includeDesktop: installFlags.HasFlag(EInstallGameFlags.CreateDesktopShortcut));
            }

            _messengerHub.Publish(new InstallStateChangedMessage());

            return EInstallUpdateProductResult.Succeeded;
        }

        public void MarkAsInstalled(
            GameInstallInfo installInfo,
            string productVersionId)
        {
            FuelLog.Info("MarkAsInstalled", new { installInfo.InstallDirectory, downloadedVersionId = productVersionId });

            installInfo.InstallVersion = productVersionId;
            installInfo.Installed = true;
            installInfo.InstallDateAsDateTime = DateTime.UtcNow;
            _installedGames.Save(installInfo);
        }

        private bool InstallPrereqs(
            GameInstallInfo installInfo,
            InstallHelperSession installHelperSession,
            string downloadedVersionId)
        {
            var response = _installHelperController.SendAndWaitForNextAsync<IpcInstallPrereqsComplete>(
                installHelperSession,
                new IpcInstallPrereqs(
                    sender: this, 
                    manifestFile: _manifestStorage.GetFilePath(installInfo.ProductId, downloadedVersionId),
                    installDir: installInfo.InstallDirectory)).GetAwaiter().GetResult();

            if (installHelperSession.CancelToken.IsCancellationRequested)
            {
                return false;
            }

            var ret = response.Succeeded
                      && response.Message.Result == IpcInstallPrereqsComplete.EResult.Succeeded;

            if (!ret)
            {
                FuelLog.Error("Game prereqs installation failed",
                    new {result = response.Result, msgResult = response.Message?.Result});

                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "InstallPrereqsFailure");
            }
            else
            {
                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "InstallPrereqsSuccess");
            }

            return ret;
        }

        private bool RegisterUninstaller(
            ProductId productId,
            InstallHelperSession installHelperSession)
        {
            var response = _installHelperController.SendAndWaitForNextAsync<IpcRegisterUninstallerComplete>(
                installHelperSession,
                new IpcRegisterUninstaller(sender: this, adgProdId: productId.Id),
                timeout: TimeSpan.FromSeconds(IpcRegisterInstallerTimeoutSec)).GetAwaiter().GetResult();

            if (installHelperSession.CancelToken.IsCancellationRequested)
            {
                return false;
            }

            var ret = response.Succeeded
                      && response.Message.Result == IpcRegisterUninstallerComplete.EResult.Succeeded;

            if (!ret)
            {
                FuelLog.Error("Game uninstall registration failed", 
                    new { result = response.Result, msgResult = response.Message?.Result });

                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "UninstallRegistrationFailure");
            }
            else
            {
                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "UninstallRegistrationSuccess");
            }

            return ret;
        }

        public bool TryCreateShortcutsForProduct(ProductId productId, bool includeDesktop)
        {
            FuelLog.Info("TryCreateShortcutsForProduct", new {pid = productId, includeDesktop});
            var game = _library.GetGame(productId);

            bool ret = true;
            try
            {
                if (game == null)
                {
                    FuelLog.Warn($"Couldn't create shortcuts for entitlement id (no game found)", new { pid = productId });
                    ret = false;
                }
                else
                {
                    var prodInfo = _entitlementDb.Get(productId);
                    if (prodInfo == null)
                    {
                        FuelLog.Warn($"Couldn't create shortcuts for entitlement id (no product info)", new { pid = productId });
                        ret = false;
                    }
                    else
                    {
                        var installInfo = _library.GetGameInstallInfo(prodInfo.ProductId);
                        if (installInfo == null)
                        {
                            FuelLog.Warn($"Couldn't create shortcuts for entitlement id (no install info)", new { pid = productId });
                            ret = false;
                        }

                        ret = _shortcutManager.TryCreateShortcuts(prodInfo, installInfo, includeDesktop);
                    }
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Couldn't create shortcuts", new { pid = productId } );
                ret = false;
            }

            _messengerHub.Publish(new MsgShortcutsCreated(this, productId, success: ret));

            if (!ret)
            {
                FuelLog.Error("Shortcut create failed");
                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "ShortcutCreateFailure");
            }

            return ret;
        }
        
    }
}