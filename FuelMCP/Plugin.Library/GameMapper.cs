﻿using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.Library
{
    public class GameMapper
    {
        public Game Convert(GameProductInfo gpi)
        {
            return new Game(
                title: gpi.ProductTitle,
                productId: gpi.ProductId,
                productSku: gpi.ProductSku,
                entitlementId: gpi.Id,
                productIconUri: gpi.ProductIconUrl,
                isDeveloper: gpi.IsDeveloper,
                screenshotUrls: gpi.Screenshots,
                videoUrls: gpi.Videos,
                backgroundUrl: gpi.Background,
                backgroundUrl2: gpi.Background2);
        }
    }
}