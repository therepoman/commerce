using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common;

namespace Amazon.Fuel.Plugin.Library
{
    public class GamePrereqInstaller : IGamePrereqInstaller
    {
        private readonly ISecureStoragePlugin _storage;
        private readonly IProcessLauncher _processLauncher;
        private readonly string _storageContext = "MCP.Plugin.Library";

        private const string PrereqSep = "?";

        public GamePrereqInstaller(ISecureStoragePlugin storage, IProcessLauncher processLauncher)
        {
            _storage = storage;
            _processLauncher = processLauncher;
        }

        // secure storage DB last run format: `<filename_with_path>?<comma_delim_args>?lastRun`
        public string GeneratePrereqSpecifier(Executable exe, string installDir)
        {
            var sep = PrereqSep;
            var args = exe.Args.Length == 0 ? "" : (sep + exe.Args.Select(WebUtility.UrlEncode).Aggregate((acc, cur) => acc + sep + cur));
            return $"{Path.Combine(installDir, exe.Command)}{args}{sep}lastRunV2";
        } 

        public void ClearPrereqHasRun(Executable exe, string installDir)
        {
            FuelLog.Info("Clearing prereq has run", new { cmd = exe.Command, args = string.Join(",", exe.Args), installDir });
            try
            {
                var v1Id = GeneratePrereqSpecifierV1(exe, installDir);
                if (!string.IsNullOrEmpty(_storage.Get(_storageContext, v1Id)))
                {
                    _storage.Remove(_storageContext, v1Id);
                }

                var v2Id = GeneratePrereqSpecifier(exe, installDir);
                if (!string.IsNullOrEmpty(_storage.Get(_storageContext, v2Id)))
                {
                    _storage.Remove(_storageContext, v2Id);
                }
            }
            catch ( Exception ex )
            {
                FuelLog.Error(ex, "Couldn't clear prereq", 
                    new {
                        cmd = exe.Command,
                        args = string.Join(",", exe.Args),
                        installDir });
            }
        }

        // version 1 didn't contain any exe args
        private string GeneratePrereqSpecifierV1(Executable exe, string installDir) 
            => $"{Path.Combine(installDir, exe.Command)}?lastRun";

        private bool CheckPrereqNeedsRun(Executable exe, string installDir, string expectedSha)
        {
            var filePath = Path.Combine(installDir, exe.Command);
            if (exe.AlwaysRun)
            {
                FuelLog.Info("Post install needs run AlwaysRun flag detected", new { filePath, args = string.Join(",", exe.Args) });
                return true;
            }

            var id = GeneratePrereqSpecifier(exe, installDir);
            var lastRunSha = _storage.Get(_storageContext, id);
            var result = expectedSha != lastRunSha;

            if (!result 
                && string.IsNullOrEmpty(lastRunSha))
            {
                // support old method (this is only checking updates to installs from old app version))
                var oldId = GeneratePrereqSpecifierV1(exe, installDir);
                var oldLastRunDt = _storage.Get(_storageContext, oldId);
                if (!string.IsNullOrEmpty(oldLastRunDt))
                {
                    FuelLog.Info("Post install check using old datetime method", new { oldId, oldLastRunDt });
                    try
                    {
                        var fileModDate = File.GetLastWriteTimeUtc(filePath);
                        var lastRunDate = DateTime.ParseExact(oldLastRunDt, Constants.ManifestTimeFormat, null);

                        if (lastRunDate < fileModDate)
                        {
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        FuelLog.Warn(ex, "Failed to decode old last run");
                    }
                }
            }

            FuelLog.Info($"Post install needs run checked", new { filePath, args = string.Join(",", exe.Args), result });
            return result;
        }

        private void StorePostInstallLastRun(Executable exe, string sha, string installDir)
        {
            FuelLog.Info("Update last run", new { installDir, exe.Command, exe.Args });
            var id = GeneratePrereqSpecifier(exe, installDir);
            _storage.Put(_storageContext, id, sha);
        }

        public List<PrereqExecutableWithSha> FilterPrereqs(List<PrereqExecutableWithSha> prereqs, string installDirectory)
        {
            var ret = new List<PrereqExecutableWithSha>();
            foreach (var prereq in prereqs)
            {
                if (CheckPrereqNeedsRun(prereq.Exe, installDir: installDirectory, expectedSha: prereq.ExpectedSha))
                {
                    ret.Add(prereq);
                }
            }

            return ret;
        }

        public bool TryInstallPrereqs(List<PrereqExecutableWithSha> prereqs, string installDirectory, CancellationToken cancelToken = default(CancellationToken))
        {
            var result = true;

            foreach (var prereq in prereqs)
            {
                var fileToRun = Path.Combine(installDirectory, prereq.Exe.Command);
                string appWithArgs = $"'{fileToRun} {string.Join(",", prereq.Exe.Args)}'";

                FuelLog.Info("Running prereq", new { appWithArgs });

                if (string.IsNullOrEmpty(prereq.Exe.Command))
                    continue;

                int[] validReturns = null;
                if (prereq.Exe.ValidReturns?.Length > 0)
                {
                    validReturns = prereq.Exe.ValidReturns;
                }

                Stopwatch sw = new Stopwatch();
                sw.Start();

                var launchResult = _processLauncher.LaunchProcess(
                    description: "PostInstall",
                    launchFolder: null,
                    pathToFile: fileToRun,
                    args: prereq.Exe.Args,
                    waitForExit: true,
                    validateAgainstReturnCodes: validReturns,
                    eLaunchUac: ELaunchUAC.RUN_WITH_CURRENT_PRIVS__THIS_ALLOWS_ELEVATION,
                    bringToForeground: false);

                switch (launchResult.Status)
                {
                    case LaunchResult.EStatus.Success:
                        // only store sha when successful
                        StorePostInstallLastRun(prereq.Exe, sha: prereq.ExpectedSha, installDir: installDirectory);
                        break;

                    case LaunchResult.EStatus.ReturnCodeError:
                        FuelLog.Error("Install prereq return code validation failed", new { launchResult.ExitCode, validReturns=string.Join(",", validReturns ?? new int[] {})});
                        break;

                    case LaunchResult.EStatus.ThrewException:
                        result = false;
                        FuelLog.Error(launchResult.Exception, "Install prereq exception hit");
                        break;
                    case LaunchResult.EStatus.AlreadyRunning:
                    case LaunchResult.EStatus.FileNotFound:
                    case LaunchResult.EStatus.LaunchFailure:
                        result = false;
                        break;
                    case LaunchResult.EStatus.UacRequestRejected:
                        result = false;
                        FuelLog.Error("Escalation failed on process (did we try to install non-escalated?)", new { appWithArgs });
                        break;
                    default:
                        Debug.Fail($"Unhandled LaunchResult.EStatus '{launchResult.Status}'");
                        goto case LaunchResult.EStatus.LaunchFailure;
                }

                FuelLog.Info("Install prereq completed", new { result, launchResult.Status, launchResult.ExitCode, durationSec = sw.Elapsed.TotalSeconds });

                if (!result)
                {
                    FuelLog.Error("Prereq installation process failed", new {appWithArgs, launchResult.Status});
                    break;  // we failed, don't run further actions
                }

                // [https://twitchtv.atlassian.net/browse/FLCL-303] we should cancel processes earlier, not let them complete and test here
                if (cancelToken.IsCancellationRequested)
                {
                    return false;
                }
            }

            return result;
        }
        
    }
}