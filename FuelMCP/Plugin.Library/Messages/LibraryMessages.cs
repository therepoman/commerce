﻿using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Library.Messages
{
    // Misc Messaging
    //////////////////////////////////////////////////////

    public class MsgShortcutsCreated : GenericTinyMessage<ProductId>
    {
        public ProductId ProductId { get { return Content; } }
        public bool Success { get; private set; }
        public MsgShortcutsCreated(object sender, ProductId pid, bool success) : base(sender, pid)
        {
            Success = success;
        }
    }
}
