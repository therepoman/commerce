﻿using System.Linq;
using System.Collections.Generic;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using System.Threading.Tasks;
using Serilog;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.IPC;

namespace Amazon.Fuel.Plugin.Library
{
    public class LibraryPlugin : ILibraryPlugin
    {
        private readonly IMessengerHub _messengerHub;
        private readonly IEntitlementsDatabase _entitlementDb;
        private readonly IInstalledGamesPlugin _installedGames;
        private readonly IInstalledGameSynchronizer _installedGameSynchronizer;

        // TODO secondary cache in DB?
        public Dictionary<ProductId, GameStateInfo> CachedGameStates { get; private set; } = new Dictionary<ProductId, GameStateInfo>();

        public LibraryPlugin(
            IMessengerHub messengerHub, 
            IEntitlementsDatabase entitlementDb,
            IInstalledGamesPlugin installedGames,
            IInstalledGameSynchronizer installedGameSynchronizer)
        {
            _messengerHub = messengerHub;
            _entitlementDb = entitlementDb;
            _installedGames = installedGames;
            _installedGameSynchronizer = installedGameSynchronizer;
        }

        public bool GameRegistered(ProductId productId) => GetGame(productId) != null;
        public List<Game> OwnedGames {
            get
            {
                Log.Information($"LibraryPlugin.OwnedGames()");
                var mapper = new GameMapper();
                // TODO: https://twitchtv.atlassian.net/browse/FLCL-193 Add Pagination to Libary
                var gpis = _entitlementDb.ActiveEntitlements;
                return gpis.Select(g => mapper.Convert(g)).ToList<Game>();
            }
        } 

        public async Task<bool> UpdateEntitledGamesList() => await _entitlementDb.SyncGoods();

        public Game GetGame(ProductId productId)
        {
            Log.Information($"LibraryPlugin.GetGame({productId})");
            GameProductInfo gpi = _entitlementDb.Get(productId);
            if (gpi == null)
            {
                return null;
            }
            return new GameMapper().Convert(gpi);
        }

        public GameInstallInfo GetGameInstallInfo(ProductId productId)
        {
            return _installedGames.Get(productId);
        }

        public List<Game> InstallableGames
        {
            get
            {
                Log.Information($"LibraryPlugin.InstallableGames()");
                return OwnedGames.Where(g => _installedGames.IsInstalled(g.ProductId)) as List<Game>;
            }
        }

        public void ClearEntitlementsDb()
        {
            Log.Information($"[LIB] Clearing all entitlements");
            _entitlementDb.RemoveAll();
        }

        public void RemoveInvalidGamesFromDb()
        {
            _installedGameSynchronizer.RemoveInvalidGames();
        }

        /// <summary>
        /// Gets a list of games from the game library.
        /// </summary>
        public async Task<GameInstallInstancesRefreshed> TryRefreshGamesListAsync(ELibraryRefreshType refreshType = ELibraryRefreshType.Full, ProductId[] filter = null)
        {
            Log.Information($"[LIB] InstallUpdateController.RefreshGamesList('{refreshType}', '{filter?.Select(a => a.Id).Aggregate((a,b) => a + "," + b)}')");
            bool changeDetected = false;

            if (refreshType == ELibraryRefreshType.Full)
            {
                changeDetected = await UpdateEntitledGamesList();
                CachedGameStates.Clear();
            }

            var ownedGames = OwnedGames;

            // revalidate our games list
            RemoveInvalidGamesFromDb();

            List<Game> invalidatedGames = new List<Game>();
            foreach (Game game in ownedGames)
            {
                // apply filter, if provided
                if (!filter?.Contains(game.ProductId) ?? false)
                {
                    continue;
                }

                EGameInstallState gameInstallState = EGameInstallState.NotInstalled;
                EGameRunningState gameRunningState = EGameRunningState.NotRunning;

                var installInfo = _installedGames.Get(game.ProductId);
                if (installInfo?.Installed ?? false)
                {
                    gameInstallState = EGameInstallState.Installed;

                    // check IsRunning
                    if (_installedGames.IsRunning(game.ProductId))
                    {
                        gameRunningState = EGameRunningState.Running;
                    }
                    else if (refreshType != ELibraryRefreshType.Offline_ProcessRunStateOnly)
                    {
                        ERetrieveVersionCondition retrieveCondition;
                        switch (refreshType)
                        {
                            case ELibraryRefreshType.Full:
                                retrieveCondition = ERetrieveVersionCondition.ForceUpdate;
                                break;
                            case ELibraryRefreshType.Cached:
                                retrieveCondition = ERetrieveVersionCondition.UpdateIfExpired;
                                break;
                            case ELibraryRefreshType.Offline_Cached:
                                retrieveCondition = ERetrieveVersionCondition.Offline;
                                break;
                            default:
                                Log.Error($"Unhandled type '{refreshType}'");
                                goto case ELibraryRefreshType.Full;
                        }
                            
                        var isUpToDateResult = await _installedGames.CheckUpToDateByInstallInfo(
                            installInfo,
                            retrieveCondition);

                        bool gameInvalid = false;;
                        switch (isUpToDateResult)
                        {
                            case ECheckUpToDateResult.RemoteVersionInvalid:
                                // This means that maybe we lost our entitlement immediately after SyncGoods, got a bad SyncGoods response, or 
                                //      our version call simply failed? Game appears invalid, remove game from list.
                                // https://twitchtv.atlassian.net/browse/FLCL-956 -- research invalid conditions
                                // https://twitchtv.atlassian.net/browse/FLCL-1075 -- add retry logic
                                gameInvalid = true;
                                break;
                            case ECheckUpToDateResult.NetworkFailure:
                                Log.Warning("Network Failure encountered when trying to update.");
                                break;
                            case ECheckUpToDateResult.NoProductInDb:
                            case ECheckUpToDateResult.NotInstalled:
                            case ECheckUpToDateResult.EntitlementNotFound:
                                Log.Error($"Unexpected isUpToDate result encountered {isUpToDateResult}");
                                break;
                            default:
                                Log.Error($"Unhandled isUpToDate result encountered {isUpToDateResult}");
                                goto case ECheckUpToDateResult.RemoteVersionInvalid;
                            case ECheckUpToDateResult.NeedsUpdate:
                                gameInstallState = EGameInstallState.InstalledAndNeedsUpdate;
                                break;
                            case ECheckUpToDateResult.UpToDate:
                                break;
                        }

                        if (gameInvalid)
                        {
                            invalidatedGames.Add(game);
                            continue;
                        }
                    }
                }

                GameStateInfo oldState = null;
                if (!CachedGameStates.TryGetValue(game.ProductId, out oldState)
                    || oldState?.InstallState != gameInstallState)
                {
                    changeDetected = true;
                }

                CachedGameStates[game.ProductId] =
                    new GameStateInfo()
                    {
                        GameRef = game,
                        GameInstallInfoRef = installInfo,
                        InstallState = gameInstallState,
                        RunningState = gameRunningState,
                        IsDeveloperMode = _entitlementDb.Get(game.ProductId)?.IsDeveloper ?? false
                    };
            }

            if ((invalidatedGames?.Count ?? 0 ) > 0)
            {
                _entitlementDb.RemoveManyById(invalidatedGames.Select(game => game.ProductId.Id).ToList());
            }

            var ret = new GameInstallInstancesRefreshed(
                                    games: new Dictionary<ProductId, GameStateInfo>(CachedGameStates),
                                    refreshType: refreshType,
                                    changeDetected: changeDetected);

            _messengerHub.Publish(new GameInstallInstancesRefreshedMessage(sender: this, refreshed: ret));

            return ret;
        }
    }
}
