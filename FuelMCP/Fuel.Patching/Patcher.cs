﻿using System;
using System.IO;
using System.IO.Compression;
using Zstandard.Net;

namespace Tv.Twitch.Fuel.Patching
{
    /// <summary>
    /// Used to apply Fuel patches. For a description of the patch file format see: https://tiny.amazon.com/2qp0ekq
    /// </summary>
    public class Patcher
    {
        // Original uncompressed patches
        private const byte FuelPatchFormatId = 0;
        // New patches compressed with zstd
        private const byte FuelPatchZstdFormatId = 1;

        // Patching operations
        public enum OpCode : byte
        {
            Seek,
            Copy,
            Insert,
            Merge
        }

        readonly Stream _source;
        readonly Stream _rawPatchStream;
        readonly Stream _target;

        // These are used during MergeWith and CopyTo calls. Allocaating here
        // to avoid repeated allocations as operations are applied.
        private const int MergeBufferSize = 65536;
        private readonly byte[] _sourceBuffer = new byte[MergeBufferSize];
        private readonly byte[] _patchBuffer = new byte[MergeBufferSize];

        /// <summary>
        /// Construct a new patcher. Passed in streams should be closed and disposed by the caller.
        /// </summary>
        /// <param name="source">the source</param>
        /// <param name="patch">the patch</param>
        /// <param name="target">the patch output</param>
        public Patcher(Stream source, Stream patch, Stream target)
        {
            _source = source;
            _rawPatchStream = patch;
            _target = target;
        }

        public void Apply()
        {
            // Read the patch format id
            var formatId = _rawPatchStream.ReadByte();
            if (formatId == FuelPatchFormatId)
            {
                using (var patch = new BinaryReader(_rawPatchStream))
                {
                    ApplyAll(patch);
                }
            }
            else if (formatId == FuelPatchZstdFormatId)
            {
                using (var zstdStream = new ZstandardStream(_rawPatchStream, CompressionMode.Decompress))
                using (var patch = new BinaryReader(zstdStream))
                {
                    ApplyAll(patch);   
                }
            }
            else
            {
                throw new Exception($"Unknown patch format id {formatId}");
            }
        }

        private void ApplyAll(BinaryReader patch)
        {
            OpCode operation;
            long length;
            for (var readInstruction = ReadNextInstruction(patch, out operation, out length);
                readInstruction;
                readInstruction = ReadNextInstruction(patch, out operation, out length))
            {
                ApplyInstruction(patch, operation, length);
            }
        }

        /// <summary>
        /// Reads the next patch instruction.
        /// </summary>
        /// <returns>true if instruction was read, false if end of patch</returns>
        private bool ReadNextInstruction(BinaryReader patch, out OpCode operation, out long length)
        {
            // Read the instruction
            try
            {
                int b = patch.ReadByte();
                operation = (OpCode)Enum.ToObject(
                    typeof(OpCode), b >> 6);
                length = GetNextLength(patch, b, operation == OpCode.Seek ? 5 : 6);

                if (operation == OpCode.Seek && (b & 0x20) != 0)
                {
                    length = -length;
                }

                return true;
            }
            catch (EndOfStreamException)
            {
                operation = OpCode.Seek;
                length = 0;
                return false;
            }

        }

        private bool HasNext(BinaryReader patch)
        {
            Stream s = patch.BaseStream;
            return s.Length != s.Position;
        }

        private long GetNextLength(BinaryReader patch, long encodedLength, int bitCount)
        {
            var mask = (1 << bitCount) - 1;
            var length = encodedLength & mask;
            if (length < mask - 4) return length + 1;
            var bytesToRead = (int)(length - (mask - 5));
            var lengthBytes = patch.ReadBytes(bytesToRead);

            length = 0;
            foreach (var b in lengthBytes)
            {
                length <<= 8;
                length |= b;
            }
            return length + 1;
        }

        private void ApplyInstruction(BinaryReader patch, OpCode operation, long length)
        {
            switch (operation)
            {
                case OpCode.Seek:
                    _source.Position += length;
                    break;
                case OpCode.Copy:
                    CopyTo(_source, _target, length);
                    break;
                case OpCode.Insert:
                    CopyTo(patch.BaseStream, _target, length);
                    break;
                case OpCode.Merge:
                    MergeWith(_source, patch.BaseStream, _target, length);
                    break;
            }
        }

        /// <summary>
        /// Reads information from the source and patch, sums (merges) the data, and writes it to the output stream.
        /// This operation is part of bsdiff delta-encoding.
        /// </summary>
        /// <param name="source">the source stream</param>
        /// <param name="patch">the patch stream</param>
        /// <param name="output">the output stream</param>
        /// <param name="count">how many bytes to merge</param>
        private void MergeWith(Stream source, Stream patch, Stream output, long count)
        {
            if (count < 0)
            {
                throw new ArgumentException("must be greater than zero", nameof(count));
            }
            int bufferSize = Math.Min(32768, (int)count); // 32 KiB or count if smaller

            long bytesLeft = count;
            while (bytesLeft > 0)
            {
                var read = source.Read(_sourceBuffer, 0, Math.Min(bufferSize, (int)bytesLeft));
                if (read < 1)
                {
                    throw new ArgumentException("longer than source stream", nameof(count));
                }
                if (patch.Read(_patchBuffer, 0, read) != read)
                {
                    throw new ArgumentException("longer than patch stream", nameof(count));
                }

                // merge the bytes
                for (int i = 0; i < read; i++)
                {
                    output.WriteByte((byte)(_sourceBuffer[i] + _patchBuffer[i]));
                }

                bytesLeft -= read;
            }
        }

        private void CopyTo(Stream input, Stream output, long count)
        {
            if (count < 0)
            {
                throw new ArgumentException("must be greater than zero", nameof(count));
            }

            long bytesLeft = count;
            int read = 0;
            while ((read = input.Read(_sourceBuffer, 0, Math.Min(_sourceBuffer.Length, (int)bytesLeft))) > 0)
            {
                output.Write(_sourceBuffer, 0, read);
                bytesLeft -= read;
            }

            if (bytesLeft > 0)
            {
                throw new ArgumentException($"was {bytesLeft} bytes longer than stream", nameof(count));
            }
        }
    }
}
