using System.IO;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Serilog;

namespace Amazon.Fuel.Plugin.InstalledGames
{
    public class InstalledGameValidator : IInstalledGameValidator
    {
        private readonly IOSDriver _osDriver;

        public InstalledGameValidator(
            IOSDriver osDriver)
        {
            _osDriver = osDriver;
        }

        public bool IsGameInstallValid(string libraryDir, string gameId)
        {
            string gamePath = Path.Combine(libraryDir, gameId);

            var requiredFiles = new string[]
            {
                Path.Combine(gamePath, Constants.Paths.FuelGameConfigFileName)
            };

            foreach (var requiredFile in requiredFiles)
            {
                if (!File.Exists(requiredFile))
                {
                    Log.Error($"ResyncCache(): Found game id '{gameId}', but '{requiredFile}' is missing");
                    return false;
                }
            }

            return true;
        }
    }
}