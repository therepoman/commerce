using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Serilog;

namespace Amazon.Fuel.Plugin.InstalledGames
{
    public class InstalledGameSynchronizer : IInstalledGameSynchronizer
    {
        private readonly IGameInstallInfoDatabase _installInfoDb;
        private readonly IInstalledGameValidator _installedGameValidator;
        private readonly IOSDriver _osDriver;
        private readonly IMetricsPlugin _metrics;
        private readonly IUninstallPlugin _uninstall;

        public InstalledGameSynchronizer(
            IGameInstallInfoDatabase installInfoDb,
            IInstalledGameValidator installedGameValidator,
            IOSDriver osDriver,
            IMetricsPlugin metrics,
            IUninstallPlugin uninstall)
        {
            _installInfoDb = installInfoDb;
            _installedGameValidator = installedGameValidator;
            _osDriver = osDriver;
            _metrics = metrics;
            _uninstall = uninstall;
        }

        // search all known game library directories and add/remove installs based on validity check
        public List<string> RemoveInvalidGames()
        {
            var ret = new List<string>();
            Log.Information("InstalledGameSynchronizer.RemoveInvalidGames()");

            foreach (var info in _installInfoDb.All)
            {
                var id = info.Id;
                if (!_installedGameValidator.IsGameInstallValid(libraryDir: info.LibraryDirectory, gameId: id))
                {
                    Log.Error($"Game invalid for id '{id}' where game data dir & db entry still exist");

                    info.Installed = false;
                    _installInfoDb.AddOrReplaceById(info);

                    _metrics.AddCounter("InstalledGameSynchronizer", "GameInstallInvalid");
                    ret.Add(id);
                }
            }

            if (_installInfoDb.AwaitingCleanup.Count > 0)
            {
                Log.Information($"There are {_installInfoDb.AwaitingCleanup.Count} games awaiting cleanup");
            }

            return ret;
        }
    }
}