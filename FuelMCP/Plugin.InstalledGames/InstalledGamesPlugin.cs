﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Coral.Model;
using Amazon.Fuel.Plugin.InstalledGames.Messages;
using Serilog;

namespace Amazon.Fuel.Plugin.InstalledGames
{
    public class InstalledGamesPlugin : IInstalledGamesPlugin
    {
        public readonly TimeSpan UpdateCheckExpiration = TimeSpan.FromMinutes(5);

        private readonly IGameInstallInfoDatabase _db;
        private readonly IEntitlementPlugin _entitlement;
        private readonly IEntitlementsDatabase _entitlementDb;
        private readonly ILaunchConfigProvider _launchConfig;
        private readonly IProcessLauncher _processLauncher;
        private readonly IMessengerHub _hub;
        private readonly IClock _clock;

        public InstalledGamesPlugin(
            IGameInstallInfoDatabase db,
            IEntitlementPlugin entitlement,
            IEntitlementsDatabase entitlementsDatabase,
            ILaunchConfigProvider launchConfig,
            IProcessLauncher processLauncher,
            IClock clock,
            IMessengerHub hub)
        {
            _db = db;
            _entitlement = entitlement;
            _entitlementDb = entitlementsDatabase;
            _launchConfig = launchConfig;
            _processLauncher = processLauncher;
            _clock = clock;
            _hub = hub;
        }

        public void Save(GameInstallInfo info)
        {
            if (info == null)
            {
                Log.Error("null info passed to Save()");
                return;
            }

            Log.Information($"InstalledGamesPlugin.Save({info.ProductId})");
            _db.AddOrReplaceById(info);
        }

        public GameInstallInfo Get(ProductId productId)
        {
            //Log.Information($"InstalledGamesPlugin.Get({productId})");
            return _db.TryFindById(productId.Id);
        }

        public bool IsInstalled(ProductId productId)
        {
            Log.Information($"InstalledGamesPlugin.IsInstalled({productId})");
            var info = _db.TryFindById(productId.Id);
            if (info == null)
            {
                return false;
            }

            if (!System.IO.Directory.Exists(info.InstallDirectory))
            {
                Log.Warning(
                    $"Product '{productId}' state is installed but directory missing '{info.InstallDirectory}', removing from db");

                info.Installed = false;
                _db.AddOrReplaceById(info);
                return false;
            }

            return info.Installed;
        }

        public bool IsEntitledInDb(ProductId productId)
        {
            return _entitlementDb.Get(productId) != null;
        }

        public void ClearInstalledVersion(ProductId pid)
        {
            var info = Get(pid);

            if (info == null)
            {
                Log.Error($"Couldn't find install info for pid {pid}");
                return;
            }

            Log.Information($"Cleared installed version: {pid}");
            info.InstallVersion = "";
            Save(info);
        }

        public async Task<ECheckUpToDateResult> CheckUpToDateByProductId(ProductId productId,
            ERetrieveVersionCondition retrieveCondition)
        {
            var installInfo = _db.TryFindById(productId.Id);
            if (installInfo == null)
            {
                Log.Warning($"CheckUpToDateByInstallInfo() -- no install info found for pid '{productId}'");
                return ECheckUpToDateResult.NoProductInDb;
            }

            return await CheckUpToDateByInstallInfo(installInfo, retrieveCondition);
        }

        public async Task<ECheckUpToDateResult> CheckUpToDateByInstallInfo(GameInstallInfo installInfo,
            ERetrieveVersionCondition retrieveCondition)
        {
            var adgProductId = installInfo.ProductId;
            if (!IsEntitledInDb(adgProductId))
            {
                Log.Warning($"Couldn't find entitlement in db for '{installInfo.ProductId}'");
                return ECheckUpToDateResult.EntitlementNotFound;
            }

            if (!installInfo.Installed)
            {
                Log.Warning($"CheckUpToDateByInstallInfo() -- non-installed product '{adgProductId.Id}'");
                return ECheckUpToDateResult.NotInstalled;
            }

            bool needRefreshVersion;
            switch (retrieveCondition)
            {
                case ERetrieveVersionCondition.ForceUpdate:
                    needRefreshVersion = true;
                    break;
                case ERetrieveVersionCondition.UpdateIfExpired:
                    needRefreshVersion = string.IsNullOrEmpty(installInfo.LastKnownLatestVersion)
                                        || installInfo.LastKnownLatestVersion == "<not_checked>"    // legacy
                                        || _clock.Now() - installInfo.LastKnownLatestVersionTimestampAsDateTime >= UpdateCheckExpiration;
                    if (needRefreshVersion)
                    {
                        Log.Information($"Version expired for product '{installInfo.ProductId}', needs refresh");
                    }
                    break;
                case ERetrieveVersionCondition.Offline:
                    needRefreshVersion = false;
                    break;
                default:
                    Log.Error($"Unhandled retrieve condition {retrieveCondition}");
                    goto case ERetrieveVersionCondition.ForceUpdate;
            }

            if (needRefreshVersion)
            {
                try
                {
                    var adgProductIds = new List<string> {adgProductId.Id};
                    var versions = await _entitlement.CurrentVersionIdsForAdgProductIds(adgProductIds);
                    string version;

                    // the server returns null for version. This is unexpected and should never happen.
                    if (!versions.TryGetValue(adgProductId.Id, out version))
                    {
                        FuelLog.Error("Couldn't get version", new {prodId=installInfo.ProductId});
                        return ECheckUpToDateResult.RemoteVersionInvalid;
                    }

                    installInfo.LastKnownLatestVersionTimestampAsDateTime = _clock.Now();
                    if (installInfo.LastKnownLatestVersion != version)
                    {
                        var oldVersion = installInfo.LastKnownLatestVersion;
                        FuelLog.Info("Updating latest product version", new { prodId=installInfo.ProductId, oldVersion, newVersion=version } );
                        installInfo.LastKnownLatestVersion = version ?? "";
                        _db.AddOrReplaceById(installInfo);

                        _hub.Publish(new ProductVersionUpdatedMessage(
                            sender: this,
                            prodId: installInfo.ProductId,
                            oldVersion: oldVersion,
                            newVersion: installInfo.LastKnownLatestVersion));
                    }
                }
                catch (HttpRequestException e)
                {
                    Log.Warning(e,
                        $"Network failure when trying to get the current version for ADGProductID - '{installInfo.ProductId}'");
                    return ECheckUpToDateResult.NetworkFailure;
                }
                catch (CoralException e)
                {
                    FuelLog.Error(e, "Exception when trying to get the current version" , new { installInfo.ProductId });
                    return ECheckUpToDateResult.RemoteVersionInvalid;
                }
            }

            return installInfo.LastKnownLatestVersion == installInfo.InstallVersion
                ? ECheckUpToDateResult.UpToDate
                : ECheckUpToDateResult.NeedsUpdate;
        }

        public List<GameInstallInfo> All => _db.All;

        // $ jbil #REFACTOR -- a lot of this is dupe code from LibraryPlugin, but I don't want to pull in all dependencies.
        public bool IsRunning(ProductId productId)
        {
            var installInfo = Get(productId);
            if (installInfo == null)
            {
                return false;
            }
            var installFolder = installInfo.InstallDirectory;

            var cfg = _launchConfig.GetLaunchConfig(installFolder);
            if (cfg == null)
            {
                return false;
            }

            if (cfg.Main == null)
            {
                Log.Error($"[InstalledGamesPlugin.IsRunning] {productId} in {installFolder} has no Main!");
                return false;
            }

            var fileToRun = System.IO.Path.Combine(installFolder, cfg.Main.Command);
            return _processLauncher.IsRunning(fileToRun);
        }
    }
}
