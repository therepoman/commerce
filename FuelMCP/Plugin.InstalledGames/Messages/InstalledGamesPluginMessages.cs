using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.InstalledGames.Messages
{
    public class ProductVersionUpdatedMessage : ITinyMessage
    {
        public object Sender { get; private set; }
        public ProductId ProdId { get; private set; }
        public string OldVersion { get; private set; }
        public string NewVersion { get; private set; }

        public ProductVersionUpdatedMessage(object sender, ProductId prodId, string oldVersion, string newVersion)
        {
            Sender = sender;
            ProdId = prodId;
            OldVersion = oldVersion;
            NewVersion = newVersion;
        }
    }
}