﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Plugin.Launcher.Messages;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public class RepairGameResultReporter : GameResultReporter<ERepairProductResult>
    {
        public RepairGameResultReporter(
            IMetricsPlugin metrics,
            IMessengerHub hub,
            ProductId id,
            ITitleProvider titleProvider)
            : base(metrics, hub, titleProvider, id) { }

        protected override string ServiceName => "RepairGame";

        protected override string MetricSource => MetricStrings.SOURCE_RepairGame;
        protected override string MetricQosSource => MetricStrings.SOURCE_RepairGameQos;
        
        protected override string MetricExpectedFailure => MetricStrings.SOURCE_RepairGameExpectedFailure;
        protected override string MetricUnexpectedFailure => MetricStrings.SOURCE_RepairGameUnexpectedFailure;

        protected override bool Succeeded => Result == ERepairProductResult.Succeeded;
        protected override bool Cancelled => Result == ERepairProductResult.Cancelled;
        protected override bool UnexpectedFailure => RepairProductResultHelpers.IsUnexpectedFailure(Result);

        protected override void SendSuccessMessage()
        {
            var msg = new MessageRepairProductSucceeded(this, ProdId, _titleProvider.TryGetTitle(ProdId));
            _hub.Publish(msg);
        }

        protected override void SendFailureMessage()
        {
            var msg = new MessageRepairProductFailed(this, Result, ProdId, _titleProvider.TryGetTitle(ProdId));
            _hub.Publish(msg);
        }
    }
}
