using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.CommandLineParser;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Entitlement.Models;
using Amazon.Fuel.Plugin.Launcher.Messages;
using Clients.Twitch.Spade.Model.Event;
using Serilog;
using Amazon.Fuel.Plugin.Download.Messages;
using Amazon.Fuel.Common.Exceptions;
using Amazon.Fuel.Common.IPC;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public interface ITitleProvider
    {
        string TryGetTitle(ProductId id);
    }

    public class FuelGameInstallerService : IFuelGameInstallerService, ITitleProvider
    {
        // Add some delays for failure cases during download retries. These are just countdown clocks to delay
        // in case of outage, the user will have the option to "retry now"
        private static readonly int[] DownloadRetryWait = new int[] { 3, 5 };
        private static readonly TimeSpan InitialReqIpcTimeoutSec = TimeSpan.FromSeconds(30);
        private static readonly TimeSpan PrimaryPathCreateIpcTimeoutSec = TimeSpan.FromSeconds(30);

        private object _busyLock = new { };
        public ProductId InstallInProgressProdId { get; private set; } = null;

        private readonly ILibraryPlugin _library = null;
        private readonly IGameInstaller _gameInstaller = null;
        private readonly IGamePrereqInstaller _prereqInstaller = null;
        private readonly IOSDriver _osDriver;
        private readonly IMessengerHub _hub = null;
        private readonly IMetricsPlugin _metrics = null;
        private readonly IDownloadPlugin _downloadPlugin = null;
        private readonly IEntitlementsDatabase _entitlementDb = null;
        private readonly IProcessLauncher _processLauncher = null;
        private readonly IOSServicesDriver _osServicesDriver;
        private readonly IUninstallPlugin _uninstall;
        private readonly IManifestStorage _manifestStorage;
        private readonly ILauncherServices _launcherServices;
        private readonly IInstalledGamesPlugin _installedGames;
        private readonly ILaunchConfigProvider _launchConfig;
        private readonly IDeferredDeletionService _deferredDeletionService;
        private readonly IInstallHelperIpcController _installHelperController;

        public bool IsBusy => InstallInProgressProdId != null;
        public bool IsInstallInProgress => InstallInProgressProdId != null;


        public FuelGameInstallerService(
            IOSDriver osDriver,
            IMessengerHub hub,
            IMetricsPlugin metrics,
            IEntitlementsDatabase entitlementDb,
            IDownloadPlugin downloadPlugin,
            ILibraryPlugin library,
            IGameInstaller gameInstaller,
            IGamePrereqInstaller prereqInstaller,
            IProcessLauncher processLauncher,
            IOSServicesDriver osServicesDriver,
            IUninstallPlugin uninstall,
            IManifestStorage manifestStorage,
            ILauncherServices launcherServices,
            IInstalledGamesPlugin installedGames,
            ILaunchConfigProvider launchConfig,
            IDeferredDeletionService deferredDeletionService,
            IInstallHelperIpcController installHelperController)
        {
            _osDriver = osDriver;
            _hub = hub;
            _metrics = metrics;
            _library = library;
            _gameInstaller = gameInstaller;
            _prereqInstaller = prereqInstaller;
            _downloadPlugin = downloadPlugin;
            _entitlementDb = entitlementDb;
            _processLauncher = processLauncher;
            _osServicesDriver = osServicesDriver;
            _uninstall = uninstall;
            _manifestStorage = manifestStorage;
            _launcherServices = launcherServices;
            _installedGames = installedGames;
            _launchConfig = launchConfig;
            _deferredDeletionService = deferredDeletionService;
            _installHelperController = installHelperController;
        }

        public EInstallUpdateProductResult InstallOrUpdateProduct(
            ProductId adgProdId,
            string libraryDirectory,
            bool isUpdate,
            bool installDesktopShortcut,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // these indirection shenanigans are necessary since `using (r = isUpdate ? new a : new b)` won't compile
            GameResultReporter<EInstallUpdateProductResult> NewInstallReporter() 
                => new InstallGameResultReporter(_metrics, _hub, adgProdId, this);
            GameResultReporter<EInstallUpdateProductResult> NewUpdateReporter() 
                => new UpdateGameResultReporter(_metrics, _hub, adgProdId, this);

            using (var reporter = isUpdate ? NewUpdateReporter() : NewInstallReporter())
            {
                Exception outException;
                reporter.Result = InstallOrUpdateProductNoReporter(
                    adgProdId: adgProdId,
                    libraryDirectory: libraryDirectory,
                    isUpdate: isUpdate,
                    installDesktopShortcut: installDesktopShortcut,
                    alwaysRerunPrereqs: false,
                    outException: out outException,
                    cancellationToken: cancellationToken);

                reporter.ResultException = outException;
                return reporter.Result;
            }
        }

        /// <summary>
        /// This is broken out to allow the repair and install/update processes to share code
        /// </summary>
        /// <returns></returns>
        private EInstallUpdateProductResult InstallOrUpdateProductNoReporter(
            ProductId adgProdId,
            string libraryDirectory,
            bool isUpdate,
            bool installDesktopShortcut,
            bool alwaysRerunPrereqs,
            out Exception outException,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            outException = null;
            ProductId installationSessionId = null;
            var gameDir = Path.Combine(libraryDirectory, adgProdId.Id);
            var installResult = EInstallUpdateProductResult.GenericFailure;

            try
            {
                _deferredDeletionService.TryUnflagForDeferredDelete(gameDir);

                var transactionId = Guid.NewGuid().ToString();
                using (CancelTaskHandler cancelHandler = new CancelTaskHandler(_hub, adgProdId))
                using (cancellationToken.Register(() => _hub.Publish(new MessageDownloadCancel(this, adgProdId))))
                {
                    _hub.Publish<FuelRequestBlockUpdate>(new FuelRequestBlockUpdate(this));

                    // some errors have been encountered with null pids
                    if (string.IsNullOrEmpty(adgProdId?.Id))
                    {
                        FuelLog.Error($"Encountered a null/empty product id when installing", new { pid = adgProdId, isUpdate });
                        return EInstallUpdateProductResult.InvalidProductId;
                    }

                    if (cancelHandler.IsCanceled)
                    {
                        return EInstallUpdateProductResult.Cancelled;
                    }

                    lock (_busyLock)
                    {
                        if (IsBusy)
                        {
                            FuelLog.Info($"User attempted to install/update while install/update already occuring", new { oldId = InstallInProgressProdId, newId = adgProdId, isUpdate });
                            return EInstallUpdateProductResult.Busy;
                        }

                        installationSessionId = adgProdId;
                        InstallInProgressProdId = adgProdId;
                    }

                    installResult = InstallOrUpdateProductWithSleepProtect(
                        adgProdId: adgProdId,
                        isUpdate: isUpdate,
                        installLibraryDirectory: libraryDirectory,
                        installDesktopShortcut: installDesktopShortcut,
                        alwaysRerunPrereqs: alwaysRerunPrereqs,
                        cancelHandler: cancelHandler,
                        transactionId: transactionId).GetAwaiter().GetResult();
                }
            }
            catch (TwitchAuthException ex)
            {
                FuelLog.Error(ex, "Auth failure during install or update", new { adgProdId, isUpdate });
                return EInstallUpdateProductResult.AuthFailure;
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Failed to install or update", new { adgProdId, isUpdate });
                outException = ex;
                return EInstallUpdateProductResult.GenericInstallOrUpdateProductException;
            }
            finally
            {
                _hub.Publish<FuelFreeBlockUpdate>(new FuelFreeBlockUpdate(this));

                // If we failed and we're not updating, we don't need the install dir. Remove the old files.
                if (!isUpdate && installResult != EInstallUpdateProductResult.Succeeded)
                {
                    FuelLog.Warn("Installation not complete, removing old files");
                    _deferredDeletionService.FlagForDeferredDelete(gameDir, deleteNow: true);
                }

                if (installationSessionId == InstallInProgressProdId)
                {
                    InstallInProgressProdId = null;
                }
            }

            return installResult;
        }

        public ERepairProductResult RepairProduct(
            ProductId productId,
            bool deleteAllFiles,
            bool installDesktopShortcut,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var resultReporter = new RepairGameResultReporter(_metrics, _hub, productId, this))
            {
                try
                {
                    Exception ex;
                    resultReporter.Result = RepairProductNoReporter(
                        productId, 
                        deleteAllFiles: deleteAllFiles, 
                        installDesktopShortcut: installDesktopShortcut,
                        cancellationToken: cancellationToken,
                        outException: out ex);
                    resultReporter.ResultException = ex;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Exception hit during repair", new { productId });
                    resultReporter.Result = ERepairProductResult.GenericFailureException;
                    resultReporter.ResultException = ex;
                }

                return resultReporter.Result;
            }
        }

        private ERepairProductResult RepairProductNoReporter(
            ProductId productId,
            bool deleteAllFiles,
            bool installDesktopShortcut,
            out Exception outException,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            outException = null;
            var startDt = DateTime.UtcNow;
            FuelLog.Info("Repairing product", new { prodId = productId.Id });

            var installedGame = _installedGames.Get(productId);
            if (installedGame == null || !installedGame.Installed)
            {
                return ERepairProductResult.NotInstalled;
            }

            // first flag the game to ensure that we will retain the "needs update"
            // state if we stop midway through the repair
            installedGame.InstallVersion = null;
            installedGame.InstallVersionName = null;
            _installedGames.Save(installedGame);

            // clear out our hash cache, just in case
            FuelLog.Info("Clearing hash cache", new { prodId = productId.Id });
            try
            {
                _uninstall.DeleteHashcache(productId);
            }
            catch (Exception ex)
            {
                // log it but keep going, hopefully we can rewrite the cache still
                FuelLog.Error(ex, "Failed to clear hash cache", new { prodId = productId.Id });
                _metrics.AddCounter(MetricStrings.SOURCE_RepairGame, "HashCacheClearFailed");
            }

            // delete all the game files
            if (deleteAllFiles)
            {
                FuelLog.Info("Removing all install files for repair", new { prodId = productId.Id, deleteAllFiles });
                try
                {
                    _deferredDeletionService.FlagForDeferredDelete(installedGame.InstallDirectory, deleteNow: true);
                }
                catch (Exception ex)
                {
                    FuelLog.Error(ex, "Couldn't delete all files");
                    _metrics.AddCounter(MetricStrings.SOURCE_RepairGame, "DeleteAllFilesFailed");
                }
            }

            bool hadPrereq = false;
            try
            {
                // TODO : this is hacky, but if we know there's a prereq to be installed, we need to force
                //          our install/update fuel.json watcher to redownload and reparse the fuel.json so
                //          that it may trigger the UAC.
                //          (eventually we should do away with all this nonsense and download the fuel.json
                //          as part of the service call)
                if ( _launchConfig.GetLaunchConfig(installedGame.InstallDirectory)?.PostInstall?.Count > 0)
                {
                    hadPrereq = true;
                    var fuelJsonPath = _launchConfig.GetLaunchConfigPath(installedGame.InstallDirectory);
                    FuelLog.Info("Removing fuel config to trigger UAC", new { fuelJsonPath });
                    File.Delete(fuelJsonPath);
                }
            }
            catch ( Exception ex)
            {
                FuelLog.Error(ex, "Couldn't reinstall prereqs");
            }

            FuelLog.Info("Updating product files/install", new { prodId = productId.Id });
            var installOrUpdateResult = InstallOrUpdateProductNoReporter(
                productId,
                installedGame.InstallDirectory,
                isUpdate: true,
                installDesktopShortcut: installDesktopShortcut,
                alwaysRerunPrereqs: true,
                outException: out outException,
                cancellationToken: cancellationToken);

            var repairResult = installOrUpdateResult.ToRepairProductResult();
            FuelLog.Info("Repair product completed with result",
                new
                {
                    prodId = productId.Id,
                    installOrUpdateResult,
                    repairResult,
                    hadPrereq,
                    durationSec = (DateTime.UtcNow - startDt).TotalSeconds
                });

            return repairResult;
        }

        public string TryGetTitle(ProductId id) => _entitlementDb.Get(id)?.ProductTitle
                                            ?? _library.InstallableGames?.Find(g => g.ProductId.Id == id.Id)?.Title;

        private EInstallUpdateProductResult DownloadJobResultToInstallUpdateProductResult(
            EDownloadJobResult dlJobResult)
        {
            switch (dlJobResult)
            {
                case EDownloadJobResult.Success:
                    return EInstallUpdateProductResult.Succeeded;
                case EDownloadJobResult.NetworkFailure:
                    return EInstallUpdateProductResult.NetworkFailure;
                case EDownloadJobResult.DownloadManifestRefreshFailure:
                    return EInstallUpdateProductResult.VersionManifestDownloadFailed;
                case EDownloadJobResult.Cancel:
                    return EInstallUpdateProductResult.Cancelled;
                case EDownloadJobResult.DownloadTimeoutEncountered:
                    return EInstallUpdateProductResult.DownloadFailure;
                case EDownloadJobResult.ExpiredFileEncountered:
                    return EInstallUpdateProductResult.DownloadFailure;
                case EDownloadJobResult.AuthFailure:
                    return EInstallUpdateProductResult.AuthFailure;
                case EDownloadJobResult.GeneralFailure:
                    return EInstallUpdateProductResult.DownloadFailure;
                case EDownloadJobResult.NotEnoughDiskSpace:
                    return EInstallUpdateProductResult.NotEnoughDiskSpace;
                case EDownloadJobResult.LockedFiles:
                    return EInstallUpdateProductResult.LockedFiles;
                default:
                    FuelLog.Error($"Unhandled EDownloadResult: '{dlJobResult}'");
#if DEBUG
                    Debug.Fail($"Unhandled EDownloadResult: '{dlJobResult}'");
#endif
                    goto case EDownloadJobResult.GeneralFailure;
            }
        }

        private EInstallUpdateProductResult TryEnsureInstallDirectory(InstallHelperSession installHelperSession, GameInstallInfo installInfo)
        {
            try
            {
                if (!TryEnsureDirWithPerms(installInfo.InstallDirectory, installHelperSession))
                {
                    return EInstallUpdateProductResult.CreateDirFailure;
                }

                return EInstallUpdateProductResult.Succeeded;
            }
            catch (Exception ex)
            {
                // [https://twitchtv.atlassian.net/browse/FLCL-90] handle these failures with better UI notification
                FuelLog.Error(ex, $"Failure creating install directory, can't install", new { installInfo.InstallDirectory });
                return EInstallUpdateProductResult.CreateDirFailure;
            }
        }


        private bool TryEnsureDirWithPerms(string dir, InstallHelperSession installHelperSession)
        {
            if (Directory.Exists(dir))
            {
                return true;
            }

            FuelLog.Info("Directory doesn't exist, attempting to create", new {dir});
            try
            {
                _osDriver.CreateDirectoryWithAllUsersPermissions(dir, setPermissionsOnTopmostNewDirectory: true);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                FuelLog.Info("Directory create failed, attempting with elevated process", new { dir });
            }
            catch (Exception e)
            {
                FuelLog.Warn(e, "Directory create failed (unexpected exception), attempting with elevated process", new { dir });
            }

            try
            {
                if (!dir.StartsWith(_osDriver.PrimaryLibraryRoot, StringComparison.InvariantCultureIgnoreCase))
                {
                    FuelLog.Error("Can't create directory via service, not primary location", new {dir});
                    return false;
                }

                // message the external app
                var response = _installHelperController.SendAndWaitForNextAsync<IpcCreateGamesLibraryPrimaryPathComplete>(
                    installHelperSession,
                    new IpcCreateGamesLibraryPrimaryPath(this),
                    timeout: PrimaryPathCreateIpcTimeoutSec).GetAwaiter().GetResult();

                if (installHelperSession.CancelToken.IsCancellationRequested)
                {
                    return false;
                }

                var success = response.Succeeded
                              && response.Message?.Result == IpcCreateGamesLibraryPrimaryPathComplete.EResult.Succeeded;

                if (success)
                {
                    return true;
                }

                FuelLog.Error("Create dir exe failed",
                    new {result = response.Result, msgResult = response.Message?.Result, dir});

                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "CreateDirectoryElevatedFailure");
                return false;
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Directory create failed (with elevated process)", new { dir });
                return false;
            }
        }

        private async Task<EInstallUpdateProductResult> InstallOrUpdateProductWithSleepProtect(
            ProductId adgProdId,
            bool isUpdate,
            string installLibraryDirectory,
            bool installDesktopShortcut,
            bool alwaysRerunPrereqs,
            CancelTaskHandler cancelHandler, 
            string transactionId)
        {
            var metricSource = isUpdate ? MetricStrings.SOURCE_UpdateGame : MetricStrings.SOURCE_InstallGame;
            FuelLog.Info("Install/Update Start", new {adgProdId, isUpdate, installLibraryDirectory});
            Stopwatch sw = new Stopwatch();
            sw.Start();

            _metrics.AddCounter(metricSource, MetricStrings.METRIC_Start);

            GameProductInfo entitlement = null;

            using (new PreventSleepScope(_osServicesDriver))
            {
                try
                {
                    // double check our entitlement in Fuel cache
                    entitlement = await _launcherServices.TryGetCachedEntitlementAsync(
                        adgProdId,
                        updateCacheOnMiss: true, 
                        updateCacheOnHit: false);

                    if (entitlement == null)
                    {
                        FuelLog.Warn($"Product '{adgProdId}' is not entitled");

                        return EInstallUpdateProductResult.NotEntitled;
                    }

                    if (cancelHandler.IsCanceled)
                    {
                        return EInstallUpdateProductResult.Cancelled;
                    }

                    if (isUpdate && IsRunning(adgProdId))
                    {
                        return EInstallUpdateProductResult.AlreadyRunning;
                    }

                    var title = entitlement.ProductTitle;

                    _hub.Publish(new MessageInstallUpdateProductStarted(this, adgProdId, isUpdate, title));
                    _metrics.SendSpadeEvent(new FuelLauncherAdgProductInfo(adgProdId.Id, entitlement.ProductAsin, entitlement.ProductTitle));

                    var installResult = await InstallOrUpdateGameCore(
                        adgProdId: adgProdId,
                        transactionId: transactionId,
                        installLibraryDirectory: installLibraryDirectory,
                        isUpdate: isUpdate,
                        installDesktopShortcut: installDesktopShortcut,
                        alwaysRerunPrereqs: alwaysRerunPrereqs,
                        cancelToken: cancelHandler.TokenSource.Token);

                    if (cancelHandler.IsCanceled)
                    {
                        return EInstallUpdateProductResult.Cancelled;
                    }

                    return installResult;
                }
                catch (Exception ex)
                {
                    FuelLog.Error(ex, "Unexpected exception installing/updating game", new { adgProdId, isUpdate });
                    return EInstallUpdateProductResult.GenericInstallOrUpdateProductWithSleepProtectException;
                }
                finally
                {
                    sw.Stop();

                    FuelLog.Info("Install/Update End", new { adgProdId, isUpdate, installLibraryDirectory, duration=sw.Elapsed.TotalSeconds });

                    if (entitlement != null)
                    {
                        _metrics.AddDiscreteValue(
                            isUpdate ? MetricStrings.SOURCE_UpdateGame : MetricStrings.SOURCE_InstallGame,
                            PmetUtilities.makeValidPmetString($"{MetricStrings.METRIC_TotalTimeInMs}:{entitlement.ProductTitle}"), 
                            (int)sw.ElapsedMilliseconds);
                    }
                }
            }
        }

        private bool IsRunning(ProductId productId)
        {
            try
            {
                return _installedGames.IsRunning(productId);
            }
            catch (InvalidLaunchConfigException ex)
            {
                FuelLog.Error(ex,
                    "Could not parse launch config when checking if game is running, proceeding with install/update.");
                _metrics.AddCounter(MetricStrings.SOURCE_LaunchCheckForUpdates, MetricStrings.METRIC_InvalidLaunchConfig);
                return false;
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Ran into unexpected exception when checking if game is running, procceeding with install/update anyways.");
                _metrics.AddCounter(MetricStrings.SOURCE_LaunchCheckForUpdates, MetricStrings.METRIC_UnknownException);
                return false;
            }
        }

        private void PreformPredownloadTasks(
            ProductId adgProdId,
            string installLibraryDirectory,
            bool isUpdate,
            ScopedDeferredHelperLauncher helperLauncher,
            CancellationToken cancelToken,
            out GameInstallInfo installInfo,
            ref EInstallUpdateProductResult result)
        {
            var metricSource = isUpdate ? MetricStrings.SOURCE_UpdateGame : MetricStrings.SOURCE_InstallGame;

            installInfo = null;
            FuelLog.Info($"Fuel install request initiating", new { adgProdId, isUpdate });

            // prep initial requirements (elevated)

            // NOTE:
            // new installations always need UAC helper immediately 
            // updates should wait until fuel.json is downloaded, to check for new prereqs before launching UAC
            if (!isUpdate)
            {
                // message the external app, set up initial requirements (fix common db permissions and such)
                var response = _installHelperController.SendAndWaitForNextAsync<IpcEnsureInitialRequirementsComplete>(
                    helperLauncher.InstallHelperSession,
                    new IpcEnsureInitialRequirements(this),
                    timeout: InitialReqIpcTimeoutSec).GetAwaiter().GetResult();

                if (cancelToken.IsCancellationRequested)
                {
                    result = EInstallUpdateProductResult.Cancelled;
                    return;
                }

                if (!response.Succeeded
                    || response.Message?.Result != IpcEnsureInitialRequirementsComplete.EResult.Succeeded)
                {
                    // log an error, but keep going (failure to set permissions here is not considered fatal)
                    FuelLog.Error("EnsureInitialRequirements failed", new { adgProdId, response.Message?.Result });
                }
            }


            if (isUpdate)
            {
                installInfo = _library.GetGameInstallInfo(adgProdId);
                if (installInfo == null)
                {
                    FuelLog.Error("No InstallInfo found in DB", new { adgProdId });
                    result = EInstallUpdateProductResult.InstallInfoNotFoundInDb;
                }
            }
            else
            {
                installInfo = new GameInstallInfo()
                {
                    InstallDirectory = Path.Combine(installLibraryDirectory, adgProdId.Id)
                };
            }

            var uninstallCreateSuccess = _uninstall.TryEnsureUninstallerExeInCommonLocation();
            if (!uninstallCreateSuccess)
            {
                _metrics.AddCounter(
                    metricSource,
                    MetricStrings.METRIC_UninstallCreateFailure);
            }

            if (!isUpdate)
            {
                // ensure install directory
                EInstallUpdateProductResult dirCreateResult = TryEnsureInstallDirectory(helperLauncher.InstallHelperSession, installInfo);
                if (dirCreateResult != EInstallUpdateProductResult.Succeeded)
                {
                    // if we got here, we had some sort of problem either creating the path or configuring it
                    bool wasPrimary = installInfo.InstallDirectory.IndexOf(_osDriver.PrimaryLibraryRoot,
                                                                            StringComparison.InvariantCultureIgnoreCase) >= 0;
                    _metrics.AddCounter(
                        metricSource,
                        wasPrimary ? "CreateLibraryDirFailedPrimary" : "CreateLibraryDirFailedSecondary");

                    result = EInstallUpdateProductResult.CreateDirFailure;
                }
            }
        }

        /// <summary>
        /// Downloads the manifest, and then the files needed to make the game up to date.
        /// If the download succeeds, asks the library to install the game.
        /// 
        /// Uses the DownloadPlugin, EntitlementPlugin, and LibraryPlugin
        /// </summary>
        private async Task<EInstallUpdateProductResult> InstallOrUpdateGameCore(
            ProductId adgProdId,
            string transactionId,
            string installLibraryDirectory,
            bool isUpdate,
            bool installDesktopShortcut,
            bool alwaysRerunPrereqs,
            CancellationToken cancelToken)
        {
            EInstallUpdateProductResult result = EInstallUpdateProductResult.Succeeded;

            FuelLog.Info("Starting game files download for game", new { adgProdId.Id });

            try
            {
                Action<GameInstallingMessage> onGameInstallingNotificationEvt = m =>
                {
                    _hub.Publish(
                        new MessageInstallUpdateProductState(
                            sender: this,
                            prodId: adgProdId,
                            isUpdate: isUpdate,
                            state: MessageInstallUpdateProductState.EState.Installing));
                };

                GameInstallInfo installInfo = null;

                // scoped helper launcher will launch elevated process (UAC prompt) in a "lazy" fashion (first required usage)
                using (var helperLauncher = new ScopedDeferredHelperLauncher(
                    _hub,
                    _installHelperController,
                    _metrics,
                    cancelToken))
                {
                    // this will trigger UAC for elevated process for the install workflow, but not for update
                    PreformPredownloadTasks(
                        adgProdId: adgProdId,
                        installLibraryDirectory: installLibraryDirectory,
                        isUpdate: isUpdate,
                        helperLauncher: helperLauncher,
                        cancelToken: cancelToken,
                        installInfo: out installInfo,
                        result: ref result);

                    using (new ScopedTinyMsgListener<GameInstallingMessage>(_hub, onGameInstallingNotificationEvt))
                    {
                        InstallUpdateProductResult downloadProductResult;

                        Func<Task<InstallUpdateProductResult>> downloadProdFunc = async ()
                            => await DownloadProduct(adgProdId, installInfo, isUpdate, transactionId, cancelToken);

                        // do we need to run the install portion, or just download?
                        bool installationRequired = false;
                        if (isUpdate)
                        {
                            // if we're updating, we need to wait for an update fuel.json to download before deciding if we need to throw UAC
                            // https://jira.twitch.com/browse/FLCL-1800 : handle scenario when fuel.json was downloaded as in update session, 
                            //  but interrupted before install (fuel.json has changed and matches new manifest sha but doesn't provoke UAC)
                            using (var updateListener = new ScopedUpdateElevationReqListener(
                                launchConfigProvider: _launchConfig,
                                gamePrereqInstaller: _prereqInstaller,
                                hub: _hub,
                                isUpdate: isUpdate,
                                alwaysRerunPrereqs: alwaysRerunPrereqs,
                                installDir: installInfo.InstallDirectory,
                                cancellationToken: cancelToken,
                                launcherProvider: helperLauncher))
                            {
                                downloadProductResult = await downloadProdFunc();
                                installationRequired = updateListener.InstallationRequired;
                            }
                        }
                        else
                        {
                            downloadProductResult = await downloadProdFunc();
                            installationRequired = true;
                        }

                        result = downloadProductResult.Result;

                        if (result != EInstallUpdateProductResult.Succeeded)
                        {
                            return result;
                        }

                        // updates should only require installation if prereqs have changed
                        if (installationRequired)
                        {
                            Stopwatch sw = new Stopwatch();
                            sw.Start();
                            FuelLog.Info("Starting installation", new {adgProdId});

                            _launcherServices.SendFuelLauncherActionEvent(adgProdId, installInfo.ProductAsin,
                                installInfo.InstallVersion,
                                isUpdate
                                    ? FuelLauncherActionType.GameUpdateStart
                                    : FuelLauncherActionType.GameInstallStart,
                                transactionId);

                            EInstallGameFlags installFlags = EInstallGameFlags.None;
                            if (!isUpdate)
                            {
                                installFlags |= EInstallGameFlags.CreateStartMenuShortcut
                                                | EInstallGameFlags.RegisterUninstaller;

                                if (installDesktopShortcut)
                                {
                                    installFlags |= EInstallGameFlags.CreateDesktopShortcut;
                                }
                            }

                            // install post-install requirements (directx, etc.)
                            result = _gameInstaller.InstallGame(
                                adgProdId,
                                installInfo,
                                helperLauncher,
                                productVersionId: downloadProductResult.DownloadedVersionId,
                                installFlags: installFlags,
                                cancelToken: cancelToken);

                            if (result == EInstallUpdateProductResult.Cancelled)
                            {
                                FuelLog.Warn("Game prereq installation cancelled", new {adgProdId, result});
                            }
                            else if (result != EInstallUpdateProductResult.Succeeded
                                     && result != EInstallUpdateProductResult.UacDenial)
                            {
                                FuelLog.Error(
                                    $"Game prereq installation failed (result will be translated as postinstall failure)",
                                    new {adgProdId, result});
                                result = EInstallUpdateProductResult.PostInstallFailure;
                            }

                            switch (result)
                            {
                                case EInstallUpdateProductResult.Succeeded:
                                    _launcherServices.SendFuelLauncherActionEvent(adgProdId, installInfo.ProductAsin,
                                        installInfo.InstallVersion,
                                        isUpdate
                                            ? FuelLauncherActionType.GameUpdateFinish
                                            : FuelLauncherActionType.GameInstallFinish, transactionId);
                                    break;
                                case EInstallUpdateProductResult.Cancelled:
                                    _launcherServices.SendFuelLauncherActionEvent(adgProdId, installInfo.ProductAsin,
                                        installInfo.InstallVersion,
                                        isUpdate
                                            ? FuelLauncherActionType.GameUpdateCancel
                                            : FuelLauncherActionType.GameInstallCancel, transactionId);
                                    break;
                                default:
                                    _launcherServices.SendFuelLauncherActionErrorEvent(adgProdId,
                                        installInfo.ProductAsin,
                                        installInfo.InstallVersion, result.ToString(),
                                        isUpdate
                                            ? FuelLauncherErrorType.GameUpdateFailure
                                            : FuelLauncherErrorType.GameInstallFailure, transactionId);
                                    break;
                            }

                            FuelLog.Info("Ended installation",
                                new {timeSec = sw.Elapsed.TotalSeconds, adgProdId, result});
                        }
                        else
                        {
                            FuelLog.Info("Updating game", new {adgProdId});
                            _gameInstaller.MarkAsInstalled(installInfo, downloadProductResult.DownloadedVersionId);
                        }
                    }
                }
            }
            catch (UacHelperAppLaunchException ex)
            {
                Log.Error(ex, "Couldn't launch UAC helper app", new {adgProdId, ex.Reason});
                result = EInstallUpdateProductResult.GenericUacHelperAppFailure;
                switch (ex.Reason)
                {
                    case UacHelperAppLaunchException.EReason.UacDenial:
                        result = EInstallUpdateProductResult.UacDenial;
                        break;
                }
            }
            catch (NotEntitledSdsException ex)
            {
                // our action failed from an unhandled exception. Report here and inform TwitchApp
                FuelLog.Error(ex, "NotEntitledSdsException failure", new {adgProdId, ex.EntitlementId});
                result = EInstallUpdateProductResult.NotEntitled;
            }
            catch (OperationCanceledException)
            {
                FuelLog.Info("Game installation/update cancelled", new {adgProdId});
                result = EInstallUpdateProductResult.Cancelled;
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Game installation/update failed", new {adgProdId});
                result = EInstallUpdateProductResult.GenericInstallOrUpdateGameCoreException;
            }

            _hub.Publish(
                new MessageInstallUpdateProductState(
                    sender: this,
                    prodId: adgProdId,
                    isUpdate: isUpdate,
                    state: MessageInstallUpdateProductState.EState.Complete));

            return result;
        }

        private async Task<InstallUpdateProductResult> DownloadProduct(
            ProductId productId,
            GameInstallInfo installInfo,
            bool isUpdate,
            string transactionId,
            CancellationToken cancelToken)
        {
            EInstallUpdateProductResult result = EInstallUpdateProductResult.Succeeded;

            Game game = _library.GetGame(productId);
            if (game == null)
            {
                FuelLog.Error("Game not entitled", new { productId });
                return new InstallUpdateProductResult
                {
                    Result = EInstallUpdateProductResult.NotEntitled
                };
            }

            _hub.Publish(
                new MessageInstallUpdateProductState(
                    sender: this,
                    prodId: productId,
                    isUpdate: isUpdate,
                    state: MessageInstallUpdateProductState.EState.Initializing,
                    title: game?.Title));

            var asin = _entitlementDb?.Get(productId)?.ProductAsin;
            DateTime dtStartDownload = DateTime.UtcNow;

            //Get the Manifest for the already installed game.
            Tv.Twitch.Fuel.Sds.Manifest previousManifest = null;
            var installedSdsVersionId = installInfo.InstallVersion;
            if (!string.IsNullOrEmpty(installedSdsVersionId))
            {
                try
                {
                    previousManifest = await _manifestStorage.Get(productId, installedSdsVersionId);
                }
                catch (Exception e)
                {
                    FuelLog.Warn(e, "Couldn't retrieve previous manifest", new { installedSdsVersionId, installInfo.Id });
                }
            }

            //TODO FLCL-1450 - Get the download manifest so we can send the SDS Version ID being downloaded.
            _launcherServices.SendFuelLauncherActionEvent(productId, asin, null, isUpdate ? FuelLauncherActionType.GamePatchDownloadStart : FuelLauncherActionType.GameFullDownloadStart, transactionId);

            Action<DownloadJobProgressMessage> onDownloadJobProgressMessage = m =>
            {
                // as a convenience, proxy to external listeners (more detailed messages are available via DownloadMessage in DownloadPlugin)
                _hub.Publish(
                    new MessageInstallUpdateProductProgress(
                        sender: this,
                        bytesDelta: m.BytesDelta,
                        bytesReceived: m.BytesReceived,
                        bytesTotal: m.BytesTotal,
                        timeRemainingSeconds: m.TimeRemainingSeconds,
                        timeRemainingNotEnoughData: m.TimeRemainingNotEnoughData,
                        productId: productId,
                        isUpdate: isUpdate,
                        title: game?.Title));
            };

            Action<DownloadFileMessage> onUpdateOperationNotificationEvt = m =>
            {
                // as a convenience, proxy to external listeners (more detailed messages are available via DownloadMessage in DownloadPlugin)
                _hub.Publish(
                    new MessageInstallUpdateProductDownloadFileNotification(
                        sender: this,
                        innerMessage: m,
                        productId: productId,
                        isUpdate: isUpdate,
                        title: game?.Title));
            };

            Action<MessageDownloadStarting> onDownloadStartingNotificationEvt = m =>
            {
                _hub.Publish(
                    new MessageInstallUpdateProductState(
                        sender: this,
                        prodId: productId,
                        isUpdate: isUpdate,
                        state: MessageInstallUpdateProductState.EState.Downloading,
                        title: game?.Title));
            };

            Action<MessageAllFilesDownloaded> onAllFilesDownloadedNotificationEvt = m =>
            {
                _hub.Publish(
                    new MessageInstallUpdateProductState(
                        sender: this,
                        prodId: productId,
                        isUpdate: isUpdate,
                        state: MessageInstallUpdateProductState.EState.Verifying,
                        title: game?.Title));
            };


            DownloadProductResult dlResult = null;

            using (new ScopedTinyMsgListener<DownloadJobProgressMessage>(_hub, onDownloadJobProgressMessage))
            using (new ScopedTinyMsgListener<MessageUpdateOperationProgress>(_hub, onUpdateOperationNotificationEvt))
            using (new ScopedTinyMsgListener<MessageDownloadStarting>(_hub, onDownloadStartingNotificationEvt))
            using (new ScopedTinyMsgListener<MessageAllFilesDownloaded>(_hub, onAllFilesDownloadedNotificationEvt))
            {
                // download game
                dlResult = await _downloadPlugin.DownloadProductWithRetryASync(
                    productId: productId,
                    entitlementId: game.EntitlementId,
                    folder: installInfo.InstallDirectory,
                    retryWaitSec: DownloadRetryWait,
                    isUpdate: isUpdate,
                    previousManifest: previousManifest,
                    cancellationToken: cancelToken);
            }

            result = DownloadJobResultToInstallUpdateProductResult(dlResult.Result);

            if (cancelToken.IsCancellationRequested)
            {
                FuelLog.Info("Cancellation detected", new { productId.Id, dlResult.Result, result });
                result = EInstallUpdateProductResult.Cancelled;
            }

            if (result == EInstallUpdateProductResult.NetworkFailure)
            {
                FuelLog.Error("Game download failed to due network failure", new { productId.Id, dlResult.Result, result });
                result = EInstallUpdateProductResult.NetworkFailure;
            }

            if (result != EInstallUpdateProductResult.Cancelled
                && dlResult?.ManifestWithVersion == null)
            {
                FuelLog.Error("Manifest is null", new { productId.Id, dlResult.Result, result });
                result = EInstallUpdateProductResult.VersionManifestDownloadFailed;
            }

            if (result == EInstallUpdateProductResult.Cancelled)
            {
                FuelLog.Warn("Game download cancelled", new { productId, result });
                _launcherServices.SendFuelLauncherActionEvent(
                    productId,
                    asin,
                    installedSdsVersionId,
                    isUpdate ? FuelLauncherActionType.GamePatchDownloadCancel
                        : FuelLauncherActionType.GameFullDownloadCancel,
                    transactionId);
            }
            else if (result != EInstallUpdateProductResult.Succeeded)
            {
                FuelLog.Error("Game download failed", new { productId.Id, dlResult.Result, result });
                _launcherServices.SendFuelLauncherActionErrorEvent(
                    productId,
                    asin,
                    installedSdsVersionId,
                    result.ToString(),
                    isUpdate ? FuelLauncherErrorType.GamePatchDownloadFailure
                        : FuelLauncherErrorType.GameFullDownloadFailure, 
                    transactionId);
            }

            FuelLog.Info("Game download ended", new { time = (DateTime.Now - dtStartDownload).ToString(), productId.Id, dlResult?.Result, result });

            if (result != EInstallUpdateProductResult.Succeeded)
            {
                FuelLog.Info("Terminating remainder of install", new { productId.Id, dlResult.Result, result });
                return new InstallUpdateProductResult
                {
                    Result = result
                };
            }

            // record serialized DB info
            installInfo.ProductTitle = game.Title;
            installInfo.ProductAsin = asin;
            installInfo.LastUpdatedAsDateTime = dtStartDownload;
            installInfo.Id = productId.Id;
            _installedGames.Save(installInfo);

            _launcherServices.SendFuelLauncherActionEvent(productId, asin, installInfo.InstallVersion, isUpdate ? FuelLauncherActionType.GamePatchDownloadFinish : FuelLauncherActionType.GameFullDownloadFinish, transactionId);

            return new InstallUpdateProductResult
            {
                Result = result,
                DownloadedVersionId = dlResult.ManifestWithVersion.SdsVersionId
            };
        }
    }
}
