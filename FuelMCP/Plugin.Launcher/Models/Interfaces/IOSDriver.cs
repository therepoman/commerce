﻿
namespace Amazon.MCP.Plugin.Launcher.Models.Interfaces
{
    public interface IOSDriver
    {
        string AppLocation { get; }

        string UACAppLocation { get; }
        string DesktopPath { get; }
        string CommonStartMenuPath { get; }
        string UserStartMenuPath { get; }
    }
}
