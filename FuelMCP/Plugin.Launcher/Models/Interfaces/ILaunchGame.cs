﻿using Amazon.MCP.Common.Contracts;
using System.Threading.Tasks;

namespace Amazon.MCP.Plugin.Launcher.Model.Interfaces
{
    public enum ELaunchResult
    {
        Success = 0,

        NotEntitled,
        NeedsUpdate,
        IsBusyUpdating,
        VersionManifestDownloadFailed,
        ConfigFileMissing,
        InstallUpdateFailed,
        LaunchFailed
    }

    public interface ILaunchGame
    {
        bool IsRunning(string installFolder);
        Task<string> GetCurrentVersion(Game game);
        Task<ELaunchResult> InstallOrUpdate(Game game, string installFolder);
        Task<ELaunchResult> Launch(Game game, string installFolder);
    }
}
