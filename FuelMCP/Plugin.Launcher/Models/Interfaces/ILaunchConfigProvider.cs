﻿using Amazon.MCP.Common.Contracts;
using Amazon.MCP.Plugin.Launcher.Data;

namespace Amazon.MCP.Plugin.Launcher.Models.Interfaces
{
    public interface ILaunchConfigProvider
    {
        LaunchConfig GetLaunchConfig(LaunchConfigContext context, string installFolder, string filename = "fuel.json");        
    }

    public class LaunchConfigContext
    {
        // https://twitchtv.atlassian.net/browse/FLCL-130 Remove these when we axe the fuel launcher
        public string EntitlementId;
        public string OauthToken;
    }
}
