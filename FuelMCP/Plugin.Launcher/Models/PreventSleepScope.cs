using System;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public class PreventSleepScope : IDisposable
    {
        private readonly IOSServicesDriver _osServicesDriver;

        public PreventSleepScope(IOSServicesDriver osServicesDriver)
        {
            _osServicesDriver = osServicesDriver;
            _osServicesDriver.PreventSleep();
        }

        public void Dispose()
        {
            _osServicesDriver.AllowSleep();
        }
    }
}