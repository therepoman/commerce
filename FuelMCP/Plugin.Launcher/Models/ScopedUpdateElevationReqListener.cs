using System;
using System.IO;
using System.Linq;
using System.Threading;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Extensions;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Messages;
using Amazon.Fuel.Plugin.Download.Models;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    // NOTE: The reason we need this is to display the UAC elevation prompt as soon as possible (fuel.json
    //  has the information we need to determine this, and should be one of the first files to
    //  download once initialized). Note that this is only used for update, as the install always
    //  requires elevation.

    /// <summary>
    /// Watch tiny messenger for fuel.json file download. Once done, check to see if we have any 
    ///  new prereqs, and launch the UAC helper if we need it.
    /// </summary>
    public class ScopedUpdateElevationReqListener : Disposable
    {
        private readonly IMessengerHub _hub;
        private readonly IHelperLauncherProvider _launcherProvider;
        private readonly CancellationToken _cancellationToken;
        private readonly ILaunchConfigProvider _launchConfigProvider;
        private readonly IGamePrereqInstaller _gamePrereqInstaller;
        private readonly bool _isUpdate;
        private readonly bool _alwaysRerunPrereqs;
        private readonly string _installDir;

        private ScopedTinyMsgListener<MessageDownloadFileFinished> _scopeListener = null;

        private bool _configFileFound = false;

        public bool InstallationRequired { get; private set; } = false;

        public ScopedUpdateElevationReqListener(
            ILaunchConfigProvider launchConfigProvider,
            IHelperLauncherProvider launcherProvider,
            IGamePrereqInstaller gamePrereqInstaller,
            IMessengerHub hub,
            bool isUpdate,
            bool alwaysRerunPrereqs,
            string installDir,
            CancellationToken cancellationToken)
        {
            _launchConfigProvider = launchConfigProvider;
            _gamePrereqInstaller = gamePrereqInstaller;
            _launcherProvider = launcherProvider;
            _hub = hub;
            _isUpdate = isUpdate;
            _cancellationToken = cancellationToken;
            _alwaysRerunPrereqs = alwaysRerunPrereqs;

            _scopeListener = new ScopedTinyMsgListener<MessageDownloadFileFinished>(_hub, HandleDownloadFileFinished);
            _installDir = installDir;
        }

        private void HandleDownloadFileFinished(MessageDownloadFileFinished msg)
        {
            if (string.IsNullOrEmpty(_installDir))
            {
                FuelLog.Error("Unexpected: installDir is null or empty (ScopedHelperLauncher)", new { _installDir });
                throw new UacHelperAppLaunchException(UacHelperAppLaunchException.EReason.GeneralFailure);
            }

            if (_cancellationToken.IsCancellationRequested
                || _configFileFound
                || _installDir != msg.Folder)
            {
                return;
            }

            // sanity: make sure file has our _installDir
            if (!msg.File.StartsWith(msg.Folder))
            {
                FuelLog.Error("Unexpected: File name doesn't include folder (ScopedHelperLauncher)", new { msg.Folder, msg.File });
                return;
            }

            // fuel.json wants a path relative to _installDir
            string relFile = msg.File.Substring(msg.Folder.Length + 1);

            if (!string.Equals(relFile, Constants.Paths.FuelGameConfigFileName, StringComparison.InvariantCultureIgnoreCase))
            {
                // we only care about fuel.json
                return;
            }

            FuelLog.Info("ScopedHelperLauncher found fuel config for update check", new { _installDir, msg.Folder, msg.File, _isUpdate });
            _configFileFound = true;

            var launchConfig = _launchConfigProvider.GetLaunchConfig(_installDir);
            if (launchConfig == null)
            {
                FuelLog.Error("Couldn't load launch config (ScopedHelperLauncher)", new { _installDir });
                throw new UacHelperAppLaunchException(UacHelperAppLaunchException.EReason.LaunchConfigError);
            }

            if (_alwaysRerunPrereqs)
            {
                // clear the prereqs cache, so that we reinstall them 
                foreach (var prereq in launchConfig.PostInstall)
                {
                    FuelLog.Info("Removing prereq, user must elevate", new { cmd=prereq.Command, args=string.Join(",", prereq.Args)});
                    _gamePrereqInstaller.ClearPrereqHasRun(prereq, _installDir);
                }
            }


            IDownloadJob job = msg.Job;
            var manifestFilesByPath = job.LastResult.ManifestWithVersion.ManifestData.Packages[0].Files
                .ToDictionary(f => Path.GetFullPath(Path.Combine(_installDir, f.Path)), StringComparer.OrdinalIgnoreCase);

            // Filter already installed prereqs
            Func<Executable, string> getShaFromExe = exe =>
            {
                var filePath = Path.GetFullPath(Path.Combine(_installDir, exe.Command));
                return manifestFilesByPath[filePath].Hash.HexString();
            };


            var prereqs = launchConfig.PostInstall.Select(exe => new PrereqExecutableWithSha() { Exe = exe, ExpectedSha = getShaFromExe(exe) })
                .ToList();

            var filteredPrereqs = launchConfig.PostInstall.Select(e =>
                $"{e.Command} {string.Join(" ", e.Args)} AlwaysRun: {e.AlwaysRun}");

            FuelLog.Info("Prereqs extracted from config", new { filteredPrereqsSummary = string.Join(",", filteredPrereqs) });

            // filter out already run prereqs
            if (prereqs.Count > 0)
                prereqs = _gamePrereqInstaller.FilterPrereqs(prereqs, _installDir);

            if (prereqs.Count == 0)
            {
                FuelLog.Info("No prereqs in config file, skipping helper");
                return;
            }

            var newFilteredPrereqs = prereqs.Select(e => $"{e.Exe.Command} {string.Join(" ", e.Exe.Args)} AlwaysRun: {e.Exe.AlwaysRun}");

            FuelLog.Info("Prereqs remain after already run filtering, running launching helper app", new { newFilteredPrereqsSummary = string.Join(",", newFilteredPrereqs) });

            InstallationRequired = true;
            _launcherProvider.LaunchHelperApp();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            _scopeListener?.Dispose();
            _scopeListener = null;
        }
    }
}