﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using System;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public abstract class GameResultReporter<T> : IDisposable
    {
        protected IMetricsPlugin _metrics;
        protected IMessengerHub _hub;
        protected ITitleProvider _titleProvider;

        public T Result { get; set; } = default(T);

        public ProductId ProdId { get; }
        public Exception ResultException { get; set; }

        protected abstract string ServiceName { get; }

        protected abstract string MetricSource { get; }
        protected abstract string MetricQosSource { get; }

        protected abstract string MetricExpectedFailure { get; }
        protected abstract string MetricUnexpectedFailure { get; }

        protected abstract bool Succeeded { get; }
        protected abstract bool Cancelled { get; }
        protected abstract bool UnexpectedFailure { get; }

        protected abstract void SendSuccessMessage();
        protected abstract void SendFailureMessage();

        public GameResultReporter(
            IMetricsPlugin metrics,
            IMessengerHub hub,
            ITitleProvider titleProvider,
            ProductId id)
        {
            _metrics = metrics;
            _hub = hub;
            _titleProvider = titleProvider;
            ProdId = id;
        }

        public virtual void Dispose()
        {
            Action<string, object> logFunc;

            if (Succeeded)
            {
                logFunc = FuelLog.Info;
                _metrics.AddCounter(MetricSource, MetricStrings.METRIC_Success, MetricStrings.PIVOT_ProductId, ProdId.Id);
                SendSuccessMessage();
            }
            else
            {
                if (Cancelled)
                {
                    logFunc = FuelLog.Warn;
                    _metrics.AddCounter(MetricSource, MetricStrings.METRIC_Cancel, MetricStrings.PIVOT_ProductId, ProdId.Id);
                }
                else
                {
                    logFunc = FuelLog.Error;

                    _metrics.AddCounter(MetricSource, Result.ToString());
                    string failureType, failureTypeSource;

                    if (UnexpectedFailure)
                    {
                        failureTypeSource = MetricUnexpectedFailure;
                        failureType = MetricStrings.METRIC_UnexpectedFailure;
                    }
                    else
                    {
                        failureTypeSource = MetricExpectedFailure;
                        failureType = MetricStrings.METRIC_ExpectedFailure;
                    }

                    // Used to get a percentage of Unexpected failure rate and expected failure rate.
                    _metrics.AddDiscreteValue(MetricSource, failureType, 1, MetricStrings.PIVOT_ProductId, ProdId.Id);
                    _metrics.AddDiscreteValue(MetricQosSource, failureType, 1);
                    
                    // Used to drill down in the most prevalent Expected and unexpected failure codes.
                    _metrics.AddDiscreteValue(
                        failureTypeSource,
                        Result.ToString() + (ResultException?.GetType().ToString() ?? string.Empty), 
                        1, 
                        MetricStrings.PIVOT_ProductId, 
                        ProdId.Id);

                }

                SendFailureMessage();
            }

            logFunc($"{ServiceName} completed with result", new { ProdId, Result });
        }
    }
}
