﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Plugin.Launcher.Messages;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public class UpdateGameResultReporter : GameResultReporter<EInstallUpdateProductResult>
    {
        public UpdateGameResultReporter(
            IMetricsPlugin metrics,
            IMessengerHub hub,
            ProductId id,
            ITitleProvider titleProvider)
            : base(metrics, hub, titleProvider, id) { }

        protected override string ServiceName => "UpdateGame";

        protected override string MetricSource => MetricStrings.SOURCE_UpdateGame;
        protected override string MetricQosSource => MetricStrings.SOURCE_UpdateGameQos;

        protected override string MetricExpectedFailure => MetricStrings.SOURCE_UpdateGameExpectedFailure;
        protected override string MetricUnexpectedFailure => MetricStrings.SOURCE_UpdateGameUnexpectedFailure;

        protected override bool Succeeded => Result == EInstallUpdateProductResult.Succeeded;
        protected override bool Cancelled => Result == EInstallUpdateProductResult.Cancelled;
        protected override bool UnexpectedFailure => InstallUpdateProductResultHelpers.IsUnexpectedFailure(Result);

        protected override void SendSuccessMessage()
        {
            var msg = new MessageInstallUpdateProductSucceeded(this, ProdId, isUpdate: false, title: _titleProvider.TryGetTitle(ProdId));
            _hub.Publish(msg);
        }

        protected override void SendFailureMessage()
        {
            var msg = new MessageInstallUpdateProductFailed(this, Result, ProdId, isUpdate: false, title: _titleProvider.TryGetTitle(ProdId));
            _hub.Publish(msg);
        }
    }
}
