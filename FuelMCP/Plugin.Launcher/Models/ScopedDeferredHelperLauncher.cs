﻿using System;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using System.Threading;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Resources;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public class UacHelperAppLaunchException : Exception
    {
        public enum EReason
        {
            UacDenial,
            LaunchConfigError,
            HelperLaunchFailure,
            HitException,
            GeneralFailure
        }

        public EReason Reason { get; }

        public UacHelperAppLaunchException(Exception ex) : base($"Exception launching UAC helper", ex)
        {
            Reason = EReason.HitException;
        }

        public UacHelperAppLaunchException(EReason reason) : base($"Exception launching UAC helper")
        {
            Reason = reason;
        }
    }

    /// <summary>
    /// This class is responsible for starting and stopping the elevated helper app (this raises a UAC prompt to 
    ///  user when launching). Starts the app lazily (will only start elevated helper app once requested).
    /// </summary>
    public class ScopedDeferredHelperLauncher : Disposable, IHelperLauncherProvider
    {
        private readonly IMessengerHub _hub;
        private readonly IInstallHelperIpcController _installHelperController;
        private readonly IMetricsPlugin _metrics;
        private readonly CancellationToken _cancellationToken;
        private readonly SemaphoreSlim _lock = new SemaphoreSlim(1);

        private bool _configFileFound = false;
        private string _installDir;

        private InstallHelperSession _installHelperSession = null;
        public InstallHelperSession InstallHelperSession
        {
            get
            {
                if (_installHelperSession == null)
                    LaunchHelperApp();

                return _installHelperSession;
            }
            private set
            {
                _installHelperSession = value;
            }
        }

        public ScopedDeferredHelperLauncher(
            IMessengerHub hub,
            IInstallHelperIpcController installHelperController,
            IMetricsPlugin metrics,
            CancellationToken cancellationToken)
        {
            _hub = hub;
            _installHelperController = installHelperController;
            _metrics = metrics;
            _cancellationToken = cancellationToken;
        }

        /// <summary>
        /// Attempts to launch the helper app, unless a cancellation is requested or the app is already running.
        /// </summary>
        public void LaunchHelperApp()
        {
            try
            {
                if (_installHelperSession != null)
                {
                    return;
                }

                try
                {
                    _lock.Wait(_cancellationToken);
                }
                catch (OperationCanceledException)
                {
                    return;
                }

                try
                {
                    if (_installHelperSession != null)
                    {
                        return;
                    }

                    // start the helper app
                    _installHelperSession = _installHelperController.Start(_hub, _cancellationToken);

                    if (_installHelperSession.StatusAfterLaunch == LaunchResult.EStatus.UacRequestRejected)
                    {
                        FuelLog.Error("Couldn't launch helper exe : user rejected UAC prompt");
                        _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "UacDenial");
                        throw new UacHelperAppLaunchException(UacHelperAppLaunchException.EReason.UacDenial);
                    }
                    else if (_installHelperSession.StatusAfterLaunch != LaunchResult.EStatus.Success)
                    {
                        FuelLog.Error("Couldn't launch helper exe", new {_installHelperSession.StatusAfterLaunch});
                        _metrics.AddCounter(MetricStrings.SOURCE_InstallGame,
                            $"HelperExeLaunchFailed_{_installHelperSession.StatusAfterLaunch}");
                        throw new UacHelperAppLaunchException(UacHelperAppLaunchException.EReason.HelperLaunchFailure);
                    }
                    else
                    {
                        _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, "UacSuccess");
                    }
                }
                finally
                {
                    _lock.Release();
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error("Couldn't connect IPC to the helper exe");
                _metrics.AddCounter(MetricStrings.SOURCE_InstallGame, $"HelperExeLaunchFailed");

                if (ex is UacHelperAppLaunchException)
                    throw;
                throw new UacHelperAppLaunchException(ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (_installHelperSession != null)
            {
                _installHelperController?.Stop(_installHelperSession, sendTerminationMessage: true);
                _installHelperSession = null;
            }
        }
    }
}
