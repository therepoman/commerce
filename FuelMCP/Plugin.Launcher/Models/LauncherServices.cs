﻿using System;
using System.Threading.Tasks;
using Serilog;
using System.Diagnostics;
using System.IO;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Extensions;
using Amazon.Fuel.Common.Implementations;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Logging.Events;
using Amazon.Fuel.Plugin.Launcher.Messages;
using Clients.Twitch.Spade.Client;
using Clients.Twitch.Spade.Model.Event;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    public class LauncherServicesException : Exception
    {
        public LauncherServicesException(string message) : base(message) { }
        public LauncherServicesException(string message, Exception inner) : base(message, inner) { }
    }

    public class LauncherServices : ILauncherServices
    {
        private readonly ITwitchLoginPlugin _twitchLoginPlugin = null;
        private readonly ILibraryPlugin _library = null;
        private readonly IOSDriver _osDriver;
        private readonly IMessengerHub _msg = null;
        private readonly IMetricsPlugin _metrics = null;
        private readonly IEntitlementsDatabase _entitlementDb = null;
        private readonly IEntitlementPlugin _entitlementPlugin = null;
        private readonly IProcessLauncher _processLauncher = null;
        private readonly ILaunchConfigProvider _launchConfig = null;
        private readonly IShortcutManager _shortcutManager;
        private readonly IOSServicesDriver _osServicesDriver;
        private readonly IClock _clock;
        private readonly ITwitchUserStoragePlugin _twitchUserStoragePlugin;
        private readonly IInstalledGamesPlugin _install;

        public LauncherServices(
            IOSDriver osDriver,
            IMessengerHub hub,
            IMetricsPlugin metrics,
            IEntitlementsDatabase entitlementDb,
            IEntitlementPlugin entitlementPlugin,
            ILibraryPlugin library,
            IProcessLauncher processLauncher,
            ITwitchLoginPlugin twitchLoginPlugin,
            ILaunchConfigProvider launchConfig,
            IShortcutManager shortcutManager,
            IClock clock,
            ITwitchUserStoragePlugin twitchUserStoragePlugin,
            IOSServicesDriver osServicesDriver,
            IInstalledGamesPlugin install)
        {
            _osDriver = osDriver;
            _msg = hub;
            _metrics = metrics;
            _library = library;
            _entitlementDb = entitlementDb;
            _entitlementPlugin = entitlementPlugin;
            _processLauncher = processLauncher;
            _twitchLoginPlugin = twitchLoginPlugin;
            _launchConfig = launchConfig;
            _shortcutManager = shortcutManager;
            _clock = clock;
            _twitchUserStoragePlugin = twitchUserStoragePlugin;
            _osServicesDriver = osServicesDriver;
            _install = install;
        }

        public Game ValidateEntitlement(ProductId productId)
        {
            _library.UpdateEntitledGamesList().Wait();
            return _library.GetGame(productId);
        }

        public async Task<bool> CheckCachedEntitledAsync(ProductId pid, bool updateCacheOnMiss = true, bool updateCacheOnHit = false)
        {
            return await TryGetCachedEntitlementAsync(
                pid: pid,
                updateCacheOnMiss: updateCacheOnMiss,
                updateCacheOnHit: updateCacheOnHit) != null;
        }

        // Default settings are to favor cache on hit and refresh on miss. Much more likely we're missing an entitlement than have a revoked entitlement.
        public async Task<GameProductInfo> TryGetCachedEntitlementAsync(ProductId pid, bool updateCacheOnMiss = true, bool updateCacheOnHit = false)
        {
            GameProductInfo ret = null;
            bool checkWithServer = false;

            ret = _entitlementDb.Get(pid);
            if (ret != null)
            {
                checkWithServer = updateCacheOnHit;
            }
            else
            {
                checkWithServer = updateCacheOnMiss;
            }

            if (checkWithServer)
            {
                // double check
                await _entitlementDb.SyncGoods();
                ret = _entitlementDb.Get(pid);
            }

            return ret;
        }

        /// <summary>
        /// Checks to see if the game is running from the given InstallDirectory.
        /// </summary>
        /// <param name="installFolder"></param>
        /// <returns></returns>
        public bool IsRunning(ProductId productId)
        {
            var installInfo = _library.GetGameInstallInfo(productId);
            if (installInfo == null)
            {
                Log.Error($"[LaunchGame.IsRunning] Unable to get Install Inforamation for {productId}");
                return false;
            }
            var installFolder = installInfo.InstallDirectory;

            var cfg = _launchConfig.GetLaunchConfig(installFolder);
            if (cfg == null)
            {
                Log.Error(
                    $"[LaunchGame.IsRunning] Unable to get Install Inforamation for {productId} from {installFolder}");
                return false;
            }

            if (cfg.Main == null)
            {
                Log.Error($"[LaunchGame.IsRunning] {productId} in {installFolder} has no Main!");
                return false;
            }

            var fileToRun = System.IO.Path.Combine(installFolder, cfg.Main.Command);
            var result = _processLauncher.IsRunning(fileToRun);
            if (result)
            {
                if (cfg.Flags.Contains(LaunchConfigFlags.KillIfSuspended))
                {
                    bool killed = _processLauncher.TryKillSuspendedProcesses(fileToRun);
                    Log.Information($"Killed any suspended threads '{fileToRun}', result '{killed}'");
                    return _processLauncher.IsRunning(fileToRun);
                }

                return true;
            }
            return false;
        }

        private ELaunchProductResult completeLaunchProduct(ProductId productId, ELaunchProductResult result, string transactionId)
        {
            var asin = _entitlementDb?.Get(productId)?.ProductAsin;
            var sdsVersionId = _library?.GetGameInstallInfo(productId)?.InstallVersion;
            switch (result)
            {
                case ELaunchProductResult.Succeeded:
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_Success, MetricStrings.PIVOT_ProductId, productId.Id);
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGameQos, MetricStrings.METRIC_Success);
                    SendFuelLauncherActionEvent(productId, asin, sdsVersionId, FuelLauncherActionType.GameLaunchFinish, transactionId);
                    break;
                case ELaunchProductResult.NeedsUpdate:
                    // add a metric for needing update but consider as task successful
                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_Failure, 0);
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGame, result.ToString());
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGameExpectedFailure, result.ToString());
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGameQos, MetricStrings.METRIC_ExpectedFailure);
                    break;
                case ELaunchProductResult.Cancelled:
                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_Cancel, 1);
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGameQos, MetricStrings.METRIC_Cancel);
                    SendFuelLauncherActionEvent(productId, asin, sdsVersionId, FuelLauncherActionType.GameLaunchCancel, transactionId);
                    break;
                default:
                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_Failure, 1, MetricStrings.PIVOT_ProductId, productId.Id);
                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGame, result.ToString());

                    var failureTypeMetricString = LaunchProductResultExt.IsUnexpectedFailure(result)
                        ? MetricStrings.METRIC_UnexpectedFailure
                        : MetricStrings.METRIC_ExpectedFailure;
                    var sourceFailureBucketString = LaunchProductResultExt.IsUnexpectedFailure(result)
                        ? MetricStrings.SOURCE_LaunchGameUnexpectedFailure
                        : MetricStrings.SOURCE_LaunchGameExpectedFailure;

                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchGameQos, failureTypeMetricString);
                    _metrics.AddCounter(sourceFailureBucketString, result.ToString());

                    Log.Error($"[LCH] LaunchProduct failed for {productId}: {result}");
                    SendFuelLauncherActionErrorEvent(productId, asin, sdsVersionId, result.ToString(), FuelLauncherErrorType.GameLaunchFailure, transactionId);
                    break;
            }

            var resultStr = $"Launch for {productId} result '{result}'";
            if (result == ELaunchProductResult.Succeeded)
            {
                Log.Information(resultStr);
            }
            else if (result == ELaunchProductResult.Cancelled)
            {
                Log.Warning(resultStr);
            }
            else
            {
                Log.Error($"Launch for {productId} result '{result}'");
                _msg.Publish(new MessageLaunchProductFailed(this, result, productId));
            }

            return result;
        }

        public async Task<ELaunchProductResult> LaunchProduct(
            ProductId productId,
            string[] productArgs,
            bool productArgsIgnoreAppCfg,
            bool checkForUpdates,
            string developerDir = null)
        {
            var transactionId = Guid.NewGuid().ToString();
            _metrics.AddCounter(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_Start);
            var asin = _entitlementDb?.Get(productId)?.ProductAsin;
            var sdsVersionId = _library?.GetGameInstallInfo(productId)?.InstallVersion;
            SendFuelLauncherActionEvent(productId, asin, sdsVersionId, FuelLauncherActionType.GameLaunchStart, transactionId);
            var productArgsJoined = productArgs != null ? string.Join(",", productArgs) : "";
            Log.Information($"[LCH] LaunchProduct({productId}, {productArgsJoined}, {productArgsIgnoreAppCfg}, {checkForUpdates}, {developerDir})");

            using (new PreventSleepScope(_osServicesDriver))
            {
                using (CancelTaskHandler cancelHandler = new CancelTaskHandler(_msg, productId))
                {
                    var entitlement = _library.GetGame(productId);
                    if (entitlement == null)
                    {
                        // attempt to obtain remote entitlement, if not cached
                        Log.Warning("Entitlement not found, attempting to retrieve from server");
                        await _entitlementDb.SyncGoods();
                        entitlement = _library.GetGame(productId);

                        if (entitlement == null)
                        {
                            return completeLaunchProduct(productId, ELaunchProductResult.NotEntitled, transactionId);
                        }
                    }

                    if (!entitlement.IsDeveloper)
                    {
                        developerDir = null;
                    }

                    if (cancelHandler.IsCanceled)
                    {
                        return completeLaunchProduct(productId, ELaunchProductResult.Cancelled, transactionId);
                    }

                    var installInfo = _library.GetGameInstallInfo(productId);

                    if (developerDir != null)
                    {
                        // Don't do anything here, we're running locally
                        Debug.Assert(entitlement.IsDeveloper);
                    }
                    else
                    {
                        if (installInfo == null || !installInfo.Installed)
                        {
                            return completeLaunchProduct(productId, ELaunchProductResult.NotInstalled, transactionId);
                        }

                        if (checkForUpdates)
                        {
                            var isUpToDateResult = await _install.CheckUpToDateByInstallInfo(
                                installInfo: installInfo,
                                retrieveCondition: ERetrieveVersionCondition.UpdateIfExpired);

                            switch (isUpToDateResult)
                            {
                                // these shouldn't ever be hit --
                                case ECheckUpToDateResult.NoProductInDb:
                                case ECheckUpToDateResult.NotInstalled:
                                case ECheckUpToDateResult.EntitlementNotFound:
                                default:
                                    Log.Error($"Unexpected up to date result '{isUpToDateResult}'");
                                    goto case ECheckUpToDateResult.RemoteVersionInvalid;
                                case ECheckUpToDateResult.RemoteVersionInvalid:
                                    // Allow for the user to launch their game even if there are server-side issues.
                                    Log.Error("Ran into unexpected issue when trying to check for update", new { isUpToDateResult});
                                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchCheckForUpdates, ELaunchProductResult.VersionManifestDownloadFailed.ToString());
                                    break;                
                                case ECheckUpToDateResult.NeedsUpdate:
                                    return completeLaunchProduct(productId, ELaunchProductResult.NeedsUpdate, transactionId);

                                case ECheckUpToDateResult.NetworkFailure:
                                    // Allow for the user to launch their game even if there are network issues.
                                    Log.Error("Ran into unexpected network failure when trying to check for update", new {isUpToDateResult});
                                    _metrics.AddCounter(MetricStrings.SOURCE_LaunchCheckForUpdates, ELaunchProductResult.NetworkFailure.ToString());
                                    break;

                                case ECheckUpToDateResult.UpToDate:
                                    // keep going
                                    break;
                            }
                        }
                    }

                    //If the install has gone okay so far we can run the game:
                    if (cancelHandler.IsCanceled)
                    {
                        return completeLaunchProduct(productId, ELaunchProductResult.Cancelled, transactionId);
                    }

                    var installDir = developerDir ?? installInfo.InstallDirectory;

                    var cfg = _launchConfig.GetLaunchConfig(installDir);

                    if (cfg == null)
                    {
                        return completeLaunchProduct(productId, ELaunchProductResult.ConfigFileMissing, transactionId);
                    }

                    if (cfg.Main == null)
                    {
                        return completeLaunchProduct(productId, ELaunchProductResult.InvalidConfigFile, transactionId);
                    }

                    string authToken = null;
                    if (string.IsNullOrEmpty(cfg.Main.ClientId))
                    {
                        Log.Warning($"[LCH] No client id for {productId}");
                    }
                    else
                    {
                        try
                        {
                            authToken = _twitchLoginPlugin.GetGameToken(cfg.Main.ClientId, cfg.Main.AuthScopes);
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "Exception thrown when attempting to exchange for the game token");
                            return completeLaunchProduct(productId, ELaunchProductResult.ProcessLaunchFailedTokenExchangeFailure, transactionId);
                        }
                    }

                    using (var launchResult = _processLauncher.LaunchGame(
                            _osDriver.SdkLocation,
                            installDir,
                            cfg,
                            productArgs,
                            productArgsIgnoreAppCfg))
                    {
                        if (launchResult == null || launchResult.Status != LaunchResult.EStatus.Success)
                        {
                            if (launchResult?.Status == LaunchResult.EStatus.AlreadyRunning)
                            {
                                return completeLaunchProduct(productId, ELaunchProductResult.ProcessLaunchFailedAlreadyRunning, transactionId);
                            }

                            if (launchResult?.Status == LaunchResult.EStatus.WrongArchitecture)
                            {
                                return completeLaunchProduct(productId, ELaunchProductResult.ProcessLaunchFailedWrongArchitecture, transactionId);
                            }
                            
                            if (launchResult?.Status == LaunchResult.EStatus.FileNotFound)
                            {
                                var pathToFile = Path.Combine(installDir, cfg.Main.Command);
                                Log.Write(ProcessLaunchFailedReportEvent.Create(
                                    Constants.CrashScope.App,
                                    Constants.CrashType.Error,
                                    new FileNotFoundException(pathToFile),
                                    installDir,
                                    pathToFile,
                                    _osDriver,
                                    productId.Id,
                                    sdsVersionId));
                                return completeLaunchProduct(productId, ELaunchProductResult.FileNotFound, transactionId);
                            }

                            if (launchResult?.Status == LaunchResult.EStatus.ReturnCodeError)
                            {
                                // we consider this a success, but let's emit a metric to track non-zero exit code counts
                                _metrics.AddDiscreteValue(MetricStrings.SOURCE_LaunchGame, MetricStrings.METRIC_LaunchExitCodeNonZero, 1, MetricStrings.PIVOT_ProductId, productId.Id);
                            }
                            else
                            {
                                return completeLaunchProduct(productId, ELaunchProductResult.ProcessLaunchFailed, transactionId);
                            }
                        }

                        int processid = 0;
                        if (launchResult.Process != null)
                        {
                            processid = launchResult.Process.Id;
                        }

                        if (!string.IsNullOrEmpty(cfg.Main.ClientId) && !string.IsNullOrEmpty(authToken))
                        {
                            _twitchUserStoragePlugin.SetTwitchCredentialSet(
                                cfg.Main.ClientId,
                                new TwitchCredentialSet
                                {
                                    AccessToken = authToken,
                                    Pid = processid,
                                    Created = _clock.Now(),
                                    Entitlement = entitlement.EntitlementId,
                                    Scopes = cfg.Main.AuthScopes,
                                });
                        }

                        if (launchResult.Process != null)
                        {
                            // watch the process for main window create, with a sane timeout
                            await _processLauncher.TryWaitOnProcessMainWndAndFocusAsync(launchResult.Process, maxWaitSec: 5.0f);
                        }

                        return completeLaunchProduct(productId, ELaunchProductResult.Succeeded, transactionId);
                    }
                }
            }
        }

        public bool CanInstallGameToPrimaryDrive(ProductId adgProdId)
        {
            try
            {
                var gpi = _entitlementDb.Get(adgProdId);
                if (gpi == null)
                {
                    // try again, with sync goods
                    _entitlementDb.SyncGoods();
                    gpi = _entitlementDb.Get(adgProdId);

                    if (gpi == null)
                    {
                        throw new LauncherServicesException("Game not entitled");
                    }
                }
                var manifestWithVersion = _entitlementPlugin.DownloadManifestAsync(gpi.Id, adgProdId).Result;
                var bytesRequired = manifestWithVersion.ManifestData.ComputeTotalSizeInBytes();

                var driveInfo = new DriveInfo(_osDriver.PrimaryLibraryRoot);
                if (driveInfo.AvailableFreeSpace <= bytesRequired)
                {
                    return false;
                }

                return true;
            }
            catch (LauncherServicesException)
            {
                throw;
            }
            catch (Exception e)
            {
                FuelLog.Error("Game not entitled when checking for primary drive install", new { adgProdId });
                throw new LauncherServicesException("Game not entitled", e);
            }
        }

        public void SendFuelLauncherActionEvent(ProductId adgProductId, string asin, string sdsVersionId, FuelLauncherActionType actionType, string transactionId)
        {
            var deviceId = DeviceInfo.DSN;
            var tuid = _twitchUserStoragePlugin?.User?.Tuid;
            var clientTime = SpadeClient.ClientTimeNow();
            _metrics.SendSpadeEvent(new FuelLauncherAction(deviceId, tuid, clientTime, asin, adgProductId?.Id, sdsVersionId, actionType, transactionId));
        }

        public void SendFuelLauncherActionErrorEvent(ProductId adgProductId, string asin, string sdsVersionId, string errorReason, FuelLauncherErrorType errorType, string transactionId)
        {
            var deviceId = DeviceInfo.DSN;
            var tuid = _twitchUserStoragePlugin?.User?.Tuid;
            var clientTime = SpadeClient.ClientTimeNow();
            _metrics.SendSpadeEvent(new FuelLauncherError(deviceId, tuid, clientTime, asin, adgProductId?.Id, sdsVersionId, errorReason, errorType, transactionId));
        }
    }
}