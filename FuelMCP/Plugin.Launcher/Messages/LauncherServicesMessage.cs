﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Launcher.Messages
{
    public class MessageInstallUpdateEventMessage : GenericTinyMessage<ProductId>
    {
        public ProductId ProductId => Content;
        public bool IsUpdate { get; private set; }
        public string Title { get; private set; }

        public MessageInstallUpdateEventMessage(object sender, ProductId productId, bool isUpdate, string title = "") : base(sender, productId)
        {
            IsUpdate = isUpdate;
            Title = title;
        }
    }

    // Install/Update Messaging
    //////////////////////////////////////////////////////

    public class MessageInstallUpdateProductStarted : MessageInstallUpdateEventMessage
    {
        public MessageInstallUpdateProductStarted(object sender, ProductId productId, bool isUpdate, string title = "") 
            : base(sender, productId, isUpdate, title) { }
    }

    public class MessageInstallUpdateProductState : MessageInstallUpdateEventMessage
    {
        public enum EState
        {
            Initializing,
            Downloading,
            Verifying,
            Installing,
            Complete
        }

        public readonly EState State;

        public MessageInstallUpdateProductState(
            object sender,
            ProductId prodId,
            bool isUpdate,
            EState state,
            string title = "") : base(sender, prodId, isUpdate, title)
        {
            State = state;
        }
    }

    public class MessageInstallUpdateProductProgress : MessageInstallUpdateEventMessage
    {
        public long BytesDelta { get; set; }
        public long BytesReceived { get; set; }
        public long BytesTotal { get; set; }
        public int TimeRemainingSeconds { get; private set; }
        public bool TimeRemainingNotEnoughData { get; private set; }

        public MessageInstallUpdateProductProgress(
            object sender, 
            long bytesDelta,
            long bytesReceived,
            long bytesTotal,
            int timeRemainingSeconds,
            bool timeRemainingNotEnoughData,
            ProductId productId, 
            bool isUpdate, 
            string title = "")
            : base(sender, productId, isUpdate, title)
        {
            BytesDelta = bytesDelta;
            BytesReceived = bytesReceived;
            BytesTotal = bytesTotal;
            TimeRemainingSeconds = timeRemainingSeconds;
            TimeRemainingNotEnoughData = timeRemainingNotEnoughData;
        }
    }

    public class FuelRequestBlockUpdate : TinyMessageBase
    {
        public FuelRequestBlockUpdate(object context) : base(context)
        {
        }
    }

    public class FuelFreeBlockUpdate : TinyMessageBase
    {
        public FuelFreeBlockUpdate(object context) : base(context)
        {
        }
    }


    public class MessageInstallUpdateProductDownloadFileNotification : MessageInstallUpdateEventMessage
    {
        public DownloadFileMessage InnerMessage { get; private set; }

        public MessageInstallUpdateProductDownloadFileNotification(
            object sender,
            DownloadFileMessage innerMessage,
            ProductId productId,
            bool isUpdate,
            string title = "")
            : base(sender, productId, isUpdate, title)
        {
            InnerMessage = innerMessage;
        }
    }

    public class MessageRepairProductSucceeded : GenericTinyMessage<ProductId>
    {
        public ProductId ProductId => Content;
        public string Title { get; private set; }

        public MessageRepairProductSucceeded(object sender, ProductId productId, string title = "")
            : base(sender, productId) { Title = title; }
    }

    public class MessageRepairProductFailed : GenericTinyMessage<ProductId>
    {
        public ProductId ProductId => Content;
        public string Title { get; private set; }
        public ERepairProductResult Result { get; private set; }

        public MessageRepairProductFailed(object sender, ERepairProductResult result, ProductId productId, string title = "")
            : base(sender, productId) { Title = title; Result = result; }
    }

    public class MessageInstallUpdateProductSucceeded : MessageInstallUpdateEventMessage
    {
        public MessageInstallUpdateProductSucceeded(object sender, ProductId productId, bool isUpdate, string title = "")
            : base(sender, productId, isUpdate, title) { }
    }

    public class MessageInstallUpdateProductFailed : MessageInstallUpdateEventMessage
    {
        public EInstallUpdateProductResult Result { get; private set; }

        public MessageInstallUpdateProductFailed(object sender, EInstallUpdateProductResult result, ProductId productId, bool isUpdate, string title = "") 
            : base(sender, productId, isUpdate, title)
        {
            Result = result;
        }
    }

    // Launch Messaging
    //////////////////////////////////////////////////////

    public class MessageLaunchProductFailed : TinyMessageBase
    {
        public ProductId Id { get; private set; }
        public ELaunchProductResult Result { get; private set; }
        public string Title { get; private set; }

        public MessageLaunchProductFailed(object sender, ELaunchProductResult result, ProductId id, string title = "") : base(sender)
        {
            Id = id;
            Result = result;
            Title = title;
        }
    }
}
