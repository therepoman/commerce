﻿using Amazon.Fuel.Common.Contracts;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Clients.Amazon.CoralCSharpClientBase
{
    /// <summary>
    /// This class is not threadsafe since it has a shared WebRequest.
    /// </summary>
    public abstract class AbstractCoralClient
    {
        public string Url { get; protected set; }
        public HttpWebRequest request { get; protected set; }
        public string ServicePath { get; protected set; }
        public string Operation { get; protected set; }
        public object JsonData { get; protected set; }
        protected string JsonDataString { get; set; }
                
        protected Dictionary<string,string> CustomHeaders { get; set; }

        public AbstractCoralClient( string url, string servicePath, string operation )
        {
            this.CustomHeaders = new Dictionary<string, string>();
            this.Url = url;
            this.ServicePath = servicePath;
            this.Operation = operation;
        }

        /// <summary>
        /// Creates a Coral request to the specified URL member
        /// </summary>
        /// <returns></returns>
        protected virtual HttpWebRequest CreateWebRequest()
        {
            var request = CreateWebRequest(new Uri(this.Url)) as HttpWebRequest;
            //[https://twitchtv.atlassian.net/browse/FLCL-306] consider applying shorter timeouts
            return request;
        }

        /// <summary>
        /// Creates a Coral request given a uri
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        protected virtual HttpWebRequest CreateWebRequest(Uri uri)
        {
            request = WebRequest.Create(uri) as HttpWebRequest;

            request.Method = "POST";
            request.Headers["Content-Encoding"] = "amz-1.0";
            request.ContentType = "application/json; charset=utf-8";
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Headers["X-Amz-Target"] = ServicePath + "." + Operation;
            request.UserAgent = "FuelSDK/release-1.0.0.0";
            
            if (CustomHeaders != null)
            {
                foreach (var header in CustomHeaders)
                {
                    request.Headers[header.Key] = header.Value;
                }
            }

            return request;
        }

        /// <summary>
        /// Encodes the id as an entitlementId, returns the encoded byte data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual byte[] GetPostData()
        {
            var message = JsonConvert.SerializeObject(JsonData);
            JsonDataString = message;

            byte[] bytes = Encoding.UTF8.GetBytes(message);
            return bytes;
        }

        /// <summary>
        /// The web request with the id data already async pushed into the stream.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual async Task<HttpWebRequest> GetRequestAsync(CancellationToken? cancellationToken = null)
        {
            var request = CreateWebRequest();

            var bytes = GetPostData();
            request.ContentLength = bytes.Length;

            //This seems to garuntee that the bytes were written before moving on:
            using (var stream = await request.GetRequestStreamAsync())
            {
                if (cancellationToken.HasValue)
                {
                    await stream.WriteAsync(bytes, 0, bytes.Length, cancellationToken.Value);
                }
                else
                {
                    await stream.WriteAsync(bytes, 0, bytes.Length);
                }
            }

            return request;
        }

        public virtual async Task<OperationResponse> RunOperation(CancellationToken? cancellationToken = null)
        {
            OperationResponse result = new OperationResponse();
            WebResponse response = null;
            result.ResponseData = null;

            try
            {
                var request = await GetRequestAsync(cancellationToken);
                if (cancellationToken.HasValue && cancellationToken.Value.IsCancellationRequested)
                {
                    return null;
                }

                // $$ jbil -- implement cancellationtoken 
                //              http://stackoverflow.com/questions/19211972/getresponseasync-does-not-accept-cancellationtoken
                response = await request.GetResponseAsync();
                if (response != null)
                {
                    if ( response is HttpWebResponse )
                    {
                        result.StatusCode = ((HttpWebResponse)response).StatusCode;
                    }

                    var responseStream = response.GetResponseStream();
                    string responseData = "";
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        responseData += await reader.ReadToEndAsync();
                    }

                    result.ResponseData = responseData;
                    result.Result = string.IsNullOrEmpty(result.ResponseData) ? OperationResponse.EResult.ResponseEmpty 
                                                                              : OperationResponse.EResult.Success;
                }
                else
                {
                    result.Result = OperationResponse.EResult.UnknownResponseFailure;
                }
            }
            catch (OperationCanceledException)
            {
                Log.Information("[COR] Download operation cancelled");
                result = null;
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.RequestCanceled)
                {
                    // apparently cancellations can occur like this
                    Log.Information("[COR] Download operation cancelled");
                    result = null;
                }
                else
                {
                    try
                    {
                        var responseStream = ex.Response?.GetResponseStream();
                        if (responseStream != null)
                        {
                            string responseData = "";
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                responseData += await reader.ReadToEndAsync();
                            }
                            result.ResponseData = responseData;
                            Log.Information($"[COR] Response: '{responseData}'");
                        }
                    }
                    catch (Exception) { }

                    result.Exception = ex;
                    result.Result = OperationResponse.EResult.FailedWithException;
                    if (ex.Response is HttpWebResponse)
                    {
                        result.StatusCode = ((HttpWebResponse)ex.Response).StatusCode;
                    }

                    string requestDetail = null;
                    if (request != null)
                    {
                        requestDetail = $"[{request}:Uri={request.RequestUri},Operation={Operation},StatusCode={result.StatusCode}]";
                    }

                    var details = await ReadErrorDetailsAsync(ex);
                    LogKnownErrors(requestDetail, details, ex);
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Result = OperationResponse.EResult.FailedWithException;
                string requestDetail = null;
                if (request != null)
                {
                    requestDetail = $"[{request}:Uri={request.RequestUri},Operation={Operation}]";
                }

                Log.Error(ex, $"Exception hit during request '{requestDetail}'");
            }
            finally
            {
                response?.Close();
            }

            return result;
        }

        protected string ReadErrorDetails(WebException ex)
        {
            //The cause of the error is stored in the response stream, we have to read it seperately.
            var output = "";
            try
            {
                var stream = ex.Response.GetResponseStream();
                using (StreamReader reader = new StreamReader(stream))
                {
                    output += "\n" + reader.ReadToEnd();
                }
            }
            catch (Exception ex2)
            {
                output += "\n" + ex2.Message;
            }
            return output;
        }
        protected async Task<string> ReadErrorDetailsAsync(WebException ex)
        {
            //The cause of the error is stored in the response stream, we have to read it seperately.
            var output = "";
            try
            {
                var stream = ex.Response.GetResponseStream();
                using (StreamReader reader = new StreamReader(stream))
                {
                    output += "\n" + await reader.ReadToEndAsync();
                }
            }
            catch (Exception ex2)
            {
                output += ex.Message + "\n" + ex2.Message;
            }
            return output;
        }

        protected void LogKnownErrors(string context, string errorDetails, Exception ex)
        {
            var output = ex.Message;
            output += errorDetails;

            var requestStr = $"context:{context}";

            if (output.Contains("NotAuthorizedException"))
            {
                Log.Error(ex, $"[COR] Not authorized for download {requestStr}, Detail:{errorDetails}");
            }
            else if (output.Contains("Digital good instance not found"))
            {
                Log.Error(ex, $"[COR] Not authorized for download {requestStr}, Detail:{errorDetails}");
            }
            else if (GetType().Name == "ReloggableAmazonEntitlementService" 
                     && output.Contains("(500) Internal Server Error"))
            {
                // prevent exception spam
                Log.Information($"SyncGoods returned 500 response, possibly expired access token ({output})");
            }
            else
            {
                Log.Error(ex, $"[COR] [{GetType().Name}] {output} {requestStr}");
            }
        }
    }
}
