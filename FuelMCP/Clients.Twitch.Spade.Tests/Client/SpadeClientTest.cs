﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using Clients.Twitch.Spade.Client;
using Clients.Twitch.Spade.Model;
using Equ;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace Clients.Twitch.Spade.Tests.Client
{
    public class SpadeClientTest
    {
        private readonly Mock<ISpadeHttpClient> _mockHttpClient = new Mock<ISpadeHttpClient>();
        private readonly Mock<ISpadeHttpResponseMessage> _mockHttpResponseMessage = new Mock<ISpadeHttpResponseMessage>();
        private readonly MockSpadeEvent _spadeEvent;
        private readonly HttpContent _spadeEventHttpContent;

        public SpadeClientTest()
        {
            var spadeEventProperties = new MockSpadeEventProperty("MOCK_SPADE_EVENT_ID");
            _spadeEvent = new MockSpadeEvent(spadeEventProperties);
            _spadeEventHttpContent = GetHttpContent(_spadeEvent);
        }

        [Fact]
        public void SpadeClient_throwsArgumentNullException_givenNullClient()
        {
            Assert.Throws<ArgumentNullException>(() => new SpadeClient(null));
        }

        [Fact]
        public void SendEventAsync_throwsSpadeException_givenHttpException()
        {
            _mockHttpClient.Setup(client => client.PostAsync(It.IsAny<HttpContent>())).Throws<HttpException>();
            var spadeClient = new SpadeClient(_mockHttpClient.Object);
            Assert.ThrowsAsync<SpadeException>(() => spadeClient.SendEventAsync(_spadeEvent));
        }

        [Fact]
        public void SendEventAsync_throwsSpadeException_givenNon200HttpResponseFromSpadeService()
        {
            _mockHttpResponseMessage.SetupGet(response => response.IsSuccessStatusCode).Returns(false);
            _mockHttpClient.Setup(client => client.PostAsync(It.IsAny<HttpContent>())).ReturnsAsync(_mockHttpResponseMessage.Object);
            var spadeClient = new SpadeClient(_mockHttpClient.Object);
            Assert.ThrowsAsync<SpadeException>(() => spadeClient.SendEventAsync(_spadeEvent));
        }

        [Fact]
        public void SendEventAsync_succeedes_givenValidInput()
        {
            _mockHttpResponseMessage.SetupGet(response => response.StatusCode).Returns(HttpStatusCode.NoContent);
            _mockHttpClient.Setup(client => client.PostAsync(
                It.Is<HttpContent>(content => Equals(content, _spadeEventHttpContent))))
                .ReturnsAsync(_mockHttpResponseMessage.Object);
            var spadeClient = new SpadeClient(_mockHttpClient.Object);
            spadeClient.SendEventAsync(_spadeEvent).Wait();
        }

        //Create expected HttpContent for given SpadeEvent
        private static HttpContent GetHttpContent(ISpadeEvent spadeEvent)
        {
            var jsonString = JsonConvert.SerializeObject(spadeEvent);
            var base64JsonString = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonString));
            var postData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("data", base64JsonString)
                };
            return new FormUrlEncodedContent(postData);
        }

        private bool Equals(HttpContent content1, HttpContent content2)
        {
            //Assume that if the content lengths and type match, that they are equal.
            return (content1.Headers.ContentLength == content2.Headers.ContentLength) 
                && Equals(content1.Headers.ContentType, content2.Headers.ContentType);
        }

        private class MockSpadeEvent : ISpadeEvent
        {
            public MockSpadeEvent(MockSpadeEventProperty id)
            {
                Properties[id.Key] = id.Value;
            }

            public string Event => "mock_spade_event";
            public Dictionary<string, dynamic> Properties => new Dictionary<string, dynamic>();
        }

        private class MockSpadeEventProperty : MemberwiseEquatable<MockSpadeEventProperty>, ISpadeEventProperty
        {
            public MockSpadeEventProperty(string id)
            {
                Value = id;
            }

            public string Key => "id";
            public dynamic Value { get; }
        }
    }
}
