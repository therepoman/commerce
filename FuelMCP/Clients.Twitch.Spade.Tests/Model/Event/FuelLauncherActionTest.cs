﻿using System;
using Clients.Twitch.Spade.Model.Event;
using Newtonsoft.Json;
using Xunit;

namespace Clients.Twitch.Spade.Tests.Model.Event
{
    public class FuelLauncherActionTest
    {
        private const string DeviceId = "mockDeviceId";
        private const string UserId = "mockUserId123";
        private const string Asin = "mockAsin897";
        private const string AdgProductId = "mock-adg-product-id-12345";
        private const string SdsVersionId = "mock-sds-version-id-33333";
        private const double ClientTime = 1234567;
        private const FuelLauncherActionType Action = FuelLauncherActionType.GameLaunchStart;
        private const string TransactionId = "mock-trans-action-id-44444";

        [Fact]
        public void FuelLauncherAction_throwsArgumentNullException_givenNullDeviceId()
        {
            Assert.Throws<ArgumentNullException>(() => new FuelLauncherAction(null, UserId, ClientTime, Asin, AdgProductId, SdsVersionId, Action, TransactionId));
        }

        [Fact]
        public void FuelLauncherAction_serializesToExpectedJson_givenValidInput()
        {
            var expectedJson = "{\"event\":\"fuel_launcher_action\",\"properties\":{\"device_id\":\"mockDeviceId\",\"user_id\":\"mockUserId123\",\"client_time\":1234567.0,\"asin\":\"mockAsin897\",\"adg_product_id\":\"mock-adg-product-id-12345\",\"sds_version_id\":\"mock-sds-version-id-33333\",\"action\":\"GameLaunchStart\",\"transaction_id\":\"mock-trans-action-id-44444\"}}";
            var spadeEvent = new FuelLauncherAction(DeviceId, UserId, ClientTime, Asin, AdgProductId, SdsVersionId, Action, TransactionId);
            var actualJson = JsonConvert.SerializeObject(spadeEvent);
            Assert.Equal(expectedJson, actualJson);
        }
    }
}
