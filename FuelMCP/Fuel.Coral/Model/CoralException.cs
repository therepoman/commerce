﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Amazon.Fuel.Coral.Model
{
    //Base model for Coral Exceptions
    //
    public class CoralException : Exception
    {
        //Map of CoralModelType('__type') to C# class
        private static IDictionary<string, Type> CoralExceptionTypesCache;

        //The full <namespace>#<type> for the coral model type. This should be overridden by subclasses of CoralException
        //e.g. com.amazonaws.gearbox.softwaredistribution.service.model#ForbiddenException
        public virtual string __type { get; }
        public string message { get; set; }
        
        public CoralException(string message) : base(message) { }

        //Constructor used during JSON deserialization
        public CoralException(SerializationInfo info, StreamingContext context)
        {
            if (info != null)
            {
                //There is no TryGet for SerializationInfo, so we will iterate though each field and see if it matches one we are interested.
                //Otherwise we get a SerializationException if the field doesn't exist (e.g. no "message" defined for the exception)
                foreach (SerializationEntry entry in info)
                {
                    switch (entry.Name)
                    {
                        case "__type":
                            __type = info.GetString("__type");
                            break;
                        case "message":
                            message = info.GetString("message");
                            break;
                    }
                }
            }
        }

        //Method used during JSON serialization. This needs to be override because the Exception class implements
        //ISerializable
        // <inheritdoc />
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info != null)
            {
                info.AddValue("__type", __type);
                info.AddValue("message", message);
            }
        }

        //Tries to create the specific CoralException (NotFoundException, AuthorizationException, etc) for the given general CoralException.
        //If no C# class is found, then an UndefinedServiceException is returned.
        public static CoralException GetSpecificException(CoralException generalException)
        {
            if (generalException?.__type != null && GetCoralExceptionTypes().ContainsKey(generalException.__type))
            {
                var specificExceptionType = GetCoralExceptionTypes()[generalException.__type];
                var specificCoralException = (CoralException) specificExceptionType
                    .GetConstructor(new[] {typeof(string)})?.Invoke(new object[] {generalException.message});
                if (specificCoralException != null)
                {
                    return specificCoralException;
                }
            }

            return new UndefinedServiceException($"[Unknown exception was thrown from service: [{generalException.__type}] {generalException.message}");
        }

        //We use reflection to cache all the CoralException Types so we don't have to search through all the loaded class every time.
        private static IDictionary<string, Type> GetCoralExceptionTypes()
        {
            //lazyload the CoralException Types Cache
            if (CoralExceptionTypesCache == null)
            {
                CoralExceptionTypesCache = new Dictionary<string, Type>();
                //Get a list of all the CoralException subclasses so we can match the specific one defined by the generalException
                //Nested loops but exceptions should be rare. We could consider caching all the exception classes.
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    var coralExceptionTypes = assembly.GetTypes().Where(type => type.IsSubclassOf(typeof(CoralException)));
                    foreach (Type coralExceptionType in coralExceptionTypes)
                    {
                        var coralException = (CoralException)coralExceptionType.GetConstructor(new[] { typeof(string) })?.Invoke(new object[] { string.Empty });
                        if (!string.IsNullOrEmpty(coralException?.__type))
                        {
                            CoralExceptionTypesCache.Add(coralException.__type, coralExceptionType);
                        }
                    }
                }
            }
            return CoralExceptionTypesCache;
        }
    }

    //Exception thrown when we don't have a model for the exception raised by the coral service.
    public class UndefinedServiceException : CoralException
    {
        public UndefinedServiceException(string message) : base(message)
        {
            this.message = message;
        }
    }

    //Exception thrown when there is an unexpected exception thrown from the coral client (issue parsing JSON, 
    public class CoralClientException : Exception
    {
        public CoralClientException(string message, Exception e) : base(message, e) { }
    }
}
