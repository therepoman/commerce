﻿namespace Amazon.Fuel.Coral.Model
{
    /// <summary>
    /// When implementing a subclass, make sure the property names match the ones in the Coral model. The names are case sensative.
    /// </summary>
    public abstract class CoralRequest
    {
        public abstract string Operation { get; }
    }
}
