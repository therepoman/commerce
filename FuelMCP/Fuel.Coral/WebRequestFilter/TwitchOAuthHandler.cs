﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amazon.Fuel.Coral.WebRequestFilter
{
    public class TwitchOAuthHandler : DelegatingHandler
    {
        private readonly Func<string> _tokenProvider;

        public TwitchOAuthHandler(Func<string> tokenProvider, HttpMessageHandler innerHandler) : base(innerHandler)
        {
            _tokenProvider = tokenProvider;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // Add the OAuth token headers
            string token = _tokenProvider.Invoke();
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            // Some of our services use non-standard headers as well. Hopefully these can be removed some day...
            request.Headers.Add("TwitchOAuthToken", token); // ADG
            request.Headers.Add("x-auth-twitch", token); // SDS

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
