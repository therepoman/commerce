﻿
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Amazon.Fuel.Coral
{
    public class LoggingHandler : DelegatingHandler
    {
        public LoggingHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            await LogRequestAsync(request);
            var response = await base.SendAsync(request, cancellationToken);
            await LogResponseAsync(response);

            return response;
        }

        private async Task LogRequestAsync(HttpRequestMessage request)
        {
            try
            {
                Log.Verbose(request.ToString());
                if (request.Content != null)
                {
                    var body = JToken.Parse(await request.Content.ReadAsStringAsync()).ToString(Formatting.Indented);
                    Log.Verbose(body);
                }
            }
            catch (Exception exception)
            {
                Log.Verbose(exception, "Failed to log request");
            }
        }

        private async Task LogResponseAsync(HttpResponseMessage response)
        {
            try
            {
                Log.Verbose(response.ToString());
                if (response.Content != null)
                {
                    var body = JToken.Parse(await response.Content.ReadAsStringAsync()).ToString(Formatting.Indented);
                    Log.Verbose(body);
                }
            }
            catch (Exception exception)
            {
                Log.Verbose(exception, "Failed to log response");
            }
        }
    }
}
