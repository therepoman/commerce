﻿using System;
using System.Text;
using System.Threading.Tasks;
using Amazon.Fuel.Coral.Model;
using Amazon.Fuel.Coral.WebRequestFilter;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Coral.Client
{
    public interface ICoralClient
    {
        Task<T> SendRequest<T>(CoralRequest request, CancellationToken cancellationToken = default(CancellationToken)) where T : CoralResponse;
    }

    public class CoralClientFactory
    {
        public virtual ICoralClient Create(string serviceUrl, string serviceNamespace, string useragent, IMetricsPlugin metrics)
        {
            HttpMessageHandler handler = new HttpClientHandler();

#if DEBUG
            // Log service request and responses
            handler = new LoggingHandler(handler);
#endif

            return new CoralRpcHttpClient(serviceUrl, serviceNamespace,
                new HttpClient(handler), metrics);
        }

        public virtual ICoralClient CreateTwitchAuthenticatedClient(string serviceUrl, string serviceNamespace,
            Func<string> twitchOAuthTokenProvider, string useragent, IMetricsPlugin metrics)
        {
            HttpMessageHandler handler = new TwitchOAuthHandler(twitchOAuthTokenProvider, new HttpClientHandler());

#if DEBUG
            // Log service request and responses
            handler = new LoggingHandler(handler);
#endif

            return new CoralRpcHttpClient(serviceUrl, serviceNamespace,
                new HttpClient(handler), metrics);
        }

        public virtual ICoralClient CreateTwitchAuthenticatedClient(string serviceUrl, string serviceNamespace,
            string twitchOAuthToken, string useragent, IMetricsPlugin metrics)
        {
            return CreateTwitchAuthenticatedClient(serviceUrl, serviceNamespace, () => twitchOAuthToken, useragent, metrics);
        }
    }

    internal class CoralRpcHttpClient : ICoralClient
    {
        private readonly HttpClient _client;
        private readonly string _serviceName;
        private readonly string _namespace;
        private readonly string _serviceUrl;
        private readonly IMetricsPlugin _metrics;

        public CoralRpcHttpClient(string serviceUrl, string serviceNamespace, HttpClient client, IMetricsPlugin metrics)
        {
            _serviceUrl = serviceUrl;
            _client = client;
            _namespace = serviceNamespace;
            _metrics = metrics;
            _serviceName = GetServiceNameFromServiceNamespace(serviceNamespace);
        }

        public virtual async Task<T> SendRequest<T>(CoralRequest coralRequest,
            CancellationToken cancellationToken = default(CancellationToken)) where T : CoralResponse
        {
            var metricsSource = GetMetricSource(coralRequest);
            var metricsTimer = _metrics.StartTimer(metricsSource, "Time");
            try
            {
                var response = await SendRequestImpl<T>(coralRequest, cancellationToken);
                metricsTimer.Stop();
                _metrics.AddDiscreteValue(metricsSource, "Error", 0);
                return response;
            }
            catch (Exception)
            {
                metricsTimer.Stop();
                _metrics.AddDiscreteValue(metricsSource, "Error", 1);
                throw;
            }
        }

        private async Task<T> SendRequestImpl<T>(CoralRequest coralRequest, CancellationToken cancellationToken = default(CancellationToken)) where T : CoralResponse
        {
            HttpContent content;
            try
            {
                var body = JsonConvert.SerializeObject(coralRequest);
                content = new StringContent(body, Encoding.UTF8, "application/json");
            }
            catch (Exception e)
            {
                throw new CoralClientException($"Unexpected exception thrown when trying to serialize coral request {coralRequest.Operation}.", e);
            }
            HttpContentHeaders headers = content.Headers;
            headers.Add("Content-Encoding", "amz-1.0");
            headers.Add("X-Amz-Target", $"{_namespace}.{coralRequest.Operation}");

            HttpResponseMessage response = await _client.PostAsync(_serviceUrl, content, cancellationToken);
            
            string jsonString;
            try
            {
                jsonString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                throw new CoralClientException($"Exception thrown when trying to get {typeof(T).Name}.", e);
            }

            if (!response.IsSuccessStatusCode)
            {
                //We don't know what kind of exception it is yet, so get the details and then throw a specific exception.
                CoralException generalCoralException;
                try
                {
                    generalCoralException = JsonConvert.DeserializeObject<CoralException>(jsonString);
                }
                catch (Exception e)
                {
                    throw new CoralClientException(
                        $"Unexpected exception thrown when trying to parse CoralException='{jsonString}'", e);
                }
                throw CoralException.GetSpecificException(generalCoralException);
            }

            //Service call was successfull, try to parse the response
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString);
            }
            catch (Exception e)
            {
                throw new CoralClientException(
                    $"Unexpected exception thrown when trying to parse {typeof(T).Name}.", e);
            }
        }

        private static string GetServiceNameFromServiceNamespace(string serviceNamespace)
        {
            //the service name is the last string in the '.' seperated service namespace (e.g. "com.amazon.SoftwareDistribtionService")
            var lastPeriodIndex = serviceNamespace?.LastIndexOf(".") ?? -1;
            if (lastPeriodIndex == -1)
            {
                //We couldn't find any '.'s so fallback to the whole namespace
                return serviceNamespace;
            }
            //remove all characters up to and include the last '.' 
            //(e.g. "com.amazon.SoftwareDistribtionService" -> "SoftwareDistribtionService")
            return serviceNamespace?.Remove(0, lastPeriodIndex + 1);
        }

        private string GetMetricSource(CoralRequest coralRequest)
        {
            return $"{_serviceName}.{coralRequest.Operation}";
        }
    }
}
