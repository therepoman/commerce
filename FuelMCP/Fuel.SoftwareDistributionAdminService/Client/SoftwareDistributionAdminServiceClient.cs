﻿using System;
using Fuel.Coral.Client;
using Fuel.Coral.Model;
using Fuel.SoftwareDistributionAdminService.Models;
using Polly;

namespace Fuel.SoftwareDistributionAdminService.Client
{
    public interface ISoftwareDistributionAdminServiceClientFactory
    {
        ISoftwareDistributionAdminServiceClient Create(string serviceUrl, string twitchOAuthToken, string useragent);
    }

    public class SoftwareDistributionAdminServiceClientFactory : ISoftwareDistributionAdminServiceClientFactory
    {
        private readonly CoralClientFactory _clientFactory;

        public SoftwareDistributionAdminServiceClientFactory(CoralClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public ISoftwareDistributionAdminServiceClient Create(string serviceUrl, string twitchOAuthToken, string useragent)
        {
            return new SoftwareDistributionAdminServiceClient(serviceUrl, twitchOAuthToken, useragent, _clientFactory);
        }
    }

    public interface ISoftwareDistributionAdminServiceClient
    {
        DescribeVersionResult DescribeVersion(DescribeVersionRequest request);

        void AttachVersionIngestionManifest(AttachVersionIngestionManifestRequest request);

        CreateVersionResult CreateVersion(CreateVersionRequest request);

        void SetChannelActiveVersion(SetChannelActiveVersionRequest request);

        ListProductVersionsResult ListProductVersions(ListProductVersionsRequest request);

        ListProductChannelsResult ListProductChannels(ListProductChannelsRequest request);

        ListProductsResult ListProducts(ListProductsRequest request);
    }

    public class SoftwareDistributionAdminServiceClient : ISoftwareDistributionAdminServiceClient
    {
        public const string BetaUrl = "https://sds-admin.integ.amazon.com/twitch/";
        public const string GammaUrl = "https://sds-admin-gamma-iad.iad.proxy.amazon.com/twitch/";
        public const string ProdUrl = "https://sds-admin.amazon.com/";
        private const string CoralNamespace =
            "com.amazonaws.gearbox.softwaredistribution.admin.service.model.SoftwareDistributionAdminService";
        private const int MaxRetries = 3;
        private const double InitialRetryDelay = 500;
        
        private readonly ICoralClient _coralClient;

        public SoftwareDistributionAdminServiceClient(string serviceUrl, string twitchOAuthToken, string useragent, CoralClientFactory coralClientFactory)
        {
            _coralClient = coralClientFactory.CreateTwitchAuthenticatedClient(serviceUrl, CoralNamespace,
                twitchOAuthToken, useragent);
        }

        public DescribeVersionResult DescribeVersion(DescribeVersionRequest request)
        {
            return RetryableSendRequest<DescribeVersionResult>(request);
        }

        public void AttachVersionIngestionManifest(AttachVersionIngestionManifestRequest request)
        {
            RetryableSendRequest<AttachVersionIngestionManifestResponse>(request);
        }

        public CreateVersionResult CreateVersion(CreateVersionRequest request)
        { 
            return RetryableSendRequest<CreateVersionResult>(request);
        }

        public void SetChannelActiveVersion(SetChannelActiveVersionRequest request)
        {
            RetryableSendRequest<SetChannelActiveVersionResponse>(request);
        }

        public ListProductVersionsResult ListProductVersions(ListProductVersionsRequest request)
        {
            return RetryableSendRequest<ListProductVersionsResult>(request);
        }

        public ListProductChannelsResult ListProductChannels(ListProductChannelsRequest request)
        {
            return RetryableSendRequest<ListProductChannelsResult>(request);
        }

        public ListProductsResult ListProducts(ListProductsRequest request)
        {
            return RetryableSendRequest<ListProductsResult>(request);
        }

        private T SendRequest<T>(CoralRequest request) where T : CoralResponse
        {
            return _coralClient.SendRequest<T>(request).Result;
        }

        private T RetryableSendRequest<T>(CoralRequest request) where T : CoralResponse
        {
            return Policy
                .Handle<InvalidTwitchAuthException>()
                .WaitAndRetry(MaxRetries,
                    retryAttempt => TimeSpan.FromMilliseconds(InitialRetryDelay * Math.Pow(retryAttempt, 2)))
                .Execute(() => SendRequest<T>(request));
        }
    }
}
