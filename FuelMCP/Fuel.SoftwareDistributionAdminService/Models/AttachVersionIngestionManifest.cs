﻿using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class AttachVersionIngestionManifestRequest : CoralRequest
    {
        public override string Operation { get; } = "AttachVersionIngestionManifest";
        public string versionId { get; set; }
        /// <summary>
        /// An XML string of the ingest-manifest to be uploaded.
        /// </summary>
        public string manifest { get; set; }

        public AttachVersionIngestionManifestRequest(string versionId, string manifest)
        {
            this.versionId = versionId;
            this.manifest = manifest;
        }
    }

    public sealed class AttachVersionIngestionManifestResponse : CoralResponse { }
}
