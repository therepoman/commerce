﻿using System.Collections.Generic;
using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class ListProductChannelsRequest : CoralRequest
    {
        public override string Operation { get; } = "ListProductChannels";

        public string productId { get; set; }
        public string exclusiveStartChannelName { get; set; }
        public int limit { get; set; }
    }
    public sealed class ListProductChannelsResult : CoralResponse
    {
        public List<ChannelRef> channels { get; set; }
        public string lastEvaluatedChannelName { get; set; }

        public ListProductChannelsResult(List<ChannelRef> channels, string lastEvaluatedChannelName)
        {
            this.channels = channels;
            this.lastEvaluatedChannelName = lastEvaluatedChannelName;
        }
    }

    public sealed class ChannelRef
    {
        public string channelId { get; set; }
        public string channelName { get; set; }
        public string activeVersionId { get; set; }
        public string description { get; set; }

        public ChannelRef(string channelId, string channelName, string activeVersionId, string description)
        {
            this.channelId = channelId;
            this.channelName = channelName;
            this.activeVersionId = activeVersionId;
            this.description = description;
        }
    }
}
