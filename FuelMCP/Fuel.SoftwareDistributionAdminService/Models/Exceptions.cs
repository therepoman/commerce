﻿using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    //Translate to known Exceptions defined in the Coral model for SDAS
    // https://code.amazon.com/packages/SoftwareDistributionAdminServiceModel/blobs/mainline/--/model/exceptions.xml
    public sealed class DependencyException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.admin.service.model#DependencyException";
        public DependencyException(string message) : base(message) { }
    }

    public sealed class NotFoundException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.admin.service.model#NotFoundException";
        public NotFoundException(string message) : base(message) { }
    }

    public sealed class InvalidStateException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.admin.service.model#InvalidStateException";
        public InvalidStateException(string message) : base(message) { }
    }

    public sealed class InvalidEntitlementException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.admin.service.model#InvalidEntitlementException";
        public InvalidEntitlementException(string message) : base(message) { }
    }

    public sealed class InvalidManifestException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.admin.service.model#InvalidManifestException";
        public InvalidManifestException(string message) : base(message) { }
    }

    public sealed class InvalidTwitchAuthException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.admin.service.model#InvalidTwitchAuthException";
        public InvalidTwitchAuthException(string message) : base(message) { }
    }
}
