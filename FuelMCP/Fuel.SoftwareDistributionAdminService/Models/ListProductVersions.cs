﻿using System.Collections.Generic;
using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class ListProductVersionsRequest : CoralRequest
    {
        public override string Operation { get; } = "ListProductVersions";

        public string productId { get; set; }
        public string exclusiveStartVersionName { get; set; }
        public int limit { get; set; }
    }
    public sealed class ListProductVersionsResult : CoralResponse
    {
        public List<VersionRef> versions { get; set; }
        public string lastEvaluatedVersionName { get; set; }

        public ListProductVersionsResult(List<VersionRef> versions, string lastEvaluatedVersionName)
        {
            this.versions = versions;
            this.lastEvaluatedVersionName = lastEvaluatedVersionName;
        }
    }

    public sealed class VersionRef
    {
        public string versionId { get; set; }
        public string versionName { get; set; }
        public string description { get; set; }
        public VersionStatus status { get; set; }

        public VersionRef(string versionId, string versionName, string description, VersionStatus status)
        {
            this.versionId = versionId;
            this.versionName = versionName;
            this.description = description;
            this.status = status;
        }
    }
}
