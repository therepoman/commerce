﻿using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class SetChannelActiveVersionRequest : CoralRequest
    {
        public override string Operation { get; } = "SetChannelActiveVersion";

        public string channelId { get; set; }
        public string versionId { get; set; }
    }
    public sealed class SetChannelActiveVersionResponse : CoralResponse { }
}
