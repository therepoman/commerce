﻿using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class DescribeVersionRequest : CoralRequest
    {
        public override string Operation { get; } = "DescribeVersion";
        public string versionId { get; set; }
    }
    public sealed class DescribeVersionResult : CoralResponse
    {
        public VersionDetails version { get; set; }

        public DescribeVersionResult(VersionDetails version)
        {
            this.version = version;
        }
    }
}
