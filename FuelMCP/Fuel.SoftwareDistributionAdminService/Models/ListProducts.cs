﻿using System.Collections.Generic;
using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class ListProductsRequest : CoralRequest
    {
        public override string Operation { get; } = "ListProducts";

        public string exclusiveStartProductName { get; set; }
        public int limit { get; set; }
    }
    public sealed class ListProductsResult : CoralResponse
    {
        public List<ProductRef> products { get; set; }
        public string lastEvaluatedProductName { get; set; }

        public ListProductsResult(List<ProductRef> products, string lastEvaluatedProductName)
        {
            this.products = products;
            this.lastEvaluatedProductName = lastEvaluatedProductName;
        }
    }

    public sealed class ProductRef
    {
        public string productId { get; set; }
        public string productName { get; set; }
        public string description { get; set; }

        public ProductRef(string productId, string productName, string description)
        {
            this.productId = productId;
            this.productName = productName;
            this.description = description;
        }
    }
}
