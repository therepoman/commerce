﻿using System.Collections.Generic;
using Fuel.Coral.Model;

namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class CreateVersionRequest : CoralRequest
    {
        public override string Operation { get; } = "CreateVersion";

        public string productId { get; set; }
        public string versionName { get; set; }
        public string description { get; set; }
        public List<ProductAsset> productAssetList { get; set; }
    }

    public sealed class ProductAsset
    {
        public string path { get; set; }
    }

    public sealed class CreateVersionResult : CoralResponse
    {
        public VersionDetails version { get; set; }

        public CreateVersionResult(VersionDetails version)
        {
            this.version = version;
        }
    }
}
