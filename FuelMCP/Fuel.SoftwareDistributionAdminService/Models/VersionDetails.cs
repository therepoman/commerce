﻿namespace Fuel.SoftwareDistributionAdminService.Models
{
    public sealed class VersionDetails
    {
        public string productId { get; set; }
        public string versionId { get; set; }
        public string versionName { get; set; }
        public string description { get; set; }
        public VersionStatus status { get; set; }
        public string statusMessage { get; set; }
        public string uploadManifest { get; set; }

        public VersionDetails(string productId, string versionId, string versionName, string description, VersionStatus status, string statusMessage, string uploadManifest)
        {
            this.productId = productId;
            this.versionId = versionId;
            this.versionName = versionName;
            this.description = description;
            this.status = status;
            this.statusMessage = statusMessage;
            this.uploadManifest = uploadManifest;
        }
    }

    public enum VersionStatus
    {
        INCOMPLETE,
        PENDING,
        PROCESSING,
        AVAILABLE,
        ERROR
    }
}