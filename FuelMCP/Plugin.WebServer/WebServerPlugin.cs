﻿using System;
using System.IO;
using Amazon.MCP.Common.Contracts;
using Amazon.MCP.Common.Logging;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using Amazon.MCP.Common.Messages;
using Autofac;
using Microsoft.Owin.Hosting.Tracing;

namespace Amazon.MCP.Plugin.WebServer
{
    public class WebServerPlugin : IWebServerPlugin, IStartablePostContainerBuild
    {
        private readonly IMessengerHub _messengerHub;
        private readonly ILifetimeScope _container;
        private IDisposable server;

        public string Name
        {
            get { throw new NotImplementedException(); }
        }
        
        public WebServerPlugin(IMessengerHub messengerHub, ILifetimeScope container)
        {
            _messengerHub = messengerHub;
            _container = container;
        }

        public void StartPostContainerBuild()
        {
            var url = "http://localhost:8082";
            var options = new StartOptions(url);

            server = WebApp.Start(options, (ab) =>
            {
                var config = new HubConfiguration();
                config.Resolver = new Autofac.Integration.SignalR.AutofacDependencyResolver(_container);

                ab.UseAutofacMiddleware(_container);
                ab.UseCors(CorsOptions.AllowAll);
                ab.MapSignalR("/signalr", config);
            });

            PmetLog.WriteOneShotEvent("Started");
            _messengerHub.Publish(new FuelStartupMessage());
            AttemptDebugCrash();

            Console.WriteLine("Server running on {0}", url);
        }

        public void Dispose()
        {
            if (server != null)
            {
                server.Dispose();
            }
        }

        /// <summary>
        /// Private method that forces the app to crash by dividing by zero if the correct command
        /// line args are provided and this is a DEBUG build. Does nothing for RELEASE builds.
        /// The command to force crash is "--fc true" (without the quotes).
        /// </summary>
        private void AttemptDebugCrash()
        {
#if DEBUG
            string[] args = Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].ToLower() == "--fc" && i <= args.Length - 2)
                {
                    if (args[i + 1].ToLower() == "true")
                    {
                        int foo = 77;
                        int bar = 0;
                        foo = foo / bar;
                    }
                }
            }
#endif
        }
    }
}