﻿using System.Collections.Generic;
using System.Linq;
using Amazon.MCP.Common.Contracts;
using Amazon.MCP.Common.Implementations;
using Microsoft.AspNet.SignalR.Hubs;
using Serilog;

namespace Amazon.MCP.Plugin.WebServer
{
    internal class MefHubActivator : IHubActivator
    {
        private IEnumerable<IHub> _hubs;

        public MefHubActivator()
        {
            foreach (IHub hub in _hubs)
            {
                Log.Information("Loaded hub of type {HubType}", hub.GetType());
            }
        }

        public IHub Create(HubDescriptor descriptor)
        {
            var newHub = _hubs.First(x => x.GetType() == descriptor.HubType);
            return newHub;
        }
    }
}