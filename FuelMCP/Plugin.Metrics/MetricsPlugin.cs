﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Metrics.Models;
using Amazon.Fuel.Common.Configuration;
using Amazon.Fuel.Common.Resources;
using Clients.Twitch.Spade.Client;
using Clients.Twitch.Spade.Model;
using Polly;
using Serilog;

public class MetricsException : Exception
{
    public MetricsException(string msg) : base(msg) { }
}

// see https://w.amazon.com/index.php/MobileDiagnostics/Platform/Metrics/FAQ for info on PMET/DCM
// (use igraph to manage collection DB)

namespace Amazon.Fuel.Plugin.Metrics
{
    public class MetricsPlugin : IMetricsPlugin
    {
        protected string _driverPrefix              = "MetricsPlugin/";

        protected IMessengerHub _messengerHub;
        protected ServiceMetricTransport _transport;
        protected MetricQueue _queue;
        private readonly AppInfo _appInfo = new AppInfo();

#if DEBUG
        protected const bool _isEnabled         = false;
#else
        protected const bool _isEnabled         = true;
#endif
        protected const int _maxQueueSize       = 100;

        protected const float _metricsTransmissionIntervalSeconds = 60.0f;

        protected const string _metricsEndpt = "https://device-metrics-us-2.amazon.com/metricsBatch";

        protected const string _programName = "TwitchLauncher";

        protected readonly DateTime _appStartTimeUtc;
        protected bool _appStartTimerRecorded;
        private readonly ISpadeClient _spadeClient;

        public MetricsPlugin(string deviceId, DateTime appStartTimeUtc, ISpadeClient spadeClient)
        {
            this._transport = new ServiceMetricTransport(
                                deviceType:         deviceId,
                                deviceSerialNumber: _appInfo.DeviceSerialId,
                                softwareVersion:    _appInfo.Version);

            this._queue = new MetricQueue(
                                postAddr       : _metricsEndpt,
                                intervalSeconds: _metricsTransmissionIntervalSeconds,
                                isEnabled      : _isEnabled,
                                maxQueueSize   : _maxQueueSize,
                                transport      : _transport);

            _appStartTimeUtc = appStartTimeUtc;
            _appStartTimerRecorded = false;

            if (spadeClient == null) { throw new ArgumentNullException(nameof(spadeClient)); }
            _spadeClient = spadeClient;
        }

        public ETransmitResult TransmitMetricBatch(IEnumerable<MetricScope> metricEvents)
        {
            return this._transport.TransmitMetricBatch(_metricsEndpt, metricEvents);
        }

        public IMetricScope CreateScope(string source)
        {
            return new MetricScope(_programName, source, _queue);
        }

        public void AddCounter(string source, string name, string pivotKey = null, string pivotValue = null)
        {
            var metric = new MetricScope(_programName, source, _queue);
            metric.IncrementCounter(name);
            if (!(string.IsNullOrEmpty(pivotKey) || string.IsNullOrEmpty(pivotValue)))
            {
                metric.AddDiscreteValue(pivotKey, pivotValue);
            }
            metric.Dispose();
        }

        public void AddCounter(string source, string name)
        {
            AddCounter(source, name, null, null);
        }

        public void AddDiscreteValue(string source, string name, int value, string pivotKey = null, string pivotValue = null)
        {
            var metric = new MetricScope(_programName, source, _queue);
            metric.SetCounterValue(name, value);
            if (!(string.IsNullOrEmpty(pivotKey) || string.IsNullOrEmpty(pivotValue)))
            {
                metric.AddDiscreteValue(pivotKey, pivotValue);
            }
            metric.Dispose();
        }

        public void AddDiscreteValue(string source, string name, int value)
        {
            AddDiscreteValue(source, name, value, null, null);
        }

        public void AddTimerValue(string source, string name, double value)
        {
            var metric = new MetricScope(_programName, source, _queue);
            metric.AddDoubleValue(name, value);
            metric.Dispose();
        }

        public void AddAppStartTimer(string source, string name, string pivotKey = null, string pivotValue = null)
        {
            if (_appStartTimerRecorded)
            {
                return;
            }

            var metric = new MetricScope(_programName, source, _queue);
            var timer = metric.SetTimer(name, _appStartTimeUtc);
            if (!(string.IsNullOrEmpty(pivotKey) || string.IsNullOrEmpty(pivotValue)))
            {
                metric.AddDiscreteValue(pivotKey, pivotValue);
            }

            timer.Dispose();
            metric.Dispose();
            _appStartTimerRecorded = true;
        }

        public void AddAppStartTimer(string source, string name)
        {
            AddAppStartTimer(source, name, null, null);
        }

        public IMetricsTimer StartTimer(string source, string name)
        {
            var metricScope = new MetricScope(_programName, source, _queue);
            var metricTimerDataPoint = metricScope.StartTimer(name);
            return new MetricsTimer(metricScope, metricTimerDataPoint);
        }

        public void FlushQueue()
        {
            if ( _queue.PendingEventCount > 0 )
            {
                _queue.Wait();
            }
        }

        public void Dispose()
        {
            if (this._transport != null)
            {
                this._transport.Dispose();
                this._transport = null;
            }
        }

        public void SendSpadeEvent(ISpadeEvent spadeEvent)
        {
            Task.Run(() => SendSpadeEventSafe(spadeEvent));
        }

        private void SendSpadeEventSafe(ISpadeEvent spadeEvent)
        {
            try
            {
                Policy
                .Handle<Exception>()
                .WaitAndRetry(4,
                    retryAttempt => TimeSpan.FromMilliseconds(100 * Math.Pow(retryAttempt, 1.5)))
                .Execute(() => { _spadeClient.SendEventAsync(spadeEvent).Wait(); });
                
                AddDiscreteValue(MetricStrings.SOURCE_SpadeEvent, MetricStrings.METRIC_Failure, 0);
            }
            catch (Exception e)
            {
                //TODO: Figure out retry/queueing strategy when adding offline mode.
                Log.Warning(e, "[MetricsPlugin] Unable to send SpadeEvent");
                AddDiscreteValue(MetricStrings.SOURCE_SpadeEvent, MetricStrings.METRIC_Failure, 1);
            }
        }
    }

    public class MetricsTimer : IMetricsTimer
    {
        private readonly IMetricScope _metricScope;
        private readonly IMetricTimerDataPoint _metricTimerDataPoint;
        private bool _isStopped;
        public MetricsTimer(MetricScope metricScope, IMetricTimerDataPoint metricTimerDataPoint)
        {
            _metricScope = metricScope;
            _metricTimerDataPoint = metricTimerDataPoint;
        }

        public void Stop()
        {
            if (!_isStopped)
            {
                _metricTimerDataPoint?.Stop();
                _metricScope?.Dispose();
                _isStopped = true;
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
