﻿namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public abstract class MetricDataPoint
    {
        protected MetricDataPoint(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        public abstract DataPointMessage Serialize();
    }
}