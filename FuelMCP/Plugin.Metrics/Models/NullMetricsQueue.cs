using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    // Don't record metrics
    class NullMetricsQueue : IMetricQueue
    {
        public void RecordEvent(MetricScope metricScope)
        {
        }
    }

}