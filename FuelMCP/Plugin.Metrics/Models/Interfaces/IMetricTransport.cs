﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public enum ETransmitResult
    {
        Failure,
        Success
    }

    public interface IMetricTransport : IDisposable
    {
        ETransmitResult TransmitMetricBatch(string postAddr, IEnumerable<MetricScope> metricEvents);
    }
}
