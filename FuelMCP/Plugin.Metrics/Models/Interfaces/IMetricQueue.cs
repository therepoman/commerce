﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public interface IMetricQueue
    {
        void RecordEvent(MetricScope metricScope);
    }
}
