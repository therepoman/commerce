﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class MetricScope : IMetricScope
    {
        private readonly ConcurrentDictionary<string, MetricDataPoint> _dataPoints;
        private readonly IMetricQueue _metricsQueue;

        public string Program { get; set; }
        public string Source { get; set; }

        public MetricScope(string program, string source, IMetricQueue metricsQueue)
        {
            _dataPoints = new ConcurrentDictionary<string, MetricDataPoint>();
            _metricsQueue = metricsQueue;
            Program = program;
            Source = source;
        }

        public void IncrementCounter(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                Debug.Fail("Metric name null or empty");
                return;
            }

            var dataPoint = _dataPoints.GetOrAdd(name, new MetricCounterDataPoint(name)) as MetricCounterDataPoint;
            Debug.Assert(dataPoint != null);
            dataPoint?.Increment();
        }

        public void SetCounterValue(string name, int value)
        {
            if (string.IsNullOrEmpty(name))
            {
                Debug.Fail("Metric name null or empty");
                return;
            }

            var dataPoint = _dataPoints.GetOrAdd(name, new MetricCounterDataPoint(name)) as MetricCounterDataPoint;
            Debug.Assert(dataPoint != null);
            dataPoint?.SetValue(value);
        }

        public void AddCounterValue(string name, int value)
        {
            if (string.IsNullOrEmpty(name))
            {
                Debug.Fail("Metric name null or empty");
                return;
            }

            var dataPoint = _dataPoints.GetOrAdd(name, new MetricCounterDataPoint(name)) as MetricCounterDataPoint;
            Debug.Assert(dataPoint != null);
            dataPoint?.AddValue(value);
        }

        public void AddDiscreteValue(string name, string value)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(value))
            {
                Debug.Fail("Metric name or value null or empty");
                return;
            }

            var dataPoint =
                _dataPoints.GetOrAdd(name, new MetricDiscreteValueDataPoint(name)) as MetricDiscreteValueDataPoint;
            Debug.Assert(dataPoint != null);
            dataPoint?.AddValue(value);
        }

        public IMetricTimerDataPoint StartTimer(string name)
        {
            var result = new MetricTimerDataPoint(name);
            _dataPoints.AddOrUpdate(name, result, (k, v) => result);
            return result;
        }

        public IMetricTimerDataPoint SetTimer(string name, DateTime startTime)
        {
            var result = new MetricTimerDataPoint(name, startTime);
            _dataPoints.AddOrUpdate(name, result, (k, v) => result);
            return result;
        }

        public void AddDoubleValue(string name, double value)
        {
            if (string.IsNullOrEmpty(name))
            {
                Debug.Fail("Metric name null or empty");
                return;
            }

            var dataPoint = _dataPoints.GetOrAdd(name, new MetricDoubleDataPoint(name, value)) as MetricDoubleDataPoint;
            Debug.Assert(dataPoint != null);
        }

        public MetricEventMessage Serialize()
        {
            return new MetricEventMessage
            {
                Program = Program,
                Source = Source,
                Timestamp = ((long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds / 1000L),
                DataPoints = _dataPoints.Select(dataPoint => dataPoint.Value.Serialize()).ToList()
            };
        }

        public void Dispose()
        {
            _metricsQueue.RecordEvent(this);
        }
    }
}
