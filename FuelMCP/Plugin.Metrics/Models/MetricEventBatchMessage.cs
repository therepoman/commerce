﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class MetricEventBatchMessage
    { 
        public string DeviceSerialNumber;
        public string DeviceType;
        public IList<MetricEventMessage> MetricEntries;
    }

    public class MetricEventMessage
    {
        public string Program;
        public string Source;
        public Int64 Timestamp;
        public IList<DataPointMessage> DataPoints;
    }

    public class DataPointMessage
    {
        public string Name;
        public string Type;
        public string Value;
        public int Samples;
    }
}
