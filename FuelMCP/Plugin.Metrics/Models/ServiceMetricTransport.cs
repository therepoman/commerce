﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Serilog;
using System;
using System.Text.RegularExpressions;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class ServiceMetricTransport : Disposable, IMetricTransport
    {
        private string m_deviceType;
        private string m_deviceSerialNumber;
        private string m_softwareVersion;

        private HttpClient m_client;

        public ServiceMetricTransport(string deviceType, string deviceSerialNumber, string softwareVersion)
        {
            m_deviceType = deviceType;
            m_deviceSerialNumber = deviceSerialNumber;
            m_softwareVersion = softwareVersion;

            m_client = new HttpClient();
            m_client.DefaultRequestHeaders.Add("x-credential-token", m_deviceType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_client.Dispose();
            }
        }

        public string ConstrainField(string fieldData, string descriptor)
        {
            string ret = fieldData;
            try
            {

                // https://w.amazon.com/index.php/MonitoringTeam/MetricAgent/Tools#Invalid_Field
                // Invalid fields are fields longer then 300 characters or fields that contain invalid characters. Valid characters are A-Z a-z 0-9 . : @ _ - / 
                Regex rex = new Regex(@"[^a-zA-Z0-9 \.:@_\-/]");
                const int MAX_FIELD_LEN = 300;
                // $ jbilas -- locale chars?
                const char INVALID_CHAR_REPLACEMENT = '_';

                if (ret.Length > MAX_FIELD_LEN)
                {
                    // this _should_ be rare, so log it
                    Log.Warning(
                        $"pmet field '{descriptor}':'{fieldData}' is longer than {MAX_FIELD_LEN} chars, truncating.");
                    ret = ret.Substring(0, MAX_FIELD_LEN - 1); // -1 for null terminator safety
                }

                // replace special chars, but don't log (too spammy)
                ret = ret.ToAscii(INVALID_CHAR_REPLACEMENT.ToString());
                ret = rex.Replace(ret, INVALID_CHAR_REPLACEMENT.ToString());
            }
            catch (Exception ex)
            {
                // let's not log here .. could be spammy
                System.Diagnostics.Debug.Fail($"Exception hit {ex.Message}");
            }

            return ret;
        }

        public ETransmitResult TransmitMetricBatch(string postAddr, IEnumerable<MetricScope> metricEvents)
        {
            var batch = new JObject(
                new JProperty("_type", "simpleMetricBatch"),
                new JProperty("deviceSerialNumber", m_deviceSerialNumber),
                new JProperty("deviceType", m_deviceType),
                new JProperty("tag", "FUEL"),
                new JProperty("metricEntries",
                    new JArray(
                        from metricEvent in metricEvents.Select(a => a.Serialize())
                        select new JObject(
                            new JProperty("_type", "simpleMetricEntry"),
                            new JProperty("program", ConstrainField(fieldData: metricEvent.Program, descriptor: "program")),
                            new JProperty("source", ConstrainField(fieldData: metricEvent.Source, descriptor: "source")),
                            new JProperty("timestamp", metricEvent.Timestamp),
                            new JProperty("dataPoints",
                                new JArray(
                                    from dataPoint in metricEvent.DataPoints
                                    select new JObject(
                                        new JProperty("_type", "simpleDataPoint"),
                                        new JProperty("name", ConstrainField(fieldData: dataPoint.Name, descriptor: "name")),
                                        new JProperty("value", dataPoint.Value),
                                        new JProperty("type", dataPoint.Type),
                                        new JProperty("samples", dataPoint.Samples))))))),
                new JProperty("metadata",
                    new JObject(
                       new JProperty("AppVersion", m_softwareVersion))));

            var content = new StringContent(batch.ToString(), Encoding.UTF8, "application/json");
            HttpResponseMessage result = null;
            // [https://twitchtv.atlassian.net/browse/FLCL-199] Track down cause of this exception
            try
            {
                result = m_client.PostAsync(postAddr, content).Result;
            }
            catch (Exception ex)
            {
                Log.Warning($"Metrics exception caught: {ex}");
            }
            
            if (result == null || !result.IsSuccessStatusCode)
            {
                var allFields = metricEvents.Select(e => e.Program + "/" + e.Source)
                                            .Distinct()
                                            .Aggregate((a, b) => a + "," + b);

                Log.Warning($"[MET] Error transmitting metrics from '{allFields}'");
                return ETransmitResult.Failure;
            }

            return ETransmitResult.Success;
        }
    }
}
