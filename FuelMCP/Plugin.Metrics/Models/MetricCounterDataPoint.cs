﻿using System.Threading;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class MetricCounterDataPoint : MetricDataPoint
    {
        private int _value;

        public MetricCounterDataPoint(string name) : base(name)
        {
        }

        public void Increment()
        {
            Interlocked.Increment(ref _value);
        }

        public void AddValue(int amount)
        {
            Interlocked.Add(ref _value, amount);
        }

        public void SetValue(int amount)
        {
            Interlocked.Exchange(ref _value, amount);
        }

        public override DataPointMessage Serialize()
        {
            return new DataPointMessage
            {
                Name = Name,
                Type = "COUNTER",
                Value = _value.ToString(),
                Samples = 0
            };
        }
    }
}
