﻿using System.Collections.Generic;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    class MetricDiscreteValueDataPoint : MetricDataPoint
    {
        private readonly HashSet<string> _values;

        public MetricDiscreteValueDataPoint(string name) : base(name)
        {
            _values = new HashSet<string>();
        }

        public bool AddValue(string value)
        {
            return _values.Add(value);
        }

        public override DataPointMessage Serialize()
        {
            return new DataPointMessage
            {
                Name = Name,
                Type = "DISCRETE",
                Value = string.Join(",", _values),
                Samples = 1
            };
        }
    }
}
