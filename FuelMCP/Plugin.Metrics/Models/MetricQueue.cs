﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class MetricQueue : Disposable, IMetricQueue
    {
        object m_transmissionLock = new object();

        Timer m_timer;
        IMetricTransport m_transport;
        int m_maxQueueSize;
        bool m_isEnabled;

        ConcurrentQueue<MetricScope> m_activeQueue = new ConcurrentQueue<MetricScope>();
        TaskList m_transportTasks = new TaskList();

        string m_postAddr = null;

        public MetricQueue(string postAddr, float intervalSeconds, bool isEnabled, int maxQueueSize, 
                            IMetricTransport transport)
        {
            m_postAddr = postAddr;
            m_transport = transport;
            m_maxQueueSize = maxQueueSize;
            m_isEnabled = isEnabled;

            m_timer = new Timer(_ => TransmitBatch(), null, Timeout.Infinite, Timeout.Infinite);

            // The interval is only configured once.  It won't change until the SDK is restarted.
            // The timer is configured asynchronously to avoid a circular dependency.
            // Metrics can't be sent over the network until the config has been loaded.
            // Metrics are reported while the config is loaded.
            // This class will queue the metrics and send them later once the config has been loaded.
            Task.Run(() =>
            {
                var intervalMilliseconds = (long)(intervalSeconds * 1000.0);
                m_timer.Change(intervalMilliseconds, intervalMilliseconds);
            });
        }

        public void RecordEvent(MetricScope metricScope)
        {
            if (!m_isEnabled)
            {
                return;
            }

            m_activeQueue.Enqueue(metricScope);

            // locking to avoid multiple transmissions is done in the 
            // TransmitBatch method
            if (m_activeQueue.Count >= m_maxQueueSize)
            {
                TransmitBatch(() => m_activeQueue.Count >= m_maxQueueSize);
            }
        }

        public int PendingEventCount { get { return m_activeQueue.Count; } }

        public void Wait()
        {
            m_timer.Change(Timeout.Infinite, Timeout.Infinite);
            TransmitBatch();
            m_transportTasks.WaitForAll();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Wait();
            }
        }

        private void TransmitBatch(Func<bool> predicate = null)
        {
            if (!m_isEnabled)
            {
                return;
            }

            var batch = GetBatch(predicate);

            if (batch != null && batch.Count > 0)
            {   
                m_transportTasks.AddTask(Task.Run(() =>
                {
                    m_transport.TransmitMetricBatch(m_postAddr, batch);
                }));
            }
        }

        private List<MetricScope> GetBatch(Func<bool> predicate = null)
        {
            lock (m_transmissionLock)
            {
                // double check the condition after the lock in case
                // another thread already transmitted the batch
                if (predicate != null && !predicate())
                {
                    return null;
                }

                MetricScope tempScope;
                var transmissionList = new List<MetricScope>();
                while (m_activeQueue.TryDequeue(out tempScope))
                {
                    transmissionList.Add(tempScope);
                }
                return transmissionList;
            }
        }
    }
}
