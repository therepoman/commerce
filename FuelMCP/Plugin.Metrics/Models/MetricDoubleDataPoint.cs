﻿using Amazon.Fuel.Common.Contracts;
using System;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class MetricDoubleDataPoint : MetricDataPoint
    {
        private readonly double _value;

        public MetricDoubleDataPoint(string name, double value) : base(name)
        {
            _value = value;
        }

        public override DataPointMessage Serialize()
        {
            return new DataPointMessage
            {
                Name = Name,
                Type = "TIMER",
                Value = _value.ToString(),
                Samples = 1
            };
        }
    }
}