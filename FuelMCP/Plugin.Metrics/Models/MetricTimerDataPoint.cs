﻿using Amazon.Fuel.Common.Contracts;
using System;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public class MetricTimerDataPoint : MetricDataPoint, IMetricTimerDataPoint
    {
        private readonly DateTime _startTime;
        private DateTime _endTime;
        private bool _isStopped;

        public MetricTimerDataPoint(string name) : base(name)
        {
            _startTime = DateTime.UtcNow;
        }

        public MetricTimerDataPoint(string name, DateTime startTime) : base(name)
        {
            _startTime = startTime;
        }

        public void Stop()
        {
            if (!_isStopped)
            {
                _endTime = DateTime.UtcNow;
                _isStopped = true;
            }
        }

        public void Dispose()
        {
            Stop();
        }

        public override DataPointMessage Serialize()
        {
            return new DataPointMessage 
            { 
                Name = Name,
                Type = "TIMER",
                Value = (_endTime - _startTime).TotalMilliseconds.ToString(),
                Samples = 1
            };
        } 
    }
}
