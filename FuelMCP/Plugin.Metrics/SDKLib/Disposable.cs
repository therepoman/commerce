﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// $$ jbilas #REFACTOR -- migrated from FuelSDK to get metrics working. Should go elsewhere

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    public abstract class Disposable : IDisposable
    {
        //private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(Disposable));

        public bool Disposed { get; private set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        public void Dispose()
        {
            if (!Disposed)
            {
                //LOG.Debug("Disposing instance of " + this.GetType().ToString());

                Disposed = true;
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        ~Disposable() 
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        protected abstract void Dispose(bool disposing);
    }
}
