﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Metrics.Models
{
    class TaskList
    {
        List<Task> m_tasks = new List<Task>();
        object m_lock = new object();
        

        public void AddTask(Task task)
        {
            // locking list to avoid race condition that leads to a null entry
            lock (m_lock)
            { 
                m_tasks.Add(task);
            }
            task.ContinueWith(RemoveTask);
        }

        public void WaitForAll()
        {
            // this was strangely intermittently failing in unit tests, so we wrapped some critical section safeties
            Task[] tasks;
            lock (m_lock)
            {
                tasks = m_tasks.ToArray();
            }

            Task.WaitAll(tasks);
        }

        private void RemoveTask(Task task)
        {
            // locking list to avoid race condition that leads to a null entry
            lock (m_lock)
            {
                m_tasks.Remove(task);
            }
        }
    }
}
