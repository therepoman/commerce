﻿using System.IO;
using System.Reflection;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.SecureStorage
{
    public class TestableAppConfig : IAppConfigPlugin
    {
        private string tmpFolderPath;
        
        public string GetUserDataDirectory()
        {
            if (tmpFolderPath != null)
            {
                return tmpFolderPath;
            }
            tmpFolderPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tmpFolderPath);
            return tmpFolderPath;
        }

        public Serilog.Events.LogEventLevel GetMinLogLevel()
        {
            return Serilog.Events.LogEventLevel.Debug;
        }

        public string GetUserDataDirectory(string v)
        {
            return Path.Combine(GetUserDataDirectory(), v);
        }

        public void WipeData()
        {
            throw new System.NotImplementedException();
        }

        public string GetSystemDataDirectory()
        {
            return GetUserDataDirectory();
        }

        public string SdkLocation { get { return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); } }

        public string FuelClientId { get { return "clientid"; } }
    }
}
