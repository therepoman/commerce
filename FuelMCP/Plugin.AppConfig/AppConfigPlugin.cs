﻿using System;
using Amazon.Fuel.Common.Contracts;
using System.IO;
using System.Reflection;
using Amazon.Fuel.Common;
using Serilog;

namespace Amazon.Fuel.Plugin.AppConfig
{
    public class AppConfigPlugin : IAppConfigPlugin
    {
        public AppConfigPlugin()
        {
        }

        public Serilog.Events.LogEventLevel GetMinLogLevel()
        {
#if DEBUG
            return Serilog.Events.LogEventLevel.Debug;
#else
            return Serilog.Events.LogEventLevel.Information;
#endif
        }
    }
}
