﻿using System;
using System.Collections.Generic;
using System.IO;
using AutoMapper;
using Fuel.SoftwareDistributionAdminService.Models;
using Ploeh.AutoFixture;

namespace Fuel.SoftwareDistributionAdminService.Tests.Utilities
{
    public class Factories
    {
        private static Fixture _instance;

        public static Fixture Fixture => _instance ?? (_instance = new Fixture());

        public static string ValidChannelId()
        {
            return Fixture.Create("channel_id");
        }

        public static string ValidUsername()
        {
            return Fixture.Create("username");
        }

        public static string ValidTuid()
        {
            return Fixture.Create("tuid");
        }

        public static string ValidOauth()
        {
            return Fixture.Create("oauth");
        }

        public static string ValidDisplayName()
        {
            return Fixture.Create("display_name");
        }

        public static string ValidLogo()
        {
            return Fixture.Create("logo");
        }

        public static string ValidEntitlement()
        {
            return Fixture.Create("valid.entitlement.id");
        }

        public static DisposableFile DisposableFile()
        {
            return new DisposableFile();
        }

        public static string ValidClientId()
        {
            return Fixture.Create("client_id");
        }

        public static string ValidRefreshToken()
        {
            return Fixture.Create("refresh_token");
        }

        public static int ValidPid()
        {
            return Fixture.Create<int>();
        }

        public static int ValidMaxThreads()
        {
            return Fixture.Create<int>();
        }

        public static IEnumerable<string> ValidScopes()
        {
            return Fixture.CreateMany<string>(2);
        }

        public static string ValidProductId()
        {
            return Fixture.Create<string>();
        }

        public static string ValidVersionId()
        {
            return Fixture.Create("version_id");
        }

        public static string ValidVersionName()
        {
            return Fixture.Create("version_name");
        }

        public static string ValidChannelName()
        {
            return Fixture.Create("channel_name");
        }

        public static string ValidProductName()
        {
            return Fixture.Create("product_name");
        }

        public static string ValidStatusMessage()
        {
            return Fixture.Create("status_message");
        }

        public static List<string> ValidFileList()
        {
            return new List<string> { Fixture.Create<string>(), Fixture.Create<string>() };
        }

        public static CreateVersionResult ValidCreateVersionResponse()
        {
            var version = new VersionDetails(ValidProductId(), ValidVersionId(), ValidVersionName(), ValidDescription(), ValidVersionStatus(), ValidStatusMessage(), ValidUrl());
            return new CreateVersionResult(version);
        }

        public static ListProductVersionsResult ValidListProductVersionsResponse(bool withLastVersion)
        {
            var lastEvaluatedVersionName = (withLastVersion ? Fixture.Create<string>() : null);
            var versionRef1 = new VersionRef(Fixture.Create<string>(), Fixture.Create<string>(),
                Fixture.Create<string>(), Fixture.Create<VersionStatus>());
            var versionRef2 = new VersionRef(Fixture.Create<string>(), Fixture.Create<string>(),
                Fixture.Create<string>(), Fixture.Create<VersionStatus>());
            var versions = new List<VersionRef>
            {
                versionRef1,
                versionRef2
            };
            return new ListProductVersionsResult(versions, lastEvaluatedVersionName);
        }

        public static ListProductChannelsResult ValidListProductChannelsResponse(bool withLastChannel)
        {
            var lastEvaluatedChannelName = (withLastChannel ? Fixture.Create<string>() : null);
            var channelRef1 = new ChannelRef(Fixture.Create<string>(), Fixture.Create<string>(),
                Fixture.Create<string>(), Fixture.Create<string>());
            var channelRef2 = new ChannelRef(Fixture.Create<string>(), Fixture.Create<string>(),
                Fixture.Create<string>(), Fixture.Create<string>());
            var channels = new List<ChannelRef>
            {
                channelRef1,
                channelRef2
            };

            return new ListProductChannelsResult(channels, lastEvaluatedChannelName);
        }

        public static ListProductsResult ValidListProductsResponse(bool withLastProduct)
        {
            var lastEvaluatedProductName = (withLastProduct ? Fixture.Create<string>() : null);
            var productRef1 = new ProductRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>());
            var productRef2 = new ProductRef(Fixture.Create<string>(), Fixture.Create<string>(), Fixture.Create<string>());
            var products = new List<ProductRef>
            {
                productRef1,
                productRef2
            };
            return new ListProductsResult(products, lastEvaluatedProductName);
        }

        public static string ValidCoralExceptionType()
        {
            return "tv.twitch.exception.Service#TestException";
        }

        public static string ValidCoralExceptionMessage()
        {
            return "test message exception";
        }
        

        public static DisposableDirectory DisposableDirectory()
        {
            return new DisposableDirectory();
        }

        public static T Copy<T>(T original, Action<T> func)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<T, T>());
            var mapper = config.CreateMapper();
            var copy = mapper.Map<T>(original);
            func(copy);
            return copy;
        }

        public static string ValidUrl()
        {
            return Fixture.Create<Uri>().ToString();
        }

        public static string ValidVersionDescription()
        {
            return Fixture.Create<string>();
        }

        public static string ValidDescription()
        {
            return Fixture.Create<string>();
        }

        public static VersionStatus ValidVersionStatus()
        {
            return Fixture.Create<VersionStatus>();
        }

        public static string ValidServiceNamespace()
        {
            return Fixture.Create<string>();
        }

        public static string ValidOperation()
        {
            return Fixture.Create<string>();
        }

        public static string ValidS3Bucket()
        {
            return Fixture.Create<string>();
        }

        public static string ValidUseragent()
        {
            return Fixture.Create<string>();
        }
    }

    public class DisposableFile : IDisposable
    {
        private readonly string _file;

        public DisposableFile()
        {
            _file = Path.GetTempFileName();
        }

        public string Get()
        {
            return _file;
        }

        public void Dispose()
        {
            try
            {
                File.Delete(_file);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }

    public class DisposableDirectory : IDisposable
    {
        public DisposableDirectory()
        {
            Dir = Path.GetTempFileName() + Factories.ValidVersionId();
            Directory.CreateDirectory(Dir);
        }

        public string Dir { get; }

        public void Dispose()
        {
            Directory.Delete(Dir, true);
        }
    }
}

