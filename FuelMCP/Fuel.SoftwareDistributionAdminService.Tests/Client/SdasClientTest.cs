﻿using System.Collections.Generic;
using Fuel.Coral.Client;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;
using Fuel.SoftwareDistributionAdminService.Tests.Utilities;
using Moq;
using Xunit;
using System.Threading;

namespace Fuel.SoftwareDistributionAdminService.Tests.Client
{
    public class SoftwareDistributionAdminServiceClientTest
    {
        private const string SdasNamespace =
            "com.amazonaws.gearbox.softwaredistribution.admin.service.model.SoftwareDistributionAdminService";
        private readonly string _serviceUrl = Factories.ValidUrl();
        private readonly string _twitchOAuthToken = Factories.ValidOauth();
        private readonly string _versionId = Factories.ValidVersionId();
        private readonly string _channelId = Factories.ValidChannelId();
        private readonly string _versionDescription = Factories.ValidDescription();
        private readonly string _channelDescription = Factories.ValidDescription();
        private readonly string _versionName = Factories.ValidVersionName();
        private readonly string _channelName = Factories.ValidChannelName();
        private readonly string _statusMessage = Factories.ValidStatusMessage();
        private readonly string _useragent = Factories.ValidUseragent();
        private readonly VersionStatus _versionStatus = Factories.ValidVersionStatus();
        private readonly string _productId = Factories.ValidProductId();
        private readonly string _productName = Factories.ValidProductName();
        private readonly string _productDescription = Factories.ValidDescription();
        private readonly Mock<CoralClientFactory> _coralClientFactory = new Mock<CoralClientFactory>();
        private readonly Mock<ICoralClient> _coralClient;
        private readonly DescribeVersionResult _describeVersionResponse;
        private readonly CreateVersionResult _createVersionResponse;
        private readonly ListProductVersionsResult _listVersionsResponse;
        private readonly ListProductChannelsResult _listChannelsResponse;
        private readonly ListProductsResult _listProductsResponse;
        private readonly string _uploadManifest = Factories.ValidUrl();
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;

        public SoftwareDistributionAdminServiceClientTest()
        {
            _coralClient = new Mock<ICoralClient>();
            _coralClientFactory.Setup(f => f.CreateTwitchAuthenticatedClient(_serviceUrl, SdasNamespace, _twitchOAuthToken, _useragent)).Returns(_coralClient.Object);
            _softwareDistributionAdminServiceClient = new SoftwareDistributionAdminServiceClient(_serviceUrl, _twitchOAuthToken, _useragent, _coralClientFactory.Object);

            var version = new VersionDetails(_productId, _versionId, _versionName, _versionDescription, _versionStatus, _statusMessage, _uploadManifest);
            _describeVersionResponse = new DescribeVersionResult(version);

            _createVersionResponse = new CreateVersionResult(version);

            var versionRef = new VersionRef(_versionId, _versionName, _versionDescription, _versionStatus);
            var versionRefs = new List<VersionRef>
            {
                versionRef
            };
            _listVersionsResponse = new ListProductVersionsResult(versionRefs, _versionName);

            var channelRef = new ChannelRef(_channelId, _channelName, _versionId, _channelDescription);
            var channelRefs = new List<ChannelRef>
            {
                channelRef
            };
            _listChannelsResponse = new ListProductChannelsResult(channelRefs, _channelName);
            
            var productRef = new ProductRef(_productId, _productName, _productDescription);
            var productRefs = new List<ProductRef>
            {
                productRef
            };
            _listProductsResponse = new ListProductsResult(productRefs, _productName);
        }

        [Fact]
        public void DescribeVersion_ShouldReturnVersionDescription_GivenValidVersionId()
        {
            _coralClient.Setup(c => c.SendRequest<DescribeVersionResult>(It.IsAny<DescribeVersionRequest>(), default(CancellationToken))).ReturnsAsync(_describeVersionResponse);
            var describeVersionRequest = new DescribeVersionRequest
            {
                versionId = _versionId,
            };
            
            var result = _softwareDistributionAdminServiceClient.DescribeVersion(describeVersionRequest).version;

            Assert.Equal(_versionDescription, result.description);
            Assert.Equal(_versionName, result.versionName);
            Assert.Equal(_versionStatus, result.status);
            Assert.Equal(_productId, result.productId);
            Assert.Equal(_versionId, result.versionId);
            Assert.Equal(_uploadManifest, result.uploadManifest);
            Assert.Equal(_statusMessage, result.statusMessage);
        }

        [Fact]
        public void CreateVersion_ShouldReturnVersionDescription_GivenValidParameters()
        {
            _coralClient.Setup(c => c.SendRequest<CreateVersionResult>(It.IsAny<CreateVersionRequest>(), default(CancellationToken))).ReturnsAsync(_createVersionResponse);
            var createVersionRequest = new CreateVersionRequest
            {
                productId = _productId,
                versionName = _versionName,
                description = _versionDescription
            };

            var result = _softwareDistributionAdminServiceClient.CreateVersion(createVersionRequest).version;

            Assert.Equal(_versionDescription, result.description);
            Assert.Equal(_versionName, result.versionName);
            Assert.Equal(_versionStatus, result.status);
            Assert.Equal(_productId, result.productId);
            Assert.Equal(_versionId, result.versionId);
            Assert.Equal(_uploadManifest, result.uploadManifest);
            Assert.Equal(_statusMessage, result.statusMessage);
        }

        [Fact]
        public void ListVersions_ShouldReturnVersionList_GivenValidProductId()
        {
            _coralClient.Setup(c => c.SendRequest<ListProductVersionsResult>(It.IsAny<ListProductVersionsRequest>(), default(CancellationToken))).ReturnsAsync(_listVersionsResponse);
            var listVersionsRequest = new ListProductVersionsRequest
            {
                productId = _productId,
                limit = 100,
            };

            var result = _softwareDistributionAdminServiceClient.ListProductVersions(listVersionsRequest);
            var versions = result.versions;
            Assert.Equal(versions.Count, 1);

            var version = versions[0];
            Assert.Equal(_versionId, version.versionId);
            Assert.Equal(_versionName, version.versionName);
            Assert.Equal(_versionDescription, version.description);
            Assert.Equal(_versionStatus, version.status);
            Assert.Equal(_versionName, result.lastEvaluatedVersionName);
        }

        [Fact]
        public void ListChannels_ShouldReturnChannelList_GivenValidProductId()
        {
            _coralClient.Setup(c => c.SendRequest<ListProductChannelsResult>(It.IsAny<ListProductChannelsRequest>(), default(CancellationToken))).ReturnsAsync(_listChannelsResponse);
            var listChannelsRequest = new ListProductChannelsRequest
            {
                productId = _productId,
                limit = 100,
            };

            var result = _softwareDistributionAdminServiceClient.ListProductChannels(listChannelsRequest);
            var channels = result.channels;
            Assert.Equal(channels.Count, 1);

            var channel = channels[0];
            Assert.Equal(_channelId, channel.channelId);
            Assert.Equal(_channelName, channel.channelName);
            Assert.Equal(_channelDescription, channel.description);
            Assert.Equal(_versionId, channel.activeVersionId);
            Assert.Equal(_channelName, result.lastEvaluatedChannelName);
        }

        [Fact]
        public void ListProducts_ShouldReturnProductList()
        {
            _coralClient.Setup(c => c.SendRequest<ListProductsResult>(It.IsAny<ListProductsRequest>(), default(CancellationToken))).ReturnsAsync(_listProductsResponse);
            var listProductsRequest = new ListProductsRequest
            {
                limit = 100,
            };

            var result = _softwareDistributionAdminServiceClient.ListProducts(listProductsRequest);
            var products = result.products;
            Assert.Equal(products.Count, 1);

            var product = products[0];
            Assert.Equal(_productId, product.productId);
            Assert.Equal(_productName, product.productName);
            Assert.Equal(_productDescription, product.description);
            Assert.Equal(_productName, result.lastEvaluatedProductName);
        }
    }
}
