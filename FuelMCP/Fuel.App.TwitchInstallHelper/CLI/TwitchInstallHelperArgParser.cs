﻿using System;
using System.Linq;
using Amazon.Fuel.Common.CommandLineParser;
using Serilog;

namespace Amazon.Fuel.App.TwitchInstallHelper
{
    public class TwitchInstallHelperArgParser
    {
        public TwitchInstallHelperArg Parse(string[] args)
        {
            Log.Information($"TwitchInstallHelperArgParser.Parse()");
            var result = new TwitchInstallHelperArg();

            if (args.Length == 0)
            {
                throw new InvalidCLIException($"Empty args");
            }

            var parser = new CommandLine.Parser();
            if (!parser.ParseArguments(args.ToArray(), result))
            {
                throw new InvalidCLIException($"Unable to parse command line arguments");
            }

            return result;
        }
    }

    public class InvalidCLIException : Exception
    {
        public string Message { get; }

        public InvalidCLIException(string message)
        {
            Message = message;
        }
    }
}
