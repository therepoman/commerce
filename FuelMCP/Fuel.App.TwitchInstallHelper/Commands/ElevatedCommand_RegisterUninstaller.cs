using System;
using System.IO;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Serilog;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Messages;
using System.Threading.Tasks;

namespace Amazon.Fuel.App.TwitchInstallHelper.Commands
{
    public class ElevatedCommand_RegisterUninstaller : IElevatedIpcCommand<InstallHelperIpcMessage>
    {
        public int TimeoutMS { get; set; } = 1000 * 60 * 5; // 5 min

        private readonly IRegisterInstallation _registerInstallation;
        private readonly IEntitlementsDatabase _entitlementDb;
        private readonly IGameInstallInfoDatabase _gameInstallInfoDb;
        private readonly ILaunchConfigProvider _launchConfigProvider;

        private readonly ProductId _adgProductId;

        public ElevatedCommand_RegisterUninstaller(
            ProductId adgProdId,
            IRegisterInstallation registerInstallation, 
            IEntitlementsDatabase entitlementDb,
            IGameInstallInfoDatabase gameInstallInfoDb,
            ILaunchConfigProvider launchConfigProvider)
        {
            _adgProductId = adgProdId;

            _registerInstallation = registerInstallation;
            _entitlementDb = entitlementDb;
            _gameInstallInfoDb = gameInstallInfoDb;
            _launchConfigProvider = launchConfigProvider;
        }

        public async Task<InstallHelperIpcMessage> Execute()
        {
            try
            {
                return await ExecuteCanThrow();
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Unexpected exception encountered");
                return new IpcRegisterUninstallerComplete(this,
                    IpcRegisterUninstallerComplete.EResult.Failure_UnknownException);
            }
        }

        private Task<InstallHelperIpcMessage> ExecuteCanThrow()
        {
            try
            {
                GameProductInfo entitlement = null;
                try
                {
                    entitlement = _entitlementDb.Get(_adgProductId);
                    if (entitlement == null)
                    {
                        throw new Exception("No entitlement in DB");
                    }
                }
                catch (Exception innerEx)
                {
                    FuelLog.Error(innerEx, "Failed to find entitlement in DB for product", new { _adgProductId });
                    return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Failure_NoEntitlement));
                }

                GameInstallInfo installInfo = null;
                try
                {
                    installInfo = _gameInstallInfoDb.TryFindById(_adgProductId.Id);
                    if (installInfo == null)
                    {
                        throw new Exception("No installInfo in DB");
                    }
                }
                catch (Exception innerEx)
                {
                    FuelLog.Error(innerEx, "Failed to find installInfo in DB for product", new { _adgProductId, entitlementId = entitlement?.Id });
                    return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Failure_NoInstallInfo));
                }

                var installDir = installInfo.InstallDirectory;

                LaunchConfig cfg = null;
                try
                {
                    cfg = _launchConfigProvider.GetLaunchConfig(installDir);
                    if (cfg == null)
                    {
                        FuelLog.Error("Couldn't find Fuel config in dir", new { installDir });
                        return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Failure_NoConfigFound));
                    }
                }
                catch (Exception innerEx)
                {
                    FuelLog.Error(innerEx, "No valid config on disk", new { _adgProductId, entitlementId = entitlement?.Id, installDir });
                    return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Failure_NoConfigFound));
                }

                var iconLocation = Path.Combine(installDir, cfg.Main.Command);

                try
                {
                    var success = _registerInstallation.RegisterUninstaller(
                        productId: _adgProductId,
                        productInfo: entitlement,
                        installInfo: installInfo,
                        estimatedSize: FileUtilities.CalcDirectorySize(installDir),
                        iconLocation: iconLocation);

                    if (!success)
                    {
                        throw new Exception("Uninstaller registration failed");
                    }
                }
                catch (Exception innerEx)
                {
                    FuelLog.Error(innerEx, "Failed to register uninstaller in registry for product", new { _adgProductId, entitlementId = entitlement?.Id, installDir });
                    return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Failure_RegistryRegistrationFailed));
                }

                return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Succeeded));
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Exception registering uninstaller");
                return Task.FromResult<InstallHelperIpcMessage>(new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Failure_Unknown));
            }
        }

        public InstallHelperIpcMessage TimeoutMessage => new IpcRegisterUninstallerComplete(this, IpcRegisterUninstallerComplete.EResult.Timeout);

        public void Abort()
        {
        }
    }
}