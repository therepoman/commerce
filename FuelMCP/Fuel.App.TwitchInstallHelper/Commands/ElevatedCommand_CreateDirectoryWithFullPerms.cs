using System;
using System.IO;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Serilog;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Messages;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.App.TwitchInstallHelper.Commands
{
    // This command creates the primary games library directory and ensure that all user permissions are granted.
    public class ElevatedCommand_CreatePrimaryDirectoryWithFullPerms : IElevatedIpcCommand<InstallHelperIpcMessage>
    {
        public int TimeoutMS { get; set; } = 1000 * 60 * 5; // 5 min

        private IOSDriver _osDriver;
        private IFileSystemUtilities _utilities;

        public ElevatedCommand_CreatePrimaryDirectoryWithFullPerms(IOSDriver driver, IFileSystemUtilities utilities)
        {
            _osDriver = driver;
            _utilities = utilities;
        }

        public async Task<InstallHelperIpcMessage> Execute()
        {
            try
            {
                return await ExecuteCanThrow();
            }
            catch ( Exception ex )
            {
                FuelLog.Error(ex, "Unexpected exception encountered");
                return new IpcCreateGamesLibraryPrimaryPathComplete(this, 
                    IpcCreateGamesLibraryPrimaryPathComplete.EResult.Failure_UnknownException);
            }
        }

        private Task<InstallHelperIpcMessage> ExecuteCanThrow()
        {
            var result = IpcCreateGamesLibraryPrimaryPathComplete.EResult.Succeeded;
            try
            {
                var dir = _osDriver.PrimaryLibraryRoot;
                _osDriver.CreateDirectoryWithAllUsersPermissions(dir, setPermissionsOnTopmostNewDirectory: false);
            }
            catch (UnauthorizedAccessException e)
            {
                Log.Error(e, "Access denied creating dir");
                result = IpcCreateGamesLibraryPrimaryPathComplete.EResult.Failure_NoAccess;
            }
            catch (Exception e)
            {
                Log.Error(e, "Exception creating dir");
                result = IpcCreateGamesLibraryPrimaryPathComplete.EResult.Failure_UnknownException;
            }

            return Task.FromResult<InstallHelperIpcMessage>(new IpcCreateGamesLibraryPrimaryPathComplete(this, result));
        }

        public InstallHelperIpcMessage TimeoutMessage => new IpcCreateGamesLibraryPrimaryPathComplete(this, IpcCreateGamesLibraryPrimaryPathComplete.EResult.Timeout);

        public void Abort()
        {
        }
    }
}