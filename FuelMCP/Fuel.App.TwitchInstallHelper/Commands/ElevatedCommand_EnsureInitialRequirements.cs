using System;
using System.IO;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Serilog;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Messages;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.App.TwitchInstallHelper.Commands
{
    // This command creates the primary games library directory and ensure that all user permissions are granted.
    public class ElevatedCommand_EnsureInitialRequirements : IElevatedIpcCommand<InstallHelperIpcMessage>
    {
        public int TimeoutMS { get; set; } = 1000 * 60 * 5; // 5 min

        private readonly IOSDriver _osDriver;

        public ElevatedCommand_EnsureInitialRequirements(IOSDriver driver)
        {
            _osDriver = driver;
        }

        public async Task<InstallHelperIpcMessage> Execute()
        {
            try
            {
                return await ExecuteCanThrow();
            }
            catch (UnauthorizedAccessException e)
            {
                FuelLog.Error(e, "Access denied setting permissions");
                return new IpcEnsureInitialRequirementsComplete(this,
                    IpcEnsureInitialRequirementsComplete.EResult.Failure_NoAccess);
            }
            catch ( Exception ex )
            {
                FuelLog.Error(ex, "Unexpected exception encountered");
                return new IpcEnsureInitialRequirementsComplete(this,
                    IpcEnsureInitialRequirementsComplete.EResult.Failure_UnknownException);
            }
        }

        private Task<InstallHelperIpcMessage> ExecuteCanThrow()
        {
            // we need the system data path to be all user access for the installinfo db
            var commonDbDir = _osDriver.SystemDataPathFull;

            if (!Directory.Exists(commonDbDir))
            {
                FuelLog.Info("Creating new directory", new {dir=commonDbDir});
                Directory.CreateDirectory(commonDbDir);
            }

            FuelLog.Info("Setting dir permissions", new { dir=commonDbDir });
            _osDriver.EnsureDirectoryHasAllUsersPermissions(commonDbDir);

            // go one level deep, just in case the permissions lost inheritance
            foreach (var subdir in Directory.GetDirectories(commonDbDir))
            {
                FuelLog.Info("Setting dir permissions", new { dir = subdir });
                _osDriver.EnsureDirectoryHasAllUsersPermissions(subdir);

                foreach (var file in Directory.GetFiles(subdir))
                {
                    FuelLog.Info("Setting file permissions", new { file });
                    _osDriver.EnsureFileHasAllUsersPermissions(file);
                }
            }

            return Task.FromResult<InstallHelperIpcMessage>(new IpcEnsureInitialRequirementsComplete(this, IpcEnsureInitialRequirementsComplete.EResult.Succeeded));
        }

        public InstallHelperIpcMessage TimeoutMessage => new IpcEnsureInitialRequirementsComplete(this, IpcEnsureInitialRequirementsComplete.EResult.Timeout);

        public void Abort()
        {
        }
    }
}