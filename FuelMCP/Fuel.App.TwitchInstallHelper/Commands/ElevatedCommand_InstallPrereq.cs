using System;
using System.IO;
using System.Threading;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Serilog;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common.Messages;
using Tv.Twitch.Fuel.Manifest;
using Sds = Tv.Twitch.Fuel.Sds;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Amazon.Fuel.Common.Extensions;

namespace Amazon.Fuel.App.TwitchInstallHelper.Commands
{
    // This command takes in a manifest file, validates cert, checks shas for fuel config and prereq exes, and runs them.
    public class ElevatedCommand_InstallPrereq : IElevatedIpcCommand<InstallHelperIpcMessage>
    {
        private readonly CancellationTokenSource _abort = new CancellationTokenSource();
        private string _manifestFile;
        private string _installDir;
        private ILaunchConfigProvider _configProvider;
        private IGamePrereqInstaller _prereqInstaller;
        private IFileSystemUtilities _fsUtil;
        private IManifestDeserializer _manifestDeserializer;

        public int TimeoutMS { get; set; } = 1000 * 60 * 60; // 1 hr

        public ElevatedCommand_InstallPrereq(
            string manifestFile, 
            string installDir, 
            ILaunchConfigProvider configProvider, 
            IFileSystemUtilities fsUtil, 
            IManifestDeserializer manifestSerializer,
            IGamePrereqInstaller prereqInstaller)
        {
            _configProvider = configProvider;
            _manifestFile = manifestFile;
            _installDir = installDir;
            _fsUtil = fsUtil;
            _manifestDeserializer = manifestSerializer;
            _prereqInstaller = prereqInstaller;
        }

        public async Task<InstallHelperIpcMessage> Execute()
        {
            // If manifest does not exists
            if (!_fsUtil.FileExists(_manifestFile))
            {
                Log.Error($"Manifest does not exist at {_manifestFile}");
                return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_MissingManifest);
            }

            // If install fuel.json and install dir do not exist
            var fuelJsonFullPath = Path.Combine(_installDir, Constants.Paths.FuelGameConfigFileName);
            if (!_fsUtil.FileExists(fuelJsonFullPath))
            {
                Log.Error($"Launch config file does not exist at {fuelJsonFullPath}");
                return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_MissingConfigFile);
            }

            // Read manifest, build path to hash map
            Sds.Manifest manifest;
            using (var stream = new FileStream(_manifestFile, FileMode.Open))
            {
                try
                {
                    manifest = await _manifestDeserializer.Deserialize(stream);
                }
                catch (BadManifestSignature)
                {
                    Log.Error($"Signature verification for manifest {_manifestFile} failed!");
                    return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_ManifestSignatureFailed);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Failed to deserialize manifest");
                    return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_Unknown);
                }
            }

            IDictionary<string, Sds.File> manifestFilesByPath = manifest.Packages[0].Files.ToDictionary(f => Path.GetFullPath(Path.Combine(_installDir, f.Path)), StringComparer.OrdinalIgnoreCase);

            LaunchConfig config = null;
            // Lock fuel.json
            using (var fuelJsonLock = new FileStream(fuelJsonFullPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                // Verify fuel.json.
                var hashAlgorithm = manifestFilesByPath[fuelJsonFullPath].Hash.Algorithm;
                if (hashAlgorithm != Sds.HashAlgorithm.Sha256)
                {
                    // This shouldn't happpen (we shouldn't serve manifests the client doesn't support) but just incase...
                    Log.Error($"Unsupported hash functionn {hashAlgorithm}");
                    return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_Unknown);
                }
                var fuelJsonExpectedHash = manifestFilesByPath[fuelJsonFullPath].Hash.Value.ToByteArray();
                var fuelJsonActualHash = ShaUtil.ComputeHashBytes(fuelJsonFullPath);

                if (!Enumerable.SequenceEqual(fuelJsonActualHash, fuelJsonExpectedHash))
                {
                    Log.Error($"Encountered corrupt launch config file {fuelJsonFullPath}");
                    return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_FileCorrupt);
                }

                // Parse fuel.json
                try
                {
                    config = _configProvider.GetLaunchConfig(_installDir, Constants.Paths.FuelGameConfigFileName);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Invalid launch config file");
                    return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_InvalidConfigFile);
                }
            }

            if (_abort.Token.IsCancellationRequested)
            {
                return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Cancelled);
            }

            bool installSuccess = false;
            Queue<FileStream> locks = new Queue<FileStream>();

            try
            {
                var filteredPrereqs = config.PostInstall.Select(e =>
                    $"{e.Command} {string.Join(" ", e.Args)} AlwaysRun: {e.AlwaysRun}");

                FuelLog.Info("Filtering prereqs", new { filteredPrereqsSummary = string.Join(",", filteredPrereqs) });

                // Filter already installed prereqs
                Func<Executable, string> getShaFromExe = exe =>
                {
                    var filePath = Path.GetFullPath(Path.Combine(_installDir, exe.Command));
                    return manifestFilesByPath[filePath].Hash.HexString();
                };

                var prereqs = config.PostInstall.Select(exe => new PrereqExecutableWithSha() { Exe = exe, ExpectedSha = getShaFromExe(exe) })
                                                .ToList();

                prereqs = _prereqInstaller.FilterPrereqs(prereqs, _installDir);

                var newFilteredPrereqs = prereqs.Select(e =>
                    $"{e.Exe.Command} {string.Join(" ", e.Exe.Args)} AlwaysRun: {e.Exe.AlwaysRun}");

                FuelLog.Info("Validating remaining prereqs", new { newFilteredPrereqsSummary = string.Join(",", newFilteredPrereqs) });

                // Hash verify check prereqs
                foreach (var prereq in prereqs)
                {
                    if (string.IsNullOrEmpty(prereq.Exe.Command))
                    {
                        continue;
                    }

                    var fullPath = Path.GetFullPath(Path.Combine(_installDir, prereq.Exe.Command));

                    // Lock pre-req
                    locks.Enqueue(new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read));
                    // Verify the prereq's hash
                    var hashAlgorithm = manifestFilesByPath[fullPath].Hash.Algorithm;
                    if (hashAlgorithm != Sds.HashAlgorithm.Sha256)
                    {
                        // This shouldn't happpen (we shouldn't serve manifests the client doesn't support) but just incase...
                        Log.Error($"Unsupported hash function {hashAlgorithm}");
                        return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_Unknown);
                    }
                    var expectedHash = manifestFilesByPath[fullPath].Hash.Value.ToByteArray();
                    var actualHash = ShaUtil.ComputeHashBytes(fullPath);
                    if (!Enumerable.SequenceEqual(actualHash, expectedHash))
                    {
                        Log.Error($"Encountered corrupt post-install command {fullPath}");
                        return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Failure_FileCorrupt);
                    }
                }

                // Install prereqs
                installSuccess = _prereqInstaller.TryInstallPrereqs(prereqs, _installDir, _abort.Token);
            }
            finally
            {
                // Release prereqs
                while (locks.Count > 0)
                {
                    locks.Dequeue().Dispose();
                }
            }

            if (_abort.Token.IsCancellationRequested)
            {
                return new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Cancelled);
            }

            return new IpcInstallPrereqsComplete(this, installSuccess ? IpcInstallPrereqsComplete.EResult.Succeeded
                                                                      : IpcInstallPrereqsComplete.EResult.Failure_FailedToInstall);
        }

        public InstallHelperIpcMessage TimeoutMessage => new IpcInstallPrereqsComplete(this, IpcInstallPrereqsComplete.EResult.Timeout);

        public void Abort()
        {
            _abort.Cancel();
        }
    }
}