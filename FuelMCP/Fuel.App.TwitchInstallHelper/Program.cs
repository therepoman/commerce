﻿using System;
using System.Diagnostics;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.CommandLineParser;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Logger;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Clients.Twitch.Spade.Client;
using Amazon.Fuel.Plugin.Metrics;
using Autofac;
using Amazon.Fuel.AppCore;
using Amazon.Fuel.Configurations.Configuration;
using Serilog;
using Amazon.Fuel.App.TwitchInstallHelper.IPC;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common.Implementations;

namespace Amazon.Fuel.App.TwitchInstallHelper
{
    class Program
    {
        private static void Main(string[] args)
        {
            #region Parse Arguments

            TwitchInstallHelperArg launchArgs = null;
            try
            {
                // added safties, as logging is not yet enabled
                try
                {
                    launchArgs = new TwitchInstallHelperArgParser().Parse(args);
                }
                catch (InvalidCLIException)
                {
                    Environment.Exit((int)ETwitchInstallHelperResult.Failure_InvalidArgumentsPassed);
                }

            }
            catch (Exception)
            {
                // can't log yet, keep going. Note that this is exceptional behavior, normal arg parses should 
                // already have gracefully quit
                launchArgs = null;
            }

#if DEBUG
            if (launchArgs.AttachDebugger)
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
                else
                {
                    Debugger.Launch();
                }
            }
#endif

            #endregion

            ISpadeClient spadeClient = new NullSpadeClient();
            IMetricsPlugin metrics = null;
            if (launchArgs?.DevMode ?? false)
            {
                metrics = new NullMetrics();
            }
            else
            {
                metrics = new MetricsPlugin(Constants.AmazonMetricsId, DateTime.UtcNow, spadeClient);
            }

            if (launchArgs == null)
            {
                metrics.AddCounter("TwitchInstallHelper", "ExceptionParsingArgs");
                Environment.Exit((int)ETwitchInstallHelperResult.Failure_InvalidArgumentsPassed);
            }

            #region Setup logging

            UncaughtExceptionHandler crash = null;
            ILoggerBaseConfig loggerBaseConfig;
            var osDriver = new WindowsFileSystemDriver()
            {
                IsDevMode = launchArgs.DevMode,
                ApplicationDataOverride = launchArgs.AppDataLocationOverride,
            };

            try
            {
                // Initialize curse logger
                Curse.Logging.LoggerConfig loggerConfig = new Curse.Logging.LoggerConfig(dir: osDriver.CurseLogDirectory, name: "Fuel");
                Curse.Logging.Logger.Init(loggerConfig);
                Curse.Logging.LogCategory curseLogger = new Curse.Logging.LogCategory("TwitchInstallHelper");

                // TODO : prefix logs
                //string prefix = "install-";
                //if (launchArgs.DoMigration || launchArgs.DoMigrationPreview)
                //{
                //    prefix = "migration-";
                //}
                //else if (launchArgs.InstallPreReqsConfigFile != null)
                //{
                //    prefix = "installprereqs-";
                //}

                loggerBaseConfig = new LoggerBaseConfig(
                    osDriver, 
                    curseLogger,
                    loggerPrefix: "",//prefix, 
                    devMode: osDriver.IsDevMode);

                crash = new UncaughtExceptionHandler(
                    metrics: metrics,
                    attachExceptionTrackers: true,
                    uploadMetricsAndData: !osDriver.IsDevMode
                );
            }
            catch (Exception)
            {
                metrics.AddCounter("TwitchInstallHelper", "ExceptionInitLogger");
                Environment.Exit((int)ETwitchInstallHelperResult.Failure_UnknownException);
                return;
            }

            Log.Information("Application started with arguments", new { ProcessName = Process.GetCurrentProcess()?.ProcessName, Args = string.Join(",", args) });

            #endregion

            if ( string.IsNullOrEmpty(launchArgs.IpcPipeName) )
            {
                FuelLog.Fatal("Invalid pipe name", new { launchArgs.IpcPipeName } );
                Environment.Exit((int)ETwitchInstallHelperResult.Failure_InvalidArgumentsPassed);
            }

            var listener = new InstallHelperIpc(
                launchArgs.IpcPipeName, 
                crash, 
                metrics, 
                osDriver, 
                new FileSystemUtilities(), 
                new MessengerHub());

            var result = listener.StartNew();

            switch (result)
            {
                case InstallHelperIpc.EInstallHelperIpcResult.Failure_Init:
                    Environment.Exit((int)ETwitchInstallHelperResult.Failure_IpcInit);
                    break;
                case InstallHelperIpc.EInstallHelperIpcResult.Failure_Read:
                    Environment.Exit((int)ETwitchInstallHelperResult.Failure_IpcRead);
                    break;
                case InstallHelperIpc.EInstallHelperIpcResult.Failure_Unknown:
                    Environment.Exit((int)ETwitchInstallHelperResult.Failure_UnknownException);
                    break;
            }

            Environment.Exit((int)ETwitchInstallHelperResult.Succeeded);
        }
    }
}
