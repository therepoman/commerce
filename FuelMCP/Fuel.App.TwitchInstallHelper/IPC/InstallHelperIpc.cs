﻿using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.IPC;
using Amazon.Fuel.Common.Utilities;
using Autofac;
using Curse.Tools.IPC;
using Amazon.Fuel.App.TwitchInstallHelper.Commands;
using Amazon.Fuel.AppCore;
using Amazon.Fuel.Configurations.Configuration;
using Polly;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Amazon.Fuel.Common.Messages;
using TinyMessenger;
using System.Threading.Tasks;
using Tv.Twitch.Fuel.Manifest;

namespace Amazon.Fuel.App.TwitchInstallHelper.IPC
{
    public class PipeClosedIpcMessage : TinyMessageBase
    {
        public PipeClosedIpcMessage(object sender) : base(sender) { }
    }

    public class HadBadDataIpcMessage : TinyMessageBase
    {
        public HadBadDataIpcMessage(object sender) : base(sender) { }
    }

    // This handles all IPC from the helper exe to the radium app controller.
    public class InstallHelperIpc
    {
        public const int WAIT_TIMEOUT_MS = 15000;
        public readonly double[] IPC_READ_RETRY_WAIT_SECS = 
            new double[] { 1.0, 2.5, 7.0 }; // abandon after last wait

        // force termination after this many secs when IPC termination message received
        public readonly int FORCE_TERMINATE_AFTER_MS = 60000;

        private readonly string _pipeName;
        private readonly UncaughtExceptionHandler _crash;
        private readonly IMetricsPlugin _metrics;
        private readonly IOSDriver _osDriver;
        private readonly IFileSystemUtilities _fsUtil;
        private readonly IMessengerHub _hub;
        private CancellationTokenSource _cancelTokenSource;
        private bool _terminateMessageReceived = false;
        private Thread _listenThread = null;

        private ConcurrentQueue<IpcMessage> _incomingMessageQueue = new ConcurrentQueue<IpcMessage>();

        public enum EInstallHelperIpcResult
        {
            Success,
            Cancelled,

            Failure_Init,
            Failure_Read,
            Failure_Unknown,
            Failure_HungProcess
        }

        public InstallHelperIpc(
            string pipeName, 
            UncaughtExceptionHandler crash, 
            IMetricsPlugin metrics,
            IOSDriver osDriver,
            IFileSystemUtilities fsUtil,
            IMessengerHub hub)
        {
            _pipeName = pipeName;
            _crash = crash;
            _metrics = metrics;
            _osDriver = osDriver;
            _fsUtil = fsUtil;
            _hub = hub;

            _cancelTokenSource = new CancellationTokenSource();
        }

        public void ReceiveLoop(BufferedIPC ipcClient)
        {
            FuelLog.Info("Start IPC client listener", new { pipe = _pipeName });

            // TODO : heartbeat + termination timeout? 
            while (!_terminateMessageReceived)
            {
                byte[] responseBytes = null;
                try
                {
                    Policy
                    .Handle<Exception>()
                    .WaitAndRetry(IPC_READ_RETRY_WAIT_SECS.Select(TimeSpan.FromSeconds),
                        (ex, retry, count, cx) =>
                        {
                            FuelLog.Error(ex, $"Couldn't read from IPC pipe, retrying in {retry.TotalSeconds} ({count}/{IPC_READ_RETRY_WAIT_SECS.Length})");
                        });
                    {
                        if (_cancelTokenSource.IsCancellationRequested || _terminateMessageReceived)
                        {
                            return;
                        }

                        responseBytes = ipcClient.Read();
                    }
                }
                catch (Exception ex)
                {
                    FuelLog.Fatal(ex, $"Fatal: Couldn't read from IPC pipe");
                    _hub.Publish(new PipeClosedIpcMessage(this)); 
                    return;
                }

                try
                {
                    FuelLog.Info("Received message", new { size = responseBytes.Length });
                    IpcMessage requestMsg = IpcMessage.Decode(newSender: this, bytes: responseBytes);
                    _incomingMessageQueue.Enqueue(requestMsg);

                    // special case : termination should occur forcably after a timeout
                    if (requestMsg is IpcTerminate)
                    {
                        FuelLog.Info("Received termination request");
                        _terminateMessageReceived = true;
                        StartForceTerminationClock();
                    }

                }
                catch (IpcMessageDecodeException ex)
                {
                    FuelLog.Fatal(ex, "TwitchApp sent bad data, terminating");

                    _hub.Publish(new HadBadDataIpcMessage(this));
                }
                catch (Exception ex)
                {
                    FuelLog.Error(ex, "Error reading reponse bytes");
                }
            }
        }

        public EInstallHelperIpcResult DecodeMessagesLoop(BufferedIPC ipcClient)
        {
            bool hadBadData = false, pipeClosed = false;
            using (new ScopedTinyMsgListener<PipeClosedIpcMessage>(_hub, m => { pipeClosed = true; }))
            using (new ScopedTinyMsgListener<HadBadDataIpcMessage>(_hub, m => { hadBadData = true; }))
            {
                while (true)
                {
                    IpcMessage requestMsg = null;
                    if (!_incomingMessageQueue.TryDequeue(out requestMsg))
                    {
                        if (!_listenThread.IsAlive)
                        {
                            return EInstallHelperIpcResult.Cancelled;
                        }

                        if (_cancelTokenSource.IsCancellationRequested)
                        {
                            return EInstallHelperIpcResult.Cancelled;
                        }

                        Thread.Sleep(100);
                        continue;
                    }

                    if (_cancelTokenSource.IsCancellationRequested)
                    {
                        return EInstallHelperIpcResult.Cancelled;
                    }

                    if (hadBadData || pipeClosed)
                    {
                        // TODO send TwitchApp notification?
                        // for now, just terminate our app (otherwise other end of the pipe will wait indefinitely)
                        // TwitchApp will try to restart the helper and ultimately fail if the data is consistently bad
                        FuelLog.Error("Pipe unexpectedly closed or had bad data", new { hadBadData, pipeClosed });
                        Environment.Exit((int)EInstallUpdateProductResult.IpcFailure);
                    }

                    FuelLog.Info("Decoded message, publishing", new { type = requestMsg?.GetType() });
                    _hub.Publish(requestMsg);

                    InstallHelperIpcMessage response = null;
                    bool shouldTerminateAfterSend = false;
                    try
                    {
                        FuelLog.Info("Handling message", new { type = requestMsg.GetType() });
                        response = HandleMessage(requestMsg, out shouldTerminateAfterSend);
                    }
                    catch (Exception ex)
                    {
                        FuelLog.Error(ex, "Error handling response message, moving to next");
                        continue;
                    }

                    if (response != null)
                    {
                        FuelLog.Info("Writing response", new { type = response.GetType() });
                        ipcClient.Write(response.GetBytes());
                    }

                    if (shouldTerminateAfterSend)
                    {
                        FuelLog.Info("Termination message received");
                        return EInstallHelperIpcResult.Success;
                    }
                }
            }
        }

        // Starts a countdown to terminate the app after a fixed amount of time (prevent hanging helper app)
        public void StartForceTerminationClock()
        {
            // termination watcher
            Task.Run(() =>
            {
                FuelLog.Info("Termination clock begun", new { terminateInMs = FORCE_TERMINATE_AFTER_MS });

                var sw = new Stopwatch();
                sw.Start();
                while (sw.Elapsed.TotalMilliseconds < FORCE_TERMINATE_AFTER_MS)
                {
                    Thread.Sleep(100);
                }

                FuelLog.Error("Forced termination of stuck process (some in-process tasks may not have completed)", 
                    new { FORCE_TERMINATE_AFTER_MS });
                _metrics?.AddCounter("InstallHelperIpc", "ForcedTermination");
                Environment.Exit((int)EInstallHelperIpcResult.Failure_HungProcess);
            });
        }

        public EInstallHelperIpcResult StartNew()
        {
            try
            { 
                FuelLog.Info("Creating IPC client", new { pipe = _pipeName });
                using (BufferedIPC ipcClient = new BufferedIPC(IPCType.Client, _pipeName))
                {
                    bool connected = false;
                    try
                    {
                        FuelLog.Info("Connecting IPC client", new { pipe = _pipeName });
                        connected = ipcClient.Wait(timeout: WAIT_TIMEOUT_MS);
                    }
                    catch (Exception ex)
                    {
                        FuelLog.Fatal(ex, $"Fatal: Couldn't initialize IPC pipe");
                        return EInstallHelperIpcResult.Failure_Init;
                    }

                    if (!connected)
                    {
                        FuelLog.Fatal($"Fatal: Couldn't initialize IPC pipe");
                        return EInstallHelperIpcResult.Failure_Init;
                    }

                    _listenThread = new Thread(() => ReceiveLoop(ipcClient))
                    {
                        IsBackground = true,
                        Name = $"TwitchAppIpc"
                    };

                    _listenThread.Start();

                    return DecodeMessagesLoop(ipcClient);
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "IPC exception");
            }

            FuelLog.Info("ControllerProcessIpc exiting");
            return EInstallHelperIpcResult.Success;
        }

        private InstallHelperIpcMessage HandleMessage(IpcMessage requestMsg, out bool outShouldTerminateAfterSend)
        {
            outShouldTerminateAfterSend = false;
            InstallHelperIpcMessage response = null;

            if (requestMsg is IpcEnsureInitialRequirements)
            {
                response = ElevatedIpcCommandRunner<InstallHelperIpcMessage>.ExecuteIpc(
                    new ElevatedCommand_EnsureInitialRequirements(_osDriver));
            }
            else if (requestMsg is IpcCreateGamesLibraryPrimaryPath)
            {
                response = ElevatedIpcCommandRunner<InstallHelperIpcMessage>.ExecuteIpc(
                    new ElevatedCommand_CreatePrimaryDirectoryWithFullPerms(_osDriver, _fsUtil));
            }
            else if (requestMsg is IpcInstallPrereqs)
            {
                response = ElevatedIpcCommandRunner<InstallHelperIpcMessage>.ExecuteIpc(
                    BuildInstallPrereqsCommand(((IpcInstallPrereqs)requestMsg).ManifestFile,
                    ((IpcInstallPrereqs)requestMsg).InstallDir));
            }
            else if (requestMsg is IpcRegisterUninstaller)
            {
                response = ElevatedIpcCommandRunner<InstallHelperIpcMessage>.ExecuteIpc(
                    BuildRegisterUninstallerCommand(
                        new ProductId(((IpcRegisterUninstaller)requestMsg).ProductId)));
            }
            else if (requestMsg is IpcTerminate)
            {
                response = new IpcTerminateAck(this);
                FuelLog.Info("Terminate request received, terminating");
                _cancelTokenSource.Cancel();
                outShouldTerminateAfterSend = true;
            }
            else
            {
                FuelLog.Error("Unknown message type received", new { requestMsg.ReflectedType });
            }

            return response;
        }

        private IElevatedIpcCommand<InstallHelperIpcMessage> BuildInstallPrereqsCommand(string manifestFile, string installDir)
        {
            var builder = new ContainerBuilder();
            new BaseContextServices().Build(builder);
            new BaseContext(_crash, _metrics).Build(builder);
            new CoreContext().Build(builder);
            new PersistenceContext().Build(builder);
            new WindowsContext().Build(builder, includeLaunchUnelevatedService: true, devMode: _osDriver.IsDevMode);
            new StorageContext().Build(builder);
            new ClientsContext().Build(builder, Constants.TwitchAPI, Constants.ClientId, Constants.ClientTimeoutRetryDelaySecs);
            var container = builder.Build();

            return new ElevatedCommand_InstallPrereq(
                manifestFile,
                installDir,
                container.Resolve<ILaunchConfigProvider>(),
                container.Resolve<IFileSystemUtilities>(),
                new ManifestSerialization(),
                container.Resolve<IGamePrereqInstaller>());
        }

        private IElevatedIpcCommand<InstallHelperIpcMessage> BuildRegisterUninstallerCommand(ProductId adgProdId)
        {
            var builder = new ContainerBuilder();
            new BaseContextServices().Build(builder);
            new BaseContext(_crash, _metrics).Build(builder);
            new CoreContext().Build(builder);
            new PersistenceContext().Build(builder);
            new WindowsContext().Build(builder, includeLaunchUnelevatedService: false, devMode: _osDriver.IsDevMode, appDataOverride: _osDriver.ApplicationDataOverride);
            new StorageContext().Build(builder);
            new ClientsContext().Build(builder, Constants.TwitchAPI, Constants.ClientId, Constants.ClientTimeoutRetryDelaySecs);

            var container = builder.Build();

            return new ElevatedCommand_RegisterUninstaller(
                    adgProdId,
                    container.Resolve<IRegisterInstallation>(),
                    container.Resolve<IEntitlementsDatabase>(),
                    container.Resolve<IGameInstallInfoDatabase>(),
                    container.Resolve<ILaunchConfigProvider>());
        }
    }
}
