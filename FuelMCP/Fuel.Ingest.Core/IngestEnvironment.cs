﻿using System;

namespace Fuel.Ingest.Core
{
    /// <summary>
    /// Allows access to the system properties the ingestion client is running in.
    /// </summary>
    public class IngestEnvironment
    {
        public virtual String GetEnvironmentVariable(string key)
        {
            return System.Environment.GetEnvironmentVariable(key);
        }
    }
}
