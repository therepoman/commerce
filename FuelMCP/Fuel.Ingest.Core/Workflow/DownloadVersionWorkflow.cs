﻿using Fuel.Ingest.Core.Command;
using Fuel.Installer;
using Fuel.SoftwareDistributionService.Client;
using Fuel.SoftwareDistributionService.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Tv.Twitch.Fuel.Manifest;
using Tv.Twitch.Fuel.Sds;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IDownloadVersionWorkflow
    {
        bool DownloadVersion(DownloadVersionArgs args);
    }

    public class DownloadVersionWorkflow : IDownloadVersionWorkflow
    {
        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly ManifestSerialization _manifestSerializer = new ManifestSerialization();

        public DownloadVersionWorkflow(ISoftwareDistributionServiceClient sds)
        {
            _sds = sds;
        }

        public bool DownloadVersion(DownloadVersionArgs args)
        {
            var getVersionsResponse = _sds.GetVersions(new GetVersionsRequest(new string[] { args.ChannelId })).Result;
            string versionId = getVersionsResponse.versions.First().versionId;

            var getDownloadManifestV2Response = _sds.GetDownloadManifestV2(new GetDownloadManifestV2Request(args.AdgGoodId)).Result;
            var targetManifestPath = DownloadToTempFile(getDownloadManifestV2Response.downloadUrls);
            Manifest targetManifest = null;
            using (var fileStream = new StreamReader(targetManifestPath))
            {
                targetManifest = _manifestSerializer.Deserialize(fileStream.BaseStream).Result; //Manifest.Parser.ParseFrom(fileStream.BaseStream);
            }

            var downloader = new VersionDownloader(_sds, args.AdgGoodId, args.ChannelId, versionId, args.DownloadPath, targetManifest);

            downloader.Begin();

            return true;
        }

        private string DownloadToTempFile(IEnumerable<string> sources)
        {
            using (var webclient = new WebClient())
            {
                string tempFileName = Path.GetTempFileName();
                foreach (var source in sources)
                {
                    try
                    {
                        webclient.DownloadFile(source, tempFileName);
                        return tempFileName;
                    }
                    catch (Exception)
                    {
                        // TODO some proper handling
                    }
                }
            }

            return null;
        }
    }
}
