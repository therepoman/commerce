﻿using Fuel.Ingest.Core.Models;
using System;
using System.Collections.Generic;
using System.Net;
using Fuel.SoftwareDistributionAdminService.Models;
using Polly;
using Fuel.Ingest.Core.Command;
using Fuel.SoftwareDistributionAdminService.Client;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IIngestVersionWorkflow
    {
        bool IngestVersion(string rootDirectory, IngestVersionArgs ingestionArgs);
    }

    public class IngestVersionWorkflow : IIngestVersionWorkflow
    {
        private static readonly int MAX_RETRIES = 3;
        private static readonly int INITIAL_RETRY = 500;
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;
        private readonly int _maxThreads;
        private readonly FileUploader _uploader;
        private readonly FileDownloader _downloader;
        private readonly FileScanner _fileScanner;
        private readonly IngestionProcess _ingestionProcess;
        private readonly IngestManifestConstructor _ingestManifestConstructor;

        public IngestVersionWorkflow (ISoftwareDistributionAdminServiceClient softwareDistributionAdminServiceClient, int maxThreads, FileUploader uploader, FileDownloader downloader, FileScanner fileScanner,
            IngestionProcess ingestionProcess, IngestManifestConstructor ingestManifestConstructor)
        {
            _softwareDistributionAdminServiceClient = softwareDistributionAdminServiceClient;
            _maxThreads = maxThreads;
            _uploader = uploader;
            _downloader = downloader;
            _fileScanner = fileScanner;
            _ingestionProcess = ingestionProcess;
            _ingestManifestConstructor = ingestManifestConstructor;
        }

        public bool IngestVersion(string rootDirectory, IngestVersionArgs ingestionArgs)
        {
            try
            {
                // Scan files
                Console.WriteLine($"Scanning files in {rootDirectory}");
                List<string> files = _fileScanner.ScanProductFiles(rootDirectory);
                for (int i = 0; i < files.Count; i++)
                {
                    // Convert absolute paths into relative
                    files[i] = files[i].Replace(rootDirectory, String.Empty);
                }

                CreateVersionRequest request = new CreateVersionRequest
                {
                    productId = ingestionArgs.ProductId,
                    versionName = ingestionArgs.VersionName,
                    description = ingestionArgs.Description,
                    productAssetList = files.ConvertAll(p => new ProductAsset { path = p })
                };

                Console.WriteLine($"Creating new version...");
                var response = _softwareDistributionAdminServiceClient.CreateVersion(request);

                // Get upload manifest
                var result = Policy
                    .Handle<WebException>()
                    .WaitAndRetry(MAX_RETRIES,
                        retryAttempt => TimeSpan.FromMilliseconds(INITIAL_RETRY * Math.Pow(retryAttempt, 2)))
                    .ExecuteAndCapture(() => _downloader.Download(response.version.uploadManifest));

                if (result.Outcome == OutcomeType.Failure)
                {
                    Console.WriteLine($"Failed to download upload manifest due to exception: {result.FinalException.Message}");
                    return false;
                }

                UploadManifest uploadManifest = result.Result;

                if (!ValidateUploadManifest(uploadManifest, files))
                {
                    Console.WriteLine("Upload manifest doesn't match product files. Aborting.");
                    return false;
                }

                // Upload product assets using manifest
                if (!_uploader.Upload(uploadManifest, rootDirectory, _maxThreads).Result)
                {
                    return false;
                }

                //generate ingest manifest
                var versionId = response.version.versionId;
                var ingestManifestXml = GetIngestManifest(uploadManifest, versionId, rootDirectory);

                _ingestionProcess.StartIngestion(versionId, ingestManifestXml);

                return _ingestionProcess.WaitForIngestionToResolve(versionId);
            }
            catch (NotFoundException e)
            {
                Console.WriteLine($"The given product or version was not found. {e.Message}");
                return false;
            }
            catch (InvalidTwitchAuthException e)
            {
                Console.WriteLine($"Given Twitch OAuth token ({ingestionArgs.TwitchOAuthToken}) doesn't have permission to ingest a new product version: \"{e.Message}\"");
                return false;
            }
            catch (Exception e)
            {
                //catchall as a last resort for any unexpected exceptions. This will allow us to atleat have some sort of information.
                Console.WriteLine($"Failed to ingest new product version: \"{e.Message}\"");
                return false;
            }
        }

        /* Validates the upload manifest by checking against the file list determined by the client.
         * The upload manifest is valid if the number of files matches and each file is present in the manifest.
         */
        private bool ValidateUploadManifest(UploadManifest uploadManifest, List<string> files)
        {
            // Get path for each file of the upload manifest
            var manifestFiles = uploadManifest.Files.ConvertAll<string>((f) => f.Path);

            return manifestFiles.Count == files.Count && files.TrueForAll((f) => manifestFiles.Contains(f));
        }

        private string GetIngestManifest(UploadManifest uploadManifest, string versionId, string rootDirectory)
        {
            Console.WriteLine("\nComputing ingest-manifest");
            return _ingestManifestConstructor.CreateIngestManifest(uploadManifest, versionId, rootDirectory).ToXml();
        }
    }

}
