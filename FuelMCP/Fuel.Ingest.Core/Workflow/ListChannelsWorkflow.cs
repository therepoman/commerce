﻿using Fuel.Ingest.Core.Command;
using System;
using System.Collections.Generic;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IListChannelsWorkflow
    {
        bool ListChannels(ListChannelsArgs listChannelsArgs);
    }

    public class ListChannelsWorkflow : IListChannelsWorkflow
    {
        private const int PAGE_LIMIT = 100;
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;

        public ListChannelsWorkflow(ISoftwareDistributionAdminServiceClient softwareDistributionAdminServiceClient)
        {
            _softwareDistributionAdminServiceClient = softwareDistributionAdminServiceClient;
        }

        public bool ListChannels(ListChannelsArgs listChannelsArgs)
        {
            try
            {
                var channels = new List<ChannelRef>();
                string lastEvaluatedChannel = null;
                do
                {
                    ListProductChannelsRequest request = new ListProductChannelsRequest
                    {
                        productId = listChannelsArgs.ProductId,
                        limit = PAGE_LIMIT,
                        exclusiveStartChannelName = lastEvaluatedChannel,
                    };

                    var result = _softwareDistributionAdminServiceClient.ListProductChannels(request);

                    lastEvaluatedChannel = result.lastEvaluatedChannelName;
                    channels.AddRange(result.channels);

                } while (lastEvaluatedChannel != null);

                if (channels.Count == 0)
                {
                    Console.WriteLine("No channels for product id {0}", listChannelsArgs.ProductId);
                }
                else
                {
                    channels.ForEach(channel =>
                    {
                        Console.WriteLine("Channel Name: {0}", channel.channelName);
                        Console.WriteLine("    Channel Id: {0}", channel.channelId);
                        Console.WriteLine("    Description: {0}", channel.description);
                        Console.WriteLine("    Active Version Id: {0}", channel.activeVersionId);
                    });
                }

                return true;
            }
            catch (NotFoundException e)
            {
                Console.WriteLine($"The given product was not found. {e.Message}");
                return false;
            }
            catch (InvalidTwitchAuthException e)
            {
                Console.WriteLine($"Given Twitch OAuth token ({listChannelsArgs.TwitchOAuthToken}) doesn't have permission to list the product channels: \"{e.Message}\"");
                return false;
            }
            catch (Exception e)
            {
                //catchall as a last resort for any unexpected exceptions. This will allow us to at least have some sort of information.
                Console.WriteLine($"Failed to list the channels for the given product: \"{e.Message}\"");
                return false;
            }
        }
    }
}
