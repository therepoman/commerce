﻿using Fuel.Ingest.Core.Command;
using System;
using System.Collections.Generic;
using Fuel.SoftwareDistributionAdminService.Models;
using Fuel.SoftwareDistributionAdminService.Client;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IListVersionsWorkflow
    {
        bool ListVersions(ListVersionsArgs listVersionsArgs);
    }

    public class ListVersionsWorkflow : IListVersionsWorkflow
    {
        private const int PAGE_LIMIT = 100;
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;

        public ListVersionsWorkflow(ISoftwareDistributionAdminServiceClient softwareDistributionAdminServiceClient)
        {
            _softwareDistributionAdminServiceClient = softwareDistributionAdminServiceClient;
        }

        public bool ListVersions(ListVersionsArgs listVersionsArgs)
        {
            try
            {
                var versions = new List<VersionRef>();
                string lastEvaluatedVersion = null;
                do
                {
                    ListProductVersionsRequest request = new ListProductVersionsRequest
                    {
                        productId = listVersionsArgs.ProductId,
                        limit = PAGE_LIMIT,
                        exclusiveStartVersionName = lastEvaluatedVersion,
                    };

                    var result = _softwareDistributionAdminServiceClient.ListProductVersions(request);

                    lastEvaluatedVersion = result.lastEvaluatedVersionName;
                    versions.AddRange(result.versions);

                } while (lastEvaluatedVersion != null);

                if (versions.Count == 0)
                {
                    Console.WriteLine("No versions for product id {0}", listVersionsArgs.ProductId);
                }
                else
                {
                    versions.ForEach(version =>
                    {
                        Console.WriteLine("Version Name: {0}, Version Id: {1}, Description: {2}, Ingestion Status: {3}",
                            version.versionName, version.versionId, version.description, version.status);
                    });
                }

                return true;
            }
            catch (NotFoundException e)
            {
                Console.WriteLine($"The given product was not found. {e.Message}");
                return false;
            }
            catch (InvalidTwitchAuthException e)
            {
                Console.WriteLine($"Given Twitch OAuth token ({listVersionsArgs.TwitchOAuthToken}) doesn't have permission to list the product versions: \"{e.Message}\"");
                return false;
            }
            catch (Exception e)
            {
                //catchall as a last resort for any unexpected exceptions. This will allow us to at least have some sort of information.
                Console.WriteLine($"Failed to list the versions for the given product: \"{e.Message}\"");
                return false;
            }
        }
    }
}
