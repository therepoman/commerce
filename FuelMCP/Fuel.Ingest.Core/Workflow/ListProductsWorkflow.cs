﻿using Fuel.Ingest.Core.Command;
using System;
using System.Collections.Generic;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionAdminService.Models;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IListProductsWorkflow
    {
        bool ListProducts(ListProductsArgs listProductsArgs);
    }

    public class ListProductsWorkflow : IListProductsWorkflow
    {
        private const int PAGE_LIMIT = 100;
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;

        public ListProductsWorkflow(ISoftwareDistributionAdminServiceClient client)
        {
            _softwareDistributionAdminServiceClient = client;
        }

        public bool ListProducts(ListProductsArgs listProductsArgs)
        {
            try
            {
                var products = new List<ProductRef>();
                string lastEvaluatedProduct = null;
                do
                {
                    ListProductsRequest request = new ListProductsRequest
                    {
                        limit = PAGE_LIMIT,
                        exclusiveStartProductName = lastEvaluatedProduct,
                    };

                    var result = _softwareDistributionAdminServiceClient.ListProducts(request);

                    lastEvaluatedProduct = result.lastEvaluatedProductName;
                    products.AddRange(result.products);

                } while (lastEvaluatedProduct != null);

                if (products.Count == 0)
                {
                    Console.WriteLine("Don't have access to any products");
                }
                else
                {
                    products.ForEach(product =>
                    {
                        Console.WriteLine("Product Name: {0}", product.productName);
                        Console.WriteLine("    Product Id: {0}", product.productId);
                        Console.WriteLine("    Description: {0}", product.description);
                    });
                }

                return true;
            }
            catch (InvalidTwitchAuthException e)
            {
                Console.WriteLine($"Given Twitch OAuth token ({listProductsArgs.TwitchOAuthToken}) doesn't have permission to list products: \"{e.Message}\"");
                return false;
            }
            catch (Exception e)
            {
                //catchall as a last resort for any unexpected exceptions. This will allow us to at least have some sort of information.
                Console.WriteLine($"Failed to list products: \"{e.Message}\"");
                return false;
            }
        }
    }
}
