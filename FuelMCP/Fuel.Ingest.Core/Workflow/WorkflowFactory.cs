﻿using System;
using Fuel.Ingest.Core.Command;
using Fuel.SoftwareDistributionAdminService.Client;
using Fuel.SoftwareDistributionService.Client;
using Fuel.Coral.Client;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IWorkflowFactory
    {
        IIngestVersionWorkflow CreateIngestVersionWorkflow(string sdasEndpointUrl, string twitchOAuthToken, int maxThreads);
        IActivateVersionWorkflow CreateActivateVersionWorkflow(string sdasEndpointUrl, string twitchOAuthToken);
        IListVersionsWorkflow CreateListVersionsWorkflow(string sdasEndpointUrl, string twitchOAuthToken);
        IListChannelsWorkflow CreateListChannelsWorkflow(string sdasEndpointUrl, string twitchOAuthToken);
        IListProductsWorkflow CreateListProductsWorkflow(string sdasEndpointUrl, string twitchOAuthToken);
        IDownloadVersionWorkflow CreateDownloadVersionWorkflow(string sdsEndpointUrl, string twitchOAuthToken);
    }

    public class WorkflowFactory : IWorkflowFactory
    {
        private readonly ISoftwareDistributionAdminServiceClientFactory _sdasFactory;
        private readonly string _useragent;
        public WorkflowFactory(ISoftwareDistributionAdminServiceClientFactory sdasFactory)
        {
            _sdasFactory = sdasFactory;
            _useragent = $"FuelIngestion/release-{VersionCommand.GetVersion()}";
        }

        public IIngestVersionWorkflow CreateIngestVersionWorkflow(string sdasEndpointUrl, string twitchOAuthToken, int maxThreads)
        {
            var client = _sdasFactory.Create(sdasEndpointUrl, twitchOAuthToken, _useragent);
            FileUploader uploader = new FileUploader();
            FileDownloader downloader = new FileDownloader();
            FileScanner fileScanner = new FileScanner();
            IngestionProcess ingestionProcess = new IngestionProcess(client);
            IngestManifestConstructor ingestManifestConstructor = new IngestManifestConstructor();
            return new IngestVersionWorkflow(client, maxThreads, uploader, downloader, fileScanner, ingestionProcess, ingestManifestConstructor);
        }

        public IActivateVersionWorkflow CreateActivateVersionWorkflow(string sdasEndpointUrl, string twitchOAuthToken)
        {
            var client = _sdasFactory.Create(sdasEndpointUrl, twitchOAuthToken, _useragent);
            return new ActivateVersionWorkflow(client);
        }

        public IListVersionsWorkflow CreateListVersionsWorkflow(string sdasEndpointUrl, string twitchOAuthToken)
        {
            var client = _sdasFactory.Create(sdasEndpointUrl, twitchOAuthToken, _useragent);
            return new ListVersionsWorkflow(client);
        }

        public IListChannelsWorkflow CreateListChannelsWorkflow(string sdasEndpointUrl, string twitchOAuthToken)
        {
            var client = _sdasFactory.Create(sdasEndpointUrl, twitchOAuthToken, _useragent);
            return new ListChannelsWorkflow(client);
        }

        public IListProductsWorkflow CreateListProductsWorkflow(string sdasEndpointUrl, string twitchOAuthToken)
        {
            var client = _sdasFactory.Create(sdasEndpointUrl, twitchOAuthToken, _useragent);
            return new ListProductsWorkflow(client);
        }

        public IDownloadVersionWorkflow CreateDownloadVersionWorkflow(string sdsEndpointUrl, string twitchOAuthToken)
        {
            var client = new SoftwareDistributionServiceClient(sdsEndpointUrl, _useragent, twitchOAuthToken);
            return new DownloadVersionWorkflow(client);
        }
    }
}
