﻿using System;
using Fuel.Ingest.Core.Command;
using Fuel.SoftwareDistributionAdminService.Models;
using Fuel.SoftwareDistributionAdminService.Client;

namespace Fuel.Ingest.Core.Workflow
{
    public interface IActivateVersionWorkflow
    {
        bool ActivateVersion(ActivateVersionArgs activateArgs);
    }

    public class ActivateVersionWorkflow : IActivateVersionWorkflow
    {
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;

        public ActivateVersionWorkflow(ISoftwareDistributionAdminServiceClient softwareDistributionAdminServiceClient)
        {
            _softwareDistributionAdminServiceClient = softwareDistributionAdminServiceClient;
        }

        public bool ActivateVersion(ActivateVersionArgs activateArgs)
        {
            try
            {
                SetChannelActiveVersionRequest request = new SetChannelActiveVersionRequest
                {
                    channelId = activateArgs.ChannelId,
                    versionId = activateArgs.VersionId
                };


                _softwareDistributionAdminServiceClient.SetChannelActiveVersion(request);

                Console.WriteLine("Success!");
                Console.WriteLine("Version id {0} is now the active version for channel id {1}", activateArgs.VersionId, activateArgs.ChannelId);
                return true;
            }
            catch (NotFoundException e)
            {
                Console.WriteLine($"The given channel or version was not found. {e.Message}");
                return false;
            }
            catch (InvalidStateException e)
            {
                Console.WriteLine($"The given channel or version is in an invalid state. {e.Message}");
                return false;
            }
            catch (InvalidTwitchAuthException e)
            {
                Console.WriteLine($"Given Twitch OAuth token ({activateArgs.TwitchOAuthToken}) doesn't have permission to activate the version: \"{e.Message}\"");
                return false;
            }
            catch (Exception e)
            {
                //catchall as a last resort for any unexpected exceptions. This will allow us to atleast have some sort of information.
                Console.WriteLine($"Failed to activate the product version for the given channel: \"{e.Message}\"");
                return false;
            }
        }
    }
}
