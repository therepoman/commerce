using System;
using System.Threading;
using Fuel.SoftwareDistributionAdminService.Models;
using Fuel.SoftwareDistributionAdminService.Client;

namespace Fuel.Ingest.Core
{
    public class IngestionProcess
    {
        private static readonly TimeSpan IngestionStatusCheckInterval = TimeSpan.FromSeconds(5);
        private static readonly TimeSpan TimeoutForIngestion = TimeSpan.FromHours(1);
        private readonly ISoftwareDistributionAdminServiceClient _softwareDistributionAdminServiceClient;

        public IngestionProcess(ISoftwareDistributionAdminServiceClient softwareDistributionAdminServiceClient)
        {
            _softwareDistributionAdminServiceClient = softwareDistributionAdminServiceClient;
        }

        public virtual void StartIngestion(string versionId, string ingestManifestXml)
        {
            Console.WriteLine("\nStarting Ingestion");
            var attachVersionIngestionManifestRequest = new AttachVersionIngestionManifestRequest(versionId, ingestManifestXml);
            _softwareDistributionAdminServiceClient.AttachVersionIngestionManifest(attachVersionIngestionManifestRequest);
        }

        public virtual bool WaitForIngestionToResolve(string versionId)
        {
            var timeoutTime = DateTimeOffset.UtcNow.Add(TimeoutForIngestion);
            var currentTime = DateTimeOffset.UtcNow;
            while (currentTime < timeoutTime)
            {
                try
                {
                    var versionDetails = GetVersionDetails(versionId);
                    //Print out the current ingestion status and overwrite the previous status message.
                    Console.Write($"\r[{currentTime.LocalDateTime}] Ingestion Status is \"{versionDetails.status}\", {versionDetails.statusMessage}                                         ");
                    switch (versionDetails.status)
                    {
                        case VersionStatus.AVAILABLE:
                            Console.WriteLine($"\nSuccess ingesting VersionId=\"{versionId}\"");
                            return true;
                        case VersionStatus.ERROR:
                            Console.WriteLine($"\nError while ingesting VersionId=\"{versionId}\"");
                            return false;
                        default:
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.Write($"\r[{currentTime.LocalDateTime}] Issue getting ingestion status. Retrying... \"{e.Message}\"                                         ");
                    
                }
                Thread.Sleep(IngestionStatusCheckInterval);
                currentTime = DateTimeOffset.UtcNow;
            }
            Console.WriteLine("\nIngestion Client has timed out while waiting for the ingestion process to complete");
            return false;
        }


        private VersionDetails GetVersionDetails(string versionId)
        {
            var describeVersionRequest = new DescribeVersionRequest
            {
                versionId = versionId
            };
            return _softwareDistributionAdminServiceClient.DescribeVersion(describeVersionRequest).version;
        }
    }
}