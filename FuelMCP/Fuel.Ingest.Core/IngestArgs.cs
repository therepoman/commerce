﻿using System.Linq;

namespace Fuel.Ingest.Core
{
    public class IngestArgs
    {
        public string[] OtherArgs = new string[] {};
        public string Command = null;
        public string ErrorMessage;
    }

    public class IngestArgsParser
    {
        public IngestArgs Parse(string[] args)
        {
            if (args.Length == 0)
            {
                return new IngestArgs
                {
                    ErrorMessage = "Must provide a command",
                };
            }

            return new IngestArgs
            {
                Command = args[0],
                OtherArgs =  args.Skip(1).ToArray(),
            };
        }
    }
}
