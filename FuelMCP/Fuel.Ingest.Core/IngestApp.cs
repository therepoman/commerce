﻿using System.Collections.Generic;

namespace Fuel.Ingest.Core
{
    public class IngestApp : ICommand
    {
        private readonly List<ICommand> _commands;

        public IngestApp(List<ICommand> commands)
        {
            _commands = commands;
        }

        public InvokeResult Exec(string[] args)
        {
            var result = new IngestArgsParser().Parse(args);
            if (result.ErrorMessage != null)
            {
                return new InvokeResult
                {
                    ExitCode = 1,
                    ErrorMessage = result.ErrorMessage,
                };
            }

            foreach (var command in _commands)
            {
                if (result.Command.Equals(command.Command))
                {
                    return command.Exec(result.OtherArgs);
                }
            }

            return InvokeResult.Fail($"No such command: {result.Command}");
        }

        public string Command => "not used";
    }
}
