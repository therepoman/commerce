﻿using System;

namespace Fuel.Ingest.Core.Models
{
    public class SignedUrl
    {
        public DateTime expires { get; }
        public string url { get; }

        public SignedUrl(string url, DateTime expires)
        {
            this.url = url;
            this.expires = expires;
        }
    }
}
