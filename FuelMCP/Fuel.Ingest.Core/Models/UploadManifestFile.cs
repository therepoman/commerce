﻿using Newtonsoft.Json;

namespace Fuel.Ingest.Core.Models
{
    public class UploadManifestFile
    {
        [JsonProperty(PropertyName="signed-url")]
        public SignedUrl SignedUrl { get; }
        [JsonProperty(PropertyName = "path")]
        public string Path { get; }
        [JsonProperty(PropertyName = "s3-bucket")]
        public string S3Bucket { get; }

        public UploadManifestFile(string path, string s3Bucket, SignedUrl signedUrl)
        {
            Path = path;
            S3Bucket = s3Bucket;
            SignedUrl = signedUrl;
        }
    }
}
