﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fuel.Ingest.Core.Models
{
    public class UploadManifest
    {
        [JsonProperty(PropertyName = "files")]
        public List<UploadManifestFile> Files { get; }

        public UploadManifest(List<UploadManifestFile> files)
        {
            this.Files = files;
        }
    }
}
