﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Fuel.Ingest.Core.Models;
using MCP.Common.Contracts.Manifest;
using System.Threading;

namespace Fuel.Ingest.Core
{
    public class IngestManifestConstructor
    {
        public virtual IngestManifest CreateIngestManifest(UploadManifest uploadManifest, string versionId, string rootDirectory)
        {
            return new IngestManifest(uploadManifest, versionId, rootDirectory);
        }
    }
    public class IngestManifest
    {
        private static readonly int FILE_STREAM_BUFFER_SIZE = 128 * 1024;
        private readonly UploadManifest _uploadManifest;
        private readonly string _versionId;
        private readonly string _rootDirectory;

        public IngestManifest(UploadManifest uploadManifest, string versionId, string rootDirectory)
        {
            _uploadManifest = uploadManifest;
            _versionId = versionId;
            _rootDirectory = rootDirectory;
        }

        public virtual string ToXml()
        {
            // TODO This should all be replaced with the new protobuf manifest or something

            throw new NotImplementedException();
            //int filesDone = 0;
            //var totalFiles = _uploadManifest.Files.Count;

            //Console.Write("\rIngest-manifest computation in progress {{{0}/{1}}}         ", filesDone, totalFiles);
            //var manifestFiles = _uploadManifest.Files.AsParallel().Select(file =>
            //{
            //    var localPath = Path.Combine(_rootDirectory, file.Path);
            //    var fileDetails = GetFileDetails(localPath);
            //    var manifestFile = new ManifestFile
            //    {
            //        Content = new Content
            //        {
            //            Length = fileDetails.Length,
            //            Type = "application/octet-stream"
            //        },
            //        Verification = new Verification
            //        {
            //            Algorithm = "SHA-256",
            //            Data = fileDetails.Checksum,
            //            Method = "hash"
            //        },
            //        Ingestion = new Ingestion
            //        {
            //            Location = new S3Location
            //            {
            //                Bucket = file.S3Bucket,
            //                Key = $"{_versionId}/{file.Path}"
            //            }
            //        },
            //        Installation = new Installation
            //        {
            //            Location = new LocalFile
            //            {
            //                FileName = Path.GetFileName(file.Path),
            //                Path = Path.GetDirectoryName(file.Path)
            //            }
            //        }
            //    };

            //    Interlocked.Increment(ref filesDone);
            //    Console.Write("\rIngest-manifest computation in progress {{{0}/{1}}}         ", filesDone, totalFiles);

            //    return manifestFile;
            //}).ToList();

            //var manifest = new Manifest
            //{
            //    Files = manifestFiles,
            //};

            //return new ManifestConverter().ToXml(manifest);
        }

        private static FileDetails GetFileDetails(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, FILE_STREAM_BUFFER_SIZE))
            {
                var sha = new SHA256Managed();
                byte[] checksumBytes = sha.ComputeHash(stream);
                var checksum = BitConverter.ToString(checksumBytes).Replace("-", String.Empty).ToLower();
                var length = new FileInfo(path).Length;
                return new FileDetails
                {
                    Length = length,
                    Checksum = checksum
                };
            }
        }

        private class FileDetails
        {
            public long Length { get; set; }
            public string Checksum { get; set; }
        }
    }
}
