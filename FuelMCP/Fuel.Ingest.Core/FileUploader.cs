﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Polly;
using Fuel.Ingest.Core.Models;
using System.Threading.Tasks;

namespace Fuel.Ingest.Core
{
    public class StateInfo
    {
        public string FilePath { get; }
        public string Url { get; }
        public int TotalFiles { get; }
        public ManualResetEvent ManualEvent { get; }
        public CancellationTokenSource TokenSource { get; }

        public StateInfo(
            string FilePath,
            string Url,
            int TotalFiles,
            ManualResetEvent ManualEvent,
            CancellationTokenSource TokenSource)
        {
            this.FilePath = FilePath;
            this.Url = Url;
            this.TotalFiles = TotalFiles;
            this.ManualEvent = ManualEvent;
            this.TokenSource = TokenSource;
        }
    }

    public class FileUploader
    {
        private static readonly int MAX_RETRIES = 3;
        private static readonly int INITIAL_RETRY = 500;

        private int _filesDone;

        public virtual async Task<bool> Upload(UploadManifest manifest, string rootDir, int threads)
        {
            _filesDone = 0;

            var files = manifest.Files;
            var signal = new ManualResetEvent(false);
            var cts = new CancellationTokenSource();

            ThreadPool.SetMaxThreads(threads, threads);

            // Show initial progress
            Console.Write("\rUpload in Progress {{{0}/{1}}}         ", _filesDone, files.Count);

            // Queue the upload for each file in the manifest
            foreach (UploadManifestFile file in files)
            {
                ThreadPool.QueueUserWorkItem(ThreadPoolWorker,
                    new StateInfo(Path.Combine(rootDir, file.Path), file.SignedUrl.url, files.Count, signal, cts));
            }

            await Task.Run(() => signal.WaitOne());

            return !cts.IsCancellationRequested;
        }

        // Wrapper around UploadFile with retries
        private void ThreadPoolWorker(Object stateInfoObj)
        {
            var state = (StateInfo)stateInfoObj;
            var result = RetryableUpload(state);

            if ((result.Outcome == OutcomeType.Failure || result.Result.StatusCode != HttpStatusCode.OK)
                && !state.TokenSource.IsCancellationRequested)
            {
                state.TokenSource.Cancel();
                var exception = result.FinalException;

                Console.WriteLine("\nError occurred uploading. Cancelling upload process.");
                Console.WriteLine("Error: {0}",
                    exception == null ? result.Result.StatusCode.ToString() + " status code returned" :
                    "Exception type: " + exception.GetType() + ", Message: " + exception.Message);
            }
            else if (result.Outcome == OutcomeType.Successful && result.Result.StatusCode == HttpStatusCode.OK)
            {
                // Add one to be relatively accurate with the upload progress. Otherwise, we never get {MAX/MAX}
                Console.Write("\rUpload in Progress {{{0}/{1}}}         ", _filesDone + 1, state.TotalFiles);
            }

            if (Interlocked.Increment(ref _filesDone) == state.TotalFiles)
            {
                state.ManualEvent.Set();
            }
        }

        private HttpWebResponse UploadFile(string url, string filePath, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            var httpRequest = WebRequest.Create(url) as HttpWebRequest;
            httpRequest.AllowWriteStreamBuffering = false;
            httpRequest.ContentLength = new FileInfo(filePath).Length;
            httpRequest.Method = "PUT";
            httpRequest.Timeout = 1000 * 60 * 60; // One hour, needed for extremely large files

            using (Stream dataStream = httpRequest.GetRequestStream())
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                byte[] buffer = new byte[1024 * 8];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    token.ThrowIfCancellationRequested();
                    dataStream.Write(buffer, 0, bytesRead);
                }
            }

            return httpRequest.GetResponse() as HttpWebResponse;
        }

        private PolicyResult<HttpWebResponse> RetryableUpload(StateInfo state)
        {
            int[] retryStatusCodes = { 408, 500, 502, 503, 504 };

            /**
             * Retry when WebException is thrown or a retryable status code is returned.
             * Will try MAX_RETRIES times waiting INITIAL_RETRY milliseconds in between retries,
             * which exponentially increases, unless successful or cancellation was requested.
             */
            return Policy
                .Handle<WebException>()
                .OrResult<HttpWebResponse>(r => !state.TokenSource.IsCancellationRequested && retryStatusCodes.Contains((int)r.StatusCode))
                .WaitAndRetry(MAX_RETRIES,
                    retryAttempt => TimeSpan.FromMilliseconds(INITIAL_RETRY * Math.Pow(retryAttempt, 2)))
                .ExecuteAndCapture(() => UploadFile(state.Url, state.FilePath, state.TokenSource.Token));
        }
    }
}
