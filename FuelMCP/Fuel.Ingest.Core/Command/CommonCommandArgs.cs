﻿using CommandLine;
using CommandLine.Text;

namespace Fuel.Ingest.Core.Command
{
    public class CommonCommandArgs : CommandArgs
    {
        public const string TwitchOauthTokenSystemProperty = "TWITCH_OAUTH_TOKEN";

        [Option('o', "oauthtoken", HelpText = "The Twitch OAuth token for the account which has permission to access this product. Or you can define it in the '" + TwitchOauthTokenSystemProperty + "' system property")]
        public string TwitchOAuthToken { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        public static T Parse<T>(string[] args, IngestEnvironment env) where T : CommonCommandArgs, new()
        {
            var parsedArgs = Parse<T>(args);
            parsedArgs.TwitchOAuthToken = GetTwitchOAuthToken(parsedArgs, env);

            if (string.IsNullOrEmpty(parsedArgs.TwitchOAuthToken))
            {
                throw new CommandLineArgParsingException("Must define the --oauthtoken argument or system property '" + CommonCommandArgs.TwitchOauthTokenSystemProperty + "'");
            }
            return parsedArgs;
        }

        //Determines which Twitch OAuth token definition to use.
        private static string GetTwitchOAuthToken(CommonCommandArgs args, IngestEnvironment env)
        {
            var envToken = env.GetEnvironmentVariable(CommonCommandArgs.TwitchOauthTokenSystemProperty);
            var cmdToken = args.TwitchOAuthToken;
            return cmdToken ?? envToken;
        }
    }
}
