﻿using System;
using System.Reflection;

namespace Fuel.Ingest.Core.Command
{
    public class VersionCommand : ICommand
    {
        public InvokeResult Exec(string[] args)
        {
            Console.WriteLine(GetVersion());
            return InvokeResult.Success;
        }

        public string Command => "version";

        public static string GetVersion()
        {
            return $"{Assembly.GetEntryAssembly()?.GetName().Version}";
        }
    }
}
