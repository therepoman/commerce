﻿using CommandLine;
using System;

namespace Fuel.Ingest.Core.Command
{
    public abstract class CommandArgs
    {
        //Parses the command line args for [Option] attribute properties of the given Type.
        public static T Parse<T>(string[] args) where T : CommandArgs, new()
        {
            var parsedArgs = new T();
            var argParser = new Parser(settings =>
            {
                settings.HelpWriter = Console.Out;
                settings.IgnoreUnknownArguments = true;
            });
            var success = argParser.ParseArguments(args, parsedArgs);
            if (!success)
            {
                throw new CommandLineArgParsingException("Missing Arguments");
            }
            return parsedArgs;
        }
    }

    public class CommandLineArgParsingException : System.Exception
    {
        public CommandLineArgParsingException(string message) : base(message) { }
    }
}
