﻿using Amazon.MCP.Common.Contracts;
using Amazon.MCP.Common.Exceptions;
using CommandLine;
using System.IO;

namespace Fuel.Ingest.Core.Command
{
    public class ValidateArgs : CommandArgs
    {
        [Option('d', "directory", HelpText = "Directory root of the product to validate, usually where fuel.json lives", Required = true)]
        public string Directory { get; set; }
    }

    public class ValidateCommand : ICommand
    {
        private readonly ILaunchConfigProvider _lcpConfigProvider;

        public ValidateCommand(ILaunchConfigProvider lcpConfigProvider)
        {
            _lcpConfigProvider = lcpConfigProvider;
        }

        public InvokeResult Exec(string[] args)
        {
            try
            {
                var vargs = CommandArgs.Parse<ValidateArgs>(args);

                if (!Directory.Exists(vargs.Directory))
                {
                    return InvokeResult.Fail($"No such directory {vargs.Directory}");
                }

                if (!File.Exists(Path.Combine(vargs.Directory, "fuel.json")))
                {
                    return InvokeResult.Fail("Missing fuel.json");
                }

                try
                {
                    _lcpConfigProvider.GetLaunchConfig(vargs.Directory);
                }
                catch (InvalidLaunchConfigException e)
                {
                    return InvokeResult.Fail(e.Message);
                }

                return InvokeResult.Success;
            }
            catch (CommandLineArgParsingException e)
            {
                return InvokeResult.Fail("Missing Arguments");
            }
        }

        public string Command => "validate";
    }
}
