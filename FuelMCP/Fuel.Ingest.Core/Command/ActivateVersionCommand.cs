﻿using CommandLine;
using Fuel.Ingest.Core.Workflow;

namespace Fuel.Ingest.Core.Command
{
    public class ActivateVersionArgs : CommonCommandArgs
    {
        [Option('c', "channel", HelpText = "The channel ID to activate the version on", Required = true)]
        public string ChannelId { get; set; }

        [Option('v', "version", HelpText = "The version ID to activate", Required = true)]
        public string VersionId { get; set; }
    }

    public class ActivateVersionCommand : ICommand
    {
        private readonly IngestEnvironment _environment;
        private readonly IWorkflowFactory _workflowFactory;

        public ActivateVersionCommand(IngestEnvironment environment, IWorkflowFactory workflowFactory)
        {
            _environment = environment;
            _workflowFactory = workflowFactory;
        }

        public InvokeResult Exec(string[] args)
        {
            try
            {
                var activateArgs = CommonCommandArgs.Parse<ActivateVersionArgs>(args, _environment);
                var activateDebugArgs = DebugCommandArgs.Parse(args);
                var sdasEndpointUrl = activateDebugArgs.TargetIngestionServiceUrl;
                var twitchOAuthToken = activateArgs.TwitchOAuthToken;

                // Activate version on channel
                IActivateVersionWorkflow workflow = _workflowFactory.CreateActivateVersionWorkflow(sdasEndpointUrl, twitchOAuthToken);
                var success = workflow.ActivateVersion(activateArgs);
                return success ? InvokeResult.Success : InvokeResult.Fail($"{Command} failed");
            }
            catch (CommandLineArgParsingException e)
            {
                return InvokeResult.Fail(e.Message);
            }
        }

        public string Command => "activate-version";
    }
}
