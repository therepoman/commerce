﻿using Fuel.Ingest.Core.Workflow;

namespace Fuel.Ingest.Core.Command
{
    public class ListProductsArgs : CommonCommandArgs
    {
        
    }

    public class ListProductsCommand : ICommand
    {
        private readonly IngestEnvironment _environment;
        private readonly IWorkflowFactory _workflowFactory;

        public ListProductsCommand(IngestEnvironment environment, IWorkflowFactory workflowFactory)
        {
            _environment = environment;
            _workflowFactory = workflowFactory;
        }

        public InvokeResult Exec(string[] args)
        {
            try
            {
                var listProductsArgs = CommonCommandArgs.Parse<ListProductsArgs>(args, _environment);
                var listProductsDebugArgs = DebugCommandArgs.Parse(args);
                var sdasEndpointUrl = listProductsDebugArgs.TargetIngestionServiceUrl;
                var twitchOAuthToken = listProductsArgs.TwitchOAuthToken;

                // List products
                IListProductsWorkflow workflow = _workflowFactory.CreateListProductsWorkflow(sdasEndpointUrl, twitchOAuthToken);
                var success = workflow.ListProducts(listProductsArgs);

                return success ? InvokeResult.Success : InvokeResult.Fail($"{Command} failed");
            }
            catch (CommandLineArgParsingException e)
            {
                return InvokeResult.Fail(e.Message);
            }
        }

        public string Command => "list-products";
    }
}
