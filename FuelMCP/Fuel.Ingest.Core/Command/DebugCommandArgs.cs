﻿using CommandLine;
using System;
using Fuel.SoftwareDistributionAdminService.Client;

namespace Fuel.Ingest.Core.Command
{
    //Command line arguments used by Fuel developers to test/debug the client. These arguments will not show up in the Help displayed to the user.
    public class DebugCommandArgs : CommandArgs
    {
        [Option('s', "server", HelpText = "[DEBUG ONLY] The URL to the Twitch Ingestion Admin Server to use. You can use an arbitrary URL, 'beta', 'gamma', or 'prod'.")]
        public string TargetIngestionServiceUrl { get; set; }

        //Parses the command line args for [Option] attribute properties of the given Type.
        public static DebugCommandArgs Parse(string[] args)
        {
            var parsedArgs = Parse<DebugCommandArgs>(args);
            var parsedSdasEndpointUrl = parsedArgs.TargetIngestionServiceUrl;
            try
            {
                parsedArgs.TargetIngestionServiceUrl = GetSdasEndpointUrl(parsedSdasEndpointUrl);
            }
            catch (Exception e)
            {
                throw new CommandLineArgParsingException($"Invalid ingestion service endpoint \"{parsedSdasEndpointUrl}\" - {e.Message}");
            }
            
            return parsedArgs;
        }

        private static string GetSdasEndpointUrl(string targetSdasEndpointUrl)
        {
            var sdasEndpointUrl = targetSdasEndpointUrl ?? SoftwareDistributionAdminServiceClient.ProdUrl;
            switch (sdasEndpointUrl.ToLower())
            {
                case "beta":
                    return SoftwareDistributionAdminServiceClient.BetaUrl;
                case "gamma":
                    return SoftwareDistributionAdminServiceClient.GammaUrl;
                case "prod":
                    return SoftwareDistributionAdminServiceClient.ProdUrl;
                default:
                    return new Uri(sdasEndpointUrl).ToString();
            }
        }
    }
}
