﻿using CommandLine;
using Fuel.Ingest.Core.Workflow;

namespace Fuel.Ingest.Core.Command
{
    public class ListVersionsArgs : CommonCommandArgs
    {
        [Option('p', "product", HelpText = "The product to list all versions for", Required = true)]
        public string ProductId { get; set; }
    }

    public class ListVersionsCommand : ICommand
    {
        private readonly IngestEnvironment _environment;
        private readonly IWorkflowFactory _workflowFactory;

        public ListVersionsCommand(IngestEnvironment environment, IWorkflowFactory workflowFactory)
        {
            _environment = environment;
            _workflowFactory = workflowFactory;
        }

        public InvokeResult Exec(string[] args)
        {
            try
            {
                var listVersionsArgs = CommonCommandArgs.Parse<ListVersionsArgs>(args, _environment);
                var listVersionsDebugArgs = DebugCommandArgs.Parse(args);
                var sdasEndpointUrl = listVersionsDebugArgs.TargetIngestionServiceUrl;
                var twitchOAuthToken = listVersionsArgs.TwitchOAuthToken;

                // List versions for product
                IListVersionsWorkflow workflow = _workflowFactory.CreateListVersionsWorkflow(sdasEndpointUrl, twitchOAuthToken);
                var success = workflow.ListVersions(listVersionsArgs);

                return success ? InvokeResult.Success : InvokeResult.Fail($"{Command} failed");
            }
            catch (CommandLineArgParsingException e)
            {
                return InvokeResult.Fail(e.Message);
            }
        }

        public string Command => "list-versions";
    }
}
