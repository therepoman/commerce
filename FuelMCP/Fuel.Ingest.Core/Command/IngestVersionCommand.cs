﻿using System;
using System.IO;
using Amazon.MCP.Common.Contracts;
using CommandLine;
using Fuel.Ingest.Core.Workflow;

namespace Fuel.Ingest.Core.Command
{
    public class IngestVersionArgs : CommonCommandArgs
    {
        [Option('d', "directory", HelpText = "Directory root of the product, where fuel.json lives", Required = true)]
        public string Directory { get; set; }

        [Option('p', "product", HelpText = "The product ID to ingest the new version for", Required = true)]
        public string ProductId { get; set; }

        [Option('n', "name", HelpText = "The name for this version", Required = true)]
        public string VersionName { get; set; }

        [Option('r', "description", HelpText = "The description of the version")]
        public string Description { get; set; }

        [Option('t', "threads", HelpText = "The maximum number of threads used for uploading product files, (Default=64)", DefaultValue = 64)]
        public int Threads { get; set; }
    }

    public class IngestVersionCommand : ICommand
    {
        private readonly ILaunchConfigProvider _lcpConfigProvider;
        private readonly IngestEnvironment _environment;
        private readonly IWorkflowFactory _workflowFactory;

        public IngestVersionCommand(ILaunchConfigProvider lcpConfigProvider, IngestEnvironment environment, IWorkflowFactory workflowFactory)
        {
            _lcpConfigProvider = lcpConfigProvider;
            _environment = environment;
            _workflowFactory = workflowFactory;
        }

        public InvokeResult Exec(string[] args)
        {
            try
            {
                var ingestionArgs = CommonCommandArgs.Parse<IngestVersionArgs>(args, _environment);
                var ingestionDebugArgs = DebugCommandArgs.Parse(args);
                var sdasEndpointUrl = ingestionDebugArgs.TargetIngestionServiceUrl;
                var twitchOAuthToken = ingestionArgs.TwitchOAuthToken;

                //validate fuel.json
                var validateResult = ValidateLaunchConfig(args);
                if (validateResult.ExitCode != 0)
                {
                    return validateResult;
                }
                var rootDirectory = NormalizeFolderPath(ingestionArgs.Directory);
                //create new version
                IIngestVersionWorkflow workflow = _workflowFactory.CreateIngestVersionWorkflow(sdasEndpointUrl, twitchOAuthToken, ingestionArgs.Threads);
                if (!workflow.IngestVersion(rootDirectory, ingestionArgs))
                {
                    return InvokeResult.Fail("Failed to ingest new version of product.");
                }

                return InvokeResult.Success;
            }
            catch (CommandLineArgParsingException e)
            {
                return InvokeResult.Fail(e.Message);
            }
        }

        public string Command => "ingest-version";

        private InvokeResult ValidateLaunchConfig(string[] args)
        {
            var validateCommand = new ValidateCommand(_lcpConfigProvider);
            return validateCommand.Exec(args);
        }

        private static string NormalizeFolderPath(string path)
        {
            if (path != null)
            {
                //Append a backslash onto the end if there isn't one already
                if (!string.IsNullOrEmpty(path) && path[path.Length - 1] != '\\')
                {
                    path = path + @"\";
                }
                //Get the absolute path
                return Path.GetFullPath(path);
            }
            return null;
        }
    }
}
