﻿using CommandLine;
using Fuel.Ingest.Core.Workflow;
using Fuel.Installer;
using Fuel.SoftwareDistributionService.Client;
using Fuel.SoftwareDistributionService.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Tv.Twitch.Fuel.Sds;

namespace Fuel.Ingest.Core.Command
{
    public class DownloadVersionArgs : CommonCommandArgs
    {
        [Option('g', "good", HelpText = "The ADG good ID", Required = true)]
        public string AdgGoodId { get; set; } // should be able to use sync goods
        [Option('c', "channel", HelpText = "The SDS channel ID", Required = true)]
        public string ChannelId { get; set; }
        [Option('p', "download", HelpText = "The download path", Required = true)]
        public string DownloadPath { get; set; }

    }

    public class DownloadVersionCommand : ICommand
    {
        public string Command => "download-version";

        private readonly IngestEnvironment _environment;
        private readonly IWorkflowFactory _workflowFactory;

        public DownloadVersionCommand(IngestEnvironment environment, IWorkflowFactory workflowFactory)
        {
            _environment = environment;
            _workflowFactory = workflowFactory;
        }

        public InvokeResult Exec(string[] sargs)
        {
            var args = CommonCommandArgs.Parse<DownloadVersionArgs>(sargs, _environment);
            var wf = _workflowFactory.CreateDownloadVersionWorkflow(SoftwareDistributionServiceClient.ProdUrl, args.TwitchOAuthToken);

            wf.DownloadVersion(args);

            return InvokeResult.Success;
        }
    }
}
