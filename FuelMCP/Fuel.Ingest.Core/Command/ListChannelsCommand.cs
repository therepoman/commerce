﻿using CommandLine;
using Fuel.Ingest.Core.Workflow;

namespace Fuel.Ingest.Core.Command
{
    public class ListChannelsArgs : CommonCommandArgs
    {
        [Option('p', "product", HelpText = "The product to list all channels for", Required = true)]
        public string ProductId { get; set; }
    }

    public class ListChannelsCommand : ICommand
    {
        private readonly IngestEnvironment _environment;
        private readonly IWorkflowFactory _workflowFactory;

        public ListChannelsCommand(IngestEnvironment environment, IWorkflowFactory workflowFactory)
        {
            _environment = environment;
            _workflowFactory = workflowFactory;
        }

        public InvokeResult Exec(string[] args)
        {
            try
            {
                var listChannelsArgs = CommonCommandArgs.Parse<ListChannelsArgs>(args, _environment);
                var listChannelsDebugArgs = DebugCommandArgs.Parse(args);
                var sdasEndpointUrl = listChannelsDebugArgs.TargetIngestionServiceUrl;
                var twitchOAuthToken = listChannelsArgs.TwitchOAuthToken;

                // List channels for product
                IListChannelsWorkflow workflow = _workflowFactory.CreateListChannelsWorkflow(sdasEndpointUrl, twitchOAuthToken);
                var success = workflow.ListChannels(listChannelsArgs);

                return success ? InvokeResult.Success : InvokeResult.Fail($"{Command} failed");
            }
            catch (CommandLineArgParsingException e)
            {
                return InvokeResult.Fail(e.Message);
            }
        }

        public string Command => "list-channels";
    }
}
