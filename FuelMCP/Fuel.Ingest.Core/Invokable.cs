﻿using Equ;

namespace Fuel.Ingest.Core
{
    public class InvokeResult : MemberwiseEquatable<InvokeResult>
    {
        public string ErrorMessage;
        public int ExitCode;

        public static InvokeResult Success => new InvokeResult
        {
            ExitCode = 0,
            ErrorMessage = null,
        };
        
        public static InvokeResult Fail(string reason = null)
        {
            return new InvokeResult
            {
                ExitCode = 1,
                ErrorMessage = reason,
            };
        }
    }

    public interface ICommand
    {
        InvokeResult Exec(string[] args);
        string Command { get; }
    }
}
