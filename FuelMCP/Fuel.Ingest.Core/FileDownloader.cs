﻿using System;
using Fuel.Ingest.Core.Models;
using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace Fuel.Ingest.Core
{
    public class FileDownloader
    {
        public virtual UploadManifest Download(string url)
        {
            var httpRequest = WebRequest.Create(url) as HttpWebRequest;
            httpRequest.Method = "GET";

            string manifest = "";
            using (var reader = new StreamReader(httpRequest.GetResponse().GetResponseStream()))
            {
                manifest = reader.ReadToEnd();
            }
            Console.WriteLine("Download Complete");
            // Given a json manifest from S3, convert it to object
            return JsonConvert.DeserializeObject<UploadManifest>(manifest);
        }
    }
}
