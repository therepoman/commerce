﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Fuel.Ingest.Core
{
    public class FileScanner
    {
        public virtual List<string> ScanProductFiles(string rootDirectory)
        {
            var files = new List<string>();
            //DFS of the given product directory
            var directories = new Stack<string>();
            directories.Push(rootDirectory);
            while (directories.Count > 0)
            {
                var currentDirectory = directories.Pop();
                Array.ForEach(Directory.GetDirectories(currentDirectory), directories.Push);
                files.AddRange(Directory.GetFiles(currentDirectory));
            }
            return files;
        }
    }
}
