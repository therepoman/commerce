﻿using System.Collections.Generic;
using Amazon.Fuel.Coral.Model;
using Newtonsoft.Json;
using Xunit;

namespace Amazon.Fuel.Coral.Tests.Model
{
    public class CoralExceptionTest
    {
        //Used only for unit testing
        private class TestCoralException : CoralException
        {
            public override string __type { get; } = "fuel.coral.tests.model#TestCoralException";

            public TestCoralException(string message) : base(message)
            {
                this.message = message;
            }
        }

        [Fact]
        public void ParseException_contructsExpectedException_givenKnownCoralExceptionType()
        {
            var expectedException = new TestCoralException("Test Message");
            var exceptionJson = CreateTestCoralExceptionJson(expectedException.__type, expectedException.message);
            var generalException = JsonConvert.DeserializeObject<CoralException>(exceptionJson);
            var actualException = CoralException.GetSpecificException(generalException);
            Assert.NotNull(actualException);
            Assert.Equal(expectedException.GetType(), actualException.GetType());
            Assert.Equal(expectedException.__type, actualException.__type);
            Assert.Equal(expectedException.message, actualException.message);
        }

        [Fact]
        public void ParseException_returnsUndefinedServiceException_givenUnknownCoralExceptionType()
        {
            var expectedExceptionType = "com.amazon.test.unknownexception";
            var expectedExceptionMessage = "unknown exception type";
            var exceptionJson = CreateTestCoralExceptionJson(expectedExceptionType, expectedExceptionMessage);
            var generalException = JsonConvert.DeserializeObject<CoralException>(exceptionJson);
            var actualException = CoralException.GetSpecificException(generalException);
            Assert.NotNull(actualException);
            Assert.Equal(typeof(UndefinedServiceException), actualException.GetType());
            Assert.True(actualException.message.Contains(expectedExceptionMessage));
        }

        [Fact]
        public void ParseException_contructsExpectedException_givenCoralExceptionTypeWithNoMessageDefined()
        {
            var expectedException = new TestCoralException(null);
            var exception = new Dictionary<string, dynamic>
            {
                ["__type"] = expectedException.__type
            };
            var exceptionJson = JsonConvert.SerializeObject(exception);
            var generalException = JsonConvert.DeserializeObject<CoralException>(exceptionJson);
            var actualException = CoralException.GetSpecificException(generalException);
            Assert.NotNull(actualException);
            Assert.Equal(expectedException.GetType(), actualException.GetType());
            Assert.Equal(expectedException.__type, actualException.__type);
            Assert.Equal(expectedException.message, actualException.message);
        }

        private string CreateTestCoralExceptionJson(string type, string message)
        {
            var exception = new Dictionary<string, dynamic>
            {
                ["__type"] = type,
                ["message"] = message
            };
            return JsonConvert.SerializeObject(exception);
        }
    }
}
