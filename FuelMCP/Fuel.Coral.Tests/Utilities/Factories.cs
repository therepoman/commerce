﻿using System;
using Amazon.Fuel.Common.Contracts;
using Ploeh.AutoFixture;

namespace Amazon.Fuel.Coral.Tests.Utilities
{
    public class Factories
    {
        private static Fixture _instance;

        public static Fixture Fixture => _instance ?? (_instance = new Fixture());

        public static string ValidOauth()
        {
            return Fixture.Create("oauth");
        }

        public static ProductId ValidProductId()
        {
            return Fixture.Create<ProductId>();
        }

        public static string ValidVersionId()
        {
            return Fixture.Create("version_id");
        }

        public static string ValidCoralExceptionType()
        {
            return "tv.twitch.exception.Service#TestException";
        }

        public static string ValidCoralExceptionMessage()
        {
            return "test message exception";
        }

        public static string ValidUrl()
        {
            return Fixture.Create<Uri>().ToString();
        }

        public static string ValidServiceNamespace()
        {
            return Fixture.Create<string>();
        }

        public static string ValidOperation()
        {
            return Fixture.Create<string>();
        }

        public static string ValidUseragent()
        {
            return Fixture.Create<string>();
        }
    }
}