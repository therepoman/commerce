using System;
using System.Threading.Tasks;

namespace Ext.Auth.Map
{
    // Interface copied from AuthMap
    // DO NOT EDIT TO RETAIN COMPATIBILITY WITH AMAZON.AUTH.MAP.DLL
    /// (see https://code.amazon.com/packages/AmazonAuthMap/trees/master/--/src/Amazon.Auth.Map.Win/Windows)
    /// 
    public interface IAppInfo
    {
        string DeviceName { get; }
        string DeviceSerialId { get; }
        string DeviceTypeId { get; }
        string HttpUserAgentName { get; }
        string LocalStoragePath { get; }
        string Name { get; }
        bool RegisterDeviceNameWithPanda { get; }
        string Version { get; }
    }


    public class NetworkAvailabilityEventArgs : EventArgs
    {
        public NetworkAvailabilityEventArgs(bool isAvailable)
        {
            IsAvailable = isAvailable;
        }

        public bool IsAvailable { get; private set; }
    }

    public interface INotifyPropertyChanged
    {
        event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    }

    public interface INetworkDetector : IDisposable, IObservable<NetworkAvailabilityEventArgs>, INotifyPropertyChanged
    {
        bool IsAvailable { get; }

        Task<bool> CheckAvailabilityAsync(bool force = false);
        IObservable<NetworkAvailabilityEventArgs> WaitUntilNetworkIsAvailable();
        IObservable<NetworkAvailabilityEventArgs> WaitUntilNetworkIsAvailable(int timeoutMilliseconds);
        IObservable<NetworkAvailabilityEventArgs> WaitUntilNetworkIsAvailable(TimeSpan timeout);
    }
}