﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common
{
    public interface IAppInfo : Ext.Auth.Map.IAppInfo
    {
        string SpectatorName { get; }

        string PmetDomain { get; }

        string PmetSource { get; }

        string DeviceSerialId { get; }

        string DeviceTypeId { get; }

        string HttpUserAgentName { get; }

        string Version { get; }
    }
}
