﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper.Configuration.Conventions;

namespace Amazon.Fuel.Common.Contracts
{
    public enum ERetrieveVersionCondition
    {
        /// <summary> Use last good cached version, regardless of expiration timings. </summary>
        Offline,
        /// <summary> Only check remote service for update if the expiration time exceeded, else use cached. </summary>
        UpdateIfExpired,
        /// <summary> Always check remote service for update. </summary>
        ForceUpdate
    }


    public enum ECheckUpToDateResult
    {
        // special case results
        /// <summary> Product not found in local product DB. Unknown product, need resync? </summary>
        NoProductInDb,
        /// <summary> GameInstallInfo not found in local install DB. Need to reinstall? </summary>
        NotInstalled,
        /// <summary> Entitlement not found in local entitlements DB. Need resync? </summary>
        EntitlementNotFound,
        /// <summary> Couldn't retrieve remote version. This could be an auth failure, a revoked entitlement. </summary>
        RemoteVersionInvalid,

        // non-special case results
        /// <summary> New version successfully obtained and doesn't match. Need update. </summary>
        NeedsUpdate,
        /// <summary> New version successfully obtained and matches. Doesn't need update. </summary>
        UpToDate,

        /// <summary> There was a network failure when trying to get the product information. </summary>
        NetworkFailure
    }

    public interface IInstalledGamesPlugin
    {
        void Save(GameInstallInfo info);
        GameInstallInfo Get(ProductId productId);
        bool IsInstalled(ProductId productId);
        Task<ECheckUpToDateResult> CheckUpToDateByProductId(ProductId productId, ERetrieveVersionCondition retrieveCondition);
        Task<ECheckUpToDateResult> CheckUpToDateByInstallInfo(GameInstallInfo installInfo, ERetrieveVersionCondition retrieveCondition);
        List<GameInstallInfo> All { get; }
        bool IsRunning(ProductId productId);
        bool IsEntitledInDb(ProductId productId);
        void ClearInstalledVersion(ProductId pid);
    }
}
