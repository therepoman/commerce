﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface ILuceneDbMigrator
    {
        bool TryEnsureDbsMigrated(bool checkMigrationState = true);
    }
}