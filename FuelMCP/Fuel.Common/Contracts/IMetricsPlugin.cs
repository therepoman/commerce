﻿using System;
using Clients.Twitch.Spade.Model;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IMetricsPlugin : IDisposable
    {
        void FlushQueue();
        void AddCounter(string source, string name, string pivotKey, string pivotValue);
        void AddCounter(string source, string name);
        void AddDiscreteValue(string source, string name, int value, string pivotKey, string pivotValue);
        void AddDiscreteValue(string source, string name, int value);
        void AddTimerValue(string source, string name, double value);
        void AddAppStartTimer(string source, string name, string pivotKey, string pivotValue);
        void AddAppStartTimer(string source, string name);
        IMetricsTimer StartTimer(string source, string name);
        IMetricScope CreateScope(string source);
        void SendSpadeEvent(ISpadeEvent spadeEvent);
    }

    public interface IMetricsTimer : IDisposable
    {
        void Stop();
    }
}
