﻿namespace Amazon.Fuel.Common
{
    // these translate directly to exit codes, interpreted by Radium
    public enum EUninstallResult
    {
        Success                    = 0,
        Cancelled                  = 1,
        Failed                     = 2,
        Failed_IsRunning           = 3,
        Failed_InvalidArgs         = 4,
        Failed_ImpersonationFailed = 5,
    }
}