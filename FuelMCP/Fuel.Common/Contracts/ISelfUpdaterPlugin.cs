﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Contracts
{
    public interface ISelfUpdaterPlugin
    {
        /// <summary>
        /// Simply returns true if there is an update available and false if there is not.
        /// </summary>
        /// <returns></returns>
        bool CheckForUpdate();

        
        /// <summary>
        /// Checks for an update and installs one if found.
        /// Doesn't have a return value because installation should kill this application.
        /// </summary>
        void CheckForUpdateAndInstall();
    }
}
