using System;
using System.Threading;

namespace Amazon.Fuel.Common.Contracts
{
    [Flags]
    public enum EInstallGameFlags
    {
        None                    = 0,

        CreateStartMenuShortcut = 1 << 0,
        CreateDesktopShortcut   = 1 << 1,
        RegisterUninstaller     = 1 << 2,

        All                     = ~0
    }

    public interface IGameInstaller
    {
        /// <summary>
        /// Given a game that has been downloaded, runs any launch config PostInstall steps needed.
        /// If Successful, marks the game as Installed in the database.
        /// </summary>
        EInstallUpdateProductResult InstallGame(
            ProductId productId,
            GameInstallInfo installInfo,
            IHelperLauncherProvider helperLauncherProvider,
            string productVersionId,
            EInstallGameFlags installFlags,
            CancellationToken cancelToken = default(CancellationToken));

        /// <summary>
        /// Updates a game's installed version (and flags as installed) in the DB without performing other installation functions.
        /// </summary>
        /// <param name="productId">Product id to update</param>
        /// <param name="productVersionId">Unique version id to assign to this specific product's install info</param>
        void MarkAsInstalled(GameInstallInfo productId, string productVersionId);

        /// <summary>
        /// Create OS file system shortcuts for a given product id.
        /// </summary>
        /// <param name="productId">Product id to install shortcuts</param>
        /// <param name="includeDesktop">Also include desktop shortcuts</param>
        /// <returns></returns>
        bool TryCreateShortcutsForProduct(ProductId productId, bool includeDesktop);
    }
}