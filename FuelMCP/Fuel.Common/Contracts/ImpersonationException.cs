﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Contracts
{
    public class ImpersonationException : Exception
    {
        public ImpersonationException(string message, Exception innerEx = null) : base(message, innerEx) { }
    }
}
