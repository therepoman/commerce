using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;
using Equ;

namespace Amazon.Fuel.Common.Contracts.Manifest
{
    /**
     * Data used to verify that content is intact.
     */
    public class Verification : MemberwiseEquatable<Verification>
    {
        public Verification ()
        {

        }

        public Verification(XElement element)
        {
            this.Method = element?.Attribute("method")?.Value;
            this.Algorithm = element?.Attribute("alg")?.Value;
            this.Data = element?.Attribute("data")?.Value;
        }
        /**
         * Get the method used to perform verification.
         */
        [XmlAttribute("method")]
        public string Method { get; set; }

        /**
         * Get the algorithm used to calculate the verification data.
         */
        [XmlAttribute("alg")]
        public string Algorithm { get; set; }

        /**
         * Get the data used to perform verification.
         */
        [XmlAttribute("data")]
        public string Data { get; set; }
    }
}
