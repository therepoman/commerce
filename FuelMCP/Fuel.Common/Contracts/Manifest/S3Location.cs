using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;
using Equ;

namespace Amazon.Fuel.Common.Contracts.Manifest
{
    /**
     * Representation of the location of an object in S3.
     */
    public class S3Location : MemberwiseEquatable<S3Location>
    {
        public S3Location()
        {

        }

        public S3Location(XElement element)
        {
            this.Bucket = element?.Attribute("bucket")?.Value;
            this.Key = element?.Attribute("key")?.Value;
        }

        /**
         * Get the name of the bucket containing the object.
         */
        [XmlAttribute("bucket")]
        public string Bucket { get; set; }

        /**
         * Get the key that references the object.
         */
        [XmlAttribute("key")]
        public string Key { get; set; }

        public override string ToString()
        {
            return this.Bucket + "/" + this.Key;
        }
    }
}
