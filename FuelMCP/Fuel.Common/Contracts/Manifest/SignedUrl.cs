using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;
using Equ;
using Amazon.Fuel.Common;

namespace Amazon.Fuel.Common.Contracts.Manifest
{
    /**
     * Representation of a signed URL.
     */
    public class SignedUrl : MemberwiseEquatable<SignedUrl>
    {
        public SignedUrl()
        {

        }

        public SignedUrl(XElement element )
        {
            this.Source = element?.Attribute("source")?.Value;
            this.Url = element?.Attribute("url")?.Value;
            
            var dateString = element?.Attribute("expires")?.Value;

            if ( dateString != null )
            {
                //TODO: Make sure this parse is safe in other time systems ( European example )                
                this.ExpiryTime = DateTime.ParseExact(dateString, Constants.ManifestTimeFormat, null);
            }
        }

        /**
         * Get the source of the URL.
         */
        [XmlAttribute("source")]
        public string Source { get; set; }

        /**
         * Get the time when this URL expires.
         */
        [XmlAttribute("expires")]
        public DateTime ExpiryTime { get; set; }

        /**
         * Get the URL.
         */
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}
