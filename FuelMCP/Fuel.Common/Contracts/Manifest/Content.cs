
using System;
using System.Xml.Linq;
using System.Xml.Serialization;
using Equ;

namespace Amazon.Fuel.Common.Contracts.Manifest
{
    /**
     * A representation of the content descriptor for a file.
     */
    public class Content : MemberwiseEquatable<Content>
    {
        public Content()
        {

        }

        public Content(XElement element)
        {
            this.Type = element?.Attribute("type")?.Value;
            var lenStr = element?.Attribute("length")?.Value;
            long lenResult;
            if ( long.TryParse(lenStr, out lenResult) )
            {
                this.Length = lenResult;
            }
        }

        /**
         * Get the mime-type of the file.
         */
        [XmlAttribute("type")]
        public string Type { get; set; }

        /**
         * Get the length, in bytes, of the file.
         */
        [XmlAttribute("length")]
        public long Length { get; set; }
    }
}