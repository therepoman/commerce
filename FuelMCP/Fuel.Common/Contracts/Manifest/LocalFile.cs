
using System.Xml.Linq;
using System.Xml.Serialization;
using Equ;

namespace Amazon.Fuel.Common.Contracts.Manifest
{
    /**
     * A representation of a local file location.
     */
    public class LocalFile : MemberwiseEquatable<LocalFile>
    {
        public LocalFile ()
        {

        }

        public LocalFile( XElement element )
        {
            this.Path = element?.Attribute("path")?.Value;
            this.FileName = element?.Attribute("name")?.Value;
            this.FilePath = System.IO.Path.Combine(Path, FileName);
        }

        /**
         * Get the relative path of the file.
         */
        [XmlAttribute("path")]
        public string Path { get; set; }

        /**
         * Get the name of the file.
         */
        [XmlAttribute("name")]
        public string FileName { get; set; }

        /**
         * Get the combined path and filename.
         */
        public string FilePath { get; set; }

        public override string ToString()
        {
            return FilePath;
        }
    }
}