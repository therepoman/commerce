﻿using Amazon.Fuel.Common.Contracts.Manifest;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Contracts
{
    public class ManifestWithVersion
    {
        public string SdsVersionId;
        public string VersionName;
        public Sds.Manifest ManifestData;
        public long TotalInstallSizeInBytes;
    }
}
