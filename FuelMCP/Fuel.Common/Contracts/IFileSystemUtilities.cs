﻿using System.IO;

namespace Amazon.Fuel.Common.Contracts
{
    // wrapped for unit testing, behavior should be identical to the .NET calls
    public interface IFileSystemUtilities
    {
        DirectoryInfo CreateDirectory(string dir);
        bool FileExists(string filePath);
        bool DirectoryExists(string filePath);
    }
}