namespace Amazon.Fuel.Common.Contracts
{
    public interface IElevatedIpcCommand<TResponse> : IElevatedCommand<TResponse>
    {
        TResponse TimeoutMessage { get; }
    }
}