﻿
using System;
using System.Security.AccessControl;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IOSDriver
    {
        bool IsDevMode { get; set; }

        string[] AllLibraryRoots { get; }
        string PrimaryLibraryRoot { get; }
        string GameInstallHelperPath { get; }
        string GameRemoverSourcePath { get; }
        string AppBasePath { get; }
        string UserDesktopPath { get; }
        string CommonDesktopPath { get; }
        string CommonStartMenuPath { get; }
        string UserStartMenuPath { get; }
        string EnvironmentApplicationData { get; }
        string EnvironmentCommonApplicationData { get; }
        string CurseLogDirectory { get; }
        string AppDataPathFull { get; }
        string SystemDataPathFull { get; }
        string GameDataPath { get; }
        string SdkLocation { get; }
        string CommonUninstallerExePath { get; }
        string ApplicationDataOverride { get; set; }

        string GetSecondaryLibraryRoot(string pathRoot);
        void CreateDirectoryWithAllUsersPermissions(string directory, bool setPermissionsOnTopmostNewDirectory);
        bool DirectoryHasAllUsersPermissions(string directory);
        void EnsureDirectoryHasAllUsersPermissions(string directory);
        bool FileHasAllUsersPermissions(string file);
        void EnsureFileHasAllUsersPermissions(string file);
        void EnsureGameDataPathExists();
        void EnsureSystemDataPathExists();
        void WipeData();
        bool TryDeleteProtected(string path, bool recurse);

        [Obsolete("Only used by Lucene db, which will be removed")]
        string AppDataPathFullDeprecated { get; }
        [Obsolete("Only used by Lucene db, which will be removed")]
        string SystemDataPathFullDeprecated { get; }

        bool HasPermissions(string path, FileSystemRights rights);
        string DescribePermissions(string path);

    }
}
