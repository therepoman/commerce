﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.IO;

namespace Amazon.Fuel.Common.Contracts
{
    // NOTE: see Support\SqliteEntityDbMigration\readme.txt when altering this object

    public class GameInstallInfo : PersistentEntry
    {
        // This is retained for rollback compatibility, and can safely be removed once this line is pushed to beta/prod
        private const string DefaultDateTime = "0001-01-01 00:00:00";

        // Config
        public override string Type => "install";
        public override string StorageContext => "GameInstallInfo";

        public string LastKnownLatestVersion { get; set; } = "";

        // NOTE: DateTime fields need special handling (see DateTime helpers below)
        public string InstallDate { get; set; } = DefaultDateTime;
        public string LastKnownLatestVersionTimestamp { get; set; } = DefaultDateTime;
        public string LastUpdated { get; set; } = DefaultDateTime;
        public string LastPlayed { get; set; } = DefaultDateTime;

        [NotMapped]
        public DateTime LastKnownLatestVersionTimestampAsDateTime
        {
            get => FromNormalizedDateTimeString(LastKnownLatestVersionTimestamp, checkEntityDateTimeFormat: false, fallback: ex => DateTime.MinValue);
            set => LastKnownLatestVersionTimestamp = ToNormalizedDateTimeString(value, fallback: ex => "");
        }

        [NotMapped]
        public DateTime LastUpdatedAsDateTime
        {
            get => FromNormalizedDateTimeString(LastUpdated, checkEntityDateTimeFormat: false, fallback: ex => DateTime.MinValue);
            set => LastUpdated = ToNormalizedDateTimeString(value, fallback: ex => "");
        }

        [NotMapped]
        public DateTime InstallDateAsDateTime
        {
            get => FromNormalizedDateTimeString(InstallDate, checkEntityDateTimeFormat: true, fallback: ex => DateTime.MinValue);
            set => InstallDate = ToNormalizedDateTimeString(value, fallback: ex => "");
        }

        [NotMapped]
        public DateTime LastPlayedAsDateTime
        {
            get => FromNormalizedDateTimeString(LastPlayed, checkEntityDateTimeFormat: true, fallback: ex => DateTime.MinValue);
            set => LastPlayed = ToNormalizedDateTimeString(value, fallback: ex => "");
        }

        //Install Status:
        public bool Installed { get; set; }
        public string InstallVersion { get; set; }
        public string InstallVersionName { get; set; }
        public string InstallDirectory { get; set; }
        public string LibraryDirectory => Directory.GetParent(InstallDirectory.TrimEnd(Path.PathSeparator)).FullName;

        public ProductId ProductId => new ProductId(Id);

        // NOTE: these may be null in older schemas
        public string ProductTitle { get; set; } = null;
        public string ProductAsin { get; set; } = null;

        #region DateTimeHelpers
        private DateTime FromNormalizedDateTimeString(string dateStr, bool checkEntityDateTimeFormat, Func<Exception, DateTime> fallback)
        {
            try
            {
                return DateTime.Parse(dateStr, null, DateTimeStyles.RoundtripKind);
            }
            catch (Exception ex)
            {
                if (checkEntityDateTimeFormat)
                {
                    // only check entity date time when we need to preserve pre-v1 DateTime formats
                    try
                    {
                        return DateTime.ParseExact(dateStr, "yyyy-MM-dd HH:mm:ss.fffffff", null);
                    }
                    catch (Exception) { }
                }

                return fallback(ex);
            }
        }

        private string ToNormalizedDateTimeString(DateTime date, Func<Exception, string> fallback)
        {
            try
            {
                return date.ToString("o", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return fallback(ex);
            }
        }
        #endregion DateTimeHelpers
    }
}