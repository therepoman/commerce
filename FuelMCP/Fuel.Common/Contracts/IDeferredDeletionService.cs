﻿
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IDeferredDeletionService
    {
        // flag a path for future deletion, and optionally delete immediately 
        // (NOTE: blocks on wait for directory movable, up to timeout)
        void FlagForDeferredDelete(string path, bool deleteNow = true);
        // unflag a path for future deletion
        bool TryUnflagForDeferredDelete(string path);

        // move paths to deleteme folder (blocking) and delete them (not blocking)
        // (NOTE: blocks on wait for directory movable, up to timeout)
        void MoveAndDeleteDeferredPaths();
        // move paths to deleteme folder and delete them in async task
        void StartMoveAndDeleteDeferredPathsAsTask();

        // Delete any _deleteMe folders for all library roots
        // (NOTE: this can get blocked on a move to deleteme directory from above call)
        void RemoveDeleteMe();
        // Delete any _deleteMe folders for all library roots in async task
        void StartRemoveDeleteMeAsTask();
    }
}
