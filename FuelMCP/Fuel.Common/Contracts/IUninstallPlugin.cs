﻿
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IUninstallPlugin
    {
        void DeleteDBDataFromDisk();
        void DeleteHashcache(ProductId productId);
        void DeleteGameFiles(ProductId productId);
        void DeleteGameFilesByManifest(Sds.Manifest manifest, string installDir);
        void DeleteUninstallerOSLink(ProductId productId);
        void DeleteGameShortcuts(ProductId productId);
        void DeleteGame(ProductId productId);
        bool IsRunning(ProductId productId);
        void RemoveUser(string user);
        bool TryEnsureUninstallerExeInCommonLocation();
        bool TryUninstallAll();
    }
}
