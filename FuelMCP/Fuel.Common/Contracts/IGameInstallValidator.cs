﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IInstalledGameValidator
    {
        bool IsGameInstallValid(string libraryDir, string gameId);
    }
}