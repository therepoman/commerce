﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IAppConfigPlugin
    {
        Serilog.Events.LogEventLevel GetMinLogLevel();
    }
}
