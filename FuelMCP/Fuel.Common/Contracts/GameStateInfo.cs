﻿using System;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Contracts
{
    public enum EGameInstallState
    {
        NotInstalled,
        InstalledAndNeedsUpdate,
        Installed
    }

    public enum EGameRunningState
    {
        NotRunning,
        // $$ jbil TODO:
        // Launching,
        Running
    }

    public class GameStateInfo : IEquatable<GameStateInfo>
    {

        public bool Installed => InstallState != EGameInstallState.NotInstalled;
        public bool IsUpToDate => Installed && InstallState != EGameInstallState.InstalledAndNeedsUpdate;
        public bool IsRunning => RunningState == EGameRunningState.Running;

        public Game GameRef;
        public GameInstallInfo GameInstallInfoRef;
        public EGameInstallState InstallState;
        public EGameRunningState RunningState;
        public bool IsDeveloperMode;

        public bool Equals(GameStateInfo other) => InstallState == other?.InstallState
                                                   && RunningState == other.RunningState
                                                   && IsDeveloperMode == other.IsDeveloperMode
                                                   && GameRef.Equals(other.GameRef)
                                                   && ( GameInstallInfoRef?.Equals(other.GameInstallInfoRef)
                                                        ?? other.GameInstallInfoRef == null );
    }
}