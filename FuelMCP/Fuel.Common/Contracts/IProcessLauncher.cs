﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Data;
using Equ;
using Amazon.Fuel.Common.CommandLineParser;

namespace Amazon.Fuel.Common.Contracts
{
    public enum ELaunchUAC
    {
        RUN_WITH_CURRENT_PRIVS__THIS_ALLOWS_ELEVATION,
        FORCE_REMOVE_UAC,
    }

    public interface IProcessLauncher
    {
        bool IsRunning(string pathToFile);
        bool IsRunning(string pathToFile, out Process outProcess);
        bool Focus(string pathToFile);
        LaunchResult LaunchProcessOrFocus(
            string description, 
            string launchFolder, 
            string pathToFile, 
            string[] args,
            Dictionary<string, string> envVars = null,
            ELaunchUAC eLaunchUac = ELaunchUAC.FORCE_REMOVE_UAC,
            bool bringToForeground = true,
            bool hideWindow = false);

        LaunchResult LaunchProcess(
            string description,
            string launchFolder,
            string pathToFile,
            string[] args,
            bool waitForExit = false,
            int[] validateAgainstReturnCodes = null,
            Dictionary<string, string> envVars = null,
            ELaunchUAC eLaunchUac = ELaunchUAC.FORCE_REMOVE_UAC,
            bool bringToForeground = true,
            bool hideWindow = false);

        LaunchResult LaunchGame(
            string sdkLocation, 
            string rootFolder, 
            LaunchConfig config, 
            string[] customArgs = null, 
            bool productArgsIgnoreAppCfg = false);
        bool LaunchLink(string url);

        Task<bool> TryWaitOnProcessMainWndAndFocusAsync(
            Process process,
            float maxWaitSec = 0.0f,
            int checkIntervalMs = 50
        );

        bool TryKillSuspendedProcesses(string pathToFile);
    }

    public class LaunchResult : MemberwiseEquatable<LaunchResult>, IDisposable
    {
        public enum EStatus
        {
            Success,
            ThrewException,
            AlreadyRunning,
            FileNotFound,
            LaunchFailure,
            ReturnCodeError,
            DeEscalateFailed,
			UacRequestRejected,
            WrongArchitecture,  // likely 64-bit on 32-bit system
        }

        public EStatus Status;
        public int ExitCode { get; set; } = -1; // -1 if run failed or waitForExit is false
        public Exception Exception {get; set;}
        public Process Process { get; set; }
        public bool DelevationFailure { get; set; } = false;    // used to emit a pMet metric on failure (which will 

        public void Dispose()
        {
            Process?.Dispose();
            Process = null;
        }
    }

    public interface ILaunchAppArgsLauncher
    {
        LaunchResult LaunchUAC(TwitchInstallHelperArg args);
    }
}
