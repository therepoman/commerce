﻿using System;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IMetricScope : IDisposable
    {
        void IncrementCounter(string name);
        void SetCounterValue(string name, int value);
        void AddCounterValue(string name, int value);
        IMetricTimerDataPoint StartTimer(string name);
    }
}
