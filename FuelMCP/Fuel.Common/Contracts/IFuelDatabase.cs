﻿using System.Collections.Generic;
using Lucene.Net.Index;
using Lucene.Net.Search;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IFuelDatabase<TEntry> where TEntry : PersistentEntry, new()
    {
        List<TEntry> All { get; }

        void RemoveAll();

        void RemoveManyById(IEnumerable<string> items);

        void AddOrReplaceById(TEntry entry);

        TEntry TryFindById(string id);

        void Remove(TEntry entry);
        void RemoveById(string id);

        void ReplaceAll(IEnumerable<TEntry> entry);
    }
}
