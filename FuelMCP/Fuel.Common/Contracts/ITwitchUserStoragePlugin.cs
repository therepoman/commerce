﻿using Equ;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Amazon.Fuel.Common.Contracts
{
    public interface ITwitchUserStoragePlugin
    {
        /// <summary>
        /// Get or set the Twitch user
        /// </summary>
        LocalTwitchUser User { get; set; }

        /// <summary>
        /// Removes the Twitch user from storage
        /// </summary>
        void Remove();

        TwitchCredentialSet GetTwitchCredentialSet(string clientId);
        void SetTwitchCredentialSet(string clientId, TwitchCredentialSet credentials);
    }

    public class LocalTwitchUser : MemberwiseEquatable<LocalTwitchUser>
    {
        /// <summary>
        /// Curse session ID.
        /// </summary>
        public string SessionUserId { get; set; }
        /// <summary>
        /// An OAuth 2.0 Access token for the Twitch user.
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// The Twitch user's avatar (logo)
        /// </summary>
        public string Logo { get; set; }
        /// <summary>
        /// The twitch username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// The Twitch display name
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// The Twitch user ID.
        /// </summary>
        public string Tuid { get; set; }
    }
    
    public class TwitchCredentialSet : MemberwiseEquatable<TwitchCredentialSet>
    {
        public string AccessToken { get; set; }
        public DateTime Created { get; set; }
        public int Pid { get; set; }
        public string Entitlement { get; set; }
        public IEnumerable<string> Scopes { get; set; }
    }
}
