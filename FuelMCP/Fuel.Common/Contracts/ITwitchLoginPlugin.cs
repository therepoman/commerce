﻿using System;
using System.Collections.Generic;

namespace Amazon.Fuel.Common.Contracts
{
    public interface ITwitchTokenProvider
    {
        string GetAndValidateAccessToken();

        string GetAccessToken();
    }

    public interface ITwitchLoginPlugin : ITwitchTokenProvider
    {
        void EnsureLoggedIn();

        void InvalidateLogin();
        void InvalidateAccessToken(bool revoke = false);

        string GetTuid();

        bool IsLoggedIn(string optionalCurseUserId = null);
        bool UserHasAccessToken();

        void SetAuthTokens(string accessToken, bool allowRefreshUserData = true);
        string GetGameToken(string clientid, IEnumerable<String> scopes);
    }
}
