﻿
using System;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IMetricTimerDataPoint : IDisposable
    {
        void Stop();
    }
}
