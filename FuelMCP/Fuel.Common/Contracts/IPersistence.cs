﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IPersistence
    {
        string DataRoot();
    }
}
