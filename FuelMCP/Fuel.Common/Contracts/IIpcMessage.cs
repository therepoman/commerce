﻿
using Amazon.Fuel.Common.IPC;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IIpcMessage
    {
        byte[] GetBytes();

        InstallHelperSession Session { get; set; }
    }
}
