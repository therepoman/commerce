﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IRetryPolicy
    {
        void IncrementRetries();
        bool WantRetry { get; }
    }

}
