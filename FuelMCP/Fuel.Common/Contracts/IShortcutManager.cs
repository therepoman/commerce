﻿
using System.Collections.Generic;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IShortcutManager
    {
        string[] GetShortcutLocationsByTitle(string gameProductTitle);
        bool TryCreateShortcuts(GameProductInfo game, GameInstallInfo installInfo, bool includeDesktop);
        void RemoveShortcuts(GameInstallInfo game);
    }
}
