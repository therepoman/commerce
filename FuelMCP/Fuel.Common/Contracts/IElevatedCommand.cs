using System.Threading.Tasks;
using Amazon.Fuel.Common.Messages;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IElevatedCommand<TResponse>
    {
        int TimeoutMS { get; set; }

        Task<TResponse> Execute();
        void Abort();
    }
}