﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IMessageHubListener
    {
        void AttachToHub(IMessengerHub hub);
    }
}