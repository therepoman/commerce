﻿using Amazon.Fuel.Common.CommandLineParser;

namespace Amazon.Fuel.Common.Contracts
{
    public enum ShortcutLocation
    {
        ShortcutFolder,
        Desktop,
        Temp,
        Twitch,
    }

    public interface IShortcutDriver
    {
        string GetShortcutFileName(ShortcutLocation location, string shortcutBaseName);

        string CreateShortcut(
            ShortcutLocation location,
            string shortcutBaseName,
            string targetFile, 
            string description, 
            string arguments = null, 
            string iconLocation = null, 
            int iconOffset = 0);

        string TryCreateUrl(
            ShortcutLocation location,
            string shortcutBaseName,
            string targetUrl,
            string description,
            string iconLocation = null,
            int iconOffset = 0);

        void RemoveShortcut(string shortcutPath);
    }
}
