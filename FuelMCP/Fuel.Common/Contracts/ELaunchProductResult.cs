﻿namespace Amazon.Fuel.Common.Contracts
{
    public enum ELaunchProductResult
    {
        Succeeded = 0,
        Cancelled,
        Failed,

        InstallationOrUpdateInProgress,

        NeedsUpdate,                            // product version out of date
        NotEntitled,                            // entitlement revoked by server?
        NotInstalled,

        NotLoggedIn,

        VersionManifestDownloadFailed,          // this happens if we can't get the version trying to check for updates

        ConfigFileMissing,                      // config file absent fron install, likely need reinstallation/repair
        InvalidConfigFile,                      // config file malformed, likely need reinstallation/repair

        ProcessLaunchFailed,
        ProcessLaunchFailedAnotherInProgress,
        ProcessLaunchFailedAlreadyRunning,
        ProcessLaunchFailedWrongArchitecture,
        ProcessLaunchFailedTokenExchangeFailure,

        NetworkFailure,
        FileNotFound
    }

    public static class LaunchProductResultExt
    {
        public static bool IsSuccess(this ELaunchProductResult thisResult)
        {
            return thisResult == ELaunchProductResult.Succeeded;
        }

        public static bool IsNetworkedFailure(this ELaunchProductResult thisResult)
        {
            return thisResult == ELaunchProductResult.NetworkFailure;
        }

        public static bool WantLogAsFailure(this ELaunchProductResult thisResult)
        {
            switch (thisResult)
            {
                case ELaunchProductResult.Failed:
                case ELaunchProductResult.VersionManifestDownloadFailed:
                case ELaunchProductResult.ConfigFileMissing:
                case ELaunchProductResult.InvalidConfigFile:
                case ELaunchProductResult.FileNotFound:
                case ELaunchProductResult.ProcessLaunchFailed:
                case ELaunchProductResult.ProcessLaunchFailedTokenExchangeFailure:
                    return true;
            }

            return false;
        }

        public static bool WantLogAsWarning(this ELaunchProductResult thisResult)
        {
            switch (thisResult)
            {
                case ELaunchProductResult.InstallationOrUpdateInProgress:
                case ELaunchProductResult.NotEntitled:
                case ELaunchProductResult.NotLoggedIn:
                case ELaunchProductResult.NotInstalled:
                case ELaunchProductResult.ProcessLaunchFailedAnotherInProgress:
                case ELaunchProductResult.ProcessLaunchFailedAlreadyRunning:
                    return true;
            }

            return false;
        }

        public static bool IsUnexpectedFailure(ELaunchProductResult result)
        {
            switch (result)
            {
                case ELaunchProductResult.Succeeded:
                case ELaunchProductResult.Cancelled:
                case ELaunchProductResult.InstallationOrUpdateInProgress:
                case ELaunchProductResult.NeedsUpdate:
                case ELaunchProductResult.NetworkFailure:
                case ELaunchProductResult.ProcessLaunchFailedWrongArchitecture:
                case ELaunchProductResult.ProcessLaunchFailedAlreadyRunning:
                case ELaunchProductResult.ProcessLaunchFailedAnotherInProgress:
                case ELaunchProductResult.NotEntitled:
                    return false;
                default:
                    return true;
            }
        }
    }
}