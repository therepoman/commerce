﻿using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Contracts
{
    public class FuelTokenResponse
    {
        public FuelTokenResponse(bool success, string accessToken= null)
        {
            Success = success;

            AccessToken = accessToken;
        }

        public bool Success { get; }

        public string AccessToken { get; }
    }

    public interface IFuelRefreshTokenProvider
    {
        FuelTokenResponse TryRefreshFuelTokens();
    }
}
