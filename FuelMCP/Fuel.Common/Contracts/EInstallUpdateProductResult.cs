namespace Amazon.Fuel.Common.Contracts
{
    // NOTE: update InstallUpdateProductResultHelpers below when modifying this enum
    public enum EInstallUpdateProductResult
    {
        Succeeded = 0,

        InvalidProductId,

        Cancelled,
        GenericFailure,
        GenericInstallOrUpdateProductException,
        GenericInstallOrUpdateProductWithSleepProtectException,
        GenericInstallOrUpdateGameCoreException,
        IpcFailure,

        Busy,

        NotEntitled,
        PostInstallFailure,
        ConfigLoadFailure,

        CreateDirFailure,
        InstallInfoNotFoundInDb,

        DownloadFailure,
        VersionManifestDownloadFailed,
        NotEnoughDiskSpace,

        AuthFailure,

        GenericUacHelperAppFailure,
        UacDenial,

        NetworkFailure,

        AlreadyRunning,
        LockedFiles,
    }

    public static class InstallUpdateProductResultHelpers
    {
        public static ERepairProductResult ToRepairProductResult(this EInstallUpdateProductResult result)
        {
            switch (result)
            {
                case EInstallUpdateProductResult.Succeeded:                                              return ERepairProductResult.Succeeded;
                case EInstallUpdateProductResult.InvalidProductId:                                       return ERepairProductResult.InvalidProductId;
                case EInstallUpdateProductResult.Cancelled:                                              return ERepairProductResult.Cancelled;
                case EInstallUpdateProductResult.GenericFailure:                                         return ERepairProductResult.GenericFailure;
                case EInstallUpdateProductResult.GenericInstallOrUpdateProductException:                 return ERepairProductResult.GenericInstallOrUpdateProductException;
                case EInstallUpdateProductResult.GenericInstallOrUpdateProductWithSleepProtectException: return ERepairProductResult.GenericInstallOrUpdateProductWithSleepProtectException;
                case EInstallUpdateProductResult.GenericInstallOrUpdateGameCoreException:                return ERepairProductResult.GenericInstallOrUpdateGameCoreException;
                case EInstallUpdateProductResult.IpcFailure:                                             return ERepairProductResult.IpcFailure;
                case EInstallUpdateProductResult.Busy:                                                   return ERepairProductResult.Busy;
                case EInstallUpdateProductResult.NotEntitled:                                            return ERepairProductResult.NotEntitled;
                case EInstallUpdateProductResult.PostInstallFailure:                                     return ERepairProductResult.PostInstallFailure;
                case EInstallUpdateProductResult.ConfigLoadFailure:                                      return ERepairProductResult.ConfigLoadFailure;
                case EInstallUpdateProductResult.CreateDirFailure:                                       return ERepairProductResult.CreateDirFailure;
                case EInstallUpdateProductResult.InstallInfoNotFoundInDb:                                return ERepairProductResult.InstallInfoNotFoundInDb;
                case EInstallUpdateProductResult.DownloadFailure:                                        return ERepairProductResult.DownloadFailure;
                case EInstallUpdateProductResult.VersionManifestDownloadFailed:                          return ERepairProductResult.VersionManifestDownloadFailed;
                case EInstallUpdateProductResult.NotEnoughDiskSpace:                                     return ERepairProductResult.NotEnoughDiskSpace;
                case EInstallUpdateProductResult.AuthFailure:                                            return ERepairProductResult.AuthFailure;
                case EInstallUpdateProductResult.GenericUacHelperAppFailure:                             return ERepairProductResult.GenericUacHelperAppFailure;
                case EInstallUpdateProductResult.UacDenial:                                              return ERepairProductResult.UacDenial;
                case EInstallUpdateProductResult.NetworkFailure:                                         return ERepairProductResult.NetworkFailure;
                case EInstallUpdateProductResult.AlreadyRunning:                                         return ERepairProductResult.AlreadyRunning;
                case EInstallUpdateProductResult.LockedFiles:                                            return ERepairProductResult.LockedFiles;
                default:
                    return ERepairProductResult.GenericFailure;
            }
        }

        public static bool IsUnexpectedFailure(EInstallUpdateProductResult result)
        {
            switch (result)
            {
                case EInstallUpdateProductResult.Succeeded:
                case EInstallUpdateProductResult.Cancelled:
                case EInstallUpdateProductResult.Busy:
                case EInstallUpdateProductResult.NotEntitled:
                case EInstallUpdateProductResult.NotEnoughDiskSpace:
                case EInstallUpdateProductResult.UacDenial:
                case EInstallUpdateProductResult.NetworkFailure:
                case EInstallUpdateProductResult.AuthFailure:
                    return false;
                default:
                    return true;
            }
        }
    }
}