﻿using System;
using Equ;
using System.Collections.Generic;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Contracts
{
    [Obsolete("Lucene will be removed")]
    public class GameInstallInfoLuceneDeprecated : PersistentEntry
    {
        public const string NOT_CHECKED_VERSION = "<not_checked>";

        public DateTime LastKnownLatestVersionTimestamp { get; set; } = DateTime.MinValue;
        public string LastKnownLatestVersion { get; set; } = NOT_CHECKED_VERSION;
        public DateTime LastUpdated { get; set; } = DateTime.MinValue;

        // Config
        public override string Type => "install";
        public override string StorageContext => "GameInstallInfo";

        //Install Status:
        public bool Installed { get; set; }
        public DateTime InstallDate { get; set; }
        public string InstallVersion { get; set; }
        public string InstallVersionName { get; set; }
        public string InstallDirectory { get; set; }
        public string LibraryDirectory => System.IO.Directory.GetParent(InstallDirectory).FullName;
        public DateTime LastPlayed { get; set; }

        public ProductId ProductId
        {
            get { return new ProductId(Id); }
        }

        // NOTE: this may be null in older schemas
        public string ProductTitle { get; set; } = null;

        public List<string> ShortcutLocations { get; set; }

        public GameInstallInfo ToGameInstallInfo()
            => new GameInstallInfo()
            {
                Id = this.Id,
                LastKnownLatestVersionTimestampAsDateTime = this.LastKnownLatestVersionTimestamp,
                LastKnownLatestVersion = this.LastKnownLatestVersion,
                LastUpdatedAsDateTime = this.LastUpdated,

                Installed = this.Installed,
                // https://jira.twitch.com/browse/FLCL-1979 non Gregorian calendars break Entity
                //InstallDate = this.InstallDate,
                InstallVersion = this.InstallVersion,
                InstallVersionName = this.InstallVersionName,
                InstallDirectory = this.InstallDirectory,
                ProductTitle = this.ProductTitle,
                ProductAsin = null
            };
    }

    [Obsolete("Lucene will be removed")]
    public class GameProductInfoLuceneDeprecated : PersistentEntry
    {
        public bool IsDeveloper { get; set; }
        public DateTime DateTime { get; set; }

        // Config
        public override string Type => "entitlement";
        public override string StorageContext => "GameProductInfo";

        // Const
        public static readonly string STATE_LIVE = "LIVE";

        //Catalog Data:
        public string ProductTitle { get; set; }
        public string ProductAsin { get; set; }
        public string ProductAsinVersion { get; set; }
        public ProductIdLuceneDeprecated ProductId { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDomain { get; set; }
        public ProductDetailsLuceneDeprecated ProductDetails { get; set; }
        public string ProductLine { get; set; }
        public string ProductSku { get; set; }
        public string ProductPublisher { get; set; }
        public string State { get; set; }
    }

    [Obsolete("Lucene will be removed")]
    public class ProductDetailsLuceneDeprecated : MemberwiseEquatable<ProductDetailsLuceneDeprecated>
    {
        public string ProductIconUrl { get; set; }
    }

    [Obsolete("Lucene will be removed")]
    public class ProductIdLuceneDeprecated : MemberwiseEquatable<ProductIdLuceneDeprecated>
    {
        public ProductIdLuceneDeprecated(string id) { this.Id = id; }
        public string Id { get; }

        public override String ToString()
        {
            return $"ProductId[{Id}]";
        }
    }

    [Obsolete("Lucene will be removed")]
    public class EntitlementIdLuceneDeprecated : MemberwiseEquatable<EntitlementIdLuceneDeprecated>
    {
        public EntitlementIdLuceneDeprecated(string id) { this.Id = id; }
        public string Id { get; }

        public override String ToString()
        {
            return $"EntitlementId[{Id}]";
        }
    }

    [Obsolete("Lucene will be removed")]
    public class GameLuceneDeprecated : MemberwiseEquatable<GameLuceneDeprecated>
    {
        public readonly string Title;
        public readonly ProductIdLuceneDeprecated ProductId;
        public readonly string ProductSku;
        public readonly string EntitlementId;
        public readonly string ProductIconUri;
        public readonly bool IsDeveloper;

        public GameLuceneDeprecated(
            string title,
            ProductIdLuceneDeprecated productId,
            string productSku,
            string entitlementId,
            string productIconUri,
            bool isDeveloper)
        {
            this.Title = title;
            this.ProductId = productId;
            this.ProductSku = productSku;
            this.EntitlementId = entitlementId;
            this.ProductIconUri = productIconUri;
            this.IsDeveloper = isDeveloper;
        }

        public string ToFullId() => $"<eid:{EntitlementId}/pid:{ProductId}/title:{Title}/sku:{ProductSku}>";

        public override string ToString()
        {
            return Title;
        }
    }
}