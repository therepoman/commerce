using System.Collections.Generic;
using Amazon.Fuel.Common.Messages;

namespace Amazon.Fuel.Common.Contracts
{
    public class GameInstallInstancesRefreshed
    {
        public GameInstallInstancesRefreshed(
            Dictionary<ProductId, GameStateInfo> games,
            ELibraryRefreshType refreshType,
            bool changeDetected)
        {
            Games = games;
            RefreshType = refreshType;
            ChangeDetected = changeDetected;
        }

        public bool ChangeDetected { get; private set; } = false;
        public ELibraryRefreshType RefreshType { get; private set; }
        public Dictionary<ProductId, GameStateInfo> Games { get; private set; }
    }
}