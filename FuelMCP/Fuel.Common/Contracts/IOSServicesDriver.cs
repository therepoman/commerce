﻿
using System.Security.Principal;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IOSServicesDriver
    {
        void PreventSleep();
        void AllowSleep();
        SecurityIdentifier UseridForUsername(string username); // gaffo: yes this is windows specific, need to come up with a platform inspecific way when porting
        string CurrentUser { get; }
    }
}
