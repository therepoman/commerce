﻿using System.Threading;
using System.Threading.Tasks;
using Clients.Twitch.Spade.Model.Event;

namespace Amazon.Fuel.Common.Contracts
{
    public interface ILauncherServices 
    {
        Task<GameProductInfo> TryGetCachedEntitlementAsync(
            ProductId pid,
            bool updateCacheOnMiss = true,
            bool updateCacheOnHit = false);

        Task<bool> CheckCachedEntitledAsync(
            ProductId pid, 
            bool updateCacheOnMiss = true,
            bool updateCacheOnHit = false);

        Task<ELaunchProductResult> LaunchProduct(
            ProductId productId,
            string[] productArgs,
            bool ignoreAppCfgArgs,
            bool checkForUpdates,
            string developerDir = null);

        Game ValidateEntitlement(ProductId productId);

        bool IsRunning(ProductId productId);

        void SendFuelLauncherActionEvent(
            ProductId adgProductId, 
            string asin, 
            string sdsVersionId,
            FuelLauncherActionType actionType,
            string transactionId);

        void SendFuelLauncherActionErrorEvent(
            ProductId adgProductId, 
            string asin, 
            string sdsVersionId,
            string errorReason, 
            FuelLauncherErrorType errorType,
            string transactionId);

        /// <summary>
        /// Given an adg product id, this will retrieve the entitlement id, the active version, and the manifest to determine disk space requirements
        /// </summary>
        /// <returns></returns>
        bool CanInstallGameToPrimaryDrive(ProductId adgProdId);
    }

    public interface IFuelGameInstallerService
    {
        EInstallUpdateProductResult InstallOrUpdateProduct(
            ProductId productId,
            string installLibraryDirectory,
            bool isUpdate,
            bool installDesktopShortcut,
            CancellationToken cancellationToken = default(CancellationToken));

        ERepairProductResult RepairProduct(
            ProductId productId,
            bool deleteAllFiles,
            bool createDesktopShortcut,
            CancellationToken cancellationToken = default(CancellationToken));

        bool IsInstallInProgress { get; }
        ProductId InstallInProgressProdId { get; }
    }

    public class InstallUpdateProductResult
    {
        public EInstallUpdateProductResult Result { get; set; }
        public string DownloadedVersionId { get; set; }
    }
}
