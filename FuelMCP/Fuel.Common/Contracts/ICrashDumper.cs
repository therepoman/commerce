﻿using System;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IUncaughtExceptionHandler
    {
        void InvokeExternalUnhandledException(object sender, Exception ex);
    }

    public class NullCrashHandler : IUncaughtExceptionHandler
    {
        public void InvokeExternalUnhandledException(object sender, Exception ex) { }
    }

}