﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Contracts
{
    public class OperationResponse
    {
        public enum EResult
        {
            Success,
            FailedWithException,
            UnknownResponseFailure,
            ResponseEmpty
        }

        public string ResponseData { get; set; } = null;
        public EResult Result { get; set; } = EResult.Success;
        public Exception Exception { get; set; } = null;
        public System.Net.HttpStatusCode? StatusCode { get; set; } = null;
    }

    public class SyncGoodsResult
    {
        public enum EResult
        {
            Success,
            NetworkFailure,
            UnknownFailure,
            ParsingFailure
        }

        public EResult Result { get; set; }
        public OperationResponse OpResult { get; set; }
        public Exception CaughtException { get; set; } = null;
        public List<GameProductInfo> ProductInfos { get; set; } = new List<GameProductInfo>();
    }

    public interface IEntitlementService
    {
        Task<SyncGoodsResult> SyncGoods(CancellationToken? cancellationToken = null);
        Task<SyncGoodsResult> SyncGoodsWithRetry(CancellationToken? cancellationToken = null);
    }
}
