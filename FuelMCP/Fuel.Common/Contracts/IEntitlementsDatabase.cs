﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IEntitlementsDatabase : IFuelDatabase<GameProductInfo>
    {
        List<GameProductInfo> ActiveEntitlements { get; }
        Task<bool> SyncGoods();
        GameProductInfo Get(ProductId productId);
    }
}
