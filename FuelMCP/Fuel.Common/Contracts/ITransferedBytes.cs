﻿
namespace Amazon.Fuel.Common.Contracts
{
    public interface ITransferedBytes
    {
        long ReceivedBytes { get;  }
    }
}
