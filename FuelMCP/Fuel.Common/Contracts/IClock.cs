﻿using System;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IClock
    {
        DateTime Now();
    }
}
