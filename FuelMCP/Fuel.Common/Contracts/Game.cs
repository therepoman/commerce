﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Equ;

namespace Amazon.Fuel.Common.Contracts
{
    public class Game : MemberwiseEquatable<Game>
    {
        public readonly string Title;
        public readonly ProductId ProductId;
        public readonly string ProductSku;
        public readonly string EntitlementId;
        public readonly string ProductIconUri;
        public readonly bool IsDeveloper;
        public readonly IList<string> ScreenshotUrls;
        public readonly IList<string> VideoUrls;
        public readonly string BackgroundUrl;
        public readonly string BackgroundUrl2;

        public Game(
            string title,
            ProductId productId,
            string productSku,
            string entitlementId,
            string productIconUri,
            bool isDeveloper,
            IList<string> screenshotUrls,
            IList<string> videoUrls,
            string backgroundUrl,
            string backgroundUrl2)
        {
            this.Title = title;
            this.ProductId = productId;
            this.ProductSku = productSku;
            this.EntitlementId = entitlementId;
            this.ProductIconUri = productIconUri;
            this.IsDeveloper = isDeveloper;
            this.ScreenshotUrls = screenshotUrls;
            this.VideoUrls = videoUrls;
            this.BackgroundUrl = backgroundUrl;
            this.BackgroundUrl2 = backgroundUrl2;
        }

        public string ToFullId() => $"<eid:{EntitlementId}/pid:{ProductId}/title:{Title}/sku:{ProductSku}>";

        public override string ToString()
        {
            return Title;
        }
    }
}