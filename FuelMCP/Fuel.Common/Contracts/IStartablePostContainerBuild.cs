﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IStartablePostContainerBuild
    {
        void StartPostContainerBuild();
    }
}