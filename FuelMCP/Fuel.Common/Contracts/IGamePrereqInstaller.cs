using Amazon.Fuel.Common.Data;
using System.Collections.Generic;
using System.Threading;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IGamePrereqInstaller
    {
        bool TryInstallPrereqs(List<PrereqExecutableWithSha> prereqs, string installDirectory, CancellationToken cancelToken = default(CancellationToken));

        void ClearPrereqHasRun(Executable exe, string installDir);

        /// <summary>
        /// Filter out any prereqs that have already run for this install dir, by file sha and arg pair (also mindful of AlwaysRun flag)
        /// </summary>
        /// <param name="prereqs">all prereqs for a game install, paired with expected sha</param>
        /// <param name="installDirectory">full path for game installation</param>
        /// <returns></returns>
        List<PrereqExecutableWithSha> FilterPrereqs(List<PrereqExecutableWithSha> prereqs, string installDirectory);
    }
}