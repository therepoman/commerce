using System.Collections.Generic;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IGameInstallInfoDatabase : IFuelDatabase<GameInstallInfo>
    {
        string GetSqlDbFile();
        List<GameInstallInfo> AwaitingCleanup { get; }
    }
}