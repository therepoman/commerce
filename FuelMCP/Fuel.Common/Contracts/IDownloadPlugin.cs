﻿using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts.Manifest;
using System.Threading;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Contracts
{
    public enum EDownloadResult
    {
        Success = 0,

        // general errors
        MissingUrl,
        ManifestDownloadFailed,

        // source errors
        ExpiredFileEncountered,
        SocketCommFailure,
        DownloadFailed,
        InvalidSourceName,

        // target errors
        TargetNameInvalid,
        TargetSecurityInvalid,
        TargetPathTooLong,
        TargetPathCreateFail,

        // recoverable
        HashVerifyFailure,
        PatchFailure,
        UnknownIORecoverable,
        UnknownWebRecoverable,
        UnknownRecoverable,

        // cancellation
        Cancel,

        // misc
        NotEnoughDiskSpace,
        LockedFile,
    }

    public enum EDownloadJobResult
    {
        Success,
        Cancel,
        NotRun,
        DownloadManifestRefreshFailure,
        ExpiredFileEncountered,
        DownloadTimeoutEncountered,
        AuthFailure,
        GeneralFailure,
        NotEnoughDiskSpace,
        NetworkFailure,
        LockedFiles
    }

    public static class EDownloadResultExt
    {
        public static bool WantsDownloadFileRetry(this EDownloadResult thisResult)
        {		
            switch (thisResult)		
            {		
                case EDownloadResult.SocketCommFailure:		
                case EDownloadResult.DownloadFailed:		
                case EDownloadResult.InvalidSourceName:		
                case EDownloadResult.HashVerifyFailure:		
                case EDownloadResult.UnknownRecoverable:		
                case EDownloadResult.UnknownIORecoverable:		
                case EDownloadResult.UnknownWebRecoverable:
                case EDownloadResult.ExpiredFileEncountered:
                    return true;		
            }		
    		
            return false;		
        }

        public static bool IsFailure(this EDownloadResult thisResult)
        {
            switch (thisResult)
            {
                case EDownloadResult.Cancel:
                case EDownloadResult.Success:
                    return false;
            }

            return true;
        }

        public static bool WantsJobRetry(this EDownloadResult thisResult)
        {
            switch (thisResult)
            {
                case EDownloadResult.SocketCommFailure:
                case EDownloadResult.DownloadFailed:
                case EDownloadResult.HashVerifyFailure:
                case EDownloadResult.UnknownRecoverable:
                case EDownloadResult.UnknownIORecoverable:
                case EDownloadResult.UnknownWebRecoverable:
                    // NOTE: in the pipeline, patches are applied post download, so we need to restart the job to reapply
                case EDownloadResult.PatchFailure:
                case EDownloadResult.ExpiredFileEncountered:
                case EDownloadResult.ManifestDownloadFailed:
                    return true;
            }

            return false;
        }
    }

    public enum EDownloadSource
    {
        Primary,
        Secondary,
    }


    public class DownloadProductResult
    {
        public EDownloadJobResult Result { get; set; }
        public ManifestWithVersion ManifestWithVersion { get; set; }

        public bool IsUnexpectedFailure()
        {
            switch (Result)
            {
                case EDownloadJobResult.Success:
                case EDownloadJobResult.Cancel:
                case EDownloadJobResult.NetworkFailure:
                case EDownloadJobResult.NotEnoughDiskSpace:
                    return false;
                default:
                    return true;
            }
        }
    }

    public interface IDownloadPlugin
    {
        Task<DownloadProductResult> DownloadProductWithRetryASync(
            ProductId productId,
            string entitlementId,
            string folder,
            int[] retryWaitSec,
            bool isUpdate,
            Sds.Manifest previousManifest = null,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}
