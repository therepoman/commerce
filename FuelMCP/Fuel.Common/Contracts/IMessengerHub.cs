﻿using System;
using TinyMessenger;

namespace Amazon.Fuel.Common.Contracts
{
    // This interface is an abstraction over TinyMessengerHub and does not
    // implement all available methods
    public interface IMessengerHub
    {
        IDisposable Subscribe<TMessage>(Action<TMessage> deliveryAction) where TMessage : class, ITinyMessage;
        void Unsubscribe<TMessage>(IDisposable subscriptionToken) where TMessage : class, ITinyMessage;
        void Publish<TMessage>(TMessage message) where TMessage : class, ITinyMessage;
        void PublishAsync<TMessage>(TMessage message) where TMessage : class, ITinyMessage;
        void PublishAsync<TMessage>(TMessage message, AsyncCallback callback) where TMessage : class, ITinyMessage;
    }
}
