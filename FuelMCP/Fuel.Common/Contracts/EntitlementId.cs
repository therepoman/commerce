﻿using System;
using Equ;

namespace Amazon.Fuel.Common.Contracts
{
    public class EntitlementId : MemberwiseEquatable<EntitlementId>
    {
        public EntitlementId(string id) { this.Id = id; }
        public string Id { get; }

        public override String ToString()
        {
            return $"EntitlementId[{Id}]";
        }
    }
}