﻿using System.IO;
using System.Threading.Tasks;
using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IManifestStorage
    {
        /// <summary>
        /// Persists a manifest to disk without deserializing first. If there is already a file stored at this ID it will be overwritten.
        /// Optionally verify the file after storage by reading and deserializing to memory.
        /// </summary>
        /// <param name="adgProdId">an adg product id</param>
        /// <param name="versionId">an sds version id</param>
        /// <param name="stream">the stream to write to disk</param>
        /// <param name="verifyAfterStorage">ensure that the stored manifest can be deserialized</param>
        /// <returns>true if the manifest was persisted successfully, false otherwise</returns>
        Task<bool> PutRaw(ProductId adgProdId, string versionId, Stream stream, bool verifyAfterStorage);

        /// <summary>
        /// Deletes a manifest on disk. This can be used if it is no longer needed, e.g. after an uninstall or update finishes.
        /// </summary>
        /// <param name="adgProdId">an adg product id</param>
        /// <param name="versionId">an sds version id</param>
        /// <returns>true if it was successfully deleted, false otherwise. If the manifest does not exist it will return true.</returns>
        bool Delete(ProductId adgProdId, string versionId);

        /// <summary>
        /// Retrieves a manifest from disk by the version id it corresponds to.
        /// </summary>
        /// <param name="adgProdId">an adg product id</param>
        /// <param name="versionId">an sds version id</param>
        /// <returns>the manifest, or null if none could be found corresponding to the given version id</returns>
        Task<Sds.Manifest> Get(ProductId adgProdId, string versionId);

        /// <summary>
        /// Returns the path to the manifest for a particular ADG product ID and SDS version ID.
        /// </summary>
        /// <param name="adgProdId"> the ADG product ID</param>
        /// <param name="versionId">the SDS version ID</param>
        /// <returns></returns>
        string GetFilePath(ProductId adgProdId, string versionId);
    }
}