﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface ILoggerBaseConfig
    {
        void FlushAndRecreateLogger();
    }
}
