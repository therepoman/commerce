﻿using Amazon.Fuel.Common.Contracts.Config;
using Amazon.Fuel.Common.Data;

namespace Amazon.Fuel.Common.Contracts
{
    public interface ILaunchConfigProvider
    {
        string GetLaunchConfigPath(string installFolder, string file = Constants.Paths.FuelGameConfigFileName);
        LaunchConfig GetLaunchConfig(string installFolder, string file = Constants.Paths.FuelGameConfigFileName);        
    }
}
