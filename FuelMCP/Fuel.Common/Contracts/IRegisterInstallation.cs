﻿namespace Amazon.Fuel.Common.Contracts
{
    public interface IRegisterInstallation
    {
        bool RegisterUninstaller(
            ProductId productId, 
            GameProductInfo productInfo, 
            GameInstallInfo installInfo,
            long estimatedSize,
            string iconLocation, 
            int iconOffset = 0);
        bool UnregisterUninstaller(ProductId productId);
    }
}
