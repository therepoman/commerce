﻿using Serilog.Configuration;

namespace Amazon.Fuel.Common.Contracts
{
    public enum ELogUploadMode
    {
        UploadAndLog,
        LogOnly,
    }

    public interface ICrashLogger
    {
        void Config(LoggerSinkConfiguration writeTo);
    }

    public interface IStreamingLogger
    {
        void Config(LoggerSinkConfiguration configuration); 
    }
}
