namespace Amazon.Fuel.Common.Contracts
{
    public enum ERepairProductResult
    {
        Succeeded = 0,

        InvalidProductId,

        Cancelled,
        GenericFailure,
        GenericInstallOrUpdateProductException,
        GenericInstallOrUpdateProductWithSleepProtectException,
        GenericInstallOrUpdateGameCoreException,
        IpcFailure,

        Busy,

        NotInstalled,
        NotEntitled,
        PostInstallFailure,
        ConfigLoadFailure,

        CreateDirFailure,
        InstallInfoNotFoundInDb,

        DownloadFailure,
        VersionManifestDownloadFailed,
        NotEnoughDiskSpace,

        AuthFailure,

        GenericUacHelperAppFailure,
        UacDenial,

        NetworkFailure,

        AlreadyRunning,
        LockedFiles,

        GenericFailureException
    }

    public static class RepairProductResultHelpers
    {
        public static bool IsUnexpectedFailure(ERepairProductResult result)
        {
            switch (result)
            {
                case ERepairProductResult.Succeeded:
                case ERepairProductResult.Cancelled:
                case ERepairProductResult.Busy:
                case ERepairProductResult.NotEntitled:
                case ERepairProductResult.NotEnoughDiskSpace:
                case ERepairProductResult.UacDenial:
                case ERepairProductResult.NetworkFailure:
                case ERepairProductResult.AuthFailure:
                    return false;
                default:
                    return true;
            }
        }
    }
}