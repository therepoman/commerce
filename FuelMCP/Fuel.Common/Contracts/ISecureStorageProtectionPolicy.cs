﻿using System;

namespace Amazon.Fuel.Common.Contracts
{
    public interface ISecureStorageProtectionPolicy
    {
        byte[] ProtectBytes(Byte[] unprotectedBytes);
        byte[] UnprotectBytes(Byte[] protectedBytes);
    }
}