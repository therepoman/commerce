﻿namespace Amazon.Fuel.Common
{
    public enum ETwitchInstallHelperResult
    {
        Succeeded,

        Failure_Timeout,
        Failure_UnknownException,
        Failure_DependencyFailed,
        Failure_InvalidArgumentsPassed,
        Failure_IpcInit,
        Failure_IpcRead,
    }
}