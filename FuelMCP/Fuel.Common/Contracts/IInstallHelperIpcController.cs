using System;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.IPC;

namespace Amazon.Fuel.Common.Contracts
{
    public class SendAndWaitResponse<THelperMessage>
        where THelperMessage : InstallHelperIpcMessage
    {
        public enum EResult
        {
            Succeeded,

            Failure_LostConnection,
            Failure_Timeout,
        }

        public bool Succeeded => Result == EResult.Succeeded;
        public THelperMessage Message { get; set; }
        public EResult Result { get; set; } = EResult.Succeeded;
    }

    public interface IInstallHelperIpcController
    {
        InstallHelperSession Start(IMessengerHub hub, CancellationToken cancelToken);
        void Stop(InstallHelperSession session, bool sendTerminationMessage);
        void Restart(InstallHelperSession session);

        bool Send(InstallHelperSession session, ControllerIpcMessage msg);
        Task<SendAndWaitResponse<THelperMessage>> SendAndWaitForNextAsync<THelperMessage>(
                InstallHelperSession session, 
                ControllerIpcMessage sendMessage,
                TimeSpan timeout = default(TimeSpan))
            where THelperMessage : InstallHelperIpcMessage;
    }
}