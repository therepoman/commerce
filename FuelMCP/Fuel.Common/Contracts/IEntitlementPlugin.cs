﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IEntitlementPlugin
    {
        Task<ManifestWithVersion> DownloadManifestAsync(string entitlementId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken));
        Task<InstallationRequirements> GetInstallationRequirements(string entitlementId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken));
        Task<IDictionary<string, string>> CurrentVersionIdsForAdgProductIds(IList<string> adgProductIds);
    }

    public class InstallationRequirements
    {
        public enum EResult
        {
            Success,
            Failure
        }

        public EResult Result { get; set; }

        public long RequiredDiskSpaceForFullInstall { get; set; }
        public long RequiredDiskSpaceForUpdate { get; set; }
    }
}
