using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Messages;

namespace Amazon.Fuel.Common.Contracts
{
    public enum ELibraryRefreshType
    {
        /// <summary> Force resync entire entitlement DB </summary>
        Full,
        /// <summary> Resync entitlements based on expiration timings </summary>
        Cached,
        /// <summary> Resync entitlements from DB cache </summary>
        Offline_Cached,
        /// <summary> Resync entitlement run state only </summary>
        Offline_ProcessRunStateOnly
    }

    public static class LibraryRefreshTypeUtil
    {
        public static bool IsNetworked(this ELibraryRefreshType thisType) 
            => thisType == ELibraryRefreshType.Full
                || thisType == ELibraryRefreshType.Cached;
    }

    public class PrereqExecutableWithSha
    {
        public Executable Exe;
        public string ExpectedSha;
    }

    public interface ILibraryPlugin
    {
        Dictionary<ProductId, GameStateInfo> CachedGameStates { get; }

        Task<bool> UpdateEntitledGamesList();

        void ClearEntitlementsDb();

        Game GetGame(ProductId productId);
        GameInstallInfo GetGameInstallInfo(ProductId productId);
        bool GameRegistered(ProductId productId);
        List<Game> OwnedGames { get; }
        List<Game> InstallableGames { get; }
        Task<GameInstallInstancesRefreshed> TryRefreshGamesListAsync(ELibraryRefreshType refreshType = ELibraryRefreshType.Full, ProductId[] filter = null);
    }
}