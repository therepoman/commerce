using Amazon.Fuel.Common.IPC;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IHelperLauncherProvider
    {
        void LaunchHelperApp();
        InstallHelperSession InstallHelperSession { get; }
    }
}