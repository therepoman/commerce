﻿namespace Amazon.Fuel.Common.Contracts
{
    public class GameEntitlementObject
    {
        public string GameAsin { get; set; }
        public string GameTitle { get; set; }
        public string GameImageUri { get; set; }
        public bool IsEntitled { get; set; }
        public string EntitledMessage { get; set; }
        public string DownloadUri { get; set; }
        public string DownloadLocation { get; set; }
    }
}
