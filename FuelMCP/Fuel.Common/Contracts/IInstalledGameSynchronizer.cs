﻿using System.Collections.Generic;

namespace Amazon.Fuel.Common.Contracts
{
    public interface IInstalledGameSynchronizer
    {
        List<string> RemoveInvalidGames();
    }
}