﻿using System;
using System.Collections.Generic;

namespace Amazon.Fuel.Common.Contracts
{
    public interface ISecureStoragePlugin : IDisposable
    {
        bool Remove(string context, string key = null);
        void Put(string context, string key, string value);
        void PutAll(string context, IEnumerable<SecureStorageItem> items);
        string Get(string context, string key);
        void Wipe();
        void Clear();
    }

    public class SecureStorageItem
    {
        public string Key { get; }
        public string Value { get; }

        public SecureStorageItem(string key, string itemValue)
        {
            Key = key;
            Value = itemValue;
        }
    }

    /// <summary>
    /// Recoverable secure storage exception (single access failure, but DB not compromised and may be recoverable)
    /// </summary>
    public class SecureStorageException : Exception
    {
        public SecureStorageException(string message, Exception e) : base(message, e) { }
        public SecureStorageException(string message) : base(message) { }
    }

    /// <summary>
    /// Unrecoverable secure storage exception (DB can't recover from this state, Fuel should shut down)
    /// </summary>
    public class SecureStorageCriticalException : Exception
    {
        public SecureStorageCriticalException(string message, Exception e) : base(message, e) { }
    }
}
