﻿using CommandLine;
using Equ;
using System.Collections.Generic;
using System.Linq;
using CommandLine.Text;
using System.Text;
using System;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Common.CommandLineParser
{
    public class TwitchInstallHelperArg : MemberwiseEquatable<TwitchInstallHelperArg>
    {
        // override the appdata location
        [Option("appdata_override")]
        public string AppDataLocationOverride { get; set; }

        // ipc pipe to communicate over w/ Radium
        [Option("ipc_pipe", Required=true)]
        public string IpcPipeName { get; set; }

#if DEBUG
        // connect a debugger on startup
        [Option("debug")]
        public bool AttachDebugger { get; set; }
#endif

        // use Twitch Dev directory
        [Option("dev_mode")]
        public bool DevMode { get; set; }

        public string[] Args
        {
            get
            {
                var args = new List<string>();

                if (!string.IsNullOrEmpty(AppDataLocationOverride))
                {
                    args.Add($"--appdata_override");
                    args.Add(AppDataLocationOverride);
                }

                if (DevMode)
                {
                    args.Add($"--dev_mode");
                }

                if (!string.IsNullOrEmpty(IpcPipeName))
                {
                    args.Add($"--ipc_pipe");
                    args.Add(IpcPipeName);
                }

#if DEBUG
                if (AttachDebugger)
                {
                    args.Add("--debug");
                }
#endif

                return args.ToArray();
            }
        }

        public string ArgsString => CLIUtilities.ArgsToCmdline(Args);
    }
}
