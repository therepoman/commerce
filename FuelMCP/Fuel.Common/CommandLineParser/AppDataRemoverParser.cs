﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommandLine;
using Equ;

namespace Amazon.Fuel.Common.CommandLineParser
{
    public class AppDataRemoverParser
    {
        public AppDataRemoverArgs Parse(string[] args)
        {
            if (args.Length == 0)
            {
                return new AppDataRemoverArgs
                {
                    Mode = AppDataRemoverArgs.Modes.AllWithConfirm
                };
            }

            var result = new AppDataRemoverArgs();
            var parsed = new Parser().ParseArguments(args, result);

            if (!parsed)
            {
                throw new InvalidCliException($"Unable to parse command line arguments: \"{string.Join(" ", args)}\"");
            }

            if (AppDataRemoverArgs.REQUIRES_PRODUCT_ID.Contains(result.Mode) && string.IsNullOrEmpty(result.ProductId))
            {
                throw new InvalidCliException($"{result.Mode} requires a productid");
            }


            return result;
        }
    }

    public class AppDataRemoverArgs : MemberwiseEquatable<AppDataRemoverArgs>
    {
        public enum Modes
        {
            All,
            Game,
            GameSilent,
            AllWithConfirm
        }

        protected internal static readonly Modes[] REQUIRES_PRODUCT_ID =
        {
            Modes.Game,
            Modes.GameSilent
        };
        [Option('m', "mode", Required = true, HelpText = "The mode of the app")]
        public Modes Mode { get; set; }

        [Option('p', "product", HelpText = "The product id")]
        public string ProductId { get; set; }

        [Option('l', "language", HelpText = "Language for UI, should be a locale-code (ex: en or en-gb,en-us,etc. for English)")]
        public string LanguageCode { get; set; }

        [Option("dev_mode")]
        public bool DevMode { get; set; }

#if DEBUG
        [Option("debug")]
        public bool AttachDebugger { get; set; }
#endif

        public string[] Args
        {
            get
            {
                var args = new List<string> {
                    "-m",
                    Mode.ToString(),
                };

                if (!string.IsNullOrEmpty(ProductId))
                {
                    args.Add("-p");
                    args.Add(ProductId);
                }

                if (!string.IsNullOrEmpty(LanguageCode))
                {
                    args.Add("-l");
                    args.Add(LanguageCode);
                }

                if (DevMode)
                {
                    args.Add("--dev_mode");
                }

#if DEBUG
                if (AttachDebugger)
                {
                    args.Add("--debug");
                }
#endif

                return args.ToArray();
            }
        }

        public string ArgsString => string.Join(" ", Args);
    }


    public class InvalidCliException : Exception
    {
        public override string Message { get; }

        public InvalidCliException(string message)
        {
            Message = message;
        }
    }
}
