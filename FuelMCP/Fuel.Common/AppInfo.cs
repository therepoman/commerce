﻿using Amazon.Fuel.Common.Implementations;
using System.Linq;
using System.Reflection;

namespace Amazon.Fuel.Common.Configuration
{
    /// <summary>
    /// Needs to closely map cloud drive.  Here's what we don't currently implement (because it seems unlikely to matter to PMET):
    /// LocalStoragePath
    /// DatabaseStoragePath
    /// LogsPath
    /// CrashLogsPath
    /// PrivateLocalStoragePath
    /// </summary>
    public class AppInfo : IAppInfo
    {
        public string AssemblyVersion
        {
            get {
#if WIN
                return Assembly.GetExecutingAssembly().GetName().Version.ToString(3);
#else
                return "99.0.0";
#endif
            }
        }
        
        public string CommitId
        {
            get
            {
#if DEBUG
                return System.Environment.UserName;
#else
#if WIN
                var platWinType = Assembly.GetExecutingAssembly();
                var platWinVersion = platWinType.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false).FirstOrDefault() as AssemblyInformationalVersionAttribute;

                if (platWinVersion != null)
                    return platWinVersion.InformationalVersion;
#elif MAC
				return (NSString) NSBundle.MainBundle.InfoDictionary.ValueForKey(new NSString("CommitId"));
#elif TEST
                return "test";
#endif
                return "XXXXXXXX";
#endif
            }
        }

#if DEBUG
        public string Version { get { return string.Format("dev-{0}.{1}", AssemblyVersion, CommitId); } }
#elif BETA
        public string Version { get { return string.Format("beta-{0}.{1}", AssemblyVersion, CommitId); } }
#else
        public string Version { get { return string.Format("{0}.{1}", AssemblyVersion, CommitId); } }
#endif

        public string Name
        {
            get { return PrivateName; }
        }

        private const string PrivateName = "Project Fuel";

        public string InternalName
        {
            get { return "FuelWin"; } //TODO: branch by platform for Fuel<Platform>
        }

        public string PmetSource
        {
            get { return "FuelDesktop"; }
        }

        public string SpectatorName
        {
            get { return "com.amazon.fuel.win"; } ///TODO: branch by platform
        }

        public string PmetDomain
        {
            get { return "Fuel"; }
        }

        public string DeviceSerialId
        {
            get { return DeviceInfo.DSN; }
        }

        public string DeviceName { get { return "Fuel"; } }
        public bool RegisterDeviceNameWithPanda { get; }
        public string LocalStoragePath { get; }

        /// <summary>
        /// Need to register a new devicetype per platform for ourselves so we don't collide with other PC software.
        /// </summary>
        public string DeviceTypeId
        {
            get { return " A2T5CWKK734D2D"; } //This is our Fuel for Windows device type https://issues.amazon.com/issues/dms-2106
        }

        public string HttpUserAgentName
        {
            get { return InternalName; }
        }
    }
}
