﻿using System.Collections.Generic;
using AutoMapper;
using Equ;
using Newtonsoft.Json;

/**
 * This file contains all of the launch configurations and their json format. Because
 * we are dealing with a CDN which can have old launch configs we need to be able to handle and parse
 * older versions on the client. Otherwise we'd have a hellacious migration issue with our
 * data and releasing new versions of the launcher.
 * 
 * WE NEED TO REV THE SCHEMA VERSION OF THE CONFIG WHENEVER:
 * - We add new fields that can't be defaulted
 * - We move or rename parts of the schema
 * 
 * Process:
 * - Copy the LaunchConfig object to a new one.
 * - Create the next LaunchConfigV# object with your stuff in it (if the max is V2, make a V3)
 * - Change the LaunchConfig to extend your version, eg LaunchConfig should now extend V3
 * - Fix the Next functionss
 * - You need to add a test in JsonLaunchConfigProviderTest that proves you can upgrade V2 to V3 (in this example)
 */
namespace Amazon.Fuel.Common.Data
{

    public class SchemaVersionObject
    {
        public string SchemaVersion;
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static SchemaVersionObject FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<SchemaVersionObject>(jsonData);
        }
    }

    public class Executable : MemberwiseEquatable<Executable>
    {
        public string Command { get; set; }
        public string[] Args { get; set; }
        public bool AlwaysRun { get; set; }
        public int[] ValidReturns { get; set; }

        public Executable()
        {
            Command = "";
            Args = new string[] { };
            ValidReturns = new int[] { };
        }

        public override string ToString()
        {
            return Command + " " + Args;
        }
    }
    public class MainExecutable : MemberwiseEquatable<MainExecutable>
    {
        public string Command { get; set; }
        // working dir defaults to install dir when null or empty
        public string WorkingSubdirOverride { get; set; }
        public string[] Args { get; set; }
        public List<string> AuthScopes { get; set; }
        public string ClientId { get; set; }
        public string PostRunLink { get; set; }


        public MainExecutable() : base()
        {
            Command = "";
            Args = new string[] { };
            AuthScopes = new List<string>();
            PostRunLink = null;
            WorkingSubdirOverride = null;
        }
    }

    public static class LaunchConfigFlags
    {
        public const string KillIfSuspended = "kill_if_suspended";
    }

    public class LaunchConfigV2 : MemberwiseEquatable<LaunchConfigV2>
    {
        public List<Executable> PostInstall;
        public MainExecutable Main;

        public class TwitchAuth : MemberwiseEquatable<TwitchAuth>
        {
            public string ClientId;
            public List<string> Scopes;
        }

        public TwitchAuth Auth;
        public List<string> Flags = new List<string>();
                
        public LaunchConfigV2()
        {
            this.PostInstall = new List<Executable>();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static LaunchConfigV2 FromJson( string jsonData)
        {
            return JsonConvert.DeserializeObject<LaunchConfigV2>(jsonData);
        }

        public LaunchConfig Next()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<LaunchConfigV2, LaunchConfig>());
            return config.CreateMapper().Map<LaunchConfig>(this);
        }

        public bool HasFlag(string flag) => Flags?.Contains(flag) ?? false;
    }

    public class LaunchConfig : LaunchConfigV2
    {
        
    }
}
