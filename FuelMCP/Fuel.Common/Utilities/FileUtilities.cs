﻿using Amazon.Fuel.Common.Contracts;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Utilities
{
    public static class FileUtilities
    {       

        public static void DeleteEmptyFolders(string startLocation)
        {
            foreach (var directory in System.IO.Directory.GetDirectories(startLocation))
            {
                DeleteEmptyFolders(directory);
                if (System.IO.Directory.GetFiles(directory).Length == 0 && System.IO.Directory.GetDirectories(directory).Length == 0)
                {
                    System.IO.Directory.Delete(directory, false);
                }
            }
        }

        public static string NormalizePath(string path)
        {
            return path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
        }

        public static long CalcDirectorySize(string path)
        {
            long ret = 0;
            try
            {
                foreach (string file in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories))
                {
                    ret += new FileInfo(file).Length;
                }
            }
            catch (Exception) { }

            return ret;
        }
    }
}
