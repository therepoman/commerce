﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Serilog;
using System;
using System.Threading;

namespace Amazon.Fuel.Common.Utilities
{
    public class CancelTaskHandler : IDisposable
    {
        private readonly IMessengerHub _hub;
        private readonly ProductId _adgProductId;

        private IDisposable _hubTokenCancel;
        private CancellationTokenSource _cancelTokenSource = null;

        public volatile bool IsCanceled;
        public string CancelId { get; private set; }

        public CancelTaskHandler(IMessengerHub hub, ProductId adgProductId)
        {
            IsCanceled = false;
            _hubTokenCancel = hub.Subscribe<MessageDownloadCancel>(CancelDownload);
            _hub = hub;
            _adgProductId = adgProductId;
        }

        ~CancelTaskHandler()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_hubTokenCancel != null)
            {
                _hub.Unsubscribe<MessageDownloadCancel>(_hubTokenCancel);
                _hubTokenCancel = null;
            }
        }

        public CancellationTokenSource TokenSource
        {
            get
            {
                _cancelTokenSource = _cancelTokenSource ?? new CancellationTokenSource();
                return _cancelTokenSource;
            }
        }

        private void CancelDownload(MessageDownloadCancel sender)
        {
            if (sender.AdgProdId == _adgProductId)
            {
                FuelLog.Info("Cancellation detected", new { _adgProductId });
                IsCanceled = true;
                _cancelTokenSource?.Cancel();
            }
        }        
    }
}
