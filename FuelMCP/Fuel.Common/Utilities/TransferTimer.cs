﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Amazon.Fuel.Common.Utilities
{
    public class TransferTimer : IDisposable
    {
        private long lastBytes = 0;
        private int transferRefreshRate;
        private decimal transferMultipler;
        private int maxHistoryCount;
        private float estIntervalSec;
        private DateTime lastEstDt = DateTime.MinValue;
        private double lastEstValue = 0.0f;
        private CancellationTokenSource threadToken = new CancellationTokenSource();

        private object Lock = new object();

        public bool IsEnabled { get; set; } = false;

        public ITransferedBytes TransferSource { get; set; }
        
        public List<long> BytesReceivedHistory { get; private set; }
        
        /// <summary>
        /// Pools the byte transfer source for changes.
        /// IsEnabled should be set to true when you wish to start polling.
        /// StartLoop must be started for any automatic polling to happen.
        /// </summary>
        /// <param name="source">The source that is receiveing bytes</param>
        /// <param name="transferRefreshRateMs">How often to check for changes in miliseconds</param>
        /// <param name="timeEstIntervalSec">How often to throttle estimated time queries</param>
        /// <param name="maxHistoryCount">How much history to keep, more is more stable, less is more accurate, but will have high varience</param>
        /// <param name="startLoop">true will start up a new thread to do the polling. If false call StartLoop() to begin pooling the source</param>
        public TransferTimer(ITransferedBytes source, int transferRefreshRateMs, float timeEstIntervalSec, int maxHistoryCount = 120, bool startLoop = true)
        {
            this.TransferSource = source;
            this.transferRefreshRate = transferRefreshRateMs;
            this.transferMultipler = 1000 / transferRefreshRateMs;
            this.BytesReceivedHistory = new List<long>();
            this.maxHistoryCount = maxHistoryCount;
            this.estIntervalSec = timeEstIntervalSec;

            if ( startLoop)
            {
                StartLoop();
            }            
        }

        public void StartLoop()
        {
            Thread thread = new Thread(new ThreadStart(transferMetrics_loop));
            thread.Name = "FuelTransferTimer";
            thread.IsBackground = true;
            thread.Start();
        }

        public void Dispose()
        {
            lock (Lock)
            {
                threadToken.Cancel();
            }
        }

        /// <summary>
        /// Ticks the timer by one.
        /// </summary>
        public void Tick()
        {
            if (IsEnabled == true)
            {
                lock (Lock)
                {
                    if (threadToken.IsCancellationRequested)
                    {
                        return;
                    }

                    var dif = TransferSource.ReceivedBytes - lastBytes;
                    dif = Math.Max(0, dif);

                    lastBytes = TransferSource.ReceivedBytes;

                    BytesReceivedHistory.Add((long)(dif * transferMultipler));

                    while (BytesReceivedHistory.Count > maxHistoryCount)
                    {
                        BytesReceivedHistory.RemoveAt(0);
                    }
                }
            }
        }

        private void transferMetrics_loop()
        {
            while (!threadToken.IsCancellationRequested)
            {
                Tick();
                Thread.Sleep(transferRefreshRate);
            }
        }

        public double MegaBytesPerSec
        {
            get
            {
                lock (Lock)
                {
                    return BytesReceivedHistory.Count == 0 ? 0 : BytesReceivedHistory.Average() / 1024.0 / 1024.0;
                }
            }
        }

        public double EstimateSecLeft(long bytesRemaining)
        {
            lock (Lock)
            {
                if ( (DateTime.Now - lastEstDt).TotalSeconds >= estIntervalSec )
                {
                    var megabytesRemaining = (double)bytesRemaining / 1024.0 / 1024.0;
                    lastEstValue = megabytesRemaining / Math.Max(0.001, MegaBytesPerSec);
                    lastEstDt = DateTime.Now;
                }

                return lastEstValue;
            }
        }
    }
}
