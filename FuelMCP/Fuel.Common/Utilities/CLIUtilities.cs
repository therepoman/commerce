﻿using System.Linq;
using System.Text;

namespace Amazon.Fuel.Common.Utilities
{
    public static class CLIUtilities
    {
        /// <summary>
        /// Translate a sequence of arguments into a command line
        /// string, using the same rules as the MS C runtime
        /// </summary>
        /// <param name="argv">argsAsSingleString array</param>
        /// <returns></returns>
        /**
         * See
         * http://msdn.microsoft.com/en-us/library/17w5ykft.aspx
         * or search http://msdn.microsoft.com for
         * "Parsing C++ Command-Line Arguments"
         */
        public static string ArgsToCmdline(string[] argv)
        {
            StringBuilder result = new StringBuilder();
            var needquote = false;
            foreach (var arg in argv)
            {
                // Add a space to separate arg
                if (result.Length > 0)
                {
                    result.Append(" ");
                }

                needquote = arg.Contains(" ") || arg.Contains("\t") || arg.Length == 0;
                if (needquote) result.Append("\"");

                StringBuilder bs_buf = new StringBuilder();
                foreach (char c in arg)
                {
                    if (c == '\\')
                    {
                        // Don't know if we need to double backslash yet
                        bs_buf.Append(c);
                    }
                    else if (c == '"')
                    {
                        // Double backslashes
                        result.Append(string.Join("", Enumerable.Repeat("\\", bs_buf.Length * 2)));
                        bs_buf.Clear();
                        result.Append("\\\"");
                    }
                    else
                    {
                        // Normal char
                        if (bs_buf.Length > 0)
                        {
                            result.Append(bs_buf);
                            bs_buf.Clear();
                        }
                        result.Append(c);
                    }
                }

                // Add remaining backslashes, if any
                if (bs_buf.Length > 0) result.Append(bs_buf);

                if (needquote)
                {
                    result.Append(bs_buf);
                    result.Append('"');
                }
            }

            return result.ToString();
        }

    }
}
