﻿using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Utilities
{
    public class RetryCountPolicy : IRetryPolicy
    {
        public RetryCountPolicy(int retryCount = 3)
        {
            RetriesRemainingCount = retryCount;
        }

        public int RetriesRemainingCount { get; private set; }

        public void IncrementRetries()
        {
            --RetriesRemainingCount;
        }

        public bool WantRetry => RetriesRemainingCount > 0;
    }

    public class RetryInfinitePolicy : IRetryPolicy
    {
        public RetryInfinitePolicy()
        {
        }


        public void IncrementRetries()
        {
        }

        public bool WantRetry => true;
    }
}
