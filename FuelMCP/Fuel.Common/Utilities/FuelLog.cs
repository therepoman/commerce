﻿using System;
using System.Text;
using AutoMapper.Execution;
using Curse.Logging;
using Serilog;

namespace Amazon.Fuel.Common.Utilities
{
    public static class FuelLog
    {
        public static void Debug(Exception ex, string msg, object data = null)
            => logHelperEx(ex, Logger.Debug, Log.Debug, msg, data);

        public static void Debug(string msg, object data = null)
            => logHelper(Logger.Debug, Log.Debug, msg, data);

        public static void Info(Exception ex, string msg, object data = null)
            => logHelperEx(ex, Logger.Info, Log.Information, msg, data);

        public static void Info(string msg, object data = null)
            => logHelper(Logger.Info, Log.Information, msg, data);

        public static void Warn(Exception ex, string msg, object data = null)
            => logHelperEx(ex, Logger.Warn, Log.Warning, msg, data);

        public static void Warn(string msg, object data = null)
            => logHelper(Logger.Warn, Log.Warning, msg, data);

        public static void Error(Exception ex, string msg, object data = null)
            => logHelperEx(ex, Logger.Error, Log.Error, msg, data);

        public static void Error(string msg, object data = null)
            => logHelper(Logger.Error, Log.Error, msg, data);

        public static void Fatal(Exception ex, string msg, object data = null)
            => logHelperEx(ex, Logger.Fatal, Log.Fatal, msg, data);

        public static void Fatal(string msg, object data = null)
            => logHelper(Logger.Fatal, Log.Fatal, msg, data);

        private static void logHelper(
            Action<string, object> curseLogger,
            Action<string> serilogLogger,
            string msg,
            object data = null)
        {
            var usernameOmittedMsg = OmitUsernameFromLog(msg);

            curseLogger(usernameOmittedMsg, data);

            if (data != null)
            {
                usernameOmittedMsg += " { " + flattenData(data) + " }";
            }

            serilogLogger(usernameOmittedMsg);
        }

        private static void logHelperEx(
            Exception ex,
            Action<Exception, string, object> curseLogger,
            Action<Exception, string> serilogLogger,
            string msg,
            object data = null)
        {
            var usernameOmittedMsg = OmitUsernameFromLog(msg);

            curseLogger(ex, usernameOmittedMsg, data);

            if (data != null)
            {
                usernameOmittedMsg += $" ({flattenData(data)})";
            }

            serilogLogger(ex, usernameOmittedMsg);
        }

        private static string OmitUsernameFromLog(string logMsg)
        {
            return logMsg.ReplaceCaseInsensitive(Environment.UserName, "<omit>");
        }

        private const string RetrieveFailedStr = "<retrieve_failed>";
        private static string flattenData(object data)
        {
            try
            {
                var sb = new StringBuilder();

                var spacer = "";
                foreach (var propertyInfo in data.GetType().GetProperties())
                {
                    if (!propertyInfo.CanRead)
                        continue;

                    string name;
                    try
                    {
                        name = propertyInfo.Name;
                    }
                    catch (Exception)
                    {
                        name = RetrieveFailedStr;
                    }

                    string value;
                    try
                    {
                        value = propertyInfo.GetValue(data).ToString();
                    }
                    catch (Exception)
                    {
                        value = RetrieveFailedStr;
                    }

                    sb.Append($"{spacer}{name}='{value}'");
                    spacer = ", ";
                }

                return sb.ToString().ReplaceCaseInsensitive(Environment.UserName, "<omit>");
            }
            catch (Exception)
            {
                return RetrieveFailedStr;
            }
        }
    }
}
