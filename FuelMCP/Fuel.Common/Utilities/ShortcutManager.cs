﻿using System;
using System.Collections.Generic;
using Amazon.Fuel.Common.CommandLineParser;
using Amazon.Fuel.Common.Contracts;
using Serilog;

namespace Amazon.Fuel.Common.Utilities
{
    public class ShortcutManager : IShortcutManager
    {
        private readonly ILaunchConfigProvider _launchConfig;
        private readonly IShortcutDriver _shortcutDriver;
        private readonly IOSDriver _osDriver;

        public ShortcutManager(IOSDriver osDriver, IShortcutDriver shortcutDriver, ILaunchConfigProvider launchConfig)
        {
            _osDriver = osDriver;
            _shortcutDriver = shortcutDriver;
            _launchConfig = launchConfig;
        }

        public string[] GetShortcutLocationsByTitle(string gameProductTitle)
            => new string[]
            {
                _shortcutDriver.GetShortcutFileName(location: ShortcutLocation.Twitch, shortcutBaseName: gameProductTitle),
                _shortcutDriver.GetShortcutFileName(location: ShortcutLocation.Desktop, shortcutBaseName: gameProductTitle)
            };

        public bool TryCreateShortcuts(GameProductInfo game, GameInstallInfo installInfo, bool includeDesktop)
        {
            var cfg = _launchConfig.GetLaunchConfig(installInfo.InstallDirectory);

            if (cfg == null)
            {
                Log.Error("Couldn't retrieve launch config file, shortcut create failed");
                return false;
            }

            var iconLocation = System.IO.Path.Combine(installInfo.InstallDirectory, cfg.Main.Command);

            var uriShortcut = string.Format(Constants.URICommands.CommandSyntax, Constants.URICommands.Launch, game.ProductId.Id);

            bool ret = true;

            try
            {
                Log.Information("Creating start menu shortcut", new { prodid = game.ProductTitle, iconLocation });
                _shortcutDriver.TryCreateUrl(
                    location: ShortcutLocation.Twitch,
                    shortcutBaseName: game.ProductTitle,
                    targetUrl: uriShortcut,
                    description: $"Play {game.ProductTitle}",
                    iconLocation: iconLocation);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Couldn't create shortcut at '{ShortcutLocation.Twitch}' named '{game.ProductTitle}': '{ex.Message}'");
                ret = false;
            }

            // desktop
            if (includeDesktop)
            {
                try
                {
                    Log.Information("Creating desktop shortcut", new { prodid = game.ProductTitle, iconLocation });
                    _shortcutDriver.TryCreateUrl(
                        location: ShortcutLocation.Desktop,
                        shortcutBaseName: game.ProductTitle,
                        targetUrl: uriShortcut,
                        description: $"Play {game.ProductTitle}",
                        iconLocation: iconLocation);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Couldn't create shortcut at '{ShortcutLocation.Desktop}' named '{game.ProductTitle}': '{ex.Message}'");
                    ret = false;
                }
            }

            return ret;
        }

        public void RemoveShortcuts(GameInstallInfo game)
        {
            if (game == null)
            {
                Log.Error("RemoveShortcuts() : null game passed");
                return;
            }

            foreach (var shortcutLocation in GetShortcutLocationsByTitle(game.ProductTitle))
            {
                _shortcutDriver.RemoveShortcut(shortcutLocation);
            }
        }
    }
}