using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Utilities
{
    public class ElevatedCommandRunner<TResponse>
    {
        public class TimeoutException : Exception { }

        // throws TimeoutException on timeout
        public static async Task<TResponse> ExecuteAsyncWithTimeout(IElevatedCommand<TResponse> cmd, CancellationToken? cancellationToken = null)
        {
            Func<Task<TResponse>> cmdFunc = cmd.Execute;

            CancellationTokenSource abortToken = new CancellationTokenSource();
            CancellationTokenSource masterCancelToken = abortToken;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            if (cancellationToken.HasValue)
            {
                // pay attention to external abort as well
                masterCancelToken = CancellationTokenSource.CreateLinkedTokenSource(abortToken.Token, cancellationToken.Value);
            }

            var cmdTask = Task.Run(cmdFunc, masterCancelToken.Token);

            if (cmdTask == await Task.WhenAny(cmdTask, Task.Delay(cmd.TimeoutMS, masterCancelToken.Token)))
            {
                await cmdTask;
            }
            else
            {
                cmd.Abort();
                abortToken?.Cancel();
                Log.Error($"Command timeout! Command: '{cmd.GetType()}'");
                throw new TimeoutException();
            }

            sw.Stop();
            FuelLog.Info("Command completed", new { type = cmd.GetType(), totalSeconds = sw.Elapsed.TotalSeconds });
            return await cmdTask;
        }

        public static TResponse Execute(IElevatedCommand<TResponse> cmd, out bool wasTimeout)
        {
            wasTimeout = false;
            TResponse result;
            try
            {
                result = ExecuteAsyncWithTimeout(cmd).GetAwaiter().GetResult();
            }
            catch (TimeoutException)
            {
                wasTimeout = true;
                result = default(TResponse);
            }

            return result;
        }
    }
}