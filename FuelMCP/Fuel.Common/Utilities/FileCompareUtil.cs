using System.IO;

namespace Amazon.Fuel.Common.Utilities
{
    public static class FileCompareUtil
    {
        public static bool BinaryCompare(string fileA, string fileB)
        {
            FileInfo fileInfoA = new FileInfo(fileA);
            FileInfo fileInfoB = new FileInfo(fileB);

            // early out
            if (fileInfoA.Length != fileInfoB.Length)
            {
                return false;
            }

            using (FileStream fsA = fileInfoA.OpenRead(), fsB = fileInfoB.OpenRead())
            using (BufferedStream bsA = new BufferedStream(fsA), bsB = new BufferedStream(fsB))
            {
                for (long i = 0; i < fileInfoA.Length; i++)
                {
                    if (bsA.ReadByte() != bsB.ReadByte())
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}