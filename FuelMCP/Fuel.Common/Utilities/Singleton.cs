﻿
using System;

namespace Amazon.Fuel.Common.Utilities
{
    public abstract class Singleton<T> where T : class, new()
    {
        protected Singleton() { }

        private static T _instance;

        public static T Instance
        {
            get
            {
                _instance = _instance ?? new T();
                return _instance;
            }
        }
    }
}
