using System;
using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Common.Utilities
{
    public class ScopedTinyMsgListener<T> : IDisposable where T : class, ITinyMessage
    {
        private readonly IMessengerHub _hub;
        private readonly IDisposable _subToken;

        public ScopedTinyMsgListener(IMessengerHub hub, Action<T> listener)
        {
            _hub = hub;
            _subToken = _hub.Subscribe<T>(listener);
        }

        public void Dispose()
        {
            _hub.Unsubscribe<T>(_subToken);
        }
    }
}