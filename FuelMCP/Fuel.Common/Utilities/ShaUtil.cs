using System;
using System.IO;
using System.Security.Cryptography;

namespace Amazon.Fuel.Common.Utilities
{
    public static class ShaUtil
    {
        public delegate void FileHashProgressCallback(long bytesRead, long bytesTotal);

        public static string ComputeHash(string file, FileHashProgressCallback ProgressCallback = null)
        {
            byte[] hash = ComputeHashBytes(file, ProgressCallback);
            return ToString(hash);
        }

        public static byte[] ComputeHashBytes(string file, FileHashProgressCallback progressCallback = null)
        {
            using (FileStream stream = File.OpenRead(file))
            using (var sha = new SHA256Managed())
            {
                var buffer = new byte[1024 * 1024];
                long totalBytes = stream.Length;
                long totalBytesRead = 0;
                int bytes = stream.Read(buffer, 0, buffer.Length);
                totalBytesRead += bytes;

                while (bytes > 0)
                {
                    sha.TransformBlock(buffer, 0, bytes, null, 0);
                    bytes = stream.Read(buffer, 0, buffer.Length);
                    totalBytesRead += bytes;

                    progressCallback?.Invoke(totalBytesRead, totalBytes);
                }
                sha.TransformFinalBlock(buffer, 0, 0);

                byte[] ret = new byte[sha.Hash.Length];
                sha.Hash.CopyTo(ret, 0);

                return ret;
            }
        }

        public static string ToString(byte[] hash)
        {
            return BitConverter.ToString(hash).Replace("-", String.Empty).ToLower();
        }
    }
}