﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Amazon.Fuel.Common.Utilities
{
    public static class LogicUtilities
    {
        /// <summary>
        /// Do an async action once followed by retries up to maximum count, if requested.
        /// </summary>
        /// <param name="taskWithAttemptCount">main async task to call, should return true when done (no further retries necessary)</param>
        /// <param name="maxRetryCount">maximum count of retries (doesn't include first attempt, so max attempts is this number +1)</param>
        /// <param name="retryWaitSec">array of seconds to wait after each failed attempt (first value fires before first retry)</param>
        /// <param name="cancelToken">optional cancellation token</param>
        /// <returns>true if completed within max retry count without wanting another retry</returns>
        public static async Task<bool> DoWithRetryAsync(
            Func<int, Task<bool>> taskWithAttemptCount, 
            int maxRetryCount,
            int[] retryWaitSec = null,
            CancellationToken? cancelToken = null)
        {
            var attemptCount = 1;

            // +1 for first attempt (not a retry)
            for (; attemptCount <= maxRetryCount + 1 ; ++attemptCount)
            {
                if (attemptCount > 1)
                {
                    Log.Information($"DoWithRetryAsync() Retrying task (attempts:{attemptCount}/{maxRetryCount+1})");

                    if (retryWaitSec?.Length > 0)
                    {
                        int waitSec = retryWaitSec[Math.Min(retryWaitSec.Length - 1, attemptCount - 2)];
                        Log.Information($"DoWithRetryAsync() Waiting for '{waitSec}' sec.");
                        if (cancelToken.HasValue)
                        {
                            await Task.Delay(TimeSpan.FromSeconds(waitSec), cancelToken.Value);
                        }
                        else
                        {
                            await Task.Delay(TimeSpan.FromSeconds(waitSec));
                        }
                    }
                }

                if (cancelToken?.IsCancellationRequested ?? false)
                {
                    Log.Information($"DoWithRetryAsync() Task cancelled (attempts:{attemptCount}/{maxRetryCount+1})");
                    return true;
                }

                bool finished = await taskWithAttemptCount(attemptCount);
                if (finished)
                {
                    // you done
                    return true;
                }

                if (cancelToken?.IsCancellationRequested ?? false)
                {
                    Log.Information($"DoWithRetryAsync() Task cancelled (attempts:{attemptCount}/{maxRetryCount+1})");
                    return true;
                }
            }

            Log.Information($"DoWithRetryAsync() Maximum attempts exceeded ({maxRetryCount + 1})");
            return false;
        }
    }

    public static class DicitionaryExt
    {
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue defaultVal)
        {
            TValue val;
            return dict.TryGetValue(key, out val) ? val : defaultVal;
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key)
        {
            TValue val;
            return dict.TryGetValue(key, out val) ? val : default(TValue);
        }
    }
}
