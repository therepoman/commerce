﻿using System;
using Microsoft.Win32;

namespace Amazon.Fuel.Common.Utilities
{
    // technically this should be in the Windows OS driver plugin, but I don't want to invoke autofac before Radium's migration, so I'm putting them here to be safe.
    public static class WindowsAppUtilities
    {
        public const string INST_LOCATION_KEY_NAME = "InstallLocation";

        public static string OsProductName = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\", "ProductName", "Value not found in registry");

        private const string NO_45_OR_LATER = ".NET Framework Version 4.5 or later is not detected.";

        private const string DOT_NET_FRAMEWORK_SUBKEY = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";

        public static string TryFindInstalledLocationForProduct(string productKey)
        {
            return TryGetUninstallRegkeyContainingProductKey(productKey)?.OpenSubKey(productKey)?.GetValue(INST_LOCATION_KEY_NAME) as string;
        }

        public static RegistryKey TryGetUninstallRegkeyContainingProductKey(string productKey, bool writable = false)
        {
            var baseReg = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);

            // this is the "proper" location
            var uninstallKey = baseReg?.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                writable: writable);

            if (uninstallKey?.OpenSubKey(productKey) == null)
            {
                baseReg = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);

                // try again in 32-bit
                uninstallKey = baseReg?.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", writable: writable);

                // in case that doesn't work, try some other means
                if (uninstallKey?.OpenSubKey(productKey) == null)
                {
                    uninstallKey =
                        Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                            writable: writable);
                    if (uninstallKey?.OpenSubKey(productKey) == null)
                    {
                        // try again with the WOW6432Node
                        uninstallKey =
                            Registry.LocalMachine.OpenSubKey(
                                @"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall", writable: writable);
                    }
                }
            }

            return uninstallKey;
        }

        // https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/how-to-determine-which-versions-are-installed#net_d
        public static String Get45PlusFromRegistry()
        {
            const string subkey = DOT_NET_FRAMEWORK_SUBKEY;

            using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(subkey))
            {
                if (ndpKey != null)
                {
                    var dotNetVersion = ndpKey.GetValue("Release");
                    return CheckFor45PlusVersion((int)dotNetVersion) ?? NO_45_OR_LATER;
                }
                else
                {
                    return NO_45_OR_LATER;
                }
            }
        }

        // Checking the version using >= will enable forward compatibility.
        private static string CheckFor45PlusVersion(int releaseKey)
        {
            string _releaseKey = " (" + releaseKey + ")";
            if (releaseKey >= 461808)
                return "4.7.2 or later" + _releaseKey;
            if (releaseKey >= 461308)
                return "4.7.1" + _releaseKey; ;
            if (releaseKey >= 460798)
                return "4.7" + _releaseKey; ;
            if (releaseKey >= 394802)
                return "4.6.2" + _releaseKey; ;
            if (releaseKey >= 394254)
                return "4.6.1" + _releaseKey; ;
            if (releaseKey >= 393295)
                return "4.6" + _releaseKey; ;
            if (releaseKey >= 379893)
                return "4.5.2" + _releaseKey; ;
            if (releaseKey >= 378675)
                return "4.5.1" + _releaseKey; ;
            if (releaseKey >= 378389)
                return "4.5" + _releaseKey; ;

            return NO_45_OR_LATER + _releaseKey;
        }
    }
}
