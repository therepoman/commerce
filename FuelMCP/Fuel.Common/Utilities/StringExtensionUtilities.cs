﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Utilities
{
    public static class StringExtensionUtilities
    {
        public static string ReplaceCaseInsensitive(this string str, string target, string destination)
        {
            str = Regex.Replace(str, target, destination, RegexOptions.IgnoreCase);
            return str;
        }
    }
}
