using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Utilities
{
    public class ElevatedIpcCommandRunner<TResponse> : ElevatedCommandRunner<TResponse>
    {
        public static TResponse ExecuteIpc(IElevatedIpcCommand<TResponse> cmd)
        {
            bool wasTimeout = false;
            var response = Execute(cmd, out wasTimeout);
            if (wasTimeout)
            {
                response = cmd.TimeoutMessage;
            }

            return response;
        }
    }
}