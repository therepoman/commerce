﻿using System.Globalization;
using System.Text;
using System.Linq;

namespace Amazon.Fuel.Common.Utilities
{
    public static class StringExt
    {
        //http://stackoverflow.com/questions/249087/how-do-i-remove-diacritics-accents-from-a-string-in-net
        static string ToDiacriticStrippedText(this string thisText)
        {
            var normalizedString = thisText.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string ToAscii(this string thisText, string unknownCharToStr = "_")
        {
            string ret = thisText.ToDiacriticStrippedText();
            Encoding encoder = Encoding.GetEncoding( "us-ascii", 
                                                    new EncoderReplacementFallback(unknownCharToStr), 
                                                    new DecoderReplacementFallback());
            ret = encoder.GetString(encoder.GetBytes(ret));

            return ret;
        }
    }
}
