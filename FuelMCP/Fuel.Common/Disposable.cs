﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common
{
    public abstract class Disposable : IDisposable
    {
        private bool _disposed = false;
        private object _disposeLock = new object();

        public bool Disposed => _disposed;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        public void Dispose()
        {
            if (!_disposed)
            {
                lock (_disposeLock)
                {
                    if (!_disposed) // double-checked locking
                    {
                        _disposed = true;
                        Log.Debug("Disposing instance of " + this.GetType().ToString());

                        Dispose(true);
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }

        ~Disposable()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        protected abstract void Dispose(bool disposing);
    }
}
