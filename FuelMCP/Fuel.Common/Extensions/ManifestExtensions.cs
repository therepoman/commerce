﻿using Amazon.Fuel.Common.Utilities;
using System;
using System.IO;
using System.Linq;

using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Common.Extensions
{
    public static class ManifestExtensions
    {
        public static bool ContainsFile(this Sds.Manifest manifest, string relativePath)
        {
            return manifest.Packages.FirstOrDefault(pkg => pkg.ContainsFile(relativePath)) != null;
        }

        public static long ComputeTotalSizeInBytes(this Sds.Manifest manifest)
        {
            return manifest.Packages[0].Files.Select(f => f.Size).Sum();
        }
    }

    public static class ManifestPackageExtensions
    {
        public static bool ContainsFile(this Sds.Package package, string relativePath)
        {
            return package.Files.FirstOrDefault(file => FileUtilities.NormalizePath(file.Path).Equals(FileUtilities.NormalizePath(relativePath), StringComparison.InvariantCultureIgnoreCase)) != null;
        }
    }

    public static class ManifestHashExtensions
    {
        public static string HexString(this Sds.Hash hash)
        {
            return hash.Value == null ? null : BitConverter.ToString(hash.Value.ToArray()).Replace("-", "").ToLower();
        }
    }
}
