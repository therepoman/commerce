﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class HashExtensions
    {
        public const string MD5 = "MD5";
        public const string DefaultHashAlgorithm = MD5;

        public static string ToHashString(this string content)
        {
            return ToHashString(content, DefaultHashAlgorithm, Encoding.Default);
        }

        public static string ToHashString(this string content, string hashName, Encoding encoding)
        {
            var buffer = ToHash(content, hashName, encoding);
            return buffer.ToHexString();
        }

        public static byte[] ToHash(this string content, string hashName, Encoding encoding)
        {
            return ToHash(encoding.GetBytes(content), hashName);
        }

        public static byte[] ToHash(this byte[] content, string hashName)
        {
            using (var algorithm = CreateHashAlgorithm(hashName))
            {
                return algorithm.ComputeHash(content);
            }
        }

        public static string ToHexString(this byte[] content)
        {
            var sb = new StringBuilder(content.Length * 2);
            foreach (var b in content)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        public static HashAlgorithm CreateHashAlgorithm(string hashName)
        {
            switch (hashName.ToUpperInvariant())
            {
                case MD5:
                    return new MD5Cng();
                default:
                    return HashAlgorithm.Create(hashName);
            }
        }
    }
}
