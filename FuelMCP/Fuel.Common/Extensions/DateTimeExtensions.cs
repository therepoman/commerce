﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class DateTimeExtensions
    {
        public static long ToUnixTime(this DateTimeOffset dt)
        {
            var epoch = new DateTimeOffset(1970, 1, 1, 0, 0, 0, 0, TimeSpan.Zero);
            var diff = dt - epoch;
            return Convert.ToInt64(diff.TotalMilliseconds);
        }
    }
}
