﻿using System.Collections.Generic;
using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    public class GameAddedMessage : ITinyMessage
    {
        public object Sender { get; private set; }
    }

    public class GameDeletedMessage : ITinyMessage
    {
        public object Sender { get; private set; }
    }

    public class GameInstallingMessage : TinyMessageBase
    {
        public GameInstallingMessage(object sender, ProductId productId) : base(sender) { ProductId = productId; }

        public ProductId ProductId { get; private set; }
    }

    public class GameInstallInstancesRefreshedMessage : TinyMessageBase
    {
        public GameInstallInstancesRefreshedMessage(
            object sender,
            GameInstallInstancesRefreshed refreshed) : base(sender)
        {
            Refreshed = refreshed;
        }

        public GameInstallInstancesRefreshed Refreshed { get; private set; }
    }

}
