﻿using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    /// <summary>
    /// Message fires once Fuel Service is started up and plugins are loaded.
    /// </summary>
    public class FuelStartupMessage : ITinyMessage
    {
        public object Sender { get; private set; }
    }
}
