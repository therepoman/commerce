﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    /// <summary>
    /// Message fires once Fuel Service is started up and plugins are loaded.
    /// </summary>
    public class FuelShutdownMessage: ITinyMessage
    {
        public FuelShutdownMessage(int exitCode)
        {
            ExitCode = exitCode;
        }

        public object Sender { get; private set; }
        public int ExitCode { get; private set; }
    }
}
