using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    public class DownloadMessage : TinyMessageBase
    {
        public DownloadMessage(object sender) : base(sender) { }
    }

    public class DownloadFileMessage : DownloadMessage
    {
        public string Folder { get; set; }
        public string File { get; set; }

        public DownloadFileMessage(object sender, string folder, string file)
            : base(sender)
        {
            this.Folder = folder;
            this.File = file;
        }
    }
}