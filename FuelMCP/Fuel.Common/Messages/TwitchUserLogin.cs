﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    /* <summary>
     * Encapsluation of a twitch user & oauth info
     * </summary>*/
    public class TwitchUserLogin
    {
        public TwitchUserLogin(string id, string username)
        {
            this.Id = id;
            this.Username = username;
        }

        public string Id { get; }
        public string Username { get; }
    }

    public class TwitchUserLogoutMessage : GenericTinyMessage<string>
    {
        public TwitchUserLogoutMessage(object sender) : base(sender, "")
        {
        }
    }

    public class TwitchUserLoginMessage : GenericTinyMessage<string>
    {
        public TwitchUserLoginMessage(object sender) : base(sender, "")
        {
        }
    }
}
