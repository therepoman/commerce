﻿using System;

namespace Amazon.Fuel.Common.Messages
{
    public class IpcEnsureInitialRequirementsComplete : InstallHelperIpcMessage
    {
        public enum EResult
        {
            Succeeded,
            Timeout,

            Failure_NoAccess,
            Failure_UnknownException
        }

        public EResult Result;

        public IpcEnsureInitialRequirementsComplete(object sender, EResult result) : base(sender)
        {
            Result = result;
        }
    }

    public class IpcCreateGamesLibraryPrimaryPathComplete : InstallHelperIpcMessage
    {
        public enum EResult
        {
            Succeeded,
            Timeout,

            Failure_Unknown,
            Failure_NoAccess,
            Failure_UnknownException
        }

        public EResult Result;

        public IpcCreateGamesLibraryPrimaryPathComplete(object sender, EResult result) : base(sender)
        {
            Result = result;
        }
    }

    public class IpcInstallPrereqsComplete : InstallHelperIpcMessage
    {
        public enum EResult
        {
            Succeeded,

            Cancelled,
            Timeout,

            Failure_Unknown,
            Failure_FailedToInstall,
            Failure_MissingConfigFile,
            Failure_InvalidConfigFile,
            Failure_MissingManifest,
            Failure_ManifestSignatureFailed,
            Failure_FileCorrupt
        }

        public EResult Result;

        public IpcInstallPrereqsComplete(object sender, EResult result) : base(sender)
        {
            Result = result;
        }
    }

    public class IpcRegisterUninstallerComplete : InstallHelperIpcMessage
    {
        public enum EResult
        {
            Succeeded,
            Timeout,

            Failure_Unknown,
            Failure_UnknownException,
            Failure_NoConfigFound,
            Failure_NoEntitlement,
            Failure_NoInstallInfo,
            Failure_RegistryRegistrationFailed
        }

        public EResult Result;

        public IpcRegisterUninstallerComplete(object sender, EResult result) : base(sender)
        {
            Result = result;
        }
    }

    public class IpcSinglePrereqInstallComplete : InstallHelperIpcMessage
    {
        public string ExeName;
        public bool Success;

        public IpcSinglePrereqInstallComplete(object sender, string exeName, bool success) : base(sender)
        {
            ExeName = exeName;
            Success = success;
        }
    }

    public class IpcTerminateAck : InstallHelperIpcMessage
    {
        public IpcTerminateAck(object sender) : base(sender) { }
    }

    public class IpcLog : InstallHelperIpcMessage
    {
        public enum ELogType
        {
            Info,
            Warn,
            Error
        }

        public string Message;
        public ELogType Type;
        public Exception Ex;

        public IpcLog(object sender, ELogType type, string message, Exception ex = null) : base(sender)
        {
            Message = message;
            Type = type;
            Ex = ex;
        }
    }

    public class InstallHelperIpcMessage : IpcMessage
    {
        public InstallHelperIpcMessage(object sender) : base(sender) { }
    }
}
