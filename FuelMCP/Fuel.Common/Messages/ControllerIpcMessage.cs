﻿namespace Amazon.Fuel.Common.Messages
{
    public class IpcInstallPrereqs : ControllerIpcMessage
    {
        public IpcInstallPrereqs(object sender, string manifestFile, string installDir) : base(sender)
        {
            ManifestFile = manifestFile;
            InstallDir = installDir;
        }

        public string ManifestFile;
        public string InstallDir;
    }

    public class IpcRegisterUninstaller : ControllerIpcMessage
    {
        public IpcRegisterUninstaller(object sender, string adgProdId) : base(sender)
        {
            ProductId = adgProdId;
        }

        public string ProductId;
    }

    public class IpcTerminate : ControllerIpcMessage
    {
        public IpcTerminate(object sender) : base(sender) { }
    }

    public class IpcCreateGamesLibraryPrimaryPath : ControllerIpcMessage
    {
        public IpcCreateGamesLibraryPrimaryPath(object sender) : base(sender) { }
    }

    public class IpcEnsureInitialRequirements : ControllerIpcMessage
    {
        public IpcEnsureInitialRequirements(object sender) : base(sender) { }
    }

    public class ControllerIpcMessage : IpcMessage
    {
        public ControllerIpcMessage(object sender) : base(sender) { }
    }
}
