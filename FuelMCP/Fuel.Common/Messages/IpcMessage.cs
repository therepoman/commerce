﻿using Amazon.Fuel.Common.Utilities;
using Equ;
using Newtonsoft.Json;
using System;
using System.Text;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.IPC;
using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    public class IpcMessageDecodeException : Exception
    {
        public IpcMessageDecodeException(string msg, Exception inner) : base(msg, inner) { }
    }

    public class IpcMessageEncodeException : Exception
    {
        public IpcMessageEncodeException(string msg, Exception inner) : base(msg, inner) { }
    }

    public class IpcMessage : MemberwiseEquatable<IpcMessage>, ITinyMessage, IIpcMessage
    {
        [JsonIgnore]
        public object Sender { get; private set; }

        [JsonIgnore]
        public InstallHelperSession Session { get; set; }

        [MemberwiseEqualityIgnore]
        public string ReflectedType;

        public IpcMessage(object sender)
        {
            Sender = sender;
        }

        // Decodes a byte array into an ipc message. Requires a new sender since it is a TinyMessage.
        public static IpcMessage Decode(object newSender, byte[] bytes)
        {
            if ( bytes == null )
            {
                throw new ArgumentNullException("bytes");
            }

            string jsonStr = "";
            try
            {
                if (bytes.Length == 0)
                {
                    throw new Exception("No bytes in message");
                }

                jsonStr = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                IpcMessage baseMsg = JsonConvert.DeserializeObject<IpcMessage>(jsonStr);
                var specificType = Type.GetType(baseMsg.ReflectedType);
                var message = JsonConvert.DeserializeObject(jsonStr, specificType) as IpcMessage;
                if (message == null)
                {
                    throw new IpcMessageDecodeException("Message decode error", null);
                }
                message.Sender = newSender;
                return message;
            }
            catch ( Exception ex )
            {
                FuelLog.Error(ex, "Couldn't decode message", new { jsonStr });
                throw new IpcMessageDecodeException("Message decode error", ex);
            }
        }

        // Encodes a message into a byte array. 
        public virtual byte[] GetBytes()
        {
            try
            {
                // update our reflection before serializing
                // Note: this is the same as GetType().AssemblyQualifiedName, but without version info
                ReflectedType = $"{GetType().FullName}, {GetType().Assembly.GetName().Name}";

                var jsonChars = JsonData.ToCharArray();
                var ret = new byte[jsonChars.Length];
                Encoding.UTF8.GetBytes(jsonChars, 0, jsonChars.Length).CopyTo(ret, 0);

                return ret;
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Couldn't encode message", new { type = this.GetType() });
                throw new IpcMessageEncodeException("Message encode error", ex);
            }
        }

        [JsonIgnore]
        public string JsonData => JsonConvert.SerializeObject(this, Formatting.None);
    }
}
