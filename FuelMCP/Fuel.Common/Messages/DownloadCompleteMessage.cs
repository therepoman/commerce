﻿using System;
using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    public class DownloadCompleteMessage : GenericTinyMessage<String>
    {
        public DownloadCompleteMessage(object sender, string content) : base(sender, content) {}
    }

    public class MessageDownloadCancel : ITinyMessage
    {
        public object Sender { get; set; }
        public readonly ProductId AdgProdId;

        public MessageDownloadCancel(object sender, ProductId adgProdId)
        {
            this.Sender = sender;
            AdgProdId = adgProdId;
        }
    }
}
