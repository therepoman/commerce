﻿using System;
using System.Net;
using System.Threading;
using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    public class SyncGoodsFailureMessage : TinyMessageBase
    {
        public SyncGoodsFailureMessage(object sender,
                                        bool willRetry,
                                        DateTime nextRetry) : base(sender)
        {
            WillRetry = willRetry;
            NextRetry = nextRetry;
        }

        public DateTime NextRetry { get; private set; }
        public bool WillRetry { get; private set; }
    }

    public class SyncGoodsSuccessMessage : TinyMessageBase
    {
        public SyncGoodsSuccessMessage(object sender) : base(sender)
        {
        }
    }
}
