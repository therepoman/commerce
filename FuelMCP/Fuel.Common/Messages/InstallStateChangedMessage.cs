﻿using TinyMessenger;

namespace Amazon.Fuel.Common.Messages
{
    public class InstallStateChangedMessage : ITinyMessage
    {
        public object Sender { get; private set; }
    }
}
