﻿using Amazon.Fuel.Common.CommandLineParser;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Curse.Tools.IPC;
using Polly;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Messages;

namespace Amazon.Fuel.Common.IPC
{
    public class InstallHelperIpcController : IInstallHelperIpcController
    {
        public const int WAIT_TIMEOUT_MS = 15000;
        public const int FORCE_CLOSE_AFTER_MS = 30000;

        private readonly IOSDriver _osDriver;
        private readonly IProcessLauncher _processLauncher;
        private readonly IMessengerHub _hub;
        private readonly IMetricsPlugin _metrics;

        public InstallHelperIpcController(IOSDriver osDriver, IProcessLauncher processLauncher, IMessengerHub hub, IMetricsPlugin metrics)
        {
            _osDriver = osDriver;
            _processLauncher = processLauncher;
            _hub = hub;
            _metrics = metrics;
        }

        public InstallHelperSession Start(IMessengerHub hub, CancellationToken cancelToken = default(CancellationToken))
        {
            // start external elevated helper exe
            // TODO allow multiple exes? Or maybe make the session singleton based
            var session = StartConnection(cancelToken: cancelToken);

            if (session.StatusAfterLaunch != LaunchResult.EStatus.Success )
            {
                return session;
            }

            session.ListenLoopThread = new Thread(() => ReceiveThreadLoop(session))
            {
                IsBackground = true,
                Name = $"InstallHelperIpc_{session.SessionId}"
            };

            session.ListenLoopThread.Start();

            return session;
        }

        public void Stop(InstallHelperSession session, bool sendTerminationMessage)
        {
            FuelLog.Info("Stopping IPC session");
            if (sendTerminationMessage
                && session.IsConnected
                && !session.IsShuttingDown)
            {
                FuelLog.Info("Requesting termination from IPC");
                var response = SendAndWaitForNextAsync<IpcTerminateAck>(
                    session, 
                    new IpcTerminate(this), 
                    timeout: TimeSpan.FromSeconds(10)).GetAwaiter().GetResult();

                FuelLog.Info("IPC terminate response result", new { response.Result });
            }

            StopConnection(session);
        }

        public void Restart(InstallHelperSession session)
        {
            FuelLog.Info("Restarting session", new { session?.PipeId, session?.ProcessId, session?.SessionId} );
            StopConnection(session);
            StartConnection(cancelToken: session.CancelTokenBase, previousSession: session);
        }

        private void StopConnection(InstallHelperSession session)
        {
            if (!session.IsShuttingDown)
            {
                return;
            }

            FuelLog.Info("Stopping session", new { session?.PipeId, session?.ProcessId, session?.SessionId });

            session.IsShuttingDown = true;
            session.AbortTokenSource.Cancel();

            session.IpcServer?.Dispose();
            session.IpcServer = null;

            if (session.ProcessId > 0)
            {
                // kill the old session, but don't wait on it
                Task.Run(() => WatchForTerminationAsync(session));
            }
        }

        private InstallHelperSession StartConnection(
            InstallHelperSession previousSession = null,    // allow reuse of old session
            CancellationToken cancelToken = default(CancellationToken))
        {
            FuelLog.Info("Starting session", new { previousSession?.PipeId, previousSession?.ProcessId, previousSession?.SessionId });

            var abortTokenSource = new CancellationTokenSource();
            var session = previousSession ?? new InstallHelperSession()
            {
                Hub = _hub,
            };

            session.CancelTokenBase = cancelToken;
            session.AbortTokenSource = abortTokenSource;
            session.CancelToken = CancellationTokenSource.CreateLinkedTokenSource(
                                    abortTokenSource.Token, cancelToken).Token;

            lock (session.SendLock)
            {
                // pipe name should be unique, supporting multiple per session
                session.PipeId = $"FuelHelper_{Guid.NewGuid().ToString()}";
                session.IpcServer = null;

                try
                {
                    session.IpcServer = new BufferedIPC(IPCType.Server, session.PipeId);
                }
                catch
                {
                    _metrics.AddCounter("InstallHelperIpcController", "IpcInitFailed");
                    FuelLog.Error("Couldn't open server pipe");
                    throw;
                }

                using ( var result = LaunchHelperApp(session.PipeId) )
                {
                    session.ProcessId = result.Process?.Id ?? -1;
                    session.StatusAfterLaunch = result.Status;

                    if (session.StatusAfterLaunch != LaunchResult.EStatus.Success)
                    {
                        FuelLog.Error("Failed to launch helper app", new { status = result.Status });
                        return session;
                    }

                    session.UptimeStopwatch.Start();

                    // connect to client
                    if (!session.IpcServer.Wait(timeout: WAIT_TIMEOUT_MS))
                    {
                        _metrics.AddCounter("InstallHelperIpcController", "IpcInitTimeout");
                        throw new Exception("Couldn't connect IPC (timeout)");
                    }
                    session.IsConnected = true;
                }
            }

            FuelLog.Info("Process started", new { status = session.StatusAfterLaunch, pid = session.ProcessId });

            return session;
        }

        public bool Send(InstallHelperSession session, ControllerIpcMessage msg)
        {
            bool success;

            if (session.IsTerminationRequested)
            {
                FuelLog.Warn("Sending messages after termination request");
            }

            if (session.IsShuttingDown)
            {
                FuelLog.Error("Sending messages during shutdown");
                return false;
            }

            if (msg is IpcTerminate)
            {
                session.IsTerminationRequested = true;
            }

            lock (session.SendLock)
            {
                FuelLog.Info("Sending InstallHelper command", new { type = msg.GetType() });
                WrapConnectionPolicy(
                    () => session.IpcServer.Write(msg.GetBytes()), 
                    session, 
                    out success);
            }

            return success;
        }

        public async Task<SendAndWaitResponse<THelperMessage>> SendAndWaitForNextAsync<THelperMessage>(
                InstallHelperSession session, 
                ControllerIpcMessage sendMessage,
                TimeSpan timeout = default(TimeSpan)) 
            where THelperMessage : InstallHelperIpcMessage
        {
            var ret = new SendAndWaitResponse<THelperMessage>();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            // TODO : timeouts?
            FuelLog.Info("Sending IPC message", new {type = sendMessage.GetType()});

            Send(session, sendMessage);
            FuelLog.Info("Waiting to receive IPC message", new {type = typeof(THelperMessage)});

            bool timedOut = false;
            while (!session.CancelToken.IsCancellationRequested
                && session.IsConnected
                && !timedOut)
            {
                ret.Message = session.TryTake<THelperMessage>();

                if (ret.Message != default(THelperMessage))
                {
                    break;
                }

                await Task.Delay(TimeSpan.FromMilliseconds(100));
                timedOut = timeout != default(TimeSpan) && timeout.TotalMilliseconds <= sw.ElapsedMilliseconds;
            }

            sw.Stop();

            if (ret.Message == null || timedOut)
            {
                if (!session.IsConnected)
                {
                    ret.Result = SendAndWaitResponse<THelperMessage>.EResult.Failure_LostConnection;
                    _metrics.AddCounter("InstallHelperIpcController", "IpcLostConnection");
                }
                else
                {
                    ret.Result = SendAndWaitResponse<THelperMessage>.EResult.Failure_Timeout;
                    _metrics.AddCounter("InstallHelperIpcController", "IpcMsgTimeout");
                }
            }

            if (ret.Result == SendAndWaitResponse<THelperMessage>.EResult.Succeeded)
            {
                FuelLog.Info("Response result successful", new { timeSec = sw.Elapsed.TotalSeconds, cancelled = session.CancelToken.IsCancellationRequested });
            }
            else
            {
                FuelLog.Error("Response result (failure)", new { result = ret.Result, timeSec = sw.Elapsed.TotalSeconds, cancelled = session.CancelToken.IsCancellationRequested });
            }

            return ret;
        }

        public async Task WatchForTerminationAsync(InstallHelperSession session)
        {
            // this just watches a process for termination without a reasonable timeframe
            // (we can't actually kill the process, since it is elevated)
            if (session.ProcessId > 0)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                // give it some time to close gracefully, else kill it forceably
                bool exited = false;
                while (sw.ElapsedMilliseconds < FORCE_CLOSE_AFTER_MS)
                {
                    try
                    {
                        if (Process.GetProcessById(session.ProcessId).HasExited)
                        {
                            exited = true;
                            break;
                        }
                    }
                    catch
                    {
                        // handle can't find process exceptions
                        exited = true;
                        break;
                    }

                    await Task.Delay(100);
                }

                if (!exited)
                {
                    FuelLog.Error("Helper process still running after attempted termination");
                    _metrics.AddCounter("InstallHelperIpcController", "ProcessTerminateFailed");
                }
                else
                {
                    // process not running, all good
                    session.ProcessId = -1;
                }
            }
        }

        private LaunchResult LaunchHelperApp(string pipeId)
        {
            FuelLog.Info("Starting helper process", new { pipeId = pipeId, devMode = _osDriver.IsDevMode });

            return _processLauncher.LaunchProcess(
                description: "Twitch Helper App",
                launchFolder: null,
                pathToFile: Path.Combine(Assembly.GetExecutingAssembly().Location, _osDriver.GameInstallHelperPath),
                args: new TwitchInstallHelperArg()
                {
                    DevMode = _osDriver.IsDevMode,
                    AppDataLocationOverride = _osDriver.EnvironmentApplicationData,
                    IpcPipeName = pipeId,
#if DEBUG
                    //                        AttachDebugger = true
#endif
                }.Args,
                waitForExit: false,
                validateAgainstReturnCodes: new int[] { 0 },
                envVars: null,
                eLaunchUac: ELaunchUAC.RUN_WITH_CURRENT_PRIVS__THIS_ALLOWS_ELEVATION,
                bringToForeground: false,
                hideWindow: !_osDriver.IsDevMode
            );
        }

        // handle exceptions thrown by ipc requests for a given session (can fail after retry attempts)
        private TRet WrapConnectionPolicy<TRet>(Func<TRet> lambda, InstallHelperSession session, out bool succeeded)
        {
            succeeded = true;
            try
            {
                return Policy
                    .Handle<IOException>()
                    .WaitAndRetry(1, i => TimeSpan.FromSeconds(2.0),
                        onRetry: (e, ts) => // try to restart the helper just once if the pipe is broken
                        {
                            if (!session.IsTerminationRequested)
                            {
                                session.IsConnected = false;
                                FuelLog.Error(e, "IPC pipe unexpectedly closed, attempting to relaunch helper exe");
                                _metrics.AddCounter("InstallHelperIpcController", "IpcPipeClosedRecoverable");

                                Restart(session);

                                if (session.StatusAfterLaunch != LaunchResult.EStatus.Success)
                                {
                                    FuelLog.Error("Failed to launch helper app", new { status = session.StatusAfterLaunch });
                                    throw new Exception("Couldn't launch helper app");
                                }
                            }
                        })      
                    .Execute(lambda);
            }
            catch (Exception ex)
            {
                if (!session.IsTerminationRequested)
                {
                    FuelLog.Error(ex, "Lost IPC connection", new { pipe = session.PipeId, uptime = session.UptimeStopwatch.Elapsed.TotalSeconds });
                    session.IsConnected = false;
                }
            }

            succeeded = false;
            return default(TRet);
        }

        private void ReceiveThreadLoop(InstallHelperSession session)
        {
            // TODO timeout/disconnection/reconnection/other error handling

            Stopwatch sw = new Stopwatch();
            sw.Start();

            FuelLog.Info("Entering IPC receive loop");

            while (!session.CancelToken.IsCancellationRequested 
                && !session.IsShuttingDown)
            {
                if (!session.IsConnected)
                {
                    Thread.Sleep(100);
                    continue;
                }

                bool success;
                byte[] responseBytes = WrapConnectionPolicy(() => { return session.IpcServer.Read(); }, session, out success);

                if (!success)
                {
                    FuelLog.Warn("read not successful, terminating receive loop");
                    break;
                }

                FuelLog.Info("Received message", new { size = responseBytes.Length });

                try
                {
                    var responseMsg = IpcMessage.Decode(newSender: this, bytes: responseBytes);
                    FuelLog.Info("Decoded message, publishing", new {type = responseMsg.GetType()});

                    responseMsg.Session = session;

                    session.AddResponseMessage(responseMsg);
                }
                catch (Exception ex)
                {
                    // TODO : should we fail the whole install here? A failed response could lock the install forever
                    FuelLog.Error(ex, "Couldn't decode response message");
                    _metrics.AddCounter("InstallHelperIpcController", "IpcResponseDecodeFailed");
                }
            }

            FuelLog.Info("Exiting IPC receive loop");
            StopConnection(session);
        }
    }
}
