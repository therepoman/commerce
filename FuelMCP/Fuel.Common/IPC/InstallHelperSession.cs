using System.Diagnostics;
using System.Threading;
using Amazon.Fuel.Common.Messages;
using Curse.Tools.IPC;
using System.Collections.Generic;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.IPC
{
    public class InstallHelperSession : Disposable
    {
        private static int _sessionIdNext = 1;

        public int SessionId { get; private set; }
        public IMessengerHub Hub { get; set; }
        public int ProcessId { get; set; }
        public string PipeId { get; set; }
        public BufferedIPC IpcServer { get; set; }
        public LaunchResult.EStatus StatusAfterLaunch { get; set; }
        public CancellationToken CancelToken { get; set; }
        public CancellationToken CancelTokenBase { get; set; }
        public CancellationTokenSource AbortTokenSource { get; set; }
        public Thread ListenLoopThread { get; set; }
        public Stopwatch UptimeStopwatch { get; private set; } = new Stopwatch();
        public object SendLock { get; set; } = new {};

        public bool IsConnected { get; set; } = false;
        public bool IsShuttingDown { get; set; } = false;
        public bool IsTerminationRequested { get; set; } = false;

        private readonly List<IpcMessage> _unhandledReceivedMessages = new List<IpcMessage>();
        private readonly object _unhandledReceivedMessagesLock = new {};

        public InstallHelperSession()
        {
            SessionId = _sessionIdNext++;
        }

        // searches for a particular message type, removes and returns it, else returns the default
        // if not found
        public TIpcMessageType TryTake<TIpcMessageType>() where TIpcMessageType : IpcMessage
        {
            lock (_unhandledReceivedMessagesLock)
            {
                var idx = _unhandledReceivedMessages.FindIndex(m => m is TIpcMessageType);
                if (idx >= 0)
                {
                    var ret = _unhandledReceivedMessages[idx];
                    _unhandledReceivedMessages.RemoveAt(idx);
                    return (TIpcMessageType)ret;
                }

                return default(TIpcMessageType);
            }
        }

        public void AddResponseMessage(IpcMessage msg)
        {
            lock (_unhandledReceivedMessagesLock)
            {
                _unhandledReceivedMessages.Add(msg);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            IpcServer?.Dispose();
            IpcServer = null;
        }
    }
}