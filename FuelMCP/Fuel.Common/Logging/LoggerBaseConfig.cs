﻿using System.IO;
using Serilog;
using System;
using Amazon.Fuel.Common.Contracts;
using Serilog.Core;
using Serilog.Enrichers;
using Serilog.Events;
using Amazon.Fuel.Common.Configuration;
using Amazon.Fuel.Common.Logging.Sinks;
using Amazon.Fuel.Common.Logging.Uploaders;
using Amazon.Fuel.Common.Logging;

namespace Amazon.Fuel.Plugin.Logger
{
    public class LoggerBaseConfig : ILoggerBaseConfig
    {
        private readonly IOSDriver _osDriver;

        private LoggingLevelSwitch _levelSwitch;
        private CrashReporterWorker _crashReporterWorker;
        private readonly bool _devMode = false;
        private readonly string _loggerPrefix;

        private readonly Curse.Logging.LogCategory _curseLogger;

        public LoggerBaseConfig(IOSDriver osDriver, Curse.Logging.LogCategory curseLogger, string loggerPrefix = "", bool devMode = false)
        {
            _osDriver = osDriver;

            _loggerPrefix = loggerPrefix;

            _devMode = devMode;

            if (_devMode)
            {
                Serilog.Debugging.SelfLog.Enable(Console.Error);
            }

            _levelSwitch = new LoggingLevelSwitch(devMode ? LogEventLevel.Debug : LogEventLevel.Information);
            _curseLogger = curseLogger;

            recreateLogger();
        }

        public void FlushAndRecreateLogger()
        {
            Log.CloseAndFlush();
            recreateLogger();
        }

        private void recreateLogger()
        {

            var detector = new ReallyOptimisticNetworkDetector();

            var logUploader = new LogUploader(new System.Net.Http.HttpClient(), new AppInfo(), detector);
            IAmazonLogs amazonLogs = null;

            var devModeStr = _devMode ? "_NoUpload" : "";
            string logsPath = Path.Combine(
                _osDriver.CurseLogDirectory,
                $"Fuel_TrimLogs{devModeStr}");

            string dumpsPath = Path.Combine(
                _osDriver.AppDataPathFull,
                $"Dumps{devModeStr}");

            if (!Directory.Exists(logsPath))
            {
                Directory.CreateDirectory(logsPath);
            }

            if (!Directory.Exists(dumpsPath))
            {
                Directory.CreateDirectory(dumpsPath);
            }

            // Initialize generic logger
            var loggerCfg = new LoggerConfiguration()
                .Enrich.With(new ThreadIdEnricher())
                .MinimumLevel.ControlledBy(_levelSwitch);

            if (!_devMode)
            {
                loggerCfg = loggerCfg
                    .WriteTo.AmazonLog(
                        logUploader: logUploader,
                        pathFormat: Path.Combine(logsPath, $"{_loggerPrefix}fuel.log"),
                        retainedFileCountLimit: 10,
                        rollSpan: TimeSpan.FromMinutes(60),
                        registerSink: (s) => { amazonLogs = s; })
                    .WriteTo.CrashReport(dumpsPath);
            }
            else
            {
                loggerCfg = loggerCfg
                    .WriteTo.AmazonSink(path: logsPath, prefix: "fuel_dev_mode-")
                    .WriteTo.LiterateConsole();
            }

            loggerCfg = loggerCfg
                    .WriteTo.CurseLog(_curseLogger)
                    .ReadFrom.AppSettings(); // see https://github.com/serilog/serilog/wiki/AppSettings

            if (amazonLogs != null)
                amazonLogs.SendLogsForProcessing();

            //_streamingLogger.Config(loggerCfg.WriteTo);
            Log.Logger = loggerCfg.CreateLogger();

            if (!_devMode)
            { 
                _crashReporterWorker = new CrashReporterWorker(
                    new CrashReportUploader(new System.Net.Http.HttpClient(), new AppInfo(), new ReallyOptimisticNetworkDetector()),
                    dumpsPath,
                    Log.Logger);
            }
        }
    }
}
