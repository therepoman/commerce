﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Ext.Auth.Map;
using Serilog;

namespace Amazon.Fuel.Common.Logging.Uploaders
{
    public class CrashReportUploader : DetUploader
    {
        public CrashReportUploader(HttpClient httpClient, IAppInfo appInfo, INetworkDetector detector)
            : base(httpClient, appInfo, detector, ContentTypeCrashReport, "crash")
        {
        }
    }

    public class KindleLogFormat10Uploader : DetUploader
    {
        public KindleLogFormat10Uploader(HttpClient httpClient, IAppInfo appInfo, INetworkDetector detector)
            : base(httpClient, appInfo, detector, ContentTypeKindleLogFormat10, "metrics")
        {
        }
    }

    public class LogUploader : DetUploader
    {
        public LogUploader(HttpClient httpClient, IAppInfo appInfo, INetworkDetector detector)
            : base(httpClient, appInfo, detector, ContentTypeUserLogs, "log")
        {
        }
    }

    // https://w.amazon.com/index.php/DeviceMetricsAndLogging/DeviceEventTracker/AnonymousLogUploads
    public abstract class DetUploader : ILogUploader
    {
        public const string BaseAddress = "https://det-ta-g7g.amazon.com/DeviceEventProxy/DETLogServlet";
        public const char UnixNewLine = '\n';

        public const string ContentTypeCrashReport = "CrashReport";
        public const string ContentTypeKindleLogFormat10 = "KindleLogFormat-1.0";
        public const string ContentTypeBinaryFile = "BinaryFile";
        public const string ContentTypeUserLogs = "Logs";

        private readonly IAppInfo _appInfo;
        private readonly HttpClient _httpClient;
        private readonly INetworkDetector _detector;
        private readonly string _contentType;
        private readonly string _uploadTag;

        protected DetUploader(HttpClient httpClient, IAppInfo appInfo, INetworkDetector detector, string contentType,
            string uploadTag = "")
        {
            _httpClient = httpClient;
            _appInfo = appInfo;
            _detector = detector;
            _contentType = contentType;
            _uploadTag = uploadTag;
        }

        public async Task<bool> Upload(string filePath)
        {
            if (!_detector.IsAvailable)
                return false;

            var fileInfo = new FileInfo(filePath);
            if (!fileInfo.Exists)
                return false;

            if (fileInfo.Length == 0)
            {
                try
                {
                    File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    Log.Warning(ex, $"Couldn't delete empty file at {filePath}");
                }

                Log.Information("deleting log file {filePath} because it was empty", filePath);

                return true;
            }

            var httpRequestMsg = await CreateRequestAsync(filePath);
            if (httpRequestMsg == null)
                return false;

            var fileUploaded = false;

            try
            {
                var httpResponse = await _httpClient.SendAsync(httpRequestMsg);

                fileUploaded = httpResponse.IsSuccessStatusCode;

                if (fileUploaded)
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception ex)
                    {
                        Log.Warning(ex, $"Couldn't delete old file at {filePath}");
                    }
                }
            }
            catch (HttpRequestException ex)     // these may be thrown if offline
            {
                // this is semi-reliable, though it really only determines if the OS thinks it is "up", and not if you can hit real endpoints.
                var isAdapterConnected = NetworkInterface.GetIsNetworkAvailable();
                if (!isAdapterConnected)
                { 
                    Log.Information(ex, $"Failed to upload {filePath} to DET, network is offline");
                }
                else
                {
                    Log.Warning(ex, $"Failed to upload {filePath} to DET, network or upload server is unavailable");
                }
            }
            catch (Exception exc)
            {
                Log.Error(exc, "Failed to upload {0} to DET.", filePath);
            }

            return fileUploaded;
        }

        public static Uri GetUri(string name, string baseAddress)
        {
            var baseUri = new UriBuilder(baseAddress);
            var queryToAppend = "key=" + Guid.NewGuid().ToString("n").Substring(0, 8) + name;

            if (!string.IsNullOrWhiteSpace(baseUri.Query))
            {
                baseUri.Query = baseUri.Query.Substring(1) + "&" + queryToAppend;
            }
            else
            {
                baseUri.Query = queryToAppend;
            }

            return baseUri.Uri;
        }

        private async Task<HttpRequestMessage> CreateRequestAsync(string filePath)
        {
            var fileName = Path.GetFileName(filePath);
            HttpRequestMessage request;

            try
            {
                using (var ms = new MemoryStream())
                using (var sw = new StreamWriter(ms, Constants.UTF8NoBOM))
                using (var contentStream = await GetContentStream(filePath))
                using (var contentReader = new StreamReader(contentStream, Constants.UTF8NoBOM))
                {
                    var content = await contentReader.ReadToEndAsync();
                    var contentLength = content.Length;

                    sw.Write("MFBS/1.0 {1}{0}", UnixNewLine, 1 /* number of files */);
                    sw.Write("Content-Name: {1}{0}", UnixNewLine, "Content");
                    sw.Write("Content-Encoding: {1}{0}", UnixNewLine, "text");
                    sw.Write("Content-Length: {1}{0}", UnixNewLine, contentLength);
                    sw.Write(UnixNewLine);
                    sw.Write(UnixNewLine);
                    sw.Write(content);
                    sw.Flush();

                    // create request
                    request = new HttpRequestMessage(HttpMethod.Post, GetUri(fileName, BaseAddress));
                    request.Headers.Add("User-Agent", _appInfo.HttpUserAgentName);

                    // add content
                    request.Content = new ByteArrayContent(ms.ToArray());
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

                    // add custom headers
                    request.Headers.Add("X-DSN", _appInfo.DeviceSerialId);
                    request.Headers.Add("X-DeviceType", _appInfo.DeviceTypeId);
#if DEBUG
                     request.Headers.Add("X-DeviceFirmwareVersion", "dev");
#else
                    request.Headers.Add("X-DeviceFirmwareVersion", _appInfo.Version);
#endif
                    request.Headers.Add("X-Content-Type", _contentType);
                    request.Headers.Add("X-Upload-Tag", _uploadTag);
                }
            }
            catch (IOException exc)
            {
                request = null;
                Log.Error(exc, "Failed to upload {0} to DET.", filePath);
            }

            return request;
        }

        protected virtual Task<Stream> GetContentStream(string filePath)
        {
            return Task.FromResult((Stream)File.OpenRead(filePath));
        }
    }
}
