﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Logging.Uploaders
{
    public interface ILogUploader
    {
        Task<bool> Upload(string fileToUpload);
    }
}
