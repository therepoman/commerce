﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Ext.Auth.Map;

namespace Amazon.Fuel.Common.Logging.Uploaders
{
    // TODO we should probably write a real detector like those cloud drive folks did but for now...
    public class ReallyOptimisticNetworkDetector : INetworkDetector
    {
        public bool IsAvailable
        {
            get
            {
                return true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Task<bool> CheckAvailabilityAsync(bool force = false)
        {
            return Task.FromResult(true);
        }

        public void Dispose()
        {
            // NOOP
        }

        public IDisposable Subscribe(IObserver<NetworkAvailabilityEventArgs> observer)
        {
            // Log uploader does not call this.
            throw new NotImplementedException();
        }

        public IObservable<NetworkAvailabilityEventArgs> WaitUntilNetworkIsAvailable()
        {
            // Log uploader does not call this.
            throw new NotImplementedException();
        }

        public IObservable<NetworkAvailabilityEventArgs> WaitUntilNetworkIsAvailable(TimeSpan timeout)
        {
            // Log uploader does not call this.
            throw new NotImplementedException();
        }

        public IObservable<NetworkAvailabilityEventArgs> WaitUntilNetworkIsAvailable(int timeoutMilliseconds)
        {
            // Log uploader does not call this.
            throw new NotImplementedException();
        }
    }
}
