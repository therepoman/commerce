﻿using System;
using System.Collections.Generic;
using Amazon.Fuel.Common.Configuration;
using Serilog;
using Serilog.Events;

namespace Amazon.Fuel.Common.Logging
{
    /// <summary>
    /// Logger for Performance Metrics (timers and counters).
    /// </summary>
    public static class PmetLog
    {
        private static ILogger _logger;
        private static readonly AppInfo AppInfo = new AppInfo();

        public static ILogger Logger
        {
            get
            {
                return PmetLog._logger;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");
                PmetLog._logger = value;
            }
        }

        public static void WriteOneShotEvent(string message)
        {
            /* TODO: https://twitchtv.atlassian.net/browse/FUEL-1234
            var counters = new Dictionary<string, long> { { message, 1 } };
            LogEvent deviceMetric = new PmetEvent(AppInfo, message, counters);
            Logger.Write(deviceMetric);
            */
        }
    }
}
