﻿using System;
using Serilog.Configuration;
using Amazon.Fuel.Common.Logging.Sinks;
using Serilog.Events;
using Serilog;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    /// <summary>
    /// Adds the WriteTo.CurseLog() extension methid to <see cref="LoggerConfiguration"/>.
    /// </summary>
    public static class CurseLogExtension
    {
        private const string DefaultOutputTemplate = "[{SourceContext}] {Message}";

        public static LoggerConfiguration CurseLog(this LoggerSinkConfiguration loggerConfiguration,
            Curse.Logging.LogCategory curseLogger,
            LogEventLevel restrictedToMinimumLevel = LevelAlias.Minimum,
            string outputTemplate = DefaultOutputTemplate,
            IFormatProvider formatProvider = null)
        {
            var formatter = new CurseTextFormatter(outputTemplate, formatProvider);
            var sink = new CurseLogSink(curseLogger, formatter);

            return loggerConfiguration.Sink(sink, restrictedToMinimumLevel);
        }
    }
}
