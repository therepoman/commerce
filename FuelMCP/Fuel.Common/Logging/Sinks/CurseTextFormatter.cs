﻿using Serilog.Events;
using Serilog.Formatting;
using Serilog.Formatting.Display;
using Serilog.Parsing;
using System;
using System.Collections.Generic;
using System.IO;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public class CurseTextFormatter : ITextFormatter
    {
        private readonly IFormatProvider _formatProvider;
        private readonly MessageTemplate _outputTemplate;

        public CurseTextFormatter(string outputTemplate, IFormatProvider formatProvider)
        {
            _outputTemplate = new MessageTemplateParser().Parse(outputTemplate);
            _formatProvider = formatProvider;
        }

        /// <summary>
        /// Format the log event into the output.
        ///
        /// </summary>
        /// <param name="logEvent">The event to format.</param><param name="output">The output.</param>
        public void Format(LogEvent logEvent, TextWriter output)
        {
            var outputProperties = FilteredOutputProperties.GetUsernameFilteredOutputProperties(logEvent);

            foreach (var messageTemplateToken in _outputTemplate.Tokens)
            {
                var propertyToken = messageTemplateToken as PropertyToken;
                if (propertyToken == null)
                {
                    messageTemplateToken.Render(outputProperties, output, _formatProvider);
                }
                else if (propertyToken.PropertyName == OutputProperties.LevelPropertyName ||
                    propertyToken.PropertyName == OutputProperties.TimestampPropertyName ||
                    propertyToken.PropertyName == OutputProperties.ExceptionPropertyName)
                {
                    continue; // Curse logger will take care of writting out the level, timestamp, and exception (if any)
                }
                else
                {
                    LogEventPropertyValue eventPropertyValue;
                    if (!outputProperties.TryGetValue(propertyToken.PropertyName, out eventPropertyValue))
                        continue;

                    var scalarValue = eventPropertyValue as ScalarValue;
                    if (scalarValue != null && scalarValue.Value is string)
                    {
                        var dictionary = new Dictionary<string, LogEventPropertyValue> {
                             {
                                 propertyToken.PropertyName,
                                 new LiteralStringValue(scalarValue.Value.ToString().ReplaceCaseInsensitive(Environment.UserName, "<OMIT>"))
                             }
                         };
                        messageTemplateToken.Render(dictionary, output, _formatProvider);
                    }
                    else
                    {
                        messageTemplateToken.Render(outputProperties, output, _formatProvider);
                    }
                }
            }
        }
    }
}
