﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.Fuel.Common.Utilities;
using Serilog.Events;
using Serilog.Formatting;
using Serilog.Formatting.Display;
using Serilog.Parsing;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    /// <summary>
    /// A <see cref="T:Serilog.Formatting.ITextFormatter"/> that supports the Serilog
    ///             message template format. Formatting log events for display
    ///             has a different set of requirements and expectations from
    ///             rendering the data within them. To meet this, the formatter
    ///             overrides some behavior: First, strings are always output
    ///             as literals (not quoted) unless some other format is applied
    ///             to them. Second, tokens without matching properties are skipped
    ///             rather than being written as raw text.
    /// </summary>
    public class AmazonTextFormatter : ITextFormatter
    {
        private static readonly IDictionary<LogEventLevel, string> Levels = new Dictionary<LogEventLevel, string> {
             {LogEventLevel.Verbose, "Verbose"},
             {LogEventLevel.Debug, "Debug"},
             {LogEventLevel.Information, " Info"},
             {LogEventLevel.Warning, " Warn"},
             {LogEventLevel.Error, "Error"},
             {LogEventLevel.Fatal, "Fatal"}
         };

        private readonly IFormatProvider _formatProvider;
        private readonly MessageTemplate _outputTemplate;

        /// <param name="outputTemplate">A message template describing the
        ///             output messages.</param><param name="formatProvider">Supplies culture-specific formatting information, or null.</param>
        public AmazonTextFormatter(string outputTemplate, IFormatProvider formatProvider)
        {
            _outputTemplate = new MessageTemplateParser().Parse(outputTemplate);
            _formatProvider = formatProvider;
        }

        /// <summary>
        /// Format the log event into the output.
        ///
        /// </summary>
        /// <param name="logEvent">The event to format.</param><param name="output">The output.</param>
        public void Format(LogEvent logEvent, TextWriter output)
        {
            var outputProperties = FilteredOutputProperties.GetUsernameFilteredOutputProperties(logEvent);

            foreach (var messageTemplateToken in _outputTemplate.Tokens)
            {
                var propertyToken = messageTemplateToken as PropertyToken;
                if (propertyToken == null)
                {
                    messageTemplateToken.Render(outputProperties, output, _formatProvider);
                }
                else if (propertyToken.PropertyName == OutputProperties.LevelPropertyName)
                {
                    output.Write(Levels[logEvent.Level]);
                }
                else
                {
                    LogEventPropertyValue eventPropertyValue;
                    if (!outputProperties.TryGetValue(propertyToken.PropertyName, out eventPropertyValue))
                        continue;

                    var scalarValue = eventPropertyValue as ScalarValue;
                    if (scalarValue != null && scalarValue.Value is string)
                    {
                        var dictionary = new Dictionary<string, LogEventPropertyValue> {
                            {
                                propertyToken.PropertyName,
                                new LiteralStringValue(scalarValue.Value.ToString().ReplaceCaseInsensitive(Environment.UserName, "<OMIT>"))
                            }
                        };
                        messageTemplateToken.Render(dictionary, output, _formatProvider);
                    }
                    else
                    {
                        messageTemplateToken.Render(outputProperties, output, _formatProvider);
                    }
                }
            }
        }
    }
}