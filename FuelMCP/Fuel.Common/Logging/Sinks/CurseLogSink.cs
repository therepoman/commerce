﻿using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog.Events;
using Curse.Logging;
using Serilog.Formatting;
using System.IO;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public class CurseLogSink : ILogEventSink
    {
        private readonly LogCategory _curseLogger;
        private readonly ITextFormatter _textFormatter;

        public CurseLogSink(LogCategory curseLogger, ITextFormatter textFormatter)
        {
            _curseLogger = curseLogger;
            _textFormatter = textFormatter;
        }

        public void Emit(LogEvent logEvent)
        {
            _curseLogger.Log(getCurseLevel(logEvent.Level),
                time: logEvent.Timestamp.DateTime,
                ex: logEvent.Exception,
                message: getMessage(logEvent),
                data: null);
        }

        private String getMessage(LogEvent logEvent)
        {
            StringWriter writer = new StringWriter();
            _textFormatter.Format(logEvent, writer);
            return writer.ToString();
        }

        private LogLevel getCurseLevel(LogEventLevel level)
        {
            switch (level)
            {
                case LogEventLevel.Debug:
                    return LogLevel.Debug;
                case LogEventLevel.Warning:
                    return LogLevel.Warn;
                case LogEventLevel.Error:
                    return LogLevel.Error;
                case LogEventLevel.Fatal:
                    return LogLevel.Fatal;
                default:
                    return LogLevel.Info;
            }
        }
    }
}
