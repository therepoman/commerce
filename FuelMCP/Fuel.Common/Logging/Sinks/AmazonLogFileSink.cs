﻿using System;
using Serilog.Formatting;
using System.Text;
using System.Globalization;
using Amazon.Fuel.Common.Configuration;
using Amazon.Fuel.Common.Utilities;
using Microsoft.Win32;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public class AmazonLogFileSink : FileSink
    {
        private readonly IAppInfo _appInfo = new AppInfo();

        public AmazonLogFileSink(string path, ITextFormatter textFormatter, long? fileSizeLimitBytes, Encoding encoding = null, Action fileSizeLimitExceededCallBack = null)
            : base(path, textFormatter, fileSizeLimitBytes, encoding, fileSizeLimitExceededCallBack)
        {
        }

        protected override void AddHeader(System.IO.StreamWriter writer)
        {
            var domain = "twitch";
            var region = RegionInfo.CurrentRegion.TwoLetterISORegionName;
            var timestamp = DateTimeOffset.UtcNow;

            writer.Write(
                "[Info]{0}" +
                "OS: {1}{0}" +
                "OSProductName: {2}{0}" +
                "DotNet: {3}{0}" +
                "DotNet45OrLater: {4}{0}" +
                "DSN: {5}{0}" +
                "DeviceType: {6}{0}" +
                "AppVersion: {7}{0}" +
                "Region: {8}{0}" +
                "Domain: {9}{0}" +
                "TimeUtc: {10}{0}" +
                "CPU: {11}{0}" +
                "ARCH: {12}{0}" +
                "CORES: {13}{0}" +
                "{0}",
                Environment.NewLine,
                Environment.OSVersion.VersionString + "/" + (Environment.Is64BitOperatingSystem ? "64" : "32"),
                WindowsAppUtilities.OsProductName,
                Environment.Version.ToString(),
                WindowsAppUtilities.Get45PlusFromRegistry(),
                _appInfo.DeviceSerialId,
                _appInfo.DeviceTypeId,
                _appInfo.Version,
                region,
                domain,
                timestamp.ToUnixTime().ToString(),
                Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER"),
                Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"),
                Environment.ProcessorCount
            );
        }
    }
}