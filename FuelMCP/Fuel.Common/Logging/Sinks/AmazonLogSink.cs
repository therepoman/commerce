﻿using Amazon.Fuel.Common.Logging.Uploaders;
using Serilog.Debugging;
using Serilog.Events;
using Serilog.Formatting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public interface IAmazonLogs
    {
        Task<bool> SendLogsForProcessing();
    }

    public class AmazonLogSink : RollingFileSinkWithCallback<LogEvent>, IAmazonLogs
    {
        private readonly LogUploader _logUploader;
        private readonly bool _devMode = false;

        public AmazonLogSink(string pathFormat, LogUploader logUploader, ITextFormatter formatter, long fileSizeLimitBytes, int retainedFileCountLimit, Encoding encoding = null, TimeSpan? rollSpan = null)
            : base(pathFormat, formatter, fileSizeLimitBytes, retainedFileCountLimit, encoding, rollSpan)
        {
#if DEBUG
            _devMode = true;
#endif

            _logUploader = logUploader;
            Roll += (s, log) => {
                string[] upload = { log };
                ProcessFilesAsync(upload);
            };
            RollFileOnNextEmit();
        }

        protected override FileSink CreateNewFileSink(string path, ITextFormatter textFormatter, long? fileSizeLimitBytes, Encoding encoding = null, Action fileSizeLimitExceededCallBack = null)
        {
            return new AmazonLogFileSink(path, textFormatter, fileSizeLimitBytes, encoding, fileSizeLimitExceededCallBack);
        }

        public override async Task<bool> ProcessFilesAsync(IEnumerable<string> potentialMatches)
        {
            if (_devMode)
            {
                return false;
            }

            var uploaded = true;

            foreach (var fileToUpload in potentialMatches)
            {
                try
                {
                    var isFileUploaded = await _logUploader.Upload(fileToUpload);
                    if (!isFileUploaded)
                    {
                        SelfLog.WriteLine("Failed to upload file {0}", fileToUpload);
                        uploaded = false;
                    }
                    else
                    {
                        File.Delete(fileToUpload);
                    }
                }
                catch (Exception ex)
                {
                    SelfLog.WriteLine("Error {0} while uploading file {1}", ex, fileToUpload);
                    uploaded = false;
                }
            }

            return uploaded;
        }
    }
}
