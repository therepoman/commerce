﻿using System;
using Serilog.Configuration;
using Amazon.Fuel.Common.Logging.Sinks;

namespace Serilog
{
    /// <summary>
    /// Adds the WriteTo.CrashReport() extension method to <see cref="LoggerConfiguration"/>.
    /// </summary>
    public static class CrashReportExtension
    {
        /// <summary>
        /// Adds a sink for writing CrashReportEvents to a file which can be uploaded to Crashboard.
        /// </summary>
        /// <param name="loggerConfiguration">The logger configuration.</param>
        /// <param name = "path">The location of the file to write to</param>
        /// <returns>Logger configuration, allowing configuration to continue.</returns>
        /// <exception cref="ArgumentNullException">A required parameter is null.</exception>
        public static LoggerConfiguration CrashReport(this LoggerSinkConfiguration loggerConfiguration, string path)
        {
            return loggerConfiguration.Sink(new CrashReportSink(path));
        }
    }
}