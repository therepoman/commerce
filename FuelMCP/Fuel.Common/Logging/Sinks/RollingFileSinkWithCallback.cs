﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog.Formatting;
using Serilog.Events;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public class RollingFileSinkWithCallback<T> : RollingFileSink<T> where T : LogEvent
    {
        public EventHandler<string> Roll;

        public RollingFileSinkWithCallback(string pathFormat, ITextFormatter textFormatter, long? fileSizeLimitBytes, int? retainedFileCountLimit, Encoding encoding = null, TimeSpan? rollSpan = default(TimeSpan?)) :
            base(pathFormat, textFormatter, fileSizeLimitBytes, retainedFileCountLimit, encoding, rollSpan)
        {
        }

        protected override void RollFile(DateTimeOffset now)
        {
            string oldFileName = _currentFilePath;
            base.RollFile(now);

            if (oldFileName != null)
                Roll.Invoke(this, oldFileName);
        }
    }
}
