﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Serilog.Core;
using Serilog.Debugging;
using Serilog.Events;
using Serilog.Formatting;
using Serilog.Formatting.Display;
using Amazon.Fuel.Common.Logging.Events;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public class CrashReportSink : ILogEventSink
    {
        public const string CrashPrefix = "appcrash-";
        public const string CrashExtension = ".crash";
        private readonly string _path;
        private readonly object _syncRoot = new object();
        private readonly ITextFormatter _textFormatter;

        /// <summary>Construct a <see cref="FileSink"/>.</summary>
        /// <param name="path">Path to the crash directory.</param>
        /// <returns>Configuration object allowing method chaining.</returns>
        /// <remarks>The file will be written using the UTF-8 character set.</remarks>
        /// <exception cref="IOException"></exception>
        public CrashReportSink(string path)
        {
            var attr = File.GetAttributes(path);
            if (!attr.HasFlag(FileAttributes.Directory))
                throw new ArgumentException($"'{path}' is not a directory.", nameof(path));

            _textFormatter = new MessageTemplateTextFormatter("{Message}{NewLine}", null);
            _path = path;
        }

        /// <summary>
        /// Emit the provided log event to the sink.
        /// </summary>
        /// <param name="logEvent">The log event to write.</param>
        public void Emit(LogEvent logEvent)
        {
            var crashEvent = logEvent as CrashReportEvent;
            if (crashEvent == null)
                return;

            lock (_syncRoot)
            {
                EnsurePathExists();

                var hash = crashEvent.EventsHash;
                var crashCount = GetCrashDuplicateCount(hash);

                logEvent.AddOrUpdateProperty(new LogEventProperty("CrashDuplicateCount", new ScalarValue(crashCount.ToString())));

                var fileName = GetFileName(hash, crashCount.ToString());
                var path = Path.Combine(_path, fileName);
                var file = File.Open(path, FileMode.Append, FileAccess.Write, FileShare.Read);

                using (var outputWriter = new StreamWriter(file, Constants.UTF8NoBOM))
                {
                    _textFormatter.Format(crashEvent, outputWriter);
                    outputWriter.Flush();
                }
            }
        }

        private string GetFileName(string hash, string index)
        {
            return CrashPrefix + hash + "-" + index + CrashExtension;
        }

        private IList<string> GetExistingLogFiles(string hash)
        {
            try
            {
                return Directory
                    .GetFiles(_path, GetFileName(hash, "*"))
                    .Select(Path.GetFileName)
                    .ToList();
            }
            catch (DirectoryNotFoundException)
            {
                return new List<string>(0);
            }
        }

        private int GetCrashDuplicateCount(string hash)
        {
            lock (_syncRoot)
            {
                var prefix = CrashPrefix + hash + "-";
                var suffix = CrashExtension;
                var existingFiles = GetExistingLogFiles(hash);

                var indexes = existingFiles.Select(x => x.Replace(prefix, "").Replace(suffix, ""));
                var highestIndex = 0;

                foreach (var i in indexes)
                {
                    int value;
                    if (Int32.TryParse(i, out value))
                        highestIndex = Math.Max(highestIndex, value);
                }

                foreach (var file in existingFiles)
                {
                    try
                    {
                        File.Delete(Path.Combine(_path, file));
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                }

                highestIndex++;
                return highestIndex;
            }
        }

        private void EnsurePathExists()
        {
            try
            {
                if (!Directory.Exists(_path))
                    Directory.CreateDirectory(_path);
            }
            catch (Exception ex)
            {
                SelfLog.WriteLine("Failed to create directory {0}: {1}", _path, ex);
            }
        }
    }
}
