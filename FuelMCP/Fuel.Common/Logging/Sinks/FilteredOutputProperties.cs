﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Utilities;
using Serilog.Events;
using Serilog.Formatting.Display;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    public static class FilteredOutputProperties
    {
        public static IReadOnlyDictionary<string, LogEventPropertyValue> GetUsernameFilteredOutputProperties(LogEvent logEvent)
        {
            var outputPropertiesOriginal = OutputProperties.GetOutputProperties(logEvent);

            var outputPropertiesFiltered = outputPropertiesOriginal
                .Select(dictionaryItem =>
                    dictionaryItem.Value.ToString().ToLowerInvariant().Contains(Environment.UserName.ToLowerInvariant())
                        ? new KeyValuePair<string, LogEventPropertyValue>(dictionaryItem.Key, new LiteralStringValue(dictionaryItem.Value.ToString().ReplaceCaseInsensitive(Environment.UserName, "<OMIT>")))
                        : new KeyValuePair<string, LogEventPropertyValue>(dictionaryItem.Key, dictionaryItem.Value))
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            return outputPropertiesFiltered;
        }
    }
}
