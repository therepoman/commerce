﻿using System;
using System.IO;
using Serilog.Events;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    // A special case (non-null) string value for use in output
    // templates. Does not apply "quoted" formatting by default.
    internal class LiteralStringValue : LogEventPropertyValue
    {
        private readonly string _value;

        public LiteralStringValue(string value)
        {
            if (value == null) throw new ArgumentNullException("value");
            _value = value;
        }

        public override void Render(TextWriter output, string format = null, IFormatProvider formatProvider = null)
        {
            var toRender = _value;

            switch (format)
            {
                case "u":
                    toRender = _value.ToUpperInvariant();
                    break;
                case "w":
                    toRender = _value.ToLowerInvariant();
                    break;
            }

            output.Write(toRender);
        }

        public override bool Equals(object obj)
        {
            var sv = obj as LiteralStringValue;
            return sv != null && Equals(_value, sv._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
    }
}