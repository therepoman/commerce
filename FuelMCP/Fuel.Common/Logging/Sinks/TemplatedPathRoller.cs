﻿
// Copyright 2014 Serilog Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    // Rolls files based on the current date, using a path
    // formatting pattern like:
    //    Logs/log-{Date}.txt
    //
    [ExcludeFromCodeCoverage] //This is from Serilog
    class TemplatedPathRoller
    {
        const string OldStyleDateSpecifier = "{0}";
        const string DateSpecifier = "{Date}";
        private const string DateFormat = "yyyy-MM-dd-HH-mm-ss-fff";
        const string DefaultSeparator = "-";

        readonly string _pathTemplate;
        readonly Regex _filenameMatcher;

        public TemplatedPathRoller(string pathTemplate)
        {
            if (pathTemplate == null) throw new ArgumentNullException(nameof(pathTemplate));
            if (pathTemplate.Contains(OldStyleDateSpecifier))
                throw new ArgumentException("The old-style date specifier " + OldStyleDateSpecifier + " is no longer supported, instead please use " + DateSpecifier);

            var directory = Path.GetDirectoryName(pathTemplate);
            if (string.IsNullOrEmpty(directory))
                directory = Environment.CurrentDirectory;

            directory = Path.GetFullPath(directory);

            if (directory.Contains(DateSpecifier))
                throw new ArgumentException("The date cannot form part of the directory name");

            var filenameTemplate = Path.GetFileName(pathTemplate);
            if (!filenameTemplate.Contains(DateSpecifier))
                filenameTemplate = Path.GetFileNameWithoutExtension(filenameTemplate) + DefaultSeparator + DateSpecifier + Path.GetExtension(filenameTemplate);

            var indexOfSpecifier = filenameTemplate.IndexOf(DateSpecifier, StringComparison.Ordinal);
            var prefix = filenameTemplate.Substring(0, indexOfSpecifier);
            var suffix = filenameTemplate.Substring(indexOfSpecifier + DateSpecifier.Length);
            _filenameMatcher = new Regex(
                "^" +
                Regex.Escape(prefix) +
                "(?<date>[\\d|-]{" + DateFormat.Length + "})" +
                "(?<inc>_[0-9]{3,}){0,1}" +
                Regex.Escape(suffix) +
                "$");

            // Grab all logs that have any prefix before {filenameTemplate}
            DirectorySearchPattern = "*" + filenameTemplate.Replace(DateSpecifier, "*");
            LogFileDirectory = directory;
            _pathTemplate = Path.Combine(LogFileDirectory, filenameTemplate);
        }

        public string LogFileDirectory { get; }

        public string DirectorySearchPattern { get; }

        public void GetLogFilePath(DateTimeOffset date, int sequenceNumber, out string path)
        {
            var tok = date.ToString(DateFormat, CultureInfo.InvariantCulture);

            if (sequenceNumber != 0)
                tok += "_" + sequenceNumber.ToString("000", CultureInfo.InvariantCulture);

            path = _pathTemplate.Replace(DateSpecifier, tok);
        }

        public IEnumerable<RollingLogFile> SelectMatches(IEnumerable<string> paths)
        {
            foreach (var path in paths)
            {
                if (path == null)
                    continue;

                var filename = Path.GetFileName(path);
                var match = _filenameMatcher.Match(filename);

                if (!match.Success)
                    continue;

                var inc = 0;
                var incGroup = match.Groups["inc"];
                if (incGroup.Captures.Count != 0)
                {
                    var incPart = incGroup.Captures[0].Value.Substring(1);
                    inc = int.Parse(incPart, CultureInfo.InvariantCulture);
                }

                DateTimeOffset date;
                var datePart = match.Groups["date"].Captures[0].Value;
                if (!DateTimeOffset.TryParseExact(
                    datePart,
                    DateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out date))
                    continue;

                yield return new RollingLogFile(path, date, inc);
            }
        }
    }
}
