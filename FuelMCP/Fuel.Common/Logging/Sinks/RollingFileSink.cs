﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Serilog.Debugging;
using Serilog.Events;
using Serilog.Formatting;
using System.Threading.Tasks;
using System.Collections.Generic;
using Serilog.Core;

namespace Amazon.Fuel.Common.Logging.Sinks
{
    internal interface IRollingFileSink : ILogEventSink
    {
        Task<bool> ProcessFilesAsync(IEnumerable<string> files);
    }

    public abstract class RollingFileSink<T> : IRollingFileSink, IDisposable
         where T : LogEvent
    {
        private readonly Encoding _encoding;
        private readonly long? _fileSizeLimitBytes;
        private readonly int? _retainedFileCountLimit;
        private readonly TemplatedPathRoller _roller;
        private readonly object _syncRoot = new object();
        private readonly ITextFormatter _textFormatter;
        private readonly TimeSpan _rollSpan;
        private FileSink _currentFile;
        protected string _currentFilePath;
        private bool _isCurrentlyProcessing;
        private bool _isDisposed;
        private DateTimeOffset? _nextCheckpoint;

        protected RollingFileSink(string pathFormat,
            ITextFormatter textFormatter,
            long? fileSizeLimitBytes,
            int? retainedFileCountLimit,
            Encoding encoding = null,
            TimeSpan? rollSpan = null)
        {
            if (fileSizeLimitBytes.HasValue && fileSizeLimitBytes < 0)
                throw new ArgumentException("Negative value provided; file size limit must be non-negative.", nameof(fileSizeLimitBytes));

            if (retainedFileCountLimit.HasValue && retainedFileCountLimit < 1)
                throw new ArgumentException("Zero or negative value provided; retained file count limit must be at least 1", nameof(retainedFileCountLimit));

            _roller = new TemplatedPathRoller(pathFormat);
            _textFormatter = textFormatter;
            _fileSizeLimitBytes = fileSizeLimitBytes;
            _retainedFileCountLimit = retainedFileCountLimit;
            _encoding = encoding ?? Constants.UTF8NoBOM;
            _rollSpan = rollSpan ?? TimeSpan.FromDays(1);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            lock (_syncRoot)
            {
                if (_currentFile == null)
                    return;

                CloseFile();

                _isDisposed = true;
            }
        }

        public void Emit(LogEvent logEvent)
        {
            if (logEvent is T == false)
                return;

            lock (_syncRoot)
            {
                if (_isDisposed)
                    throw new ObjectDisposedException("The rolling file has been disposed.");

                var now = logEvent.Timestamp;

                try
                {
                    if (_nextCheckpoint == null)
                        OpenFile(now);
                    else if (now >= _nextCheckpoint.Value)
                        RollFile(now);
                }
                catch (Exception)
                {
                    _currentFile = null;
                }

                // If the file was unable to be opened on the last attempt, it will remain
                // null until the next checkpoint passes, at which time another attempt will be made to
                // open it.
                _currentFile?.Emit(logEvent);
            }
        }

        public virtual Task<bool> ProcessFilesAsync(IEnumerable<string> potentialMatches)
        {
            return Task.FromResult(true);
        }

        protected virtual void RollFile(DateTimeOffset now)
        {
            lock (_syncRoot)
            {
                CloseFile();

                try
                {
                    OpenFile(now, forceNew: true);
                }
                catch (Exception)
                {
                    _currentFile = null;
                }
            }
        }

        /// <summary>
        /// Sets the next checkpoint date to a value less than now, so that the next time <see cref="Emit"/> is called. The file is rolled to a new one.
        /// </summary>
        protected void RollFileOnNextEmit()
        {
            lock (_syncRoot)
                _nextCheckpoint = (_nextCheckpoint ?? DateTimeOffset.UtcNow).Subtract(_rollSpan);
        }

        private void CloseFile()
        {
            lock (_syncRoot)
            {
                if (_currentFile != null)
                {
                    _currentFile.Dispose();
                    _currentFile = null;
                    _nextCheckpoint = null;
                }
            }
        }

        public async Task<bool> SendLogsForProcessing()
        {
            if (_isCurrentlyProcessing)
                return true;

            var existingFiles = GetExistingLogFiles();

            if (existingFiles.Count > 0)
            {
                RollFile(DateTimeOffset.UtcNow);

                try
                {
                    _isCurrentlyProcessing = true;
                    return await ProcessFilesAsync(existingFiles);
                }
                finally
                {
                    _isCurrentlyProcessing = false;
                }
            }

            return true;
        }

        protected virtual FileSink CreateNewFileSink(string path, ITextFormatter textFormatter, long? fileSizeLimitBytes, Encoding encoding = null, Action fileSizeLimitExceededCallBack = null)
        {
            return new FileSink(path, textFormatter, fileSizeLimitBytes, encoding, fileSizeLimitExceededCallBack);
        }

        private IList<string> GetExistingLogFiles()
        {
            try
            {
                return Directory
                    .GetFiles(_roller.LogFileDirectory, _roller.DirectorySearchPattern)
                    .Select(Path.GetFullPath)
                    .ToList();
            }
            catch (DirectoryNotFoundException)
            {
                return new List<string>(0);
            }
        }

        private void OpenFile(DateTimeOffset now, bool forceNew = false)
        {
            lock (_syncRoot)
            {
                _nextCheckpoint = now.Add(_rollSpan);

                var existingFiles = GetExistingLogFiles();
                var latestForThisDate = _roller
                    .SelectMatches(existingFiles)
                    .Where(m => m.Timestamp.Date == now.Date)
                    .OrderByDescending(m => m.Timestamp)
                    .ThenByDescending(m => m.SequenceNumber)
                    .FirstOrDefault();

                var date = latestForThisDate?.Timestamp ?? now;
                var sequence = latestForThisDate?.SequenceNumber ?? 0;

                if (forceNew)
                {
                    date = now;
                    sequence = 0;
                }

                const int maxAttempts = 3;
                for (var attempt = 0; attempt < maxAttempts; attempt++)
                {
                    _roller.GetLogFilePath(date, sequence, out _currentFilePath);
                    try
                    {
                        _currentFile = CreateNewFileSink(_currentFilePath, _textFormatter, _fileSizeLimitBytes, _encoding, RollFileOnNextEmit);
                    }
                    catch (IOException ex)
                    {
                        if (IsFileLocked(ex))
                        {
                            SelfLog.WriteLine("Rolling file target {0} was locked, attempting to open next in sequence (attempt {1})", _currentFilePath, attempt + 1);
                            sequence++;
                            continue;
                        }

                        // purposely not handling this error
                        if (IsDiskFull(ex))
                            return;

                        throw;
                    }

                    ApplyRetentionPolicy();
                    return;
                }
            }
        }

        /// <summary>
        /// Makes sure the <see cref="_retainedFileCountLimit"/> is abided by.
        /// </summary>
        private void ApplyRetentionPolicy()
        {
            if (_retainedFileCountLimit == null) return;

            var existingFiles = GetExistingLogFiles();
            var currentFileName = Path.GetFileName(_currentFilePath);

            // We want to remove the current file from the list if it is part of the existing files.
            if (existingFiles.Contains(currentFileName))
                existingFiles.Remove(currentFileName);

            var filesOutsideOfRetentionPolicy = _roller
                .SelectMatches(existingFiles)
                .OrderByDescending(m => m.Timestamp)
                .ThenByDescending(m => m.SequenceNumber)
                .Skip(_retainedFileCountLimit.Value - 1)
                .Select(x => x.Path)
                .ToList();

            foreach (var path in filesOutsideOfRetentionPolicy)
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception ex)
                {
                    SelfLog.WriteLine("Error {0} while removing obsolete file {1}", ex, path);
                }
            }
        }

        private static bool IsFileLocked(Exception ex)
        {
            var errorCode = ex.HResult;
            return errorCode == Constants.SystemErrorCodes.ERROR_SHARING_VIOLATION || errorCode == Constants.SystemErrorCodes.ERROR_LOCK_VIOLATION;

        }

        private static bool IsDiskFull(Exception ex)
        {
            var errorCode = ex.HResult;
            return errorCode == Constants.SystemErrorCodes.ERROR_HANDLE_DISK_FULL || errorCode == Constants.SystemErrorCodes.ERROR_DISK_FULL;
        }
    }
}
