﻿using Amazon.Fuel.Common.Logging.Sinks;
using Amazon.Fuel.Common.Logging.Uploaders;
using Serilog.Configuration;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serilog
{
    public static class AmazonLogExtension
    {
        private const string DefaultOutputTemplate = "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {Level}] [{SourceContext}] {Message}{NewLine}{Exception}";
        private const long DefaultFileSizeLimitBytes = 10L * 1024L * 1024L; // 10 MiB
        private const int DefaultRetainedFileCountLimit = 10;
        private const string DefaultDateFormat = "yyyy-MM-dd-HH-mm-ss-fff";

        public static LoggerConfiguration AmazonSink(
            this LoggerSinkConfiguration sinkConfiguration,
            string path,
            string prefix = "",

            string dateFormat = DefaultDateFormat,
            string outputTemplate = DefaultOutputTemplate,
            long fileSizeLimitBytes = DefaultFileSizeLimitBytes,
            IFormatProvider formatProvider = null,
            Encoding encoding = null,
            Action fileSizeLimitExceededCallBack = null)
        {
            var formatter = new AmazonTextFormatter(outputTemplate, formatProvider);

            return sinkConfiguration.Sink(new AmazonLogFileSink(
                    Path.Combine(path, $"{prefix}{DateTime.Now.ToString(dateFormat, CultureInfo.InvariantCulture)}.log"),
                    formatter,
                    fileSizeLimitBytes,
                    encoding,
                    fileSizeLimitExceededCallBack));
        }

        /// <summary>
        /// Write log events to a series of files. Each file will be named according to
        /// the date of the first log entry written to it. Only simple date-based rolling is
        /// currently supported.
        /// </summary>
        /// <param name="sinkConfiguration">Logger sink configuration.</param>
        /// <param name="pathFormat">String describing the location of the log files,
        /// with {Date} in the place of the file date. E.g. "Logs\myapp-{Date}.log" will result in log
        /// files such as "Logs\myapp-2013-10-20.log", "Logs\myapp-2013-10-21.log" and so on.</param>
        /// <param name="restrictedToMinimumLevel">The minimum level for
        /// events passed through the sink.</param>
        /// <param name="outputTemplate">A message template describing the format used to write to the sink.
        /// the default is "{Timestamp} [{Level}] {Message}{NewLine}{Exception}".</param>
        /// <param name="formatProvider">Supplies culture-specific formatting information, or null.</param>
        /// <param name="fileSizeLimitBytes">The maximum size, in bytes, to which any single log file will be allowed to grow.
        /// For unrestricted growth, pass null. The default is 1 GB.</param>
        /// <param name="retainedFileCountLimit">The maximum number of log files that will be retained,
        /// including the current log file. For unlimited retention, pass null. The default is 31.</param>
        /// <param name="logUploader"></param>
        /// <param name="registerSink"></param>
        /// <returns>Configuration object allowing method chaining.</returns>
        /// <remarks>The file will be written using the UTF-8 character set.</remarks>
        public static LoggerConfiguration AmazonLog(
            this LoggerSinkConfiguration sinkConfiguration,
            LogUploader logUploader,
            string pathFormat,
            LogEventLevel restrictedToMinimumLevel = LevelAlias.Minimum,
            string outputTemplate = DefaultOutputTemplate,
            IFormatProvider formatProvider = null,
            long fileSizeLimitBytes = DefaultFileSizeLimitBytes,
            int retainedFileCountLimit = DefaultRetainedFileCountLimit,
            Action<AmazonLogSink> registerSink = null,
            TimeSpan? rollSpan = null)
        {
            registerSink = registerSink ?? (s => { });

            var formatter = new AmazonTextFormatter(outputTemplate, formatProvider);
            var sink = new AmazonLogSink(pathFormat, logUploader, formatter, fileSizeLimitBytes, retainedFileCountLimit, rollSpan: rollSpan);

            registerSink(sink);
            return sinkConfiguration.Sink(sink, restrictedToMinimumLevel);
        }
    }
}
