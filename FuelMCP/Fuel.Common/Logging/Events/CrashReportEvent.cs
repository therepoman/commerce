﻿using Amazon.Fuel.Common.Configuration;
using Newtonsoft.Json;
using Serilog.Events;
using Serilog.Parsing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Principal;
using System.Text;
using Amazon.Fuel.Common.Extensions;
using Amazon.Fuel.Common.Utilities;
using Microsoft.Win32;
using static Amazon.Fuel.Common.Constants;

namespace Amazon.Fuel.Common.Logging.Events
{
    public class CrashReportEvent : LogEvent
    {
        private static readonly AppInfo Info = new AppInfo();

        private const char UnixNewLine = '\n';

        static CrashReportEvent()
        {
            var sb = new StringBuilder();

            sb.AppendFormat("[Metadata]{0}", UnixNewLine);
            sb.AppendFormat("Process: {1}{0}", UnixNewLine, GetFormatName("Process"));
            sb.AppendFormat("Version: {1}{0}", UnixNewLine, GetFormatName("Version"));
            sb.AppendFormat("OsBuildNumber: {1}{0}", UnixNewLine, GetFormatName("OsBuildNumber"));
            sb.AppendFormat("CrashTimeUtc: {1}{0}", UnixNewLine, GetFormatName("CrashTimeUtc"));
            sb.AppendFormat("CrashType: {1}{0}", UnixNewLine, GetFormatName("CrashType"));
            sb.AppendFormat("CrashDescriptor: {1}{0}", UnixNewLine, GetFormatName("CrashDescriptor"));
            sb.AppendFormat("ContentType: {1}{0}", UnixNewLine, GetFormatName("ContentType"));
            sb.AppendFormat("CrashDuplicateCount: {1}{0}", UnixNewLine, GetFormatName("CrashDuplicateCount"));
            sb.AppendFormat("MarketplaceID: {1}{0}", UnixNewLine, GetFormatName("MarketplaceID"));
            sb.AppendFormat("countryOfResidence: {1}{0}", UnixNewLine, GetFormatName("CountryOfResidence"));
            sb.Append(UnixNewLine);
            sb.AppendFormat("[Events]{0}", UnixNewLine);
            sb.Append(GetFormatName("Events"));

            MessageFormat = sb.ToString();
        }

        public static string MessageFormat { get; set; }

        private CrashReportEvent(DateTimeOffset timestamp, LogEventLevel level, Exception exception, MessageTemplate messageTemplate, IEnumerable<LogEventProperty> properties)
            : base(timestamp, level, exception, messageTemplate, properties) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrashReportEvent"/> class.
        /// Fields correspond to CrashReport Log Format: https://w.amazon.com/index.php/MobileDiagnostics/DML/FionaSpectatorService/Crashboard#CrashReport_Log_Format
        /// </summary>
        /// <param name="crashDescriptor">GUID uniquely identifying the stacktrace of this crash.</param>
        /// <param name="crashType">Crash type, such as Prod_App_Error or Debug_Sdk_Failure.</param>
        /// <param name="events">Crash artifact. This is the body of the crash report.</param>
        /// <param name="appInfo">Contains configuration data.</param>
        /// <param name="contentType"></param>
        private static CrashReportEvent Create(string crashDescriptor, string crashType, string events, string contentType)
        {
            var timestamp = DateTimeOffset.UtcNow;
            var parser = new MessageTemplateParser();
            var template = parser.Parse(MessageFormat);

            var properties = new[] {
                new LogEventProperty("CrashTimeUtc", new ScalarValue(timestamp.ToUnixTime().ToString())),
                new LogEventProperty("Process", new ScalarValue(Info.SpectatorName)),
                new LogEventProperty("Version", new ScalarValue(Info.Version)),
                new LogEventProperty("CrashDescriptor", new ScalarValue(crashDescriptor)),
                new LogEventProperty("OsBuildNumber", new ScalarValue(System.Environment.OSVersion.ToString() + "/" + (Environment.Is64BitOperatingSystem ? "64" : "32"))),
                new LogEventProperty("OSProductName", new ScalarValue(WindowsAppUtilities.OsProductName)),
                new LogEventProperty("DotNet", new ScalarValue(Environment.Version)),
                new LogEventProperty("DotNet45OrLater", new ScalarValue(WindowsAppUtilities.Get45PlusFromRegistry())),
                new LogEventProperty("CrashType", new ScalarValue(crashType)),
                new LogEventProperty("Events", new ScalarValue(events)),
                new LogEventProperty("CountryOfResidence", new ScalarValue(RegionInfo.CurrentRegion.TwoLetterISORegionName)),
                new LogEventProperty("ContentType", new ScalarValue(contentType)),
                new LogEventProperty("MarketplaceID", new ScalarValue("Twitch")),
                new LogEventProperty("CrashDuplicateCount", new ScalarValue("1"))
            };

            var eventsHash = events.ToHashString();

            return new CrashReportEvent(timestamp.ToLocalTime(), LogEventLevel.Fatal, null, template, properties)
            {
                EventsHash = eventsHash
            };
        }

        /// <summary>
        /// Creates a CrashReport which can be written to the log and eventually parsed by DET.
        /// </summary>
        /// <param name="scope">The component the crash happened in e.g. App, Sdk</param>
        /// <param name="type">The severity of the crash e.g. Error, Fault</param>
        /// <param name="e">The exception thrown which triggered the crash.</param>
        /// <param name="source">Optional string to describe where the error occurred.</param>
        /// <param name="additionalInfo">Optional string to provide more information about the error. 
        /// This can be any information which might be useful when debugging the cause of the error.</param>
        /// <param name="additionalDescriptors">Optional qualifiers for the crash so they can be more specifically 
        /// grouped together e.g. SDS_Product_Id, ADG_Product_Id, etc.</param>
        /// <returns></returns>
        public static CrashReportEvent Create(CrashScope scope, CrashType type, Exception e, string source = null, string additionalInfo = null, string[] additionalDescriptors = null)
        {
            return Create(GetCrashDescriptor(e, additionalDescriptors),
                GetCrashType(CrashTypePrefix, scope, type),
                GetCrashArtifact(e, source, additionalInfo),
                source);
        }

        public string EventsHash { get; private set; }

        private static string SafeEval(Func<string> lambda)
        {
            try
            {
                return lambda();
            }
            catch (Exception)
            {
                return "<could_not_retrieve>";
            }
        }

        private static string GetCrashArtifact(Exception e, string source, string additionalInfo)
        {
            var sb = new StringBuilder();

            if (!String.IsNullOrWhiteSpace(source))
            {
                sb.AppendLine($"Source: {source}");
            }

            // some extra metadata that isn't parsed by DET
            sb.AppendLine();
            sb.AppendLine($"Proc64Bit:{Environment.Is64BitOperatingSystem}");
            sb.AppendLine($"ProcModel:{SafeEval(() => Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"))}/{SafeEval(() => Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER"))}");
            sb.AppendLine($"ProcCoreCount:{Environment.ProcessorCount}");
            sb.AppendLine($"MemUse:{Environment.WorkingSet}");
            sb.AppendLine($"ImpersonationLevel:{SafeEval(() => WindowsIdentity.GetCurrent().ImpersonationLevel.ToString())}");
            sb.AppendLine($"SystemDriveFreeBytes:{SafeEval(() => new System.IO.DriveInfo(Environment.SystemDirectory).AvailableFreeSpace.ToString())}");

            sb.AppendLine();
            sb.AppendLine("[Exception]");
            sb.AppendLine();
            sb.AppendLine(e.ToString());
            sb.AppendLine();
            sb.AppendLine("[Exception JSON]");
            sb.AppendLine();

            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(e, Formatting.Indented);
                sb.AppendLine(json);
            }
            catch (Exception jsonEx)
            {
                sb.AppendLine("*** failed to serialize exception to JSON ***");
                sb.AppendLine(jsonEx.ToString());
            }

            //Add any additional information wanted in the crash report.
            if (!String.IsNullOrWhiteSpace(additionalInfo))
            {
                sb.AppendLine();
                sb.AppendLine(additionalInfo);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Creates the Hash for identifying this type of Crash.
        /// </summary>
        /// <param name="e">The exception thrown which triggered the crash.</param>
        /// <param name="additionalDescriptors">Additional qualifiers for the crash so they can be more specifically 
        /// grouped together e.g. SDS_Product_Id, ADG_Product_Id, etc.</param>
        /// <returns></returns>
        private static string GetCrashDescriptor(Exception e, string[] additionalDescriptors = null)
        {
            var ex = e;
            var sb = new StringBuilder();

            do
            {
                sb.Append(ex.Source + ex.TargetSite + ex.GetType() + ex.HResult);
                ex = ex.InnerException;
            } while (ex != null);

            foreach (var descriptor in additionalDescriptors ?? new string[0])
            {
                sb.Append(descriptor ?? "");
            }

            return sb.ToString().ToHashString();
        }

        private static string GetFormatName(string name)
        {
            return $"{{{name}:l}}";
        }

        private static string GetCrashType(string prefix, CrashScope scope, CrashType type)
        {
            // note: there appears to be a length limit somewhere around 30 characters (30 characters is the max length we have seen)
            return $"{prefix}_{scope}_{type}";
        }
    }
}
