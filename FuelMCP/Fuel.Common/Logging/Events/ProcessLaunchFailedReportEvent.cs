﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Logging.Events
{
    /// <summary>
    /// Factory for creating Crash Reports when the Twitch Desktop App fails to launch a game.
    /// </summary>
    public class ProcessLaunchFailedReportEvent
    {
        //Seperates these Crash events from other events which have the same exceptions
        private const string EventDescriptor = "ProcessLaunchFailed";

        /// <summary>
        /// Creates a CrashReport for when the Twitch Desktop App fails to launch a game. This will gather information
        /// which should help to debug the issue.
        /// </summary>
        /// <param name="scope">The context/app which is reporting this issue e.g. App, Sdk</param>
        /// <param name="type">The type of execption e.g. Error, Fault</param>
        /// <param name="e">The exception raised when the process failed to launch.</param>
        /// <param name="launchFolder">The folder designated as the root directory for launching the process.</param>
        /// <param name="pathToFile">The path to the .exe to run.</param>
        /// <param name="osDriver">Utilities for getting information about the file system.</param>
        /// <returns></returns>
        public static CrashReportEvent Create(Constants.CrashScope scope, Constants.CrashType type, Exception e, string launchFolder, string pathToFile, IOSDriver osDriver, string adgProdutId, string sdsVersionId)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"ADG Product ID: {adgProdutId}");
            sb.AppendLine($"SDS Version ID: {sdsVersionId}");

            //Get information about game executable
            var gameExecutableExists = File.Exists(pathToFile);
            sb.AppendLine($"game executable exists: {gameExecutableExists}");
            if (gameExecutableExists)
            {
                var hasAllNeededUserPermissions = osDriver.FileHasAllUsersPermissions(pathToFile);
                sb.AppendLine($"game executable has needed user permissions: {hasAllNeededUserPermissions}");
            }

            //Get information about launch folder
            var launchFolderExists = Directory.Exists(launchFolder);
            sb.AppendLine($"launchFolder exists: {launchFolderExists}");
            if (launchFolderExists)
            {
                var hasAllNeededUserPermissions = osDriver.DirectoryHasAllUsersPermissions(launchFolder);
                sb.AppendLine($"launchFolder has needed user permissions: {hasAllNeededUserPermissions}");

                sb.AppendLine($"Contents of launchFolder: {launchFolder}");
                Directory.GetFiles(launchFolder)
                    .Concat(Directory.GetDirectories(launchFolder))
                    .ToList().ForEach(path => sb.AppendLine($"  {path}"));
            }

            //Get information about game library
            try
            {
                var gameLibraryDir = Directory.GetParent(launchFolder);
                var gameLibraryExists = gameLibraryDir != null ? gameLibraryDir.Exists : false;
                sb.AppendLine($"gameLibrary exists: {gameLibraryExists}");
                if (gameLibraryExists)
                {
                    sb.AppendLine($"gameLibrary: {gameLibraryDir.FullName}");
                    var hasAllNeededUserPermissions = osDriver.DirectoryHasAllUsersPermissions(gameLibraryDir.FullName);
                    sb.AppendLine($"gameLibrary has needed user permissions: {hasAllNeededUserPermissions}");
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine($"Exception thrown when trying to get information about the gameLibrary folder. [{ex}]");
            }

            //We want the ProcessLaunchFailedReports to be grouped by adgProductId+sdsVersionId.
            var additionDescriptors = new[] { EventDescriptor, adgProdutId, sdsVersionId};
            return CrashReportEvent.Create(scope, type, e, "ProcessLaunchFailed", sb.ToString(), additionDescriptors);
        }
    }
}
