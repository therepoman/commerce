﻿using System;
using System.IO;
using System.Threading.Tasks;
using Serilog;
using Amazon.Fuel.Common.Logging.Sinks;
using Amazon.Fuel.Common.Logging.Uploaders;
using System.Threading;

namespace Amazon.Fuel.Common.Logging
{
    public class CrashReporterWorker
    {
        public const int UploadCheckPeriodMs = 1000 * 60;

        private readonly string _crashLogsPath;
        private readonly CrashReportUploader _detUploader;
        private readonly ILogger _logger;
        private readonly Timer _timer;

        private bool _lockedForUpload;

        public CrashReporterWorker(CrashReportUploader detUploader, string crashLogsPath, ILogger logger)
        {
            _detUploader = detUploader;
            _crashLogsPath = crashLogsPath;
            _logger = logger;
            // Start the timer (do this after the enqueue so we don't enqueue the current one
            _timer = new Timer(_ => TimerTick(), null, UploadCheckPeriodMs, UploadCheckPeriodMs);
        }

        public Task<bool> Stop()
        {
            _timer?.Dispose();
            return Task.FromResult(true);
        }

        private void TimerTick()
        {
            if (_lockedForUpload)
                return;

            _lockedForUpload = true;
            Task.Run(() => UploadCrashes())
                .ContinueWith(t => _lockedForUpload = false);
        }

        private async Task UploadCrashes()
        {
            try
            {
                // If Crashes directory doesn't exist because it was deleted after app was launched, then we skip uploading crash logs.
                if (Directory.Exists(_crashLogsPath))
                {
                    var crashes = Directory.GetFiles(_crashLogsPath, "*" + CrashReportSink.CrashExtension, SearchOption.TopDirectoryOnly);
                    if (crashes.Length > 0)
                    {
                        _logger.Information("uploading {0} crash files", crashes.Length);
                        foreach (var crash in crashes)
                            await _detUploader.Upload(crash);
                    }
                }
            }
            catch (DirectoryNotFoundException ex)
            {
                _logger.Error(ex, "Crash Logs directory not found.");
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Unexpected exception uploading logs.");
            }
        }
    }
}
