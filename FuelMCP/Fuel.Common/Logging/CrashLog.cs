﻿using System;
using Amazon.Fuel.Common.Configuration;
using Serilog;

namespace Amazon.Fuel.Common.Logging
{
    public static class CrashLog
    {
        private static ILogger _logger;
        private static readonly AppInfo AppInfo = new AppInfo();

        public static ILogger Logger
        {
            get
            {
                return CrashLog._logger;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                CrashLog._logger = value;
            }
        }

        public static void WriteException(string appName, Exception crashException)
        {
            /* TODO: https://twitchtv.atlassian.net/browse/FUEL-1233
            var crashReportEvent = new CrashReportEvent(crashException.Message, crashException.GetType().ToString(), crashException.StackTrace, appName, AppInfo.Version, AppInfo.DeviceTypeId);
            Logger.Write(crashReportEvent);
            */
        }
    }
}
