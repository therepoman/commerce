﻿using System;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Win32;

namespace Amazon.Fuel.Common.Implementations
{
    /// <summary>
    /// Static class that provides simple access to device information.
    /// Code mostly taken from AmazonAuthMap by CloudDrive team.
    /// https://code.amazon.com/packages/AmazonAuthMap/trees/master/--/src/Amazon.Auth.Map.Win/Windows
    /// </summary>
    public static class DeviceInfo
    {
        /// <summary>
        /// The user+device DSN.  We cache it so that we don't recalculate it on every call.
        /// Username is part of it because a PC is a multi-tenant system, unlike a Kindle.
        /// </summary>
        private static string _dsn;
        public static string DSN
        {
            get
            {
                //TODO: use persistence to store and retrieve it https://twitchtv.atlassian.net/browse/FUEL-582
                //to treat the edge case where we have to generate a GUID.
                if (string.IsNullOrEmpty(_dsn))
                {
                    _dsn = GenerateDSN();
                }
                return _dsn;
            }
        }

        /// <summary>
        /// Get a UUID from the main board / mother board.
        /// </summary>
        public static string HardwareUuid
        {
            get
            {
                try
                {
                    var uuid = GetInfo("Win32_ComputerSystemProduct", "UUID");

                    if (String.IsNullOrWhiteSpace(uuid)
                        || uuid.Equals("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF")
                        || uuid.Equals("00000000-0000-0000-0000-000000000000"))
                    {
                        return String.Empty;
                    }

                    return uuid;
                }
                catch { }

                return String.Empty;
            }
        }

        /// <summary>
        /// Get a serial number from the main drive.
        /// </summary>
        public static string MainDiskSerialNumber
        {
            get
            {
                try
                {
                    var serialNumber = GetInfo("Win32_DiskDrive", "SerialNumber");
                    return serialNumber;
                }
                catch { }

                return String.Empty;
            }
        }

        /// <summary>
        /// Get a guid from the system's crypto entry in the registry.
        /// </summary>
        public static string MachineGuid
        {
            get
            {
                try
                {
                    if (Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography") != null)
                    {
                        var openSubKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography");
                        if (openSubKey != null && openSubKey.GetValue("MachineGuid") != null)
                        {
                            var guid = openSubKey.GetValue("MachineGuid").ToString();

                            if (String.IsNullOrWhiteSpace(guid)
                                || guid.Equals("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF")
                                || guid.Equals("00000000-0000-0000-0000-000000000000"))
                            {
                                return String.Empty;
                            }

                            return guid;
                        }
                    }
                }
                catch { }

                return String.Empty;
            }
        }

        #region privateMethods
        /// <summary>
        /// Gets WMI data specified.
        /// </summary>
        /// <param name="wmiClass"></param>
        /// <param name="wmiProperty"></param>
        /// <returns></returns>
        private static string GetInfo(string wmiClass, string wmiProperty)
        {
            var mc = new ManagementClass(wmiClass);
            var moc = mc.GetInstances();

            foreach (var mo in moc)
            {
                try
                {
                    var result = mo.GetPropertyValue(wmiProperty).ToString();
                    return result;
                }
                catch { }
            }

            return String.Empty;
        }

        /// <summary>
        /// Generate an MD5 hash from the input string and return it.
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        /// <summary>
        /// Creates a DSN for the user + hardware combo.
        /// Code mostly from Cloud Drive.
        /// </summary>
        /// <returns></returns>
        private static string GenerateDSN()
        {
            string deviceID = null;
            string hashResult = null;
            //1. Try to get UUID from the mainboard
            deviceID = DeviceInfo.HardwareUuid;

            //2. Failing that, get it from the main disk serial #
            if (string.IsNullOrEmpty(deviceID))
            {
                deviceID = DeviceInfo.MainDiskSerialNumber;
            }

            //3. if we still don't have a serial, then use the GUID which is used by crypto
            if (string.IsNullOrEmpty(deviceID))
            {
                deviceID = DeviceInfo.MachineGuid;
            }

            //4. Worst case, we generate a random GUID.  This seems bad.
            if (string.IsNullOrEmpty(deviceID))
            {
                deviceID = Guid.NewGuid().ToString("n");
            }

            //Now, you definitely have something for a deviceID, so you combine it with the username
            //because an individual PC can have multiple users and a DSN should be unique by device Type.
            deviceID = (System.Environment.UserName + deviceID);

            //From https://msdn.microsoft.com/en-us/library/system.security.cryptography.md5(v=vs.110).aspx
            using (MD5 md5Hash = MD5.Create())
            {
                hashResult = GetMd5Hash(md5Hash, deviceID);
            }

            return hashResult;
        }
        #endregion
    }
}
