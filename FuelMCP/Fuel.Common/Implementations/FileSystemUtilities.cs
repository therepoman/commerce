﻿using System.IO;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Implementations
{
    public class FileSystemUtilities : IFileSystemUtilities
    {
        public virtual DirectoryInfo CreateDirectory(string dir)    => Directory.CreateDirectory(dir);
        public virtual bool FileExists(string filePath)             => File.Exists(filePath);
        public virtual bool DirectoryExists(string path)            => Directory.Exists(path);
    }
}