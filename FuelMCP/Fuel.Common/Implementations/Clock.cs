﻿using System;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Common.Implementations
{
    public class Clock : IClock
    {
        public DateTime Now()
        {
            return DateTime.UtcNow;
        }
    }
}
