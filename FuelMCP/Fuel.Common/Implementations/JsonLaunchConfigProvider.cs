﻿using Serilog;
using System;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Data;
using Amazon.Fuel.Common.Exceptions;
using System.IO;

namespace Amazon.Fuel.Plugin.Launcher.Models
{
    /*
     * The job of this class is to find and correctly parse the launch config.
     * Interesting side note, because we're using a CDN we will likely have lots
     * of old launch configs in older formats so we need to be able to parse
     * as many of them as possible so we don't have to force ingest or fix
     * old launch configs. The job of this class it to parse the schema version out,
     * load the config and keep upgrading.
     */
    public class JsonLaunchConfigProvider : ILaunchConfigProvider
    {
        public string GetLaunchConfigPath(string installFolder, string file) => 
            Path.Combine(installFolder, file);

        public LaunchConfig GetLaunchConfig(string installFolder, string file)
        {
            string contents = null;

            var jsonPath = GetLaunchConfigPath(installFolder, file);
            try
            {
                using (var sr = new StreamReader(new FileStream(jsonPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Log.Error($"[JSN] {file} could not be read for game: {jsonPath} {ex.Message}");
                return null;
            }

            SchemaVersionObject sv;
            try
            {
                sv = SchemaVersionObject.FromJson(contents);
            }
            catch (Exception ex)
            {
                Log.Error($"[JSN] Json parsing error: {ex}");
                throw new InvalidLaunchConfigException(jsonPath);
            }

            if (sv.SchemaVersion != "2")
            {
                throw new InvalidLaunchConfigException(jsonPath);
            }

            LaunchConfigV2 config = LaunchConfig.FromJson(contents);
            if (config == null)
            {
                throw new InvalidLaunchConfigException(jsonPath);
            }
            return config.Next();
        }
    }
}
