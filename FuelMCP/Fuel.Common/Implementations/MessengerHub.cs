﻿using System;

using Amazon.Fuel.Common.Contracts;
using TinyMessenger;
using Serilog;
using System.Collections.Generic;

namespace Amazon.Fuel.Common.Implementations
{
    // This class simply wraps tiny messenger to allow injection
    public class MessengerHub : IMessengerHub
    {
        private readonly ITinyMessengerHub _messengerHub;

        public MessengerHub()
        {
            _messengerHub = new TinyMessengerHub();
        }

        public MessengerHub(ITinyMessengerHub messengerHub)
        {
            _messengerHub = messengerHub;
        }

        public IDisposable Subscribe<TMessage>(Action<TMessage> deliveryAction) where TMessage : class, ITinyMessage
        {
            return _messengerHub.Subscribe(deliveryAction);
        }

        public void Unsubscribe<TMessage>(IDisposable subscriptionToken) where TMessage : class, ITinyMessage
        {
            _messengerHub.Unsubscribe<TMessage>((TinyMessageSubscriptionToken) subscriptionToken);
        }

        string[] _exceptions = new string[] { "MessageDownloadFileFinished", "MessageUpdateOperationProgress",
                                                "MessageVerifyFileStarted", "MessageDownloadFileStarted"};

        public void Publish<TMessage>(TMessage message) where TMessage : class, ITinyMessage
        {
            _messengerHub.Publish(message);
        }

        public void PublishAsync<TMessage>(TMessage message) where TMessage : class, ITinyMessage
        {
            _messengerHub.PublishAsync(message);
        }

        public void PublishAsync<TMessage>(TMessage message, AsyncCallback callback) where TMessage : class, ITinyMessage
        {
            _messengerHub.PublishAsync(message, callback);
        }
    }
}
