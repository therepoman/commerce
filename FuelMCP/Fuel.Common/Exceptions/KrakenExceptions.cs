﻿using System;
using System.Net;

namespace Amazon.Fuel.Common.Exceptions
{
    /**
     * Thrown when auth token for twitch is invalid, generally means we need a reauth.
     */
    public class TwitchAuthException : Exception
    {
        public enum EType
        {
            TokenInvalid,       // token is invalid and needs a refresh
            BadResponse,        // identity giving a bad response
            InvalidJson         // response json is invalid
        }

        public EType Type { get; private set; }
        public HttpStatusCode? ResponseStatusCode { get; private set; }

        public TwitchAuthException() { }

        public TwitchAuthException(EType type, HttpStatusCode? responseStatusCode = null, string detail = null)
            : base($"auth token invalid, type: '{type}' status:'{(responseStatusCode.HasValue ? responseStatusCode.Value.ToString() : string.Empty)}' detail: '{detail}'" )
        {
            Type = type;
            ResponseStatusCode = responseStatusCode;
        }
    }

    public class RetryableTwitchAuthException : TwitchAuthException
    {
        public RetryableTwitchAuthException(EType detail, HttpStatusCode? responseStatusCode = null)
            : base(detail, responseStatusCode) { }
    }

    public class NetworkOfflineException : Exception
    {
        public NetworkOfflineException() { }

        public NetworkOfflineException(Exception innerException) : base("Network offline", innerException) { }
    }

    public class InvalidLaunchConfigException : Exception
    {
        public string JsonPath { get; }

        public InvalidLaunchConfigException(string jsonPath)
        {
            JsonPath = jsonPath;
        }
    }
}
