﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Amazon.Fuel.Common
{
    public static class PmetUtilities
    {
        public static string makeValidPmetString(string str)
        {
            string regex = "^[a-zA-Z0-9-_.:@/]*$";

            string ret = String.Concat(str.Where(c => Regex.IsMatch(c.ToString(), regex)));

            return ret.Length > 300 ? ret.Substring(0, 300) : ret;
        }
    }
}
