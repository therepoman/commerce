﻿using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Plugin.Download.Models;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Download.Messages
{
    // TODO these should all contain product ids. Or we should localize the message hub to be in context of single download
    public class DownloadJobProgressMessage : DownloadMessage
    {
        public long BytesDelta { get; private set; }
        public long BytesReceived { get; private set; }
        public long BytesTotal { get; private set; }
        public int TimeRemainingSeconds { get; private set; }
        public bool TimeRemainingNotEnoughData { get; private set; }

        public DownloadJobProgressMessage(
            object sender, 
            long bytesDelta, 
            long bytesReceived, 
            long bytesTotal, 
            int timeRemaining, 
            bool timeRemainingNotEnoughData)
            : base(sender)
        {
            BytesDelta = bytesDelta;
            BytesReceived = bytesReceived;
            BytesTotal = bytesTotal;
            TimeRemainingSeconds = timeRemaining;
            TimeRemainingNotEnoughData = timeRemainingNotEnoughData;
        }
    }

    public class MessageDownloadStarting : DownloadMessage
    {
        public MessageDownloadStarting(object sender)
            : base(sender)
        {
        }
    }

    public class MessageAllFilesDownloaded : DownloadMessage
    {
        public MessageAllFilesDownloaded(object sender)
            : base(sender)
        {
        }
    }

    public class MessageDownloadFileStarted : DownloadFileMessage
    {
        public MessageDownloadFileStarted(object sender, string folder, string file) : base(sender, folder, file)
        { 
        }
    }

    public class MessageDownloadFileFinished : DownloadFileMessage
    {
        public IDownloadJob Job { get; private set; }

        public MessageDownloadFileFinished(object sender, IDownloadJob job, string folder, string file) : base(sender, folder, file)
        {
            Job = job;
        }
    }

    public class MessageDownloadFileFailed : DownloadFileMessage
    {
        public MessageDownloadFileFailed(object sender, string folder, string file) : base(sender, folder, file)
        {
        }
    }

    public class MessageDownloadFileCanceled : DownloadFileMessage
    {
        public MessageDownloadFileCanceled(object sender, string folder, string file) : base(sender, folder, file)
        {
        }
    }

    public class MessageDownloadFileExpired : DownloadFileMessage
    {
        public MessageDownloadFileExpired(object sender, string folder, string file) : base(sender, folder, file)
        {
        }
    }

    public class MessageDownloadFileRetried : DownloadFileMessage
    {
        public MessageDownloadFileRetried(object sender, string folder, string file) : base(sender, folder, file)
        {
        }
    }

    public class MessageTotalBytesNeededForDownload : DownloadFileMessage
    {
        public long Bytes { get; set; }
        public MessageTotalBytesNeededForDownload(object sender, string folder, string file, long bytes) 
            : base(sender, folder, file)
        {
            this.Bytes = bytes;
        }
    }

    public class MessageUpdateOperationProgress : DownloadFileMessage
    {
        public enum EFileOp
        {
            Downloading,
            Verifying,
            Cancelling
        }

        public long BytesDelta { get; private set; }
        public long BytesReceived { get; private set; }
        public long BytesTotal { get; private set; }
        public EFileOp Operation { get; private set; }

        public MessageUpdateOperationProgress(
            object sender, 
            string folder, 
            string file, 
            long bytesDelta, 
            long bytesReceived, 
            long bytesTotal,
            EFileOp operation)
            : base(sender, folder, file)
        {
            this.BytesDelta = bytesDelta;
            this.BytesReceived = bytesReceived;
            this.BytesTotal = bytesTotal;
            this.Operation = operation;
        }
    }   
}
