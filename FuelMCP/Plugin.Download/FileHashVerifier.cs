using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Models;
using Amazon.Fuel.Plugin.Persistence;
using Serilog;

namespace Amazon.Fuel.Plugin.Download
{
    public class FileHashVerifier : Singleton<FileHashVerifier>
    {
        private class FileHashTask
        {
            public DownloadFile File { get; set; }
            public bool Forced { get; set; }
            public IHashCache HashCache { get; set; }
            public Action<DownloadFile, bool> CallbackOnComplete { get; set; }  // DownloadFile file, bool hashMatchSuccess
        }

        private class JobTasksInfo
        {
            public ConcurrentBag<Task> AllTasks;
            public CancellationTokenSource TokenSource;
        }

        private DateTime _timeLastLog = DateTime.Now;

        private readonly Object _lock = new Object();

        private int _logIntervalInMs = 60000;

        private int _numFilesVerified;

        private readonly ConcurrentDictionary<Guid, JobTasksInfo> _jobCancellationDict = new ConcurrentDictionary<Guid, JobTasksInfo>();

        private readonly ConcurrentDictionary<Guid, int> _timeSpentVerifyingDict = new ConcurrentDictionary<Guid, int>();

        private readonly PriorityScheduler _priorityScheduler = new PriorityScheduler(ThreadPriority.Lowest);

        private IMetricsPlugin _metrics;

        private List<Task> _allTasks
        {
            get { return _jobCancellationDict.Values.SelectMany(v => v.AllTasks).ToList(); }
        }

        public void SetMetrics(IMetricsPlugin metrics)
        {
            _metrics = metrics;
        }

        private void TryAddCounter(string source, string metricName)
        {
            if (_metrics != null)
            {
                _metrics.AddCounter(source, metricName);
            }
            else
            {
                FuelLog.Warn("Tried to log counter metrics in FileHashVerifier without instantiating metrics.");
            }
        }

        private void TryAddValue(string source, string metricName, int val)
        {
            if (_metrics != null)
            {
                _metrics.AddDiscreteValue(source, metricName, val);
            }
            else
            {
                FuelLog.Warn("Tried to log value metrics in FileHashVerifier without instantiating metrics.");
            }
        }

        public void Shutdown()
        {
            foreach (var jobTasksInfo in _jobCancellationDict.Values)
            {
                jobTasksInfo.TokenSource.Cancel();
            }
            Task.WaitAll(_allTasks.ToArray());
        }

        public void CancelJob(Guid jobId)
        {
            JobTasksInfo tasksInfo;
            _jobCancellationDict.TryGetValue(jobId, out tasksInfo);

            tasksInfo?.TokenSource.Cancel();
        }

        public void ClearCompletedJobs()
        {
            List<Guid> completedJobIds = new List<Guid>();
            foreach (var kvp in _jobCancellationDict)
            {
                var jobTasksInfo = kvp.Value;
                if (jobTasksInfo.AllTasks.All(t => t.IsCompleted))
                    completedJobIds.Add(kvp.Key);
            }

            foreach (var id in completedJobIds)
            {
                JobTasksInfo tasksInfo;
                _jobCancellationDict.TryRemove(id, out tasksInfo);
            }
        }

        public void QueueFile(Guid jobId, DownloadFile file, bool forced, IHashCache hashCache, Action<DownloadFile, bool> callbackOnComplete)
        {
            // Avoid a constantly increasing task list by whittling down here.
            ClearCompletedJobs();

            FileHashTask verifyFileHashTask = new FileHashTask
            {
                File = file,
                Forced = forced,
                HashCache = hashCache,
                CallbackOnComplete = callbackOnComplete
            };

            if (!_jobCancellationDict.ContainsKey(jobId))
            {
                _jobCancellationDict.TryAdd(jobId, new JobTasksInfo
                {
                    TokenSource = new CancellationTokenSource(),
                    AllTasks = new ConcurrentBag<Task>()
                });
            }

            var cancellationToken = _jobCancellationDict[jobId].TokenSource.Token;
            JobTasksInfo tasksInfo;
            _jobCancellationDict.TryGetValue(jobId, out tasksInfo);
            tasksInfo?.AllTasks
                .Add(Task.Factory.StartNew(
                    () =>
                    {
                        try
                        {
                            VerifyHash(jobId, verifyFileHashTask, cancellationToken);
                        }
                        catch (Exception e)
                        {
                            FuelLog.Error(e, "Couldn't verify hash");
                        }
                        
                    }, cancellationToken, TaskCreationOptions.None, _priorityScheduler));

        }

        public int GetPendingTaskCount(Guid jobid)
        {
            Debug.Assert(jobid != null);

            JobTasksInfo tasksInfo;
            _jobCancellationDict.TryGetValue(jobid, out tasksInfo);

            return tasksInfo?.AllTasks.Count(t => !t.IsCompleted) ?? 0;
        }

        public async Task WaitOnPendingTasks(DownloadJob job)
        {
            Debug.Assert(job != null);
            ClearCompletedJobs();
            while (GetPendingTaskCount(job.JobId) > 0)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(50));
            }

            // Log total time spent doing hash verification.
            if (_timeSpentVerifyingDict.ContainsKey(job.JobId))
            {
                FuelLog.Info("Verifying files took " +
                             TimeSpan.FromMilliseconds(_timeSpentVerifyingDict[job.JobId]).ToString("G"));

                long megabytes = (job.TotalSizeInBytes / 1000000) > 0 ? (job.TotalSizeInBytes / 1000000) : Constants.MinGameSizeInMb;
                int normalizedLatency = (int) (_timeSpentVerifyingDict[job.JobId] / megabytes);
                TryAddValue(MetricStrings.SOURCE_DownloadJob, MetricStrings.METRIC_NormalizedTime, normalizedLatency);
            }
            else
            {
                FuelLog.Warn("Could not find time spent verifying for this job.");
                TryAddCounter(MetricStrings.SOURCE_HashVerification, MetricStrings.METRIC_MissingNormalizedTime);
            }

        }

        private void VerifyHash(Guid jobId, FileHashTask checkFile, CancellationToken token)
        {
            if (token.IsCancellationRequested)
                return;

            Stopwatch sw = Stopwatch.StartNew();

            Interlocked.Increment(ref _numFilesVerified);

            lock (_lock)
            {     
                if ((DateTime.UtcNow - _timeLastLog).TotalMilliseconds > _logIntervalInMs)
                {
                    Log.Information("Verifying Hashes for file number " + _numFilesVerified);
                    _timeLastLog = DateTime.UtcNow;
                }
            }

            var hashResult = checkFile.HashCache.UpdateFileHash(checkFile.File.Dest, checkFile.Forced, null);


            var hashMatch = checkFile.File.VerificationValue
                .Equals(hashResult, StringComparison.InvariantCultureIgnoreCase);

            if (!hashMatch)
            {
                Log.Warning($"File verification hashing failed in file '{checkFile.File.Dest}'");
            }

            // make sure we haven't cancelled
            if (!token.IsCancellationRequested)
            {
                checkFile.CallbackOnComplete(checkFile.File, hashMatch);
            }

            int totalTimeSpent = (int) sw.ElapsedMilliseconds;
            bool successfullyAdded = _timeSpentVerifyingDict.TryAdd(jobId, totalTimeSpent);
            if (!successfullyAdded)
            {
                // This only happens if the key already exists
                var isUpdated = false;
                var retryCount = 0;
                while (!isUpdated && retryCount < 10)
                {
                    int val;
                    _timeSpentVerifyingDict.TryGetValue(jobId, out val);
                    isUpdated = _timeSpentVerifyingDict.TryUpdate(jobId, val + totalTimeSpent, val);
                    retryCount++;
                }

                if (!isUpdated)
                {
                    _metrics.AddCounter(MetricStrings.SOURCE_FileHashVerifier, MetricStrings.METRIC_MissingNormalizedTime);
                }
            }
        }
    }
}