﻿using Amazon.Fuel.Common.Contracts;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Persistence;
using Sds = Tv.Twitch.Fuel.Sds;
using Amazon.Fuel.SoftwareDistributionService.Client;

namespace Amazon.Fuel.Plugin.Download
{
    public class DownloadPlugin : IDownloadPlugin
    {
        private readonly DownloadJobManager _jobManager;
        private readonly IMetricsPlugin _metrics;
        private readonly ITwitchTokenProvider _twitchTokenProvider;
        private readonly IMessengerHub _hub;

        public DownloadPlugin(
            ISoftwareDistributionServiceClient sds,
            IMessengerHub hub, 
            ISecureStoragePlugin storage, 
            IMetricsPlugin metrics,
            IEntitlementPlugin entitlements,
            ITwitchTokenProvider twitchTokenProvider,
            IOSDriver osDriver)
        {
            _metrics = metrics;
            _twitchTokenProvider = twitchTokenProvider;
            _hub = hub;
            _jobManager = new DownloadJobManager(entitlements, hub, _metrics, sds, _twitchTokenProvider, InstallUtil.Instance, osDriver, new DownloadJobFactory());
        }

        public async Task<DownloadProductResult> DownloadProductWithRetryASync(
            ProductId productId,
            string entitlementId,
            string folder,
            int[] retryWaitSec,
            bool isUpdate,
            Sds.Manifest previousManifest = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            Log.Information(
                $"DownloadProductWithRetryASync({productId}, {folder}, {cancellationToken.IsCancellationRequested})");

            _metrics.AddCounter(MetricStrings.SOURCE_DownloadJob, MetricStrings.METRIC_Start);

            DownloadProductResult ret = new DownloadProductResult()
            {
                Result = EDownloadJobResult.Success,
                ManifestWithVersion = null
            };

            if (cancellationToken.IsCancellationRequested)
            {
                ret.Result = EDownloadJobResult.Cancel;
                return reportMetric(ret, productId);
            }

            try
            {
                ret = await _jobManager.DownloadWithRetryAsync(
                    isUpdate: isUpdate,
                    retryWaitSec: retryWaitSec,
                    entitlementId: entitlementId,
                    productId: productId,
                    folder: folder,
                    metrics: _metrics,
                    hub: _hub,
                    previousManifest: previousManifest,
                    cancellationToken: cancellationToken);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error downloading job");
                ret.Result = EDownloadJobResult.GeneralFailure;
            }

            return reportMetric(ret, productId);
        }

        private DownloadProductResult reportMetric(DownloadProductResult result, ProductId prodId)
        {
            if (result.Result == EDownloadJobResult.Cancel || result.Result == EDownloadJobResult.Success)
            {
                // General QoS to track KPI's
                _metrics.AddCounter(MetricStrings.SOURCE_DownloadJobQos, result.Result.ToString());
            }
            else
            {
                var qosMetricString = result.IsUnexpectedFailure() ? MetricStrings.METRIC_UnexpectedFailure : MetricStrings.METRIC_ExpectedFailure;
                var failureTypeSourceString = result.IsUnexpectedFailure() ? MetricStrings.SOURCE_DownloadJobUnexpectedFailure : MetricStrings.SOURCE_DownloadJobExpectedFailure;

                // General Qos for tracking KPI's.
                _metrics.AddCounter(MetricStrings.SOURCE_DownloadJobQos, qosMetricString);
                // Log error codes to correct bucket to track the top error codes per bucket.
                _metrics.AddCounter(failureTypeSourceString, result.ToString());
            }

            _metrics.AddCounter(MetricStrings.SOURCE_DownloadJob, result.Result.ToString(),
                MetricStrings.PIVOT_ProductId, prodId.Id);
            return result;
        }
    }
}
