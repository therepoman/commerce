﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Persistence;

namespace Amazon.Fuel.Plugin.Download
{
    public interface IDownloadJobManager
    {
        IMessengerHub Hub { get; }
        IMetricsPlugin Metrics { get; }
        IEntitlementPlugin Entitlement { get; }
    }
}