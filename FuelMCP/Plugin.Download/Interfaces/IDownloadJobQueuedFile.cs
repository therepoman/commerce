using Amazon.Fuel.SoftwareDistributionService.Model;

namespace Amazon.Fuel.Plugin.Download.Interfaces
{
    public interface IDownloadJobQueuedFile
    {
        long Size { get; }
        Hash SourceHash { get; }
        Hash TargetHash { get; }
        Tv.Twitch.Fuel.Sds.File ManifestFile { get; }
        long ManifestFileIndex { get; }
        void ClearSourceHash();
    }
}