﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Extensions;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Models;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.SoftwareDistributionService.Model;
using Serilog;
using Tv.Twitch.Fuel.Patching;

using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Plugin.Download
{
    internal enum PatchingResult
    {
        SUCCESS,
        HASH_FAILED,
        INVALID_PATCH
    }

    // Contains logic for applying patches after downloading, or moving original files into place
    internal class PatchApplyTask
    {
        private readonly DownloadPatchInfo _patchInfo;
        private readonly Sds.File _manifestFile;
        private readonly string _downloadPath;
        private readonly string _targetFilePath;
        private readonly string _outputFilePath;
        private readonly IHashCache _fileVerificationShaCache;

        public PatchApplyTask(DownloadPatchInfo pathInfo, Sds.File manifestFile, string downloadPath, string targetFilePath, string outputFilePath, IHashCache fileVerificationShaCache)
        {
            _patchInfo = pathInfo;
            _manifestFile = manifestFile;
            _downloadPath = downloadPath;
            _targetFilePath = targetFilePath;
            _outputFilePath = outputFilePath;
            _fileVerificationShaCache = fileVerificationShaCache;
        }

        public PatchingResult Apply()
        {
            switch (_patchInfo.DeltaEncoding)
            {
                case DeltaEncoding.NONE:
                    // Check if the file was downloaded to a temporary location, if not then we are done
                    if (_downloadPath.Equals(_targetFilePath, StringComparison.OrdinalIgnoreCase)) return PatchingResult.SUCCESS;

                    // Move the file into the correct place
                    MoveWithOverwrite(_downloadPath, _targetFilePath);
                    return PatchingResult.SUCCESS;
                case DeltaEncoding.FUEL_PATCH:
                    FuelLog.Info($"Applying patch...", new
                    {
                        source = _targetFilePath,
                        patch = _downloadPath,
                        output = _outputFilePath
                    });
                    ApplyPatch(_targetFilePath, _downloadPath, _outputFilePath);

                    // Double-check the patching created what we expected
                    var actualHash = ShaUtil.ComputeHashBytes(_outputFilePath);
                    if (actualHash.SequenceEqual(_manifestFile.Hash.Value))
                    {
                        MoveWithOverwrite(_outputFilePath, _targetFilePath);
                        File.Delete(_downloadPath);
                        _fileVerificationShaCache.UpdateCache(_targetFilePath, ShaUtil.ToString(actualHash));
                        return PatchingResult.SUCCESS;
                    }
                    else
                    {
                        FuelLog.Error(
                            $"Failed to patch file. Expected {_manifestFile.Hash.HexString()} found {actualHash.ToHexString()}");
                        return PatchingResult.HASH_FAILED;
                    }
                default:
                    FuelLog.Fatal($"Unknown patch type! {_patchInfo.DeltaEncoding}");
                    return PatchingResult.INVALID_PATCH;
            }
        }

        private void MoveWithOverwrite(string originalPath, string destinationPath)
        {
            if (File.Exists(destinationPath)) File.Delete(destinationPath);
            File.Move(originalPath, destinationPath);
        }

        private void ApplyPatch(string sourcePath, string patchPath, string outputFilePath)
        {
            using (var source = new FileStream(sourcePath, FileMode.Open))
            using (var patch = new FileStream(patchPath, FileMode.Open))
            using (var output = new FileStream(outputFilePath, FileMode.Create))
            {
                var patcher = new Patcher(source, patch, output);

                patcher.Apply(); // TODO FLCL-1625 progress reporting, important for large patches.
            }
        }
    }
}
