﻿namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadFileSegmentContext
    {
        public int Idx { get; set; } = 0;
        public long Offset { get; set; } = 0;
        public long Size { get; set; } = 0;
        public long DownloadProgress { get; set; } = 0;
        public DownloadFile File { get; set; }
    }
}