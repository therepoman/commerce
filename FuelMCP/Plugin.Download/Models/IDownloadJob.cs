﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Download.Interfaces;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public interface IDownloadJob : IDisposable, ITransferedBytes
    {
        ProductId ProductId { get; }
        string InstallDirectory { get; }
        long TotalBytes { get; }
        long TotalSizeInBytes { get; }
        CancellationToken CancelToken { get; }
        bool IsCancelled { get; }
        bool IsUpdate { get; }
        bool IsAborted { get; }
        bool HadAuthFailure { get; }
        DownloadJobResult LastResult { get; }
        IList<IDownloadJobQueuedFile> QueuedFiles { get; }

        /// <summary>
        /// Run the full download job.
        /// </summary>
        /// <returns>Information regarding download attempt</returns>
        Task<DownloadJobResult> RunJobAsync();

        /// <summary>
        /// Calculate the effective transfer rate for a completed or in-progress job.
        /// </summary>
        /// <returns>Speed in KB per second</returns>
        int CalculateTransferKbPerSec();

        /// <summary>
        /// Test whether there is enough disk space for the given path to install the game.
        /// </summary>
        /// <returns>Has enough space</returns>
        bool JobHasDiskSpaceAvailable();

        /// <summary>
        /// Removes any files that do not exist in the manifest but are on the disk.
        /// Files that were removed from the manifest this way are removed from disk.
        /// </summary>
        /// <returns>Number of files cleaned</returns>
        int CleanUnknownFiles();


    }
}