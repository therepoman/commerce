using Amazon.Fuel.SoftwareDistributionService.Model;

using Sds = Tv.Twitch.Fuel.Sds;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadPatchInfo
    {
        public HashPair PatchHashPair { get; set; } = new HashPair();
        public DeltaEncoding DeltaEncoding { get; set; }

        public string PrimarySource { get; set; }
        public string SecondarySource { get; set; }

        public long FileLength { get; set; }
        // Hex value
        public string Hash { get; set; }
    }
}