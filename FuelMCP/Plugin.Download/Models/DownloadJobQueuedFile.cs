using Amazon.Fuel.Common.Extensions;
using Amazon.Fuel.Plugin.Download.Interfaces;
using Amazon.Fuel.SoftwareDistributionService.Model;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadJobQueuedFile : IDownloadJobQueuedFile
    {
        public DownloadJobQueuedFile(Hash sourceHash, Tv.Twitch.Fuel.Sds.File manifestFile, long manifestFileIndex)
        {
            ManifestFile = manifestFile;
            SourceHash = sourceHash;
            ManifestFileIndex = manifestFileIndex;
        }

        public long ManifestFileIndex { get; }

        public long Size => ManifestFile.Size;

        public Hash SourceHash { get; private set; }

        public Hash TargetHash => new Hash()
        {
            value = ManifestFile.Hash?.HexString(),
            algorithm = HashAlgorithm.SHA256        
        };
        public Tv.Twitch.Fuel.Sds.File ManifestFile { get; }

        public void ClearSourceHash() => SourceHash = null;
    }
}