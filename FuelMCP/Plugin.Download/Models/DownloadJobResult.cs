﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadJobResult
    {
        public EDownloadJobResult Result { get; set; } = EDownloadJobResult.Success;

        public long BytesDownloaded { get; set; } = 0;
        public int FilesCompleted { get; set; } = 0;
        public bool ManifestDownloadFailed { get; set; } = false;
        public Stopwatch RunTimer { get; } = new Stopwatch();
        public ManifestWithVersion ManifestWithVersion { get; set; } = null;

        public Dictionary<string, DownloadFileResult> LastResultByFileTgt { get; private set; } = new Dictionary<string, DownloadFileResult>();

        public Dictionary<EDownloadResult, int> DownloadResultCounts { get; private set; } = new Dictionary<EDownloadResult, int>();

        public DownloadJobResult()
        {
            RunTimer.Start();
        }

        public void AddResult(DownloadJob jobContext, DownloadFile fileResultFile, DownloadFileResult fileResult)
        {
            // tally bytes downloaded
            BytesDownloaded += fileResult.BytesDownloaded;

            Debug.Assert(fileResultFile?.Dest != null);
            LastResultByFileTgt[fileResultFile?.Dest ?? "<unknown>"] = fileResult;

            // add result tally
            DownloadResultCounts[fileResult.Result] = DownloadResultCounts.GetValueOrDefault(fileResult.Result) + 1;

            // log metrics
            if (fileResult.Result != EDownloadResult.Success && fileResult.Result != EDownloadResult.Cancel)
            {
                jobContext.JobManager.Metrics.AddCounter(
                    MetricStrings.SOURCE_DownloadFile,
                    fileResult.Result.ToString(),
                    MetricStrings.PIVOT_ProductId,
                    jobContext.ProductId.Id);

                if (fileResult.DownloadWebStatus != WebExceptionStatus.Success)
                {
                    jobContext.JobManager.Metrics.AddCounter(
                        MetricStrings.SOURCE_DownloadFile,
                        string.Format(MetricStrings.METRIC_WebErrorFormat, fileResult.DownloadWebStatus.ToString()),
                        MetricStrings.PIVOT_ProductId,
                        jobContext.ProductId.Id);
                }
            }
        }

        public bool RanOutOfDiskSpace()
        {
            int outOfSpace = 0;
            return DownloadResultCounts.TryGetValue(EDownloadResult.NotEnoughDiskSpace, out outOfSpace)
                   && outOfSpace > 0;
        } 

        public void MarkCancelled()
        {
            Result = EDownloadJobResult.Cancel;
        }

        public void UpdateResult(bool wasCancelled, bool wasAborted)
        {
            // NOTE: this integrates the overall job failure status, make sure that you order more prominent
            // errors towards the top
            Result = EDownloadJobResult.Success;

            if (DownloadResultCounts.GetValueOrDefault(EDownloadResult.LockedFile) > 0)
            {
                Result = EDownloadJobResult.LockedFiles;
            }
            else if (wasCancelled || (DownloadResultCounts.GetValueOrDefault(EDownloadResult.Cancel) > 0 && !wasAborted))
            {
                Result = EDownloadJobResult.Cancel;
            }
            else if (DownloadResultCounts.GetValueOrDefault(EDownloadResult.ExpiredFileEncountered) > 0)
            {
                Result = EDownloadJobResult.ExpiredFileEncountered;
            }
            else if (DownloadResultCounts.GetValueOrDefault(EDownloadResult.SocketCommFailure) > 0)
            {
                Result = EDownloadJobResult.DownloadTimeoutEncountered;
            }
            else if (wasAborted
                        || DownloadResultCounts.Any(result => result.Value > 0 && result.Key.IsFailure()))
            {
                Result = EDownloadJobResult.GeneralFailure;
            }
        }

        public override string ToString() => $"Result:'{Result}', BytesDownloaded:'{BytesDownloaded}'";

        public string GetResultsSummary()
        {
            StringBuilder sb = new StringBuilder();
            foreach ( var kv in DownloadResultCounts )
            {
                sb.Append($"{kv.Key}:{kv.Value}{Environment.NewLine}");
            }
            return sb.ToString();
        }

        public bool WantRetry
        {
            get
            {
                if (Result == EDownloadJobResult.Cancel) return false;

                if ( ManifestDownloadFailed ) return true;

                bool wantsRetry = false;

                foreach (var result in DownloadResultCounts)
                {
                    if (result.Key.IsFailure())
                    {
                        // we want this to block 
                        if (!result.Key.WantsJobRetry())
                        {
                            return false;
                        }
                        else
                        {
                            wantsRetry = true;
                        }
                    }
                }

                return wantsRetry;
            }
        } 

        public void LogReport()
        {
            if (Result == EDownloadJobResult.Success)
            {
                FuelLog.Info($"Game download completed successfully", new { jobTimeSec = RunTimer.Elapsed.TotalSeconds });
            }
            else
            {
                if (Result == EDownloadJobResult.Cancel)
                {
                    FuelLog.Info($"Game download cancelled", new { jobTimeSec = RunTimer.Elapsed.TotalSeconds });
                }
                else
                {
                    FuelLog.Error($"Game download failed", new { jobTimeSec = RunTimer.Elapsed.TotalSeconds, Result, retry = WantRetry });
                }
            }
        }
    }
}
