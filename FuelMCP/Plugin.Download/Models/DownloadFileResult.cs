﻿using System.Net;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadFileResult
    {
        public EDownloadResult Result { get; set; } = EDownloadResult.Success;
        public WebExceptionStatus DownloadWebStatus { get; set; } = WebExceptionStatus.Success;
        public long BytesDownloaded { get; set; } = 0;
        public DownloadFile File { get; set; }

        public bool IsSuccess => Result == EDownloadResult.Success;
    }
}