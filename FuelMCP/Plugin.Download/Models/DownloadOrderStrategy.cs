﻿using System.Collections.Generic;
using System.Linq;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Interfaces;
using System.IO;
using Amazon.Fuel.Common;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public interface IDownloadOrderStrategy
    {
        void SortFileOrder(IList<IDownloadJobQueuedFile> filesToDownload);
    }

    /// <summary>
    /// Sorts the list, takes files from Right, then Left interleaving them
    /// This results in a list that is intermixed with small and large files.
    /// </summary>
    public class DownloadFilesInterleavedStrategy : Singleton<DownloadFilesInterleavedStrategy>, IDownloadOrderStrategy
    { 
        public void SortFileOrder(IList<IDownloadJobQueuedFile> filesToDownload)
        {
            filesToDownload.OrderByDescending(f => f.Size);

            var filesToDownloadCopy = new List<IDownloadJobQueuedFile>(filesToDownload);
            filesToDownload.Clear();

            // try to push fuel.json to front, so that we can find and lock prereq exes
            var cfgIdx = filesToDownloadCopy.FindIndex(i => 
                string.Equals(Path.GetFileName(i.ManifestFile.Path), 
                Constants.Paths.FuelGameConfigFileName, 
                System.StringComparison.InvariantCultureIgnoreCase));

            if (cfgIdx == -1)
            {
                FuelLog.Warn("Couldn't find config file");
            }
            else
            {
                filesToDownload.Add(filesToDownloadCopy[cfgIdx]);
                filesToDownloadCopy.RemoveAt(cfgIdx);
            }

            while (filesToDownloadCopy.Count > 0)
            {
                var even = filesToDownload.Count % 2 == 0;

                if (even)
                {
                    int end = filesToDownloadCopy.Count - 1;
                    filesToDownload.Add(filesToDownloadCopy[end]);
                    filesToDownloadCopy.RemoveAt(end);
                }
                else
                {
                    filesToDownload.Add(filesToDownloadCopy[0]);
                    filesToDownloadCopy.RemoveAt(0);
                }
            }
        }
    }
}
