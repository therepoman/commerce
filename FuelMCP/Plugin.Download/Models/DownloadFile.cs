﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Messages;
using Serilog;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadFile
    {
        public readonly int[] RetryWaitSec = {1, 2, 3};

        private const long DownloadSegmentSize = 1024 * 1024 * 128;

        private const int DownloadTimeout = 15000;

        private const int ReadKbBufferSize = 32;

        public IMessengerHub Msg { get; private set; }
        public string Folder { get; private set; }
        public string Dest { get; private set; }
        public string Url { get; private set; }
        public int FileIndex { get; private set; }
        public int FilesTotal { get; private set; }
        public long FileSize { get; private set; }
        public int MaxAttempts { get; private set; }
        public CancellationToken? CancellationToken { get; private set; }
        public long DownloadTotalTimeMs { get; private set; } = 0;
        public long DownloadTotalBytes { get; private set; } = 0;
        // Hex string
        public string VerificationValue { get; }

        public bool IsCancelled => CancellationToken?.IsCancellationRequested ?? false;

        public List<DownloadFileSegmentContext> DownloadFileSegments { get; private set; }

        public DownloadFile(
            IMessengerHub msg,
            string folder,
            string destPath,
            string sourceUrl,
            int maxAttempts,
            int fileIdx,
            int filesTotal,
            long fileSize,
            string validationValue,
            CancellationToken? cancelToken)
        {
            VerificationValue = validationValue;
            Msg = msg;
            Folder = folder;
            Dest = destPath;
            Url = sourceUrl;
            MaxAttempts = maxAttempts;
            FileIndex = fileIdx;
            FilesTotal = filesTotal;
            FileSize = fileSize;
            CancellationToken = cancelToken;

            DownloadFileSegments = generateSegments(DownloadSegmentSize);
        }

        private List<DownloadFileSegmentContext> generateSegments(long segmentMaxLength)
        {
            var ret = new List<DownloadFileSegmentContext>();
            long sizeRemaining = FileSize;
            int count = 0;
            while (sizeRemaining > 0)
            {
                long segSize = Math.Min(segmentMaxLength, sizeRemaining);

                ret.Add(new DownloadFileSegmentContext()
                {
                    Idx = count++,
                    File = this,
                    Offset = ret.Count == 0 ? 0 : (ret[ret.Count - 1].Offset + ret[ret.Count - 1].Size),
                    Size = segSize
                });

                sizeRemaining -= segSize;
            }

            return ret;
        }

        public string TryGetDestFileName()
        {
            try
            {
                return Path.GetFileName(Dest);
            }
            catch (Exception)
            {
            }

            return null;
        }


        public Task<DownloadFileResult> StartDownloadTask(
            Action<DownloadFileResult> callbackOnComplete)
        {
            return Task.Run<DownloadFileResult>(async () =>
            {
                Msg.Publish(new MessageDownloadFileStarted(this, Folder, Dest));

                DownloadFileResult ret = await DownloadAsync();

                if (IsCancelled)
                {
                    Msg.Publish(new MessageDownloadFileCanceled(this, Folder, Dest));
                    ret.Result = EDownloadResult.Cancel;
                }

                callbackOnComplete(ret);

                return ret;
            });
        }

        private bool TryEnsureDirectoryExists()
        {
            var dirName = Path.GetDirectoryName(Dest);

            if (Directory.Exists(dirName)) return true;

            try
            {
                // NOTE: (thread safety) if directory already exists, this create function won't throw
                Directory.CreateDirectory(dirName);
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't create directory", new { dirName });
                return false;
            }

            return true;
        }

        /// <summary>
        /// Downloads a file to disk asynchronously.
        /// </summary>
        /// <returns>Returns false if the file did not download correctly or was canceled.</returns>
        private async Task<DownloadFileResult> DownloadAsync()
        {
            Debug.Assert(MaxAttempts > 1);
            int maxAttempts = Math.Max(2, MaxAttempts);

            var ret = new DownloadFileResult()
            {
                Result = EDownloadResult.DownloadFailed,
                File = this
            };

            if (!TryEnsureDirectoryExists())
            {
                ret.Result = EDownloadResult.TargetPathCreateFail;
                return ret;
            }

            if (ret.File.FileSize == 0)
            {
                // nothing to download, just create a zero byte file
                Log.Information($"[DWN] Creating zero byte file at {Dest}");

                TryCreateZeroLengthFile(ref ret);
                return ret;    
            }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously

            int segmentIdx = 0;
            var runSuccess = await LogicUtilities.DoWithRetryAsync(
                taskWithAttemptCount: async (attempt) => AttemptDownload(
                    attemptCount: attempt, 
                    segmentIdx: segmentIdx, 
                    ret: ref ret),
                maxRetryCount: maxAttempts - 1,
                retryWaitSec: RetryWaitSec,
                cancelToken: CancellationToken
            );

#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            if (IsCancelled)
            {
                ret.Result = EDownloadResult.Cancel;
            }
            else if (!ret.IsSuccess)
            {
                // attempt cleanup
                try
                {
                    if (File.Exists(Dest))
                    {
                        File.Delete(Dest);
                    }
                }
                catch (Exception ex)
                {
                    Log.Warning(ex, "[DWN] Couldn't clean up file");
                }

                Log.Warning($"[DWN] DownloadPlugin.downloadAsync failed with: {ret.Result}");
            }

            return ret;
        }

        private bool TryCreateZeroLengthFile(ref DownloadFileResult ret)
        {
            try
            {
                File.WriteAllBytes(Dest, new byte[] {});
                ret.Result = EDownloadResult.Success;
                return true;
            }
            catch (Exception ex)
            {
                // log the exception type
                Log.Error(ex, $"[DWN] Couldn't create empty file at '{Dest}'");
                ret.Result = TranslateExceptionToDownloadResult(ex);
                return false;
            }
        }


        private EDownloadResult TranslateExceptionToDownloadResult(Exception ex)
        {
            // $ jbil #REFACTOR -- combine this with exception tracking in downloadSegment()
            if (ex is PathTooLongException)
            {
                //     The specified path, file name, or both exceed the system-defined maximum length.
                //     For example, on Windows-based platforms, paths must be less than 248 characters,
                //     and file names must be less than 260 characters.
                return EDownloadResult.TargetNameInvalid;
            }

            if (ex is DirectoryNotFoundException)
            {
                //     The specified path is invalid (for example, it is on an unmapped drive).
                return EDownloadResult.TargetNameInvalid;
            }

            if (ex is UnauthorizedAccessException)
            {
                //     path specified a file that is read-only.-or- This operation is not supported
                //     on the current platform.-or- path specified a directory.-or- The caller does
                //     not have the required permission.
                return EDownloadResult.TargetSecurityInvalid;
            }

            if (ex is NotSupportedException)
            {
                //     path is in an invalid format.
                return EDownloadResult.TargetNameInvalid;
            }

            if (ex is ArgumentException)
            {
                //     path is a zero-length string, contains only white space, or contains one or more
                //     invalid characters as defined by System.IO.Path.InvalidPathChars.
                return EDownloadResult.TargetNameInvalid;
            }

            if (ex is System.Security.SecurityException)
            {
                //     The caller does not have the required permission.
                return EDownloadResult.TargetSecurityInvalid;
            }

            if (ex is IOException)
            {
                //     An I/O error occurred while opening the file.
                // maybe recoverable after wait?
                return EDownloadResult.UnknownIORecoverable;
            }

            // unknown .. maybe recoverable after wait?
            return EDownloadResult.UnknownIORecoverable;
        }

        private bool AttemptDownload(
            int attemptCount, 
            int segmentIdx, 
            ref DownloadFileResult ret)
        {
            Debug.Assert(FileSize > 0, "zero byte file encountered");

            var folder = "";
            if (Dest != null)
            {
                folder = Path.GetDirectoryName(Dest);
            }

            if (Url == null)
            {
                Log.Error($"[DWN] Url missing for file idx '{FileIndex}'");
                ret.Result = EDownloadResult.MissingUrl;
                return true;    // bail
            }
            else if (string.IsNullOrEmpty(folder))
            {
                Log.Error($"[DWN] Target InstallDirectory invalid for file '{Url}'");
                ret.Result = EDownloadResult.TargetNameInvalid;
                return true;    // bail
            }
            else
            {
                if (attemptCount > 1)
                {
                    Log.Warning($"[DWN] Download failed: Retry attempt {attemptCount} of {MaxAttempts}");
                    Msg.Publish(new MessageDownloadFileRetried(this, folder, Dest));
                }

                try
                {
                    // if we made it past the first segment, don't wipe the file out (continue)
                    bool continueLastSession = segmentIdx > 0;
                    Debug.Assert(segmentIdx < DownloadFileSegments.Count);

                    using (var outStream = new FileStream(Dest, continueLastSession ? FileMode.Open : FileMode.Create,
                        FileAccess.Write))
                    {
                        for (; segmentIdx < DownloadFileSegments.Count; ++segmentIdx)
                        {
                            if (IsCancelled)
                            {
                                ret.Result = EDownloadResult.Cancel;
                                return true;
                            }

                            var segment = DownloadFileSegments[segmentIdx];
                            DownloadSegment(
                                outStream: outStream,
                                folder: folder,
                                segment: segment,
                                segmentCount: DownloadFileSegments.Count,
                                result: ref ret);

                            if (!ret.IsSuccess)
                            {
                                // don't advance segment idx until we are successful
                                break;
                            }
                        }

                        outStream.Close();
                    }
                }
                catch (IOException ex)
                {
                    int errorCode = ex.HResult & 0xFFFF;

                    FuelLog.Error(ex, "Couldn't write download file, Ran into IOException", new { name = TryGetDestFileName(), hresult = errorCode});
                    const int ERROR_LOCK_VIOLATION = 33;
                    const int ERROR_SHARING_VIOLATION = 32;

                    

                    if (errorCode == ERROR_LOCK_VIOLATION || errorCode == ERROR_SHARING_VIOLATION)
                    {
                        ret.Result = EDownloadResult.LockedFile;
                    }
                    else
                    {
                        ret.Result = EDownloadResult.UnknownRecoverable;
                    }
                }
                catch (Exception ex)
                {
                    FuelLog.Error(ex, "Couldn't write download file for unknown exception.", new { name=TryGetDestFileName() });
                    ret.Result = EDownloadResult.UnknownRecoverable;
                }
            }

            return !ret.Result.WantsDownloadFileRetry();
        }

        private void DownloadSegment(
            FileStream outStream,
            string folder,
            DownloadFileSegmentContext segment,
            int segmentCount,
            ref DownloadFileResult result)
        {

            result.Result = EDownloadResult.DownloadFailed;
            result.File = this;

            Debug.Assert(segmentCount != 0);
            Debug.Assert(segment.File == this);
            Debug.Assert(segment.Size != 0);
            Debug.Assert(segment.Idx < segmentCount);

            var pathRoot = Path.GetPathRoot(folder);
            DriveInfo driveInfo = new DriveInfo(pathRoot);
            if (segment.Size > driveInfo.AvailableFreeSpace)
            {
                Log.Error($"[DWN] Out of disk space (want: {segment.Size}, have: {driveInfo.AvailableFreeSpace})");
                result.Result = EDownloadResult.NotEnoughDiskSpace;
                return;
            }

            Log.Verbose(
                $"[DWN] DownloadSegment '{segment.File?.TryGetDestFileName()}' {segment.Idx + 1} of {segmentCount} ({segment.Offset}+{segment.Size} of {FileSize})");

            WebResponse response = TryGetResponseAndHandleErrors(
                folder: folder,
                rangeFrom: segment.Offset,
                rangeTo: segment.Offset + segment.Size - 1,
                result: ref result);

            // check cancellation
            if (IsCancelled
                || result.Result == EDownloadResult.Cancel)
            {
                response?.Close();
                result.Result = EDownloadResult.Cancel;
            }

            if (result.Result == EDownloadResult.InvalidSourceName)
            {
                // retries won't help
                Log.Error(
                    $"[DWN] Critical download failure encountered while downloading from '{Url}' '{result.Result}'");
                response?.Close();
                return;
            }

            if (response != null)
            {
                TryReadResponse(outStream: outStream,
                    response: response,
                    segment: segment,
                    segmentOffset: segment.Offset,
                    segmentLength: segment.Size,
                    result: ref result);
            }
        }

        private bool TryReadResponse(FileStream outStream,
            WebResponse response,
            DownloadFileSegmentContext segment,
            long segmentOffset,
            long segmentLength,
            ref DownloadFileResult result)
        {
            Debug.Assert(segment.File == this);

            long bytesTotal = 0;
            Exception hitException = null;

            try
            {
                bytesTotal = response.ContentLength;

                var dlTimer = new Stopwatch();
                dlTimer.Start();

                var responseStream = response.GetResponseStream();

                var readSuccessful = ReadStreamToDisk(
                    inStream: responseStream,
                    outStream: outStream,
                    segment: segment,
                    readOffset: segmentOffset,
                    readLength: segmentLength,
                    streamLength: bytesTotal);

                dlTimer.Stop();
                DownloadTotalTimeMs += dlTimer.ElapsedMilliseconds;

                if (IsCancelled)
                {
                    result.Result = EDownloadResult.Cancel;
                    readSuccessful = false;
                }
                else if (readSuccessful)
                {
                    result.Result = EDownloadResult.Success;
                    result.BytesDownloaded += bytesTotal;
                }
                else
                {
                    FuelLog.Warn($"Download failure for file", new { Url, Dest, segmentOffset, segmentLength });
                }

                return readSuccessful;
            }
            catch (OperationCanceledException)
            {
                result.Result = EDownloadResult.Cancel;
                FuelLog.Info("Download operation cancelled for file", new { Url, Dest, segmentOffset, segmentLength });
                // don't log exception
            }
            // lots can go wrong when opening a FileStream or calling Path.GetDirectoryName() (bubbled up from ReadStreamToDisk())
            // enumerating each failure, per the docs. We'll log the specific error, but generalize for the caller to handle.
            catch (ArgumentNullException ex)
            {
                //     path is null.
                result.Result = EDownloadResult.TargetNameInvalid;
                hitException = ex;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                //     mode contains an invalid value.
                result.Result = EDownloadResult.TargetNameInvalid;
                hitException = ex;
            }
            catch (ArgumentException ex)
            {
                //     path is an empty string (""), contains only white space, or contains one or more
                //     invalid characters. -or-path refers to a non-file device, such as "con:", "com1:",
                //     "lpt1:", etc. in an NTFS environment.
                result.Result = EDownloadResult.TargetNameInvalid;
                hitException = ex;
            }
            catch (NotSupportedException ex)
            {
                //     path refers to a non-file device, such as "con:", "com1:", "lpt1:", etc. in a
                //     non-NTFS environment.
                result.Result = EDownloadResult.TargetNameInvalid;
                hitException = ex;
            }
            catch (FileNotFoundException ex)
            {
                //     The file cannot be found, such as when mode is FileMode.Truncate or FileMode.Open,
                //     and the file specified by path does not exist. The file must already exist in
                //     these modes.
                result.Result = EDownloadResult.TargetNameInvalid;
                hitException = ex;
            }
            catch (System.Security.SecurityException ex)
            {
                //     The caller does not have the required permission.
                result.Result = EDownloadResult.TargetSecurityInvalid;
                hitException = ex;
            }
            catch (DirectoryNotFoundException ex)
            {
                //     The specified path is invalid, such as being on an unmapped drive.
                result.Result = EDownloadResult.TargetNameInvalid;
                hitException = ex;
            }
            catch (PathTooLongException ex)
            {
                //     The specified path, file name, or both exceed the system-defined maximum length.
                //     For example, on Windows-based platforms, paths must be less than 248 characters,
                //     and file names must be less than 260 characters.

                result.Result = EDownloadResult.TargetPathTooLong;
                hitException = ex;
            }
            catch (UnauthorizedAccessException ex)
            {
                //     The access requested is not permitted by the operating system for the specified
                //     path, such as when access is Write or ReadWrite and the file or directory is
                //     set for read-only access.
                result.Result = EDownloadResult.TargetSecurityInvalid;
                hitException = ex;
            }
            catch (IOException ex)
            {
                //     An I/O error, such as specifying FileMode.CreateNew when the file specified by
                //     path already exists, occurred. -or-The stream has been closed.

                // NOTE: internet disconnects often incur this error, this may be recoverable with retries
                result.Result = EDownloadResult.UnknownIORecoverable;
                hitException = ex;
            }
            catch (WebException ex)
            {
                result.Result = EDownloadResult.UnknownWebRecoverable;
                hitException = ex;
            }
            catch (Exception ex)
            {
                result.Result = EDownloadResult.UnknownRecoverable;
                hitException = ex;
            }
            finally
            {
                response?.Close();

                if (!result.IsSuccess)
                {
                    // remove the busted file so we can try again later
                    try
                    {
                        if (File.Exists(Dest))
                        {
                            // clean up
                            File.Delete(Dest);
                        }
                    }
                    catch (Exception e)
                    {
                        FuelLog.Warn(e, "Couldn't cleanup file", new { Dest });
                    }
                }
            }

            // If we're here, we haven't successfully downloaded. Rewind our progress.
            if (segment.DownloadProgress > 0)
            {
                Msg.Publish(new MessageUpdateOperationProgress(
                    this,
                    Folder,
                    Dest,
                    -segment.DownloadProgress,
                    0,
                    bytesTotal,
                    MessageUpdateOperationProgress.EFileOp.Cancelling));
                segment.DownloadProgress = 0;
            }

            // log the exception in a common way
            if (hitException != null)
            {
                FuelLog.Error(hitException, $"Exception encountered during download of file", new { Url, Dest, segmentOffset, segmentLength });
            }

            return false;
        }

        private WebResponse TryGetResponseAndHandleErrors(
            string folder,
            long rangeFrom,
            long rangeTo,
            ref DownloadFileResult result)
        {
            try
            {
                return DownloadPolicyLibrary.NetworkOutagePolicy.Execute(
                    ct =>
                    {
                        var request = WebRequest.CreateHttp(Url);
                        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                        request.AddRange(rangeFrom, rangeTo); // bytes is inclusive

                        return request.GetResponse();
                    }, CancellationToken.HasValue ? CancellationToken.Value : default(CancellationToken));
            }
            catch (ProtocolViolationException ex)
            {
                //     System.Net.HttpWebRequest.Method is GET or HEAD, and either System.Net.HttpWebRequest.ContentLength
                //     is greater or equal to zero or System.Net.HttpWebRequest.SendChunked is true.-or-
                //     System.Net.HttpWebRequest.KeepAlive is true, System.Net.HttpWebRequest.AllowWriteStreamBuffering
                //     is false, System.Net.HttpWebRequest.ContentLength is -1, System.Net.HttpWebRequest.SendChunked
                //     is false, and System.Net.HttpWebRequest.Method is POST or PUT. -or- The System.Net.HttpWebRequest
                //     has an entity body but the System.Net.HttpWebRequest.GetResponse method is called
                //     without calling the System.Net.HttpWebRequest.GetRequestStream method. -or- The
                //     System.Net.HttpWebRequest.ContentLength is greater than zero, but the application
                //     does not write all of the promised data.

                // $ jbilas -- log and just try again?
                result.Result = EDownloadResult.UnknownRecoverable;
                FuelLog.Error(ex, "Couldn't download file (recoverable, ProtocolViolationException detected during download)", new {Url});
            }
            catch (NotSupportedException ex)
            {
                //     The request cache validator indicated that the response for this request can
                //     be served from the cache; however, this request includes data to be sent to the
                //     server. Requests that send data must not use the cache. This exception can occur
                //     if you are using a custom cache validator that is incorrectly implemented.

                // $ jbilas -- log and just try again?
                result.Result = EDownloadResult.UnknownRecoverable;
                FuelLog.Error(ex, "Couldn't download file (recoverable, NotSupportedException detected during download)", new { Url });
            }

            catch (WebException ex)
            {
                //     System.Net.HttpWebRequest.Abort was previously called.-or- The time-out period
                //     for the request expired.-or- An error occurred while processing the request.
                if (IsCancelled)
                {
                    Log.Information(ex, $"[DWN] File download cancelled for '{Url}");
                    result.Result = EDownloadResult.Cancel;
                }
                else if (ex.Response is HttpWebResponse
                         && ((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.Forbidden)
                {
                    // [FLCL-463] treat 403's as expired files
                    // [FLCL-464] eventually, we should allow for server authoritative control of expiration
                    Log.Warning($"[DWN] MessageDownloadFileExpired @ '{Url}' localTime:'{DateTime.Now}'");
                    Msg.Publish(new MessageDownloadFileExpired(this, folder, Dest));
                    result.Result = EDownloadResult.ExpiredFileEncountered;
                }
                else
                {
                    Log.Warning(ex, $"[DWN] Socket communication failure for '{Url}', '{ex.Status}'");
                    result.DownloadWebStatus = ex.Status;
                    result.Result = EDownloadResult.SocketCommFailure;
                }

                Log.Warning(ex, $"[DWN] WebException '{ex.GetType()}' during download from '{Url}'");
            }
            catch (InvalidOperationException ex)
            {
                // The stream is already in use by a previous call to System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object).-or-
                // System.Net.HttpWebRequest.TransferEncoding is set to a value and System.Net.HttpWebRequest.SendChunked
                // is false.
                FuelLog.Error(ex, "Couldn't download file (InvalidOperationException detected during download)", new {Url});
                result.Result = EDownloadResult.UnknownRecoverable;
            }
            catch (OperationCanceledException)
            {
                Log.Information("[DWN] Download operation cancelled");
                result.Result = EDownloadResult.Cancel;
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Couldn't download file (Exception)", new {Url});
                result.Result = EDownloadResult.UnknownRecoverable;
            }

            return null;
        }

        /// <summary>
        /// Given a file and a response stream, read the stream to disk 32 KB at a time synchronously
        /// Can be canceled by setting the context's cancel flag.
        /// </summary>
        private bool ReadStreamToDisk(Stream inStream,
                                        FileStream outStream,
                                        DownloadFileSegmentContext segment,
                                        long readOffset,
                                        long readLength,
                                        long streamLength)
        {
            //Read file 32kb at a time to disk.            
            inStream.ReadTimeout = DownloadTimeout;
            var file = segment.File;

            byte[] buffer = new byte[1024 * ReadKbBufferSize];
            segment.DownloadProgress = 0;

            while (segment.DownloadProgress != readLength)
            {
                int tgtBytes = Math.Min(buffer.Length, (int)(readLength - segment.DownloadProgress));

                int readBytes = inStream.Read(buffer, 0, tgtBytes);

                // End-of-stream check
                if (readBytes == 0)
                {
                    throw new Exception(
                        $"Unexpected end-of-stream after {segment.DownloadProgress} expected read length of {readLength}");
                }

                DownloadTotalBytes += readBytes;

                lock (segment.File)
                {
                    outStream.Seek(readOffset + segment.DownloadProgress, SeekOrigin.Begin);
                    outStream.Write(buffer, 0, readBytes);
                }

                segment.DownloadProgress += readBytes;
                Msg.Publish(new MessageUpdateOperationProgress(
                    this, 
                    folder: Folder, 
                    file: Dest, 
                    bytesDelta: readBytes, 
                    bytesReceived: segment.DownloadProgress, 
                    bytesTotal: streamLength, 
                    operation: MessageUpdateOperationProgress.EFileOp.Downloading));

                if (IsCancelled)
                {
                    return false;
                }
            }

            //All done break the retry loop:
            return true;
        }


    }
}
