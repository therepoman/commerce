﻿using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Exceptions;
using Amazon.Fuel.Common.Utilities;
using Polly;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public static class DownloadPolicyLibrary
    {
        public static Policy NetworkOutagePolicy
        {
            get
            {
                return Policy
                .Handle<WebException>(isHandledNetworkException)
                .Or<HttpRequestException>(ex =>
                {
                    WebException webEx = ex.InnerException as WebException;

                    if (webEx == null)
                    {
                        return false;
                    }

                    return isHandledNetworkException(webEx);
                })
                .WaitAndRetryForever(
                    retryAttempt => TimeSpan.FromMilliseconds(Math.Min(5000, 100 * Math.Pow(retryAttempt, 1.5))),
                    (ex, ts) =>
                    {
                        FuelLog.Info("Network outage detected", new { waitFor = ts.TotalSeconds });
                    });
            }
        }

        private static bool isHandledNetworkException(WebException webEx)
        {
            if (webEx.Status == WebExceptionStatus.NameResolutionFailure)
            {
                return true;
            }

            var innerEx = webEx.InnerException as SocketException;
            return innerEx != null
                   && (innerEx.SocketErrorCode == SocketError.NetworkDown
                       || innerEx.SocketErrorCode == SocketError.HostUnreachable);
        }

        public static Policy NonAuthExceptionPolicy
        {
            get
            {
                return Policy
                .Handle<Exception>(e => !(e is TwitchAuthException))
                .WaitAndRetry(3,
                    retryAttempt => TimeSpan.FromMilliseconds(100 * Math.Pow(retryAttempt, 1.5)),
                    (ex, ts) =>
                    {
                        FuelLog.Info("Ran into NonAuth, NonNetworkOutage exception to retry", new {exceptionType = ex.GetType(), ex.Message});
                    });
            }
        }
    }
}
