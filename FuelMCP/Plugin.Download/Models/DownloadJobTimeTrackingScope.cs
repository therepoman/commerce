using System;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Download.Models
{
    public class DownloadJobTimeTrackingScope : IDisposable
    {
        private const int TimerRefreshRateMs = 100;
        private const int TimerSamplesCount = (1000 / TimerRefreshRateMs) * 10; // use last 10 seconds of data
        private const int MinTimerSamplesBeforeValid = (1000 / TimerRefreshRateMs) * 4; // require at least 4 seconds of data
        private const float TimerEstimateIntervalSec = 2.0f;

        private readonly TransferTimer _timer;
        private readonly IDownloadJob _job;

        public DownloadJobTimeTrackingScope(IDownloadJob job)
        {
            _job = job;

            _timer = new TransferTimer(source: _job, transferRefreshRateMs: TimerRefreshRateMs, timeEstIntervalSec: TimerEstimateIntervalSec, maxHistoryCount: TimerSamplesCount)
            {
                IsEnabled = true
            };
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public int GetSecondsRemaining(out bool outNotEnoughData)
        {
            outNotEnoughData = _timer.BytesReceivedHistory.Count < MinTimerSamplesBeforeValid;
            return (int)Math.Ceiling(_timer.EstimateSecLeft(_job.TotalBytes - _job.ReceivedBytes));
        }
    }
}