﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Messages;
using Amazon.Fuel.Common.Extensions;
using Serilog;
using Sds = Tv.Twitch.Fuel.Sds;
using Amazon.Fuel.SoftwareDistributionService.Client;
using System.Linq;
using System.Net.Http;
using Amazon.Fuel.Common;
using Amazon.Fuel.Plugin.Download.Interfaces;
using Amazon.Fuel.Plugin.Persistence;
using Amazon.Fuel.SoftwareDistributionService.Model;
using Amazon.Fuel.Plugin.SecureStorage;

namespace Amazon.Fuel.Plugin.Download.Models
{
    /// <summary>
    /// DownloadJob contains the workflow for downloading all files for a single ADG product. This includes
    /// the preliminary step of downloading the manifest, controlling the getpatch calls to obtain file
    /// download info, downloading each file, verifying each file and handling error workflow (fail/retry logic).
    /// </summary>
    public class DownloadJob : Disposable, IDownloadJob
    {
        private const int MaxDownloadAttempts = 3;
        private const int ConnectionLimit = 24;
        private const int SimultaneousTransfers = 5;

        private readonly Sds.Manifest _previousManifest;
        private readonly IInstallUtil _installUtil;
        private readonly string _entitlementId;
        private readonly IMessengerHub _hub;
        private readonly IOSDriver _osDriver;

        private readonly Dictionary<EDownloadResult, int> _maxCountBeforeJobFailure = new Dictionary<EDownloadResult, int>()
        {
            { EDownloadResult.SocketCommFailure, 10 },
            { EDownloadResult.ExpiredFileEncountered, 10 },
            { EDownloadResult.UnknownRecoverable, 10 },
            { EDownloadResult.UnknownWebRecoverable, 10 },
            { EDownloadResult.UnknownIORecoverable, 10 },
            { EDownloadResult.NotEnoughDiskSpace, 0 },
            { EDownloadResult.LockedFile, 0}
        };
        private IDownloadOrderStrategy _downloadOrderStrategy = DownloadFilesInterleavedStrategy.Instance;
        private GetPatchBatcher _getPatchBatcher;

        private EDownloadSource _downloadSource;
        private SecureSQLiteStorage _fileVerificationSHACacheDb;
        private IHashCache _fileVerificationSHACache;

        public int DownloadedFileCount => _jobResult.FilesCompleted;
        public IList<IDownloadJobQueuedFile> QueuedFiles { get; private set; } = new List<IDownloadJobQueuedFile>();

        public Guid JobId { get; }

        public ProductId ProductId { get; }
        public string InstallDirectory { get; }
        public IDownloadJobManager JobManager { get; }
        public long TotalBytes { get; private set; }
        public DownloadJobResult LastResult => _jobResult;

        public bool IsUpdate { get; }

        private long _receivedBytes = 0;
        public long ReceivedBytes => _receivedBytes;

        public IList<DownloadFile> DownloadFiles { get; } = new List<DownloadFile>();

        public int TimeSpentDownloadingFilesInMs;

        // cancellation token is used to determine result (user invoked), whereas abort encompasses both user invoked and
        // failure oriented cancellation
        private readonly CancellationTokenSource _abortTokenSource = new CancellationTokenSource();
        private readonly CancellationToken _abortToken;
        public CancellationToken CancelToken { get; private set; }

        public bool IsCancelled => CancelToken.IsCancellationRequested;
        public bool IsAborted => _abortToken.IsCancellationRequested || ( _getPatchBatcher?.IsAborted ?? false );
        public long TotalSizeInBytes => _jobResult.ManifestWithVersion?.ManifestData?.ComputeTotalSizeInBytes() ?? 0L;
        public bool HadAuthFailure { get; private set; } = false;

        private DownloadJobResult _jobResult = new DownloadJobResult();

        public new string ToString() => $"Id:'{ProductId}', InstallDirectory:'{InstallDirectory}', TotalBytes:'{TotalBytes}', "
                                           + $"Source:'{_downloadSource}', IsCancelled:'{IsCancelled}'";

        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly ITwitchTokenProvider _twitchTokenProvider;
        public string VersionId => _jobResult.ManifestWithVersion.SdsVersionId;
        public string WorkDir => Path.Combine(InstallDirectory, "work-" + VersionId);
        private IMetricsPlugin _metrics;

        private DateTime _timeLastLog = DateTime.Now;

        private const int _logIntervalInMs = 60000;

        public enum EDownloadManifestRefreshResult
        {
            Success,
            UnknownFailure,
            NetworkFailure
        }

        private class DownloadJobHashCachePersistence : PersistencePlugin
        {
            public DownloadJobHashCachePersistence(IOSDriver osDriver, ProductId prodId) : base(Path.Combine(osDriver.GameDataPath, prodId.Id, Constants.Paths.SqliteDatabaseSubDir)) { }
        }

        public DownloadJob(
            ISoftwareDistributionServiceClient sds,
	        ITwitchTokenProvider twitchTokenProvider,
            IDownloadJobManager jobManager,
            IInstallUtil installUtil,
            ProductId productId,
            string entitlementId,
            string installDirectory,
            bool isUpdate,
            EDownloadSource downloadSource,
            Sds.Manifest previousManifest,
            IMetricsPlugin metrics,
            IOSDriver osDriver,
            IMessengerHub hub,
            CancellationToken cancelToken = default(CancellationToken))
        {
            JobManager = jobManager;
            ProductId = productId;
            _entitlementId = entitlementId;
            InstallDirectory = installDirectory;
            _installUtil = installUtil;
            IsUpdate = isUpdate;
            _downloadSource = downloadSource;
            CancelToken = cancelToken;
            _previousManifest = previousManifest;
            _sds = sds;
            _twitchTokenProvider = twitchTokenProvider;
            _metrics = metrics;
            _hub = hub;
            _osDriver = osDriver;
            JobId = new Guid();

            _abortToken = CancellationTokenSource.CreateLinkedTokenSource(cancelToken, _abortTokenSource.Token).Token;

            PrepareHashcache();
        }

        private void PrepareHashcache()
        {
            var persistence = new DownloadJobHashCachePersistence(_osDriver, ProductId);

            if (!Directory.Exists(persistence.DataRoot()))
            {
                Directory.CreateDirectory(persistence.DataRoot());
            }

            _fileVerificationSHACacheDb = new SecureSQLiteStorage(
                persistence,
                _metrics,
                new SecureStorageUnprotectedPolicy(),
                dbSubdir: "",
                dbFilename: Constants.Paths.SqliteHashcacheFilename);
            _fileVerificationSHACacheDb.Init();

            _fileVerificationSHACache = new HashCache(_fileVerificationSHACacheDb, _hub);
        }

        /// <summary>
        /// Downloads the manifest for the ADG product id belonging to the job.
        /// </summary>
        private async Task DownloadManifestAsync()
        {
            var refreshManifestResult = await TryRefreshManifest();

            if (refreshManifestResult == EDownloadManifestRefreshResult.Success)
            {
                _jobResult.Result = EDownloadJobResult.Success;
            }
            else
            {
                if (CancelToken.IsCancellationRequested)
                {
                    _jobResult.Result = EDownloadJobResult.Cancel;
                }
                else if (refreshManifestResult == EDownloadManifestRefreshResult.NetworkFailure)
                {
                    _jobResult.Result = EDownloadJobResult.NetworkFailure;
                }
                else
                {
                    _jobResult.Result = EDownloadJobResult.DownloadManifestRefreshFailure;
                }
            }

            if (HadAuthFailure)
            {
                _jobResult.Result = EDownloadJobResult.AuthFailure;
            }

            if (refreshManifestResult != EDownloadManifestRefreshResult.Success
                && !IsCancelled)
            {
                _jobResult.ManifestDownloadFailed = true;
            }
        }

        /// <summary>
        /// Retrieve the manifest and manifest version associated with our entitlement id.
        /// </summary>
        /// <returns>Result of operation</returns>
        public async Task<EDownloadManifestRefreshResult> TryRefreshManifest()
        {
            var abortToken = CancellationTokenSource.CreateLinkedTokenSource(CancelToken, _abortToken).Token;

            // $ jbil #TODO -- correctly handle manifest deltas
            // $ jbil #TODO -- retain segment download state

            // download the manifest
            try
            {
                _jobResult.ManifestWithVersion =
                    await DownloadManifestWithVersionByEntitlementId(_entitlementId, ProductId, CancelToken);
            }
            catch (HttpRequestException ex)
            {
                Log.Error(ex, $"Network Failure when retrieving download manifest: {ex.Message}");
                return EDownloadManifestRefreshResult.NetworkFailure;
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error downloading manifest {ex.Message}");
                return EDownloadManifestRefreshResult.UnknownFailure;
            }

            if (_jobResult.ManifestWithVersion == null)
            {
                Log.Error("Unknown error downloading manifest");
                return EDownloadManifestRefreshResult.UnknownFailure;
            }

            TotalBytes = 0;

            // try/catch safeties, just in case
            try
            {
                QueuedFiles = await ComputeFilesToDownloadAndPublishAsync(
                    ProductId, 
                    _jobResult.ManifestWithVersion.ManifestData, 
                    _fileVerificationSHACache, 
                    InstallDirectory,
                    abortToken);

                TotalBytes = QueuedFiles.Select(f => f.ManifestFile.Size).Sum();
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"[DWN] Error calculating files to download for '{ProductId}'");
                return EDownloadManifestRefreshResult.UnknownFailure;
            }

            Debug.Assert(QueuedFiles != null);
            if (QueuedFiles.Count == 0)
            {
                // this might happen if you've got all files up to date, just record metric and continue installing
                Log.Warning($"[DWN] No files to download found in {ProductId}");
            }

            return EDownloadManifestRefreshResult.Success;
        }

        private async Task WrapListenersAsync(Func<Task> lambda)
        {
            using (var timeTracking = new DownloadJobTimeTrackingScope(this))
            { 
                Action<MessageUpdateOperationProgress> onUpdateOperationProgressEvt = m =>
                {
                    var folder = m.Folder.TrimEnd(Path.DirectorySeparatorChar);

                    if (!folder.StartsWith(this.InstallDirectory.TrimEnd(Path.DirectorySeparatorChar)))
                    {
                        return;
                    }

                    Interlocked.Add(ref _receivedBytes, m.BytesDelta);
                    bool secRemainingNotEnoughData;
                    var secRemaining = timeTracking.GetSecondsRemaining(out secRemainingNotEnoughData);

                    _hub.Publish(
                        new DownloadJobProgressMessage(
                            sender: this,
                            bytesDelta: m.BytesDelta,
                            bytesReceived: ReceivedBytes,
                            bytesTotal: TotalBytes,
                            timeRemaining: secRemaining,
                            timeRemainingNotEnoughData: secRemainingNotEnoughData));
                };

                using (new ScopedTinyMsgListener<MessageUpdateOperationProgress>(_hub, onUpdateOperationProgressEvt))
                {
                    await lambda();
                }
            }
        }

        /// <summary>
        /// Executes download of full job (download manifest & manifest files)
        /// </summary>
        /// <returns>Result of job session</returns>
        public async Task<DownloadJobResult> RunJobAsync()
        {
            try
            {
                await RunJob_CanThrow();
            }
            catch (OperationCanceledException)
            {
                if (IsAborted)
                {
                    FuelLog.Error("DownloadJobWithRetryAsync() aborted");

                    // leave result if something other than success
                    if (_jobResult.Result == EDownloadJobResult.Success)
                    {
                        _jobResult.Result = EDownloadJobResult.GeneralFailure;
                    }
                }
                else
                {
                    FuelLog.Error("DownloadJobWithRetryAsync() cancelled");
                    _jobResult.Result = EDownloadJobResult.Cancel;
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "DownloadJobWithRetryAsync() failed with Exception");
                _jobResult.Result = EDownloadJobResult.GeneralFailure;
            }

            // tally some job edge cases where the job result may not have updated properly
            if (IsCancelled)
            {
                _jobResult.Result = EDownloadJobResult.Cancel;
            }
            else if (HadAuthFailure)
            {
                _jobResult.Result = EDownloadJobResult.AuthFailure;
            }

            _jobResult.LogReport();

            CleanUnknownFiles();
            CleanFailedFiles();

            return _jobResult;
        }

        private async Task<DownloadJobResult> RunJob_CanThrow()
        {
            FuelLog.Info($"Starting job", new { ProductId, IsUpdate });

            // reset the result for new session
            _jobResult = new DownloadJobResult()
            {
                Result = EDownloadJobResult.NotRun
            };

            await DownloadManifestAsync();


            if (_jobResult.Result != EDownloadJobResult.Success)
            {
                return _jobResult;
            }

            _hub.Publish(new MessageTotalBytesNeededForDownload(this, InstallDirectory, InstallDirectory, TotalBytes));

            _hub.Publish(new MessageDownloadStarting(this));

            //Assert there is enough disk space to download the game.
            if (!JobHasDiskSpaceAvailable())
            {
                _jobResult.Result = EDownloadJobResult.NotEnoughDiskSpace;
                return _jobResult;
            }

            if (IsCancelled)
            {
                _jobResult.MarkCancelled();
                return _jobResult;
            }

            var sizeKb = TotalSizeInBytes / 1024;
            FuelLog.Info($"Starting job download", new { ProductId, sizeKb, IsUpdate });

            // remove any already downloaded files
            QueuedFiles = QueuedFiles.Where(f =>
            {
                DownloadFileResult result;
                return !_jobResult.LastResultByFileTgt.TryGetValue(Path.Combine(InstallDirectory, f.ManifestFile.Path), out result)
                       || result.Result != EDownloadResult.Success;
            }).ToList();

            int count = 0;

            var allTasks = new List<Task>();
            ServicePointManager.DefaultConnectionLimit = ConnectionLimit;
            _downloadOrderStrategy.SortFileOrder(QueuedFiles);

            JobManager.Metrics.AddCounter(
                MetricStrings.SOURCE_DownloadJob,
                _downloadSource == EDownloadSource.Primary ? MetricStrings.METRIC_DownloadSourcePrimary
                                                               : MetricStrings.METRIC_DownloadSourceSecondary);

            // Create the work directory
            if (!Directory.Exists(WorkDir))
            {
                var directoryInfo = Directory.CreateDirectory(WorkDir);
                directoryInfo.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }

            _getPatchBatcher = new GetPatchBatcher(
                files: QueuedFiles,
                sds: _sds,
                twitchTokenProvider: _twitchTokenProvider,
                metrics: JobManager.Metrics,
                adgProductId: ProductId,
                versionId: VersionId,
                entitlementId: _entitlementId,
                cancelToken: _abortToken
            );

            _getPatchBatcher.AuthFailureEvent += () =>
            {
                HadAuthFailure = true;
                _abortTokenSource.Cancel();
            };

            _getPatchBatcher.PatchInfoDownloadFailed += file =>
            {
                FuelLog.Error("No patch returned for file", 
                    new { path = file.ManifestFile.Path, value = file.ManifestFile.Hash.Value });
                _abortTokenSource.Cancel();
                _jobResult.Result = EDownloadJobResult.GeneralFailure;
            };

            _getPatchBatcher.Start();

            await WrapListenersAsync(async () =>
            {
                //Download the files we are missing / corrupted / patched:
                for (int i = 0; i < QueuedFiles.Count; ++i)
                {
                    if (IsAborted)
                    {
                        break;
                    }

                    var currentFile = QueuedFiles[i];

                    var manifestFile = currentFile.ManifestFile;
                    var targetFilePath = Path.Combine(InstallDirectory, manifestFile.Path);

                    // if original file is missing, make sure sha is null
                    if (!File.Exists(targetFilePath))
                    {
                        currentFile.ClearSourceHash();
                    }

                    var patchInfo = await _getPatchBatcher.ConsumePatchByFileAsync(currentFile);

                    if (patchInfo == null)
                    {
                        FuelLog.Error("PatchInfo couldn't download, aborting", new { file = currentFile.ManifestFile.Path });
                        _abortTokenSource.Cancel();
                        break;
                    }

                    if (IsAborted)
                    {
                        break;
                    }

                    var downloadPath = GetDownloadPath(currentFile, patchInfo, targetFilePath);

                    var downloadFile = new DownloadFile(
                        msg: _hub,
                        folder: InstallDirectory,
                        destPath: downloadPath,
                        sourceUrl: _downloadSource == EDownloadSource.Primary ? patchInfo.PrimarySource
                                                                                    : (patchInfo.SecondarySource ?? patchInfo.PrimarySource),
                        maxAttempts: MaxDownloadAttempts,
                        fileIdx: count,
                        filesTotal: QueuedFiles.Count,
                        fileSize: patchInfo.FileLength,
                        validationValue: patchInfo.Hash,
                        cancelToken: _abortToken
                    );

                    DownloadFiles.Add(downloadFile);

                    var downloadTask = StartDownloadTaskWithMessageListeners(downloadFile, patchInfo, manifestFile, targetFilePath);

                    allTasks.Add(downloadTask);

                    Interlocked.Increment(ref count);

                    //Keep SimultaneousTransfers worth of download tasks in the queue;
                    if (allTasks.Count >= SimultaneousTransfers)
                    {
                        var finishedTask = await Task.WhenAny(allTasks);
                        await finishedTask;
                        allTasks.Remove(finishedTask);
                    }
                }

                if (!IsAborted)
                {
                    await Task.WhenAll(allTasks);
                }
            });

            if (IsAborted)
            {
                // abort all subsystems
                if (_getPatchBatcher.IsAborted)
                {
                    FuelLog.Warn("GetPatchBatcher abort detected, aborting job");
                    _abortTokenSource.Cancel();
                }

                FileHashVerifier.Instance.CancelJob(JobId);
            }

            //Wait for the rest.
            await Task.WhenAll(allTasks);

            FuelLog.Info("Downloading files took " + TimeSpan.FromMilliseconds(TimeSpentDownloadingFilesInMs).ToString("G"));

            long megabytes = (TotalSizeInBytes / 1000000) > 0 ? (TotalSizeInBytes / 1000000) : Constants.MinGameSizeInMb;
            double normalizedLatency = (double)TimeSpentDownloadingFilesInMs / megabytes;
            _metrics.AddTimerValue(MetricStrings.SOURCE_DownloadJob, MetricStrings.METRIC_NormalizedTime, normalizedLatency);

            // TODO failures in hash verification force restart download .. improvement would be to reinject into file queue
            FileHashVerifier.Instance.WaitOnPendingTasks(this).GetAwaiter().GetResult();

            // Delete the temp work directory when we are done with the job.
            if (Directory.Exists(WorkDir))
            {
                Directory.Delete(WorkDir, true);
            }

            _jobResult.UpdateResult(wasCancelled: IsCancelled, wasAborted: IsAborted && !IsCancelled);

            var downloadRateKbSec = CalculateTransferKbPerSec();
            FuelLog.Info($"Job completed",
                new
                {
                    LastResult,
                    fileCount= DownloadFiles.Count,
                    downloadRateKbSec,
                    results = _jobResult.GetResultsSummary(),
                }); 

            JobManager.Metrics.AddDiscreteValue(
                MetricStrings.SOURCE_DownloadJob,
                MetricStrings.METRIC_JobAvgTransferKBPerSec,
                downloadRateKbSec);

            return _jobResult;
        }

        private async Task<DownloadFileResult> StartDownloadTaskWithMessageListeners(DownloadFile downloadFile, DownloadPatchInfo patchInfo, Sds.File manifestFile, string targetFilePath)
        {

            Stopwatch sw = Stopwatch.StartNew();
            var ret = await downloadFile.StartDownloadTask(
                callbackOnComplete: result => HandleFileDownloadComplete(file: downloadFile, fileResult: result, patchInfo: patchInfo, manifestFile: manifestFile, targetFilePath: targetFilePath));

            Interlocked.Add(ref TimeSpentDownloadingFilesInMs, (int) sw.ElapsedMilliseconds);

            return ret;
        }

        private void HandleFileDownloadComplete(
            DownloadFile file,
            DownloadFileResult fileResult,
            DownloadPatchInfo patchInfo,
            Sds.File manifestFile,
            string targetFilePath)
        {
            lock (_jobResult)
            {
                ++_jobResult.FilesCompleted;

                FileHashVerifier.Instance.SetMetrics(_metrics);

                if (fileResult == null)
                {
                    Log.Error("[DWN] Empty task result encountered");
                    return;
                }

                if ((DateTime.UtcNow - _timeLastLog).TotalMilliseconds > _logIntervalInMs)
                {
                    Log.Information("Downloading Files. Just downloaded file number " + _jobResult.FilesCompleted);
                    _timeLastLog = DateTime.UtcNow;
                }

                var isPatch = patchInfo.DeltaEncoding != DeltaEncoding.NONE;
                if (IsCancelled)
                {
                    fileResult.Result = EDownloadResult.Cancel;
                }
                else if (fileResult.IsSuccess)
                {
                    if (isPatch)
                    {   
                        var outputFilePath = file.Dest + ".out";
                        var patchTask = new PatchApplyTask(
                            patchInfo, 
                            manifestFile, 
                            file.Dest, 
                            targetFilePath,
                            outputFilePath, 
                            _fileVerificationSHACache);

                        var patchingResult = patchTask.Apply();

                        if (patchingResult == PatchingResult.SUCCESS)
                        {
                            // Only send this message once we've successfully verified the file, if the file is not patched, we will verify the file below.
                            JobManager.Metrics.AddCounter(MetricStrings.SOURCE_DownloadJob, "PatchSuccess");
                            _hub.Publish(new MessageDownloadFileFinished(this, this, InstallDirectory,
                                file.Dest));
                        }
                        else
                        {
                            if (patchingResult == PatchingResult.HASH_FAILED)
                            {
                                FuelLog.Error("Couldn't download file (patch failed during hash verification)",
                                    new { outputFilePath });
                            }
                            else
                            {
                                FuelLog.Error("Couldn't download file (patch failed)",
                                    new { outputFilePath, patchingResult });
                            }

                            JobManager.Metrics.AddCounter(MetricStrings.SOURCE_DownloadJob,
                                $"PatchFailure_{patchingResult}");
                            fileResult.Result = EDownloadResult.PatchFailure;

                            // make sure the base file is removed
                            FuelLog.Info("Removing failed patch base file", new { targetFilePath });
                            _fileVerificationSHACache.Remove(targetFilePath);

                            try
                            {
                                if (File.Exists(targetFilePath))
                                {
                                    File.Delete(targetFilePath);
                                }
                            }
                            catch (Exception e)
                            {
                                FuelLog.Error(e, "Couldn't remove file", new { targetFilePath });
                            }
                        }
                    }
                    else
                    {
                        // Queue the file for hash verification if we haven't verified it already (non-patched files)
                        FileHashVerifier.Instance.QueueFile(
                            jobId: JobId,
                            file: file,
                            forced: false,
                            hashCache: _fileVerificationSHACache,
                            callbackOnComplete: (queuedFile, hashMatch) =>
                            {
                                if (!hashMatch)
                                {
                                    fileResult.Result = EDownloadResult.HashVerifyFailure;
                                }
                                else
                                {
                                    _hub.Publish(new MessageDownloadFileFinished(this, this, InstallDirectory, file.Dest));
                                }
                            });
                    }
                }
                else
                {
                    // failure
                    _hub.Publish(new MessageDownloadFileFailed(this, InstallDirectory, file.Dest));
                }

                TallyFile(_jobResult, fileResult);
            }
        }

        private void TallyFile(DownloadJobResult jobResult, DownloadFileResult fileResult)
        { 
            jobResult.AddResult(this, fileResult.File, fileResult);

            if (jobResult.DownloadResultCounts.GetValueOrDefault(EDownloadResult.Success) == QueuedFiles.Count)
            {
                _hub.Publish(new MessageAllFilesDownloaded(this));
            }

            // check for job failure conditions that could cause an abort
            foreach (var kv in _maxCountBeforeJobFailure)
            {
                var fileFailureCount = jobResult.DownloadResultCounts.GetValueOrDefault(kv.Key);
                if (fileFailureCount > kv.Value)
                {
                    Log.Error(
                        $"Job aborted after '{kv.Key}' result count {fileFailureCount} of maximum {kv.Value} exceeded");
                    _abortTokenSource.Cancel();
                    break;
                }
            }
        }

        private async Task<ManifestWithVersion> DownloadManifestWithVersionByEntitlementId(string entitlementId, ProductId adgProductId, CancellationToken cancelToken = default(CancellationToken))
        {
            DateTime dtManifest = DateTime.UtcNow;
            Log.Information($"[DWN] Starting manifest download for '{entitlementId}'");
            var versionManifest = await JobManager.Entitlement.DownloadManifestAsync(entitlementId, adgProductId, cancelToken);
            Log.Information($"[DWN] ManifestWithVersion download completed in {DateTime.UtcNow - dtManifest}, result: {versionManifest != null}");

            if (versionManifest == null)
            {
                Log.Error($"[LCH] Game manifest download failed for entitlement '{entitlementId}'");
            }

            return versionManifest;
        }

        public int CalculateTransferKbPerSec()
        {
            long totalJobBytes = 0;
            long totalJobMs = 0;
            foreach (var file in DownloadFiles)
            {
                totalJobBytes += file.DownloadTotalBytes;
                totalJobMs += file.DownloadTotalTimeMs;
            }

            return totalJobMs == 0 ? 0 : (int)Math.Round((((double)totalJobBytes / 1024.0) / ((double)totalJobMs / 1000.0)));
        }

        /// <summary>
        /// Given a manifest, computes which files will require an update, this may include rehasing a file to determine
        /// if it requires an update. Will publish progress events to the messenger hub.
        /// </summary>
        /// <returns></returns>
        private async Task<List<IDownloadJobQueuedFile>> ComputeFilesToDownloadAndPublishAsync(
            ProductId productId,
            Sds.Manifest manifest,
            IHashCache cache,
            string folder,
            CancellationToken? cancellationToken = null)
        {
            FuelLog.Info(
                "DownloadPlugin.ComputeFilesToDownloadAndPublishAsync", 
                new
                {
                    productId,
                    manifestFileCount = manifest?.Packages?.FirstOrDefault()?.Files?.Count,
                    folder
                });
            List<IDownloadJobQueuedFile> output = new List<IDownloadJobQueuedFile>();

            var sw = Stopwatch.StartNew();

            int count = 0;
            foreach (var file in manifest.Packages[0].Files)
            {
                count++;
                var dest = Path.Combine(folder, file.Path);

                bool needsDownload = true;
                string sha256 = null;
                if (File.Exists(dest))
                {
                    //TODO: Optionally, Don't bother scanning, just mark it for re-download?
                    long lastBytesScanned = 0;
                    sha256 = await cache.UpdateFileHashAsync(dest, false, (bytesRead, bytesTotal) =>
                    {
                        _hub.Publish(new MessageUpdateOperationProgress(
                            manifest, 
                            folder, 
                            dest,
                            bytesRead - lastBytesScanned, 
                            bytesRead, 
                            bytesTotal, 
                            MessageUpdateOperationProgress.EFileOp.Verifying));
                        lastBytesScanned = bytesRead;
                    });

                    if (cancellationToken?.IsCancellationRequested ?? false)
                    {
                        return new List<IDownloadJobQueuedFile>();
                    }

                    if (file.Hash.HexString().TrimStart('0')
                        .Equals(sha256.TrimStart('0'), StringComparison.InvariantCultureIgnoreCase))
                    {
                        needsDownload = false;
                    }
                }

                if (needsDownload)
                {
                    // TODO : add a factory or adapter to remove concrete type requirement
                    var sourceHash = sha256 == null ? null : new Hash
                    {
                        algorithm = HashAlgorithm.SHA256,
                        value = sha256.ToLower()
                    };
                    output.Add(new DownloadJobQueuedFile(sourceHash, file, count));
                }
            }

            if (sw.ElapsedMilliseconds > TimeSpan.FromMinutes(1).TotalMilliseconds)
            {
                _metrics.AddCounter(MetricStrings.SOURCE_DownloadJob, MetricStrings.METRIC_LongTimeVerifyingExistingFiles);
            }

            return output;
        }

        public void CleanFailedFiles()
        {
            FuelLog.Info("Cleaning failed files");

            // remove all failed files
            foreach (var fileKv in _jobResult.LastResultByFileTgt)
            {
                if (fileKv.Value == null)
                {
                    FuelLog.Error("Null value for clean file key", new {fileKv.Key});
                }

                if (!fileKv.Value.IsSuccess)
                {
                    var fileName = fileKv.Value.File.Dest;
                    FuelLog.Info("Removing failed download file", new { fileName, resultWas = fileKv.Value.Result });

                    _fileVerificationSHACache.Remove(fileName);
                    try
                    {
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }
                    }
                    catch (Exception e)
                    {
                        FuelLog.Error(e, "Couldn't remove file", new { fileName });
                    }
                }
            }
        }

        /// <summary>
        /// Removes any files that do not exist in the manifest but are on the disk.
        /// Files that were removed from the manifest this way are removed from disk.
        /// </summary>
        public int CleanUnknownFiles()
        {
            Log.Information($"DownloadJob.RemoveUnknownFiles()");
            if (string.IsNullOrEmpty(InstallDirectory) || _previousManifest == null)
            {
                Log.Warning("No files removed. Install directory or previous manifest is null.",
                    new
                    {
                        InstallDirectory,
                        _previousManifest
                    });
                return 0;
            }

            int count = 0;
            var filePaths = _previousManifest.Packages[0].Files.Select(x => x.Path);

            foreach (var filePath in filePaths)
            {
                if (!_jobResult.ManifestWithVersion.ManifestData.ContainsFile(filePath))
                {
                    try
                    {
                        File.Delete(Path.Combine(InstallDirectory, filePath));
                        count++;
                    }
                    catch (IOException ex)
                    {
                        //TODO: Game is running que the delete for later?
                        Log.Warning($"[DWN] Could not remove file: '{filePath}', exception: '{ex.Message}'");
                    }
                }
            }

            FileUtilities.DeleteEmptyFolders(InstallDirectory);
            return count;
        }

        public bool JobHasDiskSpaceAvailable()
        {
            var neededSizeInBytes = _installUtil.GetRequiredDiskSpaceForInstall(IsUpdate, TotalSizeInBytes);
            var path = InstallDirectory;
            var availableSizeInBytes = _installUtil.AvailableDiskSpace(path);
            Log.Information("[DWN] Calculating disk requirements", new { path, neededSizeInBytes, availableSizeInBytes });
            return availableSizeInBytes > neededSizeInBytes;
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing) return;

            _getPatchBatcher?.Dispose();
            _getPatchBatcher = null;

            _fileVerificationSHACache.Dispose();
            _fileVerificationSHACache = null;

            _fileVerificationSHACacheDb?.Dispose();
            _fileVerificationSHACacheDb = null;
        }

        private string GetDownloadPath(IDownloadJobQueuedFile downloadJobQueuedFile, DownloadPatchInfo patchInfo, string targetFilePath)
        {
            switch (patchInfo.DeltaEncoding)
            {
                case DeltaEncoding.NONE:
                    return targetFilePath;
                case DeltaEncoding.FUEL_PATCH:
                    // We need to keep paths below the Windows 260 character limit. So we are use an index based name
                    return Path.Combine(WorkDir, downloadJobQueuedFile.ManifestFileIndex + ".patch");
                default:
                    throw new Exception($"Unknown patch type! {patchInfo.DeltaEncoding}");
            }
        }
    }
}
