﻿using System;
using System.IO;
using System.Linq;
using Serilog;

namespace Amazon.Fuel.Plugin.Download
{
    public interface IInstallUtil
    {
        /// <summary>
        /// Calculates the required amount of free disk space needed for the download
        /// </summary>
        /// <param name="isUpdate">Is the installation a full install or an update?</param>
        /// <param name="downloadSize">The size of the download</param>
        /// <returns></returns>
        long GetRequiredDiskSpaceForInstall(bool isUpdate, long downloadSize);

        /// <summary>
        /// Calculates the available disk space on the drive for the given path.
        /// returns -1 if the drive could not be determined.
        /// </summary>
        /// <param name="path">A file which has a defined root directory</param>
        /// <returns></returns>
        long AvailableDiskSpace(string path);
    }
    public class InstallUtil : IInstallUtil
    {
        private const double DiskSpaceForInstallCoefficient = 1.5;
        private static IInstallUtil _instance;

        private InstallUtil() { }

        public static IInstallUtil Instance => _instance ?? (_instance = new InstallUtil());

        public long GetRequiredDiskSpaceForInstall(bool isUpdate, long downloadSize)
        {
            if (isUpdate)
            {
                return (long)Math.Ceiling(downloadSize * DiskSpaceForInstallCoefficient);
            }
            return downloadSize;
        }

        public long AvailableDiskSpace(string path)
        {
            var driveInfo = DriveInfo.GetDrives()
                .FirstOrDefault(drive => drive.RootDirectory.Name.Equals(Path.GetPathRoot(path),
                    StringComparison.CurrentCultureIgnoreCase));
            if (driveInfo != null) return driveInfo.AvailableFreeSpace;
            
            //This shouldn't happen...
            Log.Error("[InstallUtil] Unable to locate drive information in order to compute available disk space", new { path });
            return -1;
        }
    }
}
