﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Messages;
using Amazon.Fuel.Plugin.Download.Models;
using Serilog;
using Sds = Tv.Twitch.Fuel.Sds;
using Amazon.Fuel.SoftwareDistributionService.Client;
using Polly;
using System.Diagnostics;
using System.Linq;
using Amazon.Fuel.Plugin.Persistence;

namespace Amazon.Fuel.Plugin.Download
{
    public class DownloadJobManager : IDownloadJobManager
    {
        public IMessengerHub Hub { get; }
        public IEntitlementPlugin Entitlement { get; }
        public IMetricsPlugin Metrics { get; }

        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly ITwitchTokenProvider _twitchTokenProvider;
        private readonly IInstallUtil _installUtil;
        private readonly IDownloadJobFactory _downloadJobFactory;
        private readonly IOSDriver _osDriver;

        public DownloadJobManager(
            IEntitlementPlugin entitlementPlugin, 
            IMessengerHub hub, 
            IMetricsPlugin metrics,
            ISoftwareDistributionServiceClient sds,
            ITwitchTokenProvider twitchTokenProvider,
            IInstallUtil installUtil,
            IOSDriver osDriver,
            IDownloadJobFactory downloadJobFactory)
        {
            Hub = hub;
            Metrics = metrics;
            Entitlement = entitlementPlugin;
            _twitchTokenProvider = twitchTokenProvider;
            _sds = sds;
            _installUtil = installUtil;
            _osDriver = osDriver;
            _downloadJobFactory = downloadJobFactory;

            Hub.Subscribe<FuelShutdownMessage>(m => { FileHashVerifier.Instance.Shutdown(); });
        }

        public async Task<DownloadProductResult> DownloadWithRetryAsync(
            bool isUpdate,
            int[] retryWaitSec,
            string entitlementId,
            ProductId productId,
            string folder,
            IMetricsPlugin metrics,
            IMessengerHub hub,
            Sds.Manifest previousManifest = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            FuelLog.Info("Start download job", new { productId, folder});

            var downloadSource = EDownloadSource.Primary;

            var sessionResult = await Policy
                .HandleResult<DownloadJobResult>(s => s.WantRetry)
                .RetryAsync(retryWaitSec.Length, onRetryAsync: async (e, i, o) =>
                {
                    var waitDelay = retryWaitSec[Math.Min(retryWaitSec.Length - 1, i - 1)];
                    FuelLog.Warn("Job failed, retrying", new { downloadSource, retryAttempt = i, of = retryWaitSec.Length, waitDelay });
                    downloadSource = downloadSource != EDownloadSource.Primary ? EDownloadSource.Primary : EDownloadSource.Secondary;
                    await Task.Delay(waitDelay * 1000, cancellationToken);
                }) 
                .ExecuteAndCaptureAsync(async ct => await DownloadJobAsync(isUpdate, entitlementId, productId, folder, hub, previousManifest, downloadSource, ct), cancellationToken);

            if (cancellationToken.IsCancellationRequested
                || sessionResult.Result?.Result == EDownloadJobResult.Cancel)
            {
                FuelLog.Info("Download job cancelled", new { productId });
                return new DownloadProductResult() {Result = EDownloadJobResult.Cancel};
            }

            if (sessionResult.FinalException != null)
            {
                FuelLog.Error(sessionResult.FinalException, "Download job failed with exception", new { productId });
                return new DownloadProductResult() { Result = EDownloadJobResult.GeneralFailure };
            }

            if (sessionResult.Result == null || sessionResult.Result.Result != EDownloadJobResult.Success)
            {
                FuelLog.Error("Download job failed", new { productId });
            }

            return new DownloadProductResult()
            {
                Result = sessionResult.Result?.Result ?? EDownloadJobResult.GeneralFailure,
                ManifestWithVersion = sessionResult.Result?.ManifestWithVersion
            };
        }

        /// <summary>
        /// Get manifest and download all files
        /// </summary>
        private async Task<DownloadJobResult> DownloadJobAsync(
            bool isUpdate,
            string entitlementId,
            ProductId productId,
            string folder,
            IMessengerHub hub,
            Sds.Manifest previousManifest = null,
            EDownloadSource downloadSource = EDownloadSource.Primary,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var job = _downloadJobFactory.CreateJob(
                    _sds,
                    _twitchTokenProvider,
                    this,
                    _installUtil,
                    isUpdate,
                    entitlementId,
                    productId,
                    folder,
                    Metrics,
                    _osDriver,
                    hub,
                    previousManifest,
                    downloadSource,
                    cancellationToken))
            {
                Hub.Publish<MessageDownloadStarting>(new MessageDownloadStarting(this));

                return await job.RunJobAsync();
            }
        }
    }
}
