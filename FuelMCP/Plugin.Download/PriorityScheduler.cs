﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amazon.Fuel.Plugin.Download
{
    internal class PriorityScheduler : TaskScheduler
    {
        private readonly ThreadPriority _priority;
        private Thread[] _threads;
        private readonly BlockingCollection<Task> _tasks = new BlockingCollection<Task>(new ConcurrentQueue<Task>());

        public override int MaximumConcurrencyLevel { get; } = Math.Max(1, Environment.ProcessorCount * 2);

        public PriorityScheduler(ThreadPriority priority)
        {
            _priority = priority;
        }

        protected override void QueueTask(Task task)
        {
            _tasks.Add(task);

            if (_threads == null)
            {
                _threads = new Thread[MaximumConcurrencyLevel];
                for (var i = 0; i < _threads.Length; i++)
                {
                    _threads[i] = new Thread(() =>
                    {
                        foreach (var t in _tasks.GetConsumingEnumerable())
                        {
                            base.TryExecuteTask(t);
                        }
                    })
                    {
                        Name = "Priority Scheduler: " + i,
                        Priority = _priority,
                        IsBackground = false
                    };
                    _threads[i].Start();
                }
            }
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            return false;
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return _tasks;
        }
    }
}
