﻿using System.Threading;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Download.Models;
using Amazon.Fuel.SoftwareDistributionService.Client;

namespace Amazon.Fuel.Plugin.Download
{
    public interface IDownloadJobFactory
    {
        IDownloadJob CreateJob(
            ISoftwareDistributionServiceClient sds,
            ITwitchTokenProvider twitchTokenProvider,
            IDownloadJobManager jobManager,
            IInstallUtil installUtil,
            bool isUpdate,
            string entitlementId,
            ProductId productId,
            string folder,
            IMetricsPlugin metrics,
            IOSDriver osDriver,
            IMessengerHub hub,
            Tv.Twitch.Fuel.Sds.Manifest previousManifest = null,
            EDownloadSource downloadSource = EDownloadSource.Primary,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}