using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Exceptions;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Download.Interfaces;
using Amazon.Fuel.Plugin.Download.Models;
using Amazon.Fuel.SoftwareDistributionService.Client;
using Amazon.Fuel.SoftwareDistributionService.Model;
using Google.Protobuf;
using Serilog;
using HashAlgorithm = Tv.Twitch.Fuel.Sds.HashAlgorithm;

namespace Amazon.Fuel.Plugin.Download
{
    public class GetPatchBatcher : IDisposable
    {
        private readonly ISoftwareDistributionServiceClient _sds;
        private readonly ProductId _adgProductId;
        private readonly string _versionId;
        private readonly string _entitlementId;
        private readonly IList<IDownloadJobQueuedFile> _files;
        private readonly ITwitchTokenProvider _twitchTokenProvider;
        private readonly Dictionary<IDownloadJobQueuedFile, DownloadPatchInfo> _filesToPatches
            = new Dictionary<IDownloadJobQueuedFile, DownloadPatchInfo>();
        private readonly object _lock = new { };

        private readonly IMetricsPlugin _metrics;

        public event Action AuthFailureEvent = delegate {};

        private readonly CancellationToken _abortToken;
        private readonly CancellationTokenSource _abortTokenSource = new CancellationTokenSource();

        private bool _isStarted = false;

        public event Action<IDownloadJobQueuedFile> PatchInfoDownloadFailed = delegate { };

        public bool IsAborted => _abortToken.IsCancellationRequested;

        private class WaitingCountConfig
        {
            public int MinWaiting { get; set; }
            public int BatchSize { get; set; }
        }

        private readonly WaitingCountConfig[] _minFilledQueueSizeToBatchSize = {
            new WaitingCountConfig { MinWaiting = 0, BatchSize = 25 },         // if 0-4 files have filled download data in the queue, grab five patches
            new WaitingCountConfig { MinWaiting = 25, BatchSize = 50 },
            new WaitingCountConfig { MinWaiting = 50, BatchSize = 100 },
            new WaitingCountConfig { MinWaiting = 100, BatchSize = 0 },       // wait after this count 
        };

        public GetPatchBatcher(
            IList<IDownloadJobQueuedFile> files,
            ISoftwareDistributionServiceClient sds, 
            ITwitchTokenProvider twitchTokenProvider,
            IMetricsPlugin metrics,
            ProductId adgProductId,
            string versionId,
            string entitlementId,
            CancellationToken cancelToken = default(CancellationToken))
        {
            _files = files;
            _sds = sds;
            _adgProductId = adgProductId;
            _metrics = metrics;
            _twitchTokenProvider = twitchTokenProvider;
            _versionId = versionId;
            _entitlementId = entitlementId;
            _abortToken = CancellationTokenSource.CreateLinkedTokenSource(cancelToken, _abortTokenSource.Token).Token;
        }

        public void Start()
        {
            lock (_lock)
            {
                if (!_isStarted)
                {
                    _isStarted = true;
                    Task.Run(() =>
                    {
                        try
                        {
                            FulfillAllIInQueue();
                        }
                        catch (Exception ex)
                        {
                            FuelLog.Error(ex, "Error fulfilling queue in GetPatchBatcher");
                            _metrics.AddCounter("GetPatchBatcher", "AbortOnException");
                            _abortTokenSource.Cancel();
                        }
                    }, _abortToken);
                }
            }
        }

        private void FulfillAllIInQueue()
        {
#if DEBUG
            Thread.CurrentThread.Name = "GetPatchBatcher";
#endif

            int timeSpentGettingPatches = 0;
            // preserve order

            var batchSize = 0;
            for (int currentIdx = 0; currentIdx < _files.Count
                                        && !_abortToken.IsCancellationRequested ; currentIdx += batchSize)
            {
                var waitingPatchCount = _filesToPatches.Count;
                batchSize = CalcBatchSize(waitingPatchCount);

                if (batchSize == 0)
                {
                    Thread.Sleep(250);
                    continue;
                }

                var sw = Stopwatch.StartNew();

                FuelLog.Debug("Retrieving GetPatch batch", new { waitingPatchCount, batchSize });

                // grab the batch size worth of files
                var retrievePatchFiles = _files.Skip(currentIdx).Take(batchSize).ToList();
                var retrievePatches = retrievePatchFiles.Select(f =>
                    new DownloadPatchInfo()
                    {
                        PatchHashPair = new HashPair()
                        {
                            sourceHash = f.SourceHash,
                            targetHash = f.TargetHash
                        }
                    }
                );

                // Get download sources.
                var getPatchesResponse = RetryableGetPatchesRequest(retrievePatches.Select(f => f.PatchHashPair));

                if (_abortToken.IsCancellationRequested)
                {
                    break;
                }

                // update all retrieved patches
                lock (_lock)
                {
                    var hashesToPatch = new Dictionary<string, Patch>(StringComparer.OrdinalIgnoreCase);
                    foreach (var patch in getPatchesResponse.patches)
                    {
                        if (!hashesToPatch.ContainsKey(HashesToKey(patch.sourceHash, patch.targetHash)))
                        {
                            hashesToPatch.Add(HashesToKey(patch.sourceHash, patch.targetHash), patch);
                        }
                    }

                    using (var patchInfos = retrievePatches.GetEnumerator())
                    using (var patchInfoFiles = retrievePatchFiles.GetEnumerator())
                    {
                        while (patchInfos.MoveNext() && patchInfoFiles.MoveNext())
                        {
                            var targetHash = patchInfos.Current?.PatchHashPair.targetHash;
                            var sourceHash = patchInfos.Current?.PatchHashPair.sourceHash;

                            if (targetHash == null)
                            {
                                Log.Error("target hash was null? This should never happen.", new { sourceHash, targetHash });
                                PatchInfoDownloadFailed(patchInfoFiles.Current);
                                return;
                            }

                            Patch patch;
                            var patchFound =
                                hashesToPatch.TryGetValue(HashesToKey(sourceHash, targetHash), out patch);

                            //Find the patch in the response which matches the file we requested a patch for
                            if (patchFound && patch != null)
                            {
                                var sources = patch.downloadUrls;
                                if (sources == null || sources.Count < 1)
                                {
                                    Log.Error("Patch returned with no download sources. This should never happen!", new { sourceHash, targetHash });
                                    PatchInfoDownloadFailed(patchInfoFiles.Current);
                                    return;
                                }

                                patchInfos.Current.PrimarySource = sources.FirstOrDefault();
                                // We might support > 2 sources someday, but right now it's only S3 and Cloudfront
                                patchInfos.Current.SecondarySource = sources.Count > 1 ? sources[1] : null;

                                if (patchInfos.Current.PrimarySource == null)
                                {
                                    Log.Error("Download source was null. This should never happen!", new { sourceHash, targetHash });
                                    PatchInfoDownloadFailed(patchInfoFiles.Current);
                                    return;
                                }

                                patchInfos.Current.DeltaEncoding = patch.type;
                                patchInfos.Current.FileLength = patch.size;
                                patchInfos.Current.Hash = patch.patchHash.value;

                                _filesToPatches[patchInfoFiles.Current] = patchInfos.Current;
                            }
                            else
                            {
                                Log.Error("Patch response did not include the requested source", new { sourceHash , targetHash });
                                PatchInfoDownloadFailed(patchInfoFiles.Current);
                                return;
                            }
                        }
                    }
                }

                // Don't need to use interlocked because this is single threaded.
                timeSpentGettingPatches += (int)sw.ElapsedMilliseconds;
            }

            FuelLog.Info("Time spent getting patches was " + TimeSpan.FromMilliseconds(timeSpentGettingPatches).ToString("G"));
            if (_files.Count > 0)
            {
                _metrics.AddTimerValue(MetricStrings.SOURCE_GetPatchBatcher, MetricStrings.METRIC_NormalizedTime,
                    (double) timeSpentGettingPatches/ _files.Count);
            }
        }

        public async Task<DownloadPatchInfo> ConsumePatchByFileAsync(IDownloadJobQueuedFile file)
        {
            if (!_isStarted)
            {
                Start();
            }

            while (true)
            {
                lock (_lock)
                {
                    DownloadPatchInfo ret;
                    if (_filesToPatches.TryGetValue(file, out ret))
                    {
                        _filesToPatches.Remove(file);
                        return ret;
                    }
                }

                await Task.Delay(250, _abortToken);

                if (_abortToken.IsCancellationRequested)
                {
                    return null;
                }
            }
        }

        private string HashesToKey(Hash source, Hash target)
        {
            return string.Join(":", source?.value ?? "", target?.value ?? "");
        }

        private int CalcBatchSize(int patchInfosAvailable)
        {
            // map to largest waiting count to determine batch size
            for (int i = _minFilledQueueSizeToBatchSize.Length - 1; i >= 0; --i)
            {
                if (i == 0 || patchInfosAvailable >= _minFilledQueueSizeToBatchSize[i].MinWaiting)
                {
                    return _minFilledQueueSizeToBatchSize[i].BatchSize;
                }
            }

            // this should never happen
            Debug.Fail("empty pool config");
            return 10;
        }

        private GetPatchesResponse DoGetPatchesRequest(IEnumerable<HashPair> fileHashes, CancellationToken cancelToken)
        {
            return _sds.GetPatches(new GetPatchesRequest(
                                        versionId: _versionId,
                                        fileHashes: fileHashes,
                                        deltaEncodings: new[] { DeltaEncoding.NONE, DeltaEncoding.FUEL_PATCH },
                                        adgGoodId: _entitlementId),
                                    cancelToken).GetAwaiter().GetResult();
        }

        private GetPatchesResponse RetryableGetPatchesRequest(IEnumerable<HashPair> fileHashes)
        {
            try
            {
                return DownloadPolicyLibrary.NonAuthExceptionPolicy.Execute(
                    ctB => DownloadPolicyLibrary.NetworkOutagePolicy.Execute(
                        ctC => DoGetPatchesRequest(fileHashes, _abortToken), 
                        _abortToken),
                    _abortToken);
            }
            catch (Exception e)
            {
                if ( e is TwitchAuthException )
                {
                    AuthFailureEvent?.Invoke();
                }

                if ( _abortToken.IsCancellationRequested )
                {
                    return null;
                }

                Log.Warning(e, "GetPatcheBatcher exception, aborting");
                _abortTokenSource.Cancel();
                throw e;
            }
        }

        public void Dispose()
        {
            _abortTokenSource.Cancel();
        }
    }
}