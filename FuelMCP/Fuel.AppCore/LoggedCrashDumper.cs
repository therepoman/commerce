﻿using System;
using Serilog;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using System.Threading.Tasks;
using static Amazon.Fuel.Common.Constants;
using Amazon.Fuel.Common.Logging.Events;
using System.Diagnostics;

namespace Amazon.Fuel.AppCore
{
    public delegate void OnCurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e);
    public delegate void OnTaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs);
    public delegate void OnExternalUnhandledException(object sender, Exception ex);

    public class UncaughtExceptionHandler : IUncaughtExceptionHandler
    {
        public event OnCurrentDomainOnUnhandledException CurrentDomainUnhandledException = delegate { };
        public event OnTaskSchedulerOnUnobservedTaskException UnobservedTaskException = delegate { };
        public event OnExternalUnhandledException ExternalUnhandledException = delegate { };

        private readonly IMetricsPlugin _metrics;
        private readonly bool _uploadMetricsAndData = false;
        private bool _crashDetected;

        public UncaughtExceptionHandler(IMetricsPlugin metrics, bool attachExceptionTrackers, bool uploadMetricsAndData) 
        {
            _metrics = metrics;
            _crashDetected = false;
            _uploadMetricsAndData = uploadMetricsAndData;

            if (attachExceptionTrackers)
            {
                UnobservedTaskException += OnTaskSchedulerOnUnobservedTaskException;
                CurrentDomainUnhandledException += OnUnhandledException;
            }

            if (attachExceptionTrackers)
            {
                attachUncaughtExceptionTrackers();
            }
        }

        private void attachUncaughtExceptionTrackers()
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) => CurrentDomainUnhandledException?.Invoke(s, e);
            TaskScheduler.UnobservedTaskException += (s, a) => UnobservedTaskException?.Invoke(s, a);
        }

        public void InvokeExternalUnhandledException(object sender, Exception ex)
        {
            ExternalUnhandledException?.Invoke(sender, ex);
        }

        private void OnTaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs)
        {
            WriteCrashLog(CrashScope.App, CrashType.Fault, unobservedTaskExceptionEventArgs.Exception, "UnobservedTaskException");
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (_uploadMetricsAndData)
            {
                recordMetrics();
            }

            // This will write out a report that will get uploaded to the crash board
            WriteCrashLog(CrashScope.App, CrashType.Fault, (Exception)e.ExceptionObject, "UnhandledException");
            // This will add it to the general streaming log
            logCrash(sender, e);
        }

        private void recordMetrics()
        {
            _metrics.AddCounter(MetricStrings.SOURCE_Crash, MetricStrings.METRIC_UnhandledException);
            if (!_crashDetected)
            {
                _metrics.AddCounter(MetricStrings.SOURCE_Crash, MetricStrings.METRIC_Crash);
                _crashDetected = true;
            }
            _metrics.FlushQueue();
        }

        private void logCrash(object s, UnhandledExceptionEventArgs e)
        {
            var strErr = $"Unhandled Base Exception: sender '{s}' detail '{e.ExceptionObject}', isTerminating '{e.IsTerminating}'";
            if (e.ExceptionObject is Exception)
            {
                Log.Fatal((Exception)e.ExceptionObject, strErr);
            }
            else
            {
                Log.Fatal(strErr);
            }
            Log.CloseAndFlush();
#if DEBUG
            System.Diagnostics.Debug.Fail("Unhandled base exception encountered");
#endif
        }

        private void WriteCrashLog(CrashScope scope, CrashType type, Exception e, String source = null)
        {
            if (Debugger.IsAttached)
                Debugger.Break();

            Log.Write(CrashReportEvent.Create(scope, type, e, source));
        }
    }
}
