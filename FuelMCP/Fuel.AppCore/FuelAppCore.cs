﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Metrics;
using Autofac;
using Serilog;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Configuration;
using Amazon.Fuel.Plugin.Logger;
using Amazon.Fuel.Plugin.PlatformServices_Windows.Models;
using Amazon.Fuel.Common.Implementations;
using Clients.Twitch.Spade.Client;
using Amazon.Fuel.Plugin.SecureStorage;
using Amazon.Fuel.Configurations.Configuration;

namespace Amazon.Fuel.AppCore
{
    public class FuelAppCore : Singleton<FuelAppCore>, IStartablePostContainerBuild
    {
        private readonly IAppInfo _appInfo = new AppInfo();
        private HashSet<IMessageHubListener> _hubListeners = new HashSet<IMessageHubListener>();
        private ILoggerBaseConfig _loggerBaseConfig;
        private IAppConfigPlugin _appConfig;

        public IContainer Container { get; private set; } = null;

        public Exception TryStart(Curse.Logging.LogCategory curseLogger, Action<ContainerBuilder> preBuildStep, bool devMode, IOSDriver osDriver = null)
        {
            try
            {
                Start_CanThrow(curseLogger, preBuildStep, devMode, osDriver);
                return null;
            }
            catch (Exception e)
            {
                // let's be super cautious here ..
                try
                {
                    FuelLog.Fatal(e, "Couldn't initialize Fuel, Fuel will not function properly");
                }
                catch (Exception innerEx)
                {
                    // skip logging
                    return innerEx;
                }

                return e;
            }
        }

        private void Start_CanThrow(Curse.Logging.LogCategory curseLogger, Action<ContainerBuilder> preBuildStep, bool devMode, IOSDriver osDriver)
        {
            if (Container != null)
            {
                Log.Error("AppCore already started!");
                return;
            }

            osDriver = osDriver ?? new WindowsFileSystemDriver();
            osDriver.IsDevMode = devMode;

            var spadeClient = new SpadeClient(new SpadeHttpClient());
            IMetricsPlugin metrics = null;
            if (osDriver.IsDevMode)
            {
                metrics = new NullMetrics();
            }
            else
            {
                metrics = new MetricsPlugin(Constants.AmazonMetricsId, DateTime.UtcNow, spadeClient);
            }

            var crash = new UncaughtExceptionHandler(
                metrics: metrics,
                attachExceptionTrackers: true,
                uploadMetricsAndData: !devMode
            );
            var baseContextServices = new BaseContextServices();

            var builder = new ContainerBuilder();
            builder.RegisterInstance(this).As<IStartablePostContainerBuild>().SingleInstance();

            _loggerBaseConfig = new LoggerBaseConfig(osDriver, curseLogger, devMode: devMode);
            builder.RegisterInstance(osDriver).As<IOSDriver>().SingleInstance();
            builder.RegisterInstance(curseLogger).As<Curse.Logging.LogCategory>().SingleInstance();
            builder.RegisterInstance(_loggerBaseConfig).As<ILoggerBaseConfig>().SingleInstance();

            // now that logging is started, log any errors and continue
            Log.Information($"CurseApp/Fuel v{_appInfo.Version} started, DSN: '{DeviceInfo.DSN}'");

            // ensure that our system persistence root exists with all user permissions before we do anything
            osDriver.EnsureSystemDataPathExists();

            baseContextServices.Build(builder);

            new BaseContext(crash, metrics).Build(builder);

            new CoreContext().Build(builder);

            // unelevatedservice is injected by Radium as part of preBuildStep (else deps will fail on build)
            new WindowsContext().Build(builder, includeLaunchUnelevatedService: false, devMode: devMode);

            new StorageContext().Build(builder);

            new ClientsContext().Build(builder, Constants.TwitchAPI, Constants.ClientId, Constants.ClientTimeoutRetryDelaySecs);

            new PersistenceContext().Build(builder);

            try
            {
                FuelLog.Info("Constructing container, resolving dependencies");

                preBuildStep?.Invoke(builder);

                Container = builder.Build();

                FuelLog.Info("Container construction successful, running all IStartablePostContainerBuilds");

                foreach (var instance in Container.Resolve<IEnumerable<IStartablePostContainerBuild>>())
                {
                    Log.Information($"Starting component: {instance}");
                    instance.StartPostContainerBuild();
                }

                FuelLog.Info("Container init complete");

                Container.Resolve<IMessengerHub>().Subscribe<FuelShutdownMessage>((msg) =>
                {
                    FuelLog.Info("Fuel shutdown message received. Flushing metrics.");
                    Container?.Resolve<IMetricsPlugin>()?.FlushQueue();
                });

                metrics.AddCounter(MetricStrings.SOURCE_Version, _appInfo.Version);
                metrics.AddDiscreteValue("FuelInit", "InitSuccess", 1);
            }
            catch (Exception ex) // some issues can occur here from the IStartablePostContainerBuild plugins
            {
                // logging should be available (even if autofac failed), let's log the error
                FuelLog.Fatal(ex, "Exception hit during Autofac", new { containerBuilt = Container != null });
                metrics.AddDiscreteValue("FuelInit", "InitSuccess", 0);
                throw;
            }
        }

        public void AddHubListener(IMessageHubListener hubListener)
        {
            Debug.Assert(Container != null, "Call start first");
            if (_hubListeners.Contains(hubListener))
            {
                FuelLog.Warn("Double hub attach detected", new {hubIsNull = hubListener == null});
                return;
            }

            _hubListeners.Add(hubListener);
            hubListener.AttachToHub(Container.Resolve<IMessengerHub>());
        }

        public void StartPostContainerBuild()
        {
            // migrate Lucene->Sqlite, if necessary
            Container.Resolve<ILuceneDbMigrator>().TryEnsureDbsMigrated();

            // check for uninstaller update
            Task.Run(() =>
            {
                var success = Container.Resolve<IUninstallPlugin>().TryEnsureUninstallerExeInCommonLocation();
                Container.Resolve<IMetricsPlugin>().AddDiscreteValue("FuelAppCore", "UninstallerUpdateSuccess", success ? 1 : 0);
            });

            // start a task to remove any lingering deletion files
            Container.Resolve<IDeferredDeletionService>().StartMoveAndDeleteDeferredPathsAsTask();
        }
    }
}