﻿using System;
using System.Collections.Generic;
using Amazon.Fuel.Coral.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

//https://code.amazon.com/packages/SoftwareDistributionServiceModel/blobs/mainline/--/model/operations.xml
namespace Amazon.Fuel.SoftwareDistributionService.Model
{
    public sealed class GetPatchesRequest : CoralRequest
    {
        public override string Operation => "GetPatches";

        public GetPatchesRequest(string versionId, IEnumerable<HashPair> fileHashes, IEnumerable<DeltaEncoding> deltaEncodings, string adgGoodId)
        {
            this.versionId = versionId;
            this.fileHashes = fileHashes;
            this.deltaEncodings = deltaEncodings;
            this.adgGoodId = adgGoodId;
        }
        public string versionId { get; set; }
        public IEnumerable<HashPair> fileHashes { get; set; }
        public IEnumerable<DeltaEncoding> deltaEncodings { get; set; }
        public string adgGoodId { get; set; }
    }

    public sealed class GetPatchesResponse : CoralResponse
    {
        public IList<Patch> patches { get; set; }
    }
    public sealed class HashPair
    {
        public Hash sourceHash { get; set; }
        public Hash targetHash { get; set; }
    }

    public sealed class Patch
    {
        public Hash patchHash { get; set; }
        public Hash sourceHash { get; set; }
        public Hash targetHash { get; set; }
        public DeltaEncoding type { get; set; }
        public IList<string> downloadUrls { get; set; }
        public long offset { get; set; }
        public long size { get; set; }
    }

    public sealed class Hash
    {
        // value is a keyword, not sure if good idea
        public string value { get; set; }
        public HashAlgorithm algorithm { get; set; }

        public static bool Equals(Hash a, Hash b)
        {
            return string.Equals(a?.value, b?.value) &&
                   a?.algorithm == b?.algorithm;
        }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum DeltaEncoding
    {
        FUEL_PATCH,
        NONE
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum HashAlgorithm
    {
        SHA256
    }
}
