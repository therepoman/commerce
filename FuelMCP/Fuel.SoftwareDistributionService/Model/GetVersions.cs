﻿using System.Collections.Generic;
using Amazon.Fuel.Coral.Model;

//https://code.amazon.com/packages/SoftwareDistributionServiceModel/trees/mainline/--/model
namespace Amazon.Fuel.SoftwareDistributionService.Model
{
    public sealed class GetVersionsRequest : CoralRequest
    {
        public GetVersionsRequest(IList<string> adgProductIds)
        {
            this.adgProductIds = adgProductIds;
        }

        public IList<string> adgProductIds { get; set; }
        public override string Operation => "GetVersions";
    }

    public sealed class GetVersionsResult : CoralResponse
    {
        public IList<Version> versions { get; set; }
        public IList<GetVersionError> errors { get; set; }
    }

    public sealed class GetVersionError
    {
        public GetVersionErrorType errorType { get; set; }
        public string message { get; set; }
        public string adgProductId { get; set; }
    }

    public enum GetVersionErrorType
    {
        ADG_PRODUCT_NOT_FOUND
    }

    public sealed class Version
    {
        public string versionId { get; set; }
        public string adgProductId { get; set; }
    }
}
