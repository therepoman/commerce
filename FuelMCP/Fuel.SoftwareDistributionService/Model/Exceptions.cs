﻿using Amazon.Fuel.Coral.Model;

//https://code.amazon.com/packages/SoftwareDistributionServiceModel/blobs/mainline/--/model/operations.xml
namespace Amazon.Fuel.SoftwareDistributionService.Model
{
    public class InternalFailure : CoralException
    {
        public override string __type { get; } = "com.amazon.coral.service#InternalFailure";
        public InternalFailure(string message) : base(message) { }
    }

    public class ForbiddenException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.service.model#ForbiddenException";
        public ForbiddenException(string message) : base(message) { }
    }
    public class NotFoundException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.service.model#NotFoundException";
        public NotFoundException(string message) : base(message) { }
    }
    public class ChannelVersionOutOfDateException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.service.model#ChannelVersionOutOfDateException";
        public ChannelVersionOutOfDateException(string message) : base(message) { }
    }
    public class NotAuthorizedException : CoralException
    {
        public override string __type { get; } = "com.amazonaws.gearbox.softwaredistribution.service.model#NotAuthorizedException";
        public NotAuthorizedException(string message) : base(message) { }
    }

    public class NotAuthorizedGetVersionException : CoralException
    {
        public override string __type { get; } = "com.amazon.coral.service#NotAuthorizedException";
        public NotAuthorizedGetVersionException(string message) : base(message) { }
    }
}
