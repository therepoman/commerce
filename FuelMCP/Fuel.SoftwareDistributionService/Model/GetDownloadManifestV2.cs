﻿using System.Collections.Generic;
using Amazon.Fuel.Coral.Model;

//https://code.amazon.com/packages/SoftwareDistributionServiceModel/blobs/mainline/--/model/operations.xml
namespace Amazon.Fuel.SoftwareDistributionService.Model
{
    public sealed class GetDownloadManifestV2Request : CoralRequest
    {
        public GetDownloadManifestV2Request(string adgGoodId)
        {
            this.adgGoodId = adgGoodId;
        }
        public string adgGoodId { get; set; }
        public override string Operation => "GetDownloadManifestV2";
    }

    public sealed class GetDownloadManifestV2Response : CoralResponse
    {
        public string versionId { get; set; }
        public IList<string> downloadUrls { get; set; }
    }
}
