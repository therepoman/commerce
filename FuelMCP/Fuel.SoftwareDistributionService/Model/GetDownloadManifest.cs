﻿using Amazon.Fuel.Coral.Model;

namespace Amazon.Fuel.SoftwareDistributionService.Model
{
    public sealed class GetDownloadManifestRequest : CoralRequest
    {
        public GetDownloadManifestRequest(string entitlementId)
        {
            this.entitlementId = entitlementId;
        }

        public string entitlementId { get; set; }

        public override string Operation => "GetDownloadManifest";
    }

    public sealed class GetDownloadManifestResult : CoralResponse
    {
        public string channelId { get; set; }
        public string versionId { get; set; }
        public string manifest { get; set; }

        public GetDownloadManifestResult(string channelId, string versionId, string manifest)
        {
            this.channelId = channelId;
            this.versionId = versionId;
            this.manifest = manifest;
        }
    }
}
