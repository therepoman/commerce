﻿//https://code.amazon.com/packages/SoftwareDistributionServiceModel/trees/mainline/--/model

using System;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Coral.Client;
using Amazon.Fuel.SoftwareDistributionService.Model;
using System.Threading;
using Amazon.Fuel.Common.Utilities;
using Polly;

namespace Amazon.Fuel.SoftwareDistributionService.Client
{
    public interface ISoftwareDistributionServiceClient
    {
        Task<GetVersionsResult> GetVersions(GetVersionsRequest request, CancellationToken cancellationToken = default(CancellationToken));
        Task<GetDownloadManifestResult> GetDownloadManifest(GetDownloadManifestRequest request, CancellationToken cancellationToken = default(CancellationToken));
        Task<GetDownloadManifestV2Response> GetDownloadManifestV2(GetDownloadManifestV2Request request, CancellationToken cancellationToken = default(CancellationToken));
        Task<GetPatchesResponse> GetPatches(GetPatchesRequest request, CancellationToken cancellationToken = default(CancellationToken));
    }
    public class SoftwareDistributionServiceClient : ISoftwareDistributionServiceClient
    {
        public const string ProdUrl = "https://sds.amazon.com";
        public const string CoralNamespace = "com.amazonaws.gearbox.softwaredistribution.service.model.SoftwareDistributionService";

        private readonly ICoralClient _client;
        private readonly ITwitchTokenProvider _twitchTokenProvider;

        public SoftwareDistributionServiceClient(
            string serviceUrl, 
            string useragent, 
            CoralClientFactory coralClientFactory, 
            IMetricsPlugin metrics, 
            ITwitchTokenProvider twitchTokenProvider)
        {
            _twitchTokenProvider = twitchTokenProvider;
            _client = coralClientFactory.CreateTwitchAuthenticatedClient(serviceUrl,
                CoralNamespace,
                () => _twitchTokenProvider.GetAndValidateAccessToken(),
                useragent, metrics);
        }

        public SoftwareDistributionServiceClient(
            string serviceUrl, 
            string useragent, 
            string oauthToken, 
            IMetricsPlugin metrics, 
            ITwitchTokenProvider login, 
            CoralClientFactory coralClientFactory = null)
        {
            _twitchTokenProvider = login;
            _client = (coralClientFactory ?? new CoralClientFactory()).CreateTwitchAuthenticatedClient(serviceUrl,
                CoralNamespace,
                () => oauthToken,
                useragent, metrics);
        }

        public async Task<GetDownloadManifestResult> GetDownloadManifest(GetDownloadManifestRequest request, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _client.SendRequest<GetDownloadManifestResult>(request, cancellationToken);
        }

        public async Task<GetVersionsResult> GetVersions(GetVersionsRequest request, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _client.SendRequest<GetVersionsResult>(request, cancellationToken);
        }

        public async Task<GetDownloadManifestV2Response> GetDownloadManifestV2(GetDownloadManifestV2Request request, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _client.SendRequest<GetDownloadManifestV2Response>(request, cancellationToken);
        }

        public async Task<GetPatchesResponse> GetPatches(GetPatchesRequest request, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _client.SendRequest<GetPatchesResponse>(request, cancellationToken);
        }
    }
}
