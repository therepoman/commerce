using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Plugin.Persistence
{
    public partial class GameInstallInfoDatabase : SqliteDatabase<GameInstallInfo, GameInstallInfoDbContext>, IGameInstallInfoDatabase
    {
        public GameInstallInfoDatabase(
            IPersistence persistence, 
            IMessengerHub msg, 
            IMetricsPlugin metrics,
            IOSDriver osDriver)
            : base(persistence, msg, metrics, osDriver)
        {
            // under some circumstances this occurs, just warn if we're not elevated
            CheckAllUserPermissions(errOnFailure: false);
        }

        private void CheckAllUserPermissions(bool errOnFailure)
        {
            try
            {
                // creator should have full access in most cases, grant to all users
                _osDriver.EnsureDirectoryHasAllUsersPermissions(_persistence.DataRoot());
            }
            catch (Exception ex)
            {
                if ( errOnFailure )
                    FuelLog.Error(ex, "Couldn't set all-user permissions on directory", new {perm=_osDriver.DescribePermissions(_persistence.DataRoot()) });
                else
                    FuelLog.Warn(ex, "Couldn't set all-user permissions on directory", new { perm = _osDriver.DescribePermissions(_persistence.DataRoot()) });
            }
        }

        public override List<GameInstallInfo> All => base.All.Where(i => i.Installed).ToList();
        public List<GameInstallInfo> AwaitingCleanup => base.All.Where(i => !i.Installed).ToList();

        public string GetSqlDbFile() => SqliteDatabase<GameInstallInfo, GameInstallInfoDbContext>.GetSqlDbFile(_persistence);

        public override void DbWriteAction(
            Action<GameInstallInfoDbContext> lambda, 
            bool commitChanges,
            bool skipIfDbNotExist = false)
        {
            if (commitChanges)
            {
                // try to ensure all user access before continuing with a write 
                //  (if we're writing to the gameinstallinfo DB, we should be elevated)
                // keep going even if this fails, our local user may still have permission
                CheckAllUserPermissions(errOnFailure: true);
            }

            base.DbWriteAction(lambda, commitChanges, skipIfDbNotExist);
        }
    }
}