﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tv.Twitch.Fuel.Sds;

namespace Tv.Twitch.Fuel.Manifest
{
    public interface IManifestComparator
    {
        ManifestComparison Compare(Sds.Manifest oldManifest, Sds.Manifest newManifest);
    }

    /// <summary>
    /// Used to compare Manifests, this is useful during updates
    /// </summary>
    public class ManifestComparator : IManifestComparator
    {
        public ManifestComparison Compare(Sds.Manifest oldManifest, Sds.Manifest newManifest)
        {
            ManifestComparison comparison = new ManifestComparison();

            // Today we only support a single package
            Package oldPackage = oldManifest.Packages[0];
            Package newPackage = newManifest.Packages[0];

            IDictionary<string, File> oldFilesByPath = oldPackage.Files.ToDictionary(f => f.Path);
            IDictionary<string, File> newFilesByPath = newPackage.Files.ToDictionary(f => f.Path);

            comparison.New = newFilesByPath
                .Where(e => !oldFilesByPath.ContainsKey(e.Key))
                .Select(e => e.Value);
            comparison.Removed = oldFilesByPath
                .Where(e => !newFilesByPath.ContainsKey(e.Key))
                .Select(e => e.Value);
            comparison.Updated = oldFilesByPath
                .Where(e => newFilesByPath.ContainsKey(e.Key) && !e.Value.Hash.Value.Equals(newFilesByPath[e.Key].Hash.Value))
                .Select(e => e.Value);
                
            return comparison;
        }
    }

    public sealed class ManifestComparison
    {
        public IEnumerable<File> New { get; set; }
        public IEnumerable<File> Removed { get; set; }
        public IEnumerable<File> Updated { get; set; }
    }
}
