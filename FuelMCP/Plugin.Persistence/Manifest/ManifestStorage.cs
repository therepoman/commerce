﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Utilities;
using Sds = Tv.Twitch.Fuel.Sds;
using Tv.Twitch.Fuel.Manifest;

namespace Amazon.Fuel.Plugin.Persistence
{
    /// <summary>
    /// A class for storing SDS Manifests to disk, and reading them back.
    /// 
    /// ***This class is not thread safe***
    /// 
    /// This shouldn't be an issue as the manifests are accessed by SDS version UUIDs,
    /// and only need to be written once (near the start of an install/update) flow.
    /// The manifests can be considered constant for a version and do not change once
    /// a version is ingested.
    /// </summary>
    public class ManifestStorage : IManifestStorage
    {
        private const string METRIC_SOURCE = "ManifestStorage";

        private const string FS_EXTENSION = "manifest";

        private readonly IPersistence _persistence;
        private string Root => _persistence.DataRoot();
        private readonly IMetricsPlugin _metrics;
        private readonly ManifestSerialization _serializer;

        public ManifestStorage(IPersistence persistence, IMetricsPlugin metrics)
        {
            _persistence = persistence;
            _metrics = metrics;
            _serializer = new ManifestSerialization();

            string dir = persistence.DataRoot();
            try
            {
                if (!Directory.Exists(dir))
                {
                    CreateDirectory(dir);
                }
            }
            catch (Exception e)
            {
                metrics?.AddCounter(METRIC_SOURCE, "InitFailed");
                FuelLog.Fatal(e, "Failed to create directory to store manifests");
                throw new ManifestStorageInitException("Failed to create directory to store manifests", e);
            }
        }

        /// <summary>
        /// Creates the directory at path and makes it accessible by everyone
        /// </summary>
        /// <param name="path">path of directory to create</param>
        private void CreateDirectory(string path)
        {
            Directory.CreateDirectory(path);
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            DirectorySecurity directorySecurity = directoryInfo.GetAccessControl();
            directorySecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                FileSystemRights.FullControl,
                InheritanceFlags.ObjectInherit | InheritanceFlags.ObjectInherit,
                PropagationFlags.NoPropagateInherit,
                AccessControlType.Allow));
            directoryInfo.SetAccessControl(directorySecurity);
        }

        /// <summary>
        /// Persists a manifest to disk without deserializing first. If there is already a file stored at this ID it will be overwritten.
        /// Optionally verify the file after storage by reading and deserializing to memory.
        /// </summary>
        /// <param name="adgProdId">an adg product id</param>
        /// <param name="versionId">an sds version id</param>
        /// <param name="stream">the stream to write to disk</param>
        /// <param name="verifyAfterStorage">ensure that the stored manifest can be deserialized</param>
        /// <returns>true if the manifest was persisted successfully, false otherwise</returns>
        public async Task<bool> PutRaw(ProductId adgProdId, string versionId, Stream stream, bool verifyAfterStorage)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            try
            {
                var filename = GetFilePath(adgProdId, versionId);
                EnsureFilePathExists(filename);

                var fileStream = File.Create(filename);
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
                fileStream.Close();

                if (verifyAfterStorage)
                {
                    try
                    {
                        using (FileStream fileStreamR = new FileStream(GetFilePath(adgProdId, versionId), FileMode.Open, FileAccess.Read))
                        {
                            await _serializer.Deserialize(fileStreamR);
                        }
                    }
                    catch (Exception)
                    {
                        File.Delete(filename);
                        throw;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "PutRawFailed");
                FuelLog.Error(e, "Failed to store the manifest for version", new { versionId, adgProdId });
                return false;
            }
        }


        /// <summary>
        /// Deletes a manifest on disk. This can be used if it is no longer needed, e.g. after an uninstall or update finishes.
        /// </summary>
        /// <param name="adgProdId">an adg product id</param>
        /// <param name="versionId">an sds version id</param>
        /// <returns>true if it was successfully deleted, false otherwise. If the manifest does not exist it will return true.</returns>
        public bool Delete(ProductId adgProdId, string versionId)
        {
            if (adgProdId == null)
            {
                throw new ArgumentNullException("adgProdId");
            }

            try
            {
                if (!File.Exists(GetFilePath(adgProdId, versionId)))
                {
                    return true;
                }

                File.Delete(GetFilePath(adgProdId, versionId));
                return true;
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "DeleteFailed");
                FuelLog.Error(e, "Failed to delete manifest for version", new { versionId, adgProdId });
                return false;
            }
        }

        /// <summary>
        /// Retrieves a manifest from disk by the version id it corresponds to.
        /// </summary>
        /// <param name="adgProdId">an adg product id</param>
        /// <param name="versionId">an sds version id</param>
        /// <returns>the manifest, or null if none could be found corresponding to the given version id</returns>
        public async Task<Sds.Manifest> Get(ProductId adgProdId, string versionId)
        {
            if (adgProdId == null)
            {
                throw new ArgumentNullException("adgProdId");
            }

            try
            {
                if (!File.Exists(GetFilePath(adgProdId, versionId)))
                {
                    return null;
                }

                using (FileStream file = new FileStream(GetFilePath(adgProdId, versionId), FileMode.Open, FileAccess.Read))
                {
                    return await _serializer.Deserialize(file);
                };
            }
            catch (Exception e)
            {
                _metrics?.AddCounter(METRIC_SOURCE, "GetFailed");
                FuelLog.Error(e, "Failed to get manifest", new { versionId, adgProdId });
                return null;
            }
        }

        private void EnsureFilePathExists(string fileName)
        {
            var filePath = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }

        public string GetFilePath(ProductId adgProdId, string versionId)
        {
            return Path.Combine(
                Root, 
                Constants.Paths.GameDataSubdir, 
                adgProdId.Id,
                Constants.Paths.ManifestsSubDir,
                $"{versionId}.{FS_EXTENSION}");
        }
    }

    public class ManifestStorageInitException : Exception
    {
        public ManifestStorageInitException(string message, Exception cause) : base(message, cause)
        {
        }
    }
}
