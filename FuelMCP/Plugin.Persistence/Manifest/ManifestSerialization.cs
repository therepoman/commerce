﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using XZ.NET;
using Google.Protobuf;
using System.Threading.Tasks;
using System.Diagnostics;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;

namespace Tv.Twitch.Fuel.Manifest
{
    public interface IManifestDeserializer
    {
        Task<Sds.Manifest> Deserialize(Stream input);
    }

    /// <summary>
    /// This class helps read stored manifests. It will parse the ManifestHeader and decompress the Manifest body
    /// into the appropriate objects
    /// 
    /// Current manifests are structured as follows:
    /// (header_size: byte)(header: byte[])(compressed_manifest_data: byte[])
    /// 
    /// * The first byte describes the size of the uncompressed ManifestHeader protobuf message.
    /// * It is immediately followed by headerSize bytes that make up the ManifestHeader
    /// * The remainder of the stream is the Manifest protobuf message, possible compressed as described in the headers' CompressionSettings.
    /// </summary>
    public class ManifestSerialization : IManifestDeserializer
    {
        /// <summary>
        /// Manifest signing public key, this corresponds to the private key used by SDS to sign manifests.
        /// 
        /// The keys can be found in Odin, material set name "tv.twitch.fuel.sds.manifest.signing.prod"
        /// You can export the PublicKey via a RHEL5/AL Amazon host:
        /// 
        /// export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/apollo/env/SDETools/lib/; odin-get tv.twitch.fuel.sds.manifest.signing.prod -t PublicKey | openssl rsa -inform DER -pubin
        /// </summary>
        private const string PublicKeyPemStr = @"-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6fSRMUi3VpTtv9P4+KvM
AcAIP4SYbTQfB1ns7vyUjsj8nrF2lGNtQTtGLnrNmM2ElZ2R7VmQtNiRtPMxToIW
Rajin0H0OyzGrHA8P6w96Mj4q1JeORCzJeVFgLOBClCCMmB+5bJBWnJcq/sEMwu9
gGynCeiYNLt7ZMVpL1GOsNjl+yLk7OMMGpMj1JWCVFfgYE9Lud1QZJllFAWhRBoT
wTctAUZTikObFUoBm+KEiCsKIcay4WOybvJwxTNBUl2GL8c+ihrT2ntLPpb9aIJE
/gXU3Ihl5oXe/0P/QN0CRu/ybXWLiGzIYqKIok4nepkdo8V3gWR55K801pOuck0B
awIDAQAB
-----END PUBLIC KEY-----";

        private readonly RsaKeyParameters PublicKey;

        public ManifestSerialization()
        {
            var strReader = new StringReader(PublicKeyPemStr);
            var pemReader = new PemReader(strReader);
            PublicKey = (RsaKeyParameters)((AsymmetricKeyParameter)pemReader.ReadObject());
        }

        public async Task<Sds.Manifest> Deserialize(Stream input)
        {
            // Read the header size, big-endian
            int headerSize = (input.ReadByte() << 24) | (input.ReadByte() << 16) | (input.ReadByte() << 8) | input.ReadByte();

            if (headerSize < 1)
            {
                throw new Exception($"Invalid header size {headerSize}. Corrupt manifest or end of stream?");
            }

            var headerBytes = new byte[headerSize];
            int read = await input.ReadAsync(headerBytes, 0, headerSize);
            if (read != headerSize)
            {
                throw new Exception($"Unable to read Sds.ManifestHeader from stream expected header of size {headerSize} but was only able to read {read} bytes");
            }
            Sds.ManifestHeader header = Sds.ManifestHeader.Parser.ParseFrom(headerBytes);

            byte[] manifestBytes;
            using (Stream decompressedManifestStream = GetDecompressedStream(header, input))
            using (MemoryStream memStream = new MemoryStream())
            {
                // Get decompressed bytes
                await decompressedManifestStream.CopyToAsync(memStream);
                manifestBytes = memStream.ToArray();
            }

            // Verify the manifest signature
            var signer = GetSigner(header);
            signer.BlockUpdate(manifestBytes, 0, manifestBytes.Length);
            var signature = header.Signature.Value.ToByteArray();
            if (!signer.VerifySignature(signature))
            {
                throw new BadManifestSignature("Manifest signature check failed");
            }

            return Sds.Manifest.Parser.ParseFrom(manifestBytes);
        }

        private ISigner GetSigner(Sds.ManifestHeader header)
        {
            switch(header.Signature.Algorithm)
            {
                case Sds.SignatureAlgorithm.Sha256WithRsa:
                    var signer = SignerUtilities.GetSigner("SHA256withRSA");
                    signer.Init(false, PublicKey);
                    return signer;
                default:
                    throw new BadManifestSignature($"Unsupported signature algorithm {header.Signature.Algorithm}");
            }
        }

        private Stream GetDecompressedStream(Sds.ManifestHeader header, Stream stream)
        {
            switch(header.Compression.Algorithm)
            {
                case Sds.CompressionAlgorithm.Lzma:
                    return new XZInputStream(stream);
                case Sds.CompressionAlgorithm.None:
                    return stream;
                default:
                    // TODO real exception
                    throw new Exception($"Unexpected compression algorithm. {header.Compression.Algorithm}");
            }
        }
    }

    public class BadManifestSignature : Exception
    {
        public BadManifestSignature(string message) : base(message)
        {
        }
    }
}
