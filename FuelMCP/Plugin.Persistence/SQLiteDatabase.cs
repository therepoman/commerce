using System;
using System.Data;
using System.Collections.Generic;
using Polly;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Logging.Events;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Resources;
using Microsoft.Data.Sqlite;
using Serilog;
using Microsoft.EntityFrameworkCore;
using Directory = System.IO.Directory;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Plugin.Persistence.Messages;
using Amazon.Fuel.Plugin.Persistence.Utilities;

namespace Amazon.Fuel.Plugin.Persistence
{
    public class SqliteDatabase<TEntity, TEntityDbContext> : IFuelDatabase<TEntity> 
        where TEntity : PersistentEntry, new() 
        where TEntityDbContext : SqliteDbContext<TEntity>, new()
    {
        private const string DB_EXT = ".sqlite";
        private const int DB_OP_FAILURE_RETRIES = 3;
        private const int DB_OP_FAILURE_WAIT_BASE_MS = 300;
        private const double DB_OP_FAILURE_WAIT_RAMP = 1.5;

        // Ensures we only attempt to create the DB once
        private readonly object _initializeLock = new object();
        private volatile bool _initialized = false;

        // TODO add versioning/migration support

        protected readonly IMessengerHub _msg;
        protected readonly IMetricsPlugin _metrics;
        protected readonly IPersistence _persistence;
        protected readonly IOSDriver _osDriver;

        private bool _shutdownReceived = false;

        public SqliteDatabase(IPersistence persistence, IMessengerHub msg, IMetricsPlugin metrics, IOSDriver osDriver)
        {
            _msg = msg;
            _metrics = metrics;
            _persistence = persistence;
            _osDriver = osDriver;

            try
            {
                InitDirs();
            }
            catch (Exception e)
            {
                metrics?.AddCounter(MetricStrings.SOURCE_DbFailure, "InitFailed");
                FuelLog.Fatal(e, "Failed to create directory for database");
                throw;
            }

            _msg.Subscribe<FuelShutdownMessage>(s =>
            {
                _shutdownReceived = true;
            });
        }

        private void InitDirs()
        {
            var dbFileNameWithPath = GetSqlDbFile(_persistence);
            string baseDir = Path.GetDirectoryName(dbFileNameWithPath);

            if (!Directory.Exists(baseDir))
            {
                FuelLog.Info("DB dir missing, creating new directory", new { baseDir });
                Directory.CreateDirectory(baseDir);
            }
            else
            {
                if (!_osDriver.HasPermissions(baseDir, FileSystemRights.Read))
                {
                    FuelLog.Error("Current user has no read access to DB dir", 
                        new
                        {
                            baseDir,
                            dbDirperm = _osDriver.DescribePermissions(baseDir),
                            parentDirPerm = _osDriver.DescribePermissions(Directory.GetParent(baseDir).FullName)
                        });
                }

                if (!_osDriver.HasPermissions(baseDir, FileSystemRights.Write))
                {
                    FuelLog.Warn("Current user has no write access to DB dir",
                        new
                        {
                            baseDir,
                            dbDirperm = _osDriver.DescribePermissions(baseDir),
                            parentDirPerm = _osDriver.DescribePermissions(Directory.GetParent(baseDir).FullName)
                        });
                }

                try
                {
                    if (File.Exists(dbFileNameWithPath))
                    {
                        if (!_osDriver.HasPermissions(dbFileNameWithPath, FileSystemRights.Read))
                            FuelLog.Error("Current user has no read access to existing DB file", new { dbFileNameWithPath, fileperm = _osDriver.DescribePermissions(dbFileNameWithPath) });

                        if (!_osDriver.HasPermissions(dbFileNameWithPath, FileSystemRights.Write))
                            FuelLog.Warn("Current user has no write access to existing DB file", new { dbFileNameWithPath, fileperm = _osDriver.DescribePermissions(dbFileNameWithPath) });
                    }
                }
                catch (Exception e)
                {
                    FuelLog.Error(e, "Couldn't report DB access stats");
                }
            }
        }

        public static string GetSqlDbFile(IPersistence persistence)
            => Path.Combine(persistence.DataRoot(), Constants.Paths.SqliteDatabaseSubDir, $"{new TEntity().StorageContext}{DB_EXT}");

        public virtual List<TEntity> All
        {
            get
            {
                var ret = new List<TEntity>();

                DbReadAction(
                    context =>
                    {
                        ret = context.DbSet.ToList();
                    });

                return ret;
            }
        }

        public IEnumerable<TEntity> FindEntriesByFilter(Func<TEntity, bool> filter)
        {
            IEnumerable<TEntity> ret = new List<TEntity>();
            DbReadAction(
                context =>
                {
                    ret = context.DbSet.Where(filter).ToList();
                });

            return ret;
        }


        public void AddOrReplaceById(TEntity entity) => AddOrReplaceById(entity, commitChanges: true);

        public void AddOrReplaceById(TEntity entity, bool commitChanges)
        {
            DbWriteAction(
                context =>
                {
                    addOrReplaceByIdNotWrapped(context, entity);
                },
                commitChanges: commitChanges
            );
        }

        private void addOrReplaceByIdNotWrapped(TEntityDbContext context, TEntity entity)
        {
            var id = ((PersistentEntry)entity).Id;
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"id must not be null or empty");
            }

            var findEntity = context.DbSet.SingleOrDefault(item => item.Id == id);
            if (findEntity != default(TEntity))
            {
                // replace
                context.Entry(findEntity).CurrentValues.SetValues(entity);
            }
            else
            {
                // add
                context.DbSet.Add(entity);
            }
        }

        public TEntity TryFindById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"id must not be null or empty");
            }

            var ret = default(TEntity);
            DbReadAction(
                context =>
                {
                    ret = context.DbSet.SingleOrDefault(item => item.Id == id);
                });

            return ret;
        }

        public void Remove(TEntity entity) => Remove(entity, commitChanges: true);

        public void Remove(TEntity entity, bool commitChanges)
        {
            DbWriteAction(
                context =>
                {
                    var id = entity.Id;
                    if (String.IsNullOrEmpty(id))
                    {
                        throw new ArgumentException($"id must not be null or empty");
                    }

                    // be careful not to remove entries that don't exist, you can hit 
                    // concurrency issues when you save
                    var findEntity = context.DbSet.SingleOrDefault(dbItem => id == dbItem.Id);
                    if (findEntity != default(TEntity))
                    {
                        context.DbSet.Remove(findEntity);
                    }
                },
                commitChanges: commitChanges,
                skipIfDbNotExist: true);
        }

        public void RemoveById(string id) => RemoveById(id, commitChanges: true);

        public void RemoveById(string id, bool commitChanges)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"id must not be null or empty");
            }

            DbWriteAction(
                context =>
                {
                    // be careful not to remove entries that don't exist, you can hit 
                    // concurrency issues when you save
                    var entity = context.DbSet.SingleOrDefault(dbItem => id == dbItem.Id);
                    if (entity != default(TEntity))
                    {
                        context.DbSet.Remove(entity);
                    }
                },
                commitChanges: commitChanges,
                skipIfDbNotExist: true);
        }

        public void RemoveAll() => RemoveAll(commitChanges: true);

        public void RemoveAll(bool commitChanges)
        {
            DbWriteAction(
                context =>
                {
                    context.DbSet.RemoveRange(context.DbSet);
                },
                commitChanges: commitChanges,
                skipIfDbNotExist: true);
        }

        public void ReplaceAll(IEnumerable<TEntity> items) => ReplaceAll(items, commitChanges: true);

        public void ReplaceAll(IEnumerable<TEntity> items, bool commitChanges)
        {
            RemoveAll(commitChanges: commitChanges);
            DbWriteAction(
                context =>
                {
                    context.DbSet.AddRange(items);
                },
                commitChanges: commitChanges);
        }

        public void RemoveManyById(IEnumerable<string> items) => RemoveManyById(items, commitChanges: true);

        public void RemoveManyById(IEnumerable<string> items, bool commitChanges)
        {
            DbWriteAction(
                context =>
                {
                    foreach (var itemId in items)
                    {
                        var entity = context.DbSet.SingleOrDefault(dbItem => itemId == dbItem.Id);
                        if (entity != default(TEntity))
                        {
                            context.DbSet.Remove(entity);
                        }
                    }
                },
                commitChanges: commitChanges,
                skipIfDbNotExist: true);
        }

        public bool DbExists
        {
            get
            {
                try
                {
                    return File.Exists(GetSqlDbFile(_persistence));
                }
                catch (Exception e)
                {
                    FuelLog.Warn(e, "Exception while looking for db", new { name = GetSqlDbFile(_persistence) });
                    return false;
                }
            }
        }

        public void DbReadAction(Action<TEntityDbContext> lambda, bool skipIfDbNotExist = true)
            => DbAction(lambda, saveChanges: false, skipIfDbNotExist: skipIfDbNotExist);

        public virtual void DbWriteAction(Action<TEntityDbContext> lambda, bool commitChanges, bool skipIfDbNotExist = false)
            => DbAction(lambda, saveChanges: commitChanges, skipIfDbNotExist: skipIfDbNotExist);

        private void DbAction(Action<TEntityDbContext> lambda, bool saveChanges, bool skipIfDbNotExist)
        {
            var dbExists = DbExists;
            if (skipIfDbNotExist && !dbExists)
            {
                FuelLog.Info("Skipping db action for non-existent DB");
                return;
            }

            var dbFile = GetSqlDbFile(_persistence);
            if (dbExists && !_osDriver.HasPermissions(dbFile, FileSystemRights.Read))
            {
                FuelLog.Error("DB read access not granted", new { file = GetSqlDbFile(_persistence), allPermissions = _osDriver.DescribePermissions(dbFile) });
                // TODO store state to recreate DB once UAC granted?
                return;
            }

            if (dbExists && saveChanges && !_osDriver.HasPermissions(dbFile, FileSystemRights.Write))
            {
                FuelLog.Error("DB write access not granted", new { file = GetSqlDbFile(_persistence), allPermissions = _osDriver.DescribePermissions(dbFile) });
                // TODO https://jira.twitch.com/browse/FLCL-1696 we should throw here, once we have general handlers for Fuel failure shutdown state
                return;
            }

            WrapDbAction(
                dbContext =>
                {
                    lambda(dbContext);

                    if (saveChanges)
                    {
                        dbContext.SaveChanges();
                    }
                });
        }

        public TEntityDbContext CreateDbContext()
            => new TEntityDbContext()
            {
                DbFileNameWithPath = GetSqlDbFile(_persistence)
            };

        private void InnerDbAction(Action<TEntityDbContext> lambda)
        {
            using (var context = CreateDbContext())
            {
                try
                {
                    if (_initialized)
                    {
                        // make sure the DB still exists, otherwise we can get some nasty errors
                        if (!DbExists)
                        {
                            FuelLog.Warn("Db was marked initialized but went missing, recreating", new { dbFileName = GetSqlDbFile(_persistence) });
                            _initialized = false;
                        }
                    }

                    // double-checked locking
                    if (!_initialized)
                    {
                        lock (_initializeLock)
                        {
                            if (!_initialized)
                            {
                                FuelLog.Info("Initializing db", new { DbExists, dbFileName = GetSqlDbFile(_persistence) });

                                string pendingMigrationsStr = "";
                                bool isMigration = false;
                                if (context.Database.GetPendingMigrations()?.Any() ?? false)
                                {
                                    isMigration = true;
                                    pendingMigrationsStr = string.Join(",", context.Database.GetPendingMigrations().ToArray());
                                    FuelLog.Info("Migrations pending", new {DbExists, pendingMigrations = pendingMigrationsStr});
                                }

                                int migrationSuccessInt = 1;
                                try
                                {
                                    // NOTE: Entity sometimes throws an internal exception here, step past this line to see if it is handled internally
                                    context.Database.Migrate();
                                }
                                catch (Exception e)
                                {
                                    if (isMigration)
                                    {
                                        FuelLog.Error(e, "Migration failure, will attempt to continue", new { DbExists, pendingMigrations = pendingMigrationsStr, dbName = GetSqlDbFile(_persistence) });
                                        migrationSuccessInt = 0;
                                        // migration failure, but attempt to keep going (possibly recoverable?)
                                    }
                                    else
                                    {
                                        // general DB failure
                                        throw;
                                    }
                                }

                                if (isMigration)
                                {
                                    _metrics.AddDiscreteValue(MetricStrings.SOURCE_DbFailure, "MigrationSuccess", migrationSuccessInt);
                                }

                                _initialized = true;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
#if DEBUG
                    Debug.Fail("If you break here on EnsureCreated(), your DB schema may have incompatibilities (check model diffs)");
#endif
                    var dbFileName = GetSqlDbFile(_persistence);
                    FuelLog.Fatal(e, $"Failed to initialize SQLite dbFileName={dbFileName}");
                    _metrics?.AddCounter(MetricStrings.SOURCE_DbFailure, "InitFailed");
                    Log.Write(CrashReportEvent.Create(Constants.CrashScope.App, Constants.CrashType.Error, e, $"Sqlite DB init failure on EnsureCreated dbFileName={dbFileName}"));
                    throw;
                }

                lambda(context);
            }
        }

        private void WrapDbAction(Action<TEntityDbContext> lambda, bool allowRetry = true)
        {
            bool wipeDbAndTryAgain = false;
            try
            {
                Policy
                .Handle<DbUpdateConcurrencyException>()
                .Or<SqliteException>()
                .WaitAndRetry(
                    retryCount: DB_OP_FAILURE_RETRIES,
                    sleepDurationProvider: retryAttempt => TimeSpan.FromMilliseconds(DB_OP_FAILURE_WAIT_BASE_MS * Math.Pow(retryAttempt, DB_OP_FAILURE_WAIT_RAMP)),
                    onRetry: (e, ts) => _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"DbRetryable{e.GetType().Name}"))
                .Execute(() => InnerDbAction(lambda));
            }
            catch (DbUpdateConcurrencyException ex)
            {
                FuelLog.Error(ex, "DbUpdateConcurrencyException encountered aftern multiple retries", new { DB_OP_FAILURE_RETRIES });
                Log.Write(CrashReportEvent.Create(Constants.CrashScope.App, Constants.CrashType.Error, ex, "Sqlite DB concurrency failure"));
                _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, "DbUpdateConcurrencyException");
            }
            catch (DataException ex)
            {
                FuelLog.Error(ex, "DataException encountered"); // validity failed
                Log.Write(CrashReportEvent.Create(Constants.CrashScope.App, Constants.CrashType.Error, ex, "Sqlite DB DataException"));
                _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, "SqliteDataException");
            }
            catch (ObjectDisposedException)
            {
                // ignore, generally due to shutdown procedures
                FuelLog.Info($"Db access after disposal, shutdownReceived: '{_shutdownReceived}'");
                if (!_shutdownReceived)
                {
                    _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, "DisposedAccessBeforeShutdown");
                }
            }
            catch (SqliteException e)
            {
                if (e.Message.Contains("no such column"))
                {
#if DEBUG
                    Debug.Fail($"Column missing from a table, did you forget to migrate new/changed entries? '{e.Message}'");
#endif
                    FuelLog.Error($"Sqlite column missing error", new { e.Message });
                    _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, "SqliteExceptionMissingColumn");
                }

                FuelLog.Error(e, $"SqliteException hit during action, data may have been lost", new { type = typeof(TEntity), err = e.GetSqliteErrCode() });
                _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"SqliteException");
                _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"SqliteException_{e.GetSqliteErrCode()}");

                Log.Write(CrashReportEvent.Create(Constants.CrashScope.App, (allowRetry) ? Constants.CrashType.Error : Constants.CrashType.Fault, e, $"Unknown Sqlite DB Exception, {e.GetSqliteErrCode()} " + (allowRetry ? "" : "(unrecoverable)")));

                if (!allowRetry)
                {
                    // TODO : https://jira.twitch.com/browse/FLCL-1518 surface error to user
                    FuelLog.Fatal(e, $"Unrecoverable exception hit during action in DB '{typeof(TEntity)}', data may have been lost");
                }

                wipeDbAndTryAgain = allowRetry;
            }
            catch (Exception e)
            {
                Log.Write(CrashReportEvent.Create(Constants.CrashScope.App, Constants.CrashType.Fault, e, "Unknown Sqlite DB Exception"));
                FuelLog.Error(e, $"Exception hit during action in DB '{typeof(TEntity)}', data may have been lost");
                _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, "SqliteUnknownException");
            }

            if (wipeDbAndTryAgain
                && TryRemoveDb() )
            {
                WrapDbAction(lambda, allowRetry: false);
            }
        }

        private bool TryRemoveDb()
        { 
            // TODO : https://jira.twitch.com/browse/FLCL-1518 surface error to user
            var dbFileName = GetSqlDbFile(_persistence);
            FuelLog.Warn("First chance DB exception: requesting delete corrupt/invalid database to try again", new { dbFileName });

            var aboutToWipeMsg = new DbFailedAndWipePendingCancellableMessage<TEntity, TEntityDbContext>(this, this, dbFileName);
            _msg?.Publish(aboutToWipeMsg);
            // if a broadcast recipient cancelled, we'll abort the operation
            if (aboutToWipeMsg.Cancelled)
            {
                FuelLog.Warn("Db wipe cancelled");
                return false;
            }

            try
            {
                if ( File.Exists(dbFileName) )
                {
                    try
                    {
                        // try removing gracefully first
                        FuelLog.Info("Deleting file (via Sqlite)", new { dbFileName });
                        using (var context = CreateDbContext())
                        {
                            context.Database.EnsureDeleted();
                        }
                    }
                    catch (Exception ex)
                    {
                        FuelLog.Warn("Couldn't delete DB via Sqlite", new { dbFileName, msg = ex.Message });
                    }

                    bool fileExists = File.Exists(dbFileName);
                    FuelLog.Info("Deleting DB, if exist", new { fileExists });
                    if (fileExists)
                    {
                        FuelLog.Info("Deleting file (via FS)", new { dbFileName });
                        File.Delete(dbFileName);
                    }
                }
                else
                {
                    FuelLog.Info("Couldn't find file for deletion", new { dbFileName });
                    // no point in trying again
                    return false;
                }
            }
            catch (Exception ex)
            {
                FuelLog.Error(ex, "Couldn't delete database (no permissions or locked?)");
                return false;
            }

            _msg?.Publish(new DbFailedAndWipedMessage<TEntity, TEntityDbContext>(this, this, dbFileName));

            return true;
        }
    }
}