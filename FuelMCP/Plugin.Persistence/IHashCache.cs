﻿using System;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Utilities;

namespace Amazon.Fuel.Plugin.Persistence
{
    /// <summary>
    /// Cache for the Hashes which have been calculated for the user's local files. 
    /// Primarily used to save time recalculating the hash for large files
    /// </summary>
    public interface IHashCache : IDisposable
    {
        /// <summary>
        /// Removes a file entry for the hash cache
        /// </summary>
        void Remove(string file);


        void UpdateCache( string file, string hash );

        /// <summary>
        /// Returns the hash of a file, if there is no cached information null is returned.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="ProgressCallback"></param>
        /// <returns></returns>
        string GetCachedFileHash(string file);

        /// <summary>
        /// Recomputes the hash for a file if forceRecompute or if the cached value is null, stores it in the cache and returns the hash of a file.
        /// If the file does not exist, null will be returned.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="progressCallback"></param>
        /// <returns></returns>
        string UpdateFileHash(string file, bool forceRecompute = false, ShaUtil.FileHashProgressCallback progressCallback = null);

        /// <summary>
        /// Recomputes the hash for a file if forceRecompute or if the cached value is null, stores it in the cache and returns the hash of a file.
        /// If the file does not exist, null will be returned.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="progressCallback"></param>
        /// <returns></returns>
        Task<string> UpdateFileHashAsync(string file, bool forceRecompute = false, ShaUtil.FileHashProgressCallback progressCallback = null);
    }
}