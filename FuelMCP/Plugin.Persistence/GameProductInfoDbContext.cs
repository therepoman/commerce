﻿using System;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Plugin.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Amazon.Fuel.Plugin.Persistence
{
    public class GameProductInfoDbContext : SqliteDbContext<GameProductInfo>
    {
        public GameProductInfoDbContext()
            : base(new DbContextOptionsBuilder<SqliteDbContext<GameProductInfo>>().Options)
        {
        }
    }
}