using System;
using Amazon.Fuel.Common.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Amazon.Fuel.Plugin.Persistence
{
    public class SqliteDbContext<TEntry> : DbContext where TEntry : PersistentEntry, new()
    {
        public SqliteDbContext(DbContextOptions<SqliteDbContext<TEntry>> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={DbFileNameWithPath};");
        }

        public string DbFileNameWithPath { get; set; }
        public DbSet<TEntry> DbSet { get; set; }
    }
}