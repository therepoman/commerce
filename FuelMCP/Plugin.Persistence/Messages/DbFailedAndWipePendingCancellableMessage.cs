﻿using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Persistence.Messages
{
    // this message is broadcast just before a critical failure occurs in the DB, requesting deletion (which may be cancelled, causing the operation to abort)
    public class DbFailedAndWipePendingCancellableMessage<TDbEntity, TDbEntityDbContext> : CancellableGenericTinyMessage<SqliteDatabase<TDbEntity, TDbEntityDbContext>> 
        where TDbEntity : PersistentEntry, new()
        where TDbEntityDbContext : SqliteDbContext<TDbEntity>, new()
    {
        public bool Cancelled { get; set; } = false;
        public string SqlFile { get; private set; }

        public DbFailedAndWipePendingCancellableMessage(object sender, SqliteDatabase<TDbEntity, TDbEntityDbContext> db, string sqlFile) : base(sender, db, () => { })
        {
            Cancel = () => { Cancelled = true; };
            SqlFile = sqlFile;
        }
    }
}
