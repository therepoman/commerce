using TinyMessenger;

namespace Amazon.Fuel.Plugin.Persistence.Messages
{
    public class DbIntegrityErrorMsg : TinyMessageBase
    {
        public enum EType
        {
            DbCorruptionDetectedAndWiped,
            DbCorruptionDetectedAndUnableToWipe,
            DbLockFailedToObtain
        }

        public EType Type { get; set; }
        public string RecordType { get; set; }

        public DbIntegrityErrorMsg(object sender, EType type, string recordType) : base(sender)
        {
            Type = type;
            RecordType = recordType;
        }
    }
}