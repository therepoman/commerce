﻿using Amazon.Fuel.Common.Contracts;
using TinyMessenger;

namespace Amazon.Fuel.Plugin.Persistence.Messages
{
    // this message is broadcast after a database is deleted and the current operation is about to retry
    public class DbFailedAndWipedMessage<TDbEntity, TDbEntityDbContext> : GenericTinyMessage<SqliteDatabase<TDbEntity, TDbEntityDbContext>> 
        where TDbEntity : PersistentEntry, new()
        where TDbEntityDbContext : SqliteDbContext<TDbEntity>, new()
    {
        public string SqlFile { get; private set; }

        public DbFailedAndWipedMessage(object sender, SqliteDatabase<TDbEntity, TDbEntityDbContext> db, string sqlFile) : base(sender, db)
        {
            SqlFile = sqlFile;
        }
    }
}
