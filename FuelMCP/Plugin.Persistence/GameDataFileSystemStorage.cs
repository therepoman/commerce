using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Amazon.Fuel.Common;
using Amazon.Fuel.Common.Contracts;
using Serilog;

namespace Amazon.Fuel.Plugin.Persistence
{
    public abstract class GameDataFileSystemStorage<TEntity> 
    {
        private readonly IOSDriver _osDriver;

        // all game data goes into subdirs by id under this location
        public string DataStorageDir => _osDriver.GameDataPath;

        public abstract string DataFileName { get; }

        private string getEntityFileNameById(string id) => Path.Combine(DataStorageDir, id, DataFileName);

        public GameDataFileSystemStorage(IOSDriver osDriver)
        {
            _osDriver = osDriver;
        }

        public TEntity TryGet(string id)
        {
            try
            {
                string fileName = getEntityFileNameById(id);

                if (!File.Exists(fileName))
                {
                    return default(TEntity);
                }

                XmlSerializer serializer = new XmlSerializer(typeof(TEntity));
                
                using (StreamReader sr = new StreamReader(fileName))
                {
                    using (XmlReader xr = new XmlTextReader(sr))
                    {
                        return (TEntity)serializer.Deserialize(xr);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Couldn't get file '{id}'");
            }

            return default(TEntity);
        }

        public void Put(string id, TEntity entity)
        {
            try
            {
                _osDriver.EnsureSystemDataPathExists();

                string fileName = getEntityFileNameById(id);
                var destDir = Path.GetDirectoryName(fileName);

                if (!Directory.Exists(destDir))
                {
                    try
                    {
                        Directory.CreateDirectory(destDir);
                    }
                    catch (Exception e)
                    {
                        Log.Fatal(e, $"Couldn't create directory at '{destDir}'");
                        throw;
                    }
                }

                if (File.Exists(fileName))
                {
                    Log.Information($"Replacing file at '{fileName}'");
                    File.Delete(fileName);
                }
                else
                {
                    Log.Information($"Saving file at '{fileName}'");
                }

                XmlSerializer serializer = new XmlSerializer(typeof(TEntity));
                
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    using (XmlWriter xw = new XmlTextWriter(sw))
                    {
                        serializer.Serialize(xw, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Couldn't put file '{id}'");
            }
        }

        public void Delete(string id)
        {
            string fileName = getEntityFileNameById(id);

            try
            {
                if (!File.Exists(fileName))
                {
                    return;
                }

                Log.Information($"Removing file '{fileName}'");
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Couldn't remove file '{fileName}'");
            }
        }

        public string[] FindAllIds()
        {
            return Directory.GetDirectories(_osDriver.GameDataPath)
                .Where(dir => Directory.Exists(Path.Combine(dir, DataFileName)))
                .Select(Path.GetFileName)
                .ToArray();
        }
    }
}