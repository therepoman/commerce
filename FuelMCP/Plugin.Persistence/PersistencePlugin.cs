﻿using System;
using System.IO;
using Amazon.Fuel.Common.Contracts;
using Directory = System.IO.Directory;
using Serilog;

namespace Amazon.Fuel.Plugin.Persistence
{
    public abstract class PersistencePlugin : IPersistence
    {
        private readonly string _dataDirectoryBase = null;

        public PersistencePlugin(string persistenceRoot)
        {
            _dataDirectoryBase = persistenceRoot;
        }

        public string DataRoot()
        {
            return _dataDirectoryBase;
        }
    }

    public class UserPersistencePlugin : PersistencePlugin
    {
        public UserPersistencePlugin(IOSDriver osDriver) : base(Path.Combine(osDriver.AppDataPathFull))
        {
        }
    }

    public class SystemPersistencePlugin : PersistencePlugin
    {
        public SystemPersistencePlugin(IOSDriver osDriver) : base(Path.Combine(osDriver.SystemDataPathFull))
        {
        }
    }
}
