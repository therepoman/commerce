using Amazon.Fuel.Common.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Amazon.Fuel.Plugin.Persistence
{
    public class GameInstallInfoDbContext : SqliteDbContext<GameInstallInfo>
    {
        public GameInstallInfoDbContext()
            : base(new DbContextOptionsBuilder<SqliteDbContext<GameInstallInfo>>().Options)
        {
        }
    }
}