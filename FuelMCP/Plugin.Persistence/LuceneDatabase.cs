using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Plugin.Persistence.Messages;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Serilog;
using Directory = System.IO.Directory;

namespace Amazon.Fuel.Plugin.Persistence
{
    [Obsolete("Lucene will be removed")]
    public class LuceneDatabase<T> : IDisposable, IFuelDatabase<T> where T : PersistentEntry, new()
    {
        private FSDirectory _luceneDirectory;
        private readonly IMessengerHub _msg;
        private readonly IMetricsPlugin _metrics;

        public bool ReadonlyMode { get; set; } = true;

        private const int LOCK_OBTAIN_RETRY_COUNT = 10;

        public const int MAX_QUERY_RECORDS = 10000;

        public string DataDirectory { get; private set; }

        public LuceneDatabase(IPersistence persistence, IMessengerHub msg, IMetricsPlugin metrics)
        {
            _msg = msg;
            _metrics = metrics;
            DataDirectory = GetDataDirectoryForRoot(persistence.DataRoot());

            _msg.Subscribe<FuelShutdownMessage>((sender) => Dispose());
        }

        public static string GetDataDirectoryForRoot(string root)
        {
            var dataDirectoryBase = root;
            var indexType = new T().StorageContext;
            return Path.Combine(dataDirectoryBase, indexType);
        }

        public void AddOrReplaceById(T t)
        {
            WrapDbSafeties(() =>
            {
                Debug.Assert(!String.IsNullOrEmpty(((PersistentEntry)t).Id));
                using (var searcher = Searcher())
                {
                    var document = t.ToDocument();

                    if (searcher == null)
                    {
                        Writer(w => w.AddDocument(document));
                        return;
                    }

                    var result = searcher.Search<T>(IdTermQuery(t.Id), 1);
                    if (result.TotalHits > 0)
                    {
                        Writer(w => w.UpdateDocument(IdTerm(t.Id), document));
                        return;
                    }

                    Writer(w => w.AddDocument(document));
                }
            });
        }

        public List<T> All => AllByQuery(new MatchAllDocsQuery());

        public List<T> AllByQuery(Query query)
        {
            List<T> ret = new List<T>();
            WrapDbSafeties(() =>
            {
                using (var searcher = Searcher())
                {
                    if (searcher == null)
                    {
                        return;
                    }

                    var result = searcher.Search(query, MAX_QUERY_RECORDS);
                    var docs = result.ScoreDocs.Select(d => searcher.Doc(d.Doc));
                    foreach (var doc in docs)
                    {
                        var item = doc.ToObject<T>();
                        ret.Add(item);
                    }
                }
            });

            return ret;
        }

        public T TryFindById(string id)
        {
            Debug.Assert(!String.IsNullOrEmpty(id));
            return Get(IdTerm(id));
        }

        public T Get(Term term)
        {
            return Get(new TermQuery(term));
        }

        public T Get(Query query)
        {
            T ret = null;
            WrapDbSafeties(() =>
            {
                using (var searcher = Searcher())
                {
                    if (searcher == null)
                    {
                        return;
                    }

                    var result = searcher.Search<T>(query, 1);
                    if (result.TotalHits < 1)
                    {
                        return;
                    }

                    var document = searcher.Doc(result.ScoreDocs[0].Doc);
                    ret = document.ToObject<T>();
                }
            });

            return ret;
        }

        public void RemoveById(string id)
        {
            WrapDbSafeties(() =>
            {
                Writer(w => w.DeleteDocuments(IdTermQuery(id)));
            });
        }

        public void Remove(T t)
        {
            WrapDbSafeties(() =>
            {
                Debug.Assert(!String.IsNullOrEmpty(((PersistentEntry)t).Id));
                Writer(w => w.DeleteDocuments(IdTermQuery(t.Id)));
            });
        }

        public void RemoveAll()
        {
            WrapDbSafeties(() =>
            {
                bool create = true; // will erase the index if it exists
                using (var analyzer = new KeywordAnalyzer())
                using (var writer = new IndexWriter(
                    _luceneDirectory,
                    analyzer,
                    create,
                    IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    // Shouldn't be needed since we told Lucene to re-create the index, but just in-case
                    writer.DeleteAll();
                }
            });
        }

        protected virtual void Writer(Action<IndexWriter> func)
        {
            using (var analyzer = new KeywordAnalyzer())
            using (var writer = new IndexWriter(
                _luceneDirectory,
                analyzer,
                !IndexReader.IndexExists(_luceneDirectory),
                IndexWriter.MaxFieldLength.UNLIMITED))
            {
                func(writer);
                writer.Commit();
                writer.Flush(triggerMerge: true,
                    flushDeletes: true,
                    flushDocStores: true);
            }
        }

        private IndexSearcher Searcher()
        {
            try
            {
                return new IndexSearcher(_luceneDirectory);
            }
            catch (Exception ex) when (ex is NoSuchDirectoryException || ex is DirectoryNotFoundException)
            {
                // $ #REMOVEME we shouldn't get here, but leaving in here for the patch JUST IN CASE
                Log.Warning($"BaseDatabaseInstance.Searcher() [no harm] Couldn't' create database: {ex.GetType()}");
                return null;
            }
        }

        private Term IdTerm(string id)
        {
            return new Term(nameof(PersistentEntry.Id), id);
        }

        private TermQuery IdTermQuery(string id)
        {
            // To lower because Gets are case-sensitive and id is lower case after document conversion
            return new TermQuery(IdTerm(id));
        }

        public void ReplaceAll(IEnumerable<T> items)
        {
            WrapDbSafeties(() =>
            {
                bool create = true; // we can create the index since we are replacing everything
                using (var analyzer = new KeywordAnalyzer())
                using (var writer = new IndexWriter(_luceneDirectory, analyzer, create, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    writer.DeleteAll();
                    foreach (var item in items)
                    {
                        writer.AddDocument(item.ToDocument());
                    }
                    writer.Commit();
                    writer.Flush(true, true, true);
                }
            });
        }

        public void RemoveManyById(IEnumerable<string> items)
        {
            WrapDbSafeties(() =>
            {
                using (var analyzer = new KeywordAnalyzer())
                using (var writer = new IndexWriter(_luceneDirectory, analyzer, create: false, mfl: IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    foreach (var item in items)
                    {
                        writer.DeleteDocuments(IdTermQuery(item));
                    }
                    writer.Commit();
                    writer.Flush(true, true, true);
                }
            });
        }


        private void WrapDbSafeties(Action call, int lockObtainRetriesRemaining = LOCK_OBTAIN_RETRY_COUNT)
        {
            try
            {
                if (ReadonlyMode && !Directory.Exists(DataDirectory))
                {
                    // no DB, do nothing
                    return;
                }

                using (_luceneDirectory = FSDirectory.Open(DataDirectory))
                {
                    call();
                }
            }
            catch (AlreadyClosedException ex)
            {
                Log.Warning(ex, "Attempting to access db when it has been disposed.");
            }
            catch (CorruptIndexException ex)
            {
                Log.Error(ex, "Index corrupted when trying to remove all data, attempting to wipe DB instead.");
                bool wiped = true;
                Directory.GetFiles(DataDirectory).ToList().ForEach(f =>
                {
                    try
                    {
                        File.Delete(f);
                    }
                    catch (Exception ioex)
                    {
                        Log.Error(ioex, $"Couldn't remove file in directory {DataDirectory}: {f}");
                        wiped = false;
                    }
                });

                if (wiped)
                {
                    _msg.Publish(
                        new DbIntegrityErrorMsg(
                            sender: this,
                            recordType: typeof(T).ToString(),
                            type: DbIntegrityErrorMsg.EType.DbCorruptionDetectedAndWiped));
                    _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"{MetricStrings.METRIC_DbCorruptWiped}_{typeof(T).ToString()}");
                }
                else
                {
                    _msg.Publish(
                        new DbIntegrityErrorMsg(
                            sender: this,
                            recordType: typeof(T).ToString(),
                            type: DbIntegrityErrorMsg.EType.DbCorruptionDetectedAndUnableToWipe));
                    _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"{MetricStrings.METRIC_DbCorruptUnableToWipe}_{typeof(T).ToString()}");
                }
            }
            catch (Exception ex) // NOTE: was `when (ex is LockObtainFailedException || ex is IOException || ex is UnauthorizedAccessException)`, but I'm opting to catch all exceptions here in an attempt to recover
            {
                _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"{MetricStrings.METRIC_DbLockedFailedToObtain}_{typeof(T).ToString()}");

                if (lockObtainRetriesRemaining > 0)
                {
                    Log.Warning(ex, $"Failed to obtain lock, waiting 1-2 seconds to retry (retries remaining {lockObtainRetriesRemaining})");
                    Thread.Sleep(100 + new Random().Next() % 100);
                    WrapDbSafeties(call, lockObtainRetriesRemaining-1);
                }
                else
                {
                    Log.Error(ex, "Unable to obtain lock");
                    _msg.Publish(
                        new DbIntegrityErrorMsg(
                            sender: this, 
                            recordType: typeof(T).ToString(), 
                            type: DbIntegrityErrorMsg.EType.DbLockFailedToObtain ));

                    _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, $"{MetricStrings.METRIC_DbLockedFailedToObtainAndAborted}_{typeof(T).ToString()}");
                }
            }
            finally
            {
                _luceneDirectory = null;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _luceneDirectory?.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}