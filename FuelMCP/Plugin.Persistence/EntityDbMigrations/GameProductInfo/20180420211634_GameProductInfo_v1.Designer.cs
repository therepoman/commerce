﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Amazon.Fuel.Common.Contracts;

namespace Amazon.Fuel.Plugin.Persistence.EntityDbMigrations.GameProductInfo
{
    [DbContext(typeof(GameProductInfoDbContext))]
    [Migration("20180420211634_GameProductInfo_v1")]
    partial class GameProductInfo_v1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.3");

            modelBuilder.Entity("Amazon.Fuel.Common.Contracts.GameProductInfo", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Background");

                    b.Property<string>("Background2");

                    b.Property<bool>("IsDeveloper");

                    b.Property<string>("ProductAsin");

                    b.Property<string>("ProductAsinVersion");

                    b.Property<string>("ProductDescription");

                    b.Property<string>("ProductDomain");

                    b.Property<string>("ProductIconUrl");

                    b.Property<string>("ProductIdStr");

                    b.Property<string>("ProductLine");

                    b.Property<string>("ProductPublisher");

                    b.Property<string>("ProductSku");

                    b.Property<string>("ProductTitle");

                    b.Property<string>("ScreenshotsJson");

                    b.Property<string>("State");

                    b.Property<string>("VideosJson");

                    b.HasKey("Id");

                    b.ToTable("DbSet");
                });
        }
    }
}
