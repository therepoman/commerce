﻿using Amazon.Fuel.Common.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Fuel.Common.Messages;
using Amazon.Fuel.Common.Utilities;
using Amazon.Fuel.Common;

namespace Amazon.Fuel.Plugin.Persistence
{
    public class HashCache : Disposable, IHashCache
    {
        public class HashCacheDataSerializationException : Exception
        {
            public HashCacheDataSerializationException(string msg) : base(msg)
            {
            }
        }

        public class HashCacheData
        {
            private const char DELIM = ';';

            public string Hash { get; set; }
            public DateTime LastTimeStamp { get; set; }

            public override string ToString()
            {
                return Hash;
            }

            // returns data in format "<hash>;<datetime>", where hash is a base64 string of variable size and date time follows ISO 8601 format
            public string Serialize()
            {
                var plainTextBytes = Encoding.UTF8.GetBytes(Hash);
                return Convert.ToBase64String(plainTextBytes) 
                        + DELIM
                        + LastTimeStamp.ToString("O");
            }

            // expects data in format "<hash>;<datetime>", where hash is a base64 string of variable size and date time follows ISO 8601 format
            public static HashCacheData Deserialize(string data)
            {
                var fieldData = data.Split(new char[] { DELIM }, 2);
                if (fieldData.Length != 2)
                {
                    throw new HashCacheDataSerializationException($"Invalid data encountered '{data}'");
                }

                return new HashCacheData()
                {
                    Hash = Encoding.UTF8.GetString(Convert.FromBase64String(fieldData[0])),
                    LastTimeStamp = DateTime.Parse(fieldData[1], CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind)
                };
            }
        }

        public const string StorageContext = "HashCache";
        private const int TimeBetweenCacheUpdatesMilliseconds = 60000;

        private readonly ISecureStoragePlugin _storage;
        private readonly BlockingCollection<SecureStorageItem> _updatedItemsQueue = new BlockingCollection<SecureStorageItem>(new ConcurrentQueue<SecureStorageItem>());

        public HashCache(ISecureStoragePlugin storage, IMessengerHub hub)
        {
            _storage = storage;
            hub.Subscribe<FuelShutdownMessage>(msg =>
            {
                FlushDbQueue();
            });

            StartWorkerUpdateThread();
        }

        //Starts a background worker thread which will process Hash Cache updates in batches.
        private void StartWorkerUpdateThread()
        {
            new Thread(() =>
            {
                while (true)
                {
                    FlushDbQueue();

                    //Throttle update rate to take advantage of batch update performance.
                    Thread.Sleep(TimeBetweenCacheUpdatesMilliseconds);
                }
            }).Start();
        }

        public void Clear()
        {
            FlushDbQueue();
            _storage.Clear();
        }

        /// <summary>
        /// Flush queued writes to DB so that we can perform a read
        /// </summary>
        private void FlushDbQueue()
        {
            var items = new List<SecureStorageItem>();
            try
            {
                if (_updatedItemsQueue.Count == 0) return;

                SecureStorageItem item;
                //Take items out of the queue until there are no more.
                while (_updatedItemsQueue.TryTake(out item, 0))
                {
                    items.Add(item);
                }

                //cache the game file hash in the secure storage
                _storage.PutAll(StorageContext, items.ToArray());
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't flush HashCache");
                try
                {
                    foreach (var item in items)
                    {
                        _updatedItemsQueue.Add(item);
                    }
                }
                // An exception here could kill our HashCache thread so we want to eat whatever happens (though some exceptions
                // would mean our thread will be useless like ObjectDisposedException).
                catch (Exception)
                {
                    FuelLog.Error(e, "Couldn't add back items not added to cache.");
                }
                
            }
        }

        public void UpdateCache( string file, string hash )
        {
            var normalizedFilename = FileUtilities.NormalizePath(file);
            var data = new HashCacheData { Hash = hash, LastTimeStamp = File.GetLastWriteTimeUtc(normalizedFilename) };
            _updatedItemsQueue.Add(new SecureStorageItem(normalizedFilename, data.Serialize()));
        }

        public void Remove(string file)
        {
            var normalizedFilename = FileUtilities.NormalizePath(file);
            _updatedItemsQueue.Add(new SecureStorageItem(normalizedFilename, null));
        }

        /// <summary>
        /// Returns the hash of a file, if there is no cached information null is returned.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="ProgressCallback"></param>
        /// <returns></returns>
        public string GetCachedFileHash(string file)
        {
            var normalizedFilename = FileUtilities.NormalizePath(file);
            string lookup = null;
            try
            {
                lookup = _storage.Get(StorageContext, normalizedFilename);
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't retrieve data from HashCache");
            }

            if (lookup != null)
            {
                try
                {
                    var data = HashCacheData.Deserialize(lookup);
                    if (data.LastTimeStamp == File.GetLastWriteTimeUtc(normalizedFilename))
                    {
                        return data.Hash;
                    }
                }
                catch (Exception e)
                {
                    FuelLog.Error(e, "DB error retrieving a hash cache key");
                }
            }

            return null;
        }

        /// <summary>
        /// Recomputes the hash for a file if forceRecompute or if the cached value is null, stores it in the cache and returns the hash of a file.
        /// If the file does not exist, null will be returned.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="progressCallback"></param>
        /// <returns></returns>
        public string UpdateFileHash(string file, bool forceRecompute = false, ShaUtil.FileHashProgressCallback progressCallback = null)
        {
            string output = null;

            if (File.Exists(file))
            {
                output = GetCachedFileHash(file);
                if (forceRecompute || output == null)
                {
                    string computedHash = ShaUtil.ComputeHash(file, progressCallback);
                    UpdateCache(file, computedHash);
                    output = computedHash;
                }
            }
            return output;
        }

        /// <summary>
        /// Recomputes the hash for a file if forceRecompute or if the cached value is null, stores it in the cache and returns the hash of a file.
        /// If the file does not exist, null will be returned.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="progressCallback"></param>
        /// <returns></returns>
        public async Task<string> UpdateFileHashAsync(string file, bool forceRecompute = false, ShaUtil.FileHashProgressCallback progressCallback = null)
        {
            string output = null;
            
            if (File.Exists(file))
            {
                output = GetCachedFileHash(file);
                if (forceRecompute || output == null)
                {
                    string computedHash = await Task.Run<string>(() => ShaUtil.ComputeHash(file, progressCallback));
                    UpdateCache(file, computedHash);
                    output = computedHash;
                }
            }
            return output;
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing) return;

            FlushDbQueue();
        }
    }
}
