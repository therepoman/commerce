using System;
using System.IO;
using Amazon.Fuel.Common.Contracts;
using Amazon.Fuel.Common.Resources;
using Amazon.Fuel.Common.Utilities;
using Serilog;

namespace Amazon.Fuel.Plugin.Persistence
{
    public class LuceneSystemPersistencePlugin : PersistencePlugin
    {
        public LuceneSystemPersistencePlugin(IOSDriver osDriver) : base(Path.Combine(osDriver.SystemDataPathFullDeprecated, LuceneDbMigrator.LUCENE_DB_DIR)) { }
    }

    public class LuceneUserPersistencePlugin : PersistencePlugin
    {
        public LuceneUserPersistencePlugin(IOSDriver osDriver) : base(Path.Combine(osDriver.AppDataPathFullDeprecated, LuceneDbMigrator.LUCENE_DB_DIR)) { }
    }

    public class LuceneDbMigrator : ILuceneDbMigrator
    {
        private readonly IPersistence _oldSystemPersistence;
        private readonly IPersistence _oldUserPersistence;
        private readonly IMessengerHub _hub;
        private readonly IMetricsPlugin _metrics;
        private readonly IGameInstallInfoDatabase _tgtGameInstallInfoDb;
        private readonly IOSDriver _osDriver;

        private readonly ISecureStoragePlugin _storage;

        public const string LUCENE_DB_DIR = "db";

        public const string STORAGE_CATEGORY = "MCP.Plugin.Persistence";
        public const string STORAGE_USER_HAS_MIGRATED = "LuceneUserHasMigrated";
        public const string STORAGE_USER_MIGRATION_ATTEMPTS = "LuceneUserMigrationAttempts";

        public const int MAX_MIGRATION_ATTEMPTS = 5;

        public LuceneDbMigrator(
            IGameInstallInfoDatabase tgtGameInstallInfoDb, 
            IMessengerHub hub, 
            IMetricsPlugin metrics,
            IOSDriver osDriver,
            ISecureStoragePlugin secureStorage)
        {
            _tgtGameInstallInfoDb = tgtGameInstallInfoDb;
            _osDriver = osDriver;
            _oldUserPersistence = new LuceneUserPersistencePlugin(_osDriver);
            _oldSystemPersistence = new LuceneSystemPersistencePlugin(_osDriver);
            _hub = hub;
            _metrics = metrics;
            _storage = secureStorage;
        }

        // Gets something from secure storage, which can fail when impersonating. These calls are not considered necessary.
        private string SafeStorageGet(string context, string key)
        {
            try
            {
                return _storage.Get(context, key);
            }
            catch (Exception e)
            {
                Log.Warning("Couldn't get secure storage key (impersonation failure?)", new {context, key});
                return null;
            }
        }

        // Gets something from secure storage, which can fail when impersonating. These calls are not considered necessary.
        private void SafeStoragePut(string context, string key, string value)
        {
            try
            {
                _storage.Put(context, key, value);
            }
            catch (Exception e)
            {
                Log.Warning("Couldn't put secure storage key (are we impersonating?)", new { context, key });
            }
        }

        public bool TryEnsureDbsMigrated(bool checkMigrationState = true)
        {
            // prevent re-migration
            int attempts = 0;
            try
            {
                if (checkMigrationState)
                {
                    if (SafeStorageGet(STORAGE_CATEGORY, STORAGE_USER_HAS_MIGRATED)?.Equals(
                        "true", StringComparison.InvariantCultureIgnoreCase) ?? false)
                    {
                        return true;
                    }

                    var attemptsFetch = SafeStorageGet(STORAGE_CATEGORY, STORAGE_USER_MIGRATION_ATTEMPTS);
                    if (attemptsFetch != null)
                    {
                        attempts = int.Parse(attemptsFetch);
                    }

                    ++attempts;

                    if (attempts > MAX_MIGRATION_ATTEMPTS)
                    {
                        // stop trying 
                        FuelLog.Warn("Fuel Lucene DB migration bypassed (too many failures)",
                            new {attempts, MAX_MIGRATION_ATTEMPTS});
                        return false;
                    }
                }

                FuelLog.Info("Fuel Lucene DB migration requested", new {attempts});

                if (checkMigrationState)
                {
                    SafeStoragePut(STORAGE_CATEGORY, STORAGE_USER_MIGRATION_ATTEMPTS, attempts.ToString());
                }

                // ensure that our system persistence root exists with all user permissions before we do anything
                _osDriver.EnsureSystemDataPathExists();

                var ret = TryMigrateOldSystemDb();
                // for now, don't delete old DBs so that we can alternate between the two
                // (need to remove later: https://jira.twitch.com/browse/FLCL-1573 )
                //ret = TryRemoveOldSystemData() && ret;
                //ret = TryRemoveOldUserData() && ret;

                if (ret)
                {
                    FuelLog.Info("Fuel migration completed successfully");
                    if (checkMigrationState)
                    {
                        SafeStoragePut(STORAGE_CATEGORY, STORAGE_USER_HAS_MIGRATED, "true");
                    }

                    return true;
                }
                else
                {
                    FuelLog.Info("Fuel migration failed to complete", new {attempts});
                }
            }
            catch (Exception e)
            {
                FuelLog.Info(e, "Fuel migration failed to complete", new { attempts });
            }

            return false;
        }

        public bool TryMigrateOldSystemDb()
        {
            bool success = true;
            try
            {
                if (!Directory.Exists(_oldSystemPersistence.DataRoot()))
                {
                    return true;
                }

                // we can't guarantee that the old system data root can delete, but we can try to guarantee that the 
                //  install db migrates at least once
                if (!TryMigrateOldSystemDbHelper())
                {
                    _metrics.AddCounter(MetricStrings.SOURCE_DbMigration, MetricStrings.METRIC_LuceneMigrationFailed);
                    success = false;
                }
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Lucene migration error");
                success = false;
            }

            try
            {
                // content is non critical, always wipe out storage so that we don't keep trying
                TryRemoveOldSystemData();
            }
            catch (Exception e)
            {
                FuelLog.Error(e, "Couldn't remove old Lucene data");
                success = false;
            }

            _metrics.AddDiscreteValue(MetricStrings.SOURCE_DbMigration, "LuceneMigrationSuccess", success ? 1 : 0);

            // preserve old metric, for now
            _metrics.AddCounter(MetricStrings.SOURCE_DbMigration, success ? MetricStrings.METRIC_LuceneMigrationSucceeded : MetricStrings.METRIC_LuceneMigrationFailed);

            return success;
        }

        private bool TryMigrateOldSystemDbHelper()
        {
            bool ret = true;
            FuelLog.Info("Migrating old lucene system db", new {root=_oldSystemPersistence.DataRoot()});

            // only need to migrate the install info db (entitlements can safely be rebuilt)
            try
            {
                using (var oldSystemDb = new LuceneDatabase<GameInstallInfoLuceneDeprecated>(_oldSystemPersistence, _hub, _metrics))
                { 
                    foreach (var game in oldSystemDb.All)
                    {
                        try
                        {
                            // don't import a game twice 
                            if (_tgtGameInstallInfoDb.TryFindById(game.Id) != null)
                            {
                                continue;
                            }

                            var convertedInstallInfo = game.ToGameInstallInfo();
                            _tgtGameInstallInfoDb.AddOrReplaceById(convertedInstallInfo);
                        }
                        catch (Exception e)
                        {
                            FuelLog.Error(e, $"Couldn't import/migrate game", new {game?.Id});
                            _metrics.AddCounter(MetricStrings.SOURCE_DbFailure, MetricStrings.METRIC_InstInfoMigrateFailed);
                            ret = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ret = false;
                FuelLog.Error(e, "Couldn't migrate installinfo db");
            }

            FuelLog.Info($"System db migration completed", new {ret});
            return ret;
        }

        // this may not work, if we're not elevated
        public bool TryRemoveOldSystemData() => TryRemoveData(_oldSystemPersistence);

        public bool TryRemoveOldUserData() => TryRemoveData(_oldUserPersistence);

        private bool TryRemoveData(IPersistence persistence)
        {
            try
            {
                if (Directory.Exists(persistence.DataRoot()))
                {
                    FuelLog.Info($"Clearing out old database", new { root = persistence.DataRoot() });
                    return _osDriver.TryDeleteProtected(persistence.DataRoot(), recurse: true);
                }
            }
            catch (UnauthorizedAccessException)
            {
                // NOTE: this may fail, due to locks or elevation (TwitchApp may or may not be elevated, 
                //      whereas installer/uninstaller should always be). Our goal here is to remove Lucene
                //      once we have permissions
                FuelLog.Warn("Couldn't delete old db (not elevated?)", new {type=persistence.GetType()});
                return false;
            }
            catch (Exception e)
            {
                _metrics.AddCounter(MetricStrings.SOURCE_DbMigration, MetricStrings.METRIC_LuceneMigrationFailed);
                FuelLog.Error(e, "Couldn't delete old db", new { type = persistence.GetType() });
                return false;
            }

            return true;
        }
    }
}