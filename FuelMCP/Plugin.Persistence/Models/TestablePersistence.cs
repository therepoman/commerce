﻿using System;

namespace Amazon.Fuel.Plugin.Persistence.Models
{
    public class TestablePersistence : PersistencePlugin, IDisposable
    {
        public TestablePersistence(string datadir) : base(datadir)
        {
        }

        public void Dispose()
        {
        }
    }
}
