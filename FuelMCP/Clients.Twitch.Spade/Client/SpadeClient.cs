﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Clients.Twitch.Spade.Model;
using Newtonsoft.Json;

//http://docs.sci.twitch.tv/faq/engineers.html
//Client for call the Twitch Spade service which processes events for bussiness metrics
namespace Clients.Twitch.Spade.Client
{
    public interface ISpadeClient
    {
        Task SendEventAsync(ISpadeEvent spadeEvent);
    }

    public class SpadeClient : ISpadeClient
    {

        private readonly ISpadeHttpClient _httpClient;

        public SpadeClient(ISpadeHttpClient httpClient)
        {
            if (httpClient == null) throw new ArgumentNullException(nameof(httpClient));
            _httpClient = httpClient;
        }

        /**
         * Sends the given SpadeEvent to the Spade service. Throws SpadeException if there is an issue.
         */
        public async Task SendEventAsync(ISpadeEvent spadeEvent)
        {
            ISpadeHttpResponseMessage response;
            try
            {
                var jsonString = JsonConvert.SerializeObject(spadeEvent);
                var base64JsonString = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonString));
                var postData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("data", base64JsonString)
                };

                var httpContent = new FormUrlEncodedContent(postData);
                response = await _httpClient.PostAsync(httpContent);
            }
            catch (Exception e)
            {
                throw new SpadeException("Unexpected exception when sending Spade Event", e);
            }
            if (response.StatusCode != HttpStatusCode.NoContent)
            {
                throw new SpadeException($"Unsuccessful status code returned from SpadeService when sending Spade Event. httpStatusCode={response.StatusCode}");
            }
        }

        public static double ClientTimeNow()
        {
            return (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

    }

    public interface ISpadeHttpClient
    {
        Task<ISpadeHttpResponseMessage> PostAsync(HttpContent content);
    }

    public class SpadeHttpClient : ISpadeHttpClient
    {
        private const string Endpoint = "https://spade.twitch.tv";

        private readonly HttpClient _httpClient = new HttpClient();
        public async Task<ISpadeHttpResponseMessage> PostAsync(HttpContent content)
        {
            var httpResponseMessage = await _httpClient.PostAsync(Endpoint, content);
            return new SpadeHttpResponseMessage(httpResponseMessage);
        }
    }

    public interface ISpadeHttpResponseMessage
    {
        bool IsSuccessStatusCode { get; }
        HttpStatusCode StatusCode { get; }
    }

    public class SpadeHttpResponseMessage : ISpadeHttpResponseMessage
    {
        public SpadeHttpResponseMessage(HttpResponseMessage httpResponseMessage)
        {
            IsSuccessStatusCode = httpResponseMessage.IsSuccessStatusCode;
            StatusCode = httpResponseMessage.StatusCode;
        }

        public bool IsSuccessStatusCode { get; }
        public HttpStatusCode StatusCode { get; }
    }

    public class SpadeException : Exception
    {
        public SpadeException(string message) : base(message)
        {
        }

        public SpadeException(string message, Exception e) : base(message, e)
        {
        }
    }
}
