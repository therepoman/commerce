﻿using System.Threading.Tasks;
using Clients.Twitch.Spade.Model;

namespace Clients.Twitch.Spade.Client
{
    public class NullSpadeClient : ISpadeClient
    {
        public Task SendEventAsync(ISpadeEvent spadeEvent)
        {
            return Task.FromResult(0); //do nothing
        }
    }
}
