﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clients.Twitch.Spade.Model
{
    public interface ISpadeEvent
    {
        [JsonProperty("event")]
        string Event { get; }
        [JsonProperty("properties")]
        Dictionary<string, dynamic> Properties { get; }
    }

    public abstract class SpadeEvent : ISpadeEvent
    {
        public Dictionary<string, dynamic> Properties { get; } = new Dictionary<string, dynamic>();

        protected void AddProperty(ISpadeEventProperty property)
        {
            if (property != null)
            {
                Properties.Add(property.Key, property.Value);
            }
        }

        public abstract string Event { get; }
    }

    public interface ISpadeEventProperty
    {
        string Key { get; }
        dynamic Value { get; }
    }
}
