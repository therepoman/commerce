using System;
using Clients.Twitch.Spade.Model.Property;

// https://docs.google.com/a/justin.tv/document/d/1vPY9J6pnTKWh7y8WJw3VzwwCgFX6vid2xyEfPIUdc14/edit?usp=sharing
namespace Clients.Twitch.Spade.Model.Event
{
    public class FuelLauncherError : SpadeEvent
    {
        public FuelLauncherError(string deviceId, string userId, double clientTimestamp, string asin, string adgProductId, string sdsVersionId, string errorReason, FuelLauncherErrorType errorType, string transactionId)
        {
            if (deviceId == null) throw new ArgumentNullException(nameof(deviceId));
            AddProperty(new DeviceId(deviceId));
            AddProperty(new UserId(userId));
            AddProperty(new ClientTimestamp(clientTimestamp));
            AddProperty(new Asin(asin));
            AddProperty(new AdgProductId(adgProductId));
            AddProperty(new SdsVersionId(sdsVersionId));
            AddProperty(new ErrorType(errorType.ToString()));
            AddProperty(new ErrorReason(errorReason));
            AddProperty(new TransactionId(transactionId));
        }

        public override string Event => "fuel_launcher_error";
    }
    public enum FuelLauncherErrorType
    {
        GameFullDownloadFailure,
        GamePatchDownloadFailure,
        GameInstallFailure,
        GameLaunchFailure,
        GameUpdateFailure,
        GameUninstallFailure
    }
}