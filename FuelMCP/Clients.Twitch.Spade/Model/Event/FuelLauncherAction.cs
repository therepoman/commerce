﻿using System;
using Clients.Twitch.Spade.Model.Property;
using Action = Clients.Twitch.Spade.Model.Property.Action;

// https://docs.google.com/a/justin.tv/document/d/1vPY9J6pnTKWh7y8WJw3VzwwCgFX6vid2xyEfPIUdc14/edit?usp=sharing
namespace Clients.Twitch.Spade.Model.Event
{
    public class FuelLauncherAction : SpadeEvent
    {
        public FuelLauncherAction(string deviceId, string userId, double clientTimestamp, string asin, string adgProductId, string sdsVersionId, FuelLauncherActionType action, string transactionId)
        {
            if (deviceId == null) throw new ArgumentNullException(nameof(deviceId));
            AddProperty(new DeviceId(deviceId));
            AddProperty(new UserId(userId));
            AddProperty(new ClientTimestamp(clientTimestamp));
            AddProperty(new Asin(asin));
            AddProperty(new AdgProductId(adgProductId));
            AddProperty(new SdsVersionId(sdsVersionId));
            AddProperty(new Action(action.ToString()));
            AddProperty(new TransactionId(transactionId));
        }

        public override string Event => "fuel_launcher_action";
    }
    
    public enum FuelLauncherActionType
    {
        GameFullDownloadStart,
        GameFullDownloadFinish,
        GameFullDownloadCancel,
        GamePatchDownloadStart,
        GamePatchDownloadFinish,
        GamePatchDownloadCancel,
        GameInstallStart,
        GameInstallFinish,
        GameInstallCancel,
        GameLaunchStart,
        GameLaunchFinish,
        GameLaunchCancel,
        GameUpdateStart,
        GameUpdateFinish,
        GameUpdateCancel,
        GameUninstallStart,
        GameUninstallFinish
    }
}
