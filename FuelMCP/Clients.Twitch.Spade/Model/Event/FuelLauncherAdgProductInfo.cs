﻿using System;
using Clients.Twitch.Spade.Model.Property;

// https://docs.google.com/a/justin.tv/document/d/1vPY9J6pnTKWh7y8WJw3VzwwCgFX6vid2xyEfPIUdc14/edit?usp=sharing
namespace Clients.Twitch.Spade.Model.Event
{
    //logging the ADG Product Info we received so we can query it in Spade.
    public class FuelLauncherAdgProductInfo : SpadeEvent
    {
        public FuelLauncherAdgProductInfo(string adgProductId, string asin, string title)
        {
            if (adgProductId == null) throw new ArgumentNullException(nameof(adgProductId));
            AddProperty(new AdgProductId(adgProductId));
            AddProperty(new Asin(asin));
            AddProperty(new Title(title));
        }

        public override string Event => "fuel_launcher_adg_product_info";
    }
}
