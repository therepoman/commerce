﻿namespace Clients.Twitch.Spade.Model.Property
{
    public class DeviceId : ISpadeEventProperty
    {
        public DeviceId(string deviceId)
        {
            Value = deviceId;
        }
        public string Key => "device_id";
        public dynamic Value { get; }
    }
}
