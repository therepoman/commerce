namespace Clients.Twitch.Spade.Model.Property
{
    public class UserId : ISpadeEventProperty
    {
        public UserId(string userId)
        {
            Value = userId;
        }
        public string Key => "user_id";
        public dynamic Value { get; }
    }
}