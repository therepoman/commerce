﻿namespace Clients.Twitch.Spade.Model.Property
{
    //The ID of a multiple event transaction. Such as an installation.
    //This enables us to associate separate to SpadeEvents to a single transactions
    public class TransactionId : ISpadeEventProperty
    {
        public TransactionId(string transactionId)
        {
            Value = transactionId;
        }
        public string Key => "transaction_id";
        public dynamic Value { get; }
    }
}
