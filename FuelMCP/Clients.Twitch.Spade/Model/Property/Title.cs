namespace Clients.Twitch.Spade.Model.Property
{
    public class Title : ISpadeEventProperty
    {
        public Title(string title)
        {
            Value = title;
        }
        public string Key => "title";
        public dynamic Value { get; }
    }
}