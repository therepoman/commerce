namespace Clients.Twitch.Spade.Model.Property
{
    public class ClientTimestamp : ISpadeEventProperty
    {
        public ClientTimestamp(double clientTimestamp)
        {
            Value = clientTimestamp;
        }
        public string Key => "client_time";
        public dynamic Value { get; }
    }
}