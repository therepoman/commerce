namespace Clients.Twitch.Spade.Model.Property
{
    public class Action : ISpadeEventProperty
    {
        public Action(string action)
        {
            Value = action;
        }
        public string Key => "action";
        public dynamic Value { get; }
    }
}