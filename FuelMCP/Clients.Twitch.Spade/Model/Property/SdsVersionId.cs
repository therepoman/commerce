namespace Clients.Twitch.Spade.Model.Property
{
    public class SdsVersionId : ISpadeEventProperty
    {
        public SdsVersionId(string sdsVersionId)
        {
            Value = sdsVersionId;
        }
        public string Key => "sds_version_id";
        public dynamic Value { get; }
    }
}