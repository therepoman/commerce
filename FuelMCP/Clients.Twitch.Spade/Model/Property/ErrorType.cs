namespace Clients.Twitch.Spade.Model.Property
{
    public class ErrorType : ISpadeEventProperty
    {
        public ErrorType(string errorType)
        {
            Value = errorType;
        }
        public string Key => "error_type";
        public dynamic Value { get; }
    }
}