namespace Clients.Twitch.Spade.Model.Property
{
    public class ErrorReason : ISpadeEventProperty
    {
        public ErrorReason(string errorReason)
        {
            Value = errorReason;
        }
        public string Key => "error_reason";
        public dynamic Value { get; }
    }
}