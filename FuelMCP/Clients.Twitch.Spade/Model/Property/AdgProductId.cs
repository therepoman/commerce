namespace Clients.Twitch.Spade.Model.Property
{
    public class AdgProductId : ISpadeEventProperty
    {
        public AdgProductId(string adgProductId)
        {
            Value = adgProductId;
        }
        public string Key => "adg_product_id";
        public dynamic Value { get; }
    }
}