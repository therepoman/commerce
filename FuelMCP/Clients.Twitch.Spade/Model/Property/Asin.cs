namespace Clients.Twitch.Spade.Model.Property
{
    public class Asin : ISpadeEventProperty
    {
        public Asin(string asin)
        {
            Value = asin;
        }
        public string Key => "asin";
        public dynamic Value { get; }
    }
}