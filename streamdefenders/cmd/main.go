package main

import (
	"github.com/gempir/go-twitch-irc"
	"os"
	"fmt"
	"net/http"
	"bytes"
	"regexp"
	"encoding/json"
	"strings"
)

// e.g. the command:
//
//     go run cmd/main.go rcking scwinji oauth:abcd
//
// Will connect to rcking's channel as scwinji.
func main() {
	channelName := os.Args[1]
	botUsername := os.Args[2]
	botOauth    := os.Args[3]
	client := twitch.NewClient(botUsername, botOauth)

	client.OnNewUsernoticeMessage(func(channel string, user twitch.User, message twitch.Message) {
		if message.Tags["msg-id"] == "raid" {
			onDefenseHandler(client, channelName, user, message)
		}
	})

	client.OnNewWhisper(func(user twitch.User, message twitch.Message){
		matches, err := regexp.Match("GetReady [^ ]*", []byte(message.Text))
		if err != nil {
			println(err)
		}

		if matches {
			fmt.Printf("Got ready event: %s\n", message.Text)
			parts := strings.Split(message.Text, " ")
			onReadyHandler(client, user, parts[1])
		}
	})

	client.Join(channelName)

	println("Connecting")
	err := client.Connect()
	if err != nil {
		panic(err)
	}

	println("Success")
}

func onDefenseHandler(irc *twitch.Client, defendingUsername string, attackingUser twitch.User, message twitch.Message) {
	println(fmt.Sprintf("Tags: %s", message.Tags))
	payload := []byte(fmt.Sprintf("{\"defending_channel\":\"%s\",\"attacking_channel\":\"%s\"}", defendingUsername, message.Tags["display-name"]))
	resp, err := http.Post("https://g2v8pvkho1.execute-api.us-west-2.amazonaws.com/default/sd-start-raid", "application/json", bytes.NewBuffer(payload))
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	body := buf.String()
	log := fmt.Sprintf("Sent: %s Got: %s %s %s", string(payload), resp.Status, body, err)
	println("On defense" + log)
	irc.Whisper(defendingUsername, log)
	irc.Whisper(attackingUser.Username, log)
}

func onReadyHandler(irc *twitch.Client, user twitch.User, attacker string) {
	url := fmt.Sprintf("https://g2v8pvkho1.execute-api.us-west-2.amazonaws.com/default/sd-get-ready?channel=%s", attacker)
	resp, err := http.Get(url)
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	body := buf.String()
	irc.Whisper(user.Username, fmt.Sprintf("Sent: %s Got: %s %s %s", url, resp.Status, body, err))
}

func toTuid(username string) (int64, error) {
	url := fmt.Sprintf("https://api.twitch.tv/kraken/users/%s", username)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Client-ID","kxn32no54tlu6xpq4hzjuz6m8kuaj7")
	if err != nil {
		return 0, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}

	var parsed map[string]interface{}
	decoder := json.NewDecoder(resp.Body)
	decoder.UseNumber()
	decoder.Decode(&parsed)
	fmt.Printf("Parsed: %s\n", parsed)
	if err != nil {
		return 0, err
	}

	return parsed["_id"].(json.Number).Int64()
}