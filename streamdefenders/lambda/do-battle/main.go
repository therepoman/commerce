package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"errors"
	"fmt"
	"strings"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/dgrijalva/jwt-go"
)

var (
	ClientId               = "54im47rix9ogj7685q5e2rhjc0dpoz"
	OverlaySecret          = "jYH5QHNNcyKQKBJayn0LzDAp1uStzLX0xcj80Xwigps="
	ApplicationJson        = "application/json"
	BattleStartPubsubType  = "battleStart"
	BattleResultPubsubType = "battleResult"
)

type GetParticipantsResponse struct {
	Participants []string `json:"participants"`
}

type BattleItem struct {
	BattleID              string `json:"battleid"`
	Attacker              string `json:"attacker"`
	Defender              string `json:"defender"`
	AttackingParticipants string `json:"attacking_participants"`
	DefendingParticipants string `json:"defending_participants"`
	IsBattling            string `json:"is_battling"`
}

type GetParticipantsRequest struct {
	Channel string `json:"channel"`
}

type DoBattleRequest struct {
	BattleID string `json:"battle_id"`
}

type BattleResultPubsubMessage struct {
	PubsubType         string `json:"pubsub_type"`
	WinningChannelName string `json:"winning_channel_name"`
	WinningChannelTuid string `json:"winning_channel_tuid"`
	MVPName            string `json:"mvp_name"`
	MVPClass           string `json:"mvp_class"`
}

type PubsubPerms struct {
	Send []string `json:"send"`
}

type PubsubPayload struct {
	ContentType string   `json:"content_type"`
	Message     string   `json:"message"`
	Targets     []string `json:"targets"`
}

type Character struct {
	Tuid       string   `json:"tuid"`
	Attack     int      `json:"attack"`
	BattleLogs []string `json:"battle-logs"`
	Class      string   `json:"class"`
	Defense    int      `json:"defense"`
	Health     int      `json:"health"`
	Luck       int      `json:"luck"`
	OpaqueId   string   `json:"opaque-id"`
	Points     int      `json:"points"`
	Speed      int      `json:"speed"`
}

func Handler(event DoBattleRequest) error {
	battleId := event.BattleID
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	svc := dynamodb.New(sess)

	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("sd-battles"),
		Key: map[string]*dynamodb.AttributeValue{
			"battleid": {
				S: aws.String(battleId),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	battleItem := BattleItem{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &battleItem)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	defenders := strings.Split(battleItem.DefendingParticipants, ",")
	attackers := strings.Split(battleItem.AttackingParticipants, ",")
	fmt.Println("defenders: ", defenders)
	fmt.Println("attackers: ", attackers)

	dParticipants, nonExistingDefenders, err := batchGetParticipants(defenders)
	fmt.Println("defender Characters: ", dParticipants)

	aParticipants, nonExistingAttackers, err := batchGetParticipants(attackers)
	fmt.Println("attacker Characters: ", aParticipants)

	winnerTuid, Mvp, battleLog := doBattle(battleItem.Attacker, battleItem.Defender, dParticipants, aParticipants)
	fmt.Println("winner: ", winnerTuid, " Mvp: ", Mvp, "BattleLog: ", battleLog)

	updateBattleStatus(battleItem.BattleID)
	if winnerTuid == battleItem.Attacker {
		aParticipants = updatePeople(aParticipants, nonExistingAttackers, Mvp, 10, 20)
		dParticipants = updatePeople(dParticipants, nonExistingDefenders, Mvp, 0, 20)
	} else {
		aParticipants = updatePeople(aParticipants, nonExistingAttackers, Mvp, 0, 20)
		dParticipants = updatePeople(dParticipants, nonExistingDefenders, Mvp, 10, 20)
	}

	updatedParticipants := aParticipants
	updatedParticipants = append(updatedParticipants, dParticipants...)
	updateParticipants(updatedParticipants, battleLog)

	mvpName, err := toDisplayName(Mvp.Tuid)
	if err != nil {
		return errors.New(fmt.Sprint("failed to get mvp display name: %v", err))
	}
	mvpClass := Mvp.Class

	winningChannelName, err := toDisplayName(winnerTuid)
	if err != nil {
		return errors.New(fmt.Sprint("failed to get winning channel display name: %v", err))
	}

	time.Sleep(10 * time.Second)

	// send battle result pubsub
	err = sendBattleResultPubsub(battleItem.Defender, winningChannelName, winnerTuid, mvpName, mvpClass)
	if err != nil {
		return errors.New(fmt.Sprint("failed to send battle result pubsub: %v", err))
	}
	return nil
}

func updatePeople(participants []Character, nonExistingParticipants map[string]bool, Mvp Character, points int, MvpPoints int) []Character {
	var updated []Character
	for _, p := range participants {
		if _, ok := nonExistingParticipants[p.Tuid]; ok {
			continue
		}

		if p.Tuid == Mvp.Tuid {
			fmt.Println("Mvp is: ", p, "Points he has is: ", p.Points)
			p.Points += MvpPoints
			fmt.Println("Points he has now since he is Mvp : ", p.Points)
		} else {
			fmt.Println("User ", p.Tuid, " had : ", p.Points, " will get ", points, " more now")
			p.Points += points
			fmt.Println("User ", p.Tuid, " now has : ", p.Points)
		}
		updated = append(updated, p)
	}
	return updated
}

func doBattle(attackerChannel string, defenderChannel string, defenders []Character, attackers []Character) (string, Character, map[string]string) {
	aliveDefenderCount := len(defenders)
	aliveAttackerCount := len(attackers)
	liveDefenders := defenders
	liveAttackers := attackers
	maxDamage := 0
	var mvp Character

	DamageMap := make(map[string]int)
	battleLog := make(map[string]string)

	rand.Seed(time.Now().Unix())
	for aliveDefenderCount > 0 && aliveAttackerCount > 0 {
		attackerPos := rand.Intn(len(liveAttackers))
		defenderPos := rand.Intn(len(liveDefenders))
		attacker := liveAttackers[attackerPos]
		defender := liveDefenders[defenderPos]

		fmt.Println("Attacker: ", attacker)
		fmt.Println("Defender: ", defender)

		attackerDamageReceived := 0
		if attacker.Speed != 0 {
			attackerDamageReceived = 2 * (defender.Attack - attacker.Defense) * (defender.Speed / attacker.Speed)
		} else {
			attackerDamageReceived = 2 * (defender.Attack - attacker.Defense)
		}

		if attackerDamageReceived < 1 {
			attackerDamageReceived = 1
		}

		defenderDamageReceived := 0
		if attacker.Speed != 0 {
			defenderDamageReceived = 2 * (attacker.Attack - defender.Defense) * (attacker.Speed / defender.Speed)
		} else {
			defenderDamageReceived = 2 * (attacker.Attack - defender.Defense)
		}

		if defenderDamageReceived < 1 {
			defenderDamageReceived = 1
		}

		attackerHealthResult := attacker.Health - attackerDamageReceived
		defenderHealthResult := defender.Health - defenderDamageReceived

		if attackerHealthResult <= 0 && defenderHealthResult <= 0 {
			if attacker.Speed > defender.Speed {
				attackerDamageReceived = 0
				defender.Health = defenderHealthResult
				log := fmt.Sprintf("You dealt (%s) [%s] {%d} damage;", defender.Class, defender.Tuid, defenderDamageReceived)
				battleLog[attacker.Tuid] = battleLog[attacker.Tuid] + log
			} else if defender.Speed > attacker.Speed {
				attacker.Health = attackerHealthResult
				defenderDamageReceived = 0
				log := fmt.Sprintf("You dealt (%s) [%s] {%d} damage;", attacker.Class, attacker.Tuid, attackerDamageReceived)
				battleLog[defender.Tuid] = battleLog[defender.Tuid] + log
			} else {
				if attacker.Luck > defender.Luck {
					attackerDamageReceived = 0
					defender.Health = defenderHealthResult
					log := fmt.Sprintf("You dealt (%s) [%s] {%d} damage;", defender.Class, defender.Tuid, defenderDamageReceived)
					battleLog[attacker.Tuid] = battleLog[attacker.Tuid] + log
				} else {
					attacker.Health = attackerHealthResult
					defenderDamageReceived = 0
					log := fmt.Sprintf("You dealt (%s) [%s] {%d} damage;", attacker.Class, attacker.Tuid, attackerDamageReceived)
					battleLog[defender.Tuid] = battleLog[defender.Tuid] + log
				}
			}
		} else {
			defender.Health = defenderHealthResult
			attacker.Health = attackerHealthResult
			log := fmt.Sprintf("You dealt (%s) [%s] {%d} damage;", defender.Class, defender.Tuid, defenderDamageReceived)
			battleLog[attacker.Tuid] = battleLog[attacker.Tuid] + log

			log = fmt.Sprintf("You dealt (%s) [%s] {%d} damage;", attacker.Class, attacker.Tuid, attackerDamageReceived)
			battleLog[defender.Tuid] = battleLog[defender.Tuid] + log

		}

		liveAttackers[attackerPos] = attacker
		liveDefenders[defenderPos] = defender

		if attacker.Health <= 0 {
			log := fmt.Sprintf("(%s) [%s] killed you;", defender.Class, defender.Tuid)

			battleLog[attacker.Tuid] = battleLog[attacker.Tuid] + log
			aliveAttackerCount--
			liveAttackers = remove(liveAttackers, attackerPos)
		}
		if defender.Health <= 0 {
			log := fmt.Sprintf("(%s) [%s] killed you;", attacker.Class, attacker.Tuid)

			battleLog[defender.Tuid] = battleLog[defender.Tuid] + log
			aliveDefenderCount--
			liveDefenders = remove(liveDefenders, defenderPos)
		}

		DamageMap[attacker.Tuid] = DamageMap[attacker.Tuid] + defenderDamageReceived
		DamageMap[defender.Tuid] = DamageMap[defender.Tuid] + attackerDamageReceived

		if maxDamage < DamageMap[attacker.Tuid] {
			maxDamage = DamageMap[attacker.Tuid]
			mvp = attacker
		}
		if maxDamage < DamageMap[defender.Tuid] {
			maxDamage = DamageMap[defender.Tuid]
			mvp = defender
		}
		fmt.Println("Attackers left: ", aliveAttackerCount)
		fmt.Println("Defenders left: ", aliveDefenderCount)
	}
	winner := defenderChannel
	if aliveAttackerCount > 0 {
		winner = attackerChannel
	}
	return winner, mvp, battleLog
}

func remove(characters []Character, i int) []Character {
	characters[len(characters)-1], characters[i] = characters[i], characters[len(characters)-1]
	return characters[:len(characters)-1]
}

func updateParticipants(updatedParticipants []Character, battleLog map[string]string) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		return
	}
	svc := dynamodb.New(sess)

	for _, participant := range updatedParticipants {
		fmt.Println("battleLog:", participant.Tuid, battleLog[participant.Tuid])
		if battleLog[participant.Tuid] == "" {
			battleLog[participant.Tuid] = "You did not battle"
		}
		bLog := dynamodb.AttributeValue{
			S: aws.String(battleLog[participant.Tuid]),
		}
		log := []*dynamodb.AttributeValue{&bLog}
		emptyList := []*dynamodb.AttributeValue{}
		input := &dynamodb.UpdateItemInput{
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":p": {
					N: aws.String(strconv.Itoa(participant.Points)),
				},
				/*":b": {
					SS: []*string{
						aws.String(battleLog[participant.Tuid]),
					},
				},*/
				":b": {
					L: log,
				},
				":empty_list": {
					L: emptyList,
				},
			},
			ExpressionAttributeNames: map[string]*string{
				"#BL": aws.String("battle-logs"),
			},
			TableName: aws.String("sd-player-stats"),
			Key: map[string]*dynamodb.AttributeValue{
				"tuid": {
					S: aws.String(participant.Tuid),
				},
			},

			ReturnValues:     aws.String("UPDATED_NEW"),
			UpdateExpression: aws.String("SET points = :p, #BL= list_append(if_not_exists(#BL, :empty_list), :b)"),
		}

		_, err = svc.UpdateItem(input)
		if err != nil {
			fmt.Println("Update error %v", err)
		}
	}
	return
}

func updateBattleStatus(battleId string) error {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		return errors.New(fmt.Sprint("failed to create session: %v", err))
	}
	svc := dynamodb.New(sess)
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":r": {
				S: aws.String("FINISHED"),
			},
		},
		TableName: aws.String("sd-battles"),
		Key: map[string]*dynamodb.AttributeValue{
			"battleid": {
				S: aws.String(battleId),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set is_battling = :r"),
	}

	_, err = svc.UpdateItem(input)
	return err
}

func batchGetParticipants(users []string) ([]Character, map[string]bool, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		return nil, nil, errors.New(fmt.Sprint("failed to create session: %v", err))
	}
	svc := dynamodb.New(sess)

	var participants []Character
	nonExistingUsers := make(map[string]bool)
	for _, userTuid := range users {
		result, err := svc.GetItem(&dynamodb.GetItemInput{
			TableName: aws.String("sd-player-stats"),
			Key: map[string]*dynamodb.AttributeValue{
				"tuid": {
					S: aws.String(userTuid),
				},
			},
		})
		if err != nil {
			continue
		}
		if result.Item == nil {
			peasant := Character{
				Attack:  2,
				Class:   "peasant",
				Defense: 2,
				Health:  2,
				Luck:    2,
				Points:  0,
				Speed:   2,
				Tuid:    userTuid,
			}
			nonExistingUsers[userTuid] = true
			participants = append(participants, peasant)
			continue
		}
		characterItem := Character{}
		err = dynamodbattribute.UnmarshalMap(result.Item, &characterItem)
		if err != nil {
			continue
		}
		participants = append(participants, characterItem)
	}
	return participants, nonExistingUsers, nil
}

func toDisplayName(tuid string) (string, error) {
	url := fmt.Sprintf("https://api.twitch.tv/kraken/users/%s", tuid)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Client-ID", "kxn32no54tlu6xpq4hzjuz6m8kuaj7")
	req.Header.Set("Accept", "application/vnd.twitchtv.v5+json")
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	var parsed map[string]interface{}
	decoder := json.NewDecoder(resp.Body)
	decoder.UseNumber()
	decoder.Decode(&parsed)
	if err != nil {
		return "", err
	}

	return parsed["display_name"].(string), nil
}

func sendBattleResultPubsub(defendingChannel string, winningChannelName string, winningChannelTuid string, mvpName string, mvpClass string) error {
	message, _ := json.Marshal(BattleResultPubsubMessage{
		PubsubType:         BattleResultPubsubType,
		WinningChannelName: winningChannelName,
		WinningChannelTuid: winningChannelTuid,
		MVPName:            mvpName,
		MVPClass:           mvpClass,
	})

	err := sendPubsub(defendingChannel, message)
	return err
}

func sendPubsub(recipientChannel string, message []byte) error {
	payload := PubsubPayload{
		ContentType: ApplicationJson,
		Message:     string(message),
		Targets:     []string{"broadcast"},
	}

	jsonValue, _ := json.Marshal(payload)
	log.Printf(string(jsonValue))

	jwt, err := createJWT(recipientChannel)
	if err != nil {
		return err
	}

	log.Printf(jwt)

	url := fmt.Sprintf("https://api.twitch.tv/extensions/message/%s", recipientChannel)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", jwt))
	req.Header.Set("Client-Id", ClientId)
	req.Header.Set("Content-Type", ApplicationJson)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))

	return err
}

// Helper method to construct a JWT
func createJWT(userID string) (string, error) {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":          time.Now().Add(time.Minute * 30).Unix(),
		"user_id":      userID,
		"role":         "external",
		"channel_id":   userID,
		"pubsub_perms": PubsubPerms{Send: []string{"broadcast"}},
	})

	log.Printf("%v", token)

	base64EncodedSecret, _ := base64.StdEncoding.DecodeString(OverlaySecret)

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(base64EncodedSecret)
	if err != nil {
		log.Printf("%v", err)
		return "", err
	}
	log.Printf(tokenString)

	return tokenString, nil
}

func main() {
	lambda.Start(Handler)
}
