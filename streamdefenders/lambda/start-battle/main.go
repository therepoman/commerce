package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	lambdaclient "github.com/aws/aws-sdk-go/service/lambda"
	"github.com/dgrijalva/jwt-go"
)

var (
	ClientId               = "54im47rix9ogj7685q5e2rhjc0dpoz"
	OverlaySecret          = "jYH5QHNNcyKQKBJayn0LzDAp1uStzLX0xcj80Xwigps="
	ApplicationJson        = "application/json"
	BattleStartPubsubType  = "battleStart"
	BattleResultPubsubType = "battleResult"
	AsyncInvocationType    = "Event"
)

type CalcBattleRequest struct {
	BattleId string `json:"battle_id"`
}

type BattleInfo struct {
	BattleId              string `json:"battleid"`
	Attacker              string `json:"attacker"`
	Defender              string `json:"defender"`
	AttackingParticipants string `json:"attacking_participants"`
	DefendingParticipants string `json:"defending_participants"`
	IsBattling            string `json:"is_battling"`
}

type GetParticipantsResponse struct {
	Participants []string `json:"participants"`
}

type GetParticipantsRequest struct {
	Channel string `json:"channel"`
}

type StartBattleRequestBody struct {
	DefendingChannelName string `json:"defending_channel"`
	AttackingChannelName string `json:"attacking_channel"`
}

type BattleResultPubsubMessage struct {
	PubsubType         string `json:"pubsub_type"`
	WinningChannelName string `json:"winning_channel_name"`
	WinningChannelTuid string `json:"winning_channel_tuid"`
	MVPName            string `json:"mvp_name"`
	MVPClass           string `json:"mvp_class"`
}

type BattleStartPubsubMessage struct {
	PubsubType           string `json:"pubsub_type"`
	AttackingChannelName string `json:"attacking_channel"`
	NumAttackers         int    `json:"num_attackers"`
	NumDefenders         int    `json:"num_defenders"`
}

type PubsubPerms struct {
	Send []string `json:"send"`
}

type PubsubPayload struct {
	ContentType string   `json:"content_type"`
	Message     string   `json:"message"`
	Targets     []string `json:"targets"`
}

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Processing Lambda request: %s\n", request.RequestContext.RequestID)
	log.Printf("Body: %s", request.Body)

	// Set random seed
	rand.Seed(time.Now().Unix())

	// start up aws session
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)

	log.Printf("%s", request.Body)
	var requestBody = &StartBattleRequestBody{}
	err = json.Unmarshal([]byte(request.Body), requestBody)
	if err != nil {
		errorMsg := fmt.Sprint("failed to parse request: ", err)
		return events.APIGatewayProxyResponse{StatusCode: 401, Body: errorMsg}, errors.New(errorMsg)
	}

	// verify inputs
	if requestBody == nil || requestBody.DefendingChannelName == "" {
		errorMsg := fmt.Sprint("No defending channel name provided")
		return events.APIGatewayProxyResponse{StatusCode: 401, Body: errorMsg}, errors.New(errorMsg)
	}
	if requestBody == nil || requestBody.AttackingChannelName == "" {
		errorMsg := fmt.Sprint("No attacking channel name provided")
		return events.APIGatewayProxyResponse{StatusCode: 401, Body: errorMsg}, errors.New(errorMsg)
	}

	attackingChannelName := requestBody.AttackingChannelName
	defendingChannelName := requestBody.DefendingChannelName

	defendingChannelTuid, err := toTuid(defendingChannelName)
	if err != nil {
		errorMsg := fmt.Sprint("failed to defending channel's tuid: ", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}
	attackingChannelTuid, err := toTuid(attackingChannelName)
	if err != nil {
		errorMsg := fmt.Sprint("failed to attacking channel's tuid: ", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}

	// get attackers from battle table
	battleInfo, err := getBattleInfoByAttacker(attackingChannelTuid, sess)
	if err != nil {
		errorMsg := fmt.Sprint("failed to get battle info: ", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}
	log.Printf("attackers: %v", strings.Split(battleInfo.AttackingParticipants, ","))
	// build map of attackers
	attackMap := map[string]bool{}
	for _, tuid := range strings.Split(battleInfo.AttackingParticipants, ",") {
		attackMap[tuid] = true
	}
	numAttackers := len(attackMap)
	log.Printf("attackers: %v", attackMap)

	// get tuids in defending channel
	participants, err := getParticipants(defendingChannelName, sess)
	if err != nil {
		errorMsg := fmt.Sprint("failed to get users in defending channel: ", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}
	log.Printf("tuids in defending channel: %v", participants)

	// sort out attacking tuids
	defenders := []string{}
	for _, tuid := range participants {
		if !attackMap[tuid] {
			defenders = append(defenders, tuid)
		}
	}
	log.Printf("defenders: %v", defenders)

	numDefenders := len(defenders)
	if numDefenders <= 0 {
		errorMsg := fmt.Sprint("Invalid battle, no one to defend stream")
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}

	// set battleid info into dynamo
	err = setBattleInfo(attackingChannelTuid, battleInfo.AttackingParticipants, defendingChannelTuid, strings.Join(defenders, ","), battleInfo.BattleId, sess)
	if err != nil {
		errorMsg := fmt.Sprint("failed to update battle info: ", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}

	// send start battle pubsub
	err = sendBattleStartPubsub(attackingChannelName, defendingChannelTuid, numAttackers, numDefenders)
	if err != nil {
		errorMsg := fmt.Sprint("failed to send battleStart pubsub", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}

	startBattleCalc(battleInfo.BattleId, sess)
	if err != nil {
		errorMsg := fmt.Sprint("failed to start battle calculations", err)
		return events.APIGatewayProxyResponse{StatusCode: 500, Body: errorMsg}, errors.New(errorMsg)
	}
	/* Now being done in seperate lambda

	// simulate battle
	// for now defender always wins, and mvp is an assassin
	mvpName, err := toDisplayName(defenders[rand.Intn(len(defenders))])
	if err != nil {
		return events.APIGatewayProxyResponse{}, errors.New(fmt.Sprint("failed to get mvp display name: %v", err))
	}
	mvpClass := "assassin"

	winningChannelName := defendingChannelName
	winningChannelTuid := defendingChannelTuid

	// sleep
	time.Sleep(10 * time.Second)

	// update battleid info into dynamo, update characer info

	// send battle result pubsub
	err = sendBattleResultPubsub(defendingChannelTuid, winningChannelName, winningChannelTuid, mvpName, mvpClass)
	if err != nil {
		return events.APIGatewayProxyResponse{}, errors.New(fmt.Sprint("failed to send battle result pubsub: %v", err))
	}
	*/

	return events.APIGatewayProxyResponse{
		Body:       "Success",
		StatusCode: 200,
	}, nil
}

func sendBattleResultPubsub(defendingChannel string, winningChannelName string, winningChannelTuid string, mvpName string, mvpClass string) error {
	message, _ := json.Marshal(BattleResultPubsubMessage{
		PubsubType:         BattleResultPubsubType,
		WinningChannelName: winningChannelName,
		WinningChannelTuid: winningChannelTuid,
		MVPName:            mvpName,
		MVPClass:           mvpClass,
	})

	err := sendPubsub(defendingChannel, message)
	return err
}

func sendBattleStartPubsub(attackingChannel string, defendingChannel string, numAttackers int, numDefenders int) error {
	message, _ := json.Marshal(BattleStartPubsubMessage{
		PubsubType:           BattleStartPubsubType,
		AttackingChannelName: attackingChannel,
		NumAttackers:         numAttackers,
		NumDefenders:         numDefenders,
	})

	err := sendPubsub(defendingChannel, message)
	return err
}

func sendPubsub(recipientChannel string, message []byte) error {
	payload := PubsubPayload{
		ContentType: ApplicationJson,
		Message:     string(message),
		Targets:     []string{"broadcast"},
	}

	jsonValue, _ := json.Marshal(payload)
	log.Printf(string(jsonValue))

	jwt, err := createJWT(recipientChannel)
	if err != nil {
		return err
	}

	log.Printf(jwt)

	url := fmt.Sprintf("https://api.twitch.tv/extensions/message/%s", recipientChannel)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", jwt))
	req.Header.Set("Client-Id", ClientId)
	req.Header.Set("Content-Type", ApplicationJson)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))

	return err
}

// Helper method to construct a JWT
func createJWT(userID string) (string, error) {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":          time.Now().Add(time.Minute * 30).Unix(),
		"user_id":      userID,
		"role":         "external",
		"channel_id":   userID,
		"pubsub_perms": PubsubPerms{Send: []string{"broadcast"}},
	})

	log.Printf("%v", token)

	base64EncodedSecret, _ := base64.StdEncoding.DecodeString(OverlaySecret)

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(base64EncodedSecret)
	if err != nil {
		log.Printf("%v", err)
		return "", err
	}
	log.Printf(tokenString)

	return tokenString, nil
}

func toTuid(username string) (string, error) {
	url := fmt.Sprintf("https://api.twitch.tv/kraken/users/%s", username)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Client-ID", "kxn32no54tlu6xpq4hzjuz6m8kuaj7")
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	var parsed map[string]interface{}
	decoder := json.NewDecoder(resp.Body)
	decoder.UseNumber()
	decoder.Decode(&parsed)
	if err != nil {
		return "", err
	}
	return parsed["_id"].(json.Number).String(), nil
}

func toDisplayName(tuid string) (string, error) {
	url := fmt.Sprintf("https://api.twitch.tv/kraken/users/%s", tuid)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Client-ID", "kxn32no54tlu6xpq4hzjuz6m8kuaj7")
	req.Header.Set("Accept", "application/vnd.twitchtv.v5+json")
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	var parsed map[string]interface{}
	decoder := json.NewDecoder(resp.Body)
	decoder.UseNumber()
	decoder.Decode(&parsed)
	if err != nil {
		return "", err
	}

	return parsed["display_name"].(string), nil
}

func getParticipants(channelName string, sess *session.Session) ([]string, error) {
	client := lambdaclient.New(sess, &aws.Config{Region: aws.String("us-west-2")})
	getParticipantsRequest := GetParticipantsRequest{Channel: channelName}
	payload, err := json.Marshal(getParticipantsRequest)
	if err != nil {
		return []string{}, err
	}
	result, err := client.Invoke(&lambdaclient.InvokeInput{FunctionName: aws.String("sd-get-participants"), Payload: payload})
	if err != nil {
		return []string{}, err
	}
	getParticipantsResponse := &GetParticipantsResponse{}
	err = json.Unmarshal(result.Payload, getParticipantsResponse)
	log.Printf("getParticipants response: %v", *getParticipantsResponse)
	if err != nil {
		return []string{}, err
	}

	return getParticipantsResponse.Participants, nil
}

func startBattleCalc(battleId string, sess *session.Session) error {
	client := lambdaclient.New(sess, &aws.Config{Region: aws.String("us-west-2")})
	calcBattleRequest := CalcBattleRequest{BattleId: battleId}
	payload, err := json.Marshal(calcBattleRequest)
	if err != nil {
		return err
	}
	_, err = client.Invoke(&lambdaclient.InvokeInput{FunctionName: aws.String("sd-do-battle"), Payload: payload, InvocationType: &AsyncInvocationType})
	if err != nil {
		return err
	}
	return err
}

//get row from sd-battles
func getBattleInfoByAttacker(attackingChannelTuid string, sess *session.Session) (BattleInfo, error) {
	// Create DynamoDB client
	svc := dynamodb.New(sess)

	result, err := svc.Query(
		&dynamodb.QueryInput{
			TableName: aws.String("sd-battles"),
			IndexName: aws.String("attacker-is_battling-index"),
			KeyConditions: map[string]*dynamodb.Condition{
				"attacker": {
					ComparisonOperator: aws.String("EQ"),
					AttributeValueList: []*dynamodb.AttributeValue{{
						S: aws.String(attackingChannelTuid),
					}},
				},
				"is_battling": {
					ComparisonOperator: aws.String("EQ"),
					AttributeValueList: []*dynamodb.AttributeValue{{
						S: aws.String("READY"),
					}},
				},
			},
		},
	)
	if err != nil {
		return BattleInfo{}, err
	}
	if result == nil || result.Items == nil || len(result.Items) <= 0 {
		return BattleInfo{}, errors.New("attacking channel doesn't have ready entry in battle table")
	}

	log.Printf("sd_battle results: %s", result.String())
	battleInfo := &BattleInfo{}
	err = dynamodbattribute.UnmarshalMap(result.Items[0], battleInfo)
	if err != nil {
		return BattleInfo{}, err
	}

	return *battleInfo, nil
}

func setBattleInfo(attacker, attacking, defender, defending, battleId string, sess *session.Session) error {
	// Create DynamoDB client
	svc := dynamodb.New(sess)

	input := &dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			"battleid": {
				S: aws.String(battleId),
			},
			"defender": {
				S: aws.String(defender),
			},
			"defending_participants": {
				S: aws.String(defending),
			},
			"attacker": {
				S: aws.String(attacker),
			},
			"attacking_participants": {
				S: aws.String(attacking),
			},
			"is_battling": {
				S: aws.String("true"),
			},
		},
		TableName: aws.String("sd-battles"),
	}

	log.Printf("dynamo set input: %v", input)

	_, err := svc.PutItem(input)
	return err
}

func main() {
	lambda.Start(Handler)
}
