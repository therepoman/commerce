const jwt = require('jsonwebtoken');
const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB.DocumentClient();;
const TableName = 'sd-player-stats';

exports.handler = (event, context, callback) => {
    const token = (event.headers.authorization || event.headers.Authorization).replace(' ', '');
    try {
        const parsedJwt = jwt.verify(token, Buffer.from(process.env.SECRET, 'base64'));
        const { user_id, opaque_user_id } = parsedJwt;
        const params = {
            TableName,
            Key: {
                'tuid': user_id,
            }
        };
        if (user_id) {
            console.log('getting character for user ', user_id);
            dynamodb.get(params, (err, data) => {
                if (err) {
                    console.log(err);
                    return callback(null, {
                        statusCode: 502,
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                        },
                        body: JSON.stringify(err)
                    });
                }
                callback(null, {
                    statusCode: 200,
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                    body: JSON.stringify(data)
                });
            });
        } else {
            console.log('user not found', opaque_user_id);
            callback(null, {
                statusCode: 404,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
                body: JSON.stringify({ message: 'User not found', code: 'ID_SHARE_REQUIRED' })
            });
        }
    } catch (e) {
        console.log('unauthorized error', e);
        callback(null, {
            statusCode: 403,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({ message: 'Unauthorized' })
        });
    }
};
