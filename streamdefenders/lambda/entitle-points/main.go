package main

import (
	"encoding/json"
	"log"

	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/dgrijalva/jwt-go"
	"strconv"
)

var Catalog = map[string]int{
	"10p":  10,
	"25p":  25,
	"150p": 150,
}

type EntitlePointsRequest struct {
	SKU string `json:"sku"`
}

type SdPlayerStats struct {
	Tuid     string `json:"tuid"`
	Attack   int64  `json:"attack"`
	Class    string `json:"class"`
	Defense  int64  `json:"defense"`
	Health   int64  `json:"health"`
	Luck     int64  `json:"luck"`
	OpaqueId string `json:"opaque-id"`
	Points   string `json:"points"`
	Speed    int64  `json:"speed"`
}

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Processing Lambda request: %s\n", request.RequestContext.RequestID)
	log.Printf("Body: %s\n", request.Body)
	log.Printf("Method: %s\n", request.HTTPMethod)

	if request.HTTPMethod == "OPTIONS" {
		return events.APIGatewayProxyResponse{
			Body: "",
			Headers: map[string]string{
				"Access-Control-Allow-Headers": "*",
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Allow-Methods": "*",
			},
			StatusCode: 200,
		}, nil
	}

	var requestBody = &EntitlePointsRequest{}
	err := json.Unmarshal([]byte(request.Body), requestBody)
	if err != nil {
		log.Printf("error unmarshalling body: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error unmarshalling body: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	jwtRaw := request.Headers["Authorization"]
	parser := &jwt.Parser{}
	claims := jwt.MapClaims{}
	_, _, err = parser.ParseUnverified(jwtRaw, &claims)
	if err != nil {
		log.Printf("error decoding jwt: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error decoding jwt: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	if claims["user_id"] == "" {
		log.Printf("jwt missing 'sub' header")
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("jwt missing 'sub' header"),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	tuid := claims["user_id"].(string)

	if requestBody.SKU == "" {
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("Missing SKU to entitle."),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		log.Printf("error setting up ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error settingup ddb: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	pointsAmount, ok := Catalog[requestBody.SKU]
	if !ok {
		log.Printf("no such SKU: %s", requestBody.SKU)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("no such SKU: %s", requestBody.SKU),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	result, err := svc.UpdateItem(
		&dynamodb.UpdateItemInput{
			TableName: aws.String("sd-player-stats"),
			Key: map[string]*dynamodb.AttributeValue{
				"tuid": {
					S: aws.String(tuid),
				},
			},
			UpdateExpression: aws.String("Set #P = #P + :p"),
			ExpressionAttributeNames: map[string]*string{
				"#P": aws.String("points"),
			},
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":p": {
					N: aws.String(strconv.Itoa(pointsAmount)),
				},
			},
			ReturnValues: aws.String("ALL_NEW"),
		},
	)
	if err != nil {
		log.Printf("error querying ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error querying ddb: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	log.Printf("%s", result.Attributes)
	playerstats := &SdPlayerStats{}
	err = dynamodbattribute.UnmarshalMap(result.Attributes, playerstats)
	output, err := json.Marshal(playerstats)
	if err != nil {
		log.Printf("error marshalling response: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error marshalling response: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	return events.APIGatewayProxyResponse{
		Body: string(output),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		},
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
