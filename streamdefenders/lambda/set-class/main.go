package main

import (
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
    "github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"strconv"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var baseStatCards = map[string]BaseStatCard {
	"peasant": {
		Attack: 5,
		Defense: 5,
		Health: 15,
		Luck: 5,
		Speed: 5,
	},
	"archer": {
		Attack: 6,
		Defense: 4,
		Health: 14,
		Luck: 5,
		Speed: 10,
	},
	"bulwark": {
		Attack: 1,
		Defense: 2,
		Health: 25,
		Luck: 2,
		Speed: 4,
	},
	"assassin": {
		Attack: 2,
		Defense: 0,
		Health: 2,
		Luck: 1,
		Speed: 1,
	},
}

type BaseStatCard struct {
	Attack int `json:"attack"`
	Defense int `json:"defense"`
	Health int `json:"health"`
	Luck int `json:"luck"`
	Speed int `json:"speed"`
}

type SetClass struct {
	Class string `json:"class"`
}

type JwtPayload struct {
	sub string `json:"sub"`
}

type SdPlayerStats struct {
	Tuid     string `json:"tuid"`
	Attack   int64  `json:"attack"`
	Class    string `json:"class"`
	Defense  int64  `json:"defense"`
	Health   int64  `json:"health"`
	Luck     int64  `json:"luck"`
	OpaqueId string `json:"opaque-id"`
	Points   string `json:"points"`
	Speed    int64  `json:"speed"`
}

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Processing Lambda request: %s\n", request.RequestContext.RequestID)
	log.Printf("Body: %s\n", request.Body)
	var requestBody = &SetClass{}
	err := json.Unmarshal([]byte(request.Body), requestBody)
	log.Printf("Class: %s\n", requestBody.Class)
	if err != nil {
		log.Printf("error unmarshalling body: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error unmarshalling body: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	jwtRaw := request.Headers["Authorization"]
	parser := &jwt.Parser{}
	claims := jwt.MapClaims{}
	_, _, err = parser.ParseUnverified(jwtRaw, &claims)
	if err != nil {
		log.Printf("error decoding jwt: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error decoding jwt: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	if claims["user_id"] == "" {
		log.Printf("jwt missing 'user_id' claim")
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("jwt missing 'user_id' claim"),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	tuid := claims["user_id"].(string)

	if requestBody.Class == "" {
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("Missing class to set to"),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		log.Printf("error setting up ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error settingup ddb: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	statCard, ok := baseStatCards[requestBody.Class]
	if !ok {
		log.Printf("no such class: %s", requestBody.Class)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("no such class: %s", requestBody.Class),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	result, err := svc.UpdateItem(
		&dynamodb.UpdateItemInput{
			TableName: aws.String("sd-player-stats"),
			Key: map[string]*dynamodb.AttributeValue{
				"tuid": {
					S: aws.String(tuid),
				},
			},
			AttributeUpdates: map[string]*dynamodb.AttributeValueUpdate {
				"class": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						S: aws.String(requestBody.Class),
					},
				},
				"attack": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						N: aws.String(strconv.Itoa(statCard.Attack)),
					},
				},
				"defense": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						N: aws.String(strconv.Itoa(statCard.Defense)),
					},
				},
				"health": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						N: aws.String(strconv.Itoa(statCard.Health)),
					},
				},
				"luck": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						N: aws.String(strconv.Itoa(statCard.Luck)),
					},
				},
				"speed": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						N: aws.String(strconv.Itoa(statCard.Speed)),
					},
				},
				"points": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						N: aws.String("0"),
					},
				},
				"battle-logs": {
					Action: aws.String("PUT"),
					Value: &dynamodb.AttributeValue{
						L: []*dynamodb.AttributeValue{},
					},
				},
			},
			ReturnValues: aws.String("ALL_NEW"),
		},
	)
	if err != nil {
		log.Printf("error querying ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error querying ddb: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	log.Printf("%s", result.Attributes)
	playerstats := &SdPlayerStats{}
	err = dynamodbattribute.UnmarshalMap(result.Attributes, playerstats)
	output, err := json.Marshal(playerstats)
	if err != nil {
		log.Printf("error marshalling response: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error marshalling response: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	return events.APIGatewayProxyResponse{
		Body: string(output),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		},
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
