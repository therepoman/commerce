package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/aws/aws-lambda-go/lambda"
)

type GroupChattersJson struct {
	Links        GroupChattersJsonLinks    `json:"_links"`
	ChatterCount int                       `json:"chatter_count"`
	Chatters     GroupChattersJsonChatters `json:"chatters"`
}

type GroupChattersJsonLinks struct {
}

type GroupChattersJsonChatters struct {
	Moderators []string `json:"moderators"`
	Staff      []string `json:"staff"`
	Admins     []string `json:"admins"`
	GlobalMods []string `json:"global_mods"`
	Viewers    []string `json:"viewers"`

	Bots       []string `json:"bots,omitempty"`
	Pro        []string `json:"pro,omitempty"`
	Spectators []string `json:"spectators,omitempty"`
}

type MyEvent struct {
	Channel string `json:"channel"`
}

type ParticipantsResponse struct {
	Participants []string `json:"participants"`
}

func Handler(event MyEvent) (ParticipantsResponse, error) {
	host := "https://tmi.twitch.tv/group/user/"
	fmt.Println("event: ", event.Channel)
	channelName := event.Channel
	api := "/chatters"
	resp, err := http.Get(host + channelName + api)

	var participants []string
	if err != nil {
		fmt.Println("Got error getting viewers", err)
		return ParticipantsResponse{participants}, err
	}

	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		fmt.Println("Got error reading response body", err)
		return ParticipantsResponse{participants}, err
	}

	var tmiResponse GroupChattersJson
	if err := json.Unmarshal(body, &tmiResponse); err != nil {
		fmt.Println("Got error unmarshaling response body", err)
		return ParticipantsResponse{participants}, err
	}
	fmt.Println("response from tmi:", tmiResponse)
	for _, value := range tmiResponse.Chatters.Moderators {
		tuid, err := toTuid(value)
		if err == nil {
			participants = append(participants, tuid)
		}
	}

	for _, value := range tmiResponse.Chatters.Staff {
		tuid, err := toTuid(value)
		if err == nil {
			participants = append(participants, tuid)
		}
	}

	for _, value := range tmiResponse.Chatters.Admins {
		tuid, err := toTuid(value)
		if err == nil {
			participants = append(participants, tuid)
		}
	}

	for _, value := range tmiResponse.Chatters.GlobalMods {
		tuid, err := toTuid(value)
		if err == nil {
			participants = append(participants, tuid)
		}
	}

	for _, value := range tmiResponse.Chatters.Viewers {
		tuid, err := toTuid(value)
		if err == nil {
			participants = append(participants, tuid)
		}
	}
	fmt.Println("Returning participants: ", participants)
	return ParticipantsResponse{participants}, err
}

func toTuid(username string) (string, error) {
	url := fmt.Sprintf("https://api.twitch.tv/kraken/users/%s", username)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Client-ID", "kxn32no54tlu6xpq4hzjuz6m8kuaj7")
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	var parsed map[string]interface{}
	decoder := json.NewDecoder(resp.Body)
	decoder.UseNumber()
	decoder.Decode(&parsed)
	if err != nil {
		return "", err
	}
	return parsed["_id"].(json.Number).String(), nil
}

func main() {
	lambda.Start(Handler)
}
