package main

import (
	"encoding/json"
	"errors"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
    "github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"fmt"
)

type IsBattlingRequestBody struct {
	DefendingChannel string `json:"defending_channel"`
}

type SdBattles struct {
	defender     string `json:"defender"`
}

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Processing Lambda request: %s\n", request.RequestContext.RequestID)
	log.Printf("Body: %s", request.Body)
	var requestBody = &IsBattlingRequestBody{}
	err := json.Unmarshal([]byte(request.Body), requestBody)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}

	// verify inputs
	if requestBody == nil || requestBody.DefendingChannel == "" {
		return events.APIGatewayProxyResponse{}, errors.New("No defender channel id provided")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		log.Printf("error setting up ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error settingup ddb: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	result, err := svc.Query(
		&dynamodb.QueryInput{
			TableName: aws.String("sd-battles"),
			IndexName: aws.String("is_battling-defender-index"),
			KeyConditions: map[string]*dynamodb.Condition{
				"defender": {
					ComparisonOperator: aws.String("EQ"),
					AttributeValueList: []*dynamodb.AttributeValue{{
						S: aws.String(requestBody.DefendingChannel),
					}},
				},
				"is_battling": {
					ComparisonOperator: aws.String("EQ"),
					AttributeValueList: []*dynamodb.AttributeValue{{
						S: aws.String("true"),
					}},
				},
			},
		},
	)
	if err != nil {
		log.Printf("error querying ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error querying ddb: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	if *result.Count == 0 {
		return events.APIGatewayProxyResponse{
			Body:       "{\"isBattling\":\"false\"}",
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 200,
		}, nil
	} else {
		return events.APIGatewayProxyResponse{
			Body:       "{\"isBattling\":\"true\"}",
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 200,
		}, nil
	}
}

func main() {
	lambda.Start(Handler)
}
