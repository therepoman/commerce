package main

import (
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
    "github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type SpendPointsRequest struct {
	Attribute string `json:"attribute"`
}

type SdPlayerStats struct {
	Tuid     string `json:"tuid"`
	Attack   int64  `json:"attack"`
	Class    string `json:"class"`
	Defense  int64  `json:"defense"`
	Health   int64  `json:"health"`
	Luck     int64  `json:"luck"`
	OpaqueId string `json:"opaque-id"`
	Points   string `json:"points"`
	Speed    int64  `json:"speed"`
}

// Handler is your Lambda function handler
// It uses Amazon API Gateway request/responses provided by the aws-lambda-go/events package,
// However you could use other event sources (S3, Kinesis etc), or JSON-decoded primitive types such as 'string'.
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// stdout and stderr are sent to AWS CloudWatch Logs
	log.Printf("Processing Lambda request: %s\n", request.RequestContext.RequestID)
	log.Printf("Body: %s\n", request.Body)
	log.Printf("Method: %s\n", request.HTTPMethod)

	// Create DynamoDB client
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	if err != nil {
		log.Printf("error setting up ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error settingup ddb: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	svc := dynamodb.New(sess)

	jwtRaw := request.Headers["Authorization"]
	parser := &jwt.Parser{}
	claims := jwt.MapClaims{}
	_, _, err = parser.ParseUnverified(jwtRaw, &claims)
	if err != nil {
		log.Printf("error decoding jwt: %s", err)
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("error decoding jwt: %s", err),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	if claims["user_id"] == "" {
		log.Printf("jwt missing 'user_id' claim")
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("jwt missing 'user_id' claim"),
			Headers:    map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	tuid := claims["user_id"].(string)

	if request.HTTPMethod == "GET" {
		return get(svc, request, tuid)
	} else if request.HTTPMethod == "POST" {
		return post(svc, request, tuid)
	}

	return events.APIGatewayProxyResponse{
		Body: fmt.Sprintf("method not supported: %s", request.HTTPMethod),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		},
		StatusCode: 500,
	}, nil
}

func post(svc *dynamodb.DynamoDB, request events.APIGatewayProxyRequest, tuid string) (events.APIGatewayProxyResponse, error) {
	log.Printf("%s", request.Body)
	var requestBody = &SpendPointsRequest{}
	err := json.Unmarshal([]byte(request.Body), requestBody)

	cost := "10"
	attr := requestBody.Attribute

	result, err := svc.UpdateItem(
		&dynamodb.UpdateItemInput{
			TableName: aws.String("sd-player-stats"),
			Key: map[string]*dynamodb.AttributeValue{
				"tuid": {
					S: aws.String(tuid),
				},
			},
			ConditionExpression: aws.String("points >= :pointsDec"),
			UpdateExpression: aws.String(fmt.Sprintf("SET %s = %s + :attrInc, points = points - :pointsDec", attr, attr)),
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":attrInc": {
					N: aws.String("1"),
				},
				":pointsDec": {
					N: aws.String(cost),
				},
			},
			ReturnValues: aws.String("ALL_NEW"),
		},
	)
	if err != nil {
		log.Printf("error querying ddb: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error querying ddb: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	playerstats := &SdPlayerStats{}
	err = dynamodbattribute.UnmarshalMap(result.Attributes, playerstats)
	output, err := json.Marshal(playerstats)
	if err != nil {
		log.Printf("error marshalling response: %s", err)
		return events.APIGatewayProxyResponse{
			Body: fmt.Sprintf("error marshalling response: %s", err),
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: 500,
		}, nil
	}

	return events.APIGatewayProxyResponse{
		Body: string(output),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		},
		StatusCode: 200,
	}, nil
}

func get(svc *dynamodb.DynamoDB, request events.APIGatewayProxyRequest, tuid string) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		Body: fmt.Sprintf("not implemented"),
		Headers: map[string]string{
			"Access-Control-Allow-Origin": "*",
		},
		StatusCode: 500,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
