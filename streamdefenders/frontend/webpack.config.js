const path = require('path')
const fs = require('fs')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: {
    viewer: path.resolve(__dirname, './src/viewer/index.js'),
    config: path.resolve(__dirname, './src/config/index.js'),
    live_config: path.resolve(__dirname, './src/live_config/index.js')
  },
  output: {
    filename: '[name].js',
    chunkFilename: '[name].[chunkhash].js',
    path: path.resolve(__dirname, './dist'),
    publicPath: '/'
  },
  module: {
      rules: [
          {
              test: /\.jsx?$/,
              exclude: /(node_modules|bower_components)/,
              use: {
                loader: 'babel-loader',
                options: {
                    presets: ['babel-preset-react', 'babel-preset-env'],
                    plugins: ['transform-object-rest-spread', 'transform-class-properties']
                }
              }
          }, {
              test: /\.(scss|css)$/,
              use: [
                  {
                    loader: MiniCssExtractPlugin.loader
                  }, {
                      loader: 'css-loader',
                      options: {
                        sourceMap: true
                      }
                  }, {
                    loader: 'sass-loader',
                    options: {
                      sourceMap: true
                    }
                  }
              ]
          }, {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
              'file-loader'
            ]
          }
      ]
  },
  resolve: {
    modules: [
      'src',
      'node_modules'
    ]
  },
  plugins: [
      new CopyWebpackPlugin([
        {
            from: path.resolve(__dirname, './src/viewer/index.html'),
            // Change string below to modify the viewer html file name
            to: path.resolve(__dirname, './dist/index.html')
        }, {
            from: path.resolve(__dirname, './src/config/index.html'),
            // Change string below to modify the config html file name
            to: path.resolve(__dirname, './dist/config.html')
        }, {
            from: path.resolve(__dirname, './src/live_config/index.html'),
            // Change string below to modify the loive config html file name
            to: path.resolve(__dirname, './dist/live_config.html')
        },
      ]),
      new MiniCssExtractPlugin({ filename: '[name].css' })
  ],
  devServer: {
      contentBase: path.resolve(__dirname, './dist'),
      publicPath: '/',
      https: true,
      port: 7575 // Port for local extension server
  },
  devtool: 'source-map',
  mode: 'none'
};