const BASE_URL = 'https://g2v8pvkho1.execute-api.us-west-2.amazonaws.com/default/';
const PREFIX = '';

const METHODS = {
    GET: 'GET',
    PUT: 'PUT',
    POST: 'POST',
};

export const request = (method, url, options) => {
    return fetch(`${BASE_URL}${PREFIX}${url}`, {
        method,
        ...options,
    }).then(response => response.json());
}

request.methods = METHODS;