import { request } from '../../../shared/api';

export const CHARACTER_ACTION_TYPES = {
    SET_CHARACTER: 'setCharacter',
    LOADING: 'characterLoading',
    ID_SHARE_REQUIRED: 'idShareRequired',
    IS_BATTLING: 'isBattling'
};

export const isBattling = (defenderId, token) => {
    return (dispatch) => {
        request(request.methods.POST, 'sd-is-battling', {
            headers: {
                authorization: token,
            },
            body: JSON.stringify({ 'defending_channel': defenderId })
        }).then((data) => {
            console.log('isBattling', data);
            dispatch({
                type: CHARACTER_ACTION_TYPES.IS_BATTLING,
                isBattling: data.isBattling === 'true',
            });
        })
        .catch((e) => {
            console.log('isBattling error', e);
        }); 
    }
}

export const spendPoints = (attribute, token) => {
    return (dispatch) => {
        request(request.methods.POST, 'sd-spend-points', {
            headers: {
                authorization: token,
            },
            body: JSON.stringify({ 'attribute': attribute })
        }).then((data) => {
            console.log('spendPoints', data);
            dispatch({
                type: CHARACTER_ACTION_TYPES.SET_CHARACTER,
                character: data,
            });
        })
        .catch((e) => {
            console.log('spendPoints error', e);
        }); 
    }
}

export const getCharacter = (token) => {
    return (dispatch) => {
        dispatch({
            type: CHARACTER_ACTION_TYPES.LOADING,
        });
        request(request.methods.GET, 'sd-get-character', {
            headers: {
                authorization: token,
            },
        }).then((data) => {
            if (data.code === 'ID_SHARE_REQUIRED') {
                return dispatch({
                    type: CHARACTER_ACTION_TYPES.ID_SHARE_REQUIRED,
                });
            }
            dispatch({
                type: CHARACTER_ACTION_TYPES.SET_CHARACTER,
                character: data.Item,
            });
        })
        .catch((e) => {
            dispatch({
                type: CHARACTER_ACTION_TYPES.SET_CHARACTER,
                character: null,
            });
        }); 
    }
}

export const setCharacter = (characterClass, token) => {
    return (dispatch) => {
        dispatch({
            type: CHARACTER_ACTION_TYPES.LOADING,
        });
        request(request.methods.POST, 'sd-set-class', {
            headers: {
                'authorization': token,
            },
            body: JSON.stringify({ 'class': characterClass })
        }).then(data => {
            const character = {
                ...data
            };
            character.class = characterClass;
            console.log('setcharacter', character)
            dispatch({
                type: CHARACTER_ACTION_TYPES.SET_CHARACTER,
                character: character,
            });
        })
        .catch((e) => {
            dispatch({
                type: CHARACTER_ACTION_TYPES.SET_CHARACTER,
                character: null,
            });
        });
    }
}

export const entitlePoints = (sku, token) => {
    return (dispatch) => {
        console.log(JSON.stringify({ 'SKU': sku }));
        request(request.methods.POST, 'ds-entitle-points', {
            headers: {
                'authorization': token,
            },
            body: JSON.stringify({ 'SKU': sku })
        }).then(data => {
            console.log('entitlePoints', data)
            dispatch({
                type: CHARACTER_ACTION_TYPES.SET_CHARACTER,
                character: data,
            });
        })
        .catch((e) => {
            console.log('entitlePointsError', e);
        });
    }
}