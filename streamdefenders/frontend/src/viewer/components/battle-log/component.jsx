import * as React from 'react';
import './styles.scss';

export const BATTLE_LOG_CLASSNAME = 'battle-log';
export const BATTLE_LOG_ITEM_CLASSNAME = 'battle-log__item';
export const BATTLE_LOG_DAMAGE_CLASSNAME = 'battle-log__damage';

export const BattleLog = (props) => {
    const items = props.log.split(';').map((log, index) => {
        let item = log;
        item = item.replace(/\(([^\)]*)\)\s\[([^\]]*)\]/gi, '<a href="http://www.twitch.tv/$2">$1</a>')
        item = item.replace(/{(\d*)}/gi, `<span class="${BATTLE_LOG_DAMAGE_CLASSNAME}">$1</span>`)
        return <div key={`log-item-${index}`} className={BATTLE_LOG_ITEM_CLASSNAME} dangerouslySetInnerHTML={{__html: item}}></div>;
    });
    return (
        <div className={BATTLE_LOG_CLASSNAME}>{items}</div>
    );
}
