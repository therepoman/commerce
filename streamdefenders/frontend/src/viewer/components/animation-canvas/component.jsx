import * as React from 'react';

const PIXEL_DIM = 5;

export class CharacterAnimation extends React.Component {
  constructor(props) {
    super(props);
    this.canvas = null;
    this.animationTimer = null;
    this.currentFrame = props.frame || 0;
  }

  render() {
    const { width, height } = this.props.character;
    const zoomedDims = Math.max(this.props.zoomFactor * PIXEL_DIM, 1);

    return <canvas className={this.props.className} width={width * zoomedDims} height={height * zoomedDims} ref={this.setRef} />;
  }

  componentDidMount() {
    this.startAnimation();
  }

  componentWillUnmount() {
    this.stopAnimation();
  }

  setRef = (ref) => {
    this.canvas = ref ? ref.getContext('2d') : null;
  }

  stopAnimation = () => {
    if (this.animationTimer) {
      clearInterval(this.animationTimer);
      this.animationTimer = null;
    }
  }

  startAnimation = () => {
    const { fps } = this.props.character;
    const timerLength = 1000 / fps;
    if (this.animationTimer) {
      this.stopAnimation();
    }

    this.drawFrame();
    if (this.props.animate === false) return;
    this.animationTimer = setInterval(this.drawFrame, timerLength);
  }

  drawFrame = () => {
    if (!this.canvas) return;

    const { frames, width } = this.props.character;

    if (frames.length === this.currentFrame) {
      if (this.props.loop === false) {
        return this.stopAnimation();
      }
      this.currentFrame = 0;
    }

    frames[this.currentFrame].forEach((color, index) => {
      const x = index%width;
      const y = Math.floor(index/width);
      const zoomedDims = Math.max(this.props.zoomFactor * PIXEL_DIM, 1);

      if (color) {
        this.canvas.fillStyle = `#${color}`;
        this.canvas.fillRect(x * zoomedDims, y * zoomedDims, zoomedDims, zoomedDims);
      } else {
        this.canvas.clearRect(x * zoomedDims, y * zoomedDims, zoomedDims, zoomedDims);
      }
    });
    this.currentFrame++;
  }
}
