import * as React from 'react';
import * as castle from 'shared/animations/castle.json';
import * as trebuchetThrowing from 'shared/animations/trebuchet_throwing.json';
import * as trebuchetRolling from 'shared/animations/trebuchet_rolling.json';
import { CharacterAnimation } from '../animation-canvas';
import './styles.scss';

export const COMBAT_CONTAINER_CLASSNAME = 'combat';
export const CASTLE_CANVAS_CLASSNAME = 'combat__castle-canvas';
export const TREBUCHET_CANVAS_CLASSNAME = 'combat__trebuchet-canvas';

export class Combat extends React.Component {
    render () {
        return (
            <div className={COMBAT_CONTAINER_CLASSNAME}>
                <CharacterAnimation className={CASTLE_CANVAS_CLASSNAME} zoomFactor={this.props.zoomFactor} character={castle} />
                <CharacterAnimation className={TREBUCHET_CANVAS_CLASSNAME} zoomFactor={this.props.zoomFactor} character={trebuchetThrowing} />
            </div>
        );
    }
}