import * as React from 'react';
import { CharacterAnimation } from '../animation-canvas';
import { BattleLog } from '../battle-log';
import { classNames } from 'shared';
import * as assassin from 'shared/animations/assassin.json';
import * as bulwark from 'shared/animations/bulwark.json';
import * as archer from 'shared/animations/archer.json';
import * as peasant from 'shared/animations/peasant.json';
import './styles.scss';

export const EXPAND_ICON_CLASSNAME = 'character-editor__expand-icon';
export const CHARACTER_SELECTOR_CLASSNAME = 'character-editor__character-selector';
export const CHARACTER_SELECTOR_CLASSES_CLASSNAME = 'character-editor__character-selector-classes';
export const CHARACTER_PRODUCTS_CLASSNAME = 'character-editor__character-products';
export const CHARACTER_STATS_CLASSNAME = 'character-editor__character-stats';
export const CHARACTER_CANVAS_CLASSNAME = 'character-editor__character-canvas';
export const CHARACTER_BATTLE_LOGS_CLASSNAME = 'character-editor__battle-logs';

const CHARACTER_MAP = {
  assassin,
  bulwark,
  archer,
  peasant,
};

export class CharacterEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      selectingCharacter: false,
      selectedCharacter: 'peasant',
      purchasingPoints: false,
      showLogs: false,
    };
  }

  render(){
    return (
      <div className={classNames({
        'character-editor': true,
        'character-editor__expanded': this.state.expanded,
      })}>
        <div onClick={this.toggleEditor} className="character-editor__expand">
          { this.state.expanded ? <span className={EXPAND_ICON_CLASSNAME}>&raquo;</span> : <span className={EXPAND_ICON_CLASSNAME}>&laquo;</span> }
        </div>
        <div className="character-editor__container">
          { this._renderEditor() }
        </div>
      </div>
    );
  }

  _renderEditor = () => {
    let characterAnimation = CHARACTER_MAP.peasant;
    if (this.props.character) {
      characterAnimation = CHARACTER_MAP[this.props.character.class];
    }

    if (this.props.loading) {
      return null;
    }

    if (this.state.purchasingPoints) {
      return this._renderProducts();
    }

    if (this.state.showLogs) {
      return this._renderBattleLogs();
    }

    if (this.state.selectingCharacter) {
      return (
        <React.Fragment>
          <div></div>
          <div className={CHARACTER_SELECTOR_CLASSNAME}>
            <CharacterAnimation className={CHARACTER_CANVAS_CLASSNAME} zoomFactor={this.props.zoomFactor} character={CHARACTER_MAP[this.state.selectedCharacter]} />
            <div className={CHARACTER_SELECTOR_CLASSES_CLASSNAME}>
              <button onClick={this._setArcher}>Archer</button>
              <button onClick={this._setAssassin}>Assassin</button>
              <button onClick={this._setBulwark}>Bulwark</button>
              <button onClick={this._setPeasant}>Peasant</button>
            </div>
          </div>
          <div><button onClick={this._saveClass}>Save Class</button></div>
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        { this._renderPoints() }
        <div className={CHARACTER_SELECTOR_CLASSNAME}>
          { this.state.expanded ? <CharacterAnimation className={CHARACTER_CANVAS_CLASSNAME} zoomFactor={this.props.zoomFactor} character={characterAnimation} /> : null }
          { this._renderStats() }
        </div>
        { !this.props.character && !this.props.idShareRequired && <div><button onClick={this._setSelectingCharacter}>Select Class</button></div> }
        { !this.props.character && this.props.idShareRequired  && <div><button onClick={this.props.requestIdShare}>You must share your ID to select a class.</button></div>}
        { this._renderBattleLogs(true) }
      </React.Fragment>
    );
  }

  _renderPoints = (hideButton) => {
    if (!this.props.character || !this.state.expanded) {
      return <div></div>;
    }
    return (
      <div className={CHARACTER_STATS_CLASSNAME}>
        <span>Points:</span>
        <span>{this.props.character.points}</span>
        {hideButton !== true && <button onClick={this._showProducts}>Get more points!</button> }
      </div>
    );
  }

  _renderStats = () => {
    if (!this.props.character || !this.state.expanded) {
      return null;
    }
    return (
      <div className={CHARACTER_SELECTOR_CLASSES_CLASSNAME}>
        <div className={CHARACTER_STATS_CLASSNAME}>
          <span>Attack:</span>
          <span>{this.props.character.attack}</span>
          <button onClick={this._upgradeAttack}>+</button>
        </div>
        <div className={CHARACTER_STATS_CLASSNAME}>
          <span>Speed:</span>
          <span>{this.props.character.speed}</span>
          <button onClick={this._upgradeSpeed}>+</button>
        </div>
        <div className={CHARACTER_STATS_CLASSNAME}>
          <span>Defense:</span>
          <span>{this.props.character.defense}</span>
          <button onClick={this._upgradeDefense}>+</button>
        </div>
        <div className={CHARACTER_STATS_CLASSNAME}>
          <span>Health:</span>
          <span>{this.props.character.health}</span>
          <button onClick={this._upgradeHealth}>+</button>
        </div>
        <div className={CHARACTER_STATS_CLASSNAME}>
          <span>Luck:</span>
          <span>{this.props.character.luck}</span>
          <button onClick={this._upgradeLuck}>+</button>
        </div>
      </div>
    );
  }

  _renderProducts = () => {
    if (!this.props.products) {
      return null;
    }
    return (
      <div className={CHARACTER_PRODUCTS_CLASSNAME}>
        <div className={CHARACTER_SELECTOR_CLASSES_CLASSNAME}>
          <button onClick={this._hideProducts}>Back</button>
          {this._renderPoints(true)}
          {this.props.products.map(product => {
            return <button onClick={this.props.useBits.bind(this, product.sku)} key={product.sku}>{`Get ${product.displayName} for ${product.cost.amount} ${product.cost.type}.`}</button>;
          })}
        </div>
      </div>
    );
  }

  _renderBattleLogs = (buttonOnly) => {
    let logs = <div>No battle logs. Stay around for a Raid to join the fight.</div>;
    if (!this.props.character) {
      return null;
    }

    if (buttonOnly) {
      if (this.props.character['battle-logs'] && this.props.character['battle-logs'].length > 0) {
        return <div><button onClick={this._showLogs}>View battle logs</button></div>
      } else {
        return <div>No battle logs. Stay around for a Raid to join the fight.</div>;
      }
    }

    if (this.props.character['battle-logs'] && this.props.character['battle-logs'].length > 0) {
      logs = this.props.character['battle-logs'].map((log, index) => {
        return <BattleLog key={`log-${index}`} log={log} />
      });
    }
    return (
      <div className={CHARACTER_BATTLE_LOGS_CLASSNAME}>
        <button onClick={this._hideLogs}>Back</button>
        { logs }
      </div>
    );
  }

  toggleEditor = () => {
    this.setState({
      expanded: !this.state.expanded,
    });
  }

  _showLogs = () => {
    this.setState({
      showLogs: true,
    });
  }

  _hideLogs = () => {
    this.setState({
      showLogs: false,
    });
  }

  _showProducts = () => {
    this.setState({
      purchasingPoints: true,
    });
  }

  _hideProducts = () => {
    this.setState({
      purchasingPoints: false,
    });
  }

  _setSelectingCharacter = () => {
    this.setState({
      selectingCharacter: true,
    });
  }

  _setArcher = () => {
    this._setCharacterClass('archer');
  }

  _setAssassin = () => {
    this._setCharacterClass('assassin');
  }

  _setBulwark = () => {
    this._setCharacterClass('bulwark');
  }

  _setPeasant = () => {
    this._setCharacterClass('peasant');
  }

  _setCharacterClass = (characterClass) => {
    this.setState({
      selectedCharacter: characterClass,
    });
  }

  _saveClass = () => {
    this.props.setCharacter(this.state.selectedCharacter, this.props.token);
    this.setState({
      selectingCharacter: false,
    });
  }

  _upgradeAttack = () => {
    this.props.spendPoints('attack', this.props.token);
  }

  _upgradeSpeed = () => {
    this.props.spendPoints('speed', this.props.token);
  }

  _upgradeDefense = () => {
    this.props.spendPoints('defense', this.props.token);
  }

  _upgradeHealth = () => {
    this.props.spendPoints('health', this.props.token);
  }

  _upgradeLuck = () => {
    this.props.spendPoints('luck', this.props.token);
  }

  mergeFrames = (...frames) => {
    const totalFrames = frames.length;
    const mergedFrames = frames[0].frames.map((frame, index) => {
      return frame.map((color, colorIndex) => {
        let final = color;
        for (let i = 1; i < totalFrames; i++) {
          final = frames[i].frames[index][colorIndex] || final;
        }
        return final;
      });
    });

    return {
      ...frames[0],
      frames: mergedFrames,
    };
  }
}
