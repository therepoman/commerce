import * as React from 'react';
import * as shield from 'shared/animations/shield.json';
import { CharacterAnimation } from '../animation-canvas';
import './styles.scss';

export const BATTLE_CONTAINER_CLASSNAME = 'battle-announcement';
export const BATTLE_HEADER_CLASSNAME = 'battle-announcement__header';
export const BATTLE_SUBTITLE_CLASSNAME = 'battle-announcement__subtitle';
export const SHIELD_CANVAS_CLASSNAME = 'battle-announcement__shield-canvas';

export class BattleAnnouncement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: true,
            timer: null,
        };
    }
    render () {
        if (this.state.display === false) {
            return null;
        }
        return (
            <div className={BATTLE_CONTAINER_CLASSNAME}>
                <div className={BATTLE_HEADER_CLASSNAME}>Prepare for battle!</div>
                <CharacterAnimation className={SHIELD_CANVAS_CLASSNAME} zoomFactor={this.props.zoomFactor} character={shield} loop={false} />
                <div className={BATTLE_SUBTITLE_CLASSNAME}>{this.props.message && this.props.message.attacking_channel} has attacked you!</div>
            </div>
        );
    }

    componentDidMount = () => {
        this.setState({
            timer: setTimeout(this._hide, 5000),
        });
    }

    componentWillUnmount = () => {
        if (this.state.timer) {
            clearTimeout(this.state.timer);
        }
    }

    _hide = () => {
        if (this.state.timer) {
            clearTimeout(this.state.timer);
        }
        this.setState({
            display: false,
            timer: null,
        });
    }
}