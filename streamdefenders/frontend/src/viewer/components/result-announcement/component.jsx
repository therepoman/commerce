import * as React from 'react';
import * as assassin from 'shared/animations/assassin.json';
import * as bulwark from 'shared/animations/bulwark.json';
import * as archer from 'shared/animations/archer.json';
import * as peasant from 'shared/animations/peasant.json';
import { CharacterAnimation } from '../animation-canvas';
import './styles.scss';

export const RESULT_CONTAINER_CLASSNAME = 'result-announcement';
export const RESULT_HEADER_CLASSNAME = 'result-announcement__header';
export const RESULT_SUBTITLE_CLASSNAME = 'result-announcement__subtitle';
export const CANVAS_CLASSNAME = 'result-announcement__canvas';

const CHARACTER_MAP = {
    assassin,
    bulwark,
    archer,
    peasant,
};

export class ResultAnnouncement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timer: null,
        };
    }
    render () {
        return (
            <div className={RESULT_CONTAINER_CLASSNAME}>
                <div className={RESULT_HEADER_CLASSNAME}>{this.props.channelId === this.props.message.winning_tuid ? 'You WIN!!!' : 'You LOSE!!!' }</div>
                <CharacterAnimation className={CANVAS_CLASSNAME} zoomFactor={this.props.zoomFactor} character={CHARACTER_MAP[this.props.message.mvp_class] || CHARACTER_MAP.peasant} />
                <div className={RESULT_SUBTITLE_CLASSNAME}>{this.props.message.mvp_name} is the MVP</div>
            </div>
        );
    }

    componentDidMount = () => {
        this.props.getCharacter();
        this.setState({
            timer: setTimeout(this._hide, 5000),
        });
    }

    componentWillUnmount = () => {
        if (this.state.timer) {
            clearTimeout(this.state.timer);
        }

        if (this.props.message && this.props.message.pubsub_type === 'battleResult') {
            this.props.clearMessage();
        }
    }

    _hide = () => {
        if (this.state.timer) {
            clearTimeout(this.state.timer);
        }
        this.props.clearMessage();
    }
}