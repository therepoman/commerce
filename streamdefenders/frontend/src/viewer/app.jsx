import * as React from 'react'
import { CharacterEditor } from './components/character-editor';
import { BattleAnnouncement } from './components/battle-announcement';
import { ResultAnnouncement } from './components/result-announcement';
import { Combat } from './components/combat';
import { classNames } from 'shared';

export const APP_CLASSNAME = 'stream-defender__app';
export const HOVERED_APP_CLASSNAME = 'stream-defender__app--hovered';
export const EVENT_APP_CLASSNAME = 'stream-defender__app--event';


const RESOLUTION_SPLITTER = 'x';
const EDITOR_BASE_HEIGHT = 400; 

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
    };

    this.zoomFactor = 1;
  }

  render() {
    const playerHeight = this.props.displayResolution && this.props.displayResolution.split(RESOLUTION_SPLITTER)[1];
    const zoomFactor = this.zoomFactor = this._calcZoom(playerHeight);
    const editorPixelSize = Math.min(10, Math.max(zoomFactor * 10, 1)); // No less than 1, but no greater than 10. Used for resizing the character editor panel

    return (
      <div style={{ fontSize: `${editorPixelSize}px` }} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} className={classNames({
        [APP_CLASSNAME]: true,
        [HOVERED_APP_CLASSNAME]: this.state.hover,
        [EVENT_APP_CLASSNAME]: this._eventActive(),
      })}>
        { this._eventActive() && <BattleAnnouncement zoomFactor={zoomFactor} message={this.props.messages && this.props.messages.broadcast} /> }
        { this._eventActive() && <Combat zoomFactor={zoomFactor} /> }
        { this._battleComplete() && <ResultAnnouncement getCharacter={this._getCharacter} message={this.props.messages && this.props.messages.broadcast} zoomFactor={zoomFactor} clearMessage={this.props.clearMessage} /> }
        <CharacterEditor
          loading={this.props.loading}
          idShareRequired={this.props.idShareRequired}
          character={this.props.character}
          zoomFactor={zoomFactor}
          requestIdShare={this.props.requestIdShare}
          token={this.props.token}
          setCharacter={this.props.setCharacter}
          products={this.props.bitsProducts}
          useBits={this.props.useBits}
          spendPoints={this.props.spendPoints}
          getCharacter={this._getCharacter}
        />
      </div>
    );
  }

  componentDidMount = () => {
    this.setState({
      shieldTimer: setInterval(this._toggleShield, 10000),
    });
  }

  comoponentWillUnmount = () => {
    if (this.state.shieldTimer) {
      clearInterval(this.state.shieldTimer);
    }
  }

  onMouseEnter = () => {
    this.setState({
      hover: true,
    });
  }

  onMouseLeave = () => {
    this.setState({
      hover: false,
    });
  }

  _getCharacter = () => {
    this.props.getCharacter(this.props.token);
  }

  _calcZoom = (playerHeight) => {
    if (!this.state.hover) return this.zoomFactor;
    if (!playerHeight) return 1;

    const heightRatio = playerHeight  / EDITOR_BASE_HEIGHT;

    return Math.min(heightRatio / 2, 1);
  }

  _eventActive = () => {
    return !!(this.props.isBattling || (this.props.messages && this.props.messages.broadcast && this.props.messages.broadcast.pubsub_type === 'battleStart'));
  }

  _battleComplete = () => {
    return this.props.messages && this.props.messages.broadcast && this.props.messages.broadcast.pubsub_type === 'battleResult';
  }
}
