import { CHARACTER_ACTION_TYPES } from '../../actions';
import { ACTION_TYPES } from '../../../actions';

export const initialState = {
    character: null,
    loading: false,
    idShareRequired: false,
    isBattling: false,
}

export const CharacterStore = (state = initialState, action) => {
    switch(action.type) {
        case CHARACTER_ACTION_TYPES.SET_CHARACTER:
            return {
                ...state,
                character: action.character,
                loading: false,
                idShareRequired: false,
            }
        case CHARACTER_ACTION_TYPES.LOADING:
            return {
                ...state,
                loading: true,
                idShareRequired: false,
            }
        case CHARACTER_ACTION_TYPES.ID_SHARE_REQUIRED:
            return {
                ...state,
                loading: false,
                idShareRequired: true,
                character: null,
            }
        case CHARACTER_ACTION_TYPES.IS_BATTLING:
            return {
                ...state,
                isBattling: action.isBattling,
            }
        case ACTION_TYPES.ON_MESSAGE:
            const msg = JSON.parse(action.message);
            let isBattling = state.isBattling;
            if (msg && msg.broadcast && msg.broadcast.pubsub_type === 'battleResult') {
                isBattling = false;
            }
            return {
                ...state,
                isBattling,
            }
    }

    return state;
}
