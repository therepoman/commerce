import * as ReactDom from 'react-dom'
import * as React from 'react'
import { connect, Provider } from 'react-redux'
import * as Actions from '../actions'
import { getCharacter, setCharacter, entitlePoints, isBattling, spendPoints } from './actions'
import TwitchExtStore, { initialState } from '../store'
import { CharacterStore, initialState as initialCharacterState } from './stores'
import { App } from './app.jsx'
import * as TwitchExt from '../twitch-ext'
import './index.scss'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'

const Store = createStore(combineReducers({
    twitch: TwitchExtStore,
    character: CharacterStore,
}), applyMiddleware(thunkMiddleware))

TwitchExt.onAuthorized((authData) => {
    Store.dispatch(Actions.setAuthorization(authData))
    Store.dispatch(getCharacter(authData.token))
    Store.dispatch(Actions.listen("broadcast"))
    Store.dispatch(Actions.getBitsProducts())
    Store.dispatch(isBattling(authData.channelId, authData.token))
})
TwitchExt.onContext((contextData) => Store.dispatch(Actions.setContext(contextData)))
TwitchExt.onError((error) => Store.dispatch(Actions.setError(error)))
TwitchExt.bits.onTransactionComplete((transaction) => {
    Store.dispatch(Actions.setLatestTransaction(transaction))
    const twitchState = Store.getState().twitch;
    const token = twitchState.authData && twitchState.authData.token;
    Store.dispatch(entitlePoints(transaction.product.sku, token));
})

TwitchExt.bits.onTransactionCancelled(() => {
    console.log('transaction cancelled')
})

const mapStateToProps = (state) => {
    return {
        displayResolution: state.twitch.context.displayResolution || initialState.context.displayResolution,
        character: state.character.character || initialCharacterState.character,
        idShareRequired: state.character.idShareRequired || initialCharacterState.idShareRequired,
        loading: state.character.loading || initialCharacterState.loading,
        messages: state.twitch.messages || {},
        token: state.twitch.authData && state.twitch.authData.token || null,
        bitsProducts: state.twitch.bitsProducts || null,
        isBattling: state.character.isBattling || false,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        listen: target => dispatch(Actions.listen(target)),
        unlisten: target => dispatch(Actions.unlisten(target)),
        send: (target, contentType, message) => dispatch(Actions.send(target, contentType, message)),
        requestIdShare: () => dispatch(Actions.requestIdShare()),
        setCharacter: (characterClass, token) => dispatch(setCharacter(characterClass, token)),
        useBits: (sku) => dispatch(Actions.useBits(sku)),
        spendPoints: (attribute, token) => dispatch(spendPoints(attribute, token)),
        clearMessage: () => dispatch(Actions.setMessage('broadcast', '{}')),
        getCharacter: (token) => dispatch(getCharacter(token)),
    }
}

const AppWithRedux = connect(mapStateToProps, mapDispatchToProps)(App)

ReactDom.render(
    <Provider store={Store}>
        <AppWithRedux />
    </Provider>,
    document.getElementById('app')
)
