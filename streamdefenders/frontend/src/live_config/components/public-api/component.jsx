import * as React from 'react';

export class PublicApiComponent extends React.Component {
    constructor (props) {
        super(props)
        this.getListenButton = this.getListenButton.bind(this)
    }

    render () {
        return (
            <div>
                <p>
                    <button onClick={this.props.requestIdShare}>Share ID</button>
                    <button onClick={this.props.followChannel}>Follow Channel</button>
                </p>
                <button onClick={this.props.send}>Send: broadcast, "Testing"</button>{this.getListenButton()}
                <p>Broadcast Message</p>
                {this.props.messages && this.props.messages.broadcast}
            </div>
        );
    }

    getListenButton () {
        if (this.props.showUnlisten) {
            return <button onClick={this.props.unlisten}>Unlisten: broadcast</button>;
        }

        return <button onClick={this.props.listen}>Listen: broadcast</button>;
    }
}
