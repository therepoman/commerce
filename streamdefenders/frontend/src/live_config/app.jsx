import * as React from 'react'
import { DataDisplay } from './components/data-display/component.jsx'
import { PublicApi } from './components/public-api/container.jsx'
import { BitsApi } from './components/bits-api/container.jsx'

export class App extends React.Component {
    componentWillMount () {
        this.setState({
            anchor: this.getParameterByName('anchor'),
        })
    }
    render() {
        return (
            <div className={this.state.anchor}>
                <PublicApi />
                <hr />
                <BitsApi />
                <hr />
                <p>Auth</p>
                {this.displayObjectValues(this.props.authData)}
                <hr />
                <p>Context</p>
                {this.displayObjectValues(this.props.context)}
                <hr />
                <p>Error</p>
                {this.displayObjectValues(this.props.error)}
            </div>
        );
    }

    displayObjectValues (obj) {
        if (!obj) return null

        return Object.keys(obj).map(key => <DataDisplay key={key} label={key} value={obj[key]} />)
    }

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}
