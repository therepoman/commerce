import { ACTION_TYPES } from './actions'

export const initialState = {
    authData: null,
    context: {},
    error: null,
    messages: {},
    targets: [],
    bitsProducts: [],
    followed: {},
    latestTransaction: {},
}

export default function TwitchExt(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ON_AUTHORIZED:
            return {
                ...state,
                authData: action.data
            }
            break
        case ACTION_TYPES.ON_CONTEXT:
            return {
                ...state,
                context: action.data
            }
            break
        case ACTION_TYPES.ON_ERROR:
            return {
                ...state,
                error: action.data
            }
            break
        case ACTION_TYPES.ON_MESSAGE:
            console.log('onMessage', action.message);
            const messageData = {
                ...state.messages
            }
            messageData[action.target] = JSON.parse(action.message)
            return {
                ...state,
                messages: {
                    ...messageData
                }
            }
            break
        case ACTION_TYPES.ON_LISTEN:
            return {
                ...state,
                targets: state.targets.concat([action.target]),
            }
            break;
        case ACTION_TYPES.ON_UNLISTEN:
            const targets = []
            state.targets.map(target => {
                if (target !== action.target) {
                    targets.push(target)
                }
            })
            return {
                ...state,
                targets,
            }
            break;
        case ACTION_TYPES.ON_FOLLOW:
            const followed = {}
            const { didFollow, channelName } = action.data
            if (channelName) {
                followed[channelName] = didFollow
            }
            return {
                ...state,
                followed
            }
            break
        case ACTION_TYPES.SET_BITS_PRODUCTS:
            return {
                ...state,
                bitsProducts: action.products.slice(),
            }
            break
        case ACTION_TYPES.SET_LATEST_TRANSACTION:
            return {
                ...state,
                latestTransaction: action.transaction,
            }
            break
        default:
            return state;
    }
}
