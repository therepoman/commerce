# test-extension
top secret test extension

## Setup
First, install dependencies via
```
npm install
```

To start the dev server @https://localhost:7575 run
```
npm run dev
```

You can then see the `viewer` page at `https://localhost:7575/viewer.html`

The `Twitch.ext` global library is hooked up to the application via Redux.
In the example, the Redux state is being connected to the App component and then displayed on the page.

Additional actions are wired up for listening to pubsub messages, stop listening to them, sending pubsub
messages, and for following a channel.

### Listening to PubSub
To listen for a specific target, just dispatch a `listen` action with the target
```
import { listen } from './Actions'
import Store from './store'

Store.dispatch(listen(target))
```
Whenever a message for that target comes through, it can be found in the Redux store under `messages.{target}`

### Stop listening to PubSub
Just like listening, dispatching an action for `unlisten` will remove the listener for the specified target
NOTE: This effects all parts of the app though could be listening to the event
```
import { unlisten } from './Actions'
import Store from './store'

Store.dispatch(unlisten(target))
```

### Sending PubSub Messages
The `send` action is a passthrough to the `Twitch.ext.send` function. The documentation can be found here: https://dev.twitch.tv/docs/extensions/reference#send-functiontarget-contenttype-message
```
import { send } from './Actions'
import Store from './store'

Store.dispatch(send(target, contentType, message))
```

### Follow a channel
The `follow` action is a passthrough to the `Twitch.ext.actions.follow` function. It take just a channel name.
The application already has the `Twitch.ext.actions.onFollow` callback hooked up to the data store, so the status of the follow call
can be found in the Redux data store under `followed.{channelName}`. The value of that key in the store will match the value of the
`didFollow` argument in the `Twitch.ext.action.onFollow` callback.
```
import { follow } from './Actions'
import Store from './store'

Store.dispatch(follow(channelName))
```

### Connection Store and Action to Component
You can see how to connect the values of the data store and the actions to a react component
by viewing the index.js file within viewer