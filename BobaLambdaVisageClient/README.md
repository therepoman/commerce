# BobaLambdaVisageClient
This is a hand-made Visage client that bridges the gap between Visage and the generated BobaLambdaGoClient. Usually these Visage clients are auto-generated in the Visage package, but Boba is a Lambda service and Visage doesn't support Lambda services yet.

# Locally testing Visage
Visage's instance profile calls STS for temporary credentials to call Boba.  To emulate this authentication scheme locally, you will need to follow these steps to get local credentials for Beta:

1. Go to the boba+dev@amazon.com Isengard account with Admin privilege
1. Create an IAM user
    1. Navigate to IAM, select Users on the left
    1. When setting up the permissions, select "Attach existing policies directly" and search for "BobaLambdaVisageDevelopmentPolicy"
    1. Follow the rest of the steps to make a user, and make note of your access keys and ARN
    1. Use the access keys to set up a [named profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html) for the account
1. Change the role to trust your new user
    1. Navigate to IAM, select Roles on the left
    1. Search for "VisageLambdaRole-beta" and select it.
    1. Select the "Trust Relationship" tab and click "Edit Trust Relationship"
    1. Add your created user's ARN to the `AWS` list
1. Hack local Visage point to Beta (no great way to do this right now)
    1. In your local visage, modify `boba/client.go` to:
    ```
	  functionName = "arn:aws:lambda:us-west-2:726281605084:function:beta-mainhandler"
	  roleArn      = "arn:aws:iam::726281605084:role/VisageLambdaRole-beta"
  	```
