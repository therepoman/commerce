package client

import (
	"context"
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/pkg/errors"

	cjson "code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpc/encoding/json"
	"code.justin.tv/commerce/CoralGoModel/src/coral/model"
	"reflect"
)

// ResponsePayload represents the JSON body returned from Coral Lambdas.
type ResponsePayload struct {
	Body       string `json:"body"`
	StatusCode int    `json:"statusCode"`
}

// CoralObject extracts fields that are common to all Coral objects.
type CoralObject struct {
	Type string `json:"__type"`
}

type Client struct {
	functionName string
	svc          *lambda.Lambda
}

func NewClient(functionName string, config *aws.Config) (*Client, error) {
	svc := lambda.New(session.New(), config)
	return &Client{
		functionName: functionName,
		svc:          svc,
	}, nil
}

// Invokes a Coral lambda with given name and input. Uses an optional context
// to make the request. The output is populated if the call is successful.
//
// Ideally we would go through the generated Coral client, but there are some 
// library issues with doing so in a Lambda-y way:
//
// https://issues.amazon.com/issues/CoralGo-14
// https://issues.amazon.com/issues/CoralGo-15
func (c *Client) Call(ctx context.Context, in interface{}, out interface{}) error {
	b, err := cjson.Marshal(in)
	if err != nil {
		return err
	}

	input := &lambda.InvokeInput{
		FunctionName: aws.String(c.functionName),
		Payload:      b,
	}

	var response *lambda.InvokeOutput
	if ctx != nil {
		response, err = c.svc.InvokeWithContext(ctx, input)
	} else {
		response, err = c.svc.Invoke(input)
	}

	if err != nil {
		return err
	}

	// Extract the body from the payload so it can be unmarshalled
	var responsePayload ResponsePayload
	json.Unmarshal(response.Payload, &responsePayload)

	payloadBytes := []byte(responsePayload.Body)

	// Service Name is an optional field that enables the marshaller to resolve abstract
	// structures across model definitions.  Empty string disables the feature, as this
	// is a bit of a workaround right now.  See comments in:
	// https://code.amazon.com/reviews/CR-1384159/
	serviceName := ""

	if responsePayload.StatusCode != 200 {
		// Find the target assembly from the expected output shape.
		// This is a bit round-about.  It should be possible for the client
		// to be told explicitly what assembly it is calling on behalf of,
		// and then skipping to the next step.
		outputType := reflect.TypeOf(out).Elem()
		outputShape, err := model.GetShapeFromType(outputType)
		if err != nil {
			return errors.Errorf("Failure finding type for %s: %s", outputType, err)
		}
		outputAssembly := outputShape.Assembly()

		// Extract error FQN from response
		coralObject := &CoralObject{}
		json.Unmarshal(payloadBytes, coralObject)
		errorFQN := coralObject.Type

		// Unmarshall error object
		errorShape, err := outputAssembly.ShapeFromFQN(errorFQN)
		if err != nil {
			return errors.Errorf("Failure unmarshalling error %s: %s", errorFQN, err)
		}
		errorInstance := errorShape.New()
		err = cjson.Unmarshal(payloadBytes, errorInstance, serviceName)
		if err != nil {
			return errors.Errorf("Failure unmarshalling error %s: %s", errorFQN, err)
		}

		errorCasted, ok := errorInstance.(error)
		if !ok {
			return errors.Errorf("Status code is %d, but the returned type %s is not an error type", errorFQN)
		}

		return errorCasted
	}

	return cjson.Unmarshal(payloadBytes, out, serviceName)
}
