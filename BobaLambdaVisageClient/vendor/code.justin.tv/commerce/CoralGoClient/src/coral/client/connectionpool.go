package client

import "io"

// A ConnectionPool consists of two basic methods: Consume, which retrieves a connection from the pool, and Return, which
// puts a connection back into the pool.
type ConnectionPool interface {
	// Consume retrieves a connection from the pool or creates a new one if none exists.
	// An error is returned if the connection can't be retrieved or created.
	Consume() (io.ReadWriteCloser, error)
	// Puts a connection back into the pool.
	Return(io.ReadWriteCloser)
}
