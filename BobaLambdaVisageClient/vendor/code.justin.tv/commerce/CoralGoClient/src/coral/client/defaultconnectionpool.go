package client

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"
)

const (
	DefaultPoolSize = 5
	DefaultMaxTTL   = time.Second * 45
	DefaultJitter   = time.Second
)

type connectionFactory func() (io.ReadWriteCloser, error)

type coralConnection struct {
	io.ReadWriteCloser
	creationTime time.Time
}

// defaultConnectionPool implements ConnectionPool interface.
type defaultConnectionPool struct {
	pool    chan *coralConnection
	maxTTL  time.Duration
	jitter  time.Duration
	factory connectionFactory
	rand    *rand.Rand
}

func newDefaultConnectionPool(poolSize uint, maxTTL, jitter time.Duration, factory connectionFactory) ConnectionPool {
	p := &defaultConnectionPool{
		pool:    make(chan *coralConnection, poolSize),
		maxTTL:  maxTTL,
		jitter:  jitter,
		factory: factory,
		rand:    rand.New(rand.NewSource(time.Now().UnixNano())),
	}
	p.validate()
	return p
}

func (p *defaultConnectionPool) validate() {
	m := "Invalid argument for default connection pool, "
	if p.maxTTL < 0 {
		p.maxTTL = DefaultMaxTTL
		fmt.Fprintln(os.Stderr, m+"connection TTL must be positive")
	}

	if p.jitter <= 0 {
		p.jitter = DefaultJitter
		fmt.Fprintln(os.Stderr, m+"jitter must be positive")
	}

	if p.factory == nil {
		panic(m + "connection factory was not provided")
	}
}

// Consume retrieves a connection from the pool. If no connection is available or it has expired, then a new connection
// is created using the factory.
func (p *defaultConnectionPool) Consume() (io.ReadWriteCloser, error) {
	select {
	case conn := <-p.pool:
		// If connection has expired, then return from factory
		if time.Since(conn.creationTime) >= p.maxTTL {
			conn.Close() // error intentionally omitted
			return p.newCoralConn()
		}

		return conn, nil
	default:
		return p.newCoralConn()
	}
}

// Return puts the connection passed as parameter back into the pool. If an io.ReadWriteCloser that wasn't created
// by this pool (i.e not a *coralConnection) is passed as parameter, then it is ignored.
// When the connection pool is full or the connection being returned is already expired, the connection is closed
// and discarded.
func (p *defaultConnectionPool) Return(rw io.ReadWriteCloser) {
	coralConn, ok := rw.(*coralConnection)
	if !ok {
		return
	}

	if time.Since(coralConn.creationTime) >= p.maxTTL {
		coralConn.Close() // error intentionally omitted
		return
	}

	select {
	case p.pool <- coralConn:
	default:
		// Pool of connections is full, close it and move on.
		coralConn.Close() // error intentionally omitted
	}
}

func (p *defaultConnectionPool) length() int {
	return len(p.pool)
}

// note: creation time will be marked as time.Now() - (random 0 <= p.jitter). The intention of this is to make
// connections expire at slightly different times to avoid thundering herds.
func (p *defaultConnectionPool) newCoralConn() (*coralConnection, error) {
	conn, err := p.factory()
	if err != nil {
		return nil, err
	}

	return &coralConnection{
		ReadWriteCloser: conn,
		creationTime:    p.creationTimeWithJitter(),
	}, nil
}

func (p *defaultConnectionPool) creationTimeWithJitter() time.Time {
	now := time.Now()
	if p.jitter == 0 {
		return now
	}
	jitter := time.Duration(-p.rand.Int63n(int64(p.jitter)))
	return now.Add(jitter)
}
