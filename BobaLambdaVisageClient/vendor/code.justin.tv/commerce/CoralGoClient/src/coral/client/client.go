package client

import (
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

type Client interface {
	Call(asmName, operation string, input interface{}, output interface{}) error
}

type OperationRef struct {
	AssemblyName, Name string
}

type ServiceRef struct {
	AssemblyName, ServiceName string
}

type client struct {
	serviceRef codec.ShapeRef
	dialer     dialer.Dialer
	codec      codec.RoundTripper
	connPool   ConnectionPool
}

func NewClient(serviceAssembly, serviceName string, dialer dialer.Dialer, c codec.RoundTripper, options ...Option) Client {
	cl := &client{
		serviceRef: codec.ShapeRef{AsmName: serviceAssembly, ShapeName: serviceName},
		dialer:     dialer,
		codec:      c,
		connPool: newDefaultConnectionPool(
			DefaultPoolSize,
			DefaultMaxTTL,
			DefaultJitter,
			dialer.Dial), // Connection factory
	}
	cl.applyOptions(options)
	return cl
}

func (c *client) applyOptions(options []Option) {
	for _, o := range options {
		o(c)
	}
}

func (c *client) Call(asmName, operation string, in interface{}, out interface{}) error {
	conn, err := c.connPool.Consume()
	if err != nil {
		return err
	}
	req := &codec.Request{
		Service:   c.serviceRef,
		Operation: codec.ShapeRef{AsmName: asmName, ShapeName: operation},
		Input:     in,
		Output:    out,
	}
	err = c.codec.RoundTrip(req, conn)
	if err != nil {
		conn.Close()
		return err
	}
	c.connPool.Return(conn)
	return nil
}
