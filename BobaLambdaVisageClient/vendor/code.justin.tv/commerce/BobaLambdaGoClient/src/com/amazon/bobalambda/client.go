package com_amazon_bobalambda

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//The bounty board service.
type BobaLambdaClient struct {
	C __client__.Client
}

//Creates a new BobaLambdaClient
func NewBobaLambdaClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper, options ...__client__.Option) (service *BobaLambdaClient) {
	return &BobaLambdaClient{__client__.NewClient("com.amazon.bobalambda", "BobaLambda", dialer, codec, options...)}
}

//Checks a bounty's progress and updates its status.
func (this *BobaLambdaClient) CheckBountyProgress(input CheckBountyProgressRequest) (CheckBountyProgressResponse, error) {
	var output CheckBountyProgressResponse
	err := this.C.Call("com.amazon.bobalambda", "CheckBountyProgress", input, &output)
	return output, err
}

//Retrieves the list of bounties which match the given arguments.
func (this *BobaLambdaClient) GetBounties(input GetBountiesRequest) (GetBountiesResponse, error) {
	var output GetBountiesResponse
	err := this.C.Call("com.amazon.bobalambda", "GetBounties", input, &output)
	return output, err
}

//Update bounty progress with bounty ID.
func (this *BobaLambdaClient) UpdateBountyProgress(input UpdateBountyProgressRequest) (UpdateBountyProgressResponse, error) {
	var output UpdateBountyProgressResponse
	err := this.C.Call("com.amazon.bobalambda", "UpdateBountyProgress", input, &output)
	return output, err
}

//Take a campaign and refresh broadcaster settings in the Settings table.
func (this *BobaLambdaClient) RefreshBroadcasterSettings(input RefreshBroadcasterSettingsRequest) (RefreshBroadcasterSettingsResponse, error) {
	var output RefreshBroadcasterSettingsResponse
	err := this.C.Call("com.amazon.bobalambda", "RefreshBroadcasterSettings", input, &output)
	return output, err
}

//Cancels a bounty for a Twitch User for a particular bounty ID.
func (this *BobaLambdaClient) CancelBounty(input CancelBountyRequest) (CancelBountyResponse, error) {
	var output CancelBountyResponse
	err := this.C.Call("com.amazon.bobalambda", "CancelBounty", input, &output)
	return output, err
}

//Takes a list of broadcasters and updates them in the Settings table.
func (this *BobaLambdaClient) UpdateBroadcasterSettings(input UpdateBroadcasterSettingsRequest) (UpdateBroadcasterSettingsResponse, error) {
	var output UpdateBroadcasterSettingsResponse
	err := this.C.Call("com.amazon.bobalambda", "UpdateBroadcasterSettings", input, &output)
	return output, err
}

//Fetches all the earnings related to Bounty Board in a time frame.
func (this *BobaLambdaClient) GetBountyEarnings(input GetBountyEarningsRequest) (GetBountyEarningsResponse, error) {
	var output GetBountyEarningsResponse
	err := this.C.Call("com.amazon.bobalambda", "GetBountyEarnings", input, &output)
	return output, err
}

//Fetches the bounties report for a campaignID in Bounty Board.
func (this *BobaLambdaClient) GetBountiesReport(input GetBountiesReportRequest) (GetBountiesReportResponse, error) {
	var output GetBountiesReportResponse
	err := this.C.Call("com.amazon.bobalambda", "GetBountiesReport", input, &output)
	return output, err
}

//Update the moderation status of a bounty
func (this *BobaLambdaClient) ModerateBounty(input ModerateBountyRequest) (ModerateBountyResponse, error) {
	var output ModerateBountyResponse
	err := this.C.Call("com.amazon.bobalambda", "ModerateBounty", input, &output)
	return output, err
}

//Starts a bounty with a given bounty ID for a Twitch User.
func (this *BobaLambdaClient) StartBounty(input StartBountyRequest) (StartBountyResponse, error) {
	var output StartBountyResponse
	err := this.C.Call("com.amazon.bobalambda", "StartBounty", input, &output)
	return output, err
}

//Stops a bounty with a given bounty ID for a Twitch User.
func (this *BobaLambdaClient) StopBounty(input StopBountyRequest) (StopBountyResponse, error) {
	var output StopBountyResponse
	err := this.C.Call("com.amazon.bobalambda", "StopBounty", input, &output)
	return output, err
}

//Performs any necessary reconciliation for a bounty (ie. adjusting the
//consumed budget based on the amount of actual viewers delivered).
func (this *BobaLambdaClient) ReconcileBounty(input ReconcileBountyRequest) (ReconcileBountyResponse, error) {
	var output ReconcileBountyResponse
	err := this.C.Call("com.amazon.bobalambda", "ReconcileBounty", input, &output)
	return output, err
}

//Take a bountyId and publish bounty sponsored event.
func (this *BobaLambdaClient) PublishSponsoredEvent(input PublishSponsoredEventRequest) (PublishSponsoredEventResponse, error) {
	var output PublishSponsoredEventResponse
	err := this.C.Call("com.amazon.bobalambda", "PublishSponsoredEvent", input, &output)
	return output, err
}

//Take a bountyId and return bounty sponsored sessions.
func (this *BobaLambdaClient) SponsoredSessionsAudit(input SponsoredSessionsAuditRequest) (SponsoredSessionsAuditResponse, error) {
	var output SponsoredSessionsAuditResponse
	err := this.C.Call("com.amazon.bobalambda", "SponsoredSessionsAudit", input, &output)
	return output, err
}

//Retrieves bounty board settings data that corresponds to a Twitch User ID.
func (this *BobaLambdaClient) GetSettings(input GetSettingsRequest) (GetSettingsResponse, error) {
	var output GetSettingsResponse
	err := this.C.Call("com.amazon.bobalambda", "GetSettings", input, &output)
	return output, err
}

//Claims a bounty for a Twitch User for a particular campaign ID with a given bounty ID.
func (this *BobaLambdaClient) ClaimBounty(input ClaimBountyRequest) (ClaimBountyResponse, error) {
	var output ClaimBountyResponse
	err := this.C.Call("com.amazon.bobalambda", "ClaimBounty", input, &output)
	return output, err
}
