package model

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/pkg/errors"
)

// Generated Coral Clients and Servers register Services, Assemblies, and
// shapes in this package.  Coral models use Assembly as the top level namespace,
// but this causes problems with multiple registrations of shared model components.
// To enable registration of generated Clients for different services that share
// the same model we instead use the Service as the top level namespace.
//
// Given an Assembly c.a.shared and two Service Assemblies c.a.s1#FooService and
// c.a.s2#BarService, both of which use data types from c.a.shared.  Attempting to
// use the Clients generated for FooService and BarService would cause a conflict
// if the Assembly were used for the namespace.  Instead we have registrations for
// FooService->[c.a.shared, c.a.s1] and BarService->[c.a.shared, c.a.s2].

type Service interface {
	// Assembly returns the service specific Assembly with the given name.
	// It will never return nil.
	Assembly(name string) Assembly
	// Assemblies returns all Assemblies that have been registered for the
	// service.
	Assemblies() []Assembly
	// Name of the service.
	Name() string
}

type Assembly interface {
	// Shape returns any shape that has been registered with the given name.
	// If no shape is registered under the given name an error is returned.
	Shape(name string) (Shape, error)
	// Shapes returns a list of all Shapes that have been registered for the
	// Assembly.
	Shapes() []Shape
	// Shape returns any shape that has been registered with the given FQN.
	// If no shape is registered under the given name an error is returned.
	ShapeFromFQN(name string) (Shape, error)
	// Op returns any operation that has been registered with the given name.
	// If no op is registered under the given name an error is returned.
	Op(name string) (Op, error)
	// Ops returns a list of Operations that have been registered for the Assembly.
	Ops() []Op
	// RegisterShape adds a Shape of the given name to the Assembly.  An error is
	// returned if any field is not specified or if a shape for the given name has
	// already been registered.
	RegisterShape(name string, t reflect.Type, constructor func() interface{}) error
	// RegisterOp adds an operation of the given name to the Assembly.  All inputs
	// except for name are optional.  An error is returned if an operation for the given
	// name has already been registered or if any of the given shapes have not been
	// registered.
	RegisterOp(name string, input, output Shape, errs []Shape) error
	// Name of the Assembly
	Name() string
}

type Shape interface {
	// Assembly the shape is attached to.
	Assembly() Assembly
	// Name of the shape.
	Name() string
	// FullyQualifiedName of the shape: assembly.Name + "#" + shape.Name
	FullyQualifiedName() string
	// Type that the Shape represents.
	Type() reflect.Type
	// New generates an instance that is referred to by Type().
	New() interface{}
}

// Op defines an Operation.
type Op interface {
	// Assembly the operation is attached to.
	Assembly() Assembly
	// Name of the operation.
	Name() string
	// Input that the operation takes.  May be nil.
	Input() Shape
	// Output of the operation.  May be nil.
	Output() Shape
	// Errors that the operation can throw.  May be nil.
	Errors() []Shape
}

var (
	services   = map[string]Service{}
	shapes     = map[reflect.Type]Shape{}
	converters = map[reflect.Type]func(reflect.Value) reflect.Value{}
)

// LookupService returns the Service structure for the given name.
// It will never return nil.
func LookupService(name string) Service {
	svc := services[name]
	if svc == nil {
		svc = &service{
			assemblies: map[string]Assembly{},
			name:       name,
		}
		services[name] = svc
	}
	return svc
}

// GetShapeFromType returns the registered Shape for the given type.
// If there is no registration then an error is returned.
func GetShapeFromType(t reflect.Type) (Shape, error) {
	s, ok := shapes[t]
	if ok {
		return s, nil
	}
	return nil, errors.New("No Shape Registered for Type " + t.String())
}

// Assembly satisfies Service.
func (svc *service) Assembly(name string) Assembly {
	asm := svc.assemblies[name]
	if asm == nil {
		asm = &assembly{
			shapes: map[string]Shape{},
			ops:    map[string]Op{},
			name:   name,
		}
		svc.assemblies[name] = asm
	}
	return asm
}

// Assemblies satisfies Service.
func (svc *service) Assemblies() []Assembly {
	asms := make([]Assembly, 0, len(svc.assemblies))
	for _, asm := range svc.assemblies {
		asms = append(asms, asm)
	}
	return asms
}

// ShapeFromFQN satisfies Assembly.
func (asm *assembly) ShapeFromFQN(fqn string) (Shape, error) {
	index := strings.LastIndex(fqn, "#")
	shapeName := fqn
	if index >= 0 {
		shapeName = fqn[index+1:]
	}
	return asm.Shape(shapeName)
}

// Shape satisfies Assembly.
func (asm *assembly) Shape(name string) (Shape, error) {
	s, ok := asm.shapes[name]
	if !ok {
		return nil, errors.Errorf("No Shape registered with the name %s", name)
	}
	return s, nil
}

// Shapes satisfies Assembly.
func (asm *assembly) Shapes() []Shape {
	shapes := make([]Shape, 0, len(asm.shapes))
	for _, sh := range asm.shapes {
		shapes = append(shapes, sh)
	}
	return shapes
}

// Op satisfies Assembly.
func (asm *assembly) Op(name string) (Op, error) {
	o, ok := asm.ops[name]
	if !ok {
		return nil, errors.Errorf("No Op registered with the name %s", name)
	}
	return o, nil
}

// Ops satisfies Assembly.
func (asm *assembly) Ops() []Op {
	ops := make([]Op, 0, len(asm.ops))
	for _, op := range asm.ops {
		ops = append(ops, op)
	}
	return ops
}

// RegisterShape satisfies Assembly.
func (asm *assembly) RegisterShape(name string, interfaceType reflect.Type, constructor func() interface{}) error {
	_, ok := asm.shapes[name]
	interfaceType = interfaceType.Elem()
	if !ok {
		shape := &shape{
			assembly:    asm,
			name:        name,
			t:           interfaceType,
			constructor: constructor,
		}
		asm.shapes[name] = shape
		shapes[interfaceType] = shape
		implType := reflect.Indirect(reflect.ValueOf(constructor())).Type()
		shapes[implType] = shape
		return nil
	}
	return errors.Errorf("Shape already registered with the name %s", name)
}

// RegisterOp satisfies Assembly.
func (asm *assembly) RegisterOp(name string, input, output Shape, errs []Shape) error {
	if name == "" {
		return errors.Errorf("Op must have a name")
	}
	if _, ok := asm.ops[name]; ok {
		return errors.Errorf("Op already registered with the name %s", name)
	}

	shapes := []Shape{input, output}
	shapes = append(shapes, errs...)
	if s := asm.isRegistered(shapes); s != nil {
		return errors.Errorf("Given a shape that is not registered: %s", s.Name())
	}
	asm.ops[name] = &op{name: name, assembly: asm, input: input, output: output, errs: errs}
	return nil
}

// isRegistered iterates over the given shapes and returns the first Shape it finds
// that is not registered with the assembly.
func (asm *assembly) isRegistered(shapes []Shape) Shape {
	for _, s := range shapes {
		if s == nil {
			continue
		}
		if _, ok := asm.shapes[s.Name()]; !ok {
			return s
		}
	}
	return nil
}

func RegisterConverter(t reflect.Type, fun func(reflect.Value) reflect.Value) {
	converters[t] = fun
}

func Convert(t reflect.Type, v reflect.Value) reflect.Value {
	return v.Convert(t)
}

func ErrorMessage(e interface{}) string {
	val := reflect.ValueOf(e)
	if !val.IsValid() {
		return "nil"
	}
	name := val.Type().String()
	// Try to call a "Message() string" function.
	// Use a type assertion to an anonymous interface.
	if messager, ok := e.(interface {
		Message() string
	}); ok {
		return fmt.Sprintf("%s: %s", name, messager.Message())
	}
	// Now try the subtly different "Message() *string" function
	if messager, ok := e.(interface {
		Message() *string
	}); ok {
		if msg := messager.Message(); msg != nil {
			return fmt.Sprintf("%s: %s", name, *msg)
		}
	}
	return name
}
