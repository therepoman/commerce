package main

import (
	"code.justin.tv/commerce/BobaLambdaVisageClient/src/boba"
	"code.justin.tv/foundation/twitchclient"
	"fmt"
	"golang.org/x/net/context"
	"time"
)

// The main function is used to test the client locally.
func main() {
	clientConf := twitchclient.ClientConf{
		Host: "arn:aws:lambda:us-west-2:726281605084:function:beta-mainhandler",
	}
	client, err := boba.NewClient(clientConf)
	if err != nil {
		fmt.Printf("Error when creating the Boba client: %v\n", err)
	}

	testGetBountyEarnings(client, time.Unix(1, 0), time.Unix(100000000000, 0))
	// testClaimBounty(client)
}

func testGetBountyEarnings(client boba.Client, start time.Time, end time.Time) {
	fmt.Printf("Calling GetBountyEarnings with start %v and end %v.\n", start, end)

	earningsEvents, err := client.GetBountyEarnings(context.Background(), start, end, nil)
	if err != nil {
		fmt.Printf("Error when calling GetBountyEarnings: %v\n", err)
	}

	fmt.Printf("Found %d earnings events.\n", len(*earningsEvents))
}

func testClaimBounty(client boba.Client) {
	fmt.Printf("Calling ClaimBounty \n")

	bountyId := "3786f4a7-621c-468b-aaf0-fb148a9ae89f-test"
	campaignId := "3786f4a7-621c-468b-aaf0-fb148a9ae89f"
	userId := "87220843"
	platform := "PC"
	region := "NA"

	bounty, err := client.ClaimBounty(context.Background(), userId, bountyId, campaignId, &platform, &region, nil)
	if err != nil {
		fmt.Printf("Error when calling GetBountyEarnings: %v\n", err)
	}

	fmt.Printf("Claimed: %v", bounty)
}