package boba

import (
	"context"
	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"time"

	boba "code.justin.tv/commerce/BobaLambdaGoClient/src/com/amazon/bobalambda"
	coral "code.justin.tv/commerce/CoralLambdaGoSupport/src/coral/lambda/client"
	"code.justin.tv/foundation/twitchclient"
)

type Client interface {
	GetBountyBoardSettings(ctx context.Context, userID string, opts *twitchclient.ReqOpts) (*boba.GetSettingsResponse, error)
	GetBounties(ctx context.Context, userID string, status string, limit *int64, cursor *string, opts *twitchclient.ReqOpts) (*[]boba.Bounty, *string, error)
	ClaimBounty(ctx context.Context, userID string, bountyID string, campaignID string, platform *string, region *string, opts *twitchclient.ReqOpts) (*boba.Bounty, error)
	CancelBounty(ctx context.Context, userID string, bountyID string, reason string, opts *twitchclient.ReqOpts) (*boba.Bounty, error)
	StartBounty(ctx context.Context, userID string, bountyID string, title string, opts *twitchclient.ReqOpts) (*boba.Bounty, error)
	StopBounty(ctx context.Context, userID string, bountyID string, opts *twitchclient.ReqOpts) (*boba.Bounty, error)
	GetBountyEarnings(ctx context.Context, start time.Time, end time.Time, opts *twitchclient.ReqOpts) (*[]boba.EarningsEvent, error)
}

type client struct {
	*coral.Client
}

// development/staging - arn:aws:lambda:us-west-2:726281605084:function:beta-mainhandler
// production          - arn:aws:lambda:us-west-2:737882373154:function:prod-mainhandler

func NewClient(clientConf twitchclient.ClientConf) (Client, error) {
	// Create Coral Lambda client
	twitchClient := twitchclient.NewHTTPClient(clientConf)
	awsConfig := aws.NewConfig().WithRegion("us-west-2").WithHTTPClient(twitchClient)
	coralClient, err := coral.NewClient(clientConf.Host, awsConfig)
	if err != nil {
		return nil, err
	}

	return &client{coralClient}, nil
}

func (c *client) GetBountyBoardSettings(ctx context.Context, userID string, opts *twitchclient.ReqOpts) (*boba.GetSettingsResponse, error) {
	request := boba.NewGetSettingsRequest()
	request.SetTuid(&userID)

	var response boba.GetSettingsResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, err
	}

	if response == nil {
		return nil, errors.New("Failed to retrieve bounty board settings")
	}

	return &response, nil
}

func (c *client) GetBounties(ctx context.Context, userID string, status string, limit *int64, cursor *string, opts *twitchclient.ReqOpts) (*[]boba.Bounty, *string, error) {
	request := boba.NewGetBountiesRequest()
	request.SetTuid(&userID)
	request.SetStatus(&status)
	request.SetLimit(limit)
	request.SetCursor(cursor)

	var response boba.GetBountiesResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, nil, err
	}

	if response == nil || response.Bounties() == nil {
		return nil, nil, errors.New("Failed to retrieve bounties")
	}

	bounties := response.Bounties()
	nextCursor := response.NextCursor()
	return &bounties, nextCursor, nil
}

func (c *client) ClaimBounty(ctx context.Context, userID string, bountyID string, campaignID string, platform *string, region *string, opts *twitchclient.ReqOpts) (*boba.Bounty, error) {
	request := boba.NewClaimBountyRequest()
	request.SetTuid(&userID)
	request.SetBountyId(&bountyID)
	request.SetCampaignId(&campaignID)
	request.SetPlatform(platform)
	request.SetRegion(region)

	var response boba.ClaimBountyResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, err
	}

	if response == nil || response.Bounty() == nil {
		return nil, errors.New("Failed to claim bounty")
	}

	bounty := response.Bounty()
	return &bounty, nil
}

func (c *client) CancelBounty(ctx context.Context, userID string, bountyID string, reason string, opts *twitchclient.ReqOpts) (*boba.Bounty, error) {
	request := boba.NewCancelBountyRequest()
	request.SetTuid(&userID)
	request.SetBountyId(&bountyID)
	request.SetReason(&reason)

	var response boba.CancelBountyResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, err
	}

	if response == nil || response.Bounty() == nil {
		return nil, errors.New("Failed to cancel bounty")
	}

	bounty := response.Bounty()
	return &bounty, nil
}

func (c *client) StartBounty(ctx context.Context, userID string, bountyID string, title string, opts *twitchclient.ReqOpts) (*boba.Bounty, error) {
	request := boba.NewStartBountyRequest()
	request.SetTuid(&userID)
	request.SetBountyId(&bountyID)
	request.SetStreamTitle(&title)

	var response boba.StartBountyResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, err
	}

	if response == nil || response.Bounty() == nil {
		return nil, errors.New("Failed to start bounty")
	}

	bounty := response.Bounty()
	return &bounty, nil
}

func (c *client) StopBounty(ctx context.Context, userID string, bountyID string, opts *twitchclient.ReqOpts) (*boba.Bounty, error) {
	request := boba.NewStopBountyRequest()
	request.SetTuid(&userID)
	request.SetBountyId(&bountyID)

	var response boba.StartBountyResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, err
	}

	if response == nil || response.Bounty() == nil {
		return nil, errors.New("Failed to stop bounty")
	}

	bounty := response.Bounty()
	return &bounty, nil
}

func (c *client) GetBountyEarnings(ctx context.Context, start time.Time, end time.Time, opts *twitchclient.ReqOpts) (*[]boba.EarningsEvent, error) {
	unixStartTime := start.Unix()
	unixEndTime := end.Unix()

	request := boba.NewGetBountyEarningsRequest()
	request.SetStart(&unixStartTime)
	request.SetEnd(&unixEndTime)

	var response boba.GetBountyEarningsResponse

	err := c.Client.Call(ctx, request, &response)
	if err != nil {
		return nil, err
	}

	if response == nil || response.EarningsEvents() == nil {
		return nil, errors.New("Failed to get bounty earnings")
	}

	earningsEvents := response.EarningsEvents()
	return &earningsEvents, nil
}
