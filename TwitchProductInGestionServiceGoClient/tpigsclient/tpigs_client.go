package tpigsclient

import (
	client "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/twitchproductingestionservice"
	submissionModel "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/adg/submission/model"
	commonModel "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/adg/common/model"
	fuseModel "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/mas/devportal/fileupload/model"
	"code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/tpigsclient/coral"

	log "github.com/Sirupsen/logrus"
)

type ITPIGSClient interface {
	CreateProducts(input client.CreateProductsRequest) (submissionModel.CreateProductsResponse, error)
	CreateDomains(input client.CreateDomainsRequest) (submissionModel.CreateDomainsResponse, error)
	SaveProductRelease(input client.SaveProductReleaseRequest) (submissionModel.SaveProductReleaseResponse, error)
	SubmitProductRelease(input client.SubmitProductReleaseRequest) (submissionModel.SubmitProductReleaseResponse, error)
	CreateUpload(input client.CreateUploadRequest) (fuseModel.UploadResource, error)
	GetUpload(input client.GetUploadRequest) (fuseModel.UploadResource, error)
}

type TPIGSClient struct {
	SwsParams coral.SWSParams
}

type ITPIGSClientWrapper interface {
	CreateProducts(tuid string, products []submissionModel.SubmissionProduct, vendorId string, masVendorId string) ([]submissionModel.SubmissionProduct, []submissionModel.ProductError, error)
	CreateDomains(tuid string, domains []commonModel.Domain, parentDomain commonModel.Domain, vendorId string, masVendorId string) ([]commonModel.Domain, []submissionModel.DomainError, error)
	SaveProductRelease(tuid string, adgProductId string, adgProductReleaseId string, releaseDocument *string) (string, error)
	SubmitProductRelease(tuid string, adgProductId string, vendorId string) (string, error)
	CreateUpload(tuid string, assetType string, fileType string, tenantId string) (fuseModel.UploadResource, error)
	GetUpload(tuid string, uploadId string) (fuseModel.UploadResource, error)
}

type TPIGSClientWrapper struct {
	Client		ITPIGSClient
	SwsParams	coral.SWSParams
	IsDevelopment	bool
	RetryStrategy	coral.RetryStrategy
}

// TPIGSClient implementation

func NewTPIGSClient(params coral.SWSParams) (*client.TwitchProductInGestionServiceClient, error) {
	codec, dialer, err := coral.InitCodecAndDialer(params)
	if err != nil {
		return nil, err
	}
	client := client.NewTwitchProductInGestionServiceClient(dialer, codec)
	return client, nil
}

func (c *TPIGSClient) CreateProducts(input client.CreateProductsRequest) (submissionModel.CreateProductsResponse, error) {
	client, err := NewTPIGSClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new TwitchProductInGestionService client: %v", err)
		return nil, err
	}
	return client.CreateProducts(input)
}

func (c *TPIGSClient) CreateDomains(input client.CreateDomainsRequest) (submissionModel.CreateDomainsResponse, error) {
	client, err := NewTPIGSClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new TwitchProductInGestionService client: %v", err)
		return nil, err
	}
	return client.CreateDomains(input)
}

func (c *TPIGSClient) SaveProductRelease(input client.SaveProductReleaseRequest) (submissionModel.SaveProductReleaseResponse, error) {
	client, err := NewTPIGSClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new TwitchProductInGestionService client: %v", err)
		return nil, err
	}
	return client.SaveProductRelease(input)
}

func (c *TPIGSClient) SubmitProductRelease(input client.SubmitProductReleaseRequest) (submissionModel.SubmitProductReleaseResponse, error) {
	client, err := NewTPIGSClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new TwitchProductInGestionService client: %v", err)
		return nil, err
	}
	return client.SubmitProductRelease(input)
}

func (c *TPIGSClient) CreateUpload(input client.CreateUploadRequest) (fuseModel.UploadResource, error) {
	client, err := NewTPIGSClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new TwitchProductInGestionService client: %v", err)
		return nil, err
	}
	return client.CreateUpload(input)
}

func (c *TPIGSClient) GetUpload(input client.GetUploadRequest) (fuseModel.UploadResource, error) {
	client, err := NewTPIGSClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new TwitchProductInGestionService client: %v", err)
		return nil, err
	}
	return client.GetUpload(input)
}

// TPIGSClientWrapper implementation

func (c *TPIGSClientWrapper) CreateProducts(tuid string, products []submissionModel.SubmissionProduct, vendorId string, masVendorId string) ([]submissionModel.SubmissionProduct, []submissionModel.ProductError, error) {
	vendor := commonModel.NewVendor()
	vendor.SetId(&vendorId)
	vendor.SetMasVendorId(&masVendorId)

	adgRequest := submissionModel.NewCreateProductsRequest()
	adgRequest.SetVendor(vendor)
	adgRequest.SetNewProducts(products)

	request := client.NewCreateProductsRequest()
	request.SetTuid(&tuid)
	request.SetAdgRequest(adgRequest)

	response, err := c.Client.CreateProducts(request)

	if err != nil {
		return nil, nil, err
	}
	return response.NewProducts(), response.Errors(), nil
}

func (c *TPIGSClientWrapper) CreateDomains(tuid string, domains []commonModel.Domain, parentDomain commonModel.Domain, vendorId string, masVendorId string) ([]commonModel.Domain, []submissionModel.DomainError, error) {
	vendor := commonModel.NewVendor()
	vendor.SetId(&vendorId)
	vendor.SetMasVendorId(&masVendorId)

	adgRequest := submissionModel.NewCreateDomainsRequest()
	adgRequest.SetVendor(vendor)
	adgRequest.SetParentDomain(parentDomain)
	adgRequest.SetNewDomains(domains)

	request := client.NewCreateDomainsRequest()
	request.SetTuid(&tuid)
	request.SetAdgRequest(adgRequest)

	response, err := c.Client.CreateDomains(request)

	if err != nil {
		return nil, nil, err
	}
	return response.NewDomains(), response.Errors(), nil
}

func (c *TPIGSClientWrapper) SaveProductRelease(tuid string, adgProductId string, vendorId string, releaseDocument *string) (string, error) {
	product := commonModel.NewProduct()
	product.SetId(&adgProductId)
	
	vendor := commonModel.NewVendor()
	vendor.SetId(&vendorId)

	productRelease := submissionModel.NewProductRelease()
	productRelease.SetReleaseDocument(releaseDocument)

	adgRequest := submissionModel.NewSaveProductReleaseRequest()
	adgRequest.SetProduct(product)
	adgRequest.SetVendor(vendor)
	adgRequest.SetProductRelease(productRelease)

	request := client.NewSaveProductReleaseRequest()
	request.SetTuid(&tuid)
	request.SetAdgRequest(adgRequest)

	response, err := c.Client.SaveProductRelease(request)

	if err != nil {
		return "", err
	}

	return *(response.ProductRelease().Id()), nil
}

func (c *TPIGSClientWrapper) SubmitProductRelease(tuid string, adgProductId string, vendorId string) (string, error) {
	product := commonModel.NewProduct()
	product.SetId(&adgProductId)

	vendor := commonModel.NewVendor()
	vendor.SetId(&vendorId)

	adgRequest := submissionModel.NewSubmitProductReleaseRequest()
	adgRequest.SetProduct(product)
	adgRequest.SetVendor(vendor)

	request := client.NewSubmitProductReleaseRequest()
	request.SetTuid(&tuid)
	request.SetAdgRequest(adgRequest)

	response, err := c.Client.SubmitProductRelease(request)

	if err != nil {
		return "", err
	}

	return *(response.SubmissionId()), nil
}

func (c *TPIGSClientWrapper) CreateUpload(tuid string, assetType string, fileType string, tenantId string) (fuseModel.UploadResource, error) {
	masRequest := fuseModel.NewCreateUploadRequest()
	masRequest.SetAssetType(&assetType)
	masRequest.SetFileType(&fileType)
	masRequest.SetTenantId(&tenantId)

	request := client.NewCreateUploadRequest()
	request.SetTuid(&tuid)
	request.SetMasRequest(masRequest)

	response, err := c.Client.CreateUpload(request)

	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *TPIGSClientWrapper) GetUpload(tuid string, uploadId string) (fuseModel.UploadResource, error) {
	masRequest := fuseModel.NewGetUploadRequest()
	masRequest.SetUploadId(&uploadId)

	request := client.NewGetUploadRequest()
	request.SetTuid(&tuid)
	request.SetMasRequest(masRequest)

	response, err := c.Client.GetUpload(request)

	if err != nil {
		return nil, err
	}

	return response, nil
}
