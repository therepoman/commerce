package coral

import (
	"code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpcv1"

	"crypto/tls"
	"crypto/x509"
	"io/ioutil"

	log "github.com/Sirupsen/logrus"
)

// SWSParams : Parameters for SWS
type SWSParams struct {
	SWSEnabled	bool
	RootCertStore	string
	ClientCertChain	string
	HostAddress	string
	HostPort	uint16
}

// InitCodecAndDialer : Initialize the code and dialer to make calls to SWS
func InitCodecAndDialer(params SWSParams) (codec.RoundTripper, dialer.Dialer, error) {
	host := params.HostAddress
	codec := rpcv1.New(host)

	certKeyFile := params.ClientCertChain

	// Load client cert and private key
	clientCert, err := tls.LoadX509KeyPair(certKeyFile, certKeyFile)
	if err != nil {
		log.Errorf("Unable to load SWS certificate or key from PEM file: %v", err)
		return nil, nil, err
	}

	caCert, err := ioutil.ReadFile(params.RootCertStore)
	if err != nil {
		log.Errorf("Unable to load Amazon internal root CA cert from PEM file: %v", err)
		return nil, nil, err
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	tlsconfig := &tls.Config{
		RootCAs:		caCertPool,
		Certificates:		[]tls.Certificate{clientCert},
		InsecureSkipVerify:	false,
	}

	dialer, err := dialer.TLS(host, params.HostPort, tlsconfig)
	if err != nil {
		log.Errorf("Unable to establish connection to the host: %v", err)
		return nil, nil, err
	}

	return codec, dialer, nil
}
