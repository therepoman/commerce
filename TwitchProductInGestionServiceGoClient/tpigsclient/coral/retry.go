package coral

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
)

// RetryStrategy structure containing information for retry logic
type RetryStrategy struct {
	NumAttempts         int
	RetryDelay          time.Duration
	RetryableExceptions []string
}

// NewDefaultRetryStrategy creates a new strategy with default logic
func NewDefaultRetryStrategy() RetryStrategy {
	return RetryStrategy{
		NumAttempts: 3,
		RetryDelay:  time.Millisecond * 100,
		RetryableExceptions: []string{
			HTTPFailureException,
			EOFException,
			UnexpectedEOFException,
		},
	}
}

// NewTestingRetryStrategy creates a new strategy with no retry logic
func NewTestingRetryStrategy() RetryStrategy {
	return RetryStrategy{
		NumAttempts:         1,
		RetryDelay:          time.Millisecond * 0,
		RetryableExceptions: []string{},
	}
}

// HTTPFailureException is thrown by coral clients
const HTTPFailureException string = "HttpFailureException"

// EOFException is thrown by SWS
const EOFException string = "EOF"

// UnexpectedEOFException is thrown by SWS
const UnexpectedEOFException string = "unexpected EOF"

// ExecuteCoralCall will perform the given coralCall with retry logic and put the output in the destination
func (r RetryStrategy) ExecuteCoralCall(coralCall func() (interface{}, error), destination interface{}) error {
	destinationValue := reflect.ValueOf(destination)
	if destinationValue.Kind() != reflect.Ptr {
		msg := "Destination kind must be a pointer"
		log.Errorf(msg)
		return errors.New(msg)
	}

	destinationValueElem := destinationValue.Elem()
	if destinationValueElem.Kind() != reflect.Interface {
		msg := "Destination pointer's underlying kind must be an interface"
		log.Errorf(msg)
		return errors.New(msg)
	}

	var coralResponse interface{}
	var err error
	var actualAttempts int
	for attempt := 0; attempt < r.NumAttempts; attempt++ {
		actualAttempts++
		coralResponse, err = coralCall()
		if err == nil {
			break
		}

		log.Warnf("Encountered an error during coral call: %v", err)

		if r.IsRetryableError(err) {
			time.Sleep(r.RetryDelay)
		} else {
			break
		}
	}

	if err != nil {
		msg := fmt.Sprintf("Failed making coral call after %d attempt(s). Underlying error: %v", actualAttempts, err)
		log.Errorf(msg)
		return errors.New(msg)
	}

	coralResponseValue := reflect.ValueOf(coralResponse)
	if !coralResponseValue.Type().Implements(destinationValueElem.Type()) {
		msg := fmt.Sprintf("Coral response type must implement destination interface. Coral response type: %s , Destination interfact: %s", coralResponseValue.Type().String(), destinationValueElem.Type().String())
		log.Errorf(msg)
		return errors.New(msg)
	}

	if !destinationValueElem.CanSet() {
		msg := "Destination interface cannot be set"
		log.Errorf(msg)
		return errors.New(msg)
	}

	destinationValueElem.Set(coralResponseValue)
	return nil
}

// IsRetryableError checks if the given error is in the list of retryable errors for this strategy
func (r RetryStrategy) IsRetryableError(err error) bool {
	errorName := err.Error()
	for _, retryableException := range r.RetryableExceptions {
		if strings.Contains(errorName, retryableException) {
			return true
		}
	}
	return false
}
