/*
 Authentication Runtime Proxy Service (Client)

 Enables Authv4 services to authenticate users via ARPS

*/
package arps

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"io/ioutil"

	"encoding/base64"
	"encoding/json"

	"crypto/tls"

	"crypto/hmac"
	"crypto/sha256"

	"net/http"
	"net/http/httputil"

	"code.justin.tv/commerce/GoAuthV4/src/authv4"
)

const (
	userAgent = "GoAuthV4/1.0"

	claimFormatString = `
{
"__type": "com.amazon.aws.auth.runtime.proxy#AWS4HMACSignatureClaim", 
"AccessKeyId": "%s",
"Date": "%s", 
"Service": "%s", 
"Region": "%s", 
"Terminal": "aws4_request", 
"Data": "%s", 
"Signature": "%s", 
"SignatureMethod": "HmacSHA256"
}
`
)

type ARPSAuthorizer struct {
	*authv4.Authorizer
	authn *arpsClient
}

func NewARPSAuthorizer(region, service, accessKey, secretKey, endpoint string, whitelist []string) *ARPSAuthorizer {
	authn := newARPSClient(region, service, accessKey, secretKey, endpoint)
	return &ARPSAuthorizer{authv4.NewAuthorizer(authn, whitelist), authn}
}

type arpsClient struct {
	region    string // region we are operating in
	service   string // name of service (must be registered and whitelisted!)
	accessKey string // accessKey for service
	secretKey string // secretKey for service

	endpointrpc string // ARS endpoint url for authentication requests

	signer *authv4.Signer // signs requests to authentication server

	log *authv4.Logger
}

func newARPSClient(region, service, accessKey, secretKey, endpoint string) *arpsClient {

	// this signs request to ARS
	signer := authv4.NewSigner(region, "ars", accessKey, secretKey, nil)

	// ARS url
	epurl := "https://" + endpoint + "/rpc"

	return &arpsClient{region, service, accessKey, secretKey, epurl, signer, authv4.NewLogger()}
}

func (authn *arpsClient) SetDebugLogger(debugLogger authv4.DebugLogger) {
	authn.log.SetDebugLogger(debugLogger)
}

func (authn *arpsClient) DebugLoggerEnable() {
	authn.log.DebugLoggerEnable()
}

func (authn *arpsClient) DebugLoggerDisable() {
	authn.log.DebugLoggerDisable()
}

func (authn *arpsClient) DebugLoggerIsEnabled() bool {
	return authn.log.DebugLoggerIsEnabled()
}

// structs for JSON parsing of authentication result
type authnResult struct {
	CryptoOffLoadKeys cryptoKeys
	Subject           authSubject
}
type cryptoKeys struct {
	KeyEncryptionPrefix string
	KeyMap              map[string]string
}
type authSubject struct {
	RootUser authUser
}
type authUser struct {
	Id string
}

// decrypts signingKey (from ARS) in place
func (authn *arpsClient) decryptSigningKey(prefix, credential string, signingKey []byte) {

	/*
	   CONSIDER caching callerSecretBytes ...
	     but might not be needed since we cache the keys anyway, this function should not be called often
	*/
	callerSecretBytes := []byte("AWS4" + authn.secretKey)
	for i, piece := range strings.Split(prefix, "/") {
		if i == 0 {
			continue
		}
		hashcode := hmac.New(sha256.New, callerSecretBytes)
		hashcode.Write([]byte(piece))
		callerSecretBytes = hashcode.Sum(nil)
	}

	hashcode := hmac.New(sha256.New, callerSecretBytes)
	hashcode.Write([]byte(credential))
	encryptionKey := hashcode.Sum(nil)

	// xor
	for i, _ := range signingKey {
		signingKey[i] ^= encryptionKey[i]
	}

}

func (authn *arpsClient) Authenticate(claim *authv4.AuthV4Claim) (*authv4.AuthenticatedClient, error) {

	// send request to endpoint to get signing key (called a 'cryptoOffloadKey') and other client info

	tlsconfig := &tls.Config{InsecureSkipVerify: true}

	tr := &http.Transport{TLSClientConfig: tlsconfig}

	client := &http.Client{Transport: tr}

	_, str2sign, err := claim.GetStringToSign()
	// THIS CAN'T HAPPEN: the SOLE cause would be from a ReadSeeker in the req.Body which cannot happen in a server
	if err != nil { // future proof
		return nil, err
	}

	claimString := fmt.Sprintf(claimFormatString,
		claim.AccessKey,
		claim.Day,
		claim.Service,
		claim.Region,
		base64.StdEncoding.EncodeToString([]byte(str2sign)),
		claim.Signature)

	query_json := fmt.Sprintf(`{ "Claims": [ %s ] }`, claimString)

	req, err := http.NewRequest("POST", authn.endpointrpc, bytes.NewReader([]byte(query_json)))
	if err != nil {
		return nil, err
	}

	// headers
	req.Header.Add("user-agent", userAgent)
	req.Header.Add("content-encoding", "amz-1.0")
	req.Header.Add("x-amz-target", "com.amazon.aws.auth.runtime.proxy.AWSAuthRuntimeProxyService.Authenticate")
	req.Header.Add("content-type", "application/json")
	req.Header.Add("x-amz-requestid", fmt.Sprintf("%p-%d", req, claim.Created))

	// v4 signing
	authn.signer.Sign(req)

	if authn.log.DebugLoggerIsEnabled() {
		dump, _ := httputil.DumpRequest(req, true)
		authn.log.Debugf("Request:\n%s\n", string(dump[:]))
	}

	// go!
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if authn.log.DebugLoggerIsEnabled() {
		dump, _ := httputil.DumpResponse(res, true)
		authn.log.Debugf("Response:\n%s\n", string(dump[:]))
	}

	response_json, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("%s:\n%s", res.Status, response_json))
	}

	// parse json blob for what we want

	var parsed_json authnResult
	err = json.Unmarshal(response_json, &parsed_json)
	if err != nil {
		return nil, err
	}

	// accountId
	accountId := parsed_json.Subject.RootUser.Id
	if accountId == "" {
		return nil, errors.New(fmt.Sprintf("Cannot find accountId for %s", claim.AccessKey))
	}

	// signingKey
	signingKeyText, present := parsed_json.CryptoOffLoadKeys.KeyMap[claim.Credential]
	if !present || signingKeyText == "" {
		return nil, errors.New(fmt.Sprintf("Cannot find signing key (CryptoOffloadKey) for %s", claim.Credential))
	}
	signingKey, err := base64.StdEncoding.DecodeString(signingKeyText)
	if err != nil {
		return nil, err
	}
	// decrypt
	keyEncryptionPrefix := parsed_json.CryptoOffLoadKeys.KeyEncryptionPrefix
	authn.decryptSigningKey(keyEncryptionPrefix, claim.Credential, signingKey)

	ac := authv4.NewAuthenticatedClient(claim.AccessKey, accountId, signingKey)

	// if client is requesting a specific expiration time, set
	expires := claim.Request.Header.Get("X-Amz-Expires")
	if expires != "" {
		if seconds, err := strconv.Atoi(expires); err == nil {
			ac.SetExpires(ac.Created().Add(time.Duration(seconds) * time.Second))
		}
	}

	return ac, nil
}

// Reference snippets below...

/*


Example ARPS successful JSON result

{

"AuthNPolicyPaths":[],
"AuthorizationContext":{},
"CryptoOffloadKeys":{"KeyEncryptionPrefix":"AKIAJMXNHDTDHQZLEKLQ/20140217/us-east-1/ars_co","KeyMap":{"AKIAJMXNHDTDHQZLEKLQ/20140217/us-east-1/maestroproxy/aws4_request":"/F26DWxD4eNAKsf2SpsakFI9VhIBgak7Lku4vh1dUzg="}},
"Injections":{"PayerId":"436719000146"},
"Policies":[],
"ResponseDetail":{"Code":"Success","Message":"SUCCESS","RequestId":"ade4e6c5-9789-11e3-8716-51c5d3fb842e","ServerName":"aws-arps-7005.iad7.amazon.com"},
"Subject":{

 "Caller":{"Arn":"arn:aws:iam::797360199340:root","Domain":"AWS","Groups":[],"Id":"797360199340","Provider":"AmazonNA","Reference":"A1TKE3BDPZBUNX","RuntimePartition":"aws","Type":"Account","hasICPLicense":false},

 "RootUser":{
  "Arn":"arn:aws:iam::797360199340:root","Domain":"AWS","Groups":[],"Id":"797360199340","Provider":"AmazonNA","Reference":"A1TKE3BDPZBUNX","RuntimePartition":"aws","Type":"Account","hasICPLicense":false
  },

  "User":{"Arn":"arn:aws:iam::797360199340:root","Domain":"AWS","Groups":[],"Id":"797360199340","Provider":"AmazonNA","Reference":"A1TKE3BDPZBUNX","RuntimePartition":"aws","Type":"Account","hasICPLicense":false
  }

 }

}

*/

/*

public static Collection<CryptoOffloadKey> getDecryptedOffloadKeys(String signingMethod, String callerAKID, String callerSecret,
     74             String keyEncryptionPrefix, Map<String, byte[]> keyMap) {
     75         try {
     76             byte[] callerSecretBytes = KeyEncryptor.deriveKey(signingMethod, callerAKID, callerSecret, keyEncryptionPrefix);
     77
     78             Collection<CryptoOffloadKey> keys = new ArrayList<CryptoOffloadKey>();
     79             for(Map.Entry<String, byte[]> entry : keyMap.entrySet()) {
     80                 byte[] keyEncryptionKey = Signature.calculateRawRFC2104HMAC(entry.getKey().getBytes(), callerSecretBytes, signingMethod);
     81
     82                 CryptoOffloadKey k = new CryptoOffloadKeyImpl(signingMethod, entry.getKey(), KeyEncryptor.xorByteArray(entry.getValue(), keyEncryptionKey));
     83                 keys.add(k);
     84             }
     85             return keys;
     86         } catch (Exception e) {
     87             LOG.warn("Error decrypting crypto offload keys.", e);
     88             return null;
     89         }
     90     }

*/

/*
public static byte[] deriveKey(String signingMethod, String callerAKID, String callerSecret, String keyEncryptionScope) throws SignatureException {
     31         byte[] callerSecretBytes = ("AWS4" + callerSecret).getBytes();
     32         String[] keyEncryptionPieces = SLASH_DELIM.split(keyEncryptionScope);
     33         if (!callerAKID.equals(keyEncryptionPieces[0])) {
     34             throw new IllegalArgumentException("Server returned key encryption scope " + keyEncryptionScope + " that does not start with " + callerAKID);
     35         }
     36
     37         for (int i=1; i < keyEncryptionPieces.length; i++) {
     38             callerSecretBytes = Signature.calculateRawRFC2104HMAC(keyEncryptionPieces[i].getBytes(), callerSecretBytes, signingMethod);
     39         }
     40
     41         return callerSecretBytes;
     42     }
     43

*/
