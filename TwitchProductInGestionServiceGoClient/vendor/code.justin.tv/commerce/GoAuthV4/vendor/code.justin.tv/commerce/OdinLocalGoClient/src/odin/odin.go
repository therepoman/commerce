package odin

import (
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	Timeout = time.Second * 5 // Timeout after this
)

var (
	client = http.Client{Timeout: Timeout}
)

type Material struct {
	Serial    int64
	NotBefore time.Time
	NotAfter  time.Time
}

type PrivateKeyMaterial struct {
	PrivateKey *rsa.PrivateKey
	Material
}

type PublicKeyMaterial struct {
	PublicKey *rsa.PublicKey
	Material
}

type SymmetricKeyMaterial struct {
	SymmetricKey []byte
	Material
}

func decodeSymmetricKey(c *material) (*SymmetricKeyMaterial, error) {
	key, err := base64.StdEncoding.DecodeString(c.MaterialData)
	if err != nil {
		return nil, err
	}
	skm := SymmetricKeyMaterial{
		SymmetricKey: key,
		Material: Material{
			Serial:    c.MaterialSerial,
			NotBefore: time.Unix(c.NotBefore, 0),
			NotAfter:  time.Unix(c.NotAfter, 0),
		},
	}
	return &skm, nil
}

func SymmetricKeyBySerial(materialSet string, serial int64) (*SymmetricKeyMaterial, error) {
	c, err := retrieve(materialSet, "SymmetricKey", &serial)
	if err != nil {
		return nil, err
	}
	return decodeSymmetricKey(c)
}

func SymmetricKey(materialSet string) (*SymmetricKeyMaterial, error) {
	c, err := retrieve(materialSet, "SymmetricKey", nil)
	if err != nil {
		return nil, err
	}
	return decodeSymmetricKey(c)
}

func CredentialPair(materialSet string) (principal, credential string, err error) {
	principal, credential, _, _, err = credentialPairBySerial(materialSet, nil, nil)
	return
}

func CredentialPairAndSerial(materialSet string) (principal, credential string, principalSerial, credentialSerial int64, err error) {
	return credentialPairBySerial(materialSet, nil, nil)
}

func CredentialPairBySerial(materialSet string, principalSerial int64, credentialSerial int64) (principal, credential string, err error) {
	principal, credential, _, _, err = credentialPairBySerial(materialSet, &principalSerial, &credentialSerial)
	return
}

func credentialPairBySerial(materialSet string, principalSerial *int64, credentialSerial *int64) (principal, credential string, pSerial, cSerial int64, err error) {
	var c, p *material
	c, err = retrieve(materialSet, "Credential", principalSerial)
	if err != nil {
		return
	}
	p, err = retrieve(materialSet, "Principal", credentialSerial)
	if err != nil {
		return
	}
	var temp []byte
	temp, err = base64.StdEncoding.DecodeString(c.MaterialData)
	if err != nil {
		return
	}
	credential = string(temp)
	cSerial = c.MaterialSerial
	temp, err = base64.StdEncoding.DecodeString(p.MaterialData)
	if err != nil {
		return
	}
	principal = string(temp)
	pSerial = p.MaterialSerial
	return
}

func Certificate(materialSet string) (certificate *tls.Certificate, err error) {
	var c, p *material
	c, err = retrieve(materialSet, "Certificate", nil)
	if err != nil {
		return
	}
	p, err = retrieve(materialSet, "PrivateKey", nil)
	if err != nil {
		return
	}
	certDER := []byte{}
	certDER, err = base64.StdEncoding.DecodeString(c.MaterialData)
	if err != nil {
		return
	}
	keyDER := []byte{}
	keyDER, err = base64.StdEncoding.DecodeString(p.MaterialData)
	if err != nil {
		return
	}
	var key interface{}
	key, err = x509.ParsePKCS8PrivateKey(keyDER)
	if err != nil {
		return
	}
	var leaf *x509.Certificate
	leaf, err = x509.ParseCertificate(certDER)
	if err != nil {
		return
	}
	certificate = &tls.Certificate{
		Certificate: [][]byte{certDER},
		PrivateKey:  key,
		Leaf:        leaf,
	}
	return
}

func decodePrivateKey(p *material) (*PrivateKeyMaterial, error) {
	keyDER, err := base64.StdEncoding.DecodeString(p.MaterialData)
	if err != nil {
		return nil, err
	}
	key, err := x509.ParsePKCS8PrivateKey(keyDER)
	if err != nil {
		return nil, err
	}
	if rsakey, ok := key.(*rsa.PrivateKey); ok {
		akm := PrivateKeyMaterial{
			PrivateKey: rsakey,
			Material: Material{
				Serial:    p.MaterialSerial,
				NotBefore: time.Unix(p.NotBefore, 0),
				NotAfter:  time.Unix(p.NotAfter, 0),
			},
		}
		return &akm, nil
	}
	return nil, errors.New("found non-RSA private key in PKCS#8 wrapping")
}

func decodePublicKey(p *material) (*PublicKeyMaterial, error) {
	keyDER, err := base64.StdEncoding.DecodeString(p.MaterialData)
	if err != nil {
		return nil, err
	}
	i, err := x509.ParsePKIXPublicKey(keyDER)
	if err != nil {
		return nil, err
	}
	if key, ok := i.(*rsa.PublicKey); ok {
		akm := PublicKeyMaterial{
			PublicKey: key,
			Material: Material{
				Serial:    p.MaterialSerial,
				NotBefore: time.Unix(p.NotBefore, 0),
				NotAfter:  time.Unix(p.NotAfter, 0),
			},
		}
		return &akm, nil
	}
	return nil, errors.New("Got unexpected key type")
}

func PrivateKeyBySerial(materialSet string, serial int64) (*PrivateKeyMaterial, error) {
	p, err := retrieve(materialSet, "PrivateKey", &serial)
	if err != nil {
		return nil, err
	}
	return decodePrivateKey(p)
}

func PrivateKey(materialSet string) (*PrivateKeyMaterial, error) {
	p, err := retrieve(materialSet, "PrivateKey", nil)
	if err != nil {
		return nil, err
	}
	return decodePrivateKey(p)
}

func PublicKeyBySerial(materialSet string, serial int64) (*PublicKeyMaterial, error) {
	p, err := retrieve(materialSet, "PublicKey", &serial)
	if err != nil {
		return nil, err
	}
	return decodePublicKey(p)
}

func PublicKey(materialSet string) (*PublicKeyMaterial, error) {
	p, err := retrieve(materialSet, "PublicKey", nil)
	if err != nil {
		return nil, err
	}
	return decodePublicKey(p)
}

/*
{"Error":{"Type":"Sender","Message":"Cannot find material 'com.amazon.prediction.dao.OdinMWSCredential.??'. (Reason: Serial ?? not found for materials ('Principal') named 'com.amazon.prediction.dao.OdinMWSCredential')","Code":"MaterialNotFoundException"}}
*/

type retrieveResult struct {
	Material *material `json:"material"`
	Error    *retrieveError
}

type retrieveError struct {
	Type    string
	Message string
}

func (err retrieveError) Error() string {
	return err.Message
}

type material struct {
	MaterialType   string `json:"materialType"`
	NotBefore      int64  `json:"notBefore"`
	NotAfter       int64  `json:"notAfter"`
	MaterialData   string `json:"materialData"`
	MaterialSerial int64  `json:"materialSerial"`
	MaterialName   string `json:"materialName"`
}

func retrieve(materialSet, materialType string, serial *int64) (*material, error) {

	query := url.Values{}
	query.Set("material.materialName", materialSet)
	query.Set("material.materialType", materialType)
	query.Set("ContentType", "JSON")
	query.Set("Operation", "Retrieve")
	if serial != nil {
		query.Set("material.materialSerial", strconv.FormatInt(*serial, 10))
	}

	request, _ := http.NewRequest("GET", "http://localhost:2009/query", nil)
	request.URL.RawQuery = query.Encode()

	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	var result retrieveResult
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}

	switch {
	case result.Error != nil:
		return result.Material, result.Error
	case result.Material == nil:
		return nil, errors.New("Couldn't parse response")
	default:
		return result.Material, nil
	}
}
