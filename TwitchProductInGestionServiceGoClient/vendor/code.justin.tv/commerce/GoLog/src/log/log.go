/* Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package log

import (
	"io"
)

var globalLogger Logger

type Logger interface {
	Trace(v ...interface{})
	Tracef(format string, v ...interface{})
	Debug(v ...interface{})
	Debugf(format string, v ...interface{})
	Info(v ...interface{})
	Infof(format string, v ...interface{})
	Warn(v ...interface{})
	Warnf(format string, v ...interface{})
	Error(v ...interface{})
	Errorf(format string, v ...interface{})
	Fatal(v ...interface{})
	Fatalf(format string, v ...interface{})
}

// Sets the global logger to the specified logger instance.
func SetLogger(logger Logger) {
	globalLogger = logger
}

// Closes the global logger if possible (it must implement the io.Closer interface) then removes it.
func Close() error {
	if globalLogger == nil {
		return nil
	} else if s, ok := globalLogger.(io.Closer); ok {
		globalLogger = nil
		return s.Close()
	} else {
		globalLogger = nil
		return nil
	}
}

// Prints the Trace log with the global logger.
func Trace(v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Trace(v...)
	}
}

// Prints the Trace log with the global logger.
func Tracef(format string, v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Tracef(format, v...)
	}
}

// Prints the Debug log with the global logger.
func Debug(v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debug(v...)
	}
}

// Prints the Debug log with the global logger.
func Debugf(format string, v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debugf(format, v...)
	}
}

// Prints the Info log with the global logger.
func Info(v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Info(v...)
	}
}

// Prints the Info log with the global logger.
func Infof(format string, v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Infof(format, v...)
	}
}

// Prints the Warn log with the global logger.
func Warn(v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warn(v...)
	}
}

// Prints the Warn log with the global logger.
func Warnf(format string, v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warnf(format, v...)
	}
}

// Prints the Error log with the global logger.
func Error(v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Error(v...)
	}
}

// Prints the Error log with the global logger.
func Errorf(format string, v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Errorf(format, v...)
	}
}

// Prints the Fatal log with the global logger.
func Fatal(v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatal(v...)
	}
}

// Prints the Fatal log with the global logger.
func Fatalf(format string, v ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatalf(format, v...)
	}
}
