/* Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package log

import (
	"code.justin.tv/commerce/GoFileRotate/src/rotate"
	"code.justin.tv/commerce/GoLog/src/log/level"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

const (
	defaultBufferSize    = 100
	defaultDateFormat    = "Mon Jan 02 15:04:05 2006 UTC"
	defaultDepth         = -1
	defaultFlushDuration = 1 * time.Second
	defaultStopTimeout   = 1 * time.Second
	defaultLogLevel      = level.Debug
	defaultPrefix        = "GoLog/log"
	defaultSrcSeparator  = string(filepath.Separator) + "src" + string(filepath.Separator)
)

type AmznLogger struct {
	depth           int
	defaultLevel    level.Level
	writer          io.Writer
	packageLevels   map[string]level.Level
	applicationName string
	dateFormat      string
	hostname        string
	prefix          string
	stopTimeout     time.Duration
	flushChan       <-chan time.Time
	stopChan        chan bool
	writeChan       chan logRecord
}

type logRecord struct {
	line     int
	pid      int
	level    level.Level
	buff     string
	caller   string
	funcName string
}

// Used to enable AmznLogger to flush buffers of the writer if it exposes a Flush() function, such as bufio.Writer.
// Buffers must be flushed to ensure all logs get written out.
type Flusher interface {
	Flush() error
}

// Starts a new Amazon Logger instance that generates logs in the standard Amazon application log format.
// The logger writes to a specified io.Writer interface such as the console, or a file.
// Any number of unique options may be passed in to configure the AmznLogger instance.
func NewAmznLogger(writer io.Writer, applicationName string, options ...func(*AmznLogger) error) (*AmznLogger, error) {
	if writer == nil {
		return nil, errors.New("writer must be specified.")
	}
	hostname, _ := os.Hostname()
	l := &AmznLogger{
		writer:          writer,
		applicationName: applicationName,
		defaultLevel:    defaultLogLevel,
		hostname:        hostname,
		dateFormat:      defaultDateFormat,
		prefix:          defaultPrefix,
		depth:           defaultDepth,
		stopTimeout:     defaultStopTimeout,
		flushChan:       make(chan time.Time),
		writeChan:       make(chan logRecord, defaultBufferSize),
		stopChan:        make(chan bool),
	}
	for _, option := range options {
		err := option(l)
		if err != nil {
			return nil, fmt.Errorf("Error creating AmznLogger. Error while setting options: %v", err)
		}
	}
	// Start Go Routine that writes out logs.
	go l.writeChannelProcessor()
	return l, nil
}

// Constructor option to set the default LogLevel for all packages.
func LogLevel(level level.Level) func(*AmznLogger) error {
	return func(l *AmznLogger) error {
		l.defaultLevel = level
		return nil
	}
}

// Constructor option to set a custom date/time format.
func DateFormat(format string) func(*AmznLogger) error {
	return func(l *AmznLogger) error {
		l.dateFormat = format
		return nil
	}
}

// Constructor option to specify the size of the internal buffer.
// Default buffer size is 100 log messages.
func BufferSize(size int) func(*AmznLogger) error {
	return func(l *AmznLogger) error {
		if size < 0 {
			return errors.New("BufferSize must be >= 0.")
		}
		l.writeChan = make(chan logRecord, size)
		return nil
	}
}

// Constructor option to specify a package prefix for determining callstack depth.
// This should only be used when using a proxy in conjunction with the AmznLogger.
func LoggerPackagePrefix(prefix string) func(*AmznLogger) error {
	return func(l *AmznLogger) error {
		if len(prefix) == 0 {
			return errors.New("PackagePrefix length must be > 0!")
		}
		l.prefix = prefix
		return nil
	}
}

// Constructor option to specify the timeout when stopping the logger.
// Input time is time.Duration and must be >= 0. The default timeout is 1 second.
func StopTimeout(t time.Duration) func(*AmznLogger) error {
	return func(l *AmznLogger) error {
		if t < 0 {
			return fmt.Errorf("Stop Timeout must be >= 0. Specified timeout: %v", t)
		}
		l.stopTimeout = t
		return nil
	}
}

// Sets the specified package to the specified log level.
func (l *AmznLogger) AddPackageLogLevel(pack string, level level.Level) {
	l.packageLevels[pack] = level
}

// Sets the specified packages to the specified log levels.
func (l *AmznLogger) AddPackageLogLevelMap(m map[string]level.Level) {
	for k, v := range m {
		l.AddPackageLogLevel(k, v)
	}
}

// Gets the package log level for the specified package.
func (l *AmznLogger) GetPackageLogLevel(pack string) *level.Level {
	if val, ok := l.packageLevels[pack]; ok {
		return &val
	}
	return nil
}

// Get the package log levels for the specified packages.
func (l *AmznLogger) GetPackageLogLevelMap(packs []string) map[string]*level.Level {
	result := make(map[string]*level.Level, len(packs))
	for _, val := range packs {
		result[val] = l.GetPackageLogLevel(val)
	}
	return result
}

// Removes the log level from the specified package.
func (l *AmznLogger) RemovePackageLogLevel(pack string) {
	delete(l.packageLevels, pack)
}

// Remotes the log levels from the specified packages.
func (l *AmznLogger) RemovePackageLogLevelSlice(packs []string) {
	for _, val := range packs {
		l.RemovePackageLogLevel(val)
	}
}

func (l *AmznLogger) writeChannelProcessor() {
	flushable, _ := l.writer.(Flusher)
	if flushable != nil {
		// Writer is flushable. Ensure buffer is flushed when logger exits.
		defer flushable.Flush()
		l.flushChan = time.Tick(defaultFlushDuration)
	}
	for {
		select {
		case record := <-l.writeChan:
			l.writeLog(record)
		case <-l.stopChan:
			// Set stopChan to nil, close the writeChan, then dump all logs to the logger. If the writer is flushable, the defered flush will flush it.
			l.stopChan = nil
			close(l.writeChan)
			for r := range l.writeChan {
				l.writeLog(r)
			}
			return
		case <-l.flushChan:
			flushable.Flush()
		}
	}
}

func (l *AmznLogger) writeLog(record logRecord) {
	date := time.Now().UTC()
	str := fmt.Sprintf("%v %v %v-0@%v:0 %v %v:%v %v(): %v", date.Format(l.dateFormat), l.applicationName, record.pid, l.hostname, level.LevelStr(record.level), record.caller, record.line, record.funcName, record.buff)
	switch t := l.writer.(type) {
	case rotate.TemporalWriter:
		t.TimedWrite([]byte(str), &date)
	case io.Writer:
		t.Write([]byte(str))
	}
}

func (l *AmznLogger) enqueueLog(message string, level level.Level, file string, line int, funcName string) {
	if l.stopChan != nil {
		// If writeChan has been closed recover from the panic and continue. Unlikely to happen, but protecting against corner cases.
		defer func() {
			if r := recover(); r != nil {
				os.Stderr.Write([]byte("[ERROR] Unable to log message as logger has been stopped. Panic recovered."))
			}
		}()
		pid := os.Getpid()
		l.writeChan <- logRecord{
			line:     line,
			pid:      pid,
			level:    level,
			buff:     message,
			caller:   file,
			funcName: funcName,
		}
	} else {
		os.Stderr.Write([]byte("[ERROR] Unable to log message as logger has been stopped!"))
	}
}

// Gets the LogLevel for the specified function name.
// Checks if the package of the function has a specified LogLevel. It does this by splitting on the '.' to isolate the package
// the splits on '/' ti isolate package levels. Log level is determined by stripping off package levels until it finds the most specific
// package with a custom logging level. If all package levels are stripped and no level is found, it defaults to the specified default logging level.
func (l *AmznLogger) getLogLevel(funcName string) level.Level {
	pname := strings.Split(funcName, ".")
	p := strings.Split(pname[0], "/")
	for i := len(p); i > 0; i-- {
		if val, ok := l.packageLevels[strings.Join(append(p[0:i]), "/")]; ok {
			return val
		}
	}
	return l.defaultLevel
}

func (l *AmznLogger) logln(level level.Level, v ...interface{}) {
	file, line, funcName := l.getFileLineFuncName()
	minLevel := l.getLogLevel(funcName)
	if minLevel > level {
		return
	}
	l.enqueueLog(fmt.Sprintln(v...), level, file, line, funcName)
}

func (l *AmznLogger) logf(format string, level level.Level, v ...interface{}) {
	file, line, funcName := l.getFileLineFuncName()
	minLevel := l.getLogLevel(funcName)
	if minLevel > level {
		return
	}
	if format[len(format)-1] != '\n' {
		format = fmt.Sprintln(format)
	}
	l.enqueueLog(fmt.Sprintf(format, v...), level, file, line, funcName)
}

func (l *AmznLogger) Close() error {
	// If the logger hasn't already been close (stopChan is not nil), attempt to stop the logger.
	// To protect against the corner case of two simultaneous Close calls, timeout after 1 second to ensure client isn't blocked
	// trying to Close an already closed logger.
	if l.stopChan != nil {
		c := make(chan bool, 1)
		go func() {
			defer func() {
				if r := recover(); r != nil {
					c <- false
				}
			}()
			l.stopChan <- true
			c <- true
		}()
		select {
		case <-c:
			return nil
		case <-time.After(l.stopTimeout):
			return fmt.Errorf("Failed to Close logger. Timed out after %v second", l.stopTimeout)
		}
	}
	// Logger already stopped.
	return nil
}

// Checks to see if this logger is actively logging, or has been stoped.
// Return true if actively logging, false if it been stopped.
func (l *AmznLogger) Running() bool {
	return l.stopChan != nil
}

func (l *AmznLogger) Trace(v ...interface{}) {
	l.logln(level.Trace, v...)
}

func (l *AmznLogger) Tracef(format string, v ...interface{}) {
	l.logf(format, level.Trace, v...)
}

func (l *AmznLogger) Debug(v ...interface{}) {
	l.logln(level.Debug, v...)
}

func (l *AmznLogger) Debugf(format string, v ...interface{}) {
	l.logf(format, level.Debug, v...)
}

func (l *AmznLogger) Info(v ...interface{}) {
	l.logln(level.Info, v...)
}

func (l *AmznLogger) Infof(format string, v ...interface{}) {
	l.logf(format, level.Info, v...)
}

func (l *AmznLogger) Warn(v ...interface{}) {
	l.logln(level.Warn, v...)
}

func (l *AmznLogger) Warnf(format string, v ...interface{}) {
	l.logf(format, level.Warn, v...)
}

func (l *AmznLogger) Error(v ...interface{}) {
	l.logln(level.Error, v...)
}

func (l *AmznLogger) Errorf(format string, v ...interface{}) {
	l.logf(format, level.Error, v...)
}

func (l *AmznLogger) Fatal(v ...interface{}) {
	l.logln(level.Fatal, v...)
}

func (l *AmznLogger) Fatalf(format string, v ...interface{}) {
	l.logf(format, level.Fatal, v...)
}

// Gets the file path, line number, and function name of where the log statement was called.
func (l *AmznLogger) getFileLineFuncName() (file string, line int, funcName string) {
	if l.depth == defaultDepth {
		// Identify what callstack depth to retrieve file, line number, and function name.
		// Only runs the first time something is logged.
		seenPrefix := false
		for i := 1; true; i++ {
			pc, _, _, _ := runtime.Caller(i)
			funcName = runtime.FuncForPC(pc).Name()
			if funcName == "" {
				// Reached top of callstack and depth not found. Will return blank path, line number, and function name.
				// This likely indicates the specified prefix does not exist in the callstack.
				l.depth = i
				break
			}
			// Iterate up the call stack until we find the first occurence of the prefix.
			// Continue iterating until we no longer find the prefix and return.
			hasPrefix := strings.HasPrefix(funcName, l.prefix)
			if seenPrefix && !hasPrefix {
				l.depth = i
				break
			}
			if !seenPrefix && hasPrefix {
				seenPrefix = true
			}
		}
	}
	pc, file, line, _ := runtime.Caller(l.depth)
	funcName = runtime.FuncForPC(pc).Name()
	if p := strings.LastIndex(file, defaultSrcSeparator); p >= 0 {
		return file[p+len(defaultSrcSeparator):], line, funcName
	}
	return file, line, funcName
}
