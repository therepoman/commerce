// Package rpcv0 implents a codec that satisfies coral/codec.Codec, coral/codec.RoundTripper,
// and coral/codec.Server
//
// RPCv0 conforms to the on-the-wire protocol specified by the following link:
// https://w.amazon.com/index.php/Coral/Manual/Protocols#RPCv0

// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"code.justin.tv/commerce/GoAuthV4/src/authv4"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
)

var _ codec.Codec = RPCv0{}
var _ codec.RoundTripper = RPCv0{}
var _ codec.Server = RPCv0{}

// Option is a function that can be provided to configure an RPCv0 codec
type Option func(*RPCv0)

// RPCv0 satisfies the codec.Codec, codec.RoundTripper, and codec.Server interfaces.
//
//Unless otherwise specified these values default to the standard Go default values for their types
type RPCv0 struct {
	Host      string // host address for service. Only Clients need to set this value
	Path      string // Defaults to "/"
	SignerV4  *authv4.Signer
	AAAClient aaa.Client
}

// New returns an initialized RPCv0 struct. This function is appropriate for instantiating codecs for
// server implementations
func New(options ...Option) RPCv0 {
	v0 := RPCv0{}

	return new(v0, options...)
}

// NewClientCodec returns an RPCv0 initialized with a host. This function is appropriate for instantiating
// codecs for clients
func NewClientCodec(host string, options ...Option) RPCv0 {
	v0 := RPCv0{
		Host: host,
	}

	return new(v0, options...)
}

// SetSignerV4 is an option function to set RPCv0 Signer
func SetSignerV4(signer *authv4.Signer) Option {
	return (Option)(func(rpcv0 *RPCv0) {
		rpcv0.SignerV4 = signer
	})
}

// SetAAAClient is an option function to set a RPCv0 AAA client
func SetAAAClient(aaaClient aaa.Client) Option {
	return (Option)(func(rpcv0 *RPCv0) {
		rpcv0.AAAClient = aaaClient
	})
}

// new is the main initializer that the exported initalizers call. This functions ensures
// the default values are set appropriately
func new(v0 RPCv0, options ...Option) RPCv0 {
	for _, option := range options {
		option(&v0)
	}

	if v0.Path == "" {
		v0.Path = "/"
	}

	return v0
}
