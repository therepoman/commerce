// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/internal/test/fakemodel"
	"testing"
)

func TestMarshalInputAndUnmarshalInput(t *testing.T) {
	userName := "not-a-user"
	input := fakemodel.NewFakeInput()
	input.SetUser(userName)

	rpc := New()
	b, err := rpc.MarshalInput(
		input,
		fakemodel.TestOperation.ShapeName,
		fakemodel.TestService.ShapeName,
	)

	if err != nil {
		t.Error("expected error from MarshalInput to be nil", err)
	}

	if len(b) == 0 {
		t.Error("expected number of bytes to be greater than zero:", err)
	}

	unmarshaledInput := fakemodel.NewFakeInput()
	err = rpc.UnmarshalInput(b, &unmarshaledInput)

	if err != nil {
		t.Error("expected error from UnmarshalInput to be nil:", err)
	}

	if unmarshaledInput.User() != userName {
		t.Error("expected unmarshaled input field user name to be", userName)
		t.Logf("unmarshaledInput: %+v", unmarshaledInput)
	}
}

func TestMarshalOutputAndUnmarshalOutput(t *testing.T) {
	outStr := "this is nifty"
	output := fakemodel.NewFakeOutput()
	output.SetOutput(outStr)

	rpc := New()
	b, err := rpc.MarshalOutput(output)

	if err != nil {
		t.Error("expected error from MarshalOutput to be nil but was", err)
	}

	if len(b) == 0 {
		t.Error("expected length of marshaled output to be non zero")
	}

	unmarshaledOutput := fakemodel.NewFakeOutput()
	err = rpc.UnmarshalOutput(b, &unmarshaledOutput)

	if err != nil {
		t.Error("expected err from UnmarshalOutput to be nil but was", err)
	}

	if unmarshaledOutput.Output() != outStr {
		t.Errorf("expected unmarshaled field Output field to be '%s'", outStr)
		t.Logf("unmarshaledOutput: %+v", unmarshaledOutput)
	}
}
