// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"bytes"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/internal/test/fake"
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/internal/test/fakemodel"
	"testing"
)

const (
	validHTTPResponse = `HTTP/1.1 200 OK
x-amzn-RequestId: 954ec7e5-f4df-11e4-a33e-535b6bee04a1
Content-Type: application/json
Content-Length: 79
Date: Thu, 07 May 2015 17:36:24 GMT

{"Output": {"__type": "rpcv1_test#FakeOutput", "output": "1"},"Version": "1.0"}`
)

func TestRoundTrip(t *testing.T) {
	rpc := NewClientCodec("example:8080")
	cr := &codec.Request{
		Service:   fakemodel.TestService,
		Operation: fakemodel.TestOperation,
	}

	rule := fakemodel.NewFakeRule()
	rule.SetIdentifier("foo")
	rule.SetSource("LDAP")
	rules := []fakemodel.FakeRule{
		rule,
	}
	input := fakemodel.NewFakeInput()
	input.SetUser("not-a-real-user")
	input.SetRules(rules)
	cr.Input = input

	output := fakemodel.NewFakeOutput()
	output.SetOutput("1")
	cr.Output = &output

	// Test the happy case
	err := rpc.RoundTrip(
		cr,
		&fake.Pipe{ReadBuf: bytes.NewBufferString(validHTTPResponse)},
	)

	if err != nil {
		t.Error("expected error from round trip to be nil:", err)
	}

	if output.Output() != "1" {
		t.Errorf("expected Output field in FakeOutput to be '1'")
		t.Logf("FakeOutput: %+v", output)
	}

}
