// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/internal/test/fake"
	"testing"
)

func TestNew(t *testing.T) {
	rpc := New()

	if rpc.Path == "" {
		t.Error("expected common initializer to set Path field")
	}
}

func TestNewClientCodec(t *testing.T) {
	host := "localhost"
	rpc := NewClientCodec(
		host,
		SetAAAClient(&fake.AAA{}),
		SetSignerV4(fake.Signer),
	)

	if rpc.Host != host {
		t.Errorf("expected host to be %s but was %s", host, rpc.Host)
	}

	if rpc.AAAClient == nil {
		t.Error("expected AAA client to be non nil")
	}

	if rpc.SignerV4 == nil {
		t.Error("expected SignerV4 field to be non nil")
	}

	if rpc.Path == "" {
		t.Error("expected common initializer to set Path field")
	}
}
