// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/internal/test/fakemodel"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

// This codec was created to support the CoralRubySuperClient. That being said a best effort should be made to support all clients using RPCv0
//
// The following is some example metadata pulled from when CoralRubySuperClient is making an HTTP request.
//
// Verb:
//  "POST"
// URI:
// 	http://localhost:50051/
// Content-Type:
//	"application/json; charset=UTF-8"
// Http Body:
//   "{\"Operation\":\"com.amazon.goodreadstoplistsservice#FindTopList\",\"Service\":\"com.amazon.goodreadstoplistsservice#GoodreadsTopListsService\",\"Input\":{\"name\":\"test_list\",\"location\":\"world\",\"period\":\"all_time\",\"__type\":\"com.amazon.goodreadstoplistsservice#FindTopListInput\"}}",
// Headers:
// {
//		"x-amzn-requestid"=>["69e9cc89-00e0-4183-bef9-c42df8c729de"],
//       "content-type"=>["application/json; charset=UTF-8"],
//       "x-amz-date"=>["Thu, 05 Jan 2017 20:10:08 GMT"],
//       "x-amzn-client-ttl-seconds"=>["5.0"],
//       "x-amz-target"=>["com.amazon.goodreadstoplistsservice.GoodreadsTopListsService.FindTopList"]
// }

func TestIsSupported(t *testing.T) {
	c := New()

	// Note on http.Header:
	// It will normalize the header name using textproto.MIMEHeader. So there's no need to check
	// variations like content-type or content-Type as Content-Type will do.
	//
	// https://golang.org/src/net/http/header.go
	tests := []struct {
		in       codec.Getter
		expected bool
	}{
		{in: http.Header{headerContentType: []string{""}, headerAmznDate: []string{""}}, expected: false},
		{in: http.Header{headerContentType: []string{"application/xml"}, headerAmznDate: []string{"2016"}}, expected: false},
		{in: http.Header{headerContentType: []string{"application/json"}, headerAmznDate: []string{""}}, expected: false},
		{in: http.Header{headerContentType: []string{"application/json; charset=UTF-8"}, headerAmznDate: []string{"2016"}}, expected: true},
		{in: http.Header{headerContentType: []string{"application/json"}, headerAmznDate: []string{"Thu, 05 Jan 2017 20:10:08 GMT"}}, expected: true},
		{in: http.Header{headerContentType: []string{"application/json"}, headerAmznDate: []string{"Thu, 05 Jan 2017 20:10:08 GMT"}, headerAmznTarget: []string{"Coral#Target"}}, expected: true},
		{in: http.Header{headerContentType: []string{"application/json"}, headerAmznDate: []string{"Thu, 05 Jan 2017 20:10:08 GMT"}, headerContentEncoding: []string{contentEncoding}}, expected: false},
	}

	for _, test := range tests {
		actual := c.IsSupported(test.in)

		if actual != test.expected {
			t.Errorf(
				"IsSupported: given: %+v expected: %v actual: %v",
				test.in,
				test.expected,
				actual,
			)
		}
	}
}

func TestUnmarshalRequestSuccess(t *testing.T) {
	rpc := New()

	topListInput := fakemodel.NewFindTopListInput()
	n := "best_list_ever"
	topListInput.SetName(&n)
	topListInputBytes, err := rpc.Marshal(topListInput)

	if err != nil {
		t.Fatalf("UnmarshalRequest: could not marshal json for topListInput: %v", err)
	}

	input := rpcInput{
		Service:   "com.amazon.toplistsservice#TopListsService",
		Operation: "com.amazon.toplistsservice#FindTopList",
		Input:     topListInputBytes,
	}

	b, err := json.Marshal(&input)

	if err != nil {
		t.Fatalf("UnmarshalRequest: could not marshal json for rpcInput: %v", err)
	}

	req, err := http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(b))
	if err != nil {
		t.Fatalf("UnmarshalRequest: unable to create test http request: %v", err)
	}

	cr, err := rpc.UnmarshalRequest(req)

	if err != nil {
		t.Error("expected err from UnmarshalRequest to be nil but was", err)
	}

	if v, ok := cr.Input.(fakemodel.FindTopListInput); ok {
		unmarshaledName := v.Name()
		if *unmarshaledName != n {
			t.Errorf("expected FindTopListInput field name to be %s but was %s", n, *unmarshaledName)
		}
	} else {
		t.Error("expcted codecRequest Input to be of type FindTopListInput")
	}
}

func TestMarshalResponseSuccess(t *testing.T) {
	output := fakemodel.NewFindTopListOutput()
	name := "worst_list_ever"
	output.SetName(&name)
	cr := &codec.Request{
		Output: output,
	}

	rpc := New()
	w := httptest.NewRecorder()
	rpc.MarshalResponse(w, cr)

	if w.Code != http.StatusOK {
		t.Error("expected status code from MarshalResponse to be 200 but was", w.Code)
		t.Log(w.Body.String())
	}

	respOutput := fakemodel.NewFindTopListOutput()
	body, err := ioutil.ReadAll(w.Body)

	if err != nil {
		t.Fatal("expected ReadAll error to be nil but was", err)
	}

	err = rpc.UnmarshalOutput(body, &respOutput)

	if err != nil {
		t.Fatal("expected UnmarshalOutput error to be nil but was", err)
	}

	respName := respOutput.Name()
	if *respName != name {
		t.Errorf("expected response output field Name to be %s but was %v", name, respOutput.Name())
		t.Logf("respOutput: %+v", respOutput)
	}
}

func TestGetOperationName(t *testing.T) {
	tests := []struct {
		in       string
		expected string
	}{
		{in: "com.amazon.coral.demo#WeatherReport", expected: "WeatherReport"},
		{in: "com.amazon.coral.demo.WeatherReport", expected: ""},
		{in: "com.amazon.coral.demo#WeatherService#WeatherReport", expected: "WeatherService#WeatherReport"},
	}

	for _, test := range tests {
		actual := getOperationName(test.in)
		if actual != test.expected {
			t.Errorf("getOperationName: given %s expected %s actual %s", test.in, test.expected, actual)
		}
	}
}

func TestGetServiceName(t *testing.T) {
	tests := []struct {
		in       string
		expected string
	}{
		{in: "com.amazon.coral.demo#WeatherService", expected: "WeatherService"},
		{in: "com.amazon.coral.demo.WeatherService", expected: ""},
		{in: "com.amazon.coral.demo#WeatherService#WeatherService", expected: "WeatherService#WeatherService"},
	}

	for _, test := range tests {
		actual := getServiceName(test.in)
		if actual != test.expected {
			t.Errorf("getServiceName: given %s expected %s actual %s", test.in, test.expected, actual)
		}
	}
}

func TestGetAssembyName(t *testing.T) {
	tests := []struct {
		in       string
		expected string
	}{
		{in: "com.amazon.coral.demo#WeatherReport", expected: "com.amazon.coral.demo"},
		{in: "com.amazon.coral.demo.WeatherReport", expected: ""},
		{in: "com.amazon.coral.demo#WeatherService#WeatherReport", expected: "com.amazon.coral.demo"},
	}

	for _, test := range tests {
		actual := getAssemblyName(test.in)
		if actual != test.expected {
			t.Errorf("getAssemblyName: given %s expected %s actual %s", test.in, test.expected, actual)
		}
	}
}
