// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralGoModel/src/coral/model"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

const (
	headerAccept          = "Accept"
	headerAmznDate        = "X-Amz-Date"
	headerAmznToken       = "X-Amz-Security-Token"
	headerAmznTarget      = "X-Amz-Target"
	headerContentType     = "Content-Type"
	headerContentEncoding = "Content-Encoding"

	contentType     = "application/json"
	contentEncoding = "amz-"
)

// IsSupported satisfies codec.Server
//
// In order to truly check if a request is supported by this codec we need access to the
// request body. This is just a shallow check to satisfy the codec.Server interface.
// UnmarshalRequest will do a more a deeper check and return an appropriate error.
func (c RPCv0) IsSupported(g codec.Getter) bool {
	return isContentTypeSupported(g) && isAmzDateSupported(g) && isContentEncodingSupported(g)
}

func isContentTypeSupported(g codec.Getter) bool {
	ct := g.Get(headerContentType)

	if ct == "" {
		return false
	}

	// Content-Type can look like application/json; charset=UTF-8
	ct = strings.Split(ct, ";")[0]

	return ct == contentType
}

func isContentEncodingSupported(g codec.Getter) bool {
	// RPVc1 requires the Content-Encoding header to be amz-1.0 whereas this codec
	// has no strict preference.
	return !strings.HasPrefix(g.Get(headerContentEncoding), contentEncoding)
}

func isAmzDateSupported(g codec.Getter) bool {
	return g.Get(headerAmznDate) != ""
}

// UnmarshalRequest reads an http.Request to produce a codec.Request struct with
// the Service, Operation, and Input decoded.
func (c RPCv0) UnmarshalRequest(r *http.Request) (*codec.Request, error) {
	// TODO handle ensuring that the signature from the request is valid
	if c.SignerV4 != nil {
	}

	// TODO handle AAA signed requests
	if c.AAAClient != nil {
	}

	// This code between this comment and the next is (mostly) unique
	// to RPCv0
	var rawInput rpcInput
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&rawInput)

	if err != nil {
		return nil, errors.Wrap(err, "There was an issue decoding the JSON in the request body")
	}

	if rawInput.Operation == "" || rawInput.Service == "" {
		return nil, errors.New(
			"Invalid request. Expected Operation and Service JSON attributes in the request body",
		)
	}

	opName := getOperationName(rawInput.Operation)
	svcName := getServiceName(rawInput.Service)
	asmName := getAssemblyName(rawInput.Operation)

	if opName == "" || svcName == "" || asmName == "" {
		return nil, errors.New("Request is not supported by the RPCv0 codec.")
	}

	// code below here is (mostly) a straight copy from RPCv1
	asm := model.LookupAssembly(asmName)
	op, err := asm.Op(opName)
	if err != nil {
		return nil, errors.Wrapf(err, "Unable to retrieve operation %v", opName)
	}

	cr := &codec.Request{
		Service: codec.ShapeRef{
			AsmName:   asmName,
			ShapeName: svcName,
		},
		Operation: codec.ShapeRef{
			AsmName:   asmName,
			ShapeName: opName,
		},
	}

	if input := op.Input(); input != nil {
		cr.Input = input.New()
		if err := c.Unmarshal(rawInput.Input, &cr.Input); err != nil {
			return nil, err
		}
	}

	if output := op.Output(); output != nil {
		cr.Output = output.New()
	}

	return cr, nil
}

// MarshalResponse writes the Output of the given Request to the given ResponseWriter
// adding headers and encryption as necessary to support the CODEC and security.
// If the given Output is nil, then a status 204 response is returned.
func (c RPCv0) MarshalResponse(w http.ResponseWriter, r *codec.Request) {
	if r == nil {
		http.Error(w, "Unable to process request", http.StatusInternalServerError)
		return
	}

	var body []byte
	var err error

	if r.Output != nil {
		body, err = c.MarshalOutput(r.Output)
		if err != nil {
			http.Error(
				w,
				"Unable to process response",
				http.StatusInternalServerError,
			)
			return
		}
	}

	w.Header().Set(headerContentType, contentEncoding)

	if body != nil {
		_, err = w.Write(body)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

func getOperationName(str string) string {
	return splitAndTakeIndex(str, "#", 1)
}

func getServiceName(str string) string {
	return splitAndTakeIndex(str, "#", 1)
}

func getAssemblyName(str string) string {
	return splitAndTakeIndex(str, "#", 0)
}

// splitAndTakeIndex is a not quite generalised function for splitting strings
// It exists as a helper function to make splitting strings like "com.amazon.coral.demo#WeatherService"
// into assembly and service / operation easier
func splitAndTakeIndex(str string, split string, i int) string {
	arr := strings.SplitN(str, split, 2)

	if len(arr) < 2 {
		return ""
	}

	return arr[i]
}
