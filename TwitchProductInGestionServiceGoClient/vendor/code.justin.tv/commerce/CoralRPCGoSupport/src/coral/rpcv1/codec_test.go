/* Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package rpcv1_test

import (
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralGoRPCSupport/src/coral/internal/test/fake"
	"code.justin.tv/commerce/CoralGoRPCSupport/src/cora/internal/test/fakemodel"
	"code.justin.tv/commerce/CoralGoRPCSupport/src/coral/rpcv1"
	"bufio"
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const (
	validHttpResponse = `HTTP/1.1 200 OK
x-amzn-RequestId: 954ec7e5-f4df-11e4-a33e-535b6bee04a1
Content-Type: application/json
Content-Length: 50
Date: Thu, 07 May 2015 17:36:24 GMT

{"__type": "rpcv1_test#FakeOutput","output":"1.0"}`

	emptyHttpResponse = `HTTP/1.1 200 OK
x-amzn-RequestId: 954ec7e5-f4df-11e4-a33e-535b6bee04a1
Content-Type: application/json
Content-Length: 0
Date: Thu, 07 May 2015 17:36:24 GMT`

	errorHttpResponse = `HTTP/1.1 500 Internal Server Error
x-amzn-RequestId: 954ec7e5-f4df-11e4-a33e-535b6bee04a1
Content-Type: application/json
Content-Length: 50
Date: Thu, 07 May 2015 17:36:24 GMT

{"__type": "rpcv1_test#FakeOutput","output":"1.0"}`

	validHttpRequest = `POST https://example.com:8000 HTTP/1.1
Content-Type: application/json; charset=UTF-8
Content-Encoding: amz-1.0
Content-Length: 168
X-Amz-Date: Thu, 01 Mar 2012 22:07:13 GMT
X-Amz-Target: rpcv1_test.TestService.IsAuthorized

{
   "__type": "rpcv1_test#FakeInput",
   "user": "example",
   "rules": [ {
      "__type": "rpcv1_test#FakeRule",
      "source": "LDAP", "identifier": "foo"}
   ]
}
`
	emptyHttpRequest = `POST https://example.com:8000 HTTP/1.1
Content-Type: application/json; charset=UTF-8
Content-Encoding: amz-1.0
Content-Length: 0
X-Amz-Date: Thu, 01 Mar 2012 22:07:13 GMT
X-Amz-Target: rpcv1.test.TestService.IsAuthorized

`

	errorHttpRequest = `POST https://example.com:8000 HTTP/1.1
Content-Type: application/json; charset=UTF-8
Content-Length: 168
Content-Encoding: amz-1.0
X-Amz-Date: Thu, 01 Mar 2012 22:07:13 GMT

{
   "__type": "rpcv1.test#FakeInput",
   "user": "example",
   "rules": [ {
         "__type": "rpcv1_test#FakeRule",
         "source": "LDAP", "identifier": "foo"}
   ]
}
`
)

func TestIsSupported(t *testing.T) {
	ct1 := "application/json; charset=UTF-8;"
	ct2 := "application/json"
	ct3 := "application/xml"
	ce1 := "amz-1.0"
	ce2 := "amz_1.0"
	dt := "2016"
	tar := "asm.service.op"
	rpcv1 := rpcv1.RPCv1{}
	tests := []struct {
		name      string
		headers   http.Header
		supported bool
	}{
		{"matching request", headers(ct1, ce1, dt, tar), true},
		{"no charset", headers(ct2, ce1, dt, tar), true},
		{"nil", nil, false},
		{"empty", http.Header{}, false},
		{"missing content type", headers("", ce1, dt, tar), false},
		{"missing content encoding", headers(ct1, "", dt, tar), false},
		{"missing date", headers(ct1, ce1, "", tar), false},
		{"missing target", headers(ct1, ce1, dt, ""), false},
		{"bad content type", headers(ct3, ce1, dt, tar), false},
		{"bad content encoding", headers(ct1, ce2, dt, tar), false},
	}

	for _, test := range tests {
		if rpcv1.IsSupported(test.headers) != test.supported {
			t.Error(test.name, "- Expected", test.supported, "for headers", test.headers)
		}
	}
}

func TestRoundTrip(t *testing.T) {
	testError := fmt.Errorf("Failed")
	aaaClient := &fake.AAA{}
	fakeOut := fakemodel.NewFakeOutput()
	input := fakemodel.NewFakeInput()
	input.SetUser("baz")
	req := &codec.Request{Service: fakemodel.TestService, Operation: fakemodel.TestOperation, Input: input, Output: fakeOut}
	rpc := rpcv1.New("foo.com:8080", rpcv1.SetAAAClient(aaaClient))

	// Error cases
	// AAA encode
	aaaClient.EncodeErr = testError
	if rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString(validHttpResponse)}) == nil {
		t.Error("Expected err for AAA encode failure")
	}
	// AAA decode
	aaaClient.EncodeErr, aaaClient.DecodeErr = nil, testError
	if rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString(validHttpResponse)}) == nil {
		t.Error("Expected err for AAA decode failure")
	}
	// Empty response
	aaaClient.DecodeErr = nil
	if rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString("")}) == nil {
		t.Error("Expected err for empty response")
	}
	// Empty body
	if rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString(emptyHttpResponse)}) == nil {
		t.Error("Expected err for empty response")
	}
	// Non-200 response
	if rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString(errorHttpResponse)}) == nil {
		t.Error("Expected err for empty response")
	}

	// Success case.  Validate all the things.
	output := fakemodel.NewFakeOutput()
	req.Output = &output
	if err := rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString(validHttpResponse)}); err != nil {
		t.Error("Expected nil error but found", err)
	}
	if output.Output() != "1.0" {
		t.Error("Expected does not match actual output", output.Output())
	}

	// Not all Clients specify an output for each input
	req.Output = nil
	if err := rpc.RoundTrip(req, &fake.Pipe{ReadBuf: bytes.NewBufferString(validHttpResponse)}); err != nil {
		t.Error("Expected nil error but found", err)
	}
}

func TestGetServiceAnOp(t *testing.T) {
	tests := []struct {
		name    string
		val     string
		asm     string
		service string
		op      string
	}{
		{"emtpy string", "", "", "", ""},
		{"no period", "serviceop", "", "", ""},
		{"period at end", "service.op.", "", "", ""},
		{"simple case", "asm.service.op", "asm", "service", "op"},
		{"multiple periods", "s.e.r.v.i.c.e.op", "s.e.r.v.i.c", "e", "op"},
	}

	for _, test := range tests {
		if asm, service, op := rpcv1.GetServiceAndOp(test.val); asm != test.asm || service != test.service || op != test.op {
			t.Error(test.name, "- Expected (", test.asm, ",", test.service, ",", test.op,
				") found (", asm, ",", service, ",", op, ")")
		}
	}
}

func TestUnmarshalRequest(t *testing.T) {
	rpc := rpcv1.RPCv1{}

	// Error cases
	errorCases := []string{emptyHttpRequest, errorHttpRequest}
	for _, errorCase := range errorCases {
		if req := buildRequest(errorCase, t); req != nil {
			if creq, err := rpc.UnmarshalRequest(req); err == nil {
				t.Error("Expected error for request", errorCase, "\nbut found", creq)
			}
		}
	}

	rule := fakemodel.NewFakeRule()
	rule.SetIdentifier("foo")
	rule.SetSource("LDAP")
	rules := []fakemodel.FakeRule{
		rule,
	}
	expectedInput := fakemodel.NewFakeInput()
	expectedInput.SetUser("example")
	expectedInput.SetRules(rules)
	req := buildRequest(validHttpRequest, t)
	if creq, err := rpc.UnmarshalRequest(req); err != nil {
		t.Error("Unxpected error", err)
	} else {
		if creq.Service != fakemodel.TestService || creq.Operation != fakemodel.TestOperation {
			t.Error("Expected", fakemodel.TestService, fakemodel.TestOperation, "but found", creq.Service)
		}
		if creq.Input == nil || creq.Output == nil {
			t.Fatal("Expected both input and output to be present but found", creq.Input, creq.Output)
		}
		if in, ok := creq.Input.(fakemodel.FakeInput); !ok {
			t.Errorf("Expected input to be of type FakeInput, but found %T", creq.Input)
		} else if !reflect.DeepEqual(in, expectedInput) {
			t.Error("Expected input\n", expectedInput, "\nbut found\n", in)
		}
	}
}

func TestMarshalResponse(t *testing.T) {
	rpc := rpcv1.RPCv1{}

	out := fakemodel.NewFakeOutput()
	out.SetOutput("I out all the puts")
	// "Unable to process request" = 26 characters.
	tests := []struct {
		name       string
		req        *codec.Request
		statusCode int
		bodyLen    int
	}{
		{"nil request", nil, http.StatusInternalServerError, 26},
		{"empty operation", &codec.Request{Service: fakemodel.TestService}, http.StatusInternalServerError, 26},
		{"empty service", &codec.Request{Operation: fakemodel.TestOperation}, http.StatusInternalServerError, 26},
		{"empty output", &codec.Request{Service: fakemodel.TestService, Operation: fakemodel.TestOperation}, http.StatusNoContent, 0},
		{"with output", &codec.Request{
			Service: fakemodel.TestService, Operation: fakemodel.TestOperation, Output: out}, http.StatusOK, 64,
		},
	}

	for _, test := range tests {
		w := httptest.NewRecorder()
		rpc.MarshalResponse(w, test.req)
		if w.Code != test.statusCode {
			t.Error(test.name, "- Expected status code", test.statusCode, "but found", w.Code)
		}
		if w.Body.Len() != test.bodyLen {
			t.Error(test.name, "- Expected body of length", test.bodyLen, "but found", w.Body.Len(), ":\n", w.Body.String())
		}
	}
}

// Utilities

func headers(ct, enc, dt, tar string) http.Header {
	h := http.Header{}
	h.Set("Content-Type", ct)
	h.Set("Content-Encoding", enc)
	h.Set("X-Amz-Date", dt)
	h.Set("X-Amz-Target", tar)
	return h
}

// buildRequest uses http.ReadRequest to build an http.Request.  If ReadRequest reports an
// error then t.Error is called and nil is returned.
func buildRequest(reqBody string, t *testing.T) *http.Request {
	req, err := http.ReadRequest(bufio.NewReader(strings.NewReader(reqBody)))
	if err == nil {
		return req
	}
	t.Fatal("Unable to parse", reqBody, "\n", err)
	return nil
}
