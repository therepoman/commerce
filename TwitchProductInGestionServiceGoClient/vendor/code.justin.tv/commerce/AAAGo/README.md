# AAAGo
AAAGo provides support for AAA encoding requests and decoding responses with either AAASecurityDaemon or AAARoadsideAssist.

WARNING!! Due to Go not supported RHEL5, there have compatibility issues of Go's Unix Socket handling that renders AAASecurityDaemon unusable on RHEL5.
Either switch to Amazon Linux (recommended) or use AAARoadsideAssist.

## Supported Features
1. AAARoadsideAssist and AAASecurityDaemon implementations.
1. Encode outgoing requests.
2. Decoding incoming responses.

## Usage
To use AAAGo, a build-time dependency on AAAGo and a runtime dependency on AAARoadsideAssist must be declared in the Config.
Please refer to the examples in the code base for additional reference.

### AAA Client Instantiation
#### AAASecurityDaemon
aaaClient, err := sd.NewSDClient()

If doing something non-standard and you want to specify the SecurityDaemon's Socket path manually, you can pass the sd.StaticSocketPath("")

Note: The AAASecurityDaemon client will only take  the first HTTP header value.

#### AAARoadsideAssist
aaaClient, err := aaa.NewRAClient(&http.Client{}, aaa.RAPortAutodiscovery())


### REST Client
1. Create a new RoadsideAssist client. The port is usually 1050, however it is recorded in APOLLO_ACTUAL_ENVIRONMENT_ROOT/var/state/aaa/RoadsideAssist. Automatic parsing of this runtime info can be done using the RAPortAutodiscovery option:
```
aaaClient, err := sd.NewSDClient()
```
2. Create a http request then encode with AAA
```
req, err := http.NewRequest("GET", "http://somethingbehindavip.amazon.com/coolStuff", body)
if err != nil {
    panic(err)
}
clientCxt, err := aaaClient.EncodeRequest("SERVICE", "OPERATION", req)
if err != nil {
    panic(err)
}
resp, err := httpClient.Do(req)
```
3. Decode the http response. After decoding the response is available for use as a normal response.
```
resp, err := httpClient.Do(req)
if err != nil {
    panic(err)
}
err = aaaClient.DecodeResponse(clientCxt, resp)
if err != nil {
    panic(err)
}
```

### Coral
Coral support is pending integration of AAAGo with CoralRPCGoSupport. This documentation will be updated once integrated.

## TODOs
1. Add support for decoding requests and encoding responses. This will allow Go services to support AAA.
4. AAARoadsideAssist client performance improvments. Currently a new HTTP connection is created for every encode/decode request. This is not performant as AAARoadsideAssist approx. 10msec to accept a connection.
