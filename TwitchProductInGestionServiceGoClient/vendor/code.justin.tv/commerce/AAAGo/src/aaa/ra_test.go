package aaa

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
)

const (
	defaultServiceAddr          = "http://2.3.4.5/beer"
	defaultService              = "BeerService"
	defaultOperation            = "ListBeer"
	defaultAuthHeader           = "AUTHHEADER"
	defaultContextKeyId         = "1234"
	defaultContextKeyVersion    = "5678"
	defaultContextKeyVersionInt = 5678
	defaultDateHeader           = "Date"
	defaultStatusCode           = "200"
	defaultStatusCodeInt        = 200
	defaultCustomHeader         = "CustomHeader1"
	defaultCustomHeaderVal      = "CustomHeader1Val"
	defaultAAAErrorCode         = 6
	defaultAAAErrorMessage      = "Bad Signature"
)

const (
	expectedRAAutodiscoverUrl = "http://localhost:1337"
)

var defaultDecodeResponseAuthHeader = fmt.Sprintf("AAA SignedHeaders=%v;%v", AaaAuthHeader, AaaDateHeader)
var securityDaemonDecodeResponseAuthHeader = fmt.Sprintf("SignedHeaders=%v;%v", AaaAuthHeader, AaaDateHeader)

func setup(t *testing.T, handler http.Handler) (client Client) {
	server := httptest.NewServer(handler)
	url, err := url.Parse(server.URL)
	if err != nil {
		t.Fatalf("Failed to parse httptest.Server URL: %v", err)
	}
	proxy := http.ProxyURL(url)
	transport := http.DefaultTransport.(*http.Transport)
	transport.Proxy = proxy
	httpClient := &http.Client{Transport: transport}
	client, err = NewRAClient(httpClient, RAPort(0))
	if err != nil {
		t.Fatalf("Failed to create RA clientL %v.", err)
	}
	return client
}

func buildClientContext() (cxt *ClientContext) {
	return &ClientContext{
		KeyId:      defaultContextKeyId,
		KeyVersion: defaultContextKeyVersionInt,
		Service:    defaultService,
		Operation:  defaultOperation,
	}
}

func buildDefaultDecodeResponseHeaders() http.Header {
	h := make(http.Header, 0)
	h.Add(AaaAuthHeader, defaultDecodeResponseAuthHeader)
	h.Add(AaaDateHeader, defaultDateHeader)
	return h
}

func validateBasicHeaders(t *testing.T, r *http.Request, action aaaAction, mType aaaMessageType) {
	if r.Method != "POST" {
		t.Fatalf("Incorrect HTTP method. AAA RoadsideAssist requests must be POSTs. Recieved %v", r.Method)
	}
	if r.Proto != "HTTP/1.1" {
		t.Fatalf("Incorrect HTTP protocol. AAA RoadsideAssist requests must use HTTP/1.1. Recieved %v", r.Proto)
	}
	h := r.Header
	if h.Get(protoVersionHeader) != protoVersionHeaderVal {
		t.Fatalf("Incorrect AAA protocol version. Expected %v, recieved %v.", protoVersionHeaderVal, h.Get(protoVersionHeader))
	}
	if h.Get(actionHeader) != string(action) {
		t.Fatalf("Incorrect AAA Action. Expected %v, recieved %v.", string(action), h.Get(actionHeader))
	}
	if h.Get(messageTypeHeader) != string(mType) {
		t.Fatalf("Incorrect AAA MessageType. Expected %v, recieved %v.", string(mType), h.Get(messageTypeHeader))
	}
}

func validateEncodeRequestHeaders(t *testing.T, r *http.Request, uri, verb, service, operation string) {
	h := r.Header
	if h.Get(requestUriHeader) != uri {
		t.Fatalf("Incorrect AAA Request URI. Expected %v, recieved %v.", uri, h.Get(requestUriHeader))
	}
	if h.Get(requestVerbHeader) != verb {
		t.Fatalf("Incorrect AAA Request Verb. Expected %v, recieved %v.", verb, h.Get(requestVerbHeader))
	}
	if h.Get(contextServiceHeader) != service {
		t.Fatalf("Incorrect AAA Context Service. Expected %v, recieved %v.", service, h.Get(contextServiceHeader))
	}
	if h.Get(contextOperationHeader) != operation {
		t.Fatalf("Incorrect AAA Context Operation. Expected %v, recieved %v.", operation, h.Get(contextOperationHeader))
	}
}

func validateDecodeResponseHeaders(t *testing.T, r *http.Request, authHeaderVal string, customHeaders map[string]string) {
	h := r.Header
	if h.Get(responseCodeHeader) != defaultStatusCode {
		t.Fatalf("Incorrect AAA Message HTTP Response status code. Expected %v, recieved %v.", defaultStatusCode, h.Get(responseCodeHeader))
	}
	if h.Get(contextServiceHeader) != defaultService {
		t.Fatalf("Incorrect AAA Context Service. Expected %v, recieved %v.", defaultService, h.Get(contextServiceHeader))
	}
	if h.Get(contextOperationHeader) != defaultOperation {
		t.Fatalf("Incorrect AAA Context Operation. Expected %v, recieved %v.", defaultOperation, h.Get(contextOperationHeader))
	}
	if h.Get(contextKeyIdHeader) != defaultContextKeyId {
		t.Fatalf("Incorrect AAA Context Key Id. Expected %v, recieved %v.", defaultContextKeyId, h.Get(contextKeyIdHeader))
	}
	if h.Get(contextKeyVersionHeader) != defaultContextKeyVersion {
		t.Fatalf("Incorrect AAA Context Key Version. Expected %v, recieved %v.", defaultContextKeyVersion, h.Get(contextKeyVersionHeader))
	}
	if h.Get(AaaAuthHeader) != authHeaderVal {
		t.Fatalf("Incorrect AAA Auth Header. Expected %v, recieved %v.", authHeaderVal, h.Get(authHeader))
	}
	for k, v := range customHeaders {
		if h.Get(k) != v {
			t.Fatalf("Incorrect AAA Custom Header for key %v. Expected %v, recieved %v.", k, v, h.Get(k))
		}
	}
}

func writeBasicHeaders(w http.ResponseWriter, body []byte) {
	h := w.Header()
	h.Set(protoVersionHeader, protoVersionHeaderVal)
}

func writeEncodeRequestResponse(t *testing.T, w http.ResponseWriter, body []byte) {
	writeBasicHeaders(w, body)
	h := w.Header()
	h.Set(authHeader, defaultAuthHeader)
	h.Set(contextKeyIdHeader, defaultContextKeyId)
	h.Set(contextKeyVersionHeader, defaultContextKeyVersion)
	h.Set(dateHeader, defaultDateHeader)
	_, err := w.Write(body)
	if err != nil {
		t.Fatalf("Error when writing EncodeRequest response: %v.", err)
	}
}

func writeAAAError(w http.ResponseWriter) {
	h := w.Header()
	h.Add(errorCodeHeader, string(defaultAAAErrorCode))
	h.Add(errorMessageHeader, defaultAAAErrorMessage)
}

func validateClientContext(t *testing.T, cxt *ClientContext) {
	if cxt.KeyId != defaultContextKeyId {
		t.Fatalf("Incorrect Client Context Key Id. Expected %v, recieved %v.", defaultContextKeyId, cxt.KeyId)
	}
	keyVer := strconv.FormatUint(cxt.KeyVersion, 10)
	if keyVer != defaultContextKeyVersion {
		t.Fatalf("Incorrect Client Context Key Version. Expected %v, recieved %v.", defaultContextKeyVersion, keyVer)
	}
	if cxt.Service != defaultService {
		t.Fatalf("Incorrect Client Context Service. Expected %v, recieved %v.", defaultService, cxt.Service)
	}
	if cxt.Operation != defaultOperation {
		t.Fatalf("Incorrect Client Context Operation. Expected %v, recieved %v.", defaultOperation, cxt.Operation)
	}
}

func validateEncodedRequest(t *testing.T, r *http.Request, body []byte) {
	h := r.Header
	if h.Get(AaaAuthHeader) != defaultAuthHeader {
		t.Fatalf("Incorrect encoded request Auth Header. Expected %v, recieved %v.", defaultAuthHeader, h.Get(AaaAuthHeader))
	}
	if h.Get(AaaDateHeader) != defaultDateHeader {
		t.Fatalf("Incorrect encoded request Date Header. Expected %v, recieved %v.", defaultDateHeader, h.Get(AaaDateHeader))
	}
	rBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.Fatalf("Failed to read encoded request body: %v.", err)
	}
	if bytes.Compare(rBody, body) != 0 {
		t.Fatalf("Incorrect encoded request Body. Expected %v, recieved %v.", string(body), string(rBody))
	}
}

func validateDecodedResponse(t *testing.T, r *http.Response, body []byte) {
	rBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.Fatalf("Failed to read decoded response body: %v.", err)
	}
	if bytes.Compare(rBody, body) != 0 {
		t.Fatalf("Incorrect decoded response Body. Expected %v, recieved %v.", string(body), string(rBody))
	}
}

func Test_RAPortAutodiscovery_MockEnvironent(t *testing.T) {
	wd, err := os.Getwd()
	if err != nil {
		t.Fatalf("Error when getting working directory: %v", err)
	}
	httpClient := &http.Client{}
	os.Setenv("ENVROOT", filepath.Join(wd, "../", "testData", "mockEnv"))
	client, err := NewRAClient(httpClient, RAPortAutodiscovery())
	if err != nil {
		t.Fatalf("Unexpected error when creating RAClient: %v", err)
	}
	if client.url != expectedRAAutodiscoverUrl {
		t.Fatalf("Incorrect RA Url. Expected: %v, Acutal: %v", expectedRAAutodiscoverUrl, client.url)
	}
}

func Test_RAPortAutodiscovery_BadEnvironment(t *testing.T) {
	httpClient := &http.Client{}
	os.Setenv("ENVROOT", "/")
	_, err := NewRAClient(httpClient, RAPortAutodiscovery())
	if err == nil {
		t.Fatalf("Expected error, however none was returned.")
	}
}

func Test_EncodeRequest_NilBodyGet(t *testing.T) {
	req, err := http.NewRequest("GET", defaultServiceAddr, nil)
	if err != nil {
		t.Fatalf("Unable to create http request: %v", err)
	}
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, encodeRequest, httpRequest)
		validateEncodeRequestHeaders(t, r, defaultServiceAddr, "GET", defaultService, defaultOperation)
		writeEncodeRequestResponse(t, w, []byte{})
	})
	client := setup(t, raHandler)
	cxt, err := client.EncodeRequest(defaultService, defaultOperation, req)
	if err != nil {
		t.Fatalf("Error in EncodeRequest: %v", err)
	}
	validateClientContext(t, cxt)
	validateEncodedRequest(t, req, []byte{})
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_EncodeRequest_Post(t *testing.T) {
	body := "This is a POST test."
	eBody := "ENCODED BODY"
	req, err := http.NewRequest("POST", defaultServiceAddr, strings.NewReader(body))
	if err != nil {
		t.Fatalf("Unable to create http request: %v", err)
	}
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, encodeRequest, httpRequest)
		validateEncodeRequestHeaders(t, r, defaultServiceAddr, "POST", defaultService, defaultOperation)
		writeEncodeRequestResponse(t, w, []byte(eBody))
	})
	client := setup(t, raHandler)
	cxt, err := client.EncodeRequest(defaultService, defaultOperation, req)
	if err != nil {
		t.Fatalf("Error in EncodeRequest: %v", err)
	}
	validateClientContext(t, cxt)
	validateEncodedRequest(t, req, []byte(eBody))
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_EncodeRequest_NoRelationship(t *testing.T) {
	body := "This is a POST test."
	eBody := "ENCODED BODY"
	req, err := http.NewRequest("POST", defaultServiceAddr, strings.NewReader(body))
	if err != nil {
		t.Fatalf("Unable to create http request: %v", err)
	}
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, encodeRequest, httpRequest)
		validateEncodeRequestHeaders(t, r, defaultServiceAddr, "POST", defaultService, defaultOperation)
		writeBasicHeaders(w, []byte(eBody))
	})
	client := setup(t, raHandler)
	_, err = client.EncodeRequest(defaultService, defaultOperation, req)
	if err == nil {
		t.Fatal("No error in EncodeRequest.")
	}
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_EncodeResponse_AAAError(t *testing.T) {
	req, err := http.NewRequest("GET", defaultServiceAddr, nil)
	if err != nil {
		t.Fatalf("Unable to create http request: %v", err)
	}
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, encodeRequest, httpRequest)
		validateEncodeRequestHeaders(t, r, defaultServiceAddr, "GET", defaultService, defaultOperation)
		writeAAAError(w)
		w.Write([]byte{})
	})
	client := setup(t, raHandler)
	_, err = client.EncodeRequest(defaultService, defaultOperation, req)
	if err == nil {
		t.Fatal("No error in EncodeRequest.")
	}
	if !strings.Contains(err.Error(), defaultAAAErrorMessage) {
		t.Fatalf("Incorrect error message. Expected %v, recieved %v.", defaultAAAErrorMessage, err)
	}
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_DecodeResponse_EmptyBody(t *testing.T) {
	resp := &http.Response{
		StatusCode: defaultStatusCodeInt,
		Body:       ioutil.NopCloser(&bytes.Buffer{}),
		Header:     buildDefaultDecodeResponseHeaders(),
	}
	cxt := buildClientContext()
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, decodeResponse, httpResponse)
		validateDecodeResponseHeaders(t, r, defaultDecodeResponseAuthHeader, nil)
		w.Write([]byte{})
	})
	client := setup(t, raHandler)
	err := client.DecodeResponse(cxt, resp)
	if err != nil {
		t.Fatalf("Error in DecodeResponse: %v", err)
	}
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_DecodeResponse_NonEmptyBody(t *testing.T) {
	eBody := "ENCODED BODY"
	body := "This body has been decoded."
	resp := &http.Response{
		StatusCode: defaultStatusCodeInt,
		Body:       ioutil.NopCloser(strings.NewReader(eBody)),
		Header:     buildDefaultDecodeResponseHeaders(),
	}
	cxt := buildClientContext()
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, decodeResponse, httpResponse)
		validateDecodeResponseHeaders(t, r, defaultDecodeResponseAuthHeader, nil)
		w.Write([]byte(body))
	})
	client := setup(t, raHandler)
	err := client.DecodeResponse(cxt, resp)
	if err != nil {
		t.Fatalf("Error in DecodeResponse: %v", err)
	}
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_DecodeResponse_CustomHeader(t *testing.T) {
	authHeaderVal := fmt.Sprintf("%v;%v", defaultDecodeResponseAuthHeader, defaultCustomHeader)
	decodeResponse_CustomHeader_Tester(t, authHeaderVal)
}

func Test_DecodeResponse_CustomSecurityDaemonHeader(t *testing.T) {
	authHeaderVal := fmt.Sprintf(" %v;%v", securityDaemonDecodeResponseAuthHeader, defaultCustomHeader)
	decodeResponse_CustomHeader_Tester(t, authHeaderVal)
}

func decodeResponse_CustomHeader_Tester(t *testing.T, authHeaderVal string) {
	h := buildDefaultDecodeResponseHeaders()
	h.Set(AaaAuthHeader, authHeaderVal)
	h.Add(defaultCustomHeader, defaultCustomHeaderVal)
	resp := &http.Response{
		StatusCode: defaultStatusCodeInt,
		Body:       ioutil.NopCloser(&bytes.Buffer{}),
		Header:     h,
	}
	cxt := buildClientContext()
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, decodeResponse, httpResponse)
		validateDecodeResponseHeaders(t, r, strings.TrimSpace(authHeaderVal), map[string]string{defaultCustomHeader: defaultCustomHeaderVal})
		w.Write([]byte{})
	})
	client := setup(t, raHandler)
	err := client.DecodeResponse(cxt, resp)
	if err != nil {
		t.Fatalf("Error in DecodeResponse: %v", err)
	}
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}

func Test_DecodeResponse_AAAError(t *testing.T) {
	resp := &http.Response{
		StatusCode: defaultStatusCodeInt,
		Body:       ioutil.NopCloser(&bytes.Buffer{}),
		Header:     buildDefaultDecodeResponseHeaders(),
	}
	cxt := buildClientContext()
	handlerHit := false
	raHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerHit = true
		validateBasicHeaders(t, r, decodeResponse, httpResponse)
		validateDecodeResponseHeaders(t, r, defaultDecodeResponseAuthHeader, nil)
		writeAAAError(w)
		w.Write([]byte{})
	})
	client := setup(t, raHandler)
	err := client.DecodeResponse(cxt, resp)
	if err == nil {
		t.Fatal("No error in EncodeRequest.")
	}
	if !strings.Contains(err.Error(), defaultAAAErrorMessage) {
		t.Fatalf("Incorrect error message. Expected %v, recieved %v.", defaultAAAErrorMessage, err)
	}
	if !handlerHit {
		t.Fatal("RoadsideAssist handler never hit.")
	}
}
