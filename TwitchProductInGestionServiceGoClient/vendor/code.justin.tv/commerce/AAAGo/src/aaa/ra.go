package aaa

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/GoApolloEnvironmentInfo/src/apollo"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

// Header constants
const (
	protoVersionHeader      = "X-Amzn-AAA-PROTOCOL-VERSION"
	protoVersionHeaderVal   = "2012-04-25"
	actionHeader            = "X-Amzn-AAA-ACTION"
	messageTypeHeader       = "X-Amzn-AAA-MESSAGE-TYPE"
	customHeaderListHeader  = "X-Amzn-AAA-HEADER-LIST"
	requestUriHeader        = "X-Amzn-AAA-MESSAGE-HTTP-REQUEST-URI"
	requestVerbHeader       = "X-Amzn-AAA-MESSAGE-HTTP-REQUEST-VERB"
	contextServiceHeader    = "X-Amzn-AAA-CONTEXT-SERVICE"
	contextOperationHeader  = "X-Amzn-AAA-CONTEXT-OPERATION"
	contextKeyIdHeader      = "X-Amzn-AAA-CONTEXT-KEY-ID"
	contextKeyVersionHeader = "X-Amzn-AAA-CONTEXT-KEY-VERSION"
	authHeader              = "X-Amzn-AAA-AUTHORIZATION-HEADER"
	dateHeader              = "X-Amzn-AAA-DATE-HEADER"
	responseCodeHeader      = "X-Amzn-AAA-MESSAGE-HTTP-RESPONSE-STATUS-CODE"
	errorCodeHeader         = "X-Amzn-AAA-ERROR-CODE"
	errorMessageHeader      = "X-Amzn-AAA-ERROR-MESSAGE"
)

// AAA actions.
type aaaAction string

const (
	encodeRequest  aaaAction = "encodeRequest"
	decodeRequest  aaaAction = "decodeRequest"
	encodeResponse aaaAction = "encodeResponse"
	decodeResponse aaaAction = "decodeResponse"
	authRequest    aaaAction = "authorizeRequest"
	echo           aaaAction = "echo"
	sanityCheck    aaaAction = "sanityCheck"
)

// AAA message type.
type aaaMessageType string

const (
	httpRequest  aaaMessageType = "HttpRequest"
	httpResponse aaaMessageType = "HttpResponse"
)

// Args used to build AAARoadsideAssist request headers
type aaaHeaderArgs struct {
	uri          string
	verb         string
	action       aaaAction
	service      string
	operation    string
	keyId        string
	keyVersion   uint64
	status       int
	signedHeader map[string]string
}

// AAARoadsideAssist runtime info
const (
	raRuntimeInfoPath = "var/state/aaa/RoadsideAssist"
)

var raRuntimeInfoPortRegex = regexp.MustCompile(`^Port\:(?P<port>\d+)$`)

// AAARoadsideAssist Client
type RA struct {
	client *http.Client
	url    string
}

// Constructs a new AAARoadsideAssist client using the specified http.Client and options.
// Either the RAPort or RAPortAutodiscovery option is required to specify the RoadsideAssist port.
func NewRAClient(client *http.Client, options ...func(*RA) error) (*RA, error) {
	if client == nil {
		client = &http.Client{}
	}
	ra := &RA{
		client: client,
	}
	for _, option := range options {
		err := option(ra)
		if err != nil {
			return nil, fmt.Errorf("Error creating AAA RoadsideAssist client. Error while setting options: %v", err)
		}
	}
	if ra.url == "" {
		return nil, errors.New("Error creating AAA RoadsideAssist. RoadsideAssist's port must be specified.")
	}
	return ra, nil
}

// Constructor option for the AAARoadsideAssist client that sets the RoadsideAssist port to the specified port.
func RAPort(port uint16) func(*RA) error {
	return func(ra *RA) error {
		ra.url = fmt.Sprintf("http://localhost:%d", port)
		return nil
	}
}

// Constructor option for the AAARoadsideAssist client that discovers the RoadsideAssist port based on the current Apollo environment.
func RAPortAutodiscovery() func(*RA) error {
	return func(ra *RA) error {
		// Find Apollo environment.
		env := apollo.CurEnvironment()
		root, err := env.Root()
		if err != nil {
			return err
		}
		path := filepath.Join(root, raRuntimeInfoPath)
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		// The RoadsideAssist runtime info file is a new line seperated KEY:VALUE format.
		infos := strings.Split(string(data), "\n")
		for _, infoStr := range infos {
			match := raRuntimeInfoPortRegex.FindStringSubmatch(infoStr)
			if len(match) > 1 {
				ra.url = fmt.Sprintf("http://localhost:%v", match[1])
				return nil
			}
		}
		return errors.New("Unable to find valid RoadsideAssist port runtime info.")
	}
}

// Performs the AAARoadsideAssist Sanity Check.
func (ra *RA) SanityCheck() error {
	aaaReq, err := http.NewRequest("POST", ra.url, nil)
	if err != nil {
		return err
	}
	addRAHeaders(&aaaReq.Header, &aaaHeaderArgs{})
	resp, err := ra.client.Do(aaaReq)
	if err != nil {
		return err
	}
	headerVal := resp.Header.Get(protoVersionHeader)
	if headerVal != protoVersionHeaderVal {
		return fmt.Errorf("Invalid header %v. Expected %v, however was %v.", protoVersionHeader, protoVersionHeaderVal, headerVal)
	}
	return nil
}

// Encodes the given http.Request with AAA signing and/or encryption using AAARoadsideAssist.
func (ra *RA) EncodeRequest(service, operation string, req *http.Request) (*ClientContext, error) {
	log.Trace("Starting AAARoadsideAssist EncodeRequest operation.")
	aaaReq, err := http.NewRequest("POST", ra.url, req.Body)
	if err != nil {
		return nil, err
	}
	log.Trace("Building AAARoadsideAssist EncodeRequest request headers.")
	addRAHeaders(&aaaReq.Header, &aaaHeaderArgs{
		uri:       req.URL.String(),
		verb:      req.Method,
		action:    encodeRequest,
		service:   service,
		operation: operation,
	})
	log.Tracef("Built AAARoadsideAssist EncodeRequest request headers: %v", aaaReq.Header)
	log.Trace("Sending AAARoadsideAssist EncodeRequest request to AAARoadsideAssist.")
	resp, err := ra.client.Do(aaaReq)
	if err != nil {
		return nil, err
	}
	log.Tracef("Recieved AAARoadsideAssist EncodeRequest response headers: %v", resp.Header)
	err = checkRaError(resp)
	if err != nil {
		return nil, err
	}
	log.Trace("OK AAARoadsideAssist EncodeRequest response received from AAARoadsideAssist. Parsing response.")
	if resp.Header.Get(authHeader) == "" {
		return nil, errors.New("Error encoding request. Not authorized.")
	}
	keyVersionStr := resp.Header.Get(contextKeyVersionHeader)
	keyVersion, err := strconv.ParseUint(keyVersionStr, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("Unable to parse key version: %v", keyVersionStr)
	}
	clientCxt := &ClientContext{
		KeyId:      resp.Header.Get(contextKeyIdHeader),
		KeyVersion: keyVersion,
		Service:    service,
		Operation:  operation,
	}
	log.Trace("Adding Auth and Date header to request from AAASecurityDaemon EncodeRequest response.")
	req.Header.Add(AaaAuthHeader, resp.Header.Get(authHeader))
	req.Header.Add(AaaDateHeader, resp.Header.Get(dateHeader))
	if req.Body != nil {
		req.Body.Close()
	}
	replaceReqBody(req, resp)
	log.Trace("Successfuly completed AAARoadsideAssist EncodeRequest operation")
	return clientCxt, err
}

// Decodes the given http.Response that may or may not be AAA signed and/or encrypted using AAARoadsideAssist.
func (ra *RA) DecodeResponse(clientCxt *ClientContext, resp *http.Response) error {
	log.Trace("Starting AAARoadsideAssist DecodeResponse operation.")
	aaaReq, err := http.NewRequest("POST", ra.url, resp.Body)
	if err != nil {
		return err
	}
	signedHeaders := getSignedResponseHeaders(resp)
	log.Tracef("AAARoadsideAssist DecodeResponse found signed headers: %v", signedHeaders)
	log.Trace("Building AAARoadsideAssist DecodeResponse request headers.")
	addRAHeaders(&aaaReq.Header, &aaaHeaderArgs{
		status:       resp.StatusCode,
		action:       decodeResponse,
		service:      clientCxt.Service,
		operation:    clientCxt.Operation,
		keyId:        clientCxt.KeyId,
		keyVersion:   clientCxt.KeyVersion,
		signedHeader: signedHeaders,
	})
	log.Tracef("Built AAARoadsideAssist DecodeResponse request headers: %v", aaaReq.Header)
	log.Trace("Sending AAARoadsideAssist DecodeResponse request to AAARoadsideAssist.")
	aaaResp, err := ra.client.Do(aaaReq)
	if err != nil {
		return err
	}
	log.Tracef("Recieved AAARoadsideAssist DecodeResponse response headers: %v", resp.Header)
	err = checkRaError(aaaResp)
	if err != nil {
		return err
	}
	log.Trace("OK AAARoadsideAssist DecodeResponse response received from AAARoadsideAssist. Parsing response.")
	if resp.Body != nil {
		resp.Body.Close()
	}
	replaceRespBody(resp, aaaResp)
	log.Trace("Successfuly completed AAARoadsideAssist DecodeResponse operation")
	return nil
}

// Checks AAARoadsideAssist response for interal server error headers.
func checkRaError(aaaResp *http.Response) error {
	errorCode := aaaResp.Header.Get(errorCodeHeader)
	if errorCode != "" {
		errorMessage := aaaResp.Header.Get(errorMessageHeader)
		return fmt.Errorf("Error code %v: %v", errorCode, errorMessage)
	}
	return nil
}

func replaceReqBody(req *http.Request, aaaResp *http.Response) {
	req.Body = aaaResp.Body
	req.ContentLength = aaaResp.ContentLength
}

func replaceRespBody(resp *http.Response, aaaResp *http.Response) {
	resp.Body = aaaResp.Body
	resp.ContentLength = aaaResp.ContentLength
}

// Signed AAA Headers are in the following form: "AAA SignedHeaders=header1;header2".
// The specified signed headers should be in the responses' header as they will be included in the RoadsideAssist request.
func getSignedResponseHeaders(resp *http.Response) map[string]string {
	signedHeaders := map[string]string{
		AaaAuthHeader: resp.Header.Get(AaaAuthHeader),
		AaaDateHeader: resp.Header.Get(AaaDateHeader),
	}
	authHeaderFields := strings.Split(resp.Header.Get(AaaAuthHeader), ",")
	for _, field := range authHeaderFields {
		field = strings.TrimSpace(field)
		if strings.HasPrefix(field, "AAA SignedHeaders=") {
			// Responses encoded by AAARoadsideAssist contain AAA SignedHeaders
			headerStr := strings.TrimPrefix(field, "AAA SignedHeaders=")
			parseSignedHeaders(headerStr, signedHeaders, resp)
			log.Trace("Found SignedHeaders in Response originating from AAARoadsideAssist.")
			break
		} else if strings.HasPrefix(field, "SignedHeaders=") {
			// Responses encoded by AAASecurityDaemon contain SignedHeaders
			headerStr := strings.TrimPrefix(field, "SignedHeaders=")
			parseSignedHeaders(headerStr, signedHeaders, resp)
			log.Trace("Found SignedHeaders in Response originating from AAASecurityDaemon.")
			break
		}
	}
	return signedHeaders
}

func parseSignedHeaders(headerStr string, signedHeaders map[string]string, resp *http.Response) {
	signedHeaderKeys := strings.Split(headerStr, ";")
	for _, h := range signedHeaderKeys {
		signedHeaders[h] = resp.Header.Get(h)
	}
}

// TODO: Support request custom signed headers
// We are not adding Content-Length headers as AAARoadsideAssist ignores them despite the wiki indicating that they're manditory.
func addRAHeaders(header *http.Header, args *aaaHeaderArgs) {
	addCommonHeaders(header, args)
	addContextHeaders(header, args)
	addSignedHeaders(header, args)
	if args.action == encodeRequest || args.action == decodeRequest {
		header.Add(requestUriHeader, args.uri)
		header.Add(requestVerbHeader, args.verb)
	}
	if args.action == decodeResponse {
		header.Add(responseCodeHeader, strconv.Itoa(args.status))
	}
}

func addCommonHeaders(header *http.Header, args *aaaHeaderArgs) {
	header.Add(protoVersionHeader, protoVersionHeaderVal)
	header.Add(actionHeader, string(args.action))
	switch args.action {
	case encodeRequest:
		header.Add(messageTypeHeader, string(httpRequest))
	case decodeResponse:
		header.Add(messageTypeHeader, string(httpResponse))
	}
}

func addContextHeaders(header *http.Header, args *aaaHeaderArgs) {
	switch args.action {
	case authRequest, encodeRequest, encodeResponse, decodeResponse:
		header.Add(contextServiceHeader, args.service)
		header.Add(contextOperationHeader, args.operation)
	}
	switch args.action {
	case decodeResponse:
		header.Add(contextKeyIdHeader, args.keyId)
		header.Add(contextKeyVersionHeader, strconv.FormatUint(args.keyVersion, 10))
	}
}

func addSignedHeaders(header *http.Header, args *aaaHeaderArgs) {
	switch args.action {
	case decodeRequest, encodeResponse, decodeResponse:
		names := make([]string, 0, len(args.signedHeader))
		for name, val := range args.signedHeader {
			names = append(names, name)
			header.Add(name, val)
		}
		if len(names) > 0 {
			header.Add(customHeaderListHeader, strings.Join(names, ","))
		}
	}
}
