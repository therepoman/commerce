/* Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package msgpack_test

import (
	"code.justin.tv/commerce/AAAGo/src/aaa/internal/msgpack"
	"bytes"
	"github.com/pkg/errors"
	"math"
	"reflect"
	"testing"
)

const (
	testString1 = "デジタルカメラの売れ筋ランキング"
	testString2 = "超远摄&超广角，84倍智能变焦、22mm超广角镜头；超高分辨率，2000万像素传感器；独特的艺术滤镜功能，创意控制，修饰，全景；锂离子电池，可拍摄320张照片；3.0寸46万像素LCD"
)

var (
	testBytes1 = []byte("I'm a message that is full of bytes!")
	testBytes2 = make([]byte, math.MaxUint16)
)

func TestCodec(t *testing.T) {
	tests := []*fakeCodec{
		{bt: []byte{}},
		{true, math.MaxUint8, math.MaxUint16, math.MaxUint32, math.MaxUint64, math.MaxInt8, math.MaxInt16, testString1, testBytes1, math.MaxUint16},
		{true, math.MaxUint8 - 1, math.MaxUint16 - 1, math.MaxUint32 - 1, math.MaxUint64 - 1, math.MinInt8, math.MinInt16, testString2, testBytes2, math.MaxUint16 - 1},
	}

	for i, test := range tests {
		b, err := msgpack.Encode(test)
		if err != nil {
			t.Error(i, "- Error encoding data", test, "\nError:", err)
		}
		f := &fakeCodec{}
		buff := bytes.NewBuffer(b)
		if err := msgpack.Decode(buff, f); err != nil {
			t.Error(i, "- Error decoding data", test, "\nError:", err)
		}
		if !reflect.DeepEqual(test, f) {
			t.Error(i, "Expected\n", test, "\nbut found\n", f)
		}
	}
}

func TestDecodeIntErrors(t *testing.T) {
	det := &decodeErrTester{}
	b, err := msgpack.Encode(det)
	if err != nil {
		t.Fatal("Unexpected error encoding:", err)
	}
	buff := bytes.NewBuffer(b)
	if err := msgpack.Decode(buff, det); err != nil {
		t.Error("Unexpected error decoding:", err)
	}
}

type decodeErrTester struct{}

func (f *decodeErrTester) EncodeMsgPack(e *msgpack.Encoder) error {
	for i := 0; i < 11; i++ {
		if err := e.EncodeNil(); err != nil {
			return err
		}
	}
	return nil
}

func (f *decodeErrTester) DecodeMsgPack(d *msgpack.Decoder) error {
	if _, err := d.DecodeBool(); err == nil {
		return errors.New("Expected error from DecodeBool")
	}
	if _, err := d.DecodeUint(); err == nil {
		return errors.New("Expected error from DecodeUint")
	}
	if _, err := d.DecodeUint8(); err == nil {
		return errors.New("Expected error from DecodeUint8")
	}
	if _, err := d.DecodeUint16(); err == nil {
		return errors.New("Expected error from DecodeUint16")
	}
	if _, err := d.DecodeUint32(); err == nil {
		return errors.New("Expected error from DecodeUint32")
	}
	if _, err := d.DecodeUint64(); err == nil {
		return errors.New("Expected error from DecodeUint64")
	}
	if _, err := d.DecodeInt8(); err == nil {
		return errors.New("Expected error from DecodeInt8")
	}
	if _, err := d.DecodeInt16(); err == nil {
		return errors.New("Expected error from DecodeInt16")
	}
	if _, err := d.DecodeString(); err == nil {
		return errors.New("Expected error from DecodeString")
	}
	if _, err := d.DecodeBytes(); err == nil {
		return errors.New("Expected error from DecodeBytes")
	}
	if _, err := d.DecodeArrayLen(); err == nil {
		return errors.New("Expected error from DecodeArrayLen")
	}
	return nil
}

type fakeCodec struct {
	bl   bool
	u8   uint8
	u16  uint16
	u32  uint32
	u64  uint64
	i8   int8
	i16  int16
	st   string
	bt   []byte
	alen uint32
}

func (f *fakeCodec) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeNil(); err != nil {
		return err
	}
	if err := e.EncodeBool(f.bl); err != nil {
		return err
	}
	// Encode the Uints twice so that we can test both methods of decoding them.
	if err := e.EncodeUint8(f.u8); err != nil {
		return err
	}
	if err := e.EncodeUint8(f.u8); err != nil {
		return err
	}
	if err := e.EncodeUint16(f.u16); err != nil {
		return err
	}
	if err := e.EncodeUint16(f.u16); err != nil {
		return err
	}
	if err := e.EncodeUint32(f.u32); err != nil {
		return err
	}
	if err := e.EncodeUint32(f.u32); err != nil {
		return err
	}
	if err := e.EncodeUint64(f.u64); err != nil {
		return err
	}
	if err := e.EncodeUint64(f.u64); err != nil {
		return err
	}
	if err := e.EncodeInt8(f.i8); err != nil {
		return err
	}
	if err := e.EncodeInt16(f.i16); err != nil {
		return err
	}
	if err := e.EncodeString(f.st); err != nil {
		return err
	}
	if err := e.EncodeBytes(f.bt); err != nil {
		return err
	}
	if err := e.EncodeArrayLen(f.alen); err != nil {
		return err
	}
	return nil
}

func (f *fakeCodec) DecodeMsgPack(d *msgpack.Decoder) error {
	var err error
	if err := d.DecodeNil(); err != nil {
		return err
	}
	if f.bl, err = d.DecodeBool(); err != nil {
		return err
	}
	if f.u8, err = d.DecodeUint8(); err != nil {
		return err
	}
	if val, err := d.DecodeUint(); err != nil || val != uint64(f.u8) {
		return errors.Errorf("DecodingUint size 8 %d %d %v", val, f.u8, err)
	}
	if f.u16, err = d.DecodeUint16(); err != nil {
		return err
	}
	if val, err := d.DecodeUint(); err != nil || val != uint64(f.u16) {
		return errors.Errorf("DecodingUint size 16 %d %d %v", val, f.u16, err)
	}
	if f.u32, err = d.DecodeUint32(); err != nil {
		return err
	}
	if val, err := d.DecodeUint(); err != nil || val != uint64(f.u32) {
		return errors.Errorf("DecodingUint size 32 %d %d %v", val, f.u32, err)
	}
	if f.u64, err = d.DecodeUint64(); err != nil {
		return err
	}
	if val, err := d.DecodeUint(); err != nil || val != f.u64 {
		return errors.Errorf("DecodingUint size 64 %d %d %v", val, f.u64, err)
	}
	if f.i8, err = d.DecodeInt8(); err != nil {
		return err
	}
	if f.i16, err = d.DecodeInt16(); err != nil {
		return err
	}
	if f.st, err = d.DecodeString(); err != nil {
		return err
	}
	if f.bt, err = d.DecodeBytes(); err != nil {
		return err
	}
	if f.alen, err = d.DecodeArrayLen(); err != nil {
		return err
	}
	return nil
}
