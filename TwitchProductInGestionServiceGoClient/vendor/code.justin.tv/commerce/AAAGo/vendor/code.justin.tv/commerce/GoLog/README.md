# GoLog-1.0

GoLog provides an idiomatic logging framework for Go. It exposes a generic Logger interface with an AmznLogger implementation of this interface that logs in the standard Amazon application log format. The GoLog/log package exposes a global logger that can be configured to use any logger that implements the Logger interface. After the programs execution has completed, the Close function must be called to ensure that all logs in the buffer are written to the file. If Close is not called, not all logs are guaranteed to be written out.
If logging to files, it is recommended to use GoLogRotate to provide file rotation.

## Example Usage

This is a barebones logging implementation for FooService. It creates a new AmznLogger that writes to a file in the var/output/logs/ directory and utilizes the global logger to expose the logger to the whole service.

## main Package
```go
package main

import (
    "GoLog/log"
    "GogLog/log/level"
    "os"
)

func main() {
    logger := initLogging()
    defer logger.Close()
    log.Info("Logging initialized")
}

func initLogging() *log.AmznLogger {
    f, err := os.OpenFile("var/output/logs/fooService.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0777)
    logger, err := log.NewAmznLogger(f, "FooService", log.LogLevel(level.Info))
    log.SetLogger(logger)
    return logger
}
```

## fooLibs Package
```go
package amzn/fooLibs

import (
    "GoLog/log"
)

func DoSomething() {
    log.Info("Doing something!")
}
```

Example using a bytes.Buffer and optional parameters.

```go
func initLogging() {
    w := bytes.Buffer{}
    level := level.Fatal
    logger, err := log.NewAmznLogger(w, "FooService", log.LogLevel(level))
    log.SetLogger(logger)
}
```
## Wiki

Please see [the wiki](https://w.amazon.com/index.php/GoLog) for additional implementation details and examples.
