package envinfo

import (
	"strings"
	"testing"
)

func TestOpConfigs(t *testing.T) {
	m, err := NewManifest(strings.NewReader(manifest))
	if err != nil {
		t.Fatal(err)
	}
	configs := m.OpConfigs()

	const itemName = "Apache"
	item, ok := configs[itemName]
	if !ok {
		t.Errorf("expected %s OpConfig, not found", itemName)
	}

	const itemKey = "httpExternalRegularPort"
	port, ok := item[itemKey]
	if !ok {
		t.Errorf("expected %s, not found", itemKey)
	}
	if port != "4080" {
		t.Errorf("expected port 4080, found %s", port)
	}
}

func TestManifest(t *testing.T) {
	m, err := NewManifest(strings.NewReader(manifest))
	if err != nil {
		t.Fatal(err)
	}
	if m["deployment"] == nil {
		for k, _ := range m {
			t.Logf("Found Key: %s", k)
		}
		t.Fatalf("Did not parse manifest correctly")
	}
}

var manifest = `deployment:
    deploymentId = 308730731
    deploymentType = NotDefined
    deploymentCommands = Deactivate ManifestCleanup Pull Check Build Preactivate FlipActivate

environment:
    site = swit1na
    environmentInstanceId = 64237783
    environmentSequenceNum = 201255015
    environmentStageId = 1977221
    environmentFullName = ArmorWebsite/chaten
    environmentName = Alpha
    environmentAlias = ArmorWebsite
    environmentDiffId = 199358687
    stage = Alpha
    runAsUser = chaten

host:
    osrev = Linux-2.6c2.5-x86_64
    environmentInstancesToKeep = 63379807 64237783

instance63379807:
    environmentInstanceId = 63379807
    usedByHostList = chaten-1.desktop.amazon.com

instance64237783:
    environmentInstanceId = 64237783
    usedByInFlightDeploymentsList = 308730729 308729863

packages:
    packageList = ProcessManager BSF Pubsub JavaCore PubsubJava APLCore APLXML HTTPClient SharedColDefs blowfish expat libintl ProcessManagerCommands ApolloConfigurationCommand LogDirectoryPermissionResetCommand CronAdminCommand ApacheSecurity Openssl Rosette Rendezvous Ldap log4j Curl MonitoringCore Pcre Boost Readline Zlib concurrent JakartaCommons-beanutils JakartaCommons-collections Perl XalanJ JakartaCommons-logging JakartaCommons-lang Spiritwave JakartaCommons-httpclient Dom4j CPAN_Scalar_List_Utils CPAN_Compress_Zlib CPAN_Test_Simple CPAN_HTML_Parser CPAN_URI CPAN_libnet CPAN_libwww_perl CPAN_HTML_Tagset CPAN_Digest CPAN_Test_Harness CPAN_Class_Singleton CPAN_Attribute_Handlers CPAN_Params_Validate CPAN_DateTime_TimeZone CPAN_DateTime CPAN_DateTime_Locale CPAN_IO_stringy Joda-time JakartaCommons-codec CPAN_Test CPAN_XML_Simple CPAN_FCGI ICU4C AWSAcctVerificationInterface CPAN_Digest_MD5 CPAN_Digest_SHA1 CPAN_Digest_HMAC CPAN_MIME_Base64 J2ee_servlet BoostDateTime CPAN_XML_Parser CPAN_Log_Log4perl CPAN_Log_Dispatch Bait JakartaTomcatPlatformConfig DataPump CPAN_MailTools CPAN_Bit_Vector CPAN_Date_Calc CPAN_Carp_Clan InfAutoLbcsPerlClient AccessPointCore AccessPointStats ServiceAvailabilityManager AOPAlliance CPAN_Pod_Escapes CPAN_ExtUtils_MakeMaker CPAN_IO_Socket_SSL CPAN_MIME_tools BoostFilesystem TimeSeriesDataLibrary AWSPlatformCommon CPAN_Pod_Simple BoostThread CPAN_Class_ISA FastcgiClient IquitosClient CPAN_Crypt_SSLeay CPAN_XML_XPath CPAN_List_MoreUtils MakePerl58Default CPAN_PathTools CPAN_JSON CPAN_Net_SSLeay_pm CPAN_mod_perl CPAN_MIME_Lite J2ee_jms LibEvent BoostHeaders CPAN_SOAP_Lite TranslateToTsd BoostIostreams BoostRegex AccessControlEncryption AWSSecurityTokenInterface DozerBeanMapper Bzip2 BoostGraph CPAN_podlators OpenCsv JDK Rome JCTServiceAvailabilityManager JavaServiceAvailabilityManager Cronolog MechNormServiceInterface JavaCacheAPI DisableCMFWebsitePushLogsApollo CPAN_Time_Local APLException WebsiteLogPusher JSmart JSmart4JCT ActiveMQJavaClient DirectoryServiceCommon MessagingDNSLookup ActiveMQCPPDiscoveryClient ActiveMQCppClient ActiveMQCppBase Json-org-java Slf4j ZeroTouchVipConfig AmazonJMS BSFJavaClient ZThread Libcap Backport-util-concurrent AWSAccessPolicyInterface RequestRoutingInterfaces JakartaCommons-logging-adapters AmazonIdJava Fbopenssl AmazonTypesJava AmazonAppConfigJava IonJava AmazonApolloEnvironmentInfoJava AmazonFileAppenderJava AmazonProfilerJava Slf4j_log4j AmazonCACerts AmznJms AmqJms UdpJms TibcoJms AmpJms JakartaCommons-logging-api SpringCore SpringContext SpringBeans SpringAOP CoralMetricsReporter CPAN_Log_Dispatch_File_Rolling CoralService CoralMetrics CoralMetricsQuerylogReporter CoralOrchestrator HttpKeytabRetriever JavaHttpSpnego FluxoPerlClient Jackson AmazonJMSDeprecatedInterfaces Sun-JSR-275 CoralBeanValue CoralJsonValue CoralModel CoralQueryStringValue CoralTibcoValue CoralTransmute CoralValue CoralXmlValue RendezvousJava SimpleHttpFilters ApacheXMLSecurityJava BarkPerlLib AuthCpp MakeLatestJDKDefault CoralReflect RequestRoutingInterfacesJava CoralSpring CoralAnnotation CoralActivity MinimalProcessManager AuthJava FluxoJavaClient CoralUtil BasicFluxoJavaClient CoralMetricsBridge CoralAwsSoap CoralAwsAuthentication CoralClient Aspen CoralJavaClientDependencies CoralAwsMetering CoralClientHttp CoralAwsQuery CPAN_ExtUtils_CBuilder CPAN_ExtUtils_ParseXS CPAN_Module_Build FindBugsAnnotations CoralSpringLauncher CoralAvailability PKeyTool CoralSecurity CoralValidate GWT CoralAwsSupport CoralCacheValue LbStatusPerlClient GuacamoleClientConfig GuacamoleJavaClient GuacamoleJavaClientTools SecureJavaKeystoreGenerator CoralJsonSupport JavaEE_EL JavaEE_JSP SpringWeb HBAServiceJavaClient ApacheTomcat6 HBAServiceClientConfig LogPullerLib OdinLocalClientConfig JakartaTomcatFullCleanConfig CoralContinuationOrchestrator PredictionServiceJavaClient JavaAmazonLdapUtils LiveWatchJvmPublisher AWSAuthRuntimeClient AWSAuthRuntimeClientCore TSDAgentConfig ServiceMonitoringTSD ServiceMonitoring GoogleGuava Sun-JSR-305 TomcatApolloWebappLoader CoralRpcSupport OctaneTomcatKerberosSupport TomcatApolloScripts TomcatApolloSetupCommands JavaEE_JSTL RunWithCronolog GlassFishJSTL PingServlet TomcatQueryLogger OctaneTomcatPlatform LiveWatchJvmPublisherServlet LiveWatchJvmPublisherWebapp CoralBuffer AMPJmsExtension CoralDThrottleClient SpringOXM SpringExpression ForceRestartProcessManagerCmds CoralEnvelope AuthJavaHTTP CoralClientConfig CoralConfig CoralGrammar CoralClientBuilder WFAWebServiceJavaClient OdinLocalRetriever CoralEnvelopeCore HttpSigningCpp CoralEnvelopeInterface CoralGeneric CoralSerialize CoralSerializeCore HBAServiceJavaClientTools CoralGenericInterface CoralSchema CoralGenericValue CoralAwsWsdlEmitter CRuntime CoralCompressionEngine IonDatagramConverter ArmorWebsiteContent SalsaServiceClientConfig SalsaServiceJavaClient AWSCryptoJava GoogleGuava-GWT PredictionServiceClientConfig CoralClientNetty PredictionServiceJavaModel CapreseServiceJavaClient CapreseServiceJavaTypes ArmorNotification ArmorQueryExpression ArmorUtils ArmorQueryExpressionSDB CapreseServiceClientConfig ArmorCoralMetrics ArmorSecurity ArmorConcurrent ArmorSerializer ArmorSerializerCSV ArmorNotificationTicket FuegoConfigServiceClientConfig FuegoConfigServiceJavaClient CoralLog4jAppender GFlot AWSAuthRuntimeClientJavaTypes

ProcessManager:
    version = 3678.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 53497856
    deploymentMethod = FTP
    majorVersion = 3678
    newToRelease = false
    packageName = ProcessManager

BSF:
    version = 6779.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 34765506
    deploymentMethod = FTP
    majorVersion = 6779
    newToRelease = false
    packageName = BSF

Pubsub:
    version = 4853.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 20817656
    deploymentMethod = FTP
    majorVersion = 4853
    newToRelease = false
    packageName = Pubsub

JavaCore:
    version = 4303.0-0
    buildVariant = Generic
    bytes = 176669
    deploymentMethod = FTP
    majorVersion = 4303
    newToRelease = false
    packageName = JavaCore

PubsubJava:
    version = 2107.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 45056
    deploymentMethod = FTP
    majorVersion = 2107
    newToRelease = false
    packageName = PubsubJava

APLCore:
    version = 7753.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 37214352
    deploymentMethod = FTP
    majorVersion = 7753
    newToRelease = false
    packageName = APLCore

APLXML:
    version = 3349.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 2000455
    deploymentMethod = FTP
    majorVersion = 3349
    newToRelease = false
    packageName = APLXML

HTTPClient:
    version = 3083.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 96576708
    deploymentMethod = FTP
    majorVersion = 3083
    newToRelease = false
    packageName = HTTPClient

SharedColDefs:
    version = 768.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 92189
    deploymentMethod = FTP
    majorVersion = 768
    minorVersion = 1
    newToRelease = false
    packageName = SharedColDefs

blowfish:
    version = 2909.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 58862
    deploymentMethod = FTP
    majorVersion = 2909
    newToRelease = false
    packageName = blowfish

expat:
    version = 2094.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 311296
    deploymentMethod = FTP
    majorVersion = 2094
    newToRelease = false
    packageName = expat

libintl:
    version = 2291.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 74789
    deploymentMethod = FTP
    majorVersion = 2291
    newToRelease = false
    packageName = libintl

ProcessManagerCommands:
    version = 2198.0-0
    buildVariant = Generic
    bytes = 94208
    deploymentMethod = FTP
    majorVersion = 2198
    newToRelease = false
    packageName = ProcessManagerCommands

ApolloConfigurationCommand:
    version = 2500.0-0
    buildVariant = Generic
    bytes = 45121
    deploymentMethod = FTP
    majorVersion = 2500
    newToRelease = false
    packageName = ApolloConfigurationCommand

LogDirectoryPermissionResetCommand:
    version = 2752.0-0
    buildVariant = Generic
    bytes = 47800
    deploymentMethod = FTP
    majorVersion = 2752
    newToRelease = false
    packageName = LogDirectoryPermissionResetCommand

CronAdminCommand:
    version = 3153.0-0
    buildVariant = Generic
    bytes = 106496
    deploymentMethod = FTP
    majorVersion = 3153
    newToRelease = false
    packageName = CronAdminCommand

ApacheSecurity:
    version = 838.0-0
    buildVariant = Generic
    bytes = 97821
    deploymentMethod = FTP
    majorVersion = 838
    newToRelease = false
    packageName = ApacheSecurity

Openssl:
    version = 5471.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 25667127
    deploymentMethod = FTP
    majorVersion = 5471
    newToRelease = false
    packageName = Openssl

Rosette:
    version = 2681.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 9893176
    deploymentMethod = FTP
    majorVersion = 2681
    newToRelease = false
    packageName = Rosette

Rendezvous:
    version = 4177.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 2034765
    deploymentMethod = FTP
    majorVersion = 4177
    newToRelease = false
    packageName = Rendezvous

Ldap:
    version = 584.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 7943748
    deploymentMethod = FTP
    majorVersion = 584
    newToRelease = false
    packageName = Ldap

log4j:
    version = 1933.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 424849
    deploymentMethod = FTP
    majorVersion = 1933
    newToRelease = false
    packageName = log4j

Curl:
    version = 1090.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 5675245
    deploymentMethod = FTP
    majorVersion = 1090
    newToRelease = false
    packageName = Curl

MonitoringCore:
    version = 2344.0-0
    buildVariant = Generic
    bytes = 1100856
    deploymentMethod = FTP
    majorVersion = 2344
    newToRelease = false
    packageName = MonitoringCore

Pcre:
    version = 1047.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 833469
    deploymentMethod = FTP
    majorVersion = 1047
    minorVersion = 1
    newToRelease = false
    packageName = Pcre

Boost:
    version = 652.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 36864
    deploymentMethod = FTP
    majorVersion = 652
    newToRelease = false
    packageName = Boost

Readline:
    version = 1406.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 245760
    deploymentMethod = FTP
    majorVersion = 1406
    newToRelease = false
    packageName = Readline

Zlib:
    version = 2836.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 330538
    deploymentMethod = FTP
    majorVersion = 2836
    newToRelease = false
    packageName = Zlib

concurrent:
    version = 2761.0-0
    buildVariant = Generic
    bytes = 185646
    deploymentMethod = FTP
    majorVersion = 2761
    newToRelease = false
    packageName = concurrent

JakartaCommons-beanutils:
    version = 1027.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 2166784
    deploymentMethod = FTP
    majorVersion = 1027
    newToRelease = false
    packageName = JakartaCommons-beanutils

JakartaCommons-collections:
    version = 2816.1-0
    buildVariant = Generic
    bytes = 711235
    deploymentMethod = FTP
    majorVersion = 2816
    minorVersion = 1
    newToRelease = false
    packageName = JakartaCommons-collections

Perl:
    version = 2797.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 39430637
    deploymentMethod = FTP
    majorVersion = 2797
    newToRelease = false
    packageName = Perl

XalanJ:
    version = 1149.0-0
    buildVariant = Generic
    bytes = 3300497
    deploymentMethod = FTP
    majorVersion = 1149
    newToRelease = false
    packageName = XalanJ

JakartaCommons-logging:
    version = 3313.0-0
    buildVariant = Generic
    bytes = 33611
    deploymentMethod = FTP
    majorVersion = 3313
    newToRelease = false
    packageName = JakartaCommons-logging

JakartaCommons-lang:
    version = 1018.0-0
    buildVariant = Generic
    bytes = 291770
    deploymentMethod = FTP
    majorVersion = 1018
    newToRelease = false
    packageName = JakartaCommons-lang

Spiritwave:
    version = 2694.0-0
    buildVariant = Generic
    bytes = 926199
    deploymentMethod = FTP
    majorVersion = 2694
    newToRelease = false
    packageName = Spiritwave

JakartaCommons-httpclient:
    version = 4208.0-0
    buildVariant = Generic
    bytes = 246980
    deploymentMethod = FTP
    majorVersion = 4208
    newToRelease = false
    packageName = JakartaCommons-httpclient

Dom4j:
    version = 2861.0-0
    buildVariant = Generic
    bytes = 519531
    deploymentMethod = FTP
    majorVersion = 2861
    newToRelease = false
    packageName = Dom4j

CPAN_Scalar_List_Utils:
    version = 2631.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 33077
    deploymentMethod = FTP
    majorVersion = 2631
    newToRelease = false
    packageName = CPAN_Scalar_List_Utils

CPAN_Compress_Zlib:
    version = 2885.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 257715
    deploymentMethod = FTP
    majorVersion = 2885
    newToRelease = false
    packageName = CPAN_Compress_Zlib

CPAN_Test_Simple:
    version = 2051.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 228424
    deploymentMethod = FTP
    majorVersion = 2051
    newToRelease = false
    packageName = CPAN_Test_Simple

CPAN_HTML_Parser:
    version = 3474.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 438272
    deploymentMethod = FTP
    majorVersion = 3474
    newToRelease = false
    packageName = CPAN_HTML_Parser

CPAN_URI:
    version = 3545.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 387310
    deploymentMethod = FTP
    majorVersion = 3545
    newToRelease = false
    packageName = CPAN_URI

CPAN_libnet:
    version = 2873.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 358048
    deploymentMethod = FTP
    majorVersion = 2873
    newToRelease = false
    packageName = CPAN_libnet

CPAN_libwww_perl:
    version = 3462.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 977943
    deploymentMethod = FTP
    majorVersion = 3462
    newToRelease = false
    packageName = CPAN_libwww_perl

CPAN_HTML_Tagset:
    version = 2329.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 163840
    deploymentMethod = FTP
    majorVersion = 2329
    newToRelease = false
    packageName = CPAN_HTML_Tagset
    patchVersion = 1

CPAN_Digest:
    version = 3624.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 124338
    deploymentMethod = FTP
    majorVersion = 3624
    newToRelease = false
    packageName = CPAN_Digest

CPAN_Test_Harness:
    version = 1903.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 417792
    deploymentMethod = FTP
    majorVersion = 1903
    newToRelease = false
    packageName = CPAN_Test_Harness
    patchVersion = 1

CPAN_Class_Singleton:
    version = 2344.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 94290
    deploymentMethod = FTP
    majorVersion = 2344
    newToRelease = false
    packageName = CPAN_Class_Singleton

CPAN_Attribute_Handlers:
    version = 2625.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 128398
    deploymentMethod = FTP
    majorVersion = 2625
    newToRelease = false
    packageName = CPAN_Attribute_Handlers

CPAN_Params_Validate:
    version = 2704.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 207789
    deploymentMethod = FTP
    majorVersion = 2704
    newToRelease = false
    packageName = CPAN_Params_Validate

CPAN_DateTime_TimeZone:
    version = 2870.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 2537851
    deploymentMethod = FTP
    majorVersion = 2870
    newToRelease = false
    packageName = CPAN_DateTime_TimeZone

CPAN_DateTime:
    version = 3024.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1791625
    deploymentMethod = FTP
    majorVersion = 3024
    newToRelease = false
    packageName = CPAN_DateTime

CPAN_DateTime_Locale:
    version = 2643.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 4321977
    deploymentMethod = FTP
    majorVersion = 2643
    newToRelease = false
    packageName = CPAN_DateTime_Locale

CPAN_IO_stringy:
    version = 3097.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 419975
    deploymentMethod = FTP
    majorVersion = 3097
    newToRelease = false
    packageName = CPAN_IO_stringy

Joda-time:
    version = 1432.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 567852
    deploymentMethod = FTP
    majorVersion = 1432
    newToRelease = false
    packageName = Joda-time

JakartaCommons-codec:
    version = 3237.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 114688
    deploymentMethod = FTP
    majorVersion = 3237
    newToRelease = false
    packageName = JakartaCommons-codec

CPAN_Test:
    version = 2428.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 113933
    deploymentMethod = FTP
    majorVersion = 2428
    newToRelease = false
    packageName = CPAN_Test

CPAN_XML_Simple:
    version = 1905.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 280489
    deploymentMethod = FTP
    majorVersion = 1905
    minorVersion = 1
    newToRelease = false
    packageName = CPAN_XML_Simple

CPAN_FCGI:
    version = 3121.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 199427
    deploymentMethod = FTP
    majorVersion = 3121
    newToRelease = false
    packageName = CPAN_FCGI

ICU4C:
    version = 985.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 52961280
    deploymentMethod = FTP
    majorVersion = 985
    newToRelease = false
    packageName = ICU4C

AWSAcctVerificationInterface:
    version = 1200.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 12101434
    deploymentMethod = FTP
    majorVersion = 1200
    newToRelease = false
    packageName = AWSAcctVerificationInterface

CPAN_Digest_MD5:
    version = 3580.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 122880
    deploymentMethod = FTP
    majorVersion = 3580
    newToRelease = false
    packageName = CPAN_Digest_MD5

CPAN_Digest_SHA1:
    version = 3190.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 202227
    deploymentMethod = FTP
    majorVersion = 3190
    newToRelease = false
    packageName = CPAN_Digest_SHA1

CPAN_Digest_HMAC:
    version = 3127.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 119399
    deploymentMethod = FTP
    majorVersion = 3127
    newToRelease = false
    packageName = CPAN_Digest_HMAC

CPAN_MIME_Base64:
    version = 3674.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 115203
    deploymentMethod = FTP
    majorVersion = 3674
    newToRelease = false
    packageName = CPAN_MIME_Base64

J2ee_servlet:
    version = 2533.0-0
    buildVariant = Generic
    bytes = 147456
    deploymentMethod = FTP
    majorVersion = 2533
    newToRelease = false
    packageName = J2ee_servlet

BoostDateTime:
    version = 677.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 839680
    deploymentMethod = FTP
    majorVersion = 677
    newToRelease = false
    packageName = BoostDateTime

CPAN_XML_Parser:
    version = 3764.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1055727
    deploymentMethod = FTP
    majorVersion = 3764
    newToRelease = false
    packageName = CPAN_XML_Parser

CPAN_Log_Log4perl:
    version = 2086.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 969218
    deploymentMethod = FTP
    majorVersion = 2086
    newToRelease = false
    packageName = CPAN_Log_Log4perl

CPAN_Log_Dispatch:
    version = 1138.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 163840
    deploymentMethod = FTP
    majorVersion = 1138
    newToRelease = false
    packageName = CPAN_Log_Dispatch

Bait:
    version = 4419.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 3700849
    deploymentMethod = FTP
    majorVersion = 4419
    newToRelease = false
    packageName = Bait

JakartaTomcatPlatformConfig:
    version = 565.0-0
    buildVariant = Generic
    bytes = 47693
    deploymentMethod = FTP
    majorVersion = 565
    newToRelease = false
    packageName = JakartaTomcatPlatformConfig

DataPump:
    version = 2603.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 32031680
    deploymentMethod = FTP
    majorVersion = 2603
    newToRelease = false
    packageName = DataPump

CPAN_MailTools:
    version = 1277.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 497694
    deploymentMethod = FTP
    majorVersion = 1277
    newToRelease = false
    packageName = CPAN_MailTools

CPAN_Bit_Vector:
    version = 2846.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 588601
    deploymentMethod = FTP
    majorVersion = 2846
    newToRelease = false
    packageName = CPAN_Bit_Vector

CPAN_Date_Calc:
    version = 2846.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 842744
    deploymentMethod = FTP
    majorVersion = 2846
    newToRelease = false
    packageName = CPAN_Date_Calc

CPAN_Carp_Clan:
    version = 2897.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 110592
    deploymentMethod = FTP
    majorVersion = 2897
    newToRelease = false
    packageName = CPAN_Carp_Clan

InfAutoLbcsPerlClient:
    version = 1430.0-1
    buildVariant = Generic
    bytes = 121983
    deploymentMethod = FTP
    majorVersion = 1430
    newToRelease = false
    packageName = InfAutoLbcsPerlClient
    patchVersion = 1

AccessPointCore:
    version = 3135.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 552299
    deploymentMethod = FTP
    majorVersion = 3135
    newToRelease = false
    packageName = AccessPointCore

AccessPointStats:
    version = 3424.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 4859966
    deploymentMethod = FTP
    majorVersion = 3424
    newToRelease = false
    packageName = AccessPointStats

ServiceAvailabilityManager:
    version = 3317.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 4036248
    deploymentMethod = FTP
    majorVersion = 3317
    newToRelease = false
    packageName = ServiceAvailabilityManager

AOPAlliance:
    version = 1375.0-0
    buildVariant = Generic
    bytes = 45056
    deploymentMethod = FTP
    majorVersion = 1375
    newToRelease = false
    packageName = AOPAlliance

CPAN_Pod_Escapes:
    version = 2181.0-0
    buildVariant = Generic
    bytes = 97103
    deploymentMethod = FTP
    majorVersion = 2181
    newToRelease = false
    packageName = CPAN_Pod_Escapes

CPAN_ExtUtils_MakeMaker:
    version = 2231.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 837239
    deploymentMethod = FTP
    majorVersion = 2231
    newToRelease = false
    packageName = CPAN_ExtUtils_MakeMaker

CPAN_IO_Socket_SSL:
    version = 2604.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 148856
    deploymentMethod = FTP
    majorVersion = 2604
    newToRelease = false
    packageName = CPAN_IO_Socket_SSL

CPAN_MIME_tools:
    version = 3129.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 730805
    deploymentMethod = FTP
    majorVersion = 3129
    newToRelease = false
    packageName = CPAN_MIME_tools

BoostFilesystem:
    version = 924.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 916327
    deploymentMethod = FTP
    majorVersion = 924
    newToRelease = false
    packageName = BoostFilesystem

TimeSeriesDataLibrary:
    version = 2124.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 7132012
    deploymentMethod = FTP
    majorVersion = 2124
    newToRelease = false
    packageName = TimeSeriesDataLibrary

AWSPlatformCommon:
    version = 519.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1055394
    deploymentMethod = FTP
    majorVersion = 519
    newToRelease = false
    packageName = AWSPlatformCommon

CPAN_Pod_Simple:
    version = 2755.0-0
    buildVariant = Generic
    bytes = 929792
    deploymentMethod = FTP
    majorVersion = 2755
    newToRelease = false
    packageName = CPAN_Pod_Simple

BoostThread:
    version = 910.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1226119
    deploymentMethod = FTP
    majorVersion = 910
    newToRelease = false
    packageName = BoostThread

CPAN_Class_ISA:
    version = 2170.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 94208
    deploymentMethod = FTP
    majorVersion = 2170
    newToRelease = false
    packageName = CPAN_Class_ISA

FastcgiClient:
    version = 2648.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 134581
    deploymentMethod = FTP
    majorVersion = 2648
    newToRelease = false
    packageName = FastcgiClient

IquitosClient:
    version = 3152.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1444194
    deploymentMethod = FTP
    majorVersion = 3152
    newToRelease = false
    packageName = IquitosClient

CPAN_Crypt_SSLeay:
    version = 2993.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 184320
    deploymentMethod = FTP
    majorVersion = 2993
    newToRelease = false
    packageName = CPAN_Crypt_SSLeay

CPAN_XML_XPath:
    version = 1583.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 575453
    deploymentMethod = FTP
    majorVersion = 1583
    newToRelease = false
    packageName = CPAN_XML_XPath

CPAN_List_MoreUtils:
    version = 2575.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 159744
    deploymentMethod = FTP
    majorVersion = 2575
    newToRelease = false
    packageName = CPAN_List_MoreUtils

MakePerl58Default:
    version = 2940.0-0
    buildVariant = Generic
    bytes = 33142
    deploymentMethod = FTP
    majorVersion = 2940
    newToRelease = false
    packageName = MakePerl58Default

CPAN_PathTools:
    version = 2521.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 186664
    deploymentMethod = FTP
    majorVersion = 2521
    newToRelease = false
    packageName = CPAN_PathTools

CPAN_JSON:
    version = 1543.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 638976
    deploymentMethod = FTP
    majorVersion = 1543
    newToRelease = false
    packageName = CPAN_JSON

CPAN_Net_SSLeay_pm:
    version = 2830.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 733184
    deploymentMethod = FTP
    majorVersion = 2830
    newToRelease = false
    packageName = CPAN_Net_SSLeay_pm

CPAN_mod_perl:
    version = 2727.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 735554
    deploymentMethod = FTP
    majorVersion = 2727
    newToRelease = false
    packageName = CPAN_mod_perl

CPAN_MIME_Lite:
    version = 2642.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 249568
    deploymentMethod = FTP
    majorVersion = 2642
    newToRelease = false
    packageName = CPAN_MIME_Lite

J2ee_jms:
    version = 2741.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 60174
    deploymentMethod = FTP
    majorVersion = 2741
    newToRelease = false
    packageName = J2ee_jms

LibEvent:
    version = 2496.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1072464
    deploymentMethod = FTP
    majorVersion = 2496
    newToRelease = false
    packageName = LibEvent

BoostHeaders:
    version = 1753.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 33235
    deploymentMethod = FTP
    majorVersion = 1753
    newToRelease = false
    packageName = BoostHeaders

CPAN_SOAP_Lite:
    version = 3716.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1507328
    deploymentMethod = FTP
    majorVersion = 3716
    newToRelease = false
    packageName = CPAN_SOAP_Lite

TranslateToTsd:
    version = 3452.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 52354470
    deploymentMethod = FTP
    majorVersion = 3452
    newToRelease = false
    packageName = TranslateToTsd

BoostIostreams:
    version = 646.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 901643
    deploymentMethod = FTP
    majorVersion = 646
    newToRelease = false
    packageName = BoostIostreams

BoostRegex:
    version = 720.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 12367447
    deploymentMethod = FTP
    majorVersion = 720
    newToRelease = false
    packageName = BoostRegex

AccessControlEncryption:
    version = 284.0-1
    buildVariant = Generic
    bytes = 20480
    deploymentMethod = FTP
    majorVersion = 284
    newToRelease = false
    packageName = AccessControlEncryption
    patchVersion = 1

AWSSecurityTokenInterface:
    version = 818.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 5260643
    deploymentMethod = FTP
    majorVersion = 818
    newToRelease = false
    packageName = AWSSecurityTokenInterface

DozerBeanMapper:
    version = 32.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 282624
    deploymentMethod = FTP
    majorVersion = 32
    newToRelease = false
    packageName = DozerBeanMapper

Bzip2:
    version = 953.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1305186
    deploymentMethod = FTP
    majorVersion = 953
    newToRelease = false
    packageName = Bzip2

BoostGraph:
    version = 570.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 33423
    deploymentMethod = FTP
    majorVersion = 570
    newToRelease = false
    packageName = BoostGraph

CPAN_podlators:
    version = 2528.0-0
    buildVariant = Generic
    bytes = 310898
    deploymentMethod = FTP
    majorVersion = 2528
    newToRelease = false
    packageName = CPAN_podlators

OpenCsv:
    version = 263.0-0
    buildVariant = Generic
    bytes = 53248
    deploymentMethod = FTP
    majorVersion = 263
    newToRelease = false
    packageName = OpenCsv

JDK:
    version = 4344.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 177889183
    deploymentMethod = FTP
    majorVersion = 4344
    newToRelease = false
    packageName = JDK

Rome:
    version = 152.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 262144
    deploymentMethod = FTP
    majorVersion = 152
    newToRelease = false
    packageName = Rome

JCTServiceAvailabilityManager:
    version = 584.0-0
    buildVariant = Generic
    bytes = 20423
    deploymentMethod = FTP
    majorVersion = 584
    newToRelease = false
    packageName = JCTServiceAvailabilityManager

JavaServiceAvailabilityManager:
    version = 543.0-0
    buildVariant = Generic
    bytes = 35008
    deploymentMethod = FTP
    majorVersion = 543
    newToRelease = false
    packageName = JavaServiceAvailabilityManager

Cronolog:
    version = 327.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 100244
    deploymentMethod = FTP
    majorVersion = 327
    newToRelease = false
    packageName = Cronolog

MechNormServiceInterface:
    version = 106.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 15372288
    deploymentMethod = FTP
    majorVersion = 106
    newToRelease = false
    packageName = MechNormServiceInterface

JavaCacheAPI:
    version = 1977.0-0
    buildVariant = Generic
    bytes = 108715
    deploymentMethod = FTP
    majorVersion = 1977
    newToRelease = false
    packageName = JavaCacheAPI

DisableCMFWebsitePushLogsApollo:
    version = 1969.0-0
    buildVariant = Generic
    bytes = 41626
    deploymentMethod = FTP
    majorVersion = 1969
    newToRelease = false
    packageName = DisableCMFWebsitePushLogsApollo

CPAN_Time_Local:
    version = 2484.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 92501
    deploymentMethod = FTP
    majorVersion = 2484
    newToRelease = false
    packageName = CPAN_Time_Local

APLException:
    version = 2757.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 299008
    deploymentMethod = FTP
    majorVersion = 2757
    newToRelease = false
    packageName = APLException

WebsiteLogPusher:
    version = 4800.1-0
    buildVariant = Generic
    bytes = 139264
    deploymentMethod = FTP
    majorVersion = 4800
    minorVersion = 1
    newToRelease = false
    packageName = WebsiteLogPusher

JSmart:
    version = 2873.0-0
    buildVariant = Generic
    bytes = 73940
    deploymentMethod = FTP
    majorVersion = 2873
    newToRelease = false
    packageName = JSmart

JSmart4JCT:
    version = 4172.0-0
    buildVariant = Generic
    bytes = 32768
    deploymentMethod = FTP
    majorVersion = 4172
    newToRelease = false
    packageName = JSmart4JCT

ActiveMQJavaClient:
    version = 6727.0-0
    buildVariant = Generic
    bytes = 993533
    deploymentMethod = FTP
    majorVersion = 6727
    newToRelease = false
    packageName = ActiveMQJavaClient

DirectoryServiceCommon:
    version = 3616.0-0
    buildVariant = Generic
    bytes = 20480
    deploymentMethod = FTP
    majorVersion = 3616
    newToRelease = false
    packageName = DirectoryServiceCommon

MessagingDNSLookup:
    version = 3944.0-0
    buildVariant = Generic
    bytes = 16384
    deploymentMethod = FTP
    majorVersion = 3944
    newToRelease = false
    packageName = MessagingDNSLookup

ActiveMQCPPDiscoveryClient:
    version = 2635.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 881250
    deploymentMethod = FTP
    majorVersion = 2635
    newToRelease = false
    packageName = ActiveMQCPPDiscoveryClient

ActiveMQCppClient:
    version = 5708.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 13606986
    deploymentMethod = FTP
    majorVersion = 5708
    newToRelease = false
    packageName = ActiveMQCppClient

ActiveMQCppBase:
    version = 2585.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 185285
    deploymentMethod = FTP
    majorVersion = 2585
    newToRelease = false
    packageName = ActiveMQCppBase

Json-org-java:
    version = 1320.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 74315
    deploymentMethod = FTP
    majorVersion = 1320
    newToRelease = false
    packageName = Json-org-java

Slf4j:
    version = 1901.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 110592
    deploymentMethod = FTP
    majorVersion = 1901
    newToRelease = false
    packageName = Slf4j

ZeroTouchVipConfig:
    version = 1727.0-0
    buildVariant = Generic
    bytes = 267790
    deploymentMethod = FTP
    majorVersion = 1727
    newToRelease = false
    packageName = ZeroTouchVipConfig

AmazonJMS:
    version = 4102.0-0
    buildVariant = Generic
    bytes = 40271
    deploymentMethod = FTP
    majorVersion = 4102
    newToRelease = false
    packageName = AmazonJMS

BSFJavaClient:
    version = 7211.0-0
    buildVariant = Generic
    bytes = 297518
    deploymentMethod = FTP
    majorVersion = 7211
    newToRelease = false
    packageName = BSFJavaClient

ZThread:
    version = 3111.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 982647
    deploymentMethod = FTP
    majorVersion = 3111
    newToRelease = false
    packageName = ZThread

Libcap:
    version = 1913.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 33013
    deploymentMethod = FTP
    majorVersion = 1913
    newToRelease = false
    packageName = Libcap

Backport-util-concurrent:
    version = 2396.0-0
    buildVariant = Generic
    bytes = 362669
    deploymentMethod = FTP
    majorVersion = 2396
    newToRelease = false
    packageName = Backport-util-concurrent

AWSAccessPolicyInterface:
    version = 604.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 716822
    deploymentMethod = FTP
    majorVersion = 604
    newToRelease = false
    packageName = AWSAccessPolicyInterface

RequestRoutingInterfaces:
    version = 1278.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 267395
    deploymentMethod = FTP
    majorVersion = 1278
    newToRelease = false
    packageName = RequestRoutingInterfaces

JakartaCommons-logging-adapters:
    version = 2497.0-0
    buildVariant = Generic
    bytes = 59889
    deploymentMethod = FTP
    majorVersion = 2497
    newToRelease = false
    packageName = JakartaCommons-logging-adapters

AmazonIdJava:
    version = 4122.0-0
    buildVariant = Generic
    bytes = 37721
    deploymentMethod = FTP
    majorVersion = 4122
    newToRelease = false
    packageName = AmazonIdJava

Fbopenssl:
    version = 644.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 77824
    deploymentMethod = FTP
    majorVersion = 644
    newToRelease = false
    packageName = Fbopenssl

AmazonTypesJava:
    version = 4554.0-0
    buildVariant = Generic
    bytes = 22347
    deploymentMethod = FTP
    majorVersion = 4554
    newToRelease = false
    packageName = AmazonTypesJava

AmazonAppConfigJava:
    version = 4942.0-0
    buildVariant = Generic
    bytes = 126513
    deploymentMethod = FTP
    majorVersion = 4942
    newToRelease = false
    packageName = AmazonAppConfigJava

IonJava:
    version = 2593.0-0
    buildVariant = Generic
    bytes = 1060864
    deploymentMethod = FTP
    majorVersion = 2593
    newToRelease = false
    packageName = IonJava

AmazonApolloEnvironmentInfoJava:
    version = 4389.0-0
    buildVariant = Generic
    bytes = 24576
    deploymentMethod = FTP
    majorVersion = 4389
    newToRelease = false
    packageName = AmazonApolloEnvironmentInfoJava

AmazonFileAppenderJava:
    version = 4312.0-0
    buildVariant = Generic
    bytes = 32768
    deploymentMethod = FTP
    majorVersion = 4312
    newToRelease = false
    packageName = AmazonFileAppenderJava

AmazonProfilerJava:
    version = 4326.0-0
    buildVariant = Generic
    bytes = 30454
    deploymentMethod = FTP
    majorVersion = 4326
    newToRelease = false
    packageName = AmazonProfilerJava

Slf4j_log4j:
    version = 780.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 52345
    deploymentMethod = FTP
    majorVersion = 780
    minorVersion = 1
    newToRelease = false
    packageName = Slf4j_log4j

AmazonCACerts:
    version = 1078.0-0
    buildVariant = Generic
    bytes = 282398
    deploymentMethod = FTP
    majorVersion = 1078
    newToRelease = false
    packageName = AmazonCACerts

AmznJms:
    version = 3833.0-0
    buildVariant = Generic
    bytes = 122880
    deploymentMethod = FTP
    majorVersion = 3833
    newToRelease = false
    packageName = AmznJms

AmqJms:
    version = 4076.0-0
    buildVariant = Generic
    bytes = 70011
    deploymentMethod = FTP
    majorVersion = 4076
    newToRelease = false
    packageName = AmqJms

UdpJms:
    version = 3438.0-0
    buildVariant = Generic
    bytes = 57480
    deploymentMethod = FTP
    majorVersion = 3438
    newToRelease = false
    packageName = UdpJms

TibcoJms:
    version = 3070.0-0
    buildVariant = Generic
    bytes = 185701
    deploymentMethod = FTP
    majorVersion = 3070
    newToRelease = false
    packageName = TibcoJms

AmpJms:
    version = 3859.0-0
    buildVariant = Generic
    bytes = 34616
    deploymentMethod = FTP
    majorVersion = 3859
    newToRelease = false
    packageName = AmpJms

JakartaCommons-logging-api:
    version = 2892.0-0
    buildVariant = Generic
    bytes = 85406
    deploymentMethod = FTP
    majorVersion = 2892
    newToRelease = false
    packageName = JakartaCommons-logging-api

SpringCore:
    version = 2169.0-0
    buildVariant = Generic
    bytes = 446759
    deploymentMethod = FTP
    majorVersion = 2169
    newToRelease = false
    packageName = SpringCore

SpringContext:
    version = 2152.0-0
    buildVariant = Generic
    bytes = 1034326
    deploymentMethod = FTP
    majorVersion = 2152
    newToRelease = false
    packageName = SpringContext

SpringBeans:
    version = 2160.1-0
    buildVariant = Generic
    bytes = 938861
    deploymentMethod = FTP
    majorVersion = 2160
    minorVersion = 1
    newToRelease = false
    packageName = SpringBeans

SpringAOP:
    version = 2109.0-0
    buildVariant = Generic
    bytes = 339968
    deploymentMethod = FTP
    majorVersion = 2109
    newToRelease = false
    packageName = SpringAOP

CoralMetricsReporter:
    version = 4658.0-0
    buildVariant = Generic
    bytes = 79789
    deploymentMethod = FTP
    majorVersion = 4658
    newToRelease = false
    packageName = CoralMetricsReporter

CPAN_Log_Dispatch_File_Rolling:
    version = 1066.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 106496
    deploymentMethod = FTP
    majorVersion = 1066
    newToRelease = false
    packageName = CPAN_Log_Dispatch_File_Rolling

CoralService:
    version = 4930.0-0
    buildVariant = Generic
    bytes = 386679
    deploymentMethod = FTP
    majorVersion = 4930
    newToRelease = false
    packageName = CoralService

CoralMetrics:
    version = 3697.0-0
    buildVariant = Generic
    bytes = 24576
    deploymentMethod = FTP
    majorVersion = 3697
    newToRelease = false
    packageName = CoralMetrics

CoralMetricsQuerylogReporter:
    version = 3052.0-0
    buildVariant = Generic
    bytes = 49519
    deploymentMethod = FTP
    majorVersion = 3052
    newToRelease = false
    packageName = CoralMetricsQuerylogReporter

CoralOrchestrator:
    version = 4425.0-0
    buildVariant = Generic
    bytes = 192100
    deploymentMethod = FTP
    majorVersion = 4425
    newToRelease = false
    packageName = CoralOrchestrator

HttpKeytabRetriever:
    version = 527.0-0
    buildVariant = Generic
    bytes = 49419
    deploymentMethod = FTP
    majorVersion = 527
    newToRelease = false
    packageName = HttpKeytabRetriever

JavaHttpSpnego:
    version = 361.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 45349
    deploymentMethod = FTP
    majorVersion = 361
    newToRelease = false
    packageName = JavaHttpSpnego

FluxoPerlClient:
    version = 1381.0-0
    buildVariant = Generic
    bytes = 81920
    deploymentMethod = FTP
    majorVersion = 1381
    newToRelease = false
    packageName = FluxoPerlClient

Jackson:
    version = 1347.0-0
    buildVariant = Generic
    bytes = 245760
    deploymentMethod = FTP
    majorVersion = 1347
    newToRelease = false
    packageName = Jackson

AmazonJMSDeprecatedInterfaces:
    version = 3258.0-0
    buildVariant = Generic
    bytes = 39010
    deploymentMethod = FTP
    majorVersion = 3258
    newToRelease = false
    packageName = AmazonJMSDeprecatedInterfaces

Sun-JSR-275:
    version = 3258.0-0
    buildVariant = Generic
    bytes = 105337
    deploymentMethod = FTP
    majorVersion = 3258
    newToRelease = false
    packageName = Sun-JSR-275

CoralBeanValue:
    version = 3256.0-0
    buildVariant = Generic
    bytes = 66123
    deploymentMethod = FTP
    majorVersion = 3256
    newToRelease = false
    packageName = CoralBeanValue

CoralJsonValue:
    version = 2000.0-0
    buildVariant = Generic
    bytes = 50459
    deploymentMethod = FTP
    majorVersion = 2000
    newToRelease = false
    packageName = CoralJsonValue

CoralModel:
    version = 2759.0-0
    buildVariant = Generic
    bytes = 517617
    deploymentMethod = FTP
    majorVersion = 2759
    newToRelease = false
    packageName = CoralModel

CoralQueryStringValue:
    version = 1899.0-0
    buildVariant = Generic
    bytes = 31767
    deploymentMethod = FTP
    majorVersion = 1899
    newToRelease = false
    packageName = CoralQueryStringValue

CoralTibcoValue:
    version = 2122.0-0
    buildVariant = Generic
    bytes = 45056
    deploymentMethod = FTP
    majorVersion = 2122
    newToRelease = false
    packageName = CoralTibcoValue

CoralTransmute:
    version = 3886.0-0
    buildVariant = Generic
    bytes = 365922
    deploymentMethod = FTP
    majorVersion = 3886
    newToRelease = false
    packageName = CoralTransmute

CoralValue:
    version = 1521.0-0
    buildVariant = Generic
    bytes = 48573
    deploymentMethod = FTP
    majorVersion = 1521
    newToRelease = false
    packageName = CoralValue

CoralXmlValue:
    version = 2479.0-0
    buildVariant = Generic
    bytes = 57238
    deploymentMethod = FTP
    majorVersion = 2479
    newToRelease = false
    packageName = CoralXmlValue

RendezvousJava:
    version = 1316.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 172032
    deploymentMethod = FTP
    majorVersion = 1316
    newToRelease = false
    packageName = RendezvousJava

SimpleHttpFilters:
    version = 104.0-0
    buildVariant = Generic
    bytes = 21874
    deploymentMethod = FTP
    majorVersion = 104
    newToRelease = false
    packageName = SimpleHttpFilters

ApacheXMLSecurityJava:
    version = 494.1-0
    buildVariant = Generic
    bytes = 476819
    deploymentMethod = FTP
    majorVersion = 494
    minorVersion = 1
    newToRelease = false
    packageName = ApacheXMLSecurityJava

BarkPerlLib:
    version = 4225.0-0
    buildVariant = Generic
    bytes = 122880
    deploymentMethod = FTP
    majorVersion = 4225
    newToRelease = false
    packageName = BarkPerlLib

AuthCpp:
    version = 2429.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 329971
    deploymentMethod = FTP
    majorVersion = 2429
    newToRelease = false
    packageName = AuthCpp

MakeLatestJDKDefault:
    version = 858.0-0
    buildVariant = Generic
    bytes = 15747
    deploymentMethod = FTP
    majorVersion = 858
    newToRelease = false
    packageName = MakeLatestJDKDefault

CoralReflect:
    version = 2357.0-0
    buildVariant = Generic
    bytes = 376832
    deploymentMethod = FTP
    majorVersion = 2357
    newToRelease = false
    packageName = CoralReflect

RequestRoutingInterfacesJava:
    version = 3578.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 19689
    deploymentMethod = FTP
    majorVersion = 3578
    newToRelease = false
    packageName = RequestRoutingInterfacesJava

CoralSpring:
    version = 2510.0-0
    buildVariant = Generic
    bytes = 32642
    deploymentMethod = FTP
    majorVersion = 2510
    newToRelease = false
    packageName = CoralSpring

CoralAnnotation:
    version = 1658.0-0
    buildVariant = Generic
    bytes = 27403
    deploymentMethod = FTP
    majorVersion = 1658
    newToRelease = false
    packageName = CoralAnnotation

CoralActivity:
    version = 2663.0-0
    buildVariant = Generic
    bytes = 24576
    deploymentMethod = FTP
    majorVersion = 2663
    newToRelease = false
    packageName = CoralActivity

MinimalProcessManager:
    version = 1594.0-0
    buildVariant = Generic
    bytes = 28564
    deploymentMethod = FTP
    majorVersion = 1594
    newToRelease = false
    packageName = MinimalProcessManager

AuthJava:
    version = 4341.0-0
    buildVariant = Generic
    bytes = 26012
    deploymentMethod = FTP
    majorVersion = 4341
    newToRelease = false
    packageName = AuthJava

FluxoJavaClient:
    version = 411.0-0
    buildVariant = Generic
    bytes = 33515
    deploymentMethod = FTP
    majorVersion = 411
    newToRelease = false
    packageName = FluxoJavaClient

CoralUtil:
    version = 3873.0-0
    buildVariant = Generic
    bytes = 184218
    deploymentMethod = FTP
    majorVersion = 3873
    newToRelease = false
    packageName = CoralUtil

BasicFluxoJavaClient:
    version = 412.0-0
    buildVariant = Generic
    bytes = 28797
    deploymentMethod = FTP
    majorVersion = 412
    newToRelease = false
    packageName = BasicFluxoJavaClient

CoralMetricsBridge:
    version = 2404.0-0
    buildVariant = Generic
    bytes = 27473
    deploymentMethod = FTP
    majorVersion = 2404
    newToRelease = false
    packageName = CoralMetricsBridge

CoralAwsSoap:
    version = 655.0-0
    buildVariant = Generic
    bytes = 123754
    deploymentMethod = FTP
    majorVersion = 655
    newToRelease = false
    packageName = CoralAwsSoap

CoralAwsAuthentication:
    version = 1445.0-0
    buildVariant = Generic
    bytes = 196735
    deploymentMethod = FTP
    majorVersion = 1445
    newToRelease = false
    packageName = CoralAwsAuthentication

CoralClient:
    version = 4517.0-0
    buildVariant = Generic
    bytes = 91472
    deploymentMethod = FTP
    majorVersion = 4517
    newToRelease = false
    packageName = CoralClient

Aspen:
    version = 851.0-0
    buildVariant = Generic
    bytes = 209703
    deploymentMethod = FTP
    majorVersion = 851
    newToRelease = false
    packageName = Aspen

CoralJavaClientDependencies:
    version = 1288.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 39723
    deploymentMethod = FTP
    majorVersion = 1288
    newToRelease = false
    packageName = CoralJavaClientDependencies

CoralAwsMetering:
    version = 501.0-0
    buildVariant = Generic
    bytes = 15009
    deploymentMethod = FTP
    majorVersion = 501
    newToRelease = false
    packageName = CoralAwsMetering

CoralClientHttp:
    version = 2696.0-0
    buildVariant = Generic
    bytes = 53512
    deploymentMethod = FTP
    majorVersion = 2696
    newToRelease = false
    packageName = CoralClientHttp

CoralAwsQuery:
    version = 2622.0-0
    buildVariant = Generic
    bytes = 127783
    deploymentMethod = FTP
    majorVersion = 2622
    newToRelease = false
    packageName = CoralAwsQuery

CPAN_ExtUtils_CBuilder:
    version = 2468.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 151552
    deploymentMethod = FTP
    majorVersion = 2468
    newToRelease = false
    packageName = CPAN_ExtUtils_CBuilder

CPAN_ExtUtils_ParseXS:
    version = 2468.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 131072
    deploymentMethod = FTP
    majorVersion = 2468
    newToRelease = false
    packageName = CPAN_ExtUtils_ParseXS

CPAN_Module_Build:
    version = 2495.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 860160
    deploymentMethod = FTP
    majorVersion = 2495
    newToRelease = false
    packageName = CPAN_Module_Build

FindBugsAnnotations:
    version = 1036.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 47804
    deploymentMethod = FTP
    majorVersion = 1036
    newToRelease = false
    packageName = FindBugsAnnotations

CoralSpringLauncher:
    version = 2677.0-0
    buildVariant = Generic
    bytes = 69058
    deploymentMethod = FTP
    majorVersion = 2677
    newToRelease = false
    packageName = CoralSpringLauncher

CoralAvailability:
    version = 2258.0-0
    buildVariant = Generic
    bytes = 64621
    deploymentMethod = FTP
    majorVersion = 2258
    newToRelease = false
    packageName = CoralAvailability

PKeyTool:
    version = 216.0-0
    buildVariant = Generic
    bytes = 73749
    deploymentMethod = FTP
    majorVersion = 216
    newToRelease = false
    packageName = PKeyTool

CoralSecurity:
    version = 1859.0-0
    buildVariant = Generic
    bytes = 46061
    deploymentMethod = FTP
    majorVersion = 1859
    newToRelease = false
    packageName = CoralSecurity

CoralValidate:
    version = 2955.0-0
    buildVariant = Generic
    bytes = 46463
    deploymentMethod = FTP
    majorVersion = 2955
    newToRelease = false
    packageName = CoralValidate

GWT:
    version = 65.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 49270784
    deploymentMethod = FTP
    majorVersion = 65
    newToRelease = false
    packageName = GWT

CoralAwsSupport:
    version = 255.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 4096
    deploymentMethod = FTP
    majorVersion = 255
    newToRelease = false
    packageName = CoralAwsSupport

CoralCacheValue:
    version = 2990.0-0
    buildVariant = Generic
    bytes = 38160
    deploymentMethod = FTP
    majorVersion = 2990
    newToRelease = false
    packageName = CoralCacheValue

LbStatusPerlClient:
    version = 1242.0-0
    buildVariant = Generic
    bytes = 86016
    deploymentMethod = FTP
    majorVersion = 1242
    newToRelease = false
    packageName = LbStatusPerlClient

GuacamoleClientConfig:
    version = 5.0-0
    buildVariant = Generic
    bytes = 53248
    deploymentMethod = FTP
    majorVersion = 5
    newToRelease = false
    packageName = GuacamoleClientConfig

GuacamoleJavaClient:
    version = 40.0-0
    buildVariant = Generic
    bytes = 213688
    deploymentMethod = FTP
    majorVersion = 40
    newToRelease = false
    packageName = GuacamoleJavaClient

GuacamoleJavaClientTools:
    version = 68.0-0
    buildVariant = Generic
    bytes = 39002
    deploymentMethod = FTP
    majorVersion = 68
    newToRelease = false
    packageName = GuacamoleJavaClientTools

SecureJavaKeystoreGenerator:
    version = 264.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 49152
    deploymentMethod = FTP
    majorVersion = 264
    newToRelease = false
    packageName = SecureJavaKeystoreGenerator

CoralJsonSupport:
    version = 2848.0-0
    buildVariant = Generic
    bytes = 66153
    deploymentMethod = FTP
    majorVersion = 2848
    newToRelease = false
    packageName = CoralJsonSupport

JavaEE_EL:
    version = 357.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 69632
    deploymentMethod = FTP
    majorVersion = 357
    newToRelease = false
    packageName = JavaEE_EL
    patchVersion = 1

JavaEE_JSP:
    version = 396.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 109618
    deploymentMethod = FTP
    majorVersion = 396
    newToRelease = false
    packageName = JavaEE_JSP

SpringWeb:
    version = 282.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 413696
    deploymentMethod = FTP
    majorVersion = 282
    newToRelease = false
    packageName = SpringWeb

HBAServiceJavaClient:
    version = 19.0-0
    buildVariant = Generic
    bytes = 41477
    deploymentMethod = FTP
    majorVersion = 19
    newToRelease = false
    packageName = HBAServiceJavaClient

ApacheTomcat6:
    version = 622.0-0
    buildVariant = Generic
    bytes = 8685906
    deploymentMethod = FTP
    majorVersion = 622
    newToRelease = false
    packageName = ApacheTomcat6

HBAServiceClientConfig:
    version = 4.0-0
    buildVariant = Generic
    bytes = 49152
    deploymentMethod = FTP
    majorVersion = 4
    newToRelease = false
    packageName = HBAServiceClientConfig

LogPullerLib:
    version = 4886.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 274785
    deploymentMethod = FTP
    majorVersion = 4886
    newToRelease = false
    packageName = LogPullerLib

OdinLocalClientConfig:
    version = 1896.0-0
    buildVariant = Generic
    bytes = 41508
    deploymentMethod = FTP
    majorVersion = 1896
    newToRelease = false
    packageName = OdinLocalClientConfig

JakartaTomcatFullCleanConfig:
    version = 396.0-0
    buildVariant = Generic
    bytes = 37535
    deploymentMethod = FTP
    majorVersion = 396
    newToRelease = false
    packageName = JakartaTomcatFullCleanConfig

CoralContinuationOrchestrator:
    version = 3348.0-0
    buildVariant = Generic
    bytes = 43052
    deploymentMethod = FTP
    majorVersion = 3348
    newToRelease = false
    packageName = CoralContinuationOrchestrator

PredictionServiceJavaClient:
    version = 30.0-0
    buildVariant = Generic
    bytes = 119521
    deploymentMethod = FTP
    majorVersion = 30
    newToRelease = false
    packageName = PredictionServiceJavaClient

JavaAmazonLdapUtils:
    version = 135.0-0
    buildVariant = Generic
    bytes = 39520
    deploymentMethod = FTP
    majorVersion = 135
    newToRelease = false
    packageName = JavaAmazonLdapUtils

LiveWatchJvmPublisher:
    version = 889.0-1
    buildVariant = Generic
    bytes = 24413
    deploymentMethod = FTP
    majorVersion = 889
    newToRelease = false
    packageName = LiveWatchJvmPublisher
    patchVersion = 1

AWSAuthRuntimeClient:
    version = 1111.0-0
    buildVariant = Generic
    bytes = 46147
    deploymentMethod = FTP
    majorVersion = 1111
    newToRelease = false
    packageName = AWSAuthRuntimeClient

AWSAuthRuntimeClientCore:
    version = 1047.0-0
    buildVariant = Generic
    bytes = 242309
    deploymentMethod = FTP
    majorVersion = 1047
    newToRelease = false
    packageName = AWSAuthRuntimeClientCore

TSDAgentConfig:
    version = 1317.0-0
    buildVariant = Generic
    bytes = 45056
    deploymentMethod = FTP
    majorVersion = 1317
    newToRelease = false
    packageName = TSDAgentConfig

ServiceMonitoringTSD:
    version = 1766.1-0
    buildVariant = Generic
    bytes = 81920
    deploymentMethod = FTP
    majorVersion = 1766
    minorVersion = 1
    newToRelease = false
    packageName = ServiceMonitoringTSD

ServiceMonitoring:
    version = 1363.0-0
    buildVariant = Generic
    bytes = 36864
    deploymentMethod = FTP
    majorVersion = 1363
    newToRelease = false
    packageName = ServiceMonitoring

GoogleGuava:
    version = 859.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1924493
    deploymentMethod = FTP
    majorVersion = 859
    newToRelease = false
    packageName = GoogleGuava

Sun-JSR-305:
    version = 4.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 73728
    deploymentMethod = FTP
    majorVersion = 4
    minorVersion = 1
    newToRelease = false
    packageName = Sun-JSR-305

TomcatApolloWebappLoader:
    version = 353.0-0
    buildVariant = Generic
    bytes = 26265
    deploymentMethod = FTP
    majorVersion = 353
    newToRelease = false
    packageName = TomcatApolloWebappLoader

CoralRpcSupport:
    version = 3422.0-0
    buildVariant = Generic
    bytes = 86660
    deploymentMethod = FTP
    majorVersion = 3422
    newToRelease = false
    packageName = CoralRpcSupport

OctaneTomcatKerberosSupport:
    version = 169.0-0
    buildVariant = Generic
    bytes = 37420
    deploymentMethod = FTP
    majorVersion = 169
    newToRelease = false
    packageName = OctaneTomcatKerberosSupport

TomcatApolloScripts:
    version = 467.0-0
    buildVariant = Generic
    bytes = 103674
    deploymentMethod = FTP
    majorVersion = 467
    newToRelease = false
    packageName = TomcatApolloScripts

TomcatApolloSetupCommands:
    version = 392.0-1
    buildVariant = Generic
    bytes = 41438
    deploymentMethod = FTP
    majorVersion = 392
    newToRelease = false
    packageName = TomcatApolloSetupCommands
    patchVersion = 1

JavaEE_JSTL:
    version = 182.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 64301
    deploymentMethod = FTP
    majorVersion = 182
    newToRelease = false
    packageName = JavaEE_JSTL

RunWithCronolog:
    version = 285.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 40960
    deploymentMethod = FTP
    majorVersion = 285
    newToRelease = false
    packageName = RunWithCronolog

GlassFishJSTL:
    version = 187.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 426239
    deploymentMethod = FTP
    majorVersion = 187
    newToRelease = false
    packageName = GlassFishJSTL

PingServlet:
    version = 220.0-0
    buildVariant = Generic
    bytes = 13633
    deploymentMethod = FTP
    majorVersion = 220
    newToRelease = false
    packageName = PingServlet

TomcatQueryLogger:
    version = 364.0-0
    buildVariant = Generic
    bytes = 25515
    deploymentMethod = FTP
    majorVersion = 364
    newToRelease = false
    packageName = TomcatQueryLogger

OctaneTomcatPlatform:
    version = 319.0-0
    buildVariant = Generic
    bytes = 4096
    deploymentMethod = FTP
    majorVersion = 319
    newToRelease = false
    packageName = OctaneTomcatPlatform

LiveWatchJvmPublisherServlet:
    version = 219.0-1
    buildVariant = Generic
    bytes = 24576
    deploymentMethod = FTP
    majorVersion = 219
    newToRelease = false
    packageName = LiveWatchJvmPublisherServlet
    patchVersion = 1

LiveWatchJvmPublisherWebapp:
    version = 212.0-1
    buildVariant = Generic
    bytes = 53248
    deploymentMethod = FTP
    majorVersion = 212
    newToRelease = false
    packageName = LiveWatchJvmPublisherWebapp
    patchVersion = 1

CoralBuffer:
    version = 2179.0-0
    buildVariant = Generic
    bytes = 37519
    deploymentMethod = FTP
    majorVersion = 2179
    newToRelease = false
    packageName = CoralBuffer

AMPJmsExtension:
    version = 3059.0-0
    buildVariant = Generic
    bytes = 15280
    deploymentMethod = FTP
    majorVersion = 3059
    newToRelease = false
    packageName = AMPJmsExtension

CoralDThrottleClient:
    version = 978.0-0
    buildVariant = Generic
    bytes = 16355
    deploymentMethod = FTP
    majorVersion = 978
    newToRelease = false
    packageName = CoralDThrottleClient

SpringOXM:
    version = 179.0-0
    buildVariant = Generic
    bytes = 81920
    deploymentMethod = FTP
    majorVersion = 179
    newToRelease = false
    packageName = SpringOXM

SpringExpression:
    version = 1361.0-0
    buildVariant = Generic
    bytes = 183038
    deploymentMethod = FTP
    majorVersion = 1361
    newToRelease = false
    packageName = SpringExpression

ForceRestartProcessManagerCmds:
    version = 1496.0-0
    buildVariant = Generic
    bytes = 12530
    deploymentMethod = FTP
    majorVersion = 1496
    newToRelease = false
    packageName = ForceRestartProcessManagerCmds

CoralEnvelope:
    version = 2979.0-0
    buildVariant = Generic
    bytes = 40960
    deploymentMethod = FTP
    majorVersion = 2979
    newToRelease = false
    packageName = CoralEnvelope

AuthJavaHTTP:
    version = 502.0-0
    buildVariant = Generic
    bytes = 43141
    deploymentMethod = FTP
    majorVersion = 502
    newToRelease = false
    packageName = AuthJavaHTTP

CoralClientConfig:
    version = 3540.0-0
    buildVariant = Generic
    bytes = 47119
    deploymentMethod = FTP
    majorVersion = 3540
    newToRelease = false
    packageName = CoralClientConfig

CoralConfig:
    version = 2301.0-0
    buildVariant = Generic
    bytes = 98304
    deploymentMethod = FTP
    majorVersion = 2301
    newToRelease = false
    packageName = CoralConfig

CoralGrammar:
    version = 1942.0-0
    buildVariant = Generic
    bytes = 491012
    deploymentMethod = FTP
    majorVersion = 1942
    newToRelease = false
    packageName = CoralGrammar

CoralClientBuilder:
    version = 1662.0-0
    buildVariant = Generic
    bytes = 57185
    deploymentMethod = FTP
    majorVersion = 1662
    newToRelease = false
    packageName = CoralClientBuilder

WFAWebServiceJavaClient:
    version = 41.0-0
    buildVariant = Generic
    bytes = 992686
    deploymentMethod = FTP
    majorVersion = 41
    newToRelease = false
    packageName = WFAWebServiceJavaClient

OdinLocalRetriever:
    version = 1000.0-0
    buildVariant = Generic
    bytes = 26095
    deploymentMethod = FTP
    majorVersion = 1000
    newToRelease = false
    packageName = OdinLocalRetriever

CoralEnvelopeCore:
    version = 2702.0-0
    buildVariant = Generic
    bytes = 79686
    deploymentMethod = FTP
    majorVersion = 2702
    newToRelease = false
    packageName = CoralEnvelopeCore

HttpSigningCpp:
    version = 1240.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 1309314
    deploymentMethod = FTP
    majorVersion = 1240
    newToRelease = false
    packageName = HttpSigningCpp

CoralEnvelopeInterface:
    version = 1401.0-0
    buildVariant = Generic
    bytes = 13656
    deploymentMethod = FTP
    majorVersion = 1401
    newToRelease = false
    packageName = CoralEnvelopeInterface

CoralGeneric:
    version = 1425.0-0
    buildVariant = Generic
    bytes = 53879
    deploymentMethod = FTP
    majorVersion = 1425
    newToRelease = false
    packageName = CoralGeneric

CoralSerialize:
    version = 2600.0-0
    buildVariant = Generic
    bytes = 49152
    deploymentMethod = FTP
    majorVersion = 2600
    newToRelease = false
    packageName = CoralSerialize

CoralSerializeCore:
    version = 2314.0-0
    buildVariant = Generic
    bytes = 40960
    deploymentMethod = FTP
    majorVersion = 2314
    newToRelease = false
    packageName = CoralSerializeCore

HBAServiceJavaClientTools:
    version = 16.0-0
    buildVariant = Generic
    bytes = 17904
    deploymentMethod = FTP
    majorVersion = 16
    newToRelease = false
    packageName = HBAServiceJavaClientTools

CoralGenericInterface:
    version = 1276.0-0
    buildVariant = Generic
    bytes = 17767
    deploymentMethod = FTP
    majorVersion = 1276
    newToRelease = false
    packageName = CoralGenericInterface

CoralSchema:
    version = 1205.0-0
    buildVariant = Generic
    bytes = 20403
    deploymentMethod = FTP
    majorVersion = 1205
    newToRelease = false
    packageName = CoralSchema

CoralGenericValue:
    version = 2161.0-0
    buildVariant = Generic
    bytes = 33389
    deploymentMethod = FTP
    majorVersion = 2161
    newToRelease = false
    packageName = CoralGenericValue

CoralAwsWsdlEmitter:
    version = 437.0-0
    buildVariant = Generic
    bytes = 26205
    deploymentMethod = FTP
    majorVersion = 437
    newToRelease = false
    packageName = CoralAwsWsdlEmitter

CRuntime:
    version = 331.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 4096
    deploymentMethod = FTP
    majorVersion = 331
    newToRelease = false
    packageName = CRuntime

CoralCompressionEngine:
    version = 866.0-0
    buildVariant = Generic
    bytes = 32313
    deploymentMethod = FTP
    majorVersion = 866
    newToRelease = false
    packageName = CoralCompressionEngine

IonDatagramConverter:
    version = 1265.0-0
    buildVariant = Generic
    bytes = 28040
    deploymentMethod = FTP
    majorVersion = 1265
    newToRelease = false
    packageName = IonDatagramConverter

ArmorWebsiteContent:
    version = 428.0-0
    buildVariant = Generic
    bytes = 83543170
    deploymentMethod = FTP
    majorVersion = 428
    newToRelease = true
    packageName = ArmorWebsiteContent

SalsaServiceClientConfig:
    version = 14.0-0
    buildVariant = Generic
    bytes = 17505
    deploymentMethod = FTP
    majorVersion = 14
    newToRelease = false
    packageName = SalsaServiceClientConfig

SalsaServiceJavaClient:
    version = 44.0-0
    buildVariant = Generic
    bytes = 195380
    deploymentMethod = FTP
    majorVersion = 44
    newToRelease = false
    packageName = SalsaServiceJavaClient

AWSCryptoJava:
    version = 16.0-0
    buildVariant = Generic
    bytes = 16785
    deploymentMethod = FTP
    majorVersion = 16
    newToRelease = false
    packageName = AWSCryptoJava

GoogleGuava-GWT:
    version = 7.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 750534
    deploymentMethod = FTP
    majorVersion = 7
    newToRelease = false
    packageName = GoogleGuava-GWT

PredictionServiceClientConfig:
    version = 10.0-0
    buildVariant = Generic
    bytes = 17544
    deploymentMethod = FTP
    majorVersion = 10
    newToRelease = false
    packageName = PredictionServiceClientConfig

CoralClientNetty:
    version = 617.0-0
    buildVariant = Generic
    bytes = 861113
    deploymentMethod = FTP
    majorVersion = 617
    newToRelease = false
    packageName = CoralClientNetty

PredictionServiceJavaModel:
    version = 20.0-0
    buildVariant = Generic
    bytes = 108506
    deploymentMethod = FTP
    majorVersion = 20
    newToRelease = false
    packageName = PredictionServiceJavaModel

CapreseServiceJavaClient:
    version = 70.0-0
    buildVariant = Generic
    bytes = 77373
    deploymentMethod = FTP
    majorVersion = 70
    newToRelease = true
    packageName = CapreseServiceJavaClient

CapreseServiceJavaTypes:
    version = 73.0-0
    buildVariant = Generic
    bytes = 424215
    deploymentMethod = FTP
    majorVersion = 73
    newToRelease = true
    packageName = CapreseServiceJavaTypes

ArmorNotification:
    version = 2.0-0
    buildVariant = Generic
    bytes = 14683
    deploymentMethod = FTP
    majorVersion = 2
    newToRelease = false
    packageName = ArmorNotification

ArmorQueryExpression:
    version = 23.0-0
    buildVariant = Generic
    bytes = 24703
    deploymentMethod = FTP
    majorVersion = 23
    newToRelease = false
    packageName = ArmorQueryExpression

ArmorUtils:
    version = 139.0-0
    buildVariant = Generic
    bytes = 57588
    deploymentMethod = FTP
    majorVersion = 139
    newToRelease = true
    packageName = ArmorUtils

ArmorQueryExpressionSDB:
    version = 22.0-0
    buildVariant = Generic
    bytes = 22007
    deploymentMethod = FTP
    majorVersion = 22
    newToRelease = false
    packageName = ArmorQueryExpressionSDB

CapreseServiceClientConfig:
    version = 10.0-0
    buildVariant = Generic
    bytes = 18144
    deploymentMethod = FTP
    majorVersion = 10
    newToRelease = false
    packageName = CapreseServiceClientConfig

ArmorCoralMetrics:
    version = 17.0-0
    buildVariant = Generic
    bytes = 31733
    deploymentMethod = FTP
    majorVersion = 17
    newToRelease = false
    packageName = ArmorCoralMetrics

ArmorSecurity:
    version = 22.0-0
    buildVariant = Generic
    bytes = 21041
    deploymentMethod = FTP
    majorVersion = 22
    newToRelease = true
    packageName = ArmorSecurity

ArmorConcurrent:
    version = 42.0-0
    buildVariant = Generic
    bytes = 29576
    deploymentMethod = FTP
    majorVersion = 42
    newToRelease = false
    packageName = ArmorConcurrent

ArmorSerializer:
    version = 2.0-0
    buildVariant = Generic
    bytes = 20120
    deploymentMethod = FTP
    majorVersion = 2
    newToRelease = false
    packageName = ArmorSerializer

ArmorSerializerCSV:
    version = 2.0-0
    buildVariant = Generic
    bytes = 23018
    deploymentMethod = FTP
    majorVersion = 2
    newToRelease = false
    packageName = ArmorSerializerCSV

ArmorNotificationTicket:
    version = 2.0-0
    buildVariant = Generic
    bytes = 21491
    deploymentMethod = FTP
    majorVersion = 2
    newToRelease = false
    packageName = ArmorNotificationTicket

FuegoConfigServiceClientConfig:
    version = 19.0-0
    buildVariant = Generic
    bytes = 21536
    deploymentMethod = FTP
    majorVersion = 19
    newToRelease = false
    packageName = FuegoConfigServiceClientConfig

FuegoConfigServiceJavaClient:
    version = 67.0-0
    buildVariant = Generic
    bytes = 421146
    deploymentMethod = FTP
    majorVersion = 67
    newToRelease = false
    packageName = FuegoConfigServiceJavaClient

CoralLog4jAppender:
    version = 2.0-0
    buildVariant = Generic
    bytes = 23833
    deploymentMethod = FTP
    majorVersion = 2
    newToRelease = false
    packageName = CoralLog4jAppender

GFlot:
    version = 2.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    bytes = 440446
    deploymentMethod = FTP
    majorVersion = 2
    newToRelease = false
    packageName = GFlot

AWSAuthRuntimeClientJavaTypes:
    version = 26.0-0
    buildVariant = Generic
    bytes = 15341
    deploymentMethod = FTP
    majorVersion = 26
    newToRelease = false
    packageName = AWSAuthRuntimeClientJavaTypes

packageVersionsToKeep:
    packageList = ArmorSecurity-22.0-0 ArmorUtils-139.0-0 CapreseServiceJavaTypes-73.0-0 CapreseServiceJavaClient-70.0-0 ArmorWebsiteContent-428.0-0 ArmorWebsiteContent-421.0-0 CapreseServiceJavaTypes-65.0-0 CapreseServiceJavaClient-63.0-0 ArmorWebsiteContent-419.0-0 Sun-JSR-305-4.1-0 AWSAuthRuntimeClientJavaTypes-26.0-0 FuegoConfigServiceJavaClient-67.0-0 ArmorSerializerCSV-2.0-0 ArmorSerializer-2.0-0 ArmorQueryExpressionSDB-22.0-0 ArmorUtils-103.0-0 ArmorQueryExpression-23.0-0 CapreseServiceJavaTypes-62.0-0 CapreseServiceJavaClient-60.0-0 CoralClientNetty-617.0-0 GoogleGuava-GWT-7.0-0 SalsaServiceJavaClient-44.0-0 SalsaServiceClientConfig-14.0-0 IonDatagramConverter-1265.0-0 WFAWebServiceJavaClient-41.0-0 CoralClientBuilder-1662.0-0 CoralClientConfig-3540.0-0 TomcatQueryLogger-364.0-0 CoralRpcSupport-3422.0-0 GoogleGuava-859.0-0 AWSAuthRuntimeClientCore-1047.0-0 AWSAuthRuntimeClient-1111.0-0 JavaAmazonLdapUtils-135.0-0 CoralSpringLauncher-2677.0-0 FindBugsAnnotations-1036.0-0 CoralAwsQuery-2622.0-0 CoralClientHttp-2696.0-0 Aspen-851.0-0 CoralClient-4517.0-0 CoralAwsAuthentication-1445.0-0 CoralMetricsQuerylogReporter-3052.0-0 CoralMetricsReporter-4658.0-0 Libcap-1913.0-0 Bzip2-953.0-0 CPAN_Pod_Simple-2755.0-0 CPAN_ExtUtils_MakeMaker-2231.0-0 ServiceAvailabilityManager-3317.0-0 AccessPointCore-3135.0-0 DataPump-2603.0-0 CPAN_Log_Log4perl-2086.0-0 CPAN_MIME_Base64-3674.0-0 CPAN_Digest_MD5-3580.0-0 AWSAcctVerificationInterface-1200.0-0 CPAN_Test-2428.0-0 CPAN_Digest-3624.0-0 CPAN_libnet-2873.0-0 CPAN_URI-3545.0-0 CPAN_HTML_Parser-3474.0-0 CPAN_Test_Simple-2051.0-0 CPAN_Compress_Zlib-2885.0-0 Rosette-2681.0-0 Openssl-5471.0-0 blowfish-2909.0-0 APLXML-3349.0-0 APLCore-7753.0-0 Pubsub-4853.0-0 BSF-6779.0-0 SalsaServiceJavaClient-42.0-0 SalsaServiceJavaClient-41.0-0 FuegoConfigServiceJavaClient-62.0-0 GFlot-2.0-0 CoralLog4jAppender-2.0-0 ArmorCoralMetrics-17.0-0 FuegoConfigServiceJavaClient-58.0-0 FuegoConfigServiceClientConfig-19.0-0 ArmorNotificationTicket-2.0-0 ArmorConcurrent-42.0-0 ArmorSecurity-6.0-0 CapreseServiceClientConfig-10.0-0 ArmorQueryExpressionSDB-13.0-0 ArmorUtils-66.0-0 ArmorQueryExpression-12.0-0 ArmorNotification-2.0-0 PredictionServiceJavaModel-20.0-0 CoralClientNetty-596.0-0 PredictionServiceClientConfig-10.0-0 SalsaServiceClientConfig-12.0-0 CoralCompressionEngine-866.0-0 CoralAwsWsdlEmitter-437.0-0 CoralGenericValue-2161.0-0 CoralGenericInterface-1276.0-0 HBAServiceJavaClientTools-16.0-0 CoralSerializeCore-2314.0-0 CoralSerialize-2600.0-0 CoralGeneric-1425.0-0 CoralEnvelopeInterface-1401.0-0 CoralEnvelopeCore-2702.0-0 OdinLocalRetriever-1000.0-0 WFAWebServiceJavaClient-37.0-0 CoralClientBuilder-1493.0-0 CoralClientConfig-3379.0-0 CoralEnvelope-2979.0-0 CoralDThrottleClient-978.0-0 CoralBuffer-2179.0-0 CoralRpcSupport-3389.0-0 PredictionServiceJavaClient-30.0-0 CoralContinuationOrchestrator-3348.0-0 LogPullerLib-4886.0-0 ApacheTomcat6-622.0-0 HBAServiceJavaClient-19.0-0 CoralJsonSupport-2848.0-0 GuacamoleJavaClientTools-68.0-0 GuacamoleJavaClient-40.0-0 CoralCacheValue-2990.0-0 CoralAwsSupport-255.0-0 CoralValidate-2955.0-0 CoralSecurity-1859.0-0 CoralAvailability-2258.0-0 CoralSpringLauncher-2603.0-0 CoralAwsQuery-2543.0-0 CoralClientHttp-2665.0-0 CoralAwsMetering-501.0-0 CoralJavaClientDependencies-1288.0-0 CoralClient-4384.0-0 CoralAwsAuthentication-1408.0-0 CoralAwsSoap-655.0-0 CoralMetricsBridge-2404.0-0 CoralUtil-3873.0-0 CoralSpring-2510.0-0 MakeLatestJDKDefault-858.0-0 BarkPerlLib-4225.0-0 CoralTransmute-3886.0-0 CoralTibcoValue-2122.0-0 CoralQueryStringValue-1899.0-0 CoralModel-2759.0-0 CoralBeanValue-3256.0-0 CoralOrchestrator-4425.0-0 CoralService-4930.0-0 AmazonCACerts-1078.0-0 ZeroTouchVipConfig-1727.0-0 JDK-4344.0-0 CPAN_XML_XPath-1583.0-0 CPAN_XML_Parser-3764.0-0 CPAN_Digest_HMAC-3127.0-0 AWSAcctVerificationInterface-1139.0-0 CPAN_FCGI-3121.0-0 CPAN_libwww_perl-3462.0-0 Perl-2797.0-0 MonitoringCore-2344.0-0 Rosette-2606.0-0 AmazonCACerts-905.0-0 AWSAuthRuntimeClientJavaTypes-2.0-0 CoralSerialize-2580.0-0 CoralClientBuilder-1467.0-0 CoralGrammar-1942.0-0 CoralConfig-2301.0-0 CoralClientConfig-3336.0-0 CoralEnvelope-2955.0-0 CoralRpcSupport-3206.0-0 AWSAuthRuntimeClientCore-967.0-0 AWSAuthRuntimeClient-1006.0-0 CoralContinuationOrchestrator-3301.0-0 CoralJsonSupport-2840.0-0 CoralCacheValue-2964.0-0 CoralValidate-2935.0-0 CoralAwsQuery-2534.0-0 CoralClientHttp-2486.0-0 CoralClient-4288.0-0 CoralAwsAuthentication-1332.0-0 CoralAwsSoap-641.0-0 BasicFluxoJavaClient-412.0-0 MinimalProcessManager-1594.0-0 CoralAnnotation-1658.0-0 CoralXmlValue-2479.0-0 CoralTransmute-3802.0-0 CoralModel-2661.0-0 CoralJsonValue-2000.0-0 CoralOrchestrator-4335.0-0 CoralService-4799.0-0 ZThread-3111.0-0 BSFJavaClient-7211.0-0 JSmart4JCT-4172.0-0 Bait-4419.0-0 CPAN_Digest_SHA1-3190.0-0 AWSAcctVerificationInterface-1110.0-0 CPAN_IO_stringy-3097.0-0 CPAN_DateTime_TimeZone-2870.0-0 JakartaCommons-httpclient-4208.0-0 Rendezvous-4177.0-0 ArmorSerializerCSV-1.0-0 ArmorSerializer-1.0-0 OpenCsv-263.0-0 CoralMetricsReporter-4365.0-0 ArmorNotificationTicket-1.0-0 FluxoJavaClient-411.0-0 ArmorNotification-1.0-0 BasicFluxoJavaClient-362.1-0 AWSAuthRuntimeClientJavaTypes-1.0-0 FuegoConfigServiceClientConfig-14.0-0 ArmorConcurrent-20.0-0 CapreseServiceClientConfig-9.0-0 CoralClientNetty-355.0-0 PredictionServiceClientConfig-7.0-0 SalsaServiceClientConfig-9.0-0 CRuntime-331.0-0 CoralSchema-1205.0-0 HBAServiceJavaClientTools-15.0-0 CoralSerializeCore-2238.0-0 CoralSerialize-2501.0-0 WFAWebServiceJavaClient-30.0-1 CoralGrammar-1736.0-0 CoralConfig-2218.0-0 AuthJavaHTTP-502.0-0 SpringExpression-1361.0-0 SpringOXM-179.0-0 CoralBuffer-2044.0-0 RunWithCronolog-285.0-0 TomcatApolloScripts-467.0-0 OctaneTomcatKerberosSupport-169.0-0 TomcatApolloWebappLoader-353.0-0 GoogleGuava-554.0-0 ServiceMonitoringTSD-1766.1-0 AWSAuthRuntimeClientCore-944.0-0 AWSAuthRuntimeClient-981.0-0 JavaAmazonLdapUtils-123.0-0 JakartaTomcatFullCleanConfig-396.0-0 OdinLocalClientConfig-1896.0-0 LogPullerLib-4587.1-0 SpringWeb-282.0-0 CoralJsonSupport-2539.0-0 SecureJavaKeystoreGenerator-264.0-0 GuacamoleJavaClientTools-67.0-0 GuacamoleClientConfig-5.0-0 CoralAwsSupport-214.0-0 CoralAvailability-2129.0-0 CoralSpringLauncher-2464.0-0 CoralAwsMetering-490.0-0 Aspen-778.0-0 CoralAwsSoap-607.0-0 CoralUtil-3704.0-0 AuthJava-4341.0-0 MinimalProcessManager-1567.0-0 CoralActivity-2663.0-0 CoralSpring-2348.0-0 BarkPerlLib-4026.0-0 ApacheXMLSecurityJava-494.1-0 CoralValue-1521.0-0 CoralTransmute-3652.0-0 CoralTibcoValue-1902.0-0 CoralQueryStringValue-1808.0-0 CoralModel-2446.0-0 Sun-JSR-275-3258.0-0 CoralOrchestrator-4070.0-0 CoralMetricsQuerylogReporter-2815.0-0 CoralService-4677.0-0 CoralMetricsReporter-4285.0-0 SpringAOP-2109.0-0 SpringBeans-2160.1-0 SpringContext-2152.0-0 SpringCore-2169.0-0 Slf4j_log4j-780.1-0 AmazonProfilerJava-4326.0-0 IonJava-2593.0-0 AmazonAppConfigJava-4942.0-0 AmazonTypesJava-4554.0-0 AWSAccessPolicyInterface-604.0-0 BSFJavaClient-7093.0-0 ZeroTouchVipConfig-1630.0-0 Slf4j-1901.0-0 ActiveMQCppBase-2585.0-0 ActiveMQCPPDiscoveryClient-2635.0-0 WebsiteLogPusher-4800.1-0 APLException-2757.0-0 DisableCMFWebsitePushLogsApollo-1969.0-0 MechNormServiceInterface-106.0-0 Cronolog-327.0-0 JCTServiceAvailabilityManager-584.0-0 JDK-4229.0-0 CPAN_podlators-2528.0-0 AWSSecurityTokenInterface-818.0-0 TranslateToTsd-3452.0-0 CPAN_SOAP_Lite-3716.0-0 BoostHeaders-1753.0-0 LibEvent-2496.0-0 CPAN_mod_perl-2727.0-0 CPAN_Net_SSLeay_pm-2830.0-0 CPAN_Crypt_SSLeay-2993.0-0 IquitosClient-3152.0-0 FastcgiClient-2648.0-0 AWSPlatformCommon-519.0-0 TimeSeriesDataLibrary-2124.0-0 CPAN_MIME_tools-3129.0-0 InfAutoLbcsPerlClient-1430.0-1 CPAN_MailTools-1277.0-0 JakartaTomcatPlatformConfig-565.0-0 Bait-4290.0-0 ICU4C-985.0-0 CPAN_XML_Simple-1905.1-0 JakartaCommons-codec-3237.0-0 CPAN_Class_Singleton-2344.0-0 JakartaCommons-logging-3313.0-0 JakartaCommons-collections-2816.1-0 Pcre-1047.1-0 Curl-1090.0-0 Ldap-584.0-0 Openssl-5206.0-0 ApacheSecurity-838.0-0 CronAdminCommand-3153.0-0 LogDirectoryPermissionResetCommand-2752.0-0 HTTPClient-3083.0-0 APLCore-7527.0-0 BSF-6164.0-0 FuegoConfigServiceClientConfig-6.0-0 ArmorConcurrent-6.0-0 CapreseServiceClientConfig-4.0-0 PredictionServiceJavaModel-11.0-0 CRuntime-196.0-0 OctaneTomcatPlatform-319.0-0 TomcatApolloSetupCommands-392.0-1 TomcatApolloScripts-430.0-1 PredictionServiceJavaClient-24.0-0 LbStatusPerlClient-1242.0-0 BarkPerlLib-3935.0-0 FluxoPerlClient-1381.0-0 CoralMetrics-3697.0-0 ZeroTouchVipConfig-1503.1-0 Json-org-java-1320.0-0 JDK-4164.0-0 TranslateToTsd-3349.0-0 CPAN_List_MoreUtils-2575.0-0 TimeSeriesDataLibrary-2088.0-0 InfAutoLbcsPerlClient-1384.0-0 Bait-4223.0-0 APLCore-7296.0-0 CRuntime-4.0-0 PredictionServiceJavaClient-22.0-0 PredictionServiceClientConfig-2.1-1 PredictionServiceJavaModel-8.0-0 IonDatagramConverter-880.0-0 HBAServiceJavaClientTools-14.0-0 CoralEnvelopeCore-2332.0-0 OdinLocalRetriever-716.0-0 CoralConfig-1994.0-0 LiveWatchJvmPublisherWebapp-212.0-1 LiveWatchJvmPublisherServlet-219.0-1 TomcatQueryLogger-304.0-0 ServiceMonitoringTSD-1710.1-0 LiveWatchJvmPublisher-889.0-1 CoralContinuationOrchestrator-2936.0-0 LogPullerLib-4482.0-0 GuacamoleJavaClientTools-64.0-0 CoralAwsSupport-206.0-0 GWT-65.0-0 CoralAvailability-2072.0-0 Aspen-728.0-0 CoralUtil-3449.0-0 CoralActivity-2600.0-0 CoralSpring-2180.0-0 RequestRoutingInterfacesJava-3578.0-0 CoralReflect-2357.0-0 MakeLatestJDKDefault-756.0-0 AuthCpp-2429.0-0 Jackson-1347.0-0 CoralMetricsQuerylogReporter-2586.0-0 AmpJms-3859.0-0 IonJava-2523.0-0 AWSAccessPolicyInterface-599.0-0 BSFJavaClient-6685.0-0 AmazonJMS-4102.0-0 DirectoryServiceCommon-3616.0-0 JSmart-2873.0-0 WebsiteLogPusher-4717.0-0 MechNormServiceInterface-105.0-0 JavaServiceAvailabilityManager-543.0-0 JCTServiceAvailabilityManager-574.0-0 AWSSecurityTokenInterface-813.0-0 CPAN_JSON-1543.0-0 MakePerl58Default-2940.0-0 IquitosClient-2997.0-0 FastcgiClient-2517.0-0 AccessPointStats-3424.0-0 DataPump-2296.0-0 JakartaCommons-lang-1018.0-0 Openssl-5055.0-0 ApolloConfigurationCommand-2500.0-0 ProcessManagerCommands-2198.0-0 HTTPClient-2926.1-0 PubsubJava-2107.0-0 BSF-5879.1-0 ProcessManager-3678.0-0 ArmorSecurity-5.0-0 CoralReflect-2339.0-0 AWSCryptoJava-16.0-0 CoralAwsWsdlEmitter-313.0-0 CoralSchema-923.0-0 CoralGenericInterface-943.0-0 CoralGeneric-1091.0-0 CoralEnvelopeInterface-1067.0-0 HttpSigningCpp-1240.0-0 OdinLocalRetriever-698.1-0 CoralGrammar-1626.0-0 AuthJavaHTTP-429.0-0 ForceRestartProcessManagerCmds-1496.0-0 AMPJmsExtension-3059.0-0 LiveWatchJvmPublisherWebapp-208.0-0 LiveWatchJvmPublisherServlet-212.0-0 PingServlet-220.0-0 ServiceMonitoring-1363.0-0 LiveWatchJvmPublisher-855.0-0 JavaAmazonLdapUtils-113.0-1 CoralSecurity-1625.0-0 FindBugsAnnotations-877.0-0 CoralAnnotation-1424.0-0 ApacheXMLSecurityJava-466.0-0 CoralValue-1408.0-0 Sun-JSR-275-3096.0-0 AmazonJMSDeprecatedInterfaces-3258.0-0 FluxoPerlClient-686.0-0 JavaHttpSpnego-361.0-0 HttpKeytabRetriever-527.0-0 CoralMetrics-3612.0-0 CPAN_Log_Dispatch_File_Rolling-1066.0-0 TibcoJms-3070.0-0 UdpJms-3438.0-0 AmqJms-4076.0-0 AmznJms-3833.0-0 Slf4j_log4j-718.0-0 AmazonProfilerJava-3973.0-0 AmazonFileAppenderJava-4312.0-0 AmazonApolloEnvironmentInfoJava-4389.0-0 Fbopenssl-644.0-0 AmazonIdJava-4122.0-0 RequestRoutingInterfaces-1278.0-0 Json-org-java-1321.0-0 ActiveMQCppClient-5708.0-0 MessagingDNSLookup-3944.0-0 ActiveMQJavaClient-6727.0-0 JavaCacheAPI-1977.0-0 DozerBeanMapper-32.0-0 AccessControlEncryption-284.0-1 TranslateToTsd-3254.1-0 J2ee_jms-2741.0-0 IquitosClient-2938.0-0 AWSPlatformCommon-497.0-0 TimeSeriesDataLibrary-2008.0-0 CPAN_Pod_Escapes-2181.0-0 InfAutoLbcsPerlClient-694.0-0 CPAN_Log_Dispatch-1138.0-0 ICU4C-915.0-0 CPAN_XML_Simple-1155.0-0 Joda-time-1432.0-0 CPAN_HTML_Tagset-2329.0-1 JakartaCommons-httpclient-3918.0-1 JakartaCommons-lang-1002.0-0 Perl-2736.0-0 JakartaCommons-beanutils-1027.0-0 MonitoringCore-2069.0-0 HTTPClient-2873.1-0 PubsubJava-2037.0-3 JavaCore-4303.0-0 Pubsub-4255.0-0 BoostHeaders-1567.0-1 GoogleGuava-GWT-2.1-0 GlassFishJSTL-187.0-0 JavaEE_JSTL-182.0-0 GoogleGuava-443.1-0 TSDAgentConfig-1317.0-0 HBAServiceClientConfig-4.0-0 HBAServiceJavaClient-18.0-0 JavaEE_JSP-396.0-0 JavaEE_EL-357.0-1 GuacamoleJavaClient-38.0-0 PKeyTool-216.0-0 CPAN_Module_Build-2495.0-0 CPAN_ExtUtils_ParseXS-2468.0-0 CPAN_ExtUtils_CBuilder-2468.0-0 RendezvousJava-1316.0-0 CoralXmlValue-2159.0-0 JakartaCommons-logging-api-2892.0-0 JakartaCommons-logging-adapters-2497.0-0 AWSAccessPolicyInterface-568.0-0 Backport-util-concurrent-2396.0-0 Libcap-1581.0-0 CPAN_Time_Local-2484.0-0 MechNormServiceInterface-104.0-0 Rome-152.0-0 BoostGraph-570.0-0 Bzip2-711.0-0 AWSSecurityTokenInterface-779.0-0 BoostRegex-720.0-0 BoostIostreams-646.0-0 BoostHeaders-1543.0-0 LibEvent-2352.0-0 CPAN_MIME_Lite-2642.0-0 CPAN_mod_perl-2621.0-0 CPAN_Net_SSLeay_pm-2624.0-0 CPAN_PathTools-2521.0-0 CPAN_List_MoreUtils-2497.0-0 CPAN_Crypt_SSLeay-2779.0-0 CPAN_Class_ISA-2170.0-0 BoostThread-910.0-0 CPAN_Pod_Simple-2390.0-0 BoostFilesystem-924.0-0 CPAN_IO_Socket_SSL-2604.0-0 CPAN_ExtUtils_MakeMaker-1939.0-0 AOPAlliance-1375.0-0 ServiceAvailabilityManager-2761.0-0 AccessPointCore-2628.0-0 CPAN_Carp_Clan-2897.0-0 CPAN_Date_Calc-2846.0-0 CPAN_Bit_Vector-2846.0-0 CPAN_Log_Log4perl-1790.0-1 BoostDateTime-677.0-0 J2ee_servlet-2533.0-0 CPAN_MIME_Base64-3189.0-0 CPAN_Digest_MD5-3100.0-0 CPAN_Test-2134.0-0 CPAN_DateTime_Locale-2643.0-0 CPAN_DateTime-3024.0-0 CPAN_DateTime_TimeZone-2644.0-0 CPAN_Params_Validate-2704.0-0 CPAN_Attribute_Handlers-2625.0-0 CPAN_Test_Harness-1903.0-1 CPAN_Digest-3145.0-0 CPAN_libnet-2523.0-0 CPAN_URI-3067.0-0 CPAN_HTML_Parser-2983.0-0 CPAN_Test_Simple-1777.0-0 CPAN_Compress_Zlib-2519.0-0 CPAN_Scalar_List_Utils-2631.0-0 Dom4j-2861.0-0 Spiritwave-2694.0-0 XalanJ-1149.0-0 JakartaCommons-collections-2665.0-0 concurrent-2761.0-0 Zlib-2836.0-0 Boost-652.0-0 log4j-1933.0-0 Ldap-524.0-0 Rosette-2300.0-0 libintl-2291.0-0 expat-2094.0-0 blowfish-2530.0-0 ProcessManager-3520.0-0 ArmorQueryExpressionSDB-2.0-0 ArmorQueryExpression-2.0-0 CoralJsonValue-1653.0-1 IonJava-2424.0-0 APLXML-2837.0-0 CoralCompressionEngine-331.0-0 AWSCryptoJava-3.0-0 IonDatagramConverter-148.0-0 CoralAwsWsdlEmitter-1.0-0 CoralGenericValue-1690.0-0 CoralSchema-1.0-0 CoralGenericInterface-22.0-0 CoralSerializeCore-1302.0-1 CoralGeneric-133.0-0 CoralEnvelopeInterface-114.0-0 HttpSigningCpp-6.1-0 CoralEnvelopeCore-1686.0-1 AuthJavaHTTP-181.0-0 CoralEnvelope-2481.0-0 ForceRestartProcessManagerCmds-656.0-0 SpringExpression-582.1-0 SpringOXM-77.1-0 CoralDThrottleClient-715.1-0 AMPJmsExtension-1065.0-1 CoralBuffer-1121.0-0 LiveWatchJvmPublisherWebapp-52.0-0 LiveWatchJvmPublisherServlet-54.0-0 OctaneTomcatPlatform-143.0-0 TomcatQueryLogger-132.0-0 PingServlet-74.0-0 GlassFishJSTL-4.0-0 RunWithCronolog-152.0-0 JavaEE_JSTL-4.0-0 TomcatApolloSetupCommands-166.0-0 TomcatApolloScripts-202.0-0 OctaneTomcatKerberosSupport-104.0-0 TomcatApolloWebappLoader-163.0-0 ServiceMonitoring-368.1-0 ServiceMonitoringTSD-783.1-0 TSDAgentConfig-22.0-0 LiveWatchJvmPublisher-218.0-0 JakartaTomcatFullCleanConfig-237.0-0 OdinLocalClientConfig-1061.0-0 HBAServiceClientConfig-1.1-1 ApacheTomcat6-552.0-0 HBAServiceJavaClient-16.0-1 SpringWeb-165.1-0 JavaEE_JSP-17.0-0 JavaEE_EL-14.0-0 SecureJavaKeystoreGenerator-150.0-1 GuacamoleJavaClient-36.0-1 GuacamoleClientConfig-3.0-0 LbStatusPerlClient-592.0-0 CoralCacheValue-2518.0-0 GWT-36.0-0 CoralValidate-857.0-0 CoralSecurity-738.0-0 PKeyTool-7.0-0 FindBugsAnnotations-239.0-0 CPAN_Module_Build-276.0-0 CPAN_ExtUtils_ParseXS-276.0-0 CPAN_ExtUtils_CBuilder-275.0-0 CoralAwsMetering-433.0-0 CoralJavaClientDependencies-661.0-0 CoralMetricsBridge-789.0-0 AuthJava-2917.0-0 MinimalProcessManager-910.1-0 CoralActivity-1733.0-0 CoralAnnotation-480.0-0 RequestRoutingInterfacesJava-2630.0-0 CoralReflect-2074.0-0 MakeLatestJDKDefault-583.0-0 AuthCpp-1445.0-0 ApacheXMLSecurityJava-152.0-0 SimpleHttpFilters-104.0-0 RendezvousJava-104.0-0 CoralXmlValue-1214.0-0 CoralValue-482.0-0 CoralTibcoValue-1000.0-0 CoralQueryStringValue-1496.0-0 CoralJsonValue-682.0-0 CoralBeanValue-2787.0-0 Sun-JSR-275-1095.1-0 AmazonJMSDeprecatedInterfaces-1381.0-0 Jackson-638.0-0 FluxoPerlClient-684.0-0 JavaHttpSpnego-150.0-0 HttpKeytabRetriever-286.2-0 CoralMetrics-1484.1-0 CPAN_Log_Dispatch_File_Rolling-38.1-0 SpringAOP-1191.1-0 SpringBeans-1190.1-0 SpringContext-1229.1-0 SpringCore-1240.1-0 JakartaCommons-logging-api-304.0-0 AmpJms-2884.0-0 TibcoJms-1319.0-0 UdpJms-1546.0-0 AmqJms-1987.1-0 AmznJms-1814.1-0 Slf4j_log4j-187.1-0 AmazonProfilerJava-1706.0-0 AmazonFileAppenderJava-2679.0-1 AmazonApolloEnvironmentInfoJava-2155.0-0 AmazonAppConfigJava-4642.0-0 AmazonTypesJava-3627.1-0 Fbopenssl-307.1-0 AmazonIdJava-2122.0-0 JakartaCommons-logging-adapters-252.0-0 RequestRoutingInterfaces-254.0-0 Backport-util-concurrent-282.0-0 Libcap-22.0-0 ZThread-2306.0-0 AmazonJMS-3121.0-0 Slf4j-1294.0-1 ActiveMQCppBase-1642.0-0 ActiveMQCppClient-3884.1-0 ActiveMQCPPDiscoveryClient-1685.0-0 MessagingDNSLookup-2399.0-0 DirectoryServiceCommon-2656.0-0 ActiveMQJavaClient-4664.1-0 JSmart4JCT-3460.1-0 JSmart-1990.0-0 WebsiteLogPusher-4342.0-1 APLException-1765.0-0 CPAN_Time_Local-278.0-0 DisableCMFWebsitePushLogsApollo-883.1-0 JavaCacheAPI-806.0-0 Cronolog-176.0-0 JavaServiceAvailabilityManager-353.0-0 JCTServiceAvailabilityManager-402.0-0 Rome-20.0-0 CPAN_podlators-1687.0-0 BoostGraph-81.0-0 Bzip2-81.0-0 DozerBeanMapper-16.0-0 AccessControlEncryption-63.0-0 BoostRegex-171.0-0 BoostIostreams-261.0-1 CPAN_SOAP_Lite-2996.1-0 LibEvent-267.0-0 J2ee_jms-877.0-0 CPAN_MIME_Lite-331.0-0 CPAN_mod_perl-323.0-0 CPAN_Net_SSLeay_pm-319.0-0 CPAN_JSON-748.1-0 CPAN_PathTools-282.0-0 MakePerl58Default-2019.0-0 CPAN_List_MoreUtils-279.0-0 CPAN_XML_XPath-1165.2-0 CPAN_Crypt_SSLeay-347.0-0 FastcgiClient-1286.0-0 CPAN_Class_ISA-246.0-0 BoostThread-359.0-2 CPAN_Pod_Simple-258.0-0 AWSPlatformCommon-388.0-0 BoostFilesystem-178.0-0 CPAN_MIME_tools-2290.1-0 CPAN_IO_Socket_SSL-1164.0-0 CPAN_ExtUtils_MakeMaker-78.0-0 CPAN_Pod_Escapes-1005.0-0 AOPAlliance-66.0-0 ServiceAvailabilityManager-644.0-0 AccessPointStats-1966.0-0 AccessPointCore-558.0-0 CPAN_Carp_Clan-365.0-0 CPAN_Date_Calc-323.0-0 CPAN_Bit_Vector-319.0-0 CPAN_MailTools-544.1-0 DataPump-1238.1-0 JakartaTomcatPlatformConfig-365.0-0 CPAN_Log_Dispatch-51.1-0 CPAN_Log_Log4perl-90.0-0 CPAN_XML_Parser-2974.1-0 BoostDateTime-149.0-0 J2ee_servlet-639.0-0 CPAN_MIME_Base64-385.0-0 CPAN_Digest_HMAC-2361.1-0 CPAN_Digest_SHA1-2377.1-0 CPAN_Digest_MD5-357.0-0 ICU4C-432.0-0 CPAN_FCGI-2370.1-0 CPAN_XML_Simple-1017.1-0 CPAN_Test-84.0-0 JakartaCommons-codec-2335.0-0 Joda-time-432.1-0 CPAN_IO_stringy-2353.1-0 CPAN_DateTime_Locale-368.0-0 CPAN_DateTime-609.0-0 CPAN_DateTime_TimeZone-373.1-0 CPAN_Params_Validate-315.0-0 CPAN_Attribute_Handlers-294.0-0 CPAN_Class_Singleton-1497.0-0 CPAN_Test_Harness-633.0-1 CPAN_Digest-362.0-0 CPAN_HTML_Tagset-465.0-1 CPAN_libwww_perl-2953.0-0 CPAN_libnet-215.0-0 CPAN_URI-381.0-0 CPAN_HTML_Parser-340.0-0 CPAN_Test_Simple-122.0-0 CPAN_Compress_Zlib-215.0-0 CPAN_Scalar_List_Utils-314.0-0 Dom4j-372.0-0 JakartaCommons-httpclient-1691.0-2 Spiritwave-343.0-0 JakartaCommons-lang-534.0-0 JakartaCommons-logging-2305.1-0 XalanJ-243.0-0 Perl-2624.1-0 JakartaCommons-collections-505.0-0 JakartaCommons-beanutils-636.1-0 concurrent-356.0-0 Zlib-452.0-0 Readline-1406.0-0 Boost-102.0-0 Pcre-418.0-0 MonitoringCore-1949.1-0 Curl-764.1-0 log4j-570.0-0 Ldap-67.0-0 Rendezvous-3520.1-0 ApacheSecurity-498.0-0 CronAdminCommand-2196.0-0 LogDirectoryPermissionResetCommand-1909.0-0 ApolloConfigurationCommand-1692.1-0 ProcessManagerCommands-1449.0-0 libintl-327.0-0 expat-794.0-0 blowfish-423.0-0 SharedColDefs-768.1-0 APLXML-1998.0-2 PubsubJava-724.0-1 JavaCore-2674.0-0 Pubsub-1979.1-0 ProcessManager-3422.1-0 JakartaCommons-codec-2982.0-0 GWT-53.0-1 CPAN_Log_Dispatch-729.0-0 CPAN_Log_Dispatch_File_Rolling-656.0-0 CoralGenericValue-1370.0-0 HttpSigningCpp-866.0-0 ForceRestartProcessManagerCmds-1203.0-0 SpringExpression-1041.0-0 SpringOXM-134.0-0 CoralDThrottleClient-649.0-0 AMPJmsExtension-2562.0-0 OctaneTomcatPlatform-238.0-0 PingServlet-166.0-0 GlassFishJSTL-144.0-0 RunWithCronolog-194.0-0 JavaEE_JSTL-141.0-0 TomcatApolloSetupCommands-291.0-0 OctaneTomcatKerberosSupport-125.0-0 TomcatApolloWebappLoader-256.0-0 ServiceMonitoring-1031.0-0 TSDAgentConfig-1024.0-0 JakartaTomcatFullCleanConfig-288.0-0 OdinLocalClientConfig-1386.0-0 HBAServiceClientConfig-3.0-0 ApacheTomcat6-470.0-0 SpringWeb-222.0-0 JavaEE_JSP-301.0-0 JavaEE_EL-271.0-0 SecureJavaKeystoreGenerator-183.0-0 GuacamoleClientConfig-4.0-0 LbStatusPerlClient-942.0-0 PKeyTool-164.0-0 CPAN_Module_Build-2049.0-0 CPAN_ExtUtils_ParseXS-2046.0-0 CPAN_ExtUtils_CBuilder-2044.0-0 CoralJavaClientDependencies-622.0-0 CoralMetricsBridge-1718.0-0 AuthJava-3729.0-0 RequestRoutingInterfacesJava-3023.0-0 AuthCpp-1853.0-0 SimpleHttpFilters-204.0-0 RendezvousJava-1035.0-0 CoralBeanValue-2370.0-0 AmazonJMSDeprecatedInterfaces-2804.0-0 Jackson-1022.0-0 JavaHttpSpnego-298.0-0 HttpKeytabRetriever-432.0-0 SpringAOP-1701.0-0 SpringBeans-1705.0-0 SpringContext-1742.0-0 SpringCore-1751.0-0 JakartaCommons-logging-api-2420.0-0 AmpJms-3274.0-0 TibcoJms-2669.0-0 UdpJms-3012.0-0 AmqJms-3543.0-0 AmznJms-3354.0-0 AmazonFileAppenderJava-3796.0-0 AmazonApolloEnvironmentInfoJava-3842.0-0 AmazonAppConfigJava-4211.0-0 AmazonTypesJava-3574.0-0 Fbopenssl-518.0-0 AmazonIdJava-3656.0-0 JakartaCommons-logging-adapters-2069.0-0 RequestRoutingInterfaces-916.0-0 Backport-util-concurrent-1958.0-0 ZThread-2076.0-0 AmazonJMS-3498.0-0 Slf4j-1511.0-0 ActiveMQCppBase-2021.0-0 ActiveMQCppClient-5205.0-0 ActiveMQCPPDiscoveryClient-2069.0-0 MessagingDNSLookup-3436.0-0 DirectoryServiceCommon-3044.0-0 ActiveMQJavaClient-6214.0-0 JSmart4JCT-3210.0-0 JSmart-2392.0-0 APLException-2155.0-0 CPAN_Time_Local-2064.0-0 DisableCMFWebsitePushLogsApollo-1334.0-0 JavaCacheAPI-1614.0-0 Cronolog-215.0-0 JavaServiceAvailabilityManager-417.0-0 Rome-121.0-0 CPAN_podlators-1970.0-0 BoostGraph-421.0-0 DozerBeanMapper-23.0-0 AccessControlEncryption-209.0-0 BoostRegex-566.0-0 BoostIostreams-495.0-0 CPAN_SOAP_Lite-2906.0-0 J2ee_jms-2301.0-0 CPAN_MIME_Lite-2192.0-0 CPAN_JSON-1241.0-0 CPAN_PathTools-2077.0-0 MakePerl58Default-2388.0-0 CPAN_XML_XPath-1044.0-0 CPAN_Class_ISA-1753.0-0 BoostThread-693.0-0 BoostFilesystem-727.0-0 CPAN_MIME_tools-2207.0-0 CPAN_IO_Socket_SSL-2162.0-0 CPAN_Pod_Escapes-1752.0-0 AOPAlliance-1078.0-0 AccessPointStats-2798.0-0 CPAN_Carp_Clan-2376.0-0 CPAN_Date_Calc-2333.0-0 CPAN_Bit_Vector-2332.0-0 JakartaTomcatPlatformConfig-426.0-0 CPAN_XML_Parser-2740.0-0 BoostDateTime-528.0-0 J2ee_servlet-2132.0-0 CPAN_Digest_HMAC-2163.0-0 CPAN_Digest_SHA1-2239.0-0 CPAN_FCGI-2167.0-0 Joda-time-1176.0-0 CPAN_IO_stringy-2168.0-0 CPAN_DateTime_Locale-2183.0-0 CPAN_DateTime-2560.0-0 CPAN_Params_Validate-2238.0-0 CPAN_Attribute_Handlers-2156.0-0 CPAN_Class_Singleton-1786.0-0 CPAN_Test_Harness-1522.0-0 CPAN_HTML_Tagset-1867.0-0 CPAN_libwww_perl-2543.0-0 CPAN_Scalar_List_Utils-2171.0-0 Dom4j-2414.0-0 Spiritwave-2290.0-0 JakartaCommons-logging-2616.0-0 XalanJ-991.0-0 JakartaCommons-beanutils-858.0-0 concurrent-2333.0-0 Zlib-2339.0-0 Readline-1916.0-0 Boost-488.0-0 Pcre-639.0-0 Curl-752.0-0 log4j-1650.0-0 Rendezvous-3384.0-0 ApacheSecurity-559.0-0 CronAdminCommand-2525.0-0 LogDirectoryPermissionResetCommand-2197.0-0 ApolloConfigurationCommand-2018.0-0 ProcessManagerCommands-1716.0-0 libintl-1823.0-0 expat-1701.0-0 SharedColDefs-2075.0-0 JavaCore-3776.0-0 Json-org-java-460.0-0 CPAN_MailTools-409.0-0

ArmorSecurity-22.0-0:
    version = 22.0-0
    buildVariant = Generic
    majorVersion = 22
    packageName = ArmorSecurity

ArmorUtils-139.0-0:
    version = 139.0-0
    buildVariant = Generic
    majorVersion = 139
    packageName = ArmorUtils

CapreseServiceJavaTypes-73.0-0:
    version = 73.0-0
    buildVariant = Generic
    majorVersion = 73
    packageName = CapreseServiceJavaTypes

CapreseServiceJavaClient-70.0-0:
    version = 70.0-0
    buildVariant = Generic
    majorVersion = 70
    packageName = CapreseServiceJavaClient

ArmorWebsiteContent-428.0-0:
    version = 428.0-0
    buildVariant = Generic
    majorVersion = 428
    packageName = ArmorWebsiteContent

ArmorWebsiteContent-421.0-0:
    version = 421.0-0
    buildVariant = Generic
    majorVersion = 421
    packageName = ArmorWebsiteContent

CapreseServiceJavaTypes-65.0-0:
    version = 65.0-0
    buildVariant = Generic
    majorVersion = 65
    packageName = CapreseServiceJavaTypes

CapreseServiceJavaClient-63.0-0:
    version = 63.0-0
    buildVariant = Generic
    majorVersion = 63
    packageName = CapreseServiceJavaClient

ArmorWebsiteContent-419.0-0:
    version = 419.0-0
    buildVariant = Generic
    majorVersion = 419
    packageName = ArmorWebsiteContent

Sun-JSR-305-4.1-0:
    version = 4.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4
    minorVersion = 1
    packageName = Sun-JSR-305

AWSAuthRuntimeClientJavaTypes-26.0-0:
    version = 26.0-0
    buildVariant = Generic
    majorVersion = 26
    packageName = AWSAuthRuntimeClientJavaTypes

FuegoConfigServiceJavaClient-67.0-0:
    version = 67.0-0
    buildVariant = Generic
    majorVersion = 67
    packageName = FuegoConfigServiceJavaClient

ArmorSerializerCSV-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = ArmorSerializerCSV

ArmorSerializer-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = ArmorSerializer

ArmorQueryExpressionSDB-22.0-0:
    version = 22.0-0
    buildVariant = Generic
    majorVersion = 22
    packageName = ArmorQueryExpressionSDB

ArmorUtils-103.0-0:
    version = 103.0-0
    buildVariant = Generic
    majorVersion = 103
    packageName = ArmorUtils

ArmorQueryExpression-23.0-0:
    version = 23.0-0
    buildVariant = Generic
    majorVersion = 23
    packageName = ArmorQueryExpression

CapreseServiceJavaTypes-62.0-0:
    version = 62.0-0
    buildVariant = Generic
    majorVersion = 62
    packageName = CapreseServiceJavaTypes

CapreseServiceJavaClient-60.0-0:
    version = 60.0-0
    buildVariant = Generic
    majorVersion = 60
    packageName = CapreseServiceJavaClient

CoralClientNetty-617.0-0:
    version = 617.0-0
    buildVariant = Generic
    majorVersion = 617
    packageName = CoralClientNetty

GoogleGuava-GWT-7.0-0:
    version = 7.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 7
    packageName = GoogleGuava-GWT

SalsaServiceJavaClient-44.0-0:
    version = 44.0-0
    buildVariant = Generic
    majorVersion = 44
    packageName = SalsaServiceJavaClient

SalsaServiceClientConfig-14.0-0:
    version = 14.0-0
    buildVariant = Generic
    majorVersion = 14
    packageName = SalsaServiceClientConfig

IonDatagramConverter-1265.0-0:
    version = 1265.0-0
    buildVariant = Generic
    majorVersion = 1265
    packageName = IonDatagramConverter

WFAWebServiceJavaClient-41.0-0:
    version = 41.0-0
    buildVariant = Generic
    majorVersion = 41
    packageName = WFAWebServiceJavaClient

CoralClientBuilder-1662.0-0:
    version = 1662.0-0
    buildVariant = Generic
    majorVersion = 1662
    packageName = CoralClientBuilder

CoralClientConfig-3540.0-0:
    version = 3540.0-0
    buildVariant = Generic
    majorVersion = 3540
    packageName = CoralClientConfig

TomcatQueryLogger-364.0-0:
    version = 364.0-0
    buildVariant = Generic
    majorVersion = 364
    packageName = TomcatQueryLogger

CoralRpcSupport-3422.0-0:
    version = 3422.0-0
    buildVariant = Generic
    majorVersion = 3422
    packageName = CoralRpcSupport

GoogleGuava-859.0-0:
    version = 859.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 859
    packageName = GoogleGuava

AWSAuthRuntimeClientCore-1047.0-0:
    version = 1047.0-0
    buildVariant = Generic
    majorVersion = 1047
    packageName = AWSAuthRuntimeClientCore

AWSAuthRuntimeClient-1111.0-0:
    version = 1111.0-0
    buildVariant = Generic
    majorVersion = 1111
    packageName = AWSAuthRuntimeClient

JavaAmazonLdapUtils-135.0-0:
    version = 135.0-0
    buildVariant = Generic
    majorVersion = 135
    packageName = JavaAmazonLdapUtils

CoralSpringLauncher-2677.0-0:
    version = 2677.0-0
    buildVariant = Generic
    majorVersion = 2677
    packageName = CoralSpringLauncher

FindBugsAnnotations-1036.0-0:
    version = 1036.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1036
    packageName = FindBugsAnnotations

CoralAwsQuery-2622.0-0:
    version = 2622.0-0
    buildVariant = Generic
    majorVersion = 2622
    packageName = CoralAwsQuery

CoralClientHttp-2696.0-0:
    version = 2696.0-0
    buildVariant = Generic
    majorVersion = 2696
    packageName = CoralClientHttp

Aspen-851.0-0:
    version = 851.0-0
    buildVariant = Generic
    majorVersion = 851
    packageName = Aspen

CoralClient-4517.0-0:
    version = 4517.0-0
    buildVariant = Generic
    majorVersion = 4517
    packageName = CoralClient

CoralAwsAuthentication-1445.0-0:
    version = 1445.0-0
    buildVariant = Generic
    majorVersion = 1445
    packageName = CoralAwsAuthentication

CoralMetricsQuerylogReporter-3052.0-0:
    version = 3052.0-0
    buildVariant = Generic
    majorVersion = 3052
    packageName = CoralMetricsQuerylogReporter

CoralMetricsReporter-4658.0-0:
    version = 4658.0-0
    buildVariant = Generic
    majorVersion = 4658
    packageName = CoralMetricsReporter

Libcap-1913.0-0:
    version = 1913.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1913
    packageName = Libcap

Bzip2-953.0-0:
    version = 953.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 953
    packageName = Bzip2

CPAN_Pod_Simple-2755.0-0:
    version = 2755.0-0
    buildVariant = Generic
    majorVersion = 2755
    packageName = CPAN_Pod_Simple

CPAN_ExtUtils_MakeMaker-2231.0-0:
    version = 2231.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2231
    packageName = CPAN_ExtUtils_MakeMaker

ServiceAvailabilityManager-3317.0-0:
    version = 3317.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3317
    packageName = ServiceAvailabilityManager

AccessPointCore-3135.0-0:
    version = 3135.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3135
    packageName = AccessPointCore

DataPump-2603.0-0:
    version = 2603.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2603
    packageName = DataPump

CPAN_Log_Log4perl-2086.0-0:
    version = 2086.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2086
    packageName = CPAN_Log_Log4perl

CPAN_MIME_Base64-3674.0-0:
    version = 3674.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3674
    packageName = CPAN_MIME_Base64

CPAN_Digest_MD5-3580.0-0:
    version = 3580.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3580
    packageName = CPAN_Digest_MD5

AWSAcctVerificationInterface-1200.0-0:
    version = 1200.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1200
    packageName = AWSAcctVerificationInterface

CPAN_Test-2428.0-0:
    version = 2428.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2428
    packageName = CPAN_Test

CPAN_Digest-3624.0-0:
    version = 3624.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3624
    packageName = CPAN_Digest

CPAN_libnet-2873.0-0:
    version = 2873.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2873
    packageName = CPAN_libnet

CPAN_URI-3545.0-0:
    version = 3545.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3545
    packageName = CPAN_URI

CPAN_HTML_Parser-3474.0-0:
    version = 3474.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3474
    packageName = CPAN_HTML_Parser

CPAN_Test_Simple-2051.0-0:
    version = 2051.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2051
    packageName = CPAN_Test_Simple

CPAN_Compress_Zlib-2885.0-0:
    version = 2885.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2885
    packageName = CPAN_Compress_Zlib

Rosette-2681.0-0:
    version = 2681.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2681
    packageName = Rosette

Openssl-5471.0-0:
    version = 5471.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 5471
    packageName = Openssl

blowfish-2909.0-0:
    version = 2909.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2909
    packageName = blowfish

APLXML-3349.0-0:
    version = 3349.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3349
    packageName = APLXML

APLCore-7753.0-0:
    version = 7753.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 7753
    packageName = APLCore

Pubsub-4853.0-0:
    version = 4853.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4853
    packageName = Pubsub

BSF-6779.0-0:
    version = 6779.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 6779
    packageName = BSF

SalsaServiceJavaClient-42.0-0:
    version = 42.0-0
    buildVariant = Generic
    majorVersion = 42
    packageName = SalsaServiceJavaClient

SalsaServiceJavaClient-41.0-0:
    version = 41.0-0
    buildVariant = Generic
    majorVersion = 41
    packageName = SalsaServiceJavaClient

FuegoConfigServiceJavaClient-62.0-0:
    version = 62.0-0
    buildVariant = Generic
    majorVersion = 62
    packageName = FuegoConfigServiceJavaClient

GFlot-2.0-0:
    version = 2.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2
    packageName = GFlot

CoralLog4jAppender-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = CoralLog4jAppender

ArmorCoralMetrics-17.0-0:
    version = 17.0-0
    buildVariant = Generic
    majorVersion = 17
    packageName = ArmorCoralMetrics

FuegoConfigServiceJavaClient-58.0-0:
    version = 58.0-0
    buildVariant = Generic
    majorVersion = 58
    packageName = FuegoConfigServiceJavaClient

FuegoConfigServiceClientConfig-19.0-0:
    version = 19.0-0
    buildVariant = Generic
    majorVersion = 19
    packageName = FuegoConfigServiceClientConfig

ArmorNotificationTicket-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = ArmorNotificationTicket

ArmorConcurrent-42.0-0:
    version = 42.0-0
    buildVariant = Generic
    majorVersion = 42
    packageName = ArmorConcurrent

ArmorSecurity-6.0-0:
    version = 6.0-0
    buildVariant = Generic
    majorVersion = 6
    packageName = ArmorSecurity

CapreseServiceClientConfig-10.0-0:
    version = 10.0-0
    buildVariant = Generic
    majorVersion = 10
    packageName = CapreseServiceClientConfig

ArmorQueryExpressionSDB-13.0-0:
    version = 13.0-0
    buildVariant = Generic
    majorVersion = 13
    packageName = ArmorQueryExpressionSDB

ArmorUtils-66.0-0:
    version = 66.0-0
    buildVariant = Generic
    majorVersion = 66
    packageName = ArmorUtils

ArmorQueryExpression-12.0-0:
    version = 12.0-0
    buildVariant = Generic
    majorVersion = 12
    packageName = ArmorQueryExpression

ArmorNotification-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = ArmorNotification

PredictionServiceJavaModel-20.0-0:
    version = 20.0-0
    buildVariant = Generic
    majorVersion = 20
    packageName = PredictionServiceJavaModel

CoralClientNetty-596.0-0:
    version = 596.0-0
    buildVariant = Generic
    majorVersion = 596
    packageName = CoralClientNetty

PredictionServiceClientConfig-10.0-0:
    version = 10.0-0
    buildVariant = Generic
    majorVersion = 10
    packageName = PredictionServiceClientConfig

SalsaServiceClientConfig-12.0-0:
    version = 12.0-0
    buildVariant = Generic
    majorVersion = 12
    packageName = SalsaServiceClientConfig

CoralCompressionEngine-866.0-0:
    version = 866.0-0
    buildVariant = Generic
    majorVersion = 866
    packageName = CoralCompressionEngine

CoralAwsWsdlEmitter-437.0-0:
    version = 437.0-0
    buildVariant = Generic
    majorVersion = 437
    packageName = CoralAwsWsdlEmitter

CoralGenericValue-2161.0-0:
    version = 2161.0-0
    buildVariant = Generic
    majorVersion = 2161
    packageName = CoralGenericValue

CoralGenericInterface-1276.0-0:
    version = 1276.0-0
    buildVariant = Generic
    majorVersion = 1276
    packageName = CoralGenericInterface

HBAServiceJavaClientTools-16.0-0:
    version = 16.0-0
    buildVariant = Generic
    majorVersion = 16
    packageName = HBAServiceJavaClientTools

CoralSerializeCore-2314.0-0:
    version = 2314.0-0
    buildVariant = Generic
    majorVersion = 2314
    packageName = CoralSerializeCore

CoralSerialize-2600.0-0:
    version = 2600.0-0
    buildVariant = Generic
    majorVersion = 2600
    packageName = CoralSerialize

CoralGeneric-1425.0-0:
    version = 1425.0-0
    buildVariant = Generic
    majorVersion = 1425
    packageName = CoralGeneric

CoralEnvelopeInterface-1401.0-0:
    version = 1401.0-0
    buildVariant = Generic
    majorVersion = 1401
    packageName = CoralEnvelopeInterface

CoralEnvelopeCore-2702.0-0:
    version = 2702.0-0
    buildVariant = Generic
    majorVersion = 2702
    packageName = CoralEnvelopeCore

OdinLocalRetriever-1000.0-0:
    version = 1000.0-0
    buildVariant = Generic
    majorVersion = 1000
    packageName = OdinLocalRetriever

WFAWebServiceJavaClient-37.0-0:
    version = 37.0-0
    buildVariant = Generic
    majorVersion = 37
    packageName = WFAWebServiceJavaClient

CoralClientBuilder-1493.0-0:
    version = 1493.0-0
    buildVariant = Generic
    majorVersion = 1493
    packageName = CoralClientBuilder

CoralClientConfig-3379.0-0:
    version = 3379.0-0
    buildVariant = Generic
    majorVersion = 3379
    packageName = CoralClientConfig

CoralEnvelope-2979.0-0:
    version = 2979.0-0
    buildVariant = Generic
    majorVersion = 2979
    packageName = CoralEnvelope

CoralDThrottleClient-978.0-0:
    version = 978.0-0
    buildVariant = Generic
    majorVersion = 978
    packageName = CoralDThrottleClient

CoralBuffer-2179.0-0:
    version = 2179.0-0
    buildVariant = Generic
    majorVersion = 2179
    packageName = CoralBuffer

CoralRpcSupport-3389.0-0:
    version = 3389.0-0
    buildVariant = Generic
    majorVersion = 3389
    packageName = CoralRpcSupport

PredictionServiceJavaClient-30.0-0:
    version = 30.0-0
    buildVariant = Generic
    majorVersion = 30
    packageName = PredictionServiceJavaClient

CoralContinuationOrchestrator-3348.0-0:
    version = 3348.0-0
    buildVariant = Generic
    majorVersion = 3348
    packageName = CoralContinuationOrchestrator

LogPullerLib-4886.0-0:
    version = 4886.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4886
    packageName = LogPullerLib

ApacheTomcat6-622.0-0:
    version = 622.0-0
    buildVariant = Generic
    majorVersion = 622
    packageName = ApacheTomcat6

HBAServiceJavaClient-19.0-0:
    version = 19.0-0
    buildVariant = Generic
    majorVersion = 19
    packageName = HBAServiceJavaClient

CoralJsonSupport-2848.0-0:
    version = 2848.0-0
    buildVariant = Generic
    majorVersion = 2848
    packageName = CoralJsonSupport

GuacamoleJavaClientTools-68.0-0:
    version = 68.0-0
    buildVariant = Generic
    majorVersion = 68
    packageName = GuacamoleJavaClientTools

GuacamoleJavaClient-40.0-0:
    version = 40.0-0
    buildVariant = Generic
    majorVersion = 40
    packageName = GuacamoleJavaClient

CoralCacheValue-2990.0-0:
    version = 2990.0-0
    buildVariant = Generic
    majorVersion = 2990
    packageName = CoralCacheValue

CoralAwsSupport-255.0-0:
    version = 255.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 255
    packageName = CoralAwsSupport

CoralValidate-2955.0-0:
    version = 2955.0-0
    buildVariant = Generic
    majorVersion = 2955
    packageName = CoralValidate

CoralSecurity-1859.0-0:
    version = 1859.0-0
    buildVariant = Generic
    majorVersion = 1859
    packageName = CoralSecurity

CoralAvailability-2258.0-0:
    version = 2258.0-0
    buildVariant = Generic
    majorVersion = 2258
    packageName = CoralAvailability

CoralSpringLauncher-2603.0-0:
    version = 2603.0-0
    buildVariant = Generic
    majorVersion = 2603
    packageName = CoralSpringLauncher

CoralAwsQuery-2543.0-0:
    version = 2543.0-0
    buildVariant = Generic
    majorVersion = 2543
    packageName = CoralAwsQuery

CoralClientHttp-2665.0-0:
    version = 2665.0-0
    buildVariant = Generic
    majorVersion = 2665
    packageName = CoralClientHttp

CoralAwsMetering-501.0-0:
    version = 501.0-0
    buildVariant = Generic
    majorVersion = 501
    packageName = CoralAwsMetering

CoralJavaClientDependencies-1288.0-0:
    version = 1288.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1288
    packageName = CoralJavaClientDependencies

CoralClient-4384.0-0:
    version = 4384.0-0
    buildVariant = Generic
    majorVersion = 4384
    packageName = CoralClient

CoralAwsAuthentication-1408.0-0:
    version = 1408.0-0
    buildVariant = Generic
    majorVersion = 1408
    packageName = CoralAwsAuthentication

CoralAwsSoap-655.0-0:
    version = 655.0-0
    buildVariant = Generic
    majorVersion = 655
    packageName = CoralAwsSoap

CoralMetricsBridge-2404.0-0:
    version = 2404.0-0
    buildVariant = Generic
    majorVersion = 2404
    packageName = CoralMetricsBridge

CoralUtil-3873.0-0:
    version = 3873.0-0
    buildVariant = Generic
    majorVersion = 3873
    packageName = CoralUtil

CoralSpring-2510.0-0:
    version = 2510.0-0
    buildVariant = Generic
    majorVersion = 2510
    packageName = CoralSpring

MakeLatestJDKDefault-858.0-0:
    version = 858.0-0
    buildVariant = Generic
    majorVersion = 858
    packageName = MakeLatestJDKDefault

BarkPerlLib-4225.0-0:
    version = 4225.0-0
    buildVariant = Generic
    majorVersion = 4225
    packageName = BarkPerlLib

CoralTransmute-3886.0-0:
    version = 3886.0-0
    buildVariant = Generic
    majorVersion = 3886
    packageName = CoralTransmute

CoralTibcoValue-2122.0-0:
    version = 2122.0-0
    buildVariant = Generic
    majorVersion = 2122
    packageName = CoralTibcoValue

CoralQueryStringValue-1899.0-0:
    version = 1899.0-0
    buildVariant = Generic
    majorVersion = 1899
    packageName = CoralQueryStringValue

CoralModel-2759.0-0:
    version = 2759.0-0
    buildVariant = Generic
    majorVersion = 2759
    packageName = CoralModel

CoralBeanValue-3256.0-0:
    version = 3256.0-0
    buildVariant = Generic
    majorVersion = 3256
    packageName = CoralBeanValue

CoralOrchestrator-4425.0-0:
    version = 4425.0-0
    buildVariant = Generic
    majorVersion = 4425
    packageName = CoralOrchestrator

CoralService-4930.0-0:
    version = 4930.0-0
    buildVariant = Generic
    majorVersion = 4930
    packageName = CoralService

AmazonCACerts-1078.0-0:
    version = 1078.0-0
    buildVariant = Generic
    majorVersion = 1078
    packageName = AmazonCACerts

ZeroTouchVipConfig-1727.0-0:
    version = 1727.0-0
    buildVariant = Generic
    majorVersion = 1727
    packageName = ZeroTouchVipConfig

JDK-4344.0-0:
    version = 4344.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4344
    packageName = JDK

CPAN_XML_XPath-1583.0-0:
    version = 1583.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1583
    packageName = CPAN_XML_XPath

CPAN_XML_Parser-3764.0-0:
    version = 3764.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3764
    packageName = CPAN_XML_Parser

CPAN_Digest_HMAC-3127.0-0:
    version = 3127.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3127
    packageName = CPAN_Digest_HMAC

AWSAcctVerificationInterface-1139.0-0:
    version = 1139.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1139
    packageName = AWSAcctVerificationInterface

CPAN_FCGI-3121.0-0:
    version = 3121.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3121
    packageName = CPAN_FCGI

CPAN_libwww_perl-3462.0-0:
    version = 3462.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3462
    packageName = CPAN_libwww_perl

Perl-2797.0-0:
    version = 2797.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2797
    packageName = Perl

MonitoringCore-2344.0-0:
    version = 2344.0-0
    buildVariant = Generic
    majorVersion = 2344
    packageName = MonitoringCore

Rosette-2606.0-0:
    version = 2606.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2606
    packageName = Rosette

AmazonCACerts-905.0-0:
    version = 905.0-0
    buildVariant = Generic
    majorVersion = 905
    packageName = AmazonCACerts

AWSAuthRuntimeClientJavaTypes-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = AWSAuthRuntimeClientJavaTypes

CoralSerialize-2580.0-0:
    version = 2580.0-0
    buildVariant = Generic
    majorVersion = 2580
    packageName = CoralSerialize

CoralClientBuilder-1467.0-0:
    version = 1467.0-0
    buildVariant = Generic
    majorVersion = 1467
    packageName = CoralClientBuilder

CoralGrammar-1942.0-0:
    version = 1942.0-0
    buildVariant = Generic
    majorVersion = 1942
    packageName = CoralGrammar

CoralConfig-2301.0-0:
    version = 2301.0-0
    buildVariant = Generic
    majorVersion = 2301
    packageName = CoralConfig

CoralClientConfig-3336.0-0:
    version = 3336.0-0
    buildVariant = Generic
    majorVersion = 3336
    packageName = CoralClientConfig

CoralEnvelope-2955.0-0:
    version = 2955.0-0
    buildVariant = Generic
    majorVersion = 2955
    packageName = CoralEnvelope

CoralRpcSupport-3206.0-0:
    version = 3206.0-0
    buildVariant = Generic
    majorVersion = 3206
    packageName = CoralRpcSupport

AWSAuthRuntimeClientCore-967.0-0:
    version = 967.0-0
    buildVariant = Generic
    majorVersion = 967
    packageName = AWSAuthRuntimeClientCore

AWSAuthRuntimeClient-1006.0-0:
    version = 1006.0-0
    buildVariant = Generic
    majorVersion = 1006
    packageName = AWSAuthRuntimeClient

CoralContinuationOrchestrator-3301.0-0:
    version = 3301.0-0
    buildVariant = Generic
    majorVersion = 3301
    packageName = CoralContinuationOrchestrator

CoralJsonSupport-2840.0-0:
    version = 2840.0-0
    buildVariant = Generic
    majorVersion = 2840
    packageName = CoralJsonSupport

CoralCacheValue-2964.0-0:
    version = 2964.0-0
    buildVariant = Generic
    majorVersion = 2964
    packageName = CoralCacheValue

CoralValidate-2935.0-0:
    version = 2935.0-0
    buildVariant = Generic
    majorVersion = 2935
    packageName = CoralValidate

CoralAwsQuery-2534.0-0:
    version = 2534.0-0
    buildVariant = Generic
    majorVersion = 2534
    packageName = CoralAwsQuery

CoralClientHttp-2486.0-0:
    version = 2486.0-0
    buildVariant = Generic
    majorVersion = 2486
    packageName = CoralClientHttp

CoralClient-4288.0-0:
    version = 4288.0-0
    buildVariant = Generic
    majorVersion = 4288
    packageName = CoralClient

CoralAwsAuthentication-1332.0-0:
    version = 1332.0-0
    buildVariant = Generic
    majorVersion = 1332
    packageName = CoralAwsAuthentication

CoralAwsSoap-641.0-0:
    version = 641.0-0
    buildVariant = Generic
    majorVersion = 641
    packageName = CoralAwsSoap

BasicFluxoJavaClient-412.0-0:
    version = 412.0-0
    buildVariant = Generic
    majorVersion = 412
    packageName = BasicFluxoJavaClient

MinimalProcessManager-1594.0-0:
    version = 1594.0-0
    buildVariant = Generic
    majorVersion = 1594
    packageName = MinimalProcessManager

CoralAnnotation-1658.0-0:
    version = 1658.0-0
    buildVariant = Generic
    majorVersion = 1658
    packageName = CoralAnnotation

CoralXmlValue-2479.0-0:
    version = 2479.0-0
    buildVariant = Generic
    majorVersion = 2479
    packageName = CoralXmlValue

CoralTransmute-3802.0-0:
    version = 3802.0-0
    buildVariant = Generic
    majorVersion = 3802
    packageName = CoralTransmute

CoralModel-2661.0-0:
    version = 2661.0-0
    buildVariant = Generic
    majorVersion = 2661
    packageName = CoralModel

CoralJsonValue-2000.0-0:
    version = 2000.0-0
    buildVariant = Generic
    majorVersion = 2000
    packageName = CoralJsonValue

CoralOrchestrator-4335.0-0:
    version = 4335.0-0
    buildVariant = Generic
    majorVersion = 4335
    packageName = CoralOrchestrator

CoralService-4799.0-0:
    version = 4799.0-0
    buildVariant = Generic
    majorVersion = 4799
    packageName = CoralService

ZThread-3111.0-0:
    version = 3111.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3111
    packageName = ZThread

BSFJavaClient-7211.0-0:
    version = 7211.0-0
    buildVariant = Generic
    majorVersion = 7211
    packageName = BSFJavaClient

JSmart4JCT-4172.0-0:
    version = 4172.0-0
    buildVariant = Generic
    majorVersion = 4172
    packageName = JSmart4JCT

Bait-4419.0-0:
    version = 4419.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4419
    packageName = Bait

CPAN_Digest_SHA1-3190.0-0:
    version = 3190.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3190
    packageName = CPAN_Digest_SHA1

AWSAcctVerificationInterface-1110.0-0:
    version = 1110.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1110
    packageName = AWSAcctVerificationInterface

CPAN_IO_stringy-3097.0-0:
    version = 3097.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3097
    packageName = CPAN_IO_stringy

CPAN_DateTime_TimeZone-2870.0-0:
    version = 2870.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2870
    packageName = CPAN_DateTime_TimeZone

JakartaCommons-httpclient-4208.0-0:
    version = 4208.0-0
    buildVariant = Generic
    majorVersion = 4208
    packageName = JakartaCommons-httpclient

Rendezvous-4177.0-0:
    version = 4177.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4177
    packageName = Rendezvous

ArmorSerializerCSV-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = ArmorSerializerCSV

ArmorSerializer-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = ArmorSerializer

OpenCsv-263.0-0:
    version = 263.0-0
    buildVariant = Generic
    majorVersion = 263
    packageName = OpenCsv

CoralMetricsReporter-4365.0-0:
    version = 4365.0-0
    buildVariant = Generic
    majorVersion = 4365
    packageName = CoralMetricsReporter

ArmorNotificationTicket-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = ArmorNotificationTicket

FluxoJavaClient-411.0-0:
    version = 411.0-0
    buildVariant = Generic
    majorVersion = 411
    packageName = FluxoJavaClient

ArmorNotification-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = ArmorNotification

BasicFluxoJavaClient-362.1-0:
    version = 362.1-0
    buildVariant = Generic
    majorVersion = 362
    minorVersion = 1
    packageName = BasicFluxoJavaClient

AWSAuthRuntimeClientJavaTypes-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = AWSAuthRuntimeClientJavaTypes

FuegoConfigServiceClientConfig-14.0-0:
    version = 14.0-0
    buildVariant = Generic
    majorVersion = 14
    packageName = FuegoConfigServiceClientConfig

ArmorConcurrent-20.0-0:
    version = 20.0-0
    buildVariant = Generic
    majorVersion = 20
    packageName = ArmorConcurrent

CapreseServiceClientConfig-9.0-0:
    version = 9.0-0
    buildVariant = Generic
    majorVersion = 9
    packageName = CapreseServiceClientConfig

CoralClientNetty-355.0-0:
    version = 355.0-0
    buildVariant = Generic
    majorVersion = 355
    packageName = CoralClientNetty

PredictionServiceClientConfig-7.0-0:
    version = 7.0-0
    buildVariant = Generic
    majorVersion = 7
    packageName = PredictionServiceClientConfig

SalsaServiceClientConfig-9.0-0:
    version = 9.0-0
    buildVariant = Generic
    majorVersion = 9
    packageName = SalsaServiceClientConfig

CRuntime-331.0-0:
    version = 331.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 331
    packageName = CRuntime

CoralSchema-1205.0-0:
    version = 1205.0-0
    buildVariant = Generic
    majorVersion = 1205
    packageName = CoralSchema

HBAServiceJavaClientTools-15.0-0:
    version = 15.0-0
    buildVariant = Generic
    majorVersion = 15
    packageName = HBAServiceJavaClientTools

CoralSerializeCore-2238.0-0:
    version = 2238.0-0
    buildVariant = Generic
    majorVersion = 2238
    packageName = CoralSerializeCore

CoralSerialize-2501.0-0:
    version = 2501.0-0
    buildVariant = Generic
    majorVersion = 2501
    packageName = CoralSerialize

WFAWebServiceJavaClient-30.0-1:
    version = 30.0-1
    buildVariant = Generic
    majorVersion = 30
    packageName = WFAWebServiceJavaClient
    patchVersion = 1

CoralGrammar-1736.0-0:
    version = 1736.0-0
    buildVariant = Generic
    majorVersion = 1736
    packageName = CoralGrammar

CoralConfig-2218.0-0:
    version = 2218.0-0
    buildVariant = Generic
    majorVersion = 2218
    packageName = CoralConfig

AuthJavaHTTP-502.0-0:
    version = 502.0-0
    buildVariant = Generic
    majorVersion = 502
    packageName = AuthJavaHTTP

SpringExpression-1361.0-0:
    version = 1361.0-0
    buildVariant = Generic
    majorVersion = 1361
    packageName = SpringExpression

SpringOXM-179.0-0:
    version = 179.0-0
    buildVariant = Generic
    majorVersion = 179
    packageName = SpringOXM

CoralBuffer-2044.0-0:
    version = 2044.0-0
    buildVariant = Generic
    majorVersion = 2044
    packageName = CoralBuffer

RunWithCronolog-285.0-0:
    version = 285.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 285
    packageName = RunWithCronolog

TomcatApolloScripts-467.0-0:
    version = 467.0-0
    buildVariant = Generic
    majorVersion = 467
    packageName = TomcatApolloScripts

OctaneTomcatKerberosSupport-169.0-0:
    version = 169.0-0
    buildVariant = Generic
    majorVersion = 169
    packageName = OctaneTomcatKerberosSupport

TomcatApolloWebappLoader-353.0-0:
    version = 353.0-0
    buildVariant = Generic
    majorVersion = 353
    packageName = TomcatApolloWebappLoader

GoogleGuava-554.0-0:
    version = 554.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 554
    packageName = GoogleGuava

ServiceMonitoringTSD-1766.1-0:
    version = 1766.1-0
    buildVariant = Generic
    majorVersion = 1766
    minorVersion = 1
    packageName = ServiceMonitoringTSD

AWSAuthRuntimeClientCore-944.0-0:
    version = 944.0-0
    buildVariant = Generic
    majorVersion = 944
    packageName = AWSAuthRuntimeClientCore

AWSAuthRuntimeClient-981.0-0:
    version = 981.0-0
    buildVariant = Generic
    majorVersion = 981
    packageName = AWSAuthRuntimeClient

JavaAmazonLdapUtils-123.0-0:
    version = 123.0-0
    buildVariant = Generic
    majorVersion = 123
    packageName = JavaAmazonLdapUtils

JakartaTomcatFullCleanConfig-396.0-0:
    version = 396.0-0
    buildVariant = Generic
    majorVersion = 396
    packageName = JakartaTomcatFullCleanConfig

OdinLocalClientConfig-1896.0-0:
    version = 1896.0-0
    buildVariant = Generic
    majorVersion = 1896
    packageName = OdinLocalClientConfig

LogPullerLib-4587.1-0:
    version = 4587.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4587
    minorVersion = 1
    packageName = LogPullerLib

SpringWeb-282.0-0:
    version = 282.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 282
    packageName = SpringWeb

CoralJsonSupport-2539.0-0:
    version = 2539.0-0
    buildVariant = Generic
    majorVersion = 2539
    packageName = CoralJsonSupport

SecureJavaKeystoreGenerator-264.0-0:
    version = 264.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 264
    packageName = SecureJavaKeystoreGenerator

GuacamoleJavaClientTools-67.0-0:
    version = 67.0-0
    buildVariant = Generic
    majorVersion = 67
    packageName = GuacamoleJavaClientTools

GuacamoleClientConfig-5.0-0:
    version = 5.0-0
    buildVariant = Generic
    majorVersion = 5
    packageName = GuacamoleClientConfig

CoralAwsSupport-214.0-0:
    version = 214.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 214
    packageName = CoralAwsSupport

CoralAvailability-2129.0-0:
    version = 2129.0-0
    buildVariant = Generic
    majorVersion = 2129
    packageName = CoralAvailability

CoralSpringLauncher-2464.0-0:
    version = 2464.0-0
    buildVariant = Generic
    majorVersion = 2464
    packageName = CoralSpringLauncher

CoralAwsMetering-490.0-0:
    version = 490.0-0
    buildVariant = Generic
    majorVersion = 490
    packageName = CoralAwsMetering

Aspen-778.0-0:
    version = 778.0-0
    buildVariant = Generic
    majorVersion = 778
    packageName = Aspen

CoralAwsSoap-607.0-0:
    version = 607.0-0
    buildVariant = Generic
    majorVersion = 607
    packageName = CoralAwsSoap

CoralUtil-3704.0-0:
    version = 3704.0-0
    buildVariant = Generic
    majorVersion = 3704
    packageName = CoralUtil

AuthJava-4341.0-0:
    version = 4341.0-0
    buildVariant = Generic
    majorVersion = 4341
    packageName = AuthJava

MinimalProcessManager-1567.0-0:
    version = 1567.0-0
    buildVariant = Generic
    majorVersion = 1567
    packageName = MinimalProcessManager

CoralActivity-2663.0-0:
    version = 2663.0-0
    buildVariant = Generic
    majorVersion = 2663
    packageName = CoralActivity

CoralSpring-2348.0-0:
    version = 2348.0-0
    buildVariant = Generic
    majorVersion = 2348
    packageName = CoralSpring

BarkPerlLib-4026.0-0:
    version = 4026.0-0
    buildVariant = Generic
    majorVersion = 4026
    packageName = BarkPerlLib

ApacheXMLSecurityJava-494.1-0:
    version = 494.1-0
    buildVariant = Generic
    majorVersion = 494
    minorVersion = 1
    packageName = ApacheXMLSecurityJava

CoralValue-1521.0-0:
    version = 1521.0-0
    buildVariant = Generic
    majorVersion = 1521
    packageName = CoralValue

CoralTransmute-3652.0-0:
    version = 3652.0-0
    buildVariant = Generic
    majorVersion = 3652
    packageName = CoralTransmute

CoralTibcoValue-1902.0-0:
    version = 1902.0-0
    buildVariant = Generic
    majorVersion = 1902
    packageName = CoralTibcoValue

CoralQueryStringValue-1808.0-0:
    version = 1808.0-0
    buildVariant = Generic
    majorVersion = 1808
    packageName = CoralQueryStringValue

CoralModel-2446.0-0:
    version = 2446.0-0
    buildVariant = Generic
    majorVersion = 2446
    packageName = CoralModel

Sun-JSR-275-3258.0-0:
    version = 3258.0-0
    buildVariant = Generic
    majorVersion = 3258
    packageName = Sun-JSR-275

CoralOrchestrator-4070.0-0:
    version = 4070.0-0
    buildVariant = Generic
    majorVersion = 4070
    packageName = CoralOrchestrator

CoralMetricsQuerylogReporter-2815.0-0:
    version = 2815.0-0
    buildVariant = Generic
    majorVersion = 2815
    packageName = CoralMetricsQuerylogReporter

CoralService-4677.0-0:
    version = 4677.0-0
    buildVariant = Generic
    majorVersion = 4677
    packageName = CoralService

CoralMetricsReporter-4285.0-0:
    version = 4285.0-0
    buildVariant = Generic
    majorVersion = 4285
    packageName = CoralMetricsReporter

SpringAOP-2109.0-0:
    version = 2109.0-0
    buildVariant = Generic
    majorVersion = 2109
    packageName = SpringAOP

SpringBeans-2160.1-0:
    version = 2160.1-0
    buildVariant = Generic
    majorVersion = 2160
    minorVersion = 1
    packageName = SpringBeans

SpringContext-2152.0-0:
    version = 2152.0-0
    buildVariant = Generic
    majorVersion = 2152
    packageName = SpringContext

SpringCore-2169.0-0:
    version = 2169.0-0
    buildVariant = Generic
    majorVersion = 2169
    packageName = SpringCore

Slf4j_log4j-780.1-0:
    version = 780.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 780
    minorVersion = 1
    packageName = Slf4j_log4j

AmazonProfilerJava-4326.0-0:
    version = 4326.0-0
    buildVariant = Generic
    majorVersion = 4326
    packageName = AmazonProfilerJava

IonJava-2593.0-0:
    version = 2593.0-0
    buildVariant = Generic
    majorVersion = 2593
    packageName = IonJava

AmazonAppConfigJava-4942.0-0:
    version = 4942.0-0
    buildVariant = Generic
    majorVersion = 4942
    packageName = AmazonAppConfigJava

AmazonTypesJava-4554.0-0:
    version = 4554.0-0
    buildVariant = Generic
    majorVersion = 4554
    packageName = AmazonTypesJava

AWSAccessPolicyInterface-604.0-0:
    version = 604.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 604
    packageName = AWSAccessPolicyInterface

BSFJavaClient-7093.0-0:
    version = 7093.0-0
    buildVariant = Generic
    majorVersion = 7093
    packageName = BSFJavaClient

ZeroTouchVipConfig-1630.0-0:
    version = 1630.0-0
    buildVariant = Generic
    majorVersion = 1630
    packageName = ZeroTouchVipConfig

Slf4j-1901.0-0:
    version = 1901.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1901
    packageName = Slf4j

ActiveMQCppBase-2585.0-0:
    version = 2585.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2585
    packageName = ActiveMQCppBase

ActiveMQCPPDiscoveryClient-2635.0-0:
    version = 2635.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2635
    packageName = ActiveMQCPPDiscoveryClient

WebsiteLogPusher-4800.1-0:
    version = 4800.1-0
    buildVariant = Generic
    majorVersion = 4800
    minorVersion = 1
    packageName = WebsiteLogPusher

APLException-2757.0-0:
    version = 2757.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2757
    packageName = APLException

DisableCMFWebsitePushLogsApollo-1969.0-0:
    version = 1969.0-0
    buildVariant = Generic
    majorVersion = 1969
    packageName = DisableCMFWebsitePushLogsApollo

MechNormServiceInterface-106.0-0:
    version = 106.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 106
    packageName = MechNormServiceInterface

Cronolog-327.0-0:
    version = 327.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 327
    packageName = Cronolog

JCTServiceAvailabilityManager-584.0-0:
    version = 584.0-0
    buildVariant = Generic
    majorVersion = 584
    packageName = JCTServiceAvailabilityManager

JDK-4229.0-0:
    version = 4229.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4229
    packageName = JDK

CPAN_podlators-2528.0-0:
    version = 2528.0-0
    buildVariant = Generic
    majorVersion = 2528
    packageName = CPAN_podlators

AWSSecurityTokenInterface-818.0-0:
    version = 818.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 818
    packageName = AWSSecurityTokenInterface

TranslateToTsd-3452.0-0:
    version = 3452.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3452
    packageName = TranslateToTsd

CPAN_SOAP_Lite-3716.0-0:
    version = 3716.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3716
    packageName = CPAN_SOAP_Lite

BoostHeaders-1753.0-0:
    version = 1753.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1753
    packageName = BoostHeaders

LibEvent-2496.0-0:
    version = 2496.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2496
    packageName = LibEvent

CPAN_mod_perl-2727.0-0:
    version = 2727.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2727
    packageName = CPAN_mod_perl

CPAN_Net_SSLeay_pm-2830.0-0:
    version = 2830.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2830
    packageName = CPAN_Net_SSLeay_pm

CPAN_Crypt_SSLeay-2993.0-0:
    version = 2993.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2993
    packageName = CPAN_Crypt_SSLeay

IquitosClient-3152.0-0:
    version = 3152.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3152
    packageName = IquitosClient

FastcgiClient-2648.0-0:
    version = 2648.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2648
    packageName = FastcgiClient

AWSPlatformCommon-519.0-0:
    version = 519.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 519
    packageName = AWSPlatformCommon

TimeSeriesDataLibrary-2124.0-0:
    version = 2124.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2124
    packageName = TimeSeriesDataLibrary

CPAN_MIME_tools-3129.0-0:
    version = 3129.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3129
    packageName = CPAN_MIME_tools

InfAutoLbcsPerlClient-1430.0-1:
    version = 1430.0-1
    buildVariant = Generic
    majorVersion = 1430
    packageName = InfAutoLbcsPerlClient
    patchVersion = 1

CPAN_MailTools-1277.0-0:
    version = 1277.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1277
    packageName = CPAN_MailTools

JakartaTomcatPlatformConfig-565.0-0:
    version = 565.0-0
    buildVariant = Generic
    majorVersion = 565
    packageName = JakartaTomcatPlatformConfig

Bait-4290.0-0:
    version = 4290.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4290
    packageName = Bait

ICU4C-985.0-0:
    version = 985.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 985
    packageName = ICU4C

CPAN_XML_Simple-1905.1-0:
    version = 1905.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1905
    minorVersion = 1
    packageName = CPAN_XML_Simple

JakartaCommons-codec-3237.0-0:
    version = 3237.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3237
    packageName = JakartaCommons-codec

CPAN_Class_Singleton-2344.0-0:
    version = 2344.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2344
    packageName = CPAN_Class_Singleton

JakartaCommons-logging-3313.0-0:
    version = 3313.0-0
    buildVariant = Generic
    majorVersion = 3313
    packageName = JakartaCommons-logging

JakartaCommons-collections-2816.1-0:
    version = 2816.1-0
    buildVariant = Generic
    majorVersion = 2816
    minorVersion = 1
    packageName = JakartaCommons-collections

Pcre-1047.1-0:
    version = 1047.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1047
    minorVersion = 1
    packageName = Pcre

Curl-1090.0-0:
    version = 1090.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1090
    packageName = Curl

Ldap-584.0-0:
    version = 584.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 584
    packageName = Ldap

Openssl-5206.0-0:
    version = 5206.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 5206
    packageName = Openssl

ApacheSecurity-838.0-0:
    version = 838.0-0
    buildVariant = Generic
    majorVersion = 838
    packageName = ApacheSecurity

CronAdminCommand-3153.0-0:
    version = 3153.0-0
    buildVariant = Generic
    majorVersion = 3153
    packageName = CronAdminCommand

LogDirectoryPermissionResetCommand-2752.0-0:
    version = 2752.0-0
    buildVariant = Generic
    majorVersion = 2752
    packageName = LogDirectoryPermissionResetCommand

HTTPClient-3083.0-0:
    version = 3083.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3083
    packageName = HTTPClient

APLCore-7527.0-0:
    version = 7527.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 7527
    packageName = APLCore

BSF-6164.0-0:
    version = 6164.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 6164
    packageName = BSF

FuegoConfigServiceClientConfig-6.0-0:
    version = 6.0-0
    buildVariant = Generic
    majorVersion = 6
    packageName = FuegoConfigServiceClientConfig

ArmorConcurrent-6.0-0:
    version = 6.0-0
    buildVariant = Generic
    majorVersion = 6
    packageName = ArmorConcurrent

CapreseServiceClientConfig-4.0-0:
    version = 4.0-0
    buildVariant = Generic
    majorVersion = 4
    packageName = CapreseServiceClientConfig

PredictionServiceJavaModel-11.0-0:
    version = 11.0-0
    buildVariant = Generic
    majorVersion = 11
    packageName = PredictionServiceJavaModel

CRuntime-196.0-0:
    version = 196.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 196
    packageName = CRuntime

OctaneTomcatPlatform-319.0-0:
    version = 319.0-0
    buildVariant = Generic
    majorVersion = 319
    packageName = OctaneTomcatPlatform

TomcatApolloSetupCommands-392.0-1:
    version = 392.0-1
    buildVariant = Generic
    majorVersion = 392
    packageName = TomcatApolloSetupCommands
    patchVersion = 1

TomcatApolloScripts-430.0-1:
    version = 430.0-1
    buildVariant = Generic
    majorVersion = 430
    packageName = TomcatApolloScripts
    patchVersion = 1

PredictionServiceJavaClient-24.0-0:
    version = 24.0-0
    buildVariant = Generic
    majorVersion = 24
    packageName = PredictionServiceJavaClient

LbStatusPerlClient-1242.0-0:
    version = 1242.0-0
    buildVariant = Generic
    majorVersion = 1242
    packageName = LbStatusPerlClient

BarkPerlLib-3935.0-0:
    version = 3935.0-0
    buildVariant = Generic
    majorVersion = 3935
    packageName = BarkPerlLib

FluxoPerlClient-1381.0-0:
    version = 1381.0-0
    buildVariant = Generic
    majorVersion = 1381
    packageName = FluxoPerlClient

CoralMetrics-3697.0-0:
    version = 3697.0-0
    buildVariant = Generic
    majorVersion = 3697
    packageName = CoralMetrics

ZeroTouchVipConfig-1503.1-0:
    version = 1503.1-0
    buildVariant = Generic
    majorVersion = 1503
    minorVersion = 1
    packageName = ZeroTouchVipConfig

Json-org-java-1320.0-0:
    version = 1320.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1320
    packageName = Json-org-java

JDK-4164.0-0:
    version = 4164.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4164
    packageName = JDK

TranslateToTsd-3349.0-0:
    version = 3349.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3349
    packageName = TranslateToTsd

CPAN_List_MoreUtils-2575.0-0:
    version = 2575.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2575
    packageName = CPAN_List_MoreUtils

TimeSeriesDataLibrary-2088.0-0:
    version = 2088.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2088
    packageName = TimeSeriesDataLibrary

InfAutoLbcsPerlClient-1384.0-0:
    version = 1384.0-0
    buildVariant = Generic
    majorVersion = 1384
    packageName = InfAutoLbcsPerlClient

Bait-4223.0-0:
    version = 4223.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4223
    packageName = Bait

APLCore-7296.0-0:
    version = 7296.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 7296
    packageName = APLCore

CRuntime-4.0-0:
    version = 4.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4
    packageName = CRuntime

PredictionServiceJavaClient-22.0-0:
    version = 22.0-0
    buildVariant = Generic
    majorVersion = 22
    packageName = PredictionServiceJavaClient

PredictionServiceClientConfig-2.1-1:
    version = 2.1-1
    buildVariant = Generic
    majorVersion = 2
    minorVersion = 1
    packageName = PredictionServiceClientConfig
    patchVersion = 1

PredictionServiceJavaModel-8.0-0:
    version = 8.0-0
    buildVariant = Generic
    majorVersion = 8
    packageName = PredictionServiceJavaModel

IonDatagramConverter-880.0-0:
    version = 880.0-0
    buildVariant = Generic
    majorVersion = 880
    packageName = IonDatagramConverter

HBAServiceJavaClientTools-14.0-0:
    version = 14.0-0
    buildVariant = Generic
    majorVersion = 14
    packageName = HBAServiceJavaClientTools

CoralEnvelopeCore-2332.0-0:
    version = 2332.0-0
    buildVariant = Generic
    majorVersion = 2332
    packageName = CoralEnvelopeCore

OdinLocalRetriever-716.0-0:
    version = 716.0-0
    buildVariant = Generic
    majorVersion = 716
    packageName = OdinLocalRetriever

CoralConfig-1994.0-0:
    version = 1994.0-0
    buildVariant = Generic
    majorVersion = 1994
    packageName = CoralConfig

LiveWatchJvmPublisherWebapp-212.0-1:
    version = 212.0-1
    buildVariant = Generic
    majorVersion = 212
    packageName = LiveWatchJvmPublisherWebapp
    patchVersion = 1

LiveWatchJvmPublisherServlet-219.0-1:
    version = 219.0-1
    buildVariant = Generic
    majorVersion = 219
    packageName = LiveWatchJvmPublisherServlet
    patchVersion = 1

TomcatQueryLogger-304.0-0:
    version = 304.0-0
    buildVariant = Generic
    majorVersion = 304
    packageName = TomcatQueryLogger

ServiceMonitoringTSD-1710.1-0:
    version = 1710.1-0
    buildVariant = Generic
    majorVersion = 1710
    minorVersion = 1
    packageName = ServiceMonitoringTSD

LiveWatchJvmPublisher-889.0-1:
    version = 889.0-1
    buildVariant = Generic
    majorVersion = 889
    packageName = LiveWatchJvmPublisher
    patchVersion = 1

CoralContinuationOrchestrator-2936.0-0:
    version = 2936.0-0
    buildVariant = Generic
    majorVersion = 2936
    packageName = CoralContinuationOrchestrator

LogPullerLib-4482.0-0:
    version = 4482.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4482
    packageName = LogPullerLib

GuacamoleJavaClientTools-64.0-0:
    version = 64.0-0
    buildVariant = Generic
    majorVersion = 64
    packageName = GuacamoleJavaClientTools

CoralAwsSupport-206.0-0:
    version = 206.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 206
    packageName = CoralAwsSupport

GWT-65.0-0:
    version = 65.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 65
    packageName = GWT

CoralAvailability-2072.0-0:
    version = 2072.0-0
    buildVariant = Generic
    majorVersion = 2072
    packageName = CoralAvailability

Aspen-728.0-0:
    version = 728.0-0
    buildVariant = Generic
    majorVersion = 728
    packageName = Aspen

CoralUtil-3449.0-0:
    version = 3449.0-0
    buildVariant = Generic
    majorVersion = 3449
    packageName = CoralUtil

CoralActivity-2600.0-0:
    version = 2600.0-0
    buildVariant = Generic
    majorVersion = 2600
    packageName = CoralActivity

CoralSpring-2180.0-0:
    version = 2180.0-0
    buildVariant = Generic
    majorVersion = 2180
    packageName = CoralSpring

RequestRoutingInterfacesJava-3578.0-0:
    version = 3578.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3578
    packageName = RequestRoutingInterfacesJava

CoralReflect-2357.0-0:
    version = 2357.0-0
    buildVariant = Generic
    majorVersion = 2357
    packageName = CoralReflect

MakeLatestJDKDefault-756.0-0:
    version = 756.0-0
    buildVariant = Generic
    majorVersion = 756
    packageName = MakeLatestJDKDefault

AuthCpp-2429.0-0:
    version = 2429.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2429
    packageName = AuthCpp

Jackson-1347.0-0:
    version = 1347.0-0
    buildVariant = Generic
    majorVersion = 1347
    packageName = Jackson

CoralMetricsQuerylogReporter-2586.0-0:
    version = 2586.0-0
    buildVariant = Generic
    majorVersion = 2586
    packageName = CoralMetricsQuerylogReporter

AmpJms-3859.0-0:
    version = 3859.0-0
    buildVariant = Generic
    majorVersion = 3859
    packageName = AmpJms

IonJava-2523.0-0:
    version = 2523.0-0
    buildVariant = Generic
    majorVersion = 2523
    packageName = IonJava

AWSAccessPolicyInterface-599.0-0:
    version = 599.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 599
    packageName = AWSAccessPolicyInterface

BSFJavaClient-6685.0-0:
    version = 6685.0-0
    buildVariant = Generic
    majorVersion = 6685
    packageName = BSFJavaClient

AmazonJMS-4102.0-0:
    version = 4102.0-0
    buildVariant = Generic
    majorVersion = 4102
    packageName = AmazonJMS

DirectoryServiceCommon-3616.0-0:
    version = 3616.0-0
    buildVariant = Generic
    majorVersion = 3616
    packageName = DirectoryServiceCommon

JSmart-2873.0-0:
    version = 2873.0-0
    buildVariant = Generic
    majorVersion = 2873
    packageName = JSmart

WebsiteLogPusher-4717.0-0:
    version = 4717.0-0
    buildVariant = Generic
    majorVersion = 4717
    packageName = WebsiteLogPusher

MechNormServiceInterface-105.0-0:
    version = 105.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 105
    packageName = MechNormServiceInterface

JavaServiceAvailabilityManager-543.0-0:
    version = 543.0-0
    buildVariant = Generic
    majorVersion = 543
    packageName = JavaServiceAvailabilityManager

JCTServiceAvailabilityManager-574.0-0:
    version = 574.0-0
    buildVariant = Generic
    majorVersion = 574
    packageName = JCTServiceAvailabilityManager

AWSSecurityTokenInterface-813.0-0:
    version = 813.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 813
    packageName = AWSSecurityTokenInterface

CPAN_JSON-1543.0-0:
    version = 1543.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1543
    packageName = CPAN_JSON

MakePerl58Default-2940.0-0:
    version = 2940.0-0
    buildVariant = Generic
    majorVersion = 2940
    packageName = MakePerl58Default

IquitosClient-2997.0-0:
    version = 2997.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2997
    packageName = IquitosClient

FastcgiClient-2517.0-0:
    version = 2517.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2517
    packageName = FastcgiClient

AccessPointStats-3424.0-0:
    version = 3424.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3424
    packageName = AccessPointStats

DataPump-2296.0-0:
    version = 2296.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2296
    packageName = DataPump

JakartaCommons-lang-1018.0-0:
    version = 1018.0-0
    buildVariant = Generic
    majorVersion = 1018
    packageName = JakartaCommons-lang

Openssl-5055.0-0:
    version = 5055.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 5055
    packageName = Openssl

ApolloConfigurationCommand-2500.0-0:
    version = 2500.0-0
    buildVariant = Generic
    majorVersion = 2500
    packageName = ApolloConfigurationCommand

ProcessManagerCommands-2198.0-0:
    version = 2198.0-0
    buildVariant = Generic
    majorVersion = 2198
    packageName = ProcessManagerCommands

HTTPClient-2926.1-0:
    version = 2926.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2926
    minorVersion = 1
    packageName = HTTPClient

PubsubJava-2107.0-0:
    version = 2107.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2107
    packageName = PubsubJava

BSF-5879.1-0:
    version = 5879.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 5879
    minorVersion = 1
    packageName = BSF

ProcessManager-3678.0-0:
    version = 3678.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3678
    packageName = ProcessManager

ArmorSecurity-5.0-0:
    version = 5.0-0
    buildVariant = Generic
    majorVersion = 5
    packageName = ArmorSecurity

CoralReflect-2339.0-0:
    version = 2339.0-0
    buildVariant = Generic
    majorVersion = 2339
    packageName = CoralReflect

AWSCryptoJava-16.0-0:
    version = 16.0-0
    buildVariant = Generic
    majorVersion = 16
    packageName = AWSCryptoJava

CoralAwsWsdlEmitter-313.0-0:
    version = 313.0-0
    buildVariant = Generic
    majorVersion = 313
    packageName = CoralAwsWsdlEmitter

CoralSchema-923.0-0:
    version = 923.0-0
    buildVariant = Generic
    majorVersion = 923
    packageName = CoralSchema

CoralGenericInterface-943.0-0:
    version = 943.0-0
    buildVariant = Generic
    majorVersion = 943
    packageName = CoralGenericInterface

CoralGeneric-1091.0-0:
    version = 1091.0-0
    buildVariant = Generic
    majorVersion = 1091
    packageName = CoralGeneric

CoralEnvelopeInterface-1067.0-0:
    version = 1067.0-0
    buildVariant = Generic
    majorVersion = 1067
    packageName = CoralEnvelopeInterface

HttpSigningCpp-1240.0-0:
    version = 1240.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1240
    packageName = HttpSigningCpp

OdinLocalRetriever-698.1-0:
    version = 698.1-0
    buildVariant = Generic
    majorVersion = 698
    minorVersion = 1
    packageName = OdinLocalRetriever

CoralGrammar-1626.0-0:
    version = 1626.0-0
    buildVariant = Generic
    majorVersion = 1626
    packageName = CoralGrammar

AuthJavaHTTP-429.0-0:
    version = 429.0-0
    buildVariant = Generic
    majorVersion = 429
    packageName = AuthJavaHTTP

ForceRestartProcessManagerCmds-1496.0-0:
    version = 1496.0-0
    buildVariant = Generic
    majorVersion = 1496
    packageName = ForceRestartProcessManagerCmds

AMPJmsExtension-3059.0-0:
    version = 3059.0-0
    buildVariant = Generic
    majorVersion = 3059
    packageName = AMPJmsExtension

LiveWatchJvmPublisherWebapp-208.0-0:
    version = 208.0-0
    buildVariant = Generic
    majorVersion = 208
    packageName = LiveWatchJvmPublisherWebapp

LiveWatchJvmPublisherServlet-212.0-0:
    version = 212.0-0
    buildVariant = Generic
    majorVersion = 212
    packageName = LiveWatchJvmPublisherServlet

PingServlet-220.0-0:
    version = 220.0-0
    buildVariant = Generic
    majorVersion = 220
    packageName = PingServlet

ServiceMonitoring-1363.0-0:
    version = 1363.0-0
    buildVariant = Generic
    majorVersion = 1363
    packageName = ServiceMonitoring

LiveWatchJvmPublisher-855.0-0:
    version = 855.0-0
    buildVariant = Generic
    majorVersion = 855
    packageName = LiveWatchJvmPublisher

JavaAmazonLdapUtils-113.0-1:
    version = 113.0-1
    buildVariant = Generic
    majorVersion = 113
    packageName = JavaAmazonLdapUtils
    patchVersion = 1

CoralSecurity-1625.0-0:
    version = 1625.0-0
    buildVariant = Generic
    majorVersion = 1625
    packageName = CoralSecurity

FindBugsAnnotations-877.0-0:
    version = 877.0-0
    buildVariant = Generic
    majorVersion = 877
    packageName = FindBugsAnnotations

CoralAnnotation-1424.0-0:
    version = 1424.0-0
    buildVariant = Generic
    majorVersion = 1424
    packageName = CoralAnnotation

ApacheXMLSecurityJava-466.0-0:
    version = 466.0-0
    buildVariant = Generic
    majorVersion = 466
    packageName = ApacheXMLSecurityJava

CoralValue-1408.0-0:
    version = 1408.0-0
    buildVariant = Generic
    majorVersion = 1408
    packageName = CoralValue

Sun-JSR-275-3096.0-0:
    version = 3096.0-0
    buildVariant = Generic
    majorVersion = 3096
    packageName = Sun-JSR-275

AmazonJMSDeprecatedInterfaces-3258.0-0:
    version = 3258.0-0
    buildVariant = Generic
    majorVersion = 3258
    packageName = AmazonJMSDeprecatedInterfaces

FluxoPerlClient-686.0-0:
    version = 686.0-0
    buildVariant = Generic
    majorVersion = 686
    packageName = FluxoPerlClient

JavaHttpSpnego-361.0-0:
    version = 361.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 361
    packageName = JavaHttpSpnego

HttpKeytabRetriever-527.0-0:
    version = 527.0-0
    buildVariant = Generic
    majorVersion = 527
    packageName = HttpKeytabRetriever

CoralMetrics-3612.0-0:
    version = 3612.0-0
    buildVariant = Generic
    majorVersion = 3612
    packageName = CoralMetrics

CPAN_Log_Dispatch_File_Rolling-1066.0-0:
    version = 1066.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1066
    packageName = CPAN_Log_Dispatch_File_Rolling

TibcoJms-3070.0-0:
    version = 3070.0-0
    buildVariant = Generic
    majorVersion = 3070
    packageName = TibcoJms

UdpJms-3438.0-0:
    version = 3438.0-0
    buildVariant = Generic
    majorVersion = 3438
    packageName = UdpJms

AmqJms-4076.0-0:
    version = 4076.0-0
    buildVariant = Generic
    majorVersion = 4076
    packageName = AmqJms

AmznJms-3833.0-0:
    version = 3833.0-0
    buildVariant = Generic
    majorVersion = 3833
    packageName = AmznJms

Slf4j_log4j-718.0-0:
    version = 718.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 718
    packageName = Slf4j_log4j

AmazonProfilerJava-3973.0-0:
    version = 3973.0-0
    buildVariant = Generic
    majorVersion = 3973
    packageName = AmazonProfilerJava

AmazonFileAppenderJava-4312.0-0:
    version = 4312.0-0
    buildVariant = Generic
    majorVersion = 4312
    packageName = AmazonFileAppenderJava

AmazonApolloEnvironmentInfoJava-4389.0-0:
    version = 4389.0-0
    buildVariant = Generic
    majorVersion = 4389
    packageName = AmazonApolloEnvironmentInfoJava

Fbopenssl-644.0-0:
    version = 644.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 644
    packageName = Fbopenssl

AmazonIdJava-4122.0-0:
    version = 4122.0-0
    buildVariant = Generic
    majorVersion = 4122
    packageName = AmazonIdJava

RequestRoutingInterfaces-1278.0-0:
    version = 1278.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1278
    packageName = RequestRoutingInterfaces

Json-org-java-1321.0-0:
    version = 1321.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1321
    packageName = Json-org-java

ActiveMQCppClient-5708.0-0:
    version = 5708.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 5708
    packageName = ActiveMQCppClient

MessagingDNSLookup-3944.0-0:
    version = 3944.0-0
    buildVariant = Generic
    majorVersion = 3944
    packageName = MessagingDNSLookup

ActiveMQJavaClient-6727.0-0:
    version = 6727.0-0
    buildVariant = Generic
    majorVersion = 6727
    packageName = ActiveMQJavaClient

JavaCacheAPI-1977.0-0:
    version = 1977.0-0
    buildVariant = Generic
    majorVersion = 1977
    packageName = JavaCacheAPI

DozerBeanMapper-32.0-0:
    version = 32.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 32
    packageName = DozerBeanMapper

AccessControlEncryption-284.0-1:
    version = 284.0-1
    buildVariant = Generic
    majorVersion = 284
    packageName = AccessControlEncryption
    patchVersion = 1

TranslateToTsd-3254.1-0:
    version = 3254.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3254
    minorVersion = 1
    packageName = TranslateToTsd

J2ee_jms-2741.0-0:
    version = 2741.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2741
    packageName = J2ee_jms

IquitosClient-2938.0-0:
    version = 2938.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2938
    packageName = IquitosClient

AWSPlatformCommon-497.0-0:
    version = 497.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 497
    packageName = AWSPlatformCommon

TimeSeriesDataLibrary-2008.0-0:
    version = 2008.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2008
    packageName = TimeSeriesDataLibrary

CPAN_Pod_Escapes-2181.0-0:
    version = 2181.0-0
    buildVariant = Generic
    majorVersion = 2181
    packageName = CPAN_Pod_Escapes

InfAutoLbcsPerlClient-694.0-0:
    version = 694.0-0
    buildVariant = Generic
    majorVersion = 694
    packageName = InfAutoLbcsPerlClient

CPAN_Log_Dispatch-1138.0-0:
    version = 1138.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1138
    packageName = CPAN_Log_Dispatch

ICU4C-915.0-0:
    version = 915.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 915
    packageName = ICU4C

CPAN_XML_Simple-1155.0-0:
    version = 1155.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1155
    packageName = CPAN_XML_Simple

Joda-time-1432.0-0:
    version = 1432.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1432
    packageName = Joda-time

CPAN_HTML_Tagset-2329.0-1:
    version = 2329.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2329
    packageName = CPAN_HTML_Tagset
    patchVersion = 1

JakartaCommons-httpclient-3918.0-1:
    version = 3918.0-1
    buildVariant = Generic
    majorVersion = 3918
    packageName = JakartaCommons-httpclient
    patchVersion = 1

JakartaCommons-lang-1002.0-0:
    version = 1002.0-0
    buildVariant = Generic
    majorVersion = 1002
    packageName = JakartaCommons-lang

Perl-2736.0-0:
    version = 2736.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2736
    packageName = Perl

JakartaCommons-beanutils-1027.0-0:
    version = 1027.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1027
    packageName = JakartaCommons-beanutils

MonitoringCore-2069.0-0:
    version = 2069.0-0
    buildVariant = Generic
    majorVersion = 2069
    packageName = MonitoringCore

HTTPClient-2873.1-0:
    version = 2873.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2873
    minorVersion = 1
    packageName = HTTPClient

PubsubJava-2037.0-3:
    version = 2037.0-3
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2037
    packageName = PubsubJava
    patchVersion = 3

JavaCore-4303.0-0:
    version = 4303.0-0
    buildVariant = Generic
    majorVersion = 4303
    packageName = JavaCore

Pubsub-4255.0-0:
    version = 4255.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4255
    packageName = Pubsub

BoostHeaders-1567.0-1:
    version = 1567.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1567
    packageName = BoostHeaders
    patchVersion = 1

GoogleGuava-GWT-2.1-0:
    version = 2.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2
    minorVersion = 1
    packageName = GoogleGuava-GWT

GlassFishJSTL-187.0-0:
    version = 187.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 187
    packageName = GlassFishJSTL

JavaEE_JSTL-182.0-0:
    version = 182.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 182
    packageName = JavaEE_JSTL

GoogleGuava-443.1-0:
    version = 443.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 443
    minorVersion = 1
    packageName = GoogleGuava

TSDAgentConfig-1317.0-0:
    version = 1317.0-0
    buildVariant = Generic
    majorVersion = 1317
    packageName = TSDAgentConfig

HBAServiceClientConfig-4.0-0:
    version = 4.0-0
    buildVariant = Generic
    majorVersion = 4
    packageName = HBAServiceClientConfig

HBAServiceJavaClient-18.0-0:
    version = 18.0-0
    buildVariant = Generic
    majorVersion = 18
    packageName = HBAServiceJavaClient

JavaEE_JSP-396.0-0:
    version = 396.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 396
    packageName = JavaEE_JSP

JavaEE_EL-357.0-1:
    version = 357.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 357
    packageName = JavaEE_EL
    patchVersion = 1

GuacamoleJavaClient-38.0-0:
    version = 38.0-0
    buildVariant = Generic
    majorVersion = 38
    packageName = GuacamoleJavaClient

PKeyTool-216.0-0:
    version = 216.0-0
    buildVariant = Generic
    majorVersion = 216
    packageName = PKeyTool

CPAN_Module_Build-2495.0-0:
    version = 2495.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2495
    packageName = CPAN_Module_Build

CPAN_ExtUtils_ParseXS-2468.0-0:
    version = 2468.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2468
    packageName = CPAN_ExtUtils_ParseXS

CPAN_ExtUtils_CBuilder-2468.0-0:
    version = 2468.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2468
    packageName = CPAN_ExtUtils_CBuilder

RendezvousJava-1316.0-0:
    version = 1316.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1316
    packageName = RendezvousJava

CoralXmlValue-2159.0-0:
    version = 2159.0-0
    buildVariant = Generic
    majorVersion = 2159
    packageName = CoralXmlValue

JakartaCommons-logging-api-2892.0-0:
    version = 2892.0-0
    buildVariant = Generic
    majorVersion = 2892
    packageName = JakartaCommons-logging-api

JakartaCommons-logging-adapters-2497.0-0:
    version = 2497.0-0
    buildVariant = Generic
    majorVersion = 2497
    packageName = JakartaCommons-logging-adapters

AWSAccessPolicyInterface-568.0-0:
    version = 568.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 568
    packageName = AWSAccessPolicyInterface

Backport-util-concurrent-2396.0-0:
    version = 2396.0-0
    buildVariant = Generic
    majorVersion = 2396
    packageName = Backport-util-concurrent

Libcap-1581.0-0:
    version = 1581.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1581
    packageName = Libcap

CPAN_Time_Local-2484.0-0:
    version = 2484.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2484
    packageName = CPAN_Time_Local

MechNormServiceInterface-104.0-0:
    version = 104.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 104
    packageName = MechNormServiceInterface

Rome-152.0-0:
    version = 152.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 152
    packageName = Rome

BoostGraph-570.0-0:
    version = 570.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 570
    packageName = BoostGraph

Bzip2-711.0-0:
    version = 711.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 711
    packageName = Bzip2

AWSSecurityTokenInterface-779.0-0:
    version = 779.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 779
    packageName = AWSSecurityTokenInterface

BoostRegex-720.0-0:
    version = 720.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 720
    packageName = BoostRegex

BoostIostreams-646.0-0:
    version = 646.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 646
    packageName = BoostIostreams

BoostHeaders-1543.0-0:
    version = 1543.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1543
    packageName = BoostHeaders

LibEvent-2352.0-0:
    version = 2352.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2352
    packageName = LibEvent

CPAN_MIME_Lite-2642.0-0:
    version = 2642.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2642
    packageName = CPAN_MIME_Lite

CPAN_mod_perl-2621.0-0:
    version = 2621.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2621
    packageName = CPAN_mod_perl

CPAN_Net_SSLeay_pm-2624.0-0:
    version = 2624.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2624
    packageName = CPAN_Net_SSLeay_pm

CPAN_PathTools-2521.0-0:
    version = 2521.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2521
    packageName = CPAN_PathTools

CPAN_List_MoreUtils-2497.0-0:
    version = 2497.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2497
    packageName = CPAN_List_MoreUtils

CPAN_Crypt_SSLeay-2779.0-0:
    version = 2779.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2779
    packageName = CPAN_Crypt_SSLeay

CPAN_Class_ISA-2170.0-0:
    version = 2170.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2170
    packageName = CPAN_Class_ISA

BoostThread-910.0-0:
    version = 910.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 910
    packageName = BoostThread

CPAN_Pod_Simple-2390.0-0:
    version = 2390.0-0
    buildVariant = Generic
    majorVersion = 2390
    packageName = CPAN_Pod_Simple

BoostFilesystem-924.0-0:
    version = 924.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 924
    packageName = BoostFilesystem

CPAN_IO_Socket_SSL-2604.0-0:
    version = 2604.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2604
    packageName = CPAN_IO_Socket_SSL

CPAN_ExtUtils_MakeMaker-1939.0-0:
    version = 1939.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1939
    packageName = CPAN_ExtUtils_MakeMaker

AOPAlliance-1375.0-0:
    version = 1375.0-0
    buildVariant = Generic
    majorVersion = 1375
    packageName = AOPAlliance

ServiceAvailabilityManager-2761.0-0:
    version = 2761.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2761
    packageName = ServiceAvailabilityManager

AccessPointCore-2628.0-0:
    version = 2628.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2628
    packageName = AccessPointCore

CPAN_Carp_Clan-2897.0-0:
    version = 2897.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2897
    packageName = CPAN_Carp_Clan

CPAN_Date_Calc-2846.0-0:
    version = 2846.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2846
    packageName = CPAN_Date_Calc

CPAN_Bit_Vector-2846.0-0:
    version = 2846.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2846
    packageName = CPAN_Bit_Vector

CPAN_Log_Log4perl-1790.0-1:
    version = 1790.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1790
    packageName = CPAN_Log_Log4perl
    patchVersion = 1

BoostDateTime-677.0-0:
    version = 677.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 677
    packageName = BoostDateTime

J2ee_servlet-2533.0-0:
    version = 2533.0-0
    buildVariant = Generic
    majorVersion = 2533
    packageName = J2ee_servlet

CPAN_MIME_Base64-3189.0-0:
    version = 3189.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3189
    packageName = CPAN_MIME_Base64

CPAN_Digest_MD5-3100.0-0:
    version = 3100.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3100
    packageName = CPAN_Digest_MD5

CPAN_Test-2134.0-0:
    version = 2134.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2134
    packageName = CPAN_Test

CPAN_DateTime_Locale-2643.0-0:
    version = 2643.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2643
    packageName = CPAN_DateTime_Locale

CPAN_DateTime-3024.0-0:
    version = 3024.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3024
    packageName = CPAN_DateTime

CPAN_DateTime_TimeZone-2644.0-0:
    version = 2644.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2644
    packageName = CPAN_DateTime_TimeZone

CPAN_Params_Validate-2704.0-0:
    version = 2704.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2704
    packageName = CPAN_Params_Validate

CPAN_Attribute_Handlers-2625.0-0:
    version = 2625.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2625
    packageName = CPAN_Attribute_Handlers

CPAN_Test_Harness-1903.0-1:
    version = 1903.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1903
    packageName = CPAN_Test_Harness
    patchVersion = 1

CPAN_Digest-3145.0-0:
    version = 3145.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3145
    packageName = CPAN_Digest

CPAN_libnet-2523.0-0:
    version = 2523.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2523
    packageName = CPAN_libnet

CPAN_URI-3067.0-0:
    version = 3067.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3067
    packageName = CPAN_URI

CPAN_HTML_Parser-2983.0-0:
    version = 2983.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2983
    packageName = CPAN_HTML_Parser

CPAN_Test_Simple-1777.0-0:
    version = 1777.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1777
    packageName = CPAN_Test_Simple

CPAN_Compress_Zlib-2519.0-0:
    version = 2519.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2519
    packageName = CPAN_Compress_Zlib

CPAN_Scalar_List_Utils-2631.0-0:
    version = 2631.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2631
    packageName = CPAN_Scalar_List_Utils

Dom4j-2861.0-0:
    version = 2861.0-0
    buildVariant = Generic
    majorVersion = 2861
    packageName = Dom4j

Spiritwave-2694.0-0:
    version = 2694.0-0
    buildVariant = Generic
    majorVersion = 2694
    packageName = Spiritwave

XalanJ-1149.0-0:
    version = 1149.0-0
    buildVariant = Generic
    majorVersion = 1149
    packageName = XalanJ

JakartaCommons-collections-2665.0-0:
    version = 2665.0-0
    buildVariant = Generic
    majorVersion = 2665
    packageName = JakartaCommons-collections

concurrent-2761.0-0:
    version = 2761.0-0
    buildVariant = Generic
    majorVersion = 2761
    packageName = concurrent

Zlib-2836.0-0:
    version = 2836.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2836
    packageName = Zlib

Boost-652.0-0:
    version = 652.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 652
    packageName = Boost

log4j-1933.0-0:
    version = 1933.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1933
    packageName = log4j

Ldap-524.0-0:
    version = 524.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 524
    packageName = Ldap

Rosette-2300.0-0:
    version = 2300.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2300
    packageName = Rosette

libintl-2291.0-0:
    version = 2291.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2291
    packageName = libintl

expat-2094.0-0:
    version = 2094.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2094
    packageName = expat

blowfish-2530.0-0:
    version = 2530.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2530
    packageName = blowfish

ProcessManager-3520.0-0:
    version = 3520.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3520
    packageName = ProcessManager

ArmorQueryExpressionSDB-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = ArmorQueryExpressionSDB

ArmorQueryExpression-2.0-0:
    version = 2.0-0
    buildVariant = Generic
    majorVersion = 2
    packageName = ArmorQueryExpression

CoralJsonValue-1653.0-1:
    version = 1653.0-1
    buildVariant = Generic
    majorVersion = 1653
    packageName = CoralJsonValue
    patchVersion = 1

IonJava-2424.0-0:
    version = 2424.0-0
    buildVariant = Generic
    majorVersion = 2424
    packageName = IonJava

APLXML-2837.0-0:
    version = 2837.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2837
    packageName = APLXML

CoralCompressionEngine-331.0-0:
    version = 331.0-0
    buildVariant = Generic
    majorVersion = 331
    packageName = CoralCompressionEngine

AWSCryptoJava-3.0-0:
    version = 3.0-0
    buildVariant = Generic
    majorVersion = 3
    packageName = AWSCryptoJava

IonDatagramConverter-148.0-0:
    version = 148.0-0
    buildVariant = Generic
    majorVersion = 148
    packageName = IonDatagramConverter

CoralAwsWsdlEmitter-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = CoralAwsWsdlEmitter

CoralGenericValue-1690.0-0:
    version = 1690.0-0
    buildVariant = Generic
    majorVersion = 1690
    packageName = CoralGenericValue

CoralSchema-1.0-0:
    version = 1.0-0
    buildVariant = Generic
    majorVersion = 1
    packageName = CoralSchema

CoralGenericInterface-22.0-0:
    version = 22.0-0
    buildVariant = Generic
    majorVersion = 22
    packageName = CoralGenericInterface

CoralSerializeCore-1302.0-1:
    version = 1302.0-1
    buildVariant = Generic
    majorVersion = 1302
    packageName = CoralSerializeCore
    patchVersion = 1

CoralGeneric-133.0-0:
    version = 133.0-0
    buildVariant = Generic
    majorVersion = 133
    packageName = CoralGeneric

CoralEnvelopeInterface-114.0-0:
    version = 114.0-0
    buildVariant = Generic
    majorVersion = 114
    packageName = CoralEnvelopeInterface

HttpSigningCpp-6.1-0:
    version = 6.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 6
    minorVersion = 1
    packageName = HttpSigningCpp

CoralEnvelopeCore-1686.0-1:
    version = 1686.0-1
    buildVariant = Generic
    majorVersion = 1686
    packageName = CoralEnvelopeCore
    patchVersion = 1

AuthJavaHTTP-181.0-0:
    version = 181.0-0
    buildVariant = Generic
    majorVersion = 181
    packageName = AuthJavaHTTP

CoralEnvelope-2481.0-0:
    version = 2481.0-0
    buildVariant = Generic
    majorVersion = 2481
    packageName = CoralEnvelope

ForceRestartProcessManagerCmds-656.0-0:
    version = 656.0-0
    buildVariant = Generic
    majorVersion = 656
    packageName = ForceRestartProcessManagerCmds

SpringExpression-582.1-0:
    version = 582.1-0
    buildVariant = Generic
    majorVersion = 582
    minorVersion = 1
    packageName = SpringExpression

SpringOXM-77.1-0:
    version = 77.1-0
    buildVariant = Generic
    majorVersion = 77
    minorVersion = 1
    packageName = SpringOXM

CoralDThrottleClient-715.1-0:
    version = 715.1-0
    buildVariant = Generic
    majorVersion = 715
    minorVersion = 1
    packageName = CoralDThrottleClient

AMPJmsExtension-1065.0-1:
    version = 1065.0-1
    buildVariant = Generic
    majorVersion = 1065
    packageName = AMPJmsExtension
    patchVersion = 1

CoralBuffer-1121.0-0:
    version = 1121.0-0
    buildVariant = Generic
    majorVersion = 1121
    packageName = CoralBuffer

LiveWatchJvmPublisherWebapp-52.0-0:
    version = 52.0-0
    buildVariant = Generic
    majorVersion = 52
    packageName = LiveWatchJvmPublisherWebapp

LiveWatchJvmPublisherServlet-54.0-0:
    version = 54.0-0
    buildVariant = Generic
    majorVersion = 54
    packageName = LiveWatchJvmPublisherServlet

OctaneTomcatPlatform-143.0-0:
    version = 143.0-0
    buildVariant = Generic
    majorVersion = 143
    packageName = OctaneTomcatPlatform

TomcatQueryLogger-132.0-0:
    version = 132.0-0
    buildVariant = Generic
    majorVersion = 132
    packageName = TomcatQueryLogger

PingServlet-74.0-0:
    version = 74.0-0
    buildVariant = Generic
    majorVersion = 74
    packageName = PingServlet

GlassFishJSTL-4.0-0:
    version = 4.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4
    packageName = GlassFishJSTL

RunWithCronolog-152.0-0:
    version = 152.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 152
    packageName = RunWithCronolog

JavaEE_JSTL-4.0-0:
    version = 4.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 4
    packageName = JavaEE_JSTL

TomcatApolloSetupCommands-166.0-0:
    version = 166.0-0
    buildVariant = Generic
    majorVersion = 166
    packageName = TomcatApolloSetupCommands

TomcatApolloScripts-202.0-0:
    version = 202.0-0
    buildVariant = Generic
    majorVersion = 202
    packageName = TomcatApolloScripts

OctaneTomcatKerberosSupport-104.0-0:
    version = 104.0-0
    buildVariant = Generic
    majorVersion = 104
    packageName = OctaneTomcatKerberosSupport

TomcatApolloWebappLoader-163.0-0:
    version = 163.0-0
    buildVariant = Generic
    majorVersion = 163
    packageName = TomcatApolloWebappLoader

ServiceMonitoring-368.1-0:
    version = 368.1-0
    buildVariant = Generic
    majorVersion = 368
    minorVersion = 1
    packageName = ServiceMonitoring

ServiceMonitoringTSD-783.1-0:
    version = 783.1-0
    buildVariant = Generic
    majorVersion = 783
    minorVersion = 1
    packageName = ServiceMonitoringTSD

TSDAgentConfig-22.0-0:
    version = 22.0-0
    buildVariant = Generic
    majorVersion = 22
    packageName = TSDAgentConfig

LiveWatchJvmPublisher-218.0-0:
    version = 218.0-0
    buildVariant = Generic
    majorVersion = 218
    packageName = LiveWatchJvmPublisher

JakartaTomcatFullCleanConfig-237.0-0:
    version = 237.0-0
    buildVariant = Generic
    majorVersion = 237
    packageName = JakartaTomcatFullCleanConfig

OdinLocalClientConfig-1061.0-0:
    version = 1061.0-0
    buildVariant = Generic
    majorVersion = 1061
    packageName = OdinLocalClientConfig

HBAServiceClientConfig-1.1-1:
    version = 1.1-1
    buildVariant = Generic
    majorVersion = 1
    minorVersion = 1
    packageName = HBAServiceClientConfig
    patchVersion = 1

ApacheTomcat6-552.0-0:
    version = 552.0-0
    buildVariant = Generic
    majorVersion = 552
    packageName = ApacheTomcat6

HBAServiceJavaClient-16.0-1:
    version = 16.0-1
    buildVariant = Generic
    majorVersion = 16
    packageName = HBAServiceJavaClient
    patchVersion = 1

SpringWeb-165.1-0:
    version = 165.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 165
    minorVersion = 1
    packageName = SpringWeb

JavaEE_JSP-17.0-0:
    version = 17.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 17
    packageName = JavaEE_JSP

JavaEE_EL-14.0-0:
    version = 14.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 14
    packageName = JavaEE_EL

SecureJavaKeystoreGenerator-150.0-1:
    version = 150.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 150
    packageName = SecureJavaKeystoreGenerator
    patchVersion = 1

GuacamoleJavaClient-36.0-1:
    version = 36.0-1
    buildVariant = Generic
    majorVersion = 36
    packageName = GuacamoleJavaClient
    patchVersion = 1

GuacamoleClientConfig-3.0-0:
    version = 3.0-0
    buildVariant = Generic
    majorVersion = 3
    packageName = GuacamoleClientConfig

LbStatusPerlClient-592.0-0:
    version = 592.0-0
    buildVariant = Generic
    majorVersion = 592
    packageName = LbStatusPerlClient

CoralCacheValue-2518.0-0:
    version = 2518.0-0
    buildVariant = Generic
    majorVersion = 2518
    packageName = CoralCacheValue

GWT-36.0-0:
    version = 36.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 36
    packageName = GWT

CoralValidate-857.0-0:
    version = 857.0-0
    buildVariant = Generic
    majorVersion = 857
    packageName = CoralValidate

CoralSecurity-738.0-0:
    version = 738.0-0
    buildVariant = Generic
    majorVersion = 738
    packageName = CoralSecurity

PKeyTool-7.0-0:
    version = 7.0-0
    buildVariant = Generic
    majorVersion = 7
    packageName = PKeyTool

FindBugsAnnotations-239.0-0:
    version = 239.0-0
    buildVariant = Generic
    majorVersion = 239
    packageName = FindBugsAnnotations

CPAN_Module_Build-276.0-0:
    version = 276.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 276
    packageName = CPAN_Module_Build

CPAN_ExtUtils_ParseXS-276.0-0:
    version = 276.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 276
    packageName = CPAN_ExtUtils_ParseXS

CPAN_ExtUtils_CBuilder-275.0-0:
    version = 275.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 275
    packageName = CPAN_ExtUtils_CBuilder

CoralAwsMetering-433.0-0:
    version = 433.0-0
    buildVariant = Generic
    majorVersion = 433
    packageName = CoralAwsMetering

CoralJavaClientDependencies-661.0-0:
    version = 661.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 661
    packageName = CoralJavaClientDependencies

CoralMetricsBridge-789.0-0:
    version = 789.0-0
    buildVariant = Generic
    majorVersion = 789
    packageName = CoralMetricsBridge

AuthJava-2917.0-0:
    version = 2917.0-0
    buildVariant = Generic
    majorVersion = 2917
    packageName = AuthJava

MinimalProcessManager-910.1-0:
    version = 910.1-0
    buildVariant = Generic
    majorVersion = 910
    minorVersion = 1
    packageName = MinimalProcessManager

CoralActivity-1733.0-0:
    version = 1733.0-0
    buildVariant = Generic
    majorVersion = 1733
    packageName = CoralActivity

CoralAnnotation-480.0-0:
    version = 480.0-0
    buildVariant = Generic
    majorVersion = 480
    packageName = CoralAnnotation

RequestRoutingInterfacesJava-2630.0-0:
    version = 2630.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2630
    packageName = RequestRoutingInterfacesJava

CoralReflect-2074.0-0:
    version = 2074.0-0
    buildVariant = Generic
    majorVersion = 2074
    packageName = CoralReflect

MakeLatestJDKDefault-583.0-0:
    version = 583.0-0
    buildVariant = Generic
    majorVersion = 583
    packageName = MakeLatestJDKDefault

AuthCpp-1445.0-0:
    version = 1445.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1445
    packageName = AuthCpp

ApacheXMLSecurityJava-152.0-0:
    version = 152.0-0
    buildVariant = Generic
    majorVersion = 152
    packageName = ApacheXMLSecurityJava

SimpleHttpFilters-104.0-0:
    version = 104.0-0
    buildVariant = Generic
    majorVersion = 104
    packageName = SimpleHttpFilters

RendezvousJava-104.0-0:
    version = 104.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 104
    packageName = RendezvousJava

CoralXmlValue-1214.0-0:
    version = 1214.0-0
    buildVariant = Generic
    majorVersion = 1214
    packageName = CoralXmlValue

CoralValue-482.0-0:
    version = 482.0-0
    buildVariant = Generic
    majorVersion = 482
    packageName = CoralValue

CoralTibcoValue-1000.0-0:
    version = 1000.0-0
    buildVariant = Generic
    majorVersion = 1000
    packageName = CoralTibcoValue

CoralQueryStringValue-1496.0-0:
    version = 1496.0-0
    buildVariant = Generic
    majorVersion = 1496
    packageName = CoralQueryStringValue

CoralJsonValue-682.0-0:
    version = 682.0-0
    buildVariant = Generic
    majorVersion = 682
    packageName = CoralJsonValue

CoralBeanValue-2787.0-0:
    version = 2787.0-0
    buildVariant = Generic
    majorVersion = 2787
    packageName = CoralBeanValue

Sun-JSR-275-1095.1-0:
    version = 1095.1-0
    buildVariant = Generic
    majorVersion = 1095
    minorVersion = 1
    packageName = Sun-JSR-275

AmazonJMSDeprecatedInterfaces-1381.0-0:
    version = 1381.0-0
    buildVariant = Generic
    majorVersion = 1381
    packageName = AmazonJMSDeprecatedInterfaces

Jackson-638.0-0:
    version = 638.0-0
    buildVariant = Generic
    majorVersion = 638
    packageName = Jackson

FluxoPerlClient-684.0-0:
    version = 684.0-0
    buildVariant = Generic
    majorVersion = 684
    packageName = FluxoPerlClient

JavaHttpSpnego-150.0-0:
    version = 150.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 150
    packageName = JavaHttpSpnego

HttpKeytabRetriever-286.2-0:
    version = 286.2-0
    buildVariant = Generic
    majorVersion = 286
    minorVersion = 2
    packageName = HttpKeytabRetriever

CoralMetrics-1484.1-0:
    version = 1484.1-0
    buildVariant = Generic
    majorVersion = 1484
    minorVersion = 1
    packageName = CoralMetrics

CPAN_Log_Dispatch_File_Rolling-38.1-0:
    version = 38.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 38
    minorVersion = 1
    packageName = CPAN_Log_Dispatch_File_Rolling

SpringAOP-1191.1-0:
    version = 1191.1-0
    buildVariant = Generic
    majorVersion = 1191
    minorVersion = 1
    packageName = SpringAOP

SpringBeans-1190.1-0:
    version = 1190.1-0
    buildVariant = Generic
    majorVersion = 1190
    minorVersion = 1
    packageName = SpringBeans

SpringContext-1229.1-0:
    version = 1229.1-0
    buildVariant = Generic
    majorVersion = 1229
    minorVersion = 1
    packageName = SpringContext

SpringCore-1240.1-0:
    version = 1240.1-0
    buildVariant = Generic
    majorVersion = 1240
    minorVersion = 1
    packageName = SpringCore

JakartaCommons-logging-api-304.0-0:
    version = 304.0-0
    buildVariant = Generic
    majorVersion = 304
    packageName = JakartaCommons-logging-api

AmpJms-2884.0-0:
    version = 2884.0-0
    buildVariant = Generic
    majorVersion = 2884
    packageName = AmpJms

TibcoJms-1319.0-0:
    version = 1319.0-0
    buildVariant = Generic
    majorVersion = 1319
    packageName = TibcoJms

UdpJms-1546.0-0:
    version = 1546.0-0
    buildVariant = Generic
    majorVersion = 1546
    packageName = UdpJms

AmqJms-1987.1-0:
    version = 1987.1-0
    buildVariant = Generic
    majorVersion = 1987
    minorVersion = 1
    packageName = AmqJms

AmznJms-1814.1-0:
    version = 1814.1-0
    buildVariant = Generic
    majorVersion = 1814
    minorVersion = 1
    packageName = AmznJms

Slf4j_log4j-187.1-0:
    version = 187.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 187
    minorVersion = 1
    packageName = Slf4j_log4j

AmazonProfilerJava-1706.0-0:
    version = 1706.0-0
    buildVariant = Generic
    majorVersion = 1706
    packageName = AmazonProfilerJava

AmazonFileAppenderJava-2679.0-1:
    version = 2679.0-1
    buildVariant = Generic
    majorVersion = 2679
    packageName = AmazonFileAppenderJava
    patchVersion = 1

AmazonApolloEnvironmentInfoJava-2155.0-0:
    version = 2155.0-0
    buildVariant = Generic
    majorVersion = 2155
    packageName = AmazonApolloEnvironmentInfoJava

AmazonAppConfigJava-4642.0-0:
    version = 4642.0-0
    buildVariant = Generic
    majorVersion = 4642
    packageName = AmazonAppConfigJava

AmazonTypesJava-3627.1-0:
    version = 3627.1-0
    buildVariant = Generic
    majorVersion = 3627
    minorVersion = 1
    packageName = AmazonTypesJava

Fbopenssl-307.1-0:
    version = 307.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 307
    minorVersion = 1
    packageName = Fbopenssl

AmazonIdJava-2122.0-0:
    version = 2122.0-0
    buildVariant = Generic
    majorVersion = 2122
    packageName = AmazonIdJava

JakartaCommons-logging-adapters-252.0-0:
    version = 252.0-0
    buildVariant = Generic
    majorVersion = 252
    packageName = JakartaCommons-logging-adapters

RequestRoutingInterfaces-254.0-0:
    version = 254.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 254
    packageName = RequestRoutingInterfaces

Backport-util-concurrent-282.0-0:
    version = 282.0-0
    buildVariant = Generic
    majorVersion = 282
    packageName = Backport-util-concurrent

Libcap-22.0-0:
    version = 22.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 22
    packageName = Libcap

ZThread-2306.0-0:
    version = 2306.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2306
    packageName = ZThread

AmazonJMS-3121.0-0:
    version = 3121.0-0
    buildVariant = Generic
    majorVersion = 3121
    packageName = AmazonJMS

Slf4j-1294.0-1:
    version = 1294.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1294
    packageName = Slf4j
    patchVersion = 1

ActiveMQCppBase-1642.0-0:
    version = 1642.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1642
    packageName = ActiveMQCppBase

ActiveMQCppClient-3884.1-0:
    version = 3884.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3884
    minorVersion = 1
    packageName = ActiveMQCppClient

ActiveMQCPPDiscoveryClient-1685.0-0:
    version = 1685.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1685
    packageName = ActiveMQCPPDiscoveryClient

MessagingDNSLookup-2399.0-0:
    version = 2399.0-0
    buildVariant = Generic
    majorVersion = 2399
    packageName = MessagingDNSLookup

DirectoryServiceCommon-2656.0-0:
    version = 2656.0-0
    buildVariant = Generic
    majorVersion = 2656
    packageName = DirectoryServiceCommon

ActiveMQJavaClient-4664.1-0:
    version = 4664.1-0
    buildVariant = Generic
    majorVersion = 4664
    minorVersion = 1
    packageName = ActiveMQJavaClient

JSmart4JCT-3460.1-0:
    version = 3460.1-0
    buildVariant = Generic
    majorVersion = 3460
    minorVersion = 1
    packageName = JSmart4JCT

JSmart-1990.0-0:
    version = 1990.0-0
    buildVariant = Generic
    majorVersion = 1990
    packageName = JSmart

WebsiteLogPusher-4342.0-1:
    version = 4342.0-1
    buildVariant = Generic
    majorVersion = 4342
    packageName = WebsiteLogPusher
    patchVersion = 1

APLException-1765.0-0:
    version = 1765.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1765
    packageName = APLException

CPAN_Time_Local-278.0-0:
    version = 278.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 278
    packageName = CPAN_Time_Local

DisableCMFWebsitePushLogsApollo-883.1-0:
    version = 883.1-0
    buildVariant = Generic
    majorVersion = 883
    minorVersion = 1
    packageName = DisableCMFWebsitePushLogsApollo

JavaCacheAPI-806.0-0:
    version = 806.0-0
    buildVariant = Generic
    majorVersion = 806
    packageName = JavaCacheAPI

Cronolog-176.0-0:
    version = 176.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 176
    packageName = Cronolog

JavaServiceAvailabilityManager-353.0-0:
    version = 353.0-0
    buildVariant = Generic
    majorVersion = 353
    packageName = JavaServiceAvailabilityManager

JCTServiceAvailabilityManager-402.0-0:
    version = 402.0-0
    buildVariant = Generic
    majorVersion = 402
    packageName = JCTServiceAvailabilityManager

Rome-20.0-0:
    version = 20.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 20
    packageName = Rome

CPAN_podlators-1687.0-0:
    version = 1687.0-0
    buildVariant = Generic
    majorVersion = 1687
    packageName = CPAN_podlators

BoostGraph-81.0-0:
    version = 81.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 81
    packageName = BoostGraph

Bzip2-81.0-0:
    version = 81.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 81
    packageName = Bzip2

DozerBeanMapper-16.0-0:
    version = 16.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 16
    packageName = DozerBeanMapper

AccessControlEncryption-63.0-0:
    version = 63.0-0
    buildVariant = Generic
    majorVersion = 63
    packageName = AccessControlEncryption

BoostRegex-171.0-0:
    version = 171.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 171
    packageName = BoostRegex

BoostIostreams-261.0-1:
    version = 261.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 261
    packageName = BoostIostreams
    patchVersion = 1

CPAN_SOAP_Lite-2996.1-0:
    version = 2996.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2996
    minorVersion = 1
    packageName = CPAN_SOAP_Lite

LibEvent-267.0-0:
    version = 267.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 267
    packageName = LibEvent

J2ee_jms-877.0-0:
    version = 877.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 877
    packageName = J2ee_jms

CPAN_MIME_Lite-331.0-0:
    version = 331.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 331
    packageName = CPAN_MIME_Lite

CPAN_mod_perl-323.0-0:
    version = 323.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 323
    packageName = CPAN_mod_perl

CPAN_Net_SSLeay_pm-319.0-0:
    version = 319.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 319
    packageName = CPAN_Net_SSLeay_pm

CPAN_JSON-748.1-0:
    version = 748.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 748
    minorVersion = 1
    packageName = CPAN_JSON

CPAN_PathTools-282.0-0:
    version = 282.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 282
    packageName = CPAN_PathTools

MakePerl58Default-2019.0-0:
    version = 2019.0-0
    buildVariant = Generic
    majorVersion = 2019
    packageName = MakePerl58Default

CPAN_List_MoreUtils-279.0-0:
    version = 279.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 279
    packageName = CPAN_List_MoreUtils

CPAN_XML_XPath-1165.2-0:
    version = 1165.2-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1165
    minorVersion = 2
    packageName = CPAN_XML_XPath

CPAN_Crypt_SSLeay-347.0-0:
    version = 347.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 347
    packageName = CPAN_Crypt_SSLeay

FastcgiClient-1286.0-0:
    version = 1286.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1286
    packageName = FastcgiClient

CPAN_Class_ISA-246.0-0:
    version = 246.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 246
    packageName = CPAN_Class_ISA

BoostThread-359.0-2:
    version = 359.0-2
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 359
    packageName = BoostThread
    patchVersion = 2

CPAN_Pod_Simple-258.0-0:
    version = 258.0-0
    buildVariant = Generic
    majorVersion = 258
    packageName = CPAN_Pod_Simple

AWSPlatformCommon-388.0-0:
    version = 388.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 388
    packageName = AWSPlatformCommon

BoostFilesystem-178.0-0:
    version = 178.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 178
    packageName = BoostFilesystem

CPAN_MIME_tools-2290.1-0:
    version = 2290.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2290
    minorVersion = 1
    packageName = CPAN_MIME_tools

CPAN_IO_Socket_SSL-1164.0-0:
    version = 1164.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1164
    packageName = CPAN_IO_Socket_SSL

CPAN_ExtUtils_MakeMaker-78.0-0:
    version = 78.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 78
    packageName = CPAN_ExtUtils_MakeMaker

CPAN_Pod_Escapes-1005.0-0:
    version = 1005.0-0
    buildVariant = Generic
    majorVersion = 1005
    packageName = CPAN_Pod_Escapes

AOPAlliance-66.0-0:
    version = 66.0-0
    buildVariant = Generic
    majorVersion = 66
    packageName = AOPAlliance

ServiceAvailabilityManager-644.0-0:
    version = 644.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 644
    packageName = ServiceAvailabilityManager

AccessPointStats-1966.0-0:
    version = 1966.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1966
    packageName = AccessPointStats

AccessPointCore-558.0-0:
    version = 558.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 558
    packageName = AccessPointCore

CPAN_Carp_Clan-365.0-0:
    version = 365.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 365
    packageName = CPAN_Carp_Clan

CPAN_Date_Calc-323.0-0:
    version = 323.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 323
    packageName = CPAN_Date_Calc

CPAN_Bit_Vector-319.0-0:
    version = 319.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 319
    packageName = CPAN_Bit_Vector

CPAN_MailTools-544.1-0:
    version = 544.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 544
    minorVersion = 1
    packageName = CPAN_MailTools

DataPump-1238.1-0:
    version = 1238.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1238
    minorVersion = 1
    packageName = DataPump

JakartaTomcatPlatformConfig-365.0-0:
    version = 365.0-0
    buildVariant = Generic
    majorVersion = 365
    packageName = JakartaTomcatPlatformConfig

CPAN_Log_Dispatch-51.1-0:
    version = 51.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 51
    minorVersion = 1
    packageName = CPAN_Log_Dispatch

CPAN_Log_Log4perl-90.0-0:
    version = 90.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 90
    packageName = CPAN_Log_Log4perl

CPAN_XML_Parser-2974.1-0:
    version = 2974.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2974
    minorVersion = 1
    packageName = CPAN_XML_Parser

BoostDateTime-149.0-0:
    version = 149.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 149
    packageName = BoostDateTime

J2ee_servlet-639.0-0:
    version = 639.0-0
    buildVariant = Generic
    majorVersion = 639
    packageName = J2ee_servlet

CPAN_MIME_Base64-385.0-0:
    version = 385.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 385
    packageName = CPAN_MIME_Base64

CPAN_Digest_HMAC-2361.1-0:
    version = 2361.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2361
    minorVersion = 1
    packageName = CPAN_Digest_HMAC

CPAN_Digest_SHA1-2377.1-0:
    version = 2377.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2377
    minorVersion = 1
    packageName = CPAN_Digest_SHA1

CPAN_Digest_MD5-357.0-0:
    version = 357.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 357
    packageName = CPAN_Digest_MD5

ICU4C-432.0-0:
    version = 432.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 432
    packageName = ICU4C

CPAN_FCGI-2370.1-0:
    version = 2370.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2370
    minorVersion = 1
    packageName = CPAN_FCGI

CPAN_XML_Simple-1017.1-0:
    version = 1017.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1017
    minorVersion = 1
    packageName = CPAN_XML_Simple

CPAN_Test-84.0-0:
    version = 84.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 84
    packageName = CPAN_Test

JakartaCommons-codec-2335.0-0:
    version = 2335.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2335
    packageName = JakartaCommons-codec

Joda-time-432.1-0:
    version = 432.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 432
    minorVersion = 1
    packageName = Joda-time

CPAN_IO_stringy-2353.1-0:
    version = 2353.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2353
    minorVersion = 1
    packageName = CPAN_IO_stringy

CPAN_DateTime_Locale-368.0-0:
    version = 368.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 368
    packageName = CPAN_DateTime_Locale

CPAN_DateTime-609.0-0:
    version = 609.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 609
    packageName = CPAN_DateTime

CPAN_DateTime_TimeZone-373.1-0:
    version = 373.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 373
    minorVersion = 1
    packageName = CPAN_DateTime_TimeZone

CPAN_Params_Validate-315.0-0:
    version = 315.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 315
    packageName = CPAN_Params_Validate

CPAN_Attribute_Handlers-294.0-0:
    version = 294.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 294
    packageName = CPAN_Attribute_Handlers

CPAN_Class_Singleton-1497.0-0:
    version = 1497.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1497
    packageName = CPAN_Class_Singleton

CPAN_Test_Harness-633.0-1:
    version = 633.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 633
    packageName = CPAN_Test_Harness
    patchVersion = 1

CPAN_Digest-362.0-0:
    version = 362.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 362
    packageName = CPAN_Digest

CPAN_HTML_Tagset-465.0-1:
    version = 465.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 465
    packageName = CPAN_HTML_Tagset
    patchVersion = 1

CPAN_libwww_perl-2953.0-0:
    version = 2953.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2953
    packageName = CPAN_libwww_perl

CPAN_libnet-215.0-0:
    version = 215.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 215
    packageName = CPAN_libnet

CPAN_URI-381.0-0:
    version = 381.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 381
    packageName = CPAN_URI

CPAN_HTML_Parser-340.0-0:
    version = 340.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 340
    packageName = CPAN_HTML_Parser

CPAN_Test_Simple-122.0-0:
    version = 122.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 122
    packageName = CPAN_Test_Simple

CPAN_Compress_Zlib-215.0-0:
    version = 215.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 215
    packageName = CPAN_Compress_Zlib

CPAN_Scalar_List_Utils-314.0-0:
    version = 314.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 314
    packageName = CPAN_Scalar_List_Utils

Dom4j-372.0-0:
    version = 372.0-0
    buildVariant = Generic
    majorVersion = 372
    packageName = Dom4j

JakartaCommons-httpclient-1691.0-2:
    version = 1691.0-2
    buildVariant = Generic
    majorVersion = 1691
    packageName = JakartaCommons-httpclient
    patchVersion = 2

Spiritwave-343.0-0:
    version = 343.0-0
    buildVariant = Generic
    majorVersion = 343
    packageName = Spiritwave

JakartaCommons-lang-534.0-0:
    version = 534.0-0
    buildVariant = Generic
    majorVersion = 534
    packageName = JakartaCommons-lang

JakartaCommons-logging-2305.1-0:
    version = 2305.1-0
    buildVariant = Generic
    majorVersion = 2305
    minorVersion = 1
    packageName = JakartaCommons-logging

XalanJ-243.0-0:
    version = 243.0-0
    buildVariant = Generic
    majorVersion = 243
    packageName = XalanJ

Perl-2624.1-0:
    version = 2624.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2624
    minorVersion = 1
    packageName = Perl

JakartaCommons-collections-505.0-0:
    version = 505.0-0
    buildVariant = Generic
    majorVersion = 505
    packageName = JakartaCommons-collections

JakartaCommons-beanutils-636.1-0:
    version = 636.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 636
    minorVersion = 1
    packageName = JakartaCommons-beanutils

concurrent-356.0-0:
    version = 356.0-0
    buildVariant = Generic
    majorVersion = 356
    packageName = concurrent

Zlib-452.0-0:
    version = 452.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 452
    packageName = Zlib

Readline-1406.0-0:
    version = 1406.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1406
    packageName = Readline

Boost-102.0-0:
    version = 102.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 102
    packageName = Boost

Pcre-418.0-0:
    version = 418.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 418
    packageName = Pcre

MonitoringCore-1949.1-0:
    version = 1949.1-0
    buildVariant = Generic
    majorVersion = 1949
    minorVersion = 1
    packageName = MonitoringCore

Curl-764.1-0:
    version = 764.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 764
    minorVersion = 1
    packageName = Curl

log4j-570.0-0:
    version = 570.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 570
    packageName = log4j

Ldap-67.0-0:
    version = 67.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 67
    packageName = Ldap

Rendezvous-3520.1-0:
    version = 3520.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3520
    minorVersion = 1
    packageName = Rendezvous

ApacheSecurity-498.0-0:
    version = 498.0-0
    buildVariant = Generic
    majorVersion = 498
    packageName = ApacheSecurity

CronAdminCommand-2196.0-0:
    version = 2196.0-0
    buildVariant = Generic
    majorVersion = 2196
    packageName = CronAdminCommand

LogDirectoryPermissionResetCommand-1909.0-0:
    version = 1909.0-0
    buildVariant = Generic
    majorVersion = 1909
    packageName = LogDirectoryPermissionResetCommand

ApolloConfigurationCommand-1692.1-0:
    version = 1692.1-0
    buildVariant = Generic
    majorVersion = 1692
    minorVersion = 1
    packageName = ApolloConfigurationCommand

ProcessManagerCommands-1449.0-0:
    version = 1449.0-0
    buildVariant = Generic
    majorVersion = 1449
    packageName = ProcessManagerCommands

libintl-327.0-0:
    version = 327.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 327
    packageName = libintl

expat-794.0-0:
    version = 794.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 794
    packageName = expat

blowfish-423.0-0:
    version = 423.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 423
    packageName = blowfish

SharedColDefs-768.1-0:
    version = 768.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 768
    minorVersion = 1
    packageName = SharedColDefs

APLXML-1998.0-2:
    version = 1998.0-2
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1998
    packageName = APLXML
    patchVersion = 2

PubsubJava-724.0-1:
    version = 724.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 724
    packageName = PubsubJava
    patchVersion = 1

JavaCore-2674.0-0:
    version = 2674.0-0
    buildVariant = Generic
    majorVersion = 2674
    packageName = JavaCore

Pubsub-1979.1-0:
    version = 1979.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1979
    minorVersion = 1
    packageName = Pubsub

ProcessManager-3422.1-0:
    version = 3422.1-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3422
    minorVersion = 1
    packageName = ProcessManager

JakartaCommons-codec-2982.0-0:
    version = 2982.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2982
    packageName = JakartaCommons-codec

GWT-53.0-1:
    version = 53.0-1
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 53
    packageName = GWT
    patchVersion = 1

CPAN_Log_Dispatch-729.0-0:
    version = 729.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 729
    packageName = CPAN_Log_Dispatch

CPAN_Log_Dispatch_File_Rolling-656.0-0:
    version = 656.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 656
    packageName = CPAN_Log_Dispatch_File_Rolling

CoralGenericValue-1370.0-0:
    version = 1370.0-0
    buildVariant = Generic
    majorVersion = 1370
    packageName = CoralGenericValue

HttpSigningCpp-866.0-0:
    version = 866.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 866
    packageName = HttpSigningCpp

ForceRestartProcessManagerCmds-1203.0-0:
    version = 1203.0-0
    buildVariant = Generic
    majorVersion = 1203
    packageName = ForceRestartProcessManagerCmds

SpringExpression-1041.0-0:
    version = 1041.0-0
    buildVariant = Generic
    majorVersion = 1041
    packageName = SpringExpression

SpringOXM-134.0-0:
    version = 134.0-0
    buildVariant = Generic
    majorVersion = 134
    packageName = SpringOXM

CoralDThrottleClient-649.0-0:
    version = 649.0-0
    buildVariant = Generic
    majorVersion = 649
    packageName = CoralDThrottleClient

AMPJmsExtension-2562.0-0:
    version = 2562.0-0
    buildVariant = Generic
    majorVersion = 2562
    packageName = AMPJmsExtension

OctaneTomcatPlatform-238.0-0:
    version = 238.0-0
    buildVariant = Generic
    majorVersion = 238
    packageName = OctaneTomcatPlatform

PingServlet-166.0-0:
    version = 166.0-0
    buildVariant = Generic
    majorVersion = 166
    packageName = PingServlet

GlassFishJSTL-144.0-0:
    version = 144.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 144
    packageName = GlassFishJSTL

RunWithCronolog-194.0-0:
    version = 194.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 194
    packageName = RunWithCronolog

JavaEE_JSTL-141.0-0:
    version = 141.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 141
    packageName = JavaEE_JSTL

TomcatApolloSetupCommands-291.0-0:
    version = 291.0-0
    buildVariant = Generic
    majorVersion = 291
    packageName = TomcatApolloSetupCommands

OctaneTomcatKerberosSupport-125.0-0:
    version = 125.0-0
    buildVariant = Generic
    majorVersion = 125
    packageName = OctaneTomcatKerberosSupport

TomcatApolloWebappLoader-256.0-0:
    version = 256.0-0
    buildVariant = Generic
    majorVersion = 256
    packageName = TomcatApolloWebappLoader

ServiceMonitoring-1031.0-0:
    version = 1031.0-0
    buildVariant = Generic
    majorVersion = 1031
    packageName = ServiceMonitoring

TSDAgentConfig-1024.0-0:
    version = 1024.0-0
    buildVariant = Generic
    majorVersion = 1024
    packageName = TSDAgentConfig

JakartaTomcatFullCleanConfig-288.0-0:
    version = 288.0-0
    buildVariant = Generic
    majorVersion = 288
    packageName = JakartaTomcatFullCleanConfig

OdinLocalClientConfig-1386.0-0:
    version = 1386.0-0
    buildVariant = Generic
    majorVersion = 1386
    packageName = OdinLocalClientConfig

HBAServiceClientConfig-3.0-0:
    version = 3.0-0
    buildVariant = Generic
    majorVersion = 3
    packageName = HBAServiceClientConfig

ApacheTomcat6-470.0-0:
    version = 470.0-0
    buildVariant = Generic
    majorVersion = 470
    packageName = ApacheTomcat6

SpringWeb-222.0-0:
    version = 222.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 222
    packageName = SpringWeb

JavaEE_JSP-301.0-0:
    version = 301.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 301
    packageName = JavaEE_JSP

JavaEE_EL-271.0-0:
    version = 271.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 271
    packageName = JavaEE_EL

SecureJavaKeystoreGenerator-183.0-0:
    version = 183.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 183
    packageName = SecureJavaKeystoreGenerator

GuacamoleClientConfig-4.0-0:
    version = 4.0-0
    buildVariant = Generic
    majorVersion = 4
    packageName = GuacamoleClientConfig

LbStatusPerlClient-942.0-0:
    version = 942.0-0
    buildVariant = Generic
    majorVersion = 942
    packageName = LbStatusPerlClient

PKeyTool-164.0-0:
    version = 164.0-0
    buildVariant = Generic
    majorVersion = 164
    packageName = PKeyTool

CPAN_Module_Build-2049.0-0:
    version = 2049.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2049
    packageName = CPAN_Module_Build

CPAN_ExtUtils_ParseXS-2046.0-0:
    version = 2046.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2046
    packageName = CPAN_ExtUtils_ParseXS

CPAN_ExtUtils_CBuilder-2044.0-0:
    version = 2044.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2044
    packageName = CPAN_ExtUtils_CBuilder

CoralJavaClientDependencies-622.0-0:
    version = 622.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 622
    packageName = CoralJavaClientDependencies

CoralMetricsBridge-1718.0-0:
    version = 1718.0-0
    buildVariant = Generic
    majorVersion = 1718
    packageName = CoralMetricsBridge

AuthJava-3729.0-0:
    version = 3729.0-0
    buildVariant = Generic
    majorVersion = 3729
    packageName = AuthJava

RequestRoutingInterfacesJava-3023.0-0:
    version = 3023.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3023
    packageName = RequestRoutingInterfacesJava

AuthCpp-1853.0-0:
    version = 1853.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1853
    packageName = AuthCpp

SimpleHttpFilters-204.0-0:
    version = 204.0-0
    buildVariant = Generic
    majorVersion = 204
    packageName = SimpleHttpFilters

RendezvousJava-1035.0-0:
    version = 1035.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1035
    packageName = RendezvousJava

CoralBeanValue-2370.0-0:
    version = 2370.0-0
    buildVariant = Generic
    majorVersion = 2370
    packageName = CoralBeanValue

AmazonJMSDeprecatedInterfaces-2804.0-0:
    version = 2804.0-0
    buildVariant = Generic
    majorVersion = 2804
    packageName = AmazonJMSDeprecatedInterfaces

Jackson-1022.0-0:
    version = 1022.0-0
    buildVariant = Generic
    majorVersion = 1022
    packageName = Jackson

JavaHttpSpnego-298.0-0:
    version = 298.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 298
    packageName = JavaHttpSpnego

HttpKeytabRetriever-432.0-0:
    version = 432.0-0
    buildVariant = Generic
    majorVersion = 432
    packageName = HttpKeytabRetriever

SpringAOP-1701.0-0:
    version = 1701.0-0
    buildVariant = Generic
    majorVersion = 1701
    packageName = SpringAOP

SpringBeans-1705.0-0:
    version = 1705.0-0
    buildVariant = Generic
    majorVersion = 1705
    packageName = SpringBeans

SpringContext-1742.0-0:
    version = 1742.0-0
    buildVariant = Generic
    majorVersion = 1742
    packageName = SpringContext

SpringCore-1751.0-0:
    version = 1751.0-0
    buildVariant = Generic
    majorVersion = 1751
    packageName = SpringCore

JakartaCommons-logging-api-2420.0-0:
    version = 2420.0-0
    buildVariant = Generic
    majorVersion = 2420
    packageName = JakartaCommons-logging-api

AmpJms-3274.0-0:
    version = 3274.0-0
    buildVariant = Generic
    majorVersion = 3274
    packageName = AmpJms

TibcoJms-2669.0-0:
    version = 2669.0-0
    buildVariant = Generic
    majorVersion = 2669
    packageName = TibcoJms

UdpJms-3012.0-0:
    version = 3012.0-0
    buildVariant = Generic
    majorVersion = 3012
    packageName = UdpJms

AmqJms-3543.0-0:
    version = 3543.0-0
    buildVariant = Generic
    majorVersion = 3543
    packageName = AmqJms

AmznJms-3354.0-0:
    version = 3354.0-0
    buildVariant = Generic
    majorVersion = 3354
    packageName = AmznJms

AmazonFileAppenderJava-3796.0-0:
    version = 3796.0-0
    buildVariant = Generic
    majorVersion = 3796
    packageName = AmazonFileAppenderJava

AmazonApolloEnvironmentInfoJava-3842.0-0:
    version = 3842.0-0
    buildVariant = Generic
    majorVersion = 3842
    packageName = AmazonApolloEnvironmentInfoJava

AmazonAppConfigJava-4211.0-0:
    version = 4211.0-0
    buildVariant = Generic
    majorVersion = 4211
    packageName = AmazonAppConfigJava

AmazonTypesJava-3574.0-0:
    version = 3574.0-0
    buildVariant = Generic
    majorVersion = 3574
    packageName = AmazonTypesJava

Fbopenssl-518.0-0:
    version = 518.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 518
    packageName = Fbopenssl

AmazonIdJava-3656.0-0:
    version = 3656.0-0
    buildVariant = Generic
    majorVersion = 3656
    packageName = AmazonIdJava

JakartaCommons-logging-adapters-2069.0-0:
    version = 2069.0-0
    buildVariant = Generic
    majorVersion = 2069
    packageName = JakartaCommons-logging-adapters

RequestRoutingInterfaces-916.0-0:
    version = 916.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 916
    packageName = RequestRoutingInterfaces

Backport-util-concurrent-1958.0-0:
    version = 1958.0-0
    buildVariant = Generic
    majorVersion = 1958
    packageName = Backport-util-concurrent

ZThread-2076.0-0:
    version = 2076.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2076
    packageName = ZThread

AmazonJMS-3498.0-0:
    version = 3498.0-0
    buildVariant = Generic
    majorVersion = 3498
    packageName = AmazonJMS

Slf4j-1511.0-0:
    version = 1511.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1511
    packageName = Slf4j

ActiveMQCppBase-2021.0-0:
    version = 2021.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2021
    packageName = ActiveMQCppBase

ActiveMQCppClient-5205.0-0:
    version = 5205.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 5205
    packageName = ActiveMQCppClient

ActiveMQCPPDiscoveryClient-2069.0-0:
    version = 2069.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2069
    packageName = ActiveMQCPPDiscoveryClient

MessagingDNSLookup-3436.0-0:
    version = 3436.0-0
    buildVariant = Generic
    majorVersion = 3436
    packageName = MessagingDNSLookup

DirectoryServiceCommon-3044.0-0:
    version = 3044.0-0
    buildVariant = Generic
    majorVersion = 3044
    packageName = DirectoryServiceCommon

ActiveMQJavaClient-6214.0-0:
    version = 6214.0-0
    buildVariant = Generic
    majorVersion = 6214
    packageName = ActiveMQJavaClient

JSmart4JCT-3210.0-0:
    version = 3210.0-0
    buildVariant = Generic
    majorVersion = 3210
    packageName = JSmart4JCT

JSmart-2392.0-0:
    version = 2392.0-0
    buildVariant = Generic
    majorVersion = 2392
    packageName = JSmart

APLException-2155.0-0:
    version = 2155.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2155
    packageName = APLException

CPAN_Time_Local-2064.0-0:
    version = 2064.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2064
    packageName = CPAN_Time_Local

DisableCMFWebsitePushLogsApollo-1334.0-0:
    version = 1334.0-0
    buildVariant = Generic
    majorVersion = 1334
    packageName = DisableCMFWebsitePushLogsApollo

JavaCacheAPI-1614.0-0:
    version = 1614.0-0
    buildVariant = Generic
    majorVersion = 1614
    packageName = JavaCacheAPI

Cronolog-215.0-0:
    version = 215.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 215
    packageName = Cronolog

JavaServiceAvailabilityManager-417.0-0:
    version = 417.0-0
    buildVariant = Generic
    majorVersion = 417
    packageName = JavaServiceAvailabilityManager

Rome-121.0-0:
    version = 121.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 121
    packageName = Rome

CPAN_podlators-1970.0-0:
    version = 1970.0-0
    buildVariant = Generic
    majorVersion = 1970
    packageName = CPAN_podlators

BoostGraph-421.0-0:
    version = 421.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 421
    packageName = BoostGraph

DozerBeanMapper-23.0-0:
    version = 23.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 23
    packageName = DozerBeanMapper

AccessControlEncryption-209.0-0:
    version = 209.0-0
    buildVariant = Generic
    majorVersion = 209
    packageName = AccessControlEncryption

BoostRegex-566.0-0:
    version = 566.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 566
    packageName = BoostRegex

BoostIostreams-495.0-0:
    version = 495.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 495
    packageName = BoostIostreams

CPAN_SOAP_Lite-2906.0-0:
    version = 2906.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2906
    packageName = CPAN_SOAP_Lite

J2ee_jms-2301.0-0:
    version = 2301.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2301
    packageName = J2ee_jms

CPAN_MIME_Lite-2192.0-0:
    version = 2192.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2192
    packageName = CPAN_MIME_Lite

CPAN_JSON-1241.0-0:
    version = 1241.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1241
    packageName = CPAN_JSON

CPAN_PathTools-2077.0-0:
    version = 2077.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2077
    packageName = CPAN_PathTools

MakePerl58Default-2388.0-0:
    version = 2388.0-0
    buildVariant = Generic
    majorVersion = 2388
    packageName = MakePerl58Default

CPAN_XML_XPath-1044.0-0:
    version = 1044.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1044
    packageName = CPAN_XML_XPath

CPAN_Class_ISA-1753.0-0:
    version = 1753.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1753
    packageName = CPAN_Class_ISA

BoostThread-693.0-0:
    version = 693.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 693
    packageName = BoostThread

BoostFilesystem-727.0-0:
    version = 727.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 727
    packageName = BoostFilesystem

CPAN_MIME_tools-2207.0-0:
    version = 2207.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2207
    packageName = CPAN_MIME_tools

CPAN_IO_Socket_SSL-2162.0-0:
    version = 2162.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2162
    packageName = CPAN_IO_Socket_SSL

CPAN_Pod_Escapes-1752.0-0:
    version = 1752.0-0
    buildVariant = Generic
    majorVersion = 1752
    packageName = CPAN_Pod_Escapes

AOPAlliance-1078.0-0:
    version = 1078.0-0
    buildVariant = Generic
    majorVersion = 1078
    packageName = AOPAlliance

AccessPointStats-2798.0-0:
    version = 2798.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2798
    packageName = AccessPointStats

CPAN_Carp_Clan-2376.0-0:
    version = 2376.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2376
    packageName = CPAN_Carp_Clan

CPAN_Date_Calc-2333.0-0:
    version = 2333.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2333
    packageName = CPAN_Date_Calc

CPAN_Bit_Vector-2332.0-0:
    version = 2332.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2332
    packageName = CPAN_Bit_Vector

JakartaTomcatPlatformConfig-426.0-0:
    version = 426.0-0
    buildVariant = Generic
    majorVersion = 426
    packageName = JakartaTomcatPlatformConfig

CPAN_XML_Parser-2740.0-0:
    version = 2740.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2740
    packageName = CPAN_XML_Parser

BoostDateTime-528.0-0:
    version = 528.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 528
    packageName = BoostDateTime

J2ee_servlet-2132.0-0:
    version = 2132.0-0
    buildVariant = Generic
    majorVersion = 2132
    packageName = J2ee_servlet

CPAN_Digest_HMAC-2163.0-0:
    version = 2163.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2163
    packageName = CPAN_Digest_HMAC

CPAN_Digest_SHA1-2239.0-0:
    version = 2239.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2239
    packageName = CPAN_Digest_SHA1

CPAN_FCGI-2167.0-0:
    version = 2167.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2167
    packageName = CPAN_FCGI

Joda-time-1176.0-0:
    version = 1176.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1176
    packageName = Joda-time

CPAN_IO_stringy-2168.0-0:
    version = 2168.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2168
    packageName = CPAN_IO_stringy

CPAN_DateTime_Locale-2183.0-0:
    version = 2183.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2183
    packageName = CPAN_DateTime_Locale

CPAN_DateTime-2560.0-0:
    version = 2560.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2560
    packageName = CPAN_DateTime

CPAN_Params_Validate-2238.0-0:
    version = 2238.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2238
    packageName = CPAN_Params_Validate

CPAN_Attribute_Handlers-2156.0-0:
    version = 2156.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2156
    packageName = CPAN_Attribute_Handlers

CPAN_Class_Singleton-1786.0-0:
    version = 1786.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1786
    packageName = CPAN_Class_Singleton

CPAN_Test_Harness-1522.0-0:
    version = 1522.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1522
    packageName = CPAN_Test_Harness

CPAN_HTML_Tagset-1867.0-0:
    version = 1867.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1867
    packageName = CPAN_HTML_Tagset

CPAN_libwww_perl-2543.0-0:
    version = 2543.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2543
    packageName = CPAN_libwww_perl

CPAN_Scalar_List_Utils-2171.0-0:
    version = 2171.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2171
    packageName = CPAN_Scalar_List_Utils

Dom4j-2414.0-0:
    version = 2414.0-0
    buildVariant = Generic
    majorVersion = 2414
    packageName = Dom4j

Spiritwave-2290.0-0:
    version = 2290.0-0
    buildVariant = Generic
    majorVersion = 2290
    packageName = Spiritwave

JakartaCommons-logging-2616.0-0:
    version = 2616.0-0
    buildVariant = Generic
    majorVersion = 2616
    packageName = JakartaCommons-logging

XalanJ-991.0-0:
    version = 991.0-0
    buildVariant = Generic
    majorVersion = 991
    packageName = XalanJ

JakartaCommons-beanutils-858.0-0:
    version = 858.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 858
    packageName = JakartaCommons-beanutils

concurrent-2333.0-0:
    version = 2333.0-0
    buildVariant = Generic
    majorVersion = 2333
    packageName = concurrent

Zlib-2339.0-0:
    version = 2339.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2339
    packageName = Zlib

Readline-1916.0-0:
    version = 1916.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1916
    packageName = Readline

Boost-488.0-0:
    version = 488.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 488
    packageName = Boost

Pcre-639.0-0:
    version = 639.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 639
    packageName = Pcre

Curl-752.0-0:
    version = 752.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 752
    packageName = Curl

log4j-1650.0-0:
    version = 1650.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1650
    packageName = log4j

Rendezvous-3384.0-0:
    version = 3384.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 3384
    packageName = Rendezvous

ApacheSecurity-559.0-0:
    version = 559.0-0
    buildVariant = Generic
    majorVersion = 559
    packageName = ApacheSecurity

CronAdminCommand-2525.0-0:
    version = 2525.0-0
    buildVariant = Generic
    majorVersion = 2525
    packageName = CronAdminCommand

LogDirectoryPermissionResetCommand-2197.0-0:
    version = 2197.0-0
    buildVariant = Generic
    majorVersion = 2197
    packageName = LogDirectoryPermissionResetCommand

ApolloConfigurationCommand-2018.0-0:
    version = 2018.0-0
    buildVariant = Generic
    majorVersion = 2018
    packageName = ApolloConfigurationCommand

ProcessManagerCommands-1716.0-0:
    version = 1716.0-0
    buildVariant = Generic
    majorVersion = 1716
    packageName = ProcessManagerCommands

libintl-1823.0-0:
    version = 1823.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1823
    packageName = libintl

expat-1701.0-0:
    version = 1701.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 1701
    packageName = expat

SharedColDefs-2075.0-0:
    version = 2075.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 2075
    packageName = SharedColDefs

JavaCore-3776.0-0:
    version = 3776.0-0
    buildVariant = Generic
    majorVersion = 3776
    packageName = JavaCore

Json-org-java-460.0-0:
    version = 460.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 460
    packageName = Json-org-java

CPAN_MailTools-409.0-0:
    version = 409.0-0
    buildVariant = Linux-2.6c2.5-x86_64
    majorVersion = 409
    packageName = CPAN_MailTools



OCF.Apache:
    httpExternalRegularPort = "4080"
    httpExternalSecurePort = "4043"
    httpRegularPort = "4080"
    httpSecurePort = "4043"
    httpServerName = "chaten-1.desktop.amazon.com"

OCF.EnvironmentPreferences:
    ActivateConcurrentHosts = ""
    ActivateDelay = ""
    ExternalReleaseManager = "ApolloBounceReleaseManager"
    ExternalReleaseManagerActivityParameters = "concurrentHostsPercentage=50;stripeAcrossDC=true"
    ExternalReleaseManagerDefaultAction = "Bounce"
    Force32bitPackageVariants = "false"
    ZeroTouchController = ""

OCF.HttpKeytabRetriever:
    WebServiceURL = "kerberos.amazon.com"
    httpServerName = "chaten-1.desktop.amazon.com"
    servicePrincipalName = ""

OCF.ProcessManager:
    overrideFile = "OctaneProcessManagerOverrides.cfg"

OCF.PubSub:
    domain = "test"
    realm = "USAmazon"

OCF.Tomcat:
    JMXPort = "11099"
    accessLogFileDateFormat = "yyyy-MM-dd"
    accessLogPattern = "common"
    ajpPort = ""
    connectionTimeout = "60000"
    docBase = "ROOT"
    forceShutdownTimeout = "10"
    keyAlias = "tomcat"
    keystoreFile = "../keystore.jks"
    keystorePass = "amazon"
    keystoreType = "JKS"
    managementPort = ""
    maxKeepAliveRequests = "100"
    maxMemoryUsageMB = "256" ""
    maxPostSize = "2097152"
    maxRequestQueueSize = "10"
    maxSpareThreads = "50"
    maxThreads = "200"
    minSpareThreads = "4"
    minimumFreeMemory = "128"
    regularPort = "4080"
    securePort = "4043"
    secureRedirectPort = "4043"
    shutdownPort = "-1"
    startupHealthcheckPath = "/"
    startupHealthcheckTimeout = "10"

OCF.ZeroTouchVipConfig:
    bindOnActivate = "true"
    contactInfo = "nobody@amazon.com"
    disableOnActivate = "false"
    disableOnDeactivate = "false"
    failDeploymentOnError = "true"
    forkEnable = "false"
    generateHostPortsScript = ""
    generateLbSetNameScript = ""
    generateVipIpScript = ""
    generateVipPortPairsScript = ""
    healthCheckScript = "http_ping_health_check_script"
    hostPorts = "4080,4043"
    initialBindOnly = "false"
    maxConnections = ""
    maxQlbRetries = "5"
    maxServiceHealthChecks = "60"
    maxVipHealthChecks = "17"
    minimumHealthyPercent = "0"
    noNetVipChange = "false"
    notificationType = "email"
    remedyAggregate = "false"
    shortNamePrefix = ""
    unbindOnDeactivate = "true"
    vipPortPairs = ""

OCF.LogPuller:
    logName = "rtm-query-log"
    rtm-query-log.action = "push"
    rtm-query-log.frequency = "hourly"
    rtm-query-log.timezone = "US/Pacific"
    rtm-query-log.diskRetention = "7"
    rtm-query-log.archiveRetention = "0"



configurations:
    setList = "Apache" "EnvironmentPreferences" "HttpKeytabRetriever" "ProcessManager" "PubSub" "Tomcat" "ZeroTouchVipConfig" "LogPuller" `
