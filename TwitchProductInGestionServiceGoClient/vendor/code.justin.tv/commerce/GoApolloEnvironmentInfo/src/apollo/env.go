package apollo

//go:generate mkdir -p ../../../apollo
//go:generate cp env.go manifest.go ../../../apollo
//go:generate perl -i -pe "s/package apollo/package apollo/;" ../../../apollo/env.go ../../../apollo/manifest.go

import (
	"bytes"
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const (
	apolloParentPath   = "Apollo/Primary"
	apolloManifestPath = "Apollo/Manifest"
)

type Environment struct {
	root string //The root of the environment. "" means autodetect
}

// Gets the current Apollo environment
func CurEnvironment() *Environment {
	return NewEnvironment("")
}

// Gets the Apollo environment located at the specified path.
func NewEnvironment(path string) *Environment {
	return &Environment{
		root: path,
	}
}

// Gets the root path of this environment
func (e *Environment) Root() (string, error) {
	if e.root == "" {
		if root, err := getEnvironmentRoot(); err != nil {
			return "", err
		} else {
			e.root = root
		}
	}
	return e.root, nil
}

// Determines if this environment is a consumed environment.
func (e *Environment) IsConsumed() (bool, error) {
	p, err := e.PrimaryEnvironment()
	return e != p, err
}

// Gets the primary Environment. If e is not consumed (see IsConsumed), e itself is returned with nil error.
func (e *Environment) PrimaryEnvironment() (*Environment, error) {
	root, err := e.Root()
	if err != nil {
		return nil, err
	}
	p := filepath.Join(root, apolloParentPath)
	// If Symlink doesn't exist the current environment is the primary environment.
	if _, err = os.Lstat(filepath.FromSlash(p)); os.IsNotExist(err) {
		return e, nil
	} else if err != nil {
		return nil, err
	}
	return NewEnvironment(p), nil
}

// Gets the environment's Apollo manifest.
func (e *Environment) Manifest() (Manifest, error) {
	root, err := e.Root()
	if err != nil {
		return nil, err
	}
	file, err := os.Open(filepath.FromSlash(filepath.Join(root, apolloManifestPath)))
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return NewManifest(file)
}

func getEnvironmentRoot() (string, error) {
	root := os.Getenv("ENVROOT")
	if root != "" {
		return root, nil
	}

	root = os.Getenv("APOLLO_ACTUAL_ENVIRONMENT_ROOT")
	if root != "" {
		return root, nil
	}

	root, err := autodetectEnvroot()
	if err != nil {
		return "", err
	}
	os.Setenv("ENVROOT", root)
	return root, nil
}

func autodetectEnvroot() (string, error) {
	dir, err := exec.LookPath(os.Args[0])
	if err != nil {
		return "", err
	}
	locations := []string{}
	locations = append(locations, getSearchLocations(dir, ".envroot")...)
	locations = append(locations, getSearchLocations(dir, apolloManifestPath)...)
	location := getFirstExtantFile(locations)
	if location == "" {
		errorMsg := bytes.NewBufferString("")
		errorMsg.WriteString("Unable to detect environment root.\n")
		errorMsg.WriteString("Current executable name: " + os.Args[0] + "\n")
		errorMsg.WriteString("Detected executable location: " + dir + "\n")
		errorMsg.WriteString("Searched for the following files (in this order):\n")
		for _, file := range locations {
			errorMsg.WriteString("   " + file + "\n")
		}
		errorMsg.WriteString("For more information, see https://w.amazon.com/index.php/BrazilBuildSystem/Tools/Envroot\n")
		errorMsg.WriteString("Could not determine environment root. Please set the ENVROOT environment variable\n")
		return "", errors.New(errorMsg.String())
	}
	envroot := strings.TrimSuffix(strings.TrimSuffix(location, "/.envroot"), "/Apollo/Manifest")
	return envroot, nil
}

func getSearchLocations(dir string, file string) (ret []string) {
	for currentDir := dir; moreDirectoriesToVisit(currentDir); currentDir = filepath.Dir(currentDir) {
		ret = append(ret, filepath.Join(currentDir, file))
	}
	return
}

func moreDirectoriesToVisit(path string) bool {
	return path != filepath.Dir(path)
}

func getFirstExtantFile(locations []string) string {
	for _, location := range locations {
		if _, err := os.Stat(location); err == nil {
			return location
		}
	}
	return ""
}
