package com_amazon_adg_submission_model

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//Provides capabilities around creating and manipulating ADG product submissions.
//Submitted products can then be ingested or published by other ADG or custom systems.
type ADGSubmissionServiceClient struct {
	C __client__.Client
}

//Creates a new ADGSubmissionServiceClient
func NewADGSubmissionServiceClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *ADGSubmissionServiceClient) {
	return &ADGSubmissionServiceClient{__client__.NewClient("com.amazon.adg.submission.model", "ADGSubmissionService", dialer, codec)}
}

//Create an ADG product.
func (this *ADGSubmissionServiceClient) CreateProducts(input CreateProductsRequest) (CreateProductsResponse, error) {
	var output CreateProductsResponse
	err := this.C.Call("com.amazon.adg.submission.model", "createProducts", input, &output)
	return output, err
}

//Create an ADG product domain.
func (this *ADGSubmissionServiceClient) CreateDomains(input CreateDomainsRequest) (CreateDomainsResponse, error) {
	var output CreateDomainsResponse
	err := this.C.Call("com.amazon.adg.submission.model", "createDomains", input, &output)
	return output, err
}

//Retrieves the products for a vendor's domains.
func (this *ADGSubmissionServiceClient) GetDomainProducts(input GetDomainProductsRequest) (GetDomainProductsResponse, error) {
	var output GetDomainProductsResponse
	err := this.C.Call("com.amazon.adg.submission.model", "getDomainProducts", input, &output)
	return output, err
}

//Save a product release document.
func (this *ADGSubmissionServiceClient) SaveProductRelease(input SaveProductReleaseRequest) (SaveProductReleaseResponse, error) {
	var output SaveProductReleaseResponse
	err := this.C.Call("com.amazon.adg.submission.model", "saveProductRelease", input, &output)
	return output, err
}

//Submits a product release.
func (this *ADGSubmissionServiceClient) SubmitProductRelease(input SubmitProductReleaseRequest) (SubmitProductReleaseResponse, error) {
	var output SubmitProductReleaseResponse
	err := this.C.Call("com.amazon.adg.submission.model", "submitProductRelease", input, &output)
	return output, err
}

//Get the product release document for a product.
func (this *ADGSubmissionServiceClient) GetProductReleases(input GetProductReleasesRequest) (GetProductReleasesResponse, error) {
	var output GetProductReleasesResponse
	err := this.C.Call("com.amazon.adg.submission.model", "getProductReleases", input, &output)
	return output, err
}

//Retrieves specific products for a vendor.
func (this *ADGSubmissionServiceClient) GetProducts(input GetProductsRequest) (GetProductsResponse, error) {
	var output GetProductsResponse
	err := this.C.Call("com.amazon.adg.submission.model", "getProducts", input, &output)
	return output, err
}

//Retrieves the vendor's domain tree.
func (this *ADGSubmissionServiceClient) GetVendorDomainTree(input GetVendorDomainTreeRequest) (GetVendorDomainTreeResponse, error) {
	var output GetVendorDomainTreeResponse
	err := this.C.Call("com.amazon.adg.submission.model", "getVendorDomainTree", input, &output)
	return output, err
}

//Retrieves the specified domains for a vendor.
func (this *ADGSubmissionServiceClient) GetVendorDomains(input GetVendorDomainsRequest) (GetVendorDomainsResponse, error) {
	var output GetVendorDomainsResponse
	err := this.C.Call("com.amazon.adg.submission.model", "getVendorDomains", input, &output)
	return output, err
}
