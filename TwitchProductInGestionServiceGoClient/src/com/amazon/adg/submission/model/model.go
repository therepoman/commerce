package com_amazon_adg_submission_model

import (
	com_amazon_adg_common_model "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/adg/common/model"
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val []byte
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []byte
		if f, ok := from.Interface().([]byte); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Blob")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DocumentPayload")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type GetDomainProductsRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetDomains(v []com_amazon_adg_common_model.Domain)
	Domains() []com_amazon_adg_common_model.Domain
}
type _GetDomainProductsRequest struct {
	Ị_vendor  com_amazon_adg_common_model.Vendor   `coral:"vendor" json:"vendor"`
	Ị_domains []com_amazon_adg_common_model.Domain `coral:"domains" json:"domains"`
}

func (this *_GetDomainProductsRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_GetDomainProductsRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_GetDomainProductsRequest) Domains() []com_amazon_adg_common_model.Domain {
	return this.Ị_domains
}
func (this *_GetDomainProductsRequest) SetDomains(v []com_amazon_adg_common_model.Domain) {
	this.Ị_domains = v
}
func (this *_GetDomainProductsRequest) __type() {
}
func NewGetDomainProductsRequest() GetDomainProductsRequest {
	return &_GetDomainProductsRequest{}
}
func init() {
	var val GetDomainProductsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetDomainProductsRequest", t, func() interface{} {
		return NewGetDomainProductsRequest()
	})
}

type GetProductReleasesRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetProductReleases(v []ProductRelease)
	ProductReleases() []ProductRelease
}
type _GetProductReleasesRequest struct {
	Ị_productReleases []ProductRelease                   `coral:"productReleases" json:"productReleases"`
	Ị_vendor          com_amazon_adg_common_model.Vendor `coral:"vendor" json:"vendor"`
}

func (this *_GetProductReleasesRequest) ProductReleases() []ProductRelease {
	return this.Ị_productReleases
}
func (this *_GetProductReleasesRequest) SetProductReleases(v []ProductRelease) {
	this.Ị_productReleases = v
}
func (this *_GetProductReleasesRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_GetProductReleasesRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_GetProductReleasesRequest) __type() {
}
func NewGetProductReleasesRequest() GetProductReleasesRequest {
	return &_GetProductReleasesRequest{}
}
func init() {
	var val GetProductReleasesRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetProductReleasesRequest", t, func() interface{} {
		return NewGetProductReleasesRequest()
	})
}

//This exception is thrown on another service or resource this service depends on has an error
type DependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	ServiceException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

type HttpRequest interface {
	__type()
	SetHeaders(v map[string][]*string)
	Headers() map[string][]*string
	SetBody(v []byte)
	Body() []byte
	SetUriPath(v *string)
	UriPath() *string
	SetQueryStringMap(v map[string]*string)
	QueryStringMap() map[string]*string
	SetTenantId(v *string)
	TenantId() *string
}
type _HttpRequest struct {
	Ị_body           []byte               `coral:"body" json:"body"`
	Ị_headers        map[string][]*string `coral:"headers" json:"headers"`
	Ị_queryStringMap map[string]*string   `coral:"queryStringMap" json:"queryStringMap"`
	Ị_tenantId       *string              `coral:"tenantId" json:"tenantId"`
	Ị_uriPath        *string              `coral:"uriPath" json:"uriPath"`
}

func (this *_HttpRequest) Body() []byte {
	return this.Ị_body
}
func (this *_HttpRequest) SetBody(v []byte) {
	this.Ị_body = v
}
func (this *_HttpRequest) Headers() map[string][]*string {
	return this.Ị_headers
}
func (this *_HttpRequest) SetHeaders(v map[string][]*string) {
	this.Ị_headers = v
}
func (this *_HttpRequest) QueryStringMap() map[string]*string {
	return this.Ị_queryStringMap
}
func (this *_HttpRequest) SetQueryStringMap(v map[string]*string) {
	this.Ị_queryStringMap = v
}
func (this *_HttpRequest) TenantId() *string {
	return this.Ị_tenantId
}
func (this *_HttpRequest) SetTenantId(v *string) {
	this.Ị_tenantId = v
}
func (this *_HttpRequest) UriPath() *string {
	return this.Ị_uriPath
}
func (this *_HttpRequest) SetUriPath(v *string) {
	this.Ị_uriPath = v
}
func (this *_HttpRequest) __type() {
}
func NewHttpRequest() HttpRequest {
	return &_HttpRequest{}
}
func init() {
	var val HttpRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("HttpRequest", t, func() interface{} {
		return NewHttpRequest()
	})
}

type GetProductsRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetProducts(v []SubmissionProduct)
	Products() []SubmissionProduct
}
type _GetProductsRequest struct {
	Ị_vendor   com_amazon_adg_common_model.Vendor `coral:"vendor" json:"vendor"`
	Ị_products []SubmissionProduct                `coral:"products" json:"products"`
}

func (this *_GetProductsRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_GetProductsRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_GetProductsRequest) Products() []SubmissionProduct {
	return this.Ị_products
}
func (this *_GetProductsRequest) SetProducts(v []SubmissionProduct) {
	this.Ị_products = v
}
func (this *_GetProductsRequest) __type() {
}
func NewGetProductsRequest() GetProductsRequest {
	return &_GetProductsRequest{}
}
func init() {
	var val GetProductsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetProductsRequest", t, func() interface{} {
		return NewGetProductsRequest()
	})
}

type GetVendorDomainsResponse interface {
	__type()
	SetDomains(v []com_amazon_adg_common_model.Domain)
	Domains() []com_amazon_adg_common_model.Domain
}
type _GetVendorDomainsResponse struct {
	Ị_domains []com_amazon_adg_common_model.Domain `coral:"domains" json:"domains"`
}

func (this *_GetVendorDomainsResponse) Domains() []com_amazon_adg_common_model.Domain {
	return this.Ị_domains
}
func (this *_GetVendorDomainsResponse) SetDomains(v []com_amazon_adg_common_model.Domain) {
	this.Ị_domains = v
}
func (this *_GetVendorDomainsResponse) __type() {
}
func NewGetVendorDomainsResponse() GetVendorDomainsResponse {
	return &_GetVendorDomainsResponse{}
}
func init() {
	var val GetVendorDomainsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetVendorDomainsResponse", t, func() interface{} {
		return NewGetVendorDomainsResponse()
	})
}

//The client has submitted a request that is invalid, either with bad parameters, missing parameters,
//or unrecognized parameters.
type InvalidRequestException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidRequestException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InvalidRequestException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidRequestException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidRequestException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InvalidRequestException) __type() {
}
func NewInvalidRequestException() InvalidRequestException {
	return &_InvalidRequestException{}
}
func init() {
	var val InvalidRequestException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("InvalidRequestException", t, func() interface{} {
		return NewInvalidRequestException()
	})
}

type ServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _ServiceException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_ServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ServiceException) Message() *string {
	return this.Ị_message
}
func (this *_ServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_ServiceException) __type() {
}
func NewServiceException() ServiceException {
	return &_ServiceException{}
}
func init() {
	var val ServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("ServiceException", t, func() interface{} {
		return NewServiceException()
	})
}

type SubmitProductReleaseRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetProduct(v com_amazon_adg_common_model.Product)
	Product() com_amazon_adg_common_model.Product
}
type _SubmitProductReleaseRequest struct {
	Ị_vendor  com_amazon_adg_common_model.Vendor  `coral:"vendor" json:"vendor"`
	Ị_product com_amazon_adg_common_model.Product `coral:"product" json:"product"`
}

func (this *_SubmitProductReleaseRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_SubmitProductReleaseRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_SubmitProductReleaseRequest) Product() com_amazon_adg_common_model.Product {
	return this.Ị_product
}
func (this *_SubmitProductReleaseRequest) SetProduct(v com_amazon_adg_common_model.Product) {
	this.Ị_product = v
}
func (this *_SubmitProductReleaseRequest) __type() {
}
func NewSubmitProductReleaseRequest() SubmitProductReleaseRequest {
	return &_SubmitProductReleaseRequest{}
}
func init() {
	var val SubmitProductReleaseRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("SubmitProductReleaseRequest", t, func() interface{} {
		return NewSubmitProductReleaseRequest()
	})
}

type CreateProductsResponse interface {
	__type()
	SetNewProducts(v []SubmissionProduct)
	NewProducts() []SubmissionProduct
	SetErrors(v []ProductError)
	Errors() []ProductError
}
type _CreateProductsResponse struct {
	Ị_newProducts []SubmissionProduct `coral:"newProducts" json:"newProducts"`
	Ị_errors      []ProductError      `coral:"errors" json:"errors"`
}

func (this *_CreateProductsResponse) NewProducts() []SubmissionProduct {
	return this.Ị_newProducts
}
func (this *_CreateProductsResponse) SetNewProducts(v []SubmissionProduct) {
	this.Ị_newProducts = v
}
func (this *_CreateProductsResponse) Errors() []ProductError {
	return this.Ị_errors
}
func (this *_CreateProductsResponse) SetErrors(v []ProductError) {
	this.Ị_errors = v
}
func (this *_CreateProductsResponse) __type() {
}
func NewCreateProductsResponse() CreateProductsResponse {
	return &_CreateProductsResponse{}
}
func init() {
	var val CreateProductsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("CreateProductsResponse", t, func() interface{} {
		return NewCreateProductsResponse()
	})
}

type CreateDomainsResponse interface {
	__type()
	SetNewDomains(v []com_amazon_adg_common_model.Domain)
	NewDomains() []com_amazon_adg_common_model.Domain
	SetErrors(v []DomainError)
	Errors() []DomainError
}
type _CreateDomainsResponse struct {
	Ị_newDomains []com_amazon_adg_common_model.Domain `coral:"newDomains" json:"newDomains"`
	Ị_errors     []DomainError                        `coral:"errors" json:"errors"`
}

func (this *_CreateDomainsResponse) NewDomains() []com_amazon_adg_common_model.Domain {
	return this.Ị_newDomains
}
func (this *_CreateDomainsResponse) SetNewDomains(v []com_amazon_adg_common_model.Domain) {
	this.Ị_newDomains = v
}
func (this *_CreateDomainsResponse) Errors() []DomainError {
	return this.Ị_errors
}
func (this *_CreateDomainsResponse) SetErrors(v []DomainError) {
	this.Ị_errors = v
}
func (this *_CreateDomainsResponse) __type() {
}
func NewCreateDomainsResponse() CreateDomainsResponse {
	return &_CreateDomainsResponse{}
}
func init() {
	var val CreateDomainsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("CreateDomainsResponse", t, func() interface{} {
		return NewCreateDomainsResponse()
	})
}

type GetProductsResponse interface {
	__type()
	SetProducts(v []SubmissionProduct)
	Products() []SubmissionProduct
}
type _GetProductsResponse struct {
	Ị_products []SubmissionProduct `coral:"products" json:"products"`
}

func (this *_GetProductsResponse) Products() []SubmissionProduct {
	return this.Ị_products
}
func (this *_GetProductsResponse) SetProducts(v []SubmissionProduct) {
	this.Ị_products = v
}
func (this *_GetProductsResponse) __type() {
}
func NewGetProductsResponse() GetProductsResponse {
	return &_GetProductsResponse{}
}
func init() {
	var val GetProductsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetProductsResponse", t, func() interface{} {
		return NewGetProductsResponse()
	})
}

type GetDomainProductsResponse interface {
	__type()
	SetProducts(v []com_amazon_adg_common_model.Product)
	Products() []com_amazon_adg_common_model.Product
}
type _GetDomainProductsResponse struct {
	Ị_products []com_amazon_adg_common_model.Product `coral:"products" json:"products"`
}

func (this *_GetDomainProductsResponse) Products() []com_amazon_adg_common_model.Product {
	return this.Ị_products
}
func (this *_GetDomainProductsResponse) SetProducts(v []com_amazon_adg_common_model.Product) {
	this.Ị_products = v
}
func (this *_GetDomainProductsResponse) __type() {
}
func NewGetDomainProductsResponse() GetDomainProductsResponse {
	return &_GetDomainProductsResponse{}
}
func init() {
	var val GetDomainProductsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetDomainProductsResponse", t, func() interface{} {
		return NewGetDomainProductsResponse()
	})
}

type GetVendorDomainsRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetDomains(v []com_amazon_adg_common_model.Domain)
	Domains() []com_amazon_adg_common_model.Domain
}
type _GetVendorDomainsRequest struct {
	Ị_domains []com_amazon_adg_common_model.Domain `coral:"domains" json:"domains"`
	Ị_vendor  com_amazon_adg_common_model.Vendor   `coral:"vendor" json:"vendor"`
}

func (this *_GetVendorDomainsRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_GetVendorDomainsRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_GetVendorDomainsRequest) Domains() []com_amazon_adg_common_model.Domain {
	return this.Ị_domains
}
func (this *_GetVendorDomainsRequest) SetDomains(v []com_amazon_adg_common_model.Domain) {
	this.Ị_domains = v
}
func (this *_GetVendorDomainsRequest) __type() {
}
func NewGetVendorDomainsRequest() GetVendorDomainsRequest {
	return &_GetVendorDomainsRequest{}
}
func init() {
	var val GetVendorDomainsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetVendorDomainsRequest", t, func() interface{} {
		return NewGetVendorDomainsRequest()
	})
}

type ProductRelease interface {
	__type()
	SetId(v *string)
	Id() *string
	SetReleaseDocument(v *string)
	ReleaseDocument() *string
}
type _ProductRelease struct {
	Ị_id              *string `coral:"id" json:"id"`
	Ị_releaseDocument *string `coral:"releaseDocument" json:"releaseDocument"`
}

func (this *_ProductRelease) Id() *string {
	return this.Ị_id
}
func (this *_ProductRelease) SetId(v *string) {
	this.Ị_id = v
}
func (this *_ProductRelease) ReleaseDocument() *string {
	return this.Ị_releaseDocument
}
func (this *_ProductRelease) SetReleaseDocument(v *string) {
	this.Ị_releaseDocument = v
}
func (this *_ProductRelease) __type() {
}
func NewProductRelease() ProductRelease {
	return &_ProductRelease{}
}
func init() {
	var val ProductRelease
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("ProductRelease", t, func() interface{} {
		return NewProductRelease()
	})
}

type CreateProductsRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetNewProducts(v []SubmissionProduct)
	NewProducts() []SubmissionProduct
}
type _CreateProductsRequest struct {
	Ị_vendor      com_amazon_adg_common_model.Vendor `coral:"vendor" json:"vendor"`
	Ị_newProducts []SubmissionProduct                `coral:"newProducts" json:"newProducts"`
}

func (this *_CreateProductsRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_CreateProductsRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_CreateProductsRequest) NewProducts() []SubmissionProduct {
	return this.Ị_newProducts
}
func (this *_CreateProductsRequest) SetNewProducts(v []SubmissionProduct) {
	this.Ị_newProducts = v
}
func (this *_CreateProductsRequest) __type() {
}
func NewCreateProductsRequest() CreateProductsRequest {
	return &_CreateProductsRequest{}
}
func init() {
	var val CreateProductsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("CreateProductsRequest", t, func() interface{} {
		return NewCreateProductsRequest()
	})
}

type SaveProductReleaseRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetProduct(v com_amazon_adg_common_model.Product)
	Product() com_amazon_adg_common_model.Product
	SetProductRelease(v ProductRelease)
	ProductRelease() ProductRelease
}
type _SaveProductReleaseRequest struct {
	Ị_product        com_amazon_adg_common_model.Product `coral:"product" json:"product"`
	Ị_productRelease ProductRelease                      `coral:"productRelease" json:"productRelease"`
	Ị_vendor         com_amazon_adg_common_model.Vendor  `coral:"vendor" json:"vendor"`
}

func (this *_SaveProductReleaseRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_SaveProductReleaseRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_SaveProductReleaseRequest) Product() com_amazon_adg_common_model.Product {
	return this.Ị_product
}
func (this *_SaveProductReleaseRequest) SetProduct(v com_amazon_adg_common_model.Product) {
	this.Ị_product = v
}
func (this *_SaveProductReleaseRequest) ProductRelease() ProductRelease {
	return this.Ị_productRelease
}
func (this *_SaveProductReleaseRequest) SetProductRelease(v ProductRelease) {
	this.Ị_productRelease = v
}
func (this *_SaveProductReleaseRequest) __type() {
}
func NewSaveProductReleaseRequest() SaveProductReleaseRequest {
	return &_SaveProductReleaseRequest{}
}
func init() {
	var val SaveProductReleaseRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("SaveProductReleaseRequest", t, func() interface{} {
		return NewSaveProductReleaseRequest()
	})
}

type GetVendorDomainTreeRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
}
type _GetVendorDomainTreeRequest struct {
	Ị_vendor com_amazon_adg_common_model.Vendor `coral:"vendor" json:"vendor"`
}

func (this *_GetVendorDomainTreeRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_GetVendorDomainTreeRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_GetVendorDomainTreeRequest) __type() {
}
func NewGetVendorDomainTreeRequest() GetVendorDomainTreeRequest {
	return &_GetVendorDomainTreeRequest{}
}
func init() {
	var val GetVendorDomainTreeRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetVendorDomainTreeRequest", t, func() interface{} {
		return NewGetVendorDomainTreeRequest()
	})
}

type GetProductReleasesResponse interface {
	__type()
	SetProductReleases(v []ProductRelease)
	ProductReleases() []ProductRelease
}
type _GetProductReleasesResponse struct {
	Ị_productReleases []ProductRelease `coral:"productReleases" json:"productReleases"`
}

func (this *_GetProductReleasesResponse) ProductReleases() []ProductRelease {
	return this.Ị_productReleases
}
func (this *_GetProductReleasesResponse) SetProductReleases(v []ProductRelease) {
	this.Ị_productReleases = v
}
func (this *_GetProductReleasesResponse) __type() {
}
func NewGetProductReleasesResponse() GetProductReleasesResponse {
	return &_GetProductReleasesResponse{}
}
func init() {
	var val GetProductReleasesResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetProductReleasesResponse", t, func() interface{} {
		return NewGetProductReleasesResponse()
	})
}

type HttpResponse interface {
	__type()
	SetBody(v []byte)
	Body() []byte
	SetHeaders(v map[string][]*string)
	Headers() map[string][]*string
}
type _HttpResponse struct {
	Ị_body    []byte               `coral:"body" json:"body"`
	Ị_headers map[string][]*string `coral:"headers" json:"headers"`
}

func (this *_HttpResponse) Body() []byte {
	return this.Ị_body
}
func (this *_HttpResponse) SetBody(v []byte) {
	this.Ị_body = v
}
func (this *_HttpResponse) Headers() map[string][]*string {
	return this.Ị_headers
}
func (this *_HttpResponse) SetHeaders(v map[string][]*string) {
	this.Ị_headers = v
}
func (this *_HttpResponse) __type() {
}
func NewHttpResponse() HttpResponse {
	return &_HttpResponse{}
}
func init() {
	var val HttpResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("HttpResponse", t, func() interface{} {
		return NewHttpResponse()
	})
}

type CreateDomainsRequest interface {
	__type()
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetParentDomain(v com_amazon_adg_common_model.Domain)
	ParentDomain() com_amazon_adg_common_model.Domain
	SetNewDomains(v []com_amazon_adg_common_model.Domain)
	NewDomains() []com_amazon_adg_common_model.Domain
}
type _CreateDomainsRequest struct {
	Ị_vendor       com_amazon_adg_common_model.Vendor   `coral:"vendor" json:"vendor"`
	Ị_parentDomain com_amazon_adg_common_model.Domain   `coral:"parentDomain" json:"parentDomain"`
	Ị_newDomains   []com_amazon_adg_common_model.Domain `coral:"newDomains" json:"newDomains"`
}

func (this *_CreateDomainsRequest) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_CreateDomainsRequest) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_CreateDomainsRequest) ParentDomain() com_amazon_adg_common_model.Domain {
	return this.Ị_parentDomain
}
func (this *_CreateDomainsRequest) SetParentDomain(v com_amazon_adg_common_model.Domain) {
	this.Ị_parentDomain = v
}
func (this *_CreateDomainsRequest) NewDomains() []com_amazon_adg_common_model.Domain {
	return this.Ị_newDomains
}
func (this *_CreateDomainsRequest) SetNewDomains(v []com_amazon_adg_common_model.Domain) {
	this.Ị_newDomains = v
}
func (this *_CreateDomainsRequest) __type() {
}
func NewCreateDomainsRequest() CreateDomainsRequest {
	return &_CreateDomainsRequest{}
}
func init() {
	var val CreateDomainsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("CreateDomainsRequest", t, func() interface{} {
		return NewCreateDomainsRequest()
	})
}

type SaveProductReleaseResponse interface {
	__type()
	SetProductRelease(v ProductRelease)
	ProductRelease() ProductRelease
}
type _SaveProductReleaseResponse struct {
	Ị_productRelease ProductRelease `coral:"productRelease" json:"productRelease"`
}

func (this *_SaveProductReleaseResponse) ProductRelease() ProductRelease {
	return this.Ị_productRelease
}
func (this *_SaveProductReleaseResponse) SetProductRelease(v ProductRelease) {
	this.Ị_productRelease = v
}
func (this *_SaveProductReleaseResponse) __type() {
}
func NewSaveProductReleaseResponse() SaveProductReleaseResponse {
	return &_SaveProductReleaseResponse{}
}
func init() {
	var val SaveProductReleaseResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("SaveProductReleaseResponse", t, func() interface{} {
		return NewSaveProductReleaseResponse()
	})
}

type SubmissionProduct interface {
	__type()
	SetProductDomain(v com_amazon_adg_common_model.Domain)
	ProductDomain() com_amazon_adg_common_model.Domain
	SetId(v *string)
	Id() *string
	SetSku(v *string)
	Sku() *string
	SetAsin(v *string)
	Asin() *string
	SetState(v *string)
	State() *string
	SetProductTitle(v *string)
	ProductTitle() *string
	SetProductLine(v *string)
	ProductLine() *string
	SetVendor(v com_amazon_adg_common_model.Vendor)
	Vendor() com_amazon_adg_common_model.Vendor
	SetAsinVersion(v *int32)
	AsinVersion() *int32
	SetType(v *string)
	Type() *string
	SetProductDescription(v *string)
	ProductDescription() *string
	SetProductDetails(v com_amazon_adg_common_model.ProductDetail)
	ProductDetails() com_amazon_adg_common_model.ProductDetail
	SetOffers(v []com_amazon_adg_common_model.Offer)
	Offers() []com_amazon_adg_common_model.Offer
	SetExtendedData(v *string)
	ExtendedData() *string
}
type _SubmissionProduct struct {
	Ị_vendor             com_amazon_adg_common_model.Vendor        `coral:"vendor" json:"vendor"`
	Ị_asinVersion        *int32                                    `coral:"asinVersion" json:"asinVersion"`
	Ị_type               *string                                   `coral:"type" json:"type"`
	Ị_productDescription *string                                   `coral:"productDescription" json:"productDescription"`
	Ị_productDetails     com_amazon_adg_common_model.ProductDetail `coral:"productDetails" json:"productDetails"`
	Ị_offers             []com_amazon_adg_common_model.Offer       `coral:"offers" json:"offers"`
	Ị_id                 *string                                   `coral:"id" json:"id"`
	Ị_sku                *string                                   `coral:"sku" json:"sku"`
	Ị_asin               *string                                   `coral:"asin" json:"asin"`
	Ị_state              *string                                   `coral:"state" json:"state"`
	Ị_productTitle       *string                                   `coral:"productTitle" json:"productTitle"`
	Ị_productLine        *string                                   `coral:"productLine" json:"productLine"`
	Ị_productDomain      com_amazon_adg_common_model.Domain        `coral:"productDomain" json:"productDomain"`
	Ị_extendedData       *string                                   `coral:"extendedData" json:"extendedData"`
}

func (this *_SubmissionProduct) ProductTitle() *string {
	return this.Ị_productTitle
}
func (this *_SubmissionProduct) SetProductTitle(v *string) {
	this.Ị_productTitle = v
}
func (this *_SubmissionProduct) ProductLine() *string {
	return this.Ị_productLine
}
func (this *_SubmissionProduct) SetProductLine(v *string) {
	this.Ị_productLine = v
}
func (this *_SubmissionProduct) ProductDomain() com_amazon_adg_common_model.Domain {
	return this.Ị_productDomain
}
func (this *_SubmissionProduct) SetProductDomain(v com_amazon_adg_common_model.Domain) {
	this.Ị_productDomain = v
}
func (this *_SubmissionProduct) Id() *string {
	return this.Ị_id
}
func (this *_SubmissionProduct) SetId(v *string) {
	this.Ị_id = v
}
func (this *_SubmissionProduct) Sku() *string {
	return this.Ị_sku
}
func (this *_SubmissionProduct) SetSku(v *string) {
	this.Ị_sku = v
}
func (this *_SubmissionProduct) Asin() *string {
	return this.Ị_asin
}
func (this *_SubmissionProduct) SetAsin(v *string) {
	this.Ị_asin = v
}
func (this *_SubmissionProduct) State() *string {
	return this.Ị_state
}
func (this *_SubmissionProduct) SetState(v *string) {
	this.Ị_state = v
}
func (this *_SubmissionProduct) ProductDetails() com_amazon_adg_common_model.ProductDetail {
	return this.Ị_productDetails
}
func (this *_SubmissionProduct) SetProductDetails(v com_amazon_adg_common_model.ProductDetail) {
	this.Ị_productDetails = v
}
func (this *_SubmissionProduct) Offers() []com_amazon_adg_common_model.Offer {
	return this.Ị_offers
}
func (this *_SubmissionProduct) SetOffers(v []com_amazon_adg_common_model.Offer) {
	this.Ị_offers = v
}
func (this *_SubmissionProduct) Vendor() com_amazon_adg_common_model.Vendor {
	return this.Ị_vendor
}
func (this *_SubmissionProduct) SetVendor(v com_amazon_adg_common_model.Vendor) {
	this.Ị_vendor = v
}
func (this *_SubmissionProduct) AsinVersion() *int32 {
	return this.Ị_asinVersion
}
func (this *_SubmissionProduct) SetAsinVersion(v *int32) {
	this.Ị_asinVersion = v
}
func (this *_SubmissionProduct) Type() *string {
	return this.Ị_type
}
func (this *_SubmissionProduct) SetType(v *string) {
	this.Ị_type = v
}
func (this *_SubmissionProduct) ProductDescription() *string {
	return this.Ị_productDescription
}
func (this *_SubmissionProduct) SetProductDescription(v *string) {
	this.Ị_productDescription = v
}
func (this *_SubmissionProduct) ExtendedData() *string {
	return this.Ị_extendedData
}
func (this *_SubmissionProduct) SetExtendedData(v *string) {
	this.Ị_extendedData = v
}
func (this *_SubmissionProduct) __type() {
}
func NewSubmissionProduct() SubmissionProduct {
	return &_SubmissionProduct{}
}
func init() {
	var val SubmissionProduct
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("SubmissionProduct", t, func() interface{} {
		return NewSubmissionProduct()
	})
}

type HttpMessage interface {
	__type()
	SetBody(v []byte)
	Body() []byte
	SetHeaders(v map[string][]*string)
	Headers() map[string][]*string
}
type _HttpMessage struct {
	Ị_headers map[string][]*string `coral:"headers" json:"headers"`
	Ị_body    []byte               `coral:"body" json:"body"`
}

func (this *_HttpMessage) Body() []byte {
	return this.Ị_body
}
func (this *_HttpMessage) SetBody(v []byte) {
	this.Ị_body = v
}
func (this *_HttpMessage) Headers() map[string][]*string {
	return this.Ị_headers
}
func (this *_HttpMessage) SetHeaders(v map[string][]*string) {
	this.Ị_headers = v
}
func (this *_HttpMessage) __type() {
}
func NewHttpMessage() HttpMessage {
	return &_HttpMessage{}
}
func init() {
	var val HttpMessage
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("HttpMessage", t, func() interface{} {
		return NewHttpMessage()
	})
}

type GetVendorDomainTreeResponse interface {
	__type()
	SetDomainTree(v com_amazon_adg_common_model.DomainNode)
	DomainTree() com_amazon_adg_common_model.DomainNode
}
type _GetVendorDomainTreeResponse struct {
	Ị_domainTree com_amazon_adg_common_model.DomainNode `coral:"domainTree" json:"domainTree"`
}

func (this *_GetVendorDomainTreeResponse) DomainTree() com_amazon_adg_common_model.DomainNode {
	return this.Ị_domainTree
}
func (this *_GetVendorDomainTreeResponse) SetDomainTree(v com_amazon_adg_common_model.DomainNode) {
	this.Ị_domainTree = v
}
func (this *_GetVendorDomainTreeResponse) __type() {
}
func NewGetVendorDomainTreeResponse() GetVendorDomainTreeResponse {
	return &_GetVendorDomainTreeResponse{}
}
func init() {
	var val GetVendorDomainTreeResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("GetVendorDomainTreeResponse", t, func() interface{} {
		return NewGetVendorDomainTreeResponse()
	})
}

type SubmitProductReleaseResponse interface {
	__type()
	SetSubmissionId(v *string)
	SubmissionId() *string
}
type _SubmitProductReleaseResponse struct {
	Ị_submissionId *string `coral:"submissionId" json:"submissionId"`
}

func (this *_SubmitProductReleaseResponse) SubmissionId() *string {
	return this.Ị_submissionId
}
func (this *_SubmitProductReleaseResponse) SetSubmissionId(v *string) {
	this.Ị_submissionId = v
}
func (this *_SubmitProductReleaseResponse) __type() {
}
func NewSubmitProductReleaseResponse() SubmitProductReleaseResponse {
	return &_SubmitProductReleaseResponse{}
}
func init() {
	var val SubmitProductReleaseResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("SubmitProductReleaseResponse", t, func() interface{} {
		return NewSubmitProductReleaseResponse()
	})
}

type ProductError interface {
	__type()
	SetMessage(v *string)
	Message() *string
	SetProduct(v com_amazon_adg_common_model.Product)
	Product() com_amazon_adg_common_model.Product
}
type _ProductError struct {
	Ị_message *string                             `coral:"message" json:"message"`
	Ị_product com_amazon_adg_common_model.Product `coral:"product" json:"product"`
}

func (this *_ProductError) Message() *string {
	return this.Ị_message
}
func (this *_ProductError) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_ProductError) Product() com_amazon_adg_common_model.Product {
	return this.Ị_product
}
func (this *_ProductError) SetProduct(v com_amazon_adg_common_model.Product) {
	this.Ị_product = v
}
func (this *_ProductError) __type() {
}
func NewProductError() ProductError {
	return &_ProductError{}
}
func init() {
	var val ProductError
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("ProductError", t, func() interface{} {
		return NewProductError()
	})
}

type DomainError interface {
	__type()
	SetDomain(v com_amazon_adg_common_model.Domain)
	Domain() com_amazon_adg_common_model.Domain
	SetMessage(v *string)
	Message() *string
}
type _DomainError struct {
	Ị_message *string                            `coral:"message" json:"message"`
	Ị_domain  com_amazon_adg_common_model.Domain `coral:"domain" json:"domain"`
}

func (this *_DomainError) Message() *string {
	return this.Ị_message
}
func (this *_DomainError) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DomainError) Domain() com_amazon_adg_common_model.Domain {
	return this.Ị_domain
}
func (this *_DomainError) SetDomain(v com_amazon_adg_common_model.Domain) {
	this.Ị_domain = v
}
func (this *_DomainError) __type() {
}
func NewDomainError() DomainError {
	return &_DomainError{}
}
func init() {
	var val DomainError
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("DomainError", t, func() interface{} {
		return NewDomainError()
	})
}

//Exception for any hard errors caused by internal failures.
type InternalServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InternalServiceException struct {
	ServiceException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InternalServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InternalServiceException) Message() *string {
	return this.Ị_message
}
func (this *_InternalServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InternalServiceException) __type() {
}
func NewInternalServiceException() InternalServiceException {
	return &_InternalServiceException{}
}
func init() {
	var val InternalServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.submission.model").RegisterShape("InternalServiceException", t, func() interface{} {
		return NewInternalServiceException()
	})
}
func init() {
	var val map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*string
		if f, ok := from.Interface().(map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringToStringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string][]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string][]*string
		if f, ok := from.Interface().(map[string][]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringToStringListMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []com_amazon_adg_common_model.Domain
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []com_amazon_adg_common_model.Domain
		if f, ok := from.Interface().([]com_amazon_adg_common_model.Domain); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DomainList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []DomainError
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []DomainError
		if f, ok := from.Interface().([]DomainError); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DomainErrorList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductRelease
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductRelease
		if f, ok := from.Interface().([]ProductRelease); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductReleaseList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*int64
		if f, ok := from.Interface().([]*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []com_amazon_adg_common_model.Product
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []com_amazon_adg_common_model.Product
		if f, ok := from.Interface().([]com_amazon_adg_common_model.Product); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []SubmissionProduct
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []SubmissionProduct
		if f, ok := from.Interface().([]SubmissionProduct); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SubmissionProductList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductError
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductError
		if f, ok := from.Interface().([]ProductError); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductErrorList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//Provides capabilities around creating and manipulating ADG product submissions.
//Submitted products can then be ingested or published by other ADG or custom systems.
type ADGSubmissionService interface { //Create an ADG product.
	CreateProducts(input CreateProductsRequest) (CreateProductsResponse, error)                   //Create an ADG product domain.
	CreateDomains(input CreateDomainsRequest) (CreateDomainsResponse, error)                      //Retrieves the products for a vendor's domains.
	GetDomainProducts(input GetDomainProductsRequest) (GetDomainProductsResponse, error)          //Save a product release document.
	SaveProductRelease(input SaveProductReleaseRequest) (SaveProductReleaseResponse, error)       //Submits a product release.
	SubmitProductRelease(input SubmitProductReleaseRequest) (SubmitProductReleaseResponse, error) //Get the product release document for a product.
	GetProductReleases(input GetProductReleasesRequest) (GetProductReleasesResponse, error)       //Retrieves specific products for a vendor.
	GetProducts(input GetProductsRequest) (GetProductsResponse, error)                            //Retrieves the vendor's domain tree.
	GetVendorDomainTree(input GetVendorDomainTreeRequest) (GetVendorDomainTreeResponse, error)    //Retrieves the specified domains for a vendor.
	GetVendorDomains(input GetVendorDomainsRequest) (GetVendorDomainsResponse, error)
}
