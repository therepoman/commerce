package com_amazon_twitchproductingestionservice

import (
	com_amazon_adg_submission_model "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/adg/submission/model"
	com_amazon_mas_devportal_fileupload_model "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/mas/devportal/fileupload/model"
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
)

func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchUserID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type CreateDomainsRequest interface {
	__type()
	SetTuid(v *string)
	Tuid() *string
	SetAdgRequest(v com_amazon_adg_submission_model.CreateDomainsRequest)
	AdgRequest() com_amazon_adg_submission_model.CreateDomainsRequest
}
type _CreateDomainsRequest struct {
	Ị_tuid       *string                                              `coral:"tuid" json:"tuid"`
	Ị_adgRequest com_amazon_adg_submission_model.CreateDomainsRequest `coral:"adgRequest" json:"adgRequest"`
}

func (this *_CreateDomainsRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_CreateDomainsRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_CreateDomainsRequest) AdgRequest() com_amazon_adg_submission_model.CreateDomainsRequest {
	return this.Ị_adgRequest
}
func (this *_CreateDomainsRequest) SetAdgRequest(v com_amazon_adg_submission_model.CreateDomainsRequest) {
	this.Ị_adgRequest = v
}
func (this *_CreateDomainsRequest) __type() {
}
func NewCreateDomainsRequest() CreateDomainsRequest {
	return &_CreateDomainsRequest{}
}
func init() {
	var val CreateDomainsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.twitchproductingestionservice").RegisterShape("CreateDomainsRequest", t, func() interface{} {
		return NewCreateDomainsRequest()
	})
}

type CreateProductsRequest interface {
	__type()
	SetTuid(v *string)
	Tuid() *string
	SetAdgRequest(v com_amazon_adg_submission_model.CreateProductsRequest)
	AdgRequest() com_amazon_adg_submission_model.CreateProductsRequest
}
type _CreateProductsRequest struct {
	Ị_tuid       *string                                               `coral:"tuid" json:"tuid"`
	Ị_adgRequest com_amazon_adg_submission_model.CreateProductsRequest `coral:"adgRequest" json:"adgRequest"`
}

func (this *_CreateProductsRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_CreateProductsRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_CreateProductsRequest) AdgRequest() com_amazon_adg_submission_model.CreateProductsRequest {
	return this.Ị_adgRequest
}
func (this *_CreateProductsRequest) SetAdgRequest(v com_amazon_adg_submission_model.CreateProductsRequest) {
	this.Ị_adgRequest = v
}
func (this *_CreateProductsRequest) __type() {
}
func NewCreateProductsRequest() CreateProductsRequest {
	return &_CreateProductsRequest{}
}
func init() {
	var val CreateProductsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.twitchproductingestionservice").RegisterShape("CreateProductsRequest", t, func() interface{} {
		return NewCreateProductsRequest()
	})
}

type SaveProductReleaseRequest interface {
	__type()
	SetTuid(v *string)
	Tuid() *string
	SetAdgRequest(v com_amazon_adg_submission_model.SaveProductReleaseRequest)
	AdgRequest() com_amazon_adg_submission_model.SaveProductReleaseRequest
}
type _SaveProductReleaseRequest struct {
	Ị_tuid       *string                                                   `coral:"tuid" json:"tuid"`
	Ị_adgRequest com_amazon_adg_submission_model.SaveProductReleaseRequest `coral:"adgRequest" json:"adgRequest"`
}

func (this *_SaveProductReleaseRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_SaveProductReleaseRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_SaveProductReleaseRequest) AdgRequest() com_amazon_adg_submission_model.SaveProductReleaseRequest {
	return this.Ị_adgRequest
}
func (this *_SaveProductReleaseRequest) SetAdgRequest(v com_amazon_adg_submission_model.SaveProductReleaseRequest) {
	this.Ị_adgRequest = v
}
func (this *_SaveProductReleaseRequest) __type() {
}
func NewSaveProductReleaseRequest() SaveProductReleaseRequest {
	return &_SaveProductReleaseRequest{}
}
func init() {
	var val SaveProductReleaseRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.twitchproductingestionservice").RegisterShape("SaveProductReleaseRequest", t, func() interface{} {
		return NewSaveProductReleaseRequest()
	})
}

type SubmitProductReleaseRequest interface {
	__type()
	SetAdgRequest(v com_amazon_adg_submission_model.SubmitProductReleaseRequest)
	AdgRequest() com_amazon_adg_submission_model.SubmitProductReleaseRequest
	SetTuid(v *string)
	Tuid() *string
}
type _SubmitProductReleaseRequest struct {
	Ị_tuid       *string                                                     `coral:"tuid" json:"tuid"`
	Ị_adgRequest com_amazon_adg_submission_model.SubmitProductReleaseRequest `coral:"adgRequest" json:"adgRequest"`
}

func (this *_SubmitProductReleaseRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_SubmitProductReleaseRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_SubmitProductReleaseRequest) AdgRequest() com_amazon_adg_submission_model.SubmitProductReleaseRequest {
	return this.Ị_adgRequest
}
func (this *_SubmitProductReleaseRequest) SetAdgRequest(v com_amazon_adg_submission_model.SubmitProductReleaseRequest) {
	this.Ị_adgRequest = v
}
func (this *_SubmitProductReleaseRequest) __type() {
}
func NewSubmitProductReleaseRequest() SubmitProductReleaseRequest {
	return &_SubmitProductReleaseRequest{}
}
func init() {
	var val SubmitProductReleaseRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.twitchproductingestionservice").RegisterShape("SubmitProductReleaseRequest", t, func() interface{} {
		return NewSubmitProductReleaseRequest()
	})
}

type CreateUploadRequest interface {
	__type()
	SetMasRequest(v com_amazon_mas_devportal_fileupload_model.CreateUploadRequest)
	MasRequest() com_amazon_mas_devportal_fileupload_model.CreateUploadRequest
	SetTuid(v *string)
	Tuid() *string
}
type _CreateUploadRequest struct {
	Ị_tuid       *string                                                       `coral:"tuid" json:"tuid"`
	Ị_masRequest com_amazon_mas_devportal_fileupload_model.CreateUploadRequest `coral:"masRequest" json:"masRequest"`
}

func (this *_CreateUploadRequest) MasRequest() com_amazon_mas_devportal_fileupload_model.CreateUploadRequest {
	return this.Ị_masRequest
}
func (this *_CreateUploadRequest) SetMasRequest(v com_amazon_mas_devportal_fileupload_model.CreateUploadRequest) {
	this.Ị_masRequest = v
}
func (this *_CreateUploadRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_CreateUploadRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_CreateUploadRequest) __type() {
}
func NewCreateUploadRequest() CreateUploadRequest {
	return &_CreateUploadRequest{}
}
func init() {
	var val CreateUploadRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.twitchproductingestionservice").RegisterShape("CreateUploadRequest", t, func() interface{} {
		return NewCreateUploadRequest()
	})
}

type GetUploadRequest interface {
	__type()
	SetTuid(v *string)
	Tuid() *string
	SetMasRequest(v com_amazon_mas_devportal_fileupload_model.GetUploadRequest)
	MasRequest() com_amazon_mas_devportal_fileupload_model.GetUploadRequest
}
type _GetUploadRequest struct {
	Ị_tuid       *string                                                    `coral:"tuid" json:"tuid"`
	Ị_masRequest com_amazon_mas_devportal_fileupload_model.GetUploadRequest `coral:"masRequest" json:"masRequest"`
}

func (this *_GetUploadRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_GetUploadRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_GetUploadRequest) MasRequest() com_amazon_mas_devportal_fileupload_model.GetUploadRequest {
	return this.Ị_masRequest
}
func (this *_GetUploadRequest) SetMasRequest(v com_amazon_mas_devportal_fileupload_model.GetUploadRequest) {
	this.Ị_masRequest = v
}
func (this *_GetUploadRequest) __type() {
}
func NewGetUploadRequest() GetUploadRequest {
	return &_GetUploadRequest{}
}
func init() {
	var val GetUploadRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.twitchproductingestionservice").RegisterShape("GetUploadRequest", t, func() interface{} {
		return NewGetUploadRequest()
	})
}

//Twitch service that acts as a bridge between SubsidiaryWebService and ADG/Appstore services for Twitch's product ingestion.
type TwitchProductInGestionService interface { //Save a product release document.
	SaveProductRelease(input SaveProductReleaseRequest) (com_amazon_adg_submission_model.SaveProductReleaseResponse, error)       //Submits a product release.
	SubmitProductRelease(input SubmitProductReleaseRequest) (com_amazon_adg_submission_model.SubmitProductReleaseResponse, error) //Starts the upload process and retrieves the PUT URL.
	CreateUpload(input CreateUploadRequest) (com_amazon_mas_devportal_fileupload_model.UploadResource, error)                     //Retrieves the upload resource for the given upload id.
	GetUpload(input GetUploadRequest) (com_amazon_mas_devportal_fileupload_model.UploadResource, error)                           //Create an ADG product domain.
	CreateDomains(input CreateDomainsRequest) (com_amazon_adg_submission_model.CreateDomainsResponse, error)                      //Create an ADG product.
	CreateProducts(input CreateProductsRequest) (com_amazon_adg_submission_model.CreateProductsResponse, error)
}
