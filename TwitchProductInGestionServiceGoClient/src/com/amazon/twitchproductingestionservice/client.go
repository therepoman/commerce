package com_amazon_twitchproductingestionservice

import (
	com_amazon_adg_submission_model "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/adg/submission/model"
	com_amazon_mas_devportal_fileupload_model "code.justin.tv/commerce/TwitchProductInGestionServiceGoClient/src/com/amazon/mas/devportal/fileupload/model"
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//Twitch service that acts as a bridge between SubsidiaryWebService and ADG/Appstore services for Twitch's product ingestion.
type TwitchProductInGestionServiceClient struct {
	C __client__.Client
}

//Creates a new TwitchProductInGestionServiceClient
func NewTwitchProductInGestionServiceClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *TwitchProductInGestionServiceClient) {
	return &TwitchProductInGestionServiceClient{__client__.NewClient("com.amazon.twitchproductingestionservice", "TwitchProductInGestionService", dialer, codec)}
}

//Save a product release document.
func (this *TwitchProductInGestionServiceClient) SaveProductRelease(input SaveProductReleaseRequest) (com_amazon_adg_submission_model.SaveProductReleaseResponse, error) {
	var output com_amazon_adg_submission_model.SaveProductReleaseResponse
	err := this.C.Call("com.amazon.twitchproductingestionservice", "saveProductRelease", input, &output)
	return output, err
}

//Submits a product release.
func (this *TwitchProductInGestionServiceClient) SubmitProductRelease(input SubmitProductReleaseRequest) (com_amazon_adg_submission_model.SubmitProductReleaseResponse, error) {
	var output com_amazon_adg_submission_model.SubmitProductReleaseResponse
	err := this.C.Call("com.amazon.twitchproductingestionservice", "submitProductRelease", input, &output)
	return output, err
}

//Starts the upload process and retrieves the PUT URL.
func (this *TwitchProductInGestionServiceClient) CreateUpload(input CreateUploadRequest) (com_amazon_mas_devportal_fileupload_model.UploadResource, error) {
	var output com_amazon_mas_devportal_fileupload_model.UploadResource
	err := this.C.Call("com.amazon.twitchproductingestionservice", "createUpload", input, &output)
	return output, err
}

//Retrieves the upload resource for the given upload id.
func (this *TwitchProductInGestionServiceClient) GetUpload(input GetUploadRequest) (com_amazon_mas_devportal_fileupload_model.UploadResource, error) {
	var output com_amazon_mas_devportal_fileupload_model.UploadResource
	err := this.C.Call("com.amazon.twitchproductingestionservice", "getUpload", input, &output)
	return output, err
}

//Create an ADG product domain.
func (this *TwitchProductInGestionServiceClient) CreateDomains(input CreateDomainsRequest) (com_amazon_adg_submission_model.CreateDomainsResponse, error) {
	var output com_amazon_adg_submission_model.CreateDomainsResponse
	err := this.C.Call("com.amazon.twitchproductingestionservice", "createDomains", input, &output)
	return output, err
}

//Create an ADG product.
func (this *TwitchProductInGestionServiceClient) CreateProducts(input CreateProductsRequest) (com_amazon_adg_submission_model.CreateProductsResponse, error) {
	var output com_amazon_adg_submission_model.CreateProductsResponse
	err := this.C.Call("com.amazon.twitchproductingestionservice", "createProducts", input, &output)
	return output, err
}
