package com_amazon_mas_devportal_fileupload_model

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//Provides a service to support asset (aka file) upload for the Amazon AppStore.
//The service provides a signed S3 URL for the upload of an asset. After
//the file is uploaded, the service processes the file and associates
//it with an AppStore item.
type MASFileUploadServiceClient struct {
	C __client__.Client
}

//Creates a new MASFileUploadServiceClient
func NewMASFileUploadServiceClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *MASFileUploadServiceClient) {
	return &MASFileUploadServiceClient{__client__.NewClient("com.amazon.mas.devportal.fileupload.model", "MASFileUploadService", dialer, codec)}
}

//Deprecated!!! Processes an message indicating a file upload was completed.
func (this *MASFileUploadServiceClient) ProcessSQSUploadCompleteMessage(input ProcessSQSUploadCompleteMessageRequest) error {
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "processSQSUploadCompleteMessage", input, nil)
	return err
}

//Processes an message indicating a file upload was completed.
func (this *MASFileUploadServiceClient) HandleCasSqsMessage(input HandleCasSqsMessageRequest) (UploadResource, error) {
	var output UploadResource
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "handleCasSqsMessage", input, &output)
	return output, err
}

//Retrieves the upload resource for the given upload id.
func (this *MASFileUploadServiceClient) GetUpload(input GetUploadRequest) (UploadResource, error) {
	var output UploadResource
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "getUpload", input, &output)
	return output, err
}

//Starts the upload process and retrieves the PUT URL.
func (this *MASFileUploadServiceClient) CreateUpload(input CreateUploadRequest) (UploadResource, error) {
	var output UploadResource
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "createUpload", input, &output)
	return output, err
}

//Deprecated!!! Initiates the upload of an asset for an item in the AppStore. Returns a signed S3 URL
//that can be used to upload the asset.
func (this *MASFileUploadServiceClient) GetAssetUploadUrl(input GetAssetUploadUrlRequest) (GetAssetUploadUrlResponse, error) {
	var output GetAssetUploadUrlResponse
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "getAssetUploadUrl", input, &output)
	return output, err
}

//Deprecated!!! Provides details about all assets of a particular asset group for an AppStore item.
func (this *MASFileUploadServiceClient) GetItemAssetGroupDetails(input GetItemAssetGroupDetailsRequest) (GetItemAssetGroupDetailsResponse, error) {
	var output GetItemAssetGroupDetailsResponse
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "getItemAssetGroupDetails", input, &output)
	return output, err
}

//Deprecated!!! Provides details about all assets of a particular asset group and sub group for an
//individual AppStore item.
func (this *MASFileUploadServiceClient) GetItemAssetSubGroupDetails(input GetItemAssetSubGroupDetailsRequest) (GetItemAssetSubGroupDetailsResponse, error) {
	var output GetItemAssetSubGroupDetailsResponse
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "getItemAssetSubGroupDetails", input, &output)
	return output, err
}

//Deprecated!!!  Processes an message indicating a file upload was completed and the file was verified to
//meet requirements by CAS.
func (this *MASFileUploadServiceClient) ProcessSQSCASCompleteMessage(input ProcessSQSCASCompleteMessageRequest) error {
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "processSQSCASCompleteMessage", input, nil)
	return err
}

//Deprecated!!! Copies a specific asset already uploaded for an AppStore item to a different AppStore
//item. This allows the same asset to be resused for multiple AppStore items.
func (this *MASFileUploadServiceClient) CopyAssetToItem(input CopyAssetToItemRequest) (CopyAssetToItemResponse, error) {
	var output CopyAssetToItemResponse
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "copyAssetToItem", input, &output)
	return output, err
}

//Deprecated!!! Provides the status and details about an specific asset releated to an AppStore item.
func (this *MASFileUploadServiceClient) GetAssetDetails(input GetAssetDetailsRequest) (GetAssetDetailsResponse, error) {
	var output GetAssetDetailsResponse
	err := this.C.Call("com.amazon.mas.devportal.fileupload.model", "getAssetDetails", input, &output)
	return output, err
}
