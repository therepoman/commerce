# Update Instructions:

1. First checkout the go client amazon side and build.
    * `brazil build`
2. Commit new generated code.
    * `git commit -a -m "new update"`
3. Clone this package in locally.
    * `git clone git@git-aws.internal.justin.tv:commerce/TwitchProductInGestionServiceGoClient.git`
4. Add amazon repository as a remote.
    * `git remote add amazon ssh://git.amazon.com/pkg/TwitchProductInGestionServiceGoClient`
5. Fetch from all remotes.
    * `git fetch --all`
6. Switch to a new branch.
    * `git checkout -b update`
7. Update new branch to `amazon/mainline`.
    * `git reset --hard amazon/mainline`
8. Run `twitchify.sh`.
    * `sh twitchify.sh`
7. Make sure everything builds. You may have to clean up some unused imports.
    * `go build ./...`
8. Push updated code to Amazon and Twitch.
