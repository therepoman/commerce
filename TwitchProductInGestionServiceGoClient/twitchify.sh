#!/bin/bash

for i in `find src -name "*.go"`
do
  sed -i.pkg \
    -e 's/__client__ "coral\/client"/__client__ "code.justin.tv\/commerce\/CoralGoClient\/src\/coral\/client"/g' \
    -e 's/__dialer__ "coral\/dialer"/__dialer__ "code.justin.tv\/commerce\/CoralGoClient\/src\/coral\/dialer"/g' \
    -e 's/__model__ "coral\/model"/__model__ "code.justin.tv\/commerce\/CoralGoModel\/src\/coral\/model"/g' \
    -e 's/__codec__ "coral\/codec"/__codec__ "code.justin.tv\/commerce\/CoralGoCodec\/src\/coral\/codec"/g' \
    -e 's/com_amazon_adg_submission_model "com\/amazon\/adg\/submission\/model"/com_amazon_adg_submission_model "code.justin.tv\/commerce\/TwitchProductInGestionServiceGoClient\/src\/com\/amazon\/adg\/submission\/model"/g' \
    -e 's/com_amazon_adg_common_model "com\/amazon\/adg\/common\/model"/com_amazon_adg_common_model "code.justin.tv\/commerce\/TwitchProductInGestionServiceGoClient\/src\/com\/amazon\/adg\/common\/model"/g' \
    -e 's/com_amazon_mas_devportal_fileupload_model "com\/amazon\/mas\/devportal\/fileupload\/model"/com_amazon_mas_devportal_fileupload_model "code.justin.tv\/commerce\/TwitchProductInGestionServiceGoClient\/src\/com\/amazon\/mas\/devportal\/fileupload\/model"/g' \
    $i
done
find . -name '*.pkg' -delete
