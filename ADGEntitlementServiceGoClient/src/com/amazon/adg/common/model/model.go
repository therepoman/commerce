package model

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__big__ "math/big"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to IntegerObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LanguageCode")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Asin")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonCommonIdentifier")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StoreEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ReceiptId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductLineEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to MarketplaceId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SensitiveString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ObfuString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonOrderId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ECID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodOriginTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CountryOfResidence")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to UUID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodStateEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SensitiveECID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SensitiveAmazonCommonIdentifier")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SKU")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TransactionTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__big__.Rat
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__big__.Rat
		if f, ok := from.Interface().(*__big__.Rat); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BigDecimal")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type Product interface {
	__type()
	SetVendor(v Vendor)
	Vendor() Vendor
	SetAsinVersion(v *int32)
	AsinVersion() *int32
	SetType(v *string)
	Type() *string
	SetProductTitle(v *string)
	ProductTitle() *string
	SetProductDescription(v *string)
	ProductDescription() *string
	SetProductLine(v *string)
	ProductLine() *string
	SetProductDomain(v Domain)
	ProductDomain() Domain
	SetId(v *string)
	Id() *string
	SetSku(v *string)
	Sku() *string
	SetAsin(v *string)
	Asin() *string
	SetProductDetails(v ProductDetail)
	ProductDetails() ProductDetail
}
type _Product struct {
	Ị_productDomain      Domain        `coral:"productDomain"`
	Ị_vendor             Vendor        `coral:"vendor"`
	Ị_asinVersion        *int32        `coral:"asinVersion"`
	Ị_type               *string       `coral:"type"`
	Ị_productTitle       *string       `coral:"productTitle"`
	Ị_productDescription *string       `coral:"productDescription"`
	Ị_productLine        *string       `coral:"productLine"`
	Ị_id                 *string       `coral:"id"`
	Ị_sku                *string       `coral:"sku"`
	Ị_asin               *string       `coral:"asin"`
	Ị_productDetails     ProductDetail `coral:"productDetails"`
}

func (this *_Product) Id() *string {
	return this.Ị_id
}
func (this *_Product) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Product) Sku() *string {
	return this.Ị_sku
}
func (this *_Product) SetSku(v *string) {
	this.Ị_sku = v
}
func (this *_Product) Asin() *string {
	return this.Ị_asin
}
func (this *_Product) SetAsin(v *string) {
	this.Ị_asin = v
}
func (this *_Product) ProductDetails() ProductDetail {
	return this.Ị_productDetails
}
func (this *_Product) SetProductDetails(v ProductDetail) {
	this.Ị_productDetails = v
}
func (this *_Product) ProductDescription() *string {
	return this.Ị_productDescription
}
func (this *_Product) SetProductDescription(v *string) {
	this.Ị_productDescription = v
}
func (this *_Product) ProductLine() *string {
	return this.Ị_productLine
}
func (this *_Product) SetProductLine(v *string) {
	this.Ị_productLine = v
}
func (this *_Product) ProductDomain() Domain {
	return this.Ị_productDomain
}
func (this *_Product) SetProductDomain(v Domain) {
	this.Ị_productDomain = v
}
func (this *_Product) Vendor() Vendor {
	return this.Ị_vendor
}
func (this *_Product) SetVendor(v Vendor) {
	this.Ị_vendor = v
}
func (this *_Product) AsinVersion() *int32 {
	return this.Ị_asinVersion
}
func (this *_Product) SetAsinVersion(v *int32) {
	this.Ị_asinVersion = v
}
func (this *_Product) Type() *string {
	return this.Ị_type
}
func (this *_Product) SetType(v *string) {
	this.Ị_type = v
}
func (this *_Product) ProductTitle() *string {
	return this.Ị_productTitle
}
func (this *_Product) SetProductTitle(v *string) {
	this.Ị_productTitle = v
}
func (this *_Product) __type() {
}
func NewProduct() Product {
	return &_Product{}
}
func init() {
	var val Product
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("Product", t, func() interface{} {
		return NewProduct()
	})
}

type ProductDetail interface {
	__type()
	SetLatestProductVersion(v *string)
	LatestProductVersion() *string
	SetIsPurchasable(v *bool)
	IsPurchasable() *bool
	SetProductIconUrl(v *string)
	ProductIconUrl() *string
	SetCurrentPrice(v Currency)
	CurrentPrice() Currency
	SetCompatibleProductVersion(v *string)
	CompatibleProductVersion() *string
	SetDetails(v map[*string]map[*string]*string)
	Details() map[*string]map[*string]*string
	SetCategory(v *string)
	Category() *string
	SetListPrice(v Currency)
	ListPrice() Currency
	SetIsCompatible(v *bool)
	IsCompatible() *bool
}
type _ProductDetail struct {
	Ị_listPrice                Currency                        `coral:"listPrice"`
	Ị_isCompatible             *bool                           `coral:"isCompatible"`
	Ị_details                  map[*string]map[*string]*string `coral:"details"`
	Ị_category                 *string                         `coral:"category"`
	Ị_currentPrice             Currency                        `coral:"currentPrice"`
	Ị_compatibleProductVersion *string                         `coral:"compatibleProductVersion"`
	Ị_latestProductVersion     *string                         `coral:"latestProductVersion"`
	Ị_isPurchasable            *bool                           `coral:"isPurchasable"`
	Ị_productIconUrl           *string                         `coral:"productIconUrl"`
}

func (this *_ProductDetail) ProductIconUrl() *string {
	return this.Ị_productIconUrl
}
func (this *_ProductDetail) SetProductIconUrl(v *string) {
	this.Ị_productIconUrl = v
}
func (this *_ProductDetail) CurrentPrice() Currency {
	return this.Ị_currentPrice
}
func (this *_ProductDetail) SetCurrentPrice(v Currency) {
	this.Ị_currentPrice = v
}
func (this *_ProductDetail) CompatibleProductVersion() *string {
	return this.Ị_compatibleProductVersion
}
func (this *_ProductDetail) SetCompatibleProductVersion(v *string) {
	this.Ị_compatibleProductVersion = v
}
func (this *_ProductDetail) LatestProductVersion() *string {
	return this.Ị_latestProductVersion
}
func (this *_ProductDetail) SetLatestProductVersion(v *string) {
	this.Ị_latestProductVersion = v
}
func (this *_ProductDetail) IsPurchasable() *bool {
	return this.Ị_isPurchasable
}
func (this *_ProductDetail) SetIsPurchasable(v *bool) {
	this.Ị_isPurchasable = v
}
func (this *_ProductDetail) Category() *string {
	return this.Ị_category
}
func (this *_ProductDetail) SetCategory(v *string) {
	this.Ị_category = v
}
func (this *_ProductDetail) ListPrice() Currency {
	return this.Ị_listPrice
}
func (this *_ProductDetail) SetListPrice(v Currency) {
	this.Ị_listPrice = v
}
func (this *_ProductDetail) IsCompatible() *bool {
	return this.Ị_isCompatible
}
func (this *_ProductDetail) SetIsCompatible(v *bool) {
	this.Ị_isCompatible = v
}
func (this *_ProductDetail) Details() map[*string]map[*string]*string {
	return this.Ị_details
}
func (this *_ProductDetail) SetDetails(v map[*string]map[*string]*string) {
	this.Ị_details = v
}
func (this *_ProductDetail) __type() {
}
func NewProductDetail() ProductDetail {
	return &_ProductDetail{}
}
func init() {
	var val ProductDetail
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("ProductDetail", t, func() interface{} {
		return NewProductDetail()
	})
}

type DigitalGoodTransaction interface {
	__type()
	SetTxTimestamp(v *__time__.Time)
	TxTimestamp() *__time__.Time
	SetExternalTxId(v *string)
	ExternalTxId() *string
	SetExternalTxType(v *string)
	ExternalTxType() *string
	SetCustomerId(v *string)
	CustomerId() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetInstanceId(v *string)
	InstanceId() *string
	SetTxId(v *string)
	TxId() *string
	SetTxType(v *string)
	TxType() *string
	SetHolderId(v *string)
	HolderId() *string
}
type _DigitalGoodTransaction struct {
	Ị_instanceId     *string        `coral:"instanceId"`
	Ị_txId           *string        `coral:"txId"`
	Ị_txType         *string        `coral:"txType"`
	Ị_holderId       *string        `coral:"holderId"`
	Ị_txTimestamp    *__time__.Time `coral:"txTimestamp"`
	Ị_externalTxId   *string        `coral:"externalTxId"`
	Ị_externalTxType *string        `coral:"externalTxType"`
	Ị_customerId     *string        `coral:"customerId"`
	Ị_ownerId        *string        `coral:"ownerId"`
}

func (this *_DigitalGoodTransaction) InstanceId() *string {
	return this.Ị_instanceId
}
func (this *_DigitalGoodTransaction) SetInstanceId(v *string) {
	this.Ị_instanceId = v
}
func (this *_DigitalGoodTransaction) TxId() *string {
	return this.Ị_txId
}
func (this *_DigitalGoodTransaction) SetTxId(v *string) {
	this.Ị_txId = v
}
func (this *_DigitalGoodTransaction) TxType() *string {
	return this.Ị_txType
}
func (this *_DigitalGoodTransaction) SetTxType(v *string) {
	this.Ị_txType = v
}
func (this *_DigitalGoodTransaction) HolderId() *string {
	return this.Ị_holderId
}
func (this *_DigitalGoodTransaction) SetHolderId(v *string) {
	this.Ị_holderId = v
}
func (this *_DigitalGoodTransaction) TxTimestamp() *__time__.Time {
	return this.Ị_txTimestamp
}
func (this *_DigitalGoodTransaction) SetTxTimestamp(v *__time__.Time) {
	this.Ị_txTimestamp = v
}
func (this *_DigitalGoodTransaction) ExternalTxId() *string {
	return this.Ị_externalTxId
}
func (this *_DigitalGoodTransaction) SetExternalTxId(v *string) {
	this.Ị_externalTxId = v
}
func (this *_DigitalGoodTransaction) ExternalTxType() *string {
	return this.Ị_externalTxType
}
func (this *_DigitalGoodTransaction) SetExternalTxType(v *string) {
	this.Ị_externalTxType = v
}
func (this *_DigitalGoodTransaction) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_DigitalGoodTransaction) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_DigitalGoodTransaction) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_DigitalGoodTransaction) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_DigitalGoodTransaction) __type() {
}
func NewDigitalGoodTransaction() DigitalGoodTransaction {
	return &_DigitalGoodTransaction{}
}
func init() {
	var val DigitalGoodTransaction
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("DigitalGoodTransaction", t, func() interface{} {
		return NewDigitalGoodTransaction()
	})
}

type StoredValue interface {
	__type()
	SetProduct(v Product)
	Product() Product
	SetChannel(v Domain)
	Channel() Domain
	SetDomain(v Domain)
	Domain() Domain
	SetAdditionalDomains(v []Domain)
	AdditionalDomains() []Domain
	SetTransactions(v []DigitalGoodTransaction)
	Transactions() []DigitalGoodTransaction
	SetTransactionState(v *string)
	TransactionState() *string
	SetId(v *string)
	Id() *string
	SetCustomerId(v *string)
	CustomerId() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetReceiptId(v *string)
	ReceiptId() *string
	SetState(v *string)
	State() *string
	SetOrigin(v DigitalGoodOrigin)
	Origin() DigitalGoodOrigin
	SetModified(v *__time__.Time)
	Modified() *__time__.Time
	SetCurrentValue(v *float64)
	CurrentValue() *float64
}
type _StoredValue struct {
	Ị_channel           Domain                   `coral:"channel"`
	Ị_domain            Domain                   `coral:"domain"`
	Ị_additionalDomains []Domain                 `coral:"additionalDomains"`
	Ị_transactions      []DigitalGoodTransaction `coral:"transactions"`
	Ị_product           Product                  `coral:"product"`
	Ị_customerId        *string                  `coral:"customerId"`
	Ị_ownerId           *string                  `coral:"ownerId"`
	Ị_receiptId         *string                  `coral:"receiptId"`
	Ị_state             *string                  `coral:"state"`
	Ị_origin            DigitalGoodOrigin        `coral:"origin"`
	Ị_modified          *__time__.Time           `coral:"modified"`
	Ị_transactionState  *string                  `coral:"transactionState"`
	Ị_id                *string                  `coral:"id"`
	Ị_currentValue      *float64                 `coral:"currentValue"`
}

func (this *_StoredValue) Transactions() []DigitalGoodTransaction {
	return this.Ị_transactions
}
func (this *_StoredValue) SetTransactions(v []DigitalGoodTransaction) {
	this.Ị_transactions = v
}
func (this *_StoredValue) Product() Product {
	return this.Ị_product
}
func (this *_StoredValue) SetProduct(v Product) {
	this.Ị_product = v
}
func (this *_StoredValue) Channel() Domain {
	return this.Ị_channel
}
func (this *_StoredValue) SetChannel(v Domain) {
	this.Ị_channel = v
}
func (this *_StoredValue) Domain() Domain {
	return this.Ị_domain
}
func (this *_StoredValue) SetDomain(v Domain) {
	this.Ị_domain = v
}
func (this *_StoredValue) AdditionalDomains() []Domain {
	return this.Ị_additionalDomains
}
func (this *_StoredValue) SetAdditionalDomains(v []Domain) {
	this.Ị_additionalDomains = v
}
func (this *_StoredValue) State() *string {
	return this.Ị_state
}
func (this *_StoredValue) SetState(v *string) {
	this.Ị_state = v
}
func (this *_StoredValue) Origin() DigitalGoodOrigin {
	return this.Ị_origin
}
func (this *_StoredValue) SetOrigin(v DigitalGoodOrigin) {
	this.Ị_origin = v
}
func (this *_StoredValue) Modified() *__time__.Time {
	return this.Ị_modified
}
func (this *_StoredValue) SetModified(v *__time__.Time) {
	this.Ị_modified = v
}
func (this *_StoredValue) TransactionState() *string {
	return this.Ị_transactionState
}
func (this *_StoredValue) SetTransactionState(v *string) {
	this.Ị_transactionState = v
}
func (this *_StoredValue) Id() *string {
	return this.Ị_id
}
func (this *_StoredValue) SetId(v *string) {
	this.Ị_id = v
}
func (this *_StoredValue) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_StoredValue) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_StoredValue) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_StoredValue) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_StoredValue) ReceiptId() *string {
	return this.Ị_receiptId
}
func (this *_StoredValue) SetReceiptId(v *string) {
	this.Ị_receiptId = v
}
func (this *_StoredValue) CurrentValue() *float64 {
	return this.Ị_currentValue
}
func (this *_StoredValue) SetCurrentValue(v *float64) {
	this.Ị_currentValue = v
}
func (this *_StoredValue) __type() {
}
func NewStoredValue() StoredValue {
	return &_StoredValue{}
}
func init() {
	var val StoredValue
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("StoredValue", t, func() interface{} {
		return NewStoredValue()
	})
}

type ClientInfo interface {
	__type()
	SetClientType(v *string)
	ClientType() *string
	SetProperties(v map[*string]*string)
	Properties() map[*string]*string
	SetHeaders(v []KeyValue)
	Headers() []KeyValue
	SetDomains(v []Domain)
	Domains() []Domain
	SetClientId(v *string)
	ClientId() *string
	SetClientVersion(v *string)
	ClientVersion() *string
}
type _ClientInfo struct {
	Ị_properties    map[*string]*string `coral:"properties"`
	Ị_headers       []KeyValue          `coral:"headers"`
	Ị_domains       []Domain            `coral:"domains"`
	Ị_clientId      *string             `coral:"clientId"`
	Ị_clientVersion *string             `coral:"clientVersion"`
	Ị_clientType    *string             `coral:"clientType"`
}

func (this *_ClientInfo) ClientId() *string {
	return this.Ị_clientId
}
func (this *_ClientInfo) SetClientId(v *string) {
	this.Ị_clientId = v
}
func (this *_ClientInfo) ClientVersion() *string {
	return this.Ị_clientVersion
}
func (this *_ClientInfo) SetClientVersion(v *string) {
	this.Ị_clientVersion = v
}
func (this *_ClientInfo) ClientType() *string {
	return this.Ị_clientType
}
func (this *_ClientInfo) SetClientType(v *string) {
	this.Ị_clientType = v
}
func (this *_ClientInfo) Properties() map[*string]*string {
	return this.Ị_properties
}
func (this *_ClientInfo) SetProperties(v map[*string]*string) {
	this.Ị_properties = v
}
func (this *_ClientInfo) Headers() []KeyValue {
	return this.Ị_headers
}
func (this *_ClientInfo) SetHeaders(v []KeyValue) {
	this.Ị_headers = v
}
func (this *_ClientInfo) Domains() []Domain {
	return this.Ị_domains
}
func (this *_ClientInfo) SetDomains(v []Domain) {
	this.Ị_domains = v
}
func (this *_ClientInfo) __type() {
}
func NewClientInfo() ClientInfo {
	return &_ClientInfo{}
}
func init() {
	var val ClientInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("ClientInfo", t, func() interface{} {
		return NewClientInfo()
	})
}

type Filter interface {
	__type()
	SetName(v *string)
	Name() *string
	SetInput(v *string)
	Input() *string
}
type _Filter struct {
	Ị_name  *string `coral:"name"`
	Ị_input *string `coral:"input"`
}

func (this *_Filter) Name() *string {
	return this.Ị_name
}
func (this *_Filter) SetName(v *string) {
	this.Ị_name = v
}
func (this *_Filter) Input() *string {
	return this.Ị_input
}
func (this *_Filter) SetInput(v *string) {
	this.Ị_input = v
}
func (this *_Filter) __type() {
}
func NewFilter() Filter {
	return &_Filter{}
}
func init() {
	var val Filter
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("Filter", t, func() interface{} {
		return NewFilter()
	})
}

//A key value pair that is passed to downstream services
type KeyValue interface {
	__type()
	SetKey(v *string)
	Key() *string
	SetValue(v *string)
	Value() *string
}
type _KeyValue struct {
	Ị_key   *string `coral:"key"`
	Ị_value *string `coral:"value"`
}

func (this *_KeyValue) Key() *string {
	return this.Ị_key
}
func (this *_KeyValue) SetKey(v *string) {
	this.Ị_key = v
}
func (this *_KeyValue) Value() *string {
	return this.Ị_value
}
func (this *_KeyValue) SetValue(v *string) {
	this.Ị_value = v
}
func (this *_KeyValue) __type() {
}
func NewKeyValue() KeyValue {
	return &_KeyValue{}
}
func init() {
	var val KeyValue
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("KeyValue", t, func() interface{} {
		return NewKeyValue()
	})
}

type ADGEntity interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
}
type _ADGEntity struct {
	Ị_id   *string `coral:"id"`
	Ị_name *string `coral:"name"`
}

func (this *_ADGEntity) Id() *string {
	return this.Ị_id
}
func (this *_ADGEntity) SetId(v *string) {
	this.Ị_id = v
}
func (this *_ADGEntity) Name() *string {
	return this.Ị_name
}
func (this *_ADGEntity) SetName(v *string) {
	this.Ị_name = v
}
func (this *_ADGEntity) __type() {
}
func NewADGEntity() ADGEntity {
	return &_ADGEntity{}
}
func init() {
	var val ADGEntity
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("ADGEntity", t, func() interface{} {
		return NewADGEntity()
	})
}

type Vendor interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
	SetMasVendorId(v *string)
	MasVendorId() *string
	SetMasVendorCode(v *string)
	MasVendorCode() *string
}
type _Vendor struct {
	Ị_name          *string `coral:"name"`
	Ị_id            *string `coral:"id"`
	Ị_masVendorId   *string `coral:"masVendorId"`
	Ị_masVendorCode *string `coral:"masVendorCode"`
}

func (this *_Vendor) Name() *string {
	return this.Ị_name
}
func (this *_Vendor) SetName(v *string) {
	this.Ị_name = v
}
func (this *_Vendor) Id() *string {
	return this.Ị_id
}
func (this *_Vendor) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Vendor) MasVendorId() *string {
	return this.Ị_masVendorId
}
func (this *_Vendor) SetMasVendorId(v *string) {
	this.Ị_masVendorId = v
}
func (this *_Vendor) MasVendorCode() *string {
	return this.Ị_masVendorCode
}
func (this *_Vendor) SetMasVendorCode(v *string) {
	this.Ị_masVendorCode = v
}
func (this *_Vendor) __type() {
}
func NewVendor() Vendor {
	return &_Vendor{}
}
func init() {
	var val Vendor
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("Vendor", t, func() interface{} {
		return NewVendor()
	})
}

type DigitalGoodOrigin interface {
	__type()
	SetOriginId(v *string)
	OriginId() *string
	SetOriginTimestamp(v *__time__.Time)
	OriginTimestamp() *__time__.Time
	SetOriginType(v *string)
	OriginType() *string
}
type _DigitalGoodOrigin struct {
	Ị_originTimestamp *__time__.Time `coral:"originTimestamp"`
	Ị_originType      *string        `coral:"originType"`
	Ị_originId        *string        `coral:"originId"`
}

func (this *_DigitalGoodOrigin) OriginTimestamp() *__time__.Time {
	return this.Ị_originTimestamp
}
func (this *_DigitalGoodOrigin) SetOriginTimestamp(v *__time__.Time) {
	this.Ị_originTimestamp = v
}
func (this *_DigitalGoodOrigin) OriginType() *string {
	return this.Ị_originType
}
func (this *_DigitalGoodOrigin) SetOriginType(v *string) {
	this.Ị_originType = v
}
func (this *_DigitalGoodOrigin) OriginId() *string {
	return this.Ị_originId
}
func (this *_DigitalGoodOrigin) SetOriginId(v *string) {
	this.Ị_originId = v
}
func (this *_DigitalGoodOrigin) __type() {
}
func NewDigitalGoodOrigin() DigitalGoodOrigin {
	return &_DigitalGoodOrigin{}
}
func init() {
	var val DigitalGoodOrigin
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("DigitalGoodOrigin", t, func() interface{} {
		return NewDigitalGoodOrigin()
	})
}

type ClientRequest interface {
	__type()
	SetClient(v ClientInfo)
	Client() ClientInfo
	SetCustomer(v AmazonCustomerInfo)
	Customer() AmazonCustomerInfo
	SetPreferredLocale(v CustomerLocalePrefs)
	PreferredLocale() CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
}
type _ClientRequest struct {
	Ị_client          ClientInfo          `coral:"client"`
	Ị_customer        AmazonCustomerInfo  `coral:"customer"`
	Ị_preferredLocale CustomerLocalePrefs `coral:"preferredLocale"`
	Ị_language        *string             `coral:"language"`
}

func (this *_ClientRequest) Customer() AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_ClientRequest) SetCustomer(v AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_ClientRequest) PreferredLocale() CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_ClientRequest) SetPreferredLocale(v CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_ClientRequest) Language() *string {
	return this.Ị_language
}
func (this *_ClientRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_ClientRequest) Client() ClientInfo {
	return this.Ị_client
}
func (this *_ClientRequest) SetClient(v ClientInfo) {
	this.Ị_client = v
}
func (this *_ClientRequest) __type() {
}
func NewClientRequest() ClientRequest {
	return &_ClientRequest{}
}
func init() {
	var val ClientRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("ClientRequest", t, func() interface{} {
		return NewClientRequest()
	})
}

type PagedRequestParameters interface {
	__type()
	SetStartIndex(v *int32)
	StartIndex() *int32
	SetPageSize(v *int32)
	PageSize() *int32
}
type _PagedRequestParameters struct {
	Ị_pageSize   *int32 `coral:"pageSize"`
	Ị_startIndex *int32 `coral:"startIndex"`
}

func (this *_PagedRequestParameters) StartIndex() *int32 {
	return this.Ị_startIndex
}
func (this *_PagedRequestParameters) SetStartIndex(v *int32) {
	this.Ị_startIndex = v
}
func (this *_PagedRequestParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_PagedRequestParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_PagedRequestParameters) __type() {
}
func NewPagedRequestParameters() PagedRequestParameters {
	return &_PagedRequestParameters{}
}
func init() {
	var val PagedRequestParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("PagedRequestParameters", t, func() interface{} {
		return NewPagedRequestParameters()
	})
}

type PagingResponseParameters interface {
	__type()
	SetMoreDataAvailable(v *bool)
	MoreDataAvailable() *bool
	SetPageSize(v *int32)
	PageSize() *int32
	SetNextIndex(v *int32)
	NextIndex() *int32
}
type _PagingResponseParameters struct {
	Ị_moreDataAvailable *bool  `coral:"moreDataAvailable"`
	Ị_pageSize          *int32 `coral:"pageSize"`
	Ị_nextIndex         *int32 `coral:"nextIndex"`
}

func (this *_PagingResponseParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_PagingResponseParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_PagingResponseParameters) NextIndex() *int32 {
	return this.Ị_nextIndex
}
func (this *_PagingResponseParameters) SetNextIndex(v *int32) {
	this.Ị_nextIndex = v
}
func (this *_PagingResponseParameters) MoreDataAvailable() *bool {
	return this.Ị_moreDataAvailable
}
func (this *_PagingResponseParameters) SetMoreDataAvailable(v *bool) {
	this.Ị_moreDataAvailable = v
}
func (this *_PagingResponseParameters) __type() {
}
func NewPagingResponseParameters() PagingResponseParameters {
	return &_PagingResponseParameters{}
}
func init() {
	var val PagingResponseParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("PagingResponseParameters", t, func() interface{} {
		return NewPagingResponseParameters()
	})
}

type Domain interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
	SetOwner(v ADGEntity)
	Owner() ADGEntity
}
type _Domain struct {
	Ị_id    *string   `coral:"id"`
	Ị_name  *string   `coral:"name"`
	Ị_owner ADGEntity `coral:"owner"`
}

func (this *_Domain) Id() *string {
	return this.Ị_id
}
func (this *_Domain) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Domain) Name() *string {
	return this.Ị_name
}
func (this *_Domain) SetName(v *string) {
	this.Ị_name = v
}
func (this *_Domain) Owner() ADGEntity {
	return this.Ị_owner
}
func (this *_Domain) SetOwner(v ADGEntity) {
	this.Ị_owner = v
}
func (this *_Domain) __type() {
}
func NewDomain() Domain {
	return &_Domain{}
}
func init() {
	var val Domain
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("Domain", t, func() interface{} {
		return NewDomain()
	})
}

type DigitalGood interface {
	__type()
	SetProduct(v Product)
	Product() Product
	SetChannel(v Domain)
	Channel() Domain
	SetDomain(v Domain)
	Domain() Domain
	SetAdditionalDomains(v []Domain)
	AdditionalDomains() []Domain
	SetTransactions(v []DigitalGoodTransaction)
	Transactions() []DigitalGoodTransaction
	SetId(v *string)
	Id() *string
	SetCustomerId(v *string)
	CustomerId() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetReceiptId(v *string)
	ReceiptId() *string
	SetState(v *string)
	State() *string
	SetOrigin(v DigitalGoodOrigin)
	Origin() DigitalGoodOrigin
	SetModified(v *__time__.Time)
	Modified() *__time__.Time
	SetTransactionState(v *string)
	TransactionState() *string
	SetHidden(v *bool)
	Hidden() *bool
}
type _DigitalGood struct {
	Ị_product           Product                  `coral:"product"`
	Ị_channel           Domain                   `coral:"channel"`
	Ị_domain            Domain                   `coral:"domain"`
	Ị_additionalDomains []Domain                 `coral:"additionalDomains"`
	Ị_transactions      []DigitalGoodTransaction `coral:"transactions"`
	Ị_origin            DigitalGoodOrigin        `coral:"origin"`
	Ị_modified          *__time__.Time           `coral:"modified"`
	Ị_transactionState  *string                  `coral:"transactionState"`
	Ị_id                *string                  `coral:"id"`
	Ị_customerId        *string                  `coral:"customerId"`
	Ị_ownerId           *string                  `coral:"ownerId"`
	Ị_receiptId         *string                  `coral:"receiptId"`
	Ị_state             *string                  `coral:"state"`
	Ị_hidden            *bool                    `coral:"hidden"`
}

func (this *_DigitalGood) ReceiptId() *string {
	return this.Ị_receiptId
}
func (this *_DigitalGood) SetReceiptId(v *string) {
	this.Ị_receiptId = v
}
func (this *_DigitalGood) State() *string {
	return this.Ị_state
}
func (this *_DigitalGood) SetState(v *string) {
	this.Ị_state = v
}
func (this *_DigitalGood) Origin() DigitalGoodOrigin {
	return this.Ị_origin
}
func (this *_DigitalGood) SetOrigin(v DigitalGoodOrigin) {
	this.Ị_origin = v
}
func (this *_DigitalGood) Modified() *__time__.Time {
	return this.Ị_modified
}
func (this *_DigitalGood) SetModified(v *__time__.Time) {
	this.Ị_modified = v
}
func (this *_DigitalGood) TransactionState() *string {
	return this.Ị_transactionState
}
func (this *_DigitalGood) SetTransactionState(v *string) {
	this.Ị_transactionState = v
}
func (this *_DigitalGood) Id() *string {
	return this.Ị_id
}
func (this *_DigitalGood) SetId(v *string) {
	this.Ị_id = v
}
func (this *_DigitalGood) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_DigitalGood) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_DigitalGood) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_DigitalGood) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_DigitalGood) AdditionalDomains() []Domain {
	return this.Ị_additionalDomains
}
func (this *_DigitalGood) SetAdditionalDomains(v []Domain) {
	this.Ị_additionalDomains = v
}
func (this *_DigitalGood) Transactions() []DigitalGoodTransaction {
	return this.Ị_transactions
}
func (this *_DigitalGood) SetTransactions(v []DigitalGoodTransaction) {
	this.Ị_transactions = v
}
func (this *_DigitalGood) Product() Product {
	return this.Ị_product
}
func (this *_DigitalGood) SetProduct(v Product) {
	this.Ị_product = v
}
func (this *_DigitalGood) Channel() Domain {
	return this.Ị_channel
}
func (this *_DigitalGood) SetChannel(v Domain) {
	this.Ị_channel = v
}
func (this *_DigitalGood) Domain() Domain {
	return this.Ị_domain
}
func (this *_DigitalGood) SetDomain(v Domain) {
	this.Ị_domain = v
}
func (this *_DigitalGood) Hidden() *bool {
	return this.Ị_hidden
}
func (this *_DigitalGood) SetHidden(v *bool) {
	this.Ị_hidden = v
}
func (this *_DigitalGood) __type() {
}
func NewDigitalGood() DigitalGood {
	return &_DigitalGood{}
}
func init() {
	var val DigitalGood
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("DigitalGood", t, func() interface{} {
		return NewDigitalGood()
	})
}

type CustomerInfo interface {
	__type()
	SetId(v *string)
	Id() *string
	SetFirstName(v *string)
	FirstName() *string
	SetLastName(v *string)
	LastName() *string
}
type _CustomerInfo struct {
	Ị_id        *string `coral:"id"`
	Ị_firstName *string `coral:"firstName"`
	Ị_lastName  *string `coral:"lastName"`
}

func (this *_CustomerInfo) Id() *string {
	return this.Ị_id
}
func (this *_CustomerInfo) SetId(v *string) {
	this.Ị_id = v
}
func (this *_CustomerInfo) FirstName() *string {
	return this.Ị_firstName
}
func (this *_CustomerInfo) SetFirstName(v *string) {
	this.Ị_firstName = v
}
func (this *_CustomerInfo) LastName() *string {
	return this.Ị_lastName
}
func (this *_CustomerInfo) SetLastName(v *string) {
	this.Ị_lastName = v
}
func (this *_CustomerInfo) __type() {
}
func NewCustomerInfo() CustomerInfo {
	return &_CustomerInfo{}
}
func init() {
	var val CustomerInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("CustomerInfo", t, func() interface{} {
		return NewCustomerInfo()
	})
}

type CustomerLocalePrefs interface {
	__type()
	SetPfm(v *string)
	Pfm() *string
	SetCor(v *string)
	Cor() *string
}
type _CustomerLocalePrefs struct {
	Ị_pfm *string `coral:"pfm"`
	Ị_cor *string `coral:"cor"`
}

func (this *_CustomerLocalePrefs) Pfm() *string {
	return this.Ị_pfm
}
func (this *_CustomerLocalePrefs) SetPfm(v *string) {
	this.Ị_pfm = v
}
func (this *_CustomerLocalePrefs) Cor() *string {
	return this.Ị_cor
}
func (this *_CustomerLocalePrefs) SetCor(v *string) {
	this.Ị_cor = v
}
func (this *_CustomerLocalePrefs) __type() {
}
func NewCustomerLocalePrefs() CustomerLocalePrefs {
	return &_CustomerLocalePrefs{}
}
func init() {
	var val CustomerLocalePrefs
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("CustomerLocalePrefs", t, func() interface{} {
		return NewCustomerLocalePrefs()
	})
}

type PagedResponseParameters interface {
	__type()
	SetNextIndex(v *int64)
	NextIndex() *int64
	SetTotalFound(v *int64)
	TotalFound() *int64
	SetMoreDataAvailable(v *bool)
	MoreDataAvailable() *bool
}
type _PagedResponseParameters struct {
	Ị_nextIndex         *int64 `coral:"nextIndex"`
	Ị_totalFound        *int64 `coral:"totalFound"`
	Ị_moreDataAvailable *bool  `coral:"moreDataAvailable"`
}

func (this *_PagedResponseParameters) NextIndex() *int64 {
	return this.Ị_nextIndex
}
func (this *_PagedResponseParameters) SetNextIndex(v *int64) {
	this.Ị_nextIndex = v
}
func (this *_PagedResponseParameters) TotalFound() *int64 {
	return this.Ị_totalFound
}
func (this *_PagedResponseParameters) SetTotalFound(v *int64) {
	this.Ị_totalFound = v
}
func (this *_PagedResponseParameters) MoreDataAvailable() *bool {
	return this.Ị_moreDataAvailable
}
func (this *_PagedResponseParameters) SetMoreDataAvailable(v *bool) {
	this.Ị_moreDataAvailable = v
}
func (this *_PagedResponseParameters) __type() {
}
func NewPagedResponseParameters() PagedResponseParameters {
	return &_PagedResponseParameters{}
}
func init() {
	var val PagedResponseParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("PagedResponseParameters", t, func() interface{} {
		return NewPagedResponseParameters()
	})
}

type PagingRequestParameters interface {
	__type()
	SetStartIndex(v *int32)
	StartIndex() *int32
	SetPageSize(v *int32)
	PageSize() *int32
}
type _PagingRequestParameters struct {
	Ị_pageSize   *int32 `coral:"pageSize"`
	Ị_startIndex *int32 `coral:"startIndex"`
}

func (this *_PagingRequestParameters) StartIndex() *int32 {
	return this.Ị_startIndex
}
func (this *_PagingRequestParameters) SetStartIndex(v *int32) {
	this.Ị_startIndex = v
}
func (this *_PagingRequestParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_PagingRequestParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_PagingRequestParameters) __type() {
}
func NewPagingRequestParameters() PagingRequestParameters {
	return &_PagingRequestParameters{}
}
func init() {
	var val PagingRequestParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("PagingRequestParameters", t, func() interface{} {
		return NewPagingRequestParameters()
	})
}

type BaseDigitalGood interface {
	__type()
	SetState(v *string)
	State() *string
	SetOrigin(v DigitalGoodOrigin)
	Origin() DigitalGoodOrigin
	SetModified(v *__time__.Time)
	Modified() *__time__.Time
	SetTransactionState(v *string)
	TransactionState() *string
	SetId(v *string)
	Id() *string
	SetCustomerId(v *string)
	CustomerId() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetReceiptId(v *string)
	ReceiptId() *string
	SetTransactions(v []DigitalGoodTransaction)
	Transactions() []DigitalGoodTransaction
	SetProduct(v Product)
	Product() Product
	SetChannel(v Domain)
	Channel() Domain
	SetDomain(v Domain)
	Domain() Domain
	SetAdditionalDomains(v []Domain)
	AdditionalDomains() []Domain
}
type _BaseDigitalGood struct {
	Ị_id                *string                  `coral:"id"`
	Ị_customerId        *string                  `coral:"customerId"`
	Ị_ownerId           *string                  `coral:"ownerId"`
	Ị_receiptId         *string                  `coral:"receiptId"`
	Ị_state             *string                  `coral:"state"`
	Ị_origin            DigitalGoodOrigin        `coral:"origin"`
	Ị_modified          *__time__.Time           `coral:"modified"`
	Ị_transactionState  *string                  `coral:"transactionState"`
	Ị_product           Product                  `coral:"product"`
	Ị_channel           Domain                   `coral:"channel"`
	Ị_domain            Domain                   `coral:"domain"`
	Ị_additionalDomains []Domain                 `coral:"additionalDomains"`
	Ị_transactions      []DigitalGoodTransaction `coral:"transactions"`
}

func (this *_BaseDigitalGood) Product() Product {
	return this.Ị_product
}
func (this *_BaseDigitalGood) SetProduct(v Product) {
	this.Ị_product = v
}
func (this *_BaseDigitalGood) Channel() Domain {
	return this.Ị_channel
}
func (this *_BaseDigitalGood) SetChannel(v Domain) {
	this.Ị_channel = v
}
func (this *_BaseDigitalGood) Domain() Domain {
	return this.Ị_domain
}
func (this *_BaseDigitalGood) SetDomain(v Domain) {
	this.Ị_domain = v
}
func (this *_BaseDigitalGood) AdditionalDomains() []Domain {
	return this.Ị_additionalDomains
}
func (this *_BaseDigitalGood) SetAdditionalDomains(v []Domain) {
	this.Ị_additionalDomains = v
}
func (this *_BaseDigitalGood) Transactions() []DigitalGoodTransaction {
	return this.Ị_transactions
}
func (this *_BaseDigitalGood) SetTransactions(v []DigitalGoodTransaction) {
	this.Ị_transactions = v
}
func (this *_BaseDigitalGood) Modified() *__time__.Time {
	return this.Ị_modified
}
func (this *_BaseDigitalGood) SetModified(v *__time__.Time) {
	this.Ị_modified = v
}
func (this *_BaseDigitalGood) TransactionState() *string {
	return this.Ị_transactionState
}
func (this *_BaseDigitalGood) SetTransactionState(v *string) {
	this.Ị_transactionState = v
}
func (this *_BaseDigitalGood) Id() *string {
	return this.Ị_id
}
func (this *_BaseDigitalGood) SetId(v *string) {
	this.Ị_id = v
}
func (this *_BaseDigitalGood) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_BaseDigitalGood) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_BaseDigitalGood) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_BaseDigitalGood) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_BaseDigitalGood) ReceiptId() *string {
	return this.Ị_receiptId
}
func (this *_BaseDigitalGood) SetReceiptId(v *string) {
	this.Ị_receiptId = v
}
func (this *_BaseDigitalGood) State() *string {
	return this.Ị_state
}
func (this *_BaseDigitalGood) SetState(v *string) {
	this.Ị_state = v
}
func (this *_BaseDigitalGood) Origin() DigitalGoodOrigin {
	return this.Ị_origin
}
func (this *_BaseDigitalGood) SetOrigin(v DigitalGoodOrigin) {
	this.Ị_origin = v
}
func (this *_BaseDigitalGood) __type() {
}
func NewBaseDigitalGood() BaseDigitalGood {
	return &_BaseDigitalGood{}
}
func init() {
	var val BaseDigitalGood
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("BaseDigitalGood", t, func() interface{} {
		return NewBaseDigitalGood()
	})
}

type Currency interface {
	__type()
	SetUnit(v *string)
	Unit() *string
	SetAmount(v *__big__.Rat)
	Amount() *__big__.Rat
}
type _Currency struct {
	Ị_amount *__big__.Rat `coral:"amount"`
	Ị_unit   *string      `coral:"unit"`
}

func (this *_Currency) Amount() *__big__.Rat {
	return this.Ị_amount
}
func (this *_Currency) SetAmount(v *__big__.Rat) {
	this.Ị_amount = v
}
func (this *_Currency) Unit() *string {
	return this.Ị_unit
}
func (this *_Currency) SetUnit(v *string) {
	this.Ị_unit = v
}
func (this *_Currency) __type() {
}
func NewCurrency() Currency {
	return &_Currency{}
}
func init() {
	var val Currency
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("Currency", t, func() interface{} {
		return NewCurrency()
	})
}

type AmazonCustomerInfo interface {
	__type()
	SetId(v *string)
	Id() *string
	SetFirstName(v *string)
	FirstName() *string
	SetLastName(v *string)
	LastName() *string
	SetShoppingId(v *string)
	ShoppingId() *string
	SetShoppingAccountPool(v *string)
	ShoppingAccountPool() *string
	SetDigitalId(v *string)
	DigitalId() *string
	SetDigitalAccountPool(v *string)
	DigitalAccountPool() *string
}
type _AmazonCustomerInfo struct {
	Ị_lastName            *string `coral:"lastName"`
	Ị_id                  *string `coral:"id"`
	Ị_firstName           *string `coral:"firstName"`
	Ị_shoppingId          *string `coral:"shoppingId"`
	Ị_shoppingAccountPool *string `coral:"shoppingAccountPool"`
	Ị_digitalId           *string `coral:"digitalId"`
	Ị_digitalAccountPool  *string `coral:"digitalAccountPool"`
}

func (this *_AmazonCustomerInfo) Id() *string {
	return this.Ị_id
}
func (this *_AmazonCustomerInfo) SetId(v *string) {
	this.Ị_id = v
}
func (this *_AmazonCustomerInfo) FirstName() *string {
	return this.Ị_firstName
}
func (this *_AmazonCustomerInfo) SetFirstName(v *string) {
	this.Ị_firstName = v
}
func (this *_AmazonCustomerInfo) LastName() *string {
	return this.Ị_lastName
}
func (this *_AmazonCustomerInfo) SetLastName(v *string) {
	this.Ị_lastName = v
}
func (this *_AmazonCustomerInfo) ShoppingAccountPool() *string {
	return this.Ị_shoppingAccountPool
}
func (this *_AmazonCustomerInfo) SetShoppingAccountPool(v *string) {
	this.Ị_shoppingAccountPool = v
}
func (this *_AmazonCustomerInfo) DigitalId() *string {
	return this.Ị_digitalId
}
func (this *_AmazonCustomerInfo) SetDigitalId(v *string) {
	this.Ị_digitalId = v
}
func (this *_AmazonCustomerInfo) DigitalAccountPool() *string {
	return this.Ị_digitalAccountPool
}
func (this *_AmazonCustomerInfo) SetDigitalAccountPool(v *string) {
	this.Ị_digitalAccountPool = v
}
func (this *_AmazonCustomerInfo) ShoppingId() *string {
	return this.Ị_shoppingId
}
func (this *_AmazonCustomerInfo) SetShoppingId(v *string) {
	this.Ị_shoppingId = v
}
func (this *_AmazonCustomerInfo) __type() {
}
func NewAmazonCustomerInfo() AmazonCustomerInfo {
	return &_AmazonCustomerInfo{}
}
func init() {
	var val AmazonCustomerInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.common.model").RegisterShape("AmazonCustomerInfo", t, func() interface{} {
		return NewAmazonCustomerInfo()
	})
}
func init() {
	var val map[*string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[*string]*string
		if f, ok := from.Interface().(map[*string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[*string]map[*string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[*string]map[*string]*string
		if f, ok := from.Interface().(map[*string]map[*string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to MapOfStringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []DigitalGood
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []DigitalGood
		if f, ok := from.Interface().([]DigitalGood); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Filter
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Filter
		if f, ok := from.Interface().([]Filter); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Filters")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*int64
		if f, ok := from.Interface().([]*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []KeyValue
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []KeyValue
		if f, ok := from.Interface().([]KeyValue); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to KeyValueList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Domain
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Domain
		if f, ok := from.Interface().([]Domain); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DomainSet")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []DigitalGoodTransaction
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []DigitalGoodTransaction
		if f, ok := from.Interface().([]DigitalGoodTransaction); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodTransactionList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
