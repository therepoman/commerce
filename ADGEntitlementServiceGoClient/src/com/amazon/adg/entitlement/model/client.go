package model

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//Provides the capability to access, verify, and modify a user's digital goods and entitlements.
type ADGEntitlementServiceClient struct {
	C __client__.Client
}

//Creates a new ADGEntitlementServiceClient
func NewADGEntitlementServiceClient(dialer __dialer__.Dialer, codec __codec__.Codec) (service *ADGEntitlementServiceClient) {
	return &ADGEntitlementServiceClient{__client__.NewClient("com.amazon.adg.entitlement.model", "ADGEntitlementService", dialer, codec)}
}

//Retrieves the digital goods for a specific customer.
//
//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#getGoods
func (this *ADGEntitlementServiceClient) GetGoods(input GetGoodsRequest) (GetGoodsResponse, error) {
	var output GetGoodsResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "getGoods", input, &output)
	return output, err
}

//Consume additional value for a given stored value entry.
func (this *ADGEntitlementServiceClient) ConsumeValue(input ConsumeValueRequest) (ConsumeValueResponse, error) {
	var output ConsumeValueResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "consumeValue", input, &output)
	return output, err
}

//Convert a given digital good to additional value for a given stored value entry in a transactional way.
//If the good is a consumable, it will be consumed.  If the good is an entitlement, it will be revoked.
//It is recommend that you use this primarily with consumables.
func (this *ADGEntitlementServiceClient) ExchangeGoodForStoredValue(input ExchangeGoodForStoredValueRequest) (ExchangeGoodForStoredValueResponse, error) {
	var output ExchangeGoodForStoredValueResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "exchangeGoodForStoredValue", input, &output)
	return output, err
}

//Deletes a digital good for a customer.  This will mark it for deletion, and later it will
//be removed from the underlying datastore.
func (this *ADGEntitlementServiceClient) DeleteGood(input DeleteGoodRequest) (DeleteGoodResponse, error) {
	var output DeleteGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "deleteGood", input, &output)
	return output, err
}

//Retrieves a digital good given a customerId and a digitalGoodId.
//
//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#getGood
func (this *ADGEntitlementServiceClient) GetGood(input GetGoodRequest) (GetGoodResponse, error) {
	var output GetGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "getGood", input, &output)
	return output, err
}

//Retrieves the digital goods for a specific customer from a given sync point
//or cursor.
//
//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#syncGoods
func (this *ADGEntitlementServiceClient) SyncGoods(input SyncGoodsRequest) (SyncGoodsResponse, error) {
	var output SyncGoodsResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "syncGoods", input, &output)
	return output, err
}

//Verify the  digital goods for a specific customer are present and live.
func (this *ADGEntitlementServiceClient) VerifyGoods(input VerifyGoodsRequest) (VerifyGoodsResponse, error) {
	var output VerifyGoodsResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "verifyGoods", input, &output)
	return output, err
}

//Revoke a digital good.  This should be a rare event; good revocation is not a normal life-cycle event.
//
//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#revokeGood
func (this *ADGEntitlementServiceClient) RevokeGood(input RevokeGoodRequest) (RevokeGoodResponse, error) {
	var output RevokeGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "revokeGood", input, &output)
	return output, err
}

//Expire a subscription digital good.  Expiration and renewal are controlled by external life-cycles.
func (this *ADGEntitlementServiceClient) ExpireGood(input ExpireGoodRequest) (ExpireGoodResponse, error) {
	var output ExpireGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "expireGood", input, &output)
	return output, err
}

//Renew a subscription digital good.  Renewal and expiration are controlled by external life-cycles.
func (this *ADGEntitlementServiceClient) RenewGood(input RenewGoodRequest) (RenewGoodResponse, error) {
	var output RenewGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "renewGood", input, &output)
	return output, err
}

//Create a new digital good for a specific customer and product.  This API is
//when there is no fulfillment event such as an order.
//
//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#createGood
func (this *ADGEntitlementServiceClient) CreateGood(input CreateGoodRequest) (CreateGoodResponse, error) {
	var output CreateGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "createGood", input, &output)
	return output, err
}

//Update the fulfillment state for a digital good.
//
//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#updateGoodFulfillment
func (this *ADGEntitlementServiceClient) UpdateGoodFulfillment(input UpdateGoodFulfillmentRequest) (UpdateGoodFulfillmentResponse, error) {
	var output UpdateGoodFulfillmentResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "updateGoodFulfillment", input, &output)
	return output, err
}

//Consume a consumable digital good. Will fail if the good is already consumed.
func (this *ADGEntitlementServiceClient) ConsumeGood(input ConsumeGoodRequest) (ConsumeGoodResponse, error) {
	var output ConsumeGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "consumeGood", input, &output)
	return output, err
}

//Refill a consumable digital good.  This should be a rare event; refilling a consumableis not a normal life-cycle event.
func (this *ADGEntitlementServiceClient) RefillGood(input RefillGoodRequest) (RefillGoodResponse, error) {
	var output RefillGoodResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "refillGood", input, &output)
	return output, err
}

//Consume value from a given stored value entry and create digital goods as a result in a transactional way.
//This method allows you to convert stored value into a digital good.
func (this *ADGEntitlementServiceClient) ConsumeValueAndCreateGoods(input ConsumeValueAndCreateGoodsRequest) (ConsumeValueAndCreateGoodsResponse, error) {
	var output ConsumeValueAndCreateGoodsResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "consumeValueAndCreateGoods", input, &output)
	return output, err
}

//Store additional value for a given stored value entry.  If there is no stored value entry one will be created.
func (this *ADGEntitlementServiceClient) StoreValue(input StoreValueRequest) (StoreValueResponse, error) {
	var output StoreValueResponse
	err := this.C.Call("com.amazon.adg.entitlement.model", "storeValue", input, &output)
	return output, err
}
