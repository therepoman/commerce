package model

import (
	model "code.justin.tv/commerce/ADGEntitlementServiceGoClient/src/com/amazon/adg/common/model"
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type GetGoodsResponse interface {
	__type()
	SetGoods(v []model.DigitalGood)
	Goods() []model.DigitalGood
}
type _GetGoodsResponse struct {
	Ị_goods []model.DigitalGood `coral:"goods"`
}

func (this *_GetGoodsResponse) Goods() []model.DigitalGood {
	return this.Ị_goods
}
func (this *_GetGoodsResponse) SetGoods(v []model.DigitalGood) {
	this.Ị_goods = v
}
func (this *_GetGoodsResponse) __type() {
}
func NewGetGoodsResponse() GetGoodsResponse {
	return &_GetGoodsResponse{}
}
func init() {
	var val GetGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodsResponse", t, func() interface{} {
		return NewGetGoodsResponse()
	})
}

type ExpireGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _ExpireGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_ExpireGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ExpireGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ExpireGoodRequest) __type() {
}
func NewExpireGoodRequest() ExpireGoodRequest {
	return &_ExpireGoodRequest{}
}
func init() {
	var val ExpireGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ExpireGoodRequest", t, func() interface{} {
		return NewExpireGoodRequest()
	})
}

type RestoreGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RestoreGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RestoreGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RestoreGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RestoreGoodResponse) __type() {
}
func NewRestoreGoodResponse() RestoreGoodResponse {
	return &_RestoreGoodResponse{}
}
func init() {
	var val RestoreGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RestoreGoodResponse", t, func() interface{} {
		return NewRestoreGoodResponse()
	})
}

type ExchangeGoodForStoredValueRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
	SetForStoredValue(v StoreValueRequest)
	ForStoredValue() StoreValueRequest
}
type _ExchangeGoodForStoredValueRequest struct {
	Ị_forStoredValue StoreValueRequest `coral:"forStoredValue"`
	Ị_good           model.DigitalGood `coral:"good"`
}

func (this *_ExchangeGoodForStoredValueRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ExchangeGoodForStoredValueRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ExchangeGoodForStoredValueRequest) ForStoredValue() StoreValueRequest {
	return this.Ị_forStoredValue
}
func (this *_ExchangeGoodForStoredValueRequest) SetForStoredValue(v StoreValueRequest) {
	this.Ị_forStoredValue = v
}
func (this *_ExchangeGoodForStoredValueRequest) __type() {
}
func NewExchangeGoodForStoredValueRequest() ExchangeGoodForStoredValueRequest {
	return &_ExchangeGoodForStoredValueRequest{}
}
func init() {
	var val ExchangeGoodForStoredValueRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ExchangeGoodForStoredValueRequest", t, func() interface{} {
		return NewExchangeGoodForStoredValueRequest()
	})
}

type GetGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _GetGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_GetGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_GetGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_GetGoodResponse) __type() {
}
func NewGetGoodResponse() GetGoodResponse {
	return &_GetGoodResponse{}
}
func init() {
	var val GetGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodResponse", t, func() interface{} {
		return NewGetGoodResponse()
	})
}

type UpdateGoodFulfillmentResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _UpdateGoodFulfillmentResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_UpdateGoodFulfillmentResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_UpdateGoodFulfillmentResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_UpdateGoodFulfillmentResponse) __type() {
}
func NewUpdateGoodFulfillmentResponse() UpdateGoodFulfillmentResponse {
	return &_UpdateGoodFulfillmentResponse{}
}
func init() {
	var val UpdateGoodFulfillmentResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("UpdateGoodFulfillmentResponse", t, func() interface{} {
		return NewUpdateGoodFulfillmentResponse()
	})
}

type RestoreGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RestoreGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RestoreGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RestoreGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RestoreGoodRequest) __type() {
}
func NewRestoreGoodRequest() RestoreGoodRequest {
	return &_RestoreGoodRequest{}
}
func init() {
	var val RestoreGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RestoreGoodRequest", t, func() interface{} {
		return NewRestoreGoodRequest()
	})
}

type ServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _ServiceException struct {
	Ị_message *string `coral:"message"`
}

func (this *_ServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ServiceException) Message() *string {
	return this.Ị_message
}
func (this *_ServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_ServiceException) __type() {
}
func NewServiceException() ServiceException {
	return &_ServiceException{}
}
func init() {
	var val ServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ServiceException", t, func() interface{} {
		return NewServiceException()
	})
}

//The client has submitted a request that is invalid, either with bad parameters, missing parameters,
//or unrecognized parameters.
type InvalidRequestException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidRequestException struct {
	Ị_message *string `coral:"message"`
}

func (this *_InvalidRequestException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidRequestException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidRequestException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InvalidRequestException) __type() {
}
func NewInvalidRequestException() InvalidRequestException {
	return &_InvalidRequestException{}
}
func init() {
	var val InvalidRequestException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("InvalidRequestException", t, func() interface{} {
		return NewInvalidRequestException()
	})
}

//This exception is thrown on another service or resource this service depends on has an error
type DependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	ServiceException
	Ị_message *string `coral:"message"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

type CreateGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _CreateGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_CreateGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_CreateGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_CreateGoodRequest) __type() {
}
func NewCreateGoodRequest() CreateGoodRequest {
	return &_CreateGoodRequest{}
}
func init() {
	var val CreateGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("CreateGoodRequest", t, func() interface{} {
		return NewCreateGoodRequest()
	})
}

type RenewGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RenewGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RenewGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RenewGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RenewGoodResponse) __type() {
}
func NewRenewGoodResponse() RenewGoodResponse {
	return &_RenewGoodResponse{}
}
func init() {
	var val RenewGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RenewGoodResponse", t, func() interface{} {
		return NewRenewGoodResponse()
	})
}

type ExchangeGoodForStoredValueResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
	SetStoredValue(v model.StoredValue)
	StoredValue() model.StoredValue
}
type _ExchangeGoodForStoredValueResponse struct {
	Ị_good        model.DigitalGood `coral:"good"`
	Ị_storedValue model.StoredValue `coral:"storedValue"`
}

func (this *_ExchangeGoodForStoredValueResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ExchangeGoodForStoredValueResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ExchangeGoodForStoredValueResponse) StoredValue() model.StoredValue {
	return this.Ị_storedValue
}
func (this *_ExchangeGoodForStoredValueResponse) SetStoredValue(v model.StoredValue) {
	this.Ị_storedValue = v
}
func (this *_ExchangeGoodForStoredValueResponse) __type() {
}
func NewExchangeGoodForStoredValueResponse() ExchangeGoodForStoredValueResponse {
	return &_ExchangeGoodForStoredValueResponse{}
}
func init() {
	var val ExchangeGoodForStoredValueResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ExchangeGoodForStoredValueResponse", t, func() interface{} {
		return NewExchangeGoodForStoredValueResponse()
	})
}

type ConsumeValueAndCreateGoodsRequest interface {
	__type()
	SetConsumeStoredValue(v ConsumeValueRequest)
	ConsumeStoredValue() ConsumeValueRequest
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _ConsumeValueAndCreateGoodsRequest struct {
	Ị_consumeStoredValue ConsumeValueRequest `coral:"consumeStoredValue"`
	Ị_good               model.DigitalGood   `coral:"good"`
}

func (this *_ConsumeValueAndCreateGoodsRequest) ConsumeStoredValue() ConsumeValueRequest {
	return this.Ị_consumeStoredValue
}
func (this *_ConsumeValueAndCreateGoodsRequest) SetConsumeStoredValue(v ConsumeValueRequest) {
	this.Ị_consumeStoredValue = v
}
func (this *_ConsumeValueAndCreateGoodsRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ConsumeValueAndCreateGoodsRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ConsumeValueAndCreateGoodsRequest) __type() {
}
func NewConsumeValueAndCreateGoodsRequest() ConsumeValueAndCreateGoodsRequest {
	return &_ConsumeValueAndCreateGoodsRequest{}
}
func init() {
	var val ConsumeValueAndCreateGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeValueAndCreateGoodsRequest", t, func() interface{} {
		return NewConsumeValueAndCreateGoodsRequest()
	})
}

type SyncGoodsRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetFilters(v []model.Filter)
	Filters() []model.Filter
	SetStartSync(v SyncPointData)
	StartSync() SyncPointData
}
type _SyncGoodsRequest struct {
	Ị_language        *string                   `coral:"language"`
	Ị_client          model.ClientInfo          `coral:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale"`
	Ị_filters         []model.Filter            `coral:"filters"`
	Ị_startSync       SyncPointData             `coral:"startSync"`
}

func (this *_SyncGoodsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_SyncGoodsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_SyncGoodsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_SyncGoodsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_SyncGoodsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_SyncGoodsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_SyncGoodsRequest) Language() *string {
	return this.Ị_language
}
func (this *_SyncGoodsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_SyncGoodsRequest) Filters() []model.Filter {
	return this.Ị_filters
}
func (this *_SyncGoodsRequest) SetFilters(v []model.Filter) {
	this.Ị_filters = v
}
func (this *_SyncGoodsRequest) StartSync() SyncPointData {
	return this.Ị_startSync
}
func (this *_SyncGoodsRequest) SetStartSync(v SyncPointData) {
	this.Ị_startSync = v
}
func (this *_SyncGoodsRequest) __type() {
}
func NewSyncGoodsRequest() SyncGoodsRequest {
	return &_SyncGoodsRequest{}
}
func init() {
	var val SyncGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("SyncGoodsRequest", t, func() interface{} {
		return NewSyncGoodsRequest()
	})
}

type StoreValueResponse interface {
	__type()
	SetStoredValue(v model.StoredValue)
	StoredValue() model.StoredValue
}
type _StoreValueResponse struct {
	Ị_storedValue model.StoredValue `coral:"storedValue"`
}

func (this *_StoreValueResponse) StoredValue() model.StoredValue {
	return this.Ị_storedValue
}
func (this *_StoreValueResponse) SetStoredValue(v model.StoredValue) {
	this.Ị_storedValue = v
}
func (this *_StoreValueResponse) __type() {
}
func NewStoreValueResponse() StoreValueResponse {
	return &_StoreValueResponse{}
}
func init() {
	var val StoreValueResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("StoreValueResponse", t, func() interface{} {
		return NewStoreValueResponse()
	})
}

type SyncGoodsResponse interface {
	__type()
	SetNextSync(v SyncPointData)
	NextSync() SyncPointData
	SetGoods(v []model.DigitalGood)
	Goods() []model.DigitalGood
}
type _SyncGoodsResponse struct {
	Ị_nextSync SyncPointData       `coral:"nextSync"`
	Ị_goods    []model.DigitalGood `coral:"goods"`
}

func (this *_SyncGoodsResponse) Goods() []model.DigitalGood {
	return this.Ị_goods
}
func (this *_SyncGoodsResponse) SetGoods(v []model.DigitalGood) {
	this.Ị_goods = v
}
func (this *_SyncGoodsResponse) NextSync() SyncPointData {
	return this.Ị_nextSync
}
func (this *_SyncGoodsResponse) SetNextSync(v SyncPointData) {
	this.Ị_nextSync = v
}
func (this *_SyncGoodsResponse) __type() {
}
func NewSyncGoodsResponse() SyncGoodsResponse {
	return &_SyncGoodsResponse{}
}
func init() {
	var val SyncGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("SyncGoodsResponse", t, func() interface{} {
		return NewSyncGoodsResponse()
	})
}

type VerifyGoodsRequest interface {
	__type()
	SetLanguage(v *string)
	Language() *string
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetGoods(v []model.DigitalGood)
	Goods() []model.DigitalGood
}
type _VerifyGoodsRequest struct {
	Ị_client          model.ClientInfo          `coral:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale"`
	Ị_language        *string                   `coral:"language"`
	Ị_goods           []model.DigitalGood       `coral:"goods"`
}

func (this *_VerifyGoodsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_VerifyGoodsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_VerifyGoodsRequest) Language() *string {
	return this.Ị_language
}
func (this *_VerifyGoodsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_VerifyGoodsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_VerifyGoodsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_VerifyGoodsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_VerifyGoodsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_VerifyGoodsRequest) Goods() []model.DigitalGood {
	return this.Ị_goods
}
func (this *_VerifyGoodsRequest) SetGoods(v []model.DigitalGood) {
	this.Ị_goods = v
}
func (this *_VerifyGoodsRequest) __type() {
}
func NewVerifyGoodsRequest() VerifyGoodsRequest {
	return &_VerifyGoodsRequest{}
}
func init() {
	var val VerifyGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("VerifyGoodsRequest", t, func() interface{} {
		return NewVerifyGoodsRequest()
	})
}

type VerifyGoodsResponse interface {
	__type()
	SetStatus(v map[*string]*string)
	Status() map[*string]*string
}
type _VerifyGoodsResponse struct {
	Ị_status map[*string]*string `coral:"status"`
}

func (this *_VerifyGoodsResponse) Status() map[*string]*string {
	return this.Ị_status
}
func (this *_VerifyGoodsResponse) SetStatus(v map[*string]*string) {
	this.Ị_status = v
}
func (this *_VerifyGoodsResponse) __type() {
}
func NewVerifyGoodsResponse() VerifyGoodsResponse {
	return &_VerifyGoodsResponse{}
}
func init() {
	var val VerifyGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("VerifyGoodsResponse", t, func() interface{} {
		return NewVerifyGoodsResponse()
	})
}

type ConsumeValueResponse interface {
	__type()
	SetStoredValue(v model.StoredValue)
	StoredValue() model.StoredValue
}
type _ConsumeValueResponse struct {
	Ị_storedValue model.StoredValue `coral:"storedValue"`
}

func (this *_ConsumeValueResponse) StoredValue() model.StoredValue {
	return this.Ị_storedValue
}
func (this *_ConsumeValueResponse) SetStoredValue(v model.StoredValue) {
	this.Ị_storedValue = v
}
func (this *_ConsumeValueResponse) __type() {
}
func NewConsumeValueResponse() ConsumeValueResponse {
	return &_ConsumeValueResponse{}
}
func init() {
	var val ConsumeValueResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeValueResponse", t, func() interface{} {
		return NewConsumeValueResponse()
	})
}

type RefillGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RefillGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RefillGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RefillGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RefillGoodResponse) __type() {
}
func NewRefillGoodResponse() RefillGoodResponse {
	return &_RefillGoodResponse{}
}
func init() {
	var val RefillGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RefillGoodResponse", t, func() interface{} {
		return NewRefillGoodResponse()
	})
}

type ConsumeValueRequest interface {
	__type()
	SetStoredValue(v model.StoredValue)
	StoredValue() model.StoredValue
	SetConsumeValue(v *float64)
	ConsumeValue() *float64
}
type _ConsumeValueRequest struct {
	Ị_consumeValue *float64          `coral:"consumeValue"`
	Ị_storedValue  model.StoredValue `coral:"storedValue"`
}

func (this *_ConsumeValueRequest) StoredValue() model.StoredValue {
	return this.Ị_storedValue
}
func (this *_ConsumeValueRequest) SetStoredValue(v model.StoredValue) {
	this.Ị_storedValue = v
}
func (this *_ConsumeValueRequest) ConsumeValue() *float64 {
	return this.Ị_consumeValue
}
func (this *_ConsumeValueRequest) SetConsumeValue(v *float64) {
	this.Ị_consumeValue = v
}
func (this *_ConsumeValueRequest) __type() {
}
func NewConsumeValueRequest() ConsumeValueRequest {
	return &_ConsumeValueRequest{}
}
func init() {
	var val ConsumeValueRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeValueRequest", t, func() interface{} {
		return NewConsumeValueRequest()
	})
}

//Exception for any hard errors caused by internal failures.
type InternalServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InternalServiceException struct {
	ServiceException
	Ị_message *string `coral:"message"`
}

func (this *_InternalServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InternalServiceException) Message() *string {
	return this.Ị_message
}
func (this *_InternalServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InternalServiceException) __type() {
}
func NewInternalServiceException() InternalServiceException {
	return &_InternalServiceException{}
}
func init() {
	var val InternalServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("InternalServiceException", t, func() interface{} {
		return NewInternalServiceException()
	})
}

type GetGoodRequest interface {
	__type()
	SetLanguage(v *string)
	Language() *string
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetInstanceId(v *string)
	InstanceId() *string
}
type _GetGoodRequest struct {
	Ị_client          model.ClientInfo          `coral:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale"`
	Ị_language        *string                   `coral:"language"`
	Ị_instanceId      *string                   `coral:"instanceId"`
}

func (this *_GetGoodRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetGoodRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetGoodRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetGoodRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetGoodRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetGoodRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetGoodRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetGoodRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetGoodRequest) InstanceId() *string {
	return this.Ị_instanceId
}
func (this *_GetGoodRequest) SetInstanceId(v *string) {
	this.Ị_instanceId = v
}
func (this *_GetGoodRequest) __type() {
}
func NewGetGoodRequest() GetGoodRequest {
	return &_GetGoodRequest{}
}
func init() {
	var val GetGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodRequest", t, func() interface{} {
		return NewGetGoodRequest()
	})
}

type UpdateGoodFulfillmentRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
	SetFulfillmentState(v *string)
	FulfillmentState() *string
}
type _UpdateGoodFulfillmentRequest struct {
	Ị_good             model.DigitalGood `coral:"good"`
	Ị_fulfillmentState *string           `coral:"fulfillmentState"`
}

func (this *_UpdateGoodFulfillmentRequest) FulfillmentState() *string {
	return this.Ị_fulfillmentState
}
func (this *_UpdateGoodFulfillmentRequest) SetFulfillmentState(v *string) {
	this.Ị_fulfillmentState = v
}
func (this *_UpdateGoodFulfillmentRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_UpdateGoodFulfillmentRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_UpdateGoodFulfillmentRequest) __type() {
}
func NewUpdateGoodFulfillmentRequest() UpdateGoodFulfillmentRequest {
	return &_UpdateGoodFulfillmentRequest{}
}
func init() {
	var val UpdateGoodFulfillmentRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("UpdateGoodFulfillmentRequest", t, func() interface{} {
		return NewUpdateGoodFulfillmentRequest()
	})
}

type ConsumeGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _ConsumeGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_ConsumeGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ConsumeGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ConsumeGoodRequest) __type() {
}
func NewConsumeGoodRequest() ConsumeGoodRequest {
	return &_ConsumeGoodRequest{}
}
func init() {
	var val ConsumeGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeGoodRequest", t, func() interface{} {
		return NewConsumeGoodRequest()
	})
}

type ExpireGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _ExpireGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_ExpireGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ExpireGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ExpireGoodResponse) __type() {
}
func NewExpireGoodResponse() ExpireGoodResponse {
	return &_ExpireGoodResponse{}
}
func init() {
	var val ExpireGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ExpireGoodResponse", t, func() interface{} {
		return NewExpireGoodResponse()
	})
}

type RefillGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RefillGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RefillGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RefillGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RefillGoodRequest) __type() {
}
func NewRefillGoodRequest() RefillGoodRequest {
	return &_RefillGoodRequest{}
}
func init() {
	var val RefillGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RefillGoodRequest", t, func() interface{} {
		return NewRefillGoodRequest()
	})
}

type StoreValueRequest interface {
	__type()
	SetStoredValue(v model.StoredValue)
	StoredValue() model.StoredValue
	SetNewValue(v *float64)
	NewValue() *float64
}
type _StoreValueRequest struct {
	Ị_storedValue model.StoredValue `coral:"storedValue"`
	Ị_newValue    *float64          `coral:"newValue"`
}

func (this *_StoreValueRequest) StoredValue() model.StoredValue {
	return this.Ị_storedValue
}
func (this *_StoreValueRequest) SetStoredValue(v model.StoredValue) {
	this.Ị_storedValue = v
}
func (this *_StoreValueRequest) NewValue() *float64 {
	return this.Ị_newValue
}
func (this *_StoreValueRequest) SetNewValue(v *float64) {
	this.Ị_newValue = v
}
func (this *_StoreValueRequest) __type() {
}
func NewStoreValueRequest() StoreValueRequest {
	return &_StoreValueRequest{}
}
func init() {
	var val StoreValueRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("StoreValueRequest", t, func() interface{} {
		return NewStoreValueRequest()
	})
}

type ConsumeValueAndCreateGoodsResponse interface {
	__type()
	SetStoredValue(v model.StoredValue)
	StoredValue() model.StoredValue
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _ConsumeValueAndCreateGoodsResponse struct {
	Ị_storedValue model.StoredValue `coral:"storedValue"`
	Ị_good        model.DigitalGood `coral:"good"`
}

func (this *_ConsumeValueAndCreateGoodsResponse) StoredValue() model.StoredValue {
	return this.Ị_storedValue
}
func (this *_ConsumeValueAndCreateGoodsResponse) SetStoredValue(v model.StoredValue) {
	this.Ị_storedValue = v
}
func (this *_ConsumeValueAndCreateGoodsResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ConsumeValueAndCreateGoodsResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ConsumeValueAndCreateGoodsResponse) __type() {
}
func NewConsumeValueAndCreateGoodsResponse() ConsumeValueAndCreateGoodsResponse {
	return &_ConsumeValueAndCreateGoodsResponse{}
}
func init() {
	var val ConsumeValueAndCreateGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeValueAndCreateGoodsResponse", t, func() interface{} {
		return NewConsumeValueAndCreateGoodsResponse()
	})
}

type DeleteGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _DeleteGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_DeleteGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_DeleteGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_DeleteGoodResponse) __type() {
}
func NewDeleteGoodResponse() DeleteGoodResponse {
	return &_DeleteGoodResponse{}
}
func init() {
	var val DeleteGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("DeleteGoodResponse", t, func() interface{} {
		return NewDeleteGoodResponse()
	})
}

type SyncPointData interface {
	__type()
	SetSyncToken(v *string)
	SyncToken() *string
	SetSyncPoint(v *__time__.Time)
	SyncPoint() *__time__.Time
}
type _SyncPointData struct {
	Ị_syncToken *string        `coral:"syncToken"`
	Ị_syncPoint *__time__.Time `coral:"syncPoint"`
}

func (this *_SyncPointData) SyncToken() *string {
	return this.Ị_syncToken
}
func (this *_SyncPointData) SetSyncToken(v *string) {
	this.Ị_syncToken = v
}
func (this *_SyncPointData) SyncPoint() *__time__.Time {
	return this.Ị_syncPoint
}
func (this *_SyncPointData) SetSyncPoint(v *__time__.Time) {
	this.Ị_syncPoint = v
}
func (this *_SyncPointData) __type() {
}
func NewSyncPointData() SyncPointData {
	return &_SyncPointData{}
}
func init() {
	var val SyncPointData
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("SyncPointData", t, func() interface{} {
		return NewSyncPointData()
	})
}

type CreateGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _CreateGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_CreateGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_CreateGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_CreateGoodResponse) __type() {
}
func NewCreateGoodResponse() CreateGoodResponse {
	return &_CreateGoodResponse{}
}
func init() {
	var val CreateGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("CreateGoodResponse", t, func() interface{} {
		return NewCreateGoodResponse()
	})
}

type RevokeGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RevokeGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RevokeGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RevokeGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RevokeGoodResponse) __type() {
}
func NewRevokeGoodResponse() RevokeGoodResponse {
	return &_RevokeGoodResponse{}
}
func init() {
	var val RevokeGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RevokeGoodResponse", t, func() interface{} {
		return NewRevokeGoodResponse()
	})
}

type ConsumeGoodResponse interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _ConsumeGoodResponse struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_ConsumeGoodResponse) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_ConsumeGoodResponse) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ConsumeGoodResponse) __type() {
}
func NewConsumeGoodResponse() ConsumeGoodResponse {
	return &_ConsumeGoodResponse{}
}
func init() {
	var val ConsumeGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeGoodResponse", t, func() interface{} {
		return NewConsumeGoodResponse()
	})
}

//An exception that is thrown when there is a collision over digital good id. This usually means
//the client supplied a UUID for the digital good id and that UUID already exists.
type DigitalGoodExistsException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DigitalGoodExistsException struct {
	InvalidRequestException
	Ị_message *string `coral:"message"`
}

func (this *_DigitalGoodExistsException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DigitalGoodExistsException) Message() *string {
	return this.Ị_message
}
func (this *_DigitalGoodExistsException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DigitalGoodExistsException) __type() {
}
func NewDigitalGoodExistsException() DigitalGoodExistsException {
	return &_DigitalGoodExistsException{}
}
func init() {
	var val DigitalGoodExistsException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("DigitalGoodExistsException", t, func() interface{} {
		return NewDigitalGoodExistsException()
	})
}

type GetGoodsRequest interface {
	__type()
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetFilters(v []model.Filter)
	Filters() []model.Filter
}
type _GetGoodsRequest struct {
	Ị_language        *string                   `coral:"language"`
	Ị_client          model.ClientInfo          `coral:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale"`
	Ị_filters         []model.Filter            `coral:"filters"`
}

func (this *_GetGoodsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetGoodsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetGoodsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetGoodsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetGoodsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetGoodsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetGoodsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetGoodsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetGoodsRequest) Filters() []model.Filter {
	return this.Ị_filters
}
func (this *_GetGoodsRequest) SetFilters(v []model.Filter) {
	this.Ị_filters = v
}
func (this *_GetGoodsRequest) __type() {
}
func NewGetGoodsRequest() GetGoodsRequest {
	return &_GetGoodsRequest{}
}
func init() {
	var val GetGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodsRequest", t, func() interface{} {
		return NewGetGoodsRequest()
	})
}

type RevokeGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RevokeGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RevokeGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RevokeGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RevokeGoodRequest) __type() {
}
func NewRevokeGoodRequest() RevokeGoodRequest {
	return &_RevokeGoodRequest{}
}
func init() {
	var val RevokeGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RevokeGoodRequest", t, func() interface{} {
		return NewRevokeGoodRequest()
	})
}

type RenewGoodRequest interface {
	__type()
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _RenewGoodRequest struct {
	Ị_good model.DigitalGood `coral:"good"`
}

func (this *_RenewGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_RenewGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RenewGoodRequest) __type() {
}
func NewRenewGoodRequest() RenewGoodRequest {
	return &_RenewGoodRequest{}
}
func init() {
	var val RenewGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("RenewGoodRequest", t, func() interface{} {
		return NewRenewGoodRequest()
	})
}

type DeleteGoodRequest interface {
	__type()
	SetLanguage(v *string)
	Language() *string
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetGood(v model.DigitalGood)
	Good() model.DigitalGood
}
type _DeleteGoodRequest struct {
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale"`
	Ị_language        *string                   `coral:"language"`
	Ị_client          model.ClientInfo          `coral:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer"`
	Ị_good            model.DigitalGood         `coral:"good"`
}

func (this *_DeleteGoodRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_DeleteGoodRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_DeleteGoodRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_DeleteGoodRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_DeleteGoodRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_DeleteGoodRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_DeleteGoodRequest) Language() *string {
	return this.Ị_language
}
func (this *_DeleteGoodRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_DeleteGoodRequest) Good() model.DigitalGood {
	return this.Ị_good
}
func (this *_DeleteGoodRequest) SetGood(v model.DigitalGood) {
	this.Ị_good = v
}
func (this *_DeleteGoodRequest) __type() {
}
func NewDeleteGoodRequest() DeleteGoodRequest {
	return &_DeleteGoodRequest{}
}
func init() {
	var val DeleteGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.entitlement.model").RegisterShape("DeleteGoodRequest", t, func() interface{} {
		return NewDeleteGoodRequest()
	})
}
func init() {
	var val map[*string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[*string]*string
		if f, ok := from.Interface().(map[*string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[*string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[*string]*string
		if f, ok := from.Interface().(map[*string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to GoodsStatusMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*int64
		if f, ok := from.Interface().([]*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//Provides the capability to access, verify, and modify a user's digital goods and entitlements.
type ADGEntitlementService interface { //Retrieves the digital goods for a specific customer.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#getGoods
	GetGoods(input GetGoodsRequest) (GetGoodsResponse, error)             //Consume additional value for a given stored value entry.
	ConsumeValue(input ConsumeValueRequest) (ConsumeValueResponse, error) //Convert a given digital good to additional value for a given stored value entry in a transactional way.
	//If the good is a consumable, it will be consumed.  If the good is an entitlement, it will be revoked.
	//It is recommend that you use this primarily with consumables.
	ExchangeGoodForStoredValue(input ExchangeGoodForStoredValueRequest) (ExchangeGoodForStoredValueResponse, error) //Deletes a digital good for a customer.  This will mark it for deletion, and later it will
	//be removed from the underlying datastore.
	DeleteGood(input DeleteGoodRequest) (DeleteGoodResponse, error) //Retrieves a digital good given a customerId and a digitalGoodId.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#getGood
	GetGood(input GetGoodRequest) (GetGoodResponse, error) //Retrieves the digital goods for a specific customer from a given sync point
	//or cursor.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#syncGoods
	SyncGoods(input SyncGoodsRequest) (SyncGoodsResponse, error)       //Verify the  digital goods for a specific customer are present and live.
	VerifyGoods(input VerifyGoodsRequest) (VerifyGoodsResponse, error) //Revoke a digital good.  This should be a rare event; good revocation is not a normal life-cycle event.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#revokeGood
	RevokeGood(input RevokeGoodRequest) (RevokeGoodResponse, error) //Expire a subscription digital good.  Expiration and renewal are controlled by external life-cycles.
	ExpireGood(input ExpireGoodRequest) (ExpireGoodResponse, error) //Renew a subscription digital good.  Renewal and expiration are controlled by external life-cycles.
	RenewGood(input RenewGoodRequest) (RenewGoodResponse, error)    //Create a new digital good for a specific customer and product.  This API is
	//when there is no fulfillment event such as an order.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#createGood
	CreateGood(input CreateGoodRequest) (CreateGoodResponse, error) //Update the fulfillment state for a digital good.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#updateGoodFulfillment
	UpdateGoodFulfillment(input UpdateGoodFulfillmentRequest) (UpdateGoodFulfillmentResponse, error) //Consume a consumable digital good. Will fail if the good is already consumed.
	ConsumeGood(input ConsumeGoodRequest) (ConsumeGoodResponse, error)                               //Refill a consumable digital good.  This should be a rare event; refilling a consumableis not a normal life-cycle event.
	RefillGood(input RefillGoodRequest) (RefillGoodResponse, error)                                  //Consume value from a given stored value entry and create digital goods as a result in a transactional way.
	//This method allows you to convert stored value into a digital good.
	ConsumeValueAndCreateGoods(input ConsumeValueAndCreateGoodsRequest) (ConsumeValueAndCreateGoodsResponse, error) //Store additional value for a given stored value entry.  If there is no stored value entry one will be created.
	StoreValue(input StoreValueRequest) (StoreValueResponse, error)
}
