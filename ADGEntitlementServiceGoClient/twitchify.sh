for i in `find src -name "*.go"`
do
  sed -i.pkg 's/__client__ "coral\/client"/__client__ "code.justin.tv\/commerce\/CoralGoClient\/src\/coral\/client"/g' $i
  sed -i.pkg 's/__dialer__ "coral\/dialer"/__dialer__ "code.justin.tv\/commerce\/CoralGoClient\/src\/coral\/dialer"/g' $i
  sed -i.pkg 's/__model__ "coral\/model"/__model__ "code.justin.tv\/commerce\/CoralGoModel\/src\/coral\/model"/g' $i
  sed -i.pkg 's/__codec__ "coral\/codec"/__codec__ "code.justin.tv\/commerce\/CoralGoCodec\/src\/coral\/codec"/g' $i
  sed -i.pkg 's/model "com\/amazon\/adg\/common\/model"/model "code.justin.tv\/commerce\/ADGEntitlementServiceGoClient\/src\/com\/amazon\/adg\/common\/model"/g' $i
done
find . -name '*.pkg' | xargs rm -rf
