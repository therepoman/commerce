package simplify

import "strings"

var codeExtensions = map[string]bool{
	".go": true,
	".rb": true,
	".py": true,
	".sh": true,
	".js": true,
	".ts": true,
}

var terraformExtensions = map[string]bool{
	".tf": true,
}

var possibleCDKExtensions = map[string]bool{
	".ts": true,
	".js": true,
}

var possibleCloudformationExtensions = map[string]bool{
	".json": true,
	".yaml": true,
	".yml": true,
	".rb": true,
}

var cloudFormationPathPatterns = []string{
	"cloudformation",
	"lpt",
}

var cdkPathPatterns = []string{
	"cdk",
}

func IsCodeFile(extension string) bool {
	_, isCode := codeExtensions[extension]

	return isCode
}

func isCDKFile(path, extension string) bool {
	_, possiblyCDK := possibleCDKExtensions[extension]
	if !possiblyCDK {
		return false
	}

	// Casts a wide enough net given what exists in commerce today. Could always add some file inspection.
	for _, pattern := range cdkPathPatterns {
		if strings.Contains(strings.ToLower(path), pattern) {
			return true
		}
	}

	return false
}

func isCloudFormationFile(path, extension string) bool {
	_, possiblyCF := possibleCloudformationExtensions[extension]
	if !possiblyCF {
		return false
	}

	// Casts a wide enough net given what exists in commerce today. Could always add some file inspection.
	for _, pattern := range cloudFormationPathPatterns {
		if strings.Contains(strings.ToLower(path), pattern) {
			return true
		}
	}

	return false
}

func isTerraformFile(path, extension string) bool {
	_, isTerraform := terraformExtensions[extension]
	return isTerraform
}

func IsInfraAsCode(path, extension string) bool {
	qualifiers := []func(path, extension string) bool {
		isTerraformFile,
		isCDKFile,
		isCloudFormationFile,
	}

	for _, qualifier := range qualifiers {
		if qualifier(path, extension) {
			return true
		}
	}

	return false
}
