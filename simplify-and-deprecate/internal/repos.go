package simplify

type Repo struct {
	Name string
	Parent string
	GitAddress string
}

type Team string

const (
	MoneyPurchasePlatform = Team("MoneyPurchasePlatform")
	MoneyBillingPlatform = Team("MoneyBillingPlatform")
	MoneyCreatorFinancePlatform = Team("MoneyCreatorFinancePlatform")
	Memberships = Team("Memberships")
	DA = Team("DA")
	Moments = Team("Moments")
)

var Teams = map[Team][]Repo{
	Moments: {
		{
			Name: "Payday",
			GitAddress: "git@git.xarth.tv:commerce/payday.git",
		},
		{
			Name: "Prometheus",
			GitAddress: "git@git.xarth.tv:commerce/prometheus.git",
		},
		{
			Name: "Pagle",
			GitAddress: "git@git.xarth.tv:commerce/pagle.git",
		},
		{
			Name: "Prism",
			GitAddress: "git@git.xarth.tv:commerce/prism.git",
		},
		{
			Name: "Petozi",
			GitAddress: "git@git.xarth.tv:commerce/petozi.git",
		},
		{
			Name: "Pachter",
			GitAddress: "git@git.xarth.tv:commerce/pachter.git",
		},
		{
			Name: "Pachinko",
			GitAddress: "git@git.xarth.tv:commerce/pachinko.git",
		},
		{
			Name: "Phanto",
			GitAddress: "git@git.xarth.tv:commerce/phanto.git",
		},
		{
			Name: "Clippy",
			GitAddress: "git@git.xarth.tv:subs/clippy.git",
		},
		{
			Name: "TwitchPersonalization",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPersonalization",
		},
		{
			Name: "TwitchPersonalizationVpcInfrastructure",
			Parent: "TwitchPersonalization",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPersonalizationVpcInfrastructure",
		},
		{
			Name: "TwitchPersonalizationLpt",
			Parent: "TwitchPersonalization",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPersonalizationLpt",
		},
		{
			Name: "TwitchPersonalizationBaseLpt",
			Parent: "TwitchPersonalization",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPersonalizationBaseLpt",
		},
		{
			Name: "TwitchPersonalizationVpcInfrastructureLpt",
			Parent: "TwitchPersonalization",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPersonalizationVpcInfrastructureLpt",
		},
		{
			Name: "Winnebago",
			GitAddress: "git@git.xarth.tv:ads/winnebago.git",
		},
	},
	DA: {
		{
			Name: "Fortuna",
			GitAddress: "git@git.xarth.tv:commerce/fortuna.git",
		},
		{
			Name: "Pantheon",
			GitAddress: "git@git.xarth.tv:commerce/pantheon.git",
		},
		{
			Name: "Percy",
			GitAddress: "git@git.xarth.tv:commerce/percy.git",
		},
		{
			Name: "Mako",
			GitAddress: "git@git.xarth.tv:commerce/mako.git",
		},
		{
			Name: "Paint",
			GitAddress: "git@git.xarth.tv:subs/paint.git",
		},
	},
	Memberships: {
		{
			Name: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyager",
		},
		{
			Name: "TwitchVoyagerCDK",
			Parent: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyagerCDK",
		},
		{
			Name: "TwitchVoyagerLambdas",
			Parent: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyagerLambdas",
		},
		{
			Name: "TwitchVoyagerBaseLpt",
			Parent: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyagerBaseLpt",
		},
		{
			Name: "TwitchVoyagerLpt",
			Parent: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyagerLpt",
		},
		{
			Name: "TwitchVoyagerVpcInfrastructureLpt",
			Parent: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyagerVpcInfrastructureLpt",
		},
		{
			Name: "TwitchVoyagerVpcInfrastructure",
			Parent: "TwitchVoyager",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchVoyagerVpcInfrastructure",
		},
		{
			Name: "Subscriptions",
			GitAddress: "git@git.xarth.tv:revenue/subscriptions.git",
		},
		{
			Name: "Lambdwich",
			GitAddress: "git@git.xarth.tv:revenue/lambdwich.git",
		},
		{
			Name: "SubscriptionsLambdasLambda",
			GitAddress: "ssh://git.amazon.com/pkg/SubscriptionsLambdasLambda",
		},
		{
			Name: "SubscriptionsLambdasLambdaLpt",
			Parent: "SubscriptionsLambdasLambda",
			GitAddress: "ssh://git.amazon.com/pkg/SubscriptionsLambdasLambdaLpt",
		},
		{
			Name: "Workflows",
			GitAddress: "git@git.xarth.tv:subs/workflows.git",
		},
		{
			Name: "DBExportTerracode",
			GitAddress: "git@git.xarth.tv:subs/db-export-terracode.git",
		},
		{
			Name: "Chronobreak",
			GitAddress: "git@git.xarth.tv:subs/chronobreak.git",
		},
		{
			Name: "Valhalla",
			GitAddress: "git@git.xarth.tv:revenue/valhalla.git",
		},
		{
			Name: "BoxOffice",
			GitAddress: "git@git.xarth.tv:commerce/box-office.git",
		},
		{
			Name: "Nioh",
			GitAddress: "git@git.xarth.tv:commerce/nioh.git",
		},
		{
			Name: "Galleon",
			GitAddress: "git@git.xarth.tv:subs/galleon.git",
		},
		{
			Name: "Griphook",
			GitAddress: "git@git.xarth.tv:subs/griphook.git",
		},
	},
	MoneyPurchasePlatform: {
		{
			Name: "Destiny",
			GitAddress: "git@git.xarth.tv:subs/destiny.git",
		},
		{
			Name: "Flo",
			GitAddress: "git@git.xarth.tv:revenue/flo.git",
		},
		{
			Name: "VertexClient",
			GitAddress: "git@git.xarth.tv:revenue/vertex-client.git",
		},
		{
			Name: "VertexDeploy",
			GitAddress: "git@git.xarth.tv:revenue/vertex-deploy.git",
		},
		{
			Name: "Materia",
			GitAddress: "ssh://git.amazon.com/pkg/Materia",
		},
		{
			Name: "MateriaBaseLpt",
			Parent: "Materia",
			GitAddress: "ssh://git.amazon.com/pkg/MateriaBaseLpt",
		},
		{
			Name: "MateriaLpt",
			Parent: "Materia",
			GitAddress: "ssh://git.amazon.com/pkg/MateriaLpt",
		},
		{
			Name: "MateriaVpcInfrastructureLpt",
			Parent: "Materia",
			GitAddress: "ssh://git.amazon.com/pkg/MateriaVpcInfrastructureLpt",
		},
		{
			Name: "MateriaVpcInfrastructure",
			Parent: "Materia",
			GitAddress: "ssh://git.amazon.com/pkg/MateriaVpcInfrastructure",
		},
		{
			Name: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalog",
		},
		{
			Name: "TwitchProductCatalogCDK",
			Parent: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalogCDK",
		},
		{
			Name: "TwitchProductCatalogLambdas",
			Parent: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalogLambdas",
		},
		{
			Name: "TwitchProductCatalogBaseLpt",
			Parent: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalogBaseLpt",
		},
		{
			Name: "TwitchProductCatalogLpt",
			Parent: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalogLpt",
		},
		{
			Name: "TwitchProductCatalogVpcInfrastructureLpt",
			Parent: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalogVpcInfrastructureLpt",
		},
		{
			Name: "TwitchProductCatalogVpcInfrastructure",
			Parent: "TwitchProductCatalog",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchProductCatalogVpcInfrastructure",
		},
		{
			Name: "Corleone",
			GitAddress: "ssh://git.amazon.com/pkg/Corleone",
		},
		{
			Name: "CorleoneBaseLpt",
			Parent: "Corleone",
			GitAddress: "ssh://git.amazon.com/pkg/CorleoneBaseLpt",
		},
		{
			Name: "CorleoneLpt",
			Parent: "Corleone",
			GitAddress: "ssh://git.amazon.com/pkg/CorleoneLpt",
		},
		{
			Name: "CorleoneVpcInfrastructureLpt",
			Parent: "Corleone",
			GitAddress: "ssh://git.amazon.com/pkg/CorleoneVpcInfrastructureLpt",
		},
		{
			Name: "CorleoneVpcInfrastructure",
			Parent: "Corleone",
			GitAddress: "ssh://git.amazon.com/pkg/CorleoneVpcInfrastructure",
		},
		{
			Name: "TwitchPromoService",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPromoService",
		},
		{
			Name: "TwitchPromoServiceBaseLpt",
			Parent: "TwitchPromoService",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPromoServiceBaseLpt",
		},
		{
			Name: "TwitchPromoServiceLpt",
			Parent: "TwitchPromoService",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPromoServiceLpt",
		},
		{
			Name: "TwitchPromoServiceVpcInfrastructureLpt",
			Parent: "TwitchPromoService",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPromoServiceVpcInfrastructureLpt",
		},
		{
			Name: "TwitchPromoServiceVpcInfrastructure",
			Parent: "TwitchPromoService",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPromoServiceVpcInfrastructure",
		},
	},
	MoneyBillingPlatform: {
		{
			Name: "Bubbles",
			GitAddress: "git@git.xarth.tv:revenue/bubbles.git",
		},
		{
			Name: "Everdeen",
			GitAddress: "git@git.xarth.tv:revenue/everdeen.git",
		},
		{
			Name: "TwitchPaymentsSakura",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPaymentsSakura",
		},
		{
			Name: "TwitchPaymentsSakuraBaseLpt",
			Parent: "TwitchPaymentsSakura",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPaymentsSakuraBaseLpt",
		},
		{
			Name: "TwitchPaymentsSakuraLpt",
			Parent: "TwitchPaymentsSakura",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPaymentsSakuraLpt",
		},
		{
			Name: "TwitchPaymentsSakuraVpcInfrastructureLpt",
			Parent: "TwitchPaymentsSakura",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPaymentsSakuraVpcInfrastructureLpt",
		},
		{
			Name: "TwitchPaymentsSakuraVpcInfrastructure",
			Parent: "TwitchPaymentsSakura",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchPaymentsSakuraVpcInfrastructure",
		},
		{
			Name: "TwitchZuko",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchZuko",
		},
		{
			Name: "TwitchZukoBaseLpt",
			Parent: "TwitchZuko",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchZukoBaseLpt",
		},
		{
			Name: "TwitchZukoLpt",
			Parent: "TwitchZuko",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchZukoLpt",
		},
		{
			Name: "TwitchZukoVpcInfrastructureLpt",
			Parent: "TwitchZuko",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchZukoVpcInfrastructureLpt",
		},
		{
			Name: "TwitchZukoVpcInfrastructure",
			Parent: "TwitchZuko",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchZukoVpcInfrastructure",
		},
		{
			Name: "Mulan",
			GitAddress: "git@git.xarth.tv:revenue/mulan.git",
		},
		{
			Name: "TwitchNami",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchNami",
		},
		{
			Name: "TwitchNamiBaseLpt",
			Parent: "TwitchNami",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchNamiBaseLpt",
		},
		{
			Name: "TwitchNamiLpt",
			Parent: "TwitchNami",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchNamiLpt",
		},
		{
			Name: "TwitchNamiVpcInfrastructureLpt",
			Parent: "TwitchNami",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchNamiVpcInfrastructureLpt",
		},
		{
			Name: "TwitchNamiVpcInfrastructure",
			Parent: "TwitchNami",
			GitAddress: "ssh://git.amazon.com/pkg/TwitchNamiVpcInfrastructure",
		},
		{
			Name: "Pepper",
			GitAddress: "git@git.xarth.tv:revenue/pepper.git",
		},
		{
			Name: "PaymentsService",
			GitAddress: "git@git.xarth.tv:revenue/payments-service.git",
		},
	},
	MoneyCreatorFinancePlatform: {
		{
			Name: "Buttercup",
			GitAddress: "git@git.xarth.tv:revenue/buttercup.git",
		},
		{
			Name: "Gringotts",
			GitAddress: "git@git.xarth.tv:revenue/gringotts.git",
		},
		{
			Name: "MoneyPenny",
			GitAddress: "git@git.xarth.tv:revenue/moneypenny.git",
		},
		{
			Name: "RevenueReportingWorkers",
			GitAddress: "git@git.xarth.tv:revenue/revenuereporting-workers.git",
		},
		{
			Name: "Ripley",
			GitAddress: "git@git.xarth.tv:revenue/ripley.git",
		},
	},
}