package simplify

var IgnoredFilePatterns = []string{
	"_tools",
	"vendor",
	"mock",
	"fake",
	"generated",
	"pb.go",
	"twirp.go",
}