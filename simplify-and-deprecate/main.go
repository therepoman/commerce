package main

import (
	simplify "code.justin.tv/commerce/simplify-and-deprecate/internal"
	"encoding/csv"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// TODO: add logic for pulling in twilight components by codeowner
const (
	outFile = "./out/out.csv"
	buildDir = "tmp"
)

type CommitToProcess struct {
	SHA string
	SearchedDateRange DateRange
}

func main() {
	csvfile, err := os.OpenFile(outFile, os.O_APPEND|os.O_RDWR, 0600)
	if err != nil {
		panic(err)
	}

	csvWriter := csv.NewWriter(csvfile)
	defer func() {
		_ = csvfile.Close()
	}()

	workingDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	fullBuildDir := filepath.Join(workingDir, buildDir)

	err = os.RemoveAll(fullBuildDir)
	if err != nil {
		panic(err)
	}

	for team, repos := range simplify.Teams {
		fmt.Printf("Processing %d repos for team %s\n", len(repos), team)

		for _, repo := range repos {
			fmt.Printf("[%s][%s] Starting\n", team, repo.Name)

			targetDir := filepath.Join(fullBuildDir, string(team), repo.Name)
			err = os.MkdirAll(targetDir, 0700)
			if err != nil {
				panic(err)
			}

			clone(targetDir, repo.GitAddress)

			var commitsToProcess []CommitToProcess

			periodsToProcess := makeHistoricalRanges()
			for _, period := range periodsToProcess {
				shaForPeriod := getSHAFromRange(targetDir, period)

				// Fall back on the most recent commit before the selected period
				if shaForPeriod == "" {
					shaForPeriod = getFirstSHABefore(targetDir, period.Before)
				}

				commitsToProcess = append(commitsToProcess, CommitToProcess{
					SearchedDateRange: period,
					SHA: shaForPeriod,
				})
			}

			fmt.Printf("[%s][%s] Processing %d commits\n", team, repo.Name, len(commitsToProcess))

			for _, config := range commitsToProcess {
				if config.SHA == "" {
					continue
				}

				checkout(targetDir, config.SHA)
				cleanBigDirsThatWeDontCareAbout(targetDir)
				commitDate := getCommitDate(targetDir, config.SHA)
				codeLines, infraLines := countLOC(targetDir)
				commitsAtHead := getTotalCommitsAtHead(targetDir)

				projectName := repo.Name
				if repo.Parent != "" {
					projectName = repo.Parent
				}

				// team | projectName | repo | commit | commitDate | LOC | infraLines | commitsAtHead | _ | searchPeriodStart | searchPeriodEnd
				err := csvWriter.Write([]string{
					string(team),
					projectName,
					repo.Name,
					config.SHA,
					commitDate,
					strconv.Itoa(codeLines),
					strconv.Itoa(infraLines),
					strconv.Itoa(commitsAtHead),
					"", // Historical column, can delete
					config.SearchedDateRange.After.Format("01/02/2006"),
					config.SearchedDateRange.Before.Format("01/02/2006"),
				})
				if err != nil {
					panic(err)
				}
				csvWriter.Flush()

				fmt.Printf("[%s][%s] Done\n", team, repo.Name)
			}
		}
	}
}

func countLOC(targetDir string) (codeLines, infraLines int) {
	linesOfCode := 0
	linesOfInfra := 0

	err := filepath.WalkDir(targetDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			panic(err)
		}

		if d.Type() == os.ModeSymlink {
			return nil
		}

		for _, ignoredPattern := range simplify.IgnoredFilePatterns {
			if strings.Contains(path, ignoredPattern) {
				return nil
			}
		}

		ext := filepath.Ext(d.Name())

		isInfraAsCode := simplify.IsInfraAsCode(path, ext)
		isCode := simplify.IsCodeFile(ext)

		if !isInfraAsCode && !isCode {
			return nil
		}

		lineCount := getLOCForFile(path)

		if isInfraAsCode {
			linesOfInfra += lineCount
		} else if isCode {
			linesOfCode += lineCount
		}

		return nil
	})

	if err != nil {
		panic(err)
	}

	return linesOfCode, linesOfInfra
}

type DateRange struct {
	Before time.Time
	After time.Time
}

func makeHistoricalRanges() []DateRange {
	var dateRanges []DateRange
	before := time.Now()

	// make 16 3 month ranges (4 years)
	for i := 0; i < 16; i++ {
		after := before.Add(-time.Hour * 24 * 90)


		dateRanges = append(dateRanges, DateRange{
			Before: before,
			After: after,
		})

		before = after
	}

	return dateRanges
}

func getLOCForFile(path string) int {
	cmd := exec.Command("wc", "-l", path)

	cmd.Stderr = os.Stderr

	out, err := cmd.Output()

	// There are very rare instances where projects have directories that look like a code file (e.g. payday/cmd/restart.sh/).
	// In theses cases, the wc -l command will fail, so we can just fall back on an assumption of 0 LOC
	if err != nil {
		return 0
	}

	trimmedOut := strings.TrimSpace(string(out))
	splitOut := strings.Split(trimmedOut, " ")

	lineCount, err := strconv.Atoi(splitOut[0])
	if err != nil {
		panic(err)
	}

	return lineCount
}

func getSHAFromRange(targetDir string, dateRange DateRange) string {
	return strings.TrimSpace(execGitCommand(
		"-C", targetDir,
		"log",
		fmt.Sprintf("--before={%s}", dateRange.Before.Format("2006-01-02")),
		fmt.Sprintf("--after={%s}", dateRange.After.Format("2006-01-02")),
		"-n", "1",
		"--pretty=format:%H"))
}

func getFirstSHABefore(targetDir string, before time.Time) string {
	return strings.TrimSpace(execGitCommand(
		"-C", targetDir,
		"log",
		fmt.Sprintf("--before={%s}", before.Format("2006-01-02")),
		"-n", "1",
		"--pretty=format:%H"))
}

func getTotalCommitsAtHead(targetDir string) int {
	strCount := strings.TrimSpace(execGitCommand("-C", targetDir, "rev-list", "--count", "HEAD"))
	count, err := strconv.Atoi(strCount)
	if err != nil {
		panic(err)
	}

	return count
}

func getCommitDate(targetDir, commit string) string {
	return strings.TrimSpace(execGitCommand("-C", targetDir, "show", "-s", "--format=%ci", commit))
}

func checkout(targetDir, commit string) {
	_ = execGitCommand("-C", targetDir, "checkout", commit)
}

func clone(targetDir, address string) {
	_ = execGitCommand("clone", address, targetDir)
}

// Remove common large directories that we plan to ignore downstream (saves the cost of walking them later on)
func cleanBigDirsThatWeDontCareAbout(targetDir string) {
	err := os.RemoveAll(fmt.Sprintf("%s/%s", targetDir, "vendor"))
	if err != nil {
		panic(err)
	}

	err = os.RemoveAll(fmt.Sprintf("%s/%s", targetDir, "_tools"))
	if err != nil {
		panic(err)
	}
}

func execGitCommand(args ...string) string {
	cmd := exec.Command("git", args...)
	cmd.Stderr = os.Stderr

	out, err := cmd.Output()
	if err != nil {
		fmt.Println("git command failed", args)

		panic(err)
	}

	return string(out)
}