package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/go-yaml/yaml"
	log "github.com/sirupsen/logrus"
)

const (
	LocalConfigFilePath            = "/src/code.justin.tv/commerce/bits-bot/config/data/%s.yaml"
	GlobalConfigFilePath           = "/etc/bits-bot/config/%s.yaml"
	EnvironmentEnvironmentVariable = "ENVIRONMENT"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")

	Local   = Environment("local")
	Prod    = Environment("prod")
	Default = Local
)

type Config struct {
	Environment Environment

	EnvironmentName                       string                   `yaml:"environment-name"`
	RedisEndpoint                         string                   `yaml:"redis-endpoint"`
	UseRedisClusterMode                   bool                     `yaml:"use-redis-cluster-mode"`
	DynamoSuffix                          string                   `yaml:"dynamo-suffix"`
	AWSRegion                             string                   `yaml:"aws-region"`
	SandstormRole                         string                   `yaml:"sandstorm-role"`
	BitsBotSlackTokenSandstormKey         string                   `yaml:"bits-bot-slack-oauth-sandstorm-key"`
	BitsBotSlackToken2SandstormKey        string                   `yaml:"bits-bot-slack-oauth-2-sandstorm-key"`
	BitsBotSlackPRsWebhookURLSandstormKey string                   `yaml:"bits-bot-prs-webhook-url-sandstorm-key"`
	SlackUserID                           string                   `yaml:"slack-user-id"`
	CloudwatchMetricsConfig               *CloudwatchMetricsConfig `yaml:"cloudwatch-metrics-config"`
	StatsPrefix                           string                   `yaml:"stats-prefix"`
	StatsURL                              string                   `yaml:"stats-url"`
	GuessChannelID                        string                   `yaml:"guess-channel-id"`
	ThePubbChannelID                      string                   `yaml:"the-pubb-channel-id"`
	BitsBotTestChannelID                  string                   `yaml:"bits-bot-test-channel-id"`
	UserServiceEndpoint                   string                   `yaml:"user-service-endpoint"`
	MagicChannelID                        string                   `yaml:"magic-channel-id"`
	SeattleMagicChannelID                 string                   `yaml:"seattle-magic-channel-id"`
}

type CloudwatchMetricsConfig struct {
	BufferSize               int     `yaml:"buffer-size"`
	BatchSize                int     `yaml:"batch-size"`
	FlushIntervalMinutes     int     `yaml:"flush-interval-minutes"`
	FlushCheckDelayMS        int     `yaml:"flush-check-delay-ms"`
	EmergencyFlushPercentage float64 `yaml:"emergency-flush-percentage"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}

	cfg, err := loadConfig(filePath)
	if err != nil {
		return nil, err
	}

	cfg.Environment = env
	return cfg, nil
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}
