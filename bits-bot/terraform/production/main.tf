module "bits-bot" {
  source                              = "../modules/bits-bot"
  owner                               = "twitch-bits-aws@amazon.com"
  aws_profile                         = "bits-bot-prod"
  env                                 = "prod"
  env_full                            = "production"
  vpc_id                              = "vpc-4cfeb629"
  private_subnets                     = "subnet-31bc5e55,subnet-30393e47,subnet-6eadf037"
  private_cidr_blocks                 = ["10.193.64.0/20", "10.193.80.0/20", "10.193.96.0/20"]
  sg_id                               = "sg-097b2185ded0df885"
  instance_type                       = "c3.xlarge"
  cname_prefix                        = "bits-bot-prod"
  elasticache_replicas_per_node_group = "1"
  elasticache_num_node_groups         = "2"
  elasticache_instance_type           = "cache.m3.medium"
  autoscaling_role                    = "arn:aws:iam::021561903526:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_min_read_capacity            = 10
  dynamo_min_write_capacity           = 10
}

terraform {
  backend "s3" {
    bucket  = "bits-bot-terraform-prod"
    key     = "tfstate/commerce/bits-bot/terraform/prod"
    region  = "us-west-2"
    profile = "bits-bot-prod"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "bits-bot-prod"
}
