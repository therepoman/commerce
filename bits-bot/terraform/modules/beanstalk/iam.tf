resource "aws_iam_role" "bs" {
  name = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  path = "/"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": { "Service": "ec2.amazonaws.com"},
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource "aws_iam_role_policy" "bs" {
  name   = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  policy = "${coalesce(var.iam_role_policy, file("${path.module}/files/iam_role"))}"
  role   = "${aws_iam_role.bs.id}"
}

resource "aws_iam_instance_profile" "bs" {
  depends_on = ["aws_iam_role.bs"]
  name       = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  roles      = ["${aws_iam_role.bs.name}"]
}

// Grant the Instance Profile full dynamo access
resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment" {
  role       = "${aws_iam_role.bs.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "attach_ssm_access_policy" {
  role       = "${aws_iam_role.bs.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

// Sandstorm policy
resource "aws_iam_policy" "sandstorm_policy" {
  name   = "sandstorm-assume-role-policy"
  policy = "${coalesce(var.iam_role_policy, file("${path.module}/files/sandstorm_policy_${var.env}"))}"
}

// Sandstorm policy attachment
resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment" {
  role = "${aws_iam_role.bs.name}"
  policy_arn = "${aws_iam_policy.sandstorm_policy.arn}"
}
