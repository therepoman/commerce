# Beanstalk general variables
variable "aws_access_key" {
  description = "AWS Access key for user with IAM credentials"
  default     = ""
}

variable "aws_secret_key" {
  description = "AWS Secret key for user with IAM credentials"
  default     = ""
}

variable "aws_profile" {
  description = "AWS profile used to create resources"
  default     = ""
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "eb_application_name" {
  description = "Name of the beanstalk application that will be deployed to this beanstalk environment. #REQUIRED"
}

variable "common_name" {
  description = "Name used as a base for created resources. Must only contain upper/lower case letters, digits, and dash. Default if not specified: ${var.env}-${var.service}"
  default     = ""
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
  default     = "dev"
}

variable "service" {
  description = "All lower­case, in  team/service/[role] format. This tag should indicate the server’s function and what is running on it;  role is optional but highly recommended. #REQUIRED"
}

variable "owner" {
  description = "The user or team who owns this infrastructure. Will be used for tagging AWS resources. Should be an email address. #REQUIRED"
}

variable "iam_role_policy" {
  description = "An IAM role policy to override the default created by this module. If this is not specified or empty, a default policy is created which only allows S3 access for logs."
  default     = ""
}

variable "wait_for_ready_timeout" {
  description = "The maximum duration that Terraform should wait for an Elastic Beanstalk Environment to be in a ready state before timing out."
  default     = "10m"
}

variable "statsd_host" {
  description = "Sets STATSD_HOST env var so Go apps using twitchhttp have automatic metrics collection."
  default     = "graphite.internal.justin.tv:8125"
}

# Note: All of the variables below are used to define EB environment configuration.
# If the description is not sufficient, you may find additional information and context
# at: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
variable "vpc_id" {
  description = "VPC ID used for the environment. #REQUIRED"
}

variable "ec2_subnet_ids" {
  description = "Comma-separated string of vpc security groups to be used by beanstalk instances. #REQUIRED"
}

variable "elb_subnet_ids" {
  description = "Comma-separated string of vpc security groups to be used by the beanstalk ELB. #REQUIRED"
}

variable "solution_stack_name" {
  description = "Full beanstalk solution name. See: http://docs.aws.amazon.com/cli/latest/reference/elasticbeanstalk/list-available-solution-stacks.html.  #REQUIRED"
}

variable "eb_config_template_name" {
  description = "Optional eb configuration template. Used to pass additional or override existing beanstalk settings."
  default     = ""
}

variable "associate_public_address" {
  description = "Specifies whether to launch instances with pulic IP addresses in your VPC."
  default     = "false"
}

variable "elb_scheme" {
  description = "Type of ELB. <internal|public>"
  default     = "internal"
}

variable "elb_loadbalancer_security_groups" {
  description = "Assign one or more security groups that you created to the load balancer. #REQUIRED"
}

variable "auto_scaling_lc_iam_instance_profile" {
  description = "An instance profile enables AWS Identity and Access Management (IAM) users and AWS services to access temporary security credentials to make AWS API calls. Specify the profile name or the ARN. One is created automatically if one is not specified."
  default     = ""
}

variable "auto_scaling_lc_instance_type" {
  description = "The instance type used to run your application in an Elastic Beanstalk environment."
  default     = "t2.micro"
}

variable "auto_scaling_lc_monitoring_interval" {
  description = "Interval at which you want Amazon CloudWatch metrics returned."
  default     = "5 minute"
}

variable "auto_scaling_lc_security_groups" {
  description = "Comma-separated list of vpc security group ids to be used for the beanstalk instances."
  default     = "elasticbeanstalk-default"
}

variable "auto_scaling_lc_root_volume_type" {
  description = "Volume type (magnetic, general purpose SSD or privisioned IOPS SSD) to use for the root Amazon EBS volume attached to your environment's EC2 instances."
  default     = "gp2"
}

variable "auto_scaling_lc_root_volume_size" {
  description = "Storage capacity of the root Amazon EBS volume in whole GiB."
  default     = "20"
}

variable "auto_scaling_lc_root_volume_iops" {
  description = "Desired input/output operations per second (IOPS) for a provisioned IOPS SSD root volume."
  default     = "100"
}

variable "eb_environment_service_role" {
  description = "The name of an IAM role that Elastic Beanstalk uses to manage resources for the environment."
  default     = "aws-elasticbeanstalk-service-role"
}

variable "cname_prefix" {
  description = "Prefix to use for the fully qualified DNS name of the Environment."
  default     = ""
}

variable "acm_cert_arn" {
  description = "The arn of the ACM cert to attach to the load balancer"
  default = ""
}

# dynamic variables
resource "null_resource" "vars" {
  triggers {
    cn = "${replace("${var.env}-${var.service}", "/", "-")}"
  }
}
