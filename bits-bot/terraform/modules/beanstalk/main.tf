data "aws_acm_certificate" "bs" {
  domain      = "bits-bot.${var.env}.internal.justin.tv"
  statuses    = ["ISSUED"]
  most_recent = true
  types       = ["AMAZON_ISSUED"]
}

resource "aws_elastic_beanstalk_configuration_template" "bs" {
  name                = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-template"
  description         = "placeholder to allow the user to optionally specify a template"
  application         = "${var.eb_application_name}"
  solution_stack_name = "${var.solution_stack_name}"
}

resource "aws_elastic_beanstalk_environment" "bs" {
  name        = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-env"
  description = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  application = "${var.eb_application_name}"
  tier        = "WebServer"

  cname_prefix = "${coalesce(var.cname_prefix, null_resource.vars.triggers.cn)}"

  tags {
    Environment = "${var.env}"
    Service     = "${var.service}"
    Owner       = "${var.owner}"
    Skeleton    = "dta/docker-eb-tf-skeleton@0f0fd32b7ab08a557015f87f4f7d08554811b3be"
  }

  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"
  template_name          = "${coalesce(var.eb_config_template_name, aws_elastic_beanstalk_configuration_template.bs.name)}"

  # Vpc  and network related settings
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "${var.vpc_id}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${var.ec2_subnet_ids}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "${var.elb_subnet_ids}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "${var.associate_public_address}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "${var.elb_scheme}"
  }

  # Load Balancer Related Settings
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "SecurityGroups"
    value     = "${var.elb_loadbalancer_security_groups}"
  }

  setting {
    namespace = "aws:elb:listener:80"
    name      = "ListenerEnabled"
    value     = "false"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name      = "ListenerProtocol"
    value     = "HTTPS"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name      = "InstancePort"
    value     = "80"
  }

  setting {
    namespace = "aws:elb:listener:443"
    name      = "SSLCertificateId"
    value     = "${var.acm_cert_arn}"
  }

  # asg launch configuration settings
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${coalesce(var.auto_scaling_lc_iam_instance_profile, aws_iam_instance_profile.bs.name)}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "${var.auto_scaling_lc_instance_type}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "MonitoringInterval"
    value     = "${var.auto_scaling_lc_monitoring_interval}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = "${var.auto_scaling_lc_security_groups}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeType"
    value     = "${var.auto_scaling_lc_root_volume_type}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeSize"
    value     = "${var.auto_scaling_lc_root_volume_size}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeIOPS"
    value     = "${var.auto_scaling_lc_root_volume_iops}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENV_NAME"
    value     = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-env"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "APP"
    value     = "${var.eb_application_name}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "STATSD_HOST_PORT"
    value     = "${var.statsd_host}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "${var.env}"
  }

  # eb environment settings
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${var.eb_environment_service_role}"
  }

  # Cloudwatch logs settings
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = "true"
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = "false"
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "RetentionInDays"
    value     = "60"
  }

  # Connection Draining
  setting {
    namespace = "aws:elb:policies"
    name      = "ConnectionDrainingEnabled"
    value     = "true"
  }

  setting {
    namespace = "aws:elb:policies"
    name      = "ConnectionDrainingTimeout"
    value     = "10"                        # 10 second timeout
  }

  # Health check setting
  setting {
    namespace = "aws:elasticbeanstalk:application"
    name      = "Application Healthcheck URL"
    value     = "/ping"
  }
}
