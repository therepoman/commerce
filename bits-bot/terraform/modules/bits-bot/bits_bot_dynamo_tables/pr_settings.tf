resource "aws_dynamodb_table" "pr_settings_table" {
  name           = "pr-settings-${var.suffix}"
  hash_key       = "slack_user_id"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "slack_user_id"
    type = "S"
  }
}

module "pr_settings_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "pr-settings-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}