variable "suffix" {
  description = "Suffix at the end of dynamo table names"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}
