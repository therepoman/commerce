resource "aws_dynamodb_table" "messages_table" {
  name           = "bits-bot-messages-${var.suffix}"
  hash_key       = "slack_user_hash"
  range_key      = "slack_timestamp"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "slack_user_hash"
    type = "S"
  }

  "attribute" {
    name = "slack_timestamp"
    type = "S"
  }
}

module "messages_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "bits-bot-messages-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}