resource "aws_dynamodb_table" "key_claims_table" {
  name           = "bits-bot-key-claims-${var.suffix}"
  hash_key       = "slack_user"
  range_key      = "claim_time"
  read_capacity  = "${var.min_read_capacity}"
  write_capacity = "${var.min_write_capacity}"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "slack_user"
    type = "S"
  }

  "attribute" {
    name = "claim_time"
    type = "S"
  }
}

module "key_claims_table_autoscaling" {
  source             = "../../dynamo_table_autoscaling"
  table_name         = "bits-bot-key-claims-${var.suffix}"
  min_read_capacity  = "${var.min_read_capacity}"
  min_write_capacity = "${var.min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
}
