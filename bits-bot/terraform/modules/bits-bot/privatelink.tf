module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "bits-bot"
  environment = "${var.env_full}"
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"

  name        = "bits-bot"
  environment = "${var.env_full}"
  zone_id     = "${module.privatelink-zone.zone_id}"
  alb_dns     = "${module.beanstalk.beanstalk_environment_cname}"
}