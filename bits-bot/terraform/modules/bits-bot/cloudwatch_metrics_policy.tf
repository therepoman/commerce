# roles and policies for publishing cloudwatch metrics
resource "aws_iam_policy_attachment" "cloudwatch_metrics_policy_attachment" {
  name       = "cloudwatch_metrics_policy_attachment"
  policy_arn = "${aws_iam_policy.cloudwatch_metrics_policy.arn}"
  roles      = ["${aws_iam_role.health_service_role.name}", "${module.beanstalk.iam_role_name}"]
}

resource "aws_iam_policy" "cloudwatch_metrics_policy" {
  name        = "cloudwatch_metrics_policy"
  description = "push metrics to cloudwatch"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "cloudwatch:PutMetricData"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}
