resource "aws_cloudwatch_metric_alarm" "bits_bot_key_volume_low_alarm" {
  alarm_name                = "bits_bot_${var.env}_key_volume_low"
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "bits-bot.${var.env}.key-manager.available-key-count"
  namespace                 = "bits-bot-${var.env}"
  period                    = "86400"
  statistic                 = "Minimum"
  threshold                 = "50"
  alarm_description         = "Alarms when we have a low amount of keys in Bits Bot for ${var.env_full}"

  dimensions                =  {
    stage = "${var.env}"
  }
}