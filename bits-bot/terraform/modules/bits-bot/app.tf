module "beanstalk-app" {
  source = "git::git+ssh://git@git.xarth.tv/dta/tf_beanstalk_app.git?ref=0.0.1"
  aws_profile = "${var.aws_profile}"
  eb_application_name = "bits-bot"
  eb_application_description = "Beanstalk app created by TCS"
}