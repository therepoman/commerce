module "bits_bot_dynamo_tables" {
  source                   = "bits_bot_dynamo_tables"
  suffix                   = "${var.env}"
  autoscaling_role         = "${var.autoscaling_role}"
  min_read_capacity        = "${var.dynamo_min_read_capacity}"
  min_write_capacity       = "${var.dynamo_min_write_capacity}"
}
