package main

import (
	"math/rand"
	"os"
	"time"

	"code.justin.tv/commerce/bits-bot/backend"
	"code.justin.tv/commerce/bits-bot/config"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	log "github.com/sirupsen/logrus"
	twirp_statsd "github.com/twitchtv/twirp/hooks/statsd"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

func getEnv() config.Environment {
	env := os.Getenv(config.EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !config.IsValidEnvironment(config.Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(config.Default))
		return config.Default
	}

	return config.Environment(env)
}

func main() {
	env := getEnv()

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	// Starts bot in a goroutine
	be.Bot.DoBotStuff()

	twirpStatsHook := twirp_statsd.NewStatsdServerHooks(be.Stats)
	twirpHandler := bits_bot.NewBitsBotServer(be.Server, twirpStatsHook)

	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(bits_bot.BitsBotPathPrefix+"*"), twirpHandler)

	goji_graceful.HandleSignals()
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")
}
