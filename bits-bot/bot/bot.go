package bot

import (
	"code.justin.tv/commerce/bits-bot/backend/message"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

var Running = true

type Bot interface {
	DoBotStuff()
}

type BotImpl struct {
	SlackClient      *slack.Client     `inject:"slack_client_1"`
	MessageProcessor message.Processor `inject:""`
}

func NewBot() Bot {
	return &BotImpl{}
}

func (b BotImpl) DoBotStuff() {
	go func() {
		rtm := b.SlackClient.NewRTM()
		go rtm.ManageConnection()

		for event := range rtm.IncomingEvents {
			switch eventData := event.Data.(type) {
			case *slack.MessageEvent:
				if !Running {
					log.Info("bot not currently running - skipping message")
					continue
				}

				err := b.MessageProcessor.Process(eventData)
				if err != nil {
					log.WithError(err).Error("error processing message")
				}
			default:
			}
		}
	}()
}
