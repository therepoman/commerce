package identifier

// ProcessIdentifier represents the identity of a running process.
type ProcessIdentifier struct {
	Service  string
	Region   string
	Stage    string
	Substage string
	Version  string
	Machine  string
	LaunchID string
}
