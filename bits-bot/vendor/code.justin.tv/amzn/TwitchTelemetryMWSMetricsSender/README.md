# `TwitchTelemetryMWSMetricsSender`

This is a package that can be used for sending metrics to the [MWS PutMetricDataForAggregation endpoint](https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation). 

This package is meant to be used with samples sent from [TwitchTelemetry](https://code.amazon.com/packages/TwitchTelemetry/trees/mainline). Please note that the actual client performing the send operation is [Twitch-Video-Mwsclient](https://code.amazon.com/packages/Twitch-Video-Mwsclient/trees/v1.0.0).

When taking in new samples, this package creates appropriate rollups using the samples's designated RollupDimensions. This means that, in addition to mapping dimensions from TwitchTelemetry into the PMET service dimension schema, it will create additional datums for the rollups (e.g. what is usually seen in PMET as 'ALL' for a dimension). This also means that for any sample observed, multiple datums can be emitted to PMET. For more information on this process, please see the [Twitch Telemetry Naming Conventions document](https://docs.google.com/document/d/1-1z7B9HS8_YRYne09DvIwsN_jvYSGw77SjX0u4GWetM/edit#heading=h.uudp9kaqkwsz).

Please note that this package sends metrics to the MWS endpoint using [SEH1 histogram distributions](https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation#SEH1_distribution). For more information on SEH1 (Sparse Exponential Histograms), please see the related [Amazon wiki](https://w.amazon.com/index.php/MonitoringTeam/MetricAgent/SEHAggregation). Histograms have percentile support, so all datums will support min, max, avg, sample count, sum, and any arbitrary percentile (such as p50, p90, p99, etc.) when being graphed.

Additionally, please note that, since PMET has [9 set dimensions](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality) (and 1 metric name), there is an internal mapping from TwitchTelemetry sample dimension to PMET dimension. The mapping is based [Service Tuples and Process identifiers](https://docs.google.com/document/d/1Oc0-Ut6mt1EKw46Iha2Tupe2rHJo3DsoJ7kw9dEvOUE/) and looks as follows:


| TwitchTelemetry Dimension | PMET Dimension | Reasoning | Additional Notes|
| ------------------------- |:--------------:|:---------:|----------------:|
|Service|ServiceName|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|N/A|
|Stage|DataSet|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|N/A|
|Substage|HostGroup|A substag represents a group of instances within a Stage|N/A|
|Region|Marketplace|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|N/A|
|Operation|MethodName|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|N/A|
|ProcessAddress|Host|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|This value is commonly ProcessAddress.Machine|
|TODO (None yet)|Client|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|N/A|
|Dependency|MetricClass|[Service Dimension Cardinality Wiki](https://w.amazon.com/index.php/Monitoring/Documentation/ServiceQueryLog#Service_Dimension_Cardinality)|The value is concatenated as `Dependency:DependencyValue`|
|Any custom dimension|Instance|We can combine dependency values in MetricClass, leaving Instance as the single value for custom metrics|The value is a concatenated, sorted string list as `CustomA:Val1_CustomB:Val2...`|

#### Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchTelemetryMWSMetricsSender`

#### Running unit tests
When using brazil, simply run `brazil-build` to build and run all unit tests

#### Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel