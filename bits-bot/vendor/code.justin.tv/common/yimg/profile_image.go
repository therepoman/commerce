package yimg

import (
	"fmt"
)

const profileImageFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/%s-profile_image-{{.Uid}}-{{.Size}}.{{.Format}}"
const profileImageNewFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/{{.Uid}}-profile_image-{{.Size}}.{{.Format}}"

var ProfileImageSizes = []string{"600x600", "300x300", "150x150", "70x70", "50x50", "28x28", "96x96"}
var ProfileImageRatio = 1.0

// ProfileImages converts database yaml into an Images map for the provided username
func ProfileImages(data []byte, username string) (Images, error) {
	return parse(fmt.Sprintf(profileImageFormat, username), profileImageNewFormat, data, ProfileImageSizes, bySize)
}

func ProfileImagesToString(images *Images) (*string, error) {
	return images.Marshal(&ProfileImageRatio)
}

func ProfileImagesName(image Image, username string) (string, error) {
	if image.NewFormat {
		return fmt.Sprintf("%s-profile_image-%s.%s", image.Uid, image.Size, image.Format), image.validate()
	}
	return fmt.Sprintf("%s-profile_image-%s-%s.%s", username, image.Uid, image.Size, image.Format), image.validate()
}
