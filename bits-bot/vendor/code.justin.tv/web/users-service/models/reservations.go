package models

import "time"

type ReservationProperties struct {
	Login     string     `json:"login" validate:"nonzero" `
	Type      string     `json:"type" validate:"regexp=JTV Reservation|Reserved Record|User Block Record|Partner Block Record"`
	Reason    string     `json:"reason" validate:"nonzero" `
	ExpiresOn *time.Time `json:"expires_on" `
}

type ReservationPropertiesResult struct {
	Results []ReservationProperties `json:"results"`
}
