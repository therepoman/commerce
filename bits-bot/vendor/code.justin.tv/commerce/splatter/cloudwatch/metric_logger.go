package cloudwatch

import (
	"errors"
	"math/rand"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

const (
	unitCount      = "Count"
	unitSeconds    = "Seconds"
	unitNone       = "None"
	stageDimension = "stage"
)

// IMetricLogger interface
type IMetricLogger interface {
	LogDurationMetric(name string, duration time.Duration, sampleRate float32) error
	LogDurationSinceMetric(name string, timeOfEvent time.Time, sampleRate float32) error
	LogCountMetric(name string, value int64, sampleRate float32) error
	LogGaugeMetric(name string, value int64, sampleRate float32) error
}

// MetricLogger logs metrics
type MetricLogger struct {
	stage               string
	metricFlusher       IMetricFlusher
	defaultSamplingRate float32
	random              *rand.Rand
}

// NewMetricLogger creates a new MetricLogger using the inputs.
// Returns an error if any inputs are invalid.
func NewMetricLogger(stage string, metricFlusher IMetricFlusher) (IMetricLogger, error) {
	if stage == "" {
		return nil, errors.New("Stage cannot be empty")
	}
	if metricFlusher == nil {
		return nil, errors.New("MetricFlusher cannot be nil")
	}

	return &MetricLogger{
		stage:         stage,
		metricFlusher: metricFlusher,
	}, nil
}

// LogDurationSinceMetric logs the amount of time that has occured since the passed in time
func (logger *MetricLogger) LogDurationSinceMetric(name string, timeOfEvent time.Time, sampleRate float32) error {
	delta := time.Since(timeOfEvent)
	return logger.LogDurationMetric(name, delta, sampleRate)
}

// LogDurationMetric logs the amount of time that is passed in
func (logger *MetricLogger) LogDurationMetric(name string, duration time.Duration, sampleRate float32) error {
	return logger.logMetric(name, unitSeconds, duration.Seconds(), sampleRate)
}

// LogCountMetric logs the count passed in for the associated metric name
func (logger *MetricLogger) LogCountMetric(name string, value int64, sampleRate float32) error {
	return logger.logMetric(name, unitCount, float64(value), sampleRate)
}

// LogGaugeMetric logs the count passed in for the associated metric name
func (logger *MetricLogger) LogGaugeMetric(name string, value int64, sampleRate float32) error {
	return logger.logMetric(name, unitNone, float64(value), sampleRate)
}

func (logger *MetricLogger) logMetric(metricName string, metricUnit string, metricValue float64, sampleRate float32) error {
	sampleIndex := rand.Float32()
	if sampleIndex > sampleRate {
		return nil
	}

	sampleRate64 := float64(sampleRate) // To be consistent with the statsd counter-part we keep the param type float32. Convert for actual calculation.

	metricDatum := &cloudwatch.MetricDatum{
		MetricName: aws.String(metricName),
		Dimensions: []*cloudwatch.Dimension{
			{
				Name:  aws.String(stageDimension),
				Value: aws.String(logger.stage),
			},
		},
		Timestamp: aws.Time(time.Now().UTC()),
		Unit:      aws.String(metricUnit),
		// We don't accumulate metrics and pass all accumulated metrics. We abandon non-sampled data and send one datum per one extrapolated sample.
		Counts: []*float64{aws.Float64(1.0 / sampleRate64)},
		Values: []*float64{aws.Float64(metricValue)},
	}
	return logger.metricFlusher.AddMetricToBuffer(metricDatum)
}
