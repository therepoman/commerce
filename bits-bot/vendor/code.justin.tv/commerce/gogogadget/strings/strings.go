package strings

import (
	"errors"
	"regexp"
	"strconv"
	"strings"

	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/gogogadget/pointers"
)

// one or more digits followed by a period followed by exactly two digits
var priceRegex = regexp.MustCompile(`^\d+\.\d\d$`)

func CommatizeInt(value int) string {
	tempValue := strconv.Itoa(value)
	humanizeTotal := []string{}
	count := 0
	for i := len(tempValue) - 1; i >= 0; i-- {
		if count == 3 {
			humanizeTotal = append(humanizeTotal, ",")
			count = 0
		}
		humanizeTotal = append(humanizeTotal, string(tempValue[i]))
		count++
	}

	return Reverse(strings.Join(humanizeTotal, ""))
}

type Set interface {
	Add(s string)
	Contains(s string) bool
	GetList() []string
}

type set struct {
	l []string
	m map[string]interface{}
}

func (s *set) Add(str string) {
	if !s.Contains(str) {
		s.l = append(s.l, str)
		s.m[str] = nil
	}
}

func (s *set) Contains(str string) bool {
	_, ok := s.m[str]
	return ok
}

func (s *set) GetList() []string {
	return s.l
}

func NewSet() Set {
	return &set{
		l: make([]string, 0),
		m: make(map[string]interface{}),
	}
}

func NewSetFromArray(array []string) Set {
	set := NewSet()

	for _, entry := range array {
		set.Add(entry)
	}

	return set
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func BlankP(s *string) bool {
	return s == nil || strings.TrimSpace(*s) == ""
}

func Blank(s string) bool {
	return BlankP(pointers.StringP(s))
}

func NotBlankP(s *string) bool {
	return !BlankP(s)
}

func NotBlank(s string) bool {
	return !Blank(s)
}

func OneOf(s string, validOptions ...string) bool {
	for _, validOption := range validOptions {
		if validOption == s {
			return true
		}
	}
	return false
}

func Trucate(s string, maxLen int) string {
	return s[:math.MinInt(maxLen, len(s))]
}

func IsInt32Parsable(s string) bool {
	_, err := strconv.ParseInt(s, 10, 32)
	return err == nil
}

func IsInt64Parsable(s string) bool {
	_, err := strconv.ParseInt(s, 10, 64)
	return err == nil
}

func IsFloatParsable(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

func Dedupe(targetStrings []string) []string {
	seen := make(map[string]interface{})
	uniqueStrings := make([]string, 0)

	if len(targetStrings) == 0 {
		return uniqueStrings
	}

	for _, targetString := range targetStrings {
		if _, ok := seen[targetString]; !ok {
			seen[targetString] = nil
			uniqueStrings = append(uniqueStrings, targetString)
		}
	}

	return uniqueStrings
}

func USDPriceStringToCents(str string) (int64, error) {
	str = strings.TrimSpace(str)
	if !priceRegex.MatchString(str) {
		return 0, errors.New("not a valid USD price string")
	}

	str = strings.Replace(str, ".", "", -1)

	cents, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0, err
	}

	return cents, nil
}
