package heartbeatiface

import (
	"context"

	"code.justin.tv/systems/sandstorm/inventory/heartbeat"
)

// API is the interface that Client implements
type API interface {
	UpdateHeartbeat(secret *heartbeat.Secret)
	Start()
	FlushHeartbeat(ctx context.Context)
	SendHeartbeat()
	Stop() error
}
