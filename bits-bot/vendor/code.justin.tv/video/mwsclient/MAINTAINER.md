# Package Maintenance

This package exists both in Amazon's GitFarm / Brazil and on Twitch's GitHub
Enterprise, with users on each side. 

The Amazon side is here: https://code.amazon.com/packages/Twitch-Video-Mwsclient

The Twitch side is here: https://git-aws.internal.justin.tv/video/mwsclient

## Staying in Sync

Changes need to be kept in sync in both locations; both the code and the commits
themselves need to be published equally. Here are the commands to make that
happen.

### Setup

Set up a Brazil workspace for the project

    brazil ws create -vs Twitch/live -n ws-mwsclient
    cd ws-mwsclient/
    brazil ws use -p Twitch-Video-Mwsclient
    cd src/Twitch-Video-Mwsclient/

Add the GitHub Enterprise remote to your local checkout

    git remote add ghe git+ssh://git@git-aws.internal.justin.tv/video/mwsclient

### For Each Change

Get everything up to date

    brazil ws sync --md
    brazil ws sync

Confirm that the build works

    brazil-build

Make your change, re-confirm the build

    brazil-build

Send your change for review

    cr

After editing in response to code review feedback, re-send the change

    git commit --amend
    cr

Once the change is approved, use https://code.amazon.com/review to include your
change in the fast-forward mode.

Then get the changes from the Amazon side and upload them to the Twitch side

    git fetch
    git push ghe origin/mainline:master
