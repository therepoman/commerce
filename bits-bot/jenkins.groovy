job {
    name 'commerce-bits-bot'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'commerce/bits-bot', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
        }
    }

    steps {
        shell './scripts/docker_build.sh'
        shell './scripts/docker_push.sh'
        saveDeployArtifact 'commerce/bits-bot-deploy', 'deploy'
    }
}

job {
    name 'commerce-bits-bot-coverage'
    using 'TEMPLATE-autobuild'

    scm {
        git {
            remote {
                github 'commerce/bits-bot', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        credentialsBinding {
            string 'AWS_ACCESS_KEY', 'jenkins-terraform-aws-access-key'
            string 'AWS_SECRET_KEY', 'jenkins-terraform-aws-secret-key'
        }
        environmentVariables {
            env('GIT_BRANCH', '${GIT_BRANCH}')
            env('GIT_COMMIT', '${GIT_COMMIT}')
            env('GIT_URL', '${GIT_URL}')
        }
    }

    steps {
        shell 'manta -v -f .manta-coverage.json'
    }

    publishers {
        reportQuality('commerce/bits-bot', '.manta/coverage', '*.xml')
    }
}

freeStyleJob('commerce-bits-bot-prod-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'bits-bot-prod-tcs-access-key'
            string 'AWS_SECRET_KEY', 'bits-bot-prod-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/bits-bot-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-bits-bot:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy prod-commerce-bits-bot-env""".stripMargin()
    }
}
