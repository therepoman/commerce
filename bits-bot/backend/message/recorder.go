package message

import (
	"crypto/sha1"
	"fmt"
	"io"

	dynamo_message "code.justin.tv/commerce/bits-bot/backend/dynamo/message"
	"code.justin.tv/commerce/bits-bot/backend/sentiment"
	"code.justin.tv/commerce/bits-bot/config"
)

type Message struct {
	SlackUserID    string
	Timestamp      string
	SlackChannelID string
	Body           string
	Tone           sentiment.Tone
	Topic          sentiment.Topic
}

type Recorder interface {
	Record(msg Message) error
}

type RecorderImpl struct {
	Config     *config.Config     `inject:""`
	MessageDAO dynamo_message.DAO `inject:""`
}

func (r *RecorderImpl) Record(msg Message) error {
	hash := sha1.New()
	_, err := io.WriteString(hash, string(msg.SlackUserID))
	if err != nil {
		return err
	}
	slackUserHash := fmt.Sprintf("%x", hash.Sum(nil))

	err = r.MessageDAO.CreateNew(&dynamo_message.Message{
		SlackUserHash:  slackUserHash,
		SlackTimestamp: msg.Timestamp,
		Channel:        msg.SlackChannelID,
		Message:        msg.Body,
		Tone:           string(msg.Tone),
		Topic:          string(msg.Topic),
	})
	if err != nil {
		return err
	}

	return nil
}
