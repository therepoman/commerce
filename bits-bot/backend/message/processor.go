package message

import (
	"strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/key"
	"code.justin.tv/commerce/bits-bot/backend/mention"
	"code.justin.tv/commerce/bits-bot/backend/response"
	"code.justin.tv/commerce/bits-bot/backend/secret"
	"code.justin.tv/commerce/bits-bot/backend/sentiment"
	"code.justin.tv/commerce/bits-bot/backend/text"
	"code.justin.tv/commerce/bits-bot/config"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

const (
	botMessageSubType     = "bot_message"
	messageRepliedSubType = "message_replied"
	messageDeletedSubType = "message_deleted"
)

type Processor interface {
	Process(msg *slack.MessageEvent) error
}

type ProcessorImpl struct {
	Config              *config.Config         `inject:""`
	SlackClient         *slack.Client          `inject:"slack_client_1"`
	TextTokenizer       text.Tokenizer         `inject:""`
	MentionCounter      mention.Counter        `inject:""`
	SentimentAnalyzer   sentiment.Analyzer     `inject:""`
	KeyManager          key.Manager            `inject:""`
	Responder           response.Responder     `inject:""`
	Stats               statsd.Statter         `inject:""`
	MessageRecorder     Recorder               `inject:""`
	SecretFeatureManger secret.FeaturesManager `inject:""`
}

func (p *ProcessorImpl) logCountMetric(name string) {
	err := p.Stats.Inc(name, 1, 1.0)
	if err != nil {
		log.WithField("name", name).WithError(err).Error("error logging count metric")
	}
}

func (p *ProcessorImpl) logDurationMetric(name string, dur time.Duration) {
	err := p.Stats.TimingDuration(name, dur, 1.0)
	if err != nil {
		log.WithField("name", name).WithError(err).Error("error logging duration metric")
	}
}

func (p *ProcessorImpl) Process(msg *slack.MessageEvent) error {
	startTime := time.Now()

	if msg == nil {
		return nil
	}

	user := msg.User
	channel := msg.Channel
	txt := msg.Text

	if p.Config.Environment == config.Local && channel != p.Config.BitsBotTestChannelID {
		return nil
	}

	if user == p.Config.SlackUserID {
		return nil // Ignore messages sent by self
	}

	// Ignore messages sent by bots
	if msg.SubType == botMessageSubType ||
		// Notifications of thread responses (note: this event is sent on top of the thread reply itself)
		msg.SubType == messageRepliedSubType ||
		// Notifications of message deletion
		msg.SubType == messageDeletedSubType {
		return nil
	}

	_, err := p.SlackClient.GetConversationInfo(channel, false)
	if err != nil {
		log.WithError(err).Error("Error getting conversation info")
	}

	processedSecretFeature, err := p.SecretFeatureManger.ProcessMessageForSecretFeatures(msg, nil)
	if err != nil {
		log.WithError(err).Error("Error processing message for secret features. Continuing to process message normally.. ")
	} else if processedSecretFeature {
		return nil
	}

	isDM := false //conversation.IsIM

	if channel == "CD43U3J64" {
		p.logCountMetric("message-processor.request")

		defer func() {
			latency := time.Since(startTime)
			p.logDurationMetric("message-processor.request.latency", latency)
		}()

		err = p.MessageRecorder.Record(Message{
			SlackUserID:    user,
			Timestamp:      msg.Timestamp,
			SlackChannelID: channel,
			Body:           txt,
			Topic:          sentiment.BitsTopic,
			Tone:           sentiment.PoliteTone,
		})
		if err != nil {
			return err
		}

		dmChannel, _, _, err := p.SlackClient.OpenConversation(&slack.OpenConversationParameters{
			Users:    []string{user},
			ReturnIM: true,
		})
		if err != nil {
			return err
		}

		slackUser, err := p.SlackClient.GetUserInfo(user)
		if err != nil {
			return err
		}

		allocateResp, err := p.KeyManager.AllocateKey(user, slackUser.Name)
		if err != nil {
			return err
		}

		if !allocateResp.UserEligible {
			if strings.Contains(strings.ToLower(msg.Text), "please") || strings.Contains(strings.ToLower(msg.Text), "plz") ||
				strings.Contains(strings.ToLower(msg.Text), "pls") {
				_, _, err = p.SlackClient.PostMessage(channel, toSlackMsgOption(p.Responder.UserNotEligible(slackUser.Name, allocateResp.DurationTillEligible))...)
			}
			p.logCountMetric("message-processor.rejected.user-not-eligible")
			return err
		}

		if !allocateResp.InventoryAvailable {
			_, _, err = p.SlackClient.PostMessage(channel, toSlackMsgOption(p.Responder.NoInventory())...)
			p.logCountMetric("message-processor.rejected.no-inventory")
			return err
		}

		_, _, err = p.SlackClient.PostMessage(dmChannel.ID, toSlackMsgOption(p.Responder.ProvideKey(allocateResp.Key))...)
		if err != nil {
			return err
		}

		p.logCountMetric("message-processor.key-allocated")

		defer func() {
			latency := time.Since(startTime)
			p.logDurationMetric("message-processor.key-allocated.latency", latency)
		}()

		if isDM {
			return nil
		}

		postMsgParams := slack.NewPostMessageParameters()
		postMsgParams.LinkNames = 1 // makes @ mentions properly tokenize
		_, _, err = p.SlackClient.PostMessage(channel, toSlackMsgOption(p.Responder.NotificationOfDM(slackUser.Name))...)
		return err
	}

	return nil
}

func toSlackMsgOption(msg string) []slack.MsgOption {
	params := slack.NewPostMessageParameters()
	params.LinkNames = 1
	return []slack.MsgOption{slack.MsgOptionPostMessageParameters(params), slack.MsgOptionText(msg, false)}
}
