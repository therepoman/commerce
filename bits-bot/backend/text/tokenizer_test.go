package text_test

import (
	"strings"
	"testing"

	"code.justin.tv/commerce/bits-bot/backend/text"
	. "github.com/smartystreets/goconvey/convey"
)

func testCase(input string, expectedTokens ...string) {
	tokenizer := &text.TokenizerImpl{}
	actualTokens, err := tokenizer.Tokenize(input)

	So(err, ShouldBeNil)
	So(len(actualTokens), ShouldEqual, len(expectedTokens))
	for i := 0; i < len(actualTokens); i++ {
		So(actualTokens[i].String(), ShouldEqual, expectedTokens[i])
	}
}

func buildLoadTestInput(phrase string) (string, []string) {
	input := ""
	tokens := make([]string, 0)
	for len(input) < 4000 {
		input += phrase + " "
		tokens = append(tokens, phrase)
	}
	input = strings.TrimSpace(input)
	return input, tokens
}

func TestTokenizerImpl_Tokenize(t *testing.T) {
	Convey("Given a tokenizer", t, func() {
		Convey("Test various cases", func() {
			testCase("a", "a")
			testCase("a b", "a", "b")
			testCase("a b c", "a", "b", "c")
			testCase("por favor a", "por favor", "a")
			testCase("a por favor", "a", "por favor")
			testCase("a por favor a", "a", "por favor", "a")
			testCase("por favor por favor", "por favor", "por favor")
			testCase("por por favor favor", "por", "por favor", "favor")
			testCase("por por favor favor", "por", "por favor", "favor")
			testCase("a por favor b por favor c por favor d", "a", "por favor", "b", "por favor", "c", "por favor", "d")
			testCase("por favor s’il vous plaît", "por favor", "s’il vous plaît")
			testCase("por favor a s’il vous plaît", "por favor", "a", "s’il vous plaît")
			testCase("s’il vous plaît s’il vous plaît", "s’il vous plaît", "s’il vous plaît")
			testCase("s’il vous plaît s’il vous plaît s’il vous plaît", "s’il vous plaît", "s’il vous plaît", "s’il vous plaît")
			testCase("por favor <@ABC> s’il vous plaît", "por favor", "<@ABC>", "s’il vous plaît")
			testCase("POR FAVOR <@ABC> POR FAVOR", "por favor", "<@ABC>", "por favor")
			testCase("pOr FaVoR <@ABC> POR favor", "por favor", "<@ABC>", "por favor")
			testCase("!por favor? <@ABC> ,por favor.", "por favor", "<@ABC>", "por favor")
			testCase(":giveplz::cheer100:", ":giveplz:", ":cheer100:")
			testCase("!a! !b! !c! !bits! !please! !d!", "a", "b", "c", "bits", "please", "d")
		})

		Convey("Perform por favor load test", func() {
			input, expectedTokens := buildLoadTestInput("por favor")
			testCase(input, expectedTokens...)
		})

		Convey("Perform s’il vous plaît load test", func() {
			input, expectedTokens := buildLoadTestInput("s’il vous plaît")
			testCase(input, expectedTokens...)
		})
	})
}
