package text

import (
	"fmt"
	"regexp"
	"strings"

	"code.justin.tv/commerce/bits-bot/backend/phrase"
	string_utils "code.justin.tv/commerce/gogogadget/strings"
)

const (
	punctuationToTrim    = ".,!?"
	slackMentionRegexStr = "<@[A-Z0-9]+>"
)

var slackMentionRegex *regexp.Regexp

func init() {
	slackMentionRegex = regexp.MustCompile(slackMentionRegexStr)
}

type Token interface {
	String() string
}

type TextToken string

func (tt TextToken) String() string {
	return string(tt)
}

type MentionToken struct {
	Raw    string
	UserID string
}

func (mt MentionToken) String() string {
	return mt.Raw
}

type Tokenizer interface {
	Tokenize(text string) ([]Token, error)
}

type TokenizerImpl struct {
}

func (t *TokenizerImpl) Tokenize(text string) ([]Token, error) {
	tokens := make([]Token, 0)

	text = strings.Replace(text, "::", ": :", -1)

	regex := BuildPhraseRegex()
	strTokens := regex.FindAllString(text, -1)

	for _, strToken := range strTokens {
		if slackMentionRegex.MatchString(strToken) {
			userID := strToken
			userID = strings.TrimPrefix(userID, "<@")
			userID = strings.TrimSuffix(userID, ">")
			tokens = append(tokens, MentionToken{
				Raw:    strToken,
				UserID: userID,
			})
		} else {
			strToken = strings.ToLower(strToken)
			strToken = strings.Trim(strToken, punctuationToTrim)
			if !string_utils.Blank(strToken) {
				tokens = append(tokens, TextToken(strToken))
			}
		}
	}

	return tokens, nil
}

func BuildPhraseRegex() *regexp.Regexp {
	multiWordPhrases := phrase.GetMultiWordPhrases()
	regexStr := "(?i)" // case insensitive
	for phr := range multiWordPhrases {
		// match each multi-word phrase which may optionally be surrounded by punctuation
		regexStr += fmt.Sprintf("([%s]*%s[%s]*)|", punctuationToTrim, phr, punctuationToTrim)
	}
	// match other words
	regexStr += "[^ ]+"

	regex := regexp.MustCompile(regexStr)
	return regex
}
