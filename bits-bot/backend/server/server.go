package server

import (
	"context"

	"code.justin.tv/commerce/bits-bot/backend/api/bits_bot_roulette"

	"code.justin.tv/commerce/bits-bot/backend/api/grant_daily_pbx"

	"code.justin.tv/commerce/bits-bot/backend/api/configure_bot"
	"code.justin.tv/commerce/bits-bot/backend/api/get_available_key_count"
	"code.justin.tv/commerce/bits-bot/backend/api/get_pr_settings"
	"code.justin.tv/commerce/bits-bot/backend/api/ingest_keys"
	"code.justin.tv/commerce/bits-bot/backend/api/post_daily_digest"
	"code.justin.tv/commerce/bits-bot/backend/api/process_pull_request"
	"code.justin.tv/commerce/bits-bot/backend/api/redis_health_check"
	"code.justin.tv/commerce/bits-bot/backend/api/update_pr_settings"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
)

type Server struct {
	RedisHealthCheckAPI     *redis_health_check.API      `inject:""`
	IngestKeysAPI           *ingest_keys.API             `inject:""`
	GetAvailableKeyCountAPI *get_available_key_count.API `inject:""`
	ConfigureBotAPI         *configure_bot.API           `inject:""`
	ProcessPullRequestAPI   process_pull_request.API     `inject:""`
	UpdatePRSettingsAPI     update_pr_settings.API       `inject:""`
	GetPRSettingsAPI        get_pr_settings.API          `inject:""`
	PostDailyDigestAPI      *post_daily_digest.API       `inject:""`
	GrantDailyPBXAPI        grant_daily_pbx.API          `inject:""`
	BitsBotRouletteAPI      bits_bot_roulette.API        `inject:""`
}

func (s *Server) HealthCheck(ctx context.Context, req *bits_bot.HealthCheckReq) (*bits_bot.HealthCheckResp, error) {
	return &bits_bot.HealthCheckResp{}, nil
}

func (s *Server) RedisHealthCheck(ctx context.Context, req *bits_bot.RedisHealthCheckReq) (*bits_bot.RedisHealthCheckResp, error) {
	return s.RedisHealthCheckAPI.RedisHealthCheck(ctx, req)
}

func (s *Server) IngestKeys(ctx context.Context, req *bits_bot.IngestKeysReq) (*bits_bot.IngestKeysResp, error) {
	return s.IngestKeysAPI.IngestKeys(ctx, req)
}

func (s *Server) GetAvailableKeyCount(ctx context.Context, req *bits_bot.GetAvailableKeyCountReq) (*bits_bot.GetAvailableKeyCountResp, error) {
	return s.GetAvailableKeyCountAPI.GetAvailableKeyCount(ctx, req)
}

func (s *Server) ConfigureBot(ctx context.Context, req *bits_bot.ConfigureBotReq) (*bits_bot.ConfigureBotResp, error) {
	return s.ConfigureBotAPI.ConfigureBot(ctx, req)
}

func (s *Server) ProcessPullRequest(ctx context.Context, req *bits_bot.ProcessPullRequestReq) (*bits_bot.ProcessPullRequestResp, error) {
	return s.ProcessPullRequestAPI.Process(ctx, req)
}

func (s *Server) UpdatePRSettings(ctx context.Context, req *bits_bot.UpdatePRSettingsReq) (*bits_bot.UpdatePRSettingsResp, error) {
	return s.UpdatePRSettingsAPI.Update(ctx, req)
}

func (s *Server) GetPRSettings(ctx context.Context, req *bits_bot.GetPRSettingsReq) (*bits_bot.GetPRSettingsResp, error) {
	return s.GetPRSettingsAPI.Get(ctx, req)
}

func (s *Server) PostDailyDigest(ctx context.Context, req *bits_bot.PostDailyDigestReq) (*bits_bot.PostDailyDigestResp, error) {
	return s.PostDailyDigestAPI.PostDailyDigest(ctx, req)
}

func (s *Server) GrantDailyPBX(ctx context.Context, req *bits_bot.GrantDailyPBXReq) (*bits_bot.GrantDailyPBXResp, error) {
	return s.GrantDailyPBXAPI.Grant(ctx, req)
}

func (s *Server) BitsBotRoulette(ctx context.Context, req *bits_bot.BitsBotRouletteReq) (*bits_bot.BitsBotRouletteResp, error) {
	return s.BitsBotRouletteAPI.MaybeSpin(ctx, req)
}
