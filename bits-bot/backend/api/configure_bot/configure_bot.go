package configure_bot

import (
	"context"

	"code.justin.tv/commerce/bits-bot/bot"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"github.com/twitchtv/twirp"
)

type API struct {
}

func (api *API) ConfigureBot(ctx context.Context, req *bits_bot.ConfigureBotReq) (*bits_bot.ConfigureBotResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("body", "request cannot be nil")
	}

	// TODO: Use redis for bot configuration
	bot.Running = req.Running

	return &bits_bot.ConfigureBotResp{}, nil
}
