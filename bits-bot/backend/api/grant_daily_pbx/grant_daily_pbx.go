package grant_daily_pbx

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/bits-bot/backend/secret/pbx"
	"code.justin.tv/commerce/bits-bot/config"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"github.com/nlopes/slack"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API interface {
	Grant(ctx context.Context, req *bits_bot.GrantDailyPBXReq) (*bits_bot.GrantDailyPBXResp, error)
}

type APIImpl struct {
	PBX         pbx.PBX        `inject:""`
	SlackClient *slack.Client  `inject:"slack_client_1"`
	Config      *config.Config `inject:""`
}

func NewAPI() API {
	return &APIImpl{}
}

func (api *APIImpl) Grant(ctx context.Context, req *bits_bot.GrantDailyPBXReq) (*bits_bot.GrantDailyPBXResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("req", "request cannot be nil")
	}

	amountGranted, err := api.PBX.GrantDailyPBX()
	if err != nil {
		api.messageOnDailyGrantError()
		return nil, twirp.InternalError("error granting daily PBX")
	}

	goldenTicketWinner, goldenTicketAmount, err := api.PBX.GrantGoldenTicket()
	if err != nil {
		api.messageOnGoldenTicketError()
	}

	message := getSuccessMessage(amountGranted, goldenTicketWinner, goldenTicketAmount)
	_, _, err = api.SlackClient.PostMessage(api.Config.ThePubbChannelID, slack.MsgOptionText(message, false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		return nil, err
	}

	return &bits_bot.GrantDailyPBXResp{}, nil
}

func (api *APIImpl) messageOnDailyGrantError() {
	_, _, err := api.SlackClient.PostMessage(api.Config.ThePubbChannelID, slack.MsgOptionText("Failed to grant daily PBX", false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		logrus.WithError(err).Error("failed to post daily PBX grant error to Slack")
	}
}

func (api *APIImpl) messageOnGoldenTicketError() {
	_, _, err := api.SlackClient.PostMessage(api.Config.ThePubbChannelID, slack.MsgOptionText("Failed to grant PBX golden ticket", false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		logrus.WithError(err).Error("failed to post daily PBX golden ticket error to Slack")
	}
}

func getSuccessMessage(amountGranted int64, goldenTicketWinner string, goldenTicketAmount int64) string {
	goldenTicketMessage := ""

	if goldenTicketWinner != "" {
		goldenTicketMessage = fmt.Sprintf(" :praizos: %s has been granted an additional %d PBX!", goldenTicketWinner, goldenTicketAmount)
	}

	return fmt.Sprintf(":pog: Daily PBX Grant :pog: %d PBX has been added to each user's balance.%s", amountGranted, goldenTicketMessage)
}
