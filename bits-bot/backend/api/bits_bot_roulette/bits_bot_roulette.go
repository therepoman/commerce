package bits_bot_roulette

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/bits-bot/backend/secret/pbx"
	"code.justin.tv/commerce/bits-bot/config"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"code.justin.tv/commerce/gogogadget/random"
	"github.com/nlopes/slack"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	minimumSpinAsPercentageOfWorth = 1
	maximumSpinAsPercentageOfWorth = 10

	pocketToBetOn = "7"
)

type API interface {
	MaybeSpin(ctx context.Context, req *bits_bot.BitsBotRouletteReq) (*bits_bot.BitsBotRouletteResp, error)
}

type APIImpl struct {
	PBX          pbx.PBX            `inject:""`
	SlackClient  *slack.Client      `inject:"slack_client_1"`
	Config       *config.Config     `inject:""`
	PBXProcessor *pbx.ProcessorImpl `inject:""`
}

func NewAPI() API {
	return &APIImpl{}
}

func (api *APIImpl) MaybeSpin(ctx context.Context, req *bits_bot.BitsBotRouletteReq) (*bits_bot.BitsBotRouletteResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("req", "request cannot be nil")
	}

	// Cron is set up to execute between 9-4 M-F. To make it less spammy, further limit to every 3 hours during that
	// period.
	if random.Int(1, 3) != 3 {
		return &bits_bot.BitsBotRouletteResp{}, nil
	}

	balance, err := api.PBX.GetBalance(api.Config.SlackUserID)
	if err != nil {
		api.messageOnError()
		return nil, twirp.InternalError("error getting bits-bot balance")
	}

	percentageToSpin := random.Int64(minimumSpinAsPercentageOfWorth, maximumSpinAsPercentageOfWorth)
	amountF := float64(balance) * (float64(percentageToSpin) / float64(100))
	amount := int64(amountF)

	if amount < 1 {
		return &bits_bot.BitsBotRouletteResp{}, nil
	}

	startMessage := fmt.Sprintf(":mrdestructoid: Preparing to spin. Wish me luck, humans :mrdestructoid:\n\n`./pbx -r %s %d`", pocketToBetOn, amount)
	_, _, err = api.SlackClient.PostMessage(api.Config.ThePubbChannelID, slack.MsgOptionText(startMessage, false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		return nil, err
	}

	results, err := api.PBX.Roulette(api.Config.SlackUserID, pocketToBetOn, amount, 1)
	if err != nil {
		api.messageOnError()
		return nil, twirp.InternalError("error spinning on behalf of bits-bot")
	}

	newBalance, err := api.PBX.GetBalance(api.Config.SlackUserID)
	if err != nil {
		api.messageOnError()
		return nil, twirp.InternalError("error getting bits-bot balance")
	}

	_, _, err = api.SlackClient.PostMessage(api.Config.ThePubbChannelID, slack.MsgOptionText(api.PBXProcessor.GetRouletteMessage(results, newBalance, amount), false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		return nil, err
	}

	return &bits_bot.BitsBotRouletteResp{}, nil
}

func (api *APIImpl) messageOnError() {
	_, _, err := api.SlackClient.PostMessage(api.Config.ThePubbChannelID, slack.MsgOptionText("Failed to spin on behalf of bits-bot", false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		logrus.WithError(err).Error("failed to spin on behalf of bits-bot")
	}
}
