package process_pull_request

import (
	"context"

	"code.justin.tv/commerce/bits-bot/backend/pull_requests/review_event"
	"code.justin.tv/commerce/bits-bot/backend/pull_requests/review_requested"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"code.justin.tv/commerce/gogogadget/strings"
	"github.com/twitchtv/twirp"
)

const (
	ReviewRequested = "review_requested"
	ReviewSubmitted = "submitted"
	ReviewDismissed = "dismissed"
	ReviewEdited    = "edited"
)

type API interface {
	Process(ctx context.Context, req *bits_bot.ProcessPullRequestReq) (*bits_bot.ProcessPullRequestResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	ReviewRequestedProcessor review_requested.Processor `inject:"reviewRequestedProcessor"`
	ReviewEventProcessor     review_event.Processor     `inject:"reviewEventProcessor"`
}

func (a *api) Process(ctx context.Context, req *bits_bot.ProcessPullRequestReq) (*bits_bot.ProcessPullRequestResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("action", "Cannot send blank request")
	}
	if strings.Blank(req.Action) {
		return nil, twirp.InvalidArgumentError("action", "cannot send blank action")
	}

	switch req.Action {
	case ReviewRequested:
		err := a.ReviewRequestedProcessor.Process(req)
		if err != nil {
			return nil, err
		}
		break
	case ReviewSubmitted, ReviewDismissed, ReviewEdited:
		err := a.ReviewEventProcessor.Process(req)
		if err != nil {
			return nil, err
		}
		break
	}

	return &bits_bot.ProcessPullRequestResp{}, nil
}
