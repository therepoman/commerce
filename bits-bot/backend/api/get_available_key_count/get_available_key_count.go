package get_available_key_count

import (
	"context"

	"code.justin.tv/commerce/bits-bot/backend/key"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyManager key.Manager `inject:""`
}

func (api *API) GetAvailableKeyCount(ctx context.Context, req *bits_bot.GetAvailableKeyCountReq) (*bits_bot.GetAvailableKeyCountResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("body", "request cannot be nil")
	}

	count, err := api.KeyManager.GetKeyCount()
	if err != nil {
		log.WithError(err).Error("error counting keys in redis set")
		return nil, twirp.InternalError("downstream dependency error")
	}

	return &bits_bot.GetAvailableKeyCountResp{
		Count: count,
	}, nil
}
