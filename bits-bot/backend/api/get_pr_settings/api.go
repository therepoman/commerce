package get_pr_settings

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/bits-bot/backend/dynamo/pr_settings"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"code.justin.tv/commerce/gogogadget/strings"
	"github.com/nlopes/slack"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *bits_bot.GetPRSettingsReq) (*bits_bot.GetPRSettingsResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	SlackClient *slack.Client   `inject:"slack_client_1"`
	SettingsDAO pr_settings.DAO `inject:""`
}

func (a *api) Get(ctx context.Context, req *bits_bot.GetPRSettingsReq) (*bits_bot.GetPRSettingsResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("req", "cannot be blank")
	}
	if strings.Blank(req.Ldap) {
		return nil, twirp.InvalidArgumentError("ldap", "cannot be blank")
	}

	user, err := a.SlackClient.GetUserByEmail(fmt.Sprintf("%s@twitch.tv", req.Ldap))
	if err != nil || user == nil {
		return nil, twirp.InternalError("could not retrieve user info from Slack")
	}

	userSettings, err := a.SettingsDAO.GetSettings(user.ID)
	if err != nil {
		return nil, twirp.InternalError("could not fetch user settings from dynamo")
	}

	var notificationsDisabled bool
	if userSettings != nil {
		notificationsDisabled = userSettings.NotificationsDisabled
	}

	return &bits_bot.GetPRSettingsResp{
		NotificationsDisabled: notificationsDisabled,
	}, nil
}
