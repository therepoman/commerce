package redis_health_check

import (
	"context"

	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"github.com/twitchtv/twirp"
)

type API struct {
	RedisClient redis.Client `inject:""`
}

func (api *API) RedisHealthCheck(ctx context.Context, req *bits_bot.RedisHealthCheckReq) (*bits_bot.RedisHealthCheckResp, error) {
	err := api.RedisClient.Ping()
	if err != nil {
		return nil, twirp.InternalError("error pinging redis")
	}
	return &bits_bot.RedisHealthCheckResp{}, nil
}
