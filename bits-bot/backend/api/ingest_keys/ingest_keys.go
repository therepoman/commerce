package ingest_keys

import (
	"context"

	"code.justin.tv/commerce/bits-bot/backend/key"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

type API struct {
	KeyManager key.Manager `inject:""`
}

func (api *API) IngestKeys(ctx context.Context, req *bits_bot.IngestKeysReq) (*bits_bot.IngestKeysResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("keys", "request cannot be nil")
	} else if len(req.Keys) == 0 {
		return nil, twirp.InvalidArgumentError("keys", "keys cannot be empty")
	}

	err := api.KeyManager.IngestKeys(req.Keys)
	if err != nil {
		log.WithError(err).Error("error adding keys to redis set")
		return nil, twirp.InternalError("downstream dependency error")
	}

	return &bits_bot.IngestKeysResp{}, nil
}
