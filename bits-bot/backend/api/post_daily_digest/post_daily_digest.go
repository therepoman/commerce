package post_daily_digest

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/message"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

// curated from https://gist.github.com/nicolehe/258249f20a7bef4ef08b
var boringWords = map[string]interface{}{
	" ":       nil,
	"":        nil,
	"a":       nil,
	"an":      nil,
	"as":      nil,
	"at":      nil,
	"but":     nil,
	"by":      nil,
	"for":     nil,
	"from":    nil,
	"in":      nil,
	"of":      nil,
	"off":     nil,
	"on":      nil,
	"out":     nil,
	"to":      nil,
	"too":     nil,
	"with":    nil,
	"the":     nil,
	"this":    nil,
	"that":    nil,
	"these":   nil,
	"those":   nil,
	"your":    nil,
	"his":     nil,
	"her":     nil,
	"its":     nil,
	"it's":    nil,
	"our":     nil,
	"their":   nil,
	"it":      nil,
	"them":    nil,
	"and":     nil,
	"was":     nil,
	"is":      nil,
	"or":      nil,
	"which":   nil,
	"will":    nil,
	"be":      nil,
	"also":    nil,
	"some":    nil,
	"comes":   nil,
	"any":     nil,
	"most":    nil,
	"takes":   nil,
	"whose":   nil,
	"would":   nil,
	"each":    nil,
	"can":     nil,
	"has":     nil,
	"all":     nil,
	"there":   nil,
	"no":      nil,
	"yes":     nil,
	"they're": nil,
	"theyre":  nil,
	"i":       nil,
	"me":      nil,
	"my":      nil,
	"he":      nil,
	"she":     nil,
	"yup":     nil,
	"yep":     nil,
	"nope":    nil,
	"am":      nil,
	"are":     nil,
	"aren't":  nil,
	"you":     nil,
	"we":      nil,
	"have":    nil,
	"like":    nil,
	"not":     nil,
	"if":      nil,
	"just":    nil,
	"so":      nil,
	"get":     nil,
	"do":      nil,
	"more":    nil,
	"who":     nil,
	"what":    nil,
	"when":    nil,
	"where":   nil,
	"why":     nil,
	"how":     nil,
}

type API struct {
	SlackClient1 *slack.Client `inject:"slack_client_1"`
	SlackClient2 *slack.Client `inject:"slack_client_2"`

	MessageProcessor message.Processor `inject:""`
}

func (api *API) PostDailyDigest(ctx context.Context, req *bits_bot.PostDailyDigestReq) (*bits_bot.PostDailyDigestResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("keys", "request cannot be nil")
	}

	if strings.Contains(req.SourceChannel,":MinuteScan") {
		req.SourceChannel = strings.Replace(req.SourceChannel, ":MinuteScan", "", 1)
		req.DestChannel = req.SourceChannel

		latestTime := time.Now()
		oldestTime := latestTime.Add(-1 * time.Minute)
		var cursor string
		for i := 0; i < 10; i++ {
			resp, err := api.SlackClient2.GetConversationHistory(&slack.GetConversationHistoryParameters{
				ChannelID: req.SourceChannel,
				Limit:     1000,
				Cursor:    cursor,
				Latest:    fmt.Sprintf("%d.0", latestTime.Unix()),
				Oldest:    fmt.Sprintf("%d.0", oldestTime.Unix()),
			})
			if err != nil {
				return nil, err
			}

			for _, msg := range resp.Messages {
				messageEvent := slack.MessageEvent(msg)
				messageEvent.Channel = req.SourceChannel
				err = api.MessageProcessor.Process(&messageEvent)
				if err != nil {
					log.WithError(err).Errorf("Error processing messages for channel: %s", req.SourceChannel)
				}
			}

			if !resp.HasMore {
				break
			}
			cursor = resp.ResponseMetaData.NextCursor
			time.Sleep(100 * time.Millisecond)
		}

		return nil, nil
	}

	latestTime := time.Now()
	oldestTime := latestTime.Add(-24 * time.Hour)

	messages, err := api.getMessages(req.SourceChannel, fmt.Sprintf("%d.0", latestTime.Unix()), fmt.Sprintf("%d.0", oldestTime.Unix()))
	if err != nil {
		return nil, err
	}

	wordCount := make(map[string]int)
	for _, message := range messages {
		words := strings.Split(message.Text, " ")
		for _, word := range words {
			word = strings.ToLower(strings.TrimSpace(word))
			if _, boring := boringWords[word]; !boring {
				wordCount[word]++
			}
		}
	}

	wordCounts := make([]WordCount, 0)
	for word, count := range wordCount {
		wordCounts = append(wordCounts, WordCount{
			Word:  word,
			Count: count,
		})
	}

	msg := fmt.Sprintf("*#the-pubb Daily Digest:* %s \n", time.Now().Format("Mon Jan _2 2006"))

	msg += "Top 10 non-boring words in the past 24 hours \n ```"
	sort.Sort(sort.Reverse(ByWordCount(wordCounts)))
	for i, wordCount := range wordCounts {
		if i >= 10 {
			break
		}
		msg += fmt.Sprintf("%02d : %s (%d) \n", i+1, wordCount.Word, wordCount.Count)
	}

	msg += "```\n"
	msg += "Top 5 posts by reaction count in the past 24 hours \n"
	sort.Sort(sort.Reverse(ByTotalReactions(messages)))
	attachments := make([]slack.Attachment, 0)
	for i, m := range messages {
		if i >= 5 {
			break
		}
		attachments = append(attachments, slack.Attachment{})
		msg += fmt.Sprintf("%s \n", api.GetMsgLink(req.SourceChannel, m))
	}

	_, _, err = api.SlackClient1.PostMessage(req.DestChannel, slack.MsgOptionText(msg, false), slack.MsgOptionEnableLinkUnfurl())
	if err != nil {
		return nil, err
	}

	return &bits_bot.PostDailyDigestResp{}, nil
}

type ByWordCount []WordCount

type WordCount struct {
	Word  string
	Count int
}

func (s ByWordCount) Len() int {
	return len(s)
}
func (s ByWordCount) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByWordCount) Less(i, j int) bool {
	return s[i].Count < s[j].Count
}

type Message struct {
	User           string
	Channel        string
	Text           string
	TotalReactions int
	TotalReplies   int
	Timestamp      string
}

type ByTotalReactions []Message

func (s ByTotalReactions) Len() int {
	return len(s)
}
func (s ByTotalReactions) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByTotalReactions) Less(i, j int) bool {
	return s[i].TotalReactions < s[j].TotalReactions
}

func (api *API) GetUserName(userID string) string {
	user, err := api.SlackClient1.GetUserInfo(userID)
	if err != nil {
		log.WithError(err).Error("error getting user from slack client")
	}

	if user == nil {
		return "UNKNOWN"
	}

	if user.Profile.DisplayName != "" {
		return user.Profile.DisplayName
	}
	return user.Name
}

func (api *API) getMessages(srcChannel string, latest string, oldest string) ([]Message, error) {
	var cursor string
	messages := make([]Message, 0)
	for i := 0; i < 10; i++ {
		resp, err := api.SlackClient2.GetConversationHistory(&slack.GetConversationHistoryParameters{
			ChannelID: srcChannel,
			Limit:     1000,
			Cursor:    cursor,
			Latest:    latest,
			Oldest:    oldest,
		})
		if err != nil {
			return nil, err
		}

		for _, msg := range resp.Messages {
			messages = append(messages, Message{
				User:           msg.User,
				Channel:        msg.Channel,
				Text:           msg.Text,
				TotalReplies:   msg.ReplyCount,
				TotalReactions: sumReactions(msg),
				Timestamp:      msg.Timestamp,
			})
		}

		if !resp.HasMore {
			break
		}
		cursor = resp.ResponseMetaData.NextCursor
		time.Sleep(100 * time.Millisecond)
	}
	return messages, nil
}

func sumReactions(msg slack.Message) int {
	total := 0
	for _, reaction := range msg.Reactions {
		total += reaction.Count
	}
	return total
}

func (api *API) GetMsgLink(srcChannel string, msg Message) string {
	permalink, err := api.SlackClient1.GetPermalink(&slack.PermalinkParameters{
		Channel: srcChannel,
		Ts:      msg.Timestamp,
	})
	if err != nil {
		log.WithError(err).Error("Error getting slack permalink to msg")
		return "Error getting msg link"
	}

	return permalink
}
