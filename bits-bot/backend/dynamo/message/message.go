package message

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetMessage(slackUserHash string, slackTimestamp time.Time) (*Message, error)
	CreateNew(message *Message) error
	CreateOrUpdate(message *Message) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("bits-bot-messages-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type Message struct {
	SlackUserHash  string `dynamo:"slack_user_hash"`
	SlackTimestamp string `dynamo:"slack_timestamp"`
	Channel        string `dynamo:"channel"`
	Message        string `dynamo:"message"`
	Tone           string `dynamo:"tone"`
	Topic          string `dynamo:"topic"`
}

func (dao *daoImpl) GetMessage(slackUserHash string, slackTimestamp time.Time) (*Message, error) {
	var msg Message
	err := dao.Get("slack_user_hash", slackUserHash).Range("message_time", dynamo.Equal, slackTimestamp).One(&msg)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &msg, err
}

func (dao *daoImpl) CreateNew(message *Message) error {
	if message == nil {
		return errors.New("cannot create nil message")
	}
	return dao.Put(*message).If("attribute_not_exists(slack_user_hash)").Run()
}

func (dao *daoImpl) CreateOrUpdate(message *Message) error {
	if message == nil {
		return errors.New("cannot create/update nil message")
	}
	return dao.Put(*message).Run()
}
