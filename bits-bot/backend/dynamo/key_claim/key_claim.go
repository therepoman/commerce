package key_claim

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

type DAO interface {
	GetKeyClaim(slackUser string, claimTime time.Time) (*KeyClaim, error)
	CreateNew(keyClaim *KeyClaim) error
	CreateOrUpdate(keyClaim *KeyClaim) error
	GetUsersLatestKeyClaim(slackUser string) (*KeyClaim, error)
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("bits-bot-key-claims-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type KeyClaim struct {
	SlackUser string    `dynamo:"slack_user"`
	SlackName string    `dynamo:"slack_name"`
	ClaimTime time.Time `dynamo:"claim_time"`
	KeyHash   string    `dynamo:"key_hash"`
}

func (dao *daoImpl) GetKeyClaim(slackUser string, claimTime time.Time) (*KeyClaim, error) {
	var kc KeyClaim
	err := dao.Get("slack_user", slackUser).Range("claim_time", dynamo.Equal, claimTime).One(&kc)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &kc, err
}

func (dao *daoImpl) CreateNew(keyClaim *KeyClaim) error {
	if keyClaim == nil {
		return errors.New("cannot create nil key claim")
	}

	return dao.Put(*keyClaim).If("attribute_not_exists(slack_user)").Run()
}

func (dao *daoImpl) CreateOrUpdate(keyClaim *KeyClaim) error {
	if keyClaim == nil {
		return errors.New("cannot create/update nil key claim")
	}

	return dao.Put(*keyClaim).Run()
}

func (dao *daoImpl) GetUsersLatestKeyClaim(slackUser string) (*KeyClaim, error) {
	var kcs []*KeyClaim
	err := dao.Get("slack_user", slackUser).
		Order(dynamo.Descending).
		All(&kcs)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	if len(kcs) == 0 {
		return nil, nil
	}
	return kcs[0], err
}
