package pr_settings

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

type PRSettings struct {
	ID                    string `dynamo:"slack_user_id"`
	NotificationsDisabled bool   `dynamo:"notifications_disabled"`
}

type DAO interface {
	GetSettings(slackUserID string) (*PRSettings, error)
	UpdateSettings(settings *PRSettings) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess)
	table := db.Table(fmt.Sprintf("pr-settings-%s", suffix))
	return &dao{
		Table:  &table,
		suffix: suffix,
	}
}

type dao struct {
	*dynamo.Table
	suffix string
}

func (d *dao) GetSettings(slackUserID string) (*PRSettings, error) {
	var prSettings PRSettings
	err := d.Get("slack_user_id", slackUserID).One(&prSettings)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &prSettings, err
}

func (d *dao) UpdateSettings(settings *PRSettings) error {
	return d.Put(settings).Run()
}
