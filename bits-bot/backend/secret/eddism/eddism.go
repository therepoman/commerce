package eddism

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/secret/models"
	"code.justin.tv/commerce/bits-bot/backend/secret/utils"
	"code.justin.tv/commerce/bits-bot/config"
	"code.justin.tv/commerce/gogogadget/random"
	go_redis "github.com/go-redis/redis"
	"github.com/mb-14/gomarkov"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

const (
	eddismPrefix             = "./eddism"
	redismEddismsListKey     = "eddisms-list"
	redismPrivilegedUsersKey = "eddism-privileged-users"
	redisExpiration          = time.Hour
)

const (
	SpecificEddism   = models.Action("SpecificEddism")
	AllEddisms       = models.Action("AllEddisms")
	RandomEddism     = models.Action("RandomEddism")
	RandomEddibotism = models.Action("RandomEddibotism")
	NewEddism        = models.Action("NewEddism")
	ShareEddibotism  = models.Action("ShareEddibotism")
	DeleteEddism     = models.Action("DeleteEddism")
)

var commands = map[string]models.Action{
	"h":     models.Help,
	"help":  models.Help,
	"u":     models.Help,
	"usage": models.Help,

	"id":    SpecificEddism,
	"index": SpecificEddism,
	"i":     SpecificEddism,

	"all": AllEddisms,
	"a":   AllEddisms,

	"r":             RandomEddism,
	"random":        RandomEddism,
	"eddism":        RandomEddism,
	"eddiewisdom":   RandomEddism,
	"eddiethoughts": RandomEddism,

	"b":          RandomEddibotism,
	"bot":        RandomEddibotism,
	"eddibotism": RandomEddibotism,

	"n":          NewEddism,
	"new":        NewEddism,
	"neweddism":  NewEddism,
	"new-eddism": NewEddism,
	"new_eddism": NewEddism,
	"add":        NewEddism,
	"addeddism":  NewEddism,
	"add-eddism": NewEddism,
	"add_eddism": NewEddism,

	"s":        ShareEddibotism,
	"sb":       ShareEddibotism,
	"share":    ShareEddibotism,
	"sharebot": ShareEddibotism,
	"save":     ShareEddibotism,
	"savebot":  ShareEddibotism,

	"d":             DeleteEddism,
	"delete":        DeleteEddism,
	"deleteeddism":  DeleteEddism,
	"delete-eddism": DeleteEddism,
}

const usageMessage = `Usage: ./eddism

Commands:
--help   (-h)       : Shows this screen
--id     (-i) <id>  : Displays a specific eddism indicated by <id>
--all    (-a)       : Displays all eddisms and their IDs
--random (-r)       : Displays a random eddism 
--new    (-n)       : Stores a new eddism
--bot    (-b)       : Generate an eddibotism
--share  (-s)       : Shares your previously generated eddibotism
--delete (-d) <id>  : Deletes a specific eddism indicated by <id>
`

var initialEddisms = []string{
	"If you want to be good at a game, you gotta be toxic",
	"I'm all for pay to win if _I'm_ winning",
	"By today I meant since November 17th",
	"I never even puked in an Uber or anything",
	"I only watch streamers with cameras because I want to be able to see their expressions when they lose",
	"I mean I will be tired but from what I understand there is a lot of just sitting around looking at the baby",
	"Rocket League is basically a competitive racing game. It's a car. It's racing.",
	"Raising a baby is harder then wexit.",
	"Mowing the lawn was the highlight of my day because the rest of the day is looking at baby",
	"Where's all the new brotanks on twitch merch? Been dying to pick those up :smile:",
	"1999 that is basically brand new",
	"6.7 is basically 10",
	"You can tell it's far in the future because he's wearing an armband with lights on it",
	"Pearl Jam is absolutely not butt-rock",
	"I didn’t realize inspector gadget was a fix up job",
	"How many days until I can eat Sauhard's cactus? It's been two days so far",
	"Chipotle is basically a 5 star restaurant",
	"Playing a console game is equivalent to me reading a book, it’s very hard to get me to turn it on and actually start playing something, when league exists, but once I get into it I `may` enjoy it",
	"My whole life is fires. Fire day, fire week, everything's fires",
	"Turbo Teen is always welcome",
	"I only play games for competition not fun",
	"Streaming to one viewer is not fun. If I’m not streaming to 30 it’s not worth it.",
	"Come on! I was playing a real game! not a 20 year old game!",
	"I'm so hyped right now. I can't even breathe",
	"I'm like a lambda. I gotta warm up",
	"Don't you play songs for points in Zelda?",
	"I assume there is music involved like the witness so that makes sense",
	"So TJs name is a baton. He is named after a stick",
	"It’s wild how much I agree with my comments",
	"Kindness is bm",
	"this is ben, he's the salty one.",
	"If you have enough eddisms, you'll practically have a bot.",
	"@kenobi",
	"It's social research",
	"My body just wigged out. I couldn’t even type",
	"I'm a bias for action kind of guy",
	"It had to be real because if it wasn't real, it wouldn't be comedy",
	"I'm a winner. I don't play with food.",
	"Could someone grab me a la croix water and hand it to me in stardew valley",
	"I'll cut the sleeves off and make a brotank out of it",
	"It's like Primer for me",
	"I just don’t want things that make me look bad in eddisms",
	"I didn't realize Super Mario Sunshine was so dark! With Mario getting arrested...",
	"It's like the Sistine Chapel of operational readiness",
	"I’m too old for these memes",
	"I like the early 2000s Austin powers memes you got going. The butt rock of memes",
}

var supportedChannels []string

var cliArgRegexp *regexp.Regexp

func (p *Processor) Init() {
	supportedChannels = []string{p.Config.ThePubbChannelID, p.Config.BitsBotTestChannelID}
	cliArgRegexp = regexp.MustCompile("(^| )-+[a-z0-9-_]+( [a-z0-9-+]+)?")
}

type Processor struct {
	Config      *config.Config    `inject:""`
	SlackClient *slack.Client     `inject:"slack_client_1"`
	RedisClient redis.Client      `inject:""`
	Utils       *utils.UtilHelper `inject:""`
}

func (p *Processor) FeatureName() string {
	return "Eddism"
}

func (p *Processor) ShouldProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error) {
	if msg == nil {
		return false, nil
	}

	/*if conversation.IsIM {
		//can this user use eddisms in DMs?
		ok, err := p.Utils.IsSuperUser(msg.User, redismPrivilegedUsersKey, p.Config.ThePubbChannelID, redisExpiration)
		if err != nil {
			return false, err
		}
		if !ok {
			return false, nil
		}
	} else */
	if !utils.InSupportedChannel(msg, supportedChannels) {
		return false, nil
	}

	msgTxt := msg.Text
	msgTxt = strings.TrimSpace(msgTxt)

	if strings.HasPrefix(msgTxt, eddismPrefix) {
		return true, nil
	}

	return false, nil
}

func (p *Processor) ProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) error {
	if msg == nil {
		return nil
	}

	// Double check, just to be safe
	shouldProcess, err := p.ShouldProcessMessage(msg, conversation)
	if err != nil {
		log.WithError(err).Errorf("error calling eddism ShouldProcessMessage")
		return nil
	}
	if !shouldProcess {
		log.Errorf("eddism ShouldProcessMessage - reached illegal case")
		return nil
	}

	text := strings.TrimSpace(msg.Text)
	text = strings.TrimPrefix(text, eddismPrefix)
	text = strings.TrimSpace(text)

	args := cliArgRegexp.FindAllString(text, -1)

	argMap := make(map[models.Action]string)
	for _, arg := range args {
		argMap[utils.GetActionFromArg(arg, commands)] = utils.GetArgValue(arg)
	}

	switch utils.GetActionFromArgs(args, commands) {
	case SpecificEddism:
		return p.handleSpecificEddism(msg, conversation, argMap)
	case AllEddisms:
		return p.handleAllEddisms(msg, conversation)
	case RandomEddism:
		return p.handleRandomEddism(msg, conversation)
	case RandomEddibotism:
		return p.handleRandomEddibotism(msg, conversation)
	case ShareEddibotism:
		return p.handleShareEddibotism(msg, conversation)
	case NewEddism:
		return p.handleNewEddism(msg, conversation, text)
	case DeleteEddism:
		return p.handleDeleteSpecificEddism(msg, conversation, argMap)
	case models.Help:
		return p.handleHelp(msg, conversation)
	case models.Invalid:
		return p.handleInvalid(msg, conversation, "unknown command")
	}

	return p.handleInvalid(msg, conversation, "unknown command")
}

func (p *Processor) handleSpecificEddism(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	idArgValue, ok := argMap[SpecificEddism]
	if !ok {
		return p.handleInvalid(msg, conversation, "id arg is not present")
	}

	id, err := strconv.Atoi(idArgValue)
	if err != nil {
		return p.handleInvalid(msg, conversation, "id arg is not an integer")
	}

	eddisms, err := p.GetAllEddisms()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	maxID := len(eddisms) - 1
	if id < 0 || id > maxID {
		return p.handleInvalid(msg, conversation, fmt.Sprintf("id arg should be between 0 and %d", maxID))
	}

	rawEddism := eddisms[id]
	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(eddify(rawEddism), nil)...)
	return err
}

func (p *Processor) handleDeleteSpecificEddism(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	idArgValue, ok := argMap[DeleteEddism]
	if !ok {
		return p.handleInvalid(msg, conversation, "id arg is not present")
	}

	id, err := strconv.Atoi(idArgValue)
	if err != nil {
		return p.handleInvalid(msg, conversation, "id arg is not an integer")
	}

	eddisms, err := p.GetAllEddisms()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	maxID := len(eddisms) - 1
	if id < 0 || id > maxID {
		return p.handleInvalid(msg, conversation, fmt.Sprintf("id arg should be between 0 and %d", maxID))
	}

	rawEddism := eddisms[id]

	err = p.RedisClient.LRem(redismEddismsListKey, 1, rawEddism).Err()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(":eddietilt:Deleted:eddietilt:\n"+eddify(rawEddism), nil)...)
	return err
}

func (p *Processor) handleAllEddisms(msg *slack.MessageEvent, conversation *slack.Channel) error {
	eddisms, err := p.GetAllEddisms()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(GetAllEddismsMessage(eddisms), nil)...)
	return err
}

func (p *Processor) handleRandomEddism(msg *slack.MessageEvent, conversation *slack.Channel) error {
	eddisms, err := p.GetAllEddisms()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getRandomEddism(eddisms), nil)...)
	return err
}

func (p *Processor) handleRandomEddibotism(msg *slack.MessageEvent, conversation *slack.Channel) error {
	eddisms, err := p.GetAllEddisms()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	rawEddibotism := GenerateEddibotism(eddisms)
	p.SaveLatestUserEddibotism(msg.User, rawEddibotism)

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(eddify(rawEddibotism), nil)...)
	return err
}

func (p *Processor) handleShareEddibotism(msg *slack.MessageEvent, conversation *slack.Channel) error {
	latestRawEddibotism, err := p.RedisClient.Get(fmt.Sprintf("latest-eddibotism-%s", msg.User))
	if err != nil {
		log.WithError(err).Errorf("error getting latest eddibotism from redis")
		return p.handleInternalError(msg, conversation, errors.New("error getting latest eddibotism from redis"))
	}

	if latestRawEddibotism == "" {
		return p.handleInternalError(msg, conversation, errors.New("latest eddibotism is blank"))
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(eddify(latestRawEddibotism), nil)...)
	return err
}

func (p *Processor) handleNewEddism(msg *slack.MessageEvent, conversation *slack.Channel, text string) error {
	text = strings.TrimSpace(text)
	matches := cliArgRegexp.FindAllString(text, 1)
	if len(matches) > 0 {
		text = strings.TrimPrefix(text, matches[0])
	}

	text = strings.TrimSpace(text)
	text = strings.TrimPrefix(text, ":eddiethinking:")
	text = strings.TrimSuffix(text, "-eddie")

	text = strings.TrimSpace(text)
	if text == "" {
		return p.handleInvalid(msg, conversation, "An eddism cannot be blank")
	}

	err := p.RedisClient.RPush(redismEddismsListKey, []string{text}).Err()
	if err != nil {
		return p.handleInternalError(msg, conversation, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(eddify(text), nil)...)
	return err
}

func (p *Processor) handleInvalid(msg *slack.MessageEvent, conversation *slack.Channel, errorMessage string) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getInvalidEddismMessage(errorMessage), nil)...)
	return err
}

func (p *Processor) handleHelp(msg *slack.MessageEvent, conversation *slack.Channel) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(), nil)...)
	return err
}

func (p *Processor) handleInternalError(msg *slack.MessageEvent, conversation *slack.Channel, inErr error) error {
	log.Errorf("internal error processing eddisms: %v", inErr)
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getInternalErrorMessage(), nil)...)
	return err
}

func GetAllEddismsMessage(eddisms []string) string {
	msg := ""
	for i, rawEddism := range eddisms {
		if i != 0 {
			msg += "\n"
		}
		eddism := eddify(rawEddism)
		msg += fmt.Sprintf("`%02d` :\n%s", i, eddism)
	}
	return msg
}

func getRandomEddism(eddisms []string) string {
	numEddisms := len(eddisms)
	eddismIdx := random.Int(0, numEddisms-1)
	rawEddism := eddisms[eddismIdx]
	return eddify(rawEddism)
}

func eddify(rawEddism string) string {
	return fmt.Sprintf("> :eddiethinking: %s -eddie", rawEddism)
}

func getInternalErrorMessage() string {
	return fmt.Sprintf("```%s```", "Internal Server Error")
}

func getUsageMessage() string {
	return fmt.Sprintf("```%s```", usageMessage)
}

func getInvalidEddismMessage(errorMessage string) string {
	return fmt.Sprintf("```Invalid eddism command: %s\n\n%s```", errorMessage, usageMessage)
}

func GenerateEddibotism(eddisms []string) string {
	order := 1
	chain := gomarkov.NewChain(order)

	for _, ed := range eddisms {
		chain.Add(strings.Split(ed, " "))
	}

	tokens := make([]string, 0)
	for i := 0; i < order; i++ {
		tokens = append(tokens, gomarkov.StartToken)
	}

	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := chain.Generate(tokens[(len(tokens) - order):])
		tokens = append(tokens, next)
	}

	botMsg := ""
	for _, token := range tokens {
		if token == gomarkov.StartToken {
			continue
		}
		if token == gomarkov.EndToken {
			break
		}
		if botMsg != "" {
			botMsg += " "
		}
		botMsg += token
	}

	return botMsg
}

func (p *Processor) prepopulateEddisms() error {
	for _, ed := range initialEddisms {
		err := p.RedisClient.RPush(redismEddismsListKey, []string{ed}).Err()

		if err != nil {
			return err
		}
	}
	return nil
}

func (p *Processor) GetAllEddisms() ([]string, error) {
	numEddisms, err := p.RedisClient.LLen(redismEddismsListKey).Result()
	if err != nil && err != go_redis.Nil {
		return []string{}, err
	} else if err == go_redis.Nil {
		numEddisms = 0
	}

	if numEddisms == 0 {
		err = p.prepopulateEddisms()
		if err != nil {
			return []string{}, err
		}

		numEddisms, err = p.RedisClient.LLen(redismEddismsListKey).Result()
		if err != nil {
			return []string{}, err
		}
	}

	return p.RedisClient.LRange(redismEddismsListKey, 0, numEddisms-1).Result()

}

func (p *Processor) SaveLatestUserEddibotism(slackUserID string, botMsg string) {
	err := p.RedisClient.Set(fmt.Sprintf("latest-eddibotism-%s", slackUserID), botMsg, time.Hour)
	if err != nil {
		log.WithError(err).Errorf("error saving latest eddibotism")
	}
}
