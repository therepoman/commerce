package word_guess

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/secret/models"
	"code.justin.tv/commerce/bits-bot/backend/secret/utils"
	"code.justin.tv/commerce/bits-bot/config"
	log "code.justin.tv/commerce/logrus"
	go_redis "github.com/go-redis/redis"
	"github.com/nlopes/slack"
)

type Dictionary string

const (
	Dictionary_Words = Dictionary("words")
	Dictionary_Names = Dictionary("names")
)

var dictionaries = map[string]Dictionary{
	"name":  Dictionary_Names,
	"names": Dictionary_Names,
	"word":  Dictionary_Words,
	"words": Dictionary_Words,
}

const (
	NewGoalWord = models.Action("NewGoalWord")
	StartGame   = models.Action("StartGame")
	MakeGuess   = models.Action("MakeGuess")
	Resign      = models.Action("Resign")
	CacheBust   = models.Action("CacheBust")
)

var commands = map[string]models.Action{
	"h":    models.Help,
	"help": models.Help,

	"n":   NewGoalWord,
	"new": NewGoalWord,
	"set": NewGoalWord,

	"s":     StartGame,
	"start": StartGame,

	"g":     MakeGuess,
	"guess": MakeGuess,

	"r":      Resign,
	"resign": Resign,

	"c":         CacheBust,
	"cachebust": CacheBust,
}

const (
	usageMessage = `Usage: ./words <command> or ./w <word> to guess.

Commands:
--help   (-h)                              : Shows this screen
--new    (-n) <word> <dictionary|optional> : Stores a new goal word (must be done in DM for secrecy)
--start  (-s)                              : Starts a new game (must be done in a channel)
--guess  (-g) <word>                       : Makes a guess about the goal word and updates how close you are to it. Can also be done as "./w <word>".
--resign (-r)                              : Ends the currently running game and reveals the word

Supported Dictionaries:
- words
- names
`
	noGoalWordStoredErrMessage              = "there is no goal word stored. Use `--new` in a DM to add one"
	inDMErrMessage                          = "you must %s from a channel, not from in DMs"
	notInDMErrMessage                       = "you must %s from DMs, not from in a channel"
	notInDictionaryErrMessage               = "you must provide a single word from the chosen dictionary, you provided: "
	notAllowedToSubmitAnotherWordErrMessage = "you have already submitted a word recently. You must wait until tomorrow to submit another word"
	notAllowedToUserCommandErrMessage       = "you are not allowed to use this command"
	unsupportedActionErrMessage             = "this action is not supported at this time. You tried: %s"
)

const (
	wordGuessPrefix                        = "./words"
	wordGuessShortPrefix                   = "./w"
	redisGoalWordQueueKey                  = "{word_guess}-goal-word-queue"
	redisClosestGuessBeforeGoalKey         = "{word_guess}-closest-guessed-before-goal"
	redisClosestGuessAfterGoalKey          = "{word_guess}-closest-guessed-after-goal"
	redisNumberOfGuessesKey                = "{word_guess}-number-of-guesses"
	redisWordGuessPrivilegedUsersKey       = "{word_guess}-word-guess-privileged-users"
	redisWordGuessTestUsersKey             = "{word_guess}-word-guess-test-users"
	redisPlayerHasSubmittedWordRecentlyKey = "{word_guess}-%s-submitted-word-already"
	redisExpiration                        = time.Hour * 24
)

var supportedChannels []string

var words map[string]bool
var names map[string]bool
var cliArgRegexp *regexp.Regexp

type WordsList struct {
	Words []string `json:"words"`
}

type WordQueue struct {
	GoalWords []UserWordPair `json:"goal_words"`
}

type UserWordPair struct {
	User       string     `json:"user"`
	Word       string     `json:"word"`
	Dictionary Dictionary `json:"dictionary"`
}

type Processor struct {
	Config      *config.Config    `inject:""`
	SlackClient *slack.Client     `inject:"slack_client_1"`
	RedisClient redis.Client      `inject:""`
	Utils       *utils.UtilHelper `inject:""`
}

func (p *Processor) Init() {
	p.initNames()
	p.initWords()

	cliArgRegexp = regexp.MustCompile("(^| )-+[a-z0-9-_]+( [a-z0-9-+]+)?")
	supportedChannels = []string{p.Config.GuessChannelID, p.Config.ThePubbChannelID, p.Config.BitsBotTestChannelID}
}

func (p *Processor) initNames() {
	file, err := os.Open("/etc/bits-bot/word_guess/names.txt")
	if err != nil {
		panic(err)
	}

	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing names file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	csvReader := csv.NewReader(bytes.NewReader(fileBytes))
	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	names = make(map[string]bool, len(rows))
	for _, row := range rows {
		if len(row) == 1 {
			names[strings.ToLower(row[0])] = true
		}
	}
}

func (p *Processor) initWords() {
	file, err := os.Open("/etc/bits-bot/word_guess/words.json")
	if err != nil {
		panic(err)
	}

	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing words file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	var wordsList WordsList
	err = json.Unmarshal(fileBytes, &wordsList)
	if err != nil {
		panic(err)
	}

	words = make(map[string]bool, len(wordsList.Words))
	for _, word := range wordsList.Words {
		words[word] = true
	}
}

func (p *Processor) FeatureName() string {
	return "WordGuess"
}

func (p *Processor) ShouldProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error) {
	if msg == nil {
		return false, nil
	}

	/*if conversation.IsIM {
		//Check to see if the user is allowed to DM bits-bot about this secret
		ok, err := p.Utils.IsSuperUser(msg.User, redisWordGuessPrivilegedUsersKey, p.Config.ThePubbChannelID, redisExpiration)
		if err != nil {
			return false, err
		}
		if !ok {
			return false, nil
		}
	} else */
	if !utils.InSupportedChannel(msg, supportedChannels) {
		return false, nil
	}

	msgTxt := msg.Text
	msgTxt = strings.TrimSpace(msgTxt)

	if strings.HasPrefix(msgTxt, wordGuessPrefix) ||
		strings.HasPrefix(msgTxt, wordGuessShortPrefix) {
		return true, nil
	}

	return false, nil
}

func (p *Processor) ProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) error {
	if msg == nil {
		return nil
	}

	// Double check, just to be safe
	shouldProcess, err := p.ShouldProcessMessage(msg, conversation)
	if err != nil {
		log.WithError(err).Errorf("error calling word_guess ShouldProcessMessage")
		return nil
	}

	if !shouldProcess {
		log.Errorf("word_guess ShouldProcessMessage - reached illegal case")
		return nil
	}

	text := strings.TrimSpace(msg.Text)

	// we know that it either has ./w or ./words so check for the full ./words case
	if strings.HasPrefix(text, wordGuessPrefix) {
		text = strings.TrimPrefix(text, wordGuessPrefix)
	} else {
		text = strings.TrimPrefix(text, wordGuessShortPrefix)
	}

	text = strings.TrimSpace(text)

	args := cliArgRegexp.FindAllString(text, -1)

	argMap := make(map[models.Action]string)
	for _, arg := range args {
		argMap[utils.GetActionFromArg(arg, commands)] = utils.GetArgValue(arg)
	}

	switch utils.GetActionFromArgs(args, commands) {
	case NewGoalWord:
		return p.handleNewWord(msg, conversation, text)
	case StartGame:
		return p.handleStartGame(msg, conversation)
	case MakeGuess:
		return p.handleMakeGuess(msg, conversation, text)
	case Resign:
		return p.handleResign(msg, conversation)
	case CacheBust:
		return p.handleCacheBust(msg, conversation, text)
	case models.Help:
		return p.handleHelp(msg)
	case models.Invalid:
		return p.handleInvalid(msg, "unknown command")
	default:
		//in the case where no args were present, default to the make guess arg
		text = "-g " + text
		return p.handleMakeGuess(msg, conversation, text)
	}
}

func (p *Processor) handleNewWord(msg *slack.MessageEvent, conversation *slack.Channel, text string) error {
	newWord, newDictionary := getWordAndDictionaryArgument(text)
	if newWord == "" {
		return p.handleInvalid(msg, "word arg is empty")
	}

	//See if the word was submitted via DM
	/*if !conversation.IsIM {
		return p.handleInvalid(msg, fmt.Sprintf(notInDMErrMessage, "store words"))
	}*/

	//See if the submitted word is a single word from this game's dictionary
	err := p.verifyWord(newWord, newDictionary)
	if err != nil {
		return p.handleInvalid(msg, notInDictionaryErrMessage+newWord)
	}

	//See if the user has already submitted a word recently
	//err = p.verifyFirstWordSubmittedRecently(msg.User)
	//if err != nil {
	//	return p.handleInvalid(msg, notAllowedToSubmitAnotherWordErrMessage)
	//}

	//Store the word
	queue, err := p.storeNewGoalWord(newWord, msg.User, newDictionary)
	if err != nil {
		return err
	}

	//Store that the user has submitted a word recently
	err = p.storePlayerHasSubmittedWordRecently(msg.User)
	if err != nil {
		return err
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getNewlySavedWordMessage(newWord, len(queue.GoalWords)), nil)...)
	return err
}

func (p *Processor) handleStartGame(msg *slack.MessageEvent, conversation *slack.Channel) error {
	//See if there's a game going yet
	userWordPair, err := p.getLatestGoalUserWordPair()
	if err != nil {
		return err
	} else if userWordPair == nil {
		return p.handleInvalid(msg, noGoalWordStoredErrMessage)
	}

	//See if the game was started via DM
	/*if conversation.IsIM {
		return p.handleInvalid(msg, fmt.Sprintf(inDMErrMessage, "start games"))
	}*/

	//Start the game with the goal word
	return p.startGame(msg, userWordPair.User, userWordPair.Dictionary)
}

func (p *Processor) handleMakeGuess(msg *slack.MessageEvent, conversation *slack.Channel, text string) error {
	guessWord := strings.ToLower(getWordArgument(text))
	if guessWord == "" {
		return p.handleInvalid(msg, "you need to guess with a word! No word was present")
	}

	//See if there's a game going yet
	userWordPair, err := p.getLatestGoalUserWordPair()
	if err != nil {
		return err
	} else if userWordPair == nil {
		return p.handleInvalid(msg, noGoalWordStoredErrMessage)
	}

	//See if the guess was made via DM
	/*if conversation.IsIM {
		return p.handleInvalid(msg, fmt.Sprintf(inDMErrMessage, "make guesses"))
	}*/

	//See if the guess was a single dictionary word
	err = p.verifyWord(guessWord, userWordPair.Dictionary)
	if err != nil {
		return p.handleInvalid(msg, notInDictionaryErrMessage+guessWord)
	}

	//Update the state of the game
	err = p.updateGame(msg, guessWord, userWordPair.Word)
	if err != nil {
		log.WithError(err).Error("could not process guess for guessed word")
		return err
	}

	return nil
}

func (p *Processor) handleResign(msg *slack.MessageEvent, conversation *slack.Channel) error {
	//See if there's a game going yet
	userWordPair, err := p.getLatestGoalUserWordPair()
	if err != nil {
		return err
	} else if userWordPair == nil {
		return p.handleInvalid(msg, noGoalWordStoredErrMessage)
	}

	//See if the game was resigned via DM
	/*if conversation.IsIM {
		return p.handleInvalid(msg, fmt.Sprintf(inDMErrMessage, "resign games"))
	}*/

	//Get the number of guesses
	numGuesses := 0
	numGuessesVal, err := p.getNumberOfGuesses()
	if err != nil {
		return err
	} else if numGuessesVal != "" {
		numGuesses, err = strconv.Atoi(numGuessesVal)
		if err != nil {
			return err
		}
	}

	//Send the game over message
	_, _, err = p.SlackClient.PostMessage(msg.Channel, formatEndGameStatus(false, userWordPair.Word, numGuesses, msg.Msg)...)
	if err != nil {
		return err
	}

	err = p.resetGame()
	return err
}

func (p *Processor) handleCacheBust(msg *slack.MessageEvent, conversation *slack.Channel, text string) error {
	redisKey := getWordArgument(text)
	if redisKey == "" {
		redisKey = redisPlayerHasSubmittedWordRecentlyKey
	}

	//See if the word was submitted via DM
	/*if !conversation.IsIM {
		return p.handleInvalid(msg, fmt.Sprintf(notInDMErrMessage, "bust caches"))
	}*/

	//See if the user is allowed to bust the cache
	isAllowedToBustCache, err := p.Utils.IsSuperUser(msg.User, redisWordGuessTestUsersKey, p.Config.BitsBotTestChannelID, redisExpiration)
	if err != nil {
		return err
	}
	if !isAllowedToBustCache {
		return p.handleInvalid(msg, notAllowedToUserCommandErrMessage)
	}

	//Bust the corresponding cache
	switch redisKey {
	case redisPlayerHasSubmittedWordRecentlyKey:
		//Only delete the "recently-submitted" cache for this user
		_, err := p.RedisClient.Del([]string{fmt.Sprintf(redisKey, msg.User)})
		if err != nil {
			log.WithError(err).WithField("redisKey", redisKey).Error("error deleting 'recently submitted' key from redis")
			return err
		}
	case redisGoalWordQueueKey:
		//Only delete the submitted word(s) from this user
		wordQueue, err := p.getGoalWordQueue()
		if err != nil {
			return err
		}

		//If there is no array or the array is empty, short circuit
		if wordQueue.GoalWords == nil || len(wordQueue.GoalWords) == 0 {
			return nil
		}

		//Filter all words belonging to this user and store the rest
		var filteredGoalWords = make([]UserWordPair, 0, len(wordQueue.GoalWords))
		for _, userWordPair := range wordQueue.GoalWords {
			if userWordPair.User != msg.User {
				filteredGoalWords = append(filteredGoalWords, userWordPair)
			}
		}
		wordQueue.GoalWords = filteredGoalWords

		wordQueue, err = p.storeGoalWordQueue(wordQueue)
		if err != nil {
			return err
		}
	case redisClosestGuessBeforeGoalKey:
		fallthrough
	case redisClosestGuessAfterGoalKey:
		fallthrough
	case redisNumberOfGuessesKey:
		_, err := p.RedisClient.Del([]string{redisKey})
		if err != nil {
			log.WithError(err).WithField("redisKey", redisKey).Error("could not delete a singular item from redis")
			return err
		}
	default:
		return p.handleInvalid(msg, fmt.Sprintf(unsupportedActionErrMessage, "cache bust with "+redisKey))
	}

	return nil
}

func (p *Processor) handleHelp(msg *slack.MessageEvent) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(), nil)...)
	return err
}

func (p *Processor) handleInvalid(msg *slack.MessageEvent, errorMessage string) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getInvalidWordsGuessMessage(errorMessage), &msg.Msg)...)
	return err
}

func getWordMap(dictionary Dictionary) map[string]bool {
	switch dictionary {
	case Dictionary_Names:
		return names
	case Dictionary_Words:
		return words
	}
	return words
}

func (p *Processor) verifyWord(wordArg string, dictionary Dictionary) error {
	//See if the word was a single word and not a phrase
	submittedWordList := strings.Split(wordArg, " ")
	if len(submittedWordList) > 1 || submittedWordList[0] != wordArg {
		return errors.New("must submit a single word, not a phrase")
	}

	//There should only be one word. Get that word out of the list
	submittedWord := submittedWordList[0]

	//See if the word is a dictionary word
	_, isDictionaryWord := getWordMap(dictionary)[strings.ToLower(submittedWord)]
	if !isDictionaryWord {
		return errors.New("must submit word in this game's dictionary")
	}

	return nil
}

func (p *Processor) verifyFirstWordSubmittedRecently(player string) error {
	hasSubmitted, err := p.getHasPlayerAlreadySubmittedWordRecently(player)
	if err != nil {
		return err
	}

	if hasSubmitted {
		return errors.New("you have already submitted a word recently. Please try again tomorrow")
	}

	return nil
}

func (p *Processor) getLatestGoalUserWordPair() (*UserWordPair, error) {
	wordQueue, err := p.getGoalWordQueue()
	if err != nil {
		return nil, err
	}

	if wordQueue.GoalWords == nil || len(wordQueue.GoalWords) == 0 {
		return nil, nil
	}

	userWordPair := wordQueue.GoalWords[0]
	return &userWordPair, nil
}

func (p *Processor) getGoalWordQueue() (WordQueue, error) {
	wordQueueJSON, err := p.RedisClient.Get(redisGoalWordQueueKey)
	if err != nil && err != go_redis.Nil {
		log.WithError(err).Error("error getting latest word from redis")
		return WordQueue{}, err
	}

	var wordQueue WordQueue
	if wordQueueJSON != "" {
		err = json.Unmarshal([]byte(wordQueueJSON), &wordQueue)
		if err != nil {
			log.WithError(err).Error("error unmarshalling word queue")
			return wordQueue, err
		}
	}

	return wordQueue, nil
}

func (p *Processor) storeNewGoalWord(word string, user string, dictionary Dictionary) (WordQueue, error) {
	wordQueue, err := p.getGoalWordQueue()
	if err != nil {
		return WordQueue{}, err
	}

	userWordPair := UserWordPair{
		User:       user,
		Word:       strings.ToLower(word),
		Dictionary: dictionary,
	}

	wordQueue.GoalWords = append(wordQueue.GoalWords, userWordPair)
	return p.storeGoalWordQueue(wordQueue)
}

func (p *Processor) dequeuePreviousGoal() error {
	wordQueue, err := p.getGoalWordQueue()
	if err != nil {
		return err
	}

	if len(wordQueue.GoalWords) > 0 {
		wordQueue.GoalWords = wordQueue.GoalWords[1:]
		_, err = p.storeGoalWordQueue(wordQueue)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *Processor) storeGoalWordQueue(wordQueue WordQueue) (WordQueue, error) {
	wordQueueJSON, err := json.Marshal(wordQueue)

	err = p.RedisClient.Set(redisGoalWordQueueKey, string(wordQueueJSON), redisExpiration)
	if err != nil {
		log.WithError(err).Errorf("error setting latest word in redis")
		return WordQueue{}, err
	}

	return wordQueue, nil
}

func (p *Processor) getNumberOfGuesses() (string, error) {
	numGuessesVal, err := p.RedisClient.Get(redisNumberOfGuessesKey)
	if err != nil && err != go_redis.Nil {
		log.WithError(err).Error("error getting number of guesses from redis")
		return "", err
	}

	return numGuessesVal, nil
}

func (p *Processor) storeNumberOfGuesses(numGuesses int) error {
	err := p.RedisClient.Set(redisNumberOfGuessesKey, numGuesses, redisExpiration)
	if err != nil {
		log.WithError(err).Error("error storing number of guesses in redis")
		return err
	}

	return nil
}

func (p *Processor) getClosestGuessBeforeGoal() (string, error) {
	closestBeforeGoal, err := p.RedisClient.Get(redisClosestGuessBeforeGoalKey)
	if err != nil && err != go_redis.Nil {
		log.WithError(err).Error("error getting closest guess before goal from redis")
		return "", err
	}

	return closestBeforeGoal, nil
}

func (p *Processor) storeClosestGuessBeforeGoal(closestGuess string) error {
	err := p.RedisClient.Set(redisClosestGuessBeforeGoalKey, closestGuess, redisExpiration)
	if err != nil {
		log.WithError(err).Error("error storing closest guess before goal in redis")
		return err
	}

	return nil
}

func (p *Processor) getClosestGuessAfterGoal() (string, error) {
	closestBeforeGoal, err := p.RedisClient.Get(redisClosestGuessAfterGoalKey)
	if err != nil && err != go_redis.Nil {
		log.WithError(err).Error("error getting closest guess after goal from redis")
		return "", err
	}

	return closestBeforeGoal, nil
}

func (p *Processor) storeClosestGuessAfterGoal(closestGuess string) error {
	err := p.RedisClient.Set(redisClosestGuessAfterGoalKey, closestGuess, redisExpiration)
	if err != nil {
		log.WithError(err).Error("error storing closest guess after goal in redis")
		return err
	}

	return nil
}

func (p *Processor) getHasPlayerAlreadySubmittedWordRecently(player string) (bool, error) {
	hasSubmittedStr, err := p.RedisClient.Get(fmt.Sprintf(redisPlayerHasSubmittedWordRecentlyKey, player))
	if err != nil && err != go_redis.Nil {
		log.WithError(err).WithField("player", player).Error("error getting whether player has already submitted word recently")
		return false, err
	}

	if hasSubmittedStr == "" {
		return false, nil
	}

	hasSubmitted, err := strconv.ParseBool(hasSubmittedStr)
	if err != nil {
		return false, err
	}

	return hasSubmitted, nil
}

func (p *Processor) storePlayerHasSubmittedWordRecently(player string) error {
	//half the redis expiration so that a late afternoon submission one day can turn into an early morning one the next
	err := p.RedisClient.Set(fmt.Sprintf(redisPlayerHasSubmittedWordRecentlyKey, player), true, redisExpiration/2)
	if err != nil {
		log.WithError(err).WithField("player", player).Error("error storing player has already submitted word recently")
		return err
	}

	return nil
}

func (p *Processor) startGame(msg *slack.MessageEvent, wordOwner string, dictionary Dictionary) error {
	newGameStringPrefix := fmt.Sprintf("New game started with %s's word using the %s dictionary! Guess a word with `./words -g something`", p.Utils.GetSlackUserName(wordOwner), dictionary)
	newGameString := fmt.Sprintf("%s\n%s", newGameStringPrefix, printCurrentGameStatus("", "", 0))
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(newGameString, nil)...)
	return err
}

func (p *Processor) updateGame(msg *slack.MessageEvent, guessedWord string, word string) error {
	numGuessesVal, err := p.getNumberOfGuesses()
	if err != nil {
		return err
	}

	numGuesses := 0
	if numGuessesVal != "" {
		numGuesses, err = strconv.Atoi(numGuessesVal)
		if err != nil {
			return err
		}
	}

	numGuesses++
	err = p.storeNumberOfGuesses(numGuesses)
	if err != nil {
		return err
	}

	var updatedGameMsgOptions []slack.MsgOption
	var closestBeforeGoal = ""
	var closestAfterGoal = ""
	var isGameOver bool
	if guessedWord == word {
		updatedGameMsgOptions = formatEndGameStatus(true, word, numGuesses, msg.Msg)
		isGameOver = true
	} else {
		//Always get the previously closest before and after the goal
		closestBeforeGoal, err = p.getClosestGuessBeforeGoal()
		if err != nil {
			return err
		}

		closestAfterGoal, err = p.getClosestGuessAfterGoal()
		if err != nil {
			return err
		}

		if guessedWord < word {
			if closestBeforeGoal == "" {
				closestBeforeGoal = guessedWord

				err := p.storeClosestGuessBeforeGoal(closestBeforeGoal)
				if err != nil {
					return err
				}
			} else if closestBeforeGoal < guessedWord {
				// If the word comes alphabetically after the closest word that is before the goal, then the following
				// has happened: "closestBeforeGoal < guessedWord < goalWord < closestAfterGoal" and we need to set the
				// closestBeforeGoal to be the new guessedWord

				closestBeforeGoal = guessedWord

				err := p.storeClosestGuessBeforeGoal(closestBeforeGoal)
				if err != nil {
					return err
				}
			}
		} else {
			// guessedWord > word
			if closestAfterGoal == "" {
				closestAfterGoal = guessedWord

				err := p.storeClosestGuessAfterGoal(closestAfterGoal)
				if err != nil {
					return err
				}
			} else if closestAfterGoal > guessedWord {
				// If the word comes alphabetically after the closest word that is before the goal, then the following
				// has happened: "closestBeforeGoal < guessedWord < goalWord < closestAfterGoal" and we need to set the
				// closestAfterGoal to be the new guessedWord

				closestAfterGoal = guessedWord

				err := p.storeClosestGuessAfterGoal(closestAfterGoal)
				if err != nil {
					return err
				}
			}
		}

		updatedGameMsgOptions = utils.ToSlackMsgOption(printCurrentGameStatus(closestBeforeGoal, closestAfterGoal, numGuesses), &msg.Msg)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, updatedGameMsgOptions...)
	if err != nil {
		return err
	}

	if isGameOver {
		err = p.resetGame()
		return err
	}

	return nil
}

func (p *Processor) resetGame() error {
	_, err := p.RedisClient.Del([]string{redisNumberOfGuessesKey, redisClosestGuessBeforeGoalKey, redisClosestGuessAfterGoalKey})
	if err != nil {
		log.WithError(err).Error("could not delete items from Redis")
		return err
	}

	err = p.dequeuePreviousGoal()
	if err != nil {
		log.WithError(err).Error("error dequeueing previous goal from Redis queue")
		return err
	}

	return nil
}

func getWordArgument(text string) string {
	//we expect text to be something like "--new foo"
	inputs := strings.Split(text, " ")

	if len(inputs) <= 1 {
		return ""
	}

	//in the case where someone tries to input a phrase, we'll use the first word of that phrase
	word := inputs[1]

	return word
}

func getWordAndDictionaryArgument(text string) (string, Dictionary) {
	//we expect text to be something like "--new foo"
	inputs := strings.Split(text, " ")

	if len(inputs) <= 1 {
		return "", Dictionary_Words
	}

	//in the case where someone tries to input a phrase, we'll use the first word of that phrase
	word := inputs[1]

	dictionary := Dictionary_Words
	if len(inputs) > 2 {
		chosenDictionary, ok := dictionaries[strings.ToLower(inputs[2])]
		if ok {
			dictionary = chosenDictionary
		}
	}

	return strings.ToLower(word), dictionary
}

func getInvalidWordsGuessMessage(errorMessage string) string {
	if strings.Contains(errorMessage, notInDictionaryErrMessage) {
		return fmt.Sprintf("```Invalid words_guess command: %s\n```", errorMessage)
	} else {
		return fmt.Sprintf("```Invalid words_guess command: %s\n\n%s```", errorMessage, usageMessage)
	}
}

func getUsageMessage() string {
	return fmt.Sprintf("```%s```", usageMessage)
}

func getNewlySavedWordMessage(newWord string, queueLength int) string {
	return fmt.Sprintf("You have successfully stored `%s` as the goal word. There are currently %d words ahead of you. Use `--start` in #guess to start a new game.", newWord, queueLength-1)
}

func printCurrentGameStatus(closestBeforeGuess, closestAfterGuess string, numGuesses int) string {
	switch {
	case len(closestBeforeGuess) == 0 && len(closestAfterGuess) == 0:
		return fmt.Sprintf("There have been `%d` guesses so far.", numGuesses)
	case len(closestBeforeGuess) == 0:
		return fmt.Sprintf("There have been `%d` guesses so far. The goal word is before `%s` ", numGuesses, closestAfterGuess)
	case len(closestAfterGuess) == 0:
		return fmt.Sprintf("There have been `%d` guesses so far. The goal word is after `%s` ", numGuesses, closestBeforeGuess)
	default:
		return fmt.Sprintf("There have been `%d` guesses so far. The goal word is between `%s` and `%s` ", numGuesses, closestBeforeGuess, closestAfterGuess)
	}
}

func formatEndGameStatus(didWin bool, word string, numGuesses int, originalSlackMessage slack.Msg) []slack.MsgOption {
	if didWin {
		return utils.ToSlackMsgOption(fmt.Sprintf("Congratulations! The word was `%s`! You got it in `%d` guesses", word, numGuesses), &originalSlackMessage)
	}

	return utils.ToSlackMsgOption(fmt.Sprintf("Game over. The word was `%s`. You resigned after `%d` guesses", word, numGuesses), &originalSlackMessage)
}
