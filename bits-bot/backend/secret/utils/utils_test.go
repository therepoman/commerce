package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func testGetArgKeyAndGetArgValueCase(input string, expectedKey string, expectedValue string) {
	So(getArgKey(input), ShouldEqual, expectedKey)
	So(GetArgValue(input), ShouldEqual, expectedValue)
}

func TestGetArgKeyAndGetArgValue(t *testing.T) {
	Convey("Test various case", t, func() {
		testGetArgKeyAndGetArgValueCase("", "", "")
		testGetArgKeyAndGetArgValueCase(" ", "", "")
		testGetArgKeyAndGetArgValueCase("      ", "", "")
		testGetArgKeyAndGetArgValueCase("-", "", "")
		testGetArgKeyAndGetArgValueCase("- ", "", "")
		testGetArgKeyAndGetArgValueCase("-    ", "", "")
		testGetArgKeyAndGetArgValueCase("    -    ", "", "")
		testGetArgKeyAndGetArgValueCase("    -a    ", "a", "")
		testGetArgKeyAndGetArgValueCase("    -a b c d    ", "a", "b c d")
		testGetArgKeyAndGetArgValueCase("-a b", "a", "b")
		testGetArgKeyAndGetArgValueCase("--abc 123", "abc", "123")
		testGetArgKeyAndGetArgValueCase("   --abc 123   ", "abc", "123")
		testGetArgKeyAndGetArgValueCase("   --hello_WORLD WORLD_hello   ", "hello_world", "WORLD_hello")
		testGetArgKeyAndGetArgValueCase("   --no_value", "no_value", "")
	})
}
