package utils

import (
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/secret/models"
	log "code.justin.tv/commerce/logrus"
	go_redis "github.com/go-redis/redis"
	"github.com/nlopes/slack"
)

const (
	redisUserNameKey = "$username$-%s"
	redisUserNameTTL = 1 * time.Hour
)

type UtilHelper struct {
	SlackClient *slack.Client `inject:"slack_client_1"`
	RedisClient redis.Client  `inject:""`
}

func (u *UtilHelper) IsSuperUser(userID string, allowedUsersRedisKey string, channelToSourceUsersFrom string, redisExpiry time.Duration) (bool, error) {
	superUsers, err := u.getSecretPrivilegedUsers(allowedUsersRedisKey, channelToSourceUsersFrom, redisExpiry)
	if err != nil {
		return false, err
	}

	_, isSuperUser := superUsers[userID]
	return isSuperUser, nil
}

func (u *UtilHelper) GetSlackUserName(userID string) string {
	key := fmt.Sprintf(redisUserNameKey, userID)

	name, err := u.RedisClient.Get(key)

	if err == go_redis.Nil {
		user, err := u.SlackClient.GetUserInfo(userID)
		if err != nil {
			log.WithField("userID", userID).WithError(err).Error("error getting user from slack client")
		}

		if user == nil {
			name = "UNKNOWN"
			log.WithField("userID", userID).Info("nil user returned from slack client")
		} else if user.Profile.DisplayName != "" {
			name = user.Profile.DisplayName
		} else {
			name = user.Name
		}

		err = u.RedisClient.Set(key, name, redisUserNameTTL)
		if err != nil {
			log.WithField("userID", userID).WithError(err).Error("error setting user name cache")
		}
	} else if err != nil {
		log.WithField("userID", userID).WithError(err).Error("error getting user name from cache")
	}

	return name
}

func GetSlackChannelName(channelID string) string {
	return fmt.Sprintf("<#%s>", channelID)
}

func (u *UtilHelper) getSecretPrivilegedUsers(allowedUsersRedisKey string, channelToSourceUsersFrom string, redisExpiry time.Duration) (map[string]interface{}, error) {
	var userList []string

	users, err := u.RedisClient.Get(allowedUsersRedisKey)
	if err == go_redis.Nil {
		channelUsers, _, err := u.SlackClient.GetUsersInConversation(&slack.GetUsersInConversationParameters{
			ChannelID: channelToSourceUsersFrom,
		})
		if err != nil {
			return nil, err
		}

		err = u.RedisClient.Set(allowedUsersRedisKey, strings.Join(channelUsers, ","), redisExpiry)
		if err != nil {
			return nil, err
		}

		userList = channelUsers
	} else if err != nil {
		return nil, err
	} else {
		userList = strings.Split(users, ",")
	}

	userMap := make(map[string]interface{})
	for _, user := range userList {
		userMap[user] = nil
	}
	return userMap, nil
}

func InSupportedChannel(msg *slack.MessageEvent, supportedChannels []string) bool {
	if msg == nil {
		return false
	}

	for _, supportedChannel := range supportedChannels {
		if supportedChannel == msg.Channel {
			return true
		}
	}

	return false
}

func GetActionFromArgs(args []string, commands map[string]models.Action) models.Action {
	if len(args) < 1 {
		return ""
	}
	return GetActionFromArg(args[0], commands)
}

func GetActionFromArg(arg string, commands map[string]models.Action) models.Action {
	action, actionFound := commands[getArgKey(arg)]
	if !actionFound {
		return models.Invalid
	}

	return action
}

func ToSlackMsgOption(msg string, originalSlackMessage *slack.Msg) []slack.MsgOption {
	params := slack.NewPostMessageParameters()
	params.LinkNames = 1
	if originalSlackMessage != nil {
		if originalSlackMessage.ThreadTimestamp == "" {
			// start a thread
			params.ThreadTimestamp = originalSlackMessage.Timestamp
		} else {
			// reply in an existing thread
			params.ThreadTimestamp = originalSlackMessage.ThreadTimestamp
		}
	}
	return []slack.MsgOption{slack.MsgOptionPostMessageParameters(params), slack.MsgOptionText(msg, false)}
}

func getArgKey(arg string) string {
	arg = strings.ToLower(sanitizeArg(arg))
	spaceIdx := strings.Index(arg, " ")
	if spaceIdx == -1 {
		return arg
	}
	return arg[:spaceIdx]
}

func GetArgValue(arg string) string {
	arg = sanitizeArg(arg)
	spaceIdx := strings.Index(arg, " ")
	if spaceIdx == -1 {
		return ""
	}
	return arg[strings.Index(arg, " ")+1:]
}

// Useful for parsing tail-style args (e.g. --someCommand arg1 arg2 arg3)
//
// input: "a   10  true"
// output: ["a", "10", "true"]
func SplitTailArgs(arg string) []string {
	parsedArgs := strings.Split(arg, " ")
	var trimmedArgs []string
	for _, arg := range parsedArgs {
		trimmedArgs = append(trimmedArgs, strings.TrimSpace(arg))
	}

	return trimmedArgs
}

func sanitizeArg(arg string) string {
	return strings.TrimSpace(strings.Trim(strings.TrimSpace(arg), "-"))
}
