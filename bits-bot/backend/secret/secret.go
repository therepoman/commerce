package secret

import (
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

type Processor interface {
	Init()
	FeatureName() string
	ShouldProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error)
	ProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) error
}

type FeaturesManager interface {
	ProcessMessageForSecretFeatures(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error)
}

type FeaturesManagerImpl struct {
	Processors []Processor `inject:"secret_features"`
}

func (fm *FeaturesManagerImpl) ProcessMessageForSecretFeatures(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error) {
	for _, p := range fm.Processors {
		featureName := p.FeatureName()
		should, err := p.ShouldProcessMessage(msg, conversation)
		if err != nil {
			log.WithError(err).WithField("feature_name", featureName).Error("Error checking if should process message for secret feature")
			return false, err
		}

		if should {
			err = p.ProcessMessage(msg, conversation)
			if err != nil {
				log.WithError(err).WithField("feature_name", featureName).Error("Error processing message for secret feature")
			}
			return true, err
		}
	}
	return false, nil
}
