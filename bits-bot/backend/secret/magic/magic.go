package magic

import (
	"context"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"code.justin.tv/commerce/bits-bot/config"
	"github.com/BlueMonday/go-scryfall"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

type Processor struct {
	Config      *config.Config `inject:""`
	SlackClient *slack.Client  `inject:"slack_client_1"`
}

const scryfallPrefix = "./scryfall"
const scryfallFullListPrefix = "./scryfallFullList"

var regex = regexp.MustCompile(`\[\[.*?\]\]`)

func (p *Processor) Init() {
	// no-op
}

func (p *Processor) FeatureName() string {
	return "Scryfall"
}

func (p *Processor) ShouldProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error) {
	if msg == nil {
		return false, nil
	}

	if !p.inSupportedChannel(msg, conversation) {
		return false, nil
	}

	msgTxt := msg.Text
	msgTxt = strings.TrimSpace(msgTxt)

	if strings.HasPrefix(msgTxt, scryfallPrefix) {
		return true, nil
	} else if regex.MatchString(msgTxt) && (msg.Channel != p.Config.ThePubbChannelID) {
		return true, nil
	}

	return false, nil
}

func (p *Processor) inSupportedChannel(msg *slack.MessageEvent, conversation *slack.Channel) bool {
	if msg == nil {
		return false
	}

	supportedChannels := map[string]bool{
		p.Config.MagicChannelID:        true,
		p.Config.BitsBotTestChannelID:  true,
		p.Config.ThePubbChannelID:      true,
		p.Config.SeattleMagicChannelID: true,
	}

	supported, ok := supportedChannels[msg.Channel]
	if !ok {
		return false
	}
	return supported
}

func (p *Processor) ProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) error {
	if msg == nil {
		return nil
	}

	// Double check, just to be safe
	shouldProcess, err := p.ShouldProcessMessage(msg, conversation)
	if err != nil {
		log.WithError(err).Errorf("error calling magic ShouldProcessMessage")
		return nil
	}
	if !shouldProcess {
		log.Errorf("magic ShouldProcessMessage - reached illegal case")
		return nil
	}

	fullList := strings.Contains(msg.Text, scryfallFullListPrefix)

	if fullList && msg.ThreadTimestamp == "" {
		params := slack.NewPostMessageParameters()
		params.LinkNames = 1
		if msg.Timestamp != msg.ThreadTimestamp {
			// respond in thread
			params.ThreadTimestamp = msg.ThreadTimestamp
		}
		msgOption := []slack.MsgOption{slack.MsgOptionPostMessageParameters(params), slack.MsgOptionText("Full list mode available in threads only.", false)}
		_, _, err = p.SlackClient.PostMessage(msg.Channel, msgOption...)
		return err
	}

	var textToSearch []string

	bracketMatches := findBracketStyleMatches(msg.Text)

	if len(bracketMatches) > 0 {
		textToSearch = bracketMatches
	} else {
		text := strings.TrimSpace(msg.Text)
		if fullList {
			text = strings.TrimPrefix(text, scryfallFullListPrefix)
		} else {
			text = strings.TrimPrefix(text, scryfallPrefix)
		}
		text = strings.TrimSpace(text)
		textToSearch = append(textToSearch, text)
	}

	for _, text := range textToSearch {
		ctx := context.Background()
		client, err := scryfall.NewClient()
		if err != nil {
			log.WithError(err).Error("Error instantiating scryfall client")
			return err
		}

		sco := scryfall.SearchCardsOptions{
			Unique:        scryfall.UniqueModeCards,
			Order:         scryfall.OrderSet,
			Dir:           scryfall.DirDesc,
			IncludeExtras: true,
		}
		result, err := client.SearchCards(ctx, text, sco)

		if err != nil || len(result.Cards) == 0 {
			params := slack.NewPostMessageParameters()
			params.LinkNames = 1
			if msg.Timestamp != msg.ThreadTimestamp {
				// respond in thread
				params.ThreadTimestamp = msg.ThreadTimestamp
			}
			msgOption := []slack.MsgOption{slack.MsgOptionPostMessageParameters(params), slack.MsgOptionText("No results found for: "+text, false)}
			_, _, err = p.SlackClient.PostMessage(msg.Channel, msgOption...)
			if err != nil {
				log.WithError(err).Errorf("error calling scryfall ShouldProcessMessage")
				return err
			}
			return nil
		}

		cardIndex := 0
		if len(result.Cards) > 1 {
			if err != nil {
				log.WithError(err).Errorf("error calling scryfall ShouldProcessMessage")
				return err
			}
			cardNames := make(map[string]scryfall.Card)
			exactMatchFound := false
			for i := 0; i < len(result.Cards); i++ {
				// search for a complete text match between input text and card, ignoring case
				if strings.ToUpper(result.Cards[i].Name) == strings.ToUpper(strings.TrimSpace(text)) {
					cardIndex = i
					exactMatchFound = true
					break
				}
				cardNames[result.Cards[i].Name] = result.Cards[i]
			}
			if !exactMatchFound {
				var cardList string
				cardListCounter := 0
				if len(cardNames) >= 2 {
					for k, _ := range cardNames {
						if !fullList && cardListCounter >= 5 {
							continue
						}
						cardList = cardList + "\n" + k
						cardListCounter = cardListCounter + 1
					}

					params := slack.NewPostMessageParameters()
					params.LinkNames = 1
					if msg.Timestamp != msg.ThreadTimestamp {
						// respond in thread
						params.ThreadTimestamp = msg.ThreadTimestamp
					}

					msgOption := []slack.MsgOption{slack.MsgOptionPostMessageParameters(params), slack.MsgOptionText("Multiple ("+strconv.Itoa(len(cardNames))+") results found for: "+text+cardList, false)}

					_, _, err = p.SlackClient.PostMessage(msg.Channel, msgOption...)
					if err != nil {
						log.WithError(err).Errorf("error calling scryfall post message multiple cards found")
						return err
					} else {
						return nil
					}
				}
			}
		}

		if len(result.Cards[cardIndex].CardFaces) > 0 {
			for i := 0; i < len(result.Cards[cardIndex].CardFaces); i++ {
				attachment := slack.Attachment{
					ThumbURL:   result.Cards[cardIndex].CardFaces[i].ImageURIs.Normal,
					Title:      fmt.Sprintf("%s %s", result.Cards[cardIndex].CardFaces[i].Name, convertManaCostToSlack(result.Cards[cardIndex].CardFaces[i].ManaCost)),
					TitleLink:  result.Cards[cardIndex].ScryfallURI,
					MarkdownIn: []string{"text"},
					Text:       buildCardString(cardFaceToCard(result.Cards[cardIndex].CardFaces[i])),
				}

				err = p.postToSlack(msg, &attachment)

				if err != nil {
					log.WithError(err).Errorf("error calling scryfall post slack message for multi faced card")
					return err
				}
			}
		} else {
			attachment := slack.Attachment{
				ThumbURL:   result.Cards[cardIndex].ImageURIs.Normal,
				Title:      fmt.Sprintf("%s %s", result.Cards[cardIndex].Name, convertManaCostToSlack(result.Cards[cardIndex].ManaCost)),
				TitleLink:  result.Cards[cardIndex].ScryfallURI,
				MarkdownIn: []string{"text"},
				Text:       buildCardString(cardtoCard(result.Cards[cardIndex])),
			}

			err = p.postToSlack(msg, &attachment)

			if err != nil {
				log.WithError(err).Errorf("error calling scryfall post slack message for single faced card")
				return err
			}
		}
	}
	return nil

}

type card struct {
	TypeLine   string
	OracleText string
	Toughness  *string
	Power      *string
	Loyalty    *string
	FlavorText *string
}

func cardtoCard(c scryfall.Card) card {
	return card{
		TypeLine:   c.TypeLine,
		OracleText: c.OracleText,
		Toughness:  c.Toughness,
		Power:      c.Power,
		Loyalty:    c.Loyalty,
		FlavorText: c.FlavorText,
	}
}
func cardFaceToCard(c scryfall.CardFace) card {
	var oracleText string
	if c.OracleText != nil {
		oracleText = *c.OracleText
	}

	return card{
		TypeLine:   c.TypeLine,
		OracleText: oracleText,
		Toughness:  c.Toughness,
		Power:      c.Power,
		Loyalty:    c.Loyalty,
		FlavorText: c.FlavorText,
	}
}

func buildCardString(c card) string {
	output := fmt.Sprintf("%s\n", boldTypeLine(c.TypeLine))
	output = output + fmt.Sprintf("%s\n", convertManaCostToSlack(c.OracleText))
	if c.Power != nil && c.Toughness != nil {
		output = output + fmt.Sprintf("* %s/%s *\n", *c.Power, *c.Toughness)
	}
	if c.Loyalty != nil {
		output = output + fmt.Sprintf("*%s*\n", *c.Loyalty)
	}
	if c.FlavorText != nil {
		output = output + fmt.Sprintf("_%s_\n", *c.FlavorText)
	}

	return output
}

func boldTypeLine(typeLine string) string {
	splitStrings := strings.Split(typeLine, "—")
	splitStrings[0] = "*" + splitStrings[0] + "*"

	var finalString string
	for i, s := range splitStrings {
		if i > 0 {
			finalString = finalString + "—"
		}
		finalString = finalString + s
	}
	return finalString
}

func convertManaCostToSlack(cost string) string {
	for i := 0; i < 20; i++ {
		strI := strconv.Itoa(i)
		cost = strings.ReplaceAll(cost, "{"+strI+"}", ":mtg__"+strI+":")
	}

	cost = strings.ReplaceAll(cost, "{T}", ":mtg_tap:")
	cost = strings.ReplaceAll(cost, "{R}", ":mtg_red:")
	cost = strings.ReplaceAll(cost, "{U}", ":mtg_blue:")
	cost = strings.ReplaceAll(cost, "{G}", ":mtg_green:")
	cost = strings.ReplaceAll(cost, "{B}", ":mtg_black:")
	cost = strings.ReplaceAll(cost, "{W}", ":mtg_white:")
	cost = strings.ReplaceAll(cost, "{X}", ":mtg__x:")
	return cost
}

func (p *Processor) postToSlack(msg *slack.MessageEvent, attachment *slack.Attachment) error {
	params := slack.NewPostMessageParameters()
	params.LinkNames = 1
	if msg.Timestamp != msg.ThreadTimestamp {
		// respond in thread, if the message is in a thread.
		params.ThreadTimestamp = msg.ThreadTimestamp
	}

	_, _, err := p.SlackClient.PostMessage(msg.Channel, slack.MsgOptionPostMessageParameters(params), slack.MsgOptionAttachments(*attachment))

	return err
}

func findBracketStyleMatches(str1 string) []string {
	var slice []string
	submatchall := regex.FindAllString(str1, -1)
	for _, element := range submatchall {
		element = strings.Trim(element, "[")
		element = strings.Trim(element, "]")
		slice = append(slice, element)
	}

	return slice
}
