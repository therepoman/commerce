package user

import (
	"context"
	"strconv"
	"strings"

	"code.justin.tv/commerce/bits-bot/config"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/web/users-service/models"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

const (
	userPrefix = "./user"
)

type Processor struct {
	Config      *config.Config       `inject:""`
	SlackClient *slack.Client        `inject:"slack_client_1"`
	UsersClient users.InternalClient `inject:""`
}

func (p *Processor) Init() {
	// no-op
}

func (p *Processor) FeatureName() string {
	return "User"
}

func (p *Processor) ShouldProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error) {
	if msg == nil {
		return false, nil
	}

	msgTxt := msg.Text
	msgTxt = strings.TrimSpace(msgTxt)

	if strings.HasPrefix(msgTxt, userPrefix) {
		return true, nil
	}

	return false, nil
}

func (p *Processor) ProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) error {
	if msg == nil {
		return nil
	}

	// Double check, just to be safe
	shouldProcess, err := p.ShouldProcessMessage(msg, conversation)
	if err != nil {
		log.WithError(err).Errorf("error calling user ShouldProcessMessage")
		return nil
	}
	if !shouldProcess {
		log.Errorf("user ShouldProcessMessage - reached illegal case")
		return nil
	}

	text := strings.TrimSpace(msg.Text)
	text = strings.TrimPrefix(text, userPrefix)
	text = strings.TrimSpace(text)

	var userResp *models.Properties
	_, err = strconv.ParseInt(text, 10, 64)
	if err != nil {
		// lookup by login
		userResp, err = p.UsersClient.GetUserByLogin(context.Background(), text, nil)
		if err != nil {
			return err
		}

	} else {
		// lookup by id
		userResp, err = p.UsersClient.GetUserByID(context.Background(), text, nil)
		if err != nil {
			return err
		}
	}

	var outTxt string

	if userResp == nil || userResp.Login == nil {
		outTxt = "*User not found*"
	} else {
		outTxt += "*ID:* " + userResp.ID + "\n"
		outTxt += "*Login:* " + *userResp.Login
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, toSlackMsgOption(outTxt)...)
	return err
}

func toSlackMsgOption(msg string) []slack.MsgOption {
	params := slack.NewPostMessageParameters()
	params.LinkNames = 1
	return []slack.MsgOption{slack.MsgOptionPostMessageParameters(params), slack.MsgOptionText(msg, false)}
}
