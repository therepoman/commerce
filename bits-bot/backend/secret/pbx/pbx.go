package pbx

import (
	"fmt"
	"math"
	"math/rand"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/lock"
	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/secret/utils"
	"code.justin.tv/commerce/bits-bot/config"
	go_redis "github.com/go-redis/redis"
	"github.com/nlopes/slack"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// TODO: this file is getting huge, split it out into smaller files
const (
	redisPBXSortedSetKey           = "$pbx$-user-balance-sorted-set"
	redisPBXUserTransactionLockKey = "$pbx$-user-transaction-lock-%s"

	initialPBXGrant = 100

	minimumUserPBXInflation = 5

	freeGiftInflationRatio   = 0.01
	freeGiftCooldownRedisKey = "$pbx$-user-free-gift-cooldown-%s"
	freeGiftCooldown         = time.Hour * 24

	dailyGrantInflationRatio = 0.01

	goldenTicketInflationRatio = 0.1

	rouletteLostBetTaxRate = 0.1

	luckyMatchPayoutFactor       = 2
	luckyMatchChancesNumerator   = 1
	luckyMatchChancesDenominator = 100
)

type BalanceHolder struct {
	User  string
	Score int64
}

type LeaderboardEntry struct {
	UserID   string
	Winnings int64
}

type PBX interface {
	GetBalance(user string) (int64, error)
	GetTotalPBX() (int64, error)
	TransferBalance(from, to string, amount int64, isLoanPayoff bool) error
	TransferBalanceAll(from string, amount int64) error
	GiveFreeGift(from, to string) (time.Duration, error)
	GetBalanceHoldersFromRange(from, to int64) ([]BalanceHolder, error)
	Onboard(candidates []string) ([]string, error)
	Offboard(usersToEject []BalanceHolder) error
	ClearBalances() error
	GrantDailyPBX() (amountGranted int64, err error)
	GrantGoldenTicket() (goldenTicketWinner string, goldenTicketAmount int64, err error)

	Roulette(from string, condition string, bet int64, spins int64) (results []RouletteResult, err error)
	GetRouletteLeaderboard(n int64) (winners []LeaderboardEntry, losers []LeaderboardEntry, err error)

	CreateLoanContractRequest(loanRecipient, loanBenefactor string, amount, vig, periodDays int64) (string, error)
	AcceptLoanRequest(loanBenefactor, contractID string) error
	GetLoansForUser(user string) ([]SortedSetLoanEntry, []SortedSetLoanEntry, error)

	CreateRPSMatchContractRequest(msg slack.MessageEvent, wager int64, gamesNum int64) (string, error)
	AcceptRPSMatchRequest(msg slack.MessageEvent, challenger, contractID string) (RPSMatchContract, error)
	PlayRPSMatch(contractID string, user string, moves []string) (RPSMatchContract, error)
	RPSWagerPayout(outcome RPSMatchOutcome) error
	GetRPSMatchesForUser(user string) ([]RPSMatchContract, []RPSMatchContract, error)

	// Stonks
	UpdateStonkLeaderboard(user string, winnings int64) error
	GetStonkLeaderboard(n int64) (winners []LeaderboardEntry, losers []LeaderboardEntry, err error)
	GetStonkQuote(symbol string) (float64, error)
	GetStonk(user string, symbol string, pbxToWager int64) (purchasePrice float64, err error)
	SellStonk(user string, symbol string) (winnings int64, winningsSubInvestment int64, err error)
	GetStonkPortfolio(user string) ([]PortfolioItem, error)
}

type PBXImpl struct {
	Config        *config.Config     `inject:""`
	RedisClient   redis.Client       `inject:""`
	LockingClient lock.LockingClient `inject:""`
	Utils         *utils.UtilHelper  `inject:""`
}

func NewPBX() PBX {
	return &PBXImpl{}
}

func (p *PBXImpl) GetBalance(user string) (int64, error) {
	balance, err := p.RedisClient.ZScore(redisPBXSortedSetKey, user)

	if err == go_redis.Nil {
		return 0, errors.New(fmt.Sprintf("user [%s] not onboarded to PBX", user))
	} else if err != nil {
		logrus.WithError(err).WithField("user", user).Error("error getting user's pbx balance")
		return 0, err
	}

	return int64(balance), nil
}

func (p *PBXImpl) DeleteUser(user string) error {
	err := p.RedisClient.ZRem(redisPBXSortedSetKey, user)
	return err
}

func (p *PBXImpl) GetTotalPBX() (int64, error) {
	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return 0, err
	}

	totalPBX := sumBalances(balanceHolders)

	return totalPBX, nil
}

func (p *PBXImpl) ClearBalances() error {
	return p.RedisClient.ZRemRangeByRank(redisPBXSortedSetKey, 0, -1)
}

func (p *PBXImpl) GrantDailyPBX() (amountGranted int64, err error) {
	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return 0, err
	}

	totalPBX := sumBalances(balanceHolders)

	if totalPBX == 0 {
		return 0, nil
	}

	amountToGrant := p.getUserInflationShare(totalPBX, len(balanceHolders), dailyGrantInflationRatio)
	var grantErr error

	for _, balanceHolder := range balanceHolders {
		err := p.IncrementBalanceBy(balanceHolder.User, amountToGrant, "")
		if err != nil {
			logrus.WithField("balance_holder", balanceHolder).WithError(err).Error("failed to grant daily PBX")
		}

		grantErr = err
	}

	if grantErr != nil {
		return 0, grantErr
	}

	return amountToGrant, nil
}

func (p *PBXImpl) GrantGoldenTicket() (goldenTicketWinner string, goldenTicketAmount int64, err error) {
	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return "", 0, err
	}

	totalPBX := sumBalances(balanceHolders)

	if totalPBX == 0 {
		return "", 0, nil
	}

	amountToGrant := p.getUserInflationShare(totalPBX, len(balanceHolders), goldenTicketInflationRatio)

	goldenTicketBalanceHolder := balanceHolders[rand.Intn(len(balanceHolders))]

	err = p.IncrementBalanceBy(goldenTicketBalanceHolder.User, amountToGrant, "")
	if err != nil {
		logrus.WithField("balance_holder", goldenTicketBalanceHolder.User).WithError(err).Error("failed to grant golden ticket")
		return "", 0, err
	}

	goldenTicketWinnerName := p.Utils.GetSlackUserName(goldenTicketBalanceHolder.User)

	return goldenTicketWinnerName, amountToGrant, nil
}

func (p *PBXImpl) Offboard(usersToEject []BalanceHolder) error {
	totalToDistribute := int64(0)

	for _, user := range usersToEject {
		unlock, err := p.createTransactionLock(user.User)
		if err != nil {
			return BenefactorLockAcquisitionError
		}

		balanceToDistribute, err := p.GetBalance(user.User)
		if err != nil {
			return BenefactorBalanceQueryError
		}

		totalToDistribute = totalToDistribute + balanceToDistribute

		logrus.WithField("user", user).Info("deleting user")
		err = p.DeleteUser(user.User)
		if err != nil {
			return err
		}

		unlock()
	}

	// Get a list of active users after deleting the newly inactive users
	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return err
	}

	// Remainder will poof, oh well
	amountToDistributeEach := math.Floor(float64(totalToDistribute) / float64(len(balanceHolders)))

	for _, balanceHolder := range balanceHolders {
		unlock, err := p.createTransactionLock(balanceHolder.User)
		if err != nil {
			return RecipientLockAcquisitionError
		}

		err = p.IncrementBalanceBy(balanceHolder.User, int64(amountToDistributeEach), "")
		if err != nil {
			return err
		}

		unlock()
	}

	return nil
}

func (p *PBXImpl) Onboard(candidates []string) ([]string, error) {
	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return nil, err
	}

	totalPBX := sumBalances(balanceHolders)

	var candidatesToOnboard []string
	if totalPBX == 0 {
		// When 0 PBX exists in market, onboard all candidates
		candidatesToOnboard = candidates
	} else {
		for _, candidate := range candidates {
			hasBalance := false

			for _, balanceHolder := range balanceHolders {
				if candidate == balanceHolder.User {
					hasBalance = true
					break
				}
			}

			if !hasBalance {
				candidatesToOnboard = append(candidatesToOnboard, candidate)
			}
		}
	}

	var amountToGrant int64
	if totalPBX == 0 {
		amountToGrant = initialPBXGrant
	} else {
		// Grant new users whichever is higher between
		//  * initial PBX grant
		//  * 50% of mean balance in economy
		meanBalance := totalPBX / int64(len(balanceHolders))
		amountToGrant = int64(math.Max(float64(initialPBXGrant), 0.5*float64(meanBalance)))
	}

	logrus.WithFields(logrus.Fields{
		"total_pbx":       totalPBX,
		"candidates":      candidates,
		"amount_to_grant": amountToGrant,
	}).Info("onboarding")

	for _, candidateToOnboard := range candidatesToOnboard {
		err := p.IncrementBalanceBy(candidateToOnboard, amountToGrant, "")
		if err != nil {
			return nil, err
		}
	}

	return candidatesToOnboard, nil
}

func (p *PBXImpl) GetBalanceHoldersFromRange(from, to int64) ([]BalanceHolder, error) {
	var entries []BalanceHolder

	zs, err := p.RedisClient.ZRevRangeWithScores(redisPBXSortedSetKey, from, to)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRevRangeWithScores in PBX#GetBalanceHoldersFromRange")
		return nil, err
	}

	for _, z := range zs {
		user, ok := z.Member.(string)
		if !ok {
			continue
		}

		entries = append(entries, BalanceHolder{
			User:  user,
			Score: int64(z.Score),
		})
	}

	return entries, nil
}

func (p *PBXImpl) IncrementBalanceBy(user string, amount int64, benefactorAttribution string) error {
	log := logrus.WithField("user", user).WithField("benefactorAttribution", benefactorAttribution)

	log.WithField("amount", amount).Info("IncrementBalanceBy input")

	// Only garnish/payoff loans if balance is being added to target user
	if amount > 0 {
		var err error
		amount, err = p.possiblyGarnishIncome(user, amount)
		if err != nil {
			return err
		}

		log.WithField("amount", amount).Info("amount after garnish input")

		// Return if there is nothing left to transfer after garnishment
		if amount <= 0 {
			return nil
		}
	}

	err := p.RedisClient.ZIncrBy(redisPBXSortedSetKey, float64(amount), user)

	if err != nil {
		logrus.WithField("user", user).Error("error incrementing user balance")
		return err
	}

	return nil
}

func (p *PBXImpl) GiveFreeGift(from, to string) (timeToNextGift time.Duration, err error) {
	if from == to {
		return 0, FreeGiftSelfRecipientError
	}

	unlockTo, err := p.createTransactionLock(to)
	if err != nil {
		return 0, RecipientLockAcquisitionError
	}
	defer unlockTo()

	_, err = p.GetBalance(to)
	if err == UserNotOnboardedError {
		return 0, IllegalRecipientError
	} else if err != nil {
		return 0, RecipientBalanceQueryError
	}

	canGiveFreeGift, timeToNextGift, err := p.canUserGiveFreeGift(from)
	if err != nil {
		return 0, err
	}

	if !canGiveFreeGift {
		return timeToNextGift, FreeGiftCooldownError
	}

	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return 0, err
	}

	totalPBX := sumBalances(balanceHolders)
	freeGiftAmount := p.getUserInflationShare(totalPBX, len(balanceHolders), freeGiftInflationRatio)

	err = p.IncrementBalanceBy(to, freeGiftAmount, from)
	if err != nil {
		return 0, errors.Wrap(err, "error adding PBX to recipient")
	}

	return 0, nil
}

func (p *PBXImpl) canUserGiveFreeGift(from string) (bool, time.Duration, error) {
	key := fmt.Sprintf(freeGiftCooldownRedisKey, from)

	_, err := p.RedisClient.Get(key)

	if err == go_redis.Nil {
		err = p.RedisClient.Set(key, "1", freeGiftCooldown)
		if err != nil {
			return false, 0, err
		}

		return true, 0, nil
	} else if err != nil {
		return false, 0, err
	}

	timeToNextGift, err := p.RedisClient.TTL(key)
	if err != nil {
		return false, 0, err
	}

	return false, timeToNextGift, nil
}

func (p *PBXImpl) TransferBalanceAll(from string, amount int64) error {
	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return err
	}

	// Distribute to everyone but self
	totalAmountToDistribute := (int64(len(balanceHolders)) - 1) * amount

	fromBalance, err := p.GetBalance(from)
	if err != nil {
		return BenefactorBalanceQueryError
	}

	if totalAmountToDistribute > fromBalance {
		return InsufficientBalanceError
	}

	for _, balanceHolder := range balanceHolders {
		if balanceHolder.User == from {
			continue
		}

		err := p.TransferBalance(from, balanceHolder.User, amount, false)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *PBXImpl) TransferBalance(from, to string, amount int64, isLoanPayoff bool) error {
	if !isLoanPayoff {
		var err error
		// Payoff any benefactor -> recipient loans first
		amount, err = p.possiblyMakeLoanPayment(to, from, amount)
		if err != nil {
			return err
		}
	}

	// Return if there is nothing left to transfer after loan payoffs
	if amount <= 0 {
		return nil
	}

	if from == to {
		return TransferBalanceSelfRecipientError
	}

	if amount < 1 {
		return UseBalanceOutOfRangeError
	}

	unlockFrom, err := p.createTransactionLock(from)
	if err != nil {
		return BenefactorLockAcquisitionError
	}
	defer unlockFrom()

	unlockTo, err := p.createTransactionLock(to)
	if err != nil {
		return RecipientLockAcquisitionError
	}
	defer unlockTo()

	fromBalance, err := p.GetBalance(from)
	if err != nil {
		return BenefactorBalanceQueryError
	}

	if amount > fromBalance {
		return InsufficientBalanceError
	}

	_, err = p.GetBalance(to)
	if err == UserNotOnboardedError {
		return IllegalRecipientError
	} else if err != nil {
		return RecipientBalanceQueryError
	}

	err = p.IncrementBalanceBy(to, amount, from)
	if err != nil {
		return errors.Wrap(err, "error adding PBX to recipient")
	}

	err = p.IncrementBalanceBy(from, -amount, "")
	if err != nil {
		return errors.Wrap(err, "added PBX to recipient, but failed to deduct from benefactor")
	}

	return nil
}

func (p *PBXImpl) createTransactionLock(user string) (func(), error) {
	lock, err := p.LockingClient.ObtainLock(fmt.Sprintf(redisPBXUserTransactionLockKey, user))
	if err != nil {
		logrus.WithField("user", user).Error("error attaining lock for pbx transaction")
		return nil, err
	}

	return func() {
		err := lock.Unlock()
		if err != nil {
			logrus.WithField("user", user).Error("error attaining lock for pbx transaction")
		}
	}, nil
}

func (p *PBXImpl) getRandomNonSelfBalanceHolder(from string, attempt int) (BalanceHolder, error) {
	if attempt > 10 {
		return BalanceHolder{}, errors.New("failed to get random non-self balance holder, bad RNG")
	}

	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return BalanceHolder{}, err
	}

	randomBalanceHolder := balanceHolders[rand.Intn(len(balanceHolders))]

	if randomBalanceHolder.User == from {
		return p.getRandomNonSelfBalanceHolder(from, attempt+1)
	}

	return randomBalanceHolder, nil
}

func (p *PBXImpl) getUserInflationShare(totalPBX int64, balanceHoldersCount int, inflationRatio float64) int64 {
	return int64(math.Max(float64(minimumUserPBXInflation), float64(totalPBX)/float64(balanceHoldersCount)*inflationRatio))
}

func sumBalances(balanceHolders []BalanceHolder) int64 {
	var sum int64 = 0
	for _, balanceHolder := range balanceHolders {
		sum = sum + balanceHolder.Score
	}

	return sum
}
