package pbx

import (
	"strconv"

	go_redis "github.com/go-redis/redis"
	"github.com/sirupsen/logrus"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/gogogadget/random"
)

const (
	redisPBXRouletteLeaderboardSortedSetKey = "$pbx$-roulette-leaderboard-v2"
	maxRouletteSpins                        = 25
	minPocket                               = 0
	maxPocket                               = 36
)

var redPockets = map[int]bool{
	1:  true,
	3:  true,
	5:  true,
	7:  true,
	9:  true,
	12: true,
	14: true,
	16: true,
	18: true,
	19: true,
	21: true,
	23: true,
	25: true,
	27: true,
	30: true,
	32: true,
	34: true,
	36: true,
}

var blackPockets = map[int]bool{
	2:  true,
	4:  true,
	6:  true,
	8:  true,
	10: true,
	11: true,
	13: true,
	15: true,
	17: true,
	20: true,
	22: true,
	24: true,
	26: true,
	28: true,
	29: true,
	31: true,
	33: true,
	35: true,
}

func (p *PBXImpl) UpdateRouletteLeaderboard(from string, winnings int64) error {
	return p.RedisClient.ZIncrBy(
		redisPBXRouletteLeaderboardSortedSetKey,
		float64(winnings),
		from)
}

func (p *PBXImpl) GetTopRouletteLeaderboardEntries(n int64) ([]LeaderboardEntry, error) {
	var entries []LeaderboardEntry

	zs, err := p.RedisClient.ZRevRangeWithScores(redisPBXRouletteLeaderboardSortedSetKey, 0, n-1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRevRangeWithScores in PBX#GetTopRouletteLeaderboardEntries")
		return nil, err
	}

	for _, z := range zs {
		user, ok := z.Member.(string)
		if !ok {
			continue
		}

		entries = append(entries, LeaderboardEntry{
			UserID:   user,
			Winnings: int64(z.Score),
		})
	}

	return entries, nil
}

func (p *PBXImpl) GetBottomRouletteLeaderboardEntries(n int64) ([]LeaderboardEntry, error) {
	var entries []LeaderboardEntry

	zs, err := p.RedisClient.ZRangeWithScores(redisPBXRouletteLeaderboardSortedSetKey, 0, n-1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRangeWithScores in PBX#GetBottomRouletteLeaderboardEntries")
		return nil, err
	}

	for _, z := range zs {
		user, ok := z.Member.(string)
		if !ok {
			continue
		}

		entries = append(entries, LeaderboardEntry{
			UserID:   user,
			Winnings: int64(z.Score),
		})
	}

	return entries, nil
}

func (p *PBXImpl) GetRouletteLeaderboard(n int64) (winners []LeaderboardEntry, losers []LeaderboardEntry, err error) {
	topEntries, err := p.GetTopRouletteLeaderboardEntries(n)
	if err != nil {
		return nil, nil, err
	}

	var topEntriesOnlyPositive []LeaderboardEntry
	for _, entry := range topEntries {
		if entry.Winnings > 0 {
			topEntriesOnlyPositive = append(topEntriesOnlyPositive, entry)
		}
	}

	bottomEntries, err := p.GetBottomRouletteLeaderboardEntries(n)
	if err != nil {
		return nil, nil, err
	}

	var bottomEntriesOnlyNegative []LeaderboardEntry
	for _, entry := range bottomEntries {
		if entry.Winnings < 0 {
			bottomEntriesOnlyNegative = append(bottomEntriesOnlyNegative, entry)
		}
	}

	return topEntriesOnlyPositive, bottomEntriesOnlyNegative, nil
}

func (p *PBXImpl) Roulette(from string, condition string, bet int64, spins int64) (results []RouletteResult, err error) {
	if bet < 1 {
		return nil, UseBalanceOutOfRangeError
	}

	// Dry run to ensure there are no validation errors before deducting from the user's balance
	_, err = roulette(condition, bet)
	if err != nil {
		return nil, err
	}

	unlockFrom, err := p.createTransactionLock(from)
	if err != nil {
		return nil, BenefactorLockAcquisitionError
	}
	defer unlockFrom()

	fromBalance, err := p.GetBalance(from)
	if err != nil {
		return nil, BenefactorBalanceQueryError
	}

	netBet := spins * bet

	if netBet > fromBalance {
		return nil, InsufficientBalanceError
	}

	err = p.IncrementBalanceBy(from, -netBet, "")
	if err != nil {
		return nil, errors.Wrap(err, "failed to deduct bet before playing")
	}

	err = p.UpdateRouletteLeaderboard(from, -netBet)
	if err != nil {
		return nil, errors.Wrap(err, "failed to increment roulette leaderboard")
	}

	for i := 0; i < int(spins); i++ {
		result, err := roulette(condition, bet)

		if err != nil {
			return nil, err
		}

		// If user wins, grant them their winnings
		if result.Winnings > 0 {
			err = p.IncrementBalanceBy(from, result.Winnings, "")
			if err != nil {
				return nil, errors.Wrap(err, "failed to award winnings")
			}

			err := p.UpdateRouletteLeaderboard(from, result.Winnings)
			if err != nil {
				return nil, errors.Wrap(err, "failed to increment roulette leaderboard")
			}

			// Give a portion of their initial bet to a random member of the economy if the bet is large enough
		} else if bet >= 10 {
			recipientOfLostBet, err := p.getRandomNonSelfBalanceHolder(from, 0)
			if err != nil {
				return nil, err
			}

			result.RecipientOfLostBet = recipientOfLostBet

			unlockTo, err := p.createTransactionLock(recipientOfLostBet.User)
			if err != nil {
				return nil, RecipientLockAcquisitionError
			}

			lostBetDistribution := int64(float64(bet) * rouletteLostBetTaxRate)

			result.LostBetDistribution = lostBetDistribution

			err = p.IncrementBalanceBy(recipientOfLostBet.User, lostBetDistribution, from)
			if err != nil {
				return nil, errors.Wrap(err, "failed to award initial bet to random user")
			}

			unlockTo()
		}

		results = append(results, result)
	}

	return results, nil
}

type RouletteResult struct {
	Pocket              int
	Winnings            int64
	RecipientOfLostBet  BalanceHolder
	LostBetDistribution int64
}

func roulette(condition string, bet int64) (result RouletteResult, err error) {
	isIntCondition := false
	int64Condition, err := strconv.ParseInt(condition, 10, 64)
	intCondition := int(int64Condition)
	if err == nil {
		isIntCondition = true
	}

	pocket := random.Int(minPocket, maxPocket)
	var winnings int64 = 0

	if isIntCondition {
		if intCondition < minPocket || intCondition > maxPocket {
			return RouletteResult{}, RouletteInvalidPocketError
		}

		if intCondition == pocket {
			winnings = bet * 35
		}
	} else {
		switch condition {
		case "odd":
			if pocket%2 != 0 {
				winnings = bet * 2
			}
		case "even":
			// 0 doesn't count as even in roulette
			if pocket != 0 && pocket%2 == 0 {
				winnings = bet * 2
			}
		case "red":
			_, ok := redPockets[pocket]
			if ok {
				winnings = bet * 2
			}
		case "black":
			_, ok := blackPockets[pocket]
			if ok {
				winnings = bet * 2
			}
		case "1-18":
			if pocket >= 1 && pocket <= 18 {
				winnings = bet * 2
			}
		case "19-36":
			if pocket >= 19 && pocket <= 36 {
				winnings = bet * 2
			}
		case "1-12":
			if pocket >= 1 && pocket <= 12 {
				winnings = bet * 3
			}
		case "13-24":
			if pocket >= 13 && pocket <= 24 {
				winnings = bet * 3
			}
		case "25-36":
			if pocket >= 25 && pocket <= 36 {
				winnings = bet * 3
			}
		default:
			return RouletteResult{}, RouletteInvalidPocketError
		}
	}

	return RouletteResult{
		Pocket:   pocket,
		Winnings: winnings,
	}, nil
}
