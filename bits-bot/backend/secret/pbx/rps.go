package pbx

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/gogogadget/random"

	"github.com/nlopes/slack"

	"code.justin.tv/commerce/logrus"
	go_redis "github.com/go-redis/redis"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

const (
	redisPBXRPSMatchRequestKey = "$pbx$-rps-match-request-%s"
	RPSMatchRequestExpiration  = time.Hour

	redisPBXRPSMatchMasterRecordKey = "$pbx$-rps-match-%s"

	redisPBXRPSMatchHostSortedSetKey       = "$pbx$-rps-match-host-%s"
	redisPBXRPSMatchChallengerSortedSetKey = "$pbx$-rps-match-challenger-%s"
	redisPBXRPSMatchSortedSetMemberKey     = "%s"
	redisPBXRPSOtherPlayerStoredMoves      = "$pbx$-rps-other-player-moves-%s-%s"
	rpsWagerPayoutBenefactor               = "rpsWagerPayout"

	maxRPSGamesPerMatch     = 100
	minRPSGamesPerMatch     = 1
	rpsDefaultNumberOfGames = 1
)

const (
	rps     = MatchType("rps")
	invalid = MatchType("")
)

type MatchType string

type RPSMatchContract struct {
	Host       string
	Challenger string
	Wager      int64
	GamesNum   int64
	//TODO make contractID human readable
	ContractID         string
	RequestedAt        time.Time
	AcceptedAt         time.Time
	MsgTimestamp       string
	MsgThreadTimestamp string
	OriginalChannel    string
	Outcome            RPSMatchOutcome
}

type RPSMatchOutcome struct {
	IsMatchFinished bool
	Host            string
	HostScore       int64
	Challenger      string
	ChallengerScore int64
	RPSGameOutcomes []RPSGameOutcome
	WasATie         bool
	DidHostWin      bool
	Wager           int64
	WasLuckyMatch   bool
}

type RPSGameOutcome struct {
	HostMove       string
	ChallengerMove string
	DidHostWin     bool
	WasATie        bool
}

var WinsAgainst = map[string]string{
	"rock":     "scissors",
	"paper":    "rock",
	"scissors": "paper",
}

func (p *PBXImpl) CreateRPSMatchContractRequest(msg slack.MessageEvent, wager int64, gamesNum int64) (string, error) {
	if wager < 1 {
		return "", RPSMatchWagerTooLowError
	}

	if gamesNum > maxRPSGamesPerMatch {
		return "", RPSGamesExceedMaximumError
	}

	if gamesNum < 1 {
		return "", RPSGamesBelowMinimumError
	}

	host := msg.User
	hostBalance, err := p.GetBalance(host)
	if err != nil {
		return "", RPSHostBalanceQueryError
	}

	if hostBalance < wager {
		return "", InsufficientBalanceError
	}

	contractID, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	contractIDStr := contractID.String()

	contract := RPSMatchContract{
		Host:               host,
		Wager:              wager,
		GamesNum:           gamesNum,
		ContractID:         contractIDStr,
		RequestedAt:        time.Now(),
		MsgTimestamp:       msg.Timestamp,
		MsgThreadTimestamp: msg.ThreadTimestamp,
		OriginalChannel:    msg.Channel,
	}

	contractBytes, err := json.Marshal(contract)
	if err != nil {
		return "", err
	}

	err = p.RedisClient.Set(fmt.Sprintf(redisPBXRPSMatchRequestKey, contractIDStr), string(contractBytes), RPSMatchRequestExpiration)
	if err != nil {
		return "", err
	}

	return contractIDStr, nil
}

func (p *PBXImpl) GetRPSMatchesForUser(user string) ([]RPSMatchContract, []RPSMatchContract, error) {
	hostMatches, err := p.GetAllRPSMatchesForUser(redisPBXRPSMatchHostSortedSetKey, user)
	if err != nil {
		return nil, nil, err
	}

	challengerMatches, err := p.GetAllRPSMatchesForUser(redisPBXRPSMatchChallengerSortedSetKey, user)
	if err != nil {
		return nil, nil, err
	}

	return hostMatches, challengerMatches, nil
}

func (p *PBXImpl) AcceptRPSMatchRequest(msg slack.MessageEvent, challenger, contractID string) (RPSMatchContract, error) {
	var rpsMatchRequest RPSMatchContract

	rpsMatchRequestStr, err := p.RedisClient.Get(fmt.Sprintf(redisPBXRPSMatchRequestKey, contractID))

	if err == go_redis.Nil {
		return RPSMatchContract{}, RPSMatchNotFoundError
	} else if err != nil {
		return RPSMatchContract{}, err
	}

	err = json.Unmarshal([]byte(rpsMatchRequestStr), &rpsMatchRequest)
	if err != nil {
		return RPSMatchContract{}, err
	}

	if challenger == rpsMatchRequest.Host {
		return RPSMatchContract{}, RPSMatchHostSelfError
	}

	if rpsMatchRequest.Challenger != "" {
		return RPSMatchContract{}, RPSMatchChallengerAlreadyFoundError
	}

	// We don't know if the original message to start the match came from the start of a thread or the middle of one,
	// so we need to check either of those cases in order to validate that this accept message was in the right place
	if rpsMatchRequest.MsgTimestamp != msg.Timestamp &&
		rpsMatchRequest.MsgTimestamp != msg.ThreadTimestamp &&
		rpsMatchRequest.MsgThreadTimestamp != msg.ThreadTimestamp &&
		rpsMatchRequest.MsgThreadTimestamp != msg.Timestamp {
		return RPSMatchContract{}, RPSMatchNotInOriginalThreadError
	}

	unlockHost, err := p.createTransactionLock(rpsMatchRequest.Host)
	if err != nil {
		return RPSMatchContract{}, RPSHostLockAcquisitionError
	}
	defer unlockHost()

	unlockChallenger, err := p.createTransactionLock(challenger)
	if err != nil {
		return RPSMatchContract{}, RPSChallengerLockAcquisitionError
	}
	defer unlockChallenger()

	hostBalance, err := p.GetBalance(rpsMatchRequest.Host)
	if err != nil {
		return RPSMatchContract{}, RPSHostBalanceQueryError
	}

	if rpsMatchRequest.Wager > hostBalance {
		return RPSMatchContract{}, InsufficientBalanceError
	}

	challengerBalance, err := p.GetBalance(challenger)
	if err != nil {
		return RPSMatchContract{}, RPSChallengerBalanceQueryError
	}

	if rpsMatchRequest.Wager > challengerBalance {
		return RPSMatchContract{}, InsufficientBalanceError
	}

	rpsMatchRequest.Challenger = challenger
	rpsMatchRequest.AcceptedAt = time.Now()
	contractBytes, err := json.Marshal(rpsMatchRequest)
	if err != nil {
		return RPSMatchContract{}, err
	}

	// create master contract record
	err = p.RedisClient.Set(fmt.Sprintf(redisPBXRPSMatchMasterRecordKey, rpsMatchRequest.ContractID), string(contractBytes), 0)
	if err != nil {
		return RPSMatchContract{}, err
	}

	// delete match request
	_, err = p.RedisClient.Del([]string{fmt.Sprintf(redisPBXRPSMatchRequestKey, rpsMatchRequest.ContractID)})
	if err != nil {
		return RPSMatchContract{}, err
	}

	// Add match to challenger's sorted set
	err = p.RedisClient.ZIncrBy(
		fmt.Sprintf(redisPBXRPSMatchChallengerSortedSetKey, rpsMatchRequest.Challenger),
		float64(rpsMatchRequest.Wager),
		getRPSMatchSortedSetKey(rpsMatchRequest.ContractID))
	if err != nil {
		return RPSMatchContract{}, err
	}

	// Add match to host's sorted set
	err = p.RedisClient.ZIncrBy(
		fmt.Sprintf(redisPBXRPSMatchHostSortedSetKey, rpsMatchRequest.Host),
		float64(rpsMatchRequest.Wager),
		getRPSMatchSortedSetKey(rpsMatchRequest.ContractID))
	if err != nil {
		return RPSMatchContract{}, err
	}

	// Deduct wager amount from challenger
	err = p.IncrementBalanceBy(challenger, -rpsMatchRequest.Wager, "")
	if err != nil {
		return RPSMatchContract{}, errors.Wrap(err, "failed to deduct wager from challenger")
	}

	// Deduct wager amount from host
	err = p.IncrementBalanceBy(rpsMatchRequest.Host, -rpsMatchRequest.Wager, "")
	if err != nil {
		return RPSMatchContract{}, errors.Wrap(err, "failed to deduct wager from host")
	}

	return rpsMatchRequest, nil
}

func (p *PBXImpl) PlayRPSMatch(contractID string, user string, moves []string) (RPSMatchContract, error) {
	//TODO: remove
	logrus.WithFields(logrus.Fields{
		"contractID": contractID,
		"user":       user,
		"moves":      moves,
	}).Info("Began PlayRPSMatch")

	unlockGame, err := p.createTransactionLock(contractID)
	if err != nil {
		return RPSMatchContract{}, RPSGameLockAcquisitionError
	}
	defer unlockGame()

	foundMatch, err := p.lookupMasterContractForMatch(contractID)
	if err != nil {
		return RPSMatchContract{}, err
	}

	//TODO: remove
	logrus.WithField("foundMatch", foundMatch).Info("looked up and unmarshalled match from redis")

	if foundMatch.Outcome.IsMatchFinished {
		return RPSMatchContract{}, RPSMatchAlreadyEndedError
	}

	if int64(len(moves)) != foundMatch.GamesNum {
		return RPSMatchContract{}, RPSMatchInvalidNumberOfMovesError
	}

	err = validateUserIsPlayerInMatch(foundMatch, user)
	if err != nil {
		return RPSMatchContract{}, err
	}

	otherPlayer := ""
	if foundMatch.Host == user {
		otherPlayer = foundMatch.Challenger
	} else {
		otherPlayer = foundMatch.Host
	}

	// Has this player already submitted their moves?
	selfStoredMoves, err := p.getStoredMovesForPlayer(user, contractID)
	if err != nil {
		return RPSMatchContract{}, err
	}

	//TODO: remove
	logrus.WithField("selfStoredMoves", selfStoredMoves).Info("looked up and unmarshalled self stored moves in redis")

	//This player has already moved
	if len(selfStoredMoves) != 0 {
		return RPSMatchContract{}, RPSPlayerAlreadyMovedError
	}

	// Has the other player already submitted their moves?
	otherPlayerMoves, err := p.getStoredMovesForPlayer(otherPlayer, contractID)
	if err != nil {
		return RPSMatchContract{}, err
	}

	//TODO: remove
	logrus.WithField("otherPlayerMoves", otherPlayerMoves).Info("looked up and unmarshalled other player stored moves in redis")

	//Other player has not moved yet
	if len(otherPlayerMoves) == 0 {
		// Store these ones and return an empty contract so the processor will send a "Waiting for other player" message
		moveBytes, err := json.Marshal(moves)
		if err != nil {
			return RPSMatchContract{}, err
		}

		err = p.RedisClient.Set(fmt.Sprintf(redisPBXRPSOtherPlayerStoredMoves, contractID, user), string(moveBytes), 0)
		if err != nil {
			return RPSMatchContract{}, err
		}

		foundMatch.Outcome = RPSMatchOutcome{
			Host:       foundMatch.Host,
			Challenger: foundMatch.Challenger,
		}

		return foundMatch, nil
	}

	if len(moves) != len(otherPlayerMoves) {
		return RPSMatchContract{}, RPSMatchInvalidMovesNumberPerPlayerError
	}

	var hostMoves, challengerMoves []string
	if foundMatch.Host == user {
		hostMoves = moves
		challengerMoves = otherPlayerMoves
	} else {
		hostMoves = otherPlayerMoves
		challengerMoves = moves
	}
	matchOutcome := evaluateRPSMatch(foundMatch, hostMoves, challengerMoves)
	foundMatch.Outcome = matchOutcome

	//TODO: remove
	logrus.WithField("matchOutcome", matchOutcome).Info("evaluated RPS match")

	// Delete match from host sorted set
	err = p.RedisClient.ZRem(
		fmt.Sprintf(redisPBXRPSMatchHostSortedSetKey, foundMatch.Host),
		getRPSMatchSortedSetKey(foundMatch.ContractID))
	if err != nil {
		return RPSMatchContract{}, err
	}

	// Delete match from challenger sorted set
	err = p.RedisClient.ZRem(
		fmt.Sprintf(redisPBXRPSMatchChallengerSortedSetKey, foundMatch.Challenger),
		getRPSMatchSortedSetKey(foundMatch.ContractID))
	if err != nil {
		return RPSMatchContract{}, err
	}

	matchBytes, err := json.Marshal(foundMatch)
	if err != nil {
		return RPSMatchContract{}, err
	}

	//TODO: remove
	logrus.WithField("foundMatch", foundMatch).Info("unmarshalled updated match contract")

	err = p.RedisClient.Set(fmt.Sprintf(redisPBXRPSMatchMasterRecordKey, contractID), matchBytes, 0)
	if err != nil {
		return RPSMatchContract{}, err
	}

	return foundMatch, nil
}

func (p *PBXImpl) RPSWagerPayout(outcome RPSMatchOutcome) error {
	if outcome.WasATie {
		err := p.IncrementBalanceBy(outcome.Host, outcome.Wager, rpsWagerPayoutBenefactor)
		if err != nil {
			return err
		}

		err = p.IncrementBalanceBy(outcome.Challenger, outcome.Wager, rpsWagerPayoutBenefactor)
		if err != nil {
			return err
		}

		return nil
	}

	payout := 2 * outcome.Wager

	if outcome.WasLuckyMatch {
		payout = luckyMatchPayoutFactor * payout
	}

	if outcome.DidHostWin {
		err := p.IncrementBalanceBy(outcome.Host, payout, rpsWagerPayoutBenefactor)
		if err != nil {
			return err
		}

		return nil
	}

	err := p.IncrementBalanceBy(outcome.Challenger, payout, rpsWagerPayoutBenefactor)
	if err != nil {
		return err
	}

	return nil
}

func (p *PBXImpl) GetAllRPSMatchesForUser(key, user string) ([]RPSMatchContract, error) {
	var contracts []RPSMatchContract

	zs, err := p.RedisClient.ZRevRangeWithScores(fmt.Sprintf(key, user), 0, -1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRevRangeWithScores in PBX#GetAllRPSMatchesForUser")
		return nil, err
	}

	for _, z := range zs {
		key, ok := z.Member.(string)
		if !ok {
			continue
		}

		contractID, err := parseRPSMatchSortedSetKey(key)
		if err != nil {
			return nil, err
		}

		contract, err := p.lookupMasterContractForMatch(contractID)
		if err != nil {
			return nil, err
		}

		contracts = append(contracts, contract)
	}

	return contracts, nil
}

func getRPSMatchSortedSetKey(contractID string) string {
	return fmt.Sprintf(redisPBXRPSMatchSortedSetMemberKey, contractID)
}

func parseRPSMatchSortedSetKey(key string) (contractID string, err error) {
	parts := strings.Split(key, ".")

	if len(parts) != 1 {
		return "", errors.New("match sorted set key in unexpected format")
	}

	return parts[0], nil
}

func validateUserIsPlayerInMatch(foundMatch RPSMatchContract, user string) error {
	if foundMatch.Host != user && foundMatch.Challenger != user {
		return RPSMatchInvalidPlayerError
	}

	return nil
}

func (p *PBXImpl) lookupMasterContractForMatch(contractID string) (RPSMatchContract, error) {
	var rpsMatchContract RPSMatchContract

	contractStr, err := p.RedisClient.Get(fmt.Sprintf(redisPBXRPSMatchMasterRecordKey, contractID))
	if err == go_redis.Nil {
		logrus.WithError(err).Error("Master record lookup returned nil")
		return rpsMatchContract, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis Master record")
		return rpsMatchContract, err
	}

	err = json.Unmarshal([]byte(contractStr), &rpsMatchContract)
	if err != nil {
		return rpsMatchContract, err
	}

	return rpsMatchContract, nil
}

func (p *PBXImpl) getStoredMovesForPlayer(user string, contractID string) ([]string, error) {
	var storedMoves []string
	storedMovesStr, err := p.RedisClient.Get(fmt.Sprintf(redisPBXRPSOtherPlayerStoredMoves, contractID, user))

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	if storedMovesStr == "" {
		return nil, nil
	}

	err = json.Unmarshal([]byte(storedMovesStr), &storedMoves)
	if err != nil {
		return nil, err
	}

	logrus.WithFields(logrus.Fields{
		"user":           user,
		"contractID":     contractID,
		"storedMovesStr": storedMovesStr,
		"storedMoves":    storedMoves,
	}).Info()

	return storedMoves, nil
}

func evaluateRPSMatch(match RPSMatchContract, hostMoves []string, challengerMoves []string) RPSMatchOutcome {
	outcome := RPSMatchOutcome{
		Host:            match.Host,
		HostScore:       0,
		Challenger:      match.Challenger,
		ChallengerScore: 0,
		RPSGameOutcomes: make([]RPSGameOutcome, 0, len(hostMoves)),
		Wager:           match.Wager,
	}

	//TODO: remove
	logrus.WithField("hostMoves", hostMoves).Info("passed in host moves")

	//TODO: remove
	logrus.WithField("challengerMoves", challengerMoves).Info("passed in challenger moves")

	for i, hostMove := range hostMoves {
		challengerMove := challengerMoves[i]

		gameOutcome := evaluateRPSGame(hostMove, challengerMove)

		//TODO: remove
		logrus.WithField("gameOutcome", gameOutcome).Info("individual game outcome")

		if !gameOutcome.WasATie {
			if gameOutcome.DidHostWin {
				outcome.HostScore++
			} else {
				outcome.ChallengerScore++
			}
		}

		outcome.RPSGameOutcomes = append(outcome.RPSGameOutcomes, gameOutcome)
	}

	outcome.IsMatchFinished = true
	outcome.WasATie = outcome.HostScore == outcome.ChallengerScore
	outcome.DidHostWin = outcome.HostScore > outcome.ChallengerScore

	outcome.WasLuckyMatch = evaluateWasLuckyMatch()

	return outcome
}

func evaluateWasLuckyMatch() bool {
	randomChance := random.Int(luckyMatchChancesNumerator, luckyMatchChancesDenominator)
	if randomChance == luckyMatchChancesDenominator {
		return true
	}

	return false
}

func evaluateRPSGame(hostMove string, challengerMove string) RPSGameOutcome {
	gameOutcome := RPSGameOutcome{
		HostMove:       hostMove,
		ChallengerMove: challengerMove,
	}

	if hostMove == challengerMove {
		gameOutcome.WasATie = true
		return gameOutcome
	}

	losingMove := WinsAgainst[hostMove]

	gameOutcome.DidHostWin = losingMove == challengerMove
	return gameOutcome
}
