package pbx

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/secret"
	"code.justin.tv/commerce/bits-bot/backend/secret/models"
	"code.justin.tv/commerce/bits-bot/backend/secret/utils"
	"code.justin.tv/commerce/bits-bot/config"
	"github.com/nlopes/slack"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

//TODO: this file is getting huge, split it out into smaller files
const (
	pbxPrivilegedUsersKey    = "pbx-privileged-users"
	privilegedUserExpiration = time.Hour

	pbxPrefix = "./pbx"

	// Actions
	Balance             = models.Action("Balance")
	Transfer            = models.Action("Transfer")
	DistributeWealth    = models.Action("DistributeWealth")
	Onboard             = models.Action("Onboard")
	Offboard            = models.Action("Offboard")
	FreeGift            = models.Action("FreeGift")
	Leaderboard         = models.Action("Leaderboard")
	TotalPBX            = models.Action("TotalPBX")
	Roulette            = models.Action("Roulette")
	RouletteLeaderboard = models.Action("RouletteLeaderboard")
	RequestLoan         = models.Action("RequestLoan")
	AcceptLoan          = models.Action("AcceptLoan")
	GetLoans            = models.Action("GetLoans")
	RequestMatch        = models.Action("RequestMatch")
	AcceptMatch         = models.Action("AcceptMatch")
	PlayMatch           = models.Action("PlayMatch")
	GetMatches          = models.Action("GetMatches")
	LoanHelp            = models.Action("LoanHelp")
	MatchHelp           = models.Action("MatchHelp")
	RouletteHelp        = models.Action("RouletteHelp")
	StonkHelp           = models.Action("StonkHelp")
	StonkLeaderboard    = models.Action("StonkLeaderboard")
	StonkGet            = models.Action("StonkGet")
	StonkQuote          = models.Action("StonkQuote")
	StonkSell           = models.Action("StonkSell")
	StonkPortfolio      = models.Action("StonkPortfolio")
)

var (
	supportedChannels []string

	cliArgRegexp  *regexp.Regexp
	userArgRegexp *regexp.Regexp
)

const usageMessage = `Usage: ./pbx

Commands:
--help    (-h)                    : Shows this screen
--balance (-b)                    : Displays the PBX balance of the requesting user
--transfer (-t) <to> <amount>     : Transfer <amount> PBX to user
--distributewealth (-dw) <amount> : Distributes <amount> PBX to all other balance holders (taken from own balance)
--onboard (-o)                    : Grants initial balance to new users
--offboard (-off)                : Ejects ex-Twitch employees from PBX, distributing their balances evenly
--freegift (-fg) <to>             : Gift a user PBX equal to 1% of mean balance (once per day per benefactor) - inflates PBX
--leaderboard (-lb)               : Displays top 10 PBX holders
--totalpbx (-tp)                  : Displays total PBX in circulation


--loan-help (-lh)     : Show loan commands
--match-help (-mh)    : Show match commands
--roulette-help (-rh) : Show roulette commands
--stonk-help (-sh)    : Show stonk commands
`

const rouletteUsageMessage = `Usage: ./pbx

Commands:
--roulette (-r) <condition> <amount> <n_spins>: Spins a roulette wheel <n_spins> times, gambles <amount> on <condition>
                                       			Supported conditions - [0-36], red, black, odd, even, 1-18, 19-36, 1-12, 13-24, 25-36
--roulette-leaderboard (-rlb)        		  : Shows top 5 roulette winners and losers
`

const stonkUsageMessage = `Usage: ./pbx

Commands:
--stonk-leaderboard (-slb)        		   : Shows top 5 stonk winners and losers
--stonk-get (-sg) <symbol> <amount>        : Purchases a stonk. Removes <amount> PBX immediately, adding the symbol to your portfolio.
                                             When the stonk is sold, winnings are calculated based on the current Bid relative to the cost basis Bid.
--stonk-quote (-sq) <symbol>               : Returns current Bid for <symbol> according to Yahoo finance
--stonk-sell (-ss) <symbol>                : Sells 100% stake in <symbol>
--stonk-portfolio (-sp)                    : Returns a list of all holdings for current user
`

const matchUsageMessage = `Usage: ./pbx

Commands:
--requestmatch (-rm) <game_type> <wager> [<games_num>] : Requests a match of <game_type> of [optionally] length <games_num> with the host and challenger 
                                                         each putting forth <wager> pbx. Only the host and the first person to accept the match are 
                                                         allowed to play in it. Returns a <contract_id>. Expires in an hour if not accepted.
                                                         Currently supported games are: [rps] (Rock-Paper-Scissors).
--acceptmatch (-am) <game_type> <contract_id>          : Accepts a match, which forfeits the agreed <wager> amount from each player.
--playmatch (-pm) <game_type> <contract_id> <move>     : DM-only -- Submits your move for the associated game match. Once both players have submitted, the 
                                                         game results and payout will be shown in the original thread.
--getmatches (-gm) <game_type>                         : Lists all accepted matches of <game_type> which have not been completed. 
`

const loanUsageMessage = `Usage: ./pbx

Commands:
--requestloan (-rl) <from> <amount> <vig> <period>  : Requests a loan <from> user for <amount> with a <vig> PBX, due within <period> days.
                                                      Returns a <contract_id>. Expires in an hour if not accepted. While the contract is outstanding,
                                                      all -t/-dw/-fg/-r (loss distribution) from loan recipient -> benefactor goes towards contract payoff.
                                                      Income is garnished if loan is not repaid within <period> days. There is no benefit for paying a
                                                      loan off early. 
--acceptloan (-al) <contract_id>                    : Accepts loan request
--getloans (-gl)                                    : Lists outstanding loans
`

var commands = map[string]models.Action{
	"h":     models.Help,
	"help":  models.Help,
	"u":     models.Help,
	"usage": models.Help,

	"b":       Balance,
	"balance": Balance,

	"t":        Transfer,
	"transfer": Transfer,

	"dw":               DistributeWealth,
	"distributewealth": DistributeWealth,

	"o":       Onboard,
	"onboard": Onboard,

	"off":      Offboard,
	"offboard": Offboard,

	"fg":       FreeGift,
	"freegift": FreeGift,

	"lb":          Leaderboard,
	"leaderboard": Leaderboard,

	"tp":       TotalPBX,
	"totalpbx": TotalPBX,

	"r":        Roulette,
	"roulette": Roulette,

	"rlb":                  RouletteLeaderboard,
	"roulette-leaderboard": RouletteLeaderboard,

	"rl":          RequestLoan,
	"requestloan": RequestLoan,

	"al":         AcceptLoan,
	"acceptloan": AcceptLoan,

	"gl":       GetLoans,
	"getloans": GetLoans,

	"rm":           RequestMatch,
	"requestmatch": RequestMatch,

	"am":          AcceptMatch,
	"acceptmatch": AcceptMatch,

	"pm":        PlayMatch,
	"playmatch": PlayMatch,

	"gm":         GetMatches,
	"getmatches": GetMatches,

	"loan-help": LoanHelp,
	"lh":        LoanHelp,

	"match-help": MatchHelp,
	"mh":         MatchHelp,

	"roulette-help": RouletteHelp,
	"rh":            RouletteHelp,

	"stonk-help": StonkHelp,
	"sh":         StonkHelp,

	"slb":               StonkLeaderboard,
	"stonk-leaderboard": StonkLeaderboard,

	"sg":        StonkGet,
	"stonk-get": StonkGet,

	"sq":          StonkQuote,
	"stonk-quote": StonkQuote,

	"ss":         StonkSell,
	"stonk-sell": StonkSell,

	"sp":              StonkPortfolio,
	"stonk-portfolio": StonkPortfolio,
}

func NewPBXProcessor() secret.Processor {
	return &ProcessorImpl{}
}

type ProcessorImpl struct {
	Config      *config.Config    `inject:""`
	SlackClient *slack.Client     `inject:"slack_client_1"`
	RedisClient redis.Client      `inject:""`
	Utils       *utils.UtilHelper `inject:""`
	PBX         PBX               `inject:""`
}

func (p *ProcessorImpl) Init() {
	supportedChannels = []string{p.Config.ThePubbChannelID, p.Config.BitsBotTestChannelID}
	cliArgRegexp = regexp.MustCompile("(^| )-+[a-z0-9-_]+( .*)?")
	userArgRegexp = regexp.MustCompile("<@(.*)>")
}

func (p *ProcessorImpl) FeatureName() string {
	return "Pubb Bucks(PBX)"
}

func (p *ProcessorImpl) ProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) error {
	if msg == nil {
		return nil
	}

	// Double check, just to be safe
	shouldProcess, err := p.ShouldProcessMessage(msg, conversation)
	if err != nil {
		logrus.WithError(err).Errorf("error calling pbx ShouldProcessMessage")
		return nil
	}
	if !shouldProcess {
		logrus.Errorf("pbx ShouldProcessMessage - reached illegal case")
		return nil
	}

	text := strings.TrimSpace(msg.Text)
	text = strings.TrimPrefix(text, pbxPrefix)
	text = strings.TrimSpace(text)

	args := cliArgRegexp.FindAllString(text, -1)

	argMap := make(map[models.Action]string)
	for _, arg := range args {
		argMap[utils.GetActionFromArg(arg, commands)] = utils.GetArgValue(arg)
	}

	switch utils.GetActionFromArgs(args, commands) {
	case Balance:
		return p.handleBalanceQuery(msg, conversation)
	case Transfer:
		return p.handleBalanceTransfer(msg, conversation, argMap)
	case Onboard:
		return p.handleOnboard(msg, conversation)
	case Offboard:
		return p.handleOffboard(msg, conversation)
	case FreeGift:
		return p.handleFreeGift(msg, conversation, argMap)
	case Leaderboard:
		return p.handleLeaderboard(msg, conversation)
	case TotalPBX:
		return p.handleTotalPBXQuery(msg, conversation)
	case DistributeWealth:
		return p.handleDistributeWealth(msg, conversation, argMap)
	case Roulette:
		return p.handleRoulette(msg, conversation, argMap)
	case RouletteLeaderboard:
		return p.handleRouletteLeaderboard(msg, conversation)
	case RequestLoan:
		return p.handleRequestLoan(msg, conversation, argMap)
	case AcceptLoan:
		return p.handleAcceptLoan(msg, conversation, argMap)
	case GetLoans:
		return p.handleGetLoans(msg, conversation)
	case RequestMatch:
		switch extractMatchType(utils.SplitTailArgs(argMap[RequestMatch])) {
		case rps:
			return p.handleRequestMatchRPS(msg, conversation, argMap)
		}
		return p.handleInvalid(msg, conversation, InvalidMatchTypeError.Error())
	case AcceptMatch:
		switch extractMatchType(utils.SplitTailArgs(argMap[AcceptMatch])) {
		case rps:
			return p.handleAcceptMatchRPS(msg, conversation, argMap)
		}
		return p.handleInvalid(msg, conversation, InvalidMatchTypeError.Error())
	case PlayMatch:
		switch extractMatchType(utils.SplitTailArgs(argMap[PlayMatch])) {
		case rps:
			return p.handlePlayMatchRPS(msg, conversation, argMap)
		}
		return p.handleInvalid(msg, conversation, InvalidMatchTypeError.Error())
	case GetMatches:
		switch extractMatchType(utils.SplitTailArgs(argMap[GetMatches])) {
		case rps:
			return p.handleGetMatchesRPS(msg, conversation)
		}
		return p.handleInvalid(msg, conversation, InvalidMatchTypeError.Error())
	case StonkLeaderboard:
		return p.handleStonkLeaderboard(msg, conversation)
	case StonkQuote:
		return p.handleStonkQuote(msg, conversation, argMap)
	case StonkGet:
		return p.handleStonkGet(msg, conversation, argMap)
	case StonkSell:
		return p.handleStonkSell(msg, conversation, argMap)
	case StonkPortfolio:
		return p.handleStonkPortfolio(msg, conversation)
	case models.Help:
		return p.handleHelp(msg, conversation)
	case LoanHelp:
		return p.handleLoanHelp(msg, conversation)
	case MatchHelp:
		return p.handleMatchHelp(msg, conversation)
	case RouletteHelp:
		return p.handleRouletteHelp(msg, conversation)
	case StonkHelp:
		return p.handleStonkHelp(msg, conversation)
	case models.Invalid:
		return p.handleInvalid(msg, conversation, "unknown command")
	}

	return p.handleInvalid(msg, conversation, "unknown command")
}

func (p *ProcessorImpl) ShouldProcessMessage(msg *slack.MessageEvent, conversation *slack.Channel) (bool, error) {
	if msg == nil {
		return false, nil
	}

	//disabling convo for now
	/*if conversation.IsIM {
		//Check to see if the user is allowed to DM bits-bot about this secret
		ok, err := p.Utils.IsSuperUser(msg.User, pbxPrivilegedUsersKey, p.Config.ThePubbChannelID, privilegedUserExpiration)
		if err != nil {
			return false, err
		}
		if !ok {
			return false, nil
		}
	} else */
	if !utils.InSupportedChannel(msg, supportedChannels) {
		return false, nil
	}

	msgTxt := msg.Text
	msgTxt = strings.TrimSpace(msgTxt)

	if strings.HasPrefix(msgTxt, pbxPrefix) {
		return true, nil
	}

	return false, nil
}

func (p *ProcessorImpl) handleOnboard(msg *slack.MessageEvent, conversation *slack.Channel) error {
	members, _, err := p.SlackClient.GetUsersInConversation(&slack.GetUsersInConversationParameters{
		ChannelID: p.Config.ThePubbChannelID,
	})
	if err != nil {
		logrus.WithError(err).Error("error querying for channel members in handleOnboard")
		return p.handleError(msg, err)
	}

	usersOnboarded, err := p.PBX.Onboard(members)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getOnboardMessage(len(usersOnboarded)), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleOffboard(msg *slack.MessageEvent, conversation *slack.Channel) error {
	balanceHolders, err := p.PBX.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return p.handleError(msg, err)
	}

	var usersToEject []BalanceHolder

	for _, balanceHolder := range balanceHolders {
		user, err := p.SlackClient.GetUserInfo(balanceHolder.User)
		if err != nil {
			return p.handleError(msg, err)
		}

		if user == nil || user.Deleted {
			usersToEject = append(usersToEject, balanceHolder)
		}
	}

	if len(usersToEject) == 0 {
		_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getNilOffboardMessage(), &msg.Msg)...)
		return err
	}

	if len(usersToEject) == len(balanceHolders) {
		message := "attempting to delete all PBX balance holders while running offboard command"
		logrus.Error(message)
		return p.handleError(msg, errors.New(message))
	}

	err = p.PBX.Offboard(usersToEject)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(p.getOffboardMessage(usersToEject), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleTotalPBXQuery(msg *slack.MessageEvent, conversation *slack.Channel) error {
	totalPBX, err := p.PBX.GetTotalPBX()
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getTotalPBXMessage(totalPBX), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleLeaderboard(msg *slack.MessageEvent, conversation *slack.Channel) error {
	topBalanceHolders, err := p.PBX.GetBalanceHoldersFromRange(0, 9)
	if err != nil {
		return p.handleError(msg, err)
	}

	message := "Top PBX Holders \n ```"

	for i, balanceHolder := range topBalanceHolders {
		message += fmt.Sprintf("%d : %s (%d) \n", i+1, p.Utils.GetSlackUserName(balanceHolder.User), balanceHolder.Score)
	}

	message += "```\n"

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(message, &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleRouletteLeaderboard(msg *slack.MessageEvent, conversation *slack.Channel) error {
	winners, losers, err := p.PBX.GetRouletteLeaderboard(5)
	if err != nil {
		return p.handleError(msg, err)
	}

	message := ""

	if len(winners) > 0 {
		message = message + "Roulette Winners \n ```"

		for i, winner := range winners {
			message += fmt.Sprintf("%d : %s (%d) \n", i+1, p.Utils.GetSlackUserName(winner.UserID), winner.Winnings)
		}

		message += "```\n"
	}

	if len(losers) > 0 {
		message = message + "Roulette Losers \n ```"

		for i, loser := range losers {
			message += fmt.Sprintf("%d : %s (%d) \n", i+1, p.Utils.GetSlackUserName(loser.UserID), loser.Winnings)
		}

		message += "```\n"
	}

	if message == "" {
		message = "Roulette leaderboard empty"
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(message, &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleStonkQuote(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	quoteArgs, ok := argMap[StonkQuote]
	if !ok || quoteArgs == "" {
		return ArgValidationCountError
	}

	symbol, err := extractStonkQuoteArgs(utils.SplitTailArgs(quoteArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	quote, err := p.PBX.GetStonkQuote(symbol)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getStonkQuoteMessage(symbol, quote), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleStonkGet(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	quoteArgs, ok := argMap[StonkGet]
	if !ok || quoteArgs == "" {
		return ArgValidationCountError
	}

	symbol, amount, err := extractStonkGetArgs(utils.SplitTailArgs(quoteArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	purchasePrice, err := p.PBX.GetStonk(msg.User, symbol, amount)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getStonkGetMessage(symbol, amount, purchasePrice), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleStonkSell(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	quoteArgs, ok := argMap[StonkSell]
	if !ok || quoteArgs == "" {
		return ArgValidationCountError
	}

	symbol, err := extractStonkSellArgs(utils.SplitTailArgs(quoteArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	winnings, winningsSubInvestment, err := p.PBX.SellStonk(msg.User, symbol)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getStonkSellMessage(symbol, winnings, winningsSubInvestment), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleStonkPortfolio(msg *slack.MessageEvent, conversation *slack.Channel) error {
	portfolio, err := p.PBX.GetStonkPortfolio(msg.User)
	if err != nil {
		return p.handleError(msg, err)
	}

	message := "No symbols in portfolio"

	if len(portfolio) > 0 {
		message = "Portfolio \n ```"

		for i, item := range portfolio {
			message += fmt.Sprintf("%d : %d PBX -> %s @ %d cents cost basis \n", i+1, item.PBX, item.Symbol, item.CostBasisCents)
		}

		message += "```\n\n"
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(message, &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleStonkLeaderboard(msg *slack.MessageEvent, conversation *slack.Channel) error {
	winners, losers, err := p.PBX.GetStonkLeaderboard(5)
	if err != nil {
		return p.handleError(msg, err)
	}

	message := ""

	if len(winners) > 0 {
		message = message + "Stonk Winners \n ```"

		for i, winner := range winners {
			message += fmt.Sprintf("%d : %s (%d) \n", i+1, p.Utils.GetSlackUserName(winner.UserID), winner.Winnings)
		}

		message += "```\n"
	}

	if len(losers) > 0 {
		message = message + "Stonk Losers \n ```"

		for i, loser := range losers {
			message += fmt.Sprintf("%d : %s (%d) \n", i+1, p.Utils.GetSlackUserName(loser.UserID), loser.Winnings)
		}

		message += "```\n"
	}

	if message == "" {
		message = "Stonk leaderboard empty"
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(message, &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleRoulette(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	rouletteArgs, ok := argMap[Roulette]
	if !ok || rouletteArgs == "" {
		return ArgValidationCountError
	}

	condition, amount, spins, err := extractRouletteArgs(utils.SplitTailArgs(rouletteArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	if spins > maxRouletteSpins {
		return p.handleError(msg, RouletteMaxSpinsError)
	}

	results, err := p.PBX.Roulette(msg.User, condition, amount, spins)

	if err != nil {
		return p.handleError(msg, err)
	}

	newBalance, err := p.PBX.GetBalance(msg.User)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(p.GetRouletteMessage(results, newBalance, amount*spins), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleAcceptLoan(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	acceptLoanArgs, ok := argMap[AcceptLoan]
	if !ok || acceptLoanArgs == "" {
		return ArgValidationCountError
	}

	contractID, err := extractContractIDArgs(utils.SplitTailArgs(acceptLoanArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	err = p.PBX.AcceptLoanRequest(msg.User, contractID)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getAcceptLoanMessage(), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleRequestLoan(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	requestLoanArgs, ok := argMap[RequestLoan]
	if !ok || requestLoanArgs == "" {
		return ArgValidationCountError
	}

	loanBenefactor, amount, vig, periodDays, err := extractRequestLoanArgs(utils.SplitTailArgs(requestLoanArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	contractID, err := p.PBX.CreateLoanContractRequest(msg.User, loanBenefactor, amount, vig, periodDays)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getContractIDMessage(contractID), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleDistributeWealth(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	distributeWealthArgs, ok := argMap[DistributeWealth]
	if !ok || distributeWealthArgs == "" {
		return ArgValidationCountError
	}

	amount, err := extractDistributeWealthArgs(utils.SplitTailArgs(distributeWealthArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	err = p.PBX.TransferBalanceAll(msg.User, amount)

	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getTransferBalanceMessage(), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleBalanceTransfer(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	transferArgs, ok := argMap[Transfer]
	if !ok || transferArgs == "" {
		return ArgValidationCountError
	}

	toUser, amount, err := extractTransferBalanceArgs(utils.SplitTailArgs(transferArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	err = p.PBX.TransferBalance(msg.User, toUser, amount, false)

	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getTransferBalanceMessage(), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleFreeGift(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	giftArgs, ok := argMap[FreeGift]
	if !ok || giftArgs == "" {
		return ArgValidationCountError
	}

	toUser, err := extractGiftArgs(utils.SplitTailArgs(giftArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	timeToNextGift, err := p.PBX.GiveFreeGift(msg.User, toUser)

	if err == FreeGiftCooldownError {
		return p.handleFreeGiftCooldownError(msg, timeToNextGift)
	} else if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getFreeGiftMessage(), &msg.Msg)...)
	return err
}

func extractGiftArgs(args []string) (toUser string, err error) {
	//we expect text to be something like "-fg @michael"
	if len(args) != 1 {
		return "", ArgValidationCountError
	}

	toUserRaw := args[0]
	submatches := userArgRegexp.FindStringSubmatch(toUserRaw)

	if len(submatches) != 2 {
		return "", ArgValidationUnexpectedUserFormatError
	}

	toUser = submatches[1]
	if toUser == "" {
		return "", ArgValidationUnexpectedUserFormatError
	}

	return toUser, nil
}

func extractStonkQuoteArgs(args []string) (symbol string, err error) {
	//we expect text to be something like "-sq MSFT"
	if len(args) != 1 {
		return "", ArgValidationCountError
	}

	return strings.ToUpper(args[0]), nil
}

func extractStonkSellArgs(args []string) (symbol string, err error) {
	//we expect text to be something like "-ss MSFT"
	if len(args) != 1 {
		return "", ArgValidationCountError
	}

	return strings.ToUpper(args[0]), nil
}

func extractStonkGetArgs(args []string) (symbol string, amount int64, err error) {
	//we expect text to be something like "-sg MSFT 100"
	if len(args) != 2 {
		return "", 0, ArgValidationCountError
	}

	amountRaw := args[1]
	amount, err = strconv.ParseInt(amountRaw, 10, 64)
	if err != nil {
		return "", 0, err
	}

	return strings.ToUpper(args[0]), amount, nil
}

func extractContractIDArgs(args []string) (contractID string, err error) {
	//we expect text to be something like "-al 123-456-789"
	if len(args) != 1 {
		return "", ArgValidationCountError
	}

	contractID = args[0]

	return contractID, nil
}

func extractDistributeWealthArgs(args []string) (amount int64, err error) {
	//we expect text to be something like "-dw 10"
	if len(args) != 1 {
		return 0, ArgValidationCountError
	}

	amountRaw := args[0]
	amount, err = strconv.ParseInt(amountRaw, 10, 64)
	if err != nil {
		return 0, err
	}

	return amount, nil
}

func extractRouletteArgs(args []string) (condition string, amount int64, spins int64, err error) {
	//we expect text to be something like "--roulette red 10"
	if len(args) < 2 {
		return "", 0, 0, ArgValidationCountError
	}

	condition = args[0]

	amountRaw := args[1]
	amount, err = strconv.ParseInt(amountRaw, 10, 64)
	if err != nil {
		return "", 0, 0, err
	}

	if len(args) >= 3 {
		spinsRaw := args[2]
		spins, err = strconv.ParseInt(spinsRaw, 10, 64)
		if err != nil {
			return "", 0, 0, err
		}
	} else {
		spins = 1
	}

	return condition, amount, spins, nil
}

func extractRequestLoanArgs(args []string) (fromUser string, amount int64, vig int64, periodDays int64, err error) {
	//we expect text to be something like "-rl @michael 100 10 5"
	if len(args) != 4 {
		return "", 0, 0, 0, ArgValidationCountError
	}

	fromUserRaw := args[0]
	submatches := userArgRegexp.FindStringSubmatch(fromUserRaw)

	if len(submatches) != 2 {
		return "", 0, 0, 0, ArgValidationUnexpectedUserFormatError
	}

	fromUser = submatches[1]
	if fromUser == "" {
		return "", 0, 0, 0, ArgValidationUnexpectedUserFormatError
	}

	amountRaw := args[1]
	amount, err = strconv.ParseInt(amountRaw, 10, 64)
	if err != nil {
		return "", 0, 0, 0, err
	}

	vigRaw := args[2]
	vig, err = strconv.ParseInt(vigRaw, 10, 64)
	if err != nil {
		return "", 0, 0, 0, err
	}

	periodDaysRaw := args[3]
	periodDays, err = strconv.ParseInt(periodDaysRaw, 10, 64)
	if err != nil {
		return "", 0, 0, 0, err
	}

	return fromUser, amount, vig, periodDays, nil
}

func extractTransferBalanceArgs(args []string) (toUser string, amount int64, err error) {
	//we expect text to be something like "--transfer @michael 10"
	if len(args) != 2 {
		return "", 0, ArgValidationCountError
	}

	toUserRaw := args[0]
	submatches := userArgRegexp.FindStringSubmatch(toUserRaw)

	if len(submatches) != 2 {
		return "", 0, ArgValidationUnexpectedUserFormatError
	}

	toUser = submatches[1]
	if toUser == "" {
		return "", 0, ArgValidationUnexpectedUserFormatError
	}

	amountRaw := args[1]
	amount, err = strconv.ParseInt(amountRaw, 10, 64)
	if err != nil {
		return "", 0, err
	}

	return toUser, amount, nil
}

func (p *ProcessorImpl) handleBalanceQuery(msg *slack.MessageEvent, conversation *slack.Channel) error {
	balance, err := p.PBX.GetBalance(msg.User)

	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getBalanceMessage(balance), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleGetLoans(msg *slack.MessageEvent, conversation *slack.Channel) error {
	recipientLoans, benefactorLoans, err := p.PBX.GetLoansForUser(msg.User)
	if err != nil {
		return p.handleError(msg, err)
	}

	message := ""

	if len(recipientLoans) > 0 {
		message = "Loans payable \n ```"

		for i, loan := range recipientLoans {
			dueDate := time.Unix(loan.LoanDueAt, 0)

			message += fmt.Sprintf("%d : to %s for %d PBX due on %s\n", i+1, p.Utils.GetSlackUserName(loan.OtherParty), loan.Balance, dueDate)
		}

		message += "```\n\n"
	}

	if len(benefactorLoans) > 0 {
		message = "Loans receivable \n ```"

		for i, loan := range benefactorLoans {
			dueDate := time.Unix(loan.LoanDueAt, 0)

			message += fmt.Sprintf("%d : from %s for %d PBX due on %s\n", i+1, p.Utils.GetSlackUserName(loan.OtherParty), loan.Balance, dueDate)
		}

		message += "```\n"
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getOutstandingLoansMessage(message), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleRequestMatchRPS(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	if conversation.IsIM {
		return RPSMatchActionNotPublicError
	}

	requestRPSMatchArgs, ok := argMap[RequestMatch]
	if !ok || requestRPSMatchArgs == "" {
		return ArgValidationCountError
	}

	wager, gamesNum, err := extractRequestMatchRPSArgs(utils.SplitTailArgs(requestRPSMatchArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	contractID, err := p.PBX.CreateRPSMatchContractRequest(*msg, wager, gamesNum)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getContractIDMessage(contractID), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleAcceptMatchRPS(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	if conversation.IsIM {
		return RPSMatchActionNotPublicError
	}

	acceptRPSMatchArgs, ok := argMap[AcceptMatch]
	if !ok || acceptRPSMatchArgs == "" {
		return ArgValidationCountError
	}

	contractID, err := extractAcceptMatchRPSArgs(utils.SplitTailArgs(acceptRPSMatchArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	contract, err := p.PBX.AcceptRPSMatchRequest(*msg, msg.User, contractID)
	if err != nil {
		return p.handleError(msg, err)
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getAcceptRPSMatchMessage(contractID, contract.GamesNum), getOriginalThreadMessage(contract))...)
	return err
}

func (p *ProcessorImpl) handlePlayMatchRPS(msg *slack.MessageEvent, conversation *slack.Channel, argMap map[models.Action]string) error {
	if !conversation.IsIM {
		return RPSMatchActionIsPublicError
	}

	playRPSMatchArgs, ok := argMap[PlayMatch]
	if !ok || playRPSMatchArgs == "" {
		return ArgValidationCountError
	}

	contractID, moves, err := extractPlayMatchRPSArgs(utils.SplitTailArgs(playRPSMatchArgs))
	if err != nil {
		return p.handleError(msg, err)
	}

	//TODO: remove
	logrus.WithFields(logrus.Fields{
		"move":       moves,
		"contractID": contractID,
	}).Info("about to PlayRPSMatch")

	matchContract, err := p.PBX.PlayRPSMatch(contractID, msg.User, moves)
	if err != nil {
		return p.handleError(msg, err)
	}

	//TODO: remove
	logrus.WithField("matchContract", matchContract).Info("returned from PlayRPSMatch")

	matchOutcome := matchContract.Outcome

	//If we only have moves from one of the players so far
	if !matchOutcome.IsMatchFinished {
		var otherPlayer string
		if msg.User == matchOutcome.Host {
			otherPlayer = matchOutcome.Challenger
		} else {
			otherPlayer = matchOutcome.Host
		}

		otherPlayerSlackName := p.Utils.GetSlackUserName(otherPlayer)

		_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getRPSMatchWaitingOnOtherPlayerMovesMessage(otherPlayerSlackName, matchContract.OriginalChannel), &msg.Msg)...)
		if err != nil {
			return err
		}

		return nil
	}

	if len(matchOutcome.RPSGameOutcomes) < 1 {
		return RPSMatchNoOutcomesError
	}

	hostSlackName := p.Utils.GetSlackUserName(matchOutcome.Host)
	challengerSlackName := p.Utils.GetSlackUserName(matchOutcome.Challenger)

	//Display message in DM to go watch the game in the channel
	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getRPSPlayMatchGoWatchGameMessage(matchContract.OriginalChannel), &msg.Msg)...)
	if err != nil {
		return err
	}

	//Display a header to announce the match
	_, _, err = p.SlackClient.PostMessage(matchContract.OriginalChannel, utils.ToSlackMsgOption(getRPSPlayMatchHeaderMessage(hostSlackName, challengerSlackName, matchOutcome.Wager), getOriginalThreadMessage(matchContract))...)
	if err != nil {
		return err
	}

	for _, gameOutcome := range matchOutcome.RPSGameOutcomes {
		//Display each outcome of each game in the match after a short delay
		time.Sleep(time.Second)

		//TODO: Once multiple games per match are supported, print incremental score after each game, rather than just at the end of the match
		_, _, err = p.SlackClient.PostMessage(matchContract.OriginalChannel, utils.ToSlackMsgOption(getRPSPlayGameOutcomeMessage(hostSlackName, challengerSlackName, gameOutcome), getOriginalThreadMessage(matchContract))...)
		if err != nil {
			return err
		}
	}

	err = p.PBX.RPSWagerPayout(matchOutcome)
	if err != nil {
		return RPSMatchWagerPayoutError
	}

	//Display a conclusion congratulating the winner on their spoils
	_, _, err = p.SlackClient.PostMessage(matchContract.OriginalChannel, utils.ToSlackMsgOption(getRPSPlayMatchOutcomeMessage(hostSlackName, challengerSlackName, matchOutcome), getOriginalThreadMessage(matchContract))...)
	return err
}

func (p *ProcessorImpl) handleGetMatchesRPS(msg *slack.MessageEvent, conversation *slack.Channel) error {
	hostMatches, challengerMatches, err := p.PBX.GetRPSMatchesForUser(msg.User)
	if err != nil {
		return p.handleError(msg, err)
	}

	message := ""

	if len(challengerMatches) > 0 {
		message = "RPS Matches As Challenger \n ```"

		for i, match := range challengerMatches {
			message += fmt.Sprintf("%d : Against %s for %d PBX (%s)\n", i+1, p.Utils.GetSlackUserName(match.Host), match.Wager, match.ContractID)
		}

		message += "```\n\n"
	}

	if len(hostMatches) > 0 {
		message = "RPS Matches As Host \n ```"

		for i, match := range hostMatches {
			message += fmt.Sprintf("%d : Against %s for %d PBX (%s)\n", i+1, p.Utils.GetSlackUserName(match.Challenger), match.Wager, match.ContractID)
		}

		message += "```\n"
	}

	_, _, err = p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getRPSMatchesMessage(message), &msg.Msg)...)
	return err
}

func extractMatchType(args []string) MatchType {
	if len(args) < 1 {
		return invalid
	}

	switch MatchType(args[0]) {
	case rps:
		return rps
	}

	return invalid
}

func extractRequestMatchRPSArgs(args []string) (wager int64, gamesNum int64, err error) {
	//we expect text to be something like "-rg rps 100 10"
	//we ignore the rps arg since we already dealt with it earlier
	if len(args) != 2 && len(args) != 3 {
		return 0, 0, ArgValidationCountError
	}

	wagerRaw := args[1]
	wager, err = strconv.ParseInt(wagerRaw, 10, 64)
	if err != nil {
		return 0, 0, err
	}

	if len(args) == 3 {
		gamesNum, err = strconv.ParseInt(args[2], 10, 64)
		if err != nil {
			return 0, 0, err
		}
	} else {
		//default to 1 move per match
		gamesNum = 1
	}

	return wager, gamesNum, nil
}

func extractAcceptMatchRPSArgs(args []string) (contractID string, err error) {
	//we expect text to be something like "-ag rps e5ea201a-d160-4f60-8507-3b5cd5afe10e"
	//we ignore the rps arg since we already dealt with it earlier
	if len(args) != 2 {
		return "", ArgValidationCountError
	}

	contractIDRaw := args[1]
	contractID = strings.TrimSpace(contractIDRaw)

	return contractID, nil
}

func extractPlayMatchRPSArgs(args []string) (contractID string, moves []string, err error) {
	//we expect text to be something like "-pg rps e5ea201a-d160-4f60-8507-3b5cd5afe10e R P S"
	//we ignore the rps arg since we already dealt with it earlier
	if len(args) <= 3 {
		return "", nil, ArgValidationCountError
	}

	contractIDRaw := args[1]
	contractID = strings.TrimSpace(contractIDRaw)

	movesRaw := args[2:]

	moves = make([]string, 0, len(movesRaw))
	for _, moveRaw := range movesRaw {
		move := ""
		switch strings.ToLower(strings.TrimSpace(moveRaw)) {
		case "rock":
			fallthrough
		case "r":
			move = "rock"
		case "paper":
			fallthrough
		case "p":
			move = "paper"
		case "scissors":
			fallthrough
		case "s":
			move = "scissors"
		default:
			return "", nil, RPSMatchInvalidMoveError
		}

		moves = append(moves, move)
	}

	return contractID, moves, nil
}

func (p *ProcessorImpl) handleError(msg *slack.MessageEvent, originalErr error) error {
	var clientErr error

	switch errors.Cause(originalErr) {
	case InsufficientBalanceError,
		IllegalRecipientError,
		UserNotOnboardedError,
		ArgValidationCountError,
		ArgValidationUnexpectedUserFormatError,
		UseBalanceOutOfRangeError,
		TransferBalanceSelfRecipientError,
		FreeGiftSelfRecipientError,
		FreeGiftCooldownError,
		BenefactorLockAcquisitionError,
		RecipientLockAcquisitionError,
		RouletteInvalidPocketError,
		RouletteMaxSpinsError,
		BenefactorBalanceQueryError,
		LoanNotFoundError,
		LoanPayoffTooLargeError,
		LoanRecipientMaxContractsError,
		LoanMinVigOutOfRangeError,
		LoanMinOutOfRangeError,
		LoanPeriodOutOfRangeError,
		LoanTotalValueOutOfRangeError,
		LoanBenefactorMismatchError,
		LoanBenefactorSelfError,
		RecipientBalanceQueryError,
		InvalidMatchTypeError,
		RPSMatchActionNotPublicError,
		RPSMatchActionIsPublicError,
		RPSMatchHostSelfError,
		RPSGamesExceedMaximumError,
		RPSGamesBelowMinimumError,
		RPSMatchChallengerAlreadyFoundError,
		RPSMatchNotInOriginalThreadError,
		RPSMatchNotFoundError,
		RPSMatchWagerTooLowError,
		RPSMatchAlreadyEndedError,
		RPSMatchInvalidPlayerError,
		RPSMatchInvalidMovesNumberPerPlayerError,
		RPSPlayerAlreadyMovedError,
		RPSMatchInvalidMoveError,
		RPSMatchInvalidNumberOfMovesError,
		RPSMatchNoOutcomesError,
		RPSMatchWagerPayoutError,
		RPSHostLockAcquisitionError,
		RPSChallengerLockAcquisitionError,
		RPSGameLockAcquisitionError,
		RPSHostBalanceQueryError,
		RPSChallengerBalanceQueryError,
		StonkDoesNotExist,
		StonkHasNoValue,
		StonkNotOwned:
		clientErr = originalErr
	default:
		clientErr = ServerError
	}

	logrus.WithField("msg", msg).Error(originalErr)

	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getErrorMessage(clientErr), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleFreeGiftCooldownError(msg *slack.MessageEvent, timeToNextGift time.Duration) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getFreeGiftCooldownMessage(timeToNextGift), &msg.Msg)...)
	return err
}

func getErrorMessage(err error) string {
	return fmt.Sprintf("Error: %s", err.Error())
}

func getOnboardMessage(usersOnboardedCount int) string {
	return fmt.Sprintf("Successfully onboarded %d user(s)", usersOnboardedCount)
}

func getNilOffboardMessage() string {
	return "No users to offboard"
}

func (p *ProcessorImpl) getOffboardMessage(usersEjected []BalanceHolder) string {
	var users []string
	balanceDistributed := int64(0)

	for _, user := range usersEjected {
		userName := p.Utils.GetSlackUserName(user.User)
		users = append(users, userName)
		balanceDistributed = balanceDistributed + user.Score
	}

	return fmt.Sprintf("Users Ejected: [%s]\nBalance Distributed: %d", strings.Join(users, ","), balanceDistributed)
}

func getTotalPBXMessage(totalPBX int64) string {
	return fmt.Sprintf("Total PBX in circulation: %d", totalPBX)
}

func getFreeGiftCooldownMessage(timeToNextGift time.Duration) string {
	return fmt.Sprintf("Failed to give free gift - %s remaining on cooldown", timeToNextGift)
}

func getFreeGiftMessage() string {
	return "Success giving free gift"
}

func (p *ProcessorImpl) GetRouletteMessage(results []RouletteResult, newBalance int64, totalBet int64) string {
	var totalWinnings int64 = 0
	var totalWins int64 = 0
	var totalLosses int64 = 0
	var rounds []string

	for _, result := range results {
		totalWinnings += result.Winnings

		if result.Winnings > 0 {
			totalWins += 1
		} else {
			totalLosses += 1
		}

		roundMessage := fmt.Sprintf("Spin: %d", result.Pocket)
		if result.LostBetDistribution > 0 {
			roundMessage += fmt.Sprintf(". %d PBX -> %s", result.LostBetDistribution, p.Utils.GetSlackUserName(result.RecipientOfLostBet.User))
		}
		rounds = append(rounds, roundMessage)
	}

	message := fmt.Sprintf("W: %d L: %d \n", totalWins, totalLosses)
	netWinnings := totalWinnings - totalBet

	if netWinnings > 0 {
		message += fmt.Sprintf("Net Winnings: %d :eddiew:\n", netWinnings)
	} else {
		message += fmt.Sprintf("Net Losses: %d :eddieforkicorn:\n", netWinnings)
	}

	message += fmt.Sprintf("New Balance: %d PBX \n", newBalance)

	if len(rounds) > 0 {
		message += "\nRounds \n"
		message += strings.Join(rounds, "\n")
	}

	return message
}

func getAcceptLoanMessage() string {
	return "Loan Accepted :moneybadgerclose:"
}

func getOutstandingLoansMessage(message string) string {
	if message == "" {
		return "No outstanding loans"
	}

	return message
}

func getRPSMatchesMessage(message string) string {
	if message == "" {
		return "No RPS matches running currently"
	}

	return message
}

func getAcceptRPSMatchMessage(contractID string, gamesNum int64) string {
	movesStr := ""
	for i := 0; int64(i) < gamesNum; i++ {
		movesStr += "<move> "
	}
	return fmt.Sprintf("Match Accepted! Players, make moves in a DM with bits-bot using the template: `./pbx -pm rps %s %s`", contractID, movesStr)
}

func getOriginalThreadMessage(contract RPSMatchContract) *slack.Msg {
	return &slack.Msg{
		Timestamp:       contract.MsgTimestamp,
		ThreadTimestamp: contract.MsgThreadTimestamp,
	}
}

func getRPSMatchWaitingOnOtherPlayerMovesMessage(otherPlayer string, channel string) string {
	return fmt.Sprintf("Waiting on %s to complete their move(s). Go watch for the game to be played in your thread in %s", otherPlayer, utils.GetSlackChannelName(channel))
}

func getRPSPlayMatchGoWatchGameMessage(channel string) string {
	return fmt.Sprintf("Go watch the outcome of your game in %s", utils.GetSlackChannelName(channel))
}

func getRPSPlayMatchHeaderMessage(host string, challenger string, wager int64) string {
	return fmt.Sprintf("Now playing %s's game against %s. Winner will receive %d pbx!", host, challenger, 2*wager)
}

func getRPSPlayGameOutcomeMessage(host string, challenger string, gameOutcome RPSGameOutcome) string {
	if gameOutcome.WasATie {
		return fmt.Sprintf("There was a tie! Both players used %s", gameOutcome.HostMove)
	} else if gameOutcome.DidHostWin {
		return fmt.Sprintf("%s's %s beat %s's %s", host, gameOutcome.HostMove, challenger, gameOutcome.ChallengerMove)
	}

	return fmt.Sprintf("%s's %s beat %s's %s", challenger, gameOutcome.ChallengerMove, host, gameOutcome.HostMove)
}

func getRPSPlayMatchOutcomeMessage(host string, challenger string, outcome RPSMatchOutcome) string {
	wager := outcome.Wager

	if outcome.WasATie {
		return fmt.Sprintf("The match was a tie %d - %d. Both players will receive their %d pbx wager back.", outcome.HostScore, outcome.ChallengerScore, wager)
	}

	wager = 2 * wager // winner wins both the host's and the challenger's wagers

	wagerText := fmt.Sprintf("%d", wager)
	if outcome.WasLuckyMatch {
		wagerText = fmt.Sprintf("a lucky %d", wager*luckyMatchPayoutFactor)
	}

	if outcome.DidHostWin {
		return fmt.Sprintf("%s beat %s with a score of %d - %d. %s received %s pbx for winning!", host, challenger, outcome.HostScore, outcome.ChallengerScore, host, wagerText)
	}

	return fmt.Sprintf("%s beat %s with a score of %d - %d. %s received %s pbx for winning!", challenger, host, outcome.ChallengerScore, outcome.HostScore, challenger, wagerText)
}

func getContractIDMessage(contractID string) string {
	return fmt.Sprintf("Contract request created: %s", contractID)
}

func getTransferBalanceMessage() string {
	return "Success transferring balance"
}

func getBalanceMessage(balance int64) string {
	return fmt.Sprintf("%d PBX", balance)
}

func getStonkQuoteMessage(symbol string, quote float64) string {
	return fmt.Sprintf("%s : $%.2f", symbol, quote)
}

func getStonkGetMessage(symbol string, amount int64, purchasePrice float64) string {
	return fmt.Sprintf("Success wagering %d PBX on %s @ $%.2f", amount, symbol, purchasePrice)
}

func getStonkSellMessage(symbol string, winnings, winningsSubInvestment int64) string {
	return fmt.Sprintf("Sold %s for %d PBX (%d relative to initial investment)", symbol, winnings, winningsSubInvestment)
}

func getInvalidPBXMessage(errorMessage string) string {
	return fmt.Sprintf("```Invalid pbx command: %s\n\n%s```", errorMessage, usageMessage)
}

func (p *ProcessorImpl) handleInvalid(msg *slack.MessageEvent, conversation *slack.Channel, errorMessage string) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getInvalidPBXMessage(errorMessage), &msg.Msg)...)
	return err
}

func getUsageMessage(msg string) string {
	return fmt.Sprintf("```%s```", msg)
}

func (p *ProcessorImpl) handleRouletteHelp(msg *slack.MessageEvent, conversation *slack.Channel) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(rouletteUsageMessage), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleStonkHelp(msg *slack.MessageEvent, conversation *slack.Channel) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(stonkUsageMessage), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleLoanHelp(msg *slack.MessageEvent, conversation *slack.Channel) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(loanUsageMessage), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleMatchHelp(msg *slack.MessageEvent, conversation *slack.Channel) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(matchUsageMessage), &msg.Msg)...)
	return err
}

func (p *ProcessorImpl) handleHelp(msg *slack.MessageEvent, conversation *slack.Channel) error {
	_, _, err := p.SlackClient.PostMessage(msg.Channel, utils.ToSlackMsgOption(getUsageMessage(usageMessage), &msg.Msg)...)
	return err
}
