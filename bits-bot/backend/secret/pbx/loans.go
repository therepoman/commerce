package pbx

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/gogogadget/math"

	go_redis "github.com/go-redis/redis"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	redisPBXLoanRequestKey           = "$pbx$-loan-request-%s"
	loanRequestExpiration            = time.Hour
	minLoan                          = 10
	maxLoanAsPercentageOfMeanBalance = 0.5
	maxLoanPeriodDays                = 10

	maxOutstandingLoansForRecipient = 3

	redisPBXLoanMasterRecordKey = "$pbx$-loan-%s"

	redisPBXLoanRecipientSortedSetKey  = "$pbx$-loan-recipient-%s"
	redisPBXLoanBenefactorSortedSetKey = "$pbx$-loan-benefactor-%s"
	redisPBXLoanSortedSetMemberKey     = "%s.%d.%s"
)

type LoanContract struct {
	LoanRecipient   string
	LoanBenefactor  string
	Amount          int64
	Vig             int64
	RepaymentPeriod int64
	ID              string
	RequestedAt     time.Time
	AcceptedAt      time.Time
	Balance         int64
	DueAt           time.Time
	PaidOffAt       time.Time
}

type SortedSetLoanEntry struct {
	LoanDueAt  int64
	OtherParty string
	Balance    int64
	ContractID string
}

func (p *PBXImpl) CreateLoanContractRequest(loanRecipient, loanBenefactor string, amount, vig, periodDays int64) (string, error) {
	if loanRecipient == loanBenefactor {
		return "", LoanBenefactorSelfError
	}

	if periodDays < 1 || periodDays > maxLoanPeriodDays {
		return "", LoanPeriodOutOfRangeError
	}

	if vig < 1 {
		return "", LoanMinVigOutOfRangeError
	}

	if amount < minLoan {
		return "", LoanMinOutOfRangeError
	}

	balanceHolders, err := p.GetBalanceHoldersFromRange(0, -1)
	if err != nil {
		return "", err
	}

	totalPBX := sumBalances(balanceHolders)

	if totalPBX == 0 {
		return "", errors.New("0 PBX in economy, cannot issue loan")
	}

	maxLoan := p.getUserInflationShare(totalPBX, len(balanceHolders), maxLoanAsPercentageOfMeanBalance)

	if amount+vig > maxLoan {
		return "", LoanTotalValueOutOfRangeError
	}

	err = p.validateLoanRecipientEligibility(loanRecipient)
	if err != nil {
		return "", err
	}

	contractID, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	contractIDStr := contractID.String()

	contract := LoanContract{
		LoanRecipient:   loanRecipient,
		LoanBenefactor:  loanBenefactor,
		Amount:          amount,
		Vig:             vig,
		RepaymentPeriod: periodDays,
		ID:              contractIDStr,
		RequestedAt:     time.Now(),
		Balance:         amount + vig,
	}

	contractBytes, err := json.Marshal(contract)
	if err != nil {
		return "", err
	}

	err = p.RedisClient.Set(fmt.Sprintf(redisPBXLoanRequestKey, contractIDStr), string(contractBytes), loanRequestExpiration)
	if err != nil {
		return "", err
	}

	return contractIDStr, nil
}

func (p *PBXImpl) GetLoansForUser(user string) ([]SortedSetLoanEntry, []SortedSetLoanEntry, error) {
	recipientLoans, err := p.GetAllLoansForUser(redisPBXLoanRecipientSortedSetKey, user)
	if err != nil {
		return nil, nil, err
	}

	benefactorLoans, err := p.GetAllLoansForUser(redisPBXLoanBenefactorSortedSetKey, user)
	if err != nil {
		return nil, nil, err
	}

	return recipientLoans, benefactorLoans, nil
}

func (p *PBXImpl) AcceptLoanRequest(loanBenefactor, contractID string) error {
	var loanRequest LoanContract

	loanRequestStr, err := p.RedisClient.Get(fmt.Sprintf(redisPBXLoanRequestKey, contractID))

	if err == go_redis.Nil {
		return LoanNotFoundError
	} else if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(loanRequestStr), &loanRequest)
	if err != nil {
		return err
	}

	if loanBenefactor != loanRequest.LoanBenefactor {
		return LoanBenefactorMismatchError
	}

	unlockBenefactor, err := p.createTransactionLock(loanRequest.LoanBenefactor)
	if err != nil {
		return BenefactorLockAcquisitionError
	}
	defer unlockBenefactor()

	unlockRecipient, err := p.createTransactionLock(loanRequest.LoanRecipient)
	if err != nil {
		return RecipientLockAcquisitionError
	}
	defer unlockRecipient()

	benefactorBalance, err := p.GetBalance(loanRequest.LoanBenefactor)
	if err != nil {
		return BenefactorBalanceQueryError
	}

	if loanRequest.Amount > benefactorBalance {
		return InsufficientBalanceError
	}

	err = p.validateLoanRecipientEligibility(loanRequest.LoanRecipient)
	if err != nil {
		return err
	}

	loanRequest.AcceptedAt = time.Now()
	loanRequest.DueAt = loanRequest.AcceptedAt.Add(time.Hour * 24 * time.Duration(loanRequest.RepaymentPeriod))

	contractBytes, err := json.Marshal(loanRequest)
	if err != nil {
		return err
	}

	contractString := string(contractBytes)

	// create master contract record
	err = p.RedisClient.Set(fmt.Sprintf(redisPBXLoanMasterRecordKey, loanRequest.ID), contractString, 0)
	if err != nil {
		return err
	}

	// delete loan request
	_, err = p.RedisClient.Del([]string{fmt.Sprintf(redisPBXLoanRequestKey, loanRequest.ID)})
	if err != nil {
		return err
	}

	// Add loan to recipients sorted set
	err = p.RedisClient.ZIncrBy(
		fmt.Sprintf(redisPBXLoanRecipientSortedSetKey, loanRequest.LoanRecipient),
		float64(loanRequest.Balance),
		getLoanSortedSetKey(loanRequest.LoanBenefactor, loanRequest.DueAt.Unix(), loanRequest.ID))
	if err != nil {
		return err
	}

	// Add loan to benefactor sorted set
	err = p.RedisClient.ZIncrBy(
		fmt.Sprintf(redisPBXLoanBenefactorSortedSetKey, loanRequest.LoanBenefactor),
		float64(loanRequest.Balance),
		getLoanSortedSetKey(loanRequest.LoanRecipient, loanRequest.DueAt.Unix(), loanRequest.ID))
	if err != nil {
		return err
	}

	// Add loan amount to recipient
	err = p.IncrementBalanceBy(loanRequest.LoanRecipient, loanRequest.Amount, "")
	if err != nil {
		return errors.Wrap(err, "error adding PBX to recipient")
	}

	// Deduct loan amount from benefactor
	err = p.IncrementBalanceBy(loanRequest.LoanBenefactor, -loanRequest.Amount, "")
	if err != nil {
		return errors.Wrap(err, "added PBX to recipient, but failed to deduct from benefactor")
	}

	return nil
}

func (p *PBXImpl) GetAllLoansForUser(key, user string) ([]SortedSetLoanEntry, error) {
	var entries []SortedSetLoanEntry

	zs, err := p.RedisClient.ZRevRangeWithScores(fmt.Sprintf(key, user), 0, -1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRevRangeWithScores in PBX#GetAllLoansForUser")
		return nil, err
	}

	for _, z := range zs {
		key, ok := z.Member.(string)
		if !ok {
			continue
		}

		user, expiration, contractID, err := parseLoanSortedSetKey(key)
		if err != nil {
			return nil, err
		}

		entries = append(entries, SortedSetLoanEntry{
			OtherParty: user,
			LoanDueAt:  expiration,
			ContractID: contractID,
			Balance:    int64(z.Score),
		})
	}

	return entries, nil
}

func (p *PBXImpl) getLoanMasterRecord(contractID string) (LoanContract, error) {
	var loan LoanContract

	loanStr, err := p.RedisClient.Get(fmt.Sprintf(redisPBXLoanMasterRecordKey, contractID))

	if err == go_redis.Nil {
		return LoanContract{}, LoanNotFoundError
	} else if err != nil {
		logrus.WithError(err).WithField("contract_id", contractID).Error("error getting loan")
		return LoanContract{}, err
	}

	err = json.Unmarshal([]byte(loanStr), &loan)
	if err != nil {
		return LoanContract{}, err
	}

	return loan, nil
}

func (p *PBXImpl) makeLoanPayment(contractID string, amount int64) error {
	loan, err := p.getLoanMasterRecord(contractID)
	if err != nil {
		return err
	}

	if amount > loan.Balance {
		return LoanPayoffTooLargeError
	}

	err = p.TransferBalance(loan.LoanRecipient, loan.LoanBenefactor, amount, true)
	if err != nil {
		return err
	}

	loan.Balance = loan.Balance - amount

	if loan.Balance <= 0 {
		loan.PaidOffAt = time.Now()
	}

	contractBytes, err := json.Marshal(loan)
	if err != nil {
		return err
	}

	contractString := string(contractBytes)

	// update master contract record
	err = p.RedisClient.Set(fmt.Sprintf(redisPBXLoanMasterRecordKey, loan.ID), contractString, 0)
	if err != nil {
		return err
	}

	if loan.Balance <= 0 {
		// Delete loan from recipient sorted set
		err = p.RedisClient.ZRem(
			fmt.Sprintf(redisPBXLoanRecipientSortedSetKey, loan.LoanRecipient),
			getLoanSortedSetKey(loan.LoanBenefactor, loan.DueAt.Unix(), loan.ID))
		if err != nil {
			return err
		}

		// Delete loan from benefactor sorted set
		err = p.RedisClient.ZRem(
			fmt.Sprintf(redisPBXLoanBenefactorSortedSetKey, loan.LoanBenefactor),
			getLoanSortedSetKey(loan.LoanRecipient, loan.DueAt.Unix(), loan.ID))
		if err != nil {
			return err
		}
	} else {
		// Update loan in recipient sorted set
		err = p.RedisClient.ZIncrBy(
			fmt.Sprintf(redisPBXLoanRecipientSortedSetKey, loan.LoanRecipient),
			-float64(amount),
			getLoanSortedSetKey(loan.LoanBenefactor, loan.DueAt.Unix(), loan.ID))
		if err != nil {
			return err
		}

		// Update loan in benefactor sorted set
		err = p.RedisClient.ZIncrBy(
			fmt.Sprintf(redisPBXLoanBenefactorSortedSetKey, loan.LoanBenefactor),
			-float64(amount),
			getLoanSortedSetKey(loan.LoanRecipient, loan.DueAt.Unix(), loan.ID))
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *PBXImpl) possiblyMakeLoanPayment(user, benefactorAttribution string, amount int64) (newAmount int64, err error) {
	// Look up outstanding loans payable for the payment benefactor
	loans, err := p.GetAllLoansForUser(redisPBXLoanRecipientSortedSetKey, benefactorAttribution)
	if err != nil {
		return amount, err
	}

	logrus.WithField("loans", loans).WithField("user", user).Info("loans returned in possiblyMakeLoanPayment")

	for _, loan := range loans {
		if amount <= 0 {
			break
		}

		if loan.OtherParty != user {
			continue
		}

		amountToPayoff := math.MinInt64(amount, loan.Balance)

		err := p.makeLoanPayment(loan.ContractID, amountToPayoff)
		if err != nil {
			return amount, err
		}

		amount = amount - amountToPayoff
	}

	return amount, nil
}

func (p *PBXImpl) possiblyGarnishIncome(recipient string, amount int64) (newAmount int64, err error) {
	loans, err := p.GetAllLoansForUser(redisPBXLoanRecipientSortedSetKey, recipient)
	if err != nil {
		return amount, err
	}

	currentTime := time.Now().Unix()

	for _, loan := range loans {
		if amount <= 0 {
			break
		}

		if currentTime > loan.LoanDueAt {
			amountToGarnish := math.MinInt64(amount, loan.Balance)

			err := p.makeLoanPayment(loan.ContractID, amountToGarnish)
			if err != nil {
				return amount, err
			}

			amount = amount - amountToGarnish
		}
	}

	return amount, nil
}

func (p *PBXImpl) validateLoanRecipientEligibility(loanRecipient string) error {
	loans, err := p.GetAllLoansForUser(redisPBXLoanRecipientSortedSetKey, loanRecipient)
	if err != nil {
		return err
	}

	if len(loans) > maxOutstandingLoansForRecipient {
		return LoanRecipientMaxContractsError
	}

	return nil
}

func getLoanSortedSetKey(user string, expiration int64, contractID string) string {
	return fmt.Sprintf(redisPBXLoanSortedSetMemberKey, user, expiration, contractID)
}

func parseLoanSortedSetKey(key string) (user string, expiration int64, contractID string, err error) {
	parts := strings.Split(key, ".")

	if len(parts) != 3 {
		return "", 0, "", errors.New("loan sorted set key in unexpected format")
	}

	expiration, err = strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		return "", 0, "", err
	}

	return parts[0], expiration, parts[2], nil
}
