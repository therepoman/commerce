package pbx

import (
	"fmt"
	"strconv"
	"strings"

	go_redis "github.com/go-redis/redis"
	"github.com/piquette/finance-go/quote"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	redisPBXStonksLeaderboardSortedSetKey    = "$pbx$-stonks-leaderboard"
	redisPBXStonkPortfolioSortedSetKey       = "$pbx$-stonks-user-portfolio-%s"
	redisPBXStonkPortfolioSortedSetMemberKey = "%s_%d"
)

type PortfolioItem struct {
	PBX            int64
	CostBasisCents int64
	Symbol         string
}

func (p *PBXImpl) UpdateStonkLeaderboard(from string, winnings int64) error {
	return p.RedisClient.ZIncrBy(
		redisPBXStonksLeaderboardSortedSetKey,
		float64(winnings),
		from)
}

func (p *PBXImpl) GetStonkLeaderboard(n int64) (winners []LeaderboardEntry, losers []LeaderboardEntry, err error) {
	topEntries, err := p.GetTopStonkLeaderboardEntries(n)
	if err != nil {
		return nil, nil, err
	}

	var topEntriesOnlyPositive []LeaderboardEntry
	for _, entry := range topEntries {
		if entry.Winnings > 0 {
			topEntriesOnlyPositive = append(topEntriesOnlyPositive, entry)
		}
	}

	bottomEntries, err := p.GetBottomStonkLeaderboardEntries(n)
	if err != nil {
		return nil, nil, err
	}

	var bottomEntriesOnlyNegative []LeaderboardEntry
	for _, entry := range bottomEntries {
		if entry.Winnings < 0 {
			bottomEntriesOnlyNegative = append(bottomEntriesOnlyNegative, entry)
		}
	}

	return topEntriesOnlyPositive, bottomEntriesOnlyNegative, nil
}

func (p *PBXImpl) GetStonkQuote(symbol string) (float64, error) {
	q, err := quote.Get(symbol)

	if err != nil {
		return 0, err
	}

	if q == nil {
		return 0, StonkDoesNotExist
	}

	return q.Bid, nil
}

func (p *PBXImpl) GetStonk(user string, symbol string, pbxToWager int64) (purchasePrice float64, err error) {
	unlockFrom, err := p.createTransactionLock(user)
	if err != nil {
		return 0, BenefactorLockAcquisitionError
	}
	defer unlockFrom()

	benefactorBalance, err := p.GetBalance(user)
	if err != nil {
		return 0, BenefactorBalanceQueryError
	}

	if pbxToWager > benefactorBalance {
		return 0, InsufficientBalanceError
	}

	currentPrice, err := p.GetStonkQuote(symbol)
	if err != nil {
		return 0, err
	}

	currentPriceCents := int64(currentPrice * 100)
	if currentPriceCents == 0 {
		return 0, StonkHasNoValue
	}

	err = p.IncrementBalanceBy(user, -pbxToWager, "")
	if err != nil {
		return 0, errors.Wrap(err, "error deducting PBX in GetStonk")
	}

	portfolio, err := p.GetStonkPortfolio(user)
	if err != nil {
		return 0, err
	}

	var previousInvestment *PortfolioItem
	for _, item := range portfolio {
		if item.Symbol == symbol {
			previousInvestment = &item
		}
	}

	totalCostBasis := currentPriceCents
	totalPBXToWager := pbxToWager
	if previousInvestment != nil {
		// delete old entry from portfolio, as the key includes cost basis
		err = p.RedisClient.ZRem(
			fmt.Sprintf(redisPBXStonkPortfolioSortedSetKey, user),
			getStonksPortfolioSortedSetKey(previousInvestment.Symbol, previousInvestment.CostBasisCents),
		)
		if err != nil {
			return 0, err
		}

		totalCostBasis = ((previousInvestment.PBX * previousInvestment.CostBasisCents) + (pbxToWager * currentPriceCents)) / (previousInvestment.PBX + pbxToWager)
		totalPBXToWager = pbxToWager + previousInvestment.PBX
	}

	// add symbol to portfolio
	err = p.RedisClient.ZIncrBy(
		fmt.Sprintf(redisPBXStonkPortfolioSortedSetKey, user),
		float64(totalPBXToWager),
		getStonksPortfolioSortedSetKey(symbol, totalCostBasis),
	)
	if err != nil {
		return 0, err
	}

	return currentPrice, nil
}

func (p *PBXImpl) SellStonk(user string, symbol string) (winnings int64, winningsSubInvestment int64, err error) {
	unlockFrom, err := p.createTransactionLock(user)
	if err != nil {
		return 0, 0, BenefactorLockAcquisitionError
	}
	defer unlockFrom()

	portfolio, err := p.GetStonkPortfolio(user)
	if err != nil {
		return 0, 0, err
	}

	for _, item := range portfolio {
		if item.Symbol != symbol {
			continue
		}

		currentPrice, err := p.GetStonkQuote(symbol)
		if err != nil {
			return 0, 0, err
		}

		currentPriceCents := int64(100 * currentPrice)
		if currentPriceCents == 0 {
			return 0, 0, StonkHasNoValue
		}

		winnings := int64(float64(currentPriceCents) / float64(item.CostBasisCents) * float64(item.PBX))
		winningsSubInvestment := winnings - item.PBX

		err = p.IncrementBalanceBy(user, winnings, "")
		if err != nil {
			return 0, 0, errors.Wrap(err, "failed to award stonk winnings")
		}

		err = p.UpdateStonkLeaderboard(user, winningsSubInvestment)
		if err != nil {
			return 0, 0, errors.Wrap(err, "failed to increment stonk leaderboard")
		}

		// Delete symbol from portfolio
		err = p.RedisClient.ZRem(
			fmt.Sprintf(redisPBXStonkPortfolioSortedSetKey, user),
			getStonksPortfolioSortedSetKey(item.Symbol, item.CostBasisCents),
		)

		if err != nil {
			return 0, 0, err
		}

		return winnings, winningsSubInvestment, nil
	}

	return 0, 0, StonkNotOwned
}

func (p *PBXImpl) GetStonkPortfolio(user string) ([]PortfolioItem, error) {
	var entries []PortfolioItem

	zs, err := p.RedisClient.ZRevRangeWithScores(fmt.Sprintf(redisPBXStonkPortfolioSortedSetKey, user), 0, -1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRevRangeWithScores in PBX#GetAllLoansForUser")
		return nil, err
	}

	for _, z := range zs {
		key, ok := z.Member.(string)
		if !ok {
			continue
		}

		symbol, costBasis, err := parseStonkPortfolioSortedSetKey(key)
		if err != nil {
			return nil, err
		}

		entries = append(entries, PortfolioItem{
			CostBasisCents: costBasis,
			Symbol:         symbol,
			PBX:            int64(z.Score),
		})
	}

	return entries, nil
}

func (p *PBXImpl) GetTopStonkLeaderboardEntries(n int64) ([]LeaderboardEntry, error) {
	var entries []LeaderboardEntry

	zs, err := p.RedisClient.ZRevRangeWithScores(redisPBXStonksLeaderboardSortedSetKey, 0, n-1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRevRangeWithScores in PBX#GetTopStonkLeaderboardEntries")
		return nil, err
	}

	for _, z := range zs {
		user, ok := z.Member.(string)
		if !ok {
			continue
		}

		entries = append(entries, LeaderboardEntry{
			UserID:   user,
			Winnings: int64(z.Score),
		})
	}

	return entries, nil
}

func (p *PBXImpl) GetBottomStonkLeaderboardEntries(n int64) ([]LeaderboardEntry, error) {
	var entries []LeaderboardEntry

	zs, err := p.RedisClient.ZRangeWithScores(redisPBXStonksLeaderboardSortedSetKey, 0, n-1)

	if err == go_redis.Nil {
		return nil, nil
	} else if err != nil {
		logrus.WithError(err).Error("error querying redis ZRangeWithScores in PBX#GetBottomStonkLeaderboardEntries")
		return nil, err
	}

	for _, z := range zs {
		user, ok := z.Member.(string)
		if !ok {
			continue
		}

		entries = append(entries, LeaderboardEntry{
			UserID:   user,
			Winnings: int64(z.Score),
		})
	}

	return entries, nil
}

func getStonksPortfolioSortedSetKey(symbol string, costBasis int64) string {
	return fmt.Sprintf(redisPBXStonkPortfolioSortedSetMemberKey, symbol, costBasis)
}

func parseStonkPortfolioSortedSetKey(key string) (symbol string, costBasis int64, err error) {
	parts := strings.Split(key, "_")

	if len(parts) != 2 {
		return "", 0, errors.New("stonk portfolio sorted set key in unexpected format")
	}

	costBasis, err = strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		return "", 0, err
	}

	return parts[0], costBasis, nil
}
