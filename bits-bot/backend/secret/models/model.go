package models

type Action string

const (
	Invalid = Action("Invalid")
	Help    = Action("Help")
)
