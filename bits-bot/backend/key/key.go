package key

import (
	"crypto/sha1"
	"fmt"
	"io"
	go_strings "strings"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/lock"
	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/dynamo/key_claim"
	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

const (
	RedisKeySet      = "bits-bot-key-set"
	KeyInventoryLock = "bits-key-inventory-lock"
	KeyCountMetric   = "key-manager.available-key-count"
)

type AllocateResponse struct {
	Key                  string
	InventoryAvailable   bool
	UserEligible         bool
	DurationTillEligible time.Duration
}

type Manager interface {
	AllocateKey(slackUser string, slackName string) (AllocateResponse, error)
	GetKeyCount() (int64, error)
	IngestKeys(keys []string) error
}

type ManagerImpl struct {
	RedisClient   redis.Client       `inject:""`
	KeyClaimDAO   key_claim.DAO      `inject:""`
	LockingClient lock.LockingClient `inject:""`
	Stats         statsd.Statter     `inject:""`
}

func (m *ManagerImpl) logGaugeMetric(name string, val int64) {
	err := m.Stats.Gauge(name, val, 1.0)
	if err != nil {
		log.WithField("name", name).WithError(err).Error("error logging gauge metric")
	}
}

func (m *ManagerImpl) AllocateKey(slackUser string, slackName string) (AllocateResponse, error) {
	inventoryLock, err := m.LockingClient.ObtainLock(KeyInventoryLock)
	if err != nil {
		return AllocateResponse{}, err
	}
	defer inventoryLock.Unlock()

	latestKeyClaim, err := m.KeyClaimDAO.GetUsersLatestKeyClaim(slackUser)
	if err != nil {
		return AllocateResponse{}, err
	}

	if latestKeyClaim != nil {
		eligibleForNextKey := latestKeyClaim.ClaimTime.AddDate(0, 1, 0)

		if time.Now().Before(eligibleForNextKey) {
			durationTillEligible := eligibleForNextKey.Sub(time.Now())
			return AllocateResponse{
				UserEligible:         false,
				DurationTillEligible: durationTillEligible,
			}, nil
		}
	}

	keyCount, err := m.GetKeyCount()
	if err != nil {
		return AllocateResponse{}, err
	}

	if keyCount <= 0 {
		return AllocateResponse{
			UserEligible:       true,
			InventoryAvailable: false,
		}, nil
	}

	allocatedKey, err := m.RedisClient.SetPop(RedisKeySet)
	if err != nil {
		return AllocateResponse{}, err
	}

	m.logGaugeMetric(KeyCountMetric, keyCount-1)

	keySha1, err := sha1Hash(allocatedKey)
	if err != nil {
		return AllocateResponse{}, err
	}

	keyClaim := &key_claim.KeyClaim{
		SlackUser: slackUser,
		SlackName: slackName,
		ClaimTime: time.Now(),
		KeyHash:   keySha1,
	}
	err = m.KeyClaimDAO.CreateNew(keyClaim)
	if err != nil {
		return AllocateResponse{}, err
	}

	return AllocateResponse{
		Key:                allocatedKey,
		InventoryAvailable: true,
		UserEligible:       true,
	}, nil
}

func (m *ManagerImpl) GetKeyCount() (int64, error) {
	count, err := m.RedisClient.SetCount(RedisKeySet)
	if err != nil {
		return 0, err
	}
	m.logGaugeMetric(KeyCountMetric, count)
	return count, nil
}

func (m *ManagerImpl) IngestKeys(keys []string) error {
	err := m.RedisClient.SetAdd(RedisKeySet, keys)
	m.GetKeyCount() // trigger a new key count metric
	return err
}

func sha1Hash(rawCode string) (string, error) {
	// first remove any dashes and trim spaces
	rawCode = go_strings.Replace(rawCode, "-", "", -1)
	rawCode = go_strings.TrimSpace(rawCode)

	hash := sha1.New()
	_, err := io.WriteString(hash, string(rawCode))
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", hash.Sum(nil)), nil
}
