package sentiment

import (
	"strings"

	"code.justin.tv/commerce/bits-bot/backend/phrase"
	"code.justin.tv/commerce/bits-bot/backend/text"
)

const (
	punctuationToTrim         = ".,!?"
	acceptablePolitenessRatio = .1
)

type Tone string

const (
	PoliteTone = Tone("polite")
	RudeTone   = Tone("rude")
)

type Topic string

const (
	BitsTopic     = Topic("bits")
	NonsenseTopic = Topic("nonsense")
)

type Analyzer interface {
	Analyze(tokens []text.Token) (Tone, Topic, error)
}

type AnalyzerImpl struct {
}

func (a *AnalyzerImpl) Analyze(tokens []text.Token) (Tone, Topic, error) {
	politeWordCount := 0
	totalWordCount := 0
	bitsTopicWordCount := 0

	for _, token := range tokens {
		textToken, ok := token.(text.TextToken)
		if !ok {
			continue
		}

		tokenStr := textToken.String()
		tokenStr = strings.ToLower(tokenStr)
		tokenStr = strings.Trim(tokenStr, punctuationToTrim)

		_, isPolite := phrase.PolitePhrases[tokenStr]
		if isPolite {
			politeWordCount++
		}

		_, isBitsTopic := phrase.BitsTopicPhrases[tokenStr]
		if isBitsTopic {
			bitsTopicWordCount++
		}

		totalWordCount++
	}

	topic := NonsenseTopic
	if bitsTopicWordCount > 0 {
		topic = BitsTopic
	}

	tone := RudeTone
	if isSufficientlyPolite(politeWordCount, totalWordCount) {
		tone = PoliteTone
	}

	return tone, topic, nil
}

func isSufficientlyPolite(politeWordCount int, totalWordCount int) bool {
	return totalWordCount > 0 && (float64(politeWordCount)/float64(totalWordCount) >= acceptablePolitenessRatio)
}
