package sandstorm

import (
	"sync"
	"time"

	"code.justin.tv/commerce/bits-bot/config"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

func NewSandstorm(cfg *config.Config) SecretGetter {
	var sand SecretGetter

	if cfg != nil && cfg.Environment == config.Prod {
		sand = New(cfg.SandstormRole, cfg.AWSRegion)
	} else {
		sand = &NoOpSecretGetter{}
	}

	return sand
}

var once = new(sync.Once)
var sandstormManager *manager.Manager
var secretCache map[string]*manager.Secret

func init() {
	secretCache = make(map[string]*manager.Secret)
}

type SecretGetter interface {
	Get(secretName string) (*manager.Secret, error)
}

type NoOpSecretGetter struct {
}

func (dsr *NoOpSecretGetter) Get(secretName string) (*manager.Secret, error) {
	return &manager.Secret{
		Plaintext: []byte{},
	}, nil
}

type CachedSecretGetter struct {
}

func (csg *CachedSecretGetter) Get(secretName string) (*manager.Secret, error) {
	cachedSecret, secretCached := secretCache[secretName]
	if secretCached {
		return cachedSecret, nil
	}

	secret, err := sandstormManager.Get(secretName)
	if err != nil {
		return nil, err
	}
	secretCache[secretName] = secret
	return secret, nil
}

func ensureSandstormManagerExists(role string, region string) {
	once.Do(func() {
		if sandstormManager == nil {
			awsConfig := &aws.Config{
				Region:              aws.String(region),
				STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			}
			stsclient := sts.New(session.New(awsConfig))

			arp := &stscreds.AssumeRoleProvider{
				Duration:     900 * time.Second,
				ExpiryWindow: 10 * time.Second,
				RoleARN:      role,
				Client:       stsclient,
			}

			creds := credentials.NewCredentials(arp)
			awsConfig.WithCredentials(creds)

			config := manager.Config{
				AWSConfig: awsConfig,
				TableName: "sandstorm-production",
				KeyID:     "alias/sandstorm-production",
			}

			sandstormManager = manager.New(config)
		}
	})
}

func New(role string, region string) SecretGetter {
	ensureSandstormManagerExists(role, region)
	return &CachedSecretGetter{}
}
