package redis

import (
	"time"

	"code.justin.tv/commerce/bits-bot/config"
	"github.com/go-redis/redis"
)

type Client interface {
	ExpireAt(key string, tm time.Time) (bool, error)
	Ping() error
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
	SetAdd(setKey string, members []string) error
	SetPop(setKey string) (string, error)
	SetCount(setKey string) (int64, error)
	SetNX(key string, value interface{}, expiration time.Duration) *redis.BoolCmd
	Del(keys []string) (int64, error)
	Eval(script string, keys []string, args ...interface{}) *redis.Cmd
	EvalSha(sha1 string, keys []string, args ...interface{}) *redis.Cmd
	ScriptExists(scripts ...string) *redis.BoolSliceCmd
	ScriptLoad(script string) *redis.StringCmd
	LLen(key string) *redis.IntCmd
	LPush(key string, members []string) *redis.IntCmd
	RPush(key string, members []string) *redis.IntCmd
	LRange(key string, start, stop int64) *redis.StringSliceCmd
	LRem(key string, count int64, value interface{}) *redis.IntCmd
	TTL(key string) (time.Duration, error)
	ZIncrBy(key string, increment float64, member string) error
	ZScore(key string, member string) (float64, error)
	ZRevRangeWithScores(key string, start, stop int64) ([]redis.Z, error)
	ZRangeWithScores(key string, start, stop int64) ([]redis.Z, error)
	ZRemRangeByRank(key string, start, stop int64) error
	ZRem(key string, member interface{}) error
}

type client struct {
	redisClient redis.Cmdable
}

func NewClient(cfg config.Config) Client {

	if cfg.UseRedisClusterMode {
		redisClient := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{cfg.RedisEndpoint},
		})

		return &client{
			redis.Cmdable(redisClient),
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.RedisEndpoint,
	})

	return &client{
		redis.Cmdable(redisClient),
	}
}

func (c *client) Ping() error {
	_, err := c.redisClient.Ping().Result()
	return err
}

func (c *client) ExpireAt(key string, tm time.Time) (bool, error) {
	return c.redisClient.ExpireAt(key, tm).Result()
}

func (c *client) TTL(key string) (time.Duration, error) {
	return c.redisClient.TTL(key).Result()
}

func (c *client) Get(key string) (string, error) {
	return c.redisClient.Get(key).Result()
}

func (c *client) Set(key string, value interface{}, expiration time.Duration) error {
	return c.redisClient.Set(key, value, expiration).Err()
}

func (c *client) ZIncrBy(key string, increment float64, member string) error {
	return c.redisClient.ZIncrBy(key, increment, member).Err()
}

func (c *client) ZRem(key string, member interface{}) error {
	return c.redisClient.ZRem(key, member).Err()
}

func (c *client) ZScore(key string, member string) (float64, error) {
	return c.redisClient.ZScore(key, member).Result()
}

func (c *client) ZRevRangeWithScores(key string, start, stop int64) ([]redis.Z, error) {
	return c.redisClient.ZRevRangeWithScores(key, start, stop).Result()
}

func (c *client) ZRangeWithScores(key string, start, stop int64) ([]redis.Z, error) {
	return c.redisClient.ZRangeWithScores(key, start, stop).Result()
}

func (c *client) ZRemRangeByRank(key string, start, stop int64) error {
	return c.redisClient.ZRemRangeByRank(key, start, stop).Err()
}

func (c *client) SetAdd(setKey string, members []string) error {
	_, err := c.redisClient.SAdd(setKey, members).Result()
	return err
}

func (c *client) SetPop(setKey string) (string, error) {
	removedMember, err := c.redisClient.SPop(setKey).Result()
	if err != nil {
		return "", err
	}
	return removedMember, nil
}

func (c *client) SetCount(setKey string) (int64, error) {
	count, err := c.redisClient.SCard(setKey).Result()
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (c *client) SetNX(key string, value interface{}, expiration time.Duration) *redis.BoolCmd {
	return c.redisClient.SetNX(key, value, expiration)
}

func (c *client) Del(keys []string) (int64, error) {
	return c.redisClient.Del(keys...).Result()
}

func (c *client) Eval(script string, keys []string, args ...interface{}) *redis.Cmd {
	return c.redisClient.Eval(script, keys, args...)
}

func (c *client) EvalSha(sha1 string, keys []string, args ...interface{}) *redis.Cmd {
	return c.redisClient.EvalSha(sha1, keys, args...)
}

func (c *client) ScriptExists(scripts ...string) *redis.BoolSliceCmd {
	return c.redisClient.ScriptExists(scripts...)
}

func (c *client) ScriptLoad(script string) *redis.StringCmd {
	return c.redisClient.ScriptLoad(script)
}

func (c *client) LLen(key string) *redis.IntCmd {
	return c.redisClient.LLen(key)
}

func (c *client) LPush(key string, members []string) *redis.IntCmd {
	return c.redisClient.LPush(key, members)
}

func (c *client) RPush(key string, members []string) *redis.IntCmd {
	return c.redisClient.RPush(key, members)
}

func (c *client) LRange(key string, start, stop int64) *redis.StringSliceCmd {
	return c.redisClient.LRange(key, start, stop)
}

func (c *client) LRem(key string, count int64, value interface{}) *redis.IntCmd {
	return c.redisClient.LRem(key, count, value)
}
