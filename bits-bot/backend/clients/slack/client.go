package slack

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type Slacker interface {
	Post(webHookUrl string, msg Message) error
	GetBitsPRsWebhookURL() string
}

type slacker struct {
	bitsPRsWebhookURL string
}

func NewSlacker(bitsPRsWebhookURL string) Slacker {
	return &slacker{
		bitsPRsWebhookURL: bitsPRsWebhookURL,
	}
}

type Message struct {
	Text        string       `json:"text"`
	UnfurlLinks bool         `json:"unfurl_links"`
	Parse       string       `json:"parse"`
	Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	Title    string  `json:"title"`
	Fallback string  `json:"fallback"`
	Text     string  `json:"text"`
	Pretext  string  `json:"pretext"`
	ImageURL string  `json:"image_url"`
	Color    string  `json:"color"`
	Fields   []Field `json:"fields"`
}

type Field struct {
	Title   string `json:"title"`
	Value   string `json:"value"`
	IsShort bool   `json:"short"`
}

func (s *slacker) Post(webHookUrl string, msg Message) error {
	url := webHookUrl
	requestJson, err := json.Marshal(&msg)
	if err != nil {
		return err
	}

	requestJsonReader := bytes.NewReader(requestJson)

	_, err = http.Post(url, "application/json", requestJsonReader)
	if err != nil {
		return err
	}
	return nil
}

func (s *slacker) GetBitsPRsWebhookURL() string {
	return s.bitsPRsWebhookURL
}
