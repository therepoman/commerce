package lock

import (
	"errors"
	"time"

	lock "github.com/bsm/redis-lock"
)

const (
	defaultLockTimeout = 5 * time.Second
	defaultRetryCount  = 10
	defaultRetryDelay  = 100 * time.Millisecond
)

type redisLockingClient struct {
	RedisClient lock.RedisClient
	LockOptions *lock.Options
}

func NewRedisLockingClient(redisClient lock.RedisClient, lockOptions *lock.Options) LockingClient {
	return &redisLockingClient{
		RedisClient: redisClient,
		LockOptions: lockOptions,
	}
}

func DefaultLockOptions() *lock.Options {
	return &lock.Options{
		LockTimeout: defaultLockTimeout,
		RetryCount:  defaultRetryCount,
		RetryDelay:  defaultRetryDelay,
	}
}

func (c *redisLockingClient) ObtainLock(key string) (Lock, error) {
	rl, err := lock.Obtain(c.RedisClient, key, c.LockOptions)
	if err != nil {
		return nil, err
	}

	if rl == nil {
		return nil, errors.New("failed to obtain redis lock")
	}

	return &redisLock{
		RedisLock: rl,
	}, nil
}

type redisLock struct {
	RedisLock *lock.Locker
}

func (l *redisLock) Unlock() error {
	return l.RedisLock.Unlock()
}
