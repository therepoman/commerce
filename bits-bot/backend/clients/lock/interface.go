package lock

type LockingClient interface {
	ObtainLock(key string) (Lock, error)
}

type Lock interface {
	Unlock() error
}
