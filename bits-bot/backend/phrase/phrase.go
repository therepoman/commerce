package phrase

import (
	"strings"
)

func IsMultiWordPhrase(phrase string) bool {
	return len(strings.Split(phrase, " ")) > 1
}

func GetMultiWordPhrases() map[string]interface{} {
	multiWordPhrases := make(map[string]interface{})
	for phrase := range PolitePhrases {
		if IsMultiWordPhrase(phrase) {
			multiWordPhrases[phrase] = nil
		}
	}
	for phrase := range BitsTopicPhrases {
		if IsMultiWordPhrase(phrase) {
			multiWordPhrases[phrase] = nil
		}
	}
	return multiWordPhrases
}

var PolitePhrases = map[string]interface{}{
	"please":          nil,
	"pls":             nil,
	"plz":             nil,
	"pleeeeezuuh":     nil,
	"thanks":          nil,
	"thank":           nil,
	"thx":             nil,
	"ty":              nil,
	"love":            nil,
	"heart":           nil,
	":giveplz:":       nil,
	"plox":            nil,
	"sudo":            nil,
	"kisses":          nil,
	"por favor":       nil, // spanish
	"s’il vous plaît": nil, // french
	"s'il vous plaît": nil, // french
	"per favore":      nil, // italian
	"bitte":           nil, // german
	"alsjeblieft":     nil, // dutch
	"请":               nil, // chinese (simplified)
	"請":               nil, // chinese (traditional)
	"お願いします":          nil, // japanese
	"하십시오":            nil, // korean
}

var BitsTopicPhrases = map[string]interface{}{
	"bit":                nil,
	"bits":               nil,
	"bitz":               nil,
	"bitties":            nil,
	":b:its":             nil,
	"cheer":              nil,
	"cheers":             nil,
	"gem":                nil,
	"gems":               nil,
	":cheer1:":           nil,
	":cheer100:":         nil,
	":cheer1000:":        nil,
	":cheer5000:":        nil,
	":cheer10000:":       nil,
	":cheer100000:":      nil,
	":givebits1:":        nil,
	":givebits10:":       nil,
	":givebits100:":      nil,
	":givebits500:":      nil,
	":givebits1000:":     nil,
	":givebits10000:":    nil,
	":chuckcheer1:":      nil,
	":chuckcheer100:":    nil,
	":chuckcheer1000:":   nil,
	":chuckcheer5000:":   nil,
	":chuckcheer10000:":  nil,
	":chuckcheer100000:": nil,
	":ninjabits:":        nil,
}
