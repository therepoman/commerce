package backend

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/api/bits_bot_roulette"

	"code.justin.tv/commerce/bits-bot/backend/api/grant_daily_pbx"

	"code.justin.tv/commerce/bits-bot/backend/api/configure_bot"
	"code.justin.tv/commerce/bits-bot/backend/api/get_available_key_count"
	"code.justin.tv/commerce/bits-bot/backend/api/get_pr_settings"
	"code.justin.tv/commerce/bits-bot/backend/api/ingest_keys"
	"code.justin.tv/commerce/bits-bot/backend/api/post_daily_digest"
	"code.justin.tv/commerce/bits-bot/backend/api/process_pull_request"
	"code.justin.tv/commerce/bits-bot/backend/api/redis_health_check"
	"code.justin.tv/commerce/bits-bot/backend/api/update_pr_settings"
	"code.justin.tv/commerce/bits-bot/backend/clients/lock"
	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	"code.justin.tv/commerce/bits-bot/backend/clients/sandstorm"
	slack_client "code.justin.tv/commerce/bits-bot/backend/clients/slack"
	"code.justin.tv/commerce/bits-bot/backend/dynamo/key_claim"
	message_dynamo "code.justin.tv/commerce/bits-bot/backend/dynamo/message"
	"code.justin.tv/commerce/bits-bot/backend/dynamo/pr_settings"
	"code.justin.tv/commerce/bits-bot/backend/key"
	"code.justin.tv/commerce/bits-bot/backend/mention"
	"code.justin.tv/commerce/bits-bot/backend/message"
	pr_settings_helpers "code.justin.tv/commerce/bits-bot/backend/pull_requests/pr_settings"
	"code.justin.tv/commerce/bits-bot/backend/pull_requests/review_event"
	"code.justin.tv/commerce/bits-bot/backend/pull_requests/review_requested"
	"code.justin.tv/commerce/bits-bot/backend/response"
	"code.justin.tv/commerce/bits-bot/backend/secret"
	"code.justin.tv/commerce/bits-bot/backend/secret/eddism"
	"code.justin.tv/commerce/bits-bot/backend/secret/magic"
	"code.justin.tv/commerce/bits-bot/backend/secret/pbx"
	"code.justin.tv/commerce/bits-bot/backend/secret/user"
	"code.justin.tv/commerce/bits-bot/backend/secret/utils"
	"code.justin.tv/commerce/bits-bot/backend/secret/word_guess"
	"code.justin.tv/commerce/bits-bot/backend/sentiment"
	"code.justin.tv/commerce/bits-bot/backend/server"
	"code.justin.tv/commerce/bits-bot/backend/text"
	"code.justin.tv/commerce/bits-bot/bot"
	"code.justin.tv/commerce/bits-bot/config"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/commerce/splatter/cloudwatch"
	"code.justin.tv/foundation/twitchclient"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

type Backend struct {
	Server *server.Server `inject:""`
	Bot    bot.Bot        `inject:""`
	Stats  statsd.Statter `inject:""`
}

func NewBackend(cfg *config.Config) (*Backend, error) {
	if cfg == nil {
		return nil, errors.New("config cannot be nil")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetricsConfig != nil {
		flusher, err := cloudwatch.NewMetricFlusher(&cloudwatch.MetricFlusherConfig{
			AWSRegion:                      cfg.AWSRegion,
			Namespace:                      fmt.Sprintf("bits-bot-%s", cfg.EnvironmentName),
			BufferSize:                     cfg.CloudwatchMetricsConfig.BufferSize,
			BatchSize:                      cfg.CloudwatchMetricsConfig.BatchSize,
			FlushInterval:                  time.Minute * time.Duration(cfg.CloudwatchMetricsConfig.FlushIntervalMinutes),
			FlushPollCheckDelay:            time.Millisecond * time.Duration(cfg.CloudwatchMetricsConfig.FlushCheckDelayMS),
			BufferEmergencyFlushPercentage: cfg.CloudwatchMetricsConfig.EmergencyFlushPercentage,
		})
		if err != nil {
			return nil, err
		}

		logger, err := cloudwatch.NewMetricLogger(cfg.EnvironmentName, flusher)
		if err != nil {
			return nil, err
		}

		cloudwatchStatter, err := splatter.NewCloudwatchStatter(logger, cfg.StatsPrefix)
		if err != nil {
			return nil, err
		}

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	statsdStatter, err := statsd.NewNoopClient()
	if err != nil {
		return nil, err
	}

	statters = append(statters, statsdStatter)

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	var backend Backend
	var graph inject.Graph

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	sandstormClient := sandstorm.NewSandstorm(cfg)
	slackToken1Secret, err := sandstormClient.Get(cfg.BitsBotSlackTokenSandstormKey)
	if err != nil {
		return nil, err
	}

	if slackToken1Secret == nil {
		return nil, errors.New("could not load slack token secret 1 from sandstorm")
	}

	slackToken2Secret, err := sandstormClient.Get(cfg.BitsBotSlackToken2SandstormKey)
	if err != nil {
		return nil, err
	}

	if slackToken2Secret == nil {
		return nil, errors.New("could not load slack token secret 2 from sandstorm")
	}

	slackBitsPRsWebhookURL, err := sandstormClient.Get(cfg.BitsBotSlackPRsWebhookURLSandstormKey)
	if err != nil {
		return nil, err
	}

	if slackBitsPRsWebhookURL == nil {
		return nil, errors.New("could not load slack bits PR webhook URL from sandstorm")
	}

	redisClient := redis.NewClient(*cfg)
	redisLockingClient := lock.NewRedisLockingClient(redisClient, lock.DefaultLockOptions())

	// Secret Features
	eddismProcessor := &eddism.Processor{}
	userProcessor := &user.Processor{}
	scryfallProcessor := &magic.Processor{}
	wordsProcessor := &word_guess.Processor{}
	pbxProcessor := pbx.NewPBXProcessor()

	secretFeatures := []secret.Processor{
		userProcessor,
		eddismProcessor,
		scryfallProcessor,
		wordsProcessor,
		pbxProcessor,
	}

	transportConf := twitchclient.TransportConf{
		MaxIdleConnsPerHost: 100,
	}

	userServiceConfig := twitchclient.ClientConf{
		Host:      cfg.UserServiceEndpoint,
		Transport: transportConf,
		Stats:     statsdStatter,
	}

	usersClient, err := users.NewClient(userServiceConfig)
	if err != nil {
		return nil, err
	}

	err = graph.Provide(
		// Backend (graph root)
		&inject.Object{Value: &backend},
		&inject.Object{Value: cfg},
		&inject.Object{Value: &server.Server{}},
		&inject.Object{Value: bot.NewBot()},

		// Clients
		&inject.Object{Value: redisClient},
		&inject.Object{Value: redisLockingClient},
		&inject.Object{Value: slack.New(string(slackToken1Secret.Plaintext)), Name: "slack_client_1"},
		&inject.Object{Value: slack.New(string(slackToken2Secret.Plaintext)), Name: "slack_client_2"},
		&inject.Object{Value: sandstormClient},
		&inject.Object{Value: stats},
		&inject.Object{Value: slack_client.NewSlacker(string(slackBitsPRsWebhookURL.Plaintext))},
		&inject.Object{Value: usersClient},

		// Dynamo DAOs
		&inject.Object{Value: key_claim.NewDAO(sess, cfg.DynamoSuffix)},
		&inject.Object{Value: message_dynamo.NewDAO(sess, cfg.DynamoSuffix)},
		&inject.Object{Value: pr_settings.NewDAO(sess, cfg.DynamoSuffix)},

		// APIs
		&inject.Object{Value: &redis_health_check.API{}},
		&inject.Object{Value: &get_available_key_count.API{}},
		&inject.Object{Value: &ingest_keys.API{}},
		&inject.Object{Value: &configure_bot.API{}},
		&inject.Object{Value: process_pull_request.NewAPI()},
		&inject.Object{Value: update_pr_settings.NewAPI()},
		&inject.Object{Value: get_pr_settings.NewAPI()},
		&inject.Object{Value: &post_daily_digest.API{}},
		&inject.Object{Value: grant_daily_pbx.NewAPI()},
		&inject.Object{Value: bits_bot_roulette.NewAPI()},

		// Internal Components
		&inject.Object{Value: &message.ProcessorImpl{}},
		&inject.Object{Value: &text.TokenizerImpl{}},
		&inject.Object{Value: &mention.CounterImpl{}},
		&inject.Object{Value: &sentiment.AnalyzerImpl{}},
		&inject.Object{Value: &key.ManagerImpl{}},
		&inject.Object{Value: &response.ResponderImpl{}},
		&inject.Object{Value: &message.RecorderImpl{}},

		// Secrets
		&inject.Object{Value: &secret.FeaturesManagerImpl{}},
		&inject.Object{Value: userProcessor},
		&inject.Object{Value: eddismProcessor},
		&inject.Object{Value: scryfallProcessor},
		&inject.Object{Value: wordsProcessor},
		&inject.Object{Value: pbxProcessor},
		&inject.Object{Value: pbx.NewPBX()},
		&inject.Object{Value: &utils.UtilHelper{}},
		&inject.Object{Name: "secret_features", Value: secretFeatures},

		// PR stuff
		&inject.Object{Value: review_requested.NewProcessor(), Name: "reviewRequestedProcessor"},
		&inject.Object{Value: review_event.NewProcessor(), Name: "reviewEventProcessor"},
		&inject.Object{Value: pr_settings_helpers.NewChecker()},
	)
	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	//Initialize secrets that have runtime constants based on configs
	for _, processor := range secretFeatures {
		processor.Init()
	}

	return &backend, nil
}

func (b *Backend) Ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}
