package mention

import (
	"code.justin.tv/commerce/bits-bot/backend/text"
)

type Counter interface {
	Count(tokens []text.Token) map[string]int
}

type CounterImpl struct {
}

func (c *CounterImpl) Count(tokens []text.Token) map[string]int {
	mentionCount := make(map[string]int)
	for _, token := range tokens {
		mentionToken, ok := token.(text.MentionToken)
		if !ok {
			continue
		}
		mentionCount[mentionToken.UserID]++
	}
	return mentionCount
}
