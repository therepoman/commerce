package response

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"github.com/hako/durafmt"
)

type Responder interface {
	TooRude() string
	OffTopic() string
	NotificationOfDM(slackUsername string) string
	UserNotEligible(slackUsername string, duration time.Duration) string
	NoInventory() string
	ProvideKey(key string) string
}

type ResponderImpl struct {
}

func (r *ResponderImpl) TooRude() string {
	return randomResponse([]string{
		"No",
		":no:",
		"RUDE",
		"Try asking nicely",
		"Your request has been denied. Reason code: TOO_RUDE",
		"Your request has been denied. Your message did not meet the sentiment analyzer's minimum acceptable politeness ratio",
	})
}

func (r *ResponderImpl) OffTopic() string {
	return randomResponse([]string{
		"What?",
		"Huh?",
		"I don't understand",
		"Your request has been denied. Reason code: OFF_TOPIC",
	})
}

func (r *ResponderImpl) NotificationOfDM(slackUsername string) string {
	return fmt.Sprintf("@%s - I DM'd you", slackUsername)
}

func (r *ResponderImpl) UserNotEligible(slackUsername string, duration time.Duration) string {
	return fmt.Sprintf("@%s - You have already received a code within the past month. Try again in %s", slackUsername, formatDuration(duration))
}

func (r *ResponderImpl) NoInventory() string {
	return "Sorry, I'm all out of keys. Please let the bits team know!"
}

func (r *ResponderImpl) ProvideKey(key string) string {
	return fmt.Sprintf("Here's your key: *%s* \n Redeem it at twitch.tv/redeem?code=%s", key, key)
}

func randomResponse(possibleResponses []string) string {
	return possibleResponses[random.Int(0, len(possibleResponses)-1)]
}

func formatDuration(dur time.Duration) string {
	if dur > time.Minute {
		// round to the nearest minute
		dur = (dur / time.Minute) * time.Minute
	}
	return durafmt.Parse(dur).String()
}
