package pr_settings

import "code.justin.tv/commerce/bits-bot/backend/dynamo/pr_settings"

type Checker interface {
	IsNotificationsDisabled(slackUserID string) bool
}

func NewChecker() Checker {
	return &checker{}
}

type checker struct {
	SettingsDAO pr_settings.DAO `inject:""`
}

func (c *checker) IsNotificationsDisabled(slackUserID string) bool {
	userSettings, err := c.SettingsDAO.GetSettings(slackUserID)

	if err != nil || userSettings == nil {
		return false
	}

	return userSettings.NotificationsDisabled
}
