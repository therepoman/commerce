package review_event

import (
	"fmt"

	"code.justin.tv/commerce/bits-bot/backend/pull_requests"
	"code.justin.tv/commerce/bits-bot/backend/pull_requests/pr_settings"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	"github.com/nlopes/slack"
	"github.com/twitchtv/twirp"
)

type Processor interface {
	Process(req *bits_bot.ProcessPullRequestReq) error
}

func NewProcessor() Processor {
	return &processor{}
}

type processor struct {
	SlackClient     *slack.Client       `inject:"slack_client_1"`
	SettingsChecker pr_settings.Checker `inject:""`
}

func (p *processor) Process(req *bits_bot.ProcessPullRequestReq) error {
	if req == nil || req.Review == nil || req.Review.User == nil || req.Review.User.Login == "" {
		return nil
	}

	if req.Review.User.Login == req.PullRequest.User.Login {
		return nil
	}

	var reviewer string
	user, err := p.SlackClient.GetUserByEmail(fmt.Sprintf("%s@twitch.tv", req.Review.User.Login))
	if err != nil || user == nil {
		reviewer = "someone"
	} else {
		reviewer = fmt.Sprintf("@%s", user.ID)
	}

	var reviewState string
	switch req.Review.State {
	case "commented":
		reviewState = "commented on"
	case "changes_requested":
		reviewState = "requested changes on"
	case "approved":
		reviewState = "approved the changes in"
	default:
		reviewState = req.Review.State
	}

	title := fmt.Sprintf("[%s]", req.Repository.FullName)
	body := fmt.Sprintf("<%s|#%d: %s>\n<%s> has %s your PR", req.PullRequest.HtmlUrl, req.PullRequest.Number, req.PullRequest.Title, reviewer, reviewState)

	requesterInfo, err := p.SlackClient.GetUserByEmail(fmt.Sprintf("%s@twitch.tv", req.PullRequest.User.Login))
	if err != nil || requesterInfo == nil {
		return err
	}

	if p.SettingsChecker.IsNotificationsDisabled(requesterInfo.ID) {
		return nil
	}

	_, _, dmChannel, err := p.SlackClient.OpenIMChannel(requesterInfo.ID)
	if err != nil {
		return err
	}

	_, _, err = p.SlackClient.PostMessage(dmChannel, slack.MsgOptionAttachments(
		[]slack.Attachment{{
			Title: title,
			Text:  body,
			Color: pull_requests.TwitchPurple,
		}}...))
	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	return nil
}
