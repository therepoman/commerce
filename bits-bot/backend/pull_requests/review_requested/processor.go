package review_requested

import (
	"bytes"
	"crypto/sha1"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/bits-bot/backend/clients/lock"
	"code.justin.tv/commerce/bits-bot/backend/clients/redis"
	slacker "code.justin.tv/commerce/bits-bot/backend/clients/slack"
	"code.justin.tv/commerce/bits-bot/backend/pull_requests"
	bits_bot "code.justin.tv/commerce/bits-bot/rpc"
	go_redis "github.com/go-redis/redis"
	"github.com/nlopes/slack"
	"github.com/twitchtv/twirp"
)

const (
	ReviewRequestLock = "github-pr-review-request-lock"
)

type Processor interface {
	Process(req *bits_bot.ProcessPullRequestReq) error
}

func NewProcessor() Processor {
	return &processor{}
}

type processor struct {
	Slacker       slacker.Slacker    `inject:""`
	SlackClient   *slack.Client      `inject:"slack_client_1"`
	RedisClient   redis.Client       `inject:""`
	LockingClient lock.LockingClient `inject:""`
}

func (p *processor) Process(req *bits_bot.ProcessPullRequestReq) error {
	reviewRequestLock, err := p.LockingClient.ObtainLock(ReviewRequestLock)
	if err != nil {
		return twirp.InternalErrorWith(err)
	}
	defer reviewRequestLock.Unlock()

	var reviewer string
	user, err := p.SlackClient.GetUserByEmail(fmt.Sprintf("%s@twitch.tv", req.PullRequest.User.Login))
	if err != nil || user == nil {
		reviewer = "someone"
	} else {
		reviewer = fmt.Sprintf("@%s", user.ID)
	}

	targets := make([]slacker.Field, 0)
	for _, reviewer := range req.PullRequest.RequestedReviewers {
		var target string
		r := reviewer.Login
		user, err := p.SlackClient.GetUserByEmail(fmt.Sprintf("%s@twitch.tv", r))
		if err != nil || user == nil {
			target = "someone"
		} else {
			target = fmt.Sprintf("@%s", user.ID)
		}

		targets = append(targets, slacker.Field{
			Title:   "Reviewer",
			Value:   fmt.Sprintf("<%s>", target),
			IsShort: true,
		})
	}

	title := fmt.Sprintf("[%s]", req.Repository.FullName)
	body := fmt.Sprintf("<%s|#%d: %s> by <%s>\n%s", req.PullRequest.HtmlUrl, req.Number, req.PullRequest.Title, reviewer, req.PullRequest.Body)
	fallback := fmt.Sprintf("[%s] Review was requested: <%s|#%d: %s>", req.Repository.FullName, req.PullRequest.HtmlUrl, req.Number, req.PullRequest.Title)

	message := slacker.Message{
		Attachments: []slacker.Attachment{
			{
				Title:    title,
				Fallback: fallback,
				Text:     body,
				Color:    pull_requests.TwitchPurple,
				Fields:   targets,
			},
		},
	}

	redisPRStatusKey, err := getRedisPRStatusKey(message)
	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	prStatus, err := p.RedisClient.Get(redisPRStatusKey)
	if err != nil && err != go_redis.Nil {
		return twirp.InternalErrorWith(err)
	}

	if prStatus != "" {
		return nil
	}

	err = p.Slacker.Post(p.Slacker.GetBitsPRsWebhookURL(), message)
	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	err = p.RedisClient.Set(redisPRStatusKey, "SUBMITTED", time.Hour)
	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	return nil
}

func getRedisPRStatusKey(msg slacker.Message) (string, error) {
	if len(msg.Attachments) != 1 {
		return "", errors.New("expected exactly one attachment")
	}

	attachment := msg.Attachments[0]
	bb := bytes.NewBuffer([]byte{})
	bb.WriteString(attachment.Title)
	bb.WriteString(attachment.Text)
	for _, field := range attachment.Fields {
		bb.WriteString(field.Title)
		bb.WriteString(field.Value)
	}

	prHash := string(sha1.New().Sum(bb.Bytes()))
	return fmt.Sprintf("pr-status-%s", prHash), nil
}
