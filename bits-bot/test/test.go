package test

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/juju/errgo/errors"
	log "github.com/sirupsen/logrus"
)

const (
	localFilePath   = "/src/code.justin.tv/commerce/bits-bot/%s"
	credentialsFile = ".credentials"
)

func LoadCredentials() (string, error) {
	credsFilePath, err := getCredentialsFilePath(credentialsFile)
	if err != nil {
		return "", err
	}

	file, err := os.Open(credsFilePath)
	if err != nil {
		return "", err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return string(fileBytes), nil

}

func getCredentialsFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(localFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	return "", errors.New("could not find file")
}
