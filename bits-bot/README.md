# bits-bot
🤖 Bits Bot - the Twitch Bits automaton 

## Endpoints

| Environment | Endpoint |
| --- | --- |
| Prod | https://main.us-west-2.prod.bits-bot.twitch.a2z.com/ |
