package main

import (
	"fmt"
	"os"
	"strings"
)

type envMap map[string]string
type configArray []string

func (e *envMap) String() string {
	return fmt.Sprintf("%v", *e)
}

func (e *envMap) Set(value string) error {
	vals := strings.Split(value, "=")
	key := vals[0]
	var v string

	if len(vals) == 1 {
		v = os.Getenv(key)
		if v == "" {
			return fmt.Errorf("flag: expected %s in env, but not found", key)
		}
	} else if len(vals) == 2 {
		v = vals[1]
	}
	(*e)[key] = v
	return nil
}

func (cf *configArray) String() string {
	return fmt.Sprintf("%v", *cf)
}

func (cf *configArray) Set(value string) error {
	(*cf) = append(*cf, value)
	return nil
}
