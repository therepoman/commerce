/*
Package main defines the manta command which standardizes build environments between formal build
servers and developer machines.  It internally uses Docker to isolate and reproduce builds to prevent variance
due to the host environment.

Command Flags

Set these when you execute manta from the command line.

	-v: Enable verbose logging to stdout.
	-interact: After the build finishes (success or failure), start an interactive shell within the build container instead of extracting artifacts. Useful for debugging.
	-proxy: Configure the build container to run behind the standard Twitch proxy. You should set this when using a docker daemon running on a build server.
	-privileged: Enables "privileged" mode for docker containers.
	-dind: Enable "docker-in-docker" mode. This runs docker inside your container and enables privileged mode.
	-e: Environment variables to inject into build container at setup. Format: NAME=VALUE
	-E: Environment variables to inject into build container at build. Format: NAME=VALUE

Environment Variables

Some environment variables are automatically set, allowing you to reference them in your build commands.

	GIT_COMMIT: The commit hash of the working directory.  A "-dirty" suffix is appended if "git status --porcelain" returns output.
*/
package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"time"

	"code.justin.tv/release/manta-style/pkg/common"
	"code.justin.tv/release/manta-style/pkg/docker"
	"code.justin.tv/release/manta-style/pkg/dockerfile"
	"code.justin.tv/release/manta-style/pkg/prepare"
)

var envFlags, buildEnvFlags envMap
var configFiles configArray
var tarExcludes configArray

var (
	VERSION    = "0.3.42"
	GIT_COMMIT string
)

func init() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	envFlags = make(envMap)
	buildEnvFlags = make(envMap)
}

func main() {
	os.Exit(do())
}

func version() {
	verStr := bytes.NewBufferString(fmt.Sprintf("Manta version: %s", VERSION))
	if GIT_COMMIT != "" {
		verStr.WriteString(fmt.Sprintf(" COMMIT: %s", GIT_COMMIT))
	}
	log.Println(verStr.String())
}

func do() int {
	var verboseFlag, versionFlag, interactFlag, proxyFlag, privilegedFlag, dindFlag, updateFlag bool

	flag.Var(&envFlags, "e",
		"Environment variables to inject into build container at setup. "+
			"Format: NAME or NAME=VALUE. "+
			"The first form copies the value from the environment, in the second form you supply the value")
	flag.Var(&buildEnvFlags, "E",
		"Environment variables to inject into build container at build. "+
			"Format: NAME or NAME=VALUE. "+
			"The first form copies the value from the environment, in the second form you supply the value")
	flag.Var(&configFiles, "f",
		"Manta config file(s) to load."+
			"Files are unmarshalled in order they are specified onto the same variable.")
	flag.Var(&tarExcludes, "X", "Pattern of files to exclude when creating Docker image used for build section.")

	// TODO: use library to handle -h output with a goal of easy to read documentation in this file (your editor and godoc)
	flag.BoolVar(&verboseFlag, "v", false, "Enable verbose output")
	flag.BoolVar(&versionFlag, "version", false, "output version")
	flag.BoolVar(&interactFlag, "interact", false, "Launch interactive shell when build exits")
	flag.BoolVar(&proxyFlag, "proxy", false, "Configure build container to run behind Twitch proxy")
	flag.BoolVar(&privilegedFlag, "privileged", false, "Run build container in privileged mode")
	flag.BoolVar(&dindFlag, "dind", false, "Configure build container to run nested docker. Enables Privileged mode")
	flag.BoolVar(&updateFlag, "update", false, "Check for updated base image.")
	flag.Parse()

	if versionFlag {
		version()
		return 0
	}

	if verboseFlag {
		os.Setenv("VERBOSE", "1")
		version()
	}

	if proxyFlag {
		os.Setenv("TWITCH_PROXY", "1")
	}

	if privilegedFlag {
		os.Setenv("PRIVILEGED", "1")
	}

	if dindFlag {
		os.Setenv("DIND", "1")
		os.Setenv("PRIVILEGED", "1")
	}

	if updateFlag {
		os.Setenv("UPDATE", "1")
	}

	if configFiles == nil {
		configFiles = []string{".manta.json"}
	}

	config, err := dockerfile.LoadConfig(configFiles)
	if err != nil {
		log.Printf("error loading config: %v", err)
		return 1
	}

	config.TarExcludes = append(config.TarExcludes, tarExcludes...)

	for k, v := range envFlags {
		config.EnvList = append(config.EnvList, fmt.Sprintf("%s=%s", k, v))
	}
	for k, v := range buildEnvFlags {
		config.BuildEnvList = append(config.BuildEnvList, fmt.Sprintf("%s=%s", k, v))
	}
	tmpDir, err := ioutil.TempDir("", "manta-style")
	if err != nil {
		log.Printf("error creating dockerfile directory")
		return 1
	}
	defer os.RemoveAll(tmpDir)

	// TODO(keith): if extract.destination is set, delete it before tarring up the current directory.

	imageID, err := createImageForBuild(*config, tmpDir)
	if err != nil {
		log.Printf("error creating build image: %s", err)
		return 1
	}
	// we don't delete the image since the latest version of docker (6/13/14) does a "deep" delete, removing all the cached steps which we depend on for speed with repeated builds

	if common.Verbose() {
		log.Printf("Executing build steps using image %s", imageID)
	}

	containerID, exitCode, err := runImage(imageID, *config, config.NetworkedBuild)
	if containerID != "" {
		defer docker.DeleteContainer(containerID)
	}
	if err != nil {
		log.Printf("error running build image: %v", err)
		return 1
	}

	if interactFlag {
		time.Sleep(time.Second * 1)
		image, err := docker.CommitContainer(containerID, "", "")
		if err != nil {
			log.Printf("could not snapshot container for interact mode: %s", err)
			return 1
		}
		if common.Verbose() {
			log.Printf("created interactive image: %s", image.ID)
		}
		defer docker.DeleteImage(image.ID)

		log.Print("Creating interactive shell within build container...")

		docker.AttachTerminalToImage(image.ID, config.Mount)
	}

	if config.Extract != nil {
		err = docker.ExtractArtifact(containerID, config.Extract.Source, config.Extract.Destination)
		if err != nil {
			log.Printf("error extracting artifact: %v", err)
			return 1
		}
		log.Printf("build command finished with exit code: %d", exitCode)
	}

	if exitCode != 0 {
		return 1
	}
	return 0
}

func createImageForBuild(config common.Config, tmpDir string) (string, error) {
	var imageID string

	log.Print("Creating base docker image... (could be long on first run)")

	setupFiles := make([]string, 0, len(config.SetupFiles))
	for file, _ := range config.SetupFiles {
		setupFiles = append(setupFiles, file)
	}

	if os.Getenv("UPDATE") != "" || config.UpdateImage {
		if err := docker.UpdateImage(config.Image); err != nil {
			return "", err
		}
	}

	if useMultiImageBuild, fakeTimestamp := dockerfile.ShouldUseMultiImage(config); useMultiImageBuild {
		// multi image build

		// create image from result of Config.Setup + "bundle install"

		dockerFileContents, filesNeeded, err := dockerfile.MultiImagePre(config, tmpDir)
		if err != nil {
			return "", err
		}

		err = prepare.CopyFiles(append(setupFiles, filesNeeded...), tmpDir, fakeTimestamp)
		if err != nil {
			return "", err
		}
		tarPath, err := prepare.TarDirWithDockerfile(config, dockerFileContents, tmpDir)
		if err != nil {
			return "", fmt.Errorf("error combining pwd with manta Dockerfile: %s", err)
		}
		defer os.Remove(tarPath)

		preImageID, err := docker.CreateImage(tarPath)
		if err != nil {
			return "", err
		}

		// if setup is entirely from cache, skip running the preImage and continue to build
		// we know it is from cache if the predictable base image name already exists in docker
		baseImageName := fmt.Sprintf("%s_bundle", preImageID)
		exists, err := docker.ImageExists(baseImageName)
		if err != nil {
			return "", err
		}

		if !exists {
			if common.Verbose() {
				log.Print("executing pre-image")
			}
			containerID, exitCode, err := runImage(preImageID, config, true)
			if containerID != "" {
				defer docker.DeleteContainer(containerID)
			}
			if err != nil {
				return "", err
			}

			if exitCode != 0 {
				return "", fmt.Errorf("setup image failed to execute. error code %d", exitCode)
			}

			// commit container with a name which can be passed into MultiImagePost
			if common.Verbose() {
				log.Printf("commiting container to image %s from result of setup", baseImageName)
			}
			time.Sleep(time.Second * 1)
			_, err = docker.CommitContainer(containerID, baseImageName, "")
			if err != nil {
				return "", err
			}
		}

		if common.Verbose() {
			log.Printf("creating post-image")
		}

		// using that image as base, create second image to execute Config.Build
		prepare.CopyWorkingDir(tmpDir)
		dockerFileContents = dockerfile.MultiImagePost(baseImageName, config, tmpDir)
		tarPath, err = prepare.TarDirWithDockerfile(config, dockerFileContents, tmpDir)
		if err != nil {
			return "", fmt.Errorf("error combining pwd with manta Dockerfile: %s", err)
		}
		defer os.Remove(tarPath)

		imageID, err = docker.CreateImage(tarPath)
		if err != nil {
			return "", err
		}
	} else {
		// single image build
		// create image which includes Config.Setup and Config.Build

		prepare.CopyWorkingDir(tmpDir)
		// Make sure setupFiles have the correct permissions:
		if err := prepare.CopyFiles(setupFiles, tmpDir, false); err != nil {
			return "", err
		}
		dockerFileContents := dockerfile.SingleImage(config, tmpDir)
		tarPath, err := prepare.TarDirWithDockerfile(config, dockerFileContents, tmpDir)
		if err != nil {
			return "", fmt.Errorf("error combining pwd with manta Dockerfile: %s", err)
		}
		defer os.Remove(tarPath)

		imageID, err = docker.CreateImage(tarPath)
		if err != nil {
			return "", err
		}
	}

	return imageID, nil
}

func runImage(imageID string, config common.Config, networkEnabled bool) (string, int, error) {
	log.Print("Executing...")
	containerID, err := docker.StartImageAsDaemon(imageID, config.Mount, networkEnabled)
	if err != nil {
		return containerID, -1, fmt.Errorf("error starting image: %v", err)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)
	go func() {
		for _ = range c {
			log.Print("stopping running container before exiting")
			docker.StopContainer(containerID, 10)
		}
	}()

	exitCode, err := docker.WatchStdoutUntilDone(containerID)
	if err != nil {
		return containerID, exitCode, fmt.Errorf("failure waiting for build container: %s", err)
	}

	close(c)

	return containerID, exitCode, nil
}
