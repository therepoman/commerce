/*
Package dockerfile parses your config file and constructs a buildable Dockerfile.

JSON Syntax

Manta discovers how to build your project based on a ".manta.json" located in the root directory. The schema of ".manta.json" should contain the following fields:

	image: (String) Name of docker image used as a base for the build container. If you don't know what to pick, use "ubuntu"
	update_image: (Bool) Checks for the existence of new images before building.
	os: (String) OS distribution of the image. Values can be "ubuntu", "debian", "rhel", or "centos"
	mount: (String) Absolute path inside the container to put your source code.
	env: (Object) Set of key/value pairs defining environment variables inside the container.
	setup: (Array) List of commands to setup dependencies.  The results of these commands are cached for future runs, speeding up your build unless they are changed.
	dependencies: (Array) List of dependencies to install
		repo: Github owner/repo
		sha: SHA of the release
		target_path: Relative path in mount to install into.
	build: (Array) List of commands to build and test your application.  These commands are run every build.
	extract: (Object)
		source: Path inside the container to extract after a successful build
		destination: Local path to extract to

Language-specific Keys

Ruby

If set, will automatically install and cache gems from your Gemfile. If your Gemfile(.lock) changes, they will be re-installed.

Simply having...

	"ruby": {}

...is valid config.

	ruby: (Object)
		vendored_gems: (Array) List of relative paths to gems which exist in the project and are needed for bundle installation.
		fake_timestamp: (Boolean) Manipulates the timestamp of gem-related files to aggressively cache bundle installs. It is not normal to need this. Default: false

Yarn

If set, will automatically install and cache packages from your `yarn.lock`. If your `yarn.lock` or `package.json` changes, they will be re-installed.

Simply having...

	"yarn": {}

...is valid config.

	yarn: (Object)
		yarn_version: (String) Version of Yarn to use. Default: `latest`
		fake_timestamp: (Boolean) Manipulates the timestamp of gem-related files to aggressively cache bundle installs. It is not normal to need this. Default: false
*/
package dockerfile
