package prepare

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/release/manta-style/pkg/common"
)

func CopyWorkingDir(dir string) error {
	if common.Verbose() {
		log.Print("copying current directory for packaging")
	}

	// copy pwd into dir
	cmd := exec.Command("cp", "-a", ".", "--", dir)
	cmd.Dir = "."
	err := cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

func CopyFiles(files []string, dir string, fakeTimestamp bool) error {
	for _, file := range files {
		target := filepath.Join(dir, filepath.Dir(file))
		if common.Verbose() {
			log.Printf("copying %s to %s for packaging", file, target)
		}

		err := os.MkdirAll(target, 0755&os.ModePerm)
		if err != nil {
			return err
		}
		cmd := exec.Command("cp", "-a", "--", file, target)
		cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr
		err = cmd.Run()
		if err != nil {
			return err
		}

		err = makePathCacheable(dir, file, fakeTimestamp)
		if err != nil {
			return err
		}
	}

	return nil
}

func TarDirWithDockerfile(config common.Config, dockerFileContents *bytes.Buffer, dir string) (string, error) {
	if common.Verbose() {
		log.Printf("dockerfile:\n%v", dockerFileContents)
		log.Print("creating .tgz of files to send to docker")
	}

	if err := ioutil.WriteFile(filepath.Join(dir, "Dockerfile"), dockerFileContents.Bytes(), 0644&os.ModePerm); err != nil {
		return "", fmt.Errorf("could not write Dockerfile: %s", err)
	}

	// tar it all
	tarPath, err := createDirectoryTar(config, dir)
	if err != nil {
		return "", err
	}

	return tarPath, nil
}

func createDirectoryTar(config common.Config, dir string) (string, error) {
	file, err := ioutil.TempFile("", "manta-style")
	if err != nil {
		return "", err
	}
	file.Close()

	var tarFlags = "-pczf"
	// TODO: implement multiple verbose levels
	// if common.Verbose() {
	// 	tarFlags = "-pczvf"
	// }

	var cmd *exec.Cmd
	allParams := common.PrependArrayElements(config.TarExcludes, "--exclude")
	if !common.DockerInDockerMode() && config.TarExcludes == nil {
		allParams = append(allParams, "--exclude=.git")
	}
	allParams = append(allParams, tarFlags)
	allParams = append(allParams, file.Name())
	allParams = append(allParams, ".")
	cmd = exec.Command("tar", allParams...)
	cmd.Dir = dir
	cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr
	err = cmd.Run()
	if err != nil {
		return "", err
	}

	return file.Name(), nil
}

func makePathCacheable(dir string, path string, fakeTimestamp bool) error {
	target := filepath.Join(dir, path)
	fi, err := os.Stat(target)
	if err != nil {
		return err
	}

	if fi.IsDir() {
		filepath.Walk(target, func(absolutePath string, info os.FileInfo, err error) error {
			relativePath := strings.Replace(absolutePath, dir+"/", "", 1)
			return setGitTimestamp(dir, relativePath, fakeTimestamp)
		})
	} else {
		return setGitTimestamp(dir, path, fakeTimestamp)
	}

	return nil
}

func setGitTimestamp(dir string, file string, fakeTimestamp bool) error {
	var unixTime int64

	if !fakeTimestamp {
		// use git commands to discover file status in repository
		out := new(bytes.Buffer)
		cmd := exec.Command("git", "status", "--porcelain", "-z", "--", file)
		cmd.Dir = "."
		cmd.Stdout = out
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err != nil {
			return err
		}
		tokens := strings.Split(out.String(), "\x00")
		if len(tokens) > 2 { // multiple status lines, unexpected
			return fmt.Errorf("Unexpected git status output: %q", out.String())
		}
		if len(tokens) == 2 { // file modified from index or untracked
			// statusLine := tokens[0][:1]
			// if statusLine == ... {
			return nil // leave timestamp as is
			// }
		} // file unmodified from index
		// use git commands to discover timestamp of last commit
		out = new(bytes.Buffer)
		cmd = exec.Command("git", "rev-list", "--pretty=format:%at", "-n", "1", "HEAD", "--", file)
		cmd.Dir = "."
		cmd.Stdout = out
		cmd.Stderr = os.Stderr
		err = cmd.Run()
		if err != nil {
			return err
		}
		splitRevisionList := strings.SplitN(out.String(), "\n", 2)
		if len(splitRevisionList) != 2 {
			return fmt.Errorf("Unable to parse contents of git: %q", out.String())
		}

		unixTime, err = strconv.ParseInt(strings.TrimSpace(splitRevisionList[1]), 10, 64)
		if err != nil {
			return err
		}
	}
	timestamp := time.Unix(unixTime, 0)
	target := filepath.Join(dir, file)

	if common.Verbose() {
		log.Printf("setting timestamp of %s to %s to allow docker caching", target, timestamp)
	}

	if err := os.Chtimes(target, timestamp, timestamp); err != nil {
		return err
	}

	return nil
}
