package common

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func Verbose() bool {
	return os.Getenv("VERBOSE") == "1"
}

func UseProxy() bool {
	return os.Getenv("TWITCH_PROXY") == "1"
}

func PrivilegedMode() bool {
	return os.Getenv("PRIVILEGED") == "1"
}

func DockerInDockerMode() bool {
	return os.Getenv("DIND") == "1"
}

func NewFilteredWriter(out io.Writer) *FilteredWriter {
	return &FilteredWriter{out: out}
}

const TestImageName string = "ubuntu:trusty"

// TestOSRepoName: Should be the common repo name of the OS, such as: precise, trusty, etc.
const TestOSRepoName string = "trusty"
const TestOSName string = "ubuntu"

type Config struct {
	Image          string
	UpdateImage    bool `json:"update_image"`
	Env            interface{}
	EnvList        []string `json:"-"`
	BuildEnvList   []string `json:"-"`
	Mount          string
	OS             string            `json:"os"`
	SetupFiles     map[string]string `json:"setup_files"`
	Setup          []string
	Dependencies   []*DependenciesConfig `json:"dependencies"`
	Build          []string
	Extract        *Extract
	Ruby           *rubyConfig
	Yarn           *yarnConfig
	NetworkedBuild bool
	TarExcludes    []string `json:"tar_excludes"`
}

type Extract struct {
	Source      string
	Destination string
}

type rubyConfig struct {
	VendoredGems   []string `json:"vendored_gems,omitempty"`
	FakeTimestamp  bool     `json:"fake_timestamp,omitempty"`
	BundlerVersion string   `json:"bundler_version"`
}

type yarnConfig struct {
	FakeTimestamp bool   `json:"fake_timestamp,omitempty"`
	YarnVersion   string `json:"yarn_version"`
	IncludeBower  bool   `json:"include_bower"`
}

type DependenciesConfig struct {
	Repo       string `json:"repo"`
	SHA        string `json:"sha"`
	TargetPath string `json:"target_path"`
}

type FilteredWriter struct {
	out io.Writer
}

func (w *FilteredWriter) Write(b []byte) (int, error) {
	filteredLines := [][]byte{
		[]byte("ENV AWS_ACCESS_KEY_ID"),
		[]byte("ENV AWS_SECRET_ACCESS_KEY"),
	}
	for _, line := range filteredLines {
		i := bytes.Index(b, line)
		if i != -1 {
			b = append(b[0:i+len(line)+1], []byte("***\n")...)
			break
		}
	}
	return w.out.Write(b)
}

// PrependArrayElements - Prepends string s to every element of string array a
func PrependArrayElements(a []string, s string) []string {
	params := []string{}
	for _, v := range a {
		params = append(params, fmt.Sprintf("%v=%v", s, v))
	}
	return params
}
