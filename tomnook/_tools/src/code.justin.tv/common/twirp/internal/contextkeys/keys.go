// Package contextkeys stores the keys to the context accessor
// functions, letting generated code safely set values in contexts
// without exposing the setters to the outside world.
package contextkeys

// !! STOP !!
//
// If you're coming here to see how contexts should be used for your
// own libraries, keep looking elsewhere, amigo. This is not how most
// projects should use context keys!
//
// Lengthy Explanation from spencer@twitch.tv
// ================================================
//
// Typically, context keys should be private; this privacy is usually
// enforced with a private type. This is good because it prevents
// developers from mucking with the internals of your contexts, so you
// can use type-safe setters and getters.
//
// Here, though, we use the basic string type. This is done to allow
// generated code to use these values *without importing them*.
//
// A guiding principle for all of Twirp is to put as much as possible
// right in the generated code. The generated code should import as
// little as possible (the standard library gets a free pass).
//
// The reason for this principle is that that truly standalone
// generated code is much easier to work with because it doesn't need
// a particular version of any dependency. That's nice to have in a
// library, but for generated clients, it's more than that - it's
// actually reallly important. Otherwise, all servers and clients need
// to stay in sync together in both the version of their code
// generator *and* the versions of the imported dependencies! This can
// quickly get out of hand.
//
// This has actually come up in practice: upgrading gRPC + protobuf +
// generated code versions in code.justin.tv/video/digestion and
// code.justin.tv/video/goingest was nightmarish. So, the rule is that
// we should try to bundle everything into the one file. That way, the
// generated code stands on its own, and different applications and
// libraries can do their development in parallel, not in lockstep.
//
// The downside is that we get some funky structures like this
// one. These *must* remain publicly typed to avoid adding
// dependencies to generated code. The downside is that evil
// developers might abuse these strings and mess with stuff, which
// will probably break Twirp, but that's their own fault.

// It's okay to add keys here, but changing them would break the
// interaction between twirp and generated code. Don't change their
// values!
//
// Also, definitely don't use these keys yourself. The right way to
// access the values is with the accessor methods in
// code.justin.tv/common/twirp/context.go. Use those! Don't use these!
// These will only bring you pain and misfortune!
const (
	MethodNameKey     = "__tw_method_name"
	ServiceNameKey    = "__tw_service_name"
	StatusCodeKey     = "__tw_status_code"
	RequestHeaderKey  = "__tw_request_header"
	ResponseWriterKey = "__tw_response_writer"
)
