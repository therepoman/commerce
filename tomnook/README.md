![](http://vignette2.wikia.nocookie.net/animalcrossing/images/a/a3/400px-Tom_Nook_AF.png/revision/latest?cb=20160101020043&path-prefix=fr)

Tom Nook
========
*"Tom Nook, known in Japan as Tanukichi (たぬきち), is a fictional character in the Animal Crossing series who operates the village store." -Wikipedia*

Tom Nook is the generalized product information service for twitch commerce.
His job is to provide a centralized set of apis around sellable content
more specifically around:
* discovery (finding) of items
* viewing and pricing of items for users
* publishing of products for sale
* organizing of items (like amazon browse nodes)\

Design
------

The original design for Tom Nook can be found at: https://docs.google.com/document/d/1TSXHjOWICeaGfCeyuZDxBiKOQuETrNSvH7QhfvWKX3E

Developing
----------
Check out the project like a normal go project, set your GOPATH, and run make. Everything
should just happen automatically. If it doesn't, complain heavily and then fix it.