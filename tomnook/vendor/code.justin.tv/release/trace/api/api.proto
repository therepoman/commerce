syntax = "proto3";

package code.justin.tv.release.trace.api;
option go_package = "api";

import "code.justin.tv/release/trace/api/transaction.proto";

service Trace {
  rpc Firehose (FirehoseRequest) returns (stream Transaction) {}
}

message FirehoseRequest {
  Query query = 1;
  double sampling = 2;
  //uint64 partition = 3;
}

// A Query represents a collection of comparisons
// All comparisons in a Query must match on the same Call
// By default, subqueries in a Query must match the Call matched by the Query OR descendant subcalls of the matched Call
// This is overriden by the depth field
message Query {
  //enum Type {
  //  AND = 0;
  //  OR = 1;
  //}
  //Type type = 1;
  repeated Query subqueries = 2;
  repeated Comparison comparisons = 3;
  // depth restricts which Calls this query can match against, relative to the Call matched by the parent query
  // By default it is set to GREATER -1, causing the query to match against the parent Query's Call, or any descendant
  //IntegerComparison depth = 2;
}

// Comparison contains the various potential comparisons each Call can handle
// Servers MUST support multiple subcomparisons being set on a single Comparison
// Clients SHOULD NOT set multiple subcomparisons on a single Comparison
// Each subcomparison maps directly to the value of a field in a Call
// You can see their definitions in transaction.proto
message Comparison {
  // Inverts the result of all subcomparisons
  bool not = 1;

  StringComparison service_name = 20;
  StringComparison service_host = 21;
  IntegerComparison service_pid = 22;
  StringComparison service_peer = 23;

  IntegerComparison client_duration = 30;
  IntegerComparison server_duration = 31;

  StringComparison http_method = 40;
  //StringComparison http_uri_path = 41; // Removed due to PII concerns
  StringComparison http_route = 42;
  IntegerComparison http_status_code = 43;
}

message StringComparison {
  //enum Type {
  //  EQUALS = 0;
  //  LIKE = 1;
  //}
  //Type type = 1;
  string value = 2;
}

message IntegerComparison {
  enum Type {
    EQUALS = 0;
    GREATER = 1;
    LESSER = 2;
  }
  Type type = 1;
  sint64 value = 2;
}
