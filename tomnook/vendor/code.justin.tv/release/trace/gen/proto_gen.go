package gen

import (
	// Make sure this package is included in the vendor directory, as part of
	// a workaround for https://golang.org/issue/16580
	_ "code.justin.tv/common/chitin/grpctrace"

	// Tool to run protoc for the whole repo and add instrumentation to gRPC APIs
	_ "code.justin.tv/common/chitin/protoc_gen"
)

//go:generate go run ./vendor/code.justin.tv/common/chitin/protoc_gen/protoc.go -start ..
