package analysis

import (
	"testing"
	"time"
)

func TestTimingPercentile(t *testing.T) {
	tests := []tpTest{
		tpTest{
			times: []time.Duration{1 * time.Second, 2 * time.Second, 3 * time.Second},
			ptile: 0.5,
			want:  2 * time.Second,
		},
		tpTest{
			times: []time.Duration{1 * time.Second},
			ptile: 0.5,
			want:  1 * time.Second,
		},
		tpTest{
			times: []time.Duration{3 * time.Second, 2 * time.Second, 1 * time.Second},
			ptile: 0.5,
			want:  2 * time.Second,
		},
		tpTest{
			times: []time.Duration{3 * time.Second, 2 * time.Second, 1 * time.Second},
			ptile: 1.0,
			want:  3 * time.Second,
		},
	}
	for _, test := range tests {
		test.run(t)
	}
}

type tpTest struct {
	times []time.Duration
	ptile float64
	want  time.Duration
}

func (tpt tpTest) run(t *testing.T) {
	txs := make(txslice, len(tpt.times))
	for i, t := range tpt.times {
		txs[i] = newTxTaking(t, 0, t, i)
	}
	have := TimingPercentile(txs, tpt.ptile)
	if have != tpt.want {
		t.Errorf("have %v want %v", have, tpt.want)
	}
}
