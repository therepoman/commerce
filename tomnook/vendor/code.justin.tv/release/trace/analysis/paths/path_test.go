package paths

import "testing"

func TestPathMatches(t *testing.T) {
	cases := []struct {
		p1, p2 []uint32
		want   bool
	}{
		{[]uint32{}, []uint32{}, true},
		{[]uint32{1}, []uint32{1}, true},
		{[]uint32{1, 2, 3}, []uint32{1, 2, 3}, true},

		{[]uint32{}, []uint32{1}, false},
		{[]uint32{1}, []uint32{}, false},
		{[]uint32{1, 2, 3}, []uint32{3, 2, 1}, false},
		{[]uint32{1, 2, 3}, []uint32{1, 2}, false},
		{[]uint32{1, 2}, []uint32{1, 2, 3}, false},
	}
	for _, c := range cases {
		have := PathMatches(c.p1, c.p2)
		if have != c.want {
			t.Errorf("PathMatches(%q, %q) gave %t, want %t", c.p1, c.p2, have, c.want)
		}
	}
}

func TestMinLength(t *testing.T) {
	cases := []struct {
		p1, p2 []uint32
		want   int
	}{
		{[]uint32{}, []uint32{}, 0},
		{[]uint32{}, []uint32{1, 2, 3}, 0},
		{[]uint32{1, 2, 3}, []uint32{}, 0},
		{[]uint32{1, 2, 3}, []uint32{4, 5, 6}, 3},
		{[]uint32{1, 2, 3}, []uint32{4, 5, 6, 7, 8, 9}, 3},
		{[]uint32{1, 2, 3, 4, 5, 6}, []uint32{7, 8, 9}, 3},
	}
	for _, c := range cases {
		have := minLength(c.p1, c.p2)
		if have != c.want {
			t.Errorf("minLength(%q, %q) gave %d, want %d", c.p1, c.p2, have, c.want)
		}
	}
}

func TestIsChildPath(t *testing.T) {
	cases := []struct {
		p1, p2 []uint32
		want   bool
	}{
		{[]uint32{1, 2, 3}, []uint32{1, 2}, true},
		{[]uint32{1, 2}, []uint32{1, 2, 3}, false},
		{[]uint32{1, 2}, []uint32{1, 2}, false},
		{[]uint32{}, []uint32{}, false},
		{[]uint32{1, 2}, []uint32{}, true},
		{[]uint32{}, []uint32{1, 2}, false},
	}
	for _, c := range cases {
		have := IsChildPath(c.p1, c.p2)
		if have != c.want {
			t.Errorf("IsChildPath(%q, %q) gave %t, want %t", c.p1, c.p2, have, c.want)
		}
	}
}

func TestPathCompare(t *testing.T) {
	cases := []struct {
		p1, p2 []uint32
		want   int
	}{
		{[]uint32{}, []uint32{}, 0},
		{[]uint32{0, 1}, []uint32{0, 1}, 0},
		{[]uint32{0, 0}, []uint32{0, 1}, -1},
		{[]uint32{0, 0, 0}, []uint32{0, 1, 0}, -1},
		{[]uint32{1, 1}, []uint32{0, 1}, 1},
		{[]uint32{1}, []uint32{0, 1}, 1},
		{[]uint32{1}, []uint32{1, 0}, -1},
	}
	for _, c := range cases {
		have := PathCompare(c.p1, c.p2)
		if have != c.want {
			t.Errorf("PathCompare(%q, %q) gave %d, want %d", c.p1, c.p2, have, c.want)
		}
	}

}
