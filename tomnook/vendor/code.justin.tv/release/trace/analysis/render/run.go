// +build ignore

package main

import (
	"log"
	"os"
	"os/exec"
	"strconv"
	"time"
)

func main() {
	filename := "bindata.go"
	modtime := time.Date(2015, time.January, 1, 0, 0, 0, 0, time.UTC)

	cmd := exec.Command("go-bindata",
		"-o", filename,
		"-pkg", os.Getenv("GOPACKAGE"),
		"-modtime", strconv.FormatInt(modtime.UTC().Unix(), 10),
		"-ignore", "^bindata\\.",
		"-ignore", "\\.go$",
		"--", "./...")
	cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatalf("bindata err=%q", err)
	}
	err = exec.Command("gofmt", "-w", "--", filename).Run()
	if err != nil {
		log.Fatalf("gofmt err=%q", err)
	}
}
