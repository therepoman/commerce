package routes

import "testing"

func TestRouteMaps(t *testing.T) {
	type testCase struct {
		svc    string
		path   string
		method string
		want   string
	}

	tests := []testCase{
		{"code.justin.tv/web/web", "/lirik", "GET", "ember#ember"},
		{"code.justin.tv/web/web", "/chat/embed", "GET", "chat#embed"},
		{"code.justin.tv/web/web", "/", "GET", "front_page#front_page"},
		{"code.justin.tv/web/discovery", "/suggest", "GET", "suggest"},
		{"code.justin.tv/web/discovery", "/game/list", "GET", "game_list"},
		{"code.justin.tv/web/discovery", "/games", "PUT", "add_game"},
		{"code.justin.tv/release/skadi", "/v1/build", "GET", "/v1/build"},
		{"code.justin.tv/release/skadi", "/v1/candidate/previous", "OPTIONS", "/v1/candidate/previous"},
		{"code.justin.tv/release/skadi", "/v1/repos/foo/bar/compare/baz", "GET", "/v1/repos/{owner}/{repo}/compare/{commits}"},
		{"code.justin.tv/web/streams-api", "/kraken/streams", "GET", "streams"},

		// Visage's routes are split into multiple files. Confirm that we can
		// load routes from more than just one of them.
		{"code.justin.tv/edge/visage/cmd/visage", "/kraken/streams", "GET", "streams"},
		{"code.justin.tv/edge/visage/cmd/visage", "/v4/streams/followed", "GET", "followed"},

		// Confirm that methods are part of the equation.
		{"code.justin.tv/edge/visage/cmd/visage", "/v4/oauth2/clients/1", "GET", "v4/oauth2/clients#show"},
		{"code.justin.tv/edge/visage/cmd/visage", "/v4/oauth2/clients/1", "PUT", "v4/oauth2/clients#update"},
	}

	for _, tc := range tests {
		routemap, ok := Routes[tc.svc]
		if !ok {
			t.Errorf("missing routemap for service %q", tc.svc)
			continue
		}
		have := routemap.LookupTrimSlash(tc.path, tc.method)
		if have != tc.want {
			t.Errorf("route lookup failed for svc=%s path=%s method=%s, have=%q want=%q",
				tc.svc, tc.path, tc.method, have, tc.want,
			)
		}
	}
}
