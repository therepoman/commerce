package routes

import "testing"

const method = "GET"

func TestRouteMap(t *testing.T) {
	routemap := NewRouteMap()

	name := "api/twitch/viewer#language"
	routemap.AddRoute("/viewer/language(.:format)", method, name)
	routemap.AddRoute("/viewer/info(.:format)", method, "viewerinfo")
	assertLookup(routemap, "/viewer/language", name, t)
	assertLookup(routemap, "/viewer/language.json", name, t)

	name = "apps#xboxone"
	routemap.AddRoute("/apps/xboxone(/:view)(.:format)", method, name)
	assertLookup(routemap, "/apps/xboxone", name, t)
	assertLookup(routemap, "/apps/xboxone/view", name, t)
	assertLookup(routemap, "/apps/xboxone/view.json", name, t)
	assertLookup(routemap, "/apps/xboxone.json", name, t)
}

func assertLookup(rm RouteMap, path string, want string, t *testing.T) {
	have := rm.LookupTrimSlash(path, method)
	if have != want {
		t.Errorf("lookup  path=%q  have=%q  want=%q", path, have, want)
	}
}

func BenchmarkRailsAdminLookup(b *testing.B) {
	rm := Routes["code.justin.tv/web/web"]
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rm.LookupTrimSlash("/admin/dashboard", "GET")
	}
}

func BenchmarkRailsIndexLookup(b *testing.B) {
	rm := Routes["code.justin.tv/web/web"]
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rm.LookupTrimSlash("/", "GET")
	}
}

func BenchmarkRailsKrakenUser(b *testing.B) {
	rm := Routes["code.justin.tv/web/web"]
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rm.LookupTrimSlash("/kraken/user", "GET")
	}
}

func BenchmarkRailsUserChatBans(b *testing.B) {
	rm := Routes["code.justin.tv/web/web"]
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rm.LookupTrimSlash("/admin/users/spenczar5/chat_bans.json", "GET")
	}
}
