package callgraph

// Height returns the maximum depth (aka the height) of the call tree
func Height(n Node) int {
	var (
		childHeight        = 0
		tallestChildHeight = 0
	)
	for _, c := range n.Children() {
		childHeight = Height(c)
		if childHeight > tallestChildHeight {
			tallestChildHeight = childHeight
		}
	}
	return 1 + tallestChildHeight

}

// TreeSize returns the number of nodes in the call tree
func TreeSize(n Node) int {
	size := 1
	for _, c := range n.Children() {
		size += TreeSize(c)
	}
	return size
}

// Empty returns true if the node has no data
func Empty(n Node) bool {
	// If neither server nor client are set, then this is pretty much
	// empty.
	return n.Label(Client) == "" && n.Label(Server) == ""
}

// Walk applies a function to every node in the tree, depth-first
func Walk(n Node, f func(Node)) {
	for _, c := range n.Children() {
		Walk(c, f)
	}
	f(n)
}
