package callgraph

import (
	"strings"
	"time"
)

func trimRepo(s string) string {
	return strings.TrimPrefix(s, "code.justin.tv/")
}

// Duration returns the amount of time spent in this node of the call
// graph from the client's perspective. If there is incomplete data on
// the client's perspective, then the server's perceived duration is
// returned instead. If neither have complete data, then zero Duration
// is returned.
func Duration(n Node) time.Duration {
	var d int64
	if d = n.IntValue(ClientNanos); d != 0 {
		return time.Duration(d)
	}
	if d = n.IntValue(ServerNanos); d != 0 {
		return time.Duration(d)
	}
	return time.Duration(0)
}

// Start returns the start of the span according to the client if it's
// available, or server start if the data from the client isn't
// available. If neither is available, zero time is returned.
func Start(n Node) time.Time {
	var t int64
	if t = n.IntValue(ClientStart); t != 0 {
		return time.Unix(0, t)
	}
	if t = n.IntValue(ServerStart); t != 0 {
		return time.Unix(0, t)
	}
	return time.Time{}
}

// End returns the end timestamp of the span according to the client
// if it's available, or server end if the data from the client isn't
// available. If neither is available, zero time is returned.
func End(n Node) time.Time {
	var t int64
	if t = n.IntValue(ClientEnd); t != 0 {
		return time.Unix(0, t)
	}
	if t = n.IntValue(ServerEnd); t != 0 {
		return time.Unix(0, t)
	}
	return time.Time{}
}
