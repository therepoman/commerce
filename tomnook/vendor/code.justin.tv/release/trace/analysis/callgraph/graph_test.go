package callgraph

import "testing"

// Fulfills the Node interface
type simplenode struct {
	children []simplenode
}

func (n simplenode) String() string {
	self := "<N"
	for _, child := range n.children {
		self += child.String()
	}
	self += ">"
	return self
}

func (n simplenode) Children() []Node {
	children := make([]Node, len(n.children))
	for i := range n.children {
		children[i] = n.children[i]
	}
	return children
}

func (n simplenode) Label(dim StringDimension) string {
	return "x"
}

func (n simplenode) IntValue(dim Int64Dimension) int64 {
	return 1
}

func TestHeight(t *testing.T) {
	tree := simplenode{}
	assertHeight(tree, 1, t)

	tree = simplenode{[]simplenode{
		simplenode{},
	}}
	assertHeight(tree, 2, t)

	tree = simplenode{[]simplenode{
		simplenode{},
		simplenode{},
	}}
	assertHeight(tree, 2, t)

	tree = simplenode{[]simplenode{
		simplenode{[]simplenode{
			simplenode{},
		}},
		simplenode{},
	}}
	assertHeight(tree, 3, t)

	tree = simplenode{[]simplenode{
		simplenode{[]simplenode{
			simplenode{},
		}},
		simplenode{[]simplenode{
			simplenode{},
		}},
	}}
	assertHeight(tree, 3, t)
}

func assertHeight(tree simplenode, want int, t *testing.T) {
	have := Height(tree)
	if have != want {
		t.Errorf("height tree=%v have=%d  want=%d", tree, have, want)
	}
}

func TestSize(t *testing.T) {
	tree := simplenode{}
	assertSize(tree, 1, t)

	tree = simplenode{[]simplenode{
		simplenode{},
	}}
	assertSize(tree, 2, t)

	tree = simplenode{[]simplenode{
		simplenode{},
		simplenode{},
	}}
	assertSize(tree, 3, t)
	tree = simplenode{[]simplenode{
		simplenode{[]simplenode{
			simplenode{},
		}},
		simplenode{},
	}}
	assertSize(tree, 4, t)
}

func assertSize(tree simplenode, want int, t *testing.T) {
	have := TreeSize(tree)
	if have != want {
		t.Errorf("size tree=%v have=%d  want=%d", tree, have, want)
	}
}
