package callgraph

// stringDimensions are retrievable string values for a Node
type StringDimension int

const (
	Server StringDimension = iota
	ServerHost
	Client
	ClientHost
	Signature
	RPCName
)

type Int64Dimension int

const (
	ClientNanos Int64Dimension = iota
	ClientStart
	ClientEnd
	ClientPid
	ServerNanos
	ServerStart
	ServerEnd
	ServerPid
)
