package callgraph

import (
	"testing"
	"time"

	"code.justin.tv/release/trace/pbmsg"
)

var (
	_early  time.Time = time.Unix(0, 0)
	_middle time.Time = time.Unix(100, 0)
	_late   time.Time = time.Unix(200, 0)
)

func eventWithTime(ts time.Time) *pbmsg.Event {
	ns := ts.UnixNano()
	return &pbmsg.Event{
		Time: &ns,
	}
}

func TestMaxTime(t *testing.T) {
	cases := []struct {
		events []*pbmsg.Event
		want   time.Time
	}{
		{[]*pbmsg.Event{eventWithTime(_early), eventWithTime(_middle), eventWithTime(_late)}, _late},
		{[]*pbmsg.Event{eventWithTime(_late), eventWithTime(_middle), eventWithTime(_early)}, _late},
		{[]*pbmsg.Event{eventWithTime(_late), eventWithTime(_late)}, _late},
		{[]*pbmsg.Event{eventWithTime(_early)}, _early},
	}
	for _, c := range cases {
		have := maxTime(c.events)
		if have != c.want {
			t.Errorf("maxTime(%q) gave %s, want %s", c.events, have, c.want)
		}

	}
}

func TestMinTime(t *testing.T) {
	cases := []struct {
		events []*pbmsg.Event
		want   time.Time
	}{
		{[]*pbmsg.Event{eventWithTime(_early), eventWithTime(_middle), eventWithTime(_late)}, _early},
		{[]*pbmsg.Event{eventWithTime(_late), eventWithTime(_middle), eventWithTime(_early)}, _early},
		{[]*pbmsg.Event{eventWithTime(_late), eventWithTime(_late)}, _late},
		{[]*pbmsg.Event{eventWithTime(_early)}, _early},
	}
	for _, c := range cases {
		have := minTime(c.events)
		if have != c.want {
			t.Errorf("minTime(%q) gave %s, want %s", c.events, have, c.want)
		}

	}
}
