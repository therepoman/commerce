package analysis

import (
	"time"

	"code.justin.tv/release/trace/analysis/tx"
	"code.justin.tv/release/trace/pbmsg"
)

var (
	mockPid int32 = 1

	// service name defaults
	mockClient = "client"
	mockServer = "server"

	// timing defaults
	mockClientDuration = time.Millisecond * 100
	mockServerDelay    = time.Millisecond * 10
	mockServerDuration = time.Millisecond * 80
)

// Implements collections.TransactionSet
type txslice []*tx.Transaction

func (txs txslice) Add(tx *tx.Transaction) { txs = append(txs, tx) }
func (txs txslice) All() []*tx.Transaction { return txs }
func (txs txslice) Len() int               { return len(txs) }
func (txs txslice) Sorted(o tx.SortOrder) []*tx.Transaction {
	return tx.Sort(o, txs)
}

func newTxWithEvents(events []*pbmsg.Event, id int) *tx.Transaction {
	tx := tx.NewTransaction(intToTxid(id))
	for _, e := range events {
		tx.Add(e)
	}
	return tx
}

func newTxBetween(clientName, serverName string, id int) *tx.Transaction {
	events := []*pbmsg.Event{
		event{client, request, head, id, 0, clientName}.event(),
		event{client, response, head, id, 1, clientName}.event(),
		event{server, request, head, id, 2, serverName}.event(),
		event{server, response, head, id, 3, serverName}.event(),
	}
	return newTxWithEvents(events, id)
}

func newTxAt(t time.Time, id int) *tx.Transaction {
	ts := [4]int64{
		t.UnixNano(),
		t.Add(mockClientDuration).UnixNano(),
		t.Add(mockServerDelay).UnixNano(),
		t.Add(mockServerDelay).Add(mockServerDuration).UnixNano(),
	}

	events := []*pbmsg.Event{
		event{client, request, head, id, ts[0], mockClient}.event(),
		event{client, response, head, id, ts[1], mockClient}.event(),
		event{server, request, head, id, ts[2], mockServer}.event(),
		event{server, response, head, id, ts[3], mockServer}.event(),
	}

	return newTxWithEvents(events, id)
}

// newTxTaking returns a transaction that took `client` Duration to
// complete for the client, took `delay` Duration to reach the server,
// and took `server` Duration for the server to process.
func newTxTaking(clientDur, delay, serverDur time.Duration, id int) *tx.Transaction {
	t := time.Unix(0, 0)
	ts := [4]time.Time{
		t,
		t.Add(clientDur),
		t.Add(delay),
		t.Add(delay).Add(serverDur),
	}

	events := []*pbmsg.Event{
		event{client, request, head, id, ts[0].UnixNano(), mockClient}.event(),
		event{client, response, head, id, ts[1].UnixNano(), mockClient}.event(),
		event{server, request, head, id, ts[2].UnixNano(), mockServer}.event(),
		event{server, response, head, id, ts[3].UnixNano(), mockServer}.event(),
	}

	return newTxWithEvents(events, id)
}

func intToTxid(i int) tx.TransactionID {
	return tx.TransactionID{0: 0, 1: uint64(i)}
}

// helper struct to manage creation of pbmsg.Event structs
type event struct {
	end       end
	phase     phase
	payload   payload
	id        int
	timestamp int64
	svc       string
}

type end int

const (
	client end = iota
	server
)

type phase int

const (
	request phase = iota
	response
)

type payload int

const (
	head payload = iota
	body
)

func (es event) event() *pbmsg.Event {
	host := es.svc + ".host"
	ev := &pbmsg.Event{
		Kind:          es.kind(),
		Time:          &es.timestamp,
		Pid:           &mockPid,
		TransactionId: es.txid(),
		Hostname:      &host,
		Svcname:       &es.svc,
	}
	return ev
}

func (es event) txid() []uint64 {
	txid := intToTxid(es.id)
	return []uint64{txid[0], txid[1]}
}

func (es event) kind() *pbmsg.Event_Kind {
	type key struct {
		phase
		end
		payload
	}
	return map[key]*pbmsg.Event_Kind{
		key{request, client, head}:  pbmsg.Event_REQUEST_HEAD_PREPARED.Enum(),
		key{request, client, body}:  pbmsg.Event_REQUEST_BODY_SENT.Enum(),
		key{request, server, head}:  pbmsg.Event_REQUEST_HEAD_RECEIVED.Enum(),
		key{request, server, body}:  pbmsg.Event_REQUEST_BODY_RECEIVED.Enum(),
		key{response, client, head}: pbmsg.Event_RESPONSE_HEAD_RECEIVED.Enum(),
		key{response, client, body}: pbmsg.Event_RESPONSE_BODY_RECEIVED.Enum(),
		key{response, server, head}: pbmsg.Event_RESPONSE_HEAD_PREPARED.Enum(),
		key{response, server, body}: pbmsg.Event_RESPONSE_BODY_SENT.Enum(),
	}[key{es.phase, es.end, es.payload}]
}
