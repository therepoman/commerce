package analysis

import "testing"

func TestSplitByService(t *testing.T) {
	txs := txslice{
		newTxBetween("client1", "server1", 0),
		newTxBetween("client1", "server1", 1),
		newTxBetween("client1", "server2", 2),
		newTxBetween("client2", "server2", 3),
		newTxBetween("client3", "server3", 4),
		newTxBetween("client1", "server4", 5),
	}

	byServer := SplitByServer(txs)
	type testcase struct {
		svc   string
		count int
	}
	testcases := []testcase{
		testcase{"server1", 2},
		testcase{"server2", 2},
		testcase{"server3", 1},
		testcase{"server4", 1},
		testcase{"client1", 0},
	}
	for _, tc := range testcases {
		if tc.count == 0 {
			if byServer[tc.svc] != nil {
				t.Errorf("%s: have %+v, want nil", tc.svc, byServer[tc.svc])
			}
			continue
		}
		if byServer[tc.svc] == nil {
			t.Errorf("%s: have 0, want %d", tc.svc, tc.count)
			continue
		}

		if (byServer[tc.svc].Len()) != tc.count {
			t.Errorf("%s: have %d, want %d",
				tc.svc, byServer[tc.svc].Len(), tc.count,
			)
		}
	}
}
