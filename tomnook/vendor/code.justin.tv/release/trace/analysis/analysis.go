package analysis

import (
	"math"
	"time"

	"math/rand"

	"code.justin.tv/release/trace/analysis/tx"
)

// TimingPercentile returns the pth percentile duration of the
// transactions in the given TransactionSet, sorted from fastest up to
// slowest (so p=0 will give the minimum value, for example). If the
// collection has no valid times, zero duration is returned.
func TimingPercentile(txs tx.TransactionSet, p float64) time.Duration {
	tx := Percentile(txs, p, tx.ByTime)
	return tx.RootDuration()
}

// ExtractTimestamps returns an unordered slice of time.Durations from
// the given TransactionSet
func ExtractTimestamps(txs tx.TransactionSet) []time.Duration {
	out := make([]time.Duration, 0)
	for _, tx := range txs.All() {
		out = append(out, tx.RootDuration())
	}
	return out
}

type TimeBucket struct {
	Start, End time.Duration
	Count      int
}

// Bucketize returns time.Durations, grouped into log-log buckets.
func Bucketize(sorted []time.Duration) []TimeBucket {
	if len(sorted) == 0 {
		return nil
	}

	max := sorted[len(sorted)-1]

	var buckets []TimeBucket

	const (
		bucketsPerDecade = 10
	)

	for i, bkt := 0, 0; ; bkt++ {
		var (
			start = time.Duration(math.Exp2(float64(bkt+0)*math.Log2(10)/bucketsPerDecade)) * time.Nanosecond
			end   = time.Duration(math.Exp2(float64(bkt+1)*math.Log2(10)/bucketsPerDecade)) * time.Nanosecond
		)
		if start > max {
			break
		}
		if start == end {
			// skip over the start == end == 1 buckets
			continue
		}

		count := 0
		for ; i < len(sorted); i++ {
			curr := sorted[i]
			if curr >= end {
				break
			}
			count++
		}
		buckets = append(buckets, TimeBucket{
			Start: start,
			End:   end,
			Count: count,
		})
	}

	return buckets
}

// Sample takes n random values from a TransactionSet, sampled with
// replacement.
func Sample(txset tx.TransactionSet, n int) []*tx.Transaction {
	txs := txset.All()
	indexes := make([]int, n)
	maxIdx := len(txs)
	for i := 0; i < n; i++ {
		indexes[i] = int(rand.Int63n(int64(maxIdx)))
	}

	out := make([]*tx.Transaction, n)
	for i, idx := range indexes {
		out[i] = txs[idx]
	}

	return out
}

// Percentile returns the pth percentile greatest value in the given
// txset, sorted by the given SortOrder.
func Percentile(txset tx.TransactionSet, p float64, order tx.SortOrder) *tx.Transaction {
	txs := txset.Sorted(order)
	var idx int = int(p * float64(len(txs)-1))
	return txs[idx]
}
