// package pipeline contains utilities for working with Trace's data pipeline.
package pipeline

import (
	"log"

	"golang.org/x/net/context"
)

type ErrorHandler func(error)

// LogErrors is an ErrorHandler which calls l.Printf with errors.
func LogErrors(l *log.Logger) ErrorHandler {
	return func(e error) {
		l.Printf("Error: %s", e)
	}
}

// IgnoreErrors ignores all errors.
func IgnoreErrors(error) {}

// StoreErrors appends errors to a provided slice.
func StoreErrors() (ErrorHandler, *[]error) {
	es := make([]error, 0)
	return func(e error) {
		es = append(es, e)
	}, &es
}

// AbortOnError creates a child context which will be canceled in case
// of error.
func AbortOnError(parent context.Context) (ErrorHandler, context.Context) {
	child, cancel := context.WithCancel(parent)
	return func(e error) {
		cancel()
	}, child
}

// CombinedHandler combines multiple ErrorHandlers to be executed in
// order in case of errors.
func CombinedHandler(hs ...ErrorHandler) ErrorHandler {
	return func(e error) {
		for _, f := range hs {
			f(e)
		}
	}
}
