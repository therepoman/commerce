// package awsutils provides a helper for Chitin-ifying an AWS service configuration
package awsutils

import (
	"net"
	"net/http"
	"time"

	"code.justin.tv/common/chitin"
	"github.com/aws/aws-sdk-go/aws"
	"golang.org/x/net/context"
)

const (
	// We don't want our API calls to take an unbounded amount of time
	// if the AWS API (or our connection to it) is acting up.
	httpRequestTimeout = 10 * time.Second
)

// WithChitin configures an aws.Config to use an HTTP client that will
// cancel requests when the context is canceled. Additionally, it will
// configure the transport to maintain a large pool of keepalive
// connections, which will avoid spending a ton of time in TLS
// negotiation.
//
// The chitin API we call here returns an error if the
// http.RoundTripper doesn't implement the required additional methods
// CancelRequest and CloseIdleConnections. Since we're passing in a
// *http.Transport, we know we won't get an error. Panic if that
// somehow changes.
//
// The values for Timeout and KeepAlive in the dialer and for
// TLSHandshakeTimeout in the transport are taken from the values used
// in net/http.DefaultTransport.
func WithChitin(ctx context.Context, cfg *aws.Config) *aws.Config {
	rt, err := chitin.RoundTripper(ctx, &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		Dial: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 10 * time.Second,
		MaxIdleConnsPerHost: 1000,
	})
	if err != nil {
		panic("chitin.RoundTripper rejected *net/http.Transport")
	}
	cli := &http.Client{
		Transport: rt,
		Timeout:   httpRequestTimeout,
	}
	return cfg.WithHTTPClient(cli)
}
