package kinesalite

import "testing"

// Make sure our test helpers work and don't panic
func TestKinesisSinkTestHelpers(t *testing.T) {
	_ = Setup(t, "stream", 1)
	defer Teardown(t, "stream")
}
