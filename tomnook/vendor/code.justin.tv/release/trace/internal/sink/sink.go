package sink

import (
	"code.justin.tv/ear/kinesis/combiner"
)

type Sink func(<-chan *combiner.Blob)
