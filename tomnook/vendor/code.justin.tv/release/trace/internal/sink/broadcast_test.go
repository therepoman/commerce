package sink

import (
	"sync/atomic"
	"testing"
	"time"

	"golang.org/x/net/context"

	"code.justin.tv/ear/kinesis/combiner"
)

func TestBroadcast(t *testing.T) {
	var nSink = 5

	counters := make([]int64, nSink)
	sinks := make([]Sink, nSink)
	for i := range sinks {
		sinks[i] = counterSink(&counters[i])
	}

	want := 20
	blobs, _ := fakeBlobs(want)
	close(blobs)

	Broadcast(blobs, sinks...)

	for i, c := range counters {
		if c != int64(want) {
			t.Errorf("sink%d received %d events, want %d", i, c, want)
		}
	}
}

func TestBroadcastNoBackpressure(t *testing.T) {
	var counter int64
	ctx, cancel := context.WithCancel(context.Background())

	buffer := 10
	blobs := make(chan *combiner.Blob, buffer)
	want := buffer * 2

	broadcastDone := make(chan struct{})
	go func() {
		Broadcast(blobs, counterSink(&counter), cloggedSink(ctx))
		close(broadcastDone)
	}()

	addBlobsDone := make(chan struct{})
	go func() {
		for i := 0; i < want; i++ {
			blobs <- &combiner.Blob{}
		}
		close(blobs)
		close(addBlobsDone)
	}()

	select {
	case <-addBlobsDone:
	case <-time.After(time.Duration(want) * 50 * time.Millisecond):
		t.Fatalf("Timed out adding blobs. Sinks appear to be clogged.")
	}

	// Verify that the broadcaster is still blocked because of a
	// clogged sink
	select {
	case <-broadcastDone:
		t.Fatalf("broadcast sink isn't actually blocked by clogged sink")
	default:
	}

	// Sleep a bit to let messages get through to the counter
	time.Sleep(time.Duration(want) * 5 * time.Millisecond)
	if raceenabled {
		// Sleep a little longer when running with the (slow) race detector
		time.Sleep(time.Duration(want) * 5 * time.Millisecond)
	}

	// Check that the counter is processing happily
	if have := atomic.LoadInt64(&counter); have != int64(want) {
		t.Errorf("counter sink did not process while another sink was clogged have=%d want=%d", have, want)
		t.Logf("stats: %+v", expStats.Get("Broadcast"))
	}

	// Unblock the clogged sink
	cancel()

	// Verify that this unblocks the broadcaster
	select {
	case <-broadcastDone:
	case <-time.After(time.Millisecond * 250):
		t.Errorf("broadcast sink is still blocked by clogged sink")
		t.Logf("stats: %+v", expStats.Get("Broadcast"))
	}
}

// a sink that blocks until its context is canceled
func cloggedSink(ctx context.Context) Sink {
	return func(data <-chan *combiner.Blob) {
		<-ctx.Done()
	}
}
