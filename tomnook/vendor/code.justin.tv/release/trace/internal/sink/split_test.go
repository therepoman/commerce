package sink

import (
	"fmt"
	"math"
	"sync/atomic"
	"testing"

	"code.justin.tv/ear/kinesis/combiner"
)

func TestSplitSink(t *testing.T) {
	var (
		nInput = 100
		p      = 0.5
	)
	left := &appendSink{}
	right := &appendSink{}

	split := SplitSink(p, left.run, right.run)
	blobs, want := fakeBlobs(nInput)
	close(blobs)
	split(blobs)

	haveLeft := make(map[string]int)
	haveRight := make(map[string]int)
	for _, b := range left.recvd {
		data := string(b.Data)
		haveLeft[data] += 1
		if haveLeft[data] > 1 {
			t.Errorf("left received blob %s %d times", data, haveLeft[data])
		}
	}
	for _, b := range right.recvd {
		data := string(b.Data)
		haveRight[data] += 1
		if haveRight[data] > 1 {
			t.Errorf("right received blob %s %d times", data, haveRight[data])
		}
	}

	for _, bytes := range want {
		data := string(bytes)
		_, okLeft := haveLeft[data]
		_, okRight := haveRight[data]

		if okLeft && okRight {
			t.Errorf("both received blob %s", data)
		}
		if !okLeft && !okRight {
			t.Errorf("neither received blob %s", data)
		}
	}
}

type appendSink struct {
	recvd []*combiner.Blob
}

func (s *appendSink) run(data <-chan *combiner.Blob) {
	for b := range data {
		s.recvd = append(s.recvd, b)
	}
}

func TestSplitSinkRNG(t *testing.T) {

	type testcase struct {
		p      float64
		nShard int
	}

	tcs := []testcase{
		testcase{0.0, 1024},
		testcase{1.0, 1024},
		testcase{0.5, 1024},
		testcase{0.1, 1024},
		testcase{0.9, 1024},
	}
	for _, tc := range tcs {
		var (
			nInput           = 10000
			nRecvLeft  int64 = 0
			nRecvRight int64 = 0
			pLeft            = tc.p
			wantLeft         = pLeft * float64(nInput)
			wantRight        = (1 - pLeft) * float64(nInput)
			// Allow 3 stdevs of deviation. This should be good about
			// 99.7% of the time.
			errLeft  = 3 * math.Sqrt(wantLeft)
			errRight = 3 * math.Sqrt(wantRight)
		)

		left := counterSink(&nRecvLeft)
		right := counterSink(&nRecvRight)

		blobs, _ := fakeBlobsSetShard(nInput, tc.nShard)
		close(blobs)

		s := SplitSink(pLeft, left, right)
		s(blobs)

		haveLeft := atomic.LoadInt64(&nRecvLeft)
		deltaLeft := math.Abs(float64(haveLeft - int64(wantLeft)))
		if deltaLeft > errLeft {
			t.Errorf("unexpected fraction on left side pLeft=%f have=%d want=%d allowed err=%f", pLeft, haveLeft, int64(wantLeft), errLeft)
		}

		haveRight := atomic.LoadInt64(&nRecvRight)
		deltaRight := math.Abs(float64(haveRight - int64(wantRight)))
		if deltaRight > errRight {
			t.Errorf("unexpected fraction on right side pLeft=%f have=%d want=%d allowed err=%f", pLeft, haveRight, int64(wantRight), errRight)
		}
	}
}

func BenchmarkSplitSinkRand(b *testing.B) {
	s := newSplitSink(0, nil, nil)
	blob := &combiner.Blob{Key: "1024"}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s.rand(blob)
	}
}

// generate nBlob blobs with nShard unique shards
func fakeBlobsSetShard(nBlob, nShard int) (ch chan *combiner.Blob, want [][]byte) {
	ch = make(chan *combiner.Blob, nBlob)
	want = make([][]byte, nBlob)
	for i := 0; i < nBlob; i++ {
		want[i] = []byte(fmt.Sprintf("data-%d", i))
		ch <- &combiner.Blob{
			Data: want[i],
			Key:  fmt.Sprintf("%d", i%nShard),
		}
	}
	return ch, want
}
