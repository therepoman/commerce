package scanproto

import (
	"testing"

	"code.justin.tv/release/trace/internal/kinesalite"
)

// Make sure our test helpers work and don't panic
func TestScanprotoKinesisTestHelpers(t *testing.T) {
	_ = kinesalite.Setup(t, "stream", 1)
	defer kinesalite.Teardown(t, "stream")
}

// ListShards should get all shards
func TestListShards(t *testing.T) {
	var (
		stream = "test-list-shards"
		n      = maxShardsPerDescribe * 2
	)

	k := kinesalite.Setup(t, stream, n)
	defer kinesalite.Teardown(t, stream)

	shards, err := listShards(k, stream)
	if err != nil {
		t.Fatalf("listShards err=%q", err)
	}

	uniqueShards := make(map[string]struct{})
	for _, s := range shards {
		uniqueShards[s] = struct{}{}
	}
	if len(uniqueShards) != n {
		t.Errorf("did not get all shards have=%d wante=%d", len(shards), n)
	}
}
