package scanproto

import (
	"code.justin.tv/release/trace/api"
	"code.justin.tv/release/trace/pbmsg"
)

// LegacyTransactionSource provides a channel of *pbmsg.Transactions. The channel is
// accessible immediately, but the LegacyTransactionSource doesn't start populating
// it with data until the Run() function is called.
type LegacyTransactionSource interface {
	// Run is a blocking call which writes data into the
	// LegacyTransactionSource's channel
	Run()
	Transactions() <-chan *pbmsg.Transaction
	Errors() <-chan error
	Stop() // a blocking call which closes inputs and its events channel
}

// TransactionSource provides a channel of *api.Transactions. The channel is
// accessible immediately, but the TransactionSource doesn't start populating
// it with data until the Run() function is called.
type TransactionSource interface {
	// Run is a blocking call which writes data into the
	// TransactionSource's channel
	Run()
	Transactions() <-chan *api.Transaction
	Errors() <-chan error
	Stop() // a blocking call which closes inputs and its events channel
}
