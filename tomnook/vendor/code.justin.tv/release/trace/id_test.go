package trace

import (
	"reflect"
	"testing"
)

func TestParseID(t *testing.T) {
	var idTests = []struct {
		str  string
		txid *ID
	}{
		{
			str:  "4e61bc00000000000900000000000000.6.7.8",
			txid: &ID{txID: [2]uint64{12345678, 9}, path: []uint32{6, 7, 8}},
		},
		{
			str:  "4e61bc0000000000090000000000000.6.7.8",
			txid: nil,
		},
		{
			str:  "4e61bc00000000000900000000000000q.6.7.8",
			txid: nil,
		},
		{
			str:  "4e61bc00000000000900000000000000.6a.7.8",
			txid: nil,
		},
		{
			str:  "4e61bc0000000000090000000000000",
			txid: nil,
		},
		{
			str:  "4e61bc00000000000900000000000000",
			txid: &ID{txID: [2]uint64{12345678, 9}, path: []uint32(nil)},
		},
	}

	for _, tt := range idTests {
		if have, want := ParseID(tt.str), tt.txid; !reflect.DeepEqual(have, want) {
			t.Errorf("ParseID(%q); %#v != %#v", tt.str, have, want)
		}
	}
}

func TestIDString(t *testing.T) {
	var idTests = []struct {
		str  string
		txid *ID
	}{
		{
			txid: &ID{txID: [2]uint64{12345678, 9}, path: []uint32{6, 7, 8}},
			str:  "4e61bc00000000000900000000000000.6.7.8",
		},
		{
			txid: nil,
			str:  "",
		},
		{
			txid: &ID{txID: [2]uint64{12345678, 9}, path: []uint32(nil)},
			str:  "4e61bc00000000000900000000000000",
		},
		{
			txid: &ID{txID: [2]uint64{12345678, 9}, path: []uint32{}},
			str:  "4e61bc00000000000900000000000000",
		},
	}

	for _, tt := range idTests {
		if have, want := tt.txid.String(), tt.str; have != want {
			t.Errorf("(%#v).String(); %q != %q", tt.txid, have, want)
		}
	}
}
