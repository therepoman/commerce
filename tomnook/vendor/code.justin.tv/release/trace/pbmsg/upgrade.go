package pbmsg

import (
	"code.justin.tv/release/trace/api"
)

func UpgradeQuery(in *Query) *api.Query {
	if in == nil {
		return nil
	}

	var q api.Query

	for _, sub := range in.Subqueries {
		q.Subqueries = append(q.Subqueries, UpgradeQuery(sub))
	}
	for _, c := range in.Comparisons {
		q.Comparisons = append(q.Comparisons, upgradeComparison(c))
	}

	return &q
}

func upgradeComparison(in *Comparison) *api.Comparison {
	if in == nil {
		return nil
	}

	var c api.Comparison

	c.Not = in.Not
	c.ServiceName = upgradeStringComparison(in.ServiceName)
	c.ServiceHost = upgradeStringComparison(in.ServiceHost)
	c.ServicePid = upgradeIntegerComparison(in.ServicePid)
	c.ServicePeer = upgradeStringComparison(in.ServicePeer)

	c.ClientDuration = upgradeIntegerComparison(in.ClientDuration)
	c.ServerDuration = upgradeIntegerComparison(in.ServerDuration)

	c.HttpMethod = upgradeStringComparison(in.HttpMethod)
	c.HttpRoute = upgradeStringComparison(in.HttpRoute)
	c.HttpStatusCode = upgradeIntegerComparison(in.HttpStatusCode)

	return &c
}

func upgradeStringComparison(in *StringComparison) *api.StringComparison {
	if in == nil {
		return nil
	}
	return &api.StringComparison{Value: in.Value}
}

func upgradeIntegerComparison(in *IntegerComparison) *api.IntegerComparison {
	if in == nil {
		return nil
	}
	return &api.IntegerComparison{Value: in.Value}
}
