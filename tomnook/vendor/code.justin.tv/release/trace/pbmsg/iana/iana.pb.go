// Code generated by protoc-gen-go.
// source: code.justin.tv/release/trace/pbmsg/iana/iana.proto
// DO NOT EDIT!

/*
Package iana is a generated protocol buffer package.

It is generated from these files:
	code.justin.tv/release/trace/pbmsg/iana/iana.proto

It has these top-level messages:
*/
package iana

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// https://www.iana.org/assignments/http-methods/http-methods.xhtml
type HTTPMethod int32

const (
	// invalid, default value
	HTTPMethod_UNKNOWN HTTPMethod = 0
	// https://tools.ietf.org/html/rfc7231#section-4.3.1  safe, idempotent
	HTTPMethod_GET HTTPMethod = 1
	// https://tools.ietf.org/html/rfc7231#section-4.3.2  safe, idempotent
	HTTPMethod_HEAD HTTPMethod = 2
	// https://tools.ietf.org/html/rfc7231#section-4.3.3
	HTTPMethod_POST HTTPMethod = 3
	// https://tools.ietf.org/html/rfc7231#section-4.3.4  idempotent
	HTTPMethod_PUT HTTPMethod = 4
	// https://tools.ietf.org/html/rfc7231#section-4.3.5  idempotent
	HTTPMethod_DELETE HTTPMethod = 5
	// https://tools.ietf.org/html/rfc7231#section-4.3.6
	HTTPMethod_CONNECT HTTPMethod = 6
	// https://tools.ietf.org/html/rfc7231#section-4.3.7  safe, idempotent
	HTTPMethod_OPTIONS HTTPMethod = 7
	// https://tools.ietf.org/html/rfc7231#section-4.3.8  safe, idempotent
	HTTPMethod_TRACE HTTPMethod = 8
	// https://tools.ietf.org/html/rfc5789
	HTTPMethod_PATCH HTTPMethod = 9
)

var HTTPMethod_name = map[int32]string{
	0: "UNKNOWN",
	1: "GET",
	2: "HEAD",
	3: "POST",
	4: "PUT",
	5: "DELETE",
	6: "CONNECT",
	7: "OPTIONS",
	8: "TRACE",
	9: "PATCH",
}
var HTTPMethod_value = map[string]int32{
	"UNKNOWN": 0,
	"GET":     1,
	"HEAD":    2,
	"POST":    3,
	"PUT":     4,
	"DELETE":  5,
	"CONNECT": 6,
	"OPTIONS": 7,
	"TRACE":   8,
	"PATCH":   9,
}

func (x HTTPMethod) Enum() *HTTPMethod {
	p := new(HTTPMethod)
	*p = x
	return p
}
func (x HTTPMethod) String() string {
	return proto.EnumName(HTTPMethod_name, int32(x))
}
func (x *HTTPMethod) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(HTTPMethod_value, data, "HTTPMethod")
	if err != nil {
		return err
	}
	*x = HTTPMethod(value)
	return nil
}
func (HTTPMethod) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func init() {
	proto.RegisterEnum("code.justin.tv.release.trace.pbmsg.iana.HTTPMethod", HTTPMethod_name, HTTPMethod_value)
}

var fileDescriptor0 = []byte{
	// 203 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x54, 0xce, 0xcd, 0x4a, 0xc5, 0x30,
	0x10, 0x05, 0x60, 0xf5, 0xf6, 0xf6, 0x67, 0xdc, 0x0c, 0x79, 0x09, 0xc1, 0x45, 0x02, 0xbe, 0x41,
	0x4d, 0x83, 0x15, 0x35, 0x09, 0x76, 0x8a, 0xe0, 0x2e, 0xb6, 0x41, 0x2b, 0xda, 0x94, 0x36, 0xba,
	0xf1, 0xe5, 0x25, 0xc5, 0x8d, 0x9b, 0xe1, 0x0c, 0x33, 0x1f, 0x1c, 0xb8, 0x1a, 0xc2, 0xe8, 0xf9,
	0xfb, 0xd7, 0x16, 0xa7, 0x99, 0xc7, 0x6f, 0xb1, 0xfa, 0x0f, 0xef, 0x36, 0x2f, 0xe2, 0xea, 0x06,
	0x2f, 0x96, 0x97, 0xcf, 0xed, 0x55, 0x4c, 0x6e, 0x76, 0xfb, 0xe0, 0xcb, 0x1a, 0x62, 0x60, 0x17,
	0xff, 0x0d, 0xff, 0x33, 0x7c, 0x37, 0x7c, 0x37, 0x3c, 0xbd, 0x5f, 0xfe, 0x00, 0xb4, 0x44, 0xf6,
	0xc1, 0xc7, 0xb7, 0x30, 0xb2, 0x73, 0x28, 0x7a, 0x7d, 0xa7, 0xcd, 0x93, 0xc6, 0x13, 0x56, 0xc0,
	0xe1, 0x46, 0x11, 0x9e, 0xb2, 0x12, 0xb2, 0x56, 0xd5, 0x0d, 0x9e, 0xa5, 0x64, 0x4d, 0x47, 0x78,
	0x48, 0x47, 0xdb, 0x13, 0x66, 0x0c, 0x20, 0x6f, 0xd4, 0xbd, 0x22, 0x85, 0xc7, 0xc4, 0xa5, 0xd1,
	0x5a, 0x49, 0xc2, 0x3c, 0x2d, 0xc6, 0xd2, 0xad, 0xd1, 0x1d, 0x16, 0xac, 0x82, 0x23, 0x3d, 0xd6,
	0x52, 0x61, 0x99, 0xa2, 0xad, 0x49, 0xb6, 0x58, 0x5d, 0xe7, 0xcf, 0x59, 0x2a, 0xf1, 0x1b, 0x00,
	0x00, 0xff, 0xff, 0x00, 0x14, 0xaf, 0xe9, 0xe2, 0x00, 0x00, 0x00,
}
