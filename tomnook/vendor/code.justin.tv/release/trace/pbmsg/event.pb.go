// Code generated by protoc-gen-go.
// source: code.justin.tv/release/trace/pbmsg/event.proto
// DO NOT EDIT!

package pbmsg

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// Kind describes the type of event instrumented.
//
// When a request-response RPC takes place, there are several identifiable
// instants that can be observed by the client, and several that can be
// observed by the server.  Each of those is assigned a different Kind
// value.  An program may only be instrumented to measure a small subset
// of the possible events.  The goal of the Kind enumeration is to
// document all known events precisely, so that if two programs emit an
// event with a particular Kind, that the results will be comparable.
//
// If a program wishes to emit an event which is not described by an
// existing Kind value, a new Kind should be written to fit the bill.  The
// new value should be reusable across other programs with similar
// requirements and should not be particular to the language or framework
// used.
type Event_Kind int32

const (
	// Server reads the beginning of a request.  The server has received
	// sufficient data from the client to determine which Trace
	// transaction the request should be attributed to, if any.  If the
	// protocol differentiates between a metadata preamble and a payload
	// that follows, this event is emitted immediately after the entire
	// metadata has been received.
	//
	// For an HTTP request, this would be when the transport has presented
	// the request URI and headers are presented to the application.
	Event_REQUEST_HEAD_RECEIVED Event_Kind = 20
	// Server finishes reading the last of the request body.
	//
	// For an HTTP request, this would be when the transport has presented
	// the last of the request body to the application and notified it
	// that no more body data will be received.  Trailers may be yet to
	// come.
	Event_REQUEST_BODY_RECEIVED Event_Kind = 21
	// Server responds to a request.  There has not yet been any direct
	// communication to the client about the client's request on the
	// application layer, but the server will change that immediately
	// after generating this event.  Transport-layer acknowledgement of
	// data receipt does not trigger this event.
	//
	// For an HTTP response, this would be when the status code and
	// headers are presented to the HTTP transport, before any bytes are
	// written to the wire.
	Event_RESPONSE_HEAD_PREPARED Event_Kind = 30
	// Server has sent the last of the response body to the client.
	//
	// For an HTTP response, this would be when the last of the response
	// body has been accepted by the HTTP transport, and the application
	// has notified the transport that no more body data will be sent.
	Event_RESPONSE_BODY_SENT Event_Kind = 31
	// Client initiates a request.  There has been no indication given to
	// the outside world that this particular request is about to take
	// place.  The application knows enough about the request to begin
	// making externally-visible actions.
	//
	// For an HTTP request, this would be when the request URI and headers
	// are presented to the HTTP transport, before any bytes are written
	// to the wire.  The hostname may still need to be resolved, a TCP
	// connection may still need to be established, a TLS handshake may
	// still need to take place.
	Event_REQUEST_HEAD_PREPARED Event_Kind = 40
	// Client finishes the externally-visible actions required for the
	// request.
	//
	// For an HTTP request, this would be when the last of the request
	// body has been accepted by the HTTP transport, and the application
	// has notified the transport that no more body data will be sent.
	Event_REQUEST_BODY_SENT Event_Kind = 41
	// Client reads the beginning of the response.  The response metadata
	// has been read and is ready to be presented to the application's
	// business logic, giving some initial indication of if the RPC
	// succeeded.  The response body has not yet been read.
	//
	// For an HTTP response, this would be when the client's HTTP
	// transport has read and decoded the full response headers from the
	// wire and is presenting them to the application, but before the
	// response body has been consumed.
	Event_RESPONSE_HEAD_RECEIVED Event_Kind = 50
	// Client finishes reading the last of the response body.
	//
	// For an HTTP response, this would be when the transport has
	// presented the last of the response body to the application and
	// notified it that no more body data will be received.  Trailers may
	// be yet to come.
	Event_RESPONSE_BODY_RECEIVED Event_Kind = 51
)

var Event_Kind_name = map[int32]string{
	20: "REQUEST_HEAD_RECEIVED",
	21: "REQUEST_BODY_RECEIVED",
	30: "RESPONSE_HEAD_PREPARED",
	31: "RESPONSE_BODY_SENT",
	40: "REQUEST_HEAD_PREPARED",
	41: "REQUEST_BODY_SENT",
	50: "RESPONSE_HEAD_RECEIVED",
	51: "RESPONSE_BODY_RECEIVED",
}
var Event_Kind_value = map[string]int32{
	"REQUEST_HEAD_RECEIVED":  20,
	"REQUEST_BODY_RECEIVED":  21,
	"RESPONSE_HEAD_PREPARED": 30,
	"RESPONSE_BODY_SENT":     31,
	"REQUEST_HEAD_PREPARED":  40,
	"REQUEST_BODY_SENT":      41,
	"RESPONSE_HEAD_RECEIVED": 50,
	"RESPONSE_BODY_RECEIVED": 51,
}

func (x Event_Kind) Enum() *Event_Kind {
	p := new(Event_Kind)
	*p = x
	return p
}
func (x Event_Kind) String() string {
	return proto.EnumName(Event_Kind_name, int32(x))
}
func (x *Event_Kind) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(Event_Kind_value, data, "Event_Kind")
	if err != nil {
		return err
	}
	*x = Event_Kind(value)
	return nil
}
func (Event_Kind) EnumDescriptor() ([]byte, []int) { return fileDescriptor1, []int{0, 0} }

// https://www.iana.org/assignments/http-methods/http-methods.xhtml
type ExtraHTTP_Method int32

const (
	// invalid, default value
	ExtraHTTP_UNKNOWN ExtraHTTP_Method = 0
	// https://tools.ietf.org/html/rfc7231#section-4.3.1  safe, idempotent
	ExtraHTTP_GET ExtraHTTP_Method = 1
	// https://tools.ietf.org/html/rfc7231#section-4.3.2  safe, idempotent
	ExtraHTTP_HEAD ExtraHTTP_Method = 2
	// https://tools.ietf.org/html/rfc7231#section-4.3.3
	ExtraHTTP_POST ExtraHTTP_Method = 3
	// https://tools.ietf.org/html/rfc7231#section-4.3.4  idempotent
	ExtraHTTP_PUT ExtraHTTP_Method = 4
	// https://tools.ietf.org/html/rfc7231#section-4.3.5  idempotent
	ExtraHTTP_DELETE ExtraHTTP_Method = 5
	// https://tools.ietf.org/html/rfc7231#section-4.3.6
	ExtraHTTP_CONNECT ExtraHTTP_Method = 6
	// https://tools.ietf.org/html/rfc7231#section-4.3.7  safe, idempotent
	ExtraHTTP_OPTIONS ExtraHTTP_Method = 7
	// https://tools.ietf.org/html/rfc7231#section-4.3.8  safe, idempotent
	ExtraHTTP_TRACE ExtraHTTP_Method = 8
	// https://tools.ietf.org/html/rfc5789
	ExtraHTTP_PATCH ExtraHTTP_Method = 9
)

var ExtraHTTP_Method_name = map[int32]string{
	0: "UNKNOWN",
	1: "GET",
	2: "HEAD",
	3: "POST",
	4: "PUT",
	5: "DELETE",
	6: "CONNECT",
	7: "OPTIONS",
	8: "TRACE",
	9: "PATCH",
}
var ExtraHTTP_Method_value = map[string]int32{
	"UNKNOWN": 0,
	"GET":     1,
	"HEAD":    2,
	"POST":    3,
	"PUT":     4,
	"DELETE":  5,
	"CONNECT": 6,
	"OPTIONS": 7,
	"TRACE":   8,
	"PATCH":   9,
}

func (x ExtraHTTP_Method) Enum() *ExtraHTTP_Method {
	p := new(ExtraHTTP_Method)
	*p = x
	return p
}
func (x ExtraHTTP_Method) String() string {
	return proto.EnumName(ExtraHTTP_Method_name, int32(x))
}
func (x *ExtraHTTP_Method) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(ExtraHTTP_Method_value, data, "ExtraHTTP_Method")
	if err != nil {
		return err
	}
	*x = ExtraHTTP_Method(value)
	return nil
}
func (ExtraHTTP_Method) EnumDescriptor() ([]byte, []int) { return fileDescriptor1, []int{2, 0} }

type ExtraMemcached_MemcachedCommand int32

const (
	// Default value - unset. Probably an administrative
	// command like STAT.
	ExtraMemcached_UNKNOWN_COMMAND ExtraMemcached_MemcachedCommand = 0
	// Set means "store this data."
	ExtraMemcached_SET ExtraMemcached_MemcachedCommand = 1
	// Add means "store this data, but only if the server
	// doesn't already have data for this key."
	ExtraMemcached_ADD ExtraMemcached_MemcachedCommand = 2
	// Replace means "store this data, but only if the
	// server *does* have data for this key."
	ExtraMemcached_REPLACE ExtraMemcached_MemcachedCommand = 3
	// Append means "add this data to an existing key
	// after existing data." Append does not come with an
	// expiration time.
	ExtraMemcached_APPEND ExtraMemcached_MemcachedCommand = 4
	// Prepend means "add this data to an existing key
	// before existing data." Prepend does not come with
	// an expiration time.
	ExtraMemcached_PREPEND ExtraMemcached_MemcachedCommand = 5
	// Compare-and-set means "Store this data, if and only
	// if no one else has updated since I last fetched
	// it."
	ExtraMemcached_CAS ExtraMemcached_MemcachedCommand = 6
	// Get retrieves one or more keys. If no_reply is set
	// to true, then the server will not respond in the
	// case of cache misses, so it is the client's
	// responsibility to match up which requested keys
	// actually got a VALUE response if they ask for
	// multiple keys at once.
	ExtraMemcached_GET ExtraMemcached_MemcachedCommand = 7
	// Gets is like Get, but asks for the cas_unique to be
	// sent along with the response.
	ExtraMemcached_GETS ExtraMemcached_MemcachedCommand = 8
	// Delete forcibly removes a key.
	ExtraMemcached_DELETE ExtraMemcached_MemcachedCommand = 9
	// Incr means "increment this key by this amount." The
	// server returns the new value of the key.
	ExtraMemcached_INCR ExtraMemcached_MemcachedCommand = 10
	// Decr means "decrement this key by this amount." The
	// server returns the new value of the key.
	ExtraMemcached_DECR ExtraMemcached_MemcachedCommand = 11
)

var ExtraMemcached_MemcachedCommand_name = map[int32]string{
	0:  "UNKNOWN_COMMAND",
	1:  "SET",
	2:  "ADD",
	3:  "REPLACE",
	4:  "APPEND",
	5:  "PREPEND",
	6:  "CAS",
	7:  "GET",
	8:  "GETS",
	9:  "DELETE",
	10: "INCR",
	11: "DECR",
}
var ExtraMemcached_MemcachedCommand_value = map[string]int32{
	"UNKNOWN_COMMAND": 0,
	"SET":             1,
	"ADD":             2,
	"REPLACE":         3,
	"APPEND":          4,
	"PREPEND":         5,
	"CAS":             6,
	"GET":             7,
	"GETS":            8,
	"DELETE":          9,
	"INCR":            10,
	"DECR":            11,
}

func (x ExtraMemcached_MemcachedCommand) Enum() *ExtraMemcached_MemcachedCommand {
	p := new(ExtraMemcached_MemcachedCommand)
	*p = x
	return p
}
func (x ExtraMemcached_MemcachedCommand) String() string {
	return proto.EnumName(ExtraMemcached_MemcachedCommand_name, int32(x))
}
func (x *ExtraMemcached_MemcachedCommand) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(ExtraMemcached_MemcachedCommand_value, data, "ExtraMemcached_MemcachedCommand")
	if err != nil {
		return err
	}
	*x = ExtraMemcached_MemcachedCommand(value)
	return nil
}
func (ExtraMemcached_MemcachedCommand) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor1, []int{4, 0}
}

// An Event represents a single Trace-worthy event.  This includes RPC
// lifecycle events (client started to send a request, server finishes sending
// a response), and application-specific custom events.
type Event struct {
	// Lifecycle event this message is attached to
	Kind *Event_Kind `protobuf:"varint,1,opt,name=kind,enum=code.justin.tv.release.trace.pbmsg.Event_Kind" json:"kind,omitempty"`
	// Unix epoch time in nanoseconds, as measured by the process
	Time *int64 `protobuf:"fixed64,2,opt,name=time" json:"time,omitempty"`
	// FQDN of the process's host
	Hostname *string `protobuf:"bytes,3,opt,name=hostname" json:"hostname,omitempty"`
	// Service name of the process
	Svcname *string `protobuf:"bytes,4,opt,name=svcname" json:"svcname,omitempty"`
	// PID of the process
	Pid *int32 `protobuf:"varint,5,opt,name=pid" json:"pid,omitempty"`
	// 128-bit identifier for the entire transaction.  This field is sent as
	// two 64-bit integers, least significant word first.
	TransactionId []uint64 `protobuf:"fixed64,6,rep,packed,name=transaction_id" json:"transaction_id,omitempty"`
	// Identifier for a single RPC span (one request leading to one response).
	// Each level of the distributed call stack adds a new element to the path
	// field.  The value at each level is the (zero-indexed) number of
	// outbound RPC requests made at that level.
	Path []uint32 `protobuf:"varint,7,rep,packed,name=path" json:"path,omitempty"`
	// Additional protocol-specific information
	Extra *Extra `protobuf:"bytes,8,opt,name=extra" json:"extra,omitempty"`
	// Amazon Resource Name for the process's EC2 instance.  This may be sent
	// instead of the hostname field.
	Ec2Arn           *string `protobuf:"bytes,20,opt,name=ec2_arn" json:"ec2_arn,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Event) Reset()                    { *m = Event{} }
func (m *Event) String() string            { return proto.CompactTextString(m) }
func (*Event) ProtoMessage()               {}
func (*Event) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{0} }

func (m *Event) GetKind() Event_Kind {
	if m != nil && m.Kind != nil {
		return *m.Kind
	}
	return Event_REQUEST_HEAD_RECEIVED
}

func (m *Event) GetTime() int64 {
	if m != nil && m.Time != nil {
		return *m.Time
	}
	return 0
}

func (m *Event) GetHostname() string {
	if m != nil && m.Hostname != nil {
		return *m.Hostname
	}
	return ""
}

func (m *Event) GetSvcname() string {
	if m != nil && m.Svcname != nil {
		return *m.Svcname
	}
	return ""
}

func (m *Event) GetPid() int32 {
	if m != nil && m.Pid != nil {
		return *m.Pid
	}
	return 0
}

func (m *Event) GetTransactionId() []uint64 {
	if m != nil {
		return m.TransactionId
	}
	return nil
}

func (m *Event) GetPath() []uint32 {
	if m != nil {
		return m.Path
	}
	return nil
}

func (m *Event) GetExtra() *Extra {
	if m != nil {
		return m.Extra
	}
	return nil
}

func (m *Event) GetEc2Arn() string {
	if m != nil && m.Ec2Arn != nil {
		return *m.Ec2Arn
	}
	return ""
}

// An Extra describes protocol-specific information to be associated with an
// Event.
type Extra struct {
	// The address of the peer.  This could be an ipv4:port pair, [ipv6]:port
	// pair, or hostname:port pair for TCP/UDP-based protocols, a bare IP
	// address or hostname for IP- based protocols, or a path to a unix domain
	// socket.
	//
	// This field is NOT an attempt to identify the client, e.g. via the HTTP
	// X-Forwarded-For header.
	Peer *string `protobuf:"bytes,1,opt,name=peer" json:"peer,omitempty"`
	// Additional HTTP-specific information for the Event.
	Http *ExtraHTTP `protobuf:"bytes,2,opt,name=http" json:"http,omitempty"`
	// Additional SQL-specific information for the Event.
	Sql *ExtraSQL `protobuf:"bytes,3,opt,name=sql" json:"sql,omitempty"`
	// Additional Memcached-specific information for the Event.
	Memcached *ExtraMemcached `protobuf:"bytes,4,opt,name=memcached" json:"memcached,omitempty"`
	// Additional gRPC-specific information for the Event.
	Grpc             *ExtraGRPC `protobuf:"bytes,5,opt,name=grpc" json:"grpc,omitempty"`
	XXX_unrecognized []byte     `json:"-"`
}

func (m *Extra) Reset()                    { *m = Extra{} }
func (m *Extra) String() string            { return proto.CompactTextString(m) }
func (*Extra) ProtoMessage()               {}
func (*Extra) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{1} }

func (m *Extra) GetPeer() string {
	if m != nil && m.Peer != nil {
		return *m.Peer
	}
	return ""
}

func (m *Extra) GetHttp() *ExtraHTTP {
	if m != nil {
		return m.Http
	}
	return nil
}

func (m *Extra) GetSql() *ExtraSQL {
	if m != nil {
		return m.Sql
	}
	return nil
}

func (m *Extra) GetMemcached() *ExtraMemcached {
	if m != nil {
		return m.Memcached
	}
	return nil
}

func (m *Extra) GetGrpc() *ExtraGRPC {
	if m != nil {
		return m.Grpc
	}
	return nil
}

// An ExtraHTTP describes HTTP-specific information to be associated with an
// Event.
type ExtraHTTP struct {
	// Status code of HTTP response.  Common values will include 200 or 404.
	// Values less than zero indicate an error observed by the client which
	// caused it to give up on the request before getting a status code from
	// the server.
	StatusCode *int32 `protobuf:"zigzag32,1,opt,name=status_code" json:"status_code,omitempty"`
	// Method of HTTP request.
	Method *ExtraHTTP_Method `protobuf:"varint,2,opt,name=method,enum=code.justin.tv.release.trace.pbmsg.ExtraHTTP_Method" json:"method,omitempty"`
	// Path of the HTTP request.  This is only the path - the scheme,
	// hostname, query, and fragment are not included.
	UriPath          *string `protobuf:"bytes,16,opt,name=uri_path" json:"uri_path,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *ExtraHTTP) Reset()                    { *m = ExtraHTTP{} }
func (m *ExtraHTTP) String() string            { return proto.CompactTextString(m) }
func (*ExtraHTTP) ProtoMessage()               {}
func (*ExtraHTTP) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{2} }

func (m *ExtraHTTP) GetStatusCode() int32 {
	if m != nil && m.StatusCode != nil {
		return *m.StatusCode
	}
	return 0
}

func (m *ExtraHTTP) GetMethod() ExtraHTTP_Method {
	if m != nil && m.Method != nil {
		return *m.Method
	}
	return ExtraHTTP_UNKNOWN
}

func (m *ExtraHTTP) GetUriPath() string {
	if m != nil && m.UriPath != nil {
		return *m.UriPath
	}
	return ""
}

// An ExtraSQL describes SQL-specific information to be associated with an
// Event.
type ExtraSQL struct {
	// Name of the target database.
	DatabaseName *string `protobuf:"bytes,1,opt,name=database_name" json:"database_name,omitempty"`
	// Username used to connect to the target database.
	DatabaseUser *string `protobuf:"bytes,2,opt,name=database_user" json:"database_user,omitempty"`
	// Query string with all user data removed: e.g. strings replaced by '',
	// integers replaced by 0, etc.  This value may be truncated (particularly
	// if it's larger than 1kB).
	StrippedQuery    *string `protobuf:"bytes,16,opt,name=stripped_query" json:"stripped_query,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *ExtraSQL) Reset()                    { *m = ExtraSQL{} }
func (m *ExtraSQL) String() string            { return proto.CompactTextString(m) }
func (*ExtraSQL) ProtoMessage()               {}
func (*ExtraSQL) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{3} }

func (m *ExtraSQL) GetDatabaseName() string {
	if m != nil && m.DatabaseName != nil {
		return *m.DatabaseName
	}
	return ""
}

func (m *ExtraSQL) GetDatabaseUser() string {
	if m != nil && m.DatabaseUser != nil {
		return *m.DatabaseUser
	}
	return ""
}

func (m *ExtraSQL) GetStrippedQuery() string {
	if m != nil && m.StrippedQuery != nil {
		return *m.StrippedQuery
	}
	return ""
}

// An ExtraMemcached describes memcached-specific information to be
// associated with an event. Most of this was written with the
// assistance of the memcached protocol spec at
// https://github.com/memcached/memcached/blob/c10feb9ebe2179f6fbe4ac2e3cd12bfefdd42631/doc/protocol.txt.
type ExtraMemcached struct {
	// The command sent to memcached by a client
	Command *ExtraMemcached_MemcachedCommand `protobuf:"varint,1,opt,name=command,enum=code.justin.tv.release.trace.pbmsg.ExtraMemcached_MemcachedCommand" json:"command,omitempty"`
	// The number of keys in this memcached event. This is useful
	// for understanding bulk transactions: the request side might
	// ask for 150 keys, but the response side might have only
	// 100, indicating that the cache was hit for 100 out of the
	// 150 requested keys.
	NKeys *uint32 `protobuf:"varint,2,opt,name=n_keys" json:"n_keys,omitempty"`
	// Some commands involve the client setting a 32-bit integer
	// expiration time. According to the memcached protocol spec,
	// this may be either a Unix time, or a number of seconds
	// starting from current time. The memcached server decides
	// which it is by the size of the expiration time: if the
	// value is less than or equal to 60*60*24*30 (30 days), then
	// it's taken as a relative expiration; if it's over this
	// amount then it is interpreted as Unix time. If its set to
	// zero, then the item never expires.
	Expiration       *uint32 `protobuf:"fixed32,3,opt,name=expiration" json:"expiration,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *ExtraMemcached) Reset()                    { *m = ExtraMemcached{} }
func (m *ExtraMemcached) String() string            { return proto.CompactTextString(m) }
func (*ExtraMemcached) ProtoMessage()               {}
func (*ExtraMemcached) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{4} }

func (m *ExtraMemcached) GetCommand() ExtraMemcached_MemcachedCommand {
	if m != nil && m.Command != nil {
		return *m.Command
	}
	return ExtraMemcached_UNKNOWN_COMMAND
}

func (m *ExtraMemcached) GetNKeys() uint32 {
	if m != nil && m.NKeys != nil {
		return *m.NKeys
	}
	return 0
}

func (m *ExtraMemcached) GetExpiration() uint32 {
	if m != nil && m.Expiration != nil {
		return *m.Expiration
	}
	return 0
}

// An ExtraGRPC describes gRPC-specific information to be associated with an
// event.
type ExtraGRPC struct {
	// Fully-qualified method name, including proto package name, service
	// name, and method name.
	Method           *string `protobuf:"bytes,1,opt,name=method" json:"method,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *ExtraGRPC) Reset()                    { *m = ExtraGRPC{} }
func (m *ExtraGRPC) String() string            { return proto.CompactTextString(m) }
func (*ExtraGRPC) ProtoMessage()               {}
func (*ExtraGRPC) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{5} }

func (m *ExtraGRPC) GetMethod() string {
	if m != nil && m.Method != nil {
		return *m.Method
	}
	return ""
}

// An UnusedExtra describes protocol-specific information to be associated
// with an Event.  This message is not yet ready for use, meaning that the
// field names, identities, and numbers are subject to change!
type UnusedExtra struct {
	// written by an engineer - "string" means utf-8 encoded, which should be
	// fine?
	Text          *string `protobuf:"bytes,20,opt,name=text" json:"text,omitempty"`
	TextTruncated *bool   `protobuf:"varint,1020,opt,name=text_truncated" json:"text_truncated,omitempty"`
	// if there's a payload, how big is it?
	PayloadSize *int64 `protobuf:"varint,21,opt,name=payload_size" json:"payload_size,omitempty"`
	// request uri
	HttpUri          *string `protobuf:"bytes,41,opt,name=http_uri" json:"http_uri,omitempty"`
	HttpUriTruncated *bool   `protobuf:"varint,1041,opt,name=http_uri_truncated" json:"http_uri_truncated,omitempty"`
	// headers, partial list
	HttpHeader []*UnusedExtra_KV `protobuf:"bytes,43,rep,name=http_header" json:"http_header,omitempty"`
	// if there are headers that we expect to be very long, we may want to
	// pull them out into their own fields so we can note when we truncate
	// them
	HttpReferer          *string `protobuf:"bytes,44,opt,name=http_referer" json:"http_referer,omitempty"`
	HttpRefererTruncated *bool   `protobuf:"varint,1044,opt,name=http_referer_truncated" json:"http_referer_truncated,omitempty"`
	XXX_unrecognized     []byte  `json:"-"`
}

func (m *UnusedExtra) Reset()                    { *m = UnusedExtra{} }
func (m *UnusedExtra) String() string            { return proto.CompactTextString(m) }
func (*UnusedExtra) ProtoMessage()               {}
func (*UnusedExtra) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{6} }

func (m *UnusedExtra) GetText() string {
	if m != nil && m.Text != nil {
		return *m.Text
	}
	return ""
}

func (m *UnusedExtra) GetTextTruncated() bool {
	if m != nil && m.TextTruncated != nil {
		return *m.TextTruncated
	}
	return false
}

func (m *UnusedExtra) GetPayloadSize() int64 {
	if m != nil && m.PayloadSize != nil {
		return *m.PayloadSize
	}
	return 0
}

func (m *UnusedExtra) GetHttpUri() string {
	if m != nil && m.HttpUri != nil {
		return *m.HttpUri
	}
	return ""
}

func (m *UnusedExtra) GetHttpUriTruncated() bool {
	if m != nil && m.HttpUriTruncated != nil {
		return *m.HttpUriTruncated
	}
	return false
}

func (m *UnusedExtra) GetHttpHeader() []*UnusedExtra_KV {
	if m != nil {
		return m.HttpHeader
	}
	return nil
}

func (m *UnusedExtra) GetHttpReferer() string {
	if m != nil && m.HttpReferer != nil {
		return *m.HttpReferer
	}
	return ""
}

func (m *UnusedExtra) GetHttpRefererTruncated() bool {
	if m != nil && m.HttpRefererTruncated != nil {
		return *m.HttpRefererTruncated
	}
	return false
}

// if we use proto3 we can just use maps
type UnusedExtra_KV struct {
	// option map_entry = true;
	Key              *string `protobuf:"bytes,1,opt,name=key" json:"key,omitempty"`
	Value            *string `protobuf:"bytes,2,opt,name=value" json:"value,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *UnusedExtra_KV) Reset()                    { *m = UnusedExtra_KV{} }
func (m *UnusedExtra_KV) String() string            { return proto.CompactTextString(m) }
func (*UnusedExtra_KV) ProtoMessage()               {}
func (*UnusedExtra_KV) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{6, 0} }

func (m *UnusedExtra_KV) GetKey() string {
	if m != nil && m.Key != nil {
		return *m.Key
	}
	return ""
}

func (m *UnusedExtra_KV) GetValue() string {
	if m != nil && m.Value != nil {
		return *m.Value
	}
	return ""
}

// An EventSet is a list of Events.  It may be used when sending multiple
// events in a datagram, or when saving them to disk.  There is no codified or
// implied relation between Events grouped into an EventSet.
type EventSet struct {
	// List of Events
	Event            []*Event `protobuf:"bytes,1,rep,name=event" json:"event,omitempty"`
	XXX_unrecognized []byte   `json:"-"`
}

func (m *EventSet) Reset()                    { *m = EventSet{} }
func (m *EventSet) String() string            { return proto.CompactTextString(m) }
func (*EventSet) ProtoMessage()               {}
func (*EventSet) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{7} }

func (m *EventSet) GetEvent() []*Event {
	if m != nil {
		return m.Event
	}
	return nil
}

func init() {
	proto.RegisterType((*Event)(nil), "code.justin.tv.release.trace.pbmsg.Event")
	proto.RegisterType((*Extra)(nil), "code.justin.tv.release.trace.pbmsg.Extra")
	proto.RegisterType((*ExtraHTTP)(nil), "code.justin.tv.release.trace.pbmsg.ExtraHTTP")
	proto.RegisterType((*ExtraSQL)(nil), "code.justin.tv.release.trace.pbmsg.ExtraSQL")
	proto.RegisterType((*ExtraMemcached)(nil), "code.justin.tv.release.trace.pbmsg.ExtraMemcached")
	proto.RegisterType((*ExtraGRPC)(nil), "code.justin.tv.release.trace.pbmsg.ExtraGRPC")
	proto.RegisterType((*UnusedExtra)(nil), "code.justin.tv.release.trace.pbmsg.UnusedExtra")
	proto.RegisterType((*UnusedExtra_KV)(nil), "code.justin.tv.release.trace.pbmsg.UnusedExtra.KV")
	proto.RegisterType((*EventSet)(nil), "code.justin.tv.release.trace.pbmsg.EventSet")
	proto.RegisterEnum("code.justin.tv.release.trace.pbmsg.Event_Kind", Event_Kind_name, Event_Kind_value)
	proto.RegisterEnum("code.justin.tv.release.trace.pbmsg.ExtraHTTP_Method", ExtraHTTP_Method_name, ExtraHTTP_Method_value)
	proto.RegisterEnum("code.justin.tv.release.trace.pbmsg.ExtraMemcached_MemcachedCommand", ExtraMemcached_MemcachedCommand_name, ExtraMemcached_MemcachedCommand_value)
}

var fileDescriptor1 = []byte{
	// 911 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x94, 0x55, 0xcf, 0x6e, 0xdb, 0xc6,
	0x13, 0xfe, 0x49, 0x24, 0x45, 0x69, 0x18, 0x2b, 0xcc, 0xc6, 0xf2, 0x8f, 0x4d, 0x80, 0xc6, 0xe0,
	0xc9, 0x46, 0x53, 0x1a, 0x60, 0x7b, 0xe8, 0xbf, 0x8b, 0x4c, 0x2e, 0x6c, 0xc3, 0x36, 0xc5, 0x88,
	0x74, 0x8a, 0xf6, 0x42, 0x30, 0xe4, 0x36, 0x62, 0x63, 0x51, 0x0c, 0xb9, 0x72, 0xe3, 0xbe, 0x45,
	0x80, 0xde, 0x7a, 0xe8, 0xb5, 0xaf, 0xd4, 0xbe, 0x45, 0xef, 0x3d, 0x74, 0x76, 0xa5, 0xc8, 0x72,
	0x7d, 0xa8, 0x7c, 0xdb, 0x9d, 0x99, 0x6f, 0x76, 0xf8, 0xcd, 0x37, 0x43, 0x70, 0xb2, 0x59, 0xce,
	0x9c, 0x1f, 0xe7, 0x0d, 0x2f, 0x4a, 0x87, 0x5f, 0x1d, 0xd4, 0xec, 0x92, 0xa5, 0x0d, 0x3b, 0xe0,
	0x75, 0x9a, 0xb1, 0x83, 0xea, 0xd5, 0xb4, 0x79, 0x7d, 0xc0, 0xae, 0x58, 0xc9, 0x9d, 0xaa, 0x9e,
	0xf1, 0x19, 0xb1, 0x6f, 0xc7, 0x3b, 0xcb, 0x78, 0x47, 0xc6, 0x3b, 0x32, 0xde, 0xfe, 0x43, 0x01,
	0x8d, 0x0a, 0x0c, 0xf9, 0x06, 0xd4, 0x37, 0x45, 0x99, 0x5b, 0xad, 0xdd, 0xd6, 0x5e, 0xdf, 0x75,
	0x9c, 0xff, 0x06, 0x3b, 0x12, 0xe8, 0x9c, 0x22, 0x8a, 0x3c, 0x00, 0x95, 0x17, 0x53, 0x66, 0xb5,
	0x11, 0x6d, 0x12, 0x13, 0xba, 0x93, 0x59, 0xc3, 0xcb, 0x14, 0x2d, 0x0a, 0x5a, 0x7a, 0xe4, 0x21,
	0xe8, 0xcd, 0x55, 0x26, 0x0d, 0xaa, 0x34, 0x18, 0xa0, 0x54, 0x45, 0x6e, 0x69, 0x78, 0xd1, 0xc8,
	0x13, 0xe8, 0x63, 0xde, 0xb2, 0x49, 0x33, 0x5e, 0xcc, 0xca, 0x04, 0xed, 0x9d, 0x5d, 0x65, 0xaf,
	0x73, 0xd8, 0x36, 0x5b, 0x98, 0x4b, 0xad, 0x52, 0x3e, 0xb1, 0x74, 0xb4, 0x6c, 0x49, 0xcb, 0x17,
	0xa0, 0xb1, 0x77, 0x18, 0x6f, 0x75, 0x11, 0x6c, 0xb8, 0xfb, 0x1b, 0x95, 0x2a, 0x00, 0xa2, 0x0a,
	0x96, 0xb9, 0x49, 0x5a, 0x97, 0xd6, 0xb6, 0xa8, 0xc2, 0xfe, 0xb3, 0x05, 0xaa, 0xac, 0xff, 0x23,
	0x18, 0x8c, 0xe9, 0x8b, 0x0b, 0x1a, 0xc5, 0xc9, 0x31, 0x1d, 0xfa, 0xc9, 0x98, 0x7a, 0xf4, 0xe4,
	0x25, 0xf5, 0xcd, 0xed, 0x75, 0xd7, 0xe1, 0xc8, 0xff, 0xee, 0xc6, 0x35, 0xc0, 0xba, 0x77, 0xc6,
	0x34, 0x0a, 0x47, 0x41, 0x44, 0x17, 0xb0, 0x70, 0x4c, 0xc3, 0xe1, 0x18, 0x7d, 0x1f, 0x93, 0x1d,
	0x20, 0x2b, 0x9f, 0xc4, 0x45, 0x34, 0x88, 0xcd, 0x67, 0x77, 0x5e, 0x5a, 0x41, 0xf6, 0xc8, 0x00,
	0x1e, 0xdd, 0x7a, 0x49, 0x22, 0xf6, 0xef, 0xbe, 0xb2, 0xaa, 0xc0, 0xbd, 0xe5, 0xbb, 0x5d, 0xdd,
	0x67, 0xf6, 0xfb, 0x36, 0xf6, 0x56, 0x7e, 0x37, 0x76, 0xa7, 0x62, 0xac, 0x96, 0xbd, 0xed, 0x91,
	0xaf, 0x41, 0x9d, 0x70, 0x5e, 0xc9, 0x5e, 0x19, 0xee, 0xa7, 0x1b, 0xd3, 0x77, 0x1c, 0xc7, 0x21,
	0xf9, 0x12, 0x94, 0xe6, 0xed, 0xa5, 0xec, 0xaa, 0xe1, 0x3e, 0xdf, 0x18, 0x1b, 0xbd, 0x38, 0x23,
	0x14, 0x7a, 0x53, 0x36, 0xcd, 0xd2, 0x6c, 0xc2, 0x72, 0xa9, 0x02, 0xc3, 0x75, 0x37, 0x4e, 0x70,
	0xfe, 0x01, 0x29, 0xca, 0x7f, 0x5d, 0x57, 0x99, 0x94, 0xce, 0x7d, 0xca, 0x3f, 0x1a, 0x87, 0x9e,
	0xfd, 0x57, 0x0b, 0x7a, 0x37, 0x1f, 0xf3, 0x18, 0x8c, 0x86, 0xa7, 0x7c, 0xde, 0x24, 0x22, 0x89,
	0xa4, 0xe7, 0x11, 0xf1, 0xa1, 0x33, 0x65, 0x7c, 0x32, 0xcb, 0x25, 0x41, 0x7d, 0xf7, 0xf3, 0x7b,
	0x11, 0xe4, 0x9c, 0x4b, 0xac, 0x18, 0x81, 0x79, 0x5d, 0x24, 0x52, 0xba, 0xa6, 0xd4, 0xda, 0x4f,
	0xd0, 0x59, 0xfa, 0x0c, 0xd0, 0x2f, 0x82, 0xd3, 0x60, 0xf4, 0x6d, 0x60, 0xfe, 0x8f, 0xe8, 0xa0,
	0x1c, 0xd1, 0x18, 0x65, 0xdd, 0x05, 0x55, 0x74, 0xd7, 0x6c, 0x8b, 0x53, 0x38, 0x8a, 0x62, 0x53,
	0x11, 0xce, 0xf0, 0x22, 0x36, 0x55, 0x02, 0xd0, 0xf1, 0xe9, 0x19, 0x8d, 0xa9, 0xa9, 0x09, 0xb8,
	0x37, 0x0a, 0x02, 0xea, 0xc5, 0x66, 0x47, 0x5c, 0x46, 0x61, 0x7c, 0x82, 0x02, 0x30, 0x75, 0xd2,
	0x03, 0x2d, 0x1e, 0x0f, 0x3d, 0x6a, 0x76, 0xc5, 0x31, 0x1c, 0xc6, 0xde, 0xb1, 0xd9, 0xb3, 0x43,
	0xe8, 0xae, 0x7a, 0x30, 0x80, 0xad, 0x3c, 0xe5, 0xe9, 0x2b, 0xac, 0x3f, 0x91, 0xd3, 0xb8, 0x90,
	0xc4, 0xba, 0x79, 0xde, 0xa0, 0x52, 0xda, 0xd2, 0xbc, 0x03, 0xfd, 0x86, 0xd7, 0x45, 0x55, 0xb1,
	0x3c, 0x79, 0x3b, 0x67, 0xf5, 0xf5, 0xf2, 0x53, 0x7e, 0x6f, 0x43, 0xff, 0x5f, 0x5d, 0x89, 0x41,
	0xcf, 0x66, 0xd3, 0x69, 0xba, 0xda, 0x20, 0xde, 0xfd, 0x5b, 0xeb, 0xac, 0x4e, 0xde, 0x22, 0x15,
	0xe9, 0x43, 0xa7, 0x4c, 0xde, 0xb0, 0xeb, 0x46, 0x16, 0xb4, 0x45, 0x90, 0x07, 0xf6, 0xae, 0x2a,
	0xea, 0x54, 0xec, 0x09, 0x29, 0x42, 0xdd, 0xfe, 0xb5, 0x05, 0xe6, 0x1d, 0xe0, 0x63, 0x78, 0xb8,
	0xa4, 0x38, 0xf1, 0x46, 0xe7, 0xe7, 0xc3, 0xc0, 0x5f, 0x50, 0x1d, 0x49, 0xaa, 0xf1, 0x30, 0xf4,
	0x05, 0xd3, 0xc8, 0x1e, 0x8e, 0xdf, 0x99, 0xa0, 0x4c, 0x11, 0x1c, 0x0f, 0xc3, 0x90, 0x62, 0xa8,
	0x2a, 0x1c, 0x62, 0x30, 0xc5, 0x45, 0x13, 0xe1, 0xde, 0x30, 0x42, 0xb2, 0x97, 0xbd, 0xd2, 0x45,
	0x87, 0xf0, 0x10, 0x21, 0xcf, 0x37, 0x8d, 0xe9, 0x09, 0xeb, 0x49, 0xe0, 0x8d, 0x4d, 0x10, 0x27,
	0x9f, 0xe2, 0xc9, 0xb0, 0x9f, 0x2e, 0xf5, 0x26, 0xd4, 0x27, 0x3e, 0x67, 0x29, 0x2d, 0x49, 0xbb,
	0xfd, 0x5b, 0x1b, 0x8c, 0x8b, 0x12, 0x09, 0xcf, 0x57, 0x73, 0xca, 0x71, 0xb5, 0x2d, 0x96, 0x13,
	0xf9, 0x3f, 0x6e, 0x45, 0xbc, 0x25, 0xbc, 0x9e, 0x97, 0x59, 0xca, 0x71, 0x68, 0xfe, 0xd6, 0xd1,
	0xd1, 0x25, 0xdb, 0xf0, 0xa0, 0x4a, 0xaf, 0x2f, 0x67, 0x69, 0x9e, 0x34, 0xc5, 0xcf, 0xcc, 0x1a,
	0xa0, 0x55, 0x91, 0x4b, 0x17, 0xc7, 0x3a, 0x41, 0xd9, 0x59, 0xfb, 0x32, 0xc1, 0x53, 0x20, 0x1f,
	0x2c, 0x6b, 0x49, 0xde, 0x77, 0x65, 0x92, 0x23, 0x30, 0xa4, 0x73, 0xc2, 0xd2, 0x1c, 0x1b, 0xfe,
	0x09, 0xae, 0xd7, 0x0d, 0xe7, 0x71, 0xad, 0x62, 0xe7, 0xf4, 0xa5, 0xa8, 0x46, 0x26, 0xaa, 0xd9,
	0x0f, 0xac, 0xc6, 0x4c, 0xcf, 0xe5, 0xdb, 0xcf, 0x60, 0x67, 0xdd, 0xba, 0xf6, 0xfe, 0x2f, 0xf2,
	0xfd, 0x27, 0xbb, 0xd0, 0x46, 0x30, 0xfe, 0x06, 0xb0, 0xbd, 0x4b, 0x15, 0x6e, 0x81, 0x76, 0x95,
	0x5e, 0xce, 0x17, 0x7f, 0x91, 0xde, 0x57, 0x6d, 0xab, 0x65, 0xfb, 0xa8, 0x5d, 0xf1, 0x97, 0x89,
	0x18, 0x97, 0x7b, 0x5f, 0x9c, 0x31, 0x5a, 0xd9, 0x78, 0xef, 0x0b, 0xc0, 0xa1, 0xfe, 0xbd, 0x26,
	0xaf, 0xff, 0x04, 0x00, 0x00, 0xff, 0xff, 0xe1, 0xaf, 0xd9, 0xab, 0x43, 0x07, 0x00, 0x00,
}
