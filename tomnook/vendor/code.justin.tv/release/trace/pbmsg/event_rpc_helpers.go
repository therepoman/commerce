package pbmsg

// CacheHitRate returns the fraction of keys which were succesfully
// hit in the memcached call
func CacheHitRate(call, response *ExtraMemcached) float64 {
	num := float64(response.GetNKeys())
	den := float64(call.GetNKeys())
	if den == 0 {
		return 0
	}
	if num > den {
		return 1
	}
	return num / den
}

// CacheHit returns true if all keys that were requested were returned
// in the response.
func CacheHit(call, response *ExtraMemcached) bool {
	return CacheHitRate(call, response) == 1
}
