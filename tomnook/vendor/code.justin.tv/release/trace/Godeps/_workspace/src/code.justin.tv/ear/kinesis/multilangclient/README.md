# multilangclient #

multilangclient provides a go interface for connecting to the
[Amazon Kinesis Client MultiLang Daemon](https://github.com/awslabs/amazon-kinesis-client).
This is a framework for coordinating consumption of
a Kinesis stream across multiple consumers.

See the [examples/messagelogger](./examples/messagelogger) directory
for an example implementation.
