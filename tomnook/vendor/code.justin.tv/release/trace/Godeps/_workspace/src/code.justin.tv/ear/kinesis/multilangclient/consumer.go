package multilangclient

// A Consumer receives instructions and data from the KCL
// MultiLangDaemon. It has three handler methods.
//
// Initialize will be called when starting up, and will be passed the
// string ID of the shard that will be processed.
//
// Process will receive decoded bytes from the KCL MultiLangDaemon,
// one record at a time.
//
// Shutdown will be called when the KCL MultiLangDaemon is finished
// with a consumer.
type Consumer interface {
	Initialize(shardID string) error
	Process(record []byte) error
	Shutdown() error
}

// Run will execute a consumer. It will read from Stdin to receive
// messages from the KCL MultiLangDaemon, and will communicate back
// with Stdout, so this function should only be called exactly once
// over a process's lifetime. It will trigger the consumer's handler
// functions; if any ever return an error, it will return that error
// and stop. The KCL documentation recommends exiting the process in
// the face of errors. The KCL MultiLangDaemon will restart the
// process.
func Run(c Consumer) error {
	client := newClient(c)
	return client.run()
}
