package multilangclient

import "encoding/base64"

type action string

const (
	initialize     action = "initialize"
	processRecords action = "processRecords"
	checkpoint     action = "checkpoint"
	shutdown       action = "shutdown"
	status         action = "status"
)

type message struct {
	Action action `json:"action"`

	// initialize
	ShardID string `json:"shardId,omitempty"`

	// processRecords
	Records []*encodedRecord `json:"records,omitempty"`

	// checkpoint
	Checkpoint string `json:"checkpoint,omitempty"`
	Error      string `json:"error,omitempty"`

	// shutdown
	Reason string `json:"reason,omitempty"`

	// status
	ResponseFor action `json:"responseFor,omitempty"`
}

type encodedRecord struct {
	EncodedData    string `json:"Data"`
	PartitionKey   string `json:"partitionKey"`
	SequenceNumber string `json:"sequenceNumber"`
}

func (r *encodedRecord) decode() ([]byte, error) {
	return base64.StdEncoding.DecodeString(r.EncodedData)
}

func ack(msg *message) *message {
	return &message{Action: status, ResponseFor: msg.Action}
}
