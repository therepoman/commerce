package combiner

type RecordSet interface {
	// Adds the Record to the RecordSet
	Push(Record)

	// Removes the last added Record from the RecordSet and returns it
	// The returned Record just has to contain the same data as the original
	// Record, it does not have to have the same pointer.
	Pop() Record

	// Returns the length of the encoded RecordSet in number of bytes
	// Naively implementing this as len(self.Encode()) is fine, but
	// it is offered for performance enhancements as Size() is called
	// twice for every call to Push()
	Size() int

	// Returns a serialized version of the RecordSet, which is used
	// to set the Data field on the Blob
	Encode() []byte
}

type Record interface {
	// Returns an identifier for the Record. Does not need to be unique.
	// Records with the same Key are guarenteed to have the same Blob Key.
	Key() int

	// Returns a RecordSet that can contain the Record
	NewSet() RecordSet
}

type Blob struct {
	// A non-unique identifier to be used for sharding
	Key string

	// The serialized RecordSet
	Data []byte
}
