package multilangclient

import (
	"io/ioutil"
	"log"
)

var logger *log.Logger = log.New(ioutil.Discard, "", 0)

// SetLogger will configure kcl's internal logger. By default, all log
// lines are discarded.
func SetLogger(l *log.Logger) { logger = l }
