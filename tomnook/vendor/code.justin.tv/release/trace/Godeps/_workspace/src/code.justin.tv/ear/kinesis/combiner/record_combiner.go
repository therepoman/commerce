package combiner

import (
	"math/big"
	"time"
)

// the max signed 128-bit value:
var max128BitInt, _ = big.NewInt(1).SetString("340282366920938463463374607431768211455", 10)

type RecordCombiner struct {
	shards []*shard
	blobs  chan *Blob
	errors chan error
}

// Creates a new RecordCombiner with the number of shards specified.
// Blobs will be emitted whenever a shard reaches either maxSize, or
// maxTime elapses since the first Record was Add()ed.
func NewRecordCombiner(numShards int, maxSize int, maxTime time.Duration) *RecordCombiner {
	rc := &RecordCombiner{
		shards: []*shard{},
		blobs:  make(chan *Blob, 1024),
		errors: make(chan error, 1024),
	}
	for i := 0; i < numShards; i++ {
		rc.shards = append(rc.shards, newShard(rc.shardID(int64(i), int64(numShards)), maxSize, maxTime, rc.blobs, rc.errors))
	}
	return rc
}

// Adds the Record to the appropriate shard
// Returns an error if the record is larger than maxSize
func (rc *RecordCombiner) Add(r Record) {
	rc.shards[modulo(r.Key(), len(rc.shards))].add(r)
}

// Shards are assigned large string IDs which are equally spaced between 0 and
// 340282366920938463463374607431768211455.
func (rc *RecordCombiner) shardID(idx int64, n int64) string {
	result := big.NewInt(0)
	result = result.Div(max128BitInt, big.NewInt(n))
	result = result.Mul(result, big.NewInt(idx))
	return result.String()
}

// Processes a channel of Records, Add()ing each one as they arrive
// and Close()ing when the channel is closed.
// Returns a channel of errors from failed Add()s.
func (rc *RecordCombiner) Consume(input <-chan Record) {
	go func() {
		for r := range input {
			rc.Add(r)
		}
		rc.Close()
	}()
}

// Closes each shard, ensuring all pending Records are flushed
// Then closes the Blobs() channel
func (rc *RecordCombiner) Close() {
	for _, shard := range rc.shards {
		shard.close()
	}
	close(rc.blobs)
	close(rc.errors)
}

func (rc *RecordCombiner) Blobs() <-chan *Blob {
	return rc.blobs
}

func (rc *RecordCombiner) Errors() <-chan error {
	return rc.errors
}

// Go's built-in % operator can return negative numbers, so this
// ensures we only get positive numbers.
// x % y returns in the range of (-y, y)
// + y returns in the range of (0, 2y)
// % y returns in the range of (0, y)
func modulo(x, y int) int {
	return ((x % y) + y) % y
}
