package combiner

import (
	"fmt"
	"time"
)

type ErrRecordTooBig struct {
	Record     Record
	RecordSize int
	MaxSize    int
}

func (e ErrRecordTooBig) Error() string {
	return fmt.Sprintf("Size of record (%d) exceeds maximum allowed size (%d)", e.RecordSize, e.MaxSize)
}

type shard struct {
	id      string
	maxSize int
	maxTime time.Duration

	output chan<- *Blob
	errors chan<- error

	pending chan Record
	done    chan struct{}

	records RecordSet
	timer   *time.Timer
}

func newShard(id string, maxSize int, maxTime time.Duration, output chan<- *Blob, errors chan<- error) *shard {
	s := &shard{
		id:      id,
		maxSize: maxSize,
		maxTime: maxTime,
		output:  output,
		errors:  errors,
		pending: make(chan Record, 1024),
		done:    make(chan struct{}),
	}
	go s.run()
	return s
}

// enqueue a record for processing
func (s *shard) add(r Record) {
	s.pending <- r
}

func (s *shard) run() {
RunLoop:
	for {
		// If it's the first record in the set, we don't have a timer to check
		if s.timer == nil {
			r, open := <-s.pending
			if !open {
				break RunLoop
			}
			if err := s.process(r); err != nil {
				// Non-blocking send to avoid a deadlock in case the
				// user isn't reading from errors.
				select {
				case s.errors <- err:
				default:
				}
			}
		} else {
			select {
			case <-s.timer.C:
				s.flush()
			case r, open := <-s.pending:
				if !open {
					s.flush()
					break RunLoop
				}
				if err := s.process(r); err != nil {
					// Non-blocking send to avoid a deadlock in case the
					// user isn't reading from errors.
					select {
					case s.errors <- err:
					default:
					}
				}
			}
		}
	}
	close(s.done)
}

func (s *shard) close() {
	close(s.pending)
	<-s.done
}

func (s *shard) process(r Record) error {
	// Is this the first record in a set?
	first := s.records == nil

	// It's the first record, so start the timer
	if first {
		s.records = r.NewSet()
		s.timer = time.NewTimer(s.maxTime)
	}

	s.records.Push(r)
	size := s.records.Size()

	switch {
	// If it won't fit, flush and try again
	case size > s.maxSize:
		if first {
			// this record will *never* fit.
			s.reset()
			return ErrRecordTooBig{r, size, s.maxSize}
		}
		r = s.records.Pop()
		s.flush()
		return s.process(r)

	// Flush immediately if we can
	case size == s.maxSize:
		s.flush()
	}
	return nil
}

func (s *shard) flush() {
	s.output <- &Blob{
		Key:  s.id,
		Data: s.records.Encode(),
	}
	s.reset()
}

func (s *shard) reset() {
	if s.timer != nil {
		s.timer.Stop()
		s.timer = nil
	}
	s.records = nil
}
