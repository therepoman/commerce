#!/usr/bin/env bash
set -e -o pipefail -u -x

version="$1"

ln -s /usr/local/go/bin/{go,gofmt,godoc} /usr/local/bin/

# A stable version of Go 1.4 or newer is required for compiling Go 1.5 and
# newer.
wget -q -O go.tar.gz https://storage.googleapis.com/golang/go1.4.2.linux-amd64.tar.gz
echo "5020af94b52b65cc9b6f11d50a67e4bae07b0aff  go.tar.gz" | sha1sum --check
mkdir /usr/local/go-bootstrap
tar -C /usr/local/go-bootstrap -x --strip-components 1 -z -f go.tar.gz

# Check out and build a specific version of Go.
git clone https://github.com/golang/go.git /usr/local/go-tip
cd /usr/local/go-tip
git checkout "$version" ### Make sure you edit the ln -s line below!
cd /usr/local/go-tip/src
GOROOT_FINAL=/usr/local/go GOROOT_BOOTSTRAP=/usr/local/go-bootstrap ./make.bash

ln -s /usr/local/go-tip /usr/local/go ### Changes between go-tip and go-stable
test -x "$(go env GOTOOLDIR)/vet" || cp /usr/local/go-bootstrap/pkg/tool/linux_amd64/vet "$(go env GOTOOLDIR)"
