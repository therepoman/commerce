#!/bin/bash

set -eo pipefail

if [ $# -eq 0 ]; then
    echo "Usage: $0 <hostclass>"
    echo "<hostclass>: Tag of instance type to connect to (eg 'trace-statsdsink')";
    exit 1;
fi

hostclass="$1"
shift

ips=$(aws ec2 describe-instances \
          --output text\
          --query "Reservations[*].Instances[*].PrivateIpAddress"\
          --filter "Name=tag-value,Values=${hostclass}")

n=$(echo $ips | wc -w)

echo "Found $n hosts. Connecting..."

export PDSH_SSH_ARGS_APPEND="-q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o PreferredAuthentications=publickey"

pdsh -w "$(echo $ips | tr '\t' ,)" "$@"
