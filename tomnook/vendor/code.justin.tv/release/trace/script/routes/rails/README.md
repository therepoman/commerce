# rails/traceroutes #

traceroutes.rake is a Rake task which generates Trace-style route specs.

To run, you first need a working copy of the [Rails codebase](https://git-aws.internal.justin.tv/web/web). You might need to follow the [development environment setup guide](https://git-aws.internal.justin.tv/twitch/docs/blob/master/web/dev_environment_setup.md) to get it up and running.

Then, put traceroutes.rake in `lib/tasks`, and run `bundle exec rake traceroutes`. The routes will be written to a local file, `rails.json`.

The script takes a little while to run, maybe 30 seconds to a minute.
