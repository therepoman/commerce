#!/bin/bash

set -eo pipefail

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $dir/kinesalite_vars.sh

echo "Downloading node v${node_version}"
wget --quiet -O /tmp/node.tgz "https://nodejs.org/dist/v${node_version}/node-v${node_version}-linux-x64.tar.gz"

echo "Verifying node checksum"
echo "${node_checksum}  /tmp/node.tgz" | sha256sum -c -

echo "Unpacking node to ${node_install_dir}"
mkdir -p $node_install_dir
tar -xzf /tmp/node.tgz -C "$node_install_dir" --strip-components=1
export PATH=$PATH:$node_install_dir/bin

echo "Installing kinesalite v${kinesalite_version}"
$node_install_dir/bin/npm install --silent -g "kinesalite@${kinesalite_version}"

echo "Done installing kinesalite"
