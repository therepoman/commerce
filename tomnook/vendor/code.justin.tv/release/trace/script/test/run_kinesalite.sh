#!/bin/bash

set -e -u -o pipefail

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $dir/kinesalite_vars.sh

export PATH=$PATH:$node_install_dir/bin
exec kinesalite \
     --port=4567 \
     --ssl=0 \
     --createStreamMs=50 \
     --deleteStreamMs=50 \
     --shardLimit=50000

