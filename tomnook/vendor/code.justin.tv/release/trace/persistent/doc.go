/*

Package persistent provides read and write access to transactions and reports
in persistent storage

*/
package persistent
