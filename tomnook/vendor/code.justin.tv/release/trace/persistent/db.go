package persistent

import (
	"crypto/sha256"
	"encoding/hex"
	"path"

	"code.justin.tv/release/trace/analysis/tx"
	"code.justin.tv/release/trace/api"
	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
)

type DB struct {
	ldb *leveldb.DB
}

// OpenDB provides access to a leveldb database on the local filesystem.
func OpenDB(path string) (*DB, error) {
	db, err := leveldb.OpenFile(path, &opt.Options{})
	if err != nil {
		return nil, errors.Wrapf(err, "open leveldb path=%q", path)
	}

	return &DB{
		ldb: db,
	}, nil
}

func (db *DB) Close() error {
	return db.ldb.Close()
}

func (db *DB) CompactAll() error {
	return db.ldb.CompactRange(util.Range{})
}

func (db *DB) StoreTransaction(tx *api.Transaction) error {
	txid := idForTx(tx)

	buf, err := proto.Marshal(tx)
	if err != nil {
		return errors.Wrapf(err, "marshal transaction txid=%q", txid)
	}

	hash := sha256.Sum256(buf)
	key := path.Join("/tx/", txid.String(), hex.EncodeToString(hash[:]))

	// TODO(rhys): batch writes
	err = db.ldb.Put([]byte(key), buf, nil)
	if err != nil {
		return errors.Wrapf(err, "write transaction key=%q", key)
	}

	return nil
}

func idForTx(t *api.Transaction) tx.TransactionID {
	txid := t.TransactionId
	switch len(txid) {
	case 0:
		return tx.TransactionID{0, 0}
	case 1:
		return tx.TransactionID{0, t.TransactionId[0]}
	default:
		return tx.TransactionID{t.TransactionId[0], t.TransactionId[1]}

	}
}
