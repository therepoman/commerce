# Terraform scripts #


## Kafka cluster ##

The cluster has 4 nodes, each with 6TB of space dedicated to kafka logs. They are d2.xlarge instances: 4 CPUs, 32GB of memory each.

### Adding a topic ###

Adding a topic requires knowing a zookeeper node URI. You can get that from `terraform show:`

```
$ terraform show | grep "fqdn = trace-zk"
fqdn = trace-zk-b3421344.prod.us-west2.justin.tv
```

Use the `kafka-topics.sh` script provided with Kafka to create topics on the zookeeper node. The cluster has 4 kafka nodes, so choose a number of partitions divisible by 4 to smooth load across nodes, and don't set replication over 4 or you'll get an error.
```
spencer@trace-kafka-cd00533a:/opt/kafka$ ./bin/kafka-topics.sh --zookeeper trace-zk-b3421344.prod.us-west2.justin.tv --create --topic trace-testing --partitions 32 --replication 2
Created topic "trace-testing".
```

To validate that your topic was successfully created, use `kafka-topics --describe`:

```
spencer@trace-kafka-cd00533a:/opt/kafka$ ./bin/kafka-topics.sh --zookeeper trace-zk-b3421344.prod.us-west2.justin.tv --describe
Topic:trace-testing	PartitionCount:32	ReplicationFactor:2	Configs:
	Topic: trace-testing	Partition: 0	Leader: 2	Replicas: 2,3	Isr: 2,3
	Topic: trace-testing	Partition: 1	Leader: 3	Replicas: 3,0	Isr: 3,0
	Topic: trace-testing	Partition: 2	Leader: 0	Replicas: 0,1	Isr: 0,1
	Topic: trace-testing	Partition: 3	Leader: 1	Replicas: 1,2	Isr: 1,2
	Topic: trace-testing	Partition: 4	Leader: 2	Replicas: 2,0	Isr: 2,0
	Topic: trace-testing	Partition: 5	Leader: 3	Replicas: 3,1	Isr: 3,1
	Topic: trace-testing	Partition: 6	Leader: 0	Replicas: 0,2	Isr: 0,2
	Topic: trace-testing	Partition: 7	Leader: 1	Replicas: 1,3	Isr: 1,3
	Topic: trace-testing	Partition: 8	Leader: 2	Replicas: 2,1	Isr: 2,1
	Topic: trace-testing	Partition: 9	Leader: 3	Replicas: 3,2	Isr: 3,2
	Topic: trace-testing	Partition: 10	Leader: 0	Replicas: 0,3	Isr: 0,3
	Topic: trace-testing	Partition: 11	Leader: 1	Replicas: 1,0	Isr: 1,0
	Topic: trace-testing	Partition: 12	Leader: 2	Replicas: 2,3	Isr: 2,3
	Topic: trace-testing	Partition: 13	Leader: 3	Replicas: 3,0	Isr: 3,0
	Topic: trace-testing	Partition: 14	Leader: 0	Replicas: 0,1	Isr: 0,1
	Topic: trace-testing	Partition: 15	Leader: 1	Replicas: 1,2	Isr: 1,2
	Topic: trace-testing	Partition: 16	Leader: 2	Replicas: 2,0	Isr: 2,0
	Topic: trace-testing	Partition: 17	Leader: 3	Replicas: 3,1	Isr: 3,1
	Topic: trace-testing	Partition: 18	Leader: 0	Replicas: 0,2	Isr: 0,2
	Topic: trace-testing	Partition: 19	Leader: 1	Replicas: 1,3	Isr: 1,3
	Topic: trace-testing	Partition: 20	Leader: 2	Replicas: 2,1	Isr: 2,1
	Topic: trace-testing	Partition: 21	Leader: 3	Replicas: 3,2	Isr: 3,2
	Topic: trace-testing	Partition: 22	Leader: 0	Replicas: 0,3	Isr: 0,3
	Topic: trace-testing	Partition: 23	Leader: 1	Replicas: 1,0	Isr: 1,0
	Topic: trace-testing	Partition: 24	Leader: 2	Replicas: 2,3	Isr: 2,3
	Topic: trace-testing	Partition: 25	Leader: 3	Replicas: 3,0	Isr: 3,0
	Topic: trace-testing	Partition: 26	Leader: 0	Replicas: 0,1	Isr: 0,1
	Topic: trace-testing	Partition: 27	Leader: 1	Replicas: 1,2	Isr: 1,2
	Topic: trace-testing	Partition: 28	Leader: 2	Replicas: 2,0	Isr: 2,0
	Topic: trace-testing	Partition: 29	Leader: 3	Replicas: 3,1	Isr: 3,1
	Topic: trace-testing	Partition: 30	Leader: 0	Replicas: 0,2	Isr: 0,2
	Topic: trace-testing	Partition: 31	Leader: 1	Replicas: 1,3	Isr: 1,3
```
