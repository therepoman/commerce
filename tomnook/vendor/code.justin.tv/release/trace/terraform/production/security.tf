// Network lists can be found on the wiki
// https://twitchtv.atlassian.net/wiki/display/NET/Global+Assignments
//
// They can also be found in the usher codebase in lib/common_config.py
// https://git-aws.internal.justin.tv/video/usher/blob/master/lib/common_config.py
//
// Hurricane Electric can confirm the advertisements of AS 46489
// http://bgp.he.net/AS46489#_prefixes

resource "aws_security_group" "trace_internal" {
  name = "trace-internal"
  description = "Access to Trace data is limited to dedicated Trace hosts and ELB"
  vpc_id = "${var.vendored_base["prod_vpc"]}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",         // RFC 1918, production network
      "23.160.0.0/24",      // AS 46489, global anycast
      "38.99.7.224/27",     // office NAT
      "38.99.10.96/29",     // office NAT
      "38.104.129.210/32",  // Amazon IT office NAT
      "45.113.128.0/22",    // AS 46489
      "52.223.240.0/20",    // AS 46489
      "103.53.48.0/22",     // AS 46489
      "127.0.0.0/8",        // loopback
      "185.42.204.0/22",    // AS 46489
      "192.16.64.0/21",     // AS 46489
      "192.108.239.0/24",   // AS 46489, global anycast
      "192.168.0.0/17",     // RFC 1918, office network
      "192.168.240.0/21",   // RFC 1918, office wifi
      "199.9.248.0/21",     // AS 46489
    ]
  }

  ingress {
    from_port = 11143
    to_port = 11144
    protocol = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",         // RFC 1918, production network
      "23.160.0.0/24",      // AS 46489, global anycast
      "38.99.7.224/27",     // office NAT
      "38.99.10.96/29",     // office NAT
      "38.104.129.210/32",  // Amazon IT office NAT
      "45.113.128.0/22",    // AS 46489
      "52.223.240.0/20",    // AS 46489
      "103.53.48.0/22",     // AS 46489
      "127.0.0.0/8",        // loopback
      "185.42.204.0/22",    // AS 46489
      "192.16.64.0/21",     // AS 46489
      "192.108.239.0/24",   // AS 46489, global anycast
      "192.168.0.0/17",     // RFC 1918, office network
      "192.168.240.0/21",   // RFC 1918, office wifi
      "199.9.248.0/21",     // AS 46489
    ]
  }
}

resource "aws_security_group" "trace_external" {
  name = "trace-external"
  description = "Access to Trace ELB is allowed for hosts on our network"
  vpc_id = "${var.vendored_base["prod_vpc"]}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",         // RFC 1918, production network
      "23.160.0.0/24",      // AS 46489, global anycast
      "38.99.7.224/27",     // office NAT
      "38.99.10.96/29",     // office NAT
      "38.104.129.210/32",  // Amazon IT office NAT
      "45.113.128.0/22",    // AS 46489
      "52.223.240.0/20",    // AS 46489
      "103.53.48.0/22",     // AS 46489
      "127.0.0.0/8",        // loopback
      "185.42.204.0/22",    // AS 46489
      "192.16.64.0/21",     // AS 46489
      "192.108.239.0/24",   // AS 46489, global anycast
      "192.168.0.0/17",     // RFC 1918, office network
      "192.168.240.0/21",   // RFC 1918, office wifi
      "199.9.248.0/21",     // AS 46489
    ]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",         // RFC 1918, production network
      "23.160.0.0/24",      // AS 46489, global anycast
      "38.99.7.224/27",     // office NAT
      "38.99.10.96/29",     // office NAT
      "38.104.129.210/32",  // Amazon IT office NAT
      "45.113.128.0/22",    // AS 46489
      "52.223.240.0/20",    // AS 46489
      "103.53.48.0/22",     // AS 46489
      "127.0.0.0/8",        // loopback
      "185.42.204.0/22",    // AS 46489
      "192.16.64.0/21",     // AS 46489
      "192.108.239.0/24",   // AS 46489, global anycast
      "192.168.0.0/17",     // RFC 1918, office network
      "192.168.240.0/21",   // RFC 1918, office wifi
      "199.9.248.0/21",     // AS 46489
    ]
  }
}

