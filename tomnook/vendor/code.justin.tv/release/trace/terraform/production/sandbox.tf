variable "sandbox-application" {
  default = "trace-sandbox"
}

data "template_file" "sandbox-kinesis-application" {
  template = "${file("policies/kinesis_application_member.json.template")}"
  vars {
    region = "${var.region}"
    account-id = "${var.account-id}"
    application = "${var.sandbox-application}"
  }
}

// Role which can read from any stream, but only can write to the sandbox stream.
resource "aws_iam_role_policy" "sandbox" {
  name   = "trace-kinesis-sandbox"
  role   = "${aws_iam_role.sandbox.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    ${data.template_file.trace-sandbox-write-policy.rendered},
    ${data.template_file.trace-sandbox-read-policy.rendered},
    ${data.template_file.trace-events-read-policy.rendered},
    ${data.template_file.trace-transactions-read-policy.rendered},
    ${data.template_file.sandbox-kinesis-application.rendered},
    ${file("policies/cloudwatch_writer.json")}
  ]
}
EOF
}

resource "aws_iam_role" "sandbox" {
  name = "trace-kinesis-sandbox"
  assume_role_policy = "${file("policies/assume_role.json")}"
}

resource "aws_iam_instance_profile" "sandbox" {
  name = "trace-kinesis-sandbox"
  roles = ["${aws_iam_role.sandbox.name}"]
}

resource "aws_launch_configuration" "trace-kinesis-sandbox" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["sandbox.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.sandbox.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "1024"// gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/production.txt"), "trace-kinesis-sandbox", "trace_other")}"
}

resource "aws_autoscaling_group" "trace-kinesis-sandbox" {
  name = "trace-kinesis-sandbox"
  min_size = "${var.instances["sandbox.count"]}"
  max_size = "${var.instances["sandbox.count"]}"
  desired_capacity = "${var.instances["sandbox.count"]}"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-kinesis-sandbox.name}"
  load_balancers = ["${aws_elb.trace-kinesis-sandbox.name}"]
  health_check_type = "ELB"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-kinesis-sandbox"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/sandbox"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod/darklaunch"
    propagate_at_launch = true
  }
}

resource "aws_elb" "trace-kinesis-sandbox" {
  name = "trace-kinesis-sandbox"
  internal = true
  cross_zone_load_balancing = false
  subnets = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]

  listener {
    lb_protocol = "TCP"
    lb_port = 22

    instance_protocol = "TCP"
    instance_port = 22
  }

  health_check {
    healthy_threshold = 10
    unhealthy_threshold = 2
    target = "TCP:22"
    interval = 30
    timeout = 5
  }

  idle_timeout = 3600

  tags {
    Name = "trace-kinesis-sandbox"
  }
}

resource "aws_route53_record" "trace-kinesis-sandbox-vanity" {
  zone_id = "${var.vendored_base["r53"]}"
  name = "trace-kinesis-sandbox.prod.us-west2.justin.tv"
  type = "A"

  alias {
    name = "${aws_elb.trace-kinesis-sandbox.dns_name}"
    zone_id = "${aws_elb.trace-kinesis-sandbox.zone_id}"
    evaluate_target_health = false
  }
}
