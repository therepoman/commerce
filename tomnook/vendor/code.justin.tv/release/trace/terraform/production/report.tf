// Permissions: an IAM role for reading events and writing transactions
data "template_file" "report-kinesis-application" {
  template = "${file("policies/kinesis_application_member.json.template")}"
  vars {
    region = "${var.region}"
    account-id = "${var.account-id}"
    application = "trace-report"
  }
}

resource "aws_iam_role" "report" {
  name = "trace-report"
  assume_role_policy = "${file("policies/assume_role.json")}"
}

resource "aws_iam_role_policy" "report" {
  name = "trace-report"
  role = "${aws_iam_role.report.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    ${data.template_file.trace-transactions-read-policy.rendered},
    ${data.template_file.aggregator-kinesis-application.rendered},
    ${file("policies/cloudwatch_writer.json")}
  ]
}
EOF
}

resource "aws_iam_instance_profile" "report" {
  name = "trace-report"
  roles = ["${aws_iam_role.report.name}"]
}

// Production

resource "aws_launch_configuration" "trace-report" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["report.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.report.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "512" // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/production.txt"), "trace-report", "trace_report")}"
}

resource "aws_autoscaling_group" "trace-report" {
  name = "trace-report"
  min_size = "${var.instances["report.count"]}"
  max_size = "${var.instances["report.count"]}"
  desired_capacity = "${var.instances["report.count"]}"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-report.name}"
  load_balancers = ["${aws_elb.trace-report.name}"]
  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-report"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/report"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod"
    propagate_at_launch = true
  }
}

resource "aws_elb" "trace-report" {
  name = "trace-report"
  internal = true
  subnets = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]

  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = "arn:aws:iam::673385534282:server-certificate/wildcard.internal.justin.tv"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:80/running"
    interval = 30
  }

  tags {
    Name = "trace-report"
  }
  cross_zone_load_balancing = true
}

resource "aws_route53_record" "trace-report-vanity" {
  zone_id = "${var.vendored_base["r53"]}"
  name = "trace.prod.us-west2.justin.tv"
  type = "A"

  alias {
    name = "${aws_elb.trace-report.dns_name}"
    zone_id = "${aws_elb.trace-report.zone_id}"
    evaluate_target_health = false
  }
}

// Canary

resource "aws_launch_configuration" "trace-report-canary" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["report.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.report.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "512" // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/canary.txt"), "trace-report", "trace_report")}"
}

resource "aws_autoscaling_group" "trace-report-canary" {
  name = "trace-report-canary"
  min_size = "1"
  max_size = "1"
  desired_capacity = "1"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-report-canary.name}"
  load_balancers = ["${aws_elb.trace-report-canary.name}"]
  target_group_arns = ["${aws_alb_target_group.trace-report-canary.arn}"]
  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-report-canary"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/report"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod/canary"
    propagate_at_launch = true
  }
}

resource "aws_elb" "trace-report-canary" {
  name = "trace-report-canary"
  internal = true
  subnets = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]

  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = "arn:aws:iam::673385534282:server-certificate/wildcard.internal.justin.tv"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:80/running"
    interval = 30
  }

  tags {
    Name = "trace-report-canary"
  }
  cross_zone_load_balancing = true
}

resource "aws_route53_record" "trace-report-canary-vanity" {
  zone_id = "${var.vendored_base["r53"]}"
  name = "trace-report.canary.us-west2.justin.tv"
  type = "A"

  alias {
    name = "${aws_elb.trace-report-canary.dns_name}"
    zone_id = "${aws_elb.trace-report-canary.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_alb" "trace-report-canary" {
  name = "trace-report-canary"
  internal = true
  subnets = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]
}

resource "aws_alb_listener" "trace-report-canary" {
  load_balancer_arn = "${aws_alb.trace-report-canary.arn}"
  port = "443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-2015-05"
  certificate_arn = "arn:aws:iam::673385534282:server-certificate/wildcard.prod.us-west2.justin.tv"

  default_action {
    target_group_arn = "${aws_alb_target_group.trace-report-canary.arn}"
    type = "forward"
  }
}

resource "aws_alb_target_group" "trace-report-canary" {
  name = "trace-report-canary"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.vendored_base["prod_vpc"]}"

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    path = "/running"
    port = "traffic-port"
    protocol = "HTTP"
    matcher = "200"
  }
}

resource "aws_route53_record" "trace-report-canary-alb-vanity" {
  zone_id = "${var.vendored_base["r53"]}"
  name = "trace-report-canary.prod.us-west2.justin.tv"
  type = "A"

  alias {
    name = "${aws_alb.trace-report-canary.dns_name}"
    zone_id = "${aws_alb.trace-report-canary.zone_id}"
    evaluate_target_health = false
  }
}
