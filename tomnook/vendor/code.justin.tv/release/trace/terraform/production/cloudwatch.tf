# SNS topic that receives cloudwatch alarms
resource "aws_sns_topic" "alarms" {
  name = "trace-alarms"
}

resource "aws_sns_topic_subscription" "pagerduty" {
  topic_arn = "${aws_sns_topic.alarms.arn}"
  protocol = "https"
  endpoint = "${var.pagerduty_url}"
  endpoint_auto_confirms = true
}
