// Permissions

data "template_file" "api-kinesis-application" {
  template = "${file("policies/kinesis_application_member.json.template")}"
  vars {
    region = "${var.region}"
    account-id = "${var.account-id}"
    application = "trace-api"
  }
}

resource "aws_iam_role" "api" {
  name = "trace-api"
  assume_role_policy = "${file("policies/assume_role.json")}"
}

resource "aws_iam_role_policy" "api" {
  name = "trace-api"
  role = "${aws_iam_role.api.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    ${data.template_file.trace-transactions-read-policy.rendered},
    ${data.template_file.api-kinesis-application.rendered},
    ${file("policies/cloudwatch_writer.json")}
  ]
}
EOF
}

resource "aws_iam_instance_profile" "api" {
  name = "trace-api"
  roles = ["${aws_iam_role.api.name}"]
}

// Production

resource "aws_launch_configuration" "trace-api" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["api.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.api.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "64"  // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/production.txt"), "trace-api", "trace_api")}"
}

resource "aws_autoscaling_group" "trace-api" {
  name = "trace-api"
  min_size = "${var.instances["api.count"]}"
  max_size = "${var.instances["api.count"]}"
  desired_capacity = "${var.instances["api.count"]}"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-api.name}"
  load_balancers = ["${aws_elb.trace-api.name}"]
  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-api"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/api"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod"
    propagate_at_launch = true
  }
}

// Canary

resource "aws_launch_configuration" "trace-api-canary" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["api.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.api.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "64"  // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/canary.txt"), "trace-api", "trace_api")}"
}

resource "aws_autoscaling_group" "trace-api-canary" {
  name = "trace-api-canary"
  min_size = "1"
  max_size = "1"
  desired_capacity = "1"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-api-canary.name}"
  load_balancers = ["${aws_elb.trace-api.name}"]
  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-api-canary"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/api"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod/canary"
    propagate_at_launch = true
  }
}

// ELB

resource "aws_elb" "trace-api" {
  name = "trace-api"
  internal = true
  cross_zone_load_balancing = false
  subnets = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]

  listener {
    lb_protocol = "TCP"
    lb_port = 11143

    instance_protocol = "TCP"
    instance_port = 11143
  }

  listener {
    lb_protocol = "http"
    lb_port = 11144

    instance_protocol = "http"
    instance_port = 11144
  }

  health_check {
    healthy_threshold = 10
    unhealthy_threshold = 2
    target = "TCP:11143"
    interval = 30
    timeout = 5
  }

  tags {
    Name = "trace-api"
  }
}

resource "aws_route53_record" "trace-api-vanity" {
  zone_id = "${var.vendored_base["r53"]}"
  name = "trace-api.prod.us-west2.justin.tv"
  type = "A"

  alias {
    name = "${aws_elb.trace-api.dns_name}"
    zone_id = "${aws_elb.trace-api.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_cloudwatch_metric_alarm" "api-network-in" {
  alarm_name = "trace-api-network-in"
  alarm_description = "At least one trace-api instance is doing very little inbound network"

  comparison_operator = "LessThanThreshold"
  threshold = "1000000" // unit is bytes per minute; healthy values are about 5-20 billion
  evaluation_periods = 20
  period = "60"
  statistic = "Minimum"

  namespace = "AWS/EC2"
  metric_name = "NetworkIn"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.trace-api.name}"
  }

  alarm_actions = ["${aws_sns_topic.alarms.arn}"]
  ok_actions = ["${aws_sns_topic.alarms.arn}"]
}
