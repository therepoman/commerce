variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-west-2"
}
variable "account-id" {
  default = "673385534282"
}
variable "pagerduty_url" {
  default = "https://events.pagerduty.com/integration/117de43e1e5e46c486d8a220b2791172/enqueue"
}

variable "instances" {
  default = {
    api.type = "c4.8xlarge"
    api.count = 1

    aggregator.type = "c4.8xlarge"
    aggregator.count = 12

    collect.type = "c4.8xlarge"
    collect.count = 12

    kafka.type = "d2.xlarge"
    kafka.count = 8

    kafka_consumer_monitor.type = "c4.large"
    kafka_consumer_monitor.count = 1

    report.type = "r3.4xlarge"
    report.count = 1

    sandbox.type = "c4.8xlarge"
    sandbox.count = 1

    statsdsink.type = "c4.2xlarge"
    statsdsink.count = 12

    zookeeper.type = "m3.large"
    zookeeper.count = 1
  }
}

variable "streams" {
  default = {
    events.name    = "trace-events"
    events.shards  = 450

    transactions.name = "trace-transactions"
    transactions.shards = 450

    sandbox.name   = "trace-testing"
    sandbox.shards = 2
  }
}

variable "aws_subnets" {
  default = {
    a = "subnet-faa4239f"
    b = "subnet-88a207ff"
    c = "subnet-9c4ab1c5"
  }
}

// Remove reference to "base" module until they're updated to terraform-0.7 syntax
// module "base" {
//   source = "git::git+ssh://git@git-aws.internal.justin.tv/release/terraform.git?ref=a11f63ceda22e8e2d7a098485922f25028f88c8b//base"
// }

variable "vendored_base" {
  default = {
    r53 = "ZRG00SM48517Z"
    prod_vpc = "vpc-0213b167"
  }
}

variable "ami" {
  default = {
    blue = ""
    green = "ami-dd11f3bd"
  }
}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "${var.region}"
}

variable "owner_tag" {
  default = "edge-reliability@twitch.tv"
}

variable "service_tag_prefix" {
  default = "ear/trace"
}
