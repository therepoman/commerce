// Permissions: an IAM role for writing events.
resource "aws_iam_role_policy" "event-production" {
  name = "trace-event-production"
  role = "${aws_iam_role.event-producer.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    ${data.template_file.trace-events-write-policy.rendered},
    ${file("policies/cloudwatch_writer.json")}
  ]
}
EOF
}

resource "aws_iam_role" "event-producer" {
  name = "trace-event-producer"
  assume_role_policy = "${file("policies/assume_role.json")}"
}

resource "aws_iam_instance_profile" "event-producer" {
  name = "trace-event-producer"
  roles = ["${aws_iam_role.event-producer.name}"]
}

// Production

resource "aws_launch_configuration" "trace-collect" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["collect.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.event-producer.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]
  root_block_device {
    volume_size = "64"  // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/production.txt"), "trace-collect", "trace_collector")}"
}

resource "aws_autoscaling_group" "trace-collect" {
  name = "trace-collect"
  min_size = "${var.instances["collect.count"]}"
  max_size = "${var.instances["collect.count"]}"
  desired_capacity = "${var.instances["collect.count"]}"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-collect.name}"
  load_balancers = ["${aws_elb.trace-collect.name}"]
  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-collect"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/collect"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod"
    propagate_at_launch = true
  }
}

// Canary

resource "aws_launch_configuration" "trace-collect-canary" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["collect.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.event-producer.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]
  root_block_device {
    volume_size = "64"  // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/canary.txt"), "trace-collect", "trace_collector")}"
}

resource "aws_autoscaling_group" "trace-collect-canary" {
  name = "trace-collect-canary"
  min_size = "1"
  max_size = "1"
  desired_capacity = "1"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-collect-canary.name}"
  load_balancers = ["${aws_elb.trace-collect.name}"]
  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-collect-canary"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/collect"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod/canary"
    propagate_at_launch = true
  }
}

// ELB

resource "aws_elb" "trace-collect" {
  name = "trace-collect"
  internal = true
  cross_zone_load_balancing = false
  idle_timeout = 450
  subnets = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  security_groups = [
    "${aws_security_group.trace_internal.id}",
    "${aws_security_group.trace_external.id}",
  ]

  listener {
    lb_protocol = "TCP"
    lb_port = 11143

    instance_protocol = "TCP"
    instance_port = 11143
  }

  listener {
    lb_protocol = "http"
    lb_port = 11144

    instance_protocol = "http"
    instance_port = 11144
  }

  health_check {
    healthy_threshold = 10
    unhealthy_threshold = 2
    target = "HTTP:11144/debug/pprof/"
    interval = 30
    timeout = 5
  }

  tags {
    Name = "trace-collect"
  }
}

resource "aws_route53_record" "trace-collect-vanity" {
  zone_id = "${var.vendored_base["r53"]}"
  name = "trace-collect.prod.us-west2.justin.tv"
  type = "CNAME"
  ttl = "60"
  records = ["${aws_elb.trace-collect.dns_name}"]
}

resource "aws_cloudwatch_metric_alarm" "collect-network-in" {
  alarm_name = "trace-collect-network-in"
  alarm_description = "At least one trace-collect instance is doing very little inbound network"

  comparison_operator = "LessThanThreshold"
  threshold = "1000000" // unit is bytes per minute; healthy values are about 200,000,000 to 2,000,000,000
  evaluation_periods = 20
  period = "60"
  statistic = "Minimum"

  namespace = "AWS/EC2"
  metric_name = "NetworkIn"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.trace-collect.name}"
  }

  alarm_actions = ["${aws_sns_topic.alarms.arn}"]
  ok_actions = ["${aws_sns_topic.alarms.arn}"]
}
