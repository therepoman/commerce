// trace-events
resource "aws_kinesis_stream" "events" {
  name = "${var.streams["events.name"]}"
  shard_count = "${var.streams["events.shards"]}"
}

data "template_file" "trace-events-read-policy" {
  template = "${file("${path.root}/policies/read_kinesis.json.template")}"
  vars {
    stream_arn = "${aws_kinesis_stream.events.arn}"
  }
}

data "template_file" "trace-events-write-policy" {
  template = "${file("${path.root}/policies/write_kinesis.json.template")}"
  vars {
    stream_arn = "${aws_kinesis_stream.events.arn}"
  }
}

// trace-transactions
resource "aws_kinesis_stream" "transactions" {
  name = "${var.streams["transactions.name"]}"
  shard_count = "${var.streams["transactions.shards"]}"
}

data "template_file" "trace-transactions-read-policy" {
  template = "${file("${path.root}/policies/read_kinesis.json.template")}"
  vars {
    stream_arn = "${aws_kinesis_stream.transactions.arn}"
  }
}

data "template_file" "trace-transactions-write-policy" {
  template = "${file("${path.root}/policies/write_kinesis.json.template")}"
  vars {
    stream_arn = "${aws_kinesis_stream.transactions.arn}"
  }
}

// trace-sandbox
resource "aws_kinesis_stream" "sandbox" {
  name = "${var.streams["sandbox.name"]}"
  shard_count = "${var.streams["sandbox.shards"]}"
}

data "template_file" "trace-sandbox-write-policy" {
  template = "${file("${path.root}/policies/write_kinesis.json.template")}"
  vars {
    stream_arn = "${aws_kinesis_stream.sandbox.arn}"
  }
}

data "template_file" "trace-sandbox-read-policy" {
  template = "${file("${path.root}/policies/read_kinesis.json.template")}"
  vars {
    stream_arn = "${aws_kinesis_stream.sandbox.arn}"
  }
}
