// Permissions: an IAM role for reading events and writing transactions
data "template_file" "aggregator-kinesis-application" {
  template = "${file("policies/kinesis_application_member.json.template")}"
  vars {
    region = "${var.region}"
    account-id = "${var.account-id}"
    application = "trace-aggregator"
  }
}

resource "aws_iam_role" "aggregator" {
  name = "trace-aggregator"
  assume_role_policy = "${file("policies/assume_role.json")}"
}

resource "aws_iam_role_policy" "aggregator" {
  name = "trace-aggregator"
  role = "${aws_iam_role.aggregator.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    ${data.template_file.trace-events-read-policy.rendered},
    ${data.template_file.trace-transactions-write-policy.rendered},
    ${data.template_file.aggregator-kinesis-application.rendered},
    ${file("policies/cloudwatch_writer.json")}
  ]
}
EOF
}

resource "aws_iam_instance_profile" "aggregator" {
  name = "trace-aggregator"
  roles = ["${aws_iam_role.aggregator.name}"]
}

resource "aws_launch_configuration" "trace-aggregator-kinesis" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["aggregator.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.aggregator.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "64"  // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/production.txt"), "trace-aggregator-kinesis", "trace_aggregator_kinesis")}"
}

resource "aws_autoscaling_group" "trace-aggregator-kinesis" {
  name = "trace-aggregator-kinesis"
  min_size = "${var.instances["aggregator.count"]}"
  max_size = "${var.instances["aggregator.count"]}"
  desired_capacity = "${var.instances["aggregator.count"]}"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-aggregator-kinesis.name}"

  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-aggregator-kinesis"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/aggregator"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod"
    propagate_at_launch = true
  }
}

resource "aws_launch_configuration" "trace-aggregator-kinesis-canary" {
  image_id = "${var.ami["green"]}"
  instance_type = "${var.instances["aggregator.type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.aggregator.name}"
  security_groups = [
    "${aws_security_group.trace_internal.id}",
  ]
  root_block_device {
    volume_size = "64"  // gigabytes
    volume_type = "gp2" // ssd
  }

  lifecycle {
    create_before_destroy = true
  }

  user_data = "${format(file("userdata/canary.txt"), "trace-aggregator-kinesis", "trace_aggregator_kinesis")}"
}

resource "aws_autoscaling_group" "trace-aggregator-kinesis-canary" {
  name = "trace-aggregator-kinesis-canary"
  min_size = "1"
  max_size = "1"
  desired_capacity = "1"
  vpc_zone_identifier = [
    "${var.aws_subnets["a"]}",
    "${var.aws_subnets["b"]}",
    "${var.aws_subnets["c"]}",
  ]
  launch_configuration = "${aws_launch_configuration.trace-aggregator-kinesis-canary.name}"

  health_check_type = "EC2"
  health_check_grace_period = 1800

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key = "Name"
    value = "trace-aggregator-kinesis-canary"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "${var.service_tag_prefix}/aggregator"
    propagate_at_launch = true
  }
  tag {
    key = "Owner"
    value = "${var.owner_tag}"
    propagate_at_launch = true
  }
  tag {
    key = "Environment"
    value = "prod/canary"
    propagate_at_launch = true
  }
}

resource "aws_cloudwatch_metric_alarm" "aggregator-network-in" {
  alarm_name = "trace-aggregator-network-in"
  alarm_description = "At least one trace-aggregator instance is doing very little inbound network"

  comparison_operator = "LessThanThreshold"
  threshold = "1000000" // unit is bytes per minute; healthy values are about 1-2 billion
  evaluation_periods = 20
  period = "60"
  statistic = "Minimum"

  namespace = "AWS/EC2"
  metric_name = "NetworkIn"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.trace-aggregator-kinesis.name}"
  }

  alarm_actions = ["${aws_sns_topic.alarms.arn}"]
  ok_actions = ["${aws_sns_topic.alarms.arn}"]
}
