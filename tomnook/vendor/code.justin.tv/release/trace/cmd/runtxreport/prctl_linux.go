package main

import (
	"os/exec"
	"syscall"
)

func init() {
	osSetOptions = linuxSetOptions
}

func linuxSetOptions(cmd *exec.Cmd) {
	if cmd.SysProcAttr == nil {
		cmd.SysProcAttr = &syscall.SysProcAttr{}
	}
	cmd.SysProcAttr.Pdeathsig = syscall.SIGKILL
}
