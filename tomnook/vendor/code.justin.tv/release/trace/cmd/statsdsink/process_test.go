package main

import (
	"reflect"
	"testing"
	"time"

	"code.justin.tv/release/trace/common"
	"code.justin.tv/release/trace/events"
)

func TestClean(t *testing.T) {
	type testcase struct {
		raw   string
		clean string
	}
	testcases := []testcase{
		testcase{`aBc_123`, `aBc_123`},
		testcase{`aBc-123`, `aBc-123`},
		testcase{`message#list {:box=>"other"}`, `message-list-box-other`},
	}

	for _, tc := range testcases {
		have, want := clean(tc.raw), tc.clean
		if have != want {
			t.Errorf("clean  have=%q  want=%q", have, want)
		}
	}
}

func TestParseCall(t *testing.T) {
	type testcase struct {
		in   Call
		want Metric
	}

	req := &events.Event{
		Time: 0,
		Extra: &events.Extra{
			Http: &events.ExtraHTTP{
				Method:     common.Method_GET,
				StatusCode: 100,
				UriPath:    "/foo",
			},
		},
	}
	resp := &events.Event{
		Time: 5,
		Extra: &events.Extra{
			Http: &events.ExtraHTTP{
				StatusCode: 200,
			},
		},
	}
	call := &Call{req: req, resp: resp}
	want := &Metric{
		environment: "xxx",
		hostname:    "xxx",
		route:       "xxx",
		status:      200,
		duration:    time.Duration(5),
	}
	have := parseCall(call)
	if !reflect.DeepEqual(have, want) {
		t.Errorf("parseCall  have=%+v  want=%+v", have, want)
	}
}
