//+build linux

package main

import "syscall"

var cmdAttributes = &syscall.SysProcAttr{
	// for an explanation of the choice of SIGUSR2, see
	// https://git-aws.internal.justin.tv/common/chitin/blob/master/internal/signal/README
	Pdeathsig: syscall.SIGUSR2,
}
