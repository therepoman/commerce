package main

import (
	"fmt"
	"testing"

	"github.com/golang/protobuf/proto"

	"code.justin.tv/release/trace/analysis/paths"
	"code.justin.tv/release/trace/analysis/routes"
	"code.justin.tv/release/trace/common"
	"code.justin.tv/release/trace/events"
	"code.justin.tv/release/trace/pbmsg"
)

func mockTransaction() *pbmsg.Transaction {
	// returns a transaction that looks like this:
	// []
	//   [0]
	//   [1]
	//      [1,3]
	//      [1,4]
	//   [2]
	return &pbmsg.Transaction{
		Root: &pbmsg.Call{
			Path: []uint32{},
			Svc:  &pbmsg.Service{Name: proto.String("[]")},
			Subcalls: []*pbmsg.Call{
				&pbmsg.Call{
					Path: []uint32{0},
					Svc:  &pbmsg.Service{Name: proto.String("[0]")},
				},
				&pbmsg.Call{
					Path: []uint32{1},
					Svc:  &pbmsg.Service{Name: proto.String("[1]")},
					Subcalls: []*pbmsg.Call{
						&pbmsg.Call{
							Path: []uint32{1, 1},
							Svc:  &pbmsg.Service{Name: proto.String("[1,1]")},
						},
						&pbmsg.Call{
							Path: []uint32{1, 3},
							Svc:  &pbmsg.Service{Name: proto.String("[1,3]")},
						},
						&pbmsg.Call{
							Path: []uint32{1, 4},
							Svc:  &pbmsg.Service{Name: proto.String("[1,4]")},
						},
					},
				},
				&pbmsg.Call{
					Path: []uint32{2},
					Svc:  &pbmsg.Service{Name: proto.String("[2]")},
				},
			},
		},
	}
}

func TestFindSubcallReturnsExpectedCall(t *testing.T) {
	type testcase struct {
		path        []uint32
		shouldExist bool
	}
	testcases := []testcase{
		testcase{[]uint32{}, true},
		testcase{[]uint32{0}, true},
		testcase{[]uint32{1}, true},
		testcase{[]uint32{2}, true},
		testcase{[]uint32{3}, false},
		testcase{[]uint32{1, 0}, false},
		testcase{[]uint32{1, 3}, true},
	}

	for _, tc := range testcases {
		tx := mockTransaction() // remake every time because find can add nodes
		subcall := findSubcallInTx(tx, tc.path)
		have := subcall.GetSvc().GetName()
		var want string
		if tc.shouldExist {
			want = "["
			for i, p := range tc.path {
				if i == 0 {
					want += fmt.Sprintf("%d", p)
				} else {
					want += fmt.Sprintf(",%d", p)
				}
			}
			want += "]"
		} else {
			want = ""
		}
		if have != want {
			t.Errorf("findSubcallInTx  have=%q  want=%q", have, want)
		}
	}
}

func TestFindSubcallInsertsCorrectly(t *testing.T) {
	tx := mockTransaction()

	initialSize := len(tx.Root.Subcalls)
	findSubcallInTx(tx, []uint32{3})
	if len(tx.Root.Subcalls) != initialSize+1 {
		t.Errorf("insert test 1:  have=%d, want=%d",
			len(tx.Root.Subcalls),
			initialSize+1,
		)
	}

	tx = mockTransaction()
	pathToInsert := []uint32{1, 2, 5, 7, 9}
	// index where the node should be at each depth
	desiredIndexes := []int{1, 1, 0, 0, 0}
	// size that that the Subcalls array should be at each depth
	desiredSizes := []int{3, 4, 1, 1, 1}
	findSubcallInTx(tx, pathToInsert)
	call := tx.Root
	for depth := range pathToInsert {
		t.Logf("call depth=%d  path=%d  subcalls=%+v", depth, call.Path, call.Subcalls)
		if !paths.PathMatches(call.Path, pathToInsert[:depth]) {
			t.Fatalf("insert test 2: unexpected path at depth %d, have=%v, want=%v",
				depth,
				call.Path,
				pathToInsert[:depth],
			)
		}
		if len(call.Subcalls) != desiredSizes[depth] {
			t.Fatalf("insert test 2: unexpected size at depth %d, have=%d, want=%d",
				depth,
				len(call.Subcalls),
				desiredSizes[depth],
			)
		}
		call = call.Subcalls[desiredIndexes[depth]]
		t.Logf("going for child at index %d: getting call with path %v", desiredIndexes[depth], call.Path)
	}
	// final call should have an empty Subcalls slice
	if len(call.Subcalls) != 0 {
		t.Errorf("insert test 3: unexpected leaf call.Subcalls len have=%d, want=%d",
			len(call.Subcalls),
			0,
		)
	}
}

func mockSortTestTransaction() *pbmsg.Transaction {
	return &pbmsg.Transaction{
		Root: &pbmsg.Call{
			Path: []uint32{},
			Subcalls: []*pbmsg.Call{
				&pbmsg.Call{
					Path: []uint32{1},
				},
				&pbmsg.Call{
					Path: []uint32{2},
				},
				&pbmsg.Call{
					Path: []uint32{4},
				},
				&pbmsg.Call{
					Path: []uint32{5},
				},
			},
		},
	}
}

func TestFindSubcallMaintainsSortedOrder(t *testing.T) {

	type testCase struct {
		path []uint32
		want []uint32
	}

	tests := []testCase{
		testCase{[]uint32{0}, []uint32{0, 1, 2, 4, 5}},
		testCase{[]uint32{1}, []uint32{1, 2, 4, 5}},
		testCase{[]uint32{2}, []uint32{1, 2, 4, 5}},
		testCase{[]uint32{3}, []uint32{1, 2, 3, 4, 5}},
		testCase{[]uint32{4}, []uint32{1, 2, 4, 5}},
		testCase{[]uint32{5}, []uint32{1, 2, 4, 5}},
		testCase{[]uint32{6}, []uint32{1, 2, 4, 5, 6}},
	}

	for _, tc := range tests {
		tx := mockSortTestTransaction()
		findSubcallInTx(tx, tc.path)
		for i, call := range tx.Root.Subcalls {
			have, want := call.GetPath()[0], tc.want[i]
			if have != want {
				t.Errorf("findSubcallInTx sort order  pos=%d have=%q want=%q",
					i, have, want,
				)
			}
		}
	}
}

func BenchmarkFindSubcallEasy(b *testing.B) {
	tx := mockTransaction()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		findSubcallInTx(tx, []uint32{1})
	}
}

func BenchmarkFindSubcallInsertEasy(b *testing.B) {
	tx := mockTransaction()
	b.ResetTimer()
	for i := 3; i < b.N+3; i++ {
		findSubcallInTx(tx, []uint32{uint32(i)})
	}
}

func BenchmarkFindSubcallHard(b *testing.B) {
	tx := mockTransaction()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		findSubcallInTx(tx, []uint32{
			uint32(1),
			uint32(3),
			uint32(i % 100),
			uint32(i * 3 % 100),
			uint32(i * 7 % 100),
			uint32(i * 11 % 100),
		})
	}
}

func TestAddEventSetsTimestamps(t *testing.T) {
	var (
		txid []uint64 = []uint64{1, 1}

		clientStartTime  int64 = 1
		serviceStartTime int64 = 2
		serviceEndTime   int64 = 3
		clientEndTime    int64 = 4

		clientStartKind  = common.Kind_REQUEST_HEAD_PREPARED
		serviceStartKind = common.Kind_REQUEST_HEAD_RECEIVED
		serviceEndKind   = common.Kind_RESPONSE_HEAD_PREPARED
		clientEndKind    = common.Kind_RESPONSE_HEAD_RECEIVED
	)
	tx := &pbmsg.Transaction{
		Root:          &pbmsg.Call{},
		TransactionId: txid,
	}
	clientStart := &events.Event{
		Kind:          clientStartKind,
		Path:          []uint32{},
		TransactionId: txid,
		Time:          clientStartTime,
	}
	serviceStart := &events.Event{
		Kind:          serviceStartKind,
		Path:          []uint32{},
		TransactionId: txid,
		Time:          serviceStartTime,
	}
	serviceEnd := &events.Event{
		Kind:          serviceEndKind,
		Path:          []uint32{},
		TransactionId: txid,
		Time:          serviceEndTime,
	}
	clientEnd := &events.Event{
		Kind:          clientEndKind,
		Path:          []uint32{},
		TransactionId: txid,
		Time:          clientEndTime,
	}

	// adding a client event should add a timestamp
	addEvent(tx, clientStart)
	clientTimestamps := tx.GetRoot().GetClientTimestamps()
	if len(clientTimestamps) != 1 {
		t.Errorf("addEvent(tx, ev) num client timestamps  have=%q want=%q", len(clientTimestamps), 1)
	} else {
		ts := clientTimestamps[0]
		if ts.GetTime() != clientStartTime {
			t.Errorf("addEvent(tx, ev) client start timestamp  have=%q want=%q", ts.GetTime(), clientStartTime)
		}
		if ts.Kind.String() != pbmsg.Event_Kind(clientStartKind).Enum().String() {
			t.Errorf("addEvent(tx, ev) client start kind  have=%q want=%q", ts.GetKind(), clientStartKind)
		}
	}
	// adding a service events should add a timestamp
	addEvent(tx, serviceStart)
	serviceTimestamps := tx.GetRoot().GetServiceTimestamps()
	if len(serviceTimestamps) != 1 {
		t.Errorf("addEvent(tx, ev) num service timestamps  have=%q want=%q", len(serviceTimestamps), 1)
	} else {
		ts := serviceTimestamps[0]
		if ts.GetTime() != serviceStartTime {
			t.Errorf("addEvent(tx, ev) service start timestamp  have=%q want=%q", ts.GetTime(), serviceStartTime)
		}
		if ts.Kind.String() != pbmsg.Event_Kind(serviceStartKind).Enum().String() {
			t.Errorf("addEvent(tx, ev) service start kind  have=%q want=%q", ts.GetKind(), serviceStartKind)
		}
	}

	// adding another client event should result in two client timestamps
	addEvent(tx, clientEnd)
	clientTimestamps = tx.GetRoot().GetClientTimestamps()
	if len(clientTimestamps) != 2 {
		t.Errorf("addEvent(tx, ev) num client timestamps  have=%q want=%q", len(clientTimestamps), 1)
	} else {
		// order of timestamps should be increasing times
		startTs := clientTimestamps[0]
		if startTs.GetTime() != clientStartTime {
			t.Errorf("addEvent(tx, ev) client start timestamp  have=%q want=%q", startTs.GetTime(), clientStartTime)
		}
		if startTs.Kind.String() != pbmsg.Event_Kind(clientStartKind).Enum().String() {
			t.Errorf("addEvent(tx, ev) client start timestamp  have=%q want=%q", startTs.GetKind(), clientStartKind)
		}
		endTs := clientTimestamps[1]
		if endTs.GetTime() != clientEndTime {
			t.Errorf("addEvent(tx, ev) client end timestamp  have=%q want=%q", endTs.GetTime(), clientEndTime)
		}
		if endTs.Kind.String() != pbmsg.Event_Kind(clientEndKind).Enum().String() {
			t.Errorf("addEvent(tx, ev) client end timestamp  have=%q want=%q", endTs.GetKind(), clientEndKind)
		}
	}

	// adding another service event should result in two service timestamps
	addEvent(tx, serviceEnd)
	serviceTimestamps = tx.GetRoot().GetServiceTimestamps()
	if len(serviceTimestamps) != 2 {
		t.Errorf("addEvent(tx, ev) num service timestamps  have=%q want=%q", len(serviceTimestamps), 1)
	} else {
		// order of timestamps should be increasing times
		startTs := serviceTimestamps[0]
		if startTs.GetTime() != serviceStartTime {
			t.Errorf("addEvent(tx, ev) service start timestamp  have=%q want=%q", startTs.GetTime(), serviceStartTime)
		}
		if startTs.Kind.String() != pbmsg.Event_Kind(serviceStartKind).Enum().String() {
			t.Errorf("addEvent(tx, ev) service start timestamp  have=%q want=%q", startTs.GetKind(), serviceStartKind)
		}
		endTs := serviceTimestamps[1]
		if endTs.GetTime() != serviceEndTime {
			t.Errorf("addEvent(tx, ev) service end timestamp  have=%q want=%q", endTs.GetTime(), serviceEndTime)
		}
		if endTs.Kind.String() != pbmsg.Event_Kind(serviceEndKind).Enum().String() {
			t.Errorf("addEvent(tx, ev) service end timestamp  have=%q want=%q", endTs.GetKind(), serviceEndKind)
		}
	}
}

func TestRoute(t *testing.T) {
	prev := routes.Routes
	defer func() {
		routes.Routes = prev
	}()

	routes.Routes = map[string]routes.RouteMap{
		"testsvc": routes.NewRouteMap(),
	}
	routes.Routes["testsvc"].AddRoute("/foo", "POST", "fooroute")

	call := &pbmsg.Call{
		Svc: &pbmsg.Service{
			Name: proto.String("testsvc"),
		},
	}
	params := &events.ExtraHTTP{
		Method:  common.Method(common.Method_value["POST"]),
		UriPath: "/foo",
	}
	addHTTPParams(call, params, false)

	if have, want := call.GetParams().GetHttp().GetRoute(), "fooroute"; have != want {
		t.Errorf("call.GetParams().GetHttp().GetRoute(); %q != %q", have, want)
	}
}

func TestClientIsSet(t *testing.T) {
	var (
		txid        []uint64 = []uint64{1, 1}
		clientName           = "client"
		clientKind           = common.Kind_REQUEST_HEAD_PREPARED
		serviceName          = "service"
		serviceKind          = common.Kind_REQUEST_HEAD_RECEIVED
		rootPath             = []uint32{}
		subPath              = []uint32{0}
	)
	tx := &pbmsg.Transaction{
		Root:          &pbmsg.Call{},
		TransactionId: txid,
	}

	// Make an event that the client sent from the root path
	clientEvent := &events.Event{
		Kind:          clientKind,
		Svcname:       clientName,
		Path:          rootPath,
		TransactionId: txid,
		Time:          1,
	}

	// Make an event that the service sent from the root path
	serviceEvent := &events.Event{
		Kind:          serviceKind,
		Svcname:       serviceName,
		Path:          rootPath,
		TransactionId: txid,
		Time:          1,
	}

	addEvent(tx, clientEvent)
	addEvent(tx, serviceEvent)

	// The client of the tx should now be set properly
	if tx.GetClient() == nil {
		t.Fatalf("client was not set")
	}
	if have := tx.GetClient().GetName(); have != clientName {
		t.Errorf("incorrect client name, have=%q want=%q", have, clientName)
	}

	// Make an event sent from the service at a non-root path as a
	// client of some other thing. This shouldn't overwrite the existing client.
	serviceSubcall := &events.Event{
		Kind:          clientKind,
		Svcname:       serviceName,
		Path:          subPath,
		TransactionId: txid,
		Time:          1,
	}
	addEvent(tx, serviceSubcall)

	if have := tx.GetClient().GetName(); have != clientName {
		t.Errorf("client name overwritten by subcall, have=%q want=%q", have, clientName)
	}

}
