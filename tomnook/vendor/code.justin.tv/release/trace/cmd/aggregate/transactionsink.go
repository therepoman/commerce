package main

import (
	"bufio"
	"log"
	"os"

	"code.justin.tv/ear/kinesis/combiner"
)

// number of Transactions to hold in write buffers
var writeBuffer = 1024

// transactionSink is an interface to represent a destination for
// computed pbmsg.Transactions.
type transactionSink interface {
	consume(<-chan *combiner.Blob)
}

type fileTransactionSink struct {
	filename string
	workers  int
}

func newFileTransactionSink(filename string, workers int) transactionSink {
	return &fileTransactionSink{
		filename: filename,
		workers:  workers,
	}
}

func (ts *fileTransactionSink) consume(blobs <-chan *combiner.Blob) {
	handle, err := os.Create(ts.filename)
	if err != nil {
		log.Fatalf("sink file open err=%q", err)
	}
	defer handle.Close()

	buffered := bufio.NewWriter(handle)
	defer buffered.Flush()

	for b := range blobs {
		written, err := buffered.Write(b.Data)
		if err != nil {
			log.Fatalf("write err=%q", err)
		}
		if written != len(b.Data) {
			log.Fatalf("transaction write: short write have=%d want=%d", written, len(b.Data))
		}
	}
}
