package main

import (
	"io"
	"log"
	"net"
	"sort"
	"time"

	"github.com/spenczar/tdigest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"code.justin.tv/release/trace/pbmsg"
)

const tdigestCompression = 50

type timings struct {
	Count   int
	Network tdigest.TDigest
	Service tdigest.TDigest
	Client  tdigest.TDigest
}

func newTimings() *timings {
	return &timings{
		0,
		tdigest.New(tdigestCompression),
		tdigest.New(tdigestCompression),
		tdigest.New(tdigestCompression),
	}
}

type stringSet map[string]struct{}

func (ss stringSet) add(s string) {
	ss[s] = struct{}{}
}

func (ss stringSet) All() []string {
	out := make([]string, 0)
	for s := range ss {
		if s != "" {
			out = append(out, s)
		}
	}
	sort.Strings(out)
	return out
}

type counter struct {
	Txs   *timings
	Calls *timings
	Roots *timings

	Services    stringSet
	Datacenters stringSet
}

func newCounter() *counter {
	return &counter{
		Txs:         newTimings(),
		Calls:       newTimings(),
		Roots:       newTimings(),
		Services:    make(stringSet),
		Datacenters: make(stringSet),
	}
}

func (c *counter) addCall(call callWithClient) {
	c.Calls.Count += 1
	c.Calls.Client.Add(float64(pbmsg.ClientDuration(call.call)), 1)
	c.Calls.Service.Add(float64(pbmsg.ServiceDuration(call.call)), 1)
	c.Calls.Network.Add(float64(pbmsg.NetworkDuration(call.call)), 1)

	c.Services.add(call.call.GetSvc().GetName())
	host := call.call.GetSvc().GetHost()
	if host == "" {
		host, _, _ = net.SplitHostPort(call.call.GetRequestSentTo())
	}
	if host != "" {
		c.Datacenters.add(datacenterOfHost(host))
	}
}

func (c *counter) addTxTimings(client, service, network time.Duration) {
	c.Txs.Count += 1
	c.Txs.Client.Add(float64(client), 1)
	c.Txs.Service.Add(float64(service), 1)
	c.Txs.Network.Add(float64(network), 1)
}

func (c *counter) addRootTimings(client, service, network time.Duration) {
	c.Roots.Count += 1
	c.Roots.Client.Add(float64(client), 1)
	c.Roots.Service.Add(float64(service), 1)
	c.Roots.Network.Add(float64(network), 1)
}

type results struct {
	SameDC  *counter
	CrossDC *counter
	Total   *counter
}

func newResults() *results {
	return &results{
		newCounter(),
		newCounter(),
		newCounter(),
	}
}

func (r *results) addTx(tx *pbmsg.Transaction) {
	isCrossDatacenterTx := false
	var txClientTime, txServiceTime, txNetworkTime time.Duration

	for _, call := range calls(tx) {
		r.Total.addCall(call)
		if isCrossDatacenter(call) {
			r.CrossDC.addCall(call)
			isCrossDatacenterTx = true
		} else {
			r.SameDC.addCall(call)
		}

		txClientTime += pbmsg.ClientDuration(call.call)
		txServiceTime += pbmsg.ServiceDuration(call.call)
		txNetworkTime += pbmsg.NetworkDuration(call.call)
	}

	rootClientTime := pbmsg.ClientDuration(tx.GetRoot())
	rootServiceTime := pbmsg.ServiceDuration(tx.GetRoot())
	rootNetworkTime := pbmsg.NetworkDuration(tx.GetRoot())

	r.Total.addTxTimings(txClientTime, txServiceTime, txNetworkTime)
	r.Total.addRootTimings(rootClientTime, rootServiceTime, rootNetworkTime)
	if isCrossDatacenterTx {
		r.CrossDC.addTxTimings(txClientTime, txServiceTime, txNetworkTime)
		r.CrossDC.addRootTimings(rootClientTime, rootServiceTime, rootNetworkTime)
	} else {
		r.SameDC.addTxTimings(txClientTime, txServiceTime, txNetworkTime)
		r.SameDC.addRootTimings(rootClientTime, rootServiceTime, rootNetworkTime)
	}
}

func consumeFirehose(f pbmsg.Trace_FirehoseClient) *results {
	r := newResults()
loop:
	for {
		tx, err := f.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			switch grpc.Code(err) {
			case codes.Canceled:
				break loop
			case codes.DeadlineExceeded:
				break loop
			default:
				log.Fatal(err.Error())
			}
		}
		r.addTx(tx)
	}
	return r
}
