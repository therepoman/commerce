package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"code.justin.tv/common/chitin"
	_ "code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/release/trace/pbmsg"
	"code.justin.tv/release/trace/scanproto"
	"github.com/golang/protobuf/proto"
)

func main() {
	log.SetFlags(0)

	flag.Parse()

	err := chitin.ExperimentalTraceProcessOptIn()
	if err != nil {
		log.Fatalf("trace enable err=%q", err)
	}

	sc := scanproto.NewEventSetScanner(os.Stdin)
	for sc.Scan() {
		var es pbmsg.EventSet
		err := proto.Unmarshal(sc.Bytes(), &es)
		if err != nil {
			log.Fatalf("%s", err)
		}

		for _, ev := range es.Event {
			str := proto.CompactTextString(ev)
			fmt.Fprintf(os.Stdout, "%s\n", str)
		}
	}

	err = sc.Err()
	if err != nil {
		log.Fatalf("%s", err)
	}
}
