/*

Command txreport summarizes a large number of transactions into a single
report for human analysis.

	Usage of txreport:
	  -api
	    	Use the Trace API instead of kinesis (allows you to run txreport outside the VPC)
	  -cpuprofile string
	    	Write a CPU profile to the specified file before exiting
	  -db string
	    	Output LevelDB directory name
	  -flush duration
	    	time between flushing transactions into a report (default 30s)
	  -hosts string
	    	Comma delimited list of hosts to filter to
	  -kafka string
	    	Unused. Reserved for backwards compatibility.
	  -kinesis
	    	Use Kinesis as the transaction source (default true)
	  -outdir string
	    	Output directory name
	  -server
	    	Runs an HTTP server at the end of the run for easy local use
	  -services string
	    	Comma delimited list of services to filter to
	  -topic string
	    	kinesis topic providing transactions (default "trace-transactions")
	  -urlprefix string
	    	Prefix for all internal URLs in reports (default "/")

With the -db flag, the command will generate a leveldb database containing the
data of the report. This includes any representative example transactions, and
the statistics calculated for the population. The format of the database is
described in the code.justin.tv/release/trace/persistent package docs.

*/
package main
