package main

import (
	"sort"

	"code.justin.tv/release/trace/analysis"
	"code.justin.tv/release/trace/analysis/tx"
)

func vega(txs tx.TransactionSet) interface{} {
	timestamps := analysis.ExtractTimestamps(txs)
	sort.Sort(sortDuration(timestamps))
	buckets := analysis.Bucketize(timestamps)

	type vegadata struct {
		Name   string        `json:"name"`
		Values []interface{} `json:"values"`
	}
	type spec struct {
		Width   int `json:"width"`
		Height  int `json:"height"`
		Padding struct {
			Top    int `json:"top"`
			Bottom int `json:"bottom"`
			Left   int `json:"left"`
			Right  int `json:"right"`
		} `json:"padding"`
		Data   []vegadata               `json:"data"`
		Scales []map[string]interface{} `json:"scales"`
		Axes   []map[string]interface{} `json:"axes"`
		Marks  []map[string]interface{} `json:"marks"`
	}

	var body spec
	body.Width = 800
	body.Height = 500
	body.Padding.Top = 10
	body.Padding.Bottom = 20
	body.Padding.Left = 60
	body.Padding.Right = 10
	body.Scales = []map[string]interface{}{
		{
			"name": "x", "type": "log", "range": "width", "nice": true,
			"domain": map[string]string{"data": "table", "field": "data.x"},
		},
		{
			"name": "y", "type": "log", "range": "height", "nice": true,
			"domain":    map[string]string{"data": "table", "field": "data.y"},
			"domainMin": 0.1,
		},
	}
	body.Axes = []map[string]interface{}{
		{"type": "x", "scale": "x"},
		{"type": "y", "scale": "y"},
	}
	body.Marks = []map[string]interface{}{
		{
			"type": "rect",
			"from": map[string]string{"data": "table"},
			"properties": map[string]interface{}{
				"enter": map[string]interface{}{
					"x":  map[string]interface{}{"scale": "x", "field": "data.x"},
					"x2": map[string]interface{}{"scale": "x", "field": "data.x2"},
					"y":  map[string]interface{}{"scale": "y", "field": "data.y"},
					"y2": map[string]interface{}{"scale": "y", "value": 0.1},
				},
				"update": map[string]interface{}{
					"fill": map[string]interface{}{"value": "steelblue"},
				},
				"hover": map[string]interface{}{
					"fill": map[string]interface{}{"value": "red"},
				},
			},
		},
	}

	data := []interface{}{}

	for _, bucket := range buckets {
		type xy struct {
			X  int `json:"x"`
			X2 int `json:"x2"`
			Y  int `json:"y"`
		}
		data = append(data, xy{
			X:  int(bucket.Start.Nanoseconds()),
			X2: int(bucket.End.Nanoseconds()),
			Y:  bucket.Count,
		})
	}

	body.Data = []vegadata{
		{
			Name:   "table",
			Values: data,
		},
	}

	return body
}
