package main

import (
	"reflect"
	"testing"

	"github.com/golang/protobuf/proto"

	"code.justin.tv/release/trace/analysis/routes"
	"code.justin.tv/release/trace/api"
	"code.justin.tv/release/trace/common"
)

func TestClean(t *testing.T) {
	defer func(orig map[string]routes.RouteMap) {
		routes.Routes = orig
	}(routes.Routes)
	testRoutes := routes.NewRouteMap()
	testRoutes.AddRoute("/debug/pprof/heap", "GET", "pprof-heap")
	testRoutes.AddRoute("/debug/pprof/profile", "GET", "pprof-profile")
	routes.Routes = map[string]routes.RouteMap{"test": testRoutes}

	tests := []struct {
		input *api.Transaction
		after *api.Transaction
	}{
		{
			input: &api.Transaction{Root: &api.Call{
				Svc: &api.Service{Name: "test"},
				Params: &api.RPCParams{
					Http: &api.HTTPParams{Method: common.Method_GET, UriPath: "/debug/pprof/heap", Status: 200},
				},
			}},
			after: &api.Transaction{Root: &api.Call{
				Svc: &api.Service{Name: "test"},
				Params: &api.RPCParams{
					Http: &api.HTTPParams{Method: common.Method_GET, Status: 200, Route: "pprof-heap"},
				},
			}},
		},
	}

	for _, tt := range tests {
		orig := proto.CompactTextString(tt.input)
		if have, want := cleanedAPITransaction(tt.input), tt.after; !reflect.DeepEqual(have, want) {
			t.Errorf(`cleanedAPITransaction(
	%s
);
	%s
!=
	%s`,
				orig,
				proto.CompactTextString(have),
				proto.CompactTextString(want))
		}

		if have, want := proto.CompactTextString(tt.input), orig; have != want {
			t.Errorf(`after cleanedAPITransaction
	%s
!=
	%s`, have, want)
		}
	}
}
