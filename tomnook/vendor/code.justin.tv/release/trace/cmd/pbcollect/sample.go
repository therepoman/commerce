package main

import "math"

// Emergency pressure release
// var defaultSample = keepFraction(sampleRate)
var defaultSample = keepAll

func keepAll([2]uint64) bool {
	return true
}

func keepFraction(fraction float64) func([2]uint64) bool {
	var max uint64 = math.MaxUint64
	limit := uint64(float64(max) * fraction)
	if fraction <= 0 {
		return func(_ [2]uint64) bool { return false }
	} else if fraction >= 1 {
		limit = max
	}
	return func(txid [2]uint64) bool {
		return txid[0] <= limit
	}
}
