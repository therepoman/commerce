package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"code.justin.tv/common/chitin"
	_ "code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/release/trace/internal/pipeline"
	"code.justin.tv/release/trace/internal/sink"
	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/net/context"
)

const (
	defaultAddr        = ":11143"
	defaultHTTPAddr    = ":11144"
	dataRotateInterval = 5 * time.Minute
	// Number of messages to buffer internally between decoding from TCP and
	// writing to disk.  16k messages of 100-200 bytes each means about 3MB of
	// memory spent on the buffer.  We can afford that, and it protects us
	// from a slow file rotation.
	dataQueue = 16 << 10

	// Number of Kinesis Producers to start up for sending data to Kinesis.
	kinesisWorkers = 16
	// Region of the Kinesis stream to connect to.
	kinesisRegion = "us-west-2"

	// Number of shards to slice incoming events into. This should be greater
	// or equal to the number of underlying shards in kinesis. But too much
	// will decrease performance.
	virtualShards = 1000

	statsdAddr          = "statsd.internal.justin.tv:8125"
	statsdPrefix        = "trace.pbcollect"
	statsdFlushInterval = time.Second * 5
	statsdFlushBytes    = 512

	connectionDrainTime  = 5*time.Minute + 30*time.Second
	shutdownPollInterval = time.Second
)

var (
	exp   expdata
	stats statsd.Statter
)

func init() {
	stats, _ = statsd.NewNoopClient()
}

// pbcollect's graceful shutdown is initiated when its parent process
// has died. See cmd/runpbcollect/main.go for a full explanation.
func pollForShutdown(callback func()) {
	t := time.NewTicker(shutdownPollInterval)
	for range t.C {
		if syscall.Getppid() == 1 {
			callback()
			return
		}
	}
}

type server struct {
	addr      string
	debugAddr string

	sock      net.Listener
	debugSock net.Listener

	// fatal error, exit immediately
	quit chan error
}

func (s *server) listenAll() error {
	sock, err := net.Listen("tcp", s.addr)
	if err != nil {
		return err
	}
	s.sock = sock

	debugSock, err := net.Listen("tcp", s.debugAddr)
	if err != nil {
		return err
	}
	s.debugSock = debugSock

	return nil
}

func (s *server) serve(data chan<- []byte) {
	tcpSock := s.sock.(*net.TCPListener)

	var tempDelay time.Duration
	for {
		c, err := tcpSock.AcceptTCP()
		atomic.AddInt64(&exp.Accept, 1)
		if err != nil {
			atomic.AddInt64(&exp.AcceptError, 1)
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				backoff(&tempDelay, 5*time.Millisecond, 1*time.Second)
				continue
			}
			time.Sleep(1 * time.Second)
			continue
		}
		tempDelay = 0

		c.SetKeepAlive(true)
		c.SetKeepAlivePeriod(3 * time.Minute)
		go s.serveConn(c, data)
	}
}

func (s *server) debugServe(ctx context.Context) {
	httpSock := s.debugSock

	srv := &http.Server{
		Handler: chitin.Handler(http.DefaultServeMux, chitin.SetBaseContext(ctx)),
	}

	if err := srv.Serve(httpSock); err != nil {
		log.Printf("debugServe err=%q", err)
	}
	return
}

func (s *server) serveConn(c net.Conn, data chan<- []byte) {
	sc := newScanner(c)
	for sc.Scan() {
		atomic.AddInt64(&exp.Scan, 1)
		// Make a copy of the bytes, since sc.Bytes is only valid until the
		// next call to sc.Scan.
		out := append([]byte(nil), sc.Bytes()...)
		atomic.AddInt64(&exp.ScanByte, int64(len(out)))
		data <- out
	}

	err := sc.Err()
	if err != nil {
		log.Printf("tcp scan err=%q", err)
		atomic.AddInt64(&exp.ScanError, 1)
	}
	err = c.Close()
	if err != nil {
		log.Printf("tcp close err=%q", err)
		atomic.AddInt64(&exp.TCPCloseError, 1)
	}
}

func (s *server) flushSinks(cancel func(), done <-chan struct{}, delay time.Duration) {
	// Close sinks
	cancel()
	// wait until they have flushed
	select {
	case <-done:
		s.quit <- nil
	case <-time.After(delay):
		s.quit <- fmt.Errorf("exiting before flush")
	}

}

func (s *server) shutdown(cancel func(), done <-chan struct{}, delay time.Duration) {
	// Don't think we need a lock here - closing the sockets and sinks
	// can be done multiple times safely.
	log.Println("graceful shutdown: start")

	// stop listening, but leave active connections open
	s.sock.Close()
	s.debugSock.Close()
	log.Println("graceful shutdown: listeners closed")

	time.Sleep(connectionDrainTime)
	log.Println("graceful shutdown: connections drained")

	s.flushSinks(cancel, done, delay)
	log.Println("graceful shutdown: sinks flushed")
}

func onSignal(s os.Signal, f func()) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, s)
	<-c
	f()
}

func backoff(tempDelay *time.Duration, start, max time.Duration) {
	if *tempDelay == 0 {
		*tempDelay = start
	} else {
		*tempDelay *= 2
	}
	if *tempDelay > max {
		*tempDelay = max
	}
	time.Sleep(*tempDelay)
}

func main() {
	var addr string
	flag.StringVar(&addr, "addr", defaultAddr, "Listen address")
	var httpAddr string
	flag.StringVar(&httpAddr, "http", defaultHTTPAddr, "HTTP listen address")

	var filename string
	flag.StringVar(&filename, "file", "", "Output file to write to")

	var kinesisStream string
	flag.StringVar(&kinesisStream, "kinesis-stream", "trace-events", "Kinesis stream to publish events to")

	var dev bool
	flag.BoolVar(&dev, "dev", false, "If true, then don't register self in Consul")

	var unused string
	flag.StringVar(&unused, "kafka", "", "Unused. Reserved for backwards compatibility.")
	flag.StringVar(&unused, "kafka-topic", "", "Unused. Reserved for backwards compatibility.")

	flag.Parse()

	err := chitin.ExperimentalTraceProcessOptIn()
	if err != nil {
		log.Fatalf("trace enable err=%q", err)
	}

	// Initialize global statsd client singleton
	stats, err = statsd.NewBufferedClient(statsdAddr, statsdPrefix, statsdFlushInterval, statsdFlushBytes)
	if err != nil {
		log.Printf("unable to create statsd client err=%q", err)
		stats, _ = statsd.NewNoopClient()
	}

	s := &server{
		addr:      addr,
		debugAddr: httpAddr,

		quit: make(chan error, 1),
	}

	log.Println("setting context")
	ctx, err := chitin.ExperimentalTraceContext(context.Background())
	if err != nil {
		log.Fatalf("chitin trace-context err=%q", err)
	}

	background := ctx
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	data := make(chan []byte, dataQueue)

	sinks := make([]sink.Sink, 0)
	if filename != "" {
		log.Println("creating file sink")
		s, err := newFileSink(ctx, filename)
		if err != nil {
			log.Fatalf("error creating file sink=%q", err)
		}
		sinks = append(sinks, s)
	}
	if kinesisStream != "" {
		log.Println("creating kinesis sink")

		cfg := sink.NewKinesisConfig(kinesisStream)
		cfg.OnError = pipeline.LogErrors(cfg.Logger)

		stats, err = statsd.NewBufferedClient(statsdAddr, statsdPrefix+".kinesis", statsdFlushInterval, statsdFlushBytes)
		if err == nil {
			cfg.Stats = stats
		}

		s, err := sink.Kinesis(ctx, cfg)
		if err != nil {
			log.Fatalf("error creating kinesis sink=%q", err)
		}
		sinks = append(sinks, s)
	}
	if len(sinks) == 0 {
		log.Fatalf("no sinks configured")
	}

	log.Println("listening")
	err = s.listenAll()
	if err != nil {
		log.Fatalf("listen err=%q", err)
	}

	// TODO: remove consul code, we use DNS now
	if !dev {
		log.Println("registering in consul")
		err = s.registerConsul(ctx)
		if err != nil {
			log.Fatalf("consul err=%q", err)
		}
		go s.handleDeregisterConsul(background, ctx.Done())
	}

	log.Println("decoding inbound messages")
	msgs := concurrentDecode(data)

	flushed := make(chan struct{})
	go func() {
		log.Println("routing data to sinks")
		sink.Broadcast(msgs, sinks...)
		close(flushed)
	}()

	log.Println("starting service")
	go s.debugServe(ctx)
	go s.serve(data)

	// https://git-aws.internal.justin.tv/common/chitin/blob/master/internal/signal/README
	log.Println("listening for signals")
	// Dangerously quick shutdown
	go onSignal(syscall.SIGQUIT, func() { s.flushSinks(cancel, flushed, 0) })
	// Quick shutdown
	go onSignal(syscall.SIGTERM, func() { s.flushSinks(cancel, flushed, 1*time.Second) })
	// Graceful shutdown
	go onSignal(syscall.SIGINT, func() { s.shutdown(cancel, flushed, 5*time.Second) })
	go onSignal(syscall.SIGUSR2, func() { s.shutdown(cancel, flushed, 5*time.Second) })

	log.Println("polling ppid to check for graceful shutdown if parent process dies")
	go pollForShutdown(func() { s.shutdown(cancel, flushed, 1*time.Second) })

	err = <-s.quit
	if err != nil {
		log.Fatalf("dirty exit err=%q", err)
	} else {
		log.Printf("clean exit")
	}
}
