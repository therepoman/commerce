package main

import (
	"expvar"
	"sync/atomic"

	"code.justin.tv/common/golibs/pkgpath"
)

func init() {
	pkg, _ := pkgpath.Caller(0)
	expvar.Publish(pkg, expvar.Func(func() interface{} { return exp.dup() }))
}

type expdata struct {
	Accept        int64
	AcceptError   int64
	Scan          int64
	ScanByte      int64
	ScanError     int64
	TCPCloseError int64

	FileOpenError  int64
	FileFlushError int64
	FileCloseError int64
	FileWriteError int64
}

func (ed *expdata) dup() *expdata {
	var out expdata

	out.Accept = atomic.LoadInt64(&ed.Accept)
	out.AcceptError = atomic.LoadInt64(&ed.AcceptError)
	out.Scan = atomic.LoadInt64(&ed.Scan)
	out.ScanByte = atomic.LoadInt64(&ed.ScanByte)
	out.ScanError = atomic.LoadInt64(&ed.ScanError)
	out.TCPCloseError = atomic.LoadInt64(&ed.TCPCloseError)

	out.FileOpenError = atomic.LoadInt64(&ed.FileOpenError)
	out.FileFlushError = atomic.LoadInt64(&ed.FileFlushError)
	out.FileCloseError = atomic.LoadInt64(&ed.FileCloseError)
	out.FileWriteError = atomic.LoadInt64(&ed.FileWriteError)

	return &out
}
