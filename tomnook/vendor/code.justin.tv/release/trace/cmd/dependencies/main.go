package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"code.justin.tv/common/chitin"
	_ "code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/release/trace/pbmsg"
	"golang.org/x/net/context"
)

func main() {
	timeLimit := flag.Duration("t", 0, "Exit after reading for this long. "+
		"Default is 0, which will read until SIGINT is received.")
	flag.Parse()

	err := chitin.ExperimentalTraceProcessOptIn()
	if err != nil {
		log.Fatalf("trace enable err=%q", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	if *timeLimit != 0 {
		log.Printf("Timing out after %s", *timeLimit)
		ctx, cancel = context.WithTimeout(ctx, *timeLimit)
	}
	defer cancel()

	log.Println("connecting")
	client, err := pbmsg.Firehose(ctx, 1.0)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("connected to api")

	var count int
	defer func() {
		// wrapped in a closure so that count gets evaluated when the deferred
		// function gets called, not when it gets set up
		log.Printf("exiting, processed %d transactions", count)
	}()

	sigch := make(chan os.Signal, 1)
	signal.Notify(sigch, syscall.SIGINT)
	go func() {
		<-sigch
		cancel()
	}()

	g := make(graph)
	err = pbmsg.ConsumeFirehose(client, func(tx *pbmsg.Transaction) {
		g.addCall(tx.GetClient().GetName(), tx.GetRoot())
		count += 1
	})
	if err != nil {
		log.Fatalf("grpc error: %s", err)
	}

	<-ctx.Done()
	fmt.Println(g.String())
}
