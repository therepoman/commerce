# dependencies #

The dependencies command generates a [DOT format](http://www.graphviz.org/doc/info/lang.html) graph definition for the dependencies between services. It prints the DOT graph to stdout. Use the `dot` command line program which comes with graphviz (`brew install graphviz`) to make sense of the output:

```
➜  trace git:(dependencies) ✗ ./dependencies -t 10s > deps.dot
2015/12/09 16:34:02 Timing out after 10s
2015/12/09 16:34:02 connecting
2015/12/09 16:34:02 firehose: connect
2015/12/09 16:34:02 dialing
2015/12/09 16:34:02 connection established
2015/12/09 16:34:02 firehosing
2015/12/09 16:34:02 connected to api
2015/12/09 16:34:12 exiting, processed 6103 transactions
➜  trace git:(dependencies) ✗ dot -Tsvg deps.dot -o deps.svg && open deps.svg
# look at the beautiful svg in a browser
```

The `.dot` graph produced includes weight attributes for each edge. These are the number of calls for which the dependency was observed.
