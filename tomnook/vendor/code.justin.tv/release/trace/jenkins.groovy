job {
	name 'release-trace'
	using 'TEMPLATE-autobuild'
	scm {
		git {
			remote {
				github 'release/trace', 'ssh', 'git-aws.internal.justin.tv'
				credentials 'git-aws-read-key'
			}
			clean true
		}
	}
	steps {
		shell 'rm -rf .manta/'
		shell 'manta -v'
		saveDeployArtifact 'release/trace', '.manta'
	}
}

job {
	name 'release-trace-deploy'
	using 'TEMPLATE-deploy'
	steps {
		shell 'echo "HOSTS=${HOSTS}" ; hosts="" ; for host in $(echo "${HOSTS}" | tr "," " ") ; do if ! host "$host" &>/dev/null; then consulname="$(echo "${host}" | sed -n -e "s/^\\(.*\\)\\.\\([^\\.]*\\)\\.justin\\.tv$/\\1/p")" ; consuldc="$(echo "${host}" | sed -n -e "s/^\\(.*\\)\\.\\([^\\.]*\\)\\.justin\\.tv$/\\2/p")" ; host="$(curl -o - -s "http://consul.internal.justin.tv/v1/catalog/node/${consulname}?dc=${consuldc}" | python -mjson.tool | sed -n -e "s/.*\\"Address\\":[ ]*\\"\\([0-9][0-9\\.]*\\)\\".*/\\1/p" | sed 1q)" ; fi ; hosts="${hosts:+$hosts,}${host}" ; done ; echo "HOSTS=${hosts}" ; export HOSTS="${hosts}" ; courier deploy --repo release/trace --dir /opt/twitch/trace'
	}
}
