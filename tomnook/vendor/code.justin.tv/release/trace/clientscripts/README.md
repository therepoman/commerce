# Client Scripts

## What they are

This directory contains code for tools that consume Trace data. The code does
not run as part of the production Trace data processing pipeline.

## Why they're here

The tools here are a valuable part of the Trace ecosystem and depend on
several of Trace's packages, which is why they're included in this repository.

## Why they look like they do

Since they're effectively scripts, usually written for the purpose of
debugging a single production incident, they're subject to lower standards for
code review and test coverage.

## What they need to do

The code here must continue to compile on linux/amd64 and darwin/amd64. They
must compile in bulk with "go build ./...", with one package per directory. As
Trace's APIs evolve, their proximity to the rest of the code should make it
easy to update them without breakages. If you're changing a package they
depend on, you're responsible for these tools continuing to compile. They also
need to pass all automated checks in the repo, such as complying with gofmt
and passing go vet.

As a last resort, it's possible to add the "ignore" build tag to files that
are particularly problematic. This should be a rare occurrence.
