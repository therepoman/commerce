package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"code.justin.tv/common/chitin"
	_ "code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/release/trace/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var (
	serverHostPort = flag.String("server", "trace-api.prod.us-west2.justin.tv:11143", "The server hostport")
	duration       = flag.Duration("t", time.Second, "How long to capture data before exiting")
)

func main() {
	flag.Parse()

	err := chitin.ExperimentalTraceProcessOptIn()
	if err != nil {
		log.Fatalf("trace enable err=%q", err)
	}

	conn, err := grpc.Dial(*serverHostPort, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("dial err=%q", err)
	}
	defer conn.Close()
	client := api.NewTraceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), *duration)

	sigch := make(chan os.Signal, 1)
	signal.Notify(sigch, syscall.SIGINT)
	go func() {
		<-sigch
		cancel()
	}()

	go keepAlive(ctx, client)

	err = listProcs(ctx, os.Stdout, client)
	if err != nil {
		log.Fatalf("firehose err=%q", err)
	}
}

// keepAlive keeps a connection alive by occasionally issuing a bogus query
func keepAlive(ctx context.Context, client api.TraceClient) {
	for {
		time.Sleep(15 * time.Second)
		c, cancel := context.WithCancel(ctx)
		_, err := client.Firehose(c, &api.FirehoseRequest{
			Sampling: 1e-9,
			Query: &api.Query{
				Comparisons: []*api.Comparison{
					&api.Comparison{
						ServicePid: &api.IntegerComparison{
							Type:  api.IntegerComparison_EQUALS,
							Value: -2,
						},
					},
				},
			},
		})
		cancel()
		if err != nil {
			continue
		}
	}
}

func listProcs(ctx context.Context, w io.Writer, client api.TraceClient) error {
	type proc struct {
		svcname  string
		hostname string
		pid      int32
	}
	seen := make(map[proc]struct{})

	txs := make(chan *api.Transaction)
	errs := make(chan error)

	var filters []*api.Comparison

	read := func(txs chan *api.Transaction, errs chan error, firehose api.Trace_FirehoseClient) {
		for {
			tx, err := firehose.Recv()
			if err != nil {
				select {
				case <-firehose.Context().Done():
				case errs <- err:
				}
				return
			}
			select {
			case <-firehose.Context().Done():
			case txs <- tx:
			}
		}
	}

	ctxA, cancelA := context.WithCancel(ctx)
	for {
		cancelA()
		ctxA, cancelA = context.WithCancel(ctx)
		firehoseA, err := client.Firehose(ctxA, &api.FirehoseRequest{
			Query: &api.Query{
				Comparisons: filters,
			},
		})
		if err != nil {
			cancelA()
			return err
		}
		go read(txs, errs, firehoseA)

		for ctxA.Err() == nil {
			select {
			case <-ctxA.Done():
			case <-ctx.Done():
				cancelA()
				return nil
			case err := <-errs:
				if err != context.Canceled && grpc.Code(err) != codes.Canceled {
					cancelA()
					return err
				}
			case tx := <-txs:
				record := func(svc *api.Service) {
					if svc == nil {
						return
					}
					p := proc{
						svcname:  svc.Name,
						hostname: svc.Host,
						pid:      svc.Pid,
					}
					if _, ok := seen[p]; !ok {
						seen[p] = struct{}{}
						filters = append(filters, &api.Comparison{
							Not:         true,
							ServiceName: &api.StringComparison{Value: svc.Name},
							ServiceHost: &api.StringComparison{Value: svc.Host},
							ServicePid:  &api.IntegerComparison{Type: api.IntegerComparison_EQUALS, Value: int64(svc.Pid)},
						})
						cancelA()
						fmt.Fprintf(w, "%q %q %d\n", svc.Name, svc.Host, svc.Pid)
					}
				}

				var search func(*api.Call)
				search = func(c *api.Call) {
					if c == nil {
						return
					}
					record(c.Svc)
					for _, sub := range c.Subcalls {
						search(sub)
					}
				}

				record(tx.GetClient())
				search(tx.GetRoot())
			}
		}
	}
}
