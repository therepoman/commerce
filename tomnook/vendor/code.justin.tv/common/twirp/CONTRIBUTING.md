# Contributing #

## Requirements ##

 * **retool**: You'll need a working copy of `retool` (`go get code.justin.tv/common/retool`) We try to always use the latest version.
 * **protoc**: Download a precompiled version for your platform from https://github.com/google/protobuf/releases. We currently use v3.2.0 for development and testing.

## Tests ##

To run tests, use `make test`. This will install protoc-gen-twirp, run it to re-generate code, and then run
tests against the generated files.

## Contributing Code ##

Twirp uses githun pull requests. Hack away at your changes, run the test suite with `make test`, and submit a PR.

## Releasing Versions ##

Notes for the core maintainers, @spencer and @marioizquierdo. Releasing versions
is their responsibility.

Twirp uses [Semantic versioning](http://semver.org/): `v<major>.<minor>.<patch>`.

 * Increment major if you're making a backwards-incompatible change.
 * Increment minor if you're adding a feature that's backwards-compatible.
 * Increment patch if you're making a bugfix.

To make a release, remember to update the version number in
[protoc-gen-twirp/version.go](./protoc-gen-twirp/version.go).

Twirp uses Github releases. To make a new release:
 1. Make a PR and get the changes approved. Include the version change in the PR (if you want multiple PRs on the same version, make a version branch and merge all those PRs on that branch).
 2. When your branch is merged in master, make a git tag: `git tag <version number>` and `git push origin --tags`
    on the merge commit to annotate the release.
 3. Go to Github https://git-aws.internal.justin.tv/common/twirp/releases and "Draft a new release".
 4. Make sure to document changes, specially when upgrade instructions are needed.
