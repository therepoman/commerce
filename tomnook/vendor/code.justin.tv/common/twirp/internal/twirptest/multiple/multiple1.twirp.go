// Code generated by protoc-gen-twirp v4.6.0, DO NOT EDIT.
// source: multiple1.proto

/*
Package multiple is a generated twirp stub package.
This code was generated with code.justin.tv/common/twirp/protoc-gen-twirp v4.6.0.

test to make sure that multiple proto files in one package works

It is generated from these files:
	multiple1.proto
	multiple2.proto
*/
package multiple

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import bytes "bytes"
import strconv "strconv"
import context "golang.org/x/net/context"
import http "net/http"
import ioutil "io/ioutil"
import jsonpb "github.com/golang/protobuf/jsonpb"
import log "log"
import proto1 "github.com/golang/protobuf/proto"
import twirp "code.justin.tv/common/twirp"
import fmt1 "fmt"

// Imports only used by utility functions:
import io "io"
import strings "strings"
import json "encoding/json"
import url "net/url"
import ctxhttp "golang.org/x/net/context/ctxhttp"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// ==============
// Svc1 Interface
// ==============

type Svc1 interface {
	Send(context.Context, *Msg1) (*Msg1, error)
}

// ====================
// Svc1 Protobuf Client
// ====================

type svc1ProtobufClient struct {
	urlBase string
	client  *http.Client
}

// NewSvc1ProtobufClient creates a Protobuf client that implements the Svc1 interface.
// It communicates using protobuf messages and can be configured with a custom http.Client.
func NewSvc1ProtobufClient(addr string, client *http.Client) Svc1 {
	return &svc1ProtobufClient{
		urlBase: urlBase(addr),
		client:  withoutRedirects(client),
	}
}

func (c *svc1ProtobufClient) Send(ctx context.Context, in *Msg1) (*Msg1, error) {
	url := c.urlBase + Svc1PathPrefix + "Send"
	out := new(Msg1)
	err := doProtoRequest(ctx, c.client, url, in, out)
	return out, err
}

// ================
// Svc1 JSON Client
// ================

type svc1JSONClient struct {
	urlBase string
	client  *http.Client
}

// NewSvc1JSONClient creates a JSON client that implements the Svc1 interface.
// It communicates using JSON requests and responses instead of protobuf messages.
func NewSvc1JSONClient(addr string, client *http.Client) Svc1 {
	return &svc1JSONClient{
		urlBase: urlBase(addr),
		client:  withoutRedirects(client),
	}
}

func (c *svc1JSONClient) Send(ctx context.Context, in *Msg1) (*Msg1, error) {
	url := c.urlBase + Svc1PathPrefix + "Send"
	out := new(Msg1)
	err := doJSONRequest(ctx, c.client, url, in, out)
	return out, err
}

// ===================
// Svc1 Server Handler
// ===================

type svc1Server struct {
	Svc1
	hooks     *twirp.ServerHooks
	ctxSource ContextSource
}

func NewSvc1Server(svc Svc1, hooks *twirp.ServerHooks, ctxSrc ContextSource) TwirpServer {
	defaultHooks := twirp.NewServerHooks()
	if hooks == nil {
		hooks = defaultHooks
	} else {
		if hooks.RequestReceived == nil {
			hooks.RequestReceived = defaultHooks.RequestReceived
		}
		if hooks.RequestRouted == nil {
			hooks.RequestRouted = defaultHooks.RequestRouted
		}
		if hooks.ResponsePrepared == nil {
			hooks.ResponsePrepared = defaultHooks.ResponsePrepared
		}
		if hooks.ResponseSent == nil {
			hooks.ResponseSent = defaultHooks.ResponseSent
		}
		if hooks.Error == nil {
			hooks.Error = defaultHooks.Error
		}
	}
	if ctxSrc == nil {
		ctxSrc = RequestContextSource
	}

	return &svc1Server{
		Svc1:      svc,
		hooks:     hooks,
		ctxSource: ctxSrc,
	}
}

// Svc1PathPrefix is used for all URL paths on a twirp Svc1 server.
// Requests are always: POST Svc1PathPrefix/method
// It can be used in an HTTP mux to route twirp requests along with non-twirp requests on other routes.
const Svc1PathPrefix = "/twirp/code.justin.tv.common.twirp.internal.twirptest.multiple.Svc1/"

// Svc1PathPrefixOld is used to handle requests from v2 Clients.
// If you are using an HTTP mux, please handle this routes as well to upgrade from v2 to v3.
const Svc1PathPrefixOld = "/v2/code.justin.tv.common.twirp.internal.twirptest.multiple.Svc1/"

func (s *svc1Server) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	ctx := s.ctxSource(resp, req)
	ctx = setServerName(ctx, "Svc1")
	ctx = setVersionFromPath(ctx, req.URL.Path)
	ctx = context.WithValue(ctx, "__tw_response_writer", resp) // for SetHTTPResponseHeader
	ctx = s.hooks.RequestReceived(ctx)

	if req.Method != "POST" {
		msg := fmt1.Sprintf("unsupported method %q (only POST is allowed)", req.Method)
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.serveError(ctx, resp, req, twerr)
		return
	}

	switch req.URL.Path {
	case Svc1PathPrefix + "Send", Svc1PathPrefixOld + "Send", "/v1/Svc1/Send":
		s.serveSend(ctx, resp, req)
		return
	default:
		msg := fmt1.Sprintf("no handler for path %q", req.URL.Path)
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.serveError(ctx, resp, req, twerr)
		return
	}
}

// Make HTTP response from an error.
// If err is not a twirp.Error, it will get wrapped with twirp.InternalErrorWith(err)
func (s *svc1Server) serveError(ctx context.Context, resp http.ResponseWriter, req *http.Request, err error) {
	// When handling v2 client requests, serve backwards-compatible error responses
	if version := getVersion(ctx); version == "v1" || version == "v2" {
		s.serveErrorV1V2(ctx, resp, req, err)
	} else {
		s.serveErrorV3Plus(ctx, resp, req, err)
	}
}

// v3+ error responses.
func (s *svc1Server) serveErrorV3Plus(ctx context.Context, resp http.ResponseWriter, req *http.Request, err error) {
	// Non-twirp errors are wrapped as Internal (default)
	twerr, ok := err.(twirp.Error)
	if !ok {
		twerr = twirp.InternalErrorWith(err)
	}

	statusCode := twirp.ServerHTTPStatusFromErrorCode(twerr.Code())
	ctx = setStatusCode(ctx, statusCode)
	ctx = s.hooks.Error(ctx, twerr)

	resp.Header().Set("Content-Type", "application/json") // Error responses are always JSON (instead of protobuf)
	resp.WriteHeader(statusCode)                          // HTTP response status code

	respBody := marshalErrorToJSON(twerr)
	_, err2 := resp.Write(respBody)
	if err2 != nil {
		log.Printf("unable to send error message %q: %s", twerr, err2)
	}
	_ = s.hooks.ResponseSent(ctx)
}

// v1/v2 backwards-compatible error responses.
// To make it easy to upgrade to v3, a v3 server keeps serving v2-style errors to v2 clients.
func (s *svc1Server) serveErrorV1V2(ctx context.Context, resp http.ResponseWriter, req *http.Request, err error) {
	// Non-twirp errors are wrapped as Unknown 500 (default)
	twerr, ok := err.(twirp.Error)
	if !ok {
		twerr = twirp.NewError(twirp.Unknown, err.Error())
	}

	resp.Header().Set("Content-Type", "text/plain")

	// v2err.Retryable <= v3err.Meta("retryable")
	if twerr.Meta("retryable") != "" {
		resp.Header().Set("Twirp-Error-Retryable", "true")
	}

	// v2err.ErrorID <= v3err.Meta("v2_error_id") if present, otherwise v3err.Code()
	// or "unroutable" if generated from twirp router (BadRoute)
	var v2ErrorID string
	if twerr.Code() == twirp.BadRoute {
		v2ErrorID = "unroutable" // as was generated by previous router
	} else if twerr.Meta("v2_error_id") != "" {
		v2ErrorID = twerr.Meta("v2_error_id")
	} else {
		v2ErrorID = string(twerr.Code()) // the string-format of v3 codes match v2 error ids for the most part: i.e. "not_found", "internal", "unknown", "canceled", etc.
	}
	resp.Header().Set("Twirp-ErrorID", v2ErrorID)

	// v2err.StatusCode <= v3err.Meta("v2_status_code") if present, otherwise same as in v3
	statusCode, err := strconv.Atoi(twerr.Meta("v2_status_code"))
	if err != nil {
		statusCode = twirp.ServerHTTPStatusFromErrorCode(twerr.Code()) // v3 it's the same as v2 for the most part: i.e. "not_found" is 404
	}
	ctx = setStatusCode(ctx, statusCode)
	ctx = s.hooks.Error(ctx, twerr)
	resp.WriteHeader(statusCode)

	msg := []byte(twerr.Msg())
	if len(msg) > 1e6 {
		msg = msg[:1e6]
	}
	_, err2 := resp.Write(msg)
	if err2 != nil {
		log.Printf("unable to send error message %q: %s", twerr, err2)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *svc1Server) serveSend(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	switch req.Header.Get("Content-Type") {
	case "application/json":
		s.serveSendJSON(ctx, resp, req)
	case "application/protobuf":
		s.serveSendProtobuf(ctx, resp, req)
	default:
		msg := fmt1.Sprintf("unexpected Content-Type: %q", req.Header.Get("Content-Type"))
		twerr := badRouteError(msg, req.Method, req.URL.Path)
		s.serveError(ctx, resp, req, twerr)
	}
}

func (s *svc1Server) serveSendJSON(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "Send")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	reqContent := new(Msg1)
	unmarshaler := jsonpb.Unmarshaler{AllowUnknownFields: true}
	if err = unmarshaler.Unmarshal(req.Body, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request json")
		s.serveError(ctx, resp, req, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *Msg1
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.serveError(ctx, resp, req, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.Send(ctx, reqContent)
	}()

	if err != nil {
		s.serveError(ctx, resp, req, err)
		return
	}
	if respContent == nil {
		s.serveError(ctx, resp, req, twirp.InternalError("received a nil *Msg1 and nil error while calling Send. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	var buf bytes.Buffer
	marshaler := &jsonpb.Marshaler{OrigName: true}
	if err = marshaler.Marshal(&buf, respContent); err != nil {
		err = wrapErr(err, "failed to marshal json response")
		s.serveError(ctx, resp, req, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(buf.Bytes()); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *svc1Server) serveSendProtobuf(ctx context.Context, resp http.ResponseWriter, req *http.Request) {
	var err error
	ctx = setMethodName(ctx, "Send")
	ctx = s.hooks.RequestRouted(ctx)

	defer closebody(req.Body)
	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		err = wrapErr(err, "failed to read request body")
		s.serveError(ctx, resp, req, twirp.InternalErrorWith(err))
		return
	}
	reqContent := new(Msg1)
	if err = proto1.Unmarshal(buf, reqContent); err != nil {
		err = wrapErr(err, "failed to parse request proto")
		s.serveError(ctx, resp, req, twirp.InternalErrorWith(err))
		return
	}

	// Call service method
	var respContent *Msg1
	func() {
		defer func() {
			// In case of a panic, serve a 500 error and then panic.
			if r := recover(); r != nil {
				s.serveError(ctx, resp, req, twirp.InternalError("Internal service panic"))
				panic(r)
			}
		}()
		respContent, err = s.Send(ctx, reqContent)
	}()

	if err != nil {
		s.serveError(ctx, resp, req, err)
		return
	}
	if respContent == nil {
		s.serveError(ctx, resp, req, twirp.InternalError("received a nil *Msg1 and nil error while calling Send. nil responses are not supported"))
		return
	}

	ctx = s.hooks.ResponsePrepared(ctx)

	respBytes, err := proto1.Marshal(respContent)
	if err != nil {
		err = wrapErr(err, "failed to marshal proto response")
		s.serveError(ctx, resp, req, twirp.InternalErrorWith(err))
		return
	}

	ctx = setStatusCode(ctx, http.StatusOK)
	resp.Header().Set("Content-Type", "application/protobuf")
	resp.WriteHeader(http.StatusOK)
	if _, err = resp.Write(respBytes); err != nil {
		log.Printf("errored while writing response to client, but already sent response status code to 200: %s", err)
	}
	_ = s.hooks.ResponseSent(ctx)
}

func (s *svc1Server) ServiceDescriptor() ([]byte, int) {
	return twirpFileDescriptor0, 0
}

func (s *svc1Server) ProtocGenTwirpVersion() string {
	return "v4.6.0"
}

// =====
// Utils
// =====

// TwirpServer is the interface generated server structs will support: they're
// HTTP handlers with additional methods for accessing metadata about the
// service. Those accessors are a low-level API for building reflection tools.
// Most people can think of TwirpServers as just http.Handlers.
type TwirpServer interface {
	http.Handler
	// ServiceDescriptor returns gzipped bytes describing the .proto file that
	// this service was generated from. Once unzipped, the bytes can be
	// unmarshaled as a
	// github.com/golang/protobuf/protoc-gen-go/descriptor.FileDescriptorProto.
	//
	// The returned integer is the index of this particular service within that
	// FileDescriptorProto's 'Service' slice of ServiceDescriptorProtos. This is a
	// low-level field, expected to be used for reflection.
	ServiceDescriptor() ([]byte, int)
	// ProtocGenTwirpVersion is the semantic version string of the version of
	// twirp used to generate this file.
	ProtocGenTwirpVersion() string
}

// A ContextSource establishes the base context for a Server.
type ContextSource func(http.ResponseWriter, *http.Request) context.Context

var RequestContextSource ContextSource = func(w http.ResponseWriter, req *http.Request) context.Context {
	return req.Context()
}

var BackgroundContextSource ContextSource = func(http.ResponseWriter, *http.Request) context.Context {
	return context.Background()
}

// done returns ctx.Err() if ctx.Done() indicates that the context done
func done(ctx context.Context) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		return nil
	}
}

// urlBase helps ensure that addr specifies a scheme. If it is unparsable
// as a URL, it returns addr unchanged.
func urlBase(addr string) string {
	// If the addr specifies a scheme, use it. If not, default to
	// http. If url.Parse fails on it, return it unchanged.
	url, err := url.Parse(addr)
	if err != nil {
		return addr
	}
	if url.Scheme == "" {
		url.Scheme = "http"
	}
	return url.String()
}

// setMethodName, setServerName, and setStatusCode are functions to
// expose internal state through contexts. The context keys hardcoded
// in here are not to be used on their own. Twirp provides accessor
// functions which will retrieve these values out and give them the
// correct type for you.
func setMethodName(ctx context.Context, name string) context.Context {
	return context.WithValue(ctx, "__tw_method_name", name)
}
func setServerName(ctx context.Context, name string) context.Context {
	return context.WithValue(ctx, "__tw_service_name", name)
}
func setStatusCode(ctx context.Context, code int) context.Context {
	return context.WithValue(ctx, "__tw_status_code", strconv.Itoa(code))
}

type privateContextKey int

var versionKey = new(privateContextKey)

// setVersionFromPath adds "v1", "v2" or "v3" to the context depending on the path.
func setVersionFromPath(ctx context.Context, path string) context.Context {
	var version string
	if strings.HasPrefix(path, "/v1/") {
		version = "v1"
	} else if strings.HasPrefix(path, "/v2/") {
		version = "v2"
	} else {
		version = "v3"
	}
	return context.WithValue(ctx, versionKey, version)
}

// getVersion returns the version number ("v1", "v2", "v3") or empty string if unset.
func getVersion(ctx context.Context) string {
	v, _ := ctx.Value(versionKey).(string)
	return v
}

// getCustomHTTPReqHeaders retrieves a copy of any headers that are set in
// a context through the twirp.WithHTTPRequestHeaders function.
// If there are no headers set, or if they have the wrong type, nil is returned.
func getCustomHTTPReqHeaders(ctx context.Context) http.Header {
	header, ok := ctx.Value("__tw_request_header").(http.Header)
	if !ok || header == nil {
		return nil
	}
	copied := make(http.Header)
	for k, vv := range header {
		if vv == nil {
			copied[k] = nil
			continue
		}
		copied[k] = make([]string, len(vv))
		copy(copied[k], vv)
	}
	return copied
}

// closebody closes a response or request body and just logs
// any error encountered while closing, since errors are
// considered very unusual.
func closebody(body io.Closer) {
	if err := body.Close(); err != nil {
		log.Printf("error closing body: %q", err)
	}
}

// newRequest makes an http.Request from a client, adding common headers.
func newRequest(ctx context.Context, url string, reqBody io.Reader, contentType string) (*http.Request, error) {
	req, err := http.NewRequest("POST", url, reqBody)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if customHeader := getCustomHTTPReqHeaders(ctx); customHeader != nil {
		req.Header = customHeader
	}
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Twirp-Version", "v4.6.0")
	return req, nil
}

// JSON serialization for errors
type twerrJSON struct {
	Code string            `json:"code"`
	Msg  string            `json:"msg"`
	Meta map[string]string `json:"meta,omitempty"`
}

// marshalErrorToJSON returns JSON from a twirp.Error, that can be used as HTTP error response body.
// If serialization fails, it will use a descriptive Internal error instead.
func marshalErrorToJSON(twerr twirp.Error) []byte {
	// make sure that msg is not too large
	msg := twerr.Msg()
	if len(msg) > 1e6 {
		msg = msg[:1e6]
	}

	tj := twerrJSON{
		Code: string(twerr.Code()),
		Msg:  msg,
		Meta: twerr.MetaMap(),
	}

	buf, err := json.Marshal(&tj)
	if err != nil {
		buf = []byte("{\"type\": \"" + twirp.Internal + "\", \"msg\": \"There was an error but it could not be serialized into JSON\"}") // fallback
	}

	return buf
}

// errorFromResponse builds a twirp.Error from a non-200 HTTP response.
// If the response has a valid serialized Twirp error, then it's returned.
// If not, the response status code is used to generate a similar twirp
// error. See twirpErrorFromIntermediary for more info on intermediary errors.
func errorFromResponse(resp *http.Response) twirp.Error {
	statusCode := resp.StatusCode
	statusText := http.StatusText(statusCode)

	if isHTTPRedirect(statusCode) {
		// Unexpected redirect: it must be an error from an intermediary.
		// Twirp clients dont't follow redirects automatically, Twirp only handles
		// POST requests, redirects should only happen on GET and HEAD requests.
		location := resp.Header.Get("Location")
		msg := fmt1.Sprintf("unexpected HTTP status code %d %q received, Location=%q", statusCode, statusText, location)
		return twirpErrorFromIntermediary(statusCode, msg, location)
	}

	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return clientError("failed to read server error response body", err)
	}
	var tj twerrJSON
	if err := json.Unmarshal(respBodyBytes, &tj); err != nil {
		// Invalid JSON response; it must be an error from an intermediary.
		msg := fmt1.Sprintf("Error from intermediary with HTTP status code %d %q", statusCode, statusText)
		return twirpErrorFromIntermediary(statusCode, msg, string(respBodyBytes))
	}

	errorCode := twirp.ErrorCode(tj.Code)
	if !twirp.IsValidErrorCode(errorCode) {
		msg := "invalid type returned from server error response: " + tj.Code
		return twirp.InternalError(msg)
	}

	twerr := twirp.NewError(errorCode, tj.Msg)
	for k, v := range tj.Meta {
		twerr = twerr.WithMeta(k, v)
	}
	return twerr
}

// twirpErrorFromIntermediary maps HTTP errors from non-twirp sources to twirp errors.
// The mapping is similar to gRPC: https://github.com/grpc/grpc/blob/master/doc/http-grpc-status-mapping.md.
// Returned twirp Errors have some additional metadata for inspection.
func twirpErrorFromIntermediary(status int, msg string, bodyOrLocation string) twirp.Error {
	var code twirp.ErrorCode
	if isHTTPRedirect(status) { // 3xx
		code = twirp.Internal
	} else {
		switch status {
		case 400: // Bad Request
			code = twirp.Internal
		case 401: // Unauthorized
			code = twirp.Unauthenticated
		case 403: // Forbidden
			code = twirp.PermissionDenied
		case 404: // Not Found
			code = twirp.BadRoute
		case 429, 502, 503, 504: // Too Many Requests, Bad Gateway, Service Unavailable, Gateway Timeout
			code = twirp.Unavailable
		default: // All other codes
			code = twirp.Unknown
		}
	}

	twerr := twirp.NewError(code, msg)
	twerr = twerr.WithMeta("http_error_from_intermediary", "true") // to easily know if this error was from intermediary
	twerr = twerr.WithMeta("status_code", strconv.Itoa(status))
	if isHTTPRedirect(status) {
		twerr = twerr.WithMeta("location", bodyOrLocation)
	} else {
		twerr = twerr.WithMeta("body", bodyOrLocation)
	}
	return twerr
}
func isHTTPRedirect(status int) bool {
	return status >= 300 && status <= 399
}

// wrappedError implements the github.com/pkg/errors.Causer interface, allowing errors to be
// examined for their root cause.
type wrappedError struct {
	msg   string
	cause error
}

func wrapErr(err error, msg string) error { return &wrappedError{msg: msg, cause: err} }
func (e *wrappedError) Cause() error      { return e.cause }
func (e *wrappedError) Error() string     { return e.msg + ": " + e.cause.Error() }

// clientError adds consistency to errors generated in the client
func clientError(desc string, err error) twirp.Error {
	return twirp.InternalErrorWith(wrapErr(err, desc))
}

// badRouteError is used when the twirp server cannot route a request
func badRouteError(msg string, method, url string) twirp.Error {
	err := twirp.NewError(twirp.BadRoute, msg)
	err = err.WithMeta("twirp_invalid_route", method+" "+url)
	return err
}

// The standard library will, by default, redirect requests (including POSTs) if it gets a 302 or
// 303 response, and also 301s in go1.8. It redirects by making a second request, changing the
// method to GET and removing the body. This produces very confusing error messages, so instead we
// set a redirect policy that always errors. This stops Go from executing the redirect.
//
// We have to be a little careful in case the user-provided http.Client has its own CheckRedirect
// policy - if so, we'll run through that policy first.
//
// Because this requires modifying the http.Client, we make a new copy of the client and return it.
func withoutRedirects(in *http.Client) *http.Client {
	copy := *in
	copy.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		if in.CheckRedirect != nil {
			// Run the input's redirect if it exists, in case it has side effects, but ignore any error it
			// returns, since we want to use ErrUseLastResponse.
			err := in.CheckRedirect(req, via)
			_ = err // Silly, but this makes sure generated code passes errcheck -blank, which some people use.
		}
		return http.ErrUseLastResponse
	}
	return &copy
}

// doProtoRequest is common code to make a request to the remote twirp service.
func doProtoRequest(ctx context.Context, client *http.Client, url string, in, out proto1.Message) error {
	var err error
	reqBodyBytes, err := proto1.Marshal(in)
	if err != nil {
		return clientError("failed to marshal proto request", err)
	}
	reqBody := bytes.NewBuffer(reqBodyBytes)
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	req, err := newRequest(ctx, url, reqBody, "application/protobuf")
	if err != nil {
		return clientError("could not build request", err)
	}
	resp, err := ctxhttp.Do(ctx, client, req)
	if err != nil {
		return clientError("failed to do request", err)
	}
	defer closebody(resp.Body)
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	if resp.StatusCode != 200 {
		return errorFromResponse(resp)
	}

	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return clientError("failed to read response body", err)
	}
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	if err = proto1.Unmarshal(respBodyBytes, out); err != nil {
		return clientError("failed to unmarshal proto response", err)
	}
	return nil
}

// doJSONRequest is common code to make a request to the remote twirp service.
func doJSONRequest(ctx context.Context, client *http.Client, url string, in, out proto1.Message) error {
	var err error
	reqBody := bytes.NewBuffer(nil)
	marshaler := &jsonpb.Marshaler{OrigName: true}
	if err = marshaler.Marshal(reqBody, in); err != nil {
		return clientError("failed to marshal json request", err)
	}
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	req, err := newRequest(ctx, url, reqBody, "application/json")
	if err != nil {
		return clientError("could not build request", err)
	}
	resp, err := ctxhttp.Do(ctx, client, req)
	if err != nil {
		return clientError("failed to do request", err)
	}
	defer closebody(resp.Body)
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}

	if resp.StatusCode != 200 {
		return errorFromResponse(resp)
	}

	unmarshaler := jsonpb.Unmarshaler{AllowUnknownFields: true}
	if err = unmarshaler.Unmarshal(resp.Body, out); err != nil {
		return clientError("failed to unmarshal json response", err)
	}
	if err = done(ctx); err != nil {
		return clientError("aborted because context was done", err)
	}
	return nil
}

var twirpFileDescriptor0 = []byte{
	// 135 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xcf, 0x2d, 0xcd, 0x29,
	0xc9, 0x2c, 0xc8, 0x49, 0x35, 0xd4, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x32, 0x4f, 0xce, 0x4f,
	0x49, 0xd5, 0xcb, 0x2a, 0x2d, 0x2e, 0xc9, 0xcc, 0xd3, 0x2b, 0x29, 0xd3, 0x4b, 0xce, 0xcf, 0xcd,
	0xcd, 0xcf, 0xd3, 0x2b, 0x29, 0xcf, 0x2c, 0x2a, 0xd0, 0xcb, 0xcc, 0x2b, 0x49, 0x2d, 0xca, 0x4b,
	0xcc, 0x81, 0x70, 0x4b, 0x52, 0x8b, 0x4b, 0xf4, 0x60, 0xfa, 0x95, 0xd8, 0xb8, 0x58, 0x7c, 0x8b,
	0xd3, 0x0d, 0x8d, 0x7a, 0x19, 0xb9, 0x58, 0x82, 0xcb, 0x92, 0x0d, 0x85, 0x5a, 0x40, 0x8c, 0xd4,
	0xbc, 0x14, 0x21, 0x5b, 0x3d, 0x32, 0xcd, 0xd4, 0x03, 0x19, 0x28, 0x45, 0x99, 0x76, 0x27, 0xae,
	0x28, 0x0e, 0x98, 0x40, 0x12, 0x1b, 0xd8, 0x8f, 0xc6, 0x80, 0x00, 0x00, 0x00, 0xff, 0xff, 0x15,
	0x5c, 0xc9, 0xe2, 0xf6, 0x00, 0x00, 0x00,
}
