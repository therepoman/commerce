package twirptest

import (
	"bytes"
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"regexp"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"

	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
	"code.justin.tv/common/twirp/internal/descriptors"
	"code.justin.tv/release/trace/events"

	"golang.org/x/net/context"
)

func TestServeJSON(t *testing.T) {
	h := PickyHatmaker(1)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	client := NewHaberdasherJSONClient(s.URL, http.DefaultClient)

	hat, err := client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("JSON Client err=%q", err)
	}
	if hat.Size != 1 {
		t.Errorf("wrong hat size returned")
	}

	_, err = client.MakeHat(context.Background(), &Size{-1})
	if err == nil {
		t.Errorf("JSON Client expected err, got nil")
	}
}

func TestServerJSONWithUnknownFields(t *testing.T) {
	// Haberdasher server that returns same size it was requested
	h := HaberdasherFunc(func(ctx context.Context, s *Size) (*Hat, error) {
		return &Hat{Size: s.Inches}, nil
	})
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	// Make JSON request with unknown fields ("size" should default to zero-value)
	reqJSON := `{"unknown_field1":"foo", "EXTRASTUFF":"bar"}`
	url := s.URL + HaberdasherPathPrefix + "MakeHat"
	resp, err := http.Post(url, "application/json", bytes.NewBufferString(reqJSON))
	if err != nil {
		t.Fatalf("Unexpected error: %q", err.Error())
	}
	defer func() {
		if err = resp.Body.Close(); err != nil {
			t.Fatalf("Closing body: %q", err.Error())
		}
	}()

	// Make sure that the returned hat is valid and has empty (zero-value) size
	respBytes, err := ioutil.ReadAll(resp.Body) // read manually first in case jsonpb.Unmarshal so it can be printed for debugging
	if err != nil {
		t.Fatalf("Could not even read bytes from response: %q", err.Error())
	}
	hat := new(Hat)
	if err = jsonpb.Unmarshal(bytes.NewReader(respBytes), hat); err != nil {
		t.Fatalf("Could not unmarshall response as Hat: %s", respBytes)
	}
	if hat.Size != 0 {
		t.Errorf("Expected empty size (zero-value), found %q", hat.Size)
	}
}

func TestServeProtobuf(t *testing.T) {
	h := PickyHatmaker(1)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	client := NewHaberdasherProtobufClient(s.URL, http.DefaultClient)

	hat, err := client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("Protobuf Client err=%q", err)
	}
	if hat.Size != 1 {
		t.Errorf("wrong hat size returned")
	}

	_, err = client.MakeHat(context.Background(), &Size{-1})
	if err == nil {
		t.Errorf("Protobuf Client expected err, got nil")
	}
}

func TestDeadline(t *testing.T) {
	timeout := 1 * time.Millisecond
	responseTime := 50 * timeout
	h := SlowHatmaker(responseTime)
	s, client := ServerAndClient(h, nil)
	defer s.Close()

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	done := make(chan struct{})
	go func() {
		_, err := client.MakeHat(ctx, &Size{1})
		if err == nil {
			t.Errorf("should have timed out, but got nil err")
		}
		close(done)
	}()

	select {
	case <-done:
		// pass
	case <-time.After(responseTime):
		t.Errorf("should have timed out within %s, but %s has elapsed!", timeout, responseTime)
	}
}

type requestRecorder struct {
	sync.Mutex
	received []context.Context
	routed   []context.Context
	prepared []context.Context
	sent     []context.Context
	errored  []context.Context
}

func (r *requestRecorder) reset() {
	r.Lock()
	r.received = nil
	r.routed = nil
	r.prepared = nil
	r.sent = nil
	r.errored = nil
	r.Unlock()
}

func recorderHooks() (*twirp.ServerHooks, *requestRecorder) {
	recs := &requestRecorder{}

	hooks := &twirp.ServerHooks{
		RequestReceived: func(ctx context.Context) context.Context {
			recs.Lock()
			recs.received = append(recs.received, ctx)
			recs.Unlock()
			return ctx
		},
		RequestRouted: func(ctx context.Context) context.Context {
			recs.Lock()
			recs.routed = append(recs.routed, ctx)
			recs.Unlock()
			return ctx
		},
		ResponsePrepared: func(ctx context.Context) context.Context {
			recs.Lock()
			recs.prepared = append(recs.prepared, ctx)
			recs.Unlock()
			return ctx
		},
		ResponseSent: func(ctx context.Context) context.Context {
			recs.Lock()
			recs.sent = append(recs.sent, ctx)
			recs.Unlock()
			return ctx
		},
		Error: func(ctx context.Context, _ twirp.Error) context.Context {
			recs.Lock()
			recs.errored = append(recs.errored, ctx)
			recs.Unlock()
			return ctx
		},
	}
	return hooks, recs
}

func TestHooks(t *testing.T) {
	hooks, recorder := recorderHooks()
	h := PickyHatmaker(1)
	s := httptest.NewServer(NewHaberdasherServer(h, hooks, nil))
	defer s.Close()

	clients := map[string]Haberdasher{
		"proto": NewHaberdasherProtobufClient(s.URL, http.DefaultClient),
		"json":  NewHaberdasherJSONClient(s.URL, http.DefaultClient),
	}
	for protocol, client := range clients {
		recorder.reset()
		_, clientErr := client.MakeHat(context.Background(), &Size{1})
		if clientErr != nil {
			t.Fatalf("client err=%q", clientErr)
		}

		if len(recorder.received) != 1 {
			t.Errorf("RequestReceived hook did not fire for %s client", protocol)
		}
		if len(recorder.routed) != 1 {
			t.Errorf("RequestRouted hook did not fire for %s client", protocol)
		}
		if len(recorder.prepared) != 1 {
			t.Errorf("ResponsePrepared hook did not fire for %s client", protocol)
		}
		if len(recorder.sent) != 1 {
			t.Errorf("ResponseSent hook did not fire for %s client", protocol)
		}
		if len(recorder.errored) != 0 {
			t.Errorf("Error hook fired without any error for %s client", protocol)
		}

		// Send a bad request that fails because the underlying interface
		// implementation rejects it. We should only see the
		// RequestReceived, RequestRouted, and Error hooks fire.
		recorder.reset()
		_, clientErr = client.MakeHat(context.Background(), &Size{-1})
		if clientErr == nil {
			t.Fatalf("client err expected, but have no err for %s client", protocol)
		}

		if len(recorder.received) != 1 {
			t.Errorf("RequestReceived hook did not fire for %s client", protocol)
		}
		if len(recorder.routed) != 1 {
			t.Errorf("RequestRouted hook did not fire for %s client", protocol)
		}
		if len(recorder.prepared) != 0 {
			t.Errorf("ResponsePrepared fired, which was not expected for %s client", protocol)
		}
		if len(recorder.sent) != 1 {
			t.Errorf("ResponseSent hook did not fire for %s client", protocol)
		}
		if len(recorder.errored) != 1 {
			t.Errorf("Error hook did not fire for %s client", protocol)
		}
	}
}

// Test that a Twirp server will work even if it receives a ServiceHooks with a
// nil function for one of its hooks.
func TestNilHooks(t *testing.T) {
	var testcase = func(hooks *twirp.ServerHooks) func(*testing.T) {
		return func(t *testing.T) {
			h := PickyHatmaker(1)
			s := httptest.NewServer(NewHaberdasherServer(h, hooks, nil))
			defer s.Close()
			c := NewHaberdasherProtobufClient(s.URL, http.DefaultClient)
			_, err := c.MakeHat(context.Background(), &Size{1})
			if err != nil {
				t.Fatalf("client err=%q", err)
			}
		}
	}

	h, _ := recorderHooks()
	h.RequestReceived = nil
	t.Run("nil RequestReceived", testcase(h))

	h, _ = recorderHooks()
	h.RequestRouted = nil
	t.Run("nil RequestRouted", testcase(h))

	h, _ = recorderHooks()
	h.ResponsePrepared = nil
	t.Run("nil ResponsePrepared", testcase(h))

	h, _ = recorderHooks()
	h.ResponseSent = nil
	t.Run("nil ResponseSent", testcase(h))

	h, _ = recorderHooks()
	h.Error = nil
	t.Run("nil Error", testcase(h))
}

func TestInternalErrorPassing(t *testing.T) {
	e := twirp.InternalError("fatal :(")

	h := ErroringHatmaker(e)
	s, c := ServerAndClient(h, nil)
	defer s.Close()

	_, err := c.MakeHat(context.Background(), &Size{})
	if err == nil {
		t.Fatal("expected err, have nil")
	}

	twerr, ok := err.(twirp.Error)
	if !ok {
		t.Fatalf("expected twirp.Error type error, have %T", err)
	}

	if twerr.Code() != twirp.Internal {
		t.Errorf("expected error type to be Internal, buf found %q", twerr.Code())
	}
	if twerr.Meta("retryable") != "" {
		t.Errorf("expected error to be non-retryable, but it is (should not have meta)")
	}
	if twerr.Msg() != "fatal :(" {
		t.Errorf("expected error message to be passed through, but have=%q", twerr.Msg())
	}
}

func TestErrorWithRetryableMeta(t *testing.T) {
	eMsg := "try again!"
	e := twirp.NewError(twirp.Unavailable, eMsg)
	e = e.WithMeta("retryable", "true")

	h := ErroringHatmaker(e)
	s, c := ServerAndClient(h, nil)
	defer s.Close()

	_, err := c.MakeHat(context.Background(), &Size{})
	if err == nil {
		t.Fatal("expected err, have nil")
	}

	twerr, ok := err.(twirp.Error)
	if !ok {
		t.Fatalf("expected twirp.Error type error, have %T", err)
	}

	if twerr.Meta("retryable") != "true" {
		t.Errorf("expected error to be retryable, but it isnt")
	}
	if twerr.Msg() != eMsg {
		t.Errorf("expected error Msg to be %q, but found %q", eMsg, twerr.Msg())
	}
}

func TestErrorCodePassing(t *testing.T) {
	e := twirp.NewError(twirp.AlreadyExists, "hand-picked ErrorCode")

	h := ErroringHatmaker(e)
	s, c := ServerAndClient(h, nil)
	defer s.Close()

	_, err := c.MakeHat(context.Background(), &Size{})
	if err == nil {
		t.Fatal("expected err, have nil")
	}

	twerr, ok := err.(twirp.Error)
	if !ok {
		t.Fatalf("expected twirp.Error type error, have %T", err)
	}

	if twerr.Code() != twirp.AlreadyExists {
		t.Errorf("expected ErrorCode to be passed through to the client to be %q, but have %q", twirp.AlreadyExists, twerr.Code())
	}
}

// Non twirp errors returned by the servers should become twirp Internal errors.
func TestNonTwirpErrorWrappedAsInternal(t *testing.T) {
	e := errors.New("I am not a twirp error, should become internal")

	h := ErroringHatmaker(e)
	s, c := ServerAndClient(h, nil)
	defer s.Close()

	_, err := c.MakeHat(context.Background(), &Size{})
	if err == nil {
		t.Fatal("expected err, found nil")
	}

	twerr, ok := err.(twirp.Error)
	if !ok {
		t.Fatalf("expected error type to be 'twirp.Error', but found %T", err)
	}

	if twerr.Code() != twirp.Internal {
		t.Errorf("expected ErrorCode to be %q, but found %q", twirp.Internal, twerr.Code())
	}

	if twerr.Msg() != e.Error() { // NOTE: that twerr.Error() is not e.Error() because it has a "twirp error Internal: *" prefix
		t.Errorf("expected Msg to be %q, but found %q", e.Error(), twerr.Msg())
	}
}

func TestChitinIntegration(t *testing.T) {
	traceInterceptor := interceptTrace(t)

	h := PickyHatmaker(1)
	handler := chitin.Handler(NewHaberdasherServer(h, nil, chitin.Context))
	s := httptest.NewServer(handler)
	defer s.Close()

	client := NewHaberdasherJSONClient(s.URL, http.DefaultClient)
	_, err := client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("request err=%q", err)
	}

	// It's not worth it to inspect the individual events' contents. We
	// really just care that we are sending data at all - it's the
	// chitin package's job to send the _right_ events.
	if len(traceInterceptor.events) != 2 {
		t.Errorf("did not receive expected number of trace events have=%d want=2", len(traceInterceptor.events))
	}

	// Also, let's make sure that the context is threaded through
	// properly if we get a request from a chitin-enabled client.
	traceInterceptor = interceptTrace(t)
	client = NewHaberdasherJSONClient(s.URL, chitin.Client(context.Background()))
	_, err = client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("request err=%q", err)
	}

	if len(traceInterceptor.events) != 6 {
		t.Errorf("did not receive expected number of trace events have=%d want=6", len(traceInterceptor.events))
	}

	txid := traceInterceptor.events[0].TransactionId
	for _, ev := range traceInterceptor.events {
		if ev.TransactionId[0] != txid[0] || ev.TransactionId[1] != txid[1] {
			t.Errorf("inconsistent transaction ID. have=%v  want=%v", ev.TransactionId, txid)
		}
	}
}

// traceInterceptor intercepts all data that would be sent to Trace
// and stores it in an internal buffer.
type traceInterceptor struct {
	raw [][]byte

	events          []*events.Event
	invalidMessages [][]byte
}

func (t *traceInterceptor) Write(b []byte) (int, error) {
	t.raw = append(t.raw, b)
	es := &events.EventSet{}
	if err := proto.Unmarshal(b, es); err != nil {
		t.invalidMessages = append(t.invalidMessages, b)
		return len(b), nil // swallow the error
	} else {
		t.events = append(t.events, es.Event...)
	}

	return len(b), nil
}

func interceptTrace(t *testing.T) *traceInterceptor {
	ti := &traceInterceptor{}
	o := chitin.ExperimentalSetTraceDestination(ti)
	if err := chitin.ExperimentalTraceProcessOptIn(o); err != nil {
		// An error here badly violates our expectations of how trace works.
		t.Fatalf("unable to redirect trace towards interceptor! err=%q", err)
	}
	return ti
}

// Hacked-up HTTP transport that rewrites requests paths to be v1 or v2.
type oldPathRT struct {
	version string // "v1" or "v2"
}

func (rt *oldPathRT) RoundTrip(req *http.Request) (*http.Response, error) {
	action := strings.TrimPrefix(req.URL.Path, HaberdasherPathPrefix)
	var path string
	switch rt.version {
	case "v1":
		path = "/v1/Haberdasher/" + action
	case "v2":
		path = HaberdasherPathPrefixOld + action
	default:
		panic("Please use v1 or v2")
	}
	req.URL.Path = path
	return http.DefaultTransport.RoundTrip(req)
}

// Verify that clients can continue to use v1 paths
func TestV1PathNames(t *testing.T) {
	h := PickyHatmaker(1)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	client := NewHaberdasherJSONClient(s.URL, &http.Client{Transport: &oldPathRT{"v1"}})

	hat, err := client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("JSON Client err=%q", err)
	}
	if hat.Size != 1 {
		t.Errorf("wrong hat size returned")
	}

	_, err = client.MakeHat(context.Background(), &Size{-1})
	if err == nil {
		t.Errorf("JSON Client expected err, got nil")
	}
}

// Verify that clients can continue to use v2 paths
func TestV2PathNames(t *testing.T) {
	h := PickyHatmaker(1)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	client := NewHaberdasherJSONClient(s.URL, &http.Client{Transport: &oldPathRT{"v2"}})

	hat, err := client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("JSON Client err=%q", err)
	}
	if hat.Size != 1 {
		t.Errorf("wrong hat size returned")
	}

	_, err = client.MakeHat(context.Background(), &Size{-1})
	if err == nil {
		t.Errorf("JSON Client expected err, got nil")
	}
}

// When errors happen on v2 routes, they return v2 compatible error responses
// With v3err with no meta (default response)
//   Content-Type is "text/plain" (text body is the v3err.Msg())
//   v2err.ErrorID <= v3err.Code()
//   v2err.StatusCode <= same status for v3.Code()
//   v2err.Retryable <= false
func TestV2PathNamesWithErrorResponsesDefaultNoMeta(t *testing.T) {
	v3err := twirp.NewError(twirp.FailedPrecondition, "I don't feel like it")

	h := ErroringHatmaker(v3err)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	// Since we don't have v2 clients in this codebase,
	// we fake a v2 client request and check the expected response.
	url := s.URL + HaberdasherPathPrefixOld + "MakeHat" // v2 path
	body := strings.NewReader(`{"inches": 5}`)
	resp, err := http.Post(url, "application/json", body)
	if err != nil {
		t.Fatalf("could not make request err=%q", err)
	}

	// Read body
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("could not read response err=%q", err)
	}
	bodyStr := string(bodyBytes)
	if err := resp.Body.Close(); err != nil {
		t.Errorf("could not close response body err=%q", err)
	}

	if bodyStr != v3err.Msg() {
		t.Errorf("Expected response to be %q, but found %q", v3err.Msg(), bodyStr)
	}

	if resp.Header.Get("Content-Type") != "text/plain" {
		t.Errorf("Expected Content-Type to be 'text/plain', but found %q", resp.Header.Get("Content-Type"))
	}

	v2ErrorID := string(v3err.Code())
	if resp.Header.Get("Twirp-ErrorID") != v2ErrorID {
		t.Errorf("Expected Twirp-ErrorID to be %q, but found %q", v2ErrorID, resp.Header.Get("Twirp-ErrorID"))
	}

	v2StatusCode := twirp.ServerHTTPStatusFromErrorCode(twirp.FailedPrecondition)
	if resp.StatusCode != v2StatusCode {
		t.Errorf("Expected status code to be %q, but found %q", v2StatusCode, resp.StatusCode)
	}

	if resp.Header.Get("Twirp-Error-Retryable") != "" {
		t.Errorf("Expected Retryable to be unset, but it has unexpected value %q", resp.Header.Get("Retryable"))
	}
}

// When errors happen on v2 routes, they return v2 compatible error responses
// With v3err with meta "v2_error_id", "v2_status_code", "retryable"
//   Content-Type is "text/plain" (text body is the v3err.Msg())
//   v2err.ErrorID <= v3err.Meta("v2_error_id")
//   v2err.StatusCode <= v3err.Meta(""v2_status_code"")
//   v2err.Retryable <= v3err.Meta("retryable") != ""
func TestV2PathNamesWithErrorResponsesWithMeta(t *testing.T) {
	v3err := twirp.NewError(twirp.Internal, "I am an error with metadata")
	v3err = v3err.WithMeta("v2_error_id", "custom_v2_error_id")
	v3err = v3err.WithMeta("v2_status_code", "403") // twirp.Internal is 500, make sure to use a different value as example
	v3err = v3err.WithMeta("retryable", "true")
	v3err = v3err.WithMeta("custom_metadata", "value")

	h := ErroringHatmaker(v3err)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	// Since we don't have v2 clients in this codebase,
	// we fake a v2 client request and check the expected response.
	url := s.URL + HaberdasherPathPrefixOld + "MakeHat" // v2 path
	body := strings.NewReader(`{"inches": 5}`)
	resp, err := http.Post(url, "application/json", body)
	if err != nil {
		t.Fatalf("could not make request err=%q", err)
	}

	// Read body
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("could not read response err=%q", err)
	}
	bodyStr := string(bodyBytes)
	if err := resp.Body.Close(); err != nil {
		t.Errorf("could not close response body err=%q", err)
	}

	if bodyStr != v3err.Msg() {
		t.Errorf("Expected response to be %q, but found %q", v3err.Msg(), bodyStr)
	}

	if resp.Header.Get("Content-Type") != "text/plain" {
		t.Errorf("Expected Content-Type to be 'text/plain', but found %q", resp.Header.Get("Content-Type"))
	}

	v2ErrorID := "custom_v2_error_id"
	if resp.Header.Get("Twirp-ErrorID") != v2ErrorID {
		t.Errorf("Expected Twirp-ErrorID to be %q, but found %q", v2ErrorID, resp.Header.Get("Twirp-ErrorID"))
	}

	v2StatusCode := 403
	if resp.StatusCode != v2StatusCode {
		t.Errorf("Expected status code to be %q, but found %q", v2StatusCode, resp.StatusCode)
	}

	if resp.Header.Get("Twirp-Error-Retryable") != "true" {
		t.Errorf("Expected Retryable to be 'true', but found %q", resp.Header.Get("Retryable"))
	}
}

// When errors happen on v2 routes, they return v2 compatible error responses
// With regular error (not a twirp error)
//   Content-Type is "text/plain" (text body is the err.Error())
//   v2err.ErrorID <= "unknown" (make sure it's not "internal", which is the v3 default)
//   v2err.StatusCode <= 500
//   v2err.Retryable <= false
func TestV2PathNamesWithRegularErrorResponses(t *testing.T) {
	myerr := errors.New("I am not a twirp error")

	h := ErroringHatmaker(myerr)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	// Since we don't have v2 clients in this codebase,
	// we fake a v2 client request and check the expected response.
	url := s.URL + HaberdasherPathPrefixOld + "MakeHat" // v2 path
	body := strings.NewReader(`{"inches": 5}`)
	resp, err := http.Post(url, "application/json", body)
	if err != nil {
		t.Fatalf("could not make request err=%q", err)
	}

	// Read body
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("could not read response err=%q", err)
	}
	bodyStr := string(bodyBytes)
	if err := resp.Body.Close(); err != nil {
		t.Errorf("could not close response body err=%q", err)
	}

	if bodyStr != myerr.Error() {
		t.Errorf("Expected response to be %q, but found %q", myerr.Error(), bodyStr)
	}

	if resp.Header.Get("Content-Type") != "text/plain" {
		t.Errorf("Expected Content-Type to be 'text/plain', but found %q", resp.Header.Get("Content-Type"))
	}

	v2ErrorID := "unknown"
	if resp.Header.Get("Twirp-ErrorID") != v2ErrorID {
		t.Errorf("Expected Twirp-ErrorID to be %q, but found %q", v2ErrorID, resp.Header.Get("Twirp-ErrorID"))
	}

	v2StatusCode := 500
	if resp.StatusCode != v2StatusCode {
		t.Errorf("Expected status code to be %q, but found %q", v2StatusCode, resp.StatusCode)
	}

	if resp.Header.Get("Twirp-Error-Retryable") != "" {
		t.Errorf("Expected Retryable to be unset, but it has unexpected value %q", resp.Header.Get("Retryable"))
	}
}

// When errors happen on v1 routes, they also return v2 compatible error responses.
func TestV1PathNamesWithErrorResponsesWithMeta(t *testing.T) {
	v3err := twirp.NewError(twirp.Internal, "I am an error with metadata")
	v3err = v3err.WithMeta("v2_error_id", "custom_v2_error_id")
	v3err = v3err.WithMeta("v2_status_code", "403") // twirp.Internal is 500, make sure to use a different value as example
	v3err = v3err.WithMeta("retryable", "true")
	v3err = v3err.WithMeta("custom_metadata", "value")

	h := ErroringHatmaker(v3err)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	// Since we don't have v1 clients in this codebase,
	// we fake a v1 client request and check the expected response.
	url := s.URL + "/v1/Haberdasher/MakeHat" // v1 path
	body := strings.NewReader(`{"inches": 5}`)
	resp, err := http.Post(url, "application/json", body)
	if err != nil {
		t.Fatalf("could not make request err=%q", err)
	}

	if resp.Header.Get("Content-Type") != "text/plain" {
		t.Errorf("Expected Content-Type to be 'text/plain', but found %q", resp.Header.Get("Content-Type"))
	}
}

// Clients should be able to connect over HTTPS
func TestConnectTLS(t *testing.T) {
	h := PickyHatmaker(1)
	s := httptest.NewUnstartedServer(NewHaberdasherServer(h, nil, nil))
	s.TLS = &tls.Config{}
	s.StartTLS()
	defer s.Close()

	if !strings.HasPrefix(s.URL, "https") {
		t.Fatal("test server not serving over HTTPS")
	}

	httpsClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	client := NewHaberdasherJSONClient(s.URL, httpsClient)

	hat, err := client.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Fatalf("JSON Client err=%q", err)
	}
	if hat.Size != 1 {
		t.Errorf("wrong hat size returned")
	}

	_, err = client.MakeHat(context.Background(), &Size{-1})
	if err == nil {
		t.Errorf("JSON Client expected err, got nil")
	}
}

// It should be possible to serve twirp alongside non-twirp handlers
func TestMuxingTwirpServer(t *testing.T) {
	// Create a healthcheck endpoint. Record that it got called in a boolean.
	healthcheckCalled := false
	healthcheck := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		healthcheckCalled = true
		w.WriteHeader(200)
		_, err := w.Write([]byte("Looking good, Louis!"))
		if err != nil {
			t.Errorf("err writing response: %s", err)
		}
	})

	// Create a twirp endpoint.
	twirpHandler := NewHaberdasherServer(PickyHatmaker(1), nil, nil)

	// Serve the healthcheck at /health and the twirp handler at the
	// provided URL prefix.
	mux := http.NewServeMux()
	mux.Handle("/health", healthcheck)
	mux.Handle(HaberdasherPathPrefix, twirpHandler)

	s := httptest.NewServer(mux)
	defer s.Close()

	// Try to do a twirp request. It should get routed just fine.
	client := NewHaberdasherJSONClient(s.URL, http.DefaultClient)

	_, twerr := client.MakeHat(context.Background(), &Size{1})
	if twerr != nil {
		t.Errorf("twirp client err=%q", twerr)
	}

	// Try to request the /health endpoint. It should get routed just
	// fine too.
	resp, err := http.Get(s.URL + "/health")
	if err != nil {
		t.Errorf("health check endpoint err=%q", err)
	} else {
		if resp.StatusCode != 200 {
			body, err := ioutil.ReadAll(resp.Body)
			t.Errorf("got a non-200 response from /health: %d", resp.StatusCode)
			t.Logf("response body: %s", body)
			t.Logf("response read err: %s", err)
		}
		if !healthcheckCalled {
			t.Error("health check endpoint was never called")
		}
	}
}

// Default ContextSource should be RequestContextSource, this means that
// when serving in a mux with middleware, the modified request context should
// be available on the method handler.
func TestMuxingTwirpServerDefaultRequestContext(t *testing.T) {
	handlerCalled := false // to verity that the handler assertions were executed (avoid false positives)

	// Make a handler that can check if the context was modified
	twirpHandler := NewHaberdasherServer(HaberdasherFunc(func(ctx context.Context, s *Size) (*Hat, error) {
		handlerCalled = true // verify it was called
		if ctx.Value("modified by middleware") != "yes" {
			t.Error("expected ctx to be modified by the middleware")
		}
		return &Hat{Size: 999}, nil
	}), nil, nil)

	// Wrap with middleware that modifies the request context
	middlewareWrapper := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(context.WithValue(r.Context(), "modified by middleware", "yes"))
		twirpHandler.ServeHTTP(w, r)
	})

	// Serve in a mux
	mux := http.NewServeMux()
	mux.Handle(HaberdasherPathPrefix, middlewareWrapper)
	s := httptest.NewServer(mux)
	defer s.Close()

	// And make a request to run the expectations
	client := NewHaberdasherJSONClient(s.URL, http.DefaultClient)
	_, twerr := client.MakeHat(context.Background(), &Size{1})
	if twerr != nil {
		t.Errorf("twirp client err=%q", twerr)
	}
	if !handlerCalled {
		t.Error("For some reason the twirp request did not make it to the handler")
	}
}

// If an application panics in its handler, it should return a non-retryable error.
func TestPanickyApplication(t *testing.T) {
	hooks, recorder := recorderHooks()
	s := NewHaberdasherServer(PanickyHatmaker("OH NO!"), hooks, nil)

	// Wrap the twirp server with a handler to stop the panicking from
	// crashing the httptest server and failing our test.
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r == nil {
				t.Error("http server never panicked")
			}
		}()
		s.ServeHTTP(w, r)
	})

	server := httptest.NewServer(h)
	defer server.Close()

	client := NewHaberdasherJSONClient(server.URL, http.DefaultClient)

	hat, err := client.MakeHat(context.Background(), &Size{1})
	if err == nil {
		t.Logf("hat: %+v", hat)
		t.Fatal("twirp client err is nil for panicking handler")
	}

	twerr, ok := err.(twirp.Error)
	if !ok {
		t.Fatalf("expected twirp.Error type error, have %T", err)
	}

	if twerr.Code() != twirp.Internal {
		t.Errorf("twirp ErrorCode expected to be %q, but found %q", twirp.Internal, twerr.Code())
	}
	if twerr.Msg() != "Internal service panic" {
		t.Errorf("twirp client err has unexpected message %q, want %q", twerr.Msg(), "Internal service panic")
	}

	// All hooks should be triggered
	if len(recorder.received) != 1 {
		t.Error("RequestReceived hook did not fire")
	}
	if len(recorder.routed) != 1 {
		t.Error("RequestRouted hook did not fire")
	}
	if len(recorder.prepared) != 0 {
		t.Error("ResponsePrepared hook fired, which was not expected during a panic")
	}
	if len(recorder.sent) != 1 {
		t.Error("ResponseSent hook did not fire")
	}
	if len(recorder.errored) != 1 {
		t.Error("Error hook did not fire")
	}
}

func TestCustomRequestHeaders(t *testing.T) {
	// Create a set of headers to be sent on all requests
	customHeader := make(http.Header)
	customHeader.Set("key1", "val1")
	customHeader.Add("multikey", "val1")
	customHeader.Add("multikey", "val2")

	haberdasher := NewHaberdasherServer(PickyHatmaker(1), nil, nil)
	// Make a wrapping handler that checks headers for this test
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// All key-vals in the custom header should appear in the request
		for k, v := range customHeader {
			if !reflect.DeepEqual(r.Header[k], v) {
				t.Errorf("missing header  key=%q  wanted-val=%q  have-val=%q", k, v, r.Header[k])
			}
		}
		haberdasher.ServeHTTP(w, r)
	})

	s := httptest.NewServer(handler)
	defer s.Close()

	clients := map[string]Haberdasher{
		"protobuf": NewHaberdasherProtobufClient(s.URL, http.DefaultClient),
		"json":     NewHaberdasherJSONClient(s.URL, http.DefaultClient),
	}
	for name, c := range clients {
		t.Logf("client=%q", name)
		ctx := context.Background()
		ctx, err := twirp.WithHTTPRequestHeaders(ctx, customHeader)
		if err != nil {
			t.Fatalf("%q client WithHTTPRequestHeaders err=%q", name, err)
		}
		_, err = c.MakeHat(ctx, &Size{1})
		if err != nil {
			t.Errorf("%q client err=%q", name, err)
		}
	}
}

func TestCustomResponseHeaders(t *testing.T) {
	// service that adds headers key1 and key2
	haberdasher := NewHaberdasherServer(HaberdasherFunc(func(ctx context.Context, s *Size) (*Hat, error) {
		var err error
		errMsg := "unexpected error returned by SetHTTPResponseHeader: "

		err = twirp.SetHTTPResponseHeader(ctx, "key1", "val1")
		if err != nil {
			t.Fatalf(errMsg + err.Error())
		}

		err = twirp.SetHTTPResponseHeader(ctx, "key2", "before_override")
		if err != nil {
			t.Fatalf(errMsg + err.Error())
		}
		err = twirp.SetHTTPResponseHeader(ctx, "key2", "val2") // should override
		if err != nil {
			t.Fatalf(errMsg + err.Error())
		}

		childContext := context.WithValue(ctx, "foo", "var")
		err = twirp.SetHTTPResponseHeader(childContext, "key3", "val3")
		if err != nil {
			t.Fatalf(errMsg + err.Error())
		}

		err = twirp.SetHTTPResponseHeader(context.Background(), "key4", "should_be_ignored")
		if err != nil {
			t.Fatalf(errMsg + err.Error())
		}

		err = twirp.SetHTTPResponseHeader(context.Background(), "Content-Type", "should_return_error")
		if err == nil {
			t.Fatalf("SetHTTPResponseHeader expected to return an error on Content-Type header, found nil")
		}

		return &Hat{Size: 999}, nil
	}), nil, nil)

	// Make a wrapping handler that checks header responses for this test
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		haberdasher.ServeHTTP(w, r)

		if w.Header().Get("key1") != "val1" {
			t.Errorf("expected 'key1' header to be 'val1', but found %q", w.Header().Get("key1"))
		}
		if w.Header().Get("key2") != "val2" {
			t.Errorf("expected 'key2' header to be 'val2', but found %q", w.Header().Get("key2"))
		}
		if w.Header().Get("key3") != "val3" {
			t.Errorf("expected 'key3' header to be 'val3', but found %q", w.Header().Get("key3"))
		}
		if w.Header().Get("key4") == "should_be_ignored" {
			t.Error("expected 'key4' header to be empty, it should be ignored if the context is not coming from the handler")
		}
	})

	s := httptest.NewServer(handler)
	defer s.Close()

	c := NewHaberdasherProtobufClient(s.URL, http.DefaultClient)
	resp, err := c.MakeHat(context.Background(), &Size{1})
	if err != nil {
		t.Errorf("unexpected service error: %q", err)
	}
	if resp.Size != 999 { // make sure that the fake handler and its assertions were called
		t.Errorf("expected resp.Size to be 999 (coming from fake handler), but found %d", resp.Size)
	}
}

// A nil response should cause an 'Internal Error' response, not a
// panic.
func TestNilResponse(t *testing.T) {
	h := NewHaberdasherServer(NilHatmaker(), nil, nil)

	panicChecker := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if p := recover(); p != nil {
				t.Errorf("handler panicked: %s", p)
			}
		}()
		h.ServeHTTP(w, r)
	})
	s := httptest.NewServer(panicChecker)
	defer s.Close()

	clients := map[string]Haberdasher{
		"protobuf": NewHaberdasherProtobufClient(s.URL, http.DefaultClient),
		"json":     NewHaberdasherJSONClient(s.URL, http.DefaultClient),
	}
	for name, c := range clients {
		_, err := c.MakeHat(context.Background(), &Size{1})
		if err == nil {
			t.Errorf("%q client err=nil, which is unexpected", name)
		}
	}
}

func TestBadRoute(t *testing.T) {
	h := PickyHatmaker(1)
	s := httptest.NewServer(NewHaberdasherServer(h, nil, nil))
	defer s.Close()

	// Create a transport that we can use to force the wrong HTTP method and URL
	// in test cases.
	rw := &reqRewriter{base: http.DefaultTransport}
	httpClient := &http.Client{Transport: rw}

	clients := map[string]Haberdasher{
		"json":     NewHaberdasherJSONClient(s.URL, httpClient),
		"protobuf": NewHaberdasherProtobufClient(s.URL, httpClient),
	}

	var expectBadRouteError = func(t *testing.T, client Haberdasher) {
		_, err := client.MakeHat(context.Background(), &Size{1})
		if err == nil {
			t.Fatalf("err=nil, expected bad_route")
		}

		twerr, ok := err.(twirp.Error)
		if !ok {
			t.Fatalf("err has type=%T, expected twirp.Error", err)
		}

		if twerr.Code() != twirp.BadRoute {
			t.Errorf("err has code=%v, expected %v", twerr.Code(), twirp.BadRoute)
		}
	}

	for name, client := range clients {
		t.Run(name+" client", func(t *testing.T) {
			t.Run("good route", func(t *testing.T) {
				rw.rewrite = func(r *http.Request) *http.Request { return r }
				_, err := client.MakeHat(context.Background(), &Size{1})
				if err != nil {
					t.Errorf("unexpected error with vanilla client and transport: %v", err)
				}
			})

			t.Run("bad URL path", func(t *testing.T) {
				rw.rewrite = func(r *http.Request) *http.Request {
					r.URL.Path = r.URL.Path + "bogus"
					return r
				}
				expectBadRouteError(t, client)
			})

			t.Run("bad HTTP method", func(t *testing.T) {
				rw.rewrite = func(r *http.Request) *http.Request {
					r.Method = "GET"
					return r
				}
				expectBadRouteError(t, client)
			})

			t.Run("bad content-type", func(t *testing.T) {
				rw.rewrite = func(r *http.Request) *http.Request {
					r.Header.Set("Content-Type", "application/bogus")
					return r
				}
				expectBadRouteError(t, client)
			})
		})
	}
}

// reqRewriter is a http.RoundTripper which can be used to mess with a request
// before it actually gets sent. This is useful as a transport for http.Clients
// in tests because it lets us modify the HTTP method, path, and headers of a
// request, while still being sure that the other unchanged fields are set by a
// generated client.
type reqRewriter struct {
	base    http.RoundTripper
	rewrite func(r *http.Request) *http.Request
}

func (rw *reqRewriter) RoundTrip(req *http.Request) (*http.Response, error) {
	req = rw.rewrite(req)
	return rw.base.RoundTrip(req)
}

func TestReflection(t *testing.T) {
	h := PickyHatmaker(1)
	s := NewHaberdasherServer(h, nil, nil)

	t.Run("ServiceDescriptor", func(t *testing.T) {
		fd, sd, err := descriptors.ServiceDescriptor(s)
		if err != nil {
			t.Fatalf("unable to load descriptor: %v", err)
		}
		if have, want := fd.GetPackage(), "code.justin.tv.common.twirp.internal.twirptest"; have != want {
			t.Errorf("bad package name, have=%q, want=%q", have, want)
		}

		if have, want := sd.GetName(), "Haberdasher"; have != want {
			t.Errorf("bad service name, have=%q, want=%q", have, want)
		}
		if len(sd.Method) != 1 {
			t.Errorf("unexpected number of methods, have=%d, want=1", len(sd.Method))
		}
		if have, want := sd.Method[0].GetName(), "MakeHat"; have != want {
			t.Errorf("bad method name, have=%q, want=%q", have, want)
		}
	})
	t.Run("ProtoGenTwirpVersion", func(t *testing.T) {
		// Should match whatever is in the file at protoc-gen-twirp/version.go
		file, err := ioutil.ReadFile("../../protoc-gen-twirp/version.go")
		if err != nil {
			t.Fatalf("unable to load version file: %v", err)
		}
		rexp, err := regexp.Compile(`const twirpVersion = "(.*)"`)
		if err != nil {
			t.Fatalf("unable to compile twirpVersion regex: %v", err)
		}
		matches := rexp.FindSubmatch(file)
		if matches == nil {
			t.Fatal("unable to find twirp version from version.go file")
		}

		want := string(matches[1])
		have := s.ProtocGenTwirpVersion()
		if have != want {
			t.Errorf("bad version, have=%q, want=%q", have, want)
		}
	})
}
