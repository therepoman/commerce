// +build go1.7

package chitin

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestMergeContexts(t *testing.T) {
	defer compareStacks(t, 10*time.Millisecond)()

	t.Run("2x Background", func(t *testing.T) {
		a := context.Background()
		b := context.Background()

		c := mergeContexts(a, b)
		if c != context.Background() {
			t.Fatalf("expected Background")
		}
	})

	t.Run("Value in a", func(t *testing.T) {
		a := context.WithValue(context.Background(), "k", "va")
		b := context.Background()

		c := mergeContexts(a, b)
		if have, want := c.Value("k"), "va"; have != want {
			t.Fatalf("Value(\"k\"); %q != %q", have, want)
		}
	})

	t.Run("Value in b", func(t *testing.T) {
		a := context.Background()
		b := context.WithValue(context.Background(), "k", "vb")

		c := mergeContexts(a, b)
		if have, want := c.Value("k"), "vb"; have != want {
			t.Fatalf("Value(\"k\"); %q != %q", have, want)
		}
	})

	t.Run("Value in both", func(t *testing.T) {
		a := context.WithValue(context.Background(), "k", "va")
		b := context.WithValue(context.Background(), "k", "vb")

		a, ca := context.WithCancel(a)
		defer ca()

		c := mergeContexts(a, b)
		if have, want := c.Value("k"), "va"; have != want {
			t.Fatalf("Value(\"k\"); %q != %q", have, want)
		}
		if _, ok := c.Deadline(); ok {
			t.Fatalf("Deadline; %t != false", ok)
		}
	})

	t.Run("Ignore Background", func(t *testing.T) {
		a, ca := context.WithCancel(context.Background())
		defer ca()
		b := context.Background()

		c := mergeContexts(a, b)
		if c != a {
			t.Fatalf("expected b to be ignored")
		}
	})

	t.Run("Ignore TODO", func(t *testing.T) {
		a := context.TODO()
		b, cb := context.WithCancel(context.Background())
		defer cb()

		c := mergeContexts(a, b)
		if c != b {
			t.Fatalf("expected a to be ignored")
		}
	})

	for _, v := range []string{"a", "b"} {
		t.Run("Cancel "+v, func(t *testing.T) {
			a, ca := context.WithCancel(context.Background())
			defer ca()
			b, cb := context.WithCancel(context.Background())
			defer cb()

			c := mergeContexts(a, b)
			select {
			case <-c.Done():
				t.Fatalf("Done channel unexpectedly ready")
			default:
			}
			if have, want := c.Err(), error(nil); have != want {
				t.Fatalf("Err; %q != %q", have, want)
			}
			map[string]func(){
				"a": ca,
				"b": cb,
			}[v]()
			<-c.Done()
			if have, want := c.Err(), context.Canceled; have != want {
				t.Fatalf("Err; %q != %q", have, want)
			}
		})
	}

	t.Run("Short Timeout", func(t *testing.T) {
		a, ca := context.WithTimeout(context.Background(), 1*time.Microsecond)
		defer ca()
		b, cb := context.WithCancel(context.Background())
		defer cb()

		c := mergeContexts(a, b)
		<-c.Done()
		if have, want := c.Err(), context.DeadlineExceeded; have != want {
			t.Fatalf("Err; %q != %q", have, want)
		}
		if _, ok := c.Deadline(); !ok {
			t.Fatalf("Deadline; %t != true", ok)
		}
	})

	t.Run("Long Timeout", func(t *testing.T) {
		a, ca := context.WithTimeout(context.Background(), 1*time.Hour)
		defer ca()
		b, cb := context.WithCancel(context.Background())
		defer cb()

		c := mergeContexts(a, b)
		if have, want := c.Err(), error(nil); have != want {
			t.Fatalf("Err; %q != %q", have, want)
		}
	})

	for _, v := range []string{"a", "b"} {
		t.Run("Deadline "+v, func(t *testing.T) {
			early, late := time.Unix(1, 0), time.Unix(2, 0)
			ta := map[string]time.Time{"a": early, "b": late}[v]
			tb := map[string]time.Time{"a": late, "b": early}[v]

			a, ca := context.WithDeadline(context.Background(), ta)
			defer ca()
			b, cb := context.WithDeadline(context.Background(), tb)
			defer cb()

			c := mergeContexts(a, b)
			d, ok := c.Deadline()
			if have, want := d, early; have != want {
				t.Errorf("Deadline; %s != %s", have, want)
			}
			if have, want := ok, true; have != want {
				t.Errorf("Deadline; %t != %t", have, want)
			}
		})
	}
}

func TestClientContextLeak(t *testing.T) {
	testCase := func(handler http.HandlerFunc, closeEarly bool, body string) func(t *testing.T) {
		return func(t *testing.T) {
			defer compareStacks(t, 10*time.Millisecond)()

			srv := httptest.NewServer(handler)
			defer srv.Close()

			cl := Client(context.WithValue(context.Background(), 1, 2))
			req, err := http.NewRequest("GET", srv.URL, nil)
			if err != nil {
				t.Fatalf("http.NewRequest; err = %q", err)
			}
			req = req.WithContext(context.WithValue(context.Background(), 3, 4))
			resp, err := cl.Do(req)
			if err == nil {
				defer resp.Body.Close()
				if closeEarly {
					resp.Body.Close()
				}
				buf, err := ioutil.ReadAll(resp.Body)
				if err != nil && !closeEarly {
					t.Fatalf("ioutil.ReadAll; err = %q", err)
				}
				if have, want := string(buf), body; have != want {
					t.Errorf("body; %q != %q", have, want)
				}
			}
		}
	}

	t.Run("panic", testCase(func(w http.ResponseWriter, r *http.Request) {
		panic(nil)
	}, false, ""))

	t.Run("normal", testCase(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "hello world")
	}, false, "hello world"))

	t.Run("flushed", testCase(func(w http.ResponseWriter, r *http.Request) {
		w.(http.Flusher).Flush()
		fmt.Fprintf(w, "hello world")
	}, false, "hello world"))

	t.Run("early close", testCase(func(w http.ResponseWriter, r *http.Request) {
		w.(http.Flusher).Flush()
		fmt.Fprintf(w, "hello world")
	}, true, ""))
}

func TestMergeContextsValues(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "k", "v")

	var srv *httptest.Server
	srv = httptest.NewServer(Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if have, want := r.Context().Value("k"), interface{}("v"); have != want {
			t.Errorf("r.Context().Value(\"k\"); %#v != %#v", have, want)
		}
		if have, want := r.Context().Value(http.ServerContextKey), interface{}(srv.Config); have != want {
			t.Errorf("r.Context().Value(http.ServerContextKey); %#v != %#v", have, want)
		}
	}), SetBaseContext(ctx)))
	defer srv.Close()

	resp, err := http.Get(srv.URL)
	if err != nil {
		t.Fatalf("http.Get; err = %q", err)
	}
	defer resp.Body.Close()
}
