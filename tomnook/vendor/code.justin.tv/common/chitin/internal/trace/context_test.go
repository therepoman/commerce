package trace

import (
	"net/http"
	"reflect"
	"testing"

	"golang.org/x/net/context"
)

func TestHeader(t *testing.T) {
	h := testHeaders()
	ctx := ContextFromHeader(context.Background(), h)

	tx, _ := FromContext(ctx)
	if have, want := tx.TxID, [2]uint64{12345678, 9}; have != want {
		t.Errorf("transaction id; %d != %d", have, want)
	}
	if have, want := tx.Path, (CallPath{6, 7, 8}); !reflect.DeepEqual(have, want) {
		t.Errorf("span id; %q != %q", have, want)
	}

	out1 := make(http.Header)
	AugmentHeader(ctx, out1)
	if have, want := out1[http.CanonicalHeaderKey("Trace-ID")], []string{"12345678", "9"}; !reflect.DeepEqual(have, want) {
		t.Errorf("Trace-ID; %q != %q", have, want)
	}
	if have, want := out1.Get("Trace-Span"), ".6.7.8"; have != want {
		t.Errorf("Trace-Span; %q != %q", have, want)
	}

	out2 := make(http.Header)
	child := ctx
	for i := 0; i < 24+1; i++ {
		child = NewSpan(ctx)
	}
	AugmentHeader(child, out2)
	if have, want := out2[http.CanonicalHeaderKey("Trace-ID")], []string{"12345678", "9"}; !reflect.DeepEqual(have, want) {
		t.Errorf("Trace-ID; %q != %q", have, want)
	}
	if have, want := out2.Get("Trace-Span"), ".6.7.8.24"; have != want {
		t.Errorf("Trace-Span; %q != %q", have, want)
	}
}

func TestNonZero(t *testing.T) {
	suspect := 0
	count := 20
	for i := 0; i < count; i++ {
		h := make(http.Header)
		ctx := ContextFromHeader(context.Background(), h)
		tx, _ := FromContext(ctx)
		txid := tx.TxID

		if txid[0] == 0 || txid[1] == 0 {
			suspect++
		}
	}

	if suspect >= 5 {
		t.Errorf("TxID is often unset (%d times out of %d)", suspect, count)
	}
}

func TestLogfmt(t *testing.T) {
	h := testHeaders()
	ctx := ContextFromHeader(context.Background(), h)

	if have, want := Logfmt(ctx), `trace-id=4e61bc00000000000900000000000000 trace-span=".6.7.8"`; have != want {
		t.Errorf("Logfmt;\n%q\n!=\n%q", have, want)
	}
}

func TestGetTraceID(t *testing.T) {
	h := testHeaders()
	ctx := ContextFromHeader(context.Background(), h)

	if have, want := GetTraceID(ctx).String(), `4e61bc00000000000900000000000000.6.7.8`; have != want {
		t.Errorf("GetTraceID;\n%q\n!=\n%q", have, want)
	}
	if have, want := GetTraceID(ctx).Transaction(), `4e61bc00000000000900000000000000`; have != want {
		t.Errorf("GetTraceID;\n%q\n!=\n%q", have, want)
	}
	if have, want := GetTraceID(context.Background()).Transaction(), ``; have != want {
		t.Errorf("GetTraceID;\n%q\n!=\n%q", have, want)
	}
}

func testHeaders() http.Header {
	h := make(http.Header)
	h.Set("Trace-ID", "12345678")
	h.Add("Trace-ID", "9")
	h.Set("Trace-Span", ".6.7.8")
	return h
}
