package trace

import (
	"expvar"
	"testing"
)

// If a package using chitin imports a package which _also_ uses
// chitin, and both chitin-importers are vendoring chitin, then the
// 'publish' function can effectively be run twice. Naively, this
// would panic as an expvar.Publish gets called with the same variable
// name twice.
func TestDoublePublishExpvar(t *testing.T) {
	// Although the init() function has run, pkgpath.Caller(0) isn't
	// capable of figuring out the pkgpath of test code, so it returned
	// "", so it didn't actually publish. We need to do two publish
	// calls ourselves.
	publish("code.justin.tv/common/chitin/internal/trace", 0)
	publish("code.justin.tv/common/chitin/internal/trace", 0)

	// We should have 2 expvars available:
	v := expvar.Get("code.justin.tv/common/chitin/internal/trace")
	if v == nil {
		t.Errorf("cannot find expvar for 'code.justin.tv/common/chitin/internal/trace'")
	}
	v = expvar.Get("code.justin.tv/common/chitin/internal/trace-1")
	if v == nil {
		t.Errorf("cannot find expvar for 'code.justin.tv/common/chitin/internal/trace-1'")
	}
}
