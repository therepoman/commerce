Nobody agrees on what the signals mean. The `signal` manpage gives very short
descriptions that don't line up with the sorts of things we're trying to do
these days.

We're constrained by our process managers. The stock version of supervise only
has command line flags for a few different signals, and terminals can only
send SIGINT, SIGTSTP (not useful for us), and SIGQUIT. We might want to use
einhorn, and it has opinions about signal meanings.

The third party software we run has opinions on signals, but none of them
agree on what the signals mean.


## Process managers ##

### einhorn ###

einhorn listens for INT, TERM, QUIT, HUP, ALRM, CHLD, and USR2.
ALRM is deprecated, and CHLD we don't need.

einhorn sends USR2, TERM, and QUIT.

When einhorn gets INT, it sends USR2 and shuts down after workers die.
TERM -> TERM, and shutdown after workers die
USR2 -> USR2, and shutdown after workers die
QUIT -> QUIT, and immediate shutdown
HUP -> upgrade workers
ALRM -> upgrade workers (after printing deprecation warning)
CHLD -> ignored

USR2 means "graceful shutdown and exit"
TERM means "quick shutdown"
QUIT means "shut down as fast as you can, every process for itself"

HUP means "reload config", where sometimes config is code
INT is a convenient alias for USR2
ALRM isn't important
CHLD isn't relevant

Whereas QUIT means "exit as quickly as possible", and whereas it's one of the
signals that einhorn will send during normalish operations, Go processes
should not respond to SIGQUIT by printing a stack dump to stderr. ABRT will
still trigger this response.

### supervise ###

supervise can send a limited number of signals:
- STOP (svc -p)
- CONT (svc -c)
- HUP  (svc -h)
- ALRM (svc -a)
- INT  (svc -i)
- TERM (svc -t)
- KILL (svc -k)


## Third party apps we run ##

### nginx ###

nginx supports some signals on the master process:
INT - quick shutdown
TERM - quick shutdown
HUP - config reload .. spawn new workers and graceful shutdown the old ones
USR1 - reopen log files
USR2 - upgrade executable
WINCH - graceful shut down workers

nginx supports some signals on the worker processes:
INT - quick shutdown
TERM - quick shutdown
QUIT - graceful shutdown
USR1 - reopen log files

### unicorn ###

unicorn master process:
HUP - reload config, graceful worker restart, maybe don't reload app code
INT - quick shutdown
TERM - quick shutdown
QUIT - graceful shutdown
USR1 - reopen logs
USR2 - reexec (including reloading the app)
WINCH - graceful shut down workers
TTIN - increment worker count
TTOU - decrement worker count

unicorn worker process:
INT - quick shutdown
TERM - quick shutdown
QUIT - graceful shutdown after this request
USR1 - reopen logs

### haproxy ###

haproxy process:
USR1 - exit once all sessions are closed
TTOU - stop listening on all sockets
TTIN - start listening on all sockets
INT - quick shutdown
TERM - quick shutdown
HUP - write out status to log
QUIT - write out memory pool data to log

### postgres ###

postgres master:
TERM - graceful shutdown
INT - quick shutdown
QUIT - every process for itself


## What does it all mean? ##

Everyone but postgres agrees that SIGTERM triggers a quick shutdown. There's
disagreement on SIGQUIT, with some processes using it to signal a graceful
shutdown and others using it to signal the fastest of all possible shutdowns.
SIGUSR2, when handled, means to load a new app version and gracefully finish
the older instances. SIGHUP to a master process usually indicates that it
should load a new app version and gracefully transition to it.

### The thing we actually care about ###

To trigger a quick shutdown of a process tree, send SIGTERM to its root.
To trigger a dangerously quick shutdown of a process tree, send SIGQUIT to its root.
To trigger a graceful shutdown of a tree, send SIGUSR2 to its root.
To trigger a graceful reload of a tree, send SIGHUP to its root.

SIGINT is a convenient equivalent of SIGUSR2.
