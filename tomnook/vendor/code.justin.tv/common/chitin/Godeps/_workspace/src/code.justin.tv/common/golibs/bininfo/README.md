# BinInfo

A package used to output helpful details about your binary.

## Vars

It exposes the following properties about your binary:

```
{
"revision": "revision",
"svcname": "code.justin.tv/org/repo"
}
```

Q: how do I set the revision?

A: add the following to your .manta.json: `go build -ldflags "-X
code.justin.tv/common/golibs/bininfo.revision=$GIT_COMMIT"`

Q: how do I get access to these properties from within my code?

A: `bininfo.Foo()`, where foo is the property of interest. Currently
we have `Revision()` and `SvcName()`.

## How do I use this?

If you're using code.justin.tv/common/chitin you're already good to
go, otherwise add the following to your import path, probably in
your main package:

```
import (
  _ "code.justin.tv/common/golibs/bininfo"
)
```

