//line sql.y:6
package pgparse

import __yyfmt__ "fmt"

//line sql.y:6
import "bytes"

func SetParseTree(yylex interface{}, stmt Statement) {
	yylex.(*Tokenizer).ParseTree = stmt
}

func SetAllowComments(yylex interface{}, allow bool) {
	yylex.(*Tokenizer).AllowComments = allow
}

func ForceEOF(yylex interface{}) {
	yylex.(*Tokenizer).ForceEOF = true
}

var (
	SHARE        = []byte("share")
	MODE         = []byte("mode")
	IF_BYTES     = []byte("if")
	VALUES_BYTES = []byte("values")
)

//line sql.y:31
type yySymType struct {
	yys         int
	empty       struct{}
	statement   Statement
	selStmt     SelectStatement
	byt         byte
	bytes       []byte
	bytes2      [][]byte
	str         string
	selectExprs SelectExprs
	selectExpr  SelectExpr
	columns     Columns
	colName     *ColName
	tableExprs  TableExprs
	tableExpr   TableExpr
	smTableExpr SimpleTableExpr
	tableName   *TableName
	indexHints  *IndexHints
	expr        Expr
	boolExpr    BoolExpr
	valExpr     ValExpr
	colTuple    ColTuple
	valExprs    ValExprs
	values      Values
	rowTuple    RowTuple
	indexedExpr IndexedExpr
	subquery    *Subquery
	caseExpr    *CaseExpr
	whens       []*When
	when        *When
	orderBy     OrderBy
	order       *Order
	limit       *Limit
	insRows     InsertRows
	updateExprs UpdateExprs
	updateExpr  *UpdateExpr
}

const LEX_ERROR = 57346
const SELECT = 57347
const INSERT = 57348
const UPDATE = 57349
const DELETE = 57350
const FROM = 57351
const WHERE = 57352
const GROUP = 57353
const HAVING = 57354
const ORDER = 57355
const BY = 57356
const NULLS = 57357
const FIRST = 57358
const LAST = 57359
const LIMIT = 57360
const OFFSET = 57361
const FOR = 57362
const ALL = 57363
const DISTINCT = 57364
const AS = 57365
const EXISTS = 57366
const IN = 57367
const IS = 57368
const ILIKE = 57369
const LIKE = 57370
const BETWEEN = 57371
const NULL = 57372
const ASC = 57373
const DESC = 57374
const VALUES = 57375
const INTO = 57376
const RETURNING = 57377
const DUPLICATE = 57378
const KEY = 57379
const DEFAULT = 57380
const SET = 57381
const LOCK = 57382
const FALSE_P = 57383
const TRUE_P = 57384
const KEYRANGE = 57385
const ID = 57386
const STRING = 57387
const NUMBER = 57388
const VALUE_ARG = 57389
const LIST_ARG = 57390
const COMMENT = 57391
const TIME = 57392
const ZONE = 57393
const LE = 57394
const GE = 57395
const NE = 57396
const NOT_LIKE = 57397
const NULL_SAFE_EQUAL = 57398
const NOT_SIMILAR = 57399
const TYPECAST = 57400
const EXTRACT = 57401
const EPOCH_P = 57402
const YEAR_P = 57403
const MONTH_P = 57404
const DAY_P = 57405
const HOUR_P = 57406
const MINUTE_P = 57407
const SECOND_P = 57408
const UNION = 57409
const MINUS = 57410
const EXCEPT = 57411
const INTERSECT = 57412
const JOIN = 57413
const STRAIGHT_JOIN = 57414
const LEFT = 57415
const RIGHT = 57416
const INNER = 57417
const OUTER = 57418
const CROSS = 57419
const NATURAL = 57420
const USE = 57421
const FORCE = 57422
const ON = 57423
const OR = 57424
const AND = 57425
const NOT = 57426
const AT = 57427
const UNARY = 57428
const CASE = 57429
const WHEN = 57430
const THEN = 57431
const ELSE = 57432
const END = 57433
const CREATE = 57434
const ALTER = 57435
const DROP = 57436
const RENAME = 57437
const ANALYZE = 57438
const TABLE = 57439
const INDEX = 57440
const VIEW = 57441
const TO = 57442
const IGNORE = 57443
const IF = 57444
const UNIQUE = 57445
const USING = 57446
const SHOW = 57447
const DESCRIBE = 57448
const EXPLAIN = 57449

var yyToknames = [...]string{
	"$end",
	"error",
	"$unk",
	"LEX_ERROR",
	"SELECT",
	"INSERT",
	"UPDATE",
	"DELETE",
	"FROM",
	"WHERE",
	"GROUP",
	"HAVING",
	"ORDER",
	"BY",
	"NULLS",
	"FIRST",
	"LAST",
	"LIMIT",
	"OFFSET",
	"FOR",
	"ALL",
	"DISTINCT",
	"AS",
	"EXISTS",
	"IN",
	"IS",
	"ILIKE",
	"LIKE",
	"BETWEEN",
	"NULL",
	"ASC",
	"DESC",
	"VALUES",
	"INTO",
	"RETURNING",
	"DUPLICATE",
	"KEY",
	"DEFAULT",
	"SET",
	"LOCK",
	"FALSE_P",
	"TRUE_P",
	"KEYRANGE",
	"ID",
	"STRING",
	"NUMBER",
	"VALUE_ARG",
	"LIST_ARG",
	"COMMENT",
	"TIME",
	"ZONE",
	"LE",
	"GE",
	"NE",
	"NOT_LIKE",
	"NULL_SAFE_EQUAL",
	"NOT_SIMILAR",
	"TYPECAST",
	"'('",
	"'='",
	"'<'",
	"'>'",
	"'~'",
	"EXTRACT",
	"EPOCH_P",
	"YEAR_P",
	"MONTH_P",
	"DAY_P",
	"HOUR_P",
	"MINUTE_P",
	"SECOND_P",
	"UNION",
	"MINUS",
	"EXCEPT",
	"INTERSECT",
	"','",
	"JOIN",
	"STRAIGHT_JOIN",
	"LEFT",
	"RIGHT",
	"INNER",
	"OUTER",
	"CROSS",
	"NATURAL",
	"USE",
	"FORCE",
	"ON",
	"OR",
	"AND",
	"NOT",
	"'&'",
	"'|'",
	"'^'",
	"'+'",
	"'-'",
	"'*'",
	"'/'",
	"'%'",
	"AT",
	"'['",
	"']'",
	"'.'",
	"UNARY",
	"CASE",
	"WHEN",
	"THEN",
	"ELSE",
	"END",
	"CREATE",
	"ALTER",
	"DROP",
	"RENAME",
	"ANALYZE",
	"TABLE",
	"INDEX",
	"VIEW",
	"TO",
	"IGNORE",
	"IF",
	"UNIQUE",
	"USING",
	"SHOW",
	"DESCRIBE",
	"EXPLAIN",
	"')'",
}
var yyStatenames = [...]string{}

const yyEofCode = 1
const yyErrCode = 2
const yyInitialStackSize = 16

//line yacctab:1
var yyExca = [...]int{
	-1, 1,
	1, -1,
	-2, 0,
	-1, 91,
	25, 128,
	26, 128,
	27, 128,
	28, 128,
	29, 128,
	52, 128,
	53, 128,
	54, 128,
	56, 128,
	57, 128,
	58, 128,
	60, 128,
	61, 128,
	62, 128,
	90, 128,
	91, 128,
	92, 128,
	93, 128,
	94, 128,
	95, 128,
	96, 128,
	97, 128,
	98, 128,
	99, 128,
	-2, 111,
}

const yyNprod = 236
const yyPrivate = 57344

var yyTokenNames []string
var yyStates []string

const yyLast = 751

var yyAct = [...]int{

	94, 329, 169, 416, 62, 85, 375, 277, 365, 92,
	90, 412, 321, 222, 210, 268, 81, 389, 190, 86,
	155, 80, 168, 3, 427, 50, 288, 289, 290, 291,
	292, 427, 293, 294, 139, 138, 427, 133, 65, 249,
	327, 70, 133, 133, 73, 198, 76, 68, 77, 249,
	53, 51, 52, 147, 148, 149, 150, 151, 152, 153,
	154, 156, 174, 63, 28, 29, 30, 31, 284, 127,
	123, 247, 386, 429, 351, 122, 269, 106, 362, 131,
	428, 347, 349, 385, 135, 426, 359, 388, 353, 326,
	384, 314, 312, 69, 165, 167, 124, 72, 250, 126,
	64, 38, 43, 40, 44, 177, 299, 41, 166, 170,
	172, 49, 45, 173, 348, 204, 65, 248, 91, 65,
	188, 194, 46, 47, 48, 139, 138, 14, 15, 16,
	17, 269, 184, 319, 137, 120, 116, 216, 194, 193,
	202, 138, 155, 364, 218, 220, 205, 195, 139, 138,
	215, 91, 91, 71, 381, 118, 245, 208, 219, 219,
	221, 18, 322, 229, 230, 231, 322, 237, 238, 239,
	240, 241, 242, 243, 244, 280, 180, 155, 64, 130,
	341, 64, 192, 156, 339, 342, 191, 65, 65, 340,
	383, 273, 251, 266, 382, 252, 91, 279, 254, 281,
	256, 91, 91, 345, 344, 217, 343, 276, 272, 118,
	214, 219, 201, 203, 200, 152, 153, 154, 156, 224,
	249, 191, 155, 133, 397, 282, 300, 298, 285, 368,
	316, 19, 20, 22, 21, 23, 132, 119, 213, 91,
	301, 91, 407, 252, 24, 25, 26, 302, 303, 64,
	275, 186, 286, 212, 91, 147, 148, 149, 150, 151,
	152, 153, 154, 156, 59, 311, 406, 225, 405, 310,
	113, 187, 232, 175, 255, 320, 98, 313, 223, 182,
	233, 324, 103, 318, 328, 111, 325, 118, 155, 219,
	214, 236, 235, 105, 104, 99, 84, 100, 101, 102,
	181, 337, 338, 133, 224, 179, 288, 289, 290, 291,
	292, 89, 293, 294, 14, 109, 96, 178, 91, 28,
	29, 30, 31, 114, 355, 309, 117, 361, 65, 358,
	370, 246, 91, 75, 71, 360, 297, 136, 371, 363,
	234, 115, 88, 66, 352, 350, 107, 108, 82, 334,
	372, 373, 376, 213, 333, 155, 112, 296, 71, 377,
	207, 306, 206, 189, 214, 214, 128, 125, 212, 121,
	60, 110, 308, 307, 387, 392, 79, 253, 393, 394,
	74, 367, 78, 403, 390, 402, 401, 404, 391, 366,
	369, 150, 151, 152, 153, 154, 156, 357, 356, 219,
	252, 219, 424, 415, 411, 14, 417, 417, 417, 65,
	418, 419, 414, 420, 431, 409, 410, 376, 98, 58,
	196, 129, 425, 56, 103, 265, 54, 111, 432, 91,
	330, 331, 433, 271, 434, 105, 104, 99, 84, 100,
	101, 102, 91, 413, 91, 258, 259, 260, 261, 262,
	263, 264, 103, 89, 421, 422, 380, 109, 96, 332,
	278, 103, 379, 105, 104, 191, 14, 100, 101, 102,
	336, 64, 105, 104, 315, 305, 100, 101, 102, 226,
	61, 176, 227, 228, 88, 98, 430, 408, 107, 108,
	82, 103, 14, 32, 111, 33, 257, 197, 112, 39,
	283, 199, 105, 104, 99, 66, 100, 101, 102, 34,
	35, 36, 37, 110, 42, 67, 274, 185, 423, 398,
	89, 374, 98, 378, 109, 96, 335, 317, 103, 183,
	267, 111, 97, 93, 95, 323, 171, 270, 140, 105,
	104, 99, 66, 100, 101, 102, 87, 346, 211, 287,
	209, 88, 83, 295, 134, 107, 108, 89, 55, 27,
	57, 109, 96, 13, 12, 112, 11, 103, 10, 14,
	111, 9, 8, 7, 6, 5, 4, 2, 105, 104,
	110, 66, 100, 101, 102, 399, 400, 1, 88, 0,
	0, 0, 107, 108, 103, 0, 175, 111, 0, 396,
	109, 96, 112, 0, 0, 105, 104, 0, 66, 100,
	101, 102, 155, 0, 0, 395, 0, 110, 0, 0,
	0, 0, 0, 175, 0, 0, 0, 109, 96, 0,
	0, 107, 108, 0, 141, 146, 143, 144, 145, 155,
	0, 112, 0, 0, 0, 147, 148, 149, 150, 151,
	152, 153, 154, 156, 155, 0, 110, 0, 107, 108,
	0, 160, 161, 162, 0, 163, 164, 155, 112, 157,
	158, 159, 147, 148, 149, 150, 151, 152, 153, 154,
	156, 155, 0, 110, 0, 0, 0, 147, 148, 149,
	150, 151, 152, 153, 154, 156, 155, 0, 0, 142,
	147, 148, 149, 150, 151, 152, 153, 154, 156, 155,
	0, 0, 354, 0, 147, 148, 149, 150, 151, 152,
	153, 154, 156, 0, 0, 0, 0, 304, 0, 147,
	148, 149, 150, 151, 152, 153, 154, 156, 0, 0,
	0, 0, 147, 148, 149, 150, 151, 152, 153, 154,
	156,
}
var yyPact = [...]int{

	122, -1000, -1000, 247, -1000, -1000, -1000, -1000, -1000, -1000,
	-1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -13,
	-14, -2, 8, -3, -1000, -1000, -1000, 487, 405, -1000,
	-1000, -1000, 401, -1000, 385, 326, 471, 299, -72, -22,
	290, -1000, -17, 290, -1000, 336, -73, 290, -73, 332,
	-1000, -1000, -1000, -1000, -1000, 394, -1000, 221, 326, 302,
	34, 326, 133, -1000, 177, -1000, 33, 325, -15, 290,
	-1000, -1000, 323, -1000, -48, 322, 397, 92, 290, -1000,
	227, -1000, -1000, 314, 32, 60, 609, -1000, 498, 461,
	-1000, 10, -1000, 537, 422, 258, 246, -1000, 241, 220,
	-1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000,
	-1000, -1000, 537, -1000, 212, 299, 319, 455, 299, 537,
	290, -1000, 396, -76, -1000, 102, -1000, 318, -1000, -1000,
	316, -1000, 194, 394, -1000, -1000, 290, 109, 498, 498,
	537, 219, 454, 537, 537, 537, 250, 537, 537, 537,
	537, 537, 537, 537, 537, 290, 281, -1000, -1000, -1000,
	-1000, -1000, -1000, -1000, -1000, -1000, 609, -54, -8, -27,
	609, -1000, 537, 230, 10, 564, 252, -1000, 394, 380,
	-1000, 487, 431, -29, 651, 400, 299, 299, 211, -1000,
	447, 498, -1000, 651, -1000, -1000, -1000, 88, 290, -1000,
	-49, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, 176,
	229, 313, 309, 4, -1000, -1000, -1000, -1000, -1000, 609,
	52, 651, -1000, 564, -1000, -1000, 219, 537, 537, 651,
	651, 638, 466, -1000, 331, -1000, -1000, 297, 297, 297,
	119, 119, 84, 84, 84, -1000, 274, -1000, -1000, 537,
	-1000, 164, 651, -1000, -33, 394, -34, 465, -1000, -1000,
	-1000, -1000, -1000, -1000, -1000, -1000, 154, 26, -1000, 498,
	75, 214, 247, 79, -36, -1000, 447, 412, 445, 60,
	310, -1000, -1000, 305, -1000, 459, 194, 194, -1000, -1000,
	107, 103, 129, 127, 126, -4, -1000, 301, -51, 300,
	-37, -1000, 651, 623, 537, 356, -1000, -1000, -1000, 537,
	651, -1000, -1000, -39, -1000, 537, 431, -30, -1000, 537,
	37, 354, 345, 153, -1000, -1000, -1000, 299, 412, 354,
	537, 537, 537, -1000, -1000, 450, 442, 229, 67, -1000,
	117, -1000, 113, -1000, -1000, -1000, -1000, -25, -32, -43,
	-1000, -1000, -1000, -1000, 537, 651, -1000, -1000, 230, -1000,
	-38, -108, -1000, 651, 537, -1000, 394, 338, 214, -1000,
	354, -1000, 596, 581, 148, -1000, 554, -1000, 447, 498,
	537, 498, -1000, -1000, 209, 207, 183, 651, -1000, -1000,
	651, 147, 480, -1000, -1000, 537, 537, 537, -1000, 428,
	428, 412, 60, 144, 60, 290, 290, 290, 299, 651,
	651, -1000, -1000, 438, -1000, 382, -40, -1000, -45, -52,
	133, -1000, -1000, -1000, 479, 389, -1000, 290, -1000, -1000,
	-1000, 290, -1000, 290, -1000,
}
var yyPgo = [...]int{

	0, 587, 577, 22, 576, 575, 574, 573, 572, 571,
	568, 566, 564, 563, 493, 560, 559, 558, 21, 16,
	554, 553, 552, 550, 14, 549, 548, 264, 547, 3,
	18, 5, 546, 538, 537, 10, 19, 536, 13, 2,
	535, 9, 534, 77, 533, 62, 532, 530, 15, 529,
	527, 526, 523, 7, 521, 6, 519, 1, 518, 517,
	516, 8, 12, 4, 63, 333, 515, 514, 501, 500,
	499, 497, 0, 25, 496, 495, 11,
}
var yyR1 = [...]int{

	0, 1, 2, 2, 2, 2, 2, 2, 2, 2,
	2, 2, 2, 3, 3, 3, 4, 4, 5, 6,
	7, 8, 8, 8, 9, 9, 9, 10, 11, 11,
	11, 12, 13, 13, 13, 75, 14, 15, 15, 16,
	16, 16, 16, 16, 17, 17, 18, 18, 19, 19,
	19, 22, 22, 20, 20, 20, 23, 23, 24, 24,
	24, 24, 21, 21, 21, 25, 25, 25, 25, 25,
	25, 25, 25, 25, 26, 26, 26, 27, 27, 28,
	28, 28, 28, 29, 29, 30, 30, 31, 31, 31,
	31, 31, 32, 32, 32, 32, 32, 32, 32, 32,
	32, 32, 32, 32, 32, 32, 32, 32, 32, 32,
	32, 32, 33, 33, 33, 33, 33, 33, 33, 33,
	38, 38, 38, 43, 37, 39, 39, 36, 36, 36,
	36, 36, 36, 36, 36, 36, 36, 36, 36, 36,
	36, 36, 36, 36, 36, 36, 36, 36, 36, 42,
	42, 44, 44, 44, 74, 74, 74, 74, 74, 74,
	74, 74, 46, 49, 49, 47, 47, 48, 50, 50,
	45, 45, 35, 35, 35, 35, 35, 35, 51, 51,
	52, 52, 53, 53, 54, 54, 55, 56, 56, 56,
	76, 76, 76, 57, 57, 57, 57, 58, 58, 58,
	59, 59, 60, 60, 61, 61, 62, 62, 34, 34,
	40, 40, 41, 41, 63, 63, 64, 65, 65, 66,
	66, 67, 67, 68, 68, 68, 68, 68, 69, 69,
	70, 70, 71, 71, 72, 73,
}
var yyR2 = [...]int{

	0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 12, 4, 3, 8, 7, 9, 8,
	3, 5, 8, 4, 6, 7, 4, 5, 4, 5,
	5, 3, 2, 2, 2, 0, 2, 0, 2, 1,
	2, 1, 1, 1, 0, 1, 1, 3, 1, 2,
	3, 1, 1, 0, 1, 2, 1, 3, 3, 3,
	3, 5, 0, 1, 2, 1, 1, 2, 3, 2,
	3, 2, 2, 2, 1, 3, 1, 1, 3, 0,
	5, 5, 5, 1, 3, 0, 2, 1, 3, 3,
	2, 3, 3, 3, 4, 3, 3, 4, 5, 6,
	5, 5, 3, 4, 3, 4, 3, 4, 2, 2,
	6, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	3, 1, 1, 3, 3, 1, 3, 1, 1, 2,
	1, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	5, 2, 3, 2, 4, 5, 4, 6, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 5, 0, 1, 1, 2, 4, 0, 2,
	1, 3, 1, 1, 1, 1, 1, 1, 0, 3,
	0, 2, 0, 3, 1, 3, 2, 0, 2, 2,
	0, 2, 2, 0, 4, 4, 2, 0, 2, 4,
	0, 3, 1, 3, 0, 2, 0, 5, 2, 1,
	1, 3, 3, 1, 1, 3, 3, 0, 2, 0,
	3, 0, 1, 1, 1, 1, 1, 1, 0, 1,
	0, 1, 0, 2, 1, 0,
}
var yyChk = [...]int{

	-1000, -1, -2, -3, -4, -5, -6, -7, -8, -9,
	-10, -11, -12, -13, 5, 6, 7, 8, 39, 109,
	110, 112, 111, 113, 122, 123, 124, -16, 72, 73,
	74, 75, -14, -75, -14, -14, -14, -14, 114, -70,
	116, 120, -67, 116, 118, 114, 114, 115, 116, 114,
	-73, -73, -73, -3, 21, -17, 22, -15, 34, -27,
	44, 9, -63, -64, -45, -72, 44, -66, 119, 115,
	-72, 44, 114, -72, 44, -65, 119, -72, -65, 44,
	-18, -19, 96, -22, 44, -31, -36, -32, 90, 59,
	-35, -45, -41, -44, -72, -42, 64, -46, 24, 43,
	45, 46, 47, 30, 42, 41, -43, 94, 95, 63,
	119, 33, 104, 49, -27, 39, 102, -27, 76, 60,
	102, 44, 90, -72, -73, 44, -73, 117, 44, 24,
	87, -72, 9, 76, -20, -72, 23, 102, 89, 88,
	-33, 25, 90, 27, 28, 29, 26, 91, 92, 93,
	94, 95, 96, 97, 98, 58, 99, 60, 61, 62,
	52, 53, 54, 56, 57, -31, -36, -31, -3, -39,
	-36, -37, 100, -36, -45, 59, 59, -35, 59, 59,
	-43, 59, 59, -49, -36, -59, 39, 59, -63, 44,
	-30, 10, -64, -36, -72, -73, 24, -71, 121, -68,
	112, 110, 38, 111, 13, 44, 44, 44, -73, -23,
	-24, -26, 59, 44, -43, -19, -72, 96, -31, -36,
	-31, -36, -38, 59, -43, 48, 25, 28, 29, -36,
	-36, -36, 22, 30, 90, 42, 41, -36, -36, -36,
	-36, -36, -36, -36, -36, -72, 50, 125, 125, 76,
	125, -36, -36, 125, -18, 22, -18, -74, 65, 66,
	67, 68, 69, 70, 71, 45, -35, -47, -48, 105,
	-34, 33, -3, -63, -60, -45, -30, -53, 13, -31,
	87, -72, -73, -69, 117, -30, 76, -25, 77, 78,
	79, 80, 81, 83, 84, -21, 44, 23, -24, 102,
	-39, -38, -36, -36, 89, 9, 30, 42, 41, 51,
	-36, 101, 125, -18, 125, 9, 76, -50, -48, 107,
	-31, -62, 87, -40, -41, -62, 125, 76, -53, -57,
	18, 19, 14, 44, 44, -51, 11, -24, -24, 77,
	82, 77, 82, 77, 77, 77, -28, 85, 118, 86,
	44, 125, 44, 125, 89, -36, 42, 41, -36, 125,
	-36, -35, 108, -36, 106, -61, 35, 36, 76, -45,
	-57, -61, -36, -36, -54, -55, -36, -73, -52, 12,
	14, 87, 77, 77, 115, 115, 115, -36, 125, 125,
	-36, -18, 37, -41, -61, 19, 18, 76, -56, 31,
	32, -53, -31, -39, -31, 59, 59, 59, 7, -36,
	-36, -55, -76, 15, -76, -57, -29, -72, -29, -29,
	-63, 16, 17, -58, 20, 40, 125, 76, 125, 125,
	7, 25, -72, -72, -72,
}
var yyDef = [...]int{

	0, -2, 1, 2, 3, 4, 5, 6, 7, 8,
	9, 10, 11, 12, 35, 35, 35, 35, 35, 230,
	221, 0, 0, 0, 235, 235, 235, 0, 39, 41,
	42, 43, 44, 37, 0, 0, 0, 0, 219, 0,
	0, 231, 0, 0, 222, 0, 217, 0, 217, 0,
	32, 33, 34, 15, 40, 0, 45, 36, 0, 0,
	77, 0, 20, 214, 0, 170, 234, 0, 0, 0,
	235, 234, 0, 235, 0, 0, 0, 0, 0, 31,
	14, 46, 48, 53, 234, 51, 52, 87, 0, 0,
	127, -2, 130, 0, 170, 0, 0, 148, 0, 0,
	172, 173, 174, 175, 176, 177, 213, 151, 152, 153,
	149, 150, 163, 38, 200, 0, 0, 85, 0, 0,
	0, 235, 0, 232, 23, 0, 26, 0, 28, 218,
	0, 235, 0, 0, 49, 54, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 112, 113, 114,
	115, 116, 117, 118, 119, 90, 108, 0, 0, 0,
	125, 129, 0, 141, 128, 0, 0, 143, 0, 0,
	109, 0, 0, 0, 164, 0, 0, 0, 85, 78,
	182, 0, 215, 216, 171, 21, 220, 0, 0, 235,
	228, 223, 224, 225, 226, 227, 27, 29, 30, 85,
	56, 62, 0, 74, 76, 47, 55, 50, 88, 0,
	89, 92, 93, 0, 121, 122, 0, 0, 0, 95,
	96, 0, 0, 102, 0, 104, 106, 131, 132, 133,
	134, 135, 136, 137, 138, 139, 0, 91, 123, 0,
	212, 0, 125, 142, 0, 0, 0, 0, 154, 155,
	156, 157, 158, 159, 160, 161, 0, 168, 165, 0,
	206, 0, 209, 206, 0, 202, 182, 193, 0, 86,
	0, 233, 24, 0, 229, 178, 0, 0, 65, 66,
	0, 0, 0, 0, 0, 79, 63, 0, 0, 0,
	0, 94, 97, 0, 0, 0, 103, 105, 107, 0,
	126, 124, 144, 0, 146, 0, 0, 0, 166, 0,
	0, 204, 0, 208, 210, 17, 201, 0, 193, 204,
	0, 0, 0, 235, 25, 180, 0, 57, 60, 67,
	0, 69, 0, 71, 72, 73, 58, 0, 0, 0,
	64, 59, 75, 120, 0, 98, 100, 101, 140, 145,
	0, 0, 162, 169, 0, 16, 0, 0, 0, 203,
	204, 19, 196, 0, 183, 184, 187, 22, 182, 0,
	0, 0, 68, 70, 0, 0, 0, 99, 147, 110,
	167, 205, 0, 211, 18, 0, 0, 0, 186, 190,
	190, 193, 181, 179, 61, 0, 0, 0, 0, 194,
	195, 185, 188, 0, 189, 197, 0, 83, 0, 0,
	207, 191, 192, 13, 0, 0, 80, 0, 81, 82,
	198, 0, 84, 0, 199,
}
var yyTok1 = [...]int{

	1, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 98, 91, 3,
	59, 125, 96, 94, 76, 95, 102, 97, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	61, 60, 62, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 100, 3, 101, 93, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 92, 3, 63,
}
var yyTok2 = [...]int{

	2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
	12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
	22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
	32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
	42, 43, 44, 45, 46, 47, 48, 49, 50, 51,
	52, 53, 54, 55, 56, 57, 58, 64, 65, 66,
	67, 68, 69, 70, 71, 72, 73, 74, 75, 77,
	78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
	88, 89, 90, 99, 103, 104, 105, 106, 107, 108,
	109, 110, 111, 112, 113, 114, 115, 116, 117, 118,
	119, 120, 121, 122, 123, 124,
}
var yyTok3 = [...]int{
	0,
}

var yyErrorMessages = [...]struct {
	state int
	token int
	msg   string
}{}

//line yaccpar:1

/*	parser for yacc output	*/

var (
	yyDebug        = 0
	yyErrorVerbose = false
)

type yyLexer interface {
	Lex(lval *yySymType) int
	Error(s string)
}

type yyParser interface {
	Parse(yyLexer) int
	Lookahead() int
}

type yyParserImpl struct {
	lval  yySymType
	stack [yyInitialStackSize]yySymType
	char  int
}

func (p *yyParserImpl) Lookahead() int {
	return p.char
}

func yyNewParser() yyParser {
	return &yyParserImpl{}
}

const yyFlag = -1000

func yyTokname(c int) string {
	if c >= 1 && c-1 < len(yyToknames) {
		if yyToknames[c-1] != "" {
			return yyToknames[c-1]
		}
	}
	return __yyfmt__.Sprintf("tok-%v", c)
}

func yyStatname(s int) string {
	if s >= 0 && s < len(yyStatenames) {
		if yyStatenames[s] != "" {
			return yyStatenames[s]
		}
	}
	return __yyfmt__.Sprintf("state-%v", s)
}

func yyErrorMessage(state, lookAhead int) string {
	const TOKSTART = 4

	if !yyErrorVerbose {
		return "syntax error"
	}

	for _, e := range yyErrorMessages {
		if e.state == state && e.token == lookAhead {
			return "syntax error: " + e.msg
		}
	}

	res := "syntax error: unexpected " + yyTokname(lookAhead)

	// To match Bison, suggest at most four expected tokens.
	expected := make([]int, 0, 4)

	// Look for shiftable tokens.
	base := yyPact[state]
	for tok := TOKSTART; tok-1 < len(yyToknames); tok++ {
		if n := base + tok; n >= 0 && n < yyLast && yyChk[yyAct[n]] == tok {
			if len(expected) == cap(expected) {
				return res
			}
			expected = append(expected, tok)
		}
	}

	if yyDef[state] == -2 {
		i := 0
		for yyExca[i] != -1 || yyExca[i+1] != state {
			i += 2
		}

		// Look for tokens that we accept or reduce.
		for i += 2; yyExca[i] >= 0; i += 2 {
			tok := yyExca[i]
			if tok < TOKSTART || yyExca[i+1] == 0 {
				continue
			}
			if len(expected) == cap(expected) {
				return res
			}
			expected = append(expected, tok)
		}

		// If the default action is to accept or reduce, give up.
		if yyExca[i+1] != 0 {
			return res
		}
	}

	for i, tok := range expected {
		if i == 0 {
			res += ", expecting "
		} else {
			res += " or "
		}
		res += yyTokname(tok)
	}
	return res
}

func yylex1(lex yyLexer, lval *yySymType) (char, token int) {
	token = 0
	char = lex.Lex(lval)
	if char <= 0 {
		token = yyTok1[0]
		goto out
	}
	if char < len(yyTok1) {
		token = yyTok1[char]
		goto out
	}
	if char >= yyPrivate {
		if char < yyPrivate+len(yyTok2) {
			token = yyTok2[char-yyPrivate]
			goto out
		}
	}
	for i := 0; i < len(yyTok3); i += 2 {
		token = yyTok3[i+0]
		if token == char {
			token = yyTok3[i+1]
			goto out
		}
	}

out:
	if token == 0 {
		token = yyTok2[1] /* unknown char */
	}
	if yyDebug >= 3 {
		__yyfmt__.Printf("lex %s(%d)\n", yyTokname(token), uint(char))
	}
	return char, token
}

func yyParse(yylex yyLexer) int {
	return yyNewParser().Parse(yylex)
}

func (yyrcvr *yyParserImpl) Parse(yylex yyLexer) int {
	var yyn int
	var yyVAL yySymType
	var yyDollar []yySymType
	_ = yyDollar // silence set and not used
	yyS := yyrcvr.stack[:]

	Nerrs := 0   /* number of errors */
	Errflag := 0 /* error recovery flag */
	yystate := 0
	yyrcvr.char = -1
	yytoken := -1 // yyrcvr.char translated into internal numbering
	defer func() {
		// Make sure we report no lookahead when not parsing.
		yystate = -1
		yyrcvr.char = -1
		yytoken = -1
	}()
	yyp := -1
	goto yystack

ret0:
	return 0

ret1:
	return 1

yystack:
	/* put a state and value onto the stack */
	if yyDebug >= 4 {
		__yyfmt__.Printf("char %v in %v\n", yyTokname(yytoken), yyStatname(yystate))
	}

	yyp++
	if yyp >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyS[yyp] = yyVAL
	yyS[yyp].yys = yystate

yynewstate:
	yyn = yyPact[yystate]
	if yyn <= yyFlag {
		goto yydefault /* simple state */
	}
	if yyrcvr.char < 0 {
		yyrcvr.char, yytoken = yylex1(yylex, &yyrcvr.lval)
	}
	yyn += yytoken
	if yyn < 0 || yyn >= yyLast {
		goto yydefault
	}
	yyn = yyAct[yyn]
	if yyChk[yyn] == yytoken { /* valid shift */
		yyrcvr.char = -1
		yytoken = -1
		yyVAL = yyrcvr.lval
		yystate = yyn
		if Errflag > 0 {
			Errflag--
		}
		goto yystack
	}

yydefault:
	/* default state action */
	yyn = yyDef[yystate]
	if yyn == -2 {
		if yyrcvr.char < 0 {
			yyrcvr.char, yytoken = yylex1(yylex, &yyrcvr.lval)
		}

		/* look through exception table */
		xi := 0
		for {
			if yyExca[xi+0] == -1 && yyExca[xi+1] == yystate {
				break
			}
			xi += 2
		}
		for xi += 2; ; xi += 2 {
			yyn = yyExca[xi+0]
			if yyn < 0 || yyn == yytoken {
				break
			}
		}
		yyn = yyExca[xi+1]
		if yyn < 0 {
			goto ret0
		}
	}
	if yyn == 0 {
		/* error ... attempt to resume parsing */
		switch Errflag {
		case 0: /* brand new error */
			yylex.Error(yyErrorMessage(yystate, yytoken))
			Nerrs++
			if yyDebug >= 1 {
				__yyfmt__.Printf("%s", yyStatname(yystate))
				__yyfmt__.Printf(" saw %s\n", yyTokname(yytoken))
			}
			fallthrough

		case 1, 2: /* incompletely recovered error ... try again */
			Errflag = 3

			/* find a state where "error" is a legal shift action */
			for yyp >= 0 {
				yyn = yyPact[yyS[yyp].yys] + yyErrCode
				if yyn >= 0 && yyn < yyLast {
					yystate = yyAct[yyn] /* simulate a shift of "error" */
					if yyChk[yystate] == yyErrCode {
						goto yystack
					}
				}

				/* the current p has no shift on "error", pop stack */
				if yyDebug >= 2 {
					__yyfmt__.Printf("error recovery pops state %d\n", yyS[yyp].yys)
				}
				yyp--
			}
			/* there is no state on the stack with an error shift ... abort */
			goto ret1

		case 3: /* no shift yet; clobber input char */
			if yyDebug >= 2 {
				__yyfmt__.Printf("error recovery discards %s\n", yyTokname(yytoken))
			}
			if yytoken == yyEofCode {
				goto ret1
			}
			yyrcvr.char = -1
			yytoken = -1
			goto yynewstate /* try again in the same state */
		}
	}

	/* reduction by production yyn */
	if yyDebug >= 2 {
		__yyfmt__.Printf("reduce %v in:\n\t%v\n", yyn, yyStatname(yystate))
	}

	yynt := yyn
	yypt := yyp
	_ = yypt // guard against "declared and not used"

	yyp -= yyR2[yyn]
	// yyp is now the index of $0. Perform the default action. Iff the
	// reduced production is ε, $1 is possibly out of range.
	if yyp+1 >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyVAL = yyS[yyp+1]

	/* consult goto table to find next state */
	yyn = yyR1[yyn]
	yyg := yyPgo[yyn]
	yyj := yyg + yyS[yyp].yys + 1

	if yyj >= yyLast {
		yystate = yyAct[yyg]
	} else {
		yystate = yyAct[yyj]
		if yyChk[yystate] != -yyn {
			yystate = yyAct[yyg]
		}
	}
	// dummy call; replaced with literal code
	switch yynt {

	case 1:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:159
		{
			SetParseTree(yylex, yyDollar[1].statement)
		}
	case 2:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:165
		{
			yyVAL.statement = yyDollar[1].selStmt
		}
	case 13:
		yyDollar = yyS[yypt-12 : yypt+1]
		//line sql.y:181
		{
			yyVAL.selStmt = &Select{Comments: Comments(yyDollar[2].bytes2), Distinct: yyDollar[3].str, SelectExprs: yyDollar[4].selectExprs, From: yyDollar[6].tableExprs, Where: NewWhere(AST_WHERE, yyDollar[7].boolExpr), GroupBy: GroupBy(yyDollar[8].valExprs), Having: NewWhere(AST_HAVING, yyDollar[9].boolExpr), OrderBy: yyDollar[10].orderBy, Limit: yyDollar[11].limit, Lock: yyDollar[12].str}
		}
	case 14:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:185
		{
			yyVAL.selStmt = &Select{Comments: Comments(yyDollar[2].bytes2), Distinct: yyDollar[3].str, SelectExprs: yyDollar[4].selectExprs, From: nil, Where: nil, GroupBy: nil, Having: nil, OrderBy: nil, Limit: nil, Lock: ""}
		}
	case 15:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:189
		{
			yyVAL.selStmt = &Union{Type: yyDollar[2].str, Left: yyDollar[1].selStmt, Right: yyDollar[3].selStmt}
		}
	case 16:
		yyDollar = yyS[yypt-8 : yypt+1]
		//line sql.y:195
		{
			yyVAL.statement = &Insert{Comments: Comments(yyDollar[2].bytes2), Table: yyDollar[4].tableName, Columns: yyDollar[5].columns, Rows: yyDollar[6].insRows, OnDup: OnDup(yyDollar[7].updateExprs), Returning: Returning(yyDollar[8].selectExprs)}
		}
	case 17:
		yyDollar = yyS[yypt-7 : yypt+1]
		//line sql.y:199
		{
			cols := make(Columns, 0, len(yyDollar[6].updateExprs))
			vals := make(ValTuple, 0, len(yyDollar[6].updateExprs))
			for _, col := range yyDollar[6].updateExprs {
				cols = append(cols, &NonStarExpr{Expr: col.Name})
				vals = append(vals, col.Expr)
			}
			yyVAL.statement = &Insert{Comments: Comments(yyDollar[2].bytes2), Table: yyDollar[4].tableName, Columns: cols, Rows: Values{vals}, OnDup: OnDup(yyDollar[7].updateExprs), Returning: nil}
		}
	case 18:
		yyDollar = yyS[yypt-9 : yypt+1]
		//line sql.y:211
		{
			yyVAL.statement = &Update{Comments: Comments(yyDollar[2].bytes2), Table: yyDollar[3].tableName, Exprs: yyDollar[5].updateExprs, Where: NewWhere(AST_WHERE, yyDollar[6].boolExpr), OrderBy: yyDollar[7].orderBy, Limit: yyDollar[8].limit, Returning: Returning(yyDollar[9].selectExprs)}
		}
	case 19:
		yyDollar = yyS[yypt-8 : yypt+1]
		//line sql.y:217
		{
			yyVAL.statement = &Delete{Comments: Comments(yyDollar[2].bytes2), Table: yyDollar[4].tableName, Where: NewWhere(AST_WHERE, yyDollar[5].boolExpr), OrderBy: yyDollar[6].orderBy, Limit: yyDollar[7].limit, Returning: Returning(yyDollar[8].selectExprs)}
		}
	case 20:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:223
		{
			yyVAL.statement = &Set{Comments: Comments(yyDollar[2].bytes2), Exprs: yyDollar[3].updateExprs}
		}
	case 21:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:229
		{
			yyVAL.statement = &DDL{Action: AST_CREATE, NewName: yyDollar[4].bytes}
		}
	case 22:
		yyDollar = yyS[yypt-8 : yypt+1]
		//line sql.y:233
		{
			// Change this to an alter statement
			yyVAL.statement = &DDL{Action: AST_ALTER, Table: yyDollar[7].bytes, NewName: yyDollar[7].bytes}
		}
	case 23:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:238
		{
			yyVAL.statement = &DDL{Action: AST_CREATE, NewName: yyDollar[3].bytes}
		}
	case 24:
		yyDollar = yyS[yypt-6 : yypt+1]
		//line sql.y:244
		{
			yyVAL.statement = &DDL{Action: AST_ALTER, Table: yyDollar[4].bytes, NewName: yyDollar[4].bytes}
		}
	case 25:
		yyDollar = yyS[yypt-7 : yypt+1]
		//line sql.y:248
		{
			// Change this to a rename statement
			yyVAL.statement = &DDL{Action: AST_RENAME, Table: yyDollar[4].bytes, NewName: yyDollar[7].bytes}
		}
	case 26:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:253
		{
			yyVAL.statement = &DDL{Action: AST_ALTER, Table: yyDollar[3].bytes, NewName: yyDollar[3].bytes}
		}
	case 27:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:259
		{
			yyVAL.statement = &DDL{Action: AST_RENAME, Table: yyDollar[3].bytes, NewName: yyDollar[5].bytes}
		}
	case 28:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:265
		{
			yyVAL.statement = &DDL{Action: AST_DROP, Table: yyDollar[4].bytes}
		}
	case 29:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:269
		{
			// Change this to an alter statement
			yyVAL.statement = &DDL{Action: AST_ALTER, Table: yyDollar[5].bytes, NewName: yyDollar[5].bytes}
		}
	case 30:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:274
		{
			yyVAL.statement = &DDL{Action: AST_DROP, Table: yyDollar[4].bytes}
		}
	case 31:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:280
		{
			yyVAL.statement = &DDL{Action: AST_ALTER, Table: yyDollar[3].bytes, NewName: yyDollar[3].bytes}
		}
	case 32:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:286
		{
			yyVAL.statement = &Other{}
		}
	case 33:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:290
		{
			yyVAL.statement = &Other{}
		}
	case 34:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:294
		{
			yyVAL.statement = &Other{}
		}
	case 35:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:299
		{
			SetAllowComments(yylex, true)
		}
	case 36:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:303
		{
			yyVAL.bytes2 = yyDollar[2].bytes2
			SetAllowComments(yylex, false)
		}
	case 37:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:309
		{
			yyVAL.bytes2 = nil
		}
	case 38:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:313
		{
			yyVAL.bytes2 = append(yyDollar[1].bytes2, yyDollar[2].bytes)
		}
	case 39:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:319
		{
			yyVAL.str = AST_UNION
		}
	case 40:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:323
		{
			yyVAL.str = AST_UNION_ALL
		}
	case 41:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:327
		{
			yyVAL.str = AST_SET_MINUS
		}
	case 42:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:331
		{
			yyVAL.str = AST_EXCEPT
		}
	case 43:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:335
		{
			yyVAL.str = AST_INTERSECT
		}
	case 44:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:340
		{
			yyVAL.str = ""
		}
	case 45:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:344
		{
			yyVAL.str = AST_DISTINCT
		}
	case 46:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:350
		{
			yyVAL.selectExprs = SelectExprs{yyDollar[1].selectExpr}
		}
	case 47:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:354
		{
			yyVAL.selectExprs = append(yyVAL.selectExprs, yyDollar[3].selectExpr)
		}
	case 48:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:360
		{
			yyVAL.selectExpr = &StarExpr{}
		}
	case 49:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:364
		{
			yyVAL.selectExpr = &NonStarExpr{Expr: yyDollar[1].expr, As: yyDollar[2].bytes}
		}
	case 50:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:368
		{
			yyVAL.selectExpr = &StarExpr{TableName: yyDollar[1].bytes}
		}
	case 51:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:374
		{
			yyVAL.expr = yyDollar[1].boolExpr
		}
	case 52:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:378
		{
			yyVAL.expr = yyDollar[1].valExpr
		}
	case 53:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:383
		{
			yyVAL.bytes = nil
		}
	case 54:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:387
		{
			yyVAL.bytes = yyDollar[1].bytes
		}
	case 55:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:391
		{
			yyVAL.bytes = yyDollar[2].bytes
		}
	case 56:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:397
		{
			yyVAL.tableExprs = TableExprs{yyDollar[1].tableExpr}
		}
	case 57:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:401
		{
			yyVAL.tableExprs = append(yyVAL.tableExprs, yyDollar[3].tableExpr)
		}
	case 58:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:407
		{
			yyVAL.tableExpr = &AliasedTableExpr{Expr: yyDollar[1].smTableExpr, As: yyDollar[2].bytes, Hints: yyDollar[3].indexHints}
		}
	case 59:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:411
		{
			yyVAL.tableExpr = &ParenTableExpr{Expr: yyDollar[2].tableExpr}
		}
	case 60:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:415
		{
			yyVAL.tableExpr = &JoinTableExpr{LeftExpr: yyDollar[1].tableExpr, Join: yyDollar[2].str, RightExpr: yyDollar[3].tableExpr}
		}
	case 61:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:419
		{
			yyVAL.tableExpr = &JoinTableExpr{LeftExpr: yyDollar[1].tableExpr, Join: yyDollar[2].str, RightExpr: yyDollar[3].tableExpr, On: yyDollar[5].boolExpr}
		}
	case 62:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:424
		{
			yyVAL.bytes = nil
		}
	case 63:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:428
		{
			yyVAL.bytes = yyDollar[1].bytes
		}
	case 64:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:432
		{
			yyVAL.bytes = yyDollar[2].bytes
		}
	case 65:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:438
		{
			yyVAL.str = AST_JOIN
		}
	case 66:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:442
		{
			yyVAL.str = AST_STRAIGHT_JOIN
		}
	case 67:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:446
		{
			yyVAL.str = AST_LEFT_JOIN
		}
	case 68:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:450
		{
			yyVAL.str = AST_LEFT_JOIN
		}
	case 69:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:454
		{
			yyVAL.str = AST_RIGHT_JOIN
		}
	case 70:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:458
		{
			yyVAL.str = AST_RIGHT_JOIN
		}
	case 71:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:462
		{
			yyVAL.str = AST_JOIN
		}
	case 72:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:466
		{
			yyVAL.str = AST_CROSS_JOIN
		}
	case 73:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:470
		{
			yyVAL.str = AST_NATURAL_JOIN
		}
	case 74:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:476
		{
			yyVAL.smTableExpr = &TableName{Name: yyDollar[1].bytes}
		}
	case 75:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:480
		{
			yyVAL.smTableExpr = &TableName{Qualifier: yyDollar[1].bytes, Name: yyDollar[3].bytes}
		}
	case 76:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:484
		{
			yyVAL.smTableExpr = yyDollar[1].subquery
		}
	case 77:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:490
		{
			yyVAL.tableName = &TableName{Name: yyDollar[1].bytes}
		}
	case 78:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:494
		{
			yyVAL.tableName = &TableName{Qualifier: yyDollar[1].bytes, Name: yyDollar[3].bytes}
		}
	case 79:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:499
		{
			yyVAL.indexHints = nil
		}
	case 80:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:503
		{
			yyVAL.indexHints = &IndexHints{Type: AST_USE, Indexes: yyDollar[4].bytes2}
		}
	case 81:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:507
		{
			yyVAL.indexHints = &IndexHints{Type: AST_IGNORE, Indexes: yyDollar[4].bytes2}
		}
	case 82:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:511
		{
			yyVAL.indexHints = &IndexHints{Type: AST_FORCE, Indexes: yyDollar[4].bytes2}
		}
	case 83:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:517
		{
			yyVAL.bytes2 = [][]byte{yyDollar[1].bytes}
		}
	case 84:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:521
		{
			yyVAL.bytes2 = append(yyDollar[1].bytes2, yyDollar[3].bytes)
		}
	case 85:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:526
		{
			yyVAL.boolExpr = nil
		}
	case 86:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:530
		{
			yyVAL.boolExpr = yyDollar[2].boolExpr
		}
	case 88:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:537
		{
			yyVAL.boolExpr = &AndExpr{Left: yyDollar[1].boolExpr, Right: yyDollar[3].boolExpr}
		}
	case 89:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:541
		{
			yyVAL.boolExpr = &OrExpr{Left: yyDollar[1].boolExpr, Right: yyDollar[3].boolExpr}
		}
	case 90:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:545
		{
			yyVAL.boolExpr = &NotExpr{Expr: yyDollar[2].boolExpr}
		}
	case 91:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:549
		{
			yyVAL.boolExpr = &ParenBoolExpr{Expr: yyDollar[2].boolExpr}
		}
	case 92:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:555
		{
			yyVAL.boolExpr = &ComparisonExpr{Left: yyDollar[1].valExpr, Operator: yyDollar[2].str, Right: yyDollar[3].valExpr}
		}
	case 93:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:559
		{
			yyVAL.boolExpr = &ComparisonExpr{Left: yyDollar[1].valExpr, Operator: AST_IN, Right: yyDollar[3].colTuple}
		}
	case 94:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:563
		{
			yyVAL.boolExpr = &ComparisonExpr{Left: yyDollar[1].valExpr, Operator: AST_NOT_IN, Right: yyDollar[4].colTuple}
		}
	case 95:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:567
		{
			yyVAL.boolExpr = &ComparisonExpr{Left: yyDollar[1].valExpr, Operator: AST_ILIKE, Right: yyDollar[3].valExpr}
		}
	case 96:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:571
		{
			yyVAL.boolExpr = &ComparisonExpr{Left: yyDollar[1].valExpr, Operator: AST_LIKE, Right: yyDollar[3].valExpr}
		}
	case 97:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:575
		{
			yyVAL.boolExpr = &ComparisonExpr{Left: yyDollar[1].valExpr, Operator: AST_NOT_LIKE, Right: yyDollar[4].valExpr}
		}
	case 98:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:579
		{
			yyVAL.boolExpr = &RangeCond{Left: yyDollar[1].valExpr, Operator: AST_BETWEEN, From: yyDollar[3].valExpr, To: yyDollar[5].valExpr}
		}
	case 99:
		yyDollar = yyS[yypt-6 : yypt+1]
		//line sql.y:583
		{
			yyVAL.boolExpr = &RangeCond{Left: yyDollar[1].valExpr, Operator: AST_NOT_BETWEEN, From: yyDollar[4].valExpr, To: yyDollar[6].valExpr}
		}
	case 100:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:587
		{
			// TODO(rhys): custom type
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 101:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:592
		{
			// XXX AGB: custom type. 2015-08-11
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 102:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:597
		{
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 103:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:601
		{
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NOT_NULL, Expr: yyDollar[1].valExpr}
		}
	case 104:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:605
		{
			// TODO(rhys): custom type
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 105:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:610
		{
			// TODO(rhys): custom type
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 106:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:615
		{
			// TODO(rhys): custom type
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 107:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:620
		{
			// TODO(rhys): custom type
			yyVAL.boolExpr = &NullCheck{Operator: AST_IS_NULL, Expr: yyDollar[1].valExpr}
		}
	case 108:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:625
		{
			yyVAL.boolExpr = &NullCheck{Operator: AST_NOT, Expr: yyDollar[2].valExpr}
		}
	case 109:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:629
		{
			yyVAL.boolExpr = &ExistsExpr{Subquery: yyDollar[2].subquery}
		}
	case 110:
		yyDollar = yyS[yypt-6 : yypt+1]
		//line sql.y:633
		{
			yyVAL.boolExpr = &KeyrangeExpr{Start: yyDollar[3].valExpr, End: yyDollar[5].valExpr}
		}
	case 111:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:637
		{
			yyVAL.boolExpr = yyDollar[1].colName
		}
	case 112:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:643
		{
			yyVAL.str = AST_EQ
		}
	case 113:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:647
		{
			yyVAL.str = AST_LT
		}
	case 114:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:651
		{
			yyVAL.str = AST_GT
		}
	case 115:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:655
		{
			yyVAL.str = AST_LE
		}
	case 116:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:659
		{
			yyVAL.str = AST_GE
		}
	case 117:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:663
		{
			yyVAL.str = AST_NE
		}
	case 118:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:667
		{
			yyVAL.str = AST_NSE
		}
	case 119:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:671
		{
			yyVAL.str = AST_NSIM
		}
	case 120:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:677
		{
			yyVAL.colTuple = ValTuple(yyDollar[2].valExprs)
		}
	case 121:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:681
		{
			yyVAL.colTuple = yyDollar[1].subquery
		}
	case 122:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:685
		{
			yyVAL.colTuple = ListArg(yyDollar[1].bytes)
		}
	case 123:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:691
		{
			yyVAL.subquery = &Subquery{yyDollar[2].selStmt}
		}
	case 124:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:697
		{
			yyVAL.valExpr = yyDollar[2].valExpr
		}
	case 125:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:703
		{
			yyVAL.valExprs = ValExprs{yyDollar[1].valExpr}
		}
	case 126:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:707
		{
			yyVAL.valExprs = append(yyDollar[1].valExprs, yyDollar[3].valExpr)
		}
	case 127:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:713
		{
			yyVAL.valExpr = yyDollar[1].valExpr
		}
	case 128:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:717
		{
			yyVAL.valExpr = yyDollar[1].colName
		}
	case 129:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:721
		{
			yyVAL.valExpr = &IndexedExpr{Expr: yyDollar[1].colName, Index: yyDollar[2].valExpr}
		}
	case 130:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:725
		{
			yyVAL.valExpr = yyDollar[1].rowTuple
		}
	case 131:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:729
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_BITAND, Right: yyDollar[3].valExpr}
		}
	case 132:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:733
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_BITOR, Right: yyDollar[3].valExpr}
		}
	case 133:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:737
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_BITXOR, Right: yyDollar[3].valExpr}
		}
	case 134:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:741
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_PLUS, Right: yyDollar[3].valExpr}
		}
	case 135:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:745
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_MINUS, Right: yyDollar[3].valExpr}
		}
	case 136:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:749
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_MULT, Right: yyDollar[3].valExpr}
		}
	case 137:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:753
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_DIV, Right: yyDollar[3].valExpr}
		}
	case 138:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:757
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_MOD, Right: yyDollar[3].valExpr}
		}
	case 139:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:761
		{
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_TYPECAST, Right: StrVal(yyDollar[3].bytes)}
		}
	case 140:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:765
		{
			// TODO(rhys): custom type
			yyVAL.valExpr = &BinaryExpr{Left: yyDollar[1].valExpr, Operator: AST_MOD, Right: yyDollar[5].valExpr}
		}
	case 141:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:770
		{
			if num, ok := yyDollar[2].valExpr.(NumVal); ok {
				switch yyDollar[1].byt {
				case '-':
					yyVAL.valExpr = append(NumVal("-"), num...)
				case '+':
					yyVAL.valExpr = num
				default:
					yyVAL.valExpr = &UnaryExpr{Operator: yyDollar[1].byt, Expr: yyDollar[2].valExpr}
				}
			} else {
				yyVAL.valExpr = &UnaryExpr{Operator: yyDollar[1].byt, Expr: yyDollar[2].valExpr}
			}
		}
	case 142:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:785
		{
			yyVAL.valExpr = &FuncExpr{Name: yyDollar[1].bytes}
		}
	case 143:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:789
		{
			yyVAL.valExpr = &FuncExpr{Name: yyDollar[1].bytes, Exprs: SelectExprs{&NonStarExpr{Expr: yyDollar[2].valExpr}}}
		}
	case 144:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:793
		{
			yyVAL.valExpr = &FuncExpr{Name: yyDollar[1].bytes, Exprs: yyDollar[3].selectExprs}
		}
	case 145:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:797
		{
			yyVAL.valExpr = &FuncExpr{Name: yyDollar[1].bytes, Distinct: true, Exprs: yyDollar[4].selectExprs}
		}
	case 146:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:801
		{
			yyVAL.valExpr = &FuncExpr{Name: yyDollar[1].bytes, Exprs: yyDollar[3].selectExprs}
		}
	case 147:
		yyDollar = yyS[yypt-6 : yypt+1]
		//line sql.y:805
		{
			yyVAL.valExpr = &ExtractFuncExpr{Arg: yyDollar[3].bytes, Expr: yyDollar[5].valExpr}
		}
	case 148:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:809
		{
			yyVAL.valExpr = yyDollar[1].caseExpr
		}
	case 149:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:815
		{
			yyVAL.bytes = IF_BYTES
		}
	case 150:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:819
		{
			yyVAL.bytes = VALUES_BYTES
		}
	case 151:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:825
		{
			yyVAL.byt = AST_UPLUS
		}
	case 152:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:829
		{
			yyVAL.byt = AST_UMINUS
		}
	case 153:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:833
		{
			yyVAL.byt = AST_TILDA
		}
	case 154:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:838
		{
			yyVAL.bytes = []byte("epoch")
		}
	case 155:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:839
		{
			yyVAL.bytes = []byte("year")
		}
	case 156:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:840
		{
			yyVAL.bytes = []byte("month")
		}
	case 157:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:841
		{
			yyVAL.bytes = []byte("day")
		}
	case 158:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:842
		{
			yyVAL.bytes = []byte("hour")
		}
	case 159:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:843
		{
			yyVAL.bytes = []byte("minute")
		}
	case 160:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:844
		{
			yyVAL.bytes = []byte("second")
		}
	case 161:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:845
		{
			yyVAL.bytes = yyDollar[1].bytes
		}
	case 162:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:849
		{
			yyVAL.caseExpr = &CaseExpr{Expr: yyDollar[2].valExpr, Whens: yyDollar[3].whens, Else: yyDollar[4].valExpr}
		}
	case 163:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:854
		{
			yyVAL.valExpr = nil
		}
	case 164:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:858
		{
			yyVAL.valExpr = yyDollar[1].valExpr
		}
	case 165:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:864
		{
			yyVAL.whens = []*When{yyDollar[1].when}
		}
	case 166:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:868
		{
			yyVAL.whens = append(yyDollar[1].whens, yyDollar[2].when)
		}
	case 167:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:874
		{
			yyVAL.when = &When{Cond: yyDollar[2].boolExpr, Val: yyDollar[4].valExpr}
		}
	case 168:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:879
		{
			yyVAL.valExpr = nil
		}
	case 169:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:883
		{
			yyVAL.valExpr = yyDollar[2].valExpr
		}
	case 170:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:889
		{
			yyVAL.colName = &ColName{Name: yyDollar[1].bytes}
		}
	case 171:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:893
		{
			yyVAL.colName = &ColName{Qualifier: yyDollar[1].bytes, Name: yyDollar[3].bytes}
		}
	case 172:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:899
		{
			yyVAL.valExpr = StrVal(yyDollar[1].bytes)
		}
	case 173:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:903
		{
			yyVAL.valExpr = NumVal(yyDollar[1].bytes)
		}
	case 174:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:907
		{
			yyVAL.valExpr = ValArg(yyDollar[1].bytes)
		}
	case 175:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:911
		{
			yyVAL.valExpr = &NullVal{}
		}
	case 176:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:915
		{
			// TODO(rhys): custom type
			yyVAL.valExpr = &NullVal{}
		}
	case 177:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:920
		{
			// TODO(rhys): custom type
			yyVAL.valExpr = &NullVal{}
		}
	case 178:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:926
		{
			yyVAL.valExprs = nil
		}
	case 179:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:930
		{
			yyVAL.valExprs = yyDollar[3].valExprs
		}
	case 180:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:935
		{
			yyVAL.boolExpr = nil
		}
	case 181:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:939
		{
			yyVAL.boolExpr = yyDollar[2].boolExpr
		}
	case 182:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:944
		{
			yyVAL.orderBy = nil
		}
	case 183:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:948
		{
			yyVAL.orderBy = yyDollar[3].orderBy
		}
	case 184:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:954
		{
			yyVAL.orderBy = OrderBy{yyDollar[1].order}
		}
	case 185:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:958
		{
			yyVAL.orderBy = append(yyDollar[1].orderBy, yyDollar[3].order)
		}
	case 186:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:964
		{
			yyVAL.order = &Order{Expr: yyDollar[1].valExpr, Direction: yyDollar[2].str}
		}
	case 187:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:969
		{
			yyVAL.str = AST_ASC
		}
	case 188:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:973
		{
			// TODO(rhys): custom type
			yyVAL.str = AST_ASC
		}
	case 189:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:978
		{
			// TODO(rhys): custom type
			yyVAL.str = AST_DESC
		}
	case 193:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:988
		{
			yyVAL.limit = nil
		}
	case 194:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:992
		{
			yyVAL.limit = &Limit{Offset: yyDollar[4].valExpr, Rowcount: yyDollar[2].valExpr}
		}
	case 195:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:996
		{
			yyVAL.limit = &Limit{Offset: yyDollar[2].valExpr, Rowcount: yyDollar[4].valExpr}
		}
	case 196:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:1000
		{
			yyVAL.limit = &Limit{Rowcount: yyDollar[2].valExpr}
		}
	case 197:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1005
		{
			yyVAL.str = ""
		}
	case 198:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:1009
		{
			yyVAL.str = AST_FOR_UPDATE
		}
	case 199:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line sql.y:1013
		{
			if !bytes.Equal(yyDollar[3].bytes, SHARE) {
				yylex.Error("expecting share")
				return 1
			}
			if !bytes.Equal(yyDollar[4].bytes, MODE) {
				yylex.Error("expecting mode")
				return 1
			}
			yyVAL.str = AST_SHARE_MODE
		}
	case 200:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1026
		{
			yyVAL.columns = nil
		}
	case 201:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1030
		{
			yyVAL.columns = yyDollar[2].columns
		}
	case 202:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1036
		{
			yyVAL.columns = Columns{&NonStarExpr{Expr: yyDollar[1].colName}}
		}
	case 203:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1040
		{
			yyVAL.columns = append(yyVAL.columns, &NonStarExpr{Expr: yyDollar[3].colName})
		}
	case 204:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1045
		{
			yyVAL.selectExprs = nil
		}
	case 205:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:1049
		{
			yyVAL.selectExprs = yyDollar[2].selectExprs
		}
	case 206:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1054
		{
			yyVAL.updateExprs = nil
		}
	case 207:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line sql.y:1058
		{
			yyVAL.updateExprs = yyDollar[5].updateExprs
		}
	case 208:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:1064
		{
			yyVAL.insRows = yyDollar[2].values
		}
	case 209:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1068
		{
			yyVAL.insRows = yyDollar[1].selStmt
		}
	case 210:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1074
		{
			yyVAL.values = Values{yyDollar[1].rowTuple}
		}
	case 211:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1078
		{
			yyVAL.values = append(yyDollar[1].values, yyDollar[3].rowTuple)
		}
	case 212:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1084
		{
			yyVAL.rowTuple = ValTuple(yyDollar[2].valExprs)
		}
	case 213:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1088
		{
			yyVAL.rowTuple = yyDollar[1].subquery
		}
	case 214:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1094
		{
			yyVAL.updateExprs = UpdateExprs{yyDollar[1].updateExpr}
		}
	case 215:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1098
		{
			yyVAL.updateExprs = append(yyDollar[1].updateExprs, yyDollar[3].updateExpr)
		}
	case 216:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1104
		{
			yyVAL.updateExpr = &UpdateExpr{Name: yyDollar[1].colName, Expr: yyDollar[3].valExpr}
		}
	case 217:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1109
		{
			yyVAL.empty = struct{}{}
		}
	case 218:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:1111
		{
			yyVAL.empty = struct{}{}
		}
	case 219:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1114
		{
			yyVAL.empty = struct{}{}
		}
	case 220:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line sql.y:1116
		{
			yyVAL.empty = struct{}{}
		}
	case 221:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1119
		{
			yyVAL.empty = struct{}{}
		}
	case 222:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1121
		{
			yyVAL.empty = struct{}{}
		}
	case 223:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1125
		{
			yyVAL.empty = struct{}{}
		}
	case 224:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1127
		{
			yyVAL.empty = struct{}{}
		}
	case 225:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1129
		{
			yyVAL.empty = struct{}{}
		}
	case 226:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1131
		{
			yyVAL.empty = struct{}{}
		}
	case 227:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1133
		{
			yyVAL.empty = struct{}{}
		}
	case 228:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1136
		{
			yyVAL.empty = struct{}{}
		}
	case 229:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1138
		{
			yyVAL.empty = struct{}{}
		}
	case 230:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1141
		{
			yyVAL.empty = struct{}{}
		}
	case 231:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1143
		{
			yyVAL.empty = struct{}{}
		}
	case 232:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1146
		{
			yyVAL.empty = struct{}{}
		}
	case 233:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line sql.y:1148
		{
			yyVAL.empty = struct{}{}
		}
	case 234:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line sql.y:1152
		{
			yyVAL.bytes = bytes.ToLower(yyDollar[1].bytes)
		}
	case 235:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line sql.y:1157
		{
			ForceEOF(yylex)
		}
	}
	goto yystack /* stack new state and value */
}
