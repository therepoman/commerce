package bininfo

import (
	"expvar"
	"runtime"

	"code.justin.tv/common/golibs/pkgpath"
)

type procInfo struct {
	GoVersion string `json:"go_version"`
	Revision  string `json:"revision"`
	SvcName   string `json:"svcname"`
}

func export() interface{} {
	return &procInfo{
		GoVersion: runtime.Version(),
		Revision:  revision,
		SvcName:   svcname,
	}
}

// init Responsible for publishing procInfo into expvar
func init() {
	name, ok := pkgpath.Caller(0)
	if !ok {
		return
	}
	expvar.Publish(name, expvar.Func(func() interface{} { return export() }))
}
