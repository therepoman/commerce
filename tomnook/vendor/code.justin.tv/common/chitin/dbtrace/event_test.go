package dbtrace

import "testing"

func TestTruncate(t *testing.T) {
	tests := []struct {
		in    string
		limit int
		out   string
	}{
		{in: "SELECT", limit: 7, out: "SELECT"},
		{in: "SELECT", limit: 6, out: "SELECT"},
		{in: "SELECT", limit: 5, out: "SELEC"},

		{in: "SELECT·", limit: 9, out: "SELECT·"},
		{in: "SELECT·", limit: 8, out: "SELECT·"},
		{in: "SELECT·", limit: 7, out: "SELECT"},
		{in: "SELECT·", limit: 6, out: "SELECT"},

		{in: "\x80\x80\x80", limit: 4, out: "\x80\x80\x80"},
		{in: "\x80\x80\x80", limit: 3, out: "\x80\x80\x80"},
		{in: "\x80\x80\x80", limit: 2, out: ""},
		{in: "\x80\x80\x80", limit: 1, out: ""},
		{in: "\x80\x80\x80", limit: 0, out: ""},
		{in: "\x80\x80\x80", limit: -1, out: ""},
	}

	for _, tt := range tests {
		out := truncate(tt.in, tt.limit)
		if have, want := out, tt.out; have != want {
			t.Errorf("truncate(%q, %d) = %q; want %q", tt.in, tt.limit, have, want)
		}
	}
}
