/*

Package tcptrace allows a service owner to emit trace events for protocols
based on raw TCP connections.

It should be used rarely and with care.

*/
package tcptrace

import (
	"code.justin.tv/common/chitin/internal/trace"
	"code.justin.tv/release/trace/common"
	"code.justin.tv/release/trace/events"
	"golang.org/x/net/context"
)

// BasicRequestHeadReceived emits a trace event to mark the receipt of a
// fixed-length request from a raw TCP client.
func BasicRequestHeadReceived(ctx context.Context) {
	trace.SendEvent(ctx, &events.Event{
		Kind:  common.Kind_REQUEST_HEAD_RECEIVED,
		Extra: nil,
	})
}

// BasicResponseHeadPrepared emits a trace event to indicate that a server is
// ready to send a response to a raw TCP client.
func BasicResponseHeadPrepared(ctx context.Context) {
	trace.SendEvent(ctx, &events.Event{
		Kind:  common.Kind_RESPONSE_HEAD_PREPARED,
		Extra: nil,
	})
}

// BasicResponseBodySent emits a trace event to indicate that a server has
// finished sending its response to a raw TCP client.
func BasicResponseBodySent(ctx context.Context) {
	trace.SendEvent(ctx, &events.Event{
		Kind:  common.Kind_RESPONSE_BODY_SENT,
		Extra: nil,
	})
}
