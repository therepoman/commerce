package grpctrace

import (
	"reflect"
	"testing"

	"code.justin.tv/common/chitin/internal/trace"
	"golang.org/x/net/context"
	"google.golang.org/grpc/metadata"
)

func TestMetadata(t *testing.T) {
	md := metadata.Pairs("chitin-txid", "4e61bc00000000000900000000000000.6.7.8")
	ctx := contextFromGRPCMetadata(context.Background(), md)

	tx, _ := trace.FromContext(ctx)
	if have, want := tx.TxID, [2]uint64{12345678, 9}; have != want {
		t.Errorf("transaction id; %d != %d", have, want)
	}
	if have, want := tx.Path, (trace.CallPath{6, 7, 8}); !reflect.DeepEqual(have, want) {
		t.Errorf("span id; %q != %q", have, want)
	}

	out1 := metadata.Pairs()
	augmentGRPCMetadata(ctx, out1)
	if have, want := out1["chitin-txid"][0], "4e61bc00000000000900000000000000.6.7.8"; have != want {
		t.Errorf("chitin-txid; %q != %q", have, want)
	}

	out2 := metadata.Pairs()
	child := ctx
	for i := 0; i < 24+1; i++ {
		child = trace.NewSpan(ctx)
	}
	augmentGRPCMetadata(child, out2)
	if have, want := out2["chitin-txid"][0], "4e61bc00000000000900000000000000.6.7.8.24"; have != want {
		t.Errorf("chitin-txid; %q != %q", have, want)
	}
}
