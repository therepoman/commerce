package grpctrace

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type stream struct {
	ctx    context.Context
	stream grpc.Stream

	recvErr func(error)
}

func (s *stream) Context() context.Context    { return s.ctx }
func (s *stream) SendMsg(m interface{}) error { return s.stream.SendMsg(m) }
func (s *stream) RecvMsg(m interface{}) error {
	err := s.stream.RecvMsg(m)
	if err != nil && s.recvErr != nil {
		s.recvErr(err)
	}
	return err
}

type serverStream struct {
	stream stream
	grpc.ServerStream
}

func (ss *serverStream) Context() context.Context    { return ss.stream.Context() }
func (ss *serverStream) SendMsg(m interface{}) error { return ss.stream.SendMsg(m) }
func (ss *serverStream) RecvMsg(m interface{}) error { return ss.stream.RecvMsg(m) }

type clientStream struct {
	stream stream
	grpc.ClientStream
}

func (cs *clientStream) Context() context.Context    { return cs.stream.Context() }
func (cs *clientStream) SendMsg(m interface{}) error { return cs.stream.SendMsg(m) }
func (cs *clientStream) RecvMsg(m interface{}) error { return cs.stream.RecvMsg(m) }
