#!/usr/bin/env bash
set -e -o pipefail

for file in \
    google/protobuf/{any,api,descriptor,duration,empty,field_mask,source_context,struct,timestamp,type,wrappers}.proto \
    google/api/{http,annotations}.proto \
; do
	for dir in \
		"${GOPATH}/src/github.com/google/protobuf/src" \
		"${GOPATH}/src/github.com/google/googleapis" \
	; do
		path="${dir}/${file}"
		test -f "${path}" || continue

		mkdir -p -- "$(dirname "./${file}")"
		cp -- "${path}" "./${file}"

		break
	done
done
