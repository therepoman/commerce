package chitin

import (
	"io"

	"code.justin.tv/common/chitin/internal/trace"
)

type ProcOpt func(*trace.ProcInfo)

// ExperimentalSetTraceDestination overrides the Trace event destination.
//
// This API is subject to change.
func ExperimentalSetTraceDestination(w io.Writer) ProcOpt {
	return (func(*trace.ProcInfo))(trace.SetDestination(w))
}

// ExperimentalTraceProcessOptIn instructs chitin to emit Trace events when
// any Traceable work is done in the process.
//
// The default Trace event destination is 127.0.0.1 on UDP port 8943.
//
// This API is subject to change.
func ExperimentalTraceProcessOptIn(opts ...ProcOpt) error {
	// some awkward type conversions are required since there are two distinct
	// named types referring to
	// func(*"code.justin.tv/common/chitin/internal/trace".ProcInfo)
	var os []trace.InfoOpt
	for _, o := range opts {
		os = append(os, (func(*trace.ProcInfo))(o))
	}
	return trace.ProcessOptIn(os...)
}
