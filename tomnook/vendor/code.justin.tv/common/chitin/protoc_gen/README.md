# protoc_gen

## What it is

This directory contains a tool for generating outputs from proto files. For
Go, it generates marshal/unmarshal code and gRPC stubs with chitin support
(for Trace instrumentation, finding client leaks, etc).

It does not currently generate code for other programming languages. It does
not currently generate markdown docs for the gRPC APIs. It does not currently
generate JSON/HTTP stubs for the gRPC APIs.

This tool formerly rewrote import paths rooted at "google" so that they were rooted at
"code.justin.tv/common/chitin/third_party/google".  This is no longer necessary;
the same types will now have import paths with the prefix
"github.com/google/protobuf/ptypes".

In total, the tool should make it possible to write proto files that import
proto files from other repos - provided that they're either checked out in
your GOPATH or that you have specific versions of them in your repo's Godeps
or vendor directoy.


## How to use it

You can include it in whatever repo you like with a file like this one:

```Go
// This file could be located at ./build_deps/protoc.go within your repo

package build_deps // import "code.justin.tv/org/repo/build_deps"

import (
  // Tool to run protoc for the whole repo and add instrumentation to gRPC APIs
  _ "code.justin.tv/common/chitin/protoc_gen"
)

//go:generate go run ./vendor/code.justin.tv/common/chitin/protoc_gen/protoc.go -start ..
```


### How that works

It's a normal Go file sitting somewhere unobtrusive in your repo. It's not
hidden with build tags or underscore prefixes. When you run commands like
godep save ./..., godep will copy the entire
code.justin.tv/common/chitin/protoc_gen directory into your
Godeps/_workspace/src directory (which for release/trace and some others is
symlinked to ./vendor), since there's a file in that directory in a package
called "protoc_gen". You now have a versioned copy of the tool committed to
your repo.

The next step is to invoke the tool, which is what the go:generate line
enables. When you run a command like go generate ./..., go run will build and
run the "package main" file that was included as a side effect of godep
copying over the entire "protoc_gen" directory. The tool will find the root of
your repo on its own so it can add the Godeps and vendor directories in its
protoc include path, but it uses the "-start" flag as the starting point for
its search for proto files in need of compilation.
