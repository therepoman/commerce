package chitin

import (
	"bytes"
	"io"
	"testing"
)

func TestProcessOptIn(t *testing.T) {
	var w io.Writer = new(bytes.Buffer)
	opt := ExperimentalSetTraceDestination(w)
	if err := ExperimentalTraceProcessOptIn(opt); err != nil {
		t.Fatalf("opt in err=%q", err)
	}
}
