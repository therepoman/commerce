package rollbar

import (
	"fmt"
	"net/http"

	"code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/common/golibs/errorlogger"

	"github.com/stvp/rollbar"
)

type errorLoggerImpl struct{}

// NewRollbarLogger creates a rollbar logger.
func NewRollbarLogger(token, env string) errorlogger.ErrorLogger {
	rollbar.Token = token
	rollbar.Environment = env
	rollbar.CodeVersion = bininfo.Revision()
	return &errorLoggerImpl{}
}

func (l *errorLoggerImpl) Error(err error) {
	rollbar.ErrorWithStack(rollbar.ERR, err, rollbar.BuildStack(0))
}

func (l *errorLoggerImpl) RequestError(r *http.Request, err error) {
	rollbar.RequestErrorWithStack(rollbar.ERR, r, err, rollbar.BuildStack(0))
}

func (l *errorLoggerImpl) RequestPanic(r *http.Request, p interface{}) {
	err := fmt.Errorf(fmt.Sprintf("panic: %v", p))
	rollbar.RequestErrorWithStack(rollbar.ERR, r, err, rollbar.BuildStack(7))
}
