package jitter

import (
	"testing"
	"time"
)

func TestRandTimer(t *testing.T) {
	dist := Linear{Min: 1 * dur, Max: 3 * dur}

	timers := make([]*Timer, 1000)
	for i := range timers {
		t := NewTimer(dist)
		timers[i] = t
	}

	time.Sleep(2 * dur)

	var fired int
	for _, t := range timers {
		if !t.Stop() {
			fired++
		}
	}

	if have, min, max := fired, 400, 600; have < min || have > max {
		t.Errorf("unexpected number of timers fired; %d not in range [%d, %d]",
			have, min, max)
	}
}
