package jitter

import "container/heap"

type timerHeap []*timer

var _ heap.Interface = (*timerHeap)(nil)

func (h *timerHeap) Len() int {
	return len(*h)
}

func (h *timerHeap) Less(i, j int) bool {
	return (*h)[i].when < (*h)[j].when
}

func (h *timerHeap) Swap(i, j int) {
	(*h)[i], (*h)[j] = (*h)[j], (*h)[i]
	(*h)[i].i, (*h)[j].i = i, j
}

func (h *timerHeap) Pop() interface{} {
	t := (*h)[len(*h)-1]
	(*h) = (*h)[:len(*h)-1]
	t.i = -1
	return t
}

func (h *timerHeap) Push(val interface{}) {
	t := val.(*timer)
	t.i = len(*h)
	(*h) = append(*h, t)
}
