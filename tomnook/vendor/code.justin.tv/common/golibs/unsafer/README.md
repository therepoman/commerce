Unsafer Go Package
===
When unsafe just isn't unsafe enough.

Why
---
Under gdb you will be able to see all the Go runtime data structures. You can, but shouldn't, access the runtime routines and data structures from within your go program.

How to use
---
In your program, import unsafer:

	_ "code.justin.tv/common/golibs/unsafer"

Launch your program with gdb:

	gdb <prog> -d $GOROOT

In gdb, load the python debugging script for Go:

	source ~/go/src/pkg/runtime/runtime-gdb.py

Once your program breaks execution, you can examine Go runtime data structures:

	p *'runtime.allg'

Note, you can't see any Go runtime values until Go initializes itself. One way to make sure that it is complete is to set a break point at main, then run your program:

	b 'main.main'
	r

Caveats
---
* This only works with Go 1.2
* It is a *bad* idea to access the Go internals from within your Go progam, without first shutting down all other go routines.