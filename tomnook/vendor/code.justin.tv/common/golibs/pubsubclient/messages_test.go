package pubsubclient

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestMarshal(t *testing.T) {
	type testcase struct {
		in   typedMessage
		want string
	}

	testcases := []testcase{
		{&pingMsg{msg{ping}}, `{"type":"PING"}`},
		{&pongMsg{msg{pong}}, `{"type":"PONG"}`},
		{&listenMsg{
			msg:   msg{listen},
			Nonce: "nonce",
			Data: topicList{
				Topics:    []string{"t1", "t2"},
				AuthToken: "at",
			},
		}, `{"type":"LISTEN","nonce":"nonce","data":{"topics":["t1","t2"],"auth_token":"at"}}`},
		{&unlistenMsg{
			msg:   msg{unlisten},
			Nonce: "nonce",
			Data: topicList{
				Topics:    []string{"t1", "t2"},
				AuthToken: "at",
			},
		}, `{"type":"UNLISTEN","nonce":"nonce","data":{"topics":["t1","t2"],"auth_token":"at"}}`},
	}
	for i, tc := range testcases {
		have, err := json.Marshal(tc.in)
		if err != nil {
			t.Errorf("case=%d  err=%s", i+1, err)
			continue
		}
		if string(have) != tc.want {
			t.Errorf("case=%d  json.Marshal(%+v)  have=%s  want=%s", i+1, tc.in, string(have), tc.want)
		}
	}
}

func TestUnmarshal(t *testing.T) {
	type testcase struct {
		in   string
		want typedMessage
	}

	testcases := []testcase{
		{"", nil},
		{`{"type":"PING"}`, &pingMsg{msg{ping}}},
		{`{"type":"PING","data":null}`, &pingMsg{msg{ping}}},
		{`{"type":"PONG","data":null}`, &pongMsg{msg{pong}}},
		{`{"type":"RECONNECT","data":null}`, &reconnectMsg{msg{reconnect}}},
		{`{"type":"MESSAGE","data":{"topic":"t","message":"d"}}`,
			&messageMsg{
				msg: msg{message},
				Data: Message{
					Topic:   "t",
					Message: "d",
				},
			},
		},
		{`{"type":"RESPONSE","nonce": "abc"}`,
			&responseMsg{
				msg:   msg{response},
				Nonce: "abc",
			},
		},
		{`{"type":"RESPONSE","nonce": "abc","error":"ERR_BADMESSAGE"}`,
			&responseMsg{
				msg:   msg{response},
				Error: "ERR_BADMESSAGE",
				Nonce: "abc",
			},
		},
	}
	for i, tc := range testcases {
		have, err := unmarshal([]byte(tc.in))
		if !reflect.DeepEqual(have, tc.want) {
			t.Errorf("case %d: Unmarshal(%s), have=%+v  want=%+v", i+1, tc.in, have, tc.want)
			t.Logf("err=%s", err)
		}
	}
}
