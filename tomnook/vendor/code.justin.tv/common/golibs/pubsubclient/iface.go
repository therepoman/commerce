package pubsubclient

import "errors"

// ErrClosed indicates that a client has been closed, so it cannot
// retrieve any more messages.
var ErrClosed = errors.New("pubsubclient closed")

// ErrDisconnected indicates that a client is not connected to a
// backend, so it cannot send or retrieve any messages.
var ErrDisconnected = errors.New("pubsubclient is disconnected")

// A Client retrieves messages from pubsub. It gets messages only on
// topics that it has Subscribed to. Calls to NextMessage retrieve
// messages in a first-in, first-out ordering.
type Client interface {
	// Subscribe will subscribe to messages on a particular topic from
	// the pubsub cluster.
	Subscribe(topic string, authToken string) error

	// Unsubscribe will stop receiving messages from a particular topic.
	Unsubscribe(topic string) error

	// NextMessage will block until a message is available (it will
	// return the message and a nil error), or until the connection is
	// closed or a fatal error occurs (it will return the error; the
	// error will be ErrClosed if it's because the client was closed).
	//
	// Messages will be buffered by the client, so if you aren't pulling
	// messages out, then the buffer can overflow and you will drop
	// messages.
	NextMessage() (*Message, error)

	// Close disconnects the client.
	Close() error
}

// A Message is a single message sent from the pubsub server to a
// client.
type Message struct {
	Topic   string
	Message string
}
