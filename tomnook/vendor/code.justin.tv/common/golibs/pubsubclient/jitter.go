package pubsubclient

import (
	"math/rand"
	"time"
)

// randDuration returns a time.Duration chosen by selecting a random
// in the interval [from, to]. All values are equally likely.
func randDuration(from, to time.Duration) time.Duration {
	r := rand.Int63n(int64(to - from))
	return time.Duration(int64(from) + r)
}
