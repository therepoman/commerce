package statrecorder

import (
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestMetricName(t *testing.T) {
	type testcase struct {
		prefix string
		metric string
		want   string
	}

	tests := []testcase{
		{"", "", ""},
		{"system", "", "system"},
		{"system.subsystem", "", "system.subsystem"},
		{"", "metric", "metric"},
		{"system", "metric", "system.metric"},
		{"system.subsystem", "metric", "system.subsystem.metric"},
		{"", ".dotfirst", "dotfirst"},
		{"system", ".dotfirst", "system.dotfirst"},
		{"system.subsystem", ".dotfirst", "system.subsystem.dotfirst"},
	}

	for _, tc := range tests {
		sr := New(tc.prefix)
		have := sr.MetricName(tc.metric)
		if have != tc.want {
			t.Errorf("New(%q).MetricName(%q): have=%q, want=%q", tc.prefix, tc.metric, have, tc.want)
		}
	}
}

func TestInc(t *testing.T) {
	type testcase struct {
		m    string
		v    int64
		want map[string]int
	}

	sr := New("prefix")
	tests := []testcase{
		{"m1", 1, map[string]int{"m1": 1}},
		{"m1", 1, map[string]int{"m1": 2}},
		{"m1", 5, map[string]int{"m1": 7}},
		{"m2", -1, map[string]int{"m1": 7, "m2": -1}},
		{"m3.sub", 1 << 60, map[string]int{"m1": 7, "m2": -1, "m3.sub": 1 << 60}},
	}
	endState := map[string]int{
		"prefix.m1":     7,
		"prefix.m2":     -1,
		"prefix.m3.sub": 1 << 60,
	}

	for i, tc := range tests {
		_ = sr.Inc(tc.m, tc.v, 1)
		for m, want := range tc.want {
			have := sr.CounterValue(m)
			if have != want {
				t.Errorf("test=%d: sr.Inc(%q, %d, 1); sr.CounterValue(%q):  have=%d  want=%d", i, tc.m, tc.v, m, have, want)
			}
		}
	}
	have := sr.AllCounters()
	if !reflect.DeepEqual(have, endState) {
		t.Errorf("end state  have=%+v  want=%+v", have, endState)
	}
}

func TestGauge(t *testing.T) {
	type testcase struct {
		m    string
		v    int64
		want map[string]int
	}

	sr := New("prefix")
	tests := []testcase{
		{"m1", 1, map[string]int{"m1": 1}},
		{"m1", 1, map[string]int{"m1": 1}},
		{"m1", 5, map[string]int{"m1": 5}},
		{"m2", -1, map[string]int{"m1": 5, "m2": -1}},
		{"m3.sub", 1 << 60, map[string]int{"m1": 5, "m2": -1, "m3.sub": 1 << 60}},
	}
	endState := map[string]int{
		"prefix.m1":     5,
		"prefix.m2":     -1,
		"prefix.m3.sub": 1 << 60,
	}

	for i, tc := range tests {
		_ = sr.Gauge(tc.m, tc.v, 1)
		for m, want := range tc.want {
			have := sr.GaugeValue(m)
			if have != want {
				t.Errorf("test=%d: sr.Gauge(%q, %d, 1); sr.GaugeValue(%q):  have=%d  want=%d", i, tc.m, tc.v, m, have, want)
			}
		}
	}
	have := sr.AllGauges()
	if !reflect.DeepEqual(have, endState) {
		t.Errorf("end state  have=%+v  want=%+v", have, endState)
	}
}

func TestSets(t *testing.T) {
	type testcase struct {
		m    string
		v    string
		want map[string]map[string]struct{}
	}

	sr := New("prefix")
	tests := []testcase{
		{"m1", "k1",
			map[string]map[string]struct{}{
				"m1": {
					"k1": struct{}{},
				},
			},
		},
		{"m1", "k2",
			map[string]map[string]struct{}{
				"m1": {
					"k1": struct{}{},
					"k2": struct{}{},
				},
			},
		},
		{"m1", "k1",
			map[string]map[string]struct{}{
				"m1": {
					"k1": struct{}{},
					"k2": struct{}{},
				},
			},
		},
		{"m2", "k3",
			map[string]map[string]struct{}{
				"m1": {
					"k1": struct{}{},
					"k2": struct{}{},
				},
				"m2": {
					"k3": struct{}{},
				},
			},
		},
	}
	endState := map[string]map[string]struct{}{
		"prefix.m1": map[string]struct{}{
			"k1": struct{}{},
			"k2": struct{}{},
		},
		"prefix.m2": map[string]struct{}{
			"k3": struct{}{},
		},
	}

	for i, tc := range tests {
		_ = sr.Set(tc.m, tc.v, 1)
		for m, want := range tc.want {
			have := sr.SetValue(m)
			if !reflect.DeepEqual(have, want) {
				t.Errorf("test=%d: sr.Set(%q, %q, 1); sr.SetValue(%q):  have=%v  want=%v", i, tc.m, tc.v, m, have, want)
			}
		}
	}
	have := sr.AllSets()
	if !reflect.DeepEqual(have, endState) {
		t.Errorf("end state  have=%+v  want=%+v", have, endState)
	}
}

func TestSetValueIsImmutable(t *testing.T) {
	sr := New("prefix")
	_ = sr.Set("m", "v", 1)

	s1 := sr.SetValue("m")
	s1["v2"] = struct{}{}

	s2 := sr.SetValue("m")

	if reflect.DeepEqual(s1, s2) {
		t.Error("the return value from StatRecorder.SetValue should be a copy; mutating it shouldn't change the StatRecorder's data")
	}
}

func TestTimingDurations(t *testing.T) {
	type testcase struct {
		m    string
		v    time.Duration
		want map[string][]time.Duration
	}

	sr := New("prefix")
	tests := []testcase{
		{"m1", time.Second,
			map[string][]time.Duration{
				"m1": []time.Duration{
					time.Second,
				},
			},
		},
		{"m1", time.Millisecond,
			map[string][]time.Duration{
				"m1": []time.Duration{
					time.Second,
					time.Millisecond,
				},
			},
		},
		{"m2", time.Millisecond,
			map[string][]time.Duration{
				"m1": []time.Duration{
					time.Second,
					time.Millisecond,
				},
				"m2": []time.Duration{
					time.Millisecond,
				},
			},
		},
	}
	endState := map[string][]time.Duration{
		"prefix.m1": []time.Duration{
			time.Second,
			time.Millisecond,
		},
		"prefix.m2": []time.Duration{
			time.Millisecond,
		},
	}

	for i, tc := range tests {
		_ = sr.TimingDuration(tc.m, tc.v, 1)
		for m, want := range tc.want {
			have := sr.TimingValues(m)
			if !reflect.DeepEqual(have, want) {
				t.Errorf("test=%d: sr.TimingDuration(%q, %s, 1); sr.TimingValues(%q):  have=%v  want=%v", i, tc.m, tc.v, m, have, want)
			}
		}
	}
	have := sr.AllTimings()
	if !reflect.DeepEqual(have, endState) {
		t.Errorf("end state  have=%+v  want=%+v", have, endState)
	}
}

func TestTimingValuesIsImmutable(t *testing.T) {
	sr := New("prefix")
	_ = sr.TimingDuration("m", time.Second, 1)

	s1 := sr.TimingValues("m")
	s1[0] = time.Millisecond

	s2 := sr.TimingValues("m")

	if reflect.DeepEqual(s1, s2) {
		t.Error("the return value from StatRecorder.TimingValues should be a copy; mutating it shouldn't change the StatRecorder's data")
	}
}

func TestStatRecorderConcurrentUsage(t *testing.T) {
	if !raceDetection {
		t.Skip("run tests with -race")
	}

	const n = 1000

	sr := New("")

	var wg sync.WaitGroup
	for i := 0; i < n; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			_ = sr.Inc("metric", 1, 1)
		}()
	}
	wg.Wait()

	have := sr.CounterValue("metric")
	if have != n {
		t.Errorf("have=%d  want=%d", have, n)
	}
}

// Test that SetPrefix and MetricName methods can be called
// concurrently without triggering the race detector
func TestConcurrentNaming(t *testing.T) {
	if !raceDetection {
		t.Skip("run tests with -race")
	}

	sr := New("")

	prefixes := []string{
		"a", "b", "c", "d", "e",
	}
	metrics := []string{
		"1", "2", "3", "4", "5",
	}

	var wg sync.WaitGroup
	done := make(chan struct{})
	for i := 0; i < len(prefixes); i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			for {
				select {
				case <-done:
					return
				default:
				}
				sr.SetPrefix(prefixes[idx])
				time.Sleep(10 * time.Microsecond)
			}
		}(i)
	}

	for i := 0; i < len(metrics); i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			for {
				select {
				case <-done:
					return
				default:
				}
				sr.MetricName(metrics[idx])
				time.Sleep(10 * time.Microsecond)
			}
		}(i)
	}

	time.Sleep(5 * time.Millisecond)
	close(done)
	wg.Wait()
}
