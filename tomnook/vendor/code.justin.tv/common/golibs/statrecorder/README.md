# statrecorder #

statrecorder provides the `StatRecorder` struct, which implements
[github.com/cactus/go-statsd-client/statsd](https://github.com/cactus/go-statsd-client/statsd)'s
Statter, SubStatter and StatSender interfaces by recording statistics
in memory.

This is useful for tests: one can instantiate a StatRecorder and then
make assertions about the metrics that it has received. Like this,
say, if your `newApp` function takes a `statsd.Statter` in as an
argument:

```go
func TestAppSendsMetrics(t *testing.T) {
  stats := statrecorder.New("application.testing")
  app := newApp(stats)

  app.DoUsefulThings()
  app.HandleRequest()
  // etc

  if have := stats.CounterValue("requests.received"); have != 1 {
    t.Errorf("expected 1 request received in stats counters, have=%d", have)
  }
}
```

Check the
[godoc](https://godoc.internal.justin.tv/code.justin.tv/common/golibs/statrecorder)
for all the methods available.
