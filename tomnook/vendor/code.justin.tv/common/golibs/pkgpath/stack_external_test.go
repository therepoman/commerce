package pkgpath_test

import (
	"testing"

	"code.justin.tv/common/golibs/pkgpath"
)

func TestPkgpathMain(t *testing.T) {
	pkg, ok := pkgpath.Main()
	if have, want := pkg, "code.justin.tv/common/golibs/pkgpath/_test"; have != want {
		t.Errorf("pkgpath.Main(); %q != %q", have, want)
	}
	if have, want := ok, true; have != want {
		t.Errorf("pkgpath.Main(); %t != %t", have, want)
	}
}

func TestPkgpathCaller(t *testing.T) {
	pkg, ok := pkgpath.Caller(0)
	if have, want := pkg, "code.justin.tv/common/golibs/pkgpath_test"; have != want {
		t.Errorf("Main(); %q != %q", have, want)
	}
	if have, want := ok, true; have != want {
		t.Errorf("Main(); %t != %t", have, want)
	}
}
