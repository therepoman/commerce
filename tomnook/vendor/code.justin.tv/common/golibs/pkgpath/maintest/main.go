//+build ignore

package main

import (
	"fmt"

	"imported2"

	"code.justin.tv/common/golibs/pkgpath"
	"code.justin.tv/common/golibs/pkgpath/maintest/imported"
)

var initPkg string

func init() {
	initPkg, _ = pkgpath.Caller(0)
}

func main() {
	mainPkg, _ := pkgpath.Main()
	fmt.Printf("%s\n", mainPkg)
	fmt.Printf("%s\n", initPkg)
	fmt.Printf("%s\n", imported.Pkg)
	fmt.Printf("%s\n", imported2.Pkg)
}
