package imported

import "code.justin.tv/common/golibs/pkgpath"

var Pkg string

func init() {
	Pkg, _ = pkgpath.Caller(0)
}
