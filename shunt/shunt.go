package shunt

import (
	"hash"
	"hash/fnv"
	"math/rand"
)

// TODO: n-partitioning instead of single threshold

const maxUint32 = ^uint32(0)

// Shunter implements binary shunting (A/B). Shunter is not safe for concurrent use by multiple goroutines.
type Shunter struct {
	threshold uint32
	hash      hash.Hash32
	rand      *rand.Rand
}

// New returns a pointer to a newly-allocated Shunter with its initial pass rate set to the passed float.
// The pass rate must be a float64 within the range [0, 1]. Out-of-range pass rates clamp to the closest end of the range.
// A pass rate of 0.7, for example, indicates 70% of discrete values passed to Pass() should return true.
//
// The default hashing function used is FNV-1a. This can be changed via SetHash().
func New(passRate float64) *Shunter {
	threshold := thresholdFromRate(maxUint32, passRate)
	return &Shunter{threshold: threshold, hash: fnv.New32a(), rand: rand.New(rand.NewSource(1))}
}

// SetPassRate updates the pass rate of a Shunter in-place. The pass rate must be within the range [0, 1].
func (s *Shunter) SetPassRate(rate float64) {
	s.threshold = thresholdFromRate(maxUint32, rate)
}

// SetHash updates the internal Hash of a Shunter. Passing a nil Hash does nothing.
func (s *Shunter) SetHash(hash hash.Hash32) {
	if hash == nil {
		return
	}

	s.hash = hash
}

// SetRand updates the internal *rand.Rand of a Shunter. Passing a nil pointer does nothing.
func (s *Shunter) SetRand(rand *rand.Rand) {
	if rand == nil {
		return
	}

	s.rand = rand
}

// SeedRand seeds the internal *rand.Rand in-place.
func (s *Shunter) SeedRand(seed int64) {
	s.rand.Seed(seed)
}

// Pass, when called with many different values, will return a percentage of `true` results approximating the set
// pass rate of the Shunter.
//
// The result for a given sequence of bytes is deterministic and will not vary between repeated calls.
//
// When pass rate is 0.0, Pass() is guaranteed to return false. When pass rate is 1.0, Pass() is guaranteed to return true.
func (s *Shunter) Pass(data []byte) bool {
	if pass, shorted := s.shortCircuit(); shorted {
		return pass
	}

	s.hash.Reset()

	s.hash.Write(data)
	sum := s.hash.Sum32()

	return sum <= s.threshold
}

// Random returns a boolean value whose `true` results will, in aggregate, approximate the set pass rate of the Shunter.
// Random is useful when there is no value that can or should be used to get a deterministic pass value using Pass().
func (s *Shunter) Random() bool {
	if pass, shorted := s.shortCircuit(); shorted {
		return pass
	}

	return s.rand.Uint32() <= s.threshold
}

// shortCircuit implements detecting when a pass rate was set either to 0 or 1 to avoid doing any work in these cases,
// and to guarantee that the pass values are always false or true respectively.
func (s *Shunter) shortCircuit() (pass bool, shorted bool) {
	if s.threshold == 0 {
		pass, shorted = false, true
	}
	if s.threshold == maxUint32 {
		pass, shorted = true, true
	}

	return
}

func thresholdFromRate(numerator uint32, passRate float64) uint32 {
	if passRate < 0 {
		passRate = 0
	}
	if passRate > 1 {
		passRate = 1
	}

	return uint32(float64(numerator) * passRate)
}
