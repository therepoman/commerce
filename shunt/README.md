# shunt
A tiny utility for percentage-based and data-deterministic rollouts or traffic-tapering.

[Godoc reference](https://godoc.internal.justin.tv/code.justin.tv/commerce/shunt)

## Usage
Imagine your service has a new feature, and you want to roll it out slowly – 25% of users to start.

First, make a shunter with the appropriate pass rate:
```go
passRate := 0.25
shunter := shunt.New(passRate)
```

Next, use the shunter with some user- or request-scoped data to deterministically send traffic down the new path:
```go
userBytes := []byte(request.GetUserID())

if shunter.Pass(userBytes) {
    return doNewThing(request)
}

return doOldThing(request)
```
The shunter ensures that repeated calls with the same data, given the same pass rate, will return the same boolean value from `.Pass()`. This means users get a consistent experience during percentage rollouts.

If you don't want the conditional path to be deterministically tied to any data, you can use `.Random()`:
```go
if shunter.Random() {
    doNewThing(request)
}

doOldThing(request)
```

If you need to modify or customize the behaviors of the shunter, you can use the methods `.SetPassRate(passRate float64)`, `.SetHash(hash hash.Hash32)`, `.SetRand(rand *rand.Rand)`, and `.SeedRand(seed int64)`.

## Note
* Shunters are not safe for concurrent use by default. If you want to share a single shunter between multiple goroutines, mix with your favorite synchronization mechanism.
* If you set a pass rate of 0, all calls to `.Pass()` and `.Random()` are guaranteed to return false. Likewise, if you set a pass rate of 1, all calls to `.Pass()` and `.Random()` are guaranteed to return true.
* The hashing mechanism of shunt means that any value that passes at a given pass rate will continue to pass at any higher rate set on that shunter, assuming that `SetHash()` is not used to change the hash function during the same period.

## See also
* Would you like to change the pass rate of a shunter without an application deploy? Check out [dynamicconfig](https://git-aws.internal.justin.tv/commerce/dynamicconfig) or [distconf](https://git-aws.internal.justin.tv/hygienic/distconf)