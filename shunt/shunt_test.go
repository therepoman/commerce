package shunt

import (
	"io"
	"math"
	"math/rand"
	"testing"
	"time"
)

func TestShunterPass(t *testing.T) {
	const rounds = 1000000
	const tolerance = float64(0.01)
	const dataLength = 8

	testCases := []struct {
		setPassRate float64
		expectedPassRate float64
	}{
		{1.0, 1.0},
		{0.5, 0.5},
		{0.25, 0.25},
		{0.1, 0.1},
		{0.05, 0.05},
		{0.01, 0.01},
		{0.0, 0.0},
		{1.5, 1.0},
		{-1, 0},
	}

	for _, tt := range testCases {
		shunter := New(tt.setPassRate)

		var hits, total uint

		r := rand.New(rand.NewSource(time.Now().Unix()))
		for i := 0; i < rounds; i++ {
			data := make([]byte, dataLength)
			r.Read(data)

			if shunter.Pass(data) {
				hits++
			}

			total++
		}

		actualRate := float64(hits) / float64(total)
		testPassed := math.Abs(actualRate-tt.expectedPassRate) < tolerance
		if !testPassed {
			t.Errorf("Actual rate (%f) differed too much from desired rate (%f) (tolerance: %f)",
				actualRate, tt.expectedPassRate, tolerance)
		}
	}
}

func TestShunterPassDeterminism(t *testing.T) {
	const rounds = 100000

	testCases := []struct {
		passRate float64
		input    []byte
	}{
		{
			0.01,
			[]byte("strings in test files are one of the few places a programmer can really express themself"),
		},
		{
			0.05,
			[]byte("most people don't pay any attention to them, but they're a huge opportunity to write something meaningful"),
		},
		{
			0.5,
			[]byte("to jot down something philosophical, or a joke, or just some small wry 'I Was Here' graffito"),
		},
	}

	for _, tt := range testCases {
		var result, next bool

		shunter := New(tt.passRate)
		result = shunter.Pass(tt.input)

		for i := 1; i < rounds; i++ {
			next = shunter.Pass(tt.input)
			if next != result {
				t.Fail()
			}
		}
	}
}

func TestShunterPassRampupStickiness(t *testing.T) {
	dataCases := [][]byte{
		[]byte("a modest test string"),
		[]byte("another value"),
		[]byte("how many of these do I need to be rigorous"),
		[]byte("aaaaaaaaaaaaaaaaaaa"),
		[]byte("f"),
		[]byte("one more for good measure"),
		[]byte("and let's try an empty one:"),
		[]byte(""),
	}

	var steps []float64
	for i := 0; i < 500; i++ {
		step := float64(i) * (0.002)
		steps = append(steps, step)
	}

	for _, data := range dataCases {
		shunter := New(steps[0])
		passed := shunter.Pass(data)

		for _, rate := range steps[1:] {
			shunter.SetPassRate(rate)
			nextPassed := shunter.Pass(data)
			if passed && !nextPassed {
				t.Fatalf("went from passing to nonpassing during rampup. rate: %f", rate)
			}
			passed = nextPassed
		}
	}
}

func TestShunterRandom(t *testing.T) {
	const rounds = 1000000
	const tolerance = float64(0.01)

	testCases := []float64{
		0,
		1,
		0.05,
		0.15,
		0.5,
		0.7,
		0.9,
		0.95,
		0.99,
	}

	for _, passRate := range testCases {
		shunter := New(passRate)
		shunter.SeedRand(time.Now().Unix())

		var hits, total uint

		for i := 0; i < rounds; i++ {
			pass := shunter.Random()
			if pass {
				hits++
			}

			total++
		}

		actualRate := float64(hits) / float64(total)
		testPassed := math.Abs(actualRate-passRate) < tolerance
		if !testPassed {
			t.Errorf("Actual rate (%f) differed too much from desired rate (%f) (tolerance: %f)",
				actualRate, passRate, tolerance)
		}
	}
}

type mockHash32 struct {
	io.Writer
	sum32Called bool
}

func (m mockHash32) Sum([]byte) []byte { return nil }
func (m mockHash32) Sum32() uint32 {
	m.sum32Called = true
	return 0
}
func (m mockHash32) Reset()         {}
func (m mockHash32) Size() int      { return 0 }
func (m mockHash32) BlockSize() int { return 0 }

func TestPassRateShortCircuiting(t *testing.T) {
	testCases := []struct {
		description string
		passRate    float64
		shouldPass  bool
	}{
		{
			description: "for passRate == 0.0",
			passRate:    0.0,
			shouldPass:  false,
		},
		{
			description: "for passRate == 1.0",
			passRate:    1.0,
			shouldPass:  true,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			var mockHash mockHash32

			shunter := New(tt.passRate)
			shunter.SetHash(mockHash)

			passed := shunter.Pass([]byte("the hasher should never be called. this test data is also documentation. sorry"))

			if passed != tt.shouldPass {
				t.Errorf("shunter returned %t when Pass() was called when passRate was %f. Expected %t", passed, tt.passRate, tt.shouldPass)
			}

			if mockHash.sum32Called {
				t.Errorf("shunter failed to short-circuit when passRate was %f: mockHash.Sum32 was called", tt.passRate)
			}
		})
	}
}

func BenchmarkShunter(b *testing.B) {
	shunter := New(0.25)
	data := make([]byte, 12)

	b.ResetTimer()
	for i := 0; i <= b.N; i++ {
		shunter.Pass(data)
	}
}
