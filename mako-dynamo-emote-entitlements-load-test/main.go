package main

import (
	"bytes"
	"code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/twirp"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/golang/protobuf/jsonpb"
	vegeta "github.com/tsenart/vegeta/lib"
	"code.justin.tv/commerce/gogogadget/random"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"
)

const (
	makoEndpoint = "https://internal-mako-1463762839.us-west-2.elb.amazonaws.com/twirp/code.justin.tv.commerce.mako.Mako"
	dataDir = "data"
	outDir = "loadtest-results"
)

var (
	allFlag = flag.Bool("all", false, "run all test cases")

	getEmoteSetDetailsFlag = flag.Bool("getemotesetdetails", false, "run GetEmoteSetDetails load test")
	getActiveEmoteCodes = flag.Bool("getactiveemotecodes", false, "run GetActiveEmoteCodes load test")
)

/**
Test Data
 */

func init() {
	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	if *allFlag || *getEmoteSetDetailsFlag {
		var testUsers []string
		readJSON("UsersWithEntitlements", &testUsers)
		testCase_GetEmoteSetDetails(&testUsers)
	}

	if *allFlag || *getActiveEmoteCodes {
		var testUsers []string
		readJSON("UsersWithEntitlements", &testUsers)
		testCase_GetActiveEmoteCodes(&testUsers)
	}
}

func getRandomUserWithEntitlements(usersP *[]string) string {
	if usersP == nil {
		panic(errors.New("received nil items in getRandomUserWithEntitlements"))
	}

	items := *usersP

	randomIndex := random.Int(0, len(items) - 1)
	return items[randomIndex]
}

/**
	GetEmoteSetDetails
 */
func createGetEmoteSetDetailsTargeter(users *[]string) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := makoEndpoint + "/GetEmoteSetDetails"

	return func(t *vegeta.Target) (err error) {
		userID := getRandomUserWithEntitlements(users)

		req := &mako.GetEmoteSetDetailsRequest{
			UserId: userID,
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func testCase_GetEmoteSetDetails(items *[]string) {
	testDuration := time.Minute * 30
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{2000, time.Second},
		Amp:     vegeta.Rate{500, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(createGetEmoteSetDetailsTargeter(items), pacer, testDuration, "GetEmoteSetDetails")
}

/**
GetEmoteSetDetails
*/
func createGetActiveEmoteCodesTargeter(users *[]string) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := makoEndpoint + "/GetActiveEmoteCodes"

	return func(t *vegeta.Target) (err error) {
		userID := getRandomUserWithEntitlements(users)

		req := &mako.GetActiveEmoteCodesRequest{
			UserId: userID,
			Domain: mako.Domain_emotes,
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func testCase_GetActiveEmoteCodes(items *[]string) {
	testDuration := time.Minute * 30
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{2000, time.Second},
		Amp:     vegeta.Rate{500, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(createGetActiveEmoteCodesTargeter(items), pacer, testDuration, "GetActiveEmoteCodes")
}

/**
	Utils
 */

func readJSON(sourceFileName string, out interface{}) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	source := path.Join(dir, dataDir, fmt.Sprintf("%s.json", sourceFileName))

	bytes, err := ioutil.ReadFile(source)

	err = json.Unmarshal(bytes, out)

	if err != nil {
		panic(err)
	}
}

func printMetrics(title string, metrics vegeta.Metrics) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	file, err := os.Create(dest)
	if err != nil {
		logrus.Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Succesful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string)  {
	logrus.Info("Starting attack on " + name)
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	for res := range attacker.Attack(targeter, pacer, dur, name + "-Attack") {
		metrics.Add(res)
	}
	metrics.Close()

	printMetrics(name, metrics)
}