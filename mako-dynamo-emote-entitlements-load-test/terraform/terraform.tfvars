aws_profile = "mako-prod"

max_hosts = 1

username = "flomic"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-b8e471df"

subnets = "subnet-195df37e,subnet-6557793d,subnet-b808c8f1"

security_group = "sg-ea8f1f92"
