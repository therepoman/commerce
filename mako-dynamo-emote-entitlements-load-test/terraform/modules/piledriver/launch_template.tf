data template_file "userdata" {
  template = "${file("${path.module}/userdata.tpl")}"
  vars {
    username = "${var.username}"
    public_key = "${file("${var.public_key_path}")}"
  }
}

resource "aws_launch_template" "main" {
  name = "${var.username}-load-testing"

  image_id = "ami-061392db613a6357b"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${var.security_group}"]

  user_data = "${base64encode(data.template_file.userdata.rendered)}"
}
