// Seed plan:
//   * 10,000 users, each having
//   * Between 1-10 entitlements, each mapping to
//   * 1 groupID, each containing (create 1,000 groups)
//   * 1 bits emote (we will need to create unique per group)

/**
	TODO:
		* scale up mako ECS
		* scale up mako dynamo table(s?)
 */

package main

import (
	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/gogogadget/math"
	"context"
	"encoding/json"
	"fmt"
	"github.com/golang/protobuf/ptypes/timestamp"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"
	"github.com/gofrs/uuid"
	"golang.org/x/time/rate"
	"github.com/golang/protobuf/ptypes"
)

const (
	makoHost                  = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	emoteGroupsToCreate       = 1000
	entitledUsersToCreate     = 10000
	maxEmotesToEntitlePerUser = 10
	outDir                    = "data"

	emoteOwner = "267893713"
	emotePrefix = "wolfpack9"
	emoteSuffixStarter = "BB" // Needs to start with a capital letter. Need to change this every re-run or else we will get dups.
	emoteImage28ID =  "267893713_28_bffa5fe7-9b34-4d6c-a7ea-5b0162ae893f"
	emoteImage56ID = "267893713_56_14b3c108-7f0e-4a76-bd01-ed93f305d165"
	emoteImage112ID = "267893713_112_d228c955-02cb-4965-98c6-19e2c594b551"

	entitledUserIDPrefix = "bits_emote_load_test_user"
	entitlementOriginID = "Bits-Badge-Tier-Emote-Rewards"
	maxEntitlementBuffer = 25

	maxCreateEmoteGroupTPS = 15
	maxCreateEmoteTPS = 25
	maxEntitleTPS = 10
)

var createEmoteGroupLimiter = rate.NewLimiter(maxCreateEmoteGroupTPS, 1)
var createEmoteLimiter = rate.NewLimiter(maxCreateEmoteTPS, 1)
var entitleLimiter = rate.NewLimiter(maxEntitleTPS, 1)
var currentEmoteSuffix = 0

func makeMakoClient() mako_client.Mako {
	return mako_client.NewMakoProtobufClient(makoHost, http.DefaultClient)
}

func reportProgress(name string, progress, total int) {
	if rand.Intn(100) == 1 {
		fmt.Printf("%s: %d of %d\n", name, progress, total)
	}
}

func createEmoteGroups(mako mako_client.Mako) []string {
	ctx := context.Background()

	var emoteGroupIDs []string

	for i := 0; i < emoteGroupsToCreate; i++ {
		_ = createEmoteGroupLimiter.Wait(ctx)

		resp, err := mako.CreateEmoteGroup(ctx, &mako_client.CreateEmoteGroupRequest{})

		if err != nil {
			panic(err)
		}

		emoteGroupIDs = append(emoteGroupIDs, resp.GroupId)
		reportProgress("CreateEmoteGroup", i, emoteGroupsToCreate)
	}

	return emoteGroupIDs
}

func createUniqueEntitledUserID() string {
	ownerUUID, err := uuid.NewV4()

	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("%s-%s", entitledUserIDPrefix, ownerUUID.String())
}

func createEmotesForGroups(mako mako_client.Mako, emoteGroupIDs []string) {
	ctx := context.Background()

	for i, emoteGroupID := range emoteGroupIDs {
		_ = createEmoteLimiter.Wait(ctx)

		suffix := fmt.Sprintf("%s%d", emoteSuffixStarter, currentEmoteSuffix)
		currentEmoteSuffix++

		_, err := mako.CreateEmoticon(context.Background(), &mako_client.CreateEmoticonRequest{
			UserId:                    emoteOwner,
			Code:                      fmt.Sprintf("%s%s", emotePrefix, suffix),
			CodeSuffix:                suffix,
			GroupId:                   emoteGroupID,
			Image28Id:                 emoteImage28ID,
			Image56Id:                 emoteImage56ID,
			Image112Id:                emoteImage112ID,
			Domain:                    mako_client.Domain_emotes.String(),
			EmoteGroup:                mako_client.EmoteGroup_BitsBadgeTierEmotes,
			EnforceModerationWorkflow: false,
			State: 					   mako_client.EmoteState_active.String(),
		})

		if err != nil {
			panic(err)
		}

		reportProgress("CreateEmote", i, len(emoteGroupIDs))
	}
}

func flushEntitlements(ctx context.Context, mako mako_client.Mako, entitlements []*mako_client.EmoteGroupEntitlement) {
	_ = entitleLimiter.Wait(ctx)

	_, err := mako.CreateEmoteGroupEntitlements(context.Background(), &mako_client.CreateEmoteGroupEntitlementsRequest{
		Entitlements: entitlements,
	})

	if err != nil {
		panic(err)
	}

	fmt.Println("flushing entitlements")
}

func createAndEntitleTestUsers(mako mako_client.Mako, emoteGroupIDs []string) []string {
	ctx := context.Background()
	var userIDs []string
	var entitlementsBuffer []*mako_client.EmoteGroupEntitlement

	for i := 0; i < entitledUsersToCreate; i++ {
		groupsEntitled := map[string]bool{}
		userID := createUniqueEntitledUserID()

		// Each user is entitled between 0-10 groups (unless fewer emote groups were created)
		countOfGroupsToEntitle := math.MinInt(rand.Intn(maxEmotesToEntitlePerUser + 1), len(emoteGroupIDs))

		for i := 0; i < countOfGroupsToEntitle; i++ {
			randomEmoteGroupIndex := rand.Intn(len(emoteGroupIDs))
			emoteGroupToEntitle := emoteGroupIDs[randomEmoteGroupIndex]

			// Skip if we RNG'ed the same emote group more than once for the same user.
			// Materia will error if we try to entitle the same owner/item/group combo in the same transaction.
			if _, ok := groupsEntitled[emoteGroupToEntitle]; ok {
				continue
			}
			groupsEntitled[emoteGroupToEntitle] = true

			expireAt := time.Now().Add(time.Hour * 24 * 7)
			protoExpireAt, err := ptypes.TimestampProto(expireAt)

			if err != nil {
				panic(err)
			}

			entitlementsBuffer = append(entitlementsBuffer, &mako_client.EmoteGroupEntitlement{
				OwnerId:  userID,
				ItemId:   emoteOwner,
				GroupId:  emoteGroupToEntitle,
				OriginId: entitlementOriginID,
				Group:    mako_client.EmoteGroup_BitsBadgeTierEmotes,
				Start:    &timestamp.Timestamp{},
				End: &mako_client.InfiniteTime{
					Time: protoExpireAt,
				},
			})

			// Only call Mako/Materia with 100 entitlements at a time
			if len(entitlementsBuffer) >= maxEntitlementBuffer {
				flushEntitlements(ctx, mako, entitlementsBuffer)
				entitlementsBuffer = nil
			}
		}

		userIDs = append(userIDs, userID)
	}

	// Flush remaining entitlements
	if len(entitlementsBuffer) > 0 {
		flushEntitlements(ctx, mako, entitlementsBuffer)
	}

	return userIDs
}

func writeResults(destFileName string, payload interface{}) {
	payloadJSON, err := json.Marshal(payload)

	if err != nil {
		panic(err)
	}

	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%s.json", destFileName))
	err = ioutil.WriteFile(dest, payloadJSON, 0666)

	if err != nil {
		panic(err)
	}
}

func readResults(sourceFileName string, out interface{}) error {
	dir, err := os.Getwd()

	if err != nil {
		return err
	}

	source := path.Join(dir, outDir, fmt.Sprintf("%s.json", sourceFileName))

	bytes, err := ioutil.ReadFile(source)

	err = json.Unmarshal(bytes, out)

	if err != nil {
		return err
	}

	return nil
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	makoClient := makeMakoClient()

	// Create test emote groups
	emoteGroupIDs := createEmoteGroups(makoClient)
	writeResults("EmoteGroupIDs", emoteGroupIDs) // Cache intermediate output

	// If EmoteGroup creation succeeds, but we fail during emote creation, we can resume from
	// the cached file output by commenting out the following:
	//var emoteGroupIDs []string
	//err := readResults("EmoteGroupIDs", &emoteGroupIDs)
	//
	//if err != nil {
	//	panic(err)
	//}

	// Create one emote for each group
	createEmotesForGroups(makoClient, emoteGroupIDs)

	// Create test users and entitle 1-10 groups each
	entitledUserIDs := createAndEntitleTestUsers(makoClient, emoteGroupIDs)
	writeResults("EntitledUserIDs", entitledUserIDs)
}