#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/mako-dynamo-emote-entitlements-load-test/variables.sh

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "${i}"
done