
====== GetEmoteSetDetails Results ======
Start Time: 2019-12-10 19:05:45.454381701 +0000 UTC m=+0.012126462 
End Time: 2019-12-10 19:35:45.454027879 +0000 UTC m=+1800.011772640 
Duration: 29m59.999646178s 
Requests: 3600000 
Success Percentage: 1.000000 
Request Rate: 2000.000393 
Succesful Request Throughput: 1999.825061 

Latency Average: 10.334358ms 
Latency p99: 99.175822ms 
Latency Max: 6.877030331s 

Status Codes: map[200:3600000] 
Errors: []

====== GetActiveEmoteCodes Results ======
Start Time: 2019-12-10 19:35:45.622744088 +0000 UTC m=+1800.180488820 
End Time: 2019-12-10 20:05:45.622305388 +0000 UTC m=+3600.180050120 
Duration: 29m59.9995613s 
Requests: 3600000 
Success Percentage: 1.000000 
Request Rate: 2000.000487 
Succesful Request Throughput: 1999.928430 

Latency Average: 10.933804ms 
Latency p99: 105.30376ms 
Latency Max: 6.761486042s 

Status Codes: map[200:3600000] 
Errors: []
