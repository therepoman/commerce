#!/bin/bash

# This file exists to define some env vars for running your load test

# The AWS profile you're operating in
AWS_PROFILE=mako-prod

# Your LDAP username you use to login to EC2 hosts
USERNAME=flomic

RUN_FLAG=all

OUTPUT_DIR=loadtest-results