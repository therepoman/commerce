package mako_client

// Optional Callback data for clients to pass in and receive back from Upload Service via SNS
type EmoticonUploadSNSCallbackData struct {
	Code        string `json:"code,omitempty"`
	GroupId     string `json:"group_id,omitempty"`
	ImageHeight int32  `json:"image_height,omitempty"`
	ImageWidth  int32  `json:"image_width,omitempty"`
}
