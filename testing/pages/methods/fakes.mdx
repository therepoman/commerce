import Callout from 'nextra-theme-docs/callout'

# Test State, Not Interactions 

Another way that tests commonly depend on implementation details involves not which methods of the system the test calls (i.e., mocking), but how the results of those calls are verified.

Interaction tests tend to be more brittle than state tests for the same reason that it's more brittle to test a private method than to test a public method: interaction tests check _how_ a system arrived at its result, whereas usually you should care only _what_ the result is.

A fake is a lightweight implementation of an interface that behaves similar to the real implementation but isn't suitable for production; for example, an in-memory database.

Fakes can be a powerful tool for testing: they execute quickly and allow you to effectively test your code without the drawbacks of using real implementations. A single fake has the power to radically improve the testing experience of an API. If you scale that to a large number of fakes for all sorts of APIs, fakes can provide an enormous boost to engineering velocity across services.

## Implementing a Fake

Fakes always start with an interface that defines a contract:

```go
type UserDB interface {
    CreateUser(User) error
    ReadByID(context.Context, id string) (*User, error)
}
```

A fake can be written in a few lines of code:

```go highlight=12,17
// e.g., local/userdb.go
type UserDB struct {
    users []User 
}

func NewUserDB(users ...User) *UserDB {
    return &UserDB{
        users: users,
    }
}

func (db *UserDB) CreateUser(user User) error {
    db.users = append(db.users, user)
    return nil
}

func (db *UserDB) ReadByID(ctx context.Context, id string) (*User, error) {
    for _, user := range db.users {
        if user.ID == id {
            return user, nil
        }
    }

    return nil, errors.New("user not found")
}
```

For every interface you would write an in-memory implementation that would behave *exactly* like the real thing. This can also be viewed as a **reference** implementation, allowing you to codify the exact behaviour an interface should have.

## Testing With Interfaces

Lets start with a simple test and build on it:

```go highlight=5
func TestUserDB(t *testing.T) {
    // Create a fake instance
    db := local.NewUserDB()

    err := createUser(context.Background(), db, "testUser123")
    assert.Nil(t, err)
}

// business logic somewhere
func createUser(ctx context.Context, db UserDB, id string) error {
    // Do something that creates the user...
    return db.CreateUser(ctx, User{
        ID: id,
    })
}
```

This test doesn't really assert anything yet and it couples itself with a **specific** implementation of the `UserDB` interface. Remember that fakes are meant to be used against a **public API** which means we should be able to plug any implementation of the same interface, this is addressed in the test suites section.

**Asserting By State**

The test above ends up creating a user and the end state should be a user in either a real database or in the in-memory implementation. To test it we just need to assert the output by querying for the user.

```go highlight=8,9,10
func TestUserDB(t *testing.T) {
    // Create a fake instance
    db := local.NewUserDB()

    err := createUser(context.Background(), db, "testUser123")
    assert.Nil(t, err)

    user, err := db.ReadByID(context.Background(), "testUser123")
    assert.Nil(t, err)
    assert.Equal(t, user.ID, "testUser123")
}
```

We'll address the coupling with the local implementation (e.g., the fake) later, for now we want to understand more about the effect of this pattern.


This test codified the required behaviour for creating a user. If a new user id is passed to `createUser` then a valid user should be created as a result. The test doesn't care (or hasn't specified) any other behaviour. This means the underlying implementation can freely change as long as the original behaviour remains true.

For example, a cache could be introduced in the business logic where reading starts at the cache and then goes to the underlying database second. This test will not need to change and will be able to safe guard against faulty logic where a user is not properly returned.

```go highlight=3
func createUser(ctx context.Context, db UserDB, id string) error {
    // ooops we accidentally set the cache to an empty user object.
    cache.Set("user:" + id, User{})

    // Do something that creates the user...
    return db.CreateUser(ctx, User{
        ID: id,
    })
}
```

The test above will end up failing at the last assert, comparing the ids of each record.

<Callout emoji="💡">
    Tests don't need to change unless the behaviour also changes. This doesn't mean new code doesn't require new tests to be written though.
</Callout>