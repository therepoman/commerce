import Callout from 'nextra-theme-docs/callout'

# Classic Unit Tests With Mocks

Whenever components start to introduce dependencies (such as a database, cache, external service) unit tests are used to ensure a component behaves as expected without using real implementations. Unit tests are meant to be run with just `go test` and thus needing to connect to a Postgres database, an external Twirp service, or an SQS topic is out of the question.

Here's an example to start with:

```go
type UserDB interface {
  ReadByID(ctx context.Context, id string) (...)
}

func TestUserDB(t *testing.T) {
  // I want to test against a user db.
}
```

At Twitch it's common for a mocking library to be used that generates code based on a given interface (e..g, `UserDB`). The generated _mock_
allows for testing against interactions (e.g., method `ReadByID` was called with `X`, `Y` parameters). Interactions are typically done via a mocking API such as `foobar.On("Method")` (but other types of interactions are possible).

Expanding on the previous example, we introduce a simple mocking test:

```go highlight=10
type UserDB interface {
  ReadByID(ctx context.Context, id string) (...)
}

func TestUserDB(t *testing.T) {
  // mocks.UserDB is the generated code that allows for testing against interactions.
  mockedUserDB := make(mocks.UserDB)

  // Assert that a specific interaction will occur otherwise the test should fail.
  mockedUserDB.On("ReadByID", mock.Anything).Returns(...)

  // Call some business logic that ends up using the mock
  doSomethingWithMock(mockedUserDB)

  // TEST PASSED!
}

func doSomethingWithMock(db UserDB) {
  db.ReadByID(context.Background(), "foobar")
}
```

Mocking expects any interaction to be specified upfront by the mocking APIs, otherwise a failure will occur. Writing new tests involve a few things:

-  Generate mocks if needed (usually these are already generated, but if a new dependency was added it may need to be generated).
- Instantiate all the mocks via `new(mocks.XXX)` and pass them along to the application (e.g., `NewUserAPI(userDBMock)`).
- Write all the interactions that the system will have.
- Assert something.

## The Problems

One of the benefits to mocking is how easy it is to initially write tests. However, as components get more complicated, so does mocking. Interactions throughout the system are largely unknown or even non-deterministic (branching logic may completely change which interactions and how many run).

<Callout emoji="💡">
  More time is spent **reading** and **maintaining** tests than writing the initial version. Tests that are hard to maintain make the system harder to maintain.
</Callout>

**Tests are coupled to implementations**

Because mocks are based on asserting by interaction, it requires tests to be tightly coupled with the underlying implementation as each test needs to specify what methods are called, how they're called and what they should return.

If we take the above implementation of `doSomethingWithMock` and update it:

```go highlight=11
type UserDB interface {
  ReadByID(context.Context, id string) (...)
  CheckIfBanned(context.Context, id string) bool
}

func doSomethingWithMock(db UserDB) {
  // The original interaction that was asserted before:
  user, err := db.ReadByID(context.Background(), "foobar")
  // ...
  
  if db.CheckIfBanned(context.Background(), "foobar") {
    // do something here...
  }
}
```

_What happened here?_

We added a single additional method call in the implementation. If we were to run the tests again they would fail with something similar to:

```
Unexpected method call "CheckIfBanned" 
  Add it via On("CheckIfBanned") if required
```

_Sigh_

Anytime people see an error like that they'll copy exactly what it suggested to get the tests to pass. So lets do that!


```go highlight=7
func TestUserDB(t *testing.T) {
  // mocks.UserDB is the generated code that allows for testing against interactions.
  mockedUserDB := make(mocks.UserDB)

  // Assert taht a specific interfaction will occur otherwise the test should fail.
  mockedUserDB.On("ReadByID", mock.Anything).Returns(...)
  mockedUserDB.On("CheckIfBanned", mock.Anything).Returns(true)

  // Call some business logic that ends up using the mock
  doSomethingWithMock(mockedUserDB)
}
```

This made the test pass, great! But now the test has an exact copy of all the method calls the actual business logic is doing! In practice a test may end up looking like:

```go
// Test: A successful extension subscription post, automod pass, banned pass, suspended check pass
mocks.subscriptionNoticePublisher.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)
mocks.users.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(mockGetUserByIDResult, nil)
mocks.zuma.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(testZumaChecksPassedResp, nil)
mocks.pubsub.On("ChannelSubscription", mock.Anything, mock.Anything, mock.Anything).Return(nil)
mocks.tmi.On("SendExtendSubscriptionUserNotice", mock.Anything, mock.Anything, mock.Anything).Return(nil)
```

Anyone jumping into this code will have issues:

- Understanding tests become much harder because it's _mostly_ all mocking calls/setup. Knowing exactly what the test is supposed to do becomes harder the longer a test is.
- Business logic is exposed, including every method on mocked interfaces that are called. This increases the cost of maintaining these tests and being able to make changes to them. Most engineers won't have context on every dependency a system has, nor should they.
- Making changes to the business logic becomes a hastle. If new method calls are added then every test will need to be updated. But wait? Weren't tests written to provide confidence in code changes in the first place?

**Aren't tests supposed to give confidence when code changes?**

Yes! A small guiding rule is that tests should not need to be changed if the implementation's behaviour has not changed. If a non-behaviour change required tests to also change, how are we confident the tests weren't incorrectly updated?

There are obviously exceptions to this rule such as refactoring tests themselves to make them use new patterns and such or if large refactors were done with packages and types.

<Callout emoji="💡">
  As a codebase grows, you will inevitably need to make changes to existing code. When poorly written, automated tests can make it more difficult to mkae those changes. Brittle tests &mdash; those that over-specify expected outcomes or rely on extensive and complicated boilerplate &mdash; can actually resist change. These poorly written tests can fail even when unrelated changes are made.
</Callout>

If you have ever made a five-line change to a feature only to find dozens of unrelated, broken tests, you have felt the friction of brittle tests. Some of the worst offenders of brittle tests come from the misuse of mocks.

<Callout emoji="✋">
Google's codebase has suffered so badly from an abuse of mocking frameworks that it has led some engineers to declare "no more mocks!" &mdash; <i>Software Engineering at Google book</i>
</Callout>