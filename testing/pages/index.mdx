import Callout from 'nextra-theme-docs/callout'
import Features from 'components/features'

# Introduction

There are a variety of different methods used to write tests for Golang services and thus a need to document each of them and provide a set of resources so that engineers can write high-quality tests and services maintain consistency across the organization.

<Callout emoji="👑">
  These docs are apart of the Commerce 2021 testing initiative to level-up our service tests and produce high-quality services.
  <br/>
</Callout>

## Overview

There are a few different types of tests that can be written:

- **Unit Tests**: To run unit tests should never require anything more than running `go test`. They should never have dependencies such as a Docker container or a staging resource.
- **Integration Tests**: These test against dependencies (usually real ones). Many people use integration tests and E2E tests interchangeably. This documentation specifies integration tests as testing specific components, not end-to-end systems/APIs.
- **End-to-End (E2E) Tests**: These are similar to integration tests but operate at a much higher level and usually involves testing a system end-to-end.

## Current Approaches

Commerce has traditionally relied mostly on unit tests, with very few integration and E2E tests across services. One reason for this is how difficult writing integration & E2E tests become compared to unit tests. The patterns become different and there needs to be real systems involved in the testing flow.

**Unit Test:**

```go highlight=2,4
func TestUserDBWithMock(t *testing.T) {
  userDB := make(mocks.UserDB)

  userDB.On("ReadByID", mock.Anything, "foobar").Returns(nil, errors.New("not found"))

  user, err := userDB.ReadByID(ctx, "foobar")
  assert.NotNil(t, err)
  
  // ...
}
```

**Integration Test:**

Compared to the unit test using mocks, the integration test needs to depend on a real implementation. This forces a separate test implementation because we can't use the mocks for integration tests.

```go highlight=3,5
func TestIntegrationUserDB(t *testing.T) {
  // Connect to a docker container to use dynamo locally or use a staging table.
  userDB, err := dynamodb.MakeUserDB(...)

  user, err := userDB.ReadByID(ctx, "foobar")
  assert.NotNil(t, err)
  
  // ...
}
```

**E2E Tests:**

E2E tests usually operate at the Twirp level and make real requests into the system, expecting some behaviour to happen.

```go highlight=2,4
func TestIntegrationUserDBAPI(t *testing.T) {
  client := makeTwirpClient()

  res, err := client.GetUserByID(ctx, &usertwirp.GetUserByID{
    Id: randomID(),
  })
  assert.NotNil(t, err)

  // ...
}
```

## Redundant Testing 

All 3 of the tests above required different implementations. Real-world tests would have far greater differences between each method and different challenges than shown in the examples even. 

<Callout emoji="💡">
  Tests derive their value from the trust engineers place in them. If testing becomes a productivity sink, constantly inducing toil and uncertainty, engineers will lose trust and begin to find workarounds.
</Callout>
