export default {
  github: 'https://git.xarth.tv/commerce/testing',
  siteGithub: 'https://git.xarth.tv/commerce/testing',
  titleSuffix: ' – Testing',
  logo: <>
    <span className="mx-2 font-extrabold hidden md:inline">Testing</span>
    <span className="text-gray-600 font-normal hidden md:inline whitespace-no-wrap">Golang</span>
  </>,
  head: <>
    {/* Favicons, meta */}
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/favicon/apple-touch-icon.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="32x32"
      href="/favicon/favicon-32x32.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="16x16"
      href="/favicon/favicon-16x16.png"
    />
    <link rel="manifest" href="/favicon/site.webmanifest" />
    <link
      rel="mask-icon"
      href="/favicon/safari-pinned-tab.svg"
      color="#000000"
    />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="theme-color" content="#ffffff" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0"
    />
    <meta httpEquiv="Content-Language" content="en" />
    <meta
      name="description"
      content="Golang Testing"
    />
    <meta
      name="og:description"
      content="Golang Testing"
    />
    <meta
      name="og:title"
      content="Testing: Golang"
    />
  </>
}
