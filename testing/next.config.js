const prod = process.env.NODE_ENV === 'production';
const url = 'https://git.xarth.tv/pages/commerce/testing/';
const withNextra = require('nextra')('nextra-theme-docs', './theme.config.js')
module.exports = withNextra({
    assetPrefix: prod? url : '',
    basePath: prod? '/pages/commerce/testing': '',
    'process.env.BACKEND_URL':  prod? url : ''
});
