package api

import (
	"fmt"
	"net/http"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"goji.io/pat"
	"golang.org/x/net/context"

	log "github.com/sirupsen/logrus"
)

const (
	// URL resource path to search for Twitch related products on Amazon order history pages
	TwitchOrderHistorySearchPath = "/gp/your-account/order-history/ref=oh_aui_search?opt=ab&search=Twitch"

	// The default active marketplace to use if the tuid has no Fuel purchase activity
	DefaultActiveMarketplace = "ATVPDKIKX0DER" // Obfuscated US Marketplace Id

	// Paypal display text and dashboard Uri
	PaypalDisplayText  = "Paypal"
	PaypalDashboardUri = "www.paypal.com/mep/dashboard"
)

// GetActiveMarketplacesAPI represents the API to get emoticons
type GetActiveMarketplacesAPI struct {
	ActiveMarketplaceDAO   dynamo.IActiveMarketplaceDAO
	GatingHelper           auth.IGatingHelper
	MarketplaceToDomainMap map[string]string
	IsPaypalLaunched       bool
}

func (getActiveMarketplacesAPI *GetActiveMarketplacesAPI) getActiveMarketplaces(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(ctx, "tuid")
	if tuid == "" {
		msg := "Error getting active marketplaces: tuid is nil"
		log.Errorf(msg)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}

	// Authorization: Tuid being looked up must be the same as the acting tuid
	userIsId, err := getActiveMarketplacesAPI.GatingHelper.UserIsId(r, tuid)
	if err != nil {
		log.Warnf("Could not authorize current user to getActiveMarketplacesAPI for Twitch user %s: %s", tuid, err)
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	} else if !userIsId {
		errorMsg := fmt.Sprintf("Not authorized to getActiveMarketplacesAPI for Twitch user %s", tuid)
		log.Warn(errorMsg)
		http.Error(w, errorMsg, http.StatusForbidden)
		return
	}

	ddbResp, err := getActiveMarketplacesAPI.ActiveMarketplaceDAO.GetMarketplaces(tuid)
	if err != nil {
		log.Errorf("Error fetching active marketplaces: %s", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	resp := janus.GetActiveMarketplacesResponse{}
	activeMarketplaces := make([]janus.ActiveMarketplace, 0)
	if len(ddbResp) < 1 {
		// If there are no results, default to the US Marketplace
		defaultDomain := getActiveMarketplacesAPI.MarketplaceToDomainMap[DefaultActiveMarketplace]
		defaultActiveMarketplace := DomainToActiveMarketplace(defaultDomain)
		activeMarketplaces = append(activeMarketplaces, defaultActiveMarketplace)
	} else {
		for _, activeMarketplaceDDB := range ddbResp {
			domain, ok := getActiveMarketplacesAPI.MarketplaceToDomainMap[activeMarketplaceDDB.MarketplaceID]
			if !ok {
				log.Errorf("Unrecognized marketplace value: %s", activeMarketplaceDDB.MarketplaceID)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			activeMarketplace := DomainToActiveMarketplace(domain)
			activeMarketplaces = append(activeMarketplaces, activeMarketplace)
		}
	}

	// Authorization: Staff check
	isStaff, err := getActiveMarketplacesAPI.GatingHelper.IsAdminOrSubAdmin(r, tuid)
	if err != nil {
		log.Warnf("Failure performing Admin check for %s: %s", tuid, err)
		isStaff = false
	}

	// Hard-coded adding Paypal domain for supporting inventory page
	if isStaff || getActiveMarketplacesAPI.IsPaypalLaunched {
		paypalActiveMarketplace := PaypalActiveMarketplace()
		activeMarketplaces = append(activeMarketplaces, paypalActiveMarketplace)
	}

	resp.ActiveMarketplaces = activeMarketplaces
	writeResponse(resp, w)
}

func PaypalActiveMarketplace() janus.ActiveMarketplace {
	return janus.ActiveMarketplace{
		DisplayText:             PaypalDisplayText,
		OrderHistoryRedirectURL: PaypalDashboardUri,
	}
}

func DomainToActiveMarketplace(domain string) janus.ActiveMarketplace {
	return janus.ActiveMarketplace{
		DisplayText:             domain,
		OrderHistoryRedirectURL: domain + TwitchOrderHistorySearchPath,
	}
}
