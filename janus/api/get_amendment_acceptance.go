package api

import (
	"fmt"
	"net/http"

	"goji.io/pat"

	"time"

	log "github.com/sirupsen/logrus"

	"strconv"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/revenue/moneypenny/client"
	"code.justin.tv/web/users-service/client"
	"golang.org/x/net/context"
)

// AmendmentAcceptanceAPI represents the API to get amendment acceptance
type AmendmentAcceptanceAPI struct {
	AmendmentDAO       dynamo.IAmendmentDAO
	CartmanAuthorizer  auth.ICartmanAuthorizer
	GatingHelper       auth.IGatingHelper
	AmendmentTypes     map[string]bool
	SpadeClient        spade.Client
	UsersServiceClient usersservice.Client
	MoneyPennyClient   moneypenny.Client
	MetricLogger       metrics.IMetricLogger
}

type AmendmentEvent struct {
	ServerTime int64  `json:"server_time"`
	Channel    string `json:"channel"`
	ChannelID  int64  `json:"channel_id"`
}

func (amendmentAcceptanceAPI *AmendmentAcceptanceAPI) getAmendmentAcceptance(c context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(c, "tuid")
	amendmentType := pat.Param(c, "amendmentType")

	actingTuid, err := amendmentAcceptanceAPI.CartmanAuthorizer.ExtractActingTuid(r)
	if err != nil {
		log.Errorf("Error determining acting user: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gatingHelper := amendmentAcceptanceAPI.GatingHelper
	authChain := []auth.AuthChainFunction{
		gatingHelper.UserIsIdWrapper(r, tuid),
		gatingHelper.IsAdminOrSubAdminWrapper(r, actingTuid),
	}

	allow, errs := gatingHelper.CheckAuthChain(authChain)
	if !allow {
		if len(errs) != 0 {
			errorMessage := fmt.Sprintf("Error authorizing getAmendmentAcceptance: %s", errs)
			log.Error(errorMessage)
			http.Error(w, errorMessage, http.StatusInternalServerError)
			return
		} else {
			log.Warnf("Not authorized to getAmendmentAcceptance for Twitch user %s", tuid)
			http.Error(w, "400 Bad Request", http.StatusForbidden)
			return
		}
	}

	result, err := amendmentAcceptanceAPI.AmendmentDAO.Get(tuid, amendmentType)
	if err != nil {
		log.Errorf("Error retrieving amendment from DAO: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	amendmentAccepted := result != nil

	response := janus.GetAmendmentAcceptanceResponse{
		AmendmentAccepted: amendmentAccepted,
	}

	writeResponse(response, w)
}

func (amendmentAcceptanceAPI *AmendmentAcceptanceAPI) setAmendmentAcceptance(c context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(c, "tuid")
	amendmentType := pat.Param(c, "amendmentType")

	_, isAmendmentTypeAllowed := amendmentAcceptanceAPI.AmendmentTypes[amendmentType]
	if !isAmendmentTypeAllowed {
		log.Warnf("Error, attempting to sign amendment type %s for Twitch user %s", amendmentType, tuid)
		http.Error(w, "400 Bad Request", http.StatusBadRequest)
		return
	}

	startTime := time.Now()
	resp, err := amendmentAcceptanceAPI.MoneyPennyClient.GetUserPayoutType(context.Background(), tuid, nil)
	amendmentAcceptanceAPI.MetricLogger.LogDurationSinceMetric("moneyPennyClient.getUserPayoutType", startTime)

	if err != nil {
		log.Errorf("Error calling moneyPennyClient.GetPayoutType: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else if !resp.IsPartner {
		errorMsg := fmt.Sprintf("Cannot setAmendmentAcceptance for non-partner Twitch user %s", tuid)
		log.Warn(errorMsg)
		http.Error(w, errorMsg, http.StatusForbidden)
		return
	}

	gatingHelper := amendmentAcceptanceAPI.GatingHelper
	userIsId, err := gatingHelper.UserIsId(r, tuid)
	if err != nil {
		log.Errorf("Could not authorize current user to setAmendmentAcceptance for Twitch user %s: %s", tuid, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else if !userIsId {
		errorMsg := fmt.Sprintf("Not authorized to setAmendmentAcceptance for Twitch user %s", tuid)
		log.Warn(errorMsg)
		http.Error(w, errorMsg, http.StatusForbidden)
		return
	}

	response, err := amendmentAcceptanceAPI.setAmendmentImpl(tuid, amendmentType)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	writeResponse(response, w)
}

func (amendmentAcceptanceAPI *AmendmentAcceptanceAPI) adminSetAmendmentAcceptance(c context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(c, "tuid")
	amendmentType := pat.Param(c, "amendmentType")

	_, isAmendmentTypeAllowed := amendmentAcceptanceAPI.AmendmentTypes[amendmentType]
	if !isAmendmentTypeAllowed {
		log.Warnf("Error, attempting to sign amendment type %s for Twitch user %s", amendmentType, tuid)
		http.Error(w, "400 Bad Request", http.StatusBadRequest)
		return
	}

	response, err := amendmentAcceptanceAPI.setAmendmentImpl(tuid, amendmentType)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	writeResponse(response, w)
}

// trackEvent will send the fuel_server_agreement_form event to spade. If any errors arise, they will be logged
// as errors and swallowed so that the API call is not impacted.
func (amendmentAcceptanceAPI *AmendmentAcceptanceAPI) trackEvent(tuid string) {
	userProperties, err := amendmentAcceptanceAPI.UsersServiceClient.GetUserByID(context.Background(), tuid, nil)
	if err != nil {
		log.Errorf("Error calling user service for spade event: %v", err)
		return
	}

	tuidInt, err := strconv.ParseInt(tuid, 10, 64)
	if err != nil {
		log.Errorf("Error parsing tuid for spade event: %v", err)
		return
	}

	amendmentEvent := AmendmentEvent{
		Channel:    *userProperties.Displayname,
		ChannelID:  tuidInt,
		ServerTime: time.Now().Unix(),
	}

	if err := amendmentAcceptanceAPI.SpadeClient.TrackEvent(context.Background(), "fuel_server_agreement_form", &amendmentEvent); err != nil {
		log.Errorf("Error calling Spade: %v", err)
		return
	}
}

// setAmendmentImpl is a handler that will sign a user to the broadcaster amendment
func (amendmentAcceptanceAPI *AmendmentAcceptanceAPI) setAmendmentImpl(tuid string, amendmentType string) (janus.SetAmendmentAcceptanceResponse, error) {
	amendment := &dynamo.Amendment{
		UserID:         tuid,
		AmendmentType:  amendmentType,
		AcceptanceDate: time.Now(),
	}

	if err := amendmentAcceptanceAPI.AmendmentDAO.Put(amendment); err != nil {
		log.Errorf("Error putting amendment into DAO for Twitch user: %s, error: %s", tuid, err)
		return janus.SetAmendmentAcceptanceResponse{}, err
	}

	amendmentAcceptanceAPI.trackEvent(tuid)

	return janus.SetAmendmentAcceptanceResponse{
		AcceptDate: amendment.AcceptanceDate,
	}, nil
}
