package api

import (
	"time"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/crate"
	"code.justin.tv/commerce/janus/metadata"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"

	"golang.org/x/text/language"
)

var (
	adgDiscoClient                    *mocks.IADGDiscoClientWrapper
	adgEntitleClient                  *mocks.IADGEntitleClientWrapper
	badgesClient                      *mocks.IChatBadgesClientWrapper
	broadcasterHelper                 *mocks.IBroadcasterHelper
	payoutsEligibilityChecker         *mocks.IPayoutsEligibilityChecker
	makoClient                        *mocks.IMakoClientWrapper
	merchandiseMetadataDAO            *mocks.IMerchandiseMetadataDAO
	merchandiseLocalizationDAO        *mocks.IMerchandiseLocalizationDAO
	merchandiseLifestyleImagesDAO     *mocks.IMerchandiseLifestyleImagesDAO
	amendmentDao                      *mocks.IAmendmentDAO
	moneyPennyClient                  *mocks.IMoneyPennyClient
	fuelRevenueDao                    *mocks.IFuelRevenueDAO
	extensionsRevenueDao              *mocks.IExtensionsRevenueDAO
	cartmanAuthorizer                 *mocks.ICartmanAuthorizer
	metricLogger                      *mocks.IMetricLogger
	gatingHelper                      *mocks.IGatingHelper
	fuelChatNotificationDAO           *mocks.IFuelChatNotificationDAO
	activeMarketplacesDAO             *mocks.IActiveMarketplaceDAO
	fuelChatNotificationContentsDAO   *mocks.IFuelChatNotificationContentsDAO
	usersService                      *mocks.UsersServiceClient
	appSettings                       *mocks.Client
	asinMap                           *mocks.IAsinMap
	spadeClient                       *mocks.SpadeClient
	chatNotifier                      *mocks.INotifier
	pubsubNotifier                    *mocks.INotifier
	slackNotifier                     *mocks.INotifier
	tailsClient                       *mocks.ITailsClientWrapper
	broadcasterProductOffersDAO       *mocks.IProductOffersDAO
	broadcasterGameOffersBlacklistDAO *mocks.IBroadcasterGameOffersBlacklistDAO
	associatesClient                  *mocks.IAssociatesClientWrapper
)

const (
	checkoutURL = "checkout.com?"

	fuelPlatformURL  = "https://app.twitch.tv"
	fuelPlatformName = "Twitch"
	fuelPlatformText = "Play on ${launcherName}"

	testPlatformURL  = "platform url"
	testPlatformName = "platform name"
	testPlatformText = "platform text"

	testAcquisitionURL  = "acquisition url"
	testAcquisitionName = "acquisition name"
	testAcquisitionText = "acquisition text"

	detailPageRedirectURL = "detail-page-redirect-url"

	testAssociatesStoreID = "testAssociatesStoreID"
)

// The zero value of type Time is January 1, year 1, 00:00:00.000000000 UTC; Time is never nil.
var timeZero = time.Time{}
var timeNow = time.Now()
var timePast1 = timeNow.AddDate(-1, 0, 0)
var timeFuture1 = timeNow.AddDate(+1, 0, 0)
var timeFuture2 = timeNow.AddDate(+2, 0, 0)
var marketplaceToDomainMap = map[string]string{
	DefaultActiveMarketplace: "www.amazon.com",
	"A2EUQ1WTGCTBG2":         "www.amazon.ca",
}

func initAPI() (*Server, error) {
	adgDiscoClient = new(mocks.IADGDiscoClientWrapper)
	adgEntitleClient = new(mocks.IADGEntitleClientWrapper)
	associatesClient = new(mocks.IAssociatesClientWrapper)
	makoClient = new(mocks.IMakoClientWrapper)
	badgesClient = new(mocks.IChatBadgesClientWrapper)
	amendmentDao = new(mocks.IAmendmentDAO)
	fuelRevenueDao = new(mocks.IFuelRevenueDAO)
	extensionsRevenueDao = new(mocks.IExtensionsRevenueDAO)
	cartmanAuthorizer = new(mocks.ICartmanAuthorizer)
	metricLogger = new(mocks.IMetricLogger)
	moneyPennyClient = new(mocks.IMoneyPennyClient)
	gatingHelper = new(mocks.IGatingHelper)
	fuelChatNotificationDAO = new(mocks.IFuelChatNotificationDAO)
	fuelChatNotificationContentsDAO = new(mocks.IFuelChatNotificationContentsDAO)
	activeMarketplacesDAO = new(mocks.IActiveMarketplaceDAO)
	usersService = new(mocks.UsersServiceClient)
	appSettings = new(mocks.Client)
	asinMap = new(mocks.IAsinMap)
	spadeClient = new(mocks.SpadeClient)
	chatNotifier = new(mocks.INotifier)
	pubsubNotifier = new(mocks.INotifier)
	slackNotifier = new(mocks.INotifier)
	merchandiseMetadataDAO = new(mocks.IMerchandiseMetadataDAO)
	merchandiseLocalizationDAO = new(mocks.IMerchandiseLocalizationDAO)
	merchandiseLifestyleImagesDAO = new(mocks.IMerchandiseLifestyleImagesDAO)
	tailsClient = new(mocks.ITailsClientWrapper)
	broadcasterProductOffersDAO = new(mocks.IProductOffersDAO)
	broadcasterGameOffersBlacklistDAO = new(mocks.IBroadcasterGameOffersBlacklistDAO)
	broadcasterHelper = new(mocks.IBroadcasterHelper)
	payoutsEligibilityChecker = new(mocks.IPayoutsEligibilityChecker)

	fulfillmentDetails := models.FulfillmentMetadataMap{
		"fulfilled_fuellauncher_default": {
			AcquisitionDetails: models.AcquisitionDetails{
				IsExternalAcquisition:  false,
				AcquisitionDownloadURL: testAcquisitionURL,
				AcquisitionName:        testAcquisitionName,
				AcquisitionText:        testAcquisitionText,
			},
			PlatformDetails: models.PlatformDetails{
				IsExternalPlatform:  false,
				PlatformDownloadURL: fuelPlatformURL,
				PlatformName:        fuelPlatformName,
				PlatformText:        fuelPlatformText,
			},
		},
		"fulfilled_tpuf_ubisoft": {
			AcquisitionDetails: models.AcquisitionDetails{
				IsExternalAcquisition:  false,
				AcquisitionDownloadURL: testAcquisitionURL,
				AcquisitionName:        testAcquisitionName,
				AcquisitionText:        testAcquisitionText,
			},
			PlatformDetails: models.PlatformDetails{
				IsExternalPlatform:  true,
				PlatformDownloadURL: testPlatformURL,
				PlatformName:        testPlatformName,
				PlatformText:        testPlatformText,
			},
		},
	}

	gamesMetadata := models.GameMetadataMap{
		"buyablegame-hasmissingfulfillment": {
			ReleaseStartDate:    timePast1,
			ReleaseEndDate:      timeFuture1,
			GamePublicationDate: timePast1,
			GameEmbargoDate:     timeFuture1,
		},
		"buyablegame-hasnonefields": {
			ReleaseStartDate:    timePast1,
			ReleaseEndDate:      timeFuture1,
			GamePublicationDate: timePast1,
			GameEmbargoDate:     timeFuture1,
		},
		"buyablegame-hasmalformedvods": {
			ReleaseStartDate:    timePast1,
			ReleaseEndDate:      timeFuture1,
			GamePublicationDate: timePast1,
			GameEmbargoDate:     timeFuture1,
		},
		"buyablegame": {
			ReleaseStartDate:    timePast1,
			ReleaseEndDate:      timeFuture1,
			GamePublicationDate: timePast1,
			GameEmbargoDate:     timeFuture1,
		},
		"publishedgame": {
			ReleaseStartDate:    timeFuture1,
			ReleaseEndDate:      timeFuture2,
			GamePublicationDate: timePast1,
			GameEmbargoDate:     timeFuture2,
		},
		"futuregame": {
			ReleaseStartDate:    timeFuture1,
			ReleaseEndDate:      timeFuture2,
			GamePublicationDate: timeFuture1,
			GameEmbargoDate:     timeFuture2,
		},
	}

	asinMap.On("Get", "buyablegame-hasmissingfulfillment").Return(&janus.GameAsinMap{
		Asin:      "asin1-hasmissingfulfillment",
		GameTitle: "buyablegame-hasmissingfulfillment",
		Status:    "LIVE",
	}, nil)

	asinMap.On("Get", "buyablegame-hasnonefields").Return(&janus.GameAsinMap{
		Asin:      "asin1-hasnonefields",
		GameTitle: "buyablegame-hasnonefields",
		Status:    "LIVE",
	}, nil)

	asinMap.On("Get", "buyablegame-hasmalformedvods").Return(&janus.GameAsinMap{
		Asin:      "asin1-hasmalformedvods",
		GameTitle: "buyablegame-hasmalformedvods",
		Status:    "LIVE",
	}, nil)

	asinMap.On("Get", "buyablegame").Return(&janus.GameAsinMap{
		Asin:      "asin1",
		GameTitle: "buyablegame",
		Status:    "LIVE",
	}, nil)

	asinMap.On("Get", "publishedgame").Return(&janus.GameAsinMap{
		Asin:      "asin5",
		GameTitle: "publishedgame",
		Status:    "LIVE",
	}, nil)

	asinMap.On("Get", "futuregame").Return(&janus.GameAsinMap{
		Asin:      "asin2",
		GameTitle: "futuregame",
		Status:    "LIVE",
	}, nil)

	amendmentType := map[string]bool{
		"fuel": true,
	}

	gameMetadataDB := metadata.NewGameMetadataDB(gamesMetadata, asinMap)

	amendmentAcceptanceAPI := AmendmentAcceptanceAPI{
		AmendmentDAO:       amendmentDao,
		CartmanAuthorizer:  cartmanAuthorizer,
		GatingHelper:       gatingHelper,
		AmendmentTypes:     amendmentType,
		SpadeClient:        spadeClient,
		UsersServiceClient: usersService,
		MoneyPennyClient:   moneyPennyClient,
		MetricLogger:       metricLogger,
	}

	gameCommerceRevenueAPI := GameCommerceRevenueAPI{
		FuelRevenueDAO:       fuelRevenueDao,
		ExtensionsRevenueDAO: extensionsRevenueDao,
		CartmanAuthorizer:    cartmanAuthorizer,
		MoneyPennyClient:     moneyPennyClient,
		GatingHelper:         gatingHelper,
		MetricLogger:         metricLogger,
	}

	gameDetailsAPI := GameDetailsAPI{
		ADGDiscoClient:         adgDiscoClient,
		ADGEntitleClient:       adgEntitleClient,
		FulfillmentMetadataMap: fulfillmentDetails,
		GameMetadataDB:         gameMetadataDB,
		GatingHelper:           gatingHelper,
		CheckoutDestinationURL: checkoutURL,
		CrateHelper:            crate.NewCrateHelper(appSettings),
	}

	gameAsinAPI := GameAsinAPI{
		AsinMap: asinMap,
	}

	inGameContentDetailsAPI := InGameContentDetailsAPI{
		ADGDiscoClient:         adgDiscoClient,
		ADGEntitleClient:       adgEntitleClient,
		GameMetadataDB:         gameMetadataDB,
		GatingHelper:           gatingHelper,
		CheckoutDestinationURL: checkoutURL,
		CrateHelper:            crate.NewCrateHelper(appSettings),
	}

	broadcasterPayoutDetailsAPI := BroadcasterPayoutDetailsAPI{
		PayoutsEligibilityChecker: payoutsEligibilityChecker,
	}

	chatNotificationsAPI := ChatNotificationAPI{
		FuelChatNotificationDAO:         fuelChatNotificationDAO,
		FuelChatNotificationContentsDAO: fuelChatNotificationContentsDAO,
		CartmanAuthorizer:               cartmanAuthorizer,
		ChatBadgesClientWrapper:         badgesClient,
		MakoClientWrapper:               makoClient,
		ChatNotifier:                    chatNotifier,
		GatingHelper:                    gatingHelper,
		PubSubNotifier:                  pubsubNotifier,
		SlackNotifier:                   slackNotifier,
		ADGDiscoClient:                  adgDiscoClient,
		MetricLogger:                    metricLogger,
	}

	getCratesAPI := GetCratesAPI{
		ADGEntitleClient:     adgEntitleClient,
		ADGCratesDomainID:    "testID",
		GatingHelper:         gatingHelper,
		RSWOpenCrateEndpoint: "rswOpenCrateEndpoint",
	}

	getBadgesAPI := GetBadgesAPI{
		GatingHelper:            gatingHelper,
		MakoClientWrapper:       makoClient,
		ChatBadgesClientWrapper: badgesClient,
	}

	getEmoticonsAPI := GetEmoticonsAPI{
		GatingHelper:      gatingHelper,
		MakoClientWrapper: makoClient,
		MetricsLogger:     metricLogger,
	}

	getActiveMarketplacesAPI := GetActiveMarketplacesAPI{
		ActiveMarketplaceDAO:   activeMarketplacesDAO,
		GatingHelper:           gatingHelper,
		MarketplaceToDomainMap: marketplaceToDomainMap,
	}

	getMerchandiseAPI := GetMerchandiseAPI{
		ADGDiscoClient:                adgDiscoClient,
		MerchandiseMetadataDAO:        merchandiseMetadataDAO,
		MerchandiseLocalizationDAO:    merchandiseLocalizationDAO,
		SupportedLanguagesMatcher:     language.NewMatcher([]language.Tag{language.Make("en-US")}),
		MerchandiseLifestyleImagesDAO: merchandiseLifestyleImagesDAO,
	}

	extensionsProductsAPI := ExtensionsProductsAPI{
		ADGDiscoClient:            adgDiscoClient,
		CheckoutDestinationURL:    checkoutURL,
		SupportedLanguagesMatcher: language.NewMatcher([]language.Tag{language.Make("en-US"), language.Make("ko-KR"), language.Make("ja-JP")}),
		TailsClient:               tailsClient,
	}

	getOffersAPI := GetOffersAPI{
		ADGDiscoClient:                  adgDiscoClient,
		AssociatesClient:                associatesClient,
		BroadcasterHelper:               broadcasterHelper,
		DetailPageAssociatesRedirectURL: detailPageRedirectURL,
		GatingHelper:                    gatingHelper,
		BroadcasterProductOffersDAO:     broadcasterProductOffersDAO,
		IsDevelopment:                   true,
		TwitchAssociatesStoreID:         testAssociatesStoreID,
	}

	getOffersSettingsAPI := OffersSettingsAPI{
		BroadcasterHelper:           broadcasterHelper,
		BroadcasterProductOffersDAO: broadcasterProductOffersDAO,
		GatingHelper:                gatingHelper,
	}

	associatesStoreAPI := AssociatesStoreAPI{
		AssociatesClient:  associatesClient,
		CartmanAuthorizer: cartmanAuthorizer,
		GatingHelper:      gatingHelper,
	}

	serverAPI := ServerAPI{
		AmendmentAcceptanceAPI:      amendmentAcceptanceAPI,
		AssociatesStoreAPI:          associatesStoreAPI,
		BroadcasterPayoutDetailsAPI: broadcasterPayoutDetailsAPI,
		GameCommerceRevenueAPI:      gameCommerceRevenueAPI,
		GameDetailsAPI:              gameDetailsAPI,
		GameAsinAPI:                 gameAsinAPI,
		InGameContentDetailsAPI:     inGameContentDetailsAPI,
		ChatNotificationAPI:         chatNotificationsAPI,
		GetBadgesAPI:                getBadgesAPI,
		GetCratesAPI:                getCratesAPI,
		GetEmoticonsAPI:             getEmoticonsAPI,
		GetActiveMarketplacesAPI:    getActiveMarketplacesAPI,
		GetMerchandiseAPI:           getMerchandiseAPI,
		ExtensionsProductsAPI:       extensionsProductsAPI,
		GetOffersAPI:                getOffersAPI,
		OffersSettingsAPI:           getOffersSettingsAPI,
		//MetricLogger:              metricLogger,
	}

	return serverAPI.NewServer()
}
