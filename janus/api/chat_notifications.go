package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"io"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/notification"
	"code.justin.tv/common/goauthorization"
	log "github.com/sirupsen/logrus"
	"goji.io/pat"
	"golang.org/x/net/context"
)

const (
	// FuelChatNotificationStatusAvailable notifications that have not been used
	FuelChatNotificationStatusAvailable = "AVAILABLE"

	// FuelChatNotificationStatusShared notifications that have been shared in chat
	FuelChatNotificationStatusShared = "SHARED"

	// FuelChatNotificationStatusDismissed notifications that have been permanently dismissed
	FuelChatNotificationStatusDismissed = "DISMISSED"

	fuelChatNotificationExpirationTime = 48 * time.Hour

	genericBadRequest          = "Bad Request"
	genericInternalServerError = "Internal Server Error"

	consumeNotificationTypeShare   = "share"
	consumeNotificationTypeDismiss = "dismiss"

	contentEmote = "emote"
	contentBadge = "badge"
	contentBits  = "bits"
	contentIGC   = "igc"
)

// ChatNotificationAPI represents the API to get and consume chat notifications
type ChatNotificationAPI struct {
	FuelChatNotificationDAO         dynamo.IFuelChatNotificationDAO
	FuelChatNotificationContentsDAO dynamo.IFuelChatNotificationContentsDAO
	CartmanAuthorizer               auth.ICartmanAuthorizer
	GatingHelper                    auth.IGatingHelper
	ChatBadgesClientWrapper         clients.IChatBadgesClientWrapper
	ADGDiscoClient                  clients.IADGDiscoClientWrapper
	MakoClientWrapper               clients.IMakoClientWrapper
	MetricLogger                    metrics.IMetricLogger

	ChatNotifier   notification.INotifier
	SlackNotifier  notification.INotifier
	PubSubNotifier notification.INotifier
}

func (api *ChatNotificationAPI) getChatNotifications(c context.Context, w http.ResponseWriter, r *http.Request) {
	userID := pat.Param(c, "userID")
	channelID := r.Form.Get("channelID")

	if strings.TrimSpace(userID) == "" {
		http.Error(w, "UserID required", http.StatusBadRequest)
		return
	}

	if strings.TrimSpace(channelID) == "" {
		http.Error(w, "ChannelID required", http.StatusBadRequest)
		return
	}

	// Legacy Auth Method: Check that the Cartman token satisfies capabilities
	var isFromGQL bool = false
	startTime := time.Now()
	capabilities := goauthorization.CapabilityClaims{
		"get_chat_notifications": goauthorization.CapabilityClaim{
			"id": userID,
		},
	}
	err := api.CartmanAuthorizer.AuthorizeCartmanToken(r, &capabilities)
	if err != nil {
		// Future Auth Method: Manually check user_is_id
		userIsId, err := api.GatingHelper.UserIsId(r, userID)
		if err != nil {
			log.Warnf("Could not authorize current user to getChatNotifications for Twitch user %s: %s", userID, err)
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		} else if !userIsId {
			errorMsg := fmt.Sprintf("Not authorized to getChatNotifications for Twitch user %s", userID)
			log.Warn(errorMsg)
			http.Error(w, errorMsg, http.StatusForbidden)
			return
		}
		isFromGQL = true
	}
	api.MetricLogger.LogDurationSinceMetric(getMetricName("CartmanAuth", isFromGQL), startTime)

	startTime = time.Now()
	dynamoChatNotifications, err := api.FuelChatNotificationDAO.GetMostRecent(channelID, userID)
	if err != nil {
		log.Errorf("Error retrieving FuelChatNotifications from DAO: %v", err)
		http.Error(w, genericInternalServerError, http.StatusInternalServerError)
		return
	}
	api.MetricLogger.LogDurationSinceMetric(getMetricName("GetMostRecent", isFromGQL), startTime)

	startTime = time.Now()
	chatNotifications := make([]janus.ChatNotification, 0)
	for _, dynamoChatNotification := range dynamoChatNotifications {
		if shouldFilterNotification(dynamoChatNotification) {
			continue
		}

		startTimeGetAllContents := time.Now()
		contents, err := api.FuelChatNotificationContentsDAO.GetAll(dynamoChatNotification.Token)
		if err != nil {
			log.Errorf("Error retrieving FuelChatNotificationContents from DAO: %v", err)
			http.Error(w, genericInternalServerError, http.StatusInternalServerError)
			return
		}
		api.MetricLogger.LogDurationSinceMetric(getMetricName("GetAllChatContents", isFromGQL), startTimeGetAllContents)

		chatNotification := DynamoChatNotificationToAPIChatNotification(*dynamoChatNotification)

		rawNotificationContents, err := DynamoContentsToAPIContents(contents)
		if err != nil {
			log.Errorf("Error converting dynamo contents: %v", err)
			http.Error(w, genericInternalServerError, http.StatusInternalServerError)
			return
		}

		startTimeHydrateContents := time.Now()
		chatNotification.Contents, err = api.hydrateContents(c, rawNotificationContents, isFromGQL)
		if err != nil {
			log.Errorf("Error hydrating contents: %v", err)
			http.Error(w, genericInternalServerError, http.StatusInternalServerError)
			return
		}
		api.MetricLogger.LogDurationSinceMetric(getMetricName("HydrateContents", isFromGQL), startTimeHydrateContents)

		chatNotifications = append(chatNotifications, chatNotification)
	}
	api.MetricLogger.LogDurationSinceMetric(getMetricName("GetAllChatNotifications", isFromGQL), startTime)

	response := janus.GetChatNotificationsResponse{
		ChatNotifications: chatNotifications,
	}
	writeResponse(response, w)
	return
}

func (api *ChatNotificationAPI) consumeChatNotification(c context.Context, w http.ResponseWriter, r *http.Request) {
	userID := pat.Param(c, "userID")
	channelID := r.Form.Get("channelID")
	if strings.TrimSpace(userID) == "" {
		http.Error(w, "UserID required", http.StatusBadRequest)
		return
	}

	if strings.TrimSpace(channelID) == "" {
		http.Error(w, "ChannelID required", http.StatusBadRequest)
		return
	}

	// Legacy Auth Method: Check that the Cartman token satisfies capabilities
	var isFromGQL bool = false
	capabilities := goauthorization.CapabilityClaims{
		"consume_chat_notification": goauthorization.CapabilityClaim{
			"id": userID,
		},
	}
	err := api.CartmanAuthorizer.AuthorizeCartmanToken(r, &capabilities)
	if err != nil {
		// Future Auth Method: Manually check user_is_id
		userIsId, err := api.GatingHelper.UserIsId(r, userID)
		if err != nil {
			log.Warnf("Could not authorize current user to consumeChatNotification for Twitch user %s", userID)
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		} else if !userIsId {
			errorMsg := fmt.Sprintf("Not authorized to consumeChatNotifications for Twitch user %s", userID)
			log.Warn(errorMsg)
			http.Error(w, errorMsg, http.StatusForbidden)
			return
		}
		isFromGQL = true
	}

	request, err := api.parseRequest(r.Body)
	if err != nil {
		errMsg := fmt.Sprintf("Error parsing request: %s", err)
		log.Infof(errMsg)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	dynamoNotification, err := api.FuelChatNotificationDAO.Get(channelID, userID, request.Token)
	if err != nil {
		log.Errorf("Error getting FuelChatNotification from dynamo: %v", err)
		http.Error(w, genericInternalServerError, http.StatusInternalServerError)
		return
	}

	err = api.validateNotification(dynamoNotification)
	if err != nil {
		log.Warnf("Received a bad request to consume notification. UserID: %s, ChannelID: %s, Token: %s, Error: %s", userID, channelID, request.Token, err)
		http.Error(w, fmt.Sprintf("Cannot consume notification: %s", err), http.StatusBadRequest)
		return
	}

	chatNotification := DynamoChatNotificationToAPIChatNotification(*dynamoNotification)

	dynamoContents, err := api.FuelChatNotificationContentsDAO.GetAll(chatNotification.Token)
	if err != nil {
		log.Errorf("Error retrieving FuelChatNotificationContents from DAO: %v", err)
		http.Error(w, genericInternalServerError, http.StatusInternalServerError)
		return
	}

	rawNotificationContents, err := DynamoContentsToAPIContents(dynamoContents)
	if err != nil {
		log.Errorf("Error converting dynamo contents: %v", err)
		http.Error(w, genericInternalServerError, http.StatusInternalServerError)
		return
	}

	chatNotification.Contents, err = api.hydrateContents(c, rawNotificationContents, isFromGQL)
	if err != nil {
		log.Errorf("Error hydrating contents: %v", err)
		http.Error(w, genericInternalServerError, http.StatusInternalServerError)
		return
	}

	if request.Type == consumeNotificationTypeShare {
		// Send PubSub notification
		if err := api.PubSubNotifier.Notify(chatNotification, request.Message); err != nil {
			// Do not return early when PubSub fails, allow the Chat notification to proceed
			log.WithFields(log.Fields{
				"Channel": dynamoNotification.ChannelID,
				"UserId":  dynamoNotification.UserID,
				"error":   err,
			}).Error("Error sharing purchase in PubSub")
		}

		// Send Chat notification
		if err = api.ChatNotifier.Notify(chatNotification, request.Message); err != nil {
			log.Errorf("Error posting FuelChatNotification to chat: %v", err)
			http.Error(w, genericInternalServerError, http.StatusInternalServerError)
			return
		}
		dynamoNotification.Status = FuelChatNotificationStatusShared

		// Send Slack notification
		go func() {
			if slackErr := api.SlackNotifier.Notify(chatNotification, request.Message); slackErr != nil {
				log.Warnf("Error notifying slack: %s", slackErr)
			}
		}()
	} else {
		dynamoNotification.Status = FuelChatNotificationStatusDismissed
	}

	if err = api.FuelChatNotificationDAO.Put(dynamoNotification); err != nil {
		log.Errorf("Error updating FuelChatNotification in dynamo: %v", err)
		http.Error(w, genericInternalServerError, http.StatusInternalServerError)
		return
	}

	return
}

func (api *ChatNotificationAPI) parseRequest(body io.ReadCloser) (janus.ConsumeChatNotificationsRequest, error) {
	var request janus.ConsumeChatNotificationsRequest
	decoder := json.NewDecoder(body)
	if err := decoder.Decode(&request); err != nil {
		return request, fmt.Errorf("Error decoding request: %s", err)
	}

	if strings.TrimSpace(request.Token) == "" {
		return request, errors.New("Token required")
	}

	if request.Type != consumeNotificationTypeDismiss && request.Type != consumeNotificationTypeShare {
		return request, errors.New("Invalid consumption type")
	}

	if request.Type == consumeNotificationTypeShare {
		if strings.TrimSpace(request.Message) == "" {
			return request, errors.New("Message required")
		}
	}
	return request, nil
}

func (api *ChatNotificationAPI) validateNotification(notification *dynamo.FuelChatNotification) error {
	if notification == nil {
		return errors.New("Notification does not exist")
	}

	if notification.Status != FuelChatNotificationStatusAvailable {
		return errors.New("Notification has already been used")
	}

	if time.Now().Sub(notification.TimeReceived) >= fuelChatNotificationExpirationTime {
		return errors.New("Notification has expired")
	}
	return nil
}

func (api *ChatNotificationAPI) hydrateContents(ctx context.Context, notificationContents janus.NotificationContents, isFromGQL bool) (janus.NotificationContents, error) {
	resultContents := janus.NotificationContents{}

	badges, err := api.hydrateBadges(ctx, notificationContents.Badges, isFromGQL)
	if err != nil {
		return resultContents, fmt.Errorf("Error hydrating badges: %v", err)
	}
	resultContents.Badges = badges

	emoteSets, err := api.hydrateEmotes(ctx, notificationContents.EmoteSets, isFromGQL)
	if err != nil {
		return resultContents, fmt.Errorf("Error hydrating emotes: %v", err)
	}
	resultContents.EmoteSets = emoteSets

	igc, err := api.hydrateIGC(ctx, notificationContents.InGameContent, isFromGQL)
	if err != nil {
		return resultContents, fmt.Errorf("Error hydrating IGC: %v", err)
	}
	resultContents.InGameContent = igc

	resultContents.Bits = notificationContents.Bits
	resultContents.CrateCount = notificationContents.CrateCount
	return resultContents, nil
}

func (api *ChatNotificationAPI) hydrateBadges(ctx context.Context, rawBadges []janus.Badge, isFromGQL bool) ([]janus.Badge, error) {
	hydratedBadges := []janus.Badge{}

	if len(rawBadges) == 0 {
		return hydratedBadges, nil
	}

	startTimeGetBadgeDisplay := time.Now()
	badgeResult, err := api.ChatBadgesClientWrapper.GetBadgeDisplay(ctx)
	api.MetricLogger.LogDurationSinceMetric(getMetricName("GetBadgeDisplay", isFromGQL), startTimeGetBadgeDisplay)
	if err != nil {
		return hydratedBadges, fmt.Errorf("Error fetching badge details: %v", err)
	}

	badgeSets := badgeResult.BadgeSets
	for _, badgeContent := range rawBadges {
		badgeSet, exists := badgeSets[badgeContent.ID]
		if !exists {
			return hydratedBadges, fmt.Errorf("Could not find badge details for id: %s", badgeContent.ID)
		}

		version, err := clients.GetHighestBadgeVersion(badgeSet.Versions)
		if err != nil {
			return hydratedBadges, fmt.Errorf("Error finding version of badge: %+v , Error: %v", badgeSet.Versions, err)

		}

		badgeDetails := badgeSet.Versions[version]

		newBadge := janus.Badge{
			ClickAction: badgeDetails.ClickAction,
			ClickURL:    badgeDetails.ClickURL,
			ImageURL1X:  *badgeDetails.ImageURL1x,
			ImageURL2X:  *badgeDetails.ImageURL2x,
			ImageURL4X:  *badgeDetails.ImageURL4x,
			Title:       badgeDetails.Title,
			Description: badgeDetails.Description,
			ContentKey:  badgeContent.ContentKey,
			ID:          badgeContent.ID,
			Version:     version,
		}

		hydratedBadges = append(hydratedBadges, newBadge)
	}
	return hydratedBadges, nil
}

func (api *ChatNotificationAPI) hydrateEmotes(ctx context.Context, rawEmotes map[string][]janus.Emoticon, isFromGQL bool) (map[string][]janus.Emoticon, error) {
	hydratedEmotes := make(map[string][]janus.Emoticon)

	if len(rawEmotes) == 0 {
		return hydratedEmotes, nil
	}

	emoteSetKeys := []string{}
	for key := range rawEmotes {
		emoteSetKeys = append(emoteSetKeys, key)
	}

	startTimeGetEmoticonSetsDetails := time.Now()
	chatResp, err := api.MakoClientWrapper.GetEmoticonsByGroups(ctx, emoteSetKeys)
	api.MetricLogger.LogDurationSinceMetric(getMetricName("GetEmoticonSetsDetails", isFromGQL), startTimeGetEmoticonSetsDetails)
	if err != nil {
		return hydratedEmotes, fmt.Errorf("Error fetching emoticon sets details: %v", err)
	}

	for key, emoticons := range (*chatResp).Details {
		hydratedEmotes[key] = []janus.Emoticon{}

		rawSet, exists := rawEmotes[key]
		if !exists {
			return hydratedEmotes, fmt.Errorf("Error while hydrating emotes - raw set has no entry for key: %s", key)
		}
		if len(rawSet) == 0 {
			return hydratedEmotes, fmt.Errorf("Error while hydrating emotes - raw set is empty")
		}
		contentKey := rawSet[0].ContentKey

		for _, emoticon := range emoticons {
			hydratedEmotes[key] = append(hydratedEmotes[key], janus.Emoticon{
				ID:         emoticon.ID,
				Code:       emoticon.Pattern,
				ContentKey: contentKey,
			})
		}
	}

	return hydratedEmotes, nil
}

func (api *ChatNotificationAPI) hydrateIGC(ctx context.Context, rawIGC []janus.InGameContentNotification, isFromGQL bool) ([]janus.InGameContentNotification, error) {
	hydratedIGC := []janus.InGameContentNotification{}

	if len(rawIGC) == 0 {
		return hydratedIGC, nil
	}

	// For now, there will only be 1 IGC item per crate opening so this loop is technically unncessary
	// If/when we start giving more IGC per crate, we should add and use GetProductListDetails
	for _, igc := range rawIGC {
		startTimeGetGameProductDetails := time.Now()
		adgProduct, err := api.ADGDiscoClient.GetGameProductDetails(igc.Asin, igc.UserID)
		api.MetricLogger.LogDurationSinceMetric(getMetricName("GetGameProductDetails", isFromGQL), startTimeGetGameProductDetails)
		if err != nil {
			return hydratedIGC, fmt.Errorf("Error while hydrating igc from adg disco: %v", err)
		}

		newIGC := janus.InGameContentNotification{
			Asin:        igc.Asin,
			ContentKey:  igc.ContentKey,
			UserID:      igc.UserID,
			BoxArtURL:   adgProduct.IconURL,
			Title:       adgProduct.Title,
			Description: adgProduct.Details.ShortDescription,
		}

		hydratedIGC = append(hydratedIGC, newIGC)
	}

	return hydratedIGC, nil
}

// DynamoChatNotificationToAPIChatNotification converts dynamo chat notification to api chat notification
func DynamoChatNotificationToAPIChatNotification(dynamoChatNotification dynamo.FuelChatNotification) janus.ChatNotification {
	return janus.ChatNotification{
		ChannelID:    dynamoChatNotification.ChannelID,
		UserID:       dynamoChatNotification.UserID,
		Token:        dynamoChatNotification.Token,
		ASIN:         dynamoChatNotification.ASIN,
		Title:        dynamoChatNotification.ProductTitle,
		ImageURL:     dynamoChatNotification.ProductImageURL,
		Status:       dynamoChatNotification.Status,
		TimeReceived: dynamoChatNotification.TimeReceived,
	}
}

// DynamoContentsToAPIContents converts dynamo notification contents to api notification contents
func DynamoContentsToAPIContents(dynamoContents []*dynamo.FuelChatNotificationContent) (janus.NotificationContents, error) {
	janusContents := janus.NotificationContents{
		EmoteSets: make(map[string][]janus.Emoticon),
	}
	for _, content := range dynamoContents {
		switch content.ContentType {
		case contentEmote:
			janusContents.EmoteSets[content.ContentID] = []janus.Emoticon{
				{
					ContentKey: content.ContentKey,
				},
			}
		case contentBadge:
			badge := janus.Badge{
				ID:         content.ContentID,
				ContentKey: content.ContentKey,
			}
			janusContents.Badges = append(janusContents.Badges, badge)
		case contentBits:
			bits := janus.Bits{
				Quantity:   content.Quantity,
				ContentKey: content.ContentKey,
			}
			janusContents.Bits = append(janusContents.Bits, bits)
		case contentIGC:
			igc := janus.InGameContentNotification{
				Asin:       content.ContentID,
				ContentKey: content.ContentKey,
				UserID:     content.UserID,
			}
			janusContents.InGameContent = append(janusContents.InGameContent, igc)
		default:
			return janusContents, fmt.Errorf("Unexpected type while parsing contents: %s", content.ContentType)
		}
	}

	janusContents.CrateCount = countEvents(dynamoContents)
	return janusContents, nil
}

// count the number of events related to a slice of contents. this is done by counting the number of unique eventIds
func countEvents(dynamoContents []*dynamo.FuelChatNotificationContent) int {
	eventSet := map[string]bool{}
	for _, content := range dynamoContents {
		_, exists := eventSet[content.EventID]
		if !exists {
			eventSet[content.EventID] = true
		}
	}
	return len(eventSet)
}

// Filter function for chat notifications. Returns true if the notification should be filtered out, false otherwise.
func shouldFilterNotification(dynamoChatNotification *dynamo.FuelChatNotification) bool {
	return (dynamoChatNotification == nil ||
		dynamoChatNotification.Status != FuelChatNotificationStatusAvailable ||
		time.Now().Sub(dynamoChatNotification.TimeReceived) > fuelChatNotificationExpirationTime)
}

func getMetricName(metricSuffix string, isFromGQL bool) string {
	if isFromGQL {
		return fmt.Sprintf("getChatNotifications.%s.gql", metricSuffix)
	} else {
		return fmt.Sprintf("getChatNotifications.%s.v5", metricSuffix)
	}
}
