package api

import (
	"errors"
	"net/http/httptest"
	"testing"

	"fmt"
	"net/http"

	"encoding/json"
	"io/ioutil"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	mockShortTitle         = "Mock Short Title"
	mockTwitchAssociatesID = "mockTwitchStoreID"
)

// Test the GetOffers API
func TestGetOffers(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		userID := "12345"
		gameID := "11111"
		broadcasterID := "22222"
		mockAsin := "mockAsin"
		whitelistedViewerCountryCode := "US"
		nonWhitelistedViewerCountryCode := "SK"

		expectedDetailPageURL := detailPageRedirectURL + "asin=" + mockAsin + "&br_id=" + broadcasterID

		Convey("when /offers/<gameid>/ is requested", func() {
			Convey("with missing gameID", func() {
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s", server.URL, ""), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldBeEmpty)
			})

			Convey("with no amazon retail items associated", func() {
				daoResponse := []*dynamo.ProductOffers{}
				associatesResponse := &models.LinkedAssociatesStore{}
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldBeEmpty)
			})

			Convey("with one amazon retail item associated but no offer", func() {
				daoResponse := []*dynamo.ProductOffers{&dynamo.ProductOffers{Asin: mockAsin, Status: dynamo.StatusLive}}
				mockProduct := createMockProductForGetOffersAPI(mockAsin)
				mockProduct.Price.Price = ""
				discoResponse := []models.ADGProduct{*mockProduct}
				associatesResponse := &models.LinkedAssociatesStore{}
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgDiscoClient.On("GetAmazonRetailDetails", Anything, Anything).Return(discoResponse, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldBeEmpty)
			})

			Convey("with one amazon retail item associated", func() {
				daoResponse := []*dynamo.ProductOffers{&dynamo.ProductOffers{Asin: mockAsin, Status: dynamo.StatusLive}}
				discoResponse := []models.ADGProduct{*createMockProductForGetOffersAPI(mockAsin)}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgDiscoClient.On("GetAmazonRetailDetails", Anything, Anything).Return(discoResponse, nil)

				Convey("GatingHelper throws an error getting active tuid", func() {
					associatesResponse := &models.LinkedAssociatesStore{}
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
					gatingHelper.On("GetOptionalActingTuid", Anything).Return("", errors.New("test error"))
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetOffersResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
					So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

					for _, retailProduct := range parsedResponse.RetailProducts {
						verifyRetailProduct(retailProduct)
						So(retailProduct.DetailPageURL, ShouldStartWith, expectedDetailPageURL)
					}
				})

				Convey("GatingHelper throws an error checking whether acting tuid is staff", func() {
					associatesResponse := &models.LinkedAssociatesStore{}
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
					gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, errors.New("test error"))
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetOffersResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
					So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

					for _, retailProduct := range parsedResponse.RetailProducts {
						verifyRetailProduct(retailProduct)
						So(retailProduct.DetailPageURL, ShouldStartWith, expectedDetailPageURL)
					}
				})

				Convey("User is authenticated", func() {
					gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)

					Convey("Associates client returns no linked store", func() {
						associatesResponse := &models.LinkedAssociatesStore{StoreID: ""}
						associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
						So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

						for _, retailProduct := range parsedResponse.RetailProducts {
							verifyRetailProduct(retailProduct)
							So(retailProduct.DetailPageURL, ShouldEndWith, testAssociatesStoreID)
						}
					})

					Convey("Associates client returns with status 'NotLinked'", func() {
						associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "NotLinked"}
						associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
						So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

						for _, retailProduct := range parsedResponse.RetailProducts {
							verifyRetailProduct(retailProduct)
							So(retailProduct.DetailPageURL, ShouldEndWith, testAssociatesStoreID)
						}
					})

					Convey("Associates client returns with status 'LinkedRequiresAttention'", func() {
						associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "LinkedRequiresAttention"}
						associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
						So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

						for _, retailProduct := range parsedResponse.RetailProducts {
							verifyRetailProduct(retailProduct)
							So(retailProduct.DetailPageURL, ShouldEndWith, testAssociatesStoreID)
						}
					})

					Convey("Associates client returns with status 'Linked'", func() {
						associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "Linked"}
						associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
						So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

						for _, retailProduct := range parsedResponse.RetailProducts {
							So(retailProduct.DetailPageURL, ShouldEndWith, mockTwitchAssociatesID)
						}
					})

					Convey("Associates client returns with status 'Linked_Missing_Information'", func() {
						associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "Linked_Missing_Information"}
						associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
						So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

						for _, retailProduct := range parsedResponse.RetailProducts {
							verifyRetailProduct(retailProduct)
							So(retailProduct.DetailPageURL, ShouldEndWith, mockTwitchAssociatesID)
						}
					})
				})
			})

			Convey("with greater than max amazon retail items associated", func() {
				daoResponse := []*dynamo.ProductOffers{}
				discoResponse := []models.ADGProduct{}
				for i := 0; i < maxItemDisplayNum+1; i++ {
					daoResponse = append(daoResponse, &dynamo.ProductOffers{Asin: mockAsin, Status: dynamo.StatusLive})
					discoResponse = append(discoResponse, *createMockProductForGetOffersAPI(mockAsin))
				}

				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				adgDiscoClient.On("GetAmazonRetailDetails", Anything, Anything).Return(discoResponse, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(true)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(true, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
				So(len(parsedResponse.RetailProducts), ShouldEqual, maxItemDisplayNum)

				for _, retailProduct := range parsedResponse.RetailProducts {
					verifyRetailProduct(retailProduct)
					So(retailProduct.DetailPageURL, ShouldStartWith, expectedDetailPageURL)
				}
			})

			Convey("with amazon retail items with short title", func() {
				daoResponse := []*dynamo.ProductOffers{
					&dynamo.ProductOffers{Asin: mockAsin, Status: dynamo.StatusLive},
				}
				associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgProduct := createMockProductForGetOffersAPI(mockAsin)
				adgProduct.Details.Attributes.ShortTitle = mockShortTitle
				discoResponse := []models.ADGProduct{*adgProduct}
				adgDiscoClient.On("GetAmazonRetailDetails", Anything, Anything).Return(discoResponse, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(true)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(true, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
				So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

				for _, retailProduct := range parsedResponse.RetailProducts {
					verifyRetailProduct(retailProduct)
					So(retailProduct.Title, ShouldEqual, mockShortTitle)
					So(retailProduct.DetailPageURL, ShouldStartWith, expectedDetailPageURL)
				}
			})

			Convey("With a broadcaster that is not eligible for offers", func() {
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(false, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldBeEmpty)
			})

			Convey("With a viewer country code of US", func() {
				daoResponse := []*dynamo.ProductOffers{
					&dynamo.ProductOffers{Asin: mockAsin, Status: dynamo.StatusLive},
				}
				associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgProduct := createMockProductForGetOffersAPI(mockAsin)
				adgProduct.Details.Attributes.ShortTitle = mockShortTitle
				discoResponse := []models.ADGProduct{*adgProduct}
				adgDiscoClient.On("GetAmazonRetailDetails", Anything, Anything).Return(discoResponse, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(true)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(true, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldNotBeEmpty)
				So(len(parsedResponse.RetailProducts), ShouldEqual, 1)

				for _, retailProduct := range parsedResponse.RetailProducts {
					verifyRetailProduct(retailProduct)
					So(retailProduct.DetailPageURL, ShouldEndWith, testAssociatesStoreID)
				}
			})

			Convey("With a non-whitelisted viewer country code", func() {
				daoResponse := []*dynamo.ProductOffers{
					&dynamo.ProductOffers{Asin: mockAsin, Status: dynamo.StatusLive},
				}
				associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgProduct := createMockProductForGetOffersAPI(mockAsin)
				adgProduct.Details.Attributes.ShortTitle = mockShortTitle
				discoResponse := []models.ADGProduct{*adgProduct}
				adgDiscoClient.On("GetAmazonRetailDetails", Anything, Anything).Return(discoResponse, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(true)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(true, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, nonWhitelistedViewerCountryCode), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.RetailProducts, ShouldBeEmpty)
			})

			Convey("ProductOffers throws an error", func() {
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(nil, errors.New("test error"))
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("ADGDiscoClient throws an error", func() {
				daoResponse := []*dynamo.ProductOffers{&dynamo.ProductOffers{Asin: "mockAsin", Status: dynamo.StatusLive}}
				associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgDiscoClient.On("GetAmazonRetailDetails", Anything).Return(nil, errors.New("test error"))
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("BroadcasterHelper throws an error", func() {
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(false, errors.New("test error"))
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("Associates client throws error", func() {
				daoResponse := []*dynamo.ProductOffers{&dynamo.ProductOffers{Asin: "mockAsin", Status: dynamo.StatusLive}}
				discoResponse := []models.ADGProduct{*createMockProductForGetOffersAPI(mockAsin)}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
				adgDiscoClient.On("GetAmazonRetailDetails", Anything).Return(discoResponse, nil)
				gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(nil, errors.New("test error"))
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})
		})
	})
}

func createMockProductForGetOffersAPI(asin string) (product *models.ADGProduct) {
	product = &models.ADGProduct{}
	product.ASIN = asin
	product.Details.Crate = models.Crate{
		ASIN: "foo",
	}
	product.Title = "Mock Title"
	product.Description = "Mock Description"
	product.Price = models.Price{
		Price:        "9.99",
		CurrencyUnit: "$",
	}
	product.Details.Attributes.Brand = "Mock Brand"
	product.Details.FeaturesDetails = []string{"mock detail 1", "mock detail 2", "mock detail 3"}
	return product
}

func verifyRetailProduct(retailProduct janus.RetailProduct) {
	So(retailProduct.ASIN, ShouldNotBeNil)
	So(retailProduct.Title, ShouldNotBeNil)
	So(retailProduct.DetailBullets, ShouldNotBeEmpty)
	So(retailProduct.DetailPageURL, ShouldNotBeEmpty)
	So(retailProduct.Description, ShouldNotBeNil)
	So(retailProduct.Brand, ShouldNotBeNil)
	So(retailProduct.Price.Price, ShouldNotBeNil)
	So(retailProduct.Media.BoxArtURL, ShouldNotBeNil)
}
