package api

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/janus/client"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

// Test BroadcasterPayoutDetailsAPI TestGetBroadcasterPayoutDetails call
func TestGetBroadcasterPayoutDetails_Success(t *testing.T) {

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		metricLogger.On("LogDurationMetric", mock.Anything, mock.Anything).Return()
		req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/123/payout", server.URL), nil)
		So(err, ShouldBeNil)

		Convey("when the broadcaster is payout eligible", func() {
			payoutsEligibilityChecker.On("GetGameCommercePayoutEligibility", mock.Anything).Return(true, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBroadcasterPayoutDetailsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(parsedResponse.IsEligible, ShouldBeTrue)
		})

		Convey("when the broadcaster is not payout eligible", func() {
			payoutsEligibilityChecker.On("GetGameCommercePayoutEligibility", mock.Anything).Return(false, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBroadcasterPayoutDetailsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(parsedResponse.IsEligible, ShouldBeFalse)
		})

		Convey("when the payout helper throws an error", func() {
			payoutsEligibilityChecker.On("GetGameCommercePayoutEligibility", mock.Anything).Return(nil, errors.New("test error"))

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "500 Internal Server Error")
		})
	})
}

// Test BroadcasterPayoutDetailsAPI Test GetBroadcasterRetailPayoutDetails call
func TestGetBroadcasterRetailPayoutDetails_Success(t *testing.T) {

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		metricLogger.On("LogDurationMetric", mock.Anything, mock.Anything).Return()
		req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/123/payout/retail", server.URL), nil)
		So(err, ShouldBeNil)

		Convey("when the broadcaster is payout eligible", func() {
			payoutsEligibilityChecker.On("GetRetailPayoutEligibility", mock.Anything, mock.Anything).Return(true, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBroadcasterRetailPayoutDetailsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(parsedResponse.IsEligible, ShouldBeTrue)
		})

		Convey("when the broadcaster is not payout eligible", func() {
			payoutsEligibilityChecker.On("GetRetailPayoutEligibility", mock.Anything, mock.Anything).Return(false, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBroadcasterRetailPayoutDetailsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(parsedResponse.IsEligible, ShouldBeFalse)
		})

		Convey("when the payout helper throws an error", func() {
			payoutsEligibilityChecker.On("GetRetailPayoutEligibility", mock.Anything, mock.Anything).Return(false, errors.New("test error"))

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "500 Internal Server Error")
		})
	})
}
