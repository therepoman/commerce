package api

import (
	"fmt"
	"net/http"

	"goji.io/pat"

	"code.justin.tv/commerce/janus/associates"
	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// AssociatesStoreAPI represents the API to get information on a users linked associates store
type AssociatesStoreAPI struct {
	AssociatesClient  clients.IAssociatesClientWrapper
	CartmanAuthorizer auth.ICartmanAuthorizer
	GatingHelper      auth.IGatingHelper
}

// getAssociatesStore returns info on the users linked associates store
func (associatesStoreAPI *AssociatesStoreAPI) getAssociatesStore(c context.Context, w http.ResponseWriter, r *http.Request) {
	broadcasterID := pat.Param(c, "tuid")

	actingTuid, err := associatesStoreAPI.CartmanAuthorizer.ExtractActingTuid(r)
	if err != nil {
		log.Errorf("Error determining acting user: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	authChain := []auth.AuthChainFunction{
		associatesStoreAPI.GatingHelper.UserIsIdWrapper(r, broadcasterID),
		associatesStoreAPI.GatingHelper.IsAdminOrSubAdminWrapper(r, actingTuid),
	}
	allow, errs := associatesStoreAPI.GatingHelper.CheckAuthChain(authChain)
	if !allow {
		if len(errs) != 0 {
			errorMessage := fmt.Sprintf("Error authorizing getAssociatesStore: %s", errs)
			log.Error(errorMessage)
			http.Error(w, errorMessage, http.StatusInternalServerError)
			return
		} else {
			log.Warnf("Not authorized to getAssociatesStore for Twitch user %s", broadcasterID)
			http.Error(w, "400 Bad Request", http.StatusForbidden)
			return
		}
	}

	linkedStore, err := associatesStoreAPI.AssociatesClient.GetLinkedStoreForTwitchUser(c, broadcasterID, defaultMarketplace, true, registrationContext)
	if err != nil {
		log.WithFields(log.Fields{
			"broadcasterId": broadcasterID,
			"error":         err,
		}).Error("Error getting associates store for broadcaster")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var storeID string
	var isPayoutEnabled bool
	// Do not surface a store for a broadcaster unless the store has a rev enabled status
	if linkedStore != nil && linkedStore.StoreID != "" && associates.IsStoreStatusRevEnabled(linkedStore.Status) {
		storeID = linkedStore.StoreID
		isPayoutEnabled = associates.IsStoreStatusPayoutEnabled(linkedStore.Status)
	}

	response := janus.GetAssociatesStoreResponse{
		StoreID:         storeID,
		IsPayoutEnabled: isPayoutEnabled,
	}

	writeResponse(response, w)
}
