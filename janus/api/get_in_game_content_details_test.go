package api

import (
	"net/http/httptest"
	"testing"

	"fmt"
	"net/http"

	"encoding/json"
	"io/ioutil"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	promotedAsin    = "CMRCASIN"
	nonPromotedAsin = "foo"
)

func initDefaultGetInGameContentDetailsMocks() {
	discoResponse := []models.ADGProduct{*createMockProduct(false, false, false)}
	adgDiscoClient.On("GetProductsInDomainByASIN", Anything, Anything).Return(discoResponse, nil)

	entResponse := clients.EntitlementStatuses{}
	adgEntitleClient.On("GetEntitledAsins", Anything, Anything, Anything).Return(entResponse, nil)

	metricLogger.On("LogDurationMetric", Anything, Anything).Return()
	appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil)
}

// Test the GetInGameContentDetails API
func TestGetInGameContentDetails(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		var gameID string
		var userID string
		Convey("when /games/<gameid>/ingame?userid=<userid> is requested", func() {
			Convey("with a game that has no igc", func() {
				gameID = "buyablegame"
				discoResponse := []models.ADGProduct{}
				adgDiscoClient.On("GetProductsInDomainByASIN", Anything, Anything).Return(discoResponse, nil)
				// Notably should not call adgEntitleClient.GetEntitledAsins
				metricLogger.On("LogDurationMetric", Anything, Anything).Return()
				appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil)
				gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(false)

				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/games/%s/ingame", server.URL, gameID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")
			})

			Convey("with a published fuel game", func() {
				initDefaultGetInGameContentDetailsMocks()
				gameID = "buyablegame"
				Convey("with a whitelisted user", func() {
					gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(true)
					userID = "12345"
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetInGameContentDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					for _, inGameContent := range parsedResponse.InGameContent {
						fullURL := checkoutURL + "asin=" + inGameContent.ContentDetails.ASIN
						So(inGameContent.ActionDetails.DestinationURL, ShouldEqual, fullURL)
					}
				})

				Convey("Not Whitelisted", func() {
					gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(false)
					Convey("without a userid", func() {
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame", server.URL, gameID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")
					})

					Convey("with an empty userid", func() {
						userID = ""
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")
					})

					Convey("with a non-whitelisted userid", func() {
						userID = "5678"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")
					})
				})
			})

			Convey("with an unpublished future fuel game", func() {
				initDefaultGetInGameContentDetailsMocks()
				gameID = "futuregame"
				Convey("with whitelisted user", func() {
					gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(true)
					userID = "12345"
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetInGameContentDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					for _, inGameContent := range parsedResponse.InGameContent {
						fullURL := checkoutURL + "asin=" + inGameContent.ContentDetails.ASIN
						So(inGameContent.ActionDetails.DestinationURL, ShouldEqual, fullURL)
					}
				})

				Convey("Not Whitelisted", func() {
					gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(false)
					Convey("without a userid", func() {
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame", server.URL, gameID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})

					Convey("with an empty userid", func() {
						userID = ""
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})

					Convey("with a non-whitelisted userid", func() {
						userID = "5678"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})
				})
			})

			Convey("with an non-fuel game", func() {
				initDefaultGetInGameContentDetailsMocks()
				gameID = "nonfuelgame"

				Convey("with whitelisted user", func() {
					gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(true)
					userID = "12345"
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "404 Not Found")
				})

				Convey("Not Whitelisted", func() {
					gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(false)
					Convey("without a userid", func() {
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame", server.URL, gameID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})

					Convey("with an empty userid", func() {
						userID = ""
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})

					Convey("with a non-whitelisted userid", func() {
						userID = "5678"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})
				})
			})

			Convey("When is in multicrate experiment", func() {
				//Multicrate experiment wide configuration
				gameID = "buyablegame"
				userID = "12345"
				entResponse := clients.EntitlementStatuses{}
				adgEntitleClient.On("GetEntitledAsins", Anything, Anything, Anything).Return(entResponse, nil)
				metricLogger.On("LogDurationMetric", Anything, Anything).Return()
				gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(true)
				discoResponse := []models.ADGProduct{*createMockProduct(false, false, false)}
				adgDiscoClient.On("GetProductsInDomainByASIN", Anything, Anything).Return(discoResponse, nil)

				Convey("When appsettings are off", func() {
					discoResponse[0].ASIN = promotedAsin
					appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil).Once()

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetInGameContentDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)
					for _, inGameContent := range parsedResponse.InGameContent {
						So(len(inGameContent.ContentDetails.Crates), ShouldEqual, 1)
					}
				})
				Convey("When appsettings are on, but not for a promoted game", func() {
					discoResponse[0].ASIN = nonPromotedAsin
					appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(true, nil).Once()

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetInGameContentDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					for _, inGameContent := range parsedResponse.InGameContent {
						So(len(inGameContent.ContentDetails.Crates), ShouldEqual, 1)
					}
				})
				Convey("When appsettings are on, and for a promoted game", func() {
					discoResponse[0].ASIN = promotedAsin
					appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(true, nil).Once()

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/ingame?userid=%s", server.URL, gameID, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetInGameContentDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					for _, inGameContent := range parsedResponse.InGameContent {
						So(len(inGameContent.ContentDetails.Crates), ShouldEqual, 2)
					}
				})
			})
		})
	})
}

func TestParseIsFeatured(t *testing.T) {
	Convey("when isFeatured is true", t, func() {
		Convey("with passes and expansions type", func() {
			So(parseIsFeatured(topTierPassesAndExpansions), ShouldBeTrue)
		})
		Convey("with loot and collectibles type", func() {
			So(parseIsFeatured(topTierLootAndCollectibles), ShouldBeTrue)
		})
	})
	Convey("when isFeatured is false", t, func() {
		Convey("with a non-featured type", func() {
			So(parseIsFeatured("this is not a featured type"), ShouldBeFalse)
		})
		Convey("with empty featured type", func() {
			So(parseIsFeatured(""), ShouldBeFalse)
		})
	})
}
