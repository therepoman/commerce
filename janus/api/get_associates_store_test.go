package api

import (
	"errors"
	"net/http/httptest"
	"testing"

	"fmt"
	"net/http"

	"encoding/json"
	"io/ioutil"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

// Test the GetAssociatesStore API
func TestGetAssociatesStore(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		userID := "12345"

		Convey("when /broadcaster/<tuid>/associates/store is requested", func() {
			gatingHelper.On("UserIsIdWrapper", Anything, Anything).Return(MockWrapper(false, errors.New("test error")))
			gatingHelper.On("IsAdminOrSubAdminWrapper", Anything, Anything).Return(MockWrapper(false, errors.New("test error")))

			Convey("where CartmanAuthorizer throws an error", func() {
				cartmanAuthorizer.On("ExtractActingTuid", Anything).Return(nil, errors.New("test error"))

				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("where authz checks error", func() {
				cartmanAuthorizer.On("ExtractActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", Anything).Return(false, []error{errors.New("test error")})

				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("where authz checks return false", func() {
				cartmanAuthorizer.On("ExtractActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", Anything).Return(false, []error{})

				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "403 Forbidden")
			})

			Convey("where custom user_is_id succeeds", func() {
				cartmanAuthorizer.On("ExtractActingTuid", Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", Anything).Return(true, []error{})

				Convey("with no linked store", func() {
					associatesResponse := &models.LinkedAssociatesStore{}
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetAssociatesStoreResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.StoreID, ShouldBeEmpty)
					So(parsedResponse.IsPayoutEnabled, ShouldBeFalse)
				})

				Convey("with linked store not enabled to recieve revenue", func() {
					associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "Linked_Requires_Attention"}
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetAssociatesStoreResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.StoreID, ShouldBeEmpty)
					So(parsedResponse.IsPayoutEnabled, ShouldBeFalse)
				})

				Convey("with linked store with Linked_Missing_Information status", func() {
					associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "Linked_Missing_Information"}
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetAssociatesStoreResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.StoreID, ShouldNotBeEmpty)
					So(parsedResponse.IsPayoutEnabled, ShouldBeFalse)
				})

				Convey("with linked store with Linked status", func() {
					associatesResponse := &models.LinkedAssociatesStore{StoreID: mockTwitchAssociatesID, Status: "Linked"}
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(associatesResponse, nil)

					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetAssociatesStoreResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.StoreID, ShouldNotBeEmpty)
					So(parsedResponse.IsPayoutEnabled, ShouldBeTrue)
				})

				Convey("Associates client throws error", func() {
					associatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything, Anything, Anything, Anything).Return(nil, errors.New("test error"))
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/broadcaster/%s/associates/store", server.URL, userID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "500 Internal Server Error")
				})
			})
		})
	})
}
