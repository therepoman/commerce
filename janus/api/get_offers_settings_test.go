package api

import (
	"errors"
	"net/http/httptest"
	"testing"

	"fmt"
	"net/http"

	"encoding/json"
	"io/ioutil"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

// Test the GetOffersSettings API
func TestGetOffersSettings(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		userID := "12345"
		gameID := "buyablegame"
		broadcasterID := "MockBroadcasterID"
		whitelistedViewerCountryCode := "US"
		nonWhitelistedViewerCountryCode := "SK"

		Convey("when /offers/<gameid>/settings is requested", func() {
			Convey("with missing broadcaster_id", func() {
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, "", whitelistedViewerCountryCode), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersSettingsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.AmazonRetailEnabled, ShouldBeFalse)
			})

			Convey("with missing game_id", func() {
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, "", broadcasterID, whitelistedViewerCountryCode), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersSettingsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.AmazonRetailEnabled, ShouldBeFalse)
			})

			Convey("with no amazon retail items associated", func() {
				daoResponse := []*dynamo.ProductOffers{}
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetOffersSettingsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.AmazonRetailEnabled, ShouldBeFalse)
			})

			Convey("with one live retail item associated", func() {
				daoResponse := []*dynamo.ProductOffers{&dynamo.ProductOffers{Asin: "mockAsin", Status: dynamo.StatusLive}}
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(daoResponse, nil)

				Convey("GatingHelper throws an error getting active tuid", func() {
					gatingHelper.On("GetOptionalActingTuid", Anything).Return("", errors.New("test error"))
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
					broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetOffersSettingsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.AmazonRetailEnabled, ShouldBeTrue)
				})

				Convey("GatingHelper throws an error checking whether acting tuid is staff", func() {
					gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, errors.New("test error"))
					broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetOffersSettingsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(parsedResponse.AmazonRetailEnabled, ShouldBeTrue)
				})

				Convey("User is authenticated", func() {
					gatingHelper.On("GetOptionalActingTuid", Anything).Return(userID, nil)
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)

					Convey("Broadcaster is not eligible", func() {
						broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(false, nil)

						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersSettingsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.AmazonRetailEnabled, ShouldBeFalse)
					})

					Convey("with missing viewer_country_code", func() {
						broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)

						gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
						gatingHelper.On("IsWhitelisted", Anything).Return(false)
						gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s", server.URL, gameID, broadcasterID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersSettingsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.AmazonRetailEnabled, ShouldBeTrue)
					})

					Convey("with empty viewer_country_code", func() {
						broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)

						gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
						gatingHelper.On("IsWhitelisted", Anything).Return(false)
						gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, ""), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersSettingsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.AmazonRetailEnabled, ShouldBeTrue)
					})

					Convey("with non-whitelisted viewer_country_code", func() {
						broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(true, nil)

						gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
						gatingHelper.On("IsWhitelisted", Anything).Return(false)
						gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, nonWhitelistedViewerCountryCode), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetOffersSettingsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(parsedResponse.AmazonRetailEnabled, ShouldBeFalse)
					})
				})

				Convey("BroadcasterHelper throws an error", func() {
					gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
					gatingHelper.On("IsWhitelisted", Anything).Return(false)
					gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
					broadcasterHelper.On("GetRetailEligibility", broadcasterID, gameID).Return(false, errors.New("test error"))
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "500 Internal Server Error")
				})
			})

			Convey("ProductOffers throws an error", func() {
				broadcasterProductOffersDAO.On("GetAllVisibleItems", Anything, Anything, Anything).Return(nil, errors.New("test error"))
				gatingHelper.On("GetOptionalActingTuid", Anything).Return("", nil)
				gatingHelper.On("IsWhitelisted", Anything).Return(false)
				gatingHelper.On("IsAdminOrSubAdmin", Anything, Anything).Return(false, nil)
				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/offers/settings?game_id=%s&broadcaster_id=%s&viewer_country_code=%s", server.URL, gameID, broadcasterID, whitelistedViewerCountryCode), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})
		})
	})
}
