package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"strings"
	"time"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const chatNotificationsURLTemplate = "%s/users/%s/notifications?channelID=%s"

func initChatNotificationsMocks() {
	metricLogger.On("LogDurationSinceMetric", Anything, Anything).Return()
}

func mockDynamoNotificationResponse(channelID string, userID string, token string, asin string, status string, timeReceived time.Time) *dynamo.FuelChatNotification {
	return &dynamo.FuelChatNotification{
		ChannelUser:  channelID + ":" + userID,
		ChannelID:    channelID,
		UserID:       userID,
		Token:        token,
		ASIN:         asin,
		Status:       status,
		TimeReceived: timeReceived,
	}
}

func validateGetChatNotificationsResponseContains(response janus.GetChatNotificationsResponse, notification dynamo.FuelChatNotification) {
	So(len(response.ChatNotifications), ShouldEqual, 1)
	So(response.ChatNotifications[0].ChannelID, ShouldEqual, notification.ChannelID)
	So(response.ChatNotifications[0].UserID, ShouldEqual, notification.UserID)
	So(response.ChatNotifications[0].Token, ShouldEqual, notification.Token)
	So(response.ChatNotifications[0].ASIN, ShouldEqual, notification.ASIN)
	So(response.ChatNotifications[0].Status, ShouldEqual, notification.Status)
	So(response.ChatNotifications[0].TimeReceived, ShouldHappenWithin, time.Nanosecond, notification.TimeReceived)
}

func consumeNotificationRequest(token string, consumptionType string, message string) io.Reader {
	request := janus.ConsumeChatNotificationsRequest{
		Token:   token,
		Type:    consumptionType,
		Message: message,
	}
	jsonBytes, err := json.Marshal(request)
	So(err, ShouldBeNil)
	return bytes.NewReader(jsonBytes)
}

// Test the GetChatNotifications API
func TestGetChatNotifications(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initChatNotificationsMocks()

		userID := "123"
		channelID := "456"
		Convey("when GET /users/<userID>/notifications?channelID=<channelID> requested", func() {
			Convey("with an empty userID", func() {
				userID = " "
				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})

			Convey("with an empty channelID", func() {
				channelID = " "
				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})

			Convey("where cartman auth fails", func() {
				Convey("where cartman token is invalid", func() {
					cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(errors.New("test error"))
					gatingHelper.On("UserIsId", Anything, Anything).Return(false, errors.New("test error"))
					req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 403)
				})
				Convey("where cartman token does not match queried user", func() {
					cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(errors.New("test error"))
					gatingHelper.On("UserIsId", Anything, Anything).Return(false, nil)
					req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 403)
				})
			})

			Convey("where cartman authZ succeeds", func() {
				cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(nil)
				testGetChatNotificationsPostAuth(userID, channelID, server)
			})

			Convey("where cartman authZ fails, but custom user_is_id succeeds", func() {
				cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(errors.New("test error"))
				gatingHelper.On("UserIsId", Anything, Anything).Return(true, nil)
				testGetChatNotificationsPostAuth(userID, channelID, server)
			})
		})
	})
}

func testGetChatNotificationsPostAuth(userID string, channelID string, server *httptest.Server) {
	Convey("where dynamo fails to load records", func() {
		fuelChatNotificationDAO.On("GetMostRecent", channelID, userID).Return(nil, errors.New("test error"))
		req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 500)
	})

	Convey("where dynamo loads 0 records", func() {
		fuelChatNotificationDAO.On("GetMostRecent", channelID, userID).Return([]*dynamo.FuelChatNotification{}, nil)
		req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 200)

		body, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldBeNil)

		parsedResponse := new(janus.GetChatNotificationsResponse)
		err = json.Unmarshal(body, parsedResponse)
		So(err, ShouldBeNil)

		So(parsedResponse.ChatNotifications, ShouldBeEmpty)
	})

	token := "1"
	asin := "TEST_ASIN"
	status := "AVAILABLE"
	timeReceived := time.Now()
	Convey("where dynamo loads record", func() {
		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, status, timeReceived)
		fuelChatNotificationDAO.On("GetMostRecent", channelID, userID).Return([]*dynamo.FuelChatNotification{dynamoNotification}, nil)

		Convey("where content DAO returns failure", func() {
			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(nil, fmt.Errorf("test error"))

			req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, 500)
		})

		Convey("where content DAO returns empty", func() {
			contents := []*dynamo.FuelChatNotificationContent{}
			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, nil)
			req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)

			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, 200)

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)

			parsedResponse := new(janus.GetChatNotificationsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)
			validateGetChatNotificationsResponseContains(*parsedResponse, *dynamoNotification)

			So(len(parsedResponse.ChatNotifications[0].Contents.EmoteSets), ShouldEqual, 0)
			So(len(parsedResponse.ChatNotifications[0].Contents.Badges), ShouldEqual, 0)
			So(len(parsedResponse.ChatNotifications[0].Contents.Bits), ShouldEqual, 0)
		})

		Convey("where content DAO returns an emote", func() {
			contents := []*dynamo.FuelChatNotificationContent{
				{
					ContentKey:  "key",
					ContentID:   "123",
					Quantity:    "1",
					ContentType: "emote",
				},
			}

			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, nil)

			Convey("where emote hydrate call fails", func() {
				makoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(nil, errors.New("test error"))

				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 500)
			})

			Convey("where emote hydrate succeeds", func() {
				emoteSetId := "123"
				emoticonDetails := models.ChatEmoticonDetails{
					Details: map[string][]models.Emoticon{
						emoteSetId: {
							{
								ID:      "id",
								Pattern: "Kappa",
							},
						},
					},
				}
				makoClient.On("GetEmoticonsByGroups", Anything, Anything).Return(&emoticonDetails, nil)

				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetChatNotificationsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)
				validateGetChatNotificationsResponseContains(*parsedResponse, *dynamoNotification)
				So(len(parsedResponse.ChatNotifications[0].Contents.EmoteSets), ShouldEqual, 1)
				So(len(parsedResponse.ChatNotifications[0].Contents.Badges), ShouldEqual, 0)
				So(len(parsedResponse.ChatNotifications[0].Contents.Bits), ShouldEqual, 0)
				So(parsedResponse.ChatNotifications[0].Contents.CrateCount, ShouldEqual, 1)

				emoteSet, exists := parsedResponse.ChatNotifications[0].Contents.EmoteSets[emoteSetId]
				So(exists, ShouldBeTrue)
				So(len(emoteSet), ShouldEqual, 1)
			})
		})

		Convey("where content DAO returns a badge", func() {
			contents := []*dynamo.FuelChatNotificationContent{
				{
					ContentKey:  "key",
					ContentID:   "staff",
					Quantity:    "1",
					ContentType: "badge",
				},
			}

			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, nil)

			Convey("where badge hydrate call fails", func() {
				badgesClient.On("GetBadgeDisplay", Anything).Return(nil, errors.New("test error"))

				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 500)
			})

			Convey("where badge hydrate call succeeds", func() {
				version := "1"
				imageUrl := "foo"
				badgeDisplay := models.GetBadgeDisplay{
					BadgeSets: map[string]models.BadgeSet{
						"staff": {
							Versions: map[string]models.BadgeVersion{
								version: {
									Title:      "Staff",
									ImageURL1x: &imageUrl,
									ImageURL2x: &imageUrl,
									ImageURL4x: &imageUrl,
								},
							},
						},
					},
				}
				badgesClient.On("GetBadgeDisplay", Anything).Return(&badgeDisplay, nil)

				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetChatNotificationsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)
				validateGetChatNotificationsResponseContains(*parsedResponse, *dynamoNotification)
				So(len(parsedResponse.ChatNotifications[0].Contents.EmoteSets), ShouldEqual, 0)
				So(len(parsedResponse.ChatNotifications[0].Contents.Badges), ShouldEqual, 1)
				So(len(parsedResponse.ChatNotifications[0].Contents.Bits), ShouldEqual, 0)
				So(parsedResponse.ChatNotifications[0].Contents.CrateCount, ShouldEqual, 1)

				So(parsedResponse.ChatNotifications[0].Contents.Badges[0].ImageURL1X, ShouldEqual, imageUrl)
				So(parsedResponse.ChatNotifications[0].Contents.Badges[0].ImageURL2X, ShouldEqual, imageUrl)
				So(parsedResponse.ChatNotifications[0].Contents.Badges[0].ImageURL4X, ShouldEqual, imageUrl)
				So(parsedResponse.ChatNotifications[0].Contents.Badges[0].Version, ShouldEqual, version)
			})
		})

		Convey("where content DAO returns bits", func() {
			bitsQuantity := "17"
			contents := []*dynamo.FuelChatNotificationContent{
				{
					ContentKey:  "key",
					ContentID:   "",
					Quantity:    bitsQuantity,
					ContentType: "bits",
				},
			}

			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, 200)

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)

			parsedResponse := new(janus.GetChatNotificationsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)
			validateGetChatNotificationsResponseContains(*parsedResponse, *dynamoNotification)
			So(len(parsedResponse.ChatNotifications[0].Contents.EmoteSets), ShouldEqual, 0)
			So(len(parsedResponse.ChatNotifications[0].Contents.Badges), ShouldEqual, 0)
			So(len(parsedResponse.ChatNotifications[0].Contents.Bits), ShouldEqual, 1)
			So(parsedResponse.ChatNotifications[0].Contents.CrateCount, ShouldEqual, 1)

			So(parsedResponse.ChatNotifications[0].Contents.Bits[0].Quantity, ShouldEqual, bitsQuantity)
		})

		Convey("where content DAO returns IGC", func() {
			contents := []*dynamo.FuelChatNotificationContent{
				{
					ContentKey:  "key",
					ContentID:   "asin",
					UserID:      userID,
					ContentType: "igc",
				},
			}

			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, nil)

			Convey("where igc hydrate call fails", func() {
				adgDiscoClient.On("GetGameProductDetails", Anything, userID).Return(nil, errors.New("test error"))

				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 500)
			})

			Convey("where igc hydrate call succeeds", func() {
				adgProduct := &models.ADGProduct{
					Title:   "test IGC",
					IconURL: "bar.jpg",
					Details: models.Details{
						ShortDescription: "this is igc",
					},
				}

				adgDiscoClient.On("GetGameProductDetails", Anything, userID).Return(adgProduct, nil)

				req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetChatNotificationsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				validateGetChatNotificationsResponseContains(*parsedResponse, *dynamoNotification)
				So(len(parsedResponse.ChatNotifications[0].Contents.EmoteSets), ShouldEqual, 0)
				So(len(parsedResponse.ChatNotifications[0].Contents.Badges), ShouldEqual, 0)
				So(len(parsedResponse.ChatNotifications[0].Contents.Bits), ShouldEqual, 0)
				So(len(parsedResponse.ChatNotifications[0].Contents.InGameContent), ShouldEqual, 1)
				So(parsedResponse.ChatNotifications[0].Contents.CrateCount, ShouldEqual, 1)

				So(parsedResponse.ChatNotifications[0].Contents.InGameContent[0].Title, ShouldEqual, adgProduct.Title)
				So(parsedResponse.ChatNotifications[0].Contents.InGameContent[0].BoxArtURL, ShouldEqual, adgProduct.IconURL)
				So(parsedResponse.ChatNotifications[0].Contents.InGameContent[0].Description, ShouldEqual, adgProduct.Details.ShortDescription)
			})
		})
	})

	Convey("where dynamo loads old expired record", func() {
		timeReceived = time.Now().Add(-time.Hour * 49)

		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, status, timeReceived)
		fuelChatNotificationDAO.On("GetMostRecent", channelID, userID).Return([]*dynamo.FuelChatNotification{dynamoNotification}, nil)

		req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 200)

		body, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldBeNil)

		parsedResponse := new(janus.GetChatNotificationsResponse)
		err = json.Unmarshal(body, parsedResponse)
		So(err, ShouldBeNil)

		So(parsedResponse.ChatNotifications, ShouldBeEmpty)
	})

	Convey("where dynamo loads a dismissed record", func() {
		timeReceived = time.Now()

		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, "DISMISSED", timeReceived)
		fuelChatNotificationDAO.On("GetMostRecent", channelID, userID).Return([]*dynamo.FuelChatNotification{dynamoNotification}, nil)

		req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 200)

		body, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldBeNil)

		parsedResponse := new(janus.GetChatNotificationsResponse)
		err = json.Unmarshal(body, parsedResponse)
		So(err, ShouldBeNil)

		So(parsedResponse.ChatNotifications, ShouldBeEmpty)
	})

	Convey("where dynamo loads a shared record", func() {
		timeReceived = time.Now()

		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, "SHARED", timeReceived)
		fuelChatNotificationDAO.On("GetMostRecent", channelID, userID).Return([]*dynamo.FuelChatNotification{dynamoNotification}, nil)

		req, err := http.NewRequest("GET", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 200)

		body, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldBeNil)

		parsedResponse := new(janus.GetChatNotificationsResponse)
		err = json.Unmarshal(body, parsedResponse)
		So(err, ShouldBeNil)

		So(parsedResponse.ChatNotifications, ShouldBeEmpty)
	})
}

// Test the ConsumeChatNotification API
func TestConsumeChatNotification(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initChatNotificationsMocks()

		userID := "123"
		channelID := "456"
		Convey("when POST /users/<userID>/notifications?channelID=<channelID> requested", func() {
			Convey("with an empty userID", func() {
				userID = " "
				req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})

			Convey("with an empty channelID", func() {
				channelID = " "
				req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})

			Convey("where cartman auth fails", func() {
				cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(errors.New("test error"))

				Convey("where cartman token is invalid", func() {
					gatingHelper.On("UserIsId", Anything, Anything).Return(false, errors.New("test error"))
					req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 403)
				})
				Convey("where cartman token does not match queried user", func() {
					gatingHelper.On("UserIsId", Anything, Anything).Return(false, nil)
					req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 403)
				})
			})

			Convey("where cartman authZ succeeds", func() {
				cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(nil)
				testConsumeChatNotificationsPostAuth(userID, channelID, server)
			})

			Convey("where cartman authZ fails, but custom user_is_id succeeds", func() {
				cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(errors.New("test error"))
				gatingHelper.On("UserIsId", Anything, Anything).Return(true, nil)
				testConsumeChatNotificationsPostAuth(userID, channelID, server)
			})
		})
	})
}

func testConsumeChatNotificationsPostAuth(userID string, channelID string, server *httptest.Server) {
	Convey("with invalid JSON request", func() {
		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), strings.NewReader("not valid JSON ¯\\_(ツ)_/¯"))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	token := "1"
	message := "test"
	consumptionType := "share"
	Convey("with empty token", func() {
		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest("", consumptionType, message))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	Convey("with invalid type", func() {
		consumptionType = "invalid_type"
		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, ""))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	Convey("with empty message for share type", func() {
		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, ""))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	Convey("where dynamo fails to load records", func() {
		fuelChatNotificationDAO.On("Get", channelID, userID, token).Return(nil, errors.New("test error"))
		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 500)
	})

	Convey("where requested record does not exist in dynamo", func() {
		fuelChatNotificationDAO.On("Get", channelID, userID, token).Return(nil, nil)
		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	asin := "TEST_ASIN"
	status := "AVAILABLE"
	timeReceived := time.Now()
	Convey("where requested record used", func() {
		status = "USED"
		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, status, timeReceived)
		fuelChatNotificationDAO.On("Get", channelID, userID, token).Return(dynamoNotification, nil)

		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	Convey("where requested record is expired", func() {
		timeReceived = time.Now().Add(-time.Hour * 49)
		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, status, timeReceived)
		fuelChatNotificationDAO.On("Get", channelID, userID, token).Return(dynamoNotification, nil)

		req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
		So(err, ShouldBeNil)

		resp, err := http.DefaultClient.Do(req)
		So(err, ShouldBeNil)
		So(resp.StatusCode, ShouldEqual, 400)
	})

	Convey("where requested record is returned from dynamo", func() {
		dynamoNotification := mockDynamoNotificationResponse(channelID, userID, token, asin, status, timeReceived)
		fuelChatNotificationDAO.On("Get", channelID, userID, token).Return(dynamoNotification, nil)

		contents := []*dynamo.FuelChatNotificationContent{}
		Convey("where content DAO fails", func() {
			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, fmt.Errorf("test error"))
			req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, 500)
		})

		Convey("where content DAO succeeds", func() {
			fuelChatNotificationContentsDAO.On("GetAll", Anything).Return(contents, nil)

			Convey("where chat notifier fails", func() {
				pubsubNotifier.On("Notify", Anything, Anything).Return(nil)
				chatNotifier.On("Notify", Anything, Anything).Return(errors.New("test error"))

				req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 500)
			})

			Convey("where pubsub and slack notifiers fail, but chat succeeds", func() {
				chatNotifier.On("Notify", Anything, Anything).Return(nil)
				pubsubNotifier.On("Notify", Anything, Anything).Return(errors.New("test error"))
				slackNotifier.On("Notify", Anything, Anything).Return(errors.New("test error"))

				fuelChatNotificationDAO.On("Put", Anything).Return(nil)

				req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)
			})

			Convey("where all notifiers succeed", func() {
				pubsubNotifier.On("Notify", Anything, Anything).Return(nil)
				chatNotifier.On("Notify", Anything, Anything).Return(nil)
				slackNotifier.On("Notify", Anything, Anything).Return(nil)

				Convey("where saving back to dynamo fails", func() {
					fuelChatNotificationDAO.On("Put", Anything).Return(errors.New("Test error"))

					req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 500)
				})

				Convey("where saving back to dynamo succeeds", func() {
					fuelChatNotificationDAO.On("Put", Anything).Return(nil)

					req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 200)
				})

				Convey("where consumption type is dismiss", func() {
					consumptionType = "dismiss"
					fuelChatNotificationDAO.On("Put", Anything).Return(nil)

					req, err := http.NewRequest("POST", fmt.Sprintf(chatNotificationsURLTemplate, server.URL, userID, channelID), consumeNotificationRequest(token, consumptionType, message))
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, 200)
				})
			})
		})
	})
}
