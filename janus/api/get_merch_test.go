package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/models"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	firstMockAsin          = "FirstMockAsin"
	secondMockAsin         = "SecondMockAsin"
	salePrice              = "5.99"
	fallbackPrice  float64 = 9.99
	originalPrice  float64 = 9.99
)

// Test the GetGameDetails API
func TestGetMerchandiseDetails(t *testing.T) {

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		Convey("when /merchandise is requested", func() {

			req, err := http.NewRequest("GET",
				fmt.Sprintf("%s/merchandise", server.URL), nil)
			So(err, ShouldBeNil)

			Convey("the Dynamo responses has 2 unordered results and 1 image", func() {
				merchandiseMetadata := []*dynamo.MerchandiseMetadata{}
				merchandiseMetadata = append(merchandiseMetadata, &dynamo.MerchandiseMetadata{Asin: secondMockAsin, Rank: 2, IsNew: true, FallbackPrice: fallbackPrice, OriginalPrice: originalPrice})
				merchandiseMetadata = append(merchandiseMetadata, &dynamo.MerchandiseMetadata{Asin: firstMockAsin, Rank: 1, IsNew: true, FallbackPrice: fallbackPrice, OriginalPrice: originalPrice})
				merchandiseMetadataDAO.On("Scan").Return(merchandiseMetadata, nil)

				merchandiseLifestyleImages := []*dynamo.MerchandiseLifestyleImages{&dynamo.MerchandiseLifestyleImages{URL: "mockURL", Width: 100, Height: 100, Rank: 3}}
				merchandiseLifestyleImagesDAO.On("Scan").Return(merchandiseLifestyleImages, nil)

				Convey("the ADG response has 2 results", func() {
					item1 := createMockProduct(false, false, false)
					item1.ASIN = firstMockAsin
					item2 := createMockProduct(false, false, false)
					item2.ASIN = secondMockAsin
					discoResponse := []models.ADGProduct{*item1, *item2}
					adgDiscoClient.On("GetMerchandiseDetails", Anything, Anything).Return(discoResponse, nil)

					Convey("the localization table has results for both asins", func() {
						merchandiseLocalization := []*dynamo.MerchandiseLocalization{
							&dynamo.MerchandiseLocalization{
								Asin:   firstMockAsin,
								Locale: "en-us",
								Title:  "foo bar",
							},
							&dynamo.MerchandiseLocalization{
								Asin:   secondMockAsin,
								Locale: "en-us",
								Title:  "foo bar",
							},
						}
						merchandiseLocalizationDAO.On("BatchGet", Anything, Anything).Return(merchandiseLocalization, nil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetMerchandiseResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(len(parsedResponse.Products), ShouldEqual, 3)

						product := parsedResponse.Products[0]

						So(product.Title, ShouldNotBeEmpty)
						So(product.OriginalPrice.Price, ShouldBeEmpty)
						So(product.Price.Price, ShouldNotBeEmpty)
						So(product.DetailPageURL, ShouldNotBeEmpty)
						So(product.IsNew, ShouldBeTrue)
						So(product.ASIN, ShouldEqual, firstMockAsin)

						image := parsedResponse.Products[2]
						imageMetadata := image.Media.Images[0]

						So(image.ASIN, ShouldBeEmpty)
						So(imageMetadata.ImageURL, ShouldNotBeEmpty)
						So(imageMetadata.Height, ShouldNotBeNil)
						So(imageMetadata.Width, ShouldNotBeNil)
					})

					Convey("the localization table has no results", func() {
						merchandiseLocalization := []*dynamo.MerchandiseLocalization{}
						merchandiseLocalizationDAO.On("BatchGet", Anything, Anything).Return(merchandiseLocalization, nil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetMerchandiseResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(len(parsedResponse.Products), ShouldEqual, 3)
						product := parsedResponse.Products[0]

						So(product.Title, ShouldBeEmpty)
						So(product.OriginalPrice.Price, ShouldBeEmpty)
						So(product.Price.Price, ShouldNotBeEmpty)
						So(product.DetailPageURL, ShouldNotBeEmpty)
						So(product.IsNew, ShouldBeTrue)
						So(product.ASIN, ShouldEqual, firstMockAsin)
					})

					Convey("MerchandiseLocalizationDAO throws an error", func() {
						merchandiseLocalizationDAO.On("BatchGet", Anything, Anything).Return(nil, errors.New("test error"))

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "500 Internal Server Error")
					})
				})

				Convey("the ADG response has 1 good result and one empty result", func() {
					item1 := createMockProduct(false, false, false)
					item1.ASIN = firstMockAsin
					discoResponse := []models.ADGProduct{*item1, models.ADGProduct{}}
					adgDiscoClient.On("GetMerchandiseDetails", Anything, Anything).Return(discoResponse, nil)

					Convey("the localization table has results for both asins", func() {
						merchandiseLocalization := []*dynamo.MerchandiseLocalization{
							&dynamo.MerchandiseLocalization{
								Asin:   firstMockAsin,
								Locale: "en-us",
								Title:  "foo bar",
							},
							&dynamo.MerchandiseLocalization{
								Asin:   secondMockAsin,
								Locale: "en-us",
								Title:  "foo bar",
							},
						}
						merchandiseLocalizationDAO.On("BatchGet", Anything, Anything).Return(merchandiseLocalization, nil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetMerchandiseResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						So(len(parsedResponse.Products), ShouldEqual, 3)

						product := parsedResponse.Products[1]

						So(product.Title, ShouldNotBeEmpty)
						So(product.Price.Price, ShouldNotBeEmpty)
						So(product.OriginalPrice.Price, ShouldBeEmpty)
						So(product.DetailPageURL, ShouldNotBeEmpty)
						So(product.IsNew, ShouldBeTrue)
						So(product.ASIN, ShouldEqual, secondMockAsin)

						image := parsedResponse.Products[2]
						imageMetadata := image.Media.Images[0]

						So(image.ASIN, ShouldBeEmpty)
						So(imageMetadata.ImageURL, ShouldNotBeEmpty)
						So(imageMetadata.Height, ShouldNotBeNil)
						So(imageMetadata.Width, ShouldNotBeNil)
					})
				})

				Convey("ADGDiscoClient throws an error", func() {
					adgDiscoClient.On("GetMerchandiseDetails", Anything, Anything).Return(nil, errors.New("test error"))

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "500 Internal Server Error")
				})
			})

			Convey("MerchandiseMetadataDAO throws an error", func() {
				merchandiseMetadataDAO.On("Scan").Return(nil, errors.New("test error"))

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("MerchandiseLifestyleDAO throws an error", func() {
				merchandiseMetadataDAO.On("Scan").Return([]*dynamo.MerchandiseMetadata{}, nil)
				merchandiseLocalizationDAO.On("BatchGet", Anything, Anything).Return([]*dynamo.MerchandiseLocalization{}, nil)
				adgDiscoClient.On("GetMerchandiseDetails", Anything, Anything).Return([]models.ADGProduct{}, nil)
				merchandiseLifestyleImagesDAO.On("Scan").Return(nil, errors.New("test error"))

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})
		})
	})
}
