package api

import (
	"net/http"
	"net/url"
	"strconv"

	"code.justin.tv/commerce/janus/associates"
	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/broadcaster"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/models"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

const (
	maxItemDisplayNum    = 20
	betaCrateAsin        = "BT6RWWCKJRS"
	prodCrateAsin        = "B01MYF798N"
	crateThresholdAmount = 4.99
	asinParam            = "asin"
	broadcasterIDParam   = "br_id"
	tagParam             = "tag"
	registrationContext  = "registration"
	defaultMarketplace   = "ATVPDKIKX0DER"
)

// GetOffersAPI represents the API to get all offers info
type GetOffersAPI struct {
	ADGDiscoClient                  clients.IADGDiscoClientWrapper
	AssociatesClient                clients.IAssociatesClientWrapper
	DetailPageAssociatesRedirectURL string
	BroadcasterProductOffersDAO     dynamo.IProductOffersDAO
	BroadcasterHelper               broadcaster.IBroadcasterHelper
	GatingHelper                    auth.IGatingHelper
	IsDevelopment                   bool
	TwitchAssociatesStoreID         string
}

// GetOffers returns info on offers associated with a gameID
func (getOffersAPI *GetOffersAPI) getOffers(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameID := r.Form.Get("game_id")
	broadcasterID := r.Form.Get("broadcaster_id")
	viewerCountryCode := r.Form.Get("viewer_country_code")

	// Authenticate and authorize the acting tuid for this request, if present
	userID, err := getOffersAPI.GatingHelper.GetOptionalActingTuid(r)
	if err != nil {
		log.Warnf("Failure getting active tuid for request: %s", err)
		userID = ""
	}
	isWhitelisted := getOffersAPI.GatingHelper.IsWhitelisted(userID)
	isStaff, err := getOffersAPI.GatingHelper.IsAdminOrSubAdmin(r, userID)
	if err != nil {
		log.Warnf("Failure performing Admin check for %s: %s", userID, err)
		isStaff = false
	}

	retailProducts, err := getOffersAPI.getRetailProducts(c, gameID, broadcasterID, viewerCountryCode, isWhitelisted, isStaff)
	if err != nil {
		log.Errorf("Error getting retail products: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := janus.GetOffersResponse{
		RetailProducts: retailProducts,
	}

	writeResponse(response, w)
}

// getRetailProducts returns retail products associated with a gameID
func (getOffersAPI *GetOffersAPI) getRetailProducts(ctx context.Context, gameID string, broadcasterID string, viewerCountryCode string, isWhitelisted bool, isStaff bool) ([]janus.RetailProduct, error) {
	if gameID == "" || broadcasterID == "" || !IsRetailEnabledForCountry(viewerCountryCode) {
		return []janus.RetailProduct{}, nil
	}

	retailEnabled, err := getOffersAPI.BroadcasterHelper.GetRetailEligibility(broadcasterID, gameID)
	if err != nil {
		log.Errorf("Error getting broadcaster retail eligibility: %s", err)
		return nil, err
	}
	if !retailEnabled {
		return []janus.RetailProduct{}, nil
	}

	offers, err := getOffersAPI.BroadcasterProductOffersDAO.GetAllVisibleItems(broadcasterID, isWhitelisted, isStaff)
	if err != nil {
		log.Errorf("Error receiving product offers from dynamo: %s", err)
		return nil, err
	}
	asinList := []string{}
	for _, productOffer := range offers {
		asinList = append(asinList, productOffer.Asin)
	}

	// Call associates to get this broadcaster's linked store if available
	linkedStore, err := getOffersAPI.AssociatesClient.GetLinkedStoreForTwitchUser(ctx, broadcasterID, defaultMarketplace, true, registrationContext)
	if err != nil {
		log.WithFields(log.Fields{
			"broadcasterId": broadcasterID,
			"error":         err,
		}).Error("Error getting associates store for broadcaster")
		return nil, err
	}
	// Default to the twitch associates store
	storeID := getOffersAPI.TwitchAssociatesStoreID
	if linkedStore != nil && linkedStore.StoreID != "" && associates.IsStoreStatusRevEnabled(linkedStore.Status) {
		storeID = linkedStore.StoreID
	}

	// Get retail product data from ADG
	retailProducts, err := getOffersAPI.fetchProducts(asinList, broadcasterID, storeID)
	if err != nil {
		log.Errorf("Error receiving twitch retail ADG info: %s", err)
		return nil, err
	}

	return retailProducts, nil
}

// fetchProducts calls ADG with the list of passed in asins and returns a list of RetailProducts
// Since items on amazon that go out of stock don't have associated offer data, we check whether ADG
// gives us back offer data in order to decide whether to show a particular asin. This will show up to
// three different retail products, though more than three might be stored in dynamo as a backup.
func (getOffersAPI *GetOffersAPI) fetchProducts(asinList []string, broadcasterID string, storeID string) ([]janus.RetailProduct, error) {
	products := []janus.RetailProduct{}
	if len(asinList) == 0 {
		return products, nil
	}
	adgProducts, err := getOffersAPI.ADGDiscoClient.GetAmazonRetailDetails(asinList)
	if err != nil {
		return nil, err
	}

	for _, adgProduct := range adgProducts {
		// Do not include products without offers (e.g. out of stock)
		if adgProduct.Price.Price == "" {
			continue
		}
		product := hydrateRetailProduct(adgProduct)

		product.DetailPageURL = getOffersAPI.constructDetailPageAssociatesRedirect(broadcasterID, product.ASIN, storeID)

		// TODO: Remove this hardcoding once the Docket has KratOS onboarded and can pipe this value through
		// Hardcode a crate asin if this item passes the threshold
		priceAmount, err := strconv.ParseFloat(adgProduct.Price.Price, 64)
		if err != nil {
			log.Errorf("Error parsing price %s for asin %s. Excluding product in response: %s", adgProduct.Price.Price, adgProduct.ASIN, err)
			continue
		}
		product.Crates = getOffersAPI.buildCratesValue(priceAmount)

		products = append(products, product)

		// The current limit is arbitrarily large and we should never hit it.  We use the DDB to determine how many actual products we'll be showing for Gillette.
		if len(products) >= maxItemDisplayNum {
			log.Errorf("GetOffersAPI reached the max limit of offers, which should never be hit. Displaying max %d offers.", maxItemDisplayNum)
			break
		}
	}

	return products, nil
}

// hydrateRetailProduct creates a RetailProduct from the given ADGProduct
func hydrateRetailProduct(adgProduct models.ADGProduct) janus.RetailProduct {
	product := new(janus.RetailProduct)
	product.ASIN = adgProduct.ASIN
	product.Brand = adgProduct.Details.Attributes.Brand
	product.Description = adgProduct.Description
	product.DetailBullets = adgProduct.Details.FeaturesDetails
	product.IsPrimeEligible = adgProduct.Details.Prime.IsPrimeEligible
	product.Media = janus.Media{
		BoxArtURL: adgProduct.IconURL,
	}
	product.Price = janus.Price{
		Price:        adgProduct.Price.Price,
		CurrencyUnit: adgProduct.Price.CurrencyUnit,
	}

	// Use the short title instead of regular title if it exists in the catalog data
	if len(adgProduct.Details.Attributes.ShortTitle) > 0 {
		product.Title = adgProduct.Details.Attributes.ShortTitle
	} else {
		product.Title = adgProduct.Title
	}

	return *product
}

// TODO Remove this hardcoding once the crates field is properly piped through the ADG docket: https://issues.amazon.com/issues/ADG-5643
// buildCratesValue builds a Crates structure containing a crate asin if the given priceAmount is above the crate-granting threshold,
// and empty otherwise
func (getOffersAPI *GetOffersAPI) buildCratesValue(priceAmount float64) []janus.Crate {
	crateAsin := prodCrateAsin
	if getOffersAPI.IsDevelopment {
		crateAsin = betaCrateAsin
	}

	if priceAmount >= crateThresholdAmount {
		return []janus.Crate{
			janus.Crate{ASIN: crateAsin},
		}
	}
	return []janus.Crate{}
}

// constructDetailPageAssociatesRedirect constructs the RSW URL to be used on the channel page which redirects to the product's
// Amazon detail page
func (getOffersAPI *GetOffersAPI) constructDetailPageAssociatesRedirect(broadcasterID string, asin string, storeID string) string {
	query := url.Values{}
	query.Set(broadcasterIDParam, broadcasterID)
	query.Set(asinParam, asin)
	query.Set(tagParam, storeID)
	return getOffersAPI.DetailPageAssociatesRedirectURL + query.Encode()
}
