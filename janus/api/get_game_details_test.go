package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"

	"code.justin.tv/commerce/janus/models"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func createMockProduct(hasNoneFields bool, hasMissingFulfillment bool, hasMalformedVods bool) (product *models.ADGProduct) {
	product = &models.ADGProduct{}
	// ReleaseDate is ignored in favour metadata dates, however still needed here to make tests pass
	product.Details.Crate = models.Crate{
		ASIN: "foo",
	}
	product.Details.Attributes.ReleaseDate = "2017-10-28T16:19:52.13Z"
	product.Details.Attributes.Edition = "Mock Description"
	product.Title = "Mock Title"
	product.Description = "Mock Description"
	product.Price = models.Price{
		Price:        "9.99",
		CurrencyUnit: "$",
	}
	product.SKU = "SKU"
	product.VendorId = "VendorId"
	product.Details.Attributes.Brand = "Mock Brand"
	product.Details.FeaturesDetails = []string{"mock detail 1", "mock detail 2", "mock detail 3"}

	if hasMissingFulfillment {
		product.Details.ItemBadgeDetails = []string{"foo", "bar", ""}
	} else {
		product.Details.ItemBadgeDetails = []string{"foo", "bar", "fulfilled_tpuf_ubisoft"}
	}

	product.Details.Attributes.GameURL = "https://mock.download.url/"

	if hasNoneFields {
		product.Details.Developer.EULAURL = "None"
		product.Details.TechnicalDetails.Minimum.Other = "nOne"
		product.Details.TechnicalDetails.Recommended.Other = "noNe"
		product.Details.Videos = map[string]string{
			"0": "None",
		}

	} else {
		product.Details.Developer.EULAURL = "Mock EULA"
		product.Details.TechnicalDetails.Minimum.Other = "Mock Other"
		product.Details.TechnicalDetails.Recommended.Other = "Mock Other"
		if hasMalformedVods {
			product.Details.Videos = map[string]string{
				"0": "https://youtube.com/1",
				"1": "https://youtube.com/2",
				"2": "https://youtube.com/3",
			}
		} else {
			product.Details.Videos = map[string]string{
				"0": "https://www.twitch.tv/videos/1",
				"1": "https://www.twitch.tv/videos/2",
				"2": "https://www.twitch.tv/videos/3",
				"3": "https://www.twitch.tv/videos/4",
				"4": "https://www.twitch.tv/videos/5",
			}
		}
	}

	return
}

func initGetGameDetailsMocks() {
	adgDiscoClient.On("GetGameProductDetails", "asin1-hasnonefields", Anything).Return(createMockProduct(true, false, false), nil)
	adgDiscoClient.On("GetGameProductDetails", "asin1-hasmissingfulfillment", Anything).Return(createMockProduct(false, true, false), nil)
	adgDiscoClient.On("GetGameProductDetails", "asin1-hasmalformedvods", Anything).Return(createMockProduct(false, false, true), nil)
	adgDiscoClient.On("GetGameProductDetails", Anything, Anything).Return(createMockProduct(false, false, false), nil)
	adgEntitleClient.On("IsEntitled", Anything, Anything).Return(true, nil)
	metricLogger.On("LogDurationMetric", Anything, Anything).Return()

	gatingHelper.On("CanSeeHiddenInformation", Anything, "12345", Anything).Return(true)
	gatingHelper.On("CanSeeHiddenInformation", Anything, Anything, Anything).Return(false)
}

// Test the GetGameDetails API
func TestGetGameDetails(t *testing.T) {

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initGetGameDetailsMocks()

		var gameID string
		var userID string
		Convey("WithGameDetailMocks", func() {
			appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil)
			Convey("when /games/<gameid>/details?userid=<userid> is requested", func() {
				Convey("with a published, buy-able game", func() {
					gameID = "buyablegame"

					Convey("with a whitelisted user", func() {
						userID = "12345"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)

						fullURL := checkoutURL + "asin=" + parsedResponse.Product.ASIN
						So(parsedResponse.ActionDetails.DestinationURL, ShouldEqual, fullURL)
						checkParsedResponse(parsedResponse, gameID, userID, true, false, false, false)
					})

					Convey("without a userid", func() {
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details", server.URL, gameID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)
						checkParsedResponse(parsedResponse, gameID, "", true, false, false, false)
					})

					Convey("with an empty userid", func() {
						userID = ""
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)
						checkParsedResponse(parsedResponse, gameID, userID, true, false, false, false)
					})

					Convey("with a non-whitelisted userid", func() {
						userID = "5678"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)
						checkParsedResponse(parsedResponse, gameID, userID, true, false, false, false)
					})
				})

				Convey("with a published, buy-able game but 'None' for EULA and 'Other' requirements", func() {
					gameID = "buyablegame-hasnonefields"
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/details", server.URL, gameID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetGameDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)
					checkParsedResponse(parsedResponse, gameID, "", true, true, false, false)
				})

				Convey("with a published, buy-able game that has a fulfillment type that's missing in our metadata", func() {
					gameID = "buyablegame-hasmissingfulfillment"
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/details", server.URL, gameID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetGameDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)
					checkParsedResponse(parsedResponse, gameID, "", true, false, true, false)
				})

				Convey("with a published, buy-able game that has malformed video urls", func() {
					gameID = "buyablegame-hasmalformedvods"
					req, err := http.NewRequest("GET",
						fmt.Sprintf("%s/games/%s/details", server.URL, gameID), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.Status, ShouldEqual, "200 OK")

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)

					parsedResponse := new(janus.GetGameDetailsResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)
					checkParsedResponse(parsedResponse, gameID, "", true, false, false, true)
				})

				Convey("with a published, not yet buy-able game", func() {
					gameID = "publishedgame"

					Convey("with whitelisted user", func() {
						userID = "12345"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)
						checkParsedResponse(parsedResponse, gameID, userID, true, false, false, false)
					})

					Convey("with non-whitelisted user", func() {
						userID = "5678"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)
						checkParsedResponse(parsedResponse, gameID, userID, false, false, false, false)
					})
				})

				Convey("with an unpublished future game", func() {
					gameID = "futuregame"

					Convey("with whitelisted user", func() {
						userID = "12345"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "200 OK")

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetGameDetailsResponse)
						err = json.Unmarshal(body, parsedResponse)
						So(err, ShouldBeNil)
						checkParsedResponse(parsedResponse, gameID, userID, true, false, false, false)
					})

					Convey("without a userid", func() {
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details", server.URL, gameID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})

					Convey("with an empty userid", func() {
						userID = ""
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})

					Convey("with a non-whitelisted userid", func() {
						userID = "5678"
						req, err := http.NewRequest("GET",
							fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
						So(err, ShouldBeNil)

						resp, err := http.DefaultClient.Do(req)
						So(err, ShouldBeNil)
						So(resp.Status, ShouldEqual, "404 Not Found")
					})
				})
			})
		})

		Convey("When modifying only appsettings", func() {
			gameID = "buyablegame"
			userID = "12345"
			Convey("when app settings are off", func() {
				appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil).Once()

				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetGameDetailsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

			})
			Convey("When app settings are on", func() {
				appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(true, nil).Once()

				req, err := http.NewRequest("GET",
					fmt.Sprintf("%s/games/%s/details?userid=%s", server.URL, gameID, userID), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetGameDetailsResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)
			})
		})

	})
}

//TODO remove the two bools, and fix up the usage
func checkParsedResponse(parsedResponse *janus.GetGameDetailsResponse, gameID string, userID string, isBuyable bool, hasNoneFields bool, hasMissingFulfillment bool, hasMalformedVods bool) {
	So(parsedResponse.Product.ASIN, ShouldNotBeEmpty)
	So(parsedResponse.Product.Description, ShouldNotBeEmpty)
	So(parsedResponse.Product.Edition, ShouldNotBeEmpty)
	So(parsedResponse.Product.GameID, ShouldEqual, gameID)
	So(parsedResponse.Product.GamePublicationDate, ShouldHappenOnOrBefore, timeZero)
	So(parsedResponse.Product.GameEmbargoDate, ShouldHappenAfter, timeZero)
	So(parsedResponse.Product.Title, ShouldNotBeEmpty)
	So(parsedResponse.Product.WebsiteURL, ShouldNotBeEmpty)
	So(parsedResponse.Product.Crate.ASIN, ShouldNotBeEmpty)

	if isBuyable {
		So(parsedResponse.Product.Price.Price, ShouldNotBeEmpty)
		So(parsedResponse.Product.ReleaseEndDate, ShouldHappenAfter, timeNow)
		So(parsedResponse.ActionDetails.DestinationURL, ShouldNotBeBlank)
	} else {
		So(parsedResponse.Product.Price.Price, ShouldBeEmpty)
		So(parsedResponse.Product.ReleaseEndDate, ShouldHappenOnOrBefore, timeZero)
		So(parsedResponse.ActionDetails.DestinationURL, ShouldBeBlank)
	}

	if len(userID) == 0 {
		So(parsedResponse.UserEntitlement, ShouldBeNil)
	} else {
		So(parsedResponse.UserEntitlement.UserID, ShouldEqual, userID)
		So(parsedResponse.UserEntitlement.Entitled, ShouldBeTrue)
	}

	if hasNoneFields {
		So(parsedResponse.Product.EULAURL, ShouldBeBlank)
		So(parsedResponse.Product.Media.Videos, ShouldBeEmpty)

	} else {
		So(parsedResponse.Product.EULAURL, ShouldNotBeBlank)

		if hasMalformedVods {
			So(parsedResponse.Product.Media.Videos, ShouldBeEmpty)
		} else {
			So(len(parsedResponse.Product.Media.Videos), ShouldEqual, 5)
			So(parsedResponse.Product.Media.Videos[0].VideoID, ShouldEqual, "1")
			So(parsedResponse.Product.Media.Videos[1].VideoID, ShouldEqual, "2")
			So(parsedResponse.Product.Media.Videos[2].VideoID, ShouldEqual, "3")
			So(parsedResponse.Product.Media.Videos[3].VideoID, ShouldEqual, "4")
			So(parsedResponse.Product.Media.Videos[4].VideoID, ShouldEqual, "5")
		}
	}

	if hasMissingFulfillment {
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.IsExternalAcquisition, ShouldBeFalse)
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.AcquisitionName, ShouldEqual, testAcquisitionName)
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.AcquisitionDownloadURL, ShouldNotBeEmpty)
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.AcquisitionText, ShouldEqual, testAcquisitionText)

		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.IsExternalPlatform, ShouldBeFalse)
		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.PlatformName, ShouldEqual, fuelPlatformName)
		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.PlatformDownloadURL, ShouldEqual, fuelPlatformURL)
		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.PlatformText, ShouldEqual, fuelPlatformText)
	} else {
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.IsExternalAcquisition, ShouldBeFalse)
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.AcquisitionName, ShouldEqual, testAcquisitionName)
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.AcquisitionDownloadURL, ShouldNotBeEmpty)
		So(parsedResponse.Product.FulfillmentDetails.AcquisitionDetails.AcquisitionText, ShouldEqual, testAcquisitionText)

		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.IsExternalPlatform, ShouldBeTrue)
		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.PlatformName, ShouldEqual, testPlatformName)
		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.PlatformDownloadURL, ShouldEqual, testPlatformURL)
		So(parsedResponse.Product.FulfillmentDetails.PlatformDetails.PlatformText, ShouldEqual, testPlatformText)
	}

}

func TestGetGameDetails_LanguageParsing(t *testing.T) {
	Convey("When parsing languages", t, func() {
		api := &GameDetailsAPI{
			LanguageRankingMap: map[string]int{
				"English": 0,
				"French":  1,
				"Italian": 2,
			},
			LanguageDisplayMap: map[string]string{
				"simplified_chinese":  "Chinese (Simplified)",
				"traditional_chinese": "Chinese (Traditional)",
			},
		}
		languageTypes := map[string]string{
			"0": "manual",
			"1": "subtitled",
			"2": "audio_description",
		}
		languageValues := map[string]string{
			"0": "english",
			"1": "english",
			"2": "english",
		}
		Convey("Types are properly populated", func() {
			languages := api.deserializeLanguages(languageTypes, languageValues)
			So(len(languages), ShouldEqual, 1)

			language := languages[0]
			So(language.IsAudio, ShouldBeTrue)
			So(language.IsSubtitles, ShouldBeTrue)
			So(language.IsInterface, ShouldBeTrue)
		})
		Convey("None language sentry gets filtered", func() {
			languageValues["2"] = "none"
			languages := api.deserializeLanguages(languageTypes, languageValues)
			So(len(languages), ShouldEqual, 1)

			language := languages[0]
			So(language.IsAudio, ShouldBeFalse)
			So(language.IsSubtitles, ShouldBeTrue)
			So(language.IsInterface, ShouldBeTrue)
		})
		Convey("Languages are properly sorted", func() {
			languageValues["0"] = "french"
			languageValues["1"] = "italian"
			languages := api.deserializeLanguages(languageTypes, languageValues)
			So(len(languages), ShouldEqual, 3)

			english := languages[0]
			french := languages[1]
			italian := languages[2]
			So(english.Language, ShouldEqual, "English")
			So(french.Language, ShouldEqual, "French")
			So(italian.Language, ShouldEqual, "Italian")
		})
		Convey("When custom languages are involved", func() {
			Convey("When value is in the map", func() {
				displayLanguage := api.convertLanguageEnumToDisplayValue("simplified_chinese")
				So(displayLanguage, ShouldEqual, "Chinese (Simplified)")
			})
			Convey("When fall back value is used", func() {
				displayLanguage := api.convertLanguageEnumToDisplayValue("portuguese_brazilian")
				So(displayLanguage, ShouldEqual, "Portuguese Brazilian")
			})
		})
	})
}
