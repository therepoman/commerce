package api

// Helper functions used by API calls

import (
	"encoding/json"
	"net/http"

	"strings"

	log "github.com/sirupsen/logrus"
)

const noneString = "none"

func writeResponse(t interface{}, w http.ResponseWriter) {
	resp, err := json.Marshal(t)
	if err != nil {
		log.Errorf("Unable to marshal %s into json. Encountered error %s", t, err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Unable to marshal response"))
		if err != nil {
			log.Warningf("Failed to write marshal response error: %s", err)
		}

		return
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(resp)

	if err != nil {
		log.Errorf("Failed while writing a (normal) response out via HTTP")
	}
}

func blankIfNone(field string) string {
	if strings.ToLower(field) == noneString {
		return ""
	}
	return field
}
