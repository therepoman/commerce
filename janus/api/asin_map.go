package api

import (
	"golang.org/x/net/context"

	log "github.com/sirupsen/logrus"

	"encoding/json"
	"net/http"

	"strings"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/metadata"
	"goji.io/pat"
)

// GameAsinAPI represents the API to get and set asin mappings in dynamo
type GameAsinAPI struct {
	AsinMap metadata.IAsinMap
}

// getGameAsin API is used to retrieve the mapping from a game title to the associated asin
// Note: mappings are case insensitive
func (api *GameAsinAPI) getGameAsin(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameTitle := strings.ToLower(pat.Param(c, "game"))

	result, err := api.AsinMap.Get(gameTitle)
	if err != nil {
		log.Errorf("Error retrieving asin mapping from DAO. Game: %s, Error: %s", gameTitle, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if result == nil {
		log.Infof("Requested game %s not found", gameTitle)
		http.Error(w, "404 page not found", http.StatusNotFound)
		return
	}

	writeResponse(result, w)
}

// setGameAsin API is used to set the mapping from a game title to the associated asin
// Note: mappings are case insensitive
func (api *GameAsinAPI) setGameAsin(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameTitle := strings.ToLower(pat.Param(c, "game"))

	var request janus.GameAsinMap
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		log.Errorf("Error decoding setGameAsin request: %+v", err)
		http.Error(w, genericBadRequest, http.StatusBadRequest)
		return
	}
	request.GameTitle = gameTitle

	if request.Asin == "" {
		log.Infof("Empty asin in request: %+v", request)
		http.Error(w, "Asin is not valid", http.StatusBadRequest)
		return
	}
	request.Asin = strings.Trim(request.Asin, " ")

	if request.Status == "" {
		log.Infof("No status provided. Defaulting to LIVE for game asin map. Game: %s, Asin: %s", gameTitle, request.Asin)
		request.Status = metadata.StatusLive
	}

	if !isValidStatus(request.Status) {
		log.Infof("Invalid status in request: %+v", request)
		http.Error(w, "Status is not valid", http.StatusBadRequest)
		return
	}

	if err := api.AsinMap.Put(request); err != nil {
		log.Errorf("Error putting asin map into datastore: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	writeResponse(request, w)
}

// IsValidStatus returns true if the input status represents a valid asin map status, false otherwise.
func isValidStatus(status string) bool {
	return status == metadata.StatusLive || status == metadata.StatusSuppressed
}
