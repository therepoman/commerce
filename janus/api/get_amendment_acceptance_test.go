package api

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/revenue/moneypenny/client"

	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func initAmendmentMocks() {
	metricLogger.On("LogDurationMetric", mock.Anything, mock.Anything).Return()
	metricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

	testDisplayName := "test"
	userResp := models.Properties{
		Displayname: &testDisplayName,
	}

	gatingHelper.On("UserIsIdWrapper", mock.Anything, mock.Anything).Return(MockWrapper(false, errors.New("test error")))
	gatingHelper.On("IsAdminOrSubAdminWrapper", mock.Anything, mock.Anything).Return(MockWrapper(false, errors.New("test error")))

	usersService.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&userResp, nil)
	spadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
}

// Test AmendmentAcceptanceAPI GetAmendmentAcceptance call with an amendment
// that has a valid acceptance date
func TestGetAmendmentAcceptance_WithAcceptanceDate(t *testing.T) {
	userTuid := "1234"
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		initAmendmentMocks()

		server := httptest.NewServer(api)
		defer server.Close()

		req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/123/amendment/fuel", server.URL), nil)
		So(err, ShouldBeNil)

		Convey("where authz checks return false", func() {
			cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userTuid, nil)
			gatingHelper.On("CheckAuthChain", mock.Anything).Return(false, []error{})
			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "403 Forbidden")
		})

		Convey("where authz checks error", func() {
			cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userTuid, nil)
			gatingHelper.On("CheckAuthChain", mock.Anything).Return(false, []error{errors.New("test error")})
			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "500 Internal Server Error")
		})

		Convey("where custom user_is_id succeeds", func() {
			cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userTuid, nil)
			gatingHelper.On("CheckAuthChain", mock.Anything).Return(true, []error{})

			Convey("when the amendment exists", func() {
				amendmentDao.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Amendment{
					UserID:         "123",
					AmendmentType:  "fuel",
					AcceptanceDate: time.Now(),
				}, nil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GetAmendmentAcceptanceResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.AmendmentAccepted, ShouldBeTrue)
			})

			Convey("when the amendment doesn't exist", func() {
				amendmentDao.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GetAmendmentAcceptanceResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.AmendmentAccepted, ShouldBeFalse)
			})

			Convey("when AmendmentDAO throws an error", func() {
				amendmentDao.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})
		})
	})
}

// Test AmendmentAcceptanceAPI SetAmendmentAcceptance call with valid JSON request
func TestSetAmendmentAcceptance_Success(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		initAmendmentMocks()
		cartmanAuthorizer.On("AuthorizeCartmanToken", mock.Anything, mock.Anything).Return(nil)

		server := httptest.NewServer(api)
		defer server.Close()

		req, err := http.NewRequest("PUT", fmt.Sprintf("%s/broadcaster/123/amendment/fuel", server.URL), nil)
		So(err, ShouldBeNil)

		isNotPartnerResponse := &moneypenny.GetUserPayoutTypeResponse{IsPartner: false}
		isPartnerResponse := &moneypenny.GetUserPayoutTypeResponse{IsPartner: true}

		Convey("when the amendment type is wrong", func() {
			badReq, err := http.NewRequest("PUT", fmt.Sprintf("%s/broadcaster/123/amendment/twork", server.URL), nil)
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(true, nil)
			So(err, ShouldBeNil)
			amendmentDao.On("Put", mock.Anything).Return(nil, nil)

			resp, err := http.DefaultClient.Do(badReq)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "400 Bad Request")
		})

		Convey("when the amendment is being accepted for a user that is not the acting user", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(false, nil)
			moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isPartnerResponse, nil)
			So(err, ShouldBeNil)
			amendmentDao.On("Put", mock.Anything).Return(nil, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "403 Forbidden")
		})

		Convey("when the amendment is being accepted for a non-partner", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(true, nil)
			moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isNotPartnerResponse, nil)
			So(err, ShouldBeNil)
			amendmentDao.On("Put", mock.Anything).Return(nil, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "403 Forbidden")
		})

		Convey("when the amendment is being accepted is authorized", func() {
			Convey("when the amendment is persisted successfully", func() {
				amendmentDao.On("Put", mock.Anything).Return(nil)
				gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(true, nil)
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isPartnerResponse, nil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "200 OK")

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.SetAmendmentAcceptanceResponse)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.AcceptDate, ShouldNotBeNil)
			})

			Convey("when the amendment is not persisted successfully", func() {
				amendmentDao.On("Put", mock.Anything).Return(errors.New("test error"))
				gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(true, nil)
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isPartnerResponse, nil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})
		})

		Convey("when GatingHelper throws an error", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(nil, nil)
			moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isPartnerResponse, nil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "500 Internal Server Error")
		})

		Convey("when MoneyPennyClient throws an error", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(true, nil)
			moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "500 Internal Server Error")
		})

	})
}
