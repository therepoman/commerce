package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/common/twitchhttp"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	hasTwoEmoticonsTuid  = "1234"
	hasZeroEmoticonsTuid = "5678"
	unknownTuid          = "0000"

	emoticonSet1 = "35"
	emoticonSet2 = "42"
)

var makoKeysForZeroEmoticonSets = []string{}
var makoKeysForTwoEmoticonSets = []string{emoticonSet1, emoticonSet2}

var makoResponseWithZeroEntitlements = &models.MakoEntitlementSets{Keys: []string{}}
var makoResponseWithTwoEntitlements = &models.MakoEntitlementSets{Keys: []string{emoticonSet1, emoticonSet2}}
var chatResponseWithZeroEntitlements = &models.ChatEmoticonDetails{Details: map[string][]models.Emoticon{}}
var chatResponseWithTwoEntitlements = &models.ChatEmoticonDetails{
	Details: map[string][]models.Emoticon{
		emoticonSet1: []models.Emoticon{
			{ID: "187", Pattern: "lulOnlinetony"},
			{ID: "76", Pattern: "lulCaliPower"},
			{ID: "78", Pattern: "lulKO"},
			{ID: "79", Pattern: "lulCHEESY"},
			{ID: "80", Pattern: "lulMURDERFACE"},
		},
		emoticonSet2: []models.Emoticon{
			{ID: "483", Pattern: "\\&lt\\;3"},
			{ID: "484", Pattern: "R-?\\)"},
			{ID: "486", Pattern: "\\:\\&gt\\;"},
			{ID: "488", Pattern: ":-?(?:7|L)"},
			{ID: "489", Pattern: "\\:-?\\("},
		},
	},
}

func initMocks() {
	metricLogger.On("LogDurationMetric", Anything, Anything).Return()
	metricLogger.On("LogCountMetric", Anything, Anything).Return()

	makoClient.On("GetEmoticonSets", Anything, hasTwoEmoticonsTuid).
		Return(makoResponseWithTwoEntitlements, nil)

	makoClient.On("GetEmoticonSets", Anything, hasZeroEmoticonsTuid).
		Return(makoResponseWithZeroEntitlements, nil)

	makoClient.On("GetEmoticonSets", Anything, unknownTuid).
		Return(makoResponseWithZeroEntitlements, nil)

	makoClient.On("GetEmoticonsByGroups", Anything, makoKeysForTwoEmoticonSets).
		Return(chatResponseWithTwoEntitlements, nil)

	makoClient.On("GetEmoticonsByGroups", Anything, makoKeysForZeroEmoticonSets).
		Return(chatResponseWithZeroEntitlements, nil)

	makoClient.On("GetEmoticonsByGroups", Anything, unknownTuid).
		Return(chatResponseWithZeroEntitlements, nil)
}

// Test GetAllEmoticons with valid input
func TestGetAllEmoticons_ValidInput(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initMocks()

		var tuid string
		Convey("when the tuid has two entitlements", func() {
			tuid = hasTwoEmoticonsTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/emoticons", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetEmoticonsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.EmoticonSets), ShouldEqual, 2)
		})

		Convey("when the tuid has zero entitlements", func() {
			tuid = hasZeroEmoticonsTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/emoticons", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetEmoticonsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.EmoticonSets), ShouldEqual, 0)
		})

		Convey("when the tuid does not exist in Mako", func() {
			tuid = unknownTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/emoticons", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetEmoticonsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.EmoticonSets), ShouldEqual, 0)
		})

		Convey("when the current tuid doesn't match the requested tuid", func() {
			tuid := hasTwoEmoticonsTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(false, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/emoticons", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusForbidden)

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetEmoticonsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldNotBeNil)

			So(len(parsedResponse.EmoticonSets), ShouldEqual, 0)
		})

		Convey("when auth call fails", func() {
			tuid := hasTwoEmoticonsTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, twitchhttp.Error{StatusCode: 500})

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/emoticons", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetEmoticonsResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldNotBeNil)

			So(len(parsedResponse.EmoticonSets), ShouldEqual, 0)
		})
	})
}
