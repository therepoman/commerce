package api

import (
	"net/http"
	"net/url"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/metadata"
	"code.justin.tv/commerce/janus/models"
	log "github.com/sirupsen/logrus"
	"goji.io/pat"
	"golang.org/x/net/context"
)

const docket = "twitchDetailPage.json"

// GameSettingsAPI represents the API to get game settings
type GameSettingsAPI struct {
	GameMetadataDB metadata.GameMetadataDB
	GatingHelper   auth.IGatingHelper
	Cache          cache.IADGDiscoCache
}

func (gameSettingsAPI *GameSettingsAPI) getGameSettings(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameID := pat.Param(c, "game")

	// TODO - Remove after fixing
	var err error
	unescapedGameID, err := url.QueryUnescape(gameID)
	if err != nil {
		unescapedGameID = gameID
	}
	gameID = unescapedGameID

	// userID is optional
	userID := r.Form.Get("userid")

	// Allows users to see hidden product details (staff + whitelist)
	shouldShowHiddenDetails := gameSettingsAPI.GatingHelper.CanSeeHiddenInformation(r, userID, gameID)

	gameMetadata, err := gameSettingsAPI.GameMetadataDB.Lookup(gameID, shouldShowHiddenDetails)
	if err != nil {
		log.Errorf("Error doing metadata lookup. Game: %s, UserID: %s, Error: %s", gameID, userID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	exists := gameMetadata != nil

	// TODO integrate with actual adgclient and getProductDetails
	// Temporary direct access of the cache to test performance metrics
	if exists {
		cacheParams := &cache.ADGDiscoCacheParams{ASIN: gameMetadata.Asin, UserID: "", Docket: docket}
		cacheAdgProduct := gameSettingsAPI.Cache.GetProductDetails(cacheParams)
		if cacheAdgProduct == nil {
			sentryProduct := &models.ADGProduct{
				ASIN: gameMetadata.Asin,
			}
			gameSettingsAPI.Cache.SetProductDetails(cacheParams, sentryProduct)
		}
	}

	response := janus.GetGameSettingsResponse{
		DetailsEnabled: exists,
	}

	writeResponse(response, w)
}
