package api

import (
	"testing"

	"code.justin.tv/commerce/janus/auth"

	. "github.com/smartystreets/goconvey/convey"
)

func TestBlankIfNone(t *testing.T) {
	Convey("that blankIfNone works", t, func() {
		Convey("with a 'None' field", func() {
			So(blankIfNone("NoNe"), ShouldBeBlank)
		})
		Convey("with a non-'None' field", func() {
			So(blankIfNone("Not none"), ShouldEqual, "Not none")
		})
	})
}

func MockWrapper(b bool, e error) auth.AuthChainFunction {
	return func() (bool, error) {
		return b, e
	}
}
