package api

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/models"

	"encoding/json"

	"code.justin.tv/common/twitchhttp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func MockCrateGood(goodID string, productID string, productIconURL string, productTitle string) models.CrateGood {
	return models.CrateGood{
		CrateID:        goodID,
		ProductID:      productID,
		ProductIconURL: productIconURL,
		ProductTitle:   productTitle,
	}
}

func TestGetCratesGoConvey(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		tuid := "1234"

		server := httptest.NewServer(api)
		defer server.Close()

		metricLogger.On("LogDurationMetric", mock.Anything, mock.Anything).Return()

		Convey("when GET /inventory/1234/crates is requested 2 results", func() {
			gatingHelper.On("UserIsId", mock.Anything, tuid).Return(true, nil)

			mockedADGESGetGoodsResponse :=
				[]models.CrateGood{
					MockCrateGood("crateId1", "productId1", "iconURL1", "productTitle1"),
					MockCrateGood("crateId2", "productId2", "iconURL2", "productTitle2"),
				}
			adgEntitleClient.On("GetCrates", mock.Anything, tuid).Return(mockedADGESGetGoodsResponse, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/crates", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)

			Convey("the response has the correct HTTP status code", func() {
				So(resp.Status, ShouldEqual, "200 OK")
			})

			Convey("the reponse body has the correct body", func() {
				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetCratesResponse)
				err = json.Unmarshal(body, parsedResponse)

				So(len(parsedResponse.Crates), ShouldEqual, 2)

				for _, crateGood := range parsedResponse.Crates {
					So(crateGood.OpenCrateURL, ShouldNotBeEmpty)
					So(crateGood.ProductIconURL, ShouldNotBeEmpty)
					So(crateGood.ProductTitle, ShouldNotBeEmpty)
				}
			})
		})

		Convey("when GET /inventory/1234/crates is requested with no results", func() {
			gatingHelper.On("UserIsId", mock.Anything, tuid).Return(true, nil)

			mockedADGESGetGoodsResponse := []models.CrateGood{}
			adgEntitleClient.On("GetCrates", mock.Anything, tuid).Return(mockedADGESGetGoodsResponse, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/crates", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)

			Convey("the response has the correct HTTP status code", func() {
				So(resp.Status, ShouldEqual, "200 OK")
			})

			Convey("the reponse body has the correct body", func() {
				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)

				parsedResponse := new(janus.GetCratesResponse)
				err = json.Unmarshal(body, parsedResponse)

				So(len(parsedResponse.Crates), ShouldEqual, 0)
			})
		})

		Convey("when ADGEClient throws an error", func() {
			gatingHelper.On("UserIsId", mock.Anything, tuid).Return(true, nil)

			adgEntitleClient.On("GetCrates", mock.Anything, tuid).Return(nil, errors.New("test error"))

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/crates", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)
		})

		Convey("when the tuid is missing", func() {
			adgEntitleClient.On("GetCrates", mock.Anything, tuid).Return(nil, errors.New("test error"))

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory//crates", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusNotFound)
		})

		Convey("when the current tuid doesn't match the requested tuid", func() {
			gatingHelper.On("UserIsId", mock.Anything, tuid).Return(false, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/crates", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusForbidden)
		})

		Convey("when auth call fails", func() {
			gatingHelper.On("UserIsId", mock.Anything, tuid).Return(true, twitchhttp.Error{StatusCode: 500})

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/crates", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)
		})
	})
}
