package api

import (
	"net/http"
	"strings"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/metadata"

	"encoding/json"

	"net/url"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/crate"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/models"
	log "github.com/sirupsen/logrus"
	"goji.io/pat"
	"golang.org/x/net/context"
)

const (
	topTierPassesAndExpansions = "game-microtransactions-for-account-features-passes-and-expansions"
	topTierLootAndCollectibles = "game-microtransactions-for-loot-item-bundles-and-collectibles"
)

var topTierContentTypes = []string{
	topTierPassesAndExpansions,
	topTierLootAndCollectibles,
}

// InGameContentDetailsAPI represents the API to get game content
type InGameContentDetailsAPI struct {
	ADGDiscoClient         clients.IADGDiscoClientWrapper
	ADGEntitleClient       clients.IADGEntitleClientWrapper
	GameMetadataDB         metadata.GameMetadataDB
	GatingHelper           auth.IGatingHelper
	CheckoutDestinationURL string
	DetailsImageAssetsURL  string
	MetricsLogger          metrics.IMetricLogger
	CrateHelper            crate.ICrateHelper
}

func (inGameContentDetailsAPI *InGameContentDetailsAPI) getInGameContentDetails(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameID := pat.Param(c, "game")

	// userID is optional
	userID := r.Form.Get("userid")

	isAuthenticatedRequest := false
	if len(userID) > 0 {
		log.Infof("Request is authenticated for user: %s", userID)
		isAuthenticatedRequest = true
	}

	// Allows users to see hidden product details (staff + whitelist)
	shouldShowHiddenDetails := inGameContentDetailsAPI.GatingHelper.CanSeeHiddenInformation(r, userID, gameID)

	var err error
	gameMetadata, err := inGameContentDetailsAPI.GameMetadataDB.Lookup(gameID, shouldShowHiddenDetails)
	if err != nil {
		log.Errorf("Error doing metadata lookup. Game: %s, UserID: %s, Error: %s", gameID, userID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if gameMetadata == nil {
		http.Error(w, "404 page not found", http.StatusNotFound)
		return
	}

	inGameContent, err := inGameContentDetailsAPI.fetchInGameContent(gameID, shouldShowHiddenDetails, *gameMetadata, userID, isAuthenticatedRequest)
	if err != nil {
		log.Errorf("Error fetching in-game content for product. Error: %s", err)
		log.Warnf("Error fetching in-game content for product. Additional details: Game: %s, UserID: %s, Error: %s", gameID, userID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := janus.GetInGameContentDetailsResponse{
		InGameContent: inGameContent,
	}

	//TODO: Remove ICG which is not released yet or is expired

	json, err := json.Marshal(response)
	if err != nil {
		log.Errorf("Failed to marshall the inGameContentDetails object: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(json)
	if err != nil {
		log.Errorf("Failed to write output json: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (inGameContentDetailsAPI *InGameContentDetailsAPI) fetchInGameContent(gameID string, shouldShowHiddenDetails bool, gameMetadata models.GameMetadata, userID string, isAuthenticatedRequest bool) ([]janus.InGameContent, error) {
	asin := gameMetadata.Asin
	adgProducts, err := inGameContentDetailsAPI.ADGDiscoClient.GetProductsInDomainByASIN(asin, userID)
	if err != nil {
		return nil, err
	}

	var childAsinSlice []string
	var entitlements map[string]bool
	inGameContents := []janus.InGameContent{}

	if len(adgProducts) == 0 {
		return inGameContents, nil
	}

	if isAuthenticatedRequest {
		// Make a slice of asins
		for _, adgProduct := range adgProducts {
			childAsinSlice = append(childAsinSlice, adgProduct.ASIN)
		}

		// Convert asin slice to a comma seperated string list
		childAsinString := strings.Join(childAsinSlice, ",")
		entitlements, err = inGameContentDetailsAPI.fetchUserEntitlements(childAsinString, userID)
		if err != nil {
			log.WithFields(log.Fields{
				"Asin":   asin,
				"UserId": userID,
				"error":  err,
			}).Error("Failed to retrieve user entitlements from ADGEntitlementService. Forcing all entitlements to false and returning healthy response")
		}
	}

	for _, adgProduct := range adgProducts {
		inGameContent := inGameContentDetailsAPI.parseInGameContent(adgProduct, gameMetadata, entitlements, userID)
		inGameContents = append(inGameContents, inGameContent)
	}

	return inGameContents, nil
}

func (inGameContentDetailsAPI *InGameContentDetailsAPI) fetchUserEntitlements(asins string, userID string) (map[string]bool, error) {
	entitlementMap, err := inGameContentDetailsAPI.ADGEntitleClient.GetEntitledAsins(asins, userID, clients.FilterConsumables)
	if err != nil {
		return nil, err
	}
	return entitlementMap, nil
}

func (inGameContentDetailsAPI *InGameContentDetailsAPI) buildActionDetails(asin string) *janus.ActionDetails {
	return &janus.ActionDetails{
		DestinationURL: inGameContentDetailsAPI.buildCheckoutURL(asin),
	}
}

func (inGameContentDetailsAPI *InGameContentDetailsAPI) buildCheckoutURL(asin string) string {
	query := url.Values{}
	query.Set("asin", asin)
	return inGameContentDetailsAPI.CheckoutDestinationURL + query.Encode()
}

func (inGameContentDetailsAPI *InGameContentDetailsAPI) parseInGameContent(adgProduct models.ADGProduct, gameMetadata models.GameMetadata, entitlements map[string]bool, userID string) janus.InGameContent {
	asin := adgProduct.ASIN
	//TODO: Add IGC release dates
	return janus.InGameContent{
		ActionDetails: inGameContentDetailsAPI.buildActionDetails(asin),
		ContentDetails: &janus.ContentDetails{
			Crate:               parseCrate(adgProduct),
			Crates:              inGameContentDetailsAPI.CrateHelper.ParseCrates(adgProduct),
			ASIN:                asin,
			Description:         adgProduct.Description,
			ShortDescription:    adgProduct.Details.ShortDescription,
			GameEmbargoDate:     gameMetadata.GameEmbargoDate,
			GamePublicationDate: gameMetadata.GamePublicationDate,
			Media:               parseMedia(adgProduct),
			Price:               parsePrice(adgProduct),
			SKU:                 adgProduct.SKU,
			Tags:                adgProduct.Details.GenericKeywords,
			Title:               adgProduct.Title,
			Type:                adgProduct.Details.ItemType,
			IsFeatured:          parseIsFeatured(adgProduct.Details.ItemType),
		},
		UserEntitlement: lookupEntitlement(asin, userID, entitlements),
	}

}

func parseCrate(adgProduct models.ADGProduct) janus.Crate {
	return janus.Crate{
		ASIN: adgProduct.Details.Crate.ASIN,
	}
}

func parseMedia(adgProduct models.ADGProduct) janus.Media {
	return janus.Media{
		BackgroundImageURL: adgProduct.Details.Background.URL,
		BoxArtURL:          adgProduct.IconURL,
	}
}

func parsePrice(adgProduct models.ADGProduct) janus.Price {
	return janus.Price{
		CurrencyUnit: adgProduct.Price.CurrencyUnit,
		Price:        adgProduct.Price.Price,
	}
}

func lookupEntitlement(asin string, userID string, entitlements map[string]bool) *janus.UserEntitlement {
	// entitlements will be nil if unauthenticated
	return &janus.UserEntitlement{
		Entitled: entitlements[asin],
		UserID:   userID,
	}
}

func parseIsFeatured(itemType string) bool {
	for _, topTierContentType := range topTierContentTypes {
		if topTierContentType == itemType {
			return true
		}
	}
	return false
}
