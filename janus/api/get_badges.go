package api

import (
	"net/http"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"

	"fmt"

	"code.justin.tv/commerce/janus/auth"
	log "github.com/sirupsen/logrus"
	"goji.io/pat"
	"golang.org/x/net/context"
)

// GetBadgesAPI represents the API to get badges
type GetBadgesAPI struct {
	GatingHelper            auth.IGatingHelper
	MakoClientWrapper       clients.IMakoClientWrapper
	ChatBadgesClientWrapper clients.IChatBadgesClientWrapper
}

func (getBadgesAPI *GetBadgesAPI) getBadges(c context.Context, w http.ResponseWriter, r *http.Request) {

	tuid := pat.Param(c, "tuid") // tuid is part of URL path so shouldn't be nil, but that could change
	if len(tuid) == 0 {
		log.Errorf("Error getting emoticons: tuid is nil")
		http.Error(w, "Error getting emoticons: tuid is nil", http.StatusBadRequest)
		return
	}

	// Authorization: Tuid being looked up must be the same as the acting tuid.
	userIsId, err := getBadgesAPI.GatingHelper.UserIsId(r, tuid)
	if err != nil {
		log.Warnf("Could not authorize current user to getBadgesAPI for Twitch user %s: %s", tuid, err)
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	} else if !userIsId {
		errorMsg := fmt.Sprintf("Not authorized to getBadgesAPI for Twitch user %s", tuid)
		log.Warn(errorMsg)
		http.Error(w, errorMsg, http.StatusForbidden)
		return
	}

	userBadges, err := getBadgesAPI.MakoClientWrapper.GetBadgeSets(c, tuid)
	if err != nil { // only returns err if server failure
		msg := fmt.Sprintf("Error fetching badge sets: %s", err.Error())
		log.Errorf("Error fetching badge sets: %s", err)
		http.Error(w, msg, http.StatusInternalServerError)
		return
	}

	response := janus.GetBadgesResponse{}
	if len(userBadges.Keys) > 0 {
		// Fetch display information about badges
		badgesInformation, err := getBadgesAPI.ChatBadgesClientWrapper.GetBadgeDisplay(c)
		if err != nil {
			log.Errorf("Error fetching badge display details: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Merge both data sets
		badgesSets := map[string]janus.BadgeSet{}
		for _, badgeSet := range userBadges.Keys {
			set, exists := badgesInformation.BadgeSets[badgeSet]
			if !exists {
				log.Warnf("Badge set %s not found in general badge map", badgeSet)
				continue
			}

			// there can be multiple versions for the same badge, typically a numerical ordinal e.g. {1} or {1,2,...}
			// but it can also be alphabetical, e.g. {alliance, horde}
			// we will choose the highest numerical ordinal
			badgeVersion, err := clients.GetHighestBadgeVersion(set.Versions)
			if err != nil {
				log.Warnf("Badge set %s not found with a suitable version key", badgeSet)
				continue
			}
			version := set.Versions[badgeVersion] // we know it exists, no need to check

			versions := map[string]janus.Badge{}
			versions[badgeVersion] = janus.Badge{
				ImageURL1X:  *version.ImageURL1x,
				ImageURL2X:  *version.ImageURL2x,
				ImageURL4X:  *version.ImageURL4x,
				Description: version.Description,
				Title:       version.Title,
				ClickAction: version.ClickAction,
				ClickURL:    version.ClickURL,
			}

			badgesSets[badgeSet] = janus.BadgeSet{
				Versions: versions,
			}
		}

		response.BadgeSets = badgesSets
	}

	writeResponse(response, w)
}
