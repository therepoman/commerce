package api

import (
	"encoding/json"
	"net/http"
	"path"
	"time"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/models"

	"net/url"

	"strings"

	"errors"

	"fmt"

	"sort"

	"hash/fnv"

	"strconv"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/crate"
	"code.justin.tv/commerce/janus/metadata"
	log "github.com/sirupsen/logrus"
	"goji.io/pat"
	"golang.org/x/net/context"
)

// this is used for formatting dates from ADG.  Will be used when ADG becomes the canonical source of dates.
// const layout = "2006-01-02T15:04:05Z"

// GameDetailsAPI represents the API to get game details
type GameDetailsAPI struct {
	ADGDiscoClient         clients.IADGDiscoClientWrapper
	ADGEntitleClient       clients.IADGEntitleClientWrapper
	GameMetadataDB         metadata.GameMetadataDB
	GatingHelper           auth.IGatingHelper
	RatingMap              models.RatingMap
	FulfillmentMetadataMap models.FulfillmentMetadataMap
	CheckoutDestinationURL string
	DetailsImageAssetsURL  string
	LanguageRankingMap     map[string]int
	LanguageDisplayMap     map[string]string
	CrateHelper            crate.ICrateHelper
}

const (
	esrbDir                = "esrb"
	pegiDir                = "pegi"
	platformDir            = "platform"
	fulfilledPrefix        = "fulfilled_"
	defaultFulfillmentType = "fulfilled_fuellauncher_default"
	audio                  = "audio_description"
	manual                 = "manual"
	subtitles              = "subtitled"
	underscore             = "_"
	space                  = " "
	vodPrefix              = "https://www.twitch.tv/videos/"
	nonePrefix             = "None" // Catalog sentry value for a game to specify that it intentionally has no videos or various language features
)

func (gameDetailsAPI *GameDetailsAPI) getGameDetails(c context.Context, w http.ResponseWriter, r *http.Request) {
	timeNow := time.Now()

	gameID := pat.Param(c, "game")

	// userID is optional
	userID := r.Form.Get("userid")

	isAuthenticatedRequest := false
	if len(userID) > 0 {
		log.Infof("Request is authenticated for user: %s", userID)
		isAuthenticatedRequest = true
	}

	// Allows users to see hidden product details (staff + whitelist)
	shouldShowHiddenDetails := gameDetailsAPI.GatingHelper.CanSeeHiddenInformation(r, userID, gameID)

	var err error
	gameMetadata, err := gameDetailsAPI.GameMetadataDB.Lookup(gameID, shouldShowHiddenDetails)
	if err != nil {
		log.Errorf("Error doing metadata lookup. Game: %s, UserID: %s, Error: %v", gameID, userID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if gameMetadata == nil {
		http.Error(w, "404 page not found", http.StatusNotFound)
		return
	}

	product, err := gameDetailsAPI.fetchProduct(gameID, userID, shouldShowHiddenDetails, *gameMetadata)
	if err != nil {
		log.Errorf("Error fetching product. Error: %s", err)
		log.Warnf("Error fetching product. Additional details: Game: %s, UserID: %s, Error: %s", gameID, userID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var userEntitlement *janus.UserEntitlement
	if isAuthenticatedRequest {
		userEntitlement, err = gameDetailsAPI.fetchUserEntitlement(userID, gameMetadata.Asin)
		if err != nil {
			log.Errorf("Error fetching user entitlement. Game: %s, UserID: %s, Error: %s", gameID, userID, err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	actionDetails := gameDetailsAPI.buildActionDetails(*gameMetadata)

	response := janus.GetGameDetailsResponse{
		Product:         product,
		UserEntitlement: userEntitlement,
		ActionDetails:   actionDetails,
	}

	gameDetailsAPI.scrubData(timeNow, response, *gameMetadata, shouldShowHiddenDetails)

	json, err := json.Marshal(response)
	if err != nil {
		log.Errorf("Failed to marshall the gameDetails object: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(json)
	if err != nil {
		log.Errorf("Failed to write output json: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (gameDetailsAPI *GameDetailsAPI) scrubData(now time.Time, response janus.GetGameDetailsResponse, metadata models.GameMetadata, shouldShowHiddenDetails bool) {
	// If outside of buy-ability window, then remove offer details. Whitelisted users should still be able
	// to see future offers
	if (!shouldShowHiddenDetails && now.Before(metadata.ReleaseStartDate)) || now.After(metadata.ReleaseEndDate) {
		response.ActionDetails.DestinationURL = ""
		response.Product.Price = janus.Price{
			Price:        "",
			CurrencyUnit: "",
		}
		response.Product.ReleaseEndDate = time.Time{}
	}
}

func (gameDetailsAPI *GameDetailsAPI) getFulfillmentType(itemBadgeDetails []string) (string, error) {
	fulfillmentType := ""
	for _, itemBadgeDetail := range itemBadgeDetails {
		itemBadgeDetailLowered := strings.ToLower(itemBadgeDetail)
		if strings.HasPrefix(itemBadgeDetailLowered, fulfilledPrefix) {
			fulfillmentType = itemBadgeDetailLowered
			break
		}
	}
	if fulfillmentType == "" {
		return "", fmt.Errorf("Unable to find fulfillmentType for strings: %+v", itemBadgeDetails)
	}
	return fulfillmentType, nil
}

func (gameDetailsAPI *GameDetailsAPI) fetchProduct(gameID string, userID string, shouldShowHiddenDetails bool, gameMetadata models.GameMetadata) (*janus.Product, error) {
	product := new(janus.Product)

	// populate information from metadata into product
	product.ASIN = gameMetadata.Asin
	product.GameEmbargoDate = gameMetadata.GameEmbargoDate
	product.GameID = gameID
	product.GamePublicationDate = gameMetadata.GamePublicationDate
	product.ReleaseEndDate = gameMetadata.ReleaseEndDate
	product.ReleaseStartDate = gameMetadata.ReleaseStartDate

	adgProduct, err := gameDetailsAPI.ADGDiscoClient.GetGameProductDetails(gameMetadata.Asin, userID)
	if err != nil {
		log.WithFields(log.Fields{
			"Asin":   gameMetadata.Asin,
			"UserId": userID,
			"error":  err,
		}).Error("Error getting product details from ADG")
		return nil, errors.New("500 Internal Server Error")
	}
	product.Crate = janus.Crate{
		ASIN: adgProduct.Details.Crate.ASIN,
	}
	product.Crates = gameDetailsAPI.CrateHelper.ParseCrates(*adgProduct)
	product.Description = adgProduct.Description
	product.DeveloperName = adgProduct.Details.Developer.Name
	product.Edition = adgProduct.Details.Attributes.Edition
	product.EULAURL = blankIfNone(adgProduct.Details.Developer.EULAURL)
	product.FeatureDetails = adgProduct.Details.FeaturesDetails
	product.FeatureIcons = []janus.FeatureIcon{} //TODO Not present at launch
	product.Genres = []string{}                  // TODO Not Present at launch

	fulfillmentType, err := gameDetailsAPI.getFulfillmentType(adgProduct.Details.ItemBadgeDetails)
	if err != nil {
		log.WithFields(log.Fields{
			"Asin":          adgProduct.ASIN,
			"UserId":        userID,
			"Videos":        adgProduct.Details.Videos,
			"PC techs":      adgProduct.Details.TechnicalDetails,
			"Whole Product": adgProduct,
			"error":         err,
		}).Error("Error getting product fulfillment type from ADG. Using default value instead.")
		fulfillmentType = defaultFulfillmentType
	}
	fulfillmentDetails, exists := gameDetailsAPI.FulfillmentMetadataMap[fulfillmentType]
	if !exists {
		log.Errorf("Failed to find fulfillmentType in map. Using default. Asin: %s, UserID: %s, Fulfillment Type: %s", adgProduct.ASIN, userID, fulfillmentType)
		fulfillmentDetails = gameDetailsAPI.FulfillmentMetadataMap[defaultFulfillmentType]
	}

	product.FulfillmentDetails = janus.FulfillmentDetails{
		AcquisitionDetails: janus.AcquisitionDetails{
			IsExternalAcquisition:  fulfillmentDetails.AcquisitionDetails.IsExternalAcquisition,
			AcquisitionDownloadURL: adgProduct.Details.Attributes.GameURL,
			AcquisitionName:        fulfillmentDetails.AcquisitionDetails.AcquisitionName,
			AcquisitionText:        fulfillmentDetails.AcquisitionDetails.AcquisitionText,
		},
		PlatformDetails: janus.PlatformDetails{
			IsExternalPlatform:  fulfillmentDetails.PlatformDetails.IsExternalPlatform,
			PlatformDownloadURL: fulfillmentDetails.PlatformDetails.PlatformDownloadURL,
			PlatformName:        fulfillmentDetails.PlatformDetails.PlatformName,
			PlatformText:        fulfillmentDetails.PlatformDetails.PlatformText,
		},
	}
	product.MaturityRatings = janus.MaturityRatings{
		ESRB: janus.MaturityRating{
			Details: []string{}, // TODO Missing Fields
			IconURL: gameDetailsAPI.buildImageAssetURL(esrbDir, adgProduct.Details.Attributes.ESRBRating),
			Rating:  gameDetailsAPI.RatingMap.ESRBRating[adgProduct.Details.Attributes.ESRBRating],
		},
		PEGI: janus.MaturityRating{
			Details: []string{}, // TODO Not present at launch
			IconURL: gameDetailsAPI.buildImageAssetURL(pegiDir, adgProduct.Details.Attributes.PEGIRating),
			Rating:  gameDetailsAPI.RatingMap.PEGIRating[adgProduct.Details.Attributes.PEGIRating],
		},
	}
	product.Media = janus.Media{
		BackgroundImageURL: adgProduct.Details.Background.URL,
		BoxArtURL:          adgProduct.IconURL,
		ScreenshotURLs:     adgProduct.Details.Screenshots,
		Videos:             gameDetailsAPI.deserializeVideos(adgProduct.Details.Videos, gameMetadata.Asin),
	}
	product.Price = janus.Price{
		CurrencyUnit: adgProduct.Price.CurrencyUnit,
		Price:        adgProduct.Price.Price,
	}
	product.PublisherName = adgProduct.Details.Attributes.Publisher
	product.SKU = adgProduct.SKU
	product.SupportedLanguages = gameDetailsAPI.deserializeLanguages(adgProduct.Details.LanguageType, adgProduct.Details.LanguageValue)
	product.SupportedPlatforms = gameDetailsAPI.deserializeSupportedPlatforms(adgProduct.Details.Platforms)
	product.SupportURL = adgProduct.Details.Developer.SupportURL
	product.SystemRequirements = janus.TieredSystemRequirements{
		Minimum: janus.SystemRequirements{
			DirectXVersion: adgProduct.Details.TechnicalDetails.Minimum.DirectXVersion,
			HardDriveSpace: adgProduct.Details.TechnicalDetails.Minimum.HardDriveSpace,
			OSVersion:      adgProduct.Details.TechnicalDetails.Minimum.OSVersion,
			Other:          blankIfNone(adgProduct.Details.TechnicalDetails.Minimum.Other),
			Processor:      adgProduct.Details.TechnicalDetails.Minimum.Processor,
			RAM:            adgProduct.Details.TechnicalDetails.Minimum.RAM,
			VideoCard:      adgProduct.Details.TechnicalDetails.Minimum.VideoCard,
		},
		Recommended: janus.SystemRequirements{
			DirectXVersion: adgProduct.Details.TechnicalDetails.Recommended.DirectXVersion,
			HardDriveSpace: adgProduct.Details.TechnicalDetails.Recommended.HardDriveSpace,
			OSVersion:      adgProduct.Details.TechnicalDetails.Recommended.OSVersion,
			Other:          blankIfNone(adgProduct.Details.TechnicalDetails.Minimum.Other),
			Processor:      adgProduct.Details.TechnicalDetails.Recommended.Processor,
			RAM:            adgProduct.Details.TechnicalDetails.Recommended.RAM,
			VideoCard:      adgProduct.Details.TechnicalDetails.Recommended.VideoCard,
		},
	}
	product.Title = adgProduct.Title
	product.VendorId = adgProduct.VendorId
	product.WebsiteURL = adgProduct.Details.Attributes.GameURL

	return product, nil
}

func (gameDetailsAPI *GameDetailsAPI) fetchUserEntitlement(userID string, asin string) (*janus.UserEntitlement, error) {
	entitled, err := gameDetailsAPI.ADGEntitleClient.IsEntitled(asin, userID)

	// In case of error from ADGES, consider the user not entitled
	if err != nil {
		log.WithFields(log.Fields{
			"Asin":   asin,
			"UserId": userID,
			"error":  err,
		}).Error("Error getting product entitlement from ADG")
		entitled = false
	}

	return &janus.UserEntitlement{
		UserID:   userID,
		Entitled: entitled,
	}, nil
}

func (gameDetailsAPI *GameDetailsAPI) buildActionDetails(gameMetadata models.GameMetadata) *janus.ActionDetails {
	action := new(janus.ActionDetails)
	action.DestinationURL = gameDetailsAPI.buildCheckoutURL(gameMetadata.Asin)
	return action
}

func (gameDetailsAPI *GameDetailsAPI) buildCheckoutURL(asin string) string {
	query := url.Values{}
	query.Set("asin", asin)
	return gameDetailsAPI.CheckoutDestinationURL + query.Encode()
}

func (gameDetailsAPI *GameDetailsAPI) buildImageAssetURL(directory string, filename string) string {
	url, err := url.Parse(gameDetailsAPI.DetailsImageAssetsURL)
	if err != nil {
		log.Errorf("Failed to generate URL for static image asset: %s", err)
		return ""
	}
	url.Path = path.Join(url.Path, directory, filename)
	return url.String()
}

func (gameDetailsAPI *GameDetailsAPI) deserializeSupportedPlatforms(platforms []string) []janus.Platform {
	supportedPlatforms := make([]janus.Platform, len(platforms))
	for index, platform := range platforms {
		supportedPlatforms[index] = janus.Platform{
			IconURL:  gameDetailsAPI.buildImageAssetURL(platformDir, strings.ToLower(platform)),
			Platform: platform,
		}
	}
	return supportedPlatforms
}

func (gameDetailsApi *GameDetailsAPI) deserializeVideos(videosMap map[string]string, asin string) []janus.Video {
	videos := make([]janus.Video, 0)
	// Preserving the order of the response from ADG
	for i := 0; i < len(videosMap); i++ {
		videoURL := videosMap[strconv.Itoa(i)]
		if nonePresent := strings.Contains(videoURL, nonePrefix); nonePresent {
			log.Infof("Game has no videos: %s", asin)
			continue
		}
		if prefixPresent := strings.Contains(videoURL, vodPrefix); !prefixPresent {
			log.Errorf("Malformed video URL from asin: %s - expected prefix: %+v was missing from URL: %+v", asin, vodPrefix, videoURL)
			continue
		}
		videoID := strings.TrimPrefix(videoURL, vodPrefix)
		videos = append(videos, janus.Video{
			VideoID: videoID,
		})
	}
	return videos
}

func (gameDetailsAPI *GameDetailsAPI) deserializeLanguages(types map[string]string, values map[string]string) []janus.Language {
	languageToFeature := make(map[string][]string)
	for key, feature := range types {
		language := gameDetailsAPI.convertLanguageEnumToDisplayValue(values[key])
		languageToFeature[language] = append(languageToFeature[language], feature)
	}
	supportedLanguages := make([]janus.Language, 0)
	for language, features := range languageToFeature {
		if language == nonePrefix {
			continue
		}
		toInsert := janus.Language{
			IsAudio:     stringInArray(audio, features),
			IsInterface: stringInArray(manual, features),
			IsSubtitles: stringInArray(subtitles, features),
			Language:    language,
		}
		supportedLanguages = append(supportedLanguages, toInsert)
	}
	languageSorter := &byLanguage{
		languages:  supportedLanguages,
		rankingMap: gameDetailsAPI.LanguageRankingMap,
	}
	sort.Sort(languageSorter)
	return supportedLanguages
}

// TODO to support localizing strings we should punt the display value conversion to web client
func (gameDetailsAPI *GameDetailsAPI) convertLanguageEnumToDisplayValue(enum string) string {
	if value, isPresent := gameDetailsAPI.LanguageDisplayMap[enum]; isPresent {
		return value
	}
	// Since there is no mapping falling through to a backup display pattern
	return strings.Title(strings.Replace(enum, underscore, space, -1))
}

func stringInArray(toFind string, toSearch []string) bool {
	for _, current := range toSearch {
		if strings.ToLower(current) == toFind {
			return true
		}
	}
	return false
}

// A type for implementing the sort interface
type byLanguage struct {
	languages  []janus.Language
	rankingMap map[string]int
}

// SortInterface contract
func (x byLanguage) Len() int {
	return len(x.languages)
}

// SortInterface contract
func (x byLanguage) Less(i, j int) bool {
	return x.calculateOrder(x.languages[i].Language) < x.calculateOrder(x.languages[j].Language)
}

// SortInterface contract
func (x byLanguage) Swap(i, j int) {
	x.languages[i], x.languages[j] = x.languages[j], x.languages[i]
}

// helper function to determine the value of the language string
func (x byLanguage) calculateOrder(language string) int {
	if val, isPresent := x.rankingMap[language]; isPresent {
		return val
	}
	hashValue := x.hash(language)
	if hashValue < len(x.rankingMap) {
		hashValue += len(x.rankingMap)
	}
	return hashValue
}

func (x byLanguage) hash(input string) int {
	h := fnv.New32a()
	_, err := h.Write([]byte(input))
	if err != nil {
		return 0
	}
	return int(h.Sum32())
}
