package api

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"goji.io/pat"

	"net/url"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"

	"fmt"

	"code.justin.tv/commerce/janus/auth"
	"golang.org/x/net/context"
)

// GetCratesAPI represents the API to get crates
type GetCratesAPI struct {
	ADGEntitleClient     clients.IADGEntitleClientWrapper
	ADGCratesDomainID    string
	GatingHelper         auth.IGatingHelper
	RSWOpenCrateEndpoint string
}

func (getCrates *GetCratesAPI) getCrates(c context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(c, "tuid")

	if len(tuid) == 0 {
		log.Error("Request did not specify a valid tuid")
		return
	}

	// Authorization: Tuid being looked up must be the same as the acting tuid.
	userIsId, err := getCrates.GatingHelper.UserIsId(r, tuid)
	if err != nil {
		log.Warnf("Could not authorize current user to getCratesAPI for Twitch user %s: %s", tuid, err)
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	} else if !userIsId {
		errorMsg := fmt.Sprintf("Not authorized to getCratesAPI for Twitch user %s", tuid)
		log.Warn(errorMsg)
		http.Error(w, errorMsg, http.StatusForbidden)
		return
	}

	// ADGES call
	crateGoods, err := getCrates.ADGEntitleClient.GetCrates(getCrates.ADGCratesDomainID, tuid)
	if err != nil {
		log.Errorf("Error fetching crates: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Build response struct
	responseCrateGoods := []janus.CrateGood{}
	for _, crateGood := range crateGoods {
		openCrateURL, err := getCrates.constructOpenCrateURL(crateGood.CrateID, crateGood.ProductID)
		if err != nil {
			log.Errorf("Error constructing open crate URL: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		responseCrateGood := janus.CrateGood{
			CrateID:         crateGood.CrateID,
			OpenCrateURL:    openCrateURL,
			ProductIconURL:  crateGood.ProductIconURL,
			ProductIconType: "", // TODO need to get this from an authority
			ProductTitle:    crateGood.ProductTitle,
		}
		responseCrateGoods = append(responseCrateGoods, responseCrateGood)
	}

	response := janus.GetCratesResponse{
		Crates: responseCrateGoods,
	}

	writeResponse(response, w)
}

func (getCrates *GetCratesAPI) constructOpenCrateURL(crateID string, productID string) (string, error) {
	openCrateURL, err := url.Parse(getCrates.RSWOpenCrateEndpoint)
	if err != nil {
		return "", err
	}

	openCrateQuery := openCrateURL.Query()
	openCrateQuery.Add("crateId", crateID)
	openCrateQuery.Add("productId", productID)
	openCrateURL.RawQuery = openCrateQuery.Encode()
	return openCrateURL.String(), nil
}
