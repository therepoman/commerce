package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	janus "code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/models"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func createMockProducts(price string) []models.ADGProduct {
	var products []models.ADGProduct
	products = append(products, models.ADGProduct{
		ASIN:        "asin1",
		Description: "desc1",
		Details: models.Details{
			Developer: models.Developer{
				Name: "developersdevelopersdevelopers",
			},
			ShortDescription: "descriptio",
		},
		Price: models.Price{
			CurrencyUnit: "$",
			Price:        price,
		},
		SKU:   "sku1",
		Title: "item1",
	})
	products = append(products, models.ADGProduct{
		ASIN:        "asin2",
		Description: "desc2",
		Details: models.Details{
			Developer: models.Developer{
				Name: "developersdevelopersdevelopers",
			},
			ShortDescription: "descriptio",
		},
		Price: models.Price{
			CurrencyUnit: "£",
			Price:        price,
		},
		SKU:   "sku2",
		Title: "item2",
	})
	return products
}

func initGetExtensionsProductsMocks() {
	adgDiscoClient.On("GetProductsInDomainByVendorSKU", "vendor-code", "sku", "KR").Return(createMockProducts("KR"), nil)
	adgDiscoClient.On("GetProductsInDomainByVendorSKU", "vendor-code", "sku", "JP").Return(createMockProducts("JP"), nil)
	adgDiscoClient.On("GetProductsInDomainByVendorSKU", "vendor-code", "sku", Anything).Return(createMockProducts("price"), nil)
	adgDiscoClient.On("GetProductsInDomainByVendorSKU", "vendor-code", "error", Anything).Return(createMockProducts("price"), errors.New("unexpected"))
	tailsClient.On("GetLinkedAmazonCOR", "").Return("", errors.New("unexpected"))
	tailsClient.On("GetLinkedAmazonCOR", "linked").Return("JP", nil)
	tailsClient.On("GetLinkedAmazonCOR", Anything).Return("default", nil)
}

func TestGetExtensionsProducts(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initGetExtensionsProductsMocks()

		Convey("Missing vendor code", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "400 Bad Request")
		})

		Convey("Missing sku", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=vendor-code", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "400 Bad Request")
		})

		Convey("Invalid vendor code", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=undefined", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "400 Bad Request")
		})

		Convey("Invalid sku", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=vendor-code&sku=undefined", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "400 Bad Request")
		})

		Convey("ADG error", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=vendor-code&sku=error", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "404 Not Found")
		})

		Convey("Language COR", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=vendor-code&sku=sku&language=ko", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body := janus.GetExtensionsProductsResponse{}
			err = json.NewDecoder(resp.Body).Decode(&body)
			So(err, ShouldBeNil)

			sample := body.Products[0]
			So(sample.Price.Price, ShouldEqual, "KR")
		})

		Convey("User COR", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=vendor-code&sku=sku&userid=linked&language=ko", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body := janus.GetExtensionsProductsResponse{}
			err = json.NewDecoder(resp.Body).Decode(&body)
			So(err, ShouldBeNil)

			sample := body.Products[0]
			So(sample.Price.Price, ShouldEqual, "JP")
		})

		Convey("With valid products", func() {
			req, err := http.NewRequest("GET", server.URL+"/extensions/products?vendor_code=vendor-code&sku=sku", nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body := janus.GetExtensionsProductsResponse{}
			err = json.NewDecoder(resp.Body).Decode(&body)
			So(err, ShouldBeNil)

			So(len(body.Products), ShouldEqual, 2)
			So(body.VendorCode, ShouldEqual, "vendor-code")

			sample := body.Products[0]
			So(sample.ActionDetails.DestinationURL, ShouldContainSubstring, checkoutURL)
			So(sample.ASIN, ShouldNotBeBlank)
			So(sample.Description, ShouldNotBeBlank)
			So(sample.DeveloperName, ShouldNotBeBlank)
			So(sample.Price.CurrencyUnit, ShouldNotBeBlank)
			So(sample.Price.Price, ShouldNotBeBlank)
			So(sample.ShortDescription, ShouldNotBeBlank)
			So(sample.SKU, ShouldNotBeBlank)
			So(sample.Title, ShouldNotBeBlank)
		})
	})
}
