package api

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"

	"code.justin.tv/revenue/moneypenny/client"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	revenueCents    = 1
	expectedRevenue = 0.999
)

// Test GetGameCommerceRevenueAPI GetGameCommerceRevenue and GetExtensionsRevenue call
func TestGetRevenue(t *testing.T) {
	serviceTaxRate := 0.1
	royaltyTaxRate := 0.2

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		metricLogger.On("LogDurationMetric", mock.Anything, mock.Anything).Return()
		metricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()
		gatingHelper.On("UserIsIdWrapper", mock.Anything, mock.Anything).Return(MockWrapper(false, errors.New("test error")))
		gatingHelper.On("IsAdminOrSubAdminWrapper", mock.Anything, mock.Anything).Return(MockWrapper(false, errors.New("test error")))

		taxWithholdingRate := moneypenny.TaxWithholding{
			ServiceTaxWithholdingRate: &serviceTaxRate,
			RoyaltyTaxWithholdingRate: &royaltyTaxRate,
		}
		getWithholdingRateResponse := moneypenny.GetPayoutEntityForChannelResponse{
			PayoutEntityID:     "152152",
			OwnerChannelID:     "Pwner",
			TaxWithholdingRate: taxWithholdingRate,
		}

		revenueResult := make([]*dynamo.BaseRevenue, 1)
		revenueResult[0] = &dynamo.BaseRevenue{
			TwitchUserID: "123456789",
			TimeOfEvent:  time.Now(),
			RevenueCents: revenueCents,
		}

		userID := "1234"

		Convey("for game commerce revenue", func() {
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/%s/gamecommerce/revenue?start_time=2017-01-15&end_time=2017-01-17", server.URL, userID), nil)
			So(err, ShouldBeNil)

			Convey("where CartmanAuthorizer throws an error", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return("", errors.New("test error"))

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("where authz checks error", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", mock.Anything).Return(false, []error{errors.New("test error")})

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("where authz checks return false", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", mock.Anything).Return(false, []error{})

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "403 Forbidden")
			})

			Convey("where authorization and authentication succeeds", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", mock.Anything).Return(true, []error{})

				Convey("when the tuid has record", func() {
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)

					fuelRevenueDao.On("GetFuelRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusOK)

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					parsedResponse := new(janus.GetGameCommerceRevenueResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(len(parsedResponse.Records), ShouldEqual, 1)
					So(parsedResponse.Records[0].RevenueCents, ShouldEqual, expectedRevenue)
				})

				Convey("when the start date is after the end date", func() {
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)

					fuelRevenueDao.On("GetFuelRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)

					req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/1234/gamecommerce/revenue?start_time=2017-01-17&end_time=2017-01-15", server.URL), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
				})

				Convey("when the tuid has record but not completed tax interview", func() {
					taxWithholdingRate = moneypenny.TaxWithholding{
						RoyaltyTaxWithholdingRate: &royaltyTaxRate,
					}
					getWithholdingRateResponse.TaxWithholdingRate = taxWithholdingRate

					fuelRevenueDao.On("GetFuelRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)
					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusOK)

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					parsedResponse := new(janus.GetGameCommerceRevenueResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(len(parsedResponse.Records), ShouldEqual, 0)
				})

				Convey("when the tuid has no records", func() {
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)

					revenueResult := []*dynamo.BaseRevenue{}
					fuelRevenueDao.On("GetFuelRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusOK)

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					parsedResponse := new(janus.GetGameCommerceRevenueResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(len(parsedResponse.Records), ShouldEqual, 0)
				})

				Convey("when GetGameCommerceRevenue throws an error", func() {
					fuelRevenueDao.On("GetFuelRevenue", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)
				})
			})
		})
		Convey("for extensions revenue", func() {
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/%s/extensions/revenue?start_time=2017-01-15&end_time=2017-01-17", server.URL, userID), nil)
			So(err, ShouldBeNil)

			Convey("where CartmanAuthorizer throws an error", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return("", errors.New("test error"))

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("where authz checks error", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", mock.Anything).Return(false, []error{errors.New("test error")})

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "500 Internal Server Error")
			})

			Convey("where authz checks return false", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", mock.Anything).Return(false, []error{})

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.Status, ShouldEqual, "403 Forbidden")
			})

			Convey("where authorization and authentication succeeds", func() {
				cartmanAuthorizer.On("ExtractActingTuid", mock.Anything).Return(userID, nil)
				gatingHelper.On("CheckAuthChain", mock.Anything).Return(true, []error{})

				Convey("when the tuid has record", func() {
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)

					extensionsRevenueDao.On("GetExtensionsRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusOK)

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					parsedResponse := new(janus.GetGameCommerceRevenueResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(len(parsedResponse.Records), ShouldEqual, 1)
					So(parsedResponse.Records[0].RevenueCents, ShouldEqual, expectedRevenue)
				})

				Convey("when the start date is after the end date", func() {
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)

					extensionsRevenueDao.On("GetExtensionsRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)

					req, err := http.NewRequest("GET", fmt.Sprintf("%s/broadcaster/1234/extensions/revenue?start_time=2017-01-17&end_time=2017-01-15", server.URL), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
				})

				Convey("when the tuid has no records", func() {
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)

					revenueResult := []*dynamo.BaseRevenue{}
					extensionsRevenueDao.On("GetExtensionsRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusOK)

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					parsedResponse := new(janus.GetGameCommerceRevenueResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(len(parsedResponse.Records), ShouldEqual, 0)
				})

				Convey("when the tuid has record but not completed tax interview", func() {
					taxWithholdingRate = moneypenny.TaxWithholding{
						RoyaltyTaxWithholdingRate: &royaltyTaxRate,
					}
					getWithholdingRateResponse.TaxWithholdingRate = taxWithholdingRate

					extensionsRevenueDao.On("GetExtensionsRevenue", mock.Anything, mock.Anything, mock.Anything).Return(revenueResult, nil)
					moneyPennyClient.On("GetLivePayoutEntityForChannel", mock.Anything, mock.Anything, mock.Anything).Return(&getWithholdingRateResponse, nil)
					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusOK)

					body, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					parsedResponse := new(janus.GetGameCommerceRevenueResponse)
					err = json.Unmarshal(body, parsedResponse)
					So(err, ShouldBeNil)

					So(len(parsedResponse.Records), ShouldEqual, 0)
				})

				Convey("when GetExtensionsRevenue throws an error", func() {
					extensionsRevenueDao.On("GetExtensionsRevenue", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)
					So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)
				})
			})
		})
	})
}
