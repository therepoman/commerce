package api

import (
	"net/http"

	"golang.org/x/net/context"
)

// This is our healthcheck used by our ELBs to determine if the service is healthy.
func (s *Server) hello(c context.Context, w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		return
	}
}
