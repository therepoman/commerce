package api

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

// GoConvey Test
func TestGetHelloGoConvey(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		Convey("when GET /hello is requested", func() {
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/hello", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)

			Convey("the response has the correct HTTP status code", func() {
				So(resp.Status, ShouldEqual, "200 OK")
			})

			Convey("the reponse body has the correct body", func() {
				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				s := string(body) // Convert response body to string
				So(s, ShouldEqual, "OK")
			})
		})
	})
}
