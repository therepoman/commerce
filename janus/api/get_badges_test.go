package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/common/twitchhttp"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

var testDataJSON = []byte(`
{"badge_sets":
	{
		"warcraft": {
			"versions:": {
				"alliance": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/c4816339-bad4-4645-ae69-d1ab2076a6b0/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/c4816339-bad4-4645-ae69-d1ab2076a6b0/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/c4816339-bad4-4645-ae69-d1ab2076a6b0/3",
					"description":  "For Lordaeron!",
					"title":        "Alliance",
					"click_action": "visit_url",
					"click_url":    "http://warcraftontwitch.tv/"
				},
				"horde": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/de8b26b6-fd28-4e6c-bc89-3d597343800d/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/de8b26b6-fd28-4e6c-bc89-3d597343800d/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/de8b26b6-fd28-4e6c-bc89-3d597343800d/3",
					"description":  "For the Horde!",
					"title":        "Horde",
					"click_action": "visit_url",
					"click_url":    "http://warcraftontwitch.tv/"
				}
			}
		},
		"this-war-of-mine_1": {
			"versions": {
				"1": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/6a20f814-cb2c-414e-89cc-f8dd483e1785/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/6a20f814-cb2c-414e-89cc-f8dd483e1785/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/6a20f814-cb2c-414e-89cc-f8dd483e1785/3",
					"description":  "This War of Mine",
					"title":        "This War of Mine",
					"click_action": "visit_url",
					"click_url":    "https://www.twitch.tv/directory/game/This%20War%20of%20Mine/details"
				}
			}
		},
		"subscriber": {
			"versions": {
				"0": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/19dd8673-124d-4f44-830c-b0f4f9d78635/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/19dd8673-124d-4f44-830c-b0f4f9d78635/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/19dd8673-124d-4f44-830c-b0f4f9d78635/3",
					"description":  "Subscriber",
					"title":        "Subscriber",
					"click_action": "subscribe_to_channel",
					"click_url":    ""
				},
				"1": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/19dd8673-124d-4f44-830c-b0f4f9d78635/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/19dd8673-124d-4f44-830c-b0f4f9d78635/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/19dd8673-124d-4f44-830c-b0f4f9d78635/3",
					"description":  "Subscriber",
					"title":        "Subscriber",
					"click_action": "subscribe_to_channel",
					"click_url":    ""
				}
			}
		},
		"power-rangers": {
			"versions": {
				"0": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/9edf3e7f-62e4-40f5-86ab-7a646b10f1f0/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/9edf3e7f-62e4-40f5-86ab-7a646b10f1f0/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/9edf3e7f-62e4-40f5-86ab-7a646b10f1f0/3",
					"description":  "Black Ranger",
					"title":        "Black Ranger",
					"click_action": "none",
					"click_url":    ""
				},
				"-2001": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/1eeae8fe-5bc6-44ed-9c88-fb84d5e0df52/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/1eeae8fe-5bc6-44ed-9c88-fb84d5e0df52/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/1eeae8fe-5bc6-44ed-9c88-fb84d5e0df52/3",
					"description":  "Blue Ranger",
					"title":        "Blue Ranger",
					"click_action": "none",
					"click_url":    ""
				},
				"54": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/21bbcd6d-1751-4d28-a0c3-0b72453dd823/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/21bbcd6d-1751-4d28-a0c3-0b72453dd823/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/21bbcd6d-1751-4d28-a0c3-0b72453dd823/3",
					"description":  "Green Ranger",
					"title":        "Green Ranger",
					"click_action": "none",
					"click_url":    ""
				},
				"3": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/5c58cb40-9028-4d16-af67-5bc0c18b745e/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/5c58cb40-9028-4d16-af67-5bc0c18b745e/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/5c58cb40-9028-4d16-af67-5bc0c18b745e/3",
					"description":  "Pink Ranger",
					"title":        "Pink Ranger",
					"click_action": "none",
					"click_url":    ""
				},
				"fortify": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/8843d2de-049f-47d5-9794-b6517903db61/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/8843d2de-049f-47d5-9794-b6517903db61/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/8843d2de-049f-47d5-9794-b6517903db61/3",
					"description":  "Red Ranger",
					"title":        "Red Ranger",
					"click_action": "none",
					"click_url":    ""
				},
				"5": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/06c85e34-477e-4939-9537-fd9978976042/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/06c85e34-477e-4939-9537-fd9978976042/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/06c85e34-477e-4939-9537-fd9978976042/3",
					"description":  "White Ranger",
					"title":        "White Ranger",
					"click_action": "none",
					"click_url":    ""
				},
				"6": {
					"image_url_1x": "https://static-cdn.jtvnw.net/badges/v1/d6dca630-1ca4-48de-94e7-55ed0a24d8d1/1",
					"image_url_2x": "https://static-cdn.jtvnw.net/badges/v1/d6dca630-1ca4-48de-94e7-55ed0a24d8d1/2",
					"image_url_4x": "https://static-cdn.jtvnw.net/badges/v1/d6dca630-1ca4-48de-94e7-55ed0a24d8d1/3",
					"description":  "Yellow Ranger",
					"title":        "Yellow Ranger",
					"click_action": "none",
					"click_url":    ""
				}
			}
		}
	}
}`)

const (
	hasThreeBadgesTuid = "33331234"
	hasTwoBadgesTuid   = "22221234"
	hasOneBadgesTuid   = "11111234"
	hasZeroBadgesTuid  = "00001234"

	badgeWithManyVersions = "power-rangers"
	badgeWithTwoVersions  = "subscriber"
	badgeWithOneVersion   = "this-war-of-mine_1"
	// this is a badge that should be ignored because it does not have any versions that are numeric
	badgeWithAlphaVersion = "warcraft"
)

var (
	// these are mocked service responses, note these include `badgeWithAlphaVersion` which we want to be filtered out
	badgeEntitlementsForThreeBadgesTuid = []string{badgeWithOneVersion, badgeWithTwoVersions, badgeWithManyVersions}
	badgeEntitlementsForTwoBadgesTuid   = []string{badgeWithOneVersion, badgeWithTwoVersions, badgeWithAlphaVersion}
	badgeEntitlementsForOneBadgesTuid   = []string{badgeWithManyVersions}
	badgeEntitlementsForZeroBadgesTuid  = []string{badgeWithAlphaVersion}

	// these are expected test results, note these exclude `badgeWithAlphaVersion`
	fuelBadgeEntitlementsForThreeBadgesTuid = []string{badgeWithOneVersion, badgeWithTwoVersions, badgeWithManyVersions}
	fuelBadgeEntitlementsForTwoBadgesTuid   = []string{badgeWithOneVersion, badgeWithTwoVersions}
	fuelBadgeEntitlementsForOneBadgesTuid   = []string{badgeWithManyVersions}
	fuelBadgeEntitlementsForZeroBadgesTuid  = []string{}
)

var makoReponseForTuidWithThreeBadgeEntitlements = &models.MakoEntitlementSets{Keys: badgeEntitlementsForThreeBadgesTuid}
var makoReponseForTuidWithTwoBadgeEntitlements = &models.MakoEntitlementSets{Keys: badgeEntitlementsForTwoBadgesTuid}
var makoReponseForTuidWithOneBadgeEntitlements = &models.MakoEntitlementSets{Keys: badgeEntitlementsForOneBadgesTuid}
var makoReponseForTuidWithZeroBadgeEntitlements = &models.MakoEntitlementSets{Keys: badgeEntitlementsForZeroBadgesTuid}

func initBadgesMocks() {
	metricLogger.On("LogDurationMetric", Anything, Anything).Return()
	makoClient.On("GetBadgeSets", Anything, hasThreeBadgesTuid).Return(makoReponseForTuidWithThreeBadgeEntitlements, nil)
	makoClient.On("GetBadgeSets", Anything, hasTwoBadgesTuid).Return(makoReponseForTuidWithTwoBadgeEntitlements, nil)
	makoClient.On("GetBadgeSets", Anything, hasOneBadgesTuid).Return(makoReponseForTuidWithOneBadgeEntitlements, nil)
	makoClient.On("GetBadgeSets", Anything, hasZeroBadgesTuid).Return(makoReponseForTuidWithZeroBadgeEntitlements, nil)

	chatGlobalBadgesResponse := &models.GetBadgeDisplay{}
	err := json.Unmarshal(testDataJSON, chatGlobalBadgesResponse)
	if err != nil {
		panic(err)
	}
	badgesClient.On("GetBadgeDisplay", Anything).Return(chatGlobalBadgesResponse, nil)
}

// Test GetAllBadgess with valid input
func TestGetBadges(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initBadgesMocks()

		var tuid string
		Convey("when the tuid has three badges", func() {
			tuid = hasThreeBadgesTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/badges", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBadgesResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.BadgeSets), ShouldEqual, 3)
			for _, expected := range fuelBadgeEntitlementsForThreeBadgesTuid {
				So(len(parsedResponse.BadgeSets[expected].Versions), ShouldEqual, 1)
			}
			So(parsedResponse.BadgeSets[badgeWithTwoVersions].Versions["54"], ShouldNotBeNil)
			So(parsedResponse.BadgeSets[badgeWithTwoVersions].Versions["1"], ShouldNotBeNil)
			So(parsedResponse.BadgeSets[badgeWithOneVersion].Versions["1"], ShouldNotBeNil)
		})

		Convey("when the tuid has two badges", func() {
			tuid = hasTwoBadgesTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/badges", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBadgesResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.BadgeSets), ShouldEqual, 2)
			for _, expected := range fuelBadgeEntitlementsForTwoBadgesTuid {
				So(len(parsedResponse.BadgeSets[expected].Versions), ShouldEqual, 1)
			}
			So(parsedResponse.BadgeSets[badgeWithOneVersion].Versions["1"], ShouldNotBeNil)
			So(parsedResponse.BadgeSets[badgeWithTwoVersions].Versions["1"], ShouldNotBeNil)
		})

		Convey("when the tuid has one badges", func() {
			tuid = hasOneBadgesTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/badges", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBadgesResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.BadgeSets), ShouldEqual, 1)
			for _, expected := range fuelBadgeEntitlementsForOneBadgesTuid {
				So(len(parsedResponse.BadgeSets[expected].Versions), ShouldEqual, 1)
			}
			So(parsedResponse.BadgeSets[badgeWithManyVersions].Versions["54"], ShouldNotBeNil)
		})

		Convey("when the tuid has zero badges", func() {
			tuid = hasZeroBadgesTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/badges", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, "200 OK")

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBadgesResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldBeNil)

			So(len(parsedResponse.BadgeSets), ShouldEqual, 0)
		})

		Convey("when the current tuid doesn't match the requested tuid", func() {
			tuid := hasOneBadgesTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(false, nil)

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/badges", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusForbidden)

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBadgesResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldNotBeNil)

			So(len(parsedResponse.BadgeSets), ShouldEqual, 0)
		})

		Convey("when auth call fails", func() {
			tuid := hasOneBadgesTuid
			gatingHelper.On("UserIsId", Anything, tuid).Return(true, twitchhttp.Error{StatusCode: 500})

			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/%s/badges", server.URL, tuid), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)

			body, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			parsedResponse := new(janus.GetBadgesResponse)
			err = json.Unmarshal(body, parsedResponse)
			So(err, ShouldNotBeNil)

			So(len(parsedResponse.BadgeSets), ShouldEqual, 0)
		})
	})
}
