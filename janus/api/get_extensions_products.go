package api

import (
	"encoding/json"
	"net/http"
	"net/url"

	janus "code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"

	log "github.com/sirupsen/logrus"

	"golang.org/x/net/context"
	"golang.org/x/text/language"
)

// ExtensionsProductsAPI implements the API handler to get ASINs for Extensions products
type ExtensionsProductsAPI struct {
	ADGDiscoClient            clients.IADGDiscoClientWrapper
	CheckoutDestinationURL    string
	SupportedLanguagesMatcher language.Matcher
	TailsClient               clients.ITailsClientWrapper
}

func (extensionsProductsAPI *ExtensionsProductsAPI) getExtensionsProducts(c context.Context, w http.ResponseWriter, r *http.Request) {
	vendorCode := r.Form.Get("vendor_code")
	sku := r.Form.Get("sku")
	userID := r.Form.Get("userid")
	languageParam := r.Form.Get("language")

	// Make sure vendor code and sku are valid: not blank and not "undefined"
	if vendorCode == "" || vendorCode == "undefined" {
		http.Error(w, "missing vendor_code param", http.StatusBadRequest)
		return
	}
	if sku == "" || sku == "undefined" {
		http.Error(w, "missing sku param", http.StatusBadRequest)
		return
	}

	// Get twitch supported locale or default locale
	language, _, _ := extensionsProductsAPI.SupportedLanguagesMatcher.Match(language.Make(languageParam))

	// Get digital products from ADG
	extensionProducts, err := extensionsProductsAPI.fetchExtensionProduct(vendorCode, sku, userID, language.String())
	if err != nil {
		log.WithFields(log.Fields{
			"Vendor Code": vendorCode,
			"SKU":         sku,
			"UserId":      userID,
			"Language":    language,
			"error":       err,
		}).Error("Error fetching extension products")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	response := janus.GetExtensionsProductsResponse{
		VendorCode: vendorCode,
		Products:   extensionProducts,
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Errorf("Failed to marshall the getExtensionsProducts response: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (extensionsProductsAPI *ExtensionsProductsAPI) fetchExtensionProduct(vendorCode, sku, userID, language string) ([]janus.ExtensionProduct, error) {
	// Infer COR based on provided language code
	cor := clients.LanguageToCOR(language)
	amazonCOR, err := extensionsProductsAPI.fetchAmazonCOR(userID)
	if err != nil {
		log.Infof("Could not get linked Amazon account for Twitch user: %s. UserID: %s", err, userID)
	} else {
		// If the Twitch user has a linked Amazon account, overwrite the inferred COR
		cor = amazonCOR
	}

	products, err := extensionsProductsAPI.ADGDiscoClient.GetProductsInDomainByVendorSKU(vendorCode, sku, cor)
	if err != nil {
		return nil, err
	}

	extensionsProducts := []janus.ExtensionProduct{}
	for _, prod := range products {
		extensionProduct := janus.ExtensionProduct{
			ActionDetails: &janus.ActionDetails{
				DestinationURL: extensionsProductsAPI.buildDestinationURL(prod.ASIN),
			},
			ASIN:          prod.ASIN,
			Description:   prod.Description,
			DeveloperName: prod.Details.Developer.Name,
			Price: janus.Price{
				CurrencyUnit: prod.Price.CurrencyUnit,
				Price:        prod.Price.Price,
			},
			ShortDescription: prod.Details.ShortDescription,
			SKU:              prod.SKU,
			Title:            prod.Title,
		}
		extensionsProducts = append(extensionsProducts, extensionProduct)
	}

	return extensionsProducts, nil
}

func (extensionsProductsAPI *ExtensionsProductsAPI) fetchAmazonCOR(userID string) (string, error) {
	return extensionsProductsAPI.TailsClient.GetLinkedAmazonCOR(userID)
}

func (extensionsProductsAPI *ExtensionsProductsAPI) buildDestinationURL(asin string) string {
	query := url.Values{}
	query.Set("asin", asin)
	return extensionsProductsAPI.CheckoutDestinationURL + query.Encode()
}
