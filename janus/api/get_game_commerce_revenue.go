package api

import (
	"errors"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"fmt"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/revenue/moneypenny/client"
	"goji.io/pat"
	"golang.org/x/net/context"
)

const (
	layout = "2006-01-02"
)

// GameCommerceRevenueAPI represents the API to get GameCommerce Revenue
type GameCommerceRevenueAPI struct {
	CartmanAuthorizer    auth.ICartmanAuthorizer
	FuelRevenueDAO       dynamo.IFuelRevenueDAO
	ExtensionsRevenueDAO dynamo.IExtensionsRevenueDAO
	MoneyPennyClient     moneypenny.Client
	GatingHelper         auth.IGatingHelper
	MetricLogger         metrics.IMetricLogger
}

// GetGameCommerceRevenueParams represents the input params to this endpoint
type GetGameCommerceRevenueParams struct {
	ChannelID string
	StartDate time.Time
	EndDate   time.Time
}

// GetRevenueFunction represents the function that retrieves the revenue
type GetRevenueFunction func(params *GetGameCommerceRevenueParams) ([]*dynamo.BaseRevenue, error)

// GetGameCommerceRevenue retrieves the revenue for game commerce
func (gameCommerceRevenueAPI *GameCommerceRevenueAPI) getGameCommerceRevenue(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameCommerceRevenueAPI.getRevenue(c, w, r, func(params *GetGameCommerceRevenueParams) ([]*dynamo.BaseRevenue, error) {
		return gameCommerceRevenueAPI.FuelRevenueDAO.GetFuelRevenue(params.ChannelID, params.StartDate, params.EndDate)
	})
}

// GetExtensionsRevenue retrieves the revenue for extensions
func (gameCommerceRevenueAPI *GameCommerceRevenueAPI) getExtensionsRevenue(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameCommerceRevenueAPI.getRevenue(c, w, r, func(params *GetGameCommerceRevenueParams) ([]*dynamo.BaseRevenue, error) {
		return gameCommerceRevenueAPI.ExtensionsRevenueDAO.GetExtensionsRevenue(params.ChannelID, params.StartDate, params.EndDate)
	})
}

// GetRevenue retrieves the revenue
func (gameCommerceRevenueAPI *GameCommerceRevenueAPI) getRevenue(c context.Context, w http.ResponseWriter, r *http.Request, getRevenueFunction GetRevenueFunction) {
	tuid := pat.Param(c, "tuid")

	actingTuid, err := gameCommerceRevenueAPI.CartmanAuthorizer.ExtractActingTuid(r)
	if err != nil {
		log.Errorf("Error determining acting user: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	authChain := []auth.AuthChainFunction{
		gameCommerceRevenueAPI.GatingHelper.UserIsIdWrapper(r, tuid),
		gameCommerceRevenueAPI.GatingHelper.IsAdminOrSubAdminWrapper(r, actingTuid),
	}

	allow, errs := gameCommerceRevenueAPI.GatingHelper.CheckAuthChain(authChain)
	if !allow {
		if len(errs) != 0 {
			errorMessage := fmt.Sprintf("Error authorizing getRevenue: %s", errs)
			log.Error(errorMessage)
			http.Error(w, errorMessage, http.StatusInternalServerError)
			return
		} else {
			log.Warnf("Not authorized to getRevenue for Twitch user %s", tuid)
			http.Error(w, "400 Bad Request", http.StatusForbidden)
			return
		}
	}

	params, err := processGetGameCommerceRevenueParams(c, r)
	if err != nil {
		log.Errorf("Error processing request parameters: %s", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	result, err := getRevenueFunction(params)
	if err != nil {
		log.Errorf("Error retrieving revenue data from DAO: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	startTime := time.Now()
	resp, err := gameCommerceRevenueAPI.MoneyPennyClient.GetLivePayoutEntityForChannel(context.Background(), params.ChannelID, nil)
	gameCommerceRevenueAPI.MetricLogger.LogDurationSinceMetric("moneyPennyClient.getLivePayoutEntityForChannel", startTime)

	if err != nil {
		log.Errorf("Error retrieving tax withholding rate: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var gcRevenueResponse janus.GetGameCommerceRevenueResponse
	if resp.TaxWithholdingRate.ServiceTaxWithholdingRate != nil {
		revenueRecords, err := convertRecords(result, *resp.TaxWithholdingRate.ServiceTaxWithholdingRate)
		if err != nil {
			log.Errorf("Failed to group broadcaster %s revenue", params.ChannelID)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		gcRevenueResponse = janus.GetGameCommerceRevenueResponse{
			Records: revenueRecords,
		}
	}

	writeResponse(gcRevenueResponse, w)
}

func convertRecords(dynamoRecords []*dynamo.BaseRevenue, taxRate float64) ([]janus.GameCommerceRevenueRecord, error) {
	var result []janus.GameCommerceRevenueRecord

	for _, dr := range dynamoRecords {
		record := janus.GameCommerceRevenueRecord{
			TimeOfEvent:  dr.TimeOfEvent,
			RevenueCents: calculateRevenue(dr.RevenueCents, taxRate),
		}
		result = append(result, record)
	}

	return result, nil
}

func calculateRevenue(revenueCents int64, taxRate float64) float64 {
	revenueBeforeTax := float64(revenueCents)
	revenueAfterTax := revenueBeforeTax - (revenueBeforeTax * taxRate / 100)
	return revenueAfterTax
}

func processGetGameCommerceRevenueParams(c context.Context, r *http.Request) (*GetGameCommerceRevenueParams, error) {
	tuid := pat.Param(c, "tuid")
	if tuid == "" {
		log.Errorf("Missing channel_id parameter")
		return nil, errors.New("Missing channel_parameter")
	}

	start := r.URL.Query().Get("start_time")
	if start == "" {
		log.Errorf("Missing start_time parameter")
		return nil, errors.New("Missing start_time paramter")
	}

	end := r.URL.Query().Get("end_time")
	if start == "" {
		log.Errorf("Missing end_time parameter")
		return nil, errors.New("Missing end_time paramter")
	}

	startDateUnix, err := time.Parse(layout, start)
	if err != nil {
		log.Errorf("Invalid start date parameter: %s", start)
		return nil, err
	}

	endDateUnix, err := time.Parse(layout, end)
	if err != nil {
		log.Errorf("Invalid end date parameter: %s", end)
		return nil, err
	}

	if startDateUnix.After(endDateUnix) {
		msg := fmt.Sprintf("End date parameter: %s is before start date parameter: %s", end, start)
		log.Warn(msg)
		return nil, errors.New(msg)
	}

	return &GetGameCommerceRevenueParams{
		ChannelID: tuid,
		StartDate: startDateUnix,
		EndDate:   endDateUnix,
	}, nil
}
