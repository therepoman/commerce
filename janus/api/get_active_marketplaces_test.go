package api

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"

	"encoding/json"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func MockActiveMarketplace(tuid string, marketplaceID string) *dynamo.ActiveMarketplace {
	return &dynamo.ActiveMarketplace{
		Tuid:          tuid,
		MarketplaceID: marketplaceID,
	}
}

func TestGetActiveMarketplacesGoConvey(t *testing.T) {
	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		tuid := "1234"

		server := httptest.NewServer(api)
		defer server.Close()

		metricLogger.On("LogDurationMetric", mock.Anything, mock.Anything).Return()

		Convey("where cartman auth fails", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(false, errors.New("test error"))
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, 403)
		})

		Convey("where cartman token does not match queried user", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(false, nil)
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
			So(err, ShouldBeNil)

			resp, err := http.DefaultClient.Do(req)
			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, 403)
		})

		Convey("where authorization succeeds", func() {
			gatingHelper.On("UserIsId", mock.Anything, mock.Anything).Return(true, nil)
			Convey("when GET /inventory/1234/active_marketplaces is requested correct results", func() {
				mockedGetMarketplacesResponse :=
					[]*dynamo.ActiveMarketplace{
						MockActiveMarketplace(tuid, "ATVPDKIKX0DER"),
						MockActiveMarketplace(tuid, "A2EUQ1WTGCTBG2"),
					}
				activeMarketplacesDAO.On("GetMarketplaces", tuid).Return(mockedGetMarketplacesResponse, nil)

				Convey("staff check failed", func() {
					gatingHelper.On("IsAdminOrSubAdmin", mock.Anything, mock.Anything).Return(false, errors.New("test error"))

					req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)

					Convey("the response has the correct HTTP status code", func() {
						So(resp.Status, ShouldEqual, "200 OK")
					})

					Convey("the reponse body has the correct body non staff", func() {

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetActiveMarketplacesResponse)
						err = json.Unmarshal(body, parsedResponse)

						So(len(parsedResponse.ActiveMarketplaces), ShouldEqual, 2)
						for _, activeMarketplace := range parsedResponse.ActiveMarketplaces {
							So(activeMarketplace.DisplayText, ShouldNotBeEmpty)
							So(activeMarketplace.OrderHistoryRedirectURL, ShouldNotBeEmpty)
						}
					})
				})

				Convey("staff check success", func() {
					gatingHelper.On("IsAdminOrSubAdmin", mock.Anything, mock.Anything).Return(true, nil)

					req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)

					Convey("the response has the correct HTTP status code", func() {
						So(resp.Status, ShouldEqual, "200 OK")
					})

					Convey("the reponse body has the correct body staff", func() {

						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetActiveMarketplacesResponse)
						err = json.Unmarshal(body, parsedResponse)

						So(len(parsedResponse.ActiveMarketplaces), ShouldEqual, 3)
						for _, activeMarketplace := range parsedResponse.ActiveMarketplaces {
							So(activeMarketplace.DisplayText, ShouldNotBeEmpty)
							So(activeMarketplace.OrderHistoryRedirectURL, ShouldNotBeEmpty)
						}
					})
				})
			})

			Convey("when GET /inventory/1234/active_marketplaces is requested with no shopping activity defaults to US marketplace ", func() {
				mockedGetMarketplacesResponse := []*dynamo.ActiveMarketplace{}
				activeMarketplacesDAO.On("GetMarketplaces", tuid).Return(mockedGetMarketplacesResponse, nil)

				Convey("staff check failed", func() {
					gatingHelper.On("IsAdminOrSubAdmin", mock.Anything, mock.Anything).Return(false, errors.New("test error"))
					req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)

					Convey("the response has the correct HTTP status code", func() {
						So(resp.Status, ShouldEqual, "200 OK")
					})

					Convey("the reponse body has the correct body non staff", func() {
						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetActiveMarketplacesResponse)
						err = json.Unmarshal(body, parsedResponse)

						So(len(parsedResponse.ActiveMarketplaces), ShouldEqual, 1)
						So(parsedResponse.ActiveMarketplaces[0].DisplayText, ShouldEqual, marketplaceToDomainMap[DefaultActiveMarketplace])
					})
				})

				Convey("staff check success", func() {
					gatingHelper.On("IsAdminOrSubAdmin", mock.Anything, mock.Anything).Return(true, nil)
					req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
					So(err, ShouldBeNil)

					resp, err := http.DefaultClient.Do(req)
					So(err, ShouldBeNil)

					Convey("the response has the correct HTTP status code", func() {
						So(resp.Status, ShouldEqual, "200 OK")
					})

					Convey("the reponse body has the correct body staff", func() {
						body, err := ioutil.ReadAll(resp.Body)
						So(err, ShouldBeNil)

						parsedResponse := new(janus.GetActiveMarketplacesResponse)
						err = json.Unmarshal(body, parsedResponse)

						So(len(parsedResponse.ActiveMarketplaces), ShouldEqual, 2)
						So(parsedResponse.ActiveMarketplaces[0].DisplayText, ShouldEqual, marketplaceToDomainMap[DefaultActiveMarketplace])
						So(parsedResponse.ActiveMarketplaces[1].DisplayText, ShouldEqual, PaypalDisplayText)
					})
				})
			})

			Convey("when DAO throws an error", func() {
				activeMarketplacesDAO.On("GetMarketplaces", tuid).Return(nil, errors.New("test error"))

				req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)
			})

			Convey("when the tuid is missing", func() {
				activeMarketplacesDAO.On("GetMarketplaces", tuid).Return(nil, errors.New("test error"))

				req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory//active_marketplaces", server.URL), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, http.StatusNotFound)
			})

			Convey("when the returned marketplace is unrecognized", func() {
				mockedGetMarketplacesResponse :=
					[]*dynamo.ActiveMarketplace{
						MockActiveMarketplace(tuid, "InvalidMarketplaceID"),
					}
				activeMarketplacesDAO.On("GetMarketplaces", tuid).Return(mockedGetMarketplacesResponse, nil)

				req, err := http.NewRequest("GET", fmt.Sprintf("%s/inventory/1234/active_marketplaces", server.URL), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, http.StatusInternalServerError)
			})
		})
	})
}
