package api

import (
	"net/http"
	"net/url"
	"path"
	"sort"
	"strconv"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/dynamo"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"golang.org/x/text/language"
)

const (
	amazonDetailDirectory = "https://www.amazon.com/dp"
	merchCurrencyDefault  = "USD"
)

// GetMerchandiseAPI represents the API to get all merchandise info
type GetMerchandiseAPI struct {
	ADGDiscoClient                clients.IADGDiscoClientWrapper
	MerchandiseMetadataDAO        dynamo.IMerchandiseMetadataDAO
	MerchandiseLocalizationDAO    dynamo.IMerchandiseLocalizationDAO
	SupportedLanguagesMatcher     language.Matcher
	MerchandiseLifestyleImagesDAO dynamo.IMerchandiseLifestyleImagesDAO
}

// GetMerchandise returns info on all of the twitch merchandise products
func (merchandiseAPI *GetMerchandiseAPI) getMerchandise(c context.Context, w http.ResponseWriter, r *http.Request) {
	// locale is optional
	localeParam := r.Form.Get("locale")

	// Get twitch supported locale or default locale
	localeTag, _, _ := merchandiseAPI.SupportedLanguagesMatcher.Match(language.Make(localeParam))

	// Get merchandise data from dynamo
	merchandiseMap, err := merchandiseAPI.getMerchandiseMetadata()
	if err != nil {
		log.Errorf("Error receiving twitch merchandise metadata: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Get localized merchandise titles from dynamo
	merchandiseMap, err = merchandiseAPI.populateLocalizedTitles(merchandiseMap, localeTag)
	if err != nil {
		log.Errorf("Error receiving localized titles: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Convert map to list
	merchandiseList := []janus.Product{}
	for _, merchandiseProduct := range merchandiseMap {
		// Scrub original price if item does not have an on sale offer
		if merchandiseProduct.OriginalPrice.Price != "" && merchandiseProduct.Price.Price != "" {
			originalPriceAmount, err := strconv.ParseFloat(merchandiseProduct.OriginalPrice.Price, 64)
			if err != nil {
				log.Errorf("Error parsing originalPrice as float: %s", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			currentPriceAmount, err := strconv.ParseFloat(merchandiseProduct.Price.Price, 64)
			if err != nil {
				log.Errorf("Error parsing Price as float: %s", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			if originalPriceAmount <= currentPriceAmount {
				merchandiseProduct.OriginalPrice = janus.Price{}
			}
		}

		merchandiseList = append(merchandiseList, *merchandiseProduct)
	}

	// Get lifestyle images from dynamo and model them as products
	imageList, err := merchandiseAPI.getLifestyleImages()
	if err != nil {
		log.Errorf("Error receiving lifestyle images: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Combine lifestyle images and products into single response
	merchandiseList = append(merchandiseList, imageList...)

	sort.Sort(ByRank(merchandiseList))

	response := janus.GetMerchandiseResponse{
		Products: merchandiseList,
	}

	writeResponse(response, w)
}

// getMerchandiseMetadata returns the metadata for merchandise stored in dynamo
func (merchandiseAPI *GetMerchandiseAPI) getMerchandiseMetadata() (map[string]*janus.Product, error) {
	results, err := merchandiseAPI.MerchandiseMetadataDAO.Scan()
	if err != nil {
		log.Errorf("Error retrieving merch metadata from DAO: %s", err)
		return nil, err
	}
	merchProducts := map[string]*janus.Product{}
	for _, result := range results {
		// Add product image info from dynamo
		images := []janus.Image{}
		images = append(images, parseImage(result.ProductImageURL, int(result.ProductImageHeight), int(result.ProductImageWidth)))

		product := janus.Product{
			ASIN:          result.Asin,
			Price:         formatPriceFromFloat(result.FallbackPrice, merchCurrencyDefault),
			OriginalPrice: formatPriceFromFloat(result.OriginalPrice, merchCurrencyDefault),
			IsFeatured:    result.IsFeatured,
			IsNew:         result.IsNew,
			IsOutOfStock:  result.IsOutOfStock,
			Rank:          result.Rank,
			Media:         parseMerchandiseMedia(images),
			DetailPageURL: buildAmazonDetailsURL(result.Asin),
		}
		merchProducts[result.Asin] = &product
	}

	return merchProducts, nil
}

// getLifestyleImages returns the image data associated with lifestyle images to be displayed on the twitch merch page
func (merchandiseAPI *GetMerchandiseAPI) getLifestyleImages() ([]janus.Product, error) {
	results, err := merchandiseAPI.MerchandiseLifestyleImagesDAO.Scan()
	if err != nil {
		log.Errorf("Error retrieving merch lifestyle images from DAO: %s", err)
		return nil, err
	}
	lifestyleProducts := []janus.Product{}
	for _, result := range results {
		// Add product image info from dynamo
		images := []janus.Image{}
		images = append(images, parseImage(result.URL, int(result.Height), int(result.Width)))

		product := janus.Product{
			Rank:  result.Rank,
			Media: parseMerchandiseMedia(images),
		}
		lifestyleProducts = append(lifestyleProducts, product)
	}
	return lifestyleProducts, nil
}

// populateADGMerchandiseInfo takes a map of products keyed by asin and calls ADG to get and return a map updated with ADG info
func (merchandiseAPI *GetMerchandiseAPI) populateADGMerchandiseInfo(merchandiseMap map[string]*janus.Product, userID string) (map[string]*janus.Product, error) {
	asinList := []string{}
	for asin, _ := range merchandiseMap {
		asinList = append(asinList, asin)
	}

	adgProducts, err := merchandiseAPI.ADGDiscoClient.GetMerchandiseDetails(asinList, userID)
	if err != nil {
		return nil, err
	}

	for _, adgProduct := range adgProducts {
		product, exists := merchandiseMap[adgProduct.ASIN]
		if !exists {
			continue
		}
		if adgProduct.Price.Price != "" {
			product.Price = janus.Price{
				Price:        adgProduct.Price.Price,
				CurrencyUnit: adgProduct.Price.CurrencyUnit,
			}
		}

	}

	return merchandiseMap, nil
}

// populateLocalizedTitles takes a map of products keyed by asin and a locale and returns a map populated with the localized titles
func (merchandiseAPI *GetMerchandiseAPI) populateLocalizedTitles(merchandiseMap map[string]*janus.Product, locale language.Tag) (map[string]*janus.Product, error) {
	asinList := []string{}
	for asin, _ := range merchandiseMap {
		asinList = append(asinList, asin)
	}

	localizedTitles, err := merchandiseAPI.MerchandiseLocalizationDAO.BatchGet(asinList, locale.String())
	if err != nil {
		log.Errorf("Error retrieving merch localized strings from DAO: %s", err)
		return nil, err
	}

	for _, localizedTitle := range localizedTitles {
		if localizedTitle == nil {
			continue
		}

		merchandiseMap[localizedTitle.Asin].Title = localizedTitle.Title
	}

	return merchandiseMap, nil
}

func parseMerchandiseMedia(images []janus.Image) janus.Media {
	return janus.Media{
		Images: images,
	}
}

func parseImage(imageURL string, height int, width int) janus.Image {
	return janus.Image{
		ImageURL: imageURL,
		Height:   height,
		Width:    width,
	}
}

func formatPriceFromFloat(amount float64, currencyUnit string) janus.Price {
	// Return an empty price instead if amount calculates to 0.00
	if amount == 0 {
		return janus.Price{}
	}

	return janus.Price{
		Price:        strconv.FormatFloat(amount, 'f', 2, 64),
		CurrencyUnit: currencyUnit,
	}
}

func buildAmazonDetailsURL(asin string) string {
	url, err := url.Parse(amazonDetailDirectory)
	if err != nil {
		return ""
	}
	url.Path = path.Join(url.Path, asin)
	return url.String()
}

// ByRank and associated methods are used to sort merchandise products by rank
type ByRank []janus.Product

func (a ByRank) Len() int           { return len(a) }
func (a ByRank) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByRank) Less(i, j int) bool { return a[i].Rank < a[j].Rank }
