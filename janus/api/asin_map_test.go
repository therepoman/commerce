package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/metadata"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	liveGame       = "livegame"
	liveAsin       = "liveasin"
	suppressedGame = "suppressedgame"
	suppressedAsin = "suppressedasin"
	unmappedGame   = "unmappedgame"
	errorGame      = "errorgame"
)

func initGetGameAsinMocks() {
	metricLogger.On("LogDurationMetric", Anything, Anything).Return()

	asinMap.On("Get", liveGame).Return(&janus.GameAsinMap{
		Asin:      liveAsin,
		GameTitle: liveGame,
		Status:    metadata.StatusLive,
	}, nil)

	asinMap.On("Get", suppressedGame).Return(nil, nil)

	asinMap.On("Get", unmappedGame).Return(nil, nil)

	asinMap.On("Get", errorGame).Return(nil, fmt.Errorf("test error"))
}

func TestGetGameAsin(t *testing.T) {

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initGetGameAsinMocks()

		Convey("when GET /games/<gameID>/asin requested", func() {
			Convey("with a live game", func() {
				gameTitle := liveGame
				req, err := http.NewRequest("GET", fmt.Sprintf("%s/games/%s/asin", server.URL, gameTitle), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GameAsinMap)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.Asin, ShouldEqual, liveAsin)
			})
			Convey("with an all caps live game", func() {
				gameTitle := "LIVEGAME"
				req, err := http.NewRequest("GET", fmt.Sprintf("%s/games/%s/asin", server.URL, gameTitle), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GameAsinMap)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.Asin, ShouldEqual, liveAsin)
			})
			Convey("with a suppressed game", func() {
				gameTitle := suppressedGame
				req, err := http.NewRequest("GET", fmt.Sprintf("%s/games/%s/asin", server.URL, gameTitle), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 404)
			})
			Convey("with an unmapped game", func() {
				gameTitle := unmappedGame
				req, err := http.NewRequest("GET", fmt.Sprintf("%s/games/%s/asin", server.URL, gameTitle), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 404)
			})
			Convey("with a failed dynamo call", func() {
				gameTitle := errorGame
				req, err := http.NewRequest("GET", fmt.Sprintf("%s/games/%s/asin", server.URL, gameTitle), nil)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 500)
			})
		})
	})
}

func TestSetGameAsin(t *testing.T) {
	goodAsin := "goodasin"
	goodTitle := "goodtitle"

	Convey("With a running API", t, func() {
		api, err := initAPI()
		So(err, ShouldBeNil)

		server := httptest.NewServer(api)
		defer server.Close()

		initGetGameAsinMocks()

		Convey("when PUT /games/<gameID>/asin requested", func() {
			Convey("with a valid request", func() {
				asinMap.On("Put", Anything).Return(nil)

				request := generateSetGameAsinRequest(goodAsin, goodTitle, metadata.StatusSuppressed)
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GameAsinMap)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.Asin, ShouldEqual, goodAsin)
			})
			Convey("with a valid request with caps", func() {
				asinMap.On("Put", Anything).Return(nil)

				request := generateSetGameAsinRequest(goodAsin, "GOODTItle", metadata.StatusLive)
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GameAsinMap)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.Asin, ShouldEqual, goodAsin)
				So(parsedResponse.GameTitle, ShouldEqual, goodTitle)
			})
			Convey("with a valid request with whitespace in the asin", func() {
				asinMap.On("Put", Anything).Return(nil)

				request := generateSetGameAsinRequest(" goodasin ", goodTitle, metadata.StatusLive)
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GameAsinMap)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.Asin, ShouldEqual, goodAsin)
				So(parsedResponse.GameTitle, ShouldEqual, goodTitle)
			})
			Convey("with an invalid request body", func() {
				asinMap.On("Put", Anything).Return(nil)

				reader := bytes.NewReader([]byte("badrequest"))

				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), reader)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})
			Convey("with a missing asin", func() {
				asinMap.On("Put", Anything).Return(nil)

				asin := ""
				request := generateSetGameAsinRequest(asin, goodTitle, "SUPPRESSED")
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})
			Convey("with a missing status", func() {
				asinMap.On("Put", Anything).Return(nil)

				request := generateSetGameAsinRequest(goodAsin, goodTitle, "")
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 200)

				body, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				parsedResponse := new(janus.GameAsinMap)
				err = json.Unmarshal(body, parsedResponse)
				So(err, ShouldBeNil)

				So(parsedResponse.Asin, ShouldEqual, goodAsin)
				So(parsedResponse.Status, ShouldEqual, metadata.StatusLive)
			})
			Convey("with an invalid status", func() {
				asinMap.On("Put", Anything).Return(nil)

				request := generateSetGameAsinRequest(goodAsin, goodTitle, "badstatus")
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 400)
			})
			Convey("with a failed dynamo call", func() {
				asinMap.On("Put", Anything).Return(fmt.Errorf("test error"))

				request := generateSetGameAsinRequest(goodAsin, goodTitle, "LIVE")
				req, err := http.NewRequest("PUT", fmt.Sprintf("%s/games/%s/asin", server.URL, goodTitle), request)
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, 500)
			})
		})
	})
}

func TestIsValidStatus(t *testing.T) {
	Convey("Test isValidStatus", t, func() {
		Convey("with a live status", func() {
			isValid := isValidStatus("LIVE")
			So(isValid, ShouldBeTrue)
		})
		Convey("with a suppressed status", func() {
			isValid := isValidStatus("SUPPRESSED")
			So(isValid, ShouldBeTrue)
		})
		Convey("with an invalid status", func() {
			isValid := isValidStatus("BADSTATUS")
			So(isValid, ShouldBeFalse)
		})
		Convey("with an empty status", func() {
			isValid := isValidStatus("")
			So(isValid, ShouldBeFalse)
		})
	})
}

func generateSetGameAsinRequest(asin string, gameTitle string, status string) io.Reader {
	request := janus.GameAsinMap{
		Asin:      asin,
		GameTitle: gameTitle,
		Status:    status,
	}
	jsonBytes, err := json.Marshal(request)
	So(err, ShouldBeNil)
	return bytes.NewReader(jsonBytes)
}
