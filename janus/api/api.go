package api

import (
	"code.justin.tv/commerce/janus/api/apidef"
	"code.justin.tv/commerce/janus/decorators"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/middleware"
	authtwirp "code.justin.tv/commerce/janus/twirp/generated/dev_portal_auth/twirp"
	"code.justin.tv/common/twitchhttp"
	"goji.io"
	"goji.io/pat"
)

// ServerAPI represents regrouped APIs of our service
type ServerAPI struct {
	AmendmentAcceptanceAPI      AmendmentAcceptanceAPI
	AssociatesStoreAPI          AssociatesStoreAPI
	AuthApi                     authtwirp.Auth
	BroadcasterPayoutDetailsAPI BroadcasterPayoutDetailsAPI
	ChatNotificationAPI         ChatNotificationAPI
	GameAsinAPI                 GameAsinAPI
	GameCommerceRevenueAPI      GameCommerceRevenueAPI
	GameDetailsAPI              GameDetailsAPI
	GameSettingsAPI             GameSettingsAPI
	GetActiveMarketplacesAPI    GetActiveMarketplacesAPI
	GetBadgesAPI                GetBadgesAPI
	GetCratesAPI                GetCratesAPI
	GetMerchandiseAPI           GetMerchandiseAPI
	GetEmoticonsAPI             GetEmoticonsAPI
	InGameContentDetailsAPI     InGameContentDetailsAPI
	ExtensionsProductsAPI       ExtensionsProductsAPI
	MetricLogger                metrics.IMetricLogger
	GetOffersAPI                GetOffersAPI
	OffersSettingsAPI           OffersSettingsAPI
}

// Server Mux
type Server struct {
	*goji.Mux
}

// NewServer sets up new server
func (serverAPI *ServerAPI) NewServer() (*Server, error) {

	server := twitchhttp.NewServer()

	s := &Server{
		server,
	}

	handlerMetrics := middleware.NewHandlerMetrics(serverAPI.MetricLogger)

	// middleware
	s.Use(middleware.AccessLogger)
	s.UseC(handlerMetrics.Metrics)
	s.UseC(middleware.Recoverer)
	s.UseC(middleware.URLParser)

	// register API endpoint mapping to handlers
	s.HandleC(pat.Get("/hello"), decorators.DecorateAndAdapt(s.hello, apidef.Hello))
	s.HandleC(pat.Get("/games/:game/details"), decorators.DecorateAndAdapt(serverAPI.GameDetailsAPI.getGameDetails, apidef.GetGameDetails))
	s.HandleC(pat.Get("/games/:game/ingame"), decorators.DecorateAndAdapt(serverAPI.InGameContentDetailsAPI.getInGameContentDetails, apidef.GetInGameContentDetails))
	s.HandleC(pat.Get("/games/:game/settings"), decorators.DecorateAndAdapt(serverAPI.GameSettingsAPI.getGameSettings, apidef.GetGameSettings))
	s.HandleC(pat.Get("/games/:game/asin"), decorators.DecorateAndAdapt(serverAPI.GameAsinAPI.getGameAsin, apidef.GetGameAsin))
	s.HandleC(pat.Put("/games/:game/asin"), decorators.DecorateAndAdapt(serverAPI.GameAsinAPI.setGameAsin, apidef.SetGameAsin))

	s.HandleC(pat.Get("/offers"), decorators.DecorateAndAdapt(serverAPI.GetOffersAPI.getOffers, apidef.GetOffers))
	s.HandleC(pat.Get("/offers/settings"), decorators.DecorateAndAdapt(serverAPI.OffersSettingsAPI.getOffersSettings, apidef.GetOffersSettings))

	s.HandleC(pat.Get("/extensions/products"), decorators.DecorateAndAdapt(serverAPI.ExtensionsProductsAPI.getExtensionsProducts, apidef.GetExtensionsProducts))

	s.HandleC(pat.Get("/broadcaster/:tuid/associates/store"), decorators.DecorateAndAdapt(serverAPI.AssociatesStoreAPI.getAssociatesStore, apidef.GetAssociatesStore))

	s.HandleC(pat.Get("/broadcaster/:tuid/amendment/:amendmentType"), decorators.DecorateAndAdapt(serverAPI.AmendmentAcceptanceAPI.getAmendmentAcceptance, apidef.GetAmendmentAcceptance))
	s.HandleC(pat.Put("/broadcaster/:tuid/amendment/:amendmentType"), decorators.DecorateAndAdapt(serverAPI.AmendmentAcceptanceAPI.setAmendmentAcceptance, apidef.SetAmendmentAcceptance))

	// internal admin only api for setting amendments (unauthenticated)
	s.HandleC(pat.Put("/admin/broadcaster/:tuid/amendment/:amendmentType"), decorators.DecorateAndAdapt(serverAPI.AmendmentAcceptanceAPI.adminSetAmendmentAcceptance, apidef.AdminSetAmendmentAcceptance))

	s.HandleC(pat.Get("/broadcaster/:tuid/payout"), decorators.DecorateAndAdapt(serverAPI.BroadcasterPayoutDetailsAPI.getBroadcasterPayoutDetails, apidef.GetBroadcasterPayoutDetails))
	s.HandleC(pat.Get("/broadcaster/:tuid/payout/retail"), decorators.DecorateAndAdapt(serverAPI.BroadcasterPayoutDetailsAPI.getBroadcasterRetailPayoutDetails, apidef.GetBroadcasterRetailPayoutDetails))

	s.HandleC(pat.Get("/broadcaster/:tuid/extensions/revenue"), decorators.DecorateAndAdapt(serverAPI.GameCommerceRevenueAPI.getExtensionsRevenue, apidef.GetExtensionsRevenue))
	s.HandleC(pat.Get("/broadcaster/:tuid/gamecommerce/revenue"), decorators.DecorateAndAdapt(serverAPI.GameCommerceRevenueAPI.getGameCommerceRevenue, apidef.GetGameCommerceRevenue))

	s.HandleC(pat.Get("/inventory/:tuid/badges"), decorators.DecorateAndAdapt(serverAPI.GetBadgesAPI.getBadges, apidef.GetBadges))
	s.HandleC(pat.Get("/inventory/:tuid/crates"), decorators.DecorateAndAdapt(serverAPI.GetCratesAPI.getCrates, apidef.GetCrates))
	s.HandleC(pat.Get("/inventory/:tuid/emoticons"), decorators.DecorateAndAdapt(serverAPI.GetEmoticonsAPI.getEmoticons, apidef.GetEmoticons))
	s.HandleC(pat.Get("/inventory/:tuid/active_marketplaces"), decorators.DecorateAndAdapt(serverAPI.GetActiveMarketplacesAPI.getActiveMarketplaces, apidef.GetActiveMarketplaces))

	s.HandleC(pat.Get("/users/:userID/notifications"), decorators.DecorateAndAdapt(serverAPI.ChatNotificationAPI.getChatNotifications, apidef.GetChatNotifications))
	s.HandleC(pat.Post("/users/:userID/notifications"), decorators.DecorateAndAdapt(serverAPI.ChatNotificationAPI.consumeChatNotification, apidef.ConsumeChatNotification))

	s.HandleC(pat.Get("/merchandise"), decorators.DecorateAndAdapt(serverAPI.GetMerchandiseAPI.getMerchandise, apidef.GetMerchandise))

	s.Handle(pat.Post(authtwirp.AuthPathPrefix+"*"), authtwirp.NewAuthServer(serverAPI.AuthApi, nil, nil))

	return s, nil
}
