package apidef

import (
	goji_middleware "goji.io/middleware"
	"golang.org/x/net/context"

	log "github.com/sirupsen/logrus"
)

// HandlerName is the name of a handler.
type HandlerName string

// NamedHandler is an interface representing an API handler with a name.
type NamedHandler interface {
	GetHandlerName() HandlerName
}

// Public APIs
const (
	GetGameDetails                    HandlerName = "getGameDetails"
	GetInGameContentDetails           HandlerName = "getInGameContentDetails"
	GetGameSettings                   HandlerName = "getGameSettings"
	GetGameAsin                       HandlerName = "getGameAsin"
	SetGameAsin                       HandlerName = "setGameAsin"
	GetAmendmentAcceptance            HandlerName = "getAmendmentAcceptance"
	SetAmendmentAcceptance            HandlerName = "setAmendmentAcceptance"
	GetBroadcasterPayoutDetails       HandlerName = "getBroadcasterPayoutDetails"
	GetBroadcasterRetailPayoutDetails HandlerName = "getBroadcasterRetailPayoutDetails"
	Hello                             HandlerName = "Hello"
	GetExtensionsRevenue              HandlerName = "getExtensionsRevenue"
	GetGameCommerceRevenue            HandlerName = "getGameCommerceRevenue"
	GetBadges                         HandlerName = "getBadges"
	GetCrates                         HandlerName = "getCrates"
	GetEmoticons                      HandlerName = "getEmoticons"
	GetActiveMarketplaces             HandlerName = "getActiveMarketplaces"
	GetChatNotifications              HandlerName = "getChatNotifications"
	ConsumeChatNotification           HandlerName = "consumeChatNotification"
	AdminSetAmendmentAcceptance       HandlerName = "adminSetAmendmentAcceptance"
	GetMerchandise                    HandlerName = "getMerchandise"
	GetExtensionsProducts             HandlerName = "getExtensionsProducts"
	GetOffers                         HandlerName = "getOffers"
	GetOffersSettings                 HandlerName = "getOffersSettings"
	GetAssociatesStore                HandlerName = "getAssociatesStore"
)

// GetNamedHandler returns the NamedHandler that is handling the request with the specified context. This method will
// return nil if there is no handler associated with this request. This could happen if a bad url (e.g. /foobar) is
// requested
func GetNamedHandler(ctx context.Context) NamedHandler {
	handler := goji_middleware.Handler(ctx)
	if handler != nil {
		namedHandler, ok := handler.(NamedHandler)
		if !ok {
			log.Errorf("Handler is not of type NamedHandler. Pattern: %s", goji_middleware.Pattern(ctx))
			return nil
		}
		return namedHandler
	}
	return nil
}
