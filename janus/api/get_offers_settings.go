package api

import (
	"net/http"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/broadcaster"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// OffersSettingsAPI represents the API to get offer settings
type OffersSettingsAPI struct {
	BroadcasterHelper           broadcaster.IBroadcasterHelper
	BroadcasterProductOffersDAO dynamo.IProductOffersDAO
	GatingHelper                auth.IGatingHelper
}

// getOffersSettings returns the settings for offers associated with a gameID
func (offersSettingsAPI *OffersSettingsAPI) getOffersSettings(c context.Context, w http.ResponseWriter, r *http.Request) {
	gameID := r.Form.Get("game_id")
	broadcasterID := r.Form.Get("broadcaster_id")
	viewerCountryCode := r.Form.Get("viewer_country_code")

	// Authenticate and authorize the acting tuid for this request, if present
	userID, err := offersSettingsAPI.GatingHelper.GetOptionalActingTuid(r)
	if err != nil {
		log.Warnf("Failure getting active tuid for request: %s", err)
		userID = ""
	}
	isWhitelisted := offersSettingsAPI.GatingHelper.IsWhitelisted(userID)
	isStaff, err := offersSettingsAPI.GatingHelper.IsAdminOrSubAdmin(r, userID)
	if err != nil {
		log.Warnf("Failure performing Admin check for %s: %s", userID, err)
		isStaff = false
	}

	retailEnabled, err := offersSettingsAPI.getRetailOffersEnabled(gameID, broadcasterID, viewerCountryCode, isWhitelisted, isStaff)
	if err != nil {
		log.Errorf("Error getting whether retail offers enabled for broadcaster %s and gameID %s: %s", broadcasterID, gameID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	response := janus.GetOffersSettingsResponse{
		AmazonRetailEnabled: retailEnabled,
	}

	writeResponse(response, w)
}

// getRetailOffersEnabled determines whether retail offers should display on a specific channel page for a specific game
func (offersSettingsAPI *OffersSettingsAPI) getRetailOffersEnabled(gameID string, broadcasterID string, viewerCountryCode string, isWhitelisted bool, isStaff bool) (bool, error) {
	if gameID == "" || broadcasterID == "" {
		return false, nil
	}

	if !IsRetailEnabledForCountry(viewerCountryCode) {
		return false, nil
	}

	offers, err := offersSettingsAPI.BroadcasterProductOffersDAO.GetAllVisibleItems(broadcasterID, isWhitelisted, isStaff)
	if err != nil {
		log.Errorf("Error receiving product offers from dynamo: %s", err)
		return false, err
	}

	// Only show retail as enabled if there are offers and the broadcaster is eligible to display retail offers
	retailEnabled := false
	if len(offers) > 0 {
		retailEnabled, err = offersSettingsAPI.BroadcasterHelper.GetRetailEligibility(broadcasterID, gameID)
		if err != nil {
			log.Errorf("Failure getting broadcaster eligibility to show retail offers: %s", err)
			return false, err
		}
	}

	return retailEnabled, nil
}

var countryCodeWhitelist = map[string]bool{
	"US": true,
	"":   true, // default to showing for customers whose country we don't know
}

// Check to ensure retail offers are enabled for a country.  Currently used to restrict Gillette to US, but impacts all better-together.
// TODO: Make this smarter before we use Better Together for something else.
func IsRetailEnabledForCountry(countryCode string) bool {
	return countryCodeWhitelist[countryCode]
}
