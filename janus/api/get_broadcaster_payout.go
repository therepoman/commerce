package api

import (
	"net/http"

	"goji.io/pat"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/payouts"
	"golang.org/x/net/context"
)

// BroadcasterPayoutDetailsAPI represents the API to get whether broadcasters are enabled to recieve certain payouts
type BroadcasterPayoutDetailsAPI struct {
	PayoutsEligibilityChecker payouts.IPayoutsEligibilityChecker
}

// getBroadcasterPayoutDetails returns information related to a broadcaster's Fuel payout details
func (broadcasterPayoutDetailsAPI *BroadcasterPayoutDetailsAPI) getBroadcasterPayoutDetails(c context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(c, "tuid")

	payoutEligibility, err := broadcasterPayoutDetailsAPI.PayoutsEligibilityChecker.GetGameCommercePayoutEligibility(tuid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
	}

	response := janus.GetBroadcasterPayoutDetailsResponse{
		IsEligible: payoutEligibility,
	}

	writeResponse(response, w)
}

// getBroadcasterRetailPayoutDetails returns information related to a broadcaster's payouts details for retail commerce
func (broadcasterPayoutDetailsAPI *BroadcasterPayoutDetailsAPI) getBroadcasterRetailPayoutDetails(c context.Context, w http.ResponseWriter, r *http.Request) {
	tuid := pat.Param(c, "tuid")

	isRetailPayoutEligible, err := broadcasterPayoutDetailsAPI.PayoutsEligibilityChecker.GetRetailPayoutEligibility(c, tuid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	response := janus.GetBroadcasterRetailPayoutDetailsResponse{
		IsEligible: isRetailPayoutEligible,
	}

	writeResponse(response, w)
}
