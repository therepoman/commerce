package api

import (
	"net/http"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"goji.io/pat"
	"golang.org/x/net/context"

	"fmt"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/metrics"
	log "github.com/sirupsen/logrus"
)

const (
	GetEmoticonsAPIEmoticonSetCount = "GetEmoticonsAPI.EmoticonSetCount"
)

// GetEmoticonsAPI represents the API to get emoticons
type GetEmoticonsAPI struct {
	GatingHelper      auth.IGatingHelper
	MakoClientWrapper clients.IMakoClientWrapper
	MetricsLogger     metrics.IMetricLogger
}

func (getEmoticonsAPI *GetEmoticonsAPI) getEmoticons(ctx context.Context, w http.ResponseWriter, r *http.Request) {

	tuid := pat.Param(ctx, "tuid") // tuid is part of URL path so shouldn't be nil, but that could change
	if tuid == "" {
		log.Errorf("Error getting emoticons: tuid is nil")
		http.Error(w, "Error getting emoticons: tuid is nil", http.StatusBadRequest)
		return
	}

	// Authorization: Tuid being looked up must be the same as the acting tuid.
	userIsId, err := getEmoticonsAPI.GatingHelper.UserIsId(r, tuid)
	if err != nil {
		log.Warnf("Could not authorize current user to getEmoticonsAPI for Twitch user %s: %s", tuid, err)
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	} else if !userIsId {
		errorMsg := fmt.Sprintf("Not authorized to getEmoticonsAPI for Twitch user %s", tuid)
		log.Warn(errorMsg)
		http.Error(w, errorMsg, http.StatusForbidden)
		return
	}

	makoResp, err := getEmoticonsAPI.MakoClientWrapper.GetEmoticonSets(ctx, tuid)
	if err != nil { // only returns nil if server failure
		msg := fmt.Sprintf("Error fetching emoticon sets: %s", err.Error())
		log.Errorf("Error fetching emoticon sets: %s", err)
		http.Error(w, msg, http.StatusInternalServerError)
		return
	}

	resp := janus.GetEmoticonsResponse{}
	if len(makoResp.Keys) < 1 {
		resp.EmoticonSets = make(map[string][]janus.Emoticon)
		writeResponse(resp, w)
		return
	}

	getEmoticonsAPI.MetricsLogger.LogCountMetric(GetEmoticonsAPIEmoticonSetCount, len(makoResp.Keys))

	// FIXME add caching
	chatResp, err := getEmoticonsAPI.MakoClientWrapper.GetEmoticonsByGroups(ctx, makoResp.Keys)
	// FIXME nil can mean bad request or a server error, need to differentiate for metrics
	if err != nil {
		log.Errorf("Error fetching emoticon sets details: %s", err)
		http.Error(w, "Error fetching emoticon sets details: "+err.Error(), http.StatusInternalServerError)
		return
	}

	emoticonSets := make(map[string][]janus.Emoticon)
	for key, emoticons := range (*chatResp).Details {
		emoticonSets[key] = []janus.Emoticon{}

		for _, emoticon := range emoticons {
			emoticonSets[key] = append(emoticonSets[key], janus.Emoticon{
				ID:   emoticon.ID,
				Code: emoticon.Pattern,
			})
		}
	}

	resp.EmoticonSets = emoticonSets
	writeResponse(resp, w)
}
