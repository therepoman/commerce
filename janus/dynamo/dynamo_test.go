package dynamo

import (
	"os"
	"testing"

	"code.justin.tv/common/go_test_dynamo"
)

func TestMain(m *testing.M) {
	tddb := go_test_dynamo.Instance()
	code := m.Run()
	err := tddb.Shutdown()
	if err != nil {
		os.Exit(1)
	}
	os.Exit(code)
}
