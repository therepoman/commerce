package dynamo

import (
	"errors"
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

const (
	keyTUID      = "tuid"
	keyProductID = "productid"
)

type PermissionTable struct {
}

func (this *PermissionTable) GetBaseTableName() TableName {
	return "fuel_permission"
}

func (this *PermissionTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	tuid := StringFromAttributes(attributeMap, keyTUID)
	gameId := StringFromAttributes(attributeMap, keyProductID)

	permission := &Permission{
		TwitchUserID: tuid,
		GameID:       gameId,
		Permissions:  make([]string, 0, 1024),
	}
	for k, _ := range attributeMap {
		if k == keyTUID {
			continue
		}
		if k == keyProductID {
			continue
		}
		permission.Permissions = append(permission.Permissions, k)
	}
	return permission, nil
}

type Permission struct {
	TwitchUserID string
	GameID       string
	Permissions  []string
}

func (this *Permission) GetTable() Table {
	return &PermissionTable{}
}

func (this *Permission) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap[keyTUID] = &dynamodb.AttributeValue{S: aws.String(this.TwitchUserID)}
	keyMap[keyProductID] = &dynamodb.AttributeValue{S: aws.String(this.GameID)}
	return keyMap
}

func (this *Permission) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := this.NewItemKey()
	for _, permission := range this.Permissions {
		attributeMap[strings.ToLower(permission)] = &dynamodb.AttributeValue{
			BOOL: aws.Bool(true),
		}
	}
	return attributeMap
}

type IPermissionDAO interface {
	GetPermission(ctx context.Context, tuid, gameid string) (*Permission, error)
	AddPermission(ctx context.Context, tuid, gameid string, permissions []string) error
	GetAllPermissionsForUser(ctx context.Context, tuid string) ([]*Permission, error)
	CreateTable() error
}

type PermissionDAO struct {
	BaseDao
}

func NewPermissionDAO(client *Client) IPermissionDAO {
	return &PermissionDAO{
		BaseDao{
			Table:  &PermissionTable{},
			Client: client,
		},
	}
}

func (this *PermissionDAO) CreateTable() error {
	tableName := this.Client.GetFullTableName(this.Table).String()
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(keyTUID),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String(keyProductID),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(keyTUID),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(keyProductID),
				AttributeType: aws.String("S"),
			},
		},
	}
	_, err := this.Client.Dynamo.CreateTable(&ctr)
	return err
}

func (this *PermissionDAO) GetPermission(ctx context.Context, tuid, gameid string) (*Permission, error) {
	permRecord := &Permission{
		TwitchUserID: tuid,
		GameID:       gameid,
	}

	result, err := this.Client.GetItem(permRecord)
	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	permRecord, isType := result.(*Permission)
	if !isType {
		msg := fmt.Sprintf("Failed to convert dynamo result to Permission for User: %s, Product: %s", tuid, gameid)
		log.Error(msg)
		return nil, errors.New(msg)
	}

	return permRecord, nil
}

func (this *PermissionDAO) AddPermission(ctx context.Context, tuid, gameid string, permissions []string) error {
	return this.Client.PutItem(&Permission{
		TwitchUserID: tuid,
		GameID:       gameid,
		Permissions:  permissions,
	})
}

func (this *PermissionDAO) GetAllPermissionsForUser(ctx context.Context, tuid string) ([]*Permission, error) {
	results := make([]*Permission, 0, 1024)

	kTuid := fmt.Sprintf("#%s", keyTUID)
	vTuid := fmt.Sprintf(":%s", keyTUID)
	keyExpression := fmt.Sprintf("%s = %s", kTuid, vTuid)
	filter := QueryFilter{
		ExpressionAttributeNames: map[string]*string{
			kTuid: aws.String(keyTUID),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			vTuid: &dynamodb.AttributeValue{S: aws.String(tuid)},
		},
		KeyConditionExpression: aws.String(keyExpression),
	}
	result, err := this.Client.Query(&Permission{}, filter)
	if err != nil {
		return results, nil
	}

	for _, record := range result {
		permRecord, isRightType := record.(*Permission)
		if !isRightType {
			return results, errors.New("Encountered wrong type converting dynamo record")
		}
		results = append(results, permRecord)
	}

	return results, nil
}
