package dynamo

import (
	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const activeMarketplacesQueryLimit = 50

// ActiveMarketplaces dynamo table
type ActiveMarketplacesTable struct {
}

// ActiveMarketplace represents a marketplace a tuid has purchased from
type ActiveMarketplace struct {
	Tuid          string
	MarketplaceID string
}

// ActiveMarketplaceDAO data access object for ActiveMarketplace
type ActiveMarketplaceDAO struct {
	BaseDao
}

// IActiveMarketplaceDAO interface for the activeMarketplace DAO
type IActiveMarketplaceDAO interface {
	GetMarketplaces(tuid string) ([]*ActiveMarketplace, error)
	PutIfNotExists(*ActiveMarketplace) error
}

// NewActiveMarketplaceDAO constructor for ActiveMarketplaceDAO
func NewActiveMarketplaceDAO(client *Client) IActiveMarketplaceDAO {
	dao := &ActiveMarketplaceDAO{BaseDao{
		Client: client,
		Table:  &ActiveMarketplacesTable{},
	}}
	return dao
}

// GetBaseTableName returns the base activeMarketplace table name
func (t *ActiveMarketplacesTable) GetBaseTableName() TableName {
	return "active_marketplaces"
}

// ConvertAttributeMapToRecord converts one mapping of attributes from the Dynamo repsonse to a ActiveMarketplace record
func (t *ActiveMarketplacesTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	tuid := StringFromAttributes(attributeMap, "tuid")
	marketplaceID := StringFromAttributes(attributeMap, "marketplaceID")

	return &ActiveMarketplace{
		Tuid:          tuid,
		MarketplaceID: marketplaceID,
	}, nil
}

// GetTable returns the activeMarketplace table
func (n *ActiveMarketplace) GetTable() Table {
	return &ActiveMarketplacesTable{}
}

// GetMarketplaces queries Dynamo for ActiveMarketplaces matching the provided tuid
func (dao *ActiveMarketplaceDAO) GetMarketplaces(tuid string) ([]*ActiveMarketplace, error) {
	// This is to tell Query what table to use
	activeMarketplacesStruct := &ActiveMarketplace{}

	result, err := dao.Client.Query(activeMarketplacesStruct, getTuidFilter(tuid))
	if err != nil {
		return nil, err
	}

	// Result will be an empty list if no item is found in dynamo
	if result == nil {
		return make([]*ActiveMarketplace, 0), nil
	}

	activeMarketplacesResult := make([]*ActiveMarketplace, len(result))
	for i, record := range result {
		var isActiveMarketplaceRecord bool
		activeMarketplacesResult[i], isActiveMarketplaceRecord = record.(*ActiveMarketplace)
		if !isActiveMarketplaceRecord {
			return nil, errors.New("Encountered an unexpected type while converting dynamo result to an ActiveMarketplace record")
		}
	}

	return activeMarketplacesResult, nil
}

func getTuidFilter(tuid string) QueryFilter {
	limit := int64(fuelChatNotificationsQueryLimit)

	filter := QueryFilter{
		ExpressionAttributeNames: map[string]*string{
			"#TUID": aws.String("tuid"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":tuid": &dynamodb.AttributeValue{S: aws.String(tuid)},
		},
		KeyConditionExpression: aws.String("#TUID = :tuid"),
		Limit: &limit,
	}
	return filter
}

// PutIfNotExists adds the ActiveMarketplace to the database
func (dao *ActiveMarketplaceDAO) PutIfNotExists(activeMarketplace *ActiveMarketplace) error {
	err := dao.Client.PutItemWithCondition(activeMarketplace, aws.String("attribute_not_exists(marketplaceID)"))

	if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == "ConditionalCheckFailedException" {
		// Entry already exists
		return nil
	}

	return err
}

// NewItemKey returns the key map to query against in Dynamo
func (am *ActiveMarketplace) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["tuid"] = &dynamodb.AttributeValue{S: aws.String(am.Tuid)}
	keyMap["marketplaceID"] = &dynamodb.AttributeValue{S: aws.String(am.MarketplaceID)}
	return keyMap
}

// NewAttributeMap creates a mapping from ActiveMarketplace to Dynamo attributes
func (am *ActiveMarketplace) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, "tuid", am.Tuid)
	PopulateAttributesString(attributeMap, "marketplaceID", am.MarketplaceID)
	return attributeMap
}
