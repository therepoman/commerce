package dynamo

import (
	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
)

const (
	idKey     = "id"
	urlKey    = "url"
	heightKey = "height"
	widthKey  = "width"
)

// MerchandiseLifestyleImagesTable the image table
type MerchandiseLifestyleImagesTable struct{}

// MerchandiseLifestyleImages stores the lifestyle images needed to display on the merch page
type MerchandiseLifestyleImages struct {
	ID     string
	URL    string
	Height int64
	Width  int64
	Status string
	Rank   int64
}

// MerchandiseLifestyleImagesDAO data access object for lifestyle images
type MerchandiseLifestyleImagesDAO struct {
	BaseDao
}

// IMerchandiseLifestyleImagesDAO interface for the lifestyle images DAO
type IMerchandiseLifestyleImagesDAO interface {
	Get(id string) (*MerchandiseLifestyleImages, error)
	Put(merchLifestyleImages *MerchandiseLifestyleImages) error
	Scan() ([]*MerchandiseLifestyleImages, error)
	CreateTable() error
}

// NewMerchandiseLifestyleImagesDAO constructor for MerchandiseLifestyleImagesDAO
func NewMerchandiseLifestyleImagesDAO(client *Client) IMerchandiseLifestyleImagesDAO {
	dao := &MerchandiseLifestyleImagesDAO{BaseDao{
		Client: client,
		Table:  &MerchandiseLifestyleImagesTable{},
	}}
	return dao
}

// GetBaseTableName returns the base table name
func (merchLifestyleImagesTable *MerchandiseLifestyleImagesTable) GetBaseTableName() TableName {
	return "merchandise_lifestyle"
}

// ConvertAttributeMapToRecord converts the Dynamo response to a lifestyle images map
func (merchLifestyleImagesTable *MerchandiseLifestyleImagesTable) ConvertAttributeMapToRecord(attribute map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	id := StringFromAttributes(attribute, idKey)
	url := StringFromAttributes(attribute, urlKey)
	status := StringFromAttributes(attribute, merchStatusKey)
	height, err := Int64FromAttributes(attribute, heightKey)
	if err != nil {
		log.Errorf("Failed to convert height to int64")
		return nil, err
	}
	width, err := Int64FromAttributes(attribute, widthKey)
	if err != nil {
		log.Errorf("Failed to convert width to int64")
		return nil, err
	}
	rank, err := Int64FromAttributes(attribute, rankKey)
	if err != nil {
		log.Errorf("Failed to convert rank to int64")
		return nil, err
	}

	return &MerchandiseLifestyleImages{
		ID:     id,
		URL:    url,
		Height: height,
		Width:  width,
		Status: status,
		Rank:   rank,
	}, nil
}

// GetTable returns the lifestyle images table
func (merchLifestyleImages *MerchandiseLifestyleImages) GetTable() Table {
	return &MerchandiseLifestyleImagesTable{}
}

func getMerchandiseLifestyleScanFilter() ScanFilter {
	filter := ScanFilter{
		ExpressionAttributeNames: map[string]*string{
			"#STATUS": aws.String("status"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":status": &dynamodb.AttributeValue{S: aws.String(string(liveValue))},
		},
		KeyConditionExpression: aws.String("#STATUS = :status"),
	}
	return filter
}

// Scan returns the whole table as a list of MerchandiseLifestyleImages
func (dao *MerchandiseLifestyleImagesDAO) Scan() ([]*MerchandiseLifestyleImages, error) {
	merchLifestyleImages := &MerchandiseLifestyleImages{}
	filter := getMerchandiseLifestyleScanFilter()
	result, err := dao.Client.ScanTable(merchLifestyleImages, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	merchLifestyleImagesResult := make([]*MerchandiseLifestyleImages, len(result))
	for i, record := range result {
		var isMerchandiseLifestyleImages bool
		merchLifestyleImagesResult[i], isMerchandiseLifestyleImages = record.(*MerchandiseLifestyleImages)
		if !isMerchandiseLifestyleImages {
			msg := "Encountered an unexpected type while converting dynamo result to merch metadata record"
			log.Error(msg)
			return merchLifestyleImagesResult, errors.New(msg)
		}
	}

	return merchLifestyleImagesResult, nil
}

// Get queries Dynamo for a MerchandiseLifestyleImages matching the provided id
func (dao *MerchandiseLifestyleImagesDAO) Get(id string) (*MerchandiseLifestyleImages, error) {
	var merchLifestyleImagesResult *MerchandiseLifestyleImages

	merchLifestyleImagesQuery := &MerchandiseLifestyleImages{
		ID: id,
	}

	result, err := dao.Client.GetItem(merchLifestyleImagesQuery)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	var isMerchandiseLifestyleImages bool
	merchLifestyleImagesResult, isMerchandiseLifestyleImages = result.(*MerchandiseLifestyleImages)
	if !isMerchandiseLifestyleImages {
		log.Errorf("Failed to convert dynamo result to MerchandiseLifestyleImages - result: %+v", result)
		return nil, errors.New("Failed to convert dynamo result to MerchandiseLifestyleImages")
	}

	return merchLifestyleImagesResult, nil
}

// Put adds the MerchandiseLifestyleImages to the database
func (dao *MerchandiseLifestyleImagesDAO) Put(merchLifestyleImages *MerchandiseLifestyleImages) error {
	return dao.Client.PutItem(merchLifestyleImages)
}

// NewItemKey returns the key map to query against in Dynamo
func (merchLifestyleImages *MerchandiseLifestyleImages) NewItemKey() map[string]*dynamodb.AttributeValue {
	key := make(map[string]*dynamodb.AttributeValue)
	key[idKey] = &dynamodb.AttributeValue{S: aws.String(merchLifestyleImages.ID)}
	return key
}

// NewAttributeMap creates a mapping from MerchandiseLifestyleImages to Dynamo attributes
func (merchLifestyleImages *MerchandiseLifestyleImages) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attribute := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attribute, idKey, merchLifestyleImages.ID)
	PopulateAttributesString(attribute, urlKey, merchLifestyleImages.URL)
	PopulateAttributesString(attribute, merchStatusKey, merchLifestyleImages.Status)
	PopulateAttributesInt64(attribute, heightKey, merchLifestyleImages.Height)
	PopulateAttributesInt64(attribute, widthKey, merchLifestyleImages.Width)
	PopulateAttributesInt64(attribute, rankKey, merchLifestyleImages.Rank)
	return attribute
}

// CreateTable is used by testing and should not be used for table creation
func (merchandiseLifestyleImagesDAO *MerchandiseLifestyleImagesDAO) CreateTable() error {
	tableName := merchandiseLifestyleImagesDAO.Client.GetFullTableName(merchandiseLifestyleImagesDAO.Table).String()
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(idKey),
				KeyType:       aws.String(keyTypeHash),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(idKey),
				AttributeType: aws.String("S"),
			},
		},
	}
	_, err := merchandiseLifestyleImagesDAO.Client.Dynamo.CreateTable(&ctr)
	return err
}
