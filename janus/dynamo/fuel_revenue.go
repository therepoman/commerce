package dynamo

import (
	"errors"
	"time"

	log "github.com/sirupsen/logrus"
)

// FuelRevenueTable the fuelRevenue table
type FuelRevenueTable struct {
	BaseRevenueTable
}

// FuelRevenue metadata
type FuelRevenue struct {
	BaseRevenue
}

// IFuelRevenueDAO interface for the fuelRevenue DAO
type IFuelRevenueDAO interface {
	GetFuelRevenue(TwitchUserID string, startTime time.Time, endTime time.Time) ([]*BaseRevenue, error)
}

// FuelRevenueDAO data access object for fuelRevenue
type FuelRevenueDAO struct {
	BaseDao
}

// NewFuelRevenueDao constructor for FuelRevenueDao
func NewFuelRevenueDao(client *Client) IFuelRevenueDAO {
	dao := &FuelRevenueDAO{BaseDao{
		Client: client,
		Table:  &FuelRevenueTable{},
	}}
	return dao
}

// GetBaseTableName returns the base fuelRevenue table name
func (fuelRevenueTable *FuelRevenueTable) GetBaseTableName() TableName {
	return "fuel_dashboard_hours"
}

// GetTable returns the fuelRevenue table
func (fuelRevenue *FuelRevenue) GetTable() Table {
	return &FuelRevenueTable{}
}

// GetFuelRevenue queries Dynamo for fuelRevenue matching the provided TwitchUserID
func (fuelRevenueDAO *FuelRevenueDAO) GetFuelRevenue(twitchUserID string, startTime time.Time, endTime time.Time) ([]*BaseRevenue, error) {
	filter := getQueryFilter(twitchUserID, startTime, endTime)

	fuelRevenue := &FuelRevenue{}

	result, err := fuelRevenueDAO.Client.Query(fuelRevenue, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	revenueResult := make([]*BaseRevenue, len(result))
	for i, record := range result {
		var isFuelRevenue bool
		revenueResult[i], isFuelRevenue = record.(*BaseRevenue)
		if !isFuelRevenue {
			msg := "Encountered an unexpected type while converting dynamo result to revenue record"
			log.Error(msg)
			return revenueResult, errors.New(msg)
		}
	}

	return revenueResult, nil
}
