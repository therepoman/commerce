package dynamo_test

import (
	"testing"

	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_MerchandiseMetadata_Success(t *testing.T) {
	Convey("With Table", t, func() {
		defer go_test_dynamo.Instance().Cleanup()

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewMerchandiseMetadataDAO(testDynamoClient)
		So(dao.CreateTable(), ShouldBeNil)

		Convey("With No Records", func() {

			Convey("Scan", func() {
				merchandiseList, err := dao.Scan()
				So(err, ShouldBeNil)
				So(merchandiseList, ShouldBeEmpty)
			})
		})

		Convey("With One Record", func() {
			item := dynamo.MerchandiseMetadata{
				Asin:          "mock",
				OriginalPrice: 0.99,
				IsFeatured:    true,
				IsNew:         true,
				IsOutOfStock:  true,
				Rank:          1,
				Status:        "LIVE",
			}

			err := dao.Put(&item)
			So(err, ShouldBeNil)

			Convey("Scan", func() {
				merchandiseList, err := dao.Scan()
				So(err, ShouldBeNil)
				So(len(merchandiseList), ShouldEqual, 1)

				merchandiseItem := merchandiseList[0]
				So(merchandiseItem.Asin, ShouldNotBeNil)
				So(merchandiseItem.OriginalPrice, ShouldNotBeNil)
				So(merchandiseItem.IsFeatured, ShouldBeTrue)
				So(merchandiseItem.IsNew, ShouldBeTrue)
				So(merchandiseItem.IsOutOfStock, ShouldBeTrue)
			})
		})

		Convey("With One non-LIVE Record", func() {
			item := dynamo.MerchandiseMetadata{
				Asin:   "mock",
				Status: "foo",
			}

			err := dao.Put(&item)
			So(err, ShouldBeNil)

			Convey("Scan", func() {
				merchandiseList, err := dao.Scan()
				So(err, ShouldBeNil)
				So(merchandiseList, ShouldBeEmpty)
			})
		})
	})
}
