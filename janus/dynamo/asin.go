package dynamo

import (
	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
)

const (
	asinKey      = "asin"
	gameTitleKey = "gameTitle"
	statusKey    = "status"
)

// FuelAsinMapTable the asin map table
type FuelAsinMapTable struct{}

// FuelAsinMap stores the mapping from game title to asin, and associated metadata
type FuelAsinMap struct {
	GameTitle string
	Asin      string
	Status    string
}

// FuelAsinMapDAO data access object for asin mapping
type FuelAsinMapDAO struct {
	BaseDao
}

// IFuelAsinMapDAO interface for the asin map DAO
type IFuelAsinMapDAO interface {
	Get(gameTitle string) (*FuelAsinMap, error)
	Put(asinMap *FuelAsinMap) error
}

// NewFuelAsinMapDAO constructor for FuelAsinMapDAO
func NewFuelAsinMapDAO(client *Client) IFuelAsinMapDAO {
	dao := &FuelAsinMapDAO{BaseDao{
		Client: client,
		Table:  &FuelAsinMapTable{},
	}}
	return dao
}

// GetBaseTableName returns the base table name
func (asinMapTable *FuelAsinMapTable) GetBaseTableName() TableName {
	return "fuel_asin_map"
}

// ConvertAttributeMapToRecord converts the Dynamo response to an asin map
func (asinMapTable *FuelAsinMapTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	asin := StringFromAttributes(attributeMap, asinKey)
	gameTitle := StringFromAttributes(attributeMap, gameTitleKey)
	status := StringFromAttributes(attributeMap, statusKey)

	return &FuelAsinMap{
		Asin:      asin,
		GameTitle: gameTitle,
		Status:    status,
	}, nil
}

// GetTable returns the asin map table
func (asinMap *FuelAsinMap) GetTable() Table {
	return &FuelAsinMapTable{}
}

// Get queries Dynamo for a FuelAsinMap matching the provided gameTitle
func (dao *FuelAsinMapDAO) Get(gameTitle string) (*FuelAsinMap, error) {
	var fuelAsinMapResult *FuelAsinMap

	fuelAsinMapQuery := &FuelAsinMap{
		GameTitle: gameTitle,
	}

	result, err := dao.Client.GetItem(fuelAsinMapQuery)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	var isFuelAsinMap bool
	fuelAsinMapResult, isFuelAsinMap = result.(*FuelAsinMap)
	if !isFuelAsinMap {
		log.Errorf("Failed to convert dynamo result to FuelAsinMap - result: %+v", result)
		return nil, errors.New("Failed to convert dynamo result to FuelAsinMap")
	}

	return fuelAsinMapResult, nil
}

// Put adds the FuelAsinMap to the database
func (dao *FuelAsinMapDAO) Put(fuelAsinMap *FuelAsinMap) error {
	return dao.Client.PutItem(fuelAsinMap)
}

// NewItemKey returns the key map to query against in Dynamo
func (asinMap *FuelAsinMap) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap[gameTitleKey] = &dynamodb.AttributeValue{S: aws.String(asinMap.GameTitle)}
	return keyMap
}

// NewAttributeMap creates a mapping from FuelAsinMap to Dynamo attributes
func (asinMap *FuelAsinMap) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, asinKey, asinMap.Asin)
	PopulateAttributesString(attributeMap, gameTitleKey, asinMap.GameTitle)
	PopulateAttributesString(attributeMap, statusKey, asinMap.Status)
	return attributeMap
}
