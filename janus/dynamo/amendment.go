package dynamo

import (
	"errors"
	"time"

	"code.justin.tv/commerce/janus/cache"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	userId         = "userId"
	amendmentType  = "amendmentType"
	acceptanceDate = "acceptanceDate"
)

// AmendmentTable the amendment table
type AmendmentTable struct {
}

// Amendment metadata
type Amendment struct {
	UserID         string
	AmendmentType  string
	AcceptanceDate time.Time
}

// AmendmentDAO data access object for amendments
type AmendmentDAO struct {
	BaseDao
	cacheClient cache.IAmendmentCacheClient
}

// IAmendmentDAO interface for the amendment DAO
type IAmendmentDAO interface {
	Get(userID string, amendmentType string) (*Amendment, error)
	Put(amendment *Amendment) error
	Delete(amendment *Amendment) error
	CreateTable() error
}

// NewAmendmentDao constructor for AmendmentDao
func NewAmendmentDao(client *Client, cacheClient cache.IAmendmentCacheClient) IAmendmentDAO {
	dao := &AmendmentDAO{
		BaseDao{
			Client: client,
			Table:  &AmendmentTable{},
		},
		cacheClient,
	}
	return dao
}

// GetBaseTableName returns the base amendment table name
func (amendmentTable *AmendmentTable) GetBaseTableName() TableName {
	return "broadcaster_amendments"
}

// ConvertAttributeMapToRecord converts the Dynamo response to an Amendment
func (amendmentTable *AmendmentTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	userID := StringFromAttributes(attributeMap, userId)
	amendmentType := StringFromAttributes(attributeMap, amendmentType)

	acceptanceDate, err := TimeFromAttributes(attributeMap, acceptanceDate)
	if err != nil {
		log.Errorf("Failed to convert acceptanceDate to time")
		return nil, err
	}

	return &Amendment{
		UserID:         userID,
		AmendmentType:  amendmentType,
		AcceptanceDate: acceptanceDate,
	}, nil
}

// GetTable returns the amendment table
func (amendment *Amendment) GetTable() Table {
	return &AmendmentTable{}
}

// NewItemKey returns the key map to query against in Dynamo
func (amendment *Amendment) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap[userId] = &dynamodb.AttributeValue{S: aws.String(amendment.UserID)}
	keyMap[amendmentType] = &dynamodb.AttributeValue{S: aws.String(amendment.AmendmentType)}
	return keyMap
}

// NewAttributeMap creates a mapping from Amendment to Dynamo attributes
func (amendment *Amendment) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, userId, amendment.UserID)
	PopulateAttributesString(attributeMap, amendmentType, amendment.AmendmentType)
	PopulateAttributesTime(attributeMap, acceptanceDate, amendment.AcceptanceDate)
	return attributeMap
}

func NewAmendment(cached *cache.Amendment) *Amendment {
	return &Amendment{
		UserID:         cached.UserID,
		AmendmentType:  cached.AmendmentType,
		AcceptanceDate: cached.AcceptanceDate,
	}
}

func (blacklist *Amendment) NewCacheAmendment() *cache.Amendment {
	return &cache.Amendment{
		UserID:         blacklist.UserID,
		AmendmentType:  blacklist.AmendmentType,
		AcceptanceDate: blacklist.AcceptanceDate,
	}
}

// CreateTable is used by testing and should not be used for table creation
func (amendmentDAO *AmendmentDAO) CreateTable() error {
	tableName := amendmentDAO.Client.GetFullTableName(amendmentDAO.Table).String()
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(userId),
				KeyType:       aws.String(keyTypeHash),
			},
			{
				AttributeName: aws.String(amendmentType),
				KeyType:       aws.String(keyTypeRange),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(userId),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(amendmentType),
				AttributeType: aws.String("S"),
			},
		},
	}
	_, err := amendmentDAO.Client.Dynamo.CreateTable(&ctr)
	return err
}

// Get queries Dynamo for amendments matching the provided userID
func (amendmentDAO *AmendmentDAO) Get(userID string, amendmentType string) (*Amendment, error) {
	isCacheHit, cachedAmendment := amendmentDAO.cacheClient.Get(userID, amendmentType)
	if isCacheHit {
		if cachedAmendment != nil {
			return NewAmendment(cachedAmendment), nil
		} else {
			// The cache hit has a nil value for the amendment.
			// This means there was no record found in Dynamo when the cache entry was created.
			return nil, nil
		}
	}

	var amendmentResult *Amendment

	amendmentQuery := &Amendment{
		UserID:        userID,
		AmendmentType: amendmentType,
	}

	result, err := amendmentDAO.Client.GetItem(amendmentQuery)
	if err != nil {
		return amendmentResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		amendmentDAO.cacheClient.PutAmendmentNotFound(userID, amendmentType)
		return nil, nil
	}

	amendmentResult, isAmendment := result.(*Amendment)
	if !isAmendment {
		log.Errorf("Failed to convert dynamo result to Amendment")
		return nil, errors.New("Failed to convert dynamo result to Amendment")
	}

	amendmentDAO.cacheClient.Put(amendmentResult.NewCacheAmendment())

	return amendmentResult, nil
}

// Put adds the user to the database
func (amendmentDAO *AmendmentDAO) Put(amendment *Amendment) error {
	err := amendmentDAO.Client.PutItem(amendment)

	if err == nil {
		amendmentDAO.cacheClient.Put(amendment.NewCacheAmendment())
	}

	return err
}

// Delete removes the user from the database
func (amendmentDAO *AmendmentDAO) Delete(amendment *Amendment) error {
	return amendmentDAO.Client.DeleteItem(amendment)
}
