package dynamo

import (
	"errors"
	"strconv"
	"time"

	"fmt"

	"code.justin.tv/commerce/janus/cache"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	log "github.com/sirupsen/logrus"
)

const (
	IdKey                                 = "id"
	TitleKey                              = "title"
	AsinKey                               = "asin"
	PriorityKey                           = "priority"
	StatusKey                             = "status"
	OfferStartTimeKey                     = "offerStartDate"
	OfferEndTimeKey                       = "offerEndDate"
	IdPriorityIndexName                   = "id-priority-index"
	BroadcasterProductOffersTableBaseName = "broadcaster_product_offers" // Currently used for Gillette.
	GameProductOffersTableBaseName        = "game_product_offers"        // Better Together, currently unused.
)

// ProductOffers the ID -> Retail Offers mapping table
type ProductOffersTable struct {
	BaseTableName TableName
}

// Product offer metadata struct
// New fields here should be added to the attribute conversions below, cache.ProductOffers, and the cache conversions.
type ProductOffers struct {
	Id             string
	Title          string
	Asin           string
	Priority       int64
	Status         string
	OfferStartTime time.Time
	OfferEndTime   time.Time
	BaseTableName  TableName
}

// ProductOffers data access object for lifestyle images
type ProductOffersDAO struct {
	BaseDao
	cacheClient cache.IProductOffersCacheClient
}

// GetBaseTableName returns the base table name
func (productOffersTable *ProductOffersTable) GetBaseTableName() TableName {
	return productOffersTable.BaseTableName
}

type IProductOffersDAO interface {
	GetAllVisibleItems(id string, isWhitelisted bool, isStaff bool) ([]*ProductOffers, error)
	GetVisibleItem(id string, asin string, isWhitelisted bool, isStaff bool) (*ProductOffers, error)
	Put(productOffer *ProductOffers) error
}

/*
	IMPLEMENTED TABLE FUNCTIONS
*/

// NewProductOffersDAO constructor for ProductOffersDAO
func NewProductOffersDAO(client *Client, baseTableName TableName, cacheClient cache.IProductOffersCacheClient) IProductOffersDAO {
	dao := &ProductOffersDAO{
		BaseDao{
			Client: client,
			Table:  &ProductOffersTable{baseTableName},
		},
		cacheClient,
	}
	return dao
}

// ConvertAttributeMapToRecord converts the Dynamo response to a merch metadata map
func (productOffersTable *ProductOffersTable) ConvertAttributeMapToRecord(attribute map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	id := StringFromAttributes(attribute, IdKey)
	asin := StringFromAttributes(attribute, AsinKey)
	title := StringFromAttributes(attribute, TitleKey)
	status := StringFromAttributes(attribute, StatusKey)

	priority, err := Int64FromAttributes(attribute, PriorityKey)
	if err != nil {
		log.Errorf("Failed to convert priority to int64 while querying DB: %s", productOffersTable.GetBaseTableName())
		return nil, err
	}
	offerstarttime, err := TimeUnixSecondsFromAttributes(attribute, OfferStartTimeKey)
	if err != nil {
		log.Errorf("Failed to convert timeOfEvent to time while querying DB: %s", productOffersTable.GetBaseTableName())
		return nil, err
	}
	offerendtime, err := TimeUnixSecondsFromAttributes(attribute, OfferEndTimeKey)
	if err != nil {
		log.Errorf("Failed to convert timeOfEvent to time while querying DB: %s", productOffersTable.GetBaseTableName())
		return nil, err
	}

	return &ProductOffers{
		Id:             id,
		Asin:           asin,
		Title:          title,
		Priority:       priority,
		Status:         status,
		OfferStartTime: offerstarttime,
		OfferEndTime:   offerendtime,
	}, nil
}

/*
	IMPLEMENTED TABLE RECORD FUNCTIONS
*/

// GetTable returns the product offers table
func (productOffers *ProductOffers) GetTable() Table {
	return &ProductOffersTable{productOffers.BaseTableName}
}

// NewItemKey returns the key map to query against in Dynamo
func (productOffers *ProductOffers) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["id"] = &dynamodb.AttributeValue{S: aws.String(productOffers.Id)}
	keyMap["asin"] = &dynamodb.AttributeValue{S: aws.String(productOffers.Asin)}
	return keyMap
}

// NewAttributeMap creates a mapping from MerchandiseMetadata to Dynamo attributes
func (productOffers *ProductOffers) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attribute := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attribute, IdKey, productOffers.Id)
	PopulateAttributesString(attribute, TitleKey, productOffers.Title)
	PopulateAttributesString(attribute, AsinKey, productOffers.Asin)
	PopulateAttributesInt64(attribute, PriorityKey, productOffers.Priority)
	PopulateAttributesString(attribute, StatusKey, productOffers.Status)
	PopulateAttributesTimeUnixSeconds(attribute, OfferStartTimeKey, productOffers.OfferStartTime)
	PopulateAttributesTimeUnixSeconds(attribute, OfferEndTimeKey, productOffers.OfferEndTime)
	return attribute
}

/*
	IMPLEMENTED CACHE CONVERSION FUNCTIONS
*/

func NewProductOffers(cached *cache.ProductOffers) *ProductOffers {
	return &ProductOffers{
		Id:             cached.Id,
		Asin:           cached.Asin,
		Title:          cached.Title,
		Priority:       cached.Priority,
		Status:         cached.Status,
		OfferStartTime: cached.OfferStartTime,
		OfferEndTime:   cached.OfferEndTime,
	}
}

func (productOffers *ProductOffers) NewCacheProductOffers() *cache.ProductOffers {
	return &cache.ProductOffers{
		Id:             productOffers.Id,
		Asin:           productOffers.Asin,
		Title:          productOffers.Title,
		Priority:       productOffers.Priority,
		Status:         productOffers.Status,
		OfferStartTime: productOffers.OfferStartTime,
		OfferEndTime:   productOffers.OfferEndTime,
	}
}

/*
	IMPLEMENTED DAO METHODS
*/

// Put adds the MerchandiseMetadata to the database
func (dao *ProductOffersDAO) Put(productOffer *ProductOffers) error {
	err := dao.Client.PutItem(productOffer)
	if err == nil {
		dao.cacheClient.Put(productOffer.NewCacheProductOffers())
	}
	return err
}

// GetVisibleItem queries Dynamo for a ProductOffer matching the provided id and asin
// Hides offer based on acting user whitelist and staff status, and current time
func (dao *ProductOffersDAO) GetVisibleItem(id string, asin string, isWhitelisted bool, isStaff bool) (*ProductOffers, error) {
	var productOfferResult *ProductOffers

	cached := dao.cacheClient.Get(id, asin)

	if cached != nil {
		productOfferResult = NewProductOffers(cached)
	} else {
		getProductOfferQuery := &ProductOffers{
			Id:            id,
			Asin:          asin,
			BaseTableName: dao.Table.GetBaseTableName(),
		}

		result, err := dao.Client.GetItem(getProductOfferQuery)
		if err != nil {
			return nil, err
		}

		// Result will be nil if no item is found in dynamo
		if result == nil {
			return nil, nil
		}

		var isProductOffer bool
		productOfferResult, isProductOffer = result.(*ProductOffers)
		if !isProductOffer {
			log.Errorf("Failed to convert dynamo result to ProductOffer while querying DB: %s - result: %+v", getProductOfferQuery.BaseTableName, result)
			return nil, errors.New(fmt.Sprintf("Failed to convert dynamo result to ProductOffer while querying DB: %s", getProductOfferQuery.BaseTableName))
		}

		dao.cacheClient.Put(productOfferResult.NewCacheProductOffers())
	}

	// Only return offer if the user should see it
	timeNow := time.Now()
	withinOfferTime := timeNow.Before(productOfferResult.OfferEndTime) && timeNow.After(productOfferResult.OfferStartTime)
	if (productOfferResult.Status == StatusLive && (isWhitelisted || isStaff || withinOfferTime)) ||
		(productOfferResult.Status == StatusStaff && (isWhitelisted || isStaff)) ||
		(productOfferResult.Status == StatusWhitelist && isWhitelisted) {
		return productOfferResult, nil
	}

	return nil, nil
}

// GetAllVisibleItems all offers that should be visible given the inputted whitelist/staff status and current time
func (dao *ProductOffersDAO) GetAllVisibleItems(id string, isWhitelisted bool, isStaff bool) ([]*ProductOffers, error) {
	cachedItems := dao.cacheClient.GetVisibleItems(id, isWhitelisted, isStaff)
	if cachedItems != nil {
		contentResult := make([]*ProductOffers, len(cachedItems))
		for i, item := range cachedItems {
			contentResult[i] = NewProductOffers(item)
		}
		return contentResult, nil
	}

	filter := getProductOfferQueryFilter(id, isWhitelisted, isStaff)

	productOffers := &ProductOffers{BaseTableName: dao.Table.GetBaseTableName()}
	result, err := dao.Client.Query(productOffers, filter)
	if err != nil {
		return nil, err
	}

	contentResult := make([]*ProductOffers, len(result))
	cachedItems = make([]*cache.ProductOffers, len(result))
	for i, record := range result {
		var isProductOffer bool
		contentResult[i], isProductOffer = record.(*ProductOffers)
		if !isProductOffer {
			msg := fmt.Sprintf("Encountered an unexpected type while converting dynamo result to ProductOffer record while querying DB: %s", dao.Table.GetBaseTableName())
			log.Error(msg)
			return nil, errors.New(msg)
		}
		cachedItems[i] = contentResult[i].NewCacheProductOffers()
	}

	dao.cacheClient.PutVisibleItems(id, isWhitelisted, isStaff, cachedItems)

	return contentResult, nil
}

func getProductOfferQueryFilter(id string, isWhitelisted bool, isStaff bool) QueryFilter {
	indexName := IdPriorityIndexName
	expressionAttributeNames := map[string]*string{
		"#ID":     aws.String(IdKey),
		"#STATUS": aws.String(StatusKey),
	}
	expressionAttributeValues := map[string]*dynamodb.AttributeValue{
		":id":   {S: aws.String(string(id))},
		":live": {S: aws.String(StatusLive)},
	}

	var filterExpression string
	if isWhitelisted {
		expressionAttributeValues[":whitelist"] = &dynamodb.AttributeValue{S: aws.String(StatusWhitelist)}
		expressionAttributeValues[":staff"] = &dynamodb.AttributeValue{S: aws.String(StatusStaff)}
		filterExpression = "#STATUS = :whitelist or #STATUS = :staff or #STATUS = :live"
	} else if isStaff {
		expressionAttributeValues[":staff"] = &dynamodb.AttributeValue{S: aws.String(StatusStaff)}
		filterExpression = "#STATUS = :staff or #STATUS = :live"
	} else {
		timeString := strconv.FormatInt(time.Now().Unix(), 10)
		expressionAttributeValues[":timeNow"] = &dynamodb.AttributeValue{N: aws.String(timeString)}
		expressionAttributeNames["#START_DATE"] = aws.String(OfferStartTimeKey)
		expressionAttributeNames["#END_DATE"] = aws.String(OfferEndTimeKey)
		filterExpression = "#STATUS = :live and :timeNow BETWEEN #START_DATE AND #END_DATE"
	}

	filter := QueryFilter{
		ExpressionAttributeNames:  expressionAttributeNames,
		ExpressionAttributeValues: expressionAttributeValues,
		KeyConditionExpression:    aws.String("#ID = :id"),
		FilterExpression:          aws.String(filterExpression),
		IndexName:                 &indexName,
		Descending:                false,
	}
	return filter
}
