package dynamo

import (
	"errors"
	"time"

	log "github.com/sirupsen/logrus"
)

// ExtensionsRevenueTable the table for extensions revenue
type ExtensionsRevenueTable struct {
	BaseRevenueTable
}

// ExtensionsRevenue metadata
type ExtensionsRevenue struct {
	BaseRevenue
}

// IExtensionsRevenueDAO interface for the extensions revenue DAO
type IExtensionsRevenueDAO interface {
	GetExtensionsRevenue(TwitchUserID string, startTime time.Time, endTime time.Time) ([]*BaseRevenue, error)
}

// ExtensionsRevenueDAO data access object for extensions revenue
type ExtensionsRevenueDAO struct {
	BaseDao
}

// NewExtensionsRevenueDao constructor for ExtensionsRevenueDAO
func NewExtensionsRevenueDao(client *Client) IExtensionsRevenueDAO {
	dao := &ExtensionsRevenueDAO{BaseDao{
		Client: client,
		Table:  &ExtensionsRevenueTable{},
	}}
	return dao
}

// GetBaseTableName returns the base extensions revenue table name
func (extensionsRevenueTable *ExtensionsRevenueTable) GetBaseTableName() TableName {
	return "extension_dashboard_hours"
}

// GetTable returns the extensions revenue table
func (extensionsRevenue *ExtensionsRevenue) GetTable() Table {
	return &ExtensionsRevenueTable{}
}

// GetExtensionsRevenue queries Dynamo for extension revenue matching the provided TwitchUserID
func (extensionsRevenueDAO *ExtensionsRevenueDAO) GetExtensionsRevenue(twitchUserID string, startTime time.Time, endTime time.Time) ([]*BaseRevenue, error) {
	filter := getQueryFilter(twitchUserID, startTime, endTime)

	extensionRevenue := &ExtensionsRevenue{}

	result, err := extensionsRevenueDAO.Client.Query(extensionRevenue, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	extensionRevenueResult := make([]*BaseRevenue, len(result))
	for i, record := range result {
		var isExtensionRevenue bool
		extensionRevenueResult[i], isExtensionRevenue = record.(*BaseRevenue)
		if !isExtensionRevenue {
			msg := "Encountered an unexpected type while converting dynamo result to revenue record"
			log.Error(msg)
			return extensionRevenueResult, errors.New(msg)
		}
	}

	return extensionRevenueResult, nil
}
