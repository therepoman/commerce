package dynamo

import (
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/janus/cache"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
)

const (
	localeKey = "locale"
	titleKey  = "title"
	tableName = "merchandise_localization"
)

// MerchandiseLocalizationTable the localized strings for merch table
type MerchandiseLocalizationTable struct{}

type MerchandiseLocalization struct {
	Asin   string
	Locale string
	Title  string
}

// MerchandiseLocalizationDAO data access object for localized string mapping
type MerchandiseLocalizationDAO struct {
	BaseDao
	cacheClient cache.IMerchandiseLocalizationDAOCache
}

// IMerchandiseLocalizationDAO interface for the localized string DAO
type IMerchandiseLocalizationDAO interface {
	BatchGet(asinList []string, locale string) ([]*MerchandiseLocalization, error)
	Put(merchLocalization *MerchandiseLocalization) error
	CreateTable() error
}

// NewMerchandiseLocalizationDAO constructor for MerchandiseLocalizationDAO
func NewMerchandiseLocalizationDAO(client *Client, kvCacheClient cache.IMerchandiseLocalizationDAOCache) IMerchandiseLocalizationDAO {
	dao := &MerchandiseLocalizationDAO{BaseDao{
		Client: client,
		Table:  &MerchandiseLocalizationTable{},
	}, kvCacheClient}
	return dao
}

// GetBaseTableName returns the base table name
func (merchLocalizationTable *MerchandiseLocalizationTable) GetBaseTableName() TableName {
	return tableName
}

// ConvertAttributeMapToRecord converts the Dynamo response to a localized strings map
func (merchLocalizationTable *MerchandiseLocalizationTable) ConvertAttributeMapToRecord(attribute map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	asin := StringFromAttributes(attribute, merchAsinKey)
	locale := StringFromAttributes(attribute, localeKey)
	title := StringFromAttributes(attribute, titleKey)

	return &MerchandiseLocalization{
		Asin:   asin,
		Locale: locale,
		Title:  title,
	}, nil
}

// GetTable returns the localized string table
func (merchLocalization *MerchandiseLocalization) GetTable() Table {
	return &MerchandiseLocalizationTable{}
}

// BatchGet does a batch of multiple queries for a MerchandiseLocalization matching the provided asin and locale
func (dao *MerchandiseLocalizationDAO) BatchGet(asinList []string, locale string) ([]*MerchandiseLocalization, error) {

	merchLocalizationQueries := []TableRecord{}
	for _, asin := range asinList {
		merchLocalizationQuery := MerchandiseLocalization{
			Asin:   asin,
			Locale: locale,
		}
		merchLocalizationQueries = append(merchLocalizationQueries, &merchLocalizationQuery)
	}

	var merchLocalizationResults []*MerchandiseLocalization

	cachedTitles := dao.cacheClient.GetLocalizations(asinList, locale)
	if cachedTitles != nil && len(cachedTitles) > 0 {
		for _, cacheValue := range cachedTitles {
			merchLocalization := MerchandiseLocalization{
				Asin:   cacheValue.Asin,
				Title:  cacheValue.Title,
				Locale: locale,
			}
			merchLocalizationResults = append(merchLocalizationResults, &merchLocalization)
		}
		return merchLocalizationResults, nil
	}

	result, err := dao.Client.BatchGetItem(merchLocalizationQueries)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	cacheValue := []cache.MerchandiseLocalizationCacheValue{}
	for _, item := range result {
		merchLocalizationResult, isMerchandiseLocalization := item.(*MerchandiseLocalization)
		if !isMerchandiseLocalization {
			log.Errorf("Failed to convert dynamo result to MerchandiseLocalization - result: %+v", result)
			return nil, errors.New("Failed to convert dynamo result to MerchandiseLocalization")
		}
		merchLocalizationResults = append(merchLocalizationResults, merchLocalizationResult)

		// build the values for the cache
		cachedLocalization := cache.MerchandiseLocalizationCacheValue{
			Asin:  merchLocalizationResult.Asin,
			Title: merchLocalizationResult.Title,
		}
		cacheValue = append(cacheValue, cachedLocalization)
	}

	dao.cacheClient.SetLocalizations(asinList, locale, cacheValue)
	return merchLocalizationResults, nil
}

// Put adds the MerchandiseLocalization to the database
func (dao *MerchandiseLocalizationDAO) Put(merchLocalization *MerchandiseLocalization) error {
	return dao.Client.PutItem(merchLocalization)
}

// NewItemKey returns the key map to query against in Dynamo
func (merchLocalization *MerchandiseLocalization) NewItemKey() map[string]*dynamodb.AttributeValue {
	key := make(map[string]*dynamodb.AttributeValue)
	key[merchAsinKey] = &dynamodb.AttributeValue{S: aws.String(merchLocalization.Asin)}
	key[localeKey] = &dynamodb.AttributeValue{S: aws.String(merchLocalization.Locale)}
	return key
}

// NewAttributeMap creates a mapping from MerchandiseLocalization to Dynamo attributes
func (merchLocalization *MerchandiseLocalization) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attribute := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attribute, merchAsinKey, merchLocalization.Asin)
	PopulateAttributesString(attribute, localeKey, merchLocalization.Locale)
	PopulateAttributesString(attribute, titleKey, merchLocalization.Title)
	return attribute
}

// CreateTable is used by testing and should not be used for table creation
func (merchandiseLocalizationDAO *MerchandiseLocalizationDAO) CreateTable() error {
	tableName := merchandiseLocalizationDAO.Client.GetFullTableName(merchandiseLocalizationDAO.Table).String()
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(merchAsinKey),
				KeyType:       aws.String(keyTypeHash),
			},
			{
				AttributeName: aws.String(localeKey),
				KeyType:       aws.String(keyTypeRange),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(merchAsinKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(localeKey),
				AttributeType: aws.String("S"),
			},
		},
	}
	_, err := merchandiseLocalizationDAO.Client.Dynamo.CreateTable(&ctr)
	return err
}
