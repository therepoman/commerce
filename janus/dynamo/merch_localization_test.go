package dynamo_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	mockAsin   = "mockAsin"
	mockLocale = "mockLocale"
)

func Test_MerchandiseLocalization_Success(t *testing.T) {
	Convey("With Table", t, func() {
		defer go_test_dynamo.Instance().Cleanup()
		mockedCacheClient := new(mocks.IMerchandiseLocalizationDAOCache)

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewMerchandiseLocalizationDAO(testDynamoClient, mockedCacheClient)
		So(dao.CreateTable(), ShouldBeNil)

		Convey("With No Records", func() {

			Convey("LocalizedMerchandise", func() {
				mockedCacheClient.On("GetLocalizations", mock.Anything, mock.Anything).Return([]cache.MerchandiseLocalizationCacheValue{}, nil).Once()
				mockedCacheClient.On("SetLocalizations", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				merchandiseLocalizations, err := dao.BatchGet([]string{mockAsin}, mockLocale)
				So(err, ShouldBeNil)
				So(merchandiseLocalizations, ShouldBeNil)
			})
		})

		Convey("With One Record", func() {
			item := dynamo.MerchandiseLocalization{
				Asin:   mockAsin,
				Locale: mockLocale,
				Title:  "foo bar",
			}

			mockedCacheClient.On("SetLocalizations", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			err := dao.Put(&item)
			So(err, ShouldBeNil)

			Convey("Get when cache call errors", func() {
				mockedCacheClient.On("GetLocalizations", mock.Anything, mock.Anything).Return(nil, errors.New("Cache client get call failed!")).Once()
				mockedCacheClient.On("SetLocalizations", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
				merchandiseLocalizations, err := dao.BatchGet([]string{mockAsin}, mockLocale)
				So(err, ShouldBeNil)
				So(merchandiseLocalizations, ShouldNotBeNil)
				So(len(merchandiseLocalizations), ShouldEqual, 1)

				merchandiseLocalization := merchandiseLocalizations[0]

				So(merchandiseLocalization.Asin, ShouldNotBeNil)
				So(merchandiseLocalization.Locale, ShouldNotBeNil)
				So(merchandiseLocalization.Title, ShouldNotBeNil)
				So(merchandiseLocalization.Title, ShouldEqual, "foo bar")
			})
			Convey("Get when cache is empty", func() {
				mockedCacheClient.On("GetLocalizations", mock.Anything, mock.Anything).Return([]cache.MerchandiseLocalizationCacheValue{}, nil).Once()
				mockedCacheClient.On("SetLocalizations", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
				merchandiseLocalizations, err := dao.BatchGet([]string{mockAsin}, mockLocale)
				So(err, ShouldBeNil)
				So(merchandiseLocalizations, ShouldNotBeNil)
				So(len(merchandiseLocalizations), ShouldEqual, 1)

				merchandiseLocalization := merchandiseLocalizations[0]

				So(merchandiseLocalization.Asin, ShouldNotBeNil)
				So(merchandiseLocalization.Locale, ShouldNotBeNil)
				So(merchandiseLocalization.Title, ShouldNotBeNil)
				So(merchandiseLocalization.Title, ShouldEqual, "foo bar")
			})
			Convey("Get when cache returns value", func() {
				cachedTitles := []cache.MerchandiseLocalizationCacheValue{cache.MerchandiseLocalizationCacheValue{Asin: mockAsin, Title: "cachedTitle"}}
				mockedCacheClient.On("GetLocalizations", mock.Anything, mock.Anything).Return(cachedTitles, nil).Once()
				merchandiseLocalizations, err := dao.BatchGet([]string{mockAsin}, mockLocale)
				So(err, ShouldBeNil)
				So(merchandiseLocalizations, ShouldNotBeNil)
				So(len(merchandiseLocalizations), ShouldEqual, 1)

				merchandiseLocalization := merchandiseLocalizations[0]

				So(merchandiseLocalization.Asin, ShouldEqual, mockAsin)
				So(merchandiseLocalization.Locale, ShouldNotBeNil)
				So(merchandiseLocalization.Title, ShouldNotBeNil)
				So(merchandiseLocalization.Title, ShouldEqual, "cachedTitle")
			})
		})
	})
}
