package dynamo

import (
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// StringFromAttributes retrieves the string matching the given key from the attribute map
func StringFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) string {
	var result string
	attribute, exists := attributeMap[attributeName]
	if exists {
		result = *attribute.S
	}
	return result
}

// BoolFromAttributes retrieves the string matching the given key from the attribute map
func BoolFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	var result bool
	attribute, exists := attributeMap[attributeName]
	// TODO: add check to see if BOOL, otherwise we can get null pointer errors if someone put another type under that name :(
	if exists && attribute.BOOL != nil {
		result = *attribute.BOOL
	}
	return result
}

// TimeFromAttributes retrives the time matching the given key from the attribute map
func TimeFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (time.Time, error) {
	var result time.Time
	attribute, exists := attributeMap[attributeName]
	if exists && attribute.N != nil {
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			log.Errorf("Error converting dynamo attribute %s to time", attributeName)
			return result, err
		}
		result = time.Unix(0, unixNanoResult)
	}
	return result, nil
}

// TimeUnixSecondsFromAttributes retrives the time matching the given key from the attribute map
func TimeUnixSecondsFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (time.Time, error) {
	var result time.Time
	attribute, exists := attributeMap[attributeName]
	if exists && attribute.N != nil {
		unixResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			log.Errorf("Error converting dynamo attribute %s to time", attributeName)
			return result, err
		}
		result = time.Unix(unixResult, 0)
	}
	return result, nil
}

// Int64FromAttributes extracts an int64 value from the passed dynamo attribute map at the provided
// attribute name.
// DefaultInt64 is returned if any errors occur.
func Int64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (int64, error) {
	var result int64
	attribute, exists := attributeMap[attributeName]
	if !exists || attribute.N == nil {
		return result, nil
	}
	parsedResult, err := strconv.ParseInt(*attribute.N, 10, 64)
	if err != nil {
		log.Errorf("Error converting dynamo attribute %s to int64", attributeName)
		return result, err
	}
	return parsedResult, nil
}

// Float64FromAttributes extracts a float64 value from the passed dynamo attribute map at the provided
// attribute name.
// DefaultFloat64 is returned if any errors occur.
func Float64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (float64, error) {
	var result float64
	attribute, exists := attributeMap[attributeName]
	if !exists || attribute.N == nil {
		return result, nil
	}
	parsedResult, err := strconv.ParseFloat(*attribute.N, 64)
	if err != nil {
		log.Errorf("Error converting dynamo attribute %s to float64", attributeName)
		return result, err
	}
	return parsedResult, nil
}

// PopulateAttributesString add the given string to the attribute map
func PopulateAttributesString(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue string) {
	if attributeValue != "" {
		attributeMap[attributeName] = &dynamodb.AttributeValue{S: aws.String(attributeValue)}
	}
}

// PopulateAttributesBool add the given string to the attribute map
func PopulateAttributesBool(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue bool) {
	// switch to Bool to make nullable??
	attributeMap[attributeName] = &dynamodb.AttributeValue{BOOL: aws.Bool(attributeValue)}
}

// PopulateAttributesTime add the given time to the attribute map
func PopulateAttributesTime(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, time time.Time) {
	if time.UnixNano() > 0 {
		unixNanoStr := strconv.FormatInt(time.UnixNano(), 10)
		attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(unixNanoStr)}
	}
}

// PopulateAttributesTimeUnixSeconds add the given time to the attribute map
func PopulateAttributesTimeUnixSeconds(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, time time.Time) {
	if time.Unix() >= 0 {
		unixStr := strconv.FormatInt(time.Unix(), 10)
		attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(unixStr)}
	}
}

// PopulateAttributesInt64 populates the provided attribute map with attributeValue are
// attributeName.
func PopulateAttributesInt64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue int64) {
	strVal := strconv.FormatInt(attributeValue, 10)
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}

// PopulateAttributesFloat64 add the given float to the attribute map, where precision is the number of decimal points
func PopulateAttributesFloat64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue float64, precision int) {
	strVal := strconv.FormatFloat(attributeValue, 'f', precision, 64)
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}
