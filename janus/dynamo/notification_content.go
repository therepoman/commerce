package dynamo

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// FuelChatNotificationContentsTable the dynamo table that stores Contents for Fuel chat notifications
type FuelChatNotificationContentsTable struct{}

// FuelChatNotificationContent represents a single piece of content related to a Fuel chat notification
type FuelChatNotificationContent struct {
	Token        string // PK
	ContentKey   string // SK
	ChannelID    string
	UserID       string
	ContentType  string
	Quantity     string
	ContentID    string
	EventID      string
	TimeReceived time.Time
}

// FuelChatNotificationContentsDAO data access object for FuelChatNotificationContentsTable
type FuelChatNotificationContentsDAO struct {
	BaseDao
}

// IFuelChatNotificationContentsDAO interface for the FuelChatNotificationContentsDAO
type IFuelChatNotificationContentsDAO interface {
	GetAll(token string) ([]*FuelChatNotificationContent, error)
	PutAll(fuelChatNotificationContents []*FuelChatNotificationContent) error
}

// NewFuelChatNotificationContentsDAO constructor for FuelChatNotificationContentsDAO
func NewFuelChatNotificationContentsDAO(client *Client) IFuelChatNotificationContentsDAO {
	dao := &FuelChatNotificationContentsDAO{BaseDao{
		Client: client,
		Table:  &FuelChatNotificationContentsTable{},
	}}
	return dao
}

// GetBaseTableName returns the base fuelChatNotification table name
func (t *FuelChatNotificationContentsTable) GetBaseTableName() TableName {
	return "fuel_chat_notification_contents"
}

// ConvertAttributeMapToRecord converts one mapping of attributes from the Dynamo repsonse to a FuelChatNotification record
func (t *FuelChatNotificationContentsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	token := StringFromAttributes(attributeMap, "token")
	contentKey := StringFromAttributes(attributeMap, "contentKey")
	channelID := StringFromAttributes(attributeMap, "channelID")
	userID := StringFromAttributes(attributeMap, "userID")
	contentType := StringFromAttributes(attributeMap, "contentType")
	contentID := StringFromAttributes(attributeMap, "contentID")
	eventID := StringFromAttributes(attributeMap, "eventID")
	quantity := StringFromAttributes(attributeMap, "quantity")

	timeReceived, err := TimeFromAttributes(attributeMap, "timeReceived")
	if err != nil {
		log.Errorf("Failed to convert timeReceived to time")
		return nil, err
	}

	return &FuelChatNotificationContent{
		Token:        token,
		ContentKey:   contentKey,
		ChannelID:    channelID,
		UserID:       userID,
		ContentType:  contentType,
		Quantity:     quantity,
		ContentID:    contentID,
		EventID:      eventID,
		TimeReceived: timeReceived,
	}, nil
}

// Get queries Dynamo for all FuelChatNotificationContent for the provided token
func (dao *FuelChatNotificationContentsDAO) GetAll(token string) ([]*FuelChatNotificationContent, error) {
	filter := getFuelChatNotificationContentsQueryFilter(token)

	fuelChatNotificationContent := &FuelChatNotificationContent{}
	result, err := dao.Client.Query(fuelChatNotificationContent, filter)
	if err != nil {
		return nil, err
	}

	contentResult := make([]*FuelChatNotificationContent, len(result))
	for i, record := range result {
		var isFuelChatNotificationContent bool
		contentResult[i], isFuelChatNotificationContent = record.(*FuelChatNotificationContent)
		if !isFuelChatNotificationContent {
			msg := "Encountered an unexpected type while converting dynamo result to fuelChatNotificationContent record"
			log.Error(msg)
			return nil, errors.New(msg)
		}
	}

	return contentResult, nil
}

func getFuelChatNotificationContentsQueryFilter(token string) QueryFilter {
	filter := QueryFilter{
		ExpressionAttributeNames: map[string]*string{
			"#TOKEN": aws.String("token"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":token": &dynamodb.AttributeValue{S: aws.String(token)},
		},
		KeyConditionExpression: aws.String("#TOKEN = :token"),
	}
	return filter
}

// Put adds the FuelChatNotificationContent to the database
func (dao *FuelChatNotificationContentsDAO) PutAll(fuelChatNotificationContents []*FuelChatNotificationContent) error {
	records := make([]TableRecord, len(fuelChatNotificationContents))
	for i, v := range fuelChatNotificationContents {
		records[i] = TableRecord(v)
	}
	return dao.Client.BatchWriteItem(records)
}

// GetTable returns the fuelChatNotification table
func (n *FuelChatNotificationContent) GetTable() Table {
	return &FuelChatNotificationContentsTable{}
}

// NewItemKey returns the key map to query against in Dynamo
func (n *FuelChatNotificationContent) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["token"] = &dynamodb.AttributeValue{S: aws.String(n.Token)}
	keyMap["contentKey"] = &dynamodb.AttributeValue{S: aws.String(n.ContentKey)}
	return keyMap
}

// NewAttributeMap creates a mapping from FuelChatNotificationContent to Dynamo attributes
func (n *FuelChatNotificationContent) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, "token", n.Token)
	PopulateAttributesString(attributeMap, "contentKey", n.ContentKey)
	PopulateAttributesString(attributeMap, "channelID", n.ChannelID)
	PopulateAttributesString(attributeMap, "userID", n.UserID)
	PopulateAttributesString(attributeMap, "contentType", n.ContentType)
	PopulateAttributesString(attributeMap, "quantity", n.Quantity)
	PopulateAttributesString(attributeMap, "contentID", n.ContentID)
	PopulateAttributesString(attributeMap, "eventID", n.EventID)
	PopulateAttributesTime(attributeMap, "timeReceived", n.TimeReceived)
	return attributeMap
}
