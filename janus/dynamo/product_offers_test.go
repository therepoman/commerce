package dynamo_test

import (
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/dynamo"

	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/common/go_test_dynamo"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	keyTypeHash  = "HASH"
	keyTypeRange = "RANGE"

	testTableName = "test_table_name"
)

type UserType struct {
	Description string
	Whitelisted bool
	Staff       bool
}

type OfferType struct {
	Description string
	DynamoItem  *dynamo.ProductOffers
}

type UserOfferVisibility struct {
	User    *UserType
	Offer   *OfferType
	Visible bool
}

func Test_ProductOffers_Success(t *testing.T) {
	futureTime := time.Now().Add(time.Hour)
	pastTime := time.Unix(0, 0)

	whitelistedUser := UserType{
		Description: "Whitelisted User",
		Whitelisted: true,
		Staff:       false,
	}

	staffUser := UserType{
		Description: "Staff User",
		Whitelisted: false,
		Staff:       true,
	}

	normalUser := UserType{
		Description: "Normal User",
		Whitelisted: false,
		Staff:       false,
	}

	suppressedOffer := OfferType{
		Description: "Suppressed Offer",
		DynamoItem: &dynamo.ProductOffers{
			Id:            "mockId",
			Asin:          "mockAsin1",
			Status:        dynamo.StatusSuppressed,
			BaseTableName: testTableName,
		},
	}

	whitelistOffer := OfferType{
		Description: "Whitelisted Offer",
		DynamoItem: &dynamo.ProductOffers{
			Id:            "mockId",
			Asin:          "mockAsin1",
			Status:        dynamo.StatusWhitelist,
			BaseTableName: testTableName,
		},
	}

	staffOffer := OfferType{
		Description: "Staff Offer",
		DynamoItem: &dynamo.ProductOffers{
			Id:            "mockId",
			Asin:          "mockAsin1",
			Status:        dynamo.StatusStaff,
			BaseTableName: testTableName,
		},
	}

	livePreStartOffer := OfferType{
		Description: "Live Offer Pre-StartTime",
		DynamoItem: &dynamo.ProductOffers{
			Id:             "mockId",
			Asin:           "mockAsin1",
			Status:         dynamo.StatusLive,
			OfferStartTime: futureTime,
			OfferEndTime:   futureTime,
			BaseTableName:  testTableName,
		},
	}

	livePostEndOffer := OfferType{
		Description: "Live Offer Post-EndTime",
		DynamoItem: &dynamo.ProductOffers{
			Id:             "mockId",
			Asin:           "mockAsin1",
			Status:         dynamo.StatusLive,
			OfferStartTime: pastTime,
			OfferEndTime:   pastTime,
			BaseTableName:  testTableName,
		},
	}

	visibilityCases := []UserOfferVisibility{
		UserOfferVisibility{User: &whitelistedUser, Offer: &suppressedOffer, Visible: false},
		UserOfferVisibility{User: &staffUser, Offer: &suppressedOffer, Visible: false},
		UserOfferVisibility{User: &normalUser, Offer: &suppressedOffer, Visible: false},

		UserOfferVisibility{User: &whitelistedUser, Offer: &whitelistOffer, Visible: true},
		UserOfferVisibility{User: &staffUser, Offer: &whitelistOffer, Visible: false},
		UserOfferVisibility{User: &normalUser, Offer: &whitelistOffer, Visible: false},

		UserOfferVisibility{User: &whitelistedUser, Offer: &staffOffer, Visible: true},
		UserOfferVisibility{User: &staffUser, Offer: &staffOffer, Visible: true},
		UserOfferVisibility{User: &normalUser, Offer: &staffOffer, Visible: false},

		UserOfferVisibility{User: &whitelistedUser, Offer: &livePreStartOffer, Visible: true},
		UserOfferVisibility{User: &staffUser, Offer: &livePreStartOffer, Visible: true},
		UserOfferVisibility{User: &normalUser, Offer: &livePreStartOffer, Visible: false},

		UserOfferVisibility{User: &whitelistedUser, Offer: &livePostEndOffer, Visible: true},
		UserOfferVisibility{User: &staffUser, Offer: &livePostEndOffer, Visible: true},
		UserOfferVisibility{User: &normalUser, Offer: &livePostEndOffer, Visible: false},
	}

	Convey("With Table", t, func() {
		defer go_test_dynamo.Instance().Cleanup()
		mockedCacheClient := new(mocks.IProductOffersCacheClient)
		tableName := dynamo.TableName(testTableName)

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewProductOffersDAO(testDynamoClient, tableName, mockedCacheClient)
		So(createTable(dao.(*dynamo.ProductOffersDAO), testTableName), ShouldBeNil)

		Convey("With No Records", func() {

			Convey("GetVisibleItem", func() {
				mockedCacheClient.On("Get", "testId1", "testAsin1").Return(nil).Once()
				offer, err := dao.GetVisibleItem("testId1", "testAsin1", false, false)
				So(err, ShouldBeNil)
				So(offer, ShouldBeNil)
			})

			Convey("GetAllVisibleItems", func() {
				mockedCacheClient.On("GetVisibleItems", "testId", false, false).Return(nil).Once()
				mockedCacheClient.On("PutVisibleItems", "testId", false, false, mock.Anything).Return().Once()
				offersList, err := dao.GetAllVisibleItems("testId", false, false)
				So(err, ShouldBeNil)
				So(offersList, ShouldBeEmpty)
			})

			Reset(func() {
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		for caseId, visibilityCase := range visibilityCases {
			user := visibilityCase.User
			offer := visibilityCase.Offer
			cacheItem := offer.DynamoItem.NewCacheProductOffers()

			Convey(fmt.Sprintf("Case %d with %s and %s, Visible: %t", caseId, user.Description, offer.Description, visibilityCase.Visible), func() {
				mockedCacheClient.On("Put", mock.Anything).Return().Once()
				err := dao.Put(offer.DynamoItem)
				So(err, ShouldBeNil)

				Convey("Get with cache miss", func() {
					mockedCacheClient.On("Get", "mockId", "mockAsin1").Return(nil).Once()
					mockedCacheClient.On("Put", mock.Anything).Return().Once()

					offer, err := dao.GetVisibleItem("mockId", "mockAsin1", user.Whitelisted, user.Staff)
					So(err, ShouldBeNil)

					if visibilityCase.Visible {
						So(offer, ShouldNotBeNil)

						So(offer.Id, ShouldEqual, "mockId")
						So(offer.Asin, ShouldEqual, "mockAsin1")
						So(offer.Status, ShouldNotBeNil)
					} else {
						So(offer, ShouldBeNil)
					}
				})

				Convey("GetAll with cache miss", func() {
					mockedCacheClient.On("GetVisibleItems", "mockId", user.Whitelisted, user.Staff).Return(nil).Once()
					mockedCacheClient.On("PutVisibleItems", "mockId", user.Whitelisted, user.Staff, mock.Anything).Return().Once()

					offersList, err := dao.GetAllVisibleItems("mockId", user.Whitelisted, user.Staff)
					So(err, ShouldBeNil)

					if visibilityCase.Visible {
						So(len(offersList), ShouldEqual, 1)

						offer := offersList[0]
						So(offer.Id, ShouldEqual, "mockId")
						So(offer.Asin, ShouldEqual, "mockAsin1")
						So(offer.Status, ShouldNotBeNil)
					} else {
						So(len(offersList), ShouldEqual, 0)
					}
				})

				Convey("Get with cache hit", func() {
					mockedCacheClient.On("Get", "mockId", "mockAsin1").Return(cacheItem).Once()

					offer, err := dao.GetVisibleItem("mockId", "mockAsin1", user.Whitelisted, user.Staff)
					So(err, ShouldBeNil)

					if visibilityCase.Visible {
						So(offer, ShouldNotBeNil)

						So(offer.Id, ShouldEqual, "mockId")
						So(offer.Asin, ShouldEqual, "mockAsin1")
						So(offer.Status, ShouldNotBeNil)
					} else {
						So(offer, ShouldBeNil)
					}
				})

				Convey("GetAll with cache hit", func() {
					if visibilityCase.Visible {
						mockedCacheClient.On("GetVisibleItems", "mockId", user.Whitelisted, user.Staff).Return([]*cache.ProductOffers{cacheItem}).Once()
					} else {
						mockedCacheClient.On("GetVisibleItems", "mockId", user.Whitelisted, user.Staff).Return([]*cache.ProductOffers{}).Once()
					}

					offersList, err := dao.GetAllVisibleItems("mockId", user.Whitelisted, user.Staff)
					So(err, ShouldBeNil)

					if visibilityCase.Visible {
						So(len(offersList), ShouldEqual, 1)

						offer := offersList[0]
						So(offer.Id, ShouldEqual, "mockId")
						So(offer.Asin, ShouldEqual, "mockAsin1")
						So(offer.Status, ShouldNotBeNil)
					} else {
						So(len(offersList), ShouldEqual, 0)
					}
				})

				Reset(func() {
					So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
				})
			})
		}

		Convey("With One Record", func() {
			item := dynamo.ProductOffers{
				Id:             "mockId",
				Asin:           "mockAsin",
				Title:          "mockTitle",
				Priority:       1,
				OfferStartTime: pastTime,
				OfferEndTime:   futureTime,
				Status:         dynamo.StatusLive,
				BaseTableName:  testTableName,
			}

			mockedCacheClient.On("Put", mock.Anything).Return().Once()
			err := dao.Put(&item)
			So(err, ShouldBeNil)
			So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)

			Convey("GetVisibleItem", func() {
				mockedCacheClient.On("Get", "mockId", "mockAsin").Return(nil).Once()
				mockedCacheClient.On("Put", mock.Anything).Return().Once()
				offer, err := dao.GetVisibleItem("mockId", "mockAsin", false, false)
				So(err, ShouldBeNil)
				So(offer, ShouldNotBeNil)

				So(offer.Id, ShouldEqual, "mockId")
				So(offer.Asin, ShouldEqual, "mockAsin")
				So(offer.Title, ShouldNotBeNil)
				So(offer.Priority, ShouldNotBeNil)
				So(offer.Status, ShouldNotBeNil)
				So(offer.OfferStartTime, ShouldNotBeNil)
				So(offer.OfferEndTime, ShouldNotBeNil)
			})

			Convey("GetAllVisibleItems", func() {
				mockedCacheClient.On("GetVisibleItems", "mockId", false, false).Return(nil).Once()
				mockedCacheClient.On("PutVisibleItems", "mockId", false, false, mock.Anything).Return().Once()
				offersList, err := dao.GetAllVisibleItems("mockId", false, false)
				So(err, ShouldBeNil)
				So(len(offersList), ShouldEqual, 1)

				offer := offersList[0]
				So(offer.Id, ShouldEqual, "mockId")
				So(offer.Asin, ShouldNotBeNil)
				So(offer.Title, ShouldNotBeNil)
				So(offer.Priority, ShouldNotBeNil)
				So(offer.Status, ShouldNotBeNil)
				So(offer.OfferStartTime, ShouldNotBeNil)
				So(offer.OfferEndTime, ShouldNotBeNil)
			})

			Reset(func() {
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With Two Records to check sort", func() {
			mockedCacheClient.On("Put", mock.Anything).Return().Twice()

			item1 := dynamo.ProductOffers{
				Id:             "mockId",
				Asin:           "mockAsin1",
				Priority:       2,
				Status:         dynamo.StatusLive,
				OfferStartTime: pastTime,
				OfferEndTime:   futureTime,
				BaseTableName:  testTableName,
			}

			item2 := dynamo.ProductOffers{
				Id:             "mockId",
				Asin:           "mockAsin2",
				Priority:       1,
				Status:         dynamo.StatusLive,
				OfferStartTime: pastTime,
				OfferEndTime:   futureTime,
				BaseTableName:  testTableName,
			}

			err := dao.Put(&item1)
			So(err, ShouldBeNil)

			err = dao.Put(&item2)
			So(err, ShouldBeNil)
			So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)

			Convey("GetAllVisibleItems", func() {
				mockedCacheClient.On("GetVisibleItems", "mockId", mock.Anything, mock.Anything).Return(nil).Once()
				mockedCacheClient.On("PutVisibleItems", "mockId", mock.Anything, mock.Anything, mock.Anything).Return().Once()
				offersList, err := dao.GetAllVisibleItems("mockId", false, false)
				So(err, ShouldBeNil)
				So(len(offersList), ShouldEqual, 2)

				offer1 := offersList[0]
				So(offer1.Id, ShouldEqual, "mockId")
				So(offer1.Asin, ShouldEqual, "mockAsin2")
				So(offer1.Priority, ShouldEqual, 1)

				offer2 := offersList[1]
				So(offer2.Id, ShouldEqual, "mockId")
				So(offer2.Asin, ShouldEqual, "mockAsin1")
				So(offer2.Priority, ShouldEqual, 2)
			})

			Reset(func() {
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})
	})
}

// Create a table to use for testing
func createTable(dao *dynamo.ProductOffersDAO, tableName string) error {
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(dynamo.IdKey),
				KeyType:       aws.String(keyTypeHash),
			},
			{
				AttributeName: aws.String(dynamo.AsinKey),
				KeyType:       aws.String(keyTypeRange),
			},
		},
		LocalSecondaryIndexes: []*dynamodb.LocalSecondaryIndex{
			{
				IndexName: aws.String(dynamo.IdPriorityIndexName),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(dynamo.IdKey),
						KeyType:       aws.String(keyTypeHash),
					},
					{
						AttributeName: aws.String(dynamo.PriorityKey),
						KeyType:       aws.String(keyTypeRange),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(dynamo.IdKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(dynamo.AsinKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(dynamo.PriorityKey),
				AttributeType: aws.String("N"),
			},
		},
	}
	_, err := dao.Client.Dynamo.CreateTable(&ctr)
	return err
}
