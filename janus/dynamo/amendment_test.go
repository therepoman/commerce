package dynamo_test

import (
	"testing"

	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Amendments(t *testing.T) {
	Convey("With Table", t, func() {
		defer go_test_dynamo.Instance().Cleanup()
		mockedCacheClient := new(mocks.IAmendmentCacheClient)

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewAmendmentDao(testDynamoClient, mockedCacheClient)
		So(dao.CreateTable(), ShouldBeNil)

		Convey("With No Records", func() {

			Convey("Get", func() {
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser1", "testAmendment", false)
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldBeNil)
			})

			Convey("Get with cache hit with amendment", func() {
				expectedAmendment := &dynamo.Amendment{
					UserID:        "testUser1",
					AmendmentType: "testAmendment",
				}
				mockedCacheClient.On("Get", "testUser1", "testAmendment").Return(true, expectedAmendment.NewCacheAmendment()).Once()
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldResemble, expectedAmendment)
			})
		})

		Convey("With One Record", func() {
			mockedCacheClient.On("Put", mock.Anything).Return().Once()
			expectedAmendment := &dynamo.Amendment{
				UserID:        "testUser1",
				AmendmentType: "testAmendment",
			}
			err := dao.Put(expectedAmendment)
			So(err, ShouldBeNil)

			Convey("Get Before Delete", func() {
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser1", "testAmendment", true)
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldResemble, expectedAmendment)
			})

			Convey("Get with cache hit without amendment", func() {
				mockedCacheClient.On("Get", "testUser1", "testAmendment").Return(true, nil).Once()
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldBeNil)
			})

			err = dao.Delete(expectedAmendment)
			So(err, ShouldBeNil)

			Convey("Get After Delete", func() {
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser1", "testAmendment", false)
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldBeNil)
			})
		})

		Convey("With Two Records", func() {
			mockedCacheClient.On("Put", mock.Anything).Return().Twice()
			expectedAmendment := &dynamo.Amendment{
				UserID:        "testUser1",
				AmendmentType: "testAmendment",
			}
			err := dao.Put(expectedAmendment)
			So(err, ShouldBeNil)

			otherExpectedAmendment := &dynamo.Amendment{
				UserID:        "testUser2",
				AmendmentType: "testAmendment",
			}
			err = dao.Put(otherExpectedAmendment)
			So(err, ShouldBeNil)

			Convey("Get Before Delete", func() {
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser1", "testAmendment", true)
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldResemble, expectedAmendment)

				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser2", "testAmendment", true)
				amendment2, err := dao.Get("testUser2", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment2, ShouldResemble, otherExpectedAmendment)
			})

			err = dao.Delete(expectedAmendment)
			So(err, ShouldBeNil)

			Convey("When User1 Is Deleted And User2 Present", func() {
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser1", "testAmendment", false)
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldBeNil)

				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser2", "testAmendment", true)
				amendment2, err := dao.Get("testUser2", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment2, ShouldResemble, otherExpectedAmendment)
			})

			err = dao.Delete(otherExpectedAmendment)
			So(err, ShouldBeNil)

			Convey("When User1 And User2 Are Deleted", func() {
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser1", "testAmendment", false)
				amendment, err := dao.Get("testUser1", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment, ShouldBeNil)
				ExpectAmendmentCacheMiss(mockedCacheClient, "testUser2", "testAmendment", false)
				amendment2, err := dao.Get("testUser2", "testAmendment")
				So(err, ShouldBeNil)
				So(amendment2, ShouldBeNil)
			})
		})

		Reset(func() {
			So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
		})
	})
}

func ExpectAmendmentCacheMiss(mockedCacheClient *mocks.IAmendmentCacheClient, userID string, amendmentType string, isAmendmentFoundInDynamo bool) {
	mockedCacheClient.On("Get", userID, amendmentType).Return(false, nil).Once()
	if isAmendmentFoundInDynamo {
		mockedCacheClient.On("Put", mock.Anything).Return().Once()
	} else {
		mockedCacheClient.On("PutAmendmentNotFound", userID, amendmentType).Return().Once()
	}
}
