package dynamo

import (
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/janus/metrics"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	log "github.com/sirupsen/logrus"
)

// BaseDao struct containing common fields for all DAOs
type BaseDao struct {
	Table  Table
	Client *Client
}

// Client the Dynamo client
type Client struct {
	Dynamo       dynamodbiface.DynamoDBAPI
	MetricLogger metrics.IMetricLogger
	TablePrefix  string
}

// QueryFilter parameters for queries
type QueryFilter struct {
	KeyConditionExpression    *string
	FilterExpression          *string
	ExpressionAttributeNames  map[string]*string
	ExpressionAttributeValues map[string]*dynamodb.AttributeValue
	Limit                     *int64
	IndexName                 *string
	Descending                bool
}

// ScanFilter parameters for queries
type ScanFilter struct {
	KeyConditionExpression    *string
	ExpressionAttributeNames  map[string]*string
	ExpressionAttributeValues map[string]*dynamodb.AttributeValue
}

// NewClient the constructor for the Dynamo client
func NewClient(tablePrefix string, dynamo dynamodbiface.DynamoDBAPI, metricLogger metrics.IMetricLogger) *Client {
	return &Client{
		Dynamo:       dynamo,
		MetricLogger: metricLogger,
		TablePrefix:  tablePrefix,
	}
}

// GetFullTableName returns the full Dynamo table name
func (client *Client) GetFullTableName(table BaseTable) TableName {
	var parts []string

	if client.TablePrefix != "" {
		parts = append(parts, client.TablePrefix)
	}

	parts = append(parts, string(table.GetBaseTableName()))

	return TableName(strings.Join(parts, "_"))
}

// GetItem queries Dynamo
func (client *Client) GetItem(record TableRecord) (TableRecord, error) {
	tableName := client.GetFullTableName(record.GetTable())

	startTime := time.Now()
	metricName := fmt.Sprintf("dynamo.%s.getItem", string(tableName))
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	getItemInput := &dynamodb.GetItemInput{
		TableName: aws.String(string(tableName)),
		Key:       record.NewItemKey(),
	}

	result, err := client.Dynamo.GetItem(getItemInput)
	if err != nil {
		log.Errorf("dynamo.Client: Encountered an error calling dynamo GetItem, %v", err)
		return nil, err
	}

	if len(result.Item) == 0 {
		return nil, nil
	}

	record, convertErr := record.GetTable().ConvertAttributeMapToRecord(result.Item)
	if convertErr != nil {
		log.Errorf("dynamo.Client: Encountered an error converting dynamo GetItem result to record, %v", err)
		return nil, convertErr
	}

	return record, nil
}

// BatchGetItem queries Dynamo
// This has a limit of 100 queries per call: http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchGetItem.html
// If we want to do a batch call for more item we'll have to make multiple batch calls
func (client *Client) BatchGetItem(records []TableRecord) ([]TableRecord, error) {
	tableName := client.GetFullTableName(records[0].GetTable()).String()

	startTime := time.Now()
	metricName := fmt.Sprintf("dynamo.%s.batchGetItem", string(tableName))
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	requestItems := map[string]*dynamodb.KeysAndAttributes{}
	// get all records
	keys := []map[string]*dynamodb.AttributeValue{}
	for _, record := range records {
		keys = append(keys, record.NewItemKey())
	}
	requestItems[tableName] = &dynamodb.KeysAndAttributes{Keys: keys}

	batchGetItemInput := &dynamodb.BatchGetItemInput{
		RequestItems: requestItems,
	}

	result, err := client.Dynamo.BatchGetItem(batchGetItemInput)
	if err != nil {
		log.Errorf("dynamo.Client: Encountered an error calling dynamo BatchGetItem, %v", err)
		return nil, err
	}

	responses := result.Responses[tableName]
	tableRecords := []TableRecord{}
	for _, response := range responses {
		if len(response) == 0 {
			continue
		}
		record, convertErr := records[0].GetTable().ConvertAttributeMapToRecord(response)
		tableRecords = append(tableRecords, record)
		if convertErr != nil {
			log.Errorf("dynamo.Client: Encountered an error converting dynamo BatchGetItem result to record, %v", err)
			return nil, convertErr
		}
	}

	return tableRecords, nil
}

// Scan queries all the entries in a Dynamo table
func (client *Client) ScanTable(record TableRecord, filter ScanFilter) ([]TableRecord, error) {
	tableName := client.GetFullTableName(record.GetTable())

	startTime := time.Now()
	metricName := fmt.Sprintf("dynamo.%s.scanTable", string(tableName))
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	input := &dynamodb.ScanInput{
		TableName:                 aws.String(string(tableName)),
		FilterExpression:          filter.KeyConditionExpression,
		ExpressionAttributeNames:  filter.ExpressionAttributeNames,
		ExpressionAttributeValues: filter.ExpressionAttributeValues,
	}

	result, err := client.Dynamo.Scan(input)
	if err != nil {
		return nil, err
	}

	records, err := client.ConvertResultValuesToRecords(record.GetTable(), result.Items)
	if err != nil {
		log.Errorf("dynamo.Client: Failed to parse Scan output, %v", err)
		return nil, err
	}
	for result.LastEvaluatedKey != nil {
		input.ExclusiveStartKey = result.LastEvaluatedKey
		result, err = client.Dynamo.Scan(input)
		if err != nil {
			log.Errorf("dynamo.Client: Encountered an error calling dynamo Scan, %v", err)
			return nil, err
		}
		toAppend, err := client.ConvertResultValuesToRecords(record.GetTable(), result.Items)
		if err != nil {
			log.Errorf("dynamo.Client: Failed to parse Scan output, %v", err)
			return nil, err
		}

		records = append(records, toAppend...)
	}

	return records, nil
}

// Query queries Dynamo
func (client *Client) Query(record TableRecord, filter QueryFilter) ([]TableRecord, error) {
	tableName := client.GetFullTableName(record.GetTable())

	startTime := time.Now()
	metricName := fmt.Sprintf("dynamo.%s.query", string(tableName))
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(string(tableName)),
		ExpressionAttributeNames:  filter.ExpressionAttributeNames,
		ExpressionAttributeValues: filter.ExpressionAttributeValues,
		KeyConditionExpression:    filter.KeyConditionExpression,
		FilterExpression:          filter.FilterExpression,
		Limit:                     filter.Limit,
		IndexName:                 filter.IndexName,
	}

	if filter.Descending {
		queryInput.ScanIndexForward = aws.Bool(false)
	}

	result, err := client.Dynamo.Query(queryInput)
	if err != nil {
		log.Errorf("dynamo.Client: Encountered an error calling dynamo Query, %v", err)
		return nil, err
	}

	records, err := client.ConvertResultValuesToRecords(record.GetTable(), result.Items)
	if err != nil {
		log.Errorf("dynamo.Client: Failed to parse Query output, %v", err)
		return nil, err
	}
	for result.LastEvaluatedKey != nil && (filter.Limit == nil || int64(len(records)) < *filter.Limit) {
		queryInput.ExclusiveStartKey = result.LastEvaluatedKey
		result, err = client.Dynamo.Query(queryInput)
		if err != nil {
			log.Errorf("dynamo.Client: Encountered an error calling dynamo Query, %v", err)
			return nil, err
		}
		toAppend, err := client.ConvertResultValuesToRecords(record.GetTable(), result.Items)
		if err != nil {
			log.Errorf("dynamo.Client: Failed to parse Query output, %v", err)
			return nil, err
		}

		records = append(records, toAppend...)
	}

	return records, nil
}

// ConvertResultValuesToRecords converts the Dynamo response to an array of FuelHours records
func (client *Client) ConvertResultValuesToRecords(table Table, attributeValues []map[string]*dynamodb.AttributeValue) ([]TableRecord, error) {
	records := make([]TableRecord, len(attributeValues))

	for i, attributes := range attributeValues {
		toAdd, err := table.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}
		records[i] = toAdd
	}
	return records, nil
}

// PutItem puts the item in the table
func (client *Client) PutItem(record TableRecord) error {
	return client.PutItemWithCondition(record, nil)
}

// PutItemWithCondition puts the item in the table only if the condition string is met
// http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.ConditionExpressions.html
func (client *Client) PutItemWithCondition(record TableRecord, conditionExpression *string) error {
	tableName := client.GetFullTableName(record.GetTable())

	startTime := time.Now()
	metricName := fmt.Sprintf("dynamo.%s.putItemWithCondition", string(tableName))
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	putItemInput := &dynamodb.PutItemInput{
		TableName:           aws.String(string(tableName)),
		Item:                record.NewAttributeMap(),
		ConditionExpression: conditionExpression,
	}

	_, err := client.Dynamo.PutItem(putItemInput)

	return err
}

// BatchWriteItem puts multiple items into a table
func (client *Client) BatchWriteItem(records []TableRecord) error {
	startTime := time.Now()
	metricName := "dynamo.batchWriteItem"
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	writeRequestMap := make(map[string][]*dynamodb.WriteRequest)
	for _, record := range records {
		tableName := string(client.GetFullTableName(record.GetTable()))

		writeRequest := &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: record.NewAttributeMap(),
			},
		}

		writeRequestMap[tableName] = append(writeRequestMap[tableName], writeRequest)
	}

	batchWriteItemInput := &dynamodb.BatchWriteItemInput{
		RequestItems: writeRequestMap,
	}
	_, err := client.Dynamo.BatchWriteItem(batchWriteItemInput)
	if err != nil {
		log.Errorf("dynamo.Client: Encountered an error calling dynamo BatchWriteItem, %v", err)
		return err
	}

	return nil
}

// DeleteItem deletes a record from the table
func (client *Client) DeleteItem(record TableRecord) error {
	tableName := client.GetFullTableName(record.GetTable())

	startTime := time.Now()
	metricName := fmt.Sprintf("dynamo.%s.deleteItem", string(tableName))
	defer client.MetricLogger.LogDurationSinceMetric(metricName, startTime)

	deleteItemInput := &dynamodb.DeleteItemInput{
		TableName: aws.String(string(tableName)),
		Key:       record.NewItemKey(),
	}
	_, err := client.Dynamo.DeleteItem(deleteItemInput)
	return err
}
