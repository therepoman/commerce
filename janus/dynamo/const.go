package dynamo

const (
	keyTypeHash  = "HASH"
	keyTypeRange = "RANGE"

	/*
		OFFER STATES
	*/

	// StatusSuppressed represents the Suppressed status of an asin mapping, meaning the mapping is NOT visible to callers
	StatusSuppressed = "SUPPRESSED"

	// StatusLive represents the Live status of an asin mapping, meaning the mapping is visible to callers
	StatusLive = "LIVE"

	// StatusLive represents the Staff status of an asin mapping, meaning the mapping is visible only to Staff users
	StatusStaff = "STAFF"

	// StatusSuppressed represents the Suppressed status of an asin mapping, meaning the mapping is visible only to whitelisted users
	StatusWhitelist = "WHITELIST"
)
