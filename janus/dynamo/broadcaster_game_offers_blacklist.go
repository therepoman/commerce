package dynamo

import (
	"errors"

	"code.justin.tv/commerce/janus/cache"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	log "github.com/sirupsen/logrus"
)

const (
	BroadcasterIdKey = "broadcasterId"
	GameIdKey        = "gameId"
	BlacklistKey     = "blacklist"
	AllGamesValue    = "ALL"
)

// Table for blacklisting offers (keyed by gameId instead of offer/asin) so that they won't appear on specific broadcaster channels.
type BroadcasterGameOffersBlacklistTable struct{}

// Blacklist metadata struct
type BroadcasterGameOffersBlacklist struct {
	BroadcasterId string
	GameId        string
	Blacklist     bool
}

// BroadcasterGameOffersBlacklist data access object
type BroadcasterGameOffersBlacklistDAO struct {
	BaseDao
	cacheClient cache.IBroadcasterGameOffersBlacklistCacheClient
}

type IBroadcasterGameOffersBlacklistDAO interface {
	Put(gameProductOffer *BroadcasterGameOffersBlacklist) error
	IsBlacklisted(broadcasterId string, gameId string) (bool, error)
	CreateTable() error
}

// Constructor for BroadcasterGameOffersBlacklistDAO
func NewBroadcasterGameOffersBlacklistDAO(client *Client, cacheClient cache.IBroadcasterGameOffersBlacklistCacheClient) IBroadcasterGameOffersBlacklistDAO {
	dao := &BroadcasterGameOffersBlacklistDAO{
		BaseDao{
			Client: client,
			Table:  &BroadcasterGameOffersBlacklistTable{},
		},
		cacheClient,
	}
	return dao
}

/*
	IMPLEMENTED TABLE FUNCTIONS
*/

// GetBaseTableName returns the base table name
func (broadcasterGameOffersBlacklistTable *BroadcasterGameOffersBlacklistTable) GetBaseTableName() TableName {
	return "broadcaster_game_offers_blacklist"
}

// ConvertAttributeMapToRecord converts the Dynamo response to the local data model.
func (broadcasterGameOffersBlacklistTable *BroadcasterGameOffersBlacklistTable) ConvertAttributeMapToRecord(attribute map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	broadcasterId := StringFromAttributes(attribute, BroadcasterIdKey)
	gameId := StringFromAttributes(attribute, GameIdKey)
	blacklist := BoolFromAttributes(attribute, BlacklistKey)

	return &BroadcasterGameOffersBlacklist{
		BroadcasterId: broadcasterId,
		GameId:        gameId,
		Blacklist:     blacklist,
	}, nil
}

/*
	IMPLEMENTED TABLE RECORD FUNCTIONS
*/

// GetTable returns the game product offers table
func (broadcasterGameOffersBlacklist *BroadcasterGameOffersBlacklist) GetTable() Table {
	return &BroadcasterGameOffersBlacklistTable{}
}

// NewItemKey returns the key map to query against in Dynamo
func (blacklist *BroadcasterGameOffersBlacklist) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap[BroadcasterIdKey] = &dynamodb.AttributeValue{S: aws.String(blacklist.BroadcasterId)}
	keyMap[GameIdKey] = &dynamodb.AttributeValue{S: aws.String(blacklist.GameId)}
	return keyMap
}

// NewAttributeMap creates a mapping from BroadcasterGameOffersBlacklist to Dynamo attributes
func (blacklist *BroadcasterGameOffersBlacklist) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attribute := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attribute, BroadcasterIdKey, blacklist.BroadcasterId)
	PopulateAttributesString(attribute, GameIdKey, blacklist.GameId)
	PopulateAttributesBool(attribute, BlacklistKey, blacklist.Blacklist)
	return attribute
}

/*
	IMPLEMENTED CACHE CONVERSION FUNCTIONS
*/

func NewBroadcasterGameOffersBlacklist(cached *cache.BroadcasterGameOffersBlacklist) *BroadcasterGameOffersBlacklist {
	return &BroadcasterGameOffersBlacklist{
		BroadcasterId: cached.BroadcasterId,
		GameId:        cached.GameId,
		Blacklist:     cached.Blacklist,
	}
}

func (blacklist *BroadcasterGameOffersBlacklist) NewCacheBroadcasterGameOffersBlacklist() *cache.BroadcasterGameOffersBlacklist {
	return &cache.BroadcasterGameOffersBlacklist{
		BroadcasterId: blacklist.BroadcasterId,
		GameId:        blacklist.GameId,
		Blacklist:     blacklist.Blacklist,
	}
}

/*
	IMPLEMENTED DAO METHODS
*/

// CreateTable is used by testing and should not be used for table creation
func (dao *BroadcasterGameOffersBlacklistDAO) CreateTable() error {
	tableName := dao.Client.GetFullTableName(dao.Table).String()
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(BroadcasterIdKey),
				KeyType:       aws.String(keyTypeHash),
			},
			{
				AttributeName: aws.String(GameIdKey),
				KeyType:       aws.String(keyTypeRange),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(BroadcasterIdKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(GameIdKey),
				AttributeType: aws.String("S"),
			},
		},
	}
	_, err := dao.Client.Dynamo.CreateTable(&ctr)
	return err
}

// Put adds the BroadcasterGameOffersBlacklist to the database
func (dao *BroadcasterGameOffersBlacklistDAO) Put(blacklist *BroadcasterGameOffersBlacklist) error {
	return dao.Client.PutItem(blacklist)
}

// IsBlacklisted queries Dynamo for the broadcaster and interprets the configuration for the provided game.
func (dao *BroadcasterGameOffersBlacklistDAO) IsBlacklisted(broadcasterId string, gameId string) (bool, error) {
	var contentResult []*BroadcasterGameOffersBlacklist

	cachedBlacklist := dao.cacheClient.GetBlacklist(broadcasterId)
	if cachedBlacklist != nil {
		contentResult = make([]*BroadcasterGameOffersBlacklist, len(cachedBlacklist))
		for i, item := range cachedBlacklist {
			contentResult[i] = NewBroadcasterGameOffersBlacklist(item)
		}
	} else {
		filter := getBroadcasterGameOffersBlacklistQueryFilter(broadcasterId)

		result, err := dao.Client.Query(&BroadcasterGameOffersBlacklist{}, filter)
		if err != nil {
			return true, err
		}

		contentResult = make([]*BroadcasterGameOffersBlacklist, len(result))
		cachedBlacklist = make([]*cache.BroadcasterGameOffersBlacklist, len(result))
		for i, record := range result {
			var isValidObject bool
			contentResult[i], isValidObject = record.(*BroadcasterGameOffersBlacklist)
			if !isValidObject {
				msg := "Encountered an unexpected type while converting dynamo result to BroadcasterGameOffersBlacklist record"
				log.Error(msg)
				return true, errors.New(msg)
			}
			cachedBlacklist[i] = contentResult[i].NewCacheBroadcasterGameOffersBlacklist()
		}

		dao.cacheClient.PutBlacklist(broadcasterId, cachedBlacklist)
	}

	for _, blacklist := range contentResult {
		if (blacklist.GameId == gameId || blacklist.GameId == AllGamesValue) && blacklist.Blacklist {
			return true, nil
		}
	}

	return false, nil
}

func getBroadcasterGameOffersBlacklistQueryFilter(broadcasterId string) QueryFilter {
	expressionAttributeNames := map[string]*string{
		"#ID": aws.String(BroadcasterIdKey),
	}
	expressionAttributeValues := map[string]*dynamodb.AttributeValue{
		":broadcasterId": {S: aws.String(string(broadcasterId))},
	}

	filter := QueryFilter{
		ExpressionAttributeNames:  expressionAttributeNames,
		ExpressionAttributeValues: expressionAttributeValues,
		KeyConditionExpression:    aws.String("#ID = :broadcasterId"),
		Descending:                false,
	}
	return filter
}
