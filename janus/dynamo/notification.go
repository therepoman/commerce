package dynamo

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	log "github.com/sirupsen/logrus"
)

const fuelChatNotificationsQueryLimit = 5

// FuelChatNotificationTable the fuelChatNotification table
type FuelChatNotificationTable struct {
}

// FuelChatNotification represents a consumable chat notification
type FuelChatNotification struct {
	ChannelUser     string
	ChannelID       string
	UserID          string
	Token           string
	ASIN            string
	Status          string
	ProductTitle    string
	ProductImageURL string
	TimeReceived    time.Time
}

// FuelChatNotificationDAO data access object for fuelChatNotification
type FuelChatNotificationDAO struct {
	BaseDao
}

// IFuelChatNotificationDAO interface for the fuelChatNotification DAO
type IFuelChatNotificationDAO interface {
	GetMostRecent(channelID string, userID string) ([]*FuelChatNotification, error)
	Get(channelID string, userID string, token string) (*FuelChatNotification, error)
	Put(fuelChatNotification *FuelChatNotification) error
}

// NewFuelChatNotificationDAO constructor for FuelChatNotificationDAO
func NewFuelChatNotificationDAO(client *Client) IFuelChatNotificationDAO {
	dao := &FuelChatNotificationDAO{BaseDao{
		Client: client,
		Table:  &FuelChatNotificationTable{},
	}}
	return dao
}

// GetBaseTableName returns the base fuelChatNotification table name
func (t *FuelChatNotificationTable) GetBaseTableName() TableName {
	return "fuel_chat_notification"
}

// ConvertAttributeMapToRecord converts one mapping of attributes from the Dynamo repsonse to a FuelChatNotification record
func (t *FuelChatNotificationTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	channelUser := StringFromAttributes(attributeMap, "channelUser")
	token := StringFromAttributes(attributeMap, "token")
	channelID := StringFromAttributes(attributeMap, "channelID")
	userID := StringFromAttributes(attributeMap, "userID")
	asin := StringFromAttributes(attributeMap, "asin")
	status := StringFromAttributes(attributeMap, "status")
	productTitle := StringFromAttributes(attributeMap, "productTitle")
	productImageURL := StringFromAttributes(attributeMap, "productImageURL")

	timeReceived, err := TimeFromAttributes(attributeMap, "timeReceived")
	if err != nil {
		log.Errorf("Failed to convert timeReceived to time")
		return nil, err
	}

	return &FuelChatNotification{
		ChannelUser:     channelUser,
		ChannelID:       channelID,
		UserID:          userID,
		Token:           token,
		ASIN:            asin,
		Status:          status,
		ProductTitle:    productTitle,
		ProductImageURL: productImageURL,
		TimeReceived:    timeReceived,
	}, nil
}

// Get queries Dynamo for a FuelChatNotification matching the provided channelID / userID / token
func (dao *FuelChatNotificationDAO) Get(channelID string, userID string, token string) (*FuelChatNotification, error) {
	var notificationResult *FuelChatNotification

	channelUser := fmt.Sprintf("%s:%s", channelID, userID)
	notificationQuery := &FuelChatNotification{
		ChannelUser: channelUser,
		Token:       token,
	}

	result, err := dao.Client.GetItem(notificationQuery)
	if err != nil {
		return notificationResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	notificationResult, isNotification := result.(*FuelChatNotification)
	if !isNotification {
		log.Errorf("Failed to convert dynamo result to FuelChatNotification")
		return nil, errors.New("Failed to convert dynamo result to FuelChatNotification")
	}

	return notificationResult, nil
}

// Put adds the FuelChatNotification to the database
func (dao *FuelChatNotificationDAO) Put(fuelChatNotification *FuelChatNotification) error {
	return dao.Client.PutItem(fuelChatNotification)
}

// GetMostRecent queries Dynamo for the latest fuelChatNotifications for the provided channelUser
func (dao *FuelChatNotificationDAO) GetMostRecent(channelID string, userID string) ([]*FuelChatNotification, error) {
	channelUser := fmt.Sprintf("%s:%s", channelID, userID)
	filter := getFuelChatNotificationQueryFilter(channelUser)

	fuelChatNotification := &FuelChatNotification{}
	result, err := dao.Client.Query(fuelChatNotification, filter)
	if err != nil {
		return nil, err
	}

	fuelChatNotificationsResult := make([]*FuelChatNotification, len(result))
	for i, record := range result {
		var isFuelChatNotification bool
		fuelChatNotificationsResult[i], isFuelChatNotification = record.(*FuelChatNotification)
		if !isFuelChatNotification {
			return nil, errors.New("Encountered an unexpected type while converting dynamo result to fuelChatNotification record")
		}
	}

	return fuelChatNotificationsResult, nil
}

func getFuelChatNotificationQueryFilter(channelUser string) QueryFilter {
	limit := int64(fuelChatNotificationsQueryLimit)
	indexName := "channelUser-timeReceived-index"

	filter := QueryFilter{
		ExpressionAttributeNames: map[string]*string{
			"#CHANNELUSER": aws.String("channelUser"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":channelUser": &dynamodb.AttributeValue{S: aws.String(channelUser)},
		},
		KeyConditionExpression: aws.String("#CHANNELUSER = :channelUser"),
		IndexName:              &indexName,
		Limit:                  &limit,
		Descending:             true,
	}
	return filter
}

// GetTable returns the fuelChatNotification table
func (n *FuelChatNotification) GetTable() Table {
	return &FuelChatNotificationTable{}
}

// NewItemKey returns the key map to query against in Dynamo
func (n *FuelChatNotification) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["channelUser"] = &dynamodb.AttributeValue{S: aws.String(n.ChannelUser)}
	keyMap["token"] = &dynamodb.AttributeValue{S: aws.String(n.Token)}
	return keyMap
}

// NewAttributeMap creates a mapping from FuelChatNotification to Dynamo attributes
func (n *FuelChatNotification) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, "channelUser", n.ChannelUser)
	PopulateAttributesString(attributeMap, "channelID", n.ChannelID)
	PopulateAttributesString(attributeMap, "userID", n.UserID)
	PopulateAttributesString(attributeMap, "token", n.Token)
	PopulateAttributesString(attributeMap, "asin", n.ASIN)
	PopulateAttributesString(attributeMap, "status", n.Status)
	PopulateAttributesString(attributeMap, "productTitle", n.ProductTitle)
	PopulateAttributesString(attributeMap, "productImageURL", n.ProductImageURL)
	PopulateAttributesTime(attributeMap, "timeReceived", n.TimeReceived)
	return attributeMap
}
