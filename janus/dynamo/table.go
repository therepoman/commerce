package dynamo

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// TableName The name of the table in Dynamo
type TableName string

// BaseTable base Dynamo table interface
type BaseTable interface {
	GetBaseTableName() TableName
}

// Table a Dynamo table
type Table interface {
	BaseTable
	ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error)
}

// TableRecord a Dynamo record
type TableRecord interface {
	GetTable() Table
	NewItemKey() map[string]*dynamodb.AttributeValue
	NewAttributeMap() map[string]*dynamodb.AttributeValue
}

func (this TableName) String() string {
	return string(this)
}
