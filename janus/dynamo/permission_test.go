package dynamo_test

import (
	"testing"

	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_WithNoRecords(t *testing.T) {
	Convey("With Table", t, func() {
		defer go_test_dynamo.Instance().Cleanup()

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewPermissionDAO(testDynamoClient)
		So(dao.CreateTable(), ShouldBeNil)

		Convey("With No Records", func() {

			Convey("GetAll", func() {
				permissions, err := dao.GetAllPermissionsForUser(nil, "tuid")
				So(err, ShouldBeNil)
				So(permissions, ShouldBeEmpty)
			})

			Convey("Get", func() {
				p, err := dao.GetPermission(nil, "tuid", "gameid")

				So(err, ShouldBeNil)
				So(p, ShouldBeNil)
			})
		})

		Convey("With One Record", func() {
			expectedPermissions := []string{"permission"}
			err := dao.AddPermission(nil, "tuid", "gameid", expectedPermissions)
			So(err, ShouldBeNil)

			Convey("GetAll", func() {
				permissions, err := dao.GetAllPermissionsForUser(nil, "tuid")
				expected := []*dynamo.Permission{
					{
						TwitchUserID: "tuid",
						GameID:       "gameid",
						Permissions:  expectedPermissions,
					},
				}
				So(err, ShouldBeNil)
				So(permissions, ShouldResemble, expected)
			})

			Convey("Get", func() {
				p, err := dao.GetPermission(nil, "tuid", "gameid")

				So(err, ShouldBeNil)
				expectedPermission := &dynamo.Permission{
					TwitchUserID: "tuid",
					GameID:       "gameid",
					Permissions:  expectedPermissions,
				}
				So(p, ShouldResemble, expectedPermission)
			})
		})

		Convey("With Different Users", func() {
			expectedPermissions := []string{"permission"}
			err := dao.AddPermission(nil, "tuid", "gameid", expectedPermissions)
			So(err, ShouldBeNil)

			otherExpectedPermissions := []string{"p2"}
			err = dao.AddPermission(nil, "t2", "gameid", otherExpectedPermissions)
			So(err, ShouldBeNil)

			Convey("GetAll", func() {
				expectedPermission := []*dynamo.Permission{
					{
						TwitchUserID: "tuid",
						GameID:       "gameid",
						Permissions:  expectedPermissions,
					},
				}
				permissions, err := dao.GetAllPermissionsForUser(nil, "tuid")
				So(err, ShouldBeNil)
				So(permissions, ShouldResemble, expectedPermission)
			})

			Convey("Get", func() {
				expectedPermission := &dynamo.Permission{
					TwitchUserID: "tuid",
					GameID:       "gameid",
					Permissions:  expectedPermissions,
				}
				permissions, err := dao.GetPermission(nil, "tuid", "gameid")
				So(err, ShouldBeNil)
				So(permissions, ShouldResemble, expectedPermission)
			})
		})
	})
}
