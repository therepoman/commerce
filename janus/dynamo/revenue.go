package dynamo

import (
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	dollarToCentsFactor = 100
)

// BaseRevenueTable the base table for revenue
type BaseRevenueTable struct {
}

// BaseRevenue metadata
type BaseRevenue struct {
	TwitchUserID string
	TimeOfEvent  time.Time
	RevenueCents int64
}

// ConvertAttributeMapToRecord converts one mapping of attributes from the Dynamo repsonse to a FuelRevenue record
func (revenueTable *BaseRevenueTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	tuid := StringFromAttributes(attributeMap, "twitchUserId")
	revShare, err := Float64FromAttributes(attributeMap, "rev_share")
	if err != nil {
		log.Errorf("Failed to convert cents to float64")
		return nil, err
	}
	timeofevent, err := TimeFromAttributes(attributeMap, "timeOfEvent")
	if err != nil {
		log.Errorf("Failed to convert timeOfEvent to time")
		return nil, err
	}

	return &BaseRevenue{
		TwitchUserID: tuid,
		TimeOfEvent:  timeofevent,
		RevenueCents: int64(revShare * dollarToCentsFactor),
	}, nil
}

func getQueryFilter(tuid string, startTime time.Time, endTime time.Time) QueryFilter {
	filter := QueryFilter{
		ExpressionAttributeNames: map[string]*string{
			"#ID":   aws.String("twitchUserId"),
			"#TIME": aws.String("timeOfEvent"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":id":    &dynamodb.AttributeValue{S: aws.String(string(tuid))},
			":start": &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(startTime.UnixNano(), 10))},
			":end":   &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(endTime.UnixNano(), 10))},
		},
		KeyConditionExpression: aws.String("#ID = :id AND #TIME BETWEEN :start AND :end"),
	}
	return filter
}

// GetTable does not return a table for the base revenue struct
func (baseRevenue *BaseRevenue) GetTable() Table {
	return nil
}

// NewItemKey returns the key map to query against in Dynamo
func (baseRevenue *BaseRevenue) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["twitchUserId"] = &dynamodb.AttributeValue{S: aws.String(baseRevenue.TwitchUserID)}
	timeOfEvent := strconv.FormatInt(baseRevenue.TimeOfEvent.UnixNano(), 10)
	keyMap["timeOfEvent"] = &dynamodb.AttributeValue{N: aws.String(timeOfEvent)}
	return keyMap
}

// NewAttributeMap creates a mapping from Amendment to Dynamo attributes
func (baseRevenue *BaseRevenue) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, "twitchuserid", baseRevenue.TwitchUserID)
	PopulateAttributesTime(attributeMap, "timeofevent", baseRevenue.TimeOfEvent)
	PopulateAttributesInt64(attributeMap, "cents", baseRevenue.RevenueCents)
	return attributeMap
}
