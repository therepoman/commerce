package dynamo_test

import (
	"testing"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_BroadcasterGameOffersBlacklist_Success(t *testing.T) {
	Convey("With Table", t, func() {
		defer go_test_dynamo.Instance().Cleanup()
		mockedCacheClient := new(mocks.IBroadcasterGameOffersBlacklistCacheClient)

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewBroadcasterGameOffersBlacklistDAO(testDynamoClient, mockedCacheClient)
		So(dao.CreateTable(), ShouldBeNil)

		Convey("With No Records", func() {
			Convey("IsBlacklisted", func() {
				ExpectCacheMiss(mockedCacheClient, "broadcaster1", 1)
				AssertIsNotBlacklisted(dao, "broadcaster1", "game1")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})

			Convey("IsBlacklisted With Cached Blacklisted Game Record", func() {
				cachedBlacklist := []*cache.BroadcasterGameOffersBlacklist{
					&cache.BroadcasterGameOffersBlacklist{
						BroadcasterId: "broadcaster1",
						GameId:        "game1",
						Blacklist:     true,
					},
				}
				mockedCacheClient.On("GetBlacklist", "broadcaster1").Return(cachedBlacklist).Twice()
				AssertIsBlacklisted(dao, "broadcaster1", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster1", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})

			Convey("IsBlacklisted With Cached Non-Blacklisted Game Record", func() {
				cachedBlacklist := []*cache.BroadcasterGameOffersBlacklist{
					&cache.BroadcasterGameOffersBlacklist{
						BroadcasterId: "broadcaster1",
						GameId:        "game1",
						Blacklist:     false,
					},
				}
				mockedCacheClient.On("GetBlacklist", "broadcaster1").Return(cachedBlacklist).Once()
				AssertIsNotBlacklisted(dao, "broadcaster1", "game1")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})

			Convey("IsBlacklisted With Cached Blacklisted ALL Record", func() {
				cachedBlacklist := []*cache.BroadcasterGameOffersBlacklist{
					&cache.BroadcasterGameOffersBlacklist{
						BroadcasterId: "broadcaster1",
						GameId:        "ALL",
						Blacklist:     true,
					},
				}
				mockedCacheClient.On("GetBlacklist", "broadcaster1").Return(cachedBlacklist).Twice()
				AssertIsBlacklisted(dao, "broadcaster1", "game1")
				AssertIsBlacklisted(dao, "broadcaster1", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})

			Convey("IsBlacklisted With Cached Non-Blacklisted ALL Record", func() {
				cachedBlacklist := []*cache.BroadcasterGameOffersBlacklist{
					&cache.BroadcasterGameOffersBlacklist{
						BroadcasterId: "broadcaster1",
						GameId:        "ALL",
						Blacklist:     false,
					},
				}
				mockedCacheClient.On("GetBlacklist", "broadcaster1").Return(cachedBlacklist).Twice()
				AssertIsNotBlacklisted(dao, "broadcaster1", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster1", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})

			Convey("IsBlacklisted With Cached Blacklisted Game and ALL Record", func() {
				cachedBlacklist := []*cache.BroadcasterGameOffersBlacklist{
					&cache.BroadcasterGameOffersBlacklist{
						BroadcasterId: "broadcaster1",
						GameId:        "ALL",
						Blacklist:     true,
					},
					&cache.BroadcasterGameOffersBlacklist{
						BroadcasterId: "broadcaster1",
						GameId:        "game_id",
						Blacklist:     false,
					},
				}
				mockedCacheClient.On("GetBlacklist", "broadcaster1").Return(cachedBlacklist).Twice()
				AssertIsBlacklisted(dao, "broadcaster1", "game1")
				AssertIsBlacklisted(dao, "broadcaster1", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With Blacklisted Game Record", func() {
			PutBlacklist(dao, "broadcaster1", "game1", true)

			Convey("IsBlacklisted", func() {
				ExpectCacheMiss(mockedCacheClient, "broadcaster1", 2)
				ExpectCacheMiss(mockedCacheClient, "broadcaster2", 2)
				AssertIsBlacklisted(dao, "broadcaster1", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster1", "game2")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With Non-Blacklisted Game Record", func() {
			PutBlacklist(dao, "broadcaster1", "game1", false)

			Convey("IsBlacklisted", func() {
				ExpectCacheMiss(mockedCacheClient, "broadcaster1", 1)
				AssertIsNotBlacklisted(dao, "broadcaster1", "game1")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With Blacklisted ALL Record", func() {
			PutBlacklist(dao, "broadcaster1", "ALL", true)

			Convey("IsBlacklisted", func() {
				ExpectCacheMiss(mockedCacheClient, "broadcaster1", 2)
				ExpectCacheMiss(mockedCacheClient, "broadcaster2", 2)
				AssertIsBlacklisted(dao, "broadcaster1", "game1")
				AssertIsBlacklisted(dao, "broadcaster1", "game2")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With Non-Blacklisted ALL Record", func() {
			PutBlacklist(dao, "broadcaster1", "ALL", false)

			Convey("IsBlacklisted", func() {
				ExpectCacheMiss(mockedCacheClient, "broadcaster1", 2)
				ExpectCacheMiss(mockedCacheClient, "broadcaster2", 2)
				AssertIsNotBlacklisted(dao, "broadcaster1", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster1", "game2")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})

		Convey("With Blacklisted Game and ALL Records", func() {
			PutBlacklist(dao, "broadcaster1", "game1", true)
			PutBlacklist(dao, "broadcaster1", "ALL", true)

			Convey("IsBlacklisted", func() {
				ExpectCacheMiss(mockedCacheClient, "broadcaster1", 2)
				ExpectCacheMiss(mockedCacheClient, "broadcaster2", 2)
				AssertIsBlacklisted(dao, "broadcaster1", "game1")
				AssertIsBlacklisted(dao, "broadcaster1", "game2")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game1")
				AssertIsNotBlacklisted(dao, "broadcaster2", "game2")
				So(mockedCacheClient.AssertExpectations(t), ShouldBeTrue)
			})
		})
	})
}

func PutBlacklist(dao dynamo.IBroadcasterGameOffersBlacklistDAO, broadcasterId string, gameId string, blacklist bool) {
	So(dao.Put(&dynamo.BroadcasterGameOffersBlacklist{
		BroadcasterId: broadcasterId,
		GameId:        gameId,
		Blacklist:     blacklist,
	}), ShouldBeNil)
}

func AssertIsBlacklisted(dao dynamo.IBroadcasterGameOffersBlacklistDAO, broadcasterId string, gameId string) {
	isBlacklisted, err := dao.IsBlacklisted(broadcasterId, gameId)
	So(err, ShouldBeNil)
	So(isBlacklisted, ShouldBeTrue)
}

func AssertIsNotBlacklisted(dao dynamo.IBroadcasterGameOffersBlacklistDAO, broadcasterId string, gameId string) {
	isBlacklisted, err := dao.IsBlacklisted(broadcasterId, gameId)
	So(err, ShouldBeNil)
	So(isBlacklisted, ShouldBeFalse)
}

func ExpectCacheMiss(mockedCacheClient *mocks.IBroadcasterGameOffersBlacklistCacheClient, broadcasterId string, count int) {
	mockedCacheClient.On("GetBlacklist", broadcasterId).Return(nil).Times(count)
	mockedCacheClient.On("PutBlacklist", broadcasterId, mock.Anything).Return().Times(count)
}
