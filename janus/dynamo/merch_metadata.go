package dynamo

import (
	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/pkg/errors"
)

const (
	merchAsinKey     = "asin"
	merchStatusKey   = "status"
	originalPriceKey = "originalPrice"
	fallbackPriceKey = "fallbackPrice"
	isFeaturedKey    = "isFeatured"
	isNewKey         = "isNew"
	isOutOfStockKey  = "isOutOfStock"
	rankKey          = "rank"
	imageHeightKey   = "productImageHeight"
	imageWidthKey    = "productImageWidth"
	imageURLKey      = "productImageURL"
	liveValue        = "LIVE"
)

// MerchandiseMetadataTable the merch metadata map table
type MerchandiseMetadataTable struct{}

// MerchandiseMetadata stores the mapping from merch asin to associated metadata
type MerchandiseMetadata struct {
	Asin               string
	OriginalPrice      float64
	FallbackPrice      float64
	IsFeatured         bool
	IsNew              bool
	IsOutOfStock       bool
	Rank               int64
	ProductImageHeight int64
	ProductImageWidth  int64
	ProductImageURL    string
	Status             string
}

// MerchandiseMetadataDAO data access object for merch metadata mapping
type MerchandiseMetadataDAO struct {
	BaseDao
}

// IMerchandiseMetadataDAO interface for the merch metadata map DAO
type IMerchandiseMetadataDAO interface {
	Get(asin string) (*MerchandiseMetadata, error)
	Put(merchMetadata *MerchandiseMetadata) error
	Scan() ([]*MerchandiseMetadata, error)
	CreateTable() error
}

// NewMerchandiseMetadataDAO constructor for MerchandiseMetadataDAO
func NewMerchandiseMetadataDAO(client *Client) IMerchandiseMetadataDAO {
	dao := &MerchandiseMetadataDAO{BaseDao{
		Client: client,
		Table:  &MerchandiseMetadataTable{},
	}}
	return dao
}

// GetBaseTableName returns the base table name
func (merchMetadataTable *MerchandiseMetadataTable) GetBaseTableName() TableName {
	return "merchandise_metadata"
}

// ConvertAttributeMapToRecord converts the Dynamo response to a merch metadata map
func (merchMetadataTable *MerchandiseMetadataTable) ConvertAttributeMapToRecord(attribute map[string]*dynamodb.AttributeValue) (TableRecord, error) {
	asin := StringFromAttributes(attribute, merchAsinKey)
	status := StringFromAttributes(attribute, merchStatusKey)
	originalPrice, err := Float64FromAttributes(attribute, originalPriceKey)
	if err != nil {
		log.Errorf("Failed to convert originalPrice to float64")
		return nil, err
	}
	fallbackPrice, err := Float64FromAttributes(attribute, fallbackPriceKey)
	if err != nil {
		log.Errorf("Failed to convert fallbackPrice to float64")
		return nil, err
	}
	imageURL := StringFromAttributes(attribute, imageURLKey)
	isFeatured := BoolFromAttributes(attribute, isFeaturedKey)
	isNew := BoolFromAttributes(attribute, isNewKey)
	isOutOfStock := BoolFromAttributes(attribute, isOutOfStockKey)
	rank, err := Int64FromAttributes(attribute, rankKey)
	if err != nil {
		log.Errorf("Failed to convert rank to int64")
		return nil, err
	}
	width, err := Int64FromAttributes(attribute, imageWidthKey)
	if err != nil {
		log.Errorf("Failed to convert width to int64")
		return nil, err
	}
	height, err := Int64FromAttributes(attribute, imageHeightKey)
	if err != nil {
		log.Errorf("Failed to convert height to int64")
		return nil, err
	}

	return &MerchandiseMetadata{
		Asin:               asin,
		OriginalPrice:      originalPrice,
		FallbackPrice:      fallbackPrice,
		IsFeatured:         isFeatured,
		IsNew:              isNew,
		IsOutOfStock:       isOutOfStock,
		ProductImageHeight: height,
		ProductImageWidth:  width,
		ProductImageURL:    imageURL,
		Rank:               rank,
		Status:             status,
	}, nil
}

// GetTable returns the merch metadata map table
func (merchMetadata *MerchandiseMetadata) GetTable() Table {
	return &MerchandiseMetadataTable{}
}

// Get queries Dynamo for a MerchandiseMetadata matching the provided asin
func (dao *MerchandiseMetadataDAO) Get(asin string) (*MerchandiseMetadata, error) {
	var merchMetadataResult *MerchandiseMetadata

	merchMetadataQuery := &MerchandiseMetadata{
		Asin: asin,
	}

	result, err := dao.Client.GetItem(merchMetadataQuery)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	var isMerchandiseMetadata bool
	merchMetadataResult, isMerchandiseMetadata = result.(*MerchandiseMetadata)
	if !isMerchandiseMetadata {
		log.Errorf("Failed to convert dynamo result to MerchandiseMetadata - result: %+v", result)
		return nil, errors.New("Failed to convert dynamo result to MerchandiseMetadata")
	}

	return merchMetadataResult, nil
}

// Put adds the MerchandiseMetadata to the database
func (dao *MerchandiseMetadataDAO) Put(merchMetadata *MerchandiseMetadata) error {
	return dao.Client.PutItem(merchMetadata)
}

func getMerchandiseScanFilter() ScanFilter {
	filter := ScanFilter{
		ExpressionAttributeNames: map[string]*string{
			"#STATUS": aws.String("status"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":status": &dynamodb.AttributeValue{S: aws.String(string(liveValue))},
		},
		KeyConditionExpression: aws.String("#STATUS = :status"),
	}
	return filter
}

// Scan returns filtered items in the table as a list of MerchandiseMetadata
func (dao *MerchandiseMetadataDAO) Scan() ([]*MerchandiseMetadata, error) {
	merchMetadata := &MerchandiseMetadata{}
	filter := getMerchandiseScanFilter()
	result, err := dao.Client.ScanTable(merchMetadata, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	merchMetadataResult := make([]*MerchandiseMetadata, len(result))
	for i, record := range result {
		var isMerchandiseMetadata bool
		merchMetadataResult[i], isMerchandiseMetadata = record.(*MerchandiseMetadata)
		if !isMerchandiseMetadata {
			msg := "Encountered an unexpected type while converting dynamo result to merch metadata record"
			log.Error(msg)
			return merchMetadataResult, errors.New(msg)
		}
	}

	return merchMetadataResult, nil
}

// NewItemKey returns the key map to query against in Dynamo
func (merchMetadata *MerchandiseMetadata) NewItemKey() map[string]*dynamodb.AttributeValue {
	key := make(map[string]*dynamodb.AttributeValue)
	key[merchAsinKey] = &dynamodb.AttributeValue{S: aws.String(merchMetadata.Asin)}
	return key
}

// NewAttributeMap creates a mapping from MerchandiseMetadata to Dynamo attributes
func (merchMetadata *MerchandiseMetadata) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	attribute := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesFloat64(attribute, originalPriceKey, merchMetadata.OriginalPrice, 2)
	PopulateAttributesFloat64(attribute, fallbackPriceKey, merchMetadata.FallbackPrice, 2)
	PopulateAttributesInt64(attribute, rankKey, merchMetadata.Rank)
	PopulateAttributesInt64(attribute, imageHeightKey, merchMetadata.ProductImageHeight)
	PopulateAttributesString(attribute, imageURLKey, merchMetadata.ProductImageURL)
	PopulateAttributesInt64(attribute, imageWidthKey, merchMetadata.ProductImageWidth)
	PopulateAttributesString(attribute, merchAsinKey, merchMetadata.Asin)
	PopulateAttributesString(attribute, merchStatusKey, merchMetadata.Status)
	PopulateAttributesBool(attribute, isFeaturedKey, merchMetadata.IsFeatured)
	PopulateAttributesBool(attribute, isNewKey, merchMetadata.IsNew)
	PopulateAttributesBool(attribute, isOutOfStockKey, merchMetadata.IsOutOfStock)
	return attribute
}

// CreateTable is used by testing and should not be used for table creation
func (merchandiseMetadataDAO *MerchandiseMetadataDAO) CreateTable() error {
	tableName := merchandiseMetadataDAO.Client.GetFullTableName(merchandiseMetadataDAO.Table).String()
	ctr := dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(merchAsinKey),
				KeyType:       aws.String(keyTypeHash),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(merchAsinKey),
				AttributeType: aws.String("S"),
			},
		},
	}
	_, err := merchandiseMetadataDAO.Client.Dynamo.CreateTable(&ctr)
	return err
}
