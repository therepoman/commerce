set -e -x

REGISTRY=docker-registry.dev.us-west2.twitch.tv
REPO=commerce/janus
IMAGE=$REGISTRY/$REPO:$GIT_COMMIT

docker build -f dta/janus.docker -t $IMAGE .
docker push $IMAGE

cat <<EOF > Dockerrun.aws.json
{
    "AWSEBDockerrunVersion": 2,
    "containerDefinitions": [
        {
            "name": "janus",
            "image": "$IMAGE",
            "essential": true,
            "memory": 256,
            "portMappings": [
                {
                    "hostPort": 80,
                    "containerPort": 8000
                }
            ]
        }
    ]
}
EOF

zip app.zip Dockerrun.aws.json
