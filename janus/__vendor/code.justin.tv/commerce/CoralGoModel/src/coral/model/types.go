/* Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package model

import (
	"reflect"
	"strings"
)

type service struct {
	name       string
	assemblies map[string]Assembly
}

type assembly struct {
	shapes map[string]Shape
	ops    map[string]Op
	name   string
}

type shape struct {
	assembly    Assembly           // The assembly this Shape belongs to.
	name        string             // The name of this shape.
	t           reflect.Type       // The Type this shape represents.
	constructor func() interface{} // A function that returns the default value for the shape.
}

type op struct {
	assembly Assembly
	name     string
	input    Shape
	output   Shape
	errs     []Shape
}

func (svc *service) Name() string {
	return svc.name
}

func (asm *assembly) Name() string {
	return asm.name
}

func (s *shape) Assembly() Assembly {
	return s.assembly
}

func (s *shape) Name() string {
	return s.name
}

func (s *shape) FullyQualifiedName() string {
	return strings.Join([]string{s.Assembly().Name(), s.Name()}, "#")
}

func (s *shape) Type() reflect.Type {
	return s.t
}

func (s *shape) New() interface{} {
	return s.constructor()
}

func (o *op) Assembly() Assembly {
	return o.assembly
}

func (o *op) Name() string {
	return o.name
}

func (o *op) Input() Shape {
	return o.input
}

func (o *op) Output() Shape {
	return o.output
}

func (o *op) Errors() []Shape {
	return o.errs
}
