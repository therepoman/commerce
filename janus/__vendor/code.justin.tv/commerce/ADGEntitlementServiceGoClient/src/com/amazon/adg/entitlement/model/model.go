package com_amazon_adg_entitlement_model

import (
	com_amazon_adg_common_model "code.justin.tv/commerce/ADGEntitlementServiceGoClient/src/com/amazon/adg/common/model"
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type ExpireGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _ExpireGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_ExpireGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_ExpireGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ExpireGoodRequest) __type() {
}
func NewExpireGoodRequest() ExpireGoodRequest {
	return &_ExpireGoodRequest{}
}
func init() {
	var val ExpireGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("ExpireGoodRequest", t, func() interface{} {
		return NewExpireGoodRequest()
	})
}

//The client has submitted a request that resulted in an invalid state transition of the digital good. An example
//of this is when a client attempts to consume a digital good more than once.
type LifecycleViolationException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _LifecycleViolationException struct {
	InvalidRequestException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_LifecycleViolationException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_LifecycleViolationException) Message() *string {
	return this.Ị_message
}
func (this *_LifecycleViolationException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_LifecycleViolationException) __type() {
}
func NewLifecycleViolationException() LifecycleViolationException {
	return &_LifecycleViolationException{}
}
func init() {
	var val LifecycleViolationException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("LifecycleViolationException", t, func() interface{} {
		return NewLifecycleViolationException()
	})
}

type ServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _ServiceException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_ServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ServiceException) Message() *string {
	return this.Ị_message
}
func (this *_ServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_ServiceException) __type() {
}
func NewServiceException() ServiceException {
	return &_ServiceException{}
}
func init() {
	var val ServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("ServiceException", t, func() interface{} {
		return NewServiceException()
	})
}

//This exception is thrown on another service or resource this service depends on has an error
type DependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	ServiceException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

//An exception that is thrown when there is a collision over digital good id. This usually means
//the client supplied a UUID for the digital good id and that UUID already exists.
type DigitalGoodExistsException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DigitalGoodExistsException struct {
	InvalidRequestException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DigitalGoodExistsException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DigitalGoodExistsException) Message() *string {
	return this.Ị_message
}
func (this *_DigitalGoodExistsException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DigitalGoodExistsException) __type() {
}
func NewDigitalGoodExistsException() DigitalGoodExistsException {
	return &_DigitalGoodExistsException{}
}
func init() {
	var val DigitalGoodExistsException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("DigitalGoodExistsException", t, func() interface{} {
		return NewDigitalGoodExistsException()
	})
}

type GetGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _GetGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_GetGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_GetGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_GetGoodResponse) __type() {
}
func NewGetGoodResponse() GetGoodResponse {
	return &_GetGoodResponse{}
}
func init() {
	var val GetGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodResponse", t, func() interface{} {
		return NewGetGoodResponse()
	})
}

type VerifyGoodsResponse interface {
	__type()
	SetStatus(v map[string]*string)
	Status() map[string]*string
}
type _VerifyGoodsResponse struct {
	Ị_status map[string]*string `coral:"status" json:"status"`
}

func (this *_VerifyGoodsResponse) Status() map[string]*string {
	return this.Ị_status
}
func (this *_VerifyGoodsResponse) SetStatus(v map[string]*string) {
	this.Ị_status = v
}
func (this *_VerifyGoodsResponse) __type() {
}
func NewVerifyGoodsResponse() VerifyGoodsResponse {
	return &_VerifyGoodsResponse{}
}
func init() {
	var val VerifyGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("VerifyGoodsResponse", t, func() interface{} {
		return NewVerifyGoodsResponse()
	})
}

type ConsumeGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _ConsumeGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_ConsumeGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_ConsumeGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ConsumeGoodResponse) __type() {
}
func NewConsumeGoodResponse() ConsumeGoodResponse {
	return &_ConsumeGoodResponse{}
}
func init() {
	var val ConsumeGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeGoodResponse", t, func() interface{} {
		return NewConsumeGoodResponse()
	})
}

type RestoreGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RestoreGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RestoreGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RestoreGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RestoreGoodResponse) __type() {
}
func NewRestoreGoodResponse() RestoreGoodResponse {
	return &_RestoreGoodResponse{}
}
func init() {
	var val RestoreGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RestoreGoodResponse", t, func() interface{} {
		return NewRestoreGoodResponse()
	})
}

type DeleteGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _DeleteGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_DeleteGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_DeleteGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_DeleteGoodResponse) __type() {
}
func NewDeleteGoodResponse() DeleteGoodResponse {
	return &_DeleteGoodResponse{}
}
func init() {
	var val DeleteGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("DeleteGoodResponse", t, func() interface{} {
		return NewDeleteGoodResponse()
	})
}

type SyncPointData interface {
	__type()
	SetSyncPoint(v *__time__.Time)
	SyncPoint() *__time__.Time
	SetSyncToken(v *string)
	SyncToken() *string
}
type _SyncPointData struct {
	Ị_syncToken *string        `coral:"syncToken" json:"syncToken"`
	Ị_syncPoint *__time__.Time `coral:"syncPoint" json:"syncPoint"`
}

func (this *_SyncPointData) SyncToken() *string {
	return this.Ị_syncToken
}
func (this *_SyncPointData) SetSyncToken(v *string) {
	this.Ị_syncToken = v
}
func (this *_SyncPointData) SyncPoint() *__time__.Time {
	return this.Ị_syncPoint
}
func (this *_SyncPointData) SetSyncPoint(v *__time__.Time) {
	this.Ị_syncPoint = v
}
func (this *_SyncPointData) __type() {
}
func NewSyncPointData() SyncPointData {
	return &_SyncPointData{}
}
func init() {
	var val SyncPointData
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("SyncPointData", t, func() interface{} {
		return NewSyncPointData()
	})
}

type SyncGoodsRequest interface {
	__type()
	SetClient(v com_amazon_adg_common_model.ClientInfo)
	Client() com_amazon_adg_common_model.ClientInfo
	SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo)
	Customer() com_amazon_adg_common_model.AmazonCustomerInfo
	SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs)
	PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetFilters(v []com_amazon_adg_common_model.Filter)
	Filters() []com_amazon_adg_common_model.Filter
	SetStartSync(v SyncPointData)
	StartSync() SyncPointData
}
type _SyncGoodsRequest struct {
	Ị_client          com_amazon_adg_common_model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        com_amazon_adg_common_model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale com_amazon_adg_common_model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                                         `coral:"language" json:"language"`
	Ị_filters         []com_amazon_adg_common_model.Filter            `coral:"filters" json:"filters"`
	Ị_startSync       SyncPointData                                   `coral:"startSync" json:"startSync"`
}

func (this *_SyncGoodsRequest) PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_SyncGoodsRequest) SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_SyncGoodsRequest) Language() *string {
	return this.Ị_language
}
func (this *_SyncGoodsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_SyncGoodsRequest) Client() com_amazon_adg_common_model.ClientInfo {
	return this.Ị_client
}
func (this *_SyncGoodsRequest) SetClient(v com_amazon_adg_common_model.ClientInfo) {
	this.Ị_client = v
}
func (this *_SyncGoodsRequest) Customer() com_amazon_adg_common_model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_SyncGoodsRequest) SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_SyncGoodsRequest) Filters() []com_amazon_adg_common_model.Filter {
	return this.Ị_filters
}
func (this *_SyncGoodsRequest) SetFilters(v []com_amazon_adg_common_model.Filter) {
	this.Ị_filters = v
}
func (this *_SyncGoodsRequest) StartSync() SyncPointData {
	return this.Ị_startSync
}
func (this *_SyncGoodsRequest) SetStartSync(v SyncPointData) {
	this.Ị_startSync = v
}
func (this *_SyncGoodsRequest) __type() {
}
func NewSyncGoodsRequest() SyncGoodsRequest {
	return &_SyncGoodsRequest{}
}
func init() {
	var val SyncGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("SyncGoodsRequest", t, func() interface{} {
		return NewSyncGoodsRequest()
	})
}

type RevokeGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RevokeGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RevokeGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RevokeGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RevokeGoodRequest) __type() {
}
func NewRevokeGoodRequest() RevokeGoodRequest {
	return &_RevokeGoodRequest{}
}
func init() {
	var val RevokeGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RevokeGoodRequest", t, func() interface{} {
		return NewRevokeGoodRequest()
	})
}

type RevokeGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RevokeGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RevokeGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RevokeGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RevokeGoodResponse) __type() {
}
func NewRevokeGoodResponse() RevokeGoodResponse {
	return &_RevokeGoodResponse{}
}
func init() {
	var val RevokeGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RevokeGoodResponse", t, func() interface{} {
		return NewRevokeGoodResponse()
	})
}

type ConsumeGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _ConsumeGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_ConsumeGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_ConsumeGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ConsumeGoodRequest) __type() {
}
func NewConsumeGoodRequest() ConsumeGoodRequest {
	return &_ConsumeGoodRequest{}
}
func init() {
	var val ConsumeGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("ConsumeGoodRequest", t, func() interface{} {
		return NewConsumeGoodRequest()
	})
}

type RestoreGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RestoreGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RestoreGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RestoreGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RestoreGoodRequest) __type() {
}
func NewRestoreGoodRequest() RestoreGoodRequest {
	return &_RestoreGoodRequest{}
}
func init() {
	var val RestoreGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RestoreGoodRequest", t, func() interface{} {
		return NewRestoreGoodRequest()
	})
}

type RefillGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RefillGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RefillGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RefillGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RefillGoodRequest) __type() {
}
func NewRefillGoodRequest() RefillGoodRequest {
	return &_RefillGoodRequest{}
}
func init() {
	var val RefillGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RefillGoodRequest", t, func() interface{} {
		return NewRefillGoodRequest()
	})
}

type DeleteGoodRequest interface {
	__type()
	SetClient(v com_amazon_adg_common_model.ClientInfo)
	Client() com_amazon_adg_common_model.ClientInfo
	SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo)
	Customer() com_amazon_adg_common_model.AmazonCustomerInfo
	SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs)
	PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _DeleteGoodRequest struct {
	Ị_client          com_amazon_adg_common_model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        com_amazon_adg_common_model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale com_amazon_adg_common_model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                                         `coral:"language" json:"language"`
	Ị_good            com_amazon_adg_common_model.DigitalGood         `coral:"good" json:"good"`
}

func (this *_DeleteGoodRequest) Client() com_amazon_adg_common_model.ClientInfo {
	return this.Ị_client
}
func (this *_DeleteGoodRequest) SetClient(v com_amazon_adg_common_model.ClientInfo) {
	this.Ị_client = v
}
func (this *_DeleteGoodRequest) Customer() com_amazon_adg_common_model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_DeleteGoodRequest) SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_DeleteGoodRequest) PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_DeleteGoodRequest) SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_DeleteGoodRequest) Language() *string {
	return this.Ị_language
}
func (this *_DeleteGoodRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_DeleteGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_DeleteGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_DeleteGoodRequest) __type() {
}
func NewDeleteGoodRequest() DeleteGoodRequest {
	return &_DeleteGoodRequest{}
}
func init() {
	var val DeleteGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("DeleteGoodRequest", t, func() interface{} {
		return NewDeleteGoodRequest()
	})
}

type GetGoodRequest interface {
	__type()
	SetClient(v com_amazon_adg_common_model.ClientInfo)
	Client() com_amazon_adg_common_model.ClientInfo
	SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo)
	Customer() com_amazon_adg_common_model.AmazonCustomerInfo
	SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs)
	PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetInstanceId(v *string)
	InstanceId() *string
}
type _GetGoodRequest struct {
	Ị_client          com_amazon_adg_common_model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        com_amazon_adg_common_model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale com_amazon_adg_common_model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                                         `coral:"language" json:"language"`
	Ị_instanceId      *string                                         `coral:"instanceId" json:"instanceId"`
}

func (this *_GetGoodRequest) Client() com_amazon_adg_common_model.ClientInfo {
	return this.Ị_client
}
func (this *_GetGoodRequest) SetClient(v com_amazon_adg_common_model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetGoodRequest) Customer() com_amazon_adg_common_model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetGoodRequest) SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetGoodRequest) PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetGoodRequest) SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetGoodRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetGoodRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetGoodRequest) InstanceId() *string {
	return this.Ị_instanceId
}
func (this *_GetGoodRequest) SetInstanceId(v *string) {
	this.Ị_instanceId = v
}
func (this *_GetGoodRequest) __type() {
}
func NewGetGoodRequest() GetGoodRequest {
	return &_GetGoodRequest{}
}
func init() {
	var val GetGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodRequest", t, func() interface{} {
		return NewGetGoodRequest()
	})
}

type CreateGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _CreateGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_CreateGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_CreateGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_CreateGoodResponse) __type() {
}
func NewCreateGoodResponse() CreateGoodResponse {
	return &_CreateGoodResponse{}
}
func init() {
	var val CreateGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("CreateGoodResponse", t, func() interface{} {
		return NewCreateGoodResponse()
	})
}

type UpdateGoodFulfillmentResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _UpdateGoodFulfillmentResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_UpdateGoodFulfillmentResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_UpdateGoodFulfillmentResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_UpdateGoodFulfillmentResponse) __type() {
}
func NewUpdateGoodFulfillmentResponse() UpdateGoodFulfillmentResponse {
	return &_UpdateGoodFulfillmentResponse{}
}
func init() {
	var val UpdateGoodFulfillmentResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("UpdateGoodFulfillmentResponse", t, func() interface{} {
		return NewUpdateGoodFulfillmentResponse()
	})
}

type RenewGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RenewGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RenewGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RenewGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RenewGoodRequest) __type() {
}
func NewRenewGoodRequest() RenewGoodRequest {
	return &_RenewGoodRequest{}
}
func init() {
	var val RenewGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RenewGoodRequest", t, func() interface{} {
		return NewRenewGoodRequest()
	})
}

type GetGoodsRequest interface {
	__type()
	SetClient(v com_amazon_adg_common_model.ClientInfo)
	Client() com_amazon_adg_common_model.ClientInfo
	SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo)
	Customer() com_amazon_adg_common_model.AmazonCustomerInfo
	SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs)
	PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetFilters(v []com_amazon_adg_common_model.Filter)
	Filters() []com_amazon_adg_common_model.Filter
	SetPagination(v com_amazon_adg_common_model.Pagination)
	Pagination() com_amazon_adg_common_model.Pagination
}
type _GetGoodsRequest struct {
	Ị_client          com_amazon_adg_common_model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        com_amazon_adg_common_model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale com_amazon_adg_common_model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                                         `coral:"language" json:"language"`
	Ị_filters         []com_amazon_adg_common_model.Filter            `coral:"filters" json:"filters"`
	Ị_pagination      com_amazon_adg_common_model.Pagination          `coral:"pagination" json:"pagination"`
}

func (this *_GetGoodsRequest) Customer() com_amazon_adg_common_model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetGoodsRequest) SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetGoodsRequest) PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetGoodsRequest) SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetGoodsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetGoodsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetGoodsRequest) Client() com_amazon_adg_common_model.ClientInfo {
	return this.Ị_client
}
func (this *_GetGoodsRequest) SetClient(v com_amazon_adg_common_model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetGoodsRequest) Filters() []com_amazon_adg_common_model.Filter {
	return this.Ị_filters
}
func (this *_GetGoodsRequest) SetFilters(v []com_amazon_adg_common_model.Filter) {
	this.Ị_filters = v
}
func (this *_GetGoodsRequest) Pagination() com_amazon_adg_common_model.Pagination {
	return this.Ị_pagination
}
func (this *_GetGoodsRequest) SetPagination(v com_amazon_adg_common_model.Pagination) {
	this.Ị_pagination = v
}
func (this *_GetGoodsRequest) __type() {
}
func NewGetGoodsRequest() GetGoodsRequest {
	return &_GetGoodsRequest{}
}
func init() {
	var val GetGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodsRequest", t, func() interface{} {
		return NewGetGoodsRequest()
	})
}

type CreateGoodRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _CreateGoodRequest struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_CreateGoodRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_CreateGoodRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_CreateGoodRequest) __type() {
}
func NewCreateGoodRequest() CreateGoodRequest {
	return &_CreateGoodRequest{}
}
func init() {
	var val CreateGoodRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("CreateGoodRequest", t, func() interface{} {
		return NewCreateGoodRequest()
	})
}

type UpdateGoodFulfillmentRequest interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
	SetFulfillmentState(v *string)
	FulfillmentState() *string
}
type _UpdateGoodFulfillmentRequest struct {
	Ị_good             com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
	Ị_fulfillmentState *string                                 `coral:"fulfillmentState" json:"fulfillmentState"`
}

func (this *_UpdateGoodFulfillmentRequest) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_UpdateGoodFulfillmentRequest) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_UpdateGoodFulfillmentRequest) FulfillmentState() *string {
	return this.Ị_fulfillmentState
}
func (this *_UpdateGoodFulfillmentRequest) SetFulfillmentState(v *string) {
	this.Ị_fulfillmentState = v
}
func (this *_UpdateGoodFulfillmentRequest) __type() {
}
func NewUpdateGoodFulfillmentRequest() UpdateGoodFulfillmentRequest {
	return &_UpdateGoodFulfillmentRequest{}
}
func init() {
	var val UpdateGoodFulfillmentRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("UpdateGoodFulfillmentRequest", t, func() interface{} {
		return NewUpdateGoodFulfillmentRequest()
	})
}

type ExpireGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _ExpireGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_ExpireGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_ExpireGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_ExpireGoodResponse) __type() {
}
func NewExpireGoodResponse() ExpireGoodResponse {
	return &_ExpireGoodResponse{}
}
func init() {
	var val ExpireGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("ExpireGoodResponse", t, func() interface{} {
		return NewExpireGoodResponse()
	})
}

type RenewGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RenewGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RenewGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RenewGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RenewGoodResponse) __type() {
}
func NewRenewGoodResponse() RenewGoodResponse {
	return &_RenewGoodResponse{}
}
func init() {
	var val RenewGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RenewGoodResponse", t, func() interface{} {
		return NewRenewGoodResponse()
	})
}

type SyncGoodsResponse interface {
	__type()
	SetNextSync(v SyncPointData)
	NextSync() SyncPointData
	SetGoods(v []com_amazon_adg_common_model.DigitalGood)
	Goods() []com_amazon_adg_common_model.DigitalGood
}
type _SyncGoodsResponse struct {
	Ị_nextSync SyncPointData                             `coral:"nextSync" json:"nextSync"`
	Ị_goods    []com_amazon_adg_common_model.DigitalGood `coral:"goods" json:"goods"`
}

func (this *_SyncGoodsResponse) NextSync() SyncPointData {
	return this.Ị_nextSync
}
func (this *_SyncGoodsResponse) SetNextSync(v SyncPointData) {
	this.Ị_nextSync = v
}
func (this *_SyncGoodsResponse) Goods() []com_amazon_adg_common_model.DigitalGood {
	return this.Ị_goods
}
func (this *_SyncGoodsResponse) SetGoods(v []com_amazon_adg_common_model.DigitalGood) {
	this.Ị_goods = v
}
func (this *_SyncGoodsResponse) __type() {
}
func NewSyncGoodsResponse() SyncGoodsResponse {
	return &_SyncGoodsResponse{}
}
func init() {
	var val SyncGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("SyncGoodsResponse", t, func() interface{} {
		return NewSyncGoodsResponse()
	})
}

type RefillGoodResponse interface {
	__type()
	SetGood(v com_amazon_adg_common_model.DigitalGood)
	Good() com_amazon_adg_common_model.DigitalGood
}
type _RefillGoodResponse struct {
	Ị_good com_amazon_adg_common_model.DigitalGood `coral:"good" json:"good"`
}

func (this *_RefillGoodResponse) Good() com_amazon_adg_common_model.DigitalGood {
	return this.Ị_good
}
func (this *_RefillGoodResponse) SetGood(v com_amazon_adg_common_model.DigitalGood) {
	this.Ị_good = v
}
func (this *_RefillGoodResponse) __type() {
}
func NewRefillGoodResponse() RefillGoodResponse {
	return &_RefillGoodResponse{}
}
func init() {
	var val RefillGoodResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("RefillGoodResponse", t, func() interface{} {
		return NewRefillGoodResponse()
	})
}

//The client has submitted a request that is invalid, either with bad parameters, missing parameters,
//or unrecognized parameters.
type InvalidRequestException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidRequestException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InvalidRequestException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidRequestException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidRequestException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InvalidRequestException) __type() {
}
func NewInvalidRequestException() InvalidRequestException {
	return &_InvalidRequestException{}
}
func init() {
	var val InvalidRequestException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("InvalidRequestException", t, func() interface{} {
		return NewInvalidRequestException()
	})
}

type GetGoodsResponse interface {
	__type()
	SetGoods(v []com_amazon_adg_common_model.DigitalGood)
	Goods() []com_amazon_adg_common_model.DigitalGood
}
type _GetGoodsResponse struct {
	Ị_goods []com_amazon_adg_common_model.DigitalGood `coral:"goods" json:"goods"`
}

func (this *_GetGoodsResponse) Goods() []com_amazon_adg_common_model.DigitalGood {
	return this.Ị_goods
}
func (this *_GetGoodsResponse) SetGoods(v []com_amazon_adg_common_model.DigitalGood) {
	this.Ị_goods = v
}
func (this *_GetGoodsResponse) __type() {
}
func NewGetGoodsResponse() GetGoodsResponse {
	return &_GetGoodsResponse{}
}
func init() {
	var val GetGoodsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("GetGoodsResponse", t, func() interface{} {
		return NewGetGoodsResponse()
	})
}

type VerifyGoodsRequest interface {
	__type()
	SetClient(v com_amazon_adg_common_model.ClientInfo)
	Client() com_amazon_adg_common_model.ClientInfo
	SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo)
	Customer() com_amazon_adg_common_model.AmazonCustomerInfo
	SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs)
	PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetGoods(v []com_amazon_adg_common_model.DigitalGood)
	Goods() []com_amazon_adg_common_model.DigitalGood
}
type _VerifyGoodsRequest struct {
	Ị_client          com_amazon_adg_common_model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        com_amazon_adg_common_model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale com_amazon_adg_common_model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                                         `coral:"language" json:"language"`
	Ị_goods           []com_amazon_adg_common_model.DigitalGood       `coral:"goods" json:"goods"`
}

func (this *_VerifyGoodsRequest) PreferredLocale() com_amazon_adg_common_model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_VerifyGoodsRequest) SetPreferredLocale(v com_amazon_adg_common_model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_VerifyGoodsRequest) Language() *string {
	return this.Ị_language
}
func (this *_VerifyGoodsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_VerifyGoodsRequest) Client() com_amazon_adg_common_model.ClientInfo {
	return this.Ị_client
}
func (this *_VerifyGoodsRequest) SetClient(v com_amazon_adg_common_model.ClientInfo) {
	this.Ị_client = v
}
func (this *_VerifyGoodsRequest) Customer() com_amazon_adg_common_model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_VerifyGoodsRequest) SetCustomer(v com_amazon_adg_common_model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_VerifyGoodsRequest) Goods() []com_amazon_adg_common_model.DigitalGood {
	return this.Ị_goods
}
func (this *_VerifyGoodsRequest) SetGoods(v []com_amazon_adg_common_model.DigitalGood) {
	this.Ị_goods = v
}
func (this *_VerifyGoodsRequest) __type() {
}
func NewVerifyGoodsRequest() VerifyGoodsRequest {
	return &_VerifyGoodsRequest{}
}
func init() {
	var val VerifyGoodsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("VerifyGoodsRequest", t, func() interface{} {
		return NewVerifyGoodsRequest()
	})
}

//Exception for any hard errors caused by internal failures.
type InternalServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InternalServiceException struct {
	ServiceException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InternalServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InternalServiceException) Message() *string {
	return this.Ị_message
}
func (this *_InternalServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InternalServiceException) __type() {
}
func NewInternalServiceException() InternalServiceException {
	return &_InternalServiceException{}
}
func init() {
	var val InternalServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("InternalServiceException", t, func() interface{} {
		return NewInternalServiceException()
	})
}

//This exception is thrown when the Digital Good being asked for does not exist in the backing database. This
//means that there was likely a problem while creating the DigitalGood
type DigitalGoodDoesNotExistException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DigitalGoodDoesNotExistException struct {
	InvalidRequestException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DigitalGoodDoesNotExistException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DigitalGoodDoesNotExistException) Message() *string {
	return this.Ị_message
}
func (this *_DigitalGoodDoesNotExistException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DigitalGoodDoesNotExistException) __type() {
}
func NewDigitalGoodDoesNotExistException() DigitalGoodDoesNotExistException {
	return &_DigitalGoodDoesNotExistException{}
}
func init() {
	var val DigitalGoodDoesNotExistException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.entitlement.model").RegisterShape("DigitalGoodDoesNotExistException", t, func() interface{} {
		return NewDigitalGoodDoesNotExistException()
	})
}
func init() {
	var val map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*string
		if f, ok := from.Interface().(map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to GoodsStatusMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*string
		if f, ok := from.Interface().(map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*int64
		if f, ok := from.Interface().([]*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//Provides the capability to access, verify, and modify a user's digital goods and entitlements.
type ADGEntitlementService interface { //Retrieves the digital goods for a specific customer.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#getGoods
	GetGoods(input GetGoodsRequest) (GetGoodsResponse, error)          //Verify the  digital goods for a specific customer are present and live.
	VerifyGoods(input VerifyGoodsRequest) (VerifyGoodsResponse, error) //Renew a subscription digital good.  Renewal and expiration are controlled by external life-cycles.
	RenewGood(input RenewGoodRequest) (RenewGoodResponse, error)       //Refill a consumable digital good.  This should be a rare event; refilling a consumableis not a normal life-cycle event.
	RefillGood(input RefillGoodRequest) (RefillGoodResponse, error)    //Deletes a digital good for a customer.  This will mark it for deletion, and later it will
	//be removed from the underlying datastore.
	DeleteGood(input DeleteGoodRequest) (DeleteGoodResponse, error) //Retrieves a digital good given a customerId and a digitalGoodId.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#getGood
	GetGood(input GetGoodRequest) (GetGoodResponse, error) //Retrieves the digital goods for a specific customer from a given sync point
	//or cursor.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#syncGoods
	SyncGoods(input SyncGoodsRequest) (SyncGoodsResponse, error) //Create a new digital good for a specific customer and product.  This API is
	//when there is no fulfillment event such as an order.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#createGood
	CreateGood(input CreateGoodRequest) (CreateGoodResponse, error) //Update the fulfillment state for a digital good.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#updateGoodFulfillment
	UpdateGoodFulfillment(input UpdateGoodFulfillmentRequest) (UpdateGoodFulfillmentResponse, error) //Revoke a digital good.  This should be a rare event; good revocation is not a normal life-cycle event.
	//
	//See documentation at https://w.amazon.com/index.php/ADG/Services/ADGEntitlementService#revokeGood
	RevokeGood(input RevokeGoodRequest) (RevokeGoodResponse, error)    //Consume a consumable digital good. Will fail if the good is already consumed.
	ConsumeGood(input ConsumeGoodRequest) (ConsumeGoodResponse, error) //Expire a subscription digital good.  Expiration and renewal are controlled by external life-cycles.
	ExpireGood(input ExpireGoodRequest) (ExpireGoodResponse, error)
}
