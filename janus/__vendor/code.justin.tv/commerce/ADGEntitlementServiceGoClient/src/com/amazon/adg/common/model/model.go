package com_amazon_adg_common_model

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__big__ "math/big"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to IntegerObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to UUID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodStateEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ECID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductLineEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RenewalPolicy")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CountryOfResidence")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SKU")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TermType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ReceiptId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to MarketplaceId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StoreEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetDetails")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodOriginTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SensitiveString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SensitiveECID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonOrderId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductStateEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ObfuString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonCommonIdentifier")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ComparisonEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DurationUnit")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SensitiveAmazonCommonIdentifier")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TraversalEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to PlanType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Asin")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TransactionTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LanguageCode")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__big__.Rat
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__big__.Rat
		if f, ok := from.Interface().(*__big__.Rat); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BigDecimal")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type Vendor interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
	SetMasVendorId(v *string)
	MasVendorId() *string
	SetMasVendorCode(v *string)
	MasVendorCode() *string
}
type _Vendor struct {
	Ị_name          *string `coral:"name" json:"name"`
	Ị_id            *string `coral:"id" json:"id"`
	Ị_masVendorId   *string `coral:"masVendorId" json:"masVendorId"`
	Ị_masVendorCode *string `coral:"masVendorCode" json:"masVendorCode"`
}

func (this *_Vendor) Id() *string {
	return this.Ị_id
}
func (this *_Vendor) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Vendor) Name() *string {
	return this.Ị_name
}
func (this *_Vendor) SetName(v *string) {
	this.Ị_name = v
}
func (this *_Vendor) MasVendorCode() *string {
	return this.Ị_masVendorCode
}
func (this *_Vendor) SetMasVendorCode(v *string) {
	this.Ị_masVendorCode = v
}
func (this *_Vendor) MasVendorId() *string {
	return this.Ị_masVendorId
}
func (this *_Vendor) SetMasVendorId(v *string) {
	this.Ị_masVendorId = v
}
func (this *_Vendor) __type() {
}
func NewVendor() Vendor {
	return &_Vendor{}
}
func init() {
	var val Vendor
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Vendor", t, func() interface{} {
		return NewVendor()
	})
}

type DigitalGoodTransaction interface {
	__type()
	SetInstanceId(v *string)
	InstanceId() *string
	SetCustomerId(v *string)
	CustomerId() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetHolderId(v *string)
	HolderId() *string
	SetTxId(v *string)
	TxId() *string
	SetTxTimestamp(v *__time__.Time)
	TxTimestamp() *__time__.Time
	SetTxType(v *string)
	TxType() *string
	SetExternalTxId(v *string)
	ExternalTxId() *string
	SetExternalTxType(v *string)
	ExternalTxType() *string
}
type _DigitalGoodTransaction struct {
	Ị_instanceId     *string        `coral:"instanceId" json:"instanceId"`
	Ị_customerId     *string        `coral:"customerId" json:"customerId"`
	Ị_ownerId        *string        `coral:"ownerId" json:"ownerId"`
	Ị_holderId       *string        `coral:"holderId" json:"holderId"`
	Ị_externalTxType *string        `coral:"externalTxType" json:"externalTxType"`
	Ị_txId           *string        `coral:"txId" json:"txId"`
	Ị_txTimestamp    *__time__.Time `coral:"txTimestamp" json:"txTimestamp"`
	Ị_txType         *string        `coral:"txType" json:"txType"`
	Ị_externalTxId   *string        `coral:"externalTxId" json:"externalTxId"`
}

func (this *_DigitalGoodTransaction) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_DigitalGoodTransaction) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_DigitalGoodTransaction) HolderId() *string {
	return this.Ị_holderId
}
func (this *_DigitalGoodTransaction) SetHolderId(v *string) {
	this.Ị_holderId = v
}
func (this *_DigitalGoodTransaction) InstanceId() *string {
	return this.Ị_instanceId
}
func (this *_DigitalGoodTransaction) SetInstanceId(v *string) {
	this.Ị_instanceId = v
}
func (this *_DigitalGoodTransaction) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_DigitalGoodTransaction) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_DigitalGoodTransaction) TxType() *string {
	return this.Ị_txType
}
func (this *_DigitalGoodTransaction) SetTxType(v *string) {
	this.Ị_txType = v
}
func (this *_DigitalGoodTransaction) ExternalTxId() *string {
	return this.Ị_externalTxId
}
func (this *_DigitalGoodTransaction) SetExternalTxId(v *string) {
	this.Ị_externalTxId = v
}
func (this *_DigitalGoodTransaction) ExternalTxType() *string {
	return this.Ị_externalTxType
}
func (this *_DigitalGoodTransaction) SetExternalTxType(v *string) {
	this.Ị_externalTxType = v
}
func (this *_DigitalGoodTransaction) TxId() *string {
	return this.Ị_txId
}
func (this *_DigitalGoodTransaction) SetTxId(v *string) {
	this.Ị_txId = v
}
func (this *_DigitalGoodTransaction) TxTimestamp() *__time__.Time {
	return this.Ị_txTimestamp
}
func (this *_DigitalGoodTransaction) SetTxTimestamp(v *__time__.Time) {
	this.Ị_txTimestamp = v
}
func (this *_DigitalGoodTransaction) __type() {
}
func NewDigitalGoodTransaction() DigitalGoodTransaction {
	return &_DigitalGoodTransaction{}
}
func init() {
	var val DigitalGoodTransaction
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("DigitalGoodTransaction", t, func() interface{} {
		return NewDigitalGoodTransaction()
	})
}

type PagedRequestParameters interface {
	__type()
	SetStartIndex(v *int32)
	StartIndex() *int32
	SetPageSize(v *int32)
	PageSize() *int32
}
type _PagedRequestParameters struct {
	Ị_startIndex *int32 `coral:"startIndex" json:"startIndex"`
	Ị_pageSize   *int32 `coral:"pageSize" json:"pageSize"`
}

func (this *_PagedRequestParameters) StartIndex() *int32 {
	return this.Ị_startIndex
}
func (this *_PagedRequestParameters) SetStartIndex(v *int32) {
	this.Ị_startIndex = v
}
func (this *_PagedRequestParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_PagedRequestParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_PagedRequestParameters) __type() {
}
func NewPagedRequestParameters() PagedRequestParameters {
	return &_PagedRequestParameters{}
}
func init() {
	var val PagedRequestParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("PagedRequestParameters", t, func() interface{} {
		return NewPagedRequestParameters()
	})
}

type Product interface {
	__type()
	SetType(v *string)
	Type() *string
	SetProductLine(v *string)
	ProductLine() *string
	SetId(v *string)
	Id() *string
	SetProductDomain(v Domain)
	ProductDomain() Domain
	SetAsin(v *string)
	Asin() *string
	SetProductTitle(v *string)
	ProductTitle() *string
	SetProductDescription(v *string)
	ProductDescription() *string
	SetOffers(v []Offer)
	Offers() []Offer
	SetProductRevision(v ProductRevision)
	ProductRevision() ProductRevision
	SetVendor(v Vendor)
	Vendor() Vendor
	SetSku(v *string)
	Sku() *string
	SetAsinVersion(v *int32)
	AsinVersion() *int32
	SetState(v *string)
	State() *string
	SetProductDetails(v ProductDetail)
	ProductDetails() ProductDetail
}
type _Product struct {
	Ị_vendor             Vendor          `coral:"vendor" json:"vendor"`
	Ị_sku                *string         `coral:"sku" json:"sku"`
	Ị_asinVersion        *int32          `coral:"asinVersion" json:"asinVersion"`
	Ị_state              *string         `coral:"state" json:"state"`
	Ị_productDetails     ProductDetail   `coral:"productDetails" json:"productDetails"`
	Ị_type               *string         `coral:"type" json:"type"`
	Ị_productLine        *string         `coral:"productLine" json:"productLine"`
	Ị_id                 *string         `coral:"id" json:"id"`
	Ị_productDomain      Domain          `coral:"productDomain" json:"productDomain"`
	Ị_asin               *string         `coral:"asin" json:"asin"`
	Ị_productTitle       *string         `coral:"productTitle" json:"productTitle"`
	Ị_productDescription *string         `coral:"productDescription" json:"productDescription"`
	Ị_offers             []Offer         `coral:"offers" json:"offers"`
	Ị_productRevision    ProductRevision `coral:"productRevision" json:"productRevision"`
}

func (this *_Product) Type() *string {
	return this.Ị_type
}
func (this *_Product) SetType(v *string) {
	this.Ị_type = v
}
func (this *_Product) ProductLine() *string {
	return this.Ị_productLine
}
func (this *_Product) SetProductLine(v *string) {
	this.Ị_productLine = v
}
func (this *_Product) Id() *string {
	return this.Ị_id
}
func (this *_Product) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Product) ProductDomain() Domain {
	return this.Ị_productDomain
}
func (this *_Product) SetProductDomain(v Domain) {
	this.Ị_productDomain = v
}
func (this *_Product) ProductDescription() *string {
	return this.Ị_productDescription
}
func (this *_Product) SetProductDescription(v *string) {
	this.Ị_productDescription = v
}
func (this *_Product) Offers() []Offer {
	return this.Ị_offers
}
func (this *_Product) SetOffers(v []Offer) {
	this.Ị_offers = v
}
func (this *_Product) ProductRevision() ProductRevision {
	return this.Ị_productRevision
}
func (this *_Product) SetProductRevision(v ProductRevision) {
	this.Ị_productRevision = v
}
func (this *_Product) Asin() *string {
	return this.Ị_asin
}
func (this *_Product) SetAsin(v *string) {
	this.Ị_asin = v
}
func (this *_Product) ProductTitle() *string {
	return this.Ị_productTitle
}
func (this *_Product) SetProductTitle(v *string) {
	this.Ị_productTitle = v
}
func (this *_Product) AsinVersion() *int32 {
	return this.Ị_asinVersion
}
func (this *_Product) SetAsinVersion(v *int32) {
	this.Ị_asinVersion = v
}
func (this *_Product) State() *string {
	return this.Ị_state
}
func (this *_Product) SetState(v *string) {
	this.Ị_state = v
}
func (this *_Product) ProductDetails() ProductDetail {
	return this.Ị_productDetails
}
func (this *_Product) SetProductDetails(v ProductDetail) {
	this.Ị_productDetails = v
}
func (this *_Product) Vendor() Vendor {
	return this.Ị_vendor
}
func (this *_Product) SetVendor(v Vendor) {
	this.Ị_vendor = v
}
func (this *_Product) Sku() *string {
	return this.Ị_sku
}
func (this *_Product) SetSku(v *string) {
	this.Ị_sku = v
}
func (this *_Product) __type() {
}
func NewProduct() Product {
	return &_Product{}
}
func init() {
	var val Product
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Product", t, func() interface{} {
		return NewProduct()
	})
}

type BillingPeriod interface {
	__type()
	SetIndex(v *int32)
	Index() *int32
	SetDuration(v Duration)
	Duration() Duration
	SetPrice(v Currency)
	Price() Currency
	SetExecutionTimes(v *int32)
	ExecutionTimes() *int32
	SetRenewalPolicy(v *string)
	RenewalPolicy() *string
}
type _BillingPeriod struct {
	Ị_index          *int32   `coral:"index" json:"index"`
	Ị_duration       Duration `coral:"duration" json:"duration"`
	Ị_price          Currency `coral:"price" json:"price"`
	Ị_executionTimes *int32   `coral:"executionTimes" json:"executionTimes"`
	Ị_renewalPolicy  *string  `coral:"renewalPolicy" json:"renewalPolicy"`
}

func (this *_BillingPeriod) Index() *int32 {
	return this.Ị_index
}
func (this *_BillingPeriod) SetIndex(v *int32) {
	this.Ị_index = v
}
func (this *_BillingPeriod) Duration() Duration {
	return this.Ị_duration
}
func (this *_BillingPeriod) SetDuration(v Duration) {
	this.Ị_duration = v
}
func (this *_BillingPeriod) Price() Currency {
	return this.Ị_price
}
func (this *_BillingPeriod) SetPrice(v Currency) {
	this.Ị_price = v
}
func (this *_BillingPeriod) ExecutionTimes() *int32 {
	return this.Ị_executionTimes
}
func (this *_BillingPeriod) SetExecutionTimes(v *int32) {
	this.Ị_executionTimes = v
}
func (this *_BillingPeriod) RenewalPolicy() *string {
	return this.Ị_renewalPolicy
}
func (this *_BillingPeriod) SetRenewalPolicy(v *string) {
	this.Ị_renewalPolicy = v
}
func (this *_BillingPeriod) __type() {
}
func NewBillingPeriod() BillingPeriod {
	return &_BillingPeriod{}
}
func init() {
	var val BillingPeriod
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("BillingPeriod", t, func() interface{} {
		return NewBillingPeriod()
	})
}

type DomainNode interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
	SetParentDomainId(v *string)
	ParentDomainId() *string
	SetOwner(v ADGEntity)
	Owner() ADGEntity
	SetChildren(v []DomainNode)
	Children() []DomainNode
}
type _DomainNode struct {
	Ị_id             *string      `coral:"id" json:"id"`
	Ị_name           *string      `coral:"name" json:"name"`
	Ị_parentDomainId *string      `coral:"parentDomainId" json:"parentDomainId"`
	Ị_owner          ADGEntity    `coral:"owner" json:"owner"`
	Ị_children       []DomainNode `coral:"children" json:"children"`
}

func (this *_DomainNode) Id() *string {
	return this.Ị_id
}
func (this *_DomainNode) SetId(v *string) {
	this.Ị_id = v
}
func (this *_DomainNode) Name() *string {
	return this.Ị_name
}
func (this *_DomainNode) SetName(v *string) {
	this.Ị_name = v
}
func (this *_DomainNode) ParentDomainId() *string {
	return this.Ị_parentDomainId
}
func (this *_DomainNode) SetParentDomainId(v *string) {
	this.Ị_parentDomainId = v
}
func (this *_DomainNode) Owner() ADGEntity {
	return this.Ị_owner
}
func (this *_DomainNode) SetOwner(v ADGEntity) {
	this.Ị_owner = v
}
func (this *_DomainNode) Children() []DomainNode {
	return this.Ị_children
}
func (this *_DomainNode) SetChildren(v []DomainNode) {
	this.Ị_children = v
}
func (this *_DomainNode) __type() {
}
func NewDomainNode() DomainNode {
	return &_DomainNode{}
}
func init() {
	var val DomainNode
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("DomainNode", t, func() interface{} {
		return NewDomainNode()
	})
}

type DigitalGood interface {
	__type()
	SetId(v *string)
	Id() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetState(v *string)
	State() *string
	SetChannel(v Domain)
	Channel() Domain
	SetModified(v *__time__.Time)
	Modified() *__time__.Time
	SetTransactionState(v *string)
	TransactionState() *string
	SetTransactions(v []DigitalGoodTransaction)
	Transactions() []DigitalGoodTransaction
	SetProduct(v Product)
	Product() Product
	SetCustomerId(v *string)
	CustomerId() *string
	SetReceiptId(v *string)
	ReceiptId() *string
	SetDomain(v Domain)
	Domain() Domain
	SetAdditionalDomains(v []Domain)
	AdditionalDomains() []Domain
	SetOrigin(v DigitalGoodOrigin)
	Origin() DigitalGoodOrigin
	SetHidden(v *bool)
	Hidden() *bool
	SetStoredValue(v *float64)
	StoredValue() *float64
}
type _DigitalGood struct {
	Ị_origin            DigitalGoodOrigin        `coral:"origin" json:"origin"`
	Ị_product           Product                  `coral:"product" json:"product"`
	Ị_customerId        *string                  `coral:"customerId" json:"customerId"`
	Ị_receiptId         *string                  `coral:"receiptId" json:"receiptId"`
	Ị_domain            Domain                   `coral:"domain" json:"domain"`
	Ị_additionalDomains []Domain                 `coral:"additionalDomains" json:"additionalDomains"`
	Ị_transactionState  *string                  `coral:"transactionState" json:"transactionState"`
	Ị_transactions      []DigitalGoodTransaction `coral:"transactions" json:"transactions"`
	Ị_id                *string                  `coral:"id" json:"id"`
	Ị_ownerId           *string                  `coral:"ownerId" json:"ownerId"`
	Ị_state             *string                  `coral:"state" json:"state"`
	Ị_channel           Domain                   `coral:"channel" json:"channel"`
	Ị_modified          *__time__.Time           `coral:"modified" json:"modified"`
	Ị_hidden            *bool                    `coral:"hidden" json:"hidden"`
	Ị_storedValue       *float64                 `coral:"storedValue" json:"storedValue"`
}

func (this *_DigitalGood) ReceiptId() *string {
	return this.Ị_receiptId
}
func (this *_DigitalGood) SetReceiptId(v *string) {
	this.Ị_receiptId = v
}
func (this *_DigitalGood) Domain() Domain {
	return this.Ị_domain
}
func (this *_DigitalGood) SetDomain(v Domain) {
	this.Ị_domain = v
}
func (this *_DigitalGood) AdditionalDomains() []Domain {
	return this.Ị_additionalDomains
}
func (this *_DigitalGood) SetAdditionalDomains(v []Domain) {
	this.Ị_additionalDomains = v
}
func (this *_DigitalGood) Origin() DigitalGoodOrigin {
	return this.Ị_origin
}
func (this *_DigitalGood) SetOrigin(v DigitalGoodOrigin) {
	this.Ị_origin = v
}
func (this *_DigitalGood) Product() Product {
	return this.Ị_product
}
func (this *_DigitalGood) SetProduct(v Product) {
	this.Ị_product = v
}
func (this *_DigitalGood) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_DigitalGood) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_DigitalGood) State() *string {
	return this.Ị_state
}
func (this *_DigitalGood) SetState(v *string) {
	this.Ị_state = v
}
func (this *_DigitalGood) Channel() Domain {
	return this.Ị_channel
}
func (this *_DigitalGood) SetChannel(v Domain) {
	this.Ị_channel = v
}
func (this *_DigitalGood) Modified() *__time__.Time {
	return this.Ị_modified
}
func (this *_DigitalGood) SetModified(v *__time__.Time) {
	this.Ị_modified = v
}
func (this *_DigitalGood) TransactionState() *string {
	return this.Ị_transactionState
}
func (this *_DigitalGood) SetTransactionState(v *string) {
	this.Ị_transactionState = v
}
func (this *_DigitalGood) Transactions() []DigitalGoodTransaction {
	return this.Ị_transactions
}
func (this *_DigitalGood) SetTransactions(v []DigitalGoodTransaction) {
	this.Ị_transactions = v
}
func (this *_DigitalGood) Id() *string {
	return this.Ị_id
}
func (this *_DigitalGood) SetId(v *string) {
	this.Ị_id = v
}
func (this *_DigitalGood) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_DigitalGood) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_DigitalGood) Hidden() *bool {
	return this.Ị_hidden
}
func (this *_DigitalGood) SetHidden(v *bool) {
	this.Ị_hidden = v
}
func (this *_DigitalGood) StoredValue() *float64 {
	return this.Ị_storedValue
}
func (this *_DigitalGood) SetStoredValue(v *float64) {
	this.Ị_storedValue = v
}
func (this *_DigitalGood) __type() {
}
func NewDigitalGood() DigitalGood {
	return &_DigitalGood{}
}
func init() {
	var val DigitalGood
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("DigitalGood", t, func() interface{} {
		return NewDigitalGood()
	})
}

type PagedResponseParameters interface {
	__type()
	SetMoreDataAvailable(v *bool)
	MoreDataAvailable() *bool
	SetNextIndex(v *int64)
	NextIndex() *int64
	SetTotalFound(v *int64)
	TotalFound() *int64
}
type _PagedResponseParameters struct {
	Ị_nextIndex         *int64 `coral:"nextIndex" json:"nextIndex"`
	Ị_totalFound        *int64 `coral:"totalFound" json:"totalFound"`
	Ị_moreDataAvailable *bool  `coral:"moreDataAvailable" json:"moreDataAvailable"`
}

func (this *_PagedResponseParameters) NextIndex() *int64 {
	return this.Ị_nextIndex
}
func (this *_PagedResponseParameters) SetNextIndex(v *int64) {
	this.Ị_nextIndex = v
}
func (this *_PagedResponseParameters) TotalFound() *int64 {
	return this.Ị_totalFound
}
func (this *_PagedResponseParameters) SetTotalFound(v *int64) {
	this.Ị_totalFound = v
}
func (this *_PagedResponseParameters) MoreDataAvailable() *bool {
	return this.Ị_moreDataAvailable
}
func (this *_PagedResponseParameters) SetMoreDataAvailable(v *bool) {
	this.Ị_moreDataAvailable = v
}
func (this *_PagedResponseParameters) __type() {
}
func NewPagedResponseParameters() PagedResponseParameters {
	return &_PagedResponseParameters{}
}
func init() {
	var val PagedResponseParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("PagedResponseParameters", t, func() interface{} {
		return NewPagedResponseParameters()
	})
}

//This Offer class and its child classes are modeled to support Alexa Monetization project.
//More information can be found here w?ADG/Projects/Alexa_Subscriptions/Model
//But below is a quick example which can help people get the idea
//Dropbox is a digital product (it could be a Skill, App, ...)
//Dropbox has 2 subscription tiers which are also products: Dropbox Gold (Tier1) and Dropbox Platinum (Tier2)
//Tier1 (Dropbox Gold) has 2 offers: monthly for $9.99 (Offer1) and annually for $99.99 (Offer2)
//Offer1 ($9.99 monthly of Dropbox Prod) has 2 plans: free trial for 30 days and then $9.99 monthly (Plan1) and regular plan $9.99 monthly (Plan2)
type Offer interface {
	__type()
	SetPlans(v []Plan)
	Plans() []Plan
	SetTerm(v *string)
	Term() *string
	SetSku(v *string)
	Sku() *string
	SetOfferListingId(v *string)
	OfferListingId() *string
}
type _Offer struct {
	Ị_sku            *string `coral:"sku" json:"sku"`
	Ị_offerListingId *string `coral:"offerListingId" json:"offerListingId"`
	Ị_plans          []Plan  `coral:"plans" json:"plans"`
	Ị_term           *string `coral:"term" json:"term"`
}

func (this *_Offer) Sku() *string {
	return this.Ị_sku
}
func (this *_Offer) SetSku(v *string) {
	this.Ị_sku = v
}
func (this *_Offer) OfferListingId() *string {
	return this.Ị_offerListingId
}
func (this *_Offer) SetOfferListingId(v *string) {
	this.Ị_offerListingId = v
}
func (this *_Offer) Plans() []Plan {
	return this.Ị_plans
}
func (this *_Offer) SetPlans(v []Plan) {
	this.Ị_plans = v
}
func (this *_Offer) Term() *string {
	return this.Ị_term
}
func (this *_Offer) SetTerm(v *string) {
	this.Ị_term = v
}
func (this *_Offer) __type() {
}
func NewOffer() Offer {
	return &_Offer{}
}
func init() {
	var val Offer
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Offer", t, func() interface{} {
		return NewOffer()
	})
}

type Duration interface {
	__type()
	SetUnit(v *string)
	Unit() *string
	SetPeriod(v *int32)
	Period() *int32
}
type _Duration struct {
	Ị_period *int32  `coral:"period" json:"period"`
	Ị_unit   *string `coral:"unit" json:"unit"`
}

func (this *_Duration) Unit() *string {
	return this.Ị_unit
}
func (this *_Duration) SetUnit(v *string) {
	this.Ị_unit = v
}
func (this *_Duration) Period() *int32 {
	return this.Ị_period
}
func (this *_Duration) SetPeriod(v *int32) {
	this.Ị_period = v
}
func (this *_Duration) __type() {
}
func NewDuration() Duration {
	return &_Duration{}
}
func init() {
	var val Duration
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Duration", t, func() interface{} {
		return NewDuration()
	})
}

type BaseDigitalGood interface {
	__type()
	SetTransactionState(v *string)
	TransactionState() *string
	SetTransactions(v []DigitalGoodTransaction)
	Transactions() []DigitalGoodTransaction
	SetId(v *string)
	Id() *string
	SetOwnerId(v *string)
	OwnerId() *string
	SetState(v *string)
	State() *string
	SetChannel(v Domain)
	Channel() Domain
	SetModified(v *__time__.Time)
	Modified() *__time__.Time
	SetOrigin(v DigitalGoodOrigin)
	Origin() DigitalGoodOrigin
	SetProduct(v Product)
	Product() Product
	SetCustomerId(v *string)
	CustomerId() *string
	SetReceiptId(v *string)
	ReceiptId() *string
	SetDomain(v Domain)
	Domain() Domain
	SetAdditionalDomains(v []Domain)
	AdditionalDomains() []Domain
}
type _BaseDigitalGood struct {
	Ị_modified          *__time__.Time           `coral:"modified" json:"modified"`
	Ị_transactionState  *string                  `coral:"transactionState" json:"transactionState"`
	Ị_transactions      []DigitalGoodTransaction `coral:"transactions" json:"transactions"`
	Ị_id                *string                  `coral:"id" json:"id"`
	Ị_ownerId           *string                  `coral:"ownerId" json:"ownerId"`
	Ị_state             *string                  `coral:"state" json:"state"`
	Ị_channel           Domain                   `coral:"channel" json:"channel"`
	Ị_additionalDomains []Domain                 `coral:"additionalDomains" json:"additionalDomains"`
	Ị_origin            DigitalGoodOrigin        `coral:"origin" json:"origin"`
	Ị_product           Product                  `coral:"product" json:"product"`
	Ị_customerId        *string                  `coral:"customerId" json:"customerId"`
	Ị_receiptId         *string                  `coral:"receiptId" json:"receiptId"`
	Ị_domain            Domain                   `coral:"domain" json:"domain"`
}

func (this *_BaseDigitalGood) TransactionState() *string {
	return this.Ị_transactionState
}
func (this *_BaseDigitalGood) SetTransactionState(v *string) {
	this.Ị_transactionState = v
}
func (this *_BaseDigitalGood) Transactions() []DigitalGoodTransaction {
	return this.Ị_transactions
}
func (this *_BaseDigitalGood) SetTransactions(v []DigitalGoodTransaction) {
	this.Ị_transactions = v
}
func (this *_BaseDigitalGood) Id() *string {
	return this.Ị_id
}
func (this *_BaseDigitalGood) SetId(v *string) {
	this.Ị_id = v
}
func (this *_BaseDigitalGood) OwnerId() *string {
	return this.Ị_ownerId
}
func (this *_BaseDigitalGood) SetOwnerId(v *string) {
	this.Ị_ownerId = v
}
func (this *_BaseDigitalGood) State() *string {
	return this.Ị_state
}
func (this *_BaseDigitalGood) SetState(v *string) {
	this.Ị_state = v
}
func (this *_BaseDigitalGood) Channel() Domain {
	return this.Ị_channel
}
func (this *_BaseDigitalGood) SetChannel(v Domain) {
	this.Ị_channel = v
}
func (this *_BaseDigitalGood) Modified() *__time__.Time {
	return this.Ị_modified
}
func (this *_BaseDigitalGood) SetModified(v *__time__.Time) {
	this.Ị_modified = v
}
func (this *_BaseDigitalGood) Origin() DigitalGoodOrigin {
	return this.Ị_origin
}
func (this *_BaseDigitalGood) SetOrigin(v DigitalGoodOrigin) {
	this.Ị_origin = v
}
func (this *_BaseDigitalGood) Product() Product {
	return this.Ị_product
}
func (this *_BaseDigitalGood) SetProduct(v Product) {
	this.Ị_product = v
}
func (this *_BaseDigitalGood) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_BaseDigitalGood) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_BaseDigitalGood) ReceiptId() *string {
	return this.Ị_receiptId
}
func (this *_BaseDigitalGood) SetReceiptId(v *string) {
	this.Ị_receiptId = v
}
func (this *_BaseDigitalGood) Domain() Domain {
	return this.Ị_domain
}
func (this *_BaseDigitalGood) SetDomain(v Domain) {
	this.Ị_domain = v
}
func (this *_BaseDigitalGood) AdditionalDomains() []Domain {
	return this.Ị_additionalDomains
}
func (this *_BaseDigitalGood) SetAdditionalDomains(v []Domain) {
	this.Ị_additionalDomains = v
}
func (this *_BaseDigitalGood) __type() {
}
func NewBaseDigitalGood() BaseDigitalGood {
	return &_BaseDigitalGood{}
}
func init() {
	var val BaseDigitalGood
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("BaseDigitalGood", t, func() interface{} {
		return NewBaseDigitalGood()
	})
}

type PagingResponseParameters interface {
	__type()
	SetMoreDataAvailable(v *bool)
	MoreDataAvailable() *bool
	SetPageSize(v *int32)
	PageSize() *int32
	SetNextIndex(v *int32)
	NextIndex() *int32
}
type _PagingResponseParameters struct {
	Ị_nextIndex         *int32 `coral:"nextIndex" json:"nextIndex"`
	Ị_moreDataAvailable *bool  `coral:"moreDataAvailable" json:"moreDataAvailable"`
	Ị_pageSize          *int32 `coral:"pageSize" json:"pageSize"`
}

func (this *_PagingResponseParameters) MoreDataAvailable() *bool {
	return this.Ị_moreDataAvailable
}
func (this *_PagingResponseParameters) SetMoreDataAvailable(v *bool) {
	this.Ị_moreDataAvailable = v
}
func (this *_PagingResponseParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_PagingResponseParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_PagingResponseParameters) NextIndex() *int32 {
	return this.Ị_nextIndex
}
func (this *_PagingResponseParameters) SetNextIndex(v *int32) {
	this.Ị_nextIndex = v
}
func (this *_PagingResponseParameters) __type() {
}
func NewPagingResponseParameters() PagingResponseParameters {
	return &_PagingResponseParameters{}
}
func init() {
	var val PagingResponseParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("PagingResponseParameters", t, func() interface{} {
		return NewPagingResponseParameters()
	})
}

type CursorPagingRequestParameters interface {
	__type()
	SetCursor(v *string)
	Cursor() *string
	SetPageSize(v *int32)
	PageSize() *int32
}
type _CursorPagingRequestParameters struct {
	Ị_cursor   *string `coral:"cursor" json:"cursor"`
	Ị_pageSize *int32  `coral:"pageSize" json:"pageSize"`
}

func (this *_CursorPagingRequestParameters) Cursor() *string {
	return this.Ị_cursor
}
func (this *_CursorPagingRequestParameters) SetCursor(v *string) {
	this.Ị_cursor = v
}
func (this *_CursorPagingRequestParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_CursorPagingRequestParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_CursorPagingRequestParameters) __type() {
}
func NewCursorPagingRequestParameters() CursorPagingRequestParameters {
	return &_CursorPagingRequestParameters{}
}
func init() {
	var val CursorPagingRequestParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("CursorPagingRequestParameters", t, func() interface{} {
		return NewCursorPagingRequestParameters()
	})
}

type ProductComparison interface {
	__type()
	SetProduct(v Product)
	Product() Product
	SetComparison(v *string)
	Comparison() *string
}
type _ProductComparison struct {
	Ị_comparison *string `coral:"comparison" json:"comparison"`
	Ị_product    Product `coral:"product" json:"product"`
}

func (this *_ProductComparison) Comparison() *string {
	return this.Ị_comparison
}
func (this *_ProductComparison) SetComparison(v *string) {
	this.Ị_comparison = v
}
func (this *_ProductComparison) Product() Product {
	return this.Ị_product
}
func (this *_ProductComparison) SetProduct(v Product) {
	this.Ị_product = v
}
func (this *_ProductComparison) __type() {
}
func NewProductComparison() ProductComparison {
	return &_ProductComparison{}
}
func init() {
	var val ProductComparison
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ProductComparison", t, func() interface{} {
		return NewProductComparison()
	})
}

//A key value pair that is passed to downstream services
type KeyValue interface {
	__type()
	SetKey(v *string)
	Key() *string
	SetValue(v *string)
	Value() *string
}
type _KeyValue struct {
	Ị_key   *string `coral:"key" json:"key"`
	Ị_value *string `coral:"value" json:"value"`
}

func (this *_KeyValue) Key() *string {
	return this.Ị_key
}
func (this *_KeyValue) SetKey(v *string) {
	this.Ị_key = v
}
func (this *_KeyValue) Value() *string {
	return this.Ị_value
}
func (this *_KeyValue) SetValue(v *string) {
	this.Ị_value = v
}
func (this *_KeyValue) __type() {
}
func NewKeyValue() KeyValue {
	return &_KeyValue{}
}
func init() {
	var val KeyValue
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("KeyValue", t, func() interface{} {
		return NewKeyValue()
	})
}

type ADGEntity interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
}
type _ADGEntity struct {
	Ị_id   *string `coral:"id" json:"id"`
	Ị_name *string `coral:"name" json:"name"`
}

func (this *_ADGEntity) Name() *string {
	return this.Ị_name
}
func (this *_ADGEntity) SetName(v *string) {
	this.Ị_name = v
}
func (this *_ADGEntity) Id() *string {
	return this.Ị_id
}
func (this *_ADGEntity) SetId(v *string) {
	this.Ị_id = v
}
func (this *_ADGEntity) __type() {
}
func NewADGEntity() ADGEntity {
	return &_ADGEntity{}
}
func init() {
	var val ADGEntity
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ADGEntity", t, func() interface{} {
		return NewADGEntity()
	})
}

//ProductRevision defines a revision of a Product. A product can have many revisions, each in different state of the lifecycle.
//A ProductRevision will contain a reference to the Product, alongwith some additional fields -
//revisionId - Unique identifier
//productId - A reference to the product
//state - Defines whether a revision is Live or Retired or still being published.
//vendorDefinedRevision - Vendor can have their own way of defining revision number. This field helps them navigate through all revisions
//from their perspective.
//assetGroup - AssetGroup is a list of assets that are associated with a revision.
type ProductRevision interface {
	__type()
	SetProductId(v *string)
	ProductId() *string
	SetState(v *string)
	State() *string
	SetVendorDefinedRevision(v *string)
	VendorDefinedRevision() *string
	SetAssetGroup(v AssetGroup)
	AssetGroup() AssetGroup
	SetRevisionId(v *string)
	RevisionId() *string
}
type _ProductRevision struct {
	Ị_state                 *string    `coral:"state" json:"state"`
	Ị_vendorDefinedRevision *string    `coral:"vendorDefinedRevision" json:"vendorDefinedRevision"`
	Ị_assetGroup            AssetGroup `coral:"assetGroup" json:"assetGroup"`
	Ị_revisionId            *string    `coral:"revisionId" json:"revisionId"`
	Ị_productId             *string    `coral:"productId" json:"productId"`
}

func (this *_ProductRevision) RevisionId() *string {
	return this.Ị_revisionId
}
func (this *_ProductRevision) SetRevisionId(v *string) {
	this.Ị_revisionId = v
}
func (this *_ProductRevision) ProductId() *string {
	return this.Ị_productId
}
func (this *_ProductRevision) SetProductId(v *string) {
	this.Ị_productId = v
}
func (this *_ProductRevision) State() *string {
	return this.Ị_state
}
func (this *_ProductRevision) SetState(v *string) {
	this.Ị_state = v
}
func (this *_ProductRevision) VendorDefinedRevision() *string {
	return this.Ị_vendorDefinedRevision
}
func (this *_ProductRevision) SetVendorDefinedRevision(v *string) {
	this.Ị_vendorDefinedRevision = v
}
func (this *_ProductRevision) AssetGroup() AssetGroup {
	return this.Ị_assetGroup
}
func (this *_ProductRevision) SetAssetGroup(v AssetGroup) {
	this.Ị_assetGroup = v
}
func (this *_ProductRevision) __type() {
}
func NewProductRevision() ProductRevision {
	return &_ProductRevision{}
}
func init() {
	var val ProductRevision
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ProductRevision", t, func() interface{} {
		return NewProductRevision()
	})
}

//Asset is any binary, image or video that can be tied to a product to enhance it's value. Usually, it's a downloadable object.
//Note that we are simply storing a url for an Asset and a serialized json string for defining the asset details.
type Asset interface {
	__type()
	SetAssetId(v *string)
	AssetId() *string
	SetAssetUrl(v *string)
	AssetUrl() *string
	SetAssetSize(v *string)
	AssetSize() *string
	SetAssetDetails(v *string)
	AssetDetails() *string
}
type _Asset struct {
	Ị_assetId      *string `coral:"assetId" json:"assetId"`
	Ị_assetUrl     *string `coral:"assetUrl" json:"assetUrl"`
	Ị_assetSize    *string `coral:"assetSize" json:"assetSize"`
	Ị_assetDetails *string `coral:"assetDetails" json:"assetDetails"`
}

func (this *_Asset) AssetSize() *string {
	return this.Ị_assetSize
}
func (this *_Asset) SetAssetSize(v *string) {
	this.Ị_assetSize = v
}
func (this *_Asset) AssetDetails() *string {
	return this.Ị_assetDetails
}
func (this *_Asset) SetAssetDetails(v *string) {
	this.Ị_assetDetails = v
}
func (this *_Asset) AssetId() *string {
	return this.Ị_assetId
}
func (this *_Asset) SetAssetId(v *string) {
	this.Ị_assetId = v
}
func (this *_Asset) AssetUrl() *string {
	return this.Ị_assetUrl
}
func (this *_Asset) SetAssetUrl(v *string) {
	this.Ị_assetUrl = v
}
func (this *_Asset) __type() {
}
func NewAsset() Asset {
	return &_Asset{}
}
func init() {
	var val Asset
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Asset", t, func() interface{} {
		return NewAsset()
	})
}

type CustomerInfo interface {
	__type()
	SetId(v *string)
	Id() *string
	SetFirstName(v *string)
	FirstName() *string
	SetLastName(v *string)
	LastName() *string
}
type _CustomerInfo struct {
	Ị_firstName *string `coral:"firstName" json:"firstName"`
	Ị_lastName  *string `coral:"lastName" json:"lastName"`
	Ị_id        *string `coral:"id" json:"id"`
}

func (this *_CustomerInfo) Id() *string {
	return this.Ị_id
}
func (this *_CustomerInfo) SetId(v *string) {
	this.Ị_id = v
}
func (this *_CustomerInfo) FirstName() *string {
	return this.Ị_firstName
}
func (this *_CustomerInfo) SetFirstName(v *string) {
	this.Ị_firstName = v
}
func (this *_CustomerInfo) LastName() *string {
	return this.Ị_lastName
}
func (this *_CustomerInfo) SetLastName(v *string) {
	this.Ị_lastName = v
}
func (this *_CustomerInfo) __type() {
}
func NewCustomerInfo() CustomerInfo {
	return &_CustomerInfo{}
}
func init() {
	var val CustomerInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("CustomerInfo", t, func() interface{} {
		return NewCustomerInfo()
	})
}

type AmazonCustomerInfo interface {
	__type()
	SetId(v *string)
	Id() *string
	SetFirstName(v *string)
	FirstName() *string
	SetLastName(v *string)
	LastName() *string
	SetShoppingAccountPool(v *string)
	ShoppingAccountPool() *string
	SetDigitalId(v *string)
	DigitalId() *string
	SetDigitalAccountPool(v *string)
	DigitalAccountPool() *string
	SetShoppingId(v *string)
	ShoppingId() *string
}
type _AmazonCustomerInfo struct {
	Ị_lastName            *string `coral:"lastName" json:"lastName"`
	Ị_id                  *string `coral:"id" json:"id"`
	Ị_firstName           *string `coral:"firstName" json:"firstName"`
	Ị_digitalAccountPool  *string `coral:"digitalAccountPool" json:"digitalAccountPool"`
	Ị_shoppingId          *string `coral:"shoppingId" json:"shoppingId"`
	Ị_shoppingAccountPool *string `coral:"shoppingAccountPool" json:"shoppingAccountPool"`
	Ị_digitalId           *string `coral:"digitalId" json:"digitalId"`
}

func (this *_AmazonCustomerInfo) Id() *string {
	return this.Ị_id
}
func (this *_AmazonCustomerInfo) SetId(v *string) {
	this.Ị_id = v
}
func (this *_AmazonCustomerInfo) FirstName() *string {
	return this.Ị_firstName
}
func (this *_AmazonCustomerInfo) SetFirstName(v *string) {
	this.Ị_firstName = v
}
func (this *_AmazonCustomerInfo) LastName() *string {
	return this.Ị_lastName
}
func (this *_AmazonCustomerInfo) SetLastName(v *string) {
	this.Ị_lastName = v
}
func (this *_AmazonCustomerInfo) ShoppingId() *string {
	return this.Ị_shoppingId
}
func (this *_AmazonCustomerInfo) SetShoppingId(v *string) {
	this.Ị_shoppingId = v
}
func (this *_AmazonCustomerInfo) ShoppingAccountPool() *string {
	return this.Ị_shoppingAccountPool
}
func (this *_AmazonCustomerInfo) SetShoppingAccountPool(v *string) {
	this.Ị_shoppingAccountPool = v
}
func (this *_AmazonCustomerInfo) DigitalId() *string {
	return this.Ị_digitalId
}
func (this *_AmazonCustomerInfo) SetDigitalId(v *string) {
	this.Ị_digitalId = v
}
func (this *_AmazonCustomerInfo) DigitalAccountPool() *string {
	return this.Ị_digitalAccountPool
}
func (this *_AmazonCustomerInfo) SetDigitalAccountPool(v *string) {
	this.Ị_digitalAccountPool = v
}
func (this *_AmazonCustomerInfo) __type() {
}
func NewAmazonCustomerInfo() AmazonCustomerInfo {
	return &_AmazonCustomerInfo{}
}
func init() {
	var val AmazonCustomerInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("AmazonCustomerInfo", t, func() interface{} {
		return NewAmazonCustomerInfo()
	})
}

type Pagination interface {
	__type()
	SetIndexPaging(v PagingRequestParameters)
	IndexPaging() PagingRequestParameters
	SetCursorPaging(v CursorPagingRequestParameters)
	CursorPaging() CursorPagingRequestParameters
}
type _Pagination struct {
	Ị_indexPaging  PagingRequestParameters       `coral:"indexPaging" json:"indexPaging"`
	Ị_cursorPaging CursorPagingRequestParameters `coral:"cursorPaging" json:"cursorPaging"`
}

func (this *_Pagination) IndexPaging() PagingRequestParameters {
	return this.Ị_indexPaging
}
func (this *_Pagination) SetIndexPaging(v PagingRequestParameters) {
	this.Ị_indexPaging = v
}
func (this *_Pagination) CursorPaging() CursorPagingRequestParameters {
	return this.Ị_cursorPaging
}
func (this *_Pagination) SetCursorPaging(v CursorPagingRequestParameters) {
	this.Ị_cursorPaging = v
}
func (this *_Pagination) __type() {
}
func NewPagination() Pagination {
	return &_Pagination{}
}
func init() {
	var val Pagination
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Pagination", t, func() interface{} {
		return NewPagination()
	})
}

type ClientInfo interface {
	__type()
	SetHeaders(v []KeyValue)
	Headers() []KeyValue
	SetDomains(v []Domain)
	Domains() []Domain
	SetClientId(v *string)
	ClientId() *string
	SetClientVersion(v *string)
	ClientVersion() *string
	SetClientType(v *string)
	ClientType() *string
	SetProperties(v map[string]*string)
	Properties() map[string]*string
}
type _ClientInfo struct {
	Ị_clientId      *string            `coral:"clientId" json:"clientId"`
	Ị_clientVersion *string            `coral:"clientVersion" json:"clientVersion"`
	Ị_clientType    *string            `coral:"clientType" json:"clientType"`
	Ị_properties    map[string]*string `coral:"properties" json:"properties"`
	Ị_headers       []KeyValue         `coral:"headers" json:"headers"`
	Ị_domains       []Domain           `coral:"domains" json:"domains"`
}

func (this *_ClientInfo) ClientType() *string {
	return this.Ị_clientType
}
func (this *_ClientInfo) SetClientType(v *string) {
	this.Ị_clientType = v
}
func (this *_ClientInfo) Properties() map[string]*string {
	return this.Ị_properties
}
func (this *_ClientInfo) SetProperties(v map[string]*string) {
	this.Ị_properties = v
}
func (this *_ClientInfo) Headers() []KeyValue {
	return this.Ị_headers
}
func (this *_ClientInfo) SetHeaders(v []KeyValue) {
	this.Ị_headers = v
}
func (this *_ClientInfo) Domains() []Domain {
	return this.Ị_domains
}
func (this *_ClientInfo) SetDomains(v []Domain) {
	this.Ị_domains = v
}
func (this *_ClientInfo) ClientId() *string {
	return this.Ị_clientId
}
func (this *_ClientInfo) SetClientId(v *string) {
	this.Ị_clientId = v
}
func (this *_ClientInfo) ClientVersion() *string {
	return this.Ị_clientVersion
}
func (this *_ClientInfo) SetClientVersion(v *string) {
	this.Ị_clientVersion = v
}
func (this *_ClientInfo) __type() {
}
func NewClientInfo() ClientInfo {
	return &_ClientInfo{}
}
func init() {
	var val ClientInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ClientInfo", t, func() interface{} {
		return NewClientInfo()
	})
}

//AssetGroup is a list of Assets which are closely associated to each other, for example, a set of assets that belong to a revision.
type AssetGroup interface {
	__type()
	SetAssetGroupId(v *string)
	AssetGroupId() *string
	SetAssets(v []Asset)
	Assets() []Asset
}
type _AssetGroup struct {
	Ị_assetGroupId *string `coral:"assetGroupId" json:"assetGroupId"`
	Ị_assets       []Asset `coral:"assets" json:"assets"`
}

func (this *_AssetGroup) AssetGroupId() *string {
	return this.Ị_assetGroupId
}
func (this *_AssetGroup) SetAssetGroupId(v *string) {
	this.Ị_assetGroupId = v
}
func (this *_AssetGroup) Assets() []Asset {
	return this.Ị_assets
}
func (this *_AssetGroup) SetAssets(v []Asset) {
	this.Ị_assets = v
}
func (this *_AssetGroup) __type() {
}
func NewAssetGroup() AssetGroup {
	return &_AssetGroup{}
}
func init() {
	var val AssetGroup
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("AssetGroup", t, func() interface{} {
		return NewAssetGroup()
	})
}

type Plan interface {
	__type()
	SetType(v *string)
	Type() *string
	SetBillingPeriods(v []BillingPeriod)
	BillingPeriods() []BillingPeriod
}
type _Plan struct {
	Ị_type           *string         `coral:"type" json:"type"`
	Ị_billingPeriods []BillingPeriod `coral:"billingPeriods" json:"billingPeriods"`
}

func (this *_Plan) Type() *string {
	return this.Ị_type
}
func (this *_Plan) SetType(v *string) {
	this.Ị_type = v
}
func (this *_Plan) BillingPeriods() []BillingPeriod {
	return this.Ị_billingPeriods
}
func (this *_Plan) SetBillingPeriods(v []BillingPeriod) {
	this.Ị_billingPeriods = v
}
func (this *_Plan) __type() {
}
func NewPlan() Plan {
	return &_Plan{}
}
func init() {
	var val Plan
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Plan", t, func() interface{} {
		return NewPlan()
	})
}

type ProductDetail interface {
	__type()
	SetProductIconUrl(v *string)
	ProductIconUrl() *string
	SetListPrice(v Currency)
	ListPrice() Currency
	SetLatestProductVersion(v *string)
	LatestProductVersion() *string
	SetCategory(v *string)
	Category() *string
	SetCurrentPrice(v Currency)
	CurrentPrice() Currency
	SetCompatibleProductVersion(v *string)
	CompatibleProductVersion() *string
	SetIsCompatible(v *bool)
	IsCompatible() *bool
	SetIsPurchasable(v *bool)
	IsPurchasable() *bool
	SetDetails(v map[string]map[string]*string)
	Details() map[string]map[string]*string
}
type _ProductDetail struct {
	Ị_productIconUrl           *string                       `coral:"productIconUrl" json:"productIconUrl"`
	Ị_listPrice                Currency                      `coral:"listPrice" json:"listPrice"`
	Ị_latestProductVersion     *string                       `coral:"latestProductVersion" json:"latestProductVersion"`
	Ị_category                 *string                       `coral:"category" json:"category"`
	Ị_currentPrice             Currency                      `coral:"currentPrice" json:"currentPrice"`
	Ị_compatibleProductVersion *string                       `coral:"compatibleProductVersion" json:"compatibleProductVersion"`
	Ị_isCompatible             *bool                         `coral:"isCompatible" json:"isCompatible"`
	Ị_isPurchasable            *bool                         `coral:"isPurchasable" json:"isPurchasable"`
	Ị_details                  map[string]map[string]*string `coral:"details" json:"details"`
}

func (this *_ProductDetail) CompatibleProductVersion() *string {
	return this.Ị_compatibleProductVersion
}
func (this *_ProductDetail) SetCompatibleProductVersion(v *string) {
	this.Ị_compatibleProductVersion = v
}
func (this *_ProductDetail) IsCompatible() *bool {
	return this.Ị_isCompatible
}
func (this *_ProductDetail) SetIsCompatible(v *bool) {
	this.Ị_isCompatible = v
}
func (this *_ProductDetail) IsPurchasable() *bool {
	return this.Ị_isPurchasable
}
func (this *_ProductDetail) SetIsPurchasable(v *bool) {
	this.Ị_isPurchasable = v
}
func (this *_ProductDetail) Details() map[string]map[string]*string {
	return this.Ị_details
}
func (this *_ProductDetail) SetDetails(v map[string]map[string]*string) {
	this.Ị_details = v
}
func (this *_ProductDetail) Category() *string {
	return this.Ị_category
}
func (this *_ProductDetail) SetCategory(v *string) {
	this.Ị_category = v
}
func (this *_ProductDetail) CurrentPrice() Currency {
	return this.Ị_currentPrice
}
func (this *_ProductDetail) SetCurrentPrice(v Currency) {
	this.Ị_currentPrice = v
}
func (this *_ProductDetail) LatestProductVersion() *string {
	return this.Ị_latestProductVersion
}
func (this *_ProductDetail) SetLatestProductVersion(v *string) {
	this.Ị_latestProductVersion = v
}
func (this *_ProductDetail) ProductIconUrl() *string {
	return this.Ị_productIconUrl
}
func (this *_ProductDetail) SetProductIconUrl(v *string) {
	this.Ị_productIconUrl = v
}
func (this *_ProductDetail) ListPrice() Currency {
	return this.Ị_listPrice
}
func (this *_ProductDetail) SetListPrice(v Currency) {
	this.Ị_listPrice = v
}
func (this *_ProductDetail) __type() {
}
func NewProductDetail() ProductDetail {
	return &_ProductDetail{}
}
func init() {
	var val ProductDetail
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ProductDetail", t, func() interface{} {
		return NewProductDetail()
	})
}

type DigitalGoodOrigin interface {
	__type()
	SetOriginType(v *string)
	OriginType() *string
	SetOriginId(v *string)
	OriginId() *string
	SetOriginTimestamp(v *__time__.Time)
	OriginTimestamp() *__time__.Time
}
type _DigitalGoodOrigin struct {
	Ị_originId        *string        `coral:"originId" json:"originId"`
	Ị_originTimestamp *__time__.Time `coral:"originTimestamp" json:"originTimestamp"`
	Ị_originType      *string        `coral:"originType" json:"originType"`
}

func (this *_DigitalGoodOrigin) OriginId() *string {
	return this.Ị_originId
}
func (this *_DigitalGoodOrigin) SetOriginId(v *string) {
	this.Ị_originId = v
}
func (this *_DigitalGoodOrigin) OriginTimestamp() *__time__.Time {
	return this.Ị_originTimestamp
}
func (this *_DigitalGoodOrigin) SetOriginTimestamp(v *__time__.Time) {
	this.Ị_originTimestamp = v
}
func (this *_DigitalGoodOrigin) OriginType() *string {
	return this.Ị_originType
}
func (this *_DigitalGoodOrigin) SetOriginType(v *string) {
	this.Ị_originType = v
}
func (this *_DigitalGoodOrigin) __type() {
}
func NewDigitalGoodOrigin() DigitalGoodOrigin {
	return &_DigitalGoodOrigin{}
}
func init() {
	var val DigitalGoodOrigin
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("DigitalGoodOrigin", t, func() interface{} {
		return NewDigitalGoodOrigin()
	})
}

type ClientRequest interface {
	__type()
	SetClient(v ClientInfo)
	Client() ClientInfo
	SetCustomer(v AmazonCustomerInfo)
	Customer() AmazonCustomerInfo
	SetPreferredLocale(v CustomerLocalePrefs)
	PreferredLocale() CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
}
type _ClientRequest struct {
	Ị_client          ClientInfo          `coral:"client" json:"client"`
	Ị_customer        AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string             `coral:"language" json:"language"`
}

func (this *_ClientRequest) Customer() AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_ClientRequest) SetCustomer(v AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_ClientRequest) PreferredLocale() CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_ClientRequest) SetPreferredLocale(v CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_ClientRequest) Language() *string {
	return this.Ị_language
}
func (this *_ClientRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_ClientRequest) Client() ClientInfo {
	return this.Ị_client
}
func (this *_ClientRequest) SetClient(v ClientInfo) {
	this.Ị_client = v
}
func (this *_ClientRequest) __type() {
}
func NewClientRequest() ClientRequest {
	return &_ClientRequest{}
}
func init() {
	var val ClientRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ClientRequest", t, func() interface{} {
		return NewClientRequest()
	})
}

type PagingRequestParameters interface {
	__type()
	SetStartIndex(v *int32)
	StartIndex() *int32
	SetPageSize(v *int32)
	PageSize() *int32
}
type _PagingRequestParameters struct {
	Ị_startIndex *int32 `coral:"startIndex" json:"startIndex"`
	Ị_pageSize   *int32 `coral:"pageSize" json:"pageSize"`
}

func (this *_PagingRequestParameters) StartIndex() *int32 {
	return this.Ị_startIndex
}
func (this *_PagingRequestParameters) SetStartIndex(v *int32) {
	this.Ị_startIndex = v
}
func (this *_PagingRequestParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_PagingRequestParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_PagingRequestParameters) __type() {
}
func NewPagingRequestParameters() PagingRequestParameters {
	return &_PagingRequestParameters{}
}
func init() {
	var val PagingRequestParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("PagingRequestParameters", t, func() interface{} {
		return NewPagingRequestParameters()
	})
}

type Domain interface {
	__type()
	SetId(v *string)
	Id() *string
	SetName(v *string)
	Name() *string
	SetParentDomainId(v *string)
	ParentDomainId() *string
	SetOwner(v ADGEntity)
	Owner() ADGEntity
}
type _Domain struct {
	Ị_name           *string   `coral:"name" json:"name"`
	Ị_parentDomainId *string   `coral:"parentDomainId" json:"parentDomainId"`
	Ị_owner          ADGEntity `coral:"owner" json:"owner"`
	Ị_id             *string   `coral:"id" json:"id"`
}

func (this *_Domain) Id() *string {
	return this.Ị_id
}
func (this *_Domain) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Domain) Name() *string {
	return this.Ị_name
}
func (this *_Domain) SetName(v *string) {
	this.Ị_name = v
}
func (this *_Domain) ParentDomainId() *string {
	return this.Ị_parentDomainId
}
func (this *_Domain) SetParentDomainId(v *string) {
	this.Ị_parentDomainId = v
}
func (this *_Domain) Owner() ADGEntity {
	return this.Ị_owner
}
func (this *_Domain) SetOwner(v ADGEntity) {
	this.Ị_owner = v
}
func (this *_Domain) __type() {
}
func NewDomain() Domain {
	return &_Domain{}
}
func init() {
	var val Domain
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Domain", t, func() interface{} {
		return NewDomain()
	})
}

type Currency interface {
	__type()
	SetAmount(v *__big__.Rat)
	Amount() *__big__.Rat
	SetUnit(v *string)
	Unit() *string
}
type _Currency struct {
	Ị_unit   *string      `coral:"unit" json:"unit"`
	Ị_amount *__big__.Rat `coral:"amount" json:"amount"`
}

func (this *_Currency) Amount() *__big__.Rat {
	return this.Ị_amount
}
func (this *_Currency) SetAmount(v *__big__.Rat) {
	this.Ị_amount = v
}
func (this *_Currency) Unit() *string {
	return this.Ị_unit
}
func (this *_Currency) SetUnit(v *string) {
	this.Ị_unit = v
}
func (this *_Currency) __type() {
}
func NewCurrency() Currency {
	return &_Currency{}
}
func init() {
	var val Currency
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Currency", t, func() interface{} {
		return NewCurrency()
	})
}

type CustomerLocalePrefs interface {
	__type()
	SetPfm(v *string)
	Pfm() *string
	SetCor(v *string)
	Cor() *string
}
type _CustomerLocalePrefs struct {
	Ị_cor *string `coral:"cor" json:"cor"`
	Ị_pfm *string `coral:"pfm" json:"pfm"`
}

func (this *_CustomerLocalePrefs) Pfm() *string {
	return this.Ị_pfm
}
func (this *_CustomerLocalePrefs) SetPfm(v *string) {
	this.Ị_pfm = v
}
func (this *_CustomerLocalePrefs) Cor() *string {
	return this.Ị_cor
}
func (this *_CustomerLocalePrefs) SetCor(v *string) {
	this.Ị_cor = v
}
func (this *_CustomerLocalePrefs) __type() {
}
func NewCustomerLocalePrefs() CustomerLocalePrefs {
	return &_CustomerLocalePrefs{}
}
func init() {
	var val CustomerLocalePrefs
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("CustomerLocalePrefs", t, func() interface{} {
		return NewCustomerLocalePrefs()
	})
}

type Filter interface {
	__type()
	SetName(v *string)
	Name() *string
	SetInput(v *string)
	Input() *string
}
type _Filter struct {
	Ị_name  *string `coral:"name" json:"name"`
	Ị_input *string `coral:"input" json:"input"`
}

func (this *_Filter) Name() *string {
	return this.Ị_name
}
func (this *_Filter) SetName(v *string) {
	this.Ị_name = v
}
func (this *_Filter) Input() *string {
	return this.Ị_input
}
func (this *_Filter) SetInput(v *string) {
	this.Ị_input = v
}
func (this *_Filter) __type() {
}
func NewFilter() Filter {
	return &_Filter{}
}
func init() {
	var val Filter
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("Filter", t, func() interface{} {
		return NewFilter()
	})
}

type ProductRule interface {
	__type()
	SetAcceptIfPassesAny(v []ProductComparison)
	AcceptIfPassesAny() []ProductComparison
}
type _ProductRule struct {
	Ị_acceptIfPassesAny []ProductComparison `coral:"acceptIfPassesAny" json:"acceptIfPassesAny"`
}

func (this *_ProductRule) AcceptIfPassesAny() []ProductComparison {
	return this.Ị_acceptIfPassesAny
}
func (this *_ProductRule) SetAcceptIfPassesAny(v []ProductComparison) {
	this.Ị_acceptIfPassesAny = v
}
func (this *_ProductRule) __type() {
}
func NewProductRule() ProductRule {
	return &_ProductRule{}
}
func init() {
	var val ProductRule
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ProductRule", t, func() interface{} {
		return NewProductRule()
	})
}

type ProductFilter interface {
	__type()
	SetAcceptIfPassesAll(v []ProductRule)
	AcceptIfPassesAll() []ProductRule
}
type _ProductFilter struct {
	Ị_acceptIfPassesAll []ProductRule `coral:"acceptIfPassesAll" json:"acceptIfPassesAll"`
}

func (this *_ProductFilter) AcceptIfPassesAll() []ProductRule {
	return this.Ị_acceptIfPassesAll
}
func (this *_ProductFilter) SetAcceptIfPassesAll(v []ProductRule) {
	this.Ị_acceptIfPassesAll = v
}
func (this *_ProductFilter) __type() {
}
func NewProductFilter() ProductFilter {
	return &_ProductFilter{}
}
func init() {
	var val ProductFilter
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("ProductFilter", t, func() interface{} {
		return NewProductFilter()
	})
}

type CursorPagingResponseParameters interface {
	__type()
	SetMoreDataAvailable(v *bool)
	MoreDataAvailable() *bool
	SetPageSize(v *int32)
	PageSize() *int32
	SetNextCursor(v *string)
	NextCursor() *string
}
type _CursorPagingResponseParameters struct {
	Ị_pageSize          *int32  `coral:"pageSize" json:"pageSize"`
	Ị_nextCursor        *string `coral:"nextCursor" json:"nextCursor"`
	Ị_moreDataAvailable *bool   `coral:"moreDataAvailable" json:"moreDataAvailable"`
}

func (this *_CursorPagingResponseParameters) NextCursor() *string {
	return this.Ị_nextCursor
}
func (this *_CursorPagingResponseParameters) SetNextCursor(v *string) {
	this.Ị_nextCursor = v
}
func (this *_CursorPagingResponseParameters) MoreDataAvailable() *bool {
	return this.Ị_moreDataAvailable
}
func (this *_CursorPagingResponseParameters) SetMoreDataAvailable(v *bool) {
	this.Ị_moreDataAvailable = v
}
func (this *_CursorPagingResponseParameters) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_CursorPagingResponseParameters) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_CursorPagingResponseParameters) __type() {
}
func NewCursorPagingResponseParameters() CursorPagingResponseParameters {
	return &_CursorPagingResponseParameters{}
}
func init() {
	var val CursorPagingResponseParameters
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("ADGEntitlementService").Assembly("com.amazon.adg.common.model").RegisterShape("CursorPagingResponseParameters", t, func() interface{} {
		return NewCursorPagingResponseParameters()
	})
}
func init() {
	var val map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*string
		if f, ok := from.Interface().(map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]map[string]*string
		if f, ok := from.Interface().(map[string]map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to MapOfStringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []DomainNode
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []DomainNode
		if f, ok := from.Interface().([]DomainNode); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DomainNodeList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []DigitalGoodTransaction
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []DigitalGoodTransaction
		if f, ok := from.Interface().([]DigitalGoodTransaction); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodTransactionList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []DigitalGood
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []DigitalGood
		if f, ok := from.Interface().([]DigitalGood); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DigitalGoodList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductComparison
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductComparison
		if f, ok := from.Interface().([]ProductComparison); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductComparisonList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Asset
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Asset
		if f, ok := from.Interface().([]Asset); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Assets")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Offer
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Offer
		if f, ok := from.Interface().([]Offer); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to OfferList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Filter
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Filter
		if f, ok := from.Interface().([]Filter); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Filters")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductRule
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductRule
		if f, ok := from.Interface().([]ProductRule); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductRuleList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*int64
		if f, ok := from.Interface().([]*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Product
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Product
		if f, ok := from.Interface().([]Product); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []KeyValue
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []KeyValue
		if f, ok := from.Interface().([]KeyValue); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to KeyValueList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Plan
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Plan
		if f, ok := from.Interface().([]Plan); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to PlanList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []BillingPeriod
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []BillingPeriod
		if f, ok := from.Interface().([]BillingPeriod); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BillingPeriodList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Domain
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Domain
		if f, ok := from.Interface().([]Domain); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DomainSet")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductRevision
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductRevision
		if f, ok := from.Interface().([]ProductRevision); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductRevisionList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
