package tv_justin_tails

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to IsAlive")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Bool")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RequiredString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to OAuthState")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonCustomerId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonAccountPool")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AccountLinkChangeType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ErrorMessage")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DirectedId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TokenClientId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchUserName")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchUserID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to OAuthCode")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type CheckLinkIntegrityRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _CheckLinkIntegrityRequest struct {
	Ị_TwitchUserId *string `coral:"TwitchUserId" json:"TwitchUserId"`
}

func (this *_CheckLinkIntegrityRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_CheckLinkIntegrityRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_CheckLinkIntegrityRequest) __type() {
}
func NewCheckLinkIntegrityRequest() CheckLinkIntegrityRequest {
	return &_CheckLinkIntegrityRequest{}
}
func init() {
	var val CheckLinkIntegrityRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("CheckLinkIntegrityRequest", t, func() interface{} {
		return NewCheckLinkIntegrityRequest()
	})
}

type GetTaxInfoForPayoutEntityRequest interface {
	__type()
	SetPayoutEntityId(v *string)
	PayoutEntityId() *string
}
type _GetTaxInfoForPayoutEntityRequest struct {
	Ị_PayoutEntityId *string `coral:"PayoutEntityId" json:"PayoutEntityId"`
}

func (this *_GetTaxInfoForPayoutEntityRequest) PayoutEntityId() *string {
	return this.Ị_PayoutEntityId
}
func (this *_GetTaxInfoForPayoutEntityRequest) SetPayoutEntityId(v *string) {
	this.Ị_PayoutEntityId = v
}
func (this *_GetTaxInfoForPayoutEntityRequest) __type() {
}
func NewGetTaxInfoForPayoutEntityRequest() GetTaxInfoForPayoutEntityRequest {
	return &_GetTaxInfoForPayoutEntityRequest{}
}
func init() {
	var val GetTaxInfoForPayoutEntityRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTaxInfoForPayoutEntityRequest", t, func() interface{} {
		return NewGetTaxInfoForPayoutEntityRequest()
	})
}

type UnlinkAmazonAccountResponse interface {
	__type()
	SetSuccess(v *bool)
	Success() *bool
}
type _UnlinkAmazonAccountResponse struct {
	Ị_Success *bool `coral:"Success" json:"Success"`
}

func (this *_UnlinkAmazonAccountResponse) Success() *bool {
	return this.Ị_Success
}
func (this *_UnlinkAmazonAccountResponse) SetSuccess(v *bool) {
	this.Ị_Success = v
}
func (this *_UnlinkAmazonAccountResponse) __type() {
}
func NewUnlinkAmazonAccountResponse() UnlinkAmazonAccountResponse {
	return &_UnlinkAmazonAccountResponse{}
}
func init() {
	var val UnlinkAmazonAccountResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("UnlinkAmazonAccountResponse", t, func() interface{} {
		return NewUnlinkAmazonAccountResponse()
	})
}

//This exception is thrown when there is no customer info found from the Amazon address service
type NoCustomerInfoFoundException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _NoCustomerInfoFoundException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_NoCustomerInfoFoundException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_NoCustomerInfoFoundException) Message() *string {
	return this.Ị_Message
}
func (this *_NoCustomerInfoFoundException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_NoCustomerInfoFoundException) Context() *string {
	return this.Ị_Context
}
func (this *_NoCustomerInfoFoundException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_NoCustomerInfoFoundException) __type() {
}
func NewNoCustomerInfoFoundException() NoCustomerInfoFoundException {
	return &_NoCustomerInfoFoundException{}
}
func init() {
	var val NoCustomerInfoFoundException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("NoCustomerInfoFoundException", t, func() interface{} {
		return NewNoCustomerInfoFoundException()
	})
}

type GetPayoutIdentityRequest interface {
	__type()
	SetPayoutEntityId(v *string)
	PayoutEntityId() *string
}
type _GetPayoutIdentityRequest struct {
	Ị_PayoutEntityId *string `coral:"PayoutEntityId" json:"PayoutEntityId"`
}

func (this *_GetPayoutIdentityRequest) PayoutEntityId() *string {
	return this.Ị_PayoutEntityId
}
func (this *_GetPayoutIdentityRequest) SetPayoutEntityId(v *string) {
	this.Ị_PayoutEntityId = v
}
func (this *_GetPayoutIdentityRequest) __type() {
}
func NewGetPayoutIdentityRequest() GetPayoutIdentityRequest {
	return &_GetPayoutIdentityRequest{}
}
func init() {
	var val GetPayoutIdentityRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetPayoutIdentityRequest", t, func() interface{} {
		return NewGetPayoutIdentityRequest()
	})
}

//This exception is thrown when concurrent link modifications are made for the same item
type ConcurrentModificationException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _ConcurrentModificationException struct {
	RecoverableException
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_ConcurrentModificationException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ConcurrentModificationException) Message() *string {
	return this.Ị_Message
}
func (this *_ConcurrentModificationException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_ConcurrentModificationException) Context() *string {
	return this.Ị_Context
}
func (this *_ConcurrentModificationException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_ConcurrentModificationException) __type() {
}
func NewConcurrentModificationException() ConcurrentModificationException {
	return &_ConcurrentModificationException{}
}
func init() {
	var val ConcurrentModificationException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("ConcurrentModificationException", t, func() interface{} {
		return NewConcurrentModificationException()
	})
}

type GetTwitchUserNameResponse interface {
	__type()
	SetTwitchUserName(v *string)
	TwitchUserName() *string
	SetTwitchUserAvatar(v *string)
	TwitchUserAvatar() *string
	SetTwitchUserAccountType(v *string)
	TwitchUserAccountType() *string
}
type _GetTwitchUserNameResponse struct {
	Ị_TwitchUserName        *string `coral:"TwitchUserName" json:"TwitchUserName"`
	Ị_TwitchUserAvatar      *string `coral:"TwitchUserAvatar" json:"TwitchUserAvatar"`
	Ị_TwitchUserAccountType *string `coral:"TwitchUserAccountType" json:"TwitchUserAccountType"`
}

func (this *_GetTwitchUserNameResponse) TwitchUserAvatar() *string {
	return this.Ị_TwitchUserAvatar
}
func (this *_GetTwitchUserNameResponse) SetTwitchUserAvatar(v *string) {
	this.Ị_TwitchUserAvatar = v
}
func (this *_GetTwitchUserNameResponse) TwitchUserAccountType() *string {
	return this.Ị_TwitchUserAccountType
}
func (this *_GetTwitchUserNameResponse) SetTwitchUserAccountType(v *string) {
	this.Ị_TwitchUserAccountType = v
}
func (this *_GetTwitchUserNameResponse) TwitchUserName() *string {
	return this.Ị_TwitchUserName
}
func (this *_GetTwitchUserNameResponse) SetTwitchUserName(v *string) {
	this.Ị_TwitchUserName = v
}
func (this *_GetTwitchUserNameResponse) __type() {
}
func NewGetTwitchUserNameResponse() GetTwitchUserNameResponse {
	return &_GetTwitchUserNameResponse{}
}
func init() {
	var val GetTwitchUserNameResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTwitchUserNameResponse", t, func() interface{} {
		return NewGetTwitchUserNameResponse()
	})
}

type GetPayoutIdentityResponse interface {
	__type()
	SetHasServicesRecord(v *bool)
	HasServicesRecord() *bool
	SetSupportedBitsCountry(v *bool)
	SupportedBitsCountry() *bool
}
type _GetPayoutIdentityResponse struct {
	Ị_SupportedBitsCountry *bool `coral:"SupportedBitsCountry" json:"SupportedBitsCountry"`
	Ị_HasServicesRecord    *bool `coral:"HasServicesRecord" json:"HasServicesRecord"`
}

func (this *_GetPayoutIdentityResponse) HasServicesRecord() *bool {
	return this.Ị_HasServicesRecord
}
func (this *_GetPayoutIdentityResponse) SetHasServicesRecord(v *bool) {
	this.Ị_HasServicesRecord = v
}
func (this *_GetPayoutIdentityResponse) SupportedBitsCountry() *bool {
	return this.Ị_SupportedBitsCountry
}
func (this *_GetPayoutIdentityResponse) SetSupportedBitsCountry(v *bool) {
	this.Ị_SupportedBitsCountry = v
}
func (this *_GetPayoutIdentityResponse) __type() {
}
func NewGetPayoutIdentityResponse() GetPayoutIdentityResponse {
	return &_GetPayoutIdentityResponse{}
}
func init() {
	var val GetPayoutIdentityResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetPayoutIdentityResponse", t, func() interface{} {
		return NewGetPayoutIdentityResponse()
	})
}

//This exception is thrown when retrying will not help. e.g. bad input.
type UnrecoverableException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _UnrecoverableException struct {
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_UnrecoverableException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_UnrecoverableException) Message() *string {
	return this.Ị_Message
}
func (this *_UnrecoverableException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_UnrecoverableException) Context() *string {
	return this.Ị_Context
}
func (this *_UnrecoverableException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_UnrecoverableException) __type() {
}
func NewUnrecoverableException() UnrecoverableException {
	return &_UnrecoverableException{}
}
func init() {
	var val UnrecoverableException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("UnrecoverableException", t, func() interface{} {
		return NewUnrecoverableException()
	})
}

//The root of all TAILS exceptions.
type TAILSException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _TAILSException struct {
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_TAILSException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_TAILSException) Context() *string {
	return this.Ị_Context
}
func (this *_TAILSException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_TAILSException) Message() *string {
	return this.Ị_Message
}
func (this *_TAILSException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_TAILSException) __type() {
}
func NewTAILSException() TAILSException {
	return &_TAILSException{}
}
func init() {
	var val TAILSException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("TAILSException", t, func() interface{} {
		return NewTAILSException()
	})
}

//This exception is thrown on a database (or other dependency) error
type DependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _DependencyException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_Message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_DependencyException) Context() *string {
	return this.Ị_Context
}
func (this *_DependencyException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

//This exception is thrown when an amazon account already has the maximum number of allowable links
type TooManyTwitchAccountsLinkedException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
	SetLinkedAccountLimit(v *int32)
	LinkedAccountLimit() *int32
}
type _TooManyTwitchAccountsLinkedException struct {
	UnrecoverableException
	TAILSException
	Ị_Context            *string `coral:"Context" json:"Context"`
	Ị_Message            *string `coral:"Message" json:"Message"`
	Ị_LinkedAccountLimit *int32  `coral:"LinkedAccountLimit" json:"LinkedAccountLimit"`
}

func (this *_TooManyTwitchAccountsLinkedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_TooManyTwitchAccountsLinkedException) Message() *string {
	return this.Ị_Message
}
func (this *_TooManyTwitchAccountsLinkedException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_TooManyTwitchAccountsLinkedException) Context() *string {
	return this.Ị_Context
}
func (this *_TooManyTwitchAccountsLinkedException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_TooManyTwitchAccountsLinkedException) LinkedAccountLimit() *int32 {
	return this.Ị_LinkedAccountLimit
}
func (this *_TooManyTwitchAccountsLinkedException) SetLinkedAccountLimit(v *int32) {
	this.Ị_LinkedAccountLimit = v
}
func (this *_TooManyTwitchAccountsLinkedException) __type() {
}
func NewTooManyTwitchAccountsLinkedException() TooManyTwitchAccountsLinkedException {
	return &_TooManyTwitchAccountsLinkedException{}
}
func init() {
	var val TooManyTwitchAccountsLinkedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("TooManyTwitchAccountsLinkedException", t, func() interface{} {
		return NewTooManyTwitchAccountsLinkedException()
	})
}

type UnlinkTwitchAccountResponse interface {
	__type()
	SetSuccess(v *bool)
	Success() *bool
}
type _UnlinkTwitchAccountResponse struct {
	Ị_Success *bool `coral:"Success" json:"Success"`
}

func (this *_UnlinkTwitchAccountResponse) Success() *bool {
	return this.Ị_Success
}
func (this *_UnlinkTwitchAccountResponse) SetSuccess(v *bool) {
	this.Ị_Success = v
}
func (this *_UnlinkTwitchAccountResponse) __type() {
}
func NewUnlinkTwitchAccountResponse() UnlinkTwitchAccountResponse {
	return &_UnlinkTwitchAccountResponse{}
}
func init() {
	var val UnlinkTwitchAccountResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("UnlinkTwitchAccountResponse", t, func() interface{} {
		return NewUnlinkTwitchAccountResponse()
	})
}

type GetLinkedAmazonAccountResponse interface {
	__type()
	SetPublicAmazonAccount(v PublicAmazonAccount)
	PublicAmazonAccount() PublicAmazonAccount
	SetHasLinkedAccount(v *bool)
	HasLinkedAccount() *bool
}
type _GetLinkedAmazonAccountResponse struct {
	Ị_HasLinkedAccount    *bool               `coral:"HasLinkedAccount" json:"HasLinkedAccount"`
	Ị_PublicAmazonAccount PublicAmazonAccount `coral:"PublicAmazonAccount" json:"PublicAmazonAccount"`
}

func (this *_GetLinkedAmazonAccountResponse) HasLinkedAccount() *bool {
	return this.Ị_HasLinkedAccount
}
func (this *_GetLinkedAmazonAccountResponse) SetHasLinkedAccount(v *bool) {
	this.Ị_HasLinkedAccount = v
}
func (this *_GetLinkedAmazonAccountResponse) PublicAmazonAccount() PublicAmazonAccount {
	return this.Ị_PublicAmazonAccount
}
func (this *_GetLinkedAmazonAccountResponse) SetPublicAmazonAccount(v PublicAmazonAccount) {
	this.Ị_PublicAmazonAccount = v
}
func (this *_GetLinkedAmazonAccountResponse) __type() {
}
func NewGetLinkedAmazonAccountResponse() GetLinkedAmazonAccountResponse {
	return &_GetLinkedAmazonAccountResponse{}
}
func init() {
	var val GetLinkedAmazonAccountResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetLinkedAmazonAccountResponse", t, func() interface{} {
		return NewGetLinkedAmazonAccountResponse()
	})
}

type CheckLinkIntegrityResponse interface {
	__type()
	SetConsistentLinkState(v *bool)
	ConsistentLinkState() *bool
	SetAnnotation(v *string)
	Annotation() *string
}
type _CheckLinkIntegrityResponse struct {
	Ị_Annotation          *string `coral:"Annotation" json:"Annotation"`
	Ị_ConsistentLinkState *bool   `coral:"ConsistentLinkState" json:"ConsistentLinkState"`
}

func (this *_CheckLinkIntegrityResponse) ConsistentLinkState() *bool {
	return this.Ị_ConsistentLinkState
}
func (this *_CheckLinkIntegrityResponse) SetConsistentLinkState(v *bool) {
	this.Ị_ConsistentLinkState = v
}
func (this *_CheckLinkIntegrityResponse) Annotation() *string {
	return this.Ị_Annotation
}
func (this *_CheckLinkIntegrityResponse) SetAnnotation(v *string) {
	this.Ị_Annotation = v
}
func (this *_CheckLinkIntegrityResponse) __type() {
}
func NewCheckLinkIntegrityResponse() CheckLinkIntegrityResponse {
	return &_CheckLinkIntegrityResponse{}
}
func init() {
	var val CheckLinkIntegrityResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("CheckLinkIntegrityResponse", t, func() interface{} {
		return NewCheckLinkIntegrityResponse()
	})
}

//This exception is thrown when there was bad input.
type InvalidParameterException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _InvalidParameterException struct {
	UnrecoverableException
	TAILSException
	Ị_Context *string `coral:"Context" json:"Context"`
	Ị_Message *string `coral:"Message" json:"Message"`
}

func (this *_InvalidParameterException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidParameterException) Message() *string {
	return this.Ị_Message
}
func (this *_InvalidParameterException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_InvalidParameterException) Context() *string {
	return this.Ị_Context
}
func (this *_InvalidParameterException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_InvalidParameterException) __type() {
}
func NewInvalidParameterException() InvalidParameterException {
	return &_InvalidParameterException{}
}
func init() {
	var val InvalidParameterException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("InvalidParameterException", t, func() interface{} {
		return NewInvalidParameterException()
	})
}

//This exception is thrown when concurrent link or unlink modifications are made for the same item
type ALSConcurrentModificationException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _ALSConcurrentModificationException struct {
	UnrecoverableException
	TAILSException
	Ị_Context *string `coral:"Context" json:"Context"`
	Ị_Message *string `coral:"Message" json:"Message"`
}

func (this *_ALSConcurrentModificationException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ALSConcurrentModificationException) Message() *string {
	return this.Ị_Message
}
func (this *_ALSConcurrentModificationException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_ALSConcurrentModificationException) Context() *string {
	return this.Ị_Context
}
func (this *_ALSConcurrentModificationException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_ALSConcurrentModificationException) __type() {
}
func NewALSConcurrentModificationException() ALSConcurrentModificationException {
	return &_ALSConcurrentModificationException{}
}
func init() {
	var val ALSConcurrentModificationException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("ALSConcurrentModificationException", t, func() interface{} {
		return NewALSConcurrentModificationException()
	})
}

type LinkAccountsRequest interface {
	__type()
	SetMarketplace(v *string)
	Marketplace() *string
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
	SetTwitchUserID(v *string)
	TwitchUserID() *string
	SetWithoutSendingEmail(v *bool)
	WithoutSendingEmail() *bool
	SetWithoutSendingSNSNotification(v *bool)
	WithoutSendingSNSNotification() *bool
	SetTestCall(v *bool)
	TestCall() *bool
	SetLocale(v *string)
	Locale() *string
}
type _LinkAccountsRequest struct {
	Ị_TestCall                      *bool         `coral:"TestCall" json:"TestCall"`
	Ị_locale                        *string       `coral:"locale" json:"locale"`
	Ị_marketplace                   *string       `coral:"marketplace" json:"marketplace"`
	Ị_AmazonAccount                 AmazonAccount `coral:"AmazonAccount" json:"AmazonAccount"`
	Ị_TwitchUserID                  *string       `coral:"TwitchUserID" json:"TwitchUserID"`
	Ị_WithoutSendingEmail           *bool         `coral:"WithoutSendingEmail" json:"WithoutSendingEmail"`
	Ị_WithoutSendingSNSNotification *bool         `coral:"WithoutSendingSNSNotification" json:"WithoutSendingSNSNotification"`
}

func (this *_LinkAccountsRequest) WithoutSendingSNSNotification() *bool {
	return this.Ị_WithoutSendingSNSNotification
}
func (this *_LinkAccountsRequest) SetWithoutSendingSNSNotification(v *bool) {
	this.Ị_WithoutSendingSNSNotification = v
}
func (this *_LinkAccountsRequest) TestCall() *bool {
	return this.Ị_TestCall
}
func (this *_LinkAccountsRequest) SetTestCall(v *bool) {
	this.Ị_TestCall = v
}
func (this *_LinkAccountsRequest) Locale() *string {
	return this.Ị_locale
}
func (this *_LinkAccountsRequest) SetLocale(v *string) {
	this.Ị_locale = v
}
func (this *_LinkAccountsRequest) Marketplace() *string {
	return this.Ị_marketplace
}
func (this *_LinkAccountsRequest) SetMarketplace(v *string) {
	this.Ị_marketplace = v
}
func (this *_LinkAccountsRequest) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_LinkAccountsRequest) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_LinkAccountsRequest) TwitchUserID() *string {
	return this.Ị_TwitchUserID
}
func (this *_LinkAccountsRequest) SetTwitchUserID(v *string) {
	this.Ị_TwitchUserID = v
}
func (this *_LinkAccountsRequest) WithoutSendingEmail() *bool {
	return this.Ị_WithoutSendingEmail
}
func (this *_LinkAccountsRequest) SetWithoutSendingEmail(v *bool) {
	this.Ị_WithoutSendingEmail = v
}
func (this *_LinkAccountsRequest) __type() {
}
func NewLinkAccountsRequest() LinkAccountsRequest {
	return &_LinkAccountsRequest{}
}
func init() {
	var val LinkAccountsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("LinkAccountsRequest", t, func() interface{} {
		return NewLinkAccountsRequest()
	})
}

type GetLinkedTwitchAccountsRequest interface {
	__type()
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
	SetExcludingTwitchUserInfo(v *bool)
	ExcludingTwitchUserInfo() *bool
}
type _GetLinkedTwitchAccountsRequest struct {
	Ị_AmazonAccount           AmazonAccount `coral:"AmazonAccount" json:"AmazonAccount"`
	Ị_ExcludingTwitchUserInfo *bool         `coral:"ExcludingTwitchUserInfo" json:"ExcludingTwitchUserInfo"`
}

func (this *_GetLinkedTwitchAccountsRequest) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_GetLinkedTwitchAccountsRequest) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_GetLinkedTwitchAccountsRequest) ExcludingTwitchUserInfo() *bool {
	return this.Ị_ExcludingTwitchUserInfo
}
func (this *_GetLinkedTwitchAccountsRequest) SetExcludingTwitchUserInfo(v *bool) {
	this.Ị_ExcludingTwitchUserInfo = v
}
func (this *_GetLinkedTwitchAccountsRequest) __type() {
}
func NewGetLinkedTwitchAccountsRequest() GetLinkedTwitchAccountsRequest {
	return &_GetLinkedTwitchAccountsRequest{}
}
func init() {
	var val GetLinkedTwitchAccountsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetLinkedTwitchAccountsRequest", t, func() interface{} {
		return NewGetLinkedTwitchAccountsRequest()
	})
}

type GetTaxWithholdingRateForPayoutEntityRequest interface {
	__type()
	SetPayoutEntityId(v *string)
	PayoutEntityId() *string
}
type _GetTaxWithholdingRateForPayoutEntityRequest struct {
	Ị_PayoutEntityId *string `coral:"PayoutEntityId" json:"PayoutEntityId"`
}

func (this *_GetTaxWithholdingRateForPayoutEntityRequest) PayoutEntityId() *string {
	return this.Ị_PayoutEntityId
}
func (this *_GetTaxWithholdingRateForPayoutEntityRequest) SetPayoutEntityId(v *string) {
	this.Ị_PayoutEntityId = v
}
func (this *_GetTaxWithholdingRateForPayoutEntityRequest) __type() {
}
func NewGetTaxWithholdingRateForPayoutEntityRequest() GetTaxWithholdingRateForPayoutEntityRequest {
	return &_GetTaxWithholdingRateForPayoutEntityRequest{}
}
func init() {
	var val GetTaxWithholdingRateForPayoutEntityRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTaxWithholdingRateForPayoutEntityRequest", t, func() interface{} {
		return NewGetTaxWithholdingRateForPayoutEntityRequest()
	})
}

type SubmitDeskTicketResponse interface {
	__type()
	SetSqsMessageId(v *string)
	SqsMessageId() *string
}
type _SubmitDeskTicketResponse struct {
	Ị_SqsMessageId *string `coral:"SqsMessageId" json:"SqsMessageId"`
}

func (this *_SubmitDeskTicketResponse) SqsMessageId() *string {
	return this.Ị_SqsMessageId
}
func (this *_SubmitDeskTicketResponse) SetSqsMessageId(v *string) {
	this.Ị_SqsMessageId = v
}
func (this *_SubmitDeskTicketResponse) __type() {
}
func NewSubmitDeskTicketResponse() SubmitDeskTicketResponse {
	return &_SubmitDeskTicketResponse{}
}
func init() {
	var val SubmitDeskTicketResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("SubmitDeskTicketResponse", t, func() interface{} {
		return NewSubmitDeskTicketResponse()
	})
}

type GetTaxInfoForPayoutEntityResponse interface {
	__type()
	SetAddressId(v *string)
	AddressId() *string
	SetPayoutEntityId(v *string)
	PayoutEntityId() *string
	SetTaxInterviewStatus(v *string)
	TaxInterviewStatus() *string
}
type _GetTaxInfoForPayoutEntityResponse struct {
	Ị_PayoutEntityId     *string `coral:"PayoutEntityId" json:"PayoutEntityId"`
	Ị_TaxInterviewStatus *string `coral:"TaxInterviewStatus" json:"TaxInterviewStatus"`
	Ị_AddressId          *string `coral:"AddressId" json:"AddressId"`
}

func (this *_GetTaxInfoForPayoutEntityResponse) TaxInterviewStatus() *string {
	return this.Ị_TaxInterviewStatus
}
func (this *_GetTaxInfoForPayoutEntityResponse) SetTaxInterviewStatus(v *string) {
	this.Ị_TaxInterviewStatus = v
}
func (this *_GetTaxInfoForPayoutEntityResponse) AddressId() *string {
	return this.Ị_AddressId
}
func (this *_GetTaxInfoForPayoutEntityResponse) SetAddressId(v *string) {
	this.Ị_AddressId = v
}
func (this *_GetTaxInfoForPayoutEntityResponse) PayoutEntityId() *string {
	return this.Ị_PayoutEntityId
}
func (this *_GetTaxInfoForPayoutEntityResponse) SetPayoutEntityId(v *string) {
	this.Ị_PayoutEntityId = v
}
func (this *_GetTaxInfoForPayoutEntityResponse) __type() {
}
func NewGetTaxInfoForPayoutEntityResponse() GetTaxInfoForPayoutEntityResponse {
	return &_GetTaxInfoForPayoutEntityResponse{}
}
func init() {
	var val GetTaxInfoForPayoutEntityResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTaxInfoForPayoutEntityResponse", t, func() interface{} {
		return NewGetTaxInfoForPayoutEntityResponse()
	})
}

//A notification that is sent whenever the status of an account link changes.
//
//
//Type - The type of change that occurred.
//
//AmazonAccount - The amazon account that the change occurred on.
//
//TwitchAccounts - All the twitch accounts currently attached to the amazon account.
//
//ActionedTwitchAccount - The twitch account that the action occurred on. I.E. The account that was linked/unlinked.
type AccountLinkChangedNotification interface {
	__type()
	SetType(v *string)
	Type() *string
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
	SetTwitchAccounts(v []TwitchAccount)
	TwitchAccounts() []TwitchAccount
	SetActionedTwitchAccount(v TwitchAccount)
	ActionedTwitchAccount() TwitchAccount
}
type _AccountLinkChangedNotification struct {
	Ị_Type                  *string         `coral:"Type" json:"Type"`
	Ị_AmazonAccount         AmazonAccount   `coral:"AmazonAccount" json:"AmazonAccount"`
	Ị_TwitchAccounts        []TwitchAccount `coral:"TwitchAccounts" json:"TwitchAccounts"`
	Ị_ActionedTwitchAccount TwitchAccount   `coral:"ActionedTwitchAccount" json:"ActionedTwitchAccount"`
}

func (this *_AccountLinkChangedNotification) Type() *string {
	return this.Ị_Type
}
func (this *_AccountLinkChangedNotification) SetType(v *string) {
	this.Ị_Type = v
}
func (this *_AccountLinkChangedNotification) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_AccountLinkChangedNotification) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_AccountLinkChangedNotification) TwitchAccounts() []TwitchAccount {
	return this.Ị_TwitchAccounts
}
func (this *_AccountLinkChangedNotification) SetTwitchAccounts(v []TwitchAccount) {
	this.Ị_TwitchAccounts = v
}
func (this *_AccountLinkChangedNotification) ActionedTwitchAccount() TwitchAccount {
	return this.Ị_ActionedTwitchAccount
}
func (this *_AccountLinkChangedNotification) SetActionedTwitchAccount(v TwitchAccount) {
	this.Ị_ActionedTwitchAccount = v
}
func (this *_AccountLinkChangedNotification) __type() {
}
func NewAccountLinkChangedNotification() AccountLinkChangedNotification {
	return &_AccountLinkChangedNotification{}
}
func init() {
	var val AccountLinkChangedNotification
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("AccountLinkChangedNotification", t, func() interface{} {
		return NewAccountLinkChangedNotification()
	})
}

//This exception is thrown when trying to link an already linked Twitch account to another amazon account
type AccountAlreadyLinkedException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _AccountAlreadyLinkedException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_AccountAlreadyLinkedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_AccountAlreadyLinkedException) Message() *string {
	return this.Ị_Message
}
func (this *_AccountAlreadyLinkedException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_AccountAlreadyLinkedException) Context() *string {
	return this.Ị_Context
}
func (this *_AccountAlreadyLinkedException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_AccountAlreadyLinkedException) __type() {
}
func NewAccountAlreadyLinkedException() AccountAlreadyLinkedException {
	return &_AccountAlreadyLinkedException{}
}
func init() {
	var val AccountAlreadyLinkedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("AccountAlreadyLinkedException", t, func() interface{} {
		return NewAccountAlreadyLinkedException()
	})
}

//This exception is thrown when the Twitch account is linked to multiple Amazon accounts
type MultipleAmazonAccountsLinkedException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _MultipleAmazonAccountsLinkedException struct {
	UnrecoverableException
	TAILSException
	Ị_Context *string `coral:"Context" json:"Context"`
	Ị_Message *string `coral:"Message" json:"Message"`
}

func (this *_MultipleAmazonAccountsLinkedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_MultipleAmazonAccountsLinkedException) Message() *string {
	return this.Ị_Message
}
func (this *_MultipleAmazonAccountsLinkedException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_MultipleAmazonAccountsLinkedException) Context() *string {
	return this.Ị_Context
}
func (this *_MultipleAmazonAccountsLinkedException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_MultipleAmazonAccountsLinkedException) __type() {
}
func NewMultipleAmazonAccountsLinkedException() MultipleAmazonAccountsLinkedException {
	return &_MultipleAmazonAccountsLinkedException{}
}
func init() {
	var val MultipleAmazonAccountsLinkedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("MultipleAmazonAccountsLinkedException", t, func() interface{} {
		return NewMultipleAmazonAccountsLinkedException()
	})
}

//Thrown when the payout entity has multiple CORs in TIMS
type InconsistentTIMSDataException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _InconsistentTIMSDataException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_InconsistentTIMSDataException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InconsistentTIMSDataException) Message() *string {
	return this.Ị_Message
}
func (this *_InconsistentTIMSDataException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_InconsistentTIMSDataException) Context() *string {
	return this.Ị_Context
}
func (this *_InconsistentTIMSDataException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_InconsistentTIMSDataException) __type() {
}
func NewInconsistentTIMSDataException() InconsistentTIMSDataException {
	return &_InconsistentTIMSDataException{}
}
func init() {
	var val InconsistentTIMSDataException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("InconsistentTIMSDataException", t, func() interface{} {
		return NewInconsistentTIMSDataException()
	})
}

type GetTwitchUserNameRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _GetTwitchUserNameRequest struct {
	Ị_TwitchUserId *string `coral:"TwitchUserId" json:"TwitchUserId"`
}

func (this *_GetTwitchUserNameRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_GetTwitchUserNameRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_GetTwitchUserNameRequest) __type() {
}
func NewGetTwitchUserNameRequest() GetTwitchUserNameRequest {
	return &_GetTwitchUserNameRequest{}
}
func init() {
	var val GetTwitchUserNameRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTwitchUserNameRequest", t, func() interface{} {
		return NewGetTwitchUserNameRequest()
	})
}

type AmazonAccount interface {
	__type()
	SetAmazonId(v *string)
	AmazonId() *string
	SetAmazonAccountPool(v *string)
	AmazonAccountPool() *string
}
type _AmazonAccount struct {
	Ị_AmazonId          *string `coral:"AmazonId" json:"AmazonId"`
	Ị_AmazonAccountPool *string `coral:"AmazonAccountPool" json:"AmazonAccountPool"`
}

func (this *_AmazonAccount) AmazonId() *string {
	return this.Ị_AmazonId
}
func (this *_AmazonAccount) SetAmazonId(v *string) {
	this.Ị_AmazonId = v
}
func (this *_AmazonAccount) AmazonAccountPool() *string {
	return this.Ị_AmazonAccountPool
}
func (this *_AmazonAccount) SetAmazonAccountPool(v *string) {
	this.Ị_AmazonAccountPool = v
}
func (this *_AmazonAccount) __type() {
}
func NewAmazonAccount() AmazonAccount {
	return &_AmazonAccount{}
}
func init() {
	var val AmazonAccount
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("AmazonAccount", t, func() interface{} {
		return NewAmazonAccount()
	})
}

type GetLinkedTwitchAccountsResponse interface {
	__type()
	SetTwitchAccounts(v []TwitchAccount)
	TwitchAccounts() []TwitchAccount
}
type _GetLinkedTwitchAccountsResponse struct {
	Ị_TwitchAccounts []TwitchAccount `coral:"TwitchAccounts" json:"TwitchAccounts"`
}

func (this *_GetLinkedTwitchAccountsResponse) TwitchAccounts() []TwitchAccount {
	return this.Ị_TwitchAccounts
}
func (this *_GetLinkedTwitchAccountsResponse) SetTwitchAccounts(v []TwitchAccount) {
	this.Ị_TwitchAccounts = v
}
func (this *_GetLinkedTwitchAccountsResponse) __type() {
}
func NewGetLinkedTwitchAccountsResponse() GetLinkedTwitchAccountsResponse {
	return &_GetLinkedTwitchAccountsResponse{}
}
func init() {
	var val GetLinkedTwitchAccountsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetLinkedTwitchAccountsResponse", t, func() interface{} {
		return NewGetLinkedTwitchAccountsResponse()
	})
}

type GetTaxWithholdingRateForPayoutEntityResponse interface {
	__type()
	SetPayoutEntityId(v *string)
	PayoutEntityId() *string
	SetWithholdingRateMap(v map[string]*float64)
	WithholdingRateMap() map[string]*float64
}
type _GetTaxWithholdingRateForPayoutEntityResponse struct {
	Ị_PayoutEntityId     *string             `coral:"PayoutEntityId" json:"PayoutEntityId"`
	Ị_WithholdingRateMap map[string]*float64 `coral:"WithholdingRateMap" json:"WithholdingRateMap"`
}

func (this *_GetTaxWithholdingRateForPayoutEntityResponse) PayoutEntityId() *string {
	return this.Ị_PayoutEntityId
}
func (this *_GetTaxWithholdingRateForPayoutEntityResponse) SetPayoutEntityId(v *string) {
	this.Ị_PayoutEntityId = v
}
func (this *_GetTaxWithholdingRateForPayoutEntityResponse) WithholdingRateMap() map[string]*float64 {
	return this.Ị_WithholdingRateMap
}
func (this *_GetTaxWithholdingRateForPayoutEntityResponse) SetWithholdingRateMap(v map[string]*float64) {
	this.Ị_WithholdingRateMap = v
}
func (this *_GetTaxWithholdingRateForPayoutEntityResponse) __type() {
}
func NewGetTaxWithholdingRateForPayoutEntityResponse() GetTaxWithholdingRateForPayoutEntityResponse {
	return &_GetTaxWithholdingRateForPayoutEntityResponse{}
}
func init() {
	var val GetTaxWithholdingRateForPayoutEntityResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTaxWithholdingRateForPayoutEntityResponse", t, func() interface{} {
		return NewGetTaxWithholdingRateForPayoutEntityResponse()
	})
}

type GetLinkedAmazonDirectedIdResponse interface {
	__type()
	SetHasLinkedAccount(v *bool)
	HasLinkedAccount() *bool
	SetAmazonDirectedId(v *string)
	AmazonDirectedId() *string
	SetCountryOfResidence(v *string)
	CountryOfResidence() *string
}
type _GetLinkedAmazonDirectedIdResponse struct {
	Ị_HasLinkedAccount   *bool   `coral:"HasLinkedAccount" json:"HasLinkedAccount"`
	Ị_AmazonDirectedId   *string `coral:"AmazonDirectedId" json:"AmazonDirectedId"`
	Ị_CountryOfResidence *string `coral:"CountryOfResidence" json:"CountryOfResidence"`
}

func (this *_GetLinkedAmazonDirectedIdResponse) HasLinkedAccount() *bool {
	return this.Ị_HasLinkedAccount
}
func (this *_GetLinkedAmazonDirectedIdResponse) SetHasLinkedAccount(v *bool) {
	this.Ị_HasLinkedAccount = v
}
func (this *_GetLinkedAmazonDirectedIdResponse) AmazonDirectedId() *string {
	return this.Ị_AmazonDirectedId
}
func (this *_GetLinkedAmazonDirectedIdResponse) SetAmazonDirectedId(v *string) {
	this.Ị_AmazonDirectedId = v
}
func (this *_GetLinkedAmazonDirectedIdResponse) CountryOfResidence() *string {
	return this.Ị_CountryOfResidence
}
func (this *_GetLinkedAmazonDirectedIdResponse) SetCountryOfResidence(v *string) {
	this.Ị_CountryOfResidence = v
}
func (this *_GetLinkedAmazonDirectedIdResponse) __type() {
}
func NewGetLinkedAmazonDirectedIdResponse() GetLinkedAmazonDirectedIdResponse {
	return &_GetLinkedAmazonDirectedIdResponse{}
}
func init() {
	var val GetLinkedAmazonDirectedIdResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetLinkedAmazonDirectedIdResponse", t, func() interface{} {
		return NewGetLinkedAmazonDirectedIdResponse()
	})
}

type GetTwitchUserInfoResponse interface {
	__type()
	SetTwitchAccount(v TwitchAccount)
	TwitchAccount() TwitchAccount
}
type _GetTwitchUserInfoResponse struct {
	Ị_TwitchAccount TwitchAccount `coral:"TwitchAccount" json:"TwitchAccount"`
}

func (this *_GetTwitchUserInfoResponse) TwitchAccount() TwitchAccount {
	return this.Ị_TwitchAccount
}
func (this *_GetTwitchUserInfoResponse) SetTwitchAccount(v TwitchAccount) {
	this.Ị_TwitchAccount = v
}
func (this *_GetTwitchUserInfoResponse) __type() {
}
func NewGetTwitchUserInfoResponse() GetTwitchUserInfoResponse {
	return &_GetTwitchUserInfoResponse{}
}
func init() {
	var val GetTwitchUserInfoResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTwitchUserInfoResponse", t, func() interface{} {
		return NewGetTwitchUserInfoResponse()
	})
}

type SubmitDeskTicketRequest interface {
	__type()
	SetProblemStatement(v *string)
	ProblemStatement() *string
	SetTwitchId(v *string)
	TwitchId() *string
	SetCSCCaseId(v *string)
	CSCCaseId() *string
	SetDeskCaseId(v *string)
	DeskCaseId() *string
}
type _SubmitDeskTicketRequest struct {
	Ị_CSCCaseId        *string `coral:"CSCCaseId" json:"CSCCaseId"`
	Ị_DeskCaseId       *string `coral:"DeskCaseId" json:"DeskCaseId"`
	Ị_ProblemStatement *string `coral:"ProblemStatement" json:"ProblemStatement"`
	Ị_TwitchId         *string `coral:"TwitchId" json:"TwitchId"`
}

func (this *_SubmitDeskTicketRequest) CSCCaseId() *string {
	return this.Ị_CSCCaseId
}
func (this *_SubmitDeskTicketRequest) SetCSCCaseId(v *string) {
	this.Ị_CSCCaseId = v
}
func (this *_SubmitDeskTicketRequest) DeskCaseId() *string {
	return this.Ị_DeskCaseId
}
func (this *_SubmitDeskTicketRequest) SetDeskCaseId(v *string) {
	this.Ị_DeskCaseId = v
}
func (this *_SubmitDeskTicketRequest) ProblemStatement() *string {
	return this.Ị_ProblemStatement
}
func (this *_SubmitDeskTicketRequest) SetProblemStatement(v *string) {
	this.Ị_ProblemStatement = v
}
func (this *_SubmitDeskTicketRequest) TwitchId() *string {
	return this.Ị_TwitchId
}
func (this *_SubmitDeskTicketRequest) SetTwitchId(v *string) {
	this.Ị_TwitchId = v
}
func (this *_SubmitDeskTicketRequest) __type() {
}
func NewSubmitDeskTicketRequest() SubmitDeskTicketRequest {
	return &_SubmitDeskTicketRequest{}
}
func init() {
	var val SubmitDeskTicketRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("SubmitDeskTicketRequest", t, func() interface{} {
		return NewSubmitDeskTicketRequest()
	})
}

type LinkAccountsResponse interface {
	__type()
	SetSuccess(v *bool)
	Success() *bool
}
type _LinkAccountsResponse struct {
	Ị_Success *bool `coral:"Success" json:"Success"`
}

func (this *_LinkAccountsResponse) Success() *bool {
	return this.Ị_Success
}
func (this *_LinkAccountsResponse) SetSuccess(v *bool) {
	this.Ị_Success = v
}
func (this *_LinkAccountsResponse) __type() {
}
func NewLinkAccountsResponse() LinkAccountsResponse {
	return &_LinkAccountsResponse{}
}
func init() {
	var val LinkAccountsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("LinkAccountsResponse", t, func() interface{} {
		return NewLinkAccountsResponse()
	})
}

//This exception is thrown when retrying may help. e.g. timeout downstream.
type RecoverableException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _RecoverableException struct {
	TAILSException
	Ị_Message *string `coral:"Message" json:"Message"`
	Ị_Context *string `coral:"Context" json:"Context"`
}

func (this *_RecoverableException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_RecoverableException) Message() *string {
	return this.Ị_Message
}
func (this *_RecoverableException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_RecoverableException) Context() *string {
	return this.Ị_Context
}
func (this *_RecoverableException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_RecoverableException) __type() {
}
func NewRecoverableException() RecoverableException {
	return &_RecoverableException{}
}
func init() {
	var val RecoverableException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("RecoverableException", t, func() interface{} {
		return NewRecoverableException()
	})
}

type UnlinkTwitchAccountRequest interface {
	__type()
	SetTwitchUserID(v *string)
	TwitchUserID() *string
	SetWithoutSendingEmail(v *bool)
	WithoutSendingEmail() *bool
	SetWithoutSendingSNSNotification(v *bool)
	WithoutSendingSNSNotification() *bool
	SetTestCall(v *bool)
	TestCall() *bool
	SetLocale(v *string)
	Locale() *string
	SetMarketplace(v *string)
	Marketplace() *string
}
type _UnlinkTwitchAccountRequest struct {
	Ị_WithoutSendingEmail           *bool   `coral:"WithoutSendingEmail" json:"WithoutSendingEmail"`
	Ị_WithoutSendingSNSNotification *bool   `coral:"WithoutSendingSNSNotification" json:"WithoutSendingSNSNotification"`
	Ị_TestCall                      *bool   `coral:"TestCall" json:"TestCall"`
	Ị_locale                        *string `coral:"locale" json:"locale"`
	Ị_marketplace                   *string `coral:"marketplace" json:"marketplace"`
	Ị_TwitchUserID                  *string `coral:"TwitchUserID" json:"TwitchUserID"`
}

func (this *_UnlinkTwitchAccountRequest) TwitchUserID() *string {
	return this.Ị_TwitchUserID
}
func (this *_UnlinkTwitchAccountRequest) SetTwitchUserID(v *string) {
	this.Ị_TwitchUserID = v
}
func (this *_UnlinkTwitchAccountRequest) WithoutSendingEmail() *bool {
	return this.Ị_WithoutSendingEmail
}
func (this *_UnlinkTwitchAccountRequest) SetWithoutSendingEmail(v *bool) {
	this.Ị_WithoutSendingEmail = v
}
func (this *_UnlinkTwitchAccountRequest) WithoutSendingSNSNotification() *bool {
	return this.Ị_WithoutSendingSNSNotification
}
func (this *_UnlinkTwitchAccountRequest) SetWithoutSendingSNSNotification(v *bool) {
	this.Ị_WithoutSendingSNSNotification = v
}
func (this *_UnlinkTwitchAccountRequest) TestCall() *bool {
	return this.Ị_TestCall
}
func (this *_UnlinkTwitchAccountRequest) SetTestCall(v *bool) {
	this.Ị_TestCall = v
}
func (this *_UnlinkTwitchAccountRequest) Locale() *string {
	return this.Ị_locale
}
func (this *_UnlinkTwitchAccountRequest) SetLocale(v *string) {
	this.Ị_locale = v
}
func (this *_UnlinkTwitchAccountRequest) Marketplace() *string {
	return this.Ị_marketplace
}
func (this *_UnlinkTwitchAccountRequest) SetMarketplace(v *string) {
	this.Ị_marketplace = v
}
func (this *_UnlinkTwitchAccountRequest) __type() {
}
func NewUnlinkTwitchAccountRequest() UnlinkTwitchAccountRequest {
	return &_UnlinkTwitchAccountRequest{}
}
func init() {
	var val UnlinkTwitchAccountRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("UnlinkTwitchAccountRequest", t, func() interface{} {
		return NewUnlinkTwitchAccountRequest()
	})
}

type UnlinkAmazonAccountRequest interface {
	__type()
	SetLocale(v *string)
	Locale() *string
	SetMarketplace(v *string)
	Marketplace() *string
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
	SetWithoutSendingEmail(v *bool)
	WithoutSendingEmail() *bool
	SetWithoutSendingSNSNotification(v *bool)
	WithoutSendingSNSNotification() *bool
	SetTestCall(v *bool)
	TestCall() *bool
}
type _UnlinkAmazonAccountRequest struct {
	Ị_AmazonAccount                 AmazonAccount `coral:"AmazonAccount" json:"AmazonAccount"`
	Ị_WithoutSendingEmail           *bool         `coral:"WithoutSendingEmail" json:"WithoutSendingEmail"`
	Ị_WithoutSendingSNSNotification *bool         `coral:"WithoutSendingSNSNotification" json:"WithoutSendingSNSNotification"`
	Ị_TestCall                      *bool         `coral:"TestCall" json:"TestCall"`
	Ị_locale                        *string       `coral:"locale" json:"locale"`
	Ị_marketplace                   *string       `coral:"marketplace" json:"marketplace"`
}

func (this *_UnlinkAmazonAccountRequest) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_UnlinkAmazonAccountRequest) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_UnlinkAmazonAccountRequest) WithoutSendingEmail() *bool {
	return this.Ị_WithoutSendingEmail
}
func (this *_UnlinkAmazonAccountRequest) SetWithoutSendingEmail(v *bool) {
	this.Ị_WithoutSendingEmail = v
}
func (this *_UnlinkAmazonAccountRequest) WithoutSendingSNSNotification() *bool {
	return this.Ị_WithoutSendingSNSNotification
}
func (this *_UnlinkAmazonAccountRequest) SetWithoutSendingSNSNotification(v *bool) {
	this.Ị_WithoutSendingSNSNotification = v
}
func (this *_UnlinkAmazonAccountRequest) TestCall() *bool {
	return this.Ị_TestCall
}
func (this *_UnlinkAmazonAccountRequest) SetTestCall(v *bool) {
	this.Ị_TestCall = v
}
func (this *_UnlinkAmazonAccountRequest) Locale() *string {
	return this.Ị_locale
}
func (this *_UnlinkAmazonAccountRequest) SetLocale(v *string) {
	this.Ị_locale = v
}
func (this *_UnlinkAmazonAccountRequest) Marketplace() *string {
	return this.Ị_marketplace
}
func (this *_UnlinkAmazonAccountRequest) SetMarketplace(v *string) {
	this.Ị_marketplace = v
}
func (this *_UnlinkAmazonAccountRequest) __type() {
}
func NewUnlinkAmazonAccountRequest() UnlinkAmazonAccountRequest {
	return &_UnlinkAmazonAccountRequest{}
}
func init() {
	var val UnlinkAmazonAccountRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("UnlinkAmazonAccountRequest", t, func() interface{} {
		return NewUnlinkAmazonAccountRequest()
	})
}

type GetLinkedAmazonAccountRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _GetLinkedAmazonAccountRequest struct {
	Ị_TwitchUserId *string `coral:"TwitchUserId" json:"TwitchUserId"`
}

func (this *_GetLinkedAmazonAccountRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_GetLinkedAmazonAccountRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_GetLinkedAmazonAccountRequest) __type() {
}
func NewGetLinkedAmazonAccountRequest() GetLinkedAmazonAccountRequest {
	return &_GetLinkedAmazonAccountRequest{}
}
func init() {
	var val GetLinkedAmazonAccountRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetLinkedAmazonAccountRequest", t, func() interface{} {
		return NewGetLinkedAmazonAccountRequest()
	})
}

type GetLinkedAmazonDirectedIdRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetIncludingCOR(v *bool)
	IncludingCOR() *bool
}
type _GetLinkedAmazonDirectedIdRequest struct {
	Ị_IncludingCOR *bool   `coral:"IncludingCOR" json:"IncludingCOR"`
	Ị_TwitchUserId *string `coral:"TwitchUserId" json:"TwitchUserId"`
}

func (this *_GetLinkedAmazonDirectedIdRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_GetLinkedAmazonDirectedIdRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_GetLinkedAmazonDirectedIdRequest) IncludingCOR() *bool {
	return this.Ị_IncludingCOR
}
func (this *_GetLinkedAmazonDirectedIdRequest) SetIncludingCOR(v *bool) {
	this.Ị_IncludingCOR = v
}
func (this *_GetLinkedAmazonDirectedIdRequest) __type() {
}
func NewGetLinkedAmazonDirectedIdRequest() GetLinkedAmazonDirectedIdRequest {
	return &_GetLinkedAmazonDirectedIdRequest{}
}
func init() {
	var val GetLinkedAmazonDirectedIdRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetLinkedAmazonDirectedIdRequest", t, func() interface{} {
		return NewGetLinkedAmazonDirectedIdRequest()
	})
}

type PublicAmazonAccount interface {
	__type()
	SetAmazonDirectedId(v *string)
	AmazonDirectedId() *string
	SetAmazonObfuscatedId(v *string)
	AmazonObfuscatedId() *string
	SetAmazonName(v *string)
	AmazonName() *string
	SetAmazonAccountPool(v *string)
	AmazonAccountPool() *string
	SetAmazonEmailAddress(v *string)
	AmazonEmailAddress() *string
	SetCountryOfResidence(v *string)
	CountryOfResidence() *string
	SetPreferredMarketplace(v *string)
	PreferredMarketplace() *string
}
type _PublicAmazonAccount struct {
	Ị_AmazonDirectedId     *string `coral:"AmazonDirectedId" json:"AmazonDirectedId"`
	Ị_AmazonObfuscatedId   *string `coral:"AmazonObfuscatedId" json:"AmazonObfuscatedId"`
	Ị_AmazonName           *string `coral:"AmazonName" json:"AmazonName"`
	Ị_AmazonAccountPool    *string `coral:"AmazonAccountPool" json:"AmazonAccountPool"`
	Ị_AmazonEmailAddress   *string `coral:"AmazonEmailAddress" json:"AmazonEmailAddress"`
	Ị_CountryOfResidence   *string `coral:"CountryOfResidence" json:"CountryOfResidence"`
	Ị_PreferredMarketplace *string `coral:"PreferredMarketplace" json:"PreferredMarketplace"`
}

func (this *_PublicAmazonAccount) CountryOfResidence() *string {
	return this.Ị_CountryOfResidence
}
func (this *_PublicAmazonAccount) SetCountryOfResidence(v *string) {
	this.Ị_CountryOfResidence = v
}
func (this *_PublicAmazonAccount) PreferredMarketplace() *string {
	return this.Ị_PreferredMarketplace
}
func (this *_PublicAmazonAccount) SetPreferredMarketplace(v *string) {
	this.Ị_PreferredMarketplace = v
}
func (this *_PublicAmazonAccount) AmazonDirectedId() *string {
	return this.Ị_AmazonDirectedId
}
func (this *_PublicAmazonAccount) SetAmazonDirectedId(v *string) {
	this.Ị_AmazonDirectedId = v
}
func (this *_PublicAmazonAccount) AmazonObfuscatedId() *string {
	return this.Ị_AmazonObfuscatedId
}
func (this *_PublicAmazonAccount) SetAmazonObfuscatedId(v *string) {
	this.Ị_AmazonObfuscatedId = v
}
func (this *_PublicAmazonAccount) AmazonName() *string {
	return this.Ị_AmazonName
}
func (this *_PublicAmazonAccount) SetAmazonName(v *string) {
	this.Ị_AmazonName = v
}
func (this *_PublicAmazonAccount) AmazonAccountPool() *string {
	return this.Ị_AmazonAccountPool
}
func (this *_PublicAmazonAccount) SetAmazonAccountPool(v *string) {
	this.Ị_AmazonAccountPool = v
}
func (this *_PublicAmazonAccount) AmazonEmailAddress() *string {
	return this.Ị_AmazonEmailAddress
}
func (this *_PublicAmazonAccount) SetAmazonEmailAddress(v *string) {
	this.Ị_AmazonEmailAddress = v
}
func (this *_PublicAmazonAccount) __type() {
}
func NewPublicAmazonAccount() PublicAmazonAccount {
	return &_PublicAmazonAccount{}
}
func init() {
	var val PublicAmazonAccount
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("PublicAmazonAccount", t, func() interface{} {
		return NewPublicAmazonAccount()
	})
}

type OAuthCodeAndState interface {
	__type()
	SetOAuthState(v *string)
	OAuthState() *string
	SetOAuthCode(v *string)
	OAuthCode() *string
}
type _OAuthCodeAndState struct {
	Ị_OAuthCode  *string `coral:"OAuthCode" json:"OAuthCode"`
	Ị_OAuthState *string `coral:"OAuthState" json:"OAuthState"`
}

func (this *_OAuthCodeAndState) OAuthCode() *string {
	return this.Ị_OAuthCode
}
func (this *_OAuthCodeAndState) SetOAuthCode(v *string) {
	this.Ị_OAuthCode = v
}
func (this *_OAuthCodeAndState) OAuthState() *string {
	return this.Ị_OAuthState
}
func (this *_OAuthCodeAndState) SetOAuthState(v *string) {
	this.Ị_OAuthState = v
}
func (this *_OAuthCodeAndState) __type() {
}
func NewOAuthCodeAndState() OAuthCodeAndState {
	return &_OAuthCodeAndState{}
}
func init() {
	var val OAuthCodeAndState
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("OAuthCodeAndState", t, func() interface{} {
		return NewOAuthCodeAndState()
	})
}

type TwitchAccount interface {
	__type()
	SetTwitchUserAvatar(v *string)
	TwitchUserAvatar() *string
	SetTwitchAccountType(v *string)
	TwitchAccountType() *string
	SetClientId(v *string)
	ClientId() *string
	SetTwitchUserID(v *string)
	TwitchUserID() *string
	SetTwitchUserName(v *string)
	TwitchUserName() *string
}
type _TwitchAccount struct {
	Ị_TwitchAccountType *string `coral:"TwitchAccountType" json:"TwitchAccountType"`
	Ị_ClientId          *string `coral:"ClientId" json:"ClientId"`
	Ị_TwitchUserID      *string `coral:"TwitchUserID" json:"TwitchUserID"`
	Ị_TwitchUserName    *string `coral:"TwitchUserName" json:"TwitchUserName"`
	Ị_TwitchUserAvatar  *string `coral:"TwitchUserAvatar" json:"TwitchUserAvatar"`
}

func (this *_TwitchAccount) ClientId() *string {
	return this.Ị_ClientId
}
func (this *_TwitchAccount) SetClientId(v *string) {
	this.Ị_ClientId = v
}
func (this *_TwitchAccount) TwitchUserID() *string {
	return this.Ị_TwitchUserID
}
func (this *_TwitchAccount) SetTwitchUserID(v *string) {
	this.Ị_TwitchUserID = v
}
func (this *_TwitchAccount) TwitchUserName() *string {
	return this.Ị_TwitchUserName
}
func (this *_TwitchAccount) SetTwitchUserName(v *string) {
	this.Ị_TwitchUserName = v
}
func (this *_TwitchAccount) TwitchUserAvatar() *string {
	return this.Ị_TwitchUserAvatar
}
func (this *_TwitchAccount) SetTwitchUserAvatar(v *string) {
	this.Ị_TwitchUserAvatar = v
}
func (this *_TwitchAccount) TwitchAccountType() *string {
	return this.Ị_TwitchAccountType
}
func (this *_TwitchAccount) SetTwitchAccountType(v *string) {
	this.Ị_TwitchAccountType = v
}
func (this *_TwitchAccount) __type() {
}
func NewTwitchAccount() TwitchAccount {
	return &_TwitchAccount{}
}
func init() {
	var val TwitchAccount
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("TwitchAccount", t, func() interface{} {
		return NewTwitchAccount()
	})
}

type GetTwitchUserInfoRequest interface {
	__type()
	SetClientIdRequired(v *bool)
	ClientIdRequired() *bool
	SetOAuthCodeAndState(v OAuthCodeAndState)
	OAuthCodeAndState() OAuthCodeAndState
	SetOAuthToken(v *string)
	OAuthToken() *string
}
type _GetTwitchUserInfoRequest struct {
	Ị_OAuthCodeAndState OAuthCodeAndState `coral:"OAuthCodeAndState" json:"OAuthCodeAndState"`
	Ị_OAuthToken        *string           `coral:"OAuthToken" json:"OAuthToken"`
	Ị_ClientIdRequired  *bool             `coral:"ClientIdRequired" json:"ClientIdRequired"`
}

func (this *_GetTwitchUserInfoRequest) OAuthCodeAndState() OAuthCodeAndState {
	return this.Ị_OAuthCodeAndState
}
func (this *_GetTwitchUserInfoRequest) SetOAuthCodeAndState(v OAuthCodeAndState) {
	this.Ị_OAuthCodeAndState = v
}
func (this *_GetTwitchUserInfoRequest) OAuthToken() *string {
	return this.Ị_OAuthToken
}
func (this *_GetTwitchUserInfoRequest) SetOAuthToken(v *string) {
	this.Ị_OAuthToken = v
}
func (this *_GetTwitchUserInfoRequest) ClientIdRequired() *bool {
	return this.Ị_ClientIdRequired
}
func (this *_GetTwitchUserInfoRequest) SetClientIdRequired(v *bool) {
	this.Ị_ClientIdRequired = v
}
func (this *_GetTwitchUserInfoRequest) __type() {
}
func NewGetTwitchUserInfoRequest() GetTwitchUserInfoRequest {
	return &_GetTwitchUserInfoRequest{}
}
func init() {
	var val GetTwitchUserInfoRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("TwitchAmazonIdentityLinkingService").Assembly("tv.justin.tails").RegisterShape("GetTwitchUserInfoRequest", t, func() interface{} {
		return NewGetTwitchUserInfoRequest()
	})
}
func init() {
	var val map[string]*float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*float64
		if f, ok := from.Interface().(map[string]*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to WithholdingRateMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []TwitchAccount
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []TwitchAccount
		if f, ok := from.Interface().([]TwitchAccount); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchAccounts")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//A service that manages links between Amazon and Twitch Accounts.
type TwitchAmazonIdentityLinkingService interface { //Link a twitch account to an amazon account.
	LinkAccounts(input LinkAccountsRequest) (LinkAccountsResponse, error)                      //Unlink a twitch account from its linked amazon account.
	UnlinkTwitchAccount(input UnlinkTwitchAccountRequest) (UnlinkTwitchAccountResponse, error) //Unlink a twitch account from its linked amazon account.
	UnlinkAmazonAccount(input UnlinkAmazonAccountRequest) (UnlinkAmazonAccountResponse, error)
	CheckLinkIntegrity(input CheckLinkIntegrityRequest) (CheckLinkIntegrityResponse, error) //Get twitch linked accounts from amazon account.
	//Optionally includes Twitch username and avatar (default is to include).
	GetLinkedTwitchAccounts(input GetLinkedTwitchAccountsRequest) (GetLinkedTwitchAccountsResponse, error) //Used to lookup select TIMS information for a partners account
	GetPayoutIdentity(input GetPayoutIdentityRequest) (GetPayoutIdentityResponse, error)
	SubmitDeskTicket(input SubmitDeskTicketRequest) (SubmitDeskTicketResponse, error)                            //Gets the Tax and Address information for a payout entity id from TIMS and Address Service.
	GetTaxInfoForPayoutEntity(input GetTaxInfoForPayoutEntityRequest) (GetTaxInfoForPayoutEntityResponse, error) //Get linked Amazon Account from TwitchUserID.
	GetLinkedAmazonAccount(input GetLinkedAmazonAccountRequest) (GetLinkedAmazonAccountResponse, error)          //Get linked Amazon DirectedId from TwitchUserID.
	//Optionally includes Amazon accounts COR (default is to not include).
	GetLinkedAmazonDirectedId(input GetLinkedAmazonDirectedIdRequest) (GetLinkedAmazonDirectedIdResponse, error) //Retrieves the Twitch login name and user ID for a user, given the OAuth code and state
	GetTwitchUserInfo(input GetTwitchUserInfoRequest) (GetTwitchUserInfoResponse, error)                         //Retrieves the Twitch user name given the Twitch user ID
	GetTwitchUserName(input GetTwitchUserNameRequest) (GetTwitchUserNameResponse, error)                         //Gets the tax withholding rate for a payout entity id from TIMS and Address Service.
	GetTaxWithholdingRateForPayoutEntity(input GetTaxWithholdingRateForPayoutEntityRequest) (GetTaxWithholdingRateForPayoutEntityResponse, error)
}
