/* Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package level

type Level int

const (
	Off Level = 1 + iota
	Trace
	Debug
	Info
	Warn
	Error
	Fatal
)

func LevelStr(level Level) string {
	switch level {
	case Off:
		return ""
	case Trace:
		return "[TRACE]"
	case Debug:
		return "[DEBUG]"
	case Info:
		return "[INFO]"
	case Warn:
		return "[WARN]"
	case Error:
		return "[ERROR]"
	case Fatal:
		return "[FATAL]"
	}
	return "[UNKNOWN]"
}
