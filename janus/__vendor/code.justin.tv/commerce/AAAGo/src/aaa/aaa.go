package aaa

import (
	"net/http"
	"strings"
)

// AAA HTTP Authorization and Date headers.
const (
	AaaAuthHeader = "x-amzn-Authorization"
	AaaDateHeader = "X-Amz-Date"
)

// Support combines the Client and Server functionality.
type Support interface {
	Client
	Server
}

// Client provides functionality necessary to perform requests against AAA-enabled
// services.
type Client interface {
	// Performs a SanityCheck to validate connectivity and protocol correctness.
	SanityCheck() error

	// Encodes the given http.Request with AAA signing and/or encryption.
	// This operation will mutate the request's Body, ContentLength, and Headers.
	// The encoding ClientContext is returned. The returned ClientContext must be used when decoding
	// the server's response. An error is returned if encoding of the request fails.
	EncodeRequest(service, operation string, r *http.Request) (*ClientContext, error)

	// Decodes the given http.Response that may or may not be AAA signed and/or encrypted.
	// This operation will mutate the response's Body, ContentLength, and Header.
	// An error is returned if decoding of the response fails.
	DecodeResponse(clientCxt *ClientContext, resp *http.Response) error
}

// Server provides functionality necessary to serve requests from AAA-enabled clients.
type Server interface {
	// DecodeRequest decodes the given http.Request that may or may not have AAA signing
	// and/or encryption.  If the request has AAA signing and/or encryption then this
	// operation will mutate the request's Body, ContentLength, and Headers.
	// The Decoding ServiceContext is returned for use in EncodeResponse.  An error
	// is returned if decoding the request fails.
	DecodeRequest(r *http.Request) (*ServiceContext, error)
	// AuthorizeRequest verifies that the given AAA request is authorized.  It is
	// the responsibility of the caller to populate the Service and Operation fields
	// of the ServiceContext before performing this call.  An error is returned if
	// authorizing the request fails.  If AuthorizeRequest is not called before EncodeResponse,
	// the call to EncodeResponse may fail.
	AuthorizeRequest(ctx *ServiceContext) (*AuthorizationResult, error)
	// EncodeResponse encodes the given http.Response with AAA signing and/or encryption
	// if the request was signed and/or encrypted. This operation will mutate the request's
	// Body, ContentLength, and Headers if the request was signed and/or encrypted.  An
	// error is returned if encoding of the response fails.
	EncodeResponse(ctx *ServiceContext, headers http.Header, body []byte) ([]byte, error)
}

// State parameters generated by EncodeRequest that must be preserved for DecodeResponse.
type ClientContext struct {
	KeyId      string
	KeyVersion uint64
	Service    string
	Operation  string
}

// State parameters generated by DecodeRequest that must be passed to AuthorizeRequest
// and EncodeResponse.
type ServiceContext struct {
	KeyId          string
	KeyVersion     uint64
	Service        string
	Operation      string
	RemoteIdentity string
	Signed         bool
	Encrypted      bool
}

type AuthorizationResult struct {
	AuthorizationCode AuthorizationCode
	Authorized        bool
	ErrorMessage      string
}

type AuthorizationCode string

const (
	AuthorizationCodeAuthorized  = "Authorized"
	AuthorizationCodeDenied      = "Denied"
	AuthorizationCodePassThrough = "PassThrough"
	AuthorizationCodeNone        = ""
)

var authCodeMap = map[string]AuthorizationCode{
	"authorized": AuthorizationCodeAuthorized, "denied": AuthorizationCodeDenied, "passthrough": AuthorizationCodePassThrough,
}

var authCodeIntMap = map[byte]AuthorizationCode{
	0x0: AuthorizationCodeAuthorized, 0x1: AuthorizationCodePassThrough, 0xA: AuthorizationCodeDenied,
}

// ToAuthorizationCode normalizes the given code and returns the corresponding
// AuthorizationCode.  If there are no matches, then AuthorizationCodeNone is
// returned.
func ToAuthorizationCode(code string) AuthorizationCode {
	code = strings.ToLower(code)
	code = strings.Replace(code, "_", "", -1)
	if ac, ok := authCodeMap[code]; ok {
		return ac
	}
	return AuthorizationCodeNone
}

// AuthFromByteCode turns the given authorization code byte into an AuthorizationCode.
// If no AuthorizationCode is found for the given byte, then AuthorizationCodeNone is
// returned.
func AuthFromByteCode(code byte) AuthorizationCode {
	if ac, ok := authCodeIntMap[code]; ok {
		return ac
	}
	return AuthorizationCodeNone
}
