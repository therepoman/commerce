package authv4

import (
	"time"
)

const (
	clockSkew       = time.Minute * 5 // this is how much difference we allow when valdiating dates, matches Java implementation
	iso8601BasicFmt = "20060102T150405Z"

	authv4Algorithm = "AWS4-HMAC-SHA256"

	defaultCacheExpiration  = time.Hour * 24 // the credentials them selves are limited to 24 hours, so this is max
	defaultMaxCachedClients = 500            // default max authenticated cached client objects in lru cache
)
