package manager

import "fmt"

// Copy will copy a secret from name source to name destination inside
// the same table. The user will need to able to read and decrypt the
// old secret along with encrypting and writing the new secret.
func (m *Manager) Copy(source, destination string) error {
	secret, err := m.Get(source)
	if err != nil {
		return err
	}
	if secret == nil {
		return fmt.Errorf("Source secret '%s' not found.", source)
	}

	secret.Name = destination
	return m.Post(secret)
}
