package manager

// TableName returns name of the DynamoDB main table name
func (m *Manager) TableName() string {
	return m.Config.TableName
}

// AuditTableName returns name of the DynamoDB audit table name
func (m *Manager) AuditTableName() string {
	return m.Config.TableName + "_audit"
}

// NamespaceTableName returns name of the DynamoDB namespace table name
func (m *Manager) NamespaceTableName() string {
	return m.Config.TableName + "_namespaces"
}
