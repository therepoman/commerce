package manager

import (
	"encoding/base64"
	"fmt"
	"path"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/systems/sandstorm/errors"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// SecretClass Type for secret Category
type SecretClass int8

const (
	// ClassUnknown Secret which have not been classfified yet.
	ClassUnknown SecretClass = iota //0
	// ClassPublic :  1
	ClassPublic
	// ClassPrivate : 2
	ClassPrivate
	// ClassConfidential : 3
	ClassConfidential
	// ClassSecret : 4
	ClassSecret
	// ClassTopSecret : 5
	ClassTopSecret
)

const jsonClassField = "class"

// Secret represents a secret. It can be either in the plain or
// enciphered.
type Secret struct {
	// Name of secret
	Name string `json:"name"`
	// Plaintext to be enciphered or read by user
	Plaintext []byte `json:"plaintext,omitempty"`
	// Unix timestamp of when the key was created/updated
	UpdatedAt int64 `json:"updated_at"`
	// Ciphertext to be deciphered or uploaded
	DoNotBroadcast bool `json:"do_not_broadcast"`
	// Tombstone signifies that a secret has been deleted
	Tombstone bool `json:"tombstone"`
	// KeyARN is the key used to encrypt
	KeyARN string `json:"key_arn"`

	// Class of the document. update const jsonClassField if you change the json field name.
	Class SecretClass `json:"class"`

	ciphertext []byte
	// Enciphered key returned by kms.GenerateDataKey
	key []byte
}

func unmarshalSecret(in map[string]*dynamodb.AttributeValue) (*Secret, error) {
	if raw, ok := in["tombstone"]; ok {
		if *raw.BOOL {
			return nil, errors.SecretTombstonedError{}
		}
	}

	var secret Secret
	var encodedCiphertext, encodedKey, updatedAtStr string
	var updatedAt int64
	var ciphertext, key []byte
	failedFields := make([]string, 0, 0)

	requiredField := []string{"name", "updated_at", "value", "key"}
	for _, fieldName := range requiredField {
		if _, ok := in[fieldName]; !ok {
			failedFields = append(failedFields, fieldName)
		}
	}
	if len(failedFields) != 0 {
		return nil, fmt.Errorf("Missing fields: %q", failedFields)
	}

	// At this point we have verified that all the required field are present.
	updatedAtStr = *in["updated_at"].N
	encodedCiphertext = *in["value"].S
	encodedKey = *in["key"].S

	//Start setting fields on the secret object.
	secret.Name = *in["name"].S

	// Grab simple fields first, since if these aren't populated we can just leave them empty/default
	if v, ok := in["tombstone"]; ok {
		secret.Tombstone = *v.BOOL
	}

	if v, ok := in["key_arn"]; ok {
		secret.KeyARN = *v.S
	}

	if v, ok := in["do_not_broadcast"]; ok {
		secret.DoNotBroadcast = *v.BOOL
	}

	if v, ok := in[jsonClassField]; ok {
		class, err := strconv.ParseInt(*v.N, 10, 8)
		if err != nil {
			// falling back to default value.
			secret.Class = ClassTopSecret
		}
		secret.Class = SecretClass(class)
	}

	ciphertext, err := base64.StdEncoding.DecodeString(encodedCiphertext)
	if err != nil {
		return nil, fmt.Errorf("Error base64 decoding ciphertext for secret %s: %s", secret.Name, err.Error())
	}
	secret.ciphertext = ciphertext

	key, err = base64.StdEncoding.DecodeString(encodedKey)
	if err != nil {
		return nil, fmt.Errorf("Error base64 decoding key for secret %s: %s", secret.Name, err.Error())
	}
	secret.key = key

	updatedAt, err = strconv.ParseInt(updatedAtStr, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("Error parsing updated_at for secret %s: %s", secret.Name, err.Error())
	}
	secret.UpdatedAt = updatedAt

	return &secret, nil
}

// Timestamp updates the UpdatedAt field to current unix timestamp
func (s *Secret) Timestamp() {
	s.UpdatedAt = time.Now().Unix()
}

// Seal takes a secret, timestamps it, creates an encryption context
// and passes this with m.Config.KeyID to envelope.Seal
func (m *Manager) Seal(secret *Secret) error {
	secret.Timestamp()
	key, ciphertext, keyARN, err := m.Envelope.Seal(m.Config.KeyID, secret.EncryptionContext(), secret.Plaintext)
	if err != nil {
		return err
	}
	// XXX perform base64 here?
	secret.key = key
	secret.ciphertext = ciphertext
	Zero(secret.Plaintext)
	secret.Plaintext = nil
	secret.KeyARN = keyARN
	return nil
}

// Decrypt the Ciphertext field and store it in Plaintext
func (m *Manager) Decrypt(secret *Secret) error {
	if secret == nil {
		return nil
	}
	plaintext, err := m.Envelope.Open(secret.key, secret.ciphertext, secret.EncryptionContext())
	if err != nil {
		return err
	}
	secret.Plaintext = plaintext
	return nil
}

// EncryptionContext generates the encryption context map to pass to
// kms.GenerateDataKey / kms.Decrypt.
func (s *Secret) EncryptionContext() (context map[string]string) {
	context = map[string]string{
		"name":       s.Name,
		"updated_at": strconv.FormatInt(s.UpdatedAt, 10),
	}
	return
}

// SecretNamespace contains information about the different parts of the name of a secret.
type SecretNamespace struct {
	Team        string `json:"team"`
	System      string `json:"system"`
	Environment string `json:"environment"`
	Name        string `json:"name"`
}

func (s SecretNamespace) String() string { return path.Join(s.Team, s.System, s.Environment, s.Name) }

// ParseSecretNamespace extracts namespace information from the name of a secret.
func ParseSecretNamespace(secretName string) (*SecretNamespace, error) {
	splitted := strings.SplitN(secretName, "/", 4)
	if len(splitted) != 4 {
		return nil, errors.InvalidRequestError{Message: fmt.Errorf("sandstorm: expected 4 namespace elements, got %d", len(splitted))}
	}
	sn := &SecretNamespace{
		Team:        splitted[0],
		System:      splitted[1],
		Environment: splitted[2],
		Name:        splitted[3],
	}
	return sn, nil
}
