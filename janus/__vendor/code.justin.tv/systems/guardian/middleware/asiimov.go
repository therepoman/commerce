package asiimov

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/derekdowling/go-stdlogger"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"

	"code.justin.tv/systems/guardian/guardian"

	"goji.io"
)

// NonceStore describes the api for generating and checking nonces
type NonceStore interface {
	// generate and store a new nonce with expiry
	Generate() (nonce string, err error)

	// checks store to see if nonce is exists, hasn't been used and has not
	// expired
	Check(nonce string) (valid bool, err error)
}

// Asiimov implements middleware for handling requests that include GuardiaN
// oauth access tokens
type Asiimov struct {
	logger           std.Logger
	oauthConfig      *oauth2.Config
	allowedGroups    map[string]struct{} //for existence check
	disallowedGroups map[string]struct{} //for existence check
	VerifyAccess     func(user *guardian.User) error
	CheckTokenURL    string
	NonceStore       NonceStore
}

// guardian endpoints
const (
	AuthURL       = "https://guardian.prod.us-west2.twitch.tv/authorize"
	TokenURL      = "https://guardian.prod.us-west2.twitch.tv/oauth2/token"
	CheckTokenURL = "https://guardian.prod.us-west2.twitch.tv/oauth2/check_token"

	TestAuthURL       = "https://guardian.dev.us-west2.twitch.tv/authorize"
	TestTokenURL      = "https://guardian.dev.us-west2.twitch.tv/oauth2/token"
	TestCheckTokenURL = "https://guardian.dev.us-west2.twitch.tv/oauth2/check_token"
)

// Endpoint is oauth2.Endpoint with guardian endpoints
var Endpoint = oauth2.Endpoint{
	AuthURL:  AuthURL,
	TokenURL: TokenURL,
}

// TestEndpoint is staging oauth2 endpoints
var TestEndpoint = oauth2.Endpoint{
	AuthURL:  TestAuthURL,
	TokenURL: TestTokenURL,
}

// New returns a new asiimov instance
func New(logger std.Logger, oauthConfig *oauth2.Config) (a *Asiimov) {
	a = new(Asiimov)
	if logger != nil {
		a.logger = logger
	} else {
		// default stdlib logger
		a.logger = log.New(os.Stderr, "", log.LstdFlags)
	}

	a.oauthConfig = oauthConfig

	if a.oauthConfig.Endpoint.AuthURL == "" {
		a.oauthConfig.Endpoint.AuthURL = AuthURL
	}

	if a.oauthConfig.Endpoint.TokenURL == "" {
		a.oauthConfig.Endpoint.TokenURL = TokenURL
	}

	a.CheckTokenURL = CheckTokenURL

	return
}

// UseTestEndpoints use guardian.dev enpdoints instead of guardian.prod for testing
func (a *Asiimov) UseTestEndpoints() {
	a.oauthConfig.Endpoint = TestEndpoint
	a.CheckTokenURL = TestCheckTokenURL
}

// NewTestAsiimov returns an asiimov object with test endpoints
func NewTestAsiimov(logger std.Logger, oauthConfig *oauth2.Config) (a *Asiimov) {
	a = New(logger, oauthConfig)
	a.UseTestEndpoints()
	return
}

// ErrNilToken is returned when a nil token is provided to CheckToken
var ErrNilToken = errors.New("asiimov: nil token passed to CheckToken")

// CheckToken makes a request to the /check_token endpoint and returns a
// TokenCheck containing token and user information. If the token is invalid,
// TokenCheck is nil.
func (a *Asiimov) CheckToken(token *oauth2.Token) (tc *guardian.TokenCheck, err error) {
	if token == nil {
		err = ErrNilToken
		return
	}

	client := a.oauthConfig.Client(context.Background(), token)
	resp, err := client.Get(a.CheckTokenURL)
	if err != nil {
		err = fmt.Errorf("asiimov: error checking token: %s", err.Error())
		return
	}
	if resp != nil {
		defer func() {
			closeErr := resp.Body.Close()
			if err == nil {
				err = closeErr
			}
		}()
	}

	switch resp.StatusCode {
	case http.StatusOK:
	case http.StatusNotFound:
		return
	default:
		err = fmt.Errorf("asiimov: error checking token: received %v status code from %v instead of %v", resp.StatusCode, a.CheckTokenURL, http.StatusOK)
		return
	}

	tc = new(guardian.TokenCheck)
	err = json.NewDecoder(resp.Body).Decode(tc)
	if err != nil {
		err = fmt.Errorf("asiimov: error decoding token: %s", err.Error())
		return
	}
	return
}

// HandleGuardianRequest parses oauth token from request and fetches user
// information associated with the token.
func (a *Asiimov) HandleGuardianRequest(r *http.Request) (user *guardian.User, err error) {
	token := GetToken(r)
	if token == "" {
		return nil, nil
	}

	oauth2Token := &oauth2.Token{AccessToken: token}
	tc, err := a.CheckToken(oauth2Token)
	if err != nil {
		return nil, err
	}

	if tc == nil || tc.User == nil {
		return
	}
	user = tc.User
	if a.VerifyAccess == nil {
		return nil, fmt.Errorf("asiimov: Required value (a *Asiimov).VerifyAccess is nil")
	}
	err = a.VerifyAccess(user)
	if err != nil {
		return nil, err
	}
	return user, err
}

// AskForAuth sets the WWW-Authenticate header to the provided realm. If realm
// is empty, 'guardian' is used. Also sets http.StatusUnauthorized.
func AskForAuth(w http.ResponseWriter, r *http.Request, realm string) {
	const guardianRealm = "guardian"
	if realm == "" {
		realm = guardianRealm
	}
	w.Header().Set("WWW-Authenticate", fmt.Sprintf("OAuth realm='%s'", realm))
	w.WriteHeader(http.StatusUnauthorized)
	return
}

// HandlerC implements goji middleware. The following values passed in context:
// * "user": *guardian.User object containing information on the user
//   associated with token.
// * "guardian_error": error object that's set if there's an error fetching
//   user information from guardian.
func (a *Asiimov) HandlerC(inner goji.Handler) (outer goji.Handler) {
	return goji.HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		user, err := a.HandleGuardianRequest(r)
		if err != nil {
			a.logger.Printf("error requesting user info from guardian: %s", err.Error())
			inner.ServeHTTPC(context.WithValue(ctx, "guardian_error", err), w, r)
			return
		}

		if user == nil {
			AskForAuth(w, r, "")
			inner.ServeHTTPC(ctx, w, r)
			return
		}

		inner.ServeHTTPC(context.WithValue(ctx, "user", user), w, r)
	})
}

// GetToken parses oauth token from Authorization http header
func GetToken(r *http.Request) (token string) {
	_, token, _ = r.BasicAuth()
	if token != "" {
		return token
	}

	authHeader := strings.Split(r.Header.Get("Authorization"), " ")
	if len(authHeader) == 2 && strings.EqualFold(authHeader[0], "bearer") {
		if authHeader[1] != "" {
			token = authHeader[1]
			return token
		}
	}

	return ""
}
