package guardian

// Identifier is a user management interface for identity services
type Identifier interface {
	GetUser(cn string) (user *User, err error)
	GetUserByName(username string) (user *User, err error)
	ListUsers() (users []*User, err error)

	GetGroup(cn string) (group *Group, err error)
	ListGroups() (groups []*Group, err error)

	// checks ldap username & password
	Authenticate(username, password string) (user *User, err error)
}

// User defines a user in LDAP
type User struct {
	CN            string   `json:"cn,omitempty"`
	UID           string   `json:"uid,omitempty"`
	GIDNumber     int64    `json:"gid_number,omitempty"`
	HomeDirectory string   `json:"home_dir,omitempty"`
	UIDNumber     int64    `json:"uid_number,omitempty"`
	Email         string   `json:"email,omitempty"`
	SSHPubkeys    []string `json:"ssh_pubkeys,omitempty"`
	// Groups is an array of LDAP Group CNs
	Groups []string `json:"groups,omitempty"`
}

// Group defines a group in LDAP
type Group struct {
	GID int64 `json:"gid,omitempty"`
	// "ops", "infra", etc
	CN          string   `json:"cn,omitempty"`
	Description string   `json:"description,omitempty"`
	Members     []string `json:"members,omitempty"`
}
