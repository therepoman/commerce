package main

import (
  "log"

  "code.justin.tv/common/twitchhttp"
  "code.justin.tv/web/app-settings/api"
  "code.justin.tv/common/config"
)

func main() {
  err := config.Parse()
  if err != nil {
    log.Fatal(err)
  }

  server, err := api.NewServer()
  if err != nil {
    log.Fatal(err)
  }

  log.Fatal(twitchhttp.ListenAndServe(server))
}
