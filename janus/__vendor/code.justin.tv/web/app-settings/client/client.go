package appsettings

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/common/twitchhttp"

	"golang.org/x/net/context"
)

const (
	defaultStatSampleRate = 1.0
	defaultTimingXactName = "app-settings"
)

// Error values
var (
	ErrNotFound    = errors.New("setting does not exist")
	ErrInvalidType = errors.New("invalid value type")
)

type showResponse struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

// Client is an HTTP client for making requests to the app settings service
type Client interface {
	GetBool(ctx context.Context, namespace, key string, reqOpts *twitchhttp.ReqOpts) (bool, error)
	Get(ctx context.Context, namespace, key string, reqOpts *twitchhttp.ReqOpts) (interface{}, error)
}

type client struct {
	twitchhttp.Client
}

// NewClient creates a new HTTP client for App Settings
func NewClient(conf twitchhttp.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = "app-settings"
	}

	twitchClient, err := twitchhttp.NewClient(conf)
	if err != nil {
		return nil, err
	}

	return &client{twitchClient}, nil
}

func (c client) GetBool(ctx context.Context, namespace, key string, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	val, err := c.Get(ctx, namespace, key, reqOpts)
	if err != nil {
		return false, err
	}

	boolean, ok := val.(bool)
	if !ok {
		return false, ErrInvalidType
	}

	return boolean, nil
}

func (c client) Get(ctx context.Context, namespace, key string, reqOpts *twitchhttp.ReqOpts) (interface{}, error) {
	path := fmt.Sprintf("/settings/%s/%s", url.QueryEscape(namespace), url.QueryEscape(key))

	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return "", err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "app-settings.get",
		StatSampleRate: defaultStatSampleRate,
	})

	var data *showResponse
	resp, err := c.DoJSON(ctx, &data, req, combinedReqOpts)
	if err != nil {
		return "", err
	}

	if resp.StatusCode == http.StatusNotFound {
		return "", ErrNotFound
	}

	return data.Value, nil
}
