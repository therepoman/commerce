package models

import (
	"fmt"
	"net/http"
)

var ErrMap = map[string]*CodedError{}
var codeErrs = []*CodedError{ErrDisplaynameNotAvailable, ErrLoginNotAvailable, ErrLoginBlocked, ErrNotAllowedToChangeLogin,
	ErrPartnerDoNotSupportGuid, ErrPartnerNotReachable, ErrFilteredUser, ErrIPBlacklisted}

var ErrDisplaynameNotAvailable = &CodedError{
	ErrorValue:      "The display name you selected is not available.",
	CodeValue:       "display_name_not_available",
	StatusCodeValue: http.StatusForbidden,
}
var ErrLoginNotAvailable = &CodedError{
	ErrorValue:      "The login you selected is not available.",
	CodeValue:       "login_not_available",
	StatusCodeValue: http.StatusForbidden,
}
var ErrLoginBlocked = &CodedError{
	ErrorValue:      "The login you selected is not yet available for re-use.",
	CodeValue:       "login_blocked",
	StatusCodeValue: http.StatusForbidden,
}
var ErrNotAllowedToChangeLogin = &CodedError{
	ErrorValue:      fmt.Sprintf("You are not allowed to change your login more than once every %d days.", LoginRenameCooldown),
	CodeValue:       "not_allowed_to_change_login",
	StatusCodeValue: http.StatusForbidden,
}
var ErrPartnerDoNotSupportGuid = &CodedError{
	ErrorValue:      "The partnership service does not support GUID user IDs",
	CodeValue:       "partner_not_support_guid",
	StatusCodeValue: http.StatusForbidden,
}
var ErrPartnerNotReachable = &CodedError{
	ErrorValue:      "The partnership service is unreachable",
	CodeValue:       "partner_not_reachable",
	StatusCodeValue: http.StatusForbidden,
}
var ErrFilteredUser = &CodedError{
	ErrorValue:      "User exists but was excluded by filter criteria.",
	CodeValue:       "filtered_user_requested",
	StatusCodeValue: http.StatusUnprocessableEntity,
}
var ErrIPBlacklisted = &CodedError{
	ErrorValue:      "The IP is blacklisted.",
	CodeValue:       "ip_blacklisted",
	StatusCodeValue: http.StatusUnprocessableEntity,
}

func init() {
	for _, e := range codeErrs {
		ErrMap[e.Code()] = e
	}
}
