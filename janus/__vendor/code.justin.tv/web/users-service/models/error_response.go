package models

import (
	"code.justin.tv/chat/golibs/errx"
	"fmt"
	"net/http"
)

const (
	StatusUnprocessableEntity = 422
	InternalError             = "InternalError"
)

type ErrorResponse struct {
	Status     int    `json:"status"`
	Message    string `json:"message"`
	StatusText string `json:"error"`
	ErrorCode  string `json:"error_code"`
}

func (T ErrorResponse) Error() string {
	return fmt.Sprintf("Error (%d): %s", T.Status, T.Message)
}

func NewErrorResponse(status int, err error) ErrorResponse {
	statusText := http.StatusText(status)
	if statusText == "" {
		statusText = ExtentionStatusText(status)
	}
	return ErrorResponse{
		Status:     status,
		Message:    err.Error(),
		StatusText: statusText,
		ErrorCode:  ErrorCode(err),
	}
}

func ExtentionStatusText(code int) string {
	return extentionStatusText[code]
}

// extentionStatusText supports extra status codes that the stdlib http package does not.
var extentionStatusText = map[int]string{
	StatusUnprocessableEntity: "Unprocessable entity",
}

func ErrorCode(err error) string {
	if e, ok := errx.Unwrap(err).(ServiceError); ok {
		return e.Code()
	} else {
		return InternalError
	}
}
