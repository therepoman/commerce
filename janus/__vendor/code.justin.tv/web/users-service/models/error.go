package models

import (
	"net/http"
)

type ServiceError interface {
	Code() string
	Error() string
	StatusCode() int
	ShouldLog() bool
}

type CodedError struct {
	CodeValue       string
	ErrorValue      string
	StatusCodeValue int
	ShouldLogValue  bool
}

func (e CodedError) Code() string  { return e.CodeValue }
func (e CodedError) Error() string { return e.ErrorValue }
func (e CodedError) StatusCode() int {
	if e.StatusCodeValue == 0 {
		return http.StatusInternalServerError
	}
	return e.StatusCodeValue
}
func (e CodedError) ShouldLog() bool { return e.ShouldLogValue }
