package models

import (
	"time"

	"errors"
	"strconv"

	"code.justin.tv/common/yimg"
)

type FilterParams struct {
	IDs          []string
	Logins       []string
	Emails       []string
	DisplayNames []string
	Ips          []string

	NotDeleted      bool
	NoTOSViolation  bool
	NoDMCAViolation bool
}

const (
	NotDeletedParam      = "not_deleted"
	NoTOSViolationParam  = "no_tos_violation"
	NoDMCAViolationParam = "no_dmca_violation"

	LoginRenameCooldown = 60 // Number of days before you can rename login
)

type PropertiesResult struct {
	Results []*Properties `json:"results"`
}

type Properties struct {
	ID                      string       `json:"id" `
	Login                   *string      `json:"login" `
	Birthday                *time.Time   `json:"birthday" `
	DmcaViolation           *bool        `json:"dmca_violation" `
	TermsOfServiceViolation *bool        `json:"terms_of_service_violation" `
	DeletedOn               *time.Time   `json:"deleted_on" `
	Language                *string      `json:"language" `
	Category                *string      `json:"category" `
	RemoteIP                *string      `json:"remote_ip" `
	Email                   *string      `json:"email" `
	LastLogin               *string      `json:"last_login" `
	BannedUntil             *time.Time   `json:"banned_until" `
	DmcaViolationCount      *int         `json:"dmca_violation_count" `
	TosViolationCount       *int         `json:"tos_violation_count" `
	Admin                   *bool        `json:"admin" `
	Subadmin                *bool        `json:"subadmin" `
	GlobalMod               *bool        `json:"global_mod" `
	Displayname             *string      `json:"displayname" `
	Description             *string      `json:"description" `
	ProfileImage            *yimg.Images `json:"profile_image" `
	UpdatedOn               *time.Time   `json:"updated_on" `
	CreatedOn               *time.Time   `json:"created_on" `
	EmailVerified           *bool        `json:"email_verified" `
	PhoneNumber             *string      `json:"phone_number" `
	LastLoginChangeDate     *time.Time   `json:"last_login_change_date" `
}

const (
	UserIDCacheKey          = "u.id"
	UserLoginCacheKey       = "login"
	UserDisplaynameCacheKey = "displayname"
)

func (p Properties) CachePairs() []CachePair {
	var pairs []CachePair

	if p.ID != "" {
		pairs = append(pairs, CachePair{
			Key:   UserIDCacheKey,
			Value: p.ID,
		})
	}
	if p.Login != nil && *p.Login != "" {
		pairs = append(pairs, CachePair{
			Key:   UserLoginCacheKey,
			Value: *p.Login,
		})
	}
	if p.Displayname != nil && *p.Displayname != "" {
		pairs = append(pairs, CachePair{
			Key:   UserDisplaynameCacheKey,
			Value: *p.Displayname,
		})
	}

	return pairs
}

type PropertiesIterator []Properties

func (i PropertiesIterator) Each(f func(Cacheable) error) error {
	for _, prop := range i {
		if err := f(prop); err != nil {
			return err
		}
	}

	return nil
}

func (p Properties) ConvertToIntIDProperties() (IntIDProperties, error) {
	i, err := strconv.ParseUint(p.ID, 10, 64)
	if err != nil {
		return IntIDProperties{}, errors.New("Unable to return string user id as int")
	}
	return IntIDProperties{
		ID:                      i,
		Login:                   p.Login,
		Birthday:                p.Birthday,
		DmcaViolation:           p.DmcaViolation,
		TermsOfServiceViolation: p.TermsOfServiceViolation,
		DeletedOn:               p.DeletedOn,
		Language:                p.Language,
		Category:                p.Category,
		RemoteIP:                p.RemoteIP,
		Email:                   p.Email,
		LastLogin:               p.LastLogin,
		BannedUntil:             p.BannedUntil,
		DmcaViolationCount:      p.DmcaViolationCount,
		TosViolationCount:       p.TosViolationCount,
		Admin:                   p.Admin,
		Subadmin:                p.Subadmin,
		GlobalMod:               p.GlobalMod,
		Displayname:             p.Displayname,
		Description:             p.Description,
		ProfileImage:            p.ProfileImage,
		UpdatedOn:               p.UpdatedOn,
		CreatedOn:               p.CreatedOn,
		EmailVerified:           p.EmailVerified,
		PhoneNumber:             p.PhoneNumber,
		LastLoginChangeDate:     p.LastLoginChangeDate,
	}, nil
}

type IntIDProperties struct {
	ID                      uint64       `json:"id" `
	Login                   *string      `json:"login" `
	Birthday                *time.Time   `json:"birthday" `
	DmcaViolation           *bool        `json:"dmca_violation" `
	TermsOfServiceViolation *bool        `json:"terms_of_service_violation" `
	DeletedOn               *time.Time   `json:"deleted_on" `
	Language                *string      `json:"language" `
	Category                *string      `json:"category" `
	RemoteIP                *string      `json:"remote_ip" `
	Email                   *string      `json:"email" `
	LastLogin               *string      `json:"last_login" `
	BannedUntil             *time.Time   `json:"banned_until" `
	DmcaViolationCount      *int         `json:"dmca_violation_count" `
	TosViolationCount       *int         `json:"tos_violation_count" `
	Admin                   *bool        `json:"admin" `
	Subadmin                *bool        `json:"subadmin" `
	GlobalMod               *bool        `json:"global_mod" `
	Displayname             *string      `json:"displayname" `
	Description             *string      `json:"description" `
	ProfileImage            *yimg.Images `json:"profile_image" `
	UpdatedOn               *time.Time   `json:"updated_on" `
	CreatedOn               *time.Time   `json:"created_on" `
	EmailVerified           *bool        `json:"email_verified" `
	PhoneNumber             *string      `json:"phone_number" `
	LastLoginChangeDate     *time.Time   `json:"last_login_change_date" `
}

type RenameProperties struct {
	RenameEligible   *bool      `json:"rename_eligible"`
	RenameEligibleOn *time.Time `json:"rename_eligible_on"`
}

type BlockProperties struct {
	ID                      string  `json:"id" `
	Login                   *string `json:"login" `
	RenamedLoginBlockRecord *bool   `json:"renamed_login_block_record" `
}

type ImageProperties struct {
	ID                  string       `json:"id" `
	ProfileBanner       *yimg.Images `json:"profile_banner" `
	ChannelOfflineImage *yimg.Images `json:"channel_offline_image" `
	ProfileImage        *yimg.Images `json:"profile_image" `
}

type UpdateableProperties struct {
	ID                 string     `json:"-" `
	Login              *string    `json:"-" `
	Birthday           *time.Time `json:"birthday" `
	Email              *string    `json:"email" `
	Displayname        *string    `json:"displayname" `
	Language           *string    `json:"language" `
	Description        *string    `json:"description"`
	EmailVerified      *bool      `json:"email_verified"`
	NewLogin           *string    `json:"new_login"`
	SkipLoginCooldown  *bool      `json:"skip_login_cooldown"`
	OverrideLoginBlock *bool      `json:"override_login_block"`
	LastLogin          *string    `json:"last_login"`

	ReleaseDateDuration time.Duration `json:"-"`
}

type CreateUserProperties struct {
	Login    string   `json:"login" validate:"nonzero"`
	IP       string   `json:"ip"`
	Birthday Birthday `json:"birthday" validate:"nonzero"`
	Email    string   `json:"email" validate:"nonzero"`
	DeviceID string   `json:"device_id"`
	Language string   `json:"language"`
	Category string   `json:"category"`
}

type Birthday struct {
	Day   int        `json:"day" validate:"nonzero"`
	Month time.Month `json:"month" validate:"nonzero"`
	Year  int        `json:"year" validate:"nonzero"`
}

func (b *Birthday) ToDate() time.Time {
	return time.Date(b.Year, b.Month, b.Day, 0, 0, 0, 0, time.Local)
}

func (u *UpdateableProperties) FillFromProperties(p *Properties) {
	u.ID = p.ID
	u.Login = p.Login

	if u.Email == nil {
		u.Email = p.Email
	}
	if u.Displayname == nil {
		u.Displayname = p.Displayname
	}
	if u.Language == nil {
		u.Language = p.Language
	}
	if u.Description == nil {
		u.Description = p.Description
	}
	if u.EmailVerified == nil {
		u.EmailVerified = p.EmailVerified
	}
	if u.LastLogin == nil {
		u.LastLogin = p.LastLogin
	}
}
