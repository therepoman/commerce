package models

type LoginRenameEvent struct {
	UserID   string `json:"user_id"`
	OldLogin string `json:"old_login"`
	NewLogin string `json:"new_login"`
}

type DisplaynameChangeEvent struct {
	UserID         string `json:"user_id"`
	OldDisplayname string `json:"old_displayname"`
	NewDisplayname string `json:"new_displayname"`
}

type SignupEvent struct {
	UserID   string `json:"user_id"`
	Login    string `json:"login"`
	IP       string `json:"ip"`
	DeviceID string `json:"device_id"`
}
