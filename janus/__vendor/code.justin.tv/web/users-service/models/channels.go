package models

import (
	"fmt"
	"time"

	"code.justin.tv/common/yimg"
)

type ChannelProperties struct {
	ID                  uint64     `json:"id"`
	Name                string     `json:"name"`
	DirectoryHidden     bool       `json:"directory_hidden"`
	Broadcaster         *string    `json:"broadcaster"`
	BroadcasterLanguage *string    `json:"broadcaster_language"`
	BroadcasterSoftware *string    `json:"broadcaster_software"`
	CreatedOn           *time.Time `json:"created_on"`
	Game                *string    `json:"game"`
	GameID              *uint64    `json:"game_id"`
	Mature              *bool      `json:"mature"`
	Status              *string    `json:"status"`
	Title               *string    `json:"title"`
	UpdatedOn           *time.Time `json:"updated_on"`
	ViewsCount          *uint64    `json:"views_count"`

	BlockNonPublicAds *bool `json:"block_non_public_ads"`
	PrerollsDisabled  *bool `json:"prerolls_disabled"`
	PostrollsDisabled *bool `json:"postrolls_disabled"`
	FightAdBlock      *bool `json:"fight_ad_block"`

	CanRebroadcast           *bool      `json:"can_rebroadcast"`
	DelayControlsEnabled     *bool      `json:"delay_controls_enabled"`
	LastBroadcastTime        *time.Time `json:"last_broadcast_time"`
	LastBroadcastID          *string    `json:"last_broadcast_id"`
	LastLiveNotificationSent *time.Time `json:"last_live_notification_sent"`

	CanCreateOfflinePlaylist *bool   `json:"can_create_offline_playlist"`
	About                    *string `json:"about"`
	RedirectChannel          *string `json:"redirect_channel"`
	PrimaryTeamID            *uint64 `json:"primary_team_id"`
	DisableChat              *bool   `json:"disable_chat"`

	ChannelOfflineImage          yimg.Images `json:"channel_offline_image"`
	ProfileBanner                yimg.Images `json:"profile_banner"`
	ProfileBannerBackgroundColor *string     `json:"profile_banner_background_color"`
}

const (
	ChannelsIDCacheKey    = "u.id"
	ChannelsLoginCacheKey = "login"
)

func ChannelIDToString(id uint64) string {
	return fmt.Sprintf("%d", id)
}

func (p ChannelProperties) CachePairs() []CachePair {
	var pairs []CachePair

	if p.ID != 0 {
		pairs = append(pairs, CachePair{
			Key:   ChannelsIDCacheKey,
			Value: ChannelIDToString(p.ID),
		})
	}

	if p.Name != "" {
		pairs = append(pairs, CachePair{
			Key:   ChannelsLoginCacheKey,
			Value: p.Name,
		})
	}

	return pairs
}

type ChannelPropertiesIterator []ChannelProperties

func (i ChannelPropertiesIterator) Each(f func(Cacheable) error) error {
	for _, prop := range i {
		if err := f(prop); err != nil {
			return err
		}
	}

	return nil
}
