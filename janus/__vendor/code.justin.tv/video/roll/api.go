package roll

import (
	"errors"
)

type rollbarSuccess struct {
	Result map[string]string `json:"result"`
}

// see https://rollbar.com/docs/api/items_post/
type apiItem struct {
	AccessToken string      `json:"access_token"`
	Data        apiItemData `json:"data"`
}

type apiItemData struct {
	Environment string           `json:"environment"`
	Body        apiItemBody      `json:"body"`
	Level       string           `json:"level,omitempty"`
	Timestamp   int64            `json:"timestamp,omitempty"`
	CodeVersion string           `json:"code_version,omitempty"`
	Platform    string           `json:"platform,omitempty"`
	Language    string           `json:"language,omitempty"`
	Framework   string           `json:"framework,omitempty"`
	Context     string           `json:"context,omitempty"`
	Request     *apiItemRequest  `json:"request,omitempty"`
	Person      *apiItemPerson   `json:"person,omitempty"`
	Server      *apiItemServer   `json:"server,omitempty"`
	Client      *apiItemClient   `json:"client,omitempty"`
	Custom      apiItemCustom    `json:"custom,omitempty"`
	Fingerprint string           `json:"fingerprint,omitempty"`
	Title       string           `json:"title,omitempty"`
	UUID        string           `json:"uuid,omitempty"`
	Notifier    *apiItemNotifier `json:"notifier,omitempty"`
}

type apiItemBody struct {
	Trace       *apiItemTrace       `json:"trace,omitempty"`
	TraceChain  []*apiItemTrace     `json:"trace_chain,omitempty"`
	Message     *apiItemMessage     `json:"message,omitempty"`
	CrashReport *apiItemCrashReport `json:"crash_report,omitempty"`
}

type apiItemTrace struct {
	Frames    []apiItemTraceFrame   `json:"frames"`
	Exception apiItemTraceException `json:"exception"`
}

type apiItemTraceFrame struct {
	FileName     string                    `json:"filename"`
	LineNumber   int                       `json:"lineno,omitempty"`
	ColumnNumber int                       `json:"colno,omitempty"`
	Method       string                    `json:"method,omitempty"`
	Code         string                    `json:"code,omitempty"`
	ClassName    string                    `json:"class_name,omitempty"`
	Context      *apiItemTraceFrameContext `json:"context,omitempty"`
	ArgSpec      []string                  `json:"argspec,omitempty"`
	VarArgSpec   []string                  `json:"varargspec,omitempty"`
	KeywordSpec  string                    `json:"keywordspec,omitempty"`
	Locals       map[string]interface{}    `json:"locals,omitempty"`
}

type apiItemTraceFrameContext struct {
	Pre  []string `json:"pre,omitempty"`
	Post []string `json:"post,omitempty"`
}

type apiItemTraceException struct {
	Class       string `json:"class"`
	Message     string `json:"message,omitempty"`
	Description string `json:"descriptor,omitempty"`
}

type apiItemMessage struct {
	Body string `json:"body"`
}

type apiItemCrashReport struct {
	Raw string `json:"raw"`
}

type apiItemRequest struct {
	Url         string            `json:"url,omitempty"`
	Method      string            `json:"method,omitempty"`
	Headers     map[string]string `json:"headers,omitempty"`
	RouteParams map[string]string `json:"params,omitempty"`
	GetParams   map[string]string `json:"GET,omitempty"`
	PostParams  map[string]string `json:"POST,omitempty"`
	QueryString string            `json:"query_string,omitempty"`
	Body        string            `json:"body,omitempty"`
	UserIp      string            `json:"user_ip,omitempty"`
}

type apiItemPerson struct {
	Id       string `json:"id"`
	Username string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
}

type apiItemServer struct {
	Host        string `json:"host,omitempty"`
	Root        string `json:"root,omitempty"`
	Branch      string `json:"branch,omitempty"`
	CodeVersion string `json:"code_version,omitempty"`
}

type apiItemClient struct {
	Javascript *apiItemClientJs `json:"javascript,omitempty"`
}

type apiItemClientJs struct {
	Browser             string `json:"host,omitempty"`
	CodeVersion         string `json:"code_version,omitempty"`
	SourceMapEnabled    bool   `json:"source_map_enabled,omitempty"`
	GuessUncaughtFrames bool   `json:"guess_uncaught_frames,omitempty"`
}

type apiItemCustom map[string]interface{}

type apiItemNotifier struct {
	Name    string `json:"name,omitempty"`
	Version string `json:"version,omitempty"`
}

type apiItemResponse struct {
	Err     int    `json:"err"`
	Message string `json:message,omitempty"`
}

func (i *apiItem) Verify() (err error) {
	if i.AccessToken == "" {
		return errors.New("missing access token")
	}

	if i.Data.Environment == "" {
		return errors.New("missing enviroment")
	}

	hasTrace := (i.Data.Body.Trace != nil)
	hasTraceChain := (len(i.Data.Body.TraceChain) != 0)
	hasMessage := (i.Data.Body.Message != nil)
	hasCrashReport := (i.Data.Body.CrashReport != nil)

	// Seriously Go?
	sum := 0
	if hasTrace {
		sum += 1
	}

	if hasTraceChain {
		sum += 1
	}

	if hasMessage {
		sum += 1
	}

	if hasCrashReport {
		sum += 1
	}

	if sum == 0 {
		return errors.New("missing body: one trace, trace_chain, message, or crash_report required")
	}

	if sum > 1 {
		return errors.New("too many bodies: only one trace, trace_chain, message, or crash_report allowed")
	}

	if hasTrace || hasTraceChain {
		traceChain := i.Data.Body.TraceChain
		if len(traceChain) == 0 {
			traceChain = []*apiItemTrace{i.Data.Body.Trace}
		}

		for _, trace := range traceChain {
			if len(trace.Frames) == 0 {
				return errors.New("missing trace frames")
			}

			for _, frame := range trace.Frames {
				if frame.FileName == "" {
					return errors.New("missing trace frame file name")
				}
			}

			if trace.Exception.Class == "" {
				return errors.New("missing trace frame exception class")
			}
		}
	}

	if hasMessage {
		if i.Data.Body.Message.Body == "" {
			return errors.New("missing message body")
		}
	}

	if hasCrashReport {
		if i.Data.Body.CrashReport.Raw == "" {
			return errors.New("missing crash report")
		}
	}

	return nil
}
