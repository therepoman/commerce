package roll

import (
	"github.com/pkg/errors"
)

type Level string

const CRITICAL Level = "critical"
const ERROR Level = "error"
const WARNING Level = "warning"
const INFO Level = "info"
const DEBUG Level = "debug"

type causer interface {
	Cause() error
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}
