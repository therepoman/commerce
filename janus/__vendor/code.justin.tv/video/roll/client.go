package roll

import (
	"net/http"
	"runtime"
	"time"

	"github.com/google/uuid"
)

type Client struct {
	endpoint string
	token    string
	env      string
	host     string
	version  string

	httpClient *http.Client
}

func New(token string, env string) (c *Client) {
	c = new(Client)

	c.endpoint = "https://api.rollbar.com/api/1/item/"
	c.token = token
	c.env = env

	return c
}

func (c *Client) NewItem() (i *Item) {
	i = new(Item)

	i.SetEndpoint(c.endpoint)
	i.SetToken(c.token)
	i.SetEnvironment(c.env)

	i.SetLanguage("go")
	i.SetPlatform(runtime.GOOS)
	i.SetClientName("go-roll")
	i.SetClientVersion("0.1")

	i.SetTimestamp(time.Now())
	i.SetUUID(uuid.New().String())

	if c.host != "" {
		i.SetHost(c.host)
	}

	if c.version != "" {
		i.SetVersion(c.version)
	}

	i.SetHttpClient(c.httpClient)

	return i
}

func (c *Client) SetEndpoint(endpoint string) {
	c.endpoint = endpoint
}

func (c *Client) SetToken(token string) {
	c.token = token
}

func (c *Client) SetEnvironment(env string) {
	c.env = env
}

func (c *Client) SetHost(host string) {
	c.host = host
}

func (c *Client) SetVersion(version string) {
	c.version = version
}

func (c *Client) SetHttpClient(client *http.Client) {
	c.httpClient = client
}
