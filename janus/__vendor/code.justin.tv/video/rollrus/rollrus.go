package rollrus

import (
	"context"
	"fmt"
	"net/http"
	"sync/atomic"
	"time"

	"code.justin.tv/video/roll"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type stackSkipError struct {
	Err  error
	Skip int
}

func (err *stackSkipError) Error() string {
	return err.Err.Error()
}

func (err *stackSkipError) Cause() error {
	type causer interface {
		Cause() error
	}

	if cerr, ok := err.Err.(causer); ok {
		return cerr.Cause()
	}

	return err.Err
}

func (err *stackSkipError) StackTrace() errors.StackTrace {
	type stackTracer interface {
		StackTrace() errors.StackTrace
	}

	if serr, ok := err.Err.(stackTracer); ok {
		return serr.StackTrace()[err.Skip:]
	}

	return nil
}

type Hook struct {
	client *roll.Client
	level  log.Level

	timeout     time.Duration
	sync        bool
	concurrency int64

	active int64
}

func NewHook(client *roll.Client, level log.Level) *Hook {
	return &Hook{
		client: client,
		level:  level,

		sync:        false,
		timeout:     4 * time.Second,
		concurrency: 1000,
	}
}

// The maximum amount of time to wait for the rollbar response.
// Defaults to 4 seconds.
func (h *Hook) SetTimeout(timeout time.Duration) {
	h.timeout = timeout
}

// Run the hook synchronously instead of spawning a new go routine.
// This should only be set to true when using `logrus.SetNoLock()`
// Defaults to false.
func (h *Hook) SetSync(sync bool) {
	h.sync = sync
}

// Set the maximum number of concurrent reports.
// Defaults to 1000.
func (h *Hook) SetConcurrency(max int64) {
	h.concurrency = max
}

// Fire the hook. This is called by Logrus for entries that match the levels
// returned by Levels(). See below.
func (h *Hook) Fire(entry *log.Entry) (err error) {
	// Prevent infinite loops.
	_, ok := entry.Data["rollrus"]
	if ok {
		return nil
	}

	item := h.client.NewItem()

	// Logrus has a global mutex for hooks, so we default to async.
	// This has the downside of ignoring errors and failing to report fatal/panic.
	// We force synchronous requests for those log levels to compensate.
	sync := h.sync

	switch entry.Level {
	case log.FatalLevel:
		item.SetLevel(roll.CRITICAL)
		sync = true
	case log.PanicLevel:
		item.SetLevel(roll.CRITICAL)
		sync = true
	case log.ErrorLevel:
		item.SetLevel(roll.ERROR)
	case log.WarnLevel:
		item.SetLevel(roll.WARNING)
	case log.InfoLevel:
		item.SetLevel(roll.INFO)
	case log.DebugLevel:
		item.SetLevel(roll.DEBUG)
	}

	cause := extractError(entry, "error")
	if cause != nil {
		cause = errors.Wrap(cause, entry.Message)
		cause = &stackSkipError{Err: cause, Skip: 4}

		item.SetError(cause)

		// Rollbar includes the error class in the default title.
		// We don't care in go because it's normally errors.stringErr
		item.SetTitle(cause.Error())

		// Let the roll library calculate a fingerprint based on the error string.
		item.SetFingerprintAuto()
	} else {
		item.SetMessage(entry.Message)
	}

	request := extractRequest(entry, "request")
	if request != nil {
		item.SetRequest(request)
	}

	extras := filterExtras(entry.Data, "error", "request")
	for k, v := range extras {
		item.SetCustom(k, v)
	}

	// Add 1 to the number of active sends
	active := atomic.AddInt64(&h.active, 1)

	// Check if we've gone over the concurrency limit
	if h.concurrency > 0 && active > h.concurrency {
		atomic.AddInt64(&h.active, -1)
		return errors.New("max concurrency")
	}

	ctx, cancel := context.WithTimeout(context.Background(), h.timeout)

	// If this is a synchronous, do it inline.
	if sync {
		defer atomic.AddInt64(&h.active, -1)
		defer cancel()

		return item.Send(ctx)
	}

	// If it's asynchronous, we spawn a new goroutine.
	go func() {
		defer atomic.AddInt64(&h.active, -1)
		defer cancel()

		err := item.Send(ctx)
		if err == nil {
			return
		}

		// Log the error.
		entry.Logger.WithFields(log.Fields{
			"error":   err,
			"rollrus": true, // Prevent infinite loops.
		}).Warn("failed to send to rollbar")
	}()

	return nil
}

func (h *Hook) Levels() (levels []log.Level) {
	for _, level := range log.AllLevels {
		if level <= h.level {
			levels = append(levels, level)
		}
	}

	return levels
}

// extractError attempts to extract an error from the given field
func extractError(entry *log.Entry, key string) (err error) {
	e, ok := entry.Data[key]
	if !ok {
		return nil
	}

	err, ok = e.(error)
	if !ok {
		return nil
	}

	if err == nil {
		return nil
	}

	// Replace the error with a text version just for printing.
	entry.Data[key] = err.Error()

	return err
}

// extractRequest attempts to extract an http.Request from a well known field, request
func extractRequest(entry *log.Entry, key string) (request *http.Request) {
	e, ok := entry.Data[key]
	if !ok {
		return nil
	}

	request, ok = e.(*http.Request)
	if !ok {
		return nil
	}

	// Replace the request with a text version just for printing.
	entry.Data[key] = fmt.Sprintf("%s %s", request.Method, request.URL.String())

	return request
}

// Remove the given keys from the extras map.
func filterExtras(extras map[string]interface{}, keys ...string) (newExtras map[string]interface{}) {
	newExtras = make(map[string]interface{})

	for k, v := range extras {
		newExtras[k] = v
	}

	for _, k := range keys {
		delete(newExtras, k)
	}

	return newExtras
}
