package twitchhttp

type ReqOpts struct {
	// AuthorizationToken is the token received from our Authorization service
	// which can be forwarded to other Twitch services. Do not set this if
	// the request leaves the Twitch infrastructure.
	AuthorizationToken string

	// Name of request timing stats. Stats are emitted in the format
	// {StatName}.{Response.StatusCode} or {StatName}.0 (for errors).
	StatName string

	// Sample rate of successful request stats, errors are never sampled.
	StatSampleRate float32
}

func MergeReqOpts(input *ReqOpts, defaults ReqOpts) ReqOpts {
	if input == nil {
		return defaults
	}
	return ReqOpts{
		StatName:       mergeString(input.StatName, defaults.StatName),
		StatSampleRate: mergeFloat32(input.StatSampleRate, defaults.StatSampleRate),
	}
}

func mergeString(input, defaults string) string {
	if input != "" {
		return input
	}
	return defaults
}

func mergeFloat32(input, defaults float32) float32 {
	if input != 0 {
		return input
	}
	return defaults
}
