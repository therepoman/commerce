package yimg

import (
	"fmt"
)

const profileBannerFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/%s-profile_banner-{{.Uid}}-{{.Size}}.{{.Format}}"

var profileBannerSizes = []string{"480"}

// ProfileBanners converts database yaml into an Images map for the provided username
func ProfileBanners(data []byte, username string) (Images, error) {
	return parse(fmt.Sprintf(profileBannerFormat, username), data, profileBannerSizes)
}

func ProfileBannersName(image Image, username string) (string, error) {
	return fmt.Sprintf("%s-profile_banner-%s-%s.%s", username, image.Uid, image.Size, image.Format), image.validate()
}
