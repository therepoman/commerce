package yimg

import (
	"fmt"
)

const profileImageFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/%s-profile_image-{{.Uid}}-{{.Size}}.{{.Format}}"

var profileImageSizes = []string{"600x600", "300x300", "150x150", "70x70", "50x50", "28x28"}

// ProfileImages converts database yaml into an Images map for the provided username
func ProfileImages(data []byte, username string) (Images, error) {
	return parse(fmt.Sprintf(profileImageFormat, username), data, profileImageSizes)
}

func ProfileImagesName(image Image, username string) (string, error) {
	return fmt.Sprintf("%s-profile_image-%s-%s.%s", username, image.Uid, image.Size, image.Format), image.validate()
}
