package yimg

import (
	"fmt"
)

const channelOfflineImageFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/%s-channel_offline_image-{{.Uid}}-{{.Size}}.{{.Format}}"

var channelOfflineImageSizes = []string{"1920x1080"}

// ChannelOfflineImages converts database yaml into an Images map for the provided username
func ChannelOfflineImages(data []byte, username string) (Images, error) {
	return parse(fmt.Sprintf(channelOfflineImageFormat, username), data, channelOfflineImageSizes)
}

func ChannelImageName(image Image, username string) (string, error) {
	if image.Size == channelOfflineImageSizes[0] {
		return fmt.Sprintf("%s-channel_offline_image-%s.%s", username, image.Uid, image.Format), image.validate()
	}
	return fmt.Sprintf("%s-channel_offline_image-%s-%s.%s", username, image.Uid, image.Size, image.Format), image.validate()
}
