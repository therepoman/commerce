package spade

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/xeipuuv/gojsonschema"

	"golang.org/x/net/context"
)

// ErrMaxConcurrency is returned when there are outstanding Spade requests equaling
// the maximum concurrency set for a Client.
var ErrMaxConcurrency = errors.New("max concurrency exceeded; ignoring tracking event")

// Client defines an interface to send tracking events to Spade
type Client interface {
	// TrackEvent sends an event tracking HTTP request to Spade.
	// It returns an error if there is a problem generating the request, if the
	// http.Client errors, the HTTP response is not a 204, or it has exceeded the
	// maximum concurrent request setting for the client.
	// properties should be a map[string]interface{} whose values are all strings,
	// numbers, or booleans, or a flat struct with only string, number, and boolean
	// fields.
	TrackEvent(ctx context.Context, event string, properties interface{}) error

	// TrackEvents sends events tracking HTTP request to Spade. If number of events
	// exceeds batchSize configuration, the requests will be split into multiple
	// batches, with each batch sending events up to batchSize.
	// It returns an error if there is a problem generating the request, if the
	// http.Client errors, the HTTP response is not a 204, or it has exceeded the
	// maximum concurrent request setting for the client.
	// The Properties field of each event should be a map[string]interface{} whose
	// values are all strings, numbers, or booleans, or a flat struct with only
	// string, number, and boolean fields.
	TrackEvents(ctx context.Context, events ...Event) error
}

// Event represents a spade event
type Event struct {
	Name       string
	Properties interface{}
}

var (
	defaultURL = &url.URL{
		Scheme: "https",
		Host:   "spade.internal.justin.tv",
		Path:   "/",
	}
	defaultURLValues = url.Values{}
	noopStatHook     = func(_ string, _ int, _ time.Duration) {}
	defaultBatchSize = 32
	singleLoader     = gojsonschema.NewStringLoader(`{
	    "$schema": "http://json-schema.org/draft-04/schema#",
	    "description": "Identify spade-compatible data",
	    "type": "object",
	    "additionalProperties": {"type": ["boolean", "integer", "null", "number", "string"]}
	}`)
	batchLoader = gojsonschema.NewStringLoader(`{
	    "$schema": "http://json-schema.org/draft-04/schema#",
	    "description": "Identify spade-compatible batch data",
	    "type": "array",
	    "items": {
	        "type": "object",
	        "properties": {
		    "Name": {"type": "string"},
		    "Properties": {
		      "type": "object",
		      "additionalProperties": {"type": ["boolean", "integer", "null", "number", "string"]}
		    }
		},
		"additionalProperties": false
	    }
	}`)
)

// NewClient creates a new Spade client according to the configuration specified
// by its InitFuncs. If not specified via InitFunc, the default configuration
// options are used:
//
// BaseURL: "https://spade.internal.justin.tv/"
// MaxConcurrency: unlimited
// BatchSize: 32
//
// For code running outside the Twitch network, the base URL should be
// overridden with "https://spade.twitch.tv/" by using
//   InitBaseURL(url.URL{Scheme: "https", Host: "spade.twitch.tv", Path: "/"})
// as a parameter to NewClient().
func NewClient(opts ...InitFunc) (Client, error) {
	var init clientInit
	for _, opt := range opts {
		if err := opt(&init); err != nil {
			return nil, fmt.Errorf("initializing client: %v", err)
		}
	}

	if init.httpClientFunc != nil && init.httpClient != nil {
		return nil, errors.New("cannot set both http client and http client func")
	}

	c := &clientImpl{}
	if init.hasBaseURL {
		c.baseURL = &init.baseURL
		c.baseURLValues = c.baseURL.Query()
	} else {
		c.baseURL = defaultURL
		c.baseURLValues = defaultURLValues
	}

	c.httpClientFunc = makeHTTPClientFunc(&init)

	c.batchSize = defaultBatchSize
	if init.batchSize > 0 {
		c.batchSize = init.batchSize
	}

	if init.maxConcurrency > 0 {
		c.sem = make(chan struct{}, init.maxConcurrency)
	}

	if !init.noValidation {
		var err error
		c.singleValidator, err = gojsonschema.NewSchema(singleLoader)
		if err != nil {
			return nil, errors.New("failed to create single schema validator")
		}
		c.batchValidator, err = gojsonschema.NewSchema(batchLoader)
		if err != nil {
			return nil, errors.New("failed to create batch schema validator")
		}
	}

	c.statHook = noopStatHook
	if init.statHook != nil {
		c.statHook = init.statHook
	}

	return c, nil
}

func makeHTTPClientFunc(init *clientInit) func(context.Context) *http.Client {
	if init.httpClientFunc != nil {
		return init.httpClientFunc
	}
	if init.httpClient != nil {
		return func(context.Context) *http.Client { return init.httpClient }
	}
	return func(context.Context) *http.Client { return http.DefaultClient }
}

// URLValues returns `url.Values` to be used in a Spade event tracking request.
// This can be used if you want more control over the HTTP request sent to Spade.
func URLValues(event string, properties interface{}) (url.Values, error) {
	jb, err := json.Marshal(payload{
		Event:      event,
		Properties: properties,
	})
	if err != nil {
		return nil, fmt.Errorf("marshaling JSON: %v", err)
	}
	v := url.Values{}
	v.Add("data", base64.URLEncoding.EncodeToString(jb))
	return v, nil
}

type payload struct {
	Event      string      `json:"event"`
	Properties interface{} `json:"properties"`
}

type clientImpl struct {
	batchSize       int
	httpClientFunc  func(context.Context) *http.Client
	baseURL         *url.URL
	baseURLValues   url.Values
	sem             chan struct{}
	statHook        StatHook
	singleValidator *gojsonschema.Schema
	batchValidator  *gojsonschema.Schema
}

var _ Client = (*clientImpl)(nil)

func aggregateValidatorResults(errors []gojsonschema.ResultError) error {
	strErrors := make([]string, len(errors))
	for i, err := range errors {
		strErrors[i] = err.String()
	}
	return fmt.Errorf("invalid event: %s", strings.Join(strErrors, "; "))
}

// TrackEvent sends the event to spade
func (c *clientImpl) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	if c.singleValidator != nil {
		result, err := c.singleValidator.Validate(gojsonschema.NewGoLoader(properties))
		if err != nil {
			return fmt.Errorf("validating JSON schema: %v", err)
		}
		if !result.Valid() {
			return aggregateValidatorResults(result.Errors())
		}
	}
	v, err := URLValues(event, properties)
	if err != nil {
		return fmt.Errorf("marshaling URL values: %v", err)
	}
	if err := c.sendEvents(ctx, v); err != nil {
		return fmt.Errorf("sending events: %v", err)
	}
	return nil
}

// TrackEvents sends the events to spade, at given batchSize a time.
// Fails immediately if any batch fails to be generated/sent
func (c *clientImpl) TrackEvents(ctx context.Context, events ...Event) error {
	if c.batchValidator != nil {
		result, err := c.batchValidator.Validate(gojsonschema.NewGoLoader(events))
		if err != nil {
			return fmt.Errorf("validating JSON schema: %v", err)
		}
		if !result.Valid() {
			return aggregateValidatorResults(result.Errors())
		}
	}
	for start := 0; start < len(events); {
		end := min(start+c.batchSize, len(events))
		v, err := urlValueBatch(events[start:end])
		if err != nil {
			return fmt.Errorf("batching values: %v", err)
		}

		start = end
		if err = c.sendEvents(ctx, v); err != nil {
			return fmt.Errorf("sending batched events: %v", err)
		}
	}

	return nil
}

// An InitFunc initializes Client implementations with configuration options.
type InitFunc func(*clientInit) error

type clientInit struct {
	batchSize      int
	maxConcurrency int

	httpClientFunc func(context.Context) *http.Client
	httpClient     *http.Client

	hasBaseURL bool
	baseURL    url.URL

	noValidation bool

	statHook StatHook
}

// InitHTTPClient returns an InitFunc which sets the http.Client on the spade.Client.
// Passing both InitHTTPClient and InitHTTPClientFunc to NewClient results in an error.
func InitHTTPClient(c *http.Client) InitFunc {
	return func(init *clientInit) error {
		init.httpClient = c
		return nil
	}
}

// InitHTTPClientFunc returns an InitFunc which sets the context-sensitive http.Client on the spade.Client.
// Passing both InitHTTPClient and InitHTTPClientFunc to NewClient results in an error.
func InitHTTPClientFunc(fn func(context.Context) *http.Client) InitFunc {
	return func(init *clientInit) error {
		init.httpClientFunc = fn
		return nil
	}
}

// InitBaseURL defines the URL for Spade HTTP requests.
func InitBaseURL(base url.URL) InitFunc {
	return func(init *clientInit) error {
		if base.Host == "" {
			return fmt.Errorf("missing host in url: %v", base)
		}
		if base.Scheme == "" {
			base.Scheme = "https"
		}
		init.hasBaseURL = true
		init.baseURL = base
		return nil
	}
}

// InitMaxConcurrency defines the maximum number of outstanding HTTP requests
// a Client will allow to Spade. This protects the running process from over-allocating
// system resources (memory, network, CPU time) to Spade event tracking so that
// the running process can do higher-priority work.
func InitMaxConcurrency(max int) InitFunc {
	return func(init *clientInit) error {
		if max <= 0 {
			return fmt.Errorf("max concurrency must be positive; received %d", max)
		}
		init.maxConcurrency = max
		return nil
	}
}

// InitBatchSize defines the maximum number of spade events that can be sent in
// one http request. Each batch must be less than 500kb base64 encoded.
func InitBatchSize(batchSizeInit int) InitFunc {
	return func(init *clientInit) error {
		if batchSizeInit <= 0 {
			return fmt.Errorf("batch size must be at least 1, received %d", batchSizeInit)
		}
		init.batchSize = batchSizeInit
		return nil
	}
}

// InitNoValidation returns an InitFunc which disables validation on the spade.Client.
func InitNoValidation() InitFunc {
	return func(init *clientInit) error {
		init.noValidation = true
		return nil
	}
}

// StatHook is the callback type to track Spade HTTP request stats.
type StatHook func(name string, httpStatusCode int, d time.Duration)

// InitStatHook defines a callback to track Spade HTTP request stats
func InitStatHook(hook StatHook) InitFunc {
	return func(init *clientInit) error {
		init.statHook = hook
		return nil
	}
}

func urlValueBatch(events []Event) (url.Values, error) {
	payloads := make([]payload, len(events))
	for i, v := range events {
		payloads[i] = payload{
			Event:      v.Name,
			Properties: v.Properties,
		}
	}

	jb, err := json.Marshal(payloads)
	if err != nil {
		return nil, err
	}

	v := url.Values{}
	v.Add("data", base64.URLEncoding.EncodeToString(jb))
	return v, nil
}

func (c *clientImpl) sendEvents(ctx context.Context, value url.Values) error {
	if c.sem != nil {
		select {
		case c.sem <- struct{}{}:
			defer func() {
				<-c.sem
			}()
		default:
			return ErrMaxConcurrency
		}
	}

	req, err := http.NewRequest("POST", c.baseURL.String(), strings.NewReader(value.Encode()))
	if err != nil {
		return fmt.Errorf("constructing request: %v", err)
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	// without user agent, we could just call http.Client.PostForm()
	req.Header.Add("User-Agent", "code.justin.tv/common/spade-client/go")
	reqStart := time.Now()
	response, err := c.httpClientFunc(ctx).Do(req)
	duration := time.Since(reqStart)
	if err != nil {
		c.statHook("track_event", 0, duration)
		return fmt.Errorf("HTTP request failed: %v", err)
	}

	defer func() {
		if cerr := response.Body.Close(); cerr != nil {
			log.Printf("error closing response body: %v", cerr)
		}
	}()

	c.statHook("track_event", response.StatusCode, duration)
	if response.StatusCode != http.StatusNoContent {
		return fmt.Errorf("unexpected response status: %s", response.Status)
	}
	return nil
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}
