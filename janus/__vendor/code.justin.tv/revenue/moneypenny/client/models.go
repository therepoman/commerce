package moneypenny

import (
	"time"
)

//GetUserPayoutTypeResponse is a response for GetUserPayoutType
type GetUserPayoutTypeResponse struct {
	ChannelID         string   `json:"channel_id"`
	IsAffiliate       bool     `json:"is_affiliate"`
	IsDeveloper       bool     `json:"is_developer"`
	IsPartner         bool     `json:"is_partner"`
	Category          string   `json:"category"`
	DeveloperCategory string   `json:"developer_category"`
	Tags              []string `json:"tags"`
}

//OnboardingStatusMessage TODO
type OnboardingStatusMessage struct {
	ChannelID         string      `json:"channel_id"`
	Timestamp         time.Time   `json:"timestamp"`
	Action            string      `json:"action"`
	ActionCategory    string      `json:"action_category"`
	InvitationID      string      `json:"invite_id"`
	Category          string      `json:"category"`
	DeveloperCategory string      `json:"developer_category"`
	Tags              []string    `json:"tags"`
	Features          interface{} `json:"features"`
}

//TaxInterviewInfo TODO
type TaxInterviewInfo struct {
	IsUserActionRequired bool
	FieldsMatchDetail    TaxInterviewInfoMatchDetail `json:"fields_match_detail"`
}

//TaxInterviewInfoMatchDetail TODO
type TaxInterviewInfoMatchDetail struct {
	Name           *bool `json:",omitempty"`
	Country        *bool `json:",omitempty"`
	Email          *bool `json:",omitempty"`
	StreetAddress  *bool `json:",omitempty"`
	StreetAddress2 *bool `json:",omitempty"`
	City           *bool `json:",omitempty"`
	State          *bool `json:",omitempty"`
	Postal         *bool `json:",omitempty"`
}

//GetWorkflowStateResponse TODO
type GetWorkflowStateResponse struct {
	ChannelID        string           `json:"channel_id"`
	PayoutEntityID   string           `json:"payout_entity_id"`
	InvitationID     string           `json:"invitation_id"`
	WorkflowID       string           `json:"workflow_id"`
	State            string           `json:"state"`
	StateInfo        interface{}      `json:"state_info"`
	TaxInterviewInfo TaxInterviewInfo `json:"tax_interview_info"`
}

//GetEULAResponse TODO
type GetEULAResponse struct {
	ChannelID   string `json:"channel_id"`
	EULAType    string `json:"eula_type"`
	EULAVersion string `json:"eula_version"`
	Body        string `json:"body"`
}

//GetPayoutEntityForChannelResponse TODO
type GetPayoutEntityForChannelResponse struct {
	PayoutEntityID     string         `json:"payout_entity_id"`
	OwnerChannelID     string         `json:"owner_channel_id"`
	TaxWithholdingRate TaxWithholding `json:"tax_withholding_rate"`
}

// PayoutAttribution TODO
type PayoutAttribution struct {
	UserID    string    `json:"user_id"`
	StartDate time.Time `json:"start_date"`
}

// GetPayoutAttributionsResponse TODO
type GetPayoutAttributionsResponse struct {
	PayoutEntityID     string              `json:"payout_entity_id"`
	PayoutAttributions []PayoutAttribution `json:"payout_attributions"`
}

// CreatePayoutAttributionRequest TODO
type CreatePayoutAttributionRequest struct {
	StartDate time.Time `json:"start_date"`
}

// PayoutEntityStruct TODO
type PayoutEntityStruct struct {
	PayoutEntityID     string         `json:"payout_entity_id"`
	OwnerChannelID     string         `json:"owner_channel_id"`
	TaxWithholdingRate TaxWithholding `json:"tax_withholding_rate"`
}

// GetPayoutEntitiesOwnedByChannelResponse TODO
type GetPayoutEntitiesOwnedByChannelResponse struct {
	PayoutEntities []PayoutEntityStruct `json:"payout_entities"`
}

// TaxWithholding TODO
type TaxWithholding struct {
	RoyaltyTaxWithholdingRate *float64 `json:"royalty_tax_withholding_rate,omitempty"`
	ServiceTaxWithholdingRate *float64 `json:"service_tax_withholding_rate,omitempty"`
}

// SetOnboardingEULARequest TODO
type SetOnboardingEULARequest struct {
	EulaType    string `json:"eula_type"`
	EulaVersion string `json:"eula_version"`
}

// GetTIMSParamsRequest TODO
type GetTIMSParamsRequest struct {
	Lang       string `json:"lang"`
	IncomeType string `json:"income_type"`
	ReturnURL  string `json:"return_url"`
}

// GetTIMSParamsResponse TODO
type GetTIMSParamsResponse struct {
	AccountID             string `json:"account_id"`
	ChannelID             string `json:"channel_id"`
	PayoutEntityID        string `json:"payout_entity_id"`
	WorkflowID            string `json:"workflow_id"`
	URL                   string `json:"url"`
	IncomeType            string `json:"income_type"`
	Locale                string `json:"locale"`
	LegalName             string `json:"legal_name"`
	AddressID             string `json:"address_id"`
	ClientID              string `json:"client_id"`
	LoggedInCustomerEmail string `json:"logged_in_customer_email"`
	LoggedInCustomerID    string `json:"logged_in_customer_id"`
	ReturnURL             string `json:"return_url"`
	LogoImageURL          string `json:"logo_image_url"`
	SiteID                string `json:"site_id"`
	Signature             string `json:"signature"`
}

// GetAccountPayableRegistrationURLResponse TODO
type GetAccountPayableRegistrationURLResponse struct {
	URL string `json:"url"`
}

// GetTipaltiPayoutHistoryURLResponse TODO
type GetTipaltiPayoutHistoryURLResponse struct {
	URL string `json:"url"`
}

// GetPayoutUserAttributesResponse TODO
type GetPayoutUserAttributesResponse struct {
	WorkflowID     string `json:"workflow_id"`
	FirstName      string `json:"first_name"`
	MiddleName     string `json:"middle_name"`
	LastName       string `json:"last_name"`
	CompanyName    string `json:"company_name"`
	Email          string `json:"email"`
	StreetAddress  string `json:"street_address"`
	StreetAddress2 string `json:"street_address2"`
	City           string `json:"city"`
	State          string `json:"state"`
	Postal         string `json:"postal"`
	Country        string `json:"country"`
	//DEPRECATED
	Birthdate   string    `json:"birthdate"`
	DateOfBirth time.Time `json:"dob"`
	ParentName  string    `json:"parent_name"`
	ParentEmail string    `json:"parent_email"`
}

// SetPayoutUserAttributesRequest TODO
type SetPayoutUserAttributesRequest struct {
	FirstName      string `json:"first_name"`
	MiddleName     string `json:"middle_name"`
	LastName       string `json:"last_name"`
	CompanyName    string `json:"company_name"`
	Email          string `json:"email"`
	StreetAddress  string `json:"street_address"`
	StreetAddress2 string `json:"street_address2"`
	City           string `json:"city"`
	State          string `json:"state"`
	Postal         string `json:"postal"`
	Country        string `json:"country"`
	//DEPRECATED
	Birthdate   string    `json:"birthdate"`
	DateOfBirth time.Time `json:"dob"`
	ParentName  string    `json:"parent_name"`
	ParentEmail string    `json:"parent_email"`
}

// WorkflowEvent TODO
type WorkflowEvent struct {
	WorkflowID    string    `json:"workflow_id"`
	InvitationID  string    `json:"invitation_id"`
	State         string    `json:"state"`
	PreviousState string    `json:"previous_state"`
	Transition    string    `json:"transition"`
	Timestamp     time.Time `json:"timestamp"`
	Data          string    `json:"data"`
}

// GetWorkflowEventsResponse TODO
type GetWorkflowEventsResponse struct {
	ChannelID      string          `json:"channel_id,omitempty"`
	PayoutEntityID string          `json:"payout_entity_id,omitempty"`
	WorkflowID     string          `json:"workflow_id,omitempty"`
	Events         []WorkflowEvent `json:"workflow_events"`
}

// BulkCreateInvitationRequest TODO
type BulkCreateInvitationRequest struct {
	ChannelIDs []string                `json:"channel_ids"`
	Category   CreateInvitationRequest `json:"category"`
}

// BulkCreateInvitationResponse TODO
type BulkCreateInvitationResponse []CreateInvitationResponse

// CreateInvitationRequest TODO
type CreateInvitationRequest struct {
	Category string      `json:"category"`
	Tags     []string    `json:"tags"`
	Features interface{} `json:"features"`
}

// CreateInvitationResponse TODO
type CreateInvitationResponse struct {
	ChannelID    string `json:"channel_id"`
	InvitationID string `json:"invitation_id"`
	Success      bool   `json:"success"`
	ErrorMessage string `json:"error_message,omitempty"`
}

//GetInvitationResponse TODO
type GetInvitationResponse struct {
	InvitationID string   `json:"invitation_id"`
	ChannelID    string   `json:"channel_id"`
	Category     string   `json:"category"`
	Tags         []string `json:"tags"`
}

//GetInvitationWorkflowsResponse TODO
type GetInvitationWorkflowsResponse struct {
	InvitationID string   `json:"invitation_id"`
	WorkflowIDs  []string `json:"workflow_id"`
}

//OffboardUserRequest TODO
type OffboardUserRequest struct {
	Category string `json:"category"`
}

// InviteDeveloperResponse TODO
type InviteDeveloperResponse struct {
	ChannelID    string `json:"channel_id"`
	InvitationID string `json:"invitation_id"`
	Success      bool   `json:"success"`
	ErrorMessage string `json:"error_message,omitempty"`
}

// GetDeveloperStateResponse TODO
type GetDeveloperStateResponse struct {
	ChannelID    string `json:"channel_id"`
	CurrentState string `json:"current_state"`
	WorkflowType string `json:"workflow_type,omitempty"`
}

// PendingActivationResponse TODO
type PendingActivationResponse []ChannelPendingActivation

// ChannelPendingActivation TODO
type ChannelPendingActivation struct {
	ChannelID      string `json:"channel_id"`
	PayoutEntityID string `json:"payout_entity_id"`
}

// ActivatePendingChannelRequest TODO
type ActivatePendingChannelRequest struct {
	ContractStartDate       time.Time   `json:"contract_start_date"`
	ContractEndDate         time.Time   `json:"contract_end_date"`
	ContractAutoRenewNotice string      `json:"contract_auto_renew_notice"`
	ContractRenewalTerm     string      `json:"contract_renewal_term"`
	ContractURL             string      `json:"contract_url"`
	ContractAutoRenew       bool        `json:"contract_auto_renew"`
	Features                interface{} `json:"features"`
}

// GetContractDetailsResponse TODO
type GetContractDetailsResponse struct {
	ContractStartDate       time.Time `json:"contract_start_date"`
	ContractEndDate         time.Time `json:"contract_end_date"`
	ContractAutoRenewNotice string    `json:"contract_auto_renew_notice"`
	ContractRenewalTerm     string    `json:"contract_renewal_term"`
	ContractURL             string    `json:"contract_url"`
	ContractAutoRenew       bool      `json:"contract_auto_renew"`
}

// UpdateContractDetailsRequest is the request with values needed to update contract details
type UpdateContractDetailsRequest struct {
	ContractStartDate       time.Time `json:"contract_start_date"`
	ContractEndDate         time.Time `json:"contract_end_date"`
	ContractAutoRenewNotice string    `json:"contract_auto_renew_notice"`
	ContractRenewalTerm     string    `json:"contract_renewal_term"`
	ContractURL             string    `json:"contract_url"`
	ContractAutoRenew       bool      `json:"contract_auto_renew"`
}

// UpdateContractDetailsResponse is the response returned when updating contract details
type UpdateContractDetailsResponse struct {
	InvitationID string `json:"invitation_id"`
	Success      bool   `json:"success"`
}
