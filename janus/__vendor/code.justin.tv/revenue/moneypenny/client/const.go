package moneypenny

const (
	//OnboardStateStart is a initial state of payout workflow
	OnboardStateStart = "PE_START"

	//OnboardStateCoversheet is a state when user input their personal information
	OnboardStateCoversheet = "PE_COVERSHEET"

	//OnboardStateEula is a state when user can agree or disagree to EULA
	OnboardStateEula = "PE_EULA"

	//OnboardStateTIMSRoyalty is a state when user can go do royalty TIMS
	OnboardStateTIMSRoyalty = "PE_TIMS_ROYALTY"

	//OnboardStateTIMSService is a state when user can go do service TIMS
	OnboardStateTIMSService = "PE_TIMS_SERVICE"

	//OnboardStateTIMSPendingRoyalty is a state when user waits for status update from royalty TIMS
	OnboardStateTIMSPendingRoyalty = "PE_PENDING_TIMS_ROYALTY"

	//OnboardStateTIMSPendingService is a state when user waits for status update from service TIMS
	OnboardStateTIMSPendingService = "PE_PENDING_TIMS_SERVICE"

	//OnboardStateTIMSMismatchRoyalty is a state when user information in royalty TIMS and our system doesn't match
	OnboardStateTIMSMismatchRoyalty = "PE_MISMATCH_TIMS_ROYALTY"

	//OnboardStateTIMSMismatchService is a state when user information in service TIMS and our system doesn't match
	OnboardStateTIMSMismatchService = "PE_MISMATCH_TIMS_SERVICE"

	//OnboardStateTIMSSuccessRoyalty is a state when user completes royalty TIMS
	OnboardStateTIMSSuccessRoyalty = "PE_SUCCESS_TIMS_ROYALTY"

	//OnboardStateTIMSSuccessService is a state when user completes service TIMS
	OnboardStateTIMSSuccessService = "PE_SUCCESS_TIMS_SERVICE"

	//OnboardStateTipalti is a state when user can do payout information in Tipalti iframe
	OnboardStateTipalti = "PE_PAYOUT_INFO"

	//OnboardStateComplete is a final state in workflow where user completes onboarding flow
	OnboardStateComplete = "PE_COMPLETE"

	//OnboardStateCancel is a final state when workflow is cancelled by user
	OnboardStateCancel = "PE_CANCEL"

	//OnboardStateTIMSFailRoyalty is a state when user fails royalty TIMS
	OnboardStateTIMSFailRoyalty = "PE_FAIL_TIMS_ROYALTY"

	//OnboardStateTIMSFailService is a state when user fails service TIMS
	OnboardStateTIMSFailService = "PE_FAIL_TIMS_SERVICE"

	//OnboardStateTipaltiPending is a state when user waits for OFAC verification
	OnboardStateTipaltiPending = "PE_PENDING_PAYOUT_INFO"

	//OnboardStatePendingApproval is a state when custom partner waits for approval from partnership team
	OnboardStatePendingApproval = "PE_PENDING_APPROVAL"

	//OnboardStateTermUpgrade is a state where user has a new EULA term and can agree or disagree to the new term
	OnboardStateTermUpgrade = "PE_TERM_UPGRADE"
)
