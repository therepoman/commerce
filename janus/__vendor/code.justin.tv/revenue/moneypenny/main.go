package main

import (
	"time"

	"os"

	"fmt"

	"code.justin.tv/chat/pushy/client/events"
	"code.justin.tv/common/config"
	"code.justin.tv/common/hystrixstatsd"
	"code.justin.tv/foundation/twitchserver"
	conf "code.justin.tv/revenue/go-libs/config"
	"code.justin.tv/revenue/moneypenny/app/api"
	"code.justin.tv/revenue/moneypenny/app/auth"
	"code.justin.tv/revenue/moneypenny/app/clients/datastore"
	"code.justin.tv/revenue/moneypenny/app/clients/guardian"
	"code.justin.tv/revenue/moneypenny/app/clients/payday"
	"code.justin.tv/revenue/moneypenny/app/clients/pushy"
	"code.justin.tv/revenue/moneypenny/app/clients/ripley"
	"code.justin.tv/revenue/moneypenny/app/clients/s2s"
	"code.justin.tv/revenue/moneypenny/app/clients/tipalti"
	cfg "code.justin.tv/revenue/moneypenny/app/config"
	"code.justin.tv/revenue/moneypenny/app/workers"
	"github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/heroku/rollrus"
	log "github.com/sirupsen/logrus"
)

const (
	hystrixSampleRate = 1
)

func init() {
	log.SetLevel(log.DebugLevel)
	log.Info("Starting moneypenny.....")
}

func main() {
	err := config.Parse()
	if err != nil {
		log.WithFields(log.Fields{
			"event":  "fatal_error_parsing_config",
			"detail": err.Error(),
		}).WithError(err).Fatalf("Fatal error parsing config")
	}
	path, _ := os.Getwd()
	fileName := fmt.Sprintf("%s/config", path)
	log.Info("Config file: ", fileName)
	confObj := new(cfg.AppConfig)
	err = conf.LoadJSONConfig(fileName, confObj)
	if err != nil {
		log.WithFields(log.Fields{
			"event":  "fatal_error_parsing_config",
			"detail": err.Error(),
		}).WithError(err).Fatalf("Fatal error parsing config")
	}

	log.WithFields(log.Fields{
		"event":  "config_object",
		"detail": fmt.Sprintf("%+v", confObj),
	}).Info()

	//Setup logger file
	file, warnErr := os.OpenFile(confObj.LogOutput, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if warnErr != nil {
		log.WithFields(log.Fields{
			"event":  "error_setting_log_output",
			"detail": warnErr.Error(),
		}).Warn()
	} else {
		defer func() {
			if err := file.Close(); err != nil {
				panic(err)
			}
		}()
		log.SetOutput(file)
		// log level is panic, fatal, error, warn, info, debug
		logLevel, err := log.ParseLevel(confObj.LogLevel)
		if err != nil {
			log.WithFields(log.Fields{
				"event":  "error_setting_log_level",
				"detail": err.Error(),
			}).Warn()
			logLevel = log.InfoLevel
		}
		log.SetLevel(logLevel)
		log.SetFormatter(&log.TextFormatter{})
	}

	//Hook log with rollbar
	if confObj.RollbarToken != "" {
		log.AddHook(rollrus.NewHook(confObj.RollbarToken, config.Environment()))
	}

	authHandler, err := auth.NewHandler(confObj.CartmanCert)
	if err != nil {
		log.WithFields(log.Fields{
			"event":  "fatal_error_creating_auth_handler",
			"detail": err.Error(),
		}).WithError(err).Fatalf("Fatal error creating auth handler")
	}

	awsSession, err := api.NewSession(confObj.AwsRegion)
	if err != nil {
		log.WithFields(log.Fields{
			"event":  "fatal_error_creating_aws_session",
			"detail": err.Error(),
		}).WithError(err).Fatalf("Fatal error creating aws session")
	}

	dao := datastore.NewDynamoDAO(awsSession, confObj.KmsCustomerMasterKeyID)
	paydayClient, err := payday.NewPaydayClient(awsSession,
		confObj.PaydayEndpoint,
		confObj.AddressSqsRegion,
		confObj.AddressInputArn,
		confObj.AddressCustomerMasterKeyID,
		dao.UserAttributeDao)
	if err != nil {
		log.WithFields(log.Fields{
			"event":  "fatal_error_creating_payday_client",
			"detail": err.Error(),
		}).WithError(err).Fatalf("Fatal error creating payday client")
	}
	tipaltiClient, err := tipalti.New(confObj.TipaltiCert, confObj.TipaltiPayeeSoapEndpoint)
	if err != nil {
		log.WithFields(log.Fields{
			"event":  "fatal_error_creating_tipalti_client",
			"detail": err.Error(),
		}).WithError(err).Fatalf("Fatal error creating tipalti client")
	}
	callbacks := api.NewPayoutCallbacks(dao, tipaltiClient, paydayClient)
	statsdClients := config.Statsd()
	metricCollector.Registry.Register((&hystrixstatsd.StatsdCollectorFactory{
		Client:     statsdClients.NewSubStatter("hystrix"),
		SampleRate: hystrixSampleRate,
	}).Create)

	var pushyClient events.Publisher
	if confObj.PushySnsArn == "" {
		pushyClient = pushy.NewNoopPublisher()
	} else {
		pushyClient, err = events.NewPublisher(events.Config{
			DispatchSNSARN: confObj.PushySnsArn,
			Stats:          statsdClients,
			SNSClient:      sns.New(awsSession),
		})
		if err != nil {
			log.WithFields(log.Fields{
				"event":  "fatal_error_creating_pushy_client",
				"detail": err.Error(),
			}).WithError(err).Fatalf("Fatal error creating pushy client")
		}
	}

	ripleyClient := ripley.New(confObj.RipleyEndpoint)

	guardianClient := guardian.NewGuardianClient(confObj.GuardianEndpoint)

	s2sClient := s2s.NewS2SClient(confObj)
	err = s2sClient.Start()
	if err != nil {
		log.WithFields(log.Fields{
			"event": "fatal_error_starting_s2s_callee_client",
		}).WithError(err).Fatalf("Fatal error starting s2s callee client")
	}

	serverContext := &api.ServerContext{
		Config:         *confObj,
		Stats:          statsdClients,
		Dao:            dao,
		Callbacks:      callbacks,
		AuthHandler:    authHandler,
		PaydayClient:   paydayClient,
		TipaltiClient:  tipaltiClient,
		PushyClient:    pushyClient,
		GuardianClient: guardianClient,
		RipleyClient:   ripleyClient,
		S2SClient:      s2sClient,
	}
	//server
	server := api.NewServer(serverContext)

	//twirp TODO add later when we have implement authorization
	//hook := statsd.NewStatsdServerHooks(config.Statsd())
	//twirpServer := api.NewTwirpServer(serverContext)
	//twirpHandler := moneytwirp.NewMoneypennyServer(twirpServer, hook)
	//server.Handle(pat.Post(moneytwirp.MoneypennyPathPrefix+"*"), twirpHandler)

	timsSQSContext := workers.TIMSUpdateContext{
		PayoutEntityDao: dao.PayoutEntityDao,
		WorkflowDao:     dao.WorkflowDao,
		PayoutCallbacks: callbacks,
	}
	go workers.InitSQSWorkers(workers.CreateTIMSSQSProcessor(timsSQSContext), workers.SQSErrorHandling, statsdClients, &workers.SQSWorkerConfig{
		Region:        confObj.AwsRegion,
		QueueName:     confObj.TIMSSqsName,
		NumWorkers:    confObj.TIMSSqsNumWorker,
		MaxVisibility: time.Minute,
	})

	tipaltiSQSContext := workers.TipaltiIPNContext{
		PayoutEntityDao: dao.PayoutEntityDao,
		WorkflowDao:     dao.WorkflowDao,
		InvitationDao:   dao.AllOnboardingApplicationsDao,
		TipaltiClient:   tipaltiClient,
		Callbacks:       callbacks,
	}
	go workers.InitSQSWorkers(workers.CreateTipaltiIPNProcessor(tipaltiSQSContext), workers.SQSErrorHandling, statsdClients, &workers.SQSWorkerConfig{
		Region:        confObj.AwsRegion,
		QueueName:     confObj.TipaltiIPNSQSName,
		NumWorkers:    confObj.TipaltiIPNSQSNumWorker,
		MaxVisibility: time.Minute,
	})

	addressSQSContext := workers.AddressVerificationContext{
		PayoutEvent: dao.WorkflowDao,
		Callbacks:   callbacks,
	}
	go workers.InitSQSWorkers(workers.CreateAddressVerificationResultProcessor(addressSQSContext), workers.SQSErrorHandling, statsdClients, &workers.SQSWorkerConfig{
		Region:        confObj.AwsRegion,
		QueueName:     confObj.AddressVerificationResultSqsName,
		NumWorkers:    confObj.AddressVerificationResultNumWorker,
		MaxVisibility: time.Minute,
	})

	onboardingSQSContext := workers.OnboardingProcessorContext{
		PayoutEntityDao:         dao.PayoutEntityDao,
		OnboardingInvitationDao: dao.AllOnboardingApplicationsDao,
		WorkflowDao:             dao.WorkflowDao,
		TipaltiClient:           tipaltiClient,
		BadgesEndpoint:          confObj.BadgesEndpoint,
	}
	go workers.InitSQSWorkers(workers.CreateOnboardingProcessor(onboardingSQSContext), workers.SQSErrorHandling, statsdClients, &workers.SQSWorkerConfig{
		Region:        confObj.AwsRegion,
		QueueName:     confObj.OnboardingSQSName,
		NumWorkers:    confObj.OnboardingSQSNumWorker,
		MaxVisibility: time.Minute,
	})

	//Single DLQ for all SQS
	dlqContext := workers.PayoutDLQContext{
		TIMS:       timsSQSContext,
		Address:    addressSQSContext,
		Tipalti:    tipaltiSQSContext,
		Onboarding: onboardingSQSContext,
		Dao:        dao.DlqDao,
	}
	go workers.InitSQSWorkers(workers.CreateDynamoDLQProcessor(dlqContext), workers.SQSErrorHandling, statsdClients, &workers.SQSWorkerConfig{
		Region:        confObj.AwsRegion,
		QueueName:     confObj.DlqSqsName,
		NumWorkers:    confObj.DlqSqsNumWorker,
		MaxVisibility: time.Minute,
	})

	twitchserver.AddDefaultSignalHandlers()
	err = twitchserver.ListenAndServe(server, twitchserver.NewConfig())
	if err != nil {
		log.Fatal(err)
	}
}
