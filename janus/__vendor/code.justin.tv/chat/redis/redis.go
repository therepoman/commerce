package redis

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	redigo "github.com/garyburd/redigo/redis"
	"golang.org/x/net/context"

	"code.justin.tv/chat/timing"
)

var (
	RedisNilVal = redigo.ErrNil
)

const (
	xactRedisGroup = "redis"
	statsInterval  = 5 * time.Second

	rateIncrScript = `
local current
current = tonumber(redis.call("incr", KEYS[1]))
if current == 1 then
  redis.call("expire", KEYS[1], ARGV[1])
end
return current`
)

type Init struct {
	Address   string
	Password  string
	KeyPrefix string

	ConnectTimeout time.Duration
	ReadTimeout    time.Duration
	WriteTimeout   time.Duration

	MaxConns int

	XactGroup   string
	StatsPrefix string
	Stats       statsd.Statter
}

func (init Init) New() (Redis, error) {
	pool := &redigo.Pool{
		MaxIdle:   init.MaxConns,
		MaxActive: init.MaxConns,
		Dial: func() (redigo.Conn, error) {
			redisConn, err := redigo.DialTimeout(
				"tcp",
				init.Address,
				init.ConnectTimeout,
				init.ReadTimeout,
				init.WriteTimeout,
			)
			if err != nil {
				log.Printf("chat/redis: Error creating redis connection: %q", err)
				return nil, err
			}

			if init.Password != "" {
				if _, err := redisConn.Do("AUTH", init.Password); err != nil {
					if closeErr := redisConn.Close(); closeErr != nil {
						return nil, closeErr
					}
					return nil, err
				}
			}
			return redisConn, nil
		},
	}
	r := &redis{
		keyPrefix: init.KeyPrefix,
		redis:     pool,

		active:    0,
		maxActive: 0,
		doneCh:    make(chan struct{}, 0),

		xactGroup:   init.XactGroup,
		statsPrefix: init.StatsPrefix,
		stats:       init.Stats,
	}
	if !strings.HasSuffix(r.keyPrefix, ":") {
		r.keyPrefix += ":"
	}
	if r.xactGroup == "" {
		r.xactGroup = xactRedisGroup
	}
	if !strings.HasSuffix(r.statsPrefix, ".") {
		r.statsPrefix += "."
	}

	go r.manage()

	return r, nil
}

type Redis interface {
	Key(string) string

	Keys(ctx context.Context, pattern string) ([]string, error)
	Incr(ctx context.Context, key string, ttl time.Duration) (int64, error)
	RateIncr(ctx context.Context, key string, duration time.Duration) (current int64, err error)
	MRateIncr(ctx context.Context, keys []string, duration time.Duration) ([]int64, error)
	SafeCache(ctx context.Context, key, value string, ttl time.Duration) error
	SafeSetWithTTL(ctx context.Context, key, value string, ttl time.Duration) (bool, error)
	MSafeSetWithTTL(ctx context.Context, keys []string, values []string, ttl time.Duration) ([]bool, error)
	SetWithTTL(ctx context.Context, key, value string, ttl time.Duration) error
	MSetWithTTL(ctx context.Context, keys []string, values []string, ttl time.Duration) error
	Get(ctx context.Context, key string) (value string, ok bool, err error)
	MGet(ctx context.Context, keys ...string) (values []string, err error)
	Hmget(ctx context.Context, hash string, keys ...string) (values []string, err error)
	Del(ctx context.Context, keys ...string) (deleted int, err error)
	Ttl(ctx context.Context, key string) (ttl int, err error)
	Expire(ctx context.Context, key string, ttl time.Duration) error

	LPush(ctx context.Context, key string, value string) (count uint64, err error)
	LTrim(ctx context.Context, key string, start, end int64) error
	LRange(ctx context.Context, key string, start, end int64) ([]string, error)
	LRem(ctx context.Context, key string, count int64, item string) (removed int, err error)
	Sadd(ctx context.Context, key string, item string) (count int, err error)
	Srem(ctx context.Context, key string, item string) (count int, err error)
	Scard(ctx context.Context, key string) (count int, err error)
	Smembers(ctx context.Context, key string) ([]string, error)
	Zadd(ctx context.Context, key string, score int64, item string) (count int, err error)
	Zcard(ctx context.Context, key string) (count int, err error)
	Zrem(ctx context.Context, key string, item string) (count int, err error)
	Zscore(ctx context.Context, key string, item string) (score int64, err error)
	Zrangebyscore(ctx context.Context, key string, min int64, max int64) ([]string, error)
	Zremrangebyscore(ctx context.Context, key string, fromScore int, toScore int) (count int, err error)
	Eval(ctx context.Context, script string, keys []interface{}, args []interface{}, stat string) (reply interface{}, err error)

	Close() error
}

// TODO: Track the number of active commands are being run
type redis struct {
	keyPrefix string
	redis     *redigo.Pool

	mu        sync.Mutex
	active    int32
	maxActive int32
	doneCh    chan struct{}

	xactGroup   string
	statsPrefix string
	stats       statsd.Statter
}

type pipeline struct {
	conn  redigo.Conn
	redis *redis
}

func (p *pipeline) Close() error {
	return p.redis.closeConn(p.conn)
}

func (p *pipeline) Multi() error {
	return p.conn.Send("MULTI")
}

func (p *pipeline) SetWithTTL(ctx context.Context, key, value string, ttl time.Duration) error {
	return p.conn.Send("SET",
		redigo.Args{}.Add(p.redis.Key(key)).
			Add(value).
			Add("EX").
			Add(redisDuration(ttl))...)
}

func (p *pipeline) SafeSetWithTTL(ctx context.Context, key, value string, ttl time.Duration) error {
	return p.conn.Send("SET",
		redigo.Args{}.Add(p.redis.Key(key)).
			Add(value).
			Add("EX").
			Add(redisDuration(ttl)).
			Add("NX")...)
}

func (p *pipeline) Eval(ctx context.Context, script string, keys []interface{}, args []interface{}) error {
	prefixedKeys := p.redis.prefixKeys(keys)
	return p.conn.Send("EVAL",
		redigo.Args{}.Add(script).
			Add(len(prefixedKeys)).
			AddFlat(prefixedKeys).
			AddFlat(args)...,
	)
}

func (p *pipeline) ExecTransaction(ctx context.Context, stat string) (reply interface{}, err error) {
	return p.redis.doWithConn(ctx, p.conn, stat, "EXEC")
}

func (p *pipeline) Do(ctx context.Context, stat string) (reply interface{}, err error) {
	return p.redis.doWithConn(ctx, p.conn, stat, "")
}

func (r *redis) NewPipelineTransaction() (*pipeline, error) {
	p := r.NewPipeline()
	err := p.Multi()
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (r *redis) NewPipeline() *pipeline {
	conn := r.getConn()
	p := &pipeline{conn: conn, redis: r}

	return p
}

func (r *redis) manage() {
	ticker := time.Tick(statsInterval)
	for {
		select {
		case <-ticker:
			r.mu.Lock()
			max := r.maxActive
			r.maxActive = atomic.LoadInt32(&r.active)
			r.mu.Unlock()
			r.stats.Gauge(r.stat("state.open_conns_cap"), int64(r.redis.MaxActive), 1)
			r.stats.Gauge(r.stat("state.max_active_commands"), int64(max), 1)
			r.stats.Gauge(r.stat("state.pool_active_conns"), int64(r.redis.ActiveCount()), 1)
		case <-r.doneCh:
			return
		}
	}
}

// Not safe to call more than once
func (r *redis) Close() error {
	close(r.doneCh)
	return r.redis.Close()
}

func (r *redis) Key(k string) string {
	return r.keyPrefix + k
}

func (r *redis) Keys(ctx context.Context, pattern string) ([]string, error) {
	return redigo.Strings(r.do(
		ctx,
		r.stat("commands.keys"),
		"KEYS",
		r.Key(pattern),
	))
}

func (r *redis) Incr(ctx context.Context, key string, ttl time.Duration) (int64, error) {
	script := `
local current
current = tonumber(redis.call("incr", KEYS[1]))
redis.call("expire", KEYS[1], ARGV[1])
return current`
	return redigo.Int64(r.eval(
		ctx,
		script,
		stringsToInterfaces([]string{key}),
		stringsToInterfaces([]string{redisDuration(ttl)}),
		r.stat("commands.incr"),
	))
}

func (r *redis) RateIncr(ctx context.Context, key string, duration time.Duration) (current int64, err error) {
	return redigo.Int64(r.eval(
		ctx,
		rateIncrScript,
		stringsToInterfaces([]string{key}),
		stringsToInterfaces([]string{redisDuration(duration)}),
		r.stat("commands.rate_incr"),
	))
}

func (r *redis) MRateIncr(ctx context.Context, keys []string, duration time.Duration) (ret []int64, err error) {
	p := r.NewPipeline()
	defer func() {
		if err2 := p.Close(); err == nil && err2 != nil {
			err = fmt.Errorf("error closing body: %s", err2)
		}
	}()

	ttl := redisDuration(duration)
	for _, key := range keys {
		err = p.Eval(ctx, rateIncrScript, []interface{}{key}, []interface{}{ttl})
		if err != nil {
			return ret, err
		}
	}

	reply, err := redigo.Values(p.Do(ctx, r.stat("commands.mrate_incr")))
	if err != nil {
		return ret, err
	}

	ret = make([]int64, len(reply))
	for idx, value := range reply {
		ret[idx], err = redigo.Int64(value, nil)
		if err != nil {
			return ret, err
		}
	}
	return ret, err
}

// DEPRECATED: remove when all usages of SafeCache are removed.
func (r *redis) SafeCache(ctx context.Context, key, value string, ttl time.Duration) (err error) {
	_, err = r.SafeSetWithTTL(ctx, key, value, ttl)
	return err
}

// MSafeSetWithTTL leverages redis pipelining to issue many SET commands with NX and TTL specified.
// Pipelining is necessary since MSET alone does not support NX and TTL arguments.
// `true` in the return slice means the respective key was set
func (r *redis) MSafeSetWithTTL(ctx context.Context, keys []string, values []string, ttl time.Duration) (ret []bool, err error) {
	if len(keys) != len(values) {
		return ret, errors.New("len(keys) must equal len(values)")
	}

	p := r.NewPipeline()
	defer func() {
		if err2 := p.Close(); err == nil && err2 != nil {
			err = fmt.Errorf("error closing body: %s", err2)
		}
	}()

	for idx, key := range keys {
		err = p.SafeSetWithTTL(ctx, key, values[idx], ttl)
		if err != nil {
			return nil, err
		}
	}

	reply, err := redigo.Values(p.Do(ctx, r.stat("commands.msafe_set_with_ttl")))
	if err != nil {
		return ret, err
	}

	ret = make([]bool, len(reply))
	for idx, value := range reply {
		exists := value == nil
		ret[idx] = !exists
	}
	return ret, err
}

func (r *redis) SafeSetWithTTL(ctx context.Context, key, value string, ttl time.Duration) (ok bool, err error) {
	_, err = redigo.String(r.do(
		ctx,
		r.stat("commands.safe_set_with_ttl"),
		"SET",
		redigo.Args{}.Add(r.Key(key)).
			Add(value).
			Add("EX").
			Add(redisDuration(ttl)).
			Add("NX")...,
	))
	if err == redigo.ErrNil {
		// The key already existed
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

func (r *redis) SetWithTTL(ctx context.Context, key, value string, ttl time.Duration) (err error) {
	_, err = r.do(
		ctx,
		r.stat("commands.set_with_ttl"),
		"SET",
		redigo.Args{}.Add(r.Key(key)).
			Add(value).
			Add("EX").
			Add(redisDuration(ttl))...,
	)
	return err
}

// MSetWithTTL leverages redis pipelining to issue many SET commands with TTL specified.
// Pipelining is necessary since MSET alone does not support TTL arguments.
func (r *redis) MSetWithTTL(ctx context.Context, keys []string, values []string, ttl time.Duration) (err error) {
	if len(keys) != len(values) {
		return errors.New("len(keys) must equal len(values)")
	}

	p := r.NewPipeline()
	defer func() {
		if err2 := p.Close(); err == nil && err2 != nil {
			err = fmt.Errorf("error closing body: %s", err2)
		}
	}()

	for idx, key := range keys {
		err = p.SetWithTTL(ctx, key, values[idx], ttl)
		if err != nil {
			return err
		}
	}

	_, err = p.Do(ctx, r.stat("commands.mset_with_ttl"))
	return err
}

func (r *redis) Get(ctx context.Context, key string) (value string, ok bool, err error) {
	value, err = redigo.String(r.do(
		ctx,
		r.stat("commands.get"),
		"GET",
		redigo.Args{}.Add(r.Key(key))...,
	))
	if err == RedisNilVal {
		return value, false, nil
	} else if err != nil {
		return value, false, err
	}

	return value, true, err
}

// MGet issues a request for ordered specified keys and returns a slice of values in corresponding order.
func (r *redis) MGet(ctx context.Context, keys ...string) (values []string, err error) {
	fullKeys := make([]string, len(keys))
	for i, key := range keys {
		fullKeys[i] = r.Key(key)
	}

	return redigo.Strings(r.do(
		ctx,
		r.stat("commands.mget"),
		"MGET",
		redigo.Args{}.AddFlat(fullKeys)...,
	))
}

func (r *redis) Hmget(ctx context.Context, hash string, keys ...string) (values []string, err error) {
	return redigo.Strings(r.do(
		ctx,
		r.stat("commands.hmget"),
		"HMGET",
		redigo.Args{}.Add(r.Key(hash)).
			AddFlat(keys)...,
	))
}

func (r *redis) Del(ctx context.Context, keys ...string) (deleted int, err error) {
	args := redigo.Args{}
	for _, key := range keys {
		args = args.Add(r.Key(key))
	}
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.del"),
		"DEL",
		args...,
	))
}

func (r *redis) Ttl(ctx context.Context, key string) (ttl int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.ttl"),
		"TTL",
		redigo.Args{}.Add(r.Key(key))...,
	))
}

func (r *redis) Expire(ctx context.Context, key string, ttl time.Duration) error {
	_, err := r.do(
		ctx,
		r.stat("commands.expire"),
		"EXPIRE",
		redigo.Args{}.Add(r.Key(key)).
			Add(redisDuration(ttl))...,
	)
	return err
}

func (r *redis) LPush(ctx context.Context, key string, value string) (count uint64, err error) {
	return redigo.Uint64(r.do(
		ctx,
		r.stat("commands.lpush"),
		"LPUSH",
		redigo.Args{}.Add(r.Key(key)).
			Add(value)...,
	))
}

func (r *redis) LTrim(ctx context.Context, key string, start, end int64) (err error) {
	_, err = r.do(
		ctx,
		r.stat("commands.ltrim"),
		"LTRIM",
		redigo.Args{}.Add(r.Key(key)).
			Add(start).
			Add(end)...,
	)
	return err
}

func (r *redis) LRange(ctx context.Context, key string, start, end int64) (values []string, err error) {
	return redigo.Strings(r.do(
		ctx,
		r.stat("commands.lrange"),
		"LRANGE",
		redigo.Args{}.Add(r.Key(key)).
			Add(start).
			Add(end)...,
	))
}

func (r *redis) LRem(ctx context.Context, key string, count int64, item string) (removed int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.lrem"),
		"LREM",
		redigo.Args{}.Add(r.Key(key)).
			Add(count).
			Add(item)...,
	))
}

func (r *redis) Sadd(ctx context.Context, key, item string) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.sadd"),
		"SADD",
		redigo.Args{}.Add(r.Key(key)).
			Add(item)...,
	))
}

func (r *redis) Srem(ctx context.Context, key, item string) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.srem"),
		"SREM",
		redigo.Args{}.Add(r.Key(key)).
			Add(item)...,
	))
}

func (r *redis) Scard(ctx context.Context, key string) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.scard"),
		"SCARD",
		redigo.Args{}.Add(r.Key(key))...,
	))
}

func (r *redis) Smembers(ctx context.Context, key string) (values []string, err error) {
	return redigo.Strings(r.do(
		ctx,
		r.stat("commands.smembers"),
		"SMEMBERS",
		redigo.Args{}.Add(r.Key(key))...,
	))
}

func (r *redis) Zadd(ctx context.Context, key string, score int64, item string) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.zadd"),
		"ZADD",
		redigo.Args{}.Add(r.Key(key)).
			Add(score).
			Add(item)...,
	))
}

func (r *redis) Zcard(ctx context.Context, key string) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.zcard"),
		"ZCARD",
		redigo.Args{}.Add(r.Key(key))...,
	))
}

func (r *redis) Zrem(ctx context.Context, key string, item string) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.zrem"),
		"ZREM",
		redigo.Args{}.Add(r.Key(key)).
			Add(item)...,
	))
}

func (r *redis) Zscore(ctx context.Context, key string, item string) (score int64, err error) {
	return redigo.Int64(r.do(
		ctx,
		r.stat("commands.zscore"),
		"ZSCORE",
		redigo.Args{}.Add(r.Key(key)).
			Add(item)...,
	))
}

func (r *redis) Zrangebyscore(ctx context.Context, key string, min int64, max int64) ([]string, error) {
	return redigo.Strings(r.do(
		ctx,
		r.stat("commands.zrangebyscore"),
		"ZRANGEBYSCORE",
		redigo.Args{}.Add(r.Key(key)).
			Add(min).
			Add(max)...,
	))
}

func (r *redis) Zremrangebyscore(ctx context.Context, key string, fromScore int, toScore int) (count int, err error) {
	return redigo.Int(r.do(
		ctx,
		r.stat("commands.zremrangebyscore"),
		"ZREMRANGEBYSCORE",
		redigo.Args{}.Add(r.Key(key)).
			Add(fromScore).
			Add(toScore)...,
	))
}

func (r *redis) Eval(ctx context.Context, script string, keys []interface{}, args []interface{}, stat string) (
	reply interface{}, err error,
) {
	return r.eval(ctx, script, keys, args, r.stat("commands.eval."+stat))
}

func (r *redis) eval(ctx context.Context, script string, keys []interface{}, args []interface{}, stat string) (
	reply interface{}, err error,
) {
	prefixedKeys := r.prefixKeys(keys)
	return r.do(
		ctx,
		stat,
		"EVAL",
		redigo.Args{}.Add(script).
			Add(len(prefixedKeys)).
			AddFlat(stringsToInterfaces(prefixedKeys)).
			AddFlat(args)...,
	)
}

func (r *redis) getConn() redigo.Conn {
	active := atomic.AddInt32(&r.active, 1)
	r.mu.Lock()
	if active > r.maxActive {
		r.maxActive = active
	}
	r.mu.Unlock()

	return r.redis.Get()
}

func (r *redis) closeConn(conn redigo.Conn) error {
	atomic.AddInt32(&r.active, -1)
	return conn.Close()
}

func (r *redis) doWithConn(ctx context.Context, client redigo.Conn, stat, cmd string, args ...interface{}) (reply interface{}, err error) {
	sub, startTime := r.xactStart(ctx)
	reply, err = client.Do(cmd, args...)

	duration := r.xactEnd(sub, startTime)

	var outcome string
	var statsSampleRate = float32(0.1)
	if err != nil {
		outcome = "error"
		statsSampleRate = 1.0
	} else {
		outcome = "success"
	}
	stat = fmt.Sprintf("%s.%s", stat, outcome)
	r.stats.Inc(stat, 1, statsSampleRate)
	r.stats.TimingDuration(stat, duration, statsSampleRate)

	return reply, err
}

func (r *redis) do(ctx context.Context, stat, cmd string, args ...interface{}) (reply interface{}, err error) {
	client := r.getConn()
	defer func() {
		if err2 := r.closeConn(client); err == nil && err2 != nil {
			err = fmt.Errorf("error closing body: %s", err2)
		}
	}()

	return r.doWithConn(ctx, client, stat, cmd, args...)
}

func (r *redis) xactStart(ctx context.Context) (sub *timing.SubXact, now time.Time) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub = xact.Sub(r.xactGroup)
		return sub, sub.Start()
	}
	return nil, time.Now()
}

func (_ *redis) xactEnd(sub *timing.SubXact, startTime time.Time) time.Duration {
	if sub != nil {
		return sub.End()
	}
	return time.Since(startTime)
}

func (r *redis) stat(name string) string {
	return r.statsPrefix + name
}

func (r *redis) prefixKeys(keys []interface{}) []string {
	prefixedKeys := make([]string, len(keys))
	for i, k := range keys {
		prefixedKeys[i] = r.Key(k.(string))
	}
	return prefixedKeys
}

// Redis expects strings representing integers for TTLs
func redisDuration(d time.Duration) string {
	return strconv.Itoa(int(d.Seconds()))
}

func stringsToInterfaces(slice []string) (ret []interface{}) {
	ret = make([]interface{}, len(slice))
	for i, str := range slice {
		ret[i] = str
	}
	return ret
}
