package emoticons

import (
	"strconv"
	"strings"

	"golang.org/x/net/context"

	"code.justin.tv/chat/rails"
)

// regexChars contains characters that hints that an emoticon contains a regular expression.
var regexChars = `\|^$*+?:#`

// Fetcher fetches emoticons from some source. It should not contain caching or retry logic.
type Fetcher interface {
	Fetch(ctx context.Context) (*Lexicon, []int, error)
}

// NewFetcherFromRails returns a Fetcher backed by Rails.
func NewFetcherFromRails(r rails.Rails) Fetcher {
	return &railsEmoticonsFetcher{
		rails: r,
	}
}

type railsEmoticonsFetcher struct {
	rails rails.Rails
}

func (f *railsEmoticonsFetcher) Fetch(ctx context.Context) (*Lexicon, []int, error) {
	resp, err := f.rails.AllEmoticons(ctx, nil)
	if err != nil {
		return nil, nil, err
	}

	lex := Lexicon{}
	sets := []int{}
	for set, emoticonSlice := range resp.EmoticonSets {
		lexSet, err := strconv.Atoi(set)
		if err != nil {
			continue
		}

		lexEmoticonSlice := make([]Emoticon, 0, len(emoticonSlice))
		for _, e := range emoticonSlice {
			lexEmoticonSlice = append(lexEmoticonSlice, Emoticon{
				ID:      e.Id,
				Pattern: e.Pattern,
			})
		}
		lex[lexSet] = lexEmoticonSlice
		sets = append(sets, lexSet)
	}
	return &lex, sets, nil
}

// Emoticon stores an emoticon's ID and word or regex code (e.g. "Kappa", "\\:-?\\)").
type Emoticon struct {
	ID      int
	Pattern string
}

// IsRegex returns true if the emoticon is likely a regular expression.
func (e *Emoticon) IsRegex() bool {
	return strings.ContainsAny(e.Pattern, regexChars)
}
