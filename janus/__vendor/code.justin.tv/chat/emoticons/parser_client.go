package emoticons

import (
	"errors"
	"math/rand"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/net/context"

	"code.justin.tv/chat/emoticons/models"
	"code.justin.tv/chat/redicache"
	"code.justin.tv/chat/timing"
)

const (
	staggerDuration = 1 * time.Minute
)

type ParserClientConfig struct {
	FetchInterval time.Duration
	CacheDuration time.Duration
}

// ParserClient wraps the basic parser with logic to auto-update the lexicon from a Fetcher.
type ParserClient interface {
	Init(ctx context.Context) error
	Parse(msg string, emoticonSets []int) ([]emoticonmodels.Match, error)
}

func NewParserClient(conf ParserClientConfig, fetcher Fetcher, cache redicache.Cache, stats statsd.Statter) ParserClient {
	return &parserClientImpl{
		Parser:  NewParser(),
		conf:    conf,
		fetcher: fetcher,
		cache:   cache,
		stats:   stats,

		initialized: false,
	}
}

type parserClientImpl struct {
	Parser
	conf    ParserClientConfig
	fetcher Fetcher
	cache   redicache.Cache
	stats   statsd.Statter

	initialized bool
}

func (p *parserClientImpl) Init(ctx context.Context) error {
	// Add xact to ctx
	xact := &timing.Xact{
		Stats: p.stats,
	}
	xact.AddName("emoticons_parser_client")
	ctx = timing.XactContext(ctx, xact)

	go func() {
		// Stagger the update interval in case multiple parser clients are restarted at the same time
		jitter := jitter(staggerDuration, 0.75, 1.0)
		<-time.After(jitter)

		ticker := time.NewTicker(p.conf.FetchInterval)
		defer ticker.Stop()
		for range ticker.C {
			if err := p.update(ctx); err != nil {
				// TODO (pho): log error?
			}
		}
	}()
	// Update emoticons now and periodically
	if err := p.update(ctx); err != nil {
		return err
	}

	p.initialized = true
	return nil
}

func (p *parserClientImpl) Parse(msg string, emoticonSets []int) ([]emoticonmodels.Match, error) {
	if !p.initialized {
		return nil, errors.New("cannot parse before initializing emoticons parser client")
	}

	matches := p.Parser.Parse(msg, emoticonSets)

	// Convert byte positions to rune positions.
	runeIndexer := NewRuneIndexer(msg)
	for i, _ := range matches {
		matches[i].Start = runeIndexer.RuneIndex(matches[i].Start)
		matches[i].End = runeIndexer.RuneIndex(matches[i].End)
	}
	return matches, nil
}

func (p *parserClientImpl) update(ctx context.Context) error {
	var res *Lexicon
	err := p.cache.Cached(ctx, "lexicon", p.conf.CacheDuration, &res, func() (interface{}, error) {
		lex, _, err := p.fetcher.Fetch(ctx)
		return lex, err
	})
	if err != nil {
		return err
	}
	p.Parser.UpdateLexicon(*res)
	return nil
}

// jitter between low% and high% of duration
func jitter(d time.Duration, low, high float64) time.Duration {
	return time.Duration((float64(d) * low) + (float64(d) * (high - low) * rand.Float64()))
}
