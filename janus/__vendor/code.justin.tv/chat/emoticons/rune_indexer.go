package emoticons

// runeIndexer provides functionality to get the the rune position for a given string byte position.
// Example: Given "⌘ Kappa", the emoticons parser matches "Kappa" at byte positions 4-9 because "⌘" is 3 bytes wide.
// Calling RuneIndex(idx) would return positions 2-6 (where "K" is the third rune in the string).
type runeIndexer struct {
	indices []int
}

func NewRuneIndexer(str string) runeIndexer {
	indices := make([]int, len(str))
	count := 0
	for idx, _ := range str {
		indices[idx] = count
		count++
	}
	return runeIndexer{
		indices: indices,
	}
}

func (r runeIndexer) RuneIndex(idx int) int {
	return r.indices[idx]
}
