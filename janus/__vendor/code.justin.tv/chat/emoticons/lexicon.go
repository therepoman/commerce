package emoticons

import (
	"bytes"
	"encoding/json"
	"strconv"
)

var (
	nullJSON []byte           = []byte("null")
	_        json.Marshaler   = (*Lexicon)(nil)
	_        json.Unmarshaler = (*Lexicon)(nil)
)

// Lexicon is a dictionary of emoticons bucketed by emoticon set for quick filtering
type Lexicon map[int][]Emoticon

// MarshalJSON satisfies the json.Marshaler interface.
func (l *Lexicon) MarshalJSON() ([]byte, error) {
	m := make(map[string][]Emoticon, len(*l))
	for k, v := range *l {
		m[strconv.Itoa(k)] = v
	}
	return json.Marshal(m)
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (l *Lexicon) UnmarshalJSON(b []byte) error {
	*l = Lexicon{}
	if bytes.Equal(b, nullJSON) {
		return nil
	}

	res := map[string][]Emoticon{}
	if err := json.Unmarshal(b, &res); err != nil {
		return err
	}
	for k, v := range res {
		kInt, err := strconv.Atoi(k)
		if err != nil {
			continue
		}
		(*l)[kInt] = v
	}
	return nil
}
