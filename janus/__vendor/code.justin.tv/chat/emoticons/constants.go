package emoticons

const (
	// DefaultEmoticonSet represents the emoticon set available to all users.
	// Generic emoticons--including "Kappa", ":)", "<3"--belong to the default set.
	DefaultEmoticonSet = 0

	// GoldenKappaEmoticonSet is the emote set for the Golden Kappa emoticon.
	GoldenKappaEmoticonSet = 15940

	// CurseConnectedEmoticonSet is the emote set for emoticons granted to Twitch
	// users with accounts connected to Curse.
	CurseConnectedEmoticonSet = 19151
)
