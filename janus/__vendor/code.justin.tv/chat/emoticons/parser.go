package emoticons

import (
	"fmt"
	"html"
	"log"
	"regexp"
	"sort"
	"sync"
	"unicode"

	"code.justin.tv/chat/emoticons/models"
)

const (
	minTokenLen = 2
)

var (
	backslashRe       = regexp.MustCompile(`\\([:;&])`)
	ErrInvalidEmoteID = fmt.Errorf("Invalid emote id")
)

// byStartIndex implements sort.Interface
type byStartIndex []emoticonmodels.Match

func (s byStartIndex) Len() int {
	return len(s)
}
func (s byStartIndex) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byStartIndex) Less(i, j int) bool {
	return s[i].Start < s[j].Start
}

// Parser
type Parser interface {
	// Parse returns a list of emoticon matches (sorted by start index) found in the given string and user's emoticon sets.
	Parse(str string, sets []int) []emoticonmodels.Match
	// HasLexicon checks whether the Parser has a non-empty lexicon of emoticons.
	HasLexicon() bool
	// UpdateLexicon updates the lexicon of emoticons the parser can search for.
	UpdateLexicon(l Lexicon)
	// LookupEmote returns the name of the emote by id
	LookupEmote(id int) (string, error)
	// SetByEmoteID returns the set for the given emote by id
	SetByEmoteID(id int) (int, error)
}

// NewParser returns an emoticon parser interface.
func NewParser() Parser {
	return &parserImpl{
		regexLexBySet: make(map[int]*regexLexicon, 0),
		tokenLexBySet: make(map[int]*tokenLexicon, 0),
	}
}

type parserImpl struct {
	regexLexBySet map[int]*regexLexicon
	tokenLexBySet map[int]*tokenLexicon
	emoteMap      map[int]string
	setMap        map[int]int

	rwMutex sync.RWMutex
}

func (p *parserImpl) Parse(str string, sets []int) []emoticonmodels.Match {
	if !p.HasLexicon() {
		return []emoticonmodels.Match{}
	}

	// TODO: rely on caller to pass in nonempty emoticon sets
	if len(sets) == 0 {
		sets = []int{DefaultEmoticonSet}
	} else {
		// Sort emoticon sets in decreasing order so higher emoticon sets are matched first.
		sort.Sort(sort.Reverse(sort.IntSlice(sets)))
		// Add the default emoticon set if not already in the list of emoticon sets.
		if sets[len(sets)-1] != DefaultEmoticonSet {
			sets = append(sets, DefaultEmoticonSet)
		}
	}

	return p.findMatches(str, sets)
}

func (p *parserImpl) HasLexicon() bool {
	p.rwMutex.RLock()
	valid := (len(p.regexLexBySet) != 0 || len(p.tokenLexBySet) != 0)
	p.rwMutex.RUnlock()
	return valid
}

func (p *parserImpl) UpdateLexicon(lex Lexicon) {
	regBySet := map[int]*regexLexicon{}
	tokBySet := map[int]*tokenLexicon{}
	emoteMap := map[int]string{}
	setMap := map[int]int{}
	for set, emoticons := range lex {
		for _, e := range emoticons {
			if e.IsRegex() {
				if _, ok := regBySet[set]; !ok {
					regBySet[set] = &regexLexicon{}
				}
				regBySet[set].Insert(e)
			} else {
				emoteMap[e.ID] = e.Pattern
				if _, ok := tokBySet[set]; !ok {
					tokBySet[set] = &tokenLexicon{}
				}
				tokBySet[set].Insert(e)
			}
			setMap[e.ID] = set
		}
	}

	p.rwMutex.Lock()
	p.regexLexBySet = regBySet
	p.tokenLexBySet = tokBySet
	p.emoteMap = emoteMap
	p.setMap = setMap
	p.rwMutex.Unlock()
}

func (p *parserImpl) LookupEmote(id int) (string, error) {
	p.rwMutex.RLock()
	emote, ok := p.emoteMap[id]
	p.rwMutex.RUnlock()
	if !ok {
		return "", ErrInvalidEmoteID
	}
	return emote, nil
}

func (p *parserImpl) SetByEmoteID(id int) (int, error) {
	set, ok := p.setMap[id]
	if !ok {
		return -1, ErrInvalidEmoteID
	}
	return set, nil
}

func (p *parserImpl) findMatches(str string, sets []int) []emoticonmodels.Match {
	matches := []emoticonmodels.Match{}
	wordOffsets := parseWordOffsets(str)

	p.rwMutex.RLock()
	regBySet := p.regexLexBySet
	tokBySet := p.tokenLexBySet
	p.rwMutex.RUnlock()

	for word, offsets := range wordOffsets {
		var id, matchSet int

		// Find the first emoticon that matches the word.
		for _, set := range sets {
			if t, ok := tokBySet[set]; ok {
				id = t.Match(word)
				matchSet = set
				if id != 0 {
					break
				}
			}
			if r, ok := regBySet[set]; ok {
				id = r.Match(word)
				matchSet = set
				if id != 0 {
					break
				}
			}
		}

		if id != 0 {
			wordLen := len(word)
			for _, off := range offsets {
				matches = append(matches, emoticonmodels.Match{
					ID:    id,
					Start: off,
					End:   off + wordLen - 1,
					Set:   matchSet,
				})
			}
		}
	}
	return matches
}

// regexLexicon maps an emoticon ID to compiled regexp.
type regexLexicon map[int]*regexp.Regexp

func (l *regexLexicon) Insert(e Emoticon) {
	p := backslashRe.ReplaceAllString(e.Pattern, "$1")
	p = html.UnescapeString(p)
	p = fmt.Sprintf(`^%s$`, p)
	re, err := regexp.Compile(p)
	if err != nil {
		log.Println("error compiling regex pattern:", p)
		return
	}
	(*l)[e.ID] = re
}

func (l *regexLexicon) Match(word string) int {
	for id, re := range *l {
		if re.MatchString(word) {
			return id
		}
	}
	return 0
}

// tokenLexicon maps a word to an emoticon ID.
type tokenLexicon map[string]int

func (l *tokenLexicon) Insert(e Emoticon)     { (*l)[e.Pattern] = e.ID }
func (l *tokenLexicon) Match(word string) int { return (*l)[word] }

// parseWordOffsets finds the index offset(s) for each space-separated word.
// Example:
// 		"hello Kappa      :) Kappa"   =>   {"hello": [0], "Kappa": [6, 20], ":)": [17]}
func parseWordOffsets(str string) map[string][]int {
	offsets := map[string][]int{}
	a := 0
	for z, ch := range str {
		if unicode.IsSpace(ch) {
			if a-z < minTokenLen {
				word := str[a:z]
				offsets[word] = append(offsets[word], a)
			}
			a = z + 1
		}
	}
	if a < len(str) {
		word := str[a:]
		offsets[word] = append(offsets[word], a)
	}
	return offsets
}
