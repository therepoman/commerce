package emoticonmodels

// Match stores the start and end indices where an emoticon was found in a string.
type Match struct {
	// ID is the unique row ID for the emoticon.
	ID int `json:"id"`
	// Start is the index in the original string where the emoticon starts (0-indexed, inclusive).
	Start int `json:"start"`
	// End is the index in the original string where the emoticon ends (0-indexed, inclusive).
	End int `json:"end"`
	// Set is the emoticon set that contains this emoticon.
	Set int `json:"set"`
}
