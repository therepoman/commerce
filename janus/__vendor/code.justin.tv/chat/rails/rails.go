package rails

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"

	"code.justin.tv/common/twitchhttp"
)

const defaultStatSampleRate = 1.0

type Rails interface {
	AllEmoticons(ctx context.Context, reqOpts *twitchhttp.ReqOpts) (*EmoticonsResp, error)
	AreStreamUpEmailsDisabled(ctx context.Context, userID int, reqOpts *twitchhttp.ReqOpts) (bool, error)
	FacebookConnected(ctx context.Context, userName string, reqOpts *twitchhttp.ReqOpts) (bool, error)
	TriggerCommercial(ctx context.Context, channelName string, length time.Duration, reqOpts *twitchhttp.ReqOpts) (time.Duration, error)

	ListUserBlocks(ctx context.Context, userID string, params ListUserBlockParams, reqOpts *twitchhttp.ReqOpts) (ListUserBlockResponse, error)
	AddUserBlock(ctx context.Context, userID, targetUserID, reason, sourceContext string, reqOpts *twitchhttp.ReqOpts) (AddUserBlockResponse, error)
	RemoveUserBlock(ctx context.Context, userID, targetUserID string, reqOpts *twitchhttp.ReqOpts) error
}

type railsImpl struct {
	twitchhttp.Client
	apiKey string
}

// New returns a new Rails HTTP client.
// <apiKey> is required only if making calls to a public endpoint.
func New(conf twitchhttp.ClientConf, apiKey string) (Rails, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = "rails"
	}
	c, err := twitchhttp.NewClient(conf)
	if err != nil {
		return nil, err
	}

	return &railsImpl{Client: c, apiKey: apiKey}, nil
}

type triggerCommercialResp struct {
	Length int `json:"length"`
}

func (r *railsImpl) TriggerCommercial(ctx context.Context, channelName string, length time.Duration, reqOpts *twitchhttp.ReqOpts) (time.Duration, error) {
	values := url.Values{"length": {strconv.Itoa(int(length.Seconds()))}}
	url := url.URL{
		Path:     fmt.Sprintf("/api/internal/users/%s/commercial", channelName),
		RawQuery: values.Encode(),
	}
	req, err := r.NewRequest(http.MethodPost, url.String(), bytes.NewReader([]byte("{}")))
	if err != nil {
		return 0, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.internal.commercial",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded triggerCommercialResp
	if _, err := r.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return 0, err
	}
	return time.Duration(decoded.Length) * time.Second, nil
}

type EmoticonsResp struct {
	EmoticonSets map[string][]Emoticon `json:"emoticon_sets"`
}

type Emoticon struct {
	Id      int    `json:"id"`
	Pattern string `json:"code"`
}

func (r *railsImpl) AllEmoticons(ctx context.Context, reqOpts *twitchhttp.ReqOpts) (*EmoticonsResp, error) {
	values := url.Values{"emotesets": {"all"}}
	url := url.URL{
		Path:     "kraken/chat/emoticon_images",
		RawQuery: values.Encode(),
	}
	req, err := r.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Client-ID", r.apiKey)

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.kraken.emoticon_images",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded EmoticonsResp
	if _, err := r.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return nil, err
	}
	return &decoded, nil
}

type facebookConnectedResp struct {
	Id                int    `json:"id"`
	Login             string `json:"login"`
	FacebookConnected bool   `json:"facebook_connected?"`
}

func (r *railsImpl) FacebookConnected(ctx context.Context, userName string, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	values := url.Values{
		"logins":     {userName},
		"properties": {"facebook_connected?"},
	}
	url := url.URL{
		Path:     "/api/internal/user/properties",
		RawQuery: values.Encode(),
	}
	req, err := r.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.internal.booluserprop",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded []facebookConnectedResp
	if _, err := r.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return false, err
	}
	return decoded[0].FacebookConnected, nil
}

type streamUpEmailsDisabledResponse struct {
	ID                        int    `json:"id"`
	Login                     string `json:"login"`
	AreStreamUpEmailsDisabled bool   `json:"stream_up_emails_disabled"`
}

// Returns true if the user has disabled "stream up" email notifications
// Also returns true if the user has disabled all email notifications
func (r *railsImpl) AreStreamUpEmailsDisabled(ctx context.Context, userID int, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	values := url.Values{
		"ids":        {strconv.Itoa(userID)},
		"properties": {"stream_up_emails_disabled"},
	}
	url := url.URL{
		Path:     "/api/internal/user/properties",
		RawQuery: values.Encode(),
	}
	req, err := r.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return false, err
	}
	// TODO (pho): is this needed? the caller just be passing in this correct host
	req.Host = "api.internal.twitch.tv"

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.internal.stream_up_emails_disabled",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded []streamUpEmailsDisabledResponse
	if _, err := r.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return false, err
	}
	return decoded[0].AreStreamUpEmailsDisabled, nil
}

type AddUserBlockResponse struct {
	Status  int    `json:"status"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

func (r *railsImpl) AddUserBlock(ctx context.Context, userID, targetUserID, reason, sourceContext string, reqOpts *twitchhttp.ReqOpts) (AddUserBlockResponse, error) {
	values := url.Values{}
	if reason != "" {
		values.Set("reason", reason)
	}
	if sourceContext == "whisper" {
		values.Set("whisper", "true")
	}

	url := url.URL{
		Path: fmt.Sprintf("/api/internal/users/%s/blocks/%s", userID, targetUserID),
	}
	req, err := r.NewRequest(http.MethodPut, url.String(), strings.NewReader(values.Encode()))
	if err != nil {
		return AddUserBlockResponse{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.internal.add_user_block",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := r.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return AddUserBlockResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		var decoded AddUserBlockResponse
		if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
			return AddUserBlockResponse{}, err
		}
		return decoded, nil
	}
	return AddUserBlockResponse{Status: http.StatusNoContent}, nil
}

func (r *railsImpl) RemoveUserBlock(ctx context.Context, userID, targetUserID string, reqOpts *twitchhttp.ReqOpts) error {
	url := url.URL{
		Path: fmt.Sprintf("/api/internal/users/%s/blocks/%s", userID, targetUserID),
	}
	req, err := r.NewRequest(http.MethodDelete, url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.internal.remove_user_block",
		StatSampleRate: defaultStatSampleRate,
	})
	_, err = r.Do(ctx, req, combinedReqOpts)
	return err
}

type ListUserBlockParams struct {
	Limit  int
	Offset int
}

type ListUserBlockResponse struct {
	Blocks []string `json:"blocks"`
	Total  int      `json:"_total"`
}

func (r *railsImpl) ListUserBlocks(ctx context.Context, userID string, params ListUserBlockParams, reqOpts *twitchhttp.ReqOpts) (ListUserBlockResponse, error) {
	values := url.Values{}
	if params.Limit != 0 {
		values.Set("limit", strconv.Itoa(params.Limit))
	}
	if params.Offset != 0 {
		values.Set("offset", strconv.Itoa(params.Offset))
	}

	url := url.URL{
		Path:     fmt.Sprintf("/api/internal/users/%s/blocks", userID),
		RawQuery: values.Encode(),
	}
	req, err := r.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return ListUserBlockResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "rails.internal.list_user_blocks",
		StatSampleRate: defaultStatSampleRate,
	})
	var decoded ListUserBlockResponse
	if _, err := r.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return ListUserBlockResponse{}, err
	}
	return decoded, nil
}
