package models

// BadgeSet maps badge set versions to display info
type BadgeSet struct {
	Versions map[string]BadgeVersion `json:"versions"`
}

// BadgeVersion contains display info for a single badge set version
type BadgeVersion struct {
	ImageURL1x  *string `json:"image_url_1x"`
	ImageURL2x  *string `json:"image_url_2x"`
	ImageURL4x  *string `json:"image_url_4x"`
	Description string  `json:"description"`
	Title       string  `json:"title"`
	ClickAction string  `json:"click_action"`
	ClickURL    string  `json:"click_url"`
}

// Badge is defined by just a badge id and version
type Badge struct {
	BadgeSetID      string `json:"badge_set_id"`
	BadgeSetVersion string `json:"badge_set_version"`
}
