package models

// GetBadgeDisplayResponse is the response for GetGlobalBadgeDisplayInfo and GetChannelBadgeDisplayInfo
type GetBadgeDisplayResponse struct {
	BadgeSets map[string]BadgeSet `json:"badge_sets"`
}

// GetAvailableBadgesResponse is the response for AvailableGlobalUserBadges and AvailableChannelUserBadges
type GetAvailableBadgesResponse struct {
	AvailableBadges []Badge `json:"available_badges"`
}

// SelectBadgeResponse is an empty struct, should probably replace this (TODO)
type SelectBadgeResponse struct{}

// GrantBadgeResponse is an empty struct, should probably replace this (TODO)
type GrantBadgeResponse struct{}

// UserBadgesResponse is the response for the UserBadges endpoint
// Fields are null if not requested, or if user has no badge in slot
type UserBadgesResponse struct {
	GlobalAuthority  *Badge `json:"global_authority"`
	ChannelAuthority *Badge `json:"channel_authority"`
	Subscriber       *Badge `json:"subscriber"`
	GlobalSelected   *Badge `json:"global_selected"`
	ChannelSelected  *Badge `json:"channel_selected"`
}

// ClearCacheResponse is the response for the InvalidateSubBadgeCache endpoint
type ClearCacheResponse struct {
	Cleared bool `json:"cleared"`
}

// UploadImagesResponse is the response for the UploadImages endpoints
// Versions is a map of badge_set_version to ImageURLs
type UploadImagesResponse struct {
	Versions map[string]ImageURLs `json:"versions"`
}

// ImageURLs contains the URLs for all 3 sizes of a specific badge version
type ImageURLs struct {
	ImageURL1x string `json:"image_url_1x"`
	ImageURL2x string `json:"image_url_2x"`
	ImageURL4x string `json:"image_url_4x"`
}

// BulkRequestResponse is the response from a bulk update/remove endpoint
type BulkRequestResponse struct {
	Error        string  `json:"error"`
	ProcessedIDs []int64 `json:"processed_ids"`
}
