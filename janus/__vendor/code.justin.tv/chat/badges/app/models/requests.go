package models

// SelectBadgeParams sets a user's "selected" badge set to the specified badge set
// Same for selecting a badge set globally and selecting a badge set in a channel
type SelectBadgeParams struct {
	BadgeSetID string `json:"badge_set_id"`
}

// UserBadgesRequestParams can retrieve every badge slot for a user
type UserBadgesRequestParams struct {
	// true/false, whether you want the user's global authority badge
	GlobalAuthority bool `json:"global_authority"`
	// channel_id if you want the user's channel authority badge
	// null if you don't want the user's channel authority badge
	ChannelAuthority *int64 `json:"channel_authority"`
	// channel_id if you want the user's subscriber badge
	// null if you don't want the user's subscriber badge
	Subscriber *int64 `json:"subscriber"`
	// true/false, whether you want the user's globally selected badge
	GlobalSelected bool `json:"global_selected"`
	// channel_id if you want the user's channel selected badge
	// null if you don't want the user's channel selected badge
	ChannelSelected *int64 `json:"channel_selected"`
	// a set of user or user/channel properties that are already known by caller
	Metadata Metadata `json:"metadata"`
}

// Metadata is information that the caller already knows about a user
// Eavery field is optional
type Metadata struct {
	IsStaff       *bool `json:"is_staff"`
	IsAdmin       *bool `json:"is_admin"`
	IsGlobalMod   *bool `json:"is_global_mod"`
	IsTurbo       *bool `json:"is_turbo"`
	IsTurboHidden *bool `json:"is_turbo_hidden"`
	IsModerator   *bool `json:"is_moderator"`
	IsSubscriber  *bool `json:"is_subscriber"`
}

// GrantBadgeParams grants a user a badge version, with an optional end time
// It is intended to be used for endpoints such as /subs and /bits that
// already know the BadgeSetID
type GrantBadgeParams struct {
	BadgeSetVersion string `json:"badge_set_version"`
	EndTimestamp    int64  `json:"end_timestamp"`
}

// InvalidateBadgeParams clears the cached image urls
type InvalidateBadgeParams struct {
	UserID     int64 `json:"user_id"`
	OldBadgeID int   `json:"old_badge_id"`
}

// UploadImagesParams uploads images to s3, and sets the display info
// in dynamodb. Versions is a map of badge_set_version to image info
type UploadImagesParams struct {
	Versions map[string]ImageVersion `json:"versions"`
}

// ImageVersion contains the display info for a single badge version, including
// The title and the various sizes (in base64-encoded strings)
type ImageVersion struct {
	Title        string `json:"title"`
	ImageBytes1x string `json:"image_bytes_1x"`
	ImageBytes2x string `json:"image_bytes_2x"`
	ImageBytes4x string `json:"image_bytes_4x"`
}

// RemoveImageParams is used to remove a specific version of a channel's sub
// badge image
type RemoveImageParams struct {
	BadgeSetVersion string `json:"badge_set_version"`
}

// BulkGrantBadgeParams is used to grant a badge version to many users at once
type BulkGrantBadgeParams struct {
	BadgeSetVersion string  `json:"badge_set_version"`
	UserIDs         []int64 `json:"user_ids"`
}

// BulkRemoveBadgeParams is used to remove a badge from many users at once
type BulkRemoveBadgeParams struct {
	UserIDs []int64 `json:"user_ids"`
}
