package badges

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"golang.org/x/net/context"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/common/twitchhttp"
)

const (
	defaultStatSampleRate = 1.0
	defaultTimingXactName = "badges"
	defaultCtxTimeout     = 2 * time.Second
)

// Client is an interface for hitting Badges endpoints
type Client interface {
	// AvailableGlobalBadges gets the badges that are available to a user in the global scope
	AvailableGlobalBadges(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.GetAvailableBadgesResponse, error)
	// DeselectGlobalBadge unselects a user's global badge preference. They will have
	// no badge selected globally after calling
	DeselectGlobalBadge(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error
	// SelectGlobalBadge selects a badge for a user globally.
	// Returns false if the user does not have permissions to set the badge.
	SelectGlobalBadge(ctx context.Context, userID string, params models.SelectBadgeParams, reqOpts *twitchhttp.ReqOpts) (bool, error)
	// SelectTurboGlobalBadge selects the "turbo" badge for a user globally.
	// Returns false if the user does not have turbo
	SelectTurboGlobalBadge(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (bool, error)
	// AvailableChannelBadges gets the badges that are available to a user in the given channel scope
	AvailableChannelBadges(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) (*models.GetAvailableBadgesResponse, error)
	// DeselectChannelBadge unselects a user's badge on a specific channel. They
	// will have no badge selected on that channel after calling.
	DeselectChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) error
	// SelectChannelBadge selects a badge for a user globally.
	// Returns false if the user does not have permissions to set the badge.
	SelectChannelBadge(ctx context.Context, userID, channelID string, params models.SelectBadgeParams, reqOpts *twitchhttp.ReqOpts) (bool, error)
	// SelectBitsBadge selects the "bits" badge for a user on a channel
	// Returns false if the user does not have a bits badge available on the channel
	SelectBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) (bool, error)
	// GetSelectedGlobalBadge returns the badge that the user has selected to display globally
	// Returns nil if no badge is selected
	GetSelectedGlobalBadge(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.Badge, error)
	// GetSelectedChannelBadge returns the badge that the user has selected to display for this channel
	// Returns nil if no badge is selected
	GetSelectedChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) (*models.Badge, error)
	// UserBadges returns a user's badges in whichever scope(s) are requested
	// It takes metadata in the request (params.Metadata) and uses it in lieu of
	// hitting other services, if possible.
	UserBadges(ctx context.Context, userID string, params models.UserBadgesRequestParams, reqOpts *twitchhttp.ReqOpts) (*models.UserBadgesResponse, error)
	// GetSubscriberImages returns a channel's subscriber badge image URLs
	GetSubscriberImages(ctx context.Context, channelID string, reqOpts *twitchhttp.ReqOpts) (*models.BadgeSet, error)
	// UploadSubscriberImages uploads a a channel's subscriber images for some set
	// of badge_set_versions to the Badges Service, and returns the generated URLs
	UploadSubscriberImages(ctx context.Context, channelID string, params models.UploadImagesParams, reqOpts *twitchhttp.ReqOpts) (*models.UploadImagesResponse, error)
	// RemoveSubscriberImage unlinks a channel's subscriber badge of a specified
	// version from its current image
	RemoveSubscriberImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchhttp.ReqOpts) error
	// UpdateUserSubscriberTier updates a user's sub tier/version on a channel
	UpdateUserSubscriberTier(ctx context.Context, userID, channelID string, badgeSetVersion string, reqOpts *twitchhttp.ReqOpts) error
	// BulkUpdateUserSubscriberTier updates many user's sub tiers/versions on a
	// channel. It returns an array of the IDs that it successfully processed
	BulkUpdateUserSubscriberTier(ctx context.Context, channelID string, params models.BulkGrantBadgeParams, reqOpts *twitchhttp.ReqOpts) (*models.BulkRequestResponse, error)
	// DeleteUserSubscriberTier removes a user's sub tier/version on a channel.
	// They will have no sub badge after calling this.
	DeleteUserSubscriberTier(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) error
	// BulkDeleteUserSubscriberTier removes many user's sub tiers/versions on a
	// channel. It returns an array of the IDs that it successfully processed
	BulkDeleteUserSubscriberTier(ctx context.Context, channelID string, params models.BulkRemoveBadgeParams, reqOpts *twitchhttp.ReqOpts) (*models.BulkRequestResponse, error)
}

// NewClient creates an http client to call Badges endpoints.
func NewClient(conf twitchhttp.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchhttp.NewClient(conf)
	if err != nil {
		return nil, err
	}
	return &clientImpl{twitchClient}, nil
}

type clientImpl struct {
	twitchhttp.Client
}

func (c *clientImpl) AvailableGlobalBadges(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.GetAvailableBadgesResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/available", userID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.available_global_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to AvailableGlobalBadges", resp.StatusCode)
	}

	var decoded models.GetAvailableBadgesResponse
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) DeselectGlobalBadge(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/selected", userID)}
	req, err := c.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.deselect_global_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeselectGlobalBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) SelectTurboGlobalBadge(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	params := models.SelectBadgeParams{BadgeSetID: "turbo"}
	return c.SelectGlobalBadge(ctx, userID, params, reqOpts)
}

func (c *clientImpl) SelectGlobalBadge(ctx context.Context, userID string, params models.SelectBadgeParams, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/selected", userID)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return false, err
	}
	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.select_global_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}

	if resp.StatusCode == http.StatusForbidden {
		// User does not have global badge available
		return false, nil
	} else if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("unexpected response code %d during call to SelectGlobalBadge", resp.StatusCode)
	}

	return true, nil
}

func (c *clientImpl) AvailableChannelBadges(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) (*models.GetAvailableBadgesResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/available", userID, channelID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.available_channel_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to AvailableChannelBadges", resp.StatusCode)
	}

	var decoded models.GetAvailableBadgesResponse
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) DeselectChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/selected", userID, channelID)}
	req, err := c.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.deselect_channel_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeselectChannelBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) SelectBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	params := models.SelectBadgeParams{BadgeSetID: "bits"}
	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName: "service.badges.select_bits_badge",
	})
	return c.SelectChannelBadge(ctx, userID, channelID, params, &combinedReqOpts)
}

func (c *clientImpl) SelectChannelBadge(ctx context.Context, userID, channelID string, params models.SelectBadgeParams, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/selected", userID, channelID)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return false, err
	}
	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.select_channel_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}

	if resp.StatusCode == http.StatusForbidden {
		// User does not have the badge available
		return false, nil
	} else if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("unexpected response code %d during call to SelectChannelBadge", resp.StatusCode)
	}

	return true, nil
}

func (c *clientImpl) GetSelectedGlobalBadge(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.Badge, error) {
	params := models.UserBadgesRequestParams{
		GlobalSelected: true,
	}

	resp, err := c.UserBadges(ctx, userID, params, reqOpts)
	if err != nil {
		return nil, err
	}

	return resp.GlobalSelected, nil
}

func (c *clientImpl) GetSelectedChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) (*models.Badge, error) {
	channelIDInt, err := strconv.ParseInt(channelID, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("Invalid channel id: %s", err)
	}

	params := models.UserBadgesRequestParams{
		ChannelSelected: &channelIDInt,
	}

	resp, err := c.UserBadges(ctx, userID, params, reqOpts)
	if err != nil {
		return nil, err
	}

	return resp.ChannelSelected, nil
}

func (c *clientImpl) UserBadges(ctx context.Context, userID string, params models.UserBadgesRequestParams, reqOpts *twitchhttp.ReqOpts) (*models.UserBadgesResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s", userID)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.user_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to UserBadges", resp.StatusCode)
	}

	var decoded models.UserBadgesResponse
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) GetSubscriberImages(ctx context.Context, channelID string, reqOpts *twitchhttp.ReqOpts) (*models.BadgeSet, error) {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/subs", channelID)
	req, err := c.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.get_subscriber_images",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to GetSubscriberImages", resp.StatusCode)
	}

	var decoded models.BadgeSet
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) UploadSubscriberImages(ctx context.Context, channelID string, params models.UploadImagesParams, reqOpts *twitchhttp.ReqOpts) (*models.UploadImagesResponse, error) {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/subs", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.upload_subscriber_images",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to UploadSubscriberImages", resp.StatusCode)
	}

	var decoded models.UploadImagesResponse
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) RemoveSubscriberImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchhttp.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/subs/remove_image", channelID)
	bodyBytes, err := json.Marshal(models.RemoveImageParams{BadgeSetVersion: badgeSetVersion})
	if err != nil {
		return err
	}
	req, err := c.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.remove_subscriber_image",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to RemoveSubscriberImage", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) UpdateUserSubscriberTier(ctx context.Context, userID, channelID string, badgeSetVersion string, reqOpts *twitchhttp.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/subs", userID, channelID)
	bodyBytes, err := json.Marshal(models.GrantBadgeParams{BadgeSetVersion: badgeSetVersion})
	if err != nil {
		return err
	}
	req, err := c.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.update_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to UpdateUserSubscriberTier", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) BulkUpdateUserSubscriberTier(ctx context.Context, channelID string, params models.BulkGrantBadgeParams, reqOpts *twitchhttp.ReqOpts) (*models.BulkRequestResponse, error) {
	url := fmt.Sprintf("/v1/badges/channels/%s/subs/bulk_update", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.bulk_update_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var decoded models.BulkRequestResponse
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) DeleteUserSubscriberTier(ctx context.Context, userID, channelID string, reqOpts *twitchhttp.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/subs", userID, channelID)
	req, err := c.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.delete_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeleteUserSubscriberTier", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) BulkDeleteUserSubscriberTier(ctx context.Context, channelID string, params models.BulkRemoveBadgeParams, reqOpts *twitchhttp.ReqOpts) (*models.BulkRequestResponse, error) {
	url := fmt.Sprintf("/v1/badges/channels/%s/subs/bulk_delete", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.badges.bulk_delete_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var decoded models.BulkRequestResponse
	if err := json.Unmarshal(body, &decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}
