package logx

import (
	"context"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stvp/rollbar"

	"code.justin.tv/chat/golibs/errx"
)

const (
	callerField        = "caller"
	stackField         = "stack"
	originalErrorField = "originalError"
	waitTimeout        = 5 * time.Second

	// How many stack lines to skip in `RecoverAndLog`. 5 lines are skipped.
	// 1 line is the all to `logx.RecoverAndLog`. The other 4 lines are
	// in the go runtime library. Example:
	//
	// 	 File "/go/1.7/libexec/src/runtime/asm_amd64.s" line 479 in runtime.call32
	// 	 File "/go/1.7/libexec/src/runtime/panic.go" line 458 in runtime.gopanic
	//	 File "/go/1.7/libexec/src/runtime/panic.go" line 62 in runtime.panicmem
	//	 File "/go/1.7/libexec/src/runtime/sigpanic_unix.go" line 24 in runtime.sigpanic
	recoverAndLogSkip = 5
)

// github.com/pkg/errors interface
type stackTracer interface {
	StackTrace() errors.StackTrace
}

// github.com/pkg/errors interface
type causer interface {
	Cause() error
}

// Fields abstract logrus.Fields.
type Fields logrus.Fields

// Config allows customizing the default global logger.
type Config struct {
	// OmitFields accepts field names to omit in logs.
	// Defaults to "stack" and "time".
	OmitFields []string

	// RollbarToken is the post_server_item token in the Rollbar project.
	// If empty, Rollbar is not called.
	RollbarToken string

	// RollbarEnv is the environment to label entries in Rollbar.
	// If empty, Rollbar is not called.
	RollbarEnv string

	// RollbarLevels specifies the logrus levels that are sent to Rollbar.
	// Defaults to Panic, Fatal, and Error.
	RollbarLevels []logrus.Level

	// ShouldReportToRollbar is an optional function to filter errors before
	// being sent to rollbar. If the function returns false, the error will not
	// be sent to rollbar.
	ShouldReportToRollbar func(error) bool
}

func init() {
	logrus.SetOutput(os.Stderr)
	logrus.SetFormatter(NewOmitFieldsFormatter(stackField))
	logrus.SetLevel(logrus.DebugLevel)
}

// InitDefaultLogger initializes a logx logger with sensible defaults.
func InitDefaultLogger(conf Config) {
	if len(conf.OmitFields) == 0 {
		conf.OmitFields = []string{stackField}
	}
	if len(conf.RollbarLevels) == 0 {
		conf.RollbarLevels = []logrus.Level{
			logrus.PanicLevel,
			logrus.FatalLevel,
			logrus.ErrorLevel,
		}
	}

	logrus.SetFormatter(NewOmitFieldsFormatter(conf.OmitFields...))

	rh := NewRollbarHook(conf.RollbarToken, conf.RollbarEnv, conf.RollbarLevels, conf.ShouldReportToRollbar)
	if rh != nil {
		logrus.AddHook(rh)
	}
}

// Wait should be called by the main routine (probably main()) in services using
// logx to ensure events are sent to Rollbar before exiting. Wait is guaranteed
// to complete after a fixed amount of time to prevent a Rollbar from blocking.
func Wait() {
	done := make(chan struct{})
	go func() {
		rollbar.Wait()
		close(done)
	}()
	select {
	case <-done:
	case <-time.After(waitTimeout):
	}
}

// Info logs at Info level. Fields are optional.
func Info(ctx context.Context, msg interface{}, fields ...Fields) {
	buildEntryFromContext(ctx, msg, fields).Info(msg)
}

// Warn logs at Warn level. Fields are optional.
func Warn(ctx context.Context, msg interface{}, fields ...Fields) {
	buildEntryFromContext(ctx, msg, fields).Warn(msg)
}

// Error logs at Error level. Fields are optional.
func Error(ctx context.Context, msg interface{}, fields ...Fields) {
	buildEntryFromContext(ctx, msg, fields).Error(msg)
}

// Fatal logs at *Error* level and exit with status code 1. Fields are optional.
func Fatal(ctx context.Context, msg interface{}, fields ...Fields) {
	buildEntryFromContext(ctx, msg, fields).Error(msg)
	// Wait for Rollbar to complete in-flight error reporting before exiting.
	Wait()
	os.Exit(1)
}

// RecoverAndLog logs panics at Error level and does NOT exit.
//
// Usage example:
//
//  func doSomething() {
//    defer logx.RecoverAndLog()
//    result, err := client.doSomething()
//    c := result.Count() // panic occurs because result is nil
//    ...
//  }
func RecoverAndLog() {
	if err := recover(); err != nil {
		Error(context.Background(), errx.NewWithSkip(fmt.Errorf("panic recovered: %v", err), recoverAndLogSkip))
	}
}

func buildEntryFromContext(ctx context.Context, msg interface{}, fields []Fields) *logrus.Entry {
	merged := logrus.Fields{}

	// Store the original error
	if err, ok := msg.(error); ok {
		merged[originalErrorField] = err
	}

	// Extract fields from context.
	if ctx != nil {
		cf, ok := ctx.Value(fieldsKey).(Fields)
		if ok {
			for k, v := range cf {
				merged[k] = v
			}
		}
	}

	// Extract fields from optional passed in fields arg(s).
	for _, f := range fields {
		for k, v := range f {
			merged[k] = v
		}
	}

	switch msg.(type) {
	case errx.Error:
		handleErrxError(merged, msg.(errx.Error))
	case stackTracer:
		handlePkgErrorsError(merged, msg.(error))
	}

	var caller string
	if stack, ok := merged[stackField].(rollbar.Stack); ok && len(stack) > 0 {
		caller = buildCaller(stack[0].Filename, stack[0].Line)
	} else {
		_, filename, line, ok := runtime.Caller(2)
		if ok {
			caller = buildCaller(filename, line)
		}
	}

	merged[callerField] = caller

	return logrus.WithFields(merged)
}

func handleErrxError(merged logrus.Fields, errxErr errx.Error) {
	for k, v := range errxErr.Fields() {
		merged[k] = v
	}
}

// handles github.com/pkg/errors
func handlePkgErrorsError(merged logrus.Fields, err error) {
	var stackTraceErr stackTracer
	// Find the last error with a stack trace
	for err != nil {
		cause, ok := err.(causer)
		if !ok {
			break
		}
		err = cause.Cause()
		if stErr, ok := err.(stackTracer); ok {
			stackTraceErr = stErr
		}
	}

	// Convert the github.com/pkg/errors stack trace into a rollbar stack trace
	frames := []rollbar.Frame{}
	for _, f := range stackTraceErr.StackTrace() {
		// There doesn't seem to be a way to get the file name with path without parsing this string
		// https://godoc.org/github.com/pkg/errors#Frame
		fileData := strings.Split(fmt.Sprintf("%+s", f), "\n\t")
		if len(fileData) != 2 {
			break
		}
		fileName := fileData[1]

		method := fmt.Sprintf("%n", f)
		lineNumStr := fmt.Sprintf("%d", f)
		lineNum, err := strconv.Atoi(lineNumStr)
		if err != nil {
			break
		}

		frame := rollbar.Frame{
			Filename: fileName,
			Method:   method,
			Line:     lineNum,
		}
		frames = append(frames, frame)
	}

	merged[stackField] = rollbar.Stack(frames)
}

func buildCaller(filename string, line int) string {
	return fmt.Sprintf("%s:%d", filename, line)
}
