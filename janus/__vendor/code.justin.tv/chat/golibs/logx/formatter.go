package logx

import "github.com/sirupsen/logrus"

func NewOmitFieldsFormatter(fields ...string) logrus.Formatter {
	return &omitFieldsFormatter{
		TextFormatter: logrus.TextFormatter{
			DisableColors:    true,
			DisableTimestamp: true,
		},
		omit: fields,
	}
}

// omitFieldsFormatter is a Formatter that does not log given fields.
type omitFieldsFormatter struct {
	logrus.TextFormatter
	omit []string
}

func (f *omitFieldsFormatter) Format(e *logrus.Entry) ([]byte, error) {
	for _, field := range f.omit {
		delete(e.Data, field)
	}
	return f.TextFormatter.Format(e)
}
