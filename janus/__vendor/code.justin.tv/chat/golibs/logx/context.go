package logx

import "context"

type contextKey int

const fieldsKey contextKey = iota

// Context returns a copy of a context with logx Fields.
func Context(ctx context.Context) context.Context {
	return context.WithValue(ctx, fieldsKey, Fields{})
}

// CopyFields moves the logx fields from one context to another
// This is useful when wanting to maintain state across two different contexts.
// For example, fields attached to one context are relevant to a second
// context perhaps used for controlling an asynchronous request.
//
// CopyFields will return the destination context, containing logx.Fields from
// the original context if present. This function will be a noop if no fields
// were present on the original context.
func CopyFields(src, dest context.Context) context.Context {
	if ctxFields, ok := src.Value(fieldsKey).(Fields); ok {
		return WithFields(dest, ctxFields)
	}

	return dest
}

// WithFields returns a new context with added logx Fields.
// Callers can add metadata to the context for future log calls, such
// as user IDs, IPs, request metadata, etc.
func WithFields(ctx context.Context, fields Fields) context.Context {
	if len(fields) == 0 {
		return ctx
	}

	// Copy <ctxFields> entries to <fields>. If conflict, don't overwrite.
	if ctxFields, ok := ctx.Value(fieldsKey).(Fields); ok {
		for k, v := range ctxFields {
			if _, exists := fields[k]; !exists {
				fields[k] = v
			}
		}
	}

	return context.WithValue(ctx, fieldsKey, fields)
}
