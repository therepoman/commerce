package logx

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/stvp/rollbar"
)

// NewRollbarHook creates a Rollbar hook for logrus.
// It accepts a "stack" field for the stack trace.
func NewRollbarHook(token string, env string, levels []logrus.Level, shouldReport func(error) bool) logrus.Hook {
	if token == "" || env == "" {
		return nil
	}
	rollbar.Token = token
	rollbar.Environment = env
	return &rollbarHookImpl{levels, shouldReport}
}

type rollbarHookImpl struct {
	levels       []logrus.Level
	shouldReport func(error) bool
}

func (rh *rollbarHookImpl) Fire(e *logrus.Entry) error {
	if origErr, ok := e.Data[originalErrorField]; ok {
		delete(e.Data, originalErrorField)

		if rh.shouldReport != nil {
			if err, ok := origErr.(error); ok && !rh.shouldReport(err) {
				return nil
			}
		}
	}

	level := getRollbarLevel(e.Level)
	err := fmt.Errorf(e.Message)

	var stack rollbar.Stack
	var fields []*rollbar.Field
	for k, v := range e.Data {
		switch k {
		case stackField:
			if s, ok := v.(rollbar.Stack); ok {
				stack = s
			}
		case callerField:
			// ignore
		default:
			fields = append(fields, &rollbar.Field{
				Name: k,
				Data: v,
			})
		}
	}

	if len(stack) > 0 {
		rollbar.ErrorWithStack(level, err, stack, fields...)
	} else {
		rollbar.ErrorWithStackSkip(level, err, 5, fields...)
	}
	return nil
}

func (rh *rollbarHookImpl) Levels() []logrus.Level {
	return rh.levels
}

func getRollbarLevel(level logrus.Level) string {
	switch level {
	case logrus.FatalLevel,
		logrus.PanicLevel:
		return rollbar.CRIT
	case logrus.ErrorLevel:
		return rollbar.ERR
	case logrus.WarnLevel:
		return rollbar.WARN
	case logrus.InfoLevel:
		return rollbar.INFO
	case logrus.DebugLevel:
		return rollbar.DEBUG
	default:
		return rollbar.INFO
	}
}
