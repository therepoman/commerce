package errx

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/stvp/rollbar"
)

const (
	// StackField is the name of the field storing the stack trace.
	StackField = "stack"

	// How many stack lines to skip when calling `errx.New`. 1 line is skipped to
	// remove the stack line with `errx.New`
	newSkip = 1

	// How many stack lines to skip when calling `rollbar.BuildStack`. Two lines
	// are skipped to remove the stack lines with `errx.NewWithSkip` and `rollbar.BuildStack`
	buildStackSkip = 2
)

// New accepts an arbitrary type, appends metadata (including stack trace), and
// returns an Error. Use Wrap(...) to pass in a message prefix instead of calling
// New(fmt.Sprintf(...)).
//
// If val is:					then return:
//		nil							nil
//		a *errx.errorImpl			the same error to avoid another allocation
//		a native error				a wrapped error
//		anything else				a wrapped error with the Go string representation
//
func New(val interface{}, fields ...Fields) Error {
	return NewWithSkip(val, newSkip, fields...)
}

// Wrap prepends a message to the error string to provide context when bubbling
// errors up from multiple layers. Any error type, including native errors and
// errx.Error, can be wrapped.
//
// Example:
//		err := errx.New("failed to parse JSON")
//		err = errx.Wrap(err, "publishing to SNS")
//		log.Println(err)	// outputs "publishing to SNS: failed to parse JSON"
//
func Wrap(err error, message string, fields ...Fields) Error {
	if err == nil {
		return nil
	}

	e := NewWithSkip(err, newSkip, fields...)
	wrappedErr := e.(*errorImpl)
	wrappedErr.wrapMessages = append([]string{message}, wrappedErr.wrapMessages...)
	return wrappedErr
}

// Unwrap returns the inner error of an errx-wrapped error, or the original
// error if not wrapped. This is useful for comparing errors with sentinel
// errors or converting to another struct or interface.
//
// Example:
//		err := doSomething() // err is potentially errx-wrapped
//		if err != nil {
//			if awsErr, ok := errx.Unwrap(err).(awserr.Error); ok {
//				// ...
//			}
//		}
//
func Unwrap(err error) error {
	if e, ok := err.(*errorImpl); ok {
		return e.error
	}
	return err
}

// NewWithSkip enables the caller to skip stack frames.
// `NewWithSkip(err, 0)` is intended to be equivalent to `New(err)`
func NewWithSkip(val interface{}, skip int, fields ...Fields) Error {
	var err *errorImpl
	switch e := val.(type) {
	case nil:
		return nil
	case *errorImpl:
		err = e
	case error:
		err = &errorImpl{error: e}
	default:
		err = &errorImpl{error: fmt.Errorf("%v", e)}
	}

	if err.fields == nil {
		err.fields = make(map[string]interface{})
	}

	// Includes rollbar-compatible stack trace if one doesn't already exist.
	if _, ok := err.fields[StackField]; !ok {
		err.fields[StackField] = rollbar.BuildStack(buildStackSkip + skip)
	}

	for _, f := range fields {
		for k, v := range f {
			err.fields[k] = v
		}
	}

	return err
}

type errorImpl struct {
	error
	wrapMessages []string
	fields       Fields
}

func (e *errorImpl) Error() string {
	tokens := append(e.wrapMessages, e.error.Error())
	return strings.Join(tokens, ": ")
}

func (e *errorImpl) String() string { return e.Error() }
func (e *errorImpl) Fields() Fields { return e.fields }

// Format defines the output for various print formats:
//		"%v" and "%s" prints only the error message
// 		"%q" prints an escaped error message
// 		"+v" prints the error message with the call stack and fielkds pretty-printed
func (e *errorImpl) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			var fieldsString string
			if fieldsJSON, err := json.MarshalIndent(e.Fields(), "", "  "); err == nil {
				fieldsString = string(fieldsJSON)
			}
			fmt.Fprintf(s, "%+s\n%+s", e.Error(), fieldsString)
			return
		}
		io.WriteString(s, e.Error())
	case 's':
		io.WriteString(s, e.Error())
	case 'q':
		fmt.Fprintf(s, "%q", e.Error())
	}
}
