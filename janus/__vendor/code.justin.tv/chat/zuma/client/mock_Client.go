package zuma

import "github.com/stretchr/testify/mock"

import "golang.org/x/net/context"
import "code.justin.tv/chat/zuma/app/api"
import "code.justin.tv/common/twitchhttp"

// @generated

type MockClient struct {
	mock.Mock
}

// GetMod provides a mock function with given fields: ctx, channelID, userID, reqOpts
func (_m *MockClient) GetMod(ctx context.Context, channelID string, userID string, reqOpts *twitchhttp.ReqOpts) (api.GetModResponse, error) {
	ret := _m.Called(ctx, channelID, userID, reqOpts)

	var r0 api.GetModResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, string, *twitchhttp.ReqOpts) api.GetModResponse); ok {
		r0 = rf(ctx, channelID, userID, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetModResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, userID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListMods provides a mock function with given fields: ctx, channelID, reqOpts
func (_m *MockClient) ListMods(ctx context.Context, channelID string, reqOpts *twitchhttp.ReqOpts) (api.ListModsResponse, error) {
	ret := _m.Called(ctx, channelID, reqOpts)

	var r0 api.ListModsResponse
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) api.ListModsResponse); ok {
		r0 = rf(ctx, channelID, reqOpts)
	} else {
		r0 = ret.Get(0).(api.ListModsResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AddMod provides a mock function with given fields: ctx, channelID, targetUserID, requestingUserID, reqOpts
func (_m *MockClient) AddMod(ctx context.Context, channelID string, targetUserID string, requestingUserID string, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	ret := _m.Called(ctx, channelID, targetUserID, requestingUserID, reqOpts)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, *twitchhttp.ReqOpts) bool); ok {
		r0 = rf(ctx, channelID, targetUserID, requestingUserID, reqOpts)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, targetUserID, requestingUserID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RemoveMod provides a mock function with given fields: ctx, channelID, targetUserID, requestingUserID, reqOpts
func (_m *MockClient) RemoveMod(ctx context.Context, channelID string, targetUserID string, requestingUserID string, reqOpts *twitchhttp.ReqOpts) (bool, error) {
	ret := _m.Called(ctx, channelID, targetUserID, requestingUserID, reqOpts)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, *twitchhttp.ReqOpts) bool); ok {
		r0 = rf(ctx, channelID, targetUserID, requestingUserID, reqOpts)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, channelID, targetUserID, requestingUserID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) CreateCommunity(ctx context.Context, params api.CreateCommunityRequest, reqOpts *twitchhttp.ReqOpts) (api.CreateCommunityResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.CreateCommunityResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.CreateCommunityRequest, *twitchhttp.ReqOpts) api.CreateCommunityResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.CreateCommunityResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.CreateCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCommunityIDByName provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetCommunityIDByName(ctx context.Context, params api.GetCommunityByNameRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityByNameResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunityByNameResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunityByNameRequest, *twitchhttp.ReqOpts) api.GetCommunityByNameResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunityByNameResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunityByNameRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCommunitySettings provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunitySettingsResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunitySettingsResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunitySettingsRequest, *twitchhttp.ReqOpts) api.GetCommunitySettingsResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunitySettingsResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunitySettingsRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// InternalGetCommunitySettings provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) InternalGetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunitySettingsResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunitySettingsResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunitySettingsRequest, *twitchhttp.ReqOpts) api.GetCommunitySettingsResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunitySettingsResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunitySettingsRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetCommunitySettings provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) SetCommunitySettings(ctx context.Context, params api.SetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.SetCommunitySettingsRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// TopCommunities provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) TopCommunities(ctx context.Context, params api.TopCommunitiesRequest, reqOpts *twitchhttp.ReqOpts) (api.TopCommunitiesResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.TopCommunitiesResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.TopCommunitiesRequest, *twitchhttp.ReqOpts) api.TopCommunitiesResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.TopCommunitiesResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.TopCommunitiesRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ReportCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ReportCommunity(ctx context.Context, params api.ReportCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.ReportCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// TOSBanCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) TOSBanCommunity(ctx context.Context, params api.TOSBanCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.TOSBanCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadCommunityImage provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) UploadCommunityImage(ctx context.Context, params api.UploadCommunityImageRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.UploadCommunityImageRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RemoveCommunityImage provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) RemoveCommunityImage(ctx context.Context, params api.RemoveCommunityImageRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.RemoveCommunityImageRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCommunityMod provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetCommunityMod(ctx context.Context, params api.GetCommunityModRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityModResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunityModResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunityModRequest, *twitchhttp.ReqOpts) api.GetCommunityModResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunityModResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunityModRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListCommunityMods provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ListCommunityMods(ctx context.Context, params api.ListCommunityModsRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityModsResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.ListCommunityModsResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.ListCommunityModsRequest, *twitchhttp.ReqOpts) api.ListCommunityModsResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.ListCommunityModsResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.ListCommunityModsRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AddCommunityMod provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) AddCommunityMod(ctx context.Context, params api.AddCommunityModRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.AddCommunityModRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RemoveCommunityMod provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) RemoveCommunityMod(ctx context.Context, params api.RemoveCommunityModRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.RemoveCommunityModRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// AddCommunityBan provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) AddCommunityBan(ctx context.Context, params api.AddCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.AddCommunityBanRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCommunityBan provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetCommunityBan(ctx context.Context, params api.GetCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityBanResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunityBanResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunityBanRequest, *twitchhttp.ReqOpts) api.GetCommunityBanResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunityBanResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunityBanRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListCommunityBans provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ListCommunityBans(ctx context.Context, params api.ListCommunityBansRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityBansResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.ListCommunityBansResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.ListCommunityBansRequest, *twitchhttp.ReqOpts) api.ListCommunityBansResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.ListCommunityBansResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.ListCommunityBansRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RemoveCommunityBan provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) RemoveCommunityBan(ctx context.Context, params api.RemoveCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.RemoveCommunityBanRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// AddCommunityTimeout provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) AddCommunityTimeout(ctx context.Context, params api.AddCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.AddCommunityTimeoutRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCommunityTimeout provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetCommunityTimeout(ctx context.Context, params api.GetCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityTimeoutResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunityTimeoutResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunityTimeoutRequest, *twitchhttp.ReqOpts) api.GetCommunityTimeoutResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunityTimeoutResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunityTimeoutRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListCommunityTimeouts provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ListCommunityTimeouts(ctx context.Context, params api.ListCommunityTimeoutsRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityTimeoutsResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.ListCommunityTimeoutsResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.ListCommunityTimeoutsRequest, *twitchhttp.ReqOpts) api.ListCommunityTimeoutsResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.ListCommunityTimeoutsResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.ListCommunityTimeoutsRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RemoveCommunityTimeout provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) RemoveCommunityTimeout(ctx context.Context, params api.RemoveCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.RemoveCommunityTimeoutRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCommunityPermissions provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityPermissionsResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetCommunityPermissionsResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetCommunityPermissionsRequest, *twitchhttp.ReqOpts) api.GetCommunityPermissionsResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetCommunityPermissionsResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetCommunityPermissionsRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetChannelCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) GetChannelCommunity(ctx context.Context, params api.GetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) (api.GetChannelCommunityResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.GetChannelCommunityResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.GetChannelCommunityRequest, *twitchhttp.ReqOpts) api.GetChannelCommunityResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.GetChannelCommunityResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.GetChannelCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetChannelCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) SetChannelCommunity(ctx context.Context, params api.SetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.SetChannelCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UnsetChannelCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) UnsetChannelCommunity(ctx context.Context, params api.UnsetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.UnsetChannelCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ReportChannelCommunity provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ReportChannelCommunity(ctx context.Context, params api.ReportChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.ReportChannelCommunityRequest, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ListUserBlocks provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchhttp.ReqOpts) (api.ListUserBlocksResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.ListUserBlocksResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.ListUserBlocksParams, *twitchhttp.ReqOpts) api.ListUserBlocksResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.ListUserBlocksResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.ListUserBlocksParams, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AddUserBlock provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchhttp.ReqOpts) (api.AddUserBlockResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.AddUserBlockResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.AddUserBlockParams, *twitchhttp.ReqOpts) api.AddUserBlockResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.AddUserBlockResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.AddUserBlockParams, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RemoveUserBlock provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, api.RemoveUserBlockParams, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ParseMessage provides a mock function with given fields: ctx, params, reqOpts
func (_m *MockClient) ParseMessage(ctx context.Context, params api.ParseMessageRequest, reqOpts *twitchhttp.ReqOpts) (api.ParseMessageResponse, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 api.ParseMessageResponse
	if rf, ok := ret.Get(0).(func(context.Context, api.ParseMessageRequest, *twitchhttp.ReqOpts) api.ParseMessageResponse); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		r0 = ret.Get(0).(api.ParseMessageResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, api.ParseMessageRequest, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
