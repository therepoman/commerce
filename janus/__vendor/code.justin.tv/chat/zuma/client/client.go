package zuma

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"golang.org/x/net/context"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/common/twitchhttp"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "zuma"
)

// Client is the interface for the Zuma go client
type Client interface {
	GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchhttp.ReqOpts) (api.GetModResponse, error)
	ListMods(ctx context.Context, channelID string, reqOpts *twitchhttp.ReqOpts) (api.ListModsResponse, error)
	AddMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchhttp.ReqOpts) (success bool, err error)
	RemoveMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchhttp.ReqOpts) (success bool, err error)

	CreateCommunity(ctx context.Context, params api.CreateCommunityRequest, reqOpts *twitchhttp.ReqOpts) (api.CreateCommunityResponse, error)
	GetCommunityIDByName(ctx context.Context, params api.GetCommunityByNameRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityByNameResponse, error)
	GetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunitySettingsResponse, error)
	InternalGetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunitySettingsResponse, error)
	SetCommunitySettings(ctx context.Context, params api.SetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) error

	TopCommunities(ctx context.Context, params api.TopCommunitiesRequest, reqOpts *twitchhttp.ReqOpts) (api.TopCommunitiesResponse, error)

	ReportCommunity(ctx context.Context, params api.ReportCommunityRequest, reqOpts *twitchhttp.ReqOpts) error
	TOSBanCommunity(ctx context.Context, params api.TOSBanCommunityRequest, reqOpts *twitchhttp.ReqOpts) error

	UploadCommunityImage(ctx context.Context, params api.UploadCommunityImageRequest, reqOpts *twitchhttp.ReqOpts) error
	RemoveCommunityImage(ctx context.Context, params api.RemoveCommunityImageRequest, reqOpts *twitchhttp.ReqOpts) error

	GetCommunityMod(ctx context.Context, params api.GetCommunityModRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityModResponse, error)
	ListCommunityMods(ctx context.Context, params api.ListCommunityModsRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityModsResponse, error)
	AddCommunityMod(ctx context.Context, params api.AddCommunityModRequest, reqOpts *twitchhttp.ReqOpts) error
	RemoveCommunityMod(ctx context.Context, params api.RemoveCommunityModRequest, reqOpts *twitchhttp.ReqOpts) error

	AddCommunityBan(ctx context.Context, params api.AddCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) error
	GetCommunityBan(ctx context.Context, params api.GetCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityBanResponse, error)
	ListCommunityBans(ctx context.Context, params api.ListCommunityBansRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityBansResponse, error)
	RemoveCommunityBan(ctx context.Context, params api.RemoveCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) error

	AddCommunityTimeout(ctx context.Context, params api.AddCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) error
	GetCommunityTimeout(ctx context.Context, params api.GetCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityTimeoutResponse, error)
	ListCommunityTimeouts(ctx context.Context, params api.ListCommunityTimeoutsRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityTimeoutsResponse, error)
	RemoveCommunityTimeout(ctx context.Context, params api.RemoveCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) error

	GetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityPermissionsResponse, error)

	GetChannelCommunity(ctx context.Context, params api.GetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) (api.GetChannelCommunityResponse, error)
	SetChannelCommunity(ctx context.Context, params api.SetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error
	UnsetChannelCommunity(ctx context.Context, params api.UnsetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error
	ReportChannelCommunity(ctx context.Context, params api.ReportChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error

	ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchhttp.ReqOpts) (api.ListUserBlocksResponse, error)
	AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchhttp.ReqOpts) (api.AddUserBlockResponse, error)
	RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchhttp.ReqOpts) error

	ParseMessage(ctx context.Context, params api.ParseMessageRequest, reqOpts *twitchhttp.ReqOpts) (api.ParseMessageResponse, error)
}

type client struct {
	twitchhttp.Client
}

// NewClient creates a new Zuma go client
func NewClient(conf twitchhttp.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchhttp.NewClient(conf)
	return &client{twitchClient}, err
}

func (c *client) GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchhttp.ReqOpts) (api.GetModResponse, error) {
	bodyBytes, err := json.Marshal(api.GetModRequest{
		ChannelID: channelID,
		UserID:    userID,
	})
	if err != nil {
		return api.GetModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetModResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.mods.get",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetModResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetModResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetModResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetModResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListMods(ctx context.Context, channelID string, reqOpts *twitchhttp.ReqOpts) (api.ListModsResponse, error) {
	bodyBytes, err := json.Marshal(api.ListModsRequest{
		ChannelID: channelID,
	})
	if err != nil {
		return api.ListModsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListModsResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.mods.list",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListModsResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.ListModsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListModsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListModsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchhttp.ReqOpts) (success bool, err error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return false, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.mods.add",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return false, c.errorFromFailedRequest(resp)
	}

	return true, nil
}

func (c *client) RemoveMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchhttp.ReqOpts) (success bool, err error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return false, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.mods.remove",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return false, c.errorFromFailedRequest(resp)
	}

	return true, nil
}

func (c *client) CreateCommunity(ctx context.Context, params api.CreateCommunityRequest, reqOpts *twitchhttp.ReqOpts) (api.CreateCommunityResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CreateCommunityResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/create", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CreateCommunityResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.create",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.CreateCommunityResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.CreateCommunityResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.CreateCommunityResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.CreateCommunityResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) GetCommunityIDByName(ctx context.Context, params api.GetCommunityByNameRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityByNameResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityByNameResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/get_by_name", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityByNameResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.get_by_name",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityByNameResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunityByNameResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityByNameResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityByNameResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ReportCommunity(ctx context.Context, params api.ReportCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/report", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.report",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) TOSBanCommunity(ctx context.Context, params api.TOSBanCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/tos_ban", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.tos_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunitySettingsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.get_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunitySettingsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunitySettingsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) InternalGetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunitySettingsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/internal_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.internal_get_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunitySettingsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunitySettingsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) SetCommunitySettings(ctx context.Context, params api.SetCommunitySettingsRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/set", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.set_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) TopCommunities(ctx context.Context, params api.TopCommunitiesRequest, reqOpts *twitchhttp.ReqOpts) (api.TopCommunitiesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.TopCommunitiesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/top", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.TopCommunitiesResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.top",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.TopCommunitiesResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.TopCommunitiesResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.TopCommunitiesResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.TopCommunitiesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) UploadCommunityImage(ctx context.Context, params api.UploadCommunityImageRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/images/upload", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.upload_image",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityImage(ctx context.Context, params api.RemoveCommunityImageRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/images/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.remove_image",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityMod(ctx context.Context, params api.GetCommunityModRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityModResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityModResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.get_mod",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityModResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunityModResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityModResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityModResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListCommunityMods(ctx context.Context, params api.ListCommunityModsRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityModsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityModsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityModsResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.list_mods",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityModsResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.ListCommunityModsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListCommunityModsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListCommunityModsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddCommunityMod(ctx context.Context, params api.AddCommunityModRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.add_mod",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityMod(ctx context.Context, params api.RemoveCommunityModRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.remove_mod",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityBan(ctx context.Context, params api.GetCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityBanResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityBanResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityBanResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.get_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityBanResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunityBanResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityBanResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityBanResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListCommunityBans(ctx context.Context, params api.ListCommunityBansRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityBansResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityBansResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityBansResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.list_bans",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityBansResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.ListCommunityBansResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListCommunityBansResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListCommunityBansResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddCommunityBan(ctx context.Context, params api.AddCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.add_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityBan(ctx context.Context, params api.RemoveCommunityBanRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.remove_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityTimeout(ctx context.Context, params api.GetCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityTimeoutResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.get_timeout",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunityTimeoutResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityTimeoutResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListCommunityTimeouts(ctx context.Context, params api.ListCommunityTimeoutsRequest, reqOpts *twitchhttp.ReqOpts) (api.ListCommunityTimeoutsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.list_timeouts",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.ListCommunityTimeoutsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListCommunityTimeoutsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddCommunityTimeout(ctx context.Context, params api.AddCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.add_timeout",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityTimeout(ctx context.Context, params api.RemoveCommunityTimeoutRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.remove_timeout",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchhttp.ReqOpts) (api.GetCommunityPermissionsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/permissions", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.communities.get_permissions",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetCommunityPermissionsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityPermissionsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) GetChannelCommunity(ctx context.Context, params api.GetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) (api.GetChannelCommunityResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetChannelCommunityResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetChannelCommunityResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.channels.get_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetChannelCommunityResponse{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return api.GetChannelCommunityResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetChannelCommunityResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetChannelCommunityResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) SetChannelCommunity(ctx context.Context, params api.SetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/set", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.channels.set_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) UnsetChannelCommunity(ctx context.Context, params api.UnsetChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/unset", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.channels.unset_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) ReportChannelCommunity(ctx context.Context, params api.ReportChannelCommunityRequest, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/report", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.channels.report_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchhttp.ReqOpts) (api.ListUserBlocksResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListUserBlocksResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListUserBlocksResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.users.get_blocks",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListUserBlocksResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListUserBlocksResponse{}, err
	}
	return decoded, nil
}

func (c *client) AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchhttp.ReqOpts) (api.AddUserBlockResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.users.add_block",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}
	defer resp.Body.Close()

	// 422 is a valid, expected response and should be handled
	if resp.StatusCode >= 400 && resp.StatusCode != http.StatusUnprocessableEntity {
		return api.AddUserBlockResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.AddUserBlockResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	return data, nil
}

func (c *client) RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchhttp.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.users.remove_block",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) ParseMessage(ctx context.Context, params api.ParseMessageRequest, reqOpts *twitchhttp.ReqOpts) (api.ParseMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ParseMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/messages/parse", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ParseMessageResponse{}, err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(reqOpts, twitchhttp.ReqOpts{
		StatName:       "service.zuma.messages.parse",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ParseMessageResponse{}, err
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.ParseMessageResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.ParseMessageResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.ParseMessageResponse{}, c.errorFromFailedRequest(resp)
	}
}

// handleFailedRequest parses the detailed ErrorResponse from Zuma, if one is
// provided
func (c *client) errorFromFailedRequest(resp *http.Response) error {
	var errResp api.ErrorResponse
	err := json.NewDecoder(resp.Body).Decode(&errResp)
	if err != nil {
		// could not decode the Zuma response
		return &api.ErrorResponse{
			Err:    fmt.Sprintf("failed zuma request. StatusCode=%v Unable to read response body (%v)", resp.StatusCode, err),
			Status: resp.StatusCode,
		}
	}
	return &errResp
}
