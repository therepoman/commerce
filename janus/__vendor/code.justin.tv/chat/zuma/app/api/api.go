package api

import "time"

// GetModRequest is the body for the POST /v1/mods/get endpoint
type GetModRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// GetModResponse is the response from the POST /v1/mods/get endpoint
type GetModResponse struct {
	IsMod bool `json:"is_mod"`
}

// ListModsRequest is the body for the POST /v1/mods/list endpoint
type ListModsRequest struct {
	ChannelID string `json:"channel_id"`
}

// ListModsResponse is the response from the POST /v1/mods/list endpoint
type ListModsResponse struct {
	Mods []string `json:"mods"`
}

// AddModRequest is the body for the POST /v1/mods/add endpoint
type AddModRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveModRequest is the body for the POST /v1/mods/remove endpoint
type RemoveModRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// TopCommunitiesRequest is the body for the POST /v1/communities/top endpoint
type TopCommunitiesRequest struct {
	Limit        int    `json:"limit"`
	Cursor       string `json:"cursor"`
	FeaturedOnly bool   `json:"featured_only"`
	Language     string `json:"language"`
}

// TopCommunitiesResponse is the response from the POST /v1/communities/top endpoint
type TopCommunitiesResponse struct {
	Total   int                  `json:"total"`
	Cursor  string               `json:"cursor"`
	Results []TopCommunityResult `json:"results"`
}

// TopCommunityResult is the inner struct of the POST /v1/communities/top endpoint
type TopCommunityResult struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	Viewers        int    `json:"viewers"`
	Channels       int    `json:"channels"`
	AvatarImageURL string `json:"avatar_image_url"`
}

// ListFeaturedCommunitiesRequest is the body for the POST /v1/communities/featured/list endpoint
type ListFeaturedCommunitiesRequest struct{}

// ListFeaturedCommunitiesResponse is the response from the POST /v1/communities/featured/list endpoint
type ListFeaturedCommunitiesResponse struct {
	FeaturedCommunities []Community `json:"featured_communities"`
}

// Community is an inner struct representing a community
type Community struct {
	CommunityID      string `json:"community_id"`
	Name             string `json:"name"`
	OwnerUserID      string `json:"owner_user_id"`
	ShortDescription string `json:"short_description"`
	LongDescription  string `json:"long_description"`
	Rules            string `json:"rules"`
	Email            string `json:"email"`
	Language         string `json:"language"`
	BannerImageURL   string `json:"banner_image_url"`
	AvatarImageURL   string `json:"avatar_image_url"`
}

// AddFeaturedCommunityRequest is the body for the POST /v1/communities/featured/add endpoint
type AddFeaturedCommunityRequest struct {
	CommunityID string `json:"community_id"`
}

// RemoveFeaturedCommunityRequest is the body for the POST /v1/communities/featured/remove endpoint
type RemoveFeaturedCommunityRequest struct {
	CommunityID string `json:"community_id"`
}

// GetCommunityByNameRequest is the body for the POST /v1/communities/get_by_name endpoint
type GetCommunityByNameRequest struct {
	Name string `json:"name"`
}

// GetCommunityByNameResponse is the response for the POST /v1/communities/get_by_name endpoint
type GetCommunityByNameResponse struct {
	CommunityID string `json:"community_id"`
}

// GetCommunitySettingsRequest is the body for the POST /v1/communities/settings/get endpoint
type GetCommunitySettingsRequest struct {
	CommunityID string `json:"community_id"`
}

// GetCommunitySettingsResponse is the response from the POST /v1/communities/settings/get endpoint
type GetCommunitySettingsResponse struct {
	CommunityID         string `json:"community_id"`
	Name                string `json:"name"`
	OwnerUserID         string `json:"owner_user_id"`
	ShortDescription    string `json:"short_description"`
	LongDescription     string `json:"long_description"`
	LongDescriptionHTML string `json:"long_description_html"`
	Rules               string `json:"rules"`
	RulesHTML           string `json:"rules_html"`
	Email               string `json:"email"`
	Language            string `json:"language"`
	BannerImageURL      string `json:"banner_image_url"`
	AvatarImageURL      string `json:"avatar_image_url"`
}

// SetCommunitySettingsRequest is the body for the POST /v1/communities/settings/set endpoint
type SetCommunitySettingsRequest struct {
	CommunityID      string  `json:"community_id"`
	ShortDescription *string `json:"short_description"`
	LongDescription  *string `json:"long_description"`
	Rules            *string `json:"rules"`
	Email            *string `json:"email"`
	Language         *string `json:"language"`
	OwnerUserID      *string `json:"owner_user_id"`
}

// CreateCommunityRequest is the body for the POST /v1/communities/create endpoint
type CreateCommunityRequest struct {
	UserID           string  `json:"user_id"`
	Name             string  `json:"name"`
	ShortDescription string  `json:"short_description"`
	LongDescription  string  `json:"long_description"`
	Rules            string  `json:"rules"`
	Language         *string `json:"language"`
}

// CreateCommunityResponse is the response from the POST /v1/communities/create endpoint
type CreateCommunityResponse struct {
	CommunityID string `json:"community_id"`
}

// UploadCommunityImageRequest is the body for the POST /v1/communities/images/upload endpoint
type UploadCommunityImageRequest struct {
	CommunityID      string `json:"community_id"`
	Type             string `json:"type"`
	ImageBytesBase64 string `json:"image_bytes_base_64"`
}

// ImageTypeAvatar is the type field for an avatar image
const ImageTypeAvatar = "avatar"

// ImageTypeBanner is the type field for a banner image
const ImageTypeBanner = "banner"

// RemoveCommunityImageRequest is the body for the POST /v1/communities/images/remove endpoint
type RemoveCommunityImageRequest struct {
	CommunityID string `json:"community_id"`
	Type        string `json:"type"`
}

// GetCommunityModRequest is the body for the POST /v1/communities/mods/get endpoint
type GetCommunityModRequest struct {
	CommunityID string `json:"community_id"`
	UserID      string `json:"user_id"`
}

// GetCommunityModResponse is the response for the POST /v1/communities/mods/get endpoint
type GetCommunityModResponse struct {
	IsMod bool `json:"is_mod"`
}

// ListCommunityModsRequest is the body for the POST /v1/communities/mods/list endpoint
type ListCommunityModsRequest struct {
	CommunityID string `json:"community_id"`
}

// ListCommunityModsResponse is the response for the POST /v1/communities/mods/list endpoint
type ListCommunityModsResponse struct {
	Mods []string `json:"mods"`
}

// AddCommunityModRequest is the body for the POST /v1/communities/mods/add endpoint
type AddCommunityModRequest struct {
	CommunityID  string `json:"community_id"`
	TargetUserID string `json:"target_user_id"`
}

// RemoveCommunityModRequest is the body for the POST /v1/communities/mods/remove endpoint
type RemoveCommunityModRequest struct {
	CommunityID  string `json:"community_id"`
	TargetUserID string `json:"target_user_id"`
}

// GetCommunityBanRequest is the body for the POST /v1/communities/bans/get endpoint
type GetCommunityBanRequest struct {
	CommunityID string `json:"community_id"`
	UserID      string `json:"user_id"`
}

// GetCommunityBanResponse is the response for the POST /v1/communities/bans/get endpoint
type GetCommunityBanResponse struct {
	IsBanned  bool      `json:"is_banned"`
	Start     time.Time `json:"start"`
	ModUserID string    `json:"mod_user_id"`
	Reason    string    `json:"reason"`
}

// ListCommunityBansRequest is the body for the POST /v1/communities/bans/list endpoint
type ListCommunityBansRequest struct {
	CommunityID string `json:"community_id"`
	Cursor      string `json:"cursor"`
	Limit       int    `json:"limit"`
}

// ListCommunityBansResponse is the response for the POST /v1/communities/bans/list endpoint
// Use the cursor to request the next set of bans
type ListCommunityBansResponse struct {
	Cursor string         `json:"cursor"`
	Bans   []CommunityBan `json:"bans"`
}

// CommunityBan is the inner struct for the ListCommunityBansResponse
type CommunityBan struct {
	UserID    string    `json:"user_id"`
	Start     time.Time `json:"start"`
	ModUserID string    `json:"mod_user_id"`
	Reason    string    `json:"reason"`
}

// AddCommunityBanRequest is the body for the POST /v1/communities/bans/add endpoint
type AddCommunityBanRequest struct {
	CommunityID  string `json:"community_id"`
	TargetUserID string `json:"target_user_id"`
	Reason       string `json:"reason"`
}

// RemoveCommunityBanRequest is the body for the POST /v1/communities/bans/remove endpoint
type RemoveCommunityBanRequest struct {
	CommunityID  string `json:"community_id"`
	TargetUserID string `json:"target_user_id"`
}

// GetCommunityTimeoutRequest is the body for the POST /v1/communities/timeouts/get endpoint
type GetCommunityTimeoutRequest struct {
	CommunityID string `json:"community_id"`
	UserID      string `json:"user_id"`
}

// GetCommunityTimeoutResponse is the response for the POST /v1/communities/Timeouts/get endpoint
type GetCommunityTimeoutResponse struct {
	IsTimedOut bool      `json:"is_timed_out"`
	Start      time.Time `json:"start"`
	Expiration time.Time `json:"expiration"`
	ModUserID  string    `json:"mod_user_id"`
	Reason     string    `json:"reason"`
}

// ListCommunityTimeoutsRequest is the body for the POST /v1/communities/timeouts/list endpoint
type ListCommunityTimeoutsRequest struct {
	CommunityID string `json:"community_id"`
	Cursor      string `json:"cursor"`
	Limit       int    `json:"limit"`
}

// ListCommunityTimeoutsResponse is the response for the POST /v1/communities/Timeouts/list endpoint
// Use the cursor to request the next set of Timeouts
type ListCommunityTimeoutsResponse struct {
	Cursor   string             `json:"cursor"`
	Timeouts []CommunityTimeout `json:"timeouts"`
}

// CommunityTimeout is the inner struct for the ListCommunityTimeoutsResponse
type CommunityTimeout struct {
	UserID     string    `json:"user_id"`
	Start      time.Time `json:"start"`
	Expiration time.Time `json:"expiration"`
	ModUserID  string    `json:"mod_user_id"`
	Reason     string    `json:"reason"`
}

// AddCommunityTimeoutRequest is the body for the POST /v1/communities/timeouts/add endpoint
type AddCommunityTimeoutRequest struct {
	CommunityID  string `json:"community_id"`
	TargetUserID string `json:"target_user_id"`
	Duration     int64  `json:"duration_seconds"`
	Reason       string `json:"reason"`
}

// RemoveCommunityTimeoutRequest is the body for the POST /v1/communities/timeouts/remove endpoint
type RemoveCommunityTimeoutRequest struct {
	CommunityID  string `json:"community_id"`
	TargetUserID string `json:"target_user_id"`
}

// GetCommunityPermissionsRequest is the body for the POST /v1/communities/permissions endpoint
type GetCommunityPermissionsRequest struct {
	CommunityID string `json:"community_id"`
	UserID      string `json:"user_id"`
}

// GetCommunityPermissionsResponse is the response for the POST /v1/communities/permissions endpoint
type GetCommunityPermissionsResponse struct {
	Ban     bool `json:"ban"`
	Timeout bool `json:"timeout"`
	Edit    bool `json:"edit"`
}

// SetChannelCommunityRequest is the body for the POST /v1/channels/communities/set endpoint
type SetChannelCommunityRequest struct {
	ChannelID   string `json:"channel_id"`
	CommunityID string `json:"community_id"`
}

// UnsetChannelCommunityRequest is the body for the POST /v1/channels/communities/unset endpoint
type UnsetChannelCommunityRequest struct {
	ChannelID string `json:"channel_id"`
}

// GetChannelCommunityRequest is the body for the POST /v1/channels/communities/get endpoint
type GetChannelCommunityRequest struct {
	ChannelID string `json:"channel_id"`
}

// GetChannelCommunityResponse is the response for the POST /v1/channels/communities/get endpoint
type GetChannelCommunityResponse struct {
	CommunityID string `json:"community_id"`
}

// GetRecentChannelCommunitiesRequest is the body for the POST /v1/channels/communities/recent endpoint
type GetRecentChannelCommunitiesRequest struct {
	ChannelID string `json:"channel_id"`
}

// GetRecentChannelCommunitiesResponse is the response for the POST /v1/channels/communities/recent endpoint
type GetRecentChannelCommunitiesResponse struct {
	Communities []string `json:"communities"`
}

// BulkGetChannelCommunitiesRequest is the body for the POST /v1/channels/communities/bulk endpoint
type BulkGetChannelCommunitiesRequest struct {
	ChannelLogins []string `json:"channel_logins"`
}

// BulkGetChannelCommunitiesResponse is the response for the POST /v1/channels/communities/bulk endpoint
type BulkGetChannelCommunitiesResponse struct {
	Results []BulkGetChannelCommunitiesResult `json:"results"`
}

// BulkGetChannelCommunitiesResult is the inner JSON for the bulk get endpoint
type BulkGetChannelCommunitiesResult struct {
	ChannelID    string `json:"channel_id"`
	ChannelLogin string `json:"channel_login"`
	CommunityID  string `json:"community_id"`
}

// ReportChannelCommunityRequest is the body for the POST /v1/channels/communities/report endpoint
type ReportChannelCommunityRequest struct {
	UserID      string `json:"user_id"`
	ChannelID   string `json:"channel_id"`
	Description string `json:"description"`
}

// ReportCommunityRequest is the body for the POST /v1/communities/report endpoint
type ReportCommunityRequest struct {
	UserID      string `json:"user_id"`
	CommunityID string `json:"community_id"`
	Reason      string `json:"reason"`
	Description string `json:"description"`
}

// Valid reasons for a community report
const (
	ReasonBitsViolation         = "bits_violation"
	ReasonCheating              = "cheating"
	ReasonGore                  = "gore"
	ReasonHarassment            = "harassment"
	ReasonHarm                  = "harm"
	ReasonHateSpeech            = "hate_speech"
	ReasonImpersonation         = "impersonation"
	ReasonMusicConduct          = "music_conduct"
	ReasonNongaming             = "nongaming"
	ReasonOffensiveUsername     = "offensive_username"
	ReasonOther                 = "other"
	ReasonPorn                  = "porn"
	ReasonProhibited            = "prohibited"
	ReasonSelfharm              = "selfharm"
	ReasonSocialEatingViolation = "social_eating_violation"
	ReasonSpam                  = "spam"
	ReasonTosBanEvasion         = "tos_ban_evasion"
	ReasonUnderaged             = "underaged"
	ReasonCommunityTOSViolation = "community_tos_violation"
)

// TOSBanCommunityRequest is the body for the POST /v1/communities/tos_ban endpoint
type TOSBanCommunityRequest struct {
	CommunityID string `json:"community_id"`
}

// ListCommunityReservedNamesRequest is the request for the POST /v1/communities/reserved_names/list endpoint
// We're intentionally using per_page/page here rather than a cursor
// because this is an internal endpoint used by a rails app (admin-panel)
// and paging is easier this way with rails.
type ListCommunityReservedNamesRequest struct {
	Page    int    `json:"page"`
	PerPage int    `json:"per_page"`
	Filter  string `json:"filter"`
}

// ListCommunityReservedNamesResponse is the response for the POST /v1/communities/reserved_names/list endpoint
type ListCommunityReservedNamesResponse struct {
	TotalPages    int                     `json:"total_pages"`
	ReservedNames []CommunityReservedName `json:"reserved_names"`
}

// CommunityReservedName is the internal structure for ListCommunityReservedNamesResponse
type CommunityReservedName struct {
	Name string `json:"reserved_name"`
}

// CreateCommunityReservedNameRequest is the response for the POST /v1/communities/reserved_names/create endpoint
type CreateCommunityReservedNameRequest struct {
	ReservedName string `json:"reserved_name"`
}

// DeleteCommunityReservedNameRequest is the response for the POST /v1/communities/reserved_names/delete endpoint
type DeleteCommunityReservedNameRequest struct {
	ReservedName string `json:"reserved_name"`
}

// ListUserBlocksParams is the body for the POST /v1/users/blocks/get endpoint
type ListUserBlocksParams struct {
	UserID string `json:"user_id"`
}

// ListUserBlocksResponse is the response for POST /v1/users/blocks/get endpoint
type ListUserBlocksResponse struct {
	BlockedUserIDs []string `json:"blocked_user_ids"`
}

// AddUserBlockParams is the body for the POST /v1/users/blocks/add endpoint
type AddUserBlockParams struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
	Reason       string `json:"reason"`
	// SourceContext identifies what user flow initiated the block (e.g. chat, whisper).
	SourceContext string `json:"source_context"`
}

// AddUserBlockResponse is the respnose for POST /v1/users/blocks/add endpoint
type AddUserBlockResponse struct {
	Status  int    `json:"status"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// RemoveUserBlockParams is the body for the POST /v1/users/blocks/remove endpoint
type RemoveUserBlockParams struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
}

// ParseMessageRequest is the body for the POST /v1/messages/parse endpoint
type ParseMessageRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
	Body      string `json:"body"`
}

// ParseMessageResponse is the response of the POST /v1/messages/parse endpoint
type ParseMessageResponse struct {
	Status      ParseMessageStatus  `json:"status"`
	Message     *ParseMessageResult `json:"message,omitempty"`
	BannedUntil *time.Time          `json:"banned_until,omitempty"`
}

// ParseMessageResult is the inner JSON for the parse message endpoint
type ParseMessageResult struct {
	Body            string                      `json:"body"`
	Emoticons       []ParseMessageEmoticonRange `json:"emoticons"`
	UserBadges      []ParseMessageUserBadge     `json:"user_badges"`
	UserColor       string                      `json:"user_color"`
	UserLogin       string                      `json:"user_login"`
	UserDisplayName string                      `json:"user_display_name"`
}

// ParseMessageEmoticonRange defines the JSON structure for an emoticon range
type ParseMessageEmoticonRange struct {
	ParseMessageBodyRange
	ID string `json:"id"`
}

// ParseMessageBodyRange defines the JSON structure for an arbitrary range
type ParseMessageBodyRange struct {
	Begin int `json:"begin"`
	End   int `json:"end"`
}

type ParseMessageUserBadge struct {
	ID      string `json:"id"`
	Version string `json:"version"`
}

type ParseMessageStatus string

const (
	ParseMessageStatusUserBanned      = "user_banned"
	ParseMessageStatusUserNotVerified = "user_not_verified"
	ParseMessageStatusReviewRequired  = "review_required"
	ParseMessageStatusOK              = "ok"
)
