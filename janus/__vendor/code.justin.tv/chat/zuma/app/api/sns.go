package api

const (
	SNSCommunityModerationActionEventName = "community_moderation_action"
	SNSCommunityUserReportEventName       = "user_report"
	SNSBlockUserEventName                 = "block_user"
	SNSUnblockUserEventName               = "unblock_user"
)

// CommunityModActionSNSEvent is the event published when a mod action is taken in a community
type CommunityModActionSNSEvent struct {
	CommunityID   string `json:"community_id"`
	ModUserID     string `json:"mod_user_id"`
	TargetUserID  string `json:"target_user_id"`
	Action        string `json:"action"`
	Reason        string `json:"reason"`
	UnixTimestamp int64  `json:"unix_timestamp"`
}

const (
	ModActionTypeBan       = "ban"
	ModActionTypeUnban     = "unban"
	ModActionTypeTimeout   = "timeout"
	ModActionTypeUntimeout = "untimeout"
)

// CommunityUserReportSNSEvent is the event published when a user reports a channel in a community
type CommunityUserReportSNSEvent struct {
	CommunityID     string `json:"community_id"`
	ReportingUserID string `json:"reporting_user_id"`
	ChannelID       string `json:"channel_id"`
	Description     string `json:"description"`
	UnixTimestamp   int64  `json:"unix_timestamp"`
}

// BlockUserSNSEvent is the event published when a user is blocked
type BlockUserSNSEvent struct {
	UserID        string `json:"user_id"`
	TargetUserID  string `json:"target_user_id"`
	Reason        string `json:"reason"`
	SourceContext string `json:"source_context"`
}

// UnblockUserSNSEvent is the event published when a user is unblocked
type UnblockUserSNSEvent struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
}
