package api

const (
	// channel mods
	// used for TMI to return text such as 'that user is already a mod'
	ErrCodeTargetUserAlreadyMod = "target_user_already_mod"
	ErrCodeTargetUserNotMod     = "target_user_not_mod"

	// general-purpose
	ErrCodeRequestingUserNotPermitted = "requesting_user_not_permitted"
	ErrCodeRequestingUserNotFound     = "requesting_user_not_found"
	ErrCodeTargetUserNotFound         = "target_user_not_found"
	ErrCodeTargetUserStaff            = "target_user_staff"
	ErrCodeTargetUserSelf             = "target_user_self"

	// create community
	ErrCodeAccountTooYoung         = "account_too_young"
	ErrCodeCommunityNameExists     = "community_name_exists"
	ErrCodeCommunityNameInvalid    = "community_name_invalid"
	ErrCodeCommunityNameReserved   = "community_name_reserved"
	ErrCodeShortDescriptionTooLong = "short_description_too_long"
	ErrCodeLongDescriptionTooLong  = "long_description_too_long"
	ErrCodeRulesTooLong            = "rules_too_long"
	ErrCodeOwnTooManyCommunities   = "own_too_many_communities"
	ErrCodeUnverifiedEmail         = "unverified_email"
	ErrCodeNoTwoFactor             = "two_factor_not_enabled"
	ErrCodeInvalidLanguage         = "invalid_language"

	// get community by name
	ErrCodeCommunityNameNotFound = "community_name_not_found"

	// get community
	ErrCodeCommunityIDNotFound = "community_id_not_found"
	ErrCodeCommunityTOSBanned  = "community_tos_banned"

	// set channel community
	ErrCodeChannelBannedFromCommunity   = "channel_banned_from_community"
	ErrCodeChannelTimedOutFromCommunity = "channel_timed_out_from_community"

	// upload Images
	ErrCodeWrongImageDimensions = "wrong_image_dimensions"
	ErrCodeImageTooLarge        = "image_too_large"
	ErrCodeUnsupportedImageType = "unsupported_image_type"

	// add mods
	ErrCodeTargetUserBanned = "target_user_banned"

	// add bans
	ErrCodeTargetUserOwner = "target_user_owner"
	ErrCodeTargetUserMod   = "target_user_mod"

	// user blocks
	ErrCodeTargetUserCannotBeBlocked = "target_cannot_be_blocked"
)

// ErrorResponse is used to give services more detailed error responses to
// errors caused by user input
type ErrorResponse struct {
	Err    string `json:"error"`
	Status int    `json:"status"`
}

func (e *ErrorResponse) GetErrorCode() int {
	return e.Status
}

func (e *ErrorResponse) Error() string {
	return e.Err
}
