package redicache

import (
	"reflect"
	"time"

	"golang.org/x/net/context"
)

var _ Cache = (*StubCache)(nil)

// StubCache is a dummy cache that will always cache miss.
type StubCache struct{}

func (c *StubCache) Cached(ctx context.Context, key string, ttl time.Duration, res interface{}, fn func() (interface{}, error)) error {
	fetched, err := fn()
	if err != nil {
		return err
	}

	rv := reflect.ValueOf(res).Elem()
	if fetched == nil {
		rv.Set(reflect.Zero(rv.Type()))
	} else {
		rv.Set(reflect.ValueOf(fetched))
	}
	return nil
}

func (c *StubCache) CachedBool(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (bool, error) {
	val, err := fn()
	if err != nil {
		return false, err
	}
	return val.(bool), nil
}

func (c *StubCache) CachedBoolDifferentTTLs(ctx context.Context, key string, ttls CachedBoolTTLs, fn func() (bool, error)) (bool, error) {
	return fn()
}

func (c *StubCache) CachedInt(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (int, error) {
	val, err := fn()
	if err != nil {
		return 0, err
	}
	return val.(int), nil
}

func (c *StubCache) CachedInt64(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (int64, error) {
	val, err := fn()
	if err != nil {
		return int64(0), err
	}
	return val.(int64), nil
}

func (c *StubCache) CachedString(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (string, error) {
	val, err := fn()
	if err != nil {
		return "", err
	}
	return val.(string), nil
}

func (c *StubCache) CachedIntSlice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]int, error) {
	val, err := fn()
	if err != nil {
		return nil, err
	}
	return val.([]int), nil
}

func (c *StubCache) CachedInt64Slice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]int64, error) {
	val, err := fn()
	if err != nil {
		return nil, err
	}
	return val.([]int64), nil
}

func (c *StubCache) CachedStringSlice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]string, error) {
	val, err := fn()
	if err != nil {
		return nil, err
	}
	return val.([]string), nil
}

func (c *StubCache) Invalidate(ctx context.Context, key string) error {
	return nil
}
