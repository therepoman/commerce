package redicache

import (
	"fmt"
	"reflect"
	"time"

	"golang.org/x/net/context"

	"code.justin.tv/chat/golibs/logx"
)

// Cached is a helper to cache any arbitrary type.
// Notes:
// - Use the Cached* helpers below to cache common data types (like primitives)
// - The type of <res> must be a pointer to the return type of <fn>
//   Examples:
//   - <res> is type *int, <fn> returns 10
//   - <res> is type *Foo, <fn> returns Foo{}
//   - <res> is type **Foo, <fn> returns nil or &Foo{}
func (r *redicache) Cached(ctx context.Context, key string, ttl time.Duration, res interface{}, fn func() (interface{}, error)) error {
	// Check key for cached value
	found, err := r.getJSON(ctx, key, &res)
	if err != nil {
		logx.Error(ctx, fmt.Sprintf("cache read error: %v", err), logx.Fields{
			"key": key,
		})
	} else if found {
		return nil
	}

	// Fetch value from true data source; use reflection to set <res> argument
	fetched, err := fn()
	if err != nil {
		return err
	}
	rv := reflect.ValueOf(res).Elem()
	if !rv.CanSet() {
		return fmt.Errorf("cannot call set on zero value")
	}
	if fetched == nil {
		rv.Set(reflect.Zero(rv.Type()))
	} else {
		rv.Set(reflect.ValueOf(fetched))
	}

	// Cache value for future lookups
	if err := r.safeSetJSON(ctx, key, fetched, ttl); err != nil {
		logx.Error(ctx, fmt.Sprintf("cache write error: %v", err), logx.Fields{
			"key": key,
		})
	}
	return nil
}

type CachedBoolTTLs struct {
	TrueTTL, FalseTTL time.Duration
}

// CachedBoolDifferentTTLs works like CachedBool but lets the caller specify a
// different cache TTL for true or false values.
// A TTL of 0 implies that result should not be cached.
func (r *redicache) CachedBoolDifferentTTLs(ctx context.Context, key string, ttls CachedBoolTTLs, fn func() (bool, error)) (bool, error) {
	// Check key for cached value
	var res bool
	found, err := r.getJSON(ctx, key, &res)
	if err != nil {
		logx.Error(ctx, err, logx.Fields{
			"note": "reading from cache",
			"key":  key,
		})
	} else if found {
		return res, nil
	}

	// Fetch value from true data source; use reflection to set <res> argument
	fetched, err := fn()
	if err != nil {
		return false, err
	}

	// Cache value for future lookups
	ttl := ttls.FalseTTL
	if fetched {
		ttl = ttls.TrueTTL
	}
	if ttl > 0 {
		if err := r.safeSetJSON(ctx, key, fetched, ttl); err != nil {
			logx.Error(ctx, err, logx.Fields{
				"note": "writing to cache",
				"key":  key,
			})
		}
	}
	return fetched, nil
}

func (r *redicache) CachedBool(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (bool, error) {
	var res bool
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}

func (r *redicache) CachedInt(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (int, error) {
	var res int
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}

func (r *redicache) CachedInt64(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (int64, error) {
	var res int64
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}

func (r *redicache) CachedString(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (string, error) {
	var res string
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}

func (r *redicache) CachedIntSlice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]int, error) {
	var res []int
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}

func (r *redicache) CachedInt64Slice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]int64, error) {
	var res []int64
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}

func (r *redicache) CachedStringSlice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]string, error) {
	var res []string
	err := r.Cached(ctx, key, ttl, &res, fn)
	return res, err
}
