package redicache

import (
	"encoding/json"
	"net"
	"strconv"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/net/context"

	"code.justin.tv/chat/redis"
)

type Cache interface {
	Cached(ctx context.Context, key string, ttl time.Duration, res interface{}, fn func() (interface{}, error)) error
	CachedBool(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (bool, error)
	CachedBoolDifferentTTLs(ctx context.Context, key string, ttls CachedBoolTTLs, fn func() (bool, error)) (bool, error)
	CachedInt(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (int, error)
	CachedInt64(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (int64, error)
	CachedString(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) (string, error)
	CachedIntSlice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]int, error)
	CachedInt64Slice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]int64, error)
	CachedStringSlice(ctx context.Context, key string, ttl time.Duration, fn func() (interface{}, error)) ([]string, error)

	Invalidate(ctx context.Context, key string) error
}

type Config struct {
	Host        string
	Port        int
	Password    string
	KeyPrefix   string
	StatsPrefix string

	ServerTimeout  time.Duration
	ConnectTimeout time.Duration
	ReadTimeout    time.Duration
	WriteTimeout   time.Duration

	MaxConns int
}

type redicache struct {
	redis redis.Redis
}

func New(conf Config, stats statsd.Statter) (Cache, error) {
	redis, err := redis.Init{
		Address:   net.JoinHostPort(conf.Host, strconv.Itoa(conf.Port)),
		Password:  conf.Password,
		KeyPrefix: conf.KeyPrefix,

		ConnectTimeout: time.Duration(conf.ConnectTimeout),
		ReadTimeout:    time.Duration(conf.ReadTimeout),
		WriteTimeout:   time.Duration(conf.WriteTimeout),
		MaxConns:       conf.MaxConns,

		XactGroup:   "redis",
		StatsPrefix: conf.StatsPrefix,
		Stats:       stats,
	}.New()
	if err != nil {
		return nil, err
	}

	return &redicache{
		redis: redis,
	}, nil
}

func NewFromRedis(redis redis.Redis) Cache {
	return &redicache{
		redis: redis,
	}
}

func (r *redicache) Invalidate(ctx context.Context, key string) error {
	_, err := r.redis.Del(ctx, key)
	return err
}

func (r *redicache) getJSON(ctx context.Context, key string, res interface{}) (bool, error) {
	val, ok, err := r.redis.Get(ctx, key)
	if err != nil || !ok {
		return ok, err
	}
	err = json.Unmarshal([]byte(val), res)
	return ok, err
}

func (r *redicache) safeSetJSON(ctx context.Context, key string, val interface{}, ttl time.Duration) error {
	marshalled, err := json.Marshal(val)
	if err != nil {
		return err
	}
	_, err = r.redis.SafeSetWithTTL(ctx, key, string(marshalled), ttl)
	return err
}
