package metadata_test

import (
	"testing"

	"time"

	"fmt"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/metadata"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	oldGame    = "oldgame"
	oldAsin    = "oldasin"
	futureGame = "futuregame"
	futureAsin = "futureasin"
)

func TestGameMetadataDB_Lookup(t *testing.T) {

	var timeNow = time.Now()
	var timePast1 = timeNow.AddDate(-1, 0, 0)
	var timeFuture1 = timeNow.AddDate(+1, 0, 0)
	var timeFuture2 = timeNow.AddDate(+2, 0, 0)

	Convey("Testing game metadata DB lookup", t, func() {

		gamesMetadata := models.GameMetadataMap{
			oldGame: {
				ReleaseEndDate:      timeFuture1,
				ReleaseStartDate:    timePast1,
				GameEmbargoDate:     timeFuture1,
				GamePublicationDate: timePast1,
			},
			futureGame: {
				ReleaseEndDate:      timeFuture2,
				ReleaseStartDate:    timeFuture1,
				GameEmbargoDate:     timeFuture2,
				GamePublicationDate: timeFuture1,
			},
			errorGame: {
				ReleaseEndDate:      timeFuture1,
				ReleaseStartDate:    timePast1,
				GameEmbargoDate:     timeFuture1,
				GamePublicationDate: timePast1,
			},
		}

		asinMap := new(mocks.IAsinMap)
		asinMap.On("Get", oldGame).Return(&janus.GameAsinMap{
			Asin:      oldAsin,
			GameTitle: oldGame,
			Status:    "LIVE",
		}, nil)

		asinMap.On("Get", futureGame).Return(&janus.GameAsinMap{
			Asin:      futureAsin,
			GameTitle: futureGame,
			Status:    "LIVE",
		}, nil)

		asinMap.On("Get", errorGame).Return(nil, fmt.Errorf("test error"))

		gameMetadataDB := metadata.NewGameMetadataDB(gamesMetadata, asinMap)

		var gameMetadata *models.GameMetadata
		var err error
		Convey("with a valid gameid", func() {
			Convey("with a launch date in the past", func() {
				gameMetadata, err = gameMetadataDB.Lookup(oldGame, true)
				So(err, ShouldBeNil)
				So(gameMetadata, ShouldNotBeNil)
				So(gameMetadata.Asin, ShouldEqual, oldAsin)

				gameMetadata, err = gameMetadataDB.Lookup(oldGame, false)
				So(err, ShouldBeNil)
				So(gameMetadata, ShouldNotBeNil)
				So(gameMetadata.Asin, ShouldEqual, oldAsin)
			})

			Convey("with a launch date in the future", func() {
				gameMetadata, err = gameMetadataDB.Lookup(futureGame, true)
				So(err, ShouldBeNil)
				So(gameMetadata, ShouldNotBeNil)
				So(gameMetadata.Asin, ShouldEqual, futureAsin)

				gameMetadata, err = gameMetadataDB.Lookup(futureGame, false)
				So(err, ShouldBeNil)
				So(gameMetadata, ShouldBeNil)
			})
		})

		Convey("with incorrect case gameID", func() {
			gameMetadata, err = gameMetadataDB.Lookup("OldGame", false)
			So(err, ShouldBeNil)
			So(gameMetadata, ShouldNotBeNil)
			So(gameMetadata.Asin, ShouldEqual, oldAsin)
		})

		Convey("with invalid gameID", func() {
			gameMetadata, err = gameMetadataDB.Lookup("FakeGameNotReal", false)
			So(err, ShouldBeNil)
			So(gameMetadata, ShouldBeNil)
		})

		Convey("with error response", func() {
			gameMetadata, err = gameMetadataDB.Lookup(errorGame, false)
			So(err, ShouldNotBeNil)
			So(gameMetadata, ShouldBeNil)
		})

	})
}
