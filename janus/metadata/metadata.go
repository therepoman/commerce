package metadata

import (
	"strings"
	"time"

	"fmt"

	"code.justin.tv/commerce/janus/models"
	log "github.com/sirupsen/logrus"
)

// GameMetadataDB is the (local) database that is used to lookup Fuel game metadata.
type GameMetadataDB struct {
	AsinMap  IAsinMap
	metadata models.GameMetadataMap
}

// Lookup does a database lookup on the gameID. Lookups are case-insensitive. Game metadata will be hidden
// if the launch date is in the future, unless the input shouldShowHiddenDetails flag is set to true.
func (db GameMetadataDB) Lookup(gameID string, shouldShowHiddenDetails bool) (*models.GameMetadata, error) {
	gameIDLower := strings.ToLower(gameID)
	// games are stored in lower case. gameIDs forced to lower case for lookup
	gameMetadata, exists := db.metadata[gameIDLower]
	if !exists {
		return nil, nil
	}

	// lookup the asin map against the dynamo table
	result, err := db.AsinMap.Get(gameIDLower)
	if err != nil {
		return nil, fmt.Errorf("Error while doing asin lookup. Error: %s Game title: %s", err, gameIDLower)
	}

	// if the asin doesn't exist in dynamo, hide the game
	if result == nil {
		log.Infof("Metadata entry exists, but asin map entry missing from DAO result. "+
			"Game title: %s Metadata asin: %s", gameIDLower, gameMetadata.Asin)
		return nil, nil
	}

	// set the mapped asin from the dynamo table, not the config
	gameMetadata.Asin = result.Asin

	// if in the future and no override, hide the game
	if gameMetadata.GamePublicationDate.After(time.Now()) && !shouldShowHiddenDetails {
		log.Infof("Game [%s] has not launched, hiding it.", gameID)
		return nil, nil
	}

	return &gameMetadata, nil
}

// NewGameMetadataDB constructs a new GameMetadataDB type using the specified input metadata
func NewGameMetadataDB(input map[string]models.GameMetadata, asinMap IAsinMap) GameMetadataDB {
	return GameMetadataDB{
		metadata: input,
		AsinMap:  asinMap,
	}
}
