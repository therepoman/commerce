package metadata_test

import (
	"testing"

	"fmt"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	. "code.justin.tv/commerce/janus/metadata"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	cachedGame     = "cachedgame"
	cachedAsin     = "cachedasin"
	liveGame       = "livegame"
	liveAsin       = "liveasin"
	suppressedGame = "suppressedgame"
	suppressedAsin = "suppressedasin"
	unmappedGame   = "unamppedgame"
	errorGame      = "errorgame"
)

func TestLookup(t *testing.T) {
	fuelAsinMapDao := new(mocks.IFuelAsinMapDAO)
	asinMapCacheClient := new(mocks.IAsinMapCacheClient)

	fuelAsinMapDao.On("Get", liveGame).Return(&dynamo.FuelAsinMap{
		Asin:      liveAsin,
		GameTitle: liveGame,
		Status:    StatusLive,
	}, nil)

	fuelAsinMapDao.On("Get", suppressedGame).Return(&dynamo.FuelAsinMap{
		Asin:      suppressedAsin,
		GameTitle: suppressedGame,
		Status:    StatusSuppressed,
	}, nil)

	fuelAsinMapDao.On("Get", unmappedGame).Return(nil, nil)

	fuelAsinMapDao.On("Get", errorGame).Return(nil, fmt.Errorf("testerror"))

	asinMapCacheClient.On("GetAsinMap", cachedGame).Return(true, &janus.GameAsinMap{
		Asin:      cachedAsin,
		GameTitle: cachedGame,
		Status:    StatusLive,
	}, nil)
	asinMapCacheClient.On("GetAsinMap", liveGame).Return(false, nil)
	asinMapCacheClient.On("GetAsinMap", suppressedGame).Return(false, nil)
	asinMapCacheClient.On("GetAsinMap", unmappedGame).Return(false, nil)
	asinMapCacheClient.On("GetAsinMap", errorGame).Return(false, nil)
	asinMapCacheClient.On("SetAsinMap", mock.Anything).Return()

	asinMap := NewAsinMap(fuelAsinMapDao, asinMapCacheClient)

	Convey("Test Lookup", t, func() {
		Convey("with a cached game", func() {
			result, err := asinMap.Get(cachedGame)
			So(err, ShouldBeNil)
			So(result, ShouldNotBeNil)
			So(result.Asin, ShouldEqual, cachedAsin)
			So(result.Status, ShouldEqual, StatusLive)
		})
		Convey("with a live game", func() {
			result, err := asinMap.Get(liveGame)
			So(err, ShouldBeNil)
			So(result, ShouldNotBeNil)
			So(result.Asin, ShouldEqual, liveAsin)
			So(result.Status, ShouldEqual, StatusLive)
		})
		Convey("with a suppressed game", func() {
			result, err := asinMap.Get(suppressedGame)
			So(err, ShouldBeNil)
			So(result, ShouldBeNil)
		})
		Convey("with an unmapped game", func() {
			result, err := asinMap.Get(unmappedGame)
			So(err, ShouldBeNil)
			So(result, ShouldBeNil)
		})
		Convey("with a dynamo error", func() {
			result, err := asinMap.Get(errorGame)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})
	})
}
