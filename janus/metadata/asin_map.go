package metadata

import (
	"fmt"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/dynamo"
	log "github.com/sirupsen/logrus"
)

const (
	// StatusLive represents the Live status of an asin mapping, meaning the mapping is visible to callers
	StatusLive = "LIVE"

	// StatusSuppressed represents the Suppressed status of an asin mapping, meaning the mapping is NOT visible to callers
	StatusSuppressed = "SUPPRESSED"
)

type IAsinMap interface {
	Get(gameTitle string) (*janus.GameAsinMap, error)
	Put(gameAsinMap janus.GameAsinMap) error
}

type AsinMap struct {
	fuelAsinMapDAO dynamo.IFuelAsinMapDAO
	cache          cache.IAsinMapCacheClient
}

// NewAsinMap constructs a new AsinMap type using the specified input
func NewAsinMap(fuelAsinMapDAO dynamo.IFuelAsinMapDAO, cache cache.IAsinMapCacheClient) IAsinMap {
	return AsinMap{
		fuelAsinMapDAO: fuelAsinMapDAO,
		cache:          cache,
	}
}

// Get returns the asin mapping associated with the specified gameTitle
// The response GameAsinMap is nil when no result is found
func (asinMap AsinMap) Get(gameTitle string) (*janus.GameAsinMap, error) {
	isCacheHit, cacheEntry := asinMap.cache.GetAsinMap(gameTitle)
	if isCacheHit {
		return cacheEntry, nil
	}

	result, err := asinMap.fuelAsinMapDAO.Get(gameTitle)
	if err != nil {
		return nil, fmt.Errorf("Error during asin map DAO query: %s", err)
	}

	if result == nil {
		return nil, nil
	}

	if result.Status != StatusLive {
		log.Infof("Game asin mapping is suppressed, hiding it. Game: %s", gameTitle)
		return nil, nil
	}

	asinMapEntry := &janus.GameAsinMap{
		Asin:      result.Asin,
		GameTitle: result.GameTitle,
		Status:    result.Status,
	}

	asinMap.cache.SetAsinMap(asinMapEntry)

	return asinMapEntry, nil
}

// Put adds the input GameAsinMap entry to the underlying map datastore
func (asinMap AsinMap) Put(gameAsinMap janus.GameAsinMap) error {
	input := dynamo.FuelAsinMap{
		Asin:      gameAsinMap.Asin,
		GameTitle: gameAsinMap.GameTitle,
		Status:    gameAsinMap.Status,
	}

	return asinMap.fuelAsinMapDAO.Put(&input)
}
