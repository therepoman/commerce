package mocks

import "github.com/stretchr/testify/mock"

import "github.com/aws/aws-sdk-go/service/sqs"

type Worker struct {
	mock.Mock
}

// Handle provides a mock function with given fields: msg
func (_m *Worker) Handle(msg *sqs.Message) error {
	ret := _m.Called(msg)

	var r0 error
	if rf, ok := ret.Get(0).(func(*sqs.Message) error); ok {
		r0 = rf(msg)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
