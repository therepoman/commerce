package mocks

import "github.com/stretchr/testify/mock"

import "golang.org/x/net/context"
import "time"
import "code.justin.tv/common/twitchhttp"
import "code.justin.tv/web/users-service/models"

type UsersServiceClient struct {
	mock.Mock
}

// GetUserByID provides a mock function with given fields: ctx, userID, reqOpts
func (_m *UsersServiceClient) GetUserByID(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error) {
	ret := _m.Called(ctx, userID, reqOpts)

	var r0 *models.Properties
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) *models.Properties); ok {
		r0 = rf(ctx, userID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Properties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, userID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserByIDAndParams provides a mock function with given fields: ctx, userID, params, reqOpts
func (_m *UsersServiceClient) GetUserByIDAndParams(ctx context.Context, userID string, params *models.FilterParams, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error) {
	ret := _m.Called(ctx, userID, params, reqOpts)

	var r0 *models.Properties
	if rf, ok := ret.Get(0).(func(context.Context, string, *models.FilterParams, *twitchhttp.ReqOpts) *models.Properties); ok {
		r0 = rf(ctx, userID, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Properties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *models.FilterParams, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, userID, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserByLogin provides a mock function with given fields: ctx, login, reqOpts
func (_m *UsersServiceClient) GetUserByLogin(ctx context.Context, login string, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error) {
	ret := _m.Called(ctx, login, reqOpts)

	var r0 *models.Properties
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) *models.Properties); ok {
		r0 = rf(ctx, login, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Properties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, login, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUsers provides a mock function with given fields: ctx, params, reqOpts
func (_m *UsersServiceClient) GetUsers(ctx context.Context, params *models.FilterParams, reqOpts *twitchhttp.ReqOpts) (*models.PropertiesResult, error) {
	ret := _m.Called(ctx, params, reqOpts)

	var r0 *models.PropertiesResult
	if rf, ok := ret.Get(0).(func(context.Context, *models.FilterParams, *twitchhttp.ReqOpts) *models.PropertiesResult); ok {
		r0 = rf(ctx, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.PropertiesResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *models.FilterParams, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserByEmail provides a mock function with given fields: ctx, email, reqOpts
func (_m *UsersServiceClient) GetUserByEmail(ctx context.Context, email string, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error) {
	ret := _m.Called(ctx, email, reqOpts)

	var r0 *models.Properties
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) *models.Properties); ok {
		r0 = rf(ctx, email, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Properties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, email, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetBannedUsers provides a mock function with given fields: ctx, until, reqOpts
func (_m *UsersServiceClient) GetBannedUsers(ctx context.Context, until time.Time, reqOpts *twitchhttp.ReqOpts) (*models.PropertiesResult, error) {
	ret := _m.Called(ctx, until, reqOpts)

	var r0 *models.PropertiesResult
	if rf, ok := ret.Get(0).(func(context.Context, time.Time, *twitchhttp.ReqOpts) *models.PropertiesResult); ok {
		r0 = rf(ctx, until, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.PropertiesResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, time.Time, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, until, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUsersByLoginLike provides a mock function with given fields: ctx, pattern, reqOpts
func (_m *UsersServiceClient) GetUsersByLoginLike(ctx context.Context, pattern string, reqOpts *twitchhttp.ReqOpts) (*models.PropertiesResult, error) {
	ret := _m.Called(ctx, pattern, reqOpts)

	var r0 *models.PropertiesResult
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) *models.PropertiesResult); ok {
		r0 = rf(ctx, pattern, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.PropertiesResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, pattern, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetRenameEligibility provides a mock function with given fields: ctx, userID, reqOpts
func (_m *UsersServiceClient) GetRenameEligibility(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.RenameProperties, error) {
	ret := _m.Called(ctx, userID, reqOpts)

	var r0 *models.RenameProperties
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) *models.RenameProperties); ok {
		r0 = rf(ctx, userID, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.RenameProperties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, userID, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// BanUserByID provides a mock function with given fields: ctx, userID, reportingID, banType, isWarning, banReason, reqOpts
func (_m *UsersServiceClient) BanUserByID(ctx context.Context, userID string, reportingID string, banType string, isWarning bool, banReason string, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, userID, reportingID, banType, isWarning, banReason, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, bool, string, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, userID, reportingID, banType, isWarning, banReason, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UnbanUserByID provides a mock function with given fields: ctx, userID, reqOpts
func (_m *UsersServiceClient) UnbanUserByID(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, userID, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, userID, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// AddDMCAStrike provides a mock function with given fields: ctx, userID, reqOpts
func (_m *UsersServiceClient) AddDMCAStrike(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, userID, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, userID, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RemoveDMCAStrike provides a mock function with given fields: ctx, userID, reqOpts
func (_m *UsersServiceClient) RemoveDMCAStrike(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, userID, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, userID, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// SetUser provides a mock function with given fields: ctx, userID, uup, reqOpts
func (_m *UsersServiceClient) SetUser(ctx context.Context, userID string, uup *models.UpdateableProperties, reqOpts *twitchhttp.ReqOpts) error {
	ret := _m.Called(ctx, userID, uup, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, *models.UpdateableProperties, *twitchhttp.ReqOpts) error); ok {
		r0 = rf(ctx, userID, uup, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreateUser provides a mock function with given fields: ctx, cup, reqOpts
func (_m *UsersServiceClient) CreateUser(ctx context.Context, cup *models.CreateUserProperties, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error) {
	ret := _m.Called(ctx, cup, reqOpts)

	var r0 *models.Properties
	if rf, ok := ret.Get(0).(func(context.Context, *models.CreateUserProperties, *twitchhttp.ReqOpts) *models.Properties); ok {
		r0 = rf(ctx, cup, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Properties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *models.CreateUserProperties, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, cup, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetGlobalPrivilegedUsers provides a mock function with given fields: ctx, roles, reqOpts
func (_m *UsersServiceClient) GetGlobalPrivilegedUsers(ctx context.Context, roles []string, reqOpts *twitchhttp.ReqOpts) (*models.GlobalPrivilegedUsers, error) {
	ret := _m.Called(ctx, roles, reqOpts)

	var r0 *models.GlobalPrivilegedUsers
	if rf, ok := ret.Get(0).(func(context.Context, []string, *twitchhttp.ReqOpts) *models.GlobalPrivilegedUsers); ok {
		r0 = rf(ctx, roles, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.GlobalPrivilegedUsers)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string, *twitchhttp.ReqOpts) error); ok {
		r1 = rf(ctx, roles, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
