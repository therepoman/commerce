package mocks

import context "golang.org/x/net/context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/janus/models"

// IChatBadgesClientWrapper is an autogenerated mock type for the IChatBadgesClientWrapper type
type IChatBadgesClientWrapper struct {
	mock.Mock
}

// GetBadgeDisplay provides a mock function with given fields: ctx
func (_m *IChatBadgesClientWrapper) GetBadgeDisplay(ctx context.Context) (*models.GetBadgeDisplay, error) {
	ret := _m.Called(ctx)

	var r0 *models.GetBadgeDisplay
	if rf, ok := ret.Get(0).(func(context.Context) *models.GetBadgeDisplay); ok {
		r0 = rf(ctx)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.GetBadgeDisplay)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
