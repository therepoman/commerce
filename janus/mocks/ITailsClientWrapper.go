// Code generated by mockery v1.0.0
package mocks

import mock "github.com/stretchr/testify/mock"

// ITailsClientWrapper is an autogenerated mock type for the ITailsClientWrapper type
type ITailsClientWrapper struct {
	mock.Mock
}

// GetLinkedAmazonCOR provides a mock function with given fields: userID
func (_m *ITailsClientWrapper) GetLinkedAmazonCOR(userID string) (string, error) {
	ret := _m.Called(userID)

	var r0 string
	if rf, ok := ret.Get(0).(func(string) string); ok {
		r0 = rf(userID)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
