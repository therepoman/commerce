package mocks

import janus "code.justin.tv/commerce/janus/client"
import mock "github.com/stretchr/testify/mock"
import notification "code.justin.tv/commerce/janus/notification"

// INotifier is an autogenerated mock type for the INotifier type
type INotifier struct {
	mock.Mock
}

// Notify provides a mock function with given fields: _a0, message
func (_m *INotifier) Notify(_a0 janus.ChatNotification, message string) error {
	ret := _m.Called(_a0, message)

	var r0 error
	if rf, ok := ret.Get(0).(func(janus.ChatNotification, string) error); ok {
		r0 = rf(_a0, message)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

var _ notification.INotifier = (*INotifier)(nil)
