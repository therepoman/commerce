package crate_test

import (
	"testing"

	"code.justin.tv/commerce/janus/crate"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	whitelistedAsin    = "CMRCASIN"
	nonWhitelistedAsin = "foo"
)

func TestCrateHelper_ParseCrates(t *testing.T) {
	Convey("When calling parse crates", t, func() {
		appSettings := new(mocks.Client)
		crateHelper := crate.NewCrateHelper(appSettings)
		product := models.ADGProduct{}
		product.Price.Price = "1"
		Convey("When there are no crates", func() {
			appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil).Once()
			crates := crateHelper.ParseCrates(product)
			So(len(crates), ShouldEqual, 0)
		})
		Convey("When crate asins are empty strings", func() {
			product.Details.Crate = models.Crate{ASIN: ""}
			appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(true, nil).Once()
			product.ASIN = whitelistedAsin
			product.Price.Price = "0.00"
			crates := crateHelper.ParseCrates(product)
			So(len(crates), ShouldEqual, 2)
			So(crates[0].ASIN, ShouldEqual, crate.HardcodedCrateAsin)
			So(crates[1].ASIN, ShouldEqual, crate.HardcodedCrateAsin)
		})
		Convey("When appsettings are off", func() {
			product.Details.Crate = models.Crate{ASIN: "testasin"}
			appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(false, nil).Once()
			crates := crateHelper.ParseCrates(product)
			So(len(crates), ShouldEqual, 1)
			So(crates[0].ASIN, ShouldEqual, "testasin")
		})
		Convey("When appsettings are on", func() {
			product.Details.Crate = models.Crate{ASIN: "testasin"}
			appSettings.On("GetBool", Anything, Anything, Anything, Anything).Return(true, nil).Once()

			Convey("When asin is whitelisted", func() {
				product.ASIN = whitelistedAsin
				crates := crateHelper.ParseCrates(product)
				So(len(crates), ShouldEqual, 2)
				So(crates[0].ASIN, ShouldEqual, "testasin")
				So(crates[1].ASIN, ShouldEqual, "testasin")
			})

			Convey("When asin is not whitelisted", func() {
				product.ASIN = nonWhitelistedAsin
				crates := crateHelper.ParseCrates(product)
				So(len(crates), ShouldEqual, 1)
			})
		})
	})
}
