package crate

import (
	"context"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/web/app-settings/client"
	log "github.com/sirupsen/logrus"
)

const (
	//App Settings Info
	multiCrateEnabledNamespace = "web"
	multiCrateEnabledKey       = "cmrc_cowabunga_released"
	zeroDollars                = "0.00"

	// Temporary config for product ASINs pending https://issues.amazon.com/issues/ADG-5643
	fakeAsin          = "CMRCASIN"
	spaceFuelGameAsin = "B0722XXVTQ"
	spaceFuelIGCAsin  = "B071G9DYZS"

	HardcodedCrateAsin = "B01MYF798N"
)

type ICrateHelper interface {
	ParseCrates(adgProduct models.ADGProduct) []janus.Crate
}

type CrateHelper struct {
	AppSettings appsettings.Client
}

// NewCrateHelper creates new crate helper
func NewCrateHelper(client appsettings.Client) ICrateHelper {
	return &CrateHelper{
		AppSettings: client,
	}
}

// ParseCrates Temporary hard coding since the docket is not onboarded to KRATOS
func (crate *CrateHelper) ParseCrates(adgProduct models.ADGProduct) []janus.Crate {
	asin := adgProduct.Details.Crate.ASIN
	isProductPromoted := isPromoted(adgProduct.ASIN)

	if "" == asin {
		if zeroDollars != adgProduct.Price.Price {
			return []janus.Crate{}
		}
		// Hack to make promoted ASINs have real crate ASINs
		if isProductPromoted {
			asin = HardcodedCrateAsin
		}
	}
	crates := make([]janus.Crate, 1)
	crates[0] = janus.Crate{
		ASIN: asin,
	}
	if crate.isMultiCrate() && isProductPromoted {
		crates = append(crates, janus.Crate{
			ASIN: asin,
		})
	}
	return crates
}

func (crate *CrateHelper) isMultiCrate() bool {
	multiCrateEnabled, err := crate.AppSettings.GetBool(context.Background(), multiCrateEnabledNamespace, multiCrateEnabledKey, nil)
	if err != nil {
		log.Errorf("Unable to retrieve AppSettings multiCrateEnabled error: %+v", err)
		multiCrateEnabled = false
	}
	return multiCrateEnabled
}

func isPromoted(asin string) bool {
	asinMap := map[string]bool{
		fakeAsin:          true,
		spaceFuelGameAsin: true,
		spaceFuelIGCAsin:  true,
	}

	return asinMap[asin]
}
