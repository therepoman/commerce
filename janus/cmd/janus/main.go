package main

import (
	"code.justin.tv/commerce/janus/api"
	"code.justin.tv/common/twitchhttp"
	"code.justin.tv/video/roll"

	"encoding/base64"
	"os"
	"time"

	pubClient "code.justin.tv/chat/pubsub-go-pubclient/client"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/broadcaster"
	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/config"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/metadata"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/payouts"
	"code.justin.tv/commerce/janus/sqs"
	"code.justin.tv/commerce/janus/sqs/workers"
	"code.justin.tv/web/app-settings/client"
	"code.justin.tv/web/users-service/client"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	sqssdk "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/janus/crate"
	"code.justin.tv/commerce/janus/notification"
	"code.justin.tv/commerce/janus/twirp"
	"code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/revenue/moneypenny/client"

	"fmt"

	"code.justin.tv/video/rollrus"
	"golang.org/x/text/language"
)

// Constants
const (
	DEVELOPMENTENV = "development"
)

func main() {
	env := getEnv()

	// Logrus initialization
	log.SetLevel(log.InfoLevel)
	log.Infof("Starting Janus as a %s environment", env)

	cfg, err := config.LoadAndValidateConfigForEnvironment(env)
	if err != nil {
		log.Fatal(err)
	}

	metricsConfig := metrics.Config{
		Stage:                          env,
		AwsRegion:                      cfg.CloudwatchRegion,
		BufferSize:                     cfg.CloudwatchBufferSize,
		BatchSize:                      cfg.CloudwatchBatchSize,
		FlushInterval:                  time.Second * time.Duration(cfg.CloudwatchFlushIntervalSeconds),
		FlushPollCheckDelay:            time.Millisecond * time.Duration(cfg.CloudwatchFlushCheckDelayMs),
		BufferEmergencyFlushPercentage: cfg.CloudwatchEmergencyFlushPercentage,
	}

	metricsLogger, metricsFlusher := metrics.New(metricsConfig)
	go metricsFlusher.FlushMetricsAtInterval()

	// Initialize clients and DAOs
	sandstorm := auth.NewSandstormClient(cfg)

	// Load rollbar token from Sandstorm
	rollbarToken, err := sandstorm.Get(cfg.RollbarToken)
	if err != nil {
		log.Fatalf("Error getting rollbar token %s from sandstorm: %s", cfg.RollbarToken, err)
	}
	rollbarClient := roll.New(string(rollbarToken.Plaintext), env)
	log.AddHook(rollrus.NewHook(rollbarClient, log.WarnLevel))

	err = auth.InitializeDecoders(sandstorm, cfg.CartmanCerts)
	if err != nil {
		log.Fatal(err)
	}

	// Load cert/key pair from Sandstorm
	swsClientSecret, err := sandstorm.Get(cfg.SWSClientCertChain)
	if err != nil {
		log.Fatalf("Error getting SWS secret %s from sandstorm: %s", cfg.SWSClientCertChain, err)
	}

	// Load Amazon CA Certs from Sandstorm
	amazonCASecret, err := sandstorm.Get(cfg.SWSRootCertStore)
	if err != nil {
		log.Fatalf("Error getting AWS CA Certs %s from sandstorm: %s", cfg.SWSRootCertStore, err)
	}

	params := coral.SWSParams{
		SWSEnabled:      cfg.SWSEnabled,
		RootCertStore:   amazonCASecret.Plaintext,
		ClientCertChain: swsClientSecret.Plaintext,
		HostAddress:     cfg.SWSHostAddress,
		HostPort:        cfg.SWSHostPort,
	}

	usersServiceConfig := twitchhttp.ClientConf{
		Host: cfg.UsersServiceEndpoint,
	}
	usersServiceClient, err := usersservice.NewClient(usersServiceConfig)
	if err != nil {
		log.Fatalf("Failed to initalize users service client: %s", err)
	}

	gatingHelper := &auth.GatingHelper{
		UsersServiceClient:  usersServiceClient,
		CartmanAuthorizer:   &auth.CartmanAuthorizer{},
		FuelWhitelist:       cfg.FuelWhitelist,
		StaffGamesWhitelist: cfg.StaffGamesWhitelist,
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.RedisEndpoint,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	if err = redisClient.Ping().Err(); err != nil {
		log.Fatalf("Could not ping redis instance. %v", err)
	}
	redisClientWrapper := cache.NewRedisClientWrapper(redisClient)
	redisCache := cache.NewRedisStringCache(redisClientWrapper, metricsLogger)
	adgDiscoCache := cache.NewADGDiscoCache(redisCache, cfg.GetGameProductDetailsCacheName, cfg.GetGameProductDetailsTTLSeconds*time.Second)

	amendmentCacheClient := cache.NewAmendmentCacheClient(redisCache, cfg.AmendmentCacheName, cfg.AmendmentTTLSeconds*time.Second)
	asinMapCacheClient := cache.NewAsinMapCacheClient(redisCache, cfg.AsinMapCacheName, cfg.AsinMapTTLSeconds*time.Second)
	broadcasterGameOffersBlacklistCacheClient := cache.NewBroadcasterGameOffersBlacklistCacheClient(
		redisCache, cfg.BroadcasterGameOffersBlacklistCacheName, cfg.BroadcasterGameOffersBlacklistTTLSeconds*time.Second)
	broadcasterProductOffersCacheClient := cache.NewProductOffersCacheClient(redisCache, cfg.BroadcasterProductOffersCacheName, cfg.BroadcasterProductOffersTTLSeconds*time.Second)
	getGameSettingsCache := cache.NewADGDiscoCache(redisCache, cfg.GetGameSettingsCacheName, cfg.GetGameSettingsTTLSeconds*time.Second)
	getMerchandiseCache := cache.NewADGDiscoCache(redisCache, cfg.GetMerchandiseCacheName, cfg.GetMerchandiseTTLSeconds*time.Second)
	getOffersCache := cache.NewADGDiscoCache(redisCache, cfg.GetOffersCacheName, cfg.GetOffersTTLSeconds*time.Second)
	getExtensionsProductsCache := cache.NewADGDiscoCache(redisCache, cfg.GetExtensionsProductsCacheName, cfg.GetExtensionsProductsTTLSeconds*time.Second)
	merchandiseLocalizationCache := cache.NewMerchandiseLocalizationDAOCache(redisCache, cfg.MerchandiseLocalizationCacheName, cfg.MerchandiseLocalizationTTLSeconds*time.Second)

	// Need different tts for broadcasters that have linked associate store accounts vs those that do not have linked associate store accounts
	linkedAssociatesStoreCache := cache.NewAssociatesStoreCacheClient(redisCache, cfg.AssociatesStoreCacheName, cfg.LinkedAssociatesStoreTTLSeconds*time.Second)
	notLinkedAssociatesStoreCache := cache.NewAssociatesStoreCacheClient(redisCache, cfg.AssociatesStoreCacheName, cfg.NotLinkedAssociatesStoreTTLSeconds*time.Second)
	moneyPennyStoreCache := cache.NewMoneyPennyCache(redisCache, cfg.MoneyPennyIsAffiliateCacheName, cfg.MoneyPennyAffiliateTTLSeconds*time.Second, cfg.MoneyPennyNonAffiliateTTLSeconds*time.Second)

	adgDiscoClient := &clients.ADGDiscoClientWrapper{
		Client:        &clients.ADGDiscoClient{SwsParams: params},
		SwsParams:     params,
		MetricsLogger: metricsLogger,
		IsDevelopment: env == DEVELOPMENTENV,
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		Cache:         adgDiscoCache,
	}

	merchADGDiscoClient := &clients.ADGDiscoClientWrapper{
		Client:        &clients.ADGDiscoClient{SwsParams: params},
		SwsParams:     params,
		MetricsLogger: metricsLogger,
		IsDevelopment: env == DEVELOPMENTENV,
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		Cache:         getMerchandiseCache,
	}

	offersADGDiscoClient := &clients.ADGDiscoClientWrapper{
		Client:        &clients.ADGDiscoClient{SwsParams: params},
		SwsParams:     params,
		MetricsLogger: metricsLogger,
		IsDevelopment: env == DEVELOPMENTENV,
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		Cache:         getOffersCache,
	}

	extADGDiscoClient := &clients.ADGDiscoClientWrapper{
		Client:        &clients.ADGDiscoClient{SwsParams: params},
		SwsParams:     params,
		MetricsLogger: metricsLogger,
		IsDevelopment: env == DEVELOPMENTENV,
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		Cache:         getExtensionsProductsCache,
	}

	tailsClient := &clients.TailsClientWrapper{
		Client:        &clients.TailsClient{SWSParams: params},
		SWSParams:     params,
		MetricsLogger: metricsLogger,
		IsDevelopment: env == DEVELOPMENTENV,
		RetryStrategy: coral.NewDefaultRetryStrategy(),
	}

	moneyPennyConf := twitchclient.ClientConf{
		DialTimeout: cfg.MoneyPennyTimeoutMillisecond * time.Millisecond,
		Host:        cfg.MoneyPennyHost,
	}

	moneyPennyClient, err := moneypenny.NewClient(moneyPennyConf)
	if err != nil {
		log.Fatalf("Failed to initialize moneypenny client: %+v", err)
	}

	adgEntitleClient := &clients.ADGEntitleClientWrapper{
		Client:        &clients.ADGEntitleClient{SwsParams: params},
		SwsParams:     params,
		MetricsLogger: metricsLogger,
		RetryStrategy: coral.NewDefaultRetryStrategy(),
	}

	makoClient := &clients.MakoClientWrapper{
		MakoClient: &clients.MakoClient{
			MakoEndpoint: cfg.MakoEndpoint,
		},
		MetricsLogger: metricsLogger,
	}

	chatBadgesClient := &clients.ChatBadgesClientWrapper{
		ChatBadgesClient: &clients.ChatBadgesClient{
			ExternalChatBadgesEndpoint: cfg.ChatBadgesExternalEndpoint,
			InternalChatBadgesEndpoint: cfg.ChatBadgesInternalEndpoint,
		},
		MetricsLogger: metricsLogger,
	}

	// The secret shared with associates is base64-encoded before being stored in sandstorm and must be
	// base64-decoded in order to sign the jwt
	associatesSecret, err := sandstorm.Get(cfg.AssociatesSecret)
	if err != nil {
		log.Fatalf("Failed to load Associates key from Sandstorm: %+v", err)
	}
	decodedAssociatesSecret := make([]byte, len(associatesSecret.Plaintext))
	_, err = base64.StdEncoding.Decode(decodedAssociatesSecret, associatesSecret.Plaintext)
	if err != nil {
		log.Fatalf("Failure base64-decoding associates secret: %s", err)
	}

	associatesClient, err := clients.NewAssociatesExternalClient(cfg.AssociatesEndpointURL)
	if err != nil {
		log.Fatalf("Failed to initalize users service client: %s", err)
	}

	associatesClientWrapper := &clients.AssociatesClientWrapper{
		AssociatesClient:     associatesClient,
		MetricsLogger:        metricsLogger,
		JWTHelper:            &auth.JWTHelper{},
		AssociatesSecret:     decodedAssociatesSecret,
		LinkedCacheClient:    linkedAssociatesStoreCache,
		NotLinkedCacheClient: notLinkedAssociatesStoreCache,
	}

	ddb := dynamodb.New(
		session.New(),
		&aws.Config{
			Region: aws.String(cfg.DynamoRegion),
		})
	dynamoClient := dynamo.NewClient(cfg.DynamoTablePrefix, ddb, metricsLogger)

	amendmentDao := dynamo.NewAmendmentDao(dynamoClient, amendmentCacheClient)

	fuelRevenueDao := dynamo.NewFuelRevenueDao(dynamoClient)

	extensionsRevenueDao := dynamo.NewExtensionsRevenueDao(dynamoClient)

	fuelChatNotificationDAO := dynamo.NewFuelChatNotificationDAO(dynamoClient)

	activeMarketplaceDAO := dynamo.NewActiveMarketplaceDAO(dynamoClient)

	fuelChatNotificationContentsDAO := dynamo.NewFuelChatNotificationContentsDAO(dynamoClient)

	merchandiseMetadataDAO := dynamo.NewMerchandiseMetadataDAO(dynamoClient)

	merchandiseLocalizationDAO := dynamo.NewMerchandiseLocalizationDAO(dynamoClient, merchandiseLocalizationCache)

	merchandiseLifestyleImagesDAO := dynamo.NewMerchandiseLifestyleImagesDAO(dynamoClient)

	permissionsDynamoClient := dynamo.NewClient("", ddb, metricsLogger)
	permissionsDAO := dynamo.NewPermissionDAO(permissionsDynamoClient)

	broadcasterProductOffers := dynamo.NewProductOffersDAO(dynamoClient, dynamo.BroadcasterProductOffersTableBaseName, broadcasterProductOffersCacheClient)

	broadcasterGameOffersBlacklistDAO := dynamo.NewBroadcasterGameOffersBlacklistDAO(dynamoClient, broadcasterGameOffersBlacklistCacheClient)

	tmiClient := clients.NewTMIClient(cfg.TMIUrl, metricsLogger)

	appSettingsConf := twitchhttp.ClientConf{
		Host: cfg.AppSettingsHost,
	}

	appSettingsClient, err := appsettings.NewClient(appSettingsConf)
	if err != nil {
		log.Fatalf("Failed to initialize app settings client: %+v", err)
	}

	broadcasterHelper := broadcaster.NewBroadcasterHelper(broadcasterGameOffersBlacklistDAO)

	crateHelper := crate.NewCrateHelper(appSettingsClient)

	payoutsEligibilityChecker := payouts.NewPayoutsEligibilityChecker(amendmentDao, associatesClientWrapper, cfg.GameCommerceAmendmentType, cfg.GameCommerceV2AmendmentType, metricsLogger, moneyPennyClient, moneyPennyStoreCache)

	fuelAsinMapDAO := dynamo.NewFuelAsinMapDAO(dynamoClient)

	asinMap := metadata.NewAsinMap(fuelAsinMapDAO, asinMapCacheClient)

	gameMetadataDB := metadata.NewGameMetadataDB(cfg.GameMetadataMap, asinMap)

	spadeClient, err := spade.NewClient()
	if err != nil {
		log.Fatalf("Failed to initialize the Spade client. Error: %v", err)
	}

	pubSubConfig := twitchhttp.ClientConf{
		Host:           cfg.PubSubEndpoint,
		TimingXactName: "janus",
	}
	pubSubClient, err := pubClient.NewPubClient(pubSubConfig)
	if err != nil {
		log.Fatalf("Failed to initalize pub client: %s", err)
	}
	pubSubWrapper := clients.NewPubSubWrapper(pubSubClient, metricsLogger)

	zumaConfig := twitchhttp.ClientConf{
		Host:           cfg.ZumaEndpoint,
		TimingXactName: "janus",
	}
	zumaClient, err := zuma.NewClient(zumaConfig)
	if err != nil {
		log.Fatalf("Failed to initialize zuma client: %s", err)
	}

	zumaWrapper := clients.NewZumaWrapper(zumaClient, metricsLogger)

	slacker := clients.NewSlacker(cfg.PurchaseNotificationSlackWebHook, metricsLogger)

	slackNotifier := notification.NewSlackNotifier(slacker, usersServiceClient)
	pubsubNotifier := notification.NewPubSubNotifier(payoutsEligibilityChecker, pubSubWrapper, usersServiceClient, zumaWrapper)
	chatNotifier := notification.NewChatNotifier(tmiClient)

	// Build language matcher for localization
	localesList := []language.Tag{language.Make(cfg.DefaultLocale)}
	for localeTag, _ := range cfg.TwitchLanguageSupportList {
		localesList = append(localesList, language.Make(localeTag))
	}
	supportedLanguagesMatcher := language.NewMatcher(localesList)

	// Initialize APIs
	gameDetailsAPI := api.GameDetailsAPI{
		ADGDiscoClient:         adgDiscoClient,
		ADGEntitleClient:       adgEntitleClient,
		GameMetadataDB:         gameMetadataDB,
		GatingHelper:           gatingHelper,
		RatingMap:              cfg.RatingMap,
		FulfillmentMetadataMap: cfg.FulfillmentMetadataMap,
		CheckoutDestinationURL: cfg.CheckoutDestinationURL,
		DetailsImageAssetsURL:  cfg.DetailsImageAssetsURL,
		LanguageRankingMap:     cfg.LanguageRankingMap,
		LanguageDisplayMap:     cfg.LanguageDisplayMap,
		CrateHelper:            crateHelper,
	}

	inGameContentDetailsAPI := api.InGameContentDetailsAPI{
		ADGDiscoClient:         adgDiscoClient,
		ADGEntitleClient:       adgEntitleClient,
		GameMetadataDB:         gameMetadataDB,
		GatingHelper:           gatingHelper,
		CheckoutDestinationURL: cfg.CheckoutDestinationURL,
		DetailsImageAssetsURL:  cfg.DetailsImageAssetsURL,
		MetricsLogger:          metricsLogger,
		CrateHelper:            crateHelper,
	}

	getOffersAPI := api.GetOffersAPI{
		ADGDiscoClient:                  offersADGDiscoClient,
		AssociatesClient:                associatesClientWrapper,
		BroadcasterHelper:               broadcasterHelper,
		DetailPageAssociatesRedirectURL: cfg.DetailPageAssociatesRedirectURL,
		GatingHelper:                    gatingHelper,
		BroadcasterProductOffersDAO:     broadcasterProductOffers,
		IsDevelopment:                   env == DEVELOPMENTENV,
		TwitchAssociatesStoreID:         cfg.TwitchAssociatesStoreID,
	}

	gameSettingsAPI := api.GameSettingsAPI{
		GameMetadataDB: gameMetadataDB,
		GatingHelper:   gatingHelper,
		Cache:          getGameSettingsCache,
	}

	gameAsinAPI := api.GameAsinAPI{
		AsinMap: asinMap,
	}

	gameCommerceRevenueAPI := api.GameCommerceRevenueAPI{
		FuelRevenueDAO:       fuelRevenueDao,
		ExtensionsRevenueDAO: extensionsRevenueDao,
		CartmanAuthorizer:    &auth.CartmanAuthorizer{},
		MoneyPennyClient:     moneyPennyClient,
		GatingHelper:         gatingHelper,
		MetricLogger:         metricsLogger,
	}

	broadcasterPayoutDetailsAPI := api.BroadcasterPayoutDetailsAPI{
		PayoutsEligibilityChecker: payoutsEligibilityChecker,
	}

	chatNotificationAPI := api.ChatNotificationAPI{
		FuelChatNotificationDAO:         fuelChatNotificationDAO,
		FuelChatNotificationContentsDAO: fuelChatNotificationContentsDAO,
		CartmanAuthorizer:               &auth.CartmanAuthorizer{},
		ChatBadgesClientWrapper:         chatBadgesClient,
		MakoClientWrapper:               makoClient,
		GatingHelper:                    gatingHelper,
		SlackNotifier:                   slackNotifier,
		PubSubNotifier:                  pubsubNotifier,
		ChatNotifier:                    chatNotifier,
		ADGDiscoClient:                  adgDiscoClient,
		MetricLogger:                    metricsLogger,
	}

	amendmentAcceptanceAPI := api.AmendmentAcceptanceAPI{
		AmendmentDAO:       amendmentDao,
		CartmanAuthorizer:  &auth.CartmanAuthorizer{},
		GatingHelper:       gatingHelper,
		AmendmentTypes:     cfg.AmendmentTypes,
		SpadeClient:        spadeClient,
		UsersServiceClient: usersServiceClient,
		MoneyPennyClient:   moneyPennyClient,
		MetricLogger:       metricsLogger,
	}

	associatesStoreAPI := api.AssociatesStoreAPI{
		AssociatesClient:  associatesClientWrapper,
		CartmanAuthorizer: &auth.CartmanAuthorizer{},
		GatingHelper:      gatingHelper,
	}

	getCratesAPI := api.GetCratesAPI{
		ADGEntitleClient:     adgEntitleClient,
		ADGCratesDomainID:    cfg.ADGCratesDomainID,
		GatingHelper:         gatingHelper,
		RSWOpenCrateEndpoint: cfg.RSWOpenCrateEndpoint,
	}

	getBadgesAPI := api.GetBadgesAPI{
		GatingHelper:            gatingHelper,
		ChatBadgesClientWrapper: chatBadgesClient,
		MakoClientWrapper:       makoClient,
	}

	getEmoticonsAPI := api.GetEmoticonsAPI{
		GatingHelper:      gatingHelper,
		MakoClientWrapper: makoClient,
		MetricsLogger:     metricsLogger,
	}

	getActiveMarketplacesAPI := api.GetActiveMarketplacesAPI{
		ActiveMarketplaceDAO:   activeMarketplaceDAO,
		GatingHelper:           gatingHelper,
		MarketplaceToDomainMap: cfg.MarketplaceToDomainMap,
		IsPaypalLaunched:       cfg.IsPaypalLaunched,
	}

	getMerchandiseAPI := api.GetMerchandiseAPI{
		ADGDiscoClient:                merchADGDiscoClient,
		MerchandiseMetadataDAO:        merchandiseMetadataDAO,
		MerchandiseLocalizationDAO:    merchandiseLocalizationDAO,
		SupportedLanguagesMatcher:     supportedLanguagesMatcher,
		MerchandiseLifestyleImagesDAO: merchandiseLifestyleImagesDAO,
	}

	extensionsProductsAPI := api.ExtensionsProductsAPI{
		ADGDiscoClient:            extADGDiscoClient,
		CheckoutDestinationURL:    cfg.CheckoutDestinationURL,
		SupportedLanguagesMatcher: supportedLanguagesMatcher,
		TailsClient:               tailsClient,
	}

	offersSettingsAPI := api.OffersSettingsAPI{
		BroadcasterHelper:           broadcasterHelper,
		BroadcasterProductOffersDAO: broadcasterProductOffers,
		GatingHelper:                gatingHelper,
	}

	authServer := twirp.NewPermissionsService(permissionsDAO)

	serverAPI := api.ServerAPI{
		AmendmentAcceptanceAPI:      amendmentAcceptanceAPI,
		AssociatesStoreAPI:          associatesStoreAPI,
		AuthApi:                     authServer,
		BroadcasterPayoutDetailsAPI: broadcasterPayoutDetailsAPI,
		ChatNotificationAPI:         chatNotificationAPI,
		InGameContentDetailsAPI:     inGameContentDetailsAPI,
		GameAsinAPI:                 gameAsinAPI,
		GameCommerceRevenueAPI:      gameCommerceRevenueAPI,
		GameDetailsAPI:              gameDetailsAPI,
		GameSettingsAPI:             gameSettingsAPI,
		GetActiveMarketplacesAPI:    getActiveMarketplacesAPI,
		GetOffersAPI:                getOffersAPI,
		GetBadgesAPI:                getBadgesAPI,
		GetCratesAPI:                getCratesAPI,
		GetEmoticonsAPI:             getEmoticonsAPI,
		GetMerchandiseAPI:           getMerchandiseAPI,
		ExtensionsProductsAPI:       extensionsProductsAPI,
		OffersSettingsAPI:           offersSettingsAPI,
		MetricLogger:                metricsLogger,
	}

	var workerManagers []*sqs.WorkerManager
	if cfg.SQSEnabled {
		// statsd is not used, instead use cloudwatch metrics
		stats, err := statsd.NewNoopClient()
		if err != nil {
			log.Fatalf("Failed to initialize statsd client: %s", err)
		}

		sqsAWSConfig := &aws.Config{Region: aws.String(cfg.SQSRegion)}
		sqsClient := sqssdk.New(session.New(), sqsAWSConfig)

		purchaseNotificationSQSWorker := &workers.PurchaseNotificationWorker{
			FuelChatNotificationDAO: fuelChatNotificationDAO,
			ADGDiscoClient:          adgDiscoClient,
			PubSubClient:            pubSubWrapper,
		}
		workerManagers = append(workerManagers, sqs.NewSQSWorkerManager(cfg.PurchaseNotificationQueueName, cfg.PurchaseNotificationWorkers, sqsClient, purchaseNotificationSQSWorker, stats))

		purchaseNotificationForActiveMarketplaceSQSWorker := &workers.ActiveMarketplaceWorker{
			ActiveMarketplaceDAO: activeMarketplaceDAO,
		}
		workerManagers = append(workerManagers, sqs.NewSQSWorkerManager(cfg.PurchaseNotificationForActiveMarketplaceQueueName, cfg.PurchaseNotificationForActiveMarketplaceWorkers, sqsClient, purchaseNotificationForActiveMarketplaceSQSWorker, stats))

		openCrateEventSQSWorker := &workers.OpenCrateWorker{
			FuelChatNotificationContentDAO: fuelChatNotificationContentsDAO,
			PubSubClient:                   pubSubWrapper,
		}
		workerManagers = append(workerManagers, sqs.NewSQSWorkerManager(cfg.OpenCrateEventQueueName, cfg.OpenCrateEventWorkers, sqsClient, openCrateEventSQSWorker, stats))

		extensionPurchaseSQSWorker := &workers.ExtensionPurchaseWorker{
			PubSubClient: pubSubWrapper,
			SpadeClient:  spadeClient,
		}
		workerManagers = append(workerManagers, sqs.NewSQSWorkerManager(cfg.ExtensionPurchaseQueueName, cfg.ExtensionPurchaseWorkers, sqsClient, extensionPurchaseSQSWorker, stats))
	}

	// Run Sanity Check
	// TODO: Ideally we would run a set (or all) integ tests against this instance before giving it Prod traffic.
	// This will be easier with a pipeline, but for now this sanity check lets us safely do prerequisite work
	// to move towards a pipeline.  Particularly this check detects problems caused by:
	//
	// https://git-aws.internal.justin.tv/commerce/janus/pull/778
	//
	// ... which accidentally broke all SWS calls, undetected in dev/staging.
	if err := sanityCheckADGDisco(gameMetadataDB, adgDiscoClient); err != nil {
		log.Fatal(fmt.Sprintf("Sanity check failed: %s", err))
	}

	server, err := serverAPI.NewServer()
	if err != nil {
		log.Fatal(err)
	}

	serveErr := twitchhttp.ListenAndServe(server)

	for _, manager := range workerManagers {
		if err = manager.Shutdown(time.Duration(cfg.SQSShutdownTimeSeconds) * time.Second); err != nil {
			log.Errorf("Error shutting SQSWorkerManager: %+v, Error: %s", manager, err)
		}
	}

	log.Fatal(serveErr)
}

func sanityCheckADGDisco(db metadata.GameMetadataDB, wrapper *clients.ADGDiscoClientWrapper) error {
	if gameMetadata, err := db.Lookup("space fuel", true); err != nil {
		return err
	} else if _, err := wrapper.GetGameProductDetails(gameMetadata.Asin, ""); err != nil {
		return err
	}
	return nil
}

func getEnv() string {
	env := os.Getenv("JANUS_TWITCH_ENV")
	if env != "" {
		return env
	}

	return DEVELOPMENTENV
}
