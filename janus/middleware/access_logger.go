package middleware

import (
	"net/http"

	gorilla "github.com/gorilla/handlers"

	log "github.com/sirupsen/logrus"
)

// AccessLogger emits access_log style logging to janus.log for every request
//   e.g.: 127.0.0.1 - - [05/Oct/2016:15:04:08 -0700] "GET /games/Streamline/details HTTP/1.1" 200 2146
func AccessLogger(h http.Handler) http.Handler {
	return gorilla.LoggingHandler(log.StandardLogger().Writer(), h)
}
