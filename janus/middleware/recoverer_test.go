package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestRecoverer(t *testing.T) {
	Convey("Test Recoverer middleware", t, func() {
		recorder := httptest.NewRecorder()

		Convey("500 response with a panic handler", func() {
			handler := test.PanicHandler{T: t}
			httpHandler := Recoverer(handler)

			request, err := http.NewRequest("GET", "/myapi", nil)
			So(err, ShouldBeNil)
			httpHandler.ServeHTTPC(nil, recorder, request)

			So(recorder.Code, ShouldEqual, 500)

		})

		Convey("200 response with a noop handler", func() {
			handler := test.NOOPHandler{T: t}
			httpHandler := Recoverer(handler)

			request, err := http.NewRequest("GET", "/myapi", nil)
			So(err, ShouldBeNil)
			httpHandler.ServeHTTPC(nil, recorder, request)

			So(recorder.Code, ShouldEqual, 200)
		})
	})
}
