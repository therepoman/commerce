package middleware

import (
	"net/http"
	"runtime/debug"

	log "github.com/sirupsen/logrus"
	"goji.io"
	"golang.org/x/net/context"
)

// Recoverer recovers from panics and handles them as 500s
func Recoverer(h goji.Handler) goji.Handler {
	return goji.HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {

		defer func() {
			if err := recover(); err != nil {
				log.Errorf("Panic recovered: %+v", err)
				debug.PrintStack()
				http.Error(w, http.StatusText(500), 500)
			}
		}()

		h.ServeHTTPC(ctx, w, r)
	})
}
