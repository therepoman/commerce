package middleware

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"goji.io"
	"golang.org/x/net/context"
)

// URLParser parses the query params for all requests. Returns 400 on error.
func URLParser(h goji.Handler) goji.Handler {
	return goji.HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			log.Errorf("Error parsing form: %s", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		h.ServeHTTPC(ctx, w, r)
	})
}
