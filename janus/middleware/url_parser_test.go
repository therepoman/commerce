package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/janus/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestURLParser(t *testing.T) {
	Convey("With a stubbed handler", t, func() {
		recorder := httptest.NewRecorder()
		handler := test.NOOPHandler{T: t}
		httpHandler := URLParser(handler)

		Convey("when requested with a valid url", func() {

			request, err := http.NewRequest("GET", "/myapi?abc=123&userid=543", nil)
			So(err, ShouldBeNil)
			httpHandler.ServeHTTPC(nil, recorder, request)

			So(recorder.Code, ShouldEqual, 200)

		})

		Convey("when requested with an invalid url", func() {
			request, err := http.NewRequest("GET", "/myapi?foobar%%%%%%", nil)
			So(err, ShouldBeNil)
			httpHandler.ServeHTTPC(nil, recorder, request)

			So(recorder.Code, ShouldEqual, 400)
		})
	})
}
