package middleware

import (
	"net/http"

	"goji.io"

	"time"

	"code.justin.tv/commerce/janus/api/apidef"
	"code.justin.tv/commerce/janus/metrics"
	"golang.org/x/net/context"
)

// HandlerMetrics is the middleware responsible for logging latency metrics around all API
// handlers (e.g. getGameDetails). This happens independently of the API handler itself.
type HandlerMetrics struct {
	metricLogger metrics.IMetricLogger
}

// NewHandlerMetrics instantiates a new HandlerMetrics
func NewHandlerMetrics(metricLogger metrics.IMetricLogger) *HandlerMetrics {
	return &HandlerMetrics{
		metricLogger: metricLogger,
	}
}

// Metrics logs latency metrics around all API handlers
func (m *HandlerMetrics) Metrics(h goji.Handler) goji.Handler {
	return goji.HandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		h.ServeHTTPC(ctx, w, r)
		duration := time.Since(startTime)

		namedHandler := apidef.GetNamedHandler(ctx)
		if namedHandler != nil && m != nil && m.metricLogger != nil {
			m.metricLogger.LogDurationMetric(string(namedHandler.GetHandlerName()), duration)
		}
	})
}
