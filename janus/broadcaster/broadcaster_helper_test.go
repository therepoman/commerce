package broadcaster_test

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/janus/broadcaster"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBroadcasterHelper(t *testing.T) {
	Convey("When calling GetRetailEligibility", t, func() {
		broadcasterGameOffersBlacklistDAO := new(mocks.IBroadcasterGameOffersBlacklistDAO)
		broadcasterHelper := broadcaster.NewBroadcasterHelper(broadcasterGameOffersBlacklistDAO)

		Convey("Broadcaster is not blacklisted", func() {
			broadcasterGameOffersBlacklistDAO.On("IsBlacklisted", "broadcasterId", "gameId").Return(false, nil)
			retailEnabled, err := broadcasterHelper.GetRetailEligibility("broadcasterId", "gameId")
			So(err, ShouldBeNil)
			So(retailEnabled, ShouldBeTrue)
		})

		Convey("Broadcaster is blacklisted", func() {
			broadcasterGameOffersBlacklistDAO.On("IsBlacklisted", "broadcasterId", "gameId").Return(true, nil)
			retailEnabled, err := broadcasterHelper.GetRetailEligibility("broadcasterId", "gameId")
			So(err, ShouldBeNil)
			So(retailEnabled, ShouldBeFalse)
		})

		Convey("BroadcasterGameOffersBlacklistDAO fails", func() {
			broadcasterGameOffersBlacklistDAO.On("IsBlacklisted", "broadcasterId", "gameId").Return(true, errors.New("test error"))
			retailEnabled, err := broadcasterHelper.GetRetailEligibility("broadcasterId", "gameId")
			So(err, ShouldNotBeNil)
			So(retailEnabled, ShouldBeFalse)
		})

		Reset(func() {
			So(broadcasterGameOffersBlacklistDAO.AssertExpectations(t), ShouldBeTrue)
		})
	})
}
