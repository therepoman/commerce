package broadcaster

import (
	"code.justin.tv/commerce/janus/dynamo"
	log "github.com/sirupsen/logrus"
)

type IBroadcasterHelper interface {
	GetRetailEligibility(broadcasterID string, gameID string) (bool, error)
}

type BroadcasterHelper struct {
	BroadcasterGameOffersBlacklistDAO dynamo.IBroadcasterGameOffersBlacklistDAO
}

// NewBroadcasterHelper creates new broadcaster helper
func NewBroadcasterHelper(BroadcasterGameOffersBlacklistDAO dynamo.IBroadcasterGameOffersBlacklistDAO) IBroadcasterHelper {

	return &BroadcasterHelper{
		BroadcasterGameOffersBlacklistDAO: BroadcasterGameOffersBlacklistDAO,
	}
}

// GetRetailEligibility checks broadcaster blacklist to determine whether a broadcaster is eligible to display retail offers.
func (broadcasterHelper *BroadcasterHelper) GetRetailEligibility(broadcasterID string, gameID string) (bool, error) {
	isBlacklisted, err := broadcasterHelper.BroadcasterGameOffersBlacklistDAO.IsBlacklisted(broadcasterID, gameID)
	if err != nil {
		log.Errorf("Error getting broadcaster game offers blacklist for broadcaster %s, game %s: %s",
			broadcasterID, gameID, err)
		return false, err
	}
	if isBlacklisted {
		return false, nil
	}

	return true, nil
}
