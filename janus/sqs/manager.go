package sqs

import (
	"sync"

	"time"

	"code.justin.tv/chat/workerqueue"
	"github.com/cactus/go-statsd-client/statsd"

	"fmt"

	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	log "github.com/sirupsen/logrus"
)

// TODO: Migrate to shared commerce library (https://twitchtv.atlassian.net/browse/FUEL-3034)

const (
	workerVersion = workerqueue.TaskVersion(1)
)

// Worker a generic interface for all SQS workers
type Worker interface {
	Handle(msg *sqs.Message) error
}

// WorkerManager struct for managing workers for a single queue
type WorkerManager struct {
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
	worker      Worker
}

// NewSQSWorkerManager creates a new SQS worker manager for a given queue
func NewSQSWorkerManager(queueName string, numWorkers int, client sqsiface.SQSAPI, worker Worker, stats statsd.Statter) *WorkerManager {
	manager := &WorkerManager{
		stopChannel: make(chan struct{}),
		worker:      worker,
	}

	params := workerqueue.CreateWorkersParams{
		NumWorkers: numWorkers,
		QueueName:  queueName,
		Client:     client,
		Tasks: map[workerqueue.TaskVersion]workerqueue.TaskFn{
			workerqueue.FallbackTaskVersion: manager.worker.Handle,
			workerVersion:                   manager.worker.Handle,
		},
	}

	var errCh <-chan error
	var err error
	manager.waitGroup, errCh, err = workerqueue.CreateWorkers(params, manager.stopChannel, stats)
	if err != nil {
		log.Fatalf("Failed to create workers: %v", err)
	}

	go func() {
		for err := range errCh {
			log.Errorf("Error occurred in SQS worker: %v", err)
		}
	}()
	return manager
}

// Shutdown gracefully terminates SQS workers, but only waits until the timeout.
func (mgr *WorkerManager) Shutdown(teardownTimeout time.Duration) error {
	close(mgr.stopChannel)
	if waitWithTimeout(mgr.waitGroup, teardownTimeout) {
		return fmt.Errorf("timed out while waiting for SQS workers to complete")
	}
	return nil
}

// waitWithTimeout waits for wait group to complete, but only waits until the timeout. Returns true
// if the timeout was hit, false otherwise.
func waitWithTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	workersDone := make(chan struct{})
	go func() {
		defer close(workersDone)
		wg.Wait()
	}()

	select {
	case <-workersDone:
		return false // done, no timeout
	case <-time.After(timeout):
		return true // done, timed out
	}
}
