package sqs_test

import (
	"testing"

	"net/http/httptest"
	"strings"
	"time"

	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/sqs"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	awsSQS "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	timeout = 1 * time.Second
)

func TestManager(t *testing.T) {
	Convey("Test manager", t, func() {
		stats, err := statsd.NewNoopClient()
		So(err, ShouldBeNil)

		mockClient := new(mocks.SQSAPI)
		mockWorker := new(mocks.Worker)

		getQueueUrlOutput := awsSQS.GetQueueUrlOutput{
			QueueUrl: aws.String("testUrl"),
		}
		mockClient.On("GetQueueUrl", mock.Anything).Return(&getQueueUrlOutput, nil)

		getQueueAttributesOutput := awsSQS.GetQueueAttributesOutput{
			Attributes: map[string]*string{
				"MessageRetentionPeriod": aws.String("3"),
				"VisibilityTimeout":      aws.String("3"),
			},
		}
		mockClient.On("GetQueueAttributes", mock.Anything).Return(&getQueueAttributesOutput, nil)

		body := strings.NewReader("output")
		mockClient.On("ReceiveMessageRequest", mock.Anything).Return(&request.Request{
			HTTPRequest: httptest.NewRequest("GET", "/foo", strings.NewReader("input")),
			Body:        body,
		}, &awsSQS.ReceiveMessageOutput{})

		manager := sqs.NewSQSWorkerManager("queuename", 1, mockClient, mockWorker, stats)
		So(manager, ShouldNotBeNil)

		Convey("when shutdown is called", func() {
			err := manager.Shutdown(timeout)
			So(err, ShouldBeNil)
		})
	})
}
