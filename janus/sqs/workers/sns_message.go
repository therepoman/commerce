package workers

import "time"

// SNSMessage struct for representing an SNS message
type SNSMessage struct {
	Type             string    `json:"Type"`
	MessageID        string    `json:"MessageId"`
	Token            string    `json:"Token,optional"`
	TopicArn         string    `json:"TopicArn"`
	Subject          string    `json:"Subject,optional"`
	Message          string    `json:"Message"`
	SubscribeURL     string    `json:"SubscribeURL,optional"`
	Timestamp        time.Time `json:"Timestamp"`
	SignatureVersion string    `json:"SignatureVersion"`
	Signature        string    `json:"Signature"`
	SigningCertURL   string    `json:"SigningCertURL"`
	UnsubscribeURL   string    `json:"UnsubscribeURL,optional"`
}
