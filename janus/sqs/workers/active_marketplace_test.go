package workers

import (
	"testing"

	"errors"

	"code.justin.tv/commerce/janus/mocks"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func TestPurchaseNotifcationForActiveMarketplace(t *testing.T) {
	Convey("Test purchase notification worker", t, func() {
		activeMarketplaceDAO := new(mocks.IActiveMarketplaceDAO)

		worker := ActiveMarketplaceWorker{
			ActiveMarketplaceDAO: activeMarketplaceDAO,
		}

		Convey("With a nil message", func() {
			err := worker.Handle(nil)
			So(err, ShouldBeNil)
		})

		Convey("With a nil message body", func() {
			err := worker.Handle(&sqs.Message{})
			So(err, ShouldBeNil)
		})

		Convey("With bad SNS json", func() {
			body := "this isn't json"
			err := worker.Handle(&sqs.Message{Body: &body})
			So(err, ShouldNotBeNil)
		})

		Convey("With SNS message", func() {
			snsMsg := SNSMessage{MessageID: "test"}

			Convey("With bad inner message json", func() {
				snsMsg.Message = "this still isn't json"
				err := worker.Handle(&sqs.Message{Body: snsToJSON(snsMsg)})
				So(err, ShouldNotBeNil)

			})

			Convey("With proper inner purchase notication", func() {
				purchaseNotifcation := PurchaseNotification{
					UserID:        "testUserID",
					MarketplaceID: "testMarketplaceID",
				}

				Convey("Where UserID is missing", func() {
					purchaseNotifcation.UserID = ""
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where MarketplaceID is missing", func() {
					purchaseNotifcation.MarketplaceID = ""
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where dynamo put fails", func() {
					activeMarketplaceDAO.On("PutIfNotExists", Anything).Return(errors.New("Dynamo put error"))
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where dynamo put succeeds", func() {
					activeMarketplaceDAO.On("PutIfNotExists", Anything).Return(nil)
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
