package workers

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/mocks"

	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func extensionPurchaseToSNSJSON(notification ExtensionPurchase) *string {
	extensionPurchaseBytes, err := json.Marshal(notification)
	So(err, ShouldBeNil)
	extensionPurchaseJSON := string(extensionPurchaseBytes)
	sns := SNSMessage{
		MessageID: "test",
		Message:   extensionPurchaseJSON,
	}
	return snsToJSON(sns)
}

func TestExtensionPurchase(t *testing.T) {
	Convey("Test extension purchase worker", t, func() {
		pubSub := new(mocks.PubClient)
		metricsLogger := new(mocks.IMetricLogger)
		pubSubWrapper := clients.NewPubSubWrapper(pubSub, metricsLogger)
		spadeClient := new(mocks.SpadeClient)
		worker := ExtensionPurchaseWorker{
			PubSubClient: pubSubWrapper,
			SpadeClient:  spadeClient,
		}

		metricsLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		Convey("With a nil message", func() {
			err := worker.Handle(nil)
			So(err, ShouldBeNil)
		})

		Convey("With a nil message body", func() {
			err := worker.Handle(&sqs.Message{})
			So(err, ShouldBeNil)
		})

		Convey("With bad SNS json", func() {
			body := "this isn't json"
			err := worker.Handle(&sqs.Message{Body: &body})
			So(err, ShouldNotBeNil)
		})

		Convey("With SNS message", func() {
			snsMsg := SNSMessage{MessageID: "test"}

			Convey("With bad inner message json", func() {
				snsMsg.Message = "this still isn't json"
				err := worker.Handle(&sqs.Message{Body: snsToJSON(snsMsg)})
				So(err, ShouldNotBeNil)
			})

			Convey("With proper inner open crate event", func() {
				extensionPurchase := ExtensionPurchase{
					ASIN:        "asin",
					ContentType: extPurchaseContentType,
					UserID:      "userid",
					VendorCode:  "vendor",
				}

				Convey("With invalid content type", func() {
					extensionPurchase.ContentType = "otherPurchase"
					err := worker.Handle(&sqs.Message{Body: extensionPurchaseToSNSJSON(extensionPurchase)})
					So(err, ShouldBeNil)
				})

				Convey("Where ASIN is missing", func() {
					extensionPurchase.ASIN = ""
					err := worker.Handle(&sqs.Message{Body: extensionPurchaseToSNSJSON(extensionPurchase)})
					So(err, ShouldBeNil)
				})

				Convey("Where UserID is missing", func() {
					extensionPurchase.UserID = ""
					err := worker.Handle(&sqs.Message{Body: extensionPurchaseToSNSJSON(extensionPurchase)})
					So(err, ShouldBeNil)
				})

				Convey("Where VendorCode is missing", func() {
					extensionPurchase.VendorCode = ""
					err := worker.Handle(&sqs.Message{Body: extensionPurchaseToSNSJSON(extensionPurchase)})
					So(err, ShouldBeNil)
				})

				Convey("Where pubsub publish fails", func() {
					pubSub.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
					err := worker.Handle(&sqs.Message{Body: extensionPurchaseToSNSJSON(extensionPurchase)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where pubsub publish succeeds", func() {
					pubSub.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					spadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
					err := worker.Handle(&sqs.Message{Body: extensionPurchaseToSNSJSON(extensionPurchase)})
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
