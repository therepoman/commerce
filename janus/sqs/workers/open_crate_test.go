package workers

import (
	"testing"

	"errors"

	"encoding/json"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/mocks"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func openCrateToSNSJson(openCrate OpenCrateEvent) *string {
	openCrateJsonBytes, err := json.Marshal(openCrate)
	So(err, ShouldBeNil)
	json := string(openCrateJsonBytes)
	sns := SNSMessage{
		MessageID: "test",
		Message:   json,
	}
	return snsToJSON(sns)
}

func TestCrateOpen(t *testing.T) {
	Convey("Test open crate worker", t, func() {
		contentsDAO := new(mocks.IFuelChatNotificationContentsDAO)
		pubSub := new(mocks.PubClient)
		metricsLogger := new(mocks.IMetricLogger)
		pubSubWrapper := clients.NewPubSubWrapper(pubSub, metricsLogger)

		worker := OpenCrateWorker{
			FuelChatNotificationContentDAO: contentsDAO,
			PubSubClient:                   pubSubWrapper,
		}

		metricsLogger.On("LogDurationSinceMetric", Anything, Anything).Return()

		Convey("With a nil message", func() {
			err := worker.Handle(nil)
			So(err, ShouldBeNil)
		})

		Convey("With a nil message body", func() {
			err := worker.Handle(&sqs.Message{})
			So(err, ShouldBeNil)
		})

		Convey("With bad SNS json", func() {
			body := "this isn't json"
			err := worker.Handle(&sqs.Message{Body: &body})
			So(err, ShouldNotBeNil)
		})

		Convey("With SNS message", func() {
			snsMsg := SNSMessage{MessageID: "test"}

			Convey("With bad inner message json", func() {
				snsMsg.Message = "this still isn't json"
				err := worker.Handle(&sqs.Message{Body: snsToJSON(snsMsg)})
				So(err, ShouldNotBeNil)

			})

			Convey("With proper inner open crate event", func() {
				openCrateEvent := OpenCrateEvent{
					ChannelID: "channelid",
					CrateID:   "crateid",
					OrderID:   "orderid",
					UserID:    "userid",
					Contents: []CrateContent{
						{
							ContentID: "id1",
							Quantity:  1,
							Type:      "emote",
						},
						{
							ContentID: "id2",
							Quantity:  1,
							Type:      "badge",
						},
					},
				}

				Convey("Where OrderID is missing", func() {
					openCrateEvent.OrderID = ""
					err := worker.Handle(&sqs.Message{Body: openCrateToSNSJson(openCrateEvent)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where CrateId is missing", func() {
					openCrateEvent.CrateID = ""
					err := worker.Handle(&sqs.Message{Body: openCrateToSNSJson(openCrateEvent)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where contents are empty", func() {
					openCrateEvent.Contents = []CrateContent{}
					err := worker.Handle(&sqs.Message{Body: openCrateToSNSJson(openCrateEvent)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where dynamo put fails", func() {
					contentsDAO.On("PutAll", Anything).Return(errors.New("Dynamo put error"))
					err := worker.Handle(&sqs.Message{Body: openCrateToSNSJson(openCrateEvent)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where dynamo put succeeds", func() {
					contentsDAO.On("PutAll", Anything).Return(nil)

					Convey("Where pubsub publish fails", func() {
						pubSub.On("Publish", Anything, Anything, Anything, Anything).Return(errors.New("test error"))
						err := worker.Handle(&sqs.Message{Body: openCrateToSNSJson(openCrateEvent)})
						So(err, ShouldNotBeNil)
					})

					Convey("Where pubsub publish succeeds", func() {
						pubSub.On("Publish", Anything, Anything, Anything, Anything).Return(nil)
						err := worker.Handle(&sqs.Message{Body: openCrateToSNSJson(openCrateEvent)})
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
