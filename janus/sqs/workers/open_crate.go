package workers

import (
	log "github.com/sirupsen/logrus"

	"encoding/json"

	"fmt"

	"strconv"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/dynamo"
	"github.com/aws/aws-sdk-go/service/sqs"
	"golang.org/x/net/context"
)

// OpenCrateWorker worker for processing crate opening events from the SQS queue
type OpenCrateWorker struct {
	FuelChatNotificationContentDAO dynamo.IFuelChatNotificationContentsDAO
	PubSubClient                   clients.IPubSubWrapper
}

// OpenCrateEvent struct for representing a crate opening event. Can potentially include
// multiple contents coming out of a single crate
type OpenCrateEvent struct {
	CrateID   string         `json:"crate_id"`
	OrderID   string         `json:"order_id"`
	ChannelID string         `json:"channel_id"`
	UserID    string         `json:"twitch_user_id"`
	Contents  []CrateContent `json:"contents"`
}

// CrateContent struct represents a single piece of content that came out of a crate
type CrateContent struct {
	Type      string `json:"type"`
	ContentID string `json:"content_id"`
	Quantity  int    `json:"quantity"`
}

// Handle processes messages on the crate open SQS queue
func (w *OpenCrateWorker) Handle(msg *sqs.Message) error {
	if msg == nil || msg.Body == nil {
		return nil
	}

	var snsMessage SNSMessage
	if err := json.Unmarshal([]byte(*msg.Body), &snsMessage); err != nil {
		log.Errorf("Error unmarshaling message as SNS on open crate worker: %s", err)
		return err
	}

	var openCrateEvent OpenCrateEvent
	if err := json.Unmarshal([]byte(snsMessage.Message), &openCrateEvent); err != nil {
		log.Errorf("Error unmarshaling message as OpenCrateEvent on open crate worker: %s", err)
		return err
	}

	if openCrateEvent.OrderID == "" || openCrateEvent.CrateID == "" || len(openCrateEvent.Contents) == 0 {
		err := fmt.Errorf("SQS Message was missing fields: %+v", openCrateEvent)
		log.Errorf("OpenCrateWorker missing fields. Error: %s", err)
		return err
	}
	log.Infof("Handling OpenCrateEvent: %+v", openCrateEvent)

	var contentRecords []*dynamo.FuelChatNotificationContent
	for index, content := range openCrateEvent.Contents {
		contentKey := fmt.Sprintf("%s:%d", openCrateEvent.CrateID, index)
		contentRecord := &dynamo.FuelChatNotificationContent{
			Token:        openCrateEvent.OrderID,
			ContentKey:   contentKey,
			ChannelID:    openCrateEvent.ChannelID,
			UserID:       openCrateEvent.UserID,
			ContentType:  content.Type,
			ContentID:    content.ContentID,
			Quantity:     strconv.Itoa(content.Quantity),
			EventID:      openCrateEvent.CrateID,
			TimeReceived: snsMessage.Timestamp,
		}

		contentRecords = append(contentRecords, contentRecord)
	}

	if err := w.FuelChatNotificationContentDAO.PutAll(contentRecords); err != nil {
		log.Errorf("Error saving contents to dynamo: %s", err)
		return err
	}

	emptyMessage := []byte("{}")
	topic := fmt.Sprintf("%s.%s", clients.UserCommerceEventsTopicPrefix, openCrateEvent.UserID)
	err := w.PubSubClient.Publish(context.Background(), []string{topic}, clients.ChatNotificationPubSubMessageType, emptyMessage, nil)
	if err != nil {
		log.Errorf("Error publishing open crate event to pubsub: %s", err)
		return err
	}

	return nil
}
