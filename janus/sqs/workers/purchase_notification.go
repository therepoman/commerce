package workers

import (
	log "github.com/sirupsen/logrus"

	"encoding/json"
	"fmt"

	"errors"

	"strings"

	"code.justin.tv/commerce/janus/api"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/dynamo"
	"github.com/aws/aws-sdk-go/service/sqs"
	"golang.org/x/net/context"
)

// This is the same prefix TFS uses to check for Fuel Products.  See:
// https://tiny.amazon.com/f0fqqpbk/codeamazpackTwitblobac10src
const twitchFuelProductLinePrefix = "Twitch:Fuel"

// PurchaseNotificationWorker struct for processing messages on the purchase notification SQS queue
type PurchaseNotificationWorker struct {
	FuelChatNotificationDAO dynamo.IFuelChatNotificationDAO
	ADGDiscoClient          clients.IADGDiscoClientWrapper
	PubSubClient            clients.IPubSubWrapper
}

// PurchaseNotification struct for representing a purchase notification
type PurchaseNotification struct {
	UserID        string `json:"twitch_user_id"`
	ASIN          string `json:"asin"`
	OrderID       string `json:"order_id"`
	ChannelID     string `json:"channel_id"`
	MarketplaceID string `json:"marketplace_id"`
	ProductLine   string `json:"product_line"`
}

// Handle processes messages on the purchase notification SQS queue
func (w *PurchaseNotificationWorker) Handle(msg *sqs.Message) error {
	if msg == nil || msg.Body == nil {
		return nil
	}

	var snsMessage SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMessage)
	if err != nil {
		log.Errorf("Error unmarshaling message as SNS on purchase notification worker: %s", err)
		return err
	}

	var purchaseNotification PurchaseNotification
	err = json.Unmarshal([]byte(snsMessage.Message), &purchaseNotification)
	if err != nil {
		log.Errorf("Error unmarshaling message as PurchaseNotification on purchase notification worker: %s", err)
		return err
	}

	if purchaseNotification.UserID == "" || purchaseNotification.ChannelID == "" || purchaseNotification.ASIN == "" || purchaseNotification.OrderID == "" {
		return nil
	}

	// Only share from the Fuel Product Line
	// We also accept empty string in case of a TFS rollback to a version where it does not set ProductLine
	if !(purchaseNotification.ProductLine == "" || strings.HasPrefix(purchaseNotification.ProductLine, twitchFuelProductLinePrefix)) {
		return nil
	}

	product, err := w.ADGDiscoClient.GetGameProductDetails(purchaseNotification.ASIN, "")
	if err != nil {
		log.Errorf("Error making ADG Disco Call: %s", err)
		return err
	}

	if product == nil {
		msg := fmt.Sprintf("No product found for ASIN (%s) in purchase notification worker", purchaseNotification.ASIN)
		log.Error(msg)
		return errors.New(msg)
	}

	channelUser := fmt.Sprintf("%s:%s", purchaseNotification.ChannelID, purchaseNotification.UserID)
	dynamoChatNotification := &dynamo.FuelChatNotification{
		ChannelUser:     channelUser,
		ChannelID:       purchaseNotification.ChannelID,
		UserID:          purchaseNotification.UserID,
		Token:           purchaseNotification.OrderID,
		ASIN:            purchaseNotification.ASIN,
		Status:          api.FuelChatNotificationStatusAvailable,
		ProductTitle:    product.Title,
		ProductImageURL: product.IconURL,
		TimeReceived:    snsMessage.Timestamp,
	}

	err = w.FuelChatNotificationDAO.Put(dynamoChatNotification)
	if err != nil {
		log.Errorf("Error saving chat notification to dynamo: %s", err)
		return err
	}

	apiChatNotification := api.DynamoChatNotificationToAPIChatNotification(*dynamoChatNotification)
	notificationJSON, err := json.Marshal(apiChatNotification)
	if err != nil {
		log.Errorf("Error marshaling chat notification for pubsub: %s", err)
		return err
	}

	topic := fmt.Sprintf("%s.%s", clients.UserCommerceEventsTopicPrefix, purchaseNotification.UserID)
	err = w.PubSubClient.Publish(context.Background(), []string{topic}, clients.ChatNotificationPubSubMessageType, notificationJSON, nil)
	if err != nil {
		log.Errorf("Error publishing chat notification to pubsub: %s", err)
		return err
	}

	return nil
}
