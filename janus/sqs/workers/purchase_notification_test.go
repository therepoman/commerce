package workers

import (
	"testing"

	"encoding/json"
	"errors"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

func snsToJSON(sns SNSMessage) *string {
	jsonBytes, err := json.Marshal(sns)
	So(err, ShouldBeNil)
	jsonStr := string(jsonBytes)
	return &jsonStr
}

func purchaseNotificationToSNSJson(notification PurchaseNotification) *string {
	purchaseJsonBytes, err := json.Marshal(notification)
	So(err, ShouldBeNil)
	purchaseJson := string(purchaseJsonBytes)
	sns := SNSMessage{
		MessageID: "test",
		Message:   purchaseJson,
	}
	return snsToJSON(sns)
}

func TestPurchaseNotifcation(t *testing.T) {
	Convey("Test purchase notification worker", t, func() {
		chatNotificationDAO := new(mocks.IFuelChatNotificationDAO)
		adgDisco := new(mocks.IADGDiscoClientWrapper)
		pubSub := new(mocks.PubClient)
		metricsLogger := new(mocks.IMetricLogger)
		pubSubWrapper := clients.NewPubSubWrapper(pubSub, metricsLogger)

		worker := PurchaseNotificationWorker{
			FuelChatNotificationDAO: chatNotificationDAO,
			ADGDiscoClient:          adgDisco,
			PubSubClient:            pubSubWrapper,
		}

		metricsLogger.On("LogDurationSinceMetric", Anything, Anything).Return()

		Convey("With a nil message", func() {
			err := worker.Handle(nil)
			So(err, ShouldBeNil)
		})

		Convey("With a nil message body", func() {
			err := worker.Handle(&sqs.Message{})
			So(err, ShouldBeNil)
		})

		Convey("With bad SNS json", func() {
			body := "this isn't json"
			err := worker.Handle(&sqs.Message{Body: &body})
			So(err, ShouldNotBeNil)
		})

		Convey("With SNS message", func() {
			snsMsg := SNSMessage{MessageID: "test"}

			Convey("With bad inner message json", func() {
				snsMsg.Message = "this still isn't json"
				err := worker.Handle(&sqs.Message{Body: snsToJSON(snsMsg)})
				So(err, ShouldNotBeNil)

			})

			Convey("With proper inner purchase notication", func() {
				purchaseNotifcation := PurchaseNotification{
					UserID:      "testUserID",
					ASIN:        "testASIN",
					OrderID:     "testOrderID",
					ChannelID:   "testChannelID",
					ProductLine: twitchFuelProductLinePrefix + "Product",
				}

				Convey("Where UserID is missing", func() {
					purchaseNotifcation.UserID = ""
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldBeNil)
				})

				Convey("Where ASIN is missing", func() {
					purchaseNotifcation.ASIN = ""
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldBeNil)
				})

				Convey("Where OrderID is missing", func() {
					purchaseNotifcation.OrderID = ""
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldBeNil)
				})

				Convey("Where ChannelID is missing", func() {
					purchaseNotifcation.ChannelID = ""
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldBeNil)
				})

				Convey("Where ProductLine is not a Fuel product line", func() {
					purchaseNotifcation.ProductLine = "NotFuel"
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldBeNil)
				})

				Convey("Where ADGDisco call fails", func() {
					adgDisco.On("GetGameProductDetails", purchaseNotifcation.ASIN, "").Return(nil, errors.New("ADGDisco error"))
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where ADGDisco call returns no product", func() {
					adgDisco.On("GetGameProductDetails", purchaseNotifcation.ASIN, "").Return(nil, nil)
					err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
					So(err, ShouldNotBeNil)
				})

				Convey("Where ADGDisco call returns product", func() {
					product := &models.ADGProduct{
						ASIN:    purchaseNotifcation.ASIN,
						Title:   "testTitle",
						IconURL: "testURL",
					}
					adgDisco.On("GetGameProductDetails", purchaseNotifcation.ASIN, "").Return(product, nil)

					Convey("Where dynamo put fails", func() {
						chatNotificationDAO.On("Put", Anything).Return(errors.New("Dynamo put error"))
						err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
						So(err, ShouldNotBeNil)
					})

					Convey("Where dynamo put succeeds", func() {
						chatNotificationDAO.On("Put", Anything).Return(nil)

						Convey("Where pubsub publish fails", func() {
							pubSub.On("Publish", Anything, Anything, Anything, Anything).Return(errors.New("Pubsub publish error"))
							err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
							So(err, ShouldNotBeNil)
						})

						Convey("Where pubsub publish suceeds", func() {
							pubSub.On("Publish", Anything, Anything, Anything, Anything).Return(nil)
							err := worker.Handle(&sqs.Message{Body: purchaseNotificationToSNSJson(purchaseNotifcation)})
							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
