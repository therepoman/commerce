package workers

import (
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/common/spade-client-go/spade"
	"golang.org/x/net/context"

	"github.com/aws/aws-sdk-go/service/sqs"
	log "github.com/sirupsen/logrus"
)

// ExtensionPurchaseWorker struct for processing Extensions purchase messages
type ExtensionPurchaseWorker struct {
	PubSubClient clients.IPubSubWrapper
	SpadeClient  spade.Client
}

// ExtensionPurchase struct for Extension purchase message
type ExtensionPurchase struct {
	ASIN          string `json:"asin"`
	ContentType   string `json:"content_type"`
	SKU           string `json:"sku"`
	UserID        string `json:"twitch_user_id"`
	VendorCode    string `json:"vendor_code"`
	ChannelID     string `json:"channel_id"`
	MarketplaceID string `json:"marketplace_id"`
	ProductLine   string `json:"product_line"`
}

// ExtensionPurchaseNotification struct for Extension purchase pubsub message
type ExtensionPurchaseNotification struct {
	ASIN        string `json:"asin"`
	ContentType string `json:"content_type"`
	SKU         string `json:"sku"`
	UserID      string `json:"twitch_user_id"`
	VendorCode  string `json:"vendor_code"`
}

const (
	extensionPurchaseEvent = "extension_complete_buy"
	extPurchaseContentType = "extension"
)

// Handle processes Extensions purchase messages
func (w *ExtensionPurchaseWorker) Handle(msg *sqs.Message) error {
	if msg == nil || msg.Body == nil {
		return nil
	}

	var snsMessage SNSMessage
	if err := json.Unmarshal([]byte(*msg.Body), &snsMessage); err != nil {
		log.Errorf("Error unmarshaling message as SNS on extension purchase worker: %s", err)
		return err
	}

	var extensionPurchase ExtensionPurchase
	if err := json.Unmarshal([]byte(snsMessage.Message), &extensionPurchase); err != nil {
		log.Errorf("Error unmarshaling message as ExtensionPurchase on extension purchase worker: %s", err)
		return err
	}

	if !extensionPurchase.valid() {
		return nil
	}

	extensionPurchaseNotification := &ExtensionPurchaseNotification{
		ASIN:        extensionPurchase.ASIN,
		ContentType: extensionPurchase.ContentType,
		SKU:         extensionPurchase.SKU,
		UserID:      extensionPurchase.UserID,
		VendorCode:  extensionPurchase.VendorCode,
	}

	pubSubMessage, err := json.Marshal(extensionPurchaseNotification)
	if err != nil {
		log.Errorf("Error marshaling extension purchase for pubsub: %s", err)
		return err
	}

	topic := fmt.Sprintf("%s.%s", clients.ExtensionPurchaseTopicPrefix, extensionPurchase.UserID)
	if err := w.PubSubClient.Publish(context.Background(), []string{topic}, clients.ExtensionPurchaseMessageType, pubSubMessage, nil); err != nil {
		log.Errorf("Error publishing extension purchase to pubsub: %s", err)
		return err
	}

	if err := w.SpadeClient.TrackEvent(context.Background(), extensionPurchaseEvent, extensionPurchase); err != nil {
		log.Errorf("Error calling Spade for extensionPurchase: %+v error: %+v", extensionPurchase, err)
	}

	return nil
}

func (ep *ExtensionPurchase) valid() bool {
	// Verify the SNS message has the correct content_type and includes all the required fields
	return ep.ContentType == extPurchaseContentType && ep.ASIN != "" && ep.UserID != "" && ep.VendorCode != ""
}
