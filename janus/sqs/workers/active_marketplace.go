package workers

import (
	"errors"

	log "github.com/sirupsen/logrus"

	"encoding/json"

	"code.justin.tv/commerce/janus/dynamo"
	"github.com/aws/aws-sdk-go/service/sqs"
)

// ActiveMarketplaceWorker struct for processing messages on the purchase notification SQS queue
type ActiveMarketplaceWorker struct {
	ActiveMarketplaceDAO dynamo.IActiveMarketplaceDAO
}

// Handle processes messages on the purchase notification SQS queue
func (w *ActiveMarketplaceWorker) Handle(msg *sqs.Message) error {
	if msg == nil || msg.Body == nil {
		return nil
	}

	var snsMessage SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMessage)
	if err != nil {
		log.Errorf("Error unmarshaling message as SNS on purchase notification worker: %s", err)
		return err
	}

	var purchaseNotification PurchaseNotification
	err = json.Unmarshal([]byte(snsMessage.Message), &purchaseNotification)
	if err != nil {
		log.Errorf("Error unmarshaling message as PurchaseNotification on purchase notification worker: %s", err)
		return err
	}

	if purchaseNotification.UserID == "" {
		return errors.New("PurchaseNotification is missing the UserID field")
	}

	if purchaseNotification.MarketplaceID == "" {
		return errors.New("PurchaseNotification is missing the MarketplaceID field")
	}

	activeMarketplace := &dynamo.ActiveMarketplace{
		Tuid:          purchaseNotification.UserID,
		MarketplaceID: purchaseNotification.MarketplaceID,
	}

	err = w.ActiveMarketplaceDAO.PutIfNotExists(activeMarketplace)
	if err != nil {
		log.Errorf("Error saving active marketplace to dynamo: %s", err)
		return err
	}

	return nil
}
