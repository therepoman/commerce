package models

import (
	"time"
)

// GameMetadataMap maps key (game title) to GameMetadata
type GameMetadataMap map[string]GameMetadata

// GameMetadata represents a single Fuel game's entry to in the Fuel Game Metadata map.
type GameMetadata struct {
	Asin                string
	GameEmbargoDate     time.Time
	GamePublicationDate time.Time
	ReleaseEndDate      time.Time
	ReleaseStartDate    time.Time
}
