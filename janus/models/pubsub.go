package models

import "time"

// PubSubGameCommerceShareMessage defines the message format for purchase message
type PubSubGameCommerceShareMessage struct {
	UserID          string       `json:"user_id"`
	UserName        string       `json:"user_name"`
	UserDisplayName string       `json:"display_name"`
	ChannelID       string       `json:"channel_id"`
	ChannelName     string       `json:"channel_name"`
	Time            time.Time    `json:"time"`
	ShareMessage    ShareMessage `json:"purchase_message"`
	ItemImageURL    string       `json:"item_image_url"`
	ItemTitle       string       `json:"item_description"`
	SupportsChannel bool         `json:"supports_channel"`
}

// EmoteInfo defines the placement of emotes in the user custom message
type EmoteInfo struct {
	Start   int `json:"start"`
	End     int `json:"end"`
	EmoteID int `json:"id"`
}

// ShareMessage contains the user custom message and emote metadata
type ShareMessage struct {
	Message string      `json:"message"`
	Emotes  []EmoteInfo `json:"emotes"`
}
