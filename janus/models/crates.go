package models

// CrateGood containing crate data for a particular good
type CrateGood struct {
	CrateID        string
	ProductID      string
	ProductIconURL string
	ProductTitle   string
}
