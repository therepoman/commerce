package models

// GetBadgeDisplay contains information about badge sets
type GetBadgeDisplay struct {
	BadgeSets map[string]BadgeSet `json:"badge_sets"`
}

// BadgeSet contains information about a badge set versions
type BadgeSet struct {
	Versions map[string]BadgeVersion `json:"versions"`
}

// BadgeVersion contains information about a badge set
type BadgeVersion struct {
	ImageURL1x  *string `json:"image_url_1x"`
	ImageURL2x  *string `json:"image_url_2x"`
	ImageURL4x  *string `json:"image_url_4x"`
	Description string  `json:"description"`
	Title       string  `json:"title"`
	ClickAction string  `json:"click_action"`
	ClickURL    string  `json:"click_url"`
}

// GetAvailableBadgesResponse is the response for AvailableGlobalUserBadges and AvailableChannelUserBadges
type GetAvailableBadgesResponse struct {
	AvailableBadges []Badge `json:"available_badges"`
}

// Badge is defined by just a badge id and version
type Badge struct {
	BadgeSetID string `json:"badge_set_id"`
}
