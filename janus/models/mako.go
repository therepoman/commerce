package models

// MakoEmoticonSets contains a list of single Emoticons
type MakoEmoticonSets struct {
	Keys []string
}

// MakoEntitlementSets contains a list of single Badges
type MakoEntitlementSets struct {
	Keys []string
}

// Emoticon contains the details of one Emoticon
type Emoticon struct {
	ID      string
	Pattern string
}

// ChatEmoticonDetails contains emoticon set details
type ChatEmoticonDetails struct {
	Details map[string][]Emoticon
}
