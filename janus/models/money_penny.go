package models

// GetWithholdingRateResponse has tax info
type GetWithholdingRateResponse struct {
	PayoutEntityID        string             `json:"payout_entity_id"`
	OwnerChannelID        string             `json:"owner_channel_id"`
	TaxWithholdingRateMap map[string]float64 `json:"tax_withholding_rate"`
}

// GetUserPayoutTypeResponse has isAffiliate info
type GetUserPayoutTypeResponse struct {
	ChannelID   string `json:"channel_id"`
	IsAffiliate bool   `json:"is_affiliate"`
}
