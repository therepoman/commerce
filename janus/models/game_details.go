package models

// ADGProduct aggregates product information.
type ADGProduct struct {
	ASIN        string
	Description string
	Details     Details
	IconURL     string
	Price       Price
	Title       string
	ProductLine string
	SKU         string
	VendorId    string
	Type        string
	ID          string
	AsinVersion int32
	Category    string
}

// Price aggregates price information
type Price struct {
	CurrencyUnit string `json:"currency_unit"`
	Price        string `json:"price"`
}

// Details aggregates specific information about the product
type Details struct {
	Attributes        Attributes
	Background        Background
	ControllerTypes   []string
	Crate             Crate
	Developer         Developer
	ESRBRatingDetails []string
	FeaturesDetails   []string
	GenericKeywords   []string
	ItemBadgeDetails  []string
	Platforms         []string
	Screenshots       []string
	TechnicalDetails  TieredSystemRequirements
	ShortDescription  string
	ItemType          string
	LanguageType      map[string]string
	LanguageValue     map[string]string
	Videos            map[string]string
	Prime             Prime
}

// Background aggregates information about the background image
type Background struct {
	URL string `json:"url"`
}

// Crate aggregates crate information about the asin
type Crate struct {
	ASIN string `json:"asin"`
}

// Developer aggregates information about the product's developer
type Developer struct {
	Email         string `json:"email"`
	EULAURL       string `json:"eula_url"`
	Name          string `json:"name"`
	PrivacyPolicy string `json:"privacy_policy"`
	SupportURL    string `json:"support_url"`
}

// Attributes aggregates miscellaneous information about the product
type Attributes struct {
	Brand                    string `json:"brand"`
	DistributionRightsRegion string `json:"distribution_rights_region"`
	Edition                  string `json:"edition"`
	ESRBRating               string `json:"esrb_rating"`
	GameURL                  string `json:"game_url"`
	PEGIRating               string `json:"pegi_rating"`
	Publisher                string `json:"publisher"`
	ReleaseDate              string `json:"release_date"`
	ShortDescription         string `json:"short_description"`
	ShortTitle               string `json:"short_title"`
}

// TieredSystemRequirements aggregates multiple tiers of system requirements, including minimum and recommended specs
type TieredSystemRequirements struct {
	Minimum     SystemRequirements
	Recommended SystemRequirements
}

// SystemRequirements aggregates information about the hardware needed to run the product
type SystemRequirements struct {
	DirectXVersion string
	HardDriveSpace string
	OSVersion      []string
	Other          string
	Processor      []string
	RAM            string
	VideoCard      []string
}

// Prime aggregates prime information for retail items
type Prime struct {
	IsPrimeEligible bool `json:"is_prime_eligible"`
}
