package models

// RatingMap contains the mapping of rating to their display value
type RatingMap struct {
	ESRBRating map[string]string
	PEGIRating map[string]string
}
