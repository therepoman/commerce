package models

// KrakenStreamsSummaryResponse contains channel model data
type KrakenStreamsSummaryResponse struct {
	Channels int `json:"channels"`
	Viewers  int `json:"viewers"`
}
