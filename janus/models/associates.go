package models

import (
	"github.com/dgrijalva/jwt-go"
)

// MakoEmoticonSets contains a list of single Emoticons
type LinkedAssociatesStore struct {
	TUID          string `json:"twitchUserId"`
	MarketplaceID string `json:"marketplaceId"`
	StoreID       string `json:"storeId"`
	Status        string `json:"status"`
}

// GetLinkedStoreForTwitchUserClaims contains the claims that should be in the jwt token for the
// GetLinkedStoreForTwitchUser associates call. The json field names are important and will break
// associates authentication if changed
type GetLinkedStoreForTwitchUserClaims struct {
	TwitchUserId     string `json:"twitchUserId"`
	MarketplaceId    string `json:"marketplaceId"`
	RequireStoreData string `json:"requireStoreData"`
	Context          string `json:"context"`
	jwt.StandardClaims
}
