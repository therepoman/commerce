package models

// FulfillmentMetadataMap maps a key (fulfillment_type) to FulfillmentDetails
type FulfillmentMetadataMap map[string]FulfillmentDetails

// FulfillmentDetails provides information related to the acquiring and launching the game
type FulfillmentDetails struct {
	AcquisitionDetails AcquisitionDetails
	PlatformDetails    PlatformDetails
}

// AcquisitionDetails contains information about how a game is acquired.
// e.g. "Get this Game" link that directs to the developer's external free-to-play download page
type AcquisitionDetails struct {
	IsExternalAcquisition  bool
	AcquisitionDownloadURL string
	AcquisitionName        string
	AcquisitionText        string
}

// PlatformDetails contains information about the platform that the game uses.
// e.g. "Play on UPlay" link that directs to a UPlay external page
type PlatformDetails struct {
	IsExternalPlatform  bool
	PlatformDownloadURL string
	PlatformName        string
	PlatformText        string
}
