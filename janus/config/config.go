package config

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"code.justin.tv/commerce/janus/models"
	valid "github.com/asaskevich/govalidator"
	log "github.com/sirupsen/logrus"
)

// Configuration info for Janus.
type Configuration struct {
	// Cartman certificates
	CartmanCerts map[string]string

	// Secret used to authenticate us with Associates
	AssociatesSecret string

	// SWS portal configuration
	SWSEnabled         bool `valid:"boolean"`
	SWSRootCertStore   string
	SWSClientCertChain string
	SWSHostAddress     string
	SWSHostPort        uint16

	// Sandstorm configuration
	SandstormRoleARN   string
	SandstormTableName string
	SandstormKeyId     string
	SandstormAWSRegion string

	// Destination URL
	CheckoutDestinationURL string

	// Respawn Associates URL
	DetailPageAssociatesRedirectURL string

	// URL for MoneyPenny client
	MoneyPennyEndpointURL string
	// URL that serves the image assets to the details page
	DetailsImageAssetsURL string
	// URL for Associates client
	AssociatesEndpointURL string

	// Map of string keys to ESRB and PEGI rating strings
	RatingMap models.RatingMap

	// Map of Fulfillment metadata. Contains links and text related to game acquisition and launching.
	FulfillmentMetadataMap models.FulfillmentMetadataMap

	// Fuel Game Metadata map
	GameMetadataMap models.GameMetadataMap

	// Ranking map for language support of Fuel content
	LanguageRankingMap map[string]int
	LanguageDisplayMap map[string]string

	// Obfuscated Marketplace to Marketplace Domain map
	MarketplaceToDomainMap map[string]string

	// Cloudwatch
	CloudwatchBufferSize               int     `valid:"required"`
	CloudwatchBatchSize                int     `valid:"required"`
	CloudwatchFlushIntervalSeconds     int     `valid:"required"`
	CloudwatchFlushCheckDelayMs        int     `valid:"required"`
	CloudwatchEmergencyFlushPercentage float64 `valid:"required"`
	CloudwatchRegion                   string  `valid:"required"`

	// AWS Dynamo configuration
	DynamoRegion      string `valid:"required"`
	DynamoTablePrefix string `valid:"required"`

	// Fuel whitelist
	FuelWhitelist map[string]bool

	// Fuel staff games whitelist
	StaffGamesWhitelist map[string]bool

	// Allowable Amendment Types
	AmendmentTypes map[string]bool

	// Game Commerce Amendment Type
	GameCommerceAmendmentType string `valid:"required"`

	// Game Commerce v2 Amendment Type
	GameCommerceV2AmendmentType string `valid:"required"`

	// TMI Client (Twitch Chat Client)
	TMIUrl string

	// ADG Domain for Crates
	ADGCratesDomainID string `valid:"required"`

	// RSW OpenCrate endpoint
	RSWOpenCrateEndpoint string `valid:"required"`

	// Mako endpoint
	MakoEndpoint string `valid:"required"`

	// Chat Badges endpoint
	ChatBadgesExternalEndpoint string `valid:"required"`
	ChatBadgesInternalEndpoint string `valid:"required"`

	// SQS
	SQSEnabled                                        bool
	SQSRegion                                         string `valid:"required"`
	SQSShutdownTimeSeconds                            int    `valid:"required"`
	PurchaseNotificationQueueName                     string `valid:"required"`
	PurchaseNotificationWorkers                       int    `valid:"required"`
	PurchaseNotificationForActiveMarketplaceQueueName string `valid:"required"`
	PurchaseNotificationForActiveMarketplaceWorkers   int    `valid:"required"`
	OpenCrateEventQueueName                           string `valid:"required"`
	OpenCrateEventWorkers                             int    `valid:"required"`
	ExtensionPurchaseQueueName                        string `valid:"required"`
	ExtensionPurchaseWorkers                          int    `valid:"required"`

	// PubSub
	PubSubEndpoint string `valid:"required"`

	// Zuma
	ZumaEndpoint string `valid:"required"`

	// UsersService
	UsersServiceEndpoint string `valid:"required"`

	// Purchase Notification Slack
	PurchaseNotificationSlackWebHook string

	// App settings host
	AppSettingsHost string `valid:"required"`

	// Redis
	RedisEndpoint string `valid:"required"`

	// MoneyPenny host
	MoneyPennyHost string `valid:"required"`

	// Locale to default to when given a nil or non-supported locale
	DefaultLocale string `valid:"required"`

	// List of Supported Twitch Languages
	TwitchLanguageSupportList map[string]bool

	// Rollbar publishing token
	RollbarToken string

	// Cache Specific Constants
	AsinMapCacheName                         string        `valid:"required"`
	AsinMapTTLSeconds                        time.Duration `valid:"required"`
	AssociatesStoreCacheName                 string        `valid:"required"`
	LinkedAssociatesStoreTTLSeconds          time.Duration `valid:"required"`
	NotLinkedAssociatesStoreTTLSeconds       time.Duration `valid:"required"`
	AmendmentCacheName                       string        `valid:"required"`
	AmendmentTTLSeconds                      time.Duration `valid:"required"`
	BroadcasterGameOffersBlacklistCacheName  string        `valid:"required"`
	BroadcasterGameOffersBlacklistTTLSeconds time.Duration `valid:"required"`
	BroadcasterProductOffersCacheName        string        `valid:"required"`
	BroadcasterProductOffersTTLSeconds       time.Duration `valid:"required"`
	GetGameProductDetailsCacheName           string        `valid:"required"`
	GetGameProductDetailsTTLSeconds          time.Duration `valid:"required"`
	GetGameSettingsCacheName                 string        `valid:"required"`
	GetGameSettingsTTLSeconds                time.Duration `valid:"required"`
	GetMerchandiseCacheName                  string        `valid:"required"`
	GetMerchandiseTTLSeconds                 time.Duration `valid:"required"`
	GetExtensionsProductsCacheName           string        `valid:"required"`
	GetExtensionsProductsTTLSeconds          time.Duration `valid:"required"`
	GetOffersCacheName                       string        `valid:"required"`
	GetOffersTTLSeconds                      time.Duration `valid:"required"`
	MerchandiseLocalizationCacheName         string        `valid:"required"`
	MerchandiseLocalizationTTLSeconds        time.Duration `valid:"required"`
	MoneyPennyIsAffiliateCacheName           string        `valid:"required"`
	MoneyPennyAffiliateTTLSeconds            time.Duration `valid:"required"`
	MoneyPennyNonAffiliateTTLSeconds         time.Duration `valid:"required"`

	IsPaypalLaunched bool `valid:"boolean"`

	TwitchAssociatesStoreID string `valid:"required"`

	MoneyPennyTimeoutMillisecond time.Duration `valid:"required"`
}

const configFileName = "%s.json"
const configSearchPath = "*/"

// LoadConfigForEnvironment : Load configuration for Janus based on the environment.
func LoadConfigForEnvironment(environment string) (*Configuration, error) {
	fileName := fmt.Sprintf(configFileName, environment)
	configFilePath, err := searchFileInPaths(fileName)
	if err != nil {
		log.Errorf("Error loading config: %v\n", err)
		return nil, err
	}
	return loadConfig(configFilePath, false)
}

// LoadAndValidateConfigForEnvironment : Load configuration for Janus based on the environment and validate it.
func LoadAndValidateConfigForEnvironment(environment string) (*Configuration, error) {
	fileName := fmt.Sprintf(configFileName, environment)
	configFilePath, err := searchFileInPaths(fileName)
	if err != nil {
		log.Errorf("Error loading config: %v\n", err)
		return nil, err
	}
	return loadConfig(configFilePath, true)
}

func loadConfig(filepath string, shouldValidate bool) (*Configuration, error) {
	file, err := os.Open(filepath)

	if err != nil {
		log.Errorf("Error opening config file: %v\n", err)
		return nil, fmt.Errorf("Failed while opening config file: %v", err)
	}
	decoder := json.NewDecoder(file)
	configuration := &Configuration{}

	err = decoder.Decode(configuration)
	if err != nil {
		log.Errorf("Error parsing config file: %v\n", err)
		return nil, fmt.Errorf("Failed while parsing configuration file: %v", err)
	}

	if shouldValidate {
		err = validateConfig(configuration)
		if err != nil {
			log.Errorf("Error while validating config file: %v\n", err)
			return nil, fmt.Errorf("Configuration file did not properly validate: %v", err)
		}
	}

	defer func() {
		err := file.Close()
		if err != nil {
			log.Errorf("Error while closing config file: %v\n", err)
		}
	}()

	return configuration, nil
}

/* make sure that nothing is nil */
/* returns nil if everything is a-ok; otherwise returns an error containing a list of all fields that are unpopulated / invalid */
func validateConfig(config *Configuration) error {
	valid.TagMap["filepath"] = valid.Validator(func(str string) bool {
		if _, err := os.Stat(str); os.IsNotExist(err) {
			return false
		}
		return true
	})

	result, err := valid.ValidateStruct(config)
	if err != nil {
		log.Errorf("Error while validating config: %v\n", err)
		return err
	}

	if result != true {
		log.Errorf("Validation returned false: %v\n", err)
		return fmt.Errorf("Error occurred while during validation of config file. Additional errors: %v", err)
	}

	return nil
}

func searchFileInPaths(fileName string) (string, error) {
	filePath := configSearchPath + fileName
	files, err := filepath.Glob(filePath)
	if err != nil {
		log.Errorf("Error searching files: %v\n", err)
		return "", err
	}
	if files != nil {
		return files[0], nil
	}
	return "", fmt.Errorf("File %s not found in pattern path: %v", fileName, filePath)
}
