This directory contains the Terraform configuration for Janus, and this file contains instructions on how to use it.

**NOTE:** It is dangerous to build this Terraform configuration in its current form due to conflict between it and the Payday Terraform configuration!  See [this Twitch wiki page](https://twitchtv.atlassian.net/wiki/display/ENG/Infrastructure+considerations+and+tools) for details.

To better understand what Terraform is and how the commands below work, refer to [Terraform documentation](https://www.terraform.io/docs/index.html).  

# Installing Terraform

For compatibility with Payday Terraform configs, use Payday's cached Terraform binaries: [terraform_0.6.14_darwin_386.zip](https://git-aws.internal.justin.tv/commerce/payday/blob/master/terraform/terraform_0.6.14_darwin_386.zip).  

1. Make a terraform directory on your box: `mkdir /usr/local/terraform` 
2. Copy the zip into that directory: `cp terraform_0.6.14_darwin_386.zip /usr/local/terraform`
3. Unzip the archive: `unzip /usr/local/terraform/terraform_0.6.14_darwin_386.zip`
4. Add the terraform folder to your path: `export PATH="$PATH:/usr/local/terraform"`

# AWS Accounts and Permissions

Janus requires two separate AWS accounts: an Isengard-managed Amazon AWS account to control Janus-specific resources and an IAM account off the twitch-aws account maintained by the Systems team at Twitch.

## twitch-bits-aws@amazon.com

1. Go to [Isengard](https://isengard.amazon.com/console-access) (Amazon link) and find the 'twitch-bits-aws@amazon.com' account.
2. Click the link for an admin console.
3. Select IAM under "Security, Identity & Compliance".
4. If you don't already have a user, create one.  
5. Add permissions based on the 'terraform' user. 
6. If you have not already created and recorded access and secret keys for this account, do so now.

The IAM account for the Isengard-managed twitch-bits-aws@amazon.com account will be referred to as the *Amazon account*.  The access and secret keys for the Amazon account will be referred to as the *Amazon access key* and *Amazon secret key*.  

## twitch-aws

1. If you do not already have access to the twitch-aws account, request access in `#systems` on Slack.
2. Once you've been given access and have logged into the account, create and record access and secret keys.

The IAM account with which you logged into the twitch-aws account will be referred to as the *Twitch account*.  The access and secret keys for the Twitch account will be referred to as the *Twitch access key* and *Twitch secret key*.

# Running Terraform

All Terraform commands should be run from one of the `code.justin.tv/commerce/janus/terraform/production` or `code.justin.tv/commerce/janus/terraform/staging` directory.  

Additionally, you will need to be connected to Twitch VPN in order to pull down some of the dependent configurations used by the Janus Terraform configuration.

Before running any Terraform commands, you may need to get dependent configuration files first by running `terraform get .`

## Terraform credentials

You'll need to provide credentials when running Terraform.  This is best done by creating a `terraform.tfvars` file under the Terraform directory.  Create a `janus/terraform/terraform.tfvars` file and populate it with the AWS keys like so:

>aws_access_key = "\<Amazon access key\>"  
>aws_secret_key = "\<Amazon secret key\>"  
>r53_aws_access_key = "\<Twitch access key\>"  
>r53_aws_secret_key = "\<Twitch secret key\>"  

If you don't use a tfvars file, you'll need to provide the credentials via CLI arguments: 

`-var 'aws_access_key=<Amazon access key>' -var 'r53_aws_access_key=<Twitch access key>' -var 'aws_secret_key=<Amazon secret key>' -var 'r53_aws_secret_key=<Twitch secret key>'`

## Terraform plan

[Terraform documentation for the plan command.](https://www.terraform.io/docs/commands/plan.html)  Plan should be run to verify changes in configuration and to generate the new state file to be used by the Apply command.

> acbc32d4a58b:staging ckaukl$ pwd  
> /Users/ckaukl/workplace/janus/src/code.justin.tv/commerce/janus/terraform/staging  
>  
> acbc32d4a58b:staging ckaukl$ terraform plan

Terraform will sync some dependent configuration and output the changes to state that would occur from running `terraform apply`.  

If you are not connected to Twitch VPN, you may see one of two errors:

Hanging at this point: 

> module.tf_simple_ec2_stack.aws_route53_record.tf_elb: Refreshing state...

Or failure of the command with this error:

> \* consul_keys.dynamic-env: Failed to read Consul key 'dynamic-envs/commerce/janus/staging': Get http://consul.internal.justin.tv/v1/kv/dynamic-envs/commerce/janus/staging?dc=sfo01: dial tcp: lookup consul.internal.justin.tv on [::1]:53: read udp [::1]:59560->[::1]:53: read: connection refused

## Terraform apply

[Terraform documentation for the apply command.](https://www.terraform.io/docs/commands/apply.html)  It is strongly recommended first to run `terraform plan` with the `-out <file>` option to write the plan to a file.  Verify the changes in the file, then run apply.

> acbc32d4a58b:staging ckaukl$ pwd  
> /Users/ckaukl/workplace/janus/src/code.justin.tv/commerce/janus/terraform/staging  
>  
> \# Create a plan file first.  
> acbc32d4a58b:staging ckaukl$ terraform plan **-out newconfig.plan**
>  
> \# Apply the plan file.  
> acbc32d4a58b:staging ckaukl$ terraform apply **newconfig.plan**
