module "tf_simple_ec2_stack" "janus" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/dta/tf_simple_ec2_stack.git?ref=1.0.2"

  owner                                    = "${var.owner}"
  aws_access_key                           = "${var.aws_access_key}"
  aws_secret_key                           = "${var.aws_secret_key}"
  aws_region                               = "${var.aws_region}"
  service_name                             = "${var.service_name}"
  env                                      = "${var.env}"
  sub_env                                  = "${var.sub_env}"
  r53_aws_access_key                       = "${var.r53_aws_access_key}"
  r53_aws_secret_key                       = "${var.r53_aws_secret_key}"
  r53_zone_id                              = "${var.r53_zone_id}"
  r53_ttl                                  = "${var.r53_ttl}"
  instance_count                           = "${var.instance_count}"
  ec2_ami                                  = "${var.ec2_ami}"
  ec2_disable_api_termination              = "${var.ec2_disable_api_termination}"
  ec2_iam_instance_profile                 = "${var.ec2_iam_instance_profile}"
  ec2_instance_initiated_shutdown_behavior = "${var.ec2_instance_initiated_shutdown_behavior}"
  ec2_instance_type                        = "${var.ec2_instance_type}"
  ec2_key_name                             = "${var.ec2_key_name}"
  ec2_monitoring                           = "${var.ec2_monitoring}"
  ec2_user_data                            = "${file(var.ec2_user_data_file)}"
  vpc_id                                   = "${var.vpc_id}"
  ec2_vpc_security_group_ids               = "${var.ec2_vpc_security_group_ids}"
  ec2_subnet_ids                           = "${var.ec2_subnet_ids}"
  ec2_root_volume_type                     = "${var.ec2_root_volume_type}"
  elb_security_group_ids                   = "${var.elb_security_group_ids}"
  elb_subnet_ids                           = "${var.elb_subnet_ids}"
  elb_listener_instance_port               = "${var.elb_listener_instance_port}"
  elb_listener_instance_protocol           = "${var.elb_listener_instance_protocol}"
  elb_listener_lb_port                     = "${var.elb_listener_lb_port}"
  elb_listener_lb_protocol                 = "${var.elb_listener_lb_protocol}"
  elb_listener_ssl_certificate_id          = "${var.elb_listener_ssl_certificate_id}"
  elb_hcheck_healthy_threshold             = "${var.elb_hcheck_healthy_threshold}"
  elb_hcheck_unhealthy_threshold           = "${var.elb_hcheck_unhealthy_threshold}"
  elb_hcheck_path                          = "${var.elb_hcheck_path}"
  elb_hcheck_interval                      = "${var.elb_hcheck_interval}"
  elb_hcheck_timeout                       = "${var.elb_hcheck_timeout}"
  elb_cross_zone_load_balancing            = "${var.elb_cross_zone_load_balancing}"
  elb_idle_timeout                         = "${var.elb_idle_timeout}"
  iam_roles                                = "${var.iam_roles}"
  internal_elb                             = "${var.internal_elb}"
}

provider "consul" {
  address = "consul.internal.justin.tv"
  datacenter = "sfo01"
  scheme = "http"
}

resource "consul_keys" "dynamic-env" {
  datacenter = "sfo01"
  key {
    name = "env-registration"
    path = "dynamic-envs/commerce/janus/staging"
    value = "{}"
    delete = true
  }
}