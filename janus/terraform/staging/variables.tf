variable "aws_access_key" {
  description = "AWS Access key for user with IAM credentials"
}

variable "aws_secret_key" {
  description = "AWS Secret key for user with IAM credentials"
}

variable "r53_zone_id" {
  description = "The ID of the hosted zone to contain this record. Default is for .us-west-2.justin.tv domain hosted in twitch-aws account"
  default     = "ZRG00SM48517Z"
}

# General Shared Variables
variable "aws_region" {
  description = "AWS region to operate in"
  default = "us-west-2"
}

variable "service_name" {
  description = "The name of the service you are launching. Used to generate the route53 fqdns for ec2 instances and the elb"
  default = "janus-staging"
}

variable "sub_env" {
  description = "Unique identifier for this particular stack"
  default = "def"
}

variable "full_env_name" {
  desription = "The full name of the AWS environment. Either development or production"
  default = "development"
}

#######
# R53 #
#######
variable "r53_aws_access_key" {
  description = "Route53 AWS access key for IAM credentials"
}

variable "r53_aws_secret_key" {
  description = "Route53 AWS secret key for IAM credentials"
}

variable "r53_ttl" {
  description = "(Required for non-alias records) The TTL of the record"
  default = "300"
}

#######
# EC2 #
#######
variable "vpc_id" {
  description = "VPC ID used for the environment"
  default = "vpc-4cfeb629"
}

variable "ec2_vpc_security_group_ids" {
  description = "A comma-separated string of security group IDs to associate with"
  default = "sg-61bcd905"
}

variable "ec2_key_name" {
  description = "The key name to use for the instance"
  default     = "janus"
}

variable "instance_count" {
  description = "The number of identical ec2 instances to create"
  default = "2"
}

variable "ec2_ami" {
  description = "The ami to launch the ec2 instances with"
  default = "ami-5e26c36d"
}

variable "ec2_placement_group" {
  description = "The AZ to start the instance in. Defaults to 'us-west-2a'"
  default = ""
}

variable "ec2_ebs_encrypted" {
  description = "enables ebs encryption"
  default = ""
}

variable "ec2_ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized"
  default = ""
}

variable "ec2_disable_api_termination" {
  description = "If true, enables EC2 Instance Termination Protection"
  default = ""
}

variable "ec2_instance_initiated_shutdown_behavior" {
  description = "Shutdown behavior for the instance. Amazon defaults this to stop for EBS-backed instances and terminate for instance-store instances"
  default = ""
}

variable "ec2_instance_type" {
  description = "The type of ec2 instances to launch (ex: t2.medium)"
  default = "c3.xlarge"
}

variable "ec2_monitoring" {
  description = "If true, the launched EC2 instance will have detailed monitoring enabled."
  default = ""
}

variable "ec2_subnet_ids" {
  description = "List of VPC Subnet IDs to launch in"
  default = "subnet-ada8f5f4,subnet-0e86646a,subnet-3dc3c44a"
}

variable "ec2_associate_public_ip_address" {
  description = "Associate a public ip address with an instance in a VPC"
  default = ""
}

variable "ec2_private_ips" {
  description = "List of private IP address to associate with the instance in a VPC"
  default = ""
}

variable "ec2_source_dest_check" {
  description = "Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs"
  default = ""
}

variable "ec2_user_data_file" {
  description = "The user data string to provide when launching the instance"
  default = "userdatafile"
}

variable "ec2_iam_instance_profile" {
  description = "The instance profile for the ec2 instances. If a profile is not provided, a new instance profile will be created and used."
  default = "janus_staging"
}

variable "iam_roles" {
  description = "A comma-separated list of IAM role names to use for the new iam instance profile. This is only used if ec2_iam_instance_profile is not set"
  default = "janus_staging"
}

variable "ec2_root_volume_type" {
  description = "The type of volume. Can be \"standard\", \"gp2\", or \"io1\". (Default: \"standard\")"
  default = ""
}

variable "ec2_root_volume_size" {
  description = "The size of the volume in gigabytes"
  default = "gp2"
}

variable "ec2_root_delete_on_termination" {
  description = "Whether the volume should be destroyed on instance termination (Default: true)"
  default = true
}

#######
# ELB #
#######
variable "elb_security_group_ids" {
  description = "A comma-separated string containing the security group ids to launch the ELB with"
  default = "sg-e56db982"
}

variable "elb_subnet_ids" {
  description = "A comma-separated string containing the subnet ids to launch the ELB with"
  default = "subnet-0e86646a,subnet-3dc3c44a,subnet-ada8f5f4"
}

variable "elb_listener_instance_port" {
  description = "The port on the instance to route to"
  default = "8000"
}

variable "elb_listener_instance_protocol" {
  description = "The protocol to use to the instance. Valid values are HTTP, HTTPS, TCP, or SSL"
  default = "HTTP"
}

variable "elb_listener_lb_port" {
  description = "The port to listen on for the load balance"
  default = "80"
}

variable "elb_listener_lb_protocol" {
  description = "The protocol to listen on. Valid values are HTTP, HTTPS, TCP, or SSL"
  default = "HTTP"
}

variable "elb_listener_ssl_certificate_id" {
  description = "The id of an SSL certificate you have uploaded to AWS IAM. Only valid when instance_protocol and lb_protocol are either HTTPS or SSL"
  default = ""
}

variable "elb_hcheck_healthy_threshold" {
  description = "The number of checks before the instance is declared healthy"
  default = "2"
}

variable "elb_hcheck_unhealthy_threshold" {
  description = "The number of checks before the instance is declared unhealthy"
  default = "3"
}

variable "elb_hcheck_path" {
  description = "The path of the check"
  default = "/hello"
}

variable "elb_hcheck_interval" {
  description = "The interval between checks"
  default = "30"
}

variable "elb_hcheck_timeout" {
  description = "The length of time before the check times out"
  default = "5"
}

variable "elb_cross_zone_load_balancing" {
  description = "Enable cross-zone load balancing"
  default = "true"
}

variable "elb_idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle. Default: 60"
  default = 60
}

variable "internal_elb" {
  description = "ELB is an internal ELB if internal_elb = true"
  default = true
}

###################
# Security groups #
###################

variable "instance_sg_ingress" {
  description = "Comma separated list of Ingress rules for instance security group"
  default = "10.0.0.0/8"
}

variable "elb_sg_ingress" {
  description = "Comma separated list of Ingress rules for elb security group"
  default = "10.0.0.0/8"
}


variable "env" {
  default = "dev"
}

variable "owner" {
  default = "fuel-web@twitch.tv"
}

##########
# Consul #
##########
variable "consul_dcs" {
  description = "A comma-separated list of Consul data centers to register this service with"
  default = "us-west2"
}

variable "consul_endpoint" {
  description = "The consul endpoint to use when registering this service"
  default = "http://consul.internal.justin.tv"
}
