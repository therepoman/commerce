#######

# EC2 #

#######

output "ec2_ids" {
  value = "${module.tf_simple_ec2_stack.ec2_ids}"
}

output "ec2_azs" {
  value = "${module.tf_simple_ec2_stack.ec2_azs}"
}

output "ec2_placement_group" {
  value = "${module.tf_simple_ec2_stack.ec2_placement_group}"
}

output "ec2_key_name" {
  value = "${module.tf_simple_ec2_stack.ec2_key_name}"
}

output "ec2_private_ips" {
  value = "${module.tf_simple_ec2_stack.ec2_private_ips}"
}

output "ec2_private_dns" {
  value = "${module.tf_simple_ec2_stack.ec2_private_dns}"
}

output "ec2_public_ips" {
  value = "${module.tf_simple_ec2_stack.ec2_public_ips}"
}

output "ec2_vpc_security_group_ids" {
  value = "${module.tf_simple_ec2_stack.ec2_vpc_security_group_ids}"
}

output "ec2_subnet_ids" {
  value = "${module.tf_simple_ec2_stack.ec2_subnet_ids}"
}

#######

# ELB #

#######

output "elb_id" {
  value = "${module.tf_simple_ec2_stack.elb_id}"
}

output "elb_name" {
  value = "${module.tf_simple_ec2_stack.elb_name}"
}

output "elb_dns_name" {
  value = "${module.tf_simple_ec2_stack.elb_dns_name}"
}

output "elb_instances" {
  value = "${module.tf_simple_ec2_stack.elb_instances}"
}

output "elb_security_groups" {
  value = "${module.tf_simple_ec2_stack.elb_security_groups}"
}

output "elb_r53_zone_id" {
  value = "${module.tf_simple_ec2_stack.elb_zone_id}"
}

output "ec2_instance_fqdn" {
  value = "${module.tf_simple_ec2_stack.ec2_instance_fqdn}"
}

###########

# ELB R53 #

###########

output "janus_elb_fqdn" {
  value = "${module.tf_simple_ec2_stack.elb_r53_fqdn}"
}