resource "aws_security_group" "server" {
  name        = "${var.env}-${var.service_name}_server"
  description = "${var.env}-${var.service_name}_server traffic"
  vpc_id      = "${var.vpc_id}"
  tags {
    Name      = "${var.env}-${var.service_name}_server"
    Owner     = "${var.owner}"
  }
}

resource "aws_security_group_rule" "server_in01" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks = ["${split(",", var.instance_sg_ingress)}"]
  security_group_id = "${aws_security_group.server.id}"
}

resource "aws_security_group_rule" "server_out01" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.server.id}"
}

resource "aws_security_group" "elb" {
  name        = "${var.env}-${var.service_name}_elb"
  description = "${var.env}-${var.service_name}_elb traffic"
  vpc_id      = "${var.vpc_id}"
  tags {
    Name      = "${var.env}-${var.service_name}_elb"
    Owner     = "${var.owner}"
  }
}

resource "aws_security_group_rule" "elb_in01" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["${split(",", var.elb_sg_ingress)}"]
  security_group_id = "${aws_security_group.elb.id}"
}

resource "aws_security_group_rule" "elb_out01" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.elb.id}"
}
