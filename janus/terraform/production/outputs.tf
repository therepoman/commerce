#######
# EC2 #
#######

output "ec2_ids" {
  value = "${join(",", aws_instance.ec2_instance.*.id)}"
}

output "ec2_azs" {
  value = "${join(",", aws_instance.ec2_instance.*.availability_zone)}"
}

output "ec2_placement_group" {
  value = "${aws_instance.ec2_instance.0.placement_group}"
}

output "ec2_key_name" {
  value = "${aws_instance.ec2_instance.0.key_name}"
}

output "ec2_private_ips" {
  value = "${join(",", aws_instance.ec2_instance.*.private_ip)}"
}

output "ec2_private_dns" {
  value = "${join(",", aws_instance.ec2_instance.*.private_dns)}"
}

output "ec2_public_ips" {
  value = "${join(",", compact(aws_instance.ec2_instance.*.public_ip))}"
}

output "ec2_vpc_security_group_ids" {
  value = "${join(",", aws_instance.ec2_instance.*.vpc_security_ids)}"
}

output "ec2_subnet_ids" {
  value = "${join(",", aws_instance.ec2_instance.*.subnet_id)}"
}

#######
# ELB #
#######

output "elb_id" {
  value = "${aws_elb.tf_elb.id}"
}

output "elb_name" {
  value = "${aws_elb.tf_elb.name}"
}

output "elb_dns_name" {
  value = "${aws_elb.tf_elb.dns_name}"
}

output "elb_instances" {
  value = "${join(",", aws_elb.tf_elb.instances)}"
}

output "elb_security_groups" {
  value = "${join(",", aws_elb.tf_elb.security_groups)}"
}

output "elb_zone_id" {
  value = "${aws_elb.tf_elb.zone_id}"
}

output "ec2_instance_fqdn" {
  value = "${join(",", aws_route53_record.ec2_instance.*.fqdn)}"
}

###########
# ELB R53 #
###########

output "elb_r53_fqdn" {
  value = "${aws_route53_record.tf_elb.fqdn}"
}
