# Used for everything except R53 entries
provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.aws_region}"
}

# Only used for R53 entries
provider "aws" {
  access_key = "${var.r53_aws_access_key}"
  secret_key = "${var.r53_aws_secret_key}"
  region = "${var.aws_region}"
  alias = "r53"
}

# module that contains twitch office subnet CIDRs
module "tf_twitch_subnets" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/dta/tf_twitch_subnets.git"
}

########################
# IAM instance profile #
########################
resource "aws_iam_instance_profile" "profile" {
  name = "${var.env}-${var.service_name}-profile"
  roles = ["${split(",", var.iam_roles)}"]
}

#######
# EC2 #
#######
resource "aws_instance" "ec2_instance" {
  count = "${var.instance_count}"
  ami = "${var.ec2_ami}"
  placement_group = "${var.ec2_placement_group}"
  ebs_optimized = "${var.ec2_ebs_optimized}"
  instance_initiated_shutdown_behavior = "${var.ec2_instance_initiated_shutdown_behavior}"
  instance_type = "${var.ec2_instance_type}"
  key_name = "${var.ec2_key_name}"
  monitoring = "${var.ec2_monitoring}"
  vpc_security_group_ids = ["${compact(concat(split(",", var.ec2_vpc_security_group_ids), aws_security_group.server.id))}"]
  subnet_id = "${element(split(",", var.ec2_subnet_ids), count.index)}"
  associate_public_ip_address = "${var.ec2_associate_public_ip_address}"
  private_ip = "${element(split(",", var.ec2_private_ips), count.index)}"
  source_dest_check = "${var.ec2_source_dest_check}"
  user_data = "${file(var.ec2_user_data_file)}"
  iam_instance_profile = "${coalesce(var.ec2_iam_instance_profile, aws_iam_instance_profile.profile.name)}"

  tags {
    Name = "${format("${var.env}-${var.service_name}%02d", count.index + 1)}"
    Owner = "${var.owner}"
  }

  root_block_device = {
    volume_type = "${var.ec2_root_volume_type}"
    volume_size = "${var.ec2_root_volume_size}"
    delete_on_termination = "${var.ec2_root_delete_on_termination}"
  }
}

resource "aws_route53_record" "ec2_instance" {
  provider = "aws.r53"
  count = "${var.instance_count}"
  zone_id = "${var.r53_zone_id}"
  name = "${concat(var.service_name, "-", var.env, "-", replace(element(aws_instance.ec2_instance.*.id, count.index), "i-", ""))}"
  ttl = "${var.r53_ttl}"
  type = "A"
  records = ["${element(aws_instance.ec2_instance.*.private_ip, count.index)}"]
}

# roles and policies for running cloudwatch logs agent to push Janus logs
resource "aws_iam_policy_attachment" "janus_push_logs" {
  name = "janus_push_logs"
  policy_arn = "${aws_iam_policy.push_logs_janus.arn}"
  roles = ["${split(",", var.iam_roles)}"]
}

# roles and policies for cloudwatch
resource "aws_iam_policy_attachment" "janus_cloudwatch" {
  name = "janus_cloudwatch"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess_janus"
  roles = ["${split(",", var.iam_roles)}"]
}

# roles and policies for accessing S3
resource "aws_iam_policy_attachment" "janus_s3" {
  name = "janus_s3"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess_janus"
  roles = ["${split(",", var.iam_roles)}"]
}

resource "aws_iam_policy" "push_logs_janus" {
  name = "push_logs_janus"
  description = "push logs to cloudwatch and read config files from S3"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::cloudwatch-logs-configs/*"
      ]
    }
  ]
}
EOF
}

#######
# ELB #
#######
resource "aws_elb" "tf_elb" {
  name = "${format("${var.env}-${var.service_name}%02d", count.index + 1)}"
  security_groups = ["${compact(concat(split(",", var.elb_security_group_ids), aws_security_group.elb.id))}"]
  subnets = ["${split(",", var.elb_subnet_ids)}"]
  instances = ["${aws_instance.ec2_instance.*.id}"]
  internal = "${var.internal_elb}"

  listener {
    instance_port = "${var.elb_listener_instance_port}"
    instance_protocol = "${var.elb_listener_instance_protocol}"
    lb_port = "${var.elb_listener_lb_port}"
    lb_protocol = "${var.elb_listener_lb_protocol}"
    ssl_certificate_id = "${var.elb_listener_ssl_certificate_id}"
  }

  health_check {
    healthy_threshold = "${var.elb_hcheck_healthy_threshold}"
    unhealthy_threshold = "${var.elb_hcheck_unhealthy_threshold}"
    target = "${var.elb_hcheck_protocol}:${var.elb_listener_instance_port}${var.elb_hcheck_path}"
    interval = "${var.elb_hcheck_interval}"
    timeout = "${var.elb_hcheck_timeout}"
  }

  cross_zone_load_balancing = "${var.elb_cross_zone_load_balancing}"
  idle_timeout = "${var.elb_idle_timeout}"

  tags {
    Name = "${format("${var.env}-${var.service_name}%02d", count.index + 1)}"
    Owner = "${var.owner}"
  }
}

###########
# ELB R53 #
###########
resource "aws_route53_record" "tf_elb" {
  provider = "aws.r53"
  zone_id = "${var.r53_zone_id}"
  name = "${var.service_name}.${var.env}"
  type = "CNAME"
  ttl = "${var.r53_ttl}"
  records = ["${aws_elb.tf_elb.dns_name}"]
}

provider "consul" {
  address = "consul.internal.justin.tv"
  datacenter = "sfo01"
  scheme = "http"
}

resource "consul_keys" "dynamic-env" {
  datacenter = "sfo01"
  key {
    name = "env-registration"
    path = "dynamic-envs/commerce/janus/production"
    value = "{}"
    delete = true
  }
}
