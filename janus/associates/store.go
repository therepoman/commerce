package associates

import (
	"strings"
)

const (
	linked                   = "Linked"
	linkedMissingInformation = "Linked_Missing_Information"
)

// IsStoreStatusRevEnabled determines whether the given status indicates that the store is enabled
// recieve revenue.
func IsStoreStatusRevEnabled(status string) bool {
	// Associates store account only recieves revenue if it's in the state "Linked" or "Linked_Missing_Information"
	// https://w.amazon.com/bin/view/TwitchAssociates/Documents/Design/BT_APIs/#HStatus
	if strings.EqualFold(status, linked) || strings.EqualFold(status, linkedMissingInformation) {
		return true
	}
	return false
}

// IsStoreStatusPayoutEnabled determines whether the given status indicates that the store owner is
// enabled to recieve payouts from the store
func IsStoreStatusPayoutEnabled(status string) bool {
	// Users can only recieve payments from amazon for their associates account if it's in the "Linked" state
	// https://w.amazon.com/bin/view/TwitchAssociates/Documents/Design/BT_APIs/#HStatus
	if strings.EqualFold(status, linked) {
		return true
	}
	return false
}
