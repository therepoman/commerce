package test

import (
	"net/http"
	"testing"

	"golang.org/x/net/context"
)

// NOOPHandler is a dummy handler for testing. It does nothing.
type NOOPHandler struct {
	T *testing.T
}

// ServeHTTPC encompasses the business logic of the handler.
func (noopHandler NOOPHandler) ServeHTTPC(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	//no-op
}

// PanicHandler is a dummy handler for testing. It triggers a panic.
type PanicHandler struct {
	T *testing.T
}

// ServeHTTPC encompasses the business logic of the handler.
func (panicHandler PanicHandler) ServeHTTPC(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	panic("panic yannick!")
}
