package functional_test

import (
	"bytes"
	"context"
	"net/http"
	"testing"

	"os"

	"net/http/httptest"

	"code.justin.tv/commerce/janus/api"
	"code.justin.tv/commerce/janus/twirp/generated/dev_portal_auth/twirp"
	"code.justin.tv/common/go_test_dynamo"
	"github.com/stretchr/testify/assert"
	"goji.io"
)

func TestMain(m *testing.M) {
	tddb := go_test_dynamo.Instance()
	code := m.Run()
	err := tddb.Shutdown()
	if err != nil {
		os.Exit(1)
	}
	os.Exit(code)
}

func ReaderToString(t *testing.T, resp *http.Response) string {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(resp.Body)
	assert.NoError(t, err)
	return buf.String()
}

type LocalDirectServer struct {
	Mux     *goji.Mux
	BaseURL string
}

func (this *LocalDirectServer) RoundTrip(request *http.Request) (*http.Response, error) {
	rw := httptest.NewRecorder()
	this.Mux.ServeHTTP(rw, request)
	rw.Flush()
	return rw.Result(), nil
}

func NewLocalDirectServer(api *api.ServerAPI) *LocalDirectServer {
	server, err := api.NewServer()
	if err != nil {
		panic(err)
	}
	return &LocalDirectServer{
		Mux:     server.Mux,
		BaseURL: "http://localhost",
	}
}

// Fluent NewRemoteClient Apis
func NewLocalClient(lds *LocalDirectServer) twirp.Auth {
	return twirp.NewAuthProtobufClient("", lds.HttpClient())
}

func (this *LocalDirectServer) HttpClient() *http.Client {
	return &http.Client{Transport: this}
}

func GetProducts(t *testing.T, lds *LocalDirectServer, tuid string) []string {
	gpr := &twirp.ProductsRequest{
		UserId: tuid,
	}
	resp, err := NewLocalClient(lds).Products(context.Background(), gpr)
	assert.NoError(t, err)
	response := make([]string, 0, 1024)
	for _, v := range resp.Products {
		response = append(response, v.Name)
	}
	return response
}

func HasPermission(t *testing.T, lds *LocalDirectServer, tuid, product, permission string) bool {
	hpr := &twirp.HasPermissionRequest{
		UserId:     tuid,
		ProductId:  product,
		Permission: permission,
	}
	resp, err := NewLocalClient(lds).HasPermission(context.Background(), hpr)
	assert.NoError(t, err)
	return resp.Result
}

func AddPermissions(t *testing.T, lds *LocalDirectServer, tuid, product string, permissions []string) {
	apr := &twirp.AddPermissionRequest{
		UserId:      tuid,
		ProductId:   product,
		Permissions: permissions,
	}
	_, err := NewLocalClient(lds).AddPermissions(context.Background(), apr)
	assert.NoError(t, err)
}
