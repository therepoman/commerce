package functional_test

import (
	"testing"

	"fmt"

	"code.justin.tv/commerce/janus/api"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	janusTwirp "code.justin.tv/commerce/janus/twirp"
	"code.justin.tv/common/go_test_dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Permissions(t *testing.T) {
	Convey("With Dynamo and Server", t, func() {
		defer go_test_dynamo.Instance().Cleanup()

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewPermissionDAO(testDynamoClient)
		So(dao.CreateTable(), ShouldBeNil)
		serverAPI := &api.ServerAPI{
			AuthApi: janusTwirp.NewPermissionsService(dao),
		}
		lds := NewLocalDirectServer(serverAPI)

		Convey("With No Permissions Records", func() {
			hasPermission := HasPermission(t, lds, "tuid", "product", "permission")
			So(hasPermission, ShouldBeFalse)
		})
		Convey("With regular permission", func() {
			tuid := "tuid"
			product := "product"
			permission := "permission"
			AddPermissions(t, lds, tuid, product, []string{permission})

			hasPermission := HasPermission(t, lds, tuid, product, permission)
			So(hasPermission, ShouldBeTrue)
		})

		Convey("With admin permission", func() {
			tuid := "tuid"
			product := "product"
			AddPermissions(t, lds, tuid, product, []string{"Admin"})

			So(HasPermission(t, lds, tuid, product, "permission"), ShouldBeTrue)
			So(HasPermission(t, lds, tuid, product, "otherPermission"), ShouldBeTrue)
		})
	})
}

func Test_Products(t *testing.T) {
	Convey("With Dynamo and Server", t, func() {
		defer go_test_dynamo.Instance().Cleanup()

		mockedMetricLogger := new(mocks.IMetricLogger)
		mockedMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		testDynamoClient := dynamo.NewClient("", go_test_dynamo.Instance().Dynamo, mockedMetricLogger)
		dao := dynamo.NewPermissionDAO(testDynamoClient)
		So(dao.CreateTable(), ShouldBeNil)
		serverAPI := &api.ServerAPI{
			AuthApi: janusTwirp.NewPermissionsService(dao),
		}
		lds := NewLocalDirectServer(serverAPI)

		Convey("With no records", func() {
			products := GetProducts(t, lds, "tuid")
			So(products, ShouldBeEmpty)
		})

		Convey("With permission on product", func() {
			tuid := "tuid"
			product := "productid"
			permission := "permission"
			AddPermissions(t, lds, tuid, product, []string{permission})

			Convey("With no matching games database game", func() {
				products := GetProducts(t, lds, "tuid")
				So(products, ShouldResemble, []string{fmt.Sprintf("Unknown game for: %s", product)})
			})
		})
	})
}
