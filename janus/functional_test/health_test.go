package functional_test

import (
	"fmt"
	"testing"

	"code.justin.tv/commerce/janus/api"
	"github.com/stretchr/testify/assert"
)

func Test_Hello(t *testing.T) {
	serverAPI := &api.ServerAPI{}

	lds := NewLocalDirectServer(serverAPI)
	resp, err := lds.HttpClient().Get(fmt.Sprintf("%s/hello", lds.BaseURL))
	assert.NoError(t, err)

	s := ReaderToString(t, resp)
	assert.Equal(t, "OK", s)
}
