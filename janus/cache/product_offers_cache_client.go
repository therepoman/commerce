package cache

import (
	"encoding/json"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	productOffersCacheVersion            = "1"
	productOffersCacheService            = "dynamoDB"
	productOffersCacheItemPrefix         = "Item"
	productOffersCacheVisibleItemsPrefix = "VisibleItems"
)

// Product offer metadata struct
type ProductOffers struct {
	Id             string
	Title          string
	Asin           string
	Priority       int64
	Status         string
	OfferStartTime time.Time
	OfferEndTime   time.Time
}

type ProductOffersCacheClient struct {
	cacheClient IKeyValueCache
	cachedAPI   string
	ttl         time.Duration
}

// IProductOffersCacheClient interface for the asin map DAO Cache
type IProductOffersCacheClient interface {
	Get(id string, asin string) *ProductOffers
	Put(productOffers *ProductOffers)
	GetVisibleItems(id string, isWhitelisted bool, isStaff bool) []*ProductOffers
	PutVisibleItems(id string, isWhitelisted bool, isStaff bool, productOffers []*ProductOffers)
}

// NewProductOffersCacheClient constructs a ProductOffersCacheClient with the passed in kvCacheClient
func NewProductOffersCacheClient(kvCacheClient IKeyValueCache, api string, ttl time.Duration) IProductOffersCacheClient {
	return &ProductOffersCacheClient{
		cacheClient: kvCacheClient,
		cachedAPI:   api,
		ttl:         ttl,
	}
}

func (cacheClient *ProductOffersCacheClient) Get(id string, asin string) *ProductOffers {
	cacheKey := buildItemKey(id, asin)
	cacheRequest := NewCacheRequest(productOffersCacheService, cacheClient.cachedAPI, cacheKey, productOffersCacheVersion)
	jsonString := cacheClient.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return nil
	}
	var productOffers ProductOffers
	if err := json.Unmarshal([]byte(jsonString), &productOffers); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", jsonString, cacheRequest, err)
		return nil
	}
	return &productOffers
}

func (cacheClient *ProductOffersCacheClient) Put(productOffers *ProductOffers) {
	cacheKey := buildItemKey(productOffers.Id, productOffers.Asin)
	cacheRequest := NewCacheRequest(productOffersCacheService, cacheClient.cachedAPI, cacheKey, productOffersCacheVersion)

	marshalledValue, err := json.Marshal(productOffers)
	if err != nil {
		log.Errorf("Failed to marshal for cache: %+v cacheRequest: %+v error: %+v", productOffers, cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}

func (cacheClient *ProductOffersCacheClient) GetVisibleItems(id string, isWhitelisted bool, isStaff bool) []*ProductOffers {
	cacheKey := buildVisibleItemsKey(id, isWhitelisted, isStaff)
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, cacheKey, productOffersCacheVersion)
	jsonString := cacheClient.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return nil
	}
	var visibleItems []*ProductOffers
	if err := json.Unmarshal([]byte(jsonString), &visibleItems); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", jsonString, cacheRequest, err)
		return nil
	}
	return visibleItems
}

func (cacheClient *ProductOffersCacheClient) PutVisibleItems(id string, isWhitelisted bool, isStaff bool, productOffers []*ProductOffers) {
	cacheKey := buildVisibleItemsKey(id, isWhitelisted, isStaff)
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, cacheKey, productOffersCacheVersion)

	marshalledValue, err := json.Marshal(productOffers)
	if err != nil {
		log.Errorf("Failed to marshal visible items: %+v cacheRequest: %+v error: %+v", productOffers, cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}

func buildItemKey(id string, asin string) string {
	keyParts := []string{productOffersCacheItemPrefix, id, asin}
	return strings.Join(keyParts, KeySeparator)
}

func buildVisibleItemsKey(id string, isWhitelisted bool, isStaff bool) string {
	keyParts := []string{productOffersCacheVisibleItemsPrefix, id, strconv.FormatBool(isWhitelisted), strconv.FormatBool(isStaff)}
	return strings.Join(keyParts, KeySeparator)
}
