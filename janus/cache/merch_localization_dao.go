package cache

import (
	"encoding/json"
	"strings"
	"time"

	"sort"

	log "github.com/sirupsen/logrus"
)

const (
	merchLocalizationDAOCacheVersion = "1"
	dynamoDB                         = "dynamoDB"
)

type IMerchandiseLocalizationDAOCache interface {
	GetLocalizations(asinList []string, locale string) []MerchandiseLocalizationCacheValue
	SetLocalizations(asinList []string, locale string, value []MerchandiseLocalizationCacheValue)
}

type MerchandiseLocalizationDAOCache struct {
	cacheClient IKeyValueCache
	cachedAPI   string
	ttl         time.Duration
}

type MerchandiseLocalizationCacheValue struct {
	Asin  string
	Title string
}

// NewMerchandiseLocalizationDAOCache constructs a new IMerchandiseLocalizationDAOCache with the passed in kvCacheClient
func NewMerchandiseLocalizationDAOCache(kvCacheClient IKeyValueCache, api string, ttl time.Duration) IMerchandiseLocalizationDAOCache {
	return &MerchandiseLocalizationDAOCache{
		cacheClient: kvCacheClient,
		cachedAPI:   api,
		ttl:         ttl,
	}
}

// GetLocalizations gets the list of adgProducts from the cacheClient for the asinList and locale
func (daoCache *MerchandiseLocalizationDAOCache) GetLocalizations(asinList []string, locale string) []MerchandiseLocalizationCacheValue {
	cacheKey := buildMerchLocaleKey(asinList, locale)
	cacheRequest := NewCacheRequest(dynamoDB, daoCache.cachedAPI, cacheKey, merchLocalizationDAOCacheVersion)
	jsonString := daoCache.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return nil
	}
	var merchandiseLocalizations []MerchandiseLocalizationCacheValue
	if err := json.Unmarshal([]byte(jsonString), &merchandiseLocalizations); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", jsonString, cacheRequest, err)
		return nil
	}
	return merchandiseLocalizations
}

// SetLocalizations sets the adgProducts in the cacheClient for the asinList, userID, docketID
func (daoCache *MerchandiseLocalizationDAOCache) SetLocalizations(asinList []string, locale string, value []MerchandiseLocalizationCacheValue) {
	cacheKey := buildMerchLocaleKey(asinList, locale)
	cacheRequest := NewCacheRequest(dynamoDB, daoCache.cachedAPI, cacheKey, merchLocalizationDAOCacheVersion)

	marshalledValue, err := json.Marshal(value)
	if err != nil {
		log.Errorf("Failed to marshal for adgProducts: %+v cacheRequest: %+v error: %+v", value, cacheRequest, err)
		return
	}
	daoCache.cacheClient.Set(cacheRequest, string(marshalledValue), daoCache.ttl)
}

func buildMerchLocaleKey(asinList []string, locale string) string {
	keyParts := append([]string{}, asinList...)
	sort.Strings(keyParts)
	keyParts = append(keyParts, locale)
	return strings.Join(keyParts, KeySeparator)
}
