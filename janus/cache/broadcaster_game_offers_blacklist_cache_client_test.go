package cache_test

import (
	"errors"
	"testing"

	"encoding/json"

	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestBroadcasterGameOffersBlacklistCacheClient_Get(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	blacklistCacheClient := cache.NewBroadcasterGameOffersBlacklistCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling GetBlacklist", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			val := blacklistCacheClient.GetBlacklist("id")

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", nil).Once()
			val := blacklistCacheClient.GetBlacklist("id")

			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("this string is not valid json", nil).Once()
			val := blacklistCacheClient.GetBlacklist("id")

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			expectedResponse := []*cache.BroadcasterGameOffersBlacklist{
				&cache.BroadcasterGameOffersBlacklist{
					BroadcasterId: "br_id",
					GameId:        "ALL",
					Blacklist:     true,
				},
				&cache.BroadcasterGameOffersBlacklist{
					BroadcasterId: "br_id",
					GameId:        "game_id",
					Blacklist:     false,
				},
			}
			marshalledBytes, jsonErr := json.Marshal(expectedResponse)
			So(jsonErr, ShouldBeNil)
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			cacheResponse := blacklistCacheClient.GetBlacklist("br_id")

			So(cacheResponse, ShouldNotBeNil)
			So(len(cacheResponse), ShouldEqual, 2)

			So(cacheResponse, ShouldResemble, expectedResponse)
		})
	})
}

func TestBroadcasterGameOffersBlacklistCacheClient_Put(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	blacklistCacheClient := cache.NewBroadcasterGameOffersBlacklistCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling PutBlacklist", t, func() {
		cacheValue := []*cache.BroadcasterGameOffersBlacklist{
			&cache.BroadcasterGameOffersBlacklist{
				BroadcasterId: "br_id",
				GameId:        "ALL",
				Blacklist:     true,
			},
		}
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			blacklistCacheClient.PutBlacklist("br_id", cacheValue)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			blacklistCacheClient.PutBlacklist("br_id", cacheValue)
		})
	})
}
