package cache

import (
	"encoding/json"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	broadcasterGameOffersBlacklistCacheVersion = "1"
	broadcasterGameOffersBlacklistCacheService = "dynamoDB"
)

// Blacklist metadata struct
type BroadcasterGameOffersBlacklist struct {
	BroadcasterId string
	GameId        string
	Blacklist     bool
}

type BroadcasterGameOffersBlacklistCacheClient struct {
	cacheClient IKeyValueCache
	cachedAPI   string
	ttl         time.Duration
}

// IBroadcasterGameOffersBlacklistCacheClient interface for the blacklist DAO Cache
type IBroadcasterGameOffersBlacklistCacheClient interface {
	GetBlacklist(broadcasterId string) []*BroadcasterGameOffersBlacklist
	PutBlacklist(broadcasterId string, broadcasterGameOffersBlacklist []*BroadcasterGameOffersBlacklist)
}

// NewBroadcasterGameOffersBlacklistCacheClient constructs a BroadcasterGameOffersBlacklistCacheClient with the passed in kvCacheClient
func NewBroadcasterGameOffersBlacklistCacheClient(kvCacheClient IKeyValueCache, api string, ttl time.Duration) IBroadcasterGameOffersBlacklistCacheClient {
	return &BroadcasterGameOffersBlacklistCacheClient{
		cacheClient: kvCacheClient,
		cachedAPI:   api,
		ttl:         ttl,
	}
}

func (cacheClient *BroadcasterGameOffersBlacklistCacheClient) GetBlacklist(broadcasterId string) []*BroadcasterGameOffersBlacklist {
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, broadcasterId, broadcasterGameOffersBlacklistCacheVersion)
	jsonString := cacheClient.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return nil
	}
	var visibleItems []*BroadcasterGameOffersBlacklist
	if err := json.Unmarshal([]byte(jsonString), &visibleItems); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", jsonString, cacheRequest, err)
		return nil
	}
	return visibleItems
}

func (cacheClient *BroadcasterGameOffersBlacklistCacheClient) PutBlacklist(broadcasterId string, broadcasterGameOffersBlacklist []*BroadcasterGameOffersBlacklist) {
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, broadcasterId, broadcasterGameOffersBlacklistCacheVersion)

	marshalledValue, err := json.Marshal(broadcasterGameOffersBlacklist)
	if err != nil {
		log.Errorf("Failed to marshal broadcaster game offers blacklist items: %+v cacheRequest: %+v error: %+v", broadcasterGameOffersBlacklist, cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}
