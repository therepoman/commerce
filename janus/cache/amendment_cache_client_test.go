package cache_test

import (
	"errors"
	"testing"

	"encoding/json"

	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAmendmentCacheClient_Get(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	amendmentCacheClient := cache.NewAmendmentCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling Get", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			isCacheHit, val := amendmentCacheClient.Get("userId", "type")

			So(isCacheHit, ShouldBeFalse)
			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", nil).Once()
			isCacheHit, val := amendmentCacheClient.Get("userId", "type")

			So(isCacheHit, ShouldBeFalse)
			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("this string is not valid json", nil).Once()
			isCacheHit, val := amendmentCacheClient.Get("userId", "type")

			So(isCacheHit, ShouldBeFalse)
			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			expectedResponse := &cache.Amendment{
				UserID:         "userId",
				AmendmentType:  "type",
				AcceptanceDate: time.Now().Truncate(time.Second),
			}
			marshalledBytes, jsonErr := json.Marshal(&cache.AmendmentCacheEntry{Amendment: expectedResponse})
			So(jsonErr, ShouldBeNil)
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			isCacheHit, cacheResponse := amendmentCacheClient.Get("userId", "type")

			So(isCacheHit, ShouldBeTrue)
			So(cacheResponse.UserID, ShouldEqual, expectedResponse.UserID)
			So(cacheResponse.AmendmentType, ShouldEqual, expectedResponse.AmendmentType)
			So(cacheResponse.AcceptanceDate.Unix(), ShouldEqual, expectedResponse.AcceptanceDate.Unix())
		})
		Convey("When cache client returns an empty cache entry (happy case)", func() {
			expectedResponse := &cache.AmendmentCacheEntry{}
			marshalledBytes, jsonErr := json.Marshal(expectedResponse)
			So(jsonErr, ShouldBeNil)
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			isCacheHit, cacheResponse := amendmentCacheClient.Get("userId", "type")

			So(isCacheHit, ShouldBeTrue)
			So(cacheResponse, ShouldBeNil)
		})
	})
}

func TestAmendmentCacheClient_Put(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	amendmentCacheClient := cache.NewAmendmentCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling Put", t, func() {
		cacheValue := &cache.Amendment{
			UserID:         "userId",
			AmendmentType:  "type",
			AcceptanceDate: time.Now(),
		}
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			amendmentCacheClient.Put(cacheValue)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			amendmentCacheClient.Put(cacheValue)
		})
	})
}

func TestAmendmentCacheClient_PutAmendmentNotFound(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	amendmentCacheClient := cache.NewAmendmentCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling PutAmendmentNotFound", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			amendmentCacheClient.PutAmendmentNotFound("userId", "type")
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			amendmentCacheClient.PutAmendmentNotFound("userId", "type")
		})
	})
}
