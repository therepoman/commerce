package cache_test

import (
	"errors"
	"testing"

	"encoding/json"

	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProductOffersCacheClient_Get(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	productOffersCacheClient := cache.NewProductOffersCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling get", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			val := productOffersCacheClient.Get("id", "asin")

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return("", nil).Once()
			val := productOffersCacheClient.Get("id", "asin")

			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return("this string is not valid json", nil).Once()
			val := productOffersCacheClient.Get("id", "asin")

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			expectedResponse := cache.ProductOffers{
				Id:             "TestId",
				Asin:           "TestAsin",
				Title:          "TestTitle",
				Priority:       1234,
				Status:         "TestStatus",
				OfferStartTime: time.Now(),
				OfferEndTime:   time.Now().Add(time.Hour * 1),
			}
			marshalledBytes, jsonErr := json.Marshal(expectedResponse)
			So(jsonErr, ShouldBeNil)
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return(jsonString, nil).Once()
			cacheResponse := productOffersCacheClient.Get("id", "asin")

			So(expectedResponse.Id, ShouldEqual, cacheResponse.Id)
			So(expectedResponse.Asin, ShouldEqual, cacheResponse.Asin)
			So(expectedResponse.Status, ShouldEqual, cacheResponse.Status)
		})
	})
}

func TestAsinMapDAOCache_Put(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	productOffersCacheClient := cache.NewProductOffersCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling Put", t, func() {
		cacheValue := cache.ProductOffers{
			Id:             "TestId",
			Asin:           "TestAsin",
			Title:          "TestTitle",
			Priority:       1234,
			Status:         "TestStatus",
			OfferStartTime: time.Now(),
			OfferEndTime:   time.Now().Add(time.Hour * 1),
		}
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			productOffersCacheClient.Put(&cacheValue)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			productOffersCacheClient.Put(&cacheValue)
		})
	})
}
