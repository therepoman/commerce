package cache

import (
	"encoding/json"
	"time"

	"code.justin.tv/commerce/janus/client"
	log "github.com/sirupsen/logrus"
)

const (
	asinMapCacheService = "AsinMap"
	asinMapCacheVersion = "1"
)

type IAsinMapCacheClient interface {
	GetAsinMap(gameTitle string) (isCacheHit bool, asinMap *janus.GameAsinMap)
	SetAsinMap(asinMap *janus.GameAsinMap)
}

type AsinMapCacheClient struct {
	cacheClient IKeyValueCache
	api         string
	asinMapTTL  time.Duration
}

func NewAsinMapCacheClient(kvCacheClient IKeyValueCache, api string, asinMapTTL time.Duration) IAsinMapCacheClient {
	return &AsinMapCacheClient{
		cacheClient: kvCacheClient,
		api:         api,
		asinMapTTL:  asinMapTTL,
	}
}

func (cache *AsinMapCacheClient) GetAsinMap(gameTitle string) (isCacheHit bool, asinMap *janus.GameAsinMap) {
	cacheRequest := NewCacheRequest(asinMapCacheService, cache.api, gameTitle, asinMapCacheVersion)
	jsonString := cache.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		log.WithFields(log.Fields{"cacheRequest": cacheRequest}).Info("Cache client returned nothing")
		return false, nil
	}

	var gameAsinMap janus.GameAsinMap
	err := json.Unmarshal([]byte(jsonString), &gameAsinMap)
	if err != nil {
		log.WithFields(log.Fields{
			"jsonString":   jsonString,
			"cacheRequest": cacheRequest,
			"error":        err,
		}).Error("Failed to unmarshal json")
		return false, nil
	}

	return true, &gameAsinMap
}

func (cache *AsinMapCacheClient) SetAsinMap(asinMap *janus.GameAsinMap) {
	cacheRequest := NewCacheRequest(asinMapCacheService, cache.api, asinMap.GameTitle, asinMapCacheVersion)
	marshalledValue, err := json.Marshal(asinMap)
	if err != nil {
		log.WithFields(log.Fields{
			"cacheRequest": cacheRequest,
			"error":        err,
		}).Error("Failed to marshal asin map")
		return
	}

	cache.cacheClient.Set(cacheRequest, string(marshalledValue), cache.asinMapTTL)
	log.WithFields(log.Fields{"cacheRequest": cacheRequest}).Info("Finished adding to cache.")
}
