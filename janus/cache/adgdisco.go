package cache

import (
	"encoding/json"
	"sort"
	"strings"
	"time"

	"code.justin.tv/commerce/janus/models"

	log "github.com/sirupsen/logrus"
)

const (
	adgDiscoCacheVersion = "1"
	adgDiscoService      = "adgDiscoService"
)

// IADGDiscoCache interface for ADGDiscoCache
type IADGDiscoCache interface {
	GetProductDetails(params *ADGDiscoCacheParams) *models.ADGProduct
	GetProductDetailsList(params *ADGDiscoCacheParams) []models.ADGProduct
	GetDomainProducts(params *ADGDiscoCacheParams) []models.ADGProduct
	SetProductDetails(params *ADGDiscoCacheParams, value *models.ADGProduct)
	SetProductDetailsList(params *ADGDiscoCacheParams, value []models.ADGProduct)
	SetDomainProducts(params *ADGDiscoCacheParams, value []models.ADGProduct)
}

// ADGDiscoCache struct for getting and setting cache values
type ADGDiscoCache struct {
	cacheClient IKeyValueCache
	cachedAPI   string
	ttl         time.Duration
}

// ADGDiscoCacheParams struct for passing fields used to construct an appropriate cache key
type ADGDiscoCacheParams struct {
	ShouldCache bool

	ASIN       string
	ASINList   []string
	UserID     string
	Docket     string
	DocketID   string
	SKU        string
	VendorCode string
	UserCOR    string
}

// NewADGDiscoCache constructs a new IADGDiscoCache with the passed in kvCacheClient
func NewADGDiscoCache(kvCacheClient IKeyValueCache, api string, ttl time.Duration) IADGDiscoCache {
	return &ADGDiscoCache{
		cacheClient: kvCacheClient,
		cachedAPI:   api,
		ttl:         ttl,
	}
}

func (adg *ADGDiscoCache) get(key string, res interface{}) {
	req := NewCacheRequest(adgDiscoService, adg.cachedAPI, key, adgDiscoCacheVersion)
	raw := adg.cacheClient.Get(req)
	if raw == "" {
		return
	}
	if err := json.Unmarshal([]byte(raw), res); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", res, req, err)
		return
	}
}

// GetProductDetails gets the adgProduct from the cacheClient for the asin, userId, docket
func (adg *ADGDiscoCache) GetProductDetails(params *ADGDiscoCacheParams) *models.ADGProduct {
	cacheKey := ProductDetailsCacheKey(params)
	var adgProduct *models.ADGProduct
	adg.get(cacheKey, &adgProduct)
	return adgProduct
}

// GetProductDetailsList gets the list of adgProducts from the cacheClient for the asinList, userId, docket
func (adg *ADGDiscoCache) GetProductDetailsList(params *ADGDiscoCacheParams) []models.ADGProduct {
	cacheKey := ProductDetailsListCacheKey(params)
	var adgProducts []models.ADGProduct
	adg.get(cacheKey, &adgProducts)
	return adgProducts
}

// GetDomainProducts get the adgProducts from the cacheClient for the vendorCode, sku, cor, pfm
func (adg *ADGDiscoCache) GetDomainProducts(params *ADGDiscoCacheParams) []models.ADGProduct {
	cacheKey := DomainProductsCacheKey(params)
	var adgProducts []models.ADGProduct
	adg.get(cacheKey, &adgProducts)
	return adgProducts
}

func (adg *ADGDiscoCache) set(key string, value interface{}) {
	if key == "" {
		log.Errorf("Failed to set cache value with empty key: %+v", value)
		return
	}
	req := NewCacheRequest(adgDiscoService, adg.cachedAPI, key, adgDiscoCacheVersion)
	raw, err := json.Marshal(value)
	if err != nil {
		log.Errorf("Failed to marshal for adgProduct: %+v cacheRequest: %+v error: %+v", value, req, err)
		return
	}
	adg.cacheClient.Set(req, string(raw), adg.ttl)
}

// SetProductDetails sets the adgProduct in the cacheClient for the asin, userId, docket
func (adg *ADGDiscoCache) SetProductDetails(params *ADGDiscoCacheParams, value *models.ADGProduct) {
	cacheKey := ProductDetailsCacheKey(params)
	adg.set(cacheKey, value)
}

// SetProductDetailsList sets the adgProducts in the cacheClient for the asinList, userID, docketID
func (adg *ADGDiscoCache) SetProductDetailsList(params *ADGDiscoCacheParams, value []models.ADGProduct) {
	cacheKey := ProductDetailsListCacheKey(params)
	adg.set(cacheKey, value)
}

// SetDomainProducts sets the adgProducts in the cacheClient for the vendorCode, sku, cor
func (adg *ADGDiscoCache) SetDomainProducts(params *ADGDiscoCacheParams, value []models.ADGProduct) {
	cacheKey := DomainProductsCacheKey(params)
	adg.set(cacheKey, value)
}

// ProductDetailsCacheKey generates a cache key for product details requests
func ProductDetailsCacheKey(params *ADGDiscoCacheParams) string {
	return buildKey(params.ASIN, params.UserID, params.Docket)
}

// ProductDetailsListCacheKey generates a cache key for product details list requests
func ProductDetailsListCacheKey(params *ADGDiscoCacheParams) string {
	list := params.ASINList
	sort.Strings(list)
	list = append(list, params.UserID, params.DocketID)
	return buildKey(list...)
}

// DomainProductsCacheKey generates a cache key for domain products requests
// Queries can be keyed off a combination of asin, vendor code, sku, and cor
func DomainProductsCacheKey(params *ADGDiscoCacheParams) string {
	list := []string{}
	if params.ASIN != "" {
		list = append(list, params.ASIN)
	}
	if params.VendorCode != "" {
		list = append(list, params.VendorCode)
	}
	if params.SKU != "" {
		list = append(list, params.SKU)
	}
	if params.UserCOR != "" {
		list = append(list, params.UserCOR)
	}
	return buildKey(list...)
}

func buildKey(vals ...string) string {
	return strings.Join(vals, KeySeparator)
}
