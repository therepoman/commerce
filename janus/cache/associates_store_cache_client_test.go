package cache_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"

	"errors"

	"encoding/json"

	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLinkedAssociatesStoreCache_Get(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	associatesStoreCache := cache.NewAssociatesStoreCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling Get", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			isCacheHit, val := associatesStoreCache.Get("userId", "marketplaceId")

			So(isCacheHit, ShouldBeFalse)
			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", nil).Once()
			isCacheHit, val := associatesStoreCache.Get("userId", "marketplaceId")

			So(isCacheHit, ShouldBeFalse)
			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("this string is not valid json", nil).Once()
			isCacheHit, val := associatesStoreCache.Get("userId", "marketplaceId")

			So(isCacheHit, ShouldBeFalse)
			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			expectedResponse := &models.LinkedAssociatesStore{
				TUID:          "userId",
				MarketplaceID: "marketplaceId",
				StoreID:       "storeId",
				Status:        "status",
			}
			marshalledBytes, jsonErr := json.Marshal(&cache.AssociatesStoreCacheEntry{LinkedAssociatesStore: expectedResponse})
			So(jsonErr, ShouldBeNil)
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			isCacheHit, cacheResponse := associatesStoreCache.Get("userId", "marketplaceId")

			So(isCacheHit, ShouldBeTrue)
			So(cacheResponse.TUID, ShouldEqual, expectedResponse.TUID)
			So(cacheResponse.MarketplaceID, ShouldEqual, expectedResponse.MarketplaceID)
			So(cacheResponse.StoreID, ShouldEqual, expectedResponse.StoreID)
			So(cacheResponse.Status, ShouldEqual, expectedResponse.Status)
		})
		Convey("When cache client returns an empty cache entry (happy case)", func() {
			expectedResponse := &cache.AssociatesStoreCacheEntry{}
			marshalledBytes, jsonErr := json.Marshal(expectedResponse)
			So(jsonErr, ShouldBeNil)
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			isCacheHit, cacheResponse := associatesStoreCache.Get("userId", "marketplaceId")

			So(isCacheHit, ShouldBeTrue)
			So(cacheResponse, ShouldBeNil)
		})
	})
}

func TestLinkedAssociatesStoreCache_Put(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	associatesStoreCache := cache.NewAssociatesStoreCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling Put", t, func() {
		cacheValue := &models.LinkedAssociatesStore{
			TUID:          "userId",
			MarketplaceID: "marketplaceId",
			StoreID:       "storeId",
			Status:        "status",
		}
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			associatesStoreCache.Put(cacheValue)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			associatesStoreCache.Put(cacheValue)
		})
	})
}

func TestLinkedAssociatesStoreCache_PutStoreInfoNotFound(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	associatesStoreCache := cache.NewAssociatesStoreCacheClient(mockedCacheClient, "testapi", time.Second)
	Convey("When calling PutStoreInfoNotFound", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			associatesStoreCache.PutStoreInfoNotFound("userId", "marketplaceId")
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			associatesStoreCache.PutStoreInfoNotFound("userId", "marketplaceId")
		})
	})
}
